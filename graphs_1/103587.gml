graph [
  maxDegree 41
  minDegree 1
  meanDegree 2.0344827586206895
  density 0.011760015945784333
  graphCliqueNumber 3
  node [
    id 0
    label "pi&#281;&#263;"
    origin "text"
  ]
  node [
    id 1
    label "kawa&#322;ek"
    origin "text"
  ]
  node [
    id 2
    label "tyler"
    origin "text"
  ]
  node [
    id 3
    label "otworzy&#263;"
    origin "text"
  ]
  node [
    id 4
    label "jad&#322;ospis"
    origin "text"
  ]
  node [
    id 5
    label "wegetaria&#324;ski"
    origin "text"
  ]
  node [
    id 6
    label "strona"
    origin "text"
  ]
  node [
    id 7
    label "gdzie"
    origin "text"
  ]
  node [
    id 8
    label "opr&#243;cz"
    origin "text"
  ]
  node [
    id 9
    label "spis"
    origin "text"
  ]
  node [
    id 10
    label "danie"
    origin "text"
  ]
  node [
    id 11
    label "by&#322;a"
    origin "text"
  ]
  node [
    id 12
    label "te&#380;"
    origin "text"
  ]
  node [
    id 13
    label "gruba"
    origin "text"
  ]
  node [
    id 14
    label "szary"
    origin "text"
  ]
  node [
    id 15
    label "koperta"
    origin "text"
  ]
  node [
    id 16
    label "zgodnie"
    origin "text"
  ]
  node [
    id 17
    label "umowa"
    origin "text"
  ]
  node [
    id 18
    label "podp&#322;ywa&#263;"
  ]
  node [
    id 19
    label "podp&#322;ywanie"
  ]
  node [
    id 20
    label "plot"
  ]
  node [
    id 21
    label "podp&#322;yn&#261;&#263;"
  ]
  node [
    id 22
    label "piece"
  ]
  node [
    id 23
    label "kawa&#322;"
  ]
  node [
    id 24
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 25
    label "utw&#243;r"
  ]
  node [
    id 26
    label "podp&#322;yni&#281;cie"
  ]
  node [
    id 27
    label "establish"
  ]
  node [
    id 28
    label "cia&#322;o"
  ]
  node [
    id 29
    label "zacz&#261;&#263;"
  ]
  node [
    id 30
    label "spowodowa&#263;"
  ]
  node [
    id 31
    label "begin"
  ]
  node [
    id 32
    label "udost&#281;pni&#263;"
  ]
  node [
    id 33
    label "roz&#322;o&#380;y&#263;"
  ]
  node [
    id 34
    label "uruchomi&#263;"
  ]
  node [
    id 35
    label "przeci&#261;&#263;"
  ]
  node [
    id 36
    label "restauracja"
  ]
  node [
    id 37
    label "chart"
  ]
  node [
    id 38
    label "od&#380;ywianie_si&#281;"
  ]
  node [
    id 39
    label "menu"
  ]
  node [
    id 40
    label "spos&#243;b"
  ]
  node [
    id 41
    label "cennik"
  ]
  node [
    id 42
    label "oferta"
  ]
  node [
    id 43
    label "bezmi&#281;sny"
  ]
  node [
    id 44
    label "wegetaria&#324;sko"
  ]
  node [
    id 45
    label "skr&#281;canie"
  ]
  node [
    id 46
    label "voice"
  ]
  node [
    id 47
    label "forma"
  ]
  node [
    id 48
    label "internet"
  ]
  node [
    id 49
    label "skr&#281;ci&#263;"
  ]
  node [
    id 50
    label "kartka"
  ]
  node [
    id 51
    label "orientowa&#263;"
  ]
  node [
    id 52
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 53
    label "powierzchnia"
  ]
  node [
    id 54
    label "plik"
  ]
  node [
    id 55
    label "bok"
  ]
  node [
    id 56
    label "pagina"
  ]
  node [
    id 57
    label "orientowanie"
  ]
  node [
    id 58
    label "fragment"
  ]
  node [
    id 59
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 60
    label "s&#261;d"
  ]
  node [
    id 61
    label "skr&#281;ca&#263;"
  ]
  node [
    id 62
    label "g&#243;ra"
  ]
  node [
    id 63
    label "serwis_internetowy"
  ]
  node [
    id 64
    label "orientacja"
  ]
  node [
    id 65
    label "linia"
  ]
  node [
    id 66
    label "skr&#281;cenie"
  ]
  node [
    id 67
    label "layout"
  ]
  node [
    id 68
    label "zorientowa&#263;"
  ]
  node [
    id 69
    label "zorientowanie"
  ]
  node [
    id 70
    label "obiekt"
  ]
  node [
    id 71
    label "podmiot"
  ]
  node [
    id 72
    label "ty&#322;"
  ]
  node [
    id 73
    label "logowanie"
  ]
  node [
    id 74
    label "adres_internetowy"
  ]
  node [
    id 75
    label "uj&#281;cie"
  ]
  node [
    id 76
    label "prz&#243;d"
  ]
  node [
    id 77
    label "posta&#263;"
  ]
  node [
    id 78
    label "wyliczanka"
  ]
  node [
    id 79
    label "czynno&#347;&#263;"
  ]
  node [
    id 80
    label "catalog"
  ]
  node [
    id 81
    label "stock"
  ]
  node [
    id 82
    label "akt"
  ]
  node [
    id 83
    label "figurowa&#263;"
  ]
  node [
    id 84
    label "zbi&#243;r"
  ]
  node [
    id 85
    label "book"
  ]
  node [
    id 86
    label "pozycja"
  ]
  node [
    id 87
    label "tekst"
  ]
  node [
    id 88
    label "sumariusz"
  ]
  node [
    id 89
    label "dostarczenie"
  ]
  node [
    id 90
    label "pass"
  ]
  node [
    id 91
    label "give"
  ]
  node [
    id 92
    label "rendition"
  ]
  node [
    id 93
    label "zap&#322;acenie"
  ]
  node [
    id 94
    label "zadanie"
  ]
  node [
    id 95
    label "wyrzekni&#281;cie_si&#281;"
  ]
  node [
    id 96
    label "cios"
  ]
  node [
    id 97
    label "eating"
  ]
  node [
    id 98
    label "uderzanie"
  ]
  node [
    id 99
    label "powierzenie"
  ]
  node [
    id 100
    label "posi&#322;ek"
  ]
  node [
    id 101
    label "zrobienie"
  ]
  node [
    id 102
    label "udost&#281;pnienie"
  ]
  node [
    id 103
    label "wymienienie_si&#281;"
  ]
  node [
    id 104
    label "jedzenie"
  ]
  node [
    id 105
    label "dostanie"
  ]
  node [
    id 106
    label "wyposa&#380;enie"
  ]
  node [
    id 107
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 108
    label "potrawa"
  ]
  node [
    id 109
    label "karta"
  ]
  node [
    id 110
    label "hand"
  ]
  node [
    id 111
    label "wyst&#261;pienie"
  ]
  node [
    id 112
    label "odst&#261;pienie"
  ]
  node [
    id 113
    label "wyposa&#380;anie"
  ]
  node [
    id 114
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 115
    label "pobicie"
  ]
  node [
    id 116
    label "dodanie"
  ]
  node [
    id 117
    label "allow"
  ]
  node [
    id 118
    label "obiecanie"
  ]
  node [
    id 119
    label "przeznaczenie"
  ]
  node [
    id 120
    label "uderzenie"
  ]
  node [
    id 121
    label "przekazanie"
  ]
  node [
    id 122
    label "uprawianie_seksu"
  ]
  node [
    id 123
    label "coup"
  ]
  node [
    id 124
    label "urz&#261;dzenie"
  ]
  node [
    id 125
    label "partnerka"
  ]
  node [
    id 126
    label "p&#322;owy"
  ]
  node [
    id 127
    label "zielono"
  ]
  node [
    id 128
    label "blady"
  ]
  node [
    id 129
    label "pochmurno"
  ]
  node [
    id 130
    label "szaro"
  ]
  node [
    id 131
    label "chmurnienie"
  ]
  node [
    id 132
    label "zwyczajny"
  ]
  node [
    id 133
    label "zwyk&#322;y"
  ]
  node [
    id 134
    label "szarzenie"
  ]
  node [
    id 135
    label "niezabawny"
  ]
  node [
    id 136
    label "brzydki"
  ]
  node [
    id 137
    label "nieciekawy"
  ]
  node [
    id 138
    label "oboj&#281;tny"
  ]
  node [
    id 139
    label "poszarzenie"
  ]
  node [
    id 140
    label "bezbarwnie"
  ]
  node [
    id 141
    label "ch&#322;odny"
  ]
  node [
    id 142
    label "srebrny"
  ]
  node [
    id 143
    label "spochmurnienie"
  ]
  node [
    id 144
    label "kopertowy"
  ]
  node [
    id 145
    label "miejsce"
  ]
  node [
    id 146
    label "box"
  ]
  node [
    id 147
    label "torebka"
  ]
  node [
    id 148
    label "przekupi&#263;"
  ]
  node [
    id 149
    label "bribery"
  ]
  node [
    id 150
    label "parkowa&#263;"
  ]
  node [
    id 151
    label "zegarek"
  ]
  node [
    id 152
    label "p&#322;yta_winylowa"
  ]
  node [
    id 153
    label "przekupywa&#263;"
  ]
  node [
    id 154
    label "przekupywanie"
  ]
  node [
    id 155
    label "przekupienie"
  ]
  node [
    id 156
    label "ma&#322;y"
  ]
  node [
    id 157
    label "kwota"
  ]
  node [
    id 158
    label "obudowa"
  ]
  node [
    id 159
    label "opakowanie"
  ]
  node [
    id 160
    label "poszwa"
  ]
  node [
    id 161
    label "jednakowo"
  ]
  node [
    id 162
    label "spokojnie"
  ]
  node [
    id 163
    label "zgodny"
  ]
  node [
    id 164
    label "dobrze"
  ]
  node [
    id 165
    label "zbie&#380;nie"
  ]
  node [
    id 166
    label "czyn"
  ]
  node [
    id 167
    label "contract"
  ]
  node [
    id 168
    label "gestia_transportowa"
  ]
  node [
    id 169
    label "zawrze&#263;"
  ]
  node [
    id 170
    label "klauzula"
  ]
  node [
    id 171
    label "porozumienie"
  ]
  node [
    id 172
    label "warunek"
  ]
  node [
    id 173
    label "zawarcie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 10
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 24
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 10
    target 39
  ]
  edge [
    source 10
    target 105
  ]
  edge [
    source 10
    target 106
  ]
  edge [
    source 10
    target 107
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 10
    target 110
  ]
  edge [
    source 10
    target 111
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 10
    target 121
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 125
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 126
  ]
  edge [
    source 14
    target 127
  ]
  edge [
    source 14
    target 128
  ]
  edge [
    source 14
    target 129
  ]
  edge [
    source 14
    target 130
  ]
  edge [
    source 14
    target 131
  ]
  edge [
    source 14
    target 132
  ]
  edge [
    source 14
    target 133
  ]
  edge [
    source 14
    target 134
  ]
  edge [
    source 14
    target 135
  ]
  edge [
    source 14
    target 136
  ]
  edge [
    source 14
    target 137
  ]
  edge [
    source 14
    target 138
  ]
  edge [
    source 14
    target 139
  ]
  edge [
    source 14
    target 140
  ]
  edge [
    source 14
    target 141
  ]
  edge [
    source 14
    target 142
  ]
  edge [
    source 14
    target 143
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 144
  ]
  edge [
    source 15
    target 145
  ]
  edge [
    source 15
    target 146
  ]
  edge [
    source 15
    target 147
  ]
  edge [
    source 15
    target 148
  ]
  edge [
    source 15
    target 149
  ]
  edge [
    source 15
    target 150
  ]
  edge [
    source 15
    target 151
  ]
  edge [
    source 15
    target 152
  ]
  edge [
    source 15
    target 153
  ]
  edge [
    source 15
    target 154
  ]
  edge [
    source 15
    target 155
  ]
  edge [
    source 15
    target 156
  ]
  edge [
    source 15
    target 157
  ]
  edge [
    source 15
    target 158
  ]
  edge [
    source 15
    target 159
  ]
  edge [
    source 15
    target 160
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 161
  ]
  edge [
    source 16
    target 162
  ]
  edge [
    source 16
    target 163
  ]
  edge [
    source 16
    target 164
  ]
  edge [
    source 16
    target 165
  ]
  edge [
    source 17
    target 166
  ]
  edge [
    source 17
    target 167
  ]
  edge [
    source 17
    target 168
  ]
  edge [
    source 17
    target 169
  ]
  edge [
    source 17
    target 170
  ]
  edge [
    source 17
    target 171
  ]
  edge [
    source 17
    target 172
  ]
  edge [
    source 17
    target 173
  ]
]
