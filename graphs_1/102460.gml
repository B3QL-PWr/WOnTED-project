graph [
  maxDegree 39
  minDegree 1
  meanDegree 2.1
  density 0.010552763819095477
  graphCliqueNumber 3
  node [
    id 0
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 1
    label "rocznik"
    origin "text"
  ]
  node [
    id 2
    label "odby&#263;"
    origin "text"
  ]
  node [
    id 3
    label "si&#281;"
    origin "text"
  ]
  node [
    id 4
    label "otwarcie"
    origin "text"
  ]
  node [
    id 5
    label "warsztat"
    origin "text"
  ]
  node [
    id 6
    label "terapi"
    origin "text"
  ]
  node [
    id 7
    label "zaj&#281;ciowy"
    origin "text"
  ]
  node [
    id 8
    label "ozorkowie"
    origin "text"
  ]
  node [
    id 9
    label "ula"
    origin "text"
  ]
  node [
    id 10
    label "ko&#347;cuszki"
    origin "text"
  ]
  node [
    id 11
    label "godz"
    origin "text"
  ]
  node [
    id 12
    label "boisko"
    origin "text"
  ]
  node [
    id 13
    label "sportowy"
    origin "text"
  ]
  node [
    id 14
    label "&#322;&#281;czycki"
    origin "text"
  ]
  node [
    id 15
    label "piknik"
    origin "text"
  ]
  node [
    id 16
    label "integracyjny"
    origin "text"
  ]
  node [
    id 17
    label "serdecznie"
    origin "text"
  ]
  node [
    id 18
    label "zaprasza&#263;"
    origin "text"
  ]
  node [
    id 19
    label "wtz"
    origin "text"
  ]
  node [
    id 20
    label "wzi&#261;&#263;"
    origin "text"
  ]
  node [
    id 21
    label "udzia&#322;"
    origin "text"
  ]
  node [
    id 22
    label "w&#322;adza"
    origin "text"
  ]
  node [
    id 23
    label "miejski"
    origin "text"
  ]
  node [
    id 24
    label "powiatowy"
    origin "text"
  ]
  node [
    id 25
    label "zarz&#261;d"
    origin "text"
  ]
  node [
    id 26
    label "oddzia&#322;"
    origin "text"
  ]
  node [
    id 27
    label "&#322;&#243;dzkie"
    origin "text"
  ]
  node [
    id 28
    label "pslcnp"
    origin "text"
  ]
  node [
    id 29
    label "s&#322;o&#324;ce"
  ]
  node [
    id 30
    label "czynienie_si&#281;"
  ]
  node [
    id 31
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 32
    label "czas"
  ]
  node [
    id 33
    label "long_time"
  ]
  node [
    id 34
    label "przedpo&#322;udnie"
  ]
  node [
    id 35
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 36
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 37
    label "tydzie&#324;"
  ]
  node [
    id 38
    label "godzina"
  ]
  node [
    id 39
    label "t&#322;usty_czwartek"
  ]
  node [
    id 40
    label "wsta&#263;"
  ]
  node [
    id 41
    label "day"
  ]
  node [
    id 42
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 43
    label "przedwiecz&#243;r"
  ]
  node [
    id 44
    label "Sylwester"
  ]
  node [
    id 45
    label "po&#322;udnie"
  ]
  node [
    id 46
    label "wzej&#347;cie"
  ]
  node [
    id 47
    label "podwiecz&#243;r"
  ]
  node [
    id 48
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 49
    label "rano"
  ]
  node [
    id 50
    label "termin"
  ]
  node [
    id 51
    label "ranek"
  ]
  node [
    id 52
    label "doba"
  ]
  node [
    id 53
    label "wiecz&#243;r"
  ]
  node [
    id 54
    label "walentynki"
  ]
  node [
    id 55
    label "popo&#322;udnie"
  ]
  node [
    id 56
    label "noc"
  ]
  node [
    id 57
    label "wstanie"
  ]
  node [
    id 58
    label "formacja"
  ]
  node [
    id 59
    label "kronika"
  ]
  node [
    id 60
    label "czasopismo"
  ]
  node [
    id 61
    label "yearbook"
  ]
  node [
    id 62
    label "reserve"
  ]
  node [
    id 63
    label "przej&#347;&#263;"
  ]
  node [
    id 64
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 65
    label "jawny"
  ]
  node [
    id 66
    label "zdecydowanie"
  ]
  node [
    id 67
    label "czynno&#347;&#263;"
  ]
  node [
    id 68
    label "publicznie"
  ]
  node [
    id 69
    label "bezpo&#347;rednio"
  ]
  node [
    id 70
    label "udost&#281;pnienie"
  ]
  node [
    id 71
    label "granie"
  ]
  node [
    id 72
    label "gra&#263;"
  ]
  node [
    id 73
    label "ewidentnie"
  ]
  node [
    id 74
    label "jawnie"
  ]
  node [
    id 75
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 76
    label "rozpocz&#281;cie"
  ]
  node [
    id 77
    label "otwarty"
  ]
  node [
    id 78
    label "opening"
  ]
  node [
    id 79
    label "jawno"
  ]
  node [
    id 80
    label "miejsce"
  ]
  node [
    id 81
    label "sprawno&#347;&#263;"
  ]
  node [
    id 82
    label "spotkanie"
  ]
  node [
    id 83
    label "wyposa&#380;enie"
  ]
  node [
    id 84
    label "st&#243;&#322;_roboczy"
  ]
  node [
    id 85
    label "pracownia"
  ]
  node [
    id 86
    label "ziemia"
  ]
  node [
    id 87
    label "pole"
  ]
  node [
    id 88
    label "linia"
  ]
  node [
    id 89
    label "obiekt"
  ]
  node [
    id 90
    label "skrzyd&#322;o"
  ]
  node [
    id 91
    label "aut"
  ]
  node [
    id 92
    label "bojo"
  ]
  node [
    id 93
    label "bojowisko"
  ]
  node [
    id 94
    label "budowla"
  ]
  node [
    id 95
    label "specjalny"
  ]
  node [
    id 96
    label "na_sportowo"
  ]
  node [
    id 97
    label "uczciwy"
  ]
  node [
    id 98
    label "wygodny"
  ]
  node [
    id 99
    label "pe&#322;ny"
  ]
  node [
    id 100
    label "sportowo"
  ]
  node [
    id 101
    label "zestaw"
  ]
  node [
    id 102
    label "jedzenie"
  ]
  node [
    id 103
    label "impreza"
  ]
  node [
    id 104
    label "kibic"
  ]
  node [
    id 105
    label "posi&#322;ek"
  ]
  node [
    id 106
    label "siarczy&#347;cie"
  ]
  node [
    id 107
    label "serdeczny"
  ]
  node [
    id 108
    label "szczerze"
  ]
  node [
    id 109
    label "mi&#322;o"
  ]
  node [
    id 110
    label "invite"
  ]
  node [
    id 111
    label "ask"
  ]
  node [
    id 112
    label "oferowa&#263;"
  ]
  node [
    id 113
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 114
    label "wej&#347;&#263;"
  ]
  node [
    id 115
    label "get"
  ]
  node [
    id 116
    label "wzi&#281;cie"
  ]
  node [
    id 117
    label "wyrucha&#263;"
  ]
  node [
    id 118
    label "uciec"
  ]
  node [
    id 119
    label "ruszy&#263;"
  ]
  node [
    id 120
    label "wygra&#263;"
  ]
  node [
    id 121
    label "obj&#261;&#263;"
  ]
  node [
    id 122
    label "zacz&#261;&#263;"
  ]
  node [
    id 123
    label "wyciupcia&#263;"
  ]
  node [
    id 124
    label "World_Health_Organization"
  ]
  node [
    id 125
    label "skorzysta&#263;"
  ]
  node [
    id 126
    label "pokona&#263;"
  ]
  node [
    id 127
    label "poczyta&#263;"
  ]
  node [
    id 128
    label "poruszy&#263;"
  ]
  node [
    id 129
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 130
    label "take"
  ]
  node [
    id 131
    label "aim"
  ]
  node [
    id 132
    label "arise"
  ]
  node [
    id 133
    label "u&#380;y&#263;"
  ]
  node [
    id 134
    label "zaatakowa&#263;"
  ]
  node [
    id 135
    label "receive"
  ]
  node [
    id 136
    label "uda&#263;_si&#281;"
  ]
  node [
    id 137
    label "dosta&#263;"
  ]
  node [
    id 138
    label "otrzyma&#263;"
  ]
  node [
    id 139
    label "obskoczy&#263;"
  ]
  node [
    id 140
    label "zaopatrzy&#263;_si&#281;"
  ]
  node [
    id 141
    label "zrobi&#263;"
  ]
  node [
    id 142
    label "bra&#263;"
  ]
  node [
    id 143
    label "nakaza&#263;"
  ]
  node [
    id 144
    label "chwyci&#263;"
  ]
  node [
    id 145
    label "przyj&#261;&#263;"
  ]
  node [
    id 146
    label "seize"
  ]
  node [
    id 147
    label "odziedziczy&#263;"
  ]
  node [
    id 148
    label "withdraw"
  ]
  node [
    id 149
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 150
    label "wch&#322;on&#261;&#263;"
  ]
  node [
    id 151
    label "obecno&#347;&#263;"
  ]
  node [
    id 152
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 153
    label "kwota"
  ]
  node [
    id 154
    label "ilo&#347;&#263;"
  ]
  node [
    id 155
    label "cz&#322;owiek"
  ]
  node [
    id 156
    label "panowanie"
  ]
  node [
    id 157
    label "Kreml"
  ]
  node [
    id 158
    label "wydolno&#347;&#263;"
  ]
  node [
    id 159
    label "grupa"
  ]
  node [
    id 160
    label "prawo"
  ]
  node [
    id 161
    label "rz&#261;dzenie"
  ]
  node [
    id 162
    label "rz&#261;d"
  ]
  node [
    id 163
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 164
    label "struktura"
  ]
  node [
    id 165
    label "miejsko"
  ]
  node [
    id 166
    label "miastowy"
  ]
  node [
    id 167
    label "typowy"
  ]
  node [
    id 168
    label "publiczny"
  ]
  node [
    id 169
    label "administration"
  ]
  node [
    id 170
    label "organizacja"
  ]
  node [
    id 171
    label "administracja"
  ]
  node [
    id 172
    label "biuro"
  ]
  node [
    id 173
    label "organ_sp&#243;&#322;ki"
  ]
  node [
    id 174
    label "kierownictwo"
  ]
  node [
    id 175
    label "Bruksela"
  ]
  node [
    id 176
    label "siedziba"
  ]
  node [
    id 177
    label "centrala"
  ]
  node [
    id 178
    label "whole"
  ]
  node [
    id 179
    label "jednostka"
  ]
  node [
    id 180
    label "jednostka_geologiczna"
  ]
  node [
    id 181
    label "system"
  ]
  node [
    id 182
    label "poziom"
  ]
  node [
    id 183
    label "agencja"
  ]
  node [
    id 184
    label "dogger"
  ]
  node [
    id 185
    label "pi&#281;tro"
  ]
  node [
    id 186
    label "filia"
  ]
  node [
    id 187
    label "dzia&#322;"
  ]
  node [
    id 188
    label "promocja"
  ]
  node [
    id 189
    label "zesp&#243;&#322;"
  ]
  node [
    id 190
    label "kurs"
  ]
  node [
    id 191
    label "przegrupowywa&#263;_si&#281;"
  ]
  node [
    id 192
    label "wojsko"
  ]
  node [
    id 193
    label "bank"
  ]
  node [
    id 194
    label "lias"
  ]
  node [
    id 195
    label "szpital"
  ]
  node [
    id 196
    label "przegrupowywanie_si&#281;"
  ]
  node [
    id 197
    label "malm"
  ]
  node [
    id 198
    label "ajencja"
  ]
  node [
    id 199
    label "klasa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 13
  ]
  edge [
    source 9
    target 14
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 86
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 12
    target 89
  ]
  edge [
    source 12
    target 90
  ]
  edge [
    source 12
    target 91
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 12
    target 94
  ]
  edge [
    source 13
    target 95
  ]
  edge [
    source 13
    target 96
  ]
  edge [
    source 13
    target 97
  ]
  edge [
    source 13
    target 98
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 101
  ]
  edge [
    source 15
    target 102
  ]
  edge [
    source 15
    target 103
  ]
  edge [
    source 15
    target 104
  ]
  edge [
    source 15
    target 105
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 106
  ]
  edge [
    source 17
    target 107
  ]
  edge [
    source 17
    target 108
  ]
  edge [
    source 17
    target 109
  ]
  edge [
    source 18
    target 110
  ]
  edge [
    source 18
    target 111
  ]
  edge [
    source 18
    target 112
  ]
  edge [
    source 18
    target 113
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 114
  ]
  edge [
    source 20
    target 115
  ]
  edge [
    source 20
    target 116
  ]
  edge [
    source 20
    target 117
  ]
  edge [
    source 20
    target 118
  ]
  edge [
    source 20
    target 119
  ]
  edge [
    source 20
    target 120
  ]
  edge [
    source 20
    target 121
  ]
  edge [
    source 20
    target 122
  ]
  edge [
    source 20
    target 123
  ]
  edge [
    source 20
    target 124
  ]
  edge [
    source 20
    target 125
  ]
  edge [
    source 20
    target 126
  ]
  edge [
    source 20
    target 127
  ]
  edge [
    source 20
    target 128
  ]
  edge [
    source 20
    target 129
  ]
  edge [
    source 20
    target 130
  ]
  edge [
    source 20
    target 131
  ]
  edge [
    source 20
    target 132
  ]
  edge [
    source 20
    target 133
  ]
  edge [
    source 20
    target 134
  ]
  edge [
    source 20
    target 135
  ]
  edge [
    source 20
    target 136
  ]
  edge [
    source 20
    target 137
  ]
  edge [
    source 20
    target 138
  ]
  edge [
    source 20
    target 139
  ]
  edge [
    source 20
    target 140
  ]
  edge [
    source 20
    target 141
  ]
  edge [
    source 20
    target 142
  ]
  edge [
    source 20
    target 143
  ]
  edge [
    source 20
    target 144
  ]
  edge [
    source 20
    target 145
  ]
  edge [
    source 20
    target 146
  ]
  edge [
    source 20
    target 147
  ]
  edge [
    source 20
    target 148
  ]
  edge [
    source 20
    target 149
  ]
  edge [
    source 20
    target 150
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 151
  ]
  edge [
    source 21
    target 152
  ]
  edge [
    source 21
    target 153
  ]
  edge [
    source 21
    target 154
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 155
  ]
  edge [
    source 22
    target 156
  ]
  edge [
    source 22
    target 157
  ]
  edge [
    source 22
    target 158
  ]
  edge [
    source 22
    target 159
  ]
  edge [
    source 22
    target 160
  ]
  edge [
    source 22
    target 161
  ]
  edge [
    source 22
    target 162
  ]
  edge [
    source 22
    target 163
  ]
  edge [
    source 22
    target 164
  ]
  edge [
    source 22
    target 25
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 165
  ]
  edge [
    source 23
    target 166
  ]
  edge [
    source 23
    target 167
  ]
  edge [
    source 23
    target 168
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 169
  ]
  edge [
    source 25
    target 67
  ]
  edge [
    source 25
    target 170
  ]
  edge [
    source 25
    target 171
  ]
  edge [
    source 25
    target 172
  ]
  edge [
    source 25
    target 173
  ]
  edge [
    source 25
    target 174
  ]
  edge [
    source 25
    target 175
  ]
  edge [
    source 25
    target 176
  ]
  edge [
    source 25
    target 177
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 178
  ]
  edge [
    source 26
    target 179
  ]
  edge [
    source 26
    target 180
  ]
  edge [
    source 26
    target 181
  ]
  edge [
    source 26
    target 182
  ]
  edge [
    source 26
    target 183
  ]
  edge [
    source 26
    target 184
  ]
  edge [
    source 26
    target 58
  ]
  edge [
    source 26
    target 185
  ]
  edge [
    source 26
    target 186
  ]
  edge [
    source 26
    target 187
  ]
  edge [
    source 26
    target 188
  ]
  edge [
    source 26
    target 189
  ]
  edge [
    source 26
    target 190
  ]
  edge [
    source 26
    target 191
  ]
  edge [
    source 26
    target 192
  ]
  edge [
    source 26
    target 176
  ]
  edge [
    source 26
    target 193
  ]
  edge [
    source 26
    target 194
  ]
  edge [
    source 26
    target 195
  ]
  edge [
    source 26
    target 196
  ]
  edge [
    source 26
    target 197
  ]
  edge [
    source 26
    target 198
  ]
  edge [
    source 26
    target 199
  ]
  edge [
    source 27
    target 28
  ]
]
