graph [
  maxDegree 51
  minDegree 1
  meanDegree 2.8912579957356077
  density 0.00617790170028976
  graphCliqueNumber 15
  node [
    id 0
    label "rozpoznanie"
    origin "text"
  ]
  node [
    id 1
    label "udzia&#322;"
    origin "text"
  ]
  node [
    id 2
    label "sejm"
    origin "text"
  ]
  node [
    id 3
    label "prokurator"
    origin "text"
  ]
  node [
    id 4
    label "generalny"
    origin "text"
  ]
  node [
    id 5
    label "rozprawa"
    origin "text"
  ]
  node [
    id 6
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 7
    label "wrzesie&#324;"
    origin "text"
  ]
  node [
    id 8
    label "rocznik"
    origin "text"
  ]
  node [
    id 9
    label "pytanie"
    origin "text"
  ]
  node [
    id 10
    label "prawny"
    origin "text"
  ]
  node [
    id 11
    label "s&#261;d"
    origin "text"
  ]
  node [
    id 12
    label "okr&#281;gowy"
    origin "text"
  ]
  node [
    id 13
    label "koszalin"
    origin "text"
  ]
  node [
    id 14
    label "stwierdzenie"
    origin "text"
  ]
  node [
    id 15
    label "art"
    origin "text"
  ]
  node [
    id 16
    label "usta"
    origin "text"
  ]
  node [
    id 17
    label "pkt"
    origin "text"
  ]
  node [
    id 18
    label "ustawa"
    origin "text"
  ]
  node [
    id 19
    label "stycze&#324;"
    origin "text"
  ]
  node [
    id 20
    label "kart"
    origin "text"
  ]
  node [
    id 21
    label "nauczyciel"
    origin "text"
  ]
  node [
    id 22
    label "dziennik"
    origin "text"
  ]
  node [
    id 23
    label "poz"
    origin "text"
  ]
  node [
    id 24
    label "zmar&#322;"
    origin "text"
  ]
  node [
    id 25
    label "brzmienie"
    origin "text"
  ]
  node [
    id 26
    label "nada&#263;"
    origin "text"
  ]
  node [
    id 27
    label "przez"
    origin "text"
  ]
  node [
    id 28
    label "lit"
    origin "text"
  ]
  node [
    id 29
    label "lipiec"
    origin "text"
  ]
  node [
    id 30
    label "zmiana"
    origin "text"
  ]
  node [
    id 31
    label "niekt&#243;ry"
    origin "text"
  ]
  node [
    id 32
    label "inny"
    origin "text"
  ]
  node [
    id 33
    label "zakres"
    origin "text"
  ]
  node [
    id 34
    label "wy&#322;&#261;cza&#263;"
    origin "text"
  ]
  node [
    id 35
    label "prawo"
    origin "text"
  ]
  node [
    id 36
    label "zatrudniony"
    origin "text"
  ]
  node [
    id 37
    label "plac&#243;wka"
    origin "text"
  ]
  node [
    id 38
    label "niepubliczny"
    origin "text"
  ]
  node [
    id 39
    label "wymiar"
    origin "text"
  ]
  node [
    id 40
    label "ma&#322;o"
    origin "text"
  ]
  node [
    id 41
    label "etat"
    origin "text"
  ]
  node [
    id 42
    label "obowi&#261;zuj&#261;cy"
    origin "text"
  ]
  node [
    id 43
    label "zaj&#281;cia"
    origin "text"
  ]
  node [
    id 44
    label "grupa"
    origin "text"
  ]
  node [
    id 45
    label "uprawniony"
    origin "text"
  ]
  node [
    id 46
    label "wczesny"
    origin "text"
  ]
  node [
    id 47
    label "emerytura"
    origin "text"
  ]
  node [
    id 48
    label "bez"
    origin "text"
  ]
  node [
    id 49
    label "wzgl&#261;d"
    origin "text"
  ]
  node [
    id 50
    label "wiek"
    origin "text"
  ]
  node [
    id 51
    label "by&#263;"
    origin "text"
  ]
  node [
    id 52
    label "zgodny"
    origin "text"
  ]
  node [
    id 53
    label "cz&#281;&#347;&#263;"
    origin "text"
  ]
  node [
    id 54
    label "wyra&#380;a&#263;"
    origin "text"
  ]
  node [
    id 55
    label "zasada"
    origin "text"
  ]
  node [
    id 56
    label "sprawiedliwo&#347;&#263;"
    origin "text"
  ]
  node [
    id 57
    label "spo&#322;eczny"
    origin "text"
  ]
  node [
    id 58
    label "konstytucja"
    origin "text"
  ]
  node [
    id 59
    label "recce"
  ]
  node [
    id 60
    label "designation"
  ]
  node [
    id 61
    label "rozr&#243;&#380;nienie"
  ]
  node [
    id 62
    label "rozwa&#380;enie"
  ]
  node [
    id 63
    label "badanie"
  ]
  node [
    id 64
    label "obecno&#347;&#263;"
  ]
  node [
    id 65
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 66
    label "kwota"
  ]
  node [
    id 67
    label "ilo&#347;&#263;"
  ]
  node [
    id 68
    label "parlament"
  ]
  node [
    id 69
    label "obrady"
  ]
  node [
    id 70
    label "lewica"
  ]
  node [
    id 71
    label "zgromadzenie"
  ]
  node [
    id 72
    label "prawica"
  ]
  node [
    id 73
    label "centrum"
  ]
  node [
    id 74
    label "izba_ni&#380;sza"
  ]
  node [
    id 75
    label "siedziba"
  ]
  node [
    id 76
    label "parliament"
  ]
  node [
    id 77
    label "pracownik"
  ]
  node [
    id 78
    label "prawnik"
  ]
  node [
    id 79
    label "oskar&#380;yciel_publiczny"
  ]
  node [
    id 80
    label "nadrz&#281;dny"
  ]
  node [
    id 81
    label "podstawowy"
  ]
  node [
    id 82
    label "ca&#322;o&#347;ciowy"
  ]
  node [
    id 83
    label "og&#243;lnie"
  ]
  node [
    id 84
    label "generalnie"
  ]
  node [
    id 85
    label "porz&#261;dny"
  ]
  node [
    id 86
    label "zasadniczy"
  ]
  node [
    id 87
    label "zwierzchni"
  ]
  node [
    id 88
    label "opracowanie"
  ]
  node [
    id 89
    label "obja&#347;nienie"
  ]
  node [
    id 90
    label "rozumowanie"
  ]
  node [
    id 91
    label "proces"
  ]
  node [
    id 92
    label "s&#261;dzenie"
  ]
  node [
    id 93
    label "tekst"
  ]
  node [
    id 94
    label "cytat"
  ]
  node [
    id 95
    label "s&#322;o&#324;ce"
  ]
  node [
    id 96
    label "czynienie_si&#281;"
  ]
  node [
    id 97
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 98
    label "czas"
  ]
  node [
    id 99
    label "long_time"
  ]
  node [
    id 100
    label "przedpo&#322;udnie"
  ]
  node [
    id 101
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 102
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 103
    label "tydzie&#324;"
  ]
  node [
    id 104
    label "godzina"
  ]
  node [
    id 105
    label "t&#322;usty_czwartek"
  ]
  node [
    id 106
    label "wsta&#263;"
  ]
  node [
    id 107
    label "day"
  ]
  node [
    id 108
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 109
    label "przedwiecz&#243;r"
  ]
  node [
    id 110
    label "Sylwester"
  ]
  node [
    id 111
    label "po&#322;udnie"
  ]
  node [
    id 112
    label "wzej&#347;cie"
  ]
  node [
    id 113
    label "podwiecz&#243;r"
  ]
  node [
    id 114
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 115
    label "rano"
  ]
  node [
    id 116
    label "termin"
  ]
  node [
    id 117
    label "ranek"
  ]
  node [
    id 118
    label "doba"
  ]
  node [
    id 119
    label "wiecz&#243;r"
  ]
  node [
    id 120
    label "walentynki"
  ]
  node [
    id 121
    label "popo&#322;udnie"
  ]
  node [
    id 122
    label "noc"
  ]
  node [
    id 123
    label "wstanie"
  ]
  node [
    id 124
    label "miesi&#261;c"
  ]
  node [
    id 125
    label "formacja"
  ]
  node [
    id 126
    label "kronika"
  ]
  node [
    id 127
    label "czasopismo"
  ]
  node [
    id 128
    label "yearbook"
  ]
  node [
    id 129
    label "sprawa"
  ]
  node [
    id 130
    label "zadanie"
  ]
  node [
    id 131
    label "wypowied&#378;"
  ]
  node [
    id 132
    label "problemat"
  ]
  node [
    id 133
    label "rozpytywanie"
  ]
  node [
    id 134
    label "sprawdzian"
  ]
  node [
    id 135
    label "przes&#322;uchiwanie"
  ]
  node [
    id 136
    label "wypytanie"
  ]
  node [
    id 137
    label "zwracanie_si&#281;"
  ]
  node [
    id 138
    label "wypowiedzenie"
  ]
  node [
    id 139
    label "wywo&#322;ywanie"
  ]
  node [
    id 140
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 141
    label "problematyka"
  ]
  node [
    id 142
    label "question"
  ]
  node [
    id 143
    label "sprawdzanie"
  ]
  node [
    id 144
    label "odpowiadanie"
  ]
  node [
    id 145
    label "survey"
  ]
  node [
    id 146
    label "odpowiada&#263;"
  ]
  node [
    id 147
    label "egzaminowanie"
  ]
  node [
    id 148
    label "prawniczo"
  ]
  node [
    id 149
    label "prawnie"
  ]
  node [
    id 150
    label "konstytucyjnoprawny"
  ]
  node [
    id 151
    label "legalny"
  ]
  node [
    id 152
    label "jurydyczny"
  ]
  node [
    id 153
    label "procesowicz"
  ]
  node [
    id 154
    label "pods&#261;dny"
  ]
  node [
    id 155
    label "podejrzany"
  ]
  node [
    id 156
    label "broni&#263;"
  ]
  node [
    id 157
    label "bronienie"
  ]
  node [
    id 158
    label "system"
  ]
  node [
    id 159
    label "my&#347;l"
  ]
  node [
    id 160
    label "wytw&#243;r"
  ]
  node [
    id 161
    label "urz&#261;d"
  ]
  node [
    id 162
    label "konektyw"
  ]
  node [
    id 163
    label "court"
  ]
  node [
    id 164
    label "obrona"
  ]
  node [
    id 165
    label "s&#261;downictwo"
  ]
  node [
    id 166
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 167
    label "forum"
  ]
  node [
    id 168
    label "zesp&#243;&#322;"
  ]
  node [
    id 169
    label "post&#281;powanie"
  ]
  node [
    id 170
    label "skazany"
  ]
  node [
    id 171
    label "wydarzenie"
  ]
  node [
    id 172
    label "&#347;wiadek"
  ]
  node [
    id 173
    label "antylogizm"
  ]
  node [
    id 174
    label "strona"
  ]
  node [
    id 175
    label "oskar&#380;yciel"
  ]
  node [
    id 176
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 177
    label "biuro"
  ]
  node [
    id 178
    label "instytucja"
  ]
  node [
    id 179
    label "ustalenie"
  ]
  node [
    id 180
    label "claim"
  ]
  node [
    id 181
    label "statement"
  ]
  node [
    id 182
    label "oznajmienie"
  ]
  node [
    id 183
    label "warga_dolna"
  ]
  node [
    id 184
    label "ssa&#263;"
  ]
  node [
    id 185
    label "zaci&#261;&#263;"
  ]
  node [
    id 186
    label "ryjek"
  ]
  node [
    id 187
    label "twarz"
  ]
  node [
    id 188
    label "dzi&#243;b"
  ]
  node [
    id 189
    label "&#322;uk_Kupidyna"
  ]
  node [
    id 190
    label "ssanie"
  ]
  node [
    id 191
    label "zaci&#281;cie"
  ]
  node [
    id 192
    label "jadaczka"
  ]
  node [
    id 193
    label "zacinanie"
  ]
  node [
    id 194
    label "organ"
  ]
  node [
    id 195
    label "jama_ustna"
  ]
  node [
    id 196
    label "otw&#243;r_g&#281;bowy"
  ]
  node [
    id 197
    label "warga_g&#243;rna"
  ]
  node [
    id 198
    label "zacina&#263;"
  ]
  node [
    id 199
    label "Karta_Nauczyciela"
  ]
  node [
    id 200
    label "marc&#243;wka"
  ]
  node [
    id 201
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 202
    label "akt"
  ]
  node [
    id 203
    label "przej&#347;&#263;"
  ]
  node [
    id 204
    label "charter"
  ]
  node [
    id 205
    label "przej&#347;cie"
  ]
  node [
    id 206
    label "Nowy_Rok"
  ]
  node [
    id 207
    label "wy&#347;cig&#243;wka"
  ]
  node [
    id 208
    label "profesor"
  ]
  node [
    id 209
    label "kszta&#322;ciciel"
  ]
  node [
    id 210
    label "szkolnik"
  ]
  node [
    id 211
    label "preceptor"
  ]
  node [
    id 212
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 213
    label "pedagog"
  ]
  node [
    id 214
    label "popularyzator"
  ]
  node [
    id 215
    label "belfer"
  ]
  node [
    id 216
    label "spis"
  ]
  node [
    id 217
    label "sheet"
  ]
  node [
    id 218
    label "gazeta"
  ]
  node [
    id 219
    label "diariusz"
  ]
  node [
    id 220
    label "pami&#281;tnik"
  ]
  node [
    id 221
    label "journal"
  ]
  node [
    id 222
    label "ksi&#281;ga"
  ]
  node [
    id 223
    label "program_informacyjny"
  ]
  node [
    id 224
    label "wyra&#380;anie"
  ]
  node [
    id 225
    label "cecha"
  ]
  node [
    id 226
    label "sound"
  ]
  node [
    id 227
    label "tone"
  ]
  node [
    id 228
    label "kolorystyka"
  ]
  node [
    id 229
    label "rejestr"
  ]
  node [
    id 230
    label "spirit"
  ]
  node [
    id 231
    label "wydawanie"
  ]
  node [
    id 232
    label "da&#263;"
  ]
  node [
    id 233
    label "spowodowa&#263;"
  ]
  node [
    id 234
    label "za&#322;atwi&#263;"
  ]
  node [
    id 235
    label "donie&#347;&#263;"
  ]
  node [
    id 236
    label "give"
  ]
  node [
    id 237
    label "zarekomendowa&#263;"
  ]
  node [
    id 238
    label "przes&#322;a&#263;"
  ]
  node [
    id 239
    label "litowiec"
  ]
  node [
    id 240
    label "cent"
  ]
  node [
    id 241
    label "Litwa"
  ]
  node [
    id 242
    label "lithium"
  ]
  node [
    id 243
    label "jednostka_monetarna"
  ]
  node [
    id 244
    label "anatomopatolog"
  ]
  node [
    id 245
    label "rewizja"
  ]
  node [
    id 246
    label "oznaka"
  ]
  node [
    id 247
    label "ferment"
  ]
  node [
    id 248
    label "komplet"
  ]
  node [
    id 249
    label "tura"
  ]
  node [
    id 250
    label "amendment"
  ]
  node [
    id 251
    label "zmianka"
  ]
  node [
    id 252
    label "odmienianie"
  ]
  node [
    id 253
    label "passage"
  ]
  node [
    id 254
    label "zjawisko"
  ]
  node [
    id 255
    label "change"
  ]
  node [
    id 256
    label "praca"
  ]
  node [
    id 257
    label "jaki&#347;"
  ]
  node [
    id 258
    label "kolejny"
  ]
  node [
    id 259
    label "inaczej"
  ]
  node [
    id 260
    label "r&#243;&#380;ny"
  ]
  node [
    id 261
    label "inszy"
  ]
  node [
    id 262
    label "osobno"
  ]
  node [
    id 263
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 264
    label "granica"
  ]
  node [
    id 265
    label "circle"
  ]
  node [
    id 266
    label "podzakres"
  ]
  node [
    id 267
    label "zbi&#243;r"
  ]
  node [
    id 268
    label "dziedzina"
  ]
  node [
    id 269
    label "desygnat"
  ]
  node [
    id 270
    label "sfera"
  ]
  node [
    id 271
    label "wielko&#347;&#263;"
  ]
  node [
    id 272
    label "challenge"
  ]
  node [
    id 273
    label "odcina&#263;"
  ]
  node [
    id 274
    label "odrzuca&#263;"
  ]
  node [
    id 275
    label "wyodr&#281;bnia&#263;"
  ]
  node [
    id 276
    label "przestawa&#263;"
  ]
  node [
    id 277
    label "nieprawdopodobie&#324;stwo"
  ]
  node [
    id 278
    label "unieruchamia&#263;"
  ]
  node [
    id 279
    label "od&#322;&#261;cza&#263;"
  ]
  node [
    id 280
    label "przerywa&#263;"
  ]
  node [
    id 281
    label "reject"
  ]
  node [
    id 282
    label "exsert"
  ]
  node [
    id 283
    label "obserwacja"
  ]
  node [
    id 284
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 285
    label "nauka_prawa"
  ]
  node [
    id 286
    label "dominion"
  ]
  node [
    id 287
    label "normatywizm"
  ]
  node [
    id 288
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 289
    label "qualification"
  ]
  node [
    id 290
    label "opis"
  ]
  node [
    id 291
    label "regu&#322;a_Allena"
  ]
  node [
    id 292
    label "normalizacja"
  ]
  node [
    id 293
    label "kazuistyka"
  ]
  node [
    id 294
    label "regu&#322;a_Glogera"
  ]
  node [
    id 295
    label "kultura_duchowa"
  ]
  node [
    id 296
    label "prawo_karne"
  ]
  node [
    id 297
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 298
    label "standard"
  ]
  node [
    id 299
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 300
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 301
    label "struktura"
  ]
  node [
    id 302
    label "szko&#322;a"
  ]
  node [
    id 303
    label "prawo_karne_procesowe"
  ]
  node [
    id 304
    label "prawo_Mendla"
  ]
  node [
    id 305
    label "przepis"
  ]
  node [
    id 306
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 307
    label "criterion"
  ]
  node [
    id 308
    label "kanonistyka"
  ]
  node [
    id 309
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 310
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 311
    label "wykonawczy"
  ]
  node [
    id 312
    label "twierdzenie"
  ]
  node [
    id 313
    label "judykatura"
  ]
  node [
    id 314
    label "legislacyjnie"
  ]
  node [
    id 315
    label "umocowa&#263;"
  ]
  node [
    id 316
    label "podmiot"
  ]
  node [
    id 317
    label "procesualistyka"
  ]
  node [
    id 318
    label "kierunek"
  ]
  node [
    id 319
    label "kryminologia"
  ]
  node [
    id 320
    label "kryminalistyka"
  ]
  node [
    id 321
    label "cywilistyka"
  ]
  node [
    id 322
    label "law"
  ]
  node [
    id 323
    label "zasada_d'Alemberta"
  ]
  node [
    id 324
    label "jurisprudence"
  ]
  node [
    id 325
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 326
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 327
    label "sie&#263;"
  ]
  node [
    id 328
    label "agencja"
  ]
  node [
    id 329
    label "poziom"
  ]
  node [
    id 330
    label "znaczenie"
  ]
  node [
    id 331
    label "dymensja"
  ]
  node [
    id 332
    label "parametr"
  ]
  node [
    id 333
    label "dane"
  ]
  node [
    id 334
    label "liczba"
  ]
  node [
    id 335
    label "warunek_lokalowy"
  ]
  node [
    id 336
    label "mo&#380;liwie"
  ]
  node [
    id 337
    label "nieznaczny"
  ]
  node [
    id 338
    label "kr&#243;tko"
  ]
  node [
    id 339
    label "nieistotnie"
  ]
  node [
    id 340
    label "nieliczny"
  ]
  node [
    id 341
    label "mikroskopijnie"
  ]
  node [
    id 342
    label "pomiernie"
  ]
  node [
    id 343
    label "ma&#322;y"
  ]
  node [
    id 344
    label "dokument"
  ]
  node [
    id 345
    label "bud&#380;et"
  ]
  node [
    id 346
    label "posada"
  ]
  node [
    id 347
    label "obowi&#261;zuj&#261;co"
  ]
  node [
    id 348
    label "uprawomocnianie_si&#281;"
  ]
  node [
    id 349
    label "uprawomocnienie_si&#281;"
  ]
  node [
    id 350
    label "aktualny"
  ]
  node [
    id 351
    label "pensum"
  ]
  node [
    id 352
    label "enroll"
  ]
  node [
    id 353
    label "odm&#322;adza&#263;"
  ]
  node [
    id 354
    label "asymilowa&#263;"
  ]
  node [
    id 355
    label "cz&#261;steczka"
  ]
  node [
    id 356
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 357
    label "egzemplarz"
  ]
  node [
    id 358
    label "formacja_geologiczna"
  ]
  node [
    id 359
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 360
    label "harcerze_starsi"
  ]
  node [
    id 361
    label "liga"
  ]
  node [
    id 362
    label "Terranie"
  ]
  node [
    id 363
    label "&#346;wietliki"
  ]
  node [
    id 364
    label "pakiet_klimatyczny"
  ]
  node [
    id 365
    label "oddzia&#322;"
  ]
  node [
    id 366
    label "stage_set"
  ]
  node [
    id 367
    label "Entuzjastki"
  ]
  node [
    id 368
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 369
    label "odm&#322;odzenie"
  ]
  node [
    id 370
    label "type"
  ]
  node [
    id 371
    label "category"
  ]
  node [
    id 372
    label "asymilowanie"
  ]
  node [
    id 373
    label "specgrupa"
  ]
  node [
    id 374
    label "odm&#322;adzanie"
  ]
  node [
    id 375
    label "gromada"
  ]
  node [
    id 376
    label "Eurogrupa"
  ]
  node [
    id 377
    label "jednostka_systematyczna"
  ]
  node [
    id 378
    label "kompozycja"
  ]
  node [
    id 379
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 380
    label "w&#322;ady"
  ]
  node [
    id 381
    label "pocz&#261;tkowy"
  ]
  node [
    id 382
    label "wcze&#347;nie"
  ]
  node [
    id 383
    label "ubezpieczenie_emerytalne"
  ]
  node [
    id 384
    label "&#347;wiadczenie_spo&#322;eczne"
  ]
  node [
    id 385
    label "egzystencja"
  ]
  node [
    id 386
    label "retirement"
  ]
  node [
    id 387
    label "ki&#347;&#263;"
  ]
  node [
    id 388
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 389
    label "krzew"
  ]
  node [
    id 390
    label "pi&#380;maczkowate"
  ]
  node [
    id 391
    label "pestkowiec"
  ]
  node [
    id 392
    label "kwiat"
  ]
  node [
    id 393
    label "owoc"
  ]
  node [
    id 394
    label "oliwkowate"
  ]
  node [
    id 395
    label "ro&#347;lina"
  ]
  node [
    id 396
    label "hy&#263;ka"
  ]
  node [
    id 397
    label "lilac"
  ]
  node [
    id 398
    label "delfinidyna"
  ]
  node [
    id 399
    label "przyczyna"
  ]
  node [
    id 400
    label "uwaga"
  ]
  node [
    id 401
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 402
    label "punkt_widzenia"
  ]
  node [
    id 403
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 404
    label "period"
  ]
  node [
    id 405
    label "rok"
  ]
  node [
    id 406
    label "choroba_wieku"
  ]
  node [
    id 407
    label "jednostka_geologiczna"
  ]
  node [
    id 408
    label "chron"
  ]
  node [
    id 409
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 410
    label "si&#281;ga&#263;"
  ]
  node [
    id 411
    label "trwa&#263;"
  ]
  node [
    id 412
    label "stan"
  ]
  node [
    id 413
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 414
    label "stand"
  ]
  node [
    id 415
    label "mie&#263;_miejsce"
  ]
  node [
    id 416
    label "uczestniczy&#263;"
  ]
  node [
    id 417
    label "chodzi&#263;"
  ]
  node [
    id 418
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 419
    label "equal"
  ]
  node [
    id 420
    label "zbie&#380;ny"
  ]
  node [
    id 421
    label "dobry"
  ]
  node [
    id 422
    label "spokojny"
  ]
  node [
    id 423
    label "zgodnie"
  ]
  node [
    id 424
    label "whole"
  ]
  node [
    id 425
    label "Rzym_Zachodni"
  ]
  node [
    id 426
    label "element"
  ]
  node [
    id 427
    label "urz&#261;dzenie"
  ]
  node [
    id 428
    label "Rzym_Wschodni"
  ]
  node [
    id 429
    label "give_voice"
  ]
  node [
    id 430
    label "arouse"
  ]
  node [
    id 431
    label "represent"
  ]
  node [
    id 432
    label "komunikowa&#263;"
  ]
  node [
    id 433
    label "convey"
  ]
  node [
    id 434
    label "zdradza&#263;_si&#281;"
  ]
  node [
    id 435
    label "oznacza&#263;"
  ]
  node [
    id 436
    label "znaczy&#263;"
  ]
  node [
    id 437
    label "moralno&#347;&#263;"
  ]
  node [
    id 438
    label "podstawa"
  ]
  node [
    id 439
    label "umowa"
  ]
  node [
    id 440
    label "base"
  ]
  node [
    id 441
    label "substancja"
  ]
  node [
    id 442
    label "spos&#243;b"
  ]
  node [
    id 443
    label "prawid&#322;o"
  ]
  node [
    id 444
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 445
    label "occupation"
  ]
  node [
    id 446
    label "konsekwencja"
  ]
  node [
    id 447
    label "punishment"
  ]
  node [
    id 448
    label "roboty_przymusowe"
  ]
  node [
    id 449
    label "nemezis"
  ]
  node [
    id 450
    label "righteousness"
  ]
  node [
    id 451
    label "spo&#322;ecznie"
  ]
  node [
    id 452
    label "publiczny"
  ]
  node [
    id 453
    label "budowa"
  ]
  node [
    id 454
    label "cezar"
  ]
  node [
    id 455
    label "uchwa&#322;a"
  ]
  node [
    id 456
    label "zwi&#261;zek_chemiczny"
  ]
  node [
    id 457
    label "wyspa"
  ]
  node [
    id 458
    label "Koszalin"
  ]
  node [
    id 459
    label "karta"
  ]
  node [
    id 460
    label "u"
  ]
  node [
    id 461
    label "zeszyt"
  ]
  node [
    id 462
    label "15"
  ]
  node [
    id 463
    label "2004"
  ]
  node [
    id 464
    label "ojciec"
  ]
  node [
    id 465
    label "oraz"
  ]
  node [
    id 466
    label "ustawi&#263;"
  ]
  node [
    id 467
    label "rzeczpospolita"
  ]
  node [
    id 468
    label "polski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 11
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 18
  ]
  edge [
    source 6
    target 19
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 6
    target 9
  ]
  edge [
    source 6
    target 22
  ]
  edge [
    source 6
    target 21
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 461
  ]
  edge [
    source 6
    target 462
  ]
  edge [
    source 6
    target 463
  ]
  edge [
    source 6
    target 405
  ]
  edge [
    source 6
    target 464
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 459
  ]
  edge [
    source 6
    target 465
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 466
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 124
  ]
  edge [
    source 7
    target 18
  ]
  edge [
    source 7
    target 37
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 19
  ]
  edge [
    source 8
    target 20
  ]
  edge [
    source 8
    target 22
  ]
  edge [
    source 8
    target 23
  ]
  edge [
    source 8
    target 29
  ]
  edge [
    source 8
    target 30
  ]
  edge [
    source 8
    target 125
  ]
  edge [
    source 8
    target 126
  ]
  edge [
    source 8
    target 127
  ]
  edge [
    source 8
    target 128
  ]
  edge [
    source 8
    target 18
  ]
  edge [
    source 8
    target 38
  ]
  edge [
    source 8
    target 25
  ]
  edge [
    source 8
    target 37
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 129
  ]
  edge [
    source 9
    target 130
  ]
  edge [
    source 9
    target 131
  ]
  edge [
    source 9
    target 132
  ]
  edge [
    source 9
    target 133
  ]
  edge [
    source 9
    target 134
  ]
  edge [
    source 9
    target 135
  ]
  edge [
    source 9
    target 136
  ]
  edge [
    source 9
    target 137
  ]
  edge [
    source 9
    target 138
  ]
  edge [
    source 9
    target 139
  ]
  edge [
    source 9
    target 140
  ]
  edge [
    source 9
    target 141
  ]
  edge [
    source 9
    target 142
  ]
  edge [
    source 9
    target 143
  ]
  edge [
    source 9
    target 144
  ]
  edge [
    source 9
    target 145
  ]
  edge [
    source 9
    target 146
  ]
  edge [
    source 9
    target 147
  ]
  edge [
    source 9
    target 22
  ]
  edge [
    source 9
    target 29
  ]
  edge [
    source 9
    target 21
  ]
  edge [
    source 9
    target 39
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 148
  ]
  edge [
    source 10
    target 149
  ]
  edge [
    source 10
    target 150
  ]
  edge [
    source 10
    target 151
  ]
  edge [
    source 10
    target 152
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 153
  ]
  edge [
    source 11
    target 131
  ]
  edge [
    source 11
    target 154
  ]
  edge [
    source 11
    target 155
  ]
  edge [
    source 11
    target 156
  ]
  edge [
    source 11
    target 157
  ]
  edge [
    source 11
    target 158
  ]
  edge [
    source 11
    target 159
  ]
  edge [
    source 11
    target 160
  ]
  edge [
    source 11
    target 161
  ]
  edge [
    source 11
    target 162
  ]
  edge [
    source 11
    target 163
  ]
  edge [
    source 11
    target 164
  ]
  edge [
    source 11
    target 165
  ]
  edge [
    source 11
    target 166
  ]
  edge [
    source 11
    target 167
  ]
  edge [
    source 11
    target 168
  ]
  edge [
    source 11
    target 169
  ]
  edge [
    source 11
    target 170
  ]
  edge [
    source 11
    target 171
  ]
  edge [
    source 11
    target 172
  ]
  edge [
    source 11
    target 173
  ]
  edge [
    source 11
    target 174
  ]
  edge [
    source 11
    target 175
  ]
  edge [
    source 11
    target 176
  ]
  edge [
    source 11
    target 177
  ]
  edge [
    source 11
    target 178
  ]
  edge [
    source 11
    target 457
  ]
  edge [
    source 11
    target 458
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 457
  ]
  edge [
    source 12
    target 458
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 131
  ]
  edge [
    source 14
    target 179
  ]
  edge [
    source 14
    target 180
  ]
  edge [
    source 14
    target 181
  ]
  edge [
    source 14
    target 182
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 23
  ]
  edge [
    source 15
    target 27
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 15
    target 52
  ]
  edge [
    source 15
    target 53
  ]
  edge [
    source 15
    target 57
  ]
  edge [
    source 15
    target 58
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 183
  ]
  edge [
    source 16
    target 184
  ]
  edge [
    source 16
    target 185
  ]
  edge [
    source 16
    target 186
  ]
  edge [
    source 16
    target 187
  ]
  edge [
    source 16
    target 188
  ]
  edge [
    source 16
    target 189
  ]
  edge [
    source 16
    target 190
  ]
  edge [
    source 16
    target 191
  ]
  edge [
    source 16
    target 192
  ]
  edge [
    source 16
    target 193
  ]
  edge [
    source 16
    target 194
  ]
  edge [
    source 16
    target 195
  ]
  edge [
    source 16
    target 196
  ]
  edge [
    source 16
    target 197
  ]
  edge [
    source 16
    target 198
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 28
  ]
  edge [
    source 18
    target 28
  ]
  edge [
    source 18
    target 30
  ]
  edge [
    source 18
    target 20
  ]
  edge [
    source 18
    target 32
  ]
  edge [
    source 18
    target 22
  ]
  edge [
    source 18
    target 199
  ]
  edge [
    source 18
    target 200
  ]
  edge [
    source 18
    target 201
  ]
  edge [
    source 18
    target 202
  ]
  edge [
    source 18
    target 203
  ]
  edge [
    source 18
    target 204
  ]
  edge [
    source 18
    target 205
  ]
  edge [
    source 18
    target 38
  ]
  edge [
    source 18
    target 21
  ]
  edge [
    source 18
    target 37
  ]
  edge [
    source 18
    target 49
  ]
  edge [
    source 18
    target 461
  ]
  edge [
    source 18
    target 462
  ]
  edge [
    source 18
    target 29
  ]
  edge [
    source 18
    target 463
  ]
  edge [
    source 18
    target 405
  ]
  edge [
    source 18
    target 464
  ]
  edge [
    source 18
    target 18
  ]
  edge [
    source 18
    target 459
  ]
  edge [
    source 18
    target 465
  ]
  edge [
    source 18
    target 31
  ]
  edge [
    source 18
    target 466
  ]
  edge [
    source 19
    target 206
  ]
  edge [
    source 19
    target 124
  ]
  edge [
    source 19
    target 24
  ]
  edge [
    source 19
    target 34
  ]
  edge [
    source 19
    target 43
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 207
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 30
  ]
  edge [
    source 21
    target 35
  ]
  edge [
    source 21
    target 36
  ]
  edge [
    source 21
    target 44
  ]
  edge [
    source 21
    target 45
  ]
  edge [
    source 21
    target 208
  ]
  edge [
    source 21
    target 209
  ]
  edge [
    source 21
    target 210
  ]
  edge [
    source 21
    target 211
  ]
  edge [
    source 21
    target 212
  ]
  edge [
    source 21
    target 213
  ]
  edge [
    source 21
    target 214
  ]
  edge [
    source 21
    target 215
  ]
  edge [
    source 21
    target 26
  ]
  edge [
    source 21
    target 49
  ]
  edge [
    source 21
    target 29
  ]
  edge [
    source 21
    target 39
  ]
  edge [
    source 21
    target 42
  ]
  edge [
    source 21
    target 459
  ]
  edge [
    source 21
    target 461
  ]
  edge [
    source 21
    target 462
  ]
  edge [
    source 21
    target 463
  ]
  edge [
    source 21
    target 405
  ]
  edge [
    source 21
    target 464
  ]
  edge [
    source 21
    target 465
  ]
  edge [
    source 21
    target 31
  ]
  edge [
    source 21
    target 32
  ]
  edge [
    source 21
    target 466
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 216
  ]
  edge [
    source 22
    target 217
  ]
  edge [
    source 22
    target 218
  ]
  edge [
    source 22
    target 219
  ]
  edge [
    source 22
    target 220
  ]
  edge [
    source 22
    target 221
  ]
  edge [
    source 22
    target 222
  ]
  edge [
    source 22
    target 223
  ]
  edge [
    source 22
    target 27
  ]
  edge [
    source 22
    target 36
  ]
  edge [
    source 22
    target 39
  ]
  edge [
    source 22
    target 47
  ]
  edge [
    source 22
    target 29
  ]
  edge [
    source 22
    target 460
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 33
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 34
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 224
  ]
  edge [
    source 25
    target 225
  ]
  edge [
    source 25
    target 226
  ]
  edge [
    source 25
    target 227
  ]
  edge [
    source 25
    target 228
  ]
  edge [
    source 25
    target 229
  ]
  edge [
    source 25
    target 230
  ]
  edge [
    source 25
    target 231
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 232
  ]
  edge [
    source 26
    target 233
  ]
  edge [
    source 26
    target 234
  ]
  edge [
    source 26
    target 235
  ]
  edge [
    source 26
    target 236
  ]
  edge [
    source 26
    target 237
  ]
  edge [
    source 26
    target 238
  ]
  edge [
    source 26
    target 35
  ]
  edge [
    source 27
    target 36
  ]
  edge [
    source 27
    target 39
  ]
  edge [
    source 27
    target 47
  ]
  edge [
    source 28
    target 239
  ]
  edge [
    source 28
    target 240
  ]
  edge [
    source 28
    target 241
  ]
  edge [
    source 28
    target 242
  ]
  edge [
    source 28
    target 243
  ]
  edge [
    source 29
    target 124
  ]
  edge [
    source 29
    target 39
  ]
  edge [
    source 29
    target 461
  ]
  edge [
    source 29
    target 462
  ]
  edge [
    source 29
    target 463
  ]
  edge [
    source 29
    target 405
  ]
  edge [
    source 29
    target 464
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 459
  ]
  edge [
    source 29
    target 465
  ]
  edge [
    source 29
    target 31
  ]
  edge [
    source 29
    target 32
  ]
  edge [
    source 29
    target 466
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 244
  ]
  edge [
    source 30
    target 245
  ]
  edge [
    source 30
    target 246
  ]
  edge [
    source 30
    target 98
  ]
  edge [
    source 30
    target 247
  ]
  edge [
    source 30
    target 248
  ]
  edge [
    source 30
    target 249
  ]
  edge [
    source 30
    target 250
  ]
  edge [
    source 30
    target 251
  ]
  edge [
    source 30
    target 252
  ]
  edge [
    source 30
    target 253
  ]
  edge [
    source 30
    target 254
  ]
  edge [
    source 30
    target 255
  ]
  edge [
    source 30
    target 256
  ]
  edge [
    source 30
    target 44
  ]
  edge [
    source 30
    target 56
  ]
  edge [
    source 30
    target 52
  ]
  edge [
    source 30
    target 461
  ]
  edge [
    source 30
    target 462
  ]
  edge [
    source 30
    target 463
  ]
  edge [
    source 30
    target 405
  ]
  edge [
    source 30
    target 464
  ]
  edge [
    source 30
    target 459
  ]
  edge [
    source 30
    target 465
  ]
  edge [
    source 30
    target 30
  ]
  edge [
    source 30
    target 32
  ]
  edge [
    source 30
    target 466
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 257
  ]
  edge [
    source 31
    target 461
  ]
  edge [
    source 31
    target 462
  ]
  edge [
    source 31
    target 463
  ]
  edge [
    source 31
    target 405
  ]
  edge [
    source 31
    target 464
  ]
  edge [
    source 31
    target 459
  ]
  edge [
    source 31
    target 465
  ]
  edge [
    source 31
    target 466
  ]
  edge [
    source 32
    target 258
  ]
  edge [
    source 32
    target 259
  ]
  edge [
    source 32
    target 260
  ]
  edge [
    source 32
    target 261
  ]
  edge [
    source 32
    target 262
  ]
  edge [
    source 32
    target 461
  ]
  edge [
    source 32
    target 462
  ]
  edge [
    source 32
    target 463
  ]
  edge [
    source 32
    target 405
  ]
  edge [
    source 32
    target 464
  ]
  edge [
    source 32
    target 459
  ]
  edge [
    source 32
    target 465
  ]
  edge [
    source 32
    target 466
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 263
  ]
  edge [
    source 33
    target 264
  ]
  edge [
    source 33
    target 265
  ]
  edge [
    source 33
    target 266
  ]
  edge [
    source 33
    target 267
  ]
  edge [
    source 33
    target 268
  ]
  edge [
    source 33
    target 269
  ]
  edge [
    source 33
    target 270
  ]
  edge [
    source 33
    target 271
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 272
  ]
  edge [
    source 34
    target 273
  ]
  edge [
    source 34
    target 274
  ]
  edge [
    source 34
    target 275
  ]
  edge [
    source 34
    target 276
  ]
  edge [
    source 34
    target 277
  ]
  edge [
    source 34
    target 278
  ]
  edge [
    source 34
    target 279
  ]
  edge [
    source 34
    target 280
  ]
  edge [
    source 34
    target 281
  ]
  edge [
    source 34
    target 282
  ]
  edge [
    source 35
    target 283
  ]
  edge [
    source 35
    target 284
  ]
  edge [
    source 35
    target 285
  ]
  edge [
    source 35
    target 286
  ]
  edge [
    source 35
    target 287
  ]
  edge [
    source 35
    target 288
  ]
  edge [
    source 35
    target 289
  ]
  edge [
    source 35
    target 290
  ]
  edge [
    source 35
    target 291
  ]
  edge [
    source 35
    target 292
  ]
  edge [
    source 35
    target 293
  ]
  edge [
    source 35
    target 294
  ]
  edge [
    source 35
    target 295
  ]
  edge [
    source 35
    target 296
  ]
  edge [
    source 35
    target 297
  ]
  edge [
    source 35
    target 298
  ]
  edge [
    source 35
    target 299
  ]
  edge [
    source 35
    target 300
  ]
  edge [
    source 35
    target 301
  ]
  edge [
    source 35
    target 302
  ]
  edge [
    source 35
    target 303
  ]
  edge [
    source 35
    target 304
  ]
  edge [
    source 35
    target 305
  ]
  edge [
    source 35
    target 306
  ]
  edge [
    source 35
    target 307
  ]
  edge [
    source 35
    target 308
  ]
  edge [
    source 35
    target 309
  ]
  edge [
    source 35
    target 310
  ]
  edge [
    source 35
    target 311
  ]
  edge [
    source 35
    target 312
  ]
  edge [
    source 35
    target 313
  ]
  edge [
    source 35
    target 314
  ]
  edge [
    source 35
    target 315
  ]
  edge [
    source 35
    target 316
  ]
  edge [
    source 35
    target 317
  ]
  edge [
    source 35
    target 318
  ]
  edge [
    source 35
    target 319
  ]
  edge [
    source 35
    target 320
  ]
  edge [
    source 35
    target 321
  ]
  edge [
    source 35
    target 322
  ]
  edge [
    source 35
    target 323
  ]
  edge [
    source 35
    target 324
  ]
  edge [
    source 35
    target 55
  ]
  edge [
    source 35
    target 325
  ]
  edge [
    source 35
    target 326
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 77
  ]
  edge [
    source 36
    target 39
  ]
  edge [
    source 36
    target 47
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 327
  ]
  edge [
    source 37
    target 75
  ]
  edge [
    source 37
    target 328
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 57
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 42
  ]
  edge [
    source 39
    target 43
  ]
  edge [
    source 39
    target 329
  ]
  edge [
    source 39
    target 330
  ]
  edge [
    source 39
    target 331
  ]
  edge [
    source 39
    target 174
  ]
  edge [
    source 39
    target 225
  ]
  edge [
    source 39
    target 332
  ]
  edge [
    source 39
    target 333
  ]
  edge [
    source 39
    target 334
  ]
  edge [
    source 39
    target 67
  ]
  edge [
    source 39
    target 271
  ]
  edge [
    source 39
    target 335
  ]
  edge [
    source 39
    target 41
  ]
  edge [
    source 39
    target 47
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 336
  ]
  edge [
    source 40
    target 337
  ]
  edge [
    source 40
    target 338
  ]
  edge [
    source 40
    target 339
  ]
  edge [
    source 40
    target 340
  ]
  edge [
    source 40
    target 341
  ]
  edge [
    source 40
    target 342
  ]
  edge [
    source 40
    target 343
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 344
  ]
  edge [
    source 41
    target 345
  ]
  edge [
    source 41
    target 67
  ]
  edge [
    source 41
    target 346
  ]
  edge [
    source 42
    target 347
  ]
  edge [
    source 42
    target 348
  ]
  edge [
    source 42
    target 349
  ]
  edge [
    source 42
    target 350
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 351
  ]
  edge [
    source 43
    target 352
  ]
  edge [
    source 44
    target 353
  ]
  edge [
    source 44
    target 354
  ]
  edge [
    source 44
    target 355
  ]
  edge [
    source 44
    target 356
  ]
  edge [
    source 44
    target 357
  ]
  edge [
    source 44
    target 358
  ]
  edge [
    source 44
    target 359
  ]
  edge [
    source 44
    target 360
  ]
  edge [
    source 44
    target 361
  ]
  edge [
    source 44
    target 362
  ]
  edge [
    source 44
    target 363
  ]
  edge [
    source 44
    target 364
  ]
  edge [
    source 44
    target 365
  ]
  edge [
    source 44
    target 366
  ]
  edge [
    source 44
    target 367
  ]
  edge [
    source 44
    target 368
  ]
  edge [
    source 44
    target 369
  ]
  edge [
    source 44
    target 370
  ]
  edge [
    source 44
    target 371
  ]
  edge [
    source 44
    target 372
  ]
  edge [
    source 44
    target 373
  ]
  edge [
    source 44
    target 374
  ]
  edge [
    source 44
    target 375
  ]
  edge [
    source 44
    target 376
  ]
  edge [
    source 44
    target 377
  ]
  edge [
    source 44
    target 378
  ]
  edge [
    source 44
    target 379
  ]
  edge [
    source 44
    target 267
  ]
  edge [
    source 44
    target 56
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 380
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 381
  ]
  edge [
    source 46
    target 382
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 98
  ]
  edge [
    source 47
    target 383
  ]
  edge [
    source 47
    target 384
  ]
  edge [
    source 47
    target 385
  ]
  edge [
    source 47
    target 386
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 387
  ]
  edge [
    source 48
    target 388
  ]
  edge [
    source 48
    target 389
  ]
  edge [
    source 48
    target 390
  ]
  edge [
    source 48
    target 391
  ]
  edge [
    source 48
    target 392
  ]
  edge [
    source 48
    target 393
  ]
  edge [
    source 48
    target 394
  ]
  edge [
    source 48
    target 395
  ]
  edge [
    source 48
    target 396
  ]
  edge [
    source 48
    target 397
  ]
  edge [
    source 48
    target 398
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 399
  ]
  edge [
    source 49
    target 400
  ]
  edge [
    source 49
    target 401
  ]
  edge [
    source 49
    target 402
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 403
  ]
  edge [
    source 50
    target 98
  ]
  edge [
    source 50
    target 404
  ]
  edge [
    source 50
    target 225
  ]
  edge [
    source 50
    target 405
  ]
  edge [
    source 50
    target 99
  ]
  edge [
    source 50
    target 406
  ]
  edge [
    source 50
    target 407
  ]
  edge [
    source 50
    target 408
  ]
  edge [
    source 50
    target 409
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 410
  ]
  edge [
    source 51
    target 411
  ]
  edge [
    source 51
    target 64
  ]
  edge [
    source 51
    target 412
  ]
  edge [
    source 51
    target 413
  ]
  edge [
    source 51
    target 414
  ]
  edge [
    source 51
    target 415
  ]
  edge [
    source 51
    target 416
  ]
  edge [
    source 51
    target 417
  ]
  edge [
    source 51
    target 418
  ]
  edge [
    source 51
    target 419
  ]
  edge [
    source 52
    target 420
  ]
  edge [
    source 52
    target 421
  ]
  edge [
    source 52
    target 422
  ]
  edge [
    source 52
    target 423
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 424
  ]
  edge [
    source 53
    target 425
  ]
  edge [
    source 53
    target 426
  ]
  edge [
    source 53
    target 67
  ]
  edge [
    source 53
    target 427
  ]
  edge [
    source 53
    target 428
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 429
  ]
  edge [
    source 54
    target 430
  ]
  edge [
    source 54
    target 431
  ]
  edge [
    source 54
    target 432
  ]
  edge [
    source 54
    target 433
  ]
  edge [
    source 54
    target 434
  ]
  edge [
    source 54
    target 435
  ]
  edge [
    source 54
    target 436
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 283
  ]
  edge [
    source 55
    target 437
  ]
  edge [
    source 55
    target 438
  ]
  edge [
    source 55
    target 284
  ]
  edge [
    source 55
    target 439
  ]
  edge [
    source 55
    target 286
  ]
  edge [
    source 55
    target 289
  ]
  edge [
    source 55
    target 288
  ]
  edge [
    source 55
    target 290
  ]
  edge [
    source 55
    target 291
  ]
  edge [
    source 55
    target 292
  ]
  edge [
    source 55
    target 294
  ]
  edge [
    source 55
    target 300
  ]
  edge [
    source 55
    target 298
  ]
  edge [
    source 55
    target 440
  ]
  edge [
    source 55
    target 441
  ]
  edge [
    source 55
    target 442
  ]
  edge [
    source 55
    target 443
  ]
  edge [
    source 55
    target 304
  ]
  edge [
    source 55
    target 306
  ]
  edge [
    source 55
    target 307
  ]
  edge [
    source 55
    target 312
  ]
  edge [
    source 55
    target 444
  ]
  edge [
    source 55
    target 445
  ]
  edge [
    source 55
    target 323
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 446
  ]
  edge [
    source 56
    target 447
  ]
  edge [
    source 56
    target 225
  ]
  edge [
    source 56
    target 448
  ]
  edge [
    source 56
    target 449
  ]
  edge [
    source 56
    target 450
  ]
  edge [
    source 57
    target 451
  ]
  edge [
    source 57
    target 452
  ]
  edge [
    source 58
    target 344
  ]
  edge [
    source 58
    target 453
  ]
  edge [
    source 58
    target 202
  ]
  edge [
    source 58
    target 454
  ]
  edge [
    source 58
    target 267
  ]
  edge [
    source 58
    target 455
  ]
  edge [
    source 58
    target 301
  ]
  edge [
    source 58
    target 456
  ]
  edge [
    source 58
    target 467
  ]
  edge [
    source 58
    target 468
  ]
  edge [
    source 405
    target 461
  ]
  edge [
    source 405
    target 462
  ]
  edge [
    source 405
    target 463
  ]
  edge [
    source 405
    target 464
  ]
  edge [
    source 405
    target 459
  ]
  edge [
    source 405
    target 465
  ]
  edge [
    source 405
    target 466
  ]
  edge [
    source 457
    target 458
  ]
  edge [
    source 459
    target 461
  ]
  edge [
    source 459
    target 462
  ]
  edge [
    source 459
    target 463
  ]
  edge [
    source 459
    target 464
  ]
  edge [
    source 459
    target 465
  ]
  edge [
    source 459
    target 466
  ]
  edge [
    source 461
    target 462
  ]
  edge [
    source 461
    target 463
  ]
  edge [
    source 461
    target 464
  ]
  edge [
    source 461
    target 465
  ]
  edge [
    source 461
    target 466
  ]
  edge [
    source 462
    target 463
  ]
  edge [
    source 462
    target 464
  ]
  edge [
    source 462
    target 465
  ]
  edge [
    source 462
    target 466
  ]
  edge [
    source 463
    target 464
  ]
  edge [
    source 463
    target 465
  ]
  edge [
    source 463
    target 466
  ]
  edge [
    source 464
    target 465
  ]
  edge [
    source 464
    target 464
  ]
  edge [
    source 464
    target 466
  ]
  edge [
    source 465
    target 466
  ]
  edge [
    source 467
    target 468
  ]
]
