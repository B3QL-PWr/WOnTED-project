graph [
  maxDegree 38
  minDegree 1
  meanDegree 2.0392156862745097
  density 0.020190254319549603
  graphCliqueNumber 2
  node [
    id 0
    label "dobrze"
    origin "text"
  ]
  node [
    id 1
    label "jeszcze"
    origin "text"
  ]
  node [
    id 2
    label "m&#243;cby"
    origin "text"
  ]
  node [
    id 3
    label "pan"
    origin "text"
  ]
  node [
    id 4
    label "sprawdzi&#263;"
    origin "text"
  ]
  node [
    id 5
    label "sto"
    origin "text"
  ]
  node [
    id 6
    label "czterdzie&#347;ci"
    origin "text"
  ]
  node [
    id 7
    label "trzy"
    origin "text"
  ]
  node [
    id 8
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 9
    label "tak"
    origin "text"
  ]
  node [
    id 10
    label "ma&#322;o"
    origin "text"
  ]
  node [
    id 11
    label "du&#380;o"
    origin "text"
  ]
  node [
    id 12
    label "granica"
    origin "text"
  ]
  node [
    id 13
    label "godzina"
    origin "text"
  ]
  node [
    id 14
    label "siedemnasta"
    origin "text"
  ]
  node [
    id 15
    label "filler"
    origin "text"
  ]
  node [
    id 16
    label "szesnasta"
    origin "text"
  ]
  node [
    id 17
    label "pi&#281;&#263;"
    origin "text"
  ]
  node [
    id 18
    label "moralnie"
  ]
  node [
    id 19
    label "wiele"
  ]
  node [
    id 20
    label "lepiej"
  ]
  node [
    id 21
    label "korzystnie"
  ]
  node [
    id 22
    label "pomy&#347;lnie"
  ]
  node [
    id 23
    label "pozytywnie"
  ]
  node [
    id 24
    label "dobry"
  ]
  node [
    id 25
    label "dobroczynnie"
  ]
  node [
    id 26
    label "odpowiednio"
  ]
  node [
    id 27
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 28
    label "skutecznie"
  ]
  node [
    id 29
    label "ci&#261;gle"
  ]
  node [
    id 30
    label "cz&#322;owiek"
  ]
  node [
    id 31
    label "profesor"
  ]
  node [
    id 32
    label "kszta&#322;ciciel"
  ]
  node [
    id 33
    label "jegomo&#347;&#263;"
  ]
  node [
    id 34
    label "zwrot"
  ]
  node [
    id 35
    label "pracodawca"
  ]
  node [
    id 36
    label "rz&#261;dzenie"
  ]
  node [
    id 37
    label "m&#261;&#380;"
  ]
  node [
    id 38
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 39
    label "ch&#322;opina"
  ]
  node [
    id 40
    label "bratek"
  ]
  node [
    id 41
    label "opiekun"
  ]
  node [
    id 42
    label "doros&#322;y"
  ]
  node [
    id 43
    label "preceptor"
  ]
  node [
    id 44
    label "Midas"
  ]
  node [
    id 45
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 46
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 47
    label "murza"
  ]
  node [
    id 48
    label "ojciec"
  ]
  node [
    id 49
    label "androlog"
  ]
  node [
    id 50
    label "pupil"
  ]
  node [
    id 51
    label "efendi"
  ]
  node [
    id 52
    label "nabab"
  ]
  node [
    id 53
    label "w&#322;odarz"
  ]
  node [
    id 54
    label "szkolnik"
  ]
  node [
    id 55
    label "pedagog"
  ]
  node [
    id 56
    label "popularyzator"
  ]
  node [
    id 57
    label "andropauza"
  ]
  node [
    id 58
    label "gra_w_karty"
  ]
  node [
    id 59
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 60
    label "Mieszko_I"
  ]
  node [
    id 61
    label "bogaty"
  ]
  node [
    id 62
    label "samiec"
  ]
  node [
    id 63
    label "przyw&#243;dca"
  ]
  node [
    id 64
    label "pa&#324;stwo"
  ]
  node [
    id 65
    label "belfer"
  ]
  node [
    id 66
    label "examine"
  ]
  node [
    id 67
    label "zrobi&#263;"
  ]
  node [
    id 68
    label "mo&#380;liwie"
  ]
  node [
    id 69
    label "nieznaczny"
  ]
  node [
    id 70
    label "kr&#243;tko"
  ]
  node [
    id 71
    label "nieistotnie"
  ]
  node [
    id 72
    label "nieliczny"
  ]
  node [
    id 73
    label "mikroskopijnie"
  ]
  node [
    id 74
    label "pomiernie"
  ]
  node [
    id 75
    label "ma&#322;y"
  ]
  node [
    id 76
    label "du&#380;y"
  ]
  node [
    id 77
    label "cz&#281;sto"
  ]
  node [
    id 78
    label "bardzo"
  ]
  node [
    id 79
    label "mocno"
  ]
  node [
    id 80
    label "wiela"
  ]
  node [
    id 81
    label "zakres"
  ]
  node [
    id 82
    label "Ural"
  ]
  node [
    id 83
    label "koniec"
  ]
  node [
    id 84
    label "kres"
  ]
  node [
    id 85
    label "granice"
  ]
  node [
    id 86
    label "granica_pa&#324;stwa"
  ]
  node [
    id 87
    label "pu&#322;ap"
  ]
  node [
    id 88
    label "frontier"
  ]
  node [
    id 89
    label "end"
  ]
  node [
    id 90
    label "miara"
  ]
  node [
    id 91
    label "poj&#281;cie"
  ]
  node [
    id 92
    label "przej&#347;cie"
  ]
  node [
    id 93
    label "minuta"
  ]
  node [
    id 94
    label "doba"
  ]
  node [
    id 95
    label "czas"
  ]
  node [
    id 96
    label "p&#243;&#322;godzina"
  ]
  node [
    id 97
    label "kwadrans"
  ]
  node [
    id 98
    label "time"
  ]
  node [
    id 99
    label "jednostka_czasu"
  ]
  node [
    id 100
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 101
    label "drzewo_owocowe"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 16
  ]
  edge [
    source 6
    target 17
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 12
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 10
    target 69
  ]
  edge [
    source 10
    target 70
  ]
  edge [
    source 10
    target 71
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 76
  ]
  edge [
    source 11
    target 77
  ]
  edge [
    source 11
    target 78
  ]
  edge [
    source 11
    target 79
  ]
  edge [
    source 11
    target 80
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 81
  ]
  edge [
    source 12
    target 82
  ]
  edge [
    source 12
    target 83
  ]
  edge [
    source 12
    target 84
  ]
  edge [
    source 12
    target 85
  ]
  edge [
    source 12
    target 86
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 12
    target 89
  ]
  edge [
    source 12
    target 90
  ]
  edge [
    source 12
    target 91
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 93
  ]
  edge [
    source 13
    target 94
  ]
  edge [
    source 13
    target 95
  ]
  edge [
    source 13
    target 96
  ]
  edge [
    source 13
    target 97
  ]
  edge [
    source 13
    target 98
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 13
    target 16
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 100
  ]
  edge [
    source 15
    target 101
  ]
]
