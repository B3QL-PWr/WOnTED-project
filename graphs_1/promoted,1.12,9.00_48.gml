graph [
  maxDegree 45
  minDegree 1
  meanDegree 2.018018018018018
  density 0.018345618345618344
  graphCliqueNumber 2
  node [
    id 0
    label "historia"
    origin "text"
  ]
  node [
    id 1
    label "cho&#263;"
    origin "text"
  ]
  node [
    id 2
    label "wydawa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "si&#281;"
    origin "text"
  ]
  node [
    id 4
    label "by&#263;"
    origin "text"
  ]
  node [
    id 5
    label "kompletnie"
    origin "text"
  ]
  node [
    id 6
    label "absurdalny"
    origin "text"
  ]
  node [
    id 7
    label "mie&#263;"
    origin "text"
  ]
  node [
    id 8
    label "miejsce"
    origin "text"
  ]
  node [
    id 9
    label "naprawd&#281;"
    origin "text"
  ]
  node [
    id 10
    label "report"
  ]
  node [
    id 11
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 12
    label "wypowied&#378;"
  ]
  node [
    id 13
    label "neografia"
  ]
  node [
    id 14
    label "przedmiot"
  ]
  node [
    id 15
    label "papirologia"
  ]
  node [
    id 16
    label "historia_gospodarcza"
  ]
  node [
    id 17
    label "przebiec"
  ]
  node [
    id 18
    label "hista"
  ]
  node [
    id 19
    label "nauka_humanistyczna"
  ]
  node [
    id 20
    label "filigranistyka"
  ]
  node [
    id 21
    label "dyplomatyka"
  ]
  node [
    id 22
    label "annalistyka"
  ]
  node [
    id 23
    label "historyka"
  ]
  node [
    id 24
    label "heraldyka"
  ]
  node [
    id 25
    label "fabu&#322;a"
  ]
  node [
    id 26
    label "muzealnictwo"
  ]
  node [
    id 27
    label "varsavianistyka"
  ]
  node [
    id 28
    label "mediewistyka"
  ]
  node [
    id 29
    label "prezentyzm"
  ]
  node [
    id 30
    label "przebiegni&#281;cie"
  ]
  node [
    id 31
    label "charakter"
  ]
  node [
    id 32
    label "paleografia"
  ]
  node [
    id 33
    label "genealogia"
  ]
  node [
    id 34
    label "czynno&#347;&#263;"
  ]
  node [
    id 35
    label "prozopografia"
  ]
  node [
    id 36
    label "motyw"
  ]
  node [
    id 37
    label "nautologia"
  ]
  node [
    id 38
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 39
    label "epoka"
  ]
  node [
    id 40
    label "numizmatyka"
  ]
  node [
    id 41
    label "ruralistyka"
  ]
  node [
    id 42
    label "&#378;r&#243;d&#322;oznawstwo"
  ]
  node [
    id 43
    label "epigrafika"
  ]
  node [
    id 44
    label "historiografia"
  ]
  node [
    id 45
    label "bizantynistyka"
  ]
  node [
    id 46
    label "weksylologia"
  ]
  node [
    id 47
    label "kierunek"
  ]
  node [
    id 48
    label "ikonografia"
  ]
  node [
    id 49
    label "chronologia"
  ]
  node [
    id 50
    label "archiwistyka"
  ]
  node [
    id 51
    label "sfragistyka"
  ]
  node [
    id 52
    label "zabytkoznawstwo"
  ]
  node [
    id 53
    label "historia_sztuki"
  ]
  node [
    id 54
    label "impart"
  ]
  node [
    id 55
    label "panna_na_wydaniu"
  ]
  node [
    id 56
    label "surrender"
  ]
  node [
    id 57
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 58
    label "train"
  ]
  node [
    id 59
    label "give"
  ]
  node [
    id 60
    label "wytwarza&#263;"
  ]
  node [
    id 61
    label "dawa&#263;"
  ]
  node [
    id 62
    label "zapach"
  ]
  node [
    id 63
    label "wprowadza&#263;"
  ]
  node [
    id 64
    label "ujawnia&#263;"
  ]
  node [
    id 65
    label "wydawnictwo"
  ]
  node [
    id 66
    label "powierza&#263;"
  ]
  node [
    id 67
    label "produkcja"
  ]
  node [
    id 68
    label "denuncjowa&#263;"
  ]
  node [
    id 69
    label "mie&#263;_miejsce"
  ]
  node [
    id 70
    label "plon"
  ]
  node [
    id 71
    label "reszta"
  ]
  node [
    id 72
    label "robi&#263;"
  ]
  node [
    id 73
    label "placard"
  ]
  node [
    id 74
    label "tajemnica"
  ]
  node [
    id 75
    label "wiano"
  ]
  node [
    id 76
    label "kojarzy&#263;"
  ]
  node [
    id 77
    label "d&#378;wi&#281;k"
  ]
  node [
    id 78
    label "podawa&#263;"
  ]
  node [
    id 79
    label "si&#281;ga&#263;"
  ]
  node [
    id 80
    label "trwa&#263;"
  ]
  node [
    id 81
    label "obecno&#347;&#263;"
  ]
  node [
    id 82
    label "stan"
  ]
  node [
    id 83
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 84
    label "stand"
  ]
  node [
    id 85
    label "uczestniczy&#263;"
  ]
  node [
    id 86
    label "chodzi&#263;"
  ]
  node [
    id 87
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 88
    label "equal"
  ]
  node [
    id 89
    label "kompletny"
  ]
  node [
    id 90
    label "zupe&#322;nie"
  ]
  node [
    id 91
    label "absurdalnie"
  ]
  node [
    id 92
    label "bezsensowny"
  ]
  node [
    id 93
    label "czu&#263;"
  ]
  node [
    id 94
    label "need"
  ]
  node [
    id 95
    label "hide"
  ]
  node [
    id 96
    label "support"
  ]
  node [
    id 97
    label "cia&#322;o"
  ]
  node [
    id 98
    label "plac"
  ]
  node [
    id 99
    label "cecha"
  ]
  node [
    id 100
    label "uwaga"
  ]
  node [
    id 101
    label "przestrze&#324;"
  ]
  node [
    id 102
    label "status"
  ]
  node [
    id 103
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 104
    label "chwila"
  ]
  node [
    id 105
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 106
    label "rz&#261;d"
  ]
  node [
    id 107
    label "praca"
  ]
  node [
    id 108
    label "location"
  ]
  node [
    id 109
    label "warunek_lokalowy"
  ]
  node [
    id 110
    label "prawdziwy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 9
    target 110
  ]
]
