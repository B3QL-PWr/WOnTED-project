graph [
  maxDegree 17
  minDegree 1
  meanDegree 1.9154929577464788
  density 0.027364185110663984
  graphCliqueNumber 2
  node [
    id 0
    label "by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 2
    label "le&#380;&#261;cy"
    origin "text"
  ]
  node [
    id 3
    label "powinien"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "kopa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "bethesda"
    origin "text"
  ]
  node [
    id 7
    label "przez"
    origin "text"
  ]
  node [
    id 8
    label "swoje"
    origin "text"
  ]
  node [
    id 9
    label "post&#281;powanie"
    origin "text"
  ]
  node [
    id 10
    label "zas&#322;ugiwa&#263;"
    origin "text"
  ]
  node [
    id 11
    label "najwy&#380;szy"
    origin "text"
  ]
  node [
    id 12
    label "wymiar"
    origin "text"
  ]
  node [
    id 13
    label "kar"
    origin "text"
  ]
  node [
    id 14
    label "si&#281;ga&#263;"
  ]
  node [
    id 15
    label "trwa&#263;"
  ]
  node [
    id 16
    label "obecno&#347;&#263;"
  ]
  node [
    id 17
    label "stan"
  ]
  node [
    id 18
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 19
    label "stand"
  ]
  node [
    id 20
    label "mie&#263;_miejsce"
  ]
  node [
    id 21
    label "uczestniczy&#263;"
  ]
  node [
    id 22
    label "chodzi&#263;"
  ]
  node [
    id 23
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 24
    label "equal"
  ]
  node [
    id 25
    label "musie&#263;"
  ]
  node [
    id 26
    label "due"
  ]
  node [
    id 27
    label "spulchnia&#263;"
  ]
  node [
    id 28
    label "wykopywa&#263;"
  ]
  node [
    id 29
    label "chow"
  ]
  node [
    id 30
    label "pora&#380;a&#263;"
  ]
  node [
    id 31
    label "hack"
  ]
  node [
    id 32
    label "pi&#322;ka_no&#380;na"
  ]
  node [
    id 33
    label "robi&#263;"
  ]
  node [
    id 34
    label "d&#243;&#322;"
  ]
  node [
    id 35
    label "krytykowa&#263;"
  ]
  node [
    id 36
    label "uderza&#263;"
  ]
  node [
    id 37
    label "dig"
  ]
  node [
    id 38
    label "przeszukiwa&#263;"
  ]
  node [
    id 39
    label "robienie"
  ]
  node [
    id 40
    label "czynno&#347;&#263;"
  ]
  node [
    id 41
    label "zachowanie"
  ]
  node [
    id 42
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 43
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 44
    label "kognicja"
  ]
  node [
    id 45
    label "rozprawa"
  ]
  node [
    id 46
    label "s&#261;d"
  ]
  node [
    id 47
    label "kazanie"
  ]
  node [
    id 48
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 49
    label "campaign"
  ]
  node [
    id 50
    label "fashion"
  ]
  node [
    id 51
    label "wydarzenie"
  ]
  node [
    id 52
    label "przes&#322;anka"
  ]
  node [
    id 53
    label "zmierzanie"
  ]
  node [
    id 54
    label "addition"
  ]
  node [
    id 55
    label "poziom"
  ]
  node [
    id 56
    label "znaczenie"
  ]
  node [
    id 57
    label "dymensja"
  ]
  node [
    id 58
    label "strona"
  ]
  node [
    id 59
    label "cecha"
  ]
  node [
    id 60
    label "parametr"
  ]
  node [
    id 61
    label "dane"
  ]
  node [
    id 62
    label "liczba"
  ]
  node [
    id 63
    label "ilo&#347;&#263;"
  ]
  node [
    id 64
    label "wielko&#347;&#263;"
  ]
  node [
    id 65
    label "warunek_lokalowy"
  ]
  node [
    id 66
    label "wn&#281;ka"
  ]
  node [
    id 67
    label "edycja"
  ]
  node [
    id 68
    label "kolekcjonerski"
  ]
  node [
    id 69
    label "Falloutem"
  ]
  node [
    id 70
    label "76"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 39
  ]
  edge [
    source 9
    target 40
  ]
  edge [
    source 9
    target 41
  ]
  edge [
    source 9
    target 42
  ]
  edge [
    source 9
    target 43
  ]
  edge [
    source 9
    target 44
  ]
  edge [
    source 9
    target 45
  ]
  edge [
    source 9
    target 46
  ]
  edge [
    source 9
    target 47
  ]
  edge [
    source 9
    target 48
  ]
  edge [
    source 9
    target 49
  ]
  edge [
    source 9
    target 50
  ]
  edge [
    source 9
    target 51
  ]
  edge [
    source 9
    target 52
  ]
  edge [
    source 9
    target 53
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 54
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 55
  ]
  edge [
    source 12
    target 56
  ]
  edge [
    source 12
    target 57
  ]
  edge [
    source 12
    target 58
  ]
  edge [
    source 12
    target 59
  ]
  edge [
    source 12
    target 60
  ]
  edge [
    source 12
    target 61
  ]
  edge [
    source 12
    target 62
  ]
  edge [
    source 12
    target 63
  ]
  edge [
    source 12
    target 64
  ]
  edge [
    source 12
    target 65
  ]
  edge [
    source 13
    target 66
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 69
    target 70
  ]
]
