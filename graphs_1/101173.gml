graph [
  maxDegree 4
  minDegree 1
  meanDegree 2.6666666666666665
  density 0.3333333333333333
  graphCliqueNumber 5
  node [
    id 0
    label "olle"
    origin "text"
  ]
  node [
    id 1
    label "hansson"
    origin "text"
  ]
  node [
    id 2
    label "Olle"
  ]
  node [
    id 3
    label "Hansson"
  ]
  node [
    id 4
    label "mistrzostwo"
  ]
  node [
    id 5
    label "&#347;wiat"
  ]
  node [
    id 6
    label "wyspa"
  ]
  node [
    id 7
    label "narciarstwo"
  ]
  node [
    id 8
    label "klasyczny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 4
    target 8
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 7
    target 8
  ]
]
