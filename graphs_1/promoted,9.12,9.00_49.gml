graph [
  maxDegree 28
  minDegree 1
  meanDegree 1.9838709677419355
  density 0.016129032258064516
  graphCliqueNumber 2
  node [
    id 0
    label "amerykanin"
    origin "text"
  ]
  node [
    id 1
    label "lato"
    origin "text"
  ]
  node [
    id 2
    label "pracowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "nad"
    origin "text"
  ]
  node [
    id 4
    label "projekt"
    origin "text"
  ]
  node [
    id 5
    label "hipersoniczny"
    origin "text"
  ]
  node [
    id 6
    label "pocisk"
    origin "text"
  ]
  node [
    id 7
    label "zdolny"
    origin "text"
  ]
  node [
    id 8
    label "przeby&#263;"
    origin "text"
  ]
  node [
    id 9
    label "ca&#322;y"
    origin "text"
  ]
  node [
    id 10
    label "kontynent"
    origin "text"
  ]
  node [
    id 11
    label "mgnienie"
    origin "text"
  ]
  node [
    id 12
    label "oko"
    origin "text"
  ]
  node [
    id 13
    label "pora_roku"
  ]
  node [
    id 14
    label "endeavor"
  ]
  node [
    id 15
    label "funkcjonowa&#263;"
  ]
  node [
    id 16
    label "dobiega&#263;_si&#281;"
  ]
  node [
    id 17
    label "mie&#263;_miejsce"
  ]
  node [
    id 18
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 19
    label "dzia&#322;a&#263;"
  ]
  node [
    id 20
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 21
    label "work"
  ]
  node [
    id 22
    label "bangla&#263;"
  ]
  node [
    id 23
    label "do"
  ]
  node [
    id 24
    label "maszyna"
  ]
  node [
    id 25
    label "tryb"
  ]
  node [
    id 26
    label "dziama&#263;"
  ]
  node [
    id 27
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 28
    label "praca"
  ]
  node [
    id 29
    label "podejmowa&#263;"
  ]
  node [
    id 30
    label "dokument"
  ]
  node [
    id 31
    label "device"
  ]
  node [
    id 32
    label "program_u&#380;ytkowy"
  ]
  node [
    id 33
    label "intencja"
  ]
  node [
    id 34
    label "agreement"
  ]
  node [
    id 35
    label "pomys&#322;"
  ]
  node [
    id 36
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 37
    label "plan"
  ]
  node [
    id 38
    label "dokumentacja"
  ]
  node [
    id 39
    label "nadd&#378;wi&#281;kowo"
  ]
  node [
    id 40
    label "szybki"
  ]
  node [
    id 41
    label "g&#322;owica"
  ]
  node [
    id 42
    label "kulka"
  ]
  node [
    id 43
    label "amunicja"
  ]
  node [
    id 44
    label "przenie&#347;&#263;"
  ]
  node [
    id 45
    label "trafianie"
  ]
  node [
    id 46
    label "przenosi&#263;"
  ]
  node [
    id 47
    label "trafia&#263;"
  ]
  node [
    id 48
    label "trafienie"
  ]
  node [
    id 49
    label "przeniesienie"
  ]
  node [
    id 50
    label "&#322;adunek_bojowy"
  ]
  node [
    id 51
    label "trafi&#263;"
  ]
  node [
    id 52
    label "bro&#324;"
  ]
  node [
    id 53
    label "prochownia"
  ]
  node [
    id 54
    label "rdze&#324;"
  ]
  node [
    id 55
    label "przenoszenie"
  ]
  node [
    id 56
    label "dobry"
  ]
  node [
    id 57
    label "sk&#322;onny"
  ]
  node [
    id 58
    label "zdolnie"
  ]
  node [
    id 59
    label "odby&#263;"
  ]
  node [
    id 60
    label "traversal"
  ]
  node [
    id 61
    label "zaatakowa&#263;"
  ]
  node [
    id 62
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 63
    label "prze&#380;y&#263;"
  ]
  node [
    id 64
    label "overwhelm"
  ]
  node [
    id 65
    label "du&#380;y"
  ]
  node [
    id 66
    label "jedyny"
  ]
  node [
    id 67
    label "kompletny"
  ]
  node [
    id 68
    label "zdr&#243;w"
  ]
  node [
    id 69
    label "&#380;ywy"
  ]
  node [
    id 70
    label "ca&#322;o"
  ]
  node [
    id 71
    label "pe&#322;ny"
  ]
  node [
    id 72
    label "calu&#347;ko"
  ]
  node [
    id 73
    label "podobny"
  ]
  node [
    id 74
    label "Stary_&#346;wiat"
  ]
  node [
    id 75
    label "Antarktyda"
  ]
  node [
    id 76
    label "Germania"
  ]
  node [
    id 77
    label "Dunaj"
  ]
  node [
    id 78
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 79
    label "Europa_Zachodnia"
  ]
  node [
    id 80
    label "Afryka"
  ]
  node [
    id 81
    label "Ameryka"
  ]
  node [
    id 82
    label "Azja"
  ]
  node [
    id 83
    label "Europa_Wschodnia"
  ]
  node [
    id 84
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 85
    label "epejroforeza"
  ]
  node [
    id 86
    label "Ameryka_Centralna"
  ]
  node [
    id 87
    label "Australia"
  ]
  node [
    id 88
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 89
    label "Moza"
  ]
  node [
    id 90
    label "blok_kontynentalny"
  ]
  node [
    id 91
    label "Eurazja"
  ]
  node [
    id 92
    label "l&#261;d"
  ]
  node [
    id 93
    label "palearktyka"
  ]
  node [
    id 94
    label "Europa"
  ]
  node [
    id 95
    label "Ren"
  ]
  node [
    id 96
    label "time"
  ]
  node [
    id 97
    label "wypowied&#378;"
  ]
  node [
    id 98
    label "siniec"
  ]
  node [
    id 99
    label "uwaga"
  ]
  node [
    id 100
    label "rzecz"
  ]
  node [
    id 101
    label "&#380;y&#322;a_&#347;rodkowa_siatk&#243;wki"
  ]
  node [
    id 102
    label "powieka"
  ]
  node [
    id 103
    label "oczy"
  ]
  node [
    id 104
    label "&#347;lepko"
  ]
  node [
    id 105
    label "ga&#322;ka_oczna"
  ]
  node [
    id 106
    label "ser_&#380;&#243;&#322;ty"
  ]
  node [
    id 107
    label "&#347;lepie"
  ]
  node [
    id 108
    label "twarz"
  ]
  node [
    id 109
    label "organ"
  ]
  node [
    id 110
    label "worek_spoj&#243;wkowy"
  ]
  node [
    id 111
    label "nerw_wzrokowy"
  ]
  node [
    id 112
    label "wzrok"
  ]
  node [
    id 113
    label "&#378;renica"
  ]
  node [
    id 114
    label "spoj&#243;wka"
  ]
  node [
    id 115
    label "narz&#261;d_&#322;zowy"
  ]
  node [
    id 116
    label "nab&#322;onek_barwnikowy"
  ]
  node [
    id 117
    label "kaprawie&#263;"
  ]
  node [
    id 118
    label "kaprawienie"
  ]
  node [
    id 119
    label "spojrzenie"
  ]
  node [
    id 120
    label "net"
  ]
  node [
    id 121
    label "r&#261;bek_rog&#243;wki"
  ]
  node [
    id 122
    label "coloboma"
  ]
  node [
    id 123
    label "ros&#243;&#322;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 12
    target 107
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 12
    target 109
  ]
  edge [
    source 12
    target 110
  ]
  edge [
    source 12
    target 111
  ]
  edge [
    source 12
    target 112
  ]
  edge [
    source 12
    target 113
  ]
  edge [
    source 12
    target 114
  ]
  edge [
    source 12
    target 115
  ]
  edge [
    source 12
    target 116
  ]
  edge [
    source 12
    target 117
  ]
  edge [
    source 12
    target 118
  ]
  edge [
    source 12
    target 119
  ]
  edge [
    source 12
    target 120
  ]
  edge [
    source 12
    target 121
  ]
  edge [
    source 12
    target 122
  ]
  edge [
    source 12
    target 123
  ]
]
