graph [
  maxDegree 26
  minDegree 1
  meanDegree 2.2720588235294117
  density 0.008383980898632517
  graphCliqueNumber 4
  node [
    id 0
    label "czym"
    origin "text"
  ]
  node [
    id 1
    label "by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "wzorzec"
    origin "text"
  ]
  node [
    id 3
    label "projektowy"
    origin "text"
  ]
  node [
    id 4
    label "in&#380;ynieria"
    origin "text"
  ]
  node [
    id 5
    label "oprogramowanie"
    origin "text"
  ]
  node [
    id 6
    label "ang"
    origin "text"
  ]
  node [
    id 7
    label "design"
    origin "text"
  ]
  node [
    id 8
    label "pattern"
    origin "text"
  ]
  node [
    id 9
    label "uniwersalny"
    origin "text"
  ]
  node [
    id 10
    label "sprawdzi&#263;"
    origin "text"
  ]
  node [
    id 11
    label "rozwi&#261;zanie"
    origin "text"
  ]
  node [
    id 12
    label "cz&#281;sto"
    origin "text"
  ]
  node [
    id 13
    label "pojawia&#263;"
    origin "text"
  ]
  node [
    id 14
    label "si&#281;"
    origin "text"
  ]
  node [
    id 15
    label "problem"
    origin "text"
  ]
  node [
    id 16
    label "ciekawy"
    origin "text"
  ]
  node [
    id 17
    label "definicja"
    origin "text"
  ]
  node [
    id 18
    label "poda&#263;"
    origin "text"
  ]
  node [
    id 19
    label "cent"
    origin "text"
  ]
  node [
    id 20
    label "alexander"
    origin "text"
  ]
  node [
    id 21
    label "sekunda"
    origin "text"
  ]
  node [
    id 22
    label "ishikawa"
    origin "text"
  ]
  node [
    id 23
    label "metr"
    origin "text"
  ]
  node [
    id 24
    label "silverstein"
    origin "text"
  ]
  node [
    id 25
    label "language"
    origin "text"
  ]
  node [
    id 26
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 27
    label "opisywa&#263;"
    origin "text"
  ]
  node [
    id 28
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 29
    label "ci&#261;gle"
    origin "text"
  ]
  node [
    id 30
    label "nasz"
    origin "text"
  ]
  node [
    id 31
    label "dziedzina"
    origin "text"
  ]
  node [
    id 32
    label "nast&#281;pnie"
    origin "text"
  ]
  node [
    id 33
    label "okre&#347;la&#263;"
    origin "text"
  ]
  node [
    id 34
    label "zasadniczy"
    origin "text"
  ]
  node [
    id 35
    label "cz&#281;&#347;&#263;"
    origin "text"
  ]
  node [
    id 36
    label "taki"
    origin "text"
  ]
  node [
    id 37
    label "spos&#243;b"
    origin "text"
  ]
  node [
    id 38
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 39
    label "zastosowa&#263;"
    origin "text"
  ]
  node [
    id 40
    label "nawet"
    origin "text"
  ]
  node [
    id 41
    label "milion"
    origin "text"
  ]
  node [
    id 42
    label "raz"
    origin "text"
  ]
  node [
    id 43
    label "razem"
    origin "text"
  ]
  node [
    id 44
    label "nieco"
    origin "text"
  ]
  node [
    id 45
    label "inny"
    origin "text"
  ]
  node [
    id 46
    label "pe&#322;nia"
    origin "text"
  ]
  node [
    id 47
    label "oddawa&#263;"
    origin "text"
  ]
  node [
    id 48
    label "idea"
    origin "text"
  ]
  node [
    id 49
    label "tylko"
    origin "text"
  ]
  node [
    id 50
    label "informatyka"
    origin "text"
  ]
  node [
    id 51
    label "si&#281;ga&#263;"
  ]
  node [
    id 52
    label "trwa&#263;"
  ]
  node [
    id 53
    label "obecno&#347;&#263;"
  ]
  node [
    id 54
    label "stan"
  ]
  node [
    id 55
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 56
    label "stand"
  ]
  node [
    id 57
    label "mie&#263;_miejsce"
  ]
  node [
    id 58
    label "uczestniczy&#263;"
  ]
  node [
    id 59
    label "chodzi&#263;"
  ]
  node [
    id 60
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 61
    label "equal"
  ]
  node [
    id 62
    label "cz&#322;owiek"
  ]
  node [
    id 63
    label "ruch"
  ]
  node [
    id 64
    label "punkt_odniesienia"
  ]
  node [
    id 65
    label "ideal"
  ]
  node [
    id 66
    label "mildew"
  ]
  node [
    id 67
    label "wst&#281;pny"
  ]
  node [
    id 68
    label "fortyfikacja"
  ]
  node [
    id 69
    label "biologia_syntetyczna"
  ]
  node [
    id 70
    label "nauka"
  ]
  node [
    id 71
    label "robotyka"
  ]
  node [
    id 72
    label "program"
  ]
  node [
    id 73
    label "zbi&#243;r"
  ]
  node [
    id 74
    label "reengineering"
  ]
  node [
    id 75
    label "wygl&#261;d"
  ]
  node [
    id 76
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 77
    label "uniwersalnie"
  ]
  node [
    id 78
    label "generalny"
  ]
  node [
    id 79
    label "wszechstronnie"
  ]
  node [
    id 80
    label "examine"
  ]
  node [
    id 81
    label "zrobi&#263;"
  ]
  node [
    id 82
    label "wynik"
  ]
  node [
    id 83
    label "wyj&#347;cie"
  ]
  node [
    id 84
    label "spe&#322;nienie"
  ]
  node [
    id 85
    label "cesarskie_ci&#281;cie"
  ]
  node [
    id 86
    label "po&#322;o&#380;na"
  ]
  node [
    id 87
    label "proces_fizjologiczny"
  ]
  node [
    id 88
    label "przestanie"
  ]
  node [
    id 89
    label "marc&#243;wka"
  ]
  node [
    id 90
    label "usuni&#281;cie"
  ]
  node [
    id 91
    label "uniewa&#380;nienie"
  ]
  node [
    id 92
    label "pomys&#322;"
  ]
  node [
    id 93
    label "birth"
  ]
  node [
    id 94
    label "wymy&#347;lenie"
  ]
  node [
    id 95
    label "po&#322;&#243;g"
  ]
  node [
    id 96
    label "szok_poporodowy"
  ]
  node [
    id 97
    label "event"
  ]
  node [
    id 98
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 99
    label "rozwi&#261;zywanie_si&#281;"
  ]
  node [
    id 100
    label "dula"
  ]
  node [
    id 101
    label "cz&#281;stokro&#263;"
  ]
  node [
    id 102
    label "cz&#281;sty"
  ]
  node [
    id 103
    label "trudno&#347;&#263;"
  ]
  node [
    id 104
    label "sprawa"
  ]
  node [
    id 105
    label "ambaras"
  ]
  node [
    id 106
    label "problemat"
  ]
  node [
    id 107
    label "pierepa&#322;ka"
  ]
  node [
    id 108
    label "obstruction"
  ]
  node [
    id 109
    label "problematyka"
  ]
  node [
    id 110
    label "jajko_Kolumba"
  ]
  node [
    id 111
    label "subiekcja"
  ]
  node [
    id 112
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 113
    label "swoisty"
  ]
  node [
    id 114
    label "interesowanie"
  ]
  node [
    id 115
    label "nietuzinkowy"
  ]
  node [
    id 116
    label "ciekawie"
  ]
  node [
    id 117
    label "indagator"
  ]
  node [
    id 118
    label "interesuj&#261;cy"
  ]
  node [
    id 119
    label "dziwny"
  ]
  node [
    id 120
    label "intryguj&#261;cy"
  ]
  node [
    id 121
    label "ch&#281;tny"
  ]
  node [
    id 122
    label "definiens"
  ]
  node [
    id 123
    label "definiendum"
  ]
  node [
    id 124
    label "obja&#347;nienie"
  ]
  node [
    id 125
    label "definition"
  ]
  node [
    id 126
    label "tenis"
  ]
  node [
    id 127
    label "da&#263;"
  ]
  node [
    id 128
    label "siatk&#243;wka"
  ]
  node [
    id 129
    label "introduce"
  ]
  node [
    id 130
    label "jedzenie"
  ]
  node [
    id 131
    label "zaserwowa&#263;"
  ]
  node [
    id 132
    label "give"
  ]
  node [
    id 133
    label "ustawi&#263;"
  ]
  node [
    id 134
    label "zagra&#263;"
  ]
  node [
    id 135
    label "supply"
  ]
  node [
    id 136
    label "nafaszerowa&#263;"
  ]
  node [
    id 137
    label "poinformowa&#263;"
  ]
  node [
    id 138
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 139
    label "moneta"
  ]
  node [
    id 140
    label "minuta"
  ]
  node [
    id 141
    label "tercja"
  ]
  node [
    id 142
    label "milisekunda"
  ]
  node [
    id 143
    label "nanosekunda"
  ]
  node [
    id 144
    label "uk&#322;ad_SI"
  ]
  node [
    id 145
    label "mikrosekunda"
  ]
  node [
    id 146
    label "time"
  ]
  node [
    id 147
    label "jednostka_czasu"
  ]
  node [
    id 148
    label "jednostka"
  ]
  node [
    id 149
    label "meter"
  ]
  node [
    id 150
    label "decymetr"
  ]
  node [
    id 151
    label "megabyte"
  ]
  node [
    id 152
    label "plon"
  ]
  node [
    id 153
    label "metrum"
  ]
  node [
    id 154
    label "dekametr"
  ]
  node [
    id 155
    label "jednostka_powierzchni"
  ]
  node [
    id 156
    label "literaturoznawstwo"
  ]
  node [
    id 157
    label "wiersz"
  ]
  node [
    id 158
    label "gigametr"
  ]
  node [
    id 159
    label "miara"
  ]
  node [
    id 160
    label "nauczyciel"
  ]
  node [
    id 161
    label "kilometr_kwadratowy"
  ]
  node [
    id 162
    label "jednostka_metryczna"
  ]
  node [
    id 163
    label "jednostka_masy"
  ]
  node [
    id 164
    label "centymetr_kwadratowy"
  ]
  node [
    id 165
    label "jaki&#347;"
  ]
  node [
    id 166
    label "zapoznawa&#263;"
  ]
  node [
    id 167
    label "represent"
  ]
  node [
    id 168
    label "nieprzerwanie"
  ]
  node [
    id 169
    label "ci&#261;g&#322;y"
  ]
  node [
    id 170
    label "stale"
  ]
  node [
    id 171
    label "czyj&#347;"
  ]
  node [
    id 172
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 173
    label "zakres"
  ]
  node [
    id 174
    label "bezdro&#380;e"
  ]
  node [
    id 175
    label "funkcja"
  ]
  node [
    id 176
    label "sfera"
  ]
  node [
    id 177
    label "poddzia&#322;"
  ]
  node [
    id 178
    label "kolejny"
  ]
  node [
    id 179
    label "signify"
  ]
  node [
    id 180
    label "powodowa&#263;"
  ]
  node [
    id 181
    label "decydowa&#263;"
  ]
  node [
    id 182
    label "style"
  ]
  node [
    id 183
    label "og&#243;lny"
  ]
  node [
    id 184
    label "g&#322;&#243;wny"
  ]
  node [
    id 185
    label "zasadniczo"
  ]
  node [
    id 186
    label "surowy"
  ]
  node [
    id 187
    label "whole"
  ]
  node [
    id 188
    label "Rzym_Zachodni"
  ]
  node [
    id 189
    label "element"
  ]
  node [
    id 190
    label "ilo&#347;&#263;"
  ]
  node [
    id 191
    label "urz&#261;dzenie"
  ]
  node [
    id 192
    label "Rzym_Wschodni"
  ]
  node [
    id 193
    label "okre&#347;lony"
  ]
  node [
    id 194
    label "model"
  ]
  node [
    id 195
    label "tryb"
  ]
  node [
    id 196
    label "narz&#281;dzie"
  ]
  node [
    id 197
    label "nature"
  ]
  node [
    id 198
    label "free"
  ]
  node [
    id 199
    label "u&#380;y&#263;"
  ]
  node [
    id 200
    label "liczba"
  ]
  node [
    id 201
    label "miljon"
  ]
  node [
    id 202
    label "ba&#324;ka"
  ]
  node [
    id 203
    label "chwila"
  ]
  node [
    id 204
    label "uderzenie"
  ]
  node [
    id 205
    label "cios"
  ]
  node [
    id 206
    label "&#322;&#261;cznie"
  ]
  node [
    id 207
    label "inaczej"
  ]
  node [
    id 208
    label "r&#243;&#380;ny"
  ]
  node [
    id 209
    label "inszy"
  ]
  node [
    id 210
    label "osobno"
  ]
  node [
    id 211
    label "faza_Ksi&#281;&#380;yca"
  ]
  node [
    id 212
    label "szczyt"
  ]
  node [
    id 213
    label "za&#322;atwia&#263;_si&#281;"
  ]
  node [
    id 214
    label "render"
  ]
  node [
    id 215
    label "dawa&#263;"
  ]
  node [
    id 216
    label "przedstawia&#263;"
  ]
  node [
    id 217
    label "blurt_out"
  ]
  node [
    id 218
    label "impart"
  ]
  node [
    id 219
    label "sacrifice"
  ]
  node [
    id 220
    label "surrender"
  ]
  node [
    id 221
    label "reflect"
  ]
  node [
    id 222
    label "dostarcza&#263;"
  ]
  node [
    id 223
    label "umieszcza&#263;"
  ]
  node [
    id 224
    label "deliver"
  ]
  node [
    id 225
    label "odst&#281;powa&#263;"
  ]
  node [
    id 226
    label "sprzedawa&#263;"
  ]
  node [
    id 227
    label "odpowiada&#263;"
  ]
  node [
    id 228
    label "przekazywa&#263;"
  ]
  node [
    id 229
    label "byt"
  ]
  node [
    id 230
    label "istota"
  ]
  node [
    id 231
    label "ideologia"
  ]
  node [
    id 232
    label "intelekt"
  ]
  node [
    id 233
    label "Kant"
  ]
  node [
    id 234
    label "poj&#281;cie"
  ]
  node [
    id 235
    label "cel"
  ]
  node [
    id 236
    label "p&#322;&#243;d"
  ]
  node [
    id 237
    label "ideacja"
  ]
  node [
    id 238
    label "HP"
  ]
  node [
    id 239
    label "dost&#281;p"
  ]
  node [
    id 240
    label "sztuczna_inteligencja"
  ]
  node [
    id 241
    label "kryptologia"
  ]
  node [
    id 242
    label "zamek"
  ]
  node [
    id 243
    label "baza_danych"
  ]
  node [
    id 244
    label "przedmiot"
  ]
  node [
    id 245
    label "kierunek"
  ]
  node [
    id 246
    label "artefakt"
  ]
  node [
    id 247
    label "przetwarzanie_j&#281;zyka_naturalnego"
  ]
  node [
    id 248
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 249
    label "przetwarzanie_informacji"
  ]
  node [
    id 250
    label "dziedzina_informatyki"
  ]
  node [
    id 251
    label "gramatyka_formalna"
  ]
  node [
    id 252
    label "infa"
  ]
  node [
    id 253
    label "c&#243;rka"
  ]
  node [
    id 254
    label "Alexander"
  ]
  node [
    id 255
    label "m&#281;ski"
  ]
  node [
    id 256
    label "Silverstein"
  ]
  node [
    id 257
    label "syn"
  ]
  node [
    id 258
    label "Ishikawa"
  ]
  node [
    id 259
    label "albo"
  ]
  node [
    id 260
    label "Pattern"
  ]
  node [
    id 261
    label "Language"
  ]
  node [
    id 262
    label "Christopher"
  ]
  node [
    id 263
    label "kent"
  ]
  node [
    id 264
    label "Beck"
  ]
  node [
    id 265
    label "Desing"
  ]
  node [
    id 266
    label "banda"
  ]
  node [
    id 267
    label "cztery"
  ]
  node [
    id 268
    label "gang"
  ]
  node [
    id 269
    label "of"
  ]
  node [
    id 270
    label "Four"
  ]
  node [
    id 271
    label "bando"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 21
  ]
  edge [
    source 7
    target 31
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 24
  ]
  edge [
    source 8
    target 25
  ]
  edge [
    source 8
    target 22
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 35
  ]
  edge [
    source 11
    target 36
  ]
  edge [
    source 11
    target 82
  ]
  edge [
    source 11
    target 83
  ]
  edge [
    source 11
    target 84
  ]
  edge [
    source 11
    target 85
  ]
  edge [
    source 11
    target 86
  ]
  edge [
    source 11
    target 87
  ]
  edge [
    source 11
    target 88
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 11
    target 92
  ]
  edge [
    source 11
    target 93
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 11
    target 98
  ]
  edge [
    source 11
    target 37
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 11
    target 100
  ]
  edge [
    source 11
    target 28
  ]
  edge [
    source 11
    target 16
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 29
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 30
  ]
  edge [
    source 15
    target 27
  ]
  edge [
    source 15
    target 28
  ]
  edge [
    source 15
    target 103
  ]
  edge [
    source 15
    target 104
  ]
  edge [
    source 15
    target 105
  ]
  edge [
    source 15
    target 106
  ]
  edge [
    source 15
    target 107
  ]
  edge [
    source 15
    target 108
  ]
  edge [
    source 15
    target 109
  ]
  edge [
    source 15
    target 110
  ]
  edge [
    source 15
    target 111
  ]
  edge [
    source 15
    target 112
  ]
  edge [
    source 15
    target 44
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 113
  ]
  edge [
    source 16
    target 62
  ]
  edge [
    source 16
    target 114
  ]
  edge [
    source 16
    target 115
  ]
  edge [
    source 16
    target 116
  ]
  edge [
    source 16
    target 117
  ]
  edge [
    source 16
    target 118
  ]
  edge [
    source 16
    target 119
  ]
  edge [
    source 16
    target 120
  ]
  edge [
    source 16
    target 121
  ]
  edge [
    source 17
    target 37
  ]
  edge [
    source 17
    target 46
  ]
  edge [
    source 17
    target 122
  ]
  edge [
    source 17
    target 123
  ]
  edge [
    source 17
    target 124
  ]
  edge [
    source 17
    target 125
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 126
  ]
  edge [
    source 18
    target 127
  ]
  edge [
    source 18
    target 128
  ]
  edge [
    source 18
    target 129
  ]
  edge [
    source 18
    target 130
  ]
  edge [
    source 18
    target 131
  ]
  edge [
    source 18
    target 132
  ]
  edge [
    source 18
    target 133
  ]
  edge [
    source 18
    target 134
  ]
  edge [
    source 18
    target 135
  ]
  edge [
    source 18
    target 136
  ]
  edge [
    source 18
    target 137
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 138
  ]
  edge [
    source 19
    target 139
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 140
  ]
  edge [
    source 21
    target 141
  ]
  edge [
    source 21
    target 142
  ]
  edge [
    source 21
    target 143
  ]
  edge [
    source 21
    target 144
  ]
  edge [
    source 21
    target 145
  ]
  edge [
    source 21
    target 146
  ]
  edge [
    source 21
    target 147
  ]
  edge [
    source 21
    target 148
  ]
  edge [
    source 21
    target 31
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 149
  ]
  edge [
    source 23
    target 150
  ]
  edge [
    source 23
    target 151
  ]
  edge [
    source 23
    target 152
  ]
  edge [
    source 23
    target 153
  ]
  edge [
    source 23
    target 154
  ]
  edge [
    source 23
    target 155
  ]
  edge [
    source 23
    target 144
  ]
  edge [
    source 23
    target 156
  ]
  edge [
    source 23
    target 157
  ]
  edge [
    source 23
    target 158
  ]
  edge [
    source 23
    target 159
  ]
  edge [
    source 23
    target 160
  ]
  edge [
    source 23
    target 161
  ]
  edge [
    source 23
    target 162
  ]
  edge [
    source 23
    target 163
  ]
  edge [
    source 23
    target 164
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 42
  ]
  edge [
    source 26
    target 43
  ]
  edge [
    source 26
    target 165
  ]
  edge [
    source 27
    target 166
  ]
  edge [
    source 27
    target 167
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 168
  ]
  edge [
    source 29
    target 169
  ]
  edge [
    source 29
    target 170
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 171
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 172
  ]
  edge [
    source 31
    target 173
  ]
  edge [
    source 31
    target 174
  ]
  edge [
    source 31
    target 73
  ]
  edge [
    source 31
    target 175
  ]
  edge [
    source 31
    target 176
  ]
  edge [
    source 31
    target 177
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 178
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 179
  ]
  edge [
    source 33
    target 180
  ]
  edge [
    source 33
    target 181
  ]
  edge [
    source 33
    target 182
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 183
  ]
  edge [
    source 34
    target 184
  ]
  edge [
    source 34
    target 185
  ]
  edge [
    source 34
    target 186
  ]
  edge [
    source 35
    target 187
  ]
  edge [
    source 35
    target 188
  ]
  edge [
    source 35
    target 189
  ]
  edge [
    source 35
    target 190
  ]
  edge [
    source 35
    target 191
  ]
  edge [
    source 35
    target 192
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 193
  ]
  edge [
    source 36
    target 165
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 45
  ]
  edge [
    source 37
    target 194
  ]
  edge [
    source 37
    target 73
  ]
  edge [
    source 37
    target 195
  ]
  edge [
    source 37
    target 196
  ]
  edge [
    source 37
    target 197
  ]
  edge [
    source 38
    target 198
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 199
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 200
  ]
  edge [
    source 41
    target 201
  ]
  edge [
    source 41
    target 202
  ]
  edge [
    source 42
    target 203
  ]
  edge [
    source 42
    target 204
  ]
  edge [
    source 42
    target 205
  ]
  edge [
    source 42
    target 146
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 206
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 45
    target 178
  ]
  edge [
    source 45
    target 207
  ]
  edge [
    source 45
    target 208
  ]
  edge [
    source 45
    target 209
  ]
  edge [
    source 45
    target 210
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 173
  ]
  edge [
    source 46
    target 211
  ]
  edge [
    source 46
    target 212
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 213
  ]
  edge [
    source 47
    target 214
  ]
  edge [
    source 47
    target 215
  ]
  edge [
    source 47
    target 216
  ]
  edge [
    source 47
    target 217
  ]
  edge [
    source 47
    target 218
  ]
  edge [
    source 47
    target 219
  ]
  edge [
    source 47
    target 220
  ]
  edge [
    source 47
    target 221
  ]
  edge [
    source 47
    target 222
  ]
  edge [
    source 47
    target 132
  ]
  edge [
    source 47
    target 223
  ]
  edge [
    source 47
    target 224
  ]
  edge [
    source 47
    target 225
  ]
  edge [
    source 47
    target 226
  ]
  edge [
    source 47
    target 227
  ]
  edge [
    source 47
    target 228
  ]
  edge [
    source 48
    target 229
  ]
  edge [
    source 48
    target 230
  ]
  edge [
    source 48
    target 231
  ]
  edge [
    source 48
    target 232
  ]
  edge [
    source 48
    target 233
  ]
  edge [
    source 48
    target 92
  ]
  edge [
    source 48
    target 234
  ]
  edge [
    source 48
    target 235
  ]
  edge [
    source 48
    target 236
  ]
  edge [
    source 48
    target 237
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 50
    target 238
  ]
  edge [
    source 50
    target 239
  ]
  edge [
    source 50
    target 240
  ]
  edge [
    source 50
    target 241
  ]
  edge [
    source 50
    target 242
  ]
  edge [
    source 50
    target 243
  ]
  edge [
    source 50
    target 244
  ]
  edge [
    source 50
    target 245
  ]
  edge [
    source 50
    target 246
  ]
  edge [
    source 50
    target 72
  ]
  edge [
    source 50
    target 247
  ]
  edge [
    source 50
    target 248
  ]
  edge [
    source 50
    target 249
  ]
  edge [
    source 50
    target 250
  ]
  edge [
    source 50
    target 251
  ]
  edge [
    source 50
    target 252
  ]
  edge [
    source 253
    target 254
  ]
  edge [
    source 254
    target 262
  ]
  edge [
    source 255
    target 256
  ]
  edge [
    source 257
    target 258
  ]
  edge [
    source 259
    target 260
  ]
  edge [
    source 259
    target 261
  ]
  edge [
    source 260
    target 261
  ]
  edge [
    source 260
    target 265
  ]
  edge [
    source 263
    target 264
  ]
  edge [
    source 266
    target 267
  ]
  edge [
    source 267
    target 271
  ]
  edge [
    source 268
    target 269
  ]
  edge [
    source 268
    target 270
  ]
  edge [
    source 269
    target 270
  ]
]
