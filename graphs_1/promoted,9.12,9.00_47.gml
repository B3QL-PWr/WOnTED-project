graph [
  maxDegree 12
  minDegree 1
  meanDegree 1.84
  density 0.07666666666666666
  graphCliqueNumber 2
  node [
    id 0
    label "niez&#322;y"
    origin "text"
  ]
  node [
    id 1
    label "kawa&#322;ek"
    origin "text"
  ]
  node [
    id 2
    label "quake"
    origin "text"
  ]
  node [
    id 3
    label "korzystny"
  ]
  node [
    id 4
    label "dobrze"
  ]
  node [
    id 5
    label "nie&#378;le"
  ]
  node [
    id 6
    label "pozytywny"
  ]
  node [
    id 7
    label "intensywny"
  ]
  node [
    id 8
    label "spory"
  ]
  node [
    id 9
    label "udolny"
  ]
  node [
    id 10
    label "niczegowaty"
  ]
  node [
    id 11
    label "skuteczny"
  ]
  node [
    id 12
    label "&#347;mieszny"
  ]
  node [
    id 13
    label "nieszpetny"
  ]
  node [
    id 14
    label "podp&#322;ywa&#263;"
  ]
  node [
    id 15
    label "podp&#322;ywanie"
  ]
  node [
    id 16
    label "plot"
  ]
  node [
    id 17
    label "podp&#322;yn&#261;&#263;"
  ]
  node [
    id 18
    label "piece"
  ]
  node [
    id 19
    label "kawa&#322;"
  ]
  node [
    id 20
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 21
    label "utw&#243;r"
  ]
  node [
    id 22
    label "podp&#322;yni&#281;cie"
  ]
  node [
    id 23
    label "Quake"
  ]
  node [
    id 24
    label "II"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 23
    target 24
  ]
]
