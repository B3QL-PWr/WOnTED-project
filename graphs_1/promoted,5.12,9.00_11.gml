graph [
  maxDegree 20
  minDegree 1
  meanDegree 1.9574468085106382
  density 0.0425531914893617
  graphCliqueNumber 2
  node [
    id 0
    label "plan"
    origin "text"
  ]
  node [
    id 1
    label "by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "dobry"
    origin "text"
  ]
  node [
    id 3
    label "device"
  ]
  node [
    id 4
    label "model"
  ]
  node [
    id 5
    label "wytw&#243;r"
  ]
  node [
    id 6
    label "obraz"
  ]
  node [
    id 7
    label "przestrze&#324;"
  ]
  node [
    id 8
    label "dekoracja"
  ]
  node [
    id 9
    label "intencja"
  ]
  node [
    id 10
    label "agreement"
  ]
  node [
    id 11
    label "pomys&#322;"
  ]
  node [
    id 12
    label "punkt"
  ]
  node [
    id 13
    label "miejsce_pracy"
  ]
  node [
    id 14
    label "perspektywa"
  ]
  node [
    id 15
    label "rysunek"
  ]
  node [
    id 16
    label "reprezentacja"
  ]
  node [
    id 17
    label "si&#281;ga&#263;"
  ]
  node [
    id 18
    label "trwa&#263;"
  ]
  node [
    id 19
    label "obecno&#347;&#263;"
  ]
  node [
    id 20
    label "stan"
  ]
  node [
    id 21
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 22
    label "stand"
  ]
  node [
    id 23
    label "mie&#263;_miejsce"
  ]
  node [
    id 24
    label "uczestniczy&#263;"
  ]
  node [
    id 25
    label "chodzi&#263;"
  ]
  node [
    id 26
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 27
    label "equal"
  ]
  node [
    id 28
    label "pomy&#347;lny"
  ]
  node [
    id 29
    label "skuteczny"
  ]
  node [
    id 30
    label "moralny"
  ]
  node [
    id 31
    label "korzystny"
  ]
  node [
    id 32
    label "odpowiedni"
  ]
  node [
    id 33
    label "zwrot"
  ]
  node [
    id 34
    label "dobrze"
  ]
  node [
    id 35
    label "pozytywny"
  ]
  node [
    id 36
    label "grzeczny"
  ]
  node [
    id 37
    label "powitanie"
  ]
  node [
    id 38
    label "mi&#322;y"
  ]
  node [
    id 39
    label "dobroczynny"
  ]
  node [
    id 40
    label "pos&#322;uszny"
  ]
  node [
    id 41
    label "ca&#322;y"
  ]
  node [
    id 42
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 43
    label "czw&#243;rka"
  ]
  node [
    id 44
    label "spokojny"
  ]
  node [
    id 45
    label "&#347;mieszny"
  ]
  node [
    id 46
    label "drogi"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
]
