graph [
  maxDegree 9
  minDegree 1
  meanDegree 1.9333333333333333
  density 0.06666666666666667
  graphCliqueNumber 2
  node [
    id 0
    label "obowi&#261;zek"
    origin "text"
  ]
  node [
    id 1
    label "uprawnienie"
    origin "text"
  ]
  node [
    id 2
    label "stra&#380;nik"
    origin "text"
  ]
  node [
    id 3
    label "stra&#380;"
    origin "text"
  ]
  node [
    id 4
    label "miejski"
    origin "text"
  ]
  node [
    id 5
    label "powinno&#347;&#263;"
  ]
  node [
    id 6
    label "zadanie"
  ]
  node [
    id 7
    label "wym&#243;g"
  ]
  node [
    id 8
    label "duty"
  ]
  node [
    id 9
    label "obarczy&#263;"
  ]
  node [
    id 10
    label "dokument"
  ]
  node [
    id 11
    label "czynno&#347;&#263;"
  ]
  node [
    id 12
    label "spowodowanie"
  ]
  node [
    id 13
    label "title"
  ]
  node [
    id 14
    label "law"
  ]
  node [
    id 15
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 16
    label "authorization"
  ]
  node [
    id 17
    label "pracownik"
  ]
  node [
    id 18
    label "Cerber"
  ]
  node [
    id 19
    label "ochrona"
  ]
  node [
    id 20
    label "s&#322;u&#380;ba_publiczna"
  ]
  node [
    id 21
    label "rota"
  ]
  node [
    id 22
    label "wedeta"
  ]
  node [
    id 23
    label "posterunek"
  ]
  node [
    id 24
    label "s&#322;u&#380;ba"
  ]
  node [
    id 25
    label "stra&#380;_ogniowa"
  ]
  node [
    id 26
    label "miejsko"
  ]
  node [
    id 27
    label "miastowy"
  ]
  node [
    id 28
    label "typowy"
  ]
  node [
    id 29
    label "publiczny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
]
