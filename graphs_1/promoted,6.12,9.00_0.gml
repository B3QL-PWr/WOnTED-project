graph [
  maxDegree 8
  minDegree 1
  meanDegree 1.8823529411764706
  density 0.11764705882352941
  graphCliqueNumber 3
  node [
    id 0
    label "kolejny"
    origin "text"
  ]
  node [
    id 1
    label "szeryf"
    origin "text"
  ]
  node [
    id 2
    label "inny"
  ]
  node [
    id 3
    label "nast&#281;pnie"
  ]
  node [
    id 4
    label "kt&#243;ry&#347;"
  ]
  node [
    id 5
    label "kolejno"
  ]
  node [
    id 6
    label "nastopny"
  ]
  node [
    id 7
    label "&#347;redniowieczny"
  ]
  node [
    id 8
    label "odznaczenie"
  ]
  node [
    id 9
    label "urz&#281;dnik"
  ]
  node [
    id 10
    label "tytu&#322;"
  ]
  node [
    id 11
    label "osobisto&#347;&#263;"
  ]
  node [
    id 12
    label "czcionka"
  ]
  node [
    id 13
    label "str&#243;&#380;"
  ]
  node [
    id 14
    label "zjazd"
  ]
  node [
    id 15
    label "A1"
  ]
  node [
    id 16
    label "Wieszowa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 15
    target 16
  ]
]
