graph [
  maxDegree 37
  minDegree 1
  meanDegree 1.9710144927536233
  density 0.028985507246376812
  graphCliqueNumber 4
  node [
    id 0
    label "pan"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "siebie"
    origin "text"
  ]
  node [
    id 3
    label "odpocz&#261;&#263;"
    origin "text"
  ]
  node [
    id 4
    label "konstantynopol"
    origin "text"
  ]
  node [
    id 5
    label "nieprzewidziany"
    origin "text"
  ]
  node [
    id 6
    label "podr&#243;&#380;"
    origin "text"
  ]
  node [
    id 7
    label "cz&#322;owiek"
  ]
  node [
    id 8
    label "profesor"
  ]
  node [
    id 9
    label "kszta&#322;ciciel"
  ]
  node [
    id 10
    label "jegomo&#347;&#263;"
  ]
  node [
    id 11
    label "zwrot"
  ]
  node [
    id 12
    label "pracodawca"
  ]
  node [
    id 13
    label "rz&#261;dzenie"
  ]
  node [
    id 14
    label "m&#261;&#380;"
  ]
  node [
    id 15
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 16
    label "ch&#322;opina"
  ]
  node [
    id 17
    label "bratek"
  ]
  node [
    id 18
    label "opiekun"
  ]
  node [
    id 19
    label "doros&#322;y"
  ]
  node [
    id 20
    label "preceptor"
  ]
  node [
    id 21
    label "Midas"
  ]
  node [
    id 22
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 23
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 24
    label "murza"
  ]
  node [
    id 25
    label "ojciec"
  ]
  node [
    id 26
    label "androlog"
  ]
  node [
    id 27
    label "pupil"
  ]
  node [
    id 28
    label "efendi"
  ]
  node [
    id 29
    label "nabab"
  ]
  node [
    id 30
    label "w&#322;odarz"
  ]
  node [
    id 31
    label "szkolnik"
  ]
  node [
    id 32
    label "pedagog"
  ]
  node [
    id 33
    label "popularyzator"
  ]
  node [
    id 34
    label "andropauza"
  ]
  node [
    id 35
    label "gra_w_karty"
  ]
  node [
    id 36
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 37
    label "Mieszko_I"
  ]
  node [
    id 38
    label "bogaty"
  ]
  node [
    id 39
    label "samiec"
  ]
  node [
    id 40
    label "przyw&#243;dca"
  ]
  node [
    id 41
    label "pa&#324;stwo"
  ]
  node [
    id 42
    label "belfer"
  ]
  node [
    id 43
    label "remainder"
  ]
  node [
    id 44
    label "przerwa&#263;"
  ]
  node [
    id 45
    label "niespodziewanie"
  ]
  node [
    id 46
    label "spontaniczny"
  ]
  node [
    id 47
    label "zaskakuj&#261;cy"
  ]
  node [
    id 48
    label "nieprzewidzianie"
  ]
  node [
    id 49
    label "zbior&#243;wka"
  ]
  node [
    id 50
    label "ekskursja"
  ]
  node [
    id 51
    label "rajza"
  ]
  node [
    id 52
    label "ruch"
  ]
  node [
    id 53
    label "ekwipunek"
  ]
  node [
    id 54
    label "journey"
  ]
  node [
    id 55
    label "zmiana"
  ]
  node [
    id 56
    label "bezsilnikowy"
  ]
  node [
    id 57
    label "turystyka"
  ]
  node [
    id 58
    label "van"
  ]
  node [
    id 59
    label "Mitten"
  ]
  node [
    id 60
    label "&#8217;"
  ]
  node [
    id 61
    label "&#243;w"
  ]
  node [
    id 62
    label "z&#322;oty"
  ]
  node [
    id 63
    label "r&#243;g"
  ]
  node [
    id 64
    label "most"
  ]
  node [
    id 65
    label "su&#322;tanka"
  ]
  node [
    id 66
    label "walida"
  ]
  node [
    id 67
    label "Jeni"
  ]
  node [
    id 68
    label "Kapussi"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 60
  ]
  edge [
    source 58
    target 61
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 61
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 66
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 67
    target 68
  ]
]
