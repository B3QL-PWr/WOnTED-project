graph [
  maxDegree 10
  minDegree 1
  meanDegree 3.28
  density 0.04432432432432432
  graphCliqueNumber 7
  node [
    id 0
    label "zaprasza&#263;"
    origin "text"
  ]
  node [
    id 1
    label "&#347;ledzi&#263;"
    origin "text"
  ]
  node [
    id 2
    label "tag"
    origin "text"
  ]
  node [
    id 3
    label "aptekarskiwabik"
    origin "text"
  ]
  node [
    id 4
    label "invite"
  ]
  node [
    id 5
    label "ask"
  ]
  node [
    id 6
    label "oferowa&#263;"
  ]
  node [
    id 7
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 8
    label "examine"
  ]
  node [
    id 9
    label "szuka&#263;"
  ]
  node [
    id 10
    label "&#322;owiectwo"
  ]
  node [
    id 11
    label "chase"
  ]
  node [
    id 12
    label "robi&#263;"
  ]
  node [
    id 13
    label "szperacz"
  ]
  node [
    id 14
    label "identyfikator"
  ]
  node [
    id 15
    label "napis"
  ]
  node [
    id 16
    label "nerd"
  ]
  node [
    id 17
    label "znacznik"
  ]
  node [
    id 18
    label "komnatowy"
  ]
  node [
    id 19
    label "sport_elektroniczny"
  ]
  node [
    id 20
    label "Gerhard"
  ]
  node [
    id 21
    label "Domagk"
  ]
  node [
    id 22
    label "i"
  ]
  node [
    id 23
    label "wojna"
  ]
  node [
    id 24
    label "&#347;wiatowy"
  ]
  node [
    id 25
    label "wielki"
  ]
  node [
    id 26
    label "Iga"
  ]
  node [
    id 27
    label "Farben"
  ]
  node [
    id 28
    label "Mietzscha"
  ]
  node [
    id 29
    label "Anda"
  ]
  node [
    id 30
    label "Klarera"
  ]
  node [
    id 31
    label "naukowiec"
  ]
  node [
    id 32
    label "Paul"
  ]
  node [
    id 33
    label "Gelmo"
  ]
  node [
    id 34
    label "uniwersytet"
  ]
  node [
    id 35
    label "wiede&#324;ski"
  ]
  node [
    id 36
    label "Franklin"
  ]
  node [
    id 37
    label "D"
  ]
  node [
    id 38
    label "Winstonowi"
  ]
  node [
    id 39
    label "Churchill"
  ]
  node [
    id 40
    label "Roosevelt"
  ]
  node [
    id 41
    label "nagroda"
  ]
  node [
    id 42
    label "Nobel"
  ]
  node [
    id 43
    label "o&#347;wiadczenie"
  ]
  node [
    id 44
    label "Ana"
  ]
  node [
    id 45
    label "V"
  ]
  node [
    id 46
    label "Diez"
  ]
  node [
    id 47
    label "Rouxb"
  ]
  node [
    id 48
    label "Rustam"
  ]
  node [
    id 49
    label "Aminov"
  ]
  node [
    id 50
    label "httpswwwsciencehistoryorghistoricalprofilegerharddomagk"
  ]
  node [
    id 51
    label "httpcontenttimecomtimemagazinearticle0917177190000html"
  ]
  node [
    id 52
    label "Jos&#233;"
  ]
  node [
    id 53
    label "a"
  ]
  node [
    id 54
    label "Tapia"
  ]
  node [
    id 55
    label "Granadosa"
  ]
  node [
    id 56
    label "proca"
  ]
  node [
    id 57
    label "Natl"
  ]
  node [
    id 58
    label "Acad"
  ]
  node [
    id 59
    label "Sci"
  ]
  node [
    id 60
    label "u"
  ]
  node [
    id 61
    label "sekunda"
  ]
  node [
    id 62
    label "Life"
  ]
  node [
    id 63
    label "death"
  ]
  node [
    id 64
    label "during"
  ]
  node [
    id 65
    label "the"
  ]
  node [
    id 66
    label "Great"
  ]
  node [
    id 67
    label "Depression"
  ]
  node [
    id 68
    label "future"
  ]
  node [
    id 69
    label "szczeg&#243;lny"
  ]
  node [
    id 70
    label "podzi&#281;kowanie"
  ]
  node [
    id 71
    label "dla"
  ]
  node [
    id 72
    label "shoterxd"
  ]
  node [
    id 73
    label "brant"
  ]
  node [
    id 74
    label "InformacjaNieprawdziwaCCCLVIII"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 43
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 24
  ]
  edge [
    source 22
    target 48
  ]
  edge [
    source 22
    target 49
  ]
  edge [
    source 22
    target 69
  ]
  edge [
    source 22
    target 70
  ]
  edge [
    source 22
    target 71
  ]
  edge [
    source 22
    target 72
  ]
  edge [
    source 22
    target 73
  ]
  edge [
    source 22
    target 74
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 25
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 30
  ]
  edge [
    source 28
    target 31
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 31
  ]
  edge [
    source 29
    target 62
  ]
  edge [
    source 29
    target 63
  ]
  edge [
    source 29
    target 64
  ]
  edge [
    source 29
    target 65
  ]
  edge [
    source 29
    target 66
  ]
  edge [
    source 29
    target 67
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 40
  ]
  edge [
    source 37
    target 40
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 46
  ]
  edge [
    source 44
    target 47
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 47
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 65
  ]
  edge [
    source 50
    target 68
  ]
  edge [
    source 51
    target 65
  ]
  edge [
    source 51
    target 68
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 54
  ]
  edge [
    source 52
    target 55
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 55
  ]
  edge [
    source 53
    target 56
  ]
  edge [
    source 53
    target 57
  ]
  edge [
    source 53
    target 58
  ]
  edge [
    source 53
    target 59
  ]
  edge [
    source 53
    target 60
  ]
  edge [
    source 53
    target 61
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 58
  ]
  edge [
    source 56
    target 59
  ]
  edge [
    source 56
    target 60
  ]
  edge [
    source 56
    target 61
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 59
  ]
  edge [
    source 57
    target 60
  ]
  edge [
    source 57
    target 61
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 60
  ]
  edge [
    source 58
    target 61
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 61
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 64
  ]
  edge [
    source 62
    target 65
  ]
  edge [
    source 62
    target 66
  ]
  edge [
    source 62
    target 67
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 65
  ]
  edge [
    source 63
    target 66
  ]
  edge [
    source 63
    target 67
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 66
  ]
  edge [
    source 64
    target 67
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 67
  ]
  edge [
    source 65
    target 68
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 69
    target 71
  ]
  edge [
    source 69
    target 72
  ]
  edge [
    source 69
    target 73
  ]
  edge [
    source 69
    target 74
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 72
  ]
  edge [
    source 70
    target 73
  ]
  edge [
    source 70
    target 74
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 71
    target 73
  ]
  edge [
    source 71
    target 74
  ]
  edge [
    source 72
    target 73
  ]
  edge [
    source 72
    target 74
  ]
  edge [
    source 73
    target 74
  ]
]
