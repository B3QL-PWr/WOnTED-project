graph [
  maxDegree 8
  minDegree 1
  meanDegree 1.9310344827586208
  density 0.06896551724137931
  graphCliqueNumber 2
  node [
    id 0
    label "pochwali&#263;"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "jak"
    origin "text"
  ]
  node [
    id 3
    label "prezent"
    origin "text"
  ]
  node [
    id 4
    label "kilka"
    origin "text"
  ]
  node [
    id 5
    label "lato"
    origin "text"
  ]
  node [
    id 6
    label "temu"
    origin "text"
  ]
  node [
    id 7
    label "przygotowa&#263;by&#263;"
    origin "text"
  ]
  node [
    id 8
    label "kumpel"
    origin "text"
  ]
  node [
    id 9
    label "wywy&#380;szy&#263;"
  ]
  node [
    id 10
    label "nagrodzi&#263;"
  ]
  node [
    id 11
    label "praise"
  ]
  node [
    id 12
    label "byd&#322;o"
  ]
  node [
    id 13
    label "zobo"
  ]
  node [
    id 14
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 15
    label "yakalo"
  ]
  node [
    id 16
    label "dzo"
  ]
  node [
    id 17
    label "dar"
  ]
  node [
    id 18
    label "presentation"
  ]
  node [
    id 19
    label "&#347;ledziowate"
  ]
  node [
    id 20
    label "ryba"
  ]
  node [
    id 21
    label "pora_roku"
  ]
  node [
    id 22
    label "kumplowanie_si&#281;"
  ]
  node [
    id 23
    label "znajomy"
  ]
  node [
    id 24
    label "konfrater"
  ]
  node [
    id 25
    label "towarzysz"
  ]
  node [
    id 26
    label "zakolegowanie_si&#281;"
  ]
  node [
    id 27
    label "partner"
  ]
  node [
    id 28
    label "ziom"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 22
  ]
  edge [
    source 8
    target 23
  ]
  edge [
    source 8
    target 24
  ]
  edge [
    source 8
    target 25
  ]
  edge [
    source 8
    target 26
  ]
  edge [
    source 8
    target 27
  ]
  edge [
    source 8
    target 28
  ]
]
