graph [
  maxDegree 64
  minDegree 1
  meanDegree 1.9813084112149533
  density 0.018691588785046728
  graphCliqueNumber 3
  node [
    id 0
    label "zaskakuj&#261;cy"
    origin "text"
  ]
  node [
    id 1
    label "znalezisko"
    origin "text"
  ]
  node [
    id 2
    label "budowa"
    origin "text"
  ]
  node [
    id 3
    label "linia"
    origin "text"
  ]
  node [
    id 4
    label "metr"
    origin "text"
  ]
  node [
    id 5
    label "warszawa"
    origin "text"
  ]
  node [
    id 6
    label "nieoczekiwany"
  ]
  node [
    id 7
    label "zaskakuj&#261;co"
  ]
  node [
    id 8
    label "przedmiot"
  ]
  node [
    id 9
    label "figura"
  ]
  node [
    id 10
    label "wjazd"
  ]
  node [
    id 11
    label "struktura"
  ]
  node [
    id 12
    label "konstrukcja"
  ]
  node [
    id 13
    label "r&#243;w"
  ]
  node [
    id 14
    label "kreacja"
  ]
  node [
    id 15
    label "posesja"
  ]
  node [
    id 16
    label "cecha"
  ]
  node [
    id 17
    label "dokumentacja_powykonawcza"
  ]
  node [
    id 18
    label "organ"
  ]
  node [
    id 19
    label "mechanika"
  ]
  node [
    id 20
    label "zwierz&#281;"
  ]
  node [
    id 21
    label "miejsce_pracy"
  ]
  node [
    id 22
    label "praca"
  ]
  node [
    id 23
    label "constitution"
  ]
  node [
    id 24
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 25
    label "cz&#322;owiek"
  ]
  node [
    id 26
    label "koniec"
  ]
  node [
    id 27
    label "uporz&#261;dkowanie"
  ]
  node [
    id 28
    label "coalescence"
  ]
  node [
    id 29
    label "rz&#261;d"
  ]
  node [
    id 30
    label "grupa_organizm&#243;w"
  ]
  node [
    id 31
    label "curve"
  ]
  node [
    id 32
    label "kompleksja"
  ]
  node [
    id 33
    label "access"
  ]
  node [
    id 34
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 35
    label "tekst"
  ]
  node [
    id 36
    label "fragment"
  ]
  node [
    id 37
    label "cord"
  ]
  node [
    id 38
    label "przewo&#378;nik"
  ]
  node [
    id 39
    label "granica"
  ]
  node [
    id 40
    label "roz&#322;&#261;cza&#263;"
  ]
  node [
    id 41
    label "szpaler"
  ]
  node [
    id 42
    label "phreaker"
  ]
  node [
    id 43
    label "tract"
  ]
  node [
    id 44
    label "sztrych"
  ]
  node [
    id 45
    label "kontakt"
  ]
  node [
    id 46
    label "spos&#243;b"
  ]
  node [
    id 47
    label "przystanek"
  ]
  node [
    id 48
    label "prowadzi&#263;"
  ]
  node [
    id 49
    label "point"
  ]
  node [
    id 50
    label "materia&#322;_zecerski"
  ]
  node [
    id 51
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 52
    label "linijka"
  ]
  node [
    id 53
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 54
    label "po&#322;&#261;czenie"
  ]
  node [
    id 55
    label "transporter"
  ]
  node [
    id 56
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 57
    label "przeorientowywa&#263;"
  ]
  node [
    id 58
    label "bearing"
  ]
  node [
    id 59
    label "line"
  ]
  node [
    id 60
    label "trasa"
  ]
  node [
    id 61
    label "przew&#243;d"
  ]
  node [
    id 62
    label "figura_geometryczna"
  ]
  node [
    id 63
    label "kszta&#322;t"
  ]
  node [
    id 64
    label "drzewo_genealogiczne"
  ]
  node [
    id 65
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 66
    label "roz&#322;&#261;czanie"
  ]
  node [
    id 67
    label "wygl&#261;d"
  ]
  node [
    id 68
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 69
    label "poprowadzi&#263;"
  ]
  node [
    id 70
    label "armia"
  ]
  node [
    id 71
    label "szczep"
  ]
  node [
    id 72
    label "Ural"
  ]
  node [
    id 73
    label "przeorientowa&#263;"
  ]
  node [
    id 74
    label "prostoliniowo&#347;&#263;"
  ]
  node [
    id 75
    label "granice"
  ]
  node [
    id 76
    label "przeorientowanie"
  ]
  node [
    id 77
    label "billing"
  ]
  node [
    id 78
    label "prowadzenie"
  ]
  node [
    id 79
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 80
    label "zbi&#243;r"
  ]
  node [
    id 81
    label "przeorientowywanie"
  ]
  node [
    id 82
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 83
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 84
    label "jard"
  ]
  node [
    id 85
    label "meter"
  ]
  node [
    id 86
    label "decymetr"
  ]
  node [
    id 87
    label "megabyte"
  ]
  node [
    id 88
    label "plon"
  ]
  node [
    id 89
    label "metrum"
  ]
  node [
    id 90
    label "dekametr"
  ]
  node [
    id 91
    label "jednostka_powierzchni"
  ]
  node [
    id 92
    label "uk&#322;ad_SI"
  ]
  node [
    id 93
    label "literaturoznawstwo"
  ]
  node [
    id 94
    label "wiersz"
  ]
  node [
    id 95
    label "gigametr"
  ]
  node [
    id 96
    label "miara"
  ]
  node [
    id 97
    label "nauczyciel"
  ]
  node [
    id 98
    label "kilometr_kwadratowy"
  ]
  node [
    id 99
    label "jednostka_metryczna"
  ]
  node [
    id 100
    label "jednostka_masy"
  ]
  node [
    id 101
    label "centymetr_kwadratowy"
  ]
  node [
    id 102
    label "Warszawa"
  ]
  node [
    id 103
    label "samoch&#243;d"
  ]
  node [
    id 104
    label "fastback"
  ]
  node [
    id 105
    label "C08"
  ]
  node [
    id 106
    label "P&#322;ock"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 105
    target 106
  ]
]
