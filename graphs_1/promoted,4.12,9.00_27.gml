graph [
  maxDegree 22
  minDegree 1
  meanDegree 2.026315789473684
  density 0.027017543859649124
  graphCliqueNumber 3
  node [
    id 0
    label "playstation"
    origin "text"
  ]
  node [
    id 1
    label "classic"
    origin "text"
  ]
  node [
    id 2
    label "mie&#263;"
    origin "text"
  ]
  node [
    id 3
    label "by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "celebracja"
    origin "text"
  ]
  node [
    id 5
    label "bogaty"
    origin "text"
  ]
  node [
    id 6
    label "dziedzictwo"
    origin "text"
  ]
  node [
    id 7
    label "pierwsza"
    origin "text"
  ]
  node [
    id 8
    label "konsola"
    origin "text"
  ]
  node [
    id 9
    label "gra"
    origin "text"
  ]
  node [
    id 10
    label "sony"
    origin "text"
  ]
  node [
    id 11
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 12
    label "czu&#263;"
  ]
  node [
    id 13
    label "need"
  ]
  node [
    id 14
    label "hide"
  ]
  node [
    id 15
    label "support"
  ]
  node [
    id 16
    label "si&#281;ga&#263;"
  ]
  node [
    id 17
    label "trwa&#263;"
  ]
  node [
    id 18
    label "obecno&#347;&#263;"
  ]
  node [
    id 19
    label "stan"
  ]
  node [
    id 20
    label "stand"
  ]
  node [
    id 21
    label "mie&#263;_miejsce"
  ]
  node [
    id 22
    label "uczestniczy&#263;"
  ]
  node [
    id 23
    label "chodzi&#263;"
  ]
  node [
    id 24
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 25
    label "equal"
  ]
  node [
    id 26
    label "msza"
  ]
  node [
    id 27
    label "namaszczenie"
  ]
  node [
    id 28
    label "celebra"
  ]
  node [
    id 29
    label "cz&#322;owiek"
  ]
  node [
    id 30
    label "nabab"
  ]
  node [
    id 31
    label "forsiasty"
  ]
  node [
    id 32
    label "obfituj&#261;cy"
  ]
  node [
    id 33
    label "r&#243;&#380;norodny"
  ]
  node [
    id 34
    label "sytuowany"
  ]
  node [
    id 35
    label "zapa&#347;ny"
  ]
  node [
    id 36
    label "obficie"
  ]
  node [
    id 37
    label "spania&#322;y"
  ]
  node [
    id 38
    label "och&#281;do&#380;ny"
  ]
  node [
    id 39
    label "bogato"
  ]
  node [
    id 40
    label "wydziedziczy&#263;"
  ]
  node [
    id 41
    label "zachowek"
  ]
  node [
    id 42
    label "wydziedziczenie"
  ]
  node [
    id 43
    label "prawo"
  ]
  node [
    id 44
    label "mienie"
  ]
  node [
    id 45
    label "scheda_spadkowa"
  ]
  node [
    id 46
    label "sukcesja"
  ]
  node [
    id 47
    label "godzina"
  ]
  node [
    id 48
    label "ozdobny"
  ]
  node [
    id 49
    label "stolik"
  ]
  node [
    id 50
    label "urz&#261;dzenie"
  ]
  node [
    id 51
    label "pulpit"
  ]
  node [
    id 52
    label "wspornik"
  ]
  node [
    id 53
    label "tremo"
  ]
  node [
    id 54
    label "pad"
  ]
  node [
    id 55
    label "zabawa"
  ]
  node [
    id 56
    label "rywalizacja"
  ]
  node [
    id 57
    label "czynno&#347;&#263;"
  ]
  node [
    id 58
    label "Pok&#233;mon"
  ]
  node [
    id 59
    label "synteza"
  ]
  node [
    id 60
    label "odtworzenie"
  ]
  node [
    id 61
    label "komplet"
  ]
  node [
    id 62
    label "rekwizyt_do_gry"
  ]
  node [
    id 63
    label "odg&#322;os"
  ]
  node [
    id 64
    label "rozgrywka"
  ]
  node [
    id 65
    label "post&#281;powanie"
  ]
  node [
    id 66
    label "wydarzenie"
  ]
  node [
    id 67
    label "apparent_motion"
  ]
  node [
    id 68
    label "game"
  ]
  node [
    id 69
    label "zmienno&#347;&#263;"
  ]
  node [
    id 70
    label "zasada"
  ]
  node [
    id 71
    label "akcja"
  ]
  node [
    id 72
    label "play"
  ]
  node [
    id 73
    label "contest"
  ]
  node [
    id 74
    label "zbijany"
  ]
  node [
    id 75
    label "Classic"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 55
  ]
  edge [
    source 9
    target 56
  ]
  edge [
    source 9
    target 57
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 10
    target 50
  ]
]
