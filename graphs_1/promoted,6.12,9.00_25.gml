graph [
  maxDegree 38
  minDegree 1
  meanDegree 1.983739837398374
  density 0.016260162601626018
  graphCliqueNumber 3
  node [
    id 0
    label "naukowiec"
    origin "text"
  ]
  node [
    id 1
    label "uniwersytet"
    origin "text"
  ]
  node [
    id 2
    label "oksfordzki"
    origin "text"
  ]
  node [
    id 3
    label "by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 5
    label "rozwi&#261;za&#263;"
    origin "text"
  ]
  node [
    id 6
    label "jeden"
    origin "text"
  ]
  node [
    id 7
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 8
    label "problem"
    origin "text"
  ]
  node [
    id 9
    label "wsp&#243;&#322;czesny"
    origin "text"
  ]
  node [
    id 10
    label "fizyka"
    origin "text"
  ]
  node [
    id 11
    label "Miczurin"
  ]
  node [
    id 12
    label "&#347;ledziciel"
  ]
  node [
    id 13
    label "uczony"
  ]
  node [
    id 14
    label "ku&#378;nia"
  ]
  node [
    id 15
    label "Harvard"
  ]
  node [
    id 16
    label "uczelnia"
  ]
  node [
    id 17
    label "Uniwersytet_Oksfordzki"
  ]
  node [
    id 18
    label "Sorbona"
  ]
  node [
    id 19
    label "Stanford"
  ]
  node [
    id 20
    label "Princeton"
  ]
  node [
    id 21
    label "academy"
  ]
  node [
    id 22
    label "szko&#322;a_wy&#380;sza"
  ]
  node [
    id 23
    label "wydzia&#322;"
  ]
  node [
    id 24
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 25
    label "brytyjski"
  ]
  node [
    id 26
    label "angielski"
  ]
  node [
    id 27
    label "si&#281;ga&#263;"
  ]
  node [
    id 28
    label "trwa&#263;"
  ]
  node [
    id 29
    label "obecno&#347;&#263;"
  ]
  node [
    id 30
    label "stan"
  ]
  node [
    id 31
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 32
    label "stand"
  ]
  node [
    id 33
    label "mie&#263;_miejsce"
  ]
  node [
    id 34
    label "uczestniczy&#263;"
  ]
  node [
    id 35
    label "chodzi&#263;"
  ]
  node [
    id 36
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 37
    label "equal"
  ]
  node [
    id 38
    label "unloose"
  ]
  node [
    id 39
    label "urzeczywistni&#263;"
  ]
  node [
    id 40
    label "usun&#261;&#263;"
  ]
  node [
    id 41
    label "wymy&#347;li&#263;"
  ]
  node [
    id 42
    label "bring"
  ]
  node [
    id 43
    label "przesta&#263;"
  ]
  node [
    id 44
    label "undo"
  ]
  node [
    id 45
    label "uniewa&#380;ni&#263;"
  ]
  node [
    id 46
    label "kieliszek"
  ]
  node [
    id 47
    label "shot"
  ]
  node [
    id 48
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 49
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 50
    label "jaki&#347;"
  ]
  node [
    id 51
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 52
    label "jednolicie"
  ]
  node [
    id 53
    label "w&#243;dka"
  ]
  node [
    id 54
    label "ten"
  ]
  node [
    id 55
    label "ujednolicenie"
  ]
  node [
    id 56
    label "jednakowy"
  ]
  node [
    id 57
    label "doros&#322;y"
  ]
  node [
    id 58
    label "wiele"
  ]
  node [
    id 59
    label "dorodny"
  ]
  node [
    id 60
    label "znaczny"
  ]
  node [
    id 61
    label "du&#380;o"
  ]
  node [
    id 62
    label "prawdziwy"
  ]
  node [
    id 63
    label "niema&#322;o"
  ]
  node [
    id 64
    label "wa&#380;ny"
  ]
  node [
    id 65
    label "rozwini&#281;ty"
  ]
  node [
    id 66
    label "trudno&#347;&#263;"
  ]
  node [
    id 67
    label "sprawa"
  ]
  node [
    id 68
    label "ambaras"
  ]
  node [
    id 69
    label "problemat"
  ]
  node [
    id 70
    label "pierepa&#322;ka"
  ]
  node [
    id 71
    label "obstruction"
  ]
  node [
    id 72
    label "problematyka"
  ]
  node [
    id 73
    label "jajko_Kolumba"
  ]
  node [
    id 74
    label "subiekcja"
  ]
  node [
    id 75
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 76
    label "uwsp&#243;&#322;cze&#347;nienie"
  ]
  node [
    id 77
    label "tera&#378;niejszy"
  ]
  node [
    id 78
    label "uwsp&#243;&#322;cze&#347;nianie"
  ]
  node [
    id 79
    label "sp&#243;&#322;czesny"
  ]
  node [
    id 80
    label "unowocze&#347;nianie"
  ]
  node [
    id 81
    label "jednoczesny"
  ]
  node [
    id 82
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 83
    label "akustyka"
  ]
  node [
    id 84
    label "optyka"
  ]
  node [
    id 85
    label "termodynamika"
  ]
  node [
    id 86
    label "fizyka_cia&#322;a_sta&#322;ego"
  ]
  node [
    id 87
    label "przedmiot"
  ]
  node [
    id 88
    label "fizyka_kwantowa"
  ]
  node [
    id 89
    label "dylatometria"
  ]
  node [
    id 90
    label "elektryka"
  ]
  node [
    id 91
    label "elektrokinetyka"
  ]
  node [
    id 92
    label "fizyka_teoretyczna"
  ]
  node [
    id 93
    label "fizyka_j&#261;drowa"
  ]
  node [
    id 94
    label "dozymetria"
  ]
  node [
    id 95
    label "teoria_p&#243;l_kwantowych"
  ]
  node [
    id 96
    label "elektrodynamika"
  ]
  node [
    id 97
    label "fizyka_medyczna"
  ]
  node [
    id 98
    label "fizyka_atomowa"
  ]
  node [
    id 99
    label "fizyka_molekularna"
  ]
  node [
    id 100
    label "elektromagnetyzm"
  ]
  node [
    id 101
    label "pr&#243;&#380;nia"
  ]
  node [
    id 102
    label "fizyka_statystyczna"
  ]
  node [
    id 103
    label "kriofizyka"
  ]
  node [
    id 104
    label "mikrofizyka"
  ]
  node [
    id 105
    label "fiza"
  ]
  node [
    id 106
    label "elektrostatyka"
  ]
  node [
    id 107
    label "interferometria"
  ]
  node [
    id 108
    label "agrofizyka"
  ]
  node [
    id 109
    label "teoria_pola"
  ]
  node [
    id 110
    label "elektryczno&#347;&#263;"
  ]
  node [
    id 111
    label "kierunek"
  ]
  node [
    id 112
    label "fizyka_plazmy"
  ]
  node [
    id 113
    label "spektroskopia"
  ]
  node [
    id 114
    label "fizyka_cz&#261;stek_elementarnych"
  ]
  node [
    id 115
    label "mechanika"
  ]
  node [
    id 116
    label "chemia_powierzchni"
  ]
  node [
    id 117
    label "geofizyka"
  ]
  node [
    id 118
    label "nauka_przyrodnicza"
  ]
  node [
    id 119
    label "rentgenologia"
  ]
  node [
    id 120
    label "astronom"
  ]
  node [
    id 121
    label "Anda"
  ]
  node [
    id 122
    label "Astrophysics"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 10
    target 105
  ]
  edge [
    source 10
    target 106
  ]
  edge [
    source 10
    target 107
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 10
    target 110
  ]
  edge [
    source 10
    target 111
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 120
    target 121
  ]
  edge [
    source 120
    target 122
  ]
  edge [
    source 121
    target 122
  ]
]
