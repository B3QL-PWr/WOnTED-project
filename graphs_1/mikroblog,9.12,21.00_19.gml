graph [
  maxDegree 23
  minDegree 1
  meanDegree 1.9166666666666667
  density 0.08333333333333333
  graphCliqueNumber 2
  node [
    id 0
    label "heheszki"
    origin "text"
  ]
  node [
    id 1
    label "polski"
    origin "text"
  ]
  node [
    id 2
    label "swiat"
    origin "text"
  ]
  node [
    id 3
    label "lacki"
  ]
  node [
    id 4
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 5
    label "przedmiot"
  ]
  node [
    id 6
    label "sztajer"
  ]
  node [
    id 7
    label "drabant"
  ]
  node [
    id 8
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 9
    label "polak"
  ]
  node [
    id 10
    label "pierogi_ruskie"
  ]
  node [
    id 11
    label "krakowiak"
  ]
  node [
    id 12
    label "Polish"
  ]
  node [
    id 13
    label "j&#281;zyk"
  ]
  node [
    id 14
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 15
    label "oberek"
  ]
  node [
    id 16
    label "po_polsku"
  ]
  node [
    id 17
    label "mazur"
  ]
  node [
    id 18
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 19
    label "chodzony"
  ]
  node [
    id 20
    label "skoczny"
  ]
  node [
    id 21
    label "ryba_po_grecku"
  ]
  node [
    id 22
    label "goniony"
  ]
  node [
    id 23
    label "polsko"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
]
