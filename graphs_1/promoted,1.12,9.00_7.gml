graph [
  maxDegree 9
  minDegree 1
  meanDegree 1.9
  density 0.1
  graphCliqueNumber 2
  node [
    id 0
    label "widma"
    origin "text"
  ]
  node [
    id 1
    label "&#380;&#243;&#322;ty"
    origin "text"
  ]
  node [
    id 2
    label "kamizelka"
    origin "text"
  ]
  node [
    id 3
    label "kr&#261;&#380;y&#263;"
    origin "text"
  ]
  node [
    id 4
    label "europa"
    origin "text"
  ]
  node [
    id 5
    label "jasny"
  ]
  node [
    id 6
    label "typ_mongoloidalny"
  ]
  node [
    id 7
    label "&#380;&#243;&#322;cenie"
  ]
  node [
    id 8
    label "kolorowy"
  ]
  node [
    id 9
    label "ciep&#322;y"
  ]
  node [
    id 10
    label "&#380;&#243;&#322;to"
  ]
  node [
    id 11
    label "westa"
  ]
  node [
    id 12
    label "g&#243;ra"
  ]
  node [
    id 13
    label "krew"
  ]
  node [
    id 14
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 15
    label "kontrolowa&#263;"
  ]
  node [
    id 16
    label "przedmiot"
  ]
  node [
    id 17
    label "wheel"
  ]
  node [
    id 18
    label "sok"
  ]
  node [
    id 19
    label "carry"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
]
