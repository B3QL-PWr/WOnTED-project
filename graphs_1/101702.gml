graph [
  maxDegree 24
  minDegree 1
  meanDegree 2
  density 0.037037037037037035
  graphCliqueNumber 2
  node [
    id 0
    label "cisza"
    origin "text"
  ]
  node [
    id 1
    label "morze"
    origin "text"
  ]
  node [
    id 2
    label "ko&#347;ci&#243;&#322;"
    origin "text"
  ]
  node [
    id 3
    label "przerwa"
  ]
  node [
    id 4
    label "pok&#243;j"
  ]
  node [
    id 5
    label "rozmowa"
  ]
  node [
    id 6
    label "cicha_modlitwa"
  ]
  node [
    id 7
    label "g&#322;adki"
  ]
  node [
    id 8
    label "czas"
  ]
  node [
    id 9
    label "spok&#243;j"
  ]
  node [
    id 10
    label "cecha"
  ]
  node [
    id 11
    label "ci&#261;g"
  ]
  node [
    id 12
    label "peace"
  ]
  node [
    id 13
    label "tajemno&#347;&#263;"
  ]
  node [
    id 14
    label "cicha_praca"
  ]
  node [
    id 15
    label "cicha_msza"
  ]
  node [
    id 16
    label "zjawisko"
  ]
  node [
    id 17
    label "motionlessness"
  ]
  node [
    id 18
    label "sk&#322;on_kontynentalny"
  ]
  node [
    id 19
    label "Neptun"
  ]
  node [
    id 20
    label "Morze_Bia&#322;e"
  ]
  node [
    id 21
    label "reda"
  ]
  node [
    id 22
    label "Morze_Tyrre&#324;skie"
  ]
  node [
    id 23
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 24
    label "paliszcze"
  ]
  node [
    id 25
    label "okeanida"
  ]
  node [
    id 26
    label "latarnia_morska"
  ]
  node [
    id 27
    label "zbiornik_wodny"
  ]
  node [
    id 28
    label "Morze_Czerwone"
  ]
  node [
    id 29
    label "Morze_Ba&#322;tyckie"
  ]
  node [
    id 30
    label "laguna"
  ]
  node [
    id 31
    label "marina"
  ]
  node [
    id 32
    label "talasoterapia"
  ]
  node [
    id 33
    label "Morze_Adriatyckie"
  ]
  node [
    id 34
    label "bezmiar"
  ]
  node [
    id 35
    label "pe&#322;ne_morze"
  ]
  node [
    id 36
    label "Morze_Czarne"
  ]
  node [
    id 37
    label "nereida"
  ]
  node [
    id 38
    label "Ziemia"
  ]
  node [
    id 39
    label "przymorze"
  ]
  node [
    id 40
    label "Morze_Egejskie"
  ]
  node [
    id 41
    label "&#347;wi&#261;tynia"
  ]
  node [
    id 42
    label "zakrystia"
  ]
  node [
    id 43
    label "organizacja_religijna"
  ]
  node [
    id 44
    label "nawa"
  ]
  node [
    id 45
    label "nerwica_eklezjogenna"
  ]
  node [
    id 46
    label "kropielnica"
  ]
  node [
    id 47
    label "prezbiterium"
  ]
  node [
    id 48
    label "wsp&#243;lnota"
  ]
  node [
    id 49
    label "church"
  ]
  node [
    id 50
    label "kruchta"
  ]
  node [
    id 51
    label "Ska&#322;ka"
  ]
  node [
    id 52
    label "kult"
  ]
  node [
    id 53
    label "ub&#322;agalnia"
  ]
  node [
    id 54
    label "dom"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 0
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
]
