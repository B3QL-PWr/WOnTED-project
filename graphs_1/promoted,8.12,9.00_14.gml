graph [
  maxDegree 13
  minDegree 1
  meanDegree 1.9444444444444444
  density 0.05555555555555555
  graphCliqueNumber 2
  node [
    id 0
    label "recepta"
    origin "text"
  ]
  node [
    id 1
    label "samotno&#347;&#263;"
    origin "text"
  ]
  node [
    id 2
    label "brak"
    origin "text"
  ]
  node [
    id 3
    label "zrozumienie"
    origin "text"
  ]
  node [
    id 4
    label "receipt"
  ]
  node [
    id 5
    label "receptariusz"
  ]
  node [
    id 6
    label "przepis"
  ]
  node [
    id 7
    label "spos&#243;b"
  ]
  node [
    id 8
    label "zlecenie"
  ]
  node [
    id 9
    label "isolation"
  ]
  node [
    id 10
    label "izolacja"
  ]
  node [
    id 11
    label "samota"
  ]
  node [
    id 12
    label "wra&#380;enie"
  ]
  node [
    id 13
    label "prywatywny"
  ]
  node [
    id 14
    label "defect"
  ]
  node [
    id 15
    label "odej&#347;cie"
  ]
  node [
    id 16
    label "gap"
  ]
  node [
    id 17
    label "kr&#243;tki"
  ]
  node [
    id 18
    label "wyr&#243;b"
  ]
  node [
    id 19
    label "nieistnienie"
  ]
  node [
    id 20
    label "wada"
  ]
  node [
    id 21
    label "odej&#347;&#263;"
  ]
  node [
    id 22
    label "odchodzenie"
  ]
  node [
    id 23
    label "odchodzi&#263;"
  ]
  node [
    id 24
    label "creation"
  ]
  node [
    id 25
    label "skumanie"
  ]
  node [
    id 26
    label "przem&#243;wienie"
  ]
  node [
    id 27
    label "zorientowanie"
  ]
  node [
    id 28
    label "appreciation"
  ]
  node [
    id 29
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 30
    label "clasp"
  ]
  node [
    id 31
    label "poczucie"
  ]
  node [
    id 32
    label "ocenienie"
  ]
  node [
    id 33
    label "pos&#322;uchanie"
  ]
  node [
    id 34
    label "sympathy"
  ]
  node [
    id 35
    label "&#380;yczliwo&#347;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
]
