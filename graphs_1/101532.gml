graph [
  maxDegree 326
  minDegree 1
  meanDegree 2.323640960809102
  density 0.0029413176719102563
  graphCliqueNumber 6
  node [
    id 0
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 1
    label "podmiot"
    origin "text"
  ]
  node [
    id 2
    label "mai&#263;"
    origin "text"
  ]
  node [
    id 3
    label "sw&#243;j"
    origin "text"
  ]
  node [
    id 4
    label "siedziba"
    origin "text"
  ]
  node [
    id 5
    label "terenia"
    origin "text"
  ]
  node [
    id 6
    label "wsp&#243;lnota"
    origin "text"
  ]
  node [
    id 7
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 8
    label "swobodnie"
    origin "text"
  ]
  node [
    id 9
    label "bez"
    origin "text"
  ]
  node [
    id 10
    label "konieczno&#347;&#263;"
    origin "text"
  ]
  node [
    id 11
    label "uzyska&#263;"
    origin "text"
  ]
  node [
    id 12
    label "jakikolwiek"
    origin "text"
  ]
  node [
    id 13
    label "zgoda"
    origin "text"
  ]
  node [
    id 14
    label "w&#322;a&#347;ciwy"
    origin "text"
  ]
  node [
    id 15
    label "organ"
    origin "text"
  ]
  node [
    id 16
    label "kontrola"
    origin "text"
  ]
  node [
    id 17
    label "sprzedawa&#263;"
    origin "text"
  ]
  node [
    id 18
    label "kupowa&#263;"
    origin "text"
  ]
  node [
    id 19
    label "&#347;rodek"
    origin "text"
  ]
  node [
    id 20
    label "spo&#380;ywczy"
    origin "text"
  ]
  node [
    id 21
    label "dowolny"
    origin "text"
  ]
  node [
    id 22
    label "dotyczy&#263;"
    origin "text"
  ]
  node [
    id 23
    label "import"
    origin "text"
  ]
  node [
    id 24
    label "polska"
    origin "text"
  ]
  node [
    id 25
    label "jak"
    origin "text"
  ]
  node [
    id 26
    label "r&#243;wnie&#380;"
    origin "text"
  ]
  node [
    id 27
    label "eksport"
    origin "text"
  ]
  node [
    id 28
    label "polski"
    origin "text"
  ]
  node [
    id 29
    label "produkt"
    origin "text"
  ]
  node [
    id 30
    label "kraj"
    origin "text"
  ]
  node [
    id 31
    label "europejski"
    origin "text"
  ]
  node [
    id 32
    label "sprawowa&#263;"
    origin "text"
  ]
  node [
    id 33
    label "nadz&#243;r"
    origin "text"
  ]
  node [
    id 34
    label "nad"
    origin "text"
  ]
  node [
    id 35
    label "bezpiecze&#324;stwo"
    origin "text"
  ]
  node [
    id 36
    label "&#380;ywno&#347;&#263;"
    origin "text"
  ]
  node [
    id 37
    label "nadzorowa&#263;"
    origin "text"
  ]
  node [
    id 38
    label "zawiera&#263;"
    origin "text"
  ]
  node [
    id 39
    label "kupno"
    origin "text"
  ]
  node [
    id 40
    label "sprzeda&#380;"
    origin "text"
  ]
  node [
    id 41
    label "kompetencja"
    origin "text"
  ]
  node [
    id 42
    label "tychy"
    origin "text"
  ]
  node [
    id 43
    label "inspekcja"
    origin "text"
  ]
  node [
    id 44
    label "weterynaryjny"
    origin "text"
  ]
  node [
    id 45
    label "pa&#324;stwowy"
    origin "text"
  ]
  node [
    id 46
    label "sanitarny"
    origin "text"
  ]
  node [
    id 47
    label "by&#263;"
    origin "text"
  ]
  node [
    id 48
    label "jedynie"
    origin "text"
  ]
  node [
    id 49
    label "spe&#322;nia&#263;"
    origin "text"
  ]
  node [
    id 50
    label "wymagania"
    origin "text"
  ]
  node [
    id 51
    label "higieniczny"
    origin "text"
  ]
  node [
    id 52
    label "przez"
    origin "text"
  ]
  node [
    id 53
    label "dzia&#322;a&#263;"
    origin "text"
  ]
  node [
    id 54
    label "rynek"
    origin "text"
  ]
  node [
    id 55
    label "tym"
    origin "text"
  ]
  node [
    id 56
    label "aby"
    origin "text"
  ]
  node [
    id 57
    label "zdrowotny"
    origin "text"
  ]
  node [
    id 58
    label "&#380;aden"
    origin "text"
  ]
  node [
    id 59
    label "udziela&#263;"
    origin "text"
  ]
  node [
    id 60
    label "zezwolenie"
    origin "text"
  ]
  node [
    id 61
    label "przyw&#243;z"
    origin "text"
  ]
  node [
    id 62
    label "cz&#322;onkowski"
    origin "text"
  ]
  node [
    id 63
    label "unia"
    origin "text"
  ]
  node [
    id 64
    label "rama"
    origin "text"
  ]
  node [
    id 65
    label "by&#263;by"
    origin "text"
  ]
  node [
    id 66
    label "uznany"
    origin "text"
  ]
  node [
    id 67
    label "z&#322;amanie"
    origin "text"
  ]
  node [
    id 68
    label "zasada"
    origin "text"
  ]
  node [
    id 69
    label "swobodny"
    origin "text"
  ]
  node [
    id 70
    label "przep&#322;yw"
    origin "text"
  ]
  node [
    id 71
    label "towar"
    origin "text"
  ]
  node [
    id 72
    label "jaki&#347;"
  ]
  node [
    id 73
    label "cz&#322;owiek"
  ]
  node [
    id 74
    label "byt"
  ]
  node [
    id 75
    label "organizacja"
  ]
  node [
    id 76
    label "prawo"
  ]
  node [
    id 77
    label "nauka_prawa"
  ]
  node [
    id 78
    label "osobowo&#347;&#263;"
  ]
  node [
    id 79
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 80
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 81
    label "ozdabia&#263;"
  ]
  node [
    id 82
    label "bli&#378;ni"
  ]
  node [
    id 83
    label "odpowiedni"
  ]
  node [
    id 84
    label "swojak"
  ]
  node [
    id 85
    label "samodzielny"
  ]
  node [
    id 86
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 87
    label "Kreml"
  ]
  node [
    id 88
    label "miejsce"
  ]
  node [
    id 89
    label "budynek"
  ]
  node [
    id 90
    label "Bia&#322;y_Dom"
  ]
  node [
    id 91
    label "&#321;ubianka"
  ]
  node [
    id 92
    label "sadowisko"
  ]
  node [
    id 93
    label "dzia&#322;_personalny"
  ]
  node [
    id 94
    label "miejsce_pracy"
  ]
  node [
    id 95
    label "Skandynawia"
  ]
  node [
    id 96
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 97
    label "partnership"
  ]
  node [
    id 98
    label "zwi&#261;zek"
  ]
  node [
    id 99
    label "zwi&#261;za&#263;"
  ]
  node [
    id 100
    label "Walencja"
  ]
  node [
    id 101
    label "society"
  ]
  node [
    id 102
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 103
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 104
    label "bratnia_dusza"
  ]
  node [
    id 105
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 106
    label "marriage"
  ]
  node [
    id 107
    label "zwi&#261;zanie"
  ]
  node [
    id 108
    label "Ba&#322;kany"
  ]
  node [
    id 109
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 110
    label "wi&#261;zanie"
  ]
  node [
    id 111
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 112
    label "podobie&#324;stwo"
  ]
  node [
    id 113
    label "wolny"
  ]
  node [
    id 114
    label "lu&#378;no"
  ]
  node [
    id 115
    label "naturalnie"
  ]
  node [
    id 116
    label "wolnie"
  ]
  node [
    id 117
    label "dowolnie"
  ]
  node [
    id 118
    label "ki&#347;&#263;"
  ]
  node [
    id 119
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 120
    label "krzew"
  ]
  node [
    id 121
    label "pi&#380;maczkowate"
  ]
  node [
    id 122
    label "pestkowiec"
  ]
  node [
    id 123
    label "kwiat"
  ]
  node [
    id 124
    label "owoc"
  ]
  node [
    id 125
    label "oliwkowate"
  ]
  node [
    id 126
    label "ro&#347;lina"
  ]
  node [
    id 127
    label "hy&#263;ka"
  ]
  node [
    id 128
    label "lilac"
  ]
  node [
    id 129
    label "delfinidyna"
  ]
  node [
    id 130
    label "przymus"
  ]
  node [
    id 131
    label "obligatoryjno&#347;&#263;"
  ]
  node [
    id 132
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 133
    label "wym&#243;g"
  ]
  node [
    id 134
    label "operator_modalny"
  ]
  node [
    id 135
    label "promocja"
  ]
  node [
    id 136
    label "give_birth"
  ]
  node [
    id 137
    label "wytworzy&#263;"
  ]
  node [
    id 138
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 139
    label "realize"
  ]
  node [
    id 140
    label "zrobi&#263;"
  ]
  node [
    id 141
    label "make"
  ]
  node [
    id 142
    label "zwolnienie_si&#281;"
  ]
  node [
    id 143
    label "spok&#243;j"
  ]
  node [
    id 144
    label "zwalnianie_si&#281;"
  ]
  node [
    id 145
    label "odpowied&#378;"
  ]
  node [
    id 146
    label "wiedza"
  ]
  node [
    id 147
    label "zwalnia&#263;_si&#281;"
  ]
  node [
    id 148
    label "zwolni&#263;_si&#281;"
  ]
  node [
    id 149
    label "entity"
  ]
  node [
    id 150
    label "agreement"
  ]
  node [
    id 151
    label "consensus"
  ]
  node [
    id 152
    label "decyzja"
  ]
  node [
    id 153
    label "jednomy&#347;lno&#347;&#263;"
  ]
  node [
    id 154
    label "license"
  ]
  node [
    id 155
    label "pozwole&#324;stwo"
  ]
  node [
    id 156
    label "taki"
  ]
  node [
    id 157
    label "nale&#380;yty"
  ]
  node [
    id 158
    label "charakterystyczny"
  ]
  node [
    id 159
    label "stosownie"
  ]
  node [
    id 160
    label "dobry"
  ]
  node [
    id 161
    label "prawdziwy"
  ]
  node [
    id 162
    label "ten"
  ]
  node [
    id 163
    label "uprawniony"
  ]
  node [
    id 164
    label "zasadniczy"
  ]
  node [
    id 165
    label "typowy"
  ]
  node [
    id 166
    label "nale&#380;ny"
  ]
  node [
    id 167
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 168
    label "uk&#322;ad"
  ]
  node [
    id 169
    label "organogeneza"
  ]
  node [
    id 170
    label "Komitet_Region&#243;w"
  ]
  node [
    id 171
    label "Izba_Konsyliarska"
  ]
  node [
    id 172
    label "budowa"
  ]
  node [
    id 173
    label "okolica"
  ]
  node [
    id 174
    label "zesp&#243;&#322;"
  ]
  node [
    id 175
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 176
    label "jednostka_organizacyjna"
  ]
  node [
    id 177
    label "dekortykacja"
  ]
  node [
    id 178
    label "struktura_anatomiczna"
  ]
  node [
    id 179
    label "tkanka"
  ]
  node [
    id 180
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 181
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 182
    label "stomia"
  ]
  node [
    id 183
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 184
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 185
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 186
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 187
    label "tw&#243;r"
  ]
  node [
    id 188
    label "czynno&#347;&#263;"
  ]
  node [
    id 189
    label "examination"
  ]
  node [
    id 190
    label "legalizacja_pierwotna"
  ]
  node [
    id 191
    label "w&#322;adza"
  ]
  node [
    id 192
    label "perlustracja"
  ]
  node [
    id 193
    label "instytucja"
  ]
  node [
    id 194
    label "legalizacja_ponowna"
  ]
  node [
    id 195
    label "op&#281;dza&#263;"
  ]
  node [
    id 196
    label "oferowa&#263;"
  ]
  node [
    id 197
    label "oddawa&#263;"
  ]
  node [
    id 198
    label "handlowa&#263;"
  ]
  node [
    id 199
    label "sell"
  ]
  node [
    id 200
    label "get"
  ]
  node [
    id 201
    label "ustawia&#263;"
  ]
  node [
    id 202
    label "wierzy&#263;"
  ]
  node [
    id 203
    label "przyjmowa&#263;"
  ]
  node [
    id 204
    label "pozyskiwa&#263;"
  ]
  node [
    id 205
    label "gra&#263;"
  ]
  node [
    id 206
    label "kupywa&#263;"
  ]
  node [
    id 207
    label "uznawa&#263;"
  ]
  node [
    id 208
    label "bra&#263;"
  ]
  node [
    id 209
    label "czas"
  ]
  node [
    id 210
    label "abstrakcja"
  ]
  node [
    id 211
    label "punkt"
  ]
  node [
    id 212
    label "substancja"
  ]
  node [
    id 213
    label "spos&#243;b"
  ]
  node [
    id 214
    label "chemikalia"
  ]
  node [
    id 215
    label "konsumpcyjnie"
  ]
  node [
    id 216
    label "jadalny"
  ]
  node [
    id 217
    label "uwolnienie"
  ]
  node [
    id 218
    label "uwalnianie"
  ]
  node [
    id 219
    label "bargain"
  ]
  node [
    id 220
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 221
    label "tycze&#263;"
  ]
  node [
    id 222
    label "bilans_handlowy"
  ]
  node [
    id 223
    label "importoch&#322;onno&#347;&#263;"
  ]
  node [
    id 224
    label "eksport_netto"
  ]
  node [
    id 225
    label "dostawa"
  ]
  node [
    id 226
    label "byd&#322;o"
  ]
  node [
    id 227
    label "zobo"
  ]
  node [
    id 228
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 229
    label "yakalo"
  ]
  node [
    id 230
    label "dzo"
  ]
  node [
    id 231
    label "lacki"
  ]
  node [
    id 232
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 233
    label "przedmiot"
  ]
  node [
    id 234
    label "sztajer"
  ]
  node [
    id 235
    label "drabant"
  ]
  node [
    id 236
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 237
    label "polak"
  ]
  node [
    id 238
    label "pierogi_ruskie"
  ]
  node [
    id 239
    label "krakowiak"
  ]
  node [
    id 240
    label "Polish"
  ]
  node [
    id 241
    label "j&#281;zyk"
  ]
  node [
    id 242
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 243
    label "oberek"
  ]
  node [
    id 244
    label "po_polsku"
  ]
  node [
    id 245
    label "mazur"
  ]
  node [
    id 246
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 247
    label "chodzony"
  ]
  node [
    id 248
    label "skoczny"
  ]
  node [
    id 249
    label "ryba_po_grecku"
  ]
  node [
    id 250
    label "goniony"
  ]
  node [
    id 251
    label "polsko"
  ]
  node [
    id 252
    label "production"
  ]
  node [
    id 253
    label "rezultat"
  ]
  node [
    id 254
    label "wytw&#243;r"
  ]
  node [
    id 255
    label "Rwanda"
  ]
  node [
    id 256
    label "Filipiny"
  ]
  node [
    id 257
    label "Yorkshire"
  ]
  node [
    id 258
    label "Kaukaz"
  ]
  node [
    id 259
    label "Podbeskidzie"
  ]
  node [
    id 260
    label "Toskania"
  ]
  node [
    id 261
    label "&#321;emkowszczyzna"
  ]
  node [
    id 262
    label "obszar"
  ]
  node [
    id 263
    label "Monako"
  ]
  node [
    id 264
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 265
    label "Amhara"
  ]
  node [
    id 266
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 267
    label "Lombardia"
  ]
  node [
    id 268
    label "Korea"
  ]
  node [
    id 269
    label "Kalabria"
  ]
  node [
    id 270
    label "Czarnog&#243;ra"
  ]
  node [
    id 271
    label "Ghana"
  ]
  node [
    id 272
    label "Tyrol"
  ]
  node [
    id 273
    label "Malawi"
  ]
  node [
    id 274
    label "Indonezja"
  ]
  node [
    id 275
    label "Bu&#322;garia"
  ]
  node [
    id 276
    label "Nauru"
  ]
  node [
    id 277
    label "Kenia"
  ]
  node [
    id 278
    label "Pamir"
  ]
  node [
    id 279
    label "Kambod&#380;a"
  ]
  node [
    id 280
    label "Lubelszczyzna"
  ]
  node [
    id 281
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 282
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 283
    label "Mali"
  ]
  node [
    id 284
    label "&#379;ywiecczyzna"
  ]
  node [
    id 285
    label "Austria"
  ]
  node [
    id 286
    label "interior"
  ]
  node [
    id 287
    label "Europa_Wschodnia"
  ]
  node [
    id 288
    label "Armenia"
  ]
  node [
    id 289
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 290
    label "Fid&#380;i"
  ]
  node [
    id 291
    label "Tuwalu"
  ]
  node [
    id 292
    label "Zabajkale"
  ]
  node [
    id 293
    label "Etiopia"
  ]
  node [
    id 294
    label "Malezja"
  ]
  node [
    id 295
    label "Malta"
  ]
  node [
    id 296
    label "Kaszuby"
  ]
  node [
    id 297
    label "Noworosja"
  ]
  node [
    id 298
    label "Bo&#347;nia"
  ]
  node [
    id 299
    label "Tad&#380;ykistan"
  ]
  node [
    id 300
    label "Grenada"
  ]
  node [
    id 301
    label "Wehrlen"
  ]
  node [
    id 302
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 303
    label "Anglia"
  ]
  node [
    id 304
    label "Kielecczyzna"
  ]
  node [
    id 305
    label "Rumunia"
  ]
  node [
    id 306
    label "Pomorze_Zachodnie"
  ]
  node [
    id 307
    label "Maroko"
  ]
  node [
    id 308
    label "Bhutan"
  ]
  node [
    id 309
    label "Opolskie"
  ]
  node [
    id 310
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 311
    label "Ko&#322;yma"
  ]
  node [
    id 312
    label "Oksytania"
  ]
  node [
    id 313
    label "S&#322;owacja"
  ]
  node [
    id 314
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 315
    label "Seszele"
  ]
  node [
    id 316
    label "Syjon"
  ]
  node [
    id 317
    label "Kuwejt"
  ]
  node [
    id 318
    label "Arabia_Saudyjska"
  ]
  node [
    id 319
    label "Kociewie"
  ]
  node [
    id 320
    label "Kanada"
  ]
  node [
    id 321
    label "Ekwador"
  ]
  node [
    id 322
    label "ziemia"
  ]
  node [
    id 323
    label "Japonia"
  ]
  node [
    id 324
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 325
    label "Hiszpania"
  ]
  node [
    id 326
    label "Wyspy_Marshalla"
  ]
  node [
    id 327
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 328
    label "D&#380;ibuti"
  ]
  node [
    id 329
    label "Botswana"
  ]
  node [
    id 330
    label "Huculszczyzna"
  ]
  node [
    id 331
    label "Wietnam"
  ]
  node [
    id 332
    label "Egipt"
  ]
  node [
    id 333
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 334
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 335
    label "Burkina_Faso"
  ]
  node [
    id 336
    label "Bawaria"
  ]
  node [
    id 337
    label "Niemcy"
  ]
  node [
    id 338
    label "Khitai"
  ]
  node [
    id 339
    label "Macedonia"
  ]
  node [
    id 340
    label "Albania"
  ]
  node [
    id 341
    label "Madagaskar"
  ]
  node [
    id 342
    label "Bahrajn"
  ]
  node [
    id 343
    label "Jemen"
  ]
  node [
    id 344
    label "Lesoto"
  ]
  node [
    id 345
    label "Maghreb"
  ]
  node [
    id 346
    label "Samoa"
  ]
  node [
    id 347
    label "Andora"
  ]
  node [
    id 348
    label "Bory_Tucholskie"
  ]
  node [
    id 349
    label "Chiny"
  ]
  node [
    id 350
    label "Europa_Zachodnia"
  ]
  node [
    id 351
    label "Cypr"
  ]
  node [
    id 352
    label "Wielka_Brytania"
  ]
  node [
    id 353
    label "Kerala"
  ]
  node [
    id 354
    label "Podhale"
  ]
  node [
    id 355
    label "Kabylia"
  ]
  node [
    id 356
    label "Ukraina"
  ]
  node [
    id 357
    label "Paragwaj"
  ]
  node [
    id 358
    label "Trynidad_i_Tobago"
  ]
  node [
    id 359
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 360
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 361
    label "Ma&#322;opolska"
  ]
  node [
    id 362
    label "Polesie"
  ]
  node [
    id 363
    label "Liguria"
  ]
  node [
    id 364
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 365
    label "Libia"
  ]
  node [
    id 366
    label "&#321;&#243;dzkie"
  ]
  node [
    id 367
    label "Surinam"
  ]
  node [
    id 368
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 369
    label "Palestyna"
  ]
  node [
    id 370
    label "Nigeria"
  ]
  node [
    id 371
    label "Australia"
  ]
  node [
    id 372
    label "Honduras"
  ]
  node [
    id 373
    label "Bojkowszczyzna"
  ]
  node [
    id 374
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 375
    label "Karaiby"
  ]
  node [
    id 376
    label "Peru"
  ]
  node [
    id 377
    label "USA"
  ]
  node [
    id 378
    label "Bangladesz"
  ]
  node [
    id 379
    label "Kazachstan"
  ]
  node [
    id 380
    label "Nepal"
  ]
  node [
    id 381
    label "Irak"
  ]
  node [
    id 382
    label "Nadrenia"
  ]
  node [
    id 383
    label "Sudan"
  ]
  node [
    id 384
    label "S&#261;decczyzna"
  ]
  node [
    id 385
    label "Sand&#380;ak"
  ]
  node [
    id 386
    label "San_Marino"
  ]
  node [
    id 387
    label "Burundi"
  ]
  node [
    id 388
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 389
    label "Dominikana"
  ]
  node [
    id 390
    label "Komory"
  ]
  node [
    id 391
    label "Zakarpacie"
  ]
  node [
    id 392
    label "Gwatemala"
  ]
  node [
    id 393
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 394
    label "Zag&#243;rze"
  ]
  node [
    id 395
    label "Andaluzja"
  ]
  node [
    id 396
    label "granica_pa&#324;stwa"
  ]
  node [
    id 397
    label "Turkiestan"
  ]
  node [
    id 398
    label "Naddniestrze"
  ]
  node [
    id 399
    label "Hercegowina"
  ]
  node [
    id 400
    label "Brunei"
  ]
  node [
    id 401
    label "Iran"
  ]
  node [
    id 402
    label "jednostka_administracyjna"
  ]
  node [
    id 403
    label "Zimbabwe"
  ]
  node [
    id 404
    label "Namibia"
  ]
  node [
    id 405
    label "Meksyk"
  ]
  node [
    id 406
    label "Opolszczyzna"
  ]
  node [
    id 407
    label "Kamerun"
  ]
  node [
    id 408
    label "Afryka_Wschodnia"
  ]
  node [
    id 409
    label "Szlezwik"
  ]
  node [
    id 410
    label "Lotaryngia"
  ]
  node [
    id 411
    label "Somalia"
  ]
  node [
    id 412
    label "Angola"
  ]
  node [
    id 413
    label "Gabon"
  ]
  node [
    id 414
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 415
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 416
    label "Nowa_Zelandia"
  ]
  node [
    id 417
    label "Mozambik"
  ]
  node [
    id 418
    label "Tunezja"
  ]
  node [
    id 419
    label "Tajwan"
  ]
  node [
    id 420
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 421
    label "Liban"
  ]
  node [
    id 422
    label "Jordania"
  ]
  node [
    id 423
    label "Tonga"
  ]
  node [
    id 424
    label "Czad"
  ]
  node [
    id 425
    label "Gwinea"
  ]
  node [
    id 426
    label "Liberia"
  ]
  node [
    id 427
    label "Belize"
  ]
  node [
    id 428
    label "Mazowsze"
  ]
  node [
    id 429
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 430
    label "Benin"
  ]
  node [
    id 431
    label "&#321;otwa"
  ]
  node [
    id 432
    label "Syria"
  ]
  node [
    id 433
    label "Afryka_Zachodnia"
  ]
  node [
    id 434
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 435
    label "Dominika"
  ]
  node [
    id 436
    label "Antigua_i_Barbuda"
  ]
  node [
    id 437
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 438
    label "Hanower"
  ]
  node [
    id 439
    label "Galicja"
  ]
  node [
    id 440
    label "Szkocja"
  ]
  node [
    id 441
    label "Walia"
  ]
  node [
    id 442
    label "Afganistan"
  ]
  node [
    id 443
    label "W&#322;ochy"
  ]
  node [
    id 444
    label "Kiribati"
  ]
  node [
    id 445
    label "Szwajcaria"
  ]
  node [
    id 446
    label "Powi&#347;le"
  ]
  node [
    id 447
    label "Chorwacja"
  ]
  node [
    id 448
    label "Sahara_Zachodnia"
  ]
  node [
    id 449
    label "Tajlandia"
  ]
  node [
    id 450
    label "Salwador"
  ]
  node [
    id 451
    label "Bahamy"
  ]
  node [
    id 452
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 453
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 454
    label "Zamojszczyzna"
  ]
  node [
    id 455
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 456
    label "S&#322;owenia"
  ]
  node [
    id 457
    label "Gambia"
  ]
  node [
    id 458
    label "Urugwaj"
  ]
  node [
    id 459
    label "Podlasie"
  ]
  node [
    id 460
    label "Zair"
  ]
  node [
    id 461
    label "Erytrea"
  ]
  node [
    id 462
    label "Laponia"
  ]
  node [
    id 463
    label "Kujawy"
  ]
  node [
    id 464
    label "Umbria"
  ]
  node [
    id 465
    label "Rosja"
  ]
  node [
    id 466
    label "Mauritius"
  ]
  node [
    id 467
    label "Niger"
  ]
  node [
    id 468
    label "Uganda"
  ]
  node [
    id 469
    label "Turkmenistan"
  ]
  node [
    id 470
    label "Turcja"
  ]
  node [
    id 471
    label "Mezoameryka"
  ]
  node [
    id 472
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 473
    label "Irlandia"
  ]
  node [
    id 474
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 475
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 476
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 477
    label "Gwinea_Bissau"
  ]
  node [
    id 478
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 479
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 480
    label "Kurdystan"
  ]
  node [
    id 481
    label "Belgia"
  ]
  node [
    id 482
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 483
    label "Palau"
  ]
  node [
    id 484
    label "Barbados"
  ]
  node [
    id 485
    label "Wenezuela"
  ]
  node [
    id 486
    label "W&#281;gry"
  ]
  node [
    id 487
    label "Chile"
  ]
  node [
    id 488
    label "Argentyna"
  ]
  node [
    id 489
    label "Kolumbia"
  ]
  node [
    id 490
    label "Armagnac"
  ]
  node [
    id 491
    label "Kampania"
  ]
  node [
    id 492
    label "Sierra_Leone"
  ]
  node [
    id 493
    label "Azerbejd&#380;an"
  ]
  node [
    id 494
    label "Kongo"
  ]
  node [
    id 495
    label "Polinezja"
  ]
  node [
    id 496
    label "Warmia"
  ]
  node [
    id 497
    label "Pakistan"
  ]
  node [
    id 498
    label "Liechtenstein"
  ]
  node [
    id 499
    label "Wielkopolska"
  ]
  node [
    id 500
    label "Nikaragua"
  ]
  node [
    id 501
    label "Senegal"
  ]
  node [
    id 502
    label "brzeg"
  ]
  node [
    id 503
    label "Bordeaux"
  ]
  node [
    id 504
    label "Lauda"
  ]
  node [
    id 505
    label "Indie"
  ]
  node [
    id 506
    label "Mazury"
  ]
  node [
    id 507
    label "Suazi"
  ]
  node [
    id 508
    label "Polska"
  ]
  node [
    id 509
    label "Algieria"
  ]
  node [
    id 510
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 511
    label "Jamajka"
  ]
  node [
    id 512
    label "Timor_Wschodni"
  ]
  node [
    id 513
    label "Oceania"
  ]
  node [
    id 514
    label "Kostaryka"
  ]
  node [
    id 515
    label "Lasko"
  ]
  node [
    id 516
    label "Podkarpacie"
  ]
  node [
    id 517
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 518
    label "Kuba"
  ]
  node [
    id 519
    label "Mauretania"
  ]
  node [
    id 520
    label "Amazonia"
  ]
  node [
    id 521
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 522
    label "Portoryko"
  ]
  node [
    id 523
    label "Brazylia"
  ]
  node [
    id 524
    label "Mo&#322;dawia"
  ]
  node [
    id 525
    label "Litwa"
  ]
  node [
    id 526
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 527
    label "Kirgistan"
  ]
  node [
    id 528
    label "Izrael"
  ]
  node [
    id 529
    label "Grecja"
  ]
  node [
    id 530
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 531
    label "Kurpie"
  ]
  node [
    id 532
    label "Holandia"
  ]
  node [
    id 533
    label "Sri_Lanka"
  ]
  node [
    id 534
    label "Tonkin"
  ]
  node [
    id 535
    label "Katar"
  ]
  node [
    id 536
    label "Azja_Wschodnia"
  ]
  node [
    id 537
    label "Kaszmir"
  ]
  node [
    id 538
    label "Mikronezja"
  ]
  node [
    id 539
    label "Ukraina_Zachodnia"
  ]
  node [
    id 540
    label "Laos"
  ]
  node [
    id 541
    label "Mongolia"
  ]
  node [
    id 542
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 543
    label "Malediwy"
  ]
  node [
    id 544
    label "Zambia"
  ]
  node [
    id 545
    label "Turyngia"
  ]
  node [
    id 546
    label "Tanzania"
  ]
  node [
    id 547
    label "Gujana"
  ]
  node [
    id 548
    label "Apulia"
  ]
  node [
    id 549
    label "Uzbekistan"
  ]
  node [
    id 550
    label "Panama"
  ]
  node [
    id 551
    label "Czechy"
  ]
  node [
    id 552
    label "Gruzja"
  ]
  node [
    id 553
    label "Baszkiria"
  ]
  node [
    id 554
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 555
    label "Francja"
  ]
  node [
    id 556
    label "Serbia"
  ]
  node [
    id 557
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 558
    label "Togo"
  ]
  node [
    id 559
    label "Estonia"
  ]
  node [
    id 560
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 561
    label "Indochiny"
  ]
  node [
    id 562
    label "Boliwia"
  ]
  node [
    id 563
    label "Oman"
  ]
  node [
    id 564
    label "Portugalia"
  ]
  node [
    id 565
    label "Wyspy_Salomona"
  ]
  node [
    id 566
    label "Haiti"
  ]
  node [
    id 567
    label "Luksemburg"
  ]
  node [
    id 568
    label "Lubuskie"
  ]
  node [
    id 569
    label "Biskupizna"
  ]
  node [
    id 570
    label "Birma"
  ]
  node [
    id 571
    label "Rodezja"
  ]
  node [
    id 572
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 573
    label "European"
  ]
  node [
    id 574
    label "po_europejsku"
  ]
  node [
    id 575
    label "europejsko"
  ]
  node [
    id 576
    label "prosecute"
  ]
  node [
    id 577
    label "BHP"
  ]
  node [
    id 578
    label "katapultowa&#263;"
  ]
  node [
    id 579
    label "ubezpiecza&#263;"
  ]
  node [
    id 580
    label "stan"
  ]
  node [
    id 581
    label "ubezpieczenie"
  ]
  node [
    id 582
    label "cecha"
  ]
  node [
    id 583
    label "porz&#261;dek"
  ]
  node [
    id 584
    label "ubezpieczy&#263;"
  ]
  node [
    id 585
    label "safety"
  ]
  node [
    id 586
    label "katapultowanie"
  ]
  node [
    id 587
    label "ubezpieczanie"
  ]
  node [
    id 588
    label "test_zderzeniowy"
  ]
  node [
    id 589
    label "podanie"
  ]
  node [
    id 590
    label "wiwenda"
  ]
  node [
    id 591
    label "jad&#322;o"
  ]
  node [
    id 592
    label "podawanie"
  ]
  node [
    id 593
    label "podawa&#263;"
  ]
  node [
    id 594
    label "rzecz"
  ]
  node [
    id 595
    label "szama"
  ]
  node [
    id 596
    label "koryto"
  ]
  node [
    id 597
    label "poda&#263;"
  ]
  node [
    id 598
    label "manipulate"
  ]
  node [
    id 599
    label "pracowa&#263;"
  ]
  node [
    id 600
    label "obejmowa&#263;"
  ]
  node [
    id 601
    label "mie&#263;"
  ]
  node [
    id 602
    label "zamyka&#263;"
  ]
  node [
    id 603
    label "lock"
  ]
  node [
    id 604
    label "poznawa&#263;"
  ]
  node [
    id 605
    label "fold"
  ]
  node [
    id 606
    label "ustala&#263;"
  ]
  node [
    id 607
    label "sprzedaj&#261;cy"
  ]
  node [
    id 608
    label "transakcja"
  ]
  node [
    id 609
    label "przeniesienie_praw"
  ]
  node [
    id 610
    label "rabat"
  ]
  node [
    id 611
    label "przeda&#380;"
  ]
  node [
    id 612
    label "ability"
  ]
  node [
    id 613
    label "authority"
  ]
  node [
    id 614
    label "sprawno&#347;&#263;"
  ]
  node [
    id 615
    label "znawstwo"
  ]
  node [
    id 616
    label "zdolno&#347;&#263;"
  ]
  node [
    id 617
    label "gestia"
  ]
  node [
    id 618
    label "pa&#324;stwowo"
  ]
  node [
    id 619
    label "upa&#324;stwowienie"
  ]
  node [
    id 620
    label "upa&#324;stwawianie"
  ]
  node [
    id 621
    label "wsp&#243;lny"
  ]
  node [
    id 622
    label "medyczny"
  ]
  node [
    id 623
    label "si&#281;ga&#263;"
  ]
  node [
    id 624
    label "trwa&#263;"
  ]
  node [
    id 625
    label "obecno&#347;&#263;"
  ]
  node [
    id 626
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 627
    label "stand"
  ]
  node [
    id 628
    label "mie&#263;_miejsce"
  ]
  node [
    id 629
    label "uczestniczy&#263;"
  ]
  node [
    id 630
    label "chodzi&#263;"
  ]
  node [
    id 631
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 632
    label "equal"
  ]
  node [
    id 633
    label "robi&#263;"
  ]
  node [
    id 634
    label "close"
  ]
  node [
    id 635
    label "perform"
  ]
  node [
    id 636
    label "urzeczywistnia&#263;"
  ]
  node [
    id 637
    label "zdrowy"
  ]
  node [
    id 638
    label "po&#380;&#261;dany"
  ]
  node [
    id 639
    label "czysty"
  ]
  node [
    id 640
    label "higienicznie"
  ]
  node [
    id 641
    label "work"
  ]
  node [
    id 642
    label "reakcja_chemiczna"
  ]
  node [
    id 643
    label "function"
  ]
  node [
    id 644
    label "commit"
  ]
  node [
    id 645
    label "bangla&#263;"
  ]
  node [
    id 646
    label "determine"
  ]
  node [
    id 647
    label "tryb"
  ]
  node [
    id 648
    label "powodowa&#263;"
  ]
  node [
    id 649
    label "dziama&#263;"
  ]
  node [
    id 650
    label "istnie&#263;"
  ]
  node [
    id 651
    label "stoisko"
  ]
  node [
    id 652
    label "plac"
  ]
  node [
    id 653
    label "emitowanie"
  ]
  node [
    id 654
    label "targowica"
  ]
  node [
    id 655
    label "emitowa&#263;"
  ]
  node [
    id 656
    label "wprowadzanie"
  ]
  node [
    id 657
    label "wprowadzi&#263;"
  ]
  node [
    id 658
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 659
    label "rynek_wt&#243;rny"
  ]
  node [
    id 660
    label "wprowadzenie"
  ]
  node [
    id 661
    label "kram"
  ]
  node [
    id 662
    label "wprowadza&#263;"
  ]
  node [
    id 663
    label "pojawienie_si&#281;"
  ]
  node [
    id 664
    label "rynek_podstawowy"
  ]
  node [
    id 665
    label "biznes"
  ]
  node [
    id 666
    label "gospodarka"
  ]
  node [
    id 667
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 668
    label "obiekt_handlowy"
  ]
  node [
    id 669
    label "konsument"
  ]
  node [
    id 670
    label "wytw&#243;rca"
  ]
  node [
    id 671
    label "segment_rynku"
  ]
  node [
    id 672
    label "wska&#378;nik_rynkowy"
  ]
  node [
    id 673
    label "rynek_r&#243;wnoleg&#322;y"
  ]
  node [
    id 674
    label "troch&#281;"
  ]
  node [
    id 675
    label "prozdrowotny"
  ]
  node [
    id 676
    label "zdrowotnie"
  ]
  node [
    id 677
    label "nijaki"
  ]
  node [
    id 678
    label "zezwala&#263;"
  ]
  node [
    id 679
    label "dawa&#263;"
  ]
  node [
    id 680
    label "assign"
  ]
  node [
    id 681
    label "render"
  ]
  node [
    id 682
    label "accord"
  ]
  node [
    id 683
    label "odst&#281;powa&#263;"
  ]
  node [
    id 684
    label "przyznawa&#263;"
  ]
  node [
    id 685
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 686
    label "dokument"
  ]
  node [
    id 687
    label "zrobienie"
  ]
  node [
    id 688
    label "pofolgowanie"
  ]
  node [
    id 689
    label "odwieszenie"
  ]
  node [
    id 690
    label "uznanie"
  ]
  node [
    id 691
    label "authorization"
  ]
  node [
    id 692
    label "dostawianie"
  ]
  node [
    id 693
    label "dostawia&#263;"
  ]
  node [
    id 694
    label "transport"
  ]
  node [
    id 695
    label "dostawienie"
  ]
  node [
    id 696
    label "dostawi&#263;"
  ]
  node [
    id 697
    label "partia"
  ]
  node [
    id 698
    label "Unia_Wolno&#347;ci"
  ]
  node [
    id 699
    label "Unia_Europejska"
  ]
  node [
    id 700
    label "combination"
  ]
  node [
    id 701
    label "union"
  ]
  node [
    id 702
    label "Unia"
  ]
  node [
    id 703
    label "zakres"
  ]
  node [
    id 704
    label "dodatek"
  ]
  node [
    id 705
    label "struktura"
  ]
  node [
    id 706
    label "stela&#380;"
  ]
  node [
    id 707
    label "za&#322;o&#380;enie"
  ]
  node [
    id 708
    label "human_body"
  ]
  node [
    id 709
    label "szablon"
  ]
  node [
    id 710
    label "oprawa"
  ]
  node [
    id 711
    label "paczka"
  ]
  node [
    id 712
    label "obramowanie"
  ]
  node [
    id 713
    label "pojazd"
  ]
  node [
    id 714
    label "postawa"
  ]
  node [
    id 715
    label "element_konstrukcyjny"
  ]
  node [
    id 716
    label "ceniony"
  ]
  node [
    id 717
    label "uraz"
  ]
  node [
    id 718
    label "nastawianie"
  ]
  node [
    id 719
    label "nastawia&#263;"
  ]
  node [
    id 720
    label "z&#322;o&#380;enie"
  ]
  node [
    id 721
    label "transgresja"
  ]
  node [
    id 722
    label "szyjka_udowa"
  ]
  node [
    id 723
    label "interruption"
  ]
  node [
    id 724
    label "fracture"
  ]
  node [
    id 725
    label "nastawi&#263;"
  ]
  node [
    id 726
    label "przygn&#281;bienie"
  ]
  node [
    id 727
    label "wygranie"
  ]
  node [
    id 728
    label "dislocation"
  ]
  node [
    id 729
    label "discourtesy"
  ]
  node [
    id 730
    label "gips"
  ]
  node [
    id 731
    label "nastawienie"
  ]
  node [
    id 732
    label "prze&#322;amanie_si&#281;"
  ]
  node [
    id 733
    label "za&#322;amanie_si&#281;"
  ]
  node [
    id 734
    label "podzielenie"
  ]
  node [
    id 735
    label "wy&#322;amanie"
  ]
  node [
    id 736
    label "obserwacja"
  ]
  node [
    id 737
    label "moralno&#347;&#263;"
  ]
  node [
    id 738
    label "podstawa"
  ]
  node [
    id 739
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 740
    label "umowa"
  ]
  node [
    id 741
    label "dominion"
  ]
  node [
    id 742
    label "qualification"
  ]
  node [
    id 743
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 744
    label "opis"
  ]
  node [
    id 745
    label "regu&#322;a_Allena"
  ]
  node [
    id 746
    label "normalizacja"
  ]
  node [
    id 747
    label "regu&#322;a_Glogera"
  ]
  node [
    id 748
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 749
    label "standard"
  ]
  node [
    id 750
    label "base"
  ]
  node [
    id 751
    label "prawid&#322;o"
  ]
  node [
    id 752
    label "prawo_Mendla"
  ]
  node [
    id 753
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 754
    label "criterion"
  ]
  node [
    id 755
    label "twierdzenie"
  ]
  node [
    id 756
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 757
    label "occupation"
  ]
  node [
    id 758
    label "zasada_d'Alemberta"
  ]
  node [
    id 759
    label "niezale&#380;ny"
  ]
  node [
    id 760
    label "naturalny"
  ]
  node [
    id 761
    label "bezpruderyjny"
  ]
  node [
    id 762
    label "wygodny"
  ]
  node [
    id 763
    label "obieg"
  ]
  node [
    id 764
    label "ruch"
  ]
  node [
    id 765
    label "flux"
  ]
  node [
    id 766
    label "obr&#243;t_handlowy"
  ]
  node [
    id 767
    label "rzuca&#263;"
  ]
  node [
    id 768
    label "tkanina"
  ]
  node [
    id 769
    label "rzucenie"
  ]
  node [
    id 770
    label "tandeta"
  ]
  node [
    id 771
    label "naszprycowa&#263;"
  ]
  node [
    id 772
    label "&#322;&#243;dzki"
  ]
  node [
    id 773
    label "&#347;rodek_psychoaktywny"
  ]
  node [
    id 774
    label "naszprycowanie"
  ]
  node [
    id 775
    label "za&#322;adownia"
  ]
  node [
    id 776
    label "szprycowa&#263;"
  ]
  node [
    id 777
    label "rzuci&#263;"
  ]
  node [
    id 778
    label "wyr&#243;b"
  ]
  node [
    id 779
    label "szprycowanie"
  ]
  node [
    id 780
    label "asortyment"
  ]
  node [
    id 781
    label "poci&#261;gaj&#261;cy"
  ]
  node [
    id 782
    label "rzucanie"
  ]
  node [
    id 783
    label "narkobiznes"
  ]
  node [
    id 784
    label "metka"
  ]
  node [
    id 785
    label "zbi&#243;r"
  ]
  node [
    id 786
    label "komisja"
  ]
  node [
    id 787
    label "krajowy"
  ]
  node [
    id 788
    label "plan"
  ]
  node [
    id 789
    label "pob&#243;r"
  ]
  node [
    id 790
    label "pr&#243;bka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 22
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 19
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 16
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 9
    target 119
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 129
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 130
  ]
  edge [
    source 10
    target 131
  ]
  edge [
    source 10
    target 132
  ]
  edge [
    source 10
    target 133
  ]
  edge [
    source 10
    target 134
  ]
  edge [
    source 10
    target 26
  ]
  edge [
    source 10
    target 36
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 135
  ]
  edge [
    source 11
    target 136
  ]
  edge [
    source 11
    target 137
  ]
  edge [
    source 11
    target 138
  ]
  edge [
    source 11
    target 139
  ]
  edge [
    source 11
    target 140
  ]
  edge [
    source 11
    target 141
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 72
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 142
  ]
  edge [
    source 13
    target 143
  ]
  edge [
    source 13
    target 144
  ]
  edge [
    source 13
    target 145
  ]
  edge [
    source 13
    target 146
  ]
  edge [
    source 13
    target 147
  ]
  edge [
    source 13
    target 148
  ]
  edge [
    source 13
    target 149
  ]
  edge [
    source 13
    target 150
  ]
  edge [
    source 13
    target 151
  ]
  edge [
    source 13
    target 152
  ]
  edge [
    source 13
    target 153
  ]
  edge [
    source 13
    target 154
  ]
  edge [
    source 13
    target 155
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 156
  ]
  edge [
    source 14
    target 157
  ]
  edge [
    source 14
    target 158
  ]
  edge [
    source 14
    target 159
  ]
  edge [
    source 14
    target 160
  ]
  edge [
    source 14
    target 161
  ]
  edge [
    source 14
    target 162
  ]
  edge [
    source 14
    target 163
  ]
  edge [
    source 14
    target 164
  ]
  edge [
    source 14
    target 165
  ]
  edge [
    source 14
    target 166
  ]
  edge [
    source 14
    target 167
  ]
  edge [
    source 14
    target 31
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 31
  ]
  edge [
    source 15
    target 32
  ]
  edge [
    source 15
    target 42
  ]
  edge [
    source 15
    target 43
  ]
  edge [
    source 15
    target 58
  ]
  edge [
    source 15
    target 168
  ]
  edge [
    source 15
    target 169
  ]
  edge [
    source 15
    target 170
  ]
  edge [
    source 15
    target 171
  ]
  edge [
    source 15
    target 172
  ]
  edge [
    source 15
    target 173
  ]
  edge [
    source 15
    target 174
  ]
  edge [
    source 15
    target 175
  ]
  edge [
    source 15
    target 176
  ]
  edge [
    source 15
    target 177
  ]
  edge [
    source 15
    target 178
  ]
  edge [
    source 15
    target 179
  ]
  edge [
    source 15
    target 180
  ]
  edge [
    source 15
    target 181
  ]
  edge [
    source 15
    target 182
  ]
  edge [
    source 15
    target 183
  ]
  edge [
    source 15
    target 184
  ]
  edge [
    source 15
    target 185
  ]
  edge [
    source 15
    target 186
  ]
  edge [
    source 15
    target 187
  ]
  edge [
    source 15
    target 28
  ]
  edge [
    source 15
    target 40
  ]
  edge [
    source 15
    target 20
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 48
  ]
  edge [
    source 16
    target 33
  ]
  edge [
    source 16
    target 20
  ]
  edge [
    source 16
    target 36
  ]
  edge [
    source 16
    target 188
  ]
  edge [
    source 16
    target 189
  ]
  edge [
    source 16
    target 190
  ]
  edge [
    source 16
    target 191
  ]
  edge [
    source 16
    target 192
  ]
  edge [
    source 16
    target 193
  ]
  edge [
    source 16
    target 194
  ]
  edge [
    source 16
    target 43
  ]
  edge [
    source 16
    target 19
  ]
  edge [
    source 16
    target 38
  ]
  edge [
    source 16
    target 22
  ]
  edge [
    source 16
    target 16
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 56
  ]
  edge [
    source 17
    target 52
  ]
  edge [
    source 17
    target 195
  ]
  edge [
    source 17
    target 196
  ]
  edge [
    source 17
    target 197
  ]
  edge [
    source 17
    target 198
  ]
  edge [
    source 17
    target 199
  ]
  edge [
    source 17
    target 36
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 200
  ]
  edge [
    source 18
    target 201
  ]
  edge [
    source 18
    target 202
  ]
  edge [
    source 18
    target 203
  ]
  edge [
    source 18
    target 204
  ]
  edge [
    source 18
    target 205
  ]
  edge [
    source 18
    target 206
  ]
  edge [
    source 18
    target 207
  ]
  edge [
    source 18
    target 208
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 88
  ]
  edge [
    source 19
    target 209
  ]
  edge [
    source 19
    target 210
  ]
  edge [
    source 19
    target 211
  ]
  edge [
    source 19
    target 212
  ]
  edge [
    source 19
    target 213
  ]
  edge [
    source 19
    target 214
  ]
  edge [
    source 19
    target 38
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 54
  ]
  edge [
    source 20
    target 215
  ]
  edge [
    source 20
    target 216
  ]
  edge [
    source 20
    target 33
  ]
  edge [
    source 20
    target 51
  ]
  edge [
    source 20
    target 58
  ]
  edge [
    source 20
    target 65
  ]
  edge [
    source 20
    target 40
  ]
  edge [
    source 21
    target 117
  ]
  edge [
    source 21
    target 217
  ]
  edge [
    source 21
    target 218
  ]
  edge [
    source 21
    target 69
  ]
  edge [
    source 21
    target 60
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 40
  ]
  edge [
    source 22
    target 36
  ]
  edge [
    source 22
    target 219
  ]
  edge [
    source 22
    target 220
  ]
  edge [
    source 22
    target 221
  ]
  edge [
    source 22
    target 57
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 222
  ]
  edge [
    source 23
    target 223
  ]
  edge [
    source 23
    target 224
  ]
  edge [
    source 23
    target 225
  ]
  edge [
    source 23
    target 61
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 36
  ]
  edge [
    source 24
    target 40
  ]
  edge [
    source 24
    target 33
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 226
  ]
  edge [
    source 25
    target 227
  ]
  edge [
    source 25
    target 228
  ]
  edge [
    source 25
    target 229
  ]
  edge [
    source 25
    target 230
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 36
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 40
  ]
  edge [
    source 27
    target 224
  ]
  edge [
    source 27
    target 222
  ]
  edge [
    source 27
    target 45
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 231
  ]
  edge [
    source 28
    target 232
  ]
  edge [
    source 28
    target 233
  ]
  edge [
    source 28
    target 234
  ]
  edge [
    source 28
    target 235
  ]
  edge [
    source 28
    target 236
  ]
  edge [
    source 28
    target 237
  ]
  edge [
    source 28
    target 238
  ]
  edge [
    source 28
    target 239
  ]
  edge [
    source 28
    target 240
  ]
  edge [
    source 28
    target 241
  ]
  edge [
    source 28
    target 242
  ]
  edge [
    source 28
    target 243
  ]
  edge [
    source 28
    target 244
  ]
  edge [
    source 28
    target 245
  ]
  edge [
    source 28
    target 246
  ]
  edge [
    source 28
    target 247
  ]
  edge [
    source 28
    target 248
  ]
  edge [
    source 28
    target 249
  ]
  edge [
    source 28
    target 250
  ]
  edge [
    source 28
    target 251
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 252
  ]
  edge [
    source 29
    target 212
  ]
  edge [
    source 29
    target 253
  ]
  edge [
    source 29
    target 254
  ]
  edge [
    source 30
    target 36
  ]
  edge [
    source 30
    target 62
  ]
  edge [
    source 30
    target 95
  ]
  edge [
    source 30
    target 255
  ]
  edge [
    source 30
    target 256
  ]
  edge [
    source 30
    target 257
  ]
  edge [
    source 30
    target 258
  ]
  edge [
    source 30
    target 259
  ]
  edge [
    source 30
    target 260
  ]
  edge [
    source 30
    target 261
  ]
  edge [
    source 30
    target 262
  ]
  edge [
    source 30
    target 263
  ]
  edge [
    source 30
    target 264
  ]
  edge [
    source 30
    target 265
  ]
  edge [
    source 30
    target 266
  ]
  edge [
    source 30
    target 267
  ]
  edge [
    source 30
    target 268
  ]
  edge [
    source 30
    target 269
  ]
  edge [
    source 30
    target 270
  ]
  edge [
    source 30
    target 271
  ]
  edge [
    source 30
    target 272
  ]
  edge [
    source 30
    target 273
  ]
  edge [
    source 30
    target 274
  ]
  edge [
    source 30
    target 275
  ]
  edge [
    source 30
    target 276
  ]
  edge [
    source 30
    target 277
  ]
  edge [
    source 30
    target 278
  ]
  edge [
    source 30
    target 279
  ]
  edge [
    source 30
    target 280
  ]
  edge [
    source 30
    target 281
  ]
  edge [
    source 30
    target 282
  ]
  edge [
    source 30
    target 283
  ]
  edge [
    source 30
    target 284
  ]
  edge [
    source 30
    target 285
  ]
  edge [
    source 30
    target 286
  ]
  edge [
    source 30
    target 287
  ]
  edge [
    source 30
    target 288
  ]
  edge [
    source 30
    target 289
  ]
  edge [
    source 30
    target 290
  ]
  edge [
    source 30
    target 291
  ]
  edge [
    source 30
    target 292
  ]
  edge [
    source 30
    target 293
  ]
  edge [
    source 30
    target 294
  ]
  edge [
    source 30
    target 295
  ]
  edge [
    source 30
    target 296
  ]
  edge [
    source 30
    target 297
  ]
  edge [
    source 30
    target 298
  ]
  edge [
    source 30
    target 299
  ]
  edge [
    source 30
    target 300
  ]
  edge [
    source 30
    target 108
  ]
  edge [
    source 30
    target 301
  ]
  edge [
    source 30
    target 302
  ]
  edge [
    source 30
    target 303
  ]
  edge [
    source 30
    target 304
  ]
  edge [
    source 30
    target 305
  ]
  edge [
    source 30
    target 306
  ]
  edge [
    source 30
    target 307
  ]
  edge [
    source 30
    target 308
  ]
  edge [
    source 30
    target 309
  ]
  edge [
    source 30
    target 310
  ]
  edge [
    source 30
    target 311
  ]
  edge [
    source 30
    target 312
  ]
  edge [
    source 30
    target 313
  ]
  edge [
    source 30
    target 314
  ]
  edge [
    source 30
    target 315
  ]
  edge [
    source 30
    target 316
  ]
  edge [
    source 30
    target 317
  ]
  edge [
    source 30
    target 318
  ]
  edge [
    source 30
    target 319
  ]
  edge [
    source 30
    target 320
  ]
  edge [
    source 30
    target 321
  ]
  edge [
    source 30
    target 322
  ]
  edge [
    source 30
    target 323
  ]
  edge [
    source 30
    target 324
  ]
  edge [
    source 30
    target 325
  ]
  edge [
    source 30
    target 326
  ]
  edge [
    source 30
    target 327
  ]
  edge [
    source 30
    target 328
  ]
  edge [
    source 30
    target 329
  ]
  edge [
    source 30
    target 330
  ]
  edge [
    source 30
    target 331
  ]
  edge [
    source 30
    target 332
  ]
  edge [
    source 30
    target 333
  ]
  edge [
    source 30
    target 334
  ]
  edge [
    source 30
    target 335
  ]
  edge [
    source 30
    target 336
  ]
  edge [
    source 30
    target 337
  ]
  edge [
    source 30
    target 338
  ]
  edge [
    source 30
    target 339
  ]
  edge [
    source 30
    target 340
  ]
  edge [
    source 30
    target 341
  ]
  edge [
    source 30
    target 342
  ]
  edge [
    source 30
    target 343
  ]
  edge [
    source 30
    target 344
  ]
  edge [
    source 30
    target 345
  ]
  edge [
    source 30
    target 346
  ]
  edge [
    source 30
    target 347
  ]
  edge [
    source 30
    target 348
  ]
  edge [
    source 30
    target 349
  ]
  edge [
    source 30
    target 350
  ]
  edge [
    source 30
    target 351
  ]
  edge [
    source 30
    target 352
  ]
  edge [
    source 30
    target 353
  ]
  edge [
    source 30
    target 354
  ]
  edge [
    source 30
    target 355
  ]
  edge [
    source 30
    target 356
  ]
  edge [
    source 30
    target 357
  ]
  edge [
    source 30
    target 358
  ]
  edge [
    source 30
    target 359
  ]
  edge [
    source 30
    target 360
  ]
  edge [
    source 30
    target 361
  ]
  edge [
    source 30
    target 362
  ]
  edge [
    source 30
    target 363
  ]
  edge [
    source 30
    target 364
  ]
  edge [
    source 30
    target 365
  ]
  edge [
    source 30
    target 366
  ]
  edge [
    source 30
    target 367
  ]
  edge [
    source 30
    target 368
  ]
  edge [
    source 30
    target 369
  ]
  edge [
    source 30
    target 370
  ]
  edge [
    source 30
    target 371
  ]
  edge [
    source 30
    target 372
  ]
  edge [
    source 30
    target 373
  ]
  edge [
    source 30
    target 374
  ]
  edge [
    source 30
    target 375
  ]
  edge [
    source 30
    target 376
  ]
  edge [
    source 30
    target 377
  ]
  edge [
    source 30
    target 378
  ]
  edge [
    source 30
    target 379
  ]
  edge [
    source 30
    target 380
  ]
  edge [
    source 30
    target 381
  ]
  edge [
    source 30
    target 382
  ]
  edge [
    source 30
    target 383
  ]
  edge [
    source 30
    target 384
  ]
  edge [
    source 30
    target 385
  ]
  edge [
    source 30
    target 386
  ]
  edge [
    source 30
    target 387
  ]
  edge [
    source 30
    target 388
  ]
  edge [
    source 30
    target 389
  ]
  edge [
    source 30
    target 390
  ]
  edge [
    source 30
    target 391
  ]
  edge [
    source 30
    target 392
  ]
  edge [
    source 30
    target 393
  ]
  edge [
    source 30
    target 394
  ]
  edge [
    source 30
    target 395
  ]
  edge [
    source 30
    target 396
  ]
  edge [
    source 30
    target 397
  ]
  edge [
    source 30
    target 398
  ]
  edge [
    source 30
    target 399
  ]
  edge [
    source 30
    target 400
  ]
  edge [
    source 30
    target 401
  ]
  edge [
    source 30
    target 402
  ]
  edge [
    source 30
    target 403
  ]
  edge [
    source 30
    target 404
  ]
  edge [
    source 30
    target 405
  ]
  edge [
    source 30
    target 406
  ]
  edge [
    source 30
    target 407
  ]
  edge [
    source 30
    target 408
  ]
  edge [
    source 30
    target 409
  ]
  edge [
    source 30
    target 410
  ]
  edge [
    source 30
    target 411
  ]
  edge [
    source 30
    target 412
  ]
  edge [
    source 30
    target 413
  ]
  edge [
    source 30
    target 414
  ]
  edge [
    source 30
    target 415
  ]
  edge [
    source 30
    target 416
  ]
  edge [
    source 30
    target 417
  ]
  edge [
    source 30
    target 418
  ]
  edge [
    source 30
    target 419
  ]
  edge [
    source 30
    target 420
  ]
  edge [
    source 30
    target 421
  ]
  edge [
    source 30
    target 422
  ]
  edge [
    source 30
    target 423
  ]
  edge [
    source 30
    target 424
  ]
  edge [
    source 30
    target 425
  ]
  edge [
    source 30
    target 426
  ]
  edge [
    source 30
    target 427
  ]
  edge [
    source 30
    target 428
  ]
  edge [
    source 30
    target 429
  ]
  edge [
    source 30
    target 430
  ]
  edge [
    source 30
    target 431
  ]
  edge [
    source 30
    target 432
  ]
  edge [
    source 30
    target 433
  ]
  edge [
    source 30
    target 434
  ]
  edge [
    source 30
    target 435
  ]
  edge [
    source 30
    target 436
  ]
  edge [
    source 30
    target 437
  ]
  edge [
    source 30
    target 438
  ]
  edge [
    source 30
    target 439
  ]
  edge [
    source 30
    target 440
  ]
  edge [
    source 30
    target 441
  ]
  edge [
    source 30
    target 442
  ]
  edge [
    source 30
    target 443
  ]
  edge [
    source 30
    target 444
  ]
  edge [
    source 30
    target 445
  ]
  edge [
    source 30
    target 446
  ]
  edge [
    source 30
    target 447
  ]
  edge [
    source 30
    target 448
  ]
  edge [
    source 30
    target 449
  ]
  edge [
    source 30
    target 450
  ]
  edge [
    source 30
    target 451
  ]
  edge [
    source 30
    target 452
  ]
  edge [
    source 30
    target 453
  ]
  edge [
    source 30
    target 454
  ]
  edge [
    source 30
    target 455
  ]
  edge [
    source 30
    target 456
  ]
  edge [
    source 30
    target 457
  ]
  edge [
    source 30
    target 96
  ]
  edge [
    source 30
    target 458
  ]
  edge [
    source 30
    target 459
  ]
  edge [
    source 30
    target 460
  ]
  edge [
    source 30
    target 461
  ]
  edge [
    source 30
    target 462
  ]
  edge [
    source 30
    target 463
  ]
  edge [
    source 30
    target 464
  ]
  edge [
    source 30
    target 465
  ]
  edge [
    source 30
    target 466
  ]
  edge [
    source 30
    target 467
  ]
  edge [
    source 30
    target 468
  ]
  edge [
    source 30
    target 469
  ]
  edge [
    source 30
    target 470
  ]
  edge [
    source 30
    target 471
  ]
  edge [
    source 30
    target 472
  ]
  edge [
    source 30
    target 473
  ]
  edge [
    source 30
    target 474
  ]
  edge [
    source 30
    target 475
  ]
  edge [
    source 30
    target 476
  ]
  edge [
    source 30
    target 477
  ]
  edge [
    source 30
    target 478
  ]
  edge [
    source 30
    target 479
  ]
  edge [
    source 30
    target 480
  ]
  edge [
    source 30
    target 481
  ]
  edge [
    source 30
    target 482
  ]
  edge [
    source 30
    target 483
  ]
  edge [
    source 30
    target 484
  ]
  edge [
    source 30
    target 485
  ]
  edge [
    source 30
    target 486
  ]
  edge [
    source 30
    target 487
  ]
  edge [
    source 30
    target 488
  ]
  edge [
    source 30
    target 489
  ]
  edge [
    source 30
    target 490
  ]
  edge [
    source 30
    target 491
  ]
  edge [
    source 30
    target 492
  ]
  edge [
    source 30
    target 493
  ]
  edge [
    source 30
    target 494
  ]
  edge [
    source 30
    target 495
  ]
  edge [
    source 30
    target 496
  ]
  edge [
    source 30
    target 497
  ]
  edge [
    source 30
    target 498
  ]
  edge [
    source 30
    target 499
  ]
  edge [
    source 30
    target 500
  ]
  edge [
    source 30
    target 501
  ]
  edge [
    source 30
    target 502
  ]
  edge [
    source 30
    target 503
  ]
  edge [
    source 30
    target 504
  ]
  edge [
    source 30
    target 505
  ]
  edge [
    source 30
    target 506
  ]
  edge [
    source 30
    target 507
  ]
  edge [
    source 30
    target 508
  ]
  edge [
    source 30
    target 509
  ]
  edge [
    source 30
    target 510
  ]
  edge [
    source 30
    target 511
  ]
  edge [
    source 30
    target 512
  ]
  edge [
    source 30
    target 513
  ]
  edge [
    source 30
    target 514
  ]
  edge [
    source 30
    target 515
  ]
  edge [
    source 30
    target 516
  ]
  edge [
    source 30
    target 517
  ]
  edge [
    source 30
    target 518
  ]
  edge [
    source 30
    target 519
  ]
  edge [
    source 30
    target 520
  ]
  edge [
    source 30
    target 521
  ]
  edge [
    source 30
    target 522
  ]
  edge [
    source 30
    target 523
  ]
  edge [
    source 30
    target 524
  ]
  edge [
    source 30
    target 75
  ]
  edge [
    source 30
    target 525
  ]
  edge [
    source 30
    target 526
  ]
  edge [
    source 30
    target 527
  ]
  edge [
    source 30
    target 528
  ]
  edge [
    source 30
    target 529
  ]
  edge [
    source 30
    target 530
  ]
  edge [
    source 30
    target 531
  ]
  edge [
    source 30
    target 532
  ]
  edge [
    source 30
    target 533
  ]
  edge [
    source 30
    target 534
  ]
  edge [
    source 30
    target 535
  ]
  edge [
    source 30
    target 536
  ]
  edge [
    source 30
    target 537
  ]
  edge [
    source 30
    target 538
  ]
  edge [
    source 30
    target 539
  ]
  edge [
    source 30
    target 540
  ]
  edge [
    source 30
    target 541
  ]
  edge [
    source 30
    target 542
  ]
  edge [
    source 30
    target 543
  ]
  edge [
    source 30
    target 544
  ]
  edge [
    source 30
    target 545
  ]
  edge [
    source 30
    target 546
  ]
  edge [
    source 30
    target 547
  ]
  edge [
    source 30
    target 548
  ]
  edge [
    source 30
    target 549
  ]
  edge [
    source 30
    target 550
  ]
  edge [
    source 30
    target 551
  ]
  edge [
    source 30
    target 552
  ]
  edge [
    source 30
    target 553
  ]
  edge [
    source 30
    target 554
  ]
  edge [
    source 30
    target 555
  ]
  edge [
    source 30
    target 556
  ]
  edge [
    source 30
    target 557
  ]
  edge [
    source 30
    target 558
  ]
  edge [
    source 30
    target 559
  ]
  edge [
    source 30
    target 560
  ]
  edge [
    source 30
    target 561
  ]
  edge [
    source 30
    target 562
  ]
  edge [
    source 30
    target 563
  ]
  edge [
    source 30
    target 564
  ]
  edge [
    source 30
    target 565
  ]
  edge [
    source 30
    target 566
  ]
  edge [
    source 30
    target 567
  ]
  edge [
    source 30
    target 568
  ]
  edge [
    source 30
    target 569
  ]
  edge [
    source 30
    target 570
  ]
  edge [
    source 30
    target 571
  ]
  edge [
    source 30
    target 572
  ]
  edge [
    source 31
    target 63
  ]
  edge [
    source 31
    target 64
  ]
  edge [
    source 31
    target 573
  ]
  edge [
    source 31
    target 574
  ]
  edge [
    source 31
    target 158
  ]
  edge [
    source 31
    target 575
  ]
  edge [
    source 31
    target 165
  ]
  edge [
    source 31
    target 786
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 47
  ]
  edge [
    source 32
    target 576
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 188
  ]
  edge [
    source 33
    target 189
  ]
  edge [
    source 33
    target 193
  ]
  edge [
    source 33
    target 51
  ]
  edge [
    source 33
    target 54
  ]
  edge [
    source 33
    target 58
  ]
  edge [
    source 33
    target 65
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 49
  ]
  edge [
    source 34
    target 55
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 577
  ]
  edge [
    source 35
    target 578
  ]
  edge [
    source 35
    target 579
  ]
  edge [
    source 35
    target 580
  ]
  edge [
    source 35
    target 581
  ]
  edge [
    source 35
    target 582
  ]
  edge [
    source 35
    target 583
  ]
  edge [
    source 35
    target 584
  ]
  edge [
    source 35
    target 585
  ]
  edge [
    source 35
    target 586
  ]
  edge [
    source 35
    target 587
  ]
  edge [
    source 35
    target 588
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 41
  ]
  edge [
    source 36
    target 49
  ]
  edge [
    source 36
    target 59
  ]
  edge [
    source 36
    target 589
  ]
  edge [
    source 36
    target 590
  ]
  edge [
    source 36
    target 591
  ]
  edge [
    source 36
    target 592
  ]
  edge [
    source 36
    target 593
  ]
  edge [
    source 36
    target 594
  ]
  edge [
    source 36
    target 595
  ]
  edge [
    source 36
    target 596
  ]
  edge [
    source 36
    target 597
  ]
  edge [
    source 36
    target 40
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 598
  ]
  edge [
    source 37
    target 599
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 600
  ]
  edge [
    source 38
    target 601
  ]
  edge [
    source 38
    target 602
  ]
  edge [
    source 38
    target 603
  ]
  edge [
    source 38
    target 604
  ]
  edge [
    source 38
    target 605
  ]
  edge [
    source 38
    target 141
  ]
  edge [
    source 38
    target 606
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 607
  ]
  edge [
    source 39
    target 608
  ]
  edge [
    source 40
    target 61
  ]
  edge [
    source 40
    target 608
  ]
  edge [
    source 40
    target 607
  ]
  edge [
    source 40
    target 609
  ]
  edge [
    source 40
    target 610
  ]
  edge [
    source 40
    target 611
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 612
  ]
  edge [
    source 41
    target 613
  ]
  edge [
    source 41
    target 614
  ]
  edge [
    source 41
    target 615
  ]
  edge [
    source 41
    target 76
  ]
  edge [
    source 41
    target 616
  ]
  edge [
    source 41
    target 617
  ]
  edge [
    source 41
    target 43
  ]
  edge [
    source 41
    target 66
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 45
  ]
  edge [
    source 43
    target 46
  ]
  edge [
    source 43
    target 189
  ]
  edge [
    source 43
    target 193
  ]
  edge [
    source 43
    target 66
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 70
  ]
  edge [
    source 45
    target 618
  ]
  edge [
    source 45
    target 619
  ]
  edge [
    source 45
    target 620
  ]
  edge [
    source 45
    target 621
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 622
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 623
  ]
  edge [
    source 47
    target 624
  ]
  edge [
    source 47
    target 625
  ]
  edge [
    source 47
    target 580
  ]
  edge [
    source 47
    target 626
  ]
  edge [
    source 47
    target 627
  ]
  edge [
    source 47
    target 628
  ]
  edge [
    source 47
    target 629
  ]
  edge [
    source 47
    target 630
  ]
  edge [
    source 47
    target 631
  ]
  edge [
    source 47
    target 632
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 633
  ]
  edge [
    source 49
    target 634
  ]
  edge [
    source 49
    target 635
  ]
  edge [
    source 49
    target 636
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 57
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 637
  ]
  edge [
    source 51
    target 638
  ]
  edge [
    source 51
    target 639
  ]
  edge [
    source 51
    target 640
  ]
  edge [
    source 51
    target 54
  ]
  edge [
    source 51
    target 58
  ]
  edge [
    source 51
    target 65
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 628
  ]
  edge [
    source 53
    target 641
  ]
  edge [
    source 53
    target 642
  ]
  edge [
    source 53
    target 643
  ]
  edge [
    source 53
    target 644
  ]
  edge [
    source 53
    target 645
  ]
  edge [
    source 53
    target 633
  ]
  edge [
    source 53
    target 646
  ]
  edge [
    source 53
    target 647
  ]
  edge [
    source 53
    target 648
  ]
  edge [
    source 53
    target 649
  ]
  edge [
    source 53
    target 650
  ]
  edge [
    source 54
    target 651
  ]
  edge [
    source 54
    target 652
  ]
  edge [
    source 54
    target 653
  ]
  edge [
    source 54
    target 654
  ]
  edge [
    source 54
    target 655
  ]
  edge [
    source 54
    target 656
  ]
  edge [
    source 54
    target 657
  ]
  edge [
    source 54
    target 658
  ]
  edge [
    source 54
    target 659
  ]
  edge [
    source 54
    target 660
  ]
  edge [
    source 54
    target 661
  ]
  edge [
    source 54
    target 662
  ]
  edge [
    source 54
    target 663
  ]
  edge [
    source 54
    target 664
  ]
  edge [
    source 54
    target 665
  ]
  edge [
    source 54
    target 666
  ]
  edge [
    source 54
    target 667
  ]
  edge [
    source 54
    target 668
  ]
  edge [
    source 54
    target 669
  ]
  edge [
    source 54
    target 670
  ]
  edge [
    source 54
    target 671
  ]
  edge [
    source 54
    target 672
  ]
  edge [
    source 54
    target 673
  ]
  edge [
    source 54
    target 58
  ]
  edge [
    source 54
    target 65
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 56
    target 674
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 637
  ]
  edge [
    source 57
    target 160
  ]
  edge [
    source 57
    target 675
  ]
  edge [
    source 57
    target 676
  ]
  edge [
    source 58
    target 677
  ]
  edge [
    source 58
    target 65
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 678
  ]
  edge [
    source 59
    target 679
  ]
  edge [
    source 59
    target 680
  ]
  edge [
    source 59
    target 681
  ]
  edge [
    source 59
    target 682
  ]
  edge [
    source 59
    target 683
  ]
  edge [
    source 59
    target 684
  ]
  edge [
    source 59
    target 685
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 686
  ]
  edge [
    source 60
    target 687
  ]
  edge [
    source 60
    target 142
  ]
  edge [
    source 60
    target 144
  ]
  edge [
    source 60
    target 145
  ]
  edge [
    source 60
    target 146
  ]
  edge [
    source 60
    target 147
  ]
  edge [
    source 60
    target 155
  ]
  edge [
    source 60
    target 148
  ]
  edge [
    source 60
    target 688
  ]
  edge [
    source 60
    target 152
  ]
  edge [
    source 60
    target 154
  ]
  edge [
    source 60
    target 689
  ]
  edge [
    source 60
    target 690
  ]
  edge [
    source 60
    target 691
  ]
  edge [
    source 61
    target 692
  ]
  edge [
    source 61
    target 693
  ]
  edge [
    source 61
    target 694
  ]
  edge [
    source 61
    target 695
  ]
  edge [
    source 61
    target 696
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 63
    target 168
  ]
  edge [
    source 63
    target 697
  ]
  edge [
    source 63
    target 75
  ]
  edge [
    source 63
    target 698
  ]
  edge [
    source 63
    target 699
  ]
  edge [
    source 63
    target 700
  ]
  edge [
    source 63
    target 701
  ]
  edge [
    source 63
    target 702
  ]
  edge [
    source 64
    target 703
  ]
  edge [
    source 64
    target 704
  ]
  edge [
    source 64
    target 705
  ]
  edge [
    source 64
    target 706
  ]
  edge [
    source 64
    target 707
  ]
  edge [
    source 64
    target 708
  ]
  edge [
    source 64
    target 709
  ]
  edge [
    source 64
    target 710
  ]
  edge [
    source 64
    target 711
  ]
  edge [
    source 64
    target 181
  ]
  edge [
    source 64
    target 712
  ]
  edge [
    source 64
    target 713
  ]
  edge [
    source 64
    target 714
  ]
  edge [
    source 64
    target 715
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 66
    target 716
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 67
    target 717
  ]
  edge [
    source 67
    target 687
  ]
  edge [
    source 67
    target 718
  ]
  edge [
    source 67
    target 719
  ]
  edge [
    source 67
    target 720
  ]
  edge [
    source 67
    target 721
  ]
  edge [
    source 67
    target 722
  ]
  edge [
    source 67
    target 723
  ]
  edge [
    source 67
    target 724
  ]
  edge [
    source 67
    target 725
  ]
  edge [
    source 67
    target 726
  ]
  edge [
    source 67
    target 727
  ]
  edge [
    source 67
    target 728
  ]
  edge [
    source 67
    target 729
  ]
  edge [
    source 67
    target 730
  ]
  edge [
    source 67
    target 731
  ]
  edge [
    source 67
    target 732
  ]
  edge [
    source 67
    target 733
  ]
  edge [
    source 67
    target 734
  ]
  edge [
    source 67
    target 735
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 68
    target 736
  ]
  edge [
    source 68
    target 737
  ]
  edge [
    source 68
    target 738
  ]
  edge [
    source 68
    target 739
  ]
  edge [
    source 68
    target 740
  ]
  edge [
    source 68
    target 741
  ]
  edge [
    source 68
    target 742
  ]
  edge [
    source 68
    target 743
  ]
  edge [
    source 68
    target 744
  ]
  edge [
    source 68
    target 745
  ]
  edge [
    source 68
    target 746
  ]
  edge [
    source 68
    target 747
  ]
  edge [
    source 68
    target 748
  ]
  edge [
    source 68
    target 749
  ]
  edge [
    source 68
    target 750
  ]
  edge [
    source 68
    target 212
  ]
  edge [
    source 68
    target 213
  ]
  edge [
    source 68
    target 751
  ]
  edge [
    source 68
    target 752
  ]
  edge [
    source 68
    target 753
  ]
  edge [
    source 68
    target 754
  ]
  edge [
    source 68
    target 755
  ]
  edge [
    source 68
    target 756
  ]
  edge [
    source 68
    target 76
  ]
  edge [
    source 68
    target 757
  ]
  edge [
    source 68
    target 758
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 69
    target 759
  ]
  edge [
    source 69
    target 760
  ]
  edge [
    source 69
    target 761
  ]
  edge [
    source 69
    target 116
  ]
  edge [
    source 69
    target 762
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 763
  ]
  edge [
    source 70
    target 764
  ]
  edge [
    source 70
    target 765
  ]
  edge [
    source 71
    target 73
  ]
  edge [
    source 71
    target 766
  ]
  edge [
    source 71
    target 767
  ]
  edge [
    source 71
    target 768
  ]
  edge [
    source 71
    target 769
  ]
  edge [
    source 71
    target 770
  ]
  edge [
    source 71
    target 771
  ]
  edge [
    source 71
    target 772
  ]
  edge [
    source 71
    target 773
  ]
  edge [
    source 71
    target 774
  ]
  edge [
    source 71
    target 775
  ]
  edge [
    source 71
    target 776
  ]
  edge [
    source 71
    target 777
  ]
  edge [
    source 71
    target 778
  ]
  edge [
    source 71
    target 779
  ]
  edge [
    source 71
    target 780
  ]
  edge [
    source 71
    target 781
  ]
  edge [
    source 71
    target 782
  ]
  edge [
    source 71
    target 783
  ]
  edge [
    source 71
    target 784
  ]
  edge [
    source 71
    target 785
  ]
  edge [
    source 787
    target 788
  ]
  edge [
    source 787
    target 789
  ]
  edge [
    source 787
    target 790
  ]
  edge [
    source 788
    target 789
  ]
  edge [
    source 788
    target 790
  ]
  edge [
    source 789
    target 790
  ]
]
