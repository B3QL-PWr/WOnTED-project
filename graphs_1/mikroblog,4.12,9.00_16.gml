graph [
  maxDegree 42
  minDegree 1
  meanDegree 2
  density 0.024096385542168676
  graphCliqueNumber 2
  node [
    id 0
    label "dwa"
    origin "text"
  ]
  node [
    id 1
    label "kot"
    origin "text"
  ]
  node [
    id 2
    label "wygranie"
    origin "text"
  ]
  node [
    id 3
    label "&#380;ycie"
    origin "text"
  ]
  node [
    id 4
    label "poleca&#263;"
    origin "text"
  ]
  node [
    id 5
    label "pokazkota"
    origin "text"
  ]
  node [
    id 6
    label "duszekitygrysek"
    origin "text"
  ]
  node [
    id 7
    label "zamiaucze&#263;"
  ]
  node [
    id 8
    label "pierwszoklasista"
  ]
  node [
    id 9
    label "fala"
  ]
  node [
    id 10
    label "miaucze&#263;"
  ]
  node [
    id 11
    label "kabanos"
  ]
  node [
    id 12
    label "kotowate"
  ]
  node [
    id 13
    label "miaukni&#281;cie"
  ]
  node [
    id 14
    label "samiec"
  ]
  node [
    id 15
    label "otrz&#281;siny"
  ]
  node [
    id 16
    label "miauczenie"
  ]
  node [
    id 17
    label "czworon&#243;g"
  ]
  node [
    id 18
    label "ssak_drapie&#380;ny"
  ]
  node [
    id 19
    label "rekrut"
  ]
  node [
    id 20
    label "zaj&#261;c"
  ]
  node [
    id 21
    label "kotwica"
  ]
  node [
    id 22
    label "trackball"
  ]
  node [
    id 23
    label "felinoterapia"
  ]
  node [
    id 24
    label "odk&#322;aczacz"
  ]
  node [
    id 25
    label "zamiauczenie"
  ]
  node [
    id 26
    label "zrobienie"
  ]
  node [
    id 27
    label "zwojowanie"
  ]
  node [
    id 28
    label "beat"
  ]
  node [
    id 29
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 30
    label "zapanowanie"
  ]
  node [
    id 31
    label "zniesienie"
  ]
  node [
    id 32
    label "zdarzenie_si&#281;"
  ]
  node [
    id 33
    label "wygrywanie"
  ]
  node [
    id 34
    label "wr&#243;cenie_z_tarcz&#261;"
  ]
  node [
    id 35
    label "energy"
  ]
  node [
    id 36
    label "czas"
  ]
  node [
    id 37
    label "bycie"
  ]
  node [
    id 38
    label "zegar_biologiczny"
  ]
  node [
    id 39
    label "okres_noworodkowy"
  ]
  node [
    id 40
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 41
    label "entity"
  ]
  node [
    id 42
    label "prze&#380;ywanie"
  ]
  node [
    id 43
    label "prze&#380;ycie"
  ]
  node [
    id 44
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 45
    label "wiek_matuzalemowy"
  ]
  node [
    id 46
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 47
    label "dzieci&#324;stwo"
  ]
  node [
    id 48
    label "power"
  ]
  node [
    id 49
    label "szwung"
  ]
  node [
    id 50
    label "menopauza"
  ]
  node [
    id 51
    label "umarcie"
  ]
  node [
    id 52
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 53
    label "life"
  ]
  node [
    id 54
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 55
    label "&#380;ywy"
  ]
  node [
    id 56
    label "rozw&#243;j"
  ]
  node [
    id 57
    label "po&#322;&#243;g"
  ]
  node [
    id 58
    label "byt"
  ]
  node [
    id 59
    label "przebywanie"
  ]
  node [
    id 60
    label "subsistence"
  ]
  node [
    id 61
    label "koleje_losu"
  ]
  node [
    id 62
    label "raj_utracony"
  ]
  node [
    id 63
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 64
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 65
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 66
    label "andropauza"
  ]
  node [
    id 67
    label "warunki"
  ]
  node [
    id 68
    label "do&#380;ywanie"
  ]
  node [
    id 69
    label "niemowl&#281;ctwo"
  ]
  node [
    id 70
    label "umieranie"
  ]
  node [
    id 71
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 72
    label "staro&#347;&#263;"
  ]
  node [
    id 73
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 74
    label "&#347;mier&#263;"
  ]
  node [
    id 75
    label "control"
  ]
  node [
    id 76
    label "placard"
  ]
  node [
    id 77
    label "m&#243;wi&#263;"
  ]
  node [
    id 78
    label "charge"
  ]
  node [
    id 79
    label "zadawa&#263;"
  ]
  node [
    id 80
    label "ordynowa&#263;"
  ]
  node [
    id 81
    label "powierza&#263;"
  ]
  node [
    id 82
    label "wydawa&#263;"
  ]
  node [
    id 83
    label "doradza&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 5
    target 6
  ]
]
