graph [
  maxDegree 8
  minDegree 1
  meanDegree 1.9310344827586208
  density 0.06896551724137931
  graphCliqueNumber 2
  node [
    id 0
    label "zniewoli&#263;"
    origin "text"
  ]
  node [
    id 1
    label "okoliczno&#347;&#263;"
    origin "text"
  ]
  node [
    id 2
    label "ahmet"
    origin "text"
  ]
  node [
    id 3
    label "przedsi&#281;bra&#263;"
    origin "text"
  ]
  node [
    id 4
    label "energiczny"
    origin "text"
  ]
  node [
    id 5
    label "postanowienie"
    origin "text"
  ]
  node [
    id 6
    label "narzuci&#263;"
  ]
  node [
    id 7
    label "winnings"
  ]
  node [
    id 8
    label "dosta&#263;"
  ]
  node [
    id 9
    label "manipulate"
  ]
  node [
    id 10
    label "zachwyci&#263;"
  ]
  node [
    id 11
    label "zdoby&#263;_przychylno&#347;&#263;"
  ]
  node [
    id 12
    label "podporz&#261;dkowa&#263;"
  ]
  node [
    id 13
    label "wydarzenie"
  ]
  node [
    id 14
    label "sk&#322;adnik"
  ]
  node [
    id 15
    label "warunki"
  ]
  node [
    id 16
    label "sytuacja"
  ]
  node [
    id 17
    label "robi&#263;"
  ]
  node [
    id 18
    label "draw"
  ]
  node [
    id 19
    label "jary"
  ]
  node [
    id 20
    label "energicznie"
  ]
  node [
    id 21
    label "&#380;ywy"
  ]
  node [
    id 22
    label "ostry"
  ]
  node [
    id 23
    label "mocny"
  ]
  node [
    id 24
    label "zrobienie"
  ]
  node [
    id 25
    label "wypowied&#378;"
  ]
  node [
    id 26
    label "decyzja"
  ]
  node [
    id 27
    label "podj&#281;cie"
  ]
  node [
    id 28
    label "judgment"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
]
