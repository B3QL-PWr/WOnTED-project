graph [
  maxDegree 3
  minDegree 1
  meanDegree 1.5
  density 0.5
  graphCliqueNumber 2
  node [
    id 0
    label "uwodzi&#263;"
    origin "text"
  ]
  node [
    id 1
    label "zaleca&#263;_si&#281;"
  ]
  node [
    id 2
    label "denounce"
  ]
  node [
    id 3
    label "ba&#322;amuci&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
]
