graph [
  maxDegree 22
  minDegree 1
  meanDegree 1.9770114942528736
  density 0.022988505747126436
  graphCliqueNumber 2
  node [
    id 0
    label "polski"
    origin "text"
  ]
  node [
    id 1
    label "g&#243;rnictwo"
    origin "text"
  ]
  node [
    id 2
    label "dostawa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "miliard"
    origin "text"
  ]
  node [
    id 4
    label "z&#322;oty"
    origin "text"
  ]
  node [
    id 5
    label "wsparcie"
    origin "text"
  ]
  node [
    id 6
    label "lacki"
  ]
  node [
    id 7
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 8
    label "przedmiot"
  ]
  node [
    id 9
    label "sztajer"
  ]
  node [
    id 10
    label "drabant"
  ]
  node [
    id 11
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 12
    label "polak"
  ]
  node [
    id 13
    label "pierogi_ruskie"
  ]
  node [
    id 14
    label "krakowiak"
  ]
  node [
    id 15
    label "Polish"
  ]
  node [
    id 16
    label "j&#281;zyk"
  ]
  node [
    id 17
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 18
    label "oberek"
  ]
  node [
    id 19
    label "po_polsku"
  ]
  node [
    id 20
    label "mazur"
  ]
  node [
    id 21
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 22
    label "chodzony"
  ]
  node [
    id 23
    label "skoczny"
  ]
  node [
    id 24
    label "ryba_po_grecku"
  ]
  node [
    id 25
    label "goniony"
  ]
  node [
    id 26
    label "polsko"
  ]
  node [
    id 27
    label "przemys&#322;_ci&#281;&#380;ki"
  ]
  node [
    id 28
    label "wydobycie"
  ]
  node [
    id 29
    label "wiertnictwo"
  ]
  node [
    id 30
    label "wydobywanie"
  ]
  node [
    id 31
    label "krzeska"
  ]
  node [
    id 32
    label "solnictwo"
  ]
  node [
    id 33
    label "zgarniacz"
  ]
  node [
    id 34
    label "obrywak"
  ]
  node [
    id 35
    label "aerologia_g&#243;rnicza"
  ]
  node [
    id 36
    label "przesyp"
  ]
  node [
    id 37
    label "wydobywa&#263;"
  ]
  node [
    id 38
    label "rozpierak"
  ]
  node [
    id 39
    label "&#322;adownik"
  ]
  node [
    id 40
    label "wydoby&#263;"
  ]
  node [
    id 41
    label "odstrzeliwa&#263;"
  ]
  node [
    id 42
    label "odstrzeliwanie"
  ]
  node [
    id 43
    label "wcinka"
  ]
  node [
    id 44
    label "nauka"
  ]
  node [
    id 45
    label "si&#281;ga&#263;"
  ]
  node [
    id 46
    label "by&#263;"
  ]
  node [
    id 47
    label "uzyskiwa&#263;"
  ]
  node [
    id 48
    label "wystarcza&#263;"
  ]
  node [
    id 49
    label "range"
  ]
  node [
    id 50
    label "winnings"
  ]
  node [
    id 51
    label "otrzymywa&#263;"
  ]
  node [
    id 52
    label "mie&#263;_miejsce"
  ]
  node [
    id 53
    label "opanowywa&#263;"
  ]
  node [
    id 54
    label "kupowa&#263;"
  ]
  node [
    id 55
    label "nabywa&#263;"
  ]
  node [
    id 56
    label "bra&#263;"
  ]
  node [
    id 57
    label "obskakiwa&#263;"
  ]
  node [
    id 58
    label "liczba"
  ]
  node [
    id 59
    label "szlachetny"
  ]
  node [
    id 60
    label "metaliczny"
  ]
  node [
    id 61
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 62
    label "z&#322;oci&#347;cie"
  ]
  node [
    id 63
    label "grosz"
  ]
  node [
    id 64
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 65
    label "utytu&#322;owany"
  ]
  node [
    id 66
    label "poz&#322;ocenie"
  ]
  node [
    id 67
    label "Polska"
  ]
  node [
    id 68
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 69
    label "wspania&#322;y"
  ]
  node [
    id 70
    label "doskona&#322;y"
  ]
  node [
    id 71
    label "kochany"
  ]
  node [
    id 72
    label "jednostka_monetarna"
  ]
  node [
    id 73
    label "z&#322;ocenie"
  ]
  node [
    id 74
    label "comfort"
  ]
  node [
    id 75
    label "u&#322;atwienie"
  ]
  node [
    id 76
    label "doch&#243;d"
  ]
  node [
    id 77
    label "oparcie"
  ]
  node [
    id 78
    label "telefon_zaufania"
  ]
  node [
    id 79
    label "dar"
  ]
  node [
    id 80
    label "zapomoga"
  ]
  node [
    id 81
    label "pocieszenie"
  ]
  node [
    id 82
    label "darowizna"
  ]
  node [
    id 83
    label "pomoc"
  ]
  node [
    id 84
    label "&#347;rodek"
  ]
  node [
    id 85
    label "wyci&#261;gni&#281;cie_pomocnej_d&#322;oni"
  ]
  node [
    id 86
    label "support"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
]
