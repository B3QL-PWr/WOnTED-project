graph [
  maxDegree 10
  minDegree 1
  meanDegree 1.9393939393939394
  density 0.06060606060606061
  graphCliqueNumber 2
  node [
    id 0
    label "kurwa"
    origin "text"
  ]
  node [
    id 1
    label "minuta"
    origin "text"
  ]
  node [
    id 2
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 3
    label "przyj&#347;&#263;"
    origin "text"
  ]
  node [
    id 4
    label "typiara"
    origin "text"
  ]
  node [
    id 5
    label "rozowypasek"
    origin "text"
  ]
  node [
    id 6
    label "tinder"
    origin "text"
  ]
  node [
    id 7
    label "szmaciarz"
  ]
  node [
    id 8
    label "zo&#322;za"
  ]
  node [
    id 9
    label "skurwienie_si&#281;"
  ]
  node [
    id 10
    label "kurwienie_si&#281;"
  ]
  node [
    id 11
    label "karze&#322;"
  ]
  node [
    id 12
    label "kurcz&#281;"
  ]
  node [
    id 13
    label "wyzwisko"
  ]
  node [
    id 14
    label "przekle&#324;stwo"
  ]
  node [
    id 15
    label "prostytutka"
  ]
  node [
    id 16
    label "zapis"
  ]
  node [
    id 17
    label "godzina"
  ]
  node [
    id 18
    label "sekunda"
  ]
  node [
    id 19
    label "kwadrans"
  ]
  node [
    id 20
    label "stopie&#324;"
  ]
  node [
    id 21
    label "design"
  ]
  node [
    id 22
    label "time"
  ]
  node [
    id 23
    label "jednostka"
  ]
  node [
    id 24
    label "czyj&#347;"
  ]
  node [
    id 25
    label "m&#261;&#380;"
  ]
  node [
    id 26
    label "sta&#263;_si&#281;"
  ]
  node [
    id 27
    label "zaistnie&#263;"
  ]
  node [
    id 28
    label "czas"
  ]
  node [
    id 29
    label "doj&#347;&#263;"
  ]
  node [
    id 30
    label "become"
  ]
  node [
    id 31
    label "line_up"
  ]
  node [
    id 32
    label "przyby&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
]
