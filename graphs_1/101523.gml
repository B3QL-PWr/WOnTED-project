graph [
  maxDegree 13
  minDegree 1
  meanDegree 1.5151515151515151
  density 0.04734848484848485
  graphCliqueNumber 3
  node [
    id 0
    label "pose&#322;"
    origin "text"
  ]
  node [
    id 1
    label "jerzy"
    origin "text"
  ]
  node [
    id 2
    label "kozdro&#324;"
    origin "text"
  ]
  node [
    id 3
    label "dyplomata"
  ]
  node [
    id 4
    label "wys&#322;annik"
  ]
  node [
    id 5
    label "przedstawiciel"
  ]
  node [
    id 6
    label "kurier_dyplomatyczny"
  ]
  node [
    id 7
    label "ablegat"
  ]
  node [
    id 8
    label "klubista"
  ]
  node [
    id 9
    label "Miko&#322;ajczyk"
  ]
  node [
    id 10
    label "Korwin"
  ]
  node [
    id 11
    label "parlamentarzysta"
  ]
  node [
    id 12
    label "dyscyplina_partyjna"
  ]
  node [
    id 13
    label "izba_ni&#380;sza"
  ]
  node [
    id 14
    label "poselstwo"
  ]
  node [
    id 15
    label "Jerzy"
  ]
  node [
    id 16
    label "Kozdro&#324;"
  ]
  node [
    id 17
    label "Jaros&#322;awa"
  ]
  node [
    id 18
    label "Kalinowski"
  ]
  node [
    id 19
    label "Barbara"
  ]
  node [
    id 20
    label "Bartu&#347;"
  ]
  node [
    id 21
    label "prawo"
  ]
  node [
    id 22
    label "i"
  ]
  node [
    id 23
    label "sprawiedliwo&#347;&#263;"
  ]
  node [
    id 24
    label "Ryszarda"
  ]
  node [
    id 25
    label "Kalisz"
  ]
  node [
    id 26
    label "stan"
  ]
  node [
    id 27
    label "zjednoczy&#263;"
  ]
  node [
    id 28
    label "ministerstwo"
  ]
  node [
    id 29
    label "Zbigniew"
  ]
  node [
    id 30
    label "Ziobro"
  ]
  node [
    id 31
    label "kodeks"
  ]
  node [
    id 32
    label "cywilny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 23
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 28
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 31
    target 32
  ]
]
