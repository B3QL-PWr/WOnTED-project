graph [
  maxDegree 26
  minDegree 1
  meanDegree 1.9506172839506173
  density 0.024382716049382715
  graphCliqueNumber 2
  node [
    id 0
    label "trzy"
    origin "text"
  ]
  node [
    id 1
    label "ksi&#261;&#380;ka"
    origin "text"
  ]
  node [
    id 2
    label "podczas"
    origin "text"
  ]
  node [
    id 3
    label "pisanie"
    origin "text"
  ]
  node [
    id 4
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 5
    label "umrze&#263;"
    origin "text"
  ]
  node [
    id 6
    label "du&#380;o"
    origin "text"
  ]
  node [
    id 7
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 8
    label "ok&#322;adka"
  ]
  node [
    id 9
    label "zak&#322;adka"
  ]
  node [
    id 10
    label "ekslibris"
  ]
  node [
    id 11
    label "wk&#322;ad"
  ]
  node [
    id 12
    label "przek&#322;adacz"
  ]
  node [
    id 13
    label "wydawnictwo"
  ]
  node [
    id 14
    label "grzbiet_ksi&#261;&#380;ki"
  ]
  node [
    id 15
    label "tytu&#322;"
  ]
  node [
    id 16
    label "bibliofilstwo"
  ]
  node [
    id 17
    label "falc"
  ]
  node [
    id 18
    label "nomina&#322;"
  ]
  node [
    id 19
    label "pagina"
  ]
  node [
    id 20
    label "rozdzia&#322;"
  ]
  node [
    id 21
    label "egzemplarz"
  ]
  node [
    id 22
    label "zw&#243;j"
  ]
  node [
    id 23
    label "tekst"
  ]
  node [
    id 24
    label "przypisywanie"
  ]
  node [
    id 25
    label "popisanie"
  ]
  node [
    id 26
    label "tworzenie"
  ]
  node [
    id 27
    label "przepisanie"
  ]
  node [
    id 28
    label "zamazywanie"
  ]
  node [
    id 29
    label "t&#322;uczenie"
  ]
  node [
    id 30
    label "enchantment"
  ]
  node [
    id 31
    label "wci&#261;ganie"
  ]
  node [
    id 32
    label "zamazanie"
  ]
  node [
    id 33
    label "pisywanie"
  ]
  node [
    id 34
    label "dopisywanie"
  ]
  node [
    id 35
    label "ozdabianie"
  ]
  node [
    id 36
    label "odpisywanie"
  ]
  node [
    id 37
    label "kre&#347;lenie"
  ]
  node [
    id 38
    label "writing"
  ]
  node [
    id 39
    label "formu&#322;owanie"
  ]
  node [
    id 40
    label "dysortografia"
  ]
  node [
    id 41
    label "donoszenie"
  ]
  node [
    id 42
    label "dysgrafia"
  ]
  node [
    id 43
    label "stawianie"
  ]
  node [
    id 44
    label "os&#322;abn&#261;&#263;"
  ]
  node [
    id 45
    label "przesta&#263;"
  ]
  node [
    id 46
    label "znikn&#261;&#263;"
  ]
  node [
    id 47
    label "die"
  ]
  node [
    id 48
    label "pa&#347;&#263;"
  ]
  node [
    id 49
    label "du&#380;y"
  ]
  node [
    id 50
    label "cz&#281;sto"
  ]
  node [
    id 51
    label "bardzo"
  ]
  node [
    id 52
    label "mocno"
  ]
  node [
    id 53
    label "wiela"
  ]
  node [
    id 54
    label "asymilowa&#263;"
  ]
  node [
    id 55
    label "wapniak"
  ]
  node [
    id 56
    label "dwun&#243;g"
  ]
  node [
    id 57
    label "polifag"
  ]
  node [
    id 58
    label "wz&#243;r"
  ]
  node [
    id 59
    label "profanum"
  ]
  node [
    id 60
    label "hominid"
  ]
  node [
    id 61
    label "homo_sapiens"
  ]
  node [
    id 62
    label "nasada"
  ]
  node [
    id 63
    label "podw&#322;adny"
  ]
  node [
    id 64
    label "ludzko&#347;&#263;"
  ]
  node [
    id 65
    label "os&#322;abianie"
  ]
  node [
    id 66
    label "mikrokosmos"
  ]
  node [
    id 67
    label "portrecista"
  ]
  node [
    id 68
    label "duch"
  ]
  node [
    id 69
    label "g&#322;owa"
  ]
  node [
    id 70
    label "oddzia&#322;ywanie"
  ]
  node [
    id 71
    label "asymilowanie"
  ]
  node [
    id 72
    label "osoba"
  ]
  node [
    id 73
    label "os&#322;abia&#263;"
  ]
  node [
    id 74
    label "figura"
  ]
  node [
    id 75
    label "Adam"
  ]
  node [
    id 76
    label "senior"
  ]
  node [
    id 77
    label "antropochoria"
  ]
  node [
    id 78
    label "posta&#263;"
  ]
  node [
    id 79
    label "Atlas"
  ]
  node [
    id 80
    label "grzyb"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 79
    target 80
  ]
]
