graph [
  maxDegree 2
  minDegree 1
  meanDegree 1.5
  density 0.5
  graphCliqueNumber 2
  node [
    id 0
    label "wygryw"
    origin "text"
  ]
  node [
    id 1
    label "gownowpis"
    origin "text"
  ]
  node [
    id 2
    label "heheszki"
    origin "text"
  ]
  node [
    id 3
    label "szcz&#281;&#347;ciarz"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 1
    target 2
  ]
]
