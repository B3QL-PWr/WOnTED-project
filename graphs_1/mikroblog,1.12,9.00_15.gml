graph [
  maxDegree 15
  minDegree 1
  meanDegree 1.9285714285714286
  density 0.07142857142857142
  graphCliqueNumber 2
  node [
    id 0
    label "r&#243;wno"
    origin "text"
  ]
  node [
    id 1
    label "rok"
    origin "text"
  ]
  node [
    id 2
    label "temu"
    origin "text"
  ]
  node [
    id 3
    label "dok&#322;adnie"
  ]
  node [
    id 4
    label "miarowy"
  ]
  node [
    id 5
    label "stabilnie"
  ]
  node [
    id 6
    label "jednakowy"
  ]
  node [
    id 7
    label "r&#243;wny"
  ]
  node [
    id 8
    label "jednolicie"
  ]
  node [
    id 9
    label "prosto"
  ]
  node [
    id 10
    label "pewnie"
  ]
  node [
    id 11
    label "niezmiennie"
  ]
  node [
    id 12
    label "identically"
  ]
  node [
    id 13
    label "jednocze&#347;nie"
  ]
  node [
    id 14
    label "regularnie"
  ]
  node [
    id 15
    label "stulecie"
  ]
  node [
    id 16
    label "kalendarz"
  ]
  node [
    id 17
    label "czas"
  ]
  node [
    id 18
    label "pora_roku"
  ]
  node [
    id 19
    label "cykl_astronomiczny"
  ]
  node [
    id 20
    label "p&#243;&#322;rocze"
  ]
  node [
    id 21
    label "grupa"
  ]
  node [
    id 22
    label "kwarta&#322;"
  ]
  node [
    id 23
    label "kurs"
  ]
  node [
    id 24
    label "jubileusz"
  ]
  node [
    id 25
    label "miesi&#261;c"
  ]
  node [
    id 26
    label "lata"
  ]
  node [
    id 27
    label "martwy_sezon"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
]
