graph [
  maxDegree 39
  minDegree 1
  meanDegree 2.0331491712707184
  density 0.011295273173726212
  graphCliqueNumber 2
  node [
    id 0
    label "mirka"
    origin "text"
  ]
  node [
    id 1
    label "troch&#281;"
    origin "text"
  ]
  node [
    id 2
    label "mama"
    origin "text"
  ]
  node [
    id 3
    label "czas"
    origin "text"
  ]
  node [
    id 4
    label "gra"
    origin "text"
  ]
  node [
    id 5
    label "c&#243;rka"
    origin "text"
  ]
  node [
    id 6
    label "inny"
    origin "text"
  ]
  node [
    id 7
    label "sprawa"
    origin "text"
  ]
  node [
    id 8
    label "g&#322;owa"
    origin "text"
  ]
  node [
    id 9
    label "zastanawia&#263;"
    origin "text"
  ]
  node [
    id 10
    label "si&#281;"
    origin "text"
  ]
  node [
    id 11
    label "odda&#263;"
    origin "text"
  ]
  node [
    id 12
    label "kto&#347;"
    origin "text"
  ]
  node [
    id 13
    label "konto"
    origin "text"
  ]
  node [
    id 14
    label "matczysko"
  ]
  node [
    id 15
    label "macierz"
  ]
  node [
    id 16
    label "przodkini"
  ]
  node [
    id 17
    label "Matka_Boska"
  ]
  node [
    id 18
    label "macocha"
  ]
  node [
    id 19
    label "matka_zast&#281;pcza"
  ]
  node [
    id 20
    label "stara"
  ]
  node [
    id 21
    label "rodzice"
  ]
  node [
    id 22
    label "rodzic"
  ]
  node [
    id 23
    label "czasokres"
  ]
  node [
    id 24
    label "trawienie"
  ]
  node [
    id 25
    label "kategoria_gramatyczna"
  ]
  node [
    id 26
    label "period"
  ]
  node [
    id 27
    label "odczyt"
  ]
  node [
    id 28
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 29
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 30
    label "chwila"
  ]
  node [
    id 31
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 32
    label "poprzedzenie"
  ]
  node [
    id 33
    label "koniugacja"
  ]
  node [
    id 34
    label "dzieje"
  ]
  node [
    id 35
    label "poprzedzi&#263;"
  ]
  node [
    id 36
    label "przep&#322;ywanie"
  ]
  node [
    id 37
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 38
    label "odwlekanie_si&#281;"
  ]
  node [
    id 39
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 40
    label "Zeitgeist"
  ]
  node [
    id 41
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 42
    label "okres_czasu"
  ]
  node [
    id 43
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 44
    label "pochodzi&#263;"
  ]
  node [
    id 45
    label "schy&#322;ek"
  ]
  node [
    id 46
    label "czwarty_wymiar"
  ]
  node [
    id 47
    label "chronometria"
  ]
  node [
    id 48
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 49
    label "poprzedzanie"
  ]
  node [
    id 50
    label "pogoda"
  ]
  node [
    id 51
    label "zegar"
  ]
  node [
    id 52
    label "pochodzenie"
  ]
  node [
    id 53
    label "poprzedza&#263;"
  ]
  node [
    id 54
    label "trawi&#263;"
  ]
  node [
    id 55
    label "time_period"
  ]
  node [
    id 56
    label "rachuba_czasu"
  ]
  node [
    id 57
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 58
    label "czasoprzestrze&#324;"
  ]
  node [
    id 59
    label "laba"
  ]
  node [
    id 60
    label "zabawa"
  ]
  node [
    id 61
    label "rywalizacja"
  ]
  node [
    id 62
    label "czynno&#347;&#263;"
  ]
  node [
    id 63
    label "Pok&#233;mon"
  ]
  node [
    id 64
    label "synteza"
  ]
  node [
    id 65
    label "odtworzenie"
  ]
  node [
    id 66
    label "komplet"
  ]
  node [
    id 67
    label "rekwizyt_do_gry"
  ]
  node [
    id 68
    label "odg&#322;os"
  ]
  node [
    id 69
    label "rozgrywka"
  ]
  node [
    id 70
    label "post&#281;powanie"
  ]
  node [
    id 71
    label "wydarzenie"
  ]
  node [
    id 72
    label "apparent_motion"
  ]
  node [
    id 73
    label "game"
  ]
  node [
    id 74
    label "zmienno&#347;&#263;"
  ]
  node [
    id 75
    label "zasada"
  ]
  node [
    id 76
    label "akcja"
  ]
  node [
    id 77
    label "play"
  ]
  node [
    id 78
    label "contest"
  ]
  node [
    id 79
    label "zbijany"
  ]
  node [
    id 80
    label "cz&#322;owiek"
  ]
  node [
    id 81
    label "dziewka"
  ]
  node [
    id 82
    label "potomkini"
  ]
  node [
    id 83
    label "dziewoja"
  ]
  node [
    id 84
    label "dziunia"
  ]
  node [
    id 85
    label "dziecko"
  ]
  node [
    id 86
    label "dziewczynina"
  ]
  node [
    id 87
    label "siksa"
  ]
  node [
    id 88
    label "dziewcz&#281;"
  ]
  node [
    id 89
    label "kora"
  ]
  node [
    id 90
    label "m&#322;&#243;dka"
  ]
  node [
    id 91
    label "dziecina"
  ]
  node [
    id 92
    label "sikorka"
  ]
  node [
    id 93
    label "kolejny"
  ]
  node [
    id 94
    label "inaczej"
  ]
  node [
    id 95
    label "r&#243;&#380;ny"
  ]
  node [
    id 96
    label "inszy"
  ]
  node [
    id 97
    label "osobno"
  ]
  node [
    id 98
    label "temat"
  ]
  node [
    id 99
    label "kognicja"
  ]
  node [
    id 100
    label "idea"
  ]
  node [
    id 101
    label "szczeg&#243;&#322;"
  ]
  node [
    id 102
    label "rzecz"
  ]
  node [
    id 103
    label "przes&#322;anka"
  ]
  node [
    id 104
    label "rozprawa"
  ]
  node [
    id 105
    label "object"
  ]
  node [
    id 106
    label "proposition"
  ]
  node [
    id 107
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 108
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 109
    label "ucho"
  ]
  node [
    id 110
    label "makrocefalia"
  ]
  node [
    id 111
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 112
    label "m&#243;zg"
  ]
  node [
    id 113
    label "kierownictwo"
  ]
  node [
    id 114
    label "czaszka"
  ]
  node [
    id 115
    label "dekiel"
  ]
  node [
    id 116
    label "umys&#322;"
  ]
  node [
    id 117
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 118
    label "&#347;ci&#281;cie"
  ]
  node [
    id 119
    label "sztuka"
  ]
  node [
    id 120
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 121
    label "g&#243;ra"
  ]
  node [
    id 122
    label "byd&#322;o"
  ]
  node [
    id 123
    label "alkohol"
  ]
  node [
    id 124
    label "wiedza"
  ]
  node [
    id 125
    label "ro&#347;lina"
  ]
  node [
    id 126
    label "&#347;ci&#281;gno"
  ]
  node [
    id 127
    label "&#380;ycie"
  ]
  node [
    id 128
    label "pryncypa&#322;"
  ]
  node [
    id 129
    label "fryzura"
  ]
  node [
    id 130
    label "noosfera"
  ]
  node [
    id 131
    label "kierowa&#263;"
  ]
  node [
    id 132
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 133
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 134
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 135
    label "cecha"
  ]
  node [
    id 136
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 137
    label "zdolno&#347;&#263;"
  ]
  node [
    id 138
    label "kszta&#322;t"
  ]
  node [
    id 139
    label "cz&#322;onek"
  ]
  node [
    id 140
    label "cia&#322;o"
  ]
  node [
    id 141
    label "obiekt"
  ]
  node [
    id 142
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 143
    label "strike"
  ]
  node [
    id 144
    label "interesowa&#263;"
  ]
  node [
    id 145
    label "restore"
  ]
  node [
    id 146
    label "da&#263;"
  ]
  node [
    id 147
    label "dostarczy&#263;"
  ]
  node [
    id 148
    label "odst&#261;pi&#263;"
  ]
  node [
    id 149
    label "sacrifice"
  ]
  node [
    id 150
    label "odpowiedzie&#263;"
  ]
  node [
    id 151
    label "sprzeda&#263;"
  ]
  node [
    id 152
    label "reflect"
  ]
  node [
    id 153
    label "przedstawi&#263;"
  ]
  node [
    id 154
    label "transfer"
  ]
  node [
    id 155
    label "give"
  ]
  node [
    id 156
    label "za&#322;atwi&#263;_si&#281;"
  ]
  node [
    id 157
    label "convey"
  ]
  node [
    id 158
    label "umie&#347;ci&#263;"
  ]
  node [
    id 159
    label "przekaza&#263;"
  ]
  node [
    id 160
    label "z_powrotem"
  ]
  node [
    id 161
    label "zrobi&#263;"
  ]
  node [
    id 162
    label "picture"
  ]
  node [
    id 163
    label "deliver"
  ]
  node [
    id 164
    label "znaczenie"
  ]
  node [
    id 165
    label "go&#347;&#263;"
  ]
  node [
    id 166
    label "osoba"
  ]
  node [
    id 167
    label "posta&#263;"
  ]
  node [
    id 168
    label "ksi&#281;gowo&#347;&#263;"
  ]
  node [
    id 169
    label "dost&#281;p"
  ]
  node [
    id 170
    label "bank"
  ]
  node [
    id 171
    label "kariera"
  ]
  node [
    id 172
    label "rejestrowa&#263;_si&#281;"
  ]
  node [
    id 173
    label "dorobek"
  ]
  node [
    id 174
    label "debet"
  ]
  node [
    id 175
    label "rachunek"
  ]
  node [
    id 176
    label "logowa&#263;_si&#281;"
  ]
  node [
    id 177
    label "mienie"
  ]
  node [
    id 178
    label "subkonto"
  ]
  node [
    id 179
    label "kredyt"
  ]
  node [
    id 180
    label "reprezentacja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 8
    target 122
  ]
  edge [
    source 8
    target 123
  ]
  edge [
    source 8
    target 124
  ]
  edge [
    source 8
    target 125
  ]
  edge [
    source 8
    target 126
  ]
  edge [
    source 8
    target 127
  ]
  edge [
    source 8
    target 128
  ]
  edge [
    source 8
    target 129
  ]
  edge [
    source 8
    target 130
  ]
  edge [
    source 8
    target 131
  ]
  edge [
    source 8
    target 132
  ]
  edge [
    source 8
    target 133
  ]
  edge [
    source 8
    target 134
  ]
  edge [
    source 8
    target 135
  ]
  edge [
    source 8
    target 136
  ]
  edge [
    source 8
    target 137
  ]
  edge [
    source 8
    target 138
  ]
  edge [
    source 8
    target 139
  ]
  edge [
    source 8
    target 140
  ]
  edge [
    source 8
    target 141
  ]
  edge [
    source 8
    target 142
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 143
  ]
  edge [
    source 9
    target 144
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 145
  ]
  edge [
    source 11
    target 146
  ]
  edge [
    source 11
    target 147
  ]
  edge [
    source 11
    target 148
  ]
  edge [
    source 11
    target 149
  ]
  edge [
    source 11
    target 150
  ]
  edge [
    source 11
    target 151
  ]
  edge [
    source 11
    target 152
  ]
  edge [
    source 11
    target 153
  ]
  edge [
    source 11
    target 154
  ]
  edge [
    source 11
    target 155
  ]
  edge [
    source 11
    target 156
  ]
  edge [
    source 11
    target 157
  ]
  edge [
    source 11
    target 158
  ]
  edge [
    source 11
    target 159
  ]
  edge [
    source 11
    target 160
  ]
  edge [
    source 11
    target 161
  ]
  edge [
    source 11
    target 162
  ]
  edge [
    source 11
    target 163
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 80
  ]
  edge [
    source 12
    target 164
  ]
  edge [
    source 12
    target 165
  ]
  edge [
    source 12
    target 166
  ]
  edge [
    source 12
    target 167
  ]
  edge [
    source 13
    target 168
  ]
  edge [
    source 13
    target 169
  ]
  edge [
    source 13
    target 170
  ]
  edge [
    source 13
    target 171
  ]
  edge [
    source 13
    target 172
  ]
  edge [
    source 13
    target 173
  ]
  edge [
    source 13
    target 174
  ]
  edge [
    source 13
    target 175
  ]
  edge [
    source 13
    target 176
  ]
  edge [
    source 13
    target 177
  ]
  edge [
    source 13
    target 178
  ]
  edge [
    source 13
    target 179
  ]
  edge [
    source 13
    target 180
  ]
]
