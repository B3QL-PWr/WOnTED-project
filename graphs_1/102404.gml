graph [
  maxDegree 11
  minDegree 1
  meanDegree 2
  density 0.03333333333333333
  graphCliqueNumber 2
  node [
    id 0
    label "dni"
    origin "text"
  ]
  node [
    id 1
    label "czerwiec"
    origin "text"
  ]
  node [
    id 2
    label "mediabarcamp"
    origin "text"
  ]
  node [
    id 3
    label "spotka&#263;"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "przedstawiciel"
    origin "text"
  ]
  node [
    id 6
    label "media"
    origin "text"
  ]
  node [
    id 7
    label "bia&#322;oru&#347;"
    origin "text"
  ]
  node [
    id 8
    label "ukraina"
    origin "text"
  ]
  node [
    id 9
    label "szwecja"
    origin "text"
  ]
  node [
    id 10
    label "trzecia"
    origin "text"
  ]
  node [
    id 11
    label "edycja"
    origin "text"
  ]
  node [
    id 12
    label "impreza"
    origin "text"
  ]
  node [
    id 13
    label "odby&#263;"
    origin "text"
  ]
  node [
    id 14
    label "litwa"
    origin "text"
  ]
  node [
    id 15
    label "czas"
  ]
  node [
    id 16
    label "ro&#347;lina_zielna"
  ]
  node [
    id 17
    label "go&#378;dzikowate"
  ]
  node [
    id 18
    label "miesi&#261;c"
  ]
  node [
    id 19
    label "Bo&#380;e_Cia&#322;o"
  ]
  node [
    id 20
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 21
    label "befall"
  ]
  node [
    id 22
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 23
    label "pozna&#263;"
  ]
  node [
    id 24
    label "spowodowa&#263;"
  ]
  node [
    id 25
    label "go_steady"
  ]
  node [
    id 26
    label "insert"
  ]
  node [
    id 27
    label "znale&#378;&#263;"
  ]
  node [
    id 28
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 29
    label "visualize"
  ]
  node [
    id 30
    label "cz&#322;owiek"
  ]
  node [
    id 31
    label "cz&#322;onek"
  ]
  node [
    id 32
    label "substytuowanie"
  ]
  node [
    id 33
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 34
    label "przyk&#322;ad"
  ]
  node [
    id 35
    label "zast&#281;pca"
  ]
  node [
    id 36
    label "substytuowa&#263;"
  ]
  node [
    id 37
    label "przekazior"
  ]
  node [
    id 38
    label "mass-media"
  ]
  node [
    id 39
    label "uzbrajanie"
  ]
  node [
    id 40
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 41
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 42
    label "medium"
  ]
  node [
    id 43
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 44
    label "godzina"
  ]
  node [
    id 45
    label "impression"
  ]
  node [
    id 46
    label "odmiana"
  ]
  node [
    id 47
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 48
    label "notification"
  ]
  node [
    id 49
    label "cykl"
  ]
  node [
    id 50
    label "zmiana"
  ]
  node [
    id 51
    label "produkcja"
  ]
  node [
    id 52
    label "egzemplarz"
  ]
  node [
    id 53
    label "party"
  ]
  node [
    id 54
    label "rozrywka"
  ]
  node [
    id 55
    label "przyj&#281;cie"
  ]
  node [
    id 56
    label "okazja"
  ]
  node [
    id 57
    label "impra"
  ]
  node [
    id 58
    label "reserve"
  ]
  node [
    id 59
    label "przej&#347;&#263;"
  ]
  node [
    id 60
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 44
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 45
  ]
  edge [
    source 11
    target 46
  ]
  edge [
    source 11
    target 47
  ]
  edge [
    source 11
    target 48
  ]
  edge [
    source 11
    target 49
  ]
  edge [
    source 11
    target 50
  ]
  edge [
    source 11
    target 51
  ]
  edge [
    source 11
    target 52
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 53
  ]
  edge [
    source 12
    target 54
  ]
  edge [
    source 12
    target 55
  ]
  edge [
    source 12
    target 56
  ]
  edge [
    source 12
    target 57
  ]
  edge [
    source 13
    target 58
  ]
  edge [
    source 13
    target 59
  ]
  edge [
    source 13
    target 60
  ]
]
