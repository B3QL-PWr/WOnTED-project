graph [
  maxDegree 9
  minDegree 1
  meanDegree 3.066666666666667
  density 0.10574712643678161
  graphCliqueNumber 6
  node [
    id 0
    label "pa&#322;ac"
    origin "text"
  ]
  node [
    id 1
    label "august"
    origin "text"
  ]
  node [
    id 2
    label "haertiga"
    origin "text"
  ]
  node [
    id 3
    label "Wersal"
  ]
  node [
    id 4
    label "Belweder"
  ]
  node [
    id 5
    label "budynek"
  ]
  node [
    id 6
    label "w&#322;adza"
  ]
  node [
    id 7
    label "rezydencja"
  ]
  node [
    id 8
    label "tytu&#322;"
  ]
  node [
    id 9
    label "Haertiga"
  ]
  node [
    id 10
    label "Franciszka"
  ]
  node [
    id 11
    label "che&#322;mi&#324;ski"
  ]
  node [
    id 12
    label "towarzystwo"
  ]
  node [
    id 13
    label "ubezpieczenie"
  ]
  node [
    id 14
    label "i"
  ]
  node [
    id 15
    label "reasekuracja"
  ]
  node [
    id 16
    label "warta&#263;"
  ]
  node [
    id 17
    label "zabytek"
  ]
  node [
    id 18
    label "zadba&#263;"
  ]
  node [
    id 19
    label "krajowy"
  ]
  node [
    id 20
    label "o&#347;rodek"
  ]
  node [
    id 21
    label "badanie"
  ]
  node [
    id 22
    label "dokumentacja"
  ]
  node [
    id 23
    label "fundacja"
  ]
  node [
    id 24
    label "ulica"
  ]
  node [
    id 25
    label "piotrkowski"
  ]
  node [
    id 26
    label "dobry"
  ]
  node [
    id 27
    label "wn&#281;trze"
  ]
  node [
    id 28
    label "rok"
  ]
  node [
    id 29
    label "2005"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 12
    target 15
  ]
  edge [
    source 12
    target 16
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 13
    target 16
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 14
    target 19
  ]
  edge [
    source 14
    target 20
  ]
  edge [
    source 14
    target 21
  ]
  edge [
    source 14
    target 22
  ]
  edge [
    source 14
    target 17
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 19
  ]
  edge [
    source 17
    target 20
  ]
  edge [
    source 17
    target 21
  ]
  edge [
    source 17
    target 22
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 21
  ]
  edge [
    source 19
    target 22
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 22
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 25
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 28
  ]
  edge [
    source 26
    target 29
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 29
  ]
  edge [
    source 28
    target 29
  ]
]
