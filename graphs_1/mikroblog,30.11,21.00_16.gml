graph [
  maxDegree 8
  minDegree 1
  meanDegree 1.8333333333333333
  density 0.16666666666666666
  graphCliqueNumber 2
  node [
    id 0
    label "heheszki"
    origin "text"
  ]
  node [
    id 1
    label "humorobrazkowy"
    origin "text"
  ]
  node [
    id 2
    label "pdk"
    origin "text"
  ]
  node [
    id 3
    label "policja"
    origin "text"
  ]
  node [
    id 4
    label "bagieta"
    origin "text"
  ]
  node [
    id 5
    label "komisariat"
  ]
  node [
    id 6
    label "psiarnia"
  ]
  node [
    id 7
    label "posterunek"
  ]
  node [
    id 8
    label "grupa"
  ]
  node [
    id 9
    label "organ"
  ]
  node [
    id 10
    label "s&#322;u&#380;ba"
  ]
  node [
    id 11
    label "listwa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 4
    target 11
  ]
]
