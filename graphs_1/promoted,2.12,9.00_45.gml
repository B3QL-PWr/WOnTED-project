graph [
  maxDegree 10
  minDegree 1
  meanDegree 1.9230769230769231
  density 0.07692307692307693
  graphCliqueNumber 2
  node [
    id 0
    label "pewne"
    origin "text"
  ]
  node [
    id 1
    label "moment"
    origin "text"
  ]
  node [
    id 2
    label "zaj&#281;cia"
    origin "text"
  ]
  node [
    id 3
    label "wkracza&#263;"
    origin "text"
  ]
  node [
    id 4
    label "niepozorny"
    origin "text"
  ]
  node [
    id 5
    label "okres_czasu"
  ]
  node [
    id 6
    label "minute"
  ]
  node [
    id 7
    label "jednostka_geologiczna"
  ]
  node [
    id 8
    label "warto&#347;&#263;_oczekiwana"
  ]
  node [
    id 9
    label "time"
  ]
  node [
    id 10
    label "chron"
  ]
  node [
    id 11
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 12
    label "fragment"
  ]
  node [
    id 13
    label "pensum"
  ]
  node [
    id 14
    label "enroll"
  ]
  node [
    id 15
    label "w&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 16
    label "zajmowa&#263;"
  ]
  node [
    id 17
    label "wchodzi&#263;"
  ]
  node [
    id 18
    label "porusza&#263;"
  ]
  node [
    id 19
    label "zaczyna&#263;"
  ]
  node [
    id 20
    label "dochodzi&#263;"
  ]
  node [
    id 21
    label "invade"
  ]
  node [
    id 22
    label "intervene"
  ]
  node [
    id 23
    label "szaraczek"
  ]
  node [
    id 24
    label "niepozornie"
  ]
  node [
    id 25
    label "zwyczajny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
]
