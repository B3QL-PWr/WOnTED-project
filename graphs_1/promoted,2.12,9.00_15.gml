graph [
  maxDegree 42
  minDegree 1
  meanDegree 1.9859154929577465
  density 0.014084507042253521
  graphCliqueNumber 2
  node [
    id 0
    label "wia&#263;"
    origin "text"
  ]
  node [
    id 1
    label "czwartek"
    origin "text"
  ]
  node [
    id 2
    label "silny"
    origin "text"
  ]
  node [
    id 3
    label "po&#322;udniowy"
    origin "text"
  ]
  node [
    id 4
    label "wiatr"
    origin "text"
  ]
  node [
    id 5
    label "spowodowa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "rekordowo"
    origin "text"
  ]
  node [
    id 7
    label "niski"
    origin "text"
  ]
  node [
    id 8
    label "stan"
    origin "text"
  ]
  node [
    id 9
    label "woda"
    origin "text"
  ]
  node [
    id 10
    label "ba&#322;tyk"
    origin "text"
  ]
  node [
    id 11
    label "mie&#263;_miejsce"
  ]
  node [
    id 12
    label "blow"
  ]
  node [
    id 13
    label "dzie&#324;_powszedni"
  ]
  node [
    id 14
    label "Bo&#380;e_Cia&#322;o"
  ]
  node [
    id 15
    label "Wielki_Czwartek"
  ]
  node [
    id 16
    label "tydzie&#324;"
  ]
  node [
    id 17
    label "przekonuj&#261;cy"
  ]
  node [
    id 18
    label "du&#380;y"
  ]
  node [
    id 19
    label "zdecydowany"
  ]
  node [
    id 20
    label "niepodwa&#380;alny"
  ]
  node [
    id 21
    label "wytrzyma&#322;y"
  ]
  node [
    id 22
    label "zdrowy"
  ]
  node [
    id 23
    label "silnie"
  ]
  node [
    id 24
    label "&#380;ywotny"
  ]
  node [
    id 25
    label "konkretny"
  ]
  node [
    id 26
    label "intensywny"
  ]
  node [
    id 27
    label "krzepienie"
  ]
  node [
    id 28
    label "meflochina"
  ]
  node [
    id 29
    label "zajebisty"
  ]
  node [
    id 30
    label "mocno"
  ]
  node [
    id 31
    label "pokrzepienie"
  ]
  node [
    id 32
    label "mocny"
  ]
  node [
    id 33
    label "gor&#261;cy"
  ]
  node [
    id 34
    label "s&#322;oneczny"
  ]
  node [
    id 35
    label "po&#322;udniowo"
  ]
  node [
    id 36
    label "wietrzno&#347;&#263;"
  ]
  node [
    id 37
    label "skala_Beauforta"
  ]
  node [
    id 38
    label "porywisto&#347;&#263;"
  ]
  node [
    id 39
    label "powia&#263;"
  ]
  node [
    id 40
    label "powianie"
  ]
  node [
    id 41
    label "powietrze"
  ]
  node [
    id 42
    label "zjawisko"
  ]
  node [
    id 43
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 44
    label "act"
  ]
  node [
    id 45
    label "rekordowy"
  ]
  node [
    id 46
    label "maksymalnie"
  ]
  node [
    id 47
    label "marny"
  ]
  node [
    id 48
    label "wstydliwy"
  ]
  node [
    id 49
    label "nieznaczny"
  ]
  node [
    id 50
    label "gorszy"
  ]
  node [
    id 51
    label "bliski"
  ]
  node [
    id 52
    label "s&#322;aby"
  ]
  node [
    id 53
    label "obni&#380;enie"
  ]
  node [
    id 54
    label "nisko"
  ]
  node [
    id 55
    label "n&#281;dznie"
  ]
  node [
    id 56
    label "po&#347;ledni"
  ]
  node [
    id 57
    label "uni&#380;ony"
  ]
  node [
    id 58
    label "pospolity"
  ]
  node [
    id 59
    label "obni&#380;anie"
  ]
  node [
    id 60
    label "pomierny"
  ]
  node [
    id 61
    label "ma&#322;y"
  ]
  node [
    id 62
    label "Arizona"
  ]
  node [
    id 63
    label "Georgia"
  ]
  node [
    id 64
    label "warstwa"
  ]
  node [
    id 65
    label "jednostka_administracyjna"
  ]
  node [
    id 66
    label "Hawaje"
  ]
  node [
    id 67
    label "Goa"
  ]
  node [
    id 68
    label "Floryda"
  ]
  node [
    id 69
    label "Oklahoma"
  ]
  node [
    id 70
    label "punkt"
  ]
  node [
    id 71
    label "Alaska"
  ]
  node [
    id 72
    label "wci&#281;cie"
  ]
  node [
    id 73
    label "Alabama"
  ]
  node [
    id 74
    label "Oregon"
  ]
  node [
    id 75
    label "poziom"
  ]
  node [
    id 76
    label "by&#263;"
  ]
  node [
    id 77
    label "Teksas"
  ]
  node [
    id 78
    label "Illinois"
  ]
  node [
    id 79
    label "Waszyngton"
  ]
  node [
    id 80
    label "Jukatan"
  ]
  node [
    id 81
    label "shape"
  ]
  node [
    id 82
    label "Nowy_Meksyk"
  ]
  node [
    id 83
    label "ilo&#347;&#263;"
  ]
  node [
    id 84
    label "state"
  ]
  node [
    id 85
    label "Nowy_York"
  ]
  node [
    id 86
    label "Arakan"
  ]
  node [
    id 87
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 88
    label "Kalifornia"
  ]
  node [
    id 89
    label "wektor"
  ]
  node [
    id 90
    label "Massachusetts"
  ]
  node [
    id 91
    label "miejsce"
  ]
  node [
    id 92
    label "Pensylwania"
  ]
  node [
    id 93
    label "Michigan"
  ]
  node [
    id 94
    label "Maryland"
  ]
  node [
    id 95
    label "Ohio"
  ]
  node [
    id 96
    label "Kansas"
  ]
  node [
    id 97
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 98
    label "Luizjana"
  ]
  node [
    id 99
    label "samopoczucie"
  ]
  node [
    id 100
    label "Wirginia"
  ]
  node [
    id 101
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 102
    label "wypowied&#378;"
  ]
  node [
    id 103
    label "obiekt_naturalny"
  ]
  node [
    id 104
    label "bicie"
  ]
  node [
    id 105
    label "wysi&#281;k"
  ]
  node [
    id 106
    label "pustka"
  ]
  node [
    id 107
    label "woda_s&#322;odka"
  ]
  node [
    id 108
    label "p&#322;ycizna"
  ]
  node [
    id 109
    label "ciecz"
  ]
  node [
    id 110
    label "spi&#281;trza&#263;"
  ]
  node [
    id 111
    label "uj&#281;cie_wody"
  ]
  node [
    id 112
    label "chlasta&#263;"
  ]
  node [
    id 113
    label "spi&#281;trzy&#263;"
  ]
  node [
    id 114
    label "nap&#243;j"
  ]
  node [
    id 115
    label "bombast"
  ]
  node [
    id 116
    label "water"
  ]
  node [
    id 117
    label "kryptodepresja"
  ]
  node [
    id 118
    label "wodnik"
  ]
  node [
    id 119
    label "pojazd"
  ]
  node [
    id 120
    label "&#347;r&#243;dl&#261;dowy"
  ]
  node [
    id 121
    label "fala"
  ]
  node [
    id 122
    label "Waruna"
  ]
  node [
    id 123
    label "nap&#243;j_bezalkoholowy"
  ]
  node [
    id 124
    label "zrzut"
  ]
  node [
    id 125
    label "dotleni&#263;"
  ]
  node [
    id 126
    label "utylizator"
  ]
  node [
    id 127
    label "przyroda"
  ]
  node [
    id 128
    label "uci&#261;g"
  ]
  node [
    id 129
    label "wybrze&#380;e"
  ]
  node [
    id 130
    label "nabranie"
  ]
  node [
    id 131
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 132
    label "chlastanie"
  ]
  node [
    id 133
    label "klarownik"
  ]
  node [
    id 134
    label "przybrze&#380;e"
  ]
  node [
    id 135
    label "deklamacja"
  ]
  node [
    id 136
    label "spi&#281;trzenie"
  ]
  node [
    id 137
    label "przybieranie"
  ]
  node [
    id 138
    label "nabra&#263;"
  ]
  node [
    id 139
    label "tlenek"
  ]
  node [
    id 140
    label "spi&#281;trzanie"
  ]
  node [
    id 141
    label "l&#243;d"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 102
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 104
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 9
    target 116
  ]
  edge [
    source 9
    target 117
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 9
    target 119
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 129
  ]
  edge [
    source 9
    target 130
  ]
  edge [
    source 9
    target 131
  ]
  edge [
    source 9
    target 132
  ]
  edge [
    source 9
    target 133
  ]
  edge [
    source 9
    target 134
  ]
  edge [
    source 9
    target 135
  ]
  edge [
    source 9
    target 136
  ]
  edge [
    source 9
    target 137
  ]
  edge [
    source 9
    target 138
  ]
  edge [
    source 9
    target 139
  ]
  edge [
    source 9
    target 140
  ]
  edge [
    source 9
    target 141
  ]
]
