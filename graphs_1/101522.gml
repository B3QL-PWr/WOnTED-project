graph [
  maxDegree 218
  minDegree 1
  meanDegree 2.2267303102625298
  density 0.0026603707410543962
  graphCliqueNumber 4
  node [
    id 0
    label "aby"
    origin "text"
  ]
  node [
    id 1
    label "edukacja"
    origin "text"
  ]
  node [
    id 2
    label "zawodowy"
    origin "text"
  ]
  node [
    id 3
    label "by&#322;a"
    origin "text"
  ]
  node [
    id 4
    label "rzeczywi&#347;cie"
    origin "text"
  ]
  node [
    id 5
    label "skuteczny"
    origin "text"
  ]
  node [
    id 6
    label "nowoczesny"
    origin "text"
  ]
  node [
    id 7
    label "potrzeba"
    origin "text"
  ]
  node [
    id 8
    label "jak"
    origin "text"
  ]
  node [
    id 9
    label "niekt&#243;ry"
    origin "text"
  ]
  node [
    id 10
    label "pa&#324;stwo"
    origin "text"
  ]
  node [
    id 11
    label "sceptycznie"
    origin "text"
  ]
  node [
    id 12
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 13
    label "bardzo"
    origin "text"
  ]
  node [
    id 14
    label "du&#380;o"
    origin "text"
  ]
  node [
    id 15
    label "czas"
    origin "text"
  ]
  node [
    id 16
    label "by&#263;"
    origin "text"
  ]
  node [
    id 17
    label "proces"
    origin "text"
  ]
  node [
    id 18
    label "d&#322;ugofalowy"
    origin "text"
  ]
  node [
    id 19
    label "powiedzie&#263;"
    origin "text"
  ]
  node [
    id 20
    label "tak"
    origin "text"
  ]
  node [
    id 21
    label "podziela&#263;"
    origin "text"
  ]
  node [
    id 22
    label "pe&#322;nia"
    origin "text"
  ]
  node [
    id 23
    label "opinia"
    origin "text"
  ]
  node [
    id 24
    label "kszta&#322;cenie"
    origin "text"
  ]
  node [
    id 25
    label "og&#243;lny"
    origin "text"
  ]
  node [
    id 26
    label "wszyscy"
    origin "text"
  ]
  node [
    id 27
    label "szko&#322;a"
    origin "text"
  ]
  node [
    id 28
    label "ko&#324;czy&#263;"
    origin "text"
  ]
  node [
    id 29
    label "si&#281;"
    origin "text"
  ]
  node [
    id 30
    label "matura"
    origin "text"
  ]
  node [
    id 31
    label "powinien"
    origin "text"
  ]
  node [
    id 32
    label "jednakowy"
    origin "text"
  ]
  node [
    id 33
    label "je&#347;li"
    origin "text"
  ]
  node [
    id 34
    label "chodzi&#263;"
    origin "text"
  ]
  node [
    id 35
    label "standard"
    origin "text"
  ]
  node [
    id 36
    label "sens"
    origin "text"
  ]
  node [
    id 37
    label "przedmiot"
    origin "text"
  ]
  node [
    id 38
    label "rozszerzy&#263;"
    origin "text"
  ]
  node [
    id 39
    label "uczenie"
    origin "text"
  ]
  node [
    id 40
    label "rozumowanie"
    origin "text"
  ]
  node [
    id 41
    label "w&#322;a&#347;nie"
    origin "text"
  ]
  node [
    id 42
    label "pewien"
    origin "text"
  ]
  node [
    id 43
    label "elastyczno&#347;&#263;"
    origin "text"
  ]
  node [
    id 44
    label "my&#347;lenie"
    origin "text"
  ]
  node [
    id 45
    label "szalenie"
    origin "text"
  ]
  node [
    id 46
    label "potrzebny"
    origin "text"
  ]
  node [
    id 47
    label "taki"
    origin "text"
  ]
  node [
    id 48
    label "kompetencja"
    origin "text"
  ]
  node [
    id 49
    label "rynek"
    origin "text"
  ]
  node [
    id 50
    label "praca"
    origin "text"
  ]
  node [
    id 51
    label "wa&#380;ny"
    origin "text"
  ]
  node [
    id 52
    label "tylko"
    origin "text"
  ]
  node [
    id 53
    label "ale"
    origin "text"
  ]
  node [
    id 54
    label "mo&#380;liwo&#347;&#263;"
    origin "text"
  ]
  node [
    id 55
    label "podj&#281;cie"
    origin "text"
  ]
  node [
    id 56
    label "studia"
    origin "text"
  ]
  node [
    id 57
    label "przygotowa&#263;"
    origin "text"
  ]
  node [
    id 58
    label "przez"
    origin "text"
  ]
  node [
    id 59
    label "nasa"
    origin "text"
  ]
  node [
    id 60
    label "projekt"
    origin "text"
  ]
  node [
    id 61
    label "zmiana"
    origin "text"
  ]
  node [
    id 62
    label "korekta"
    origin "text"
  ]
  node [
    id 63
    label "podstawa"
    origin "text"
  ]
  node [
    id 64
    label "programowy"
    origin "text"
  ]
  node [
    id 65
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 66
    label "uwzgl&#281;dnia&#263;"
    origin "text"
  ]
  node [
    id 67
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 68
    label "czym"
    origin "text"
  ]
  node [
    id 69
    label "pan"
    origin "text"
  ]
  node [
    id 70
    label "pose&#322;"
    origin "text"
  ]
  node [
    id 71
    label "uprzejmy"
    origin "text"
  ]
  node [
    id 72
    label "warto"
    origin "text"
  ]
  node [
    id 73
    label "&#380;eby"
    origin "text"
  ]
  node [
    id 74
    label "troch&#281;"
  ]
  node [
    id 75
    label "kwalifikacje"
  ]
  node [
    id 76
    label "Karta_Nauczyciela"
  ]
  node [
    id 77
    label "szkolnictwo"
  ]
  node [
    id 78
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 79
    label "program_nauczania"
  ]
  node [
    id 80
    label "formation"
  ]
  node [
    id 81
    label "miasteczko_rowerowe"
  ]
  node [
    id 82
    label "gospodarka"
  ]
  node [
    id 83
    label "urszulanki"
  ]
  node [
    id 84
    label "wiedza"
  ]
  node [
    id 85
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 86
    label "skolaryzacja"
  ]
  node [
    id 87
    label "niepokalanki"
  ]
  node [
    id 88
    label "heureza"
  ]
  node [
    id 89
    label "form"
  ]
  node [
    id 90
    label "nauka"
  ]
  node [
    id 91
    label "&#322;awa_szkolna"
  ]
  node [
    id 92
    label "formalny"
  ]
  node [
    id 93
    label "zawodowo"
  ]
  node [
    id 94
    label "zawo&#322;any"
  ]
  node [
    id 95
    label "profesjonalny"
  ]
  node [
    id 96
    label "czadowy"
  ]
  node [
    id 97
    label "fajny"
  ]
  node [
    id 98
    label "fachowy"
  ]
  node [
    id 99
    label "s&#322;u&#380;bowo"
  ]
  node [
    id 100
    label "klawy"
  ]
  node [
    id 101
    label "partnerka"
  ]
  node [
    id 102
    label "realny"
  ]
  node [
    id 103
    label "naprawd&#281;"
  ]
  node [
    id 104
    label "skutkowanie"
  ]
  node [
    id 105
    label "poskutkowanie"
  ]
  node [
    id 106
    label "dobry"
  ]
  node [
    id 107
    label "sprawny"
  ]
  node [
    id 108
    label "skutecznie"
  ]
  node [
    id 109
    label "nowocze&#347;nie"
  ]
  node [
    id 110
    label "nowo&#380;ytny"
  ]
  node [
    id 111
    label "otwarty"
  ]
  node [
    id 112
    label "nowy"
  ]
  node [
    id 113
    label "need"
  ]
  node [
    id 114
    label "wym&#243;g"
  ]
  node [
    id 115
    label "necessity"
  ]
  node [
    id 116
    label "pragnienie"
  ]
  node [
    id 117
    label "sytuacja"
  ]
  node [
    id 118
    label "byd&#322;o"
  ]
  node [
    id 119
    label "zobo"
  ]
  node [
    id 120
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 121
    label "yakalo"
  ]
  node [
    id 122
    label "dzo"
  ]
  node [
    id 123
    label "jaki&#347;"
  ]
  node [
    id 124
    label "Rwanda"
  ]
  node [
    id 125
    label "Filipiny"
  ]
  node [
    id 126
    label "Monako"
  ]
  node [
    id 127
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 128
    label "Korea"
  ]
  node [
    id 129
    label "Czarnog&#243;ra"
  ]
  node [
    id 130
    label "Ghana"
  ]
  node [
    id 131
    label "Malawi"
  ]
  node [
    id 132
    label "Indonezja"
  ]
  node [
    id 133
    label "Bu&#322;garia"
  ]
  node [
    id 134
    label "Nauru"
  ]
  node [
    id 135
    label "Kenia"
  ]
  node [
    id 136
    label "Kambod&#380;a"
  ]
  node [
    id 137
    label "Mali"
  ]
  node [
    id 138
    label "Austria"
  ]
  node [
    id 139
    label "interior"
  ]
  node [
    id 140
    label "Armenia"
  ]
  node [
    id 141
    label "Fid&#380;i"
  ]
  node [
    id 142
    label "Tuwalu"
  ]
  node [
    id 143
    label "Etiopia"
  ]
  node [
    id 144
    label "Malezja"
  ]
  node [
    id 145
    label "Malta"
  ]
  node [
    id 146
    label "Tad&#380;ykistan"
  ]
  node [
    id 147
    label "Grenada"
  ]
  node [
    id 148
    label "Wehrlen"
  ]
  node [
    id 149
    label "para"
  ]
  node [
    id 150
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 151
    label "Rumunia"
  ]
  node [
    id 152
    label "Maroko"
  ]
  node [
    id 153
    label "Bhutan"
  ]
  node [
    id 154
    label "S&#322;owacja"
  ]
  node [
    id 155
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 156
    label "Seszele"
  ]
  node [
    id 157
    label "Kuwejt"
  ]
  node [
    id 158
    label "Arabia_Saudyjska"
  ]
  node [
    id 159
    label "Kanada"
  ]
  node [
    id 160
    label "Ekwador"
  ]
  node [
    id 161
    label "Japonia"
  ]
  node [
    id 162
    label "ziemia"
  ]
  node [
    id 163
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 164
    label "Hiszpania"
  ]
  node [
    id 165
    label "Wyspy_Marshalla"
  ]
  node [
    id 166
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 167
    label "D&#380;ibuti"
  ]
  node [
    id 168
    label "Botswana"
  ]
  node [
    id 169
    label "grupa"
  ]
  node [
    id 170
    label "Wietnam"
  ]
  node [
    id 171
    label "Egipt"
  ]
  node [
    id 172
    label "Burkina_Faso"
  ]
  node [
    id 173
    label "Niemcy"
  ]
  node [
    id 174
    label "Khitai"
  ]
  node [
    id 175
    label "Macedonia"
  ]
  node [
    id 176
    label "Albania"
  ]
  node [
    id 177
    label "Madagaskar"
  ]
  node [
    id 178
    label "Bahrajn"
  ]
  node [
    id 179
    label "Jemen"
  ]
  node [
    id 180
    label "Lesoto"
  ]
  node [
    id 181
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 182
    label "Samoa"
  ]
  node [
    id 183
    label "Andora"
  ]
  node [
    id 184
    label "Chiny"
  ]
  node [
    id 185
    label "Cypr"
  ]
  node [
    id 186
    label "Wielka_Brytania"
  ]
  node [
    id 187
    label "Ukraina"
  ]
  node [
    id 188
    label "Paragwaj"
  ]
  node [
    id 189
    label "Trynidad_i_Tobago"
  ]
  node [
    id 190
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 191
    label "Libia"
  ]
  node [
    id 192
    label "Surinam"
  ]
  node [
    id 193
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 194
    label "Nigeria"
  ]
  node [
    id 195
    label "Australia"
  ]
  node [
    id 196
    label "Honduras"
  ]
  node [
    id 197
    label "Peru"
  ]
  node [
    id 198
    label "USA"
  ]
  node [
    id 199
    label "Bangladesz"
  ]
  node [
    id 200
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 201
    label "Kazachstan"
  ]
  node [
    id 202
    label "holoarktyka"
  ]
  node [
    id 203
    label "Nepal"
  ]
  node [
    id 204
    label "Sudan"
  ]
  node [
    id 205
    label "Irak"
  ]
  node [
    id 206
    label "San_Marino"
  ]
  node [
    id 207
    label "Burundi"
  ]
  node [
    id 208
    label "Dominikana"
  ]
  node [
    id 209
    label "Komory"
  ]
  node [
    id 210
    label "granica_pa&#324;stwa"
  ]
  node [
    id 211
    label "Gwatemala"
  ]
  node [
    id 212
    label "Antarktis"
  ]
  node [
    id 213
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 214
    label "Brunei"
  ]
  node [
    id 215
    label "Iran"
  ]
  node [
    id 216
    label "Zimbabwe"
  ]
  node [
    id 217
    label "Namibia"
  ]
  node [
    id 218
    label "Meksyk"
  ]
  node [
    id 219
    label "Kamerun"
  ]
  node [
    id 220
    label "zwrot"
  ]
  node [
    id 221
    label "Somalia"
  ]
  node [
    id 222
    label "Angola"
  ]
  node [
    id 223
    label "Gabon"
  ]
  node [
    id 224
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 225
    label "Nowa_Zelandia"
  ]
  node [
    id 226
    label "Mozambik"
  ]
  node [
    id 227
    label "Tunezja"
  ]
  node [
    id 228
    label "Tajwan"
  ]
  node [
    id 229
    label "Liban"
  ]
  node [
    id 230
    label "Jordania"
  ]
  node [
    id 231
    label "Tonga"
  ]
  node [
    id 232
    label "Czad"
  ]
  node [
    id 233
    label "Gwinea"
  ]
  node [
    id 234
    label "Liberia"
  ]
  node [
    id 235
    label "Belize"
  ]
  node [
    id 236
    label "Benin"
  ]
  node [
    id 237
    label "&#321;otwa"
  ]
  node [
    id 238
    label "Syria"
  ]
  node [
    id 239
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 240
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 241
    label "Dominika"
  ]
  node [
    id 242
    label "Antigua_i_Barbuda"
  ]
  node [
    id 243
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 244
    label "Hanower"
  ]
  node [
    id 245
    label "partia"
  ]
  node [
    id 246
    label "Afganistan"
  ]
  node [
    id 247
    label "W&#322;ochy"
  ]
  node [
    id 248
    label "Kiribati"
  ]
  node [
    id 249
    label "Szwajcaria"
  ]
  node [
    id 250
    label "Chorwacja"
  ]
  node [
    id 251
    label "Sahara_Zachodnia"
  ]
  node [
    id 252
    label "Tajlandia"
  ]
  node [
    id 253
    label "Salwador"
  ]
  node [
    id 254
    label "Bahamy"
  ]
  node [
    id 255
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 256
    label "S&#322;owenia"
  ]
  node [
    id 257
    label "Gambia"
  ]
  node [
    id 258
    label "Urugwaj"
  ]
  node [
    id 259
    label "Zair"
  ]
  node [
    id 260
    label "Erytrea"
  ]
  node [
    id 261
    label "Rosja"
  ]
  node [
    id 262
    label "Mauritius"
  ]
  node [
    id 263
    label "Niger"
  ]
  node [
    id 264
    label "Uganda"
  ]
  node [
    id 265
    label "Turkmenistan"
  ]
  node [
    id 266
    label "Turcja"
  ]
  node [
    id 267
    label "Irlandia"
  ]
  node [
    id 268
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 269
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 270
    label "Gwinea_Bissau"
  ]
  node [
    id 271
    label "Belgia"
  ]
  node [
    id 272
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 273
    label "Palau"
  ]
  node [
    id 274
    label "Barbados"
  ]
  node [
    id 275
    label "Wenezuela"
  ]
  node [
    id 276
    label "W&#281;gry"
  ]
  node [
    id 277
    label "Chile"
  ]
  node [
    id 278
    label "Argentyna"
  ]
  node [
    id 279
    label "Kolumbia"
  ]
  node [
    id 280
    label "Sierra_Leone"
  ]
  node [
    id 281
    label "Azerbejd&#380;an"
  ]
  node [
    id 282
    label "Kongo"
  ]
  node [
    id 283
    label "Pakistan"
  ]
  node [
    id 284
    label "Liechtenstein"
  ]
  node [
    id 285
    label "Nikaragua"
  ]
  node [
    id 286
    label "Senegal"
  ]
  node [
    id 287
    label "Indie"
  ]
  node [
    id 288
    label "Suazi"
  ]
  node [
    id 289
    label "Polska"
  ]
  node [
    id 290
    label "Algieria"
  ]
  node [
    id 291
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 292
    label "terytorium"
  ]
  node [
    id 293
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 294
    label "Jamajka"
  ]
  node [
    id 295
    label "Kostaryka"
  ]
  node [
    id 296
    label "Timor_Wschodni"
  ]
  node [
    id 297
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 298
    label "Kuba"
  ]
  node [
    id 299
    label "Mauretania"
  ]
  node [
    id 300
    label "Portoryko"
  ]
  node [
    id 301
    label "Brazylia"
  ]
  node [
    id 302
    label "Mo&#322;dawia"
  ]
  node [
    id 303
    label "organizacja"
  ]
  node [
    id 304
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 305
    label "Litwa"
  ]
  node [
    id 306
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 307
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 308
    label "Izrael"
  ]
  node [
    id 309
    label "Grecja"
  ]
  node [
    id 310
    label "Kirgistan"
  ]
  node [
    id 311
    label "Holandia"
  ]
  node [
    id 312
    label "Sri_Lanka"
  ]
  node [
    id 313
    label "Katar"
  ]
  node [
    id 314
    label "Mikronezja"
  ]
  node [
    id 315
    label "Laos"
  ]
  node [
    id 316
    label "Mongolia"
  ]
  node [
    id 317
    label "Malediwy"
  ]
  node [
    id 318
    label "Zambia"
  ]
  node [
    id 319
    label "Tanzania"
  ]
  node [
    id 320
    label "Gujana"
  ]
  node [
    id 321
    label "Uzbekistan"
  ]
  node [
    id 322
    label "Panama"
  ]
  node [
    id 323
    label "Czechy"
  ]
  node [
    id 324
    label "Gruzja"
  ]
  node [
    id 325
    label "Serbia"
  ]
  node [
    id 326
    label "Francja"
  ]
  node [
    id 327
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 328
    label "Togo"
  ]
  node [
    id 329
    label "Estonia"
  ]
  node [
    id 330
    label "Boliwia"
  ]
  node [
    id 331
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 332
    label "Oman"
  ]
  node [
    id 333
    label "Wyspy_Salomona"
  ]
  node [
    id 334
    label "Haiti"
  ]
  node [
    id 335
    label "Luksemburg"
  ]
  node [
    id 336
    label "Portugalia"
  ]
  node [
    id 337
    label "Birma"
  ]
  node [
    id 338
    label "Rodezja"
  ]
  node [
    id 339
    label "sceptyczny"
  ]
  node [
    id 340
    label "remark"
  ]
  node [
    id 341
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 342
    label "u&#380;ywa&#263;"
  ]
  node [
    id 343
    label "okre&#347;la&#263;"
  ]
  node [
    id 344
    label "j&#281;zyk"
  ]
  node [
    id 345
    label "say"
  ]
  node [
    id 346
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 347
    label "formu&#322;owa&#263;"
  ]
  node [
    id 348
    label "talk"
  ]
  node [
    id 349
    label "powiada&#263;"
  ]
  node [
    id 350
    label "informowa&#263;"
  ]
  node [
    id 351
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 352
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 353
    label "wydobywa&#263;"
  ]
  node [
    id 354
    label "express"
  ]
  node [
    id 355
    label "chew_the_fat"
  ]
  node [
    id 356
    label "dysfonia"
  ]
  node [
    id 357
    label "umie&#263;"
  ]
  node [
    id 358
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 359
    label "tell"
  ]
  node [
    id 360
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 361
    label "wyra&#380;a&#263;"
  ]
  node [
    id 362
    label "gaworzy&#263;"
  ]
  node [
    id 363
    label "rozmawia&#263;"
  ]
  node [
    id 364
    label "dziama&#263;"
  ]
  node [
    id 365
    label "prawi&#263;"
  ]
  node [
    id 366
    label "w_chuj"
  ]
  node [
    id 367
    label "du&#380;y"
  ]
  node [
    id 368
    label "cz&#281;sto"
  ]
  node [
    id 369
    label "mocno"
  ]
  node [
    id 370
    label "wiela"
  ]
  node [
    id 371
    label "czasokres"
  ]
  node [
    id 372
    label "trawienie"
  ]
  node [
    id 373
    label "kategoria_gramatyczna"
  ]
  node [
    id 374
    label "period"
  ]
  node [
    id 375
    label "odczyt"
  ]
  node [
    id 376
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 377
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 378
    label "chwila"
  ]
  node [
    id 379
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 380
    label "poprzedzenie"
  ]
  node [
    id 381
    label "koniugacja"
  ]
  node [
    id 382
    label "dzieje"
  ]
  node [
    id 383
    label "poprzedzi&#263;"
  ]
  node [
    id 384
    label "przep&#322;ywanie"
  ]
  node [
    id 385
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 386
    label "odwlekanie_si&#281;"
  ]
  node [
    id 387
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 388
    label "Zeitgeist"
  ]
  node [
    id 389
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 390
    label "okres_czasu"
  ]
  node [
    id 391
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 392
    label "pochodzi&#263;"
  ]
  node [
    id 393
    label "schy&#322;ek"
  ]
  node [
    id 394
    label "czwarty_wymiar"
  ]
  node [
    id 395
    label "chronometria"
  ]
  node [
    id 396
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 397
    label "poprzedzanie"
  ]
  node [
    id 398
    label "pogoda"
  ]
  node [
    id 399
    label "zegar"
  ]
  node [
    id 400
    label "pochodzenie"
  ]
  node [
    id 401
    label "poprzedza&#263;"
  ]
  node [
    id 402
    label "trawi&#263;"
  ]
  node [
    id 403
    label "time_period"
  ]
  node [
    id 404
    label "rachuba_czasu"
  ]
  node [
    id 405
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 406
    label "czasoprzestrze&#324;"
  ]
  node [
    id 407
    label "laba"
  ]
  node [
    id 408
    label "si&#281;ga&#263;"
  ]
  node [
    id 409
    label "trwa&#263;"
  ]
  node [
    id 410
    label "obecno&#347;&#263;"
  ]
  node [
    id 411
    label "stan"
  ]
  node [
    id 412
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 413
    label "stand"
  ]
  node [
    id 414
    label "mie&#263;_miejsce"
  ]
  node [
    id 415
    label "uczestniczy&#263;"
  ]
  node [
    id 416
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 417
    label "equal"
  ]
  node [
    id 418
    label "legislacyjnie"
  ]
  node [
    id 419
    label "kognicja"
  ]
  node [
    id 420
    label "przebieg"
  ]
  node [
    id 421
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 422
    label "wydarzenie"
  ]
  node [
    id 423
    label "przes&#322;anka"
  ]
  node [
    id 424
    label "rozprawa"
  ]
  node [
    id 425
    label "zjawisko"
  ]
  node [
    id 426
    label "nast&#281;pstwo"
  ]
  node [
    id 427
    label "dalekosi&#281;&#380;ny"
  ]
  node [
    id 428
    label "d&#322;ugofalowo"
  ]
  node [
    id 429
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 430
    label "rzekn&#261;&#263;"
  ]
  node [
    id 431
    label "okre&#347;li&#263;"
  ]
  node [
    id 432
    label "wyrazi&#263;"
  ]
  node [
    id 433
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 434
    label "unwrap"
  ]
  node [
    id 435
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 436
    label "convey"
  ]
  node [
    id 437
    label "discover"
  ]
  node [
    id 438
    label "wydoby&#263;"
  ]
  node [
    id 439
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 440
    label "poda&#263;"
  ]
  node [
    id 441
    label "share"
  ]
  node [
    id 442
    label "authorize"
  ]
  node [
    id 443
    label "przyznawa&#263;"
  ]
  node [
    id 444
    label "wsp&#243;&#322;uczestniczy&#263;"
  ]
  node [
    id 445
    label "zakres"
  ]
  node [
    id 446
    label "faza_Ksi&#281;&#380;yca"
  ]
  node [
    id 447
    label "szczyt"
  ]
  node [
    id 448
    label "dokument"
  ]
  node [
    id 449
    label "rehabilitowanie_si&#281;"
  ]
  node [
    id 450
    label "reputacja"
  ]
  node [
    id 451
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 452
    label "rehabilitowa&#263;_si&#281;"
  ]
  node [
    id 453
    label "cecha"
  ]
  node [
    id 454
    label "zrehabilitowanie_si&#281;"
  ]
  node [
    id 455
    label "informacja"
  ]
  node [
    id 456
    label "sofcik"
  ]
  node [
    id 457
    label "appraisal"
  ]
  node [
    id 458
    label "ekspertyza"
  ]
  node [
    id 459
    label "zrehabilitowa&#263;_si&#281;"
  ]
  node [
    id 460
    label "pogl&#261;d"
  ]
  node [
    id 461
    label "kryterium"
  ]
  node [
    id 462
    label "wielko&#347;&#263;"
  ]
  node [
    id 463
    label "zapoznawanie"
  ]
  node [
    id 464
    label "wysy&#322;anie"
  ]
  node [
    id 465
    label "rozwijanie"
  ]
  node [
    id 466
    label "training"
  ]
  node [
    id 467
    label "pomaganie"
  ]
  node [
    id 468
    label "o&#347;wiecanie"
  ]
  node [
    id 469
    label "kliker"
  ]
  node [
    id 470
    label "pouczenie"
  ]
  node [
    id 471
    label "nadrz&#281;dny"
  ]
  node [
    id 472
    label "zbiorowy"
  ]
  node [
    id 473
    label "&#322;&#261;czny"
  ]
  node [
    id 474
    label "kompletny"
  ]
  node [
    id 475
    label "og&#243;lnie"
  ]
  node [
    id 476
    label "ca&#322;y"
  ]
  node [
    id 477
    label "og&#243;&#322;owy"
  ]
  node [
    id 478
    label "powszechnie"
  ]
  node [
    id 479
    label "Mickiewicz"
  ]
  node [
    id 480
    label "szkolenie"
  ]
  node [
    id 481
    label "przepisa&#263;"
  ]
  node [
    id 482
    label "lesson"
  ]
  node [
    id 483
    label "praktyka"
  ]
  node [
    id 484
    label "metoda"
  ]
  node [
    id 485
    label "kara"
  ]
  node [
    id 486
    label "zda&#263;"
  ]
  node [
    id 487
    label "system"
  ]
  node [
    id 488
    label "przepisanie"
  ]
  node [
    id 489
    label "sztuba"
  ]
  node [
    id 490
    label "stopek"
  ]
  node [
    id 491
    label "school"
  ]
  node [
    id 492
    label "absolwent"
  ]
  node [
    id 493
    label "gabinet"
  ]
  node [
    id 494
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 495
    label "ideologia"
  ]
  node [
    id 496
    label "lekcja"
  ]
  node [
    id 497
    label "muzyka"
  ]
  node [
    id 498
    label "podr&#281;cznik"
  ]
  node [
    id 499
    label "zdanie"
  ]
  node [
    id 500
    label "siedziba"
  ]
  node [
    id 501
    label "sekretariat"
  ]
  node [
    id 502
    label "do&#347;wiadczenie"
  ]
  node [
    id 503
    label "tablica"
  ]
  node [
    id 504
    label "teren_szko&#322;y"
  ]
  node [
    id 505
    label "instytucja"
  ]
  node [
    id 506
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 507
    label "klasa"
  ]
  node [
    id 508
    label "zako&#324;cza&#263;"
  ]
  node [
    id 509
    label "przestawa&#263;"
  ]
  node [
    id 510
    label "robi&#263;"
  ]
  node [
    id 511
    label "satisfy"
  ]
  node [
    id 512
    label "close"
  ]
  node [
    id 513
    label "determine"
  ]
  node [
    id 514
    label "dokonywa&#263;_&#380;ywota"
  ]
  node [
    id 515
    label "stanowi&#263;"
  ]
  node [
    id 516
    label "&#347;wiadectwo"
  ]
  node [
    id 517
    label "studni&#243;wka"
  ]
  node [
    id 518
    label "matriculation"
  ]
  node [
    id 519
    label "egzamin"
  ]
  node [
    id 520
    label "musie&#263;"
  ]
  node [
    id 521
    label "due"
  ]
  node [
    id 522
    label "mundurowanie"
  ]
  node [
    id 523
    label "zr&#243;wnywanie_si&#281;"
  ]
  node [
    id 524
    label "jednakowo"
  ]
  node [
    id 525
    label "taki&#380;"
  ]
  node [
    id 526
    label "zr&#243;wnanie"
  ]
  node [
    id 527
    label "mundurowa&#263;"
  ]
  node [
    id 528
    label "zr&#243;wnywanie"
  ]
  node [
    id 529
    label "identyczny"
  ]
  node [
    id 530
    label "proceed"
  ]
  node [
    id 531
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 532
    label "bangla&#263;"
  ]
  node [
    id 533
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 534
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 535
    label "run"
  ]
  node [
    id 536
    label "tryb"
  ]
  node [
    id 537
    label "p&#322;ywa&#263;"
  ]
  node [
    id 538
    label "continue"
  ]
  node [
    id 539
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 540
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 541
    label "przebiega&#263;"
  ]
  node [
    id 542
    label "wk&#322;ada&#263;"
  ]
  node [
    id 543
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 544
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 545
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 546
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 547
    label "krok"
  ]
  node [
    id 548
    label "str&#243;j"
  ]
  node [
    id 549
    label "bywa&#263;"
  ]
  node [
    id 550
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 551
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 552
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 553
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 554
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 555
    label "stara&#263;_si&#281;"
  ]
  node [
    id 556
    label "carry"
  ]
  node [
    id 557
    label "zorganizowa&#263;"
  ]
  node [
    id 558
    label "model"
  ]
  node [
    id 559
    label "zwyk&#322;o&#347;&#263;"
  ]
  node [
    id 560
    label "taniec_towarzyski"
  ]
  node [
    id 561
    label "ordinariness"
  ]
  node [
    id 562
    label "organizowanie"
  ]
  node [
    id 563
    label "criterion"
  ]
  node [
    id 564
    label "zorganizowanie"
  ]
  node [
    id 565
    label "organizowa&#263;"
  ]
  node [
    id 566
    label "istota"
  ]
  node [
    id 567
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 568
    label "robienie"
  ]
  node [
    id 569
    label "kr&#261;&#380;enie"
  ]
  node [
    id 570
    label "rzecz"
  ]
  node [
    id 571
    label "zbacza&#263;"
  ]
  node [
    id 572
    label "entity"
  ]
  node [
    id 573
    label "element"
  ]
  node [
    id 574
    label "omawia&#263;"
  ]
  node [
    id 575
    label "om&#243;wi&#263;"
  ]
  node [
    id 576
    label "sponiewiera&#263;"
  ]
  node [
    id 577
    label "sponiewieranie"
  ]
  node [
    id 578
    label "omawianie"
  ]
  node [
    id 579
    label "charakter"
  ]
  node [
    id 580
    label "w&#261;tek"
  ]
  node [
    id 581
    label "thing"
  ]
  node [
    id 582
    label "zboczenie"
  ]
  node [
    id 583
    label "zbaczanie"
  ]
  node [
    id 584
    label "tre&#347;&#263;"
  ]
  node [
    id 585
    label "tematyka"
  ]
  node [
    id 586
    label "kultura"
  ]
  node [
    id 587
    label "zboczy&#263;"
  ]
  node [
    id 588
    label "discipline"
  ]
  node [
    id 589
    label "om&#243;wienie"
  ]
  node [
    id 590
    label "develop"
  ]
  node [
    id 591
    label "spowodowa&#263;"
  ]
  node [
    id 592
    label "powi&#281;kszy&#263;"
  ]
  node [
    id 593
    label "zwi&#281;kszy&#263;"
  ]
  node [
    id 594
    label "teaching"
  ]
  node [
    id 595
    label "uczony"
  ]
  node [
    id 596
    label "education"
  ]
  node [
    id 597
    label "oddzia&#322;ywanie"
  ]
  node [
    id 598
    label "przyuczenie"
  ]
  node [
    id 599
    label "pracowanie"
  ]
  node [
    id 600
    label "przyuczanie"
  ]
  node [
    id 601
    label "m&#261;drze"
  ]
  node [
    id 602
    label "wychowywanie"
  ]
  node [
    id 603
    label "zinterpretowanie"
  ]
  node [
    id 604
    label "czynno&#347;&#263;"
  ]
  node [
    id 605
    label "wnioskowanie"
  ]
  node [
    id 606
    label "domy&#347;lanie_si&#281;"
  ]
  node [
    id 607
    label "skupianie_si&#281;"
  ]
  node [
    id 608
    label "domy&#347;lenie_si&#281;"
  ]
  node [
    id 609
    label "judgment"
  ]
  node [
    id 610
    label "proces_my&#347;lowy"
  ]
  node [
    id 611
    label "dok&#322;adnie"
  ]
  node [
    id 612
    label "upewnienie_si&#281;"
  ]
  node [
    id 613
    label "wierzenie"
  ]
  node [
    id 614
    label "mo&#380;liwy"
  ]
  node [
    id 615
    label "ufanie"
  ]
  node [
    id 616
    label "spokojny"
  ]
  node [
    id 617
    label "upewnianie_si&#281;"
  ]
  node [
    id 618
    label "zmienno&#347;&#263;"
  ]
  node [
    id 619
    label "rozci&#261;gliwo&#347;&#263;"
  ]
  node [
    id 620
    label "poj&#281;cie"
  ]
  node [
    id 621
    label "pilnowanie"
  ]
  node [
    id 622
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 623
    label "troskanie_si&#281;"
  ]
  node [
    id 624
    label "s&#261;dzenie"
  ]
  node [
    id 625
    label "walczy&#263;"
  ]
  node [
    id 626
    label "reflection"
  ]
  node [
    id 627
    label "wzlecie&#263;"
  ]
  node [
    id 628
    label "wzlecenie"
  ]
  node [
    id 629
    label "walczenie"
  ]
  node [
    id 630
    label "treatment"
  ]
  node [
    id 631
    label "przehulanie"
  ]
  node [
    id 632
    label "lampartowanie_si&#281;"
  ]
  node [
    id 633
    label "lumpowanie_si&#281;"
  ]
  node [
    id 634
    label "przeta&#324;czenie"
  ]
  node [
    id 635
    label "zachowywanie_si&#281;"
  ]
  node [
    id 636
    label "lumpowanie"
  ]
  node [
    id 637
    label "szalony"
  ]
  node [
    id 638
    label "lubienie"
  ]
  node [
    id 639
    label "bawienie_si&#281;"
  ]
  node [
    id 640
    label "worship"
  ]
  node [
    id 641
    label "dzianie_si&#281;"
  ]
  node [
    id 642
    label "bomblowanie"
  ]
  node [
    id 643
    label "play"
  ]
  node [
    id 644
    label "potrzebnie"
  ]
  node [
    id 645
    label "przydatny"
  ]
  node [
    id 646
    label "okre&#347;lony"
  ]
  node [
    id 647
    label "ability"
  ]
  node [
    id 648
    label "authority"
  ]
  node [
    id 649
    label "sprawno&#347;&#263;"
  ]
  node [
    id 650
    label "znawstwo"
  ]
  node [
    id 651
    label "prawo"
  ]
  node [
    id 652
    label "zdolno&#347;&#263;"
  ]
  node [
    id 653
    label "gestia"
  ]
  node [
    id 654
    label "stoisko"
  ]
  node [
    id 655
    label "plac"
  ]
  node [
    id 656
    label "emitowanie"
  ]
  node [
    id 657
    label "targowica"
  ]
  node [
    id 658
    label "wprowadzanie"
  ]
  node [
    id 659
    label "emitowa&#263;"
  ]
  node [
    id 660
    label "wprowadzi&#263;"
  ]
  node [
    id 661
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 662
    label "rynek_wt&#243;rny"
  ]
  node [
    id 663
    label "wprowadzenie"
  ]
  node [
    id 664
    label "kram"
  ]
  node [
    id 665
    label "wprowadza&#263;"
  ]
  node [
    id 666
    label "pojawienie_si&#281;"
  ]
  node [
    id 667
    label "rynek_podstawowy"
  ]
  node [
    id 668
    label "biznes"
  ]
  node [
    id 669
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 670
    label "obiekt_handlowy"
  ]
  node [
    id 671
    label "konsument"
  ]
  node [
    id 672
    label "wytw&#243;rca"
  ]
  node [
    id 673
    label "segment_rynku"
  ]
  node [
    id 674
    label "wska&#378;nik_rynkowy"
  ]
  node [
    id 675
    label "rynek_r&#243;wnoleg&#322;y"
  ]
  node [
    id 676
    label "stosunek_pracy"
  ]
  node [
    id 677
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 678
    label "benedykty&#324;ski"
  ]
  node [
    id 679
    label "zaw&#243;d"
  ]
  node [
    id 680
    label "kierownictwo"
  ]
  node [
    id 681
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 682
    label "wytw&#243;r"
  ]
  node [
    id 683
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 684
    label "tynkarski"
  ]
  node [
    id 685
    label "czynnik_produkcji"
  ]
  node [
    id 686
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 687
    label "zobowi&#261;zanie"
  ]
  node [
    id 688
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 689
    label "tyrka"
  ]
  node [
    id 690
    label "pracowa&#263;"
  ]
  node [
    id 691
    label "poda&#380;_pracy"
  ]
  node [
    id 692
    label "miejsce"
  ]
  node [
    id 693
    label "zak&#322;ad"
  ]
  node [
    id 694
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 695
    label "najem"
  ]
  node [
    id 696
    label "silny"
  ]
  node [
    id 697
    label "wa&#380;nie"
  ]
  node [
    id 698
    label "eksponowany"
  ]
  node [
    id 699
    label "istotnie"
  ]
  node [
    id 700
    label "znaczny"
  ]
  node [
    id 701
    label "wynios&#322;y"
  ]
  node [
    id 702
    label "dono&#347;ny"
  ]
  node [
    id 703
    label "piwo"
  ]
  node [
    id 704
    label "wyb&#243;r"
  ]
  node [
    id 705
    label "prospect"
  ]
  node [
    id 706
    label "egzekutywa"
  ]
  node [
    id 707
    label "alternatywa"
  ]
  node [
    id 708
    label "potencja&#322;"
  ]
  node [
    id 709
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 710
    label "obliczeniowo"
  ]
  node [
    id 711
    label "operator_modalny"
  ]
  node [
    id 712
    label "posiada&#263;"
  ]
  node [
    id 713
    label "zrobienie"
  ]
  node [
    id 714
    label "consumption"
  ]
  node [
    id 715
    label "spowodowanie"
  ]
  node [
    id 716
    label "zareagowanie"
  ]
  node [
    id 717
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 718
    label "erecting"
  ]
  node [
    id 719
    label "movement"
  ]
  node [
    id 720
    label "entertainment"
  ]
  node [
    id 721
    label "zacz&#281;cie"
  ]
  node [
    id 722
    label "badanie"
  ]
  node [
    id 723
    label "arrange"
  ]
  node [
    id 724
    label "dress"
  ]
  node [
    id 725
    label "wyszkoli&#263;"
  ]
  node [
    id 726
    label "narz&#261;dzi&#263;"
  ]
  node [
    id 727
    label "wytworzy&#263;"
  ]
  node [
    id 728
    label "ukierunkowa&#263;"
  ]
  node [
    id 729
    label "train"
  ]
  node [
    id 730
    label "wykona&#263;"
  ]
  node [
    id 731
    label "zrobi&#263;"
  ]
  node [
    id 732
    label "cook"
  ]
  node [
    id 733
    label "set"
  ]
  node [
    id 734
    label "device"
  ]
  node [
    id 735
    label "program_u&#380;ytkowy"
  ]
  node [
    id 736
    label "intencja"
  ]
  node [
    id 737
    label "agreement"
  ]
  node [
    id 738
    label "pomys&#322;"
  ]
  node [
    id 739
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 740
    label "plan"
  ]
  node [
    id 741
    label "dokumentacja"
  ]
  node [
    id 742
    label "anatomopatolog"
  ]
  node [
    id 743
    label "rewizja"
  ]
  node [
    id 744
    label "oznaka"
  ]
  node [
    id 745
    label "ferment"
  ]
  node [
    id 746
    label "komplet"
  ]
  node [
    id 747
    label "tura"
  ]
  node [
    id 748
    label "amendment"
  ]
  node [
    id 749
    label "zmianka"
  ]
  node [
    id 750
    label "odmienianie"
  ]
  node [
    id 751
    label "passage"
  ]
  node [
    id 752
    label "change"
  ]
  node [
    id 753
    label "poprawa"
  ]
  node [
    id 754
    label "odbitka"
  ]
  node [
    id 755
    label "strojenie"
  ]
  node [
    id 756
    label "dzia&#322;"
  ]
  node [
    id 757
    label "opustka"
  ]
  node [
    id 758
    label "podstawowy"
  ]
  node [
    id 759
    label "strategia"
  ]
  node [
    id 760
    label "pot&#281;ga"
  ]
  node [
    id 761
    label "zasadzenie"
  ]
  node [
    id 762
    label "za&#322;o&#380;enie"
  ]
  node [
    id 763
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 764
    label "&#347;ciana"
  ]
  node [
    id 765
    label "documentation"
  ]
  node [
    id 766
    label "dzieci&#281;ctwo"
  ]
  node [
    id 767
    label "bok"
  ]
  node [
    id 768
    label "d&#243;&#322;"
  ]
  node [
    id 769
    label "punkt_odniesienia"
  ]
  node [
    id 770
    label "column"
  ]
  node [
    id 771
    label "zasadzi&#263;"
  ]
  node [
    id 772
    label "background"
  ]
  node [
    id 773
    label "zaplanowany"
  ]
  node [
    id 774
    label "kierunkowy"
  ]
  node [
    id 775
    label "zdeklarowany"
  ]
  node [
    id 776
    label "przewidywalny"
  ]
  node [
    id 777
    label "reprezentatywny"
  ]
  node [
    id 778
    label "celowy"
  ]
  node [
    id 779
    label "programowo"
  ]
  node [
    id 780
    label "my&#347;le&#263;"
  ]
  node [
    id 781
    label "involve"
  ]
  node [
    id 782
    label "czyj&#347;"
  ]
  node [
    id 783
    label "m&#261;&#380;"
  ]
  node [
    id 784
    label "cz&#322;owiek"
  ]
  node [
    id 785
    label "profesor"
  ]
  node [
    id 786
    label "kszta&#322;ciciel"
  ]
  node [
    id 787
    label "jegomo&#347;&#263;"
  ]
  node [
    id 788
    label "pracodawca"
  ]
  node [
    id 789
    label "rz&#261;dzenie"
  ]
  node [
    id 790
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 791
    label "ch&#322;opina"
  ]
  node [
    id 792
    label "bratek"
  ]
  node [
    id 793
    label "opiekun"
  ]
  node [
    id 794
    label "doros&#322;y"
  ]
  node [
    id 795
    label "preceptor"
  ]
  node [
    id 796
    label "Midas"
  ]
  node [
    id 797
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 798
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 799
    label "murza"
  ]
  node [
    id 800
    label "ojciec"
  ]
  node [
    id 801
    label "androlog"
  ]
  node [
    id 802
    label "pupil"
  ]
  node [
    id 803
    label "efendi"
  ]
  node [
    id 804
    label "nabab"
  ]
  node [
    id 805
    label "w&#322;odarz"
  ]
  node [
    id 806
    label "szkolnik"
  ]
  node [
    id 807
    label "pedagog"
  ]
  node [
    id 808
    label "popularyzator"
  ]
  node [
    id 809
    label "andropauza"
  ]
  node [
    id 810
    label "gra_w_karty"
  ]
  node [
    id 811
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 812
    label "Mieszko_I"
  ]
  node [
    id 813
    label "bogaty"
  ]
  node [
    id 814
    label "samiec"
  ]
  node [
    id 815
    label "przyw&#243;dca"
  ]
  node [
    id 816
    label "belfer"
  ]
  node [
    id 817
    label "dyplomata"
  ]
  node [
    id 818
    label "wys&#322;annik"
  ]
  node [
    id 819
    label "przedstawiciel"
  ]
  node [
    id 820
    label "kurier_dyplomatyczny"
  ]
  node [
    id 821
    label "ablegat"
  ]
  node [
    id 822
    label "klubista"
  ]
  node [
    id 823
    label "Miko&#322;ajczyk"
  ]
  node [
    id 824
    label "Korwin"
  ]
  node [
    id 825
    label "parlamentarzysta"
  ]
  node [
    id 826
    label "dyscyplina_partyjna"
  ]
  node [
    id 827
    label "izba_ni&#380;sza"
  ]
  node [
    id 828
    label "poselstwo"
  ]
  node [
    id 829
    label "dobrze_wychowany"
  ]
  node [
    id 830
    label "grzeczny"
  ]
  node [
    id 831
    label "sk&#322;onny"
  ]
  node [
    id 832
    label "uprzejmie"
  ]
  node [
    id 833
    label "mi&#322;y"
  ]
  node [
    id 834
    label "przysparza&#263;"
  ]
  node [
    id 835
    label "give"
  ]
  node [
    id 836
    label "kali&#263;_si&#281;"
  ]
  node [
    id 837
    label "bonanza"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 113
  ]
  edge [
    source 7
    target 114
  ]
  edge [
    source 7
    target 115
  ]
  edge [
    source 7
    target 116
  ]
  edge [
    source 7
    target 117
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 8
    target 122
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 10
    target 125
  ]
  edge [
    source 10
    target 126
  ]
  edge [
    source 10
    target 127
  ]
  edge [
    source 10
    target 128
  ]
  edge [
    source 10
    target 129
  ]
  edge [
    source 10
    target 130
  ]
  edge [
    source 10
    target 131
  ]
  edge [
    source 10
    target 132
  ]
  edge [
    source 10
    target 133
  ]
  edge [
    source 10
    target 134
  ]
  edge [
    source 10
    target 135
  ]
  edge [
    source 10
    target 136
  ]
  edge [
    source 10
    target 137
  ]
  edge [
    source 10
    target 138
  ]
  edge [
    source 10
    target 139
  ]
  edge [
    source 10
    target 140
  ]
  edge [
    source 10
    target 141
  ]
  edge [
    source 10
    target 142
  ]
  edge [
    source 10
    target 143
  ]
  edge [
    source 10
    target 144
  ]
  edge [
    source 10
    target 145
  ]
  edge [
    source 10
    target 146
  ]
  edge [
    source 10
    target 147
  ]
  edge [
    source 10
    target 148
  ]
  edge [
    source 10
    target 149
  ]
  edge [
    source 10
    target 150
  ]
  edge [
    source 10
    target 151
  ]
  edge [
    source 10
    target 152
  ]
  edge [
    source 10
    target 153
  ]
  edge [
    source 10
    target 154
  ]
  edge [
    source 10
    target 155
  ]
  edge [
    source 10
    target 156
  ]
  edge [
    source 10
    target 157
  ]
  edge [
    source 10
    target 158
  ]
  edge [
    source 10
    target 159
  ]
  edge [
    source 10
    target 160
  ]
  edge [
    source 10
    target 161
  ]
  edge [
    source 10
    target 162
  ]
  edge [
    source 10
    target 163
  ]
  edge [
    source 10
    target 164
  ]
  edge [
    source 10
    target 165
  ]
  edge [
    source 10
    target 166
  ]
  edge [
    source 10
    target 167
  ]
  edge [
    source 10
    target 168
  ]
  edge [
    source 10
    target 169
  ]
  edge [
    source 10
    target 170
  ]
  edge [
    source 10
    target 171
  ]
  edge [
    source 10
    target 172
  ]
  edge [
    source 10
    target 173
  ]
  edge [
    source 10
    target 174
  ]
  edge [
    source 10
    target 175
  ]
  edge [
    source 10
    target 176
  ]
  edge [
    source 10
    target 177
  ]
  edge [
    source 10
    target 178
  ]
  edge [
    source 10
    target 179
  ]
  edge [
    source 10
    target 180
  ]
  edge [
    source 10
    target 181
  ]
  edge [
    source 10
    target 182
  ]
  edge [
    source 10
    target 183
  ]
  edge [
    source 10
    target 184
  ]
  edge [
    source 10
    target 185
  ]
  edge [
    source 10
    target 186
  ]
  edge [
    source 10
    target 187
  ]
  edge [
    source 10
    target 188
  ]
  edge [
    source 10
    target 189
  ]
  edge [
    source 10
    target 190
  ]
  edge [
    source 10
    target 191
  ]
  edge [
    source 10
    target 192
  ]
  edge [
    source 10
    target 193
  ]
  edge [
    source 10
    target 194
  ]
  edge [
    source 10
    target 195
  ]
  edge [
    source 10
    target 196
  ]
  edge [
    source 10
    target 197
  ]
  edge [
    source 10
    target 198
  ]
  edge [
    source 10
    target 199
  ]
  edge [
    source 10
    target 200
  ]
  edge [
    source 10
    target 201
  ]
  edge [
    source 10
    target 202
  ]
  edge [
    source 10
    target 203
  ]
  edge [
    source 10
    target 204
  ]
  edge [
    source 10
    target 205
  ]
  edge [
    source 10
    target 206
  ]
  edge [
    source 10
    target 207
  ]
  edge [
    source 10
    target 208
  ]
  edge [
    source 10
    target 209
  ]
  edge [
    source 10
    target 210
  ]
  edge [
    source 10
    target 211
  ]
  edge [
    source 10
    target 212
  ]
  edge [
    source 10
    target 213
  ]
  edge [
    source 10
    target 214
  ]
  edge [
    source 10
    target 215
  ]
  edge [
    source 10
    target 216
  ]
  edge [
    source 10
    target 217
  ]
  edge [
    source 10
    target 218
  ]
  edge [
    source 10
    target 219
  ]
  edge [
    source 10
    target 220
  ]
  edge [
    source 10
    target 221
  ]
  edge [
    source 10
    target 222
  ]
  edge [
    source 10
    target 223
  ]
  edge [
    source 10
    target 224
  ]
  edge [
    source 10
    target 225
  ]
  edge [
    source 10
    target 226
  ]
  edge [
    source 10
    target 227
  ]
  edge [
    source 10
    target 228
  ]
  edge [
    source 10
    target 229
  ]
  edge [
    source 10
    target 230
  ]
  edge [
    source 10
    target 231
  ]
  edge [
    source 10
    target 232
  ]
  edge [
    source 10
    target 233
  ]
  edge [
    source 10
    target 234
  ]
  edge [
    source 10
    target 235
  ]
  edge [
    source 10
    target 236
  ]
  edge [
    source 10
    target 237
  ]
  edge [
    source 10
    target 238
  ]
  edge [
    source 10
    target 239
  ]
  edge [
    source 10
    target 240
  ]
  edge [
    source 10
    target 241
  ]
  edge [
    source 10
    target 242
  ]
  edge [
    source 10
    target 243
  ]
  edge [
    source 10
    target 244
  ]
  edge [
    source 10
    target 245
  ]
  edge [
    source 10
    target 246
  ]
  edge [
    source 10
    target 247
  ]
  edge [
    source 10
    target 248
  ]
  edge [
    source 10
    target 249
  ]
  edge [
    source 10
    target 250
  ]
  edge [
    source 10
    target 251
  ]
  edge [
    source 10
    target 252
  ]
  edge [
    source 10
    target 253
  ]
  edge [
    source 10
    target 254
  ]
  edge [
    source 10
    target 255
  ]
  edge [
    source 10
    target 256
  ]
  edge [
    source 10
    target 257
  ]
  edge [
    source 10
    target 258
  ]
  edge [
    source 10
    target 259
  ]
  edge [
    source 10
    target 260
  ]
  edge [
    source 10
    target 261
  ]
  edge [
    source 10
    target 262
  ]
  edge [
    source 10
    target 263
  ]
  edge [
    source 10
    target 264
  ]
  edge [
    source 10
    target 265
  ]
  edge [
    source 10
    target 266
  ]
  edge [
    source 10
    target 267
  ]
  edge [
    source 10
    target 268
  ]
  edge [
    source 10
    target 269
  ]
  edge [
    source 10
    target 270
  ]
  edge [
    source 10
    target 271
  ]
  edge [
    source 10
    target 272
  ]
  edge [
    source 10
    target 273
  ]
  edge [
    source 10
    target 274
  ]
  edge [
    source 10
    target 275
  ]
  edge [
    source 10
    target 276
  ]
  edge [
    source 10
    target 277
  ]
  edge [
    source 10
    target 278
  ]
  edge [
    source 10
    target 279
  ]
  edge [
    source 10
    target 280
  ]
  edge [
    source 10
    target 281
  ]
  edge [
    source 10
    target 282
  ]
  edge [
    source 10
    target 283
  ]
  edge [
    source 10
    target 284
  ]
  edge [
    source 10
    target 285
  ]
  edge [
    source 10
    target 286
  ]
  edge [
    source 10
    target 287
  ]
  edge [
    source 10
    target 288
  ]
  edge [
    source 10
    target 289
  ]
  edge [
    source 10
    target 290
  ]
  edge [
    source 10
    target 291
  ]
  edge [
    source 10
    target 292
  ]
  edge [
    source 10
    target 293
  ]
  edge [
    source 10
    target 294
  ]
  edge [
    source 10
    target 295
  ]
  edge [
    source 10
    target 296
  ]
  edge [
    source 10
    target 297
  ]
  edge [
    source 10
    target 298
  ]
  edge [
    source 10
    target 299
  ]
  edge [
    source 10
    target 300
  ]
  edge [
    source 10
    target 301
  ]
  edge [
    source 10
    target 302
  ]
  edge [
    source 10
    target 303
  ]
  edge [
    source 10
    target 304
  ]
  edge [
    source 10
    target 305
  ]
  edge [
    source 10
    target 306
  ]
  edge [
    source 10
    target 307
  ]
  edge [
    source 10
    target 308
  ]
  edge [
    source 10
    target 309
  ]
  edge [
    source 10
    target 310
  ]
  edge [
    source 10
    target 311
  ]
  edge [
    source 10
    target 312
  ]
  edge [
    source 10
    target 313
  ]
  edge [
    source 10
    target 314
  ]
  edge [
    source 10
    target 315
  ]
  edge [
    source 10
    target 316
  ]
  edge [
    source 10
    target 317
  ]
  edge [
    source 10
    target 318
  ]
  edge [
    source 10
    target 319
  ]
  edge [
    source 10
    target 320
  ]
  edge [
    source 10
    target 321
  ]
  edge [
    source 10
    target 322
  ]
  edge [
    source 10
    target 323
  ]
  edge [
    source 10
    target 324
  ]
  edge [
    source 10
    target 325
  ]
  edge [
    source 10
    target 326
  ]
  edge [
    source 10
    target 327
  ]
  edge [
    source 10
    target 328
  ]
  edge [
    source 10
    target 329
  ]
  edge [
    source 10
    target 330
  ]
  edge [
    source 10
    target 331
  ]
  edge [
    source 10
    target 332
  ]
  edge [
    source 10
    target 333
  ]
  edge [
    source 10
    target 334
  ]
  edge [
    source 10
    target 335
  ]
  edge [
    source 10
    target 336
  ]
  edge [
    source 10
    target 337
  ]
  edge [
    source 10
    target 338
  ]
  edge [
    source 10
    target 69
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 339
  ]
  edge [
    source 11
    target 27
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 71
  ]
  edge [
    source 12
    target 72
  ]
  edge [
    source 12
    target 340
  ]
  edge [
    source 12
    target 341
  ]
  edge [
    source 12
    target 342
  ]
  edge [
    source 12
    target 343
  ]
  edge [
    source 12
    target 344
  ]
  edge [
    source 12
    target 345
  ]
  edge [
    source 12
    target 346
  ]
  edge [
    source 12
    target 347
  ]
  edge [
    source 12
    target 348
  ]
  edge [
    source 12
    target 349
  ]
  edge [
    source 12
    target 350
  ]
  edge [
    source 12
    target 351
  ]
  edge [
    source 12
    target 352
  ]
  edge [
    source 12
    target 353
  ]
  edge [
    source 12
    target 354
  ]
  edge [
    source 12
    target 355
  ]
  edge [
    source 12
    target 356
  ]
  edge [
    source 12
    target 357
  ]
  edge [
    source 12
    target 358
  ]
  edge [
    source 12
    target 359
  ]
  edge [
    source 12
    target 360
  ]
  edge [
    source 12
    target 361
  ]
  edge [
    source 12
    target 362
  ]
  edge [
    source 12
    target 363
  ]
  edge [
    source 12
    target 364
  ]
  edge [
    source 12
    target 365
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 366
  ]
  edge [
    source 13
    target 45
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 367
  ]
  edge [
    source 14
    target 368
  ]
  edge [
    source 14
    target 369
  ]
  edge [
    source 14
    target 370
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 371
  ]
  edge [
    source 15
    target 372
  ]
  edge [
    source 15
    target 373
  ]
  edge [
    source 15
    target 374
  ]
  edge [
    source 15
    target 375
  ]
  edge [
    source 15
    target 376
  ]
  edge [
    source 15
    target 377
  ]
  edge [
    source 15
    target 378
  ]
  edge [
    source 15
    target 379
  ]
  edge [
    source 15
    target 380
  ]
  edge [
    source 15
    target 381
  ]
  edge [
    source 15
    target 382
  ]
  edge [
    source 15
    target 383
  ]
  edge [
    source 15
    target 384
  ]
  edge [
    source 15
    target 385
  ]
  edge [
    source 15
    target 386
  ]
  edge [
    source 15
    target 387
  ]
  edge [
    source 15
    target 388
  ]
  edge [
    source 15
    target 389
  ]
  edge [
    source 15
    target 390
  ]
  edge [
    source 15
    target 391
  ]
  edge [
    source 15
    target 392
  ]
  edge [
    source 15
    target 393
  ]
  edge [
    source 15
    target 394
  ]
  edge [
    source 15
    target 395
  ]
  edge [
    source 15
    target 396
  ]
  edge [
    source 15
    target 397
  ]
  edge [
    source 15
    target 398
  ]
  edge [
    source 15
    target 399
  ]
  edge [
    source 15
    target 400
  ]
  edge [
    source 15
    target 401
  ]
  edge [
    source 15
    target 402
  ]
  edge [
    source 15
    target 403
  ]
  edge [
    source 15
    target 404
  ]
  edge [
    source 15
    target 405
  ]
  edge [
    source 15
    target 406
  ]
  edge [
    source 15
    target 407
  ]
  edge [
    source 15
    target 27
  ]
  edge [
    source 15
    target 61
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 31
  ]
  edge [
    source 16
    target 32
  ]
  edge [
    source 16
    target 44
  ]
  edge [
    source 16
    target 45
  ]
  edge [
    source 16
    target 51
  ]
  edge [
    source 16
    target 52
  ]
  edge [
    source 16
    target 67
  ]
  edge [
    source 16
    target 68
  ]
  edge [
    source 16
    target 70
  ]
  edge [
    source 16
    target 71
  ]
  edge [
    source 16
    target 20
  ]
  edge [
    source 16
    target 408
  ]
  edge [
    source 16
    target 409
  ]
  edge [
    source 16
    target 410
  ]
  edge [
    source 16
    target 411
  ]
  edge [
    source 16
    target 412
  ]
  edge [
    source 16
    target 413
  ]
  edge [
    source 16
    target 414
  ]
  edge [
    source 16
    target 415
  ]
  edge [
    source 16
    target 34
  ]
  edge [
    source 16
    target 416
  ]
  edge [
    source 16
    target 417
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 418
  ]
  edge [
    source 17
    target 419
  ]
  edge [
    source 17
    target 420
  ]
  edge [
    source 17
    target 421
  ]
  edge [
    source 17
    target 422
  ]
  edge [
    source 17
    target 423
  ]
  edge [
    source 17
    target 424
  ]
  edge [
    source 17
    target 425
  ]
  edge [
    source 17
    target 426
  ]
  edge [
    source 17
    target 44
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 427
  ]
  edge [
    source 18
    target 428
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 429
  ]
  edge [
    source 19
    target 354
  ]
  edge [
    source 19
    target 430
  ]
  edge [
    source 19
    target 431
  ]
  edge [
    source 19
    target 432
  ]
  edge [
    source 19
    target 433
  ]
  edge [
    source 19
    target 434
  ]
  edge [
    source 19
    target 435
  ]
  edge [
    source 19
    target 436
  ]
  edge [
    source 19
    target 437
  ]
  edge [
    source 19
    target 438
  ]
  edge [
    source 19
    target 439
  ]
  edge [
    source 19
    target 440
  ]
  edge [
    source 19
    target 54
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 30
  ]
  edge [
    source 20
    target 41
  ]
  edge [
    source 20
    target 73
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 441
  ]
  edge [
    source 21
    target 442
  ]
  edge [
    source 21
    target 443
  ]
  edge [
    source 21
    target 444
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 445
  ]
  edge [
    source 22
    target 446
  ]
  edge [
    source 22
    target 447
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 448
  ]
  edge [
    source 23
    target 449
  ]
  edge [
    source 23
    target 450
  ]
  edge [
    source 23
    target 451
  ]
  edge [
    source 23
    target 452
  ]
  edge [
    source 23
    target 453
  ]
  edge [
    source 23
    target 454
  ]
  edge [
    source 23
    target 455
  ]
  edge [
    source 23
    target 456
  ]
  edge [
    source 23
    target 457
  ]
  edge [
    source 23
    target 458
  ]
  edge [
    source 23
    target 459
  ]
  edge [
    source 23
    target 460
  ]
  edge [
    source 23
    target 461
  ]
  edge [
    source 23
    target 462
  ]
  edge [
    source 23
    target 41
  ]
  edge [
    source 23
    target 64
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 52
  ]
  edge [
    source 24
    target 64
  ]
  edge [
    source 24
    target 463
  ]
  edge [
    source 24
    target 464
  ]
  edge [
    source 24
    target 80
  ]
  edge [
    source 24
    target 465
  ]
  edge [
    source 24
    target 466
  ]
  edge [
    source 24
    target 467
  ]
  edge [
    source 24
    target 468
  ]
  edge [
    source 24
    target 469
  ]
  edge [
    source 24
    target 88
  ]
  edge [
    source 24
    target 470
  ]
  edge [
    source 24
    target 90
  ]
  edge [
    source 24
    target 26
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 65
  ]
  edge [
    source 25
    target 471
  ]
  edge [
    source 25
    target 472
  ]
  edge [
    source 25
    target 473
  ]
  edge [
    source 25
    target 474
  ]
  edge [
    source 25
    target 475
  ]
  edge [
    source 25
    target 476
  ]
  edge [
    source 25
    target 477
  ]
  edge [
    source 25
    target 478
  ]
  edge [
    source 25
    target 32
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 66
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 479
  ]
  edge [
    source 27
    target 480
  ]
  edge [
    source 27
    target 481
  ]
  edge [
    source 27
    target 482
  ]
  edge [
    source 27
    target 169
  ]
  edge [
    source 27
    target 483
  ]
  edge [
    source 27
    target 484
  ]
  edge [
    source 27
    target 87
  ]
  edge [
    source 27
    target 485
  ]
  edge [
    source 27
    target 486
  ]
  edge [
    source 27
    target 89
  ]
  edge [
    source 27
    target 75
  ]
  edge [
    source 27
    target 487
  ]
  edge [
    source 27
    target 488
  ]
  edge [
    source 27
    target 489
  ]
  edge [
    source 27
    target 84
  ]
  edge [
    source 27
    target 490
  ]
  edge [
    source 27
    target 491
  ]
  edge [
    source 27
    target 492
  ]
  edge [
    source 27
    target 83
  ]
  edge [
    source 27
    target 493
  ]
  edge [
    source 27
    target 494
  ]
  edge [
    source 27
    target 495
  ]
  edge [
    source 27
    target 496
  ]
  edge [
    source 27
    target 497
  ]
  edge [
    source 27
    target 498
  ]
  edge [
    source 27
    target 499
  ]
  edge [
    source 27
    target 500
  ]
  edge [
    source 27
    target 501
  ]
  edge [
    source 27
    target 90
  ]
  edge [
    source 27
    target 502
  ]
  edge [
    source 27
    target 503
  ]
  edge [
    source 27
    target 504
  ]
  edge [
    source 27
    target 505
  ]
  edge [
    source 27
    target 506
  ]
  edge [
    source 27
    target 86
  ]
  edge [
    source 27
    target 91
  ]
  edge [
    source 27
    target 507
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 508
  ]
  edge [
    source 28
    target 509
  ]
  edge [
    source 28
    target 510
  ]
  edge [
    source 28
    target 511
  ]
  edge [
    source 28
    target 512
  ]
  edge [
    source 28
    target 513
  ]
  edge [
    source 28
    target 514
  ]
  edge [
    source 28
    target 515
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 516
  ]
  edge [
    source 30
    target 517
  ]
  edge [
    source 30
    target 518
  ]
  edge [
    source 30
    target 519
  ]
  edge [
    source 31
    target 520
  ]
  edge [
    source 31
    target 521
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 522
  ]
  edge [
    source 32
    target 523
  ]
  edge [
    source 32
    target 524
  ]
  edge [
    source 32
    target 525
  ]
  edge [
    source 32
    target 526
  ]
  edge [
    source 32
    target 527
  ]
  edge [
    source 32
    target 528
  ]
  edge [
    source 32
    target 529
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 46
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 47
  ]
  edge [
    source 34
    target 530
  ]
  edge [
    source 34
    target 531
  ]
  edge [
    source 34
    target 532
  ]
  edge [
    source 34
    target 533
  ]
  edge [
    source 34
    target 534
  ]
  edge [
    source 34
    target 535
  ]
  edge [
    source 34
    target 536
  ]
  edge [
    source 34
    target 537
  ]
  edge [
    source 34
    target 538
  ]
  edge [
    source 34
    target 539
  ]
  edge [
    source 34
    target 540
  ]
  edge [
    source 34
    target 541
  ]
  edge [
    source 34
    target 414
  ]
  edge [
    source 34
    target 542
  ]
  edge [
    source 34
    target 543
  ]
  edge [
    source 34
    target 544
  ]
  edge [
    source 34
    target 149
  ]
  edge [
    source 34
    target 545
  ]
  edge [
    source 34
    target 546
  ]
  edge [
    source 34
    target 547
  ]
  edge [
    source 34
    target 548
  ]
  edge [
    source 34
    target 549
  ]
  edge [
    source 34
    target 550
  ]
  edge [
    source 34
    target 551
  ]
  edge [
    source 34
    target 552
  ]
  edge [
    source 34
    target 553
  ]
  edge [
    source 34
    target 554
  ]
  edge [
    source 34
    target 364
  ]
  edge [
    source 34
    target 555
  ]
  edge [
    source 34
    target 556
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 557
  ]
  edge [
    source 35
    target 558
  ]
  edge [
    source 35
    target 559
  ]
  edge [
    source 35
    target 560
  ]
  edge [
    source 35
    target 561
  ]
  edge [
    source 35
    target 562
  ]
  edge [
    source 35
    target 563
  ]
  edge [
    source 35
    target 564
  ]
  edge [
    source 35
    target 505
  ]
  edge [
    source 35
    target 565
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 566
  ]
  edge [
    source 36
    target 567
  ]
  edge [
    source 36
    target 455
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 568
  ]
  edge [
    source 37
    target 569
  ]
  edge [
    source 37
    target 570
  ]
  edge [
    source 37
    target 571
  ]
  edge [
    source 37
    target 572
  ]
  edge [
    source 37
    target 573
  ]
  edge [
    source 37
    target 574
  ]
  edge [
    source 37
    target 575
  ]
  edge [
    source 37
    target 576
  ]
  edge [
    source 37
    target 577
  ]
  edge [
    source 37
    target 578
  ]
  edge [
    source 37
    target 579
  ]
  edge [
    source 37
    target 79
  ]
  edge [
    source 37
    target 580
  ]
  edge [
    source 37
    target 581
  ]
  edge [
    source 37
    target 582
  ]
  edge [
    source 37
    target 583
  ]
  edge [
    source 37
    target 584
  ]
  edge [
    source 37
    target 585
  ]
  edge [
    source 37
    target 566
  ]
  edge [
    source 37
    target 550
  ]
  edge [
    source 37
    target 586
  ]
  edge [
    source 37
    target 587
  ]
  edge [
    source 37
    target 588
  ]
  edge [
    source 37
    target 589
  ]
  edge [
    source 37
    target 63
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 590
  ]
  edge [
    source 38
    target 591
  ]
  edge [
    source 38
    target 592
  ]
  edge [
    source 38
    target 593
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 463
  ]
  edge [
    source 39
    target 594
  ]
  edge [
    source 39
    target 595
  ]
  edge [
    source 39
    target 470
  ]
  edge [
    source 39
    target 596
  ]
  edge [
    source 39
    target 465
  ]
  edge [
    source 39
    target 466
  ]
  edge [
    source 39
    target 597
  ]
  edge [
    source 39
    target 598
  ]
  edge [
    source 39
    target 599
  ]
  edge [
    source 39
    target 467
  ]
  edge [
    source 39
    target 469
  ]
  edge [
    source 39
    target 600
  ]
  edge [
    source 39
    target 468
  ]
  edge [
    source 39
    target 601
  ]
  edge [
    source 39
    target 602
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 603
  ]
  edge [
    source 40
    target 568
  ]
  edge [
    source 40
    target 604
  ]
  edge [
    source 40
    target 605
  ]
  edge [
    source 40
    target 606
  ]
  edge [
    source 40
    target 607
  ]
  edge [
    source 40
    target 608
  ]
  edge [
    source 40
    target 609
  ]
  edge [
    source 40
    target 610
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 67
  ]
  edge [
    source 41
    target 611
  ]
  edge [
    source 41
    target 64
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 612
  ]
  edge [
    source 42
    target 613
  ]
  edge [
    source 42
    target 614
  ]
  edge [
    source 42
    target 615
  ]
  edge [
    source 42
    target 123
  ]
  edge [
    source 42
    target 616
  ]
  edge [
    source 42
    target 617
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 618
  ]
  edge [
    source 43
    target 619
  ]
  edge [
    source 43
    target 620
  ]
  edge [
    source 43
    target 56
  ]
  edge [
    source 44
    target 603
  ]
  edge [
    source 44
    target 621
  ]
  edge [
    source 44
    target 568
  ]
  edge [
    source 44
    target 605
  ]
  edge [
    source 44
    target 622
  ]
  edge [
    source 44
    target 623
  ]
  edge [
    source 44
    target 607
  ]
  edge [
    source 44
    target 624
  ]
  edge [
    source 44
    target 609
  ]
  edge [
    source 44
    target 625
  ]
  edge [
    source 44
    target 425
  ]
  edge [
    source 44
    target 604
  ]
  edge [
    source 44
    target 626
  ]
  edge [
    source 44
    target 627
  ]
  edge [
    source 44
    target 608
  ]
  edge [
    source 44
    target 610
  ]
  edge [
    source 44
    target 628
  ]
  edge [
    source 44
    target 629
  ]
  edge [
    source 44
    target 630
  ]
  edge [
    source 44
    target 606
  ]
  edge [
    source 44
    target 57
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 631
  ]
  edge [
    source 45
    target 632
  ]
  edge [
    source 45
    target 633
  ]
  edge [
    source 45
    target 634
  ]
  edge [
    source 45
    target 635
  ]
  edge [
    source 45
    target 636
  ]
  edge [
    source 45
    target 637
  ]
  edge [
    source 45
    target 638
  ]
  edge [
    source 45
    target 639
  ]
  edge [
    source 45
    target 640
  ]
  edge [
    source 45
    target 641
  ]
  edge [
    source 45
    target 642
  ]
  edge [
    source 45
    target 643
  ]
  edge [
    source 46
    target 644
  ]
  edge [
    source 46
    target 645
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 646
  ]
  edge [
    source 47
    target 123
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 647
  ]
  edge [
    source 48
    target 648
  ]
  edge [
    source 48
    target 649
  ]
  edge [
    source 48
    target 650
  ]
  edge [
    source 48
    target 651
  ]
  edge [
    source 48
    target 652
  ]
  edge [
    source 48
    target 653
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 654
  ]
  edge [
    source 49
    target 655
  ]
  edge [
    source 49
    target 656
  ]
  edge [
    source 49
    target 657
  ]
  edge [
    source 49
    target 658
  ]
  edge [
    source 49
    target 659
  ]
  edge [
    source 49
    target 660
  ]
  edge [
    source 49
    target 661
  ]
  edge [
    source 49
    target 662
  ]
  edge [
    source 49
    target 663
  ]
  edge [
    source 49
    target 664
  ]
  edge [
    source 49
    target 665
  ]
  edge [
    source 49
    target 666
  ]
  edge [
    source 49
    target 667
  ]
  edge [
    source 49
    target 668
  ]
  edge [
    source 49
    target 82
  ]
  edge [
    source 49
    target 669
  ]
  edge [
    source 49
    target 670
  ]
  edge [
    source 49
    target 671
  ]
  edge [
    source 49
    target 672
  ]
  edge [
    source 49
    target 673
  ]
  edge [
    source 49
    target 674
  ]
  edge [
    source 49
    target 675
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 676
  ]
  edge [
    source 50
    target 677
  ]
  edge [
    source 50
    target 678
  ]
  edge [
    source 50
    target 599
  ]
  edge [
    source 50
    target 679
  ]
  edge [
    source 50
    target 680
  ]
  edge [
    source 50
    target 61
  ]
  edge [
    source 50
    target 681
  ]
  edge [
    source 50
    target 682
  ]
  edge [
    source 50
    target 683
  ]
  edge [
    source 50
    target 684
  ]
  edge [
    source 50
    target 685
  ]
  edge [
    source 50
    target 686
  ]
  edge [
    source 50
    target 687
  ]
  edge [
    source 50
    target 688
  ]
  edge [
    source 50
    target 604
  ]
  edge [
    source 50
    target 689
  ]
  edge [
    source 50
    target 690
  ]
  edge [
    source 50
    target 500
  ]
  edge [
    source 50
    target 691
  ]
  edge [
    source 50
    target 692
  ]
  edge [
    source 50
    target 693
  ]
  edge [
    source 50
    target 694
  ]
  edge [
    source 50
    target 695
  ]
  edge [
    source 51
    target 696
  ]
  edge [
    source 51
    target 697
  ]
  edge [
    source 51
    target 698
  ]
  edge [
    source 51
    target 699
  ]
  edge [
    source 51
    target 700
  ]
  edge [
    source 51
    target 106
  ]
  edge [
    source 51
    target 701
  ]
  edge [
    source 51
    target 702
  ]
  edge [
    source 51
    target 64
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 703
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 647
  ]
  edge [
    source 54
    target 704
  ]
  edge [
    source 54
    target 705
  ]
  edge [
    source 54
    target 706
  ]
  edge [
    source 54
    target 707
  ]
  edge [
    source 54
    target 708
  ]
  edge [
    source 54
    target 453
  ]
  edge [
    source 54
    target 709
  ]
  edge [
    source 54
    target 710
  ]
  edge [
    source 54
    target 422
  ]
  edge [
    source 54
    target 711
  ]
  edge [
    source 54
    target 712
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 713
  ]
  edge [
    source 55
    target 714
  ]
  edge [
    source 55
    target 604
  ]
  edge [
    source 55
    target 715
  ]
  edge [
    source 55
    target 716
  ]
  edge [
    source 55
    target 717
  ]
  edge [
    source 55
    target 718
  ]
  edge [
    source 55
    target 719
  ]
  edge [
    source 55
    target 720
  ]
  edge [
    source 55
    target 721
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 722
  ]
  edge [
    source 56
    target 90
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 723
  ]
  edge [
    source 57
    target 591
  ]
  edge [
    source 57
    target 724
  ]
  edge [
    source 57
    target 725
  ]
  edge [
    source 57
    target 726
  ]
  edge [
    source 57
    target 727
  ]
  edge [
    source 57
    target 728
  ]
  edge [
    source 57
    target 729
  ]
  edge [
    source 57
    target 730
  ]
  edge [
    source 57
    target 731
  ]
  edge [
    source 57
    target 732
  ]
  edge [
    source 57
    target 733
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 448
  ]
  edge [
    source 60
    target 734
  ]
  edge [
    source 60
    target 735
  ]
  edge [
    source 60
    target 736
  ]
  edge [
    source 60
    target 737
  ]
  edge [
    source 60
    target 738
  ]
  edge [
    source 60
    target 739
  ]
  edge [
    source 60
    target 740
  ]
  edge [
    source 60
    target 741
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 742
  ]
  edge [
    source 61
    target 743
  ]
  edge [
    source 61
    target 744
  ]
  edge [
    source 61
    target 745
  ]
  edge [
    source 61
    target 746
  ]
  edge [
    source 61
    target 747
  ]
  edge [
    source 61
    target 748
  ]
  edge [
    source 61
    target 749
  ]
  edge [
    source 61
    target 750
  ]
  edge [
    source 61
    target 751
  ]
  edge [
    source 61
    target 425
  ]
  edge [
    source 61
    target 752
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 753
  ]
  edge [
    source 62
    target 754
  ]
  edge [
    source 62
    target 755
  ]
  edge [
    source 62
    target 756
  ]
  edge [
    source 62
    target 757
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 758
  ]
  edge [
    source 63
    target 759
  ]
  edge [
    source 63
    target 760
  ]
  edge [
    source 63
    target 761
  ]
  edge [
    source 63
    target 762
  ]
  edge [
    source 63
    target 763
  ]
  edge [
    source 63
    target 764
  ]
  edge [
    source 63
    target 765
  ]
  edge [
    source 63
    target 766
  ]
  edge [
    source 63
    target 738
  ]
  edge [
    source 63
    target 767
  ]
  edge [
    source 63
    target 768
  ]
  edge [
    source 63
    target 769
  ]
  edge [
    source 63
    target 770
  ]
  edge [
    source 63
    target 771
  ]
  edge [
    source 63
    target 772
  ]
  edge [
    source 64
    target 773
  ]
  edge [
    source 64
    target 774
  ]
  edge [
    source 64
    target 775
  ]
  edge [
    source 64
    target 776
  ]
  edge [
    source 64
    target 777
  ]
  edge [
    source 64
    target 778
  ]
  edge [
    source 64
    target 779
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 66
    target 780
  ]
  edge [
    source 66
    target 781
  ]
  edge [
    source 67
    target 782
  ]
  edge [
    source 67
    target 783
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 69
    target 784
  ]
  edge [
    source 69
    target 785
  ]
  edge [
    source 69
    target 786
  ]
  edge [
    source 69
    target 787
  ]
  edge [
    source 69
    target 220
  ]
  edge [
    source 69
    target 788
  ]
  edge [
    source 69
    target 789
  ]
  edge [
    source 69
    target 783
  ]
  edge [
    source 69
    target 790
  ]
  edge [
    source 69
    target 791
  ]
  edge [
    source 69
    target 792
  ]
  edge [
    source 69
    target 793
  ]
  edge [
    source 69
    target 794
  ]
  edge [
    source 69
    target 795
  ]
  edge [
    source 69
    target 796
  ]
  edge [
    source 69
    target 797
  ]
  edge [
    source 69
    target 798
  ]
  edge [
    source 69
    target 799
  ]
  edge [
    source 69
    target 800
  ]
  edge [
    source 69
    target 801
  ]
  edge [
    source 69
    target 802
  ]
  edge [
    source 69
    target 803
  ]
  edge [
    source 69
    target 804
  ]
  edge [
    source 69
    target 805
  ]
  edge [
    source 69
    target 806
  ]
  edge [
    source 69
    target 807
  ]
  edge [
    source 69
    target 808
  ]
  edge [
    source 69
    target 809
  ]
  edge [
    source 69
    target 810
  ]
  edge [
    source 69
    target 811
  ]
  edge [
    source 69
    target 812
  ]
  edge [
    source 69
    target 813
  ]
  edge [
    source 69
    target 814
  ]
  edge [
    source 69
    target 815
  ]
  edge [
    source 69
    target 816
  ]
  edge [
    source 70
    target 817
  ]
  edge [
    source 70
    target 818
  ]
  edge [
    source 70
    target 819
  ]
  edge [
    source 70
    target 820
  ]
  edge [
    source 70
    target 821
  ]
  edge [
    source 70
    target 822
  ]
  edge [
    source 70
    target 823
  ]
  edge [
    source 70
    target 824
  ]
  edge [
    source 70
    target 825
  ]
  edge [
    source 70
    target 826
  ]
  edge [
    source 70
    target 827
  ]
  edge [
    source 70
    target 828
  ]
  edge [
    source 71
    target 829
  ]
  edge [
    source 71
    target 830
  ]
  edge [
    source 71
    target 831
  ]
  edge [
    source 71
    target 832
  ]
  edge [
    source 71
    target 833
  ]
  edge [
    source 72
    target 73
  ]
  edge [
    source 72
    target 834
  ]
  edge [
    source 72
    target 835
  ]
  edge [
    source 72
    target 836
  ]
  edge [
    source 72
    target 837
  ]
]
