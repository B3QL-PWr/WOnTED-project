graph [
  maxDegree 2
  minDegree 1
  meanDegree 1.3333333333333333
  density 0.6666666666666666
  graphCliqueNumber 2
  node [
    id 0
    label "cent"
    origin "text"
  ]
  node [
    id 1
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 2
    label "moneta"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
]
