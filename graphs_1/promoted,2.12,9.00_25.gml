graph [
  maxDegree 16
  minDegree 1
  meanDegree 1.9375
  density 0.0625
  graphCliqueNumber 2
  node [
    id 0
    label "gdy"
    origin "text"
  ]
  node [
    id 1
    label "spotka&#263;"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "rutyna"
    origin "text"
  ]
  node [
    id 4
    label "brawura"
    origin "text"
  ]
  node [
    id 5
    label "befall"
  ]
  node [
    id 6
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 7
    label "pozna&#263;"
  ]
  node [
    id 8
    label "spowodowa&#263;"
  ]
  node [
    id 9
    label "go_steady"
  ]
  node [
    id 10
    label "insert"
  ]
  node [
    id 11
    label "znale&#378;&#263;"
  ]
  node [
    id 12
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 13
    label "visualize"
  ]
  node [
    id 14
    label "przeciwutleniacz"
  ]
  node [
    id 15
    label "skill"
  ]
  node [
    id 16
    label "flawonoid"
  ]
  node [
    id 17
    label "witamina"
  ]
  node [
    id 18
    label "metyl"
  ]
  node [
    id 19
    label "wiedza"
  ]
  node [
    id 20
    label "nawyk"
  ]
  node [
    id 21
    label "glukoza"
  ]
  node [
    id 22
    label "znawstwo"
  ]
  node [
    id 23
    label "adeptness"
  ]
  node [
    id 24
    label "kwercetyna"
  ]
  node [
    id 25
    label "glikozyd"
  ]
  node [
    id 26
    label "eksperiencja"
  ]
  node [
    id 27
    label "monotonia"
  ]
  node [
    id 28
    label "efektowno&#347;&#263;"
  ]
  node [
    id 29
    label "odwaga"
  ]
  node [
    id 30
    label "wymy&#347;lno&#347;&#263;"
  ]
  node [
    id 31
    label "nieostro&#380;no&#347;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
]
