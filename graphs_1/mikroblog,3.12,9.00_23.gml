graph [
  maxDegree 39
  minDegree 1
  meanDegree 1.967741935483871
  density 0.03225806451612903
  graphCliqueNumber 2
  node [
    id 0
    label "kilka"
    origin "text"
  ]
  node [
    id 1
    label "osoba"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "zapyta&#263;"
    origin "text"
  ]
  node [
    id 4
    label "teraz"
    origin "text"
  ]
  node [
    id 5
    label "wygl&#261;da&#263;"
    origin "text"
  ]
  node [
    id 6
    label "roba"
    origin "text"
  ]
  node [
    id 7
    label "pokazforme"
    origin "text"
  ]
  node [
    id 8
    label "&#347;ledziowate"
  ]
  node [
    id 9
    label "ryba"
  ]
  node [
    id 10
    label "Zgredek"
  ]
  node [
    id 11
    label "kategoria_gramatyczna"
  ]
  node [
    id 12
    label "Casanova"
  ]
  node [
    id 13
    label "Don_Juan"
  ]
  node [
    id 14
    label "Gargantua"
  ]
  node [
    id 15
    label "Faust"
  ]
  node [
    id 16
    label "profanum"
  ]
  node [
    id 17
    label "Chocho&#322;"
  ]
  node [
    id 18
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 19
    label "koniugacja"
  ]
  node [
    id 20
    label "Winnetou"
  ]
  node [
    id 21
    label "Dwukwiat"
  ]
  node [
    id 22
    label "homo_sapiens"
  ]
  node [
    id 23
    label "Edyp"
  ]
  node [
    id 24
    label "Herkules_Poirot"
  ]
  node [
    id 25
    label "ludzko&#347;&#263;"
  ]
  node [
    id 26
    label "mikrokosmos"
  ]
  node [
    id 27
    label "person"
  ]
  node [
    id 28
    label "Szwejk"
  ]
  node [
    id 29
    label "portrecista"
  ]
  node [
    id 30
    label "Sherlock_Holmes"
  ]
  node [
    id 31
    label "Hamlet"
  ]
  node [
    id 32
    label "duch"
  ]
  node [
    id 33
    label "oddzia&#322;ywanie"
  ]
  node [
    id 34
    label "g&#322;owa"
  ]
  node [
    id 35
    label "Quasimodo"
  ]
  node [
    id 36
    label "Dulcynea"
  ]
  node [
    id 37
    label "Wallenrod"
  ]
  node [
    id 38
    label "Don_Kiszot"
  ]
  node [
    id 39
    label "Plastu&#347;"
  ]
  node [
    id 40
    label "Harry_Potter"
  ]
  node [
    id 41
    label "figura"
  ]
  node [
    id 42
    label "parali&#380;owa&#263;"
  ]
  node [
    id 43
    label "istota"
  ]
  node [
    id 44
    label "Werter"
  ]
  node [
    id 45
    label "antropochoria"
  ]
  node [
    id 46
    label "posta&#263;"
  ]
  node [
    id 47
    label "sprawdzi&#263;"
  ]
  node [
    id 48
    label "ask"
  ]
  node [
    id 49
    label "przes&#322;ucha&#263;"
  ]
  node [
    id 50
    label "quiz"
  ]
  node [
    id 51
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 52
    label "chwila"
  ]
  node [
    id 53
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 54
    label "by&#263;"
  ]
  node [
    id 55
    label "czeka&#263;"
  ]
  node [
    id 56
    label "lookout"
  ]
  node [
    id 57
    label "wyziera&#263;"
  ]
  node [
    id 58
    label "peep"
  ]
  node [
    id 59
    label "look"
  ]
  node [
    id 60
    label "patrze&#263;"
  ]
  node [
    id 61
    label "suknia"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 61
  ]
]
