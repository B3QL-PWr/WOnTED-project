graph [
  maxDegree 25
  minDegree 1
  meanDegree 1.951219512195122
  density 0.02408912978018669
  graphCliqueNumber 2
  node [
    id 0
    label "heheszki"
    origin "text"
  ]
  node [
    id 1
    label "logikarozowychpaskow"
    origin "text"
  ]
  node [
    id 2
    label "zwiazki"
    origin "text"
  ]
  node [
    id 3
    label "le&#380;a"
    origin "text"
  ]
  node [
    id 4
    label "kwicze"
    origin "text"
  ]
  node [
    id 5
    label "&#380;ona"
    origin "text"
  ]
  node [
    id 6
    label "wr&#243;ci&#263;"
    origin "text"
  ]
  node [
    id 7
    label "zakup"
    origin "text"
  ]
  node [
    id 8
    label "jaki&#347;"
    origin "text"
  ]
  node [
    id 9
    label "hipermarket"
    origin "text"
  ]
  node [
    id 10
    label "kupi&#263;"
    origin "text"
  ]
  node [
    id 11
    label "mnustwo"
    origin "text"
  ]
  node [
    id 12
    label "szpeju"
    origin "text"
  ]
  node [
    id 13
    label "promocja"
    origin "text"
  ]
  node [
    id 14
    label "drewno"
  ]
  node [
    id 15
    label "legowisko"
  ]
  node [
    id 16
    label "ma&#322;&#380;onek"
  ]
  node [
    id 17
    label "panna_m&#322;oda"
  ]
  node [
    id 18
    label "partnerka"
  ]
  node [
    id 19
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 20
    label "&#347;lubna"
  ]
  node [
    id 21
    label "kobita"
  ]
  node [
    id 22
    label "render"
  ]
  node [
    id 23
    label "return"
  ]
  node [
    id 24
    label "zosta&#263;"
  ]
  node [
    id 25
    label "spowodowa&#263;"
  ]
  node [
    id 26
    label "przyj&#347;&#263;"
  ]
  node [
    id 27
    label "zjawi&#263;_si&#281;"
  ]
  node [
    id 28
    label "revive"
  ]
  node [
    id 29
    label "pojedna&#263;_si&#281;"
  ]
  node [
    id 30
    label "podj&#261;&#263;"
  ]
  node [
    id 31
    label "nawi&#261;za&#263;"
  ]
  node [
    id 32
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 33
    label "przyby&#263;"
  ]
  node [
    id 34
    label "recur"
  ]
  node [
    id 35
    label "sprzedaj&#261;cy"
  ]
  node [
    id 36
    label "dobro"
  ]
  node [
    id 37
    label "transakcja"
  ]
  node [
    id 38
    label "jako&#347;"
  ]
  node [
    id 39
    label "charakterystyczny"
  ]
  node [
    id 40
    label "ciekawy"
  ]
  node [
    id 41
    label "jako_tako"
  ]
  node [
    id 42
    label "dziwny"
  ]
  node [
    id 43
    label "niez&#322;y"
  ]
  node [
    id 44
    label "przyzwoity"
  ]
  node [
    id 45
    label "supermarket"
  ]
  node [
    id 46
    label "get"
  ]
  node [
    id 47
    label "wzi&#261;&#263;"
  ]
  node [
    id 48
    label "catch"
  ]
  node [
    id 49
    label "przyj&#261;&#263;"
  ]
  node [
    id 50
    label "beget"
  ]
  node [
    id 51
    label "pozyska&#263;"
  ]
  node [
    id 52
    label "ustawi&#263;"
  ]
  node [
    id 53
    label "uzna&#263;"
  ]
  node [
    id 54
    label "zagra&#263;"
  ]
  node [
    id 55
    label "uwierzy&#263;"
  ]
  node [
    id 56
    label "nominacja"
  ]
  node [
    id 57
    label "sprzeda&#380;"
  ]
  node [
    id 58
    label "zamiana"
  ]
  node [
    id 59
    label "graduacja"
  ]
  node [
    id 60
    label "&#347;wiadectwo"
  ]
  node [
    id 61
    label "gradation"
  ]
  node [
    id 62
    label "brief"
  ]
  node [
    id 63
    label "uzyska&#263;"
  ]
  node [
    id 64
    label "promotion"
  ]
  node [
    id 65
    label "promowa&#263;"
  ]
  node [
    id 66
    label "klasa"
  ]
  node [
    id 67
    label "akcja"
  ]
  node [
    id 68
    label "wypromowa&#263;"
  ]
  node [
    id 69
    label "warcaby"
  ]
  node [
    id 70
    label "popularyzacja"
  ]
  node [
    id 71
    label "bran&#380;a"
  ]
  node [
    id 72
    label "informacja"
  ]
  node [
    id 73
    label "impreza"
  ]
  node [
    id 74
    label "decyzja"
  ]
  node [
    id 75
    label "okazja"
  ]
  node [
    id 76
    label "commencement"
  ]
  node [
    id 77
    label "udzieli&#263;"
  ]
  node [
    id 78
    label "szachy"
  ]
  node [
    id 79
    label "damka"
  ]
  node [
    id 80
    label "Dawid"
  ]
  node [
    id 81
    label "podsi&#261;&#347;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 16
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 22
  ]
  edge [
    source 6
    target 23
  ]
  edge [
    source 6
    target 24
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 35
  ]
  edge [
    source 7
    target 36
  ]
  edge [
    source 7
    target 37
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 38
  ]
  edge [
    source 8
    target 39
  ]
  edge [
    source 8
    target 40
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 45
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 46
  ]
  edge [
    source 10
    target 47
  ]
  edge [
    source 10
    target 48
  ]
  edge [
    source 10
    target 49
  ]
  edge [
    source 10
    target 50
  ]
  edge [
    source 10
    target 51
  ]
  edge [
    source 10
    target 52
  ]
  edge [
    source 10
    target 53
  ]
  edge [
    source 10
    target 54
  ]
  edge [
    source 10
    target 55
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 56
  ]
  edge [
    source 13
    target 57
  ]
  edge [
    source 13
    target 58
  ]
  edge [
    source 13
    target 59
  ]
  edge [
    source 13
    target 60
  ]
  edge [
    source 13
    target 61
  ]
  edge [
    source 13
    target 62
  ]
  edge [
    source 13
    target 63
  ]
  edge [
    source 13
    target 64
  ]
  edge [
    source 13
    target 65
  ]
  edge [
    source 13
    target 66
  ]
  edge [
    source 13
    target 67
  ]
  edge [
    source 13
    target 68
  ]
  edge [
    source 13
    target 69
  ]
  edge [
    source 13
    target 70
  ]
  edge [
    source 13
    target 71
  ]
  edge [
    source 13
    target 72
  ]
  edge [
    source 13
    target 73
  ]
  edge [
    source 13
    target 74
  ]
  edge [
    source 13
    target 75
  ]
  edge [
    source 13
    target 76
  ]
  edge [
    source 13
    target 77
  ]
  edge [
    source 13
    target 78
  ]
  edge [
    source 13
    target 79
  ]
  edge [
    source 80
    target 81
  ]
]
