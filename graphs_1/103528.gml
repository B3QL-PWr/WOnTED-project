graph [
  maxDegree 8
  minDegree 1
  meanDegree 1.7777777777777777
  density 0.2222222222222222
  graphCliqueNumber 2
  node [
    id 0
    label "szcz&#281;sna"
    origin "text"
  ]
  node [
    id 1
    label "kupka"
    origin "text"
  ]
  node [
    id 2
    label "balas"
  ]
  node [
    id 3
    label "stool"
  ]
  node [
    id 4
    label "g&#243;wno"
  ]
  node [
    id 5
    label "koprofilia"
  ]
  node [
    id 6
    label "wydalina"
  ]
  node [
    id 7
    label "fekalia"
  ]
  node [
    id 8
    label "odchody"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
]
