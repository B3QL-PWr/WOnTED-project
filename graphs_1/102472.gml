graph [
  maxDegree 23
  minDegree 1
  meanDegree 1.9649122807017543
  density 0.03508771929824561
  graphCliqueNumber 2
  node [
    id 0
    label "ozogalerii"
    origin "text"
  ]
  node [
    id 1
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 2
    label "zobaczy&#263;"
    origin "text"
  ]
  node [
    id 3
    label "zdj&#281;cia"
    origin "text"
  ]
  node [
    id 4
    label "wygrany"
    origin "text"
  ]
  node [
    id 5
    label "mecz"
    origin "text"
  ]
  node [
    id 6
    label "mks"
    origin "text"
  ]
  node [
    id 7
    label "bzura"
    origin "text"
  ]
  node [
    id 8
    label "sms"
    origin "text"
  ]
  node [
    id 9
    label "pzps"
    origin "text"
  ]
  node [
    id 10
    label "spa&#322;a"
    origin "text"
  ]
  node [
    id 11
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 12
    label "odby&#263;"
    origin "text"
  ]
  node [
    id 13
    label "si&#281;"
    origin "text"
  ]
  node [
    id 14
    label "dwa"
    origin "text"
  ]
  node [
    id 15
    label "tydzie&#324;"
    origin "text"
  ]
  node [
    id 16
    label "temu"
    origin "text"
  ]
  node [
    id 17
    label "ozorkowie"
    origin "text"
  ]
  node [
    id 18
    label "free"
  ]
  node [
    id 19
    label "spotka&#263;"
  ]
  node [
    id 20
    label "wyobrazi&#263;_sobie"
  ]
  node [
    id 21
    label "peek"
  ]
  node [
    id 22
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 23
    label "spoziera&#263;"
  ]
  node [
    id 24
    label "obejrze&#263;"
  ]
  node [
    id 25
    label "sp&#243;jrze&#263;"
  ]
  node [
    id 26
    label "postrzec"
  ]
  node [
    id 27
    label "spot"
  ]
  node [
    id 28
    label "go_steady"
  ]
  node [
    id 29
    label "pojrze&#263;"
  ]
  node [
    id 30
    label "popatrze&#263;"
  ]
  node [
    id 31
    label "u&#347;wiadomi&#263;_sobie"
  ]
  node [
    id 32
    label "pogl&#261;dn&#261;&#263;"
  ]
  node [
    id 33
    label "see"
  ]
  node [
    id 34
    label "zwr&#243;ci&#263;_uwag&#281;"
  ]
  node [
    id 35
    label "dostrzec"
  ]
  node [
    id 36
    label "zinterpretowa&#263;"
  ]
  node [
    id 37
    label "przekona&#263;_si&#281;"
  ]
  node [
    id 38
    label "znale&#378;&#263;"
  ]
  node [
    id 39
    label "cognizance"
  ]
  node [
    id 40
    label "zwyci&#281;ski"
  ]
  node [
    id 41
    label "obrona"
  ]
  node [
    id 42
    label "gra"
  ]
  node [
    id 43
    label "dwumecz"
  ]
  node [
    id 44
    label "game"
  ]
  node [
    id 45
    label "serw"
  ]
  node [
    id 46
    label "SMS"
  ]
  node [
    id 47
    label "pie&#324;"
  ]
  node [
    id 48
    label "reserve"
  ]
  node [
    id 49
    label "przej&#347;&#263;"
  ]
  node [
    id 50
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 51
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 52
    label "doba"
  ]
  node [
    id 53
    label "czas"
  ]
  node [
    id 54
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 55
    label "weekend"
  ]
  node [
    id 56
    label "miesi&#261;c"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 46
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 47
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 48
  ]
  edge [
    source 12
    target 49
  ]
  edge [
    source 12
    target 50
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 51
  ]
  edge [
    source 15
    target 52
  ]
  edge [
    source 15
    target 53
  ]
  edge [
    source 15
    target 54
  ]
  edge [
    source 15
    target 55
  ]
  edge [
    source 15
    target 56
  ]
  edge [
    source 16
    target 17
  ]
]
