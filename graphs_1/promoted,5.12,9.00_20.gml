graph [
  maxDegree 12
  minDegree 1
  meanDegree 1.9375
  density 0.0625
  graphCliqueNumber 2
  node [
    id 0
    label "jeden"
    origin "text"
  ]
  node [
    id 1
    label "radiow&#243;z"
    origin "text"
  ]
  node [
    id 2
    label "eskortowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "delegacja"
    origin "text"
  ]
  node [
    id 4
    label "botswana"
    origin "text"
  ]
  node [
    id 5
    label "dachowa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "kieliszek"
  ]
  node [
    id 7
    label "shot"
  ]
  node [
    id 8
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 9
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 10
    label "jaki&#347;"
  ]
  node [
    id 11
    label "jednolicie"
  ]
  node [
    id 12
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 13
    label "w&#243;dka"
  ]
  node [
    id 14
    label "ten"
  ]
  node [
    id 15
    label "ujednolicenie"
  ]
  node [
    id 16
    label "jednakowy"
  ]
  node [
    id 17
    label "samoch&#243;d"
  ]
  node [
    id 18
    label "misiow&#243;z"
  ]
  node [
    id 19
    label "convoy"
  ]
  node [
    id 20
    label "company"
  ]
  node [
    id 21
    label "dostarcza&#263;"
  ]
  node [
    id 22
    label "towarzyszy&#263;"
  ]
  node [
    id 23
    label "mission"
  ]
  node [
    id 24
    label "deputacja"
  ]
  node [
    id 25
    label "upowa&#380;nienie"
  ]
  node [
    id 26
    label "wyjazd"
  ]
  node [
    id 27
    label "przeniesienie"
  ]
  node [
    id 28
    label "za&#347;wiadczenie"
  ]
  node [
    id 29
    label "reprezentacja"
  ]
  node [
    id 30
    label "przewraca&#263;_si&#281;"
  ]
  node [
    id 31
    label "przewr&#243;ci&#263;_si&#281;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
]
