graph [
  maxDegree 40
  minDegree 1
  meanDegree 2.0961538461538463
  density 0.010126347082868822
  graphCliqueNumber 3
  node [
    id 0
    label "cho&#263;"
    origin "text"
  ]
  node [
    id 1
    label "zazwyczaj"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "bardzo"
    origin "text"
  ]
  node [
    id 4
    label "ostro&#380;ny"
    origin "text"
  ]
  node [
    id 5
    label "klika&#263;"
    origin "text"
  ]
  node [
    id 6
    label "wszystko"
    origin "text"
  ]
  node [
    id 7
    label "popa&#347;&#263;"
    origin "text"
  ]
  node [
    id 8
    label "tym"
    origin "text"
  ]
  node [
    id 9
    label "razem"
    origin "text"
  ]
  node [
    id 10
    label "zachowa&#263;by&#263;"
    origin "text"
  ]
  node [
    id 11
    label "czujno&#347;&#263;"
    origin "text"
  ]
  node [
    id 12
    label "rewolucyjny"
    origin "text"
  ]
  node [
    id 13
    label "klikn&#261;&#263;by&#263;"
    origin "text"
  ]
  node [
    id 14
    label "url"
    origin "text"
  ]
  node [
    id 15
    label "wiadomo&#347;ci"
    origin "text"
  ]
  node [
    id 16
    label "odruchowo"
    origin "text"
  ]
  node [
    id 17
    label "bez"
    origin "text"
  ]
  node [
    id 18
    label "zastanowienie"
    origin "text"
  ]
  node [
    id 19
    label "tylko"
    origin "text"
  ]
  node [
    id 20
    label "dzi&#281;ki"
    origin "text"
  ]
  node [
    id 21
    label "firefoxowi"
    origin "text"
  ]
  node [
    id 22
    label "wej&#347;&#263;by&#263;"
    origin "text"
  ]
  node [
    id 23
    label "strona"
    origin "text"
  ]
  node [
    id 24
    label "usi&#322;owa&#263;"
    origin "text"
  ]
  node [
    id 25
    label "si&#281;"
    origin "text"
  ]
  node [
    id 26
    label "otworzy&#263;"
    origin "text"
  ]
  node [
    id 27
    label "przegl&#261;darka"
    origin "text"
  ]
  node [
    id 28
    label "zidentyfikowa&#263;"
    origin "text"
  ]
  node [
    id 29
    label "jako"
    origin "text"
  ]
  node [
    id 30
    label "niebezpieczny"
    origin "text"
  ]
  node [
    id 31
    label "sprawdzi&#263;by&#263;"
    origin "text"
  ]
  node [
    id 32
    label "dok&#322;adnie"
    origin "text"
  ]
  node [
    id 33
    label "adres"
    origin "text"
  ]
  node [
    id 34
    label "pod"
    origin "text"
  ]
  node [
    id 35
    label "linek"
    origin "text"
  ]
  node [
    id 36
    label "ukry&#263;"
    origin "text"
  ]
  node [
    id 37
    label "nim"
    origin "text"
  ]
  node [
    id 38
    label "fragment"
    origin "text"
  ]
  node [
    id 39
    label "secure"
    origin "text"
  ]
  node [
    id 40
    label "poza"
    origin "text"
  ]
  node [
    id 41
    label "podejrzana"
    origin "text"
  ]
  node [
    id 42
    label "przekierowa&#263;"
    origin "text"
  ]
  node [
    id 43
    label "autentyczny"
    origin "text"
  ]
  node [
    id 44
    label "zawiera&#263;"
    origin "text"
  ]
  node [
    id 45
    label "my&#347;lnik"
    origin "text"
  ]
  node [
    id 46
    label "kropka"
    origin "text"
  ]
  node [
    id 47
    label "zwykle"
  ]
  node [
    id 48
    label "si&#281;ga&#263;"
  ]
  node [
    id 49
    label "trwa&#263;"
  ]
  node [
    id 50
    label "obecno&#347;&#263;"
  ]
  node [
    id 51
    label "stan"
  ]
  node [
    id 52
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 53
    label "stand"
  ]
  node [
    id 54
    label "mie&#263;_miejsce"
  ]
  node [
    id 55
    label "uczestniczy&#263;"
  ]
  node [
    id 56
    label "chodzi&#263;"
  ]
  node [
    id 57
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 58
    label "equal"
  ]
  node [
    id 59
    label "w_chuj"
  ]
  node [
    id 60
    label "ostro&#380;nie"
  ]
  node [
    id 61
    label "naciska&#263;"
  ]
  node [
    id 62
    label "wybiera&#263;"
  ]
  node [
    id 63
    label "buton"
  ]
  node [
    id 64
    label "lock"
  ]
  node [
    id 65
    label "absolut"
  ]
  node [
    id 66
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 67
    label "fall"
  ]
  node [
    id 68
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 69
    label "&#322;&#261;cznie"
  ]
  node [
    id 70
    label "u&#347;pienie"
  ]
  node [
    id 71
    label "uwaga"
  ]
  node [
    id 72
    label "wakefulness"
  ]
  node [
    id 73
    label "prze&#322;omowy"
  ]
  node [
    id 74
    label "rewolucyjnie"
  ]
  node [
    id 75
    label "involuntarily"
  ]
  node [
    id 76
    label "bezwiedny"
  ]
  node [
    id 77
    label "samoistnie"
  ]
  node [
    id 78
    label "nie&#347;wiadomie"
  ]
  node [
    id 79
    label "ki&#347;&#263;"
  ]
  node [
    id 80
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 81
    label "krzew"
  ]
  node [
    id 82
    label "pi&#380;maczkowate"
  ]
  node [
    id 83
    label "pestkowiec"
  ]
  node [
    id 84
    label "kwiat"
  ]
  node [
    id 85
    label "owoc"
  ]
  node [
    id 86
    label "oliwkowate"
  ]
  node [
    id 87
    label "ro&#347;lina"
  ]
  node [
    id 88
    label "hy&#263;ka"
  ]
  node [
    id 89
    label "lilac"
  ]
  node [
    id 90
    label "delfinidyna"
  ]
  node [
    id 91
    label "zastanowienie_si&#281;"
  ]
  node [
    id 92
    label "trud"
  ]
  node [
    id 93
    label "zainteresowanie"
  ]
  node [
    id 94
    label "consideration"
  ]
  node [
    id 95
    label "skr&#281;canie"
  ]
  node [
    id 96
    label "voice"
  ]
  node [
    id 97
    label "forma"
  ]
  node [
    id 98
    label "internet"
  ]
  node [
    id 99
    label "skr&#281;ci&#263;"
  ]
  node [
    id 100
    label "kartka"
  ]
  node [
    id 101
    label "orientowa&#263;"
  ]
  node [
    id 102
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 103
    label "powierzchnia"
  ]
  node [
    id 104
    label "plik"
  ]
  node [
    id 105
    label "bok"
  ]
  node [
    id 106
    label "pagina"
  ]
  node [
    id 107
    label "orientowanie"
  ]
  node [
    id 108
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 109
    label "s&#261;d"
  ]
  node [
    id 110
    label "skr&#281;ca&#263;"
  ]
  node [
    id 111
    label "g&#243;ra"
  ]
  node [
    id 112
    label "serwis_internetowy"
  ]
  node [
    id 113
    label "orientacja"
  ]
  node [
    id 114
    label "linia"
  ]
  node [
    id 115
    label "skr&#281;cenie"
  ]
  node [
    id 116
    label "layout"
  ]
  node [
    id 117
    label "zorientowa&#263;"
  ]
  node [
    id 118
    label "zorientowanie"
  ]
  node [
    id 119
    label "obiekt"
  ]
  node [
    id 120
    label "podmiot"
  ]
  node [
    id 121
    label "ty&#322;"
  ]
  node [
    id 122
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 123
    label "logowanie"
  ]
  node [
    id 124
    label "adres_internetowy"
  ]
  node [
    id 125
    label "uj&#281;cie"
  ]
  node [
    id 126
    label "prz&#243;d"
  ]
  node [
    id 127
    label "posta&#263;"
  ]
  node [
    id 128
    label "stara&#263;_si&#281;"
  ]
  node [
    id 129
    label "try"
  ]
  node [
    id 130
    label "establish"
  ]
  node [
    id 131
    label "cia&#322;o"
  ]
  node [
    id 132
    label "zacz&#261;&#263;"
  ]
  node [
    id 133
    label "spowodowa&#263;"
  ]
  node [
    id 134
    label "begin"
  ]
  node [
    id 135
    label "udost&#281;pni&#263;"
  ]
  node [
    id 136
    label "roz&#322;o&#380;y&#263;"
  ]
  node [
    id 137
    label "uruchomi&#263;"
  ]
  node [
    id 138
    label "przeci&#261;&#263;"
  ]
  node [
    id 139
    label "viewer"
  ]
  node [
    id 140
    label "przyrz&#261;d"
  ]
  node [
    id 141
    label "program"
  ]
  node [
    id 142
    label "browser"
  ]
  node [
    id 143
    label "projektor"
  ]
  node [
    id 144
    label "rozpozna&#263;"
  ]
  node [
    id 145
    label "tag"
  ]
  node [
    id 146
    label "ujednolici&#263;"
  ]
  node [
    id 147
    label "gro&#378;ny"
  ]
  node [
    id 148
    label "k&#322;opotliwy"
  ]
  node [
    id 149
    label "niebezpiecznie"
  ]
  node [
    id 150
    label "punctiliously"
  ]
  node [
    id 151
    label "dok&#322;adny"
  ]
  node [
    id 152
    label "meticulously"
  ]
  node [
    id 153
    label "precyzyjnie"
  ]
  node [
    id 154
    label "rzetelnie"
  ]
  node [
    id 155
    label "adres_elektroniczny"
  ]
  node [
    id 156
    label "domena"
  ]
  node [
    id 157
    label "po&#322;o&#380;enie"
  ]
  node [
    id 158
    label "kod_pocztowy"
  ]
  node [
    id 159
    label "dane"
  ]
  node [
    id 160
    label "pismo"
  ]
  node [
    id 161
    label "przesy&#322;ka"
  ]
  node [
    id 162
    label "personalia"
  ]
  node [
    id 163
    label "siedziba"
  ]
  node [
    id 164
    label "dziedzina"
  ]
  node [
    id 165
    label "zachowa&#263;"
  ]
  node [
    id 166
    label "ensconce"
  ]
  node [
    id 167
    label "hide"
  ]
  node [
    id 168
    label "zmie&#347;ci&#263;"
  ]
  node [
    id 169
    label "umie&#347;ci&#263;"
  ]
  node [
    id 170
    label "przytai&#263;"
  ]
  node [
    id 171
    label "gra_planszowa"
  ]
  node [
    id 172
    label "utw&#243;r"
  ]
  node [
    id 173
    label "mode"
  ]
  node [
    id 174
    label "gra"
  ]
  node [
    id 175
    label "przesada"
  ]
  node [
    id 176
    label "ustawienie"
  ]
  node [
    id 177
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 178
    label "zmieni&#263;"
  ]
  node [
    id 179
    label "skierowa&#263;"
  ]
  node [
    id 180
    label "szczery"
  ]
  node [
    id 181
    label "istny"
  ]
  node [
    id 182
    label "wyj&#261;tkowy"
  ]
  node [
    id 183
    label "naturalny"
  ]
  node [
    id 184
    label "prawdziwie"
  ]
  node [
    id 185
    label "prawdziwy"
  ]
  node [
    id 186
    label "autentycznie"
  ]
  node [
    id 187
    label "&#380;ywny"
  ]
  node [
    id 188
    label "obejmowa&#263;"
  ]
  node [
    id 189
    label "mie&#263;"
  ]
  node [
    id 190
    label "zamyka&#263;"
  ]
  node [
    id 191
    label "poznawa&#263;"
  ]
  node [
    id 192
    label "fold"
  ]
  node [
    id 193
    label "make"
  ]
  node [
    id 194
    label "ustala&#263;"
  ]
  node [
    id 195
    label "znak_interpunkcyjny"
  ]
  node [
    id 196
    label "kreska"
  ]
  node [
    id 197
    label "plamka"
  ]
  node [
    id 198
    label "znak_muzyczny"
  ]
  node [
    id 199
    label "wielokropek"
  ]
  node [
    id 200
    label "alfabet_Morse'a"
  ]
  node [
    id 201
    label "period"
  ]
  node [
    id 202
    label "mark"
  ]
  node [
    id 203
    label "znak_diakrytyczny"
  ]
  node [
    id 204
    label "punkt"
  ]
  node [
    id 205
    label "znak_matematyczny"
  ]
  node [
    id 206
    label "znak_pisarski"
  ]
  node [
    id 207
    label "point"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 70
  ]
  edge [
    source 11
    target 71
  ]
  edge [
    source 11
    target 72
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 73
  ]
  edge [
    source 12
    target 74
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 75
  ]
  edge [
    source 16
    target 76
  ]
  edge [
    source 16
    target 77
  ]
  edge [
    source 16
    target 78
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 79
  ]
  edge [
    source 17
    target 80
  ]
  edge [
    source 17
    target 81
  ]
  edge [
    source 17
    target 82
  ]
  edge [
    source 17
    target 83
  ]
  edge [
    source 17
    target 84
  ]
  edge [
    source 17
    target 85
  ]
  edge [
    source 17
    target 86
  ]
  edge [
    source 17
    target 87
  ]
  edge [
    source 17
    target 88
  ]
  edge [
    source 17
    target 89
  ]
  edge [
    source 17
    target 90
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 91
  ]
  edge [
    source 18
    target 92
  ]
  edge [
    source 18
    target 93
  ]
  edge [
    source 18
    target 94
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 45
  ]
  edge [
    source 19
    target 46
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 28
  ]
  edge [
    source 23
    target 29
  ]
  edge [
    source 23
    target 95
  ]
  edge [
    source 23
    target 96
  ]
  edge [
    source 23
    target 97
  ]
  edge [
    source 23
    target 98
  ]
  edge [
    source 23
    target 99
  ]
  edge [
    source 23
    target 100
  ]
  edge [
    source 23
    target 101
  ]
  edge [
    source 23
    target 102
  ]
  edge [
    source 23
    target 103
  ]
  edge [
    source 23
    target 104
  ]
  edge [
    source 23
    target 105
  ]
  edge [
    source 23
    target 106
  ]
  edge [
    source 23
    target 107
  ]
  edge [
    source 23
    target 38
  ]
  edge [
    source 23
    target 108
  ]
  edge [
    source 23
    target 109
  ]
  edge [
    source 23
    target 110
  ]
  edge [
    source 23
    target 111
  ]
  edge [
    source 23
    target 112
  ]
  edge [
    source 23
    target 113
  ]
  edge [
    source 23
    target 114
  ]
  edge [
    source 23
    target 115
  ]
  edge [
    source 23
    target 116
  ]
  edge [
    source 23
    target 117
  ]
  edge [
    source 23
    target 118
  ]
  edge [
    source 23
    target 119
  ]
  edge [
    source 23
    target 120
  ]
  edge [
    source 23
    target 121
  ]
  edge [
    source 23
    target 122
  ]
  edge [
    source 23
    target 123
  ]
  edge [
    source 23
    target 124
  ]
  edge [
    source 23
    target 125
  ]
  edge [
    source 23
    target 126
  ]
  edge [
    source 23
    target 127
  ]
  edge [
    source 23
    target 33
  ]
  edge [
    source 23
    target 36
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 128
  ]
  edge [
    source 24
    target 129
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 130
  ]
  edge [
    source 26
    target 131
  ]
  edge [
    source 26
    target 132
  ]
  edge [
    source 26
    target 133
  ]
  edge [
    source 26
    target 134
  ]
  edge [
    source 26
    target 135
  ]
  edge [
    source 26
    target 136
  ]
  edge [
    source 26
    target 137
  ]
  edge [
    source 26
    target 138
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 139
  ]
  edge [
    source 27
    target 140
  ]
  edge [
    source 27
    target 141
  ]
  edge [
    source 27
    target 142
  ]
  edge [
    source 27
    target 143
  ]
  edge [
    source 28
    target 144
  ]
  edge [
    source 28
    target 145
  ]
  edge [
    source 28
    target 146
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 147
  ]
  edge [
    source 30
    target 148
  ]
  edge [
    source 30
    target 149
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 150
  ]
  edge [
    source 32
    target 151
  ]
  edge [
    source 32
    target 152
  ]
  edge [
    source 32
    target 153
  ]
  edge [
    source 32
    target 154
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 43
  ]
  edge [
    source 33
    target 44
  ]
  edge [
    source 33
    target 155
  ]
  edge [
    source 33
    target 156
  ]
  edge [
    source 33
    target 157
  ]
  edge [
    source 33
    target 158
  ]
  edge [
    source 33
    target 159
  ]
  edge [
    source 33
    target 160
  ]
  edge [
    source 33
    target 161
  ]
  edge [
    source 33
    target 162
  ]
  edge [
    source 33
    target 163
  ]
  edge [
    source 33
    target 164
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 165
  ]
  edge [
    source 36
    target 166
  ]
  edge [
    source 36
    target 167
  ]
  edge [
    source 36
    target 168
  ]
  edge [
    source 36
    target 169
  ]
  edge [
    source 36
    target 170
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 171
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 122
  ]
  edge [
    source 38
    target 172
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 46
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 173
  ]
  edge [
    source 40
    target 174
  ]
  edge [
    source 40
    target 175
  ]
  edge [
    source 40
    target 176
  ]
  edge [
    source 40
    target 108
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 177
  ]
  edge [
    source 42
    target 178
  ]
  edge [
    source 42
    target 179
  ]
  edge [
    source 43
    target 180
  ]
  edge [
    source 43
    target 181
  ]
  edge [
    source 43
    target 182
  ]
  edge [
    source 43
    target 183
  ]
  edge [
    source 43
    target 184
  ]
  edge [
    source 43
    target 185
  ]
  edge [
    source 43
    target 186
  ]
  edge [
    source 43
    target 187
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 188
  ]
  edge [
    source 44
    target 189
  ]
  edge [
    source 44
    target 190
  ]
  edge [
    source 44
    target 64
  ]
  edge [
    source 44
    target 191
  ]
  edge [
    source 44
    target 192
  ]
  edge [
    source 44
    target 193
  ]
  edge [
    source 44
    target 194
  ]
  edge [
    source 45
    target 195
  ]
  edge [
    source 45
    target 196
  ]
  edge [
    source 46
    target 195
  ]
  edge [
    source 46
    target 197
  ]
  edge [
    source 46
    target 198
  ]
  edge [
    source 46
    target 199
  ]
  edge [
    source 46
    target 200
  ]
  edge [
    source 46
    target 201
  ]
  edge [
    source 46
    target 202
  ]
  edge [
    source 46
    target 203
  ]
  edge [
    source 46
    target 204
  ]
  edge [
    source 46
    target 205
  ]
  edge [
    source 46
    target 206
  ]
  edge [
    source 46
    target 207
  ]
]
