graph [
  maxDegree 56
  minDegree 1
  meanDegree 2.3504424778761064
  density 0.004167451201907989
  graphCliqueNumber 7
  node [
    id 0
    label "pan"
    origin "text"
  ]
  node [
    id 1
    label "marsza&#322;ek"
    origin "text"
  ]
  node [
    id 2
    label "wysoki"
    origin "text"
  ]
  node [
    id 3
    label "izba"
    origin "text"
  ]
  node [
    id 4
    label "minister"
    origin "text"
  ]
  node [
    id 5
    label "uczy&#263;"
    origin "text"
  ]
  node [
    id 6
    label "kiedy&#347;"
    origin "text"
  ]
  node [
    id 7
    label "dawno"
    origin "text"
  ]
  node [
    id 8
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 9
    label "warto"
    origin "text"
  ]
  node [
    id 10
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 11
    label "dyskusja"
    origin "text"
  ]
  node [
    id 12
    label "rozpoczyna&#263;"
    origin "text"
  ]
  node [
    id 13
    label "terminologiczny"
    origin "text"
  ]
  node [
    id 14
    label "uporz&#261;dkowa&#263;"
    origin "text"
  ]
  node [
    id 15
    label "poj&#281;cie"
    origin "text"
  ]
  node [
    id 16
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 17
    label "rozmawia&#263;"
    origin "text"
  ]
  node [
    id 18
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 19
    label "tym"
    origin "text"
  ]
  node [
    id 20
    label "te&#380;"
    origin "text"
  ]
  node [
    id 21
    label "dlatego"
    origin "text"
  ]
  node [
    id 22
    label "podczas"
    origin "text"
  ]
  node [
    id 23
    label "nad"
    origin "text"
  ]
  node [
    id 24
    label "sprawozdanie"
    origin "text"
  ]
  node [
    id 25
    label "krajowy"
    origin "text"
  ]
  node [
    id 26
    label "rad"
    origin "text"
  ]
  node [
    id 27
    label "radiofonia"
    origin "text"
  ]
  node [
    id 28
    label "telewizja"
    origin "text"
  ]
  node [
    id 29
    label "przekona&#263;by&#263;"
    origin "text"
  ]
  node [
    id 30
    label "si&#281;"
    origin "text"
  ]
  node [
    id 31
    label "istnie&#263;"
    origin "text"
  ]
  node [
    id 32
    label "ten"
    origin "text"
  ]
  node [
    id 33
    label "materia"
    origin "text"
  ]
  node [
    id 34
    label "pewien"
    origin "text"
  ]
  node [
    id 35
    label "ba&#322;agan"
    origin "text"
  ]
  node [
    id 36
    label "by&#263;"
    origin "text"
  ]
  node [
    id 37
    label "chocia&#380;by"
    origin "text"
  ]
  node [
    id 38
    label "fakt"
    origin "text"
  ]
  node [
    id 39
    label "rada"
    origin "text"
  ]
  node [
    id 40
    label "swoje"
    origin "text"
  ]
  node [
    id 41
    label "domaga&#322;a"
    origin "text"
  ]
  node [
    id 42
    label "doprecyzowa&#263;"
    origin "text"
  ]
  node [
    id 43
    label "przyk&#322;ad"
    origin "text"
  ]
  node [
    id 44
    label "kategoria"
    origin "text"
  ]
  node [
    id 45
    label "rozrywka"
    origin "text"
  ]
  node [
    id 46
    label "chcie&#263;byby&#263;"
    origin "text"
  ]
  node [
    id 47
    label "wi&#281;c"
    origin "text"
  ]
  node [
    id 48
    label "zaproponowa&#263;"
    origin "text"
  ]
  node [
    id 49
    label "zwraca&#263;"
    origin "text"
  ]
  node [
    id 50
    label "rozpocz&#281;cie"
    origin "text"
  ]
  node [
    id 51
    label "ca&#322;y"
    origin "text"
  ]
  node [
    id 52
    label "kultura"
    origin "text"
  ]
  node [
    id 53
    label "wiedzie&#263;"
    origin "text"
  ]
  node [
    id 54
    label "o&#347;rodek"
    origin "text"
  ]
  node [
    id 55
    label "wroc&#322;awskie"
    origin "text"
  ]
  node [
    id 56
    label "pozna&#324;skie"
    origin "text"
  ]
  node [
    id 57
    label "ale"
    origin "text"
  ]
  node [
    id 58
    label "studia"
    origin "text"
  ]
  node [
    id 59
    label "wbi&#263;"
    origin "text"
  ]
  node [
    id 60
    label "g&#322;owa"
    origin "text"
  ]
  node [
    id 61
    label "definicja"
    origin "text"
  ]
  node [
    id 62
    label "zesp&#243;&#322;"
    origin "text"
  ]
  node [
    id 63
    label "forma"
    origin "text"
  ]
  node [
    id 64
    label "&#347;wiadomo&#347;&#263;"
    origin "text"
  ]
  node [
    id 65
    label "spo&#322;eczny"
    origin "text"
  ]
  node [
    id 66
    label "obs&#322;ugiwa&#263;"
    origin "text"
  ]
  node [
    id 67
    label "odpowiada&#263;"
    origin "text"
  ]
  node [
    id 68
    label "typ"
    origin "text"
  ]
  node [
    id 69
    label "praktyk"
    origin "text"
  ]
  node [
    id 70
    label "skomplikowany"
    origin "text"
  ]
  node [
    id 71
    label "r&#243;wnie&#380;"
    origin "text"
  ]
  node [
    id 72
    label "pow&#243;d"
    origin "text"
  ]
  node [
    id 73
    label "dzwonek"
    origin "text"
  ]
  node [
    id 74
    label "obejmowa&#263;"
    origin "text"
  ]
  node [
    id 75
    label "cho&#263;by"
    origin "text"
  ]
  node [
    id 76
    label "cz&#322;owiek"
  ]
  node [
    id 77
    label "profesor"
  ]
  node [
    id 78
    label "kszta&#322;ciciel"
  ]
  node [
    id 79
    label "jegomo&#347;&#263;"
  ]
  node [
    id 80
    label "zwrot"
  ]
  node [
    id 81
    label "pracodawca"
  ]
  node [
    id 82
    label "rz&#261;dzenie"
  ]
  node [
    id 83
    label "m&#261;&#380;"
  ]
  node [
    id 84
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 85
    label "ch&#322;opina"
  ]
  node [
    id 86
    label "bratek"
  ]
  node [
    id 87
    label "opiekun"
  ]
  node [
    id 88
    label "doros&#322;y"
  ]
  node [
    id 89
    label "preceptor"
  ]
  node [
    id 90
    label "Midas"
  ]
  node [
    id 91
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 92
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 93
    label "murza"
  ]
  node [
    id 94
    label "ojciec"
  ]
  node [
    id 95
    label "androlog"
  ]
  node [
    id 96
    label "pupil"
  ]
  node [
    id 97
    label "efendi"
  ]
  node [
    id 98
    label "nabab"
  ]
  node [
    id 99
    label "w&#322;odarz"
  ]
  node [
    id 100
    label "szkolnik"
  ]
  node [
    id 101
    label "pedagog"
  ]
  node [
    id 102
    label "popularyzator"
  ]
  node [
    id 103
    label "andropauza"
  ]
  node [
    id 104
    label "gra_w_karty"
  ]
  node [
    id 105
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 106
    label "Mieszko_I"
  ]
  node [
    id 107
    label "bogaty"
  ]
  node [
    id 108
    label "samiec"
  ]
  node [
    id 109
    label "przyw&#243;dca"
  ]
  node [
    id 110
    label "pa&#324;stwo"
  ]
  node [
    id 111
    label "belfer"
  ]
  node [
    id 112
    label "stopie&#324;_wojskowy"
  ]
  node [
    id 113
    label "dostojnik"
  ]
  node [
    id 114
    label "oficer"
  ]
  node [
    id 115
    label "parlamentarzysta"
  ]
  node [
    id 116
    label "Pi&#322;sudski"
  ]
  node [
    id 117
    label "warto&#347;ciowy"
  ]
  node [
    id 118
    label "du&#380;y"
  ]
  node [
    id 119
    label "wysoce"
  ]
  node [
    id 120
    label "daleki"
  ]
  node [
    id 121
    label "znaczny"
  ]
  node [
    id 122
    label "wysoko"
  ]
  node [
    id 123
    label "szczytnie"
  ]
  node [
    id 124
    label "wznios&#322;y"
  ]
  node [
    id 125
    label "wyrafinowany"
  ]
  node [
    id 126
    label "z_wysoka"
  ]
  node [
    id 127
    label "chwalebny"
  ]
  node [
    id 128
    label "uprzywilejowany"
  ]
  node [
    id 129
    label "niepo&#347;ledni"
  ]
  node [
    id 130
    label "pok&#243;j"
  ]
  node [
    id 131
    label "parlament"
  ]
  node [
    id 132
    label "zwi&#261;zek"
  ]
  node [
    id 133
    label "NIK"
  ]
  node [
    id 134
    label "urz&#261;d"
  ]
  node [
    id 135
    label "organ"
  ]
  node [
    id 136
    label "pomieszczenie"
  ]
  node [
    id 137
    label "Goebbels"
  ]
  node [
    id 138
    label "Sto&#322;ypin"
  ]
  node [
    id 139
    label "rz&#261;d"
  ]
  node [
    id 140
    label "zapoznawa&#263;"
  ]
  node [
    id 141
    label "teach"
  ]
  node [
    id 142
    label "train"
  ]
  node [
    id 143
    label "rozwija&#263;"
  ]
  node [
    id 144
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 145
    label "pracowa&#263;"
  ]
  node [
    id 146
    label "szkoli&#263;"
  ]
  node [
    id 147
    label "dawny"
  ]
  node [
    id 148
    label "ongi&#347;"
  ]
  node [
    id 149
    label "dawnie"
  ]
  node [
    id 150
    label "wcze&#347;niej"
  ]
  node [
    id 151
    label "d&#322;ugotrwale"
  ]
  node [
    id 152
    label "przysparza&#263;"
  ]
  node [
    id 153
    label "give"
  ]
  node [
    id 154
    label "kali&#263;_si&#281;"
  ]
  node [
    id 155
    label "bonanza"
  ]
  node [
    id 156
    label "jaki&#347;"
  ]
  node [
    id 157
    label "rozmowa"
  ]
  node [
    id 158
    label "sympozjon"
  ]
  node [
    id 159
    label "conference"
  ]
  node [
    id 160
    label "start"
  ]
  node [
    id 161
    label "robi&#263;_pierwszy_krok"
  ]
  node [
    id 162
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 163
    label "begin"
  ]
  node [
    id 164
    label "sprawowa&#263;"
  ]
  node [
    id 165
    label "zorganizowa&#263;"
  ]
  node [
    id 166
    label "zadba&#263;"
  ]
  node [
    id 167
    label "order"
  ]
  node [
    id 168
    label "ustawi&#263;"
  ]
  node [
    id 169
    label "zebra&#263;"
  ]
  node [
    id 170
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 171
    label "posprz&#261;ta&#263;"
  ]
  node [
    id 172
    label "skumanie"
  ]
  node [
    id 173
    label "zorientowanie"
  ]
  node [
    id 174
    label "wytw&#243;r"
  ]
  node [
    id 175
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 176
    label "clasp"
  ]
  node [
    id 177
    label "teoria"
  ]
  node [
    id 178
    label "pos&#322;uchanie"
  ]
  node [
    id 179
    label "orientacja"
  ]
  node [
    id 180
    label "przem&#243;wienie"
  ]
  node [
    id 181
    label "talk"
  ]
  node [
    id 182
    label "gaworzy&#263;"
  ]
  node [
    id 183
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 184
    label "remark"
  ]
  node [
    id 185
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 186
    label "u&#380;ywa&#263;"
  ]
  node [
    id 187
    label "okre&#347;la&#263;"
  ]
  node [
    id 188
    label "j&#281;zyk"
  ]
  node [
    id 189
    label "say"
  ]
  node [
    id 190
    label "formu&#322;owa&#263;"
  ]
  node [
    id 191
    label "powiada&#263;"
  ]
  node [
    id 192
    label "informowa&#263;"
  ]
  node [
    id 193
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 194
    label "wydobywa&#263;"
  ]
  node [
    id 195
    label "express"
  ]
  node [
    id 196
    label "chew_the_fat"
  ]
  node [
    id 197
    label "dysfonia"
  ]
  node [
    id 198
    label "umie&#263;"
  ]
  node [
    id 199
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 200
    label "tell"
  ]
  node [
    id 201
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 202
    label "wyra&#380;a&#263;"
  ]
  node [
    id 203
    label "dziama&#263;"
  ]
  node [
    id 204
    label "prawi&#263;"
  ]
  node [
    id 205
    label "message"
  ]
  node [
    id 206
    label "korespondent"
  ]
  node [
    id 207
    label "wypowied&#378;"
  ]
  node [
    id 208
    label "sprawko"
  ]
  node [
    id 209
    label "rodzimy"
  ]
  node [
    id 210
    label "berylowiec"
  ]
  node [
    id 211
    label "u&#347;miechni&#281;ty"
  ]
  node [
    id 212
    label "pierwiastek_promieniotw&#243;rczy"
  ]
  node [
    id 213
    label "mikroradian"
  ]
  node [
    id 214
    label "zadowolenie_si&#281;"
  ]
  node [
    id 215
    label "usatysfakcjonowanie_si&#281;"
  ]
  node [
    id 216
    label "content"
  ]
  node [
    id 217
    label "jednostka_promieniowania"
  ]
  node [
    id 218
    label "miliradian"
  ]
  node [
    id 219
    label "jednostka"
  ]
  node [
    id 220
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 221
    label "radio"
  ]
  node [
    id 222
    label "radiofonizacja"
  ]
  node [
    id 223
    label "infrastruktura"
  ]
  node [
    id 224
    label "radiokomunikacja"
  ]
  node [
    id 225
    label "Polsat"
  ]
  node [
    id 226
    label "paj&#281;czarz"
  ]
  node [
    id 227
    label "gadaj&#261;ca_g&#322;owa"
  ]
  node [
    id 228
    label "programowiec"
  ]
  node [
    id 229
    label "technologia"
  ]
  node [
    id 230
    label "urz&#261;dzenie_RTV"
  ]
  node [
    id 231
    label "Interwizja"
  ]
  node [
    id 232
    label "BBC"
  ]
  node [
    id 233
    label "ekran"
  ]
  node [
    id 234
    label "redakcja"
  ]
  node [
    id 235
    label "media"
  ]
  node [
    id 236
    label "odbieranie"
  ]
  node [
    id 237
    label "odbiera&#263;"
  ]
  node [
    id 238
    label "odbiornik"
  ]
  node [
    id 239
    label "instytucja"
  ]
  node [
    id 240
    label "du&#380;y_pok&#243;j"
  ]
  node [
    id 241
    label "studio"
  ]
  node [
    id 242
    label "telekomunikacja"
  ]
  node [
    id 243
    label "muza"
  ]
  node [
    id 244
    label "stand"
  ]
  node [
    id 245
    label "okre&#347;lony"
  ]
  node [
    id 246
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 247
    label "byt"
  ]
  node [
    id 248
    label "temat"
  ]
  node [
    id 249
    label "materia&#322;"
  ]
  node [
    id 250
    label "szczeg&#243;&#322;"
  ]
  node [
    id 251
    label "informacja"
  ]
  node [
    id 252
    label "rzecz"
  ]
  node [
    id 253
    label "ropa"
  ]
  node [
    id 254
    label "dualizm_falowo-korpuskularny"
  ]
  node [
    id 255
    label "upewnienie_si&#281;"
  ]
  node [
    id 256
    label "wierzenie"
  ]
  node [
    id 257
    label "mo&#380;liwy"
  ]
  node [
    id 258
    label "ufanie"
  ]
  node [
    id 259
    label "spokojny"
  ]
  node [
    id 260
    label "upewnianie_si&#281;"
  ]
  node [
    id 261
    label "nieporz&#261;dek"
  ]
  node [
    id 262
    label "rowdiness"
  ]
  node [
    id 263
    label "kipisz"
  ]
  node [
    id 264
    label "meksyk"
  ]
  node [
    id 265
    label "si&#281;ga&#263;"
  ]
  node [
    id 266
    label "trwa&#263;"
  ]
  node [
    id 267
    label "obecno&#347;&#263;"
  ]
  node [
    id 268
    label "stan"
  ]
  node [
    id 269
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 270
    label "mie&#263;_miejsce"
  ]
  node [
    id 271
    label "uczestniczy&#263;"
  ]
  node [
    id 272
    label "chodzi&#263;"
  ]
  node [
    id 273
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 274
    label "equal"
  ]
  node [
    id 275
    label "wydarzenie"
  ]
  node [
    id 276
    label "bia&#322;e_plamy"
  ]
  node [
    id 277
    label "grupa"
  ]
  node [
    id 278
    label "Rada_Unii_Europejskiej"
  ]
  node [
    id 279
    label "zgromadzenie"
  ]
  node [
    id 280
    label "wskaz&#243;wka"
  ]
  node [
    id 281
    label "konsylium"
  ]
  node [
    id 282
    label "Rada_Europy"
  ]
  node [
    id 283
    label "Rada_Europejska"
  ]
  node [
    id 284
    label "posiedzenie"
  ]
  node [
    id 285
    label "sprecyzowa&#263;"
  ]
  node [
    id 286
    label "czyn"
  ]
  node [
    id 287
    label "przedstawiciel"
  ]
  node [
    id 288
    label "ilustracja"
  ]
  node [
    id 289
    label "type"
  ]
  node [
    id 290
    label "zbi&#243;r"
  ]
  node [
    id 291
    label "klasa"
  ]
  node [
    id 292
    label "czasoumilacz"
  ]
  node [
    id 293
    label "game"
  ]
  node [
    id 294
    label "odpoczynek"
  ]
  node [
    id 295
    label "volunteer"
  ]
  node [
    id 296
    label "announce"
  ]
  node [
    id 297
    label "zach&#281;ci&#263;"
  ]
  node [
    id 298
    label "indicate"
  ]
  node [
    id 299
    label "kandydatura"
  ]
  node [
    id 300
    label "poinformowa&#263;"
  ]
  node [
    id 301
    label "ustawia&#263;"
  ]
  node [
    id 302
    label "wydala&#263;"
  ]
  node [
    id 303
    label "manipulate"
  ]
  node [
    id 304
    label "przeznacza&#263;"
  ]
  node [
    id 305
    label "haftowa&#263;"
  ]
  node [
    id 306
    label "przekazywa&#263;"
  ]
  node [
    id 307
    label "dzia&#322;anie"
  ]
  node [
    id 308
    label "zrobienie"
  ]
  node [
    id 309
    label "znalezienie_si&#281;"
  ]
  node [
    id 310
    label "zrobienie_pierwszego_kroku"
  ]
  node [
    id 311
    label "zacz&#281;cie"
  ]
  node [
    id 312
    label "pocz&#261;tek"
  ]
  node [
    id 313
    label "opening"
  ]
  node [
    id 314
    label "jedyny"
  ]
  node [
    id 315
    label "kompletny"
  ]
  node [
    id 316
    label "zdr&#243;w"
  ]
  node [
    id 317
    label "&#380;ywy"
  ]
  node [
    id 318
    label "ca&#322;o"
  ]
  node [
    id 319
    label "pe&#322;ny"
  ]
  node [
    id 320
    label "calu&#347;ko"
  ]
  node [
    id 321
    label "podobny"
  ]
  node [
    id 322
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 323
    label "przedmiot"
  ]
  node [
    id 324
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 325
    label "Wsch&#243;d"
  ]
  node [
    id 326
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 327
    label "sztuka"
  ]
  node [
    id 328
    label "religia"
  ]
  node [
    id 329
    label "przejmowa&#263;"
  ]
  node [
    id 330
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 331
    label "makrokosmos"
  ]
  node [
    id 332
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 333
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 334
    label "zjawisko"
  ]
  node [
    id 335
    label "praca_rolnicza"
  ]
  node [
    id 336
    label "tradycja"
  ]
  node [
    id 337
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 338
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 339
    label "przejmowanie"
  ]
  node [
    id 340
    label "cecha"
  ]
  node [
    id 341
    label "asymilowanie_si&#281;"
  ]
  node [
    id 342
    label "przej&#261;&#263;"
  ]
  node [
    id 343
    label "hodowla"
  ]
  node [
    id 344
    label "brzoskwiniarnia"
  ]
  node [
    id 345
    label "populace"
  ]
  node [
    id 346
    label "konwencja"
  ]
  node [
    id 347
    label "propriety"
  ]
  node [
    id 348
    label "jako&#347;&#263;"
  ]
  node [
    id 349
    label "kuchnia"
  ]
  node [
    id 350
    label "zwyczaj"
  ]
  node [
    id 351
    label "przej&#281;cie"
  ]
  node [
    id 352
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 353
    label "cognizance"
  ]
  node [
    id 354
    label "miejsce"
  ]
  node [
    id 355
    label "Hollywood"
  ]
  node [
    id 356
    label "zal&#261;&#380;ek"
  ]
  node [
    id 357
    label "otoczenie"
  ]
  node [
    id 358
    label "o&#347;rodkowy_uk&#322;ad_nerwowy"
  ]
  node [
    id 359
    label "&#347;rodek"
  ]
  node [
    id 360
    label "center"
  ]
  node [
    id 361
    label "skupisko"
  ]
  node [
    id 362
    label "warunki"
  ]
  node [
    id 363
    label "piwo"
  ]
  node [
    id 364
    label "badanie"
  ]
  node [
    id 365
    label "nauka"
  ]
  node [
    id 366
    label "przyswoi&#263;"
  ]
  node [
    id 367
    label "insert"
  ]
  node [
    id 368
    label "przybi&#263;"
  ]
  node [
    id 369
    label "doda&#263;"
  ]
  node [
    id 370
    label "zag&#322;&#281;bi&#263;"
  ]
  node [
    id 371
    label "cofn&#261;&#263;"
  ]
  node [
    id 372
    label "wt&#322;oczy&#263;"
  ]
  node [
    id 373
    label "wprowadzi&#263;"
  ]
  node [
    id 374
    label "set"
  ]
  node [
    id 375
    label "da&#263;"
  ]
  node [
    id 376
    label "wla&#263;"
  ]
  node [
    id 377
    label "skuli&#263;"
  ]
  node [
    id 378
    label "wmurowa&#263;"
  ]
  node [
    id 379
    label "umie&#347;ci&#263;"
  ]
  node [
    id 380
    label "zdoby&#263;"
  ]
  node [
    id 381
    label "zwr&#243;ci&#263;"
  ]
  node [
    id 382
    label "wprawi&#263;"
  ]
  node [
    id 383
    label "nasadzi&#263;"
  ]
  node [
    id 384
    label "wrzuci&#263;"
  ]
  node [
    id 385
    label "przyj&#347;&#263;"
  ]
  node [
    id 386
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 387
    label "ustosunkowa&#263;_si&#281;"
  ]
  node [
    id 388
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 389
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 390
    label "ucho"
  ]
  node [
    id 391
    label "makrocefalia"
  ]
  node [
    id 392
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 393
    label "m&#243;zg"
  ]
  node [
    id 394
    label "kierownictwo"
  ]
  node [
    id 395
    label "czaszka"
  ]
  node [
    id 396
    label "dekiel"
  ]
  node [
    id 397
    label "umys&#322;"
  ]
  node [
    id 398
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 399
    label "&#347;ci&#281;cie"
  ]
  node [
    id 400
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 401
    label "g&#243;ra"
  ]
  node [
    id 402
    label "byd&#322;o"
  ]
  node [
    id 403
    label "alkohol"
  ]
  node [
    id 404
    label "wiedza"
  ]
  node [
    id 405
    label "ro&#347;lina"
  ]
  node [
    id 406
    label "&#347;ci&#281;gno"
  ]
  node [
    id 407
    label "&#380;ycie"
  ]
  node [
    id 408
    label "pryncypa&#322;"
  ]
  node [
    id 409
    label "fryzura"
  ]
  node [
    id 410
    label "noosfera"
  ]
  node [
    id 411
    label "kierowa&#263;"
  ]
  node [
    id 412
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 413
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 414
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 415
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 416
    label "zdolno&#347;&#263;"
  ]
  node [
    id 417
    label "kszta&#322;t"
  ]
  node [
    id 418
    label "cz&#322;onek"
  ]
  node [
    id 419
    label "cia&#322;o"
  ]
  node [
    id 420
    label "obiekt"
  ]
  node [
    id 421
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 422
    label "definiens"
  ]
  node [
    id 423
    label "definiendum"
  ]
  node [
    id 424
    label "obja&#347;nienie"
  ]
  node [
    id 425
    label "definition"
  ]
  node [
    id 426
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 427
    label "whole"
  ]
  node [
    id 428
    label "odm&#322;adza&#263;"
  ]
  node [
    id 429
    label "zabudowania"
  ]
  node [
    id 430
    label "odm&#322;odzenie"
  ]
  node [
    id 431
    label "zespolik"
  ]
  node [
    id 432
    label "skupienie"
  ]
  node [
    id 433
    label "schorzenie"
  ]
  node [
    id 434
    label "Depeche_Mode"
  ]
  node [
    id 435
    label "Mazowsze"
  ]
  node [
    id 436
    label "The_Beatles"
  ]
  node [
    id 437
    label "group"
  ]
  node [
    id 438
    label "&#346;wietliki"
  ]
  node [
    id 439
    label "odm&#322;adzanie"
  ]
  node [
    id 440
    label "batch"
  ]
  node [
    id 441
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 442
    label "punkt_widzenia"
  ]
  node [
    id 443
    label "do&#322;ek"
  ]
  node [
    id 444
    label "formality"
  ]
  node [
    id 445
    label "wz&#243;r"
  ]
  node [
    id 446
    label "kantyzm"
  ]
  node [
    id 447
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 448
    label "ornamentyka"
  ]
  node [
    id 449
    label "odmiana"
  ]
  node [
    id 450
    label "mode"
  ]
  node [
    id 451
    label "style"
  ]
  node [
    id 452
    label "formacja"
  ]
  node [
    id 453
    label "maszyna_drukarska"
  ]
  node [
    id 454
    label "poznanie"
  ]
  node [
    id 455
    label "szablon"
  ]
  node [
    id 456
    label "struktura"
  ]
  node [
    id 457
    label "spirala"
  ]
  node [
    id 458
    label "blaszka"
  ]
  node [
    id 459
    label "linearno&#347;&#263;"
  ]
  node [
    id 460
    label "kielich"
  ]
  node [
    id 461
    label "pasmo"
  ]
  node [
    id 462
    label "rdze&#324;"
  ]
  node [
    id 463
    label "leksem"
  ]
  node [
    id 464
    label "dyspozycja"
  ]
  node [
    id 465
    label "wygl&#261;d"
  ]
  node [
    id 466
    label "October"
  ]
  node [
    id 467
    label "zawarto&#347;&#263;"
  ]
  node [
    id 468
    label "creation"
  ]
  node [
    id 469
    label "p&#281;tla"
  ]
  node [
    id 470
    label "p&#322;at"
  ]
  node [
    id 471
    label "gwiazda"
  ]
  node [
    id 472
    label "arystotelizm"
  ]
  node [
    id 473
    label "dzie&#322;o"
  ]
  node [
    id 474
    label "naczynie"
  ]
  node [
    id 475
    label "wyra&#380;enie"
  ]
  node [
    id 476
    label "jednostka_systematyczna"
  ]
  node [
    id 477
    label "miniatura"
  ]
  node [
    id 478
    label "morfem"
  ]
  node [
    id 479
    label "posta&#263;"
  ]
  node [
    id 480
    label "psychika"
  ]
  node [
    id 481
    label "psychoanaliza"
  ]
  node [
    id 482
    label "ekstraspekcja"
  ]
  node [
    id 483
    label "zemdle&#263;"
  ]
  node [
    id 484
    label "conscience"
  ]
  node [
    id 485
    label "Freud"
  ]
  node [
    id 486
    label "feeling"
  ]
  node [
    id 487
    label "niepubliczny"
  ]
  node [
    id 488
    label "spo&#322;ecznie"
  ]
  node [
    id 489
    label "publiczny"
  ]
  node [
    id 490
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 491
    label "treat"
  ]
  node [
    id 492
    label "uprawia&#263;_seks"
  ]
  node [
    id 493
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 494
    label "serve"
  ]
  node [
    id 495
    label "zaspakaja&#263;"
  ]
  node [
    id 496
    label "suffice"
  ]
  node [
    id 497
    label "zaspokaja&#263;"
  ]
  node [
    id 498
    label "report"
  ]
  node [
    id 499
    label "dawa&#263;"
  ]
  node [
    id 500
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 501
    label "reagowa&#263;"
  ]
  node [
    id 502
    label "contend"
  ]
  node [
    id 503
    label "ponosi&#263;"
  ]
  node [
    id 504
    label "impart"
  ]
  node [
    id 505
    label "react"
  ]
  node [
    id 506
    label "tone"
  ]
  node [
    id 507
    label "equate"
  ]
  node [
    id 508
    label "pytanie"
  ]
  node [
    id 509
    label "powodowa&#263;"
  ]
  node [
    id 510
    label "answer"
  ]
  node [
    id 511
    label "gromada"
  ]
  node [
    id 512
    label "autorament"
  ]
  node [
    id 513
    label "przypuszczenie"
  ]
  node [
    id 514
    label "cynk"
  ]
  node [
    id 515
    label "rezultat"
  ]
  node [
    id 516
    label "kr&#243;lestwo"
  ]
  node [
    id 517
    label "obstawia&#263;"
  ]
  node [
    id 518
    label "design"
  ]
  node [
    id 519
    label "facet"
  ]
  node [
    id 520
    label "variety"
  ]
  node [
    id 521
    label "antycypacja"
  ]
  node [
    id 522
    label "znawca"
  ]
  node [
    id 523
    label "skomplikowanie"
  ]
  node [
    id 524
    label "trudny"
  ]
  node [
    id 525
    label "strona"
  ]
  node [
    id 526
    label "przyczyna"
  ]
  node [
    id 527
    label "matuszka"
  ]
  node [
    id 528
    label "geneza"
  ]
  node [
    id 529
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 530
    label "czynnik"
  ]
  node [
    id 531
    label "poci&#261;ganie"
  ]
  node [
    id 532
    label "uprz&#261;&#380;"
  ]
  node [
    id 533
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 534
    label "subject"
  ]
  node [
    id 535
    label "ro&#347;lina_zielna"
  ]
  node [
    id 536
    label "karo"
  ]
  node [
    id 537
    label "dzwonkowate"
  ]
  node [
    id 538
    label "campanula"
  ]
  node [
    id 539
    label "przycisk"
  ]
  node [
    id 540
    label "&#322;&#261;cznik_instalacyjny"
  ]
  node [
    id 541
    label "dzwoni&#263;"
  ]
  node [
    id 542
    label "sygna&#322;_d&#378;wi&#281;kowy"
  ]
  node [
    id 543
    label "dzwonienie"
  ]
  node [
    id 544
    label "zadzwoni&#263;"
  ]
  node [
    id 545
    label "karta"
  ]
  node [
    id 546
    label "sygnalizator"
  ]
  node [
    id 547
    label "kolor"
  ]
  node [
    id 548
    label "dotyka&#263;"
  ]
  node [
    id 549
    label "cover"
  ]
  node [
    id 550
    label "obj&#261;&#263;"
  ]
  node [
    id 551
    label "zagarnia&#263;"
  ]
  node [
    id 552
    label "involve"
  ]
  node [
    id 553
    label "mie&#263;"
  ]
  node [
    id 554
    label "zobowi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 555
    label "embrace"
  ]
  node [
    id 556
    label "meet"
  ]
  node [
    id 557
    label "fold"
  ]
  node [
    id 558
    label "senator"
  ]
  node [
    id 559
    label "dotyczy&#263;"
  ]
  node [
    id 560
    label "rozumie&#263;"
  ]
  node [
    id 561
    label "obejmowanie"
  ]
  node [
    id 562
    label "zaskakiwa&#263;"
  ]
  node [
    id 563
    label "rozprzestrzenia&#263;_si&#281;"
  ]
  node [
    id 564
    label "podejmowa&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 147
  ]
  edge [
    source 7
    target 148
  ]
  edge [
    source 7
    target 149
  ]
  edge [
    source 7
    target 150
  ]
  edge [
    source 7
    target 151
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 152
  ]
  edge [
    source 9
    target 153
  ]
  edge [
    source 9
    target 154
  ]
  edge [
    source 9
    target 155
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 156
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 22
  ]
  edge [
    source 11
    target 23
  ]
  edge [
    source 11
    target 157
  ]
  edge [
    source 11
    target 158
  ]
  edge [
    source 11
    target 159
  ]
  edge [
    source 11
    target 39
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 160
  ]
  edge [
    source 12
    target 161
  ]
  edge [
    source 12
    target 162
  ]
  edge [
    source 12
    target 163
  ]
  edge [
    source 12
    target 164
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 32
  ]
  edge [
    source 14
    target 165
  ]
  edge [
    source 14
    target 166
  ]
  edge [
    source 14
    target 167
  ]
  edge [
    source 14
    target 168
  ]
  edge [
    source 14
    target 169
  ]
  edge [
    source 14
    target 170
  ]
  edge [
    source 14
    target 171
  ]
  edge [
    source 14
    target 25
  ]
  edge [
    source 14
    target 27
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 50
  ]
  edge [
    source 15
    target 75
  ]
  edge [
    source 15
    target 172
  ]
  edge [
    source 15
    target 173
  ]
  edge [
    source 15
    target 63
  ]
  edge [
    source 15
    target 174
  ]
  edge [
    source 15
    target 175
  ]
  edge [
    source 15
    target 176
  ]
  edge [
    source 15
    target 177
  ]
  edge [
    source 15
    target 178
  ]
  edge [
    source 15
    target 179
  ]
  edge [
    source 15
    target 180
  ]
  edge [
    source 15
    target 44
  ]
  edge [
    source 15
    target 31
  ]
  edge [
    source 15
    target 74
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 181
  ]
  edge [
    source 17
    target 182
  ]
  edge [
    source 17
    target 183
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 57
  ]
  edge [
    source 18
    target 184
  ]
  edge [
    source 18
    target 185
  ]
  edge [
    source 18
    target 186
  ]
  edge [
    source 18
    target 187
  ]
  edge [
    source 18
    target 188
  ]
  edge [
    source 18
    target 189
  ]
  edge [
    source 18
    target 183
  ]
  edge [
    source 18
    target 190
  ]
  edge [
    source 18
    target 181
  ]
  edge [
    source 18
    target 191
  ]
  edge [
    source 18
    target 192
  ]
  edge [
    source 18
    target 193
  ]
  edge [
    source 18
    target 144
  ]
  edge [
    source 18
    target 194
  ]
  edge [
    source 18
    target 195
  ]
  edge [
    source 18
    target 196
  ]
  edge [
    source 18
    target 197
  ]
  edge [
    source 18
    target 198
  ]
  edge [
    source 18
    target 199
  ]
  edge [
    source 18
    target 200
  ]
  edge [
    source 18
    target 201
  ]
  edge [
    source 18
    target 202
  ]
  edge [
    source 18
    target 182
  ]
  edge [
    source 18
    target 203
  ]
  edge [
    source 18
    target 204
  ]
  edge [
    source 18
    target 67
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 71
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 69
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 40
  ]
  edge [
    source 24
    target 41
  ]
  edge [
    source 24
    target 205
  ]
  edge [
    source 24
    target 206
  ]
  edge [
    source 24
    target 207
  ]
  edge [
    source 24
    target 208
  ]
  edge [
    source 24
    target 53
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 38
  ]
  edge [
    source 25
    target 39
  ]
  edge [
    source 25
    target 209
  ]
  edge [
    source 25
    target 27
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 210
  ]
  edge [
    source 26
    target 211
  ]
  edge [
    source 26
    target 212
  ]
  edge [
    source 26
    target 213
  ]
  edge [
    source 26
    target 214
  ]
  edge [
    source 26
    target 215
  ]
  edge [
    source 26
    target 216
  ]
  edge [
    source 26
    target 217
  ]
  edge [
    source 26
    target 218
  ]
  edge [
    source 26
    target 219
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 39
  ]
  edge [
    source 27
    target 40
  ]
  edge [
    source 27
    target 220
  ]
  edge [
    source 27
    target 221
  ]
  edge [
    source 27
    target 222
  ]
  edge [
    source 27
    target 223
  ]
  edge [
    source 27
    target 224
  ]
  edge [
    source 27
    target 60
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 225
  ]
  edge [
    source 28
    target 226
  ]
  edge [
    source 28
    target 227
  ]
  edge [
    source 28
    target 228
  ]
  edge [
    source 28
    target 229
  ]
  edge [
    source 28
    target 230
  ]
  edge [
    source 28
    target 231
  ]
  edge [
    source 28
    target 232
  ]
  edge [
    source 28
    target 233
  ]
  edge [
    source 28
    target 234
  ]
  edge [
    source 28
    target 235
  ]
  edge [
    source 28
    target 236
  ]
  edge [
    source 28
    target 237
  ]
  edge [
    source 28
    target 238
  ]
  edge [
    source 28
    target 239
  ]
  edge [
    source 28
    target 240
  ]
  edge [
    source 28
    target 241
  ]
  edge [
    source 28
    target 242
  ]
  edge [
    source 28
    target 243
  ]
  edge [
    source 28
    target 60
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 41
  ]
  edge [
    source 30
    target 42
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 244
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 51
  ]
  edge [
    source 32
    target 245
  ]
  edge [
    source 32
    target 246
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 247
  ]
  edge [
    source 33
    target 248
  ]
  edge [
    source 33
    target 249
  ]
  edge [
    source 33
    target 250
  ]
  edge [
    source 33
    target 251
  ]
  edge [
    source 33
    target 252
  ]
  edge [
    source 33
    target 253
  ]
  edge [
    source 33
    target 254
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 255
  ]
  edge [
    source 34
    target 256
  ]
  edge [
    source 34
    target 257
  ]
  edge [
    source 34
    target 258
  ]
  edge [
    source 34
    target 156
  ]
  edge [
    source 34
    target 259
  ]
  edge [
    source 34
    target 260
  ]
  edge [
    source 34
    target 50
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 261
  ]
  edge [
    source 35
    target 262
  ]
  edge [
    source 35
    target 263
  ]
  edge [
    source 35
    target 264
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 54
  ]
  edge [
    source 36
    target 55
  ]
  edge [
    source 36
    target 56
  ]
  edge [
    source 36
    target 265
  ]
  edge [
    source 36
    target 266
  ]
  edge [
    source 36
    target 267
  ]
  edge [
    source 36
    target 268
  ]
  edge [
    source 36
    target 269
  ]
  edge [
    source 36
    target 244
  ]
  edge [
    source 36
    target 270
  ]
  edge [
    source 36
    target 271
  ]
  edge [
    source 36
    target 272
  ]
  edge [
    source 36
    target 273
  ]
  edge [
    source 36
    target 274
  ]
  edge [
    source 36
    target 67
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 38
    target 275
  ]
  edge [
    source 38
    target 276
  ]
  edge [
    source 38
    target 43
  ]
  edge [
    source 39
    target 277
  ]
  edge [
    source 39
    target 278
  ]
  edge [
    source 39
    target 159
  ]
  edge [
    source 39
    target 135
  ]
  edge [
    source 39
    target 279
  ]
  edge [
    source 39
    target 280
  ]
  edge [
    source 39
    target 281
  ]
  edge [
    source 39
    target 282
  ]
  edge [
    source 39
    target 283
  ]
  edge [
    source 39
    target 284
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 285
  ]
  edge [
    source 42
    target 66
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 76
  ]
  edge [
    source 43
    target 286
  ]
  edge [
    source 43
    target 287
  ]
  edge [
    source 43
    target 288
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 63
  ]
  edge [
    source 44
    target 174
  ]
  edge [
    source 44
    target 289
  ]
  edge [
    source 44
    target 177
  ]
  edge [
    source 44
    target 290
  ]
  edge [
    source 44
    target 291
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 292
  ]
  edge [
    source 45
    target 293
  ]
  edge [
    source 45
    target 294
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 61
  ]
  edge [
    source 46
    target 75
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 295
  ]
  edge [
    source 48
    target 296
  ]
  edge [
    source 48
    target 297
  ]
  edge [
    source 48
    target 298
  ]
  edge [
    source 48
    target 299
  ]
  edge [
    source 48
    target 300
  ]
  edge [
    source 49
    target 301
  ]
  edge [
    source 49
    target 302
  ]
  edge [
    source 49
    target 153
  ]
  edge [
    source 49
    target 303
  ]
  edge [
    source 49
    target 298
  ]
  edge [
    source 49
    target 304
  ]
  edge [
    source 49
    target 305
  ]
  edge [
    source 49
    target 306
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 52
  ]
  edge [
    source 50
    target 307
  ]
  edge [
    source 50
    target 308
  ]
  edge [
    source 50
    target 160
  ]
  edge [
    source 50
    target 309
  ]
  edge [
    source 50
    target 310
  ]
  edge [
    source 50
    target 311
  ]
  edge [
    source 50
    target 275
  ]
  edge [
    source 50
    target 312
  ]
  edge [
    source 50
    target 313
  ]
  edge [
    source 51
    target 118
  ]
  edge [
    source 51
    target 314
  ]
  edge [
    source 51
    target 315
  ]
  edge [
    source 51
    target 316
  ]
  edge [
    source 51
    target 317
  ]
  edge [
    source 51
    target 318
  ]
  edge [
    source 51
    target 319
  ]
  edge [
    source 51
    target 320
  ]
  edge [
    source 51
    target 321
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 61
  ]
  edge [
    source 52
    target 62
  ]
  edge [
    source 52
    target 322
  ]
  edge [
    source 52
    target 323
  ]
  edge [
    source 52
    target 324
  ]
  edge [
    source 52
    target 325
  ]
  edge [
    source 52
    target 252
  ]
  edge [
    source 52
    target 326
  ]
  edge [
    source 52
    target 327
  ]
  edge [
    source 52
    target 328
  ]
  edge [
    source 52
    target 329
  ]
  edge [
    source 52
    target 330
  ]
  edge [
    source 52
    target 331
  ]
  edge [
    source 52
    target 332
  ]
  edge [
    source 52
    target 333
  ]
  edge [
    source 52
    target 334
  ]
  edge [
    source 52
    target 335
  ]
  edge [
    source 52
    target 336
  ]
  edge [
    source 52
    target 337
  ]
  edge [
    source 52
    target 338
  ]
  edge [
    source 52
    target 339
  ]
  edge [
    source 52
    target 340
  ]
  edge [
    source 52
    target 341
  ]
  edge [
    source 52
    target 342
  ]
  edge [
    source 52
    target 343
  ]
  edge [
    source 52
    target 344
  ]
  edge [
    source 52
    target 345
  ]
  edge [
    source 52
    target 346
  ]
  edge [
    source 52
    target 347
  ]
  edge [
    source 52
    target 348
  ]
  edge [
    source 52
    target 349
  ]
  edge [
    source 52
    target 350
  ]
  edge [
    source 52
    target 351
  ]
  edge [
    source 52
    target 352
  ]
  edge [
    source 53
    target 353
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 354
  ]
  edge [
    source 54
    target 355
  ]
  edge [
    source 54
    target 356
  ]
  edge [
    source 54
    target 357
  ]
  edge [
    source 54
    target 358
  ]
  edge [
    source 54
    target 359
  ]
  edge [
    source 54
    target 360
  ]
  edge [
    source 54
    target 239
  ]
  edge [
    source 54
    target 361
  ]
  edge [
    source 54
    target 362
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 70
  ]
  edge [
    source 57
    target 363
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 364
  ]
  edge [
    source 58
    target 365
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 366
  ]
  edge [
    source 59
    target 367
  ]
  edge [
    source 59
    target 368
  ]
  edge [
    source 59
    target 369
  ]
  edge [
    source 59
    target 370
  ]
  edge [
    source 59
    target 371
  ]
  edge [
    source 59
    target 372
  ]
  edge [
    source 59
    target 373
  ]
  edge [
    source 59
    target 374
  ]
  edge [
    source 59
    target 375
  ]
  edge [
    source 59
    target 376
  ]
  edge [
    source 59
    target 377
  ]
  edge [
    source 59
    target 378
  ]
  edge [
    source 59
    target 379
  ]
  edge [
    source 59
    target 380
  ]
  edge [
    source 59
    target 381
  ]
  edge [
    source 59
    target 382
  ]
  edge [
    source 59
    target 383
  ]
  edge [
    source 59
    target 384
  ]
  edge [
    source 59
    target 385
  ]
  edge [
    source 59
    target 386
  ]
  edge [
    source 59
    target 387
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 76
  ]
  edge [
    source 60
    target 388
  ]
  edge [
    source 60
    target 389
  ]
  edge [
    source 60
    target 390
  ]
  edge [
    source 60
    target 391
  ]
  edge [
    source 60
    target 392
  ]
  edge [
    source 60
    target 393
  ]
  edge [
    source 60
    target 394
  ]
  edge [
    source 60
    target 395
  ]
  edge [
    source 60
    target 396
  ]
  edge [
    source 60
    target 397
  ]
  edge [
    source 60
    target 398
  ]
  edge [
    source 60
    target 399
  ]
  edge [
    source 60
    target 327
  ]
  edge [
    source 60
    target 400
  ]
  edge [
    source 60
    target 401
  ]
  edge [
    source 60
    target 402
  ]
  edge [
    source 60
    target 403
  ]
  edge [
    source 60
    target 404
  ]
  edge [
    source 60
    target 405
  ]
  edge [
    source 60
    target 406
  ]
  edge [
    source 60
    target 407
  ]
  edge [
    source 60
    target 408
  ]
  edge [
    source 60
    target 409
  ]
  edge [
    source 60
    target 410
  ]
  edge [
    source 60
    target 411
  ]
  edge [
    source 60
    target 412
  ]
  edge [
    source 60
    target 413
  ]
  edge [
    source 60
    target 414
  ]
  edge [
    source 60
    target 340
  ]
  edge [
    source 60
    target 415
  ]
  edge [
    source 60
    target 416
  ]
  edge [
    source 60
    target 417
  ]
  edge [
    source 60
    target 418
  ]
  edge [
    source 60
    target 419
  ]
  edge [
    source 60
    target 420
  ]
  edge [
    source 60
    target 421
  ]
  edge [
    source 60
    target 63
  ]
  edge [
    source 61
    target 72
  ]
  edge [
    source 61
    target 73
  ]
  edge [
    source 61
    target 422
  ]
  edge [
    source 61
    target 423
  ]
  edge [
    source 61
    target 424
  ]
  edge [
    source 61
    target 425
  ]
  edge [
    source 61
    target 75
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 426
  ]
  edge [
    source 62
    target 427
  ]
  edge [
    source 62
    target 428
  ]
  edge [
    source 62
    target 429
  ]
  edge [
    source 62
    target 430
  ]
  edge [
    source 62
    target 431
  ]
  edge [
    source 62
    target 432
  ]
  edge [
    source 62
    target 433
  ]
  edge [
    source 62
    target 277
  ]
  edge [
    source 62
    target 434
  ]
  edge [
    source 62
    target 435
  ]
  edge [
    source 62
    target 405
  ]
  edge [
    source 62
    target 290
  ]
  edge [
    source 62
    target 436
  ]
  edge [
    source 62
    target 437
  ]
  edge [
    source 62
    target 438
  ]
  edge [
    source 62
    target 439
  ]
  edge [
    source 62
    target 440
  ]
  edge [
    source 62
    target 71
  ]
  edge [
    source 62
    target 72
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 441
  ]
  edge [
    source 63
    target 442
  ]
  edge [
    source 63
    target 443
  ]
  edge [
    source 63
    target 444
  ]
  edge [
    source 63
    target 445
  ]
  edge [
    source 63
    target 446
  ]
  edge [
    source 63
    target 447
  ]
  edge [
    source 63
    target 448
  ]
  edge [
    source 63
    target 449
  ]
  edge [
    source 63
    target 450
  ]
  edge [
    source 63
    target 451
  ]
  edge [
    source 63
    target 452
  ]
  edge [
    source 63
    target 453
  ]
  edge [
    source 63
    target 454
  ]
  edge [
    source 63
    target 455
  ]
  edge [
    source 63
    target 456
  ]
  edge [
    source 63
    target 457
  ]
  edge [
    source 63
    target 458
  ]
  edge [
    source 63
    target 459
  ]
  edge [
    source 63
    target 268
  ]
  edge [
    source 63
    target 248
  ]
  edge [
    source 63
    target 340
  ]
  edge [
    source 63
    target 460
  ]
  edge [
    source 63
    target 416
  ]
  edge [
    source 63
    target 417
  ]
  edge [
    source 63
    target 461
  ]
  edge [
    source 63
    target 462
  ]
  edge [
    source 63
    target 463
  ]
  edge [
    source 63
    target 464
  ]
  edge [
    source 63
    target 465
  ]
  edge [
    source 63
    target 466
  ]
  edge [
    source 63
    target 467
  ]
  edge [
    source 63
    target 468
  ]
  edge [
    source 63
    target 469
  ]
  edge [
    source 63
    target 470
  ]
  edge [
    source 63
    target 471
  ]
  edge [
    source 63
    target 472
  ]
  edge [
    source 63
    target 420
  ]
  edge [
    source 63
    target 473
  ]
  edge [
    source 63
    target 474
  ]
  edge [
    source 63
    target 475
  ]
  edge [
    source 63
    target 476
  ]
  edge [
    source 63
    target 477
  ]
  edge [
    source 63
    target 350
  ]
  edge [
    source 63
    target 478
  ]
  edge [
    source 63
    target 479
  ]
  edge [
    source 63
    target 71
  ]
  edge [
    source 63
    target 72
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 268
  ]
  edge [
    source 64
    target 480
  ]
  edge [
    source 64
    target 481
  ]
  edge [
    source 64
    target 404
  ]
  edge [
    source 64
    target 482
  ]
  edge [
    source 64
    target 483
  ]
  edge [
    source 64
    target 484
  ]
  edge [
    source 64
    target 485
  ]
  edge [
    source 64
    target 486
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 69
  ]
  edge [
    source 65
    target 487
  ]
  edge [
    source 65
    target 488
  ]
  edge [
    source 65
    target 489
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 66
    target 490
  ]
  edge [
    source 66
    target 491
  ]
  edge [
    source 66
    target 492
  ]
  edge [
    source 66
    target 493
  ]
  edge [
    source 66
    target 494
  ]
  edge [
    source 66
    target 495
  ]
  edge [
    source 66
    target 496
  ]
  edge [
    source 66
    target 497
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 67
    target 498
  ]
  edge [
    source 67
    target 499
  ]
  edge [
    source 67
    target 500
  ]
  edge [
    source 67
    target 501
  ]
  edge [
    source 67
    target 502
  ]
  edge [
    source 67
    target 503
  ]
  edge [
    source 67
    target 504
  ]
  edge [
    source 67
    target 505
  ]
  edge [
    source 67
    target 506
  ]
  edge [
    source 67
    target 507
  ]
  edge [
    source 67
    target 508
  ]
  edge [
    source 67
    target 509
  ]
  edge [
    source 67
    target 510
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 68
    target 76
  ]
  edge [
    source 68
    target 511
  ]
  edge [
    source 68
    target 512
  ]
  edge [
    source 68
    target 513
  ]
  edge [
    source 68
    target 514
  ]
  edge [
    source 68
    target 515
  ]
  edge [
    source 68
    target 476
  ]
  edge [
    source 68
    target 516
  ]
  edge [
    source 68
    target 517
  ]
  edge [
    source 68
    target 518
  ]
  edge [
    source 68
    target 519
  ]
  edge [
    source 68
    target 520
  ]
  edge [
    source 68
    target 327
  ]
  edge [
    source 68
    target 521
  ]
  edge [
    source 69
    target 522
  ]
  edge [
    source 70
    target 523
  ]
  edge [
    source 70
    target 524
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 72
    target 525
  ]
  edge [
    source 72
    target 526
  ]
  edge [
    source 72
    target 527
  ]
  edge [
    source 72
    target 528
  ]
  edge [
    source 72
    target 529
  ]
  edge [
    source 72
    target 530
  ]
  edge [
    source 72
    target 531
  ]
  edge [
    source 72
    target 515
  ]
  edge [
    source 72
    target 532
  ]
  edge [
    source 72
    target 533
  ]
  edge [
    source 72
    target 534
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 73
    target 535
  ]
  edge [
    source 73
    target 536
  ]
  edge [
    source 73
    target 537
  ]
  edge [
    source 73
    target 538
  ]
  edge [
    source 73
    target 539
  ]
  edge [
    source 73
    target 540
  ]
  edge [
    source 73
    target 541
  ]
  edge [
    source 73
    target 542
  ]
  edge [
    source 73
    target 543
  ]
  edge [
    source 73
    target 544
  ]
  edge [
    source 73
    target 545
  ]
  edge [
    source 73
    target 546
  ]
  edge [
    source 73
    target 547
  ]
  edge [
    source 74
    target 75
  ]
  edge [
    source 74
    target 548
  ]
  edge [
    source 74
    target 549
  ]
  edge [
    source 74
    target 550
  ]
  edge [
    source 74
    target 551
  ]
  edge [
    source 74
    target 552
  ]
  edge [
    source 74
    target 553
  ]
  edge [
    source 74
    target 554
  ]
  edge [
    source 74
    target 555
  ]
  edge [
    source 74
    target 556
  ]
  edge [
    source 74
    target 557
  ]
  edge [
    source 74
    target 558
  ]
  edge [
    source 74
    target 559
  ]
  edge [
    source 74
    target 560
  ]
  edge [
    source 74
    target 561
  ]
  edge [
    source 74
    target 562
  ]
  edge [
    source 74
    target 563
  ]
  edge [
    source 74
    target 509
  ]
  edge [
    source 74
    target 564
  ]
]
