graph [
  maxDegree 47
  minDegree 1
  meanDegree 2.3333333333333335
  density 0.02456140350877193
  graphCliqueNumber 8
  node [
    id 0
    label "podsekretarz"
    origin "text"
  ]
  node [
    id 1
    label "stan"
    origin "text"
  ]
  node [
    id 2
    label "ministerstwo"
    origin "text"
  ]
  node [
    id 3
    label "rolnictwo"
    origin "text"
  ]
  node [
    id 4
    label "rozw&#243;j"
    origin "text"
  ]
  node [
    id 5
    label "wsi"
    origin "text"
  ]
  node [
    id 6
    label "artur"
    origin "text"
  ]
  node [
    id 7
    label "&#322;awniczak"
    origin "text"
  ]
  node [
    id 8
    label "zast&#281;pca"
  ]
  node [
    id 9
    label "dostojnik"
  ]
  node [
    id 10
    label "sekretarz_stanu"
  ]
  node [
    id 11
    label "Arizona"
  ]
  node [
    id 12
    label "Georgia"
  ]
  node [
    id 13
    label "warstwa"
  ]
  node [
    id 14
    label "jednostka_administracyjna"
  ]
  node [
    id 15
    label "Goa"
  ]
  node [
    id 16
    label "Hawaje"
  ]
  node [
    id 17
    label "Floryda"
  ]
  node [
    id 18
    label "Oklahoma"
  ]
  node [
    id 19
    label "punkt"
  ]
  node [
    id 20
    label "Alaska"
  ]
  node [
    id 21
    label "Alabama"
  ]
  node [
    id 22
    label "wci&#281;cie"
  ]
  node [
    id 23
    label "Oregon"
  ]
  node [
    id 24
    label "poziom"
  ]
  node [
    id 25
    label "by&#263;"
  ]
  node [
    id 26
    label "Teksas"
  ]
  node [
    id 27
    label "Illinois"
  ]
  node [
    id 28
    label "Jukatan"
  ]
  node [
    id 29
    label "Waszyngton"
  ]
  node [
    id 30
    label "shape"
  ]
  node [
    id 31
    label "Nowy_Meksyk"
  ]
  node [
    id 32
    label "ilo&#347;&#263;"
  ]
  node [
    id 33
    label "state"
  ]
  node [
    id 34
    label "Nowy_York"
  ]
  node [
    id 35
    label "Arakan"
  ]
  node [
    id 36
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 37
    label "Kalifornia"
  ]
  node [
    id 38
    label "wektor"
  ]
  node [
    id 39
    label "Massachusetts"
  ]
  node [
    id 40
    label "miejsce"
  ]
  node [
    id 41
    label "Pensylwania"
  ]
  node [
    id 42
    label "Maryland"
  ]
  node [
    id 43
    label "Michigan"
  ]
  node [
    id 44
    label "Ohio"
  ]
  node [
    id 45
    label "Kansas"
  ]
  node [
    id 46
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 47
    label "Luizjana"
  ]
  node [
    id 48
    label "samopoczucie"
  ]
  node [
    id 49
    label "Wirginia"
  ]
  node [
    id 50
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 51
    label "Ministerstwo_Obrony_Narodowej"
  ]
  node [
    id 52
    label "ministerium"
  ]
  node [
    id 53
    label "resort"
  ]
  node [
    id 54
    label "urz&#261;d"
  ]
  node [
    id 55
    label "MSW"
  ]
  node [
    id 56
    label "departament"
  ]
  node [
    id 57
    label "NKWD"
  ]
  node [
    id 58
    label "intensyfikacja"
  ]
  node [
    id 59
    label "agronomia"
  ]
  node [
    id 60
    label "gleboznawstwo"
  ]
  node [
    id 61
    label "&#322;&#261;karstwo"
  ]
  node [
    id 62
    label "sadownictwo"
  ]
  node [
    id 63
    label "&#322;owiectwo"
  ]
  node [
    id 64
    label "agrobiznes"
  ]
  node [
    id 65
    label "nasiennictwo"
  ]
  node [
    id 66
    label "agroekologia"
  ]
  node [
    id 67
    label "uprawianie"
  ]
  node [
    id 68
    label "gospodarka"
  ]
  node [
    id 69
    label "zgarniacz"
  ]
  node [
    id 70
    label "hodowla"
  ]
  node [
    id 71
    label "agrochemia"
  ]
  node [
    id 72
    label "farmerstwo"
  ]
  node [
    id 73
    label "zootechnika"
  ]
  node [
    id 74
    label "agrotechnika"
  ]
  node [
    id 75
    label "ogrodnictwo"
  ]
  node [
    id 76
    label "nauka"
  ]
  node [
    id 77
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 78
    label "procedura"
  ]
  node [
    id 79
    label "process"
  ]
  node [
    id 80
    label "cycle"
  ]
  node [
    id 81
    label "proces"
  ]
  node [
    id 82
    label "&#380;ycie"
  ]
  node [
    id 83
    label "z&#322;ote_czasy"
  ]
  node [
    id 84
    label "proces_biologiczny"
  ]
  node [
    id 85
    label "wyspa"
  ]
  node [
    id 86
    label "i"
  ]
  node [
    id 87
    label "wie&#347;"
  ]
  node [
    id 88
    label "Artur"
  ]
  node [
    id 89
    label "&#321;awniczak"
  ]
  node [
    id 90
    label "Jerzy"
  ]
  node [
    id 91
    label "Szmajdzi&#324;ski"
  ]
  node [
    id 92
    label "Roberta"
  ]
  node [
    id 93
    label "Telus"
  ]
  node [
    id 94
    label "Wojciecha"
  ]
  node [
    id 95
    label "Mojzesowicz"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 85
    target 86
  ]
  edge [
    source 85
    target 87
  ]
  edge [
    source 86
    target 87
  ]
  edge [
    source 88
    target 89
  ]
  edge [
    source 90
    target 91
  ]
  edge [
    source 92
    target 93
  ]
  edge [
    source 94
    target 95
  ]
]
