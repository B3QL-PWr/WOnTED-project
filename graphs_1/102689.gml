graph [
  maxDegree 15
  minDegree 1
  meanDegree 2.0952380952380953
  density 0.05110336817653891
  graphCliqueNumber 3
  node [
    id 0
    label "pi&#281;&#263;set"
    origin "text"
  ]
  node [
    id 1
    label "dwadzie&#347;cia"
    origin "text"
  ]
  node [
    id 2
    label "cztery"
    origin "text"
  ]
  node [
    id 3
    label "tak"
    origin "text"
  ]
  node [
    id 4
    label "troszk&#281;"
    origin "text"
  ]
  node [
    id 5
    label "okr&#281;&#380;nie"
    origin "text"
  ]
  node [
    id 6
    label "jakby"
    origin "text"
  ]
  node [
    id 7
    label "zawraca&#263;"
    origin "text"
  ]
  node [
    id 8
    label "lewo"
    origin "text"
  ]
  node [
    id 9
    label "jak"
    origin "text"
  ]
  node [
    id 10
    label "patrzy&#263;by&#263;"
    origin "text"
  ]
  node [
    id 11
    label "plan"
    origin "text"
  ]
  node [
    id 12
    label "po&#347;rednio"
  ]
  node [
    id 13
    label "okr&#281;&#380;ny"
  ]
  node [
    id 14
    label "zawile"
  ]
  node [
    id 15
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 16
    label "return"
  ]
  node [
    id 17
    label "podejrzanie"
  ]
  node [
    id 18
    label "kierunek"
  ]
  node [
    id 19
    label "kiepsko"
  ]
  node [
    id 20
    label "szko&#322;a"
  ]
  node [
    id 21
    label "nielegalnie"
  ]
  node [
    id 22
    label "lewy"
  ]
  node [
    id 23
    label "byd&#322;o"
  ]
  node [
    id 24
    label "zobo"
  ]
  node [
    id 25
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 26
    label "yakalo"
  ]
  node [
    id 27
    label "dzo"
  ]
  node [
    id 28
    label "device"
  ]
  node [
    id 29
    label "model"
  ]
  node [
    id 30
    label "wytw&#243;r"
  ]
  node [
    id 31
    label "obraz"
  ]
  node [
    id 32
    label "przestrze&#324;"
  ]
  node [
    id 33
    label "dekoracja"
  ]
  node [
    id 34
    label "intencja"
  ]
  node [
    id 35
    label "agreement"
  ]
  node [
    id 36
    label "pomys&#322;"
  ]
  node [
    id 37
    label "punkt"
  ]
  node [
    id 38
    label "miejsce_pracy"
  ]
  node [
    id 39
    label "perspektywa"
  ]
  node [
    id 40
    label "rysunek"
  ]
  node [
    id 41
    label "reprezentacja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 5
    target 12
  ]
  edge [
    source 5
    target 13
  ]
  edge [
    source 5
    target 14
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 15
  ]
  edge [
    source 7
    target 16
  ]
  edge [
    source 8
    target 17
  ]
  edge [
    source 8
    target 18
  ]
  edge [
    source 8
    target 19
  ]
  edge [
    source 8
    target 20
  ]
  edge [
    source 8
    target 21
  ]
  edge [
    source 8
    target 22
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 23
  ]
  edge [
    source 9
    target 24
  ]
  edge [
    source 9
    target 25
  ]
  edge [
    source 9
    target 26
  ]
  edge [
    source 9
    target 27
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 28
  ]
  edge [
    source 11
    target 29
  ]
  edge [
    source 11
    target 30
  ]
  edge [
    source 11
    target 31
  ]
  edge [
    source 11
    target 32
  ]
  edge [
    source 11
    target 33
  ]
  edge [
    source 11
    target 34
  ]
  edge [
    source 11
    target 35
  ]
  edge [
    source 11
    target 36
  ]
  edge [
    source 11
    target 37
  ]
  edge [
    source 11
    target 38
  ]
  edge [
    source 11
    target 39
  ]
  edge [
    source 11
    target 40
  ]
  edge [
    source 11
    target 41
  ]
]
