graph [
  maxDegree 30
  minDegree 1
  meanDegree 1.9583333333333333
  density 0.041666666666666664
  graphCliqueNumber 2
  node [
    id 0
    label "da&#263;"
    origin "text"
  ]
  node [
    id 1
    label "plus"
    origin "text"
  ]
  node [
    id 2
    label "mirka"
    origin "text"
  ]
  node [
    id 3
    label "pocieszenie"
    origin "text"
  ]
  node [
    id 4
    label "dostarczy&#263;"
  ]
  node [
    id 5
    label "obieca&#263;"
  ]
  node [
    id 6
    label "pozwoli&#263;"
  ]
  node [
    id 7
    label "przeznaczy&#263;"
  ]
  node [
    id 8
    label "doda&#263;"
  ]
  node [
    id 9
    label "give"
  ]
  node [
    id 10
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 11
    label "wyrzec_si&#281;"
  ]
  node [
    id 12
    label "supply"
  ]
  node [
    id 13
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 14
    label "zada&#263;"
  ]
  node [
    id 15
    label "odst&#261;pi&#263;"
  ]
  node [
    id 16
    label "feed"
  ]
  node [
    id 17
    label "testify"
  ]
  node [
    id 18
    label "powierzy&#263;"
  ]
  node [
    id 19
    label "convey"
  ]
  node [
    id 20
    label "przekaza&#263;"
  ]
  node [
    id 21
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 22
    label "zap&#322;aci&#263;"
  ]
  node [
    id 23
    label "dress"
  ]
  node [
    id 24
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 25
    label "udost&#281;pni&#263;"
  ]
  node [
    id 26
    label "sztachn&#261;&#263;"
  ]
  node [
    id 27
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 28
    label "zrobi&#263;"
  ]
  node [
    id 29
    label "przywali&#263;"
  ]
  node [
    id 30
    label "rap"
  ]
  node [
    id 31
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 32
    label "picture"
  ]
  node [
    id 33
    label "warto&#347;&#263;"
  ]
  node [
    id 34
    label "wabik"
  ]
  node [
    id 35
    label "rewaluowa&#263;"
  ]
  node [
    id 36
    label "korzy&#347;&#263;"
  ]
  node [
    id 37
    label "dodawanie"
  ]
  node [
    id 38
    label "rewaluowanie"
  ]
  node [
    id 39
    label "stopie&#324;"
  ]
  node [
    id 40
    label "ocena"
  ]
  node [
    id 41
    label "zrewaluowa&#263;"
  ]
  node [
    id 42
    label "liczba"
  ]
  node [
    id 43
    label "znak_matematyczny"
  ]
  node [
    id 44
    label "strona"
  ]
  node [
    id 45
    label "zrewaluowanie"
  ]
  node [
    id 46
    label "pomo&#380;enie"
  ]
  node [
    id 47
    label "ukojenie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
]
