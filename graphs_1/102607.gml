graph [
  maxDegree 28
  minDegree 1
  meanDegree 2.0142857142857142
  density 0.014491264131551902
  graphCliqueNumber 3
  node [
    id 0
    label "stosunek"
    origin "text"
  ]
  node [
    id 1
    label "praca"
    origin "text"
  ]
  node [
    id 2
    label "mianowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "pracownik"
    origin "text"
  ]
  node [
    id 4
    label "naukowy"
    origin "text"
  ]
  node [
    id 5
    label "ulega&#263;"
    origin "text"
  ]
  node [
    id 6
    label "rozwi&#261;zanie"
    origin "text"
  ]
  node [
    id 7
    label "bez"
    origin "text"
  ]
  node [
    id 8
    label "wypowiedzenie"
    origin "text"
  ]
  node [
    id 9
    label "erotyka"
  ]
  node [
    id 10
    label "podniecanie"
  ]
  node [
    id 11
    label "wzw&#243;d"
  ]
  node [
    id 12
    label "rozmna&#380;anie"
  ]
  node [
    id 13
    label "po&#380;&#261;danie"
  ]
  node [
    id 14
    label "imisja"
  ]
  node [
    id 15
    label "po&#380;ycie"
  ]
  node [
    id 16
    label "pozycja_misjonarska"
  ]
  node [
    id 17
    label "podnieci&#263;"
  ]
  node [
    id 18
    label "podnieca&#263;"
  ]
  node [
    id 19
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 20
    label "iloraz"
  ]
  node [
    id 21
    label "czynno&#347;&#263;"
  ]
  node [
    id 22
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 23
    label "gra_wst&#281;pna"
  ]
  node [
    id 24
    label "podej&#347;cie"
  ]
  node [
    id 25
    label "cecha"
  ]
  node [
    id 26
    label "wyraz_skrajny"
  ]
  node [
    id 27
    label "numer"
  ]
  node [
    id 28
    label "ruch_frykcyjny"
  ]
  node [
    id 29
    label "baraszki"
  ]
  node [
    id 30
    label "powaga"
  ]
  node [
    id 31
    label "na_pieska"
  ]
  node [
    id 32
    label "z&#322;&#261;czenie"
  ]
  node [
    id 33
    label "relacja"
  ]
  node [
    id 34
    label "seks"
  ]
  node [
    id 35
    label "podniecenie"
  ]
  node [
    id 36
    label "stosunek_pracy"
  ]
  node [
    id 37
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 38
    label "benedykty&#324;ski"
  ]
  node [
    id 39
    label "pracowanie"
  ]
  node [
    id 40
    label "zaw&#243;d"
  ]
  node [
    id 41
    label "kierownictwo"
  ]
  node [
    id 42
    label "zmiana"
  ]
  node [
    id 43
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 44
    label "wytw&#243;r"
  ]
  node [
    id 45
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 46
    label "tynkarski"
  ]
  node [
    id 47
    label "czynnik_produkcji"
  ]
  node [
    id 48
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 49
    label "zobowi&#261;zanie"
  ]
  node [
    id 50
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 51
    label "tyrka"
  ]
  node [
    id 52
    label "pracowa&#263;"
  ]
  node [
    id 53
    label "siedziba"
  ]
  node [
    id 54
    label "poda&#380;_pracy"
  ]
  node [
    id 55
    label "miejsce"
  ]
  node [
    id 56
    label "zak&#322;ad"
  ]
  node [
    id 57
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 58
    label "najem"
  ]
  node [
    id 59
    label "assign"
  ]
  node [
    id 60
    label "nada&#263;"
  ]
  node [
    id 61
    label "powo&#322;ywa&#263;"
  ]
  node [
    id 62
    label "appoint"
  ]
  node [
    id 63
    label "powo&#322;a&#263;"
  ]
  node [
    id 64
    label "nadawa&#263;"
  ]
  node [
    id 65
    label "cz&#322;owiek_pracy"
  ]
  node [
    id 66
    label "cz&#322;owiek"
  ]
  node [
    id 67
    label "delegowa&#263;"
  ]
  node [
    id 68
    label "s&#322;u&#380;ba_cywilna"
  ]
  node [
    id 69
    label "pracu&#347;"
  ]
  node [
    id 70
    label "delegowanie"
  ]
  node [
    id 71
    label "r&#281;ka"
  ]
  node [
    id 72
    label "salariat"
  ]
  node [
    id 73
    label "specjalny"
  ]
  node [
    id 74
    label "edukacyjnie"
  ]
  node [
    id 75
    label "intelektualny"
  ]
  node [
    id 76
    label "skomplikowany"
  ]
  node [
    id 77
    label "zgodny"
  ]
  node [
    id 78
    label "naukowo"
  ]
  node [
    id 79
    label "scjentyficzny"
  ]
  node [
    id 80
    label "teoretyczny"
  ]
  node [
    id 81
    label "specjalistyczny"
  ]
  node [
    id 82
    label "zezwala&#263;"
  ]
  node [
    id 83
    label "render"
  ]
  node [
    id 84
    label "podporz&#261;dkowywa&#263;_si&#281;"
  ]
  node [
    id 85
    label "postpone"
  ]
  node [
    id 86
    label "kobieta"
  ]
  node [
    id 87
    label "poddawa&#263;"
  ]
  node [
    id 88
    label "poddawa&#263;_si&#281;"
  ]
  node [
    id 89
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 90
    label "przywo&#322;a&#263;"
  ]
  node [
    id 91
    label "subject"
  ]
  node [
    id 92
    label "wynik"
  ]
  node [
    id 93
    label "wyj&#347;cie"
  ]
  node [
    id 94
    label "spe&#322;nienie"
  ]
  node [
    id 95
    label "cesarskie_ci&#281;cie"
  ]
  node [
    id 96
    label "po&#322;o&#380;na"
  ]
  node [
    id 97
    label "proces_fizjologiczny"
  ]
  node [
    id 98
    label "przestanie"
  ]
  node [
    id 99
    label "marc&#243;wka"
  ]
  node [
    id 100
    label "usuni&#281;cie"
  ]
  node [
    id 101
    label "uniewa&#380;nienie"
  ]
  node [
    id 102
    label "pomys&#322;"
  ]
  node [
    id 103
    label "birth"
  ]
  node [
    id 104
    label "wymy&#347;lenie"
  ]
  node [
    id 105
    label "po&#322;&#243;g"
  ]
  node [
    id 106
    label "szok_poporodowy"
  ]
  node [
    id 107
    label "event"
  ]
  node [
    id 108
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 109
    label "spos&#243;b"
  ]
  node [
    id 110
    label "rozwi&#261;zywanie_si&#281;"
  ]
  node [
    id 111
    label "dula"
  ]
  node [
    id 112
    label "ki&#347;&#263;"
  ]
  node [
    id 113
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 114
    label "krzew"
  ]
  node [
    id 115
    label "pi&#380;maczkowate"
  ]
  node [
    id 116
    label "pestkowiec"
  ]
  node [
    id 117
    label "kwiat"
  ]
  node [
    id 118
    label "owoc"
  ]
  node [
    id 119
    label "oliwkowate"
  ]
  node [
    id 120
    label "ro&#347;lina"
  ]
  node [
    id 121
    label "hy&#263;ka"
  ]
  node [
    id 122
    label "lilac"
  ]
  node [
    id 123
    label "delfinidyna"
  ]
  node [
    id 124
    label "&#347;cie&#347;nienie"
  ]
  node [
    id 125
    label "denunciation"
  ]
  node [
    id 126
    label "wydanie"
  ]
  node [
    id 127
    label "konwersja"
  ]
  node [
    id 128
    label "notification"
  ]
  node [
    id 129
    label "generowa&#263;"
  ]
  node [
    id 130
    label "wyra&#380;enie"
  ]
  node [
    id 131
    label "message"
  ]
  node [
    id 132
    label "powiedzenie"
  ]
  node [
    id 133
    label "szyk"
  ]
  node [
    id 134
    label "notice"
  ]
  node [
    id 135
    label "wydobycie"
  ]
  node [
    id 136
    label "generowanie"
  ]
  node [
    id 137
    label "przepowiedzenie"
  ]
  node [
    id 138
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 139
    label "zwerbalizowanie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 7
    target 113
  ]
  edge [
    source 7
    target 114
  ]
  edge [
    source 7
    target 115
  ]
  edge [
    source 7
    target 116
  ]
  edge [
    source 7
    target 117
  ]
  edge [
    source 7
    target 118
  ]
  edge [
    source 7
    target 119
  ]
  edge [
    source 7
    target 120
  ]
  edge [
    source 7
    target 121
  ]
  edge [
    source 7
    target 122
  ]
  edge [
    source 7
    target 123
  ]
  edge [
    source 8
    target 124
  ]
  edge [
    source 8
    target 125
  ]
  edge [
    source 8
    target 126
  ]
  edge [
    source 8
    target 127
  ]
  edge [
    source 8
    target 128
  ]
  edge [
    source 8
    target 129
  ]
  edge [
    source 8
    target 130
  ]
  edge [
    source 8
    target 131
  ]
  edge [
    source 8
    target 132
  ]
  edge [
    source 8
    target 133
  ]
  edge [
    source 8
    target 134
  ]
  edge [
    source 8
    target 135
  ]
  edge [
    source 8
    target 136
  ]
  edge [
    source 8
    target 137
  ]
  edge [
    source 8
    target 138
  ]
  edge [
    source 8
    target 139
  ]
]
