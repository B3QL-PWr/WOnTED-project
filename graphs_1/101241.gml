graph [
  maxDegree 37
  minDegree 1
  meanDegree 2.2133333333333334
  density 0.02990990990990991
  graphCliqueNumber 5
  node [
    id 0
    label "nowa"
    origin "text"
  ]
  node [
    id 1
    label "polityk"
    origin "text"
  ]
  node [
    id 2
    label "ekonomiczny"
    origin "text"
  ]
  node [
    id 3
    label "gwiazda"
  ]
  node [
    id 4
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 5
    label "J&#281;drzejewicz"
  ]
  node [
    id 6
    label "Sto&#322;ypin"
  ]
  node [
    id 7
    label "Nixon"
  ]
  node [
    id 8
    label "Perykles"
  ]
  node [
    id 9
    label "bezpartyjny"
  ]
  node [
    id 10
    label "Gomu&#322;ka"
  ]
  node [
    id 11
    label "Gorbaczow"
  ]
  node [
    id 12
    label "Borel"
  ]
  node [
    id 13
    label "Katon"
  ]
  node [
    id 14
    label "McCarthy"
  ]
  node [
    id 15
    label "Gierek"
  ]
  node [
    id 16
    label "Naser"
  ]
  node [
    id 17
    label "Goebbels"
  ]
  node [
    id 18
    label "Ko&#322;&#322;&#261;taj"
  ]
  node [
    id 19
    label "Jaros&#322;aw_Kaczy&#324;ski"
  ]
  node [
    id 20
    label "de_Gaulle"
  ]
  node [
    id 21
    label "Vladimir_Me&#269;iar"
  ]
  node [
    id 22
    label "Bre&#380;niew"
  ]
  node [
    id 23
    label "Juliusz_Cezar"
  ]
  node [
    id 24
    label "Bierut"
  ]
  node [
    id 25
    label "Kuro&#324;"
  ]
  node [
    id 26
    label "Arafat"
  ]
  node [
    id 27
    label "Fidel_Castro"
  ]
  node [
    id 28
    label "Moczar"
  ]
  node [
    id 29
    label "Miko&#322;ajczyk"
  ]
  node [
    id 30
    label "Korwin"
  ]
  node [
    id 31
    label "dzia&#322;acz"
  ]
  node [
    id 32
    label "Winston_Churchill"
  ]
  node [
    id 33
    label "Leszek_Miller"
  ]
  node [
    id 34
    label "Ziobro"
  ]
  node [
    id 35
    label "Mao"
  ]
  node [
    id 36
    label "Chruszczow"
  ]
  node [
    id 37
    label "Putin"
  ]
  node [
    id 38
    label "Falandysz"
  ]
  node [
    id 39
    label "Metternich"
  ]
  node [
    id 40
    label "ekonomicznie"
  ]
  node [
    id 41
    label "korzystny"
  ]
  node [
    id 42
    label "oszcz&#281;dny"
  ]
  node [
    id 43
    label "Rosja"
  ]
  node [
    id 44
    label "radziecki"
  ]
  node [
    id 45
    label "nowy"
  ]
  node [
    id 46
    label "polityka"
  ]
  node [
    id 47
    label "Nowaja"
  ]
  node [
    id 48
    label "ekonomiczeskaja"
  ]
  node [
    id 49
    label "politika"
  ]
  node [
    id 50
    label "&#1053;&#1086;&#1074;&#1072;&#1103;"
  ]
  node [
    id 51
    label "&#1101;&#1082;&#1086;&#1085;&#1086;&#1084;&#1080;&#1095;&#1077;&#1089;&#1082;&#1072;&#1103;"
  ]
  node [
    id 52
    label "&#1087;&#1086;&#1083;&#1080;&#1090;&#1080;&#1082;&#1072;"
  ]
  node [
    id 53
    label "ksi&#261;&#380;&#281;"
  ]
  node [
    id 54
    label "zjazd"
  ]
  node [
    id 55
    label "rosyjski"
  ]
  node [
    id 56
    label "partia"
  ]
  node [
    id 57
    label "komunistyczny"
  ]
  node [
    id 58
    label "Og&#243;lnorosyjski"
  ]
  node [
    id 59
    label "komitet"
  ]
  node [
    id 60
    label "wykonawczy"
  ]
  node [
    id 61
    label "rada"
  ]
  node [
    id 62
    label "wojna"
  ]
  node [
    id 63
    label "domowy"
  ]
  node [
    id 64
    label "wyspa"
  ]
  node [
    id 65
    label "i"
  ]
  node [
    id 66
    label "&#347;wiatowy"
  ]
  node [
    id 67
    label "rewolucja"
  ]
  node [
    id 68
    label "pa&#378;dziernikowy"
  ]
  node [
    id 69
    label "lutowy"
  ]
  node [
    id 70
    label "rz&#261;d"
  ]
  node [
    id 71
    label "tymczasowy"
  ]
  node [
    id 72
    label "Socjal"
  ]
  node [
    id 73
    label "demokratyczny"
  ]
  node [
    id 74
    label "robotniczy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 62
  ]
  edge [
    source 43
    target 63
  ]
  edge [
    source 43
    target 64
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 49
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 52
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 57
  ]
  edge [
    source 55
    target 72
  ]
  edge [
    source 55
    target 73
  ]
  edge [
    source 55
    target 74
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 72
  ]
  edge [
    source 56
    target 73
  ]
  edge [
    source 56
    target 74
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 60
  ]
  edge [
    source 58
    target 61
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 61
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 64
  ]
  edge [
    source 62
    target 65
  ]
  edge [
    source 62
    target 66
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 67
    target 69
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 72
    target 73
  ]
  edge [
    source 72
    target 74
  ]
  edge [
    source 73
    target 74
  ]
]
