graph [
  maxDegree 51
  minDegree 1
  meanDegree 2.3853503184713376
  density 0.0019006775445986753
  graphCliqueNumber 5
  node [
    id 0
    label "gdyby"
    origin "text"
  ]
  node [
    id 1
    label "tak"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "bill"
    origin "text"
  ]
  node [
    id 4
    label "gates"
    origin "text"
  ]
  node [
    id 5
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 6
    label "dawno"
    origin "text"
  ]
  node [
    id 7
    label "sprzedawa&#263;by"
    origin "text"
  ]
  node [
    id 8
    label "hamburger"
    origin "text"
  ]
  node [
    id 9
    label "zaplecze"
    origin "text"
  ]
  node [
    id 10
    label "sma&#380;y&#263;by"
    origin "text"
  ]
  node [
    id 11
    label "steve"
    origin "text"
  ]
  node [
    id 12
    label "jobs"
    origin "text"
  ]
  node [
    id 13
    label "licencja"
    origin "text"
  ]
  node [
    id 14
    label "nic"
    origin "text"
  ]
  node [
    id 15
    label "wprawdzie"
    origin "text"
  ]
  node [
    id 16
    label "kosztowa&#263;"
    origin "text"
  ]
  node [
    id 17
    label "ale"
    origin "text"
  ]
  node [
    id 18
    label "firma"
    origin "text"
  ]
  node [
    id 19
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 20
    label "liczy&#263;"
    origin "text"
  ]
  node [
    id 21
    label "reda"
    origin "text"
  ]
  node [
    id 22
    label "hat"
    origin "text"
  ]
  node [
    id 23
    label "patrze&#263;"
    origin "text"
  ]
  node [
    id 24
    label "tylko"
    origin "text"
  ]
  node [
    id 25
    label "metka"
    origin "text"
  ]
  node [
    id 26
    label "rz&#261;dzi&#263;"
    origin "text"
  ]
  node [
    id 27
    label "tco"
    origin "text"
  ]
  node [
    id 28
    label "sum"
    origin "text"
  ]
  node [
    id 29
    label "wszyscy"
    origin "text"
  ]
  node [
    id 30
    label "element"
    origin "text"
  ]
  node [
    id 31
    label "koszt"
    origin "text"
  ]
  node [
    id 32
    label "szkolenie"
    origin "text"
  ]
  node [
    id 33
    label "przenosi&#263;"
    origin "text"
  ]
  node [
    id 34
    label "dana"
    origin "text"
  ]
  node [
    id 35
    label "r&#243;&#380;ny"
    origin "text"
  ]
  node [
    id 36
    label "badanie"
    origin "text"
  ]
  node [
    id 37
    label "wychodzi&#263;"
    origin "text"
  ]
  node [
    id 38
    label "wynik"
    origin "text"
  ]
  node [
    id 39
    label "zale&#380;ny"
    origin "text"
  ]
  node [
    id 40
    label "przed"
    origin "text"
  ]
  node [
    id 41
    label "kto"
    origin "text"
  ]
  node [
    id 42
    label "badan"
    origin "text"
  ]
  node [
    id 43
    label "zam&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 44
    label "wolne"
    origin "text"
  ]
  node [
    id 45
    label "oprogramowanie"
    origin "text"
  ]
  node [
    id 46
    label "cz&#281;sto"
    origin "text"
  ]
  node [
    id 47
    label "traci&#263;"
    origin "text"
  ]
  node [
    id 48
    label "zmiana"
    origin "text"
  ]
  node [
    id 49
    label "przyzwyczajenie"
    origin "text"
  ]
  node [
    id 50
    label "u&#380;ytkownik"
    origin "text"
  ]
  node [
    id 51
    label "whitehurst"
    origin "text"
  ]
  node [
    id 52
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 53
    label "wywiad"
    origin "text"
  ]
  node [
    id 54
    label "niezale&#380;no&#347;&#263;"
    origin "text"
  ]
  node [
    id 55
    label "jeden"
    origin "text"
  ]
  node [
    id 56
    label "producent"
    origin "text"
  ]
  node [
    id 57
    label "mo&#380;liwo&#347;ci"
    origin "text"
  ]
  node [
    id 58
    label "kod"
    origin "text"
  ]
  node [
    id 59
    label "serwowa&#263;"
    origin "text"
  ]
  node [
    id 60
    label "tradycyjny"
    origin "text"
  ]
  node [
    id 61
    label "porcja"
    origin "text"
  ]
  node [
    id 62
    label "frazes"
    origin "text"
  ]
  node [
    id 63
    label "poziom"
    origin "text"
  ]
  node [
    id 64
    label "wielki"
    origin "text"
  ]
  node [
    id 65
    label "klient"
    origin "text"
  ]
  node [
    id 66
    label "korporacyjny"
    origin "text"
  ]
  node [
    id 67
    label "zale&#380;no&#347;&#263;"
    origin "text"
  ]
  node [
    id 68
    label "wsp&#243;&#322;zale&#380;no&#347;&#263;"
    origin "text"
  ]
  node [
    id 69
    label "bywa&#263;"
    origin "text"
  ]
  node [
    id 70
    label "dost&#281;pny"
    origin "text"
  ]
  node [
    id 71
    label "zn&#243;w"
    origin "text"
  ]
  node [
    id 72
    label "&#380;eby"
    origin "text"
  ]
  node [
    id 73
    label "da&#263;"
    origin "text"
  ]
  node [
    id 74
    label "si&#281;"
    origin "text"
  ]
  node [
    id 75
    label "przekona&#263;"
    origin "text"
  ]
  node [
    id 76
    label "tym"
    origin "text"
  ]
  node [
    id 77
    label "argument"
    origin "text"
  ]
  node [
    id 78
    label "trzeba"
    origin "text"
  ]
  node [
    id 79
    label "czeka&#263;"
    origin "text"
  ]
  node [
    id 80
    label "kryzys"
    origin "text"
  ]
  node [
    id 81
    label "gdzie"
    origin "text"
  ]
  node [
    id 82
    label "ten"
    origin "text"
  ]
  node [
    id 83
    label "optymizm"
    origin "text"
  ]
  node [
    id 84
    label "chyba"
    origin "text"
  ]
  node [
    id 85
    label "tam"
    origin "text"
  ]
  node [
    id 86
    label "keen"
    origin "text"
  ]
  node [
    id 87
    label "widzie&#263;"
    origin "text"
  ]
  node [
    id 88
    label "&#378;r&#243;d&#322;o"
    origin "text"
  ]
  node [
    id 89
    label "sukces"
    origin "text"
  ]
  node [
    id 90
    label "produkcja"
    origin "text"
  ]
  node [
    id 91
    label "partnerski"
    origin "text"
  ]
  node [
    id 92
    label "czas"
    origin "text"
  ]
  node [
    id 93
    label "dobrobyt"
    origin "text"
  ]
  node [
    id 94
    label "brak"
    origin "text"
  ]
  node [
    id 95
    label "szacunek"
    origin "text"
  ]
  node [
    id 96
    label "dla"
    origin "text"
  ]
  node [
    id 97
    label "ludzki"
    origin "text"
  ]
  node [
    id 98
    label "praca"
    origin "text"
  ]
  node [
    id 99
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 100
    label "przyj&#261;&#263;"
    origin "text"
  ]
  node [
    id 101
    label "forma"
    origin "text"
  ]
  node [
    id 102
    label "zwi&#281;ksza&#263;"
    origin "text"
  ]
  node [
    id 103
    label "obowi&#261;zek"
    origin "text"
  ]
  node [
    id 104
    label "bez"
    origin "text"
  ]
  node [
    id 105
    label "wynagrodzenie"
    origin "text"
  ]
  node [
    id 106
    label "trudno"
    origin "text"
  ]
  node [
    id 107
    label "pracownik"
    origin "text"
  ]
  node [
    id 108
    label "cokolwiek"
    origin "text"
  ]
  node [
    id 109
    label "zmusi&#263;"
    origin "text"
  ]
  node [
    id 110
    label "je&#347;li"
    origin "text"
  ]
  node [
    id 111
    label "odej&#347;&#263;"
    origin "text"
  ]
  node [
    id 112
    label "&#322;atwo"
    origin "text"
  ]
  node [
    id 113
    label "znale&#378;&#263;"
    origin "text"
  ]
  node [
    id 114
    label "inny"
    origin "text"
  ]
  node [
    id 115
    label "niech"
    origin "text"
  ]
  node [
    id 116
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 117
    label "godzina"
    origin "text"
  ]
  node [
    id 118
    label "sam"
    origin "text"
  ]
  node [
    id 119
    label "uczy&#263;"
    origin "text"
  ]
  node [
    id 120
    label "dzia&#322;ania"
    origin "text"
  ]
  node [
    id 121
    label "nowy"
    origin "text"
  ]
  node [
    id 122
    label "program"
    origin "text"
  ]
  node [
    id 123
    label "rozwi&#261;zywa&#263;"
    origin "text"
  ]
  node [
    id 124
    label "problem"
    origin "text"
  ]
  node [
    id 125
    label "jak"
    origin "text"
  ]
  node [
    id 126
    label "przyzwyczai&#263;"
    origin "text"
  ]
  node [
    id 127
    label "zdecydowa&#263;"
    origin "text"
  ]
  node [
    id 128
    label "zaoszcz&#281;dzi&#263;"
    origin "text"
  ]
  node [
    id 129
    label "kilka"
    origin "text"
  ]
  node [
    id 130
    label "st&#243;wa"
    origin "text"
  ]
  node [
    id 131
    label "kupowa&#263;"
    origin "text"
  ]
  node [
    id 132
    label "komputer"
    origin "text"
  ]
  node [
    id 133
    label "office"
    origin "text"
  ]
  node [
    id 134
    label "windows"
    origin "text"
  ]
  node [
    id 135
    label "strona"
    origin "text"
  ]
  node [
    id 136
    label "poda&#380;"
    origin "text"
  ]
  node [
    id 137
    label "straci&#263;"
    origin "text"
  ]
  node [
    id 138
    label "cz&#281;&#347;&#263;"
    origin "text"
  ]
  node [
    id 139
    label "udzia&#322;"
    origin "text"
  ]
  node [
    id 140
    label "projekt"
    origin "text"
  ]
  node [
    id 141
    label "wolny"
    origin "text"
  ]
  node [
    id 142
    label "kultura"
    origin "text"
  ]
  node [
    id 143
    label "zwi&#281;kszy&#263;"
    origin "text"
  ]
  node [
    id 144
    label "popyt"
    origin "text"
  ]
  node [
    id 145
    label "zyska&#263;"
    origin "text"
  ]
  node [
    id 146
    label "nowa"
    origin "text"
  ]
  node [
    id 147
    label "p&#281;kn&#261;&#263;"
    origin "text"
  ]
  node [
    id 148
    label "ba&#324;ka"
    origin "text"
  ]
  node [
    id 149
    label "internetowy"
    origin "text"
  ]
  node [
    id 150
    label "lata"
    origin "text"
  ]
  node [
    id 151
    label "doprowadzi&#263;"
    origin "text"
  ]
  node [
    id 152
    label "porzuci&#263;"
    origin "text"
  ]
  node [
    id 153
    label "drogi"
    origin "text"
  ]
  node [
    id 154
    label "zastrzec"
    origin "text"
  ]
  node [
    id 155
    label "technologia"
    origin "text"
  ]
  node [
    id 156
    label "serwerowy"
    origin "text"
  ]
  node [
    id 157
    label "komercyjny"
    origin "text"
  ]
  node [
    id 158
    label "unix"
    origin "text"
  ]
  node [
    id 159
    label "niestandardowy"
    origin "text"
  ]
  node [
    id 160
    label "sprz&#281;t"
    origin "text"
  ]
  node [
    id 161
    label "rzecz"
    origin "text"
  ]
  node [
    id 162
    label "zwyk&#322;y"
    origin "text"
  ]
  node [
    id 163
    label "pecet"
    origin "text"
  ]
  node [
    id 164
    label "linuksem"
    origin "text"
  ]
  node [
    id 165
    label "apacz"
    origin "text"
  ]
  node [
    id 166
    label "albo"
    origin "text"
  ]
  node [
    id 167
    label "akurat"
    origin "text"
  ]
  node [
    id 168
    label "wa&#380;ny"
    origin "text"
  ]
  node [
    id 169
    label "czego"
    origin "text"
  ]
  node [
    id 170
    label "teraz"
    origin "text"
  ]
  node [
    id 171
    label "si&#281;ga&#263;"
  ]
  node [
    id 172
    label "trwa&#263;"
  ]
  node [
    id 173
    label "obecno&#347;&#263;"
  ]
  node [
    id 174
    label "stan"
  ]
  node [
    id 175
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 176
    label "stand"
  ]
  node [
    id 177
    label "mie&#263;_miejsce"
  ]
  node [
    id 178
    label "uczestniczy&#263;"
  ]
  node [
    id 179
    label "chodzi&#263;"
  ]
  node [
    id 180
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 181
    label "equal"
  ]
  node [
    id 182
    label "dawny"
  ]
  node [
    id 183
    label "ongi&#347;"
  ]
  node [
    id 184
    label "dawnie"
  ]
  node [
    id 185
    label "wcze&#347;niej"
  ]
  node [
    id 186
    label "d&#322;ugotrwale"
  ]
  node [
    id 187
    label "burger"
  ]
  node [
    id 188
    label "fast_food"
  ]
  node [
    id 189
    label "kanapka"
  ]
  node [
    id 190
    label "wyposa&#380;enie"
  ]
  node [
    id 191
    label "infrastruktura"
  ]
  node [
    id 192
    label "pomieszczenie"
  ]
  node [
    id 193
    label "sklep"
  ]
  node [
    id 194
    label "prawo"
  ]
  node [
    id 195
    label "licencjonowa&#263;"
  ]
  node [
    id 196
    label "pozwolenie"
  ]
  node [
    id 197
    label "hodowla"
  ]
  node [
    id 198
    label "rasowy"
  ]
  node [
    id 199
    label "license"
  ]
  node [
    id 200
    label "zezwolenie"
  ]
  node [
    id 201
    label "za&#347;wiadczenie"
  ]
  node [
    id 202
    label "miernota"
  ]
  node [
    id 203
    label "g&#243;wno"
  ]
  node [
    id 204
    label "love"
  ]
  node [
    id 205
    label "ilo&#347;&#263;"
  ]
  node [
    id 206
    label "ciura"
  ]
  node [
    id 207
    label "cena"
  ]
  node [
    id 208
    label "try"
  ]
  node [
    id 209
    label "essay"
  ]
  node [
    id 210
    label "spo&#380;ywa&#263;"
  ]
  node [
    id 211
    label "doznawa&#263;"
  ]
  node [
    id 212
    label "savor"
  ]
  node [
    id 213
    label "piwo"
  ]
  node [
    id 214
    label "cz&#322;owiek"
  ]
  node [
    id 215
    label "MAC"
  ]
  node [
    id 216
    label "Hortex"
  ]
  node [
    id 217
    label "reengineering"
  ]
  node [
    id 218
    label "nazwa_w&#322;asna"
  ]
  node [
    id 219
    label "podmiot_gospodarczy"
  ]
  node [
    id 220
    label "Google"
  ]
  node [
    id 221
    label "zaufanie"
  ]
  node [
    id 222
    label "biurowiec"
  ]
  node [
    id 223
    label "interes"
  ]
  node [
    id 224
    label "zasoby_ludzkie"
  ]
  node [
    id 225
    label "networking"
  ]
  node [
    id 226
    label "paczkarnia"
  ]
  node [
    id 227
    label "Canon"
  ]
  node [
    id 228
    label "HP"
  ]
  node [
    id 229
    label "Baltona"
  ]
  node [
    id 230
    label "Pewex"
  ]
  node [
    id 231
    label "MAN_SE"
  ]
  node [
    id 232
    label "Apeks"
  ]
  node [
    id 233
    label "zasoby"
  ]
  node [
    id 234
    label "Orbis"
  ]
  node [
    id 235
    label "miejsce_pracy"
  ]
  node [
    id 236
    label "siedziba"
  ]
  node [
    id 237
    label "Spo&#322;em"
  ]
  node [
    id 238
    label "przedsi&#281;biorczo&#347;&#263;"
  ]
  node [
    id 239
    label "Orlen"
  ]
  node [
    id 240
    label "klasa"
  ]
  node [
    id 241
    label "report"
  ]
  node [
    id 242
    label "dodawa&#263;"
  ]
  node [
    id 243
    label "wymienia&#263;"
  ]
  node [
    id 244
    label "okre&#347;la&#263;"
  ]
  node [
    id 245
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 246
    label "dyskalkulia"
  ]
  node [
    id 247
    label "admit"
  ]
  node [
    id 248
    label "osi&#261;ga&#263;"
  ]
  node [
    id 249
    label "wyznacza&#263;"
  ]
  node [
    id 250
    label "posiada&#263;"
  ]
  node [
    id 251
    label "mierzy&#263;"
  ]
  node [
    id 252
    label "odlicza&#263;"
  ]
  node [
    id 253
    label "bra&#263;"
  ]
  node [
    id 254
    label "wycenia&#263;"
  ]
  node [
    id 255
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 256
    label "rachowa&#263;"
  ]
  node [
    id 257
    label "tell"
  ]
  node [
    id 258
    label "uwzgl&#281;dnia&#263;"
  ]
  node [
    id 259
    label "policza&#263;"
  ]
  node [
    id 260
    label "count"
  ]
  node [
    id 261
    label "akwatorium"
  ]
  node [
    id 262
    label "morze"
  ]
  node [
    id 263
    label "falochron"
  ]
  node [
    id 264
    label "koso"
  ]
  node [
    id 265
    label "szuka&#263;"
  ]
  node [
    id 266
    label "go_steady"
  ]
  node [
    id 267
    label "dba&#263;"
  ]
  node [
    id 268
    label "traktowa&#263;"
  ]
  node [
    id 269
    label "os&#261;dza&#263;"
  ]
  node [
    id 270
    label "punkt_widzenia"
  ]
  node [
    id 271
    label "robi&#263;"
  ]
  node [
    id 272
    label "uwa&#380;a&#263;"
  ]
  node [
    id 273
    label "look"
  ]
  node [
    id 274
    label "pogl&#261;da&#263;"
  ]
  node [
    id 275
    label "przygl&#261;da&#263;_si&#281;"
  ]
  node [
    id 276
    label "label"
  ]
  node [
    id 277
    label "kartonik"
  ]
  node [
    id 278
    label "informacja"
  ]
  node [
    id 279
    label "kawa&#322;ek"
  ]
  node [
    id 280
    label "kie&#322;basa"
  ]
  node [
    id 281
    label "towar"
  ]
  node [
    id 282
    label "dokazywa&#263;"
  ]
  node [
    id 283
    label "control"
  ]
  node [
    id 284
    label "dzier&#380;e&#263;"
  ]
  node [
    id 285
    label "sprawowa&#263;"
  ]
  node [
    id 286
    label "g&#243;rowa&#263;"
  ]
  node [
    id 287
    label "w&#322;adza"
  ]
  node [
    id 288
    label "manipulate"
  ]
  node [
    id 289
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 290
    label "warunkowa&#263;"
  ]
  node [
    id 291
    label "sumowate"
  ]
  node [
    id 292
    label "Uzbekistan"
  ]
  node [
    id 293
    label "catfish"
  ]
  node [
    id 294
    label "ryba"
  ]
  node [
    id 295
    label "jednostka_monetarna"
  ]
  node [
    id 296
    label "szkodnik"
  ]
  node [
    id 297
    label "&#347;rodowisko"
  ]
  node [
    id 298
    label "component"
  ]
  node [
    id 299
    label "p&#243;&#322;&#347;wiatek"
  ]
  node [
    id 300
    label "r&#243;&#380;niczka"
  ]
  node [
    id 301
    label "przedmiot"
  ]
  node [
    id 302
    label "typ_spod_ciemnej_gwiazdy"
  ]
  node [
    id 303
    label "gangsterski"
  ]
  node [
    id 304
    label "szambo"
  ]
  node [
    id 305
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 306
    label "materia"
  ]
  node [
    id 307
    label "aspo&#322;eczny"
  ]
  node [
    id 308
    label "poj&#281;cie"
  ]
  node [
    id 309
    label "underworld"
  ]
  node [
    id 310
    label "nak&#322;ad"
  ]
  node [
    id 311
    label "rachunek_koszt&#243;w"
  ]
  node [
    id 312
    label "sumpt"
  ]
  node [
    id 313
    label "wydatek"
  ]
  node [
    id 314
    label "zapoznawanie"
  ]
  node [
    id 315
    label "seria"
  ]
  node [
    id 316
    label "course"
  ]
  node [
    id 317
    label "training"
  ]
  node [
    id 318
    label "zaj&#281;cia"
  ]
  node [
    id 319
    label "Lira"
  ]
  node [
    id 320
    label "pomaganie"
  ]
  node [
    id 321
    label "o&#347;wiecanie"
  ]
  node [
    id 322
    label "kliker"
  ]
  node [
    id 323
    label "pouczenie"
  ]
  node [
    id 324
    label "nauka"
  ]
  node [
    id 325
    label "infest"
  ]
  node [
    id 326
    label "zmienia&#263;"
  ]
  node [
    id 327
    label "wytrzyma&#263;"
  ]
  node [
    id 328
    label "dostosowywa&#263;"
  ]
  node [
    id 329
    label "rozpowszechnia&#263;"
  ]
  node [
    id 330
    label "ponosi&#263;"
  ]
  node [
    id 331
    label "przemieszcza&#263;"
  ]
  node [
    id 332
    label "move"
  ]
  node [
    id 333
    label "przelatywa&#263;"
  ]
  node [
    id 334
    label "strzela&#263;"
  ]
  node [
    id 335
    label "kopiowa&#263;"
  ]
  node [
    id 336
    label "transfer"
  ]
  node [
    id 337
    label "umieszcza&#263;"
  ]
  node [
    id 338
    label "pocisk"
  ]
  node [
    id 339
    label "circulate"
  ]
  node [
    id 340
    label "estrange"
  ]
  node [
    id 341
    label "go"
  ]
  node [
    id 342
    label "rozprzestrzenia&#263;"
  ]
  node [
    id 343
    label "dar"
  ]
  node [
    id 344
    label "cnota"
  ]
  node [
    id 345
    label "buddyzm"
  ]
  node [
    id 346
    label "r&#243;&#380;nie"
  ]
  node [
    id 347
    label "jaki&#347;"
  ]
  node [
    id 348
    label "usi&#322;owanie"
  ]
  node [
    id 349
    label "examination"
  ]
  node [
    id 350
    label "investigation"
  ]
  node [
    id 351
    label "ustalenie"
  ]
  node [
    id 352
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 353
    label "ustalanie"
  ]
  node [
    id 354
    label "bia&#322;a_niedziela"
  ]
  node [
    id 355
    label "analysis"
  ]
  node [
    id 356
    label "rozpatrywanie"
  ]
  node [
    id 357
    label "wziernikowanie"
  ]
  node [
    id 358
    label "obserwowanie"
  ]
  node [
    id 359
    label "omawianie"
  ]
  node [
    id 360
    label "sprawdzanie"
  ]
  node [
    id 361
    label "udowadnianie"
  ]
  node [
    id 362
    label "diagnostyka"
  ]
  node [
    id 363
    label "czynno&#347;&#263;"
  ]
  node [
    id 364
    label "macanie"
  ]
  node [
    id 365
    label "rektalny"
  ]
  node [
    id 366
    label "penetrowanie"
  ]
  node [
    id 367
    label "krytykowanie"
  ]
  node [
    id 368
    label "kontrola"
  ]
  node [
    id 369
    label "dociekanie"
  ]
  node [
    id 370
    label "zrecenzowanie"
  ]
  node [
    id 371
    label "rezultat"
  ]
  node [
    id 372
    label "uzyskiwa&#263;"
  ]
  node [
    id 373
    label "impart"
  ]
  node [
    id 374
    label "proceed"
  ]
  node [
    id 375
    label "blend"
  ]
  node [
    id 376
    label "give"
  ]
  node [
    id 377
    label "ograniczenie"
  ]
  node [
    id 378
    label "wydostawa&#263;_si&#281;"
  ]
  node [
    id 379
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 380
    label "za&#322;atwi&#263;"
  ]
  node [
    id 381
    label "schodzi&#263;"
  ]
  node [
    id 382
    label "gra&#263;"
  ]
  node [
    id 383
    label "okazywa&#263;_si&#281;"
  ]
  node [
    id 384
    label "seclude"
  ]
  node [
    id 385
    label "strona_&#347;wiata"
  ]
  node [
    id 386
    label "przedk&#322;ada&#263;"
  ]
  node [
    id 387
    label "przedstawia&#263;"
  ]
  node [
    id 388
    label "appear"
  ]
  node [
    id 389
    label "publish"
  ]
  node [
    id 390
    label "ko&#324;czy&#263;"
  ]
  node [
    id 391
    label "wypada&#263;"
  ]
  node [
    id 392
    label "pochodzi&#263;"
  ]
  node [
    id 393
    label "ukazywa&#263;_si&#281;_drukiem"
  ]
  node [
    id 394
    label "uwalnia&#263;_si&#281;"
  ]
  node [
    id 395
    label "wygl&#261;da&#263;"
  ]
  node [
    id 396
    label "opuszcza&#263;"
  ]
  node [
    id 397
    label "wystarcza&#263;"
  ]
  node [
    id 398
    label "wyrusza&#263;"
  ]
  node [
    id 399
    label "perform"
  ]
  node [
    id 400
    label "heighten"
  ]
  node [
    id 401
    label "typ"
  ]
  node [
    id 402
    label "dzia&#322;anie"
  ]
  node [
    id 403
    label "przyczyna"
  ]
  node [
    id 404
    label "zaokr&#261;gli&#263;"
  ]
  node [
    id 405
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 406
    label "zaokr&#261;glenie"
  ]
  node [
    id 407
    label "event"
  ]
  node [
    id 408
    label "uzale&#380;nienie_si&#281;"
  ]
  node [
    id 409
    label "uzale&#380;nianie_si&#281;"
  ]
  node [
    id 410
    label "uzale&#380;nianie"
  ]
  node [
    id 411
    label "zale&#380;nie"
  ]
  node [
    id 412
    label "uzale&#380;nienie"
  ]
  node [
    id 413
    label "poleci&#263;"
  ]
  node [
    id 414
    label "zarezerwowa&#263;"
  ]
  node [
    id 415
    label "zamawianie"
  ]
  node [
    id 416
    label "zaczarowa&#263;"
  ]
  node [
    id 417
    label "order"
  ]
  node [
    id 418
    label "zam&#243;wienie"
  ]
  node [
    id 419
    label "bespeak"
  ]
  node [
    id 420
    label "indent"
  ]
  node [
    id 421
    label "indenture"
  ]
  node [
    id 422
    label "zamawia&#263;"
  ]
  node [
    id 423
    label "zg&#322;osi&#263;"
  ]
  node [
    id 424
    label "appoint"
  ]
  node [
    id 425
    label "um&#243;wi&#263;_si&#281;"
  ]
  node [
    id 426
    label "zleci&#263;"
  ]
  node [
    id 427
    label "czas_wolny"
  ]
  node [
    id 428
    label "zbi&#243;r"
  ]
  node [
    id 429
    label "cz&#281;stokro&#263;"
  ]
  node [
    id 430
    label "cz&#281;sty"
  ]
  node [
    id 431
    label "omija&#263;"
  ]
  node [
    id 432
    label "forfeit"
  ]
  node [
    id 433
    label "przegrywa&#263;"
  ]
  node [
    id 434
    label "execute"
  ]
  node [
    id 435
    label "zabija&#263;"
  ]
  node [
    id 436
    label "wytraca&#263;"
  ]
  node [
    id 437
    label "pogarsza&#263;_si&#281;"
  ]
  node [
    id 438
    label "szasta&#263;"
  ]
  node [
    id 439
    label "anatomopatolog"
  ]
  node [
    id 440
    label "rewizja"
  ]
  node [
    id 441
    label "oznaka"
  ]
  node [
    id 442
    label "ferment"
  ]
  node [
    id 443
    label "komplet"
  ]
  node [
    id 444
    label "tura"
  ]
  node [
    id 445
    label "amendment"
  ]
  node [
    id 446
    label "zmianka"
  ]
  node [
    id 447
    label "odmienianie"
  ]
  node [
    id 448
    label "passage"
  ]
  node [
    id 449
    label "zjawisko"
  ]
  node [
    id 450
    label "change"
  ]
  node [
    id 451
    label "zwyczaj"
  ]
  node [
    id 452
    label "oddzia&#322;anie"
  ]
  node [
    id 453
    label "nawyknienie"
  ]
  node [
    id 454
    label "use"
  ]
  node [
    id 455
    label "j&#281;zykowo"
  ]
  node [
    id 456
    label "podmiot"
  ]
  node [
    id 457
    label "remark"
  ]
  node [
    id 458
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 459
    label "u&#380;ywa&#263;"
  ]
  node [
    id 460
    label "j&#281;zyk"
  ]
  node [
    id 461
    label "say"
  ]
  node [
    id 462
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 463
    label "formu&#322;owa&#263;"
  ]
  node [
    id 464
    label "talk"
  ]
  node [
    id 465
    label "powiada&#263;"
  ]
  node [
    id 466
    label "informowa&#263;"
  ]
  node [
    id 467
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 468
    label "wydobywa&#263;"
  ]
  node [
    id 469
    label "express"
  ]
  node [
    id 470
    label "chew_the_fat"
  ]
  node [
    id 471
    label "dysfonia"
  ]
  node [
    id 472
    label "umie&#263;"
  ]
  node [
    id 473
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 474
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 475
    label "wyra&#380;a&#263;"
  ]
  node [
    id 476
    label "gaworzy&#263;"
  ]
  node [
    id 477
    label "rozmawia&#263;"
  ]
  node [
    id 478
    label "dziama&#263;"
  ]
  node [
    id 479
    label "prawi&#263;"
  ]
  node [
    id 480
    label "rozmowa"
  ]
  node [
    id 481
    label "sonda&#380;"
  ]
  node [
    id 482
    label "autoryzowanie"
  ]
  node [
    id 483
    label "autoryzowa&#263;"
  ]
  node [
    id 484
    label "s&#322;u&#380;by_specjalne"
  ]
  node [
    id 485
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 486
    label "agent"
  ]
  node [
    id 487
    label "inquiry"
  ]
  node [
    id 488
    label "consultation"
  ]
  node [
    id 489
    label "s&#322;u&#380;ba"
  ]
  node [
    id 490
    label "status"
  ]
  node [
    id 491
    label "relacja"
  ]
  node [
    id 492
    label "independence"
  ]
  node [
    id 493
    label "charakter"
  ]
  node [
    id 494
    label "kieliszek"
  ]
  node [
    id 495
    label "shot"
  ]
  node [
    id 496
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 497
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 498
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 499
    label "jednolicie"
  ]
  node [
    id 500
    label "w&#243;dka"
  ]
  node [
    id 501
    label "ujednolicenie"
  ]
  node [
    id 502
    label "jednakowy"
  ]
  node [
    id 503
    label "rynek"
  ]
  node [
    id 504
    label "&#322;a&#324;cuch_pokarmowy"
  ]
  node [
    id 505
    label "manufacturer"
  ]
  node [
    id 506
    label "bran&#380;owiec"
  ]
  node [
    id 507
    label "artel"
  ]
  node [
    id 508
    label "filmowiec"
  ]
  node [
    id 509
    label "muzyk"
  ]
  node [
    id 510
    label "wykonawca"
  ]
  node [
    id 511
    label "autotrof"
  ]
  node [
    id 512
    label "Wedel"
  ]
  node [
    id 513
    label "capability"
  ]
  node [
    id 514
    label "zdolno&#347;&#263;"
  ]
  node [
    id 515
    label "potencja&#322;"
  ]
  node [
    id 516
    label "language"
  ]
  node [
    id 517
    label "code"
  ]
  node [
    id 518
    label "ci&#261;g"
  ]
  node [
    id 519
    label "szablon"
  ]
  node [
    id 520
    label "szyfrowanie"
  ]
  node [
    id 521
    label "struktura"
  ]
  node [
    id 522
    label "faszerowa&#263;"
  ]
  node [
    id 523
    label "jedzenie"
  ]
  node [
    id 524
    label "deal"
  ]
  node [
    id 525
    label "zaczyna&#263;"
  ]
  node [
    id 526
    label "pi&#322;ka"
  ]
  node [
    id 527
    label "kelner"
  ]
  node [
    id 528
    label "podawa&#263;"
  ]
  node [
    id 529
    label "stawia&#263;"
  ]
  node [
    id 530
    label "nienowoczesny"
  ]
  node [
    id 531
    label "zachowawczy"
  ]
  node [
    id 532
    label "zwyczajowy"
  ]
  node [
    id 533
    label "modelowy"
  ]
  node [
    id 534
    label "przyj&#281;ty"
  ]
  node [
    id 535
    label "wierny"
  ]
  node [
    id 536
    label "tradycyjnie"
  ]
  node [
    id 537
    label "surowy"
  ]
  node [
    id 538
    label "&#380;o&#322;d"
  ]
  node [
    id 539
    label "zas&#243;b"
  ]
  node [
    id 540
    label "trywializm"
  ]
  node [
    id 541
    label "wypowied&#378;"
  ]
  node [
    id 542
    label "wysoko&#347;&#263;"
  ]
  node [
    id 543
    label "faza"
  ]
  node [
    id 544
    label "szczebel"
  ]
  node [
    id 545
    label "po&#322;o&#380;enie"
  ]
  node [
    id 546
    label "kierunek"
  ]
  node [
    id 547
    label "wyk&#322;adnik"
  ]
  node [
    id 548
    label "budynek"
  ]
  node [
    id 549
    label "jako&#347;&#263;"
  ]
  node [
    id 550
    label "ranga"
  ]
  node [
    id 551
    label "p&#322;aszczyzna"
  ]
  node [
    id 552
    label "dupny"
  ]
  node [
    id 553
    label "wysoce"
  ]
  node [
    id 554
    label "wyj&#261;tkowy"
  ]
  node [
    id 555
    label "wybitny"
  ]
  node [
    id 556
    label "znaczny"
  ]
  node [
    id 557
    label "prawdziwy"
  ]
  node [
    id 558
    label "nieprzeci&#281;tny"
  ]
  node [
    id 559
    label "bratek"
  ]
  node [
    id 560
    label "klientela"
  ]
  node [
    id 561
    label "szlachcic"
  ]
  node [
    id 562
    label "agent_rozliczeniowy"
  ]
  node [
    id 563
    label "komputer_cyfrowy"
  ]
  node [
    id 564
    label "us&#322;ugobiorca"
  ]
  node [
    id 565
    label "Rzymianin"
  ]
  node [
    id 566
    label "obywatel"
  ]
  node [
    id 567
    label "zwi&#261;zek"
  ]
  node [
    id 568
    label "relatywizowa&#263;"
  ]
  node [
    id 569
    label "relatywizowanie"
  ]
  node [
    id 570
    label "zrelatywizowanie"
  ]
  node [
    id 571
    label "podporz&#261;dkowanie"
  ]
  node [
    id 572
    label "zrelatywizowa&#263;"
  ]
  node [
    id 573
    label "niesamodzielno&#347;&#263;"
  ]
  node [
    id 574
    label "mutuality"
  ]
  node [
    id 575
    label "pokazywa&#263;_si&#281;"
  ]
  node [
    id 576
    label "zdarza&#263;_si&#281;"
  ]
  node [
    id 577
    label "pozosta&#263;"
  ]
  node [
    id 578
    label "&#380;egna&#263;"
  ]
  node [
    id 579
    label "&#322;atwy"
  ]
  node [
    id 580
    label "mo&#380;liwy"
  ]
  node [
    id 581
    label "dost&#281;pnie"
  ]
  node [
    id 582
    label "ods&#322;oni&#281;cie_si&#281;"
  ]
  node [
    id 583
    label "przyst&#281;pnie"
  ]
  node [
    id 584
    label "zrozumia&#322;y"
  ]
  node [
    id 585
    label "odblokowywanie_si&#281;"
  ]
  node [
    id 586
    label "odblokowanie_si&#281;"
  ]
  node [
    id 587
    label "ods&#322;anianie_si&#281;"
  ]
  node [
    id 588
    label "dostarczy&#263;"
  ]
  node [
    id 589
    label "obieca&#263;"
  ]
  node [
    id 590
    label "pozwoli&#263;"
  ]
  node [
    id 591
    label "przeznaczy&#263;"
  ]
  node [
    id 592
    label "doda&#263;"
  ]
  node [
    id 593
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 594
    label "wyrzec_si&#281;"
  ]
  node [
    id 595
    label "supply"
  ]
  node [
    id 596
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 597
    label "zada&#263;"
  ]
  node [
    id 598
    label "odst&#261;pi&#263;"
  ]
  node [
    id 599
    label "feed"
  ]
  node [
    id 600
    label "testify"
  ]
  node [
    id 601
    label "powierzy&#263;"
  ]
  node [
    id 602
    label "convey"
  ]
  node [
    id 603
    label "przekaza&#263;"
  ]
  node [
    id 604
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 605
    label "zap&#322;aci&#263;"
  ]
  node [
    id 606
    label "dress"
  ]
  node [
    id 607
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 608
    label "udost&#281;pni&#263;"
  ]
  node [
    id 609
    label "sztachn&#261;&#263;"
  ]
  node [
    id 610
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 611
    label "zrobi&#263;"
  ]
  node [
    id 612
    label "przywali&#263;"
  ]
  node [
    id 613
    label "rap"
  ]
  node [
    id 614
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 615
    label "picture"
  ]
  node [
    id 616
    label "get"
  ]
  node [
    id 617
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 618
    label "nak&#322;oni&#263;"
  ]
  node [
    id 619
    label "s&#261;d"
  ]
  node [
    id 620
    label "argumentacja"
  ]
  node [
    id 621
    label "parametr"
  ]
  node [
    id 622
    label "dow&#243;d"
  ]
  node [
    id 623
    label "operand"
  ]
  node [
    id 624
    label "zmienna"
  ]
  node [
    id 625
    label "trza"
  ]
  node [
    id 626
    label "necessity"
  ]
  node [
    id 627
    label "hold"
  ]
  node [
    id 628
    label "sp&#281;dza&#263;"
  ]
  node [
    id 629
    label "decydowa&#263;"
  ]
  node [
    id 630
    label "oczekiwa&#263;"
  ]
  node [
    id 631
    label "pauzowa&#263;"
  ]
  node [
    id 632
    label "anticipate"
  ]
  node [
    id 633
    label "schorzenie"
  ]
  node [
    id 634
    label "Marzec_'68"
  ]
  node [
    id 635
    label "zwrot"
  ]
  node [
    id 636
    label "cykl_koniunkturalny"
  ]
  node [
    id 637
    label "k&#322;opot"
  ]
  node [
    id 638
    label "head"
  ]
  node [
    id 639
    label "July"
  ]
  node [
    id 640
    label "pogorszenie"
  ]
  node [
    id 641
    label "sytuacja"
  ]
  node [
    id 642
    label "okre&#347;lony"
  ]
  node [
    id 643
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 644
    label "postawa"
  ]
  node [
    id 645
    label "buoyancy"
  ]
  node [
    id 646
    label "tu"
  ]
  node [
    id 647
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 648
    label "perceive"
  ]
  node [
    id 649
    label "reagowa&#263;"
  ]
  node [
    id 650
    label "spowodowa&#263;"
  ]
  node [
    id 651
    label "male&#263;"
  ]
  node [
    id 652
    label "zmale&#263;"
  ]
  node [
    id 653
    label "spotka&#263;"
  ]
  node [
    id 654
    label "dostrzega&#263;"
  ]
  node [
    id 655
    label "zwi&#281;ksza&#263;_si&#281;"
  ]
  node [
    id 656
    label "zdawa&#263;_sobie_spraw&#281;"
  ]
  node [
    id 657
    label "ogl&#261;da&#263;"
  ]
  node [
    id 658
    label "aprobowa&#263;"
  ]
  node [
    id 659
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 660
    label "wzrok"
  ]
  node [
    id 661
    label "postrzega&#263;"
  ]
  node [
    id 662
    label "notice"
  ]
  node [
    id 663
    label "bra&#263;_si&#281;"
  ]
  node [
    id 664
    label "&#347;wiadectwo"
  ]
  node [
    id 665
    label "matuszka"
  ]
  node [
    id 666
    label "geneza"
  ]
  node [
    id 667
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 668
    label "kamena"
  ]
  node [
    id 669
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 670
    label "czynnik"
  ]
  node [
    id 671
    label "pocz&#261;tek"
  ]
  node [
    id 672
    label "poci&#261;ganie"
  ]
  node [
    id 673
    label "ciek_wodny"
  ]
  node [
    id 674
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 675
    label "subject"
  ]
  node [
    id 676
    label "success"
  ]
  node [
    id 677
    label "passa"
  ]
  node [
    id 678
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 679
    label "kobieta_sukcesu"
  ]
  node [
    id 680
    label "tingel-tangel"
  ]
  node [
    id 681
    label "odtworzenie"
  ]
  node [
    id 682
    label "wydawa&#263;"
  ]
  node [
    id 683
    label "realizacja"
  ]
  node [
    id 684
    label "monta&#380;"
  ]
  node [
    id 685
    label "rozw&#243;j"
  ]
  node [
    id 686
    label "fabrication"
  ]
  node [
    id 687
    label "kreacja"
  ]
  node [
    id 688
    label "uzysk"
  ]
  node [
    id 689
    label "dorobek"
  ]
  node [
    id 690
    label "wyda&#263;"
  ]
  node [
    id 691
    label "impreza"
  ]
  node [
    id 692
    label "postprodukcja"
  ]
  node [
    id 693
    label "numer"
  ]
  node [
    id 694
    label "kooperowa&#263;"
  ]
  node [
    id 695
    label "creation"
  ]
  node [
    id 696
    label "trema"
  ]
  node [
    id 697
    label "product"
  ]
  node [
    id 698
    label "performance"
  ]
  node [
    id 699
    label "wsp&#243;&#322;pracowanie"
  ]
  node [
    id 700
    label "po_partnersku"
  ]
  node [
    id 701
    label "wsp&#243;lny"
  ]
  node [
    id 702
    label "partnersko"
  ]
  node [
    id 703
    label "czasokres"
  ]
  node [
    id 704
    label "trawienie"
  ]
  node [
    id 705
    label "kategoria_gramatyczna"
  ]
  node [
    id 706
    label "period"
  ]
  node [
    id 707
    label "odczyt"
  ]
  node [
    id 708
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 709
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 710
    label "chwila"
  ]
  node [
    id 711
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 712
    label "poprzedzenie"
  ]
  node [
    id 713
    label "koniugacja"
  ]
  node [
    id 714
    label "dzieje"
  ]
  node [
    id 715
    label "poprzedzi&#263;"
  ]
  node [
    id 716
    label "przep&#322;ywanie"
  ]
  node [
    id 717
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 718
    label "odwlekanie_si&#281;"
  ]
  node [
    id 719
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 720
    label "Zeitgeist"
  ]
  node [
    id 721
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 722
    label "okres_czasu"
  ]
  node [
    id 723
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 724
    label "schy&#322;ek"
  ]
  node [
    id 725
    label "czwarty_wymiar"
  ]
  node [
    id 726
    label "chronometria"
  ]
  node [
    id 727
    label "poprzedzanie"
  ]
  node [
    id 728
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 729
    label "pogoda"
  ]
  node [
    id 730
    label "zegar"
  ]
  node [
    id 731
    label "trawi&#263;"
  ]
  node [
    id 732
    label "pochodzenie"
  ]
  node [
    id 733
    label "poprzedza&#263;"
  ]
  node [
    id 734
    label "time_period"
  ]
  node [
    id 735
    label "rachuba_czasu"
  ]
  node [
    id 736
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 737
    label "czasoprzestrze&#324;"
  ]
  node [
    id 738
    label "laba"
  ]
  node [
    id 739
    label "fortune"
  ]
  node [
    id 740
    label "cecha"
  ]
  node [
    id 741
    label "z&#322;ote_czasy"
  ]
  node [
    id 742
    label "prywatywny"
  ]
  node [
    id 743
    label "defect"
  ]
  node [
    id 744
    label "odej&#347;cie"
  ]
  node [
    id 745
    label "gap"
  ]
  node [
    id 746
    label "kr&#243;tki"
  ]
  node [
    id 747
    label "wyr&#243;b"
  ]
  node [
    id 748
    label "nieistnienie"
  ]
  node [
    id 749
    label "wada"
  ]
  node [
    id 750
    label "odchodzenie"
  ]
  node [
    id 751
    label "odchodzi&#263;"
  ]
  node [
    id 752
    label "zaimponowanie"
  ]
  node [
    id 753
    label "follow-up"
  ]
  node [
    id 754
    label "szanowa&#263;"
  ]
  node [
    id 755
    label "uhonorowa&#263;"
  ]
  node [
    id 756
    label "honorowanie"
  ]
  node [
    id 757
    label "uszanowa&#263;"
  ]
  node [
    id 758
    label "rewerencja"
  ]
  node [
    id 759
    label "uszanowanie"
  ]
  node [
    id 760
    label "imponowanie"
  ]
  node [
    id 761
    label "uhonorowanie"
  ]
  node [
    id 762
    label "respect"
  ]
  node [
    id 763
    label "przewidzenie"
  ]
  node [
    id 764
    label "honorowa&#263;"
  ]
  node [
    id 765
    label "szacuneczek"
  ]
  node [
    id 766
    label "empatyczny"
  ]
  node [
    id 767
    label "naturalny"
  ]
  node [
    id 768
    label "ludzko"
  ]
  node [
    id 769
    label "po_ludzku"
  ]
  node [
    id 770
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 771
    label "normalny"
  ]
  node [
    id 772
    label "przyzwoity"
  ]
  node [
    id 773
    label "stosunek_pracy"
  ]
  node [
    id 774
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 775
    label "benedykty&#324;ski"
  ]
  node [
    id 776
    label "pracowanie"
  ]
  node [
    id 777
    label "zaw&#243;d"
  ]
  node [
    id 778
    label "kierownictwo"
  ]
  node [
    id 779
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 780
    label "wytw&#243;r"
  ]
  node [
    id 781
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 782
    label "tynkarski"
  ]
  node [
    id 783
    label "czynnik_produkcji"
  ]
  node [
    id 784
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 785
    label "zobowi&#261;zanie"
  ]
  node [
    id 786
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 787
    label "tyrka"
  ]
  node [
    id 788
    label "pracowa&#263;"
  ]
  node [
    id 789
    label "poda&#380;_pracy"
  ]
  node [
    id 790
    label "miejsce"
  ]
  node [
    id 791
    label "zak&#322;ad"
  ]
  node [
    id 792
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 793
    label "najem"
  ]
  node [
    id 794
    label "poch&#322;on&#261;&#263;"
  ]
  node [
    id 795
    label "strike"
  ]
  node [
    id 796
    label "przybra&#263;"
  ]
  node [
    id 797
    label "swallow"
  ]
  node [
    id 798
    label "zobowi&#261;za&#263;_si&#281;"
  ]
  node [
    id 799
    label "odebra&#263;"
  ]
  node [
    id 800
    label "umie&#347;ci&#263;"
  ]
  node [
    id 801
    label "obra&#263;"
  ]
  node [
    id 802
    label "fall"
  ]
  node [
    id 803
    label "wzi&#261;&#263;"
  ]
  node [
    id 804
    label "undertake"
  ]
  node [
    id 805
    label "absorb"
  ]
  node [
    id 806
    label "dopu&#347;ci&#263;"
  ]
  node [
    id 807
    label "receive"
  ]
  node [
    id 808
    label "draw"
  ]
  node [
    id 809
    label "przyj&#281;cie"
  ]
  node [
    id 810
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 811
    label "zgodzi&#263;_si&#281;"
  ]
  node [
    id 812
    label "uzna&#263;"
  ]
  node [
    id 813
    label "wpu&#347;ci&#263;"
  ]
  node [
    id 814
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 815
    label "do&#322;ek"
  ]
  node [
    id 816
    label "formality"
  ]
  node [
    id 817
    label "wz&#243;r"
  ]
  node [
    id 818
    label "kantyzm"
  ]
  node [
    id 819
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 820
    label "ornamentyka"
  ]
  node [
    id 821
    label "odmiana"
  ]
  node [
    id 822
    label "mode"
  ]
  node [
    id 823
    label "style"
  ]
  node [
    id 824
    label "formacja"
  ]
  node [
    id 825
    label "maszyna_drukarska"
  ]
  node [
    id 826
    label "poznanie"
  ]
  node [
    id 827
    label "spirala"
  ]
  node [
    id 828
    label "blaszka"
  ]
  node [
    id 829
    label "linearno&#347;&#263;"
  ]
  node [
    id 830
    label "temat"
  ]
  node [
    id 831
    label "g&#322;owa"
  ]
  node [
    id 832
    label "kielich"
  ]
  node [
    id 833
    label "kszta&#322;t"
  ]
  node [
    id 834
    label "pasmo"
  ]
  node [
    id 835
    label "rdze&#324;"
  ]
  node [
    id 836
    label "leksem"
  ]
  node [
    id 837
    label "dyspozycja"
  ]
  node [
    id 838
    label "wygl&#261;d"
  ]
  node [
    id 839
    label "October"
  ]
  node [
    id 840
    label "zawarto&#347;&#263;"
  ]
  node [
    id 841
    label "gwiazda"
  ]
  node [
    id 842
    label "p&#281;tla"
  ]
  node [
    id 843
    label "p&#322;at"
  ]
  node [
    id 844
    label "arystotelizm"
  ]
  node [
    id 845
    label "obiekt"
  ]
  node [
    id 846
    label "dzie&#322;o"
  ]
  node [
    id 847
    label "naczynie"
  ]
  node [
    id 848
    label "wyra&#380;enie"
  ]
  node [
    id 849
    label "jednostka_systematyczna"
  ]
  node [
    id 850
    label "miniatura"
  ]
  node [
    id 851
    label "morfem"
  ]
  node [
    id 852
    label "posta&#263;"
  ]
  node [
    id 853
    label "increase"
  ]
  node [
    id 854
    label "powinno&#347;&#263;"
  ]
  node [
    id 855
    label "zadanie"
  ]
  node [
    id 856
    label "wym&#243;g"
  ]
  node [
    id 857
    label "duty"
  ]
  node [
    id 858
    label "obarczy&#263;"
  ]
  node [
    id 859
    label "ki&#347;&#263;"
  ]
  node [
    id 860
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 861
    label "krzew"
  ]
  node [
    id 862
    label "pi&#380;maczkowate"
  ]
  node [
    id 863
    label "pestkowiec"
  ]
  node [
    id 864
    label "kwiat"
  ]
  node [
    id 865
    label "owoc"
  ]
  node [
    id 866
    label "oliwkowate"
  ]
  node [
    id 867
    label "ro&#347;lina"
  ]
  node [
    id 868
    label "hy&#263;ka"
  ]
  node [
    id 869
    label "lilac"
  ]
  node [
    id 870
    label "delfinidyna"
  ]
  node [
    id 871
    label "liczenie"
  ]
  node [
    id 872
    label "return"
  ]
  node [
    id 873
    label "doch&#243;d"
  ]
  node [
    id 874
    label "zap&#322;ata"
  ]
  node [
    id 875
    label "wynagrodzenie_brutto"
  ]
  node [
    id 876
    label "ryzyko_niewygas&#322;e"
  ]
  node [
    id 877
    label "koszt_rodzajowy"
  ]
  node [
    id 878
    label "zado&#347;&#263;uczynienie"
  ]
  node [
    id 879
    label "danie"
  ]
  node [
    id 880
    label "policzenie"
  ]
  node [
    id 881
    label "policzy&#263;"
  ]
  node [
    id 882
    label "refund"
  ]
  node [
    id 883
    label "bud&#380;et_domowy"
  ]
  node [
    id 884
    label "pay"
  ]
  node [
    id 885
    label "ordynaria"
  ]
  node [
    id 886
    label "trudny"
  ]
  node [
    id 887
    label "hard"
  ]
  node [
    id 888
    label "cz&#322;owiek_pracy"
  ]
  node [
    id 889
    label "delegowa&#263;"
  ]
  node [
    id 890
    label "s&#322;u&#380;ba_cywilna"
  ]
  node [
    id 891
    label "pracu&#347;"
  ]
  node [
    id 892
    label "delegowanie"
  ]
  node [
    id 893
    label "r&#281;ka"
  ]
  node [
    id 894
    label "salariat"
  ]
  node [
    id 895
    label "co&#347;"
  ]
  node [
    id 896
    label "pop&#281;dzi&#263;_kota"
  ]
  node [
    id 897
    label "zada&#263;_gwa&#322;t"
  ]
  node [
    id 898
    label "force"
  ]
  node [
    id 899
    label "sandbag"
  ]
  node [
    id 900
    label "opu&#347;ci&#263;"
  ]
  node [
    id 901
    label "zrezygnowa&#263;"
  ]
  node [
    id 902
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 903
    label "leave_office"
  ]
  node [
    id 904
    label "retract"
  ]
  node [
    id 905
    label "min&#261;&#263;"
  ]
  node [
    id 906
    label "przesta&#263;"
  ]
  node [
    id 907
    label "odrzut"
  ]
  node [
    id 908
    label "ruszy&#263;"
  ]
  node [
    id 909
    label "drop"
  ]
  node [
    id 910
    label "die"
  ]
  node [
    id 911
    label "oddzieli&#263;_si&#281;"
  ]
  node [
    id 912
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 913
    label "szybko"
  ]
  node [
    id 914
    label "&#322;atwie"
  ]
  node [
    id 915
    label "prosto"
  ]
  node [
    id 916
    label "snadnie"
  ]
  node [
    id 917
    label "przyjemnie"
  ]
  node [
    id 918
    label "&#322;acno"
  ]
  node [
    id 919
    label "odzyska&#263;"
  ]
  node [
    id 920
    label "devise"
  ]
  node [
    id 921
    label "oceni&#263;"
  ]
  node [
    id 922
    label "znaj&#347;&#263;"
  ]
  node [
    id 923
    label "wymy&#347;li&#263;"
  ]
  node [
    id 924
    label "invent"
  ]
  node [
    id 925
    label "pozyska&#263;"
  ]
  node [
    id 926
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 927
    label "wykry&#263;"
  ]
  node [
    id 928
    label "dozna&#263;"
  ]
  node [
    id 929
    label "kolejny"
  ]
  node [
    id 930
    label "inaczej"
  ]
  node [
    id 931
    label "inszy"
  ]
  node [
    id 932
    label "osobno"
  ]
  node [
    id 933
    label "catch"
  ]
  node [
    id 934
    label "osta&#263;_si&#281;"
  ]
  node [
    id 935
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 936
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 937
    label "minuta"
  ]
  node [
    id 938
    label "doba"
  ]
  node [
    id 939
    label "p&#243;&#322;godzina"
  ]
  node [
    id 940
    label "kwadrans"
  ]
  node [
    id 941
    label "time"
  ]
  node [
    id 942
    label "jednostka_czasu"
  ]
  node [
    id 943
    label "zapoznawa&#263;"
  ]
  node [
    id 944
    label "teach"
  ]
  node [
    id 945
    label "train"
  ]
  node [
    id 946
    label "rozwija&#263;"
  ]
  node [
    id 947
    label "szkoli&#263;"
  ]
  node [
    id 948
    label "strategia"
  ]
  node [
    id 949
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 950
    label "ob&#322;awa_augustowska"
  ]
  node [
    id 951
    label "nowotny"
  ]
  node [
    id 952
    label "drugi"
  ]
  node [
    id 953
    label "bie&#380;&#261;cy"
  ]
  node [
    id 954
    label "nowo"
  ]
  node [
    id 955
    label "narybek"
  ]
  node [
    id 956
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 957
    label "obcy"
  ]
  node [
    id 958
    label "spis"
  ]
  node [
    id 959
    label "odinstalowanie"
  ]
  node [
    id 960
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 961
    label "za&#322;o&#380;enie"
  ]
  node [
    id 962
    label "podstawa"
  ]
  node [
    id 963
    label "emitowanie"
  ]
  node [
    id 964
    label "odinstalowywanie"
  ]
  node [
    id 965
    label "instrukcja"
  ]
  node [
    id 966
    label "punkt"
  ]
  node [
    id 967
    label "teleferie"
  ]
  node [
    id 968
    label "emitowa&#263;"
  ]
  node [
    id 969
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 970
    label "sekcja_krytyczna"
  ]
  node [
    id 971
    label "oferta"
  ]
  node [
    id 972
    label "prezentowa&#263;"
  ]
  node [
    id 973
    label "blok"
  ]
  node [
    id 974
    label "podprogram"
  ]
  node [
    id 975
    label "tryb"
  ]
  node [
    id 976
    label "dzia&#322;"
  ]
  node [
    id 977
    label "broszura"
  ]
  node [
    id 978
    label "deklaracja"
  ]
  node [
    id 979
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 980
    label "struktura_organizacyjna"
  ]
  node [
    id 981
    label "zaprezentowanie"
  ]
  node [
    id 982
    label "informatyka"
  ]
  node [
    id 983
    label "booklet"
  ]
  node [
    id 984
    label "menu"
  ]
  node [
    id 985
    label "instalowanie"
  ]
  node [
    id 986
    label "furkacja"
  ]
  node [
    id 987
    label "odinstalowa&#263;"
  ]
  node [
    id 988
    label "instalowa&#263;"
  ]
  node [
    id 989
    label "pirat"
  ]
  node [
    id 990
    label "zainstalowanie"
  ]
  node [
    id 991
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 992
    label "ogranicznik_referencyjny"
  ]
  node [
    id 993
    label "zainstalowa&#263;"
  ]
  node [
    id 994
    label "kana&#322;"
  ]
  node [
    id 995
    label "zaprezentowa&#263;"
  ]
  node [
    id 996
    label "interfejs"
  ]
  node [
    id 997
    label "odinstalowywa&#263;"
  ]
  node [
    id 998
    label "folder"
  ]
  node [
    id 999
    label "course_of_study"
  ]
  node [
    id 1000
    label "ram&#243;wka"
  ]
  node [
    id 1001
    label "prezentowanie"
  ]
  node [
    id 1002
    label "okno"
  ]
  node [
    id 1003
    label "cope"
  ]
  node [
    id 1004
    label "uniewa&#380;nia&#263;"
  ]
  node [
    id 1005
    label "odkrywa&#263;"
  ]
  node [
    id 1006
    label "przestawa&#263;"
  ]
  node [
    id 1007
    label "urzeczywistnia&#263;"
  ]
  node [
    id 1008
    label "usuwa&#263;"
  ]
  node [
    id 1009
    label "undo"
  ]
  node [
    id 1010
    label "trudno&#347;&#263;"
  ]
  node [
    id 1011
    label "sprawa"
  ]
  node [
    id 1012
    label "ambaras"
  ]
  node [
    id 1013
    label "problemat"
  ]
  node [
    id 1014
    label "pierepa&#322;ka"
  ]
  node [
    id 1015
    label "obstruction"
  ]
  node [
    id 1016
    label "problematyka"
  ]
  node [
    id 1017
    label "jajko_Kolumba"
  ]
  node [
    id 1018
    label "subiekcja"
  ]
  node [
    id 1019
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 1020
    label "byd&#322;o"
  ]
  node [
    id 1021
    label "zobo"
  ]
  node [
    id 1022
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 1023
    label "yakalo"
  ]
  node [
    id 1024
    label "dzo"
  ]
  node [
    id 1025
    label "habit"
  ]
  node [
    id 1026
    label "sta&#263;_si&#281;"
  ]
  node [
    id 1027
    label "podj&#261;&#263;"
  ]
  node [
    id 1028
    label "determine"
  ]
  node [
    id 1029
    label "decide"
  ]
  node [
    id 1030
    label "preserve"
  ]
  node [
    id 1031
    label "uchroni&#263;"
  ]
  node [
    id 1032
    label "postpone"
  ]
  node [
    id 1033
    label "znie&#347;&#263;"
  ]
  node [
    id 1034
    label "&#347;ledziowate"
  ]
  node [
    id 1035
    label "pieni&#261;dz"
  ]
  node [
    id 1036
    label "ustawia&#263;"
  ]
  node [
    id 1037
    label "wierzy&#263;"
  ]
  node [
    id 1038
    label "przyjmowa&#263;"
  ]
  node [
    id 1039
    label "pozyskiwa&#263;"
  ]
  node [
    id 1040
    label "kupywa&#263;"
  ]
  node [
    id 1041
    label "uznawa&#263;"
  ]
  node [
    id 1042
    label "pami&#281;&#263;"
  ]
  node [
    id 1043
    label "urz&#261;dzenie"
  ]
  node [
    id 1044
    label "maszyna_Turinga"
  ]
  node [
    id 1045
    label "emulacja"
  ]
  node [
    id 1046
    label "botnet"
  ]
  node [
    id 1047
    label "moc_obliczeniowa"
  ]
  node [
    id 1048
    label "stacja_dysk&#243;w"
  ]
  node [
    id 1049
    label "monitor"
  ]
  node [
    id 1050
    label "karta"
  ]
  node [
    id 1051
    label "elektroniczna_maszyna_licz&#261;ca"
  ]
  node [
    id 1052
    label "mysz"
  ]
  node [
    id 1053
    label "pad"
  ]
  node [
    id 1054
    label "twardy_dysk"
  ]
  node [
    id 1055
    label "radiator"
  ]
  node [
    id 1056
    label "modem"
  ]
  node [
    id 1057
    label "klawiatura"
  ]
  node [
    id 1058
    label "p&#322;yta_g&#322;&#243;wna"
  ]
  node [
    id 1059
    label "procesor"
  ]
  node [
    id 1060
    label "skr&#281;canie"
  ]
  node [
    id 1061
    label "voice"
  ]
  node [
    id 1062
    label "internet"
  ]
  node [
    id 1063
    label "skr&#281;ci&#263;"
  ]
  node [
    id 1064
    label "kartka"
  ]
  node [
    id 1065
    label "orientowa&#263;"
  ]
  node [
    id 1066
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 1067
    label "powierzchnia"
  ]
  node [
    id 1068
    label "plik"
  ]
  node [
    id 1069
    label "bok"
  ]
  node [
    id 1070
    label "pagina"
  ]
  node [
    id 1071
    label "orientowanie"
  ]
  node [
    id 1072
    label "fragment"
  ]
  node [
    id 1073
    label "skr&#281;ca&#263;"
  ]
  node [
    id 1074
    label "g&#243;ra"
  ]
  node [
    id 1075
    label "serwis_internetowy"
  ]
  node [
    id 1076
    label "orientacja"
  ]
  node [
    id 1077
    label "linia"
  ]
  node [
    id 1078
    label "skr&#281;cenie"
  ]
  node [
    id 1079
    label "layout"
  ]
  node [
    id 1080
    label "zorientowa&#263;"
  ]
  node [
    id 1081
    label "zorientowanie"
  ]
  node [
    id 1082
    label "ty&#322;"
  ]
  node [
    id 1083
    label "logowanie"
  ]
  node [
    id 1084
    label "adres_internetowy"
  ]
  node [
    id 1085
    label "uj&#281;cie"
  ]
  node [
    id 1086
    label "prz&#243;d"
  ]
  node [
    id 1087
    label "cenowa_zmiana_poda&#380;y"
  ]
  node [
    id 1088
    label "proces_ekonomiczny"
  ]
  node [
    id 1089
    label "nawis_inflacyjny"
  ]
  node [
    id 1090
    label "krzywa_poda&#380;y"
  ]
  node [
    id 1091
    label "lepko&#347;&#263;_cen"
  ]
  node [
    id 1092
    label "cena_r&#243;wnowagi_rynkowej"
  ]
  node [
    id 1093
    label "poda&#380;_zagregowana"
  ]
  node [
    id 1094
    label "omin&#261;&#263;"
  ]
  node [
    id 1095
    label "stracenie"
  ]
  node [
    id 1096
    label "pogorszy&#263;_si&#281;"
  ]
  node [
    id 1097
    label "zabi&#263;"
  ]
  node [
    id 1098
    label "wytraci&#263;"
  ]
  node [
    id 1099
    label "liszy&#263;_si&#281;"
  ]
  node [
    id 1100
    label "przegra&#263;"
  ]
  node [
    id 1101
    label "waste"
  ]
  node [
    id 1102
    label "whole"
  ]
  node [
    id 1103
    label "Rzym_Zachodni"
  ]
  node [
    id 1104
    label "Rzym_Wschodni"
  ]
  node [
    id 1105
    label "kwota"
  ]
  node [
    id 1106
    label "dokument"
  ]
  node [
    id 1107
    label "device"
  ]
  node [
    id 1108
    label "program_u&#380;ytkowy"
  ]
  node [
    id 1109
    label "intencja"
  ]
  node [
    id 1110
    label "agreement"
  ]
  node [
    id 1111
    label "pomys&#322;"
  ]
  node [
    id 1112
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 1113
    label "plan"
  ]
  node [
    id 1114
    label "dokumentacja"
  ]
  node [
    id 1115
    label "niezale&#380;ny"
  ]
  node [
    id 1116
    label "swobodnie"
  ]
  node [
    id 1117
    label "niespieszny"
  ]
  node [
    id 1118
    label "rozrzedzanie"
  ]
  node [
    id 1119
    label "zwolnienie_si&#281;"
  ]
  node [
    id 1120
    label "wolno"
  ]
  node [
    id 1121
    label "rozrzedzenie"
  ]
  node [
    id 1122
    label "lu&#378;no"
  ]
  node [
    id 1123
    label "zwalnianie_si&#281;"
  ]
  node [
    id 1124
    label "wolnie"
  ]
  node [
    id 1125
    label "strza&#322;"
  ]
  node [
    id 1126
    label "rozwodnienie"
  ]
  node [
    id 1127
    label "wakowa&#263;"
  ]
  node [
    id 1128
    label "rozwadnianie"
  ]
  node [
    id 1129
    label "rzedni&#281;cie"
  ]
  node [
    id 1130
    label "zrzedni&#281;cie"
  ]
  node [
    id 1131
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 1132
    label "Wsch&#243;d"
  ]
  node [
    id 1133
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 1134
    label "sztuka"
  ]
  node [
    id 1135
    label "religia"
  ]
  node [
    id 1136
    label "przejmowa&#263;"
  ]
  node [
    id 1137
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 1138
    label "makrokosmos"
  ]
  node [
    id 1139
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 1140
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 1141
    label "praca_rolnicza"
  ]
  node [
    id 1142
    label "tradycja"
  ]
  node [
    id 1143
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 1144
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 1145
    label "przejmowanie"
  ]
  node [
    id 1146
    label "asymilowanie_si&#281;"
  ]
  node [
    id 1147
    label "przej&#261;&#263;"
  ]
  node [
    id 1148
    label "brzoskwiniarnia"
  ]
  node [
    id 1149
    label "populace"
  ]
  node [
    id 1150
    label "konwencja"
  ]
  node [
    id 1151
    label "propriety"
  ]
  node [
    id 1152
    label "kuchnia"
  ]
  node [
    id 1153
    label "przej&#281;cie"
  ]
  node [
    id 1154
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 1155
    label "zmieni&#263;"
  ]
  node [
    id 1156
    label "ascend"
  ]
  node [
    id 1157
    label "elastyczno&#347;&#263;_popytu"
  ]
  node [
    id 1158
    label "krzywa_popytu"
  ]
  node [
    id 1159
    label "popyt_zagregowany"
  ]
  node [
    id 1160
    label "mechanizm_mno&#380;nikowy"
  ]
  node [
    id 1161
    label "handel"
  ]
  node [
    id 1162
    label "czynnik_niecenowy"
  ]
  node [
    id 1163
    label "poprawi&#263;_si&#281;"
  ]
  node [
    id 1164
    label "naby&#263;"
  ]
  node [
    id 1165
    label "uzyska&#263;"
  ]
  node [
    id 1166
    label "utilize"
  ]
  node [
    id 1167
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 1168
    label "fly"
  ]
  node [
    id 1169
    label "rozerwa&#263;_si&#281;"
  ]
  node [
    id 1170
    label "zniszczy&#263;_si&#281;"
  ]
  node [
    id 1171
    label "break"
  ]
  node [
    id 1172
    label "collapse"
  ]
  node [
    id 1173
    label "sp&#281;ka&#263;"
  ]
  node [
    id 1174
    label "explode"
  ]
  node [
    id 1175
    label "zanikn&#261;&#263;"
  ]
  node [
    id 1176
    label "niedostateczny"
  ]
  node [
    id 1177
    label "mak&#243;wka"
  ]
  node [
    id 1178
    label "gourd"
  ]
  node [
    id 1179
    label "obiekt_naturalny"
  ]
  node [
    id 1180
    label "milion"
  ]
  node [
    id 1181
    label "pojemnik"
  ]
  node [
    id 1182
    label "bubble"
  ]
  node [
    id 1183
    label "&#322;eb"
  ]
  node [
    id 1184
    label "dynia"
  ]
  node [
    id 1185
    label "narz&#281;dzie"
  ]
  node [
    id 1186
    label "czaszka"
  ]
  node [
    id 1187
    label "nowoczesny"
  ]
  node [
    id 1188
    label "elektroniczny"
  ]
  node [
    id 1189
    label "sieciowo"
  ]
  node [
    id 1190
    label "netowy"
  ]
  node [
    id 1191
    label "internetowo"
  ]
  node [
    id 1192
    label "summer"
  ]
  node [
    id 1193
    label "poprowadzi&#263;"
  ]
  node [
    id 1194
    label "pos&#322;a&#263;"
  ]
  node [
    id 1195
    label "have_a_bun_in_the_oven"
  ]
  node [
    id 1196
    label "wykona&#263;"
  ]
  node [
    id 1197
    label "wzbudzi&#263;"
  ]
  node [
    id 1198
    label "wprowadzi&#263;"
  ]
  node [
    id 1199
    label "set"
  ]
  node [
    id 1200
    label "take"
  ]
  node [
    id 1201
    label "carry"
  ]
  node [
    id 1202
    label "unwrap"
  ]
  node [
    id 1203
    label "zostawi&#263;"
  ]
  node [
    id 1204
    label "warto&#347;ciowy"
  ]
  node [
    id 1205
    label "bliski"
  ]
  node [
    id 1206
    label "drogo"
  ]
  node [
    id 1207
    label "przyjaciel"
  ]
  node [
    id 1208
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 1209
    label "mi&#322;y"
  ]
  node [
    id 1210
    label "wym&#243;wi&#263;"
  ]
  node [
    id 1211
    label "uprzedzi&#263;"
  ]
  node [
    id 1212
    label "condition"
  ]
  node [
    id 1213
    label "zapewni&#263;"
  ]
  node [
    id 1214
    label "engineering"
  ]
  node [
    id 1215
    label "technika"
  ]
  node [
    id 1216
    label "mikrotechnologia"
  ]
  node [
    id 1217
    label "technologia_nieorganiczna"
  ]
  node [
    id 1218
    label "biotechnologia"
  ]
  node [
    id 1219
    label "spos&#243;b"
  ]
  node [
    id 1220
    label "skomercjalizowanie"
  ]
  node [
    id 1221
    label "rynkowy"
  ]
  node [
    id 1222
    label "komercjalizowanie"
  ]
  node [
    id 1223
    label "masowy"
  ]
  node [
    id 1224
    label "komercyjnie"
  ]
  node [
    id 1225
    label "niestandardowo"
  ]
  node [
    id 1226
    label "nietypowy"
  ]
  node [
    id 1227
    label "kolekcja"
  ]
  node [
    id 1228
    label "sprz&#281;cior"
  ]
  node [
    id 1229
    label "furniture"
  ]
  node [
    id 1230
    label "equipment"
  ]
  node [
    id 1231
    label "penis"
  ]
  node [
    id 1232
    label "sprz&#281;cik"
  ]
  node [
    id 1233
    label "istota"
  ]
  node [
    id 1234
    label "wpa&#347;&#263;"
  ]
  node [
    id 1235
    label "wpadanie"
  ]
  node [
    id 1236
    label "wpada&#263;"
  ]
  node [
    id 1237
    label "przyroda"
  ]
  node [
    id 1238
    label "mienie"
  ]
  node [
    id 1239
    label "object"
  ]
  node [
    id 1240
    label "wpadni&#281;cie"
  ]
  node [
    id 1241
    label "zwyczajnie"
  ]
  node [
    id 1242
    label "zwykle"
  ]
  node [
    id 1243
    label "przeci&#281;tny"
  ]
  node [
    id 1244
    label "mikrokomputer"
  ]
  node [
    id 1245
    label "odpowiedni"
  ]
  node [
    id 1246
    label "dok&#322;adnie"
  ]
  node [
    id 1247
    label "dobry"
  ]
  node [
    id 1248
    label "akuratny"
  ]
  node [
    id 1249
    label "silny"
  ]
  node [
    id 1250
    label "wa&#380;nie"
  ]
  node [
    id 1251
    label "eksponowany"
  ]
  node [
    id 1252
    label "istotnie"
  ]
  node [
    id 1253
    label "wynios&#322;y"
  ]
  node [
    id 1254
    label "dono&#347;ny"
  ]
  node [
    id 1255
    label "tera&#378;niejszo&#347;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 187
  ]
  edge [
    source 8
    target 188
  ]
  edge [
    source 8
    target 189
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 190
  ]
  edge [
    source 9
    target 191
  ]
  edge [
    source 9
    target 192
  ]
  edge [
    source 9
    target 193
  ]
  edge [
    source 9
    target 156
  ]
  edge [
    source 9
    target 169
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 31
  ]
  edge [
    source 13
    target 32
  ]
  edge [
    source 13
    target 194
  ]
  edge [
    source 13
    target 195
  ]
  edge [
    source 13
    target 196
  ]
  edge [
    source 13
    target 197
  ]
  edge [
    source 13
    target 198
  ]
  edge [
    source 13
    target 199
  ]
  edge [
    source 13
    target 200
  ]
  edge [
    source 13
    target 201
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 202
  ]
  edge [
    source 14
    target 203
  ]
  edge [
    source 14
    target 204
  ]
  edge [
    source 14
    target 205
  ]
  edge [
    source 14
    target 94
  ]
  edge [
    source 14
    target 206
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 207
  ]
  edge [
    source 16
    target 208
  ]
  edge [
    source 16
    target 209
  ]
  edge [
    source 16
    target 210
  ]
  edge [
    source 16
    target 211
  ]
  edge [
    source 16
    target 212
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 213
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 214
  ]
  edge [
    source 18
    target 215
  ]
  edge [
    source 18
    target 216
  ]
  edge [
    source 18
    target 217
  ]
  edge [
    source 18
    target 218
  ]
  edge [
    source 18
    target 219
  ]
  edge [
    source 18
    target 220
  ]
  edge [
    source 18
    target 221
  ]
  edge [
    source 18
    target 222
  ]
  edge [
    source 18
    target 223
  ]
  edge [
    source 18
    target 224
  ]
  edge [
    source 18
    target 225
  ]
  edge [
    source 18
    target 226
  ]
  edge [
    source 18
    target 227
  ]
  edge [
    source 18
    target 228
  ]
  edge [
    source 18
    target 229
  ]
  edge [
    source 18
    target 230
  ]
  edge [
    source 18
    target 231
  ]
  edge [
    source 18
    target 232
  ]
  edge [
    source 18
    target 233
  ]
  edge [
    source 18
    target 234
  ]
  edge [
    source 18
    target 235
  ]
  edge [
    source 18
    target 236
  ]
  edge [
    source 18
    target 237
  ]
  edge [
    source 18
    target 238
  ]
  edge [
    source 18
    target 239
  ]
  edge [
    source 18
    target 240
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 98
  ]
  edge [
    source 19
    target 80
  ]
  edge [
    source 19
    target 96
  ]
  edge [
    source 19
    target 31
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 241
  ]
  edge [
    source 20
    target 242
  ]
  edge [
    source 20
    target 243
  ]
  edge [
    source 20
    target 244
  ]
  edge [
    source 20
    target 245
  ]
  edge [
    source 20
    target 246
  ]
  edge [
    source 20
    target 105
  ]
  edge [
    source 20
    target 247
  ]
  edge [
    source 20
    target 248
  ]
  edge [
    source 20
    target 249
  ]
  edge [
    source 20
    target 250
  ]
  edge [
    source 20
    target 251
  ]
  edge [
    source 20
    target 252
  ]
  edge [
    source 20
    target 253
  ]
  edge [
    source 20
    target 254
  ]
  edge [
    source 20
    target 255
  ]
  edge [
    source 20
    target 256
  ]
  edge [
    source 20
    target 257
  ]
  edge [
    source 20
    target 258
  ]
  edge [
    source 20
    target 259
  ]
  edge [
    source 20
    target 260
  ]
  edge [
    source 20
    target 42
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 261
  ]
  edge [
    source 21
    target 262
  ]
  edge [
    source 21
    target 263
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 264
  ]
  edge [
    source 23
    target 265
  ]
  edge [
    source 23
    target 266
  ]
  edge [
    source 23
    target 267
  ]
  edge [
    source 23
    target 268
  ]
  edge [
    source 23
    target 269
  ]
  edge [
    source 23
    target 270
  ]
  edge [
    source 23
    target 271
  ]
  edge [
    source 23
    target 272
  ]
  edge [
    source 23
    target 273
  ]
  edge [
    source 23
    target 274
  ]
  edge [
    source 23
    target 275
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 276
  ]
  edge [
    source 25
    target 277
  ]
  edge [
    source 25
    target 278
  ]
  edge [
    source 25
    target 279
  ]
  edge [
    source 25
    target 280
  ]
  edge [
    source 25
    target 281
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 282
  ]
  edge [
    source 26
    target 283
  ]
  edge [
    source 26
    target 284
  ]
  edge [
    source 26
    target 285
  ]
  edge [
    source 26
    target 286
  ]
  edge [
    source 26
    target 287
  ]
  edge [
    source 26
    target 288
  ]
  edge [
    source 26
    target 289
  ]
  edge [
    source 26
    target 290
  ]
  edge [
    source 26
    target 35
  ]
  edge [
    source 26
    target 109
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 291
  ]
  edge [
    source 28
    target 292
  ]
  edge [
    source 28
    target 293
  ]
  edge [
    source 28
    target 294
  ]
  edge [
    source 28
    target 295
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 40
  ]
  edge [
    source 29
    target 41
  ]
  edge [
    source 29
    target 59
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 296
  ]
  edge [
    source 30
    target 297
  ]
  edge [
    source 30
    target 298
  ]
  edge [
    source 30
    target 299
  ]
  edge [
    source 30
    target 300
  ]
  edge [
    source 30
    target 301
  ]
  edge [
    source 30
    target 302
  ]
  edge [
    source 30
    target 303
  ]
  edge [
    source 30
    target 304
  ]
  edge [
    source 30
    target 305
  ]
  edge [
    source 30
    target 306
  ]
  edge [
    source 30
    target 307
  ]
  edge [
    source 30
    target 138
  ]
  edge [
    source 30
    target 308
  ]
  edge [
    source 30
    target 309
  ]
  edge [
    source 30
    target 98
  ]
  edge [
    source 31
    target 47
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 139
  ]
  edge [
    source 31
    target 310
  ]
  edge [
    source 31
    target 311
  ]
  edge [
    source 31
    target 312
  ]
  edge [
    source 31
    target 313
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 48
  ]
  edge [
    source 32
    target 314
  ]
  edge [
    source 32
    target 315
  ]
  edge [
    source 32
    target 316
  ]
  edge [
    source 32
    target 317
  ]
  edge [
    source 32
    target 318
  ]
  edge [
    source 32
    target 319
  ]
  edge [
    source 32
    target 320
  ]
  edge [
    source 32
    target 321
  ]
  edge [
    source 32
    target 322
  ]
  edge [
    source 32
    target 323
  ]
  edge [
    source 32
    target 324
  ]
  edge [
    source 32
    target 46
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 325
  ]
  edge [
    source 33
    target 326
  ]
  edge [
    source 33
    target 327
  ]
  edge [
    source 33
    target 328
  ]
  edge [
    source 33
    target 329
  ]
  edge [
    source 33
    target 330
  ]
  edge [
    source 33
    target 331
  ]
  edge [
    source 33
    target 332
  ]
  edge [
    source 33
    target 333
  ]
  edge [
    source 33
    target 334
  ]
  edge [
    source 33
    target 335
  ]
  edge [
    source 33
    target 336
  ]
  edge [
    source 33
    target 337
  ]
  edge [
    source 33
    target 338
  ]
  edge [
    source 33
    target 339
  ]
  edge [
    source 33
    target 340
  ]
  edge [
    source 33
    target 341
  ]
  edge [
    source 33
    target 342
  ]
  edge [
    source 33
    target 113
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 343
  ]
  edge [
    source 34
    target 344
  ]
  edge [
    source 34
    target 345
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 37
  ]
  edge [
    source 35
    target 38
  ]
  edge [
    source 35
    target 346
  ]
  edge [
    source 35
    target 114
  ]
  edge [
    source 35
    target 347
  ]
  edge [
    source 35
    target 109
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 348
  ]
  edge [
    source 36
    target 349
  ]
  edge [
    source 36
    target 350
  ]
  edge [
    source 36
    target 351
  ]
  edge [
    source 36
    target 352
  ]
  edge [
    source 36
    target 353
  ]
  edge [
    source 36
    target 354
  ]
  edge [
    source 36
    target 355
  ]
  edge [
    source 36
    target 356
  ]
  edge [
    source 36
    target 357
  ]
  edge [
    source 36
    target 358
  ]
  edge [
    source 36
    target 359
  ]
  edge [
    source 36
    target 360
  ]
  edge [
    source 36
    target 361
  ]
  edge [
    source 36
    target 362
  ]
  edge [
    source 36
    target 363
  ]
  edge [
    source 36
    target 364
  ]
  edge [
    source 36
    target 365
  ]
  edge [
    source 36
    target 366
  ]
  edge [
    source 36
    target 367
  ]
  edge [
    source 36
    target 368
  ]
  edge [
    source 36
    target 369
  ]
  edge [
    source 36
    target 370
  ]
  edge [
    source 36
    target 98
  ]
  edge [
    source 36
    target 371
  ]
  edge [
    source 37
    target 372
  ]
  edge [
    source 37
    target 373
  ]
  edge [
    source 37
    target 374
  ]
  edge [
    source 37
    target 375
  ]
  edge [
    source 37
    target 376
  ]
  edge [
    source 37
    target 377
  ]
  edge [
    source 37
    target 378
  ]
  edge [
    source 37
    target 379
  ]
  edge [
    source 37
    target 380
  ]
  edge [
    source 37
    target 381
  ]
  edge [
    source 37
    target 382
  ]
  edge [
    source 37
    target 248
  ]
  edge [
    source 37
    target 383
  ]
  edge [
    source 37
    target 384
  ]
  edge [
    source 37
    target 385
  ]
  edge [
    source 37
    target 386
  ]
  edge [
    source 37
    target 387
  ]
  edge [
    source 37
    target 388
  ]
  edge [
    source 37
    target 389
  ]
  edge [
    source 37
    target 390
  ]
  edge [
    source 37
    target 391
  ]
  edge [
    source 37
    target 392
  ]
  edge [
    source 37
    target 393
  ]
  edge [
    source 37
    target 394
  ]
  edge [
    source 37
    target 395
  ]
  edge [
    source 37
    target 396
  ]
  edge [
    source 37
    target 397
  ]
  edge [
    source 37
    target 398
  ]
  edge [
    source 37
    target 399
  ]
  edge [
    source 37
    target 400
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 401
  ]
  edge [
    source 38
    target 402
  ]
  edge [
    source 38
    target 403
  ]
  edge [
    source 38
    target 404
  ]
  edge [
    source 38
    target 405
  ]
  edge [
    source 38
    target 406
  ]
  edge [
    source 38
    target 407
  ]
  edge [
    source 38
    target 371
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 408
  ]
  edge [
    source 39
    target 409
  ]
  edge [
    source 39
    target 410
  ]
  edge [
    source 39
    target 411
  ]
  edge [
    source 39
    target 412
  ]
  edge [
    source 39
    target 51
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 413
  ]
  edge [
    source 43
    target 414
  ]
  edge [
    source 43
    target 415
  ]
  edge [
    source 43
    target 416
  ]
  edge [
    source 43
    target 417
  ]
  edge [
    source 43
    target 418
  ]
  edge [
    source 43
    target 419
  ]
  edge [
    source 43
    target 420
  ]
  edge [
    source 43
    target 421
  ]
  edge [
    source 43
    target 422
  ]
  edge [
    source 43
    target 423
  ]
  edge [
    source 43
    target 424
  ]
  edge [
    source 43
    target 425
  ]
  edge [
    source 43
    target 426
  ]
  edge [
    source 43
    target 60
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 427
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 141
  ]
  edge [
    source 45
    target 74
  ]
  edge [
    source 45
    target 122
  ]
  edge [
    source 45
    target 428
  ]
  edge [
    source 45
    target 217
  ]
  edge [
    source 45
    target 152
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 68
  ]
  edge [
    source 46
    target 429
  ]
  edge [
    source 46
    target 430
  ]
  edge [
    source 47
    target 431
  ]
  edge [
    source 47
    target 177
  ]
  edge [
    source 47
    target 432
  ]
  edge [
    source 47
    target 433
  ]
  edge [
    source 47
    target 434
  ]
  edge [
    source 47
    target 435
  ]
  edge [
    source 47
    target 436
  ]
  edge [
    source 47
    target 437
  ]
  edge [
    source 47
    target 438
  ]
  edge [
    source 47
    target 388
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 57
  ]
  edge [
    source 48
    target 58
  ]
  edge [
    source 48
    target 439
  ]
  edge [
    source 48
    target 440
  ]
  edge [
    source 48
    target 441
  ]
  edge [
    source 48
    target 92
  ]
  edge [
    source 48
    target 442
  ]
  edge [
    source 48
    target 443
  ]
  edge [
    source 48
    target 444
  ]
  edge [
    source 48
    target 445
  ]
  edge [
    source 48
    target 446
  ]
  edge [
    source 48
    target 447
  ]
  edge [
    source 48
    target 448
  ]
  edge [
    source 48
    target 449
  ]
  edge [
    source 48
    target 450
  ]
  edge [
    source 48
    target 98
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 451
  ]
  edge [
    source 49
    target 452
  ]
  edge [
    source 49
    target 453
  ]
  edge [
    source 49
    target 454
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 455
  ]
  edge [
    source 50
    target 456
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 457
  ]
  edge [
    source 52
    target 458
  ]
  edge [
    source 52
    target 459
  ]
  edge [
    source 52
    target 244
  ]
  edge [
    source 52
    target 460
  ]
  edge [
    source 52
    target 461
  ]
  edge [
    source 52
    target 462
  ]
  edge [
    source 52
    target 463
  ]
  edge [
    source 52
    target 464
  ]
  edge [
    source 52
    target 465
  ]
  edge [
    source 52
    target 466
  ]
  edge [
    source 52
    target 467
  ]
  edge [
    source 52
    target 289
  ]
  edge [
    source 52
    target 468
  ]
  edge [
    source 52
    target 469
  ]
  edge [
    source 52
    target 470
  ]
  edge [
    source 52
    target 471
  ]
  edge [
    source 52
    target 472
  ]
  edge [
    source 52
    target 473
  ]
  edge [
    source 52
    target 257
  ]
  edge [
    source 52
    target 474
  ]
  edge [
    source 52
    target 475
  ]
  edge [
    source 52
    target 476
  ]
  edge [
    source 52
    target 477
  ]
  edge [
    source 52
    target 478
  ]
  edge [
    source 52
    target 479
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 362
  ]
  edge [
    source 53
    target 480
  ]
  edge [
    source 53
    target 481
  ]
  edge [
    source 53
    target 482
  ]
  edge [
    source 53
    target 483
  ]
  edge [
    source 53
    target 484
  ]
  edge [
    source 53
    target 485
  ]
  edge [
    source 53
    target 486
  ]
  edge [
    source 53
    target 487
  ]
  edge [
    source 53
    target 488
  ]
  edge [
    source 53
    target 489
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 490
  ]
  edge [
    source 54
    target 491
  ]
  edge [
    source 54
    target 492
  ]
  edge [
    source 54
    target 493
  ]
  edge [
    source 54
    target 94
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 494
  ]
  edge [
    source 55
    target 495
  ]
  edge [
    source 55
    target 496
  ]
  edge [
    source 55
    target 497
  ]
  edge [
    source 55
    target 347
  ]
  edge [
    source 55
    target 498
  ]
  edge [
    source 55
    target 499
  ]
  edge [
    source 55
    target 500
  ]
  edge [
    source 55
    target 82
  ]
  edge [
    source 55
    target 501
  ]
  edge [
    source 55
    target 502
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 138
  ]
  edge [
    source 56
    target 96
  ]
  edge [
    source 56
    target 503
  ]
  edge [
    source 56
    target 456
  ]
  edge [
    source 56
    target 504
  ]
  edge [
    source 56
    target 505
  ]
  edge [
    source 56
    target 506
  ]
  edge [
    source 56
    target 507
  ]
  edge [
    source 56
    target 508
  ]
  edge [
    source 56
    target 509
  ]
  edge [
    source 56
    target 227
  ]
  edge [
    source 56
    target 510
  ]
  edge [
    source 56
    target 511
  ]
  edge [
    source 56
    target 512
  ]
  edge [
    source 56
    target 127
  ]
  edge [
    source 56
    target 141
  ]
  edge [
    source 56
    target 161
  ]
  edge [
    source 56
    target 170
  ]
  edge [
    source 57
    target 513
  ]
  edge [
    source 57
    target 514
  ]
  edge [
    source 57
    target 515
  ]
  edge [
    source 57
    target 89
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 68
  ]
  edge [
    source 58
    target 69
  ]
  edge [
    source 58
    target 516
  ]
  edge [
    source 58
    target 517
  ]
  edge [
    source 58
    target 518
  ]
  edge [
    source 58
    target 519
  ]
  edge [
    source 58
    target 520
  ]
  edge [
    source 58
    target 521
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 522
  ]
  edge [
    source 59
    target 523
  ]
  edge [
    source 59
    target 524
  ]
  edge [
    source 59
    target 376
  ]
  edge [
    source 59
    target 525
  ]
  edge [
    source 59
    target 526
  ]
  edge [
    source 59
    target 527
  ]
  edge [
    source 59
    target 528
  ]
  edge [
    source 59
    target 529
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 530
  ]
  edge [
    source 60
    target 531
  ]
  edge [
    source 60
    target 162
  ]
  edge [
    source 60
    target 532
  ]
  edge [
    source 60
    target 533
  ]
  edge [
    source 60
    target 534
  ]
  edge [
    source 60
    target 535
  ]
  edge [
    source 60
    target 536
  ]
  edge [
    source 60
    target 537
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 538
  ]
  edge [
    source 61
    target 205
  ]
  edge [
    source 61
    target 539
  ]
  edge [
    source 61
    target 70
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 540
  ]
  edge [
    source 62
    target 541
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 542
  ]
  edge [
    source 63
    target 543
  ]
  edge [
    source 63
    target 544
  ]
  edge [
    source 63
    target 545
  ]
  edge [
    source 63
    target 546
  ]
  edge [
    source 63
    target 547
  ]
  edge [
    source 63
    target 548
  ]
  edge [
    source 63
    target 270
  ]
  edge [
    source 63
    target 549
  ]
  edge [
    source 63
    target 138
  ]
  edge [
    source 63
    target 550
  ]
  edge [
    source 63
    target 551
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 552
  ]
  edge [
    source 64
    target 553
  ]
  edge [
    source 64
    target 554
  ]
  edge [
    source 64
    target 555
  ]
  edge [
    source 64
    target 556
  ]
  edge [
    source 64
    target 557
  ]
  edge [
    source 64
    target 168
  ]
  edge [
    source 64
    target 558
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 214
  ]
  edge [
    source 65
    target 559
  ]
  edge [
    source 65
    target 560
  ]
  edge [
    source 65
    target 561
  ]
  edge [
    source 65
    target 562
  ]
  edge [
    source 65
    target 563
  ]
  edge [
    source 65
    target 122
  ]
  edge [
    source 65
    target 564
  ]
  edge [
    source 65
    target 565
  ]
  edge [
    source 65
    target 566
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 67
    target 567
  ]
  edge [
    source 67
    target 568
  ]
  edge [
    source 67
    target 569
  ]
  edge [
    source 67
    target 570
  ]
  edge [
    source 67
    target 490
  ]
  edge [
    source 67
    target 571
  ]
  edge [
    source 67
    target 572
  ]
  edge [
    source 67
    target 573
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 68
    target 574
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 69
    target 374
  ]
  edge [
    source 69
    target 575
  ]
  edge [
    source 69
    target 576
  ]
  edge [
    source 69
    target 577
  ]
  edge [
    source 69
    target 578
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 579
  ]
  edge [
    source 70
    target 580
  ]
  edge [
    source 70
    target 581
  ]
  edge [
    source 70
    target 582
  ]
  edge [
    source 70
    target 583
  ]
  edge [
    source 70
    target 584
  ]
  edge [
    source 70
    target 585
  ]
  edge [
    source 70
    target 586
  ]
  edge [
    source 70
    target 587
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 72
    target 73
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 73
    target 588
  ]
  edge [
    source 73
    target 589
  ]
  edge [
    source 73
    target 590
  ]
  edge [
    source 73
    target 591
  ]
  edge [
    source 73
    target 592
  ]
  edge [
    source 73
    target 376
  ]
  edge [
    source 73
    target 593
  ]
  edge [
    source 73
    target 594
  ]
  edge [
    source 73
    target 595
  ]
  edge [
    source 73
    target 596
  ]
  edge [
    source 73
    target 597
  ]
  edge [
    source 73
    target 598
  ]
  edge [
    source 73
    target 599
  ]
  edge [
    source 73
    target 600
  ]
  edge [
    source 73
    target 601
  ]
  edge [
    source 73
    target 602
  ]
  edge [
    source 73
    target 603
  ]
  edge [
    source 73
    target 604
  ]
  edge [
    source 73
    target 605
  ]
  edge [
    source 73
    target 606
  ]
  edge [
    source 73
    target 607
  ]
  edge [
    source 73
    target 608
  ]
  edge [
    source 73
    target 609
  ]
  edge [
    source 73
    target 610
  ]
  edge [
    source 73
    target 611
  ]
  edge [
    source 73
    target 612
  ]
  edge [
    source 73
    target 613
  ]
  edge [
    source 73
    target 614
  ]
  edge [
    source 73
    target 615
  ]
  edge [
    source 74
    target 75
  ]
  edge [
    source 74
    target 118
  ]
  edge [
    source 74
    target 119
  ]
  edge [
    source 74
    target 98
  ]
  edge [
    source 74
    target 127
  ]
  edge [
    source 74
    target 128
  ]
  edge [
    source 74
    target 143
  ]
  edge [
    source 75
    target 76
  ]
  edge [
    source 75
    target 616
  ]
  edge [
    source 75
    target 617
  ]
  edge [
    source 75
    target 618
  ]
  edge [
    source 75
    target 91
  ]
  edge [
    source 76
    target 77
  ]
  edge [
    source 77
    target 78
  ]
  edge [
    source 77
    target 619
  ]
  edge [
    source 77
    target 620
  ]
  edge [
    source 77
    target 621
  ]
  edge [
    source 77
    target 161
  ]
  edge [
    source 77
    target 622
  ]
  edge [
    source 77
    target 623
  ]
  edge [
    source 77
    target 624
  ]
  edge [
    source 78
    target 79
  ]
  edge [
    source 78
    target 625
  ]
  edge [
    source 78
    target 626
  ]
  edge [
    source 79
    target 80
  ]
  edge [
    source 79
    target 627
  ]
  edge [
    source 79
    target 628
  ]
  edge [
    source 79
    target 273
  ]
  edge [
    source 79
    target 629
  ]
  edge [
    source 79
    target 630
  ]
  edge [
    source 79
    target 631
  ]
  edge [
    source 79
    target 632
  ]
  edge [
    source 80
    target 81
  ]
  edge [
    source 80
    target 99
  ]
  edge [
    source 80
    target 169
  ]
  edge [
    source 80
    target 151
  ]
  edge [
    source 80
    target 633
  ]
  edge [
    source 80
    target 634
  ]
  edge [
    source 80
    target 635
  ]
  edge [
    source 80
    target 636
  ]
  edge [
    source 80
    target 637
  ]
  edge [
    source 80
    target 638
  ]
  edge [
    source 80
    target 639
  ]
  edge [
    source 80
    target 640
  ]
  edge [
    source 80
    target 641
  ]
  edge [
    source 80
    target 145
  ]
  edge [
    source 80
    target 157
  ]
  edge [
    source 80
    target 163
  ]
  edge [
    source 80
    target 132
  ]
  edge [
    source 81
    target 82
  ]
  edge [
    source 81
    target 85
  ]
  edge [
    source 81
    target 86
  ]
  edge [
    source 82
    target 83
  ]
  edge [
    source 82
    target 642
  ]
  edge [
    source 82
    target 643
  ]
  edge [
    source 83
    target 84
  ]
  edge [
    source 83
    target 644
  ]
  edge [
    source 83
    target 645
  ]
  edge [
    source 84
    target 85
  ]
  edge [
    source 85
    target 646
  ]
  edge [
    source 86
    target 87
  ]
  edge [
    source 87
    target 88
  ]
  edge [
    source 87
    target 647
  ]
  edge [
    source 87
    target 648
  ]
  edge [
    source 87
    target 649
  ]
  edge [
    source 87
    target 650
  ]
  edge [
    source 87
    target 651
  ]
  edge [
    source 87
    target 652
  ]
  edge [
    source 87
    target 653
  ]
  edge [
    source 87
    target 266
  ]
  edge [
    source 87
    target 654
  ]
  edge [
    source 87
    target 655
  ]
  edge [
    source 87
    target 656
  ]
  edge [
    source 87
    target 657
  ]
  edge [
    source 87
    target 269
  ]
  edge [
    source 87
    target 658
  ]
  edge [
    source 87
    target 270
  ]
  edge [
    source 87
    target 659
  ]
  edge [
    source 87
    target 660
  ]
  edge [
    source 87
    target 661
  ]
  edge [
    source 87
    target 662
  ]
  edge [
    source 87
    target 90
  ]
  edge [
    source 88
    target 89
  ]
  edge [
    source 88
    target 663
  ]
  edge [
    source 88
    target 664
  ]
  edge [
    source 88
    target 403
  ]
  edge [
    source 88
    target 665
  ]
  edge [
    source 88
    target 666
  ]
  edge [
    source 88
    target 667
  ]
  edge [
    source 88
    target 668
  ]
  edge [
    source 88
    target 669
  ]
  edge [
    source 88
    target 670
  ]
  edge [
    source 88
    target 671
  ]
  edge [
    source 88
    target 672
  ]
  edge [
    source 88
    target 371
  ]
  edge [
    source 88
    target 673
  ]
  edge [
    source 88
    target 674
  ]
  edge [
    source 88
    target 675
  ]
  edge [
    source 88
    target 136
  ]
  edge [
    source 88
    target 143
  ]
  edge [
    source 89
    target 90
  ]
  edge [
    source 89
    target 676
  ]
  edge [
    source 89
    target 677
  ]
  edge [
    source 89
    target 678
  ]
  edge [
    source 89
    target 679
  ]
  edge [
    source 89
    target 371
  ]
  edge [
    source 90
    target 91
  ]
  edge [
    source 90
    target 136
  ]
  edge [
    source 90
    target 680
  ]
  edge [
    source 90
    target 681
  ]
  edge [
    source 90
    target 485
  ]
  edge [
    source 90
    target 682
  ]
  edge [
    source 90
    target 683
  ]
  edge [
    source 90
    target 684
  ]
  edge [
    source 90
    target 685
  ]
  edge [
    source 90
    target 686
  ]
  edge [
    source 90
    target 687
  ]
  edge [
    source 90
    target 688
  ]
  edge [
    source 90
    target 689
  ]
  edge [
    source 90
    target 690
  ]
  edge [
    source 90
    target 691
  ]
  edge [
    source 90
    target 692
  ]
  edge [
    source 90
    target 693
  ]
  edge [
    source 90
    target 694
  ]
  edge [
    source 90
    target 695
  ]
  edge [
    source 90
    target 696
  ]
  edge [
    source 90
    target 428
  ]
  edge [
    source 90
    target 697
  ]
  edge [
    source 90
    target 698
  ]
  edge [
    source 91
    target 92
  ]
  edge [
    source 91
    target 137
  ]
  edge [
    source 91
    target 699
  ]
  edge [
    source 91
    target 700
  ]
  edge [
    source 91
    target 701
  ]
  edge [
    source 91
    target 702
  ]
  edge [
    source 92
    target 93
  ]
  edge [
    source 92
    target 703
  ]
  edge [
    source 92
    target 704
  ]
  edge [
    source 92
    target 705
  ]
  edge [
    source 92
    target 706
  ]
  edge [
    source 92
    target 707
  ]
  edge [
    source 92
    target 708
  ]
  edge [
    source 92
    target 709
  ]
  edge [
    source 92
    target 710
  ]
  edge [
    source 92
    target 711
  ]
  edge [
    source 92
    target 712
  ]
  edge [
    source 92
    target 713
  ]
  edge [
    source 92
    target 714
  ]
  edge [
    source 92
    target 715
  ]
  edge [
    source 92
    target 716
  ]
  edge [
    source 92
    target 717
  ]
  edge [
    source 92
    target 718
  ]
  edge [
    source 92
    target 719
  ]
  edge [
    source 92
    target 720
  ]
  edge [
    source 92
    target 721
  ]
  edge [
    source 92
    target 722
  ]
  edge [
    source 92
    target 723
  ]
  edge [
    source 92
    target 392
  ]
  edge [
    source 92
    target 724
  ]
  edge [
    source 92
    target 725
  ]
  edge [
    source 92
    target 726
  ]
  edge [
    source 92
    target 727
  ]
  edge [
    source 92
    target 728
  ]
  edge [
    source 92
    target 729
  ]
  edge [
    source 92
    target 730
  ]
  edge [
    source 92
    target 731
  ]
  edge [
    source 92
    target 732
  ]
  edge [
    source 92
    target 733
  ]
  edge [
    source 92
    target 734
  ]
  edge [
    source 92
    target 735
  ]
  edge [
    source 92
    target 736
  ]
  edge [
    source 92
    target 737
  ]
  edge [
    source 92
    target 738
  ]
  edge [
    source 92
    target 117
  ]
  edge [
    source 92
    target 150
  ]
  edge [
    source 93
    target 94
  ]
  edge [
    source 93
    target 739
  ]
  edge [
    source 93
    target 740
  ]
  edge [
    source 93
    target 741
  ]
  edge [
    source 93
    target 641
  ]
  edge [
    source 94
    target 95
  ]
  edge [
    source 94
    target 742
  ]
  edge [
    source 94
    target 743
  ]
  edge [
    source 94
    target 744
  ]
  edge [
    source 94
    target 745
  ]
  edge [
    source 94
    target 746
  ]
  edge [
    source 94
    target 747
  ]
  edge [
    source 94
    target 748
  ]
  edge [
    source 94
    target 749
  ]
  edge [
    source 94
    target 111
  ]
  edge [
    source 94
    target 750
  ]
  edge [
    source 94
    target 751
  ]
  edge [
    source 95
    target 96
  ]
  edge [
    source 95
    target 752
  ]
  edge [
    source 95
    target 753
  ]
  edge [
    source 95
    target 754
  ]
  edge [
    source 95
    target 755
  ]
  edge [
    source 95
    target 756
  ]
  edge [
    source 95
    target 757
  ]
  edge [
    source 95
    target 758
  ]
  edge [
    source 95
    target 759
  ]
  edge [
    source 95
    target 760
  ]
  edge [
    source 95
    target 761
  ]
  edge [
    source 95
    target 762
  ]
  edge [
    source 95
    target 763
  ]
  edge [
    source 95
    target 764
  ]
  edge [
    source 95
    target 644
  ]
  edge [
    source 95
    target 765
  ]
  edge [
    source 96
    target 97
  ]
  edge [
    source 97
    target 98
  ]
  edge [
    source 97
    target 766
  ]
  edge [
    source 97
    target 767
  ]
  edge [
    source 97
    target 557
  ]
  edge [
    source 97
    target 768
  ]
  edge [
    source 97
    target 769
  ]
  edge [
    source 97
    target 770
  ]
  edge [
    source 97
    target 771
  ]
  edge [
    source 97
    target 772
  ]
  edge [
    source 98
    target 114
  ]
  edge [
    source 98
    target 110
  ]
  edge [
    source 98
    target 126
  ]
  edge [
    source 98
    target 773
  ]
  edge [
    source 98
    target 774
  ]
  edge [
    source 98
    target 775
  ]
  edge [
    source 98
    target 776
  ]
  edge [
    source 98
    target 777
  ]
  edge [
    source 98
    target 778
  ]
  edge [
    source 98
    target 779
  ]
  edge [
    source 98
    target 780
  ]
  edge [
    source 98
    target 781
  ]
  edge [
    source 98
    target 782
  ]
  edge [
    source 98
    target 783
  ]
  edge [
    source 98
    target 784
  ]
  edge [
    source 98
    target 785
  ]
  edge [
    source 98
    target 786
  ]
  edge [
    source 98
    target 363
  ]
  edge [
    source 98
    target 787
  ]
  edge [
    source 98
    target 788
  ]
  edge [
    source 98
    target 236
  ]
  edge [
    source 98
    target 789
  ]
  edge [
    source 98
    target 790
  ]
  edge [
    source 98
    target 791
  ]
  edge [
    source 98
    target 792
  ]
  edge [
    source 98
    target 793
  ]
  edge [
    source 99
    target 100
  ]
  edge [
    source 99
    target 110
  ]
  edge [
    source 99
    target 111
  ]
  edge [
    source 99
    target 115
  ]
  edge [
    source 99
    target 126
  ]
  edge [
    source 99
    target 127
  ]
  edge [
    source 99
    target 133
  ]
  edge [
    source 99
    target 104
  ]
  edge [
    source 100
    target 101
  ]
  edge [
    source 100
    target 588
  ]
  edge [
    source 100
    target 794
  ]
  edge [
    source 100
    target 795
  ]
  edge [
    source 100
    target 796
  ]
  edge [
    source 100
    target 797
  ]
  edge [
    source 100
    target 798
  ]
  edge [
    source 100
    target 799
  ]
  edge [
    source 100
    target 800
  ]
  edge [
    source 100
    target 801
  ]
  edge [
    source 100
    target 802
  ]
  edge [
    source 100
    target 803
  ]
  edge [
    source 100
    target 804
  ]
  edge [
    source 100
    target 805
  ]
  edge [
    source 100
    target 806
  ]
  edge [
    source 100
    target 807
  ]
  edge [
    source 100
    target 808
  ]
  edge [
    source 100
    target 611
  ]
  edge [
    source 100
    target 809
  ]
  edge [
    source 100
    target 810
  ]
  edge [
    source 100
    target 811
  ]
  edge [
    source 100
    target 812
  ]
  edge [
    source 100
    target 813
  ]
  edge [
    source 101
    target 102
  ]
  edge [
    source 101
    target 814
  ]
  edge [
    source 101
    target 270
  ]
  edge [
    source 101
    target 815
  ]
  edge [
    source 101
    target 816
  ]
  edge [
    source 101
    target 817
  ]
  edge [
    source 101
    target 818
  ]
  edge [
    source 101
    target 819
  ]
  edge [
    source 101
    target 820
  ]
  edge [
    source 101
    target 821
  ]
  edge [
    source 101
    target 822
  ]
  edge [
    source 101
    target 823
  ]
  edge [
    source 101
    target 824
  ]
  edge [
    source 101
    target 825
  ]
  edge [
    source 101
    target 826
  ]
  edge [
    source 101
    target 519
  ]
  edge [
    source 101
    target 521
  ]
  edge [
    source 101
    target 827
  ]
  edge [
    source 101
    target 828
  ]
  edge [
    source 101
    target 829
  ]
  edge [
    source 101
    target 174
  ]
  edge [
    source 101
    target 830
  ]
  edge [
    source 101
    target 740
  ]
  edge [
    source 101
    target 831
  ]
  edge [
    source 101
    target 832
  ]
  edge [
    source 101
    target 514
  ]
  edge [
    source 101
    target 308
  ]
  edge [
    source 101
    target 833
  ]
  edge [
    source 101
    target 834
  ]
  edge [
    source 101
    target 835
  ]
  edge [
    source 101
    target 836
  ]
  edge [
    source 101
    target 837
  ]
  edge [
    source 101
    target 838
  ]
  edge [
    source 101
    target 839
  ]
  edge [
    source 101
    target 840
  ]
  edge [
    source 101
    target 695
  ]
  edge [
    source 101
    target 841
  ]
  edge [
    source 101
    target 842
  ]
  edge [
    source 101
    target 843
  ]
  edge [
    source 101
    target 844
  ]
  edge [
    source 101
    target 845
  ]
  edge [
    source 101
    target 846
  ]
  edge [
    source 101
    target 847
  ]
  edge [
    source 101
    target 848
  ]
  edge [
    source 101
    target 849
  ]
  edge [
    source 101
    target 850
  ]
  edge [
    source 101
    target 451
  ]
  edge [
    source 101
    target 851
  ]
  edge [
    source 101
    target 852
  ]
  edge [
    source 101
    target 135
  ]
  edge [
    source 102
    target 103
  ]
  edge [
    source 102
    target 104
  ]
  edge [
    source 102
    target 105
  ]
  edge [
    source 102
    target 326
  ]
  edge [
    source 102
    target 853
  ]
  edge [
    source 103
    target 104
  ]
  edge [
    source 103
    target 854
  ]
  edge [
    source 103
    target 855
  ]
  edge [
    source 103
    target 856
  ]
  edge [
    source 103
    target 857
  ]
  edge [
    source 103
    target 858
  ]
  edge [
    source 104
    target 133
  ]
  edge [
    source 104
    target 134
  ]
  edge [
    source 104
    target 859
  ]
  edge [
    source 104
    target 860
  ]
  edge [
    source 104
    target 861
  ]
  edge [
    source 104
    target 862
  ]
  edge [
    source 104
    target 863
  ]
  edge [
    source 104
    target 864
  ]
  edge [
    source 104
    target 865
  ]
  edge [
    source 104
    target 866
  ]
  edge [
    source 104
    target 867
  ]
  edge [
    source 104
    target 868
  ]
  edge [
    source 104
    target 869
  ]
  edge [
    source 104
    target 870
  ]
  edge [
    source 105
    target 106
  ]
  edge [
    source 105
    target 871
  ]
  edge [
    source 105
    target 872
  ]
  edge [
    source 105
    target 873
  ]
  edge [
    source 105
    target 874
  ]
  edge [
    source 105
    target 875
  ]
  edge [
    source 105
    target 876
  ]
  edge [
    source 105
    target 877
  ]
  edge [
    source 105
    target 878
  ]
  edge [
    source 105
    target 879
  ]
  edge [
    source 105
    target 880
  ]
  edge [
    source 105
    target 881
  ]
  edge [
    source 105
    target 882
  ]
  edge [
    source 105
    target 883
  ]
  edge [
    source 105
    target 884
  ]
  edge [
    source 105
    target 885
  ]
  edge [
    source 105
    target 126
  ]
  edge [
    source 106
    target 886
  ]
  edge [
    source 106
    target 887
  ]
  edge [
    source 106
    target 110
  ]
  edge [
    source 106
    target 111
  ]
  edge [
    source 107
    target 108
  ]
  edge [
    source 107
    target 888
  ]
  edge [
    source 107
    target 214
  ]
  edge [
    source 107
    target 889
  ]
  edge [
    source 107
    target 890
  ]
  edge [
    source 107
    target 891
  ]
  edge [
    source 107
    target 892
  ]
  edge [
    source 107
    target 893
  ]
  edge [
    source 107
    target 894
  ]
  edge [
    source 108
    target 109
  ]
  edge [
    source 108
    target 205
  ]
  edge [
    source 108
    target 895
  ]
  edge [
    source 109
    target 110
  ]
  edge [
    source 109
    target 896
  ]
  edge [
    source 109
    target 650
  ]
  edge [
    source 109
    target 897
  ]
  edge [
    source 109
    target 898
  ]
  edge [
    source 109
    target 899
  ]
  edge [
    source 109
    target 617
  ]
  edge [
    source 110
    target 111
  ]
  edge [
    source 111
    target 112
  ]
  edge [
    source 111
    target 900
  ]
  edge [
    source 111
    target 901
  ]
  edge [
    source 111
    target 374
  ]
  edge [
    source 111
    target 902
  ]
  edge [
    source 111
    target 903
  ]
  edge [
    source 111
    target 904
  ]
  edge [
    source 111
    target 905
  ]
  edge [
    source 111
    target 906
  ]
  edge [
    source 111
    target 907
  ]
  edge [
    source 111
    target 908
  ]
  edge [
    source 111
    target 909
  ]
  edge [
    source 111
    target 611
  ]
  edge [
    source 111
    target 910
  ]
  edge [
    source 111
    target 911
  ]
  edge [
    source 111
    target 912
  ]
  edge [
    source 112
    target 113
  ]
  edge [
    source 112
    target 579
  ]
  edge [
    source 112
    target 913
  ]
  edge [
    source 112
    target 914
  ]
  edge [
    source 112
    target 915
  ]
  edge [
    source 112
    target 916
  ]
  edge [
    source 112
    target 917
  ]
  edge [
    source 112
    target 918
  ]
  edge [
    source 113
    target 114
  ]
  edge [
    source 113
    target 919
  ]
  edge [
    source 113
    target 920
  ]
  edge [
    source 113
    target 921
  ]
  edge [
    source 113
    target 922
  ]
  edge [
    source 113
    target 923
  ]
  edge [
    source 113
    target 924
  ]
  edge [
    source 113
    target 925
  ]
  edge [
    source 113
    target 926
  ]
  edge [
    source 113
    target 927
  ]
  edge [
    source 113
    target 610
  ]
  edge [
    source 113
    target 928
  ]
  edge [
    source 114
    target 929
  ]
  edge [
    source 114
    target 930
  ]
  edge [
    source 114
    target 931
  ]
  edge [
    source 114
    target 932
  ]
  edge [
    source 115
    target 116
  ]
  edge [
    source 115
    target 122
  ]
  edge [
    source 115
    target 123
  ]
  edge [
    source 116
    target 117
  ]
  edge [
    source 116
    target 374
  ]
  edge [
    source 116
    target 933
  ]
  edge [
    source 116
    target 577
  ]
  edge [
    source 116
    target 934
  ]
  edge [
    source 116
    target 935
  ]
  edge [
    source 116
    target 936
  ]
  edge [
    source 116
    target 610
  ]
  edge [
    source 116
    target 450
  ]
  edge [
    source 116
    target 912
  ]
  edge [
    source 117
    target 118
  ]
  edge [
    source 117
    target 937
  ]
  edge [
    source 117
    target 938
  ]
  edge [
    source 117
    target 939
  ]
  edge [
    source 117
    target 940
  ]
  edge [
    source 117
    target 941
  ]
  edge [
    source 117
    target 942
  ]
  edge [
    source 118
    target 193
  ]
  edge [
    source 119
    target 120
  ]
  edge [
    source 119
    target 943
  ]
  edge [
    source 119
    target 944
  ]
  edge [
    source 119
    target 945
  ]
  edge [
    source 119
    target 946
  ]
  edge [
    source 119
    target 289
  ]
  edge [
    source 119
    target 788
  ]
  edge [
    source 119
    target 947
  ]
  edge [
    source 120
    target 121
  ]
  edge [
    source 120
    target 948
  ]
  edge [
    source 120
    target 485
  ]
  edge [
    source 120
    target 949
  ]
  edge [
    source 120
    target 950
  ]
  edge [
    source 121
    target 122
  ]
  edge [
    source 121
    target 131
  ]
  edge [
    source 121
    target 132
  ]
  edge [
    source 121
    target 214
  ]
  edge [
    source 121
    target 951
  ]
  edge [
    source 121
    target 952
  ]
  edge [
    source 121
    target 929
  ]
  edge [
    source 121
    target 953
  ]
  edge [
    source 121
    target 954
  ]
  edge [
    source 121
    target 955
  ]
  edge [
    source 121
    target 956
  ]
  edge [
    source 121
    target 957
  ]
  edge [
    source 122
    target 958
  ]
  edge [
    source 122
    target 959
  ]
  edge [
    source 122
    target 960
  ]
  edge [
    source 122
    target 961
  ]
  edge [
    source 122
    target 962
  ]
  edge [
    source 122
    target 963
  ]
  edge [
    source 122
    target 964
  ]
  edge [
    source 122
    target 965
  ]
  edge [
    source 122
    target 966
  ]
  edge [
    source 122
    target 967
  ]
  edge [
    source 122
    target 968
  ]
  edge [
    source 122
    target 780
  ]
  edge [
    source 122
    target 969
  ]
  edge [
    source 122
    target 970
  ]
  edge [
    source 122
    target 971
  ]
  edge [
    source 122
    target 972
  ]
  edge [
    source 122
    target 973
  ]
  edge [
    source 122
    target 974
  ]
  edge [
    source 122
    target 975
  ]
  edge [
    source 122
    target 976
  ]
  edge [
    source 122
    target 977
  ]
  edge [
    source 122
    target 978
  ]
  edge [
    source 122
    target 979
  ]
  edge [
    source 122
    target 980
  ]
  edge [
    source 122
    target 981
  ]
  edge [
    source 122
    target 982
  ]
  edge [
    source 122
    target 983
  ]
  edge [
    source 122
    target 984
  ]
  edge [
    source 122
    target 985
  ]
  edge [
    source 122
    target 986
  ]
  edge [
    source 122
    target 987
  ]
  edge [
    source 122
    target 988
  ]
  edge [
    source 122
    target 989
  ]
  edge [
    source 122
    target 990
  ]
  edge [
    source 122
    target 991
  ]
  edge [
    source 122
    target 992
  ]
  edge [
    source 122
    target 993
  ]
  edge [
    source 122
    target 994
  ]
  edge [
    source 122
    target 995
  ]
  edge [
    source 122
    target 996
  ]
  edge [
    source 122
    target 997
  ]
  edge [
    source 122
    target 998
  ]
  edge [
    source 122
    target 999
  ]
  edge [
    source 122
    target 1000
  ]
  edge [
    source 122
    target 1001
  ]
  edge [
    source 122
    target 1002
  ]
  edge [
    source 123
    target 124
  ]
  edge [
    source 123
    target 1003
  ]
  edge [
    source 123
    target 1004
  ]
  edge [
    source 123
    target 1005
  ]
  edge [
    source 123
    target 1006
  ]
  edge [
    source 123
    target 1007
  ]
  edge [
    source 123
    target 1008
  ]
  edge [
    source 123
    target 1009
  ]
  edge [
    source 124
    target 125
  ]
  edge [
    source 124
    target 1010
  ]
  edge [
    source 124
    target 1011
  ]
  edge [
    source 124
    target 1012
  ]
  edge [
    source 124
    target 1013
  ]
  edge [
    source 124
    target 1014
  ]
  edge [
    source 124
    target 1015
  ]
  edge [
    source 124
    target 1016
  ]
  edge [
    source 124
    target 1017
  ]
  edge [
    source 124
    target 1018
  ]
  edge [
    source 124
    target 1019
  ]
  edge [
    source 124
    target 141
  ]
  edge [
    source 124
    target 159
  ]
  edge [
    source 125
    target 135
  ]
  edge [
    source 125
    target 1020
  ]
  edge [
    source 125
    target 1021
  ]
  edge [
    source 125
    target 1022
  ]
  edge [
    source 125
    target 1023
  ]
  edge [
    source 125
    target 1024
  ]
  edge [
    source 126
    target 617
  ]
  edge [
    source 126
    target 1025
  ]
  edge [
    source 127
    target 1026
  ]
  edge [
    source 127
    target 617
  ]
  edge [
    source 127
    target 1027
  ]
  edge [
    source 127
    target 1028
  ]
  edge [
    source 127
    target 1029
  ]
  edge [
    source 127
    target 611
  ]
  edge [
    source 127
    target 141
  ]
  edge [
    source 127
    target 161
  ]
  edge [
    source 127
    target 170
  ]
  edge [
    source 128
    target 129
  ]
  edge [
    source 128
    target 1030
  ]
  edge [
    source 128
    target 1031
  ]
  edge [
    source 128
    target 1032
  ]
  edge [
    source 128
    target 1033
  ]
  edge [
    source 129
    target 130
  ]
  edge [
    source 129
    target 1034
  ]
  edge [
    source 129
    target 294
  ]
  edge [
    source 130
    target 131
  ]
  edge [
    source 130
    target 1035
  ]
  edge [
    source 130
    target 133
  ]
  edge [
    source 131
    target 616
  ]
  edge [
    source 131
    target 1036
  ]
  edge [
    source 131
    target 1037
  ]
  edge [
    source 131
    target 1038
  ]
  edge [
    source 131
    target 1039
  ]
  edge [
    source 131
    target 382
  ]
  edge [
    source 131
    target 1040
  ]
  edge [
    source 131
    target 1041
  ]
  edge [
    source 131
    target 253
  ]
  edge [
    source 132
    target 1042
  ]
  edge [
    source 132
    target 1043
  ]
  edge [
    source 132
    target 1044
  ]
  edge [
    source 132
    target 1045
  ]
  edge [
    source 132
    target 1046
  ]
  edge [
    source 132
    target 1047
  ]
  edge [
    source 132
    target 1048
  ]
  edge [
    source 132
    target 1049
  ]
  edge [
    source 132
    target 985
  ]
  edge [
    source 132
    target 1050
  ]
  edge [
    source 132
    target 988
  ]
  edge [
    source 132
    target 1051
  ]
  edge [
    source 132
    target 1052
  ]
  edge [
    source 132
    target 1053
  ]
  edge [
    source 132
    target 990
  ]
  edge [
    source 132
    target 1054
  ]
  edge [
    source 132
    target 1055
  ]
  edge [
    source 132
    target 1056
  ]
  edge [
    source 132
    target 993
  ]
  edge [
    source 132
    target 1057
  ]
  edge [
    source 132
    target 1058
  ]
  edge [
    source 132
    target 1059
  ]
  edge [
    source 134
    target 166
  ]
  edge [
    source 134
    target 167
  ]
  edge [
    source 135
    target 136
  ]
  edge [
    source 135
    target 143
  ]
  edge [
    source 135
    target 144
  ]
  edge [
    source 135
    target 1060
  ]
  edge [
    source 135
    target 1061
  ]
  edge [
    source 135
    target 1062
  ]
  edge [
    source 135
    target 1063
  ]
  edge [
    source 135
    target 1064
  ]
  edge [
    source 135
    target 1065
  ]
  edge [
    source 135
    target 1066
  ]
  edge [
    source 135
    target 1067
  ]
  edge [
    source 135
    target 1068
  ]
  edge [
    source 135
    target 1069
  ]
  edge [
    source 135
    target 1070
  ]
  edge [
    source 135
    target 1071
  ]
  edge [
    source 135
    target 1072
  ]
  edge [
    source 135
    target 819
  ]
  edge [
    source 135
    target 619
  ]
  edge [
    source 135
    target 1073
  ]
  edge [
    source 135
    target 1074
  ]
  edge [
    source 135
    target 1075
  ]
  edge [
    source 135
    target 1076
  ]
  edge [
    source 135
    target 1077
  ]
  edge [
    source 135
    target 1078
  ]
  edge [
    source 135
    target 1079
  ]
  edge [
    source 135
    target 1080
  ]
  edge [
    source 135
    target 1081
  ]
  edge [
    source 135
    target 845
  ]
  edge [
    source 135
    target 456
  ]
  edge [
    source 135
    target 1082
  ]
  edge [
    source 135
    target 138
  ]
  edge [
    source 135
    target 1083
  ]
  edge [
    source 135
    target 1084
  ]
  edge [
    source 135
    target 1085
  ]
  edge [
    source 135
    target 1086
  ]
  edge [
    source 135
    target 852
  ]
  edge [
    source 136
    target 1087
  ]
  edge [
    source 136
    target 1088
  ]
  edge [
    source 136
    target 1089
  ]
  edge [
    source 136
    target 1090
  ]
  edge [
    source 136
    target 1091
  ]
  edge [
    source 136
    target 1092
  ]
  edge [
    source 136
    target 1093
  ]
  edge [
    source 136
    target 143
  ]
  edge [
    source 137
    target 138
  ]
  edge [
    source 137
    target 1094
  ]
  edge [
    source 137
    target 650
  ]
  edge [
    source 137
    target 903
  ]
  edge [
    source 137
    target 432
  ]
  edge [
    source 137
    target 1095
  ]
  edge [
    source 137
    target 434
  ]
  edge [
    source 137
    target 1096
  ]
  edge [
    source 137
    target 1097
  ]
  edge [
    source 137
    target 1098
  ]
  edge [
    source 137
    target 610
  ]
  edge [
    source 137
    target 1099
  ]
  edge [
    source 137
    target 1100
  ]
  edge [
    source 137
    target 1101
  ]
  edge [
    source 138
    target 1102
  ]
  edge [
    source 138
    target 1103
  ]
  edge [
    source 138
    target 205
  ]
  edge [
    source 138
    target 1043
  ]
  edge [
    source 138
    target 1104
  ]
  edge [
    source 138
    target 142
  ]
  edge [
    source 139
    target 140
  ]
  edge [
    source 139
    target 173
  ]
  edge [
    source 139
    target 949
  ]
  edge [
    source 139
    target 1105
  ]
  edge [
    source 139
    target 205
  ]
  edge [
    source 140
    target 141
  ]
  edge [
    source 140
    target 1106
  ]
  edge [
    source 140
    target 1107
  ]
  edge [
    source 140
    target 1108
  ]
  edge [
    source 140
    target 1109
  ]
  edge [
    source 140
    target 1110
  ]
  edge [
    source 140
    target 1111
  ]
  edge [
    source 140
    target 1112
  ]
  edge [
    source 140
    target 1113
  ]
  edge [
    source 140
    target 1114
  ]
  edge [
    source 141
    target 142
  ]
  edge [
    source 141
    target 1115
  ]
  edge [
    source 141
    target 1116
  ]
  edge [
    source 141
    target 1117
  ]
  edge [
    source 141
    target 1118
  ]
  edge [
    source 141
    target 1119
  ]
  edge [
    source 141
    target 1120
  ]
  edge [
    source 141
    target 1121
  ]
  edge [
    source 141
    target 1122
  ]
  edge [
    source 141
    target 1123
  ]
  edge [
    source 141
    target 1124
  ]
  edge [
    source 141
    target 1125
  ]
  edge [
    source 141
    target 1126
  ]
  edge [
    source 141
    target 1127
  ]
  edge [
    source 141
    target 1128
  ]
  edge [
    source 141
    target 1129
  ]
  edge [
    source 141
    target 1130
  ]
  edge [
    source 141
    target 159
  ]
  edge [
    source 141
    target 161
  ]
  edge [
    source 141
    target 170
  ]
  edge [
    source 142
    target 1131
  ]
  edge [
    source 142
    target 301
  ]
  edge [
    source 142
    target 485
  ]
  edge [
    source 142
    target 1132
  ]
  edge [
    source 142
    target 161
  ]
  edge [
    source 142
    target 1133
  ]
  edge [
    source 142
    target 1134
  ]
  edge [
    source 142
    target 1135
  ]
  edge [
    source 142
    target 1136
  ]
  edge [
    source 142
    target 1137
  ]
  edge [
    source 142
    target 1138
  ]
  edge [
    source 142
    target 1139
  ]
  edge [
    source 142
    target 1140
  ]
  edge [
    source 142
    target 449
  ]
  edge [
    source 142
    target 1141
  ]
  edge [
    source 142
    target 1142
  ]
  edge [
    source 142
    target 1143
  ]
  edge [
    source 142
    target 1144
  ]
  edge [
    source 142
    target 1145
  ]
  edge [
    source 142
    target 740
  ]
  edge [
    source 142
    target 1146
  ]
  edge [
    source 142
    target 1147
  ]
  edge [
    source 142
    target 197
  ]
  edge [
    source 142
    target 1148
  ]
  edge [
    source 142
    target 1149
  ]
  edge [
    source 142
    target 1150
  ]
  edge [
    source 142
    target 1151
  ]
  edge [
    source 142
    target 549
  ]
  edge [
    source 142
    target 1152
  ]
  edge [
    source 142
    target 451
  ]
  edge [
    source 142
    target 1153
  ]
  edge [
    source 142
    target 1154
  ]
  edge [
    source 143
    target 1155
  ]
  edge [
    source 143
    target 1156
  ]
  edge [
    source 144
    target 145
  ]
  edge [
    source 144
    target 1157
  ]
  edge [
    source 144
    target 1158
  ]
  edge [
    source 144
    target 1159
  ]
  edge [
    source 144
    target 1160
  ]
  edge [
    source 144
    target 1088
  ]
  edge [
    source 144
    target 1161
  ]
  edge [
    source 144
    target 1089
  ]
  edge [
    source 144
    target 1091
  ]
  edge [
    source 144
    target 1092
  ]
  edge [
    source 144
    target 1162
  ]
  edge [
    source 145
    target 146
  ]
  edge [
    source 145
    target 1163
  ]
  edge [
    source 145
    target 933
  ]
  edge [
    source 145
    target 1164
  ]
  edge [
    source 145
    target 1165
  ]
  edge [
    source 145
    target 807
  ]
  edge [
    source 145
    target 925
  ]
  edge [
    source 145
    target 1166
  ]
  edge [
    source 145
    target 157
  ]
  edge [
    source 145
    target 163
  ]
  edge [
    source 146
    target 147
  ]
  edge [
    source 146
    target 841
  ]
  edge [
    source 146
    target 1167
  ]
  edge [
    source 147
    target 148
  ]
  edge [
    source 147
    target 1168
  ]
  edge [
    source 147
    target 1169
  ]
  edge [
    source 147
    target 1170
  ]
  edge [
    source 147
    target 1171
  ]
  edge [
    source 147
    target 1172
  ]
  edge [
    source 147
    target 1173
  ]
  edge [
    source 147
    target 1174
  ]
  edge [
    source 147
    target 1175
  ]
  edge [
    source 148
    target 149
  ]
  edge [
    source 148
    target 1176
  ]
  edge [
    source 148
    target 1177
  ]
  edge [
    source 148
    target 1178
  ]
  edge [
    source 148
    target 1179
  ]
  edge [
    source 148
    target 1180
  ]
  edge [
    source 148
    target 1181
  ]
  edge [
    source 148
    target 847
  ]
  edge [
    source 148
    target 1182
  ]
  edge [
    source 148
    target 1183
  ]
  edge [
    source 148
    target 1184
  ]
  edge [
    source 148
    target 1185
  ]
  edge [
    source 148
    target 1186
  ]
  edge [
    source 148
    target 1105
  ]
  edge [
    source 149
    target 150
  ]
  edge [
    source 149
    target 1187
  ]
  edge [
    source 149
    target 1188
  ]
  edge [
    source 149
    target 1189
  ]
  edge [
    source 149
    target 1190
  ]
  edge [
    source 149
    target 1191
  ]
  edge [
    source 149
    target 168
  ]
  edge [
    source 150
    target 151
  ]
  edge [
    source 150
    target 1192
  ]
  edge [
    source 151
    target 152
  ]
  edge [
    source 151
    target 170
  ]
  edge [
    source 151
    target 1193
  ]
  edge [
    source 151
    target 650
  ]
  edge [
    source 151
    target 1194
  ]
  edge [
    source 151
    target 1195
  ]
  edge [
    source 151
    target 1196
  ]
  edge [
    source 151
    target 1197
  ]
  edge [
    source 151
    target 1198
  ]
  edge [
    source 151
    target 1199
  ]
  edge [
    source 151
    target 1200
  ]
  edge [
    source 151
    target 1201
  ]
  edge [
    source 152
    target 153
  ]
  edge [
    source 152
    target 906
  ]
  edge [
    source 152
    target 1202
  ]
  edge [
    source 152
    target 1203
  ]
  edge [
    source 152
    target 909
  ]
  edge [
    source 153
    target 154
  ]
  edge [
    source 153
    target 214
  ]
  edge [
    source 153
    target 1204
  ]
  edge [
    source 153
    target 1205
  ]
  edge [
    source 153
    target 1206
  ]
  edge [
    source 153
    target 1207
  ]
  edge [
    source 153
    target 1208
  ]
  edge [
    source 153
    target 1209
  ]
  edge [
    source 154
    target 155
  ]
  edge [
    source 154
    target 1210
  ]
  edge [
    source 154
    target 1211
  ]
  edge [
    source 154
    target 1212
  ]
  edge [
    source 154
    target 1213
  ]
  edge [
    source 155
    target 156
  ]
  edge [
    source 155
    target 1214
  ]
  edge [
    source 155
    target 1215
  ]
  edge [
    source 155
    target 1216
  ]
  edge [
    source 155
    target 1217
  ]
  edge [
    source 155
    target 1218
  ]
  edge [
    source 155
    target 1219
  ]
  edge [
    source 156
    target 157
  ]
  edge [
    source 156
    target 169
  ]
  edge [
    source 157
    target 158
  ]
  edge [
    source 157
    target 1220
  ]
  edge [
    source 157
    target 1221
  ]
  edge [
    source 157
    target 1222
  ]
  edge [
    source 157
    target 1223
  ]
  edge [
    source 157
    target 1224
  ]
  edge [
    source 157
    target 163
  ]
  edge [
    source 158
    target 159
  ]
  edge [
    source 159
    target 160
  ]
  edge [
    source 159
    target 1225
  ]
  edge [
    source 159
    target 1226
  ]
  edge [
    source 160
    target 161
  ]
  edge [
    source 160
    target 301
  ]
  edge [
    source 160
    target 1227
  ]
  edge [
    source 160
    target 1228
  ]
  edge [
    source 160
    target 1229
  ]
  edge [
    source 160
    target 1230
  ]
  edge [
    source 160
    target 1231
  ]
  edge [
    source 160
    target 428
  ]
  edge [
    source 160
    target 1232
  ]
  edge [
    source 161
    target 162
  ]
  edge [
    source 161
    target 845
  ]
  edge [
    source 161
    target 830
  ]
  edge [
    source 161
    target 1233
  ]
  edge [
    source 161
    target 1234
  ]
  edge [
    source 161
    target 1235
  ]
  edge [
    source 161
    target 301
  ]
  edge [
    source 161
    target 1236
  ]
  edge [
    source 161
    target 1237
  ]
  edge [
    source 161
    target 1238
  ]
  edge [
    source 161
    target 1239
  ]
  edge [
    source 161
    target 1240
  ]
  edge [
    source 161
    target 170
  ]
  edge [
    source 162
    target 163
  ]
  edge [
    source 162
    target 1241
  ]
  edge [
    source 162
    target 642
  ]
  edge [
    source 162
    target 1242
  ]
  edge [
    source 162
    target 1243
  ]
  edge [
    source 162
    target 430
  ]
  edge [
    source 163
    target 164
  ]
  edge [
    source 163
    target 169
  ]
  edge [
    source 163
    target 1244
  ]
  edge [
    source 164
    target 165
  ]
  edge [
    source 165
    target 166
  ]
  edge [
    source 167
    target 168
  ]
  edge [
    source 167
    target 1245
  ]
  edge [
    source 167
    target 1246
  ]
  edge [
    source 167
    target 1247
  ]
  edge [
    source 167
    target 1248
  ]
  edge [
    source 168
    target 1249
  ]
  edge [
    source 168
    target 1250
  ]
  edge [
    source 168
    target 1251
  ]
  edge [
    source 168
    target 1252
  ]
  edge [
    source 168
    target 556
  ]
  edge [
    source 168
    target 1247
  ]
  edge [
    source 168
    target 1253
  ]
  edge [
    source 168
    target 1254
  ]
  edge [
    source 170
    target 710
  ]
  edge [
    source 170
    target 1255
  ]
]
