graph [
  maxDegree 32
  minDegree 1
  meanDegree 2.0994475138121547
  density 0.005815644082582146
  graphCliqueNumber 3
  node [
    id 0
    label "nieprawda"
    origin "text"
  ]
  node [
    id 1
    label "parena&#347;cie"
    origin "text"
  ]
  node [
    id 2
    label "milion"
    origin "text"
  ]
  node [
    id 3
    label "tw&#243;rca"
    origin "text"
  ]
  node [
    id 4
    label "wcale"
    origin "text"
  ]
  node [
    id 5
    label "tychy"
    origin "text"
  ]
  node [
    id 6
    label "pieni&#261;dz"
    origin "text"
  ]
  node [
    id 7
    label "dosta&#263;"
    origin "text"
  ]
  node [
    id 8
    label "instytucja"
    origin "text"
  ]
  node [
    id 9
    label "dobrze"
    origin "text"
  ]
  node [
    id 10
    label "wiedza"
    origin "text"
  ]
  node [
    id 11
    label "stosowa&#263;"
    origin "text"
  ]
  node [
    id 12
    label "si&#281;"
    origin "text"
  ]
  node [
    id 13
    label "literalnie"
    origin "text"
  ]
  node [
    id 14
    label "obowi&#261;zuj&#261;cy"
    origin "text"
  ]
  node [
    id 15
    label "przepis"
    origin "text"
  ]
  node [
    id 16
    label "prawa"
    origin "text"
  ]
  node [
    id 17
    label "autorski"
    origin "text"
  ]
  node [
    id 18
    label "podpisa&#263;by"
    origin "text"
  ]
  node [
    id 19
    label "siebie"
    origin "text"
  ]
  node [
    id 20
    label "wyrok"
    origin "text"
  ]
  node [
    id 21
    label "&#347;mier&#263;"
    origin "text"
  ]
  node [
    id 22
    label "cud"
    origin "text"
  ]
  node [
    id 23
    label "kreatywny"
    origin "text"
  ]
  node [
    id 24
    label "interpretacja"
    origin "text"
  ]
  node [
    id 25
    label "trafia&#263;"
    origin "text"
  ]
  node [
    id 26
    label "tylko"
    origin "text"
  ]
  node [
    id 27
    label "niewielki"
    origin "text"
  ]
  node [
    id 28
    label "wybrana"
    origin "text"
  ]
  node [
    id 29
    label "grupa"
    origin "text"
  ]
  node [
    id 30
    label "bogus&#322;aw"
    origin "text"
  ]
  node [
    id 31
    label "pluta"
    origin "text"
  ]
  node [
    id 32
    label "zpav"
    origin "text"
  ]
  node [
    id 33
    label "podczas"
    origin "text"
  ]
  node [
    id 34
    label "debata"
    origin "text"
  ]
  node [
    id 35
    label "wok&#243;&#322;"
    origin "text"
  ]
  node [
    id 36
    label "film"
    origin "text"
  ]
  node [
    id 37
    label "good"
    origin "text"
  ]
  node [
    id 38
    label "copy"
    origin "text"
  ]
  node [
    id 39
    label "bad"
    origin "text"
  ]
  node [
    id 40
    label "wprost"
    origin "text"
  ]
  node [
    id 41
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 42
    label "tytu&#322;owy"
    origin "text"
  ]
  node [
    id 43
    label "miliard"
    origin "text"
  ]
  node [
    id 44
    label "by&#263;"
    origin "text"
  ]
  node [
    id 45
    label "dla"
    origin "text"
  ]
  node [
    id 46
    label "spory"
    origin "text"
  ]
  node [
    id 47
    label "k&#322;opot"
    origin "text"
  ]
  node [
    id 48
    label "pewnie"
    origin "text"
  ]
  node [
    id 49
    label "tak"
    origin "text"
  ]
  node [
    id 50
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 51
    label "bywa&#263;"
    origin "text"
  ]
  node [
    id 52
    label "rzeczywisto&#347;&#263;"
    origin "text"
  ]
  node [
    id 53
    label "s&#261;d"
  ]
  node [
    id 54
    label "u&#347;miercenie"
  ]
  node [
    id 55
    label "fa&#322;szywo&#347;&#263;"
  ]
  node [
    id 56
    label "u&#347;mierci&#263;"
  ]
  node [
    id 57
    label "przeciwstawno&#347;&#263;"
  ]
  node [
    id 58
    label "fib"
  ]
  node [
    id 59
    label "liczba"
  ]
  node [
    id 60
    label "miljon"
  ]
  node [
    id 61
    label "ba&#324;ka"
  ]
  node [
    id 62
    label "pomys&#322;odawca"
  ]
  node [
    id 63
    label "kszta&#322;ciciel"
  ]
  node [
    id 64
    label "tworzyciel"
  ]
  node [
    id 65
    label "&#347;w"
  ]
  node [
    id 66
    label "wykonawca"
  ]
  node [
    id 67
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 68
    label "ni_chuja"
  ]
  node [
    id 69
    label "ca&#322;kiem"
  ]
  node [
    id 70
    label "zupe&#322;nie"
  ]
  node [
    id 71
    label "poda&#380;_pieni&#261;dza"
  ]
  node [
    id 72
    label "zdewaluowa&#263;"
  ]
  node [
    id 73
    label "moniak"
  ]
  node [
    id 74
    label "wytw&#243;r"
  ]
  node [
    id 75
    label "zdewaluowanie"
  ]
  node [
    id 76
    label "jednostka_monetarna"
  ]
  node [
    id 77
    label "&#347;rodek_p&#322;atniczy"
  ]
  node [
    id 78
    label "numizmat"
  ]
  node [
    id 79
    label "rozmienianie"
  ]
  node [
    id 80
    label "rozmienienie"
  ]
  node [
    id 81
    label "rozmieni&#263;"
  ]
  node [
    id 82
    label "dewaluowanie"
  ]
  node [
    id 83
    label "nomina&#322;"
  ]
  node [
    id 84
    label "coin"
  ]
  node [
    id 85
    label "dewaluowa&#263;"
  ]
  node [
    id 86
    label "pieni&#261;dze"
  ]
  node [
    id 87
    label "znak_pieni&#281;&#380;ny"
  ]
  node [
    id 88
    label "rozmienia&#263;"
  ]
  node [
    id 89
    label "get"
  ]
  node [
    id 90
    label "doczeka&#263;"
  ]
  node [
    id 91
    label "zwiastun"
  ]
  node [
    id 92
    label "si&#281;gn&#261;&#263;"
  ]
  node [
    id 93
    label "develop"
  ]
  node [
    id 94
    label "catch"
  ]
  node [
    id 95
    label "uzyska&#263;"
  ]
  node [
    id 96
    label "kupi&#263;"
  ]
  node [
    id 97
    label "wzi&#261;&#263;"
  ]
  node [
    id 98
    label "naby&#263;"
  ]
  node [
    id 99
    label "nabawienie_si&#281;"
  ]
  node [
    id 100
    label "obskoczy&#263;"
  ]
  node [
    id 101
    label "zapanowa&#263;"
  ]
  node [
    id 102
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 103
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 104
    label "zrobi&#263;"
  ]
  node [
    id 105
    label "nabawianie_si&#281;"
  ]
  node [
    id 106
    label "range"
  ]
  node [
    id 107
    label "schorzenie"
  ]
  node [
    id 108
    label "wystarczy&#263;"
  ]
  node [
    id 109
    label "wysta&#263;"
  ]
  node [
    id 110
    label "afiliowa&#263;"
  ]
  node [
    id 111
    label "osoba_prawna"
  ]
  node [
    id 112
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 113
    label "urz&#261;d"
  ]
  node [
    id 114
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 115
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 116
    label "establishment"
  ]
  node [
    id 117
    label "standard"
  ]
  node [
    id 118
    label "organizacja"
  ]
  node [
    id 119
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 120
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 121
    label "zamykanie"
  ]
  node [
    id 122
    label "zamyka&#263;"
  ]
  node [
    id 123
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 124
    label "poj&#281;cie"
  ]
  node [
    id 125
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 126
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 127
    label "Fundusze_Unijne"
  ]
  node [
    id 128
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 129
    label "biuro"
  ]
  node [
    id 130
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 131
    label "moralnie"
  ]
  node [
    id 132
    label "wiele"
  ]
  node [
    id 133
    label "lepiej"
  ]
  node [
    id 134
    label "korzystnie"
  ]
  node [
    id 135
    label "pomy&#347;lnie"
  ]
  node [
    id 136
    label "pozytywnie"
  ]
  node [
    id 137
    label "dobry"
  ]
  node [
    id 138
    label "dobroczynnie"
  ]
  node [
    id 139
    label "odpowiednio"
  ]
  node [
    id 140
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 141
    label "skutecznie"
  ]
  node [
    id 142
    label "pozwolenie"
  ]
  node [
    id 143
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 144
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 145
    label "wykszta&#322;cenie"
  ]
  node [
    id 146
    label "zaawansowanie"
  ]
  node [
    id 147
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 148
    label "intelekt"
  ]
  node [
    id 149
    label "cognition"
  ]
  node [
    id 150
    label "u&#380;ywa&#263;"
  ]
  node [
    id 151
    label "literally"
  ]
  node [
    id 152
    label "wiernie"
  ]
  node [
    id 153
    label "prawdziwie"
  ]
  node [
    id 154
    label "dos&#322;owny"
  ]
  node [
    id 155
    label "obowi&#261;zuj&#261;co"
  ]
  node [
    id 156
    label "uprawomocnianie_si&#281;"
  ]
  node [
    id 157
    label "uprawomocnienie_si&#281;"
  ]
  node [
    id 158
    label "aktualny"
  ]
  node [
    id 159
    label "przedawnienie_si&#281;"
  ]
  node [
    id 160
    label "recepta"
  ]
  node [
    id 161
    label "norma_prawna"
  ]
  node [
    id 162
    label "kodeks"
  ]
  node [
    id 163
    label "prawo"
  ]
  node [
    id 164
    label "regulation"
  ]
  node [
    id 165
    label "przedawni&#263;_si&#281;"
  ]
  node [
    id 166
    label "porada"
  ]
  node [
    id 167
    label "przedawnianie_si&#281;"
  ]
  node [
    id 168
    label "spos&#243;b"
  ]
  node [
    id 169
    label "przedawnia&#263;_si&#281;"
  ]
  node [
    id 170
    label "w&#322;asny"
  ]
  node [
    id 171
    label "oryginalny"
  ]
  node [
    id 172
    label "autorsko"
  ]
  node [
    id 173
    label "orzeczenie"
  ]
  node [
    id 174
    label "order"
  ]
  node [
    id 175
    label "wydarzenie"
  ]
  node [
    id 176
    label "kara"
  ]
  node [
    id 177
    label "judgment"
  ]
  node [
    id 178
    label "sentencja"
  ]
  node [
    id 179
    label "kres_&#380;ycia"
  ]
  node [
    id 180
    label "defenestracja"
  ]
  node [
    id 181
    label "kres"
  ]
  node [
    id 182
    label "agonia"
  ]
  node [
    id 183
    label "&#380;ycie"
  ]
  node [
    id 184
    label "szeol"
  ]
  node [
    id 185
    label "mogi&#322;a"
  ]
  node [
    id 186
    label "pogrzeb"
  ]
  node [
    id 187
    label "istota_nadprzyrodzona"
  ]
  node [
    id 188
    label "pogrzebanie"
  ]
  node [
    id 189
    label "&#380;a&#322;oba"
  ]
  node [
    id 190
    label "zabicie"
  ]
  node [
    id 191
    label "upadek"
  ]
  node [
    id 192
    label "achiropita"
  ]
  node [
    id 193
    label "zjawisko"
  ]
  node [
    id 194
    label "rzadko&#347;&#263;"
  ]
  node [
    id 195
    label "niecodzienno&#347;&#263;"
  ]
  node [
    id 196
    label "kreatywnie"
  ]
  node [
    id 197
    label "zmienny"
  ]
  node [
    id 198
    label "pomys&#322;owy"
  ]
  node [
    id 199
    label "sprzyjaj&#261;cy"
  ]
  node [
    id 200
    label "tw&#243;rczo"
  ]
  node [
    id 201
    label "inspiruj&#261;cy"
  ]
  node [
    id 202
    label "wypowied&#378;"
  ]
  node [
    id 203
    label "kontekst"
  ]
  node [
    id 204
    label "obja&#347;nienie"
  ]
  node [
    id 205
    label "wypracowanie"
  ]
  node [
    id 206
    label "hermeneutyka"
  ]
  node [
    id 207
    label "interpretation"
  ]
  node [
    id 208
    label "explanation"
  ]
  node [
    id 209
    label "realizacja"
  ]
  node [
    id 210
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 211
    label "wpada&#263;"
  ]
  node [
    id 212
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 213
    label "dolatywa&#263;"
  ]
  node [
    id 214
    label "happen"
  ]
  node [
    id 215
    label "zjawia&#263;_si&#281;"
  ]
  node [
    id 216
    label "hit"
  ]
  node [
    id 217
    label "spotyka&#263;"
  ]
  node [
    id 218
    label "indicate"
  ]
  node [
    id 219
    label "znajdowa&#263;"
  ]
  node [
    id 220
    label "dosi&#281;ga&#263;"
  ]
  node [
    id 221
    label "pocisk"
  ]
  node [
    id 222
    label "dociera&#263;"
  ]
  node [
    id 223
    label "dopasowywa&#263;_si&#281;"
  ]
  node [
    id 224
    label "ma&#322;o"
  ]
  node [
    id 225
    label "nielicznie"
  ]
  node [
    id 226
    label "ma&#322;y"
  ]
  node [
    id 227
    label "niewa&#380;ny"
  ]
  node [
    id 228
    label "odm&#322;adza&#263;"
  ]
  node [
    id 229
    label "asymilowa&#263;"
  ]
  node [
    id 230
    label "cz&#261;steczka"
  ]
  node [
    id 231
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 232
    label "egzemplarz"
  ]
  node [
    id 233
    label "formacja_geologiczna"
  ]
  node [
    id 234
    label "harcerze_starsi"
  ]
  node [
    id 235
    label "liga"
  ]
  node [
    id 236
    label "Terranie"
  ]
  node [
    id 237
    label "&#346;wietliki"
  ]
  node [
    id 238
    label "pakiet_klimatyczny"
  ]
  node [
    id 239
    label "oddzia&#322;"
  ]
  node [
    id 240
    label "stage_set"
  ]
  node [
    id 241
    label "Entuzjastki"
  ]
  node [
    id 242
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 243
    label "odm&#322;odzenie"
  ]
  node [
    id 244
    label "type"
  ]
  node [
    id 245
    label "category"
  ]
  node [
    id 246
    label "asymilowanie"
  ]
  node [
    id 247
    label "specgrupa"
  ]
  node [
    id 248
    label "odm&#322;adzanie"
  ]
  node [
    id 249
    label "gromada"
  ]
  node [
    id 250
    label "Eurogrupa"
  ]
  node [
    id 251
    label "jednostka_systematyczna"
  ]
  node [
    id 252
    label "kompozycja"
  ]
  node [
    id 253
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 254
    label "zbi&#243;r"
  ]
  node [
    id 255
    label "rozmowa"
  ]
  node [
    id 256
    label "sympozjon"
  ]
  node [
    id 257
    label "conference"
  ]
  node [
    id 258
    label "blisko"
  ]
  node [
    id 259
    label "wsz&#281;dzie"
  ]
  node [
    id 260
    label "rozbieg&#243;wka"
  ]
  node [
    id 261
    label "block"
  ]
  node [
    id 262
    label "odczula&#263;"
  ]
  node [
    id 263
    label "blik"
  ]
  node [
    id 264
    label "rola"
  ]
  node [
    id 265
    label "trawiarnia"
  ]
  node [
    id 266
    label "b&#322;ona"
  ]
  node [
    id 267
    label "filmoteka"
  ]
  node [
    id 268
    label "sztuka"
  ]
  node [
    id 269
    label "muza"
  ]
  node [
    id 270
    label "odczuli&#263;"
  ]
  node [
    id 271
    label "klatka"
  ]
  node [
    id 272
    label "odczulenie"
  ]
  node [
    id 273
    label "emulsja_fotograficzna"
  ]
  node [
    id 274
    label "animatronika"
  ]
  node [
    id 275
    label "dorobek"
  ]
  node [
    id 276
    label "odczulanie"
  ]
  node [
    id 277
    label "scena"
  ]
  node [
    id 278
    label "czo&#322;&#243;wka"
  ]
  node [
    id 279
    label "ty&#322;&#243;wka"
  ]
  node [
    id 280
    label "napisy"
  ]
  node [
    id 281
    label "photograph"
  ]
  node [
    id 282
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 283
    label "postprodukcja"
  ]
  node [
    id 284
    label "sklejarka"
  ]
  node [
    id 285
    label "anamorfoza"
  ]
  node [
    id 286
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 287
    label "ta&#347;ma"
  ]
  node [
    id 288
    label "b&#322;ona_&#347;wiat&#322;oczu&#322;a"
  ]
  node [
    id 289
    label "uj&#281;cie"
  ]
  node [
    id 290
    label "otwarcie"
  ]
  node [
    id 291
    label "prosto"
  ]
  node [
    id 292
    label "naprzeciwko"
  ]
  node [
    id 293
    label "remark"
  ]
  node [
    id 294
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 295
    label "okre&#347;la&#263;"
  ]
  node [
    id 296
    label "j&#281;zyk"
  ]
  node [
    id 297
    label "say"
  ]
  node [
    id 298
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 299
    label "formu&#322;owa&#263;"
  ]
  node [
    id 300
    label "talk"
  ]
  node [
    id 301
    label "powiada&#263;"
  ]
  node [
    id 302
    label "informowa&#263;"
  ]
  node [
    id 303
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 304
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 305
    label "wydobywa&#263;"
  ]
  node [
    id 306
    label "express"
  ]
  node [
    id 307
    label "chew_the_fat"
  ]
  node [
    id 308
    label "dysfonia"
  ]
  node [
    id 309
    label "umie&#263;"
  ]
  node [
    id 310
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 311
    label "tell"
  ]
  node [
    id 312
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 313
    label "wyra&#380;a&#263;"
  ]
  node [
    id 314
    label "gaworzy&#263;"
  ]
  node [
    id 315
    label "rozmawia&#263;"
  ]
  node [
    id 316
    label "dziama&#263;"
  ]
  node [
    id 317
    label "prawi&#263;"
  ]
  node [
    id 318
    label "g&#322;&#243;wny"
  ]
  node [
    id 319
    label "si&#281;ga&#263;"
  ]
  node [
    id 320
    label "trwa&#263;"
  ]
  node [
    id 321
    label "obecno&#347;&#263;"
  ]
  node [
    id 322
    label "stan"
  ]
  node [
    id 323
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 324
    label "stand"
  ]
  node [
    id 325
    label "mie&#263;_miejsce"
  ]
  node [
    id 326
    label "uczestniczy&#263;"
  ]
  node [
    id 327
    label "chodzi&#263;"
  ]
  node [
    id 328
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 329
    label "equal"
  ]
  node [
    id 330
    label "sporo"
  ]
  node [
    id 331
    label "intensywny"
  ]
  node [
    id 332
    label "wa&#380;ny"
  ]
  node [
    id 333
    label "subiekcja"
  ]
  node [
    id 334
    label "problem"
  ]
  node [
    id 335
    label "zwinnie"
  ]
  node [
    id 336
    label "bezpiecznie"
  ]
  node [
    id 337
    label "wiarygodnie"
  ]
  node [
    id 338
    label "pewniej"
  ]
  node [
    id 339
    label "pewny"
  ]
  node [
    id 340
    label "mocno"
  ]
  node [
    id 341
    label "najpewniej"
  ]
  node [
    id 342
    label "proceed"
  ]
  node [
    id 343
    label "pokazywa&#263;_si&#281;"
  ]
  node [
    id 344
    label "zdarza&#263;_si&#281;"
  ]
  node [
    id 345
    label "pozosta&#263;"
  ]
  node [
    id 346
    label "&#380;egna&#263;"
  ]
  node [
    id 347
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 348
    label "real"
  ]
  node [
    id 349
    label "&#347;wiat"
  ]
  node [
    id 350
    label "cecha"
  ]
  node [
    id 351
    label "dar"
  ]
  node [
    id 352
    label "message"
  ]
  node [
    id 353
    label "Bogus&#322;awa"
  ]
  node [
    id 354
    label "Good"
  ]
  node [
    id 355
    label "Copy"
  ]
  node [
    id 356
    label "komunikacja"
  ]
  node [
    id 357
    label "elektroniczny"
  ]
  node [
    id 358
    label "gazeta"
  ]
  node [
    id 359
    label "wyborczy"
  ]
  node [
    id 360
    label "Korwin"
  ]
  node [
    id 361
    label "Mikke"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 15
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 8
    target 122
  ]
  edge [
    source 8
    target 123
  ]
  edge [
    source 8
    target 124
  ]
  edge [
    source 8
    target 125
  ]
  edge [
    source 8
    target 126
  ]
  edge [
    source 8
    target 127
  ]
  edge [
    source 8
    target 128
  ]
  edge [
    source 8
    target 129
  ]
  edge [
    source 8
    target 130
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 131
  ]
  edge [
    source 9
    target 132
  ]
  edge [
    source 9
    target 133
  ]
  edge [
    source 9
    target 134
  ]
  edge [
    source 9
    target 135
  ]
  edge [
    source 9
    target 136
  ]
  edge [
    source 9
    target 137
  ]
  edge [
    source 9
    target 138
  ]
  edge [
    source 9
    target 139
  ]
  edge [
    source 9
    target 140
  ]
  edge [
    source 9
    target 141
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 142
  ]
  edge [
    source 10
    target 143
  ]
  edge [
    source 10
    target 144
  ]
  edge [
    source 10
    target 145
  ]
  edge [
    source 10
    target 146
  ]
  edge [
    source 10
    target 147
  ]
  edge [
    source 10
    target 148
  ]
  edge [
    source 10
    target 149
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 150
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 151
  ]
  edge [
    source 13
    target 152
  ]
  edge [
    source 13
    target 153
  ]
  edge [
    source 13
    target 154
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 155
  ]
  edge [
    source 14
    target 156
  ]
  edge [
    source 14
    target 157
  ]
  edge [
    source 14
    target 158
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 24
  ]
  edge [
    source 15
    target 159
  ]
  edge [
    source 15
    target 160
  ]
  edge [
    source 15
    target 161
  ]
  edge [
    source 15
    target 162
  ]
  edge [
    source 15
    target 163
  ]
  edge [
    source 15
    target 164
  ]
  edge [
    source 15
    target 165
  ]
  edge [
    source 15
    target 166
  ]
  edge [
    source 15
    target 167
  ]
  edge [
    source 15
    target 168
  ]
  edge [
    source 15
    target 169
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 25
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 170
  ]
  edge [
    source 17
    target 171
  ]
  edge [
    source 17
    target 172
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 173
  ]
  edge [
    source 20
    target 174
  ]
  edge [
    source 20
    target 175
  ]
  edge [
    source 20
    target 176
  ]
  edge [
    source 20
    target 177
  ]
  edge [
    source 20
    target 178
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 179
  ]
  edge [
    source 21
    target 180
  ]
  edge [
    source 21
    target 181
  ]
  edge [
    source 21
    target 182
  ]
  edge [
    source 21
    target 183
  ]
  edge [
    source 21
    target 184
  ]
  edge [
    source 21
    target 185
  ]
  edge [
    source 21
    target 186
  ]
  edge [
    source 21
    target 187
  ]
  edge [
    source 21
    target 188
  ]
  edge [
    source 21
    target 189
  ]
  edge [
    source 21
    target 190
  ]
  edge [
    source 21
    target 191
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 192
  ]
  edge [
    source 22
    target 193
  ]
  edge [
    source 22
    target 194
  ]
  edge [
    source 22
    target 195
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 196
  ]
  edge [
    source 23
    target 197
  ]
  edge [
    source 23
    target 171
  ]
  edge [
    source 23
    target 198
  ]
  edge [
    source 23
    target 199
  ]
  edge [
    source 23
    target 200
  ]
  edge [
    source 23
    target 201
  ]
  edge [
    source 24
    target 202
  ]
  edge [
    source 24
    target 74
  ]
  edge [
    source 24
    target 203
  ]
  edge [
    source 24
    target 204
  ]
  edge [
    source 24
    target 205
  ]
  edge [
    source 24
    target 206
  ]
  edge [
    source 24
    target 207
  ]
  edge [
    source 24
    target 168
  ]
  edge [
    source 24
    target 208
  ]
  edge [
    source 24
    target 209
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 210
  ]
  edge [
    source 25
    target 211
  ]
  edge [
    source 25
    target 212
  ]
  edge [
    source 25
    target 213
  ]
  edge [
    source 25
    target 214
  ]
  edge [
    source 25
    target 215
  ]
  edge [
    source 25
    target 216
  ]
  edge [
    source 25
    target 217
  ]
  edge [
    source 25
    target 218
  ]
  edge [
    source 25
    target 219
  ]
  edge [
    source 25
    target 220
  ]
  edge [
    source 25
    target 221
  ]
  edge [
    source 25
    target 222
  ]
  edge [
    source 25
    target 223
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 224
  ]
  edge [
    source 27
    target 225
  ]
  edge [
    source 27
    target 226
  ]
  edge [
    source 27
    target 227
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 228
  ]
  edge [
    source 29
    target 229
  ]
  edge [
    source 29
    target 230
  ]
  edge [
    source 29
    target 231
  ]
  edge [
    source 29
    target 232
  ]
  edge [
    source 29
    target 233
  ]
  edge [
    source 29
    target 143
  ]
  edge [
    source 29
    target 234
  ]
  edge [
    source 29
    target 235
  ]
  edge [
    source 29
    target 236
  ]
  edge [
    source 29
    target 237
  ]
  edge [
    source 29
    target 238
  ]
  edge [
    source 29
    target 239
  ]
  edge [
    source 29
    target 240
  ]
  edge [
    source 29
    target 241
  ]
  edge [
    source 29
    target 242
  ]
  edge [
    source 29
    target 243
  ]
  edge [
    source 29
    target 244
  ]
  edge [
    source 29
    target 245
  ]
  edge [
    source 29
    target 246
  ]
  edge [
    source 29
    target 247
  ]
  edge [
    source 29
    target 248
  ]
  edge [
    source 29
    target 249
  ]
  edge [
    source 29
    target 250
  ]
  edge [
    source 29
    target 251
  ]
  edge [
    source 29
    target 252
  ]
  edge [
    source 29
    target 253
  ]
  edge [
    source 29
    target 254
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 353
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 255
  ]
  edge [
    source 34
    target 256
  ]
  edge [
    source 34
    target 257
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 258
  ]
  edge [
    source 35
    target 259
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 260
  ]
  edge [
    source 36
    target 261
  ]
  edge [
    source 36
    target 262
  ]
  edge [
    source 36
    target 263
  ]
  edge [
    source 36
    target 264
  ]
  edge [
    source 36
    target 265
  ]
  edge [
    source 36
    target 266
  ]
  edge [
    source 36
    target 267
  ]
  edge [
    source 36
    target 268
  ]
  edge [
    source 36
    target 269
  ]
  edge [
    source 36
    target 270
  ]
  edge [
    source 36
    target 271
  ]
  edge [
    source 36
    target 272
  ]
  edge [
    source 36
    target 273
  ]
  edge [
    source 36
    target 274
  ]
  edge [
    source 36
    target 275
  ]
  edge [
    source 36
    target 276
  ]
  edge [
    source 36
    target 277
  ]
  edge [
    source 36
    target 278
  ]
  edge [
    source 36
    target 279
  ]
  edge [
    source 36
    target 280
  ]
  edge [
    source 36
    target 281
  ]
  edge [
    source 36
    target 282
  ]
  edge [
    source 36
    target 283
  ]
  edge [
    source 36
    target 284
  ]
  edge [
    source 36
    target 285
  ]
  edge [
    source 36
    target 286
  ]
  edge [
    source 36
    target 287
  ]
  edge [
    source 36
    target 288
  ]
  edge [
    source 36
    target 289
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 40
  ]
  edge [
    source 39
    target 354
  ]
  edge [
    source 39
    target 355
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 290
  ]
  edge [
    source 40
    target 291
  ]
  edge [
    source 40
    target 292
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 293
  ]
  edge [
    source 41
    target 294
  ]
  edge [
    source 41
    target 150
  ]
  edge [
    source 41
    target 295
  ]
  edge [
    source 41
    target 296
  ]
  edge [
    source 41
    target 297
  ]
  edge [
    source 41
    target 298
  ]
  edge [
    source 41
    target 299
  ]
  edge [
    source 41
    target 300
  ]
  edge [
    source 41
    target 301
  ]
  edge [
    source 41
    target 302
  ]
  edge [
    source 41
    target 303
  ]
  edge [
    source 41
    target 304
  ]
  edge [
    source 41
    target 305
  ]
  edge [
    source 41
    target 306
  ]
  edge [
    source 41
    target 307
  ]
  edge [
    source 41
    target 308
  ]
  edge [
    source 41
    target 309
  ]
  edge [
    source 41
    target 310
  ]
  edge [
    source 41
    target 311
  ]
  edge [
    source 41
    target 312
  ]
  edge [
    source 41
    target 313
  ]
  edge [
    source 41
    target 314
  ]
  edge [
    source 41
    target 315
  ]
  edge [
    source 41
    target 316
  ]
  edge [
    source 41
    target 317
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 318
  ]
  edge [
    source 43
    target 59
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 48
  ]
  edge [
    source 44
    target 47
  ]
  edge [
    source 44
    target 319
  ]
  edge [
    source 44
    target 320
  ]
  edge [
    source 44
    target 321
  ]
  edge [
    source 44
    target 322
  ]
  edge [
    source 44
    target 323
  ]
  edge [
    source 44
    target 324
  ]
  edge [
    source 44
    target 325
  ]
  edge [
    source 44
    target 326
  ]
  edge [
    source 44
    target 327
  ]
  edge [
    source 44
    target 328
  ]
  edge [
    source 44
    target 329
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 330
  ]
  edge [
    source 46
    target 331
  ]
  edge [
    source 46
    target 332
  ]
  edge [
    source 46
    target 48
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 49
  ]
  edge [
    source 47
    target 333
  ]
  edge [
    source 47
    target 334
  ]
  edge [
    source 48
    target 335
  ]
  edge [
    source 48
    target 336
  ]
  edge [
    source 48
    target 337
  ]
  edge [
    source 48
    target 338
  ]
  edge [
    source 48
    target 339
  ]
  edge [
    source 48
    target 340
  ]
  edge [
    source 48
    target 341
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 342
  ]
  edge [
    source 51
    target 343
  ]
  edge [
    source 51
    target 344
  ]
  edge [
    source 51
    target 345
  ]
  edge [
    source 51
    target 346
  ]
  edge [
    source 52
    target 347
  ]
  edge [
    source 52
    target 203
  ]
  edge [
    source 52
    target 348
  ]
  edge [
    source 52
    target 349
  ]
  edge [
    source 52
    target 350
  ]
  edge [
    source 52
    target 351
  ]
  edge [
    source 52
    target 352
  ]
  edge [
    source 52
    target 193
  ]
  edge [
    source 113
    target 356
  ]
  edge [
    source 113
    target 357
  ]
  edge [
    source 354
    target 355
  ]
  edge [
    source 355
    target 355
  ]
  edge [
    source 356
    target 357
  ]
  edge [
    source 358
    target 359
  ]
  edge [
    source 360
    target 361
  ]
]
