graph [
  maxDegree 22
  minDegree 1
  meanDegree 1.9333333333333333
  density 0.06666666666666667
  graphCliqueNumber 2
  node [
    id 0
    label "taki"
    origin "text"
  ]
  node [
    id 1
    label "g&#243;ra"
    origin "text"
  ]
  node [
    id 2
    label "zmajstrowa&#263;by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "dla"
    origin "text"
  ]
  node [
    id 4
    label "syn"
    origin "text"
  ]
  node [
    id 5
    label "okre&#347;lony"
  ]
  node [
    id 6
    label "jaki&#347;"
  ]
  node [
    id 7
    label "przedmiot"
  ]
  node [
    id 8
    label "grupa"
  ]
  node [
    id 9
    label "element"
  ]
  node [
    id 10
    label "przele&#378;&#263;"
  ]
  node [
    id 11
    label "pi&#281;tro"
  ]
  node [
    id 12
    label "karczek"
  ]
  node [
    id 13
    label "wysoki"
  ]
  node [
    id 14
    label "rami&#261;czko"
  ]
  node [
    id 15
    label "Ropa"
  ]
  node [
    id 16
    label "Jaworze"
  ]
  node [
    id 17
    label "Synaj"
  ]
  node [
    id 18
    label "wzniesienie"
  ]
  node [
    id 19
    label "przelezienie"
  ]
  node [
    id 20
    label "&#347;piew"
  ]
  node [
    id 21
    label "kupa"
  ]
  node [
    id 22
    label "kierunek"
  ]
  node [
    id 23
    label "Ma&#322;a_Rawka"
  ]
  node [
    id 24
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 25
    label "d&#378;wi&#281;k"
  ]
  node [
    id 26
    label "Kreml"
  ]
  node [
    id 27
    label "usynowienie"
  ]
  node [
    id 28
    label "usynawianie"
  ]
  node [
    id 29
    label "dziecko"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
]
