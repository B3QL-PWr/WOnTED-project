graph [
  maxDegree 13
  minDegree 1
  meanDegree 1.9090909090909092
  density 0.09090909090909091
  graphCliqueNumber 2
  node [
    id 0
    label "odrzutowiec"
    origin "text"
  ]
  node [
    id 1
    label "by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "fan"
    origin "text"
  ]
  node [
    id 3
    label "paliwo&#380;erny"
    origin "text"
  ]
  node [
    id 4
    label "hummer"
    origin "text"
  ]
  node [
    id 5
    label "samolot"
  ]
  node [
    id 6
    label "si&#281;ga&#263;"
  ]
  node [
    id 7
    label "trwa&#263;"
  ]
  node [
    id 8
    label "obecno&#347;&#263;"
  ]
  node [
    id 9
    label "stan"
  ]
  node [
    id 10
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 11
    label "stand"
  ]
  node [
    id 12
    label "mie&#263;_miejsce"
  ]
  node [
    id 13
    label "uczestniczy&#263;"
  ]
  node [
    id 14
    label "chodzi&#263;"
  ]
  node [
    id 15
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 16
    label "equal"
  ]
  node [
    id 17
    label "mi&#322;o&#347;nik"
  ]
  node [
    id 18
    label "fan_club"
  ]
  node [
    id 19
    label "fandom"
  ]
  node [
    id 20
    label "paliwo&#380;ernie"
  ]
  node [
    id 21
    label "nieoszcz&#281;dny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
]
