graph [
  maxDegree 27
  minDegree 1
  meanDegree 1.9666666666666666
  density 0.03333333333333333
  graphCliqueNumber 2
  node [
    id 0
    label "pracbaza"
    origin "text"
  ]
  node [
    id 1
    label "patologiazewsi"
    origin "text"
  ]
  node [
    id 2
    label "zalesie"
    origin "text"
  ]
  node [
    id 3
    label "wasz"
    origin "text"
  ]
  node [
    id 4
    label "te&#380;"
    origin "text"
  ]
  node [
    id 5
    label "wkurwia&#263;"
    origin "text"
  ]
  node [
    id 6
    label "co&#347;"
    origin "text"
  ]
  node [
    id 7
    label "taki"
    origin "text"
  ]
  node [
    id 8
    label "praca"
    origin "text"
  ]
  node [
    id 9
    label "zaczyna&#263;"
    origin "text"
  ]
  node [
    id 10
    label "ko&#324;czy&#263;"
    origin "text"
  ]
  node [
    id 11
    label "teraz"
    origin "text"
  ]
  node [
    id 12
    label "czyj&#347;"
  ]
  node [
    id 13
    label "dzia&#322;a&#263;_na_nerwy"
  ]
  node [
    id 14
    label "thing"
  ]
  node [
    id 15
    label "cosik"
  ]
  node [
    id 16
    label "okre&#347;lony"
  ]
  node [
    id 17
    label "jaki&#347;"
  ]
  node [
    id 18
    label "stosunek_pracy"
  ]
  node [
    id 19
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 20
    label "benedykty&#324;ski"
  ]
  node [
    id 21
    label "pracowanie"
  ]
  node [
    id 22
    label "zaw&#243;d"
  ]
  node [
    id 23
    label "kierownictwo"
  ]
  node [
    id 24
    label "zmiana"
  ]
  node [
    id 25
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 26
    label "wytw&#243;r"
  ]
  node [
    id 27
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 28
    label "tynkarski"
  ]
  node [
    id 29
    label "czynnik_produkcji"
  ]
  node [
    id 30
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 31
    label "zobowi&#261;zanie"
  ]
  node [
    id 32
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 33
    label "czynno&#347;&#263;"
  ]
  node [
    id 34
    label "tyrka"
  ]
  node [
    id 35
    label "pracowa&#263;"
  ]
  node [
    id 36
    label "siedziba"
  ]
  node [
    id 37
    label "poda&#380;_pracy"
  ]
  node [
    id 38
    label "miejsce"
  ]
  node [
    id 39
    label "zak&#322;ad"
  ]
  node [
    id 40
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 41
    label "najem"
  ]
  node [
    id 42
    label "open"
  ]
  node [
    id 43
    label "odejmowa&#263;"
  ]
  node [
    id 44
    label "mie&#263;_miejsce"
  ]
  node [
    id 45
    label "robi&#263;_pierwszy_krok"
  ]
  node [
    id 46
    label "set_about"
  ]
  node [
    id 47
    label "begin"
  ]
  node [
    id 48
    label "post&#281;powa&#263;"
  ]
  node [
    id 49
    label "bankrupt"
  ]
  node [
    id 50
    label "zako&#324;cza&#263;"
  ]
  node [
    id 51
    label "przestawa&#263;"
  ]
  node [
    id 52
    label "robi&#263;"
  ]
  node [
    id 53
    label "satisfy"
  ]
  node [
    id 54
    label "close"
  ]
  node [
    id 55
    label "determine"
  ]
  node [
    id 56
    label "dokonywa&#263;_&#380;ywota"
  ]
  node [
    id 57
    label "stanowi&#263;"
  ]
  node [
    id 58
    label "chwila"
  ]
  node [
    id 59
    label "tera&#378;niejszo&#347;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 13
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 14
  ]
  edge [
    source 6
    target 15
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 16
  ]
  edge [
    source 7
    target 17
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 8
    target 18
  ]
  edge [
    source 8
    target 19
  ]
  edge [
    source 8
    target 20
  ]
  edge [
    source 8
    target 21
  ]
  edge [
    source 8
    target 22
  ]
  edge [
    source 8
    target 23
  ]
  edge [
    source 8
    target 24
  ]
  edge [
    source 8
    target 25
  ]
  edge [
    source 8
    target 26
  ]
  edge [
    source 8
    target 27
  ]
  edge [
    source 8
    target 28
  ]
  edge [
    source 8
    target 29
  ]
  edge [
    source 8
    target 30
  ]
  edge [
    source 8
    target 31
  ]
  edge [
    source 8
    target 32
  ]
  edge [
    source 8
    target 33
  ]
  edge [
    source 8
    target 34
  ]
  edge [
    source 8
    target 35
  ]
  edge [
    source 8
    target 36
  ]
  edge [
    source 8
    target 37
  ]
  edge [
    source 8
    target 38
  ]
  edge [
    source 8
    target 39
  ]
  edge [
    source 8
    target 40
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 9
    target 42
  ]
  edge [
    source 9
    target 43
  ]
  edge [
    source 9
    target 44
  ]
  edge [
    source 9
    target 45
  ]
  edge [
    source 9
    target 46
  ]
  edge [
    source 9
    target 47
  ]
  edge [
    source 9
    target 48
  ]
  edge [
    source 9
    target 49
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 50
  ]
  edge [
    source 10
    target 51
  ]
  edge [
    source 10
    target 52
  ]
  edge [
    source 10
    target 53
  ]
  edge [
    source 10
    target 54
  ]
  edge [
    source 10
    target 55
  ]
  edge [
    source 10
    target 56
  ]
  edge [
    source 10
    target 57
  ]
  edge [
    source 11
    target 58
  ]
  edge [
    source 11
    target 59
  ]
]
