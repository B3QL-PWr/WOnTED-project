graph [
  maxDegree 41
  minDegree 1
  meanDegree 2.0491803278688523
  density 0.008432840855427377
  graphCliqueNumber 2
  node [
    id 0
    label "stwierdzenie"
    origin "text"
  ]
  node [
    id 1
    label "do&#347;&#263;"
    origin "text"
  ]
  node [
    id 2
    label "banalny"
    origin "text"
  ]
  node [
    id 3
    label "ale"
    origin "text"
  ]
  node [
    id 4
    label "odniesienie"
    origin "text"
  ]
  node [
    id 5
    label "rynek"
    origin "text"
  ]
  node [
    id 6
    label "gra"
    origin "text"
  ]
  node [
    id 7
    label "okazywa&#263;"
    origin "text"
  ]
  node [
    id 8
    label "si&#281;"
    origin "text"
  ]
  node [
    id 9
    label "odkrywczy"
    origin "text"
  ]
  node [
    id 10
    label "nowy"
    origin "text"
  ]
  node [
    id 11
    label "raport"
    origin "text"
  ]
  node [
    id 12
    label "firma"
    origin "text"
  ]
  node [
    id 13
    label "npd"
    origin "text"
  ]
  node [
    id 14
    label "po&#347;wi&#281;cony"
    origin "text"
  ]
  node [
    id 15
    label "nowa"
    origin "text"
  ]
  node [
    id 16
    label "generacja"
    origin "text"
  ]
  node [
    id 17
    label "konsola"
    origin "text"
  ]
  node [
    id 18
    label "wylewa&#263;"
    origin "text"
  ]
  node [
    id 19
    label "bowiem"
    origin "text"
  ]
  node [
    id 20
    label "producent"
    origin "text"
  ]
  node [
    id 21
    label "kube&#322;"
    origin "text"
  ]
  node [
    id 22
    label "zimny"
    origin "text"
  ]
  node [
    id 23
    label "woda"
    origin "text"
  ]
  node [
    id 24
    label "wypowied&#378;"
  ]
  node [
    id 25
    label "ustalenie"
  ]
  node [
    id 26
    label "claim"
  ]
  node [
    id 27
    label "statement"
  ]
  node [
    id 28
    label "oznajmienie"
  ]
  node [
    id 29
    label "b&#322;aho"
  ]
  node [
    id 30
    label "zbanalizowanie_si&#281;"
  ]
  node [
    id 31
    label "g&#322;upi"
  ]
  node [
    id 32
    label "&#322;atwiutko"
  ]
  node [
    id 33
    label "banalnienie"
  ]
  node [
    id 34
    label "duperelny"
  ]
  node [
    id 35
    label "pospolity"
  ]
  node [
    id 36
    label "banalnie"
  ]
  node [
    id 37
    label "niepoczesny"
  ]
  node [
    id 38
    label "trywializowanie"
  ]
  node [
    id 39
    label "strywializowanie"
  ]
  node [
    id 40
    label "piwo"
  ]
  node [
    id 41
    label "od&#322;o&#380;enie"
  ]
  node [
    id 42
    label "skill"
  ]
  node [
    id 43
    label "doznanie"
  ]
  node [
    id 44
    label "gaze"
  ]
  node [
    id 45
    label "deference"
  ]
  node [
    id 46
    label "dochrapanie_si&#281;"
  ]
  node [
    id 47
    label "dostarczenie"
  ]
  node [
    id 48
    label "mention"
  ]
  node [
    id 49
    label "bearing"
  ]
  node [
    id 50
    label "po&#380;yczenie"
  ]
  node [
    id 51
    label "cel"
  ]
  node [
    id 52
    label "uzyskanie"
  ]
  node [
    id 53
    label "oddanie"
  ]
  node [
    id 54
    label "powi&#261;zanie"
  ]
  node [
    id 55
    label "stoisko"
  ]
  node [
    id 56
    label "plac"
  ]
  node [
    id 57
    label "emitowanie"
  ]
  node [
    id 58
    label "targowica"
  ]
  node [
    id 59
    label "emitowa&#263;"
  ]
  node [
    id 60
    label "wprowadzanie"
  ]
  node [
    id 61
    label "wprowadzi&#263;"
  ]
  node [
    id 62
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 63
    label "rynek_wt&#243;rny"
  ]
  node [
    id 64
    label "wprowadzenie"
  ]
  node [
    id 65
    label "kram"
  ]
  node [
    id 66
    label "wprowadza&#263;"
  ]
  node [
    id 67
    label "pojawienie_si&#281;"
  ]
  node [
    id 68
    label "rynek_podstawowy"
  ]
  node [
    id 69
    label "biznes"
  ]
  node [
    id 70
    label "gospodarka"
  ]
  node [
    id 71
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 72
    label "obiekt_handlowy"
  ]
  node [
    id 73
    label "konsument"
  ]
  node [
    id 74
    label "wytw&#243;rca"
  ]
  node [
    id 75
    label "segment_rynku"
  ]
  node [
    id 76
    label "wska&#378;nik_rynkowy"
  ]
  node [
    id 77
    label "rynek_r&#243;wnoleg&#322;y"
  ]
  node [
    id 78
    label "zabawa"
  ]
  node [
    id 79
    label "rywalizacja"
  ]
  node [
    id 80
    label "czynno&#347;&#263;"
  ]
  node [
    id 81
    label "Pok&#233;mon"
  ]
  node [
    id 82
    label "synteza"
  ]
  node [
    id 83
    label "odtworzenie"
  ]
  node [
    id 84
    label "komplet"
  ]
  node [
    id 85
    label "rekwizyt_do_gry"
  ]
  node [
    id 86
    label "odg&#322;os"
  ]
  node [
    id 87
    label "rozgrywka"
  ]
  node [
    id 88
    label "post&#281;powanie"
  ]
  node [
    id 89
    label "wydarzenie"
  ]
  node [
    id 90
    label "apparent_motion"
  ]
  node [
    id 91
    label "game"
  ]
  node [
    id 92
    label "zmienno&#347;&#263;"
  ]
  node [
    id 93
    label "zasada"
  ]
  node [
    id 94
    label "akcja"
  ]
  node [
    id 95
    label "play"
  ]
  node [
    id 96
    label "contest"
  ]
  node [
    id 97
    label "zbijany"
  ]
  node [
    id 98
    label "arouse"
  ]
  node [
    id 99
    label "pokazywa&#263;"
  ]
  node [
    id 100
    label "zdradza&#263;_si&#281;"
  ]
  node [
    id 101
    label "signify"
  ]
  node [
    id 102
    label "badawczy"
  ]
  node [
    id 103
    label "odkrywczo"
  ]
  node [
    id 104
    label "odwa&#380;ny"
  ]
  node [
    id 105
    label "oryginalny"
  ]
  node [
    id 106
    label "nowatorsko"
  ]
  node [
    id 107
    label "cz&#322;owiek"
  ]
  node [
    id 108
    label "nowotny"
  ]
  node [
    id 109
    label "drugi"
  ]
  node [
    id 110
    label "kolejny"
  ]
  node [
    id 111
    label "bie&#380;&#261;cy"
  ]
  node [
    id 112
    label "nowo"
  ]
  node [
    id 113
    label "narybek"
  ]
  node [
    id 114
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 115
    label "obcy"
  ]
  node [
    id 116
    label "raport_Beveridge'a"
  ]
  node [
    id 117
    label "relacja"
  ]
  node [
    id 118
    label "raport_Fischlera"
  ]
  node [
    id 119
    label "MAC"
  ]
  node [
    id 120
    label "Hortex"
  ]
  node [
    id 121
    label "reengineering"
  ]
  node [
    id 122
    label "nazwa_w&#322;asna"
  ]
  node [
    id 123
    label "podmiot_gospodarczy"
  ]
  node [
    id 124
    label "Google"
  ]
  node [
    id 125
    label "zaufanie"
  ]
  node [
    id 126
    label "biurowiec"
  ]
  node [
    id 127
    label "interes"
  ]
  node [
    id 128
    label "zasoby_ludzkie"
  ]
  node [
    id 129
    label "networking"
  ]
  node [
    id 130
    label "paczkarnia"
  ]
  node [
    id 131
    label "Canon"
  ]
  node [
    id 132
    label "HP"
  ]
  node [
    id 133
    label "Baltona"
  ]
  node [
    id 134
    label "Pewex"
  ]
  node [
    id 135
    label "MAN_SE"
  ]
  node [
    id 136
    label "Apeks"
  ]
  node [
    id 137
    label "zasoby"
  ]
  node [
    id 138
    label "Orbis"
  ]
  node [
    id 139
    label "miejsce_pracy"
  ]
  node [
    id 140
    label "siedziba"
  ]
  node [
    id 141
    label "Spo&#322;em"
  ]
  node [
    id 142
    label "przedsi&#281;biorczo&#347;&#263;"
  ]
  node [
    id 143
    label "Orlen"
  ]
  node [
    id 144
    label "klasa"
  ]
  node [
    id 145
    label "oddany"
  ]
  node [
    id 146
    label "gwiazda"
  ]
  node [
    id 147
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 148
    label "rocznik"
  ]
  node [
    id 149
    label "zbi&#243;r"
  ]
  node [
    id 150
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 151
    label "praca"
  ]
  node [
    id 152
    label "proces_fizyczny"
  ]
  node [
    id 153
    label "coevals"
  ]
  node [
    id 154
    label "ozdobny"
  ]
  node [
    id 155
    label "stolik"
  ]
  node [
    id 156
    label "urz&#261;dzenie"
  ]
  node [
    id 157
    label "pulpit"
  ]
  node [
    id 158
    label "wspornik"
  ]
  node [
    id 159
    label "tremo"
  ]
  node [
    id 160
    label "pad"
  ]
  node [
    id 161
    label "la&#263;"
  ]
  node [
    id 162
    label "opr&#243;&#380;nia&#263;"
  ]
  node [
    id 163
    label "spill"
  ]
  node [
    id 164
    label "wyrzuca&#263;"
  ]
  node [
    id 165
    label "pokrywa&#263;"
  ]
  node [
    id 166
    label "wyra&#380;a&#263;"
  ]
  node [
    id 167
    label "zwalnia&#263;"
  ]
  node [
    id 168
    label "wydostawa&#263;_si&#281;"
  ]
  node [
    id 169
    label "podmiot"
  ]
  node [
    id 170
    label "&#322;a&#324;cuch_pokarmowy"
  ]
  node [
    id 171
    label "manufacturer"
  ]
  node [
    id 172
    label "bran&#380;owiec"
  ]
  node [
    id 173
    label "artel"
  ]
  node [
    id 174
    label "filmowiec"
  ]
  node [
    id 175
    label "muzyk"
  ]
  node [
    id 176
    label "wykonawca"
  ]
  node [
    id 177
    label "autotrof"
  ]
  node [
    id 178
    label "Wedel"
  ]
  node [
    id 179
    label "zawarto&#347;&#263;"
  ]
  node [
    id 180
    label "pojemnik"
  ]
  node [
    id 181
    label "transporter"
  ]
  node [
    id 182
    label "naczynie"
  ]
  node [
    id 183
    label "&#347;mietnik"
  ]
  node [
    id 184
    label "basket"
  ]
  node [
    id 185
    label "wymborek"
  ]
  node [
    id 186
    label "bucket"
  ]
  node [
    id 187
    label "zimno"
  ]
  node [
    id 188
    label "obumarcie"
  ]
  node [
    id 189
    label "niemi&#322;y"
  ]
  node [
    id 190
    label "znieczulanie"
  ]
  node [
    id 191
    label "znieczulenie"
  ]
  node [
    id 192
    label "ozi&#281;bienie"
  ]
  node [
    id 193
    label "obumieranie"
  ]
  node [
    id 194
    label "wiszenie"
  ]
  node [
    id 195
    label "umarcie"
  ]
  node [
    id 196
    label "martwo"
  ]
  node [
    id 197
    label "rozs&#261;dny"
  ]
  node [
    id 198
    label "umieranie"
  ]
  node [
    id 199
    label "z&#322;y"
  ]
  node [
    id 200
    label "opanowany"
  ]
  node [
    id 201
    label "niezadowalaj&#261;cy"
  ]
  node [
    id 202
    label "niech&#281;tny"
  ]
  node [
    id 203
    label "ch&#322;odno"
  ]
  node [
    id 204
    label "nieczu&#322;y"
  ]
  node [
    id 205
    label "obiekt_naturalny"
  ]
  node [
    id 206
    label "bicie"
  ]
  node [
    id 207
    label "wysi&#281;k"
  ]
  node [
    id 208
    label "pustka"
  ]
  node [
    id 209
    label "woda_s&#322;odka"
  ]
  node [
    id 210
    label "p&#322;ycizna"
  ]
  node [
    id 211
    label "ciecz"
  ]
  node [
    id 212
    label "spi&#281;trza&#263;"
  ]
  node [
    id 213
    label "uj&#281;cie_wody"
  ]
  node [
    id 214
    label "chlasta&#263;"
  ]
  node [
    id 215
    label "spi&#281;trzy&#263;"
  ]
  node [
    id 216
    label "nap&#243;j"
  ]
  node [
    id 217
    label "bombast"
  ]
  node [
    id 218
    label "water"
  ]
  node [
    id 219
    label "kryptodepresja"
  ]
  node [
    id 220
    label "wodnik"
  ]
  node [
    id 221
    label "pojazd"
  ]
  node [
    id 222
    label "&#347;r&#243;dl&#261;dowy"
  ]
  node [
    id 223
    label "fala"
  ]
  node [
    id 224
    label "Waruna"
  ]
  node [
    id 225
    label "nap&#243;j_bezalkoholowy"
  ]
  node [
    id 226
    label "zrzut"
  ]
  node [
    id 227
    label "dotleni&#263;"
  ]
  node [
    id 228
    label "utylizator"
  ]
  node [
    id 229
    label "przyroda"
  ]
  node [
    id 230
    label "uci&#261;g"
  ]
  node [
    id 231
    label "wybrze&#380;e"
  ]
  node [
    id 232
    label "nabranie"
  ]
  node [
    id 233
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 234
    label "klarownik"
  ]
  node [
    id 235
    label "chlastanie"
  ]
  node [
    id 236
    label "przybrze&#380;e"
  ]
  node [
    id 237
    label "deklamacja"
  ]
  node [
    id 238
    label "spi&#281;trzenie"
  ]
  node [
    id 239
    label "przybieranie"
  ]
  node [
    id 240
    label "nabra&#263;"
  ]
  node [
    id 241
    label "tlenek"
  ]
  node [
    id 242
    label "spi&#281;trzanie"
  ]
  node [
    id 243
    label "l&#243;d"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 17
  ]
  edge [
    source 6
    target 18
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 102
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 104
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 107
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 10
    target 110
  ]
  edge [
    source 10
    target 111
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 116
  ]
  edge [
    source 11
    target 117
  ]
  edge [
    source 11
    target 118
  ]
  edge [
    source 11
    target 27
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 107
  ]
  edge [
    source 12
    target 119
  ]
  edge [
    source 12
    target 120
  ]
  edge [
    source 12
    target 121
  ]
  edge [
    source 12
    target 122
  ]
  edge [
    source 12
    target 123
  ]
  edge [
    source 12
    target 124
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 12
    target 131
  ]
  edge [
    source 12
    target 132
  ]
  edge [
    source 12
    target 133
  ]
  edge [
    source 12
    target 134
  ]
  edge [
    source 12
    target 135
  ]
  edge [
    source 12
    target 136
  ]
  edge [
    source 12
    target 137
  ]
  edge [
    source 12
    target 138
  ]
  edge [
    source 12
    target 139
  ]
  edge [
    source 12
    target 140
  ]
  edge [
    source 12
    target 141
  ]
  edge [
    source 12
    target 142
  ]
  edge [
    source 12
    target 143
  ]
  edge [
    source 12
    target 144
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 145
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 146
  ]
  edge [
    source 15
    target 147
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 148
  ]
  edge [
    source 16
    target 149
  ]
  edge [
    source 16
    target 150
  ]
  edge [
    source 16
    target 151
  ]
  edge [
    source 16
    target 152
  ]
  edge [
    source 16
    target 153
  ]
  edge [
    source 17
    target 154
  ]
  edge [
    source 17
    target 155
  ]
  edge [
    source 17
    target 156
  ]
  edge [
    source 17
    target 157
  ]
  edge [
    source 17
    target 158
  ]
  edge [
    source 17
    target 159
  ]
  edge [
    source 17
    target 160
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 161
  ]
  edge [
    source 18
    target 162
  ]
  edge [
    source 18
    target 163
  ]
  edge [
    source 18
    target 164
  ]
  edge [
    source 18
    target 165
  ]
  edge [
    source 18
    target 166
  ]
  edge [
    source 18
    target 167
  ]
  edge [
    source 18
    target 168
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 169
  ]
  edge [
    source 20
    target 170
  ]
  edge [
    source 20
    target 171
  ]
  edge [
    source 20
    target 172
  ]
  edge [
    source 20
    target 173
  ]
  edge [
    source 20
    target 174
  ]
  edge [
    source 20
    target 175
  ]
  edge [
    source 20
    target 131
  ]
  edge [
    source 20
    target 176
  ]
  edge [
    source 20
    target 177
  ]
  edge [
    source 20
    target 178
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 179
  ]
  edge [
    source 21
    target 180
  ]
  edge [
    source 21
    target 181
  ]
  edge [
    source 21
    target 182
  ]
  edge [
    source 21
    target 183
  ]
  edge [
    source 21
    target 184
  ]
  edge [
    source 21
    target 185
  ]
  edge [
    source 21
    target 186
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 187
  ]
  edge [
    source 22
    target 188
  ]
  edge [
    source 22
    target 189
  ]
  edge [
    source 22
    target 190
  ]
  edge [
    source 22
    target 191
  ]
  edge [
    source 22
    target 192
  ]
  edge [
    source 22
    target 193
  ]
  edge [
    source 22
    target 194
  ]
  edge [
    source 22
    target 195
  ]
  edge [
    source 22
    target 196
  ]
  edge [
    source 22
    target 197
  ]
  edge [
    source 22
    target 198
  ]
  edge [
    source 22
    target 199
  ]
  edge [
    source 22
    target 200
  ]
  edge [
    source 22
    target 201
  ]
  edge [
    source 22
    target 202
  ]
  edge [
    source 22
    target 203
  ]
  edge [
    source 22
    target 204
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 205
  ]
  edge [
    source 23
    target 206
  ]
  edge [
    source 23
    target 207
  ]
  edge [
    source 23
    target 208
  ]
  edge [
    source 23
    target 209
  ]
  edge [
    source 23
    target 210
  ]
  edge [
    source 23
    target 211
  ]
  edge [
    source 23
    target 212
  ]
  edge [
    source 23
    target 213
  ]
  edge [
    source 23
    target 214
  ]
  edge [
    source 23
    target 215
  ]
  edge [
    source 23
    target 216
  ]
  edge [
    source 23
    target 217
  ]
  edge [
    source 23
    target 218
  ]
  edge [
    source 23
    target 219
  ]
  edge [
    source 23
    target 220
  ]
  edge [
    source 23
    target 221
  ]
  edge [
    source 23
    target 222
  ]
  edge [
    source 23
    target 223
  ]
  edge [
    source 23
    target 224
  ]
  edge [
    source 23
    target 225
  ]
  edge [
    source 23
    target 226
  ]
  edge [
    source 23
    target 227
  ]
  edge [
    source 23
    target 228
  ]
  edge [
    source 23
    target 229
  ]
  edge [
    source 23
    target 230
  ]
  edge [
    source 23
    target 231
  ]
  edge [
    source 23
    target 232
  ]
  edge [
    source 23
    target 233
  ]
  edge [
    source 23
    target 234
  ]
  edge [
    source 23
    target 235
  ]
  edge [
    source 23
    target 236
  ]
  edge [
    source 23
    target 237
  ]
  edge [
    source 23
    target 238
  ]
  edge [
    source 23
    target 239
  ]
  edge [
    source 23
    target 240
  ]
  edge [
    source 23
    target 241
  ]
  edge [
    source 23
    target 242
  ]
  edge [
    source 23
    target 243
  ]
]
