graph [
  maxDegree 26
  minDegree 1
  meanDegree 2
  density 0.027777777777777776
  graphCliqueNumber 3
  node [
    id 0
    label "mama"
    origin "text"
  ]
  node [
    id 1
    label "z&#322;y"
    origin "text"
  ]
  node [
    id 2
    label "jako&#347;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "powietrze"
    origin "text"
  ]
  node [
    id 4
    label "unia"
    origin "text"
  ]
  node [
    id 5
    label "europejski"
    origin "text"
  ]
  node [
    id 6
    label "matczysko"
  ]
  node [
    id 7
    label "macierz"
  ]
  node [
    id 8
    label "przodkini"
  ]
  node [
    id 9
    label "Matka_Boska"
  ]
  node [
    id 10
    label "macocha"
  ]
  node [
    id 11
    label "matka_zast&#281;pcza"
  ]
  node [
    id 12
    label "stara"
  ]
  node [
    id 13
    label "rodzice"
  ]
  node [
    id 14
    label "rodzic"
  ]
  node [
    id 15
    label "zdenerwowany"
  ]
  node [
    id 16
    label "zez&#322;oszczenie"
  ]
  node [
    id 17
    label "rozz&#322;oszczenie_si&#281;"
  ]
  node [
    id 18
    label "gniewanie"
  ]
  node [
    id 19
    label "niekorzystny"
  ]
  node [
    id 20
    label "niemoralny"
  ]
  node [
    id 21
    label "niegrzeczny"
  ]
  node [
    id 22
    label "pieski"
  ]
  node [
    id 23
    label "negatywny"
  ]
  node [
    id 24
    label "&#378;le"
  ]
  node [
    id 25
    label "syf"
  ]
  node [
    id 26
    label "niew&#322;a&#347;ciwy"
  ]
  node [
    id 27
    label "sierdzisty"
  ]
  node [
    id 28
    label "z&#322;oszczenie"
  ]
  node [
    id 29
    label "rozgniewanie"
  ]
  node [
    id 30
    label "niepomy&#347;lny"
  ]
  node [
    id 31
    label "warto&#347;&#263;"
  ]
  node [
    id 32
    label "co&#347;"
  ]
  node [
    id 33
    label "state"
  ]
  node [
    id 34
    label "quality"
  ]
  node [
    id 35
    label "mieszanina"
  ]
  node [
    id 36
    label "przewietrzy&#263;"
  ]
  node [
    id 37
    label "przewietrza&#263;"
  ]
  node [
    id 38
    label "tlen"
  ]
  node [
    id 39
    label "eter"
  ]
  node [
    id 40
    label "dmucha&#263;"
  ]
  node [
    id 41
    label "dmuchanie"
  ]
  node [
    id 42
    label "breeze"
  ]
  node [
    id 43
    label "pojazd"
  ]
  node [
    id 44
    label "pneumatyczny"
  ]
  node [
    id 45
    label "wydychanie"
  ]
  node [
    id 46
    label "podgrzew"
  ]
  node [
    id 47
    label "wdychanie"
  ]
  node [
    id 48
    label "luft"
  ]
  node [
    id 49
    label "geosystem"
  ]
  node [
    id 50
    label "dmuchn&#261;&#263;"
  ]
  node [
    id 51
    label "dmuchni&#281;cie"
  ]
  node [
    id 52
    label "&#380;ywio&#322;"
  ]
  node [
    id 53
    label "wdycha&#263;"
  ]
  node [
    id 54
    label "wydycha&#263;"
  ]
  node [
    id 55
    label "napowietrzy&#263;"
  ]
  node [
    id 56
    label "front"
  ]
  node [
    id 57
    label "przewietrzenie"
  ]
  node [
    id 58
    label "przewietrzanie"
  ]
  node [
    id 59
    label "uk&#322;ad"
  ]
  node [
    id 60
    label "partia"
  ]
  node [
    id 61
    label "organizacja"
  ]
  node [
    id 62
    label "Unia_Wolno&#347;ci"
  ]
  node [
    id 63
    label "Unia_Europejska"
  ]
  node [
    id 64
    label "combination"
  ]
  node [
    id 65
    label "union"
  ]
  node [
    id 66
    label "Unia"
  ]
  node [
    id 67
    label "European"
  ]
  node [
    id 68
    label "po_europejsku"
  ]
  node [
    id 69
    label "charakterystyczny"
  ]
  node [
    id 70
    label "europejsko"
  ]
  node [
    id 71
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 72
    label "typowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
]
