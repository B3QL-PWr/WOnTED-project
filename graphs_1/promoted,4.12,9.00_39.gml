graph [
  maxDegree 23
  minDegree 1
  meanDegree 2.015873015873016
  density 0.01612698412698413
  graphCliqueNumber 3
  node [
    id 0
    label "dro&#380;yzna"
    origin "text"
  ]
  node [
    id 1
    label "ci&#261;g"
    origin "text"
  ]
  node [
    id 2
    label "daleki"
    origin "text"
  ]
  node [
    id 3
    label "kolej"
    origin "text"
  ]
  node [
    id 4
    label "mazowiecki"
    origin "text"
  ]
  node [
    id 5
    label "protestowa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "przeciwko"
    origin "text"
  ]
  node [
    id 7
    label "blisko"
    origin "text"
  ]
  node [
    id 8
    label "proca"
    origin "text"
  ]
  node [
    id 9
    label "wysoki"
    origin "text"
  ]
  node [
    id 10
    label "rachunek"
    origin "text"
  ]
  node [
    id 11
    label "jaki"
    origin "text"
  ]
  node [
    id 12
    label "wystawia&#263;"
    origin "text"
  ]
  node [
    id 13
    label "pkp"
    origin "text"
  ]
  node [
    id 14
    label "energetyk"
    origin "text"
  ]
  node [
    id 15
    label "dearth"
  ]
  node [
    id 16
    label "cena"
  ]
  node [
    id 17
    label "zdzierca"
  ]
  node [
    id 18
    label "si&#322;a"
  ]
  node [
    id 19
    label "stan"
  ]
  node [
    id 20
    label "lina"
  ]
  node [
    id 21
    label "way"
  ]
  node [
    id 22
    label "cable"
  ]
  node [
    id 23
    label "przebieg"
  ]
  node [
    id 24
    label "zbi&#243;r"
  ]
  node [
    id 25
    label "ch&#243;d"
  ]
  node [
    id 26
    label "trasa"
  ]
  node [
    id 27
    label "rz&#261;d"
  ]
  node [
    id 28
    label "k&#322;us"
  ]
  node [
    id 29
    label "progression"
  ]
  node [
    id 30
    label "current"
  ]
  node [
    id 31
    label "pr&#261;d"
  ]
  node [
    id 32
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 33
    label "wydarzenie"
  ]
  node [
    id 34
    label "lot"
  ]
  node [
    id 35
    label "dawny"
  ]
  node [
    id 36
    label "du&#380;y"
  ]
  node [
    id 37
    label "s&#322;aby"
  ]
  node [
    id 38
    label "oddalony"
  ]
  node [
    id 39
    label "daleko"
  ]
  node [
    id 40
    label "przysz&#322;y"
  ]
  node [
    id 41
    label "ogl&#281;dny"
  ]
  node [
    id 42
    label "r&#243;&#380;ny"
  ]
  node [
    id 43
    label "g&#322;&#281;boki"
  ]
  node [
    id 44
    label "odlegle"
  ]
  node [
    id 45
    label "nieobecny"
  ]
  node [
    id 46
    label "odleg&#322;y"
  ]
  node [
    id 47
    label "d&#322;ugi"
  ]
  node [
    id 48
    label "zwi&#261;zany"
  ]
  node [
    id 49
    label "obcy"
  ]
  node [
    id 50
    label "pojazd_kolejowy"
  ]
  node [
    id 51
    label "czas"
  ]
  node [
    id 52
    label "tor"
  ]
  node [
    id 53
    label "tender"
  ]
  node [
    id 54
    label "droga"
  ]
  node [
    id 55
    label "blokada"
  ]
  node [
    id 56
    label "wagon"
  ]
  node [
    id 57
    label "run"
  ]
  node [
    id 58
    label "cedu&#322;a"
  ]
  node [
    id 59
    label "kolejno&#347;&#263;"
  ]
  node [
    id 60
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 61
    label "skomunikowanie_si&#281;"
  ]
  node [
    id 62
    label "trakcja"
  ]
  node [
    id 63
    label "linia"
  ]
  node [
    id 64
    label "cug"
  ]
  node [
    id 65
    label "poci&#261;g"
  ]
  node [
    id 66
    label "pocz&#261;tek"
  ]
  node [
    id 67
    label "nast&#281;pstwo"
  ]
  node [
    id 68
    label "lokomotywa"
  ]
  node [
    id 69
    label "skomunikowa&#263;_si&#281;"
  ]
  node [
    id 70
    label "proces"
  ]
  node [
    id 71
    label "polski"
  ]
  node [
    id 72
    label "po_mazowiecku"
  ]
  node [
    id 73
    label "regionalny"
  ]
  node [
    id 74
    label "napastowa&#263;"
  ]
  node [
    id 75
    label "post&#281;powa&#263;"
  ]
  node [
    id 76
    label "anticipate"
  ]
  node [
    id 77
    label "dok&#322;adnie"
  ]
  node [
    id 78
    label "bliski"
  ]
  node [
    id 79
    label "silnie"
  ]
  node [
    id 80
    label "tu&#380;_tu&#380;"
  ]
  node [
    id 81
    label "bro&#324;_miotaj&#261;ca"
  ]
  node [
    id 82
    label "zabawka"
  ]
  node [
    id 83
    label "bro&#324;_my&#347;liwska"
  ]
  node [
    id 84
    label "bro&#324;"
  ]
  node [
    id 85
    label "catapult"
  ]
  node [
    id 86
    label "urwis"
  ]
  node [
    id 87
    label "warto&#347;ciowy"
  ]
  node [
    id 88
    label "wysoce"
  ]
  node [
    id 89
    label "znaczny"
  ]
  node [
    id 90
    label "wysoko"
  ]
  node [
    id 91
    label "szczytnie"
  ]
  node [
    id 92
    label "wznios&#322;y"
  ]
  node [
    id 93
    label "wyrafinowany"
  ]
  node [
    id 94
    label "z_wysoka"
  ]
  node [
    id 95
    label "chwalebny"
  ]
  node [
    id 96
    label "uprzywilejowany"
  ]
  node [
    id 97
    label "niepo&#347;ledni"
  ]
  node [
    id 98
    label "spis"
  ]
  node [
    id 99
    label "wytw&#243;r"
  ]
  node [
    id 100
    label "check"
  ]
  node [
    id 101
    label "count"
  ]
  node [
    id 102
    label "mienie"
  ]
  node [
    id 103
    label "nale&#380;no&#347;&#263;"
  ]
  node [
    id 104
    label "budowa&#263;"
  ]
  node [
    id 105
    label "wyjmowa&#263;"
  ]
  node [
    id 106
    label "typify"
  ]
  node [
    id 107
    label "eksponowa&#263;"
  ]
  node [
    id 108
    label "proponowa&#263;"
  ]
  node [
    id 109
    label "wysuwa&#263;"
  ]
  node [
    id 110
    label "dopuszcza&#263;"
  ]
  node [
    id 111
    label "wychyla&#263;"
  ]
  node [
    id 112
    label "wynosi&#263;"
  ]
  node [
    id 113
    label "represent"
  ]
  node [
    id 114
    label "wyra&#380;a&#263;"
  ]
  node [
    id 115
    label "wskazywa&#263;"
  ]
  node [
    id 116
    label "wypisywa&#263;"
  ]
  node [
    id 117
    label "pies_my&#347;liwski"
  ]
  node [
    id 118
    label "przedstawia&#263;"
  ]
  node [
    id 119
    label "nap&#243;j_gazowany"
  ]
  node [
    id 120
    label "in&#380;ynier"
  ]
  node [
    id 121
    label "robotnik"
  ]
  node [
    id 122
    label "nap&#243;j_bezalkoholowy"
  ]
  node [
    id 123
    label "bran&#380;owiec"
  ]
  node [
    id 124
    label "tauryna"
  ]
  node [
    id 125
    label "PKP"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 36
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 12
    target 107
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 12
    target 109
  ]
  edge [
    source 12
    target 110
  ]
  edge [
    source 12
    target 111
  ]
  edge [
    source 12
    target 112
  ]
  edge [
    source 12
    target 113
  ]
  edge [
    source 12
    target 114
  ]
  edge [
    source 12
    target 115
  ]
  edge [
    source 12
    target 116
  ]
  edge [
    source 12
    target 117
  ]
  edge [
    source 12
    target 118
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 119
  ]
  edge [
    source 14
    target 120
  ]
  edge [
    source 14
    target 121
  ]
  edge [
    source 14
    target 122
  ]
  edge [
    source 14
    target 123
  ]
  edge [
    source 14
    target 124
  ]
  edge [
    source 14
    target 125
  ]
]
