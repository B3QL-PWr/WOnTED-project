graph [
  maxDegree 27
  minDegree 1
  meanDegree 1.9705882352941178
  density 0.029411764705882353
  graphCliqueNumber 2
  node [
    id 0
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "jeden"
    origin "text"
  ]
  node [
    id 3
    label "grosz"
    origin "text"
  ]
  node [
    id 4
    label "&#380;aden"
    origin "text"
  ]
  node [
    id 5
    label "pieni&#261;dz"
    origin "text"
  ]
  node [
    id 6
    label "remark"
  ]
  node [
    id 7
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 8
    label "u&#380;ywa&#263;"
  ]
  node [
    id 9
    label "okre&#347;la&#263;"
  ]
  node [
    id 10
    label "j&#281;zyk"
  ]
  node [
    id 11
    label "say"
  ]
  node [
    id 12
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 13
    label "formu&#322;owa&#263;"
  ]
  node [
    id 14
    label "talk"
  ]
  node [
    id 15
    label "powiada&#263;"
  ]
  node [
    id 16
    label "informowa&#263;"
  ]
  node [
    id 17
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 18
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 19
    label "wydobywa&#263;"
  ]
  node [
    id 20
    label "express"
  ]
  node [
    id 21
    label "chew_the_fat"
  ]
  node [
    id 22
    label "dysfonia"
  ]
  node [
    id 23
    label "umie&#263;"
  ]
  node [
    id 24
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 25
    label "tell"
  ]
  node [
    id 26
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 27
    label "wyra&#380;a&#263;"
  ]
  node [
    id 28
    label "gaworzy&#263;"
  ]
  node [
    id 29
    label "rozmawia&#263;"
  ]
  node [
    id 30
    label "dziama&#263;"
  ]
  node [
    id 31
    label "prawi&#263;"
  ]
  node [
    id 32
    label "kieliszek"
  ]
  node [
    id 33
    label "shot"
  ]
  node [
    id 34
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 35
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 36
    label "jaki&#347;"
  ]
  node [
    id 37
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 38
    label "jednolicie"
  ]
  node [
    id 39
    label "w&#243;dka"
  ]
  node [
    id 40
    label "ten"
  ]
  node [
    id 41
    label "ujednolicenie"
  ]
  node [
    id 42
    label "jednakowy"
  ]
  node [
    id 43
    label "z&#322;oty"
  ]
  node [
    id 44
    label "groszak"
  ]
  node [
    id 45
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 46
    label "moneta"
  ]
  node [
    id 47
    label "szyling_austryjacki"
  ]
  node [
    id 48
    label "kwota"
  ]
  node [
    id 49
    label "nijaki"
  ]
  node [
    id 50
    label "poda&#380;_pieni&#261;dza"
  ]
  node [
    id 51
    label "zdewaluowa&#263;"
  ]
  node [
    id 52
    label "moniak"
  ]
  node [
    id 53
    label "wytw&#243;r"
  ]
  node [
    id 54
    label "zdewaluowanie"
  ]
  node [
    id 55
    label "jednostka_monetarna"
  ]
  node [
    id 56
    label "&#347;rodek_p&#322;atniczy"
  ]
  node [
    id 57
    label "numizmat"
  ]
  node [
    id 58
    label "rozmieni&#263;"
  ]
  node [
    id 59
    label "rozmienienie"
  ]
  node [
    id 60
    label "rozmienianie"
  ]
  node [
    id 61
    label "dewaluowanie"
  ]
  node [
    id 62
    label "nomina&#322;"
  ]
  node [
    id 63
    label "coin"
  ]
  node [
    id 64
    label "dewaluowa&#263;"
  ]
  node [
    id 65
    label "pieni&#261;dze"
  ]
  node [
    id 66
    label "znak_pieni&#281;&#380;ny"
  ]
  node [
    id 67
    label "rozmienia&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
]
