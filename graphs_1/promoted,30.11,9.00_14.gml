graph [
  maxDegree 27
  minDegree 1
  meanDegree 2.0240963855421685
  density 0.024684102262709375
  graphCliqueNumber 3
  node [
    id 0
    label "agencja"
    origin "text"
  ]
  node [
    id 1
    label "praca"
    origin "text"
  ]
  node [
    id 2
    label "niemcy"
    origin "text"
  ]
  node [
    id 3
    label "podpisywa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "kontrakt"
    origin "text"
  ]
  node [
    id 5
    label "sprowadzi&#263;"
    origin "text"
  ]
  node [
    id 6
    label "tysi&#261;c"
    origin "text"
  ]
  node [
    id 7
    label "pracownik"
    origin "text"
  ]
  node [
    id 8
    label "turcja"
    origin "text"
  ]
  node [
    id 9
    label "bank"
  ]
  node [
    id 10
    label "whole"
  ]
  node [
    id 11
    label "NASA"
  ]
  node [
    id 12
    label "firma"
  ]
  node [
    id 13
    label "przedstawicielstwo"
  ]
  node [
    id 14
    label "ajencja"
  ]
  node [
    id 15
    label "instytucja"
  ]
  node [
    id 16
    label "filia"
  ]
  node [
    id 17
    label "dzia&#322;"
  ]
  node [
    id 18
    label "siedziba"
  ]
  node [
    id 19
    label "oddzia&#322;"
  ]
  node [
    id 20
    label "stosunek_pracy"
  ]
  node [
    id 21
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 22
    label "benedykty&#324;ski"
  ]
  node [
    id 23
    label "pracowanie"
  ]
  node [
    id 24
    label "zaw&#243;d"
  ]
  node [
    id 25
    label "kierownictwo"
  ]
  node [
    id 26
    label "zmiana"
  ]
  node [
    id 27
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 28
    label "wytw&#243;r"
  ]
  node [
    id 29
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 30
    label "tynkarski"
  ]
  node [
    id 31
    label "czynnik_produkcji"
  ]
  node [
    id 32
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 33
    label "zobowi&#261;zanie"
  ]
  node [
    id 34
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 35
    label "czynno&#347;&#263;"
  ]
  node [
    id 36
    label "tyrka"
  ]
  node [
    id 37
    label "pracowa&#263;"
  ]
  node [
    id 38
    label "poda&#380;_pracy"
  ]
  node [
    id 39
    label "miejsce"
  ]
  node [
    id 40
    label "zak&#322;ad"
  ]
  node [
    id 41
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 42
    label "najem"
  ]
  node [
    id 43
    label "sign"
  ]
  node [
    id 44
    label "opatrywa&#263;"
  ]
  node [
    id 45
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 46
    label "stawia&#263;"
  ]
  node [
    id 47
    label "zjazd"
  ]
  node [
    id 48
    label "bryd&#380;"
  ]
  node [
    id 49
    label "akt"
  ]
  node [
    id 50
    label "umowa"
  ]
  node [
    id 51
    label "agent"
  ]
  node [
    id 52
    label "attachment"
  ]
  node [
    id 53
    label "powie&#347;&#263;"
  ]
  node [
    id 54
    label "get"
  ]
  node [
    id 55
    label "pom&#243;c"
  ]
  node [
    id 56
    label "ograniczy&#263;"
  ]
  node [
    id 57
    label "pos&#322;a&#263;"
  ]
  node [
    id 58
    label "spowodowa&#263;"
  ]
  node [
    id 59
    label "zwr&#243;ci&#263;"
  ]
  node [
    id 60
    label "become"
  ]
  node [
    id 61
    label "bring"
  ]
  node [
    id 62
    label "u&#322;amek"
  ]
  node [
    id 63
    label "zmieni&#263;"
  ]
  node [
    id 64
    label "carry"
  ]
  node [
    id 65
    label "wprowadzi&#263;"
  ]
  node [
    id 66
    label "upro&#347;ci&#263;"
  ]
  node [
    id 67
    label "tauzen"
  ]
  node [
    id 68
    label "musik"
  ]
  node [
    id 69
    label "molarity"
  ]
  node [
    id 70
    label "licytacja"
  ]
  node [
    id 71
    label "patyk"
  ]
  node [
    id 72
    label "liczba"
  ]
  node [
    id 73
    label "gra_w_karty"
  ]
  node [
    id 74
    label "kwota"
  ]
  node [
    id 75
    label "cz&#322;owiek_pracy"
  ]
  node [
    id 76
    label "cz&#322;owiek"
  ]
  node [
    id 77
    label "delegowa&#263;"
  ]
  node [
    id 78
    label "s&#322;u&#380;ba_cywilna"
  ]
  node [
    id 79
    label "pracu&#347;"
  ]
  node [
    id 80
    label "delegowanie"
  ]
  node [
    id 81
    label "r&#281;ka"
  ]
  node [
    id 82
    label "salariat"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
]
