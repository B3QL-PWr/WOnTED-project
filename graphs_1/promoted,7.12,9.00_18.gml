graph [
  maxDegree 16
  minDegree 1
  meanDegree 2
  density 0.037037037037037035
  graphCliqueNumber 2
  node [
    id 0
    label "by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "decyzja"
    origin "text"
  ]
  node [
    id 2
    label "podpis"
    origin "text"
  ]
  node [
    id 3
    label "minister"
    origin "text"
  ]
  node [
    id 4
    label "infrastruktura"
    origin "text"
  ]
  node [
    id 5
    label "pod"
    origin "text"
  ]
  node [
    id 6
    label "plan"
    origin "text"
  ]
  node [
    id 7
    label "inwestycyjny"
    origin "text"
  ]
  node [
    id 8
    label "si&#281;ga&#263;"
  ]
  node [
    id 9
    label "trwa&#263;"
  ]
  node [
    id 10
    label "obecno&#347;&#263;"
  ]
  node [
    id 11
    label "stan"
  ]
  node [
    id 12
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 13
    label "stand"
  ]
  node [
    id 14
    label "mie&#263;_miejsce"
  ]
  node [
    id 15
    label "uczestniczy&#263;"
  ]
  node [
    id 16
    label "chodzi&#263;"
  ]
  node [
    id 17
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 18
    label "equal"
  ]
  node [
    id 19
    label "dokument"
  ]
  node [
    id 20
    label "resolution"
  ]
  node [
    id 21
    label "zdecydowanie"
  ]
  node [
    id 22
    label "wytw&#243;r"
  ]
  node [
    id 23
    label "podj&#281;cie_decyzji"
  ]
  node [
    id 24
    label "management"
  ]
  node [
    id 25
    label "w&#322;asnor&#281;czny"
  ]
  node [
    id 26
    label "napis"
  ]
  node [
    id 27
    label "obja&#347;nienie"
  ]
  node [
    id 28
    label "sign"
  ]
  node [
    id 29
    label "potwierdzenie"
  ]
  node [
    id 30
    label "signal"
  ]
  node [
    id 31
    label "znak"
  ]
  node [
    id 32
    label "Goebbels"
  ]
  node [
    id 33
    label "Sto&#322;ypin"
  ]
  node [
    id 34
    label "rz&#261;d"
  ]
  node [
    id 35
    label "dostojnik"
  ]
  node [
    id 36
    label "telefonia"
  ]
  node [
    id 37
    label "trasa"
  ]
  node [
    id 38
    label "zaplecze"
  ]
  node [
    id 39
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 40
    label "radiofonia"
  ]
  node [
    id 41
    label "device"
  ]
  node [
    id 42
    label "model"
  ]
  node [
    id 43
    label "obraz"
  ]
  node [
    id 44
    label "przestrze&#324;"
  ]
  node [
    id 45
    label "dekoracja"
  ]
  node [
    id 46
    label "intencja"
  ]
  node [
    id 47
    label "agreement"
  ]
  node [
    id 48
    label "pomys&#322;"
  ]
  node [
    id 49
    label "punkt"
  ]
  node [
    id 50
    label "miejsce_pracy"
  ]
  node [
    id 51
    label "perspektywa"
  ]
  node [
    id 52
    label "rysunek"
  ]
  node [
    id 53
    label "reprezentacja"
  ]
  node [
    id 54
    label "inwestycyjnie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 22
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 7
    target 54
  ]
]
