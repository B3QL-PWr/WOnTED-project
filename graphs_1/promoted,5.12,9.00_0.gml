graph [
  maxDegree 30
  minDegree 1
  meanDegree 2.026315789473684
  density 0.027017543859649124
  graphCliqueNumber 2
  node [
    id 0
    label "imigrant"
    origin "text"
  ]
  node [
    id 1
    label "emerytura"
    origin "text"
  ]
  node [
    id 2
    label "otrzymywa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "mimo"
    origin "text"
  ]
  node [
    id 4
    label "pracowa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "jeden"
    origin "text"
  ]
  node [
    id 6
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 7
    label "cudzoziemiec"
  ]
  node [
    id 8
    label "przybysz"
  ]
  node [
    id 9
    label "migrant"
  ]
  node [
    id 10
    label "imigracja"
  ]
  node [
    id 11
    label "ochrona_uzupe&#322;niaj&#261;ca"
  ]
  node [
    id 12
    label "czas"
  ]
  node [
    id 13
    label "ubezpieczenie_emerytalne"
  ]
  node [
    id 14
    label "&#347;wiadczenie_spo&#322;eczne"
  ]
  node [
    id 15
    label "egzystencja"
  ]
  node [
    id 16
    label "retirement"
  ]
  node [
    id 17
    label "wytwarza&#263;"
  ]
  node [
    id 18
    label "take"
  ]
  node [
    id 19
    label "dostawa&#263;"
  ]
  node [
    id 20
    label "return"
  ]
  node [
    id 21
    label "endeavor"
  ]
  node [
    id 22
    label "funkcjonowa&#263;"
  ]
  node [
    id 23
    label "dobiega&#263;_si&#281;"
  ]
  node [
    id 24
    label "mie&#263;_miejsce"
  ]
  node [
    id 25
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 26
    label "dzia&#322;a&#263;"
  ]
  node [
    id 27
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 28
    label "work"
  ]
  node [
    id 29
    label "bangla&#263;"
  ]
  node [
    id 30
    label "do"
  ]
  node [
    id 31
    label "maszyna"
  ]
  node [
    id 32
    label "tryb"
  ]
  node [
    id 33
    label "dziama&#263;"
  ]
  node [
    id 34
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 35
    label "praca"
  ]
  node [
    id 36
    label "podejmowa&#263;"
  ]
  node [
    id 37
    label "kieliszek"
  ]
  node [
    id 38
    label "shot"
  ]
  node [
    id 39
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 40
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 41
    label "jaki&#347;"
  ]
  node [
    id 42
    label "jednolicie"
  ]
  node [
    id 43
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 44
    label "w&#243;dka"
  ]
  node [
    id 45
    label "ten"
  ]
  node [
    id 46
    label "ujednolicenie"
  ]
  node [
    id 47
    label "jednakowy"
  ]
  node [
    id 48
    label "s&#322;o&#324;ce"
  ]
  node [
    id 49
    label "czynienie_si&#281;"
  ]
  node [
    id 50
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 51
    label "long_time"
  ]
  node [
    id 52
    label "przedpo&#322;udnie"
  ]
  node [
    id 53
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 54
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 55
    label "tydzie&#324;"
  ]
  node [
    id 56
    label "godzina"
  ]
  node [
    id 57
    label "t&#322;usty_czwartek"
  ]
  node [
    id 58
    label "wsta&#263;"
  ]
  node [
    id 59
    label "day"
  ]
  node [
    id 60
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 61
    label "przedwiecz&#243;r"
  ]
  node [
    id 62
    label "Sylwester"
  ]
  node [
    id 63
    label "po&#322;udnie"
  ]
  node [
    id 64
    label "wzej&#347;cie"
  ]
  node [
    id 65
    label "podwiecz&#243;r"
  ]
  node [
    id 66
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 67
    label "rano"
  ]
  node [
    id 68
    label "termin"
  ]
  node [
    id 69
    label "ranek"
  ]
  node [
    id 70
    label "doba"
  ]
  node [
    id 71
    label "wiecz&#243;r"
  ]
  node [
    id 72
    label "walentynki"
  ]
  node [
    id 73
    label "popo&#322;udnie"
  ]
  node [
    id 74
    label "noc"
  ]
  node [
    id 75
    label "wstanie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 1
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 12
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
]
