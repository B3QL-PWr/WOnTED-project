graph [
  maxDegree 20
  minDegree 1
  meanDegree 1.9428571428571428
  density 0.05714285714285714
  graphCliqueNumber 2
  node [
    id 0
    label "festiwal"
    origin "text"
  ]
  node [
    id 1
    label "edukacja"
    origin "text"
  ]
  node [
    id 2
    label "nieformalny"
    origin "text"
  ]
  node [
    id 3
    label "bia&#322;oru&#347;"
    origin "text"
  ]
  node [
    id 4
    label "Nowe_Horyzonty"
  ]
  node [
    id 5
    label "Interwizja"
  ]
  node [
    id 6
    label "Open'er"
  ]
  node [
    id 7
    label "Przystanek_Woodstock"
  ]
  node [
    id 8
    label "impreza"
  ]
  node [
    id 9
    label "Woodstock"
  ]
  node [
    id 10
    label "Metalmania"
  ]
  node [
    id 11
    label "Opole"
  ]
  node [
    id 12
    label "FAMA"
  ]
  node [
    id 13
    label "Eurowizja"
  ]
  node [
    id 14
    label "Brutal"
  ]
  node [
    id 15
    label "kwalifikacje"
  ]
  node [
    id 16
    label "Karta_Nauczyciela"
  ]
  node [
    id 17
    label "szkolnictwo"
  ]
  node [
    id 18
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 19
    label "program_nauczania"
  ]
  node [
    id 20
    label "formation"
  ]
  node [
    id 21
    label "miasteczko_rowerowe"
  ]
  node [
    id 22
    label "gospodarka"
  ]
  node [
    id 23
    label "urszulanki"
  ]
  node [
    id 24
    label "wiedza"
  ]
  node [
    id 25
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 26
    label "skolaryzacja"
  ]
  node [
    id 27
    label "proces"
  ]
  node [
    id 28
    label "niepokalanki"
  ]
  node [
    id 29
    label "heureza"
  ]
  node [
    id 30
    label "form"
  ]
  node [
    id 31
    label "nauka"
  ]
  node [
    id 32
    label "&#322;awa_szkolna"
  ]
  node [
    id 33
    label "nieformalnie"
  ]
  node [
    id 34
    label "nieoficjalny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
]
