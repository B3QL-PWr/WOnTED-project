graph [
  maxDegree 12
  minDegree 1
  meanDegree 1.9473684210526316
  density 0.05263157894736842
  graphCliqueNumber 2
  node [
    id 0
    label "my&#347;le&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "&#380;eby"
    origin "text"
  ]
  node [
    id 2
    label "napisa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "ckliwy"
    origin "text"
  ]
  node [
    id 4
    label "post"
    origin "text"
  ]
  node [
    id 5
    label "ale"
    origin "text"
  ]
  node [
    id 6
    label "odpu&#347;ci&#263;"
    origin "text"
  ]
  node [
    id 7
    label "postawi&#263;"
  ]
  node [
    id 8
    label "prasa"
  ]
  node [
    id 9
    label "stworzy&#263;"
  ]
  node [
    id 10
    label "donie&#347;&#263;"
  ]
  node [
    id 11
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 12
    label "write"
  ]
  node [
    id 13
    label "styl"
  ]
  node [
    id 14
    label "read"
  ]
  node [
    id 15
    label "przes&#322;adzanie"
  ]
  node [
    id 16
    label "uczuciowy"
  ]
  node [
    id 17
    label "przes&#322;odzenie"
  ]
  node [
    id 18
    label "md&#322;y"
  ]
  node [
    id 19
    label "ckliwo"
  ]
  node [
    id 20
    label "czu&#322;ostkowy"
  ]
  node [
    id 21
    label "ckliwie"
  ]
  node [
    id 22
    label "pie&#347;ciwy"
  ]
  node [
    id 23
    label "wzruszaj&#261;cy"
  ]
  node [
    id 24
    label "czu&#322;y"
  ]
  node [
    id 25
    label "zachowanie"
  ]
  node [
    id 26
    label "zachowa&#263;"
  ]
  node [
    id 27
    label "czas"
  ]
  node [
    id 28
    label "zachowywa&#263;"
  ]
  node [
    id 29
    label "pow&#347;ci&#261;gliwo&#347;&#263;"
  ]
  node [
    id 30
    label "rok_ko&#347;cielny"
  ]
  node [
    id 31
    label "zachowywanie"
  ]
  node [
    id 32
    label "praktyka"
  ]
  node [
    id 33
    label "tekst"
  ]
  node [
    id 34
    label "piwo"
  ]
  node [
    id 35
    label "postpone"
  ]
  node [
    id 36
    label "zrezygnowa&#263;"
  ]
  node [
    id 37
    label "zmi&#281;kczy&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
]
