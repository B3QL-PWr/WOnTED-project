graph [
  maxDegree 45
  minDegree 1
  meanDegree 1.9696969696969697
  density 0.030303030303030304
  graphCliqueNumber 2
  node [
    id 0
    label "historia"
    origin "text"
  ]
  node [
    id 1
    label "edycja"
    origin "text"
  ]
  node [
    id 2
    label "post"
    origin "text"
  ]
  node [
    id 3
    label "leszke"
    origin "text"
  ]
  node [
    id 4
    label "xdddd"
    origin "text"
  ]
  node [
    id 5
    label "report"
  ]
  node [
    id 6
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 7
    label "wypowied&#378;"
  ]
  node [
    id 8
    label "neografia"
  ]
  node [
    id 9
    label "przedmiot"
  ]
  node [
    id 10
    label "papirologia"
  ]
  node [
    id 11
    label "historia_gospodarcza"
  ]
  node [
    id 12
    label "przebiec"
  ]
  node [
    id 13
    label "hista"
  ]
  node [
    id 14
    label "nauka_humanistyczna"
  ]
  node [
    id 15
    label "filigranistyka"
  ]
  node [
    id 16
    label "dyplomatyka"
  ]
  node [
    id 17
    label "annalistyka"
  ]
  node [
    id 18
    label "historyka"
  ]
  node [
    id 19
    label "heraldyka"
  ]
  node [
    id 20
    label "fabu&#322;a"
  ]
  node [
    id 21
    label "muzealnictwo"
  ]
  node [
    id 22
    label "varsavianistyka"
  ]
  node [
    id 23
    label "prezentyzm"
  ]
  node [
    id 24
    label "mediewistyka"
  ]
  node [
    id 25
    label "przebiegni&#281;cie"
  ]
  node [
    id 26
    label "charakter"
  ]
  node [
    id 27
    label "paleografia"
  ]
  node [
    id 28
    label "genealogia"
  ]
  node [
    id 29
    label "czynno&#347;&#263;"
  ]
  node [
    id 30
    label "prozopografia"
  ]
  node [
    id 31
    label "motyw"
  ]
  node [
    id 32
    label "nautologia"
  ]
  node [
    id 33
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 34
    label "epoka"
  ]
  node [
    id 35
    label "numizmatyka"
  ]
  node [
    id 36
    label "ruralistyka"
  ]
  node [
    id 37
    label "epigrafika"
  ]
  node [
    id 38
    label "&#378;r&#243;d&#322;oznawstwo"
  ]
  node [
    id 39
    label "historiografia"
  ]
  node [
    id 40
    label "bizantynistyka"
  ]
  node [
    id 41
    label "weksylologia"
  ]
  node [
    id 42
    label "kierunek"
  ]
  node [
    id 43
    label "ikonografia"
  ]
  node [
    id 44
    label "chronologia"
  ]
  node [
    id 45
    label "archiwistyka"
  ]
  node [
    id 46
    label "sfragistyka"
  ]
  node [
    id 47
    label "zabytkoznawstwo"
  ]
  node [
    id 48
    label "historia_sztuki"
  ]
  node [
    id 49
    label "impression"
  ]
  node [
    id 50
    label "odmiana"
  ]
  node [
    id 51
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 52
    label "notification"
  ]
  node [
    id 53
    label "cykl"
  ]
  node [
    id 54
    label "zmiana"
  ]
  node [
    id 55
    label "produkcja"
  ]
  node [
    id 56
    label "egzemplarz"
  ]
  node [
    id 57
    label "zachowanie"
  ]
  node [
    id 58
    label "zachowa&#263;"
  ]
  node [
    id 59
    label "czas"
  ]
  node [
    id 60
    label "zachowywa&#263;"
  ]
  node [
    id 61
    label "pow&#347;ci&#261;gliwo&#347;&#263;"
  ]
  node [
    id 62
    label "rok_ko&#347;cielny"
  ]
  node [
    id 63
    label "zachowywanie"
  ]
  node [
    id 64
    label "praktyka"
  ]
  node [
    id 65
    label "tekst"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 3
    target 4
  ]
]
