graph [
  maxDegree 12
  minDegree 1
  meanDegree 2
  density 0.058823529411764705
  graphCliqueNumber 2
  node [
    id 0
    label "lena"
    origin "text"
  ]
  node [
    id 1
    label "zaczyna&#263;"
    origin "text"
  ]
  node [
    id 2
    label "nuci&#263;"
    origin "text"
  ]
  node [
    id 3
    label "fragment"
    origin "text"
  ]
  node [
    id 4
    label "oskar"
    origin "text"
  ]
  node [
    id 5
    label "w&#322;&#261;cza&#263;"
    origin "text"
  ]
  node [
    id 6
    label "si&#281;"
    origin "text"
  ]
  node [
    id 7
    label "chwila"
    origin "text"
  ]
  node [
    id 8
    label "open"
  ]
  node [
    id 9
    label "odejmowa&#263;"
  ]
  node [
    id 10
    label "mie&#263;_miejsce"
  ]
  node [
    id 11
    label "robi&#263;_pierwszy_krok"
  ]
  node [
    id 12
    label "set_about"
  ]
  node [
    id 13
    label "begin"
  ]
  node [
    id 14
    label "post&#281;powa&#263;"
  ]
  node [
    id 15
    label "bankrupt"
  ]
  node [
    id 16
    label "wykonywa&#263;"
  ]
  node [
    id 17
    label "melodia"
  ]
  node [
    id 18
    label "hum"
  ]
  node [
    id 19
    label "gaworzy&#263;"
  ]
  node [
    id 20
    label "piosenka"
  ]
  node [
    id 21
    label "chant"
  ]
  node [
    id 22
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 23
    label "utw&#243;r"
  ]
  node [
    id 24
    label "nastawia&#263;"
  ]
  node [
    id 25
    label "connect"
  ]
  node [
    id 26
    label "ogl&#261;da&#263;"
  ]
  node [
    id 27
    label "uruchamia&#263;"
  ]
  node [
    id 28
    label "umieszcza&#263;"
  ]
  node [
    id 29
    label "odkr&#281;ca&#263;_kurek"
  ]
  node [
    id 30
    label "dokoptowywa&#263;"
  ]
  node [
    id 31
    label "get_in_touch"
  ]
  node [
    id 32
    label "involve"
  ]
  node [
    id 33
    label "czas"
  ]
  node [
    id 34
    label "time"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 33
  ]
  edge [
    source 7
    target 34
  ]
]
