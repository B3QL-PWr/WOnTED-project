graph [
  maxDegree 6
  minDegree 1
  meanDegree 1.7333333333333334
  density 0.12380952380952381
  graphCliqueNumber 3
  node [
    id 0
    label "smard"
    origin "text"
  ]
  node [
    id 1
    label "dolny"
    origin "text"
  ]
  node [
    id 2
    label "wolny_ch&#322;op"
  ]
  node [
    id 3
    label "cz&#322;owiek"
  ]
  node [
    id 4
    label "istota_&#380;ywa"
  ]
  node [
    id 5
    label "nisko"
  ]
  node [
    id 6
    label "zminimalizowanie"
  ]
  node [
    id 7
    label "minimalnie"
  ]
  node [
    id 8
    label "graniczny"
  ]
  node [
    id 9
    label "minimalizowanie"
  ]
  node [
    id 10
    label "brama"
  ]
  node [
    id 11
    label "brandenburski"
  ]
  node [
    id 12
    label "Carlo"
  ]
  node [
    id 13
    label "Gotharda"
  ]
  node [
    id 14
    label "Langhansa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 13
    target 14
  ]
]
