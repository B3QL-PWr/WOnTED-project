graph [
  maxDegree 21
  minDegree 1
  meanDegree 1.9574468085106382
  density 0.0425531914893617
  graphCliqueNumber 2
  node [
    id 0
    label "chyba"
    origin "text"
  ]
  node [
    id 1
    label "dobry"
    origin "text"
  ]
  node [
    id 2
    label "podsumowanie"
    origin "text"
  ]
  node [
    id 3
    label "zmiana"
    origin "text"
  ]
  node [
    id 4
    label "rafonixa"
    origin "text"
  ]
  node [
    id 5
    label "pomy&#347;lny"
  ]
  node [
    id 6
    label "skuteczny"
  ]
  node [
    id 7
    label "moralny"
  ]
  node [
    id 8
    label "korzystny"
  ]
  node [
    id 9
    label "odpowiedni"
  ]
  node [
    id 10
    label "zwrot"
  ]
  node [
    id 11
    label "dobrze"
  ]
  node [
    id 12
    label "pozytywny"
  ]
  node [
    id 13
    label "grzeczny"
  ]
  node [
    id 14
    label "powitanie"
  ]
  node [
    id 15
    label "mi&#322;y"
  ]
  node [
    id 16
    label "dobroczynny"
  ]
  node [
    id 17
    label "pos&#322;uszny"
  ]
  node [
    id 18
    label "ca&#322;y"
  ]
  node [
    id 19
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 20
    label "czw&#243;rka"
  ]
  node [
    id 21
    label "spokojny"
  ]
  node [
    id 22
    label "&#347;mieszny"
  ]
  node [
    id 23
    label "drogi"
  ]
  node [
    id 24
    label "przedstawienie"
  ]
  node [
    id 25
    label "pozbieranie"
  ]
  node [
    id 26
    label "recapitulation"
  ]
  node [
    id 27
    label "policzenie"
  ]
  node [
    id 28
    label "ocenienie"
  ]
  node [
    id 29
    label "relacja"
  ]
  node [
    id 30
    label "collection"
  ]
  node [
    id 31
    label "statement"
  ]
  node [
    id 32
    label "addition"
  ]
  node [
    id 33
    label "anatomopatolog"
  ]
  node [
    id 34
    label "rewizja"
  ]
  node [
    id 35
    label "oznaka"
  ]
  node [
    id 36
    label "czas"
  ]
  node [
    id 37
    label "ferment"
  ]
  node [
    id 38
    label "komplet"
  ]
  node [
    id 39
    label "tura"
  ]
  node [
    id 40
    label "amendment"
  ]
  node [
    id 41
    label "zmianka"
  ]
  node [
    id 42
    label "odmienianie"
  ]
  node [
    id 43
    label "passage"
  ]
  node [
    id 44
    label "zjawisko"
  ]
  node [
    id 45
    label "change"
  ]
  node [
    id 46
    label "praca"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
]
