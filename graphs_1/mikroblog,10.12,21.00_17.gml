graph [
  maxDegree 39
  minDegree 1
  meanDegree 2.014925373134328
  density 0.015149814835596454
  graphCliqueNumber 3
  node [
    id 0
    label "go&#347;cia"
    origin "text"
  ]
  node [
    id 1
    label "reddicie"
    origin "text"
  ]
  node [
    id 2
    label "pisa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "telewizor"
    origin "text"
  ]
  node [
    id 4
    label "xiaomi"
    origin "text"
  ]
  node [
    id 5
    label "dwukrotnie"
    origin "text"
  ]
  node [
    id 6
    label "wy&#347;wietli&#263;"
    origin "text"
  ]
  node [
    id 7
    label "sekundowy"
    origin "text"
  ]
  node [
    id 8
    label "reklama"
    origin "text"
  ]
  node [
    id 9
    label "czas"
    origin "text"
  ]
  node [
    id 10
    label "przechodzenie"
    origin "text"
  ]
  node [
    id 11
    label "mi&#281;dzy"
    origin "text"
  ]
  node [
    id 12
    label "port"
    origin "text"
  ]
  node [
    id 13
    label "ozdabia&#263;"
  ]
  node [
    id 14
    label "dysgrafia"
  ]
  node [
    id 15
    label "prasa"
  ]
  node [
    id 16
    label "spell"
  ]
  node [
    id 17
    label "skryba"
  ]
  node [
    id 18
    label "donosi&#263;"
  ]
  node [
    id 19
    label "code"
  ]
  node [
    id 20
    label "tekst"
  ]
  node [
    id 21
    label "dysortografia"
  ]
  node [
    id 22
    label "read"
  ]
  node [
    id 23
    label "tworzy&#263;"
  ]
  node [
    id 24
    label "formu&#322;owa&#263;"
  ]
  node [
    id 25
    label "styl"
  ]
  node [
    id 26
    label "stawia&#263;"
  ]
  node [
    id 27
    label "paj&#281;czarz"
  ]
  node [
    id 28
    label "urz&#261;dzenie_RTV"
  ]
  node [
    id 29
    label "ekran"
  ]
  node [
    id 30
    label "odbieranie"
  ]
  node [
    id 31
    label "odbiera&#263;"
  ]
  node [
    id 32
    label "du&#380;y_pok&#243;j"
  ]
  node [
    id 33
    label "odbiornik"
  ]
  node [
    id 34
    label "dwukrotny"
  ]
  node [
    id 35
    label "kilkukrotnie"
  ]
  node [
    id 36
    label "dwakro&#263;"
  ]
  node [
    id 37
    label "parokrotnie"
  ]
  node [
    id 38
    label "pokaza&#263;"
  ]
  node [
    id 39
    label "copywriting"
  ]
  node [
    id 40
    label "brief"
  ]
  node [
    id 41
    label "bran&#380;a"
  ]
  node [
    id 42
    label "informacja"
  ]
  node [
    id 43
    label "promowa&#263;"
  ]
  node [
    id 44
    label "akcja"
  ]
  node [
    id 45
    label "wypromowa&#263;"
  ]
  node [
    id 46
    label "samplowanie"
  ]
  node [
    id 47
    label "czasokres"
  ]
  node [
    id 48
    label "trawienie"
  ]
  node [
    id 49
    label "kategoria_gramatyczna"
  ]
  node [
    id 50
    label "period"
  ]
  node [
    id 51
    label "odczyt"
  ]
  node [
    id 52
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 53
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 54
    label "chwila"
  ]
  node [
    id 55
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 56
    label "poprzedzenie"
  ]
  node [
    id 57
    label "koniugacja"
  ]
  node [
    id 58
    label "dzieje"
  ]
  node [
    id 59
    label "poprzedzi&#263;"
  ]
  node [
    id 60
    label "przep&#322;ywanie"
  ]
  node [
    id 61
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 62
    label "odwlekanie_si&#281;"
  ]
  node [
    id 63
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 64
    label "Zeitgeist"
  ]
  node [
    id 65
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 66
    label "okres_czasu"
  ]
  node [
    id 67
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 68
    label "pochodzi&#263;"
  ]
  node [
    id 69
    label "schy&#322;ek"
  ]
  node [
    id 70
    label "czwarty_wymiar"
  ]
  node [
    id 71
    label "chronometria"
  ]
  node [
    id 72
    label "poprzedzanie"
  ]
  node [
    id 73
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 74
    label "pogoda"
  ]
  node [
    id 75
    label "zegar"
  ]
  node [
    id 76
    label "trawi&#263;"
  ]
  node [
    id 77
    label "pochodzenie"
  ]
  node [
    id 78
    label "poprzedza&#263;"
  ]
  node [
    id 79
    label "time_period"
  ]
  node [
    id 80
    label "rachuba_czasu"
  ]
  node [
    id 81
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 82
    label "czasoprzestrze&#324;"
  ]
  node [
    id 83
    label "laba"
  ]
  node [
    id 84
    label "nale&#380;enie"
  ]
  node [
    id 85
    label "bycie"
  ]
  node [
    id 86
    label "stawanie_si&#281;"
  ]
  node [
    id 87
    label "pass"
  ]
  node [
    id 88
    label "passing"
  ]
  node [
    id 89
    label "odmienianie"
  ]
  node [
    id 90
    label "test"
  ]
  node [
    id 91
    label "dostawanie_si&#281;"
  ]
  node [
    id 92
    label "zaliczanie"
  ]
  node [
    id 93
    label "popychanie"
  ]
  node [
    id 94
    label "trwanie"
  ]
  node [
    id 95
    label "dzianie_si&#281;"
  ]
  node [
    id 96
    label "zmierzanie"
  ]
  node [
    id 97
    label "uznanie"
  ]
  node [
    id 98
    label "potr&#261;canie"
  ]
  node [
    id 99
    label "nasycanie_si&#281;"
  ]
  node [
    id 100
    label "przepuszczanie"
  ]
  node [
    id 101
    label "passage"
  ]
  node [
    id 102
    label "przebywanie"
  ]
  node [
    id 103
    label "nas&#261;czanie"
  ]
  node [
    id 104
    label "zast&#281;powanie"
  ]
  node [
    id 105
    label "przedostawanie_si&#281;"
  ]
  node [
    id 106
    label "mijanie"
  ]
  node [
    id 107
    label "zaznawanie"
  ]
  node [
    id 108
    label "przerabianie"
  ]
  node [
    id 109
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 110
    label "przemakanie"
  ]
  node [
    id 111
    label "przestawanie"
  ]
  node [
    id 112
    label "impregnation"
  ]
  node [
    id 113
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 114
    label "popchni&#281;cie"
  ]
  node [
    id 115
    label "wytyczenie"
  ]
  node [
    id 116
    label "zaczynanie"
  ]
  node [
    id 117
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 118
    label "Bordeaux"
  ]
  node [
    id 119
    label "nabrze&#380;e"
  ]
  node [
    id 120
    label "baza"
  ]
  node [
    id 121
    label "Kajenna"
  ]
  node [
    id 122
    label "Samara"
  ]
  node [
    id 123
    label "basen"
  ]
  node [
    id 124
    label "Berdia&#324;sk"
  ]
  node [
    id 125
    label "sztauer"
  ]
  node [
    id 126
    label "Koper"
  ]
  node [
    id 127
    label "Baku"
  ]
  node [
    id 128
    label "Korynt"
  ]
  node [
    id 129
    label "za&#322;adownia"
  ]
  node [
    id 130
    label "terminal"
  ]
  node [
    id 131
    label "HDMI1"
  ]
  node [
    id 132
    label "a"
  ]
  node [
    id 133
    label "HDMI3"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 39
  ]
  edge [
    source 8
    target 40
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 8
    target 46
  ]
  edge [
    source 8
    target 20
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 47
  ]
  edge [
    source 9
    target 48
  ]
  edge [
    source 9
    target 49
  ]
  edge [
    source 9
    target 50
  ]
  edge [
    source 9
    target 51
  ]
  edge [
    source 9
    target 52
  ]
  edge [
    source 9
    target 53
  ]
  edge [
    source 9
    target 54
  ]
  edge [
    source 9
    target 55
  ]
  edge [
    source 9
    target 56
  ]
  edge [
    source 9
    target 57
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 60
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 10
    target 105
  ]
  edge [
    source 10
    target 106
  ]
  edge [
    source 10
    target 107
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 10
    target 110
  ]
  edge [
    source 10
    target 111
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 118
  ]
  edge [
    source 12
    target 119
  ]
  edge [
    source 12
    target 120
  ]
  edge [
    source 12
    target 121
  ]
  edge [
    source 12
    target 122
  ]
  edge [
    source 12
    target 123
  ]
  edge [
    source 12
    target 124
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 131
    target 132
  ]
  edge [
    source 131
    target 133
  ]
  edge [
    source 132
    target 133
  ]
]
