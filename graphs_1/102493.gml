graph [
  maxDegree 32
  minDegree 1
  meanDegree 2.0683760683760686
  density 0.008877150508051795
  graphCliqueNumber 2
  node [
    id 0
    label "policyjny"
    origin "text"
  ]
  node [
    id 1
    label "izba"
    origin "text"
  ]
  node [
    id 2
    label "zatrzymanie"
    origin "text"
  ]
  node [
    id 3
    label "zgierz"
    origin "text"
  ]
  node [
    id 4
    label "trafi&#263;"
    origin "text"
  ]
  node [
    id 5
    label "letni"
    origin "text"
  ]
  node [
    id 6
    label "mieszkaniec"
    origin "text"
  ]
  node [
    id 7
    label "&#322;&#243;d&#378;"
    origin "text"
  ]
  node [
    id 8
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 9
    label "wraz"
    origin "text"
  ]
  node [
    id 10
    label "latko"
    origin "text"
  ]
  node [
    id 11
    label "pabianice"
    origin "text"
  ]
  node [
    id 12
    label "skra&#347;&#263;"
    origin "text"
  ]
  node [
    id 13
    label "automat"
    origin "text"
  ]
  node [
    id 14
    label "zabawka"
    origin "text"
  ]
  node [
    id 15
    label "moneta"
    origin "text"
  ]
  node [
    id 16
    label "kradzie&#380;"
    origin "text"
  ]
  node [
    id 17
    label "doj&#347;&#263;"
    origin "text"
  ]
  node [
    id 18
    label "poniedzia&#322;kowy"
    origin "text"
  ]
  node [
    id 19
    label "wiecz&#243;r"
    origin "text"
  ]
  node [
    id 20
    label "przed"
    origin "text"
  ]
  node [
    id 21
    label "sklep"
    origin "text"
  ]
  node [
    id 22
    label "ozorkowie"
    origin "text"
  ]
  node [
    id 23
    label "poszkodowany"
    origin "text"
  ]
  node [
    id 24
    label "rozpozna&#263;"
    origin "text"
  ]
  node [
    id 25
    label "z&#322;odziej"
    origin "text"
  ]
  node [
    id 26
    label "nieletni"
    origin "text"
  ]
  node [
    id 27
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 28
    label "przekaz"
    origin "text"
  ]
  node [
    id 29
    label "rodzic"
    origin "text"
  ]
  node [
    id 30
    label "w&#322;a&#347;ciciel"
    origin "text"
  ]
  node [
    id 31
    label "wyceni&#263;"
    origin "text"
  ]
  node [
    id 32
    label "z&#322;oty"
    origin "text"
  ]
  node [
    id 33
    label "pok&#243;j"
  ]
  node [
    id 34
    label "parlament"
  ]
  node [
    id 35
    label "zwi&#261;zek"
  ]
  node [
    id 36
    label "NIK"
  ]
  node [
    id 37
    label "urz&#261;d"
  ]
  node [
    id 38
    label "organ"
  ]
  node [
    id 39
    label "pomieszczenie"
  ]
  node [
    id 40
    label "funkcjonowanie"
  ]
  node [
    id 41
    label "przefiltrowanie"
  ]
  node [
    id 42
    label "zaczepienie"
  ]
  node [
    id 43
    label "discontinuance"
  ]
  node [
    id 44
    label "observation"
  ]
  node [
    id 45
    label "zaaresztowanie"
  ]
  node [
    id 46
    label "przerwanie"
  ]
  node [
    id 47
    label "career"
  ]
  node [
    id 48
    label "&#322;apanie"
  ]
  node [
    id 49
    label "pozajmowanie"
  ]
  node [
    id 50
    label "przetrzymanie"
  ]
  node [
    id 51
    label "capture"
  ]
  node [
    id 52
    label "unieruchomienie"
  ]
  node [
    id 53
    label "czynno&#347;&#263;"
  ]
  node [
    id 54
    label "z&#322;apanie"
  ]
  node [
    id 55
    label "check"
  ]
  node [
    id 56
    label "hipostaza"
  ]
  node [
    id 57
    label "pochowanie"
  ]
  node [
    id 58
    label "spowodowanie"
  ]
  node [
    id 59
    label "uniemo&#380;liwienie"
  ]
  node [
    id 60
    label "przestanie"
  ]
  node [
    id 61
    label "oddzia&#322;anie"
  ]
  node [
    id 62
    label "zabranie"
  ]
  node [
    id 63
    label "zamkni&#281;cie"
  ]
  node [
    id 64
    label "przechowanie"
  ]
  node [
    id 65
    label "closure"
  ]
  node [
    id 66
    label "powodowanie"
  ]
  node [
    id 67
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 68
    label "przypasowa&#263;"
  ]
  node [
    id 69
    label "wpa&#347;&#263;"
  ]
  node [
    id 70
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 71
    label "spotka&#263;"
  ]
  node [
    id 72
    label "dotrze&#263;"
  ]
  node [
    id 73
    label "zjawi&#263;_si&#281;"
  ]
  node [
    id 74
    label "happen"
  ]
  node [
    id 75
    label "znale&#378;&#263;"
  ]
  node [
    id 76
    label "hit"
  ]
  node [
    id 77
    label "pocisk"
  ]
  node [
    id 78
    label "stumble"
  ]
  node [
    id 79
    label "dolecie&#263;"
  ]
  node [
    id 80
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 81
    label "nijaki"
  ]
  node [
    id 82
    label "sezonowy"
  ]
  node [
    id 83
    label "letnio"
  ]
  node [
    id 84
    label "s&#322;oneczny"
  ]
  node [
    id 85
    label "weso&#322;y"
  ]
  node [
    id 86
    label "oboj&#281;tny"
  ]
  node [
    id 87
    label "latowy"
  ]
  node [
    id 88
    label "ciep&#322;y"
  ]
  node [
    id 89
    label "typowy"
  ]
  node [
    id 90
    label "cz&#322;owiek"
  ]
  node [
    id 91
    label "ludno&#347;&#263;"
  ]
  node [
    id 92
    label "zwierz&#281;"
  ]
  node [
    id 93
    label "regaty"
  ]
  node [
    id 94
    label "statek"
  ]
  node [
    id 95
    label "spalin&#243;wka"
  ]
  node [
    id 96
    label "pok&#322;ad"
  ]
  node [
    id 97
    label "ster"
  ]
  node [
    id 98
    label "kratownica"
  ]
  node [
    id 99
    label "pojazd_niemechaniczny"
  ]
  node [
    id 100
    label "drzewce"
  ]
  node [
    id 101
    label "zabra&#263;"
  ]
  node [
    id 102
    label "zw&#281;dzi&#263;"
  ]
  node [
    id 103
    label "podpierdoli&#263;"
  ]
  node [
    id 104
    label "przyw&#322;aszczy&#263;"
  ]
  node [
    id 105
    label "overcharge"
  ]
  node [
    id 106
    label "dehumanizacja"
  ]
  node [
    id 107
    label "automatyczna_skrzynia_bieg&#243;w"
  ]
  node [
    id 108
    label "pralka"
  ]
  node [
    id 109
    label "telefon"
  ]
  node [
    id 110
    label "pistolet"
  ]
  node [
    id 111
    label "lody_w&#322;oskie"
  ]
  node [
    id 112
    label "samoch&#243;d"
  ]
  node [
    id 113
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 114
    label "urz&#261;dzenie"
  ]
  node [
    id 115
    label "bro&#324;_samoczynno-samopowtarzalna"
  ]
  node [
    id 116
    label "wrzutnik_monet"
  ]
  node [
    id 117
    label "bawid&#322;o"
  ]
  node [
    id 118
    label "smoczek"
  ]
  node [
    id 119
    label "przedmiot"
  ]
  node [
    id 120
    label "frisbee"
  ]
  node [
    id 121
    label "narz&#281;dzie"
  ]
  node [
    id 122
    label "rewers"
  ]
  node [
    id 123
    label "legenda"
  ]
  node [
    id 124
    label "pieni&#261;dz"
  ]
  node [
    id 125
    label "otok"
  ]
  node [
    id 126
    label "awers"
  ]
  node [
    id 127
    label "liga"
  ]
  node [
    id 128
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 129
    label "egzerga"
  ]
  node [
    id 130
    label "balansjerka"
  ]
  node [
    id 131
    label "plundering"
  ]
  node [
    id 132
    label "wydarzenie"
  ]
  node [
    id 133
    label "przest&#281;pstwo"
  ]
  node [
    id 134
    label "get"
  ]
  node [
    id 135
    label "zaj&#347;&#263;"
  ]
  node [
    id 136
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 137
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 138
    label "dop&#322;ata"
  ]
  node [
    id 139
    label "supervene"
  ]
  node [
    id 140
    label "heed"
  ]
  node [
    id 141
    label "dodatek"
  ]
  node [
    id 142
    label "catch"
  ]
  node [
    id 143
    label "uzyska&#263;"
  ]
  node [
    id 144
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 145
    label "orgazm"
  ]
  node [
    id 146
    label "dozna&#263;"
  ]
  node [
    id 147
    label "sta&#263;_si&#281;"
  ]
  node [
    id 148
    label "bodziec"
  ]
  node [
    id 149
    label "drive"
  ]
  node [
    id 150
    label "informacja"
  ]
  node [
    id 151
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 152
    label "spowodowa&#263;"
  ]
  node [
    id 153
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 154
    label "postrzega&#263;"
  ]
  node [
    id 155
    label "become"
  ]
  node [
    id 156
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 157
    label "dop&#322;yn&#261;&#263;"
  ]
  node [
    id 158
    label "przesy&#322;ka"
  ]
  node [
    id 159
    label "przy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 160
    label "dokoptowa&#263;"
  ]
  node [
    id 161
    label "zach&#243;d"
  ]
  node [
    id 162
    label "vesper"
  ]
  node [
    id 163
    label "spotkanie"
  ]
  node [
    id 164
    label "przyj&#281;cie"
  ]
  node [
    id 165
    label "pora"
  ]
  node [
    id 166
    label "dzie&#324;"
  ]
  node [
    id 167
    label "night"
  ]
  node [
    id 168
    label "stoisko"
  ]
  node [
    id 169
    label "sk&#322;ad"
  ]
  node [
    id 170
    label "firma"
  ]
  node [
    id 171
    label "lokal_u&#380;ytkowy"
  ]
  node [
    id 172
    label "witryna"
  ]
  node [
    id 173
    label "obiekt_handlowy"
  ]
  node [
    id 174
    label "zaplecze"
  ]
  node [
    id 175
    label "p&#243;&#322;ka"
  ]
  node [
    id 176
    label "biedny"
  ]
  node [
    id 177
    label "ofiara"
  ]
  node [
    id 178
    label "ukrzywdzony"
  ]
  node [
    id 179
    label "rozr&#243;&#380;ni&#263;"
  ]
  node [
    id 180
    label "topographic_point"
  ]
  node [
    id 181
    label "rozwa&#380;y&#263;"
  ]
  node [
    id 182
    label "set"
  ]
  node [
    id 183
    label "zorientowa&#263;_si&#281;"
  ]
  node [
    id 184
    label "przest&#281;pca"
  ]
  node [
    id 185
    label "ma&#322;oletni"
  ]
  node [
    id 186
    label "ma&#322;oletny"
  ]
  node [
    id 187
    label "m&#322;ody"
  ]
  node [
    id 188
    label "proceed"
  ]
  node [
    id 189
    label "pozosta&#263;"
  ]
  node [
    id 190
    label "osta&#263;_si&#281;"
  ]
  node [
    id 191
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 192
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 193
    label "change"
  ]
  node [
    id 194
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 195
    label "dokument"
  ]
  node [
    id 196
    label "znaczenie"
  ]
  node [
    id 197
    label "implicite"
  ]
  node [
    id 198
    label "transakcja"
  ]
  node [
    id 199
    label "order"
  ]
  node [
    id 200
    label "explicite"
  ]
  node [
    id 201
    label "draft"
  ]
  node [
    id 202
    label "proces"
  ]
  node [
    id 203
    label "kwota"
  ]
  node [
    id 204
    label "blankiet"
  ]
  node [
    id 205
    label "tekst"
  ]
  node [
    id 206
    label "opiekun"
  ]
  node [
    id 207
    label "wapniak"
  ]
  node [
    id 208
    label "rodzic_chrzestny"
  ]
  node [
    id 209
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 210
    label "rodzice"
  ]
  node [
    id 211
    label "wykupienie"
  ]
  node [
    id 212
    label "bycie_w_posiadaniu"
  ]
  node [
    id 213
    label "wykupywanie"
  ]
  node [
    id 214
    label "podmiot"
  ]
  node [
    id 215
    label "policzy&#263;"
  ]
  node [
    id 216
    label "ustali&#263;"
  ]
  node [
    id 217
    label "estimate"
  ]
  node [
    id 218
    label "cena"
  ]
  node [
    id 219
    label "szlachetny"
  ]
  node [
    id 220
    label "metaliczny"
  ]
  node [
    id 221
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 222
    label "z&#322;ocenie"
  ]
  node [
    id 223
    label "grosz"
  ]
  node [
    id 224
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 225
    label "utytu&#322;owany"
  ]
  node [
    id 226
    label "poz&#322;ocenie"
  ]
  node [
    id 227
    label "Polska"
  ]
  node [
    id 228
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 229
    label "wspania&#322;y"
  ]
  node [
    id 230
    label "doskona&#322;y"
  ]
  node [
    id 231
    label "kochany"
  ]
  node [
    id 232
    label "jednostka_monetarna"
  ]
  node [
    id 233
    label "z&#322;oci&#347;cie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 31
  ]
  edge [
    source 13
    target 32
  ]
  edge [
    source 13
    target 90
  ]
  edge [
    source 13
    target 106
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 13
    target 113
  ]
  edge [
    source 13
    target 114
  ]
  edge [
    source 13
    target 115
  ]
  edge [
    source 13
    target 116
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 117
  ]
  edge [
    source 14
    target 118
  ]
  edge [
    source 14
    target 119
  ]
  edge [
    source 14
    target 120
  ]
  edge [
    source 14
    target 121
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 122
  ]
  edge [
    source 15
    target 123
  ]
  edge [
    source 15
    target 124
  ]
  edge [
    source 15
    target 125
  ]
  edge [
    source 15
    target 126
  ]
  edge [
    source 15
    target 127
  ]
  edge [
    source 15
    target 128
  ]
  edge [
    source 15
    target 129
  ]
  edge [
    source 15
    target 130
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 131
  ]
  edge [
    source 16
    target 132
  ]
  edge [
    source 16
    target 133
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 134
  ]
  edge [
    source 17
    target 135
  ]
  edge [
    source 17
    target 136
  ]
  edge [
    source 17
    target 137
  ]
  edge [
    source 17
    target 138
  ]
  edge [
    source 17
    target 139
  ]
  edge [
    source 17
    target 140
  ]
  edge [
    source 17
    target 141
  ]
  edge [
    source 17
    target 142
  ]
  edge [
    source 17
    target 70
  ]
  edge [
    source 17
    target 143
  ]
  edge [
    source 17
    target 144
  ]
  edge [
    source 17
    target 145
  ]
  edge [
    source 17
    target 146
  ]
  edge [
    source 17
    target 147
  ]
  edge [
    source 17
    target 148
  ]
  edge [
    source 17
    target 149
  ]
  edge [
    source 17
    target 150
  ]
  edge [
    source 17
    target 151
  ]
  edge [
    source 17
    target 152
  ]
  edge [
    source 17
    target 153
  ]
  edge [
    source 17
    target 72
  ]
  edge [
    source 17
    target 154
  ]
  edge [
    source 17
    target 155
  ]
  edge [
    source 17
    target 156
  ]
  edge [
    source 17
    target 157
  ]
  edge [
    source 17
    target 158
  ]
  edge [
    source 17
    target 79
  ]
  edge [
    source 17
    target 159
  ]
  edge [
    source 17
    target 160
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 161
  ]
  edge [
    source 19
    target 162
  ]
  edge [
    source 19
    target 163
  ]
  edge [
    source 19
    target 164
  ]
  edge [
    source 19
    target 165
  ]
  edge [
    source 19
    target 166
  ]
  edge [
    source 19
    target 167
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 168
  ]
  edge [
    source 21
    target 169
  ]
  edge [
    source 21
    target 170
  ]
  edge [
    source 21
    target 171
  ]
  edge [
    source 21
    target 172
  ]
  edge [
    source 21
    target 173
  ]
  edge [
    source 21
    target 174
  ]
  edge [
    source 21
    target 175
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 176
  ]
  edge [
    source 23
    target 177
  ]
  edge [
    source 23
    target 178
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 179
  ]
  edge [
    source 24
    target 180
  ]
  edge [
    source 24
    target 181
  ]
  edge [
    source 24
    target 182
  ]
  edge [
    source 24
    target 183
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 184
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 90
  ]
  edge [
    source 26
    target 185
  ]
  edge [
    source 26
    target 186
  ]
  edge [
    source 26
    target 187
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 188
  ]
  edge [
    source 27
    target 142
  ]
  edge [
    source 27
    target 189
  ]
  edge [
    source 27
    target 190
  ]
  edge [
    source 27
    target 191
  ]
  edge [
    source 27
    target 192
  ]
  edge [
    source 27
    target 151
  ]
  edge [
    source 27
    target 193
  ]
  edge [
    source 27
    target 194
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 195
  ]
  edge [
    source 28
    target 196
  ]
  edge [
    source 28
    target 197
  ]
  edge [
    source 28
    target 198
  ]
  edge [
    source 28
    target 199
  ]
  edge [
    source 28
    target 200
  ]
  edge [
    source 28
    target 201
  ]
  edge [
    source 28
    target 202
  ]
  edge [
    source 28
    target 203
  ]
  edge [
    source 28
    target 204
  ]
  edge [
    source 28
    target 205
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 206
  ]
  edge [
    source 29
    target 207
  ]
  edge [
    source 29
    target 208
  ]
  edge [
    source 29
    target 209
  ]
  edge [
    source 29
    target 210
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 211
  ]
  edge [
    source 30
    target 212
  ]
  edge [
    source 30
    target 213
  ]
  edge [
    source 30
    target 214
  ]
  edge [
    source 31
    target 215
  ]
  edge [
    source 31
    target 216
  ]
  edge [
    source 31
    target 217
  ]
  edge [
    source 31
    target 218
  ]
  edge [
    source 32
    target 219
  ]
  edge [
    source 32
    target 220
  ]
  edge [
    source 32
    target 221
  ]
  edge [
    source 32
    target 222
  ]
  edge [
    source 32
    target 223
  ]
  edge [
    source 32
    target 224
  ]
  edge [
    source 32
    target 225
  ]
  edge [
    source 32
    target 226
  ]
  edge [
    source 32
    target 227
  ]
  edge [
    source 32
    target 228
  ]
  edge [
    source 32
    target 229
  ]
  edge [
    source 32
    target 230
  ]
  edge [
    source 32
    target 231
  ]
  edge [
    source 32
    target 232
  ]
  edge [
    source 32
    target 233
  ]
]
