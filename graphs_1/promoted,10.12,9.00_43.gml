graph [
  maxDegree 13
  minDegree 1
  meanDegree 1.935483870967742
  density 0.06451612903225806
  graphCliqueNumber 2
  node [
    id 0
    label "powa&#380;ny"
    origin "text"
  ]
  node [
    id 1
    label "wypadek"
    origin "text"
  ]
  node [
    id 2
    label "&#322;&#243;d&#378;"
    origin "text"
  ]
  node [
    id 3
    label "gro&#378;ny"
  ]
  node [
    id 4
    label "trudny"
  ]
  node [
    id 5
    label "du&#380;y"
  ]
  node [
    id 6
    label "spowa&#380;nienie"
  ]
  node [
    id 7
    label "prawdziwy"
  ]
  node [
    id 8
    label "powa&#380;nienie"
  ]
  node [
    id 9
    label "powa&#380;nie"
  ]
  node [
    id 10
    label "ci&#281;&#380;ko"
  ]
  node [
    id 11
    label "ci&#281;&#380;ki"
  ]
  node [
    id 12
    label "czynno&#347;&#263;"
  ]
  node [
    id 13
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 14
    label "motyw"
  ]
  node [
    id 15
    label "ubezpieczenie_wypadkowe"
  ]
  node [
    id 16
    label "fabu&#322;a"
  ]
  node [
    id 17
    label "przebiec"
  ]
  node [
    id 18
    label "wydarzenie"
  ]
  node [
    id 19
    label "happening"
  ]
  node [
    id 20
    label "przebiegni&#281;cie"
  ]
  node [
    id 21
    label "event"
  ]
  node [
    id 22
    label "charakter"
  ]
  node [
    id 23
    label "regaty"
  ]
  node [
    id 24
    label "statek"
  ]
  node [
    id 25
    label "spalin&#243;wka"
  ]
  node [
    id 26
    label "pok&#322;ad"
  ]
  node [
    id 27
    label "ster"
  ]
  node [
    id 28
    label "kratownica"
  ]
  node [
    id 29
    label "pojazd_niemechaniczny"
  ]
  node [
    id 30
    label "drzewce"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
]
