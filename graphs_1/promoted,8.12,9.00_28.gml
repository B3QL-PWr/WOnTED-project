graph [
  maxDegree 18
  minDegree 1
  meanDegree 1.9444444444444444
  density 0.05555555555555555
  graphCliqueNumber 2
  node [
    id 0
    label "kalifornia"
    origin "text"
  ]
  node [
    id 1
    label "wprowadzi&#263;"
    origin "text"
  ]
  node [
    id 2
    label "nowa"
    origin "text"
  ]
  node [
    id 3
    label "wymaganie"
    origin "text"
  ]
  node [
    id 4
    label "dotyczy&#263;"
    origin "text"
  ]
  node [
    id 5
    label "nieruchomo&#347;ci"
    origin "text"
  ]
  node [
    id 6
    label "wej&#347;&#263;"
  ]
  node [
    id 7
    label "rynek"
  ]
  node [
    id 8
    label "zacz&#261;&#263;"
  ]
  node [
    id 9
    label "zej&#347;&#263;"
  ]
  node [
    id 10
    label "spowodowa&#263;"
  ]
  node [
    id 11
    label "wpisa&#263;"
  ]
  node [
    id 12
    label "insert"
  ]
  node [
    id 13
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 14
    label "testify"
  ]
  node [
    id 15
    label "indicate"
  ]
  node [
    id 16
    label "zapozna&#263;"
  ]
  node [
    id 17
    label "umie&#347;ci&#263;"
  ]
  node [
    id 18
    label "zak&#322;&#243;ci&#263;"
  ]
  node [
    id 19
    label "zrobi&#263;"
  ]
  node [
    id 20
    label "doprowadzi&#263;"
  ]
  node [
    id 21
    label "picture"
  ]
  node [
    id 22
    label "gwiazda"
  ]
  node [
    id 23
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 24
    label "czynno&#347;&#263;"
  ]
  node [
    id 25
    label "bycie"
  ]
  node [
    id 26
    label "dopominanie_si&#281;"
  ]
  node [
    id 27
    label "zmuszanie"
  ]
  node [
    id 28
    label "umowa"
  ]
  node [
    id 29
    label "condition"
  ]
  node [
    id 30
    label "sequestration"
  ]
  node [
    id 31
    label "demand"
  ]
  node [
    id 32
    label "privation"
  ]
  node [
    id 33
    label "bargain"
  ]
  node [
    id 34
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 35
    label "tycze&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
]
