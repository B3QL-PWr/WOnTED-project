graph [
  maxDegree 8
  minDegree 1
  meanDegree 2.727272727272727
  density 0.08522727272727272
  graphCliqueNumber 7
  node [
    id 0
    label "hubert"
    origin "text"
  ]
  node [
    id 1
    label "pirker"
    origin "text"
  ]
  node [
    id 2
    label "Huberta"
  ]
  node [
    id 3
    label "Pirker"
  ]
  node [
    id 4
    label "Rennweg"
  ]
  node [
    id 5
    label "AM"
  ]
  node [
    id 6
    label "Katschberg"
  ]
  node [
    id 7
    label "parlament"
  ]
  node [
    id 8
    label "europejski"
  ]
  node [
    id 9
    label "uniwersytet"
  ]
  node [
    id 10
    label "wyspa"
  ]
  node [
    id 11
    label "Klagenfurcie"
  ]
  node [
    id 12
    label "austriacki"
  ]
  node [
    id 13
    label "partia"
  ]
  node [
    id 14
    label "ludowy"
  ]
  node [
    id 15
    label "rada"
  ]
  node [
    id 16
    label "narodowy"
  ]
  node [
    id 17
    label "unia"
  ]
  node [
    id 18
    label "Ursuli"
  ]
  node [
    id 19
    label "Stenzel"
  ]
  node [
    id 20
    label "komisja"
  ]
  node [
    id 21
    label "sprawi&#263;"
  ]
  node [
    id 22
    label "zagraniczny"
  ]
  node [
    id 23
    label "delegacja"
  ]
  node [
    id 24
    label "do"
  ]
  node [
    id 25
    label "stosunki"
  ]
  node [
    id 26
    label "zeszyt"
  ]
  node [
    id 27
    label "p&#243;&#322;wysep"
  ]
  node [
    id 28
    label "korea&#324;ski"
  ]
  node [
    id 29
    label "podkomisja"
  ]
  node [
    id 30
    label "bezpiecze&#324;stwo"
  ]
  node [
    id 31
    label "i"
  ]
  node [
    id 32
    label "obrona"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 17
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 22
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 23
  ]
  edge [
    source 21
    target 24
  ]
  edge [
    source 21
    target 25
  ]
  edge [
    source 21
    target 26
  ]
  edge [
    source 21
    target 27
  ]
  edge [
    source 21
    target 28
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 25
  ]
  edge [
    source 23
    target 26
  ]
  edge [
    source 23
    target 27
  ]
  edge [
    source 23
    target 28
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 26
  ]
  edge [
    source 24
    target 27
  ]
  edge [
    source 24
    target 28
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 27
  ]
  edge [
    source 25
    target 28
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 28
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 31
  ]
  edge [
    source 29
    target 32
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 32
  ]
  edge [
    source 31
    target 32
  ]
]
