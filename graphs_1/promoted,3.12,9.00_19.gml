graph [
  maxDegree 10
  minDegree 1
  meanDegree 1.9428571428571428
  density 0.05714285714285714
  graphCliqueNumber 2
  node [
    id 0
    label "kierowca"
    origin "text"
  ]
  node [
    id 1
    label "w&#281;gorzewo"
    origin "text"
  ]
  node [
    id 2
    label "napisa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "list"
    origin "text"
  ]
  node [
    id 4
    label "podzi&#281;kowanie"
    origin "text"
  ]
  node [
    id 5
    label "policjant"
    origin "text"
  ]
  node [
    id 6
    label "cz&#322;owiek"
  ]
  node [
    id 7
    label "transportowiec"
  ]
  node [
    id 8
    label "postawi&#263;"
  ]
  node [
    id 9
    label "prasa"
  ]
  node [
    id 10
    label "stworzy&#263;"
  ]
  node [
    id 11
    label "donie&#347;&#263;"
  ]
  node [
    id 12
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 13
    label "write"
  ]
  node [
    id 14
    label "styl"
  ]
  node [
    id 15
    label "read"
  ]
  node [
    id 16
    label "li&#347;&#263;"
  ]
  node [
    id 17
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 18
    label "poczta"
  ]
  node [
    id 19
    label "epistolografia"
  ]
  node [
    id 20
    label "przesy&#322;ka"
  ]
  node [
    id 21
    label "poczta_elektroniczna"
  ]
  node [
    id 22
    label "znaczek_pocztowy"
  ]
  node [
    id 23
    label "wypowied&#378;"
  ]
  node [
    id 24
    label "z&#322;o&#380;enie"
  ]
  node [
    id 25
    label "sprzeciwienie_si&#281;"
  ]
  node [
    id 26
    label "denial"
  ]
  node [
    id 27
    label "notyfikowanie"
  ]
  node [
    id 28
    label "notyfikowa&#263;"
  ]
  node [
    id 29
    label "policja"
  ]
  node [
    id 30
    label "blacharz"
  ]
  node [
    id 31
    label "pa&#322;a"
  ]
  node [
    id 32
    label "mundurowy"
  ]
  node [
    id 33
    label "str&#243;&#380;"
  ]
  node [
    id 34
    label "glina"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
]
