graph [
  maxDegree 22
  minDegree 1
  meanDegree 2
  density 0.02666666666666667
  graphCliqueNumber 3
  node [
    id 0
    label "ten"
    origin "text"
  ]
  node [
    id 1
    label "licytacja"
    origin "text"
  ]
  node [
    id 2
    label "unikat"
    origin "text"
  ]
  node [
    id 3
    label "gra"
    origin "text"
  ]
  node [
    id 4
    label "wied&#378;min"
    origin "text"
  ]
  node [
    id 5
    label "podpis"
    origin "text"
  ]
  node [
    id 6
    label "sapkowski"
    origin "text"
  ]
  node [
    id 7
    label "plastron"
    origin "text"
  ]
  node [
    id 8
    label "justyna"
    origin "text"
  ]
  node [
    id 9
    label "kowalczyk"
    origin "text"
  ]
  node [
    id 10
    label "unikalny"
    origin "text"
  ]
  node [
    id 11
    label "gad&#380;et"
    origin "text"
  ]
  node [
    id 12
    label "star"
    origin "text"
  ]
  node [
    id 13
    label "wars"
    origin "text"
  ]
  node [
    id 14
    label "etc"
    origin "text"
  ]
  node [
    id 15
    label "okre&#347;lony"
  ]
  node [
    id 16
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 17
    label "bryd&#380;"
  ]
  node [
    id 18
    label "rozdanie"
  ]
  node [
    id 19
    label "faza"
  ]
  node [
    id 20
    label "tysi&#261;c"
  ]
  node [
    id 21
    label "pas"
  ]
  node [
    id 22
    label "przetarg"
  ]
  node [
    id 23
    label "skat"
  ]
  node [
    id 24
    label "sprzeda&#380;"
  ]
  node [
    id 25
    label "gra_w_karty"
  ]
  node [
    id 26
    label "zabawa"
  ]
  node [
    id 27
    label "rywalizacja"
  ]
  node [
    id 28
    label "czynno&#347;&#263;"
  ]
  node [
    id 29
    label "Pok&#233;mon"
  ]
  node [
    id 30
    label "synteza"
  ]
  node [
    id 31
    label "odtworzenie"
  ]
  node [
    id 32
    label "komplet"
  ]
  node [
    id 33
    label "rekwizyt_do_gry"
  ]
  node [
    id 34
    label "odg&#322;os"
  ]
  node [
    id 35
    label "rozgrywka"
  ]
  node [
    id 36
    label "post&#281;powanie"
  ]
  node [
    id 37
    label "wydarzenie"
  ]
  node [
    id 38
    label "apparent_motion"
  ]
  node [
    id 39
    label "game"
  ]
  node [
    id 40
    label "zmienno&#347;&#263;"
  ]
  node [
    id 41
    label "zasada"
  ]
  node [
    id 42
    label "akcja"
  ]
  node [
    id 43
    label "play"
  ]
  node [
    id 44
    label "contest"
  ]
  node [
    id 45
    label "zbijany"
  ]
  node [
    id 46
    label "posta&#263;_literacka"
  ]
  node [
    id 47
    label "najemnik"
  ]
  node [
    id 48
    label "istota_fantastyczna"
  ]
  node [
    id 49
    label "w&#322;asnor&#281;czny"
  ]
  node [
    id 50
    label "napis"
  ]
  node [
    id 51
    label "obja&#347;nienie"
  ]
  node [
    id 52
    label "sign"
  ]
  node [
    id 53
    label "potwierdzenie"
  ]
  node [
    id 54
    label "signal"
  ]
  node [
    id 55
    label "znak"
  ]
  node [
    id 56
    label "&#380;&#243;&#322;w"
  ]
  node [
    id 57
    label "napier&#347;nik"
  ]
  node [
    id 58
    label "zbroja"
  ]
  node [
    id 59
    label "krawat"
  ]
  node [
    id 60
    label "skorupa"
  ]
  node [
    id 61
    label "ochraniacz"
  ]
  node [
    id 62
    label "koszula"
  ]
  node [
    id 63
    label "czeladnik"
  ]
  node [
    id 64
    label "ucze&#324;"
  ]
  node [
    id 65
    label "pojedynczy"
  ]
  node [
    id 66
    label "osobny"
  ]
  node [
    id 67
    label "wyj&#261;tkowy"
  ]
  node [
    id 68
    label "unikatowo"
  ]
  node [
    id 69
    label "specyficzny"
  ]
  node [
    id 70
    label "unikalnie"
  ]
  node [
    id 71
    label "przedmiot"
  ]
  node [
    id 72
    label "Justyna"
  ]
  node [
    id 73
    label "Star"
  ]
  node [
    id 74
    label "wykop"
  ]
  node [
    id 75
    label "efekt"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 65
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 10
    target 69
  ]
  edge [
    source 10
    target 70
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 71
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 73
  ]
  edge [
    source 74
    target 75
  ]
]
