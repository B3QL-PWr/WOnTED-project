graph [
  maxDegree 39
  minDegree 1
  meanDegree 2.140127388535032
  density 0.00455346252879794
  graphCliqueNumber 3
  node [
    id 0
    label "hubert"
    origin "text"
  ]
  node [
    id 1
    label "sekretarz"
    origin "text"
  ]
  node [
    id 2
    label "landau"
    origin "text"
  ]
  node [
    id 3
    label "zacz&#261;&#263;"
    origin "text"
  ]
  node [
    id 4
    label "sw&#243;j"
    origin "text"
  ]
  node [
    id 5
    label "dzienia"
    origin "text"
  ]
  node [
    id 6
    label "jak"
    origin "text"
  ]
  node [
    id 7
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 8
    label "inny"
    origin "text"
  ]
  node [
    id 9
    label "punktualnie"
    origin "text"
  ]
  node [
    id 10
    label "&#243;sma"
    origin "text"
  ]
  node [
    id 11
    label "rano"
    origin "text"
  ]
  node [
    id 12
    label "siedzie&#263;"
    origin "text"
  ]
  node [
    id 13
    label "przy"
    origin "text"
  ]
  node [
    id 14
    label "swoje"
    origin "text"
  ]
  node [
    id 15
    label "pulpit"
    origin "text"
  ]
  node [
    id 16
    label "pok&#243;j"
    origin "text"
  ]
  node [
    id 17
    label "administracja"
    origin "text"
  ]
  node [
    id 18
    label "przypomina&#263;"
    origin "text"
  ]
  node [
    id 19
    label "siebie"
    origin "text"
  ]
  node [
    id 20
    label "przygotowa&#263;"
    origin "text"
  ]
  node [
    id 21
    label "wczesno"
    origin "text"
  ]
  node [
    id 22
    label "rozk&#322;ad"
    origin "text"
  ]
  node [
    id 23
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 24
    label "du&#380;o"
    origin "text"
  ]
  node [
    id 25
    label "niepok&#243;j"
    origin "text"
  ]
  node [
    id 26
    label "przysparza&#263;"
    origin "text"
  ]
  node [
    id 27
    label "punkt"
    origin "text"
  ]
  node [
    id 28
    label "trzeci"
    origin "text"
  ]
  node [
    id 29
    label "wizyta"
    origin "text"
  ]
  node [
    id 30
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 31
    label "lord"
    origin "text"
  ]
  node [
    id 32
    label "bartela"
    origin "text"
  ]
  node [
    id 33
    label "bogacz"
    origin "text"
  ]
  node [
    id 34
    label "laerdmor"
    origin "text"
  ]
  node [
    id 35
    label "lukas"
    origin "text"
  ]
  node [
    id 36
    label "uzna&#263;"
    origin "text"
  ]
  node [
    id 37
    label "w&#322;a&#347;ciwy"
    origin "text"
  ]
  node [
    id 38
    label "po&#322;&#261;czy&#263;"
    origin "text"
  ]
  node [
    id 39
    label "nim"
    origin "text"
  ]
  node [
    id 40
    label "si&#322;a"
    origin "text"
  ]
  node [
    id 41
    label "pewne"
    origin "text"
  ]
  node [
    id 42
    label "zakres"
    origin "text"
  ]
  node [
    id 43
    label "dlatego"
    origin "text"
  ]
  node [
    id 44
    label "dzisiaj"
    origin "text"
  ]
  node [
    id 45
    label "w&#322;a&#347;nie"
    origin "text"
  ]
  node [
    id 46
    label "kilka"
    origin "text"
  ]
  node [
    id 47
    label "godzina"
    origin "text"
  ]
  node [
    id 48
    label "mia&#322;"
    origin "text"
  ]
  node [
    id 49
    label "oprowadzi&#263;"
    origin "text"
  ]
  node [
    id 50
    label "wys&#322;annik"
    origin "text"
  ]
  node [
    id 51
    label "czu&#263;"
    origin "text"
  ]
  node [
    id 52
    label "zdenerwowanie"
    origin "text"
  ]
  node [
    id 53
    label "zamy&#347;lenie"
    origin "text"
  ]
  node [
    id 54
    label "wyrwa&#263;"
    origin "text"
  ]
  node [
    id 55
    label "pukanie"
    origin "text"
  ]
  node [
    id 56
    label "drzwi"
    origin "text"
  ]
  node [
    id 57
    label "gabinet"
    origin "text"
  ]
  node [
    id 58
    label "wej&#347;&#263;"
    origin "text"
  ]
  node [
    id 59
    label "ejlo"
    origin "text"
  ]
  node [
    id 60
    label "asystent"
    origin "text"
  ]
  node [
    id 61
    label "ptak_drapie&#380;ny"
  ]
  node [
    id 62
    label "sekretarze"
  ]
  node [
    id 63
    label "odznaczenie"
  ]
  node [
    id 64
    label "kancelaria"
  ]
  node [
    id 65
    label "urz&#281;dnik"
  ]
  node [
    id 66
    label "Gomu&#322;ka"
  ]
  node [
    id 67
    label "tytu&#322;"
  ]
  node [
    id 68
    label "Jan_Czeczot"
  ]
  node [
    id 69
    label "ptak_egzotyczny"
  ]
  node [
    id 70
    label "Bre&#380;niew"
  ]
  node [
    id 71
    label "Gierek"
  ]
  node [
    id 72
    label "pracownik_biurowy"
  ]
  node [
    id 73
    label "zwierzchnik"
  ]
  node [
    id 74
    label "cause"
  ]
  node [
    id 75
    label "introduce"
  ]
  node [
    id 76
    label "begin"
  ]
  node [
    id 77
    label "odj&#261;&#263;"
  ]
  node [
    id 78
    label "post&#261;pi&#263;"
  ]
  node [
    id 79
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 80
    label "do"
  ]
  node [
    id 81
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 82
    label "zrobi&#263;"
  ]
  node [
    id 83
    label "bli&#378;ni"
  ]
  node [
    id 84
    label "odpowiedni"
  ]
  node [
    id 85
    label "swojak"
  ]
  node [
    id 86
    label "samodzielny"
  ]
  node [
    id 87
    label "byd&#322;o"
  ]
  node [
    id 88
    label "zobo"
  ]
  node [
    id 89
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 90
    label "yakalo"
  ]
  node [
    id 91
    label "dzo"
  ]
  node [
    id 92
    label "jaki&#347;"
  ]
  node [
    id 93
    label "kolejny"
  ]
  node [
    id 94
    label "inaczej"
  ]
  node [
    id 95
    label "r&#243;&#380;ny"
  ]
  node [
    id 96
    label "inszy"
  ]
  node [
    id 97
    label "osobno"
  ]
  node [
    id 98
    label "punktualny"
  ]
  node [
    id 99
    label "planowo"
  ]
  node [
    id 100
    label "wsch&#243;d"
  ]
  node [
    id 101
    label "aurora"
  ]
  node [
    id 102
    label "pora"
  ]
  node [
    id 103
    label "zjawisko"
  ]
  node [
    id 104
    label "trwa&#263;"
  ]
  node [
    id 105
    label "sit"
  ]
  node [
    id 106
    label "pause"
  ]
  node [
    id 107
    label "ptak"
  ]
  node [
    id 108
    label "garowa&#263;"
  ]
  node [
    id 109
    label "mieszka&#263;"
  ]
  node [
    id 110
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 111
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 112
    label "przebywa&#263;"
  ]
  node [
    id 113
    label "brood"
  ]
  node [
    id 114
    label "zwierz&#281;"
  ]
  node [
    id 115
    label "doprowadza&#263;"
  ]
  node [
    id 116
    label "spoczywa&#263;"
  ]
  node [
    id 117
    label "tkwi&#263;"
  ]
  node [
    id 118
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 119
    label "system_operacyjny"
  ]
  node [
    id 120
    label "ikona"
  ]
  node [
    id 121
    label "mebel"
  ]
  node [
    id 122
    label "obszar"
  ]
  node [
    id 123
    label "podstawa"
  ]
  node [
    id 124
    label "interfejs"
  ]
  node [
    id 125
    label "blat"
  ]
  node [
    id 126
    label "okno"
  ]
  node [
    id 127
    label "uk&#322;ad"
  ]
  node [
    id 128
    label "spok&#243;j"
  ]
  node [
    id 129
    label "preliminarium_pokojowe"
  ]
  node [
    id 130
    label "grupa"
  ]
  node [
    id 131
    label "mir"
  ]
  node [
    id 132
    label "pacyfista"
  ]
  node [
    id 133
    label "pomieszczenie"
  ]
  node [
    id 134
    label "zarz&#261;d"
  ]
  node [
    id 135
    label "gospodarka"
  ]
  node [
    id 136
    label "petent"
  ]
  node [
    id 137
    label "biuro"
  ]
  node [
    id 138
    label "dziekanat"
  ]
  node [
    id 139
    label "struktura"
  ]
  node [
    id 140
    label "siedziba"
  ]
  node [
    id 141
    label "recall"
  ]
  node [
    id 142
    label "u&#347;wiadamia&#263;"
  ]
  node [
    id 143
    label "informowa&#263;"
  ]
  node [
    id 144
    label "prompt"
  ]
  node [
    id 145
    label "arrange"
  ]
  node [
    id 146
    label "spowodowa&#263;"
  ]
  node [
    id 147
    label "dress"
  ]
  node [
    id 148
    label "wyszkoli&#263;"
  ]
  node [
    id 149
    label "narz&#261;dzi&#263;"
  ]
  node [
    id 150
    label "wytworzy&#263;"
  ]
  node [
    id 151
    label "ukierunkowa&#263;"
  ]
  node [
    id 152
    label "train"
  ]
  node [
    id 153
    label "wykona&#263;"
  ]
  node [
    id 154
    label "cook"
  ]
  node [
    id 155
    label "set"
  ]
  node [
    id 156
    label "wcze&#347;nie"
  ]
  node [
    id 157
    label "u&#322;o&#380;enie"
  ]
  node [
    id 158
    label "miara_probabilistyczna"
  ]
  node [
    id 159
    label "reticule"
  ]
  node [
    id 160
    label "katabolizm"
  ]
  node [
    id 161
    label "wyst&#281;powanie"
  ]
  node [
    id 162
    label "zwierzyna"
  ]
  node [
    id 163
    label "dissociation"
  ]
  node [
    id 164
    label "cecha"
  ]
  node [
    id 165
    label "czas_p&#243;&#322;trwania"
  ]
  node [
    id 166
    label "inclination"
  ]
  node [
    id 167
    label "manner"
  ]
  node [
    id 168
    label "dissolution"
  ]
  node [
    id 169
    label "kondycja"
  ]
  node [
    id 170
    label "proces_chemiczny"
  ]
  node [
    id 171
    label "plan"
  ]
  node [
    id 172
    label "proces"
  ]
  node [
    id 173
    label "rozmieszczenie"
  ]
  node [
    id 174
    label "reducent"
  ]
  node [
    id 175
    label "proces_fizyczny"
  ]
  node [
    id 176
    label "antykataboliczny"
  ]
  node [
    id 177
    label "s&#322;o&#324;ce"
  ]
  node [
    id 178
    label "czynienie_si&#281;"
  ]
  node [
    id 179
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 180
    label "czas"
  ]
  node [
    id 181
    label "long_time"
  ]
  node [
    id 182
    label "przedpo&#322;udnie"
  ]
  node [
    id 183
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 184
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 185
    label "tydzie&#324;"
  ]
  node [
    id 186
    label "t&#322;usty_czwartek"
  ]
  node [
    id 187
    label "wsta&#263;"
  ]
  node [
    id 188
    label "day"
  ]
  node [
    id 189
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 190
    label "przedwiecz&#243;r"
  ]
  node [
    id 191
    label "Sylwester"
  ]
  node [
    id 192
    label "po&#322;udnie"
  ]
  node [
    id 193
    label "wzej&#347;cie"
  ]
  node [
    id 194
    label "podwiecz&#243;r"
  ]
  node [
    id 195
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 196
    label "termin"
  ]
  node [
    id 197
    label "ranek"
  ]
  node [
    id 198
    label "doba"
  ]
  node [
    id 199
    label "wiecz&#243;r"
  ]
  node [
    id 200
    label "walentynki"
  ]
  node [
    id 201
    label "popo&#322;udnie"
  ]
  node [
    id 202
    label "noc"
  ]
  node [
    id 203
    label "wstanie"
  ]
  node [
    id 204
    label "du&#380;y"
  ]
  node [
    id 205
    label "cz&#281;sto"
  ]
  node [
    id 206
    label "bardzo"
  ]
  node [
    id 207
    label "mocno"
  ]
  node [
    id 208
    label "wiela"
  ]
  node [
    id 209
    label "akatyzja"
  ]
  node [
    id 210
    label "niespokojno&#347;&#263;"
  ]
  node [
    id 211
    label "emocja"
  ]
  node [
    id 212
    label "w&#261;tpliwo&#347;&#263;"
  ]
  node [
    id 213
    label "motion"
  ]
  node [
    id 214
    label "nerwowo&#347;&#263;"
  ]
  node [
    id 215
    label "zwi&#281;ksza&#263;"
  ]
  node [
    id 216
    label "dodawa&#263;"
  ]
  node [
    id 217
    label "enlarge"
  ]
  node [
    id 218
    label "prosta"
  ]
  node [
    id 219
    label "po&#322;o&#380;enie"
  ]
  node [
    id 220
    label "chwila"
  ]
  node [
    id 221
    label "ust&#281;p"
  ]
  node [
    id 222
    label "problemat"
  ]
  node [
    id 223
    label "kres"
  ]
  node [
    id 224
    label "mark"
  ]
  node [
    id 225
    label "pozycja"
  ]
  node [
    id 226
    label "point"
  ]
  node [
    id 227
    label "stopie&#324;_pisma"
  ]
  node [
    id 228
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 229
    label "przestrze&#324;"
  ]
  node [
    id 230
    label "wojsko"
  ]
  node [
    id 231
    label "problematyka"
  ]
  node [
    id 232
    label "zapunktowa&#263;"
  ]
  node [
    id 233
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 234
    label "obiekt_matematyczny"
  ]
  node [
    id 235
    label "sprawa"
  ]
  node [
    id 236
    label "plamka"
  ]
  node [
    id 237
    label "miejsce"
  ]
  node [
    id 238
    label "obiekt"
  ]
  node [
    id 239
    label "podpunkt"
  ]
  node [
    id 240
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 241
    label "jednostka"
  ]
  node [
    id 242
    label "neutralny"
  ]
  node [
    id 243
    label "przypadkowy"
  ]
  node [
    id 244
    label "postronnie"
  ]
  node [
    id 245
    label "go&#347;&#263;"
  ]
  node [
    id 246
    label "leczenie"
  ]
  node [
    id 247
    label "odwiedzalno&#347;&#263;"
  ]
  node [
    id 248
    label "pobyt"
  ]
  node [
    id 249
    label "odwiedziny"
  ]
  node [
    id 250
    label "asymilowa&#263;"
  ]
  node [
    id 251
    label "wapniak"
  ]
  node [
    id 252
    label "dwun&#243;g"
  ]
  node [
    id 253
    label "polifag"
  ]
  node [
    id 254
    label "wz&#243;r"
  ]
  node [
    id 255
    label "profanum"
  ]
  node [
    id 256
    label "hominid"
  ]
  node [
    id 257
    label "homo_sapiens"
  ]
  node [
    id 258
    label "nasada"
  ]
  node [
    id 259
    label "podw&#322;adny"
  ]
  node [
    id 260
    label "ludzko&#347;&#263;"
  ]
  node [
    id 261
    label "os&#322;abianie"
  ]
  node [
    id 262
    label "mikrokosmos"
  ]
  node [
    id 263
    label "portrecista"
  ]
  node [
    id 264
    label "duch"
  ]
  node [
    id 265
    label "oddzia&#322;ywanie"
  ]
  node [
    id 266
    label "g&#322;owa"
  ]
  node [
    id 267
    label "asymilowanie"
  ]
  node [
    id 268
    label "osoba"
  ]
  node [
    id 269
    label "os&#322;abia&#263;"
  ]
  node [
    id 270
    label "figura"
  ]
  node [
    id 271
    label "Adam"
  ]
  node [
    id 272
    label "senior"
  ]
  node [
    id 273
    label "antropochoria"
  ]
  node [
    id 274
    label "posta&#263;"
  ]
  node [
    id 275
    label "dostojnik"
  ]
  node [
    id 276
    label "arystokrata"
  ]
  node [
    id 277
    label "lordostwo"
  ]
  node [
    id 278
    label "milord"
  ]
  node [
    id 279
    label "nabab"
  ]
  node [
    id 280
    label "bogaty"
  ]
  node [
    id 281
    label "rede"
  ]
  node [
    id 282
    label "assent"
  ]
  node [
    id 283
    label "oceni&#263;"
  ]
  node [
    id 284
    label "przyzna&#263;"
  ]
  node [
    id 285
    label "stwierdzi&#263;"
  ]
  node [
    id 286
    label "see"
  ]
  node [
    id 287
    label "taki"
  ]
  node [
    id 288
    label "nale&#380;yty"
  ]
  node [
    id 289
    label "charakterystyczny"
  ]
  node [
    id 290
    label "stosownie"
  ]
  node [
    id 291
    label "dobry"
  ]
  node [
    id 292
    label "prawdziwy"
  ]
  node [
    id 293
    label "ten"
  ]
  node [
    id 294
    label "uprawniony"
  ]
  node [
    id 295
    label "zasadniczy"
  ]
  node [
    id 296
    label "typowy"
  ]
  node [
    id 297
    label "nale&#380;ny"
  ]
  node [
    id 298
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 299
    label "relate"
  ]
  node [
    id 300
    label "stworzy&#263;"
  ]
  node [
    id 301
    label "po&#322;&#261;czenie"
  ]
  node [
    id 302
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 303
    label "connect"
  ]
  node [
    id 304
    label "zjednoczy&#263;"
  ]
  node [
    id 305
    label "incorporate"
  ]
  node [
    id 306
    label "gra_planszowa"
  ]
  node [
    id 307
    label "magnitude"
  ]
  node [
    id 308
    label "energia"
  ]
  node [
    id 309
    label "capacity"
  ]
  node [
    id 310
    label "wuchta"
  ]
  node [
    id 311
    label "parametr"
  ]
  node [
    id 312
    label "moment_si&#322;y"
  ]
  node [
    id 313
    label "przemoc"
  ]
  node [
    id 314
    label "zdolno&#347;&#263;"
  ]
  node [
    id 315
    label "mn&#243;stwo"
  ]
  node [
    id 316
    label "wektorowa_wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 317
    label "rozwi&#261;zanie"
  ]
  node [
    id 318
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 319
    label "potencja"
  ]
  node [
    id 320
    label "zaleta"
  ]
  node [
    id 321
    label "wielko&#347;&#263;"
  ]
  node [
    id 322
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 323
    label "granica"
  ]
  node [
    id 324
    label "circle"
  ]
  node [
    id 325
    label "podzakres"
  ]
  node [
    id 326
    label "zbi&#243;r"
  ]
  node [
    id 327
    label "desygnat"
  ]
  node [
    id 328
    label "sfera"
  ]
  node [
    id 329
    label "dziedzina"
  ]
  node [
    id 330
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 331
    label "dzi&#347;"
  ]
  node [
    id 332
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 333
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 334
    label "dok&#322;adnie"
  ]
  node [
    id 335
    label "&#347;ledziowate"
  ]
  node [
    id 336
    label "ryba"
  ]
  node [
    id 337
    label "minuta"
  ]
  node [
    id 338
    label "p&#243;&#322;godzina"
  ]
  node [
    id 339
    label "kwadrans"
  ]
  node [
    id 340
    label "time"
  ]
  node [
    id 341
    label "jednostka_czasu"
  ]
  node [
    id 342
    label "proszek"
  ]
  node [
    id 343
    label "pokaza&#263;"
  ]
  node [
    id 344
    label "guidebook"
  ]
  node [
    id 345
    label "przedstawiciel"
  ]
  node [
    id 346
    label "ablegat"
  ]
  node [
    id 347
    label "by&#263;"
  ]
  node [
    id 348
    label "uczuwa&#263;"
  ]
  node [
    id 349
    label "przewidywa&#263;"
  ]
  node [
    id 350
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 351
    label "smell"
  ]
  node [
    id 352
    label "postrzega&#263;"
  ]
  node [
    id 353
    label "doznawa&#263;"
  ]
  node [
    id 354
    label "spirit"
  ]
  node [
    id 355
    label "anticipate"
  ]
  node [
    id 356
    label "spina"
  ]
  node [
    id 357
    label "z&#322;o&#347;&#263;"
  ]
  node [
    id 358
    label "wzbudzenie"
  ]
  node [
    id 359
    label "wkurzenie"
  ]
  node [
    id 360
    label "irritation"
  ]
  node [
    id 361
    label "fuss"
  ]
  node [
    id 362
    label "zrobienie"
  ]
  node [
    id 363
    label "czynno&#347;&#263;"
  ]
  node [
    id 364
    label "stan"
  ]
  node [
    id 365
    label "przygotowywanie"
  ]
  node [
    id 366
    label "refleksyjno&#347;&#263;"
  ]
  node [
    id 367
    label "przygotowanie"
  ]
  node [
    id 368
    label "proposition"
  ]
  node [
    id 369
    label "uwolni&#263;"
  ]
  node [
    id 370
    label "wydosta&#263;"
  ]
  node [
    id 371
    label "poderwa&#263;"
  ]
  node [
    id 372
    label "zabra&#263;"
  ]
  node [
    id 373
    label "extract"
  ]
  node [
    id 374
    label "fascinate"
  ]
  node [
    id 375
    label "pull"
  ]
  node [
    id 376
    label "podnie&#347;&#263;"
  ]
  node [
    id 377
    label "wywo&#322;a&#263;"
  ]
  node [
    id 378
    label "ruszy&#263;"
  ]
  node [
    id 379
    label "odpalanie"
  ]
  node [
    id 380
    label "wystrzelanie"
  ]
  node [
    id 381
    label "robienie"
  ]
  node [
    id 382
    label "trafianie"
  ]
  node [
    id 383
    label "uleganie"
  ]
  node [
    id 384
    label "ostrzeliwanie"
  ]
  node [
    id 385
    label "wiretap"
  ]
  node [
    id 386
    label "odstrzelenie"
  ]
  node [
    id 387
    label "plucie"
  ]
  node [
    id 388
    label "palenie"
  ]
  node [
    id 389
    label "stuk"
  ]
  node [
    id 390
    label "kropni&#281;cie"
  ]
  node [
    id 391
    label "wstrzeliwanie_si&#281;"
  ]
  node [
    id 392
    label "uderzanie"
  ]
  node [
    id 393
    label "branie"
  ]
  node [
    id 394
    label "wylatywanie"
  ]
  node [
    id 395
    label "prze&#322;adowywanie"
  ]
  node [
    id 396
    label "zestrzelenie"
  ]
  node [
    id 397
    label "grzanie"
  ]
  node [
    id 398
    label "fire"
  ]
  node [
    id 399
    label "chybienie"
  ]
  node [
    id 400
    label "wystukanie"
  ]
  node [
    id 401
    label "postukanie"
  ]
  node [
    id 402
    label "postukiwanie"
  ]
  node [
    id 403
    label "odstrzeliwanie"
  ]
  node [
    id 404
    label "wystukiwanie"
  ]
  node [
    id 405
    label "przestrzeliwanie"
  ]
  node [
    id 406
    label "kucie"
  ]
  node [
    id 407
    label "zastukanie"
  ]
  node [
    id 408
    label "postrzelanie"
  ]
  node [
    id 409
    label "zestrzeliwanie"
  ]
  node [
    id 410
    label "walczenie"
  ]
  node [
    id 411
    label "ostrzelanie"
  ]
  node [
    id 412
    label "pat"
  ]
  node [
    id 413
    label "chybianie"
  ]
  node [
    id 414
    label "powodowanie"
  ]
  node [
    id 415
    label "wyj&#347;cie"
  ]
  node [
    id 416
    label "wytw&#243;r"
  ]
  node [
    id 417
    label "doj&#347;cie"
  ]
  node [
    id 418
    label "skrzyd&#322;o"
  ]
  node [
    id 419
    label "zamek"
  ]
  node [
    id 420
    label "futryna"
  ]
  node [
    id 421
    label "antaba"
  ]
  node [
    id 422
    label "szafa"
  ]
  node [
    id 423
    label "szafka"
  ]
  node [
    id 424
    label "wej&#347;cie"
  ]
  node [
    id 425
    label "zawiasy"
  ]
  node [
    id 426
    label "samozamykacz"
  ]
  node [
    id 427
    label "ko&#322;atka"
  ]
  node [
    id 428
    label "klamka"
  ]
  node [
    id 429
    label "wrzeci&#261;dz"
  ]
  node [
    id 430
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 431
    label "s&#261;d"
  ]
  node [
    id 432
    label "egzekutywa"
  ]
  node [
    id 433
    label "biurko"
  ]
  node [
    id 434
    label "gabinet_cieni"
  ]
  node [
    id 435
    label "palestra"
  ]
  node [
    id 436
    label "premier"
  ]
  node [
    id 437
    label "Londyn"
  ]
  node [
    id 438
    label "Konsulat"
  ]
  node [
    id 439
    label "pracownia"
  ]
  node [
    id 440
    label "instytucja"
  ]
  node [
    id 441
    label "boks"
  ]
  node [
    id 442
    label "szko&#322;a"
  ]
  node [
    id 443
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 444
    label "get"
  ]
  node [
    id 445
    label "pozna&#263;"
  ]
  node [
    id 446
    label "spotka&#263;"
  ]
  node [
    id 447
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 448
    label "przenikn&#261;&#263;"
  ]
  node [
    id 449
    label "submit"
  ]
  node [
    id 450
    label "nast&#261;pi&#263;"
  ]
  node [
    id 451
    label "zag&#322;&#281;bi&#263;_si&#281;"
  ]
  node [
    id 452
    label "ascend"
  ]
  node [
    id 453
    label "intervene"
  ]
  node [
    id 454
    label "catch"
  ]
  node [
    id 455
    label "doj&#347;&#263;"
  ]
  node [
    id 456
    label "wnikn&#261;&#263;"
  ]
  node [
    id 457
    label "przekroczy&#263;"
  ]
  node [
    id 458
    label "zaistnie&#263;"
  ]
  node [
    id 459
    label "sta&#263;_si&#281;"
  ]
  node [
    id 460
    label "wzi&#261;&#263;"
  ]
  node [
    id 461
    label "drapn&#261;&#263;_si&#281;"
  ]
  node [
    id 462
    label "z&#322;oi&#263;"
  ]
  node [
    id 463
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 464
    label "w&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 465
    label "wznie&#347;&#263;_si&#281;"
  ]
  node [
    id 466
    label "move"
  ]
  node [
    id 467
    label "become"
  ]
  node [
    id 468
    label "przy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 469
    label "pomocnik"
  ]
  node [
    id 470
    label "nauczyciel_akademicki"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 47
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 100
  ]
  edge [
    source 11
    target 101
  ]
  edge [
    source 11
    target 102
  ]
  edge [
    source 11
    target 103
  ]
  edge [
    source 11
    target 23
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 12
    target 107
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 12
    target 109
  ]
  edge [
    source 12
    target 110
  ]
  edge [
    source 12
    target 111
  ]
  edge [
    source 12
    target 112
  ]
  edge [
    source 12
    target 113
  ]
  edge [
    source 12
    target 114
  ]
  edge [
    source 12
    target 115
  ]
  edge [
    source 12
    target 116
  ]
  edge [
    source 12
    target 117
  ]
  edge [
    source 12
    target 118
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 119
  ]
  edge [
    source 15
    target 120
  ]
  edge [
    source 15
    target 121
  ]
  edge [
    source 15
    target 122
  ]
  edge [
    source 15
    target 123
  ]
  edge [
    source 15
    target 124
  ]
  edge [
    source 15
    target 125
  ]
  edge [
    source 15
    target 126
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 127
  ]
  edge [
    source 16
    target 128
  ]
  edge [
    source 16
    target 129
  ]
  edge [
    source 16
    target 130
  ]
  edge [
    source 16
    target 131
  ]
  edge [
    source 16
    target 132
  ]
  edge [
    source 16
    target 133
  ]
  edge [
    source 16
    target 57
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 134
  ]
  edge [
    source 17
    target 135
  ]
  edge [
    source 17
    target 136
  ]
  edge [
    source 17
    target 137
  ]
  edge [
    source 17
    target 138
  ]
  edge [
    source 17
    target 139
  ]
  edge [
    source 17
    target 140
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 141
  ]
  edge [
    source 18
    target 142
  ]
  edge [
    source 18
    target 143
  ]
  edge [
    source 18
    target 144
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 145
  ]
  edge [
    source 20
    target 146
  ]
  edge [
    source 20
    target 147
  ]
  edge [
    source 20
    target 148
  ]
  edge [
    source 20
    target 149
  ]
  edge [
    source 20
    target 150
  ]
  edge [
    source 20
    target 151
  ]
  edge [
    source 20
    target 152
  ]
  edge [
    source 20
    target 153
  ]
  edge [
    source 20
    target 82
  ]
  edge [
    source 20
    target 154
  ]
  edge [
    source 20
    target 155
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 156
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 157
  ]
  edge [
    source 22
    target 158
  ]
  edge [
    source 22
    target 159
  ]
  edge [
    source 22
    target 160
  ]
  edge [
    source 22
    target 161
  ]
  edge [
    source 22
    target 162
  ]
  edge [
    source 22
    target 163
  ]
  edge [
    source 22
    target 164
  ]
  edge [
    source 22
    target 165
  ]
  edge [
    source 22
    target 166
  ]
  edge [
    source 22
    target 167
  ]
  edge [
    source 22
    target 127
  ]
  edge [
    source 22
    target 168
  ]
  edge [
    source 22
    target 169
  ]
  edge [
    source 22
    target 170
  ]
  edge [
    source 22
    target 171
  ]
  edge [
    source 22
    target 172
  ]
  edge [
    source 22
    target 173
  ]
  edge [
    source 22
    target 174
  ]
  edge [
    source 22
    target 175
  ]
  edge [
    source 22
    target 176
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 177
  ]
  edge [
    source 23
    target 178
  ]
  edge [
    source 23
    target 179
  ]
  edge [
    source 23
    target 180
  ]
  edge [
    source 23
    target 181
  ]
  edge [
    source 23
    target 182
  ]
  edge [
    source 23
    target 183
  ]
  edge [
    source 23
    target 184
  ]
  edge [
    source 23
    target 185
  ]
  edge [
    source 23
    target 47
  ]
  edge [
    source 23
    target 186
  ]
  edge [
    source 23
    target 187
  ]
  edge [
    source 23
    target 188
  ]
  edge [
    source 23
    target 189
  ]
  edge [
    source 23
    target 190
  ]
  edge [
    source 23
    target 191
  ]
  edge [
    source 23
    target 192
  ]
  edge [
    source 23
    target 193
  ]
  edge [
    source 23
    target 194
  ]
  edge [
    source 23
    target 195
  ]
  edge [
    source 23
    target 196
  ]
  edge [
    source 23
    target 197
  ]
  edge [
    source 23
    target 198
  ]
  edge [
    source 23
    target 199
  ]
  edge [
    source 23
    target 200
  ]
  edge [
    source 23
    target 201
  ]
  edge [
    source 23
    target 202
  ]
  edge [
    source 23
    target 203
  ]
  edge [
    source 23
    target 28
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 204
  ]
  edge [
    source 24
    target 205
  ]
  edge [
    source 24
    target 206
  ]
  edge [
    source 24
    target 207
  ]
  edge [
    source 24
    target 208
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 209
  ]
  edge [
    source 25
    target 210
  ]
  edge [
    source 25
    target 211
  ]
  edge [
    source 25
    target 212
  ]
  edge [
    source 25
    target 213
  ]
  edge [
    source 25
    target 214
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 215
  ]
  edge [
    source 26
    target 216
  ]
  edge [
    source 26
    target 217
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 218
  ]
  edge [
    source 27
    target 219
  ]
  edge [
    source 27
    target 220
  ]
  edge [
    source 27
    target 221
  ]
  edge [
    source 27
    target 222
  ]
  edge [
    source 27
    target 223
  ]
  edge [
    source 27
    target 224
  ]
  edge [
    source 27
    target 225
  ]
  edge [
    source 27
    target 226
  ]
  edge [
    source 27
    target 227
  ]
  edge [
    source 27
    target 228
  ]
  edge [
    source 27
    target 229
  ]
  edge [
    source 27
    target 230
  ]
  edge [
    source 27
    target 231
  ]
  edge [
    source 27
    target 232
  ]
  edge [
    source 27
    target 233
  ]
  edge [
    source 27
    target 234
  ]
  edge [
    source 27
    target 235
  ]
  edge [
    source 27
    target 236
  ]
  edge [
    source 27
    target 237
  ]
  edge [
    source 27
    target 238
  ]
  edge [
    source 27
    target 171
  ]
  edge [
    source 27
    target 239
  ]
  edge [
    source 27
    target 240
  ]
  edge [
    source 27
    target 241
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 30
  ]
  edge [
    source 28
    target 242
  ]
  edge [
    source 28
    target 243
  ]
  edge [
    source 28
    target 244
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 245
  ]
  edge [
    source 29
    target 246
  ]
  edge [
    source 29
    target 247
  ]
  edge [
    source 29
    target 248
  ]
  edge [
    source 29
    target 249
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 250
  ]
  edge [
    source 30
    target 251
  ]
  edge [
    source 30
    target 252
  ]
  edge [
    source 30
    target 253
  ]
  edge [
    source 30
    target 254
  ]
  edge [
    source 30
    target 255
  ]
  edge [
    source 30
    target 256
  ]
  edge [
    source 30
    target 257
  ]
  edge [
    source 30
    target 258
  ]
  edge [
    source 30
    target 259
  ]
  edge [
    source 30
    target 260
  ]
  edge [
    source 30
    target 261
  ]
  edge [
    source 30
    target 262
  ]
  edge [
    source 30
    target 263
  ]
  edge [
    source 30
    target 264
  ]
  edge [
    source 30
    target 265
  ]
  edge [
    source 30
    target 266
  ]
  edge [
    source 30
    target 267
  ]
  edge [
    source 30
    target 268
  ]
  edge [
    source 30
    target 269
  ]
  edge [
    source 30
    target 270
  ]
  edge [
    source 30
    target 271
  ]
  edge [
    source 30
    target 272
  ]
  edge [
    source 30
    target 273
  ]
  edge [
    source 30
    target 274
  ]
  edge [
    source 30
    target 33
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 275
  ]
  edge [
    source 31
    target 276
  ]
  edge [
    source 31
    target 277
  ]
  edge [
    source 31
    target 67
  ]
  edge [
    source 31
    target 278
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 50
  ]
  edge [
    source 32
    target 51
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 279
  ]
  edge [
    source 33
    target 280
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 281
  ]
  edge [
    source 36
    target 282
  ]
  edge [
    source 36
    target 283
  ]
  edge [
    source 36
    target 284
  ]
  edge [
    source 36
    target 285
  ]
  edge [
    source 36
    target 286
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 287
  ]
  edge [
    source 37
    target 288
  ]
  edge [
    source 37
    target 289
  ]
  edge [
    source 37
    target 290
  ]
  edge [
    source 37
    target 291
  ]
  edge [
    source 37
    target 292
  ]
  edge [
    source 37
    target 293
  ]
  edge [
    source 37
    target 294
  ]
  edge [
    source 37
    target 295
  ]
  edge [
    source 37
    target 296
  ]
  edge [
    source 37
    target 297
  ]
  edge [
    source 37
    target 298
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 299
  ]
  edge [
    source 38
    target 146
  ]
  edge [
    source 38
    target 300
  ]
  edge [
    source 38
    target 301
  ]
  edge [
    source 38
    target 302
  ]
  edge [
    source 38
    target 303
  ]
  edge [
    source 38
    target 304
  ]
  edge [
    source 38
    target 305
  ]
  edge [
    source 38
    target 82
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 306
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 230
  ]
  edge [
    source 40
    target 307
  ]
  edge [
    source 40
    target 308
  ]
  edge [
    source 40
    target 309
  ]
  edge [
    source 40
    target 310
  ]
  edge [
    source 40
    target 164
  ]
  edge [
    source 40
    target 311
  ]
  edge [
    source 40
    target 312
  ]
  edge [
    source 40
    target 313
  ]
  edge [
    source 40
    target 314
  ]
  edge [
    source 40
    target 315
  ]
  edge [
    source 40
    target 316
  ]
  edge [
    source 40
    target 317
  ]
  edge [
    source 40
    target 318
  ]
  edge [
    source 40
    target 319
  ]
  edge [
    source 40
    target 103
  ]
  edge [
    source 40
    target 320
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 321
  ]
  edge [
    source 42
    target 322
  ]
  edge [
    source 42
    target 323
  ]
  edge [
    source 42
    target 324
  ]
  edge [
    source 42
    target 325
  ]
  edge [
    source 42
    target 326
  ]
  edge [
    source 42
    target 327
  ]
  edge [
    source 42
    target 328
  ]
  edge [
    source 42
    target 329
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 198
  ]
  edge [
    source 44
    target 330
  ]
  edge [
    source 44
    target 331
  ]
  edge [
    source 44
    target 332
  ]
  edge [
    source 44
    target 333
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 334
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 335
  ]
  edge [
    source 46
    target 336
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 337
  ]
  edge [
    source 47
    target 198
  ]
  edge [
    source 47
    target 180
  ]
  edge [
    source 47
    target 338
  ]
  edge [
    source 47
    target 339
  ]
  edge [
    source 47
    target 340
  ]
  edge [
    source 47
    target 341
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 342
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 343
  ]
  edge [
    source 49
    target 344
  ]
  edge [
    source 50
    target 345
  ]
  edge [
    source 50
    target 346
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 347
  ]
  edge [
    source 51
    target 348
  ]
  edge [
    source 51
    target 349
  ]
  edge [
    source 51
    target 350
  ]
  edge [
    source 51
    target 351
  ]
  edge [
    source 51
    target 352
  ]
  edge [
    source 51
    target 353
  ]
  edge [
    source 51
    target 354
  ]
  edge [
    source 51
    target 355
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 356
  ]
  edge [
    source 52
    target 209
  ]
  edge [
    source 52
    target 211
  ]
  edge [
    source 52
    target 357
  ]
  edge [
    source 52
    target 213
  ]
  edge [
    source 52
    target 358
  ]
  edge [
    source 52
    target 359
  ]
  edge [
    source 52
    target 214
  ]
  edge [
    source 52
    target 360
  ]
  edge [
    source 52
    target 361
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 362
  ]
  edge [
    source 53
    target 363
  ]
  edge [
    source 53
    target 364
  ]
  edge [
    source 53
    target 365
  ]
  edge [
    source 53
    target 366
  ]
  edge [
    source 53
    target 367
  ]
  edge [
    source 53
    target 368
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 369
  ]
  edge [
    source 54
    target 370
  ]
  edge [
    source 54
    target 371
  ]
  edge [
    source 54
    target 372
  ]
  edge [
    source 54
    target 373
  ]
  edge [
    source 54
    target 374
  ]
  edge [
    source 54
    target 375
  ]
  edge [
    source 54
    target 376
  ]
  edge [
    source 54
    target 377
  ]
  edge [
    source 54
    target 378
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 379
  ]
  edge [
    source 55
    target 380
  ]
  edge [
    source 55
    target 381
  ]
  edge [
    source 55
    target 382
  ]
  edge [
    source 55
    target 383
  ]
  edge [
    source 55
    target 384
  ]
  edge [
    source 55
    target 385
  ]
  edge [
    source 55
    target 386
  ]
  edge [
    source 55
    target 387
  ]
  edge [
    source 55
    target 388
  ]
  edge [
    source 55
    target 389
  ]
  edge [
    source 55
    target 390
  ]
  edge [
    source 55
    target 391
  ]
  edge [
    source 55
    target 392
  ]
  edge [
    source 55
    target 393
  ]
  edge [
    source 55
    target 394
  ]
  edge [
    source 55
    target 395
  ]
  edge [
    source 55
    target 363
  ]
  edge [
    source 55
    target 396
  ]
  edge [
    source 55
    target 397
  ]
  edge [
    source 55
    target 398
  ]
  edge [
    source 55
    target 399
  ]
  edge [
    source 55
    target 400
  ]
  edge [
    source 55
    target 401
  ]
  edge [
    source 55
    target 402
  ]
  edge [
    source 55
    target 403
  ]
  edge [
    source 55
    target 404
  ]
  edge [
    source 55
    target 405
  ]
  edge [
    source 55
    target 406
  ]
  edge [
    source 55
    target 407
  ]
  edge [
    source 55
    target 408
  ]
  edge [
    source 55
    target 409
  ]
  edge [
    source 55
    target 410
  ]
  edge [
    source 55
    target 411
  ]
  edge [
    source 55
    target 412
  ]
  edge [
    source 55
    target 413
  ]
  edge [
    source 55
    target 414
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 415
  ]
  edge [
    source 56
    target 416
  ]
  edge [
    source 56
    target 417
  ]
  edge [
    source 56
    target 418
  ]
  edge [
    source 56
    target 419
  ]
  edge [
    source 56
    target 420
  ]
  edge [
    source 56
    target 421
  ]
  edge [
    source 56
    target 422
  ]
  edge [
    source 56
    target 423
  ]
  edge [
    source 56
    target 424
  ]
  edge [
    source 56
    target 425
  ]
  edge [
    source 56
    target 426
  ]
  edge [
    source 56
    target 427
  ]
  edge [
    source 56
    target 428
  ]
  edge [
    source 56
    target 429
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 430
  ]
  edge [
    source 57
    target 431
  ]
  edge [
    source 57
    target 432
  ]
  edge [
    source 57
    target 433
  ]
  edge [
    source 57
    target 434
  ]
  edge [
    source 57
    target 435
  ]
  edge [
    source 57
    target 436
  ]
  edge [
    source 57
    target 437
  ]
  edge [
    source 57
    target 438
  ]
  edge [
    source 57
    target 439
  ]
  edge [
    source 57
    target 440
  ]
  edge [
    source 57
    target 441
  ]
  edge [
    source 57
    target 133
  ]
  edge [
    source 57
    target 442
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 443
  ]
  edge [
    source 58
    target 444
  ]
  edge [
    source 58
    target 445
  ]
  edge [
    source 58
    target 446
  ]
  edge [
    source 58
    target 447
  ]
  edge [
    source 58
    target 448
  ]
  edge [
    source 58
    target 449
  ]
  edge [
    source 58
    target 450
  ]
  edge [
    source 58
    target 451
  ]
  edge [
    source 58
    target 452
  ]
  edge [
    source 58
    target 453
  ]
  edge [
    source 58
    target 454
  ]
  edge [
    source 58
    target 455
  ]
  edge [
    source 58
    target 456
  ]
  edge [
    source 58
    target 457
  ]
  edge [
    source 58
    target 458
  ]
  edge [
    source 58
    target 459
  ]
  edge [
    source 58
    target 460
  ]
  edge [
    source 58
    target 461
  ]
  edge [
    source 58
    target 462
  ]
  edge [
    source 58
    target 463
  ]
  edge [
    source 58
    target 464
  ]
  edge [
    source 58
    target 465
  ]
  edge [
    source 58
    target 466
  ]
  edge [
    source 58
    target 467
  ]
  edge [
    source 58
    target 468
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 60
    target 469
  ]
  edge [
    source 60
    target 470
  ]
]
