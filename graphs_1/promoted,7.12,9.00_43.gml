graph [
  maxDegree 32
  minDegree 1
  meanDegree 2.011627906976744
  density 0.01176390588875289
  graphCliqueNumber 2
  node [
    id 0
    label "tysi&#261;c"
    origin "text"
  ]
  node [
    id 1
    label "z&#322;oty"
    origin "text"
  ]
  node [
    id 2
    label "oferowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "darczy&#324;ca"
    origin "text"
  ]
  node [
    id 4
    label "wskazanie"
    origin "text"
  ]
  node [
    id 5
    label "sprawca"
    origin "text"
  ]
  node [
    id 6
    label "bestialski"
    origin "text"
  ]
  node [
    id 7
    label "powiesi&#263;"
    origin "text"
  ]
  node [
    id 8
    label "pies"
    origin "text"
  ]
  node [
    id 9
    label "jaki"
    origin "text"
  ]
  node [
    id 10
    label "doj&#347;&#263;"
    origin "text"
  ]
  node [
    id 11
    label "przed"
    origin "text"
  ]
  node [
    id 12
    label "kilka"
    origin "text"
  ]
  node [
    id 13
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 14
    label "teren"
    origin "text"
  ]
  node [
    id 15
    label "wa&#322;brzych"
    origin "text"
  ]
  node [
    id 16
    label "nieopodal"
    origin "text"
  ]
  node [
    id 17
    label "schronisko"
    origin "text"
  ]
  node [
    id 18
    label "tauzen"
  ]
  node [
    id 19
    label "musik"
  ]
  node [
    id 20
    label "molarity"
  ]
  node [
    id 21
    label "licytacja"
  ]
  node [
    id 22
    label "patyk"
  ]
  node [
    id 23
    label "liczba"
  ]
  node [
    id 24
    label "gra_w_karty"
  ]
  node [
    id 25
    label "kwota"
  ]
  node [
    id 26
    label "szlachetny"
  ]
  node [
    id 27
    label "metaliczny"
  ]
  node [
    id 28
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 29
    label "z&#322;oci&#347;cie"
  ]
  node [
    id 30
    label "grosz"
  ]
  node [
    id 31
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 32
    label "utytu&#322;owany"
  ]
  node [
    id 33
    label "poz&#322;ocenie"
  ]
  node [
    id 34
    label "Polska"
  ]
  node [
    id 35
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 36
    label "wspania&#322;y"
  ]
  node [
    id 37
    label "doskona&#322;y"
  ]
  node [
    id 38
    label "kochany"
  ]
  node [
    id 39
    label "jednostka_monetarna"
  ]
  node [
    id 40
    label "z&#322;ocenie"
  ]
  node [
    id 41
    label "volunteer"
  ]
  node [
    id 42
    label "zach&#281;ca&#263;"
  ]
  node [
    id 43
    label "cz&#322;owiek"
  ]
  node [
    id 44
    label "wynik"
  ]
  node [
    id 45
    label "podkre&#347;lenie"
  ]
  node [
    id 46
    label "wybranie"
  ]
  node [
    id 47
    label "appointment"
  ]
  node [
    id 48
    label "podanie"
  ]
  node [
    id 49
    label "education"
  ]
  node [
    id 50
    label "przyczyna"
  ]
  node [
    id 51
    label "meaning"
  ]
  node [
    id 52
    label "wskaz&#243;wka"
  ]
  node [
    id 53
    label "pokazanie"
  ]
  node [
    id 54
    label "wyja&#347;nienie"
  ]
  node [
    id 55
    label "sprawiciel"
  ]
  node [
    id 56
    label "z&#322;y"
  ]
  node [
    id 57
    label "okrutny"
  ]
  node [
    id 58
    label "zwierz&#281;cy"
  ]
  node [
    id 59
    label "bestialsko"
  ]
  node [
    id 60
    label "stryczek"
  ]
  node [
    id 61
    label "szubienica"
  ]
  node [
    id 62
    label "bent"
  ]
  node [
    id 63
    label "suspend"
  ]
  node [
    id 64
    label "umie&#347;ci&#263;"
  ]
  node [
    id 65
    label "zabi&#263;"
  ]
  node [
    id 66
    label "wy&#263;"
  ]
  node [
    id 67
    label "ssak_drapie&#380;ny"
  ]
  node [
    id 68
    label "spragniony"
  ]
  node [
    id 69
    label "rakarz"
  ]
  node [
    id 70
    label "psowate"
  ]
  node [
    id 71
    label "istota_&#380;ywa"
  ]
  node [
    id 72
    label "kabanos"
  ]
  node [
    id 73
    label "mi&#322;o&#347;nik"
  ]
  node [
    id 74
    label "&#322;ajdak"
  ]
  node [
    id 75
    label "czworon&#243;g"
  ]
  node [
    id 76
    label "policjant"
  ]
  node [
    id 77
    label "szczucie"
  ]
  node [
    id 78
    label "s&#322;u&#380;enie"
  ]
  node [
    id 79
    label "sobaka"
  ]
  node [
    id 80
    label "dogoterapia"
  ]
  node [
    id 81
    label "Cerber"
  ]
  node [
    id 82
    label "wyzwisko"
  ]
  node [
    id 83
    label "szczu&#263;"
  ]
  node [
    id 84
    label "wycie"
  ]
  node [
    id 85
    label "szczeka&#263;"
  ]
  node [
    id 86
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 87
    label "trufla"
  ]
  node [
    id 88
    label "samiec"
  ]
  node [
    id 89
    label "piese&#322;"
  ]
  node [
    id 90
    label "zawy&#263;"
  ]
  node [
    id 91
    label "get"
  ]
  node [
    id 92
    label "zaj&#347;&#263;"
  ]
  node [
    id 93
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 94
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 95
    label "dop&#322;ata"
  ]
  node [
    id 96
    label "supervene"
  ]
  node [
    id 97
    label "heed"
  ]
  node [
    id 98
    label "dodatek"
  ]
  node [
    id 99
    label "catch"
  ]
  node [
    id 100
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 101
    label "uzyska&#263;"
  ]
  node [
    id 102
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 103
    label "orgazm"
  ]
  node [
    id 104
    label "dozna&#263;"
  ]
  node [
    id 105
    label "sta&#263;_si&#281;"
  ]
  node [
    id 106
    label "bodziec"
  ]
  node [
    id 107
    label "drive"
  ]
  node [
    id 108
    label "informacja"
  ]
  node [
    id 109
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 110
    label "spowodowa&#263;"
  ]
  node [
    id 111
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 112
    label "dotrze&#263;"
  ]
  node [
    id 113
    label "postrzega&#263;"
  ]
  node [
    id 114
    label "become"
  ]
  node [
    id 115
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 116
    label "dop&#322;yn&#261;&#263;"
  ]
  node [
    id 117
    label "przesy&#322;ka"
  ]
  node [
    id 118
    label "dolecie&#263;"
  ]
  node [
    id 119
    label "przy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 120
    label "dokoptowa&#263;"
  ]
  node [
    id 121
    label "&#347;ledziowate"
  ]
  node [
    id 122
    label "ryba"
  ]
  node [
    id 123
    label "s&#322;o&#324;ce"
  ]
  node [
    id 124
    label "czynienie_si&#281;"
  ]
  node [
    id 125
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 126
    label "czas"
  ]
  node [
    id 127
    label "long_time"
  ]
  node [
    id 128
    label "przedpo&#322;udnie"
  ]
  node [
    id 129
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 130
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 131
    label "tydzie&#324;"
  ]
  node [
    id 132
    label "godzina"
  ]
  node [
    id 133
    label "t&#322;usty_czwartek"
  ]
  node [
    id 134
    label "wsta&#263;"
  ]
  node [
    id 135
    label "day"
  ]
  node [
    id 136
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 137
    label "przedwiecz&#243;r"
  ]
  node [
    id 138
    label "Sylwester"
  ]
  node [
    id 139
    label "po&#322;udnie"
  ]
  node [
    id 140
    label "wzej&#347;cie"
  ]
  node [
    id 141
    label "podwiecz&#243;r"
  ]
  node [
    id 142
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 143
    label "rano"
  ]
  node [
    id 144
    label "termin"
  ]
  node [
    id 145
    label "ranek"
  ]
  node [
    id 146
    label "doba"
  ]
  node [
    id 147
    label "wiecz&#243;r"
  ]
  node [
    id 148
    label "walentynki"
  ]
  node [
    id 149
    label "popo&#322;udnie"
  ]
  node [
    id 150
    label "noc"
  ]
  node [
    id 151
    label "wstanie"
  ]
  node [
    id 152
    label "zakres"
  ]
  node [
    id 153
    label "kontekst"
  ]
  node [
    id 154
    label "wymiar"
  ]
  node [
    id 155
    label "obszar"
  ]
  node [
    id 156
    label "krajobraz"
  ]
  node [
    id 157
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 158
    label "w&#322;adza"
  ]
  node [
    id 159
    label "nation"
  ]
  node [
    id 160
    label "przyroda"
  ]
  node [
    id 161
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 162
    label "miejsce_pracy"
  ]
  node [
    id 163
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 164
    label "tu&#380;_tu&#380;"
  ]
  node [
    id 165
    label "bliski"
  ]
  node [
    id 166
    label "plac&#243;wka_opieku&#324;czo-wychowawcza"
  ]
  node [
    id 167
    label "budynek_zamieszkania_zbiorowego"
  ]
  node [
    id 168
    label "nocleg"
  ]
  node [
    id 169
    label "lokal"
  ]
  node [
    id 170
    label "jaskinia"
  ]
  node [
    id 171
    label "dom"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 10
    target 105
  ]
  edge [
    source 10
    target 106
  ]
  edge [
    source 10
    target 107
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 10
    target 110
  ]
  edge [
    source 10
    target 111
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 121
  ]
  edge [
    source 12
    target 122
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 123
  ]
  edge [
    source 13
    target 124
  ]
  edge [
    source 13
    target 125
  ]
  edge [
    source 13
    target 126
  ]
  edge [
    source 13
    target 127
  ]
  edge [
    source 13
    target 128
  ]
  edge [
    source 13
    target 129
  ]
  edge [
    source 13
    target 130
  ]
  edge [
    source 13
    target 131
  ]
  edge [
    source 13
    target 132
  ]
  edge [
    source 13
    target 133
  ]
  edge [
    source 13
    target 134
  ]
  edge [
    source 13
    target 135
  ]
  edge [
    source 13
    target 136
  ]
  edge [
    source 13
    target 137
  ]
  edge [
    source 13
    target 138
  ]
  edge [
    source 13
    target 139
  ]
  edge [
    source 13
    target 140
  ]
  edge [
    source 13
    target 141
  ]
  edge [
    source 13
    target 142
  ]
  edge [
    source 13
    target 143
  ]
  edge [
    source 13
    target 144
  ]
  edge [
    source 13
    target 145
  ]
  edge [
    source 13
    target 146
  ]
  edge [
    source 13
    target 147
  ]
  edge [
    source 13
    target 148
  ]
  edge [
    source 13
    target 149
  ]
  edge [
    source 13
    target 150
  ]
  edge [
    source 13
    target 151
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 152
  ]
  edge [
    source 14
    target 153
  ]
  edge [
    source 14
    target 154
  ]
  edge [
    source 14
    target 155
  ]
  edge [
    source 14
    target 156
  ]
  edge [
    source 14
    target 157
  ]
  edge [
    source 14
    target 158
  ]
  edge [
    source 14
    target 159
  ]
  edge [
    source 14
    target 160
  ]
  edge [
    source 14
    target 161
  ]
  edge [
    source 14
    target 162
  ]
  edge [
    source 14
    target 163
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 164
  ]
  edge [
    source 16
    target 165
  ]
  edge [
    source 17
    target 166
  ]
  edge [
    source 17
    target 167
  ]
  edge [
    source 17
    target 168
  ]
  edge [
    source 17
    target 169
  ]
  edge [
    source 17
    target 170
  ]
  edge [
    source 17
    target 171
  ]
]
