graph [
  maxDegree 52
  minDegree 1
  meanDegree 2
  density 0.017699115044247787
  graphCliqueNumber 2
  node [
    id 0
    label "stado"
    origin "text"
  ]
  node [
    id 1
    label "atakowa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "buddysta"
    origin "text"
  ]
  node [
    id 3
    label "myli&#263;"
    origin "text"
  ]
  node [
    id 4
    label "tatua&#380;"
    origin "text"
  ]
  node [
    id 5
    label "cia&#322;o"
    origin "text"
  ]
  node [
    id 6
    label "swastyka"
    origin "text"
  ]
  node [
    id 7
    label "oskar&#380;a&#263;"
    origin "text"
  ]
  node [
    id 8
    label "bycie"
    origin "text"
  ]
  node [
    id 9
    label "nazista"
    origin "text"
  ]
  node [
    id 10
    label "hurma"
  ]
  node [
    id 11
    label "school"
  ]
  node [
    id 12
    label "zbi&#243;r"
  ]
  node [
    id 13
    label "grupa"
  ]
  node [
    id 14
    label "napada&#263;"
  ]
  node [
    id 15
    label "dzia&#322;a&#263;"
  ]
  node [
    id 16
    label "nast&#281;powa&#263;"
  ]
  node [
    id 17
    label "strike"
  ]
  node [
    id 18
    label "trouble_oneself"
  ]
  node [
    id 19
    label "m&#243;wi&#263;"
  ]
  node [
    id 20
    label "walczy&#263;"
  ]
  node [
    id 21
    label "usi&#322;owa&#263;"
  ]
  node [
    id 22
    label "rozgrywa&#263;"
  ]
  node [
    id 23
    label "aim"
  ]
  node [
    id 24
    label "sport"
  ]
  node [
    id 25
    label "przewaga"
  ]
  node [
    id 26
    label "attack"
  ]
  node [
    id 27
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 28
    label "robi&#263;"
  ]
  node [
    id 29
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 30
    label "schorzenie"
  ]
  node [
    id 31
    label "epidemia"
  ]
  node [
    id 32
    label "krytykowa&#263;"
  ]
  node [
    id 33
    label "ofensywny"
  ]
  node [
    id 34
    label "wyznawca"
  ]
  node [
    id 35
    label "fool"
  ]
  node [
    id 36
    label "misinform"
  ]
  node [
    id 37
    label "mi&#281;sza&#263;"
  ]
  node [
    id 38
    label "mani&#263;"
  ]
  node [
    id 39
    label "oszukiwa&#263;"
  ]
  node [
    id 40
    label "ba&#322;amuci&#263;"
  ]
  node [
    id 41
    label "tattoo"
  ]
  node [
    id 42
    label "ozdoba"
  ]
  node [
    id 43
    label "technika"
  ]
  node [
    id 44
    label "balsamowa&#263;"
  ]
  node [
    id 45
    label "Komitet_Region&#243;w"
  ]
  node [
    id 46
    label "pochowa&#263;"
  ]
  node [
    id 47
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 48
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 49
    label "odwodnienie"
  ]
  node [
    id 50
    label "otworzenie"
  ]
  node [
    id 51
    label "zabalsamowanie"
  ]
  node [
    id 52
    label "tanatoplastyk"
  ]
  node [
    id 53
    label "biorytm"
  ]
  node [
    id 54
    label "istota_&#380;ywa"
  ]
  node [
    id 55
    label "zabalsamowa&#263;"
  ]
  node [
    id 56
    label "pogrzeb"
  ]
  node [
    id 57
    label "otwieranie"
  ]
  node [
    id 58
    label "tanatoplastyka"
  ]
  node [
    id 59
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 60
    label "l&#281;d&#378;wie"
  ]
  node [
    id 61
    label "sk&#243;ra"
  ]
  node [
    id 62
    label "nieumar&#322;y"
  ]
  node [
    id 63
    label "unerwienie"
  ]
  node [
    id 64
    label "sekcja"
  ]
  node [
    id 65
    label "ow&#322;osienie"
  ]
  node [
    id 66
    label "odwadnia&#263;"
  ]
  node [
    id 67
    label "zesp&#243;&#322;"
  ]
  node [
    id 68
    label "ekshumowa&#263;"
  ]
  node [
    id 69
    label "jednostka_organizacyjna"
  ]
  node [
    id 70
    label "kremacja"
  ]
  node [
    id 71
    label "pochowanie"
  ]
  node [
    id 72
    label "ekshumowanie"
  ]
  node [
    id 73
    label "otworzy&#263;"
  ]
  node [
    id 74
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 75
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 76
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 77
    label "balsamowanie"
  ]
  node [
    id 78
    label "Izba_Konsyliarska"
  ]
  node [
    id 79
    label "odwadnianie"
  ]
  node [
    id 80
    label "uk&#322;ad"
  ]
  node [
    id 81
    label "cz&#322;onek"
  ]
  node [
    id 82
    label "miejsce"
  ]
  node [
    id 83
    label "szkielet"
  ]
  node [
    id 84
    label "odwodni&#263;"
  ]
  node [
    id 85
    label "ty&#322;"
  ]
  node [
    id 86
    label "materia"
  ]
  node [
    id 87
    label "temperatura"
  ]
  node [
    id 88
    label "staw"
  ]
  node [
    id 89
    label "mi&#281;so"
  ]
  node [
    id 90
    label "prz&#243;d"
  ]
  node [
    id 91
    label "otwiera&#263;"
  ]
  node [
    id 92
    label "p&#322;aszczyzna"
  ]
  node [
    id 93
    label "ornament"
  ]
  node [
    id 94
    label "swastika"
  ]
  node [
    id 95
    label "symbol"
  ]
  node [
    id 96
    label "twierdzi&#263;"
  ]
  node [
    id 97
    label "prosecute"
  ]
  node [
    id 98
    label "produkowanie"
  ]
  node [
    id 99
    label "przeszkadzanie"
  ]
  node [
    id 100
    label "widzenie"
  ]
  node [
    id 101
    label "byt"
  ]
  node [
    id 102
    label "robienie"
  ]
  node [
    id 103
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 104
    label "znikni&#281;cie"
  ]
  node [
    id 105
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 106
    label "obejrzenie"
  ]
  node [
    id 107
    label "urzeczywistnianie"
  ]
  node [
    id 108
    label "wyprodukowanie"
  ]
  node [
    id 109
    label "dematerializowanie_si&#281;"
  ]
  node [
    id 110
    label "przeszkodzenie"
  ]
  node [
    id 111
    label "being"
  ]
  node [
    id 112
    label "Goebbels"
  ]
  node [
    id 113
    label "faszysta"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 12
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
]
