graph [
  maxDegree 42
  minDegree 1
  meanDegree 2.0784313725490198
  density 0.005838290372328706
  graphCliqueNumber 3
  node [
    id 0
    label "zwolennica"
    origin "text"
  ]
  node [
    id 1
    label "kondratiewowskiego"
    origin "text"
  ]
  node [
    id 2
    label "determinizm"
    origin "text"
  ]
  node [
    id 3
    label "skupowa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "z&#322;oto"
    origin "text"
  ]
  node [
    id 5
    label "keynesi&#347;ci"
    origin "text"
  ]
  node [
    id 6
    label "drukowa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "pieni&#261;dz"
    origin "text"
  ]
  node [
    id 8
    label "pozosta&#322;y"
    origin "text"
  ]
  node [
    id 9
    label "przy"
    origin "text"
  ]
  node [
    id 10
    label "&#380;ycie"
    origin "text"
  ]
  node [
    id 11
    label "bank"
    origin "text"
  ]
  node [
    id 12
    label "zapewne"
    origin "text"
  ]
  node [
    id 13
    label "poszukiwa&#263;"
    origin "text"
  ]
  node [
    id 14
    label "nowa"
    origin "text"
  ]
  node [
    id 15
    label "statystyk"
    origin "text"
  ]
  node [
    id 16
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 17
    label "specjalista"
    origin "text"
  ]
  node [
    id 18
    label "stochastyki"
    origin "text"
  ]
  node [
    id 19
    label "si&#281;"
    origin "text"
  ]
  node [
    id 20
    label "pomyli&#263;"
    origin "text"
  ]
  node [
    id 21
    label "inny"
    origin "text"
  ]
  node [
    id 22
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 23
    label "pewno"
    origin "text"
  ]
  node [
    id 24
    label "przewidzie&#263;"
    origin "text"
  ]
  node [
    id 25
    label "wszystko"
    origin "text"
  ]
  node [
    id 26
    label "w&#322;a&#347;ciwie"
    origin "text"
  ]
  node [
    id 27
    label "ekonomia"
    origin "text"
  ]
  node [
    id 28
    label "by&#322;a"
    origin "text"
  ]
  node [
    id 29
    label "za&#322;o&#380;enia"
    origin "text"
  ]
  node [
    id 30
    label "nauka"
    origin "text"
  ]
  node [
    id 31
    label "spo&#322;eczny"
    origin "text"
  ]
  node [
    id 32
    label "opisywa&#263;"
    origin "text"
  ]
  node [
    id 33
    label "analizowa&#263;"
    origin "text"
  ]
  node [
    id 34
    label "empiryczny"
    origin "text"
  ]
  node [
    id 35
    label "raczej"
    origin "text"
  ]
  node [
    id 36
    label "wszechstronny"
    origin "text"
  ]
  node [
    id 37
    label "wszechstronno&#347;&#263;"
    origin "text"
  ]
  node [
    id 38
    label "czas"
    origin "text"
  ]
  node [
    id 39
    label "wychodzi&#263;"
    origin "text"
  ]
  node [
    id 40
    label "bok"
    origin "text"
  ]
  node [
    id 41
    label "nadmierny"
    origin "text"
  ]
  node [
    id 42
    label "zapa&#322;"
    origin "text"
  ]
  node [
    id 43
    label "filozoficzny"
    origin "text"
  ]
  node [
    id 44
    label "u&#322;atwia&#263;"
    origin "text"
  ]
  node [
    id 45
    label "analiza"
    origin "text"
  ]
  node [
    id 46
    label "przeciwie&#324;stwo"
    origin "text"
  ]
  node [
    id 47
    label "czyste"
    origin "text"
  ]
  node [
    id 48
    label "elegancki"
    origin "text"
  ]
  node [
    id 49
    label "wz&#243;r"
    origin "text"
  ]
  node [
    id 50
    label "matematyczny"
    origin "text"
  ]
  node [
    id 51
    label "determinism"
  ]
  node [
    id 52
    label "koncepcja"
  ]
  node [
    id 53
    label "nieuchronno&#347;&#263;"
  ]
  node [
    id 54
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 55
    label "gromadzi&#263;"
  ]
  node [
    id 56
    label "kupowa&#263;"
  ]
  node [
    id 57
    label "purchase"
  ]
  node [
    id 58
    label "cz&#322;owiek"
  ]
  node [
    id 59
    label "amber"
  ]
  node [
    id 60
    label "z&#322;ocisty"
  ]
  node [
    id 61
    label "metalicznie"
  ]
  node [
    id 62
    label "p&#322;uczkarnia"
  ]
  node [
    id 63
    label "miedziowiec"
  ]
  node [
    id 64
    label "bi&#380;uteria"
  ]
  node [
    id 65
    label "metal_szlachetny"
  ]
  node [
    id 66
    label "pieni&#261;dze"
  ]
  node [
    id 67
    label "p&#322;ucznia"
  ]
  node [
    id 68
    label "medal"
  ]
  node [
    id 69
    label "&#380;&#243;&#322;&#263;"
  ]
  node [
    id 70
    label "&#380;&#243;&#322;to"
  ]
  node [
    id 71
    label "og&#322;asza&#263;"
  ]
  node [
    id 72
    label "odbija&#263;"
  ]
  node [
    id 73
    label "publish"
  ]
  node [
    id 74
    label "powiela&#263;"
  ]
  node [
    id 75
    label "poda&#380;_pieni&#261;dza"
  ]
  node [
    id 76
    label "zdewaluowa&#263;"
  ]
  node [
    id 77
    label "moniak"
  ]
  node [
    id 78
    label "wytw&#243;r"
  ]
  node [
    id 79
    label "zdewaluowanie"
  ]
  node [
    id 80
    label "jednostka_monetarna"
  ]
  node [
    id 81
    label "&#347;rodek_p&#322;atniczy"
  ]
  node [
    id 82
    label "numizmat"
  ]
  node [
    id 83
    label "rozmieni&#263;"
  ]
  node [
    id 84
    label "rozmienienie"
  ]
  node [
    id 85
    label "rozmienianie"
  ]
  node [
    id 86
    label "dewaluowanie"
  ]
  node [
    id 87
    label "nomina&#322;"
  ]
  node [
    id 88
    label "coin"
  ]
  node [
    id 89
    label "dewaluowa&#263;"
  ]
  node [
    id 90
    label "znak_pieni&#281;&#380;ny"
  ]
  node [
    id 91
    label "rozmienia&#263;"
  ]
  node [
    id 92
    label "&#380;ywy"
  ]
  node [
    id 93
    label "energy"
  ]
  node [
    id 94
    label "bycie"
  ]
  node [
    id 95
    label "zegar_biologiczny"
  ]
  node [
    id 96
    label "okres_noworodkowy"
  ]
  node [
    id 97
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 98
    label "entity"
  ]
  node [
    id 99
    label "prze&#380;ywanie"
  ]
  node [
    id 100
    label "prze&#380;ycie"
  ]
  node [
    id 101
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 102
    label "wiek_matuzalemowy"
  ]
  node [
    id 103
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 104
    label "dzieci&#324;stwo"
  ]
  node [
    id 105
    label "power"
  ]
  node [
    id 106
    label "szwung"
  ]
  node [
    id 107
    label "menopauza"
  ]
  node [
    id 108
    label "umarcie"
  ]
  node [
    id 109
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 110
    label "life"
  ]
  node [
    id 111
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 112
    label "rozw&#243;j"
  ]
  node [
    id 113
    label "po&#322;&#243;g"
  ]
  node [
    id 114
    label "byt"
  ]
  node [
    id 115
    label "przebywanie"
  ]
  node [
    id 116
    label "subsistence"
  ]
  node [
    id 117
    label "koleje_losu"
  ]
  node [
    id 118
    label "raj_utracony"
  ]
  node [
    id 119
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 120
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 121
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 122
    label "andropauza"
  ]
  node [
    id 123
    label "warunki"
  ]
  node [
    id 124
    label "do&#380;ywanie"
  ]
  node [
    id 125
    label "niemowl&#281;ctwo"
  ]
  node [
    id 126
    label "umieranie"
  ]
  node [
    id 127
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 128
    label "staro&#347;&#263;"
  ]
  node [
    id 129
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 130
    label "&#347;mier&#263;"
  ]
  node [
    id 131
    label "wk&#322;adca"
  ]
  node [
    id 132
    label "agencja"
  ]
  node [
    id 133
    label "konto"
  ]
  node [
    id 134
    label "agent_rozliczeniowy"
  ]
  node [
    id 135
    label "eurorynek"
  ]
  node [
    id 136
    label "zbi&#243;r"
  ]
  node [
    id 137
    label "instytucja"
  ]
  node [
    id 138
    label "siedziba"
  ]
  node [
    id 139
    label "kwota"
  ]
  node [
    id 140
    label "ask"
  ]
  node [
    id 141
    label "stara&#263;_si&#281;"
  ]
  node [
    id 142
    label "szuka&#263;"
  ]
  node [
    id 143
    label "look"
  ]
  node [
    id 144
    label "gwiazda"
  ]
  node [
    id 145
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 146
    label "matematyk"
  ]
  node [
    id 147
    label "znawca"
  ]
  node [
    id 148
    label "wyspecjalizowanie_si&#281;"
  ]
  node [
    id 149
    label "lekarz"
  ]
  node [
    id 150
    label "spec"
  ]
  node [
    id 151
    label "confuse"
  ]
  node [
    id 152
    label "pomi&#281;sza&#263;"
  ]
  node [
    id 153
    label "wykona&#263;"
  ]
  node [
    id 154
    label "popieprzy&#263;"
  ]
  node [
    id 155
    label "kolejny"
  ]
  node [
    id 156
    label "inaczej"
  ]
  node [
    id 157
    label "r&#243;&#380;ny"
  ]
  node [
    id 158
    label "inszy"
  ]
  node [
    id 159
    label "osobno"
  ]
  node [
    id 160
    label "envision"
  ]
  node [
    id 161
    label "zaplanowa&#263;"
  ]
  node [
    id 162
    label "spodzia&#263;_si&#281;"
  ]
  node [
    id 163
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 164
    label "lock"
  ]
  node [
    id 165
    label "absolut"
  ]
  node [
    id 166
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 167
    label "dobrze"
  ]
  node [
    id 168
    label "stosowny"
  ]
  node [
    id 169
    label "nale&#380;ycie"
  ]
  node [
    id 170
    label "charakterystycznie"
  ]
  node [
    id 171
    label "prawdziwie"
  ]
  node [
    id 172
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 173
    label "nale&#380;nie"
  ]
  node [
    id 174
    label "portfel_zam&#243;wie&#324;"
  ]
  node [
    id 175
    label "dysponowa&#263;"
  ]
  node [
    id 176
    label "nauka_ekonomiczna"
  ]
  node [
    id 177
    label "sprawno&#347;&#263;"
  ]
  node [
    id 178
    label "makroekonomia"
  ]
  node [
    id 179
    label "ekonomika"
  ]
  node [
    id 180
    label "ekonometria"
  ]
  node [
    id 181
    label "kierunek"
  ]
  node [
    id 182
    label "nominalizm"
  ]
  node [
    id 183
    label "farmakoekonomika"
  ]
  node [
    id 184
    label "katalaksja"
  ]
  node [
    id 185
    label "neuroekonimia"
  ]
  node [
    id 186
    label "ekonomia_gospodarstw_domowych"
  ]
  node [
    id 187
    label "bankowo&#347;&#263;"
  ]
  node [
    id 188
    label "ekonomia_instytucjonalna"
  ]
  node [
    id 189
    label "book-building"
  ]
  node [
    id 190
    label "mikroekonomia"
  ]
  node [
    id 191
    label "wydzia&#322;"
  ]
  node [
    id 192
    label "partnerka"
  ]
  node [
    id 193
    label "nauki_o_Ziemi"
  ]
  node [
    id 194
    label "teoria_naukowa"
  ]
  node [
    id 195
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 196
    label "nauki_o_poznaniu"
  ]
  node [
    id 197
    label "nomotetyczny"
  ]
  node [
    id 198
    label "metodologia"
  ]
  node [
    id 199
    label "przem&#243;wienie"
  ]
  node [
    id 200
    label "wiedza"
  ]
  node [
    id 201
    label "kultura_duchowa"
  ]
  node [
    id 202
    label "nauki_penalne"
  ]
  node [
    id 203
    label "systematyka"
  ]
  node [
    id 204
    label "inwentyka"
  ]
  node [
    id 205
    label "dziedzina"
  ]
  node [
    id 206
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 207
    label "miasteczko_rowerowe"
  ]
  node [
    id 208
    label "fotowoltaika"
  ]
  node [
    id 209
    label "porada"
  ]
  node [
    id 210
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 211
    label "proces"
  ]
  node [
    id 212
    label "imagineskopia"
  ]
  node [
    id 213
    label "typologia"
  ]
  node [
    id 214
    label "&#322;awa_szkolna"
  ]
  node [
    id 215
    label "niepubliczny"
  ]
  node [
    id 216
    label "spo&#322;ecznie"
  ]
  node [
    id 217
    label "publiczny"
  ]
  node [
    id 218
    label "zapoznawa&#263;"
  ]
  node [
    id 219
    label "represent"
  ]
  node [
    id 220
    label "badany"
  ]
  node [
    id 221
    label "consider"
  ]
  node [
    id 222
    label "poddawa&#263;"
  ]
  node [
    id 223
    label "bada&#263;"
  ]
  node [
    id 224
    label "rozpatrywa&#263;"
  ]
  node [
    id 225
    label "do&#347;wiadczalnie"
  ]
  node [
    id 226
    label "praktyczny"
  ]
  node [
    id 227
    label "ca&#322;o&#347;ciowy"
  ]
  node [
    id 228
    label "dok&#322;adny"
  ]
  node [
    id 229
    label "wszechstronnie"
  ]
  node [
    id 230
    label "r&#243;&#380;norodny"
  ]
  node [
    id 231
    label "wielostronnie"
  ]
  node [
    id 232
    label "zdolny"
  ]
  node [
    id 233
    label "szeroki"
  ]
  node [
    id 234
    label "versatility"
  ]
  node [
    id 235
    label "zdolno&#347;&#263;"
  ]
  node [
    id 236
    label "czasokres"
  ]
  node [
    id 237
    label "trawienie"
  ]
  node [
    id 238
    label "kategoria_gramatyczna"
  ]
  node [
    id 239
    label "period"
  ]
  node [
    id 240
    label "odczyt"
  ]
  node [
    id 241
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 242
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 243
    label "chwila"
  ]
  node [
    id 244
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 245
    label "poprzedzenie"
  ]
  node [
    id 246
    label "koniugacja"
  ]
  node [
    id 247
    label "dzieje"
  ]
  node [
    id 248
    label "poprzedzi&#263;"
  ]
  node [
    id 249
    label "przep&#322;ywanie"
  ]
  node [
    id 250
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 251
    label "odwlekanie_si&#281;"
  ]
  node [
    id 252
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 253
    label "Zeitgeist"
  ]
  node [
    id 254
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 255
    label "okres_czasu"
  ]
  node [
    id 256
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 257
    label "pochodzi&#263;"
  ]
  node [
    id 258
    label "schy&#322;ek"
  ]
  node [
    id 259
    label "czwarty_wymiar"
  ]
  node [
    id 260
    label "chronometria"
  ]
  node [
    id 261
    label "poprzedzanie"
  ]
  node [
    id 262
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 263
    label "pogoda"
  ]
  node [
    id 264
    label "zegar"
  ]
  node [
    id 265
    label "trawi&#263;"
  ]
  node [
    id 266
    label "pochodzenie"
  ]
  node [
    id 267
    label "poprzedza&#263;"
  ]
  node [
    id 268
    label "time_period"
  ]
  node [
    id 269
    label "rachuba_czasu"
  ]
  node [
    id 270
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 271
    label "czasoprzestrze&#324;"
  ]
  node [
    id 272
    label "laba"
  ]
  node [
    id 273
    label "uzyskiwa&#263;"
  ]
  node [
    id 274
    label "impart"
  ]
  node [
    id 275
    label "proceed"
  ]
  node [
    id 276
    label "blend"
  ]
  node [
    id 277
    label "give"
  ]
  node [
    id 278
    label "ograniczenie"
  ]
  node [
    id 279
    label "wydostawa&#263;_si&#281;"
  ]
  node [
    id 280
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 281
    label "za&#322;atwi&#263;"
  ]
  node [
    id 282
    label "schodzi&#263;"
  ]
  node [
    id 283
    label "gra&#263;"
  ]
  node [
    id 284
    label "osi&#261;ga&#263;"
  ]
  node [
    id 285
    label "okazywa&#263;_si&#281;"
  ]
  node [
    id 286
    label "seclude"
  ]
  node [
    id 287
    label "strona_&#347;wiata"
  ]
  node [
    id 288
    label "przedk&#322;ada&#263;"
  ]
  node [
    id 289
    label "przedstawia&#263;"
  ]
  node [
    id 290
    label "appear"
  ]
  node [
    id 291
    label "ko&#324;czy&#263;"
  ]
  node [
    id 292
    label "wypada&#263;"
  ]
  node [
    id 293
    label "ukazywa&#263;_si&#281;_drukiem"
  ]
  node [
    id 294
    label "uwalnia&#263;_si&#281;"
  ]
  node [
    id 295
    label "wygl&#261;da&#263;"
  ]
  node [
    id 296
    label "opuszcza&#263;"
  ]
  node [
    id 297
    label "wystarcza&#263;"
  ]
  node [
    id 298
    label "wyrusza&#263;"
  ]
  node [
    id 299
    label "perform"
  ]
  node [
    id 300
    label "heighten"
  ]
  node [
    id 301
    label "strzelba"
  ]
  node [
    id 302
    label "wielok&#261;t"
  ]
  node [
    id 303
    label "&#347;ciana"
  ]
  node [
    id 304
    label "odcinek"
  ]
  node [
    id 305
    label "tu&#322;&#243;w"
  ]
  node [
    id 306
    label "bro&#324;_my&#347;liwska"
  ]
  node [
    id 307
    label "lufa"
  ]
  node [
    id 308
    label "strona"
  ]
  node [
    id 309
    label "nadmiernie"
  ]
  node [
    id 310
    label "passion"
  ]
  node [
    id 311
    label "zapalno&#347;&#263;"
  ]
  node [
    id 312
    label "podekscytowanie"
  ]
  node [
    id 313
    label "typowy"
  ]
  node [
    id 314
    label "filozoficznie"
  ]
  node [
    id 315
    label "my&#347;licielski"
  ]
  node [
    id 316
    label "ease"
  ]
  node [
    id 317
    label "robi&#263;"
  ]
  node [
    id 318
    label "powodowa&#263;"
  ]
  node [
    id 319
    label "&#322;atwi&#263;"
  ]
  node [
    id 320
    label "opis"
  ]
  node [
    id 321
    label "analysis"
  ]
  node [
    id 322
    label "reakcja_chemiczna"
  ]
  node [
    id 323
    label "dissection"
  ]
  node [
    id 324
    label "badanie"
  ]
  node [
    id 325
    label "metoda"
  ]
  node [
    id 326
    label "trudno&#347;&#263;"
  ]
  node [
    id 327
    label "odmienno&#347;&#263;"
  ]
  node [
    id 328
    label "odwrotny"
  ]
  node [
    id 329
    label "obstruction"
  ]
  node [
    id 330
    label "reverse"
  ]
  node [
    id 331
    label "wyszukany"
  ]
  node [
    id 332
    label "pi&#281;kny"
  ]
  node [
    id 333
    label "luksusowy"
  ]
  node [
    id 334
    label "&#322;adny"
  ]
  node [
    id 335
    label "kulturalny"
  ]
  node [
    id 336
    label "elegancko"
  ]
  node [
    id 337
    label "galantyna"
  ]
  node [
    id 338
    label "przejrzysty"
  ]
  node [
    id 339
    label "grzeczny"
  ]
  node [
    id 340
    label "fajny"
  ]
  node [
    id 341
    label "gustowny"
  ]
  node [
    id 342
    label "zgrabny"
  ]
  node [
    id 343
    label "akuratny"
  ]
  node [
    id 344
    label "typ"
  ]
  node [
    id 345
    label "rule"
  ]
  node [
    id 346
    label "projekt"
  ]
  node [
    id 347
    label "zapis"
  ]
  node [
    id 348
    label "motyw"
  ]
  node [
    id 349
    label "ruch"
  ]
  node [
    id 350
    label "figure"
  ]
  node [
    id 351
    label "dekal"
  ]
  node [
    id 352
    label "ideal"
  ]
  node [
    id 353
    label "mildew"
  ]
  node [
    id 354
    label "spos&#243;b"
  ]
  node [
    id 355
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 356
    label "matematycznie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 21
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 38
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 10
    target 105
  ]
  edge [
    source 10
    target 106
  ]
  edge [
    source 10
    target 107
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 10
    target 110
  ]
  edge [
    source 10
    target 111
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 10
    target 121
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 10
    target 125
  ]
  edge [
    source 10
    target 126
  ]
  edge [
    source 10
    target 127
  ]
  edge [
    source 10
    target 128
  ]
  edge [
    source 10
    target 129
  ]
  edge [
    source 10
    target 130
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 131
  ]
  edge [
    source 11
    target 132
  ]
  edge [
    source 11
    target 133
  ]
  edge [
    source 11
    target 134
  ]
  edge [
    source 11
    target 135
  ]
  edge [
    source 11
    target 136
  ]
  edge [
    source 11
    target 137
  ]
  edge [
    source 11
    target 138
  ]
  edge [
    source 11
    target 139
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 140
  ]
  edge [
    source 13
    target 141
  ]
  edge [
    source 13
    target 142
  ]
  edge [
    source 13
    target 143
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 144
  ]
  edge [
    source 14
    target 145
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 146
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 58
  ]
  edge [
    source 17
    target 147
  ]
  edge [
    source 17
    target 148
  ]
  edge [
    source 17
    target 149
  ]
  edge [
    source 17
    target 150
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 151
  ]
  edge [
    source 20
    target 152
  ]
  edge [
    source 20
    target 153
  ]
  edge [
    source 20
    target 154
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 155
  ]
  edge [
    source 21
    target 156
  ]
  edge [
    source 21
    target 157
  ]
  edge [
    source 21
    target 158
  ]
  edge [
    source 21
    target 159
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 160
  ]
  edge [
    source 24
    target 161
  ]
  edge [
    source 24
    target 162
  ]
  edge [
    source 24
    target 163
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 164
  ]
  edge [
    source 25
    target 165
  ]
  edge [
    source 25
    target 166
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 167
  ]
  edge [
    source 26
    target 168
  ]
  edge [
    source 26
    target 169
  ]
  edge [
    source 26
    target 170
  ]
  edge [
    source 26
    target 171
  ]
  edge [
    source 26
    target 172
  ]
  edge [
    source 26
    target 173
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 174
  ]
  edge [
    source 27
    target 175
  ]
  edge [
    source 27
    target 176
  ]
  edge [
    source 27
    target 177
  ]
  edge [
    source 27
    target 178
  ]
  edge [
    source 27
    target 179
  ]
  edge [
    source 27
    target 180
  ]
  edge [
    source 27
    target 181
  ]
  edge [
    source 27
    target 182
  ]
  edge [
    source 27
    target 183
  ]
  edge [
    source 27
    target 184
  ]
  edge [
    source 27
    target 185
  ]
  edge [
    source 27
    target 186
  ]
  edge [
    source 27
    target 187
  ]
  edge [
    source 27
    target 188
  ]
  edge [
    source 27
    target 189
  ]
  edge [
    source 27
    target 190
  ]
  edge [
    source 27
    target 191
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 33
  ]
  edge [
    source 28
    target 30
  ]
  edge [
    source 28
    target 192
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 34
  ]
  edge [
    source 30
    target 193
  ]
  edge [
    source 30
    target 194
  ]
  edge [
    source 30
    target 195
  ]
  edge [
    source 30
    target 196
  ]
  edge [
    source 30
    target 197
  ]
  edge [
    source 30
    target 198
  ]
  edge [
    source 30
    target 199
  ]
  edge [
    source 30
    target 200
  ]
  edge [
    source 30
    target 201
  ]
  edge [
    source 30
    target 202
  ]
  edge [
    source 30
    target 203
  ]
  edge [
    source 30
    target 204
  ]
  edge [
    source 30
    target 205
  ]
  edge [
    source 30
    target 206
  ]
  edge [
    source 30
    target 207
  ]
  edge [
    source 30
    target 208
  ]
  edge [
    source 30
    target 209
  ]
  edge [
    source 30
    target 210
  ]
  edge [
    source 30
    target 211
  ]
  edge [
    source 30
    target 212
  ]
  edge [
    source 30
    target 213
  ]
  edge [
    source 30
    target 214
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 215
  ]
  edge [
    source 31
    target 216
  ]
  edge [
    source 31
    target 217
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 218
  ]
  edge [
    source 32
    target 219
  ]
  edge [
    source 33
    target 220
  ]
  edge [
    source 33
    target 221
  ]
  edge [
    source 33
    target 222
  ]
  edge [
    source 33
    target 223
  ]
  edge [
    source 33
    target 224
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 225
  ]
  edge [
    source 34
    target 226
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 43
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 227
  ]
  edge [
    source 36
    target 228
  ]
  edge [
    source 36
    target 229
  ]
  edge [
    source 36
    target 230
  ]
  edge [
    source 36
    target 231
  ]
  edge [
    source 36
    target 232
  ]
  edge [
    source 36
    target 233
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 234
  ]
  edge [
    source 37
    target 235
  ]
  edge [
    source 37
    target 44
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 236
  ]
  edge [
    source 38
    target 237
  ]
  edge [
    source 38
    target 238
  ]
  edge [
    source 38
    target 239
  ]
  edge [
    source 38
    target 240
  ]
  edge [
    source 38
    target 241
  ]
  edge [
    source 38
    target 242
  ]
  edge [
    source 38
    target 243
  ]
  edge [
    source 38
    target 244
  ]
  edge [
    source 38
    target 245
  ]
  edge [
    source 38
    target 246
  ]
  edge [
    source 38
    target 247
  ]
  edge [
    source 38
    target 248
  ]
  edge [
    source 38
    target 249
  ]
  edge [
    source 38
    target 250
  ]
  edge [
    source 38
    target 251
  ]
  edge [
    source 38
    target 252
  ]
  edge [
    source 38
    target 253
  ]
  edge [
    source 38
    target 254
  ]
  edge [
    source 38
    target 255
  ]
  edge [
    source 38
    target 256
  ]
  edge [
    source 38
    target 257
  ]
  edge [
    source 38
    target 258
  ]
  edge [
    source 38
    target 259
  ]
  edge [
    source 38
    target 260
  ]
  edge [
    source 38
    target 261
  ]
  edge [
    source 38
    target 262
  ]
  edge [
    source 38
    target 263
  ]
  edge [
    source 38
    target 264
  ]
  edge [
    source 38
    target 265
  ]
  edge [
    source 38
    target 266
  ]
  edge [
    source 38
    target 267
  ]
  edge [
    source 38
    target 268
  ]
  edge [
    source 38
    target 269
  ]
  edge [
    source 38
    target 270
  ]
  edge [
    source 38
    target 271
  ]
  edge [
    source 38
    target 272
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 273
  ]
  edge [
    source 39
    target 274
  ]
  edge [
    source 39
    target 275
  ]
  edge [
    source 39
    target 276
  ]
  edge [
    source 39
    target 277
  ]
  edge [
    source 39
    target 278
  ]
  edge [
    source 39
    target 279
  ]
  edge [
    source 39
    target 280
  ]
  edge [
    source 39
    target 281
  ]
  edge [
    source 39
    target 282
  ]
  edge [
    source 39
    target 283
  ]
  edge [
    source 39
    target 284
  ]
  edge [
    source 39
    target 285
  ]
  edge [
    source 39
    target 286
  ]
  edge [
    source 39
    target 287
  ]
  edge [
    source 39
    target 288
  ]
  edge [
    source 39
    target 289
  ]
  edge [
    source 39
    target 290
  ]
  edge [
    source 39
    target 73
  ]
  edge [
    source 39
    target 291
  ]
  edge [
    source 39
    target 292
  ]
  edge [
    source 39
    target 257
  ]
  edge [
    source 39
    target 293
  ]
  edge [
    source 39
    target 294
  ]
  edge [
    source 39
    target 295
  ]
  edge [
    source 39
    target 296
  ]
  edge [
    source 39
    target 297
  ]
  edge [
    source 39
    target 298
  ]
  edge [
    source 39
    target 299
  ]
  edge [
    source 39
    target 300
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 301
  ]
  edge [
    source 40
    target 302
  ]
  edge [
    source 40
    target 303
  ]
  edge [
    source 40
    target 181
  ]
  edge [
    source 40
    target 304
  ]
  edge [
    source 40
    target 305
  ]
  edge [
    source 40
    target 306
  ]
  edge [
    source 40
    target 307
  ]
  edge [
    source 40
    target 308
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 309
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 310
  ]
  edge [
    source 42
    target 105
  ]
  edge [
    source 42
    target 311
  ]
  edge [
    source 42
    target 312
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 313
  ]
  edge [
    source 43
    target 314
  ]
  edge [
    source 43
    target 315
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 316
  ]
  edge [
    source 44
    target 317
  ]
  edge [
    source 44
    target 318
  ]
  edge [
    source 44
    target 319
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 320
  ]
  edge [
    source 45
    target 321
  ]
  edge [
    source 45
    target 322
  ]
  edge [
    source 45
    target 323
  ]
  edge [
    source 45
    target 324
  ]
  edge [
    source 45
    target 325
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 326
  ]
  edge [
    source 46
    target 327
  ]
  edge [
    source 46
    target 328
  ]
  edge [
    source 46
    target 329
  ]
  edge [
    source 46
    target 330
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 331
  ]
  edge [
    source 48
    target 332
  ]
  edge [
    source 48
    target 333
  ]
  edge [
    source 48
    target 334
  ]
  edge [
    source 48
    target 335
  ]
  edge [
    source 48
    target 336
  ]
  edge [
    source 48
    target 337
  ]
  edge [
    source 48
    target 338
  ]
  edge [
    source 48
    target 339
  ]
  edge [
    source 48
    target 340
  ]
  edge [
    source 48
    target 341
  ]
  edge [
    source 48
    target 342
  ]
  edge [
    source 48
    target 343
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 344
  ]
  edge [
    source 49
    target 58
  ]
  edge [
    source 49
    target 345
  ]
  edge [
    source 49
    target 346
  ]
  edge [
    source 49
    target 347
  ]
  edge [
    source 49
    target 348
  ]
  edge [
    source 49
    target 349
  ]
  edge [
    source 49
    target 350
  ]
  edge [
    source 49
    target 351
  ]
  edge [
    source 49
    target 352
  ]
  edge [
    source 49
    target 353
  ]
  edge [
    source 49
    target 354
  ]
  edge [
    source 49
    target 355
  ]
  edge [
    source 50
    target 228
  ]
  edge [
    source 50
    target 356
  ]
]
