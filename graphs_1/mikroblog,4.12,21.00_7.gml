graph [
  maxDegree 22
  minDegree 1
  meanDegree 1.92
  density 0.08
  graphCliqueNumber 2
  node [
    id 0
    label "szkola"
    origin "text"
  ]
  node [
    id 1
    label "sekretariat"
    origin "text"
  ]
  node [
    id 2
    label "dziecko"
    origin "text"
  ]
  node [
    id 3
    label "pracbaza"
    origin "text"
  ]
  node [
    id 4
    label "biuro"
  ]
  node [
    id 5
    label "cz&#322;owiek"
  ]
  node [
    id 6
    label "potomstwo"
  ]
  node [
    id 7
    label "organizm"
  ]
  node [
    id 8
    label "sraluch"
  ]
  node [
    id 9
    label "utulanie"
  ]
  node [
    id 10
    label "pediatra"
  ]
  node [
    id 11
    label "dzieciarnia"
  ]
  node [
    id 12
    label "m&#322;odziak"
  ]
  node [
    id 13
    label "dzieciak"
  ]
  node [
    id 14
    label "utula&#263;"
  ]
  node [
    id 15
    label "potomek"
  ]
  node [
    id 16
    label "entliczek-pentliczek"
  ]
  node [
    id 17
    label "pedofil"
  ]
  node [
    id 18
    label "m&#322;odzik"
  ]
  node [
    id 19
    label "cz&#322;owieczek"
  ]
  node [
    id 20
    label "zwierz&#281;"
  ]
  node [
    id 21
    label "niepe&#322;noletni"
  ]
  node [
    id 22
    label "fledgling"
  ]
  node [
    id 23
    label "utuli&#263;"
  ]
  node [
    id 24
    label "utulenie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
]
