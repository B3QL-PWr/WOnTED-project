graph [
  maxDegree 30
  minDegree 1
  meanDegree 2.0141843971631204
  density 0.014387031408308004
  graphCliqueNumber 3
  node [
    id 0
    label "wroc&#322;awski"
    origin "text"
  ]
  node [
    id 1
    label "zesp&#243;&#322;"
    origin "text"
  ]
  node [
    id 2
    label "digit"
    origin "text"
  ]
  node [
    id 3
    label "all"
    origin "text"
  ]
  node [
    id 4
    label "love"
    origin "text"
  ]
  node [
    id 5
    label "wyda&#263;"
    origin "text"
  ]
  node [
    id 6
    label "p&#322;yta"
    origin "text"
  ]
  node [
    id 7
    label "dobry"
    origin "text"
  ]
  node [
    id 8
    label "muzyka"
    origin "text"
  ]
  node [
    id 9
    label "kilka"
    origin "text"
  ]
  node [
    id 10
    label "kawa&#322;ek"
    origin "text"
  ]
  node [
    id 11
    label "wideo"
    origin "text"
  ]
  node [
    id 12
    label "licencja"
    origin "text"
  ]
  node [
    id 13
    label "creative"
    origin "text"
  ]
  node [
    id 14
    label "commons"
    origin "text"
  ]
  node [
    id 15
    label "po_wroc&#322;awsku"
  ]
  node [
    id 16
    label "dolno&#347;l&#261;ski"
  ]
  node [
    id 17
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 18
    label "whole"
  ]
  node [
    id 19
    label "odm&#322;adza&#263;"
  ]
  node [
    id 20
    label "zabudowania"
  ]
  node [
    id 21
    label "odm&#322;odzenie"
  ]
  node [
    id 22
    label "zespolik"
  ]
  node [
    id 23
    label "skupienie"
  ]
  node [
    id 24
    label "schorzenie"
  ]
  node [
    id 25
    label "grupa"
  ]
  node [
    id 26
    label "Depeche_Mode"
  ]
  node [
    id 27
    label "Mazowsze"
  ]
  node [
    id 28
    label "ro&#347;lina"
  ]
  node [
    id 29
    label "zbi&#243;r"
  ]
  node [
    id 30
    label "The_Beatles"
  ]
  node [
    id 31
    label "group"
  ]
  node [
    id 32
    label "&#346;wietliki"
  ]
  node [
    id 33
    label "odm&#322;adzanie"
  ]
  node [
    id 34
    label "batch"
  ]
  node [
    id 35
    label "impart"
  ]
  node [
    id 36
    label "panna_na_wydaniu"
  ]
  node [
    id 37
    label "translate"
  ]
  node [
    id 38
    label "give"
  ]
  node [
    id 39
    label "pieni&#261;dze"
  ]
  node [
    id 40
    label "supply"
  ]
  node [
    id 41
    label "wprowadzi&#263;"
  ]
  node [
    id 42
    label "da&#263;"
  ]
  node [
    id 43
    label "zapach"
  ]
  node [
    id 44
    label "wydawnictwo"
  ]
  node [
    id 45
    label "powierzy&#263;"
  ]
  node [
    id 46
    label "produkcja"
  ]
  node [
    id 47
    label "poda&#263;"
  ]
  node [
    id 48
    label "skojarzy&#263;"
  ]
  node [
    id 49
    label "dress"
  ]
  node [
    id 50
    label "plon"
  ]
  node [
    id 51
    label "ujawni&#263;"
  ]
  node [
    id 52
    label "reszta"
  ]
  node [
    id 53
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 54
    label "zadenuncjowa&#263;"
  ]
  node [
    id 55
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 56
    label "zrobi&#263;"
  ]
  node [
    id 57
    label "tajemnica"
  ]
  node [
    id 58
    label "wiano"
  ]
  node [
    id 59
    label "gor&#261;cy_pieni&#261;dz"
  ]
  node [
    id 60
    label "wytworzy&#263;"
  ]
  node [
    id 61
    label "d&#378;wi&#281;k"
  ]
  node [
    id 62
    label "picture"
  ]
  node [
    id 63
    label "sheet"
  ]
  node [
    id 64
    label "miejsce"
  ]
  node [
    id 65
    label "no&#347;nik_danych"
  ]
  node [
    id 66
    label "przedmiot"
  ]
  node [
    id 67
    label "plate"
  ]
  node [
    id 68
    label "AGD"
  ]
  node [
    id 69
    label "nagranie"
  ]
  node [
    id 70
    label "phonograph_record"
  ]
  node [
    id 71
    label "p&#322;ytoteka"
  ]
  node [
    id 72
    label "kuchnia"
  ]
  node [
    id 73
    label "dysk"
  ]
  node [
    id 74
    label "nagranie_d&#378;wi&#281;kowe"
  ]
  node [
    id 75
    label "pomy&#347;lny"
  ]
  node [
    id 76
    label "skuteczny"
  ]
  node [
    id 77
    label "moralny"
  ]
  node [
    id 78
    label "korzystny"
  ]
  node [
    id 79
    label "odpowiedni"
  ]
  node [
    id 80
    label "zwrot"
  ]
  node [
    id 81
    label "dobrze"
  ]
  node [
    id 82
    label "pozytywny"
  ]
  node [
    id 83
    label "grzeczny"
  ]
  node [
    id 84
    label "powitanie"
  ]
  node [
    id 85
    label "mi&#322;y"
  ]
  node [
    id 86
    label "dobroczynny"
  ]
  node [
    id 87
    label "pos&#322;uszny"
  ]
  node [
    id 88
    label "ca&#322;y"
  ]
  node [
    id 89
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 90
    label "czw&#243;rka"
  ]
  node [
    id 91
    label "spokojny"
  ]
  node [
    id 92
    label "&#347;mieszny"
  ]
  node [
    id 93
    label "drogi"
  ]
  node [
    id 94
    label "wys&#322;ucha&#263;"
  ]
  node [
    id 95
    label "kontrapunkt"
  ]
  node [
    id 96
    label "set"
  ]
  node [
    id 97
    label "sztuka"
  ]
  node [
    id 98
    label "muza"
  ]
  node [
    id 99
    label "wykonywa&#263;"
  ]
  node [
    id 100
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 101
    label "wytw&#243;r"
  ]
  node [
    id 102
    label "notacja_muzyczna"
  ]
  node [
    id 103
    label "britpop"
  ]
  node [
    id 104
    label "instrumentalistyka"
  ]
  node [
    id 105
    label "zjawisko"
  ]
  node [
    id 106
    label "szko&#322;a"
  ]
  node [
    id 107
    label "komponowanie"
  ]
  node [
    id 108
    label "wys&#322;uchanie"
  ]
  node [
    id 109
    label "beatbox"
  ]
  node [
    id 110
    label "wokalistyka"
  ]
  node [
    id 111
    label "nauka"
  ]
  node [
    id 112
    label "pasa&#380;"
  ]
  node [
    id 113
    label "wykonywanie"
  ]
  node [
    id 114
    label "harmonia"
  ]
  node [
    id 115
    label "komponowa&#263;"
  ]
  node [
    id 116
    label "kapela"
  ]
  node [
    id 117
    label "&#347;ledziowate"
  ]
  node [
    id 118
    label "ryba"
  ]
  node [
    id 119
    label "podp&#322;ywa&#263;"
  ]
  node [
    id 120
    label "podp&#322;ywanie"
  ]
  node [
    id 121
    label "plot"
  ]
  node [
    id 122
    label "podp&#322;yn&#261;&#263;"
  ]
  node [
    id 123
    label "piece"
  ]
  node [
    id 124
    label "kawa&#322;"
  ]
  node [
    id 125
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 126
    label "utw&#243;r"
  ]
  node [
    id 127
    label "podp&#322;yni&#281;cie"
  ]
  node [
    id 128
    label "odtwarzacz"
  ]
  node [
    id 129
    label "g&#322;owica_elektromagnetyczna"
  ]
  node [
    id 130
    label "film"
  ]
  node [
    id 131
    label "technika"
  ]
  node [
    id 132
    label "wideokaseta"
  ]
  node [
    id 133
    label "prawo"
  ]
  node [
    id 134
    label "licencjonowa&#263;"
  ]
  node [
    id 135
    label "pozwolenie"
  ]
  node [
    id 136
    label "hodowla"
  ]
  node [
    id 137
    label "rasowy"
  ]
  node [
    id 138
    label "license"
  ]
  node [
    id 139
    label "zezwolenie"
  ]
  node [
    id 140
    label "za&#347;wiadczenie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 117
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 10
    target 121
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 10
    target 125
  ]
  edge [
    source 10
    target 126
  ]
  edge [
    source 10
    target 127
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 128
  ]
  edge [
    source 11
    target 129
  ]
  edge [
    source 11
    target 130
  ]
  edge [
    source 11
    target 131
  ]
  edge [
    source 11
    target 132
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 133
  ]
  edge [
    source 12
    target 134
  ]
  edge [
    source 12
    target 135
  ]
  edge [
    source 12
    target 136
  ]
  edge [
    source 12
    target 137
  ]
  edge [
    source 12
    target 138
  ]
  edge [
    source 12
    target 139
  ]
  edge [
    source 12
    target 140
  ]
  edge [
    source 13
    target 14
  ]
]
