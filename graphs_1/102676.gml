graph [
  maxDegree 21
  minDegree 1
  meanDegree 1.9393939393939394
  density 0.06060606060606061
  graphCliqueNumber 2
  node [
    id 0
    label "filler"
    origin "text"
  ]
  node [
    id 1
    label "dzienia"
    origin "text"
  ]
  node [
    id 2
    label "dobry"
    origin "text"
  ]
  node [
    id 3
    label "xxx"
    origin "text"
  ]
  node [
    id 4
    label "s&#322;ucha&#263;"
    origin "text"
  ]
  node [
    id 5
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 6
    label "drzewo_owocowe"
  ]
  node [
    id 7
    label "pomy&#347;lny"
  ]
  node [
    id 8
    label "skuteczny"
  ]
  node [
    id 9
    label "moralny"
  ]
  node [
    id 10
    label "korzystny"
  ]
  node [
    id 11
    label "odpowiedni"
  ]
  node [
    id 12
    label "zwrot"
  ]
  node [
    id 13
    label "dobrze"
  ]
  node [
    id 14
    label "pozytywny"
  ]
  node [
    id 15
    label "grzeczny"
  ]
  node [
    id 16
    label "powitanie"
  ]
  node [
    id 17
    label "mi&#322;y"
  ]
  node [
    id 18
    label "dobroczynny"
  ]
  node [
    id 19
    label "pos&#322;uszny"
  ]
  node [
    id 20
    label "ca&#322;y"
  ]
  node [
    id 21
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 22
    label "czw&#243;rka"
  ]
  node [
    id 23
    label "spokojny"
  ]
  node [
    id 24
    label "&#347;mieszny"
  ]
  node [
    id 25
    label "drogi"
  ]
  node [
    id 26
    label "continue"
  ]
  node [
    id 27
    label "podporz&#261;dkowywa&#263;_si&#281;"
  ]
  node [
    id 28
    label "lubi&#263;"
  ]
  node [
    id 29
    label "odbiera&#263;"
  ]
  node [
    id 30
    label "wybiera&#263;"
  ]
  node [
    id 31
    label "odtwarza&#263;"
  ]
  node [
    id 32
    label "nat&#281;&#380;a&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
]
