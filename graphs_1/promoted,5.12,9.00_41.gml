graph [
  maxDegree 30
  minDegree 1
  meanDegree 1.9753086419753085
  density 0.024691358024691357
  graphCliqueNumber 2
  node [
    id 0
    label "konrad"
    origin "text"
  ]
  node [
    id 1
    label "smuniewski"
    origin "text"
  ]
  node [
    id 2
    label "opisa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "incydent"
    origin "text"
  ]
  node [
    id 4
    label "z&#322;oty"
    origin "text"
  ]
  node [
    id 5
    label "taras"
    origin "text"
  ]
  node [
    id 6
    label "udzia&#322;"
    origin "text"
  ]
  node [
    id 7
    label "grupa"
    origin "text"
  ]
  node [
    id 8
    label "hindus"
    origin "text"
  ]
  node [
    id 9
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 10
    label "g&#322;o&#347;no"
    origin "text"
  ]
  node [
    id 11
    label "krzycze&#263;"
    origin "text"
  ]
  node [
    id 12
    label "zinterpretowa&#263;"
  ]
  node [
    id 13
    label "relate"
  ]
  node [
    id 14
    label "zapozna&#263;"
  ]
  node [
    id 15
    label "delineate"
  ]
  node [
    id 16
    label "event"
  ]
  node [
    id 17
    label "wydarzenie"
  ]
  node [
    id 18
    label "ubezpieczenie_wypadkowe"
  ]
  node [
    id 19
    label "szlachetny"
  ]
  node [
    id 20
    label "metaliczny"
  ]
  node [
    id 21
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 22
    label "z&#322;oci&#347;cie"
  ]
  node [
    id 23
    label "grosz"
  ]
  node [
    id 24
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 25
    label "utytu&#322;owany"
  ]
  node [
    id 26
    label "poz&#322;ocenie"
  ]
  node [
    id 27
    label "Polska"
  ]
  node [
    id 28
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 29
    label "wspania&#322;y"
  ]
  node [
    id 30
    label "doskona&#322;y"
  ]
  node [
    id 31
    label "kochany"
  ]
  node [
    id 32
    label "jednostka_monetarna"
  ]
  node [
    id 33
    label "z&#322;ocenie"
  ]
  node [
    id 34
    label "formacja_geologiczna"
  ]
  node [
    id 35
    label "wyst&#281;p"
  ]
  node [
    id 36
    label "balkon"
  ]
  node [
    id 37
    label "p&#322;aszczyzna"
  ]
  node [
    id 38
    label "obecno&#347;&#263;"
  ]
  node [
    id 39
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 40
    label "kwota"
  ]
  node [
    id 41
    label "ilo&#347;&#263;"
  ]
  node [
    id 42
    label "odm&#322;adza&#263;"
  ]
  node [
    id 43
    label "asymilowa&#263;"
  ]
  node [
    id 44
    label "cz&#261;steczka"
  ]
  node [
    id 45
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 46
    label "egzemplarz"
  ]
  node [
    id 47
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 48
    label "harcerze_starsi"
  ]
  node [
    id 49
    label "liga"
  ]
  node [
    id 50
    label "Terranie"
  ]
  node [
    id 51
    label "&#346;wietliki"
  ]
  node [
    id 52
    label "pakiet_klimatyczny"
  ]
  node [
    id 53
    label "oddzia&#322;"
  ]
  node [
    id 54
    label "stage_set"
  ]
  node [
    id 55
    label "Entuzjastki"
  ]
  node [
    id 56
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 57
    label "odm&#322;odzenie"
  ]
  node [
    id 58
    label "type"
  ]
  node [
    id 59
    label "category"
  ]
  node [
    id 60
    label "asymilowanie"
  ]
  node [
    id 61
    label "specgrupa"
  ]
  node [
    id 62
    label "odm&#322;adzanie"
  ]
  node [
    id 63
    label "gromada"
  ]
  node [
    id 64
    label "Eurogrupa"
  ]
  node [
    id 65
    label "jednostka_systematyczna"
  ]
  node [
    id 66
    label "kompozycja"
  ]
  node [
    id 67
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 68
    label "zbi&#243;r"
  ]
  node [
    id 69
    label "loudly"
  ]
  node [
    id 70
    label "jawnie"
  ]
  node [
    id 71
    label "szczerze"
  ]
  node [
    id 72
    label "g&#322;o&#347;ny"
  ]
  node [
    id 73
    label "rant"
  ]
  node [
    id 74
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 75
    label "p&#322;aka&#263;"
  ]
  node [
    id 76
    label "wydobywa&#263;"
  ]
  node [
    id 77
    label "gl&#281;dzi&#263;"
  ]
  node [
    id 78
    label "Konrad"
  ]
  node [
    id 79
    label "Smuniewski"
  ]
  node [
    id 80
    label "Taras"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 34
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 69
  ]
  edge [
    source 10
    target 70
  ]
  edge [
    source 10
    target 71
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 11
    target 73
  ]
  edge [
    source 11
    target 74
  ]
  edge [
    source 11
    target 75
  ]
  edge [
    source 11
    target 76
  ]
  edge [
    source 11
    target 77
  ]
  edge [
    source 78
    target 79
  ]
]
