graph [
  maxDegree 33
  minDegree 1
  meanDegree 2
  density 0.023529411764705882
  graphCliqueNumber 2
  node [
    id 0
    label "kto"
    origin "text"
  ]
  node [
    id 1
    label "uwa&#380;a&#263;"
    origin "text"
  ]
  node [
    id 2
    label "subtelny"
    origin "text"
  ]
  node [
    id 3
    label "paseczek"
    origin "text"
  ]
  node [
    id 4
    label "wst&#261;&#380;eczka"
    origin "text"
  ]
  node [
    id 5
    label "kokardka"
    origin "text"
  ]
  node [
    id 6
    label "rajstopki"
    origin "text"
  ]
  node [
    id 7
    label "mega"
    origin "text"
  ]
  node [
    id 8
    label "seksowny"
    origin "text"
  ]
  node [
    id 9
    label "dawa&#263;"
    origin "text"
  ]
  node [
    id 10
    label "plus"
    origin "text"
  ]
  node [
    id 11
    label "continue"
  ]
  node [
    id 12
    label "str&#243;&#380;y&#263;"
  ]
  node [
    id 13
    label "consider"
  ]
  node [
    id 14
    label "my&#347;le&#263;"
  ]
  node [
    id 15
    label "pilnowa&#263;"
  ]
  node [
    id 16
    label "robi&#263;"
  ]
  node [
    id 17
    label "uznawa&#263;"
  ]
  node [
    id 18
    label "obserwowa&#263;"
  ]
  node [
    id 19
    label "skupia&#263;_si&#281;"
  ]
  node [
    id 20
    label "mie&#263;_na_baczenie"
  ]
  node [
    id 21
    label "deliver"
  ]
  node [
    id 22
    label "elegancki"
  ]
  node [
    id 23
    label "filigranowo"
  ]
  node [
    id 24
    label "drobny"
  ]
  node [
    id 25
    label "wnikliwy"
  ]
  node [
    id 26
    label "wydelikacanie"
  ]
  node [
    id 27
    label "k&#322;opotliwy"
  ]
  node [
    id 28
    label "zdelikatnienie"
  ]
  node [
    id 29
    label "subtelnie"
  ]
  node [
    id 30
    label "wydelikacenie"
  ]
  node [
    id 31
    label "delikatnie"
  ]
  node [
    id 32
    label "delikatnienie"
  ]
  node [
    id 33
    label "wra&#380;liwy"
  ]
  node [
    id 34
    label "dra&#380;liwy"
  ]
  node [
    id 35
    label "delikatny"
  ]
  node [
    id 36
    label "w&#281;ze&#322;"
  ]
  node [
    id 37
    label "cockade"
  ]
  node [
    id 38
    label "makaron"
  ]
  node [
    id 39
    label "atrakcyjny"
  ]
  node [
    id 40
    label "powabny"
  ]
  node [
    id 41
    label "podniecaj&#261;cy"
  ]
  node [
    id 42
    label "seksownie"
  ]
  node [
    id 43
    label "render"
  ]
  node [
    id 44
    label "hold"
  ]
  node [
    id 45
    label "surrender"
  ]
  node [
    id 46
    label "traktowa&#263;"
  ]
  node [
    id 47
    label "dostarcza&#263;"
  ]
  node [
    id 48
    label "tender"
  ]
  node [
    id 49
    label "train"
  ]
  node [
    id 50
    label "give"
  ]
  node [
    id 51
    label "umieszcza&#263;"
  ]
  node [
    id 52
    label "nalewa&#263;"
  ]
  node [
    id 53
    label "przeznacza&#263;"
  ]
  node [
    id 54
    label "p&#322;aci&#263;"
  ]
  node [
    id 55
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 56
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 57
    label "powierza&#263;"
  ]
  node [
    id 58
    label "hold_out"
  ]
  node [
    id 59
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 60
    label "wyrzeka&#263;_si&#281;"
  ]
  node [
    id 61
    label "mie&#263;_miejsce"
  ]
  node [
    id 62
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 63
    label "t&#322;uc"
  ]
  node [
    id 64
    label "wpiernicza&#263;"
  ]
  node [
    id 65
    label "przekazywa&#263;"
  ]
  node [
    id 66
    label "puszcza&#263;_si&#281;"
  ]
  node [
    id 67
    label "zezwala&#263;"
  ]
  node [
    id 68
    label "rap"
  ]
  node [
    id 69
    label "obiecywa&#263;"
  ]
  node [
    id 70
    label "&#322;adowa&#263;"
  ]
  node [
    id 71
    label "odst&#281;powa&#263;"
  ]
  node [
    id 72
    label "exsert"
  ]
  node [
    id 73
    label "warto&#347;&#263;"
  ]
  node [
    id 74
    label "wabik"
  ]
  node [
    id 75
    label "rewaluowa&#263;"
  ]
  node [
    id 76
    label "korzy&#347;&#263;"
  ]
  node [
    id 77
    label "dodawanie"
  ]
  node [
    id 78
    label "rewaluowanie"
  ]
  node [
    id 79
    label "stopie&#324;"
  ]
  node [
    id 80
    label "ocena"
  ]
  node [
    id 81
    label "zrewaluowa&#263;"
  ]
  node [
    id 82
    label "liczba"
  ]
  node [
    id 83
    label "znak_matematyczny"
  ]
  node [
    id 84
    label "strona"
  ]
  node [
    id 85
    label "zrewaluowanie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 39
  ]
  edge [
    source 8
    target 40
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 43
  ]
  edge [
    source 9
    target 44
  ]
  edge [
    source 9
    target 45
  ]
  edge [
    source 9
    target 46
  ]
  edge [
    source 9
    target 47
  ]
  edge [
    source 9
    target 48
  ]
  edge [
    source 9
    target 49
  ]
  edge [
    source 9
    target 50
  ]
  edge [
    source 9
    target 51
  ]
  edge [
    source 9
    target 52
  ]
  edge [
    source 9
    target 53
  ]
  edge [
    source 9
    target 54
  ]
  edge [
    source 9
    target 55
  ]
  edge [
    source 9
    target 56
  ]
  edge [
    source 9
    target 57
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 16
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 85
  ]
]
