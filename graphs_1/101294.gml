graph [
  maxDegree 36
  minDegree 1
  meanDegree 5.6455696202531644
  density 0.0723790976955534
  graphCliqueNumber 8
  node [
    id 0
    label "gwardia"
    origin "text"
  ]
  node [
    id 1
    label "imperium"
    origin "text"
  ]
  node [
    id 2
    label "rosyjski"
    origin "text"
  ]
  node [
    id 3
    label "ochrona"
  ]
  node [
    id 4
    label "formacja"
  ]
  node [
    id 5
    label "pa&#324;stwo"
  ]
  node [
    id 6
    label "Imperium_Osma&#324;skie"
  ]
  node [
    id 7
    label "pot&#281;ga"
  ]
  node [
    id 8
    label "po_rosyjsku"
  ]
  node [
    id 9
    label "j&#281;zyk"
  ]
  node [
    id 10
    label "wielkoruski"
  ]
  node [
    id 11
    label "kacapski"
  ]
  node [
    id 12
    label "Russian"
  ]
  node [
    id 13
    label "rusek"
  ]
  node [
    id 14
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 15
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 16
    label "Piotr"
  ]
  node [
    id 17
    label "i"
  ]
  node [
    id 18
    label "wielki"
  ]
  node [
    id 19
    label "korpus"
  ]
  node [
    id 20
    label "petersburski"
  ]
  node [
    id 21
    label "okr&#281;g"
  ]
  node [
    id 22
    label "wojskowy"
  ]
  node [
    id 23
    label "Sankt"
  ]
  node [
    id 24
    label "Petersburg"
  ]
  node [
    id 25
    label "1"
  ]
  node [
    id 26
    label "dywizja"
  ]
  node [
    id 27
    label "piechota"
  ]
  node [
    id 28
    label "brygada"
  ]
  node [
    id 29
    label "lejba"
  ]
  node [
    id 30
    label "gwardyjski"
  ]
  node [
    id 31
    label "Przeobra&#380;e&#324;ski"
  ]
  node [
    id 32
    label "pu&#322;k"
  ]
  node [
    id 33
    label "on"
  ]
  node [
    id 34
    label "wysoko&#347;&#263;"
  ]
  node [
    id 35
    label "Semenowski"
  ]
  node [
    id 36
    label "2"
  ]
  node [
    id 37
    label "Izmai&#322;owski"
  ]
  node [
    id 38
    label "jegier"
  ]
  node [
    id 39
    label "artyleria"
  ]
  node [
    id 40
    label "moskiewski"
  ]
  node [
    id 41
    label "grenadier"
  ]
  node [
    id 42
    label "paw&#322;owski"
  ]
  node [
    id 43
    label "fi&#324;ski"
  ]
  node [
    id 44
    label "strzelec"
  ]
  node [
    id 45
    label "kawaleria"
  ]
  node [
    id 46
    label "szwole&#380;er"
  ]
  node [
    id 47
    label "Maria"
  ]
  node [
    id 48
    label "Fiodorowny"
  ]
  node [
    id 49
    label "konny"
  ]
  node [
    id 50
    label "kirasjer"
  ]
  node [
    id 51
    label "3"
  ]
  node [
    id 52
    label "kozaki"
  ]
  node [
    id 53
    label "atama&#324;ski"
  ]
  node [
    id 54
    label "kombinowa&#263;"
  ]
  node [
    id 55
    label "sotnia"
  ]
  node [
    id 56
    label "uralski"
  ]
  node [
    id 57
    label "orenburski"
  ]
  node [
    id 58
    label "4"
  ]
  node [
    id 59
    label "amurski"
  ]
  node [
    id 60
    label "starszy"
  ]
  node [
    id 61
    label "lansjer"
  ]
  node [
    id 62
    label "Teodorowny"
  ]
  node [
    id 63
    label "dragon"
  ]
  node [
    id 64
    label "huzar"
  ]
  node [
    id 65
    label "samodzielny"
  ]
  node [
    id 66
    label "23"
  ]
  node [
    id 67
    label "ki"
  ]
  node [
    id 68
    label "batalion"
  ]
  node [
    id 69
    label "haubica"
  ]
  node [
    id 70
    label "saper"
  ]
  node [
    id 71
    label "kompania"
  ]
  node [
    id 72
    label "lotniczy"
  ]
  node [
    id 73
    label "armijny"
  ]
  node [
    id 74
    label "warszawski"
  ]
  node [
    id 75
    label "litewski"
  ]
  node [
    id 76
    label "Keksholmski"
  ]
  node [
    id 77
    label "cesarz"
  ]
  node [
    id 78
    label "austriacki"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 18
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 19
    target 66
  ]
  edge [
    source 19
    target 73
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 22
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 74
  ]
  edge [
    source 22
    target 74
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 60
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 27
  ]
  edge [
    source 25
    target 28
  ]
  edge [
    source 25
    target 29
  ]
  edge [
    source 25
    target 30
  ]
  edge [
    source 25
    target 39
  ]
  edge [
    source 25
    target 45
  ]
  edge [
    source 25
    target 55
  ]
  edge [
    source 25
    target 56
  ]
  edge [
    source 25
    target 33
  ]
  edge [
    source 25
    target 34
  ]
  edge [
    source 25
    target 49
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 36
  ]
  edge [
    source 26
    target 45
  ]
  edge [
    source 26
    target 51
  ]
  edge [
    source 27
    target 36
  ]
  edge [
    source 27
    target 51
  ]
  edge [
    source 28
    target 36
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 30
  ]
  edge [
    source 28
    target 39
  ]
  edge [
    source 28
    target 44
  ]
  edge [
    source 28
    target 51
  ]
  edge [
    source 28
    target 49
  ]
  edge [
    source 28
    target 65
  ]
  edge [
    source 28
    target 45
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 31
  ]
  edge [
    source 29
    target 32
  ]
  edge [
    source 29
    target 33
  ]
  edge [
    source 29
    target 34
  ]
  edge [
    source 29
    target 35
  ]
  edge [
    source 29
    target 37
  ]
  edge [
    source 29
    target 38
  ]
  edge [
    source 29
    target 39
  ]
  edge [
    source 29
    target 40
  ]
  edge [
    source 29
    target 41
  ]
  edge [
    source 29
    target 42
  ]
  edge [
    source 29
    target 43
  ]
  edge [
    source 29
    target 36
  ]
  edge [
    source 29
    target 49
  ]
  edge [
    source 29
    target 50
  ]
  edge [
    source 29
    target 47
  ]
  edge [
    source 29
    target 48
  ]
  edge [
    source 29
    target 52
  ]
  edge [
    source 29
    target 53
  ]
  edge [
    source 29
    target 54
  ]
  edge [
    source 29
    target 61
  ]
  edge [
    source 29
    target 62
  ]
  edge [
    source 29
    target 63
  ]
  edge [
    source 29
    target 64
  ]
  edge [
    source 29
    target 68
  ]
  edge [
    source 29
    target 70
  ]
  edge [
    source 29
    target 75
  ]
  edge [
    source 29
    target 76
  ]
  edge [
    source 29
    target 77
  ]
  edge [
    source 29
    target 78
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 32
  ]
  edge [
    source 30
    target 33
  ]
  edge [
    source 30
    target 34
  ]
  edge [
    source 30
    target 35
  ]
  edge [
    source 30
    target 37
  ]
  edge [
    source 30
    target 38
  ]
  edge [
    source 30
    target 39
  ]
  edge [
    source 30
    target 40
  ]
  edge [
    source 30
    target 41
  ]
  edge [
    source 30
    target 42
  ]
  edge [
    source 30
    target 43
  ]
  edge [
    source 30
    target 36
  ]
  edge [
    source 30
    target 49
  ]
  edge [
    source 30
    target 50
  ]
  edge [
    source 30
    target 47
  ]
  edge [
    source 30
    target 48
  ]
  edge [
    source 30
    target 52
  ]
  edge [
    source 30
    target 53
  ]
  edge [
    source 30
    target 54
  ]
  edge [
    source 30
    target 61
  ]
  edge [
    source 30
    target 62
  ]
  edge [
    source 30
    target 63
  ]
  edge [
    source 30
    target 64
  ]
  edge [
    source 30
    target 68
  ]
  edge [
    source 30
    target 69
  ]
  edge [
    source 30
    target 70
  ]
  edge [
    source 30
    target 71
  ]
  edge [
    source 30
    target 72
  ]
  edge [
    source 30
    target 75
  ]
  edge [
    source 30
    target 76
  ]
  edge [
    source 30
    target 77
  ]
  edge [
    source 30
    target 78
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 33
  ]
  edge [
    source 31
    target 34
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 34
  ]
  edge [
    source 32
    target 35
  ]
  edge [
    source 32
    target 37
  ]
  edge [
    source 32
    target 38
  ]
  edge [
    source 32
    target 40
  ]
  edge [
    source 32
    target 41
  ]
  edge [
    source 32
    target 42
  ]
  edge [
    source 32
    target 43
  ]
  edge [
    source 32
    target 46
  ]
  edge [
    source 32
    target 47
  ]
  edge [
    source 32
    target 48
  ]
  edge [
    source 32
    target 49
  ]
  edge [
    source 32
    target 50
  ]
  edge [
    source 32
    target 52
  ]
  edge [
    source 32
    target 53
  ]
  edge [
    source 32
    target 54
  ]
  edge [
    source 32
    target 61
  ]
  edge [
    source 32
    target 62
  ]
  edge [
    source 32
    target 63
  ]
  edge [
    source 32
    target 64
  ]
  edge [
    source 32
    target 75
  ]
  edge [
    source 32
    target 76
  ]
  edge [
    source 32
    target 77
  ]
  edge [
    source 32
    target 78
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 46
  ]
  edge [
    source 33
    target 47
  ]
  edge [
    source 33
    target 48
  ]
  edge [
    source 33
    target 50
  ]
  edge [
    source 33
    target 52
  ]
  edge [
    source 33
    target 53
  ]
  edge [
    source 33
    target 55
  ]
  edge [
    source 33
    target 56
  ]
  edge [
    source 33
    target 61
  ]
  edge [
    source 33
    target 62
  ]
  edge [
    source 33
    target 64
  ]
  edge [
    source 33
    target 76
  ]
  edge [
    source 33
    target 77
  ]
  edge [
    source 33
    target 78
  ]
  edge [
    source 34
    target 46
  ]
  edge [
    source 34
    target 47
  ]
  edge [
    source 34
    target 48
  ]
  edge [
    source 34
    target 50
  ]
  edge [
    source 34
    target 52
  ]
  edge [
    source 34
    target 53
  ]
  edge [
    source 34
    target 55
  ]
  edge [
    source 34
    target 56
  ]
  edge [
    source 34
    target 61
  ]
  edge [
    source 34
    target 62
  ]
  edge [
    source 34
    target 64
  ]
  edge [
    source 36
    target 39
  ]
  edge [
    source 36
    target 55
  ]
  edge [
    source 36
    target 57
  ]
  edge [
    source 36
    target 45
  ]
  edge [
    source 36
    target 49
  ]
  edge [
    source 39
    target 49
  ]
  edge [
    source 39
    target 68
  ]
  edge [
    source 39
    target 69
  ]
  edge [
    source 41
    target 49
  ]
  edge [
    source 45
    target 65
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 48
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 50
  ]
  edge [
    source 47
    target 61
  ]
  edge [
    source 47
    target 62
  ]
  edge [
    source 48
    target 50
  ]
  edge [
    source 51
    target 55
  ]
  edge [
    source 51
    target 54
  ]
  edge [
    source 52
    target 54
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 57
  ]
  edge [
    source 55
    target 58
  ]
  edge [
    source 55
    target 59
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 66
    target 73
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 68
    target 70
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 76
    target 77
  ]
  edge [
    source 76
    target 78
  ]
  edge [
    source 77
    target 78
  ]
]
