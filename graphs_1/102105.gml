graph [
  maxDegree 20
  minDegree 1
  meanDegree 2.0789473684210527
  density 0.027719298245614036
  graphCliqueNumber 3
  node [
    id 0
    label "zaprasza&#263;"
    origin "text"
  ]
  node [
    id 1
    label "wernisa&#380;"
    origin "text"
  ]
  node [
    id 2
    label "wystawa"
    origin "text"
  ]
  node [
    id 3
    label "malarstwo"
    origin "text"
  ]
  node [
    id 4
    label "ozorkowianki"
    origin "text"
  ]
  node [
    id 5
    label "beata"
    origin "text"
  ]
  node [
    id 6
    label "koszyk"
    origin "text"
  ]
  node [
    id 7
    label "ta&#324;cz&#261;ca"
    origin "text"
  ]
  node [
    id 8
    label "anio&#322;"
    origin "text"
  ]
  node [
    id 9
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 10
    label "ogl&#261;da&#263;"
    origin "text"
  ]
  node [
    id 11
    label "koniec"
    origin "text"
  ]
  node [
    id 12
    label "grudzie&#324;"
    origin "text"
  ]
  node [
    id 13
    label "invite"
  ]
  node [
    id 14
    label "ask"
  ]
  node [
    id 15
    label "oferowa&#263;"
  ]
  node [
    id 16
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 17
    label "impreza"
  ]
  node [
    id 18
    label "galeria"
  ]
  node [
    id 19
    label "miejsce"
  ]
  node [
    id 20
    label "sklep"
  ]
  node [
    id 21
    label "muzeum"
  ]
  node [
    id 22
    label "Arsena&#322;"
  ]
  node [
    id 23
    label "kolekcja"
  ]
  node [
    id 24
    label "szyba"
  ]
  node [
    id 25
    label "kurator"
  ]
  node [
    id 26
    label "Agropromocja"
  ]
  node [
    id 27
    label "kustosz"
  ]
  node [
    id 28
    label "ekspozycja"
  ]
  node [
    id 29
    label "okno"
  ]
  node [
    id 30
    label "syntetyzm"
  ]
  node [
    id 31
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 32
    label "linearyzm"
  ]
  node [
    id 33
    label "kompozycja"
  ]
  node [
    id 34
    label "plastyka"
  ]
  node [
    id 35
    label "gwasz"
  ]
  node [
    id 36
    label "rezultat"
  ]
  node [
    id 37
    label "skr&#243;t_perspektywiczny"
  ]
  node [
    id 38
    label "tempera"
  ]
  node [
    id 39
    label "zestaw"
  ]
  node [
    id 40
    label "pojemnik"
  ]
  node [
    id 41
    label "wiklina"
  ]
  node [
    id 42
    label "program"
  ]
  node [
    id 43
    label "basket"
  ]
  node [
    id 44
    label "duch"
  ]
  node [
    id 45
    label "cz&#322;owiek"
  ]
  node [
    id 46
    label "anielski"
  ]
  node [
    id 47
    label "anielstwo"
  ]
  node [
    id 48
    label "niebianin"
  ]
  node [
    id 49
    label "free"
  ]
  node [
    id 50
    label "notice"
  ]
  node [
    id 51
    label "zapoznawa&#263;_si&#281;"
  ]
  node [
    id 52
    label "styka&#263;_si&#281;"
  ]
  node [
    id 53
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 54
    label "defenestracja"
  ]
  node [
    id 55
    label "szereg"
  ]
  node [
    id 56
    label "dzia&#322;anie"
  ]
  node [
    id 57
    label "ostatnie_podrygi"
  ]
  node [
    id 58
    label "kres"
  ]
  node [
    id 59
    label "agonia"
  ]
  node [
    id 60
    label "visitation"
  ]
  node [
    id 61
    label "szeol"
  ]
  node [
    id 62
    label "mogi&#322;a"
  ]
  node [
    id 63
    label "chwila"
  ]
  node [
    id 64
    label "wydarzenie"
  ]
  node [
    id 65
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 66
    label "pogrzebanie"
  ]
  node [
    id 67
    label "punkt"
  ]
  node [
    id 68
    label "&#380;a&#322;oba"
  ]
  node [
    id 69
    label "zabicie"
  ]
  node [
    id 70
    label "kres_&#380;ycia"
  ]
  node [
    id 71
    label "Bo&#380;e_Narodzenie"
  ]
  node [
    id 72
    label "miesi&#261;c"
  ]
  node [
    id 73
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 74
    label "Barb&#243;rka"
  ]
  node [
    id 75
    label "Sylwester"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 2
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 8
    target 46
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 49
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 50
  ]
  edge [
    source 10
    target 51
  ]
  edge [
    source 10
    target 52
  ]
  edge [
    source 10
    target 53
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 54
  ]
  edge [
    source 11
    target 55
  ]
  edge [
    source 11
    target 56
  ]
  edge [
    source 11
    target 19
  ]
  edge [
    source 11
    target 57
  ]
  edge [
    source 11
    target 58
  ]
  edge [
    source 11
    target 59
  ]
  edge [
    source 11
    target 60
  ]
  edge [
    source 11
    target 61
  ]
  edge [
    source 11
    target 62
  ]
  edge [
    source 11
    target 63
  ]
  edge [
    source 11
    target 64
  ]
  edge [
    source 11
    target 65
  ]
  edge [
    source 11
    target 66
  ]
  edge [
    source 11
    target 67
  ]
  edge [
    source 11
    target 68
  ]
  edge [
    source 11
    target 69
  ]
  edge [
    source 11
    target 70
  ]
  edge [
    source 12
    target 71
  ]
  edge [
    source 12
    target 72
  ]
  edge [
    source 12
    target 73
  ]
  edge [
    source 12
    target 74
  ]
  edge [
    source 12
    target 75
  ]
]
