graph [
  maxDegree 32
  minDegree 1
  meanDegree 2.032
  density 0.01638709677419355
  graphCliqueNumber 2
  node [
    id 0
    label "moja"
    origin "text"
  ]
  node [
    id 1
    label "dziewczyna"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "chorobliwie"
    origin "text"
  ]
  node [
    id 4
    label "zazdrosna"
    origin "text"
  ]
  node [
    id 5
    label "pozwala&#263;"
    origin "text"
  ]
  node [
    id 6
    label "nigdzie"
    origin "text"
  ]
  node [
    id 7
    label "wychodzi&#263;"
    origin "text"
  ]
  node [
    id 8
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 9
    label "weekend"
    origin "text"
  ]
  node [
    id 10
    label "sp&#281;dza&#263;"
    origin "text"
  ]
  node [
    id 11
    label "razem"
    origin "text"
  ]
  node [
    id 12
    label "pami&#281;ta&#263;"
    origin "text"
  ]
  node [
    id 13
    label "kiedy"
    origin "text"
  ]
  node [
    id 14
    label "ostatnio"
    origin "text"
  ]
  node [
    id 15
    label "wyszedlem"
    origin "text"
  ]
  node [
    id 16
    label "kumpel"
    origin "text"
  ]
  node [
    id 17
    label "piwo"
    origin "text"
  ]
  node [
    id 18
    label "cz&#322;owiek"
  ]
  node [
    id 19
    label "dziewka"
  ]
  node [
    id 20
    label "dziewoja"
  ]
  node [
    id 21
    label "siksa"
  ]
  node [
    id 22
    label "partnerka"
  ]
  node [
    id 23
    label "dziewczynina"
  ]
  node [
    id 24
    label "dziunia"
  ]
  node [
    id 25
    label "sympatia"
  ]
  node [
    id 26
    label "dziewcz&#281;"
  ]
  node [
    id 27
    label "s&#322;u&#380;&#261;ca"
  ]
  node [
    id 28
    label "kora"
  ]
  node [
    id 29
    label "m&#322;&#243;dka"
  ]
  node [
    id 30
    label "dziecina"
  ]
  node [
    id 31
    label "sikorka"
  ]
  node [
    id 32
    label "si&#281;ga&#263;"
  ]
  node [
    id 33
    label "trwa&#263;"
  ]
  node [
    id 34
    label "obecno&#347;&#263;"
  ]
  node [
    id 35
    label "stan"
  ]
  node [
    id 36
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 37
    label "stand"
  ]
  node [
    id 38
    label "mie&#263;_miejsce"
  ]
  node [
    id 39
    label "uczestniczy&#263;"
  ]
  node [
    id 40
    label "chodzi&#263;"
  ]
  node [
    id 41
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 42
    label "equal"
  ]
  node [
    id 43
    label "niew&#322;a&#347;ciwie"
  ]
  node [
    id 44
    label "chorobliwy"
  ]
  node [
    id 45
    label "nienormalnie"
  ]
  node [
    id 46
    label "strasznie"
  ]
  node [
    id 47
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 48
    label "authorize"
  ]
  node [
    id 49
    label "uznawa&#263;"
  ]
  node [
    id 50
    label "consent"
  ]
  node [
    id 51
    label "uzyskiwa&#263;"
  ]
  node [
    id 52
    label "impart"
  ]
  node [
    id 53
    label "proceed"
  ]
  node [
    id 54
    label "blend"
  ]
  node [
    id 55
    label "give"
  ]
  node [
    id 56
    label "ograniczenie"
  ]
  node [
    id 57
    label "wydostawa&#263;_si&#281;"
  ]
  node [
    id 58
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 59
    label "za&#322;atwi&#263;"
  ]
  node [
    id 60
    label "schodzi&#263;"
  ]
  node [
    id 61
    label "gra&#263;"
  ]
  node [
    id 62
    label "osi&#261;ga&#263;"
  ]
  node [
    id 63
    label "okazywa&#263;_si&#281;"
  ]
  node [
    id 64
    label "seclude"
  ]
  node [
    id 65
    label "strona_&#347;wiata"
  ]
  node [
    id 66
    label "przedk&#322;ada&#263;"
  ]
  node [
    id 67
    label "przedstawia&#263;"
  ]
  node [
    id 68
    label "appear"
  ]
  node [
    id 69
    label "publish"
  ]
  node [
    id 70
    label "ko&#324;czy&#263;"
  ]
  node [
    id 71
    label "wypada&#263;"
  ]
  node [
    id 72
    label "pochodzi&#263;"
  ]
  node [
    id 73
    label "ukazywa&#263;_si&#281;_drukiem"
  ]
  node [
    id 74
    label "uwalnia&#263;_si&#281;"
  ]
  node [
    id 75
    label "wygl&#261;da&#263;"
  ]
  node [
    id 76
    label "opuszcza&#263;"
  ]
  node [
    id 77
    label "wystarcza&#263;"
  ]
  node [
    id 78
    label "wyrusza&#263;"
  ]
  node [
    id 79
    label "perform"
  ]
  node [
    id 80
    label "heighten"
  ]
  node [
    id 81
    label "jaki&#347;"
  ]
  node [
    id 82
    label "niedziela"
  ]
  node [
    id 83
    label "sobota"
  ]
  node [
    id 84
    label "tydzie&#324;"
  ]
  node [
    id 85
    label "usuwa&#263;"
  ]
  node [
    id 86
    label "robi&#263;"
  ]
  node [
    id 87
    label "base_on_balls"
  ]
  node [
    id 88
    label "przep&#281;dza&#263;"
  ]
  node [
    id 89
    label "przykrzy&#263;"
  ]
  node [
    id 90
    label "doprowadza&#263;"
  ]
  node [
    id 91
    label "p&#281;dzi&#263;"
  ]
  node [
    id 92
    label "&#322;&#261;cznie"
  ]
  node [
    id 93
    label "zna&#263;"
  ]
  node [
    id 94
    label "troska&#263;_si&#281;"
  ]
  node [
    id 95
    label "zachowywa&#263;"
  ]
  node [
    id 96
    label "chowa&#263;"
  ]
  node [
    id 97
    label "think"
  ]
  node [
    id 98
    label "pilnowa&#263;"
  ]
  node [
    id 99
    label "recall"
  ]
  node [
    id 100
    label "echo"
  ]
  node [
    id 101
    label "take_care"
  ]
  node [
    id 102
    label "ostatni"
  ]
  node [
    id 103
    label "poprzednio"
  ]
  node [
    id 104
    label "aktualnie"
  ]
  node [
    id 105
    label "kumplowanie_si&#281;"
  ]
  node [
    id 106
    label "znajomy"
  ]
  node [
    id 107
    label "konfrater"
  ]
  node [
    id 108
    label "towarzysz"
  ]
  node [
    id 109
    label "zakolegowanie_si&#281;"
  ]
  node [
    id 110
    label "partner"
  ]
  node [
    id 111
    label "ziom"
  ]
  node [
    id 112
    label "browarnia"
  ]
  node [
    id 113
    label "anta&#322;"
  ]
  node [
    id 114
    label "wyj&#347;cie"
  ]
  node [
    id 115
    label "warzy&#263;"
  ]
  node [
    id 116
    label "warzenie"
  ]
  node [
    id 117
    label "uwarzenie"
  ]
  node [
    id 118
    label "alkohol"
  ]
  node [
    id 119
    label "nap&#243;j"
  ]
  node [
    id 120
    label "nawarzy&#263;"
  ]
  node [
    id 121
    label "bacik"
  ]
  node [
    id 122
    label "uwarzy&#263;"
  ]
  node [
    id 123
    label "nawarzenie"
  ]
  node [
    id 124
    label "birofilia"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 48
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 92
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 32
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 12
    target 94
  ]
  edge [
    source 12
    target 95
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 12
    target 86
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 102
  ]
  edge [
    source 14
    target 103
  ]
  edge [
    source 14
    target 104
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 105
  ]
  edge [
    source 16
    target 106
  ]
  edge [
    source 16
    target 107
  ]
  edge [
    source 16
    target 108
  ]
  edge [
    source 16
    target 109
  ]
  edge [
    source 16
    target 110
  ]
  edge [
    source 16
    target 111
  ]
  edge [
    source 17
    target 112
  ]
  edge [
    source 17
    target 113
  ]
  edge [
    source 17
    target 114
  ]
  edge [
    source 17
    target 115
  ]
  edge [
    source 17
    target 116
  ]
  edge [
    source 17
    target 117
  ]
  edge [
    source 17
    target 118
  ]
  edge [
    source 17
    target 119
  ]
  edge [
    source 17
    target 120
  ]
  edge [
    source 17
    target 121
  ]
  edge [
    source 17
    target 122
  ]
  edge [
    source 17
    target 123
  ]
  edge [
    source 17
    target 124
  ]
]
