graph [
  maxDegree 14
  minDegree 1
  meanDegree 1.8666666666666667
  density 0.13333333333333333
  graphCliqueNumber 2
  node [
    id 0
    label "tytu&#322;"
    origin "text"
  ]
  node [
    id 1
    label "podtytu&#322;"
  ]
  node [
    id 2
    label "debit"
  ]
  node [
    id 3
    label "szata_graficzna"
  ]
  node [
    id 4
    label "elevation"
  ]
  node [
    id 5
    label "wyda&#263;"
  ]
  node [
    id 6
    label "nadtytu&#322;"
  ]
  node [
    id 7
    label "tytulatura"
  ]
  node [
    id 8
    label "nazwa"
  ]
  node [
    id 9
    label "redaktor"
  ]
  node [
    id 10
    label "wydawa&#263;"
  ]
  node [
    id 11
    label "druk"
  ]
  node [
    id 12
    label "mianowaniec"
  ]
  node [
    id 13
    label "poster"
  ]
  node [
    id 14
    label "publikacja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
]
