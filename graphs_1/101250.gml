graph [
  maxDegree 10
  minDegree 1
  meanDegree 1.5151515151515151
  density 0.04734848484848485
  graphCliqueNumber 3
  node [
    id 0
    label "janusz"
    origin "text"
  ]
  node [
    id 1
    label "rolicki"
    origin "text"
  ]
  node [
    id 2
    label "Polak"
  ]
  node [
    id 3
    label "gra&#380;yna"
  ]
  node [
    id 4
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 5
    label "pata&#322;ach"
  ]
  node [
    id 6
    label "cwaniaczek"
  ]
  node [
    id 7
    label "kibic"
  ]
  node [
    id 8
    label "przeci&#281;tniak"
  ]
  node [
    id 9
    label "tatusiek"
  ]
  node [
    id 10
    label "facet"
  ]
  node [
    id 11
    label "Janusz"
  ]
  node [
    id 12
    label "Rolicki"
  ]
  node [
    id 13
    label "Andrzej"
  ]
  node [
    id 14
    label "wydzia&#322;"
  ]
  node [
    id 15
    label "historia"
  ]
  node [
    id 16
    label "Uniwerystetu"
  ]
  node [
    id 17
    label "warszawski"
  ]
  node [
    id 18
    label "telewizja"
  ]
  node [
    id 19
    label "polski"
  ]
  node [
    id 20
    label "godzina"
  ]
  node [
    id 21
    label "szczero&#347;&#263;"
  ]
  node [
    id 22
    label "tylko"
  ]
  node [
    id 23
    label "wyspa"
  ]
  node [
    id 24
    label "niedziela"
  ]
  node [
    id 25
    label "sam"
  ]
  node [
    id 26
    label "na"
  ]
  node [
    id 27
    label "Edward"
  ]
  node [
    id 28
    label "Gierek"
  ]
  node [
    id 29
    label "Juliana"
  ]
  node [
    id 30
    label "Bruno"
  ]
  node [
    id 31
    label "Boles&#322;awa"
  ]
  node [
    id 32
    label "Prus"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 13
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 24
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 25
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 31
    target 32
  ]
]
