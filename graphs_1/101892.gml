graph [
  maxDegree 40
  minDegree 1
  meanDegree 1.978021978021978
  density 0.02197802197802198
  graphCliqueNumber 3
  node [
    id 0
    label "oblicze"
    origin "text"
  ]
  node [
    id 1
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "godzina"
    origin "text"
  ]
  node [
    id 4
    label "czas"
    origin "text"
  ]
  node [
    id 5
    label "s&#322;oneczny"
    origin "text"
  ]
  node [
    id 6
    label "nowa"
    origin "text"
  ]
  node [
    id 7
    label "jork"
    origin "text"
  ]
  node [
    id 8
    label "je&#380;eli"
    origin "text"
  ]
  node [
    id 9
    label "amsterdam"
    origin "text"
  ]
  node [
    id 10
    label "wygl&#261;d"
  ]
  node [
    id 11
    label "cz&#322;owiek"
  ]
  node [
    id 12
    label "comeliness"
  ]
  node [
    id 13
    label "twarz"
  ]
  node [
    id 14
    label "facjata"
  ]
  node [
    id 15
    label "charakter"
  ]
  node [
    id 16
    label "face"
  ]
  node [
    id 17
    label "si&#281;ga&#263;"
  ]
  node [
    id 18
    label "trwa&#263;"
  ]
  node [
    id 19
    label "obecno&#347;&#263;"
  ]
  node [
    id 20
    label "stan"
  ]
  node [
    id 21
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 22
    label "stand"
  ]
  node [
    id 23
    label "mie&#263;_miejsce"
  ]
  node [
    id 24
    label "uczestniczy&#263;"
  ]
  node [
    id 25
    label "chodzi&#263;"
  ]
  node [
    id 26
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 27
    label "equal"
  ]
  node [
    id 28
    label "minuta"
  ]
  node [
    id 29
    label "doba"
  ]
  node [
    id 30
    label "p&#243;&#322;godzina"
  ]
  node [
    id 31
    label "kwadrans"
  ]
  node [
    id 32
    label "time"
  ]
  node [
    id 33
    label "jednostka_czasu"
  ]
  node [
    id 34
    label "czasokres"
  ]
  node [
    id 35
    label "trawienie"
  ]
  node [
    id 36
    label "kategoria_gramatyczna"
  ]
  node [
    id 37
    label "period"
  ]
  node [
    id 38
    label "odczyt"
  ]
  node [
    id 39
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 40
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 41
    label "chwila"
  ]
  node [
    id 42
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 43
    label "poprzedzenie"
  ]
  node [
    id 44
    label "koniugacja"
  ]
  node [
    id 45
    label "dzieje"
  ]
  node [
    id 46
    label "poprzedzi&#263;"
  ]
  node [
    id 47
    label "przep&#322;ywanie"
  ]
  node [
    id 48
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 49
    label "odwlekanie_si&#281;"
  ]
  node [
    id 50
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 51
    label "Zeitgeist"
  ]
  node [
    id 52
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 53
    label "okres_czasu"
  ]
  node [
    id 54
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 55
    label "pochodzi&#263;"
  ]
  node [
    id 56
    label "schy&#322;ek"
  ]
  node [
    id 57
    label "czwarty_wymiar"
  ]
  node [
    id 58
    label "chronometria"
  ]
  node [
    id 59
    label "poprzedzanie"
  ]
  node [
    id 60
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 61
    label "pogoda"
  ]
  node [
    id 62
    label "zegar"
  ]
  node [
    id 63
    label "trawi&#263;"
  ]
  node [
    id 64
    label "pochodzenie"
  ]
  node [
    id 65
    label "poprzedza&#263;"
  ]
  node [
    id 66
    label "time_period"
  ]
  node [
    id 67
    label "rachuba_czasu"
  ]
  node [
    id 68
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 69
    label "czasoprzestrze&#324;"
  ]
  node [
    id 70
    label "laba"
  ]
  node [
    id 71
    label "bezchmurny"
  ]
  node [
    id 72
    label "bezdeszczowy"
  ]
  node [
    id 73
    label "s&#322;onecznie"
  ]
  node [
    id 74
    label "jasny"
  ]
  node [
    id 75
    label "fotowoltaiczny"
  ]
  node [
    id 76
    label "pogodny"
  ]
  node [
    id 77
    label "weso&#322;y"
  ]
  node [
    id 78
    label "ciep&#322;y"
  ]
  node [
    id 79
    label "letni"
  ]
  node [
    id 80
    label "gwiazda"
  ]
  node [
    id 81
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 82
    label "pies_do_towarzystwa"
  ]
  node [
    id 83
    label "terier"
  ]
  node [
    id 84
    label "pies_pokojowy"
  ]
  node [
    id 85
    label "pies_miniaturowy"
  ]
  node [
    id 86
    label "pies_ozdobny"
  ]
  node [
    id 87
    label "nowy"
  ]
  node [
    id 88
    label "Jork"
  ]
  node [
    id 89
    label "los"
  ]
  node [
    id 90
    label "Angeles"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 87
    target 88
  ]
  edge [
    source 89
    target 90
  ]
]
