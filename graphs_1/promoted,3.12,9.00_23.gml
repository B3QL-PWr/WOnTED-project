graph [
  maxDegree 26
  minDegree 1
  meanDegree 2
  density 0.03333333333333333
  graphCliqueNumber 3
  node [
    id 0
    label "staro&#380;ytno&#347;&#263;"
    origin "text"
  ]
  node [
    id 1
    label "by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "epoka"
    origin "text"
  ]
  node [
    id 3
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 4
    label "kojarzy&#263;"
    origin "text"
  ]
  node [
    id 5
    label "si&#281;"
    origin "text"
  ]
  node [
    id 6
    label "bardzo"
    origin "text"
  ]
  node [
    id 7
    label "pompatycznie"
    origin "text"
  ]
  node [
    id 8
    label "aretalogia"
  ]
  node [
    id 9
    label "styl_dorycki"
  ]
  node [
    id 10
    label "okres_klasyczny"
  ]
  node [
    id 11
    label "klasycyzm"
  ]
  node [
    id 12
    label "styl_koryncki"
  ]
  node [
    id 13
    label "antyk"
  ]
  node [
    id 14
    label "styl_kompozytowy"
  ]
  node [
    id 15
    label "styl_jo&#324;ski"
  ]
  node [
    id 16
    label "si&#281;ga&#263;"
  ]
  node [
    id 17
    label "trwa&#263;"
  ]
  node [
    id 18
    label "obecno&#347;&#263;"
  ]
  node [
    id 19
    label "stan"
  ]
  node [
    id 20
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 21
    label "stand"
  ]
  node [
    id 22
    label "mie&#263;_miejsce"
  ]
  node [
    id 23
    label "uczestniczy&#263;"
  ]
  node [
    id 24
    label "chodzi&#263;"
  ]
  node [
    id 25
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 26
    label "equal"
  ]
  node [
    id 27
    label "pliocen"
  ]
  node [
    id 28
    label "czas"
  ]
  node [
    id 29
    label "eocen"
  ]
  node [
    id 30
    label "jura_p&#243;&#378;na"
  ]
  node [
    id 31
    label "jednostka_geologiczna"
  ]
  node [
    id 32
    label "&#347;rodkowy_trias"
  ]
  node [
    id 33
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 34
    label "paleocen"
  ]
  node [
    id 35
    label "dzieje"
  ]
  node [
    id 36
    label "plejstocen"
  ]
  node [
    id 37
    label "bajos"
  ]
  node [
    id 38
    label "holocen"
  ]
  node [
    id 39
    label "oligocen"
  ]
  node [
    id 40
    label "term"
  ]
  node [
    id 41
    label "Zeitgeist"
  ]
  node [
    id 42
    label "kelowej"
  ]
  node [
    id 43
    label "schy&#322;ek"
  ]
  node [
    id 44
    label "miocen"
  ]
  node [
    id 45
    label "aalen"
  ]
  node [
    id 46
    label "wczesny_trias"
  ]
  node [
    id 47
    label "jura_wczesna"
  ]
  node [
    id 48
    label "jura_&#347;rodkowa"
  ]
  node [
    id 49
    label "okres"
  ]
  node [
    id 50
    label "cover"
  ]
  node [
    id 51
    label "rozumie&#263;"
  ]
  node [
    id 52
    label "zaskakiwa&#263;"
  ]
  node [
    id 53
    label "swat"
  ]
  node [
    id 54
    label "relate"
  ]
  node [
    id 55
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 56
    label "w_chuj"
  ]
  node [
    id 57
    label "nadmiernie"
  ]
  node [
    id 58
    label "pompatyczny"
  ]
  node [
    id 59
    label "portentously"
  ]
  node [
    id 60
    label "podnio&#347;le"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
]
