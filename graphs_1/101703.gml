graph [
  maxDegree 12
  minDegree 1
  meanDegree 1.8823529411764706
  density 0.11764705882352941
  graphCliqueNumber 2
  node [
    id 0
    label "festiwal"
    origin "text"
  ]
  node [
    id 1
    label "machinimy"
    origin "text"
  ]
  node [
    id 2
    label "teraz"
    origin "text"
  ]
  node [
    id 3
    label "europa"
    origin "text"
  ]
  node [
    id 4
    label "Nowe_Horyzonty"
  ]
  node [
    id 5
    label "Interwizja"
  ]
  node [
    id 6
    label "Open'er"
  ]
  node [
    id 7
    label "Przystanek_Woodstock"
  ]
  node [
    id 8
    label "impreza"
  ]
  node [
    id 9
    label "Woodstock"
  ]
  node [
    id 10
    label "Metalmania"
  ]
  node [
    id 11
    label "Opole"
  ]
  node [
    id 12
    label "FAMA"
  ]
  node [
    id 13
    label "Eurowizja"
  ]
  node [
    id 14
    label "Brutal"
  ]
  node [
    id 15
    label "chwila"
  ]
  node [
    id 16
    label "tera&#378;niejszo&#347;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
]
