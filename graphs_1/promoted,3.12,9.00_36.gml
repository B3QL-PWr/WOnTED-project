graph [
  maxDegree 217
  minDegree 1
  meanDegree 2.0194805194805197
  density 0.006578112441304624
  graphCliqueNumber 3
  node [
    id 0
    label "polska"
    origin "text"
  ]
  node [
    id 1
    label "by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "jedyna"
    origin "text"
  ]
  node [
    id 3
    label "pa&#324;stwo"
    origin "text"
  ]
  node [
    id 4
    label "unia"
    origin "text"
  ]
  node [
    id 5
    label "europejski"
    origin "text"
  ]
  node [
    id 6
    label "gdzie"
    origin "text"
  ]
  node [
    id 7
    label "cena"
    origin "text"
  ]
  node [
    id 8
    label "detaliczny"
    origin "text"
  ]
  node [
    id 9
    label "olej"
    origin "text"
  ]
  node [
    id 10
    label "nap&#281;dowy"
    origin "text"
  ]
  node [
    id 11
    label "wzros&#322;y"
    origin "text"
  ]
  node [
    id 12
    label "odniesienie"
    origin "text"
  ]
  node [
    id 13
    label "poprzedni"
    origin "text"
  ]
  node [
    id 14
    label "tydzie&#324;"
    origin "text"
  ]
  node [
    id 15
    label "miesi&#261;c"
    origin "text"
  ]
  node [
    id 16
    label "si&#281;ga&#263;"
  ]
  node [
    id 17
    label "trwa&#263;"
  ]
  node [
    id 18
    label "obecno&#347;&#263;"
  ]
  node [
    id 19
    label "stan"
  ]
  node [
    id 20
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 21
    label "stand"
  ]
  node [
    id 22
    label "mie&#263;_miejsce"
  ]
  node [
    id 23
    label "uczestniczy&#263;"
  ]
  node [
    id 24
    label "chodzi&#263;"
  ]
  node [
    id 25
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 26
    label "equal"
  ]
  node [
    id 27
    label "kobieta"
  ]
  node [
    id 28
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 29
    label "Filipiny"
  ]
  node [
    id 30
    label "Rwanda"
  ]
  node [
    id 31
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 32
    label "Monako"
  ]
  node [
    id 33
    label "Korea"
  ]
  node [
    id 34
    label "Ghana"
  ]
  node [
    id 35
    label "Czarnog&#243;ra"
  ]
  node [
    id 36
    label "Malawi"
  ]
  node [
    id 37
    label "Indonezja"
  ]
  node [
    id 38
    label "Bu&#322;garia"
  ]
  node [
    id 39
    label "Nauru"
  ]
  node [
    id 40
    label "Kenia"
  ]
  node [
    id 41
    label "Kambod&#380;a"
  ]
  node [
    id 42
    label "Mali"
  ]
  node [
    id 43
    label "Austria"
  ]
  node [
    id 44
    label "interior"
  ]
  node [
    id 45
    label "Armenia"
  ]
  node [
    id 46
    label "Fid&#380;i"
  ]
  node [
    id 47
    label "Tuwalu"
  ]
  node [
    id 48
    label "Etiopia"
  ]
  node [
    id 49
    label "Malta"
  ]
  node [
    id 50
    label "Malezja"
  ]
  node [
    id 51
    label "Grenada"
  ]
  node [
    id 52
    label "Tad&#380;ykistan"
  ]
  node [
    id 53
    label "Wehrlen"
  ]
  node [
    id 54
    label "para"
  ]
  node [
    id 55
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 56
    label "Rumunia"
  ]
  node [
    id 57
    label "Maroko"
  ]
  node [
    id 58
    label "Bhutan"
  ]
  node [
    id 59
    label "S&#322;owacja"
  ]
  node [
    id 60
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 61
    label "Seszele"
  ]
  node [
    id 62
    label "Kuwejt"
  ]
  node [
    id 63
    label "Arabia_Saudyjska"
  ]
  node [
    id 64
    label "Ekwador"
  ]
  node [
    id 65
    label "Kanada"
  ]
  node [
    id 66
    label "Japonia"
  ]
  node [
    id 67
    label "ziemia"
  ]
  node [
    id 68
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 69
    label "Hiszpania"
  ]
  node [
    id 70
    label "Wyspy_Marshalla"
  ]
  node [
    id 71
    label "Botswana"
  ]
  node [
    id 72
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 73
    label "D&#380;ibuti"
  ]
  node [
    id 74
    label "grupa"
  ]
  node [
    id 75
    label "Wietnam"
  ]
  node [
    id 76
    label "Egipt"
  ]
  node [
    id 77
    label "Burkina_Faso"
  ]
  node [
    id 78
    label "Niemcy"
  ]
  node [
    id 79
    label "Khitai"
  ]
  node [
    id 80
    label "Macedonia"
  ]
  node [
    id 81
    label "Albania"
  ]
  node [
    id 82
    label "Madagaskar"
  ]
  node [
    id 83
    label "Bahrajn"
  ]
  node [
    id 84
    label "Jemen"
  ]
  node [
    id 85
    label "Lesoto"
  ]
  node [
    id 86
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 87
    label "Samoa"
  ]
  node [
    id 88
    label "Andora"
  ]
  node [
    id 89
    label "Chiny"
  ]
  node [
    id 90
    label "Cypr"
  ]
  node [
    id 91
    label "Wielka_Brytania"
  ]
  node [
    id 92
    label "Ukraina"
  ]
  node [
    id 93
    label "Paragwaj"
  ]
  node [
    id 94
    label "Trynidad_i_Tobago"
  ]
  node [
    id 95
    label "Libia"
  ]
  node [
    id 96
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 97
    label "Surinam"
  ]
  node [
    id 98
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 99
    label "Australia"
  ]
  node [
    id 100
    label "Nigeria"
  ]
  node [
    id 101
    label "Honduras"
  ]
  node [
    id 102
    label "Bangladesz"
  ]
  node [
    id 103
    label "Peru"
  ]
  node [
    id 104
    label "Kazachstan"
  ]
  node [
    id 105
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 106
    label "Irak"
  ]
  node [
    id 107
    label "holoarktyka"
  ]
  node [
    id 108
    label "USA"
  ]
  node [
    id 109
    label "Sudan"
  ]
  node [
    id 110
    label "Nepal"
  ]
  node [
    id 111
    label "San_Marino"
  ]
  node [
    id 112
    label "Burundi"
  ]
  node [
    id 113
    label "Dominikana"
  ]
  node [
    id 114
    label "Komory"
  ]
  node [
    id 115
    label "granica_pa&#324;stwa"
  ]
  node [
    id 116
    label "Gwatemala"
  ]
  node [
    id 117
    label "Antarktis"
  ]
  node [
    id 118
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 119
    label "Brunei"
  ]
  node [
    id 120
    label "Iran"
  ]
  node [
    id 121
    label "Zimbabwe"
  ]
  node [
    id 122
    label "Namibia"
  ]
  node [
    id 123
    label "Meksyk"
  ]
  node [
    id 124
    label "Kamerun"
  ]
  node [
    id 125
    label "zwrot"
  ]
  node [
    id 126
    label "Somalia"
  ]
  node [
    id 127
    label "Angola"
  ]
  node [
    id 128
    label "Gabon"
  ]
  node [
    id 129
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 130
    label "Mozambik"
  ]
  node [
    id 131
    label "Tajwan"
  ]
  node [
    id 132
    label "Tunezja"
  ]
  node [
    id 133
    label "Nowa_Zelandia"
  ]
  node [
    id 134
    label "Liban"
  ]
  node [
    id 135
    label "Jordania"
  ]
  node [
    id 136
    label "Tonga"
  ]
  node [
    id 137
    label "Czad"
  ]
  node [
    id 138
    label "Liberia"
  ]
  node [
    id 139
    label "Gwinea"
  ]
  node [
    id 140
    label "Belize"
  ]
  node [
    id 141
    label "&#321;otwa"
  ]
  node [
    id 142
    label "Syria"
  ]
  node [
    id 143
    label "Benin"
  ]
  node [
    id 144
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 145
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 146
    label "Dominika"
  ]
  node [
    id 147
    label "Antigua_i_Barbuda"
  ]
  node [
    id 148
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 149
    label "Hanower"
  ]
  node [
    id 150
    label "partia"
  ]
  node [
    id 151
    label "Afganistan"
  ]
  node [
    id 152
    label "Kiribati"
  ]
  node [
    id 153
    label "W&#322;ochy"
  ]
  node [
    id 154
    label "Szwajcaria"
  ]
  node [
    id 155
    label "Sahara_Zachodnia"
  ]
  node [
    id 156
    label "Chorwacja"
  ]
  node [
    id 157
    label "Tajlandia"
  ]
  node [
    id 158
    label "Salwador"
  ]
  node [
    id 159
    label "Bahamy"
  ]
  node [
    id 160
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 161
    label "S&#322;owenia"
  ]
  node [
    id 162
    label "Gambia"
  ]
  node [
    id 163
    label "Urugwaj"
  ]
  node [
    id 164
    label "Zair"
  ]
  node [
    id 165
    label "Erytrea"
  ]
  node [
    id 166
    label "Rosja"
  ]
  node [
    id 167
    label "Uganda"
  ]
  node [
    id 168
    label "Niger"
  ]
  node [
    id 169
    label "Mauritius"
  ]
  node [
    id 170
    label "Turkmenistan"
  ]
  node [
    id 171
    label "Turcja"
  ]
  node [
    id 172
    label "Irlandia"
  ]
  node [
    id 173
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 174
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 175
    label "Gwinea_Bissau"
  ]
  node [
    id 176
    label "Belgia"
  ]
  node [
    id 177
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 178
    label "Palau"
  ]
  node [
    id 179
    label "Barbados"
  ]
  node [
    id 180
    label "Chile"
  ]
  node [
    id 181
    label "Wenezuela"
  ]
  node [
    id 182
    label "W&#281;gry"
  ]
  node [
    id 183
    label "Argentyna"
  ]
  node [
    id 184
    label "Kolumbia"
  ]
  node [
    id 185
    label "Sierra_Leone"
  ]
  node [
    id 186
    label "Azerbejd&#380;an"
  ]
  node [
    id 187
    label "Kongo"
  ]
  node [
    id 188
    label "Pakistan"
  ]
  node [
    id 189
    label "Liechtenstein"
  ]
  node [
    id 190
    label "Nikaragua"
  ]
  node [
    id 191
    label "Senegal"
  ]
  node [
    id 192
    label "Indie"
  ]
  node [
    id 193
    label "Suazi"
  ]
  node [
    id 194
    label "Polska"
  ]
  node [
    id 195
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 196
    label "Algieria"
  ]
  node [
    id 197
    label "terytorium"
  ]
  node [
    id 198
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 199
    label "Jamajka"
  ]
  node [
    id 200
    label "Kostaryka"
  ]
  node [
    id 201
    label "Timor_Wschodni"
  ]
  node [
    id 202
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 203
    label "Kuba"
  ]
  node [
    id 204
    label "Mauretania"
  ]
  node [
    id 205
    label "Portoryko"
  ]
  node [
    id 206
    label "Brazylia"
  ]
  node [
    id 207
    label "Mo&#322;dawia"
  ]
  node [
    id 208
    label "organizacja"
  ]
  node [
    id 209
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 210
    label "Litwa"
  ]
  node [
    id 211
    label "Kirgistan"
  ]
  node [
    id 212
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 213
    label "Izrael"
  ]
  node [
    id 214
    label "Grecja"
  ]
  node [
    id 215
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 216
    label "Holandia"
  ]
  node [
    id 217
    label "Sri_Lanka"
  ]
  node [
    id 218
    label "Katar"
  ]
  node [
    id 219
    label "Mikronezja"
  ]
  node [
    id 220
    label "Mongolia"
  ]
  node [
    id 221
    label "Laos"
  ]
  node [
    id 222
    label "Malediwy"
  ]
  node [
    id 223
    label "Zambia"
  ]
  node [
    id 224
    label "Tanzania"
  ]
  node [
    id 225
    label "Gujana"
  ]
  node [
    id 226
    label "Czechy"
  ]
  node [
    id 227
    label "Panama"
  ]
  node [
    id 228
    label "Uzbekistan"
  ]
  node [
    id 229
    label "Gruzja"
  ]
  node [
    id 230
    label "Serbia"
  ]
  node [
    id 231
    label "Francja"
  ]
  node [
    id 232
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 233
    label "Togo"
  ]
  node [
    id 234
    label "Estonia"
  ]
  node [
    id 235
    label "Oman"
  ]
  node [
    id 236
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 237
    label "Portugalia"
  ]
  node [
    id 238
    label "Boliwia"
  ]
  node [
    id 239
    label "Luksemburg"
  ]
  node [
    id 240
    label "Haiti"
  ]
  node [
    id 241
    label "Wyspy_Salomona"
  ]
  node [
    id 242
    label "Birma"
  ]
  node [
    id 243
    label "Rodezja"
  ]
  node [
    id 244
    label "uk&#322;ad"
  ]
  node [
    id 245
    label "Unia_Wolno&#347;ci"
  ]
  node [
    id 246
    label "Unia_Europejska"
  ]
  node [
    id 247
    label "combination"
  ]
  node [
    id 248
    label "union"
  ]
  node [
    id 249
    label "Unia"
  ]
  node [
    id 250
    label "European"
  ]
  node [
    id 251
    label "po_europejsku"
  ]
  node [
    id 252
    label "charakterystyczny"
  ]
  node [
    id 253
    label "europejsko"
  ]
  node [
    id 254
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 255
    label "typowy"
  ]
  node [
    id 256
    label "cenowa_zmiana_poda&#380;y"
  ]
  node [
    id 257
    label "warto&#347;&#263;"
  ]
  node [
    id 258
    label "wycenienie"
  ]
  node [
    id 259
    label "cenowa_elastyczno&#347;&#263;_popytu"
  ]
  node [
    id 260
    label "dyskryminacja_cenowa"
  ]
  node [
    id 261
    label "inflacja"
  ]
  node [
    id 262
    label "kosztowa&#263;"
  ]
  node [
    id 263
    label "kupowanie"
  ]
  node [
    id 264
    label "wyceni&#263;"
  ]
  node [
    id 265
    label "worth"
  ]
  node [
    id 266
    label "kosztowanie"
  ]
  node [
    id 267
    label "dok&#322;adny"
  ]
  node [
    id 268
    label "detalicznie"
  ]
  node [
    id 269
    label "drobny"
  ]
  node [
    id 270
    label "szczeg&#243;&#322;owo"
  ]
  node [
    id 271
    label "skrupulatny"
  ]
  node [
    id 272
    label "farba"
  ]
  node [
    id 273
    label "obraz"
  ]
  node [
    id 274
    label "farba_olejna"
  ]
  node [
    id 275
    label "technika"
  ]
  node [
    id 276
    label "porcja"
  ]
  node [
    id 277
    label "olejny"
  ]
  node [
    id 278
    label "substancja"
  ]
  node [
    id 279
    label "oil"
  ]
  node [
    id 280
    label "nap&#281;dny"
  ]
  node [
    id 281
    label "wypowied&#378;"
  ]
  node [
    id 282
    label "od&#322;o&#380;enie"
  ]
  node [
    id 283
    label "skill"
  ]
  node [
    id 284
    label "doznanie"
  ]
  node [
    id 285
    label "gaze"
  ]
  node [
    id 286
    label "deference"
  ]
  node [
    id 287
    label "dochrapanie_si&#281;"
  ]
  node [
    id 288
    label "dostarczenie"
  ]
  node [
    id 289
    label "mention"
  ]
  node [
    id 290
    label "bearing"
  ]
  node [
    id 291
    label "po&#380;yczenie"
  ]
  node [
    id 292
    label "cel"
  ]
  node [
    id 293
    label "uzyskanie"
  ]
  node [
    id 294
    label "oddanie"
  ]
  node [
    id 295
    label "powi&#261;zanie"
  ]
  node [
    id 296
    label "poprzednio"
  ]
  node [
    id 297
    label "przesz&#322;y"
  ]
  node [
    id 298
    label "wcze&#347;niejszy"
  ]
  node [
    id 299
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 300
    label "doba"
  ]
  node [
    id 301
    label "czas"
  ]
  node [
    id 302
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 303
    label "weekend"
  ]
  node [
    id 304
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 305
    label "rok"
  ]
  node [
    id 306
    label "miech"
  ]
  node [
    id 307
    label "kalendy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 257
  ]
  edge [
    source 7
    target 258
  ]
  edge [
    source 7
    target 259
  ]
  edge [
    source 7
    target 260
  ]
  edge [
    source 7
    target 261
  ]
  edge [
    source 7
    target 262
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 265
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 267
  ]
  edge [
    source 8
    target 268
  ]
  edge [
    source 8
    target 269
  ]
  edge [
    source 8
    target 270
  ]
  edge [
    source 8
    target 271
  ]
  edge [
    source 8
    target 254
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 272
  ]
  edge [
    source 9
    target 273
  ]
  edge [
    source 9
    target 274
  ]
  edge [
    source 9
    target 275
  ]
  edge [
    source 9
    target 276
  ]
  edge [
    source 9
    target 277
  ]
  edge [
    source 9
    target 278
  ]
  edge [
    source 9
    target 279
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 280
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 281
  ]
  edge [
    source 12
    target 282
  ]
  edge [
    source 12
    target 283
  ]
  edge [
    source 12
    target 284
  ]
  edge [
    source 12
    target 285
  ]
  edge [
    source 12
    target 286
  ]
  edge [
    source 12
    target 287
  ]
  edge [
    source 12
    target 288
  ]
  edge [
    source 12
    target 289
  ]
  edge [
    source 12
    target 290
  ]
  edge [
    source 12
    target 291
  ]
  edge [
    source 12
    target 292
  ]
  edge [
    source 12
    target 293
  ]
  edge [
    source 12
    target 294
  ]
  edge [
    source 12
    target 295
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 296
  ]
  edge [
    source 13
    target 297
  ]
  edge [
    source 13
    target 298
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 299
  ]
  edge [
    source 14
    target 300
  ]
  edge [
    source 14
    target 301
  ]
  edge [
    source 14
    target 302
  ]
  edge [
    source 14
    target 303
  ]
  edge [
    source 15
    target 301
  ]
  edge [
    source 15
    target 304
  ]
  edge [
    source 15
    target 305
  ]
  edge [
    source 15
    target 306
  ]
  edge [
    source 15
    target 307
  ]
]
