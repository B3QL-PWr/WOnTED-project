graph [
  maxDegree 217
  minDegree 1
  meanDegree 2.129166666666667
  density 0.0044450243562978425
  graphCliqueNumber 3
  node [
    id 0
    label "decyzja"
    origin "text"
  ]
  node [
    id 1
    label "mon"
    origin "text"
  ]
  node [
    id 2
    label "minister"
    origin "text"
  ]
  node [
    id 3
    label "obrona"
    origin "text"
  ]
  node [
    id 4
    label "narodowy"
    origin "text"
  ]
  node [
    id 5
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 6
    label "marco"
    origin "text"
  ]
  node [
    id 7
    label "rocznik"
    origin "text"
  ]
  node [
    id 8
    label "sprawa"
    origin "text"
  ]
  node [
    id 9
    label "zwi&#281;kszenie"
    origin "text"
  ]
  node [
    id 10
    label "nale&#380;no&#347;&#263;"
    origin "text"
  ]
  node [
    id 11
    label "zagraniczny"
    origin "text"
  ]
  node [
    id 12
    label "dodatek"
    origin "text"
  ]
  node [
    id 13
    label "otrzymywa&#263;"
    origin "text"
  ]
  node [
    id 14
    label "przez"
    origin "text"
  ]
  node [
    id 15
    label "&#380;o&#322;nierz"
    origin "text"
  ]
  node [
    id 16
    label "zawodowy"
    origin "text"
  ]
  node [
    id 17
    label "niezawodowy"
    origin "text"
  ]
  node [
    id 18
    label "wyznaczy&#263;"
    origin "text"
  ]
  node [
    id 19
    label "pe&#322;nie&#263;"
    origin "text"
  ]
  node [
    id 20
    label "s&#322;u&#380;ba"
    origin "text"
  ]
  node [
    id 21
    label "poza"
    origin "text"
  ]
  node [
    id 22
    label "granica"
    origin "text"
  ]
  node [
    id 23
    label "pa&#324;stwo"
    origin "text"
  ]
  node [
    id 24
    label "pracownik"
    origin "text"
  ]
  node [
    id 25
    label "wojska"
    origin "text"
  ]
  node [
    id 26
    label "zatrudniona"
    origin "text"
  ]
  node [
    id 27
    label "jednostka"
    origin "text"
  ]
  node [
    id 28
    label "wojskowy"
    origin "text"
  ]
  node [
    id 29
    label "wykonywa&#263;"
    origin "text"
  ]
  node [
    id 30
    label "zadanie"
    origin "text"
  ]
  node [
    id 31
    label "dokument"
  ]
  node [
    id 32
    label "resolution"
  ]
  node [
    id 33
    label "zdecydowanie"
  ]
  node [
    id 34
    label "wytw&#243;r"
  ]
  node [
    id 35
    label "podj&#281;cie_decyzji"
  ]
  node [
    id 36
    label "management"
  ]
  node [
    id 37
    label "Goebbels"
  ]
  node [
    id 38
    label "Sto&#322;ypin"
  ]
  node [
    id 39
    label "rz&#261;d"
  ]
  node [
    id 40
    label "dostojnik"
  ]
  node [
    id 41
    label "manewr"
  ]
  node [
    id 42
    label "reakcja"
  ]
  node [
    id 43
    label "auspices"
  ]
  node [
    id 44
    label "mecz"
  ]
  node [
    id 45
    label "poparcie"
  ]
  node [
    id 46
    label "ochrona"
  ]
  node [
    id 47
    label "s&#261;d"
  ]
  node [
    id 48
    label "defensive_structure"
  ]
  node [
    id 49
    label "liga"
  ]
  node [
    id 50
    label "egzamin"
  ]
  node [
    id 51
    label "gracz"
  ]
  node [
    id 52
    label "defense"
  ]
  node [
    id 53
    label "walka"
  ]
  node [
    id 54
    label "post&#281;powanie"
  ]
  node [
    id 55
    label "wojsko"
  ]
  node [
    id 56
    label "protection"
  ]
  node [
    id 57
    label "poj&#281;cie"
  ]
  node [
    id 58
    label "guard_duty"
  ]
  node [
    id 59
    label "strona"
  ]
  node [
    id 60
    label "sp&#243;r"
  ]
  node [
    id 61
    label "gra"
  ]
  node [
    id 62
    label "nacjonalistyczny"
  ]
  node [
    id 63
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 64
    label "narodowo"
  ]
  node [
    id 65
    label "wa&#380;ny"
  ]
  node [
    id 66
    label "s&#322;o&#324;ce"
  ]
  node [
    id 67
    label "czynienie_si&#281;"
  ]
  node [
    id 68
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 69
    label "czas"
  ]
  node [
    id 70
    label "long_time"
  ]
  node [
    id 71
    label "przedpo&#322;udnie"
  ]
  node [
    id 72
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 73
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 74
    label "tydzie&#324;"
  ]
  node [
    id 75
    label "godzina"
  ]
  node [
    id 76
    label "t&#322;usty_czwartek"
  ]
  node [
    id 77
    label "wsta&#263;"
  ]
  node [
    id 78
    label "day"
  ]
  node [
    id 79
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 80
    label "przedwiecz&#243;r"
  ]
  node [
    id 81
    label "Sylwester"
  ]
  node [
    id 82
    label "po&#322;udnie"
  ]
  node [
    id 83
    label "wzej&#347;cie"
  ]
  node [
    id 84
    label "podwiecz&#243;r"
  ]
  node [
    id 85
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 86
    label "rano"
  ]
  node [
    id 87
    label "termin"
  ]
  node [
    id 88
    label "ranek"
  ]
  node [
    id 89
    label "doba"
  ]
  node [
    id 90
    label "wiecz&#243;r"
  ]
  node [
    id 91
    label "walentynki"
  ]
  node [
    id 92
    label "popo&#322;udnie"
  ]
  node [
    id 93
    label "noc"
  ]
  node [
    id 94
    label "wstanie"
  ]
  node [
    id 95
    label "formacja"
  ]
  node [
    id 96
    label "kronika"
  ]
  node [
    id 97
    label "czasopismo"
  ]
  node [
    id 98
    label "yearbook"
  ]
  node [
    id 99
    label "temat"
  ]
  node [
    id 100
    label "kognicja"
  ]
  node [
    id 101
    label "idea"
  ]
  node [
    id 102
    label "szczeg&#243;&#322;"
  ]
  node [
    id 103
    label "rzecz"
  ]
  node [
    id 104
    label "wydarzenie"
  ]
  node [
    id 105
    label "przes&#322;anka"
  ]
  node [
    id 106
    label "rozprawa"
  ]
  node [
    id 107
    label "object"
  ]
  node [
    id 108
    label "proposition"
  ]
  node [
    id 109
    label "extension"
  ]
  node [
    id 110
    label "zmienienie"
  ]
  node [
    id 111
    label "powi&#281;kszenie_si&#281;"
  ]
  node [
    id 112
    label "powi&#281;kszenie"
  ]
  node [
    id 113
    label "wi&#281;kszy"
  ]
  node [
    id 114
    label "&#347;ci&#261;ganie"
  ]
  node [
    id 115
    label "fee"
  ]
  node [
    id 116
    label "uregulowa&#263;"
  ]
  node [
    id 117
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 118
    label "kwota"
  ]
  node [
    id 119
    label "&#347;ci&#261;ga&#263;"
  ]
  node [
    id 120
    label "zagranicznie"
  ]
  node [
    id 121
    label "obcy"
  ]
  node [
    id 122
    label "doj&#347;cie"
  ]
  node [
    id 123
    label "doch&#243;d"
  ]
  node [
    id 124
    label "doj&#347;&#263;"
  ]
  node [
    id 125
    label "przedmiot"
  ]
  node [
    id 126
    label "dochodzenie"
  ]
  node [
    id 127
    label "dziennik"
  ]
  node [
    id 128
    label "aneks"
  ]
  node [
    id 129
    label "element"
  ]
  node [
    id 130
    label "galanteria"
  ]
  node [
    id 131
    label "wytwarza&#263;"
  ]
  node [
    id 132
    label "take"
  ]
  node [
    id 133
    label "dostawa&#263;"
  ]
  node [
    id 134
    label "return"
  ]
  node [
    id 135
    label "cz&#322;owiek"
  ]
  node [
    id 136
    label "demobilizowa&#263;"
  ]
  node [
    id 137
    label "rota"
  ]
  node [
    id 138
    label "walcz&#261;cy"
  ]
  node [
    id 139
    label "demobilizowanie"
  ]
  node [
    id 140
    label "harcap"
  ]
  node [
    id 141
    label "&#380;o&#322;nierstwo"
  ]
  node [
    id 142
    label "&#380;o&#322;dowy"
  ]
  node [
    id 143
    label "zdemobilizowanie"
  ]
  node [
    id 144
    label "elew"
  ]
  node [
    id 145
    label "mundurowy"
  ]
  node [
    id 146
    label "s&#322;u&#380;y&#263;_w_wojsku"
  ]
  node [
    id 147
    label "so&#322;dat"
  ]
  node [
    id 148
    label "s&#322;u&#380;enie_w_wojsku"
  ]
  node [
    id 149
    label "zdemobilizowa&#263;"
  ]
  node [
    id 150
    label "Gurkha"
  ]
  node [
    id 151
    label "formalny"
  ]
  node [
    id 152
    label "zawodowo"
  ]
  node [
    id 153
    label "zawo&#322;any"
  ]
  node [
    id 154
    label "profesjonalny"
  ]
  node [
    id 155
    label "czadowy"
  ]
  node [
    id 156
    label "fajny"
  ]
  node [
    id 157
    label "fachowy"
  ]
  node [
    id 158
    label "s&#322;u&#380;bowo"
  ]
  node [
    id 159
    label "klawy"
  ]
  node [
    id 160
    label "niezawodowo"
  ]
  node [
    id 161
    label "aim"
  ]
  node [
    id 162
    label "okre&#347;li&#263;"
  ]
  node [
    id 163
    label "wybra&#263;"
  ]
  node [
    id 164
    label "sign"
  ]
  node [
    id 165
    label "position"
  ]
  node [
    id 166
    label "zaznaczy&#263;"
  ]
  node [
    id 167
    label "ustali&#263;"
  ]
  node [
    id 168
    label "set"
  ]
  node [
    id 169
    label "service"
  ]
  node [
    id 170
    label "ZOMO"
  ]
  node [
    id 171
    label "czworak"
  ]
  node [
    id 172
    label "zesp&#243;&#322;"
  ]
  node [
    id 173
    label "s&#322;u&#380;&#261;ca"
  ]
  node [
    id 174
    label "instytucja"
  ]
  node [
    id 175
    label "s&#322;u&#380;&#261;cy"
  ]
  node [
    id 176
    label "praca"
  ]
  node [
    id 177
    label "wys&#322;uga"
  ]
  node [
    id 178
    label "mode"
  ]
  node [
    id 179
    label "przesada"
  ]
  node [
    id 180
    label "ustawienie"
  ]
  node [
    id 181
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 182
    label "zakres"
  ]
  node [
    id 183
    label "Ural"
  ]
  node [
    id 184
    label "koniec"
  ]
  node [
    id 185
    label "kres"
  ]
  node [
    id 186
    label "granice"
  ]
  node [
    id 187
    label "granica_pa&#324;stwa"
  ]
  node [
    id 188
    label "pu&#322;ap"
  ]
  node [
    id 189
    label "frontier"
  ]
  node [
    id 190
    label "end"
  ]
  node [
    id 191
    label "miara"
  ]
  node [
    id 192
    label "przej&#347;cie"
  ]
  node [
    id 193
    label "Rwanda"
  ]
  node [
    id 194
    label "Filipiny"
  ]
  node [
    id 195
    label "Monako"
  ]
  node [
    id 196
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 197
    label "Korea"
  ]
  node [
    id 198
    label "Czarnog&#243;ra"
  ]
  node [
    id 199
    label "Ghana"
  ]
  node [
    id 200
    label "Malawi"
  ]
  node [
    id 201
    label "Indonezja"
  ]
  node [
    id 202
    label "Bu&#322;garia"
  ]
  node [
    id 203
    label "Nauru"
  ]
  node [
    id 204
    label "Kenia"
  ]
  node [
    id 205
    label "Kambod&#380;a"
  ]
  node [
    id 206
    label "Mali"
  ]
  node [
    id 207
    label "Austria"
  ]
  node [
    id 208
    label "interior"
  ]
  node [
    id 209
    label "Armenia"
  ]
  node [
    id 210
    label "Fid&#380;i"
  ]
  node [
    id 211
    label "Tuwalu"
  ]
  node [
    id 212
    label "Etiopia"
  ]
  node [
    id 213
    label "Malezja"
  ]
  node [
    id 214
    label "Malta"
  ]
  node [
    id 215
    label "Tad&#380;ykistan"
  ]
  node [
    id 216
    label "Grenada"
  ]
  node [
    id 217
    label "Wehrlen"
  ]
  node [
    id 218
    label "para"
  ]
  node [
    id 219
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 220
    label "Rumunia"
  ]
  node [
    id 221
    label "Maroko"
  ]
  node [
    id 222
    label "Bhutan"
  ]
  node [
    id 223
    label "S&#322;owacja"
  ]
  node [
    id 224
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 225
    label "Seszele"
  ]
  node [
    id 226
    label "Kuwejt"
  ]
  node [
    id 227
    label "Arabia_Saudyjska"
  ]
  node [
    id 228
    label "Kanada"
  ]
  node [
    id 229
    label "Ekwador"
  ]
  node [
    id 230
    label "Japonia"
  ]
  node [
    id 231
    label "ziemia"
  ]
  node [
    id 232
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 233
    label "Hiszpania"
  ]
  node [
    id 234
    label "Wyspy_Marshalla"
  ]
  node [
    id 235
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 236
    label "D&#380;ibuti"
  ]
  node [
    id 237
    label "Botswana"
  ]
  node [
    id 238
    label "grupa"
  ]
  node [
    id 239
    label "Wietnam"
  ]
  node [
    id 240
    label "Egipt"
  ]
  node [
    id 241
    label "Burkina_Faso"
  ]
  node [
    id 242
    label "Niemcy"
  ]
  node [
    id 243
    label "Khitai"
  ]
  node [
    id 244
    label "Macedonia"
  ]
  node [
    id 245
    label "Albania"
  ]
  node [
    id 246
    label "Madagaskar"
  ]
  node [
    id 247
    label "Bahrajn"
  ]
  node [
    id 248
    label "Jemen"
  ]
  node [
    id 249
    label "Lesoto"
  ]
  node [
    id 250
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 251
    label "Samoa"
  ]
  node [
    id 252
    label "Andora"
  ]
  node [
    id 253
    label "Chiny"
  ]
  node [
    id 254
    label "Cypr"
  ]
  node [
    id 255
    label "Wielka_Brytania"
  ]
  node [
    id 256
    label "Ukraina"
  ]
  node [
    id 257
    label "Paragwaj"
  ]
  node [
    id 258
    label "Trynidad_i_Tobago"
  ]
  node [
    id 259
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 260
    label "Libia"
  ]
  node [
    id 261
    label "Surinam"
  ]
  node [
    id 262
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 263
    label "Nigeria"
  ]
  node [
    id 264
    label "Australia"
  ]
  node [
    id 265
    label "Honduras"
  ]
  node [
    id 266
    label "Peru"
  ]
  node [
    id 267
    label "USA"
  ]
  node [
    id 268
    label "Bangladesz"
  ]
  node [
    id 269
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 270
    label "Kazachstan"
  ]
  node [
    id 271
    label "holoarktyka"
  ]
  node [
    id 272
    label "Nepal"
  ]
  node [
    id 273
    label "Sudan"
  ]
  node [
    id 274
    label "Irak"
  ]
  node [
    id 275
    label "San_Marino"
  ]
  node [
    id 276
    label "Burundi"
  ]
  node [
    id 277
    label "Dominikana"
  ]
  node [
    id 278
    label "Komory"
  ]
  node [
    id 279
    label "Gwatemala"
  ]
  node [
    id 280
    label "Antarktis"
  ]
  node [
    id 281
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 282
    label "Brunei"
  ]
  node [
    id 283
    label "Iran"
  ]
  node [
    id 284
    label "Zimbabwe"
  ]
  node [
    id 285
    label "Namibia"
  ]
  node [
    id 286
    label "Meksyk"
  ]
  node [
    id 287
    label "Kamerun"
  ]
  node [
    id 288
    label "zwrot"
  ]
  node [
    id 289
    label "Somalia"
  ]
  node [
    id 290
    label "Angola"
  ]
  node [
    id 291
    label "Gabon"
  ]
  node [
    id 292
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 293
    label "Nowa_Zelandia"
  ]
  node [
    id 294
    label "Mozambik"
  ]
  node [
    id 295
    label "Tunezja"
  ]
  node [
    id 296
    label "Tajwan"
  ]
  node [
    id 297
    label "Liban"
  ]
  node [
    id 298
    label "Jordania"
  ]
  node [
    id 299
    label "Tonga"
  ]
  node [
    id 300
    label "Czad"
  ]
  node [
    id 301
    label "Gwinea"
  ]
  node [
    id 302
    label "Liberia"
  ]
  node [
    id 303
    label "Belize"
  ]
  node [
    id 304
    label "Benin"
  ]
  node [
    id 305
    label "&#321;otwa"
  ]
  node [
    id 306
    label "Syria"
  ]
  node [
    id 307
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 308
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 309
    label "Dominika"
  ]
  node [
    id 310
    label "Antigua_i_Barbuda"
  ]
  node [
    id 311
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 312
    label "Hanower"
  ]
  node [
    id 313
    label "partia"
  ]
  node [
    id 314
    label "Afganistan"
  ]
  node [
    id 315
    label "W&#322;ochy"
  ]
  node [
    id 316
    label "Kiribati"
  ]
  node [
    id 317
    label "Szwajcaria"
  ]
  node [
    id 318
    label "Chorwacja"
  ]
  node [
    id 319
    label "Sahara_Zachodnia"
  ]
  node [
    id 320
    label "Tajlandia"
  ]
  node [
    id 321
    label "Salwador"
  ]
  node [
    id 322
    label "Bahamy"
  ]
  node [
    id 323
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 324
    label "S&#322;owenia"
  ]
  node [
    id 325
    label "Gambia"
  ]
  node [
    id 326
    label "Urugwaj"
  ]
  node [
    id 327
    label "Zair"
  ]
  node [
    id 328
    label "Erytrea"
  ]
  node [
    id 329
    label "Rosja"
  ]
  node [
    id 330
    label "Mauritius"
  ]
  node [
    id 331
    label "Niger"
  ]
  node [
    id 332
    label "Uganda"
  ]
  node [
    id 333
    label "Turkmenistan"
  ]
  node [
    id 334
    label "Turcja"
  ]
  node [
    id 335
    label "Irlandia"
  ]
  node [
    id 336
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 337
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 338
    label "Gwinea_Bissau"
  ]
  node [
    id 339
    label "Belgia"
  ]
  node [
    id 340
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 341
    label "Palau"
  ]
  node [
    id 342
    label "Barbados"
  ]
  node [
    id 343
    label "Wenezuela"
  ]
  node [
    id 344
    label "W&#281;gry"
  ]
  node [
    id 345
    label "Chile"
  ]
  node [
    id 346
    label "Argentyna"
  ]
  node [
    id 347
    label "Kolumbia"
  ]
  node [
    id 348
    label "Sierra_Leone"
  ]
  node [
    id 349
    label "Azerbejd&#380;an"
  ]
  node [
    id 350
    label "Kongo"
  ]
  node [
    id 351
    label "Pakistan"
  ]
  node [
    id 352
    label "Liechtenstein"
  ]
  node [
    id 353
    label "Nikaragua"
  ]
  node [
    id 354
    label "Senegal"
  ]
  node [
    id 355
    label "Indie"
  ]
  node [
    id 356
    label "Suazi"
  ]
  node [
    id 357
    label "Polska"
  ]
  node [
    id 358
    label "Algieria"
  ]
  node [
    id 359
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 360
    label "terytorium"
  ]
  node [
    id 361
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 362
    label "Jamajka"
  ]
  node [
    id 363
    label "Kostaryka"
  ]
  node [
    id 364
    label "Timor_Wschodni"
  ]
  node [
    id 365
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 366
    label "Kuba"
  ]
  node [
    id 367
    label "Mauretania"
  ]
  node [
    id 368
    label "Portoryko"
  ]
  node [
    id 369
    label "Brazylia"
  ]
  node [
    id 370
    label "Mo&#322;dawia"
  ]
  node [
    id 371
    label "organizacja"
  ]
  node [
    id 372
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 373
    label "Litwa"
  ]
  node [
    id 374
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 375
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 376
    label "Izrael"
  ]
  node [
    id 377
    label "Grecja"
  ]
  node [
    id 378
    label "Kirgistan"
  ]
  node [
    id 379
    label "Holandia"
  ]
  node [
    id 380
    label "Sri_Lanka"
  ]
  node [
    id 381
    label "Katar"
  ]
  node [
    id 382
    label "Mikronezja"
  ]
  node [
    id 383
    label "Laos"
  ]
  node [
    id 384
    label "Mongolia"
  ]
  node [
    id 385
    label "Malediwy"
  ]
  node [
    id 386
    label "Zambia"
  ]
  node [
    id 387
    label "Tanzania"
  ]
  node [
    id 388
    label "Gujana"
  ]
  node [
    id 389
    label "Uzbekistan"
  ]
  node [
    id 390
    label "Panama"
  ]
  node [
    id 391
    label "Czechy"
  ]
  node [
    id 392
    label "Gruzja"
  ]
  node [
    id 393
    label "Serbia"
  ]
  node [
    id 394
    label "Francja"
  ]
  node [
    id 395
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 396
    label "Togo"
  ]
  node [
    id 397
    label "Estonia"
  ]
  node [
    id 398
    label "Boliwia"
  ]
  node [
    id 399
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 400
    label "Oman"
  ]
  node [
    id 401
    label "Wyspy_Salomona"
  ]
  node [
    id 402
    label "Haiti"
  ]
  node [
    id 403
    label "Luksemburg"
  ]
  node [
    id 404
    label "Portugalia"
  ]
  node [
    id 405
    label "Birma"
  ]
  node [
    id 406
    label "Rodezja"
  ]
  node [
    id 407
    label "cz&#322;owiek_pracy"
  ]
  node [
    id 408
    label "delegowa&#263;"
  ]
  node [
    id 409
    label "s&#322;u&#380;ba_cywilna"
  ]
  node [
    id 410
    label "pracu&#347;"
  ]
  node [
    id 411
    label "delegowanie"
  ]
  node [
    id 412
    label "r&#281;ka"
  ]
  node [
    id 413
    label "salariat"
  ]
  node [
    id 414
    label "infimum"
  ]
  node [
    id 415
    label "ewoluowanie"
  ]
  node [
    id 416
    label "przyswoi&#263;"
  ]
  node [
    id 417
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 418
    label "wyewoluowanie"
  ]
  node [
    id 419
    label "individual"
  ]
  node [
    id 420
    label "profanum"
  ]
  node [
    id 421
    label "starzenie_si&#281;"
  ]
  node [
    id 422
    label "homo_sapiens"
  ]
  node [
    id 423
    label "skala"
  ]
  node [
    id 424
    label "supremum"
  ]
  node [
    id 425
    label "przyswaja&#263;"
  ]
  node [
    id 426
    label "ludzko&#347;&#263;"
  ]
  node [
    id 427
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 428
    label "one"
  ]
  node [
    id 429
    label "funkcja"
  ]
  node [
    id 430
    label "przeliczenie"
  ]
  node [
    id 431
    label "przeliczanie"
  ]
  node [
    id 432
    label "mikrokosmos"
  ]
  node [
    id 433
    label "rzut"
  ]
  node [
    id 434
    label "portrecista"
  ]
  node [
    id 435
    label "przelicza&#263;"
  ]
  node [
    id 436
    label "przyswajanie"
  ]
  node [
    id 437
    label "duch"
  ]
  node [
    id 438
    label "wyewoluowa&#263;"
  ]
  node [
    id 439
    label "ewoluowa&#263;"
  ]
  node [
    id 440
    label "oddzia&#322;ywanie"
  ]
  node [
    id 441
    label "g&#322;owa"
  ]
  node [
    id 442
    label "liczba_naturalna"
  ]
  node [
    id 443
    label "osoba"
  ]
  node [
    id 444
    label "figura"
  ]
  node [
    id 445
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 446
    label "obiekt"
  ]
  node [
    id 447
    label "matematyka"
  ]
  node [
    id 448
    label "przyswojenie"
  ]
  node [
    id 449
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 450
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 451
    label "czynnik_biotyczny"
  ]
  node [
    id 452
    label "przeliczy&#263;"
  ]
  node [
    id 453
    label "antropochoria"
  ]
  node [
    id 454
    label "militarnie"
  ]
  node [
    id 455
    label "typowy"
  ]
  node [
    id 456
    label "antybalistyczny"
  ]
  node [
    id 457
    label "specjalny"
  ]
  node [
    id 458
    label "podleg&#322;y"
  ]
  node [
    id 459
    label "wojskowo"
  ]
  node [
    id 460
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 461
    label "work"
  ]
  node [
    id 462
    label "robi&#263;"
  ]
  node [
    id 463
    label "muzyka"
  ]
  node [
    id 464
    label "rola"
  ]
  node [
    id 465
    label "create"
  ]
  node [
    id 466
    label "yield"
  ]
  node [
    id 467
    label "czynno&#347;&#263;"
  ]
  node [
    id 468
    label "problem"
  ]
  node [
    id 469
    label "przepisanie"
  ]
  node [
    id 470
    label "przepisa&#263;"
  ]
  node [
    id 471
    label "za&#322;o&#380;enie"
  ]
  node [
    id 472
    label "nakarmienie"
  ]
  node [
    id 473
    label "duty"
  ]
  node [
    id 474
    label "zbi&#243;r"
  ]
  node [
    id 475
    label "powierzanie"
  ]
  node [
    id 476
    label "zaszkodzenie"
  ]
  node [
    id 477
    label "d&#378;wigni&#281;cie"
  ]
  node [
    id 478
    label "zaj&#281;cie"
  ]
  node [
    id 479
    label "zobowi&#261;zanie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 13
  ]
  edge [
    source 11
    target 120
  ]
  edge [
    source 11
    target 121
  ]
  edge [
    source 12
    target 122
  ]
  edge [
    source 12
    target 123
  ]
  edge [
    source 12
    target 124
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 131
  ]
  edge [
    source 13
    target 132
  ]
  edge [
    source 13
    target 133
  ]
  edge [
    source 13
    target 134
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 15
    target 135
  ]
  edge [
    source 15
    target 136
  ]
  edge [
    source 15
    target 137
  ]
  edge [
    source 15
    target 138
  ]
  edge [
    source 15
    target 139
  ]
  edge [
    source 15
    target 140
  ]
  edge [
    source 15
    target 141
  ]
  edge [
    source 15
    target 142
  ]
  edge [
    source 15
    target 143
  ]
  edge [
    source 15
    target 144
  ]
  edge [
    source 15
    target 145
  ]
  edge [
    source 15
    target 146
  ]
  edge [
    source 15
    target 147
  ]
  edge [
    source 15
    target 55
  ]
  edge [
    source 15
    target 148
  ]
  edge [
    source 15
    target 149
  ]
  edge [
    source 15
    target 150
  ]
  edge [
    source 15
    target 18
  ]
  edge [
    source 16
    target 151
  ]
  edge [
    source 16
    target 152
  ]
  edge [
    source 16
    target 153
  ]
  edge [
    source 16
    target 154
  ]
  edge [
    source 16
    target 155
  ]
  edge [
    source 16
    target 156
  ]
  edge [
    source 16
    target 157
  ]
  edge [
    source 16
    target 158
  ]
  edge [
    source 16
    target 159
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 160
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 161
  ]
  edge [
    source 18
    target 162
  ]
  edge [
    source 18
    target 163
  ]
  edge [
    source 18
    target 164
  ]
  edge [
    source 18
    target 165
  ]
  edge [
    source 18
    target 166
  ]
  edge [
    source 18
    target 167
  ]
  edge [
    source 18
    target 168
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 169
  ]
  edge [
    source 20
    target 170
  ]
  edge [
    source 20
    target 171
  ]
  edge [
    source 20
    target 172
  ]
  edge [
    source 20
    target 173
  ]
  edge [
    source 20
    target 174
  ]
  edge [
    source 20
    target 175
  ]
  edge [
    source 20
    target 176
  ]
  edge [
    source 20
    target 177
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 30
  ]
  edge [
    source 21
    target 178
  ]
  edge [
    source 21
    target 61
  ]
  edge [
    source 21
    target 179
  ]
  edge [
    source 21
    target 180
  ]
  edge [
    source 21
    target 181
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 182
  ]
  edge [
    source 22
    target 183
  ]
  edge [
    source 22
    target 184
  ]
  edge [
    source 22
    target 185
  ]
  edge [
    source 22
    target 186
  ]
  edge [
    source 22
    target 187
  ]
  edge [
    source 22
    target 188
  ]
  edge [
    source 22
    target 189
  ]
  edge [
    source 22
    target 190
  ]
  edge [
    source 22
    target 191
  ]
  edge [
    source 22
    target 57
  ]
  edge [
    source 22
    target 192
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 193
  ]
  edge [
    source 23
    target 194
  ]
  edge [
    source 23
    target 195
  ]
  edge [
    source 23
    target 196
  ]
  edge [
    source 23
    target 197
  ]
  edge [
    source 23
    target 198
  ]
  edge [
    source 23
    target 199
  ]
  edge [
    source 23
    target 200
  ]
  edge [
    source 23
    target 201
  ]
  edge [
    source 23
    target 202
  ]
  edge [
    source 23
    target 203
  ]
  edge [
    source 23
    target 204
  ]
  edge [
    source 23
    target 205
  ]
  edge [
    source 23
    target 206
  ]
  edge [
    source 23
    target 207
  ]
  edge [
    source 23
    target 208
  ]
  edge [
    source 23
    target 209
  ]
  edge [
    source 23
    target 210
  ]
  edge [
    source 23
    target 211
  ]
  edge [
    source 23
    target 212
  ]
  edge [
    source 23
    target 213
  ]
  edge [
    source 23
    target 214
  ]
  edge [
    source 23
    target 215
  ]
  edge [
    source 23
    target 216
  ]
  edge [
    source 23
    target 217
  ]
  edge [
    source 23
    target 218
  ]
  edge [
    source 23
    target 219
  ]
  edge [
    source 23
    target 220
  ]
  edge [
    source 23
    target 221
  ]
  edge [
    source 23
    target 222
  ]
  edge [
    source 23
    target 223
  ]
  edge [
    source 23
    target 224
  ]
  edge [
    source 23
    target 225
  ]
  edge [
    source 23
    target 226
  ]
  edge [
    source 23
    target 227
  ]
  edge [
    source 23
    target 228
  ]
  edge [
    source 23
    target 229
  ]
  edge [
    source 23
    target 230
  ]
  edge [
    source 23
    target 231
  ]
  edge [
    source 23
    target 232
  ]
  edge [
    source 23
    target 233
  ]
  edge [
    source 23
    target 234
  ]
  edge [
    source 23
    target 235
  ]
  edge [
    source 23
    target 236
  ]
  edge [
    source 23
    target 237
  ]
  edge [
    source 23
    target 238
  ]
  edge [
    source 23
    target 239
  ]
  edge [
    source 23
    target 240
  ]
  edge [
    source 23
    target 241
  ]
  edge [
    source 23
    target 242
  ]
  edge [
    source 23
    target 243
  ]
  edge [
    source 23
    target 244
  ]
  edge [
    source 23
    target 245
  ]
  edge [
    source 23
    target 246
  ]
  edge [
    source 23
    target 247
  ]
  edge [
    source 23
    target 248
  ]
  edge [
    source 23
    target 249
  ]
  edge [
    source 23
    target 250
  ]
  edge [
    source 23
    target 251
  ]
  edge [
    source 23
    target 252
  ]
  edge [
    source 23
    target 253
  ]
  edge [
    source 23
    target 254
  ]
  edge [
    source 23
    target 255
  ]
  edge [
    source 23
    target 256
  ]
  edge [
    source 23
    target 257
  ]
  edge [
    source 23
    target 258
  ]
  edge [
    source 23
    target 259
  ]
  edge [
    source 23
    target 260
  ]
  edge [
    source 23
    target 261
  ]
  edge [
    source 23
    target 262
  ]
  edge [
    source 23
    target 263
  ]
  edge [
    source 23
    target 264
  ]
  edge [
    source 23
    target 265
  ]
  edge [
    source 23
    target 266
  ]
  edge [
    source 23
    target 267
  ]
  edge [
    source 23
    target 268
  ]
  edge [
    source 23
    target 269
  ]
  edge [
    source 23
    target 270
  ]
  edge [
    source 23
    target 271
  ]
  edge [
    source 23
    target 272
  ]
  edge [
    source 23
    target 273
  ]
  edge [
    source 23
    target 274
  ]
  edge [
    source 23
    target 275
  ]
  edge [
    source 23
    target 276
  ]
  edge [
    source 23
    target 277
  ]
  edge [
    source 23
    target 278
  ]
  edge [
    source 23
    target 187
  ]
  edge [
    source 23
    target 279
  ]
  edge [
    source 23
    target 280
  ]
  edge [
    source 23
    target 281
  ]
  edge [
    source 23
    target 282
  ]
  edge [
    source 23
    target 283
  ]
  edge [
    source 23
    target 284
  ]
  edge [
    source 23
    target 285
  ]
  edge [
    source 23
    target 286
  ]
  edge [
    source 23
    target 287
  ]
  edge [
    source 23
    target 288
  ]
  edge [
    source 23
    target 289
  ]
  edge [
    source 23
    target 290
  ]
  edge [
    source 23
    target 291
  ]
  edge [
    source 23
    target 292
  ]
  edge [
    source 23
    target 293
  ]
  edge [
    source 23
    target 294
  ]
  edge [
    source 23
    target 295
  ]
  edge [
    source 23
    target 296
  ]
  edge [
    source 23
    target 297
  ]
  edge [
    source 23
    target 298
  ]
  edge [
    source 23
    target 299
  ]
  edge [
    source 23
    target 300
  ]
  edge [
    source 23
    target 301
  ]
  edge [
    source 23
    target 302
  ]
  edge [
    source 23
    target 303
  ]
  edge [
    source 23
    target 304
  ]
  edge [
    source 23
    target 305
  ]
  edge [
    source 23
    target 306
  ]
  edge [
    source 23
    target 307
  ]
  edge [
    source 23
    target 308
  ]
  edge [
    source 23
    target 309
  ]
  edge [
    source 23
    target 310
  ]
  edge [
    source 23
    target 311
  ]
  edge [
    source 23
    target 312
  ]
  edge [
    source 23
    target 313
  ]
  edge [
    source 23
    target 314
  ]
  edge [
    source 23
    target 315
  ]
  edge [
    source 23
    target 316
  ]
  edge [
    source 23
    target 317
  ]
  edge [
    source 23
    target 318
  ]
  edge [
    source 23
    target 319
  ]
  edge [
    source 23
    target 320
  ]
  edge [
    source 23
    target 321
  ]
  edge [
    source 23
    target 322
  ]
  edge [
    source 23
    target 323
  ]
  edge [
    source 23
    target 324
  ]
  edge [
    source 23
    target 325
  ]
  edge [
    source 23
    target 326
  ]
  edge [
    source 23
    target 327
  ]
  edge [
    source 23
    target 328
  ]
  edge [
    source 23
    target 329
  ]
  edge [
    source 23
    target 330
  ]
  edge [
    source 23
    target 331
  ]
  edge [
    source 23
    target 332
  ]
  edge [
    source 23
    target 333
  ]
  edge [
    source 23
    target 334
  ]
  edge [
    source 23
    target 335
  ]
  edge [
    source 23
    target 336
  ]
  edge [
    source 23
    target 337
  ]
  edge [
    source 23
    target 338
  ]
  edge [
    source 23
    target 339
  ]
  edge [
    source 23
    target 340
  ]
  edge [
    source 23
    target 341
  ]
  edge [
    source 23
    target 342
  ]
  edge [
    source 23
    target 343
  ]
  edge [
    source 23
    target 344
  ]
  edge [
    source 23
    target 345
  ]
  edge [
    source 23
    target 346
  ]
  edge [
    source 23
    target 347
  ]
  edge [
    source 23
    target 348
  ]
  edge [
    source 23
    target 349
  ]
  edge [
    source 23
    target 350
  ]
  edge [
    source 23
    target 351
  ]
  edge [
    source 23
    target 352
  ]
  edge [
    source 23
    target 353
  ]
  edge [
    source 23
    target 354
  ]
  edge [
    source 23
    target 355
  ]
  edge [
    source 23
    target 356
  ]
  edge [
    source 23
    target 357
  ]
  edge [
    source 23
    target 358
  ]
  edge [
    source 23
    target 359
  ]
  edge [
    source 23
    target 360
  ]
  edge [
    source 23
    target 361
  ]
  edge [
    source 23
    target 362
  ]
  edge [
    source 23
    target 363
  ]
  edge [
    source 23
    target 364
  ]
  edge [
    source 23
    target 365
  ]
  edge [
    source 23
    target 366
  ]
  edge [
    source 23
    target 367
  ]
  edge [
    source 23
    target 368
  ]
  edge [
    source 23
    target 369
  ]
  edge [
    source 23
    target 370
  ]
  edge [
    source 23
    target 371
  ]
  edge [
    source 23
    target 372
  ]
  edge [
    source 23
    target 373
  ]
  edge [
    source 23
    target 374
  ]
  edge [
    source 23
    target 375
  ]
  edge [
    source 23
    target 376
  ]
  edge [
    source 23
    target 377
  ]
  edge [
    source 23
    target 378
  ]
  edge [
    source 23
    target 379
  ]
  edge [
    source 23
    target 380
  ]
  edge [
    source 23
    target 381
  ]
  edge [
    source 23
    target 382
  ]
  edge [
    source 23
    target 383
  ]
  edge [
    source 23
    target 384
  ]
  edge [
    source 23
    target 385
  ]
  edge [
    source 23
    target 386
  ]
  edge [
    source 23
    target 387
  ]
  edge [
    source 23
    target 388
  ]
  edge [
    source 23
    target 389
  ]
  edge [
    source 23
    target 390
  ]
  edge [
    source 23
    target 391
  ]
  edge [
    source 23
    target 392
  ]
  edge [
    source 23
    target 393
  ]
  edge [
    source 23
    target 394
  ]
  edge [
    source 23
    target 395
  ]
  edge [
    source 23
    target 396
  ]
  edge [
    source 23
    target 397
  ]
  edge [
    source 23
    target 398
  ]
  edge [
    source 23
    target 399
  ]
  edge [
    source 23
    target 400
  ]
  edge [
    source 23
    target 401
  ]
  edge [
    source 23
    target 402
  ]
  edge [
    source 23
    target 403
  ]
  edge [
    source 23
    target 404
  ]
  edge [
    source 23
    target 405
  ]
  edge [
    source 23
    target 406
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 407
  ]
  edge [
    source 24
    target 135
  ]
  edge [
    source 24
    target 408
  ]
  edge [
    source 24
    target 409
  ]
  edge [
    source 24
    target 410
  ]
  edge [
    source 24
    target 411
  ]
  edge [
    source 24
    target 412
  ]
  edge [
    source 24
    target 413
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 414
  ]
  edge [
    source 27
    target 415
  ]
  edge [
    source 27
    target 416
  ]
  edge [
    source 27
    target 42
  ]
  edge [
    source 27
    target 417
  ]
  edge [
    source 27
    target 418
  ]
  edge [
    source 27
    target 419
  ]
  edge [
    source 27
    target 420
  ]
  edge [
    source 27
    target 421
  ]
  edge [
    source 27
    target 422
  ]
  edge [
    source 27
    target 423
  ]
  edge [
    source 27
    target 424
  ]
  edge [
    source 27
    target 425
  ]
  edge [
    source 27
    target 426
  ]
  edge [
    source 27
    target 427
  ]
  edge [
    source 27
    target 428
  ]
  edge [
    source 27
    target 429
  ]
  edge [
    source 27
    target 430
  ]
  edge [
    source 27
    target 431
  ]
  edge [
    source 27
    target 432
  ]
  edge [
    source 27
    target 433
  ]
  edge [
    source 27
    target 434
  ]
  edge [
    source 27
    target 435
  ]
  edge [
    source 27
    target 436
  ]
  edge [
    source 27
    target 437
  ]
  edge [
    source 27
    target 438
  ]
  edge [
    source 27
    target 439
  ]
  edge [
    source 27
    target 440
  ]
  edge [
    source 27
    target 441
  ]
  edge [
    source 27
    target 442
  ]
  edge [
    source 27
    target 57
  ]
  edge [
    source 27
    target 443
  ]
  edge [
    source 27
    target 444
  ]
  edge [
    source 27
    target 445
  ]
  edge [
    source 27
    target 446
  ]
  edge [
    source 27
    target 447
  ]
  edge [
    source 27
    target 448
  ]
  edge [
    source 27
    target 449
  ]
  edge [
    source 27
    target 450
  ]
  edge [
    source 27
    target 451
  ]
  edge [
    source 27
    target 452
  ]
  edge [
    source 27
    target 453
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 135
  ]
  edge [
    source 28
    target 137
  ]
  edge [
    source 28
    target 143
  ]
  edge [
    source 28
    target 454
  ]
  edge [
    source 28
    target 146
  ]
  edge [
    source 28
    target 150
  ]
  edge [
    source 28
    target 139
  ]
  edge [
    source 28
    target 138
  ]
  edge [
    source 28
    target 140
  ]
  edge [
    source 28
    target 142
  ]
  edge [
    source 28
    target 145
  ]
  edge [
    source 28
    target 148
  ]
  edge [
    source 28
    target 149
  ]
  edge [
    source 28
    target 455
  ]
  edge [
    source 28
    target 456
  ]
  edge [
    source 28
    target 457
  ]
  edge [
    source 28
    target 458
  ]
  edge [
    source 28
    target 141
  ]
  edge [
    source 28
    target 144
  ]
  edge [
    source 28
    target 147
  ]
  edge [
    source 28
    target 55
  ]
  edge [
    source 28
    target 459
  ]
  edge [
    source 28
    target 136
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 460
  ]
  edge [
    source 29
    target 461
  ]
  edge [
    source 29
    target 462
  ]
  edge [
    source 29
    target 463
  ]
  edge [
    source 29
    target 464
  ]
  edge [
    source 29
    target 465
  ]
  edge [
    source 29
    target 131
  ]
  edge [
    source 29
    target 176
  ]
  edge [
    source 30
    target 466
  ]
  edge [
    source 30
    target 467
  ]
  edge [
    source 30
    target 468
  ]
  edge [
    source 30
    target 469
  ]
  edge [
    source 30
    target 470
  ]
  edge [
    source 30
    target 471
  ]
  edge [
    source 30
    target 461
  ]
  edge [
    source 30
    target 472
  ]
  edge [
    source 30
    target 473
  ]
  edge [
    source 30
    target 474
  ]
  edge [
    source 30
    target 475
  ]
  edge [
    source 30
    target 476
  ]
  edge [
    source 30
    target 477
  ]
  edge [
    source 30
    target 478
  ]
  edge [
    source 30
    target 479
  ]
]
