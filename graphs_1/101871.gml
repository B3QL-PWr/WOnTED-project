graph [
  maxDegree 596
  minDegree 1
  meanDegree 2.131725417439703
  density 0.001979317936341414
  graphCliqueNumber 3
  node [
    id 0
    label "poniedzia&#322;ek"
    origin "text"
  ]
  node [
    id 1
    label "wojew&#243;dztwo"
    origin "text"
  ]
  node [
    id 2
    label "&#322;&#243;dzkie"
    origin "text"
  ]
  node [
    id 3
    label "trwa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "ferie"
    origin "text"
  ]
  node [
    id 5
    label "zimowy"
    origin "text"
  ]
  node [
    id 6
    label "dla"
    origin "text"
  ]
  node [
    id 7
    label "dziecko"
    origin "text"
  ]
  node [
    id 8
    label "m&#322;odzie&#380;"
    origin "text"
  ]
  node [
    id 9
    label "tym"
    origin "text"
  ]
  node [
    id 10
    label "okres"
    origin "text"
  ]
  node [
    id 11
    label "terenia"
    origin "text"
  ]
  node [
    id 12
    label "miasto"
    origin "text"
  ]
  node [
    id 13
    label "ozorkowy"
    origin "text"
  ]
  node [
    id 14
    label "organizowa&#263;"
    origin "text"
  ]
  node [
    id 15
    label "by&#263;"
    origin "text"
  ]
  node [
    id 16
    label "r&#243;&#380;ny"
    origin "text"
  ]
  node [
    id 17
    label "typ"
    origin "text"
  ]
  node [
    id 18
    label "zaj&#281;cia"
    origin "text"
  ]
  node [
    id 19
    label "sportowy"
    origin "text"
  ]
  node [
    id 20
    label "kulturalny"
    origin "text"
  ]
  node [
    id 21
    label "przestawia&#263;"
    origin "text"
  ]
  node [
    id 22
    label "skr&#243;t"
    origin "text"
  ]
  node [
    id 23
    label "c&#243;&#380;"
    origin "text"
  ]
  node [
    id 24
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 25
    label "robi&#263;"
    origin "text"
  ]
  node [
    id 26
    label "pierwsza"
    origin "text"
  ]
  node [
    id 27
    label "tydzie&#324;"
    origin "text"
  ]
  node [
    id 28
    label "pi&#261;tek"
    origin "text"
  ]
  node [
    id 29
    label "gimnazjum"
    origin "text"
  ]
  node [
    id 30
    label "udost&#281;pni&#263;"
    origin "text"
  ]
  node [
    id 31
    label "darmo"
    origin "text"
  ]
  node [
    id 32
    label "si&#322;ownia"
    origin "text"
  ]
  node [
    id 33
    label "hala"
    origin "text"
  ]
  node [
    id 34
    label "pogra&#263;"
    origin "text"
  ]
  node [
    id 35
    label "siatka"
    origin "text"
  ]
  node [
    id 36
    label "lub"
    origin "text"
  ]
  node [
    id 37
    label "unihocka"
    origin "text"
  ]
  node [
    id 38
    label "odby&#263;"
    origin "text"
  ]
  node [
    id 39
    label "si&#281;"
    origin "text"
  ]
  node [
    id 40
    label "turniej"
    origin "text"
  ]
  node [
    id 41
    label "amator"
    origin "text"
  ]
  node [
    id 42
    label "siatk&#243;wka"
    origin "text"
  ]
  node [
    id 43
    label "tr&#243;jka"
    origin "text"
  ]
  node [
    id 44
    label "godzina"
    origin "text"
  ]
  node [
    id 45
    label "basen"
    origin "text"
  ]
  node [
    id 46
    label "sauna"
    origin "text"
  ]
  node [
    id 47
    label "mok"
    origin "text"
  ]
  node [
    id 48
    label "koncert"
    origin "text"
  ]
  node [
    id 49
    label "rockowy"
    origin "text"
  ]
  node [
    id 50
    label "ponadto"
    origin "text"
  ]
  node [
    id 51
    label "czynny"
    origin "text"
  ]
  node [
    id 52
    label "kafejka"
    origin "text"
  ]
  node [
    id 53
    label "internetowy"
    origin "text"
  ]
  node [
    id 54
    label "mdk"
    origin "text"
  ]
  node [
    id 55
    label "r&#243;&#380;norodny"
    origin "text"
  ]
  node [
    id 56
    label "kino"
    origin "text"
  ]
  node [
    id 57
    label "te&#380;"
    origin "text"
  ]
  node [
    id 58
    label "pochodzi&#263;"
    origin "text"
  ]
  node [
    id 59
    label "zapisa&#263;"
    origin "text"
  ]
  node [
    id 60
    label "hufiec"
    origin "text"
  ]
  node [
    id 61
    label "bawi&#263;"
    origin "text"
  ]
  node [
    id 62
    label "razem"
    origin "text"
  ]
  node [
    id 63
    label "harcerz"
    origin "text"
  ]
  node [
    id 64
    label "dawny"
    origin "text"
  ]
  node [
    id 65
    label "dzie&#324;_powszedni"
  ]
  node [
    id 66
    label "Poniedzia&#322;ek_Wielkonocny"
  ]
  node [
    id 67
    label "jednostka_administracyjna"
  ]
  node [
    id 68
    label "makroregion"
  ]
  node [
    id 69
    label "powiat"
  ]
  node [
    id 70
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 71
    label "mikroregion"
  ]
  node [
    id 72
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 73
    label "pa&#324;stwo"
  ]
  node [
    id 74
    label "zostawa&#263;"
  ]
  node [
    id 75
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 76
    label "pozostawa&#263;"
  ]
  node [
    id 77
    label "stand"
  ]
  node [
    id 78
    label "adhere"
  ]
  node [
    id 79
    label "istnie&#263;"
  ]
  node [
    id 80
    label "czas_wolny"
  ]
  node [
    id 81
    label "rok_akademicki"
  ]
  node [
    id 82
    label "rok_szkolny"
  ]
  node [
    id 83
    label "sezonowy"
  ]
  node [
    id 84
    label "zimowo"
  ]
  node [
    id 85
    label "ch&#322;odny"
  ]
  node [
    id 86
    label "typowy"
  ]
  node [
    id 87
    label "hibernowy"
  ]
  node [
    id 88
    label "cz&#322;owiek"
  ]
  node [
    id 89
    label "potomstwo"
  ]
  node [
    id 90
    label "organizm"
  ]
  node [
    id 91
    label "sraluch"
  ]
  node [
    id 92
    label "utulanie"
  ]
  node [
    id 93
    label "pediatra"
  ]
  node [
    id 94
    label "dzieciarnia"
  ]
  node [
    id 95
    label "m&#322;odziak"
  ]
  node [
    id 96
    label "dzieciak"
  ]
  node [
    id 97
    label "utula&#263;"
  ]
  node [
    id 98
    label "potomek"
  ]
  node [
    id 99
    label "entliczek-pentliczek"
  ]
  node [
    id 100
    label "pedofil"
  ]
  node [
    id 101
    label "m&#322;odzik"
  ]
  node [
    id 102
    label "cz&#322;owieczek"
  ]
  node [
    id 103
    label "zwierz&#281;"
  ]
  node [
    id 104
    label "niepe&#322;noletni"
  ]
  node [
    id 105
    label "fledgling"
  ]
  node [
    id 106
    label "utuli&#263;"
  ]
  node [
    id 107
    label "utulenie"
  ]
  node [
    id 108
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 109
    label "smarkateria"
  ]
  node [
    id 110
    label "paleogen"
  ]
  node [
    id 111
    label "spell"
  ]
  node [
    id 112
    label "czas"
  ]
  node [
    id 113
    label "period"
  ]
  node [
    id 114
    label "prekambr"
  ]
  node [
    id 115
    label "jura"
  ]
  node [
    id 116
    label "interstadia&#322;"
  ]
  node [
    id 117
    label "jednostka_geologiczna"
  ]
  node [
    id 118
    label "izochronizm"
  ]
  node [
    id 119
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 120
    label "okres_noachijski"
  ]
  node [
    id 121
    label "orosir"
  ]
  node [
    id 122
    label "kreda"
  ]
  node [
    id 123
    label "sten"
  ]
  node [
    id 124
    label "drugorz&#281;d"
  ]
  node [
    id 125
    label "semester"
  ]
  node [
    id 126
    label "trzeciorz&#281;d"
  ]
  node [
    id 127
    label "ton"
  ]
  node [
    id 128
    label "dzieje"
  ]
  node [
    id 129
    label "poprzednik"
  ]
  node [
    id 130
    label "ordowik"
  ]
  node [
    id 131
    label "karbon"
  ]
  node [
    id 132
    label "trias"
  ]
  node [
    id 133
    label "kalim"
  ]
  node [
    id 134
    label "stater"
  ]
  node [
    id 135
    label "era"
  ]
  node [
    id 136
    label "cykl"
  ]
  node [
    id 137
    label "p&#243;&#322;okres"
  ]
  node [
    id 138
    label "czwartorz&#281;d"
  ]
  node [
    id 139
    label "pulsacja"
  ]
  node [
    id 140
    label "okres_amazo&#324;ski"
  ]
  node [
    id 141
    label "kambr"
  ]
  node [
    id 142
    label "Zeitgeist"
  ]
  node [
    id 143
    label "nast&#281;pnik"
  ]
  node [
    id 144
    label "kriogen"
  ]
  node [
    id 145
    label "glacja&#322;"
  ]
  node [
    id 146
    label "fala"
  ]
  node [
    id 147
    label "okres_czasu"
  ]
  node [
    id 148
    label "riak"
  ]
  node [
    id 149
    label "schy&#322;ek"
  ]
  node [
    id 150
    label "okres_hesperyjski"
  ]
  node [
    id 151
    label "sylur"
  ]
  node [
    id 152
    label "dewon"
  ]
  node [
    id 153
    label "ciota"
  ]
  node [
    id 154
    label "epoka"
  ]
  node [
    id 155
    label "pierwszorz&#281;d"
  ]
  node [
    id 156
    label "okres_halsztacki"
  ]
  node [
    id 157
    label "ektas"
  ]
  node [
    id 158
    label "zdanie"
  ]
  node [
    id 159
    label "condition"
  ]
  node [
    id 160
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 161
    label "wapie&#324;_muszlowy"
  ]
  node [
    id 162
    label "postglacja&#322;"
  ]
  node [
    id 163
    label "faza"
  ]
  node [
    id 164
    label "proces_fizjologiczny"
  ]
  node [
    id 165
    label "ediakar"
  ]
  node [
    id 166
    label "time_period"
  ]
  node [
    id 167
    label "ruch_drgaj&#261;cy"
  ]
  node [
    id 168
    label "perm"
  ]
  node [
    id 169
    label "neogen"
  ]
  node [
    id 170
    label "sider"
  ]
  node [
    id 171
    label "flow"
  ]
  node [
    id 172
    label "podokres"
  ]
  node [
    id 173
    label "preglacja&#322;"
  ]
  node [
    id 174
    label "retoryka"
  ]
  node [
    id 175
    label "choroba_przyrodzona"
  ]
  node [
    id 176
    label "Brac&#322;aw"
  ]
  node [
    id 177
    label "Zb&#261;szy&#324;"
  ]
  node [
    id 178
    label "G&#322;uch&#243;w"
  ]
  node [
    id 179
    label "Hallstatt"
  ]
  node [
    id 180
    label "Zbara&#380;"
  ]
  node [
    id 181
    label "Wielkie_Tyrnowo"
  ]
  node [
    id 182
    label "Nachiczewan"
  ]
  node [
    id 183
    label "Suworow"
  ]
  node [
    id 184
    label "Halicz"
  ]
  node [
    id 185
    label "Gandawa"
  ]
  node [
    id 186
    label "Kamionka_Strumi&#322;owa"
  ]
  node [
    id 187
    label "Wismar"
  ]
  node [
    id 188
    label "Norymberga"
  ]
  node [
    id 189
    label "Ruciane-Nida"
  ]
  node [
    id 190
    label "Wia&#378;ma"
  ]
  node [
    id 191
    label "Sewilla"
  ]
  node [
    id 192
    label "Pietropaw&#322;owsk"
  ]
  node [
    id 193
    label "Kobry&#324;"
  ]
  node [
    id 194
    label "Brno"
  ]
  node [
    id 195
    label "Tomsk"
  ]
  node [
    id 196
    label "Poniatowa"
  ]
  node [
    id 197
    label "Hadziacz"
  ]
  node [
    id 198
    label "Tiume&#324;"
  ]
  node [
    id 199
    label "Karlsbad"
  ]
  node [
    id 200
    label "Drohobycz"
  ]
  node [
    id 201
    label "Lyon"
  ]
  node [
    id 202
    label "Dzier&#380;y&#324;sk"
  ]
  node [
    id 203
    label "K&#322;odawa"
  ]
  node [
    id 204
    label "Solikamsk"
  ]
  node [
    id 205
    label "Wolgast"
  ]
  node [
    id 206
    label "Saloniki"
  ]
  node [
    id 207
    label "Lw&#243;w"
  ]
  node [
    id 208
    label "Al-Kufa"
  ]
  node [
    id 209
    label "Hamburg"
  ]
  node [
    id 210
    label "Bogusz&#243;w-Gorce"
  ]
  node [
    id 211
    label "Nampula"
  ]
  node [
    id 212
    label "burmistrz"
  ]
  node [
    id 213
    label "D&#252;sseldorf"
  ]
  node [
    id 214
    label "Nowy_Orlean"
  ]
  node [
    id 215
    label "Bamberg"
  ]
  node [
    id 216
    label "Osaka"
  ]
  node [
    id 217
    label "Niko&#322;ajewsk_nad_Amurem"
  ]
  node [
    id 218
    label "Michalovce"
  ]
  node [
    id 219
    label "Fryburg"
  ]
  node [
    id 220
    label "Trabzon"
  ]
  node [
    id 221
    label "Wersal"
  ]
  node [
    id 222
    label "Swatowe"
  ]
  node [
    id 223
    label "Ka&#322;uga"
  ]
  node [
    id 224
    label "Dijon"
  ]
  node [
    id 225
    label "Cannes"
  ]
  node [
    id 226
    label "Borowsk"
  ]
  node [
    id 227
    label "Kursk"
  ]
  node [
    id 228
    label "Tyberiada"
  ]
  node [
    id 229
    label "Boden"
  ]
  node [
    id 230
    label "Dodona"
  ]
  node [
    id 231
    label "Vukovar"
  ]
  node [
    id 232
    label "Soleczniki"
  ]
  node [
    id 233
    label "Barcelona"
  ]
  node [
    id 234
    label "Oszmiana"
  ]
  node [
    id 235
    label "Stuttgart"
  ]
  node [
    id 236
    label "Nerczy&#324;sk"
  ]
  node [
    id 237
    label "Bijsk"
  ]
  node [
    id 238
    label "Essen"
  ]
  node [
    id 239
    label "Luboml"
  ]
  node [
    id 240
    label "Gr&#243;dek"
  ]
  node [
    id 241
    label "Orany"
  ]
  node [
    id 242
    label "Siedliszcze"
  ]
  node [
    id 243
    label "P&#322;owdiw"
  ]
  node [
    id 244
    label "A&#322;apajewsk"
  ]
  node [
    id 245
    label "Liverpool"
  ]
  node [
    id 246
    label "Ostrawa"
  ]
  node [
    id 247
    label "Penza"
  ]
  node [
    id 248
    label "Rudki"
  ]
  node [
    id 249
    label "Aktobe"
  ]
  node [
    id 250
    label "I&#322;awka"
  ]
  node [
    id 251
    label "Tolkmicko"
  ]
  node [
    id 252
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 253
    label "Sajgon"
  ]
  node [
    id 254
    label "Windawa"
  ]
  node [
    id 255
    label "Weimar"
  ]
  node [
    id 256
    label "Jekaterynburg"
  ]
  node [
    id 257
    label "Lejda"
  ]
  node [
    id 258
    label "Cremona"
  ]
  node [
    id 259
    label "Wo&#322;godo&#324;sk"
  ]
  node [
    id 260
    label "Kordoba"
  ]
  node [
    id 261
    label "urz&#261;d"
  ]
  node [
    id 262
    label "&#321;ohojsk"
  ]
  node [
    id 263
    label "Kalmar"
  ]
  node [
    id 264
    label "Akerman"
  ]
  node [
    id 265
    label "Locarno"
  ]
  node [
    id 266
    label "Bych&#243;w"
  ]
  node [
    id 267
    label "Toledo"
  ]
  node [
    id 268
    label "Minusi&#324;sk"
  ]
  node [
    id 269
    label "Szk&#322;&#243;w"
  ]
  node [
    id 270
    label "Wenecja"
  ]
  node [
    id 271
    label "Bazylea"
  ]
  node [
    id 272
    label "Peszt"
  ]
  node [
    id 273
    label "Piza"
  ]
  node [
    id 274
    label "Tanger"
  ]
  node [
    id 275
    label "Krzywi&#324;"
  ]
  node [
    id 276
    label "Eger"
  ]
  node [
    id 277
    label "Bogus&#322;aw"
  ]
  node [
    id 278
    label "Taganrog"
  ]
  node [
    id 279
    label "Oksford"
  ]
  node [
    id 280
    label "Gwardiejsk"
  ]
  node [
    id 281
    label "Tyraspol"
  ]
  node [
    id 282
    label "Kleczew"
  ]
  node [
    id 283
    label "Nowa_D&#281;ba"
  ]
  node [
    id 284
    label "Wilejka"
  ]
  node [
    id 285
    label "Modena"
  ]
  node [
    id 286
    label "Demmin"
  ]
  node [
    id 287
    label "Houston"
  ]
  node [
    id 288
    label "Rydu&#322;towy"
  ]
  node [
    id 289
    label "Bordeaux"
  ]
  node [
    id 290
    label "Schmalkalden"
  ]
  node [
    id 291
    label "O&#322;omuniec"
  ]
  node [
    id 292
    label "Tuluza"
  ]
  node [
    id 293
    label "tramwaj"
  ]
  node [
    id 294
    label "Nantes"
  ]
  node [
    id 295
    label "Debreczyn"
  ]
  node [
    id 296
    label "Kowel"
  ]
  node [
    id 297
    label "Witnica"
  ]
  node [
    id 298
    label "Stalingrad"
  ]
  node [
    id 299
    label "Drezno"
  ]
  node [
    id 300
    label "Perejas&#322;aw"
  ]
  node [
    id 301
    label "Luksor"
  ]
  node [
    id 302
    label "Ostaszk&#243;w"
  ]
  node [
    id 303
    label "Gettysburg"
  ]
  node [
    id 304
    label "Trydent"
  ]
  node [
    id 305
    label "Poczdam"
  ]
  node [
    id 306
    label "Mesyna"
  ]
  node [
    id 307
    label "Krasnogorsk"
  ]
  node [
    id 308
    label "Kars"
  ]
  node [
    id 309
    label "Darmstadt"
  ]
  node [
    id 310
    label "Rzg&#243;w"
  ]
  node [
    id 311
    label "Kar&#322;owice"
  ]
  node [
    id 312
    label "Czeskie_Budziejowice"
  ]
  node [
    id 313
    label "Buda"
  ]
  node [
    id 314
    label "Monako"
  ]
  node [
    id 315
    label "Pardubice"
  ]
  node [
    id 316
    label "Pas&#322;&#281;k"
  ]
  node [
    id 317
    label "Fatima"
  ]
  node [
    id 318
    label "Bir&#380;e"
  ]
  node [
    id 319
    label "Wo&#322;oko&#322;amsk"
  ]
  node [
    id 320
    label "Wi&#322;komierz"
  ]
  node [
    id 321
    label "Opawa"
  ]
  node [
    id 322
    label "Mantua"
  ]
  node [
    id 323
    label "ulica"
  ]
  node [
    id 324
    label "Tarragona"
  ]
  node [
    id 325
    label "Antwerpia"
  ]
  node [
    id 326
    label "Asuan"
  ]
  node [
    id 327
    label "Korynt"
  ]
  node [
    id 328
    label "Armenia"
  ]
  node [
    id 329
    label "Budionnowsk"
  ]
  node [
    id 330
    label "Lengyel"
  ]
  node [
    id 331
    label "Betlejem"
  ]
  node [
    id 332
    label "Asy&#380;"
  ]
  node [
    id 333
    label "Batumi"
  ]
  node [
    id 334
    label "Paczk&#243;w"
  ]
  node [
    id 335
    label "Grenada"
  ]
  node [
    id 336
    label "Suczawa"
  ]
  node [
    id 337
    label "Nowogard"
  ]
  node [
    id 338
    label "Tyr"
  ]
  node [
    id 339
    label "Bria&#324;sk"
  ]
  node [
    id 340
    label "Bar"
  ]
  node [
    id 341
    label "Czerkiesk"
  ]
  node [
    id 342
    label "Ja&#322;ta"
  ]
  node [
    id 343
    label "Mo&#347;ciska"
  ]
  node [
    id 344
    label "Medyna"
  ]
  node [
    id 345
    label "Tartu"
  ]
  node [
    id 346
    label "Pemba"
  ]
  node [
    id 347
    label "Lipawa"
  ]
  node [
    id 348
    label "Tyl&#380;a"
  ]
  node [
    id 349
    label "Lipsk"
  ]
  node [
    id 350
    label "Dayton"
  ]
  node [
    id 351
    label "Rohatyn"
  ]
  node [
    id 352
    label "Peszawar"
  ]
  node [
    id 353
    label "Azow"
  ]
  node [
    id 354
    label "Adrianopol"
  ]
  node [
    id 355
    label "Iwano-Frankowsk"
  ]
  node [
    id 356
    label "Czarnobyl"
  ]
  node [
    id 357
    label "Rakoniewice"
  ]
  node [
    id 358
    label "Obuch&#243;w"
  ]
  node [
    id 359
    label "Orneta"
  ]
  node [
    id 360
    label "Koszyce"
  ]
  node [
    id 361
    label "Czeski_Cieszyn"
  ]
  node [
    id 362
    label "Zagorsk"
  ]
  node [
    id 363
    label "Nieder_Selters"
  ]
  node [
    id 364
    label "Ko&#322;omna"
  ]
  node [
    id 365
    label "Rost&#243;w"
  ]
  node [
    id 366
    label "Bolonia"
  ]
  node [
    id 367
    label "Rajgr&#243;d"
  ]
  node [
    id 368
    label "L&#252;neburg"
  ]
  node [
    id 369
    label "Brack"
  ]
  node [
    id 370
    label "Konstancja"
  ]
  node [
    id 371
    label "Koluszki"
  ]
  node [
    id 372
    label "Semipa&#322;aty&#324;sk"
  ]
  node [
    id 373
    label "Suez"
  ]
  node [
    id 374
    label "Mrocza"
  ]
  node [
    id 375
    label "Triest"
  ]
  node [
    id 376
    label "Murma&#324;sk"
  ]
  node [
    id 377
    label "Tu&#322;a"
  ]
  node [
    id 378
    label "Tarnogr&#243;d"
  ]
  node [
    id 379
    label "Radziech&#243;w"
  ]
  node [
    id 380
    label "Kokand"
  ]
  node [
    id 381
    label "Kircholm"
  ]
  node [
    id 382
    label "Nowa_Ruda"
  ]
  node [
    id 383
    label "Huma&#324;"
  ]
  node [
    id 384
    label "Turkiestan"
  ]
  node [
    id 385
    label "Kani&#243;w"
  ]
  node [
    id 386
    label "Pilzno"
  ]
  node [
    id 387
    label "Dubno"
  ]
  node [
    id 388
    label "Bras&#322;aw"
  ]
  node [
    id 389
    label "Korfant&#243;w"
  ]
  node [
    id 390
    label "Choroszcz"
  ]
  node [
    id 391
    label "Nowogr&#243;d"
  ]
  node [
    id 392
    label "Konotop"
  ]
  node [
    id 393
    label "M&#347;cis&#322;aw"
  ]
  node [
    id 394
    label "Jastarnia"
  ]
  node [
    id 395
    label "Ba&#324;ska_Bystrzyca"
  ]
  node [
    id 396
    label "Omsk"
  ]
  node [
    id 397
    label "Troick"
  ]
  node [
    id 398
    label "Koper"
  ]
  node [
    id 399
    label "Jenisejsk"
  ]
  node [
    id 400
    label "Frankfurt_nad_Odr&#261;"
  ]
  node [
    id 401
    label "Oldenburg_in_Holstein"
  ]
  node [
    id 402
    label "Trenczyn"
  ]
  node [
    id 403
    label "Wormacja"
  ]
  node [
    id 404
    label "Wagram"
  ]
  node [
    id 405
    label "Lubeka"
  ]
  node [
    id 406
    label "Genewa"
  ]
  node [
    id 407
    label "&#379;ydacz&#243;w"
  ]
  node [
    id 408
    label "Kleck"
  ]
  node [
    id 409
    label "G&#243;ra_Kalwaria"
  ]
  node [
    id 410
    label "Struga"
  ]
  node [
    id 411
    label "Izmir"
  ]
  node [
    id 412
    label "Dortmund"
  ]
  node [
    id 413
    label "Izbica_Kujawska"
  ]
  node [
    id 414
    label "Stalinogorsk"
  ]
  node [
    id 415
    label "Workuta"
  ]
  node [
    id 416
    label "Jerycho"
  ]
  node [
    id 417
    label "Brunszwik"
  ]
  node [
    id 418
    label "Aleksandria"
  ]
  node [
    id 419
    label "miejscowo&#347;&#263;"
  ]
  node [
    id 420
    label "Borys&#322;aw"
  ]
  node [
    id 421
    label "Zaleszczyki"
  ]
  node [
    id 422
    label "Z&#322;oczew"
  ]
  node [
    id 423
    label "Piast&#243;w"
  ]
  node [
    id 424
    label "Frankfurt_nad_Menem"
  ]
  node [
    id 425
    label "Bor"
  ]
  node [
    id 426
    label "Nazaret"
  ]
  node [
    id 427
    label "Sarat&#243;w"
  ]
  node [
    id 428
    label "Brasz&#243;w"
  ]
  node [
    id 429
    label "Malin"
  ]
  node [
    id 430
    label "Parma"
  ]
  node [
    id 431
    label "Wierchoja&#324;sk"
  ]
  node [
    id 432
    label "Tarent"
  ]
  node [
    id 433
    label "Mariampol"
  ]
  node [
    id 434
    label "Wuhan"
  ]
  node [
    id 435
    label "Split"
  ]
  node [
    id 436
    label "Baranowicze"
  ]
  node [
    id 437
    label "Marki"
  ]
  node [
    id 438
    label "Adana"
  ]
  node [
    id 439
    label "B&#322;aszki"
  ]
  node [
    id 440
    label "Lubecz"
  ]
  node [
    id 441
    label "Sulech&#243;w"
  ]
  node [
    id 442
    label "Borys&#243;w"
  ]
  node [
    id 443
    label "Homel"
  ]
  node [
    id 444
    label "Tours"
  ]
  node [
    id 445
    label "Kapsztad"
  ]
  node [
    id 446
    label "Edam"
  ]
  node [
    id 447
    label "Zaporo&#380;e"
  ]
  node [
    id 448
    label "Miedwie&#380;jegorsk"
  ]
  node [
    id 449
    label "Kamieniec_Podolski"
  ]
  node [
    id 450
    label "Chocim"
  ]
  node [
    id 451
    label "Mohylew"
  ]
  node [
    id 452
    label "Merseburg"
  ]
  node [
    id 453
    label "Konstantynopol"
  ]
  node [
    id 454
    label "Sambor"
  ]
  node [
    id 455
    label "Manchester"
  ]
  node [
    id 456
    label "Pi&#324;sk"
  ]
  node [
    id 457
    label "Ochryda"
  ]
  node [
    id 458
    label "Rybi&#324;sk"
  ]
  node [
    id 459
    label "Czadca"
  ]
  node [
    id 460
    label "Orenburg"
  ]
  node [
    id 461
    label "Krajowa"
  ]
  node [
    id 462
    label "Eleusis"
  ]
  node [
    id 463
    label "Awinion"
  ]
  node [
    id 464
    label "Rzeczyca"
  ]
  node [
    id 465
    label "Barczewo"
  ]
  node [
    id 466
    label "Lozanna"
  ]
  node [
    id 467
    label "&#379;migr&#243;d"
  ]
  node [
    id 468
    label "Chabarowsk"
  ]
  node [
    id 469
    label "Jena"
  ]
  node [
    id 470
    label "Xai-Xai"
  ]
  node [
    id 471
    label "Radk&#243;w"
  ]
  node [
    id 472
    label "Syrakuzy"
  ]
  node [
    id 473
    label "Zas&#322;aw"
  ]
  node [
    id 474
    label "Getynga"
  ]
  node [
    id 475
    label "Windsor"
  ]
  node [
    id 476
    label "Carrara"
  ]
  node [
    id 477
    label "Madras"
  ]
  node [
    id 478
    label "Nitra"
  ]
  node [
    id 479
    label "Kilonia"
  ]
  node [
    id 480
    label "Rawenna"
  ]
  node [
    id 481
    label "Stawropol"
  ]
  node [
    id 482
    label "Warna"
  ]
  node [
    id 483
    label "Ba&#322;tijsk"
  ]
  node [
    id 484
    label "Cumana"
  ]
  node [
    id 485
    label "Kostroma"
  ]
  node [
    id 486
    label "Bajonna"
  ]
  node [
    id 487
    label "Magadan"
  ]
  node [
    id 488
    label "Kercz"
  ]
  node [
    id 489
    label "Harbin"
  ]
  node [
    id 490
    label "Sankt_Florian"
  ]
  node [
    id 491
    label "Norak"
  ]
  node [
    id 492
    label "Wo&#322;kowysk"
  ]
  node [
    id 493
    label "Bia&#322;ogr&#243;d_nad_Dniestrem"
  ]
  node [
    id 494
    label "S&#232;vres"
  ]
  node [
    id 495
    label "Barwice"
  ]
  node [
    id 496
    label "Jutrosin"
  ]
  node [
    id 497
    label "Sumy"
  ]
  node [
    id 498
    label "Canterbury"
  ]
  node [
    id 499
    label "Czerkasy"
  ]
  node [
    id 500
    label "Troki"
  ]
  node [
    id 501
    label "Dnieprodzier&#380;y&#324;sk"
  ]
  node [
    id 502
    label "Turka"
  ]
  node [
    id 503
    label "Budziszyn"
  ]
  node [
    id 504
    label "A&#322;czewsk"
  ]
  node [
    id 505
    label "Chark&#243;w"
  ]
  node [
    id 506
    label "Go&#347;cino"
  ]
  node [
    id 507
    label "Ku&#378;nieck"
  ]
  node [
    id 508
    label "Wotki&#324;sk"
  ]
  node [
    id 509
    label "Symferopol"
  ]
  node [
    id 510
    label "Dmitrow"
  ]
  node [
    id 511
    label "Cherso&#324;"
  ]
  node [
    id 512
    label "zabudowa"
  ]
  node [
    id 513
    label "Nowogr&#243;dek"
  ]
  node [
    id 514
    label "Orlean"
  ]
  node [
    id 515
    label "Ku&#378;nia_Raciborska"
  ]
  node [
    id 516
    label "Berdia&#324;sk"
  ]
  node [
    id 517
    label "Szumsk"
  ]
  node [
    id 518
    label "Z&#322;ocz&#243;w"
  ]
  node [
    id 519
    label "Orsza"
  ]
  node [
    id 520
    label "Cluny"
  ]
  node [
    id 521
    label "Aralsk"
  ]
  node [
    id 522
    label "Stoczek_&#321;ukowski"
  ]
  node [
    id 523
    label "Bogumin"
  ]
  node [
    id 524
    label "Antiochia"
  ]
  node [
    id 525
    label "grupa"
  ]
  node [
    id 526
    label "Inhambane"
  ]
  node [
    id 527
    label "S&#322;awia&#324;sk_nad_Kubaniem"
  ]
  node [
    id 528
    label "Wielki_Pres&#322;aw"
  ]
  node [
    id 529
    label "Trewir"
  ]
  node [
    id 530
    label "Nowogr&#243;d_Wielki"
  ]
  node [
    id 531
    label "Siewieromorsk"
  ]
  node [
    id 532
    label "Calais"
  ]
  node [
    id 533
    label "&#379;ytawa"
  ]
  node [
    id 534
    label "Eupatoria"
  ]
  node [
    id 535
    label "Twer"
  ]
  node [
    id 536
    label "Stara_Zagora"
  ]
  node [
    id 537
    label "Jastrowie"
  ]
  node [
    id 538
    label "Piatigorsk"
  ]
  node [
    id 539
    label "&#379;&#243;&#322;kiew"
  ]
  node [
    id 540
    label "Le&#324;sk"
  ]
  node [
    id 541
    label "Johannesburg"
  ]
  node [
    id 542
    label "Kaszyn"
  ]
  node [
    id 543
    label "&#346;wi&#281;ciany"
  ]
  node [
    id 544
    label "&#379;ylina"
  ]
  node [
    id 545
    label "Sewastopol"
  ]
  node [
    id 546
    label "Pietrozawodsk"
  ]
  node [
    id 547
    label "Bobolice"
  ]
  node [
    id 548
    label "Mosty"
  ]
  node [
    id 549
    label "Korsu&#324;_Szewczenkowski"
  ]
  node [
    id 550
    label "Karaganda"
  ]
  node [
    id 551
    label "Marsylia"
  ]
  node [
    id 552
    label "Buchara"
  ]
  node [
    id 553
    label "Dubrownik"
  ]
  node [
    id 554
    label "Be&#322;z"
  ]
  node [
    id 555
    label "Oran"
  ]
  node [
    id 556
    label "Regensburg"
  ]
  node [
    id 557
    label "Rotterdam"
  ]
  node [
    id 558
    label "Trembowla"
  ]
  node [
    id 559
    label "Woskriesiensk"
  ]
  node [
    id 560
    label "Po&#322;ock"
  ]
  node [
    id 561
    label "Poprad"
  ]
  node [
    id 562
    label "Los_Angeles"
  ]
  node [
    id 563
    label "Kronsztad"
  ]
  node [
    id 564
    label "U&#322;an_Ude"
  ]
  node [
    id 565
    label "U&#347;cie_nad_&#321;ab&#261;"
  ]
  node [
    id 566
    label "W&#322;adywostok"
  ]
  node [
    id 567
    label "Kandahar"
  ]
  node [
    id 568
    label "Tobolsk"
  ]
  node [
    id 569
    label "Boston"
  ]
  node [
    id 570
    label "Hawana"
  ]
  node [
    id 571
    label "Kis&#322;owodzk"
  ]
  node [
    id 572
    label "Tulon"
  ]
  node [
    id 573
    label "Utrecht"
  ]
  node [
    id 574
    label "Oleszyce"
  ]
  node [
    id 575
    label "Hradec_Kr&#225;lov&#233;"
  ]
  node [
    id 576
    label "Katania"
  ]
  node [
    id 577
    label "Teby"
  ]
  node [
    id 578
    label "Paw&#322;owo"
  ]
  node [
    id 579
    label "W&#252;rzburg"
  ]
  node [
    id 580
    label "Podiebrady"
  ]
  node [
    id 581
    label "Uppsala"
  ]
  node [
    id 582
    label "Poniewie&#380;"
  ]
  node [
    id 583
    label "Berezyna"
  ]
  node [
    id 584
    label "Aczy&#324;sk"
  ]
  node [
    id 585
    label "Niko&#322;ajewsk"
  ]
  node [
    id 586
    label "Ostr&#243;g"
  ]
  node [
    id 587
    label "Brze&#347;&#263;"
  ]
  node [
    id 588
    label "Stryj"
  ]
  node [
    id 589
    label "Lancaster"
  ]
  node [
    id 590
    label "Kozielsk"
  ]
  node [
    id 591
    label "Loreto"
  ]
  node [
    id 592
    label "B&#322;agowieszcze&#324;sk"
  ]
  node [
    id 593
    label "Hebron"
  ]
  node [
    id 594
    label "Kaspijsk"
  ]
  node [
    id 595
    label "Peczora"
  ]
  node [
    id 596
    label "Isfahan"
  ]
  node [
    id 597
    label "Chimoio"
  ]
  node [
    id 598
    label "Mory&#324;"
  ]
  node [
    id 599
    label "Kowno"
  ]
  node [
    id 600
    label "Nie&#347;wie&#380;"
  ]
  node [
    id 601
    label "Opalenica"
  ]
  node [
    id 602
    label "Kolonia"
  ]
  node [
    id 603
    label "Stary_Sambor"
  ]
  node [
    id 604
    label "Kolkata"
  ]
  node [
    id 605
    label "Turkmenbaszy"
  ]
  node [
    id 606
    label "Mohyl&#243;w_Podolski"
  ]
  node [
    id 607
    label "Nankin"
  ]
  node [
    id 608
    label "Krzanowice"
  ]
  node [
    id 609
    label "Efez"
  ]
  node [
    id 610
    label "Dobrodzie&#324;"
  ]
  node [
    id 611
    label "Neapol"
  ]
  node [
    id 612
    label "S&#322;uck"
  ]
  node [
    id 613
    label "Fryburg_Bryzgowijski"
  ]
  node [
    id 614
    label "W&#322;odzimierz_Wo&#322;y&#324;ski"
  ]
  node [
    id 615
    label "Frydek-Mistek"
  ]
  node [
    id 616
    label "Korsze"
  ]
  node [
    id 617
    label "T&#322;uszcz"
  ]
  node [
    id 618
    label "Soligorsk"
  ]
  node [
    id 619
    label "Kie&#380;mark"
  ]
  node [
    id 620
    label "Mannheim"
  ]
  node [
    id 621
    label "Ulm"
  ]
  node [
    id 622
    label "Podhajce"
  ]
  node [
    id 623
    label "Dniepropetrowsk"
  ]
  node [
    id 624
    label "Szamocin"
  ]
  node [
    id 625
    label "Ko&#322;omyja"
  ]
  node [
    id 626
    label "Buczacz"
  ]
  node [
    id 627
    label "M&#252;nster"
  ]
  node [
    id 628
    label "Brema"
  ]
  node [
    id 629
    label "Delhi"
  ]
  node [
    id 630
    label "Nicea"
  ]
  node [
    id 631
    label "&#346;niatyn"
  ]
  node [
    id 632
    label "Szawle"
  ]
  node [
    id 633
    label "Czerniowce"
  ]
  node [
    id 634
    label "Mi&#347;nia"
  ]
  node [
    id 635
    label "Sydney"
  ]
  node [
    id 636
    label "Moguncja"
  ]
  node [
    id 637
    label "Narbona"
  ]
  node [
    id 638
    label "Swiet&#322;ogorsk"
  ]
  node [
    id 639
    label "Wittenberga"
  ]
  node [
    id 640
    label "Uljanowsk"
  ]
  node [
    id 641
    label "Wyborg"
  ]
  node [
    id 642
    label "&#321;uga&#324;sk"
  ]
  node [
    id 643
    label "Trojan"
  ]
  node [
    id 644
    label "Kalwaria_Zebrzydowska"
  ]
  node [
    id 645
    label "Brandenburg"
  ]
  node [
    id 646
    label "Kemerowo"
  ]
  node [
    id 647
    label "Kaszgar"
  ]
  node [
    id 648
    label "Lenzen"
  ]
  node [
    id 649
    label "Nanning"
  ]
  node [
    id 650
    label "Gotha"
  ]
  node [
    id 651
    label "Zurych"
  ]
  node [
    id 652
    label "Baltimore"
  ]
  node [
    id 653
    label "&#321;uck"
  ]
  node [
    id 654
    label "Bristol"
  ]
  node [
    id 655
    label "Ferrara"
  ]
  node [
    id 656
    label "Mariupol"
  ]
  node [
    id 657
    label "przestrze&#324;_publiczna"
  ]
  node [
    id 658
    label "Filadelfia"
  ]
  node [
    id 659
    label "Czerniejewo"
  ]
  node [
    id 660
    label "Milan&#243;wek"
  ]
  node [
    id 661
    label "Lhasa"
  ]
  node [
    id 662
    label "Kanton"
  ]
  node [
    id 663
    label "Perwomajsk"
  ]
  node [
    id 664
    label "Nieftiegorsk"
  ]
  node [
    id 665
    label "Greifswald"
  ]
  node [
    id 666
    label "Pittsburgh"
  ]
  node [
    id 667
    label "Akwileja"
  ]
  node [
    id 668
    label "Norfolk"
  ]
  node [
    id 669
    label "Perm"
  ]
  node [
    id 670
    label "Fergana"
  ]
  node [
    id 671
    label "Detroit"
  ]
  node [
    id 672
    label "Starobielsk"
  ]
  node [
    id 673
    label "Wielsk"
  ]
  node [
    id 674
    label "Zaklik&#243;w"
  ]
  node [
    id 675
    label "Majsur"
  ]
  node [
    id 676
    label "Narwa"
  ]
  node [
    id 677
    label "Chicago"
  ]
  node [
    id 678
    label "Byczyna"
  ]
  node [
    id 679
    label "Mozyrz"
  ]
  node [
    id 680
    label "Konstantyn&#243;wka"
  ]
  node [
    id 681
    label "Miko&#322;aj&#243;w"
  ]
  node [
    id 682
    label "Megara"
  ]
  node [
    id 683
    label "Stralsund"
  ]
  node [
    id 684
    label "Wo&#322;gograd"
  ]
  node [
    id 685
    label "Lichinga"
  ]
  node [
    id 686
    label "Haga"
  ]
  node [
    id 687
    label "Tarnopol"
  ]
  node [
    id 688
    label "Nowomoskowsk"
  ]
  node [
    id 689
    label "K&#322;ajpeda"
  ]
  node [
    id 690
    label "Ussuryjsk"
  ]
  node [
    id 691
    label "Brugia"
  ]
  node [
    id 692
    label "Natal"
  ]
  node [
    id 693
    label "Kro&#347;niewice"
  ]
  node [
    id 694
    label "Edynburg"
  ]
  node [
    id 695
    label "Marburg"
  ]
  node [
    id 696
    label "Dalton"
  ]
  node [
    id 697
    label "S&#322;onim"
  ]
  node [
    id 698
    label "&#346;wiebodzice"
  ]
  node [
    id 699
    label "Smorgonie"
  ]
  node [
    id 700
    label "Orze&#322;"
  ]
  node [
    id 701
    label "Nowoku&#378;nieck"
  ]
  node [
    id 702
    label "Zadar"
  ]
  node [
    id 703
    label "Koprzywnica"
  ]
  node [
    id 704
    label "Angarsk"
  ]
  node [
    id 705
    label "Tel_Awiw-Jafa"
  ]
  node [
    id 706
    label "Mo&#380;ajsk"
  ]
  node [
    id 707
    label "Norylsk"
  ]
  node [
    id 708
    label "Akwizgran"
  ]
  node [
    id 709
    label "Jawor&#243;w"
  ]
  node [
    id 710
    label "weduta"
  ]
  node [
    id 711
    label "Jab&#322;onk&#243;w"
  ]
  node [
    id 712
    label "Suzdal"
  ]
  node [
    id 713
    label "W&#322;odzimierz"
  ]
  node [
    id 714
    label "Bujnaksk"
  ]
  node [
    id 715
    label "Beresteczko"
  ]
  node [
    id 716
    label "Strzelno"
  ]
  node [
    id 717
    label "Siewsk"
  ]
  node [
    id 718
    label "Cymlansk"
  ]
  node [
    id 719
    label "Trzyniec"
  ]
  node [
    id 720
    label "Hanower"
  ]
  node [
    id 721
    label "Wuppertal"
  ]
  node [
    id 722
    label "Sura&#380;"
  ]
  node [
    id 723
    label "Samara"
  ]
  node [
    id 724
    label "Winchester"
  ]
  node [
    id 725
    label "Krasnodar"
  ]
  node [
    id 726
    label "Sydon"
  ]
  node [
    id 727
    label "Worone&#380;"
  ]
  node [
    id 728
    label "Paw&#322;odar"
  ]
  node [
    id 729
    label "Czelabi&#324;sk"
  ]
  node [
    id 730
    label "Reda"
  ]
  node [
    id 731
    label "Karwina"
  ]
  node [
    id 732
    label "Wyszehrad"
  ]
  node [
    id 733
    label "Sara&#324;sk"
  ]
  node [
    id 734
    label "Koby&#322;ka"
  ]
  node [
    id 735
    label "Tambow"
  ]
  node [
    id 736
    label "Pyskowice"
  ]
  node [
    id 737
    label "Winnica"
  ]
  node [
    id 738
    label "Heidelberg"
  ]
  node [
    id 739
    label "Maribor"
  ]
  node [
    id 740
    label "Werona"
  ]
  node [
    id 741
    label "G&#322;uszyca"
  ]
  node [
    id 742
    label "Rostock"
  ]
  node [
    id 743
    label "Mekka"
  ]
  node [
    id 744
    label "Liberec"
  ]
  node [
    id 745
    label "Bie&#322;gorod"
  ]
  node [
    id 746
    label "Berdycz&#243;w"
  ]
  node [
    id 747
    label "Sierdobsk"
  ]
  node [
    id 748
    label "Bobrujsk"
  ]
  node [
    id 749
    label "Padwa"
  ]
  node [
    id 750
    label "Chanty-Mansyjsk"
  ]
  node [
    id 751
    label "Pasawa"
  ]
  node [
    id 752
    label "Poczaj&#243;w"
  ]
  node [
    id 753
    label "&#379;ar&#243;w"
  ]
  node [
    id 754
    label "Barabi&#324;sk"
  ]
  node [
    id 755
    label "Gorycja"
  ]
  node [
    id 756
    label "Haarlem"
  ]
  node [
    id 757
    label "Kiejdany"
  ]
  node [
    id 758
    label "Chmielnicki"
  ]
  node [
    id 759
    label "Siena"
  ]
  node [
    id 760
    label "Burgas"
  ]
  node [
    id 761
    label "Magnitogorsk"
  ]
  node [
    id 762
    label "Korzec"
  ]
  node [
    id 763
    label "Bonn"
  ]
  node [
    id 764
    label "Pr&#243;szk&#243;w"
  ]
  node [
    id 765
    label "Walencja"
  ]
  node [
    id 766
    label "Mosina"
  ]
  node [
    id 767
    label "mi&#281;sny"
  ]
  node [
    id 768
    label "planowa&#263;"
  ]
  node [
    id 769
    label "dostosowywa&#263;"
  ]
  node [
    id 770
    label "pozyskiwa&#263;"
  ]
  node [
    id 771
    label "wprowadza&#263;"
  ]
  node [
    id 772
    label "treat"
  ]
  node [
    id 773
    label "przygotowywa&#263;"
  ]
  node [
    id 774
    label "create"
  ]
  node [
    id 775
    label "ensnare"
  ]
  node [
    id 776
    label "tworzy&#263;"
  ]
  node [
    id 777
    label "standard"
  ]
  node [
    id 778
    label "skupia&#263;"
  ]
  node [
    id 779
    label "si&#281;ga&#263;"
  ]
  node [
    id 780
    label "obecno&#347;&#263;"
  ]
  node [
    id 781
    label "stan"
  ]
  node [
    id 782
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 783
    label "mie&#263;_miejsce"
  ]
  node [
    id 784
    label "uczestniczy&#263;"
  ]
  node [
    id 785
    label "chodzi&#263;"
  ]
  node [
    id 786
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 787
    label "equal"
  ]
  node [
    id 788
    label "r&#243;&#380;nie"
  ]
  node [
    id 789
    label "inny"
  ]
  node [
    id 790
    label "jaki&#347;"
  ]
  node [
    id 791
    label "gromada"
  ]
  node [
    id 792
    label "autorament"
  ]
  node [
    id 793
    label "przypuszczenie"
  ]
  node [
    id 794
    label "cynk"
  ]
  node [
    id 795
    label "rezultat"
  ]
  node [
    id 796
    label "jednostka_systematyczna"
  ]
  node [
    id 797
    label "kr&#243;lestwo"
  ]
  node [
    id 798
    label "obstawia&#263;"
  ]
  node [
    id 799
    label "design"
  ]
  node [
    id 800
    label "facet"
  ]
  node [
    id 801
    label "variety"
  ]
  node [
    id 802
    label "sztuka"
  ]
  node [
    id 803
    label "antycypacja"
  ]
  node [
    id 804
    label "pensum"
  ]
  node [
    id 805
    label "enroll"
  ]
  node [
    id 806
    label "specjalny"
  ]
  node [
    id 807
    label "na_sportowo"
  ]
  node [
    id 808
    label "uczciwy"
  ]
  node [
    id 809
    label "wygodny"
  ]
  node [
    id 810
    label "pe&#322;ny"
  ]
  node [
    id 811
    label "sportowo"
  ]
  node [
    id 812
    label "elegancki"
  ]
  node [
    id 813
    label "kulturalnie"
  ]
  node [
    id 814
    label "dobrze_wychowany"
  ]
  node [
    id 815
    label "kulturny"
  ]
  node [
    id 816
    label "wykszta&#322;cony"
  ]
  node [
    id 817
    label "stosowny"
  ]
  node [
    id 818
    label "nastawia&#263;"
  ]
  node [
    id 819
    label "switch"
  ]
  node [
    id 820
    label "zmienia&#263;"
  ]
  node [
    id 821
    label "shift"
  ]
  node [
    id 822
    label "przebudowywa&#263;"
  ]
  node [
    id 823
    label "przemieszcza&#263;"
  ]
  node [
    id 824
    label "sprawia&#263;"
  ]
  node [
    id 825
    label "stawia&#263;"
  ]
  node [
    id 826
    label "retrenchment"
  ]
  node [
    id 827
    label "shortening"
  ]
  node [
    id 828
    label "redukcja"
  ]
  node [
    id 829
    label "contraction"
  ]
  node [
    id 830
    label "przej&#347;cie"
  ]
  node [
    id 831
    label "leksem"
  ]
  node [
    id 832
    label "tekst"
  ]
  node [
    id 833
    label "free"
  ]
  node [
    id 834
    label "tentegowa&#263;"
  ]
  node [
    id 835
    label "urz&#261;dza&#263;"
  ]
  node [
    id 836
    label "give"
  ]
  node [
    id 837
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 838
    label "czyni&#263;"
  ]
  node [
    id 839
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 840
    label "post&#281;powa&#263;"
  ]
  node [
    id 841
    label "wydala&#263;"
  ]
  node [
    id 842
    label "oszukiwa&#263;"
  ]
  node [
    id 843
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 844
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 845
    label "work"
  ]
  node [
    id 846
    label "przerabia&#263;"
  ]
  node [
    id 847
    label "stylizowa&#263;"
  ]
  node [
    id 848
    label "falowa&#263;"
  ]
  node [
    id 849
    label "act"
  ]
  node [
    id 850
    label "peddle"
  ]
  node [
    id 851
    label "ukazywa&#263;"
  ]
  node [
    id 852
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 853
    label "praca"
  ]
  node [
    id 854
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 855
    label "doba"
  ]
  node [
    id 856
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 857
    label "weekend"
  ]
  node [
    id 858
    label "miesi&#261;c"
  ]
  node [
    id 859
    label "Wielki_Pi&#261;tek"
  ]
  node [
    id 860
    label "miejsce"
  ]
  node [
    id 861
    label "szko&#322;a"
  ]
  node [
    id 862
    label "szko&#322;a_ponadpodstawowa"
  ]
  node [
    id 863
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 864
    label "open"
  ]
  node [
    id 865
    label "bezskutecznie"
  ]
  node [
    id 866
    label "darmowy"
  ]
  node [
    id 867
    label "bezcelowy"
  ]
  node [
    id 868
    label "zb&#281;dnie"
  ]
  node [
    id 869
    label "pakowa&#263;"
  ]
  node [
    id 870
    label "kulturysta"
  ]
  node [
    id 871
    label "obiekt"
  ]
  node [
    id 872
    label "budowla"
  ]
  node [
    id 873
    label "zak&#322;ad_komunalny"
  ]
  node [
    id 874
    label "pakernia"
  ]
  node [
    id 875
    label "halizna"
  ]
  node [
    id 876
    label "fabryka"
  ]
  node [
    id 877
    label "huta"
  ]
  node [
    id 878
    label "pastwisko"
  ]
  node [
    id 879
    label "budynek"
  ]
  node [
    id 880
    label "pi&#281;tro"
  ]
  node [
    id 881
    label "oczyszczalnia"
  ]
  node [
    id 882
    label "kopalnia"
  ]
  node [
    id 883
    label "lotnisko"
  ]
  node [
    id 884
    label "dworzec"
  ]
  node [
    id 885
    label "pomieszczenie"
  ]
  node [
    id 886
    label "post&#261;pi&#263;"
  ]
  node [
    id 887
    label "porobi&#263;"
  ]
  node [
    id 888
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 889
    label "play"
  ]
  node [
    id 890
    label "nitka"
  ]
  node [
    id 891
    label "podanie"
  ]
  node [
    id 892
    label "elektroda"
  ]
  node [
    id 893
    label "&#347;cina&#263;"
  ]
  node [
    id 894
    label "reticule"
  ]
  node [
    id 895
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 896
    label "bramka"
  ]
  node [
    id 897
    label "kort"
  ]
  node [
    id 898
    label "&#347;ci&#281;cie"
  ]
  node [
    id 899
    label "schemat"
  ]
  node [
    id 900
    label "lobowanie"
  ]
  node [
    id 901
    label "torba"
  ]
  node [
    id 902
    label "lampa_elektronowa"
  ]
  node [
    id 903
    label "organization"
  ]
  node [
    id 904
    label "lobowa&#263;"
  ]
  node [
    id 905
    label "blok"
  ]
  node [
    id 906
    label "web"
  ]
  node [
    id 907
    label "pi&#322;ka"
  ]
  node [
    id 908
    label "poda&#263;"
  ]
  node [
    id 909
    label "organizacja"
  ]
  node [
    id 910
    label "&#347;cinanie"
  ]
  node [
    id 911
    label "vane"
  ]
  node [
    id 912
    label "podawanie"
  ]
  node [
    id 913
    label "przelobowa&#263;"
  ]
  node [
    id 914
    label "kszta&#322;t"
  ]
  node [
    id 915
    label "kokonizacja"
  ]
  node [
    id 916
    label "przelobowanie"
  ]
  node [
    id 917
    label "plecionka"
  ]
  node [
    id 918
    label "plan"
  ]
  node [
    id 919
    label "rozmieszczenie"
  ]
  node [
    id 920
    label "podawa&#263;"
  ]
  node [
    id 921
    label "reserve"
  ]
  node [
    id 922
    label "przej&#347;&#263;"
  ]
  node [
    id 923
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 924
    label "eliminacje"
  ]
  node [
    id 925
    label "zawody"
  ]
  node [
    id 926
    label "Wielki_Szlem"
  ]
  node [
    id 927
    label "drive"
  ]
  node [
    id 928
    label "impreza"
  ]
  node [
    id 929
    label "pojedynek"
  ]
  node [
    id 930
    label "runda"
  ]
  node [
    id 931
    label "tournament"
  ]
  node [
    id 932
    label "rywalizacja"
  ]
  node [
    id 933
    label "entuzjasta"
  ]
  node [
    id 934
    label "sympatyk"
  ]
  node [
    id 935
    label "klient"
  ]
  node [
    id 936
    label "rekreacja"
  ]
  node [
    id 937
    label "ch&#281;tny"
  ]
  node [
    id 938
    label "sportowiec"
  ]
  node [
    id 939
    label "nieprofesjonalista"
  ]
  node [
    id 940
    label "plamka_&#380;&#243;&#322;ta"
  ]
  node [
    id 941
    label "retinopatia"
  ]
  node [
    id 942
    label "cia&#322;o_szkliste"
  ]
  node [
    id 943
    label "barwnik_&#347;wiat&#322;oczu&#322;y"
  ]
  node [
    id 944
    label "zeaksantyna"
  ]
  node [
    id 945
    label "dno_oka"
  ]
  node [
    id 946
    label "pok&#243;j"
  ]
  node [
    id 947
    label "blotka"
  ]
  node [
    id 948
    label "kie&#322;"
  ]
  node [
    id 949
    label "hotel"
  ]
  node [
    id 950
    label "krok_taneczny"
  ]
  node [
    id 951
    label "trafienie"
  ]
  node [
    id 952
    label "stopie&#324;"
  ]
  node [
    id 953
    label "zbi&#243;r"
  ]
  node [
    id 954
    label "cyfra"
  ]
  node [
    id 955
    label "toto-lotek"
  ]
  node [
    id 956
    label "three"
  ]
  node [
    id 957
    label "zaprz&#281;g"
  ]
  node [
    id 958
    label "bilard"
  ]
  node [
    id 959
    label "minuta"
  ]
  node [
    id 960
    label "p&#243;&#322;godzina"
  ]
  node [
    id 961
    label "kwadrans"
  ]
  node [
    id 962
    label "time"
  ]
  node [
    id 963
    label "jednostka_czasu"
  ]
  node [
    id 964
    label "zawarto&#347;&#263;"
  ]
  node [
    id 965
    label "k&#261;pielisko"
  ]
  node [
    id 966
    label "zbiornik"
  ]
  node [
    id 967
    label "naczynie"
  ]
  node [
    id 968
    label "region"
  ]
  node [
    id 969
    label "port"
  ]
  node [
    id 970
    label "niecka_basenowa"
  ]
  node [
    id 971
    label "zbiornik_wodny"
  ]
  node [
    id 972
    label "falownica"
  ]
  node [
    id 973
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 974
    label "&#322;a&#378;nia"
  ]
  node [
    id 975
    label "k&#261;piel"
  ]
  node [
    id 976
    label "p&#322;acz"
  ]
  node [
    id 977
    label "wyst&#281;p"
  ]
  node [
    id 978
    label "performance"
  ]
  node [
    id 979
    label "bogactwo"
  ]
  node [
    id 980
    label "mn&#243;stwo"
  ]
  node [
    id 981
    label "utw&#243;r"
  ]
  node [
    id 982
    label "show"
  ]
  node [
    id 983
    label "pokaz"
  ]
  node [
    id 984
    label "zjawisko"
  ]
  node [
    id 985
    label "szale&#324;stwo"
  ]
  node [
    id 986
    label "muzyczny"
  ]
  node [
    id 987
    label "nieklasyczny"
  ]
  node [
    id 988
    label "charakterystyczny"
  ]
  node [
    id 989
    label "rockowo"
  ]
  node [
    id 990
    label "dzia&#322;anie"
  ]
  node [
    id 991
    label "uczynnienie"
  ]
  node [
    id 992
    label "zaj&#281;ty"
  ]
  node [
    id 993
    label "czynnie"
  ]
  node [
    id 994
    label "realny"
  ]
  node [
    id 995
    label "aktywnie"
  ]
  node [
    id 996
    label "dzia&#322;alny"
  ]
  node [
    id 997
    label "wa&#380;ny"
  ]
  node [
    id 998
    label "intensywny"
  ]
  node [
    id 999
    label "ciekawy"
  ]
  node [
    id 1000
    label "faktyczny"
  ]
  node [
    id 1001
    label "dobry"
  ]
  node [
    id 1002
    label "zaanga&#380;owany"
  ]
  node [
    id 1003
    label "istotny"
  ]
  node [
    id 1004
    label "zdolny"
  ]
  node [
    id 1005
    label "uczynnianie"
  ]
  node [
    id 1006
    label "gastronomia"
  ]
  node [
    id 1007
    label "zak&#322;ad"
  ]
  node [
    id 1008
    label "bar"
  ]
  node [
    id 1009
    label "nowoczesny"
  ]
  node [
    id 1010
    label "elektroniczny"
  ]
  node [
    id 1011
    label "sieciowo"
  ]
  node [
    id 1012
    label "netowy"
  ]
  node [
    id 1013
    label "internetowo"
  ]
  node [
    id 1014
    label "zr&#243;&#380;nicowany"
  ]
  node [
    id 1015
    label "r&#243;&#380;norodnie"
  ]
  node [
    id 1016
    label "animatronika"
  ]
  node [
    id 1017
    label "cyrk"
  ]
  node [
    id 1018
    label "seans"
  ]
  node [
    id 1019
    label "dorobek"
  ]
  node [
    id 1020
    label "ekran"
  ]
  node [
    id 1021
    label "kinoteatr"
  ]
  node [
    id 1022
    label "picture"
  ]
  node [
    id 1023
    label "bioskop"
  ]
  node [
    id 1024
    label "muza"
  ]
  node [
    id 1025
    label "date"
  ]
  node [
    id 1026
    label "str&#243;j"
  ]
  node [
    id 1027
    label "spowodowa&#263;"
  ]
  node [
    id 1028
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 1029
    label "uda&#263;_si&#281;"
  ]
  node [
    id 1030
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 1031
    label "poby&#263;"
  ]
  node [
    id 1032
    label "pop&#322;ywa&#263;"
  ]
  node [
    id 1033
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 1034
    label "wynika&#263;"
  ]
  node [
    id 1035
    label "fall"
  ]
  node [
    id 1036
    label "bolt"
  ]
  node [
    id 1037
    label "rewrite"
  ]
  node [
    id 1038
    label "napisa&#263;"
  ]
  node [
    id 1039
    label "zaleci&#263;"
  ]
  node [
    id 1040
    label "wype&#322;ni&#263;"
  ]
  node [
    id 1041
    label "utrwali&#263;"
  ]
  node [
    id 1042
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 1043
    label "przekaza&#263;"
  ]
  node [
    id 1044
    label "lekarstwo"
  ]
  node [
    id 1045
    label "write"
  ]
  node [
    id 1046
    label "substitute"
  ]
  node [
    id 1047
    label "harcerstwo"
  ]
  node [
    id 1048
    label "szczep"
  ]
  node [
    id 1049
    label "ubawia&#263;"
  ]
  node [
    id 1050
    label "amuse"
  ]
  node [
    id 1051
    label "zajmowa&#263;"
  ]
  node [
    id 1052
    label "wzbudza&#263;"
  ]
  node [
    id 1053
    label "przebywa&#263;"
  ]
  node [
    id 1054
    label "zabawia&#263;"
  ]
  node [
    id 1055
    label "&#322;&#261;cznie"
  ]
  node [
    id 1056
    label "rycerz"
  ]
  node [
    id 1057
    label "lilijka_harcerska"
  ]
  node [
    id 1058
    label "wywiadowca"
  ]
  node [
    id 1059
    label "odkrywca"
  ]
  node [
    id 1060
    label "harcerz_Rzeczypospolitej"
  ]
  node [
    id 1061
    label "zast&#281;p"
  ]
  node [
    id 1062
    label "mundurek"
  ]
  node [
    id 1063
    label "&#263;wik"
  ]
  node [
    id 1064
    label "skaut"
  ]
  node [
    id 1065
    label "przesz&#322;y"
  ]
  node [
    id 1066
    label "dawno"
  ]
  node [
    id 1067
    label "dawniej"
  ]
  node [
    id 1068
    label "kombatant"
  ]
  node [
    id 1069
    label "stary"
  ]
  node [
    id 1070
    label "odleg&#322;y"
  ]
  node [
    id 1071
    label "anachroniczny"
  ]
  node [
    id 1072
    label "przestarza&#322;y"
  ]
  node [
    id 1073
    label "od_dawna"
  ]
  node [
    id 1074
    label "poprzedni"
  ]
  node [
    id 1075
    label "d&#322;ugoletni"
  ]
  node [
    id 1076
    label "wcze&#347;niejszy"
  ]
  node [
    id 1077
    label "niegdysiejszy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 110
  ]
  edge [
    source 10
    target 111
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 10
    target 121
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 10
    target 125
  ]
  edge [
    source 10
    target 126
  ]
  edge [
    source 10
    target 127
  ]
  edge [
    source 10
    target 128
  ]
  edge [
    source 10
    target 129
  ]
  edge [
    source 10
    target 130
  ]
  edge [
    source 10
    target 131
  ]
  edge [
    source 10
    target 132
  ]
  edge [
    source 10
    target 133
  ]
  edge [
    source 10
    target 134
  ]
  edge [
    source 10
    target 135
  ]
  edge [
    source 10
    target 136
  ]
  edge [
    source 10
    target 137
  ]
  edge [
    source 10
    target 138
  ]
  edge [
    source 10
    target 139
  ]
  edge [
    source 10
    target 140
  ]
  edge [
    source 10
    target 141
  ]
  edge [
    source 10
    target 142
  ]
  edge [
    source 10
    target 143
  ]
  edge [
    source 10
    target 144
  ]
  edge [
    source 10
    target 145
  ]
  edge [
    source 10
    target 146
  ]
  edge [
    source 10
    target 147
  ]
  edge [
    source 10
    target 148
  ]
  edge [
    source 10
    target 149
  ]
  edge [
    source 10
    target 150
  ]
  edge [
    source 10
    target 151
  ]
  edge [
    source 10
    target 152
  ]
  edge [
    source 10
    target 153
  ]
  edge [
    source 10
    target 154
  ]
  edge [
    source 10
    target 155
  ]
  edge [
    source 10
    target 156
  ]
  edge [
    source 10
    target 157
  ]
  edge [
    source 10
    target 158
  ]
  edge [
    source 10
    target 159
  ]
  edge [
    source 10
    target 160
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 10
    target 161
  ]
  edge [
    source 10
    target 162
  ]
  edge [
    source 10
    target 163
  ]
  edge [
    source 10
    target 164
  ]
  edge [
    source 10
    target 165
  ]
  edge [
    source 10
    target 166
  ]
  edge [
    source 10
    target 167
  ]
  edge [
    source 10
    target 168
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 10
    target 169
  ]
  edge [
    source 10
    target 170
  ]
  edge [
    source 10
    target 171
  ]
  edge [
    source 10
    target 172
  ]
  edge [
    source 10
    target 173
  ]
  edge [
    source 10
    target 174
  ]
  edge [
    source 10
    target 175
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 58
  ]
  edge [
    source 12
    target 36
  ]
  edge [
    source 12
    target 176
  ]
  edge [
    source 12
    target 177
  ]
  edge [
    source 12
    target 178
  ]
  edge [
    source 12
    target 179
  ]
  edge [
    source 12
    target 180
  ]
  edge [
    source 12
    target 181
  ]
  edge [
    source 12
    target 182
  ]
  edge [
    source 12
    target 183
  ]
  edge [
    source 12
    target 184
  ]
  edge [
    source 12
    target 185
  ]
  edge [
    source 12
    target 186
  ]
  edge [
    source 12
    target 187
  ]
  edge [
    source 12
    target 188
  ]
  edge [
    source 12
    target 189
  ]
  edge [
    source 12
    target 190
  ]
  edge [
    source 12
    target 191
  ]
  edge [
    source 12
    target 192
  ]
  edge [
    source 12
    target 193
  ]
  edge [
    source 12
    target 194
  ]
  edge [
    source 12
    target 195
  ]
  edge [
    source 12
    target 196
  ]
  edge [
    source 12
    target 197
  ]
  edge [
    source 12
    target 198
  ]
  edge [
    source 12
    target 199
  ]
  edge [
    source 12
    target 200
  ]
  edge [
    source 12
    target 201
  ]
  edge [
    source 12
    target 202
  ]
  edge [
    source 12
    target 203
  ]
  edge [
    source 12
    target 204
  ]
  edge [
    source 12
    target 205
  ]
  edge [
    source 12
    target 206
  ]
  edge [
    source 12
    target 207
  ]
  edge [
    source 12
    target 208
  ]
  edge [
    source 12
    target 209
  ]
  edge [
    source 12
    target 210
  ]
  edge [
    source 12
    target 211
  ]
  edge [
    source 12
    target 212
  ]
  edge [
    source 12
    target 213
  ]
  edge [
    source 12
    target 214
  ]
  edge [
    source 12
    target 215
  ]
  edge [
    source 12
    target 216
  ]
  edge [
    source 12
    target 217
  ]
  edge [
    source 12
    target 218
  ]
  edge [
    source 12
    target 219
  ]
  edge [
    source 12
    target 220
  ]
  edge [
    source 12
    target 221
  ]
  edge [
    source 12
    target 222
  ]
  edge [
    source 12
    target 223
  ]
  edge [
    source 12
    target 224
  ]
  edge [
    source 12
    target 225
  ]
  edge [
    source 12
    target 226
  ]
  edge [
    source 12
    target 227
  ]
  edge [
    source 12
    target 228
  ]
  edge [
    source 12
    target 229
  ]
  edge [
    source 12
    target 230
  ]
  edge [
    source 12
    target 231
  ]
  edge [
    source 12
    target 232
  ]
  edge [
    source 12
    target 233
  ]
  edge [
    source 12
    target 234
  ]
  edge [
    source 12
    target 235
  ]
  edge [
    source 12
    target 236
  ]
  edge [
    source 12
    target 237
  ]
  edge [
    source 12
    target 238
  ]
  edge [
    source 12
    target 239
  ]
  edge [
    source 12
    target 240
  ]
  edge [
    source 12
    target 241
  ]
  edge [
    source 12
    target 242
  ]
  edge [
    source 12
    target 243
  ]
  edge [
    source 12
    target 244
  ]
  edge [
    source 12
    target 245
  ]
  edge [
    source 12
    target 246
  ]
  edge [
    source 12
    target 247
  ]
  edge [
    source 12
    target 248
  ]
  edge [
    source 12
    target 249
  ]
  edge [
    source 12
    target 250
  ]
  edge [
    source 12
    target 251
  ]
  edge [
    source 12
    target 252
  ]
  edge [
    source 12
    target 253
  ]
  edge [
    source 12
    target 254
  ]
  edge [
    source 12
    target 255
  ]
  edge [
    source 12
    target 256
  ]
  edge [
    source 12
    target 257
  ]
  edge [
    source 12
    target 258
  ]
  edge [
    source 12
    target 259
  ]
  edge [
    source 12
    target 260
  ]
  edge [
    source 12
    target 261
  ]
  edge [
    source 12
    target 262
  ]
  edge [
    source 12
    target 263
  ]
  edge [
    source 12
    target 264
  ]
  edge [
    source 12
    target 265
  ]
  edge [
    source 12
    target 266
  ]
  edge [
    source 12
    target 267
  ]
  edge [
    source 12
    target 268
  ]
  edge [
    source 12
    target 269
  ]
  edge [
    source 12
    target 270
  ]
  edge [
    source 12
    target 271
  ]
  edge [
    source 12
    target 272
  ]
  edge [
    source 12
    target 273
  ]
  edge [
    source 12
    target 274
  ]
  edge [
    source 12
    target 275
  ]
  edge [
    source 12
    target 276
  ]
  edge [
    source 12
    target 277
  ]
  edge [
    source 12
    target 278
  ]
  edge [
    source 12
    target 279
  ]
  edge [
    source 12
    target 280
  ]
  edge [
    source 12
    target 281
  ]
  edge [
    source 12
    target 282
  ]
  edge [
    source 12
    target 283
  ]
  edge [
    source 12
    target 284
  ]
  edge [
    source 12
    target 285
  ]
  edge [
    source 12
    target 286
  ]
  edge [
    source 12
    target 287
  ]
  edge [
    source 12
    target 288
  ]
  edge [
    source 12
    target 289
  ]
  edge [
    source 12
    target 290
  ]
  edge [
    source 12
    target 291
  ]
  edge [
    source 12
    target 292
  ]
  edge [
    source 12
    target 293
  ]
  edge [
    source 12
    target 294
  ]
  edge [
    source 12
    target 295
  ]
  edge [
    source 12
    target 296
  ]
  edge [
    source 12
    target 297
  ]
  edge [
    source 12
    target 298
  ]
  edge [
    source 12
    target 299
  ]
  edge [
    source 12
    target 300
  ]
  edge [
    source 12
    target 301
  ]
  edge [
    source 12
    target 302
  ]
  edge [
    source 12
    target 303
  ]
  edge [
    source 12
    target 304
  ]
  edge [
    source 12
    target 305
  ]
  edge [
    source 12
    target 306
  ]
  edge [
    source 12
    target 307
  ]
  edge [
    source 12
    target 308
  ]
  edge [
    source 12
    target 309
  ]
  edge [
    source 12
    target 310
  ]
  edge [
    source 12
    target 311
  ]
  edge [
    source 12
    target 312
  ]
  edge [
    source 12
    target 313
  ]
  edge [
    source 12
    target 314
  ]
  edge [
    source 12
    target 315
  ]
  edge [
    source 12
    target 316
  ]
  edge [
    source 12
    target 317
  ]
  edge [
    source 12
    target 318
  ]
  edge [
    source 12
    target 319
  ]
  edge [
    source 12
    target 320
  ]
  edge [
    source 12
    target 321
  ]
  edge [
    source 12
    target 322
  ]
  edge [
    source 12
    target 323
  ]
  edge [
    source 12
    target 324
  ]
  edge [
    source 12
    target 325
  ]
  edge [
    source 12
    target 326
  ]
  edge [
    source 12
    target 327
  ]
  edge [
    source 12
    target 328
  ]
  edge [
    source 12
    target 329
  ]
  edge [
    source 12
    target 330
  ]
  edge [
    source 12
    target 331
  ]
  edge [
    source 12
    target 332
  ]
  edge [
    source 12
    target 333
  ]
  edge [
    source 12
    target 334
  ]
  edge [
    source 12
    target 335
  ]
  edge [
    source 12
    target 336
  ]
  edge [
    source 12
    target 337
  ]
  edge [
    source 12
    target 338
  ]
  edge [
    source 12
    target 339
  ]
  edge [
    source 12
    target 340
  ]
  edge [
    source 12
    target 341
  ]
  edge [
    source 12
    target 342
  ]
  edge [
    source 12
    target 343
  ]
  edge [
    source 12
    target 344
  ]
  edge [
    source 12
    target 345
  ]
  edge [
    source 12
    target 346
  ]
  edge [
    source 12
    target 347
  ]
  edge [
    source 12
    target 348
  ]
  edge [
    source 12
    target 349
  ]
  edge [
    source 12
    target 350
  ]
  edge [
    source 12
    target 351
  ]
  edge [
    source 12
    target 352
  ]
  edge [
    source 12
    target 353
  ]
  edge [
    source 12
    target 354
  ]
  edge [
    source 12
    target 355
  ]
  edge [
    source 12
    target 356
  ]
  edge [
    source 12
    target 357
  ]
  edge [
    source 12
    target 358
  ]
  edge [
    source 12
    target 359
  ]
  edge [
    source 12
    target 360
  ]
  edge [
    source 12
    target 361
  ]
  edge [
    source 12
    target 362
  ]
  edge [
    source 12
    target 363
  ]
  edge [
    source 12
    target 364
  ]
  edge [
    source 12
    target 365
  ]
  edge [
    source 12
    target 366
  ]
  edge [
    source 12
    target 367
  ]
  edge [
    source 12
    target 368
  ]
  edge [
    source 12
    target 369
  ]
  edge [
    source 12
    target 370
  ]
  edge [
    source 12
    target 371
  ]
  edge [
    source 12
    target 372
  ]
  edge [
    source 12
    target 373
  ]
  edge [
    source 12
    target 374
  ]
  edge [
    source 12
    target 375
  ]
  edge [
    source 12
    target 376
  ]
  edge [
    source 12
    target 377
  ]
  edge [
    source 12
    target 378
  ]
  edge [
    source 12
    target 379
  ]
  edge [
    source 12
    target 380
  ]
  edge [
    source 12
    target 381
  ]
  edge [
    source 12
    target 382
  ]
  edge [
    source 12
    target 383
  ]
  edge [
    source 12
    target 384
  ]
  edge [
    source 12
    target 385
  ]
  edge [
    source 12
    target 386
  ]
  edge [
    source 12
    target 387
  ]
  edge [
    source 12
    target 388
  ]
  edge [
    source 12
    target 389
  ]
  edge [
    source 12
    target 390
  ]
  edge [
    source 12
    target 391
  ]
  edge [
    source 12
    target 392
  ]
  edge [
    source 12
    target 393
  ]
  edge [
    source 12
    target 394
  ]
  edge [
    source 12
    target 395
  ]
  edge [
    source 12
    target 396
  ]
  edge [
    source 12
    target 397
  ]
  edge [
    source 12
    target 398
  ]
  edge [
    source 12
    target 399
  ]
  edge [
    source 12
    target 400
  ]
  edge [
    source 12
    target 401
  ]
  edge [
    source 12
    target 402
  ]
  edge [
    source 12
    target 403
  ]
  edge [
    source 12
    target 404
  ]
  edge [
    source 12
    target 405
  ]
  edge [
    source 12
    target 406
  ]
  edge [
    source 12
    target 407
  ]
  edge [
    source 12
    target 408
  ]
  edge [
    source 12
    target 409
  ]
  edge [
    source 12
    target 410
  ]
  edge [
    source 12
    target 411
  ]
  edge [
    source 12
    target 412
  ]
  edge [
    source 12
    target 413
  ]
  edge [
    source 12
    target 414
  ]
  edge [
    source 12
    target 415
  ]
  edge [
    source 12
    target 416
  ]
  edge [
    source 12
    target 417
  ]
  edge [
    source 12
    target 418
  ]
  edge [
    source 12
    target 419
  ]
  edge [
    source 12
    target 420
  ]
  edge [
    source 12
    target 421
  ]
  edge [
    source 12
    target 422
  ]
  edge [
    source 12
    target 423
  ]
  edge [
    source 12
    target 424
  ]
  edge [
    source 12
    target 425
  ]
  edge [
    source 12
    target 426
  ]
  edge [
    source 12
    target 427
  ]
  edge [
    source 12
    target 428
  ]
  edge [
    source 12
    target 429
  ]
  edge [
    source 12
    target 430
  ]
  edge [
    source 12
    target 431
  ]
  edge [
    source 12
    target 432
  ]
  edge [
    source 12
    target 433
  ]
  edge [
    source 12
    target 434
  ]
  edge [
    source 12
    target 435
  ]
  edge [
    source 12
    target 436
  ]
  edge [
    source 12
    target 437
  ]
  edge [
    source 12
    target 438
  ]
  edge [
    source 12
    target 439
  ]
  edge [
    source 12
    target 440
  ]
  edge [
    source 12
    target 441
  ]
  edge [
    source 12
    target 442
  ]
  edge [
    source 12
    target 443
  ]
  edge [
    source 12
    target 444
  ]
  edge [
    source 12
    target 445
  ]
  edge [
    source 12
    target 446
  ]
  edge [
    source 12
    target 447
  ]
  edge [
    source 12
    target 448
  ]
  edge [
    source 12
    target 449
  ]
  edge [
    source 12
    target 450
  ]
  edge [
    source 12
    target 451
  ]
  edge [
    source 12
    target 452
  ]
  edge [
    source 12
    target 453
  ]
  edge [
    source 12
    target 454
  ]
  edge [
    source 12
    target 455
  ]
  edge [
    source 12
    target 456
  ]
  edge [
    source 12
    target 457
  ]
  edge [
    source 12
    target 458
  ]
  edge [
    source 12
    target 459
  ]
  edge [
    source 12
    target 460
  ]
  edge [
    source 12
    target 461
  ]
  edge [
    source 12
    target 462
  ]
  edge [
    source 12
    target 463
  ]
  edge [
    source 12
    target 464
  ]
  edge [
    source 12
    target 465
  ]
  edge [
    source 12
    target 466
  ]
  edge [
    source 12
    target 467
  ]
  edge [
    source 12
    target 468
  ]
  edge [
    source 12
    target 469
  ]
  edge [
    source 12
    target 470
  ]
  edge [
    source 12
    target 471
  ]
  edge [
    source 12
    target 472
  ]
  edge [
    source 12
    target 473
  ]
  edge [
    source 12
    target 474
  ]
  edge [
    source 12
    target 475
  ]
  edge [
    source 12
    target 476
  ]
  edge [
    source 12
    target 477
  ]
  edge [
    source 12
    target 478
  ]
  edge [
    source 12
    target 479
  ]
  edge [
    source 12
    target 480
  ]
  edge [
    source 12
    target 481
  ]
  edge [
    source 12
    target 482
  ]
  edge [
    source 12
    target 483
  ]
  edge [
    source 12
    target 484
  ]
  edge [
    source 12
    target 485
  ]
  edge [
    source 12
    target 486
  ]
  edge [
    source 12
    target 487
  ]
  edge [
    source 12
    target 488
  ]
  edge [
    source 12
    target 489
  ]
  edge [
    source 12
    target 490
  ]
  edge [
    source 12
    target 491
  ]
  edge [
    source 12
    target 492
  ]
  edge [
    source 12
    target 493
  ]
  edge [
    source 12
    target 494
  ]
  edge [
    source 12
    target 495
  ]
  edge [
    source 12
    target 496
  ]
  edge [
    source 12
    target 497
  ]
  edge [
    source 12
    target 498
  ]
  edge [
    source 12
    target 499
  ]
  edge [
    source 12
    target 500
  ]
  edge [
    source 12
    target 501
  ]
  edge [
    source 12
    target 502
  ]
  edge [
    source 12
    target 503
  ]
  edge [
    source 12
    target 504
  ]
  edge [
    source 12
    target 505
  ]
  edge [
    source 12
    target 506
  ]
  edge [
    source 12
    target 507
  ]
  edge [
    source 12
    target 508
  ]
  edge [
    source 12
    target 509
  ]
  edge [
    source 12
    target 510
  ]
  edge [
    source 12
    target 511
  ]
  edge [
    source 12
    target 512
  ]
  edge [
    source 12
    target 513
  ]
  edge [
    source 12
    target 514
  ]
  edge [
    source 12
    target 515
  ]
  edge [
    source 12
    target 516
  ]
  edge [
    source 12
    target 517
  ]
  edge [
    source 12
    target 518
  ]
  edge [
    source 12
    target 519
  ]
  edge [
    source 12
    target 520
  ]
  edge [
    source 12
    target 521
  ]
  edge [
    source 12
    target 522
  ]
  edge [
    source 12
    target 523
  ]
  edge [
    source 12
    target 524
  ]
  edge [
    source 12
    target 525
  ]
  edge [
    source 12
    target 526
  ]
  edge [
    source 12
    target 527
  ]
  edge [
    source 12
    target 528
  ]
  edge [
    source 12
    target 529
  ]
  edge [
    source 12
    target 530
  ]
  edge [
    source 12
    target 531
  ]
  edge [
    source 12
    target 532
  ]
  edge [
    source 12
    target 533
  ]
  edge [
    source 12
    target 534
  ]
  edge [
    source 12
    target 535
  ]
  edge [
    source 12
    target 536
  ]
  edge [
    source 12
    target 537
  ]
  edge [
    source 12
    target 538
  ]
  edge [
    source 12
    target 539
  ]
  edge [
    source 12
    target 540
  ]
  edge [
    source 12
    target 541
  ]
  edge [
    source 12
    target 542
  ]
  edge [
    source 12
    target 543
  ]
  edge [
    source 12
    target 544
  ]
  edge [
    source 12
    target 545
  ]
  edge [
    source 12
    target 546
  ]
  edge [
    source 12
    target 547
  ]
  edge [
    source 12
    target 548
  ]
  edge [
    source 12
    target 549
  ]
  edge [
    source 12
    target 550
  ]
  edge [
    source 12
    target 551
  ]
  edge [
    source 12
    target 552
  ]
  edge [
    source 12
    target 553
  ]
  edge [
    source 12
    target 554
  ]
  edge [
    source 12
    target 555
  ]
  edge [
    source 12
    target 556
  ]
  edge [
    source 12
    target 557
  ]
  edge [
    source 12
    target 558
  ]
  edge [
    source 12
    target 559
  ]
  edge [
    source 12
    target 560
  ]
  edge [
    source 12
    target 561
  ]
  edge [
    source 12
    target 562
  ]
  edge [
    source 12
    target 563
  ]
  edge [
    source 12
    target 564
  ]
  edge [
    source 12
    target 565
  ]
  edge [
    source 12
    target 566
  ]
  edge [
    source 12
    target 567
  ]
  edge [
    source 12
    target 568
  ]
  edge [
    source 12
    target 569
  ]
  edge [
    source 12
    target 570
  ]
  edge [
    source 12
    target 571
  ]
  edge [
    source 12
    target 572
  ]
  edge [
    source 12
    target 573
  ]
  edge [
    source 12
    target 574
  ]
  edge [
    source 12
    target 575
  ]
  edge [
    source 12
    target 576
  ]
  edge [
    source 12
    target 577
  ]
  edge [
    source 12
    target 578
  ]
  edge [
    source 12
    target 579
  ]
  edge [
    source 12
    target 580
  ]
  edge [
    source 12
    target 581
  ]
  edge [
    source 12
    target 582
  ]
  edge [
    source 12
    target 583
  ]
  edge [
    source 12
    target 584
  ]
  edge [
    source 12
    target 585
  ]
  edge [
    source 12
    target 586
  ]
  edge [
    source 12
    target 587
  ]
  edge [
    source 12
    target 588
  ]
  edge [
    source 12
    target 589
  ]
  edge [
    source 12
    target 590
  ]
  edge [
    source 12
    target 591
  ]
  edge [
    source 12
    target 592
  ]
  edge [
    source 12
    target 593
  ]
  edge [
    source 12
    target 594
  ]
  edge [
    source 12
    target 595
  ]
  edge [
    source 12
    target 596
  ]
  edge [
    source 12
    target 597
  ]
  edge [
    source 12
    target 598
  ]
  edge [
    source 12
    target 599
  ]
  edge [
    source 12
    target 600
  ]
  edge [
    source 12
    target 601
  ]
  edge [
    source 12
    target 602
  ]
  edge [
    source 12
    target 603
  ]
  edge [
    source 12
    target 604
  ]
  edge [
    source 12
    target 605
  ]
  edge [
    source 12
    target 606
  ]
  edge [
    source 12
    target 607
  ]
  edge [
    source 12
    target 608
  ]
  edge [
    source 12
    target 609
  ]
  edge [
    source 12
    target 610
  ]
  edge [
    source 12
    target 611
  ]
  edge [
    source 12
    target 612
  ]
  edge [
    source 12
    target 613
  ]
  edge [
    source 12
    target 614
  ]
  edge [
    source 12
    target 615
  ]
  edge [
    source 12
    target 616
  ]
  edge [
    source 12
    target 617
  ]
  edge [
    source 12
    target 618
  ]
  edge [
    source 12
    target 619
  ]
  edge [
    source 12
    target 620
  ]
  edge [
    source 12
    target 621
  ]
  edge [
    source 12
    target 622
  ]
  edge [
    source 12
    target 623
  ]
  edge [
    source 12
    target 624
  ]
  edge [
    source 12
    target 625
  ]
  edge [
    source 12
    target 626
  ]
  edge [
    source 12
    target 627
  ]
  edge [
    source 12
    target 628
  ]
  edge [
    source 12
    target 629
  ]
  edge [
    source 12
    target 630
  ]
  edge [
    source 12
    target 631
  ]
  edge [
    source 12
    target 632
  ]
  edge [
    source 12
    target 633
  ]
  edge [
    source 12
    target 634
  ]
  edge [
    source 12
    target 635
  ]
  edge [
    source 12
    target 636
  ]
  edge [
    source 12
    target 637
  ]
  edge [
    source 12
    target 638
  ]
  edge [
    source 12
    target 639
  ]
  edge [
    source 12
    target 640
  ]
  edge [
    source 12
    target 641
  ]
  edge [
    source 12
    target 642
  ]
  edge [
    source 12
    target 643
  ]
  edge [
    source 12
    target 644
  ]
  edge [
    source 12
    target 645
  ]
  edge [
    source 12
    target 646
  ]
  edge [
    source 12
    target 647
  ]
  edge [
    source 12
    target 648
  ]
  edge [
    source 12
    target 649
  ]
  edge [
    source 12
    target 650
  ]
  edge [
    source 12
    target 651
  ]
  edge [
    source 12
    target 652
  ]
  edge [
    source 12
    target 653
  ]
  edge [
    source 12
    target 654
  ]
  edge [
    source 12
    target 655
  ]
  edge [
    source 12
    target 656
  ]
  edge [
    source 12
    target 657
  ]
  edge [
    source 12
    target 658
  ]
  edge [
    source 12
    target 659
  ]
  edge [
    source 12
    target 660
  ]
  edge [
    source 12
    target 661
  ]
  edge [
    source 12
    target 662
  ]
  edge [
    source 12
    target 663
  ]
  edge [
    source 12
    target 664
  ]
  edge [
    source 12
    target 665
  ]
  edge [
    source 12
    target 666
  ]
  edge [
    source 12
    target 667
  ]
  edge [
    source 12
    target 668
  ]
  edge [
    source 12
    target 669
  ]
  edge [
    source 12
    target 670
  ]
  edge [
    source 12
    target 671
  ]
  edge [
    source 12
    target 672
  ]
  edge [
    source 12
    target 673
  ]
  edge [
    source 12
    target 674
  ]
  edge [
    source 12
    target 675
  ]
  edge [
    source 12
    target 676
  ]
  edge [
    source 12
    target 677
  ]
  edge [
    source 12
    target 678
  ]
  edge [
    source 12
    target 679
  ]
  edge [
    source 12
    target 680
  ]
  edge [
    source 12
    target 681
  ]
  edge [
    source 12
    target 682
  ]
  edge [
    source 12
    target 683
  ]
  edge [
    source 12
    target 684
  ]
  edge [
    source 12
    target 685
  ]
  edge [
    source 12
    target 686
  ]
  edge [
    source 12
    target 687
  ]
  edge [
    source 12
    target 688
  ]
  edge [
    source 12
    target 689
  ]
  edge [
    source 12
    target 690
  ]
  edge [
    source 12
    target 691
  ]
  edge [
    source 12
    target 692
  ]
  edge [
    source 12
    target 693
  ]
  edge [
    source 12
    target 694
  ]
  edge [
    source 12
    target 695
  ]
  edge [
    source 12
    target 696
  ]
  edge [
    source 12
    target 697
  ]
  edge [
    source 12
    target 698
  ]
  edge [
    source 12
    target 699
  ]
  edge [
    source 12
    target 700
  ]
  edge [
    source 12
    target 701
  ]
  edge [
    source 12
    target 702
  ]
  edge [
    source 12
    target 703
  ]
  edge [
    source 12
    target 704
  ]
  edge [
    source 12
    target 705
  ]
  edge [
    source 12
    target 706
  ]
  edge [
    source 12
    target 707
  ]
  edge [
    source 12
    target 708
  ]
  edge [
    source 12
    target 709
  ]
  edge [
    source 12
    target 710
  ]
  edge [
    source 12
    target 711
  ]
  edge [
    source 12
    target 712
  ]
  edge [
    source 12
    target 713
  ]
  edge [
    source 12
    target 714
  ]
  edge [
    source 12
    target 715
  ]
  edge [
    source 12
    target 716
  ]
  edge [
    source 12
    target 717
  ]
  edge [
    source 12
    target 718
  ]
  edge [
    source 12
    target 719
  ]
  edge [
    source 12
    target 720
  ]
  edge [
    source 12
    target 721
  ]
  edge [
    source 12
    target 722
  ]
  edge [
    source 12
    target 723
  ]
  edge [
    source 12
    target 724
  ]
  edge [
    source 12
    target 725
  ]
  edge [
    source 12
    target 726
  ]
  edge [
    source 12
    target 727
  ]
  edge [
    source 12
    target 728
  ]
  edge [
    source 12
    target 729
  ]
  edge [
    source 12
    target 730
  ]
  edge [
    source 12
    target 731
  ]
  edge [
    source 12
    target 732
  ]
  edge [
    source 12
    target 733
  ]
  edge [
    source 12
    target 734
  ]
  edge [
    source 12
    target 735
  ]
  edge [
    source 12
    target 736
  ]
  edge [
    source 12
    target 737
  ]
  edge [
    source 12
    target 738
  ]
  edge [
    source 12
    target 739
  ]
  edge [
    source 12
    target 740
  ]
  edge [
    source 12
    target 741
  ]
  edge [
    source 12
    target 742
  ]
  edge [
    source 12
    target 743
  ]
  edge [
    source 12
    target 744
  ]
  edge [
    source 12
    target 745
  ]
  edge [
    source 12
    target 746
  ]
  edge [
    source 12
    target 747
  ]
  edge [
    source 12
    target 748
  ]
  edge [
    source 12
    target 749
  ]
  edge [
    source 12
    target 750
  ]
  edge [
    source 12
    target 751
  ]
  edge [
    source 12
    target 752
  ]
  edge [
    source 12
    target 753
  ]
  edge [
    source 12
    target 754
  ]
  edge [
    source 12
    target 755
  ]
  edge [
    source 12
    target 756
  ]
  edge [
    source 12
    target 757
  ]
  edge [
    source 12
    target 758
  ]
  edge [
    source 12
    target 759
  ]
  edge [
    source 12
    target 760
  ]
  edge [
    source 12
    target 761
  ]
  edge [
    source 12
    target 762
  ]
  edge [
    source 12
    target 763
  ]
  edge [
    source 12
    target 764
  ]
  edge [
    source 12
    target 765
  ]
  edge [
    source 12
    target 766
  ]
  edge [
    source 12
    target 45
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 767
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 768
  ]
  edge [
    source 14
    target 769
  ]
  edge [
    source 14
    target 770
  ]
  edge [
    source 14
    target 771
  ]
  edge [
    source 14
    target 772
  ]
  edge [
    source 14
    target 773
  ]
  edge [
    source 14
    target 774
  ]
  edge [
    source 14
    target 775
  ]
  edge [
    source 14
    target 776
  ]
  edge [
    source 14
    target 777
  ]
  edge [
    source 14
    target 778
  ]
  edge [
    source 14
    target 25
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 30
  ]
  edge [
    source 15
    target 31
  ]
  edge [
    source 15
    target 51
  ]
  edge [
    source 15
    target 52
  ]
  edge [
    source 15
    target 779
  ]
  edge [
    source 15
    target 780
  ]
  edge [
    source 15
    target 781
  ]
  edge [
    source 15
    target 782
  ]
  edge [
    source 15
    target 77
  ]
  edge [
    source 15
    target 783
  ]
  edge [
    source 15
    target 784
  ]
  edge [
    source 15
    target 785
  ]
  edge [
    source 15
    target 786
  ]
  edge [
    source 15
    target 787
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 788
  ]
  edge [
    source 16
    target 789
  ]
  edge [
    source 16
    target 790
  ]
  edge [
    source 16
    target 55
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 88
  ]
  edge [
    source 17
    target 791
  ]
  edge [
    source 17
    target 792
  ]
  edge [
    source 17
    target 793
  ]
  edge [
    source 17
    target 794
  ]
  edge [
    source 17
    target 795
  ]
  edge [
    source 17
    target 796
  ]
  edge [
    source 17
    target 797
  ]
  edge [
    source 17
    target 798
  ]
  edge [
    source 17
    target 799
  ]
  edge [
    source 17
    target 800
  ]
  edge [
    source 17
    target 801
  ]
  edge [
    source 17
    target 802
  ]
  edge [
    source 17
    target 803
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 55
  ]
  edge [
    source 18
    target 56
  ]
  edge [
    source 18
    target 804
  ]
  edge [
    source 18
    target 805
  ]
  edge [
    source 18
    target 45
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 806
  ]
  edge [
    source 19
    target 807
  ]
  edge [
    source 19
    target 808
  ]
  edge [
    source 19
    target 809
  ]
  edge [
    source 19
    target 810
  ]
  edge [
    source 19
    target 811
  ]
  edge [
    source 19
    target 37
  ]
  edge [
    source 19
    target 63
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 812
  ]
  edge [
    source 20
    target 813
  ]
  edge [
    source 20
    target 814
  ]
  edge [
    source 20
    target 815
  ]
  edge [
    source 20
    target 816
  ]
  edge [
    source 20
    target 817
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 818
  ]
  edge [
    source 21
    target 819
  ]
  edge [
    source 21
    target 820
  ]
  edge [
    source 21
    target 821
  ]
  edge [
    source 21
    target 822
  ]
  edge [
    source 21
    target 823
  ]
  edge [
    source 21
    target 824
  ]
  edge [
    source 21
    target 825
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 826
  ]
  edge [
    source 22
    target 827
  ]
  edge [
    source 22
    target 828
  ]
  edge [
    source 22
    target 829
  ]
  edge [
    source 22
    target 830
  ]
  edge [
    source 22
    target 831
  ]
  edge [
    source 22
    target 832
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 33
  ]
  edge [
    source 24
    target 34
  ]
  edge [
    source 24
    target 56
  ]
  edge [
    source 24
    target 57
  ]
  edge [
    source 24
    target 833
  ]
  edge [
    source 24
    target 32
  ]
  edge [
    source 24
    target 50
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 834
  ]
  edge [
    source 25
    target 835
  ]
  edge [
    source 25
    target 836
  ]
  edge [
    source 25
    target 837
  ]
  edge [
    source 25
    target 838
  ]
  edge [
    source 25
    target 839
  ]
  edge [
    source 25
    target 840
  ]
  edge [
    source 25
    target 841
  ]
  edge [
    source 25
    target 842
  ]
  edge [
    source 25
    target 843
  ]
  edge [
    source 25
    target 844
  ]
  edge [
    source 25
    target 845
  ]
  edge [
    source 25
    target 846
  ]
  edge [
    source 25
    target 847
  ]
  edge [
    source 25
    target 848
  ]
  edge [
    source 25
    target 849
  ]
  edge [
    source 25
    target 850
  ]
  edge [
    source 25
    target 851
  ]
  edge [
    source 25
    target 852
  ]
  edge [
    source 25
    target 853
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 44
  ]
  edge [
    source 27
    target 854
  ]
  edge [
    source 27
    target 855
  ]
  edge [
    source 27
    target 112
  ]
  edge [
    source 27
    target 856
  ]
  edge [
    source 27
    target 857
  ]
  edge [
    source 27
    target 858
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 65
  ]
  edge [
    source 28
    target 859
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 37
  ]
  edge [
    source 29
    target 38
  ]
  edge [
    source 29
    target 860
  ]
  edge [
    source 29
    target 861
  ]
  edge [
    source 29
    target 862
  ]
  edge [
    source 30
    target 863
  ]
  edge [
    source 30
    target 864
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 865
  ]
  edge [
    source 31
    target 866
  ]
  edge [
    source 31
    target 867
  ]
  edge [
    source 31
    target 868
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 45
  ]
  edge [
    source 32
    target 46
  ]
  edge [
    source 32
    target 869
  ]
  edge [
    source 32
    target 870
  ]
  edge [
    source 32
    target 871
  ]
  edge [
    source 32
    target 872
  ]
  edge [
    source 32
    target 873
  ]
  edge [
    source 32
    target 874
  ]
  edge [
    source 32
    target 34
  ]
  edge [
    source 33
    target 875
  ]
  edge [
    source 33
    target 876
  ]
  edge [
    source 33
    target 877
  ]
  edge [
    source 33
    target 878
  ]
  edge [
    source 33
    target 879
  ]
  edge [
    source 33
    target 880
  ]
  edge [
    source 33
    target 881
  ]
  edge [
    source 33
    target 882
  ]
  edge [
    source 33
    target 883
  ]
  edge [
    source 33
    target 884
  ]
  edge [
    source 33
    target 885
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 886
  ]
  edge [
    source 34
    target 887
  ]
  edge [
    source 34
    target 888
  ]
  edge [
    source 34
    target 889
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 890
  ]
  edge [
    source 35
    target 891
  ]
  edge [
    source 35
    target 892
  ]
  edge [
    source 35
    target 893
  ]
  edge [
    source 35
    target 894
  ]
  edge [
    source 35
    target 895
  ]
  edge [
    source 35
    target 896
  ]
  edge [
    source 35
    target 897
  ]
  edge [
    source 35
    target 898
  ]
  edge [
    source 35
    target 899
  ]
  edge [
    source 35
    target 900
  ]
  edge [
    source 35
    target 901
  ]
  edge [
    source 35
    target 902
  ]
  edge [
    source 35
    target 903
  ]
  edge [
    source 35
    target 904
  ]
  edge [
    source 35
    target 905
  ]
  edge [
    source 35
    target 906
  ]
  edge [
    source 35
    target 907
  ]
  edge [
    source 35
    target 908
  ]
  edge [
    source 35
    target 909
  ]
  edge [
    source 35
    target 910
  ]
  edge [
    source 35
    target 911
  ]
  edge [
    source 35
    target 912
  ]
  edge [
    source 35
    target 913
  ]
  edge [
    source 35
    target 914
  ]
  edge [
    source 35
    target 915
  ]
  edge [
    source 35
    target 916
  ]
  edge [
    source 35
    target 917
  ]
  edge [
    source 35
    target 918
  ]
  edge [
    source 35
    target 919
  ]
  edge [
    source 35
    target 920
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 59
  ]
  edge [
    source 37
    target 63
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 44
  ]
  edge [
    source 38
    target 921
  ]
  edge [
    source 38
    target 922
  ]
  edge [
    source 38
    target 923
  ]
  edge [
    source 38
    target 52
  ]
  edge [
    source 38
    target 53
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 48
  ]
  edge [
    source 39
    target 59
  ]
  edge [
    source 39
    target 60
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 924
  ]
  edge [
    source 40
    target 925
  ]
  edge [
    source 40
    target 926
  ]
  edge [
    source 40
    target 927
  ]
  edge [
    source 40
    target 928
  ]
  edge [
    source 40
    target 929
  ]
  edge [
    source 40
    target 930
  ]
  edge [
    source 40
    target 931
  ]
  edge [
    source 40
    target 932
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 933
  ]
  edge [
    source 41
    target 88
  ]
  edge [
    source 41
    target 934
  ]
  edge [
    source 41
    target 935
  ]
  edge [
    source 41
    target 936
  ]
  edge [
    source 41
    target 937
  ]
  edge [
    source 41
    target 938
  ]
  edge [
    source 41
    target 939
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 908
  ]
  edge [
    source 42
    target 940
  ]
  edge [
    source 42
    target 891
  ]
  edge [
    source 42
    target 900
  ]
  edge [
    source 42
    target 916
  ]
  edge [
    source 42
    target 893
  ]
  edge [
    source 42
    target 910
  ]
  edge [
    source 42
    target 912
  ]
  edge [
    source 42
    target 905
  ]
  edge [
    source 42
    target 904
  ]
  edge [
    source 42
    target 895
  ]
  edge [
    source 42
    target 941
  ]
  edge [
    source 42
    target 942
  ]
  edge [
    source 42
    target 913
  ]
  edge [
    source 42
    target 898
  ]
  edge [
    source 42
    target 943
  ]
  edge [
    source 42
    target 944
  ]
  edge [
    source 42
    target 920
  ]
  edge [
    source 42
    target 907
  ]
  edge [
    source 42
    target 945
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 946
  ]
  edge [
    source 43
    target 947
  ]
  edge [
    source 43
    target 948
  ]
  edge [
    source 43
    target 871
  ]
  edge [
    source 43
    target 949
  ]
  edge [
    source 43
    target 950
  ]
  edge [
    source 43
    target 951
  ]
  edge [
    source 43
    target 952
  ]
  edge [
    source 43
    target 953
  ]
  edge [
    source 43
    target 954
  ]
  edge [
    source 43
    target 955
  ]
  edge [
    source 43
    target 956
  ]
  edge [
    source 43
    target 957
  ]
  edge [
    source 43
    target 958
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 47
  ]
  edge [
    source 44
    target 959
  ]
  edge [
    source 44
    target 855
  ]
  edge [
    source 44
    target 112
  ]
  edge [
    source 44
    target 960
  ]
  edge [
    source 44
    target 961
  ]
  edge [
    source 44
    target 962
  ]
  edge [
    source 44
    target 963
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 47
  ]
  edge [
    source 45
    target 964
  ]
  edge [
    source 45
    target 871
  ]
  edge [
    source 45
    target 965
  ]
  edge [
    source 45
    target 966
  ]
  edge [
    source 45
    target 967
  ]
  edge [
    source 45
    target 968
  ]
  edge [
    source 45
    target 969
  ]
  edge [
    source 45
    target 970
  ]
  edge [
    source 45
    target 971
  ]
  edge [
    source 45
    target 872
  ]
  edge [
    source 45
    target 972
  ]
  edge [
    source 45
    target 973
  ]
  edge [
    source 46
    target 974
  ]
  edge [
    source 46
    target 975
  ]
  edge [
    source 46
    target 58
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 976
  ]
  edge [
    source 48
    target 977
  ]
  edge [
    source 48
    target 978
  ]
  edge [
    source 48
    target 979
  ]
  edge [
    source 48
    target 980
  ]
  edge [
    source 48
    target 981
  ]
  edge [
    source 48
    target 982
  ]
  edge [
    source 48
    target 983
  ]
  edge [
    source 48
    target 984
  ]
  edge [
    source 48
    target 985
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 986
  ]
  edge [
    source 49
    target 987
  ]
  edge [
    source 49
    target 988
  ]
  edge [
    source 49
    target 989
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 51
    target 990
  ]
  edge [
    source 51
    target 991
  ]
  edge [
    source 51
    target 992
  ]
  edge [
    source 51
    target 993
  ]
  edge [
    source 51
    target 994
  ]
  edge [
    source 51
    target 995
  ]
  edge [
    source 51
    target 996
  ]
  edge [
    source 51
    target 997
  ]
  edge [
    source 51
    target 998
  ]
  edge [
    source 51
    target 999
  ]
  edge [
    source 51
    target 1000
  ]
  edge [
    source 51
    target 1001
  ]
  edge [
    source 51
    target 1002
  ]
  edge [
    source 51
    target 1003
  ]
  edge [
    source 51
    target 1004
  ]
  edge [
    source 51
    target 1005
  ]
  edge [
    source 51
    target 55
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 1006
  ]
  edge [
    source 52
    target 1007
  ]
  edge [
    source 52
    target 1008
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 1009
  ]
  edge [
    source 53
    target 1010
  ]
  edge [
    source 53
    target 1011
  ]
  edge [
    source 53
    target 1012
  ]
  edge [
    source 53
    target 1013
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 55
    target 1014
  ]
  edge [
    source 55
    target 1015
  ]
  edge [
    source 56
    target 1016
  ]
  edge [
    source 56
    target 1017
  ]
  edge [
    source 56
    target 1018
  ]
  edge [
    source 56
    target 1019
  ]
  edge [
    source 56
    target 1020
  ]
  edge [
    source 56
    target 879
  ]
  edge [
    source 56
    target 1021
  ]
  edge [
    source 56
    target 1022
  ]
  edge [
    source 56
    target 1023
  ]
  edge [
    source 56
    target 802
  ]
  edge [
    source 56
    target 1024
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 58
    target 1025
  ]
  edge [
    source 58
    target 1026
  ]
  edge [
    source 58
    target 112
  ]
  edge [
    source 58
    target 888
  ]
  edge [
    source 58
    target 1027
  ]
  edge [
    source 58
    target 1028
  ]
  edge [
    source 58
    target 1029
  ]
  edge [
    source 58
    target 1030
  ]
  edge [
    source 58
    target 1031
  ]
  edge [
    source 58
    target 1032
  ]
  edge [
    source 58
    target 1033
  ]
  edge [
    source 58
    target 1034
  ]
  edge [
    source 58
    target 1035
  ]
  edge [
    source 58
    target 1036
  ]
  edge [
    source 59
    target 1037
  ]
  edge [
    source 59
    target 1027
  ]
  edge [
    source 59
    target 1038
  ]
  edge [
    source 59
    target 1039
  ]
  edge [
    source 59
    target 1040
  ]
  edge [
    source 59
    target 1041
  ]
  edge [
    source 59
    target 1042
  ]
  edge [
    source 59
    target 1043
  ]
  edge [
    source 59
    target 1044
  ]
  edge [
    source 59
    target 1045
  ]
  edge [
    source 59
    target 1046
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 1047
  ]
  edge [
    source 60
    target 1048
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 1049
  ]
  edge [
    source 61
    target 1050
  ]
  edge [
    source 61
    target 1051
  ]
  edge [
    source 61
    target 1052
  ]
  edge [
    source 61
    target 1053
  ]
  edge [
    source 61
    target 824
  ]
  edge [
    source 61
    target 1054
  ]
  edge [
    source 61
    target 889
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 1055
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 1056
  ]
  edge [
    source 63
    target 1057
  ]
  edge [
    source 63
    target 1058
  ]
  edge [
    source 63
    target 1059
  ]
  edge [
    source 63
    target 1060
  ]
  edge [
    source 63
    target 101
  ]
  edge [
    source 63
    target 1061
  ]
  edge [
    source 63
    target 1062
  ]
  edge [
    source 63
    target 1063
  ]
  edge [
    source 63
    target 1064
  ]
  edge [
    source 64
    target 1065
  ]
  edge [
    source 64
    target 1066
  ]
  edge [
    source 64
    target 1067
  ]
  edge [
    source 64
    target 1068
  ]
  edge [
    source 64
    target 1069
  ]
  edge [
    source 64
    target 1070
  ]
  edge [
    source 64
    target 1071
  ]
  edge [
    source 64
    target 1072
  ]
  edge [
    source 64
    target 1073
  ]
  edge [
    source 64
    target 1074
  ]
  edge [
    source 64
    target 1075
  ]
  edge [
    source 64
    target 1076
  ]
  edge [
    source 64
    target 1077
  ]
]
