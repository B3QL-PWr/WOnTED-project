graph [
  maxDegree 33
  minDegree 1
  meanDegree 1.9692307692307693
  density 0.03076923076923077
  graphCliqueNumber 2
  node [
    id 0
    label "kultura"
    origin "text"
  ]
  node [
    id 1
    label "wspiera&#263;"
    origin "text"
  ]
  node [
    id 2
    label "lokalny"
    origin "text"
  ]
  node [
    id 3
    label "marek"
    origin "text"
  ]
  node [
    id 4
    label "by&#263;"
    origin "text"
  ]
  node [
    id 5
    label "obecna"
    origin "text"
  ]
  node [
    id 6
    label "niemcy"
    origin "text"
  ]
  node [
    id 7
    label "usa"
    origin "text"
  ]
  node [
    id 8
    label "francja"
    origin "text"
  ]
  node [
    id 9
    label "japonia"
    origin "text"
  ]
  node [
    id 10
    label "lato"
    origin "text"
  ]
  node [
    id 11
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 12
    label "przedmiot"
  ]
  node [
    id 13
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 14
    label "Wsch&#243;d"
  ]
  node [
    id 15
    label "rzecz"
  ]
  node [
    id 16
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 17
    label "sztuka"
  ]
  node [
    id 18
    label "religia"
  ]
  node [
    id 19
    label "przejmowa&#263;"
  ]
  node [
    id 20
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 21
    label "makrokosmos"
  ]
  node [
    id 22
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 23
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 24
    label "zjawisko"
  ]
  node [
    id 25
    label "praca_rolnicza"
  ]
  node [
    id 26
    label "tradycja"
  ]
  node [
    id 27
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 28
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 29
    label "przejmowanie"
  ]
  node [
    id 30
    label "cecha"
  ]
  node [
    id 31
    label "asymilowanie_si&#281;"
  ]
  node [
    id 32
    label "przej&#261;&#263;"
  ]
  node [
    id 33
    label "hodowla"
  ]
  node [
    id 34
    label "brzoskwiniarnia"
  ]
  node [
    id 35
    label "populace"
  ]
  node [
    id 36
    label "konwencja"
  ]
  node [
    id 37
    label "propriety"
  ]
  node [
    id 38
    label "jako&#347;&#263;"
  ]
  node [
    id 39
    label "kuchnia"
  ]
  node [
    id 40
    label "zwyczaj"
  ]
  node [
    id 41
    label "przej&#281;cie"
  ]
  node [
    id 42
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 43
    label "sprzyja&#263;"
  ]
  node [
    id 44
    label "back"
  ]
  node [
    id 45
    label "pociesza&#263;"
  ]
  node [
    id 46
    label "Warszawa"
  ]
  node [
    id 47
    label "u&#322;atwia&#263;"
  ]
  node [
    id 48
    label "opiera&#263;"
  ]
  node [
    id 49
    label "lokalnie"
  ]
  node [
    id 50
    label "selerowate"
  ]
  node [
    id 51
    label "bylina"
  ]
  node [
    id 52
    label "hygrofit"
  ]
  node [
    id 53
    label "si&#281;ga&#263;"
  ]
  node [
    id 54
    label "trwa&#263;"
  ]
  node [
    id 55
    label "obecno&#347;&#263;"
  ]
  node [
    id 56
    label "stan"
  ]
  node [
    id 57
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 58
    label "stand"
  ]
  node [
    id 59
    label "mie&#263;_miejsce"
  ]
  node [
    id 60
    label "uczestniczy&#263;"
  ]
  node [
    id 61
    label "chodzi&#263;"
  ]
  node [
    id 62
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 63
    label "equal"
  ]
  node [
    id 64
    label "pora_roku"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 64
  ]
]
