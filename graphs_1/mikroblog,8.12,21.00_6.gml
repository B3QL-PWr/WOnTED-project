graph [
  maxDegree 46
  minDegree 1
  meanDegree 2
  density 0.028169014084507043
  graphCliqueNumber 3
  node [
    id 0
    label "kr&#243;tki"
    origin "text"
  ]
  node [
    id 1
    label "smutny"
    origin "text"
  ]
  node [
    id 2
    label "historia"
    origin "text"
  ]
  node [
    id 3
    label "by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "jednowyrazowy"
  ]
  node [
    id 5
    label "s&#322;aby"
  ]
  node [
    id 6
    label "bliski"
  ]
  node [
    id 7
    label "drobny"
  ]
  node [
    id 8
    label "kr&#243;tko"
  ]
  node [
    id 9
    label "ruch"
  ]
  node [
    id 10
    label "z&#322;y"
  ]
  node [
    id 11
    label "zwi&#281;z&#322;y"
  ]
  node [
    id 12
    label "szybki"
  ]
  node [
    id 13
    label "brak"
  ]
  node [
    id 14
    label "smutno"
  ]
  node [
    id 15
    label "negatywny"
  ]
  node [
    id 16
    label "przykry"
  ]
  node [
    id 17
    label "report"
  ]
  node [
    id 18
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 19
    label "wypowied&#378;"
  ]
  node [
    id 20
    label "neografia"
  ]
  node [
    id 21
    label "przedmiot"
  ]
  node [
    id 22
    label "papirologia"
  ]
  node [
    id 23
    label "historia_gospodarcza"
  ]
  node [
    id 24
    label "przebiec"
  ]
  node [
    id 25
    label "hista"
  ]
  node [
    id 26
    label "nauka_humanistyczna"
  ]
  node [
    id 27
    label "filigranistyka"
  ]
  node [
    id 28
    label "dyplomatyka"
  ]
  node [
    id 29
    label "annalistyka"
  ]
  node [
    id 30
    label "historyka"
  ]
  node [
    id 31
    label "heraldyka"
  ]
  node [
    id 32
    label "fabu&#322;a"
  ]
  node [
    id 33
    label "muzealnictwo"
  ]
  node [
    id 34
    label "varsavianistyka"
  ]
  node [
    id 35
    label "mediewistyka"
  ]
  node [
    id 36
    label "prezentyzm"
  ]
  node [
    id 37
    label "przebiegni&#281;cie"
  ]
  node [
    id 38
    label "charakter"
  ]
  node [
    id 39
    label "paleografia"
  ]
  node [
    id 40
    label "genealogia"
  ]
  node [
    id 41
    label "czynno&#347;&#263;"
  ]
  node [
    id 42
    label "prozopografia"
  ]
  node [
    id 43
    label "motyw"
  ]
  node [
    id 44
    label "nautologia"
  ]
  node [
    id 45
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 46
    label "epoka"
  ]
  node [
    id 47
    label "numizmatyka"
  ]
  node [
    id 48
    label "ruralistyka"
  ]
  node [
    id 49
    label "&#378;r&#243;d&#322;oznawstwo"
  ]
  node [
    id 50
    label "epigrafika"
  ]
  node [
    id 51
    label "historiografia"
  ]
  node [
    id 52
    label "bizantynistyka"
  ]
  node [
    id 53
    label "weksylologia"
  ]
  node [
    id 54
    label "kierunek"
  ]
  node [
    id 55
    label "ikonografia"
  ]
  node [
    id 56
    label "chronologia"
  ]
  node [
    id 57
    label "archiwistyka"
  ]
  node [
    id 58
    label "sfragistyka"
  ]
  node [
    id 59
    label "zabytkoznawstwo"
  ]
  node [
    id 60
    label "historia_sztuki"
  ]
  node [
    id 61
    label "si&#281;ga&#263;"
  ]
  node [
    id 62
    label "trwa&#263;"
  ]
  node [
    id 63
    label "obecno&#347;&#263;"
  ]
  node [
    id 64
    label "stan"
  ]
  node [
    id 65
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 66
    label "stand"
  ]
  node [
    id 67
    label "mie&#263;_miejsce"
  ]
  node [
    id 68
    label "uczestniczy&#263;"
  ]
  node [
    id 69
    label "chodzi&#263;"
  ]
  node [
    id 70
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 71
    label "equal"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
]
