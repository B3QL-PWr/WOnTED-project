graph [
  maxDegree 28
  minDegree 1
  meanDegree 1.9803921568627452
  density 0.0196078431372549
  graphCliqueNumber 2
  node [
    id 0
    label "ruszy&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "poch&#243;d"
    origin "text"
  ]
  node [
    id 2
    label "&#380;yd"
    origin "text"
  ]
  node [
    id 3
    label "wieczny"
    origin "text"
  ]
  node [
    id 4
    label "tu&#322;acz"
    origin "text"
  ]
  node [
    id 5
    label "wkr&#243;tce"
    origin "text"
  ]
  node [
    id 6
    label "z&#322;&#261;czy&#263;"
    origin "text"
  ]
  node [
    id 7
    label "si&#281;"
    origin "text"
  ]
  node [
    id 8
    label "tak"
    origin "text"
  ]
  node [
    id 9
    label "daleko"
    origin "text"
  ]
  node [
    id 10
    label "ci&#261;gn&#261;&#263;"
    origin "text"
  ]
  node [
    id 11
    label "opowiadanie"
    origin "text"
  ]
  node [
    id 12
    label "swoje"
    origin "text"
  ]
  node [
    id 13
    label "przygoda"
    origin "text"
  ]
  node [
    id 14
    label "przemarsz"
  ]
  node [
    id 15
    label "grupa"
  ]
  node [
    id 16
    label "pageant"
  ]
  node [
    id 17
    label "chciwiec"
  ]
  node [
    id 18
    label "judenrat"
  ]
  node [
    id 19
    label "materialista"
  ]
  node [
    id 20
    label "sk&#261;piarz"
  ]
  node [
    id 21
    label "wyznawca"
  ]
  node [
    id 22
    label "&#379;ydziak"
  ]
  node [
    id 23
    label "sk&#261;py"
  ]
  node [
    id 24
    label "szmonces"
  ]
  node [
    id 25
    label "monoteista"
  ]
  node [
    id 26
    label "mosiek"
  ]
  node [
    id 27
    label "ci&#261;g&#322;y"
  ]
  node [
    id 28
    label "pradawny"
  ]
  node [
    id 29
    label "wiecznie"
  ]
  node [
    id 30
    label "sta&#322;y"
  ]
  node [
    id 31
    label "trwa&#322;y"
  ]
  node [
    id 32
    label "cz&#322;owiek"
  ]
  node [
    id 33
    label "ward&#281;ga"
  ]
  node [
    id 34
    label "migrant"
  ]
  node [
    id 35
    label "istota_&#380;ywa"
  ]
  node [
    id 36
    label "emigracja"
  ]
  node [
    id 37
    label "wyemigrowanie"
  ]
  node [
    id 38
    label "chachar"
  ]
  node [
    id 39
    label "wpr&#281;dce"
  ]
  node [
    id 40
    label "blisko"
  ]
  node [
    id 41
    label "nied&#322;ugi"
  ]
  node [
    id 42
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 43
    label "relate"
  ]
  node [
    id 44
    label "spowodowa&#263;"
  ]
  node [
    id 45
    label "incorporate"
  ]
  node [
    id 46
    label "zrobi&#263;"
  ]
  node [
    id 47
    label "wi&#281;&#378;"
  ]
  node [
    id 48
    label "dawno"
  ]
  node [
    id 49
    label "nisko"
  ]
  node [
    id 50
    label "nieobecnie"
  ]
  node [
    id 51
    label "daleki"
  ]
  node [
    id 52
    label "het"
  ]
  node [
    id 53
    label "wysoko"
  ]
  node [
    id 54
    label "du&#380;o"
  ]
  node [
    id 55
    label "znacznie"
  ]
  node [
    id 56
    label "g&#322;&#281;boko"
  ]
  node [
    id 57
    label "proceed"
  ]
  node [
    id 58
    label "imperativeness"
  ]
  node [
    id 59
    label "przeci&#261;ga&#263;"
  ]
  node [
    id 60
    label "force"
  ]
  node [
    id 61
    label "wia&#263;"
  ]
  node [
    id 62
    label "rusza&#263;"
  ]
  node [
    id 63
    label "przewozi&#263;"
  ]
  node [
    id 64
    label "chcie&#263;"
  ]
  node [
    id 65
    label "adhere"
  ]
  node [
    id 66
    label "przesuwa&#263;"
  ]
  node [
    id 67
    label "wch&#322;ania&#263;"
  ]
  node [
    id 68
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 69
    label "radzi&#263;_sobie"
  ]
  node [
    id 70
    label "wyjmowa&#263;"
  ]
  node [
    id 71
    label "wa&#380;y&#263;"
  ]
  node [
    id 72
    label "przemieszcza&#263;"
  ]
  node [
    id 73
    label "set_about"
  ]
  node [
    id 74
    label "robi&#263;"
  ]
  node [
    id 75
    label "obrabia&#263;"
  ]
  node [
    id 76
    label "wyd&#322;u&#380;a&#263;"
  ]
  node [
    id 77
    label "zabiera&#263;"
  ]
  node [
    id 78
    label "wyt&#322;acza&#263;"
  ]
  node [
    id 79
    label "poci&#261;ga&#263;"
  ]
  node [
    id 80
    label "prosecute"
  ]
  node [
    id 81
    label "blow_up"
  ]
  node [
    id 82
    label "i&#347;&#263;"
  ]
  node [
    id 83
    label "rozpowiadanie"
  ]
  node [
    id 84
    label "spalenie"
  ]
  node [
    id 85
    label "podbarwianie"
  ]
  node [
    id 86
    label "follow-up"
  ]
  node [
    id 87
    label "wypowied&#378;"
  ]
  node [
    id 88
    label "report"
  ]
  node [
    id 89
    label "story"
  ]
  node [
    id 90
    label "przedstawianie"
  ]
  node [
    id 91
    label "fabu&#322;a"
  ]
  node [
    id 92
    label "proza"
  ]
  node [
    id 93
    label "prawienie"
  ]
  node [
    id 94
    label "utw&#243;r_epicki"
  ]
  node [
    id 95
    label "rozpowiedzenie"
  ]
  node [
    id 96
    label "wydarzenie"
  ]
  node [
    id 97
    label "awanturnik"
  ]
  node [
    id 98
    label "fascynacja"
  ]
  node [
    id 99
    label "romans"
  ]
  node [
    id 100
    label "awantura"
  ]
  node [
    id 101
    label "affair"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 48
  ]
  edge [
    source 9
    target 49
  ]
  edge [
    source 9
    target 50
  ]
  edge [
    source 9
    target 51
  ]
  edge [
    source 9
    target 52
  ]
  edge [
    source 9
    target 53
  ]
  edge [
    source 9
    target 54
  ]
  edge [
    source 9
    target 55
  ]
  edge [
    source 9
    target 56
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 57
  ]
  edge [
    source 10
    target 58
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 10
    target 60
  ]
  edge [
    source 10
    target 61
  ]
  edge [
    source 10
    target 62
  ]
  edge [
    source 10
    target 63
  ]
  edge [
    source 10
    target 64
  ]
  edge [
    source 10
    target 65
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 10
    target 69
  ]
  edge [
    source 10
    target 70
  ]
  edge [
    source 10
    target 71
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 83
  ]
  edge [
    source 11
    target 84
  ]
  edge [
    source 11
    target 85
  ]
  edge [
    source 11
    target 86
  ]
  edge [
    source 11
    target 87
  ]
  edge [
    source 11
    target 88
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 11
    target 92
  ]
  edge [
    source 11
    target 93
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 96
  ]
  edge [
    source 13
    target 97
  ]
  edge [
    source 13
    target 98
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 13
    target 101
  ]
]
