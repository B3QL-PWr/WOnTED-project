graph [
  maxDegree 10
  minDegree 1
  meanDegree 1.8181818181818181
  density 0.18181818181818182
  graphCliqueNumber 2
  node [
    id 0
    label "hamulec"
    origin "text"
  ]
  node [
    id 1
    label "elektrodynamiczny"
    origin "text"
  ]
  node [
    id 2
    label "szcz&#281;ka"
  ]
  node [
    id 3
    label "pow&#347;ci&#261;g"
  ]
  node [
    id 4
    label "uk&#322;ad_hamulcowy"
  ]
  node [
    id 5
    label "brake"
  ]
  node [
    id 6
    label "czuwak"
  ]
  node [
    id 7
    label "luzownik"
  ]
  node [
    id 8
    label "urz&#261;dzenie"
  ]
  node [
    id 9
    label "pojazd"
  ]
  node [
    id 10
    label "przeszkoda"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
]
