graph [
  maxDegree 324
  minDegree 1
  meanDegree 2.009009009009009
  density 0.004535009049681736
  graphCliqueNumber 3
  node [
    id 0
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 1
    label "wprawdzie"
    origin "text"
  ]
  node [
    id 2
    label "powiedzie&#263;"
    origin "text"
  ]
  node [
    id 3
    label "zawsze"
    origin "text"
  ]
  node [
    id 4
    label "por&#243;wnanie"
    origin "text"
  ]
  node [
    id 5
    label "dwa"
    origin "text"
  ]
  node [
    id 6
    label "kraj"
    origin "text"
  ]
  node [
    id 7
    label "trafi&#263;"
    origin "text"
  ]
  node [
    id 8
    label "r&#243;&#380;ny"
    origin "text"
  ]
  node [
    id 9
    label "problem"
    origin "text"
  ]
  node [
    id 10
    label "niepor&#243;wnywalny"
    origin "text"
  ]
  node [
    id 11
    label "element"
    origin "text"
  ]
  node [
    id 12
    label "pewnie"
    origin "text"
  ]
  node [
    id 13
    label "wybra&#263;"
    origin "text"
  ]
  node [
    id 14
    label "siebie"
    origin "text"
  ]
  node [
    id 15
    label "dobrze"
    origin "text"
  ]
  node [
    id 16
    label "albo"
    origin "text"
  ]
  node [
    id 17
    label "&#378;le"
    origin "text"
  ]
  node [
    id 18
    label "free"
  ]
  node [
    id 19
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 20
    label "express"
  ]
  node [
    id 21
    label "rzekn&#261;&#263;"
  ]
  node [
    id 22
    label "okre&#347;li&#263;"
  ]
  node [
    id 23
    label "wyrazi&#263;"
  ]
  node [
    id 24
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 25
    label "unwrap"
  ]
  node [
    id 26
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 27
    label "convey"
  ]
  node [
    id 28
    label "discover"
  ]
  node [
    id 29
    label "wydoby&#263;"
  ]
  node [
    id 30
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 31
    label "poda&#263;"
  ]
  node [
    id 32
    label "zaw&#380;dy"
  ]
  node [
    id 33
    label "ci&#261;gle"
  ]
  node [
    id 34
    label "na_zawsze"
  ]
  node [
    id 35
    label "cz&#281;sto"
  ]
  node [
    id 36
    label "simile"
  ]
  node [
    id 37
    label "figura_stylistyczna"
  ]
  node [
    id 38
    label "zestawienie"
  ]
  node [
    id 39
    label "zr&#243;&#380;nicowanie"
  ]
  node [
    id 40
    label "comparison"
  ]
  node [
    id 41
    label "zanalizowanie"
  ]
  node [
    id 42
    label "Skandynawia"
  ]
  node [
    id 43
    label "Filipiny"
  ]
  node [
    id 44
    label "Rwanda"
  ]
  node [
    id 45
    label "Kaukaz"
  ]
  node [
    id 46
    label "Kaszmir"
  ]
  node [
    id 47
    label "Toskania"
  ]
  node [
    id 48
    label "Yorkshire"
  ]
  node [
    id 49
    label "&#321;emkowszczyzna"
  ]
  node [
    id 50
    label "obszar"
  ]
  node [
    id 51
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 52
    label "Monako"
  ]
  node [
    id 53
    label "Amhara"
  ]
  node [
    id 54
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 55
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 56
    label "Lombardia"
  ]
  node [
    id 57
    label "Korea"
  ]
  node [
    id 58
    label "Kalabria"
  ]
  node [
    id 59
    label "Ghana"
  ]
  node [
    id 60
    label "Czarnog&#243;ra"
  ]
  node [
    id 61
    label "Tyrol"
  ]
  node [
    id 62
    label "Malawi"
  ]
  node [
    id 63
    label "Indonezja"
  ]
  node [
    id 64
    label "Bu&#322;garia"
  ]
  node [
    id 65
    label "Nauru"
  ]
  node [
    id 66
    label "Kenia"
  ]
  node [
    id 67
    label "Pamir"
  ]
  node [
    id 68
    label "Kambod&#380;a"
  ]
  node [
    id 69
    label "Lubelszczyzna"
  ]
  node [
    id 70
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 71
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 72
    label "Mali"
  ]
  node [
    id 73
    label "&#379;ywiecczyzna"
  ]
  node [
    id 74
    label "Austria"
  ]
  node [
    id 75
    label "interior"
  ]
  node [
    id 76
    label "Europa_Wschodnia"
  ]
  node [
    id 77
    label "Armenia"
  ]
  node [
    id 78
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 79
    label "Fid&#380;i"
  ]
  node [
    id 80
    label "Tuwalu"
  ]
  node [
    id 81
    label "Zabajkale"
  ]
  node [
    id 82
    label "Etiopia"
  ]
  node [
    id 83
    label "Malta"
  ]
  node [
    id 84
    label "Malezja"
  ]
  node [
    id 85
    label "Kaszuby"
  ]
  node [
    id 86
    label "Bo&#347;nia"
  ]
  node [
    id 87
    label "Noworosja"
  ]
  node [
    id 88
    label "Grenada"
  ]
  node [
    id 89
    label "Tad&#380;ykistan"
  ]
  node [
    id 90
    label "Ba&#322;kany"
  ]
  node [
    id 91
    label "Wehrlen"
  ]
  node [
    id 92
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 93
    label "Anglia"
  ]
  node [
    id 94
    label "Kielecczyzna"
  ]
  node [
    id 95
    label "Rumunia"
  ]
  node [
    id 96
    label "Pomorze_Zachodnie"
  ]
  node [
    id 97
    label "Maroko"
  ]
  node [
    id 98
    label "Bhutan"
  ]
  node [
    id 99
    label "Opolskie"
  ]
  node [
    id 100
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 101
    label "Ko&#322;yma"
  ]
  node [
    id 102
    label "Oksytania"
  ]
  node [
    id 103
    label "S&#322;owacja"
  ]
  node [
    id 104
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 105
    label "Seszele"
  ]
  node [
    id 106
    label "Syjon"
  ]
  node [
    id 107
    label "Kuwejt"
  ]
  node [
    id 108
    label "Arabia_Saudyjska"
  ]
  node [
    id 109
    label "Kociewie"
  ]
  node [
    id 110
    label "Ekwador"
  ]
  node [
    id 111
    label "Kanada"
  ]
  node [
    id 112
    label "ziemia"
  ]
  node [
    id 113
    label "Japonia"
  ]
  node [
    id 114
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 115
    label "Hiszpania"
  ]
  node [
    id 116
    label "Wyspy_Marshalla"
  ]
  node [
    id 117
    label "Botswana"
  ]
  node [
    id 118
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 119
    label "D&#380;ibuti"
  ]
  node [
    id 120
    label "Huculszczyzna"
  ]
  node [
    id 121
    label "Wietnam"
  ]
  node [
    id 122
    label "Egipt"
  ]
  node [
    id 123
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 124
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 125
    label "Burkina_Faso"
  ]
  node [
    id 126
    label "Bawaria"
  ]
  node [
    id 127
    label "Niemcy"
  ]
  node [
    id 128
    label "Khitai"
  ]
  node [
    id 129
    label "Macedonia"
  ]
  node [
    id 130
    label "Albania"
  ]
  node [
    id 131
    label "Madagaskar"
  ]
  node [
    id 132
    label "Bahrajn"
  ]
  node [
    id 133
    label "Jemen"
  ]
  node [
    id 134
    label "Lesoto"
  ]
  node [
    id 135
    label "Maghreb"
  ]
  node [
    id 136
    label "Samoa"
  ]
  node [
    id 137
    label "Andora"
  ]
  node [
    id 138
    label "Bory_Tucholskie"
  ]
  node [
    id 139
    label "Chiny"
  ]
  node [
    id 140
    label "Europa_Zachodnia"
  ]
  node [
    id 141
    label "Cypr"
  ]
  node [
    id 142
    label "Wielka_Brytania"
  ]
  node [
    id 143
    label "Kerala"
  ]
  node [
    id 144
    label "Podhale"
  ]
  node [
    id 145
    label "Kabylia"
  ]
  node [
    id 146
    label "Ukraina"
  ]
  node [
    id 147
    label "Paragwaj"
  ]
  node [
    id 148
    label "Trynidad_i_Tobago"
  ]
  node [
    id 149
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 150
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 151
    label "Ma&#322;opolska"
  ]
  node [
    id 152
    label "Polesie"
  ]
  node [
    id 153
    label "Liguria"
  ]
  node [
    id 154
    label "Libia"
  ]
  node [
    id 155
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 156
    label "&#321;&#243;dzkie"
  ]
  node [
    id 157
    label "Surinam"
  ]
  node [
    id 158
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 159
    label "Palestyna"
  ]
  node [
    id 160
    label "Australia"
  ]
  node [
    id 161
    label "Nigeria"
  ]
  node [
    id 162
    label "Honduras"
  ]
  node [
    id 163
    label "Bojkowszczyzna"
  ]
  node [
    id 164
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 165
    label "Karaiby"
  ]
  node [
    id 166
    label "Bangladesz"
  ]
  node [
    id 167
    label "Peru"
  ]
  node [
    id 168
    label "Kazachstan"
  ]
  node [
    id 169
    label "USA"
  ]
  node [
    id 170
    label "Irak"
  ]
  node [
    id 171
    label "Nepal"
  ]
  node [
    id 172
    label "S&#261;decczyzna"
  ]
  node [
    id 173
    label "Sudan"
  ]
  node [
    id 174
    label "Sand&#380;ak"
  ]
  node [
    id 175
    label "Nadrenia"
  ]
  node [
    id 176
    label "San_Marino"
  ]
  node [
    id 177
    label "Burundi"
  ]
  node [
    id 178
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 179
    label "Dominikana"
  ]
  node [
    id 180
    label "Komory"
  ]
  node [
    id 181
    label "Zakarpacie"
  ]
  node [
    id 182
    label "Gwatemala"
  ]
  node [
    id 183
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 184
    label "Zag&#243;rze"
  ]
  node [
    id 185
    label "Andaluzja"
  ]
  node [
    id 186
    label "granica_pa&#324;stwa"
  ]
  node [
    id 187
    label "Turkiestan"
  ]
  node [
    id 188
    label "Naddniestrze"
  ]
  node [
    id 189
    label "Hercegowina"
  ]
  node [
    id 190
    label "Brunei"
  ]
  node [
    id 191
    label "Iran"
  ]
  node [
    id 192
    label "jednostka_administracyjna"
  ]
  node [
    id 193
    label "Zimbabwe"
  ]
  node [
    id 194
    label "Namibia"
  ]
  node [
    id 195
    label "Meksyk"
  ]
  node [
    id 196
    label "Lotaryngia"
  ]
  node [
    id 197
    label "Kamerun"
  ]
  node [
    id 198
    label "Opolszczyzna"
  ]
  node [
    id 199
    label "Afryka_Wschodnia"
  ]
  node [
    id 200
    label "Szlezwik"
  ]
  node [
    id 201
    label "Somalia"
  ]
  node [
    id 202
    label "Angola"
  ]
  node [
    id 203
    label "Gabon"
  ]
  node [
    id 204
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 205
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 206
    label "Mozambik"
  ]
  node [
    id 207
    label "Tajwan"
  ]
  node [
    id 208
    label "Tunezja"
  ]
  node [
    id 209
    label "Nowa_Zelandia"
  ]
  node [
    id 210
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 211
    label "Podbeskidzie"
  ]
  node [
    id 212
    label "Liban"
  ]
  node [
    id 213
    label "Jordania"
  ]
  node [
    id 214
    label "Tonga"
  ]
  node [
    id 215
    label "Czad"
  ]
  node [
    id 216
    label "Liberia"
  ]
  node [
    id 217
    label "Gwinea"
  ]
  node [
    id 218
    label "Belize"
  ]
  node [
    id 219
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 220
    label "Mazowsze"
  ]
  node [
    id 221
    label "&#321;otwa"
  ]
  node [
    id 222
    label "Syria"
  ]
  node [
    id 223
    label "Benin"
  ]
  node [
    id 224
    label "Afryka_Zachodnia"
  ]
  node [
    id 225
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 226
    label "Dominika"
  ]
  node [
    id 227
    label "Antigua_i_Barbuda"
  ]
  node [
    id 228
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 229
    label "Hanower"
  ]
  node [
    id 230
    label "Galicja"
  ]
  node [
    id 231
    label "Szkocja"
  ]
  node [
    id 232
    label "Walia"
  ]
  node [
    id 233
    label "Afganistan"
  ]
  node [
    id 234
    label "Kiribati"
  ]
  node [
    id 235
    label "W&#322;ochy"
  ]
  node [
    id 236
    label "Szwajcaria"
  ]
  node [
    id 237
    label "Powi&#347;le"
  ]
  node [
    id 238
    label "Sahara_Zachodnia"
  ]
  node [
    id 239
    label "Chorwacja"
  ]
  node [
    id 240
    label "Tajlandia"
  ]
  node [
    id 241
    label "Salwador"
  ]
  node [
    id 242
    label "Bahamy"
  ]
  node [
    id 243
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 244
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 245
    label "Zamojszczyzna"
  ]
  node [
    id 246
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 247
    label "S&#322;owenia"
  ]
  node [
    id 248
    label "Gambia"
  ]
  node [
    id 249
    label "Kujawy"
  ]
  node [
    id 250
    label "Urugwaj"
  ]
  node [
    id 251
    label "Podlasie"
  ]
  node [
    id 252
    label "Zair"
  ]
  node [
    id 253
    label "Erytrea"
  ]
  node [
    id 254
    label "Laponia"
  ]
  node [
    id 255
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 256
    label "Umbria"
  ]
  node [
    id 257
    label "Rosja"
  ]
  node [
    id 258
    label "Uganda"
  ]
  node [
    id 259
    label "Niger"
  ]
  node [
    id 260
    label "Mauritius"
  ]
  node [
    id 261
    label "Turkmenistan"
  ]
  node [
    id 262
    label "Turcja"
  ]
  node [
    id 263
    label "Mezoameryka"
  ]
  node [
    id 264
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 265
    label "Irlandia"
  ]
  node [
    id 266
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 267
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 268
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 269
    label "Gwinea_Bissau"
  ]
  node [
    id 270
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 271
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 272
    label "Kurdystan"
  ]
  node [
    id 273
    label "Belgia"
  ]
  node [
    id 274
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 275
    label "Palau"
  ]
  node [
    id 276
    label "Barbados"
  ]
  node [
    id 277
    label "Chile"
  ]
  node [
    id 278
    label "Wenezuela"
  ]
  node [
    id 279
    label "W&#281;gry"
  ]
  node [
    id 280
    label "Argentyna"
  ]
  node [
    id 281
    label "Kolumbia"
  ]
  node [
    id 282
    label "Kampania"
  ]
  node [
    id 283
    label "Armagnac"
  ]
  node [
    id 284
    label "Sierra_Leone"
  ]
  node [
    id 285
    label "Azerbejd&#380;an"
  ]
  node [
    id 286
    label "Kongo"
  ]
  node [
    id 287
    label "Polinezja"
  ]
  node [
    id 288
    label "Warmia"
  ]
  node [
    id 289
    label "Pakistan"
  ]
  node [
    id 290
    label "Liechtenstein"
  ]
  node [
    id 291
    label "Wielkopolska"
  ]
  node [
    id 292
    label "Nikaragua"
  ]
  node [
    id 293
    label "Senegal"
  ]
  node [
    id 294
    label "brzeg"
  ]
  node [
    id 295
    label "Bordeaux"
  ]
  node [
    id 296
    label "Lauda"
  ]
  node [
    id 297
    label "Indie"
  ]
  node [
    id 298
    label "Mazury"
  ]
  node [
    id 299
    label "Suazi"
  ]
  node [
    id 300
    label "Polska"
  ]
  node [
    id 301
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 302
    label "Algieria"
  ]
  node [
    id 303
    label "Jamajka"
  ]
  node [
    id 304
    label "Timor_Wschodni"
  ]
  node [
    id 305
    label "Oceania"
  ]
  node [
    id 306
    label "Kostaryka"
  ]
  node [
    id 307
    label "Podkarpacie"
  ]
  node [
    id 308
    label "Lasko"
  ]
  node [
    id 309
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 310
    label "Kuba"
  ]
  node [
    id 311
    label "Mauretania"
  ]
  node [
    id 312
    label "Amazonia"
  ]
  node [
    id 313
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 314
    label "Portoryko"
  ]
  node [
    id 315
    label "Brazylia"
  ]
  node [
    id 316
    label "Mo&#322;dawia"
  ]
  node [
    id 317
    label "organizacja"
  ]
  node [
    id 318
    label "Litwa"
  ]
  node [
    id 319
    label "Kirgistan"
  ]
  node [
    id 320
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 321
    label "Izrael"
  ]
  node [
    id 322
    label "Grecja"
  ]
  node [
    id 323
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 324
    label "Kurpie"
  ]
  node [
    id 325
    label "Holandia"
  ]
  node [
    id 326
    label "Sri_Lanka"
  ]
  node [
    id 327
    label "Tonkin"
  ]
  node [
    id 328
    label "Katar"
  ]
  node [
    id 329
    label "Azja_Wschodnia"
  ]
  node [
    id 330
    label "Mikronezja"
  ]
  node [
    id 331
    label "Ukraina_Zachodnia"
  ]
  node [
    id 332
    label "Laos"
  ]
  node [
    id 333
    label "Mongolia"
  ]
  node [
    id 334
    label "Turyngia"
  ]
  node [
    id 335
    label "Malediwy"
  ]
  node [
    id 336
    label "Zambia"
  ]
  node [
    id 337
    label "Baszkiria"
  ]
  node [
    id 338
    label "Tanzania"
  ]
  node [
    id 339
    label "Gujana"
  ]
  node [
    id 340
    label "Apulia"
  ]
  node [
    id 341
    label "Czechy"
  ]
  node [
    id 342
    label "Panama"
  ]
  node [
    id 343
    label "Uzbekistan"
  ]
  node [
    id 344
    label "Gruzja"
  ]
  node [
    id 345
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 346
    label "Serbia"
  ]
  node [
    id 347
    label "Francja"
  ]
  node [
    id 348
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 349
    label "Togo"
  ]
  node [
    id 350
    label "Estonia"
  ]
  node [
    id 351
    label "Indochiny"
  ]
  node [
    id 352
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 353
    label "Oman"
  ]
  node [
    id 354
    label "Boliwia"
  ]
  node [
    id 355
    label "Portugalia"
  ]
  node [
    id 356
    label "Wyspy_Salomona"
  ]
  node [
    id 357
    label "Luksemburg"
  ]
  node [
    id 358
    label "Haiti"
  ]
  node [
    id 359
    label "Biskupizna"
  ]
  node [
    id 360
    label "Lubuskie"
  ]
  node [
    id 361
    label "Birma"
  ]
  node [
    id 362
    label "Rodezja"
  ]
  node [
    id 363
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 364
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 365
    label "przypasowa&#263;"
  ]
  node [
    id 366
    label "wpa&#347;&#263;"
  ]
  node [
    id 367
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 368
    label "spotka&#263;"
  ]
  node [
    id 369
    label "dotrze&#263;"
  ]
  node [
    id 370
    label "zjawi&#263;_si&#281;"
  ]
  node [
    id 371
    label "happen"
  ]
  node [
    id 372
    label "znale&#378;&#263;"
  ]
  node [
    id 373
    label "hit"
  ]
  node [
    id 374
    label "pocisk"
  ]
  node [
    id 375
    label "stumble"
  ]
  node [
    id 376
    label "dolecie&#263;"
  ]
  node [
    id 377
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 378
    label "r&#243;&#380;nie"
  ]
  node [
    id 379
    label "inny"
  ]
  node [
    id 380
    label "jaki&#347;"
  ]
  node [
    id 381
    label "trudno&#347;&#263;"
  ]
  node [
    id 382
    label "sprawa"
  ]
  node [
    id 383
    label "ambaras"
  ]
  node [
    id 384
    label "problemat"
  ]
  node [
    id 385
    label "pierepa&#322;ka"
  ]
  node [
    id 386
    label "obstruction"
  ]
  node [
    id 387
    label "problematyka"
  ]
  node [
    id 388
    label "jajko_Kolumba"
  ]
  node [
    id 389
    label "subiekcja"
  ]
  node [
    id 390
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 391
    label "niepor&#243;wnywalnie"
  ]
  node [
    id 392
    label "wyj&#261;tkowy"
  ]
  node [
    id 393
    label "szkodnik"
  ]
  node [
    id 394
    label "&#347;rodowisko"
  ]
  node [
    id 395
    label "component"
  ]
  node [
    id 396
    label "p&#243;&#322;&#347;wiatek"
  ]
  node [
    id 397
    label "r&#243;&#380;niczka"
  ]
  node [
    id 398
    label "przedmiot"
  ]
  node [
    id 399
    label "typ_spod_ciemnej_gwiazdy"
  ]
  node [
    id 400
    label "gangsterski"
  ]
  node [
    id 401
    label "szambo"
  ]
  node [
    id 402
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 403
    label "materia"
  ]
  node [
    id 404
    label "aspo&#322;eczny"
  ]
  node [
    id 405
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 406
    label "poj&#281;cie"
  ]
  node [
    id 407
    label "underworld"
  ]
  node [
    id 408
    label "zwinnie"
  ]
  node [
    id 409
    label "pewniej"
  ]
  node [
    id 410
    label "bezpiecznie"
  ]
  node [
    id 411
    label "wiarygodnie"
  ]
  node [
    id 412
    label "pewny"
  ]
  node [
    id 413
    label "mocno"
  ]
  node [
    id 414
    label "najpewniej"
  ]
  node [
    id 415
    label "zu&#380;y&#263;"
  ]
  node [
    id 416
    label "distill"
  ]
  node [
    id 417
    label "wyj&#261;&#263;"
  ]
  node [
    id 418
    label "sie&#263;_rybacka"
  ]
  node [
    id 419
    label "powo&#322;a&#263;"
  ]
  node [
    id 420
    label "kotwica"
  ]
  node [
    id 421
    label "ustali&#263;"
  ]
  node [
    id 422
    label "pick"
  ]
  node [
    id 423
    label "moralnie"
  ]
  node [
    id 424
    label "wiele"
  ]
  node [
    id 425
    label "lepiej"
  ]
  node [
    id 426
    label "korzystnie"
  ]
  node [
    id 427
    label "pomy&#347;lnie"
  ]
  node [
    id 428
    label "pozytywnie"
  ]
  node [
    id 429
    label "dobry"
  ]
  node [
    id 430
    label "dobroczynnie"
  ]
  node [
    id 431
    label "odpowiednio"
  ]
  node [
    id 432
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 433
    label "skutecznie"
  ]
  node [
    id 434
    label "niezgodnie"
  ]
  node [
    id 435
    label "niepomy&#347;lnie"
  ]
  node [
    id 436
    label "negatywnie"
  ]
  node [
    id 437
    label "piesko"
  ]
  node [
    id 438
    label "z&#322;y"
  ]
  node [
    id 439
    label "niew&#322;a&#347;ciwie"
  ]
  node [
    id 440
    label "gorzej"
  ]
  node [
    id 441
    label "niekorzystnie"
  ]
  node [
    id 442
    label "Arabia"
  ]
  node [
    id 443
    label "saudyjski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 151
  ]
  edge [
    source 6
    target 152
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 6
    target 250
  ]
  edge [
    source 6
    target 251
  ]
  edge [
    source 6
    target 252
  ]
  edge [
    source 6
    target 253
  ]
  edge [
    source 6
    target 254
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 6
    target 263
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 266
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 6
    target 322
  ]
  edge [
    source 6
    target 323
  ]
  edge [
    source 6
    target 324
  ]
  edge [
    source 6
    target 325
  ]
  edge [
    source 6
    target 326
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 328
  ]
  edge [
    source 6
    target 329
  ]
  edge [
    source 6
    target 330
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 332
  ]
  edge [
    source 6
    target 333
  ]
  edge [
    source 6
    target 334
  ]
  edge [
    source 6
    target 335
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 6
    target 337
  ]
  edge [
    source 6
    target 338
  ]
  edge [
    source 6
    target 339
  ]
  edge [
    source 6
    target 340
  ]
  edge [
    source 6
    target 341
  ]
  edge [
    source 6
    target 342
  ]
  edge [
    source 6
    target 343
  ]
  edge [
    source 6
    target 344
  ]
  edge [
    source 6
    target 345
  ]
  edge [
    source 6
    target 346
  ]
  edge [
    source 6
    target 347
  ]
  edge [
    source 6
    target 348
  ]
  edge [
    source 6
    target 349
  ]
  edge [
    source 6
    target 350
  ]
  edge [
    source 6
    target 351
  ]
  edge [
    source 6
    target 352
  ]
  edge [
    source 6
    target 353
  ]
  edge [
    source 6
    target 354
  ]
  edge [
    source 6
    target 355
  ]
  edge [
    source 6
    target 356
  ]
  edge [
    source 6
    target 357
  ]
  edge [
    source 6
    target 358
  ]
  edge [
    source 6
    target 359
  ]
  edge [
    source 6
    target 360
  ]
  edge [
    source 6
    target 361
  ]
  edge [
    source 6
    target 362
  ]
  edge [
    source 6
    target 363
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 378
  ]
  edge [
    source 8
    target 379
  ]
  edge [
    source 8
    target 380
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 381
  ]
  edge [
    source 9
    target 382
  ]
  edge [
    source 9
    target 383
  ]
  edge [
    source 9
    target 384
  ]
  edge [
    source 9
    target 385
  ]
  edge [
    source 9
    target 386
  ]
  edge [
    source 9
    target 387
  ]
  edge [
    source 9
    target 388
  ]
  edge [
    source 9
    target 389
  ]
  edge [
    source 9
    target 390
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 391
  ]
  edge [
    source 10
    target 392
  ]
  edge [
    source 11
    target 393
  ]
  edge [
    source 11
    target 394
  ]
  edge [
    source 11
    target 395
  ]
  edge [
    source 11
    target 396
  ]
  edge [
    source 11
    target 397
  ]
  edge [
    source 11
    target 398
  ]
  edge [
    source 11
    target 399
  ]
  edge [
    source 11
    target 400
  ]
  edge [
    source 11
    target 401
  ]
  edge [
    source 11
    target 402
  ]
  edge [
    source 11
    target 403
  ]
  edge [
    source 11
    target 404
  ]
  edge [
    source 11
    target 405
  ]
  edge [
    source 11
    target 406
  ]
  edge [
    source 11
    target 407
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 408
  ]
  edge [
    source 12
    target 409
  ]
  edge [
    source 12
    target 410
  ]
  edge [
    source 12
    target 411
  ]
  edge [
    source 12
    target 412
  ]
  edge [
    source 12
    target 413
  ]
  edge [
    source 12
    target 414
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 415
  ]
  edge [
    source 13
    target 416
  ]
  edge [
    source 13
    target 417
  ]
  edge [
    source 13
    target 418
  ]
  edge [
    source 13
    target 419
  ]
  edge [
    source 13
    target 420
  ]
  edge [
    source 13
    target 421
  ]
  edge [
    source 13
    target 422
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 423
  ]
  edge [
    source 15
    target 424
  ]
  edge [
    source 15
    target 425
  ]
  edge [
    source 15
    target 426
  ]
  edge [
    source 15
    target 427
  ]
  edge [
    source 15
    target 428
  ]
  edge [
    source 15
    target 429
  ]
  edge [
    source 15
    target 430
  ]
  edge [
    source 15
    target 431
  ]
  edge [
    source 15
    target 432
  ]
  edge [
    source 15
    target 433
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 434
  ]
  edge [
    source 17
    target 435
  ]
  edge [
    source 17
    target 436
  ]
  edge [
    source 17
    target 437
  ]
  edge [
    source 17
    target 438
  ]
  edge [
    source 17
    target 439
  ]
  edge [
    source 17
    target 440
  ]
  edge [
    source 17
    target 441
  ]
  edge [
    source 442
    target 443
  ]
]
