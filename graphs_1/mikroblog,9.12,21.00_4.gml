graph [
  maxDegree 46
  minDegree 1
  meanDegree 2.0097560975609756
  density 0.009851745576279292
  graphCliqueNumber 2
  node [
    id 0
    label "iksde"
    origin "text"
  ]
  node [
    id 1
    label "moja"
    origin "text"
  ]
  node [
    id 2
    label "siostra"
    origin "text"
  ]
  node [
    id 3
    label "miesi&#281;czny"
    origin "text"
  ]
  node [
    id 4
    label "zwi&#261;zek"
    origin "text"
  ]
  node [
    id 5
    label "bra&#263;"
    origin "text"
  ]
  node [
    id 6
    label "zar&#281;czyny"
    origin "text"
  ]
  node [
    id 7
    label "ch&#322;opak"
    origin "text"
  ]
  node [
    id 8
    label "w&#322;a&#347;nie"
    origin "text"
  ]
  node [
    id 9
    label "wbi&#263;"
    origin "text"
  ]
  node [
    id 10
    label "chat"
    origin "text"
  ]
  node [
    id 11
    label "kwiat"
    origin "text"
  ]
  node [
    id 12
    label "spyta&#263;"
    origin "text"
  ]
  node [
    id 13
    label "pozwolenie"
    origin "text"
  ]
  node [
    id 14
    label "chuj"
    origin "text"
  ]
  node [
    id 15
    label "wiedzie&#263;"
    origin "text"
  ]
  node [
    id 16
    label "matka"
    origin "text"
  ]
  node [
    id 17
    label "co&#347;"
    origin "text"
  ]
  node [
    id 18
    label "byby&#263;"
    origin "text"
  ]
  node [
    id 19
    label "odjebal"
    origin "text"
  ]
  node [
    id 20
    label "ale"
    origin "text"
  ]
  node [
    id 21
    label "kornet"
  ]
  node [
    id 22
    label "wyznawczyni"
  ]
  node [
    id 23
    label "pigu&#322;a"
  ]
  node [
    id 24
    label "rodze&#324;stwo"
  ]
  node [
    id 25
    label "czepek"
  ]
  node [
    id 26
    label "krewna"
  ]
  node [
    id 27
    label "siostrzyca"
  ]
  node [
    id 28
    label "pingwin"
  ]
  node [
    id 29
    label "siora"
  ]
  node [
    id 30
    label "anestetysta"
  ]
  node [
    id 31
    label "jasny"
  ]
  node [
    id 32
    label "miesi&#281;cznie"
  ]
  node [
    id 33
    label "trzydziestodniowy"
  ]
  node [
    id 34
    label "kilkudziesi&#281;ciodniowy"
  ]
  node [
    id 35
    label "odwodnienie"
  ]
  node [
    id 36
    label "konstytucja"
  ]
  node [
    id 37
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 38
    label "substancja_chemiczna"
  ]
  node [
    id 39
    label "bratnia_dusza"
  ]
  node [
    id 40
    label "zwi&#261;zanie"
  ]
  node [
    id 41
    label "lokant"
  ]
  node [
    id 42
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 43
    label "zwi&#261;za&#263;"
  ]
  node [
    id 44
    label "organizacja"
  ]
  node [
    id 45
    label "odwadnia&#263;"
  ]
  node [
    id 46
    label "marriage"
  ]
  node [
    id 47
    label "marketing_afiliacyjny"
  ]
  node [
    id 48
    label "bearing"
  ]
  node [
    id 49
    label "wi&#261;zanie"
  ]
  node [
    id 50
    label "odwadnianie"
  ]
  node [
    id 51
    label "koligacja"
  ]
  node [
    id 52
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 53
    label "odwodni&#263;"
  ]
  node [
    id 54
    label "azeotrop"
  ]
  node [
    id 55
    label "powi&#261;zanie"
  ]
  node [
    id 56
    label "get"
  ]
  node [
    id 57
    label "przewa&#380;a&#263;"
  ]
  node [
    id 58
    label "zobowi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 59
    label "poczytywa&#263;"
  ]
  node [
    id 60
    label "levy"
  ]
  node [
    id 61
    label "pokonywa&#263;"
  ]
  node [
    id 62
    label "u&#380;ywa&#263;"
  ]
  node [
    id 63
    label "rusza&#263;"
  ]
  node [
    id 64
    label "zalicza&#263;"
  ]
  node [
    id 65
    label "by&#263;"
  ]
  node [
    id 66
    label "wygrywa&#263;"
  ]
  node [
    id 67
    label "open"
  ]
  node [
    id 68
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 69
    label "branie"
  ]
  node [
    id 70
    label "korzysta&#263;"
  ]
  node [
    id 71
    label "&#263;pa&#263;"
  ]
  node [
    id 72
    label "wch&#322;ania&#263;"
  ]
  node [
    id 73
    label "interpretowa&#263;"
  ]
  node [
    id 74
    label "atakowa&#263;"
  ]
  node [
    id 75
    label "prowadzi&#263;"
  ]
  node [
    id 76
    label "rucha&#263;"
  ]
  node [
    id 77
    label "take"
  ]
  node [
    id 78
    label "dostawa&#263;"
  ]
  node [
    id 79
    label "wzi&#261;&#263;"
  ]
  node [
    id 80
    label "wk&#322;ada&#263;"
  ]
  node [
    id 81
    label "chwyta&#263;"
  ]
  node [
    id 82
    label "arise"
  ]
  node [
    id 83
    label "za&#380;ywa&#263;"
  ]
  node [
    id 84
    label "uprawia&#263;_seks"
  ]
  node [
    id 85
    label "porywa&#263;"
  ]
  node [
    id 86
    label "robi&#263;"
  ]
  node [
    id 87
    label "grza&#263;"
  ]
  node [
    id 88
    label "abstract"
  ]
  node [
    id 89
    label "zaopatrywa&#263;_si&#281;"
  ]
  node [
    id 90
    label "wzmaga&#263;_si&#281;"
  ]
  node [
    id 91
    label "towarzystwo"
  ]
  node [
    id 92
    label "otrzymywa&#263;"
  ]
  node [
    id 93
    label "przyjmowa&#263;"
  ]
  node [
    id 94
    label "wchodzi&#263;"
  ]
  node [
    id 95
    label "ucieka&#263;"
  ]
  node [
    id 96
    label "odurza&#263;_si&#281;"
  ]
  node [
    id 97
    label "buzowa&#263;_si&#281;"
  ]
  node [
    id 98
    label "&#322;apa&#263;"
  ]
  node [
    id 99
    label "raise"
  ]
  node [
    id 100
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 101
    label "employment"
  ]
  node [
    id 102
    label "cz&#322;owiek"
  ]
  node [
    id 103
    label "pomocnik"
  ]
  node [
    id 104
    label "g&#243;wniarz"
  ]
  node [
    id 105
    label "&#347;l&#261;ski"
  ]
  node [
    id 106
    label "m&#322;odzieniec"
  ]
  node [
    id 107
    label "kajtek"
  ]
  node [
    id 108
    label "kawaler"
  ]
  node [
    id 109
    label "usynawianie"
  ]
  node [
    id 110
    label "dziecko"
  ]
  node [
    id 111
    label "okrzos"
  ]
  node [
    id 112
    label "usynowienie"
  ]
  node [
    id 113
    label "sympatia"
  ]
  node [
    id 114
    label "pederasta"
  ]
  node [
    id 115
    label "synek"
  ]
  node [
    id 116
    label "boyfriend"
  ]
  node [
    id 117
    label "dok&#322;adnie"
  ]
  node [
    id 118
    label "przyswoi&#263;"
  ]
  node [
    id 119
    label "insert"
  ]
  node [
    id 120
    label "przybi&#263;"
  ]
  node [
    id 121
    label "doda&#263;"
  ]
  node [
    id 122
    label "zag&#322;&#281;bi&#263;"
  ]
  node [
    id 123
    label "cofn&#261;&#263;"
  ]
  node [
    id 124
    label "wt&#322;oczy&#263;"
  ]
  node [
    id 125
    label "wprowadzi&#263;"
  ]
  node [
    id 126
    label "set"
  ]
  node [
    id 127
    label "da&#263;"
  ]
  node [
    id 128
    label "wla&#263;"
  ]
  node [
    id 129
    label "skuli&#263;"
  ]
  node [
    id 130
    label "wmurowa&#263;"
  ]
  node [
    id 131
    label "umie&#347;ci&#263;"
  ]
  node [
    id 132
    label "zdoby&#263;"
  ]
  node [
    id 133
    label "zwr&#243;ci&#263;"
  ]
  node [
    id 134
    label "wprawi&#263;"
  ]
  node [
    id 135
    label "nasadzi&#263;"
  ]
  node [
    id 136
    label "wrzuci&#263;"
  ]
  node [
    id 137
    label "przyj&#347;&#263;"
  ]
  node [
    id 138
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 139
    label "ustosunkowa&#263;_si&#281;"
  ]
  node [
    id 140
    label "rozmowa"
  ]
  node [
    id 141
    label "strona"
  ]
  node [
    id 142
    label "flakon"
  ]
  node [
    id 143
    label "dno_kwiatowe"
  ]
  node [
    id 144
    label "organ_ro&#347;linny"
  ]
  node [
    id 145
    label "ozdoba"
  ]
  node [
    id 146
    label "warga"
  ]
  node [
    id 147
    label "ro&#347;lina"
  ]
  node [
    id 148
    label "kielich"
  ]
  node [
    id 149
    label "korona"
  ]
  node [
    id 150
    label "ogon"
  ]
  node [
    id 151
    label "przykoronek"
  ]
  node [
    id 152
    label "rurka"
  ]
  node [
    id 153
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 154
    label "ask"
  ]
  node [
    id 155
    label "dokument"
  ]
  node [
    id 156
    label "zrobienie"
  ]
  node [
    id 157
    label "zwolnienie_si&#281;"
  ]
  node [
    id 158
    label "zwalnianie_si&#281;"
  ]
  node [
    id 159
    label "odpowied&#378;"
  ]
  node [
    id 160
    label "umo&#380;liwienie"
  ]
  node [
    id 161
    label "wiedza"
  ]
  node [
    id 162
    label "pozwole&#324;stwo"
  ]
  node [
    id 163
    label "franchise"
  ]
  node [
    id 164
    label "pofolgowanie"
  ]
  node [
    id 165
    label "zwolni&#263;_si&#281;"
  ]
  node [
    id 166
    label "zwalnia&#263;_si&#281;"
  ]
  node [
    id 167
    label "decyzja"
  ]
  node [
    id 168
    label "license"
  ]
  node [
    id 169
    label "bycie_w_stanie"
  ]
  node [
    id 170
    label "odwieszenie"
  ]
  node [
    id 171
    label "uznanie"
  ]
  node [
    id 172
    label "koncesjonowanie"
  ]
  node [
    id 173
    label "authorization"
  ]
  node [
    id 174
    label "dupek"
  ]
  node [
    id 175
    label "skurwysyn"
  ]
  node [
    id 176
    label "wyzwisko"
  ]
  node [
    id 177
    label "penis"
  ]
  node [
    id 178
    label "ciul"
  ]
  node [
    id 179
    label "przekle&#324;stwo"
  ]
  node [
    id 180
    label "cognizance"
  ]
  node [
    id 181
    label "Matka_Boska"
  ]
  node [
    id 182
    label "matka_zast&#281;pcza"
  ]
  node [
    id 183
    label "stara"
  ]
  node [
    id 184
    label "rodzic"
  ]
  node [
    id 185
    label "matczysko"
  ]
  node [
    id 186
    label "pi&#322;ka_palantowa"
  ]
  node [
    id 187
    label "gracz"
  ]
  node [
    id 188
    label "zawodnik"
  ]
  node [
    id 189
    label "macierz"
  ]
  node [
    id 190
    label "owad"
  ]
  node [
    id 191
    label "przyczyna"
  ]
  node [
    id 192
    label "macocha"
  ]
  node [
    id 193
    label "dwa_ognie"
  ]
  node [
    id 194
    label "staruszka"
  ]
  node [
    id 195
    label "kom&#243;rka_jajowa"
  ]
  node [
    id 196
    label "rozsadnik"
  ]
  node [
    id 197
    label "zakonnica"
  ]
  node [
    id 198
    label "obiekt"
  ]
  node [
    id 199
    label "samica"
  ]
  node [
    id 200
    label "przodkini"
  ]
  node [
    id 201
    label "rodzice"
  ]
  node [
    id 202
    label "thing"
  ]
  node [
    id 203
    label "cosik"
  ]
  node [
    id 204
    label "piwo"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 7
    target 113
  ]
  edge [
    source 7
    target 114
  ]
  edge [
    source 7
    target 115
  ]
  edge [
    source 7
    target 116
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 9
    target 119
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 129
  ]
  edge [
    source 9
    target 130
  ]
  edge [
    source 9
    target 131
  ]
  edge [
    source 9
    target 132
  ]
  edge [
    source 9
    target 133
  ]
  edge [
    source 9
    target 134
  ]
  edge [
    source 9
    target 135
  ]
  edge [
    source 9
    target 136
  ]
  edge [
    source 9
    target 137
  ]
  edge [
    source 9
    target 138
  ]
  edge [
    source 9
    target 139
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 140
  ]
  edge [
    source 10
    target 141
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 142
  ]
  edge [
    source 11
    target 143
  ]
  edge [
    source 11
    target 144
  ]
  edge [
    source 11
    target 145
  ]
  edge [
    source 11
    target 146
  ]
  edge [
    source 11
    target 147
  ]
  edge [
    source 11
    target 148
  ]
  edge [
    source 11
    target 149
  ]
  edge [
    source 11
    target 150
  ]
  edge [
    source 11
    target 151
  ]
  edge [
    source 11
    target 152
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 153
  ]
  edge [
    source 12
    target 154
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 155
  ]
  edge [
    source 13
    target 156
  ]
  edge [
    source 13
    target 157
  ]
  edge [
    source 13
    target 158
  ]
  edge [
    source 13
    target 159
  ]
  edge [
    source 13
    target 160
  ]
  edge [
    source 13
    target 161
  ]
  edge [
    source 13
    target 162
  ]
  edge [
    source 13
    target 163
  ]
  edge [
    source 13
    target 164
  ]
  edge [
    source 13
    target 165
  ]
  edge [
    source 13
    target 166
  ]
  edge [
    source 13
    target 167
  ]
  edge [
    source 13
    target 168
  ]
  edge [
    source 13
    target 169
  ]
  edge [
    source 13
    target 170
  ]
  edge [
    source 13
    target 171
  ]
  edge [
    source 13
    target 172
  ]
  edge [
    source 13
    target 173
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 174
  ]
  edge [
    source 14
    target 175
  ]
  edge [
    source 14
    target 176
  ]
  edge [
    source 14
    target 177
  ]
  edge [
    source 14
    target 178
  ]
  edge [
    source 14
    target 179
  ]
  edge [
    source 15
    target 180
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 181
  ]
  edge [
    source 16
    target 182
  ]
  edge [
    source 16
    target 183
  ]
  edge [
    source 16
    target 184
  ]
  edge [
    source 16
    target 185
  ]
  edge [
    source 16
    target 147
  ]
  edge [
    source 16
    target 186
  ]
  edge [
    source 16
    target 187
  ]
  edge [
    source 16
    target 188
  ]
  edge [
    source 16
    target 189
  ]
  edge [
    source 16
    target 190
  ]
  edge [
    source 16
    target 191
  ]
  edge [
    source 16
    target 192
  ]
  edge [
    source 16
    target 193
  ]
  edge [
    source 16
    target 194
  ]
  edge [
    source 16
    target 195
  ]
  edge [
    source 16
    target 196
  ]
  edge [
    source 16
    target 197
  ]
  edge [
    source 16
    target 198
  ]
  edge [
    source 16
    target 199
  ]
  edge [
    source 16
    target 200
  ]
  edge [
    source 16
    target 201
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 202
  ]
  edge [
    source 17
    target 203
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 204
  ]
]
