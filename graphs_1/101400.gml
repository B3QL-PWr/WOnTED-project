graph [
  maxDegree 3
  minDegree 1
  meanDegree 1.7777777777777777
  density 0.2222222222222222
  graphCliqueNumber 3
  node [
    id 0
    label "usv"
    origin "text"
  ]
  node [
    id 1
    label "eschen"
    origin "text"
  ]
  node [
    id 2
    label "mauren"
    origin "text"
  ]
  node [
    id 3
    label "USV"
  ]
  node [
    id 4
    label "Eschen"
  ]
  node [
    id 5
    label "Mauren"
  ]
  node [
    id 6
    label "FC"
  ]
  node [
    id 7
    label "puchar"
  ]
  node [
    id 8
    label "Liechtenstein"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 7
    target 8
  ]
]
