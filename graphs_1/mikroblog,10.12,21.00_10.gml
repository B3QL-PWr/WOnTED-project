graph [
  maxDegree 14
  minDegree 1
  meanDegree 2.032258064516129
  density 0.033315705975674244
  graphCliqueNumber 2
  node [
    id 0
    label "dla"
    origin "text"
  ]
  node [
    id 1
    label "klient"
    origin "text"
  ]
  node [
    id 2
    label "galeria"
    origin "text"
  ]
  node [
    id 3
    label "forum"
    origin "text"
  ]
  node [
    id 4
    label "by&#263;"
    origin "text"
  ]
  node [
    id 5
    label "zni&#380;ka"
    origin "text"
  ]
  node [
    id 6
    label "taxi"
    origin "text"
  ]
  node [
    id 7
    label "podeszlam"
    origin "text"
  ]
  node [
    id 8
    label "taks&#243;wka"
    origin "text"
  ]
  node [
    id 9
    label "spyta&#263;by&#263;"
    origin "text"
  ]
  node [
    id 10
    label "ile"
    origin "text"
  ]
  node [
    id 11
    label "zawie&#378;&#263;"
    origin "text"
  ]
  node [
    id 12
    label "wrzeszcz"
    origin "text"
  ]
  node [
    id 13
    label "cz&#322;owiek"
  ]
  node [
    id 14
    label "bratek"
  ]
  node [
    id 15
    label "klientela"
  ]
  node [
    id 16
    label "szlachcic"
  ]
  node [
    id 17
    label "agent_rozliczeniowy"
  ]
  node [
    id 18
    label "komputer_cyfrowy"
  ]
  node [
    id 19
    label "program"
  ]
  node [
    id 20
    label "us&#322;ugobiorca"
  ]
  node [
    id 21
    label "Rzymianin"
  ]
  node [
    id 22
    label "obywatel"
  ]
  node [
    id 23
    label "sklep"
  ]
  node [
    id 24
    label "eskalator"
  ]
  node [
    id 25
    label "wystawa"
  ]
  node [
    id 26
    label "balkon"
  ]
  node [
    id 27
    label "centrum_handlowe"
  ]
  node [
    id 28
    label "ci&#261;g_komunikacyjny"
  ]
  node [
    id 29
    label "Galeria_Arsena&#322;"
  ]
  node [
    id 30
    label "publiczno&#347;&#263;"
  ]
  node [
    id 31
    label "zbi&#243;r"
  ]
  node [
    id 32
    label "sala"
  ]
  node [
    id 33
    label "&#322;&#261;cznik"
  ]
  node [
    id 34
    label "muzeum"
  ]
  node [
    id 35
    label "s&#261;d"
  ]
  node [
    id 36
    label "plac"
  ]
  node [
    id 37
    label "miejsce"
  ]
  node [
    id 38
    label "grupa_dyskusyjna"
  ]
  node [
    id 39
    label "grupa"
  ]
  node [
    id 40
    label "przestrze&#324;"
  ]
  node [
    id 41
    label "agora"
  ]
  node [
    id 42
    label "bazylika"
  ]
  node [
    id 43
    label "konferencja"
  ]
  node [
    id 44
    label "strona"
  ]
  node [
    id 45
    label "portal"
  ]
  node [
    id 46
    label "si&#281;ga&#263;"
  ]
  node [
    id 47
    label "trwa&#263;"
  ]
  node [
    id 48
    label "obecno&#347;&#263;"
  ]
  node [
    id 49
    label "stan"
  ]
  node [
    id 50
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 51
    label "stand"
  ]
  node [
    id 52
    label "mie&#263;_miejsce"
  ]
  node [
    id 53
    label "uczestniczy&#263;"
  ]
  node [
    id 54
    label "chodzi&#263;"
  ]
  node [
    id 55
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 56
    label "equal"
  ]
  node [
    id 57
    label "spadek"
  ]
  node [
    id 58
    label "sa&#322;ata"
  ]
  node [
    id 59
    label "transport"
  ]
  node [
    id 60
    label "dostarczy&#263;"
  ]
  node [
    id 61
    label "take"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 60
  ]
  edge [
    source 11
    target 61
  ]
]
