graph [
  maxDegree 45
  minDegree 1
  meanDegree 2.111111111111111
  density 0.013112491373360938
  graphCliqueNumber 6
  node [
    id 0
    label "system"
    origin "text"
  ]
  node [
    id 1
    label "zefir"
    origin "text"
  ]
  node [
    id 2
    label "informatyczny"
    origin "text"
  ]
  node [
    id 3
    label "kompleksowo"
    origin "text"
  ]
  node [
    id 4
    label "obs&#322;ugiwa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "sprawa"
    origin "text"
  ]
  node [
    id 6
    label "zwi&#261;za&#263;"
    origin "text"
  ]
  node [
    id 7
    label "finanse"
    origin "text"
  ]
  node [
    id 8
    label "polski"
    origin "text"
  ]
  node [
    id 9
    label "s&#322;u&#380;ba"
    origin "text"
  ]
  node [
    id 10
    label "celny"
    origin "text"
  ]
  node [
    id 11
    label "model"
  ]
  node [
    id 12
    label "sk&#322;ad"
  ]
  node [
    id 13
    label "zachowanie"
  ]
  node [
    id 14
    label "podstawa"
  ]
  node [
    id 15
    label "porz&#261;dek"
  ]
  node [
    id 16
    label "Android"
  ]
  node [
    id 17
    label "przyn&#281;ta"
  ]
  node [
    id 18
    label "jednostka_geologiczna"
  ]
  node [
    id 19
    label "metoda"
  ]
  node [
    id 20
    label "podsystem"
  ]
  node [
    id 21
    label "p&#322;&#243;d"
  ]
  node [
    id 22
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 23
    label "s&#261;d"
  ]
  node [
    id 24
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 25
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 26
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 27
    label "j&#261;dro"
  ]
  node [
    id 28
    label "eratem"
  ]
  node [
    id 29
    label "ryba"
  ]
  node [
    id 30
    label "pulpit"
  ]
  node [
    id 31
    label "struktura"
  ]
  node [
    id 32
    label "spos&#243;b"
  ]
  node [
    id 33
    label "oddzia&#322;"
  ]
  node [
    id 34
    label "usenet"
  ]
  node [
    id 35
    label "o&#347;"
  ]
  node [
    id 36
    label "oprogramowanie"
  ]
  node [
    id 37
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 38
    label "poj&#281;cie"
  ]
  node [
    id 39
    label "w&#281;dkarstwo"
  ]
  node [
    id 40
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 41
    label "Leopard"
  ]
  node [
    id 42
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 43
    label "systemik"
  ]
  node [
    id 44
    label "rozprz&#261;c"
  ]
  node [
    id 45
    label "cybernetyk"
  ]
  node [
    id 46
    label "konstelacja"
  ]
  node [
    id 47
    label "doktryna"
  ]
  node [
    id 48
    label "net"
  ]
  node [
    id 49
    label "zbi&#243;r"
  ]
  node [
    id 50
    label "method"
  ]
  node [
    id 51
    label "systemat"
  ]
  node [
    id 52
    label "wiatr"
  ]
  node [
    id 53
    label "bawe&#322;na"
  ]
  node [
    id 54
    label "comprehensively"
  ]
  node [
    id 55
    label "ca&#322;o&#347;ciowy"
  ]
  node [
    id 56
    label "wielostronnie"
  ]
  node [
    id 57
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 58
    label "treat"
  ]
  node [
    id 59
    label "uprawia&#263;_seks"
  ]
  node [
    id 60
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 61
    label "serve"
  ]
  node [
    id 62
    label "zaspakaja&#263;"
  ]
  node [
    id 63
    label "suffice"
  ]
  node [
    id 64
    label "zaspokaja&#263;"
  ]
  node [
    id 65
    label "temat"
  ]
  node [
    id 66
    label "kognicja"
  ]
  node [
    id 67
    label "idea"
  ]
  node [
    id 68
    label "szczeg&#243;&#322;"
  ]
  node [
    id 69
    label "rzecz"
  ]
  node [
    id 70
    label "wydarzenie"
  ]
  node [
    id 71
    label "przes&#322;anka"
  ]
  node [
    id 72
    label "rozprawa"
  ]
  node [
    id 73
    label "object"
  ]
  node [
    id 74
    label "proposition"
  ]
  node [
    id 75
    label "st&#281;&#380;e&#263;"
  ]
  node [
    id 76
    label "tobo&#322;ek"
  ]
  node [
    id 77
    label "obezw&#322;adni&#263;"
  ]
  node [
    id 78
    label "scali&#263;"
  ]
  node [
    id 79
    label "zawi&#261;za&#263;"
  ]
  node [
    id 80
    label "zatrzyma&#263;"
  ]
  node [
    id 81
    label "form"
  ]
  node [
    id 82
    label "bind"
  ]
  node [
    id 83
    label "wi&#261;zanie_chemiczne"
  ]
  node [
    id 84
    label "unify"
  ]
  node [
    id 85
    label "consort"
  ]
  node [
    id 86
    label "incorporate"
  ]
  node [
    id 87
    label "wi&#281;&#378;"
  ]
  node [
    id 88
    label "zobowi&#261;za&#263;"
  ]
  node [
    id 89
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 90
    label "w&#281;ze&#322;"
  ]
  node [
    id 91
    label "cz&#261;steczka_chemiczna"
  ]
  node [
    id 92
    label "powi&#261;za&#263;"
  ]
  node [
    id 93
    label "opakowa&#263;"
  ]
  node [
    id 94
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 95
    label "cement"
  ]
  node [
    id 96
    label "zaprawa"
  ]
  node [
    id 97
    label "relate"
  ]
  node [
    id 98
    label "uruchomienie"
  ]
  node [
    id 99
    label "nap&#322;yn&#261;&#263;"
  ]
  node [
    id 100
    label "nauka_ekonomiczna"
  ]
  node [
    id 101
    label "absolutorium"
  ]
  node [
    id 102
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 103
    label "podupada&#263;"
  ]
  node [
    id 104
    label "nap&#322;yni&#281;cie"
  ]
  node [
    id 105
    label "supernadz&#243;r"
  ]
  node [
    id 106
    label "nap&#322;ywanie"
  ]
  node [
    id 107
    label "podupadanie"
  ]
  node [
    id 108
    label "kwestor"
  ]
  node [
    id 109
    label "uruchamia&#263;"
  ]
  node [
    id 110
    label "mienie"
  ]
  node [
    id 111
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 112
    label "uruchamianie"
  ]
  node [
    id 113
    label "czynnik_produkcji"
  ]
  node [
    id 114
    label "lacki"
  ]
  node [
    id 115
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 116
    label "przedmiot"
  ]
  node [
    id 117
    label "sztajer"
  ]
  node [
    id 118
    label "drabant"
  ]
  node [
    id 119
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 120
    label "polak"
  ]
  node [
    id 121
    label "pierogi_ruskie"
  ]
  node [
    id 122
    label "krakowiak"
  ]
  node [
    id 123
    label "Polish"
  ]
  node [
    id 124
    label "j&#281;zyk"
  ]
  node [
    id 125
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 126
    label "oberek"
  ]
  node [
    id 127
    label "po_polsku"
  ]
  node [
    id 128
    label "mazur"
  ]
  node [
    id 129
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 130
    label "chodzony"
  ]
  node [
    id 131
    label "skoczny"
  ]
  node [
    id 132
    label "ryba_po_grecku"
  ]
  node [
    id 133
    label "goniony"
  ]
  node [
    id 134
    label "polsko"
  ]
  node [
    id 135
    label "service"
  ]
  node [
    id 136
    label "ZOMO"
  ]
  node [
    id 137
    label "czworak"
  ]
  node [
    id 138
    label "zesp&#243;&#322;"
  ]
  node [
    id 139
    label "s&#322;u&#380;&#261;ca"
  ]
  node [
    id 140
    label "instytucja"
  ]
  node [
    id 141
    label "s&#322;u&#380;&#261;cy"
  ]
  node [
    id 142
    label "praca"
  ]
  node [
    id 143
    label "wys&#322;uga"
  ]
  node [
    id 144
    label "trafnie"
  ]
  node [
    id 145
    label "c&#322;owy"
  ]
  node [
    id 146
    label "celnie"
  ]
  node [
    id 147
    label "udany"
  ]
  node [
    id 148
    label "wybitny"
  ]
  node [
    id 149
    label "dobry"
  ]
  node [
    id 150
    label "s&#322;uszny"
  ]
  node [
    id 151
    label "skarb"
  ]
  node [
    id 152
    label "pa&#324;stwo"
  ]
  node [
    id 153
    label "narodowy"
  ]
  node [
    id 154
    label "bank"
  ]
  node [
    id 155
    label "polskie"
  ]
  node [
    id 156
    label "og&#243;lnopolski"
  ]
  node [
    id 157
    label "systema"
  ]
  node [
    id 158
    label "obs&#322;uga"
  ]
  node [
    id 159
    label "zabezpieczenie"
  ]
  node [
    id 160
    label "i"
  ]
  node [
    id 161
    label "pozwolenie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 0
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 7
    target 113
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 8
    target 122
  ]
  edge [
    source 8
    target 123
  ]
  edge [
    source 8
    target 124
  ]
  edge [
    source 8
    target 125
  ]
  edge [
    source 8
    target 126
  ]
  edge [
    source 8
    target 127
  ]
  edge [
    source 8
    target 128
  ]
  edge [
    source 8
    target 129
  ]
  edge [
    source 8
    target 130
  ]
  edge [
    source 8
    target 131
  ]
  edge [
    source 8
    target 132
  ]
  edge [
    source 8
    target 133
  ]
  edge [
    source 8
    target 134
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 135
  ]
  edge [
    source 9
    target 136
  ]
  edge [
    source 9
    target 137
  ]
  edge [
    source 9
    target 138
  ]
  edge [
    source 9
    target 139
  ]
  edge [
    source 9
    target 140
  ]
  edge [
    source 9
    target 141
  ]
  edge [
    source 9
    target 142
  ]
  edge [
    source 9
    target 143
  ]
  edge [
    source 10
    target 144
  ]
  edge [
    source 10
    target 145
  ]
  edge [
    source 10
    target 146
  ]
  edge [
    source 10
    target 147
  ]
  edge [
    source 10
    target 148
  ]
  edge [
    source 10
    target 149
  ]
  edge [
    source 10
    target 150
  ]
  edge [
    source 151
    target 152
  ]
  edge [
    source 153
    target 154
  ]
  edge [
    source 153
    target 155
  ]
  edge [
    source 154
    target 155
  ]
  edge [
    source 156
    target 157
  ]
  edge [
    source 156
    target 158
  ]
  edge [
    source 156
    target 159
  ]
  edge [
    source 156
    target 160
  ]
  edge [
    source 156
    target 161
  ]
  edge [
    source 157
    target 158
  ]
  edge [
    source 157
    target 159
  ]
  edge [
    source 157
    target 160
  ]
  edge [
    source 157
    target 161
  ]
  edge [
    source 158
    target 159
  ]
  edge [
    source 158
    target 160
  ]
  edge [
    source 158
    target 161
  ]
  edge [
    source 159
    target 160
  ]
  edge [
    source 159
    target 161
  ]
  edge [
    source 160
    target 161
  ]
]
