graph [
  maxDegree 33
  minDegree 1
  meanDegree 1.96
  density 0.04
  graphCliqueNumber 2
  node [
    id 0
    label "wybi&#263;"
    origin "text"
  ]
  node [
    id 1
    label "gor&#261;c"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "pszyrka"
    origin "text"
  ]
  node [
    id 4
    label "pull"
  ]
  node [
    id 5
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 6
    label "nast&#261;pi&#263;"
  ]
  node [
    id 7
    label "strike"
  ]
  node [
    id 8
    label "pozbija&#263;"
  ]
  node [
    id 9
    label "zabi&#263;"
  ]
  node [
    id 10
    label "thrash"
  ]
  node [
    id 11
    label "sypn&#261;&#263;_si&#281;"
  ]
  node [
    id 12
    label "wywy&#380;szy&#263;"
  ]
  node [
    id 13
    label "pozabija&#263;"
  ]
  node [
    id 14
    label "transgress"
  ]
  node [
    id 15
    label "precipitate"
  ]
  node [
    id 16
    label "beat"
  ]
  node [
    id 17
    label "wydosta&#263;_si&#281;"
  ]
  node [
    id 18
    label "wystuka&#263;"
  ]
  node [
    id 19
    label "usun&#261;&#263;"
  ]
  node [
    id 20
    label "wyperswadowa&#263;"
  ]
  node [
    id 21
    label "zagra&#263;"
  ]
  node [
    id 22
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 23
    label "obi&#263;"
  ]
  node [
    id 24
    label "przegoni&#263;"
  ]
  node [
    id 25
    label "sprawi&#263;"
  ]
  node [
    id 26
    label "wyt&#322;oczy&#263;"
  ]
  node [
    id 27
    label "spowodowa&#263;"
  ]
  node [
    id 28
    label "ukaza&#263;_si&#281;"
  ]
  node [
    id 29
    label "uszkodzi&#263;"
  ]
  node [
    id 30
    label "wskaza&#263;"
  ]
  node [
    id 31
    label "wytworzy&#263;"
  ]
  node [
    id 32
    label "zbi&#263;"
  ]
  node [
    id 33
    label "po&#322;ama&#263;"
  ]
  node [
    id 34
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 35
    label "crush"
  ]
  node [
    id 36
    label "ardor"
  ]
  node [
    id 37
    label "ciep&#322;o"
  ]
  node [
    id 38
    label "war"
  ]
  node [
    id 39
    label "si&#281;ga&#263;"
  ]
  node [
    id 40
    label "trwa&#263;"
  ]
  node [
    id 41
    label "obecno&#347;&#263;"
  ]
  node [
    id 42
    label "stan"
  ]
  node [
    id 43
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 44
    label "stand"
  ]
  node [
    id 45
    label "mie&#263;_miejsce"
  ]
  node [
    id 46
    label "uczestniczy&#263;"
  ]
  node [
    id 47
    label "chodzi&#263;"
  ]
  node [
    id 48
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 49
    label "equal"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
]
