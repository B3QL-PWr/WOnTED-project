graph [
  maxDegree 3
  minDegree 1
  meanDegree 1.6
  density 0.17777777777777778
  graphCliqueNumber 3
  node [
    id 0
    label "gal"
    origin "text"
  ]
  node [
    id 1
    label "metal"
  ]
  node [
    id 2
    label "jednostka_przy&#347;pieszenia"
  ]
  node [
    id 3
    label "borowiec"
  ]
  node [
    id 4
    label "34D"
  ]
  node [
    id 5
    label "ga&#322;a"
  ]
  node [
    id 6
    label "Walter"
  ]
  node [
    id 7
    label "Frederick"
  ]
  node [
    id 8
    label "dzienny"
  ]
  node [
    id 9
    label "gwiazda"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 8
    target 9
  ]
]
