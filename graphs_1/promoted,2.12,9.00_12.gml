graph [
  maxDegree 23
  minDegree 1
  meanDegree 2.272727272727273
  density 0.03496503496503497
  graphCliqueNumber 4
  node [
    id 0
    label "angielski"
    origin "text"
  ]
  node [
    id 1
    label "francuski"
    origin "text"
  ]
  node [
    id 2
    label "rosyjski"
    origin "text"
  ]
  node [
    id 3
    label "hiszpa&#324;ski"
    origin "text"
  ]
  node [
    id 4
    label "polski"
    origin "text"
  ]
  node [
    id 5
    label "angielsko"
  ]
  node [
    id 6
    label "English"
  ]
  node [
    id 7
    label "anglicki"
  ]
  node [
    id 8
    label "j&#281;zyk"
  ]
  node [
    id 9
    label "angol"
  ]
  node [
    id 10
    label "j&#281;zyki_angielskie"
  ]
  node [
    id 11
    label "j&#281;zyk_zachodniogerma&#324;ski"
  ]
  node [
    id 12
    label "brytyjski"
  ]
  node [
    id 13
    label "po_angielsku"
  ]
  node [
    id 14
    label "kurant"
  ]
  node [
    id 15
    label "menuet"
  ]
  node [
    id 16
    label "nami&#281;tny"
  ]
  node [
    id 17
    label "j&#281;zyk_kreolski_seszelski"
  ]
  node [
    id 18
    label "europejski"
  ]
  node [
    id 19
    label "po_francusku"
  ]
  node [
    id 20
    label "chrancuski"
  ]
  node [
    id 21
    label "francuz"
  ]
  node [
    id 22
    label "verlan"
  ]
  node [
    id 23
    label "zachodnioeuropejski"
  ]
  node [
    id 24
    label "bourr&#233;e"
  ]
  node [
    id 25
    label "French"
  ]
  node [
    id 26
    label "j&#281;zyk_roma&#324;ski"
  ]
  node [
    id 27
    label "frankofonia"
  ]
  node [
    id 28
    label "farandola"
  ]
  node [
    id 29
    label "po_rosyjsku"
  ]
  node [
    id 30
    label "wielkoruski"
  ]
  node [
    id 31
    label "kacapski"
  ]
  node [
    id 32
    label "Russian"
  ]
  node [
    id 33
    label "rusek"
  ]
  node [
    id 34
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 35
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 36
    label "&#347;r&#243;dziemnomorski"
  ]
  node [
    id 37
    label "po_hiszpa&#324;sku"
  ]
  node [
    id 38
    label "fandango"
  ]
  node [
    id 39
    label "hispanoj&#281;zyczny"
  ]
  node [
    id 40
    label "po&#322;udniowoeuropejski"
  ]
  node [
    id 41
    label "paso_doble"
  ]
  node [
    id 42
    label "hispanistyka"
  ]
  node [
    id 43
    label "Spanish"
  ]
  node [
    id 44
    label "sarabanda"
  ]
  node [
    id 45
    label "pawana"
  ]
  node [
    id 46
    label "hiszpan"
  ]
  node [
    id 47
    label "lacki"
  ]
  node [
    id 48
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 49
    label "przedmiot"
  ]
  node [
    id 50
    label "sztajer"
  ]
  node [
    id 51
    label "drabant"
  ]
  node [
    id 52
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 53
    label "polak"
  ]
  node [
    id 54
    label "pierogi_ruskie"
  ]
  node [
    id 55
    label "krakowiak"
  ]
  node [
    id 56
    label "Polish"
  ]
  node [
    id 57
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 58
    label "oberek"
  ]
  node [
    id 59
    label "po_polsku"
  ]
  node [
    id 60
    label "mazur"
  ]
  node [
    id 61
    label "chodzony"
  ]
  node [
    id 62
    label "skoczny"
  ]
  node [
    id 63
    label "ryba_po_grecku"
  ]
  node [
    id 64
    label "goniony"
  ]
  node [
    id 65
    label "polsko"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 8
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
]
