graph [
  maxDegree 39
  minDegree 1
  meanDegree 2.048
  density 0.016516129032258065
  graphCliqueNumber 4
  node [
    id 0
    label "kolejny"
    origin "text"
  ]
  node [
    id 1
    label "osoba"
    origin "text"
  ]
  node [
    id 2
    label "zarzut"
    origin "text"
  ]
  node [
    id 3
    label "udzia&#322;"
    origin "text"
  ]
  node [
    id 4
    label "gang"
    origin "text"
  ]
  node [
    id 5
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 6
    label "mia&#322;"
    origin "text"
  ]
  node [
    id 7
    label "wy&#322;udzi&#263;"
    origin "text"
  ]
  node [
    id 8
    label "kasa"
    origin "text"
  ]
  node [
    id 9
    label "s&#261;d"
    origin "text"
  ]
  node [
    id 10
    label "apelacyjny"
    origin "text"
  ]
  node [
    id 11
    label "krak"
    origin "text"
  ]
  node [
    id 12
    label "milion"
    origin "text"
  ]
  node [
    id 13
    label "z&#322;oty"
    origin "text"
  ]
  node [
    id 14
    label "inny"
  ]
  node [
    id 15
    label "nast&#281;pnie"
  ]
  node [
    id 16
    label "kt&#243;ry&#347;"
  ]
  node [
    id 17
    label "kolejno"
  ]
  node [
    id 18
    label "nastopny"
  ]
  node [
    id 19
    label "Zgredek"
  ]
  node [
    id 20
    label "kategoria_gramatyczna"
  ]
  node [
    id 21
    label "Casanova"
  ]
  node [
    id 22
    label "Don_Juan"
  ]
  node [
    id 23
    label "Gargantua"
  ]
  node [
    id 24
    label "Faust"
  ]
  node [
    id 25
    label "profanum"
  ]
  node [
    id 26
    label "Chocho&#322;"
  ]
  node [
    id 27
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 28
    label "koniugacja"
  ]
  node [
    id 29
    label "Winnetou"
  ]
  node [
    id 30
    label "Dwukwiat"
  ]
  node [
    id 31
    label "homo_sapiens"
  ]
  node [
    id 32
    label "Edyp"
  ]
  node [
    id 33
    label "Herkules_Poirot"
  ]
  node [
    id 34
    label "ludzko&#347;&#263;"
  ]
  node [
    id 35
    label "mikrokosmos"
  ]
  node [
    id 36
    label "person"
  ]
  node [
    id 37
    label "Szwejk"
  ]
  node [
    id 38
    label "portrecista"
  ]
  node [
    id 39
    label "Sherlock_Holmes"
  ]
  node [
    id 40
    label "Hamlet"
  ]
  node [
    id 41
    label "duch"
  ]
  node [
    id 42
    label "oddzia&#322;ywanie"
  ]
  node [
    id 43
    label "g&#322;owa"
  ]
  node [
    id 44
    label "Quasimodo"
  ]
  node [
    id 45
    label "Dulcynea"
  ]
  node [
    id 46
    label "Wallenrod"
  ]
  node [
    id 47
    label "Don_Kiszot"
  ]
  node [
    id 48
    label "Plastu&#347;"
  ]
  node [
    id 49
    label "Harry_Potter"
  ]
  node [
    id 50
    label "figura"
  ]
  node [
    id 51
    label "parali&#380;owa&#263;"
  ]
  node [
    id 52
    label "istota"
  ]
  node [
    id 53
    label "Werter"
  ]
  node [
    id 54
    label "antropochoria"
  ]
  node [
    id 55
    label "posta&#263;"
  ]
  node [
    id 56
    label "oskar&#380;enie"
  ]
  node [
    id 57
    label "pretensja"
  ]
  node [
    id 58
    label "oskar&#380;ycielstwo"
  ]
  node [
    id 59
    label "obecno&#347;&#263;"
  ]
  node [
    id 60
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 61
    label "kwota"
  ]
  node [
    id 62
    label "ilo&#347;&#263;"
  ]
  node [
    id 63
    label "banda"
  ]
  node [
    id 64
    label "organizacja"
  ]
  node [
    id 65
    label "przest&#281;pczo&#347;&#263;"
  ]
  node [
    id 66
    label "gangster"
  ]
  node [
    id 67
    label "proszek"
  ]
  node [
    id 68
    label "pozyska&#263;"
  ]
  node [
    id 69
    label "nabra&#263;"
  ]
  node [
    id 70
    label "wycygani&#263;"
  ]
  node [
    id 71
    label "distill"
  ]
  node [
    id 72
    label "skrzynia"
  ]
  node [
    id 73
    label "miejsce"
  ]
  node [
    id 74
    label "instytucja"
  ]
  node [
    id 75
    label "szafa"
  ]
  node [
    id 76
    label "pieni&#261;dze"
  ]
  node [
    id 77
    label "urz&#261;dzenie"
  ]
  node [
    id 78
    label "procesowicz"
  ]
  node [
    id 79
    label "wypowied&#378;"
  ]
  node [
    id 80
    label "pods&#261;dny"
  ]
  node [
    id 81
    label "podejrzany"
  ]
  node [
    id 82
    label "broni&#263;"
  ]
  node [
    id 83
    label "bronienie"
  ]
  node [
    id 84
    label "system"
  ]
  node [
    id 85
    label "my&#347;l"
  ]
  node [
    id 86
    label "wytw&#243;r"
  ]
  node [
    id 87
    label "urz&#261;d"
  ]
  node [
    id 88
    label "konektyw"
  ]
  node [
    id 89
    label "court"
  ]
  node [
    id 90
    label "obrona"
  ]
  node [
    id 91
    label "s&#261;downictwo"
  ]
  node [
    id 92
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 93
    label "forum"
  ]
  node [
    id 94
    label "zesp&#243;&#322;"
  ]
  node [
    id 95
    label "post&#281;powanie"
  ]
  node [
    id 96
    label "skazany"
  ]
  node [
    id 97
    label "wydarzenie"
  ]
  node [
    id 98
    label "&#347;wiadek"
  ]
  node [
    id 99
    label "antylogizm"
  ]
  node [
    id 100
    label "strona"
  ]
  node [
    id 101
    label "oskar&#380;yciel"
  ]
  node [
    id 102
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 103
    label "biuro"
  ]
  node [
    id 104
    label "appellate"
  ]
  node [
    id 105
    label "liczba"
  ]
  node [
    id 106
    label "miljon"
  ]
  node [
    id 107
    label "ba&#324;ka"
  ]
  node [
    id 108
    label "szlachetny"
  ]
  node [
    id 109
    label "metaliczny"
  ]
  node [
    id 110
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 111
    label "z&#322;oci&#347;cie"
  ]
  node [
    id 112
    label "grosz"
  ]
  node [
    id 113
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 114
    label "utytu&#322;owany"
  ]
  node [
    id 115
    label "poz&#322;ocenie"
  ]
  node [
    id 116
    label "Polska"
  ]
  node [
    id 117
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 118
    label "wspania&#322;y"
  ]
  node [
    id 119
    label "doskona&#322;y"
  ]
  node [
    id 120
    label "kochany"
  ]
  node [
    id 121
    label "jednostka_monetarna"
  ]
  node [
    id 122
    label "z&#322;ocenie"
  ]
  node [
    id 123
    label "w"
  ]
  node [
    id 124
    label "Krak"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 9
    target 100
  ]
  edge [
    source 9
    target 101
  ]
  edge [
    source 9
    target 102
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 12
    target 107
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 13
    target 113
  ]
  edge [
    source 13
    target 114
  ]
  edge [
    source 13
    target 115
  ]
  edge [
    source 13
    target 116
  ]
  edge [
    source 13
    target 117
  ]
  edge [
    source 13
    target 118
  ]
  edge [
    source 13
    target 119
  ]
  edge [
    source 13
    target 120
  ]
  edge [
    source 13
    target 121
  ]
  edge [
    source 13
    target 122
  ]
  edge [
    source 123
    target 124
  ]
]
