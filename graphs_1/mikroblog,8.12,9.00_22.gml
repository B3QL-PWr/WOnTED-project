graph [
  maxDegree 42
  minDegree 1
  meanDegree 1.972972972972973
  density 0.02702702702702703
  graphCliqueNumber 2
  node [
    id 0
    label "nieg&#322;upi"
    origin "text"
  ]
  node [
    id 1
    label "pomys&#322;"
    origin "text"
  ]
  node [
    id 2
    label "uprzykrzenie"
    origin "text"
  ]
  node [
    id 3
    label "&#380;ycie"
    origin "text"
  ]
  node [
    id 4
    label "upierdliwy"
    origin "text"
  ]
  node [
    id 5
    label "s&#261;siad"
    origin "text"
  ]
  node [
    id 6
    label "xddddddddddddddddd"
    origin "text"
  ]
  node [
    id 7
    label "heheszki"
    origin "text"
  ]
  node [
    id 8
    label "humor"
    origin "text"
  ]
  node [
    id 9
    label "nieg&#322;upio"
  ]
  node [
    id 10
    label "dobry"
  ]
  node [
    id 11
    label "system"
  ]
  node [
    id 12
    label "wytw&#243;r"
  ]
  node [
    id 13
    label "idea"
  ]
  node [
    id 14
    label "ukra&#347;&#263;"
  ]
  node [
    id 15
    label "ukradzenie"
  ]
  node [
    id 16
    label "pocz&#261;tki"
  ]
  node [
    id 17
    label "energy"
  ]
  node [
    id 18
    label "czas"
  ]
  node [
    id 19
    label "bycie"
  ]
  node [
    id 20
    label "zegar_biologiczny"
  ]
  node [
    id 21
    label "okres_noworodkowy"
  ]
  node [
    id 22
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 23
    label "entity"
  ]
  node [
    id 24
    label "prze&#380;ywanie"
  ]
  node [
    id 25
    label "prze&#380;ycie"
  ]
  node [
    id 26
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 27
    label "wiek_matuzalemowy"
  ]
  node [
    id 28
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 29
    label "dzieci&#324;stwo"
  ]
  node [
    id 30
    label "power"
  ]
  node [
    id 31
    label "szwung"
  ]
  node [
    id 32
    label "menopauza"
  ]
  node [
    id 33
    label "umarcie"
  ]
  node [
    id 34
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 35
    label "life"
  ]
  node [
    id 36
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 37
    label "&#380;ywy"
  ]
  node [
    id 38
    label "rozw&#243;j"
  ]
  node [
    id 39
    label "po&#322;&#243;g"
  ]
  node [
    id 40
    label "byt"
  ]
  node [
    id 41
    label "przebywanie"
  ]
  node [
    id 42
    label "subsistence"
  ]
  node [
    id 43
    label "koleje_losu"
  ]
  node [
    id 44
    label "raj_utracony"
  ]
  node [
    id 45
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 46
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 47
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 48
    label "andropauza"
  ]
  node [
    id 49
    label "warunki"
  ]
  node [
    id 50
    label "do&#380;ywanie"
  ]
  node [
    id 51
    label "niemowl&#281;ctwo"
  ]
  node [
    id 52
    label "umieranie"
  ]
  node [
    id 53
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 54
    label "staro&#347;&#263;"
  ]
  node [
    id 55
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 56
    label "&#347;mier&#263;"
  ]
  node [
    id 57
    label "niezno&#347;ny"
  ]
  node [
    id 58
    label "natr&#281;tny"
  ]
  node [
    id 59
    label "upierdliwie"
  ]
  node [
    id 60
    label "uprzykrzony"
  ]
  node [
    id 61
    label "mieszkaniec"
  ]
  node [
    id 62
    label "somsiad"
  ]
  node [
    id 63
    label "bli&#378;ni"
  ]
  node [
    id 64
    label "stan"
  ]
  node [
    id 65
    label "temper"
  ]
  node [
    id 66
    label "&#347;mieszno&#347;&#263;"
  ]
  node [
    id 67
    label "mechanizm_obronny"
  ]
  node [
    id 68
    label "nastr&#243;j"
  ]
  node [
    id 69
    label "state"
  ]
  node [
    id 70
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 71
    label "samopoczucie"
  ]
  node [
    id 72
    label "fondness"
  ]
  node [
    id 73
    label "p&#322;yn_ustrojowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
]
