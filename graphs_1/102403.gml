graph [
  maxDegree 11
  minDegree 1
  meanDegree 1.9545454545454546
  density 0.045454545454545456
  graphCliqueNumber 2
  node [
    id 0
    label "og&#322;asza&#263;"
    origin "text"
  ]
  node [
    id 1
    label "lista"
    origin "text"
  ]
  node [
    id 2
    label "zwyci&#281;zca"
    origin "text"
  ]
  node [
    id 3
    label "internetowy"
    origin "text"
  ]
  node [
    id 4
    label "wirtualny"
    origin "text"
  ]
  node [
    id 5
    label "konkurs"
    origin "text"
  ]
  node [
    id 6
    label "chopinowski"
    origin "text"
  ]
  node [
    id 7
    label "garage"
    origin "text"
  ]
  node [
    id 8
    label "band"
    origin "text"
  ]
  node [
    id 9
    label "announce"
  ]
  node [
    id 10
    label "publikowa&#263;"
  ]
  node [
    id 11
    label "obwo&#322;ywa&#263;"
  ]
  node [
    id 12
    label "post"
  ]
  node [
    id 13
    label "podawa&#263;"
  ]
  node [
    id 14
    label "wyliczanka"
  ]
  node [
    id 15
    label "catalog"
  ]
  node [
    id 16
    label "stock"
  ]
  node [
    id 17
    label "figurowa&#263;"
  ]
  node [
    id 18
    label "zbi&#243;r"
  ]
  node [
    id 19
    label "book"
  ]
  node [
    id 20
    label "pozycja"
  ]
  node [
    id 21
    label "tekst"
  ]
  node [
    id 22
    label "sumariusz"
  ]
  node [
    id 23
    label "zwyci&#281;&#380;yciel"
  ]
  node [
    id 24
    label "uczestnik"
  ]
  node [
    id 25
    label "nowoczesny"
  ]
  node [
    id 26
    label "elektroniczny"
  ]
  node [
    id 27
    label "sieciowo"
  ]
  node [
    id 28
    label "netowy"
  ]
  node [
    id 29
    label "internetowo"
  ]
  node [
    id 30
    label "mo&#380;liwy"
  ]
  node [
    id 31
    label "wirtualnie"
  ]
  node [
    id 32
    label "nieprawdziwy"
  ]
  node [
    id 33
    label "eliminacje"
  ]
  node [
    id 34
    label "Interwizja"
  ]
  node [
    id 35
    label "emulation"
  ]
  node [
    id 36
    label "impreza"
  ]
  node [
    id 37
    label "casting"
  ]
  node [
    id 38
    label "Eurowizja"
  ]
  node [
    id 39
    label "nab&#243;r"
  ]
  node [
    id 40
    label "chopinowsko"
  ]
  node [
    id 41
    label "szopenowsko"
  ]
  node [
    id 42
    label "charakterystyczny"
  ]
  node [
    id 43
    label "zesp&#243;&#322;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 43
  ]
]
