graph [
  maxDegree 8
  minDegree 1
  meanDegree 1.44
  density 0.06
  graphCliqueNumber 3
  node [
    id 0
    label "pomnik"
    origin "text"
  ]
  node [
    id 1
    label "karol"
    origin "text"
  ]
  node [
    id 2
    label "linneusz"
    origin "text"
  ]
  node [
    id 3
    label "cok&#243;&#322;"
  ]
  node [
    id 4
    label "&#347;wiadectwo"
  ]
  node [
    id 5
    label "p&#322;yta"
  ]
  node [
    id 6
    label "dzie&#322;o"
  ]
  node [
    id 7
    label "rzecz"
  ]
  node [
    id 8
    label "dow&#243;d"
  ]
  node [
    id 9
    label "gr&#243;b"
  ]
  node [
    id 10
    label "Karol"
  ]
  node [
    id 11
    label "Linneusz"
  ]
  node [
    id 12
    label "albert"
  ]
  node [
    id 13
    label "Rachner"
  ]
  node [
    id 14
    label "Moritz"
  ]
  node [
    id 15
    label "weseli&#263;"
  ]
  node [
    id 16
    label "ogr&#243;d"
  ]
  node [
    id 17
    label "botaniczny"
  ]
  node [
    id 18
    label "ii"
  ]
  node [
    id 19
    label "wojna"
  ]
  node [
    id 20
    label "&#347;wiatowy"
  ]
  node [
    id 21
    label "Antonio"
  ]
  node [
    id 22
    label "Comolli"
  ]
  node [
    id 23
    label "bohdan"
  ]
  node [
    id 24
    label "Chmielewski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 20
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 23
    target 24
  ]
]
