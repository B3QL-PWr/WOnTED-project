graph [
  maxDegree 12
  minDegree 1
  meanDegree 2.217391304347826
  density 0.04927536231884058
  graphCliqueNumber 4
  node [
    id 0
    label "henrykowo"
    origin "text"
  ]
  node [
    id 1
    label "powiat"
    origin "text"
  ]
  node [
    id 2
    label "lidzbarski"
    origin "text"
  ]
  node [
    id 3
    label "gmina"
  ]
  node [
    id 4
    label "jednostka_administracyjna"
  ]
  node [
    id 5
    label "wojew&#243;dztwo"
  ]
  node [
    id 6
    label "warmi&#324;sko"
  ]
  node [
    id 7
    label "mazurski"
  ]
  node [
    id 8
    label "drogi"
  ]
  node [
    id 9
    label "wojew&#243;dzki"
  ]
  node [
    id 10
    label "nr"
  ]
  node [
    id 11
    label "507"
  ]
  node [
    id 12
    label "Henryka"
  ]
  node [
    id 13
    label "Laberyka"
  ]
  node [
    id 14
    label "kapitu&#322;a"
  ]
  node [
    id 15
    label "warmi&#324;ski"
  ]
  node [
    id 16
    label "wojna"
  ]
  node [
    id 17
    label "g&#322;odowy"
  ]
  node [
    id 18
    label "trzynastoletni"
  ]
  node [
    id 19
    label "ko&#347;ci&#243;&#322;"
  ]
  node [
    id 20
    label "PW"
  ]
  node [
    id 21
    label "&#347;wi&#281;ty"
  ]
  node [
    id 22
    label "Katarzyna"
  ]
  node [
    id 23
    label "Christian"
  ]
  node [
    id 24
    label "beniamin"
  ]
  node [
    id 25
    label "Szultza"
  ]
  node [
    id 26
    label "Andrzej"
  ]
  node [
    id 27
    label "von"
  ]
  node [
    id 28
    label "Hattena"
  ]
  node [
    id 29
    label "J"
  ]
  node [
    id 30
    label "Pipera"
  ]
  node [
    id 31
    label "Maria"
  ]
  node [
    id 32
    label "Magdalena"
  ]
  node [
    id 33
    label "J&#243;zefa"
  ]
  node [
    id 34
    label "koronacja"
  ]
  node [
    id 35
    label "zeszyt"
  ]
  node [
    id 36
    label "dzieci&#261;tko"
  ]
  node [
    id 37
    label "zdj&#281;cie"
  ]
  node [
    id 38
    label "krzy&#380;"
  ]
  node [
    id 39
    label "Antoni"
  ]
  node [
    id 40
    label "uzdrawia&#263;"
  ]
  node [
    id 41
    label "chory"
  ]
  node [
    id 42
    label "Piotr"
  ]
  node [
    id 43
    label "Kolberg"
  ]
  node [
    id 44
    label "Bernhard"
  ]
  node [
    id 45
    label "Poschmann"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 8
    target 11
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 18
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 21
  ]
  edge [
    source 19
    target 22
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 22
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 31
  ]
  edge [
    source 21
    target 32
  ]
  edge [
    source 21
    target 33
  ]
  edge [
    source 21
    target 34
  ]
  edge [
    source 21
    target 35
  ]
  edge [
    source 21
    target 36
  ]
  edge [
    source 21
    target 39
  ]
  edge [
    source 21
    target 40
  ]
  edge [
    source 21
    target 41
  ]
  edge [
    source 22
    target 34
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 25
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 28
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 33
    target 35
  ]
  edge [
    source 33
    target 36
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 37
  ]
  edge [
    source 35
    target 38
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 41
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 44
    target 45
  ]
]
