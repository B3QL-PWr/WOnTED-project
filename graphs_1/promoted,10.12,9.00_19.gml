graph [
  maxDegree 40
  minDegree 1
  meanDegree 1.9777777777777779
  density 0.022222222222222223
  graphCliqueNumber 2
  node [
    id 0
    label "obci&#261;&#380;enie"
    origin "text"
  ]
  node [
    id 1
    label "polski"
    origin "text"
  ]
  node [
    id 2
    label "gospodarka"
    origin "text"
  ]
  node [
    id 3
    label "podatek"
    origin "text"
  ]
  node [
    id 4
    label "wzrosn&#261;&#263;"
    origin "text"
  ]
  node [
    id 5
    label "zawada"
  ]
  node [
    id 6
    label "hindrance"
  ]
  node [
    id 7
    label "baga&#380;"
  ]
  node [
    id 8
    label "charge"
  ]
  node [
    id 9
    label "encumbrance"
  ]
  node [
    id 10
    label "psucie_si&#281;"
  ]
  node [
    id 11
    label "oskar&#380;enie"
  ]
  node [
    id 12
    label "zaszkodzenie"
  ]
  node [
    id 13
    label "loading"
  ]
  node [
    id 14
    label "zobowi&#261;zanie"
  ]
  node [
    id 15
    label "na&#322;o&#380;enie"
  ]
  node [
    id 16
    label "lacki"
  ]
  node [
    id 17
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 18
    label "przedmiot"
  ]
  node [
    id 19
    label "sztajer"
  ]
  node [
    id 20
    label "drabant"
  ]
  node [
    id 21
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 22
    label "polak"
  ]
  node [
    id 23
    label "pierogi_ruskie"
  ]
  node [
    id 24
    label "krakowiak"
  ]
  node [
    id 25
    label "Polish"
  ]
  node [
    id 26
    label "j&#281;zyk"
  ]
  node [
    id 27
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 28
    label "oberek"
  ]
  node [
    id 29
    label "po_polsku"
  ]
  node [
    id 30
    label "mazur"
  ]
  node [
    id 31
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 32
    label "chodzony"
  ]
  node [
    id 33
    label "skoczny"
  ]
  node [
    id 34
    label "ryba_po_grecku"
  ]
  node [
    id 35
    label "goniony"
  ]
  node [
    id 36
    label "polsko"
  ]
  node [
    id 37
    label "rynek"
  ]
  node [
    id 38
    label "pole"
  ]
  node [
    id 39
    label "szkolnictwo"
  ]
  node [
    id 40
    label "przemys&#322;"
  ]
  node [
    id 41
    label "gospodarka_wodna"
  ]
  node [
    id 42
    label "fabryka"
  ]
  node [
    id 43
    label "rolnictwo"
  ]
  node [
    id 44
    label "gospodarka_le&#347;na"
  ]
  node [
    id 45
    label "gospodarowa&#263;"
  ]
  node [
    id 46
    label "sektor_prywatny"
  ]
  node [
    id 47
    label "obronno&#347;&#263;"
  ]
  node [
    id 48
    label "obora"
  ]
  node [
    id 49
    label "mieszkalnictwo"
  ]
  node [
    id 50
    label "sektor_publiczny"
  ]
  node [
    id 51
    label "czerwona_strefa"
  ]
  node [
    id 52
    label "struktura"
  ]
  node [
    id 53
    label "stodo&#322;a"
  ]
  node [
    id 54
    label "rybo&#322;&#243;wstwo"
  ]
  node [
    id 55
    label "produkowanie"
  ]
  node [
    id 56
    label "gospodarowanie"
  ]
  node [
    id 57
    label "agregat_ekonomiczny"
  ]
  node [
    id 58
    label "sch&#322;adza&#263;"
  ]
  node [
    id 59
    label "spichlerz"
  ]
  node [
    id 60
    label "inwentarz"
  ]
  node [
    id 61
    label "transport"
  ]
  node [
    id 62
    label "sch&#322;odzenie"
  ]
  node [
    id 63
    label "s&#322;u&#380;ba_zdrowia"
  ]
  node [
    id 64
    label "miejsce_pracy"
  ]
  node [
    id 65
    label "wytw&#243;rnia"
  ]
  node [
    id 66
    label "farmaceutyka"
  ]
  node [
    id 67
    label "bud&#380;et&#243;wka"
  ]
  node [
    id 68
    label "skarbowo&#347;&#263;"
  ]
  node [
    id 69
    label "administracja"
  ]
  node [
    id 70
    label "sch&#322;adzanie"
  ]
  node [
    id 71
    label "bankowo&#347;&#263;"
  ]
  node [
    id 72
    label "zasada"
  ]
  node [
    id 73
    label "sch&#322;odzi&#263;"
  ]
  node [
    id 74
    label "regulacja_cen"
  ]
  node [
    id 75
    label "bilans_handlowy"
  ]
  node [
    id 76
    label "op&#322;ata"
  ]
  node [
    id 77
    label "danina"
  ]
  node [
    id 78
    label "trybut"
  ]
  node [
    id 79
    label "tr&#243;jk&#261;t_Harbergera"
  ]
  node [
    id 80
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 81
    label "sta&#263;_si&#281;"
  ]
  node [
    id 82
    label "zrodzi&#263;_si&#281;"
  ]
  node [
    id 83
    label "rozwin&#261;&#263;_si&#281;"
  ]
  node [
    id 84
    label "rise"
  ]
  node [
    id 85
    label "increase"
  ]
  node [
    id 86
    label "urosn&#261;&#263;"
  ]
  node [
    id 87
    label "narosn&#261;&#263;"
  ]
  node [
    id 88
    label "wzbi&#263;_si&#281;"
  ]
  node [
    id 89
    label "zrobi&#263;_si&#281;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
]
