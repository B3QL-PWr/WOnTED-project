graph [
  maxDegree 45
  minDegree 1
  meanDegree 1.9814814814814814
  density 0.018518518518518517
  graphCliqueNumber 2
  node [
    id 0
    label "rom"
    origin "text"
  ]
  node [
    id 1
    label "k&#322;odzki"
    origin "text"
  ]
  node [
    id 2
    label "chodzenie"
    origin "text"
  ]
  node [
    id 3
    label "szko&#322;a"
    origin "text"
  ]
  node [
    id 4
    label "dostawa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "stypendium"
    origin "text"
  ]
  node [
    id 6
    label "motywacyjny"
    origin "text"
  ]
  node [
    id 7
    label "po_k&#322;odzku"
  ]
  node [
    id 8
    label "polski"
  ]
  node [
    id 9
    label "regionalny"
  ]
  node [
    id 10
    label "uchodzenie_si&#281;"
  ]
  node [
    id 11
    label "uruchamianie"
  ]
  node [
    id 12
    label "przechodzenie"
  ]
  node [
    id 13
    label "ubieranie"
  ]
  node [
    id 14
    label "w&#322;&#261;czanie"
  ]
  node [
    id 15
    label "ucz&#281;szczanie"
  ]
  node [
    id 16
    label "noszenie"
  ]
  node [
    id 17
    label "rozdeptywanie"
  ]
  node [
    id 18
    label "wydeptanie"
  ]
  node [
    id 19
    label "wydeptywanie"
  ]
  node [
    id 20
    label "ko&#322;atanie_si&#281;"
  ]
  node [
    id 21
    label "zdeptanie"
  ]
  node [
    id 22
    label "uganianie_si&#281;"
  ]
  node [
    id 23
    label "p&#322;ywanie"
  ]
  node [
    id 24
    label "jitter"
  ]
  node [
    id 25
    label "funkcja"
  ]
  node [
    id 26
    label "dzianie_si&#281;"
  ]
  node [
    id 27
    label "zmierzanie"
  ]
  node [
    id 28
    label "wear"
  ]
  node [
    id 29
    label "deptanie"
  ]
  node [
    id 30
    label "rozpowszechnianie_si&#281;"
  ]
  node [
    id 31
    label "rozdeptanie"
  ]
  node [
    id 32
    label "ubieranie_si&#281;"
  ]
  node [
    id 33
    label "znoszenie"
  ]
  node [
    id 34
    label "staranie_si&#281;"
  ]
  node [
    id 35
    label "podtrzymywanie"
  ]
  node [
    id 36
    label "przenoszenie"
  ]
  node [
    id 37
    label "poruszanie_si&#281;"
  ]
  node [
    id 38
    label "nakr&#281;canie"
  ]
  node [
    id 39
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 40
    label "pochodzenie"
  ]
  node [
    id 41
    label "uruchomienie"
  ]
  node [
    id 42
    label "bywanie"
  ]
  node [
    id 43
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 44
    label "tr&#243;jstronny"
  ]
  node [
    id 45
    label "impact"
  ]
  node [
    id 46
    label "dochodzenie"
  ]
  node [
    id 47
    label "w&#322;&#261;czenie"
  ]
  node [
    id 48
    label "zatrzymanie"
  ]
  node [
    id 49
    label "obchodzenie"
  ]
  node [
    id 50
    label "donoszenie"
  ]
  node [
    id 51
    label "nakr&#281;cenie"
  ]
  node [
    id 52
    label "donaszanie"
  ]
  node [
    id 53
    label "Mickiewicz"
  ]
  node [
    id 54
    label "czas"
  ]
  node [
    id 55
    label "szkolenie"
  ]
  node [
    id 56
    label "przepisa&#263;"
  ]
  node [
    id 57
    label "lesson"
  ]
  node [
    id 58
    label "grupa"
  ]
  node [
    id 59
    label "praktyka"
  ]
  node [
    id 60
    label "metoda"
  ]
  node [
    id 61
    label "niepokalanki"
  ]
  node [
    id 62
    label "kara"
  ]
  node [
    id 63
    label "zda&#263;"
  ]
  node [
    id 64
    label "form"
  ]
  node [
    id 65
    label "kwalifikacje"
  ]
  node [
    id 66
    label "system"
  ]
  node [
    id 67
    label "przepisanie"
  ]
  node [
    id 68
    label "sztuba"
  ]
  node [
    id 69
    label "wiedza"
  ]
  node [
    id 70
    label "stopek"
  ]
  node [
    id 71
    label "school"
  ]
  node [
    id 72
    label "absolwent"
  ]
  node [
    id 73
    label "urszulanki"
  ]
  node [
    id 74
    label "gabinet"
  ]
  node [
    id 75
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 76
    label "ideologia"
  ]
  node [
    id 77
    label "lekcja"
  ]
  node [
    id 78
    label "muzyka"
  ]
  node [
    id 79
    label "podr&#281;cznik"
  ]
  node [
    id 80
    label "zdanie"
  ]
  node [
    id 81
    label "siedziba"
  ]
  node [
    id 82
    label "sekretariat"
  ]
  node [
    id 83
    label "nauka"
  ]
  node [
    id 84
    label "do&#347;wiadczenie"
  ]
  node [
    id 85
    label "tablica"
  ]
  node [
    id 86
    label "teren_szko&#322;y"
  ]
  node [
    id 87
    label "instytucja"
  ]
  node [
    id 88
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 89
    label "skolaryzacja"
  ]
  node [
    id 90
    label "&#322;awa_szkolna"
  ]
  node [
    id 91
    label "klasa"
  ]
  node [
    id 92
    label "si&#281;ga&#263;"
  ]
  node [
    id 93
    label "by&#263;"
  ]
  node [
    id 94
    label "uzyskiwa&#263;"
  ]
  node [
    id 95
    label "wystarcza&#263;"
  ]
  node [
    id 96
    label "range"
  ]
  node [
    id 97
    label "winnings"
  ]
  node [
    id 98
    label "otrzymywa&#263;"
  ]
  node [
    id 99
    label "mie&#263;_miejsce"
  ]
  node [
    id 100
    label "opanowywa&#263;"
  ]
  node [
    id 101
    label "kupowa&#263;"
  ]
  node [
    id 102
    label "nabywa&#263;"
  ]
  node [
    id 103
    label "bra&#263;"
  ]
  node [
    id 104
    label "obskakiwa&#263;"
  ]
  node [
    id 105
    label "&#347;wiadczenie"
  ]
  node [
    id 106
    label "motywacyjnie"
  ]
  node [
    id 107
    label "s&#322;owotw&#243;rczy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
]
