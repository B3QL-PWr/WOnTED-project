graph [
  maxDegree 29
  minDegree 1
  meanDegree 2.0427807486631018
  density 0.01098269219711345
  graphCliqueNumber 3
  node [
    id 0
    label "rama"
    origin "text"
  ]
  node [
    id 1
    label "badanie"
    origin "text"
  ]
  node [
    id 2
    label "wykorzystanie"
    origin "text"
  ]
  node [
    id 3
    label "licencja"
    origin "text"
  ]
  node [
    id 4
    label "creative"
    origin "text"
  ]
  node [
    id 5
    label "commons"
    origin "text"
  ]
  node [
    id 6
    label "serwis"
    origin "text"
  ]
  node [
    id 7
    label "fotograficzny"
    origin "text"
  ]
  node [
    id 8
    label "flickr"
    origin "text"
  ]
  node [
    id 9
    label "prowadzi&#263;"
    origin "text"
  ]
  node [
    id 10
    label "niewielki"
    origin "text"
  ]
  node [
    id 11
    label "badan"
    origin "text"
  ]
  node [
    id 12
    label "sonda&#380;owy"
    origin "text"
  ]
  node [
    id 13
    label "w&#347;r&#243;d"
    origin "text"
  ]
  node [
    id 14
    label "u&#380;ytkownik"
    origin "text"
  ]
  node [
    id 15
    label "raz"
    origin "text"
  ]
  node [
    id 16
    label "uda&#263;"
    origin "text"
  ]
  node [
    id 17
    label "si&#281;"
    origin "text"
  ]
  node [
    id 18
    label "zebra&#263;"
    origin "text"
  ]
  node [
    id 19
    label "sporo"
    origin "text"
  ]
  node [
    id 20
    label "odpowied&#378;"
    origin "text"
  ]
  node [
    id 21
    label "dzi&#281;ki"
    origin "text"
  ]
  node [
    id 22
    label "reklama"
    origin "text"
  ]
  node [
    id 23
    label "boing"
    origin "text"
  ]
  node [
    id 24
    label "boingu"
    origin "text"
  ]
  node [
    id 25
    label "ale"
    origin "text"
  ]
  node [
    id 26
    label "by&#263;"
    origin "text"
  ]
  node [
    id 27
    label "angielskoj&#281;zyczny"
    origin "text"
  ]
  node [
    id 28
    label "zakres"
  ]
  node [
    id 29
    label "dodatek"
  ]
  node [
    id 30
    label "struktura"
  ]
  node [
    id 31
    label "stela&#380;"
  ]
  node [
    id 32
    label "za&#322;o&#380;enie"
  ]
  node [
    id 33
    label "human_body"
  ]
  node [
    id 34
    label "szablon"
  ]
  node [
    id 35
    label "oprawa"
  ]
  node [
    id 36
    label "paczka"
  ]
  node [
    id 37
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 38
    label "obramowanie"
  ]
  node [
    id 39
    label "pojazd"
  ]
  node [
    id 40
    label "postawa"
  ]
  node [
    id 41
    label "element_konstrukcyjny"
  ]
  node [
    id 42
    label "usi&#322;owanie"
  ]
  node [
    id 43
    label "examination"
  ]
  node [
    id 44
    label "investigation"
  ]
  node [
    id 45
    label "ustalenie"
  ]
  node [
    id 46
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 47
    label "ustalanie"
  ]
  node [
    id 48
    label "bia&#322;a_niedziela"
  ]
  node [
    id 49
    label "analysis"
  ]
  node [
    id 50
    label "rozpatrywanie"
  ]
  node [
    id 51
    label "wziernikowanie"
  ]
  node [
    id 52
    label "obserwowanie"
  ]
  node [
    id 53
    label "omawianie"
  ]
  node [
    id 54
    label "sprawdzanie"
  ]
  node [
    id 55
    label "udowadnianie"
  ]
  node [
    id 56
    label "diagnostyka"
  ]
  node [
    id 57
    label "czynno&#347;&#263;"
  ]
  node [
    id 58
    label "macanie"
  ]
  node [
    id 59
    label "rektalny"
  ]
  node [
    id 60
    label "penetrowanie"
  ]
  node [
    id 61
    label "krytykowanie"
  ]
  node [
    id 62
    label "kontrola"
  ]
  node [
    id 63
    label "dociekanie"
  ]
  node [
    id 64
    label "zrecenzowanie"
  ]
  node [
    id 65
    label "praca"
  ]
  node [
    id 66
    label "rezultat"
  ]
  node [
    id 67
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 68
    label "zrobienie"
  ]
  node [
    id 69
    label "use"
  ]
  node [
    id 70
    label "u&#380;ycie"
  ]
  node [
    id 71
    label "stosowanie"
  ]
  node [
    id 72
    label "nau&#380;ywanie_si&#281;"
  ]
  node [
    id 73
    label "u&#380;yteczny"
  ]
  node [
    id 74
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 75
    label "exploitation"
  ]
  node [
    id 76
    label "prawo"
  ]
  node [
    id 77
    label "licencjonowa&#263;"
  ]
  node [
    id 78
    label "pozwolenie"
  ]
  node [
    id 79
    label "hodowla"
  ]
  node [
    id 80
    label "rasowy"
  ]
  node [
    id 81
    label "license"
  ]
  node [
    id 82
    label "zezwolenie"
  ]
  node [
    id 83
    label "za&#347;wiadczenie"
  ]
  node [
    id 84
    label "mecz"
  ]
  node [
    id 85
    label "service"
  ]
  node [
    id 86
    label "wytw&#243;r"
  ]
  node [
    id 87
    label "zak&#322;ad"
  ]
  node [
    id 88
    label "us&#322;uga"
  ]
  node [
    id 89
    label "uderzenie"
  ]
  node [
    id 90
    label "doniesienie"
  ]
  node [
    id 91
    label "zastawa"
  ]
  node [
    id 92
    label "YouTube"
  ]
  node [
    id 93
    label "punkt"
  ]
  node [
    id 94
    label "porcja"
  ]
  node [
    id 95
    label "strona"
  ]
  node [
    id 96
    label "wizualny"
  ]
  node [
    id 97
    label "wierny"
  ]
  node [
    id 98
    label "fotograficznie"
  ]
  node [
    id 99
    label "control"
  ]
  node [
    id 100
    label "eksponowa&#263;"
  ]
  node [
    id 101
    label "kre&#347;li&#263;"
  ]
  node [
    id 102
    label "g&#243;rowa&#263;"
  ]
  node [
    id 103
    label "message"
  ]
  node [
    id 104
    label "partner"
  ]
  node [
    id 105
    label "string"
  ]
  node [
    id 106
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 107
    label "przesuwa&#263;"
  ]
  node [
    id 108
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 109
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 110
    label "powodowa&#263;"
  ]
  node [
    id 111
    label "kierowa&#263;"
  ]
  node [
    id 112
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 113
    label "robi&#263;"
  ]
  node [
    id 114
    label "manipulate"
  ]
  node [
    id 115
    label "&#380;y&#263;"
  ]
  node [
    id 116
    label "navigate"
  ]
  node [
    id 117
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 118
    label "ukierunkowywa&#263;"
  ]
  node [
    id 119
    label "linia_melodyczna"
  ]
  node [
    id 120
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 121
    label "prowadzenie"
  ]
  node [
    id 122
    label "tworzy&#263;"
  ]
  node [
    id 123
    label "przewy&#380;sza&#263;"
  ]
  node [
    id 124
    label "sterowa&#263;"
  ]
  node [
    id 125
    label "krzywa"
  ]
  node [
    id 126
    label "ma&#322;o"
  ]
  node [
    id 127
    label "nielicznie"
  ]
  node [
    id 128
    label "ma&#322;y"
  ]
  node [
    id 129
    label "niewa&#380;ny"
  ]
  node [
    id 130
    label "pr&#243;bny"
  ]
  node [
    id 131
    label "sonda&#380;owo"
  ]
  node [
    id 132
    label "j&#281;zykowo"
  ]
  node [
    id 133
    label "podmiot"
  ]
  node [
    id 134
    label "chwila"
  ]
  node [
    id 135
    label "cios"
  ]
  node [
    id 136
    label "time"
  ]
  node [
    id 137
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 138
    label "represent"
  ]
  node [
    id 139
    label "wzi&#261;&#263;"
  ]
  node [
    id 140
    label "spowodowa&#263;"
  ]
  node [
    id 141
    label "sprz&#261;tn&#261;&#263;"
  ]
  node [
    id 142
    label "przyci&#261;gn&#261;&#263;"
  ]
  node [
    id 143
    label "wezbra&#263;"
  ]
  node [
    id 144
    label "u&#322;o&#380;y&#263;"
  ]
  node [
    id 145
    label "congregate"
  ]
  node [
    id 146
    label "oszcz&#281;dzi&#263;"
  ]
  node [
    id 147
    label "dosta&#263;"
  ]
  node [
    id 148
    label "pozyska&#263;"
  ]
  node [
    id 149
    label "zgromadzi&#263;"
  ]
  node [
    id 150
    label "przej&#261;&#263;"
  ]
  node [
    id 151
    label "umie&#347;ci&#263;"
  ]
  node [
    id 152
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 153
    label "raise"
  ]
  node [
    id 154
    label "skupi&#263;"
  ]
  node [
    id 155
    label "nat&#281;&#380;y&#263;"
  ]
  node [
    id 156
    label "plane"
  ]
  node [
    id 157
    label "spory"
  ]
  node [
    id 158
    label "dokument"
  ]
  node [
    id 159
    label "rozmowa"
  ]
  node [
    id 160
    label "reakcja"
  ]
  node [
    id 161
    label "wyj&#347;cie"
  ]
  node [
    id 162
    label "react"
  ]
  node [
    id 163
    label "respondent"
  ]
  node [
    id 164
    label "replica"
  ]
  node [
    id 165
    label "copywriting"
  ]
  node [
    id 166
    label "brief"
  ]
  node [
    id 167
    label "bran&#380;a"
  ]
  node [
    id 168
    label "informacja"
  ]
  node [
    id 169
    label "promowa&#263;"
  ]
  node [
    id 170
    label "akcja"
  ]
  node [
    id 171
    label "wypromowa&#263;"
  ]
  node [
    id 172
    label "samplowanie"
  ]
  node [
    id 173
    label "tekst"
  ]
  node [
    id 174
    label "piwo"
  ]
  node [
    id 175
    label "si&#281;ga&#263;"
  ]
  node [
    id 176
    label "trwa&#263;"
  ]
  node [
    id 177
    label "obecno&#347;&#263;"
  ]
  node [
    id 178
    label "stan"
  ]
  node [
    id 179
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 180
    label "stand"
  ]
  node [
    id 181
    label "mie&#263;_miejsce"
  ]
  node [
    id 182
    label "uczestniczy&#263;"
  ]
  node [
    id 183
    label "chodzi&#263;"
  ]
  node [
    id 184
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 185
    label "equal"
  ]
  node [
    id 186
    label "po_angielsku"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 14
  ]
  edge [
    source 6
    target 15
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 18
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 9
    target 100
  ]
  edge [
    source 9
    target 101
  ]
  edge [
    source 9
    target 102
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 104
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 9
    target 116
  ]
  edge [
    source 9
    target 117
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 9
    target 119
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 126
  ]
  edge [
    source 10
    target 127
  ]
  edge [
    source 10
    target 128
  ]
  edge [
    source 10
    target 129
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 12
    target 131
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 20
  ]
  edge [
    source 14
    target 27
  ]
  edge [
    source 14
    target 132
  ]
  edge [
    source 14
    target 133
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 134
  ]
  edge [
    source 15
    target 89
  ]
  edge [
    source 15
    target 135
  ]
  edge [
    source 15
    target 136
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 137
  ]
  edge [
    source 16
    target 138
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 139
  ]
  edge [
    source 18
    target 140
  ]
  edge [
    source 18
    target 141
  ]
  edge [
    source 18
    target 142
  ]
  edge [
    source 18
    target 143
  ]
  edge [
    source 18
    target 144
  ]
  edge [
    source 18
    target 145
  ]
  edge [
    source 18
    target 146
  ]
  edge [
    source 18
    target 147
  ]
  edge [
    source 18
    target 148
  ]
  edge [
    source 18
    target 149
  ]
  edge [
    source 18
    target 150
  ]
  edge [
    source 18
    target 151
  ]
  edge [
    source 18
    target 152
  ]
  edge [
    source 18
    target 153
  ]
  edge [
    source 18
    target 154
  ]
  edge [
    source 18
    target 155
  ]
  edge [
    source 18
    target 156
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 157
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 26
  ]
  edge [
    source 20
    target 158
  ]
  edge [
    source 20
    target 159
  ]
  edge [
    source 20
    target 160
  ]
  edge [
    source 20
    target 161
  ]
  edge [
    source 20
    target 162
  ]
  edge [
    source 20
    target 163
  ]
  edge [
    source 20
    target 164
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 165
  ]
  edge [
    source 22
    target 166
  ]
  edge [
    source 22
    target 167
  ]
  edge [
    source 22
    target 168
  ]
  edge [
    source 22
    target 169
  ]
  edge [
    source 22
    target 170
  ]
  edge [
    source 22
    target 171
  ]
  edge [
    source 22
    target 172
  ]
  edge [
    source 22
    target 173
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 174
  ]
  edge [
    source 26
    target 175
  ]
  edge [
    source 26
    target 176
  ]
  edge [
    source 26
    target 177
  ]
  edge [
    source 26
    target 178
  ]
  edge [
    source 26
    target 179
  ]
  edge [
    source 26
    target 180
  ]
  edge [
    source 26
    target 181
  ]
  edge [
    source 26
    target 182
  ]
  edge [
    source 26
    target 183
  ]
  edge [
    source 26
    target 184
  ]
  edge [
    source 26
    target 185
  ]
  edge [
    source 27
    target 186
  ]
]
