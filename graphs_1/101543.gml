graph [
  maxDegree 49
  minDegree 1
  meanDegree 2.010204081632653
  density 0.010308738880167452
  graphCliqueNumber 2
  node [
    id 0
    label "lmde"
    origin "text"
  ]
  node [
    id 1
    label "kry&#263;"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "pod"
    origin "text"
  ]
  node [
    id 4
    label "system"
    origin "text"
  ]
  node [
    id 5
    label "debian"
    origin "text"
  ]
  node [
    id 6
    label "testing"
    origin "text"
  ]
  node [
    id 7
    label "kilka"
    origin "text"
  ]
  node [
    id 8
    label "program"
    origin "text"
  ]
  node [
    id 9
    label "gui"
    origin "text"
  ]
  node [
    id 10
    label "maska"
    origin "text"
  ]
  node [
    id 11
    label "by&#263;"
    origin "text"
  ]
  node [
    id 12
    label "czysty"
    origin "text"
  ]
  node [
    id 13
    label "ubuntu"
    origin "text"
  ]
  node [
    id 14
    label "report"
  ]
  node [
    id 15
    label "cover"
  ]
  node [
    id 16
    label "zap&#322;adnia&#263;"
  ]
  node [
    id 17
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 18
    label "chowany"
  ]
  node [
    id 19
    label "umieszcza&#263;"
  ]
  node [
    id 20
    label "zas&#322;ania&#263;"
  ]
  node [
    id 21
    label "os&#322;ania&#263;"
  ]
  node [
    id 22
    label "farba"
  ]
  node [
    id 23
    label "ukrywa&#263;"
  ]
  node [
    id 24
    label "cache"
  ]
  node [
    id 25
    label "przeciwdzia&#322;a&#263;"
  ]
  node [
    id 26
    label "rozwija&#263;"
  ]
  node [
    id 27
    label "pokrywa&#263;"
  ]
  node [
    id 28
    label "zataja&#263;"
  ]
  node [
    id 29
    label "zawiera&#263;"
  ]
  node [
    id 30
    label "meliniarz"
  ]
  node [
    id 31
    label "hide"
  ]
  node [
    id 32
    label "robi&#263;"
  ]
  node [
    id 33
    label "r&#243;wna&#263;"
  ]
  node [
    id 34
    label "pilnowa&#263;"
  ]
  node [
    id 35
    label "model"
  ]
  node [
    id 36
    label "sk&#322;ad"
  ]
  node [
    id 37
    label "zachowanie"
  ]
  node [
    id 38
    label "podstawa"
  ]
  node [
    id 39
    label "porz&#261;dek"
  ]
  node [
    id 40
    label "Android"
  ]
  node [
    id 41
    label "przyn&#281;ta"
  ]
  node [
    id 42
    label "jednostka_geologiczna"
  ]
  node [
    id 43
    label "metoda"
  ]
  node [
    id 44
    label "podsystem"
  ]
  node [
    id 45
    label "p&#322;&#243;d"
  ]
  node [
    id 46
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 47
    label "s&#261;d"
  ]
  node [
    id 48
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 49
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 50
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 51
    label "j&#261;dro"
  ]
  node [
    id 52
    label "eratem"
  ]
  node [
    id 53
    label "ryba"
  ]
  node [
    id 54
    label "pulpit"
  ]
  node [
    id 55
    label "struktura"
  ]
  node [
    id 56
    label "spos&#243;b"
  ]
  node [
    id 57
    label "oddzia&#322;"
  ]
  node [
    id 58
    label "usenet"
  ]
  node [
    id 59
    label "o&#347;"
  ]
  node [
    id 60
    label "oprogramowanie"
  ]
  node [
    id 61
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 62
    label "poj&#281;cie"
  ]
  node [
    id 63
    label "w&#281;dkarstwo"
  ]
  node [
    id 64
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 65
    label "Leopard"
  ]
  node [
    id 66
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 67
    label "systemik"
  ]
  node [
    id 68
    label "rozprz&#261;c"
  ]
  node [
    id 69
    label "cybernetyk"
  ]
  node [
    id 70
    label "konstelacja"
  ]
  node [
    id 71
    label "doktryna"
  ]
  node [
    id 72
    label "net"
  ]
  node [
    id 73
    label "zbi&#243;r"
  ]
  node [
    id 74
    label "method"
  ]
  node [
    id 75
    label "systemat"
  ]
  node [
    id 76
    label "&#347;ledziowate"
  ]
  node [
    id 77
    label "spis"
  ]
  node [
    id 78
    label "odinstalowanie"
  ]
  node [
    id 79
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 80
    label "za&#322;o&#380;enie"
  ]
  node [
    id 81
    label "emitowanie"
  ]
  node [
    id 82
    label "odinstalowywanie"
  ]
  node [
    id 83
    label "instrukcja"
  ]
  node [
    id 84
    label "punkt"
  ]
  node [
    id 85
    label "teleferie"
  ]
  node [
    id 86
    label "emitowa&#263;"
  ]
  node [
    id 87
    label "wytw&#243;r"
  ]
  node [
    id 88
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 89
    label "sekcja_krytyczna"
  ]
  node [
    id 90
    label "oferta"
  ]
  node [
    id 91
    label "prezentowa&#263;"
  ]
  node [
    id 92
    label "blok"
  ]
  node [
    id 93
    label "podprogram"
  ]
  node [
    id 94
    label "tryb"
  ]
  node [
    id 95
    label "dzia&#322;"
  ]
  node [
    id 96
    label "broszura"
  ]
  node [
    id 97
    label "deklaracja"
  ]
  node [
    id 98
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 99
    label "struktura_organizacyjna"
  ]
  node [
    id 100
    label "zaprezentowanie"
  ]
  node [
    id 101
    label "informatyka"
  ]
  node [
    id 102
    label "booklet"
  ]
  node [
    id 103
    label "menu"
  ]
  node [
    id 104
    label "instalowanie"
  ]
  node [
    id 105
    label "furkacja"
  ]
  node [
    id 106
    label "odinstalowa&#263;"
  ]
  node [
    id 107
    label "instalowa&#263;"
  ]
  node [
    id 108
    label "pirat"
  ]
  node [
    id 109
    label "zainstalowanie"
  ]
  node [
    id 110
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 111
    label "ogranicznik_referencyjny"
  ]
  node [
    id 112
    label "zainstalowa&#263;"
  ]
  node [
    id 113
    label "kana&#322;"
  ]
  node [
    id 114
    label "zaprezentowa&#263;"
  ]
  node [
    id 115
    label "interfejs"
  ]
  node [
    id 116
    label "odinstalowywa&#263;"
  ]
  node [
    id 117
    label "folder"
  ]
  node [
    id 118
    label "course_of_study"
  ]
  node [
    id 119
    label "ram&#243;wka"
  ]
  node [
    id 120
    label "prezentowanie"
  ]
  node [
    id 121
    label "okno"
  ]
  node [
    id 122
    label "visor"
  ]
  node [
    id 123
    label "twarz"
  ]
  node [
    id 124
    label "kosmetyk"
  ]
  node [
    id 125
    label "mask"
  ]
  node [
    id 126
    label "os&#322;ona"
  ]
  node [
    id 127
    label "gra"
  ]
  node [
    id 128
    label "mode"
  ]
  node [
    id 129
    label "aparat_tlenowy"
  ]
  node [
    id 130
    label "hood"
  ]
  node [
    id 131
    label "przesada"
  ]
  node [
    id 132
    label "nag&#322;owie"
  ]
  node [
    id 133
    label "pokrywa"
  ]
  node [
    id 134
    label "karoseria"
  ]
  node [
    id 135
    label "odlew"
  ]
  node [
    id 136
    label "narz&#281;dzie"
  ]
  node [
    id 137
    label "przebieraniec"
  ]
  node [
    id 138
    label "zas&#322;ona"
  ]
  node [
    id 139
    label "si&#281;ga&#263;"
  ]
  node [
    id 140
    label "trwa&#263;"
  ]
  node [
    id 141
    label "obecno&#347;&#263;"
  ]
  node [
    id 142
    label "stan"
  ]
  node [
    id 143
    label "stand"
  ]
  node [
    id 144
    label "mie&#263;_miejsce"
  ]
  node [
    id 145
    label "uczestniczy&#263;"
  ]
  node [
    id 146
    label "chodzi&#263;"
  ]
  node [
    id 147
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 148
    label "equal"
  ]
  node [
    id 149
    label "sklarowanie"
  ]
  node [
    id 150
    label "cnotliwy"
  ]
  node [
    id 151
    label "umycie"
  ]
  node [
    id 152
    label "wolny"
  ]
  node [
    id 153
    label "klarowny"
  ]
  node [
    id 154
    label "czysto"
  ]
  node [
    id 155
    label "prawdziwy"
  ]
  node [
    id 156
    label "ca&#322;y"
  ]
  node [
    id 157
    label "bezpieczny"
  ]
  node [
    id 158
    label "klarowanie"
  ]
  node [
    id 159
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 160
    label "do_czysta"
  ]
  node [
    id 161
    label "bezchmurny"
  ]
  node [
    id 162
    label "wspinaczka"
  ]
  node [
    id 163
    label "przyjemny"
  ]
  node [
    id 164
    label "udany"
  ]
  node [
    id 165
    label "zdrowy"
  ]
  node [
    id 166
    label "mycie"
  ]
  node [
    id 167
    label "klarowanie_si&#281;"
  ]
  node [
    id 168
    label "schludny"
  ]
  node [
    id 169
    label "orze&#378;wiaj&#261;cy"
  ]
  node [
    id 170
    label "ostry"
  ]
  node [
    id 171
    label "nieemisyjny"
  ]
  node [
    id 172
    label "ewidentny"
  ]
  node [
    id 173
    label "dopuszczalny"
  ]
  node [
    id 174
    label "jednolity"
  ]
  node [
    id 175
    label "wyczyszczenie_si&#281;"
  ]
  node [
    id 176
    label "legalny"
  ]
  node [
    id 177
    label "doskona&#322;y"
  ]
  node [
    id 178
    label "wyklarowanie_si&#281;"
  ]
  node [
    id 179
    label "prze&#378;roczy"
  ]
  node [
    id 180
    label "przezroczy&#347;cie"
  ]
  node [
    id 181
    label "bezb&#322;&#281;dny"
  ]
  node [
    id 182
    label "kompletny"
  ]
  node [
    id 183
    label "uczciwy"
  ]
  node [
    id 184
    label "czyszczenie_si&#281;"
  ]
  node [
    id 185
    label "pewny"
  ]
  node [
    id 186
    label "porz&#261;dny"
  ]
  node [
    id 187
    label "dobry"
  ]
  node [
    id 188
    label "nieodrodny"
  ]
  node [
    id 189
    label "ekologiczny"
  ]
  node [
    id 190
    label "Debian"
  ]
  node [
    id 191
    label "Testing"
  ]
  node [
    id 192
    label "Minta"
  ]
  node [
    id 193
    label "10"
  ]
  node [
    id 194
    label "Linux"
  ]
  node [
    id 195
    label "Mint"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 12
  ]
  edge [
    source 5
    target 13
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 38
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 10
    target 125
  ]
  edge [
    source 10
    target 126
  ]
  edge [
    source 10
    target 127
  ]
  edge [
    source 10
    target 128
  ]
  edge [
    source 10
    target 129
  ]
  edge [
    source 10
    target 130
  ]
  edge [
    source 10
    target 131
  ]
  edge [
    source 10
    target 132
  ]
  edge [
    source 10
    target 133
  ]
  edge [
    source 10
    target 134
  ]
  edge [
    source 10
    target 135
  ]
  edge [
    source 10
    target 136
  ]
  edge [
    source 10
    target 137
  ]
  edge [
    source 10
    target 138
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 139
  ]
  edge [
    source 11
    target 140
  ]
  edge [
    source 11
    target 141
  ]
  edge [
    source 11
    target 142
  ]
  edge [
    source 11
    target 17
  ]
  edge [
    source 11
    target 143
  ]
  edge [
    source 11
    target 144
  ]
  edge [
    source 11
    target 145
  ]
  edge [
    source 11
    target 146
  ]
  edge [
    source 11
    target 147
  ]
  edge [
    source 11
    target 148
  ]
  edge [
    source 12
    target 149
  ]
  edge [
    source 12
    target 150
  ]
  edge [
    source 12
    target 151
  ]
  edge [
    source 12
    target 152
  ]
  edge [
    source 12
    target 153
  ]
  edge [
    source 12
    target 154
  ]
  edge [
    source 12
    target 155
  ]
  edge [
    source 12
    target 156
  ]
  edge [
    source 12
    target 157
  ]
  edge [
    source 12
    target 158
  ]
  edge [
    source 12
    target 159
  ]
  edge [
    source 12
    target 160
  ]
  edge [
    source 12
    target 161
  ]
  edge [
    source 12
    target 162
  ]
  edge [
    source 12
    target 163
  ]
  edge [
    source 12
    target 164
  ]
  edge [
    source 12
    target 165
  ]
  edge [
    source 12
    target 166
  ]
  edge [
    source 12
    target 167
  ]
  edge [
    source 12
    target 168
  ]
  edge [
    source 12
    target 169
  ]
  edge [
    source 12
    target 170
  ]
  edge [
    source 12
    target 171
  ]
  edge [
    source 12
    target 172
  ]
  edge [
    source 12
    target 173
  ]
  edge [
    source 12
    target 174
  ]
  edge [
    source 12
    target 175
  ]
  edge [
    source 12
    target 176
  ]
  edge [
    source 12
    target 177
  ]
  edge [
    source 12
    target 178
  ]
  edge [
    source 12
    target 179
  ]
  edge [
    source 12
    target 180
  ]
  edge [
    source 12
    target 181
  ]
  edge [
    source 12
    target 182
  ]
  edge [
    source 12
    target 183
  ]
  edge [
    source 12
    target 184
  ]
  edge [
    source 12
    target 185
  ]
  edge [
    source 12
    target 186
  ]
  edge [
    source 12
    target 187
  ]
  edge [
    source 12
    target 188
  ]
  edge [
    source 12
    target 189
  ]
  edge [
    source 190
    target 191
  ]
  edge [
    source 192
    target 193
  ]
  edge [
    source 194
    target 195
  ]
]
