graph [
  maxDegree 40
  minDegree 1
  meanDegree 2.073298429319372
  density 0.010912096996417747
  graphCliqueNumber 3
  node [
    id 0
    label "doby&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "szpada"
    origin "text"
  ]
  node [
    id 2
    label "jak"
    origin "text"
  ]
  node [
    id 3
    label "fechmistrz"
    origin "text"
  ]
  node [
    id 4
    label "anglia"
    origin "text"
  ]
  node [
    id 5
    label "dziewczyna"
    origin "text"
  ]
  node [
    id 6
    label "poda&#263;"
    origin "text"
  ]
  node [
    id 7
    label "s&#322;omka"
    origin "text"
  ]
  node [
    id 8
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 9
    label "robi&#263;by&#263;"
    origin "text"
  ]
  node [
    id 10
    label "pika"
    origin "text"
  ]
  node [
    id 11
    label "si&#281;"
    origin "text"
  ]
  node [
    id 12
    label "ten"
    origin "text"
  ]
  node [
    id 13
    label "sztuka"
    origin "text"
  ]
  node [
    id 14
    label "m&#322;odo&#347;&#263;"
    origin "text"
  ]
  node [
    id 15
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 16
    label "by&#263;by&#263;"
    origin "text"
  ]
  node [
    id 17
    label "pokazywa&#263;"
    origin "text"
  ]
  node [
    id 18
    label "dwana&#347;cie"
    origin "text"
  ]
  node [
    id 19
    label "raz"
    origin "text"
  ]
  node [
    id 20
    label "zawsze"
    origin "text"
  ]
  node [
    id 21
    label "to&#380;"
    origin "text"
  ]
  node [
    id 22
    label "sam"
    origin "text"
  ]
  node [
    id 23
    label "powtarza&#263;"
    origin "text"
  ]
  node [
    id 24
    label "prawie"
    origin "text"
  ]
  node [
    id 25
    label "kona&#263;by&#263;"
    origin "text"
  ]
  node [
    id 26
    label "trud"
    origin "text"
  ]
  node [
    id 27
    label "przykro&#347;&#263;"
    origin "text"
  ]
  node [
    id 28
    label "nudno&#347;ci"
    origin "text"
  ]
  node [
    id 29
    label "bro&#324;_osobista"
  ]
  node [
    id 30
    label "bro&#324;_sportowa"
  ]
  node [
    id 31
    label "bro&#324;"
  ]
  node [
    id 32
    label "punta"
  ]
  node [
    id 33
    label "sword"
  ]
  node [
    id 34
    label "szermierka"
  ]
  node [
    id 35
    label "byd&#322;o"
  ]
  node [
    id 36
    label "zobo"
  ]
  node [
    id 37
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 38
    label "yakalo"
  ]
  node [
    id 39
    label "dzo"
  ]
  node [
    id 40
    label "instruktor"
  ]
  node [
    id 41
    label "sportowiec"
  ]
  node [
    id 42
    label "szermierz"
  ]
  node [
    id 43
    label "cz&#322;owiek"
  ]
  node [
    id 44
    label "dziewka"
  ]
  node [
    id 45
    label "dziewoja"
  ]
  node [
    id 46
    label "dziunia"
  ]
  node [
    id 47
    label "partnerka"
  ]
  node [
    id 48
    label "dziewczynina"
  ]
  node [
    id 49
    label "siksa"
  ]
  node [
    id 50
    label "sympatia"
  ]
  node [
    id 51
    label "dziewcz&#281;"
  ]
  node [
    id 52
    label "s&#322;u&#380;&#261;ca"
  ]
  node [
    id 53
    label "kora"
  ]
  node [
    id 54
    label "m&#322;&#243;dka"
  ]
  node [
    id 55
    label "dziecina"
  ]
  node [
    id 56
    label "sikorka"
  ]
  node [
    id 57
    label "tenis"
  ]
  node [
    id 58
    label "da&#263;"
  ]
  node [
    id 59
    label "siatk&#243;wka"
  ]
  node [
    id 60
    label "introduce"
  ]
  node [
    id 61
    label "jedzenie"
  ]
  node [
    id 62
    label "zaserwowa&#263;"
  ]
  node [
    id 63
    label "give"
  ]
  node [
    id 64
    label "ustawi&#263;"
  ]
  node [
    id 65
    label "zagra&#263;"
  ]
  node [
    id 66
    label "supply"
  ]
  node [
    id 67
    label "nafaszerowa&#263;"
  ]
  node [
    id 68
    label "poinformowa&#263;"
  ]
  node [
    id 69
    label "straw"
  ]
  node [
    id 70
    label "&#378;d&#378;b&#322;o"
  ]
  node [
    id 71
    label "rurka"
  ]
  node [
    id 72
    label "ratyszcze"
  ]
  node [
    id 73
    label "bawe&#322;na"
  ]
  node [
    id 74
    label "grot"
  ]
  node [
    id 75
    label "bro&#324;_drzewcowa"
  ]
  node [
    id 76
    label "lot"
  ]
  node [
    id 77
    label "okre&#347;lony"
  ]
  node [
    id 78
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 79
    label "theatrical_performance"
  ]
  node [
    id 80
    label "sprawno&#347;&#263;"
  ]
  node [
    id 81
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 82
    label "ods&#322;ona"
  ]
  node [
    id 83
    label "przedmiot"
  ]
  node [
    id 84
    label "Faust"
  ]
  node [
    id 85
    label "jednostka"
  ]
  node [
    id 86
    label "fortel"
  ]
  node [
    id 87
    label "environment"
  ]
  node [
    id 88
    label "rola"
  ]
  node [
    id 89
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 90
    label "utw&#243;r"
  ]
  node [
    id 91
    label "egzemplarz"
  ]
  node [
    id 92
    label "realizacja"
  ]
  node [
    id 93
    label "turn"
  ]
  node [
    id 94
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 95
    label "scenografia"
  ]
  node [
    id 96
    label "kultura_duchowa"
  ]
  node [
    id 97
    label "ilo&#347;&#263;"
  ]
  node [
    id 98
    label "pokaz"
  ]
  node [
    id 99
    label "przedstawia&#263;"
  ]
  node [
    id 100
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 101
    label "pr&#243;bowanie"
  ]
  node [
    id 102
    label "kobieta"
  ]
  node [
    id 103
    label "scena"
  ]
  node [
    id 104
    label "przedstawianie"
  ]
  node [
    id 105
    label "scenariusz"
  ]
  node [
    id 106
    label "didaskalia"
  ]
  node [
    id 107
    label "Apollo"
  ]
  node [
    id 108
    label "czyn"
  ]
  node [
    id 109
    label "przedstawienie"
  ]
  node [
    id 110
    label "head"
  ]
  node [
    id 111
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 112
    label "przedstawi&#263;"
  ]
  node [
    id 113
    label "ambala&#380;"
  ]
  node [
    id 114
    label "kultura"
  ]
  node [
    id 115
    label "towar"
  ]
  node [
    id 116
    label "m&#322;odzie&#324;czo&#347;&#263;"
  ]
  node [
    id 117
    label "adolescence"
  ]
  node [
    id 118
    label "wiek"
  ]
  node [
    id 119
    label "szczeni&#281;ce_lata"
  ]
  node [
    id 120
    label "&#380;ycie"
  ]
  node [
    id 121
    label "zielone_lata"
  ]
  node [
    id 122
    label "s&#322;o&#324;ce"
  ]
  node [
    id 123
    label "czynienie_si&#281;"
  ]
  node [
    id 124
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 125
    label "czas"
  ]
  node [
    id 126
    label "long_time"
  ]
  node [
    id 127
    label "przedpo&#322;udnie"
  ]
  node [
    id 128
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 129
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 130
    label "tydzie&#324;"
  ]
  node [
    id 131
    label "godzina"
  ]
  node [
    id 132
    label "t&#322;usty_czwartek"
  ]
  node [
    id 133
    label "wsta&#263;"
  ]
  node [
    id 134
    label "day"
  ]
  node [
    id 135
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 136
    label "przedwiecz&#243;r"
  ]
  node [
    id 137
    label "Sylwester"
  ]
  node [
    id 138
    label "po&#322;udnie"
  ]
  node [
    id 139
    label "wzej&#347;cie"
  ]
  node [
    id 140
    label "podwiecz&#243;r"
  ]
  node [
    id 141
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 142
    label "rano"
  ]
  node [
    id 143
    label "termin"
  ]
  node [
    id 144
    label "ranek"
  ]
  node [
    id 145
    label "doba"
  ]
  node [
    id 146
    label "wiecz&#243;r"
  ]
  node [
    id 147
    label "walentynki"
  ]
  node [
    id 148
    label "popo&#322;udnie"
  ]
  node [
    id 149
    label "noc"
  ]
  node [
    id 150
    label "wstanie"
  ]
  node [
    id 151
    label "przeszkala&#263;"
  ]
  node [
    id 152
    label "by&#263;"
  ]
  node [
    id 153
    label "warto&#347;&#263;"
  ]
  node [
    id 154
    label "informowa&#263;"
  ]
  node [
    id 155
    label "bespeak"
  ]
  node [
    id 156
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 157
    label "represent"
  ]
  node [
    id 158
    label "indicate"
  ]
  node [
    id 159
    label "wyraz"
  ]
  node [
    id 160
    label "wyra&#380;a&#263;"
  ]
  node [
    id 161
    label "exhibit"
  ]
  node [
    id 162
    label "powodowa&#263;"
  ]
  node [
    id 163
    label "podawa&#263;"
  ]
  node [
    id 164
    label "exsert"
  ]
  node [
    id 165
    label "chwila"
  ]
  node [
    id 166
    label "uderzenie"
  ]
  node [
    id 167
    label "cios"
  ]
  node [
    id 168
    label "time"
  ]
  node [
    id 169
    label "zaw&#380;dy"
  ]
  node [
    id 170
    label "ci&#261;gle"
  ]
  node [
    id 171
    label "na_zawsze"
  ]
  node [
    id 172
    label "cz&#281;sto"
  ]
  node [
    id 173
    label "sklep"
  ]
  node [
    id 174
    label "impart"
  ]
  node [
    id 175
    label "repeat"
  ]
  node [
    id 176
    label "robi&#263;"
  ]
  node [
    id 177
    label "podszkala&#263;_si&#281;"
  ]
  node [
    id 178
    label "zach&#243;d"
  ]
  node [
    id 179
    label "trudzenie"
  ]
  node [
    id 180
    label "przytoczenie_si&#281;"
  ]
  node [
    id 181
    label "trudzi&#263;"
  ]
  node [
    id 182
    label "cecha"
  ]
  node [
    id 183
    label "wleczenie_si&#281;"
  ]
  node [
    id 184
    label "przytaczanie_si&#281;"
  ]
  node [
    id 185
    label "wlec_si&#281;"
  ]
  node [
    id 186
    label "disagreeableness"
  ]
  node [
    id 187
    label "prze&#380;ycie"
  ]
  node [
    id 188
    label "doznanie"
  ]
  node [
    id 189
    label "mdlenie"
  ]
  node [
    id 190
    label "oznaka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 10
    target 31
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 77
  ]
  edge [
    source 12
    target 78
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 79
  ]
  edge [
    source 13
    target 43
  ]
  edge [
    source 13
    target 80
  ]
  edge [
    source 13
    target 81
  ]
  edge [
    source 13
    target 82
  ]
  edge [
    source 13
    target 83
  ]
  edge [
    source 13
    target 84
  ]
  edge [
    source 13
    target 85
  ]
  edge [
    source 13
    target 86
  ]
  edge [
    source 13
    target 87
  ]
  edge [
    source 13
    target 88
  ]
  edge [
    source 13
    target 89
  ]
  edge [
    source 13
    target 90
  ]
  edge [
    source 13
    target 91
  ]
  edge [
    source 13
    target 92
  ]
  edge [
    source 13
    target 93
  ]
  edge [
    source 13
    target 94
  ]
  edge [
    source 13
    target 95
  ]
  edge [
    source 13
    target 96
  ]
  edge [
    source 13
    target 97
  ]
  edge [
    source 13
    target 98
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 13
    target 101
  ]
  edge [
    source 13
    target 102
  ]
  edge [
    source 13
    target 103
  ]
  edge [
    source 13
    target 104
  ]
  edge [
    source 13
    target 105
  ]
  edge [
    source 13
    target 106
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 13
    target 113
  ]
  edge [
    source 13
    target 114
  ]
  edge [
    source 13
    target 115
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 116
  ]
  edge [
    source 14
    target 117
  ]
  edge [
    source 14
    target 118
  ]
  edge [
    source 14
    target 119
  ]
  edge [
    source 14
    target 120
  ]
  edge [
    source 14
    target 121
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 122
  ]
  edge [
    source 15
    target 123
  ]
  edge [
    source 15
    target 124
  ]
  edge [
    source 15
    target 125
  ]
  edge [
    source 15
    target 126
  ]
  edge [
    source 15
    target 127
  ]
  edge [
    source 15
    target 128
  ]
  edge [
    source 15
    target 129
  ]
  edge [
    source 15
    target 130
  ]
  edge [
    source 15
    target 131
  ]
  edge [
    source 15
    target 132
  ]
  edge [
    source 15
    target 133
  ]
  edge [
    source 15
    target 134
  ]
  edge [
    source 15
    target 135
  ]
  edge [
    source 15
    target 136
  ]
  edge [
    source 15
    target 137
  ]
  edge [
    source 15
    target 138
  ]
  edge [
    source 15
    target 139
  ]
  edge [
    source 15
    target 140
  ]
  edge [
    source 15
    target 141
  ]
  edge [
    source 15
    target 142
  ]
  edge [
    source 15
    target 143
  ]
  edge [
    source 15
    target 144
  ]
  edge [
    source 15
    target 145
  ]
  edge [
    source 15
    target 146
  ]
  edge [
    source 15
    target 147
  ]
  edge [
    source 15
    target 148
  ]
  edge [
    source 15
    target 149
  ]
  edge [
    source 15
    target 150
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 151
  ]
  edge [
    source 17
    target 152
  ]
  edge [
    source 17
    target 153
  ]
  edge [
    source 17
    target 154
  ]
  edge [
    source 17
    target 60
  ]
  edge [
    source 17
    target 155
  ]
  edge [
    source 17
    target 156
  ]
  edge [
    source 17
    target 157
  ]
  edge [
    source 17
    target 158
  ]
  edge [
    source 17
    target 159
  ]
  edge [
    source 17
    target 160
  ]
  edge [
    source 17
    target 161
  ]
  edge [
    source 17
    target 162
  ]
  edge [
    source 17
    target 163
  ]
  edge [
    source 17
    target 164
  ]
  edge [
    source 17
    target 99
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 165
  ]
  edge [
    source 19
    target 166
  ]
  edge [
    source 19
    target 167
  ]
  edge [
    source 19
    target 168
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 169
  ]
  edge [
    source 20
    target 170
  ]
  edge [
    source 20
    target 171
  ]
  edge [
    source 20
    target 172
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 173
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 174
  ]
  edge [
    source 23
    target 175
  ]
  edge [
    source 23
    target 176
  ]
  edge [
    source 23
    target 177
  ]
  edge [
    source 23
    target 163
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 178
  ]
  edge [
    source 26
    target 179
  ]
  edge [
    source 26
    target 180
  ]
  edge [
    source 26
    target 181
  ]
  edge [
    source 26
    target 182
  ]
  edge [
    source 26
    target 183
  ]
  edge [
    source 26
    target 184
  ]
  edge [
    source 26
    target 185
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 186
  ]
  edge [
    source 27
    target 187
  ]
  edge [
    source 27
    target 188
  ]
  edge [
    source 28
    target 189
  ]
  edge [
    source 28
    target 190
  ]
  edge [
    source 28
    target 188
  ]
]
