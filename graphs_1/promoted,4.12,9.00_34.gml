graph [
  maxDegree 28
  minDegree 1
  meanDegree 2.0846560846560847
  density 0.011088596194979174
  graphCliqueNumber 4
  node [
    id 0
    label "s&#261;d"
    origin "text"
  ]
  node [
    id 1
    label "najwy&#380;szy"
    origin "text"
  ]
  node [
    id 2
    label "zgodzi&#263;"
    origin "text"
  ]
  node [
    id 3
    label "si&#281;"
    origin "text"
  ]
  node [
    id 4
    label "aresztowanie"
    origin "text"
  ]
  node [
    id 5
    label "wypu&#347;ci&#263;"
    origin "text"
  ]
  node [
    id 6
    label "wolno&#347;&#263;"
    origin "text"
  ]
  node [
    id 7
    label "podpu&#322;kownik"
    origin "text"
  ]
  node [
    id 8
    label "s&#322;awomir"
    origin "text"
  ]
  node [
    id 9
    label "sekunda"
    origin "text"
  ]
  node [
    id 10
    label "podejrzany"
    origin "text"
  ]
  node [
    id 11
    label "wy&#322;udzenie"
    origin "text"
  ]
  node [
    id 12
    label "wojskowy"
    origin "text"
  ]
  node [
    id 13
    label "instytut"
    origin "text"
  ]
  node [
    id 14
    label "techniczny"
    origin "text"
  ]
  node [
    id 15
    label "uzbrojenie"
    origin "text"
  ]
  node [
    id 16
    label "gdzie"
    origin "text"
  ]
  node [
    id 17
    label "by&#263;"
    origin "text"
  ]
  node [
    id 18
    label "kierownik"
    origin "text"
  ]
  node [
    id 19
    label "ma&#322;o"
    origin "text"
  ]
  node [
    id 20
    label "milion"
    origin "text"
  ]
  node [
    id 21
    label "z&#322;oty"
    origin "text"
  ]
  node [
    id 22
    label "procesowicz"
  ]
  node [
    id 23
    label "wypowied&#378;"
  ]
  node [
    id 24
    label "pods&#261;dny"
  ]
  node [
    id 25
    label "broni&#263;"
  ]
  node [
    id 26
    label "bronienie"
  ]
  node [
    id 27
    label "system"
  ]
  node [
    id 28
    label "my&#347;l"
  ]
  node [
    id 29
    label "wytw&#243;r"
  ]
  node [
    id 30
    label "urz&#261;d"
  ]
  node [
    id 31
    label "konektyw"
  ]
  node [
    id 32
    label "court"
  ]
  node [
    id 33
    label "obrona"
  ]
  node [
    id 34
    label "s&#261;downictwo"
  ]
  node [
    id 35
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 36
    label "forum"
  ]
  node [
    id 37
    label "zesp&#243;&#322;"
  ]
  node [
    id 38
    label "post&#281;powanie"
  ]
  node [
    id 39
    label "skazany"
  ]
  node [
    id 40
    label "wydarzenie"
  ]
  node [
    id 41
    label "&#347;wiadek"
  ]
  node [
    id 42
    label "antylogizm"
  ]
  node [
    id 43
    label "strona"
  ]
  node [
    id 44
    label "oskar&#380;yciel"
  ]
  node [
    id 45
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 46
    label "biuro"
  ]
  node [
    id 47
    label "instytucja"
  ]
  node [
    id 48
    label "zatrudni&#263;"
  ]
  node [
    id 49
    label "zgodzenie"
  ]
  node [
    id 50
    label "pomoc"
  ]
  node [
    id 51
    label "zgadza&#263;"
  ]
  node [
    id 52
    label "czynno&#347;&#263;"
  ]
  node [
    id 53
    label "spowodowanie"
  ]
  node [
    id 54
    label "zajmowanie"
  ]
  node [
    id 55
    label "z&#322;apanie"
  ]
  node [
    id 56
    label "check"
  ]
  node [
    id 57
    label "imprisonment"
  ]
  node [
    id 58
    label "powodowanie"
  ]
  node [
    id 59
    label "&#322;apanie"
  ]
  node [
    id 60
    label "zaj&#281;cie"
  ]
  node [
    id 61
    label "rynek"
  ]
  node [
    id 62
    label "pu&#347;ci&#263;"
  ]
  node [
    id 63
    label "wykszta&#322;ci&#263;"
  ]
  node [
    id 64
    label "zej&#347;&#263;"
  ]
  node [
    id 65
    label "issue"
  ]
  node [
    id 66
    label "pozwoli&#263;"
  ]
  node [
    id 67
    label "wyda&#263;"
  ]
  node [
    id 68
    label "leave"
  ]
  node [
    id 69
    label "release"
  ]
  node [
    id 70
    label "przesta&#263;"
  ]
  node [
    id 71
    label "picture"
  ]
  node [
    id 72
    label "zwolni&#263;"
  ]
  node [
    id 73
    label "zrobi&#263;"
  ]
  node [
    id 74
    label "publish"
  ]
  node [
    id 75
    label "absolutno&#347;&#263;"
  ]
  node [
    id 76
    label "obecno&#347;&#263;"
  ]
  node [
    id 77
    label "uwi&#281;zi&#263;"
  ]
  node [
    id 78
    label "cecha"
  ]
  node [
    id 79
    label "freedom"
  ]
  node [
    id 80
    label "niezale&#380;no&#347;&#263;"
  ]
  node [
    id 81
    label "uwi&#281;zienie"
  ]
  node [
    id 82
    label "nieokre&#347;lono&#347;&#263;"
  ]
  node [
    id 83
    label "oficer"
  ]
  node [
    id 84
    label "stopie&#324;_wojskowy"
  ]
  node [
    id 85
    label "minuta"
  ]
  node [
    id 86
    label "tercja"
  ]
  node [
    id 87
    label "milisekunda"
  ]
  node [
    id 88
    label "nanosekunda"
  ]
  node [
    id 89
    label "uk&#322;ad_SI"
  ]
  node [
    id 90
    label "mikrosekunda"
  ]
  node [
    id 91
    label "time"
  ]
  node [
    id 92
    label "jednostka_czasu"
  ]
  node [
    id 93
    label "jednostka"
  ]
  node [
    id 94
    label "nieprzejrzysty"
  ]
  node [
    id 95
    label "pos&#261;dzanie"
  ]
  node [
    id 96
    label "podejrzanie"
  ]
  node [
    id 97
    label "podmiot"
  ]
  node [
    id 98
    label "niepewny"
  ]
  node [
    id 99
    label "z&#322;y"
  ]
  node [
    id 100
    label "wycyganienie"
  ]
  node [
    id 101
    label "przest&#281;pstwo"
  ]
  node [
    id 102
    label "extortion"
  ]
  node [
    id 103
    label "nabranie"
  ]
  node [
    id 104
    label "pozyskanie"
  ]
  node [
    id 105
    label "cz&#322;owiek"
  ]
  node [
    id 106
    label "rota"
  ]
  node [
    id 107
    label "zdemobilizowanie"
  ]
  node [
    id 108
    label "militarnie"
  ]
  node [
    id 109
    label "s&#322;u&#380;y&#263;_w_wojsku"
  ]
  node [
    id 110
    label "Gurkha"
  ]
  node [
    id 111
    label "demobilizowanie"
  ]
  node [
    id 112
    label "walcz&#261;cy"
  ]
  node [
    id 113
    label "harcap"
  ]
  node [
    id 114
    label "&#380;o&#322;dowy"
  ]
  node [
    id 115
    label "mundurowy"
  ]
  node [
    id 116
    label "s&#322;u&#380;enie_w_wojsku"
  ]
  node [
    id 117
    label "zdemobilizowa&#263;"
  ]
  node [
    id 118
    label "typowy"
  ]
  node [
    id 119
    label "antybalistyczny"
  ]
  node [
    id 120
    label "specjalny"
  ]
  node [
    id 121
    label "podleg&#322;y"
  ]
  node [
    id 122
    label "&#380;o&#322;nierstwo"
  ]
  node [
    id 123
    label "elew"
  ]
  node [
    id 124
    label "so&#322;dat"
  ]
  node [
    id 125
    label "wojsko"
  ]
  node [
    id 126
    label "wojskowo"
  ]
  node [
    id 127
    label "demobilizowa&#263;"
  ]
  node [
    id 128
    label "plac&#243;wka"
  ]
  node [
    id 129
    label "institute"
  ]
  node [
    id 130
    label "&#379;ydowski_Instytut_Historyczny"
  ]
  node [
    id 131
    label "Ossolineum"
  ]
  node [
    id 132
    label "plac&#243;wka_naukowa"
  ]
  node [
    id 133
    label "nieznaczny"
  ]
  node [
    id 134
    label "suchy"
  ]
  node [
    id 135
    label "pozamerytoryczny"
  ]
  node [
    id 136
    label "ambitny"
  ]
  node [
    id 137
    label "technicznie"
  ]
  node [
    id 138
    label "amunicja"
  ]
  node [
    id 139
    label "rozbroi&#263;"
  ]
  node [
    id 140
    label "tackle"
  ]
  node [
    id 141
    label "zainstalowanie"
  ]
  node [
    id 142
    label "przyrz&#261;d"
  ]
  node [
    id 143
    label "osprz&#281;t"
  ]
  node [
    id 144
    label "instalacja"
  ]
  node [
    id 145
    label "munition"
  ]
  node [
    id 146
    label "wyposa&#380;enie"
  ]
  node [
    id 147
    label "twornik"
  ]
  node [
    id 148
    label "or&#281;&#380;"
  ]
  node [
    id 149
    label "rozbraja&#263;"
  ]
  node [
    id 150
    label "rozbrajanie"
  ]
  node [
    id 151
    label "rozbrojenie"
  ]
  node [
    id 152
    label "si&#281;ga&#263;"
  ]
  node [
    id 153
    label "trwa&#263;"
  ]
  node [
    id 154
    label "stan"
  ]
  node [
    id 155
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 156
    label "stand"
  ]
  node [
    id 157
    label "mie&#263;_miejsce"
  ]
  node [
    id 158
    label "uczestniczy&#263;"
  ]
  node [
    id 159
    label "chodzi&#263;"
  ]
  node [
    id 160
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 161
    label "equal"
  ]
  node [
    id 162
    label "zwierzchnik"
  ]
  node [
    id 163
    label "mo&#380;liwie"
  ]
  node [
    id 164
    label "kr&#243;tko"
  ]
  node [
    id 165
    label "nieistotnie"
  ]
  node [
    id 166
    label "nieliczny"
  ]
  node [
    id 167
    label "mikroskopijnie"
  ]
  node [
    id 168
    label "pomiernie"
  ]
  node [
    id 169
    label "ma&#322;y"
  ]
  node [
    id 170
    label "liczba"
  ]
  node [
    id 171
    label "miljon"
  ]
  node [
    id 172
    label "ba&#324;ka"
  ]
  node [
    id 173
    label "szlachetny"
  ]
  node [
    id 174
    label "metaliczny"
  ]
  node [
    id 175
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 176
    label "z&#322;ocenie"
  ]
  node [
    id 177
    label "grosz"
  ]
  node [
    id 178
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 179
    label "utytu&#322;owany"
  ]
  node [
    id 180
    label "poz&#322;ocenie"
  ]
  node [
    id 181
    label "Polska"
  ]
  node [
    id 182
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 183
    label "wspania&#322;y"
  ]
  node [
    id 184
    label "doskona&#322;y"
  ]
  node [
    id 185
    label "kochany"
  ]
  node [
    id 186
    label "jednostka_monetarna"
  ]
  node [
    id 187
    label "z&#322;oci&#347;cie"
  ]
  node [
    id 188
    label "S&#322;awomir"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 188
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 188
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 100
  ]
  edge [
    source 11
    target 101
  ]
  edge [
    source 11
    target 102
  ]
  edge [
    source 11
    target 103
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 12
    target 107
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 12
    target 109
  ]
  edge [
    source 12
    target 110
  ]
  edge [
    source 12
    target 111
  ]
  edge [
    source 12
    target 112
  ]
  edge [
    source 12
    target 113
  ]
  edge [
    source 12
    target 114
  ]
  edge [
    source 12
    target 115
  ]
  edge [
    source 12
    target 116
  ]
  edge [
    source 12
    target 117
  ]
  edge [
    source 12
    target 118
  ]
  edge [
    source 12
    target 119
  ]
  edge [
    source 12
    target 120
  ]
  edge [
    source 12
    target 121
  ]
  edge [
    source 12
    target 122
  ]
  edge [
    source 12
    target 123
  ]
  edge [
    source 12
    target 124
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 12
    target 15
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 128
  ]
  edge [
    source 13
    target 129
  ]
  edge [
    source 13
    target 130
  ]
  edge [
    source 13
    target 131
  ]
  edge [
    source 13
    target 47
  ]
  edge [
    source 13
    target 132
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 120
  ]
  edge [
    source 14
    target 133
  ]
  edge [
    source 14
    target 134
  ]
  edge [
    source 14
    target 135
  ]
  edge [
    source 14
    target 136
  ]
  edge [
    source 14
    target 137
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 138
  ]
  edge [
    source 15
    target 139
  ]
  edge [
    source 15
    target 140
  ]
  edge [
    source 15
    target 141
  ]
  edge [
    source 15
    target 142
  ]
  edge [
    source 15
    target 143
  ]
  edge [
    source 15
    target 144
  ]
  edge [
    source 15
    target 145
  ]
  edge [
    source 15
    target 146
  ]
  edge [
    source 15
    target 147
  ]
  edge [
    source 15
    target 148
  ]
  edge [
    source 15
    target 149
  ]
  edge [
    source 15
    target 150
  ]
  edge [
    source 15
    target 151
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 152
  ]
  edge [
    source 17
    target 153
  ]
  edge [
    source 17
    target 76
  ]
  edge [
    source 17
    target 154
  ]
  edge [
    source 17
    target 155
  ]
  edge [
    source 17
    target 156
  ]
  edge [
    source 17
    target 157
  ]
  edge [
    source 17
    target 158
  ]
  edge [
    source 17
    target 159
  ]
  edge [
    source 17
    target 160
  ]
  edge [
    source 17
    target 161
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 162
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 163
  ]
  edge [
    source 19
    target 133
  ]
  edge [
    source 19
    target 164
  ]
  edge [
    source 19
    target 165
  ]
  edge [
    source 19
    target 166
  ]
  edge [
    source 19
    target 167
  ]
  edge [
    source 19
    target 168
  ]
  edge [
    source 19
    target 169
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 170
  ]
  edge [
    source 20
    target 171
  ]
  edge [
    source 20
    target 172
  ]
  edge [
    source 21
    target 173
  ]
  edge [
    source 21
    target 174
  ]
  edge [
    source 21
    target 175
  ]
  edge [
    source 21
    target 176
  ]
  edge [
    source 21
    target 177
  ]
  edge [
    source 21
    target 178
  ]
  edge [
    source 21
    target 179
  ]
  edge [
    source 21
    target 180
  ]
  edge [
    source 21
    target 181
  ]
  edge [
    source 21
    target 182
  ]
  edge [
    source 21
    target 183
  ]
  edge [
    source 21
    target 184
  ]
  edge [
    source 21
    target 185
  ]
  edge [
    source 21
    target 186
  ]
  edge [
    source 21
    target 187
  ]
]
