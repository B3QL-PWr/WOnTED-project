graph [
  maxDegree 14
  minDegree 1
  meanDegree 1.8888888888888888
  density 0.1111111111111111
  graphCliqueNumber 2
  node [
    id 0
    label "dzwonek"
    origin "text"
  ]
  node [
    id 1
    label "teatralny"
    origin "text"
  ]
  node [
    id 2
    label "ro&#347;lina_zielna"
  ]
  node [
    id 3
    label "karo"
  ]
  node [
    id 4
    label "dzwonkowate"
  ]
  node [
    id 5
    label "campanula"
  ]
  node [
    id 6
    label "przycisk"
  ]
  node [
    id 7
    label "&#322;&#261;cznik_instalacyjny"
  ]
  node [
    id 8
    label "dzwoni&#263;"
  ]
  node [
    id 9
    label "sygna&#322;_d&#378;wi&#281;kowy"
  ]
  node [
    id 10
    label "dzwonienie"
  ]
  node [
    id 11
    label "zadzwoni&#263;"
  ]
  node [
    id 12
    label "karta"
  ]
  node [
    id 13
    label "sygnalizator"
  ]
  node [
    id 14
    label "kolor"
  ]
  node [
    id 15
    label "nadmierny"
  ]
  node [
    id 16
    label "nienaturalny"
  ]
  node [
    id 17
    label "teatralnie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
]
