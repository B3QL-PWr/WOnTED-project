graph [
  maxDegree 23
  minDegree 1
  meanDegree 1.9615384615384615
  density 0.038461538461538464
  graphCliqueNumber 2
  node [
    id 0
    label "dziennik"
    origin "text"
  ]
  node [
    id 1
    label "urz&#281;dowy"
    origin "text"
  ]
  node [
    id 2
    label "minister"
    origin "text"
  ]
  node [
    id 3
    label "obrona"
    origin "text"
  ]
  node [
    id 4
    label "narodowy"
    origin "text"
  ]
  node [
    id 5
    label "czerwiec"
    origin "text"
  ]
  node [
    id 6
    label "poz"
    origin "text"
  ]
  node [
    id 7
    label "spis"
  ]
  node [
    id 8
    label "sheet"
  ]
  node [
    id 9
    label "gazeta"
  ]
  node [
    id 10
    label "diariusz"
  ]
  node [
    id 11
    label "pami&#281;tnik"
  ]
  node [
    id 12
    label "journal"
  ]
  node [
    id 13
    label "ksi&#281;ga"
  ]
  node [
    id 14
    label "program_informacyjny"
  ]
  node [
    id 15
    label "urz&#281;dowo"
  ]
  node [
    id 16
    label "oficjalny"
  ]
  node [
    id 17
    label "formalny"
  ]
  node [
    id 18
    label "Goebbels"
  ]
  node [
    id 19
    label "Sto&#322;ypin"
  ]
  node [
    id 20
    label "rz&#261;d"
  ]
  node [
    id 21
    label "dostojnik"
  ]
  node [
    id 22
    label "manewr"
  ]
  node [
    id 23
    label "reakcja"
  ]
  node [
    id 24
    label "auspices"
  ]
  node [
    id 25
    label "mecz"
  ]
  node [
    id 26
    label "poparcie"
  ]
  node [
    id 27
    label "ochrona"
  ]
  node [
    id 28
    label "s&#261;d"
  ]
  node [
    id 29
    label "defensive_structure"
  ]
  node [
    id 30
    label "liga"
  ]
  node [
    id 31
    label "egzamin"
  ]
  node [
    id 32
    label "gracz"
  ]
  node [
    id 33
    label "defense"
  ]
  node [
    id 34
    label "walka"
  ]
  node [
    id 35
    label "post&#281;powanie"
  ]
  node [
    id 36
    label "wojsko"
  ]
  node [
    id 37
    label "protection"
  ]
  node [
    id 38
    label "poj&#281;cie"
  ]
  node [
    id 39
    label "guard_duty"
  ]
  node [
    id 40
    label "strona"
  ]
  node [
    id 41
    label "sp&#243;r"
  ]
  node [
    id 42
    label "gra"
  ]
  node [
    id 43
    label "nacjonalistyczny"
  ]
  node [
    id 44
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 45
    label "narodowo"
  ]
  node [
    id 46
    label "wa&#380;ny"
  ]
  node [
    id 47
    label "ro&#347;lina_zielna"
  ]
  node [
    id 48
    label "go&#378;dzikowate"
  ]
  node [
    id 49
    label "miesi&#261;c"
  ]
  node [
    id 50
    label "Bo&#380;e_Cia&#322;o"
  ]
  node [
    id 51
    label "&#347;wi&#281;ty_Jan"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
]
