graph [
  maxDegree 4
  minDegree 1
  meanDegree 1.8666666666666667
  density 0.04242424242424243
  graphCliqueNumber 4
  node [
    id 0
    label "rocznik"
    origin "text"
  ]
  node [
    id 1
    label "formacja"
  ]
  node [
    id 2
    label "kronika"
  ]
  node [
    id 3
    label "czasopismo"
  ]
  node [
    id 4
    label "yearbook"
  ]
  node [
    id 5
    label "rok"
  ]
  node [
    id 6
    label "33"
  ]
  node [
    id 7
    label "mig"
  ]
  node [
    id 8
    label "31"
  ]
  node [
    id 9
    label "by&#322;y"
  ]
  node [
    id 10
    label "1"
  ]
  node [
    id 11
    label "lancer"
  ]
  node [
    id 12
    label "senior"
  ]
  node [
    id 13
    label "71"
  ]
  node [
    id 14
    label "Blackbird"
  ]
  node [
    id 15
    label "52"
  ]
  node [
    id 16
    label "Stratofortress"
  ]
  node [
    id 17
    label "aa"
  ]
  node [
    id 18
    label "9"
  ]
  node [
    id 19
    label "Amos"
  ]
  node [
    id 20
    label "AIM"
  ]
  node [
    id 21
    label "54"
  ]
  node [
    id 22
    label "Phoenix"
  ]
  node [
    id 23
    label "40"
  ]
  node [
    id 24
    label "25"
  ]
  node [
    id 25
    label "XB"
  ]
  node [
    id 26
    label "70"
  ]
  node [
    id 27
    label "Valkyrie"
  ]
  node [
    id 28
    label "GosMBK"
  ]
  node [
    id 29
    label "Wympie&#322;"
  ]
  node [
    id 30
    label "&#1043;&#1086;&#1089;&#1052;&#1050;&#1041;"
  ]
  node [
    id 31
    label "&#171;"
  ]
  node [
    id 32
    label "&#1042;&#1099;&#1084;&#1087;&#1077;&#1083;"
  ]
  node [
    id 33
    label "&#187;"
  ]
  node [
    id 34
    label "e"
  ]
  node [
    id 35
    label "155MP"
  ]
  node [
    id 36
    label "ko&#322;o"
  ]
  node [
    id 37
    label "23"
  ]
  node [
    id 38
    label "13"
  ]
  node [
    id 39
    label "tu"
  ]
  node [
    id 40
    label "104"
  ]
  node [
    id 41
    label "ZBI"
  ]
  node [
    id 42
    label "16"
  ]
  node [
    id 43
    label "zas&#322;ona"
  ]
  node [
    id 44
    label "21"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 24
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 9
    target 15
  ]
  edge [
    source 9
    target 16
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 19
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 22
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 27
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 32
  ]
  edge [
    source 30
    target 33
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 33
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 38
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 43
  ]
  edge [
    source 42
    target 43
  ]
]
