graph [
  maxDegree 23
  minDegree 1
  meanDegree 1.951219512195122
  density 0.02408912978018669
  graphCliqueNumber 2
  node [
    id 0
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 1
    label "liga"
    origin "text"
  ]
  node [
    id 2
    label "europejski"
    origin "text"
  ]
  node [
    id 3
    label "protestowa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "przeciwko"
    origin "text"
  ]
  node [
    id 5
    label "wprowadzenie"
    origin "text"
  ]
  node [
    id 6
    label "zmiana"
    origin "text"
  ]
  node [
    id 7
    label "prawie"
    origin "text"
  ]
  node [
    id 8
    label "autorski"
    origin "text"
  ]
  node [
    id 9
    label "doros&#322;y"
  ]
  node [
    id 10
    label "wiele"
  ]
  node [
    id 11
    label "dorodny"
  ]
  node [
    id 12
    label "znaczny"
  ]
  node [
    id 13
    label "du&#380;o"
  ]
  node [
    id 14
    label "prawdziwy"
  ]
  node [
    id 15
    label "niema&#322;o"
  ]
  node [
    id 16
    label "wa&#380;ny"
  ]
  node [
    id 17
    label "rozwini&#281;ty"
  ]
  node [
    id 18
    label "poziom"
  ]
  node [
    id 19
    label "organizacja"
  ]
  node [
    id 20
    label "&#347;rodowisko"
  ]
  node [
    id 21
    label "atak"
  ]
  node [
    id 22
    label "obrona"
  ]
  node [
    id 23
    label "grupa"
  ]
  node [
    id 24
    label "mecz_mistrzowski"
  ]
  node [
    id 25
    label "pr&#243;ba"
  ]
  node [
    id 26
    label "zbi&#243;r"
  ]
  node [
    id 27
    label "arrangement"
  ]
  node [
    id 28
    label "rezerwa"
  ]
  node [
    id 29
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 30
    label "union"
  ]
  node [
    id 31
    label "pomoc"
  ]
  node [
    id 32
    label "moneta"
  ]
  node [
    id 33
    label "European"
  ]
  node [
    id 34
    label "po_europejsku"
  ]
  node [
    id 35
    label "charakterystyczny"
  ]
  node [
    id 36
    label "europejsko"
  ]
  node [
    id 37
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 38
    label "typowy"
  ]
  node [
    id 39
    label "napastowa&#263;"
  ]
  node [
    id 40
    label "post&#281;powa&#263;"
  ]
  node [
    id 41
    label "anticipate"
  ]
  node [
    id 42
    label "rynek"
  ]
  node [
    id 43
    label "issue"
  ]
  node [
    id 44
    label "evocation"
  ]
  node [
    id 45
    label "wst&#281;p"
  ]
  node [
    id 46
    label "nuklearyzacja"
  ]
  node [
    id 47
    label "umo&#380;liwienie"
  ]
  node [
    id 48
    label "zacz&#281;cie"
  ]
  node [
    id 49
    label "wpisanie"
  ]
  node [
    id 50
    label "zapoznanie"
  ]
  node [
    id 51
    label "zrobienie"
  ]
  node [
    id 52
    label "czynno&#347;&#263;"
  ]
  node [
    id 53
    label "entrance"
  ]
  node [
    id 54
    label "wej&#347;cie"
  ]
  node [
    id 55
    label "podstawy"
  ]
  node [
    id 56
    label "spowodowanie"
  ]
  node [
    id 57
    label "zak&#322;&#243;cenie"
  ]
  node [
    id 58
    label "w&#322;&#261;czenie"
  ]
  node [
    id 59
    label "doprowadzenie"
  ]
  node [
    id 60
    label "przewietrzenie"
  ]
  node [
    id 61
    label "deduction"
  ]
  node [
    id 62
    label "umieszczenie"
  ]
  node [
    id 63
    label "anatomopatolog"
  ]
  node [
    id 64
    label "rewizja"
  ]
  node [
    id 65
    label "oznaka"
  ]
  node [
    id 66
    label "czas"
  ]
  node [
    id 67
    label "ferment"
  ]
  node [
    id 68
    label "komplet"
  ]
  node [
    id 69
    label "tura"
  ]
  node [
    id 70
    label "amendment"
  ]
  node [
    id 71
    label "zmianka"
  ]
  node [
    id 72
    label "odmienianie"
  ]
  node [
    id 73
    label "passage"
  ]
  node [
    id 74
    label "zjawisko"
  ]
  node [
    id 75
    label "change"
  ]
  node [
    id 76
    label "praca"
  ]
  node [
    id 77
    label "w&#322;asny"
  ]
  node [
    id 78
    label "oryginalny"
  ]
  node [
    id 79
    label "autorsko"
  ]
  node [
    id 80
    label "Donald"
  ]
  node [
    id 81
    label "Tusk"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 80
    target 81
  ]
]
