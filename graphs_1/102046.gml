graph [
  maxDegree 11
  minDegree 1
  meanDegree 3.4583333333333335
  density 0.07358156028368794
  graphCliqueNumber 9
  node [
    id 0
    label "konsultacja"
    origin "text"
  ]
  node [
    id 1
    label "dla"
    origin "text"
  ]
  node [
    id 2
    label "rodzic"
    origin "text"
  ]
  node [
    id 3
    label "nauczyciel"
    origin "text"
  ]
  node [
    id 4
    label "mgr"
    origin "text"
  ]
  node [
    id 5
    label "el&#380;bieta"
    origin "text"
  ]
  node [
    id 6
    label "skonieczna"
    origin "text"
  ]
  node [
    id 7
    label "ocena"
  ]
  node [
    id 8
    label "narada"
  ]
  node [
    id 9
    label "porada"
  ]
  node [
    id 10
    label "opiekun"
  ]
  node [
    id 11
    label "wapniak"
  ]
  node [
    id 12
    label "rodzic_chrzestny"
  ]
  node [
    id 13
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 14
    label "rodzice"
  ]
  node [
    id 15
    label "profesor"
  ]
  node [
    id 16
    label "kszta&#322;ciciel"
  ]
  node [
    id 17
    label "szkolnik"
  ]
  node [
    id 18
    label "preceptor"
  ]
  node [
    id 19
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 20
    label "pedagog"
  ]
  node [
    id 21
    label "popularyzator"
  ]
  node [
    id 22
    label "belfer"
  ]
  node [
    id 23
    label "stopie&#324;_naukowy"
  ]
  node [
    id 24
    label "tytu&#322;"
  ]
  node [
    id 25
    label "El&#380;bieta"
  ]
  node [
    id 26
    label "Skonieczna"
  ]
  node [
    id 27
    label "jaka"
  ]
  node [
    id 28
    label "pom&#243;c"
  ]
  node [
    id 29
    label "dziecko"
  ]
  node [
    id 30
    label "zeszyt"
  ]
  node [
    id 31
    label "deficyt"
  ]
  node [
    id 32
    label "rozwojowy"
  ]
  node [
    id 33
    label "wyleczy&#263;"
  ]
  node [
    id 34
    label "by&#263;"
  ]
  node [
    id 35
    label "dysleksja"
  ]
  node [
    id 36
    label "metoda"
  ]
  node [
    id 37
    label "kinezjologii"
  ]
  node [
    id 38
    label "edukacyjny"
  ]
  node [
    id 39
    label "Alicja"
  ]
  node [
    id 40
    label "Nyczaj"
  ]
  node [
    id 41
    label "&#8211;"
  ]
  node [
    id 42
    label "Matusiak"
  ]
  node [
    id 43
    label "Anna"
  ]
  node [
    id 44
    label "Sadecka"
  ]
  node [
    id 45
    label "ortografia"
  ]
  node [
    id 46
    label "na"
  ]
  node [
    id 47
    label "weso&#322;o"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 29
  ]
  edge [
    source 27
    target 30
  ]
  edge [
    source 27
    target 31
  ]
  edge [
    source 27
    target 32
  ]
  edge [
    source 27
    target 33
  ]
  edge [
    source 27
    target 34
  ]
  edge [
    source 27
    target 35
  ]
  edge [
    source 27
    target 36
  ]
  edge [
    source 27
    target 37
  ]
  edge [
    source 27
    target 38
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 30
  ]
  edge [
    source 28
    target 31
  ]
  edge [
    source 28
    target 32
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 31
  ]
  edge [
    source 29
    target 32
  ]
  edge [
    source 29
    target 33
  ]
  edge [
    source 29
    target 34
  ]
  edge [
    source 29
    target 35
  ]
  edge [
    source 29
    target 36
  ]
  edge [
    source 29
    target 37
  ]
  edge [
    source 29
    target 38
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 32
  ]
  edge [
    source 30
    target 33
  ]
  edge [
    source 30
    target 34
  ]
  edge [
    source 30
    target 35
  ]
  edge [
    source 30
    target 36
  ]
  edge [
    source 30
    target 37
  ]
  edge [
    source 30
    target 38
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 35
  ]
  edge [
    source 33
    target 36
  ]
  edge [
    source 33
    target 37
  ]
  edge [
    source 33
    target 38
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 36
  ]
  edge [
    source 34
    target 37
  ]
  edge [
    source 34
    target 38
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 37
  ]
  edge [
    source 35
    target 38
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 38
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 41
  ]
  edge [
    source 39
    target 42
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 42
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 47
  ]
  edge [
    source 46
    target 47
  ]
]
