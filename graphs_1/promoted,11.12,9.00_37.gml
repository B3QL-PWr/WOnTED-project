graph [
  maxDegree 16
  minDegree 1
  meanDegree 1.9230769230769231
  density 0.07692307692307693
  graphCliqueNumber 2
  node [
    id 0
    label "ba&#324;ski"
    origin "text"
  ]
  node [
    id 1
    label "bystrzyca"
    origin "text"
  ]
  node [
    id 2
    label "jedno"
    origin "text"
  ]
  node [
    id 3
    label "&#322;adny"
    origin "text"
  ]
  node [
    id 4
    label "s&#322;owacki"
    origin "text"
  ]
  node [
    id 5
    label "nurt"
  ]
  node [
    id 6
    label "g&#322;adki"
  ]
  node [
    id 7
    label "przyjemny"
  ]
  node [
    id 8
    label "ch&#281;dogi"
  ]
  node [
    id 9
    label "harny"
  ]
  node [
    id 10
    label "po&#380;&#261;dany"
  ]
  node [
    id 11
    label "&#322;adnie"
  ]
  node [
    id 12
    label "obyczajny"
  ]
  node [
    id 13
    label "dobrze"
  ]
  node [
    id 14
    label "z&#322;y"
  ]
  node [
    id 15
    label "dobry"
  ]
  node [
    id 16
    label "ca&#322;y"
  ]
  node [
    id 17
    label "niez&#322;y"
  ]
  node [
    id 18
    label "&#347;warny"
  ]
  node [
    id 19
    label "przyzwoity"
  ]
  node [
    id 20
    label "j&#281;zyk_zachodnios&#322;owia&#324;ski"
  ]
  node [
    id 21
    label "j&#281;zyk"
  ]
  node [
    id 22
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 23
    label "Slovak"
  ]
  node [
    id 24
    label "po_s&#322;owacku"
  ]
  node [
    id 25
    label "s&#322;owia&#324;ski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
]
