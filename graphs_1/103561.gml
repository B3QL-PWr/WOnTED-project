graph [
  maxDegree 39
  minDegree 1
  meanDegree 1.980952380952381
  density 0.01904761904761905
  graphCliqueNumber 2
  node [
    id 0
    label "&#322;azienki"
    origin "text"
  ]
  node [
    id 1
    label "wychodzi&#263;"
    origin "text"
  ]
  node [
    id 2
    label "rom"
    origin "text"
  ]
  node [
    id 3
    label "wyciera&#263;"
    origin "text"
  ]
  node [
    id 4
    label "g&#322;owa"
    origin "text"
  ]
  node [
    id 5
    label "r&#281;cznik"
    origin "text"
  ]
  node [
    id 6
    label "widzie&#263;"
    origin "text"
  ]
  node [
    id 7
    label "rodzic"
    origin "text"
  ]
  node [
    id 8
    label "uzyskiwa&#263;"
  ]
  node [
    id 9
    label "impart"
  ]
  node [
    id 10
    label "proceed"
  ]
  node [
    id 11
    label "blend"
  ]
  node [
    id 12
    label "give"
  ]
  node [
    id 13
    label "ograniczenie"
  ]
  node [
    id 14
    label "wydostawa&#263;_si&#281;"
  ]
  node [
    id 15
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 16
    label "za&#322;atwi&#263;"
  ]
  node [
    id 17
    label "schodzi&#263;"
  ]
  node [
    id 18
    label "gra&#263;"
  ]
  node [
    id 19
    label "osi&#261;ga&#263;"
  ]
  node [
    id 20
    label "okazywa&#263;_si&#281;"
  ]
  node [
    id 21
    label "seclude"
  ]
  node [
    id 22
    label "strona_&#347;wiata"
  ]
  node [
    id 23
    label "przedk&#322;ada&#263;"
  ]
  node [
    id 24
    label "przedstawia&#263;"
  ]
  node [
    id 25
    label "appear"
  ]
  node [
    id 26
    label "publish"
  ]
  node [
    id 27
    label "ko&#324;czy&#263;"
  ]
  node [
    id 28
    label "wypada&#263;"
  ]
  node [
    id 29
    label "pochodzi&#263;"
  ]
  node [
    id 30
    label "ukazywa&#263;_si&#281;_drukiem"
  ]
  node [
    id 31
    label "uwalnia&#263;_si&#281;"
  ]
  node [
    id 32
    label "wygl&#261;da&#263;"
  ]
  node [
    id 33
    label "opuszcza&#263;"
  ]
  node [
    id 34
    label "wystarcza&#263;"
  ]
  node [
    id 35
    label "wyrusza&#263;"
  ]
  node [
    id 36
    label "perform"
  ]
  node [
    id 37
    label "heighten"
  ]
  node [
    id 38
    label "niszczy&#263;"
  ]
  node [
    id 39
    label "embroil"
  ]
  node [
    id 40
    label "osusza&#263;"
  ]
  node [
    id 41
    label "reject"
  ]
  node [
    id 42
    label "czy&#347;ci&#263;"
  ]
  node [
    id 43
    label "cz&#322;owiek"
  ]
  node [
    id 44
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 45
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 46
    label "ucho"
  ]
  node [
    id 47
    label "makrocefalia"
  ]
  node [
    id 48
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 49
    label "m&#243;zg"
  ]
  node [
    id 50
    label "kierownictwo"
  ]
  node [
    id 51
    label "czaszka"
  ]
  node [
    id 52
    label "dekiel"
  ]
  node [
    id 53
    label "umys&#322;"
  ]
  node [
    id 54
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 55
    label "&#347;ci&#281;cie"
  ]
  node [
    id 56
    label "sztuka"
  ]
  node [
    id 57
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 58
    label "g&#243;ra"
  ]
  node [
    id 59
    label "byd&#322;o"
  ]
  node [
    id 60
    label "alkohol"
  ]
  node [
    id 61
    label "wiedza"
  ]
  node [
    id 62
    label "ro&#347;lina"
  ]
  node [
    id 63
    label "&#347;ci&#281;gno"
  ]
  node [
    id 64
    label "&#380;ycie"
  ]
  node [
    id 65
    label "pryncypa&#322;"
  ]
  node [
    id 66
    label "fryzura"
  ]
  node [
    id 67
    label "noosfera"
  ]
  node [
    id 68
    label "kierowa&#263;"
  ]
  node [
    id 69
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 70
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 71
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 72
    label "cecha"
  ]
  node [
    id 73
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 74
    label "zdolno&#347;&#263;"
  ]
  node [
    id 75
    label "kszta&#322;t"
  ]
  node [
    id 76
    label "cz&#322;onek"
  ]
  node [
    id 77
    label "cia&#322;o"
  ]
  node [
    id 78
    label "obiekt"
  ]
  node [
    id 79
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 80
    label "przedmiot"
  ]
  node [
    id 81
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 82
    label "perceive"
  ]
  node [
    id 83
    label "reagowa&#263;"
  ]
  node [
    id 84
    label "spowodowa&#263;"
  ]
  node [
    id 85
    label "male&#263;"
  ]
  node [
    id 86
    label "zmale&#263;"
  ]
  node [
    id 87
    label "spotka&#263;"
  ]
  node [
    id 88
    label "go_steady"
  ]
  node [
    id 89
    label "dostrzega&#263;"
  ]
  node [
    id 90
    label "zwi&#281;ksza&#263;_si&#281;"
  ]
  node [
    id 91
    label "zdawa&#263;_sobie_spraw&#281;"
  ]
  node [
    id 92
    label "ogl&#261;da&#263;"
  ]
  node [
    id 93
    label "os&#261;dza&#263;"
  ]
  node [
    id 94
    label "aprobowa&#263;"
  ]
  node [
    id 95
    label "punkt_widzenia"
  ]
  node [
    id 96
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 97
    label "wzrok"
  ]
  node [
    id 98
    label "postrzega&#263;"
  ]
  node [
    id 99
    label "notice"
  ]
  node [
    id 100
    label "opiekun"
  ]
  node [
    id 101
    label "wapniak"
  ]
  node [
    id 102
    label "rodzic_chrzestny"
  ]
  node [
    id 103
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 104
    label "rodzice"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
]
