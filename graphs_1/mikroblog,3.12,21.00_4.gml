graph [
  maxDegree 3
  minDegree 1
  meanDegree 2.3076923076923075
  density 0.19230769230769232
  graphCliqueNumber 4
  node [
    id 0
    label "achtung"
    origin "text"
  ]
  node [
    id 1
    label "rozdajo"
    origin "text"
  ]
  node [
    id 2
    label "steam"
    origin "text"
  ]
  node [
    id 3
    label "Humble"
  ]
  node [
    id 4
    label "Monthly"
  ]
  node [
    id 5
    label "metal"
  ]
  node [
    id 6
    label "Gear"
  ]
  node [
    id 7
    label "solid"
  ]
  node [
    id 8
    label "V"
  ]
  node [
    id 9
    label "Phantom"
  ]
  node [
    id 10
    label "Pain"
  ]
  node [
    id 11
    label "Ground"
  ]
  node [
    id 12
    label "Zeroes"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 9
    target 12
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 11
    target 12
  ]
]
