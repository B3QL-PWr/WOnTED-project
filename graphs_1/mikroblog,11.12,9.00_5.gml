graph [
  maxDegree 44
  minDegree 1
  meanDegree 2.0128205128205128
  density 0.012985938792390406
  graphCliqueNumber 3
  node [
    id 0
    label "moi"
    origin "text"
  ]
  node [
    id 1
    label "zdanie"
    origin "text"
  ]
  node [
    id 2
    label "&#380;eby"
    origin "text"
  ]
  node [
    id 3
    label "osi&#261;gn&#261;&#263;"
    origin "text"
  ]
  node [
    id 4
    label "spok&#243;j"
    origin "text"
  ]
  node [
    id 5
    label "duch"
    origin "text"
  ]
  node [
    id 6
    label "trzeba"
    origin "text"
  ]
  node [
    id 7
    label "zna&#263;"
    origin "text"
  ]
  node [
    id 8
    label "odpowied&#378;"
    origin "text"
  ]
  node [
    id 9
    label "fundamentalny"
    origin "text"
  ]
  node [
    id 10
    label "pytanie"
    origin "text"
  ]
  node [
    id 11
    label "chuj"
    origin "text"
  ]
  node [
    id 12
    label "chodzi&#263;"
    origin "text"
  ]
  node [
    id 13
    label "attitude"
  ]
  node [
    id 14
    label "system"
  ]
  node [
    id 15
    label "przedstawienie"
  ]
  node [
    id 16
    label "fraza"
  ]
  node [
    id 17
    label "prison_term"
  ]
  node [
    id 18
    label "adjudication"
  ]
  node [
    id 19
    label "przekazanie"
  ]
  node [
    id 20
    label "pass"
  ]
  node [
    id 21
    label "wyra&#380;enie"
  ]
  node [
    id 22
    label "okres"
  ]
  node [
    id 23
    label "z&#322;o&#380;enie_egzaminu"
  ]
  node [
    id 24
    label "wypowiedzenie"
  ]
  node [
    id 25
    label "konektyw"
  ]
  node [
    id 26
    label "zaliczenie"
  ]
  node [
    id 27
    label "stanowisko"
  ]
  node [
    id 28
    label "powierzenie"
  ]
  node [
    id 29
    label "antylogizm"
  ]
  node [
    id 30
    label "zmuszenie"
  ]
  node [
    id 31
    label "szko&#322;a"
  ]
  node [
    id 32
    label "profit"
  ]
  node [
    id 33
    label "score"
  ]
  node [
    id 34
    label "dotrze&#263;"
  ]
  node [
    id 35
    label "uzyska&#263;"
  ]
  node [
    id 36
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 37
    label "make"
  ]
  node [
    id 38
    label "control"
  ]
  node [
    id 39
    label "stan"
  ]
  node [
    id 40
    label "czas"
  ]
  node [
    id 41
    label "cecha"
  ]
  node [
    id 42
    label "ci&#261;g"
  ]
  node [
    id 43
    label "tajemno&#347;&#263;"
  ]
  node [
    id 44
    label "slowness"
  ]
  node [
    id 45
    label "cisza"
  ]
  node [
    id 46
    label "cz&#322;owiek"
  ]
  node [
    id 47
    label "T&#281;sknica"
  ]
  node [
    id 48
    label "kompleks"
  ]
  node [
    id 49
    label "sfera_afektywna"
  ]
  node [
    id 50
    label "sumienie"
  ]
  node [
    id 51
    label "entity"
  ]
  node [
    id 52
    label "kompleksja"
  ]
  node [
    id 53
    label "power"
  ]
  node [
    id 54
    label "nekromancja"
  ]
  node [
    id 55
    label "piek&#322;o"
  ]
  node [
    id 56
    label "psychika"
  ]
  node [
    id 57
    label "zapalno&#347;&#263;"
  ]
  node [
    id 58
    label "podekscytowanie"
  ]
  node [
    id 59
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 60
    label "shape"
  ]
  node [
    id 61
    label "fizjonomia"
  ]
  node [
    id 62
    label "ofiarowa&#263;"
  ]
  node [
    id 63
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 64
    label "charakter"
  ]
  node [
    id 65
    label "mikrokosmos"
  ]
  node [
    id 66
    label "byt"
  ]
  node [
    id 67
    label "si&#322;a"
  ]
  node [
    id 68
    label "ofiarowanie"
  ]
  node [
    id 69
    label "Po&#347;wist"
  ]
  node [
    id 70
    label "passion"
  ]
  node [
    id 71
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 72
    label "ego"
  ]
  node [
    id 73
    label "human_body"
  ]
  node [
    id 74
    label "zmar&#322;y"
  ]
  node [
    id 75
    label "osobowo&#347;&#263;"
  ]
  node [
    id 76
    label "ofiarowywanie"
  ]
  node [
    id 77
    label "osoba"
  ]
  node [
    id 78
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 79
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 80
    label "deformowa&#263;"
  ]
  node [
    id 81
    label "oddech"
  ]
  node [
    id 82
    label "seksualno&#347;&#263;"
  ]
  node [
    id 83
    label "zjawa"
  ]
  node [
    id 84
    label "istota_nadprzyrodzona"
  ]
  node [
    id 85
    label "ofiarowywa&#263;"
  ]
  node [
    id 86
    label "deformowanie"
  ]
  node [
    id 87
    label "trza"
  ]
  node [
    id 88
    label "necessity"
  ]
  node [
    id 89
    label "wiedzie&#263;"
  ]
  node [
    id 90
    label "cognizance"
  ]
  node [
    id 91
    label "dokument"
  ]
  node [
    id 92
    label "rozmowa"
  ]
  node [
    id 93
    label "reakcja"
  ]
  node [
    id 94
    label "wyj&#347;cie"
  ]
  node [
    id 95
    label "react"
  ]
  node [
    id 96
    label "respondent"
  ]
  node [
    id 97
    label "replica"
  ]
  node [
    id 98
    label "zasadniczy"
  ]
  node [
    id 99
    label "podstawowy"
  ]
  node [
    id 100
    label "fundamentalnie"
  ]
  node [
    id 101
    label "sprawa"
  ]
  node [
    id 102
    label "zadanie"
  ]
  node [
    id 103
    label "wypowied&#378;"
  ]
  node [
    id 104
    label "problemat"
  ]
  node [
    id 105
    label "rozpytywanie"
  ]
  node [
    id 106
    label "sprawdzian"
  ]
  node [
    id 107
    label "przes&#322;uchiwanie"
  ]
  node [
    id 108
    label "wypytanie"
  ]
  node [
    id 109
    label "zwracanie_si&#281;"
  ]
  node [
    id 110
    label "wywo&#322;ywanie"
  ]
  node [
    id 111
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 112
    label "problematyka"
  ]
  node [
    id 113
    label "question"
  ]
  node [
    id 114
    label "sprawdzanie"
  ]
  node [
    id 115
    label "odpowiadanie"
  ]
  node [
    id 116
    label "survey"
  ]
  node [
    id 117
    label "odpowiada&#263;"
  ]
  node [
    id 118
    label "egzaminowanie"
  ]
  node [
    id 119
    label "dupek"
  ]
  node [
    id 120
    label "skurwysyn"
  ]
  node [
    id 121
    label "wyzwisko"
  ]
  node [
    id 122
    label "penis"
  ]
  node [
    id 123
    label "ciul"
  ]
  node [
    id 124
    label "przekle&#324;stwo"
  ]
  node [
    id 125
    label "proceed"
  ]
  node [
    id 126
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 127
    label "bangla&#263;"
  ]
  node [
    id 128
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 129
    label "by&#263;"
  ]
  node [
    id 130
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 131
    label "run"
  ]
  node [
    id 132
    label "tryb"
  ]
  node [
    id 133
    label "p&#322;ywa&#263;"
  ]
  node [
    id 134
    label "continue"
  ]
  node [
    id 135
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 136
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 137
    label "przebiega&#263;"
  ]
  node [
    id 138
    label "mie&#263;_miejsce"
  ]
  node [
    id 139
    label "wk&#322;ada&#263;"
  ]
  node [
    id 140
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 141
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 142
    label "para"
  ]
  node [
    id 143
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 144
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 145
    label "krok"
  ]
  node [
    id 146
    label "str&#243;j"
  ]
  node [
    id 147
    label "bywa&#263;"
  ]
  node [
    id 148
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 149
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 150
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 151
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 152
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 153
    label "dziama&#263;"
  ]
  node [
    id 154
    label "stara&#263;_si&#281;"
  ]
  node [
    id 155
    label "carry"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 9
    target 100
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 10
    target 105
  ]
  edge [
    source 10
    target 106
  ]
  edge [
    source 10
    target 107
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 10
    target 24
  ]
  edge [
    source 10
    target 110
  ]
  edge [
    source 10
    target 111
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 119
  ]
  edge [
    source 11
    target 120
  ]
  edge [
    source 11
    target 121
  ]
  edge [
    source 11
    target 122
  ]
  edge [
    source 11
    target 123
  ]
  edge [
    source 11
    target 124
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 12
    target 131
  ]
  edge [
    source 12
    target 132
  ]
  edge [
    source 12
    target 133
  ]
  edge [
    source 12
    target 134
  ]
  edge [
    source 12
    target 135
  ]
  edge [
    source 12
    target 136
  ]
  edge [
    source 12
    target 137
  ]
  edge [
    source 12
    target 138
  ]
  edge [
    source 12
    target 139
  ]
  edge [
    source 12
    target 140
  ]
  edge [
    source 12
    target 141
  ]
  edge [
    source 12
    target 142
  ]
  edge [
    source 12
    target 143
  ]
  edge [
    source 12
    target 144
  ]
  edge [
    source 12
    target 145
  ]
  edge [
    source 12
    target 146
  ]
  edge [
    source 12
    target 147
  ]
  edge [
    source 12
    target 148
  ]
  edge [
    source 12
    target 149
  ]
  edge [
    source 12
    target 150
  ]
  edge [
    source 12
    target 151
  ]
  edge [
    source 12
    target 152
  ]
  edge [
    source 12
    target 153
  ]
  edge [
    source 12
    target 154
  ]
  edge [
    source 12
    target 155
  ]
]
