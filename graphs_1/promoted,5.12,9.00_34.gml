graph [
  maxDegree 30
  minDegree 1
  meanDegree 2
  density 0.017094017094017096
  graphCliqueNumber 2
  node [
    id 0
    label "ofiara"
    origin "text"
  ]
  node [
    id 1
    label "ksi&#261;dz"
    origin "text"
  ]
  node [
    id 2
    label "pedofil"
    origin "text"
  ]
  node [
    id 3
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 4
    label "s&#261;d"
    origin "text"
  ]
  node [
    id 5
    label "przyzna&#263;"
    origin "text"
  ]
  node [
    id 6
    label "milion"
    origin "text"
  ]
  node [
    id 7
    label "z&#322;oty"
    origin "text"
  ]
  node [
    id 8
    label "odszkodowanie"
    origin "text"
  ]
  node [
    id 9
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 10
    label "skorzysta&#263;"
    origin "text"
  ]
  node [
    id 11
    label "pieni&#261;dz"
    origin "text"
  ]
  node [
    id 12
    label "cz&#322;owiek"
  ]
  node [
    id 13
    label "gamo&#324;"
  ]
  node [
    id 14
    label "istota_&#380;ywa"
  ]
  node [
    id 15
    label "dupa_wo&#322;owa"
  ]
  node [
    id 16
    label "przedmiot"
  ]
  node [
    id 17
    label "pastwa"
  ]
  node [
    id 18
    label "rzecz"
  ]
  node [
    id 19
    label "zbi&#243;rka"
  ]
  node [
    id 20
    label "dar"
  ]
  node [
    id 21
    label "nieszcz&#281;&#347;nik"
  ]
  node [
    id 22
    label "nastawienie"
  ]
  node [
    id 23
    label "crack"
  ]
  node [
    id 24
    label "zwierz&#281;"
  ]
  node [
    id 25
    label "ko&#347;cielny"
  ]
  node [
    id 26
    label "po&#347;miewisko"
  ]
  node [
    id 27
    label "klecha"
  ]
  node [
    id 28
    label "eklezjasta"
  ]
  node [
    id 29
    label "rozgrzeszanie"
  ]
  node [
    id 30
    label "duszpasterstwo"
  ]
  node [
    id 31
    label "rozgrzesza&#263;"
  ]
  node [
    id 32
    label "duchowny"
  ]
  node [
    id 33
    label "ksi&#281;&#380;a"
  ]
  node [
    id 34
    label "kap&#322;an"
  ]
  node [
    id 35
    label "kol&#281;da"
  ]
  node [
    id 36
    label "seminarzysta"
  ]
  node [
    id 37
    label "pasterz"
  ]
  node [
    id 38
    label "organizm"
  ]
  node [
    id 39
    label "dewiant"
  ]
  node [
    id 40
    label "dziecko"
  ]
  node [
    id 41
    label "procesowicz"
  ]
  node [
    id 42
    label "wypowied&#378;"
  ]
  node [
    id 43
    label "pods&#261;dny"
  ]
  node [
    id 44
    label "podejrzany"
  ]
  node [
    id 45
    label "broni&#263;"
  ]
  node [
    id 46
    label "bronienie"
  ]
  node [
    id 47
    label "system"
  ]
  node [
    id 48
    label "my&#347;l"
  ]
  node [
    id 49
    label "wytw&#243;r"
  ]
  node [
    id 50
    label "urz&#261;d"
  ]
  node [
    id 51
    label "konektyw"
  ]
  node [
    id 52
    label "court"
  ]
  node [
    id 53
    label "obrona"
  ]
  node [
    id 54
    label "s&#261;downictwo"
  ]
  node [
    id 55
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 56
    label "forum"
  ]
  node [
    id 57
    label "zesp&#243;&#322;"
  ]
  node [
    id 58
    label "post&#281;powanie"
  ]
  node [
    id 59
    label "skazany"
  ]
  node [
    id 60
    label "wydarzenie"
  ]
  node [
    id 61
    label "&#347;wiadek"
  ]
  node [
    id 62
    label "antylogizm"
  ]
  node [
    id 63
    label "strona"
  ]
  node [
    id 64
    label "oskar&#380;yciel"
  ]
  node [
    id 65
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 66
    label "biuro"
  ]
  node [
    id 67
    label "instytucja"
  ]
  node [
    id 68
    label "da&#263;"
  ]
  node [
    id 69
    label "nada&#263;"
  ]
  node [
    id 70
    label "pozwoli&#263;"
  ]
  node [
    id 71
    label "give"
  ]
  node [
    id 72
    label "stwierdzi&#263;"
  ]
  node [
    id 73
    label "liczba"
  ]
  node [
    id 74
    label "miljon"
  ]
  node [
    id 75
    label "ba&#324;ka"
  ]
  node [
    id 76
    label "szlachetny"
  ]
  node [
    id 77
    label "metaliczny"
  ]
  node [
    id 78
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 79
    label "z&#322;ocenie"
  ]
  node [
    id 80
    label "grosz"
  ]
  node [
    id 81
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 82
    label "utytu&#322;owany"
  ]
  node [
    id 83
    label "poz&#322;ocenie"
  ]
  node [
    id 84
    label "Polska"
  ]
  node [
    id 85
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 86
    label "wspania&#322;y"
  ]
  node [
    id 87
    label "doskona&#322;y"
  ]
  node [
    id 88
    label "kochany"
  ]
  node [
    id 89
    label "jednostka_monetarna"
  ]
  node [
    id 90
    label "z&#322;oci&#347;cie"
  ]
  node [
    id 91
    label "zap&#322;ata"
  ]
  node [
    id 92
    label "refund"
  ]
  node [
    id 93
    label "ryzyko_niewygas&#322;e"
  ]
  node [
    id 94
    label "zado&#347;&#263;uczynienie"
  ]
  node [
    id 95
    label "zrobi&#263;"
  ]
  node [
    id 96
    label "u&#380;y&#263;"
  ]
  node [
    id 97
    label "uzyska&#263;"
  ]
  node [
    id 98
    label "utilize"
  ]
  node [
    id 99
    label "poda&#380;_pieni&#261;dza"
  ]
  node [
    id 100
    label "zdewaluowa&#263;"
  ]
  node [
    id 101
    label "moniak"
  ]
  node [
    id 102
    label "zdewaluowanie"
  ]
  node [
    id 103
    label "&#347;rodek_p&#322;atniczy"
  ]
  node [
    id 104
    label "numizmat"
  ]
  node [
    id 105
    label "rozmienianie"
  ]
  node [
    id 106
    label "rozmienienie"
  ]
  node [
    id 107
    label "rozmieni&#263;"
  ]
  node [
    id 108
    label "dewaluowanie"
  ]
  node [
    id 109
    label "nomina&#322;"
  ]
  node [
    id 110
    label "coin"
  ]
  node [
    id 111
    label "dewaluowa&#263;"
  ]
  node [
    id 112
    label "pieni&#261;dze"
  ]
  node [
    id 113
    label "znak_pieni&#281;&#380;ny"
  ]
  node [
    id 114
    label "rozmienia&#263;"
  ]
  node [
    id 115
    label "towarzystwo"
  ]
  node [
    id 116
    label "chrystusowy"
  ]
  node [
    id 117
    label "najwy&#380;szy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 11
    target 100
  ]
  edge [
    source 11
    target 101
  ]
  edge [
    source 11
    target 49
  ]
  edge [
    source 11
    target 102
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 11
    target 103
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 108
  ]
  edge [
    source 11
    target 109
  ]
  edge [
    source 11
    target 110
  ]
  edge [
    source 11
    target 111
  ]
  edge [
    source 11
    target 112
  ]
  edge [
    source 11
    target 113
  ]
  edge [
    source 11
    target 114
  ]
  edge [
    source 115
    target 116
  ]
]
