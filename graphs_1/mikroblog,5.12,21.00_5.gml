graph [
  maxDegree 23
  minDegree 1
  meanDegree 1.9428571428571428
  density 0.05714285714285714
  graphCliqueNumber 2
  node [
    id 0
    label "heheszki"
    origin "text"
  ]
  node [
    id 1
    label "humor"
    origin "text"
  ]
  node [
    id 2
    label "alkohol"
    origin "text"
  ]
  node [
    id 3
    label "humorobrazkowy"
    origin "text"
  ]
  node [
    id 4
    label "stan"
  ]
  node [
    id 5
    label "temper"
  ]
  node [
    id 6
    label "&#347;mieszno&#347;&#263;"
  ]
  node [
    id 7
    label "mechanizm_obronny"
  ]
  node [
    id 8
    label "nastr&#243;j"
  ]
  node [
    id 9
    label "state"
  ]
  node [
    id 10
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 11
    label "samopoczucie"
  ]
  node [
    id 12
    label "fondness"
  ]
  node [
    id 13
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 14
    label "gorzelnia_rolnicza"
  ]
  node [
    id 15
    label "upija&#263;"
  ]
  node [
    id 16
    label "szk&#322;o"
  ]
  node [
    id 17
    label "spirytualia"
  ]
  node [
    id 18
    label "nap&#243;j"
  ]
  node [
    id 19
    label "wypicie"
  ]
  node [
    id 20
    label "poniewierca"
  ]
  node [
    id 21
    label "rozgrzewacz"
  ]
  node [
    id 22
    label "upajanie"
  ]
  node [
    id 23
    label "piwniczka"
  ]
  node [
    id 24
    label "najebka"
  ]
  node [
    id 25
    label "grupa_hydroksylowa"
  ]
  node [
    id 26
    label "le&#380;akownia"
  ]
  node [
    id 27
    label "g&#322;owa"
  ]
  node [
    id 28
    label "upi&#263;"
  ]
  node [
    id 29
    label "upojenie"
  ]
  node [
    id 30
    label "likwor"
  ]
  node [
    id 31
    label "u&#380;ywka"
  ]
  node [
    id 32
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 33
    label "alko"
  ]
  node [
    id 34
    label "picie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
]
