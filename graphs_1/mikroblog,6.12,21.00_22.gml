graph [
  maxDegree 18
  minDegree 1
  meanDegree 1.9
  density 0.1
  graphCliqueNumber 2
  node [
    id 0
    label "heheszki"
    origin "text"
  ]
  node [
    id 1
    label "religia"
    origin "text"
  ]
  node [
    id 2
    label "bekazkatoli"
    origin "text"
  ]
  node [
    id 3
    label "humorinformatykow"
    origin "text"
  ]
  node [
    id 4
    label "wyznanie"
  ]
  node [
    id 5
    label "mitologia"
  ]
  node [
    id 6
    label "przedmiot"
  ]
  node [
    id 7
    label "ideologia"
  ]
  node [
    id 8
    label "kosmogonia"
  ]
  node [
    id 9
    label "mistyka"
  ]
  node [
    id 10
    label "nawraca&#263;"
  ]
  node [
    id 11
    label "nawracanie_si&#281;"
  ]
  node [
    id 12
    label "duchowny"
  ]
  node [
    id 13
    label "kultura"
  ]
  node [
    id 14
    label "kultura_duchowa"
  ]
  node [
    id 15
    label "kosmologia"
  ]
  node [
    id 16
    label "zwi&#261;zek_wyznaniowy"
  ]
  node [
    id 17
    label "nawraca&#263;_si&#281;"
  ]
  node [
    id 18
    label "kult"
  ]
  node [
    id 19
    label "rela"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 2
    target 3
  ]
]
