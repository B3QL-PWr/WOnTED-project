graph [
  maxDegree 18
  minDegree 1
  meanDegree 1.9726027397260273
  density 0.0273972602739726
  graphCliqueNumber 2
  node [
    id 0
    label "jeden"
    origin "text"
  ]
  node [
    id 1
    label "ko&#347;ci&#243;&#322;"
    origin "text"
  ]
  node [
    id 2
    label "protestancki"
    origin "text"
  ]
  node [
    id 3
    label "haga"
    origin "text"
  ]
  node [
    id 4
    label "pa&#378;dziernik"
    origin "text"
  ]
  node [
    id 5
    label "duchowny"
    origin "text"
  ]
  node [
    id 6
    label "odprawia&#263;"
    origin "text"
  ]
  node [
    id 7
    label "nieprzerwany"
    origin "text"
  ]
  node [
    id 8
    label "nabo&#380;e&#324;stwo"
    origin "text"
  ]
  node [
    id 9
    label "kieliszek"
  ]
  node [
    id 10
    label "shot"
  ]
  node [
    id 11
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 12
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 13
    label "jaki&#347;"
  ]
  node [
    id 14
    label "jednolicie"
  ]
  node [
    id 15
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 16
    label "w&#243;dka"
  ]
  node [
    id 17
    label "ten"
  ]
  node [
    id 18
    label "ujednolicenie"
  ]
  node [
    id 19
    label "jednakowy"
  ]
  node [
    id 20
    label "&#347;wi&#261;tynia"
  ]
  node [
    id 21
    label "zakrystia"
  ]
  node [
    id 22
    label "organizacja_religijna"
  ]
  node [
    id 23
    label "nawa"
  ]
  node [
    id 24
    label "nerwica_eklezjogenna"
  ]
  node [
    id 25
    label "prezbiterium"
  ]
  node [
    id 26
    label "kropielnica"
  ]
  node [
    id 27
    label "wsp&#243;lnota"
  ]
  node [
    id 28
    label "church"
  ]
  node [
    id 29
    label "kruchta"
  ]
  node [
    id 30
    label "Ska&#322;ka"
  ]
  node [
    id 31
    label "kult"
  ]
  node [
    id 32
    label "ub&#322;agalnia"
  ]
  node [
    id 33
    label "dom"
  ]
  node [
    id 34
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 35
    label "chrze&#347;cija&#324;ski"
  ]
  node [
    id 36
    label "wyznaniowy"
  ]
  node [
    id 37
    label "Dzie&#324;_Papieski"
  ]
  node [
    id 38
    label "oktober"
  ]
  node [
    id 39
    label "miesi&#261;c"
  ]
  node [
    id 40
    label "cz&#322;owiek"
  ]
  node [
    id 41
    label "religia"
  ]
  node [
    id 42
    label "tonsura"
  ]
  node [
    id 43
    label "przedstawiciel"
  ]
  node [
    id 44
    label "kongregacja"
  ]
  node [
    id 45
    label "religijny"
  ]
  node [
    id 46
    label "sekularyzacja"
  ]
  node [
    id 47
    label "seminarzysta"
  ]
  node [
    id 48
    label "&#347;wi&#281;cenia"
  ]
  node [
    id 49
    label "&#347;w"
  ]
  node [
    id 50
    label "Bayes"
  ]
  node [
    id 51
    label "wyznawca"
  ]
  node [
    id 52
    label "Luter"
  ]
  node [
    id 53
    label "eklezjasta"
  ]
  node [
    id 54
    label "Hus"
  ]
  node [
    id 55
    label "duchowie&#324;stwo"
  ]
  node [
    id 56
    label "prezbiter"
  ]
  node [
    id 57
    label "wylewa&#263;"
  ]
  node [
    id 58
    label "unbosom"
  ]
  node [
    id 59
    label "wyprawia&#263;"
  ]
  node [
    id 60
    label "robi&#263;"
  ]
  node [
    id 61
    label "oddala&#263;"
  ]
  node [
    id 62
    label "perform"
  ]
  node [
    id 63
    label "wymawia&#263;"
  ]
  node [
    id 64
    label "niesko&#324;czony"
  ]
  node [
    id 65
    label "ci&#261;g&#322;y"
  ]
  node [
    id 66
    label "niezmierzony"
  ]
  node [
    id 67
    label "nieprzerwanie"
  ]
  node [
    id 68
    label "nieustanny"
  ]
  node [
    id 69
    label "devotion"
  ]
  node [
    id 70
    label "obrz&#281;d"
  ]
  node [
    id 71
    label "powa&#380;anie"
  ]
  node [
    id 72
    label "dba&#322;o&#347;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
]
