graph [
  maxDegree 19
  minDegree 1
  meanDegree 2.1666666666666665
  density 0.03672316384180791
  graphCliqueNumber 5
  node [
    id 0
    label "podpis"
    origin "text"
  ]
  node [
    id 1
    label "prezydent"
    origin "text"
  ]
  node [
    id 2
    label "czeka&#263;"
    origin "text"
  ]
  node [
    id 3
    label "nowelizacja"
    origin "text"
  ]
  node [
    id 4
    label "ustawa"
    origin "text"
  ]
  node [
    id 5
    label "krajowy"
    origin "text"
  ]
  node [
    id 6
    label "administracja"
    origin "text"
  ]
  node [
    id 7
    label "skarbowy"
    origin "text"
  ]
  node [
    id 8
    label "w&#322;asnor&#281;czny"
  ]
  node [
    id 9
    label "napis"
  ]
  node [
    id 10
    label "obja&#347;nienie"
  ]
  node [
    id 11
    label "sign"
  ]
  node [
    id 12
    label "potwierdzenie"
  ]
  node [
    id 13
    label "signal"
  ]
  node [
    id 14
    label "znak"
  ]
  node [
    id 15
    label "Jelcyn"
  ]
  node [
    id 16
    label "Roosevelt"
  ]
  node [
    id 17
    label "Clinton"
  ]
  node [
    id 18
    label "dostojnik"
  ]
  node [
    id 19
    label "Nixon"
  ]
  node [
    id 20
    label "Tito"
  ]
  node [
    id 21
    label "de_Gaulle"
  ]
  node [
    id 22
    label "gruba_ryba"
  ]
  node [
    id 23
    label "Gorbaczow"
  ]
  node [
    id 24
    label "Wa&#322;&#281;sa"
  ]
  node [
    id 25
    label "Putin"
  ]
  node [
    id 26
    label "Naser"
  ]
  node [
    id 27
    label "samorz&#261;dowiec"
  ]
  node [
    id 28
    label "Kemal"
  ]
  node [
    id 29
    label "g&#322;owa_pa&#324;stwa"
  ]
  node [
    id 30
    label "zwierzchnik"
  ]
  node [
    id 31
    label "Bierut"
  ]
  node [
    id 32
    label "by&#263;"
  ]
  node [
    id 33
    label "hold"
  ]
  node [
    id 34
    label "sp&#281;dza&#263;"
  ]
  node [
    id 35
    label "look"
  ]
  node [
    id 36
    label "decydowa&#263;"
  ]
  node [
    id 37
    label "oczekiwa&#263;"
  ]
  node [
    id 38
    label "pauzowa&#263;"
  ]
  node [
    id 39
    label "anticipate"
  ]
  node [
    id 40
    label "modyfikacja"
  ]
  node [
    id 41
    label "story"
  ]
  node [
    id 42
    label "amendment"
  ]
  node [
    id 43
    label "Karta_Nauczyciela"
  ]
  node [
    id 44
    label "marc&#243;wka"
  ]
  node [
    id 45
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 46
    label "akt"
  ]
  node [
    id 47
    label "przej&#347;&#263;"
  ]
  node [
    id 48
    label "charter"
  ]
  node [
    id 49
    label "przej&#347;cie"
  ]
  node [
    id 50
    label "rodzimy"
  ]
  node [
    id 51
    label "zarz&#261;d"
  ]
  node [
    id 52
    label "gospodarka"
  ]
  node [
    id 53
    label "petent"
  ]
  node [
    id 54
    label "biuro"
  ]
  node [
    id 55
    label "dziekanat"
  ]
  node [
    id 56
    label "struktura"
  ]
  node [
    id 57
    label "siedziba"
  ]
  node [
    id 58
    label "fiscal"
  ]
  node [
    id 59
    label "o"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
]
