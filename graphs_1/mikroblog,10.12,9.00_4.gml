graph [
  maxDegree 31
  minDegree 1
  meanDegree 1.980952380952381
  density 0.01904761904761905
  graphCliqueNumber 2
  node [
    id 0
    label "wita&#263;"
    origin "text"
  ]
  node [
    id 1
    label "serdecznie"
    origin "text"
  ]
  node [
    id 2
    label "nasi"
    origin "text"
  ]
  node [
    id 3
    label "bohater"
    origin "text"
  ]
  node [
    id 4
    label "bez"
    origin "text"
  ]
  node [
    id 5
    label "peleryna"
    origin "text"
  ]
  node [
    id 6
    label "pisa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "sprawa"
    origin "text"
  ]
  node [
    id 8
    label "marta"
    origin "text"
  ]
  node [
    id 9
    label "wczorajszy"
    origin "text"
  ]
  node [
    id 10
    label "zbi&#243;rka"
    origin "text"
  ]
  node [
    id 11
    label "https"
    origin "text"
  ]
  node [
    id 12
    label "pozdrawia&#263;"
  ]
  node [
    id 13
    label "&#347;wi&#281;towa&#263;"
  ]
  node [
    id 14
    label "greet"
  ]
  node [
    id 15
    label "uzewn&#281;trznia&#263;_si&#281;"
  ]
  node [
    id 16
    label "welcome"
  ]
  node [
    id 17
    label "siarczy&#347;cie"
  ]
  node [
    id 18
    label "serdeczny"
  ]
  node [
    id 19
    label "szczerze"
  ]
  node [
    id 20
    label "mi&#322;o"
  ]
  node [
    id 21
    label "bohaterski"
  ]
  node [
    id 22
    label "cz&#322;owiek"
  ]
  node [
    id 23
    label "Zgredek"
  ]
  node [
    id 24
    label "Herkules"
  ]
  node [
    id 25
    label "Casanova"
  ]
  node [
    id 26
    label "Borewicz"
  ]
  node [
    id 27
    label "Don_Juan"
  ]
  node [
    id 28
    label "posta&#263;_mitologiczna"
  ]
  node [
    id 29
    label "Winnetou"
  ]
  node [
    id 30
    label "niezwyci&#281;&#380;ony"
  ]
  node [
    id 31
    label "Messi"
  ]
  node [
    id 32
    label "Herkules_Poirot"
  ]
  node [
    id 33
    label "cz&#322;owiek_wielkiego_ducha"
  ]
  node [
    id 34
    label "Szwejk"
  ]
  node [
    id 35
    label "Sherlock_Holmes"
  ]
  node [
    id 36
    label "Fantastyczna_Czw&#243;rka"
  ]
  node [
    id 37
    label "Hamlet"
  ]
  node [
    id 38
    label "Asterix"
  ]
  node [
    id 39
    label "Quasimodo"
  ]
  node [
    id 40
    label "Don_Kiszot"
  ]
  node [
    id 41
    label "Wallenrod"
  ]
  node [
    id 42
    label "uczestnik"
  ]
  node [
    id 43
    label "&#347;mia&#322;ek"
  ]
  node [
    id 44
    label "Harry_Potter"
  ]
  node [
    id 45
    label "podmiot"
  ]
  node [
    id 46
    label "Achilles"
  ]
  node [
    id 47
    label "Werter"
  ]
  node [
    id 48
    label "Mario"
  ]
  node [
    id 49
    label "posta&#263;"
  ]
  node [
    id 50
    label "ki&#347;&#263;"
  ]
  node [
    id 51
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 52
    label "krzew"
  ]
  node [
    id 53
    label "pi&#380;maczkowate"
  ]
  node [
    id 54
    label "pestkowiec"
  ]
  node [
    id 55
    label "kwiat"
  ]
  node [
    id 56
    label "owoc"
  ]
  node [
    id 57
    label "oliwkowate"
  ]
  node [
    id 58
    label "ro&#347;lina"
  ]
  node [
    id 59
    label "hy&#263;ka"
  ]
  node [
    id 60
    label "lilac"
  ]
  node [
    id 61
    label "delfinidyna"
  ]
  node [
    id 62
    label "p&#322;aszcz"
  ]
  node [
    id 63
    label "okrycie"
  ]
  node [
    id 64
    label "ozdabia&#263;"
  ]
  node [
    id 65
    label "dysgrafia"
  ]
  node [
    id 66
    label "prasa"
  ]
  node [
    id 67
    label "spell"
  ]
  node [
    id 68
    label "skryba"
  ]
  node [
    id 69
    label "donosi&#263;"
  ]
  node [
    id 70
    label "code"
  ]
  node [
    id 71
    label "tekst"
  ]
  node [
    id 72
    label "dysortografia"
  ]
  node [
    id 73
    label "read"
  ]
  node [
    id 74
    label "tworzy&#263;"
  ]
  node [
    id 75
    label "formu&#322;owa&#263;"
  ]
  node [
    id 76
    label "styl"
  ]
  node [
    id 77
    label "stawia&#263;"
  ]
  node [
    id 78
    label "temat"
  ]
  node [
    id 79
    label "kognicja"
  ]
  node [
    id 80
    label "idea"
  ]
  node [
    id 81
    label "szczeg&#243;&#322;"
  ]
  node [
    id 82
    label "rzecz"
  ]
  node [
    id 83
    label "wydarzenie"
  ]
  node [
    id 84
    label "przes&#322;anka"
  ]
  node [
    id 85
    label "rozprawa"
  ]
  node [
    id 86
    label "object"
  ]
  node [
    id 87
    label "proposition"
  ]
  node [
    id 88
    label "dawny"
  ]
  node [
    id 89
    label "stary"
  ]
  node [
    id 90
    label "archaicznie"
  ]
  node [
    id 91
    label "zgrzybienie"
  ]
  node [
    id 92
    label "przestarzale"
  ]
  node [
    id 93
    label "starzenie_si&#281;"
  ]
  node [
    id 94
    label "zestarzenie_si&#281;"
  ]
  node [
    id 95
    label "niedzisiejszy"
  ]
  node [
    id 96
    label "kwestowanie"
  ]
  node [
    id 97
    label "apel"
  ]
  node [
    id 98
    label "czynno&#347;&#263;"
  ]
  node [
    id 99
    label "koszyk&#243;wka"
  ]
  node [
    id 100
    label "recoil"
  ]
  node [
    id 101
    label "spotkanie"
  ]
  node [
    id 102
    label "kwestarz"
  ]
  node [
    id 103
    label "collection"
  ]
  node [
    id 104
    label "chwyt"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 104
  ]
]
