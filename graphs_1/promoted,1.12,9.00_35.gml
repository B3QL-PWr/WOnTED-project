graph [
  maxDegree 11
  minDegree 1
  meanDegree 1.9285714285714286
  density 0.07142857142857142
  graphCliqueNumber 2
  node [
    id 0
    label "zawie&#347;&#263;"
    origin "text"
  ]
  node [
    id 1
    label "hamulec"
    origin "text"
  ]
  node [
    id 2
    label "postojowy"
    origin "text"
  ]
  node [
    id 3
    label "uciec"
    origin "text"
  ]
  node [
    id 4
    label "kierowca"
    origin "text"
  ]
  node [
    id 5
    label "gdy"
    origin "text"
  ]
  node [
    id 6
    label "wysie&#347;&#263;"
    origin "text"
  ]
  node [
    id 7
    label "wzbudzi&#263;"
  ]
  node [
    id 8
    label "moderate"
  ]
  node [
    id 9
    label "szcz&#281;ka"
  ]
  node [
    id 10
    label "pow&#347;ci&#261;g"
  ]
  node [
    id 11
    label "uk&#322;ad_hamulcowy"
  ]
  node [
    id 12
    label "brake"
  ]
  node [
    id 13
    label "czuwak"
  ]
  node [
    id 14
    label "luzownik"
  ]
  node [
    id 15
    label "urz&#261;dzenie"
  ]
  node [
    id 16
    label "pojazd"
  ]
  node [
    id 17
    label "przeszkoda"
  ]
  node [
    id 18
    label "wzi&#261;&#263;"
  ]
  node [
    id 19
    label "fly"
  ]
  node [
    id 20
    label "wypierdoli&#263;"
  ]
  node [
    id 21
    label "pass"
  ]
  node [
    id 22
    label "zwia&#263;"
  ]
  node [
    id 23
    label "spieprzy&#263;"
  ]
  node [
    id 24
    label "przedosta&#263;_si&#281;"
  ]
  node [
    id 25
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 26
    label "cz&#322;owiek"
  ]
  node [
    id 27
    label "transportowiec"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 5
    target 6
  ]
]
