graph [
  maxDegree 23
  minDegree 1
  meanDegree 1.8
  density 0.046153846153846156
  graphCliqueNumber 3
  node [
    id 0
    label "stratosphere"
    origin "text"
  ]
  node [
    id 1
    label "las"
    origin "text"
  ]
  node [
    id 2
    label "vegas"
    origin "text"
  ]
  node [
    id 3
    label "chody"
  ]
  node [
    id 4
    label "dno_lasu"
  ]
  node [
    id 5
    label "obr&#281;b"
  ]
  node [
    id 6
    label "podszyt"
  ]
  node [
    id 7
    label "&#347;ci&#243;&#322;ka"
  ]
  node [
    id 8
    label "rewir"
  ]
  node [
    id 9
    label "podrost"
  ]
  node [
    id 10
    label "teren"
  ]
  node [
    id 11
    label "le&#347;nictwo"
  ]
  node [
    id 12
    label "wykarczowanie"
  ]
  node [
    id 13
    label "runo"
  ]
  node [
    id 14
    label "teren_le&#347;ny"
  ]
  node [
    id 15
    label "wykarczowa&#263;"
  ]
  node [
    id 16
    label "mn&#243;stwo"
  ]
  node [
    id 17
    label "nadle&#347;nictwo"
  ]
  node [
    id 18
    label "formacja_ro&#347;linna"
  ]
  node [
    id 19
    label "zalesienie"
  ]
  node [
    id 20
    label "karczowa&#263;"
  ]
  node [
    id 21
    label "wiatro&#322;om"
  ]
  node [
    id 22
    label "karczowanie"
  ]
  node [
    id 23
    label "driada"
  ]
  node [
    id 24
    label "Stratosphere"
  ]
  node [
    id 25
    label "lasa"
  ]
  node [
    id 26
    label "Vegas"
  ]
  node [
    id 27
    label "CN"
  ]
  node [
    id 28
    label "Tower"
  ]
  node [
    id 29
    label "Newa"
  ]
  node [
    id 30
    label "york"
  ]
  node [
    id 31
    label "Plaza"
  ]
  node [
    id 32
    label "Freemont"
  ]
  node [
    id 33
    label "street"
  ]
  node [
    id 34
    label "biga"
  ]
  node [
    id 35
    label "Shot"
  ]
  node [
    id 36
    label "ksi&#261;&#380;&#281;"
  ]
  node [
    id 37
    label "Scream"
  ]
  node [
    id 38
    label "High"
  ]
  node [
    id 39
    label "Roller"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 26
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 31
  ]
  edge [
    source 26
    target 31
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 29
  ]
  edge [
    source 30
    target 30
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 38
    target 39
  ]
]
