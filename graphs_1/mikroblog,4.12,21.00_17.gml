graph [
  maxDegree 16
  minDegree 1
  meanDegree 1.9230769230769231
  density 0.07692307692307693
  graphCliqueNumber 2
  node [
    id 0
    label "wiedzieliscie"
    origin "text"
  ]
  node [
    id 1
    label "wa&#322;&#281;sa"
    origin "text"
  ]
  node [
    id 2
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 3
    label "tytu&#322;"
    origin "text"
  ]
  node [
    id 4
    label "doktor"
    origin "text"
  ]
  node [
    id 5
    label "czyj&#347;"
  ]
  node [
    id 6
    label "m&#261;&#380;"
  ]
  node [
    id 7
    label "podtytu&#322;"
  ]
  node [
    id 8
    label "debit"
  ]
  node [
    id 9
    label "szata_graficzna"
  ]
  node [
    id 10
    label "elevation"
  ]
  node [
    id 11
    label "wyda&#263;"
  ]
  node [
    id 12
    label "nadtytu&#322;"
  ]
  node [
    id 13
    label "tytulatura"
  ]
  node [
    id 14
    label "nazwa"
  ]
  node [
    id 15
    label "wydawa&#263;"
  ]
  node [
    id 16
    label "redaktor"
  ]
  node [
    id 17
    label "druk"
  ]
  node [
    id 18
    label "mianowaniec"
  ]
  node [
    id 19
    label "poster"
  ]
  node [
    id 20
    label "publikacja"
  ]
  node [
    id 21
    label "doktorant"
  ]
  node [
    id 22
    label "doktoryzowanie_si&#281;"
  ]
  node [
    id 23
    label "pracownik"
  ]
  node [
    id 24
    label "pracownik_naukowy"
  ]
  node [
    id 25
    label "stopie&#324;_naukowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
]
