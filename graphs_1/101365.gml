graph [
  maxDegree 2
  minDegree 1
  meanDegree 1.1111111111111112
  density 0.1388888888888889
  graphCliqueNumber 2
  node [
    id 0
    label "wysokie"
    origin "text"
  ]
  node [
    id 1
    label "beskid"
    origin "text"
  ]
  node [
    id 2
    label "&#347;l&#261;sk"
    origin "text"
  ]
  node [
    id 3
    label "Beskid"
  ]
  node [
    id 4
    label "&#347;l&#261;ski"
  ]
  node [
    id 5
    label "bielski"
  ]
  node [
    id 6
    label "bia&#322;y"
  ]
  node [
    id 7
    label "dolina"
  ]
  node [
    id 8
    label "Wapienicy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 7
    target 8
  ]
]
