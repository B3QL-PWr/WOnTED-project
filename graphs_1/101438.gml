graph [
  maxDegree 76
  minDegree 1
  meanDegree 2.3
  density 0.019327731092436976
  graphCliqueNumber 7
  node [
    id 0
    label "mistrzostwa"
    origin "text"
  ]
  node [
    id 1
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 2
    label "pi&#322;ka"
    origin "text"
  ]
  node [
    id 3
    label "r&#281;czny"
    origin "text"
  ]
  node [
    id 4
    label "m&#281;&#380;czyzna"
    origin "text"
  ]
  node [
    id 5
    label "championship"
  ]
  node [
    id 6
    label "Formu&#322;a_1"
  ]
  node [
    id 7
    label "zawody"
  ]
  node [
    id 8
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 9
    label "obszar"
  ]
  node [
    id 10
    label "obiekt_naturalny"
  ]
  node [
    id 11
    label "przedmiot"
  ]
  node [
    id 12
    label "Stary_&#346;wiat"
  ]
  node [
    id 13
    label "grupa"
  ]
  node [
    id 14
    label "stw&#243;r"
  ]
  node [
    id 15
    label "biosfera"
  ]
  node [
    id 16
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 17
    label "rzecz"
  ]
  node [
    id 18
    label "magnetosfera"
  ]
  node [
    id 19
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 20
    label "environment"
  ]
  node [
    id 21
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 22
    label "geosfera"
  ]
  node [
    id 23
    label "Nowy_&#346;wiat"
  ]
  node [
    id 24
    label "planeta"
  ]
  node [
    id 25
    label "przejmowa&#263;"
  ]
  node [
    id 26
    label "litosfera"
  ]
  node [
    id 27
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 28
    label "makrokosmos"
  ]
  node [
    id 29
    label "barysfera"
  ]
  node [
    id 30
    label "biota"
  ]
  node [
    id 31
    label "p&#243;&#322;noc"
  ]
  node [
    id 32
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 33
    label "fauna"
  ]
  node [
    id 34
    label "wszechstworzenie"
  ]
  node [
    id 35
    label "geotermia"
  ]
  node [
    id 36
    label "biegun"
  ]
  node [
    id 37
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 38
    label "ekosystem"
  ]
  node [
    id 39
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 40
    label "teren"
  ]
  node [
    id 41
    label "zjawisko"
  ]
  node [
    id 42
    label "p&#243;&#322;kula"
  ]
  node [
    id 43
    label "atmosfera"
  ]
  node [
    id 44
    label "mikrokosmos"
  ]
  node [
    id 45
    label "class"
  ]
  node [
    id 46
    label "po&#322;udnie"
  ]
  node [
    id 47
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 48
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 49
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 50
    label "przejmowanie"
  ]
  node [
    id 51
    label "przestrze&#324;"
  ]
  node [
    id 52
    label "asymilowanie_si&#281;"
  ]
  node [
    id 53
    label "przej&#261;&#263;"
  ]
  node [
    id 54
    label "ekosfera"
  ]
  node [
    id 55
    label "przyroda"
  ]
  node [
    id 56
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 57
    label "ciemna_materia"
  ]
  node [
    id 58
    label "geoida"
  ]
  node [
    id 59
    label "Wsch&#243;d"
  ]
  node [
    id 60
    label "populace"
  ]
  node [
    id 61
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 62
    label "huczek"
  ]
  node [
    id 63
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 64
    label "Ziemia"
  ]
  node [
    id 65
    label "universe"
  ]
  node [
    id 66
    label "ozonosfera"
  ]
  node [
    id 67
    label "rze&#378;ba"
  ]
  node [
    id 68
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 69
    label "zagranica"
  ]
  node [
    id 70
    label "hydrosfera"
  ]
  node [
    id 71
    label "woda"
  ]
  node [
    id 72
    label "kuchnia"
  ]
  node [
    id 73
    label "przej&#281;cie"
  ]
  node [
    id 74
    label "czarna_dziura"
  ]
  node [
    id 75
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 76
    label "morze"
  ]
  node [
    id 77
    label "zagrywka"
  ]
  node [
    id 78
    label "serwowanie"
  ]
  node [
    id 79
    label "sport"
  ]
  node [
    id 80
    label "do&#347;rodkowywa&#263;"
  ]
  node [
    id 81
    label "sport_zespo&#322;owy"
  ]
  node [
    id 82
    label "odbicie"
  ]
  node [
    id 83
    label "pi&#322;a_r&#281;czna"
  ]
  node [
    id 84
    label "rzucanka"
  ]
  node [
    id 85
    label "gra"
  ]
  node [
    id 86
    label "aut"
  ]
  node [
    id 87
    label "orb"
  ]
  node [
    id 88
    label "do&#347;rodkowywanie"
  ]
  node [
    id 89
    label "zaserwowanie"
  ]
  node [
    id 90
    label "zaserwowa&#263;"
  ]
  node [
    id 91
    label "kula"
  ]
  node [
    id 92
    label "&#347;wieca"
  ]
  node [
    id 93
    label "serwowa&#263;"
  ]
  node [
    id 94
    label "musket_ball"
  ]
  node [
    id 95
    label "r&#281;cznie"
  ]
  node [
    id 96
    label "ch&#322;opina"
  ]
  node [
    id 97
    label "cz&#322;owiek"
  ]
  node [
    id 98
    label "jegomo&#347;&#263;"
  ]
  node [
    id 99
    label "bratek"
  ]
  node [
    id 100
    label "doros&#322;y"
  ]
  node [
    id 101
    label "samiec"
  ]
  node [
    id 102
    label "ojciec"
  ]
  node [
    id 103
    label "twardziel"
  ]
  node [
    id 104
    label "androlog"
  ]
  node [
    id 105
    label "pa&#324;stwo"
  ]
  node [
    id 106
    label "m&#261;&#380;"
  ]
  node [
    id 107
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 108
    label "andropauza"
  ]
  node [
    id 109
    label "mistrzostwo"
  ]
  node [
    id 110
    label "wyspa"
  ]
  node [
    id 111
    label "1978"
  ]
  node [
    id 112
    label "niemiecki"
  ]
  node [
    id 113
    label "republika"
  ]
  node [
    id 114
    label "demokratyczny"
  ]
  node [
    id 115
    label "federalny"
  ]
  node [
    id 116
    label "niemiec"
  ]
  node [
    id 117
    label "zwi&#261;zka"
  ]
  node [
    id 118
    label "socjalistyczny"
  ]
  node [
    id 119
    label "radziecki"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 109
    target 110
  ]
  edge [
    source 109
    target 111
  ]
  edge [
    source 110
    target 111
  ]
  edge [
    source 112
    target 113
  ]
  edge [
    source 112
    target 114
  ]
  edge [
    source 113
    target 114
  ]
  edge [
    source 113
    target 115
  ]
  edge [
    source 113
    target 116
  ]
  edge [
    source 113
    target 117
  ]
  edge [
    source 113
    target 118
  ]
  edge [
    source 113
    target 119
  ]
  edge [
    source 115
    target 116
  ]
  edge [
    source 117
    target 118
  ]
  edge [
    source 117
    target 119
  ]
  edge [
    source 118
    target 119
  ]
]
