graph [
  maxDegree 23
  minDegree 1
  meanDegree 1.956989247311828
  density 0.021271622253389435
  graphCliqueNumber 2
  node [
    id 0
    label "polska"
    origin "text"
  ]
  node [
    id 1
    label "wikipedia"
    origin "text"
  ]
  node [
    id 2
    label "dodanie"
    origin "text"
  ]
  node [
    id 3
    label "artyku&#322;"
    origin "text"
  ]
  node [
    id 4
    label "miejscowo&#347;&#263;"
    origin "text"
  ]
  node [
    id 5
    label "konrad"
    origin "text"
  ]
  node [
    id 6
    label "liczy&#263;"
    origin "text"
  ]
  node [
    id 7
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 8
    label "tysi&#261;c"
    origin "text"
  ]
  node [
    id 9
    label "has&#322;o"
    origin "text"
  ]
  node [
    id 10
    label "dop&#322;acenie"
  ]
  node [
    id 11
    label "do&#347;wietlenie"
  ]
  node [
    id 12
    label "wym&#243;wienie"
  ]
  node [
    id 13
    label "dokupienie"
  ]
  node [
    id 14
    label "summation"
  ]
  node [
    id 15
    label "do&#322;&#261;czenie"
  ]
  node [
    id 16
    label "rozdzielno&#347;&#263;_mno&#380;enia_wzgl&#281;dem_dodawania"
  ]
  node [
    id 17
    label "policzenie"
  ]
  node [
    id 18
    label "dzia&#322;anie_arytmetyczne"
  ]
  node [
    id 19
    label "&#322;&#261;czno&#347;&#263;"
  ]
  node [
    id 20
    label "addition"
  ]
  node [
    id 21
    label "dokument"
  ]
  node [
    id 22
    label "towar"
  ]
  node [
    id 23
    label "nag&#322;&#243;wek"
  ]
  node [
    id 24
    label "znak_j&#281;zykowy"
  ]
  node [
    id 25
    label "wyr&#243;b"
  ]
  node [
    id 26
    label "blok"
  ]
  node [
    id 27
    label "line"
  ]
  node [
    id 28
    label "paragraf"
  ]
  node [
    id 29
    label "rodzajnik"
  ]
  node [
    id 30
    label "prawda"
  ]
  node [
    id 31
    label "szkic"
  ]
  node [
    id 32
    label "tekst"
  ]
  node [
    id 33
    label "fragment"
  ]
  node [
    id 34
    label "Aurignac"
  ]
  node [
    id 35
    label "osiedle"
  ]
  node [
    id 36
    label "Levallois-Perret"
  ]
  node [
    id 37
    label "Sabaudia"
  ]
  node [
    id 38
    label "Opat&#243;wek"
  ]
  node [
    id 39
    label "Saint-Acheul"
  ]
  node [
    id 40
    label "Boulogne"
  ]
  node [
    id 41
    label "Cecora"
  ]
  node [
    id 42
    label "report"
  ]
  node [
    id 43
    label "dodawa&#263;"
  ]
  node [
    id 44
    label "wymienia&#263;"
  ]
  node [
    id 45
    label "okre&#347;la&#263;"
  ]
  node [
    id 46
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 47
    label "dyskalkulia"
  ]
  node [
    id 48
    label "wynagrodzenie"
  ]
  node [
    id 49
    label "admit"
  ]
  node [
    id 50
    label "osi&#261;ga&#263;"
  ]
  node [
    id 51
    label "wyznacza&#263;"
  ]
  node [
    id 52
    label "posiada&#263;"
  ]
  node [
    id 53
    label "mierzy&#263;"
  ]
  node [
    id 54
    label "odlicza&#263;"
  ]
  node [
    id 55
    label "bra&#263;"
  ]
  node [
    id 56
    label "wycenia&#263;"
  ]
  node [
    id 57
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 58
    label "rachowa&#263;"
  ]
  node [
    id 59
    label "tell"
  ]
  node [
    id 60
    label "uwzgl&#281;dnia&#263;"
  ]
  node [
    id 61
    label "policza&#263;"
  ]
  node [
    id 62
    label "count"
  ]
  node [
    id 63
    label "tauzen"
  ]
  node [
    id 64
    label "musik"
  ]
  node [
    id 65
    label "molarity"
  ]
  node [
    id 66
    label "licytacja"
  ]
  node [
    id 67
    label "patyk"
  ]
  node [
    id 68
    label "liczba"
  ]
  node [
    id 69
    label "gra_w_karty"
  ]
  node [
    id 70
    label "kwota"
  ]
  node [
    id 71
    label "ochrona"
  ]
  node [
    id 72
    label "sztuka_dla_sztuki"
  ]
  node [
    id 73
    label "dost&#281;p"
  ]
  node [
    id 74
    label "przes&#322;anie"
  ]
  node [
    id 75
    label "definicja"
  ]
  node [
    id 76
    label "idea"
  ]
  node [
    id 77
    label "sygna&#322;"
  ]
  node [
    id 78
    label "kwalifikator"
  ]
  node [
    id 79
    label "wyra&#380;enie"
  ]
  node [
    id 80
    label "powiedzenie"
  ]
  node [
    id 81
    label "artyku&#322;_has&#322;owy"
  ]
  node [
    id 82
    label "guide_word"
  ]
  node [
    id 83
    label "rozwi&#261;zanie"
  ]
  node [
    id 84
    label "solicitation"
  ]
  node [
    id 85
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 86
    label "kod"
  ]
  node [
    id 87
    label "pozycja"
  ]
  node [
    id 88
    label "leksem"
  ]
  node [
    id 89
    label "polski"
  ]
  node [
    id 90
    label "Wikipedia"
  ]
  node [
    id 91
    label "Heather"
  ]
  node [
    id 92
    label "Morrison"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 89
    target 90
  ]
  edge [
    source 91
    target 92
  ]
]
