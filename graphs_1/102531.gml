graph [
  maxDegree 39
  minDegree 1
  meanDegree 2.0246913580246915
  density 0.012575722720650257
  graphCliqueNumber 3
  node [
    id 0
    label "zaprasza&#263;"
    origin "text"
  ]
  node [
    id 1
    label "nauczyciel"
    origin "text"
  ]
  node [
    id 2
    label "geografia"
    origin "text"
  ]
  node [
    id 3
    label "wolontariusz"
    origin "text"
  ]
  node [
    id 4
    label "wszystek"
    origin "text"
  ]
  node [
    id 5
    label "zainteresowana"
    origin "text"
  ]
  node [
    id 6
    label "osoba"
    origin "text"
  ]
  node [
    id 7
    label "wsp&#243;&#322;praca"
    origin "text"
  ]
  node [
    id 8
    label "przy"
    origin "text"
  ]
  node [
    id 9
    label "pisanie"
    origin "text"
  ]
  node [
    id 10
    label "podr&#281;cznik"
    origin "text"
  ]
  node [
    id 11
    label "dla"
    origin "text"
  ]
  node [
    id 12
    label "gimnazjum"
    origin "text"
  ]
  node [
    id 13
    label "w&#322;&#261;czy&#263;"
    origin "text"
  ]
  node [
    id 14
    label "si&#281;"
    origin "text"
  ]
  node [
    id 15
    label "prowadzony"
    origin "text"
  ]
  node [
    id 16
    label "przez"
    origin "text"
  ]
  node [
    id 17
    label "nasa"
    origin "text"
  ]
  node [
    id 18
    label "praca"
    origin "text"
  ]
  node [
    id 19
    label "invite"
  ]
  node [
    id 20
    label "ask"
  ]
  node [
    id 21
    label "oferowa&#263;"
  ]
  node [
    id 22
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 23
    label "profesor"
  ]
  node [
    id 24
    label "kszta&#322;ciciel"
  ]
  node [
    id 25
    label "szkolnik"
  ]
  node [
    id 26
    label "preceptor"
  ]
  node [
    id 27
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 28
    label "pedagog"
  ]
  node [
    id 29
    label "popularyzator"
  ]
  node [
    id 30
    label "belfer"
  ]
  node [
    id 31
    label "nauki_o_Ziemi"
  ]
  node [
    id 32
    label "przedmiot"
  ]
  node [
    id 33
    label "geography"
  ]
  node [
    id 34
    label "antropogeografia"
  ]
  node [
    id 35
    label "zoogeografia"
  ]
  node [
    id 36
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 37
    label "jeografia"
  ]
  node [
    id 38
    label "gegra"
  ]
  node [
    id 39
    label "fitogeografia"
  ]
  node [
    id 40
    label "homologia"
  ]
  node [
    id 41
    label "biogeografia"
  ]
  node [
    id 42
    label "hierotopografia"
  ]
  node [
    id 43
    label "oceanologia"
  ]
  node [
    id 44
    label "topografia"
  ]
  node [
    id 45
    label "kartografia"
  ]
  node [
    id 46
    label "krajoznawstwo"
  ]
  node [
    id 47
    label "warunki"
  ]
  node [
    id 48
    label "geografia_regionalna"
  ]
  node [
    id 49
    label "geografia_lingwistyczna"
  ]
  node [
    id 50
    label "geografia_fizyczna"
  ]
  node [
    id 51
    label "klimatologia"
  ]
  node [
    id 52
    label "kierunek"
  ]
  node [
    id 53
    label "turyzm"
  ]
  node [
    id 54
    label "geografia_ekonomiczna"
  ]
  node [
    id 55
    label "astrografia"
  ]
  node [
    id 56
    label "nauka_przyrodnicza"
  ]
  node [
    id 57
    label "polarnictwo"
  ]
  node [
    id 58
    label "sedymentologia"
  ]
  node [
    id 59
    label "ochotnik"
  ]
  node [
    id 60
    label "spo&#322;ecznik"
  ]
  node [
    id 61
    label "praktykant"
  ]
  node [
    id 62
    label "ca&#322;y"
  ]
  node [
    id 63
    label "Zgredek"
  ]
  node [
    id 64
    label "kategoria_gramatyczna"
  ]
  node [
    id 65
    label "Casanova"
  ]
  node [
    id 66
    label "Don_Juan"
  ]
  node [
    id 67
    label "Gargantua"
  ]
  node [
    id 68
    label "Faust"
  ]
  node [
    id 69
    label "profanum"
  ]
  node [
    id 70
    label "Chocho&#322;"
  ]
  node [
    id 71
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 72
    label "koniugacja"
  ]
  node [
    id 73
    label "Winnetou"
  ]
  node [
    id 74
    label "Dwukwiat"
  ]
  node [
    id 75
    label "homo_sapiens"
  ]
  node [
    id 76
    label "Edyp"
  ]
  node [
    id 77
    label "Herkules_Poirot"
  ]
  node [
    id 78
    label "ludzko&#347;&#263;"
  ]
  node [
    id 79
    label "mikrokosmos"
  ]
  node [
    id 80
    label "person"
  ]
  node [
    id 81
    label "Sherlock_Holmes"
  ]
  node [
    id 82
    label "portrecista"
  ]
  node [
    id 83
    label "Szwejk"
  ]
  node [
    id 84
    label "Hamlet"
  ]
  node [
    id 85
    label "duch"
  ]
  node [
    id 86
    label "g&#322;owa"
  ]
  node [
    id 87
    label "oddzia&#322;ywanie"
  ]
  node [
    id 88
    label "Quasimodo"
  ]
  node [
    id 89
    label "Dulcynea"
  ]
  node [
    id 90
    label "Don_Kiszot"
  ]
  node [
    id 91
    label "Wallenrod"
  ]
  node [
    id 92
    label "Plastu&#347;"
  ]
  node [
    id 93
    label "Harry_Potter"
  ]
  node [
    id 94
    label "figura"
  ]
  node [
    id 95
    label "parali&#380;owa&#263;"
  ]
  node [
    id 96
    label "istota"
  ]
  node [
    id 97
    label "Werter"
  ]
  node [
    id 98
    label "antropochoria"
  ]
  node [
    id 99
    label "posta&#263;"
  ]
  node [
    id 100
    label "wsp&#243;&#322;pracownictwo"
  ]
  node [
    id 101
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 102
    label "przypisywanie"
  ]
  node [
    id 103
    label "popisanie"
  ]
  node [
    id 104
    label "tworzenie"
  ]
  node [
    id 105
    label "przepisanie"
  ]
  node [
    id 106
    label "zamazywanie"
  ]
  node [
    id 107
    label "t&#322;uczenie"
  ]
  node [
    id 108
    label "enchantment"
  ]
  node [
    id 109
    label "wci&#261;ganie"
  ]
  node [
    id 110
    label "zamazanie"
  ]
  node [
    id 111
    label "pisywanie"
  ]
  node [
    id 112
    label "dopisywanie"
  ]
  node [
    id 113
    label "ozdabianie"
  ]
  node [
    id 114
    label "odpisywanie"
  ]
  node [
    id 115
    label "kre&#347;lenie"
  ]
  node [
    id 116
    label "writing"
  ]
  node [
    id 117
    label "formu&#322;owanie"
  ]
  node [
    id 118
    label "dysortografia"
  ]
  node [
    id 119
    label "donoszenie"
  ]
  node [
    id 120
    label "dysgrafia"
  ]
  node [
    id 121
    label "stawianie"
  ]
  node [
    id 122
    label "wyprawka"
  ]
  node [
    id 123
    label "pomoc_naukowa"
  ]
  node [
    id 124
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 125
    label "miejsce"
  ]
  node [
    id 126
    label "szko&#322;a"
  ]
  node [
    id 127
    label "szko&#322;a_ponadpodstawowa"
  ]
  node [
    id 128
    label "zacz&#261;&#263;"
  ]
  node [
    id 129
    label "nastawi&#263;"
  ]
  node [
    id 130
    label "prosecute"
  ]
  node [
    id 131
    label "odkr&#281;ci&#263;_kurek"
  ]
  node [
    id 132
    label "impersonate"
  ]
  node [
    id 133
    label "umie&#347;ci&#263;"
  ]
  node [
    id 134
    label "obejrze&#263;"
  ]
  node [
    id 135
    label "draw"
  ]
  node [
    id 136
    label "incorporate"
  ]
  node [
    id 137
    label "uruchomi&#263;"
  ]
  node [
    id 138
    label "dokoptowa&#263;"
  ]
  node [
    id 139
    label "stosunek_pracy"
  ]
  node [
    id 140
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 141
    label "benedykty&#324;ski"
  ]
  node [
    id 142
    label "pracowanie"
  ]
  node [
    id 143
    label "zaw&#243;d"
  ]
  node [
    id 144
    label "kierownictwo"
  ]
  node [
    id 145
    label "zmiana"
  ]
  node [
    id 146
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 147
    label "wytw&#243;r"
  ]
  node [
    id 148
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 149
    label "tynkarski"
  ]
  node [
    id 150
    label "czynnik_produkcji"
  ]
  node [
    id 151
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 152
    label "zobowi&#261;zanie"
  ]
  node [
    id 153
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 154
    label "czynno&#347;&#263;"
  ]
  node [
    id 155
    label "tyrka"
  ]
  node [
    id 156
    label "pracowa&#263;"
  ]
  node [
    id 157
    label "siedziba"
  ]
  node [
    id 158
    label "poda&#380;_pracy"
  ]
  node [
    id 159
    label "zak&#322;ad"
  ]
  node [
    id 160
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 161
    label "najem"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 102
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 104
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 9
    target 116
  ]
  edge [
    source 9
    target 117
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 9
    target 119
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 128
  ]
  edge [
    source 13
    target 129
  ]
  edge [
    source 13
    target 130
  ]
  edge [
    source 13
    target 131
  ]
  edge [
    source 13
    target 132
  ]
  edge [
    source 13
    target 133
  ]
  edge [
    source 13
    target 134
  ]
  edge [
    source 13
    target 135
  ]
  edge [
    source 13
    target 136
  ]
  edge [
    source 13
    target 137
  ]
  edge [
    source 13
    target 138
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 139
  ]
  edge [
    source 18
    target 140
  ]
  edge [
    source 18
    target 141
  ]
  edge [
    source 18
    target 142
  ]
  edge [
    source 18
    target 143
  ]
  edge [
    source 18
    target 144
  ]
  edge [
    source 18
    target 145
  ]
  edge [
    source 18
    target 146
  ]
  edge [
    source 18
    target 147
  ]
  edge [
    source 18
    target 148
  ]
  edge [
    source 18
    target 149
  ]
  edge [
    source 18
    target 150
  ]
  edge [
    source 18
    target 151
  ]
  edge [
    source 18
    target 152
  ]
  edge [
    source 18
    target 153
  ]
  edge [
    source 18
    target 154
  ]
  edge [
    source 18
    target 155
  ]
  edge [
    source 18
    target 156
  ]
  edge [
    source 18
    target 157
  ]
  edge [
    source 18
    target 158
  ]
  edge [
    source 18
    target 125
  ]
  edge [
    source 18
    target 159
  ]
  edge [
    source 18
    target 160
  ]
  edge [
    source 18
    target 161
  ]
]
