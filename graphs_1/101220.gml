graph [
  maxDegree 2
  minDegree 0
  meanDegree 1
  density 0.06666666666666667
  graphCliqueNumber 2
  node [
    id 0
    label "wojnasy"
    origin "text"
  ]
  node [
    id 1
    label "warmi&#324;sko"
  ]
  node [
    id 2
    label "mazurski"
  ]
  node [
    id 3
    label "Krzysztofa"
  ]
  node [
    id 4
    label "Glaubitz"
  ]
  node [
    id 5
    label "Micha&#322;"
  ]
  node [
    id 6
    label "nurkowy"
  ]
  node [
    id 7
    label "wojtek"
  ]
  node [
    id 8
    label "Wojnasowi"
  ]
  node [
    id 9
    label "Krystyna"
  ]
  node [
    id 10
    label "lacha"
  ]
  node [
    id 11
    label "uniwersytet"
  ]
  node [
    id 12
    label "warszawski"
  ]
  node [
    id 13
    label "szyrm"
  ]
  node [
    id 14
    label "wielki"
  ]
  node [
    id 15
    label "brytania"
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 13
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 14
    target 15
  ]
]
