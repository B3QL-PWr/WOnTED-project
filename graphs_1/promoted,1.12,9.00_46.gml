graph [
  maxDegree 28
  minDegree 1
  meanDegree 2.045977011494253
  density 0.023790430366212242
  graphCliqueNumber 3
  node [
    id 0
    label "rabin"
    origin "text"
  ]
  node [
    id 1
    label "jay"
    origin "text"
  ]
  node [
    id 2
    label "michaelson"
    origin "text"
  ]
  node [
    id 3
    label "udowadnia&#263;"
    origin "text"
  ]
  node [
    id 4
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 5
    label "my&#347;l&#261;cy"
    origin "text"
  ]
  node [
    id 6
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 7
    label "interesuj&#261;cy"
    origin "text"
  ]
  node [
    id 8
    label "si&#281;"
    origin "text"
  ]
  node [
    id 9
    label "geopolityka"
    origin "text"
  ]
  node [
    id 10
    label "by&#263;"
    origin "text"
  ]
  node [
    id 11
    label "antysemita"
    origin "text"
  ]
  node [
    id 12
    label "nawet"
    origin "text"
  ]
  node [
    id 13
    label "je&#347;li"
    origin "text"
  ]
  node [
    id 14
    label "kocha&#263;"
    origin "text"
  ]
  node [
    id 15
    label "&#380;yd"
    origin "text"
  ]
  node [
    id 16
    label "duchowny"
  ]
  node [
    id 17
    label "judaizm"
  ]
  node [
    id 18
    label "Sabataj_Cwi"
  ]
  node [
    id 19
    label "uzasadnia&#263;"
  ]
  node [
    id 20
    label "testify"
  ]
  node [
    id 21
    label "court"
  ]
  node [
    id 22
    label "jaki&#347;"
  ]
  node [
    id 23
    label "przytomny"
  ]
  node [
    id 24
    label "inteligentny"
  ]
  node [
    id 25
    label "rozumnie"
  ]
  node [
    id 26
    label "asymilowa&#263;"
  ]
  node [
    id 27
    label "wapniak"
  ]
  node [
    id 28
    label "dwun&#243;g"
  ]
  node [
    id 29
    label "polifag"
  ]
  node [
    id 30
    label "wz&#243;r"
  ]
  node [
    id 31
    label "profanum"
  ]
  node [
    id 32
    label "hominid"
  ]
  node [
    id 33
    label "homo_sapiens"
  ]
  node [
    id 34
    label "nasada"
  ]
  node [
    id 35
    label "podw&#322;adny"
  ]
  node [
    id 36
    label "ludzko&#347;&#263;"
  ]
  node [
    id 37
    label "os&#322;abianie"
  ]
  node [
    id 38
    label "mikrokosmos"
  ]
  node [
    id 39
    label "portrecista"
  ]
  node [
    id 40
    label "duch"
  ]
  node [
    id 41
    label "oddzia&#322;ywanie"
  ]
  node [
    id 42
    label "g&#322;owa"
  ]
  node [
    id 43
    label "asymilowanie"
  ]
  node [
    id 44
    label "osoba"
  ]
  node [
    id 45
    label "os&#322;abia&#263;"
  ]
  node [
    id 46
    label "figura"
  ]
  node [
    id 47
    label "Adam"
  ]
  node [
    id 48
    label "senior"
  ]
  node [
    id 49
    label "antropochoria"
  ]
  node [
    id 50
    label "posta&#263;"
  ]
  node [
    id 51
    label "swoisty"
  ]
  node [
    id 52
    label "atrakcyjny"
  ]
  node [
    id 53
    label "ciekawie"
  ]
  node [
    id 54
    label "interesuj&#261;co"
  ]
  node [
    id 55
    label "dziwny"
  ]
  node [
    id 56
    label "astropolityka"
  ]
  node [
    id 57
    label "geostrategia"
  ]
  node [
    id 58
    label "nauka"
  ]
  node [
    id 59
    label "si&#281;ga&#263;"
  ]
  node [
    id 60
    label "trwa&#263;"
  ]
  node [
    id 61
    label "obecno&#347;&#263;"
  ]
  node [
    id 62
    label "stan"
  ]
  node [
    id 63
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 64
    label "stand"
  ]
  node [
    id 65
    label "mie&#263;_miejsce"
  ]
  node [
    id 66
    label "uczestniczy&#263;"
  ]
  node [
    id 67
    label "chodzi&#263;"
  ]
  node [
    id 68
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 69
    label "equal"
  ]
  node [
    id 70
    label "zwolennik"
  ]
  node [
    id 71
    label "like"
  ]
  node [
    id 72
    label "czu&#263;"
  ]
  node [
    id 73
    label "chowa&#263;"
  ]
  node [
    id 74
    label "mi&#322;owa&#263;"
  ]
  node [
    id 75
    label "chciwiec"
  ]
  node [
    id 76
    label "judenrat"
  ]
  node [
    id 77
    label "materialista"
  ]
  node [
    id 78
    label "sk&#261;piarz"
  ]
  node [
    id 79
    label "wyznawca"
  ]
  node [
    id 80
    label "&#379;ydziak"
  ]
  node [
    id 81
    label "sk&#261;py"
  ]
  node [
    id 82
    label "szmonces"
  ]
  node [
    id 83
    label "monoteista"
  ]
  node [
    id 84
    label "mosiek"
  ]
  node [
    id 85
    label "Jay"
  ]
  node [
    id 86
    label "Michaelson"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 11
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 56
  ]
  edge [
    source 9
    target 57
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 10
    target 60
  ]
  edge [
    source 10
    target 61
  ]
  edge [
    source 10
    target 62
  ]
  edge [
    source 10
    target 63
  ]
  edge [
    source 10
    target 64
  ]
  edge [
    source 10
    target 65
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 10
    target 69
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 70
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 71
  ]
  edge [
    source 14
    target 72
  ]
  edge [
    source 14
    target 73
  ]
  edge [
    source 14
    target 74
  ]
  edge [
    source 15
    target 75
  ]
  edge [
    source 15
    target 76
  ]
  edge [
    source 15
    target 77
  ]
  edge [
    source 15
    target 78
  ]
  edge [
    source 15
    target 79
  ]
  edge [
    source 15
    target 80
  ]
  edge [
    source 15
    target 81
  ]
  edge [
    source 15
    target 82
  ]
  edge [
    source 15
    target 83
  ]
  edge [
    source 15
    target 84
  ]
  edge [
    source 85
    target 86
  ]
]
