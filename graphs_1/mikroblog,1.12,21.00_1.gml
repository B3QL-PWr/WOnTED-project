graph [
  maxDegree 30
  minDegree 1
  meanDegree 1.9852941176470589
  density 0.014705882352941176
  graphCliqueNumber 2
  node [
    id 0
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 1
    label "wczorajszy"
    origin "text"
  ]
  node [
    id 2
    label "pewne"
    origin "text"
  ]
  node [
    id 3
    label "okoliczno&#347;&#263;"
    origin "text"
  ]
  node [
    id 4
    label "dowiedzie&#263;by&#263;"
    origin "text"
  ]
  node [
    id 5
    label "si&#281;"
    origin "text"
  ]
  node [
    id 6
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 7
    label "wujek"
    origin "text"
  ]
  node [
    id 8
    label "g&#243;rnik"
    origin "text"
  ]
  node [
    id 9
    label "dom"
    origin "text"
  ]
  node [
    id 10
    label "ogrzewa&#263;"
    origin "text"
  ]
  node [
    id 11
    label "mi&#281;dzy"
    origin "text"
  ]
  node [
    id 12
    label "inny"
    origin "text"
  ]
  node [
    id 13
    label "stara"
    origin "text"
  ]
  node [
    id 14
    label "mebel"
    origin "text"
  ]
  node [
    id 15
    label "zbiera&#263;"
    origin "text"
  ]
  node [
    id 16
    label "okoliczny"
    origin "text"
  ]
  node [
    id 17
    label "&#347;mietnik"
    origin "text"
  ]
  node [
    id 18
    label "s&#322;o&#324;ce"
  ]
  node [
    id 19
    label "czynienie_si&#281;"
  ]
  node [
    id 20
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 21
    label "czas"
  ]
  node [
    id 22
    label "long_time"
  ]
  node [
    id 23
    label "przedpo&#322;udnie"
  ]
  node [
    id 24
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 25
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 26
    label "tydzie&#324;"
  ]
  node [
    id 27
    label "godzina"
  ]
  node [
    id 28
    label "t&#322;usty_czwartek"
  ]
  node [
    id 29
    label "wsta&#263;"
  ]
  node [
    id 30
    label "day"
  ]
  node [
    id 31
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 32
    label "przedwiecz&#243;r"
  ]
  node [
    id 33
    label "Sylwester"
  ]
  node [
    id 34
    label "po&#322;udnie"
  ]
  node [
    id 35
    label "wzej&#347;cie"
  ]
  node [
    id 36
    label "podwiecz&#243;r"
  ]
  node [
    id 37
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 38
    label "rano"
  ]
  node [
    id 39
    label "termin"
  ]
  node [
    id 40
    label "ranek"
  ]
  node [
    id 41
    label "doba"
  ]
  node [
    id 42
    label "wiecz&#243;r"
  ]
  node [
    id 43
    label "walentynki"
  ]
  node [
    id 44
    label "popo&#322;udnie"
  ]
  node [
    id 45
    label "noc"
  ]
  node [
    id 46
    label "wstanie"
  ]
  node [
    id 47
    label "dawny"
  ]
  node [
    id 48
    label "stary"
  ]
  node [
    id 49
    label "archaicznie"
  ]
  node [
    id 50
    label "zgrzybienie"
  ]
  node [
    id 51
    label "przestarzale"
  ]
  node [
    id 52
    label "starzenie_si&#281;"
  ]
  node [
    id 53
    label "zestarzenie_si&#281;"
  ]
  node [
    id 54
    label "niedzisiejszy"
  ]
  node [
    id 55
    label "wydarzenie"
  ]
  node [
    id 56
    label "sk&#322;adnik"
  ]
  node [
    id 57
    label "warunki"
  ]
  node [
    id 58
    label "sytuacja"
  ]
  node [
    id 59
    label "czyj&#347;"
  ]
  node [
    id 60
    label "m&#261;&#380;"
  ]
  node [
    id 61
    label "przyjaciel_domu"
  ]
  node [
    id 62
    label "krewny"
  ]
  node [
    id 63
    label "wujo"
  ]
  node [
    id 64
    label "banknot"
  ]
  node [
    id 65
    label "hawierz"
  ]
  node [
    id 66
    label "robotnik"
  ]
  node [
    id 67
    label "pi&#281;&#263;setz&#322;ot&#243;wka"
  ]
  node [
    id 68
    label "wydobywca"
  ]
  node [
    id 69
    label "rudnik"
  ]
  node [
    id 70
    label "kopalnia"
  ]
  node [
    id 71
    label "Barb&#243;rka"
  ]
  node [
    id 72
    label "garderoba"
  ]
  node [
    id 73
    label "wiecha"
  ]
  node [
    id 74
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 75
    label "grupa"
  ]
  node [
    id 76
    label "budynek"
  ]
  node [
    id 77
    label "fratria"
  ]
  node [
    id 78
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 79
    label "poj&#281;cie"
  ]
  node [
    id 80
    label "rodzina"
  ]
  node [
    id 81
    label "substancja_mieszkaniowa"
  ]
  node [
    id 82
    label "instytucja"
  ]
  node [
    id 83
    label "dom_rodzinny"
  ]
  node [
    id 84
    label "stead"
  ]
  node [
    id 85
    label "siedziba"
  ]
  node [
    id 86
    label "heat"
  ]
  node [
    id 87
    label "podnosi&#263;"
  ]
  node [
    id 88
    label "kolejny"
  ]
  node [
    id 89
    label "inaczej"
  ]
  node [
    id 90
    label "r&#243;&#380;ny"
  ]
  node [
    id 91
    label "inszy"
  ]
  node [
    id 92
    label "osobno"
  ]
  node [
    id 93
    label "matka"
  ]
  node [
    id 94
    label "kobieta"
  ]
  node [
    id 95
    label "partnerka"
  ]
  node [
    id 96
    label "&#380;ona"
  ]
  node [
    id 97
    label "starzy"
  ]
  node [
    id 98
    label "nadstawa"
  ]
  node [
    id 99
    label "umeblowanie"
  ]
  node [
    id 100
    label "obudowywa&#263;"
  ]
  node [
    id 101
    label "obudowywanie"
  ]
  node [
    id 102
    label "przeszklenie"
  ]
  node [
    id 103
    label "obudowanie"
  ]
  node [
    id 104
    label "sprz&#281;t"
  ]
  node [
    id 105
    label "element_wyposa&#380;enia"
  ]
  node [
    id 106
    label "obudowa&#263;"
  ]
  node [
    id 107
    label "ramiak"
  ]
  node [
    id 108
    label "gzyms"
  ]
  node [
    id 109
    label "consolidate"
  ]
  node [
    id 110
    label "przejmowa&#263;"
  ]
  node [
    id 111
    label "dostawa&#263;"
  ]
  node [
    id 112
    label "mie&#263;_miejsce"
  ]
  node [
    id 113
    label "sprz&#261;ta&#263;"
  ]
  node [
    id 114
    label "oszcz&#281;dza&#263;"
  ]
  node [
    id 115
    label "pozyskiwa&#263;"
  ]
  node [
    id 116
    label "meet"
  ]
  node [
    id 117
    label "congregate"
  ]
  node [
    id 118
    label "poci&#261;ga&#263;"
  ]
  node [
    id 119
    label "robi&#263;"
  ]
  node [
    id 120
    label "uk&#322;ada&#263;"
  ]
  node [
    id 121
    label "umieszcza&#263;"
  ]
  node [
    id 122
    label "wzbiera&#263;"
  ]
  node [
    id 123
    label "gromadzi&#263;"
  ]
  node [
    id 124
    label "powodowa&#263;"
  ]
  node [
    id 125
    label "bra&#263;"
  ]
  node [
    id 126
    label "nat&#281;&#380;a&#263;"
  ]
  node [
    id 127
    label "tutejszy"
  ]
  node [
    id 128
    label "okolicznie"
  ]
  node [
    id 129
    label "okoliczno&#347;ciowy"
  ]
  node [
    id 130
    label "sytuacyjny"
  ]
  node [
    id 131
    label "pobliski"
  ]
  node [
    id 132
    label "&#347;miecisko"
  ]
  node [
    id 133
    label "pojemnik"
  ]
  node [
    id 134
    label "zbiornik"
  ]
  node [
    id 135
    label "sk&#322;adowisko"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 12
    target 89
  ]
  edge [
    source 12
    target 90
  ]
  edge [
    source 12
    target 91
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 93
  ]
  edge [
    source 13
    target 94
  ]
  edge [
    source 13
    target 95
  ]
  edge [
    source 13
    target 96
  ]
  edge [
    source 13
    target 97
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 98
  ]
  edge [
    source 14
    target 99
  ]
  edge [
    source 14
    target 100
  ]
  edge [
    source 14
    target 101
  ]
  edge [
    source 14
    target 102
  ]
  edge [
    source 14
    target 103
  ]
  edge [
    source 14
    target 104
  ]
  edge [
    source 14
    target 105
  ]
  edge [
    source 14
    target 106
  ]
  edge [
    source 14
    target 107
  ]
  edge [
    source 14
    target 108
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 109
  ]
  edge [
    source 15
    target 110
  ]
  edge [
    source 15
    target 111
  ]
  edge [
    source 15
    target 112
  ]
  edge [
    source 15
    target 113
  ]
  edge [
    source 15
    target 114
  ]
  edge [
    source 15
    target 115
  ]
  edge [
    source 15
    target 116
  ]
  edge [
    source 15
    target 117
  ]
  edge [
    source 15
    target 118
  ]
  edge [
    source 15
    target 119
  ]
  edge [
    source 15
    target 120
  ]
  edge [
    source 15
    target 121
  ]
  edge [
    source 15
    target 122
  ]
  edge [
    source 15
    target 123
  ]
  edge [
    source 15
    target 124
  ]
  edge [
    source 15
    target 125
  ]
  edge [
    source 15
    target 126
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 127
  ]
  edge [
    source 16
    target 128
  ]
  edge [
    source 16
    target 129
  ]
  edge [
    source 16
    target 130
  ]
  edge [
    source 16
    target 131
  ]
  edge [
    source 17
    target 132
  ]
  edge [
    source 17
    target 133
  ]
  edge [
    source 17
    target 134
  ]
  edge [
    source 17
    target 135
  ]
]
