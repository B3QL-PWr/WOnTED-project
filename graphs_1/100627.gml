graph [
  maxDegree 25
  minDegree 1
  meanDegree 3.1780821917808217
  density 0.0441400304414003
  graphCliqueNumber 10
  node [
    id 0
    label "mistrzostwa"
    origin "text"
  ]
  node [
    id 1
    label "wojew&#243;dztwo"
    origin "text"
  ]
  node [
    id 2
    label "&#322;&#243;dzkie"
    origin "text"
  ]
  node [
    id 3
    label "junior"
    origin "text"
  ]
  node [
    id 4
    label "m&#322;odsza"
    origin "text"
  ]
  node [
    id 5
    label "m&#322;odzik"
    origin "text"
  ]
  node [
    id 6
    label "bryd&#380;"
    origin "text"
  ]
  node [
    id 7
    label "sportowy"
    origin "text"
  ]
  node [
    id 8
    label "redaktor"
    origin "text"
  ]
  node [
    id 9
    label "micha&#322;"
    origin "text"
  ]
  node [
    id 10
    label "kowalski"
    origin "text"
  ]
  node [
    id 11
    label "championship"
  ]
  node [
    id 12
    label "Formu&#322;a_1"
  ]
  node [
    id 13
    label "zawody"
  ]
  node [
    id 14
    label "jednostka_administracyjna"
  ]
  node [
    id 15
    label "makroregion"
  ]
  node [
    id 16
    label "powiat"
  ]
  node [
    id 17
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 18
    label "mikroregion"
  ]
  node [
    id 19
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 20
    label "pa&#324;stwo"
  ]
  node [
    id 21
    label "cz&#322;owiek"
  ]
  node [
    id 22
    label "potomek"
  ]
  node [
    id 23
    label "zawodnik"
  ]
  node [
    id 24
    label "m&#322;odzieniec"
  ]
  node [
    id 25
    label "organizm"
  ]
  node [
    id 26
    label "stopie&#324;_harcerski"
  ]
  node [
    id 27
    label "m&#322;ode"
  ]
  node [
    id 28
    label "harcerz"
  ]
  node [
    id 29
    label "g&#243;wniarz"
  ]
  node [
    id 30
    label "dziecko"
  ]
  node [
    id 31
    label "ch&#322;opta&#347;"
  ]
  node [
    id 32
    label "beniaminek"
  ]
  node [
    id 33
    label "zwierz&#281;"
  ]
  node [
    id 34
    label "niepe&#322;noletni"
  ]
  node [
    id 35
    label "go&#322;ow&#261;s"
  ]
  node [
    id 36
    label "sport"
  ]
  node [
    id 37
    label "longer"
  ]
  node [
    id 38
    label "odzywa&#263;_si&#281;"
  ]
  node [
    id 39
    label "odwrotka"
  ]
  node [
    id 40
    label "kontrakt"
  ]
  node [
    id 41
    label "licytacja"
  ]
  node [
    id 42
    label "odzywanie_si&#281;"
  ]
  node [
    id 43
    label "odezwanie_si&#281;"
  ]
  node [
    id 44
    label "rober"
  ]
  node [
    id 45
    label "odezwa&#263;_si&#281;"
  ]
  node [
    id 46
    label "korona"
  ]
  node [
    id 47
    label "inwit"
  ]
  node [
    id 48
    label "gra_w_karty"
  ]
  node [
    id 49
    label "rekontra"
  ]
  node [
    id 50
    label "specjalny"
  ]
  node [
    id 51
    label "na_sportowo"
  ]
  node [
    id 52
    label "uczciwy"
  ]
  node [
    id 53
    label "wygodny"
  ]
  node [
    id 54
    label "pe&#322;ny"
  ]
  node [
    id 55
    label "sportowo"
  ]
  node [
    id 56
    label "bran&#380;owiec"
  ]
  node [
    id 57
    label "wydawnictwo"
  ]
  node [
    id 58
    label "edytor"
  ]
  node [
    id 59
    label "redakcja"
  ]
  node [
    id 60
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 61
    label "przedmiot"
  ]
  node [
    id 62
    label "rzemie&#347;lniczy"
  ]
  node [
    id 63
    label "mistrzostwo"
  ]
  node [
    id 64
    label "&#322;&#243;dzki"
  ]
  node [
    id 65
    label "m&#322;ody"
  ]
  node [
    id 66
    label "i"
  ]
  node [
    id 67
    label "wyspa"
  ]
  node [
    id 68
    label "Micha&#322;"
  ]
  node [
    id 69
    label "wojew&#243;dzki"
  ]
  node [
    id 70
    label "zwi&#261;zka"
  ]
  node [
    id 71
    label "sokolnik"
  ]
  node [
    id 72
    label "las"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 21
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 10
    target 50
  ]
  edge [
    source 10
    target 62
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 65
  ]
  edge [
    source 63
    target 66
  ]
  edge [
    source 63
    target 67
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 66
  ]
  edge [
    source 64
    target 67
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 67
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 71
    target 72
  ]
]
