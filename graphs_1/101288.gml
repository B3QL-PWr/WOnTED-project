graph [
  maxDegree 17
  minDegree 1
  meanDegree 3.05
  density 0.0782051282051282
  graphCliqueNumber 4
  node [
    id 0
    label "brygada"
    origin "text"
  ]
  node [
    id 1
    label "kawaleria"
    origin "text"
  ]
  node [
    id 2
    label "za&#322;oga"
  ]
  node [
    id 3
    label "dywizja"
  ]
  node [
    id 4
    label "armia"
  ]
  node [
    id 5
    label "formacja"
  ]
  node [
    id 6
    label "zwi&#261;zek_taktyczny"
  ]
  node [
    id 7
    label "zesp&#243;&#322;"
  ]
  node [
    id 8
    label "crew"
  ]
  node [
    id 9
    label "D&#261;browszczacy"
  ]
  node [
    id 10
    label "batalion"
  ]
  node [
    id 11
    label "cavalry"
  ]
  node [
    id 12
    label "chor&#261;giew"
  ]
  node [
    id 13
    label "szwadron"
  ]
  node [
    id 14
    label "XV"
  ]
  node [
    id 15
    label "wojsko"
  ]
  node [
    id 16
    label "polskie"
  ]
  node [
    id 17
    label "BK"
  ]
  node [
    id 18
    label "ii"
  ]
  node [
    id 19
    label "RP"
  ]
  node [
    id 20
    label "garnizon"
  ]
  node [
    id 21
    label "Grudzi&#261;dz"
  ]
  node [
    id 22
    label "3"
  ]
  node [
    id 23
    label "Erazma"
  ]
  node [
    id 24
    label "Stablewski"
  ]
  node [
    id 25
    label "18"
  ]
  node [
    id 26
    label "pu&#322;k"
  ]
  node [
    id 27
    label "u&#322;an"
  ]
  node [
    id 28
    label "pomorski"
  ]
  node [
    id 29
    label "8"
  ]
  node [
    id 30
    label "strzelec"
  ]
  node [
    id 31
    label "konny"
  ]
  node [
    id 32
    label "Toru&#324;"
  ]
  node [
    id 33
    label "2"
  ]
  node [
    id 34
    label "szwole&#380;er"
  ]
  node [
    id 35
    label "Rokitnia&#324;skich"
  ]
  node [
    id 36
    label "16"
  ]
  node [
    id 37
    label "XIV"
  ]
  node [
    id 38
    label "samodzielny"
  ]
  node [
    id 39
    label "Bydgoszcz"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 14
    target 17
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 17
    target 32
  ]
  edge [
    source 17
    target 39
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 39
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 27
  ]
  edge [
    source 25
    target 28
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 28
  ]
  edge [
    source 26
    target 29
  ]
  edge [
    source 26
    target 30
  ]
  edge [
    source 26
    target 31
  ]
  edge [
    source 26
    target 33
  ]
  edge [
    source 26
    target 34
  ]
  edge [
    source 26
    target 35
  ]
  edge [
    source 26
    target 36
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 36
  ]
  edge [
    source 28
    target 36
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 31
  ]
  edge [
    source 29
    target 38
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 35
  ]
  edge [
    source 34
    target 35
  ]
]
