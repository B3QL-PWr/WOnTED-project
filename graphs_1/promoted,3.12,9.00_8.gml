graph [
  maxDegree 17
  minDegree 1
  meanDegree 2.03125
  density 0.032242063492063495
  graphCliqueNumber 3
  node [
    id 0
    label "r&#243;&#380;nica"
    origin "text"
  ]
  node [
    id 1
    label "wynika&#263;"
    origin "text"
  ]
  node [
    id 2
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 3
    label "liczba"
    origin "text"
  ]
  node [
    id 4
    label "narodziny"
    origin "text"
  ]
  node [
    id 5
    label "ch&#322;opiec"
    origin "text"
  ]
  node [
    id 6
    label "dziewczynka"
    origin "text"
  ]
  node [
    id 7
    label "uwzgl&#281;dnia&#263;"
    origin "text"
  ]
  node [
    id 8
    label "imigrant"
    origin "text"
  ]
  node [
    id 9
    label "wynik"
  ]
  node [
    id 10
    label "r&#243;&#380;nienie"
  ]
  node [
    id 11
    label "cecha"
  ]
  node [
    id 12
    label "kontrastowy"
  ]
  node [
    id 13
    label "discord"
  ]
  node [
    id 14
    label "okazywa&#263;_si&#281;"
  ]
  node [
    id 15
    label "rise"
  ]
  node [
    id 16
    label "appear"
  ]
  node [
    id 17
    label "rodzi&#263;_si&#281;"
  ]
  node [
    id 18
    label "doros&#322;y"
  ]
  node [
    id 19
    label "wiele"
  ]
  node [
    id 20
    label "dorodny"
  ]
  node [
    id 21
    label "znaczny"
  ]
  node [
    id 22
    label "du&#380;o"
  ]
  node [
    id 23
    label "prawdziwy"
  ]
  node [
    id 24
    label "niema&#322;o"
  ]
  node [
    id 25
    label "wa&#380;ny"
  ]
  node [
    id 26
    label "rozwini&#281;ty"
  ]
  node [
    id 27
    label "kategoria"
  ]
  node [
    id 28
    label "kategoria_gramatyczna"
  ]
  node [
    id 29
    label "kwadrat_magiczny"
  ]
  node [
    id 30
    label "grupa"
  ]
  node [
    id 31
    label "wyra&#380;enie"
  ]
  node [
    id 32
    label "pierwiastek"
  ]
  node [
    id 33
    label "rozmiar"
  ]
  node [
    id 34
    label "number"
  ]
  node [
    id 35
    label "poj&#281;cie"
  ]
  node [
    id 36
    label "koniugacja"
  ]
  node [
    id 37
    label "pocz&#261;tek"
  ]
  node [
    id 38
    label "cz&#322;owiek"
  ]
  node [
    id 39
    label "pomocnik"
  ]
  node [
    id 40
    label "g&#243;wniarz"
  ]
  node [
    id 41
    label "&#347;l&#261;ski"
  ]
  node [
    id 42
    label "m&#322;odzieniec"
  ]
  node [
    id 43
    label "kajtek"
  ]
  node [
    id 44
    label "kawaler"
  ]
  node [
    id 45
    label "usynawianie"
  ]
  node [
    id 46
    label "dziecko"
  ]
  node [
    id 47
    label "okrzos"
  ]
  node [
    id 48
    label "usynowienie"
  ]
  node [
    id 49
    label "sympatia"
  ]
  node [
    id 50
    label "pederasta"
  ]
  node [
    id 51
    label "synek"
  ]
  node [
    id 52
    label "boyfriend"
  ]
  node [
    id 53
    label "potomkini"
  ]
  node [
    id 54
    label "prostytutka"
  ]
  node [
    id 55
    label "my&#347;le&#263;"
  ]
  node [
    id 56
    label "involve"
  ]
  node [
    id 57
    label "cudzoziemiec"
  ]
  node [
    id 58
    label "przybysz"
  ]
  node [
    id 59
    label "migrant"
  ]
  node [
    id 60
    label "imigracja"
  ]
  node [
    id 61
    label "ochrona_uzupe&#322;niaj&#261;ca"
  ]
  node [
    id 62
    label "Bengalczyk&#243;w"
  ]
  node [
    id 63
    label "98"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 62
    target 63
  ]
]
