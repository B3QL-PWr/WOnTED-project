graph [
  maxDegree 5
  minDegree 1
  meanDegree 1.5
  density 0.21428571428571427
  graphCliqueNumber 2
  node [
    id 0
    label "fallout"
    origin "text"
  ]
  node [
    id 1
    label "padaka"
    origin "text"
  ]
  node [
    id 2
    label "wy&#347;miewanie_si&#281;"
  ]
  node [
    id 3
    label "ubaw"
  ]
  node [
    id 4
    label "&#347;miech"
  ]
  node [
    id 5
    label "bankructwo"
  ]
  node [
    id 6
    label "Newa"
  ]
  node [
    id 7
    label "Vegas"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 6
    target 7
  ]
]
