graph [
  maxDegree 57
  minDegree 1
  meanDegree 2.046783625730994
  density 0.012039903680770554
  graphCliqueNumber 3
  node [
    id 0
    label "s&#243;l"
    origin "text"
  ]
  node [
    id 1
    label "droga"
    origin "text"
  ]
  node [
    id 2
    label "niszczy&#263;"
    origin "text"
  ]
  node [
    id 3
    label "ro&#347;lina"
    origin "text"
  ]
  node [
    id 4
    label "okalecza&#263;"
    origin "text"
  ]
  node [
    id 5
    label "zwierz&#281;"
    origin "text"
  ]
  node [
    id 6
    label "sprzyja&#263;"
    origin "text"
  ]
  node [
    id 7
    label "korozja"
    origin "text"
  ]
  node [
    id 8
    label "samoch&#243;d"
    origin "text"
  ]
  node [
    id 9
    label "crusta"
  ]
  node [
    id 10
    label "reszta_kwasowa"
  ]
  node [
    id 11
    label "przyprawa"
  ]
  node [
    id 12
    label "&#380;upnik"
  ]
  node [
    id 13
    label "kryptografia"
  ]
  node [
    id 14
    label "dane"
  ]
  node [
    id 15
    label "s&#322;onisko"
  ]
  node [
    id 16
    label "zwi&#261;zek_chemiczny"
  ]
  node [
    id 17
    label "margarita"
  ]
  node [
    id 18
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 19
    label "journey"
  ]
  node [
    id 20
    label "podbieg"
  ]
  node [
    id 21
    label "bezsilnikowy"
  ]
  node [
    id 22
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 23
    label "wylot"
  ]
  node [
    id 24
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 25
    label "drogowskaz"
  ]
  node [
    id 26
    label "nawierzchnia"
  ]
  node [
    id 27
    label "turystyka"
  ]
  node [
    id 28
    label "budowla"
  ]
  node [
    id 29
    label "spos&#243;b"
  ]
  node [
    id 30
    label "passage"
  ]
  node [
    id 31
    label "marszrutyzacja"
  ]
  node [
    id 32
    label "zbior&#243;wka"
  ]
  node [
    id 33
    label "ekskursja"
  ]
  node [
    id 34
    label "rajza"
  ]
  node [
    id 35
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 36
    label "ruch"
  ]
  node [
    id 37
    label "trasa"
  ]
  node [
    id 38
    label "wyb&#243;j"
  ]
  node [
    id 39
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 40
    label "ekwipunek"
  ]
  node [
    id 41
    label "korona_drogi"
  ]
  node [
    id 42
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 43
    label "pobocze"
  ]
  node [
    id 44
    label "os&#322;abia&#263;"
  ]
  node [
    id 45
    label "destroy"
  ]
  node [
    id 46
    label "wygrywa&#263;"
  ]
  node [
    id 47
    label "pamper"
  ]
  node [
    id 48
    label "zdrowie"
  ]
  node [
    id 49
    label "szkodzi&#263;"
  ]
  node [
    id 50
    label "uszkadza&#263;"
  ]
  node [
    id 51
    label "mar"
  ]
  node [
    id 52
    label "powodowa&#263;"
  ]
  node [
    id 53
    label "strzy&#380;enie"
  ]
  node [
    id 54
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 55
    label "wegetowa&#263;"
  ]
  node [
    id 56
    label "nieuleczalnie_chory"
  ]
  node [
    id 57
    label "flawonoid"
  ]
  node [
    id 58
    label "rozmn&#243;&#380;ka"
  ]
  node [
    id 59
    label "strzyc"
  ]
  node [
    id 60
    label "g&#322;uszy&#263;"
  ]
  node [
    id 61
    label "bulwka"
  ]
  node [
    id 62
    label "fitotron"
  ]
  node [
    id 63
    label "pochewka"
  ]
  node [
    id 64
    label "ro&#347;liny"
  ]
  node [
    id 65
    label "wegetowanie"
  ]
  node [
    id 66
    label "zadziorek"
  ]
  node [
    id 67
    label "epiderma"
  ]
  node [
    id 68
    label "zawi&#261;zek"
  ]
  node [
    id 69
    label "odn&#243;&#380;ka"
  ]
  node [
    id 70
    label "fitocenoza"
  ]
  node [
    id 71
    label "g&#322;uszenie"
  ]
  node [
    id 72
    label "fotoautotrof"
  ]
  node [
    id 73
    label "wypotnik"
  ]
  node [
    id 74
    label "gumoza"
  ]
  node [
    id 75
    label "wyro&#347;le"
  ]
  node [
    id 76
    label "owoc"
  ]
  node [
    id 77
    label "zbiorowisko"
  ]
  node [
    id 78
    label "p&#281;d"
  ]
  node [
    id 79
    label "hodowla"
  ]
  node [
    id 80
    label "asocjacja_ro&#347;lin"
  ]
  node [
    id 81
    label "sok"
  ]
  node [
    id 82
    label "wegetacja"
  ]
  node [
    id 83
    label "do&#322;owanie"
  ]
  node [
    id 84
    label "pora&#380;a&#263;"
  ]
  node [
    id 85
    label "w&#322;&#243;kno"
  ]
  node [
    id 86
    label "j&#281;zyczek_li&#347;ciowy"
  ]
  node [
    id 87
    label "system_korzeniowy"
  ]
  node [
    id 88
    label "do&#322;owa&#263;"
  ]
  node [
    id 89
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 90
    label "kaleczy&#263;"
  ]
  node [
    id 91
    label "krzywdzi&#263;"
  ]
  node [
    id 92
    label "cz&#322;owiek"
  ]
  node [
    id 93
    label "grzbiet"
  ]
  node [
    id 94
    label "fukanie"
  ]
  node [
    id 95
    label "zachowanie"
  ]
  node [
    id 96
    label "popapraniec"
  ]
  node [
    id 97
    label "siedzie&#263;"
  ]
  node [
    id 98
    label "tresowa&#263;"
  ]
  node [
    id 99
    label "oswaja&#263;"
  ]
  node [
    id 100
    label "napa&#347;&#263;_si&#281;"
  ]
  node [
    id 101
    label "poskramia&#263;"
  ]
  node [
    id 102
    label "zwyrol"
  ]
  node [
    id 103
    label "animalista"
  ]
  node [
    id 104
    label "skubn&#261;&#263;"
  ]
  node [
    id 105
    label "fukni&#281;cie"
  ]
  node [
    id 106
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 107
    label "zar&#380;ni&#281;cie"
  ]
  node [
    id 108
    label "farba"
  ]
  node [
    id 109
    label "istota_&#380;ywa"
  ]
  node [
    id 110
    label "budowa"
  ]
  node [
    id 111
    label "monogamia"
  ]
  node [
    id 112
    label "pa&#347;&#263;_si&#281;"
  ]
  node [
    id 113
    label "sodomita"
  ]
  node [
    id 114
    label "budowa_cia&#322;a"
  ]
  node [
    id 115
    label "dor&#380;n&#261;&#263;"
  ]
  node [
    id 116
    label "oz&#243;r"
  ]
  node [
    id 117
    label "gad"
  ]
  node [
    id 118
    label "&#322;eb"
  ]
  node [
    id 119
    label "treser"
  ]
  node [
    id 120
    label "fauna"
  ]
  node [
    id 121
    label "pasienie_si&#281;"
  ]
  node [
    id 122
    label "degenerat"
  ]
  node [
    id 123
    label "czerniak"
  ]
  node [
    id 124
    label "siedzenie"
  ]
  node [
    id 125
    label "le&#380;e&#263;"
  ]
  node [
    id 126
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 127
    label "weterynarz"
  ]
  node [
    id 128
    label "wiwarium"
  ]
  node [
    id 129
    label "wios&#322;owa&#263;"
  ]
  node [
    id 130
    label "skuba&#263;"
  ]
  node [
    id 131
    label "skubni&#281;cie"
  ]
  node [
    id 132
    label "poligamia"
  ]
  node [
    id 133
    label "przyssawka"
  ]
  node [
    id 134
    label "agresja"
  ]
  node [
    id 135
    label "niecz&#322;owiek"
  ]
  node [
    id 136
    label "skubanie"
  ]
  node [
    id 137
    label "wios&#322;owanie"
  ]
  node [
    id 138
    label "napasienie_si&#281;"
  ]
  node [
    id 139
    label "okrutnik"
  ]
  node [
    id 140
    label "wylinka"
  ]
  node [
    id 141
    label "paszcza"
  ]
  node [
    id 142
    label "bestia"
  ]
  node [
    id 143
    label "zwierz&#281;ta"
  ]
  node [
    id 144
    label "le&#380;enie"
  ]
  node [
    id 145
    label "czu&#263;"
  ]
  node [
    id 146
    label "chowa&#263;"
  ]
  node [
    id 147
    label "stanowi&#263;"
  ]
  node [
    id 148
    label "w&#380;er"
  ]
  node [
    id 149
    label "proces_chemiczny"
  ]
  node [
    id 150
    label "baga&#380;nik"
  ]
  node [
    id 151
    label "immobilizer"
  ]
  node [
    id 152
    label "ko&#322;o_samochodowe"
  ]
  node [
    id 153
    label "poduszka_powietrzna"
  ]
  node [
    id 154
    label "dachowanie"
  ]
  node [
    id 155
    label "dwu&#347;lad"
  ]
  node [
    id 156
    label "deska_rozdzielcza"
  ]
  node [
    id 157
    label "poci&#261;g_drogowy"
  ]
  node [
    id 158
    label "kierownica"
  ]
  node [
    id 159
    label "pojazd_drogowy"
  ]
  node [
    id 160
    label "uk&#322;ad_kierowniczy"
  ]
  node [
    id 161
    label "pompa_wodna"
  ]
  node [
    id 162
    label "silnik"
  ]
  node [
    id 163
    label "wycieraczka"
  ]
  node [
    id 164
    label "bak"
  ]
  node [
    id 165
    label "ABS"
  ]
  node [
    id 166
    label "most"
  ]
  node [
    id 167
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 168
    label "spryskiwacz"
  ]
  node [
    id 169
    label "t&#322;umik"
  ]
  node [
    id 170
    label "tempomat"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 148
  ]
  edge [
    source 7
    target 149
  ]
  edge [
    source 8
    target 150
  ]
  edge [
    source 8
    target 151
  ]
  edge [
    source 8
    target 18
  ]
  edge [
    source 8
    target 152
  ]
  edge [
    source 8
    target 153
  ]
  edge [
    source 8
    target 154
  ]
  edge [
    source 8
    target 155
  ]
  edge [
    source 8
    target 156
  ]
  edge [
    source 8
    target 157
  ]
  edge [
    source 8
    target 158
  ]
  edge [
    source 8
    target 159
  ]
  edge [
    source 8
    target 160
  ]
  edge [
    source 8
    target 161
  ]
  edge [
    source 8
    target 162
  ]
  edge [
    source 8
    target 163
  ]
  edge [
    source 8
    target 164
  ]
  edge [
    source 8
    target 165
  ]
  edge [
    source 8
    target 166
  ]
  edge [
    source 8
    target 167
  ]
  edge [
    source 8
    target 168
  ]
  edge [
    source 8
    target 169
  ]
  edge [
    source 8
    target 170
  ]
]
