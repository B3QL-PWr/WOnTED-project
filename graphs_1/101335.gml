graph [
  maxDegree 9
  minDegree 1
  meanDegree 1.7777777777777777
  density 0.10457516339869281
  graphCliqueNumber 2
  node [
    id 0
    label "ma&#322;a"
    origin "text"
  ]
  node [
    id 1
    label "wojew&#243;dztwo"
    origin "text"
  ]
  node [
    id 2
    label "pomorski"
    origin "text"
  ]
  node [
    id 3
    label "dziewczynka"
  ]
  node [
    id 4
    label "dziewczyna"
  ]
  node [
    id 5
    label "jednostka_administracyjna"
  ]
  node [
    id 6
    label "makroregion"
  ]
  node [
    id 7
    label "powiat"
  ]
  node [
    id 8
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 9
    label "mikroregion"
  ]
  node [
    id 10
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 11
    label "pa&#324;stwo"
  ]
  node [
    id 12
    label "polski"
  ]
  node [
    id 13
    label "po_pomorsku"
  ]
  node [
    id 14
    label "etnolekt"
  ]
  node [
    id 15
    label "regionalny"
  ]
  node [
    id 16
    label "pojezierze"
  ]
  node [
    id 17
    label "bytowski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 16
    target 17
  ]
]
