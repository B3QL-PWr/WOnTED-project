graph [
  maxDegree 37
  minDegree 1
  meanDegree 2.2718446601941746
  density 0.00552760257954787
  graphCliqueNumber 5
  node [
    id 0
    label "wysoki"
    origin "text"
  ]
  node [
    id 1
    label "izba"
    origin "text"
  ]
  node [
    id 2
    label "propozycja"
    origin "text"
  ]
  node [
    id 3
    label "rz&#261;d"
    origin "text"
  ]
  node [
    id 4
    label "przewidywa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "dokonanie"
    origin "text"
  ]
  node [
    id 6
    label "zmiana"
    origin "text"
  ]
  node [
    id 7
    label "mai&#263;"
    origin "text"
  ]
  node [
    id 8
    label "cela"
    origin "text"
  ]
  node [
    id 9
    label "przekaza&#263;"
    origin "text"
  ]
  node [
    id 10
    label "minister"
    origin "text"
  ]
  node [
    id 11
    label "w&#322;a&#347;ciwy"
    origin "text"
  ]
  node [
    id 12
    label "sprawa"
    origin "text"
  ]
  node [
    id 13
    label "zabezpieczenie"
    origin "text"
  ]
  node [
    id 14
    label "spo&#322;eczny"
    origin "text"
  ]
  node [
    id 15
    label "uprawnienie"
    origin "text"
  ]
  node [
    id 16
    label "nadzorczy"
    origin "text"
  ]
  node [
    id 17
    label "nad"
    origin "text"
  ]
  node [
    id 18
    label "zak&#322;ad"
    origin "text"
  ]
  node [
    id 19
    label "ubezpieczenie"
    origin "text"
  ]
  node [
    id 20
    label "przypomnie&#263;"
    origin "text"
  ]
  node [
    id 21
    label "obecnie"
    origin "text"
  ]
  node [
    id 22
    label "nadz&#243;r"
    origin "text"
  ]
  node [
    id 23
    label "ten"
    origin "text"
  ]
  node [
    id 24
    label "sprawowa&#263;"
    origin "text"
  ]
  node [
    id 25
    label "prezes"
    origin "text"
  ]
  node [
    id 26
    label "rad"
    origin "text"
  ]
  node [
    id 27
    label "uzasadnienie"
    origin "text"
  ]
  node [
    id 28
    label "projekt"
    origin "text"
  ]
  node [
    id 29
    label "wskaza&#263;"
    origin "text"
  ]
  node [
    id 30
    label "skupienie"
    origin "text"
  ]
  node [
    id 31
    label "wszyscy"
    origin "text"
  ]
  node [
    id 32
    label "niezb&#281;dny"
    origin "text"
  ]
  node [
    id 33
    label "kompetencja"
    origin "text"
  ]
  node [
    id 34
    label "r&#281;ka"
    origin "text"
  ]
  node [
    id 35
    label "jeden"
    origin "text"
  ]
  node [
    id 36
    label "organ"
    origin "text"
  ]
  node [
    id 37
    label "jaki"
    origin "text"
  ]
  node [
    id 38
    label "by&#263;"
    origin "text"
  ]
  node [
    id 39
    label "praca"
    origin "text"
  ]
  node [
    id 40
    label "polityka"
    origin "text"
  ]
  node [
    id 41
    label "przyczyni&#263;"
    origin "text"
  ]
  node [
    id 42
    label "si&#281;"
    origin "text"
  ]
  node [
    id 43
    label "zracjonalizowa&#263;"
    origin "text"
  ]
  node [
    id 44
    label "proces"
    origin "text"
  ]
  node [
    id 45
    label "decyzyjny"
    origin "text"
  ]
  node [
    id 46
    label "realizowa&#263;"
    origin "text"
  ]
  node [
    id 47
    label "przez"
    origin "text"
  ]
  node [
    id 48
    label "zakres"
    origin "text"
  ]
  node [
    id 49
    label "pozwoli&#263;"
    origin "text"
  ]
  node [
    id 50
    label "unikn&#261;&#263;"
    origin "text"
  ]
  node [
    id 51
    label "w&#261;tpliwo&#347;&#263;"
    origin "text"
  ]
  node [
    id 52
    label "sp&#243;r"
    origin "text"
  ]
  node [
    id 53
    label "kompetencyjny"
    origin "text"
  ]
  node [
    id 54
    label "warto&#347;ciowy"
  ]
  node [
    id 55
    label "du&#380;y"
  ]
  node [
    id 56
    label "wysoce"
  ]
  node [
    id 57
    label "daleki"
  ]
  node [
    id 58
    label "znaczny"
  ]
  node [
    id 59
    label "wysoko"
  ]
  node [
    id 60
    label "szczytnie"
  ]
  node [
    id 61
    label "wznios&#322;y"
  ]
  node [
    id 62
    label "wyrafinowany"
  ]
  node [
    id 63
    label "z_wysoka"
  ]
  node [
    id 64
    label "chwalebny"
  ]
  node [
    id 65
    label "uprzywilejowany"
  ]
  node [
    id 66
    label "niepo&#347;ledni"
  ]
  node [
    id 67
    label "pok&#243;j"
  ]
  node [
    id 68
    label "parlament"
  ]
  node [
    id 69
    label "zwi&#261;zek"
  ]
  node [
    id 70
    label "NIK"
  ]
  node [
    id 71
    label "urz&#261;d"
  ]
  node [
    id 72
    label "pomieszczenie"
  ]
  node [
    id 73
    label "pomys&#322;"
  ]
  node [
    id 74
    label "proposal"
  ]
  node [
    id 75
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 76
    label "kategoria"
  ]
  node [
    id 77
    label "egzekutywa"
  ]
  node [
    id 78
    label "gabinet_cieni"
  ]
  node [
    id 79
    label "gromada"
  ]
  node [
    id 80
    label "premier"
  ]
  node [
    id 81
    label "Londyn"
  ]
  node [
    id 82
    label "Konsulat"
  ]
  node [
    id 83
    label "uporz&#261;dkowanie"
  ]
  node [
    id 84
    label "jednostka_systematyczna"
  ]
  node [
    id 85
    label "szpaler"
  ]
  node [
    id 86
    label "przybli&#380;enie"
  ]
  node [
    id 87
    label "tract"
  ]
  node [
    id 88
    label "number"
  ]
  node [
    id 89
    label "lon&#380;a"
  ]
  node [
    id 90
    label "w&#322;adza"
  ]
  node [
    id 91
    label "instytucja"
  ]
  node [
    id 92
    label "klasa"
  ]
  node [
    id 93
    label "zamierza&#263;"
  ]
  node [
    id 94
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 95
    label "anticipate"
  ]
  node [
    id 96
    label "dzia&#322;anie"
  ]
  node [
    id 97
    label "zrobienie"
  ]
  node [
    id 98
    label "zas&#322;u&#380;ony"
  ]
  node [
    id 99
    label "czynno&#347;&#263;"
  ]
  node [
    id 100
    label "sukces"
  ]
  node [
    id 101
    label "act"
  ]
  node [
    id 102
    label "termination"
  ]
  node [
    id 103
    label "zaawansowanie"
  ]
  node [
    id 104
    label "performance"
  ]
  node [
    id 105
    label "anatomopatolog"
  ]
  node [
    id 106
    label "rewizja"
  ]
  node [
    id 107
    label "oznaka"
  ]
  node [
    id 108
    label "czas"
  ]
  node [
    id 109
    label "ferment"
  ]
  node [
    id 110
    label "komplet"
  ]
  node [
    id 111
    label "tura"
  ]
  node [
    id 112
    label "amendment"
  ]
  node [
    id 113
    label "zmianka"
  ]
  node [
    id 114
    label "odmienianie"
  ]
  node [
    id 115
    label "passage"
  ]
  node [
    id 116
    label "zjawisko"
  ]
  node [
    id 117
    label "change"
  ]
  node [
    id 118
    label "ozdabia&#263;"
  ]
  node [
    id 119
    label "klasztor"
  ]
  node [
    id 120
    label "impart"
  ]
  node [
    id 121
    label "sygna&#322;"
  ]
  node [
    id 122
    label "propagate"
  ]
  node [
    id 123
    label "transfer"
  ]
  node [
    id 124
    label "give"
  ]
  node [
    id 125
    label "wys&#322;a&#263;"
  ]
  node [
    id 126
    label "zrobi&#263;"
  ]
  node [
    id 127
    label "poda&#263;"
  ]
  node [
    id 128
    label "wp&#322;aci&#263;"
  ]
  node [
    id 129
    label "Goebbels"
  ]
  node [
    id 130
    label "Sto&#322;ypin"
  ]
  node [
    id 131
    label "dostojnik"
  ]
  node [
    id 132
    label "taki"
  ]
  node [
    id 133
    label "nale&#380;yty"
  ]
  node [
    id 134
    label "charakterystyczny"
  ]
  node [
    id 135
    label "stosownie"
  ]
  node [
    id 136
    label "dobry"
  ]
  node [
    id 137
    label "prawdziwy"
  ]
  node [
    id 138
    label "uprawniony"
  ]
  node [
    id 139
    label "zasadniczy"
  ]
  node [
    id 140
    label "typowy"
  ]
  node [
    id 141
    label "nale&#380;ny"
  ]
  node [
    id 142
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 143
    label "temat"
  ]
  node [
    id 144
    label "kognicja"
  ]
  node [
    id 145
    label "idea"
  ]
  node [
    id 146
    label "szczeg&#243;&#322;"
  ]
  node [
    id 147
    label "rzecz"
  ]
  node [
    id 148
    label "wydarzenie"
  ]
  node [
    id 149
    label "przes&#322;anka"
  ]
  node [
    id 150
    label "rozprawa"
  ]
  node [
    id 151
    label "object"
  ]
  node [
    id 152
    label "proposition"
  ]
  node [
    id 153
    label "zabezpiecza&#263;_si&#281;"
  ]
  node [
    id 154
    label "cover"
  ]
  node [
    id 155
    label "spowodowanie"
  ]
  node [
    id 156
    label "obiekt"
  ]
  node [
    id 157
    label "chroniony"
  ]
  node [
    id 158
    label "por&#281;czenie"
  ]
  node [
    id 159
    label "zainstalowanie"
  ]
  node [
    id 160
    label "bro&#324;_palna"
  ]
  node [
    id 161
    label "pistolet"
  ]
  node [
    id 162
    label "guarantee"
  ]
  node [
    id 163
    label "tarcza"
  ]
  node [
    id 164
    label "zabezpieczanie_si&#281;"
  ]
  node [
    id 165
    label "zaplecze"
  ]
  node [
    id 166
    label "zastaw"
  ]
  node [
    id 167
    label "metoda"
  ]
  node [
    id 168
    label "zapewnienie"
  ]
  node [
    id 169
    label "niepubliczny"
  ]
  node [
    id 170
    label "spo&#322;ecznie"
  ]
  node [
    id 171
    label "publiczny"
  ]
  node [
    id 172
    label "dokument"
  ]
  node [
    id 173
    label "title"
  ]
  node [
    id 174
    label "law"
  ]
  node [
    id 175
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 176
    label "authorization"
  ]
  node [
    id 177
    label "kontrolny"
  ]
  node [
    id 178
    label "czyn"
  ]
  node [
    id 179
    label "company"
  ]
  node [
    id 180
    label "zak&#322;adka"
  ]
  node [
    id 181
    label "firma"
  ]
  node [
    id 182
    label "instytut"
  ]
  node [
    id 183
    label "wyko&#324;czenie"
  ]
  node [
    id 184
    label "jednostka_organizacyjna"
  ]
  node [
    id 185
    label "umowa"
  ]
  node [
    id 186
    label "miejsce_pracy"
  ]
  node [
    id 187
    label "ochrona"
  ]
  node [
    id 188
    label "franszyza"
  ]
  node [
    id 189
    label "ryzyko_niewygas&#322;e"
  ]
  node [
    id 190
    label "suma_ubezpieczenia"
  ]
  node [
    id 191
    label "screen"
  ]
  node [
    id 192
    label "przyznanie"
  ]
  node [
    id 193
    label "op&#322;ata"
  ]
  node [
    id 194
    label "uchronienie"
  ]
  node [
    id 195
    label "uprz&#261;&#380;_wspinaczkowa"
  ]
  node [
    id 196
    label "ubezpieczalnia"
  ]
  node [
    id 197
    label "bezpiecze&#324;stwo"
  ]
  node [
    id 198
    label "insurance"
  ]
  node [
    id 199
    label "oddzia&#322;"
  ]
  node [
    id 200
    label "poinformowa&#263;"
  ]
  node [
    id 201
    label "u&#347;wiadomi&#263;"
  ]
  node [
    id 202
    label "prompt"
  ]
  node [
    id 203
    label "ninie"
  ]
  node [
    id 204
    label "aktualny"
  ]
  node [
    id 205
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 206
    label "examination"
  ]
  node [
    id 207
    label "okre&#347;lony"
  ]
  node [
    id 208
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 209
    label "prosecute"
  ]
  node [
    id 210
    label "gruba_ryba"
  ]
  node [
    id 211
    label "zwierzchnik"
  ]
  node [
    id 212
    label "berylowiec"
  ]
  node [
    id 213
    label "u&#347;miechni&#281;ty"
  ]
  node [
    id 214
    label "pierwiastek_promieniotw&#243;rczy"
  ]
  node [
    id 215
    label "mikroradian"
  ]
  node [
    id 216
    label "zadowolenie_si&#281;"
  ]
  node [
    id 217
    label "usatysfakcjonowanie_si&#281;"
  ]
  node [
    id 218
    label "content"
  ]
  node [
    id 219
    label "jednostka_promieniowania"
  ]
  node [
    id 220
    label "miliradian"
  ]
  node [
    id 221
    label "jednostka"
  ]
  node [
    id 222
    label "justyfikacja"
  ]
  node [
    id 223
    label "apologetyk"
  ]
  node [
    id 224
    label "informacja"
  ]
  node [
    id 225
    label "gossip"
  ]
  node [
    id 226
    label "wyja&#347;nienie"
  ]
  node [
    id 227
    label "device"
  ]
  node [
    id 228
    label "program_u&#380;ytkowy"
  ]
  node [
    id 229
    label "intencja"
  ]
  node [
    id 230
    label "agreement"
  ]
  node [
    id 231
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 232
    label "plan"
  ]
  node [
    id 233
    label "dokumentacja"
  ]
  node [
    id 234
    label "aim"
  ]
  node [
    id 235
    label "pokaza&#263;"
  ]
  node [
    id 236
    label "podkre&#347;li&#263;"
  ]
  node [
    id 237
    label "wybra&#263;"
  ]
  node [
    id 238
    label "indicate"
  ]
  node [
    id 239
    label "wyja&#347;ni&#263;"
  ]
  node [
    id 240
    label "picture"
  ]
  node [
    id 241
    label "point"
  ]
  node [
    id 242
    label "przegrupowanie"
  ]
  node [
    id 243
    label "concentration"
  ]
  node [
    id 244
    label "po&#322;&#261;czenie"
  ]
  node [
    id 245
    label "z&#322;&#261;czenie"
  ]
  node [
    id 246
    label "congestion"
  ]
  node [
    id 247
    label "uwaga"
  ]
  node [
    id 248
    label "agglomeration"
  ]
  node [
    id 249
    label "zgromadzenie"
  ]
  node [
    id 250
    label "zbi&#243;r"
  ]
  node [
    id 251
    label "po&#347;wi&#281;cenie"
  ]
  node [
    id 252
    label "kupienie"
  ]
  node [
    id 253
    label "niezb&#281;dnie"
  ]
  node [
    id 254
    label "ability"
  ]
  node [
    id 255
    label "authority"
  ]
  node [
    id 256
    label "sprawno&#347;&#263;"
  ]
  node [
    id 257
    label "znawstwo"
  ]
  node [
    id 258
    label "prawo"
  ]
  node [
    id 259
    label "zdolno&#347;&#263;"
  ]
  node [
    id 260
    label "gestia"
  ]
  node [
    id 261
    label "krzy&#380;"
  ]
  node [
    id 262
    label "paw"
  ]
  node [
    id 263
    label "rami&#281;"
  ]
  node [
    id 264
    label "gestykulowanie"
  ]
  node [
    id 265
    label "pi&#322;ka_no&#380;na"
  ]
  node [
    id 266
    label "pracownik"
  ]
  node [
    id 267
    label "ko&#324;czyna_g&#243;rna"
  ]
  node [
    id 268
    label "bramkarz"
  ]
  node [
    id 269
    label "&#347;r&#243;dr&#281;cze"
  ]
  node [
    id 270
    label "handwriting"
  ]
  node [
    id 271
    label "hasta"
  ]
  node [
    id 272
    label "pi&#322;ka"
  ]
  node [
    id 273
    label "&#322;okie&#263;"
  ]
  node [
    id 274
    label "spos&#243;b"
  ]
  node [
    id 275
    label "zagrywka"
  ]
  node [
    id 276
    label "obietnica"
  ]
  node [
    id 277
    label "przedrami&#281;"
  ]
  node [
    id 278
    label "chwyta&#263;"
  ]
  node [
    id 279
    label "r&#261;czyna"
  ]
  node [
    id 280
    label "cecha"
  ]
  node [
    id 281
    label "wykroczenie"
  ]
  node [
    id 282
    label "kroki"
  ]
  node [
    id 283
    label "palec"
  ]
  node [
    id 284
    label "&#380;&#243;&#322;ta_kartka"
  ]
  node [
    id 285
    label "graba"
  ]
  node [
    id 286
    label "hand"
  ]
  node [
    id 287
    label "nadgarstek"
  ]
  node [
    id 288
    label "pomocnik"
  ]
  node [
    id 289
    label "k&#322;&#261;b"
  ]
  node [
    id 290
    label "hazena"
  ]
  node [
    id 291
    label "gestykulowa&#263;"
  ]
  node [
    id 292
    label "cmoknonsens"
  ]
  node [
    id 293
    label "d&#322;o&#324;"
  ]
  node [
    id 294
    label "chwytanie"
  ]
  node [
    id 295
    label "czerwona_kartka"
  ]
  node [
    id 296
    label "kieliszek"
  ]
  node [
    id 297
    label "shot"
  ]
  node [
    id 298
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 299
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 300
    label "jaki&#347;"
  ]
  node [
    id 301
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 302
    label "jednolicie"
  ]
  node [
    id 303
    label "w&#243;dka"
  ]
  node [
    id 304
    label "ujednolicenie"
  ]
  node [
    id 305
    label "jednakowy"
  ]
  node [
    id 306
    label "uk&#322;ad"
  ]
  node [
    id 307
    label "organogeneza"
  ]
  node [
    id 308
    label "Komitet_Region&#243;w"
  ]
  node [
    id 309
    label "Izba_Konsyliarska"
  ]
  node [
    id 310
    label "budowa"
  ]
  node [
    id 311
    label "okolica"
  ]
  node [
    id 312
    label "zesp&#243;&#322;"
  ]
  node [
    id 313
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 314
    label "dekortykacja"
  ]
  node [
    id 315
    label "struktura_anatomiczna"
  ]
  node [
    id 316
    label "tkanka"
  ]
  node [
    id 317
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 318
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 319
    label "stomia"
  ]
  node [
    id 320
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 321
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 322
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 323
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 324
    label "tw&#243;r"
  ]
  node [
    id 325
    label "si&#281;ga&#263;"
  ]
  node [
    id 326
    label "trwa&#263;"
  ]
  node [
    id 327
    label "obecno&#347;&#263;"
  ]
  node [
    id 328
    label "stan"
  ]
  node [
    id 329
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 330
    label "stand"
  ]
  node [
    id 331
    label "mie&#263;_miejsce"
  ]
  node [
    id 332
    label "uczestniczy&#263;"
  ]
  node [
    id 333
    label "chodzi&#263;"
  ]
  node [
    id 334
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 335
    label "equal"
  ]
  node [
    id 336
    label "stosunek_pracy"
  ]
  node [
    id 337
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 338
    label "benedykty&#324;ski"
  ]
  node [
    id 339
    label "pracowanie"
  ]
  node [
    id 340
    label "zaw&#243;d"
  ]
  node [
    id 341
    label "kierownictwo"
  ]
  node [
    id 342
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 343
    label "wytw&#243;r"
  ]
  node [
    id 344
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 345
    label "tynkarski"
  ]
  node [
    id 346
    label "czynnik_produkcji"
  ]
  node [
    id 347
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 348
    label "zobowi&#261;zanie"
  ]
  node [
    id 349
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 350
    label "tyrka"
  ]
  node [
    id 351
    label "pracowa&#263;"
  ]
  node [
    id 352
    label "siedziba"
  ]
  node [
    id 353
    label "poda&#380;_pracy"
  ]
  node [
    id 354
    label "miejsce"
  ]
  node [
    id 355
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 356
    label "najem"
  ]
  node [
    id 357
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 358
    label "policy"
  ]
  node [
    id 359
    label "dyplomacja"
  ]
  node [
    id 360
    label "Europejska_Polityka_S&#261;siedztwa"
  ]
  node [
    id 361
    label "uzasadni&#263;"
  ]
  node [
    id 362
    label "unowocze&#347;ni&#263;"
  ]
  node [
    id 363
    label "spowodowa&#263;"
  ]
  node [
    id 364
    label "legislacyjnie"
  ]
  node [
    id 365
    label "przebieg"
  ]
  node [
    id 366
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 367
    label "nast&#281;pstwo"
  ]
  node [
    id 368
    label "zdolny"
  ]
  node [
    id 369
    label "wykorzystywa&#263;"
  ]
  node [
    id 370
    label "przeprowadza&#263;"
  ]
  node [
    id 371
    label "spieni&#281;&#380;a&#263;"
  ]
  node [
    id 372
    label "create"
  ]
  node [
    id 373
    label "prawdzi&#263;"
  ]
  node [
    id 374
    label "tworzy&#263;"
  ]
  node [
    id 375
    label "wielko&#347;&#263;"
  ]
  node [
    id 376
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 377
    label "granica"
  ]
  node [
    id 378
    label "circle"
  ]
  node [
    id 379
    label "podzakres"
  ]
  node [
    id 380
    label "desygnat"
  ]
  node [
    id 381
    label "sfera"
  ]
  node [
    id 382
    label "dziedzina"
  ]
  node [
    id 383
    label "pofolgowa&#263;"
  ]
  node [
    id 384
    label "assent"
  ]
  node [
    id 385
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 386
    label "leave"
  ]
  node [
    id 387
    label "uzna&#263;"
  ]
  node [
    id 388
    label "fly"
  ]
  node [
    id 389
    label "umkn&#261;&#263;"
  ]
  node [
    id 390
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 391
    label "tent-fly"
  ]
  node [
    id 392
    label "w&#261;tpienie"
  ]
  node [
    id 393
    label "wypowied&#378;"
  ]
  node [
    id 394
    label "question"
  ]
  node [
    id 395
    label "wsp&#243;r"
  ]
  node [
    id 396
    label "clash"
  ]
  node [
    id 397
    label "konflikt"
  ]
  node [
    id 398
    label "obrona"
  ]
  node [
    id 399
    label "kompetentny"
  ]
  node [
    id 400
    label "kompetencyjnie"
  ]
  node [
    id 401
    label "rada"
  ]
  node [
    id 402
    label "komisja"
  ]
  node [
    id 403
    label "i"
  ]
  node [
    id 404
    label "rodzina"
  ]
  node [
    id 405
    label "biuro"
  ]
  node [
    id 406
    label "analiza"
  ]
  node [
    id 407
    label "sejmowy"
  ]
  node [
    id 408
    label "ustawa"
  ]
  node [
    id 409
    label "ojciec"
  ]
  node [
    id 410
    label "s&#322;u&#380;ba"
  ]
  node [
    id 411
    label "cywilny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 118
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 26
  ]
  edge [
    source 10
    target 38
  ]
  edge [
    source 10
    target 39
  ]
  edge [
    source 10
    target 129
  ]
  edge [
    source 10
    target 130
  ]
  edge [
    source 10
    target 131
  ]
  edge [
    source 10
    target 401
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 132
  ]
  edge [
    source 11
    target 133
  ]
  edge [
    source 11
    target 134
  ]
  edge [
    source 11
    target 135
  ]
  edge [
    source 11
    target 136
  ]
  edge [
    source 11
    target 137
  ]
  edge [
    source 11
    target 23
  ]
  edge [
    source 11
    target 138
  ]
  edge [
    source 11
    target 139
  ]
  edge [
    source 11
    target 140
  ]
  edge [
    source 11
    target 141
  ]
  edge [
    source 11
    target 142
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 143
  ]
  edge [
    source 12
    target 144
  ]
  edge [
    source 12
    target 145
  ]
  edge [
    source 12
    target 146
  ]
  edge [
    source 12
    target 147
  ]
  edge [
    source 12
    target 148
  ]
  edge [
    source 12
    target 149
  ]
  edge [
    source 12
    target 150
  ]
  edge [
    source 12
    target 151
  ]
  edge [
    source 12
    target 152
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 48
  ]
  edge [
    source 13
    target 153
  ]
  edge [
    source 13
    target 154
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 13
    target 155
  ]
  edge [
    source 13
    target 156
  ]
  edge [
    source 13
    target 157
  ]
  edge [
    source 13
    target 158
  ]
  edge [
    source 13
    target 159
  ]
  edge [
    source 13
    target 160
  ]
  edge [
    source 13
    target 161
  ]
  edge [
    source 13
    target 162
  ]
  edge [
    source 13
    target 163
  ]
  edge [
    source 13
    target 19
  ]
  edge [
    source 13
    target 164
  ]
  edge [
    source 13
    target 165
  ]
  edge [
    source 13
    target 166
  ]
  edge [
    source 13
    target 167
  ]
  edge [
    source 13
    target 168
  ]
  edge [
    source 13
    target 33
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 19
  ]
  edge [
    source 14
    target 20
  ]
  edge [
    source 14
    target 40
  ]
  edge [
    source 14
    target 41
  ]
  edge [
    source 14
    target 49
  ]
  edge [
    source 14
    target 169
  ]
  edge [
    source 14
    target 170
  ]
  edge [
    source 14
    target 171
  ]
  edge [
    source 14
    target 33
  ]
  edge [
    source 14
    target 18
  ]
  edge [
    source 14
    target 402
  ]
  edge [
    source 14
    target 403
  ]
  edge [
    source 14
    target 404
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 172
  ]
  edge [
    source 15
    target 99
  ]
  edge [
    source 15
    target 155
  ]
  edge [
    source 15
    target 173
  ]
  edge [
    source 15
    target 174
  ]
  edge [
    source 15
    target 175
  ]
  edge [
    source 15
    target 176
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 177
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 178
  ]
  edge [
    source 18
    target 179
  ]
  edge [
    source 18
    target 180
  ]
  edge [
    source 18
    target 181
  ]
  edge [
    source 18
    target 182
  ]
  edge [
    source 18
    target 183
  ]
  edge [
    source 18
    target 184
  ]
  edge [
    source 18
    target 185
  ]
  edge [
    source 18
    target 91
  ]
  edge [
    source 18
    target 186
  ]
  edge [
    source 18
    target 39
  ]
  edge [
    source 19
    target 187
  ]
  edge [
    source 19
    target 154
  ]
  edge [
    source 19
    target 188
  ]
  edge [
    source 19
    target 189
  ]
  edge [
    source 19
    target 190
  ]
  edge [
    source 19
    target 191
  ]
  edge [
    source 19
    target 192
  ]
  edge [
    source 19
    target 185
  ]
  edge [
    source 19
    target 193
  ]
  edge [
    source 19
    target 194
  ]
  edge [
    source 19
    target 195
  ]
  edge [
    source 19
    target 168
  ]
  edge [
    source 19
    target 196
  ]
  edge [
    source 19
    target 197
  ]
  edge [
    source 19
    target 198
  ]
  edge [
    source 19
    target 199
  ]
  edge [
    source 19
    target 25
  ]
  edge [
    source 19
    target 50
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 200
  ]
  edge [
    source 20
    target 201
  ]
  edge [
    source 20
    target 202
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 203
  ]
  edge [
    source 21
    target 204
  ]
  edge [
    source 21
    target 205
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 99
  ]
  edge [
    source 22
    target 206
  ]
  edge [
    source 22
    target 91
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 207
  ]
  edge [
    source 23
    target 208
  ]
  edge [
    source 23
    target 35
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 38
  ]
  edge [
    source 24
    target 209
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 210
  ]
  edge [
    source 25
    target 211
  ]
  edge [
    source 25
    target 50
  ]
  edge [
    source 26
    target 212
  ]
  edge [
    source 26
    target 213
  ]
  edge [
    source 26
    target 214
  ]
  edge [
    source 26
    target 215
  ]
  edge [
    source 26
    target 216
  ]
  edge [
    source 26
    target 217
  ]
  edge [
    source 26
    target 218
  ]
  edge [
    source 26
    target 219
  ]
  edge [
    source 26
    target 220
  ]
  edge [
    source 26
    target 221
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 222
  ]
  edge [
    source 27
    target 223
  ]
  edge [
    source 27
    target 224
  ]
  edge [
    source 27
    target 225
  ]
  edge [
    source 27
    target 226
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 172
  ]
  edge [
    source 28
    target 227
  ]
  edge [
    source 28
    target 228
  ]
  edge [
    source 28
    target 229
  ]
  edge [
    source 28
    target 230
  ]
  edge [
    source 28
    target 73
  ]
  edge [
    source 28
    target 231
  ]
  edge [
    source 28
    target 232
  ]
  edge [
    source 28
    target 233
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 234
  ]
  edge [
    source 29
    target 235
  ]
  edge [
    source 29
    target 236
  ]
  edge [
    source 29
    target 237
  ]
  edge [
    source 29
    target 238
  ]
  edge [
    source 29
    target 239
  ]
  edge [
    source 29
    target 127
  ]
  edge [
    source 29
    target 240
  ]
  edge [
    source 29
    target 241
  ]
  edge [
    source 29
    target 47
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 242
  ]
  edge [
    source 30
    target 243
  ]
  edge [
    source 30
    target 155
  ]
  edge [
    source 30
    target 99
  ]
  edge [
    source 30
    target 244
  ]
  edge [
    source 30
    target 245
  ]
  edge [
    source 30
    target 246
  ]
  edge [
    source 30
    target 247
  ]
  edge [
    source 30
    target 248
  ]
  edge [
    source 30
    target 249
  ]
  edge [
    source 30
    target 250
  ]
  edge [
    source 30
    target 251
  ]
  edge [
    source 30
    target 252
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 253
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 254
  ]
  edge [
    source 33
    target 255
  ]
  edge [
    source 33
    target 256
  ]
  edge [
    source 33
    target 257
  ]
  edge [
    source 33
    target 258
  ]
  edge [
    source 33
    target 259
  ]
  edge [
    source 33
    target 260
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 261
  ]
  edge [
    source 34
    target 262
  ]
  edge [
    source 34
    target 263
  ]
  edge [
    source 34
    target 264
  ]
  edge [
    source 34
    target 265
  ]
  edge [
    source 34
    target 266
  ]
  edge [
    source 34
    target 267
  ]
  edge [
    source 34
    target 268
  ]
  edge [
    source 34
    target 269
  ]
  edge [
    source 34
    target 270
  ]
  edge [
    source 34
    target 271
  ]
  edge [
    source 34
    target 272
  ]
  edge [
    source 34
    target 273
  ]
  edge [
    source 34
    target 274
  ]
  edge [
    source 34
    target 275
  ]
  edge [
    source 34
    target 276
  ]
  edge [
    source 34
    target 277
  ]
  edge [
    source 34
    target 278
  ]
  edge [
    source 34
    target 279
  ]
  edge [
    source 34
    target 280
  ]
  edge [
    source 34
    target 281
  ]
  edge [
    source 34
    target 282
  ]
  edge [
    source 34
    target 283
  ]
  edge [
    source 34
    target 284
  ]
  edge [
    source 34
    target 285
  ]
  edge [
    source 34
    target 286
  ]
  edge [
    source 34
    target 287
  ]
  edge [
    source 34
    target 288
  ]
  edge [
    source 34
    target 289
  ]
  edge [
    source 34
    target 290
  ]
  edge [
    source 34
    target 291
  ]
  edge [
    source 34
    target 292
  ]
  edge [
    source 34
    target 293
  ]
  edge [
    source 34
    target 294
  ]
  edge [
    source 34
    target 295
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 296
  ]
  edge [
    source 35
    target 297
  ]
  edge [
    source 35
    target 298
  ]
  edge [
    source 35
    target 299
  ]
  edge [
    source 35
    target 300
  ]
  edge [
    source 35
    target 301
  ]
  edge [
    source 35
    target 302
  ]
  edge [
    source 35
    target 303
  ]
  edge [
    source 35
    target 304
  ]
  edge [
    source 35
    target 305
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 306
  ]
  edge [
    source 36
    target 307
  ]
  edge [
    source 36
    target 308
  ]
  edge [
    source 36
    target 309
  ]
  edge [
    source 36
    target 310
  ]
  edge [
    source 36
    target 311
  ]
  edge [
    source 36
    target 312
  ]
  edge [
    source 36
    target 313
  ]
  edge [
    source 36
    target 184
  ]
  edge [
    source 36
    target 314
  ]
  edge [
    source 36
    target 315
  ]
  edge [
    source 36
    target 316
  ]
  edge [
    source 36
    target 317
  ]
  edge [
    source 36
    target 318
  ]
  edge [
    source 36
    target 319
  ]
  edge [
    source 36
    target 320
  ]
  edge [
    source 36
    target 321
  ]
  edge [
    source 36
    target 322
  ]
  edge [
    source 36
    target 323
  ]
  edge [
    source 36
    target 324
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 38
    target 325
  ]
  edge [
    source 38
    target 326
  ]
  edge [
    source 38
    target 327
  ]
  edge [
    source 38
    target 328
  ]
  edge [
    source 38
    target 329
  ]
  edge [
    source 38
    target 330
  ]
  edge [
    source 38
    target 331
  ]
  edge [
    source 38
    target 332
  ]
  edge [
    source 38
    target 333
  ]
  edge [
    source 38
    target 334
  ]
  edge [
    source 38
    target 335
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 336
  ]
  edge [
    source 39
    target 337
  ]
  edge [
    source 39
    target 338
  ]
  edge [
    source 39
    target 339
  ]
  edge [
    source 39
    target 340
  ]
  edge [
    source 39
    target 341
  ]
  edge [
    source 39
    target 342
  ]
  edge [
    source 39
    target 343
  ]
  edge [
    source 39
    target 344
  ]
  edge [
    source 39
    target 345
  ]
  edge [
    source 39
    target 346
  ]
  edge [
    source 39
    target 347
  ]
  edge [
    source 39
    target 348
  ]
  edge [
    source 39
    target 349
  ]
  edge [
    source 39
    target 99
  ]
  edge [
    source 39
    target 350
  ]
  edge [
    source 39
    target 351
  ]
  edge [
    source 39
    target 352
  ]
  edge [
    source 39
    target 353
  ]
  edge [
    source 39
    target 354
  ]
  edge [
    source 39
    target 355
  ]
  edge [
    source 39
    target 356
  ]
  edge [
    source 40
    target 357
  ]
  edge [
    source 40
    target 358
  ]
  edge [
    source 40
    target 359
  ]
  edge [
    source 40
    target 360
  ]
  edge [
    source 40
    target 167
  ]
  edge [
    source 40
    target 402
  ]
  edge [
    source 40
    target 403
  ]
  edge [
    source 40
    target 404
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 361
  ]
  edge [
    source 43
    target 362
  ]
  edge [
    source 43
    target 363
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 364
  ]
  edge [
    source 44
    target 144
  ]
  edge [
    source 44
    target 365
  ]
  edge [
    source 44
    target 366
  ]
  edge [
    source 44
    target 148
  ]
  edge [
    source 44
    target 149
  ]
  edge [
    source 44
    target 150
  ]
  edge [
    source 44
    target 116
  ]
  edge [
    source 44
    target 367
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 368
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 369
  ]
  edge [
    source 46
    target 370
  ]
  edge [
    source 46
    target 371
  ]
  edge [
    source 46
    target 209
  ]
  edge [
    source 46
    target 372
  ]
  edge [
    source 46
    target 373
  ]
  edge [
    source 46
    target 374
  ]
  edge [
    source 48
    target 375
  ]
  edge [
    source 48
    target 376
  ]
  edge [
    source 48
    target 377
  ]
  edge [
    source 48
    target 378
  ]
  edge [
    source 48
    target 379
  ]
  edge [
    source 48
    target 250
  ]
  edge [
    source 48
    target 380
  ]
  edge [
    source 48
    target 381
  ]
  edge [
    source 48
    target 382
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 383
  ]
  edge [
    source 49
    target 384
  ]
  edge [
    source 49
    target 385
  ]
  edge [
    source 49
    target 386
  ]
  edge [
    source 49
    target 387
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 126
  ]
  edge [
    source 50
    target 388
  ]
  edge [
    source 50
    target 389
  ]
  edge [
    source 50
    target 390
  ]
  edge [
    source 50
    target 391
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 392
  ]
  edge [
    source 51
    target 393
  ]
  edge [
    source 51
    target 343
  ]
  edge [
    source 51
    target 394
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 395
  ]
  edge [
    source 52
    target 396
  ]
  edge [
    source 52
    target 397
  ]
  edge [
    source 52
    target 398
  ]
  edge [
    source 53
    target 399
  ]
  edge [
    source 53
    target 400
  ]
  edge [
    source 402
    target 403
  ]
  edge [
    source 402
    target 404
  ]
  edge [
    source 403
    target 404
  ]
  edge [
    source 405
    target 406
  ]
  edge [
    source 405
    target 407
  ]
  edge [
    source 406
    target 407
  ]
  edge [
    source 408
    target 409
  ]
  edge [
    source 408
    target 410
  ]
  edge [
    source 408
    target 411
  ]
  edge [
    source 409
    target 410
  ]
  edge [
    source 409
    target 411
  ]
  edge [
    source 410
    target 411
  ]
]
