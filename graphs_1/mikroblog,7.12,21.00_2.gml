graph [
  maxDegree 13
  minDegree 1
  meanDegree 2
  density 0.02702702702702703
  graphCliqueNumber 2
  node [
    id 0
    label "poprzedni"
    origin "text"
  ]
  node [
    id 1
    label "paczek"
    origin "text"
  ]
  node [
    id 2
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 3
    label "wys&#322;a&#263;"
    origin "text"
  ]
  node [
    id 4
    label "razem"
    origin "text"
  ]
  node [
    id 5
    label "kolejny"
    origin "text"
  ]
  node [
    id 6
    label "rozdajo"
    origin "text"
  ]
  node [
    id 7
    label "jeszcze"
    origin "text"
  ]
  node [
    id 8
    label "jeden"
    origin "text"
  ]
  node [
    id 9
    label "mysterybox"
    origin "text"
  ]
  node [
    id 10
    label "kategoria"
    origin "text"
  ]
  node [
    id 11
    label "elektronik"
    origin "text"
  ]
  node [
    id 12
    label "gad&#380;et"
    origin "text"
  ]
  node [
    id 13
    label "elektroniczny"
    origin "text"
  ]
  node [
    id 14
    label "kabel"
    origin "text"
  ]
  node [
    id 15
    label "usb"
    origin "text"
  ]
  node [
    id 16
    label "pami&#281;&#263;"
    origin "text"
  ]
  node [
    id 17
    label "ram"
    origin "text"
  ]
  node [
    id 18
    label "poprzednio"
  ]
  node [
    id 19
    label "przesz&#322;y"
  ]
  node [
    id 20
    label "wcze&#347;niejszy"
  ]
  node [
    id 21
    label "wytworzy&#263;"
  ]
  node [
    id 22
    label "line"
  ]
  node [
    id 23
    label "ship"
  ]
  node [
    id 24
    label "convey"
  ]
  node [
    id 25
    label "przekaza&#263;"
  ]
  node [
    id 26
    label "post"
  ]
  node [
    id 27
    label "wy&#322;o&#380;y&#263;"
  ]
  node [
    id 28
    label "nakaza&#263;"
  ]
  node [
    id 29
    label "&#322;&#261;cznie"
  ]
  node [
    id 30
    label "inny"
  ]
  node [
    id 31
    label "nast&#281;pnie"
  ]
  node [
    id 32
    label "kt&#243;ry&#347;"
  ]
  node [
    id 33
    label "kolejno"
  ]
  node [
    id 34
    label "nastopny"
  ]
  node [
    id 35
    label "ci&#261;gle"
  ]
  node [
    id 36
    label "kieliszek"
  ]
  node [
    id 37
    label "shot"
  ]
  node [
    id 38
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 39
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 40
    label "jaki&#347;"
  ]
  node [
    id 41
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 42
    label "jednolicie"
  ]
  node [
    id 43
    label "w&#243;dka"
  ]
  node [
    id 44
    label "ten"
  ]
  node [
    id 45
    label "ujednolicenie"
  ]
  node [
    id 46
    label "jednakowy"
  ]
  node [
    id 47
    label "forma"
  ]
  node [
    id 48
    label "wytw&#243;r"
  ]
  node [
    id 49
    label "type"
  ]
  node [
    id 50
    label "teoria"
  ]
  node [
    id 51
    label "zbi&#243;r"
  ]
  node [
    id 52
    label "poj&#281;cie"
  ]
  node [
    id 53
    label "klasa"
  ]
  node [
    id 54
    label "in&#380;ynier"
  ]
  node [
    id 55
    label "technik"
  ]
  node [
    id 56
    label "przedmiot"
  ]
  node [
    id 57
    label "elektronicznie"
  ]
  node [
    id 58
    label "elektrycznie"
  ]
  node [
    id 59
    label "linia_telefoniczna"
  ]
  node [
    id 60
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 61
    label "okablowanie"
  ]
  node [
    id 62
    label "donosiciel"
  ]
  node [
    id 63
    label "instalacja_elektryczna"
  ]
  node [
    id 64
    label "przew&#243;d"
  ]
  node [
    id 65
    label "jednostka_miary_u&#380;ywana_w_&#380;egludze"
  ]
  node [
    id 66
    label "hipokamp"
  ]
  node [
    id 67
    label "wymazanie"
  ]
  node [
    id 68
    label "zachowa&#263;"
  ]
  node [
    id 69
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 70
    label "memory"
  ]
  node [
    id 71
    label "pami&#281;tno&#347;&#263;"
  ]
  node [
    id 72
    label "umys&#322;"
  ]
  node [
    id 73
    label "sprz&#281;t_komputerowy"
  ]
  node [
    id 74
    label "komputer"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 35
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 36
  ]
  edge [
    source 8
    target 37
  ]
  edge [
    source 8
    target 38
  ]
  edge [
    source 8
    target 39
  ]
  edge [
    source 8
    target 40
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 8
    target 46
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 47
  ]
  edge [
    source 10
    target 48
  ]
  edge [
    source 10
    target 49
  ]
  edge [
    source 10
    target 50
  ]
  edge [
    source 10
    target 51
  ]
  edge [
    source 10
    target 52
  ]
  edge [
    source 10
    target 53
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 54
  ]
  edge [
    source 11
    target 55
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 56
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 57
  ]
  edge [
    source 13
    target 58
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 59
  ]
  edge [
    source 14
    target 60
  ]
  edge [
    source 14
    target 61
  ]
  edge [
    source 14
    target 62
  ]
  edge [
    source 14
    target 63
  ]
  edge [
    source 14
    target 64
  ]
  edge [
    source 14
    target 65
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 66
  ]
  edge [
    source 16
    target 67
  ]
  edge [
    source 16
    target 48
  ]
  edge [
    source 16
    target 68
  ]
  edge [
    source 16
    target 69
  ]
  edge [
    source 16
    target 70
  ]
  edge [
    source 16
    target 71
  ]
  edge [
    source 16
    target 72
  ]
  edge [
    source 16
    target 73
  ]
  edge [
    source 16
    target 74
  ]
]
