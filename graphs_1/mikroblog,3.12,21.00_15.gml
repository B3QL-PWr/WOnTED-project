graph [
  maxDegree 16
  minDegree 1
  meanDegree 2
  density 0.014084507042253521
  graphCliqueNumber 2
  node [
    id 0
    label "nudzi&#263;"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "wczoraj"
    origin "text"
  ]
  node [
    id 3
    label "za&#322;o&#380;y&#263;by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "fake"
    origin "text"
  ]
  node [
    id 5
    label "konto"
    origin "text"
  ]
  node [
    id 6
    label "r&#243;&#380;owy"
    origin "text"
  ]
  node [
    id 7
    label "tinder"
    origin "text"
  ]
  node [
    id 8
    label "pare"
    origin "text"
  ]
  node [
    id 9
    label "minuta"
    origin "text"
  ]
  node [
    id 10
    label "jaki&#347;"
    origin "text"
  ]
  node [
    id 11
    label "typ"
    origin "text"
  ]
  node [
    id 12
    label "podawa&#263;"
    origin "text"
  ]
  node [
    id 13
    label "adres"
    origin "text"
  ]
  node [
    id 14
    label "siebie"
    origin "text"
  ]
  node [
    id 15
    label "pyta"
    origin "text"
  ]
  node [
    id 16
    label "jeszcze"
    origin "text"
  ]
  node [
    id 17
    label "luba"
    origin "text"
  ]
  node [
    id 18
    label "&#347;niadanie"
    origin "text"
  ]
  node [
    id 19
    label "my&#347;l"
    origin "text"
  ]
  node [
    id 20
    label "co&#347;"
    origin "text"
  ]
  node [
    id 21
    label "by&#263;"
    origin "text"
  ]
  node [
    id 22
    label "rant"
  ]
  node [
    id 23
    label "gada&#263;"
  ]
  node [
    id 24
    label "buttonhole"
  ]
  node [
    id 25
    label "wzbudza&#263;"
  ]
  node [
    id 26
    label "harass"
  ]
  node [
    id 27
    label "narzeka&#263;"
  ]
  node [
    id 28
    label "chat_up"
  ]
  node [
    id 29
    label "naprzykrza&#263;_si&#281;"
  ]
  node [
    id 30
    label "nalega&#263;"
  ]
  node [
    id 31
    label "doba"
  ]
  node [
    id 32
    label "dawno"
  ]
  node [
    id 33
    label "niedawno"
  ]
  node [
    id 34
    label "ksi&#281;gowo&#347;&#263;"
  ]
  node [
    id 35
    label "dost&#281;p"
  ]
  node [
    id 36
    label "bank"
  ]
  node [
    id 37
    label "kariera"
  ]
  node [
    id 38
    label "rejestrowa&#263;_si&#281;"
  ]
  node [
    id 39
    label "dorobek"
  ]
  node [
    id 40
    label "debet"
  ]
  node [
    id 41
    label "rachunek"
  ]
  node [
    id 42
    label "logowa&#263;_si&#281;"
  ]
  node [
    id 43
    label "mienie"
  ]
  node [
    id 44
    label "subkonto"
  ]
  node [
    id 45
    label "kredyt"
  ]
  node [
    id 46
    label "reprezentacja"
  ]
  node [
    id 47
    label "zar&#243;&#380;owienie_si&#281;"
  ]
  node [
    id 48
    label "zar&#243;&#380;owienie"
  ]
  node [
    id 49
    label "r&#243;&#380;owo"
  ]
  node [
    id 50
    label "r&#243;&#380;owienie"
  ]
  node [
    id 51
    label "weso&#322;y"
  ]
  node [
    id 52
    label "czerwonawy"
  ]
  node [
    id 53
    label "optymistyczny"
  ]
  node [
    id 54
    label "zapis"
  ]
  node [
    id 55
    label "godzina"
  ]
  node [
    id 56
    label "sekunda"
  ]
  node [
    id 57
    label "kwadrans"
  ]
  node [
    id 58
    label "stopie&#324;"
  ]
  node [
    id 59
    label "design"
  ]
  node [
    id 60
    label "time"
  ]
  node [
    id 61
    label "jednostka"
  ]
  node [
    id 62
    label "jako&#347;"
  ]
  node [
    id 63
    label "charakterystyczny"
  ]
  node [
    id 64
    label "ciekawy"
  ]
  node [
    id 65
    label "jako_tako"
  ]
  node [
    id 66
    label "dziwny"
  ]
  node [
    id 67
    label "niez&#322;y"
  ]
  node [
    id 68
    label "przyzwoity"
  ]
  node [
    id 69
    label "cz&#322;owiek"
  ]
  node [
    id 70
    label "gromada"
  ]
  node [
    id 71
    label "autorament"
  ]
  node [
    id 72
    label "przypuszczenie"
  ]
  node [
    id 73
    label "cynk"
  ]
  node [
    id 74
    label "rezultat"
  ]
  node [
    id 75
    label "jednostka_systematyczna"
  ]
  node [
    id 76
    label "kr&#243;lestwo"
  ]
  node [
    id 77
    label "obstawia&#263;"
  ]
  node [
    id 78
    label "facet"
  ]
  node [
    id 79
    label "variety"
  ]
  node [
    id 80
    label "sztuka"
  ]
  node [
    id 81
    label "antycypacja"
  ]
  node [
    id 82
    label "tenis"
  ]
  node [
    id 83
    label "cover"
  ]
  node [
    id 84
    label "siatk&#243;wka"
  ]
  node [
    id 85
    label "dawa&#263;"
  ]
  node [
    id 86
    label "faszerowa&#263;"
  ]
  node [
    id 87
    label "informowa&#263;"
  ]
  node [
    id 88
    label "introduce"
  ]
  node [
    id 89
    label "jedzenie"
  ]
  node [
    id 90
    label "tender"
  ]
  node [
    id 91
    label "deal"
  ]
  node [
    id 92
    label "kelner"
  ]
  node [
    id 93
    label "serwowa&#263;"
  ]
  node [
    id 94
    label "rozgrywa&#263;"
  ]
  node [
    id 95
    label "stawia&#263;"
  ]
  node [
    id 96
    label "strona"
  ]
  node [
    id 97
    label "adres_elektroniczny"
  ]
  node [
    id 98
    label "domena"
  ]
  node [
    id 99
    label "po&#322;o&#380;enie"
  ]
  node [
    id 100
    label "kod_pocztowy"
  ]
  node [
    id 101
    label "dane"
  ]
  node [
    id 102
    label "pismo"
  ]
  node [
    id 103
    label "przesy&#322;ka"
  ]
  node [
    id 104
    label "personalia"
  ]
  node [
    id 105
    label "siedziba"
  ]
  node [
    id 106
    label "dziedzina"
  ]
  node [
    id 107
    label "narz&#281;dzie_wymierzania_kary"
  ]
  node [
    id 108
    label "penis"
  ]
  node [
    id 109
    label "ci&#261;gle"
  ]
  node [
    id 110
    label "ptaszyna"
  ]
  node [
    id 111
    label "umi&#322;owana"
  ]
  node [
    id 112
    label "kochanka"
  ]
  node [
    id 113
    label "kochanie"
  ]
  node [
    id 114
    label "Dulcynea"
  ]
  node [
    id 115
    label "wybranka"
  ]
  node [
    id 116
    label "posi&#322;ek"
  ]
  node [
    id 117
    label "system"
  ]
  node [
    id 118
    label "s&#261;d"
  ]
  node [
    id 119
    label "wytw&#243;r"
  ]
  node [
    id 120
    label "istota"
  ]
  node [
    id 121
    label "thinking"
  ]
  node [
    id 122
    label "idea"
  ]
  node [
    id 123
    label "political_orientation"
  ]
  node [
    id 124
    label "pomys&#322;"
  ]
  node [
    id 125
    label "szko&#322;a"
  ]
  node [
    id 126
    label "umys&#322;"
  ]
  node [
    id 127
    label "fantomatyka"
  ]
  node [
    id 128
    label "t&#322;oczenie_si&#281;"
  ]
  node [
    id 129
    label "p&#322;&#243;d"
  ]
  node [
    id 130
    label "thing"
  ]
  node [
    id 131
    label "cosik"
  ]
  node [
    id 132
    label "si&#281;ga&#263;"
  ]
  node [
    id 133
    label "trwa&#263;"
  ]
  node [
    id 134
    label "obecno&#347;&#263;"
  ]
  node [
    id 135
    label "stan"
  ]
  node [
    id 136
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 137
    label "stand"
  ]
  node [
    id 138
    label "mie&#263;_miejsce"
  ]
  node [
    id 139
    label "uczestniczy&#263;"
  ]
  node [
    id 140
    label "chodzi&#263;"
  ]
  node [
    id 141
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 142
    label "equal"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 54
  ]
  edge [
    source 9
    target 55
  ]
  edge [
    source 9
    target 56
  ]
  edge [
    source 9
    target 57
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 62
  ]
  edge [
    source 10
    target 63
  ]
  edge [
    source 10
    target 64
  ]
  edge [
    source 10
    target 65
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 69
  ]
  edge [
    source 11
    target 70
  ]
  edge [
    source 11
    target 71
  ]
  edge [
    source 11
    target 72
  ]
  edge [
    source 11
    target 73
  ]
  edge [
    source 11
    target 74
  ]
  edge [
    source 11
    target 75
  ]
  edge [
    source 11
    target 76
  ]
  edge [
    source 11
    target 77
  ]
  edge [
    source 11
    target 59
  ]
  edge [
    source 11
    target 78
  ]
  edge [
    source 11
    target 79
  ]
  edge [
    source 11
    target 80
  ]
  edge [
    source 11
    target 81
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 82
  ]
  edge [
    source 12
    target 83
  ]
  edge [
    source 12
    target 84
  ]
  edge [
    source 12
    target 85
  ]
  edge [
    source 12
    target 86
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 12
    target 89
  ]
  edge [
    source 12
    target 90
  ]
  edge [
    source 12
    target 91
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 12
    target 94
  ]
  edge [
    source 12
    target 95
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 96
  ]
  edge [
    source 13
    target 97
  ]
  edge [
    source 13
    target 98
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 13
    target 101
  ]
  edge [
    source 13
    target 102
  ]
  edge [
    source 13
    target 103
  ]
  edge [
    source 13
    target 104
  ]
  edge [
    source 13
    target 105
  ]
  edge [
    source 13
    target 106
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 107
  ]
  edge [
    source 15
    target 108
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 109
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 110
  ]
  edge [
    source 17
    target 111
  ]
  edge [
    source 17
    target 112
  ]
  edge [
    source 17
    target 113
  ]
  edge [
    source 17
    target 114
  ]
  edge [
    source 17
    target 115
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 116
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 117
  ]
  edge [
    source 19
    target 118
  ]
  edge [
    source 19
    target 119
  ]
  edge [
    source 19
    target 120
  ]
  edge [
    source 19
    target 121
  ]
  edge [
    source 19
    target 122
  ]
  edge [
    source 19
    target 123
  ]
  edge [
    source 19
    target 124
  ]
  edge [
    source 19
    target 125
  ]
  edge [
    source 19
    target 126
  ]
  edge [
    source 19
    target 127
  ]
  edge [
    source 19
    target 128
  ]
  edge [
    source 19
    target 129
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 130
  ]
  edge [
    source 20
    target 131
  ]
  edge [
    source 21
    target 132
  ]
  edge [
    source 21
    target 133
  ]
  edge [
    source 21
    target 134
  ]
  edge [
    source 21
    target 135
  ]
  edge [
    source 21
    target 136
  ]
  edge [
    source 21
    target 137
  ]
  edge [
    source 21
    target 138
  ]
  edge [
    source 21
    target 139
  ]
  edge [
    source 21
    target 140
  ]
  edge [
    source 21
    target 141
  ]
  edge [
    source 21
    target 142
  ]
]
