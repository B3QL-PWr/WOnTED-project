graph [
  maxDegree 228
  minDegree 1
  meanDegree 2.404833836858006
  density 0.00242422765812299
  graphCliqueNumber 6
  node [
    id 0
    label "tak"
    origin "text"
  ]
  node [
    id 1
    label "powiedzie&#263;"
    origin "text"
  ]
  node [
    id 2
    label "pierwsza"
    origin "text"
  ]
  node [
    id 3
    label "cz&#281;&#347;&#263;"
    origin "text"
  ]
  node [
    id 4
    label "druga"
    origin "text"
  ]
  node [
    id 5
    label "dotyczy&#263;"
    origin "text"
  ]
  node [
    id 6
    label "kwestia"
    origin "text"
  ]
  node [
    id 7
    label "tychy"
    origin "text"
  ]
  node [
    id 8
    label "dwa"
    origin "text"
  ]
  node [
    id 9
    label "ustawa"
    origin "text"
  ]
  node [
    id 10
    label "jaki"
    origin "text"
  ]
  node [
    id 11
    label "socjaldemokracja"
    origin "text"
  ]
  node [
    id 12
    label "polska"
    origin "text"
  ]
  node [
    id 13
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 14
    label "propozycja"
    origin "text"
  ]
  node [
    id 15
    label "oczywi&#347;cie"
    origin "text"
  ]
  node [
    id 16
    label "je&#347;li"
    origin "text"
  ]
  node [
    id 17
    label "chodzi&#263;"
    origin "text"
  ]
  node [
    id 18
    label "proponowa&#263;"
    origin "text"
  ]
  node [
    id 19
    label "przez"
    origin "text"
  ]
  node [
    id 20
    label "pani"
    origin "text"
  ]
  node [
    id 21
    label "pose&#322;"
    origin "text"
  ]
  node [
    id 22
    label "kidawa"
    origin "text"
  ]
  node [
    id 23
    label "b&#322;o&#324;ska"
    origin "text"
  ]
  node [
    id 24
    label "powinny"
    origin "text"
  ]
  node [
    id 25
    label "by&#263;"
    origin "text"
  ]
  node [
    id 26
    label "przedmiot"
    origin "text"
  ]
  node [
    id 27
    label "daleki"
    origin "text"
  ]
  node [
    id 28
    label "praca"
    origin "text"
  ]
  node [
    id 29
    label "mama"
    origin "text"
  ]
  node [
    id 30
    label "problem"
    origin "text"
  ]
  node [
    id 31
    label "pan"
    origin "text"
  ]
  node [
    id 32
    label "gowina"
    origin "text"
  ]
  node [
    id 33
    label "zgadza&#263;"
    origin "text"
  ]
  node [
    id 34
    label "si&#281;"
    origin "text"
  ]
  node [
    id 35
    label "bowiem"
    origin "text"
  ]
  node [
    id 36
    label "wypowied&#378;"
    origin "text"
  ]
  node [
    id 37
    label "pa&#324;stwo"
    origin "text"
  ]
  node [
    id 38
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 39
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 40
    label "faktycznie"
    origin "text"
  ]
  node [
    id 41
    label "zahamowa&#263;"
    origin "text"
  ]
  node [
    id 42
    label "ten"
    origin "text"
  ]
  node [
    id 43
    label "proces"
    origin "text"
  ]
  node [
    id 44
    label "znaczy&#263;"
    origin "text"
  ]
  node [
    id 45
    label "wyeliminowa&#263;"
    origin "text"
  ]
  node [
    id 46
    label "zupe&#322;nie"
    origin "text"
  ]
  node [
    id 47
    label "ale"
    origin "text"
  ]
  node [
    id 48
    label "przyk&#322;ad"
    origin "text"
  ]
  node [
    id 49
    label "pa&#324;ski"
    origin "text"
  ]
  node [
    id 50
    label "&#380;eby"
    origin "text"
  ]
  node [
    id 51
    label "odm&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 52
    label "prawa"
    origin "text"
  ]
  node [
    id 53
    label "selekcjonowa&#263;"
    origin "text"
  ]
  node [
    id 54
    label "brzmie&#263;"
    origin "text"
  ]
  node [
    id 55
    label "gro&#378;nie"
    origin "text"
  ]
  node [
    id 56
    label "tutaj"
    origin "text"
  ]
  node [
    id 57
    label "prosty"
    origin "text"
  ]
  node [
    id 58
    label "wszczepia&#263;"
    origin "text"
  ]
  node [
    id 59
    label "zarodek"
    origin "text"
  ]
  node [
    id 60
    label "obci&#261;&#380;y&#263;"
    origin "text"
  ]
  node [
    id 61
    label "wada"
    origin "text"
  ]
  node [
    id 62
    label "genetyczny"
    origin "text"
  ]
  node [
    id 63
    label "chcie&#263;"
    origin "text"
  ]
  node [
    id 64
    label "wszczepi&#263;"
    origin "text"
  ]
  node [
    id 65
    label "nast&#281;pnie"
    origin "text"
  ]
  node [
    id 66
    label "kobieta"
    origin "text"
  ]
  node [
    id 67
    label "prawo"
    origin "text"
  ]
  node [
    id 68
    label "aborcja"
    origin "text"
  ]
  node [
    id 69
    label "zgodnie"
    origin "text"
  ]
  node [
    id 70
    label "obecna"
    origin "text"
  ]
  node [
    id 71
    label "restrykcyjny"
    origin "text"
  ]
  node [
    id 72
    label "podawa&#263;"
    origin "text"
  ]
  node [
    id 73
    label "prosi&#263;"
    origin "text"
  ]
  node [
    id 74
    label "dopiero"
    origin "text"
  ]
  node [
    id 75
    label "humanitarny"
    origin "text"
  ]
  node [
    id 76
    label "wielki"
    origin "text"
  ]
  node [
    id 77
    label "humanitaryzm"
    origin "text"
  ]
  node [
    id 78
    label "s&#322;abo&#347;&#263;"
    origin "text"
  ]
  node [
    id 79
    label "pa&#324;skie"
    origin "text"
  ]
  node [
    id 80
    label "projekt"
    origin "text"
  ]
  node [
    id 81
    label "siebie"
    origin "text"
  ]
  node [
    id 82
    label "pewnie"
    origin "text"
  ]
  node [
    id 83
    label "zdawa&#263;"
    origin "text"
  ]
  node [
    id 84
    label "sprawa"
    origin "text"
  ]
  node [
    id 85
    label "dobry"
    origin "text"
  ]
  node [
    id 86
    label "punkt"
    origin "text"
  ]
  node [
    id 87
    label "widzenie"
    origin "text"
  ]
  node [
    id 88
    label "powinienby&#263;"
    origin "text"
  ]
  node [
    id 89
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 90
    label "z&#322;o&#380;ony"
    origin "text"
  ]
  node [
    id 91
    label "taki"
    origin "text"
  ]
  node [
    id 92
    label "wniosek"
    origin "text"
  ]
  node [
    id 93
    label "jeden"
    origin "text"
  ]
  node [
    id 94
    label "pow&#243;d"
    origin "text"
  ]
  node [
    id 95
    label "gowin"
    origin "text"
  ]
  node [
    id 96
    label "wykaza&#263;"
    origin "text"
  ]
  node [
    id 97
    label "odwaga"
    origin "text"
  ]
  node [
    id 98
    label "cywilny"
    origin "text"
  ]
  node [
    id 99
    label "wbrew"
    origin "text"
  ]
  node [
    id 100
    label "stanowisko"
    origin "text"
  ]
  node [
    id 101
    label "hierarcha"
    origin "text"
  ]
  node [
    id 102
    label "biskup"
    origin "text"
  ]
  node [
    id 103
    label "hoser"
    origin "text"
  ]
  node [
    id 104
    label "gro&#378;ba"
    origin "text"
  ]
  node [
    id 105
    label "ekskomunika"
    origin "text"
  ]
  node [
    id 106
    label "wyst&#261;pi&#263;"
    origin "text"
  ]
  node [
    id 107
    label "tym"
    origin "text"
  ]
  node [
    id 108
    label "nadal"
    origin "text"
  ]
  node [
    id 109
    label "popiera&#263;"
    origin "text"
  ]
  node [
    id 110
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 111
    label "express"
  ]
  node [
    id 112
    label "rzekn&#261;&#263;"
  ]
  node [
    id 113
    label "okre&#347;li&#263;"
  ]
  node [
    id 114
    label "wyrazi&#263;"
  ]
  node [
    id 115
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 116
    label "unwrap"
  ]
  node [
    id 117
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 118
    label "convey"
  ]
  node [
    id 119
    label "discover"
  ]
  node [
    id 120
    label "wydoby&#263;"
  ]
  node [
    id 121
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 122
    label "poda&#263;"
  ]
  node [
    id 123
    label "godzina"
  ]
  node [
    id 124
    label "whole"
  ]
  node [
    id 125
    label "Rzym_Zachodni"
  ]
  node [
    id 126
    label "element"
  ]
  node [
    id 127
    label "ilo&#347;&#263;"
  ]
  node [
    id 128
    label "urz&#261;dzenie"
  ]
  node [
    id 129
    label "Rzym_Wschodni"
  ]
  node [
    id 130
    label "bargain"
  ]
  node [
    id 131
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 132
    label "tycze&#263;"
  ]
  node [
    id 133
    label "problemat"
  ]
  node [
    id 134
    label "dialog"
  ]
  node [
    id 135
    label "problematyka"
  ]
  node [
    id 136
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 137
    label "subject"
  ]
  node [
    id 138
    label "Karta_Nauczyciela"
  ]
  node [
    id 139
    label "marc&#243;wka"
  ]
  node [
    id 140
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 141
    label "akt"
  ]
  node [
    id 142
    label "przej&#347;&#263;"
  ]
  node [
    id 143
    label "charter"
  ]
  node [
    id 144
    label "przej&#347;cie"
  ]
  node [
    id 145
    label "mienszewizm"
  ]
  node [
    id 146
    label "socjaldemokrata"
  ]
  node [
    id 147
    label "ideologia"
  ]
  node [
    id 148
    label "ruch"
  ]
  node [
    id 149
    label "lewica"
  ]
  node [
    id 150
    label "czyj&#347;"
  ]
  node [
    id 151
    label "m&#261;&#380;"
  ]
  node [
    id 152
    label "pomys&#322;"
  ]
  node [
    id 153
    label "proposal"
  ]
  node [
    id 154
    label "proceed"
  ]
  node [
    id 155
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 156
    label "bangla&#263;"
  ]
  node [
    id 157
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 158
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 159
    label "run"
  ]
  node [
    id 160
    label "tryb"
  ]
  node [
    id 161
    label "p&#322;ywa&#263;"
  ]
  node [
    id 162
    label "continue"
  ]
  node [
    id 163
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 164
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 165
    label "przebiega&#263;"
  ]
  node [
    id 166
    label "mie&#263;_miejsce"
  ]
  node [
    id 167
    label "wk&#322;ada&#263;"
  ]
  node [
    id 168
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 169
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 170
    label "para"
  ]
  node [
    id 171
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 172
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 173
    label "krok"
  ]
  node [
    id 174
    label "str&#243;j"
  ]
  node [
    id 175
    label "bywa&#263;"
  ]
  node [
    id 176
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 177
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 178
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 179
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 180
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 181
    label "dziama&#263;"
  ]
  node [
    id 182
    label "stara&#263;_si&#281;"
  ]
  node [
    id 183
    label "carry"
  ]
  node [
    id 184
    label "report"
  ]
  node [
    id 185
    label "volunteer"
  ]
  node [
    id 186
    label "informowa&#263;"
  ]
  node [
    id 187
    label "zach&#281;ca&#263;"
  ]
  node [
    id 188
    label "wnioskowa&#263;"
  ]
  node [
    id 189
    label "suggest"
  ]
  node [
    id 190
    label "kandydatura"
  ]
  node [
    id 191
    label "cz&#322;owiek"
  ]
  node [
    id 192
    label "przekwitanie"
  ]
  node [
    id 193
    label "zwrot"
  ]
  node [
    id 194
    label "uleganie"
  ]
  node [
    id 195
    label "ulega&#263;"
  ]
  node [
    id 196
    label "partner"
  ]
  node [
    id 197
    label "doros&#322;y"
  ]
  node [
    id 198
    label "przyw&#243;dczyni"
  ]
  node [
    id 199
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 200
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 201
    label "ulec"
  ]
  node [
    id 202
    label "kobita"
  ]
  node [
    id 203
    label "&#322;ono"
  ]
  node [
    id 204
    label "w&#322;a&#347;cicielka"
  ]
  node [
    id 205
    label "m&#281;&#380;yna"
  ]
  node [
    id 206
    label "babka"
  ]
  node [
    id 207
    label "samica"
  ]
  node [
    id 208
    label "ulegni&#281;cie"
  ]
  node [
    id 209
    label "menopauza"
  ]
  node [
    id 210
    label "dyplomata"
  ]
  node [
    id 211
    label "wys&#322;annik"
  ]
  node [
    id 212
    label "przedstawiciel"
  ]
  node [
    id 213
    label "kurier_dyplomatyczny"
  ]
  node [
    id 214
    label "ablegat"
  ]
  node [
    id 215
    label "klubista"
  ]
  node [
    id 216
    label "Miko&#322;ajczyk"
  ]
  node [
    id 217
    label "Korwin"
  ]
  node [
    id 218
    label "parlamentarzysta"
  ]
  node [
    id 219
    label "dyscyplina_partyjna"
  ]
  node [
    id 220
    label "izba_ni&#380;sza"
  ]
  node [
    id 221
    label "poselstwo"
  ]
  node [
    id 222
    label "nale&#380;ny"
  ]
  node [
    id 223
    label "si&#281;ga&#263;"
  ]
  node [
    id 224
    label "trwa&#263;"
  ]
  node [
    id 225
    label "obecno&#347;&#263;"
  ]
  node [
    id 226
    label "stan"
  ]
  node [
    id 227
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 228
    label "stand"
  ]
  node [
    id 229
    label "uczestniczy&#263;"
  ]
  node [
    id 230
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 231
    label "equal"
  ]
  node [
    id 232
    label "robienie"
  ]
  node [
    id 233
    label "kr&#261;&#380;enie"
  ]
  node [
    id 234
    label "rzecz"
  ]
  node [
    id 235
    label "zbacza&#263;"
  ]
  node [
    id 236
    label "entity"
  ]
  node [
    id 237
    label "omawia&#263;"
  ]
  node [
    id 238
    label "om&#243;wi&#263;"
  ]
  node [
    id 239
    label "sponiewiera&#263;"
  ]
  node [
    id 240
    label "sponiewieranie"
  ]
  node [
    id 241
    label "omawianie"
  ]
  node [
    id 242
    label "charakter"
  ]
  node [
    id 243
    label "program_nauczania"
  ]
  node [
    id 244
    label "w&#261;tek"
  ]
  node [
    id 245
    label "thing"
  ]
  node [
    id 246
    label "zboczenie"
  ]
  node [
    id 247
    label "zbaczanie"
  ]
  node [
    id 248
    label "tre&#347;&#263;"
  ]
  node [
    id 249
    label "tematyka"
  ]
  node [
    id 250
    label "istota"
  ]
  node [
    id 251
    label "kultura"
  ]
  node [
    id 252
    label "zboczy&#263;"
  ]
  node [
    id 253
    label "discipline"
  ]
  node [
    id 254
    label "om&#243;wienie"
  ]
  node [
    id 255
    label "dawny"
  ]
  node [
    id 256
    label "du&#380;y"
  ]
  node [
    id 257
    label "s&#322;aby"
  ]
  node [
    id 258
    label "oddalony"
  ]
  node [
    id 259
    label "daleko"
  ]
  node [
    id 260
    label "przysz&#322;y"
  ]
  node [
    id 261
    label "ogl&#281;dny"
  ]
  node [
    id 262
    label "r&#243;&#380;ny"
  ]
  node [
    id 263
    label "g&#322;&#281;boki"
  ]
  node [
    id 264
    label "odlegle"
  ]
  node [
    id 265
    label "nieobecny"
  ]
  node [
    id 266
    label "odleg&#322;y"
  ]
  node [
    id 267
    label "d&#322;ugi"
  ]
  node [
    id 268
    label "zwi&#261;zany"
  ]
  node [
    id 269
    label "obcy"
  ]
  node [
    id 270
    label "stosunek_pracy"
  ]
  node [
    id 271
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 272
    label "benedykty&#324;ski"
  ]
  node [
    id 273
    label "pracowanie"
  ]
  node [
    id 274
    label "zaw&#243;d"
  ]
  node [
    id 275
    label "kierownictwo"
  ]
  node [
    id 276
    label "zmiana"
  ]
  node [
    id 277
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 278
    label "wytw&#243;r"
  ]
  node [
    id 279
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 280
    label "tynkarski"
  ]
  node [
    id 281
    label "czynnik_produkcji"
  ]
  node [
    id 282
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 283
    label "zobowi&#261;zanie"
  ]
  node [
    id 284
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 285
    label "czynno&#347;&#263;"
  ]
  node [
    id 286
    label "tyrka"
  ]
  node [
    id 287
    label "pracowa&#263;"
  ]
  node [
    id 288
    label "siedziba"
  ]
  node [
    id 289
    label "poda&#380;_pracy"
  ]
  node [
    id 290
    label "miejsce"
  ]
  node [
    id 291
    label "zak&#322;ad"
  ]
  node [
    id 292
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 293
    label "najem"
  ]
  node [
    id 294
    label "matczysko"
  ]
  node [
    id 295
    label "macierz"
  ]
  node [
    id 296
    label "przodkini"
  ]
  node [
    id 297
    label "Matka_Boska"
  ]
  node [
    id 298
    label "macocha"
  ]
  node [
    id 299
    label "matka_zast&#281;pcza"
  ]
  node [
    id 300
    label "stara"
  ]
  node [
    id 301
    label "rodzice"
  ]
  node [
    id 302
    label "rodzic"
  ]
  node [
    id 303
    label "trudno&#347;&#263;"
  ]
  node [
    id 304
    label "ambaras"
  ]
  node [
    id 305
    label "pierepa&#322;ka"
  ]
  node [
    id 306
    label "obstruction"
  ]
  node [
    id 307
    label "jajko_Kolumba"
  ]
  node [
    id 308
    label "subiekcja"
  ]
  node [
    id 309
    label "profesor"
  ]
  node [
    id 310
    label "kszta&#322;ciciel"
  ]
  node [
    id 311
    label "jegomo&#347;&#263;"
  ]
  node [
    id 312
    label "pracodawca"
  ]
  node [
    id 313
    label "rz&#261;dzenie"
  ]
  node [
    id 314
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 315
    label "ch&#322;opina"
  ]
  node [
    id 316
    label "bratek"
  ]
  node [
    id 317
    label "opiekun"
  ]
  node [
    id 318
    label "preceptor"
  ]
  node [
    id 319
    label "Midas"
  ]
  node [
    id 320
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 321
    label "murza"
  ]
  node [
    id 322
    label "ojciec"
  ]
  node [
    id 323
    label "androlog"
  ]
  node [
    id 324
    label "pupil"
  ]
  node [
    id 325
    label "efendi"
  ]
  node [
    id 326
    label "nabab"
  ]
  node [
    id 327
    label "w&#322;odarz"
  ]
  node [
    id 328
    label "szkolnik"
  ]
  node [
    id 329
    label "pedagog"
  ]
  node [
    id 330
    label "popularyzator"
  ]
  node [
    id 331
    label "andropauza"
  ]
  node [
    id 332
    label "gra_w_karty"
  ]
  node [
    id 333
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 334
    label "Mieszko_I"
  ]
  node [
    id 335
    label "bogaty"
  ]
  node [
    id 336
    label "samiec"
  ]
  node [
    id 337
    label "przyw&#243;dca"
  ]
  node [
    id 338
    label "belfer"
  ]
  node [
    id 339
    label "zgodzi&#263;"
  ]
  node [
    id 340
    label "assent"
  ]
  node [
    id 341
    label "zgadzanie"
  ]
  node [
    id 342
    label "zatrudnia&#263;"
  ]
  node [
    id 343
    label "parafrazowa&#263;"
  ]
  node [
    id 344
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 345
    label "sparafrazowa&#263;"
  ]
  node [
    id 346
    label "s&#261;d"
  ]
  node [
    id 347
    label "trawestowanie"
  ]
  node [
    id 348
    label "sparafrazowanie"
  ]
  node [
    id 349
    label "strawestowa&#263;"
  ]
  node [
    id 350
    label "sformu&#322;owanie"
  ]
  node [
    id 351
    label "strawestowanie"
  ]
  node [
    id 352
    label "komunikat"
  ]
  node [
    id 353
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 354
    label "delimitacja"
  ]
  node [
    id 355
    label "trawestowa&#263;"
  ]
  node [
    id 356
    label "parafrazowanie"
  ]
  node [
    id 357
    label "stylizacja"
  ]
  node [
    id 358
    label "ozdobnik"
  ]
  node [
    id 359
    label "pos&#322;uchanie"
  ]
  node [
    id 360
    label "rezultat"
  ]
  node [
    id 361
    label "Filipiny"
  ]
  node [
    id 362
    label "Rwanda"
  ]
  node [
    id 363
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 364
    label "Monako"
  ]
  node [
    id 365
    label "Korea"
  ]
  node [
    id 366
    label "Ghana"
  ]
  node [
    id 367
    label "Czarnog&#243;ra"
  ]
  node [
    id 368
    label "Malawi"
  ]
  node [
    id 369
    label "Indonezja"
  ]
  node [
    id 370
    label "Bu&#322;garia"
  ]
  node [
    id 371
    label "Nauru"
  ]
  node [
    id 372
    label "Kenia"
  ]
  node [
    id 373
    label "Kambod&#380;a"
  ]
  node [
    id 374
    label "Mali"
  ]
  node [
    id 375
    label "Austria"
  ]
  node [
    id 376
    label "interior"
  ]
  node [
    id 377
    label "Armenia"
  ]
  node [
    id 378
    label "Fid&#380;i"
  ]
  node [
    id 379
    label "Tuwalu"
  ]
  node [
    id 380
    label "Etiopia"
  ]
  node [
    id 381
    label "Malta"
  ]
  node [
    id 382
    label "Malezja"
  ]
  node [
    id 383
    label "Grenada"
  ]
  node [
    id 384
    label "Tad&#380;ykistan"
  ]
  node [
    id 385
    label "Wehrlen"
  ]
  node [
    id 386
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 387
    label "Rumunia"
  ]
  node [
    id 388
    label "Maroko"
  ]
  node [
    id 389
    label "Bhutan"
  ]
  node [
    id 390
    label "S&#322;owacja"
  ]
  node [
    id 391
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 392
    label "Seszele"
  ]
  node [
    id 393
    label "Kuwejt"
  ]
  node [
    id 394
    label "Arabia_Saudyjska"
  ]
  node [
    id 395
    label "Ekwador"
  ]
  node [
    id 396
    label "Kanada"
  ]
  node [
    id 397
    label "Japonia"
  ]
  node [
    id 398
    label "ziemia"
  ]
  node [
    id 399
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 400
    label "Hiszpania"
  ]
  node [
    id 401
    label "Wyspy_Marshalla"
  ]
  node [
    id 402
    label "Botswana"
  ]
  node [
    id 403
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 404
    label "D&#380;ibuti"
  ]
  node [
    id 405
    label "grupa"
  ]
  node [
    id 406
    label "Wietnam"
  ]
  node [
    id 407
    label "Egipt"
  ]
  node [
    id 408
    label "Burkina_Faso"
  ]
  node [
    id 409
    label "Niemcy"
  ]
  node [
    id 410
    label "Khitai"
  ]
  node [
    id 411
    label "Macedonia"
  ]
  node [
    id 412
    label "Albania"
  ]
  node [
    id 413
    label "Madagaskar"
  ]
  node [
    id 414
    label "Bahrajn"
  ]
  node [
    id 415
    label "Jemen"
  ]
  node [
    id 416
    label "Lesoto"
  ]
  node [
    id 417
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 418
    label "Samoa"
  ]
  node [
    id 419
    label "Andora"
  ]
  node [
    id 420
    label "Chiny"
  ]
  node [
    id 421
    label "Cypr"
  ]
  node [
    id 422
    label "Wielka_Brytania"
  ]
  node [
    id 423
    label "Ukraina"
  ]
  node [
    id 424
    label "Paragwaj"
  ]
  node [
    id 425
    label "Trynidad_i_Tobago"
  ]
  node [
    id 426
    label "Libia"
  ]
  node [
    id 427
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 428
    label "Surinam"
  ]
  node [
    id 429
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 430
    label "Australia"
  ]
  node [
    id 431
    label "Nigeria"
  ]
  node [
    id 432
    label "Honduras"
  ]
  node [
    id 433
    label "Bangladesz"
  ]
  node [
    id 434
    label "Peru"
  ]
  node [
    id 435
    label "Kazachstan"
  ]
  node [
    id 436
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 437
    label "Irak"
  ]
  node [
    id 438
    label "holoarktyka"
  ]
  node [
    id 439
    label "USA"
  ]
  node [
    id 440
    label "Sudan"
  ]
  node [
    id 441
    label "Nepal"
  ]
  node [
    id 442
    label "San_Marino"
  ]
  node [
    id 443
    label "Burundi"
  ]
  node [
    id 444
    label "Dominikana"
  ]
  node [
    id 445
    label "Komory"
  ]
  node [
    id 446
    label "granica_pa&#324;stwa"
  ]
  node [
    id 447
    label "Gwatemala"
  ]
  node [
    id 448
    label "Antarktis"
  ]
  node [
    id 449
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 450
    label "Brunei"
  ]
  node [
    id 451
    label "Iran"
  ]
  node [
    id 452
    label "Zimbabwe"
  ]
  node [
    id 453
    label "Namibia"
  ]
  node [
    id 454
    label "Meksyk"
  ]
  node [
    id 455
    label "Kamerun"
  ]
  node [
    id 456
    label "Somalia"
  ]
  node [
    id 457
    label "Angola"
  ]
  node [
    id 458
    label "Gabon"
  ]
  node [
    id 459
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 460
    label "Mozambik"
  ]
  node [
    id 461
    label "Tajwan"
  ]
  node [
    id 462
    label "Tunezja"
  ]
  node [
    id 463
    label "Nowa_Zelandia"
  ]
  node [
    id 464
    label "Liban"
  ]
  node [
    id 465
    label "Jordania"
  ]
  node [
    id 466
    label "Tonga"
  ]
  node [
    id 467
    label "Czad"
  ]
  node [
    id 468
    label "Liberia"
  ]
  node [
    id 469
    label "Gwinea"
  ]
  node [
    id 470
    label "Belize"
  ]
  node [
    id 471
    label "&#321;otwa"
  ]
  node [
    id 472
    label "Syria"
  ]
  node [
    id 473
    label "Benin"
  ]
  node [
    id 474
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 475
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 476
    label "Dominika"
  ]
  node [
    id 477
    label "Antigua_i_Barbuda"
  ]
  node [
    id 478
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 479
    label "Hanower"
  ]
  node [
    id 480
    label "partia"
  ]
  node [
    id 481
    label "Afganistan"
  ]
  node [
    id 482
    label "Kiribati"
  ]
  node [
    id 483
    label "W&#322;ochy"
  ]
  node [
    id 484
    label "Szwajcaria"
  ]
  node [
    id 485
    label "Sahara_Zachodnia"
  ]
  node [
    id 486
    label "Chorwacja"
  ]
  node [
    id 487
    label "Tajlandia"
  ]
  node [
    id 488
    label "Salwador"
  ]
  node [
    id 489
    label "Bahamy"
  ]
  node [
    id 490
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 491
    label "S&#322;owenia"
  ]
  node [
    id 492
    label "Gambia"
  ]
  node [
    id 493
    label "Urugwaj"
  ]
  node [
    id 494
    label "Zair"
  ]
  node [
    id 495
    label "Erytrea"
  ]
  node [
    id 496
    label "Rosja"
  ]
  node [
    id 497
    label "Uganda"
  ]
  node [
    id 498
    label "Niger"
  ]
  node [
    id 499
    label "Mauritius"
  ]
  node [
    id 500
    label "Turkmenistan"
  ]
  node [
    id 501
    label "Turcja"
  ]
  node [
    id 502
    label "Irlandia"
  ]
  node [
    id 503
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 504
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 505
    label "Gwinea_Bissau"
  ]
  node [
    id 506
    label "Belgia"
  ]
  node [
    id 507
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 508
    label "Palau"
  ]
  node [
    id 509
    label "Barbados"
  ]
  node [
    id 510
    label "Chile"
  ]
  node [
    id 511
    label "Wenezuela"
  ]
  node [
    id 512
    label "W&#281;gry"
  ]
  node [
    id 513
    label "Argentyna"
  ]
  node [
    id 514
    label "Kolumbia"
  ]
  node [
    id 515
    label "Sierra_Leone"
  ]
  node [
    id 516
    label "Azerbejd&#380;an"
  ]
  node [
    id 517
    label "Kongo"
  ]
  node [
    id 518
    label "Pakistan"
  ]
  node [
    id 519
    label "Liechtenstein"
  ]
  node [
    id 520
    label "Nikaragua"
  ]
  node [
    id 521
    label "Senegal"
  ]
  node [
    id 522
    label "Indie"
  ]
  node [
    id 523
    label "Suazi"
  ]
  node [
    id 524
    label "Polska"
  ]
  node [
    id 525
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 526
    label "Algieria"
  ]
  node [
    id 527
    label "terytorium"
  ]
  node [
    id 528
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 529
    label "Jamajka"
  ]
  node [
    id 530
    label "Kostaryka"
  ]
  node [
    id 531
    label "Timor_Wschodni"
  ]
  node [
    id 532
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 533
    label "Kuba"
  ]
  node [
    id 534
    label "Mauretania"
  ]
  node [
    id 535
    label "Portoryko"
  ]
  node [
    id 536
    label "Brazylia"
  ]
  node [
    id 537
    label "Mo&#322;dawia"
  ]
  node [
    id 538
    label "organizacja"
  ]
  node [
    id 539
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 540
    label "Litwa"
  ]
  node [
    id 541
    label "Kirgistan"
  ]
  node [
    id 542
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 543
    label "Izrael"
  ]
  node [
    id 544
    label "Grecja"
  ]
  node [
    id 545
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 546
    label "Holandia"
  ]
  node [
    id 547
    label "Sri_Lanka"
  ]
  node [
    id 548
    label "Katar"
  ]
  node [
    id 549
    label "Mikronezja"
  ]
  node [
    id 550
    label "Mongolia"
  ]
  node [
    id 551
    label "Laos"
  ]
  node [
    id 552
    label "Malediwy"
  ]
  node [
    id 553
    label "Zambia"
  ]
  node [
    id 554
    label "Tanzania"
  ]
  node [
    id 555
    label "Gujana"
  ]
  node [
    id 556
    label "Czechy"
  ]
  node [
    id 557
    label "Panama"
  ]
  node [
    id 558
    label "Uzbekistan"
  ]
  node [
    id 559
    label "Gruzja"
  ]
  node [
    id 560
    label "Serbia"
  ]
  node [
    id 561
    label "Francja"
  ]
  node [
    id 562
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 563
    label "Togo"
  ]
  node [
    id 564
    label "Estonia"
  ]
  node [
    id 565
    label "Oman"
  ]
  node [
    id 566
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 567
    label "Portugalia"
  ]
  node [
    id 568
    label "Boliwia"
  ]
  node [
    id 569
    label "Luksemburg"
  ]
  node [
    id 570
    label "Haiti"
  ]
  node [
    id 571
    label "Wyspy_Salomona"
  ]
  node [
    id 572
    label "Birma"
  ]
  node [
    id 573
    label "Rodezja"
  ]
  node [
    id 574
    label "remark"
  ]
  node [
    id 575
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 576
    label "u&#380;ywa&#263;"
  ]
  node [
    id 577
    label "okre&#347;la&#263;"
  ]
  node [
    id 578
    label "j&#281;zyk"
  ]
  node [
    id 579
    label "say"
  ]
  node [
    id 580
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 581
    label "formu&#322;owa&#263;"
  ]
  node [
    id 582
    label "talk"
  ]
  node [
    id 583
    label "powiada&#263;"
  ]
  node [
    id 584
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 585
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 586
    label "wydobywa&#263;"
  ]
  node [
    id 587
    label "chew_the_fat"
  ]
  node [
    id 588
    label "dysfonia"
  ]
  node [
    id 589
    label "umie&#263;"
  ]
  node [
    id 590
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 591
    label "tell"
  ]
  node [
    id 592
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 593
    label "wyra&#380;a&#263;"
  ]
  node [
    id 594
    label "gaworzy&#263;"
  ]
  node [
    id 595
    label "rozmawia&#263;"
  ]
  node [
    id 596
    label "prawi&#263;"
  ]
  node [
    id 597
    label "realnie"
  ]
  node [
    id 598
    label "faktyczny"
  ]
  node [
    id 599
    label "przerwa&#263;"
  ]
  node [
    id 600
    label "check"
  ]
  node [
    id 601
    label "zatrzyma&#263;"
  ]
  node [
    id 602
    label "stan&#261;&#263;"
  ]
  node [
    id 603
    label "suspend"
  ]
  node [
    id 604
    label "okre&#347;lony"
  ]
  node [
    id 605
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 606
    label "legislacyjnie"
  ]
  node [
    id 607
    label "kognicja"
  ]
  node [
    id 608
    label "przebieg"
  ]
  node [
    id 609
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 610
    label "wydarzenie"
  ]
  node [
    id 611
    label "przes&#322;anka"
  ]
  node [
    id 612
    label "rozprawa"
  ]
  node [
    id 613
    label "zjawisko"
  ]
  node [
    id 614
    label "nast&#281;pstwo"
  ]
  node [
    id 615
    label "spell"
  ]
  node [
    id 616
    label "odgrywa&#263;_rol&#281;"
  ]
  node [
    id 617
    label "zostawia&#263;"
  ]
  node [
    id 618
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 619
    label "represent"
  ]
  node [
    id 620
    label "count"
  ]
  node [
    id 621
    label "wyraz"
  ]
  node [
    id 622
    label "zdobi&#263;"
  ]
  node [
    id 623
    label "usun&#261;&#263;"
  ]
  node [
    id 624
    label "knock_out"
  ]
  node [
    id 625
    label "wykluczy&#263;"
  ]
  node [
    id 626
    label "kompletny"
  ]
  node [
    id 627
    label "wniwecz"
  ]
  node [
    id 628
    label "zupe&#322;ny"
  ]
  node [
    id 629
    label "piwo"
  ]
  node [
    id 630
    label "czyn"
  ]
  node [
    id 631
    label "ilustracja"
  ]
  node [
    id 632
    label "fakt"
  ]
  node [
    id 633
    label "charakterystyczny"
  ]
  node [
    id 634
    label "pa&#324;sko"
  ]
  node [
    id 635
    label "sprzeciwi&#263;_si&#281;"
  ]
  node [
    id 636
    label "odrzuci&#263;"
  ]
  node [
    id 637
    label "odpowiedzie&#263;"
  ]
  node [
    id 638
    label "oceni&#263;"
  ]
  node [
    id 639
    label "disown"
  ]
  node [
    id 640
    label "refuse"
  ]
  node [
    id 641
    label "decline"
  ]
  node [
    id 642
    label "take"
  ]
  node [
    id 643
    label "ustala&#263;"
  ]
  node [
    id 644
    label "sound"
  ]
  node [
    id 645
    label "wydawa&#263;"
  ]
  node [
    id 646
    label "s&#322;ycha&#263;"
  ]
  node [
    id 647
    label "echo"
  ]
  node [
    id 648
    label "gro&#378;ny"
  ]
  node [
    id 649
    label "niebezpiecznie"
  ]
  node [
    id 650
    label "surowie"
  ]
  node [
    id 651
    label "sternly"
  ]
  node [
    id 652
    label "powa&#380;nie"
  ]
  node [
    id 653
    label "tam"
  ]
  node [
    id 654
    label "&#322;atwy"
  ]
  node [
    id 655
    label "prostowanie_si&#281;"
  ]
  node [
    id 656
    label "rozprostowanie_si&#281;"
  ]
  node [
    id 657
    label "rozprostowanie"
  ]
  node [
    id 658
    label "niez&#322;o&#380;ony"
  ]
  node [
    id 659
    label "prostoduszny"
  ]
  node [
    id 660
    label "naturalny"
  ]
  node [
    id 661
    label "naiwny"
  ]
  node [
    id 662
    label "cios"
  ]
  node [
    id 663
    label "prostowanie"
  ]
  node [
    id 664
    label "niepozorny"
  ]
  node [
    id 665
    label "zwyk&#322;y"
  ]
  node [
    id 666
    label "prosto"
  ]
  node [
    id 667
    label "po_prostu"
  ]
  node [
    id 668
    label "skromny"
  ]
  node [
    id 669
    label "wyprostowanie_si&#281;"
  ]
  node [
    id 670
    label "wpaja&#263;"
  ]
  node [
    id 671
    label "wprowadza&#263;"
  ]
  node [
    id 672
    label "inculcate"
  ]
  node [
    id 673
    label "uodpornia&#263;"
  ]
  node [
    id 674
    label "przyjmowa&#263;_si&#281;"
  ]
  node [
    id 675
    label "inoculate"
  ]
  node [
    id 676
    label "operowa&#263;"
  ]
  node [
    id 677
    label "organizm"
  ]
  node [
    id 678
    label "blastula"
  ]
  node [
    id 679
    label "embrioblast"
  ]
  node [
    id 680
    label "embryo"
  ]
  node [
    id 681
    label "nasiono"
  ]
  node [
    id 682
    label "tarczka"
  ]
  node [
    id 683
    label "merystem_zarodkowy"
  ]
  node [
    id 684
    label "listek_zarodkowy"
  ]
  node [
    id 685
    label "morula"
  ]
  node [
    id 686
    label "owodniowiec"
  ]
  node [
    id 687
    label "pocz&#261;tek"
  ]
  node [
    id 688
    label "gastrula"
  ]
  node [
    id 689
    label "kszta&#322;t"
  ]
  node [
    id 690
    label "blastocel"
  ]
  node [
    id 691
    label "przednercze"
  ]
  node [
    id 692
    label "blame"
  ]
  node [
    id 693
    label "load"
  ]
  node [
    id 694
    label "zaszkodzi&#263;"
  ]
  node [
    id 695
    label "charge"
  ]
  node [
    id 696
    label "oskar&#380;y&#263;"
  ]
  node [
    id 697
    label "zobowi&#261;za&#263;"
  ]
  node [
    id 698
    label "na&#322;o&#380;y&#263;"
  ]
  node [
    id 699
    label "defect"
  ]
  node [
    id 700
    label "faintness"
  ]
  node [
    id 701
    label "schorzenie"
  ]
  node [
    id 702
    label "cecha"
  ]
  node [
    id 703
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 704
    label "imperfection"
  ]
  node [
    id 705
    label "strona"
  ]
  node [
    id 706
    label "dziedziczny"
  ]
  node [
    id 707
    label "genetycznie"
  ]
  node [
    id 708
    label "czu&#263;"
  ]
  node [
    id 709
    label "desire"
  ]
  node [
    id 710
    label "kcie&#263;"
  ]
  node [
    id 711
    label "przyj&#261;&#263;_si&#281;"
  ]
  node [
    id 712
    label "zoperowa&#263;"
  ]
  node [
    id 713
    label "wpoi&#263;"
  ]
  node [
    id 714
    label "uodporni&#263;"
  ]
  node [
    id 715
    label "wprowadzi&#263;"
  ]
  node [
    id 716
    label "kolejny"
  ]
  node [
    id 717
    label "partnerka"
  ]
  node [
    id 718
    label "&#380;ona"
  ]
  node [
    id 719
    label "obserwacja"
  ]
  node [
    id 720
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 721
    label "nauka_prawa"
  ]
  node [
    id 722
    label "dominion"
  ]
  node [
    id 723
    label "normatywizm"
  ]
  node [
    id 724
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 725
    label "qualification"
  ]
  node [
    id 726
    label "opis"
  ]
  node [
    id 727
    label "regu&#322;a_Allena"
  ]
  node [
    id 728
    label "normalizacja"
  ]
  node [
    id 729
    label "kazuistyka"
  ]
  node [
    id 730
    label "regu&#322;a_Glogera"
  ]
  node [
    id 731
    label "kultura_duchowa"
  ]
  node [
    id 732
    label "prawo_karne"
  ]
  node [
    id 733
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 734
    label "standard"
  ]
  node [
    id 735
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 736
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 737
    label "struktura"
  ]
  node [
    id 738
    label "szko&#322;a"
  ]
  node [
    id 739
    label "prawo_karne_procesowe"
  ]
  node [
    id 740
    label "prawo_Mendla"
  ]
  node [
    id 741
    label "przepis"
  ]
  node [
    id 742
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 743
    label "criterion"
  ]
  node [
    id 744
    label "kanonistyka"
  ]
  node [
    id 745
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 746
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 747
    label "wykonawczy"
  ]
  node [
    id 748
    label "twierdzenie"
  ]
  node [
    id 749
    label "judykatura"
  ]
  node [
    id 750
    label "umocowa&#263;"
  ]
  node [
    id 751
    label "podmiot"
  ]
  node [
    id 752
    label "procesualistyka"
  ]
  node [
    id 753
    label "kierunek"
  ]
  node [
    id 754
    label "kryminologia"
  ]
  node [
    id 755
    label "kryminalistyka"
  ]
  node [
    id 756
    label "cywilistyka"
  ]
  node [
    id 757
    label "law"
  ]
  node [
    id 758
    label "zasada_d'Alemberta"
  ]
  node [
    id 759
    label "jurisprudence"
  ]
  node [
    id 760
    label "zasada"
  ]
  node [
    id 761
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 762
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 763
    label "zabieg"
  ]
  node [
    id 764
    label "termination"
  ]
  node [
    id 765
    label "zygotarianin"
  ]
  node [
    id 766
    label "abrazja"
  ]
  node [
    id 767
    label "jednakowo"
  ]
  node [
    id 768
    label "spokojnie"
  ]
  node [
    id 769
    label "zgodny"
  ]
  node [
    id 770
    label "dobrze"
  ]
  node [
    id 771
    label "zbie&#380;nie"
  ]
  node [
    id 772
    label "twardy"
  ]
  node [
    id 773
    label "restryktywnie"
  ]
  node [
    id 774
    label "tenis"
  ]
  node [
    id 775
    label "cover"
  ]
  node [
    id 776
    label "siatk&#243;wka"
  ]
  node [
    id 777
    label "dawa&#263;"
  ]
  node [
    id 778
    label "faszerowa&#263;"
  ]
  node [
    id 779
    label "introduce"
  ]
  node [
    id 780
    label "jedzenie"
  ]
  node [
    id 781
    label "tender"
  ]
  node [
    id 782
    label "deal"
  ]
  node [
    id 783
    label "kelner"
  ]
  node [
    id 784
    label "serwowa&#263;"
  ]
  node [
    id 785
    label "rozgrywa&#263;"
  ]
  node [
    id 786
    label "stawia&#263;"
  ]
  node [
    id 787
    label "zezwala&#263;"
  ]
  node [
    id 788
    label "ask"
  ]
  node [
    id 789
    label "invite"
  ]
  node [
    id 790
    label "preach"
  ]
  node [
    id 791
    label "pies"
  ]
  node [
    id 792
    label "ujmowa&#263;_si&#281;"
  ]
  node [
    id 793
    label "poleca&#263;"
  ]
  node [
    id 794
    label "zaprasza&#263;"
  ]
  node [
    id 795
    label "suffice"
  ]
  node [
    id 796
    label "dziwi&#263;_si&#281;"
  ]
  node [
    id 797
    label "pomocowy"
  ]
  node [
    id 798
    label "ludzki"
  ]
  node [
    id 799
    label "humanitarnie"
  ]
  node [
    id 800
    label "dupny"
  ]
  node [
    id 801
    label "wysoce"
  ]
  node [
    id 802
    label "wyj&#261;tkowy"
  ]
  node [
    id 803
    label "wybitny"
  ]
  node [
    id 804
    label "znaczny"
  ]
  node [
    id 805
    label "prawdziwy"
  ]
  node [
    id 806
    label "wa&#380;ny"
  ]
  node [
    id 807
    label "nieprzeci&#281;tny"
  ]
  node [
    id 808
    label "dobro&#263;"
  ]
  node [
    id 809
    label "energia"
  ]
  node [
    id 810
    label "doznanie"
  ]
  node [
    id 811
    label "kr&#243;tkotrwa&#322;o&#347;&#263;"
  ]
  node [
    id 812
    label "instability"
  ]
  node [
    id 813
    label "infirmity"
  ]
  node [
    id 814
    label "pogorszenie"
  ]
  node [
    id 815
    label "dokument"
  ]
  node [
    id 816
    label "device"
  ]
  node [
    id 817
    label "program_u&#380;ytkowy"
  ]
  node [
    id 818
    label "intencja"
  ]
  node [
    id 819
    label "agreement"
  ]
  node [
    id 820
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 821
    label "plan"
  ]
  node [
    id 822
    label "dokumentacja"
  ]
  node [
    id 823
    label "zwinnie"
  ]
  node [
    id 824
    label "bezpiecznie"
  ]
  node [
    id 825
    label "wiarygodnie"
  ]
  node [
    id 826
    label "pewniej"
  ]
  node [
    id 827
    label "pewny"
  ]
  node [
    id 828
    label "mocno"
  ]
  node [
    id 829
    label "najpewniej"
  ]
  node [
    id 830
    label "przyst&#281;powa&#263;"
  ]
  node [
    id 831
    label "zalicza&#263;"
  ]
  node [
    id 832
    label "opowiada&#263;"
  ]
  node [
    id 833
    label "render"
  ]
  node [
    id 834
    label "impart"
  ]
  node [
    id 835
    label "oddawa&#263;"
  ]
  node [
    id 836
    label "sk&#322;ada&#263;"
  ]
  node [
    id 837
    label "powierza&#263;"
  ]
  node [
    id 838
    label "bequeath"
  ]
  node [
    id 839
    label "temat"
  ]
  node [
    id 840
    label "idea"
  ]
  node [
    id 841
    label "szczeg&#243;&#322;"
  ]
  node [
    id 842
    label "object"
  ]
  node [
    id 843
    label "proposition"
  ]
  node [
    id 844
    label "pomy&#347;lny"
  ]
  node [
    id 845
    label "skuteczny"
  ]
  node [
    id 846
    label "moralny"
  ]
  node [
    id 847
    label "korzystny"
  ]
  node [
    id 848
    label "odpowiedni"
  ]
  node [
    id 849
    label "pozytywny"
  ]
  node [
    id 850
    label "grzeczny"
  ]
  node [
    id 851
    label "powitanie"
  ]
  node [
    id 852
    label "mi&#322;y"
  ]
  node [
    id 853
    label "dobroczynny"
  ]
  node [
    id 854
    label "pos&#322;uszny"
  ]
  node [
    id 855
    label "ca&#322;y"
  ]
  node [
    id 856
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 857
    label "czw&#243;rka"
  ]
  node [
    id 858
    label "spokojny"
  ]
  node [
    id 859
    label "&#347;mieszny"
  ]
  node [
    id 860
    label "drogi"
  ]
  node [
    id 861
    label "prosta"
  ]
  node [
    id 862
    label "po&#322;o&#380;enie"
  ]
  node [
    id 863
    label "chwila"
  ]
  node [
    id 864
    label "ust&#281;p"
  ]
  node [
    id 865
    label "kres"
  ]
  node [
    id 866
    label "mark"
  ]
  node [
    id 867
    label "pozycja"
  ]
  node [
    id 868
    label "point"
  ]
  node [
    id 869
    label "stopie&#324;_pisma"
  ]
  node [
    id 870
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 871
    label "przestrze&#324;"
  ]
  node [
    id 872
    label "wojsko"
  ]
  node [
    id 873
    label "zapunktowa&#263;"
  ]
  node [
    id 874
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 875
    label "obiekt_matematyczny"
  ]
  node [
    id 876
    label "plamka"
  ]
  node [
    id 877
    label "obiekt"
  ]
  node [
    id 878
    label "podpunkt"
  ]
  node [
    id 879
    label "jednostka"
  ]
  node [
    id 880
    label "zwi&#281;kszanie_si&#281;"
  ]
  node [
    id 881
    label "visit"
  ]
  node [
    id 882
    label "przywidzenie"
  ]
  node [
    id 883
    label "obejrzenie"
  ]
  node [
    id 884
    label "punkt_widzenia"
  ]
  node [
    id 885
    label "&#347;nienie"
  ]
  node [
    id 886
    label "w&#322;&#261;czanie"
  ]
  node [
    id 887
    label "uznawanie"
  ]
  node [
    id 888
    label "wyobra&#380;anie_sobie"
  ]
  node [
    id 889
    label "przegl&#261;danie"
  ]
  node [
    id 890
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 891
    label "pojmowanie"
  ]
  node [
    id 892
    label "reagowanie"
  ]
  node [
    id 893
    label "przejrzenie"
  ]
  node [
    id 894
    label "zmalenie"
  ]
  node [
    id 895
    label "vision"
  ]
  node [
    id 896
    label "wychodzenie"
  ]
  node [
    id 897
    label "odwiedziny"
  ]
  node [
    id 898
    label "widywanie"
  ]
  node [
    id 899
    label "u&#322;uda"
  ]
  node [
    id 900
    label "ocenianie"
  ]
  node [
    id 901
    label "wzrok"
  ]
  node [
    id 902
    label "view"
  ]
  node [
    id 903
    label "patrzenie"
  ]
  node [
    id 904
    label "dostrzeganie"
  ]
  node [
    id 905
    label "sojourn"
  ]
  node [
    id 906
    label "ogl&#261;danie"
  ]
  node [
    id 907
    label "zobaczenie"
  ]
  node [
    id 908
    label "malenie"
  ]
  node [
    id 909
    label "aprobowanie"
  ]
  node [
    id 910
    label "postrzeganie"
  ]
  node [
    id 911
    label "realization"
  ]
  node [
    id 912
    label "catch"
  ]
  node [
    id 913
    label "pozosta&#263;"
  ]
  node [
    id 914
    label "osta&#263;_si&#281;"
  ]
  node [
    id 915
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 916
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 917
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 918
    label "change"
  ]
  node [
    id 919
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 920
    label "skomplikowanie"
  ]
  node [
    id 921
    label "trudny"
  ]
  node [
    id 922
    label "jaki&#347;"
  ]
  node [
    id 923
    label "my&#347;l"
  ]
  node [
    id 924
    label "wnioskowanie"
  ]
  node [
    id 925
    label "motion"
  ]
  node [
    id 926
    label "pismo"
  ]
  node [
    id 927
    label "prayer"
  ]
  node [
    id 928
    label "kieliszek"
  ]
  node [
    id 929
    label "shot"
  ]
  node [
    id 930
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 931
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 932
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 933
    label "jednolicie"
  ]
  node [
    id 934
    label "w&#243;dka"
  ]
  node [
    id 935
    label "ujednolicenie"
  ]
  node [
    id 936
    label "jednakowy"
  ]
  node [
    id 937
    label "przyczyna"
  ]
  node [
    id 938
    label "matuszka"
  ]
  node [
    id 939
    label "geneza"
  ]
  node [
    id 940
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 941
    label "czynnik"
  ]
  node [
    id 942
    label "poci&#261;ganie"
  ]
  node [
    id 943
    label "uprz&#261;&#380;"
  ]
  node [
    id 944
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 945
    label "uzasadni&#263;"
  ]
  node [
    id 946
    label "testify"
  ]
  node [
    id 947
    label "stwierdzi&#263;"
  ]
  node [
    id 948
    label "realize"
  ]
  node [
    id 949
    label "courage"
  ]
  node [
    id 950
    label "dusza"
  ]
  node [
    id 951
    label "cywilnie"
  ]
  node [
    id 952
    label "nieoficjalny"
  ]
  node [
    id 953
    label "postawi&#263;"
  ]
  node [
    id 954
    label "awansowanie"
  ]
  node [
    id 955
    label "awansowa&#263;"
  ]
  node [
    id 956
    label "uprawianie"
  ]
  node [
    id 957
    label "powierzanie"
  ]
  node [
    id 958
    label "pogl&#261;d"
  ]
  node [
    id 959
    label "wakowa&#263;"
  ]
  node [
    id 960
    label "Berkeley"
  ]
  node [
    id 961
    label "prekonizacja"
  ]
  node [
    id 962
    label "sakra"
  ]
  node [
    id 963
    label "pontyfikat"
  ]
  node [
    id 964
    label "&#347;w"
  ]
  node [
    id 965
    label "konsekrowanie"
  ]
  node [
    id 966
    label "episkopat"
  ]
  node [
    id 967
    label "dostojnik_ko&#347;cielny"
  ]
  node [
    id 968
    label "pontyfikalia"
  ]
  node [
    id 969
    label "postrach"
  ]
  node [
    id 970
    label "zawisa&#263;"
  ]
  node [
    id 971
    label "zawisanie"
  ]
  node [
    id 972
    label "threat"
  ]
  node [
    id 973
    label "pogroza"
  ]
  node [
    id 974
    label "czarny_punkt"
  ]
  node [
    id 975
    label "zagrozi&#263;"
  ]
  node [
    id 976
    label "chrze&#347;cija&#324;stwo"
  ]
  node [
    id 977
    label "cenzura"
  ]
  node [
    id 978
    label "kl&#261;twa"
  ]
  node [
    id 979
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 980
    label "odst&#261;pi&#263;"
  ]
  node [
    id 981
    label "zacz&#261;&#263;"
  ]
  node [
    id 982
    label "zrezygnowa&#263;"
  ]
  node [
    id 983
    label "wyj&#347;&#263;"
  ]
  node [
    id 984
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 985
    label "happen"
  ]
  node [
    id 986
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 987
    label "perform"
  ]
  node [
    id 988
    label "nak&#322;oni&#263;"
  ]
  node [
    id 989
    label "appear"
  ]
  node [
    id 990
    label "uzasadnia&#263;"
  ]
  node [
    id 991
    label "pomaga&#263;"
  ]
  node [
    id 992
    label "unbosom"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 3
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 17
  ]
  edge [
    source 9
    target 18
  ]
  edge [
    source 9
    target 30
  ]
  edge [
    source 9
    target 39
  ]
  edge [
    source 9
    target 40
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 138
  ]
  edge [
    source 9
    target 139
  ]
  edge [
    source 9
    target 140
  ]
  edge [
    source 9
    target 141
  ]
  edge [
    source 9
    target 142
  ]
  edge [
    source 9
    target 143
  ]
  edge [
    source 9
    target 144
  ]
  edge [
    source 9
    target 38
  ]
  edge [
    source 9
    target 27
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 9
    target 100
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 21
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 145
  ]
  edge [
    source 11
    target 146
  ]
  edge [
    source 11
    target 147
  ]
  edge [
    source 11
    target 148
  ]
  edge [
    source 11
    target 149
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 22
  ]
  edge [
    source 12
    target 36
  ]
  edge [
    source 12
    target 49
  ]
  edge [
    source 12
    target 84
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 38
  ]
  edge [
    source 13
    target 67
  ]
  edge [
    source 13
    target 150
  ]
  edge [
    source 13
    target 151
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 49
  ]
  edge [
    source 14
    target 50
  ]
  edge [
    source 14
    target 152
  ]
  edge [
    source 14
    target 153
  ]
  edge [
    source 14
    target 92
  ]
  edge [
    source 14
    target 52
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 56
  ]
  edge [
    source 17
    target 57
  ]
  edge [
    source 17
    target 154
  ]
  edge [
    source 17
    target 155
  ]
  edge [
    source 17
    target 156
  ]
  edge [
    source 17
    target 157
  ]
  edge [
    source 17
    target 25
  ]
  edge [
    source 17
    target 158
  ]
  edge [
    source 17
    target 159
  ]
  edge [
    source 17
    target 160
  ]
  edge [
    source 17
    target 161
  ]
  edge [
    source 17
    target 162
  ]
  edge [
    source 17
    target 163
  ]
  edge [
    source 17
    target 164
  ]
  edge [
    source 17
    target 165
  ]
  edge [
    source 17
    target 166
  ]
  edge [
    source 17
    target 167
  ]
  edge [
    source 17
    target 168
  ]
  edge [
    source 17
    target 169
  ]
  edge [
    source 17
    target 170
  ]
  edge [
    source 17
    target 171
  ]
  edge [
    source 17
    target 172
  ]
  edge [
    source 17
    target 173
  ]
  edge [
    source 17
    target 174
  ]
  edge [
    source 17
    target 175
  ]
  edge [
    source 17
    target 176
  ]
  edge [
    source 17
    target 177
  ]
  edge [
    source 17
    target 178
  ]
  edge [
    source 17
    target 179
  ]
  edge [
    source 17
    target 180
  ]
  edge [
    source 17
    target 181
  ]
  edge [
    source 17
    target 182
  ]
  edge [
    source 17
    target 183
  ]
  edge [
    source 17
    target 21
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 184
  ]
  edge [
    source 18
    target 185
  ]
  edge [
    source 18
    target 186
  ]
  edge [
    source 18
    target 187
  ]
  edge [
    source 18
    target 188
  ]
  edge [
    source 18
    target 189
  ]
  edge [
    source 18
    target 190
  ]
  edge [
    source 18
    target 92
  ]
  edge [
    source 18
    target 21
  ]
  edge [
    source 18
    target 76
  ]
  edge [
    source 18
    target 109
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 31
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 191
  ]
  edge [
    source 20
    target 192
  ]
  edge [
    source 20
    target 193
  ]
  edge [
    source 20
    target 194
  ]
  edge [
    source 20
    target 195
  ]
  edge [
    source 20
    target 196
  ]
  edge [
    source 20
    target 197
  ]
  edge [
    source 20
    target 198
  ]
  edge [
    source 20
    target 199
  ]
  edge [
    source 20
    target 200
  ]
  edge [
    source 20
    target 201
  ]
  edge [
    source 20
    target 202
  ]
  edge [
    source 20
    target 203
  ]
  edge [
    source 20
    target 66
  ]
  edge [
    source 20
    target 204
  ]
  edge [
    source 20
    target 205
  ]
  edge [
    source 20
    target 206
  ]
  edge [
    source 20
    target 207
  ]
  edge [
    source 20
    target 37
  ]
  edge [
    source 20
    target 208
  ]
  edge [
    source 20
    target 209
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 31
  ]
  edge [
    source 21
    target 32
  ]
  edge [
    source 21
    target 95
  ]
  edge [
    source 21
    target 210
  ]
  edge [
    source 21
    target 211
  ]
  edge [
    source 21
    target 212
  ]
  edge [
    source 21
    target 213
  ]
  edge [
    source 21
    target 214
  ]
  edge [
    source 21
    target 215
  ]
  edge [
    source 21
    target 216
  ]
  edge [
    source 21
    target 217
  ]
  edge [
    source 21
    target 218
  ]
  edge [
    source 21
    target 219
  ]
  edge [
    source 21
    target 220
  ]
  edge [
    source 21
    target 221
  ]
  edge [
    source 21
    target 38
  ]
  edge [
    source 21
    target 92
  ]
  edge [
    source 21
    target 76
  ]
  edge [
    source 21
    target 109
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 36
  ]
  edge [
    source 22
    target 49
  ]
  edge [
    source 22
    target 84
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 37
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 222
  ]
  edge [
    source 24
    target 38
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 38
  ]
  edge [
    source 25
    target 60
  ]
  edge [
    source 25
    target 37
  ]
  edge [
    source 25
    target 74
  ]
  edge [
    source 25
    target 75
  ]
  edge [
    source 25
    target 76
  ]
  edge [
    source 25
    target 77
  ]
  edge [
    source 25
    target 78
  ]
  edge [
    source 25
    target 223
  ]
  edge [
    source 25
    target 224
  ]
  edge [
    source 25
    target 225
  ]
  edge [
    source 25
    target 226
  ]
  edge [
    source 25
    target 227
  ]
  edge [
    source 25
    target 228
  ]
  edge [
    source 25
    target 166
  ]
  edge [
    source 25
    target 229
  ]
  edge [
    source 25
    target 230
  ]
  edge [
    source 25
    target 231
  ]
  edge [
    source 25
    target 44
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 232
  ]
  edge [
    source 26
    target 233
  ]
  edge [
    source 26
    target 234
  ]
  edge [
    source 26
    target 235
  ]
  edge [
    source 26
    target 236
  ]
  edge [
    source 26
    target 126
  ]
  edge [
    source 26
    target 237
  ]
  edge [
    source 26
    target 238
  ]
  edge [
    source 26
    target 239
  ]
  edge [
    source 26
    target 240
  ]
  edge [
    source 26
    target 241
  ]
  edge [
    source 26
    target 242
  ]
  edge [
    source 26
    target 243
  ]
  edge [
    source 26
    target 244
  ]
  edge [
    source 26
    target 245
  ]
  edge [
    source 26
    target 246
  ]
  edge [
    source 26
    target 247
  ]
  edge [
    source 26
    target 248
  ]
  edge [
    source 26
    target 249
  ]
  edge [
    source 26
    target 250
  ]
  edge [
    source 26
    target 176
  ]
  edge [
    source 26
    target 251
  ]
  edge [
    source 26
    target 252
  ]
  edge [
    source 26
    target 253
  ]
  edge [
    source 26
    target 254
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 255
  ]
  edge [
    source 27
    target 256
  ]
  edge [
    source 27
    target 257
  ]
  edge [
    source 27
    target 258
  ]
  edge [
    source 27
    target 259
  ]
  edge [
    source 27
    target 260
  ]
  edge [
    source 27
    target 261
  ]
  edge [
    source 27
    target 262
  ]
  edge [
    source 27
    target 263
  ]
  edge [
    source 27
    target 264
  ]
  edge [
    source 27
    target 265
  ]
  edge [
    source 27
    target 266
  ]
  edge [
    source 27
    target 267
  ]
  edge [
    source 27
    target 268
  ]
  edge [
    source 27
    target 269
  ]
  edge [
    source 27
    target 39
  ]
  edge [
    source 27
    target 95
  ]
  edge [
    source 27
    target 99
  ]
  edge [
    source 27
    target 100
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 270
  ]
  edge [
    source 28
    target 271
  ]
  edge [
    source 28
    target 272
  ]
  edge [
    source 28
    target 273
  ]
  edge [
    source 28
    target 274
  ]
  edge [
    source 28
    target 275
  ]
  edge [
    source 28
    target 276
  ]
  edge [
    source 28
    target 277
  ]
  edge [
    source 28
    target 278
  ]
  edge [
    source 28
    target 279
  ]
  edge [
    source 28
    target 280
  ]
  edge [
    source 28
    target 281
  ]
  edge [
    source 28
    target 282
  ]
  edge [
    source 28
    target 283
  ]
  edge [
    source 28
    target 284
  ]
  edge [
    source 28
    target 285
  ]
  edge [
    source 28
    target 286
  ]
  edge [
    source 28
    target 287
  ]
  edge [
    source 28
    target 288
  ]
  edge [
    source 28
    target 289
  ]
  edge [
    source 28
    target 290
  ]
  edge [
    source 28
    target 291
  ]
  edge [
    source 28
    target 292
  ]
  edge [
    source 28
    target 293
  ]
  edge [
    source 28
    target 100
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 294
  ]
  edge [
    source 29
    target 295
  ]
  edge [
    source 29
    target 296
  ]
  edge [
    source 29
    target 297
  ]
  edge [
    source 29
    target 298
  ]
  edge [
    source 29
    target 299
  ]
  edge [
    source 29
    target 300
  ]
  edge [
    source 29
    target 301
  ]
  edge [
    source 29
    target 302
  ]
  edge [
    source 30
    target 303
  ]
  edge [
    source 30
    target 84
  ]
  edge [
    source 30
    target 304
  ]
  edge [
    source 30
    target 133
  ]
  edge [
    source 30
    target 305
  ]
  edge [
    source 30
    target 306
  ]
  edge [
    source 30
    target 135
  ]
  edge [
    source 30
    target 307
  ]
  edge [
    source 30
    target 308
  ]
  edge [
    source 30
    target 136
  ]
  edge [
    source 31
    target 80
  ]
  edge [
    source 31
    target 81
  ]
  edge [
    source 31
    target 94
  ]
  edge [
    source 31
    target 98
  ]
  edge [
    source 31
    target 191
  ]
  edge [
    source 31
    target 309
  ]
  edge [
    source 31
    target 310
  ]
  edge [
    source 31
    target 311
  ]
  edge [
    source 31
    target 193
  ]
  edge [
    source 31
    target 312
  ]
  edge [
    source 31
    target 313
  ]
  edge [
    source 31
    target 151
  ]
  edge [
    source 31
    target 314
  ]
  edge [
    source 31
    target 315
  ]
  edge [
    source 31
    target 316
  ]
  edge [
    source 31
    target 317
  ]
  edge [
    source 31
    target 197
  ]
  edge [
    source 31
    target 318
  ]
  edge [
    source 31
    target 319
  ]
  edge [
    source 31
    target 320
  ]
  edge [
    source 31
    target 199
  ]
  edge [
    source 31
    target 321
  ]
  edge [
    source 31
    target 322
  ]
  edge [
    source 31
    target 323
  ]
  edge [
    source 31
    target 324
  ]
  edge [
    source 31
    target 325
  ]
  edge [
    source 31
    target 326
  ]
  edge [
    source 31
    target 327
  ]
  edge [
    source 31
    target 328
  ]
  edge [
    source 31
    target 329
  ]
  edge [
    source 31
    target 330
  ]
  edge [
    source 31
    target 331
  ]
  edge [
    source 31
    target 332
  ]
  edge [
    source 31
    target 333
  ]
  edge [
    source 31
    target 334
  ]
  edge [
    source 31
    target 335
  ]
  edge [
    source 31
    target 336
  ]
  edge [
    source 31
    target 337
  ]
  edge [
    source 31
    target 37
  ]
  edge [
    source 31
    target 338
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 339
  ]
  edge [
    source 33
    target 340
  ]
  edge [
    source 33
    target 341
  ]
  edge [
    source 33
    target 342
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 72
  ]
  edge [
    source 34
    target 68
  ]
  edge [
    source 34
    target 92
  ]
  edge [
    source 34
    target 93
  ]
  edge [
    source 34
    target 96
  ]
  edge [
    source 34
    target 76
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 343
  ]
  edge [
    source 36
    target 344
  ]
  edge [
    source 36
    target 345
  ]
  edge [
    source 36
    target 346
  ]
  edge [
    source 36
    target 347
  ]
  edge [
    source 36
    target 348
  ]
  edge [
    source 36
    target 349
  ]
  edge [
    source 36
    target 350
  ]
  edge [
    source 36
    target 351
  ]
  edge [
    source 36
    target 352
  ]
  edge [
    source 36
    target 353
  ]
  edge [
    source 36
    target 354
  ]
  edge [
    source 36
    target 355
  ]
  edge [
    source 36
    target 356
  ]
  edge [
    source 36
    target 357
  ]
  edge [
    source 36
    target 358
  ]
  edge [
    source 36
    target 359
  ]
  edge [
    source 36
    target 360
  ]
  edge [
    source 36
    target 104
  ]
  edge [
    source 36
    target 49
  ]
  edge [
    source 36
    target 84
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 62
  ]
  edge [
    source 37
    target 63
  ]
  edge [
    source 37
    target 73
  ]
  edge [
    source 37
    target 361
  ]
  edge [
    source 37
    target 362
  ]
  edge [
    source 37
    target 363
  ]
  edge [
    source 37
    target 364
  ]
  edge [
    source 37
    target 365
  ]
  edge [
    source 37
    target 366
  ]
  edge [
    source 37
    target 367
  ]
  edge [
    source 37
    target 368
  ]
  edge [
    source 37
    target 369
  ]
  edge [
    source 37
    target 370
  ]
  edge [
    source 37
    target 371
  ]
  edge [
    source 37
    target 372
  ]
  edge [
    source 37
    target 373
  ]
  edge [
    source 37
    target 374
  ]
  edge [
    source 37
    target 375
  ]
  edge [
    source 37
    target 376
  ]
  edge [
    source 37
    target 377
  ]
  edge [
    source 37
    target 378
  ]
  edge [
    source 37
    target 379
  ]
  edge [
    source 37
    target 380
  ]
  edge [
    source 37
    target 381
  ]
  edge [
    source 37
    target 382
  ]
  edge [
    source 37
    target 383
  ]
  edge [
    source 37
    target 384
  ]
  edge [
    source 37
    target 385
  ]
  edge [
    source 37
    target 170
  ]
  edge [
    source 37
    target 386
  ]
  edge [
    source 37
    target 387
  ]
  edge [
    source 37
    target 388
  ]
  edge [
    source 37
    target 389
  ]
  edge [
    source 37
    target 390
  ]
  edge [
    source 37
    target 391
  ]
  edge [
    source 37
    target 392
  ]
  edge [
    source 37
    target 393
  ]
  edge [
    source 37
    target 394
  ]
  edge [
    source 37
    target 395
  ]
  edge [
    source 37
    target 396
  ]
  edge [
    source 37
    target 397
  ]
  edge [
    source 37
    target 398
  ]
  edge [
    source 37
    target 399
  ]
  edge [
    source 37
    target 400
  ]
  edge [
    source 37
    target 401
  ]
  edge [
    source 37
    target 402
  ]
  edge [
    source 37
    target 403
  ]
  edge [
    source 37
    target 404
  ]
  edge [
    source 37
    target 405
  ]
  edge [
    source 37
    target 406
  ]
  edge [
    source 37
    target 407
  ]
  edge [
    source 37
    target 408
  ]
  edge [
    source 37
    target 409
  ]
  edge [
    source 37
    target 410
  ]
  edge [
    source 37
    target 411
  ]
  edge [
    source 37
    target 412
  ]
  edge [
    source 37
    target 413
  ]
  edge [
    source 37
    target 414
  ]
  edge [
    source 37
    target 415
  ]
  edge [
    source 37
    target 416
  ]
  edge [
    source 37
    target 417
  ]
  edge [
    source 37
    target 418
  ]
  edge [
    source 37
    target 419
  ]
  edge [
    source 37
    target 420
  ]
  edge [
    source 37
    target 421
  ]
  edge [
    source 37
    target 422
  ]
  edge [
    source 37
    target 423
  ]
  edge [
    source 37
    target 424
  ]
  edge [
    source 37
    target 425
  ]
  edge [
    source 37
    target 426
  ]
  edge [
    source 37
    target 427
  ]
  edge [
    source 37
    target 428
  ]
  edge [
    source 37
    target 429
  ]
  edge [
    source 37
    target 430
  ]
  edge [
    source 37
    target 431
  ]
  edge [
    source 37
    target 432
  ]
  edge [
    source 37
    target 433
  ]
  edge [
    source 37
    target 434
  ]
  edge [
    source 37
    target 435
  ]
  edge [
    source 37
    target 436
  ]
  edge [
    source 37
    target 437
  ]
  edge [
    source 37
    target 438
  ]
  edge [
    source 37
    target 439
  ]
  edge [
    source 37
    target 440
  ]
  edge [
    source 37
    target 441
  ]
  edge [
    source 37
    target 442
  ]
  edge [
    source 37
    target 443
  ]
  edge [
    source 37
    target 444
  ]
  edge [
    source 37
    target 445
  ]
  edge [
    source 37
    target 446
  ]
  edge [
    source 37
    target 447
  ]
  edge [
    source 37
    target 448
  ]
  edge [
    source 37
    target 449
  ]
  edge [
    source 37
    target 450
  ]
  edge [
    source 37
    target 451
  ]
  edge [
    source 37
    target 452
  ]
  edge [
    source 37
    target 453
  ]
  edge [
    source 37
    target 454
  ]
  edge [
    source 37
    target 455
  ]
  edge [
    source 37
    target 193
  ]
  edge [
    source 37
    target 456
  ]
  edge [
    source 37
    target 457
  ]
  edge [
    source 37
    target 458
  ]
  edge [
    source 37
    target 459
  ]
  edge [
    source 37
    target 460
  ]
  edge [
    source 37
    target 461
  ]
  edge [
    source 37
    target 462
  ]
  edge [
    source 37
    target 463
  ]
  edge [
    source 37
    target 464
  ]
  edge [
    source 37
    target 465
  ]
  edge [
    source 37
    target 466
  ]
  edge [
    source 37
    target 467
  ]
  edge [
    source 37
    target 468
  ]
  edge [
    source 37
    target 469
  ]
  edge [
    source 37
    target 470
  ]
  edge [
    source 37
    target 471
  ]
  edge [
    source 37
    target 472
  ]
  edge [
    source 37
    target 473
  ]
  edge [
    source 37
    target 474
  ]
  edge [
    source 37
    target 475
  ]
  edge [
    source 37
    target 476
  ]
  edge [
    source 37
    target 477
  ]
  edge [
    source 37
    target 478
  ]
  edge [
    source 37
    target 479
  ]
  edge [
    source 37
    target 480
  ]
  edge [
    source 37
    target 481
  ]
  edge [
    source 37
    target 482
  ]
  edge [
    source 37
    target 483
  ]
  edge [
    source 37
    target 484
  ]
  edge [
    source 37
    target 485
  ]
  edge [
    source 37
    target 486
  ]
  edge [
    source 37
    target 487
  ]
  edge [
    source 37
    target 488
  ]
  edge [
    source 37
    target 489
  ]
  edge [
    source 37
    target 490
  ]
  edge [
    source 37
    target 491
  ]
  edge [
    source 37
    target 492
  ]
  edge [
    source 37
    target 493
  ]
  edge [
    source 37
    target 494
  ]
  edge [
    source 37
    target 495
  ]
  edge [
    source 37
    target 496
  ]
  edge [
    source 37
    target 497
  ]
  edge [
    source 37
    target 498
  ]
  edge [
    source 37
    target 499
  ]
  edge [
    source 37
    target 500
  ]
  edge [
    source 37
    target 501
  ]
  edge [
    source 37
    target 502
  ]
  edge [
    source 37
    target 503
  ]
  edge [
    source 37
    target 504
  ]
  edge [
    source 37
    target 505
  ]
  edge [
    source 37
    target 506
  ]
  edge [
    source 37
    target 507
  ]
  edge [
    source 37
    target 508
  ]
  edge [
    source 37
    target 509
  ]
  edge [
    source 37
    target 510
  ]
  edge [
    source 37
    target 511
  ]
  edge [
    source 37
    target 512
  ]
  edge [
    source 37
    target 513
  ]
  edge [
    source 37
    target 514
  ]
  edge [
    source 37
    target 515
  ]
  edge [
    source 37
    target 516
  ]
  edge [
    source 37
    target 517
  ]
  edge [
    source 37
    target 518
  ]
  edge [
    source 37
    target 519
  ]
  edge [
    source 37
    target 520
  ]
  edge [
    source 37
    target 521
  ]
  edge [
    source 37
    target 522
  ]
  edge [
    source 37
    target 523
  ]
  edge [
    source 37
    target 524
  ]
  edge [
    source 37
    target 525
  ]
  edge [
    source 37
    target 526
  ]
  edge [
    source 37
    target 527
  ]
  edge [
    source 37
    target 528
  ]
  edge [
    source 37
    target 529
  ]
  edge [
    source 37
    target 530
  ]
  edge [
    source 37
    target 531
  ]
  edge [
    source 37
    target 532
  ]
  edge [
    source 37
    target 533
  ]
  edge [
    source 37
    target 534
  ]
  edge [
    source 37
    target 535
  ]
  edge [
    source 37
    target 536
  ]
  edge [
    source 37
    target 537
  ]
  edge [
    source 37
    target 538
  ]
  edge [
    source 37
    target 539
  ]
  edge [
    source 37
    target 540
  ]
  edge [
    source 37
    target 541
  ]
  edge [
    source 37
    target 542
  ]
  edge [
    source 37
    target 543
  ]
  edge [
    source 37
    target 544
  ]
  edge [
    source 37
    target 545
  ]
  edge [
    source 37
    target 546
  ]
  edge [
    source 37
    target 547
  ]
  edge [
    source 37
    target 548
  ]
  edge [
    source 37
    target 549
  ]
  edge [
    source 37
    target 550
  ]
  edge [
    source 37
    target 551
  ]
  edge [
    source 37
    target 552
  ]
  edge [
    source 37
    target 553
  ]
  edge [
    source 37
    target 554
  ]
  edge [
    source 37
    target 555
  ]
  edge [
    source 37
    target 556
  ]
  edge [
    source 37
    target 557
  ]
  edge [
    source 37
    target 558
  ]
  edge [
    source 37
    target 559
  ]
  edge [
    source 37
    target 560
  ]
  edge [
    source 37
    target 561
  ]
  edge [
    source 37
    target 562
  ]
  edge [
    source 37
    target 563
  ]
  edge [
    source 37
    target 564
  ]
  edge [
    source 37
    target 565
  ]
  edge [
    source 37
    target 566
  ]
  edge [
    source 37
    target 567
  ]
  edge [
    source 37
    target 568
  ]
  edge [
    source 37
    target 569
  ]
  edge [
    source 37
    target 570
  ]
  edge [
    source 37
    target 571
  ]
  edge [
    source 37
    target 572
  ]
  edge [
    source 37
    target 573
  ]
  edge [
    source 37
    target 66
  ]
  edge [
    source 37
    target 46
  ]
  edge [
    source 37
    target 81
  ]
  edge [
    source 37
    target 92
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 59
  ]
  edge [
    source 38
    target 66
  ]
  edge [
    source 39
    target 574
  ]
  edge [
    source 39
    target 575
  ]
  edge [
    source 39
    target 576
  ]
  edge [
    source 39
    target 577
  ]
  edge [
    source 39
    target 578
  ]
  edge [
    source 39
    target 579
  ]
  edge [
    source 39
    target 580
  ]
  edge [
    source 39
    target 581
  ]
  edge [
    source 39
    target 582
  ]
  edge [
    source 39
    target 583
  ]
  edge [
    source 39
    target 186
  ]
  edge [
    source 39
    target 584
  ]
  edge [
    source 39
    target 585
  ]
  edge [
    source 39
    target 586
  ]
  edge [
    source 39
    target 111
  ]
  edge [
    source 39
    target 587
  ]
  edge [
    source 39
    target 588
  ]
  edge [
    source 39
    target 589
  ]
  edge [
    source 39
    target 590
  ]
  edge [
    source 39
    target 591
  ]
  edge [
    source 39
    target 592
  ]
  edge [
    source 39
    target 593
  ]
  edge [
    source 39
    target 594
  ]
  edge [
    source 39
    target 595
  ]
  edge [
    source 39
    target 181
  ]
  edge [
    source 39
    target 596
  ]
  edge [
    source 39
    target 95
  ]
  edge [
    source 39
    target 99
  ]
  edge [
    source 39
    target 100
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 597
  ]
  edge [
    source 40
    target 598
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 47
  ]
  edge [
    source 41
    target 48
  ]
  edge [
    source 41
    target 599
  ]
  edge [
    source 41
    target 600
  ]
  edge [
    source 41
    target 601
  ]
  edge [
    source 41
    target 602
  ]
  edge [
    source 41
    target 603
  ]
  edge [
    source 41
    target 71
  ]
  edge [
    source 41
    target 80
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 604
  ]
  edge [
    source 42
    target 605
  ]
  edge [
    source 42
    target 93
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 606
  ]
  edge [
    source 43
    target 607
  ]
  edge [
    source 43
    target 608
  ]
  edge [
    source 43
    target 609
  ]
  edge [
    source 43
    target 610
  ]
  edge [
    source 43
    target 611
  ]
  edge [
    source 43
    target 612
  ]
  edge [
    source 43
    target 613
  ]
  edge [
    source 43
    target 614
  ]
  edge [
    source 43
    target 87
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 615
  ]
  edge [
    source 44
    target 616
  ]
  edge [
    source 44
    target 617
  ]
  edge [
    source 44
    target 618
  ]
  edge [
    source 44
    target 619
  ]
  edge [
    source 44
    target 620
  ]
  edge [
    source 44
    target 621
  ]
  edge [
    source 44
    target 622
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 623
  ]
  edge [
    source 45
    target 624
  ]
  edge [
    source 45
    target 625
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 626
  ]
  edge [
    source 46
    target 627
  ]
  edge [
    source 46
    target 628
  ]
  edge [
    source 46
    target 92
  ]
  edge [
    source 47
    target 629
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 191
  ]
  edge [
    source 48
    target 630
  ]
  edge [
    source 48
    target 212
  ]
  edge [
    source 48
    target 631
  ]
  edge [
    source 48
    target 632
  ]
  edge [
    source 49
    target 633
  ]
  edge [
    source 49
    target 634
  ]
  edge [
    source 49
    target 84
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 57
  ]
  edge [
    source 50
    target 58
  ]
  edge [
    source 50
    target 63
  ]
  edge [
    source 50
    target 64
  ]
  edge [
    source 50
    target 65
  ]
  edge [
    source 50
    target 66
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 635
  ]
  edge [
    source 51
    target 636
  ]
  edge [
    source 51
    target 637
  ]
  edge [
    source 51
    target 638
  ]
  edge [
    source 51
    target 639
  ]
  edge [
    source 51
    target 640
  ]
  edge [
    source 51
    target 641
  ]
  edge [
    source 51
    target 57
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 642
  ]
  edge [
    source 53
    target 643
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 644
  ]
  edge [
    source 54
    target 593
  ]
  edge [
    source 54
    target 645
  ]
  edge [
    source 54
    target 646
  ]
  edge [
    source 54
    target 647
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 648
  ]
  edge [
    source 55
    target 649
  ]
  edge [
    source 55
    target 650
  ]
  edge [
    source 55
    target 651
  ]
  edge [
    source 55
    target 652
  ]
  edge [
    source 56
    target 653
  ]
  edge [
    source 57
    target 654
  ]
  edge [
    source 57
    target 655
  ]
  edge [
    source 57
    target 656
  ]
  edge [
    source 57
    target 657
  ]
  edge [
    source 57
    target 658
  ]
  edge [
    source 57
    target 659
  ]
  edge [
    source 57
    target 660
  ]
  edge [
    source 57
    target 661
  ]
  edge [
    source 57
    target 662
  ]
  edge [
    source 57
    target 663
  ]
  edge [
    source 57
    target 664
  ]
  edge [
    source 57
    target 665
  ]
  edge [
    source 57
    target 666
  ]
  edge [
    source 57
    target 667
  ]
  edge [
    source 57
    target 668
  ]
  edge [
    source 57
    target 669
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 670
  ]
  edge [
    source 58
    target 671
  ]
  edge [
    source 58
    target 672
  ]
  edge [
    source 58
    target 673
  ]
  edge [
    source 58
    target 674
  ]
  edge [
    source 58
    target 675
  ]
  edge [
    source 58
    target 676
  ]
  edge [
    source 59
    target 677
  ]
  edge [
    source 59
    target 678
  ]
  edge [
    source 59
    target 679
  ]
  edge [
    source 59
    target 680
  ]
  edge [
    source 59
    target 681
  ]
  edge [
    source 59
    target 682
  ]
  edge [
    source 59
    target 683
  ]
  edge [
    source 59
    target 684
  ]
  edge [
    source 59
    target 685
  ]
  edge [
    source 59
    target 686
  ]
  edge [
    source 59
    target 687
  ]
  edge [
    source 59
    target 688
  ]
  edge [
    source 59
    target 689
  ]
  edge [
    source 59
    target 690
  ]
  edge [
    source 59
    target 691
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 692
  ]
  edge [
    source 60
    target 693
  ]
  edge [
    source 60
    target 694
  ]
  edge [
    source 60
    target 695
  ]
  edge [
    source 60
    target 696
  ]
  edge [
    source 60
    target 697
  ]
  edge [
    source 60
    target 698
  ]
  edge [
    source 60
    target 68
  ]
  edge [
    source 60
    target 75
  ]
  edge [
    source 60
    target 78
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 699
  ]
  edge [
    source 61
    target 700
  ]
  edge [
    source 61
    target 701
  ]
  edge [
    source 61
    target 702
  ]
  edge [
    source 61
    target 703
  ]
  edge [
    source 61
    target 704
  ]
  edge [
    source 61
    target 705
  ]
  edge [
    source 61
    target 78
  ]
  edge [
    source 62
    target 706
  ]
  edge [
    source 62
    target 707
  ]
  edge [
    source 63
    target 708
  ]
  edge [
    source 63
    target 709
  ]
  edge [
    source 63
    target 710
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 711
  ]
  edge [
    source 64
    target 712
  ]
  edge [
    source 64
    target 713
  ]
  edge [
    source 64
    target 675
  ]
  edge [
    source 64
    target 714
  ]
  edge [
    source 64
    target 715
  ]
  edge [
    source 65
    target 716
  ]
  edge [
    source 66
    target 191
  ]
  edge [
    source 66
    target 192
  ]
  edge [
    source 66
    target 205
  ]
  edge [
    source 66
    target 206
  ]
  edge [
    source 66
    target 207
  ]
  edge [
    source 66
    target 197
  ]
  edge [
    source 66
    target 201
  ]
  edge [
    source 66
    target 194
  ]
  edge [
    source 66
    target 717
  ]
  edge [
    source 66
    target 718
  ]
  edge [
    source 66
    target 195
  ]
  edge [
    source 66
    target 200
  ]
  edge [
    source 66
    target 208
  ]
  edge [
    source 66
    target 209
  ]
  edge [
    source 66
    target 203
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 67
    target 719
  ]
  edge [
    source 67
    target 720
  ]
  edge [
    source 67
    target 721
  ]
  edge [
    source 67
    target 722
  ]
  edge [
    source 67
    target 723
  ]
  edge [
    source 67
    target 724
  ]
  edge [
    source 67
    target 725
  ]
  edge [
    source 67
    target 726
  ]
  edge [
    source 67
    target 727
  ]
  edge [
    source 67
    target 728
  ]
  edge [
    source 67
    target 729
  ]
  edge [
    source 67
    target 730
  ]
  edge [
    source 67
    target 731
  ]
  edge [
    source 67
    target 732
  ]
  edge [
    source 67
    target 733
  ]
  edge [
    source 67
    target 734
  ]
  edge [
    source 67
    target 735
  ]
  edge [
    source 67
    target 736
  ]
  edge [
    source 67
    target 737
  ]
  edge [
    source 67
    target 738
  ]
  edge [
    source 67
    target 739
  ]
  edge [
    source 67
    target 740
  ]
  edge [
    source 67
    target 741
  ]
  edge [
    source 67
    target 742
  ]
  edge [
    source 67
    target 743
  ]
  edge [
    source 67
    target 744
  ]
  edge [
    source 67
    target 745
  ]
  edge [
    source 67
    target 746
  ]
  edge [
    source 67
    target 747
  ]
  edge [
    source 67
    target 748
  ]
  edge [
    source 67
    target 749
  ]
  edge [
    source 67
    target 606
  ]
  edge [
    source 67
    target 750
  ]
  edge [
    source 67
    target 751
  ]
  edge [
    source 67
    target 752
  ]
  edge [
    source 67
    target 753
  ]
  edge [
    source 67
    target 754
  ]
  edge [
    source 67
    target 755
  ]
  edge [
    source 67
    target 756
  ]
  edge [
    source 67
    target 757
  ]
  edge [
    source 67
    target 758
  ]
  edge [
    source 67
    target 759
  ]
  edge [
    source 67
    target 760
  ]
  edge [
    source 67
    target 761
  ]
  edge [
    source 67
    target 762
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 68
    target 763
  ]
  edge [
    source 68
    target 764
  ]
  edge [
    source 68
    target 765
  ]
  edge [
    source 68
    target 766
  ]
  edge [
    source 68
    target 75
  ]
  edge [
    source 68
    target 78
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 69
    target 767
  ]
  edge [
    source 69
    target 768
  ]
  edge [
    source 69
    target 769
  ]
  edge [
    source 69
    target 770
  ]
  edge [
    source 69
    target 771
  ]
  edge [
    source 69
    target 76
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 71
    target 772
  ]
  edge [
    source 71
    target 773
  ]
  edge [
    source 71
    target 80
  ]
  edge [
    source 72
    target 774
  ]
  edge [
    source 72
    target 775
  ]
  edge [
    source 72
    target 776
  ]
  edge [
    source 72
    target 777
  ]
  edge [
    source 72
    target 778
  ]
  edge [
    source 72
    target 186
  ]
  edge [
    source 72
    target 779
  ]
  edge [
    source 72
    target 780
  ]
  edge [
    source 72
    target 781
  ]
  edge [
    source 72
    target 782
  ]
  edge [
    source 72
    target 783
  ]
  edge [
    source 72
    target 784
  ]
  edge [
    source 72
    target 785
  ]
  edge [
    source 72
    target 786
  ]
  edge [
    source 73
    target 224
  ]
  edge [
    source 73
    target 787
  ]
  edge [
    source 73
    target 788
  ]
  edge [
    source 73
    target 789
  ]
  edge [
    source 73
    target 187
  ]
  edge [
    source 73
    target 790
  ]
  edge [
    source 73
    target 575
  ]
  edge [
    source 73
    target 791
  ]
  edge [
    source 73
    target 792
  ]
  edge [
    source 73
    target 793
  ]
  edge [
    source 73
    target 794
  ]
  edge [
    source 73
    target 795
  ]
  edge [
    source 73
    target 796
  ]
  edge [
    source 74
    target 75
  ]
  edge [
    source 75
    target 797
  ]
  edge [
    source 75
    target 798
  ]
  edge [
    source 75
    target 799
  ]
  edge [
    source 75
    target 78
  ]
  edge [
    source 76
    target 77
  ]
  edge [
    source 76
    target 97
  ]
  edge [
    source 76
    target 800
  ]
  edge [
    source 76
    target 801
  ]
  edge [
    source 76
    target 802
  ]
  edge [
    source 76
    target 803
  ]
  edge [
    source 76
    target 804
  ]
  edge [
    source 76
    target 805
  ]
  edge [
    source 76
    target 806
  ]
  edge [
    source 76
    target 807
  ]
  edge [
    source 76
    target 92
  ]
  edge [
    source 76
    target 109
  ]
  edge [
    source 77
    target 808
  ]
  edge [
    source 78
    target 79
  ]
  edge [
    source 78
    target 809
  ]
  edge [
    source 78
    target 700
  ]
  edge [
    source 78
    target 810
  ]
  edge [
    source 78
    target 811
  ]
  edge [
    source 78
    target 812
  ]
  edge [
    source 78
    target 703
  ]
  edge [
    source 78
    target 813
  ]
  edge [
    source 78
    target 814
  ]
  edge [
    source 79
    target 80
  ]
  edge [
    source 80
    target 107
  ]
  edge [
    source 80
    target 108
  ]
  edge [
    source 80
    target 815
  ]
  edge [
    source 80
    target 816
  ]
  edge [
    source 80
    target 817
  ]
  edge [
    source 80
    target 818
  ]
  edge [
    source 80
    target 819
  ]
  edge [
    source 80
    target 152
  ]
  edge [
    source 80
    target 820
  ]
  edge [
    source 80
    target 821
  ]
  edge [
    source 80
    target 822
  ]
  edge [
    source 81
    target 82
  ]
  edge [
    source 82
    target 83
  ]
  edge [
    source 82
    target 823
  ]
  edge [
    source 82
    target 824
  ]
  edge [
    source 82
    target 825
  ]
  edge [
    source 82
    target 826
  ]
  edge [
    source 82
    target 827
  ]
  edge [
    source 82
    target 828
  ]
  edge [
    source 82
    target 829
  ]
  edge [
    source 82
    target 88
  ]
  edge [
    source 83
    target 84
  ]
  edge [
    source 83
    target 830
  ]
  edge [
    source 83
    target 831
  ]
  edge [
    source 83
    target 832
  ]
  edge [
    source 83
    target 833
  ]
  edge [
    source 83
    target 834
  ]
  edge [
    source 83
    target 835
  ]
  edge [
    source 83
    target 617
  ]
  edge [
    source 83
    target 836
  ]
  edge [
    source 83
    target 118
  ]
  edge [
    source 83
    target 837
  ]
  edge [
    source 83
    target 838
  ]
  edge [
    source 84
    target 85
  ]
  edge [
    source 84
    target 86
  ]
  edge [
    source 84
    target 839
  ]
  edge [
    source 84
    target 607
  ]
  edge [
    source 84
    target 840
  ]
  edge [
    source 84
    target 841
  ]
  edge [
    source 84
    target 234
  ]
  edge [
    source 84
    target 610
  ]
  edge [
    source 84
    target 611
  ]
  edge [
    source 84
    target 612
  ]
  edge [
    source 84
    target 842
  ]
  edge [
    source 84
    target 843
  ]
  edge [
    source 85
    target 844
  ]
  edge [
    source 85
    target 845
  ]
  edge [
    source 85
    target 846
  ]
  edge [
    source 85
    target 847
  ]
  edge [
    source 85
    target 848
  ]
  edge [
    source 85
    target 193
  ]
  edge [
    source 85
    target 770
  ]
  edge [
    source 85
    target 849
  ]
  edge [
    source 85
    target 850
  ]
  edge [
    source 85
    target 851
  ]
  edge [
    source 85
    target 852
  ]
  edge [
    source 85
    target 853
  ]
  edge [
    source 85
    target 854
  ]
  edge [
    source 85
    target 855
  ]
  edge [
    source 85
    target 856
  ]
  edge [
    source 85
    target 857
  ]
  edge [
    source 85
    target 858
  ]
  edge [
    source 85
    target 859
  ]
  edge [
    source 85
    target 860
  ]
  edge [
    source 86
    target 87
  ]
  edge [
    source 86
    target 861
  ]
  edge [
    source 86
    target 862
  ]
  edge [
    source 86
    target 863
  ]
  edge [
    source 86
    target 864
  ]
  edge [
    source 86
    target 133
  ]
  edge [
    source 86
    target 865
  ]
  edge [
    source 86
    target 866
  ]
  edge [
    source 86
    target 867
  ]
  edge [
    source 86
    target 868
  ]
  edge [
    source 86
    target 869
  ]
  edge [
    source 86
    target 870
  ]
  edge [
    source 86
    target 871
  ]
  edge [
    source 86
    target 872
  ]
  edge [
    source 86
    target 135
  ]
  edge [
    source 86
    target 873
  ]
  edge [
    source 86
    target 874
  ]
  edge [
    source 86
    target 875
  ]
  edge [
    source 86
    target 876
  ]
  edge [
    source 86
    target 290
  ]
  edge [
    source 86
    target 877
  ]
  edge [
    source 86
    target 821
  ]
  edge [
    source 86
    target 878
  ]
  edge [
    source 86
    target 136
  ]
  edge [
    source 86
    target 879
  ]
  edge [
    source 86
    target 100
  ]
  edge [
    source 87
    target 88
  ]
  edge [
    source 87
    target 880
  ]
  edge [
    source 87
    target 881
  ]
  edge [
    source 87
    target 882
  ]
  edge [
    source 87
    target 883
  ]
  edge [
    source 87
    target 884
  ]
  edge [
    source 87
    target 885
  ]
  edge [
    source 87
    target 886
  ]
  edge [
    source 87
    target 887
  ]
  edge [
    source 87
    target 888
  ]
  edge [
    source 87
    target 889
  ]
  edge [
    source 87
    target 890
  ]
  edge [
    source 87
    target 891
  ]
  edge [
    source 87
    target 892
  ]
  edge [
    source 87
    target 893
  ]
  edge [
    source 87
    target 894
  ]
  edge [
    source 87
    target 895
  ]
  edge [
    source 87
    target 896
  ]
  edge [
    source 87
    target 897
  ]
  edge [
    source 87
    target 898
  ]
  edge [
    source 87
    target 899
  ]
  edge [
    source 87
    target 900
  ]
  edge [
    source 87
    target 901
  ]
  edge [
    source 87
    target 902
  ]
  edge [
    source 87
    target 903
  ]
  edge [
    source 87
    target 904
  ]
  edge [
    source 87
    target 905
  ]
  edge [
    source 87
    target 906
  ]
  edge [
    source 87
    target 907
  ]
  edge [
    source 87
    target 908
  ]
  edge [
    source 87
    target 909
  ]
  edge [
    source 87
    target 910
  ]
  edge [
    source 87
    target 911
  ]
  edge [
    source 88
    target 89
  ]
  edge [
    source 89
    target 90
  ]
  edge [
    source 89
    target 154
  ]
  edge [
    source 89
    target 912
  ]
  edge [
    source 89
    target 913
  ]
  edge [
    source 89
    target 914
  ]
  edge [
    source 89
    target 915
  ]
  edge [
    source 89
    target 916
  ]
  edge [
    source 89
    target 917
  ]
  edge [
    source 89
    target 918
  ]
  edge [
    source 89
    target 919
  ]
  edge [
    source 89
    target 103
  ]
  edge [
    source 90
    target 91
  ]
  edge [
    source 90
    target 920
  ]
  edge [
    source 90
    target 921
  ]
  edge [
    source 91
    target 92
  ]
  edge [
    source 91
    target 604
  ]
  edge [
    source 91
    target 922
  ]
  edge [
    source 92
    target 748
  ]
  edge [
    source 92
    target 923
  ]
  edge [
    source 92
    target 924
  ]
  edge [
    source 92
    target 925
  ]
  edge [
    source 92
    target 926
  ]
  edge [
    source 92
    target 927
  ]
  edge [
    source 92
    target 109
  ]
  edge [
    source 93
    target 94
  ]
  edge [
    source 93
    target 928
  ]
  edge [
    source 93
    target 929
  ]
  edge [
    source 93
    target 930
  ]
  edge [
    source 93
    target 931
  ]
  edge [
    source 93
    target 922
  ]
  edge [
    source 93
    target 932
  ]
  edge [
    source 93
    target 933
  ]
  edge [
    source 93
    target 934
  ]
  edge [
    source 93
    target 935
  ]
  edge [
    source 93
    target 936
  ]
  edge [
    source 94
    target 705
  ]
  edge [
    source 94
    target 937
  ]
  edge [
    source 94
    target 938
  ]
  edge [
    source 94
    target 939
  ]
  edge [
    source 94
    target 940
  ]
  edge [
    source 94
    target 941
  ]
  edge [
    source 94
    target 942
  ]
  edge [
    source 94
    target 360
  ]
  edge [
    source 94
    target 943
  ]
  edge [
    source 94
    target 944
  ]
  edge [
    source 94
    target 137
  ]
  edge [
    source 95
    target 96
  ]
  edge [
    source 95
    target 99
  ]
  edge [
    source 95
    target 100
  ]
  edge [
    source 96
    target 114
  ]
  edge [
    source 96
    target 945
  ]
  edge [
    source 96
    target 946
  ]
  edge [
    source 96
    target 947
  ]
  edge [
    source 96
    target 948
  ]
  edge [
    source 97
    target 98
  ]
  edge [
    source 97
    target 703
  ]
  edge [
    source 97
    target 702
  ]
  edge [
    source 97
    target 949
  ]
  edge [
    source 97
    target 950
  ]
  edge [
    source 98
    target 951
  ]
  edge [
    source 98
    target 952
  ]
  edge [
    source 99
    target 100
  ]
  edge [
    source 99
    target 101
  ]
  edge [
    source 100
    target 101
  ]
  edge [
    source 100
    target 102
  ]
  edge [
    source 100
    target 953
  ]
  edge [
    source 100
    target 290
  ]
  edge [
    source 100
    target 954
  ]
  edge [
    source 100
    target 862
  ]
  edge [
    source 100
    target 955
  ]
  edge [
    source 100
    target 956
  ]
  edge [
    source 100
    target 957
  ]
  edge [
    source 100
    target 958
  ]
  edge [
    source 100
    target 872
  ]
  edge [
    source 100
    target 959
  ]
  edge [
    source 100
    target 786
  ]
  edge [
    source 101
    target 960
  ]
  edge [
    source 101
    target 961
  ]
  edge [
    source 101
    target 962
  ]
  edge [
    source 101
    target 963
  ]
  edge [
    source 101
    target 327
  ]
  edge [
    source 101
    target 319
  ]
  edge [
    source 101
    target 964
  ]
  edge [
    source 101
    target 337
  ]
  edge [
    source 101
    target 199
  ]
  edge [
    source 101
    target 313
  ]
  edge [
    source 101
    target 965
  ]
  edge [
    source 101
    target 966
  ]
  edge [
    source 101
    target 967
  ]
  edge [
    source 101
    target 334
  ]
  edge [
    source 101
    target 968
  ]
  edge [
    source 102
    target 103
  ]
  edge [
    source 102
    target 960
  ]
  edge [
    source 102
    target 961
  ]
  edge [
    source 102
    target 962
  ]
  edge [
    source 102
    target 963
  ]
  edge [
    source 102
    target 964
  ]
  edge [
    source 102
    target 965
  ]
  edge [
    source 102
    target 966
  ]
  edge [
    source 102
    target 967
  ]
  edge [
    source 102
    target 968
  ]
  edge [
    source 102
    target 105
  ]
  edge [
    source 103
    target 104
  ]
  edge [
    source 104
    target 105
  ]
  edge [
    source 104
    target 969
  ]
  edge [
    source 104
    target 970
  ]
  edge [
    source 104
    target 971
  ]
  edge [
    source 104
    target 972
  ]
  edge [
    source 104
    target 702
  ]
  edge [
    source 104
    target 973
  ]
  edge [
    source 104
    target 974
  ]
  edge [
    source 104
    target 975
  ]
  edge [
    source 105
    target 106
  ]
  edge [
    source 105
    target 976
  ]
  edge [
    source 105
    target 977
  ]
  edge [
    source 105
    target 978
  ]
  edge [
    source 106
    target 107
  ]
  edge [
    source 106
    target 979
  ]
  edge [
    source 106
    target 980
  ]
  edge [
    source 106
    target 981
  ]
  edge [
    source 106
    target 982
  ]
  edge [
    source 106
    target 983
  ]
  edge [
    source 106
    target 984
  ]
  edge [
    source 106
    target 985
  ]
  edge [
    source 106
    target 986
  ]
  edge [
    source 106
    target 987
  ]
  edge [
    source 106
    target 988
  ]
  edge [
    source 106
    target 989
  ]
  edge [
    source 108
    target 109
  ]
  edge [
    source 109
    target 990
  ]
  edge [
    source 109
    target 991
  ]
  edge [
    source 109
    target 992
  ]
]
