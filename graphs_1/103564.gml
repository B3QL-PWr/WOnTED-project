graph [
  maxDegree 15
  minDegree 1
  meanDegree 1.875
  density 0.125
  graphCliqueNumber 2
  node [
    id 0
    label "aktor"
    origin "text"
  ]
  node [
    id 1
    label "fanfaron"
  ]
  node [
    id 2
    label "Eastwood"
  ]
  node [
    id 3
    label "odtw&#243;rca"
  ]
  node [
    id 4
    label "podmiot"
  ]
  node [
    id 5
    label "teatr"
  ]
  node [
    id 6
    label "bajerant"
  ]
  node [
    id 7
    label "Allen"
  ]
  node [
    id 8
    label "obsada"
  ]
  node [
    id 9
    label "pracownik"
  ]
  node [
    id 10
    label "Roland_Topor"
  ]
  node [
    id 11
    label "Daniel_Olbrychski"
  ]
  node [
    id 12
    label "interpretator"
  ]
  node [
    id 13
    label "Stuhr"
  ]
  node [
    id 14
    label "uczestnik"
  ]
  node [
    id 15
    label "wykonawca"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
]
