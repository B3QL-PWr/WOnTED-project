graph [
  maxDegree 13
  minDegree 1
  meanDegree 1.894736842105263
  density 0.10526315789473684
  graphCliqueNumber 2
  node [
    id 0
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 1
    label "jebanyn"
    origin "text"
  ]
  node [
    id 2
    label "razem"
    origin "text"
  ]
  node [
    id 3
    label "humorobrazkowy"
    origin "text"
  ]
  node [
    id 4
    label "lekarstwo"
    origin "text"
  ]
  node [
    id 5
    label "heheszki"
    origin "text"
  ]
  node [
    id 6
    label "jaki&#347;"
  ]
  node [
    id 7
    label "&#322;&#261;cznie"
  ]
  node [
    id 8
    label "naszprycowa&#263;"
  ]
  node [
    id 9
    label "tonizowa&#263;"
  ]
  node [
    id 10
    label "medicine"
  ]
  node [
    id 11
    label "szprycowa&#263;"
  ]
  node [
    id 12
    label "przepisanie"
  ]
  node [
    id 13
    label "przepisa&#263;"
  ]
  node [
    id 14
    label "tonizowanie"
  ]
  node [
    id 15
    label "szprycowanie"
  ]
  node [
    id 16
    label "naszprycowanie"
  ]
  node [
    id 17
    label "apteczka"
  ]
  node [
    id 18
    label "substancja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 8
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 4
    target 10
  ]
  edge [
    source 4
    target 11
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
]
