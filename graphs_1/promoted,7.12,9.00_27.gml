graph [
  maxDegree 11
  minDegree 1
  meanDegree 1.9130434782608696
  density 0.08695652173913043
  graphCliqueNumber 2
  node [
    id 0
    label "babcia"
    origin "text"
  ]
  node [
    id 1
    label "letni"
    origin "text"
  ]
  node [
    id 2
    label "wnuczek"
    origin "text"
  ]
  node [
    id 3
    label "pewno"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "spodziewa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "babulinka"
  ]
  node [
    id 7
    label "przodkini"
  ]
  node [
    id 8
    label "kobieta"
  ]
  node [
    id 9
    label "baba"
  ]
  node [
    id 10
    label "starszy_cz&#322;owiek"
  ]
  node [
    id 11
    label "dziadkowie"
  ]
  node [
    id 12
    label "nijaki"
  ]
  node [
    id 13
    label "sezonowy"
  ]
  node [
    id 14
    label "letnio"
  ]
  node [
    id 15
    label "s&#322;oneczny"
  ]
  node [
    id 16
    label "weso&#322;y"
  ]
  node [
    id 17
    label "oboj&#281;tny"
  ]
  node [
    id 18
    label "latowy"
  ]
  node [
    id 19
    label "ciep&#322;y"
  ]
  node [
    id 20
    label "typowy"
  ]
  node [
    id 21
    label "wnucz&#281;"
  ]
  node [
    id 22
    label "potomek"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
]
