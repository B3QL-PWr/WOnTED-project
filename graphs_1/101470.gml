graph [
  maxDegree 7
  minDegree 1
  meanDegree 2.1818181818181817
  density 0.21818181818181817
  graphCliqueNumber 4
  node [
    id 0
    label "castel"
    origin "text"
  ]
  node [
    id 1
    label "san"
    origin "text"
  ]
  node [
    id 2
    label "pietro"
    origin "text"
  ]
  node [
    id 3
    label "terme"
    origin "text"
  ]
  node [
    id 4
    label "alfabet_grecki"
  ]
  node [
    id 5
    label "litera"
  ]
  node [
    id 6
    label "Castel"
  ]
  node [
    id 7
    label "Pietro"
  ]
  node [
    id 8
    label "Terme"
  ]
  node [
    id 9
    label "Emilia"
  ]
  node [
    id 10
    label "Romania"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 9
    target 10
  ]
]
