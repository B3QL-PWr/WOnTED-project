graph [
  maxDegree 63
  minDegree 1
  meanDegree 2.0295566502463056
  density 0.010047310149734185
  graphCliqueNumber 2
  node [
    id 0
    label "nowa"
    origin "text"
  ]
  node [
    id 1
    label "badan"
    origin "text"
  ]
  node [
    id 2
    label "pokazowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "latkowie"
    origin "text"
  ]
  node [
    id 4
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 5
    label "przez"
    origin "text"
  ]
  node [
    id 6
    label "lato"
    origin "text"
  ]
  node [
    id 7
    label "by&#322;y"
    origin "text"
  ]
  node [
    id 8
    label "by&#263;"
    origin "text"
  ]
  node [
    id 9
    label "aktywny"
    origin "text"
  ]
  node [
    id 10
    label "fizycznie"
    origin "text"
  ]
  node [
    id 11
    label "maja"
    origin "text"
  ]
  node [
    id 12
    label "p&#322;uco"
    origin "text"
  ]
  node [
    id 13
    label "serce"
    origin "text"
  ]
  node [
    id 14
    label "mi&#281;sie&#324;"
    origin "text"
  ]
  node [
    id 15
    label "og&#243;lny"
    origin "text"
  ]
  node [
    id 16
    label "stan"
    origin "text"
  ]
  node [
    id 17
    label "zdrowie"
    origin "text"
  ]
  node [
    id 18
    label "gwiazda"
  ]
  node [
    id 19
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 20
    label "pora_roku"
  ]
  node [
    id 21
    label "dawny"
  ]
  node [
    id 22
    label "rozw&#243;d"
  ]
  node [
    id 23
    label "rozwodzi&#263;_si&#281;"
  ]
  node [
    id 24
    label "eksprezydent"
  ]
  node [
    id 25
    label "rozwie&#347;&#263;_si&#281;"
  ]
  node [
    id 26
    label "partner"
  ]
  node [
    id 27
    label "rozwiedzenie_si&#281;"
  ]
  node [
    id 28
    label "rozwodzenie_si&#281;"
  ]
  node [
    id 29
    label "wcze&#347;niejszy"
  ]
  node [
    id 30
    label "si&#281;ga&#263;"
  ]
  node [
    id 31
    label "trwa&#263;"
  ]
  node [
    id 32
    label "obecno&#347;&#263;"
  ]
  node [
    id 33
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 34
    label "stand"
  ]
  node [
    id 35
    label "mie&#263;_miejsce"
  ]
  node [
    id 36
    label "uczestniczy&#263;"
  ]
  node [
    id 37
    label "chodzi&#263;"
  ]
  node [
    id 38
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 39
    label "equal"
  ]
  node [
    id 40
    label "czynny"
  ]
  node [
    id 41
    label "dzia&#322;anie"
  ]
  node [
    id 42
    label "uczynnienie"
  ]
  node [
    id 43
    label "czynnie"
  ]
  node [
    id 44
    label "realny"
  ]
  node [
    id 45
    label "aktywnie"
  ]
  node [
    id 46
    label "wa&#380;ny"
  ]
  node [
    id 47
    label "intensywny"
  ]
  node [
    id 48
    label "ciekawy"
  ]
  node [
    id 49
    label "faktyczny"
  ]
  node [
    id 50
    label "istotny"
  ]
  node [
    id 51
    label "zdolny"
  ]
  node [
    id 52
    label "uczynnianie"
  ]
  node [
    id 53
    label "namacalnie"
  ]
  node [
    id 54
    label "fizykalny"
  ]
  node [
    id 55
    label "fizyczny"
  ]
  node [
    id 56
    label "po_newtonowsku"
  ]
  node [
    id 57
    label "physically"
  ]
  node [
    id 58
    label "forcibly"
  ]
  node [
    id 59
    label "wedyzm"
  ]
  node [
    id 60
    label "energia"
  ]
  node [
    id 61
    label "buddyzm"
  ]
  node [
    id 62
    label "p&#322;uca"
  ]
  node [
    id 63
    label "wrodzona_gruczolakowato&#347;&#263;_torbielowata_p&#322;uc"
  ]
  node [
    id 64
    label "hipoplazja_p&#322;uca"
  ]
  node [
    id 65
    label "op&#322;ucna"
  ]
  node [
    id 66
    label "lung"
  ]
  node [
    id 67
    label "niedodma"
  ]
  node [
    id 68
    label "cz&#322;owiek"
  ]
  node [
    id 69
    label "courage"
  ]
  node [
    id 70
    label "kompleks"
  ]
  node [
    id 71
    label "sfera_afektywna"
  ]
  node [
    id 72
    label "heart"
  ]
  node [
    id 73
    label "sumienie"
  ]
  node [
    id 74
    label "przedsionek"
  ]
  node [
    id 75
    label "entity"
  ]
  node [
    id 76
    label "nastawienie"
  ]
  node [
    id 77
    label "punkt"
  ]
  node [
    id 78
    label "kompleksja"
  ]
  node [
    id 79
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 80
    label "kier"
  ]
  node [
    id 81
    label "systol"
  ]
  node [
    id 82
    label "pikawa"
  ]
  node [
    id 83
    label "power"
  ]
  node [
    id 84
    label "zastawka"
  ]
  node [
    id 85
    label "psychika"
  ]
  node [
    id 86
    label "wola"
  ]
  node [
    id 87
    label "struna_&#347;ci&#281;gnista"
  ]
  node [
    id 88
    label "komora"
  ]
  node [
    id 89
    label "zapalno&#347;&#263;"
  ]
  node [
    id 90
    label "wsierdzie"
  ]
  node [
    id 91
    label "mi&#281;sie&#324;_sercowy"
  ]
  node [
    id 92
    label "podekscytowanie"
  ]
  node [
    id 93
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 94
    label "strunowiec"
  ]
  node [
    id 95
    label "favor"
  ]
  node [
    id 96
    label "dusza"
  ]
  node [
    id 97
    label "fizjonomia"
  ]
  node [
    id 98
    label "uk&#322;ad_przedsionkowo-komorowy"
  ]
  node [
    id 99
    label "przegroda_mi&#281;dzykomorowa"
  ]
  node [
    id 100
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 101
    label "charakter"
  ]
  node [
    id 102
    label "mikrokosmos"
  ]
  node [
    id 103
    label "dzwon"
  ]
  node [
    id 104
    label "koniuszek_serca"
  ]
  node [
    id 105
    label "pozytywno&#347;&#263;"
  ]
  node [
    id 106
    label "passion"
  ]
  node [
    id 107
    label "cecha"
  ]
  node [
    id 108
    label "pulsowa&#263;"
  ]
  node [
    id 109
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 110
    label "organ"
  ]
  node [
    id 111
    label "ego"
  ]
  node [
    id 112
    label "w&#281;ze&#322;_zatokowo-przedsionkowy"
  ]
  node [
    id 113
    label "kardiografia"
  ]
  node [
    id 114
    label "osobowo&#347;&#263;"
  ]
  node [
    id 115
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 116
    label "kszta&#322;t"
  ]
  node [
    id 117
    label "karta"
  ]
  node [
    id 118
    label "zatoka_wie&#324;cowa"
  ]
  node [
    id 119
    label "dobro&#263;"
  ]
  node [
    id 120
    label "podroby"
  ]
  node [
    id 121
    label "mi&#281;sie&#324;_brodawkowaty"
  ]
  node [
    id 122
    label "pulsowanie"
  ]
  node [
    id 123
    label "deformowa&#263;"
  ]
  node [
    id 124
    label "&#347;r&#243;dsierdzie"
  ]
  node [
    id 125
    label "seksualno&#347;&#263;"
  ]
  node [
    id 126
    label "deformowanie"
  ]
  node [
    id 127
    label "uk&#322;ad_krwiono&#347;ny"
  ]
  node [
    id 128
    label "elektrokardiografia"
  ]
  node [
    id 129
    label "dogrzewanie"
  ]
  node [
    id 130
    label "dogrza&#263;"
  ]
  node [
    id 131
    label "masa_mi&#281;&#347;niowa"
  ]
  node [
    id 132
    label "dogrzanie"
  ]
  node [
    id 133
    label "brzusiec"
  ]
  node [
    id 134
    label "dogrzewa&#263;"
  ]
  node [
    id 135
    label "elektromiografia"
  ]
  node [
    id 136
    label "niedow&#322;ad_po&#322;owiczy"
  ]
  node [
    id 137
    label "fosfagen"
  ]
  node [
    id 138
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 139
    label "hemiplegia"
  ]
  node [
    id 140
    label "&#347;ci&#281;gno"
  ]
  node [
    id 141
    label "przyczep"
  ]
  node [
    id 142
    label "nadrz&#281;dny"
  ]
  node [
    id 143
    label "zbiorowy"
  ]
  node [
    id 144
    label "&#322;&#261;czny"
  ]
  node [
    id 145
    label "kompletny"
  ]
  node [
    id 146
    label "og&#243;lnie"
  ]
  node [
    id 147
    label "ca&#322;y"
  ]
  node [
    id 148
    label "og&#243;&#322;owy"
  ]
  node [
    id 149
    label "powszechnie"
  ]
  node [
    id 150
    label "Arizona"
  ]
  node [
    id 151
    label "Georgia"
  ]
  node [
    id 152
    label "warstwa"
  ]
  node [
    id 153
    label "jednostka_administracyjna"
  ]
  node [
    id 154
    label "Hawaje"
  ]
  node [
    id 155
    label "Goa"
  ]
  node [
    id 156
    label "Floryda"
  ]
  node [
    id 157
    label "Oklahoma"
  ]
  node [
    id 158
    label "Alaska"
  ]
  node [
    id 159
    label "wci&#281;cie"
  ]
  node [
    id 160
    label "Alabama"
  ]
  node [
    id 161
    label "Oregon"
  ]
  node [
    id 162
    label "poziom"
  ]
  node [
    id 163
    label "Teksas"
  ]
  node [
    id 164
    label "Illinois"
  ]
  node [
    id 165
    label "Waszyngton"
  ]
  node [
    id 166
    label "Jukatan"
  ]
  node [
    id 167
    label "shape"
  ]
  node [
    id 168
    label "Nowy_Meksyk"
  ]
  node [
    id 169
    label "ilo&#347;&#263;"
  ]
  node [
    id 170
    label "state"
  ]
  node [
    id 171
    label "Nowy_York"
  ]
  node [
    id 172
    label "Arakan"
  ]
  node [
    id 173
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 174
    label "Kalifornia"
  ]
  node [
    id 175
    label "wektor"
  ]
  node [
    id 176
    label "Massachusetts"
  ]
  node [
    id 177
    label "miejsce"
  ]
  node [
    id 178
    label "Pensylwania"
  ]
  node [
    id 179
    label "Michigan"
  ]
  node [
    id 180
    label "Maryland"
  ]
  node [
    id 181
    label "Ohio"
  ]
  node [
    id 182
    label "Kansas"
  ]
  node [
    id 183
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 184
    label "Luizjana"
  ]
  node [
    id 185
    label "samopoczucie"
  ]
  node [
    id 186
    label "Wirginia"
  ]
  node [
    id 187
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 188
    label "os&#322;abia&#263;"
  ]
  node [
    id 189
    label "niszczy&#263;"
  ]
  node [
    id 190
    label "zniszczy&#263;"
  ]
  node [
    id 191
    label "firmness"
  ]
  node [
    id 192
    label "kondycja"
  ]
  node [
    id 193
    label "os&#322;abi&#263;"
  ]
  node [
    id 194
    label "rozsypanie_si&#281;"
  ]
  node [
    id 195
    label "zdarcie"
  ]
  node [
    id 196
    label "zniszczenie"
  ]
  node [
    id 197
    label "zedrze&#263;"
  ]
  node [
    id 198
    label "zdrowotno&#347;&#263;"
  ]
  node [
    id 199
    label "niszczenie"
  ]
  node [
    id 200
    label "os&#322;abienie"
  ]
  node [
    id 201
    label "soundness"
  ]
  node [
    id 202
    label "os&#322;abianie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 20
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 21
  ]
  edge [
    source 7
    target 22
  ]
  edge [
    source 7
    target 23
  ]
  edge [
    source 7
    target 24
  ]
  edge [
    source 7
    target 25
  ]
  edge [
    source 7
    target 26
  ]
  edge [
    source 7
    target 27
  ]
  edge [
    source 7
    target 28
  ]
  edge [
    source 7
    target 29
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 30
  ]
  edge [
    source 8
    target 31
  ]
  edge [
    source 8
    target 32
  ]
  edge [
    source 8
    target 16
  ]
  edge [
    source 8
    target 33
  ]
  edge [
    source 8
    target 34
  ]
  edge [
    source 8
    target 35
  ]
  edge [
    source 8
    target 36
  ]
  edge [
    source 8
    target 37
  ]
  edge [
    source 8
    target 38
  ]
  edge [
    source 8
    target 39
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 40
  ]
  edge [
    source 9
    target 41
  ]
  edge [
    source 9
    target 42
  ]
  edge [
    source 9
    target 43
  ]
  edge [
    source 9
    target 44
  ]
  edge [
    source 9
    target 45
  ]
  edge [
    source 9
    target 46
  ]
  edge [
    source 9
    target 47
  ]
  edge [
    source 9
    target 48
  ]
  edge [
    source 9
    target 49
  ]
  edge [
    source 9
    target 50
  ]
  edge [
    source 9
    target 51
  ]
  edge [
    source 9
    target 52
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 53
  ]
  edge [
    source 10
    target 54
  ]
  edge [
    source 10
    target 55
  ]
  edge [
    source 10
    target 56
  ]
  edge [
    source 10
    target 57
  ]
  edge [
    source 10
    target 58
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 59
  ]
  edge [
    source 11
    target 60
  ]
  edge [
    source 11
    target 61
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 62
  ]
  edge [
    source 12
    target 63
  ]
  edge [
    source 12
    target 64
  ]
  edge [
    source 12
    target 65
  ]
  edge [
    source 12
    target 66
  ]
  edge [
    source 12
    target 67
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 68
  ]
  edge [
    source 13
    target 69
  ]
  edge [
    source 13
    target 70
  ]
  edge [
    source 13
    target 71
  ]
  edge [
    source 13
    target 72
  ]
  edge [
    source 13
    target 73
  ]
  edge [
    source 13
    target 74
  ]
  edge [
    source 13
    target 75
  ]
  edge [
    source 13
    target 76
  ]
  edge [
    source 13
    target 77
  ]
  edge [
    source 13
    target 78
  ]
  edge [
    source 13
    target 79
  ]
  edge [
    source 13
    target 80
  ]
  edge [
    source 13
    target 81
  ]
  edge [
    source 13
    target 82
  ]
  edge [
    source 13
    target 83
  ]
  edge [
    source 13
    target 84
  ]
  edge [
    source 13
    target 85
  ]
  edge [
    source 13
    target 86
  ]
  edge [
    source 13
    target 87
  ]
  edge [
    source 13
    target 88
  ]
  edge [
    source 13
    target 89
  ]
  edge [
    source 13
    target 90
  ]
  edge [
    source 13
    target 91
  ]
  edge [
    source 13
    target 92
  ]
  edge [
    source 13
    target 93
  ]
  edge [
    source 13
    target 94
  ]
  edge [
    source 13
    target 95
  ]
  edge [
    source 13
    target 96
  ]
  edge [
    source 13
    target 97
  ]
  edge [
    source 13
    target 98
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 13
    target 101
  ]
  edge [
    source 13
    target 102
  ]
  edge [
    source 13
    target 103
  ]
  edge [
    source 13
    target 104
  ]
  edge [
    source 13
    target 105
  ]
  edge [
    source 13
    target 106
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 13
    target 113
  ]
  edge [
    source 13
    target 114
  ]
  edge [
    source 13
    target 115
  ]
  edge [
    source 13
    target 116
  ]
  edge [
    source 13
    target 117
  ]
  edge [
    source 13
    target 118
  ]
  edge [
    source 13
    target 119
  ]
  edge [
    source 13
    target 120
  ]
  edge [
    source 13
    target 121
  ]
  edge [
    source 13
    target 122
  ]
  edge [
    source 13
    target 123
  ]
  edge [
    source 13
    target 124
  ]
  edge [
    source 13
    target 125
  ]
  edge [
    source 13
    target 126
  ]
  edge [
    source 13
    target 127
  ]
  edge [
    source 13
    target 128
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 129
  ]
  edge [
    source 14
    target 130
  ]
  edge [
    source 14
    target 131
  ]
  edge [
    source 14
    target 132
  ]
  edge [
    source 14
    target 133
  ]
  edge [
    source 14
    target 134
  ]
  edge [
    source 14
    target 135
  ]
  edge [
    source 14
    target 136
  ]
  edge [
    source 14
    target 137
  ]
  edge [
    source 14
    target 138
  ]
  edge [
    source 14
    target 139
  ]
  edge [
    source 14
    target 140
  ]
  edge [
    source 14
    target 141
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 142
  ]
  edge [
    source 15
    target 143
  ]
  edge [
    source 15
    target 144
  ]
  edge [
    source 15
    target 145
  ]
  edge [
    source 15
    target 146
  ]
  edge [
    source 15
    target 147
  ]
  edge [
    source 15
    target 148
  ]
  edge [
    source 15
    target 149
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 150
  ]
  edge [
    source 16
    target 151
  ]
  edge [
    source 16
    target 152
  ]
  edge [
    source 16
    target 153
  ]
  edge [
    source 16
    target 154
  ]
  edge [
    source 16
    target 155
  ]
  edge [
    source 16
    target 156
  ]
  edge [
    source 16
    target 157
  ]
  edge [
    source 16
    target 77
  ]
  edge [
    source 16
    target 158
  ]
  edge [
    source 16
    target 159
  ]
  edge [
    source 16
    target 160
  ]
  edge [
    source 16
    target 161
  ]
  edge [
    source 16
    target 162
  ]
  edge [
    source 16
    target 163
  ]
  edge [
    source 16
    target 164
  ]
  edge [
    source 16
    target 165
  ]
  edge [
    source 16
    target 166
  ]
  edge [
    source 16
    target 167
  ]
  edge [
    source 16
    target 168
  ]
  edge [
    source 16
    target 169
  ]
  edge [
    source 16
    target 170
  ]
  edge [
    source 16
    target 171
  ]
  edge [
    source 16
    target 172
  ]
  edge [
    source 16
    target 173
  ]
  edge [
    source 16
    target 174
  ]
  edge [
    source 16
    target 175
  ]
  edge [
    source 16
    target 176
  ]
  edge [
    source 16
    target 177
  ]
  edge [
    source 16
    target 178
  ]
  edge [
    source 16
    target 179
  ]
  edge [
    source 16
    target 180
  ]
  edge [
    source 16
    target 181
  ]
  edge [
    source 16
    target 182
  ]
  edge [
    source 16
    target 183
  ]
  edge [
    source 16
    target 184
  ]
  edge [
    source 16
    target 185
  ]
  edge [
    source 16
    target 186
  ]
  edge [
    source 16
    target 187
  ]
  edge [
    source 17
    target 188
  ]
  edge [
    source 17
    target 189
  ]
  edge [
    source 17
    target 190
  ]
  edge [
    source 17
    target 191
  ]
  edge [
    source 17
    target 192
  ]
  edge [
    source 17
    target 193
  ]
  edge [
    source 17
    target 194
  ]
  edge [
    source 17
    target 195
  ]
  edge [
    source 17
    target 107
  ]
  edge [
    source 17
    target 196
  ]
  edge [
    source 17
    target 197
  ]
  edge [
    source 17
    target 198
  ]
  edge [
    source 17
    target 199
  ]
  edge [
    source 17
    target 200
  ]
  edge [
    source 17
    target 201
  ]
  edge [
    source 17
    target 202
  ]
]
