graph [
  maxDegree 28
  minDegree 1
  meanDegree 2.017391304347826
  density 0.01769641495041953
  graphCliqueNumber 2
  node [
    id 0
    label "wprawdzie"
    origin "text"
  ]
  node [
    id 1
    label "klub"
    origin "text"
  ]
  node [
    id 2
    label "pi&#322;karski"
    origin "text"
  ]
  node [
    id 3
    label "ostatni"
    origin "text"
  ]
  node [
    id 4
    label "lato"
    origin "text"
  ]
  node [
    id 5
    label "maja"
    origin "text"
  ]
  node [
    id 6
    label "lekko"
    origin "text"
  ]
  node [
    id 7
    label "ale"
    origin "text"
  ]
  node [
    id 8
    label "okr&#261;g"
    origin "text"
  ]
  node [
    id 9
    label "&#322;&#243;dzkie"
    origin "text"
  ]
  node [
    id 10
    label "by&#263;"
    origin "text"
  ]
  node [
    id 11
    label "tragicznie"
    origin "text"
  ]
  node [
    id 12
    label "ubieg&#322;y"
    origin "text"
  ]
  node [
    id 13
    label "rok"
    origin "text"
  ]
  node [
    id 14
    label "zaledwie"
    origin "text"
  ]
  node [
    id 15
    label "trzy"
    origin "text"
  ]
  node [
    id 16
    label "zbankrutowa&#263;"
    origin "text"
  ]
  node [
    id 17
    label "society"
  ]
  node [
    id 18
    label "jakobini"
  ]
  node [
    id 19
    label "klubista"
  ]
  node [
    id 20
    label "stowarzyszenie"
  ]
  node [
    id 21
    label "lokal"
  ]
  node [
    id 22
    label "od&#322;am"
  ]
  node [
    id 23
    label "siedziba"
  ]
  node [
    id 24
    label "bar"
  ]
  node [
    id 25
    label "specjalny"
  ]
  node [
    id 26
    label "sportowy"
  ]
  node [
    id 27
    label "po_pi&#322;karsku"
  ]
  node [
    id 28
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 29
    label "typowy"
  ]
  node [
    id 30
    label "pi&#322;karsko"
  ]
  node [
    id 31
    label "cz&#322;owiek"
  ]
  node [
    id 32
    label "kolejny"
  ]
  node [
    id 33
    label "istota_&#380;ywa"
  ]
  node [
    id 34
    label "najgorszy"
  ]
  node [
    id 35
    label "aktualny"
  ]
  node [
    id 36
    label "ostatnio"
  ]
  node [
    id 37
    label "niedawno"
  ]
  node [
    id 38
    label "wi&#261;&#380;&#261;cy"
  ]
  node [
    id 39
    label "sko&#324;czony"
  ]
  node [
    id 40
    label "poprzedni"
  ]
  node [
    id 41
    label "pozosta&#322;y"
  ]
  node [
    id 42
    label "w&#261;tpliwy"
  ]
  node [
    id 43
    label "pora_roku"
  ]
  node [
    id 44
    label "wedyzm"
  ]
  node [
    id 45
    label "energia"
  ]
  node [
    id 46
    label "buddyzm"
  ]
  node [
    id 47
    label "mi&#281;kko"
  ]
  node [
    id 48
    label "nieznaczny"
  ]
  node [
    id 49
    label "p&#322;ynnie"
  ]
  node [
    id 50
    label "mi&#281;ciuchno"
  ]
  node [
    id 51
    label "&#322;atwie"
  ]
  node [
    id 52
    label "prosto"
  ]
  node [
    id 53
    label "lekki"
  ]
  node [
    id 54
    label "pewnie"
  ]
  node [
    id 55
    label "delikatny"
  ]
  node [
    id 56
    label "bezpiecznie"
  ]
  node [
    id 57
    label "dietetycznie"
  ]
  node [
    id 58
    label "snadnie"
  ]
  node [
    id 59
    label "&#322;atwo"
  ]
  node [
    id 60
    label "zwinny"
  ]
  node [
    id 61
    label "&#322;atwy"
  ]
  node [
    id 62
    label "sprawnie"
  ]
  node [
    id 63
    label "polotnie"
  ]
  node [
    id 64
    label "nieg&#322;&#281;boki"
  ]
  node [
    id 65
    label "&#322;acno"
  ]
  node [
    id 66
    label "beztroski"
  ]
  node [
    id 67
    label "g&#322;adki"
  ]
  node [
    id 68
    label "mo&#380;liwie"
  ]
  node [
    id 69
    label "s&#322;abo"
  ]
  node [
    id 70
    label "przyjemnie"
  ]
  node [
    id 71
    label "delikatnie"
  ]
  node [
    id 72
    label "cienko"
  ]
  node [
    id 73
    label "piwo"
  ]
  node [
    id 74
    label "&#322;uk"
  ]
  node [
    id 75
    label "circumference"
  ]
  node [
    id 76
    label "figura_p&#322;aska"
  ]
  node [
    id 77
    label "circle"
  ]
  node [
    id 78
    label "figura_geometryczna"
  ]
  node [
    id 79
    label "ko&#322;o"
  ]
  node [
    id 80
    label "si&#281;ga&#263;"
  ]
  node [
    id 81
    label "trwa&#263;"
  ]
  node [
    id 82
    label "obecno&#347;&#263;"
  ]
  node [
    id 83
    label "stan"
  ]
  node [
    id 84
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 85
    label "stand"
  ]
  node [
    id 86
    label "mie&#263;_miejsce"
  ]
  node [
    id 87
    label "uczestniczy&#263;"
  ]
  node [
    id 88
    label "chodzi&#263;"
  ]
  node [
    id 89
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 90
    label "equal"
  ]
  node [
    id 91
    label "&#347;miertelnie"
  ]
  node [
    id 92
    label "dramatycznie"
  ]
  node [
    id 93
    label "masakryczny"
  ]
  node [
    id 94
    label "strasznie"
  ]
  node [
    id 95
    label "pora&#380;aj&#261;co"
  ]
  node [
    id 96
    label "tragiczny"
  ]
  node [
    id 97
    label "koszmarnie"
  ]
  node [
    id 98
    label "stulecie"
  ]
  node [
    id 99
    label "kalendarz"
  ]
  node [
    id 100
    label "czas"
  ]
  node [
    id 101
    label "cykl_astronomiczny"
  ]
  node [
    id 102
    label "p&#243;&#322;rocze"
  ]
  node [
    id 103
    label "grupa"
  ]
  node [
    id 104
    label "kwarta&#322;"
  ]
  node [
    id 105
    label "kurs"
  ]
  node [
    id 106
    label "jubileusz"
  ]
  node [
    id 107
    label "miesi&#261;c"
  ]
  node [
    id 108
    label "lata"
  ]
  node [
    id 109
    label "martwy_sezon"
  ]
  node [
    id 110
    label "fail"
  ]
  node [
    id 111
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 112
    label "ruin"
  ]
  node [
    id 113
    label "fall"
  ]
  node [
    id 114
    label "pa&#347;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 11
    target 92
  ]
  edge [
    source 11
    target 93
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 98
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 13
    target 43
  ]
  edge [
    source 13
    target 101
  ]
  edge [
    source 13
    target 102
  ]
  edge [
    source 13
    target 103
  ]
  edge [
    source 13
    target 104
  ]
  edge [
    source 13
    target 105
  ]
  edge [
    source 13
    target 106
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 110
  ]
  edge [
    source 16
    target 111
  ]
  edge [
    source 16
    target 112
  ]
  edge [
    source 16
    target 113
  ]
  edge [
    source 16
    target 114
  ]
]
