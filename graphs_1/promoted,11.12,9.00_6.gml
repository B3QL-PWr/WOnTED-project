graph [
  maxDegree 33
  minDegree 1
  meanDegree 2.033333333333333
  density 0.03446327683615819
  graphCliqueNumber 3
  node [
    id 0
    label "propagandowy"
    origin "text"
  ]
  node [
    id 1
    label "film"
    origin "text"
  ]
  node [
    id 2
    label "zmanipulowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "serial"
    origin "text"
  ]
  node [
    id 4
    label "ksi&#261;&#380;kowy"
    origin "text"
  ]
  node [
    id 5
    label "pean"
    origin "text"
  ]
  node [
    id 6
    label "banalny"
  ]
  node [
    id 7
    label "propagandowo"
  ]
  node [
    id 8
    label "sloganowo"
  ]
  node [
    id 9
    label "rozbieg&#243;wka"
  ]
  node [
    id 10
    label "block"
  ]
  node [
    id 11
    label "blik"
  ]
  node [
    id 12
    label "odczula&#263;"
  ]
  node [
    id 13
    label "rola"
  ]
  node [
    id 14
    label "trawiarnia"
  ]
  node [
    id 15
    label "b&#322;ona"
  ]
  node [
    id 16
    label "filmoteka"
  ]
  node [
    id 17
    label "sztuka"
  ]
  node [
    id 18
    label "muza"
  ]
  node [
    id 19
    label "odczuli&#263;"
  ]
  node [
    id 20
    label "klatka"
  ]
  node [
    id 21
    label "odczulenie"
  ]
  node [
    id 22
    label "emulsja_fotograficzna"
  ]
  node [
    id 23
    label "animatronika"
  ]
  node [
    id 24
    label "dorobek"
  ]
  node [
    id 25
    label "odczulanie"
  ]
  node [
    id 26
    label "scena"
  ]
  node [
    id 27
    label "czo&#322;&#243;wka"
  ]
  node [
    id 28
    label "ty&#322;&#243;wka"
  ]
  node [
    id 29
    label "napisy"
  ]
  node [
    id 30
    label "photograph"
  ]
  node [
    id 31
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 32
    label "postprodukcja"
  ]
  node [
    id 33
    label "sklejarka"
  ]
  node [
    id 34
    label "anamorfoza"
  ]
  node [
    id 35
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 36
    label "ta&#347;ma"
  ]
  node [
    id 37
    label "b&#322;ona_&#347;wiat&#322;oczu&#322;a"
  ]
  node [
    id 38
    label "uj&#281;cie"
  ]
  node [
    id 39
    label "zmodyfikowa&#263;"
  ]
  node [
    id 40
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 41
    label "wykona&#263;"
  ]
  node [
    id 42
    label "zdeformowa&#263;"
  ]
  node [
    id 43
    label "seria"
  ]
  node [
    id 44
    label "Klan"
  ]
  node [
    id 45
    label "Ranczo"
  ]
  node [
    id 46
    label "program_telewizyjny"
  ]
  node [
    id 47
    label "ksi&#261;&#380;kowo"
  ]
  node [
    id 48
    label "podr&#281;cznikowo"
  ]
  node [
    id 49
    label "klasyczny"
  ]
  node [
    id 50
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 51
    label "wzorowy"
  ]
  node [
    id 52
    label "wzorcowy"
  ]
  node [
    id 53
    label "pochwa&#322;a"
  ]
  node [
    id 54
    label "Apollo"
  ]
  node [
    id 55
    label "grecki"
  ]
  node [
    id 56
    label "encomium"
  ]
  node [
    id 57
    label "starogrecki"
  ]
  node [
    id 58
    label "wiersz"
  ]
  node [
    id 59
    label "pie&#347;&#324;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
]
