graph [
  maxDegree 21
  minDegree 1
  meanDegree 1.9811320754716981
  density 0.018867924528301886
  graphCliqueNumber 2
  node [
    id 0
    label "adam"
    origin "text"
  ]
  node [
    id 1
    label "widzie&#263;"
    origin "text"
  ]
  node [
    id 2
    label "jak"
    origin "text"
  ]
  node [
    id 3
    label "siedzie&#263;"
    origin "text"
  ]
  node [
    id 4
    label "markotny"
    origin "text"
  ]
  node [
    id 5
    label "krzes&#322;o"
    origin "text"
  ]
  node [
    id 6
    label "strzela&#263;"
    origin "text"
  ]
  node [
    id 7
    label "palce"
    origin "text"
  ]
  node [
    id 8
    label "podchodzi&#263;"
    origin "text"
  ]
  node [
    id 9
    label "g&#322;ad&#378;"
    origin "text"
  ]
  node [
    id 10
    label "policzek"
    origin "text"
  ]
  node [
    id 11
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 12
    label "perceive"
  ]
  node [
    id 13
    label "reagowa&#263;"
  ]
  node [
    id 14
    label "spowodowa&#263;"
  ]
  node [
    id 15
    label "male&#263;"
  ]
  node [
    id 16
    label "zmale&#263;"
  ]
  node [
    id 17
    label "spotka&#263;"
  ]
  node [
    id 18
    label "go_steady"
  ]
  node [
    id 19
    label "dostrzega&#263;"
  ]
  node [
    id 20
    label "zwi&#281;ksza&#263;_si&#281;"
  ]
  node [
    id 21
    label "zdawa&#263;_sobie_spraw&#281;"
  ]
  node [
    id 22
    label "ogl&#261;da&#263;"
  ]
  node [
    id 23
    label "os&#261;dza&#263;"
  ]
  node [
    id 24
    label "aprobowa&#263;"
  ]
  node [
    id 25
    label "punkt_widzenia"
  ]
  node [
    id 26
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 27
    label "wzrok"
  ]
  node [
    id 28
    label "postrzega&#263;"
  ]
  node [
    id 29
    label "notice"
  ]
  node [
    id 30
    label "byd&#322;o"
  ]
  node [
    id 31
    label "zobo"
  ]
  node [
    id 32
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 33
    label "yakalo"
  ]
  node [
    id 34
    label "dzo"
  ]
  node [
    id 35
    label "trwa&#263;"
  ]
  node [
    id 36
    label "sit"
  ]
  node [
    id 37
    label "pause"
  ]
  node [
    id 38
    label "ptak"
  ]
  node [
    id 39
    label "garowa&#263;"
  ]
  node [
    id 40
    label "mieszka&#263;"
  ]
  node [
    id 41
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 42
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 43
    label "przebywa&#263;"
  ]
  node [
    id 44
    label "brood"
  ]
  node [
    id 45
    label "zwierz&#281;"
  ]
  node [
    id 46
    label "doprowadza&#263;"
  ]
  node [
    id 47
    label "spoczywa&#263;"
  ]
  node [
    id 48
    label "tkwi&#263;"
  ]
  node [
    id 49
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 50
    label "smutny"
  ]
  node [
    id 51
    label "markotnie"
  ]
  node [
    id 52
    label "oparcie"
  ]
  node [
    id 53
    label "zaplecek"
  ]
  node [
    id 54
    label "siedzenie"
  ]
  node [
    id 55
    label "siedzisko"
  ]
  node [
    id 56
    label "&#322;awka"
  ]
  node [
    id 57
    label "krzes&#322;o_elektryczne"
  ]
  node [
    id 58
    label "por&#281;cz"
  ]
  node [
    id 59
    label "mebel"
  ]
  node [
    id 60
    label "noga"
  ]
  node [
    id 61
    label "kierowa&#263;"
  ]
  node [
    id 62
    label "napierdziela&#263;"
  ]
  node [
    id 63
    label "walczy&#263;"
  ]
  node [
    id 64
    label "fight"
  ]
  node [
    id 65
    label "train"
  ]
  node [
    id 66
    label "robi&#263;"
  ]
  node [
    id 67
    label "plu&#263;"
  ]
  node [
    id 68
    label "uderza&#263;"
  ]
  node [
    id 69
    label "odpala&#263;"
  ]
  node [
    id 70
    label "snap"
  ]
  node [
    id 71
    label "stopa"
  ]
  node [
    id 72
    label "przyst&#281;powa&#263;"
  ]
  node [
    id 73
    label "przygotowywa&#263;_si&#281;"
  ]
  node [
    id 74
    label "approach"
  ]
  node [
    id 75
    label "sprawdzian"
  ]
  node [
    id 76
    label "set_about"
  ]
  node [
    id 77
    label "traktowa&#263;"
  ]
  node [
    id 78
    label "zalicza&#263;_si&#281;"
  ]
  node [
    id 79
    label "zmienia&#263;_pozycj&#281;"
  ]
  node [
    id 80
    label "wspina&#263;_si&#281;"
  ]
  node [
    id 81
    label "nasyca&#263;_si&#281;"
  ]
  node [
    id 82
    label "wype&#322;nia&#263;"
  ]
  node [
    id 83
    label "dochodzi&#263;"
  ]
  node [
    id 84
    label "dociera&#263;"
  ]
  node [
    id 85
    label "oszukiwa&#263;"
  ]
  node [
    id 86
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 87
    label "odpowiada&#263;"
  ]
  node [
    id 88
    label "ciecz"
  ]
  node [
    id 89
    label "warstwa"
  ]
  node [
    id 90
    label "degree"
  ]
  node [
    id 91
    label "kowad&#322;o"
  ]
  node [
    id 92
    label "ukszta&#322;towanie"
  ]
  node [
    id 93
    label "twarz"
  ]
  node [
    id 94
    label "krzywda"
  ]
  node [
    id 95
    label "uderzenie"
  ]
  node [
    id 96
    label "wyzwisko"
  ]
  node [
    id 97
    label "do&#322;eczek"
  ]
  node [
    id 98
    label "wrzuta"
  ]
  node [
    id 99
    label "ubliga"
  ]
  node [
    id 100
    label "nieuprzejmo&#347;&#263;"
  ]
  node [
    id 101
    label "tekst"
  ]
  node [
    id 102
    label "niedorobek"
  ]
  node [
    id 103
    label "indignation"
  ]
  node [
    id 104
    label "lico"
  ]
  node [
    id 105
    label "element_anatomiczny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 10
    target 105
  ]
]
