graph [
  maxDegree 56
  minDegree 1
  meanDegree 2.267515923566879
  density 0.0036164528286553093
  graphCliqueNumber 4
  node [
    id 0
    label "poniedzia&#322;kowy"
    origin "text"
  ]
  node [
    id 1
    label "program"
    origin "text"
  ]
  node [
    id 2
    label "ksi&#261;&#380;ek"
    origin "text"
  ]
  node [
    id 3
    label "telewizja"
    origin "text"
  ]
  node [
    id 4
    label "publiczny"
    origin "text"
  ]
  node [
    id 5
    label "poinformowa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "metr"
    origin "text"
  ]
  node [
    id 7
    label "uroczysto&#347;&#263;"
    origin "text"
  ]
  node [
    id 8
    label "wr&#281;czy&#263;"
    origin "text"
  ]
  node [
    id 9
    label "nagroda"
    origin "text"
  ]
  node [
    id 10
    label "nike"
    origin "text"
  ]
  node [
    id 11
    label "werdykt"
    origin "text"
  ]
  node [
    id 12
    label "uzasadnia&#263;"
    origin "text"
  ]
  node [
    id 13
    label "cz&#322;onek"
    origin "text"
  ]
  node [
    id 14
    label "jura"
    origin "text"
  ]
  node [
    id 15
    label "bereza"
    origin "text"
  ]
  node [
    id 16
    label "cywi&#324;ska"
    origin "text"
  ]
  node [
    id 17
    label "druga"
    origin "text"
  ]
  node [
    id 18
    label "strona"
    origin "text"
  ]
  node [
    id 19
    label "autor"
    origin "text"
  ]
  node [
    id 20
    label "zaproszony"
    origin "text"
  ]
  node [
    id 21
    label "krytyk"
    origin "text"
  ]
  node [
    id 22
    label "pierwsza"
    origin "text"
  ]
  node [
    id 23
    label "dorota"
    origin "text"
  ]
  node [
    id 24
    label "nie"
    origin "text"
  ]
  node [
    id 25
    label "by&#263;"
    origin "text"
  ]
  node [
    id 26
    label "literacki"
    origin "text"
  ]
  node [
    id 27
    label "oraz"
    origin "text"
  ]
  node [
    id 28
    label "dlaczego"
    origin "text"
  ]
  node [
    id 29
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 30
    label "literatura"
    origin "text"
  ]
  node [
    id 31
    label "p&#243;&#378;no"
    origin "text"
  ]
  node [
    id 32
    label "salman"
    origin "text"
  ]
  node [
    id 33
    label "rushdie"
    origin "text"
  ]
  node [
    id 34
    label "nagra&#263;"
    origin "text"
  ]
  node [
    id 35
    label "&#322;azienki"
    origin "text"
  ]
  node [
    id 36
    label "kr&#243;lewski"
    origin "text"
  ]
  node [
    id 37
    label "pisarz"
    origin "text"
  ]
  node [
    id 38
    label "mieszka&#263;"
    origin "text"
  ]
  node [
    id 39
    label "sta&#322;a"
    origin "text"
  ]
  node [
    id 40
    label "nowa"
    origin "text"
  ]
  node [
    id 41
    label "jork"
    origin "text"
  ]
  node [
    id 42
    label "znale&#378;&#263;"
    origin "text"
  ]
  node [
    id 43
    label "si&#281;"
    origin "text"
  ]
  node [
    id 44
    label "nagle"
    origin "text"
  ]
  node [
    id 45
    label "akurat"
    origin "text"
  ]
  node [
    id 46
    label "warszawa"
    origin "text"
  ]
  node [
    id 47
    label "spos&#243;b"
    origin "text"
  ]
  node [
    id 48
    label "nawet"
    origin "text"
  ]
  node [
    id 49
    label "domy&#347;li&#263;"
    origin "text"
  ]
  node [
    id 50
    label "chyba"
    origin "text"
  ]
  node [
    id 51
    label "przyjecha&#263;"
    origin "text"
  ]
  node [
    id 52
    label "zreszt&#261;"
    origin "text"
  ]
  node [
    id 53
    label "kto"
    origin "text"
  ]
  node [
    id 54
    label "przyznawa&#263;"
    origin "text"
  ]
  node [
    id 55
    label "ale"
    origin "text"
  ]
  node [
    id 56
    label "ciekawy"
    origin "text"
  ]
  node [
    id 57
    label "rzecz"
    origin "text"
  ]
  node [
    id 58
    label "dzia&#322;"
    origin "text"
  ]
  node [
    id 59
    label "dopiero"
    origin "text"
  ]
  node [
    id 60
    label "fina&#322;"
    origin "text"
  ]
  node [
    id 61
    label "podczas"
    origin "text"
  ]
  node [
    id 62
    label "dyskusja"
    origin "text"
  ]
  node [
    id 63
    label "tom"
    origin "text"
  ]
  node [
    id 64
    label "czes&#322;aw"
    origin "text"
  ]
  node [
    id 65
    label "mi&#322;osz"
    origin "text"
  ]
  node [
    id 66
    label "ostatnie"
    origin "text"
  ]
  node [
    id 67
    label "kiedy"
    origin "text"
  ]
  node [
    id 68
    label "prowadz&#261;ca"
    origin "text"
  ]
  node [
    id 69
    label "audycja"
    origin "text"
  ]
  node [
    id 70
    label "magdalena"
    origin "text"
  ]
  node [
    id 71
    label "miecznicka"
    origin "text"
  ]
  node [
    id 72
    label "stwierdzi&#263;"
    origin "text"
  ]
  node [
    id 73
    label "zachwyci&#263;"
    origin "text"
  ]
  node [
    id 74
    label "sam"
    origin "text"
  ]
  node [
    id 75
    label "siebie"
    origin "text"
  ]
  node [
    id 76
    label "zdziwi&#263;"
    origin "text"
  ]
  node [
    id 77
    label "andrzej"
    origin "text"
  ]
  node [
    id 78
    label "franaszek"
    origin "text"
  ]
  node [
    id 79
    label "biograf"
    origin "text"
  ]
  node [
    id 80
    label "edytor"
    origin "text"
  ]
  node [
    id 81
    label "dzie&#322;o"
    origin "text"
  ]
  node [
    id 82
    label "poeta"
    origin "text"
  ]
  node [
    id 83
    label "potwierdzenie"
    origin "text"
  ]
  node [
    id 84
    label "ten"
    origin "text"
  ]
  node [
    id 85
    label "przyzna&#263;"
    origin "text"
  ]
  node [
    id 86
    label "&#347;mia&#322;y"
    origin "text"
  ]
  node [
    id 87
    label "teza"
    origin "text"
  ]
  node [
    id 88
    label "zacytowa&#263;"
    origin "text"
  ]
  node [
    id 89
    label "fragment"
    origin "text"
  ]
  node [
    id 90
    label "jeden"
    origin "text"
  ]
  node [
    id 91
    label "wiersz"
    origin "text"
  ]
  node [
    id 92
    label "cytowa&#263;"
    origin "text"
  ]
  node [
    id 93
    label "kr&#243;tko"
    origin "text"
  ]
  node [
    id 94
    label "par"
    origin "text"
  ]
  node [
    id 95
    label "strof"
    origin "text"
  ]
  node [
    id 96
    label "zawyrokowa&#263;"
    origin "text"
  ]
  node [
    id 97
    label "prosty"
    origin "text"
  ]
  node [
    id 98
    label "ma&#322;o"
    origin "text"
  ]
  node [
    id 99
    label "du&#380;o"
    origin "text"
  ]
  node [
    id 100
    label "daleko"
    origin "text"
  ]
  node [
    id 101
    label "d&#322;u&#380;yzna"
    origin "text"
  ]
  node [
    id 102
    label "tylko"
    origin "text"
  ]
  node [
    id 103
    label "g&#322;&#281;boko"
    origin "text"
  ]
  node [
    id 104
    label "westchn&#261;&#263;"
    origin "text"
  ]
  node [
    id 105
    label "wznie&#347;&#263;"
    origin "text"
  ]
  node [
    id 106
    label "oko"
    origin "text"
  ]
  node [
    id 107
    label "niebo"
    origin "text"
  ]
  node [
    id 108
    label "spis"
  ]
  node [
    id 109
    label "odinstalowanie"
  ]
  node [
    id 110
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 111
    label "za&#322;o&#380;enie"
  ]
  node [
    id 112
    label "podstawa"
  ]
  node [
    id 113
    label "emitowanie"
  ]
  node [
    id 114
    label "odinstalowywanie"
  ]
  node [
    id 115
    label "instrukcja"
  ]
  node [
    id 116
    label "punkt"
  ]
  node [
    id 117
    label "teleferie"
  ]
  node [
    id 118
    label "emitowa&#263;"
  ]
  node [
    id 119
    label "wytw&#243;r"
  ]
  node [
    id 120
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 121
    label "sekcja_krytyczna"
  ]
  node [
    id 122
    label "oferta"
  ]
  node [
    id 123
    label "prezentowa&#263;"
  ]
  node [
    id 124
    label "blok"
  ]
  node [
    id 125
    label "podprogram"
  ]
  node [
    id 126
    label "tryb"
  ]
  node [
    id 127
    label "broszura"
  ]
  node [
    id 128
    label "deklaracja"
  ]
  node [
    id 129
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 130
    label "struktura_organizacyjna"
  ]
  node [
    id 131
    label "zaprezentowanie"
  ]
  node [
    id 132
    label "informatyka"
  ]
  node [
    id 133
    label "booklet"
  ]
  node [
    id 134
    label "menu"
  ]
  node [
    id 135
    label "oprogramowanie"
  ]
  node [
    id 136
    label "instalowanie"
  ]
  node [
    id 137
    label "furkacja"
  ]
  node [
    id 138
    label "odinstalowa&#263;"
  ]
  node [
    id 139
    label "instalowa&#263;"
  ]
  node [
    id 140
    label "pirat"
  ]
  node [
    id 141
    label "zainstalowanie"
  ]
  node [
    id 142
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 143
    label "ogranicznik_referencyjny"
  ]
  node [
    id 144
    label "zainstalowa&#263;"
  ]
  node [
    id 145
    label "kana&#322;"
  ]
  node [
    id 146
    label "zaprezentowa&#263;"
  ]
  node [
    id 147
    label "interfejs"
  ]
  node [
    id 148
    label "odinstalowywa&#263;"
  ]
  node [
    id 149
    label "folder"
  ]
  node [
    id 150
    label "course_of_study"
  ]
  node [
    id 151
    label "ram&#243;wka"
  ]
  node [
    id 152
    label "prezentowanie"
  ]
  node [
    id 153
    label "okno"
  ]
  node [
    id 154
    label "Polsat"
  ]
  node [
    id 155
    label "paj&#281;czarz"
  ]
  node [
    id 156
    label "gadaj&#261;ca_g&#322;owa"
  ]
  node [
    id 157
    label "programowiec"
  ]
  node [
    id 158
    label "technologia"
  ]
  node [
    id 159
    label "urz&#261;dzenie_RTV"
  ]
  node [
    id 160
    label "Interwizja"
  ]
  node [
    id 161
    label "BBC"
  ]
  node [
    id 162
    label "ekran"
  ]
  node [
    id 163
    label "redakcja"
  ]
  node [
    id 164
    label "media"
  ]
  node [
    id 165
    label "odbieranie"
  ]
  node [
    id 166
    label "odbiera&#263;"
  ]
  node [
    id 167
    label "odbiornik"
  ]
  node [
    id 168
    label "instytucja"
  ]
  node [
    id 169
    label "du&#380;y_pok&#243;j"
  ]
  node [
    id 170
    label "studio"
  ]
  node [
    id 171
    label "telekomunikacja"
  ]
  node [
    id 172
    label "muza"
  ]
  node [
    id 173
    label "jawny"
  ]
  node [
    id 174
    label "upublicznienie"
  ]
  node [
    id 175
    label "upublicznianie"
  ]
  node [
    id 176
    label "publicznie"
  ]
  node [
    id 177
    label "zakomunikowa&#263;"
  ]
  node [
    id 178
    label "inform"
  ]
  node [
    id 179
    label "meter"
  ]
  node [
    id 180
    label "decymetr"
  ]
  node [
    id 181
    label "megabyte"
  ]
  node [
    id 182
    label "plon"
  ]
  node [
    id 183
    label "metrum"
  ]
  node [
    id 184
    label "dekametr"
  ]
  node [
    id 185
    label "jednostka_powierzchni"
  ]
  node [
    id 186
    label "uk&#322;ad_SI"
  ]
  node [
    id 187
    label "literaturoznawstwo"
  ]
  node [
    id 188
    label "gigametr"
  ]
  node [
    id 189
    label "miara"
  ]
  node [
    id 190
    label "nauczyciel"
  ]
  node [
    id 191
    label "kilometr_kwadratowy"
  ]
  node [
    id 192
    label "jednostka_metryczna"
  ]
  node [
    id 193
    label "jednostka_masy"
  ]
  node [
    id 194
    label "centymetr_kwadratowy"
  ]
  node [
    id 195
    label "cecha"
  ]
  node [
    id 196
    label "przysi&#281;ga_wojskowa"
  ]
  node [
    id 197
    label "egzaltacja"
  ]
  node [
    id 198
    label "wydarzenie"
  ]
  node [
    id 199
    label "przyrzeczenie_harcerskie"
  ]
  node [
    id 200
    label "atmosfera"
  ]
  node [
    id 201
    label "patos"
  ]
  node [
    id 202
    label "supply"
  ]
  node [
    id 203
    label "poda&#263;"
  ]
  node [
    id 204
    label "return"
  ]
  node [
    id 205
    label "konsekwencja"
  ]
  node [
    id 206
    label "oskar"
  ]
  node [
    id 207
    label "wyr&#243;&#380;nienie"
  ]
  node [
    id 208
    label "opinion"
  ]
  node [
    id 209
    label "decyzja"
  ]
  node [
    id 210
    label "explain"
  ]
  node [
    id 211
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 212
    label "cz&#322;owiek"
  ]
  node [
    id 213
    label "cia&#322;o"
  ]
  node [
    id 214
    label "organizacja"
  ]
  node [
    id 215
    label "przedstawiciel"
  ]
  node [
    id 216
    label "shaft"
  ]
  node [
    id 217
    label "podmiot"
  ]
  node [
    id 218
    label "fiut"
  ]
  node [
    id 219
    label "przyrodzenie"
  ]
  node [
    id 220
    label "wchodzenie"
  ]
  node [
    id 221
    label "grupa"
  ]
  node [
    id 222
    label "ptaszek"
  ]
  node [
    id 223
    label "organ"
  ]
  node [
    id 224
    label "wej&#347;cie"
  ]
  node [
    id 225
    label "narz&#261;d_kopulacyjny"
  ]
  node [
    id 226
    label "element_anatomiczny"
  ]
  node [
    id 227
    label "formacja_geologiczna"
  ]
  node [
    id 228
    label "jura_&#347;rodkowa"
  ]
  node [
    id 229
    label "dogger"
  ]
  node [
    id 230
    label "jura_p&#243;&#378;na"
  ]
  node [
    id 231
    label "era_mezozoiczna"
  ]
  node [
    id 232
    label "plezjozaur"
  ]
  node [
    id 233
    label "euoplocefal"
  ]
  node [
    id 234
    label "jura_wczesna"
  ]
  node [
    id 235
    label "godzina"
  ]
  node [
    id 236
    label "skr&#281;canie"
  ]
  node [
    id 237
    label "voice"
  ]
  node [
    id 238
    label "forma"
  ]
  node [
    id 239
    label "internet"
  ]
  node [
    id 240
    label "skr&#281;ci&#263;"
  ]
  node [
    id 241
    label "kartka"
  ]
  node [
    id 242
    label "orientowa&#263;"
  ]
  node [
    id 243
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 244
    label "powierzchnia"
  ]
  node [
    id 245
    label "plik"
  ]
  node [
    id 246
    label "bok"
  ]
  node [
    id 247
    label "pagina"
  ]
  node [
    id 248
    label "orientowanie"
  ]
  node [
    id 249
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 250
    label "s&#261;d"
  ]
  node [
    id 251
    label "skr&#281;ca&#263;"
  ]
  node [
    id 252
    label "g&#243;ra"
  ]
  node [
    id 253
    label "serwis_internetowy"
  ]
  node [
    id 254
    label "orientacja"
  ]
  node [
    id 255
    label "linia"
  ]
  node [
    id 256
    label "skr&#281;cenie"
  ]
  node [
    id 257
    label "layout"
  ]
  node [
    id 258
    label "zorientowa&#263;"
  ]
  node [
    id 259
    label "zorientowanie"
  ]
  node [
    id 260
    label "obiekt"
  ]
  node [
    id 261
    label "ty&#322;"
  ]
  node [
    id 262
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 263
    label "logowanie"
  ]
  node [
    id 264
    label "adres_internetowy"
  ]
  node [
    id 265
    label "uj&#281;cie"
  ]
  node [
    id 266
    label "prz&#243;d"
  ]
  node [
    id 267
    label "posta&#263;"
  ]
  node [
    id 268
    label "pomys&#322;odawca"
  ]
  node [
    id 269
    label "kszta&#322;ciciel"
  ]
  node [
    id 270
    label "tworzyciel"
  ]
  node [
    id 271
    label "&#347;w"
  ]
  node [
    id 272
    label "wykonawca"
  ]
  node [
    id 273
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 274
    label "uczestnik"
  ]
  node [
    id 275
    label "przeciwnik"
  ]
  node [
    id 276
    label "publicysta"
  ]
  node [
    id 277
    label "krytyka"
  ]
  node [
    id 278
    label "sprzeciw"
  ]
  node [
    id 279
    label "si&#281;ga&#263;"
  ]
  node [
    id 280
    label "trwa&#263;"
  ]
  node [
    id 281
    label "obecno&#347;&#263;"
  ]
  node [
    id 282
    label "stan"
  ]
  node [
    id 283
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 284
    label "stand"
  ]
  node [
    id 285
    label "mie&#263;_miejsce"
  ]
  node [
    id 286
    label "uczestniczy&#263;"
  ]
  node [
    id 287
    label "chodzi&#263;"
  ]
  node [
    id 288
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 289
    label "equal"
  ]
  node [
    id 290
    label "artystyczny"
  ]
  node [
    id 291
    label "pi&#281;kny"
  ]
  node [
    id 292
    label "dba&#322;y"
  ]
  node [
    id 293
    label "literacko"
  ]
  node [
    id 294
    label "po_literacku"
  ]
  node [
    id 295
    label "wysoki"
  ]
  node [
    id 296
    label "czyj&#347;"
  ]
  node [
    id 297
    label "m&#261;&#380;"
  ]
  node [
    id 298
    label "dokument"
  ]
  node [
    id 299
    label "amorfizm"
  ]
  node [
    id 300
    label "liryka"
  ]
  node [
    id 301
    label "bibliografia"
  ]
  node [
    id 302
    label "translator"
  ]
  node [
    id 303
    label "dramat"
  ]
  node [
    id 304
    label "pi&#347;miennictwo"
  ]
  node [
    id 305
    label "zoologia_fantastyczna"
  ]
  node [
    id 306
    label "pisarstwo"
  ]
  node [
    id 307
    label "epika"
  ]
  node [
    id 308
    label "sztuka"
  ]
  node [
    id 309
    label "p&#243;&#378;ny"
  ]
  node [
    id 310
    label "utrwali&#263;"
  ]
  node [
    id 311
    label "popisowy"
  ]
  node [
    id 312
    label "po_kr&#243;lewsku"
  ]
  node [
    id 313
    label "kr&#243;lewsko"
  ]
  node [
    id 314
    label "majestatyczny"
  ]
  node [
    id 315
    label "mistrzowski"
  ]
  node [
    id 316
    label "wspania&#322;y"
  ]
  node [
    id 317
    label "wynios&#322;y"
  ]
  node [
    id 318
    label "Lem"
  ]
  node [
    id 319
    label "Stendhal"
  ]
  node [
    id 320
    label "Sienkiewicz"
  ]
  node [
    id 321
    label "Katon"
  ]
  node [
    id 322
    label "surogator"
  ]
  node [
    id 323
    label "Andersen"
  ]
  node [
    id 324
    label "Proust"
  ]
  node [
    id 325
    label "Iwaszkiewicz"
  ]
  node [
    id 326
    label "pisarczyk"
  ]
  node [
    id 327
    label "Walter_Scott"
  ]
  node [
    id 328
    label "Tolkien"
  ]
  node [
    id 329
    label "Conrad"
  ]
  node [
    id 330
    label "Juliusz_Cezar"
  ]
  node [
    id 331
    label "To&#322;stoj"
  ]
  node [
    id 332
    label "Bergson"
  ]
  node [
    id 333
    label "Brecht"
  ]
  node [
    id 334
    label "Flaubert"
  ]
  node [
    id 335
    label "Thomas_Mann"
  ]
  node [
    id 336
    label "Orwell"
  ]
  node [
    id 337
    label "Gogol"
  ]
  node [
    id 338
    label "Kafka"
  ]
  node [
    id 339
    label "artysta"
  ]
  node [
    id 340
    label "Machiavelli"
  ]
  node [
    id 341
    label "Reymont"
  ]
  node [
    id 342
    label "urz&#281;dnik"
  ]
  node [
    id 343
    label "Voltaire"
  ]
  node [
    id 344
    label "Boy-&#379;ele&#324;ski"
  ]
  node [
    id 345
    label "Balzak"
  ]
  node [
    id 346
    label "Gombrowicz"
  ]
  node [
    id 347
    label "zajmowa&#263;"
  ]
  node [
    id 348
    label "sta&#263;"
  ]
  node [
    id 349
    label "przebywa&#263;"
  ]
  node [
    id 350
    label "room"
  ]
  node [
    id 351
    label "panowa&#263;"
  ]
  node [
    id 352
    label "fall"
  ]
  node [
    id 353
    label "wielko&#347;&#263;"
  ]
  node [
    id 354
    label "element"
  ]
  node [
    id 355
    label "constant"
  ]
  node [
    id 356
    label "gwiazda"
  ]
  node [
    id 357
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 358
    label "pies_do_towarzystwa"
  ]
  node [
    id 359
    label "terier"
  ]
  node [
    id 360
    label "pies_pokojowy"
  ]
  node [
    id 361
    label "pies_miniaturowy"
  ]
  node [
    id 362
    label "pies_ozdobny"
  ]
  node [
    id 363
    label "odzyska&#263;"
  ]
  node [
    id 364
    label "devise"
  ]
  node [
    id 365
    label "oceni&#263;"
  ]
  node [
    id 366
    label "znaj&#347;&#263;"
  ]
  node [
    id 367
    label "wymy&#347;li&#263;"
  ]
  node [
    id 368
    label "invent"
  ]
  node [
    id 369
    label "pozyska&#263;"
  ]
  node [
    id 370
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 371
    label "wykry&#263;"
  ]
  node [
    id 372
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 373
    label "dozna&#263;"
  ]
  node [
    id 374
    label "szybko"
  ]
  node [
    id 375
    label "raptowny"
  ]
  node [
    id 376
    label "nieprzewidzianie"
  ]
  node [
    id 377
    label "odpowiedni"
  ]
  node [
    id 378
    label "dok&#322;adnie"
  ]
  node [
    id 379
    label "dobry"
  ]
  node [
    id 380
    label "akuratny"
  ]
  node [
    id 381
    label "Warszawa"
  ]
  node [
    id 382
    label "samoch&#243;d"
  ]
  node [
    id 383
    label "fastback"
  ]
  node [
    id 384
    label "model"
  ]
  node [
    id 385
    label "zbi&#243;r"
  ]
  node [
    id 386
    label "narz&#281;dzie"
  ]
  node [
    id 387
    label "nature"
  ]
  node [
    id 388
    label "get"
  ]
  node [
    id 389
    label "dotrze&#263;"
  ]
  node [
    id 390
    label "stawi&#263;_si&#281;"
  ]
  node [
    id 391
    label "dawa&#263;"
  ]
  node [
    id 392
    label "confer"
  ]
  node [
    id 393
    label "distribute"
  ]
  node [
    id 394
    label "s&#261;dzi&#263;"
  ]
  node [
    id 395
    label "stwierdza&#263;"
  ]
  node [
    id 396
    label "nadawa&#263;"
  ]
  node [
    id 397
    label "piwo"
  ]
  node [
    id 398
    label "swoisty"
  ]
  node [
    id 399
    label "interesowanie"
  ]
  node [
    id 400
    label "nietuzinkowy"
  ]
  node [
    id 401
    label "ciekawie"
  ]
  node [
    id 402
    label "indagator"
  ]
  node [
    id 403
    label "interesuj&#261;cy"
  ]
  node [
    id 404
    label "dziwny"
  ]
  node [
    id 405
    label "intryguj&#261;cy"
  ]
  node [
    id 406
    label "ch&#281;tny"
  ]
  node [
    id 407
    label "temat"
  ]
  node [
    id 408
    label "istota"
  ]
  node [
    id 409
    label "wpa&#347;&#263;"
  ]
  node [
    id 410
    label "wpadanie"
  ]
  node [
    id 411
    label "przedmiot"
  ]
  node [
    id 412
    label "wpada&#263;"
  ]
  node [
    id 413
    label "kultura"
  ]
  node [
    id 414
    label "przyroda"
  ]
  node [
    id 415
    label "mienie"
  ]
  node [
    id 416
    label "object"
  ]
  node [
    id 417
    label "wpadni&#281;cie"
  ]
  node [
    id 418
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 419
    label "zakres"
  ]
  node [
    id 420
    label "whole"
  ]
  node [
    id 421
    label "column"
  ]
  node [
    id 422
    label "distribution"
  ]
  node [
    id 423
    label "bezdro&#380;e"
  ]
  node [
    id 424
    label "competence"
  ]
  node [
    id 425
    label "zesp&#243;&#322;"
  ]
  node [
    id 426
    label "urz&#261;d"
  ]
  node [
    id 427
    label "jednostka_organizacyjna"
  ]
  node [
    id 428
    label "stopie&#324;"
  ]
  node [
    id 429
    label "insourcing"
  ]
  node [
    id 430
    label "sfera"
  ]
  node [
    id 431
    label "miejsce_pracy"
  ]
  node [
    id 432
    label "poddzia&#322;"
  ]
  node [
    id 433
    label "koniec"
  ]
  node [
    id 434
    label "conclusion"
  ]
  node [
    id 435
    label "coating"
  ]
  node [
    id 436
    label "runda"
  ]
  node [
    id 437
    label "rozmowa"
  ]
  node [
    id 438
    label "sympozjon"
  ]
  node [
    id 439
    label "conference"
  ]
  node [
    id 440
    label "b&#281;ben"
  ]
  node [
    id 441
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 442
    label "pi&#281;cioksi&#261;g"
  ]
  node [
    id 443
    label "powiedzie&#263;"
  ]
  node [
    id 444
    label "testify"
  ]
  node [
    id 445
    label "uzna&#263;"
  ]
  node [
    id 446
    label "oznajmi&#263;"
  ]
  node [
    id 447
    label "declare"
  ]
  node [
    id 448
    label "wzbudzi&#263;"
  ]
  node [
    id 449
    label "stimulate"
  ]
  node [
    id 450
    label "sklep"
  ]
  node [
    id 451
    label "podziwi&#263;"
  ]
  node [
    id 452
    label "overwhelm"
  ]
  node [
    id 453
    label "przedsi&#281;biorca"
  ]
  node [
    id 454
    label "tekstolog"
  ]
  node [
    id 455
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 456
    label "redaktor"
  ]
  node [
    id 457
    label "creation"
  ]
  node [
    id 458
    label "retrospektywa"
  ]
  node [
    id 459
    label "tre&#347;&#263;"
  ]
  node [
    id 460
    label "tetralogia"
  ]
  node [
    id 461
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 462
    label "dorobek"
  ]
  node [
    id 463
    label "obrazowanie"
  ]
  node [
    id 464
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 465
    label "komunikat"
  ]
  node [
    id 466
    label "praca"
  ]
  node [
    id 467
    label "works"
  ]
  node [
    id 468
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 469
    label "tekst"
  ]
  node [
    id 470
    label "Zagajewski"
  ]
  node [
    id 471
    label "Osjan"
  ]
  node [
    id 472
    label "Horacy"
  ]
  node [
    id 473
    label "Mi&#322;osz"
  ]
  node [
    id 474
    label "Schiller"
  ]
  node [
    id 475
    label "Rej"
  ]
  node [
    id 476
    label "Byron"
  ]
  node [
    id 477
    label "Jan_Czeczot"
  ]
  node [
    id 478
    label "Peiper"
  ]
  node [
    id 479
    label "Bara&#324;czak"
  ]
  node [
    id 480
    label "Asnyk"
  ]
  node [
    id 481
    label "Bia&#322;oszewski"
  ]
  node [
    id 482
    label "Herbert"
  ]
  node [
    id 483
    label "Dante"
  ]
  node [
    id 484
    label "Puszkin"
  ]
  node [
    id 485
    label "Le&#347;mian"
  ]
  node [
    id 486
    label "R&#243;&#380;ewicz"
  ]
  node [
    id 487
    label "Jesienin"
  ]
  node [
    id 488
    label "wierszopis"
  ]
  node [
    id 489
    label "Tuwim"
  ]
  node [
    id 490
    label "Norwid"
  ]
  node [
    id 491
    label "Homer"
  ]
  node [
    id 492
    label "Pindar"
  ]
  node [
    id 493
    label "Dawid"
  ]
  node [
    id 494
    label "sanction"
  ]
  node [
    id 495
    label "certificate"
  ]
  node [
    id 496
    label "przy&#347;wiadczenie"
  ]
  node [
    id 497
    label "o&#347;wiadczenie"
  ]
  node [
    id 498
    label "stwierdzenie"
  ]
  node [
    id 499
    label "kontrasygnowanie"
  ]
  node [
    id 500
    label "zgodzenie_si&#281;"
  ]
  node [
    id 501
    label "okre&#347;lony"
  ]
  node [
    id 502
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 503
    label "da&#263;"
  ]
  node [
    id 504
    label "nada&#263;"
  ]
  node [
    id 505
    label "pozwoli&#263;"
  ]
  node [
    id 506
    label "give"
  ]
  node [
    id 507
    label "&#347;mia&#322;o"
  ]
  node [
    id 508
    label "dziarski"
  ]
  node [
    id 509
    label "odwa&#380;ny"
  ]
  node [
    id 510
    label "twierdzenie"
  ]
  node [
    id 511
    label "znaczenie"
  ]
  node [
    id 512
    label "argument"
  ]
  node [
    id 513
    label "idea"
  ]
  node [
    id 514
    label "dialektyka"
  ]
  node [
    id 515
    label "poj&#281;cie"
  ]
  node [
    id 516
    label "przywo&#322;a&#263;"
  ]
  node [
    id 517
    label "reference"
  ]
  node [
    id 518
    label "utw&#243;r"
  ]
  node [
    id 519
    label "kieliszek"
  ]
  node [
    id 520
    label "shot"
  ]
  node [
    id 521
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 522
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 523
    label "jaki&#347;"
  ]
  node [
    id 524
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 525
    label "jednolicie"
  ]
  node [
    id 526
    label "w&#243;dka"
  ]
  node [
    id 527
    label "ujednolicenie"
  ]
  node [
    id 528
    label "jednakowy"
  ]
  node [
    id 529
    label "refren"
  ]
  node [
    id 530
    label "wypowied&#378;"
  ]
  node [
    id 531
    label "figura_stylistyczna"
  ]
  node [
    id 532
    label "podmiot_liryczny"
  ]
  node [
    id 533
    label "zwrotka"
  ]
  node [
    id 534
    label "dzie&#322;o_poetyckie"
  ]
  node [
    id 535
    label "cezura"
  ]
  node [
    id 536
    label "strofoida"
  ]
  node [
    id 537
    label "wymienia&#263;"
  ]
  node [
    id 538
    label "quote"
  ]
  node [
    id 539
    label "przytacza&#263;"
  ]
  node [
    id 540
    label "kr&#243;tki"
  ]
  node [
    id 541
    label "Izba_Par&#243;w"
  ]
  node [
    id 542
    label "lord"
  ]
  node [
    id 543
    label "Izba_Lord&#243;w"
  ]
  node [
    id 544
    label "parlamentarzysta"
  ]
  node [
    id 545
    label "lennik"
  ]
  node [
    id 546
    label "decide"
  ]
  node [
    id 547
    label "zdecydowa&#263;"
  ]
  node [
    id 548
    label "&#322;atwy"
  ]
  node [
    id 549
    label "prostowanie_si&#281;"
  ]
  node [
    id 550
    label "rozprostowanie_si&#281;"
  ]
  node [
    id 551
    label "rozprostowanie"
  ]
  node [
    id 552
    label "niez&#322;o&#380;ony"
  ]
  node [
    id 553
    label "prostoduszny"
  ]
  node [
    id 554
    label "naturalny"
  ]
  node [
    id 555
    label "naiwny"
  ]
  node [
    id 556
    label "cios"
  ]
  node [
    id 557
    label "prostowanie"
  ]
  node [
    id 558
    label "niepozorny"
  ]
  node [
    id 559
    label "zwyk&#322;y"
  ]
  node [
    id 560
    label "prosto"
  ]
  node [
    id 561
    label "po_prostu"
  ]
  node [
    id 562
    label "skromny"
  ]
  node [
    id 563
    label "wyprostowanie_si&#281;"
  ]
  node [
    id 564
    label "mo&#380;liwie"
  ]
  node [
    id 565
    label "nieznaczny"
  ]
  node [
    id 566
    label "nieistotnie"
  ]
  node [
    id 567
    label "nieliczny"
  ]
  node [
    id 568
    label "mikroskopijnie"
  ]
  node [
    id 569
    label "pomiernie"
  ]
  node [
    id 570
    label "ma&#322;y"
  ]
  node [
    id 571
    label "du&#380;y"
  ]
  node [
    id 572
    label "cz&#281;sto"
  ]
  node [
    id 573
    label "bardzo"
  ]
  node [
    id 574
    label "mocno"
  ]
  node [
    id 575
    label "wiela"
  ]
  node [
    id 576
    label "dawno"
  ]
  node [
    id 577
    label "nisko"
  ]
  node [
    id 578
    label "nieobecnie"
  ]
  node [
    id 579
    label "daleki"
  ]
  node [
    id 580
    label "het"
  ]
  node [
    id 581
    label "wysoko"
  ]
  node [
    id 582
    label "znacznie"
  ]
  node [
    id 583
    label "scena"
  ]
  node [
    id 584
    label "nudziarstwo"
  ]
  node [
    id 585
    label "silnie"
  ]
  node [
    id 586
    label "g&#322;&#281;boki"
  ]
  node [
    id 587
    label "gruntownie"
  ]
  node [
    id 588
    label "intensywnie"
  ]
  node [
    id 589
    label "szczerze"
  ]
  node [
    id 590
    label "sigh"
  ]
  node [
    id 591
    label "establish"
  ]
  node [
    id 592
    label "pos&#322;a&#263;"
  ]
  node [
    id 593
    label "podnie&#347;&#263;"
  ]
  node [
    id 594
    label "wytworzy&#263;"
  ]
  node [
    id 595
    label "raise"
  ]
  node [
    id 596
    label "budowla"
  ]
  node [
    id 597
    label "siniec"
  ]
  node [
    id 598
    label "uwaga"
  ]
  node [
    id 599
    label "&#380;y&#322;a_&#347;rodkowa_siatk&#243;wki"
  ]
  node [
    id 600
    label "powieka"
  ]
  node [
    id 601
    label "oczy"
  ]
  node [
    id 602
    label "&#347;lepko"
  ]
  node [
    id 603
    label "ga&#322;ka_oczna"
  ]
  node [
    id 604
    label "ser_&#380;&#243;&#322;ty"
  ]
  node [
    id 605
    label "&#347;lepie"
  ]
  node [
    id 606
    label "twarz"
  ]
  node [
    id 607
    label "worek_spoj&#243;wkowy"
  ]
  node [
    id 608
    label "nerw_wzrokowy"
  ]
  node [
    id 609
    label "wzrok"
  ]
  node [
    id 610
    label "&#378;renica"
  ]
  node [
    id 611
    label "spoj&#243;wka"
  ]
  node [
    id 612
    label "narz&#261;d_&#322;zowy"
  ]
  node [
    id 613
    label "nab&#322;onek_barwnikowy"
  ]
  node [
    id 614
    label "kaprawie&#263;"
  ]
  node [
    id 615
    label "kaprawienie"
  ]
  node [
    id 616
    label "spojrzenie"
  ]
  node [
    id 617
    label "net"
  ]
  node [
    id 618
    label "r&#261;bek_rog&#243;wki"
  ]
  node [
    id 619
    label "coloboma"
  ]
  node [
    id 620
    label "ros&#243;&#322;"
  ]
  node [
    id 621
    label "Waruna"
  ]
  node [
    id 622
    label "przestrze&#324;"
  ]
  node [
    id 623
    label "za&#347;wiaty"
  ]
  node [
    id 624
    label "zodiak"
  ]
  node [
    id 625
    label "znak_zodiaku"
  ]
  node [
    id 626
    label "bezchmurno&#347;&#263;"
  ]
  node [
    id 627
    label "Kr&#243;lestwo_Niebieskie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 195
  ]
  edge [
    source 7
    target 196
  ]
  edge [
    source 7
    target 197
  ]
  edge [
    source 7
    target 198
  ]
  edge [
    source 7
    target 199
  ]
  edge [
    source 7
    target 200
  ]
  edge [
    source 7
    target 201
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 202
  ]
  edge [
    source 8
    target 203
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 25
  ]
  edge [
    source 9
    target 26
  ]
  edge [
    source 9
    target 204
  ]
  edge [
    source 9
    target 205
  ]
  edge [
    source 9
    target 206
  ]
  edge [
    source 9
    target 207
  ]
  edge [
    source 9
    target 46
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 51
  ]
  edge [
    source 10
    target 52
  ]
  edge [
    source 10
    target 25
  ]
  edge [
    source 10
    target 53
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 208
  ]
  edge [
    source 11
    target 209
  ]
  edge [
    source 11
    target 30
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 210
  ]
  edge [
    source 12
    target 211
  ]
  edge [
    source 12
    target 67
  ]
  edge [
    source 12
    target 70
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 212
  ]
  edge [
    source 13
    target 213
  ]
  edge [
    source 13
    target 214
  ]
  edge [
    source 13
    target 215
  ]
  edge [
    source 13
    target 216
  ]
  edge [
    source 13
    target 217
  ]
  edge [
    source 13
    target 218
  ]
  edge [
    source 13
    target 219
  ]
  edge [
    source 13
    target 220
  ]
  edge [
    source 13
    target 221
  ]
  edge [
    source 13
    target 222
  ]
  edge [
    source 13
    target 223
  ]
  edge [
    source 13
    target 224
  ]
  edge [
    source 13
    target 225
  ]
  edge [
    source 13
    target 226
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 227
  ]
  edge [
    source 14
    target 228
  ]
  edge [
    source 14
    target 229
  ]
  edge [
    source 14
    target 230
  ]
  edge [
    source 14
    target 231
  ]
  edge [
    source 14
    target 232
  ]
  edge [
    source 14
    target 233
  ]
  edge [
    source 14
    target 234
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 28
  ]
  edge [
    source 15
    target 29
  ]
  edge [
    source 15
    target 60
  ]
  edge [
    source 15
    target 69
  ]
  edge [
    source 15
    target 93
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 27
  ]
  edge [
    source 17
    target 28
  ]
  edge [
    source 17
    target 235
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 236
  ]
  edge [
    source 18
    target 237
  ]
  edge [
    source 18
    target 238
  ]
  edge [
    source 18
    target 239
  ]
  edge [
    source 18
    target 240
  ]
  edge [
    source 18
    target 241
  ]
  edge [
    source 18
    target 242
  ]
  edge [
    source 18
    target 243
  ]
  edge [
    source 18
    target 244
  ]
  edge [
    source 18
    target 245
  ]
  edge [
    source 18
    target 246
  ]
  edge [
    source 18
    target 247
  ]
  edge [
    source 18
    target 248
  ]
  edge [
    source 18
    target 89
  ]
  edge [
    source 18
    target 249
  ]
  edge [
    source 18
    target 250
  ]
  edge [
    source 18
    target 251
  ]
  edge [
    source 18
    target 252
  ]
  edge [
    source 18
    target 253
  ]
  edge [
    source 18
    target 254
  ]
  edge [
    source 18
    target 255
  ]
  edge [
    source 18
    target 256
  ]
  edge [
    source 18
    target 257
  ]
  edge [
    source 18
    target 258
  ]
  edge [
    source 18
    target 259
  ]
  edge [
    source 18
    target 260
  ]
  edge [
    source 18
    target 217
  ]
  edge [
    source 18
    target 261
  ]
  edge [
    source 18
    target 262
  ]
  edge [
    source 18
    target 263
  ]
  edge [
    source 18
    target 264
  ]
  edge [
    source 18
    target 265
  ]
  edge [
    source 18
    target 266
  ]
  edge [
    source 18
    target 267
  ]
  edge [
    source 18
    target 35
  ]
  edge [
    source 19
    target 268
  ]
  edge [
    source 19
    target 269
  ]
  edge [
    source 19
    target 270
  ]
  edge [
    source 19
    target 271
  ]
  edge [
    source 19
    target 272
  ]
  edge [
    source 19
    target 273
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 274
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 275
  ]
  edge [
    source 21
    target 276
  ]
  edge [
    source 21
    target 277
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 235
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 278
  ]
  edge [
    source 25
    target 47
  ]
  edge [
    source 25
    target 43
  ]
  edge [
    source 25
    target 52
  ]
  edge [
    source 25
    target 65
  ]
  edge [
    source 25
    target 73
  ]
  edge [
    source 25
    target 100
  ]
  edge [
    source 25
    target 101
  ]
  edge [
    source 25
    target 279
  ]
  edge [
    source 25
    target 280
  ]
  edge [
    source 25
    target 281
  ]
  edge [
    source 25
    target 282
  ]
  edge [
    source 25
    target 283
  ]
  edge [
    source 25
    target 284
  ]
  edge [
    source 25
    target 285
  ]
  edge [
    source 25
    target 286
  ]
  edge [
    source 25
    target 287
  ]
  edge [
    source 25
    target 288
  ]
  edge [
    source 25
    target 289
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 290
  ]
  edge [
    source 26
    target 291
  ]
  edge [
    source 26
    target 292
  ]
  edge [
    source 26
    target 293
  ]
  edge [
    source 26
    target 294
  ]
  edge [
    source 26
    target 295
  ]
  edge [
    source 26
    target 33
  ]
  edge [
    source 28
    target 36
  ]
  edge [
    source 28
    target 37
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 296
  ]
  edge [
    source 29
    target 297
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 298
  ]
  edge [
    source 30
    target 299
  ]
  edge [
    source 30
    target 300
  ]
  edge [
    source 30
    target 301
  ]
  edge [
    source 30
    target 302
  ]
  edge [
    source 30
    target 303
  ]
  edge [
    source 30
    target 304
  ]
  edge [
    source 30
    target 305
  ]
  edge [
    source 30
    target 306
  ]
  edge [
    source 30
    target 307
  ]
  edge [
    source 30
    target 308
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 309
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 37
  ]
  edge [
    source 33
    target 38
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 310
  ]
  edge [
    source 34
    target 45
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 36
    target 311
  ]
  edge [
    source 36
    target 312
  ]
  edge [
    source 36
    target 313
  ]
  edge [
    source 36
    target 314
  ]
  edge [
    source 36
    target 315
  ]
  edge [
    source 36
    target 316
  ]
  edge [
    source 36
    target 317
  ]
  edge [
    source 37
    target 212
  ]
  edge [
    source 37
    target 318
  ]
  edge [
    source 37
    target 319
  ]
  edge [
    source 37
    target 320
  ]
  edge [
    source 37
    target 321
  ]
  edge [
    source 37
    target 322
  ]
  edge [
    source 37
    target 323
  ]
  edge [
    source 37
    target 324
  ]
  edge [
    source 37
    target 325
  ]
  edge [
    source 37
    target 326
  ]
  edge [
    source 37
    target 327
  ]
  edge [
    source 37
    target 328
  ]
  edge [
    source 37
    target 329
  ]
  edge [
    source 37
    target 330
  ]
  edge [
    source 37
    target 331
  ]
  edge [
    source 37
    target 332
  ]
  edge [
    source 37
    target 333
  ]
  edge [
    source 37
    target 334
  ]
  edge [
    source 37
    target 335
  ]
  edge [
    source 37
    target 336
  ]
  edge [
    source 37
    target 337
  ]
  edge [
    source 37
    target 338
  ]
  edge [
    source 37
    target 339
  ]
  edge [
    source 37
    target 340
  ]
  edge [
    source 37
    target 341
  ]
  edge [
    source 37
    target 342
  ]
  edge [
    source 37
    target 343
  ]
  edge [
    source 37
    target 344
  ]
  edge [
    source 37
    target 345
  ]
  edge [
    source 37
    target 346
  ]
  edge [
    source 37
    target 79
  ]
  edge [
    source 37
    target 82
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 347
  ]
  edge [
    source 38
    target 348
  ]
  edge [
    source 38
    target 349
  ]
  edge [
    source 38
    target 350
  ]
  edge [
    source 38
    target 351
  ]
  edge [
    source 38
    target 352
  ]
  edge [
    source 38
    target 47
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 353
  ]
  edge [
    source 39
    target 129
  ]
  edge [
    source 39
    target 354
  ]
  edge [
    source 39
    target 355
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 356
  ]
  edge [
    source 40
    target 357
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 358
  ]
  edge [
    source 41
    target 359
  ]
  edge [
    source 41
    target 360
  ]
  edge [
    source 41
    target 361
  ]
  edge [
    source 41
    target 362
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 363
  ]
  edge [
    source 42
    target 364
  ]
  edge [
    source 42
    target 365
  ]
  edge [
    source 42
    target 366
  ]
  edge [
    source 42
    target 367
  ]
  edge [
    source 42
    target 368
  ]
  edge [
    source 42
    target 369
  ]
  edge [
    source 42
    target 370
  ]
  edge [
    source 42
    target 371
  ]
  edge [
    source 42
    target 372
  ]
  edge [
    source 42
    target 373
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 48
  ]
  edge [
    source 43
    target 58
  ]
  edge [
    source 43
    target 59
  ]
  edge [
    source 43
    target 76
  ]
  edge [
    source 43
    target 77
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 374
  ]
  edge [
    source 44
    target 375
  ]
  edge [
    source 44
    target 376
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 377
  ]
  edge [
    source 45
    target 378
  ]
  edge [
    source 45
    target 379
  ]
  edge [
    source 45
    target 380
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 381
  ]
  edge [
    source 46
    target 382
  ]
  edge [
    source 46
    target 383
  ]
  edge [
    source 46
    target 107
  ]
  edge [
    source 47
    target 384
  ]
  edge [
    source 47
    target 385
  ]
  edge [
    source 47
    target 126
  ]
  edge [
    source 47
    target 386
  ]
  edge [
    source 47
    target 387
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 51
    target 388
  ]
  edge [
    source 51
    target 389
  ]
  edge [
    source 51
    target 390
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 391
  ]
  edge [
    source 54
    target 392
  ]
  edge [
    source 54
    target 393
  ]
  edge [
    source 54
    target 394
  ]
  edge [
    source 54
    target 395
  ]
  edge [
    source 54
    target 396
  ]
  edge [
    source 54
    target 61
  ]
  edge [
    source 54
    target 76
  ]
  edge [
    source 54
    target 101
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 65
  ]
  edge [
    source 55
    target 92
  ]
  edge [
    source 55
    target 397
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 398
  ]
  edge [
    source 56
    target 212
  ]
  edge [
    source 56
    target 399
  ]
  edge [
    source 56
    target 400
  ]
  edge [
    source 56
    target 401
  ]
  edge [
    source 56
    target 402
  ]
  edge [
    source 56
    target 403
  ]
  edge [
    source 56
    target 404
  ]
  edge [
    source 56
    target 405
  ]
  edge [
    source 56
    target 406
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 260
  ]
  edge [
    source 57
    target 407
  ]
  edge [
    source 57
    target 408
  ]
  edge [
    source 57
    target 409
  ]
  edge [
    source 57
    target 410
  ]
  edge [
    source 57
    target 411
  ]
  edge [
    source 57
    target 412
  ]
  edge [
    source 57
    target 413
  ]
  edge [
    source 57
    target 414
  ]
  edge [
    source 57
    target 415
  ]
  edge [
    source 57
    target 416
  ]
  edge [
    source 57
    target 417
  ]
  edge [
    source 57
    target 106
  ]
  edge [
    source 57
    target 65
  ]
  edge [
    source 58
    target 418
  ]
  edge [
    source 58
    target 419
  ]
  edge [
    source 58
    target 420
  ]
  edge [
    source 58
    target 119
  ]
  edge [
    source 58
    target 421
  ]
  edge [
    source 58
    target 422
  ]
  edge [
    source 58
    target 423
  ]
  edge [
    source 58
    target 424
  ]
  edge [
    source 58
    target 425
  ]
  edge [
    source 58
    target 426
  ]
  edge [
    source 58
    target 427
  ]
  edge [
    source 58
    target 428
  ]
  edge [
    source 58
    target 429
  ]
  edge [
    source 58
    target 262
  ]
  edge [
    source 58
    target 430
  ]
  edge [
    source 58
    target 431
  ]
  edge [
    source 58
    target 432
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 433
  ]
  edge [
    source 60
    target 434
  ]
  edge [
    source 60
    target 435
  ]
  edge [
    source 60
    target 436
  ]
  edge [
    source 60
    target 69
  ]
  edge [
    source 60
    target 93
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 76
  ]
  edge [
    source 61
    target 101
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 437
  ]
  edge [
    source 62
    target 438
  ]
  edge [
    source 62
    target 439
  ]
  edge [
    source 62
    target 64
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 440
  ]
  edge [
    source 63
    target 441
  ]
  edge [
    source 63
    target 442
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 72
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 91
  ]
  edge [
    source 65
    target 82
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 67
    target 70
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 69
    target 93
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 71
    target 82
  ]
  edge [
    source 71
    target 83
  ]
  edge [
    source 71
    target 92
  ]
  edge [
    source 72
    target 443
  ]
  edge [
    source 72
    target 444
  ]
  edge [
    source 72
    target 445
  ]
  edge [
    source 72
    target 446
  ]
  edge [
    source 72
    target 447
  ]
  edge [
    source 72
    target 85
  ]
  edge [
    source 72
    target 96
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 73
    target 448
  ]
  edge [
    source 73
    target 449
  ]
  edge [
    source 74
    target 75
  ]
  edge [
    source 74
    target 450
  ]
  edge [
    source 75
    target 76
  ]
  edge [
    source 76
    target 451
  ]
  edge [
    source 76
    target 448
  ]
  edge [
    source 76
    target 452
  ]
  edge [
    source 76
    target 101
  ]
  edge [
    source 77
    target 78
  ]
  edge [
    source 77
    target 85
  ]
  edge [
    source 78
    target 79
  ]
  edge [
    source 78
    target 101
  ]
  edge [
    source 78
    target 102
  ]
  edge [
    source 79
    target 80
  ]
  edge [
    source 80
    target 81
  ]
  edge [
    source 80
    target 453
  ]
  edge [
    source 80
    target 454
  ]
  edge [
    source 80
    target 455
  ]
  edge [
    source 80
    target 456
  ]
  edge [
    source 81
    target 82
  ]
  edge [
    source 81
    target 457
  ]
  edge [
    source 81
    target 238
  ]
  edge [
    source 81
    target 458
  ]
  edge [
    source 81
    target 459
  ]
  edge [
    source 81
    target 460
  ]
  edge [
    source 81
    target 461
  ]
  edge [
    source 81
    target 462
  ]
  edge [
    source 81
    target 463
  ]
  edge [
    source 81
    target 464
  ]
  edge [
    source 81
    target 465
  ]
  edge [
    source 81
    target 466
  ]
  edge [
    source 81
    target 467
  ]
  edge [
    source 81
    target 468
  ]
  edge [
    source 81
    target 469
  ]
  edge [
    source 82
    target 470
  ]
  edge [
    source 82
    target 471
  ]
  edge [
    source 82
    target 472
  ]
  edge [
    source 82
    target 473
  ]
  edge [
    source 82
    target 474
  ]
  edge [
    source 82
    target 475
  ]
  edge [
    source 82
    target 476
  ]
  edge [
    source 82
    target 477
  ]
  edge [
    source 82
    target 478
  ]
  edge [
    source 82
    target 479
  ]
  edge [
    source 82
    target 480
  ]
  edge [
    source 82
    target 481
  ]
  edge [
    source 82
    target 482
  ]
  edge [
    source 82
    target 483
  ]
  edge [
    source 82
    target 484
  ]
  edge [
    source 82
    target 485
  ]
  edge [
    source 82
    target 486
  ]
  edge [
    source 82
    target 487
  ]
  edge [
    source 82
    target 488
  ]
  edge [
    source 82
    target 489
  ]
  edge [
    source 82
    target 490
  ]
  edge [
    source 82
    target 491
  ]
  edge [
    source 82
    target 492
  ]
  edge [
    source 82
    target 493
  ]
  edge [
    source 83
    target 84
  ]
  edge [
    source 83
    target 298
  ]
  edge [
    source 83
    target 494
  ]
  edge [
    source 83
    target 495
  ]
  edge [
    source 83
    target 496
  ]
  edge [
    source 83
    target 497
  ]
  edge [
    source 83
    target 498
  ]
  edge [
    source 83
    target 499
  ]
  edge [
    source 83
    target 500
  ]
  edge [
    source 84
    target 85
  ]
  edge [
    source 84
    target 501
  ]
  edge [
    source 84
    target 502
  ]
  edge [
    source 84
    target 90
  ]
  edge [
    source 85
    target 86
  ]
  edge [
    source 85
    target 503
  ]
  edge [
    source 85
    target 504
  ]
  edge [
    source 85
    target 505
  ]
  edge [
    source 85
    target 506
  ]
  edge [
    source 86
    target 87
  ]
  edge [
    source 86
    target 507
  ]
  edge [
    source 86
    target 508
  ]
  edge [
    source 86
    target 509
  ]
  edge [
    source 87
    target 88
  ]
  edge [
    source 87
    target 510
  ]
  edge [
    source 87
    target 250
  ]
  edge [
    source 87
    target 511
  ]
  edge [
    source 87
    target 512
  ]
  edge [
    source 87
    target 513
  ]
  edge [
    source 87
    target 514
  ]
  edge [
    source 87
    target 515
  ]
  edge [
    source 88
    target 89
  ]
  edge [
    source 88
    target 516
  ]
  edge [
    source 88
    target 517
  ]
  edge [
    source 89
    target 90
  ]
  edge [
    source 89
    target 262
  ]
  edge [
    source 89
    target 518
  ]
  edge [
    source 89
    target 91
  ]
  edge [
    source 90
    target 91
  ]
  edge [
    source 90
    target 519
  ]
  edge [
    source 90
    target 520
  ]
  edge [
    source 90
    target 521
  ]
  edge [
    source 90
    target 522
  ]
  edge [
    source 90
    target 523
  ]
  edge [
    source 90
    target 524
  ]
  edge [
    source 90
    target 525
  ]
  edge [
    source 90
    target 526
  ]
  edge [
    source 90
    target 527
  ]
  edge [
    source 90
    target 528
  ]
  edge [
    source 91
    target 529
  ]
  edge [
    source 91
    target 530
  ]
  edge [
    source 91
    target 531
  ]
  edge [
    source 91
    target 532
  ]
  edge [
    source 91
    target 533
  ]
  edge [
    source 91
    target 534
  ]
  edge [
    source 91
    target 535
  ]
  edge [
    source 91
    target 536
  ]
  edge [
    source 91
    target 469
  ]
  edge [
    source 92
    target 93
  ]
  edge [
    source 92
    target 537
  ]
  edge [
    source 92
    target 538
  ]
  edge [
    source 92
    target 539
  ]
  edge [
    source 93
    target 94
  ]
  edge [
    source 93
    target 540
  ]
  edge [
    source 93
    target 98
  ]
  edge [
    source 94
    target 95
  ]
  edge [
    source 94
    target 541
  ]
  edge [
    source 94
    target 542
  ]
  edge [
    source 94
    target 543
  ]
  edge [
    source 94
    target 544
  ]
  edge [
    source 94
    target 545
  ]
  edge [
    source 95
    target 96
  ]
  edge [
    source 96
    target 97
  ]
  edge [
    source 96
    target 546
  ]
  edge [
    source 96
    target 547
  ]
  edge [
    source 97
    target 98
  ]
  edge [
    source 97
    target 548
  ]
  edge [
    source 97
    target 549
  ]
  edge [
    source 97
    target 550
  ]
  edge [
    source 97
    target 551
  ]
  edge [
    source 97
    target 552
  ]
  edge [
    source 97
    target 553
  ]
  edge [
    source 97
    target 554
  ]
  edge [
    source 97
    target 555
  ]
  edge [
    source 97
    target 556
  ]
  edge [
    source 97
    target 557
  ]
  edge [
    source 97
    target 558
  ]
  edge [
    source 97
    target 559
  ]
  edge [
    source 97
    target 560
  ]
  edge [
    source 97
    target 561
  ]
  edge [
    source 97
    target 562
  ]
  edge [
    source 97
    target 563
  ]
  edge [
    source 98
    target 99
  ]
  edge [
    source 98
    target 564
  ]
  edge [
    source 98
    target 565
  ]
  edge [
    source 98
    target 566
  ]
  edge [
    source 98
    target 567
  ]
  edge [
    source 98
    target 568
  ]
  edge [
    source 98
    target 569
  ]
  edge [
    source 98
    target 570
  ]
  edge [
    source 99
    target 100
  ]
  edge [
    source 99
    target 571
  ]
  edge [
    source 99
    target 572
  ]
  edge [
    source 99
    target 573
  ]
  edge [
    source 99
    target 574
  ]
  edge [
    source 99
    target 575
  ]
  edge [
    source 100
    target 576
  ]
  edge [
    source 100
    target 577
  ]
  edge [
    source 100
    target 578
  ]
  edge [
    source 100
    target 579
  ]
  edge [
    source 100
    target 580
  ]
  edge [
    source 100
    target 581
  ]
  edge [
    source 100
    target 582
  ]
  edge [
    source 100
    target 103
  ]
  edge [
    source 101
    target 583
  ]
  edge [
    source 101
    target 584
  ]
  edge [
    source 102
    target 103
  ]
  edge [
    source 103
    target 104
  ]
  edge [
    source 103
    target 585
  ]
  edge [
    source 103
    target 577
  ]
  edge [
    source 103
    target 586
  ]
  edge [
    source 103
    target 587
  ]
  edge [
    source 103
    target 588
  ]
  edge [
    source 103
    target 574
  ]
  edge [
    source 103
    target 589
  ]
  edge [
    source 104
    target 105
  ]
  edge [
    source 104
    target 443
  ]
  edge [
    source 104
    target 590
  ]
  edge [
    source 105
    target 106
  ]
  edge [
    source 105
    target 591
  ]
  edge [
    source 105
    target 592
  ]
  edge [
    source 105
    target 593
  ]
  edge [
    source 105
    target 594
  ]
  edge [
    source 105
    target 595
  ]
  edge [
    source 105
    target 596
  ]
  edge [
    source 106
    target 107
  ]
  edge [
    source 106
    target 530
  ]
  edge [
    source 106
    target 597
  ]
  edge [
    source 106
    target 598
  ]
  edge [
    source 106
    target 599
  ]
  edge [
    source 106
    target 600
  ]
  edge [
    source 106
    target 601
  ]
  edge [
    source 106
    target 602
  ]
  edge [
    source 106
    target 603
  ]
  edge [
    source 106
    target 604
  ]
  edge [
    source 106
    target 605
  ]
  edge [
    source 106
    target 606
  ]
  edge [
    source 106
    target 223
  ]
  edge [
    source 106
    target 607
  ]
  edge [
    source 106
    target 608
  ]
  edge [
    source 106
    target 609
  ]
  edge [
    source 106
    target 610
  ]
  edge [
    source 106
    target 611
  ]
  edge [
    source 106
    target 612
  ]
  edge [
    source 106
    target 613
  ]
  edge [
    source 106
    target 614
  ]
  edge [
    source 106
    target 615
  ]
  edge [
    source 106
    target 616
  ]
  edge [
    source 106
    target 617
  ]
  edge [
    source 106
    target 618
  ]
  edge [
    source 106
    target 619
  ]
  edge [
    source 106
    target 620
  ]
  edge [
    source 107
    target 621
  ]
  edge [
    source 107
    target 622
  ]
  edge [
    source 107
    target 623
  ]
  edge [
    source 107
    target 624
  ]
  edge [
    source 107
    target 625
  ]
  edge [
    source 107
    target 626
  ]
  edge [
    source 107
    target 627
  ]
]
