graph [
  maxDegree 32
  minDegree 1
  meanDegree 2.134715025906736
  density 0.011118307426597583
  graphCliqueNumber 3
  node [
    id 0
    label "miejski"
    origin "text"
  ]
  node [
    id 1
    label "biblioteka"
    origin "text"
  ]
  node [
    id 2
    label "publiczny"
    origin "text"
  ]
  node [
    id 3
    label "organizowa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "wyjazd"
    origin "text"
  ]
  node [
    id 5
    label "kino"
    origin "text"
  ]
  node [
    id 6
    label "silverscreen"
    origin "text"
  ]
  node [
    id 7
    label "&#322;&#243;d&#378;"
    origin "text"
  ]
  node [
    id 8
    label "film"
    origin "text"
  ]
  node [
    id 9
    label "pi&#261;tek"
    origin "text"
  ]
  node [
    id 10
    label "pokaza&#263;"
    origin "text"
  ]
  node [
    id 11
    label "marco"
    origin "text"
  ]
  node [
    id 12
    label "rocznik"
    origin "text"
  ]
  node [
    id 13
    label "godz"
    origin "text"
  ]
  node [
    id 14
    label "sprzed"
    origin "text"
  ]
  node [
    id 15
    label "przy"
    origin "text"
  ]
  node [
    id 16
    label "ula"
    origin "text"
  ]
  node [
    id 17
    label "listopadowy"
    origin "text"
  ]
  node [
    id 18
    label "ca&#322;kowity"
    origin "text"
  ]
  node [
    id 19
    label "koszt"
    origin "text"
  ]
  node [
    id 20
    label "z&#322;oty"
    origin "text"
  ]
  node [
    id 21
    label "bilet"
    origin "text"
  ]
  node [
    id 22
    label "przejazd"
    origin "text"
  ]
  node [
    id 23
    label "zapis"
    origin "text"
  ]
  node [
    id 24
    label "mbp"
    origin "text"
  ]
  node [
    id 25
    label "liczba"
    origin "text"
  ]
  node [
    id 26
    label "miejsce"
    origin "text"
  ]
  node [
    id 27
    label "ograniczony"
    origin "text"
  ]
  node [
    id 28
    label "zaprasza&#263;"
    origin "text"
  ]
  node [
    id 29
    label "miejsko"
  ]
  node [
    id 30
    label "miastowy"
  ]
  node [
    id 31
    label "typowy"
  ]
  node [
    id 32
    label "pok&#243;j"
  ]
  node [
    id 33
    label "rewers"
  ]
  node [
    id 34
    label "informatorium"
  ]
  node [
    id 35
    label "kolekcja"
  ]
  node [
    id 36
    label "czytelnik"
  ]
  node [
    id 37
    label "budynek"
  ]
  node [
    id 38
    label "zbi&#243;r"
  ]
  node [
    id 39
    label "programowanie"
  ]
  node [
    id 40
    label "ksi&#281;gozbi&#243;r_podr&#281;czny"
  ]
  node [
    id 41
    label "library"
  ]
  node [
    id 42
    label "instytucja"
  ]
  node [
    id 43
    label "czytelnia"
  ]
  node [
    id 44
    label "jawny"
  ]
  node [
    id 45
    label "upublicznienie"
  ]
  node [
    id 46
    label "upublicznianie"
  ]
  node [
    id 47
    label "publicznie"
  ]
  node [
    id 48
    label "planowa&#263;"
  ]
  node [
    id 49
    label "dostosowywa&#263;"
  ]
  node [
    id 50
    label "pozyskiwa&#263;"
  ]
  node [
    id 51
    label "wprowadza&#263;"
  ]
  node [
    id 52
    label "treat"
  ]
  node [
    id 53
    label "przygotowywa&#263;"
  ]
  node [
    id 54
    label "create"
  ]
  node [
    id 55
    label "ensnare"
  ]
  node [
    id 56
    label "tworzy&#263;"
  ]
  node [
    id 57
    label "standard"
  ]
  node [
    id 58
    label "skupia&#263;"
  ]
  node [
    id 59
    label "podr&#243;&#380;"
  ]
  node [
    id 60
    label "digression"
  ]
  node [
    id 61
    label "animatronika"
  ]
  node [
    id 62
    label "cyrk"
  ]
  node [
    id 63
    label "seans"
  ]
  node [
    id 64
    label "dorobek"
  ]
  node [
    id 65
    label "ekran"
  ]
  node [
    id 66
    label "kinoteatr"
  ]
  node [
    id 67
    label "picture"
  ]
  node [
    id 68
    label "bioskop"
  ]
  node [
    id 69
    label "sztuka"
  ]
  node [
    id 70
    label "muza"
  ]
  node [
    id 71
    label "regaty"
  ]
  node [
    id 72
    label "statek"
  ]
  node [
    id 73
    label "spalin&#243;wka"
  ]
  node [
    id 74
    label "pok&#322;ad"
  ]
  node [
    id 75
    label "ster"
  ]
  node [
    id 76
    label "kratownica"
  ]
  node [
    id 77
    label "pojazd_niemechaniczny"
  ]
  node [
    id 78
    label "drzewce"
  ]
  node [
    id 79
    label "rozbieg&#243;wka"
  ]
  node [
    id 80
    label "block"
  ]
  node [
    id 81
    label "odczula&#263;"
  ]
  node [
    id 82
    label "blik"
  ]
  node [
    id 83
    label "rola"
  ]
  node [
    id 84
    label "trawiarnia"
  ]
  node [
    id 85
    label "b&#322;ona"
  ]
  node [
    id 86
    label "filmoteka"
  ]
  node [
    id 87
    label "odczuli&#263;"
  ]
  node [
    id 88
    label "klatka"
  ]
  node [
    id 89
    label "odczulenie"
  ]
  node [
    id 90
    label "emulsja_fotograficzna"
  ]
  node [
    id 91
    label "odczulanie"
  ]
  node [
    id 92
    label "scena"
  ]
  node [
    id 93
    label "czo&#322;&#243;wka"
  ]
  node [
    id 94
    label "ty&#322;&#243;wka"
  ]
  node [
    id 95
    label "napisy"
  ]
  node [
    id 96
    label "photograph"
  ]
  node [
    id 97
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 98
    label "postprodukcja"
  ]
  node [
    id 99
    label "sklejarka"
  ]
  node [
    id 100
    label "anamorfoza"
  ]
  node [
    id 101
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 102
    label "ta&#347;ma"
  ]
  node [
    id 103
    label "b&#322;ona_&#347;wiat&#322;oczu&#322;a"
  ]
  node [
    id 104
    label "uj&#281;cie"
  ]
  node [
    id 105
    label "dzie&#324;_powszedni"
  ]
  node [
    id 106
    label "Wielki_Pi&#261;tek"
  ]
  node [
    id 107
    label "tydzie&#324;"
  ]
  node [
    id 108
    label "spowodowa&#263;"
  ]
  node [
    id 109
    label "wyrazi&#263;"
  ]
  node [
    id 110
    label "da&#263;_w_ko&#347;&#263;"
  ]
  node [
    id 111
    label "przedstawi&#263;"
  ]
  node [
    id 112
    label "testify"
  ]
  node [
    id 113
    label "indicate"
  ]
  node [
    id 114
    label "przeszkoli&#263;"
  ]
  node [
    id 115
    label "udowodni&#263;"
  ]
  node [
    id 116
    label "poinformowa&#263;"
  ]
  node [
    id 117
    label "poda&#263;"
  ]
  node [
    id 118
    label "point"
  ]
  node [
    id 119
    label "formacja"
  ]
  node [
    id 120
    label "kronika"
  ]
  node [
    id 121
    label "czasopismo"
  ]
  node [
    id 122
    label "yearbook"
  ]
  node [
    id 123
    label "w_pizdu"
  ]
  node [
    id 124
    label "&#322;&#261;czny"
  ]
  node [
    id 125
    label "og&#243;lnie"
  ]
  node [
    id 126
    label "ca&#322;y"
  ]
  node [
    id 127
    label "pe&#322;ny"
  ]
  node [
    id 128
    label "zupe&#322;nie"
  ]
  node [
    id 129
    label "kompletnie"
  ]
  node [
    id 130
    label "nak&#322;ad"
  ]
  node [
    id 131
    label "rachunek_koszt&#243;w"
  ]
  node [
    id 132
    label "sumpt"
  ]
  node [
    id 133
    label "wydatek"
  ]
  node [
    id 134
    label "szlachetny"
  ]
  node [
    id 135
    label "metaliczny"
  ]
  node [
    id 136
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 137
    label "z&#322;oci&#347;cie"
  ]
  node [
    id 138
    label "grosz"
  ]
  node [
    id 139
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 140
    label "utytu&#322;owany"
  ]
  node [
    id 141
    label "poz&#322;ocenie"
  ]
  node [
    id 142
    label "Polska"
  ]
  node [
    id 143
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 144
    label "wspania&#322;y"
  ]
  node [
    id 145
    label "doskona&#322;y"
  ]
  node [
    id 146
    label "kochany"
  ]
  node [
    id 147
    label "jednostka_monetarna"
  ]
  node [
    id 148
    label "z&#322;ocenie"
  ]
  node [
    id 149
    label "karta_wst&#281;pu"
  ]
  node [
    id 150
    label "cedu&#322;a"
  ]
  node [
    id 151
    label "passe-partout"
  ]
  node [
    id 152
    label "konik"
  ]
  node [
    id 153
    label "way"
  ]
  node [
    id 154
    label "jazda"
  ]
  node [
    id 155
    label "czynno&#347;&#263;"
  ]
  node [
    id 156
    label "wytw&#243;r"
  ]
  node [
    id 157
    label "entrance"
  ]
  node [
    id 158
    label "normalizacja"
  ]
  node [
    id 159
    label "wpis"
  ]
  node [
    id 160
    label "spos&#243;b"
  ]
  node [
    id 161
    label "kategoria"
  ]
  node [
    id 162
    label "kategoria_gramatyczna"
  ]
  node [
    id 163
    label "kwadrat_magiczny"
  ]
  node [
    id 164
    label "grupa"
  ]
  node [
    id 165
    label "cecha"
  ]
  node [
    id 166
    label "wyra&#380;enie"
  ]
  node [
    id 167
    label "pierwiastek"
  ]
  node [
    id 168
    label "rozmiar"
  ]
  node [
    id 169
    label "number"
  ]
  node [
    id 170
    label "poj&#281;cie"
  ]
  node [
    id 171
    label "koniugacja"
  ]
  node [
    id 172
    label "cia&#322;o"
  ]
  node [
    id 173
    label "plac"
  ]
  node [
    id 174
    label "uwaga"
  ]
  node [
    id 175
    label "przestrze&#324;"
  ]
  node [
    id 176
    label "status"
  ]
  node [
    id 177
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 178
    label "chwila"
  ]
  node [
    id 179
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 180
    label "rz&#261;d"
  ]
  node [
    id 181
    label "praca"
  ]
  node [
    id 182
    label "location"
  ]
  node [
    id 183
    label "warunek_lokalowy"
  ]
  node [
    id 184
    label "ograniczenie_si&#281;"
  ]
  node [
    id 185
    label "nieelastyczny"
  ]
  node [
    id 186
    label "ograniczanie_si&#281;"
  ]
  node [
    id 187
    label "powolny"
  ]
  node [
    id 188
    label "ciasno"
  ]
  node [
    id 189
    label "invite"
  ]
  node [
    id 190
    label "ask"
  ]
  node [
    id 191
    label "oferowa&#263;"
  ]
  node [
    id 192
    label "zwraca&#263;_si&#281;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 10
  ]
  edge [
    source 4
    target 11
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 10
    target 110
  ]
  edge [
    source 10
    target 111
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 18
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 119
  ]
  edge [
    source 12
    target 120
  ]
  edge [
    source 12
    target 121
  ]
  edge [
    source 12
    target 122
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 24
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 25
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 123
  ]
  edge [
    source 18
    target 124
  ]
  edge [
    source 18
    target 125
  ]
  edge [
    source 18
    target 126
  ]
  edge [
    source 18
    target 127
  ]
  edge [
    source 18
    target 128
  ]
  edge [
    source 18
    target 129
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 130
  ]
  edge [
    source 19
    target 131
  ]
  edge [
    source 19
    target 132
  ]
  edge [
    source 19
    target 133
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 134
  ]
  edge [
    source 20
    target 135
  ]
  edge [
    source 20
    target 136
  ]
  edge [
    source 20
    target 137
  ]
  edge [
    source 20
    target 138
  ]
  edge [
    source 20
    target 139
  ]
  edge [
    source 20
    target 140
  ]
  edge [
    source 20
    target 141
  ]
  edge [
    source 20
    target 142
  ]
  edge [
    source 20
    target 143
  ]
  edge [
    source 20
    target 144
  ]
  edge [
    source 20
    target 145
  ]
  edge [
    source 20
    target 146
  ]
  edge [
    source 20
    target 147
  ]
  edge [
    source 20
    target 148
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 149
  ]
  edge [
    source 21
    target 150
  ]
  edge [
    source 21
    target 151
  ]
  edge [
    source 21
    target 152
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 26
  ]
  edge [
    source 22
    target 153
  ]
  edge [
    source 22
    target 154
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 155
  ]
  edge [
    source 23
    target 156
  ]
  edge [
    source 23
    target 157
  ]
  edge [
    source 23
    target 158
  ]
  edge [
    source 23
    target 159
  ]
  edge [
    source 23
    target 160
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 161
  ]
  edge [
    source 25
    target 162
  ]
  edge [
    source 25
    target 163
  ]
  edge [
    source 25
    target 164
  ]
  edge [
    source 25
    target 165
  ]
  edge [
    source 25
    target 166
  ]
  edge [
    source 25
    target 167
  ]
  edge [
    source 25
    target 168
  ]
  edge [
    source 25
    target 169
  ]
  edge [
    source 25
    target 170
  ]
  edge [
    source 25
    target 171
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 172
  ]
  edge [
    source 26
    target 173
  ]
  edge [
    source 26
    target 165
  ]
  edge [
    source 26
    target 174
  ]
  edge [
    source 26
    target 175
  ]
  edge [
    source 26
    target 176
  ]
  edge [
    source 26
    target 177
  ]
  edge [
    source 26
    target 178
  ]
  edge [
    source 26
    target 179
  ]
  edge [
    source 26
    target 180
  ]
  edge [
    source 26
    target 181
  ]
  edge [
    source 26
    target 182
  ]
  edge [
    source 26
    target 183
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 184
  ]
  edge [
    source 27
    target 185
  ]
  edge [
    source 27
    target 186
  ]
  edge [
    source 27
    target 187
  ]
  edge [
    source 27
    target 188
  ]
  edge [
    source 28
    target 189
  ]
  edge [
    source 28
    target 190
  ]
  edge [
    source 28
    target 191
  ]
  edge [
    source 28
    target 192
  ]
]
