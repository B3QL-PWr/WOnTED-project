graph [
  maxDegree 33
  minDegree 1
  meanDegree 2.810810810810811
  density 0.03850425768233987
  graphCliqueNumber 6
  node [
    id 0
    label "unia"
    origin "text"
  ]
  node [
    id 1
    label "demokrat"
    origin "text"
  ]
  node [
    id 2
    label "rzecz"
    origin "text"
  ]
  node [
    id 3
    label "republika"
    origin "text"
  ]
  node [
    id 4
    label "uk&#322;ad"
  ]
  node [
    id 5
    label "partia"
  ]
  node [
    id 6
    label "organizacja"
  ]
  node [
    id 7
    label "Unia_Wolno&#347;ci"
  ]
  node [
    id 8
    label "Unia_Europejska"
  ]
  node [
    id 9
    label "combination"
  ]
  node [
    id 10
    label "union"
  ]
  node [
    id 11
    label "Unia"
  ]
  node [
    id 12
    label "obiekt"
  ]
  node [
    id 13
    label "temat"
  ]
  node [
    id 14
    label "istota"
  ]
  node [
    id 15
    label "wpa&#347;&#263;"
  ]
  node [
    id 16
    label "wpadanie"
  ]
  node [
    id 17
    label "przedmiot"
  ]
  node [
    id 18
    label "wpada&#263;"
  ]
  node [
    id 19
    label "kultura"
  ]
  node [
    id 20
    label "przyroda"
  ]
  node [
    id 21
    label "mienie"
  ]
  node [
    id 22
    label "object"
  ]
  node [
    id 23
    label "wpadni&#281;cie"
  ]
  node [
    id 24
    label "Buriacja"
  ]
  node [
    id 25
    label "Abchazja"
  ]
  node [
    id 26
    label "Inguszetia"
  ]
  node [
    id 27
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 28
    label "Nachiczewan"
  ]
  node [
    id 29
    label "Karaka&#322;pacja"
  ]
  node [
    id 30
    label "Jakucja"
  ]
  node [
    id 31
    label "Singapur"
  ]
  node [
    id 32
    label "Komi"
  ]
  node [
    id 33
    label "Karelia"
  ]
  node [
    id 34
    label "Tatarstan"
  ]
  node [
    id 35
    label "Chakasja"
  ]
  node [
    id 36
    label "Dagestan"
  ]
  node [
    id 37
    label "Mordowia"
  ]
  node [
    id 38
    label "Ka&#322;mucja"
  ]
  node [
    id 39
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 40
    label "ustr&#243;j"
  ]
  node [
    id 41
    label "Republika_Cisalpi&#324;ska"
  ]
  node [
    id 42
    label "Baszkiria"
  ]
  node [
    id 43
    label "pa&#324;stwo_demokratyczne"
  ]
  node [
    id 44
    label "Mari_El"
  ]
  node [
    id 45
    label "Ad&#380;aria"
  ]
  node [
    id 46
    label "Czuwaszja"
  ]
  node [
    id 47
    label "Tuwa"
  ]
  node [
    id 48
    label "Czeczenia"
  ]
  node [
    id 49
    label "Udmurcja"
  ]
  node [
    id 50
    label "pa&#324;stwo"
  ]
  node [
    id 51
    label "demokrata"
  ]
  node [
    id 52
    label "na"
  ]
  node [
    id 53
    label "Union"
  ]
  node [
    id 54
    label "desa"
  ]
  node [
    id 55
    label "D&#233;mocrates"
  ]
  node [
    id 56
    label "pour"
  ]
  node [
    id 57
    label "la"
  ]
  node [
    id 58
    label "R&#233;publique"
  ]
  node [
    id 59
    label "Gaullist&#243;w"
  ]
  node [
    id 60
    label "Rassemblement"
  ]
  node [
    id 61
    label "du"
  ]
  node [
    id 62
    label "Peuple"
  ]
  node [
    id 63
    label "Fran&#231;ais"
  ]
  node [
    id 64
    label "Charles"
  ]
  node [
    id 65
    label "de"
  ]
  node [
    id 66
    label "Gaulle"
  ]
  node [
    id 67
    label "pi&#261;ty"
  ]
  node [
    id 68
    label "Nouvelle"
  ]
  node [
    id 69
    label "demokratyczny"
  ]
  node [
    id 70
    label "praca"
  ]
  node [
    id 71
    label "nowy"
  ]
  node [
    id 72
    label "Jacques"
  ]
  node [
    id 73
    label "Chirac"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 67
  ]
  edge [
    source 52
    target 67
  ]
  edge [
    source 52
    target 71
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 55
  ]
  edge [
    source 53
    target 56
  ]
  edge [
    source 53
    target 57
  ]
  edge [
    source 53
    target 58
  ]
  edge [
    source 53
    target 68
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 56
  ]
  edge [
    source 54
    target 57
  ]
  edge [
    source 54
    target 58
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 57
  ]
  edge [
    source 55
    target 58
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 58
  ]
  edge [
    source 56
    target 68
  ]
  edge [
    source 56
    target 60
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 68
  ]
  edge [
    source 57
    target 60
  ]
  edge [
    source 58
    target 68
  ]
  edge [
    source 58
    target 60
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 62
  ]
  edge [
    source 60
    target 63
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 63
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 66
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 72
    target 73
  ]
]
