graph [
  maxDegree 17
  minDegree 1
  meanDegree 1.9701492537313432
  density 0.029850746268656716
  graphCliqueNumber 2
  node [
    id 0
    label "letni"
    origin "text"
  ]
  node [
    id 1
    label "ch&#322;opiec"
    origin "text"
  ]
  node [
    id 2
    label "wys&#322;a&#263;"
    origin "text"
  ]
  node [
    id 3
    label "kartka"
    origin "text"
  ]
  node [
    id 4
    label "urodzinowy"
    origin "text"
  ]
  node [
    id 5
    label "swoje"
    origin "text"
  ]
  node [
    id 6
    label "zmar&#322;y"
    origin "text"
  ]
  node [
    id 7
    label "tata"
    origin "text"
  ]
  node [
    id 8
    label "nijaki"
  ]
  node [
    id 9
    label "sezonowy"
  ]
  node [
    id 10
    label "letnio"
  ]
  node [
    id 11
    label "s&#322;oneczny"
  ]
  node [
    id 12
    label "weso&#322;y"
  ]
  node [
    id 13
    label "oboj&#281;tny"
  ]
  node [
    id 14
    label "latowy"
  ]
  node [
    id 15
    label "ciep&#322;y"
  ]
  node [
    id 16
    label "typowy"
  ]
  node [
    id 17
    label "cz&#322;owiek"
  ]
  node [
    id 18
    label "pomocnik"
  ]
  node [
    id 19
    label "g&#243;wniarz"
  ]
  node [
    id 20
    label "&#347;l&#261;ski"
  ]
  node [
    id 21
    label "m&#322;odzieniec"
  ]
  node [
    id 22
    label "kajtek"
  ]
  node [
    id 23
    label "kawaler"
  ]
  node [
    id 24
    label "usynawianie"
  ]
  node [
    id 25
    label "dziecko"
  ]
  node [
    id 26
    label "okrzos"
  ]
  node [
    id 27
    label "usynowienie"
  ]
  node [
    id 28
    label "sympatia"
  ]
  node [
    id 29
    label "pederasta"
  ]
  node [
    id 30
    label "synek"
  ]
  node [
    id 31
    label "boyfriend"
  ]
  node [
    id 32
    label "wytworzy&#263;"
  ]
  node [
    id 33
    label "line"
  ]
  node [
    id 34
    label "ship"
  ]
  node [
    id 35
    label "convey"
  ]
  node [
    id 36
    label "przekaza&#263;"
  ]
  node [
    id 37
    label "post"
  ]
  node [
    id 38
    label "wy&#322;o&#380;y&#263;"
  ]
  node [
    id 39
    label "nakaza&#263;"
  ]
  node [
    id 40
    label "ticket"
  ]
  node [
    id 41
    label "kartonik"
  ]
  node [
    id 42
    label "arkusz"
  ]
  node [
    id 43
    label "bon"
  ]
  node [
    id 44
    label "wk&#322;ad"
  ]
  node [
    id 45
    label "faul"
  ]
  node [
    id 46
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 47
    label "s&#281;dzia"
  ]
  node [
    id 48
    label "kara"
  ]
  node [
    id 49
    label "strona"
  ]
  node [
    id 50
    label "duch"
  ]
  node [
    id 51
    label "umarlak"
  ]
  node [
    id 52
    label "zw&#322;oki"
  ]
  node [
    id 53
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 54
    label "chowanie"
  ]
  node [
    id 55
    label "nieumar&#322;y"
  ]
  node [
    id 56
    label "martwy"
  ]
  node [
    id 57
    label "stary"
  ]
  node [
    id 58
    label "papa"
  ]
  node [
    id 59
    label "ojciec"
  ]
  node [
    id 60
    label "kuwada"
  ]
  node [
    id 61
    label "ojczym"
  ]
  node [
    id 62
    label "przodek"
  ]
  node [
    id 63
    label "rodzice"
  ]
  node [
    id 64
    label "rodzic"
  ]
  node [
    id 65
    label "Royal"
  ]
  node [
    id 66
    label "mail"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 17
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 65
    target 66
  ]
]
