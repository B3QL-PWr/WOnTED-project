graph [
  maxDegree 48
  minDegree 1
  meanDegree 2.3222003929273085
  density 0.004571260615998639
  graphCliqueNumber 6
  node [
    id 0
    label "rada"
    origin "text"
  ]
  node [
    id 1
    label "nadzorczy"
    origin "text"
  ]
  node [
    id 2
    label "polski"
    origin "text"
  ]
  node [
    id 3
    label "radio"
    origin "text"
  ]
  node [
    id 4
    label "przywr&#243;ci&#263;"
    origin "text"
  ]
  node [
    id 5
    label "jerzy"
    origin "text"
  ]
  node [
    id 6
    label "targalskiego"
    origin "text"
  ]
  node [
    id 7
    label "prawo"
    origin "text"
  ]
  node [
    id 8
    label "cz&#322;onek"
    origin "text"
  ]
  node [
    id 9
    label "zarz&#261;d"
    origin "text"
  ]
  node [
    id 10
    label "by&#263;"
    origin "text"
  ]
  node [
    id 11
    label "zawiesi&#263;"
    origin "text"
  ]
  node [
    id 12
    label "kwiecie&#324;"
    origin "text"
  ]
  node [
    id 13
    label "zwi&#261;zek"
    origin "text"
  ]
  node [
    id 14
    label "oskar&#380;enie"
    origin "text"
  ]
  node [
    id 15
    label "obra&#378;liwy"
    origin "text"
  ]
  node [
    id 16
    label "wypowied&#378;"
    origin "text"
  ]
  node [
    id 17
    label "pod"
    origin "text"
  ]
  node [
    id 18
    label "adres"
    origin "text"
  ]
  node [
    id 19
    label "zwalnia&#263;"
    origin "text"
  ]
  node [
    id 20
    label "pracownik"
    origin "text"
  ]
  node [
    id 21
    label "uwzgl&#281;dnia&#263;"
    origin "text"
  ]
  node [
    id 22
    label "dotychczasowy"
    origin "text"
  ]
  node [
    id 23
    label "wk&#322;ad"
    origin "text"
  ]
  node [
    id 24
    label "praca"
    origin "text"
  ]
  node [
    id 25
    label "mai&#263;"
    origin "text"
  ]
  node [
    id 26
    label "uwaga"
    origin "text"
  ]
  node [
    id 27
    label "sprawno&#347;&#263;"
    origin "text"
  ]
  node [
    id 28
    label "dzia&#322;ania"
    origin "text"
  ]
  node [
    id 29
    label "sp&#243;&#322;ka"
    origin "text"
  ]
  node [
    id 30
    label "g&#322;osi&#263;"
    origin "text"
  ]
  node [
    id 31
    label "komunikat"
    origin "text"
  ]
  node [
    id 32
    label "rzecznik"
    origin "text"
  ]
  node [
    id 33
    label "sobotni"
    origin "text"
  ]
  node [
    id 34
    label "decyzja"
    origin "text"
  ]
  node [
    id 35
    label "rad"
    origin "text"
  ]
  node [
    id 36
    label "poprzedzi&#263;"
    origin "text"
  ]
  node [
    id 37
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 38
    label "kilka"
    origin "text"
  ]
  node [
    id 39
    label "posiedzenie"
    origin "text"
  ]
  node [
    id 40
    label "podczas"
    origin "text"
  ]
  node [
    id 41
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 42
    label "wys&#322;ucha&#263;"
    origin "text"
  ]
  node [
    id 43
    label "opinia"
    origin "text"
  ]
  node [
    id 44
    label "szef"
    origin "text"
  ]
  node [
    id 45
    label "dzia&#322;a&#263;"
    origin "text"
  ]
  node [
    id 46
    label "zawodowy"
    origin "text"
  ]
  node [
    id 47
    label "rozmawia&#263;"
    origin "text"
  ]
  node [
    id 48
    label "sam"
    origin "text"
  ]
  node [
    id 49
    label "targalskim"
    origin "text"
  ]
  node [
    id 50
    label "kilkadziesi&#261;t"
    origin "text"
  ]
  node [
    id 51
    label "metr"
    origin "text"
  ]
  node [
    id 52
    label "maria"
    origin "text"
  ]
  node [
    id 53
    label "szab&#322;owska"
    origin "text"
  ]
  node [
    id 54
    label "m&#261;&#380;"
    origin "text"
  ]
  node [
    id 55
    label "marco"
    origin "text"
  ]
  node [
    id 56
    label "lipi&#324;ska"
    origin "text"
  ]
  node [
    id 57
    label "tadeusz"
    origin "text"
  ]
  node [
    id 58
    label "sznukiem"
    origin "text"
  ]
  node [
    id 59
    label "ma&#322;gorzata"
    origin "text"
  ]
  node [
    id 60
    label "s&#322;omkowsk&#261;"
    origin "text"
  ]
  node [
    id 61
    label "zapozna&#263;"
    origin "text"
  ]
  node [
    id 62
    label "si&#281;"
    origin "text"
  ]
  node [
    id 63
    label "te&#380;"
    origin "text"
  ]
  node [
    id 64
    label "badaj&#261;cy"
    origin "text"
  ]
  node [
    id 65
    label "sprawa"
    origin "text"
  ]
  node [
    id 66
    label "radiowy"
    origin "text"
  ]
  node [
    id 67
    label "komisja"
    origin "text"
  ]
  node [
    id 68
    label "etyka"
    origin "text"
  ]
  node [
    id 69
    label "dyskusja"
  ]
  node [
    id 70
    label "grupa"
  ]
  node [
    id 71
    label "Rada_Unii_Europejskiej"
  ]
  node [
    id 72
    label "conference"
  ]
  node [
    id 73
    label "organ"
  ]
  node [
    id 74
    label "zgromadzenie"
  ]
  node [
    id 75
    label "wskaz&#243;wka"
  ]
  node [
    id 76
    label "konsylium"
  ]
  node [
    id 77
    label "Rada_Europy"
  ]
  node [
    id 78
    label "Rada_Europejska"
  ]
  node [
    id 79
    label "kontrolny"
  ]
  node [
    id 80
    label "lacki"
  ]
  node [
    id 81
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 82
    label "przedmiot"
  ]
  node [
    id 83
    label "sztajer"
  ]
  node [
    id 84
    label "drabant"
  ]
  node [
    id 85
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 86
    label "polak"
  ]
  node [
    id 87
    label "pierogi_ruskie"
  ]
  node [
    id 88
    label "krakowiak"
  ]
  node [
    id 89
    label "Polish"
  ]
  node [
    id 90
    label "j&#281;zyk"
  ]
  node [
    id 91
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 92
    label "oberek"
  ]
  node [
    id 93
    label "po_polsku"
  ]
  node [
    id 94
    label "mazur"
  ]
  node [
    id 95
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 96
    label "chodzony"
  ]
  node [
    id 97
    label "skoczny"
  ]
  node [
    id 98
    label "ryba_po_grecku"
  ]
  node [
    id 99
    label "goniony"
  ]
  node [
    id 100
    label "polsko"
  ]
  node [
    id 101
    label "uk&#322;ad"
  ]
  node [
    id 102
    label "paj&#281;czarz"
  ]
  node [
    id 103
    label "fala_radiowa"
  ]
  node [
    id 104
    label "spot"
  ]
  node [
    id 105
    label "programowiec"
  ]
  node [
    id 106
    label "urz&#261;dzenie_RTV"
  ]
  node [
    id 107
    label "eliminator"
  ]
  node [
    id 108
    label "studio"
  ]
  node [
    id 109
    label "radiola"
  ]
  node [
    id 110
    label "redakcja"
  ]
  node [
    id 111
    label "odbieranie"
  ]
  node [
    id 112
    label "dyskryminator"
  ]
  node [
    id 113
    label "odbiera&#263;"
  ]
  node [
    id 114
    label "odbiornik"
  ]
  node [
    id 115
    label "media"
  ]
  node [
    id 116
    label "urz&#261;dzenie_radiowe"
  ]
  node [
    id 117
    label "stacja"
  ]
  node [
    id 118
    label "radiolinia"
  ]
  node [
    id 119
    label "radiofonia"
  ]
  node [
    id 120
    label "doprowadzi&#263;"
  ]
  node [
    id 121
    label "obserwacja"
  ]
  node [
    id 122
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 123
    label "nauka_prawa"
  ]
  node [
    id 124
    label "dominion"
  ]
  node [
    id 125
    label "normatywizm"
  ]
  node [
    id 126
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 127
    label "qualification"
  ]
  node [
    id 128
    label "opis"
  ]
  node [
    id 129
    label "regu&#322;a_Allena"
  ]
  node [
    id 130
    label "normalizacja"
  ]
  node [
    id 131
    label "kazuistyka"
  ]
  node [
    id 132
    label "regu&#322;a_Glogera"
  ]
  node [
    id 133
    label "kultura_duchowa"
  ]
  node [
    id 134
    label "prawo_karne"
  ]
  node [
    id 135
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 136
    label "standard"
  ]
  node [
    id 137
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 138
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 139
    label "struktura"
  ]
  node [
    id 140
    label "szko&#322;a"
  ]
  node [
    id 141
    label "prawo_karne_procesowe"
  ]
  node [
    id 142
    label "prawo_Mendla"
  ]
  node [
    id 143
    label "przepis"
  ]
  node [
    id 144
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 145
    label "criterion"
  ]
  node [
    id 146
    label "kanonistyka"
  ]
  node [
    id 147
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 148
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 149
    label "wykonawczy"
  ]
  node [
    id 150
    label "twierdzenie"
  ]
  node [
    id 151
    label "judykatura"
  ]
  node [
    id 152
    label "legislacyjnie"
  ]
  node [
    id 153
    label "umocowa&#263;"
  ]
  node [
    id 154
    label "podmiot"
  ]
  node [
    id 155
    label "procesualistyka"
  ]
  node [
    id 156
    label "kierunek"
  ]
  node [
    id 157
    label "kryminologia"
  ]
  node [
    id 158
    label "kryminalistyka"
  ]
  node [
    id 159
    label "cywilistyka"
  ]
  node [
    id 160
    label "law"
  ]
  node [
    id 161
    label "zasada_d'Alemberta"
  ]
  node [
    id 162
    label "jurisprudence"
  ]
  node [
    id 163
    label "zasada"
  ]
  node [
    id 164
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 165
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 166
    label "cz&#322;owiek"
  ]
  node [
    id 167
    label "cia&#322;o"
  ]
  node [
    id 168
    label "organizacja"
  ]
  node [
    id 169
    label "przedstawiciel"
  ]
  node [
    id 170
    label "shaft"
  ]
  node [
    id 171
    label "fiut"
  ]
  node [
    id 172
    label "przyrodzenie"
  ]
  node [
    id 173
    label "wchodzenie"
  ]
  node [
    id 174
    label "ptaszek"
  ]
  node [
    id 175
    label "wej&#347;cie"
  ]
  node [
    id 176
    label "narz&#261;d_kopulacyjny"
  ]
  node [
    id 177
    label "element_anatomiczny"
  ]
  node [
    id 178
    label "administration"
  ]
  node [
    id 179
    label "czynno&#347;&#263;"
  ]
  node [
    id 180
    label "administracja"
  ]
  node [
    id 181
    label "biuro"
  ]
  node [
    id 182
    label "w&#322;adza"
  ]
  node [
    id 183
    label "organ_sp&#243;&#322;ki"
  ]
  node [
    id 184
    label "kierownictwo"
  ]
  node [
    id 185
    label "Bruksela"
  ]
  node [
    id 186
    label "siedziba"
  ]
  node [
    id 187
    label "centrala"
  ]
  node [
    id 188
    label "si&#281;ga&#263;"
  ]
  node [
    id 189
    label "trwa&#263;"
  ]
  node [
    id 190
    label "obecno&#347;&#263;"
  ]
  node [
    id 191
    label "stan"
  ]
  node [
    id 192
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 193
    label "stand"
  ]
  node [
    id 194
    label "mie&#263;_miejsce"
  ]
  node [
    id 195
    label "uczestniczy&#263;"
  ]
  node [
    id 196
    label "chodzi&#263;"
  ]
  node [
    id 197
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 198
    label "equal"
  ]
  node [
    id 199
    label "zabra&#263;"
  ]
  node [
    id 200
    label "bind"
  ]
  node [
    id 201
    label "powiesi&#263;"
  ]
  node [
    id 202
    label "przymocowa&#263;"
  ]
  node [
    id 203
    label "wstrzyma&#263;"
  ]
  node [
    id 204
    label "suspend"
  ]
  node [
    id 205
    label "umie&#347;ci&#263;"
  ]
  node [
    id 206
    label "miesi&#261;c"
  ]
  node [
    id 207
    label "odwodnienie"
  ]
  node [
    id 208
    label "konstytucja"
  ]
  node [
    id 209
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 210
    label "substancja_chemiczna"
  ]
  node [
    id 211
    label "bratnia_dusza"
  ]
  node [
    id 212
    label "zwi&#261;zanie"
  ]
  node [
    id 213
    label "lokant"
  ]
  node [
    id 214
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 215
    label "zwi&#261;za&#263;"
  ]
  node [
    id 216
    label "odwadnia&#263;"
  ]
  node [
    id 217
    label "marriage"
  ]
  node [
    id 218
    label "marketing_afiliacyjny"
  ]
  node [
    id 219
    label "bearing"
  ]
  node [
    id 220
    label "wi&#261;zanie"
  ]
  node [
    id 221
    label "odwadnianie"
  ]
  node [
    id 222
    label "koligacja"
  ]
  node [
    id 223
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 224
    label "odwodni&#263;"
  ]
  node [
    id 225
    label "azeotrop"
  ]
  node [
    id 226
    label "powi&#261;zanie"
  ]
  node [
    id 227
    label "s&#261;d"
  ]
  node [
    id 228
    label "ocena"
  ]
  node [
    id 229
    label "ocenienie"
  ]
  node [
    id 230
    label "post&#281;powanie"
  ]
  node [
    id 231
    label "skar&#380;yciel"
  ]
  node [
    id 232
    label "poj&#281;cie"
  ]
  node [
    id 233
    label "suspicja"
  ]
  node [
    id 234
    label "strona"
  ]
  node [
    id 235
    label "z&#322;y"
  ]
  node [
    id 236
    label "zel&#380;ywy"
  ]
  node [
    id 237
    label "obra&#378;liwie"
  ]
  node [
    id 238
    label "parafrazowa&#263;"
  ]
  node [
    id 239
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 240
    label "sparafrazowa&#263;"
  ]
  node [
    id 241
    label "trawestowanie"
  ]
  node [
    id 242
    label "sparafrazowanie"
  ]
  node [
    id 243
    label "strawestowa&#263;"
  ]
  node [
    id 244
    label "sformu&#322;owanie"
  ]
  node [
    id 245
    label "strawestowanie"
  ]
  node [
    id 246
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 247
    label "delimitacja"
  ]
  node [
    id 248
    label "trawestowa&#263;"
  ]
  node [
    id 249
    label "parafrazowanie"
  ]
  node [
    id 250
    label "stylizacja"
  ]
  node [
    id 251
    label "ozdobnik"
  ]
  node [
    id 252
    label "pos&#322;uchanie"
  ]
  node [
    id 253
    label "rezultat"
  ]
  node [
    id 254
    label "adres_elektroniczny"
  ]
  node [
    id 255
    label "domena"
  ]
  node [
    id 256
    label "po&#322;o&#380;enie"
  ]
  node [
    id 257
    label "kod_pocztowy"
  ]
  node [
    id 258
    label "dane"
  ]
  node [
    id 259
    label "pismo"
  ]
  node [
    id 260
    label "przesy&#322;ka"
  ]
  node [
    id 261
    label "personalia"
  ]
  node [
    id 262
    label "dziedzina"
  ]
  node [
    id 263
    label "wylewa&#263;"
  ]
  node [
    id 264
    label "wypuszcza&#263;"
  ]
  node [
    id 265
    label "unbosom"
  ]
  node [
    id 266
    label "sprawia&#263;"
  ]
  node [
    id 267
    label "uprzedza&#263;"
  ]
  node [
    id 268
    label "robi&#263;"
  ]
  node [
    id 269
    label "odpuszcza&#263;"
  ]
  node [
    id 270
    label "oddala&#263;"
  ]
  node [
    id 271
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 272
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 273
    label "wymawia&#263;"
  ]
  node [
    id 274
    label "powodowa&#263;"
  ]
  node [
    id 275
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 276
    label "polu&#378;nia&#263;"
  ]
  node [
    id 277
    label "deliver"
  ]
  node [
    id 278
    label "cz&#322;owiek_pracy"
  ]
  node [
    id 279
    label "delegowa&#263;"
  ]
  node [
    id 280
    label "s&#322;u&#380;ba_cywilna"
  ]
  node [
    id 281
    label "pracu&#347;"
  ]
  node [
    id 282
    label "delegowanie"
  ]
  node [
    id 283
    label "r&#281;ka"
  ]
  node [
    id 284
    label "salariat"
  ]
  node [
    id 285
    label "my&#347;le&#263;"
  ]
  node [
    id 286
    label "involve"
  ]
  node [
    id 287
    label "dotychczasowo"
  ]
  node [
    id 288
    label "input"
  ]
  node [
    id 289
    label "ok&#322;adka"
  ]
  node [
    id 290
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 291
    label "kartka"
  ]
  node [
    id 292
    label "lokata"
  ]
  node [
    id 293
    label "uczestnictwo"
  ]
  node [
    id 294
    label "element"
  ]
  node [
    id 295
    label "czasopismo"
  ]
  node [
    id 296
    label "zeszyt"
  ]
  node [
    id 297
    label "kwota"
  ]
  node [
    id 298
    label "stosunek_pracy"
  ]
  node [
    id 299
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 300
    label "benedykty&#324;ski"
  ]
  node [
    id 301
    label "pracowanie"
  ]
  node [
    id 302
    label "zaw&#243;d"
  ]
  node [
    id 303
    label "zmiana"
  ]
  node [
    id 304
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 305
    label "wytw&#243;r"
  ]
  node [
    id 306
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 307
    label "tynkarski"
  ]
  node [
    id 308
    label "czynnik_produkcji"
  ]
  node [
    id 309
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 310
    label "zobowi&#261;zanie"
  ]
  node [
    id 311
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 312
    label "tyrka"
  ]
  node [
    id 313
    label "pracowa&#263;"
  ]
  node [
    id 314
    label "poda&#380;_pracy"
  ]
  node [
    id 315
    label "miejsce"
  ]
  node [
    id 316
    label "zak&#322;ad"
  ]
  node [
    id 317
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 318
    label "najem"
  ]
  node [
    id 319
    label "ozdabia&#263;"
  ]
  node [
    id 320
    label "nagana"
  ]
  node [
    id 321
    label "dzienniczek"
  ]
  node [
    id 322
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 323
    label "wzgl&#261;d"
  ]
  node [
    id 324
    label "gossip"
  ]
  node [
    id 325
    label "upomnienie"
  ]
  node [
    id 326
    label "tekst"
  ]
  node [
    id 327
    label "zdrowie"
  ]
  node [
    id 328
    label "szybko&#347;&#263;"
  ]
  node [
    id 329
    label "harcerski"
  ]
  node [
    id 330
    label "cecha"
  ]
  node [
    id 331
    label "kondycja_fizyczna"
  ]
  node [
    id 332
    label "jako&#347;&#263;"
  ]
  node [
    id 333
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 334
    label "odznaka"
  ]
  node [
    id 335
    label "strategia"
  ]
  node [
    id 336
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 337
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 338
    label "ob&#322;awa_augustowska"
  ]
  node [
    id 339
    label "podmiot_gospodarczy"
  ]
  node [
    id 340
    label "zesp&#243;&#322;"
  ]
  node [
    id 341
    label "wsp&#243;lnictwo"
  ]
  node [
    id 342
    label "wypowiada&#263;"
  ]
  node [
    id 343
    label "szczeka&#263;"
  ]
  node [
    id 344
    label "rozpowszechnia&#263;"
  ]
  node [
    id 345
    label "publicize"
  ]
  node [
    id 346
    label "rumor"
  ]
  node [
    id 347
    label "talk"
  ]
  node [
    id 348
    label "pies_my&#347;liwski"
  ]
  node [
    id 349
    label "kreacjonista"
  ]
  node [
    id 350
    label "roi&#263;_si&#281;"
  ]
  node [
    id 351
    label "uroi&#263;_si&#281;"
  ]
  node [
    id 352
    label "communication"
  ]
  node [
    id 353
    label "doradca"
  ]
  node [
    id 354
    label "przyjaciel"
  ]
  node [
    id 355
    label "dokument"
  ]
  node [
    id 356
    label "resolution"
  ]
  node [
    id 357
    label "zdecydowanie"
  ]
  node [
    id 358
    label "podj&#281;cie_decyzji"
  ]
  node [
    id 359
    label "management"
  ]
  node [
    id 360
    label "berylowiec"
  ]
  node [
    id 361
    label "u&#347;miechni&#281;ty"
  ]
  node [
    id 362
    label "pierwiastek_promieniotw&#243;rczy"
  ]
  node [
    id 363
    label "mikroradian"
  ]
  node [
    id 364
    label "zadowolenie_si&#281;"
  ]
  node [
    id 365
    label "usatysfakcjonowanie_si&#281;"
  ]
  node [
    id 366
    label "content"
  ]
  node [
    id 367
    label "jednostka_promieniowania"
  ]
  node [
    id 368
    label "miliradian"
  ]
  node [
    id 369
    label "jednostka"
  ]
  node [
    id 370
    label "czas"
  ]
  node [
    id 371
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 372
    label "opatrzy&#263;"
  ]
  node [
    id 373
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 374
    label "zrobi&#263;"
  ]
  node [
    id 375
    label "overwhelm"
  ]
  node [
    id 376
    label "proceed"
  ]
  node [
    id 377
    label "catch"
  ]
  node [
    id 378
    label "pozosta&#263;"
  ]
  node [
    id 379
    label "osta&#263;_si&#281;"
  ]
  node [
    id 380
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 381
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 382
    label "change"
  ]
  node [
    id 383
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 384
    label "&#347;ledziowate"
  ]
  node [
    id 385
    label "ryba"
  ]
  node [
    id 386
    label "porobienie"
  ]
  node [
    id 387
    label "odsiedzenie"
  ]
  node [
    id 388
    label "convention"
  ]
  node [
    id 389
    label "adjustment"
  ]
  node [
    id 390
    label "pobycie"
  ]
  node [
    id 391
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 392
    label "muzyka"
  ]
  node [
    id 393
    label "nagranie"
  ]
  node [
    id 394
    label "spe&#322;ni&#263;"
  ]
  node [
    id 395
    label "rehabilitowanie_si&#281;"
  ]
  node [
    id 396
    label "reputacja"
  ]
  node [
    id 397
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 398
    label "rehabilitowa&#263;_si&#281;"
  ]
  node [
    id 399
    label "zrehabilitowanie_si&#281;"
  ]
  node [
    id 400
    label "informacja"
  ]
  node [
    id 401
    label "sofcik"
  ]
  node [
    id 402
    label "appraisal"
  ]
  node [
    id 403
    label "ekspertyza"
  ]
  node [
    id 404
    label "zrehabilitowa&#263;_si&#281;"
  ]
  node [
    id 405
    label "pogl&#261;d"
  ]
  node [
    id 406
    label "kryterium"
  ]
  node [
    id 407
    label "wielko&#347;&#263;"
  ]
  node [
    id 408
    label "kierowa&#263;"
  ]
  node [
    id 409
    label "zwrot"
  ]
  node [
    id 410
    label "pryncypa&#322;"
  ]
  node [
    id 411
    label "work"
  ]
  node [
    id 412
    label "reakcja_chemiczna"
  ]
  node [
    id 413
    label "function"
  ]
  node [
    id 414
    label "commit"
  ]
  node [
    id 415
    label "bangla&#263;"
  ]
  node [
    id 416
    label "determine"
  ]
  node [
    id 417
    label "tryb"
  ]
  node [
    id 418
    label "dziama&#263;"
  ]
  node [
    id 419
    label "istnie&#263;"
  ]
  node [
    id 420
    label "zawodowo"
  ]
  node [
    id 421
    label "zawo&#322;any"
  ]
  node [
    id 422
    label "profesjonalny"
  ]
  node [
    id 423
    label "klawy"
  ]
  node [
    id 424
    label "czadowy"
  ]
  node [
    id 425
    label "fajny"
  ]
  node [
    id 426
    label "fachowy"
  ]
  node [
    id 427
    label "s&#322;u&#380;bowo"
  ]
  node [
    id 428
    label "formalny"
  ]
  node [
    id 429
    label "gaworzy&#263;"
  ]
  node [
    id 430
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 431
    label "sklep"
  ]
  node [
    id 432
    label "meter"
  ]
  node [
    id 433
    label "decymetr"
  ]
  node [
    id 434
    label "megabyte"
  ]
  node [
    id 435
    label "plon"
  ]
  node [
    id 436
    label "metrum"
  ]
  node [
    id 437
    label "dekametr"
  ]
  node [
    id 438
    label "jednostka_powierzchni"
  ]
  node [
    id 439
    label "uk&#322;ad_SI"
  ]
  node [
    id 440
    label "literaturoznawstwo"
  ]
  node [
    id 441
    label "wiersz"
  ]
  node [
    id 442
    label "gigametr"
  ]
  node [
    id 443
    label "miara"
  ]
  node [
    id 444
    label "nauczyciel"
  ]
  node [
    id 445
    label "kilometr_kwadratowy"
  ]
  node [
    id 446
    label "jednostka_metryczna"
  ]
  node [
    id 447
    label "jednostka_masy"
  ]
  node [
    id 448
    label "centymetr_kwadratowy"
  ]
  node [
    id 449
    label "pan_domu"
  ]
  node [
    id 450
    label "ch&#322;op"
  ]
  node [
    id 451
    label "ma&#322;&#380;onek"
  ]
  node [
    id 452
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 453
    label "stary"
  ]
  node [
    id 454
    label "&#347;lubny"
  ]
  node [
    id 455
    label "m&#243;j"
  ]
  node [
    id 456
    label "pan_i_w&#322;adca"
  ]
  node [
    id 457
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 458
    label "pan_m&#322;ody"
  ]
  node [
    id 459
    label "pozna&#263;"
  ]
  node [
    id 460
    label "zawrze&#263;"
  ]
  node [
    id 461
    label "teach"
  ]
  node [
    id 462
    label "insert"
  ]
  node [
    id 463
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 464
    label "poinformowa&#263;"
  ]
  node [
    id 465
    label "obznajomi&#263;"
  ]
  node [
    id 466
    label "temat"
  ]
  node [
    id 467
    label "kognicja"
  ]
  node [
    id 468
    label "idea"
  ]
  node [
    id 469
    label "szczeg&#243;&#322;"
  ]
  node [
    id 470
    label "rzecz"
  ]
  node [
    id 471
    label "wydarzenie"
  ]
  node [
    id 472
    label "przes&#322;anka"
  ]
  node [
    id 473
    label "rozprawa"
  ]
  node [
    id 474
    label "object"
  ]
  node [
    id 475
    label "proposition"
  ]
  node [
    id 476
    label "medialny"
  ]
  node [
    id 477
    label "radiowo"
  ]
  node [
    id 478
    label "obrady"
  ]
  node [
    id 479
    label "Komisja_Europejska"
  ]
  node [
    id 480
    label "podkomisja"
  ]
  node [
    id 481
    label "morality"
  ]
  node [
    id 482
    label "hedonizm_etyczny"
  ]
  node [
    id 483
    label "felicytologia"
  ]
  node [
    id 484
    label "aretologia"
  ]
  node [
    id 485
    label "metaetyka"
  ]
  node [
    id 486
    label "bioetyka"
  ]
  node [
    id 487
    label "moralistyka"
  ]
  node [
    id 488
    label "cyrenaizm"
  ]
  node [
    id 489
    label "deontologia"
  ]
  node [
    id 490
    label "ascetyka"
  ]
  node [
    id 491
    label "ethics"
  ]
  node [
    id 492
    label "polskie"
  ]
  node [
    id 493
    label "Jerzy"
  ]
  node [
    id 494
    label "Targalskiego"
  ]
  node [
    id 495
    label "Maria"
  ]
  node [
    id 496
    label "Szab&#322;owska"
  ]
  node [
    id 497
    label "Ma&#322;gorzata"
  ]
  node [
    id 498
    label "S&#322;omkowsk&#261;"
  ]
  node [
    id 499
    label "Tadeusz"
  ]
  node [
    id 500
    label "Sznukiem"
  ]
  node [
    id 501
    label "marek"
  ]
  node [
    id 502
    label "Lipi&#324;ski"
  ]
  node [
    id 503
    label "Janina"
  ]
  node [
    id 504
    label "Jankowska"
  ]
  node [
    id 505
    label "Targalski"
  ]
  node [
    id 506
    label "medium"
  ]
  node [
    id 507
    label "Magdalena"
  ]
  node [
    id 508
    label "bajer"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 492
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 506
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 492
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 492
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 21
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 121
  ]
  edge [
    source 7
    target 122
  ]
  edge [
    source 7
    target 123
  ]
  edge [
    source 7
    target 124
  ]
  edge [
    source 7
    target 125
  ]
  edge [
    source 7
    target 126
  ]
  edge [
    source 7
    target 127
  ]
  edge [
    source 7
    target 128
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 7
    target 130
  ]
  edge [
    source 7
    target 131
  ]
  edge [
    source 7
    target 132
  ]
  edge [
    source 7
    target 133
  ]
  edge [
    source 7
    target 134
  ]
  edge [
    source 7
    target 135
  ]
  edge [
    source 7
    target 136
  ]
  edge [
    source 7
    target 137
  ]
  edge [
    source 7
    target 138
  ]
  edge [
    source 7
    target 139
  ]
  edge [
    source 7
    target 140
  ]
  edge [
    source 7
    target 141
  ]
  edge [
    source 7
    target 142
  ]
  edge [
    source 7
    target 143
  ]
  edge [
    source 7
    target 144
  ]
  edge [
    source 7
    target 145
  ]
  edge [
    source 7
    target 146
  ]
  edge [
    source 7
    target 147
  ]
  edge [
    source 7
    target 148
  ]
  edge [
    source 7
    target 149
  ]
  edge [
    source 7
    target 150
  ]
  edge [
    source 7
    target 151
  ]
  edge [
    source 7
    target 152
  ]
  edge [
    source 7
    target 153
  ]
  edge [
    source 7
    target 154
  ]
  edge [
    source 7
    target 155
  ]
  edge [
    source 7
    target 156
  ]
  edge [
    source 7
    target 157
  ]
  edge [
    source 7
    target 158
  ]
  edge [
    source 7
    target 159
  ]
  edge [
    source 7
    target 160
  ]
  edge [
    source 7
    target 161
  ]
  edge [
    source 7
    target 162
  ]
  edge [
    source 7
    target 163
  ]
  edge [
    source 7
    target 164
  ]
  edge [
    source 7
    target 165
  ]
  edge [
    source 7
    target 24
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 166
  ]
  edge [
    source 8
    target 167
  ]
  edge [
    source 8
    target 168
  ]
  edge [
    source 8
    target 169
  ]
  edge [
    source 8
    target 170
  ]
  edge [
    source 8
    target 154
  ]
  edge [
    source 8
    target 171
  ]
  edge [
    source 8
    target 172
  ]
  edge [
    source 8
    target 173
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 174
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 175
  ]
  edge [
    source 8
    target 176
  ]
  edge [
    source 8
    target 177
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 28
  ]
  edge [
    source 9
    target 29
  ]
  edge [
    source 9
    target 178
  ]
  edge [
    source 9
    target 179
  ]
  edge [
    source 9
    target 168
  ]
  edge [
    source 9
    target 180
  ]
  edge [
    source 9
    target 181
  ]
  edge [
    source 9
    target 182
  ]
  edge [
    source 9
    target 183
  ]
  edge [
    source 9
    target 184
  ]
  edge [
    source 9
    target 185
  ]
  edge [
    source 9
    target 186
  ]
  edge [
    source 9
    target 187
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 188
  ]
  edge [
    source 10
    target 189
  ]
  edge [
    source 10
    target 190
  ]
  edge [
    source 10
    target 191
  ]
  edge [
    source 10
    target 192
  ]
  edge [
    source 10
    target 193
  ]
  edge [
    source 10
    target 194
  ]
  edge [
    source 10
    target 195
  ]
  edge [
    source 10
    target 196
  ]
  edge [
    source 10
    target 197
  ]
  edge [
    source 10
    target 198
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 199
  ]
  edge [
    source 11
    target 200
  ]
  edge [
    source 11
    target 201
  ]
  edge [
    source 11
    target 202
  ]
  edge [
    source 11
    target 203
  ]
  edge [
    source 11
    target 204
  ]
  edge [
    source 11
    target 205
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 206
  ]
  edge [
    source 12
    target 43
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 46
  ]
  edge [
    source 13
    target 207
  ]
  edge [
    source 13
    target 208
  ]
  edge [
    source 13
    target 209
  ]
  edge [
    source 13
    target 210
  ]
  edge [
    source 13
    target 211
  ]
  edge [
    source 13
    target 212
  ]
  edge [
    source 13
    target 213
  ]
  edge [
    source 13
    target 214
  ]
  edge [
    source 13
    target 215
  ]
  edge [
    source 13
    target 168
  ]
  edge [
    source 13
    target 216
  ]
  edge [
    source 13
    target 217
  ]
  edge [
    source 13
    target 218
  ]
  edge [
    source 13
    target 219
  ]
  edge [
    source 13
    target 220
  ]
  edge [
    source 13
    target 221
  ]
  edge [
    source 13
    target 222
  ]
  edge [
    source 13
    target 223
  ]
  edge [
    source 13
    target 224
  ]
  edge [
    source 13
    target 225
  ]
  edge [
    source 13
    target 226
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 227
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 14
    target 228
  ]
  edge [
    source 14
    target 229
  ]
  edge [
    source 14
    target 230
  ]
  edge [
    source 14
    target 231
  ]
  edge [
    source 14
    target 232
  ]
  edge [
    source 14
    target 233
  ]
  edge [
    source 14
    target 234
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 235
  ]
  edge [
    source 15
    target 236
  ]
  edge [
    source 15
    target 237
  ]
  edge [
    source 15
    target 53
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 238
  ]
  edge [
    source 16
    target 239
  ]
  edge [
    source 16
    target 240
  ]
  edge [
    source 16
    target 227
  ]
  edge [
    source 16
    target 241
  ]
  edge [
    source 16
    target 242
  ]
  edge [
    source 16
    target 243
  ]
  edge [
    source 16
    target 244
  ]
  edge [
    source 16
    target 245
  ]
  edge [
    source 16
    target 31
  ]
  edge [
    source 16
    target 246
  ]
  edge [
    source 16
    target 247
  ]
  edge [
    source 16
    target 248
  ]
  edge [
    source 16
    target 249
  ]
  edge [
    source 16
    target 250
  ]
  edge [
    source 16
    target 251
  ]
  edge [
    source 16
    target 252
  ]
  edge [
    source 16
    target 253
  ]
  edge [
    source 16
    target 26
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 234
  ]
  edge [
    source 18
    target 254
  ]
  edge [
    source 18
    target 255
  ]
  edge [
    source 18
    target 256
  ]
  edge [
    source 18
    target 257
  ]
  edge [
    source 18
    target 258
  ]
  edge [
    source 18
    target 259
  ]
  edge [
    source 18
    target 260
  ]
  edge [
    source 18
    target 261
  ]
  edge [
    source 18
    target 186
  ]
  edge [
    source 18
    target 262
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 263
  ]
  edge [
    source 19
    target 264
  ]
  edge [
    source 19
    target 194
  ]
  edge [
    source 19
    target 265
  ]
  edge [
    source 19
    target 266
  ]
  edge [
    source 19
    target 267
  ]
  edge [
    source 19
    target 268
  ]
  edge [
    source 19
    target 204
  ]
  edge [
    source 19
    target 269
  ]
  edge [
    source 19
    target 270
  ]
  edge [
    source 19
    target 271
  ]
  edge [
    source 19
    target 272
  ]
  edge [
    source 19
    target 273
  ]
  edge [
    source 19
    target 274
  ]
  edge [
    source 19
    target 275
  ]
  edge [
    source 19
    target 276
  ]
  edge [
    source 19
    target 277
  ]
  edge [
    source 20
    target 50
  ]
  edge [
    source 20
    target 278
  ]
  edge [
    source 20
    target 166
  ]
  edge [
    source 20
    target 279
  ]
  edge [
    source 20
    target 280
  ]
  edge [
    source 20
    target 281
  ]
  edge [
    source 20
    target 282
  ]
  edge [
    source 20
    target 283
  ]
  edge [
    source 20
    target 284
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 285
  ]
  edge [
    source 21
    target 286
  ]
  edge [
    source 21
    target 41
  ]
  edge [
    source 21
    target 52
  ]
  edge [
    source 21
    target 61
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 287
  ]
  edge [
    source 22
    target 47
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 288
  ]
  edge [
    source 23
    target 289
  ]
  edge [
    source 23
    target 290
  ]
  edge [
    source 23
    target 291
  ]
  edge [
    source 23
    target 292
  ]
  edge [
    source 23
    target 293
  ]
  edge [
    source 23
    target 294
  ]
  edge [
    source 23
    target 295
  ]
  edge [
    source 23
    target 296
  ]
  edge [
    source 23
    target 297
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 298
  ]
  edge [
    source 24
    target 299
  ]
  edge [
    source 24
    target 300
  ]
  edge [
    source 24
    target 301
  ]
  edge [
    source 24
    target 302
  ]
  edge [
    source 24
    target 184
  ]
  edge [
    source 24
    target 303
  ]
  edge [
    source 24
    target 304
  ]
  edge [
    source 24
    target 305
  ]
  edge [
    source 24
    target 306
  ]
  edge [
    source 24
    target 307
  ]
  edge [
    source 24
    target 308
  ]
  edge [
    source 24
    target 309
  ]
  edge [
    source 24
    target 310
  ]
  edge [
    source 24
    target 311
  ]
  edge [
    source 24
    target 179
  ]
  edge [
    source 24
    target 312
  ]
  edge [
    source 24
    target 313
  ]
  edge [
    source 24
    target 186
  ]
  edge [
    source 24
    target 314
  ]
  edge [
    source 24
    target 315
  ]
  edge [
    source 24
    target 316
  ]
  edge [
    source 24
    target 317
  ]
  edge [
    source 24
    target 318
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 319
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 320
  ]
  edge [
    source 26
    target 191
  ]
  edge [
    source 26
    target 321
  ]
  edge [
    source 26
    target 322
  ]
  edge [
    source 26
    target 323
  ]
  edge [
    source 26
    target 324
  ]
  edge [
    source 26
    target 325
  ]
  edge [
    source 26
    target 326
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 191
  ]
  edge [
    source 27
    target 327
  ]
  edge [
    source 27
    target 328
  ]
  edge [
    source 27
    target 329
  ]
  edge [
    source 27
    target 330
  ]
  edge [
    source 27
    target 331
  ]
  edge [
    source 27
    target 332
  ]
  edge [
    source 27
    target 333
  ]
  edge [
    source 27
    target 232
  ]
  edge [
    source 27
    target 334
  ]
  edge [
    source 27
    target 304
  ]
  edge [
    source 28
    target 335
  ]
  edge [
    source 28
    target 336
  ]
  edge [
    source 28
    target 337
  ]
  edge [
    source 28
    target 338
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 339
  ]
  edge [
    source 29
    target 168
  ]
  edge [
    source 29
    target 340
  ]
  edge [
    source 29
    target 341
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 342
  ]
  edge [
    source 30
    target 343
  ]
  edge [
    source 30
    target 344
  ]
  edge [
    source 30
    target 345
  ]
  edge [
    source 30
    target 346
  ]
  edge [
    source 30
    target 347
  ]
  edge [
    source 30
    target 348
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 349
  ]
  edge [
    source 31
    target 305
  ]
  edge [
    source 31
    target 350
  ]
  edge [
    source 31
    target 351
  ]
  edge [
    source 31
    target 352
  ]
  edge [
    source 32
    target 353
  ]
  edge [
    source 32
    target 354
  ]
  edge [
    source 32
    target 169
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 355
  ]
  edge [
    source 34
    target 356
  ]
  edge [
    source 34
    target 357
  ]
  edge [
    source 34
    target 305
  ]
  edge [
    source 34
    target 358
  ]
  edge [
    source 34
    target 359
  ]
  edge [
    source 35
    target 360
  ]
  edge [
    source 35
    target 361
  ]
  edge [
    source 35
    target 362
  ]
  edge [
    source 35
    target 363
  ]
  edge [
    source 35
    target 364
  ]
  edge [
    source 35
    target 365
  ]
  edge [
    source 35
    target 366
  ]
  edge [
    source 35
    target 367
  ]
  edge [
    source 35
    target 368
  ]
  edge [
    source 35
    target 369
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 370
  ]
  edge [
    source 36
    target 371
  ]
  edge [
    source 36
    target 372
  ]
  edge [
    source 36
    target 373
  ]
  edge [
    source 36
    target 374
  ]
  edge [
    source 36
    target 375
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 376
  ]
  edge [
    source 37
    target 377
  ]
  edge [
    source 37
    target 378
  ]
  edge [
    source 37
    target 379
  ]
  edge [
    source 37
    target 380
  ]
  edge [
    source 37
    target 381
  ]
  edge [
    source 37
    target 371
  ]
  edge [
    source 37
    target 382
  ]
  edge [
    source 37
    target 383
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 384
  ]
  edge [
    source 38
    target 385
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 386
  ]
  edge [
    source 39
    target 387
  ]
  edge [
    source 39
    target 69
  ]
  edge [
    source 39
    target 72
  ]
  edge [
    source 39
    target 388
  ]
  edge [
    source 39
    target 389
  ]
  edge [
    source 39
    target 74
  ]
  edge [
    source 39
    target 390
  ]
  edge [
    source 39
    target 76
  ]
  edge [
    source 39
    target 391
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 41
    target 52
  ]
  edge [
    source 41
    target 61
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 246
  ]
  edge [
    source 42
    target 392
  ]
  edge [
    source 42
    target 393
  ]
  edge [
    source 42
    target 394
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 63
  ]
  edge [
    source 43
    target 64
  ]
  edge [
    source 43
    target 355
  ]
  edge [
    source 43
    target 395
  ]
  edge [
    source 43
    target 396
  ]
  edge [
    source 43
    target 397
  ]
  edge [
    source 43
    target 398
  ]
  edge [
    source 43
    target 330
  ]
  edge [
    source 43
    target 399
  ]
  edge [
    source 43
    target 400
  ]
  edge [
    source 43
    target 401
  ]
  edge [
    source 43
    target 402
  ]
  edge [
    source 43
    target 403
  ]
  edge [
    source 43
    target 404
  ]
  edge [
    source 43
    target 405
  ]
  edge [
    source 43
    target 406
  ]
  edge [
    source 43
    target 407
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 166
  ]
  edge [
    source 44
    target 408
  ]
  edge [
    source 44
    target 409
  ]
  edge [
    source 44
    target 184
  ]
  edge [
    source 44
    target 410
  ]
  edge [
    source 45
    target 194
  ]
  edge [
    source 45
    target 411
  ]
  edge [
    source 45
    target 412
  ]
  edge [
    source 45
    target 413
  ]
  edge [
    source 45
    target 414
  ]
  edge [
    source 45
    target 415
  ]
  edge [
    source 45
    target 268
  ]
  edge [
    source 45
    target 416
  ]
  edge [
    source 45
    target 417
  ]
  edge [
    source 45
    target 274
  ]
  edge [
    source 45
    target 418
  ]
  edge [
    source 45
    target 419
  ]
  edge [
    source 45
    target 59
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 420
  ]
  edge [
    source 46
    target 421
  ]
  edge [
    source 46
    target 422
  ]
  edge [
    source 46
    target 423
  ]
  edge [
    source 46
    target 424
  ]
  edge [
    source 46
    target 425
  ]
  edge [
    source 46
    target 426
  ]
  edge [
    source 46
    target 427
  ]
  edge [
    source 46
    target 428
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 347
  ]
  edge [
    source 47
    target 429
  ]
  edge [
    source 47
    target 430
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 431
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 432
  ]
  edge [
    source 51
    target 433
  ]
  edge [
    source 51
    target 434
  ]
  edge [
    source 51
    target 435
  ]
  edge [
    source 51
    target 436
  ]
  edge [
    source 51
    target 437
  ]
  edge [
    source 51
    target 438
  ]
  edge [
    source 51
    target 439
  ]
  edge [
    source 51
    target 440
  ]
  edge [
    source 51
    target 441
  ]
  edge [
    source 51
    target 442
  ]
  edge [
    source 51
    target 443
  ]
  edge [
    source 51
    target 444
  ]
  edge [
    source 51
    target 445
  ]
  edge [
    source 51
    target 446
  ]
  edge [
    source 51
    target 447
  ]
  edge [
    source 51
    target 448
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 61
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 449
  ]
  edge [
    source 54
    target 166
  ]
  edge [
    source 54
    target 450
  ]
  edge [
    source 54
    target 451
  ]
  edge [
    source 54
    target 452
  ]
  edge [
    source 54
    target 453
  ]
  edge [
    source 54
    target 454
  ]
  edge [
    source 54
    target 455
  ]
  edge [
    source 54
    target 456
  ]
  edge [
    source 54
    target 457
  ]
  edge [
    source 54
    target 458
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 459
  ]
  edge [
    source 61
    target 460
  ]
  edge [
    source 61
    target 461
  ]
  edge [
    source 61
    target 462
  ]
  edge [
    source 61
    target 463
  ]
  edge [
    source 61
    target 464
  ]
  edge [
    source 61
    target 465
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 466
  ]
  edge [
    source 65
    target 467
  ]
  edge [
    source 65
    target 468
  ]
  edge [
    source 65
    target 469
  ]
  edge [
    source 65
    target 470
  ]
  edge [
    source 65
    target 471
  ]
  edge [
    source 65
    target 472
  ]
  edge [
    source 65
    target 473
  ]
  edge [
    source 65
    target 474
  ]
  edge [
    source 65
    target 475
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 66
    target 476
  ]
  edge [
    source 66
    target 477
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 67
    target 478
  ]
  edge [
    source 67
    target 340
  ]
  edge [
    source 67
    target 73
  ]
  edge [
    source 67
    target 479
  ]
  edge [
    source 67
    target 480
  ]
  edge [
    source 68
    target 481
  ]
  edge [
    source 68
    target 482
  ]
  edge [
    source 68
    target 483
  ]
  edge [
    source 68
    target 484
  ]
  edge [
    source 68
    target 485
  ]
  edge [
    source 68
    target 82
  ]
  edge [
    source 68
    target 486
  ]
  edge [
    source 68
    target 340
  ]
  edge [
    source 68
    target 487
  ]
  edge [
    source 68
    target 488
  ]
  edge [
    source 68
    target 489
  ]
  edge [
    source 68
    target 490
  ]
  edge [
    source 68
    target 262
  ]
  edge [
    source 68
    target 491
  ]
  edge [
    source 68
    target 506
  ]
  edge [
    source 493
    target 494
  ]
  edge [
    source 493
    target 505
  ]
  edge [
    source 495
    target 496
  ]
  edge [
    source 497
    target 498
  ]
  edge [
    source 499
    target 500
  ]
  edge [
    source 501
    target 502
  ]
  edge [
    source 503
    target 504
  ]
  edge [
    source 507
    target 508
  ]
]
