graph [
  maxDegree 8
  minDegree 1
  meanDegree 1.48
  density 0.030204081632653063
  graphCliqueNumber 3
  node [
    id 0
    label "kariera"
    origin "text"
  ]
  node [
    id 1
    label "klubowy"
    origin "text"
  ]
  node [
    id 2
    label "pozycja_spo&#322;eczna"
  ]
  node [
    id 3
    label "awansowanie"
  ]
  node [
    id 4
    label "awansowa&#263;"
  ]
  node [
    id 5
    label "degradacja"
  ]
  node [
    id 6
    label "przebieg"
  ]
  node [
    id 7
    label "rozw&#243;j"
  ]
  node [
    id 8
    label "awans"
  ]
  node [
    id 9
    label "klubowo"
  ]
  node [
    id 10
    label "rozrywkowy"
  ]
  node [
    id 11
    label "Nigerdock"
  ]
  node [
    id 12
    label "soccer"
  ]
  node [
    id 13
    label "Academy"
  ]
  node [
    id 14
    label "Maurice"
  ]
  node [
    id 15
    label "Cooreman"
  ]
  node [
    id 16
    label "Gabros"
  ]
  node [
    id 17
    label "international"
  ]
  node [
    id 18
    label "premiera"
  ]
  node [
    id 19
    label "League"
  ]
  node [
    id 20
    label "Lobi"
  ]
  node [
    id 21
    label "Stars"
  ]
  node [
    id 22
    label "Olympique"
  ]
  node [
    id 23
    label "marsylia"
  ]
  node [
    id 24
    label "Bixente"
  ]
  node [
    id 25
    label "Lizarazu"
  ]
  node [
    id 26
    label "Ligue"
  ]
  node [
    id 27
    label "1"
  ]
  node [
    id 28
    label "RC"
  ]
  node [
    id 29
    label "Lens"
  ]
  node [
    id 30
    label "puchar"
  ]
  node [
    id 31
    label "Intertoto"
  ]
  node [
    id 32
    label "Lyon"
  ]
  node [
    id 33
    label "UEFA"
  ]
  node [
    id 34
    label "SC"
  ]
  node [
    id 35
    label "Heerenveen"
  ]
  node [
    id 36
    label "Inter"
  ]
  node [
    id 37
    label "Mediolan"
  ]
  node [
    id 38
    label "liga"
  ]
  node [
    id 39
    label "francuski"
  ]
  node [
    id 40
    label "Taye"
  ]
  node [
    id 41
    label "Taiwo"
  ]
  node [
    id 42
    label "m&#322;odzie&#380;owy"
  ]
  node [
    id 43
    label "mistrzostwo"
  ]
  node [
    id 44
    label "&#347;wiat"
  ]
  node [
    id 45
    label "Lionelem"
  ]
  node [
    id 46
    label "Messim"
  ]
  node [
    id 47
    label "John"
  ]
  node [
    id 48
    label "obi"
  ]
  node [
    id 49
    label "Mikelem"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 13
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 32
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 33
  ]
  edge [
    source 30
    target 38
  ]
  edge [
    source 30
    target 39
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 44
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 49
  ]
  edge [
    source 48
    target 49
  ]
]
