graph [
  maxDegree 12
  minDegree 1
  meanDegree 1.9375
  density 0.0625
  graphCliqueNumber 2
  node [
    id 0
    label "podstawowy"
    origin "text"
  ]
  node [
    id 1
    label "udzia&#322;owiec"
    origin "text"
  ]
  node [
    id 2
    label "przedsi&#281;wzi&#281;cie"
    origin "text"
  ]
  node [
    id 3
    label "informatyczny"
    origin "text"
  ]
  node [
    id 4
    label "by&#263;"
    origin "text"
  ]
  node [
    id 5
    label "pocz&#261;tkowy"
  ]
  node [
    id 6
    label "podstawowo"
  ]
  node [
    id 7
    label "najwa&#380;niejszy"
  ]
  node [
    id 8
    label "niezaawansowany"
  ]
  node [
    id 9
    label "gwarectwo"
  ]
  node [
    id 10
    label "transza_otwarta"
  ]
  node [
    id 11
    label "akcjonariat"
  ]
  node [
    id 12
    label "walne_zgromadzenie"
  ]
  node [
    id 13
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 14
    label "wsp&#243;&#322;w&#322;a&#347;ciciel"
  ]
  node [
    id 15
    label "zrobienie"
  ]
  node [
    id 16
    label "consumption"
  ]
  node [
    id 17
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 18
    label "zacz&#281;cie"
  ]
  node [
    id 19
    label "startup"
  ]
  node [
    id 20
    label "plan"
  ]
  node [
    id 21
    label "si&#281;ga&#263;"
  ]
  node [
    id 22
    label "trwa&#263;"
  ]
  node [
    id 23
    label "obecno&#347;&#263;"
  ]
  node [
    id 24
    label "stan"
  ]
  node [
    id 25
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 26
    label "stand"
  ]
  node [
    id 27
    label "mie&#263;_miejsce"
  ]
  node [
    id 28
    label "uczestniczy&#263;"
  ]
  node [
    id 29
    label "chodzi&#263;"
  ]
  node [
    id 30
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 31
    label "equal"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
]
