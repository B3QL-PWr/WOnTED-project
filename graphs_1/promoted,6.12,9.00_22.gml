graph [
  maxDegree 46
  minDegree 1
  meanDegree 2
  density 0.016
  graphCliqueNumber 2
  node [
    id 0
    label "&#380;ycie"
    origin "text"
  ]
  node [
    id 1
    label "pisa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "niesamowity"
    origin "text"
  ]
  node [
    id 3
    label "historia"
    origin "text"
  ]
  node [
    id 4
    label "czego"
    origin "text"
  ]
  node [
    id 5
    label "przyk&#322;ad"
    origin "text"
  ]
  node [
    id 6
    label "by&#263;"
    origin "text"
  ]
  node [
    id 7
    label "poni&#380;szy"
    origin "text"
  ]
  node [
    id 8
    label "energy"
  ]
  node [
    id 9
    label "czas"
  ]
  node [
    id 10
    label "bycie"
  ]
  node [
    id 11
    label "zegar_biologiczny"
  ]
  node [
    id 12
    label "okres_noworodkowy"
  ]
  node [
    id 13
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 14
    label "entity"
  ]
  node [
    id 15
    label "prze&#380;ywanie"
  ]
  node [
    id 16
    label "prze&#380;ycie"
  ]
  node [
    id 17
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 18
    label "wiek_matuzalemowy"
  ]
  node [
    id 19
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 20
    label "dzieci&#324;stwo"
  ]
  node [
    id 21
    label "power"
  ]
  node [
    id 22
    label "szwung"
  ]
  node [
    id 23
    label "menopauza"
  ]
  node [
    id 24
    label "umarcie"
  ]
  node [
    id 25
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 26
    label "life"
  ]
  node [
    id 27
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 28
    label "&#380;ywy"
  ]
  node [
    id 29
    label "rozw&#243;j"
  ]
  node [
    id 30
    label "po&#322;&#243;g"
  ]
  node [
    id 31
    label "byt"
  ]
  node [
    id 32
    label "przebywanie"
  ]
  node [
    id 33
    label "subsistence"
  ]
  node [
    id 34
    label "koleje_losu"
  ]
  node [
    id 35
    label "raj_utracony"
  ]
  node [
    id 36
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 37
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 38
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 39
    label "andropauza"
  ]
  node [
    id 40
    label "warunki"
  ]
  node [
    id 41
    label "do&#380;ywanie"
  ]
  node [
    id 42
    label "niemowl&#281;ctwo"
  ]
  node [
    id 43
    label "umieranie"
  ]
  node [
    id 44
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 45
    label "staro&#347;&#263;"
  ]
  node [
    id 46
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 47
    label "&#347;mier&#263;"
  ]
  node [
    id 48
    label "ozdabia&#263;"
  ]
  node [
    id 49
    label "dysgrafia"
  ]
  node [
    id 50
    label "prasa"
  ]
  node [
    id 51
    label "spell"
  ]
  node [
    id 52
    label "skryba"
  ]
  node [
    id 53
    label "donosi&#263;"
  ]
  node [
    id 54
    label "code"
  ]
  node [
    id 55
    label "tekst"
  ]
  node [
    id 56
    label "dysortografia"
  ]
  node [
    id 57
    label "read"
  ]
  node [
    id 58
    label "tworzy&#263;"
  ]
  node [
    id 59
    label "formu&#322;owa&#263;"
  ]
  node [
    id 60
    label "styl"
  ]
  node [
    id 61
    label "stawia&#263;"
  ]
  node [
    id 62
    label "niesamowicie"
  ]
  node [
    id 63
    label "niezwyk&#322;y"
  ]
  node [
    id 64
    label "report"
  ]
  node [
    id 65
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 66
    label "wypowied&#378;"
  ]
  node [
    id 67
    label "neografia"
  ]
  node [
    id 68
    label "przedmiot"
  ]
  node [
    id 69
    label "papirologia"
  ]
  node [
    id 70
    label "historia_gospodarcza"
  ]
  node [
    id 71
    label "przebiec"
  ]
  node [
    id 72
    label "hista"
  ]
  node [
    id 73
    label "nauka_humanistyczna"
  ]
  node [
    id 74
    label "filigranistyka"
  ]
  node [
    id 75
    label "dyplomatyka"
  ]
  node [
    id 76
    label "annalistyka"
  ]
  node [
    id 77
    label "historyka"
  ]
  node [
    id 78
    label "heraldyka"
  ]
  node [
    id 79
    label "fabu&#322;a"
  ]
  node [
    id 80
    label "muzealnictwo"
  ]
  node [
    id 81
    label "varsavianistyka"
  ]
  node [
    id 82
    label "mediewistyka"
  ]
  node [
    id 83
    label "prezentyzm"
  ]
  node [
    id 84
    label "przebiegni&#281;cie"
  ]
  node [
    id 85
    label "charakter"
  ]
  node [
    id 86
    label "paleografia"
  ]
  node [
    id 87
    label "genealogia"
  ]
  node [
    id 88
    label "czynno&#347;&#263;"
  ]
  node [
    id 89
    label "prozopografia"
  ]
  node [
    id 90
    label "motyw"
  ]
  node [
    id 91
    label "nautologia"
  ]
  node [
    id 92
    label "epoka"
  ]
  node [
    id 93
    label "numizmatyka"
  ]
  node [
    id 94
    label "ruralistyka"
  ]
  node [
    id 95
    label "&#378;r&#243;d&#322;oznawstwo"
  ]
  node [
    id 96
    label "epigrafika"
  ]
  node [
    id 97
    label "historiografia"
  ]
  node [
    id 98
    label "bizantynistyka"
  ]
  node [
    id 99
    label "weksylologia"
  ]
  node [
    id 100
    label "kierunek"
  ]
  node [
    id 101
    label "ikonografia"
  ]
  node [
    id 102
    label "chronologia"
  ]
  node [
    id 103
    label "archiwistyka"
  ]
  node [
    id 104
    label "sfragistyka"
  ]
  node [
    id 105
    label "zabytkoznawstwo"
  ]
  node [
    id 106
    label "historia_sztuki"
  ]
  node [
    id 107
    label "cz&#322;owiek"
  ]
  node [
    id 108
    label "czyn"
  ]
  node [
    id 109
    label "przedstawiciel"
  ]
  node [
    id 110
    label "ilustracja"
  ]
  node [
    id 111
    label "fakt"
  ]
  node [
    id 112
    label "si&#281;ga&#263;"
  ]
  node [
    id 113
    label "trwa&#263;"
  ]
  node [
    id 114
    label "obecno&#347;&#263;"
  ]
  node [
    id 115
    label "stan"
  ]
  node [
    id 116
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 117
    label "stand"
  ]
  node [
    id 118
    label "mie&#263;_miejsce"
  ]
  node [
    id 119
    label "uczestniczy&#263;"
  ]
  node [
    id 120
    label "chodzi&#263;"
  ]
  node [
    id 121
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 122
    label "equal"
  ]
  node [
    id 123
    label "ten"
  ]
  node [
    id 124
    label "kolejny"
  ]
  node [
    id 125
    label "poni&#380;ej"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 7
    target 123
  ]
  edge [
    source 7
    target 124
  ]
  edge [
    source 7
    target 125
  ]
]
