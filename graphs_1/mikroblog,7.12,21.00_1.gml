graph [
  maxDegree 22
  minDegree 1
  meanDegree 1.9555555555555555
  density 0.044444444444444446
  graphCliqueNumber 2
  node [
    id 0
    label "robi&#263;"
    origin "text"
  ]
  node [
    id 1
    label "kolejny"
    origin "text"
  ]
  node [
    id 2
    label "rozdajo"
    origin "text"
  ]
  node [
    id 3
    label "zgarn&#261;&#263;"
    origin "text"
  ]
  node [
    id 4
    label "zapalniczka"
    origin "text"
  ]
  node [
    id 5
    label "elektryczny"
    origin "text"
  ]
  node [
    id 6
    label "davidoff"
    origin "text"
  ]
  node [
    id 7
    label "tentegowa&#263;"
  ]
  node [
    id 8
    label "urz&#261;dza&#263;"
  ]
  node [
    id 9
    label "give"
  ]
  node [
    id 10
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 11
    label "czyni&#263;"
  ]
  node [
    id 12
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 13
    label "post&#281;powa&#263;"
  ]
  node [
    id 14
    label "wydala&#263;"
  ]
  node [
    id 15
    label "oszukiwa&#263;"
  ]
  node [
    id 16
    label "organizowa&#263;"
  ]
  node [
    id 17
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 18
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 19
    label "work"
  ]
  node [
    id 20
    label "przerabia&#263;"
  ]
  node [
    id 21
    label "stylizowa&#263;"
  ]
  node [
    id 22
    label "falowa&#263;"
  ]
  node [
    id 23
    label "act"
  ]
  node [
    id 24
    label "peddle"
  ]
  node [
    id 25
    label "ukazywa&#263;"
  ]
  node [
    id 26
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 27
    label "praca"
  ]
  node [
    id 28
    label "inny"
  ]
  node [
    id 29
    label "nast&#281;pnie"
  ]
  node [
    id 30
    label "kt&#243;ry&#347;"
  ]
  node [
    id 31
    label "kolejno"
  ]
  node [
    id 32
    label "nastopny"
  ]
  node [
    id 33
    label "zaaresztowa&#263;"
  ]
  node [
    id 34
    label "ukra&#347;&#263;"
  ]
  node [
    id 35
    label "oddali&#263;"
  ]
  node [
    id 36
    label "pozyska&#263;"
  ]
  node [
    id 37
    label "zgromadzi&#263;"
  ]
  node [
    id 38
    label "porwa&#263;"
  ]
  node [
    id 39
    label "take"
  ]
  node [
    id 40
    label "lighter"
  ]
  node [
    id 41
    label "ogie&#324;"
  ]
  node [
    id 42
    label "przyrz&#261;d"
  ]
  node [
    id 43
    label "trafika"
  ]
  node [
    id 44
    label "elektrycznie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 44
  ]
]
