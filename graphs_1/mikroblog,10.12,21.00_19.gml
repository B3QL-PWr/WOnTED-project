graph [
  maxDegree 20
  minDegree 1
  meanDegree 1.935483870967742
  density 0.06451612903225806
  graphCliqueNumber 2
  node [
    id 0
    label "widzie&#263;"
    origin "text"
  ]
  node [
    id 1
    label "wykop"
    origin "text"
  ]
  node [
    id 2
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 3
    label "perceive"
  ]
  node [
    id 4
    label "reagowa&#263;"
  ]
  node [
    id 5
    label "spowodowa&#263;"
  ]
  node [
    id 6
    label "male&#263;"
  ]
  node [
    id 7
    label "zmale&#263;"
  ]
  node [
    id 8
    label "spotka&#263;"
  ]
  node [
    id 9
    label "go_steady"
  ]
  node [
    id 10
    label "dostrzega&#263;"
  ]
  node [
    id 11
    label "zwi&#281;ksza&#263;_si&#281;"
  ]
  node [
    id 12
    label "zdawa&#263;_sobie_spraw&#281;"
  ]
  node [
    id 13
    label "ogl&#261;da&#263;"
  ]
  node [
    id 14
    label "os&#261;dza&#263;"
  ]
  node [
    id 15
    label "aprobowa&#263;"
  ]
  node [
    id 16
    label "punkt_widzenia"
  ]
  node [
    id 17
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 18
    label "wzrok"
  ]
  node [
    id 19
    label "postrzega&#263;"
  ]
  node [
    id 20
    label "notice"
  ]
  node [
    id 21
    label "odwa&#322;"
  ]
  node [
    id 22
    label "chody"
  ]
  node [
    id 23
    label "grodzisko"
  ]
  node [
    id 24
    label "budowa"
  ]
  node [
    id 25
    label "kopniak"
  ]
  node [
    id 26
    label "wyrobisko"
  ]
  node [
    id 27
    label "zrzutowy"
  ]
  node [
    id 28
    label "szaniec"
  ]
  node [
    id 29
    label "odk&#322;ad"
  ]
  node [
    id 30
    label "wg&#322;&#281;bienie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
]
