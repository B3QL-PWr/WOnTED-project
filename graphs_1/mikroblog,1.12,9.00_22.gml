graph [
  maxDegree 11
  minDegree 1
  meanDegree 1.9272727272727272
  density 0.03569023569023569
  graphCliqueNumber 2
  node [
    id 0
    label "jezus"
    origin "text"
  ]
  node [
    id 1
    label "uwielbia&#263;"
    origin "text"
  ]
  node [
    id 2
    label "smaczek"
    origin "text"
  ]
  node [
    id 3
    label "kaczor"
    origin "text"
  ]
  node [
    id 4
    label "donald"
    origin "text"
  ]
  node [
    id 5
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 6
    label "zwraca&#263;"
    origin "text"
  ]
  node [
    id 7
    label "si&#281;"
    origin "text"
  ]
  node [
    id 8
    label "uwaga"
    origin "text"
  ]
  node [
    id 9
    label "dzieciak"
    origin "text"
  ]
  node [
    id 10
    label "teraz"
    origin "text"
  ]
  node [
    id 11
    label "bawi&#263;"
    origin "text"
  ]
  node [
    id 12
    label "&#322;za"
    origin "text"
  ]
  node [
    id 13
    label "love"
  ]
  node [
    id 14
    label "w&#347;cieka&#263;_si&#281;"
  ]
  node [
    id 15
    label "lubi&#263;"
  ]
  node [
    id 16
    label "nieprzyzwoito&#347;&#263;"
  ]
  node [
    id 17
    label "cecha"
  ]
  node [
    id 18
    label "kaczka"
  ]
  node [
    id 19
    label "samiec"
  ]
  node [
    id 20
    label "ustawia&#263;"
  ]
  node [
    id 21
    label "wydala&#263;"
  ]
  node [
    id 22
    label "give"
  ]
  node [
    id 23
    label "manipulate"
  ]
  node [
    id 24
    label "indicate"
  ]
  node [
    id 25
    label "przeznacza&#263;"
  ]
  node [
    id 26
    label "haftowa&#263;"
  ]
  node [
    id 27
    label "przekazywa&#263;"
  ]
  node [
    id 28
    label "nagana"
  ]
  node [
    id 29
    label "wypowied&#378;"
  ]
  node [
    id 30
    label "stan"
  ]
  node [
    id 31
    label "dzienniczek"
  ]
  node [
    id 32
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 33
    label "wzgl&#261;d"
  ]
  node [
    id 34
    label "gossip"
  ]
  node [
    id 35
    label "upomnienie"
  ]
  node [
    id 36
    label "tekst"
  ]
  node [
    id 37
    label "naiwniak"
  ]
  node [
    id 38
    label "dziecinny"
  ]
  node [
    id 39
    label "dziecko"
  ]
  node [
    id 40
    label "bech"
  ]
  node [
    id 41
    label "chwila"
  ]
  node [
    id 42
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 43
    label "ubawia&#263;"
  ]
  node [
    id 44
    label "amuse"
  ]
  node [
    id 45
    label "zajmowa&#263;"
  ]
  node [
    id 46
    label "wzbudza&#263;"
  ]
  node [
    id 47
    label "przebywa&#263;"
  ]
  node [
    id 48
    label "sprawia&#263;"
  ]
  node [
    id 49
    label "zabawia&#263;"
  ]
  node [
    id 50
    label "play"
  ]
  node [
    id 51
    label "wydzielina"
  ]
  node [
    id 52
    label "&#347;loza"
  ]
  node [
    id 53
    label "Kaczor"
  ]
  node [
    id 54
    label "Donald"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 20
  ]
  edge [
    source 6
    target 21
  ]
  edge [
    source 6
    target 22
  ]
  edge [
    source 6
    target 23
  ]
  edge [
    source 6
    target 24
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 28
  ]
  edge [
    source 8
    target 29
  ]
  edge [
    source 8
    target 30
  ]
  edge [
    source 8
    target 31
  ]
  edge [
    source 8
    target 32
  ]
  edge [
    source 8
    target 33
  ]
  edge [
    source 8
    target 34
  ]
  edge [
    source 8
    target 35
  ]
  edge [
    source 8
    target 36
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 37
  ]
  edge [
    source 9
    target 38
  ]
  edge [
    source 9
    target 39
  ]
  edge [
    source 9
    target 40
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 41
  ]
  edge [
    source 10
    target 42
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 43
  ]
  edge [
    source 11
    target 44
  ]
  edge [
    source 11
    target 45
  ]
  edge [
    source 11
    target 46
  ]
  edge [
    source 11
    target 47
  ]
  edge [
    source 11
    target 48
  ]
  edge [
    source 11
    target 49
  ]
  edge [
    source 11
    target 50
  ]
  edge [
    source 12
    target 51
  ]
  edge [
    source 12
    target 52
  ]
  edge [
    source 53
    target 54
  ]
]
