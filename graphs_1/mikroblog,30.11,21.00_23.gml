graph [
  maxDegree 29
  minDegree 1
  meanDegree 2.049586776859504
  density 0.017079889807162536
  graphCliqueNumber 3
  node [
    id 0
    label "dobra"
    origin "text"
  ]
  node [
    id 1
    label "mirka"
    origin "text"
  ]
  node [
    id 2
    label "jaki&#347;"
    origin "text"
  ]
  node [
    id 3
    label "lato"
    origin "text"
  ]
  node [
    id 4
    label "nieudolnie"
    origin "text"
  ]
  node [
    id 5
    label "pr&#243;bowa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 7
    label "raper"
    origin "text"
  ]
  node [
    id 8
    label "pseudonim"
    origin "text"
  ]
  node [
    id 9
    label "prowadzi&#263;"
    origin "text"
  ]
  node [
    id 10
    label "zeszyt"
    origin "text"
  ]
  node [
    id 11
    label "rym"
    origin "text"
  ]
  node [
    id 12
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 13
    label "zapisowa&#263;"
    origin "text"
  ]
  node [
    id 14
    label "mia&#322;"
    origin "text"
  ]
  node [
    id 15
    label "by&#263;"
    origin "text"
  ]
  node [
    id 16
    label "u&#380;yty"
    origin "text"
  ]
  node [
    id 17
    label "moi"
    origin "text"
  ]
  node [
    id 18
    label "kawa&#322;ek"
    origin "text"
  ]
  node [
    id 19
    label "frymark"
  ]
  node [
    id 20
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 21
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 22
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 23
    label "commodity"
  ]
  node [
    id 24
    label "mienie"
  ]
  node [
    id 25
    label "Wilko"
  ]
  node [
    id 26
    label "jednostka_monetarna"
  ]
  node [
    id 27
    label "centym"
  ]
  node [
    id 28
    label "jako&#347;"
  ]
  node [
    id 29
    label "charakterystyczny"
  ]
  node [
    id 30
    label "ciekawy"
  ]
  node [
    id 31
    label "jako_tako"
  ]
  node [
    id 32
    label "dziwny"
  ]
  node [
    id 33
    label "niez&#322;y"
  ]
  node [
    id 34
    label "przyzwoity"
  ]
  node [
    id 35
    label "pora_roku"
  ]
  node [
    id 36
    label "nieudanie"
  ]
  node [
    id 37
    label "nieudolny"
  ]
  node [
    id 38
    label "przygotowywa&#263;_si&#281;"
  ]
  node [
    id 39
    label "feel"
  ]
  node [
    id 40
    label "przedstawienie"
  ]
  node [
    id 41
    label "try"
  ]
  node [
    id 42
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 43
    label "pos&#322;ugiwa&#263;_si&#281;"
  ]
  node [
    id 44
    label "spo&#380;ywa&#263;"
  ]
  node [
    id 45
    label "sprawdza&#263;"
  ]
  node [
    id 46
    label "stara&#263;_si&#281;"
  ]
  node [
    id 47
    label "kosztowa&#263;"
  ]
  node [
    id 48
    label "proceed"
  ]
  node [
    id 49
    label "catch"
  ]
  node [
    id 50
    label "pozosta&#263;"
  ]
  node [
    id 51
    label "osta&#263;_si&#281;"
  ]
  node [
    id 52
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 53
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 54
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 55
    label "change"
  ]
  node [
    id 56
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 57
    label "muzyk"
  ]
  node [
    id 58
    label "nazwa_w&#322;asna"
  ]
  node [
    id 59
    label "control"
  ]
  node [
    id 60
    label "eksponowa&#263;"
  ]
  node [
    id 61
    label "kre&#347;li&#263;"
  ]
  node [
    id 62
    label "g&#243;rowa&#263;"
  ]
  node [
    id 63
    label "message"
  ]
  node [
    id 64
    label "partner"
  ]
  node [
    id 65
    label "string"
  ]
  node [
    id 66
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 67
    label "przesuwa&#263;"
  ]
  node [
    id 68
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 69
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 70
    label "powodowa&#263;"
  ]
  node [
    id 71
    label "kierowa&#263;"
  ]
  node [
    id 72
    label "robi&#263;"
  ]
  node [
    id 73
    label "manipulate"
  ]
  node [
    id 74
    label "&#380;y&#263;"
  ]
  node [
    id 75
    label "navigate"
  ]
  node [
    id 76
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 77
    label "ukierunkowywa&#263;"
  ]
  node [
    id 78
    label "linia_melodyczna"
  ]
  node [
    id 79
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 80
    label "prowadzenie"
  ]
  node [
    id 81
    label "tworzy&#263;"
  ]
  node [
    id 82
    label "przewy&#380;sza&#263;"
  ]
  node [
    id 83
    label "sterowa&#263;"
  ]
  node [
    id 84
    label "krzywa"
  ]
  node [
    id 85
    label "ok&#322;adka"
  ]
  node [
    id 86
    label "impression"
  ]
  node [
    id 87
    label "wydanie"
  ]
  node [
    id 88
    label "kartka"
  ]
  node [
    id 89
    label "wydawnictwo"
  ]
  node [
    id 90
    label "wk&#322;ad"
  ]
  node [
    id 91
    label "artyku&#322;"
  ]
  node [
    id 92
    label "kajet"
  ]
  node [
    id 93
    label "czasopismo"
  ]
  node [
    id 94
    label "egzemplarz"
  ]
  node [
    id 95
    label "publikacja"
  ]
  node [
    id 96
    label "rhyme"
  ]
  node [
    id 97
    label "wiersz"
  ]
  node [
    id 98
    label "instrumentacja_g&#322;oskowa"
  ]
  node [
    id 99
    label "wsp&#243;&#322;brzmienie"
  ]
  node [
    id 100
    label "proszek"
  ]
  node [
    id 101
    label "si&#281;ga&#263;"
  ]
  node [
    id 102
    label "trwa&#263;"
  ]
  node [
    id 103
    label "obecno&#347;&#263;"
  ]
  node [
    id 104
    label "stan"
  ]
  node [
    id 105
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 106
    label "stand"
  ]
  node [
    id 107
    label "mie&#263;_miejsce"
  ]
  node [
    id 108
    label "uczestniczy&#263;"
  ]
  node [
    id 109
    label "chodzi&#263;"
  ]
  node [
    id 110
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 111
    label "equal"
  ]
  node [
    id 112
    label "podp&#322;ywa&#263;"
  ]
  node [
    id 113
    label "podp&#322;ywanie"
  ]
  node [
    id 114
    label "plot"
  ]
  node [
    id 115
    label "podp&#322;yn&#261;&#263;"
  ]
  node [
    id 116
    label "piece"
  ]
  node [
    id 117
    label "kawa&#322;"
  ]
  node [
    id 118
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 119
    label "utw&#243;r"
  ]
  node [
    id 120
    label "podp&#322;yni&#281;cie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 10
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 42
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 13
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 11
    target 98
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 100
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 101
  ]
  edge [
    source 15
    target 102
  ]
  edge [
    source 15
    target 103
  ]
  edge [
    source 15
    target 104
  ]
  edge [
    source 15
    target 105
  ]
  edge [
    source 15
    target 106
  ]
  edge [
    source 15
    target 107
  ]
  edge [
    source 15
    target 108
  ]
  edge [
    source 15
    target 109
  ]
  edge [
    source 15
    target 110
  ]
  edge [
    source 15
    target 111
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 112
  ]
  edge [
    source 18
    target 113
  ]
  edge [
    source 18
    target 114
  ]
  edge [
    source 18
    target 115
  ]
  edge [
    source 18
    target 116
  ]
  edge [
    source 18
    target 117
  ]
  edge [
    source 18
    target 118
  ]
  edge [
    source 18
    target 119
  ]
  edge [
    source 18
    target 120
  ]
]
