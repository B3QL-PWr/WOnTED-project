graph [
  maxDegree 12
  minDegree 1
  meanDegree 2.210526315789474
  density 0.059743954480796585
  graphCliqueNumber 4
  node [
    id 0
    label "aleja"
    origin "text"
  ]
  node [
    id 1
    label "unia"
    origin "text"
  ]
  node [
    id 2
    label "lubelski"
    origin "text"
  ]
  node [
    id 3
    label "lublin"
    origin "text"
  ]
  node [
    id 4
    label "chodnik"
  ]
  node [
    id 5
    label "ulica"
  ]
  node [
    id 6
    label "deptak"
  ]
  node [
    id 7
    label "uk&#322;ad"
  ]
  node [
    id 8
    label "partia"
  ]
  node [
    id 9
    label "organizacja"
  ]
  node [
    id 10
    label "Unia_Wolno&#347;ci"
  ]
  node [
    id 11
    label "Unia_Europejska"
  ]
  node [
    id 12
    label "combination"
  ]
  node [
    id 13
    label "union"
  ]
  node [
    id 14
    label "Unia"
  ]
  node [
    id 15
    label "lubelak"
  ]
  node [
    id 16
    label "cebularz"
  ]
  node [
    id 17
    label "regionalny"
  ]
  node [
    id 18
    label "polski"
  ]
  node [
    id 19
    label "par&#243;wka"
  ]
  node [
    id 20
    label "po_lubelsku"
  ]
  node [
    id 21
    label "rondo"
  ]
  node [
    id 22
    label "lipiec"
  ]
  node [
    id 23
    label "80"
  ]
  node [
    id 24
    label "Romana"
  ]
  node [
    id 25
    label "Dmowski"
  ]
  node [
    id 26
    label "kr&#243;lestwo"
  ]
  node [
    id 27
    label "polskie"
  ]
  node [
    id 28
    label "wielki"
  ]
  node [
    id 29
    label "ksi&#281;stwo"
  ]
  node [
    id 30
    label "litewski"
  ]
  node [
    id 31
    label "zamek"
  ]
  node [
    id 32
    label "wyspa"
  ]
  node [
    id 33
    label "W&#322;adys&#322;awa"
  ]
  node [
    id 34
    label "Gomu&#322;ka"
  ]
  node [
    id 35
    label "MPK"
  ]
  node [
    id 36
    label "szpital"
  ]
  node [
    id 37
    label "kliniczny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 23
  ]
  edge [
    source 21
    target 24
  ]
  edge [
    source 21
    target 25
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 30
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 36
    target 37
  ]
]
