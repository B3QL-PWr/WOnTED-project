graph [
  maxDegree 26
  minDegree 1
  meanDegree 2
  density 0.03076923076923077
  graphCliqueNumber 3
  node [
    id 0
    label "pr&#243;bowa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "opatentowa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "praca"
    origin "text"
  ]
  node [
    id 3
    label "aplikant"
    origin "text"
  ]
  node [
    id 4
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 5
    label "rozmawia&#263;"
    origin "text"
  ]
  node [
    id 6
    label "sw&#243;j"
    origin "text"
  ]
  node [
    id 7
    label "w&#322;asny"
    origin "text"
  ]
  node [
    id 8
    label "technologia"
    origin "text"
  ]
  node [
    id 9
    label "przygotowywa&#263;_si&#281;"
  ]
  node [
    id 10
    label "feel"
  ]
  node [
    id 11
    label "przedstawienie"
  ]
  node [
    id 12
    label "try"
  ]
  node [
    id 13
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 14
    label "pos&#322;ugiwa&#263;_si&#281;"
  ]
  node [
    id 15
    label "spo&#380;ywa&#263;"
  ]
  node [
    id 16
    label "sprawdza&#263;"
  ]
  node [
    id 17
    label "stara&#263;_si&#281;"
  ]
  node [
    id 18
    label "kosztowa&#263;"
  ]
  node [
    id 19
    label "zastrzec"
  ]
  node [
    id 20
    label "patent"
  ]
  node [
    id 21
    label "stosunek_pracy"
  ]
  node [
    id 22
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 23
    label "benedykty&#324;ski"
  ]
  node [
    id 24
    label "pracowanie"
  ]
  node [
    id 25
    label "zaw&#243;d"
  ]
  node [
    id 26
    label "kierownictwo"
  ]
  node [
    id 27
    label "zmiana"
  ]
  node [
    id 28
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 29
    label "wytw&#243;r"
  ]
  node [
    id 30
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 31
    label "tynkarski"
  ]
  node [
    id 32
    label "czynnik_produkcji"
  ]
  node [
    id 33
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 34
    label "zobowi&#261;zanie"
  ]
  node [
    id 35
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 36
    label "czynno&#347;&#263;"
  ]
  node [
    id 37
    label "tyrka"
  ]
  node [
    id 38
    label "pracowa&#263;"
  ]
  node [
    id 39
    label "siedziba"
  ]
  node [
    id 40
    label "poda&#380;_pracy"
  ]
  node [
    id 41
    label "miejsce"
  ]
  node [
    id 42
    label "zak&#322;ad"
  ]
  node [
    id 43
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 44
    label "najem"
  ]
  node [
    id 45
    label "absolwent"
  ]
  node [
    id 46
    label "sta&#380;ysta"
  ]
  node [
    id 47
    label "prawnik"
  ]
  node [
    id 48
    label "talk"
  ]
  node [
    id 49
    label "gaworzy&#263;"
  ]
  node [
    id 50
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 51
    label "cz&#322;owiek"
  ]
  node [
    id 52
    label "bli&#378;ni"
  ]
  node [
    id 53
    label "odpowiedni"
  ]
  node [
    id 54
    label "swojak"
  ]
  node [
    id 55
    label "samodzielny"
  ]
  node [
    id 56
    label "swoisty"
  ]
  node [
    id 57
    label "czyj&#347;"
  ]
  node [
    id 58
    label "osobny"
  ]
  node [
    id 59
    label "zwi&#261;zany"
  ]
  node [
    id 60
    label "engineering"
  ]
  node [
    id 61
    label "technika"
  ]
  node [
    id 62
    label "mikrotechnologia"
  ]
  node [
    id 63
    label "technologia_nieorganiczna"
  ]
  node [
    id 64
    label "biotechnologia"
  ]
  node [
    id 65
    label "spos&#243;b"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
]
