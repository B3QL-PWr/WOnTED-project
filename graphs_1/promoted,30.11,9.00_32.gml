graph [
  maxDegree 58
  minDegree 1
  meanDegree 2.040201005025126
  density 0.010304045479924877
  graphCliqueNumber 2
  node [
    id 0
    label "mi&#322;o&#347;nica"
    origin "text"
  ]
  node [
    id 1
    label "posiadacz"
    origin "text"
  ]
  node [
    id 2
    label "zwierz&#281;"
    origin "text"
  ]
  node [
    id 3
    label "apelowa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "interwencja"
    origin "text"
  ]
  node [
    id 5
    label "sprawa"
    origin "text"
  ]
  node [
    id 6
    label "brzemienny"
    origin "text"
  ]
  node [
    id 7
    label "skutek"
    origin "text"
  ]
  node [
    id 8
    label "decyzja"
    origin "text"
  ]
  node [
    id 9
    label "polski"
    origin "text"
  ]
  node [
    id 10
    label "parlament"
    origin "text"
  ]
  node [
    id 11
    label "nacisk"
    origin "text"
  ]
  node [
    id 12
    label "pomoc"
    origin "text"
  ]
  node [
    id 13
    label "utrzymanie"
    origin "text"
  ]
  node [
    id 14
    label "obecna"
    origin "text"
  ]
  node [
    id 15
    label "stawek"
    origin "text"
  ]
  node [
    id 16
    label "podatek"
    origin "text"
  ]
  node [
    id 17
    label "vat"
    origin "text"
  ]
  node [
    id 18
    label "lek"
    origin "text"
  ]
  node [
    id 19
    label "artyku&#322;"
    origin "text"
  ]
  node [
    id 20
    label "dla"
    origin "text"
  ]
  node [
    id 21
    label "kochanka"
  ]
  node [
    id 22
    label "mi&#322;o&#347;niczka"
  ]
  node [
    id 23
    label "wykupienie"
  ]
  node [
    id 24
    label "bycie_w_posiadaniu"
  ]
  node [
    id 25
    label "wykupywanie"
  ]
  node [
    id 26
    label "podmiot"
  ]
  node [
    id 27
    label "cz&#322;owiek"
  ]
  node [
    id 28
    label "grzbiet"
  ]
  node [
    id 29
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 30
    label "zachowanie"
  ]
  node [
    id 31
    label "fukanie"
  ]
  node [
    id 32
    label "popapraniec"
  ]
  node [
    id 33
    label "tresowa&#263;"
  ]
  node [
    id 34
    label "siedzie&#263;"
  ]
  node [
    id 35
    label "oswaja&#263;"
  ]
  node [
    id 36
    label "napa&#347;&#263;_si&#281;"
  ]
  node [
    id 37
    label "poskramia&#263;"
  ]
  node [
    id 38
    label "zwyrol"
  ]
  node [
    id 39
    label "animalista"
  ]
  node [
    id 40
    label "skubn&#261;&#263;"
  ]
  node [
    id 41
    label "fukni&#281;cie"
  ]
  node [
    id 42
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 43
    label "zar&#380;ni&#281;cie"
  ]
  node [
    id 44
    label "farba"
  ]
  node [
    id 45
    label "istota_&#380;ywa"
  ]
  node [
    id 46
    label "budowa"
  ]
  node [
    id 47
    label "pa&#347;&#263;_si&#281;"
  ]
  node [
    id 48
    label "budowa_cia&#322;a"
  ]
  node [
    id 49
    label "monogamia"
  ]
  node [
    id 50
    label "sodomita"
  ]
  node [
    id 51
    label "dor&#380;n&#261;&#263;"
  ]
  node [
    id 52
    label "oz&#243;r"
  ]
  node [
    id 53
    label "gad"
  ]
  node [
    id 54
    label "&#322;eb"
  ]
  node [
    id 55
    label "treser"
  ]
  node [
    id 56
    label "fauna"
  ]
  node [
    id 57
    label "pasienie_si&#281;"
  ]
  node [
    id 58
    label "degenerat"
  ]
  node [
    id 59
    label "czerniak"
  ]
  node [
    id 60
    label "siedzenie"
  ]
  node [
    id 61
    label "wiwarium"
  ]
  node [
    id 62
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 63
    label "weterynarz"
  ]
  node [
    id 64
    label "wios&#322;owa&#263;"
  ]
  node [
    id 65
    label "le&#380;e&#263;"
  ]
  node [
    id 66
    label "skuba&#263;"
  ]
  node [
    id 67
    label "skubni&#281;cie"
  ]
  node [
    id 68
    label "poligamia"
  ]
  node [
    id 69
    label "hodowla"
  ]
  node [
    id 70
    label "przyssawka"
  ]
  node [
    id 71
    label "agresja"
  ]
  node [
    id 72
    label "niecz&#322;owiek"
  ]
  node [
    id 73
    label "skubanie"
  ]
  node [
    id 74
    label "wios&#322;owanie"
  ]
  node [
    id 75
    label "napasienie_si&#281;"
  ]
  node [
    id 76
    label "okrutnik"
  ]
  node [
    id 77
    label "wylinka"
  ]
  node [
    id 78
    label "paszcza"
  ]
  node [
    id 79
    label "bestia"
  ]
  node [
    id 80
    label "zwierz&#281;ta"
  ]
  node [
    id 81
    label "le&#380;enie"
  ]
  node [
    id 82
    label "call"
  ]
  node [
    id 83
    label "prosi&#263;"
  ]
  node [
    id 84
    label "odwo&#322;ywa&#263;_si&#281;"
  ]
  node [
    id 85
    label "przemawia&#263;"
  ]
  node [
    id 86
    label "address"
  ]
  node [
    id 87
    label "interposition"
  ]
  node [
    id 88
    label "akcja"
  ]
  node [
    id 89
    label "ingerencja"
  ]
  node [
    id 90
    label "temat"
  ]
  node [
    id 91
    label "kognicja"
  ]
  node [
    id 92
    label "idea"
  ]
  node [
    id 93
    label "szczeg&#243;&#322;"
  ]
  node [
    id 94
    label "rzecz"
  ]
  node [
    id 95
    label "wydarzenie"
  ]
  node [
    id 96
    label "przes&#322;anka"
  ]
  node [
    id 97
    label "rozprawa"
  ]
  node [
    id 98
    label "object"
  ]
  node [
    id 99
    label "proposition"
  ]
  node [
    id 100
    label "ci&#281;&#380;arny"
  ]
  node [
    id 101
    label "brzemiennie"
  ]
  node [
    id 102
    label "pe&#322;ny"
  ]
  node [
    id 103
    label "wa&#380;ny"
  ]
  node [
    id 104
    label "rezultat"
  ]
  node [
    id 105
    label "dokument"
  ]
  node [
    id 106
    label "resolution"
  ]
  node [
    id 107
    label "zdecydowanie"
  ]
  node [
    id 108
    label "wytw&#243;r"
  ]
  node [
    id 109
    label "podj&#281;cie_decyzji"
  ]
  node [
    id 110
    label "management"
  ]
  node [
    id 111
    label "lacki"
  ]
  node [
    id 112
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 113
    label "przedmiot"
  ]
  node [
    id 114
    label "sztajer"
  ]
  node [
    id 115
    label "drabant"
  ]
  node [
    id 116
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 117
    label "polak"
  ]
  node [
    id 118
    label "pierogi_ruskie"
  ]
  node [
    id 119
    label "krakowiak"
  ]
  node [
    id 120
    label "Polish"
  ]
  node [
    id 121
    label "j&#281;zyk"
  ]
  node [
    id 122
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 123
    label "oberek"
  ]
  node [
    id 124
    label "po_polsku"
  ]
  node [
    id 125
    label "mazur"
  ]
  node [
    id 126
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 127
    label "chodzony"
  ]
  node [
    id 128
    label "skoczny"
  ]
  node [
    id 129
    label "ryba_po_grecku"
  ]
  node [
    id 130
    label "goniony"
  ]
  node [
    id 131
    label "polsko"
  ]
  node [
    id 132
    label "grupa"
  ]
  node [
    id 133
    label "plankton_polityczny"
  ]
  node [
    id 134
    label "ustawodawca"
  ]
  node [
    id 135
    label "urz&#261;d"
  ]
  node [
    id 136
    label "europarlament"
  ]
  node [
    id 137
    label "grupa_bilateralna"
  ]
  node [
    id 138
    label "zjawisko"
  ]
  node [
    id 139
    label "force"
  ]
  node [
    id 140
    label "uwaga"
  ]
  node [
    id 141
    label "wp&#322;yw"
  ]
  node [
    id 142
    label "zgodzi&#263;"
  ]
  node [
    id 143
    label "pomocnik"
  ]
  node [
    id 144
    label "doch&#243;d"
  ]
  node [
    id 145
    label "property"
  ]
  node [
    id 146
    label "telefon_zaufania"
  ]
  node [
    id 147
    label "darowizna"
  ]
  node [
    id 148
    label "&#347;rodek"
  ]
  node [
    id 149
    label "liga"
  ]
  node [
    id 150
    label "potrzymanie"
  ]
  node [
    id 151
    label "zrobienie"
  ]
  node [
    id 152
    label "manewr"
  ]
  node [
    id 153
    label "podtrzymanie"
  ]
  node [
    id 154
    label "obronienie"
  ]
  node [
    id 155
    label "byt"
  ]
  node [
    id 156
    label "zdo&#322;anie"
  ]
  node [
    id 157
    label "zapewnienie"
  ]
  node [
    id 158
    label "bearing"
  ]
  node [
    id 159
    label "subsystencja"
  ]
  node [
    id 160
    label "wy&#380;ywienie"
  ]
  node [
    id 161
    label "wychowanie"
  ]
  node [
    id 162
    label "uniesienie"
  ]
  node [
    id 163
    label "preservation"
  ]
  node [
    id 164
    label "zap&#322;acenie"
  ]
  node [
    id 165
    label "przetrzymanie"
  ]
  node [
    id 166
    label "bilans_handlowy"
  ]
  node [
    id 167
    label "op&#322;ata"
  ]
  node [
    id 168
    label "danina"
  ]
  node [
    id 169
    label "trybut"
  ]
  node [
    id 170
    label "tr&#243;jk&#261;t_Harbergera"
  ]
  node [
    id 171
    label "podatek_konsumpcyjny"
  ]
  node [
    id 172
    label "naszprycowa&#263;"
  ]
  node [
    id 173
    label "Albania"
  ]
  node [
    id 174
    label "tonizowa&#263;"
  ]
  node [
    id 175
    label "medicine"
  ]
  node [
    id 176
    label "szprycowa&#263;"
  ]
  node [
    id 177
    label "przepisanie"
  ]
  node [
    id 178
    label "przepisa&#263;"
  ]
  node [
    id 179
    label "tonizowanie"
  ]
  node [
    id 180
    label "szprycowanie"
  ]
  node [
    id 181
    label "naszprycowanie"
  ]
  node [
    id 182
    label "jednostka_monetarna"
  ]
  node [
    id 183
    label "apteczka"
  ]
  node [
    id 184
    label "substancja"
  ]
  node [
    id 185
    label "spos&#243;b"
  ]
  node [
    id 186
    label "quindarka"
  ]
  node [
    id 187
    label "towar"
  ]
  node [
    id 188
    label "nag&#322;&#243;wek"
  ]
  node [
    id 189
    label "znak_j&#281;zykowy"
  ]
  node [
    id 190
    label "wyr&#243;b"
  ]
  node [
    id 191
    label "blok"
  ]
  node [
    id 192
    label "line"
  ]
  node [
    id 193
    label "paragraf"
  ]
  node [
    id 194
    label "rodzajnik"
  ]
  node [
    id 195
    label "prawda"
  ]
  node [
    id 196
    label "szkic"
  ]
  node [
    id 197
    label "tekst"
  ]
  node [
    id 198
    label "fragment"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 9
    target 116
  ]
  edge [
    source 9
    target 117
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 9
    target 119
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 129
  ]
  edge [
    source 9
    target 130
  ]
  edge [
    source 9
    target 131
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 132
  ]
  edge [
    source 10
    target 133
  ]
  edge [
    source 10
    target 134
  ]
  edge [
    source 10
    target 135
  ]
  edge [
    source 10
    target 136
  ]
  edge [
    source 10
    target 137
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 138
  ]
  edge [
    source 11
    target 139
  ]
  edge [
    source 11
    target 140
  ]
  edge [
    source 11
    target 141
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 142
  ]
  edge [
    source 12
    target 143
  ]
  edge [
    source 12
    target 144
  ]
  edge [
    source 12
    target 145
  ]
  edge [
    source 12
    target 113
  ]
  edge [
    source 12
    target 132
  ]
  edge [
    source 12
    target 146
  ]
  edge [
    source 12
    target 147
  ]
  edge [
    source 12
    target 148
  ]
  edge [
    source 12
    target 149
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 150
  ]
  edge [
    source 13
    target 151
  ]
  edge [
    source 13
    target 152
  ]
  edge [
    source 13
    target 153
  ]
  edge [
    source 13
    target 30
  ]
  edge [
    source 13
    target 154
  ]
  edge [
    source 13
    target 155
  ]
  edge [
    source 13
    target 156
  ]
  edge [
    source 13
    target 157
  ]
  edge [
    source 13
    target 158
  ]
  edge [
    source 13
    target 159
  ]
  edge [
    source 13
    target 160
  ]
  edge [
    source 13
    target 161
  ]
  edge [
    source 13
    target 162
  ]
  edge [
    source 13
    target 163
  ]
  edge [
    source 13
    target 164
  ]
  edge [
    source 13
    target 165
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 166
  ]
  edge [
    source 16
    target 167
  ]
  edge [
    source 16
    target 168
  ]
  edge [
    source 16
    target 169
  ]
  edge [
    source 16
    target 170
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 171
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 172
  ]
  edge [
    source 18
    target 173
  ]
  edge [
    source 18
    target 174
  ]
  edge [
    source 18
    target 175
  ]
  edge [
    source 18
    target 176
  ]
  edge [
    source 18
    target 177
  ]
  edge [
    source 18
    target 178
  ]
  edge [
    source 18
    target 179
  ]
  edge [
    source 18
    target 180
  ]
  edge [
    source 18
    target 181
  ]
  edge [
    source 18
    target 182
  ]
  edge [
    source 18
    target 183
  ]
  edge [
    source 18
    target 184
  ]
  edge [
    source 18
    target 185
  ]
  edge [
    source 18
    target 186
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 105
  ]
  edge [
    source 19
    target 187
  ]
  edge [
    source 19
    target 188
  ]
  edge [
    source 19
    target 189
  ]
  edge [
    source 19
    target 190
  ]
  edge [
    source 19
    target 191
  ]
  edge [
    source 19
    target 192
  ]
  edge [
    source 19
    target 193
  ]
  edge [
    source 19
    target 194
  ]
  edge [
    source 19
    target 195
  ]
  edge [
    source 19
    target 196
  ]
  edge [
    source 19
    target 197
  ]
  edge [
    source 19
    target 198
  ]
]
