graph [
  maxDegree 35
  minDegree 1
  meanDegree 1.951219512195122
  density 0.04878048780487805
  graphCliqueNumber 2
  node [
    id 0
    label "eksploatacja"
    origin "text"
  ]
  node [
    id 1
    label "sie&#263;"
    origin "text"
  ]
  node [
    id 2
    label "telekomunikacyjny"
    origin "text"
  ]
  node [
    id 3
    label "utilization"
  ]
  node [
    id 4
    label "czynno&#347;&#263;"
  ]
  node [
    id 5
    label "wyzysk"
  ]
  node [
    id 6
    label "proces"
  ]
  node [
    id 7
    label "hipertekst"
  ]
  node [
    id 8
    label "gauze"
  ]
  node [
    id 9
    label "nitka"
  ]
  node [
    id 10
    label "mesh"
  ]
  node [
    id 11
    label "e-hazard"
  ]
  node [
    id 12
    label "netbook"
  ]
  node [
    id 13
    label "cyberprzestrze&#324;"
  ]
  node [
    id 14
    label "biznes_elektroniczny"
  ]
  node [
    id 15
    label "snu&#263;"
  ]
  node [
    id 16
    label "organization"
  ]
  node [
    id 17
    label "zasadzka"
  ]
  node [
    id 18
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 19
    label "web"
  ]
  node [
    id 20
    label "provider"
  ]
  node [
    id 21
    label "struktura"
  ]
  node [
    id 22
    label "us&#322;uga_internetowa"
  ]
  node [
    id 23
    label "punkt_dost&#281;pu"
  ]
  node [
    id 24
    label "organizacja"
  ]
  node [
    id 25
    label "mem"
  ]
  node [
    id 26
    label "vane"
  ]
  node [
    id 27
    label "podcast"
  ]
  node [
    id 28
    label "grooming"
  ]
  node [
    id 29
    label "kszta&#322;t"
  ]
  node [
    id 30
    label "strona"
  ]
  node [
    id 31
    label "obiekt"
  ]
  node [
    id 32
    label "wysnu&#263;"
  ]
  node [
    id 33
    label "gra_sieciowa"
  ]
  node [
    id 34
    label "instalacja"
  ]
  node [
    id 35
    label "sie&#263;_komputerowa"
  ]
  node [
    id 36
    label "net"
  ]
  node [
    id 37
    label "plecionka"
  ]
  node [
    id 38
    label "media"
  ]
  node [
    id 39
    label "rozmieszczenie"
  ]
  node [
    id 40
    label "prawo"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 2
    target 40
  ]
]
