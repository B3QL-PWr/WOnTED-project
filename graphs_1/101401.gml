graph [
  maxDegree 1
  minDegree 1
  meanDegree 1
  density 0.1111111111111111
  graphCliqueNumber 2
  node [
    id 0
    label "john"
    origin "text"
  ]
  node [
    id 1
    label "yarmuth"
    origin "text"
  ]
  node [
    id 2
    label "John"
  ]
  node [
    id 3
    label "Yarmuth"
  ]
  node [
    id 4
    label "partia"
  ]
  node [
    id 5
    label "demokratyczny"
  ]
  node [
    id 6
    label "izba"
  ]
  node [
    id 7
    label "reprezentant"
  ]
  node [
    id 8
    label "stanowi&#263;"
  ]
  node [
    id 9
    label "zjednoczy&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 8
    target 9
  ]
]
