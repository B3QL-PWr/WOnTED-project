graph [
  maxDegree 40
  minDegree 1
  meanDegree 2
  density 0.02857142857142857
  graphCliqueNumber 2
  node [
    id 0
    label "do&#380;y&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "czas"
    origin "text"
  ]
  node [
    id 2
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 3
    label "empatia"
    origin "text"
  ]
  node [
    id 4
    label "nieznacz&#261;cy"
    origin "text"
  ]
  node [
    id 5
    label "s&#322;owo"
    origin "text"
  ]
  node [
    id 6
    label "przesz&#322;o&#347;&#263;"
    origin "text"
  ]
  node [
    id 7
    label "czasokres"
  ]
  node [
    id 8
    label "trawienie"
  ]
  node [
    id 9
    label "kategoria_gramatyczna"
  ]
  node [
    id 10
    label "period"
  ]
  node [
    id 11
    label "odczyt"
  ]
  node [
    id 12
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 13
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 14
    label "chwila"
  ]
  node [
    id 15
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 16
    label "poprzedzenie"
  ]
  node [
    id 17
    label "koniugacja"
  ]
  node [
    id 18
    label "dzieje"
  ]
  node [
    id 19
    label "poprzedzi&#263;"
  ]
  node [
    id 20
    label "przep&#322;ywanie"
  ]
  node [
    id 21
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 22
    label "odwlekanie_si&#281;"
  ]
  node [
    id 23
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 24
    label "Zeitgeist"
  ]
  node [
    id 25
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 26
    label "okres_czasu"
  ]
  node [
    id 27
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 28
    label "pochodzi&#263;"
  ]
  node [
    id 29
    label "schy&#322;ek"
  ]
  node [
    id 30
    label "czwarty_wymiar"
  ]
  node [
    id 31
    label "chronometria"
  ]
  node [
    id 32
    label "poprzedzanie"
  ]
  node [
    id 33
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 34
    label "pogoda"
  ]
  node [
    id 35
    label "zegar"
  ]
  node [
    id 36
    label "trawi&#263;"
  ]
  node [
    id 37
    label "pochodzenie"
  ]
  node [
    id 38
    label "poprzedza&#263;"
  ]
  node [
    id 39
    label "time_period"
  ]
  node [
    id 40
    label "rachuba_czasu"
  ]
  node [
    id 41
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 42
    label "czasoprzestrze&#324;"
  ]
  node [
    id 43
    label "laba"
  ]
  node [
    id 44
    label "wczuwa&#263;_si&#281;"
  ]
  node [
    id 45
    label "empathy"
  ]
  node [
    id 46
    label "zrozumienie"
  ]
  node [
    id 47
    label "lito&#347;&#263;"
  ]
  node [
    id 48
    label "nieistotnie"
  ]
  node [
    id 49
    label "ma&#322;owa&#380;ny"
  ]
  node [
    id 50
    label "nieznaczny"
  ]
  node [
    id 51
    label "obietnica"
  ]
  node [
    id 52
    label "bit"
  ]
  node [
    id 53
    label "s&#322;ownictwo"
  ]
  node [
    id 54
    label "jednostka_leksykalna"
  ]
  node [
    id 55
    label "pisanie_si&#281;"
  ]
  node [
    id 56
    label "wykrzyknik"
  ]
  node [
    id 57
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 58
    label "pole_semantyczne"
  ]
  node [
    id 59
    label "&#347;r&#243;dg&#322;os"
  ]
  node [
    id 60
    label "komunikat"
  ]
  node [
    id 61
    label "pisa&#263;_si&#281;"
  ]
  node [
    id 62
    label "wypowiedzenie"
  ]
  node [
    id 63
    label "nag&#322;os"
  ]
  node [
    id 64
    label "wordnet"
  ]
  node [
    id 65
    label "morfem"
  ]
  node [
    id 66
    label "czasownik"
  ]
  node [
    id 67
    label "wyg&#322;os"
  ]
  node [
    id 68
    label "jednostka_informacji"
  ]
  node [
    id 69
    label "&#380;ycie"
  ]
  node [
    id 70
    label "koleje_losu"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
]
