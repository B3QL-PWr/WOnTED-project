graph [
  maxDegree 45
  minDegree 1
  meanDegree 2
  density 0.020618556701030927
  graphCliqueNumber 3
  node [
    id 0
    label "historia"
    origin "text"
  ]
  node [
    id 1
    label "praca"
    origin "text"
  ]
  node [
    id 2
    label "automatyk"
    origin "text"
  ]
  node [
    id 3
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 4
    label "mama"
    origin "text"
  ]
  node [
    id 5
    label "nadzieja"
    origin "text"
  ]
  node [
    id 6
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 7
    label "co&#347;"
    origin "text"
  ]
  node [
    id 8
    label "nauczy&#263;"
    origin "text"
  ]
  node [
    id 9
    label "report"
  ]
  node [
    id 10
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 11
    label "wypowied&#378;"
  ]
  node [
    id 12
    label "neografia"
  ]
  node [
    id 13
    label "przedmiot"
  ]
  node [
    id 14
    label "papirologia"
  ]
  node [
    id 15
    label "historia_gospodarcza"
  ]
  node [
    id 16
    label "przebiec"
  ]
  node [
    id 17
    label "hista"
  ]
  node [
    id 18
    label "nauka_humanistyczna"
  ]
  node [
    id 19
    label "filigranistyka"
  ]
  node [
    id 20
    label "dyplomatyka"
  ]
  node [
    id 21
    label "annalistyka"
  ]
  node [
    id 22
    label "historyka"
  ]
  node [
    id 23
    label "heraldyka"
  ]
  node [
    id 24
    label "fabu&#322;a"
  ]
  node [
    id 25
    label "muzealnictwo"
  ]
  node [
    id 26
    label "varsavianistyka"
  ]
  node [
    id 27
    label "prezentyzm"
  ]
  node [
    id 28
    label "mediewistyka"
  ]
  node [
    id 29
    label "przebiegni&#281;cie"
  ]
  node [
    id 30
    label "charakter"
  ]
  node [
    id 31
    label "paleografia"
  ]
  node [
    id 32
    label "genealogia"
  ]
  node [
    id 33
    label "czynno&#347;&#263;"
  ]
  node [
    id 34
    label "prozopografia"
  ]
  node [
    id 35
    label "motyw"
  ]
  node [
    id 36
    label "nautologia"
  ]
  node [
    id 37
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 38
    label "epoka"
  ]
  node [
    id 39
    label "numizmatyka"
  ]
  node [
    id 40
    label "ruralistyka"
  ]
  node [
    id 41
    label "epigrafika"
  ]
  node [
    id 42
    label "&#378;r&#243;d&#322;oznawstwo"
  ]
  node [
    id 43
    label "historiografia"
  ]
  node [
    id 44
    label "bizantynistyka"
  ]
  node [
    id 45
    label "weksylologia"
  ]
  node [
    id 46
    label "kierunek"
  ]
  node [
    id 47
    label "ikonografia"
  ]
  node [
    id 48
    label "chronologia"
  ]
  node [
    id 49
    label "archiwistyka"
  ]
  node [
    id 50
    label "sfragistyka"
  ]
  node [
    id 51
    label "zabytkoznawstwo"
  ]
  node [
    id 52
    label "historia_sztuki"
  ]
  node [
    id 53
    label "stosunek_pracy"
  ]
  node [
    id 54
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 55
    label "benedykty&#324;ski"
  ]
  node [
    id 56
    label "pracowanie"
  ]
  node [
    id 57
    label "zaw&#243;d"
  ]
  node [
    id 58
    label "kierownictwo"
  ]
  node [
    id 59
    label "zmiana"
  ]
  node [
    id 60
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 61
    label "wytw&#243;r"
  ]
  node [
    id 62
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 63
    label "tynkarski"
  ]
  node [
    id 64
    label "czynnik_produkcji"
  ]
  node [
    id 65
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 66
    label "zobowi&#261;zanie"
  ]
  node [
    id 67
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 68
    label "tyrka"
  ]
  node [
    id 69
    label "pracowa&#263;"
  ]
  node [
    id 70
    label "siedziba"
  ]
  node [
    id 71
    label "poda&#380;_pracy"
  ]
  node [
    id 72
    label "miejsce"
  ]
  node [
    id 73
    label "zak&#322;ad"
  ]
  node [
    id 74
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 75
    label "najem"
  ]
  node [
    id 76
    label "in&#380;ynier"
  ]
  node [
    id 77
    label "technik"
  ]
  node [
    id 78
    label "matczysko"
  ]
  node [
    id 79
    label "macierz"
  ]
  node [
    id 80
    label "przodkini"
  ]
  node [
    id 81
    label "Matka_Boska"
  ]
  node [
    id 82
    label "macocha"
  ]
  node [
    id 83
    label "matka_zast&#281;pcza"
  ]
  node [
    id 84
    label "stara"
  ]
  node [
    id 85
    label "rodzice"
  ]
  node [
    id 86
    label "rodzic"
  ]
  node [
    id 87
    label "&#347;wiate&#322;ko_w_tunelu"
  ]
  node [
    id 88
    label "wierzy&#263;"
  ]
  node [
    id 89
    label "szansa"
  ]
  node [
    id 90
    label "oczekiwanie"
  ]
  node [
    id 91
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 92
    label "spoczywa&#263;"
  ]
  node [
    id 93
    label "thing"
  ]
  node [
    id 94
    label "cosik"
  ]
  node [
    id 95
    label "instruct"
  ]
  node [
    id 96
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 97
    label "wyszkoli&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
]
