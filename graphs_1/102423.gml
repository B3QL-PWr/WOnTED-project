graph [
  maxDegree 23
  minDegree 1
  meanDegree 2.0454545454545454
  density 0.04756871035940803
  graphCliqueNumber 3
  node [
    id 0
    label "holenderski"
    origin "text"
  ]
  node [
    id 1
    label "organizacja"
    origin "text"
  ]
  node [
    id 2
    label "zbiorowy"
    origin "text"
  ]
  node [
    id 3
    label "zarz&#261;d"
    origin "text"
  ]
  node [
    id 4
    label "wsp&#243;&#322;pracowa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "creative"
    origin "text"
  ]
  node [
    id 6
    label "commons"
    origin "text"
  ]
  node [
    id 7
    label "niderlandzki"
  ]
  node [
    id 8
    label "j&#281;zyk_niderlandzki"
  ]
  node [
    id 9
    label "europejski"
  ]
  node [
    id 10
    label "holendersko"
  ]
  node [
    id 11
    label "po_holendersku"
  ]
  node [
    id 12
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 13
    label "endecki"
  ]
  node [
    id 14
    label "komitet_koordynacyjny"
  ]
  node [
    id 15
    label "przybud&#243;wka"
  ]
  node [
    id 16
    label "ZOMO"
  ]
  node [
    id 17
    label "podmiot"
  ]
  node [
    id 18
    label "boj&#243;wka"
  ]
  node [
    id 19
    label "zesp&#243;&#322;"
  ]
  node [
    id 20
    label "organization"
  ]
  node [
    id 21
    label "TOPR"
  ]
  node [
    id 22
    label "jednostka_organizacyjna"
  ]
  node [
    id 23
    label "przedstawicielstwo"
  ]
  node [
    id 24
    label "Cepelia"
  ]
  node [
    id 25
    label "GOPR"
  ]
  node [
    id 26
    label "ZMP"
  ]
  node [
    id 27
    label "ZBoWiD"
  ]
  node [
    id 28
    label "struktura"
  ]
  node [
    id 29
    label "od&#322;am"
  ]
  node [
    id 30
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 31
    label "centrala"
  ]
  node [
    id 32
    label "zbiorowo"
  ]
  node [
    id 33
    label "wsp&#243;lny"
  ]
  node [
    id 34
    label "administration"
  ]
  node [
    id 35
    label "czynno&#347;&#263;"
  ]
  node [
    id 36
    label "administracja"
  ]
  node [
    id 37
    label "biuro"
  ]
  node [
    id 38
    label "w&#322;adza"
  ]
  node [
    id 39
    label "organ_sp&#243;&#322;ki"
  ]
  node [
    id 40
    label "kierownictwo"
  ]
  node [
    id 41
    label "Bruksela"
  ]
  node [
    id 42
    label "siedziba"
  ]
  node [
    id 43
    label "dzia&#322;a&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 5
    target 6
  ]
]
