graph [
  maxDegree 50
  minDegree 1
  meanDegree 2.105769230769231
  density 0.010172798216276478
  graphCliqueNumber 4
  node [
    id 0
    label "po&#322;a"
    origin "text"
  ]
  node [
    id 1
    label "rok"
    origin "text"
  ]
  node [
    id 2
    label "ograniczenie"
    origin "text"
  ]
  node [
    id 3
    label "wolno&#347;&#263;"
    origin "text"
  ]
  node [
    id 4
    label "forma"
    origin "text"
  ]
  node [
    id 5
    label "praca"
    origin "text"
  ]
  node [
    id 6
    label "spo&#322;eczny"
    origin "text"
  ]
  node [
    id 7
    label "wymiar"
    origin "text"
  ]
  node [
    id 8
    label "godzina"
    origin "text"
  ]
  node [
    id 9
    label "miesi&#261;c"
    origin "text"
  ]
  node [
    id 10
    label "taka"
    origin "text"
  ]
  node [
    id 11
    label "kara"
    origin "text"
  ]
  node [
    id 12
    label "orzec"
    origin "text"
  ]
  node [
    id 13
    label "dzi&#347;"
    origin "text"
  ]
  node [
    id 14
    label "s&#261;d"
    origin "text"
  ]
  node [
    id 15
    label "okr&#281;gowy"
    origin "text"
  ]
  node [
    id 16
    label "gda&#324;ski"
    origin "text"
  ]
  node [
    id 17
    label "dla"
    origin "text"
  ]
  node [
    id 18
    label "wnuk"
    origin "text"
  ]
  node [
    id 19
    label "lech"
    origin "text"
  ]
  node [
    id 20
    label "wa&#322;&#281;sa"
    origin "text"
  ]
  node [
    id 21
    label "str&#243;j"
  ]
  node [
    id 22
    label "fragment"
  ]
  node [
    id 23
    label "stulecie"
  ]
  node [
    id 24
    label "kalendarz"
  ]
  node [
    id 25
    label "czas"
  ]
  node [
    id 26
    label "pora_roku"
  ]
  node [
    id 27
    label "cykl_astronomiczny"
  ]
  node [
    id 28
    label "p&#243;&#322;rocze"
  ]
  node [
    id 29
    label "grupa"
  ]
  node [
    id 30
    label "kwarta&#322;"
  ]
  node [
    id 31
    label "kurs"
  ]
  node [
    id 32
    label "jubileusz"
  ]
  node [
    id 33
    label "lata"
  ]
  node [
    id 34
    label "martwy_sezon"
  ]
  node [
    id 35
    label "pomroczno&#347;&#263;_jasna"
  ]
  node [
    id 36
    label "prevention"
  ]
  node [
    id 37
    label "zdyskryminowanie"
  ]
  node [
    id 38
    label "barrier"
  ]
  node [
    id 39
    label "zmniejszenie"
  ]
  node [
    id 40
    label "otoczenie"
  ]
  node [
    id 41
    label "przekracza&#263;"
  ]
  node [
    id 42
    label "cecha"
  ]
  node [
    id 43
    label "przekraczanie"
  ]
  node [
    id 44
    label "przekroczenie"
  ]
  node [
    id 45
    label "pomiarkowanie"
  ]
  node [
    id 46
    label "limitation"
  ]
  node [
    id 47
    label "warunek"
  ]
  node [
    id 48
    label "przekroczy&#263;"
  ]
  node [
    id 49
    label "g&#322;upstwo"
  ]
  node [
    id 50
    label "osielstwo"
  ]
  node [
    id 51
    label "przeszkoda"
  ]
  node [
    id 52
    label "reservation"
  ]
  node [
    id 53
    label "intelekt"
  ]
  node [
    id 54
    label "finlandyzacja"
  ]
  node [
    id 55
    label "absolutno&#347;&#263;"
  ]
  node [
    id 56
    label "obecno&#347;&#263;"
  ]
  node [
    id 57
    label "uwi&#281;zi&#263;"
  ]
  node [
    id 58
    label "freedom"
  ]
  node [
    id 59
    label "niezale&#380;no&#347;&#263;"
  ]
  node [
    id 60
    label "uwi&#281;zienie"
  ]
  node [
    id 61
    label "nieokre&#347;lono&#347;&#263;"
  ]
  node [
    id 62
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 63
    label "punkt_widzenia"
  ]
  node [
    id 64
    label "do&#322;ek"
  ]
  node [
    id 65
    label "formality"
  ]
  node [
    id 66
    label "wz&#243;r"
  ]
  node [
    id 67
    label "kantyzm"
  ]
  node [
    id 68
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 69
    label "ornamentyka"
  ]
  node [
    id 70
    label "odmiana"
  ]
  node [
    id 71
    label "mode"
  ]
  node [
    id 72
    label "style"
  ]
  node [
    id 73
    label "formacja"
  ]
  node [
    id 74
    label "maszyna_drukarska"
  ]
  node [
    id 75
    label "poznanie"
  ]
  node [
    id 76
    label "szablon"
  ]
  node [
    id 77
    label "struktura"
  ]
  node [
    id 78
    label "spirala"
  ]
  node [
    id 79
    label "blaszka"
  ]
  node [
    id 80
    label "linearno&#347;&#263;"
  ]
  node [
    id 81
    label "stan"
  ]
  node [
    id 82
    label "temat"
  ]
  node [
    id 83
    label "g&#322;owa"
  ]
  node [
    id 84
    label "kielich"
  ]
  node [
    id 85
    label "zdolno&#347;&#263;"
  ]
  node [
    id 86
    label "poj&#281;cie"
  ]
  node [
    id 87
    label "kszta&#322;t"
  ]
  node [
    id 88
    label "pasmo"
  ]
  node [
    id 89
    label "rdze&#324;"
  ]
  node [
    id 90
    label "leksem"
  ]
  node [
    id 91
    label "dyspozycja"
  ]
  node [
    id 92
    label "wygl&#261;d"
  ]
  node [
    id 93
    label "October"
  ]
  node [
    id 94
    label "zawarto&#347;&#263;"
  ]
  node [
    id 95
    label "creation"
  ]
  node [
    id 96
    label "p&#281;tla"
  ]
  node [
    id 97
    label "p&#322;at"
  ]
  node [
    id 98
    label "gwiazda"
  ]
  node [
    id 99
    label "arystotelizm"
  ]
  node [
    id 100
    label "obiekt"
  ]
  node [
    id 101
    label "dzie&#322;o"
  ]
  node [
    id 102
    label "naczynie"
  ]
  node [
    id 103
    label "wyra&#380;enie"
  ]
  node [
    id 104
    label "jednostka_systematyczna"
  ]
  node [
    id 105
    label "miniatura"
  ]
  node [
    id 106
    label "zwyczaj"
  ]
  node [
    id 107
    label "morfem"
  ]
  node [
    id 108
    label "posta&#263;"
  ]
  node [
    id 109
    label "stosunek_pracy"
  ]
  node [
    id 110
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 111
    label "benedykty&#324;ski"
  ]
  node [
    id 112
    label "pracowanie"
  ]
  node [
    id 113
    label "zaw&#243;d"
  ]
  node [
    id 114
    label "kierownictwo"
  ]
  node [
    id 115
    label "zmiana"
  ]
  node [
    id 116
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 117
    label "wytw&#243;r"
  ]
  node [
    id 118
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 119
    label "tynkarski"
  ]
  node [
    id 120
    label "czynnik_produkcji"
  ]
  node [
    id 121
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 122
    label "zobowi&#261;zanie"
  ]
  node [
    id 123
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 124
    label "czynno&#347;&#263;"
  ]
  node [
    id 125
    label "tyrka"
  ]
  node [
    id 126
    label "pracowa&#263;"
  ]
  node [
    id 127
    label "siedziba"
  ]
  node [
    id 128
    label "poda&#380;_pracy"
  ]
  node [
    id 129
    label "miejsce"
  ]
  node [
    id 130
    label "zak&#322;ad"
  ]
  node [
    id 131
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 132
    label "najem"
  ]
  node [
    id 133
    label "niepubliczny"
  ]
  node [
    id 134
    label "spo&#322;ecznie"
  ]
  node [
    id 135
    label "publiczny"
  ]
  node [
    id 136
    label "poziom"
  ]
  node [
    id 137
    label "znaczenie"
  ]
  node [
    id 138
    label "dymensja"
  ]
  node [
    id 139
    label "strona"
  ]
  node [
    id 140
    label "parametr"
  ]
  node [
    id 141
    label "dane"
  ]
  node [
    id 142
    label "liczba"
  ]
  node [
    id 143
    label "ilo&#347;&#263;"
  ]
  node [
    id 144
    label "wielko&#347;&#263;"
  ]
  node [
    id 145
    label "warunek_lokalowy"
  ]
  node [
    id 146
    label "minuta"
  ]
  node [
    id 147
    label "doba"
  ]
  node [
    id 148
    label "p&#243;&#322;godzina"
  ]
  node [
    id 149
    label "kwadrans"
  ]
  node [
    id 150
    label "time"
  ]
  node [
    id 151
    label "jednostka_czasu"
  ]
  node [
    id 152
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 153
    label "miech"
  ]
  node [
    id 154
    label "kalendy"
  ]
  node [
    id 155
    label "tydzie&#324;"
  ]
  node [
    id 156
    label "Bangladesz"
  ]
  node [
    id 157
    label "jednostka_monetarna"
  ]
  node [
    id 158
    label "klacz"
  ]
  node [
    id 159
    label "konsekwencja"
  ]
  node [
    id 160
    label "punishment"
  ]
  node [
    id 161
    label "forfeit"
  ]
  node [
    id 162
    label "roboty_przymusowe"
  ]
  node [
    id 163
    label "nemezis"
  ]
  node [
    id 164
    label "kwota"
  ]
  node [
    id 165
    label "connote"
  ]
  node [
    id 166
    label "decide"
  ]
  node [
    id 167
    label "zdecydowa&#263;"
  ]
  node [
    id 168
    label "stwierdzi&#263;"
  ]
  node [
    id 169
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 170
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 171
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 172
    label "procesowicz"
  ]
  node [
    id 173
    label "wypowied&#378;"
  ]
  node [
    id 174
    label "pods&#261;dny"
  ]
  node [
    id 175
    label "podejrzany"
  ]
  node [
    id 176
    label "broni&#263;"
  ]
  node [
    id 177
    label "bronienie"
  ]
  node [
    id 178
    label "system"
  ]
  node [
    id 179
    label "my&#347;l"
  ]
  node [
    id 180
    label "urz&#261;d"
  ]
  node [
    id 181
    label "konektyw"
  ]
  node [
    id 182
    label "court"
  ]
  node [
    id 183
    label "obrona"
  ]
  node [
    id 184
    label "s&#261;downictwo"
  ]
  node [
    id 185
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 186
    label "forum"
  ]
  node [
    id 187
    label "zesp&#243;&#322;"
  ]
  node [
    id 188
    label "post&#281;powanie"
  ]
  node [
    id 189
    label "skazany"
  ]
  node [
    id 190
    label "wydarzenie"
  ]
  node [
    id 191
    label "&#347;wiadek"
  ]
  node [
    id 192
    label "antylogizm"
  ]
  node [
    id 193
    label "oskar&#380;yciel"
  ]
  node [
    id 194
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 195
    label "biuro"
  ]
  node [
    id 196
    label "instytucja"
  ]
  node [
    id 197
    label "zdobny"
  ]
  node [
    id 198
    label "barokowy"
  ]
  node [
    id 199
    label "pomorski"
  ]
  node [
    id 200
    label "renesansowy"
  ]
  node [
    id 201
    label "po_gda&#324;sku"
  ]
  node [
    id 202
    label "masywny"
  ]
  node [
    id 203
    label "wnucz&#281;"
  ]
  node [
    id 204
    label "potomek"
  ]
  node [
    id 205
    label "w"
  ]
  node [
    id 206
    label "Lech"
  ]
  node [
    id 207
    label "Dominik"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 136
  ]
  edge [
    source 7
    target 137
  ]
  edge [
    source 7
    target 138
  ]
  edge [
    source 7
    target 139
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 7
    target 140
  ]
  edge [
    source 7
    target 141
  ]
  edge [
    source 7
    target 142
  ]
  edge [
    source 7
    target 143
  ]
  edge [
    source 7
    target 144
  ]
  edge [
    source 7
    target 145
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 146
  ]
  edge [
    source 8
    target 147
  ]
  edge [
    source 8
    target 25
  ]
  edge [
    source 8
    target 148
  ]
  edge [
    source 8
    target 149
  ]
  edge [
    source 8
    target 150
  ]
  edge [
    source 8
    target 151
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 25
  ]
  edge [
    source 9
    target 152
  ]
  edge [
    source 9
    target 153
  ]
  edge [
    source 9
    target 154
  ]
  edge [
    source 9
    target 155
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 156
  ]
  edge [
    source 10
    target 157
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 158
  ]
  edge [
    source 11
    target 159
  ]
  edge [
    source 11
    target 160
  ]
  edge [
    source 11
    target 161
  ]
  edge [
    source 11
    target 162
  ]
  edge [
    source 11
    target 163
  ]
  edge [
    source 11
    target 164
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 165
  ]
  edge [
    source 12
    target 166
  ]
  edge [
    source 12
    target 167
  ]
  edge [
    source 12
    target 168
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 147
  ]
  edge [
    source 13
    target 169
  ]
  edge [
    source 13
    target 170
  ]
  edge [
    source 13
    target 171
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 172
  ]
  edge [
    source 14
    target 173
  ]
  edge [
    source 14
    target 174
  ]
  edge [
    source 14
    target 175
  ]
  edge [
    source 14
    target 176
  ]
  edge [
    source 14
    target 177
  ]
  edge [
    source 14
    target 178
  ]
  edge [
    source 14
    target 179
  ]
  edge [
    source 14
    target 117
  ]
  edge [
    source 14
    target 180
  ]
  edge [
    source 14
    target 181
  ]
  edge [
    source 14
    target 182
  ]
  edge [
    source 14
    target 183
  ]
  edge [
    source 14
    target 184
  ]
  edge [
    source 14
    target 185
  ]
  edge [
    source 14
    target 186
  ]
  edge [
    source 14
    target 187
  ]
  edge [
    source 14
    target 188
  ]
  edge [
    source 14
    target 189
  ]
  edge [
    source 14
    target 190
  ]
  edge [
    source 14
    target 191
  ]
  edge [
    source 14
    target 192
  ]
  edge [
    source 14
    target 139
  ]
  edge [
    source 14
    target 193
  ]
  edge [
    source 14
    target 194
  ]
  edge [
    source 14
    target 195
  ]
  edge [
    source 14
    target 196
  ]
  edge [
    source 14
    target 205
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 205
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 197
  ]
  edge [
    source 16
    target 198
  ]
  edge [
    source 16
    target 199
  ]
  edge [
    source 16
    target 200
  ]
  edge [
    source 16
    target 201
  ]
  edge [
    source 16
    target 202
  ]
  edge [
    source 16
    target 205
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 203
  ]
  edge [
    source 18
    target 204
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 206
  ]
  edge [
    source 205
    target 207
  ]
]
