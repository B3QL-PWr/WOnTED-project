graph [
  maxDegree 7
  minDegree 1
  meanDegree 1.6
  density 0.11428571428571428
  graphCliqueNumber 3
  node [
    id 0
    label "bitwa"
    origin "text"
  ]
  node [
    id 1
    label "pod"
    origin "text"
  ]
  node [
    id 2
    label "lublin"
    origin "text"
  ]
  node [
    id 3
    label "zaj&#347;cie"
  ]
  node [
    id 4
    label "batalista"
  ]
  node [
    id 5
    label "walka"
  ]
  node [
    id 6
    label "action"
  ]
  node [
    id 7
    label "bitwa_pod_Pi&#322;awcami"
  ]
  node [
    id 8
    label "Kazimierz"
  ]
  node [
    id 9
    label "wielki"
  ]
  node [
    id 10
    label "Dymitr"
  ]
  node [
    id 11
    label "Detko"
  ]
  node [
    id 12
    label "ziemia"
  ]
  node [
    id 13
    label "lubelski"
  ]
  node [
    id 14
    label "sanocki"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 14
  ]
]
