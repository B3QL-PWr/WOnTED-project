graph [
  maxDegree 43
  minDegree 1
  meanDegree 2.0256410256410255
  density 0.013068651778329199
  graphCliqueNumber 3
  node [
    id 0
    label "podczas"
    origin "text"
  ]
  node [
    id 1
    label "wczorajszy"
    origin "text"
  ]
  node [
    id 2
    label "burzliwy"
    origin "text"
  ]
  node [
    id 3
    label "demonstracja"
    origin "text"
  ]
  node [
    id 4
    label "jaki"
    origin "text"
  ]
  node [
    id 5
    label "ogarn&#261;&#263;"
    origin "text"
  ]
  node [
    id 6
    label "francja"
    origin "text"
  ]
  node [
    id 7
    label "jedno"
    origin "text"
  ]
  node [
    id 8
    label "zdarzenie"
    origin "text"
  ]
  node [
    id 9
    label "niezwykle"
    origin "text"
  ]
  node [
    id 10
    label "wzburzy&#263;"
    origin "text"
  ]
  node [
    id 11
    label "opinia"
    origin "text"
  ]
  node [
    id 12
    label "publiczny"
    origin "text"
  ]
  node [
    id 13
    label "nagranie"
    origin "text"
  ]
  node [
    id 14
    label "kr&#261;&#380;y&#263;"
    origin "text"
  ]
  node [
    id 15
    label "internet"
    origin "text"
  ]
  node [
    id 16
    label "bi&#263;"
    origin "text"
  ]
  node [
    id 17
    label "rekord"
    origin "text"
  ]
  node [
    id 18
    label "popularno&#347;&#263;"
    origin "text"
  ]
  node [
    id 19
    label "dawny"
  ]
  node [
    id 20
    label "stary"
  ]
  node [
    id 21
    label "archaicznie"
  ]
  node [
    id 22
    label "zgrzybienie"
  ]
  node [
    id 23
    label "przestarzale"
  ]
  node [
    id 24
    label "starzenie_si&#281;"
  ]
  node [
    id 25
    label "zestarzenie_si&#281;"
  ]
  node [
    id 26
    label "niedzisiejszy"
  ]
  node [
    id 27
    label "porywczy"
  ]
  node [
    id 28
    label "niesta&#322;y"
  ]
  node [
    id 29
    label "nerwowy"
  ]
  node [
    id 30
    label "niespokojny"
  ]
  node [
    id 31
    label "zmienny"
  ]
  node [
    id 32
    label "pe&#322;ny"
  ]
  node [
    id 33
    label "burzliwie"
  ]
  node [
    id 34
    label "wzburzony"
  ]
  node [
    id 35
    label "g&#322;o&#347;ny"
  ]
  node [
    id 36
    label "manewr"
  ]
  node [
    id 37
    label "exhibition"
  ]
  node [
    id 38
    label "grupa"
  ]
  node [
    id 39
    label "zgromadzenie"
  ]
  node [
    id 40
    label "show"
  ]
  node [
    id 41
    label "pokaz"
  ]
  node [
    id 42
    label "visit"
  ]
  node [
    id 43
    label "spowodowa&#263;"
  ]
  node [
    id 44
    label "spotka&#263;"
  ]
  node [
    id 45
    label "manipulate"
  ]
  node [
    id 46
    label "environment"
  ]
  node [
    id 47
    label "otoczy&#263;"
  ]
  node [
    id 48
    label "dotkn&#261;&#263;"
  ]
  node [
    id 49
    label "involve"
  ]
  node [
    id 50
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 51
    label "czynno&#347;&#263;"
  ]
  node [
    id 52
    label "motyw"
  ]
  node [
    id 53
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 54
    label "fabu&#322;a"
  ]
  node [
    id 55
    label "przebiec"
  ]
  node [
    id 56
    label "przebiegni&#281;cie"
  ]
  node [
    id 57
    label "charakter"
  ]
  node [
    id 58
    label "niezwyk&#322;y"
  ]
  node [
    id 59
    label "zdenerwowa&#263;"
  ]
  node [
    id 60
    label "infuriate"
  ]
  node [
    id 61
    label "interrupt"
  ]
  node [
    id 62
    label "poruszy&#263;"
  ]
  node [
    id 63
    label "dokument"
  ]
  node [
    id 64
    label "rehabilitowanie_si&#281;"
  ]
  node [
    id 65
    label "reputacja"
  ]
  node [
    id 66
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 67
    label "rehabilitowa&#263;_si&#281;"
  ]
  node [
    id 68
    label "cecha"
  ]
  node [
    id 69
    label "zrehabilitowanie_si&#281;"
  ]
  node [
    id 70
    label "informacja"
  ]
  node [
    id 71
    label "sofcik"
  ]
  node [
    id 72
    label "appraisal"
  ]
  node [
    id 73
    label "ekspertyza"
  ]
  node [
    id 74
    label "zrehabilitowa&#263;_si&#281;"
  ]
  node [
    id 75
    label "pogl&#261;d"
  ]
  node [
    id 76
    label "kryterium"
  ]
  node [
    id 77
    label "wielko&#347;&#263;"
  ]
  node [
    id 78
    label "jawny"
  ]
  node [
    id 79
    label "upublicznienie"
  ]
  node [
    id 80
    label "upublicznianie"
  ]
  node [
    id 81
    label "publicznie"
  ]
  node [
    id 82
    label "wys&#322;uchanie"
  ]
  node [
    id 83
    label "wytw&#243;r"
  ]
  node [
    id 84
    label "wys&#322;ucha&#263;"
  ]
  node [
    id 85
    label "recording"
  ]
  node [
    id 86
    label "utrwalenie"
  ]
  node [
    id 87
    label "krew"
  ]
  node [
    id 88
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 89
    label "kontrolowa&#263;"
  ]
  node [
    id 90
    label "przedmiot"
  ]
  node [
    id 91
    label "wheel"
  ]
  node [
    id 92
    label "sok"
  ]
  node [
    id 93
    label "carry"
  ]
  node [
    id 94
    label "us&#322;uga_internetowa"
  ]
  node [
    id 95
    label "biznes_elektroniczny"
  ]
  node [
    id 96
    label "punkt_dost&#281;pu"
  ]
  node [
    id 97
    label "hipertekst"
  ]
  node [
    id 98
    label "gra_sieciowa"
  ]
  node [
    id 99
    label "mem"
  ]
  node [
    id 100
    label "e-hazard"
  ]
  node [
    id 101
    label "sie&#263;_komputerowa"
  ]
  node [
    id 102
    label "media"
  ]
  node [
    id 103
    label "podcast"
  ]
  node [
    id 104
    label "netbook"
  ]
  node [
    id 105
    label "provider"
  ]
  node [
    id 106
    label "cyberprzestrze&#324;"
  ]
  node [
    id 107
    label "grooming"
  ]
  node [
    id 108
    label "strona"
  ]
  node [
    id 109
    label "funkcjonowa&#263;"
  ]
  node [
    id 110
    label "rejestrowa&#263;"
  ]
  node [
    id 111
    label "proceed"
  ]
  node [
    id 112
    label "traktowa&#263;"
  ]
  node [
    id 113
    label "strike"
  ]
  node [
    id 114
    label "nalewa&#263;"
  ]
  node [
    id 115
    label "wydostawa&#263;_si&#281;"
  ]
  node [
    id 116
    label "dawa&#263;"
  ]
  node [
    id 117
    label "wygrywa&#263;"
  ]
  node [
    id 118
    label "zag&#322;&#281;bia&#263;"
  ]
  node [
    id 119
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 120
    label "przygotowywa&#263;"
  ]
  node [
    id 121
    label "zabija&#263;"
  ]
  node [
    id 122
    label "dzwoni&#263;"
  ]
  node [
    id 123
    label "take"
  ]
  node [
    id 124
    label "emanowa&#263;"
  ]
  node [
    id 125
    label "niszczy&#263;"
  ]
  node [
    id 126
    label "str&#261;ca&#263;"
  ]
  node [
    id 127
    label "beat"
  ]
  node [
    id 128
    label "butcher"
  ]
  node [
    id 129
    label "przerabia&#263;"
  ]
  node [
    id 130
    label "t&#322;uc"
  ]
  node [
    id 131
    label "wpiernicza&#263;"
  ]
  node [
    id 132
    label "balansjerka"
  ]
  node [
    id 133
    label "murder"
  ]
  node [
    id 134
    label "krzywdzi&#263;"
  ]
  node [
    id 135
    label "rap"
  ]
  node [
    id 136
    label "napierdziela&#263;"
  ]
  node [
    id 137
    label "pra&#263;"
  ]
  node [
    id 138
    label "usuwa&#263;"
  ]
  node [
    id 139
    label "chop"
  ]
  node [
    id 140
    label "skuwa&#263;"
  ]
  node [
    id 141
    label "macha&#263;"
  ]
  node [
    id 142
    label "t&#322;oczy&#263;"
  ]
  node [
    id 143
    label "peddle"
  ]
  node [
    id 144
    label "tug"
  ]
  node [
    id 145
    label "&#322;adowa&#263;"
  ]
  node [
    id 146
    label "powodowa&#263;"
  ]
  node [
    id 147
    label "uderza&#263;"
  ]
  node [
    id 148
    label "&#380;&#322;obi&#263;"
  ]
  node [
    id 149
    label "zwalcza&#263;"
  ]
  node [
    id 150
    label "record"
  ]
  node [
    id 151
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 152
    label "baza_danych"
  ]
  node [
    id 153
    label "dane"
  ]
  node [
    id 154
    label "szczyt"
  ]
  node [
    id 155
    label "popularity"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 10
    target 43
  ]
  edge [
    source 10
    target 60
  ]
  edge [
    source 10
    target 61
  ]
  edge [
    source 10
    target 62
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 63
  ]
  edge [
    source 11
    target 64
  ]
  edge [
    source 11
    target 65
  ]
  edge [
    source 11
    target 66
  ]
  edge [
    source 11
    target 67
  ]
  edge [
    source 11
    target 68
  ]
  edge [
    source 11
    target 69
  ]
  edge [
    source 11
    target 70
  ]
  edge [
    source 11
    target 71
  ]
  edge [
    source 11
    target 72
  ]
  edge [
    source 11
    target 73
  ]
  edge [
    source 11
    target 74
  ]
  edge [
    source 11
    target 75
  ]
  edge [
    source 11
    target 76
  ]
  edge [
    source 11
    target 77
  ]
  edge [
    source 11
    target 18
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 78
  ]
  edge [
    source 12
    target 79
  ]
  edge [
    source 12
    target 80
  ]
  edge [
    source 12
    target 81
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 82
  ]
  edge [
    source 13
    target 83
  ]
  edge [
    source 13
    target 84
  ]
  edge [
    source 13
    target 85
  ]
  edge [
    source 13
    target 86
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 87
  ]
  edge [
    source 14
    target 88
  ]
  edge [
    source 14
    target 89
  ]
  edge [
    source 14
    target 90
  ]
  edge [
    source 14
    target 91
  ]
  edge [
    source 14
    target 92
  ]
  edge [
    source 14
    target 93
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 94
  ]
  edge [
    source 15
    target 95
  ]
  edge [
    source 15
    target 96
  ]
  edge [
    source 15
    target 97
  ]
  edge [
    source 15
    target 98
  ]
  edge [
    source 15
    target 99
  ]
  edge [
    source 15
    target 100
  ]
  edge [
    source 15
    target 101
  ]
  edge [
    source 15
    target 102
  ]
  edge [
    source 15
    target 103
  ]
  edge [
    source 15
    target 104
  ]
  edge [
    source 15
    target 105
  ]
  edge [
    source 15
    target 106
  ]
  edge [
    source 15
    target 107
  ]
  edge [
    source 15
    target 108
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 109
  ]
  edge [
    source 16
    target 110
  ]
  edge [
    source 16
    target 111
  ]
  edge [
    source 16
    target 112
  ]
  edge [
    source 16
    target 113
  ]
  edge [
    source 16
    target 114
  ]
  edge [
    source 16
    target 115
  ]
  edge [
    source 16
    target 116
  ]
  edge [
    source 16
    target 117
  ]
  edge [
    source 16
    target 118
  ]
  edge [
    source 16
    target 119
  ]
  edge [
    source 16
    target 120
  ]
  edge [
    source 16
    target 121
  ]
  edge [
    source 16
    target 122
  ]
  edge [
    source 16
    target 123
  ]
  edge [
    source 16
    target 124
  ]
  edge [
    source 16
    target 125
  ]
  edge [
    source 16
    target 126
  ]
  edge [
    source 16
    target 127
  ]
  edge [
    source 16
    target 128
  ]
  edge [
    source 16
    target 129
  ]
  edge [
    source 16
    target 130
  ]
  edge [
    source 16
    target 131
  ]
  edge [
    source 16
    target 132
  ]
  edge [
    source 16
    target 133
  ]
  edge [
    source 16
    target 134
  ]
  edge [
    source 16
    target 135
  ]
  edge [
    source 16
    target 136
  ]
  edge [
    source 16
    target 137
  ]
  edge [
    source 16
    target 138
  ]
  edge [
    source 16
    target 139
  ]
  edge [
    source 16
    target 140
  ]
  edge [
    source 16
    target 141
  ]
  edge [
    source 16
    target 142
  ]
  edge [
    source 16
    target 143
  ]
  edge [
    source 16
    target 144
  ]
  edge [
    source 16
    target 145
  ]
  edge [
    source 16
    target 146
  ]
  edge [
    source 16
    target 147
  ]
  edge [
    source 16
    target 148
  ]
  edge [
    source 16
    target 149
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 150
  ]
  edge [
    source 17
    target 151
  ]
  edge [
    source 17
    target 152
  ]
  edge [
    source 17
    target 153
  ]
  edge [
    source 17
    target 154
  ]
  edge [
    source 18
    target 155
  ]
  edge [
    source 18
    target 68
  ]
]
