graph [
  maxDegree 46
  minDegree 1
  meanDegree 2.2379310344827585
  density 0.0038651658626645225
  graphCliqueNumber 4
  node [
    id 0
    label "tymczasem"
    origin "text"
  ]
  node [
    id 1
    label "nad"
    origin "text"
  ]
  node [
    id 2
    label "wis&#322;a"
    origin "text"
  ]
  node [
    id 3
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 4
    label "wyrazi&#263;"
    origin "text"
  ]
  node [
    id 5
    label "&#380;al"
    origin "text"
  ]
  node [
    id 6
    label "rocznica"
    origin "text"
  ]
  node [
    id 7
    label "by&#263;"
    origin "text"
  ]
  node [
    id 8
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 9
    label "wszyscy"
    origin "text"
  ]
  node [
    id 10
    label "&#347;rodowisko"
    origin "text"
  ]
  node [
    id 11
    label "przynajmniej"
    origin "text"
  ]
  node [
    id 12
    label "okazja"
    origin "text"
  ]
  node [
    id 13
    label "refleksja"
    origin "text"
  ]
  node [
    id 14
    label "zaduma"
    origin "text"
  ]
  node [
    id 15
    label "kr&#281;ty"
    origin "text"
  ]
  node [
    id 16
    label "&#347;cie&#380;ek"
    origin "text"
  ]
  node [
    id 17
    label "polski"
    origin "text"
  ]
  node [
    id 18
    label "historia"
    origin "text"
  ]
  node [
    id 19
    label "lecz"
    origin "text"
  ]
  node [
    id 20
    label "si&#281;"
    origin "text"
  ]
  node [
    id 21
    label "manifestacja"
    origin "text"
  ]
  node [
    id 22
    label "odwet"
    origin "text"
  ]
  node [
    id 23
    label "resentyment"
    origin "text"
  ]
  node [
    id 24
    label "staj"
    origin "text"
  ]
  node [
    id 25
    label "dla"
    origin "text"
  ]
  node [
    id 26
    label "wiele"
    origin "text"
  ]
  node [
    id 27
    label "nienawi&#347;&#263;"
    origin "text"
  ]
  node [
    id 28
    label "&#380;&#261;danie"
    origin "text"
  ]
  node [
    id 29
    label "wobec"
    origin "text"
  ]
  node [
    id 30
    label "dawny"
    origin "text"
  ]
  node [
    id 31
    label "wr&#243;g"
    origin "text"
  ]
  node [
    id 32
    label "wykluczy&#263;"
    origin "text"
  ]
  node [
    id 33
    label "odcz&#322;owieczy&#263;"
    origin "text"
  ]
  node [
    id 34
    label "szczeg&#243;lnie"
    origin "text"
  ]
  node [
    id 35
    label "oburzaj&#261;cy"
    origin "text"
  ]
  node [
    id 36
    label "p&#322;yn&#261;&#263;"
    origin "text"
  ]
  node [
    id 37
    label "strona"
    origin "text"
  ]
  node [
    id 38
    label "niekt&#243;ry"
    origin "text"
  ]
  node [
    id 39
    label "atak"
    origin "text"
  ]
  node [
    id 40
    label "schorowany"
    origin "text"
  ]
  node [
    id 41
    label "genera&#322;"
    origin "text"
  ]
  node [
    id 42
    label "jaruzelski"
    origin "text"
  ]
  node [
    id 43
    label "przecie&#380;"
    origin "text"
  ]
  node [
    id 44
    label "jaros&#322;aw"
    origin "text"
  ]
  node [
    id 45
    label "kaczy&#324;ski"
    origin "text"
  ]
  node [
    id 46
    label "por&#243;wna&#263;"
    origin "text"
  ]
  node [
    id 47
    label "adolf"
    origin "text"
  ]
  node [
    id 48
    label "eichmann"
    origin "text"
  ]
  node [
    id 49
    label "polak"
    origin "text"
  ]
  node [
    id 50
    label "kto&#347;"
    origin "text"
  ]
  node [
    id 51
    label "podoba"
    origin "text"
  ]
  node [
    id 52
    label "pozostawa&#263;"
    origin "text"
  ]
  node [
    id 53
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 54
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 55
    label "zapobiec"
    origin "text"
  ]
  node [
    id 56
    label "narodowy"
    origin "text"
  ]
  node [
    id 57
    label "tragedia"
    origin "text"
  ]
  node [
    id 58
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 59
    label "muszy"
    origin "text"
  ]
  node [
    id 60
    label "up&#322;yn&#261;&#263;"
    origin "text"
  ]
  node [
    id 61
    label "jeszcze"
    origin "text"
  ]
  node [
    id 62
    label "lata"
    origin "text"
  ]
  node [
    id 63
    label "aby"
    origin "text"
  ]
  node [
    id 64
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 65
    label "spe&#322;ni&#263;"
    origin "text"
  ]
  node [
    id 66
    label "wszelki"
    origin "text"
  ]
  node [
    id 67
    label "warunek"
    origin "text"
  ]
  node [
    id 68
    label "obiektywny"
    origin "text"
  ]
  node [
    id 69
    label "ocena"
    origin "text"
  ]
  node [
    id 70
    label "wewn&#281;trzny"
    origin "text"
  ]
  node [
    id 71
    label "zewn&#281;trzny"
    origin "text"
  ]
  node [
    id 72
    label "przes&#322;anka"
    origin "text"
  ]
  node [
    id 73
    label "decyzja"
    origin "text"
  ]
  node [
    id 74
    label "podj&#261;&#263;"
    origin "text"
  ]
  node [
    id 75
    label "pami&#281;tne"
    origin "text"
  ]
  node [
    id 76
    label "grudzie&#324;"
    origin "text"
  ]
  node [
    id 77
    label "rozmaity"
    origin "text"
  ]
  node [
    id 78
    label "skutek"
    origin "text"
  ]
  node [
    id 79
    label "kr&#243;tko"
    origin "text"
  ]
  node [
    id 80
    label "d&#322;ugoterminowy"
    origin "text"
  ]
  node [
    id 81
    label "pojednanie"
    origin "text"
  ]
  node [
    id 82
    label "przebaczenie"
    origin "text"
  ]
  node [
    id 83
    label "chyba"
    origin "text"
  ]
  node [
    id 84
    label "powinienby&#263;"
    origin "text"
  ]
  node [
    id 85
    label "czeka&#263;"
    origin "text"
  ]
  node [
    id 86
    label "d&#322;ugo"
    origin "text"
  ]
  node [
    id 87
    label "&#263;wier&#263;"
    origin "text"
  ]
  node [
    id 88
    label "wiek"
    origin "text"
  ]
  node [
    id 89
    label "szmat"
    origin "text"
  ]
  node [
    id 90
    label "czas"
    origin "text"
  ]
  node [
    id 91
    label "alternatywa"
    origin "text"
  ]
  node [
    id 92
    label "jedynie"
    origin "text"
  ]
  node [
    id 93
    label "pog&#322;&#281;bia&#263;"
    origin "text"
  ]
  node [
    id 94
    label "istnie&#263;"
    origin "text"
  ]
  node [
    id 95
    label "podzia&#322;"
    origin "text"
  ]
  node [
    id 96
    label "czasowo"
  ]
  node [
    id 97
    label "wtedy"
  ]
  node [
    id 98
    label "free"
  ]
  node [
    id 99
    label "zdradzi&#263;_si&#281;"
  ]
  node [
    id 100
    label "zakomunikowa&#263;"
  ]
  node [
    id 101
    label "vent"
  ]
  node [
    id 102
    label "oznaczy&#263;"
  ]
  node [
    id 103
    label "testify"
  ]
  node [
    id 104
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 105
    label "niezadowolenie"
  ]
  node [
    id 106
    label "wstyd"
  ]
  node [
    id 107
    label "gniewa&#263;_si&#281;"
  ]
  node [
    id 108
    label "gniewanie_si&#281;"
  ]
  node [
    id 109
    label "smutek"
  ]
  node [
    id 110
    label "czu&#263;"
  ]
  node [
    id 111
    label "emocja"
  ]
  node [
    id 112
    label "commiseration"
  ]
  node [
    id 113
    label "pang"
  ]
  node [
    id 114
    label "krytyka"
  ]
  node [
    id 115
    label "pogniewa&#263;_si&#281;"
  ]
  node [
    id 116
    label "uraza"
  ]
  node [
    id 117
    label "criticism"
  ]
  node [
    id 118
    label "sorrow"
  ]
  node [
    id 119
    label "pogniewanie_si&#281;"
  ]
  node [
    id 120
    label "sytuacja"
  ]
  node [
    id 121
    label "termin"
  ]
  node [
    id 122
    label "obchody"
  ]
  node [
    id 123
    label "si&#281;ga&#263;"
  ]
  node [
    id 124
    label "trwa&#263;"
  ]
  node [
    id 125
    label "obecno&#347;&#263;"
  ]
  node [
    id 126
    label "stan"
  ]
  node [
    id 127
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 128
    label "stand"
  ]
  node [
    id 129
    label "mie&#263;_miejsce"
  ]
  node [
    id 130
    label "uczestniczy&#263;"
  ]
  node [
    id 131
    label "chodzi&#263;"
  ]
  node [
    id 132
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 133
    label "equal"
  ]
  node [
    id 134
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 135
    label "obiekt_naturalny"
  ]
  node [
    id 136
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 137
    label "grupa"
  ]
  node [
    id 138
    label "stw&#243;r"
  ]
  node [
    id 139
    label "rzecz"
  ]
  node [
    id 140
    label "environment"
  ]
  node [
    id 141
    label "biota"
  ]
  node [
    id 142
    label "wszechstworzenie"
  ]
  node [
    id 143
    label "otoczenie"
  ]
  node [
    id 144
    label "fauna"
  ]
  node [
    id 145
    label "ekosystem"
  ]
  node [
    id 146
    label "teren"
  ]
  node [
    id 147
    label "mikrokosmos"
  ]
  node [
    id 148
    label "class"
  ]
  node [
    id 149
    label "zesp&#243;&#322;"
  ]
  node [
    id 150
    label "warunki"
  ]
  node [
    id 151
    label "huczek"
  ]
  node [
    id 152
    label "Ziemia"
  ]
  node [
    id 153
    label "woda"
  ]
  node [
    id 154
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 155
    label "atrakcyjny"
  ]
  node [
    id 156
    label "oferta"
  ]
  node [
    id 157
    label "adeptness"
  ]
  node [
    id 158
    label "okazka"
  ]
  node [
    id 159
    label "wydarzenie"
  ]
  node [
    id 160
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 161
    label "podw&#243;zka"
  ]
  node [
    id 162
    label "autostop"
  ]
  node [
    id 163
    label "my&#347;l"
  ]
  node [
    id 164
    label "meditation"
  ]
  node [
    id 165
    label "namys&#322;"
  ]
  node [
    id 166
    label "nastr&#243;j"
  ]
  node [
    id 167
    label "contemplation"
  ]
  node [
    id 168
    label "zakrzywiony"
  ]
  node [
    id 169
    label "niezrozumia&#322;y"
  ]
  node [
    id 170
    label "t&#322;umaczenie"
  ]
  node [
    id 171
    label "kr&#281;to"
  ]
  node [
    id 172
    label "lacki"
  ]
  node [
    id 173
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 174
    label "przedmiot"
  ]
  node [
    id 175
    label "sztajer"
  ]
  node [
    id 176
    label "drabant"
  ]
  node [
    id 177
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 178
    label "pierogi_ruskie"
  ]
  node [
    id 179
    label "krakowiak"
  ]
  node [
    id 180
    label "Polish"
  ]
  node [
    id 181
    label "j&#281;zyk"
  ]
  node [
    id 182
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 183
    label "oberek"
  ]
  node [
    id 184
    label "po_polsku"
  ]
  node [
    id 185
    label "mazur"
  ]
  node [
    id 186
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 187
    label "chodzony"
  ]
  node [
    id 188
    label "skoczny"
  ]
  node [
    id 189
    label "ryba_po_grecku"
  ]
  node [
    id 190
    label "goniony"
  ]
  node [
    id 191
    label "polsko"
  ]
  node [
    id 192
    label "report"
  ]
  node [
    id 193
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 194
    label "wypowied&#378;"
  ]
  node [
    id 195
    label "neografia"
  ]
  node [
    id 196
    label "papirologia"
  ]
  node [
    id 197
    label "historia_gospodarcza"
  ]
  node [
    id 198
    label "przebiec"
  ]
  node [
    id 199
    label "hista"
  ]
  node [
    id 200
    label "nauka_humanistyczna"
  ]
  node [
    id 201
    label "filigranistyka"
  ]
  node [
    id 202
    label "dyplomatyka"
  ]
  node [
    id 203
    label "annalistyka"
  ]
  node [
    id 204
    label "historyka"
  ]
  node [
    id 205
    label "heraldyka"
  ]
  node [
    id 206
    label "fabu&#322;a"
  ]
  node [
    id 207
    label "muzealnictwo"
  ]
  node [
    id 208
    label "varsavianistyka"
  ]
  node [
    id 209
    label "prezentyzm"
  ]
  node [
    id 210
    label "mediewistyka"
  ]
  node [
    id 211
    label "przebiegni&#281;cie"
  ]
  node [
    id 212
    label "charakter"
  ]
  node [
    id 213
    label "paleografia"
  ]
  node [
    id 214
    label "genealogia"
  ]
  node [
    id 215
    label "czynno&#347;&#263;"
  ]
  node [
    id 216
    label "prozopografia"
  ]
  node [
    id 217
    label "motyw"
  ]
  node [
    id 218
    label "nautologia"
  ]
  node [
    id 219
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 220
    label "epoka"
  ]
  node [
    id 221
    label "numizmatyka"
  ]
  node [
    id 222
    label "ruralistyka"
  ]
  node [
    id 223
    label "epigrafika"
  ]
  node [
    id 224
    label "&#378;r&#243;d&#322;oznawstwo"
  ]
  node [
    id 225
    label "historiografia"
  ]
  node [
    id 226
    label "bizantynistyka"
  ]
  node [
    id 227
    label "weksylologia"
  ]
  node [
    id 228
    label "kierunek"
  ]
  node [
    id 229
    label "ikonografia"
  ]
  node [
    id 230
    label "chronologia"
  ]
  node [
    id 231
    label "archiwistyka"
  ]
  node [
    id 232
    label "sfragistyka"
  ]
  node [
    id 233
    label "zabytkoznawstwo"
  ]
  node [
    id 234
    label "historia_sztuki"
  ]
  node [
    id 235
    label "exhibition"
  ]
  node [
    id 236
    label "zgromadzenie"
  ]
  node [
    id 237
    label "show"
  ]
  node [
    id 238
    label "pokaz"
  ]
  node [
    id 239
    label "reakcja"
  ]
  node [
    id 240
    label "wiela"
  ]
  node [
    id 241
    label "du&#380;y"
  ]
  node [
    id 242
    label "uroszczenie"
  ]
  node [
    id 243
    label "request"
  ]
  node [
    id 244
    label "dopominanie_si&#281;"
  ]
  node [
    id 245
    label "claim"
  ]
  node [
    id 246
    label "przesz&#322;y"
  ]
  node [
    id 247
    label "dawno"
  ]
  node [
    id 248
    label "dawniej"
  ]
  node [
    id 249
    label "kombatant"
  ]
  node [
    id 250
    label "stary"
  ]
  node [
    id 251
    label "odleg&#322;y"
  ]
  node [
    id 252
    label "anachroniczny"
  ]
  node [
    id 253
    label "przestarza&#322;y"
  ]
  node [
    id 254
    label "od_dawna"
  ]
  node [
    id 255
    label "poprzedni"
  ]
  node [
    id 256
    label "d&#322;ugoletni"
  ]
  node [
    id 257
    label "wcze&#347;niejszy"
  ]
  node [
    id 258
    label "niegdysiejszy"
  ]
  node [
    id 259
    label "czynnik"
  ]
  node [
    id 260
    label "przeciwnik"
  ]
  node [
    id 261
    label "posta&#263;"
  ]
  node [
    id 262
    label "wojna"
  ]
  node [
    id 263
    label "challenge"
  ]
  node [
    id 264
    label "odrzuci&#263;"
  ]
  node [
    id 265
    label "spowodowa&#263;"
  ]
  node [
    id 266
    label "od&#322;&#261;czy&#263;"
  ]
  node [
    id 267
    label "nieprawdopodobie&#324;stwo"
  ]
  node [
    id 268
    label "strike"
  ]
  node [
    id 269
    label "wykluczenie"
  ]
  node [
    id 270
    label "barroom"
  ]
  node [
    id 271
    label "dehumanize"
  ]
  node [
    id 272
    label "upodli&#263;"
  ]
  node [
    id 273
    label "szczeg&#243;lny"
  ]
  node [
    id 274
    label "osobnie"
  ]
  node [
    id 275
    label "wyj&#261;tkowo"
  ]
  node [
    id 276
    label "specially"
  ]
  node [
    id 277
    label "skandalicznie"
  ]
  node [
    id 278
    label "straszny"
  ]
  node [
    id 279
    label "gorsz&#261;cy"
  ]
  node [
    id 280
    label "rozchodzi&#263;_si&#281;"
  ]
  node [
    id 281
    label "przy&#322;&#261;czy&#263;"
  ]
  node [
    id 282
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 283
    label "rise"
  ]
  node [
    id 284
    label "proceed"
  ]
  node [
    id 285
    label "rodzi&#263;_si&#281;"
  ]
  node [
    id 286
    label "lecie&#263;"
  ]
  node [
    id 287
    label "run"
  ]
  node [
    id 288
    label "toczy&#263;_si&#281;"
  ]
  node [
    id 289
    label "przy&#322;&#261;cza&#263;"
  ]
  node [
    id 290
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 291
    label "biega&#263;"
  ]
  node [
    id 292
    label "zwierz&#281;"
  ]
  node [
    id 293
    label "bie&#380;e&#263;"
  ]
  node [
    id 294
    label "tent-fly"
  ]
  node [
    id 295
    label "carry"
  ]
  node [
    id 296
    label "skr&#281;canie"
  ]
  node [
    id 297
    label "voice"
  ]
  node [
    id 298
    label "forma"
  ]
  node [
    id 299
    label "internet"
  ]
  node [
    id 300
    label "skr&#281;ci&#263;"
  ]
  node [
    id 301
    label "kartka"
  ]
  node [
    id 302
    label "orientowa&#263;"
  ]
  node [
    id 303
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 304
    label "powierzchnia"
  ]
  node [
    id 305
    label "plik"
  ]
  node [
    id 306
    label "bok"
  ]
  node [
    id 307
    label "pagina"
  ]
  node [
    id 308
    label "orientowanie"
  ]
  node [
    id 309
    label "fragment"
  ]
  node [
    id 310
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 311
    label "s&#261;d"
  ]
  node [
    id 312
    label "skr&#281;ca&#263;"
  ]
  node [
    id 313
    label "g&#243;ra"
  ]
  node [
    id 314
    label "serwis_internetowy"
  ]
  node [
    id 315
    label "orientacja"
  ]
  node [
    id 316
    label "linia"
  ]
  node [
    id 317
    label "skr&#281;cenie"
  ]
  node [
    id 318
    label "layout"
  ]
  node [
    id 319
    label "zorientowa&#263;"
  ]
  node [
    id 320
    label "zorientowanie"
  ]
  node [
    id 321
    label "obiekt"
  ]
  node [
    id 322
    label "podmiot"
  ]
  node [
    id 323
    label "ty&#322;"
  ]
  node [
    id 324
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 325
    label "logowanie"
  ]
  node [
    id 326
    label "adres_internetowy"
  ]
  node [
    id 327
    label "uj&#281;cie"
  ]
  node [
    id 328
    label "prz&#243;d"
  ]
  node [
    id 329
    label "jaki&#347;"
  ]
  node [
    id 330
    label "manewr"
  ]
  node [
    id 331
    label "spasm"
  ]
  node [
    id 332
    label "rzucenie"
  ]
  node [
    id 333
    label "pogorszenie"
  ]
  node [
    id 334
    label "stroke"
  ]
  node [
    id 335
    label "liga"
  ]
  node [
    id 336
    label "pozycja"
  ]
  node [
    id 337
    label "zagrywka"
  ]
  node [
    id 338
    label "rzuci&#263;"
  ]
  node [
    id 339
    label "walka"
  ]
  node [
    id 340
    label "przemoc"
  ]
  node [
    id 341
    label "pogoda"
  ]
  node [
    id 342
    label "przyp&#322;yw"
  ]
  node [
    id 343
    label "fit"
  ]
  node [
    id 344
    label "ofensywa"
  ]
  node [
    id 345
    label "knock"
  ]
  node [
    id 346
    label "oznaka"
  ]
  node [
    id 347
    label "bat"
  ]
  node [
    id 348
    label "kaszel"
  ]
  node [
    id 349
    label "s&#322;abowity"
  ]
  node [
    id 350
    label "Ko&#347;ciuszko"
  ]
  node [
    id 351
    label "starosta"
  ]
  node [
    id 352
    label "stopie&#324;_wojskowy"
  ]
  node [
    id 353
    label "Moczar"
  ]
  node [
    id 354
    label "oficer"
  ]
  node [
    id 355
    label "jenera&#322;"
  ]
  node [
    id 356
    label "Franco"
  ]
  node [
    id 357
    label "Maczek"
  ]
  node [
    id 358
    label "sejmik"
  ]
  node [
    id 359
    label "Anders"
  ]
  node [
    id 360
    label "zakonnik"
  ]
  node [
    id 361
    label "zwierzchnik"
  ]
  node [
    id 362
    label "zanalizowa&#263;"
  ]
  node [
    id 363
    label "compose"
  ]
  node [
    id 364
    label "znaczenie"
  ]
  node [
    id 365
    label "go&#347;&#263;"
  ]
  node [
    id 366
    label "osoba"
  ]
  node [
    id 367
    label "ostawa&#263;_si&#281;"
  ]
  node [
    id 368
    label "stop"
  ]
  node [
    id 369
    label "utrzymywa&#263;_si&#281;"
  ]
  node [
    id 370
    label "przebywa&#263;"
  ]
  node [
    id 371
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 372
    label "support"
  ]
  node [
    id 373
    label "blend"
  ]
  node [
    id 374
    label "zachowywa&#263;_si&#281;"
  ]
  node [
    id 375
    label "prze&#380;ywa&#263;"
  ]
  node [
    id 376
    label "asymilowa&#263;"
  ]
  node [
    id 377
    label "wapniak"
  ]
  node [
    id 378
    label "dwun&#243;g"
  ]
  node [
    id 379
    label "polifag"
  ]
  node [
    id 380
    label "wz&#243;r"
  ]
  node [
    id 381
    label "profanum"
  ]
  node [
    id 382
    label "hominid"
  ]
  node [
    id 383
    label "homo_sapiens"
  ]
  node [
    id 384
    label "nasada"
  ]
  node [
    id 385
    label "podw&#322;adny"
  ]
  node [
    id 386
    label "ludzko&#347;&#263;"
  ]
  node [
    id 387
    label "os&#322;abianie"
  ]
  node [
    id 388
    label "portrecista"
  ]
  node [
    id 389
    label "duch"
  ]
  node [
    id 390
    label "g&#322;owa"
  ]
  node [
    id 391
    label "oddzia&#322;ywanie"
  ]
  node [
    id 392
    label "asymilowanie"
  ]
  node [
    id 393
    label "os&#322;abia&#263;"
  ]
  node [
    id 394
    label "figura"
  ]
  node [
    id 395
    label "Adam"
  ]
  node [
    id 396
    label "senior"
  ]
  node [
    id 397
    label "antropochoria"
  ]
  node [
    id 398
    label "zapobie&#380;e&#263;"
  ]
  node [
    id 399
    label "zrobi&#263;"
  ]
  node [
    id 400
    label "cook"
  ]
  node [
    id 401
    label "nacjonalistyczny"
  ]
  node [
    id 402
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 403
    label "narodowo"
  ]
  node [
    id 404
    label "wa&#380;ny"
  ]
  node [
    id 405
    label "lipa"
  ]
  node [
    id 406
    label "cios"
  ]
  node [
    id 407
    label "dramat"
  ]
  node [
    id 408
    label "gatunek_literacki"
  ]
  node [
    id 409
    label "play"
  ]
  node [
    id 410
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 411
    label "ci&#261;gle"
  ]
  node [
    id 412
    label "summer"
  ]
  node [
    id 413
    label "troch&#281;"
  ]
  node [
    id 414
    label "catch"
  ]
  node [
    id 415
    label "pozosta&#263;"
  ]
  node [
    id 416
    label "osta&#263;_si&#281;"
  ]
  node [
    id 417
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 418
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 419
    label "change"
  ]
  node [
    id 420
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 421
    label "play_along"
  ]
  node [
    id 422
    label "urzeczywistni&#263;"
  ]
  node [
    id 423
    label "perform"
  ]
  node [
    id 424
    label "ka&#380;dy"
  ]
  node [
    id 425
    label "za&#322;o&#380;enie"
  ]
  node [
    id 426
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 427
    label "umowa"
  ]
  node [
    id 428
    label "agent"
  ]
  node [
    id 429
    label "condition"
  ]
  node [
    id 430
    label "ekspozycja"
  ]
  node [
    id 431
    label "faktor"
  ]
  node [
    id 432
    label "niezale&#380;ny"
  ]
  node [
    id 433
    label "zobiektywizowanie"
  ]
  node [
    id 434
    label "bezsporny"
  ]
  node [
    id 435
    label "uczciwy"
  ]
  node [
    id 436
    label "neutralny"
  ]
  node [
    id 437
    label "faktyczny"
  ]
  node [
    id 438
    label "obiektywnie"
  ]
  node [
    id 439
    label "obiektywizowanie"
  ]
  node [
    id 440
    label "informacja"
  ]
  node [
    id 441
    label "sofcik"
  ]
  node [
    id 442
    label "appraisal"
  ]
  node [
    id 443
    label "pogl&#261;d"
  ]
  node [
    id 444
    label "kryterium"
  ]
  node [
    id 445
    label "wewn&#281;trznie"
  ]
  node [
    id 446
    label "wn&#281;trzny"
  ]
  node [
    id 447
    label "psychiczny"
  ]
  node [
    id 448
    label "numer"
  ]
  node [
    id 449
    label "oddzia&#322;"
  ]
  node [
    id 450
    label "zewn&#281;trznie"
  ]
  node [
    id 451
    label "na_prawo"
  ]
  node [
    id 452
    label "z_prawa"
  ]
  node [
    id 453
    label "powierzchny"
  ]
  node [
    id 454
    label "wnioskowanie"
  ]
  node [
    id 455
    label "przyczyna"
  ]
  node [
    id 456
    label "proces"
  ]
  node [
    id 457
    label "fakt"
  ]
  node [
    id 458
    label "dokument"
  ]
  node [
    id 459
    label "resolution"
  ]
  node [
    id 460
    label "zdecydowanie"
  ]
  node [
    id 461
    label "wytw&#243;r"
  ]
  node [
    id 462
    label "podj&#281;cie_decyzji"
  ]
  node [
    id 463
    label "management"
  ]
  node [
    id 464
    label "zmieni&#263;"
  ]
  node [
    id 465
    label "zacz&#261;&#263;"
  ]
  node [
    id 466
    label "zareagowa&#263;"
  ]
  node [
    id 467
    label "allude"
  ]
  node [
    id 468
    label "raise"
  ]
  node [
    id 469
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 470
    label "draw"
  ]
  node [
    id 471
    label "op&#322;ata"
  ]
  node [
    id 472
    label "Bo&#380;e_Narodzenie"
  ]
  node [
    id 473
    label "miesi&#261;c"
  ]
  node [
    id 474
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 475
    label "Barb&#243;rka"
  ]
  node [
    id 476
    label "Sylwester"
  ]
  node [
    id 477
    label "inny"
  ]
  node [
    id 478
    label "r&#243;&#380;norodnie"
  ]
  node [
    id 479
    label "r&#243;&#380;nie"
  ]
  node [
    id 480
    label "zr&#243;&#380;nicowany"
  ]
  node [
    id 481
    label "rezultat"
  ]
  node [
    id 482
    label "kr&#243;tki"
  ]
  node [
    id 483
    label "d&#322;ugoterminowo"
  ]
  node [
    id 484
    label "przysz&#322;o&#347;ciowy"
  ]
  node [
    id 485
    label "d&#322;ugi"
  ]
  node [
    id 486
    label "czasowy"
  ]
  node [
    id 487
    label "zgoda"
  ]
  node [
    id 488
    label "umo&#380;liwienie"
  ]
  node [
    id 489
    label "reconciliation"
  ]
  node [
    id 490
    label "rozgrzeszenie"
  ]
  node [
    id 491
    label "zrobienie"
  ]
  node [
    id 492
    label "forgiveness"
  ]
  node [
    id 493
    label "puszczenie_p&#322;azem"
  ]
  node [
    id 494
    label "hold"
  ]
  node [
    id 495
    label "sp&#281;dza&#263;"
  ]
  node [
    id 496
    label "look"
  ]
  node [
    id 497
    label "decydowa&#263;"
  ]
  node [
    id 498
    label "oczekiwa&#263;"
  ]
  node [
    id 499
    label "pauzowa&#263;"
  ]
  node [
    id 500
    label "anticipate"
  ]
  node [
    id 501
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 502
    label "period"
  ]
  node [
    id 503
    label "rok"
  ]
  node [
    id 504
    label "cecha"
  ]
  node [
    id 505
    label "long_time"
  ]
  node [
    id 506
    label "choroba_wieku"
  ]
  node [
    id 507
    label "jednostka_geologiczna"
  ]
  node [
    id 508
    label "chron"
  ]
  node [
    id 509
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 510
    label "obszar"
  ]
  node [
    id 511
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 512
    label "mn&#243;stwo"
  ]
  node [
    id 513
    label "sp&#322;ache&#263;"
  ]
  node [
    id 514
    label "czasokres"
  ]
  node [
    id 515
    label "trawienie"
  ]
  node [
    id 516
    label "kategoria_gramatyczna"
  ]
  node [
    id 517
    label "odczyt"
  ]
  node [
    id 518
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 519
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 520
    label "chwila"
  ]
  node [
    id 521
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 522
    label "poprzedzenie"
  ]
  node [
    id 523
    label "koniugacja"
  ]
  node [
    id 524
    label "dzieje"
  ]
  node [
    id 525
    label "poprzedzi&#263;"
  ]
  node [
    id 526
    label "przep&#322;ywanie"
  ]
  node [
    id 527
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 528
    label "odwlekanie_si&#281;"
  ]
  node [
    id 529
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 530
    label "Zeitgeist"
  ]
  node [
    id 531
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 532
    label "okres_czasu"
  ]
  node [
    id 533
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 534
    label "pochodzi&#263;"
  ]
  node [
    id 535
    label "schy&#322;ek"
  ]
  node [
    id 536
    label "czwarty_wymiar"
  ]
  node [
    id 537
    label "chronometria"
  ]
  node [
    id 538
    label "poprzedzanie"
  ]
  node [
    id 539
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 540
    label "zegar"
  ]
  node [
    id 541
    label "trawi&#263;"
  ]
  node [
    id 542
    label "pochodzenie"
  ]
  node [
    id 543
    label "poprzedza&#263;"
  ]
  node [
    id 544
    label "time_period"
  ]
  node [
    id 545
    label "rachuba_czasu"
  ]
  node [
    id 546
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 547
    label "czasoprzestrze&#324;"
  ]
  node [
    id 548
    label "laba"
  ]
  node [
    id 549
    label "problem"
  ]
  node [
    id 550
    label "ruch"
  ]
  node [
    id 551
    label "zdecydowa&#263;_si&#281;"
  ]
  node [
    id 552
    label "zdecydowanie_si&#281;"
  ]
  node [
    id 553
    label "rozwi&#261;zanie"
  ]
  node [
    id 554
    label "ewentualno&#347;&#263;"
  ]
  node [
    id 555
    label "obrabia&#263;"
  ]
  node [
    id 556
    label "powi&#281;ksza&#263;"
  ]
  node [
    id 557
    label "sink"
  ]
  node [
    id 558
    label "escalate"
  ]
  node [
    id 559
    label "wzmaga&#263;"
  ]
  node [
    id 560
    label "competence"
  ]
  node [
    id 561
    label "eksdywizja"
  ]
  node [
    id 562
    label "stopie&#324;"
  ]
  node [
    id 563
    label "blastogeneza"
  ]
  node [
    id 564
    label "fission"
  ]
  node [
    id 565
    label "distribution"
  ]
  node [
    id 566
    label "Jaros&#322;awa"
  ]
  node [
    id 567
    label "Kaczy&#324;ski"
  ]
  node [
    id 568
    label "Adolfa"
  ]
  node [
    id 569
    label "Eichmann"
  ]
  node [
    id 570
    label "lewica"
  ]
  node [
    id 571
    label "i"
  ]
  node [
    id 572
    label "demokrata"
  ]
  node [
    id 573
    label "Krzysztofa"
  ]
  node [
    id 574
    label "Putra"
  ]
  node [
    id 575
    label "Artur"
  ]
  node [
    id 576
    label "g&#243;rski"
  ]
  node [
    id 577
    label "prawo"
  ]
  node [
    id 578
    label "sprawiedliwo&#347;&#263;"
  ]
  node [
    id 579
    label "prawy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 35
  ]
  edge [
    source 7
    target 36
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 123
  ]
  edge [
    source 7
    target 124
  ]
  edge [
    source 7
    target 125
  ]
  edge [
    source 7
    target 126
  ]
  edge [
    source 7
    target 127
  ]
  edge [
    source 7
    target 128
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 7
    target 130
  ]
  edge [
    source 7
    target 131
  ]
  edge [
    source 7
    target 132
  ]
  edge [
    source 7
    target 133
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 38
  ]
  edge [
    source 10
    target 39
  ]
  edge [
    source 10
    target 134
  ]
  edge [
    source 10
    target 135
  ]
  edge [
    source 10
    target 136
  ]
  edge [
    source 10
    target 137
  ]
  edge [
    source 10
    target 138
  ]
  edge [
    source 10
    target 139
  ]
  edge [
    source 10
    target 140
  ]
  edge [
    source 10
    target 141
  ]
  edge [
    source 10
    target 142
  ]
  edge [
    source 10
    target 143
  ]
  edge [
    source 10
    target 144
  ]
  edge [
    source 10
    target 145
  ]
  edge [
    source 10
    target 146
  ]
  edge [
    source 10
    target 147
  ]
  edge [
    source 10
    target 148
  ]
  edge [
    source 10
    target 149
  ]
  edge [
    source 10
    target 150
  ]
  edge [
    source 10
    target 151
  ]
  edge [
    source 10
    target 152
  ]
  edge [
    source 10
    target 153
  ]
  edge [
    source 10
    target 154
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 46
  ]
  edge [
    source 11
    target 75
  ]
  edge [
    source 11
    target 83
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 26
  ]
  edge [
    source 12
    target 27
  ]
  edge [
    source 12
    target 31
  ]
  edge [
    source 12
    target 32
  ]
  edge [
    source 12
    target 155
  ]
  edge [
    source 12
    target 156
  ]
  edge [
    source 12
    target 157
  ]
  edge [
    source 12
    target 158
  ]
  edge [
    source 12
    target 159
  ]
  edge [
    source 12
    target 160
  ]
  edge [
    source 12
    target 161
  ]
  edge [
    source 12
    target 162
  ]
  edge [
    source 12
    target 120
  ]
  edge [
    source 12
    target 66
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 163
  ]
  edge [
    source 13
    target 164
  ]
  edge [
    source 13
    target 165
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 166
  ]
  edge [
    source 14
    target 167
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 168
  ]
  edge [
    source 15
    target 169
  ]
  edge [
    source 15
    target 170
  ]
  edge [
    source 15
    target 171
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 34
  ]
  edge [
    source 16
    target 41
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 172
  ]
  edge [
    source 17
    target 173
  ]
  edge [
    source 17
    target 174
  ]
  edge [
    source 17
    target 175
  ]
  edge [
    source 17
    target 176
  ]
  edge [
    source 17
    target 177
  ]
  edge [
    source 17
    target 49
  ]
  edge [
    source 17
    target 178
  ]
  edge [
    source 17
    target 179
  ]
  edge [
    source 17
    target 180
  ]
  edge [
    source 17
    target 181
  ]
  edge [
    source 17
    target 182
  ]
  edge [
    source 17
    target 183
  ]
  edge [
    source 17
    target 184
  ]
  edge [
    source 17
    target 185
  ]
  edge [
    source 17
    target 186
  ]
  edge [
    source 17
    target 187
  ]
  edge [
    source 17
    target 188
  ]
  edge [
    source 17
    target 189
  ]
  edge [
    source 17
    target 190
  ]
  edge [
    source 17
    target 191
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 192
  ]
  edge [
    source 18
    target 193
  ]
  edge [
    source 18
    target 194
  ]
  edge [
    source 18
    target 195
  ]
  edge [
    source 18
    target 174
  ]
  edge [
    source 18
    target 196
  ]
  edge [
    source 18
    target 197
  ]
  edge [
    source 18
    target 198
  ]
  edge [
    source 18
    target 199
  ]
  edge [
    source 18
    target 200
  ]
  edge [
    source 18
    target 201
  ]
  edge [
    source 18
    target 202
  ]
  edge [
    source 18
    target 203
  ]
  edge [
    source 18
    target 204
  ]
  edge [
    source 18
    target 205
  ]
  edge [
    source 18
    target 206
  ]
  edge [
    source 18
    target 207
  ]
  edge [
    source 18
    target 208
  ]
  edge [
    source 18
    target 209
  ]
  edge [
    source 18
    target 210
  ]
  edge [
    source 18
    target 211
  ]
  edge [
    source 18
    target 212
  ]
  edge [
    source 18
    target 213
  ]
  edge [
    source 18
    target 214
  ]
  edge [
    source 18
    target 215
  ]
  edge [
    source 18
    target 216
  ]
  edge [
    source 18
    target 217
  ]
  edge [
    source 18
    target 218
  ]
  edge [
    source 18
    target 219
  ]
  edge [
    source 18
    target 220
  ]
  edge [
    source 18
    target 221
  ]
  edge [
    source 18
    target 222
  ]
  edge [
    source 18
    target 223
  ]
  edge [
    source 18
    target 224
  ]
  edge [
    source 18
    target 225
  ]
  edge [
    source 18
    target 226
  ]
  edge [
    source 18
    target 227
  ]
  edge [
    source 18
    target 228
  ]
  edge [
    source 18
    target 229
  ]
  edge [
    source 18
    target 230
  ]
  edge [
    source 18
    target 231
  ]
  edge [
    source 18
    target 232
  ]
  edge [
    source 18
    target 233
  ]
  edge [
    source 18
    target 234
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 24
  ]
  edge [
    source 20
    target 25
  ]
  edge [
    source 20
    target 49
  ]
  edge [
    source 20
    target 50
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 235
  ]
  edge [
    source 21
    target 137
  ]
  edge [
    source 21
    target 236
  ]
  edge [
    source 21
    target 237
  ]
  edge [
    source 21
    target 238
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 28
  ]
  edge [
    source 22
    target 29
  ]
  edge [
    source 22
    target 239
  ]
  edge [
    source 22
    target 37
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 116
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 49
  ]
  edge [
    source 26
    target 61
  ]
  edge [
    source 26
    target 62
  ]
  edge [
    source 26
    target 240
  ]
  edge [
    source 26
    target 241
  ]
  edge [
    source 26
    target 32
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 111
  ]
  edge [
    source 27
    target 66
  ]
  edge [
    source 28
    target 242
  ]
  edge [
    source 28
    target 243
  ]
  edge [
    source 28
    target 194
  ]
  edge [
    source 28
    target 244
  ]
  edge [
    source 28
    target 245
  ]
  edge [
    source 28
    target 39
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 246
  ]
  edge [
    source 30
    target 247
  ]
  edge [
    source 30
    target 248
  ]
  edge [
    source 30
    target 249
  ]
  edge [
    source 30
    target 250
  ]
  edge [
    source 30
    target 251
  ]
  edge [
    source 30
    target 252
  ]
  edge [
    source 30
    target 253
  ]
  edge [
    source 30
    target 254
  ]
  edge [
    source 30
    target 255
  ]
  edge [
    source 30
    target 256
  ]
  edge [
    source 30
    target 257
  ]
  edge [
    source 30
    target 258
  ]
  edge [
    source 31
    target 259
  ]
  edge [
    source 31
    target 260
  ]
  edge [
    source 31
    target 261
  ]
  edge [
    source 31
    target 262
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 263
  ]
  edge [
    source 32
    target 264
  ]
  edge [
    source 32
    target 265
  ]
  edge [
    source 32
    target 266
  ]
  edge [
    source 32
    target 267
  ]
  edge [
    source 32
    target 268
  ]
  edge [
    source 32
    target 269
  ]
  edge [
    source 32
    target 270
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 271
  ]
  edge [
    source 33
    target 272
  ]
  edge [
    source 33
    target 64
  ]
  edge [
    source 33
    target 80
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 273
  ]
  edge [
    source 34
    target 274
  ]
  edge [
    source 34
    target 275
  ]
  edge [
    source 34
    target 276
  ]
  edge [
    source 34
    target 41
  ]
  edge [
    source 35
    target 277
  ]
  edge [
    source 35
    target 278
  ]
  edge [
    source 35
    target 279
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 280
  ]
  edge [
    source 36
    target 281
  ]
  edge [
    source 36
    target 282
  ]
  edge [
    source 36
    target 283
  ]
  edge [
    source 36
    target 284
  ]
  edge [
    source 36
    target 285
  ]
  edge [
    source 36
    target 286
  ]
  edge [
    source 36
    target 287
  ]
  edge [
    source 36
    target 288
  ]
  edge [
    source 36
    target 289
  ]
  edge [
    source 36
    target 290
  ]
  edge [
    source 36
    target 291
  ]
  edge [
    source 36
    target 292
  ]
  edge [
    source 36
    target 293
  ]
  edge [
    source 36
    target 294
  ]
  edge [
    source 36
    target 295
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 296
  ]
  edge [
    source 37
    target 297
  ]
  edge [
    source 37
    target 298
  ]
  edge [
    source 37
    target 299
  ]
  edge [
    source 37
    target 300
  ]
  edge [
    source 37
    target 301
  ]
  edge [
    source 37
    target 302
  ]
  edge [
    source 37
    target 303
  ]
  edge [
    source 37
    target 304
  ]
  edge [
    source 37
    target 305
  ]
  edge [
    source 37
    target 306
  ]
  edge [
    source 37
    target 307
  ]
  edge [
    source 37
    target 308
  ]
  edge [
    source 37
    target 309
  ]
  edge [
    source 37
    target 310
  ]
  edge [
    source 37
    target 311
  ]
  edge [
    source 37
    target 312
  ]
  edge [
    source 37
    target 313
  ]
  edge [
    source 37
    target 314
  ]
  edge [
    source 37
    target 315
  ]
  edge [
    source 37
    target 316
  ]
  edge [
    source 37
    target 317
  ]
  edge [
    source 37
    target 318
  ]
  edge [
    source 37
    target 319
  ]
  edge [
    source 37
    target 320
  ]
  edge [
    source 37
    target 321
  ]
  edge [
    source 37
    target 322
  ]
  edge [
    source 37
    target 323
  ]
  edge [
    source 37
    target 324
  ]
  edge [
    source 37
    target 325
  ]
  edge [
    source 37
    target 326
  ]
  edge [
    source 37
    target 327
  ]
  edge [
    source 37
    target 328
  ]
  edge [
    source 37
    target 261
  ]
  edge [
    source 38
    target 329
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 330
  ]
  edge [
    source 39
    target 331
  ]
  edge [
    source 39
    target 194
  ]
  edge [
    source 39
    target 332
  ]
  edge [
    source 39
    target 333
  ]
  edge [
    source 39
    target 114
  ]
  edge [
    source 39
    target 334
  ]
  edge [
    source 39
    target 335
  ]
  edge [
    source 39
    target 336
  ]
  edge [
    source 39
    target 337
  ]
  edge [
    source 39
    target 338
  ]
  edge [
    source 39
    target 339
  ]
  edge [
    source 39
    target 340
  ]
  edge [
    source 39
    target 341
  ]
  edge [
    source 39
    target 342
  ]
  edge [
    source 39
    target 343
  ]
  edge [
    source 39
    target 344
  ]
  edge [
    source 39
    target 345
  ]
  edge [
    source 39
    target 346
  ]
  edge [
    source 39
    target 347
  ]
  edge [
    source 39
    target 348
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 349
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 46
  ]
  edge [
    source 41
    target 47
  ]
  edge [
    source 41
    target 350
  ]
  edge [
    source 41
    target 351
  ]
  edge [
    source 41
    target 352
  ]
  edge [
    source 41
    target 353
  ]
  edge [
    source 41
    target 354
  ]
  edge [
    source 41
    target 355
  ]
  edge [
    source 41
    target 356
  ]
  edge [
    source 41
    target 357
  ]
  edge [
    source 41
    target 358
  ]
  edge [
    source 41
    target 359
  ]
  edge [
    source 41
    target 360
  ]
  edge [
    source 41
    target 361
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 88
  ]
  edge [
    source 43
    target 89
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 46
    target 362
  ]
  edge [
    source 46
    target 363
  ]
  edge [
    source 46
    target 75
  ]
  edge [
    source 46
    target 83
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 53
  ]
  edge [
    source 50
    target 364
  ]
  edge [
    source 50
    target 365
  ]
  edge [
    source 50
    target 366
  ]
  edge [
    source 50
    target 261
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 57
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 367
  ]
  edge [
    source 52
    target 368
  ]
  edge [
    source 52
    target 369
  ]
  edge [
    source 52
    target 370
  ]
  edge [
    source 52
    target 371
  ]
  edge [
    source 52
    target 372
  ]
  edge [
    source 52
    target 373
  ]
  edge [
    source 52
    target 374
  ]
  edge [
    source 52
    target 375
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 376
  ]
  edge [
    source 53
    target 377
  ]
  edge [
    source 53
    target 378
  ]
  edge [
    source 53
    target 379
  ]
  edge [
    source 53
    target 380
  ]
  edge [
    source 53
    target 381
  ]
  edge [
    source 53
    target 382
  ]
  edge [
    source 53
    target 383
  ]
  edge [
    source 53
    target 384
  ]
  edge [
    source 53
    target 385
  ]
  edge [
    source 53
    target 386
  ]
  edge [
    source 53
    target 387
  ]
  edge [
    source 53
    target 147
  ]
  edge [
    source 53
    target 388
  ]
  edge [
    source 53
    target 389
  ]
  edge [
    source 53
    target 390
  ]
  edge [
    source 53
    target 391
  ]
  edge [
    source 53
    target 392
  ]
  edge [
    source 53
    target 366
  ]
  edge [
    source 53
    target 393
  ]
  edge [
    source 53
    target 394
  ]
  edge [
    source 53
    target 395
  ]
  edge [
    source 53
    target 396
  ]
  edge [
    source 53
    target 397
  ]
  edge [
    source 53
    target 261
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 398
  ]
  edge [
    source 55
    target 399
  ]
  edge [
    source 55
    target 400
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 401
  ]
  edge [
    source 56
    target 402
  ]
  edge [
    source 56
    target 403
  ]
  edge [
    source 56
    target 404
  ]
  edge [
    source 57
    target 405
  ]
  edge [
    source 57
    target 406
  ]
  edge [
    source 57
    target 407
  ]
  edge [
    source 57
    target 408
  ]
  edge [
    source 57
    target 409
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 81
  ]
  edge [
    source 58
    target 82
  ]
  edge [
    source 58
    target 91
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 410
  ]
  edge [
    source 60
    target 287
  ]
  edge [
    source 61
    target 411
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 412
  ]
  edge [
    source 62
    target 90
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 413
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 284
  ]
  edge [
    source 64
    target 414
  ]
  edge [
    source 64
    target 415
  ]
  edge [
    source 64
    target 416
  ]
  edge [
    source 64
    target 417
  ]
  edge [
    source 64
    target 418
  ]
  edge [
    source 64
    target 104
  ]
  edge [
    source 64
    target 419
  ]
  edge [
    source 64
    target 420
  ]
  edge [
    source 64
    target 80
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 421
  ]
  edge [
    source 65
    target 422
  ]
  edge [
    source 65
    target 423
  ]
  edge [
    source 65
    target 399
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 66
    target 424
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 67
    target 425
  ]
  edge [
    source 67
    target 426
  ]
  edge [
    source 67
    target 427
  ]
  edge [
    source 67
    target 428
  ]
  edge [
    source 67
    target 429
  ]
  edge [
    source 67
    target 430
  ]
  edge [
    source 67
    target 431
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 68
    target 432
  ]
  edge [
    source 68
    target 433
  ]
  edge [
    source 68
    target 434
  ]
  edge [
    source 68
    target 435
  ]
  edge [
    source 68
    target 436
  ]
  edge [
    source 68
    target 437
  ]
  edge [
    source 68
    target 438
  ]
  edge [
    source 68
    target 439
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 69
    target 440
  ]
  edge [
    source 69
    target 441
  ]
  edge [
    source 69
    target 442
  ]
  edge [
    source 69
    target 73
  ]
  edge [
    source 69
    target 443
  ]
  edge [
    source 69
    target 444
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 445
  ]
  edge [
    source 70
    target 446
  ]
  edge [
    source 70
    target 447
  ]
  edge [
    source 70
    target 448
  ]
  edge [
    source 70
    target 449
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 71
    target 450
  ]
  edge [
    source 71
    target 451
  ]
  edge [
    source 71
    target 452
  ]
  edge [
    source 71
    target 453
  ]
  edge [
    source 72
    target 73
  ]
  edge [
    source 72
    target 311
  ]
  edge [
    source 72
    target 454
  ]
  edge [
    source 72
    target 455
  ]
  edge [
    source 72
    target 426
  ]
  edge [
    source 72
    target 456
  ]
  edge [
    source 72
    target 457
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 73
    target 458
  ]
  edge [
    source 73
    target 459
  ]
  edge [
    source 73
    target 460
  ]
  edge [
    source 73
    target 461
  ]
  edge [
    source 73
    target 462
  ]
  edge [
    source 73
    target 463
  ]
  edge [
    source 74
    target 75
  ]
  edge [
    source 74
    target 464
  ]
  edge [
    source 74
    target 465
  ]
  edge [
    source 74
    target 466
  ]
  edge [
    source 74
    target 467
  ]
  edge [
    source 74
    target 468
  ]
  edge [
    source 74
    target 469
  ]
  edge [
    source 74
    target 470
  ]
  edge [
    source 74
    target 399
  ]
  edge [
    source 75
    target 76
  ]
  edge [
    source 75
    target 471
  ]
  edge [
    source 75
    target 83
  ]
  edge [
    source 76
    target 77
  ]
  edge [
    source 76
    target 472
  ]
  edge [
    source 76
    target 473
  ]
  edge [
    source 76
    target 474
  ]
  edge [
    source 76
    target 475
  ]
  edge [
    source 76
    target 476
  ]
  edge [
    source 77
    target 78
  ]
  edge [
    source 77
    target 477
  ]
  edge [
    source 77
    target 329
  ]
  edge [
    source 77
    target 478
  ]
  edge [
    source 77
    target 479
  ]
  edge [
    source 77
    target 480
  ]
  edge [
    source 78
    target 79
  ]
  edge [
    source 78
    target 481
  ]
  edge [
    source 79
    target 80
  ]
  edge [
    source 79
    target 482
  ]
  edge [
    source 80
    target 81
  ]
  edge [
    source 80
    target 483
  ]
  edge [
    source 80
    target 484
  ]
  edge [
    source 80
    target 485
  ]
  edge [
    source 80
    target 486
  ]
  edge [
    source 81
    target 487
  ]
  edge [
    source 81
    target 488
  ]
  edge [
    source 81
    target 489
  ]
  edge [
    source 82
    target 83
  ]
  edge [
    source 82
    target 490
  ]
  edge [
    source 82
    target 491
  ]
  edge [
    source 82
    target 492
  ]
  edge [
    source 82
    target 493
  ]
  edge [
    source 83
    target 84
  ]
  edge [
    source 84
    target 85
  ]
  edge [
    source 85
    target 86
  ]
  edge [
    source 85
    target 494
  ]
  edge [
    source 85
    target 495
  ]
  edge [
    source 85
    target 496
  ]
  edge [
    source 85
    target 497
  ]
  edge [
    source 85
    target 498
  ]
  edge [
    source 85
    target 499
  ]
  edge [
    source 85
    target 500
  ]
  edge [
    source 86
    target 87
  ]
  edge [
    source 86
    target 485
  ]
  edge [
    source 87
    target 88
  ]
  edge [
    source 87
    target 324
  ]
  edge [
    source 88
    target 501
  ]
  edge [
    source 88
    target 90
  ]
  edge [
    source 88
    target 502
  ]
  edge [
    source 88
    target 503
  ]
  edge [
    source 88
    target 504
  ]
  edge [
    source 88
    target 505
  ]
  edge [
    source 88
    target 506
  ]
  edge [
    source 88
    target 507
  ]
  edge [
    source 88
    target 508
  ]
  edge [
    source 88
    target 509
  ]
  edge [
    source 89
    target 90
  ]
  edge [
    source 89
    target 510
  ]
  edge [
    source 89
    target 511
  ]
  edge [
    source 89
    target 512
  ]
  edge [
    source 89
    target 409
  ]
  edge [
    source 89
    target 513
  ]
  edge [
    source 89
    target 91
  ]
  edge [
    source 90
    target 91
  ]
  edge [
    source 90
    target 514
  ]
  edge [
    source 90
    target 515
  ]
  edge [
    source 90
    target 516
  ]
  edge [
    source 90
    target 502
  ]
  edge [
    source 90
    target 517
  ]
  edge [
    source 90
    target 518
  ]
  edge [
    source 90
    target 519
  ]
  edge [
    source 90
    target 520
  ]
  edge [
    source 90
    target 521
  ]
  edge [
    source 90
    target 522
  ]
  edge [
    source 90
    target 523
  ]
  edge [
    source 90
    target 524
  ]
  edge [
    source 90
    target 525
  ]
  edge [
    source 90
    target 526
  ]
  edge [
    source 90
    target 527
  ]
  edge [
    source 90
    target 528
  ]
  edge [
    source 90
    target 529
  ]
  edge [
    source 90
    target 530
  ]
  edge [
    source 90
    target 531
  ]
  edge [
    source 90
    target 532
  ]
  edge [
    source 90
    target 533
  ]
  edge [
    source 90
    target 534
  ]
  edge [
    source 90
    target 535
  ]
  edge [
    source 90
    target 536
  ]
  edge [
    source 90
    target 537
  ]
  edge [
    source 90
    target 538
  ]
  edge [
    source 90
    target 539
  ]
  edge [
    source 90
    target 341
  ]
  edge [
    source 90
    target 540
  ]
  edge [
    source 90
    target 541
  ]
  edge [
    source 90
    target 542
  ]
  edge [
    source 90
    target 543
  ]
  edge [
    source 90
    target 544
  ]
  edge [
    source 90
    target 545
  ]
  edge [
    source 90
    target 546
  ]
  edge [
    source 90
    target 547
  ]
  edge [
    source 90
    target 548
  ]
  edge [
    source 91
    target 311
  ]
  edge [
    source 91
    target 549
  ]
  edge [
    source 91
    target 550
  ]
  edge [
    source 91
    target 551
  ]
  edge [
    source 91
    target 552
  ]
  edge [
    source 91
    target 553
  ]
  edge [
    source 91
    target 554
  ]
  edge [
    source 92
    target 93
  ]
  edge [
    source 93
    target 94
  ]
  edge [
    source 93
    target 555
  ]
  edge [
    source 93
    target 556
  ]
  edge [
    source 93
    target 557
  ]
  edge [
    source 93
    target 558
  ]
  edge [
    source 93
    target 559
  ]
  edge [
    source 94
    target 95
  ]
  edge [
    source 94
    target 128
  ]
  edge [
    source 95
    target 461
  ]
  edge [
    source 95
    target 560
  ]
  edge [
    source 95
    target 561
  ]
  edge [
    source 95
    target 562
  ]
  edge [
    source 95
    target 563
  ]
  edge [
    source 95
    target 159
  ]
  edge [
    source 95
    target 564
  ]
  edge [
    source 95
    target 565
  ]
  edge [
    source 566
    target 567
  ]
  edge [
    source 568
    target 569
  ]
  edge [
    source 570
    target 571
  ]
  edge [
    source 570
    target 572
  ]
  edge [
    source 571
    target 572
  ]
  edge [
    source 571
    target 577
  ]
  edge [
    source 571
    target 578
  ]
  edge [
    source 571
    target 579
  ]
  edge [
    source 573
    target 574
  ]
  edge [
    source 575
    target 576
  ]
  edge [
    source 577
    target 578
  ]
  edge [
    source 578
    target 579
  ]
]
