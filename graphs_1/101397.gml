graph [
  maxDegree 36
  minDegree 1
  meanDegree 1.9473684210526316
  density 0.05263157894736842
  graphCliqueNumber 2
  node [
    id 0
    label "partia"
    origin "text"
  ]
  node [
    id 1
    label "towaroznawstwo"
    origin "text"
  ]
  node [
    id 2
    label "SLD"
  ]
  node [
    id 3
    label "niedoczas"
  ]
  node [
    id 4
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 5
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 6
    label "grupa"
  ]
  node [
    id 7
    label "game"
  ]
  node [
    id 8
    label "ZChN"
  ]
  node [
    id 9
    label "wybranka"
  ]
  node [
    id 10
    label "Wigowie"
  ]
  node [
    id 11
    label "egzekutywa"
  ]
  node [
    id 12
    label "unit"
  ]
  node [
    id 13
    label "blok"
  ]
  node [
    id 14
    label "Razem"
  ]
  node [
    id 15
    label "si&#322;a"
  ]
  node [
    id 16
    label "organizacja"
  ]
  node [
    id 17
    label "wybranek"
  ]
  node [
    id 18
    label "materia&#322;"
  ]
  node [
    id 19
    label "PiS"
  ]
  node [
    id 20
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 21
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 22
    label "AWS"
  ]
  node [
    id 23
    label "package"
  ]
  node [
    id 24
    label "Bund"
  ]
  node [
    id 25
    label "Kuomintang"
  ]
  node [
    id 26
    label "aktyw"
  ]
  node [
    id 27
    label "Jakobici"
  ]
  node [
    id 28
    label "PSL"
  ]
  node [
    id 29
    label "Federali&#347;ci"
  ]
  node [
    id 30
    label "gra"
  ]
  node [
    id 31
    label "ZSL"
  ]
  node [
    id 32
    label "PPR"
  ]
  node [
    id 33
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 34
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 35
    label "PO"
  ]
  node [
    id 36
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 37
    label "nauka_ekonomiczna"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 1
    target 37
  ]
]
