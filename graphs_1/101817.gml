graph [
  maxDegree 18
  minDegree 1
  meanDegree 2
  density 0.020833333333333332
  graphCliqueNumber 3
  node [
    id 0
    label "podobnie"
    origin "text"
  ]
  node [
    id 1
    label "jak"
    origin "text"
  ]
  node [
    id 2
    label "przypadek"
    origin "text"
  ]
  node [
    id 3
    label "wyra&#380;a&#263;"
    origin "text"
  ]
  node [
    id 4
    label "&#380;al"
    origin "text"
  ]
  node [
    id 5
    label "r&#243;wnie&#380;"
    origin "text"
  ]
  node [
    id 6
    label "manifestowa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "niezadowolenie"
    origin "text"
  ]
  node [
    id 8
    label "by&#263;"
    origin "text"
  ]
  node [
    id 9
    label "praktyka"
    origin "text"
  ]
  node [
    id 10
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 11
    label "u&#380;ytkownik"
    origin "text"
  ]
  node [
    id 12
    label "podejmowa&#263;"
    origin "text"
  ]
  node [
    id 13
    label "masowo"
    origin "text"
  ]
  node [
    id 14
    label "charakterystycznie"
  ]
  node [
    id 15
    label "podobny"
  ]
  node [
    id 16
    label "byd&#322;o"
  ]
  node [
    id 17
    label "zobo"
  ]
  node [
    id 18
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 19
    label "yakalo"
  ]
  node [
    id 20
    label "dzo"
  ]
  node [
    id 21
    label "pacjent"
  ]
  node [
    id 22
    label "kategoria_gramatyczna"
  ]
  node [
    id 23
    label "schorzenie"
  ]
  node [
    id 24
    label "przeznaczenie"
  ]
  node [
    id 25
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 26
    label "wydarzenie"
  ]
  node [
    id 27
    label "happening"
  ]
  node [
    id 28
    label "przyk&#322;ad"
  ]
  node [
    id 29
    label "give_voice"
  ]
  node [
    id 30
    label "arouse"
  ]
  node [
    id 31
    label "represent"
  ]
  node [
    id 32
    label "komunikowa&#263;"
  ]
  node [
    id 33
    label "convey"
  ]
  node [
    id 34
    label "zdradza&#263;_si&#281;"
  ]
  node [
    id 35
    label "oznacza&#263;"
  ]
  node [
    id 36
    label "znaczy&#263;"
  ]
  node [
    id 37
    label "wstyd"
  ]
  node [
    id 38
    label "gniewa&#263;_si&#281;"
  ]
  node [
    id 39
    label "gniewanie_si&#281;"
  ]
  node [
    id 40
    label "smutek"
  ]
  node [
    id 41
    label "czu&#263;"
  ]
  node [
    id 42
    label "emocja"
  ]
  node [
    id 43
    label "commiseration"
  ]
  node [
    id 44
    label "pang"
  ]
  node [
    id 45
    label "krytyka"
  ]
  node [
    id 46
    label "pogniewa&#263;_si&#281;"
  ]
  node [
    id 47
    label "uraza"
  ]
  node [
    id 48
    label "criticism"
  ]
  node [
    id 49
    label "sorrow"
  ]
  node [
    id 50
    label "pogniewanie_si&#281;"
  ]
  node [
    id 51
    label "sytuacja"
  ]
  node [
    id 52
    label "pokazywa&#263;"
  ]
  node [
    id 53
    label "bespeak"
  ]
  node [
    id 54
    label "attest"
  ]
  node [
    id 55
    label "narzekanie"
  ]
  node [
    id 56
    label "narzeka&#263;"
  ]
  node [
    id 57
    label "alienation"
  ]
  node [
    id 58
    label "nieukontentowanie"
  ]
  node [
    id 59
    label "si&#281;ga&#263;"
  ]
  node [
    id 60
    label "trwa&#263;"
  ]
  node [
    id 61
    label "obecno&#347;&#263;"
  ]
  node [
    id 62
    label "stan"
  ]
  node [
    id 63
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 64
    label "stand"
  ]
  node [
    id 65
    label "mie&#263;_miejsce"
  ]
  node [
    id 66
    label "uczestniczy&#263;"
  ]
  node [
    id 67
    label "chodzi&#263;"
  ]
  node [
    id 68
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 69
    label "equal"
  ]
  node [
    id 70
    label "czyn"
  ]
  node [
    id 71
    label "practice"
  ]
  node [
    id 72
    label "skill"
  ]
  node [
    id 73
    label "znawstwo"
  ]
  node [
    id 74
    label "wiedza"
  ]
  node [
    id 75
    label "zwyczaj"
  ]
  node [
    id 76
    label "eksperiencja"
  ]
  node [
    id 77
    label "praca"
  ]
  node [
    id 78
    label "nauka"
  ]
  node [
    id 79
    label "j&#281;zykowo"
  ]
  node [
    id 80
    label "podmiot"
  ]
  node [
    id 81
    label "zmienia&#263;"
  ]
  node [
    id 82
    label "reagowa&#263;"
  ]
  node [
    id 83
    label "rise"
  ]
  node [
    id 84
    label "admit"
  ]
  node [
    id 85
    label "drive"
  ]
  node [
    id 86
    label "robi&#263;"
  ]
  node [
    id 87
    label "draw"
  ]
  node [
    id 88
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 89
    label "podnosi&#263;"
  ]
  node [
    id 90
    label "seryjnie"
  ]
  node [
    id 91
    label "masowy"
  ]
  node [
    id 92
    label "nisko"
  ]
  node [
    id 93
    label "nasz"
  ]
  node [
    id 94
    label "klasy"
  ]
  node [
    id 95
    label "katastrofa"
  ]
  node [
    id 96
    label "smole&#324;ski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 79
  ]
  edge [
    source 11
    target 80
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 81
  ]
  edge [
    source 12
    target 82
  ]
  edge [
    source 12
    target 83
  ]
  edge [
    source 12
    target 84
  ]
  edge [
    source 12
    target 85
  ]
  edge [
    source 12
    target 86
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 12
    target 89
  ]
  edge [
    source 13
    target 90
  ]
  edge [
    source 13
    target 91
  ]
  edge [
    source 13
    target 92
  ]
  edge [
    source 93
    target 94
  ]
  edge [
    source 95
    target 96
  ]
]
