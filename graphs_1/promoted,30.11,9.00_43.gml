graph [
  maxDegree 25
  minDegree 1
  meanDegree 2
  density 0.02564102564102564
  graphCliqueNumber 2
  node [
    id 0
    label "przysz&#322;y"
    origin "text"
  ]
  node [
    id 1
    label "rok"
    origin "text"
  ]
  node [
    id 2
    label "koszt"
    origin "text"
  ]
  node [
    id 3
    label "pr&#261;d"
    origin "text"
  ]
  node [
    id 4
    label "p&#243;j&#347;&#263;"
    origin "text"
  ]
  node [
    id 5
    label "g&#243;ra"
    origin "text"
  ]
  node [
    id 6
    label "kolejny"
  ]
  node [
    id 7
    label "stulecie"
  ]
  node [
    id 8
    label "kalendarz"
  ]
  node [
    id 9
    label "czas"
  ]
  node [
    id 10
    label "pora_roku"
  ]
  node [
    id 11
    label "cykl_astronomiczny"
  ]
  node [
    id 12
    label "p&#243;&#322;rocze"
  ]
  node [
    id 13
    label "grupa"
  ]
  node [
    id 14
    label "kwarta&#322;"
  ]
  node [
    id 15
    label "kurs"
  ]
  node [
    id 16
    label "jubileusz"
  ]
  node [
    id 17
    label "miesi&#261;c"
  ]
  node [
    id 18
    label "lata"
  ]
  node [
    id 19
    label "martwy_sezon"
  ]
  node [
    id 20
    label "nak&#322;ad"
  ]
  node [
    id 21
    label "rachunek_koszt&#243;w"
  ]
  node [
    id 22
    label "sumpt"
  ]
  node [
    id 23
    label "wydatek"
  ]
  node [
    id 24
    label "system"
  ]
  node [
    id 25
    label "przep&#322;yw"
  ]
  node [
    id 26
    label "energia"
  ]
  node [
    id 27
    label "licznik_energii_elektrycznej"
  ]
  node [
    id 28
    label "ideologia"
  ]
  node [
    id 29
    label "ruch"
  ]
  node [
    id 30
    label "dreszcz"
  ]
  node [
    id 31
    label "praktyka"
  ]
  node [
    id 32
    label "metoda"
  ]
  node [
    id 33
    label "apparent_motion"
  ]
  node [
    id 34
    label "electricity"
  ]
  node [
    id 35
    label "zjawisko"
  ]
  node [
    id 36
    label "przyp&#322;yw"
  ]
  node [
    id 37
    label "opu&#347;ci&#263;"
  ]
  node [
    id 38
    label "poruszy&#263;_si&#281;"
  ]
  node [
    id 39
    label "proceed"
  ]
  node [
    id 40
    label "powie&#347;&#263;_si&#281;"
  ]
  node [
    id 41
    label "popsu&#263;_si&#281;"
  ]
  node [
    id 42
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 43
    label "kopn&#261;&#263;_si&#281;"
  ]
  node [
    id 44
    label "zacz&#261;&#263;"
  ]
  node [
    id 45
    label "zmieni&#263;"
  ]
  node [
    id 46
    label "zosta&#263;"
  ]
  node [
    id 47
    label "sail"
  ]
  node [
    id 48
    label "leave"
  ]
  node [
    id 49
    label "uda&#263;_si&#281;"
  ]
  node [
    id 50
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 51
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 52
    label "zrobi&#263;"
  ]
  node [
    id 53
    label "wyp&#322;yn&#261;&#263;"
  ]
  node [
    id 54
    label "sprzeda&#263;_si&#281;"
  ]
  node [
    id 55
    label "przyj&#261;&#263;"
  ]
  node [
    id 56
    label "potoczy&#263;_si&#281;"
  ]
  node [
    id 57
    label "become"
  ]
  node [
    id 58
    label "play_along"
  ]
  node [
    id 59
    label "travel"
  ]
  node [
    id 60
    label "przedmiot"
  ]
  node [
    id 61
    label "element"
  ]
  node [
    id 62
    label "przele&#378;&#263;"
  ]
  node [
    id 63
    label "pi&#281;tro"
  ]
  node [
    id 64
    label "karczek"
  ]
  node [
    id 65
    label "wysoki"
  ]
  node [
    id 66
    label "rami&#261;czko"
  ]
  node [
    id 67
    label "Ropa"
  ]
  node [
    id 68
    label "Jaworze"
  ]
  node [
    id 69
    label "Synaj"
  ]
  node [
    id 70
    label "wzniesienie"
  ]
  node [
    id 71
    label "przelezienie"
  ]
  node [
    id 72
    label "&#347;piew"
  ]
  node [
    id 73
    label "kupa"
  ]
  node [
    id 74
    label "kierunek"
  ]
  node [
    id 75
    label "Ma&#322;a_Rawka"
  ]
  node [
    id 76
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 77
    label "d&#378;wi&#281;k"
  ]
  node [
    id 78
    label "Kreml"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 13
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
]
