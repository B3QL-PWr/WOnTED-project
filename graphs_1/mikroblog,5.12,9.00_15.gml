graph [
  maxDegree 20
  minDegree 1
  meanDegree 1.9545454545454546
  density 0.045454545454545456
  graphCliqueNumber 2
  node [
    id 0
    label "dobra"
    origin "text"
  ]
  node [
    id 1
    label "wiedzie&#263;"
    origin "text"
  ]
  node [
    id 2
    label "juz"
    origin "text"
  ]
  node [
    id 3
    label "leca"
    origin "text"
  ]
  node [
    id 4
    label "marek"
    origin "text"
  ]
  node [
    id 5
    label "mowi"
    origin "text"
  ]
  node [
    id 6
    label "wygladam"
    origin "text"
  ]
  node [
    id 7
    label "lato"
    origin "text"
  ]
  node [
    id 8
    label "logikarozowychpaskow"
    origin "text"
  ]
  node [
    id 9
    label "atencyjnyrozowypasek"
    origin "text"
  ]
  node [
    id 10
    label "pytanie"
    origin "text"
  ]
  node [
    id 11
    label "frymark"
  ]
  node [
    id 12
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 13
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 14
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 15
    label "commodity"
  ]
  node [
    id 16
    label "mienie"
  ]
  node [
    id 17
    label "Wilko"
  ]
  node [
    id 18
    label "jednostka_monetarna"
  ]
  node [
    id 19
    label "centym"
  ]
  node [
    id 20
    label "cognizance"
  ]
  node [
    id 21
    label "selerowate"
  ]
  node [
    id 22
    label "bylina"
  ]
  node [
    id 23
    label "hygrofit"
  ]
  node [
    id 24
    label "pora_roku"
  ]
  node [
    id 25
    label "sprawa"
  ]
  node [
    id 26
    label "zadanie"
  ]
  node [
    id 27
    label "wypowied&#378;"
  ]
  node [
    id 28
    label "problemat"
  ]
  node [
    id 29
    label "rozpytywanie"
  ]
  node [
    id 30
    label "sprawdzian"
  ]
  node [
    id 31
    label "przes&#322;uchiwanie"
  ]
  node [
    id 32
    label "wypytanie"
  ]
  node [
    id 33
    label "zwracanie_si&#281;"
  ]
  node [
    id 34
    label "wypowiedzenie"
  ]
  node [
    id 35
    label "wywo&#322;ywanie"
  ]
  node [
    id 36
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 37
    label "problematyka"
  ]
  node [
    id 38
    label "question"
  ]
  node [
    id 39
    label "sprawdzanie"
  ]
  node [
    id 40
    label "odpowiadanie"
  ]
  node [
    id 41
    label "survey"
  ]
  node [
    id 42
    label "odpowiada&#263;"
  ]
  node [
    id 43
    label "egzaminowanie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 24
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 25
  ]
  edge [
    source 10
    target 26
  ]
  edge [
    source 10
    target 27
  ]
  edge [
    source 10
    target 28
  ]
  edge [
    source 10
    target 29
  ]
  edge [
    source 10
    target 30
  ]
  edge [
    source 10
    target 31
  ]
  edge [
    source 10
    target 32
  ]
  edge [
    source 10
    target 33
  ]
  edge [
    source 10
    target 34
  ]
  edge [
    source 10
    target 35
  ]
  edge [
    source 10
    target 36
  ]
  edge [
    source 10
    target 37
  ]
  edge [
    source 10
    target 38
  ]
  edge [
    source 10
    target 39
  ]
  edge [
    source 10
    target 40
  ]
  edge [
    source 10
    target 41
  ]
  edge [
    source 10
    target 42
  ]
  edge [
    source 10
    target 43
  ]
]
