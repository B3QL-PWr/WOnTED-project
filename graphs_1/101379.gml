graph [
  maxDegree 87
  minDegree 1
  meanDegree 1.98
  density 0.02
  graphCliqueNumber 2
  node [
    id 0
    label "osiedle"
    origin "text"
  ]
  node [
    id 1
    label "&#347;r&#243;dmie&#347;cie"
    origin "text"
  ]
  node [
    id 2
    label "po&#322;udnie"
    origin "text"
  ]
  node [
    id 3
    label "rzesz&#243;w"
    origin "text"
  ]
  node [
    id 4
    label "G&#243;rce"
  ]
  node [
    id 5
    label "Powsin"
  ]
  node [
    id 6
    label "Kar&#322;owice"
  ]
  node [
    id 7
    label "Rakowiec"
  ]
  node [
    id 8
    label "Dojlidy"
  ]
  node [
    id 9
    label "Horodyszcze"
  ]
  node [
    id 10
    label "Kujbyszewe"
  ]
  node [
    id 11
    label "Kabaty"
  ]
  node [
    id 12
    label "jednostka_osadnicza"
  ]
  node [
    id 13
    label "Ujazd&#243;w"
  ]
  node [
    id 14
    label "Kaw&#281;czyn"
  ]
  node [
    id 15
    label "Siersza"
  ]
  node [
    id 16
    label "Groch&#243;w"
  ]
  node [
    id 17
    label "Paw&#322;owice"
  ]
  node [
    id 18
    label "Bielice"
  ]
  node [
    id 19
    label "siedziba"
  ]
  node [
    id 20
    label "Tarchomin"
  ]
  node [
    id 21
    label "Br&#243;dno"
  ]
  node [
    id 22
    label "Jelcz"
  ]
  node [
    id 23
    label "Mariensztat"
  ]
  node [
    id 24
    label "Falenica"
  ]
  node [
    id 25
    label "Izborsk"
  ]
  node [
    id 26
    label "Wi&#347;niewo"
  ]
  node [
    id 27
    label "Marymont"
  ]
  node [
    id 28
    label "Solec"
  ]
  node [
    id 29
    label "Zakrz&#243;w"
  ]
  node [
    id 30
    label "Wi&#347;niowiec"
  ]
  node [
    id 31
    label "Natolin"
  ]
  node [
    id 32
    label "grupa"
  ]
  node [
    id 33
    label "Grabiszyn"
  ]
  node [
    id 34
    label "Anin"
  ]
  node [
    id 35
    label "Orunia"
  ]
  node [
    id 36
    label "Gronik"
  ]
  node [
    id 37
    label "Boryszew"
  ]
  node [
    id 38
    label "Bogucice"
  ]
  node [
    id 39
    label "&#379;era&#324;"
  ]
  node [
    id 40
    label "zesp&#243;&#322;"
  ]
  node [
    id 41
    label "Jasienica"
  ]
  node [
    id 42
    label "Laskowice_O&#322;awskie"
  ]
  node [
    id 43
    label "Salwator"
  ]
  node [
    id 44
    label "Zerze&#324;"
  ]
  node [
    id 45
    label "M&#322;ociny"
  ]
  node [
    id 46
    label "Branice"
  ]
  node [
    id 47
    label "Chojny"
  ]
  node [
    id 48
    label "Wad&#243;w"
  ]
  node [
    id 49
    label "jednostka_administracyjna"
  ]
  node [
    id 50
    label "Miedzeszyn"
  ]
  node [
    id 51
    label "Ok&#281;cie"
  ]
  node [
    id 52
    label "Lewin&#243;w"
  ]
  node [
    id 53
    label "Broch&#243;w"
  ]
  node [
    id 54
    label "Marysin"
  ]
  node [
    id 55
    label "Szack"
  ]
  node [
    id 56
    label "Wielopole"
  ]
  node [
    id 57
    label "Opor&#243;w"
  ]
  node [
    id 58
    label "Osobowice"
  ]
  node [
    id 59
    label "Lubiesz&#243;w"
  ]
  node [
    id 60
    label "&#379;erniki"
  ]
  node [
    id 61
    label "Powi&#347;le"
  ]
  node [
    id 62
    label "osadnictwo"
  ]
  node [
    id 63
    label "Wojn&#243;w"
  ]
  node [
    id 64
    label "Latycz&#243;w"
  ]
  node [
    id 65
    label "Kortowo"
  ]
  node [
    id 66
    label "Rej&#243;w"
  ]
  node [
    id 67
    label "Arsk"
  ]
  node [
    id 68
    label "&#321;agiewniki"
  ]
  node [
    id 69
    label "Azory"
  ]
  node [
    id 70
    label "Imielin"
  ]
  node [
    id 71
    label "Rataje"
  ]
  node [
    id 72
    label "Nadodrze"
  ]
  node [
    id 73
    label "Szczytniki"
  ]
  node [
    id 74
    label "Gr&#281;ba&#322;&#243;w"
  ]
  node [
    id 75
    label "dzielnica"
  ]
  node [
    id 76
    label "S&#281;polno"
  ]
  node [
    id 77
    label "G&#243;rczyn"
  ]
  node [
    id 78
    label "Szcz&#281;&#347;liwice"
  ]
  node [
    id 79
    label "Zalesie"
  ]
  node [
    id 80
    label "Ochock"
  ]
  node [
    id 81
    label "Gutkowo"
  ]
  node [
    id 82
    label "G&#322;uszyna"
  ]
  node [
    id 83
    label "Le&#347;nica"
  ]
  node [
    id 84
    label "Micha&#322;owo"
  ]
  node [
    id 85
    label "Jelonki"
  ]
  node [
    id 86
    label "Marysin_Wawerski"
  ]
  node [
    id 87
    label "Biskupin"
  ]
  node [
    id 88
    label "Goc&#322;aw"
  ]
  node [
    id 89
    label "Wawrzyszew"
  ]
  node [
    id 90
    label "rejon"
  ]
  node [
    id 91
    label "dwunasta"
  ]
  node [
    id 92
    label "obszar"
  ]
  node [
    id 93
    label "Ziemia"
  ]
  node [
    id 94
    label "godzina"
  ]
  node [
    id 95
    label "strona_&#347;wiata"
  ]
  node [
    id 96
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 97
    label "&#347;rodek"
  ]
  node [
    id 98
    label "pora"
  ]
  node [
    id 99
    label "dzie&#324;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
]
