graph [
  maxDegree 42
  minDegree 1
  meanDegree 2.0153846153846153
  density 0.015623136553369112
  graphCliqueNumber 3
  node [
    id 0
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 1
    label "stan"
    origin "text"
  ]
  node [
    id 2
    label "hibernacja"
    origin "text"
  ]
  node [
    id 3
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 4
    label "sen"
    origin "text"
  ]
  node [
    id 5
    label "nieprawda"
    origin "text"
  ]
  node [
    id 6
    label "&#347;ni&#263;"
    origin "text"
  ]
  node [
    id 7
    label "nieprzerwanie"
    origin "text"
  ]
  node [
    id 8
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 9
    label "setny"
    origin "text"
  ]
  node [
    id 10
    label "rok"
    origin "text"
  ]
  node [
    id 11
    label "wci&#261;&#380;"
    origin "text"
  ]
  node [
    id 12
    label "sam"
    origin "text"
  ]
  node [
    id 13
    label "remark"
  ]
  node [
    id 14
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 15
    label "u&#380;ywa&#263;"
  ]
  node [
    id 16
    label "okre&#347;la&#263;"
  ]
  node [
    id 17
    label "j&#281;zyk"
  ]
  node [
    id 18
    label "say"
  ]
  node [
    id 19
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 20
    label "formu&#322;owa&#263;"
  ]
  node [
    id 21
    label "talk"
  ]
  node [
    id 22
    label "powiada&#263;"
  ]
  node [
    id 23
    label "informowa&#263;"
  ]
  node [
    id 24
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 25
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 26
    label "wydobywa&#263;"
  ]
  node [
    id 27
    label "express"
  ]
  node [
    id 28
    label "chew_the_fat"
  ]
  node [
    id 29
    label "dysfonia"
  ]
  node [
    id 30
    label "umie&#263;"
  ]
  node [
    id 31
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 32
    label "tell"
  ]
  node [
    id 33
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 34
    label "wyra&#380;a&#263;"
  ]
  node [
    id 35
    label "gaworzy&#263;"
  ]
  node [
    id 36
    label "rozmawia&#263;"
  ]
  node [
    id 37
    label "dziama&#263;"
  ]
  node [
    id 38
    label "prawi&#263;"
  ]
  node [
    id 39
    label "Arizona"
  ]
  node [
    id 40
    label "Georgia"
  ]
  node [
    id 41
    label "warstwa"
  ]
  node [
    id 42
    label "jednostka_administracyjna"
  ]
  node [
    id 43
    label "Hawaje"
  ]
  node [
    id 44
    label "Goa"
  ]
  node [
    id 45
    label "Floryda"
  ]
  node [
    id 46
    label "Oklahoma"
  ]
  node [
    id 47
    label "punkt"
  ]
  node [
    id 48
    label "Alaska"
  ]
  node [
    id 49
    label "wci&#281;cie"
  ]
  node [
    id 50
    label "Alabama"
  ]
  node [
    id 51
    label "Oregon"
  ]
  node [
    id 52
    label "poziom"
  ]
  node [
    id 53
    label "by&#263;"
  ]
  node [
    id 54
    label "Teksas"
  ]
  node [
    id 55
    label "Illinois"
  ]
  node [
    id 56
    label "Waszyngton"
  ]
  node [
    id 57
    label "Jukatan"
  ]
  node [
    id 58
    label "shape"
  ]
  node [
    id 59
    label "Nowy_Meksyk"
  ]
  node [
    id 60
    label "ilo&#347;&#263;"
  ]
  node [
    id 61
    label "state"
  ]
  node [
    id 62
    label "Nowy_York"
  ]
  node [
    id 63
    label "Arakan"
  ]
  node [
    id 64
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 65
    label "Kalifornia"
  ]
  node [
    id 66
    label "wektor"
  ]
  node [
    id 67
    label "Massachusetts"
  ]
  node [
    id 68
    label "miejsce"
  ]
  node [
    id 69
    label "Pensylwania"
  ]
  node [
    id 70
    label "Michigan"
  ]
  node [
    id 71
    label "Maryland"
  ]
  node [
    id 72
    label "Ohio"
  ]
  node [
    id 73
    label "Kansas"
  ]
  node [
    id 74
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 75
    label "Luizjana"
  ]
  node [
    id 76
    label "samopoczucie"
  ]
  node [
    id 77
    label "Wirginia"
  ]
  node [
    id 78
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 79
    label "przerwa"
  ]
  node [
    id 80
    label "hibernation"
  ]
  node [
    id 81
    label "diapauza"
  ]
  node [
    id 82
    label "czyj&#347;"
  ]
  node [
    id 83
    label "m&#261;&#380;"
  ]
  node [
    id 84
    label "kima"
  ]
  node [
    id 85
    label "wytw&#243;r"
  ]
  node [
    id 86
    label "proces_fizjologiczny"
  ]
  node [
    id 87
    label "relaxation"
  ]
  node [
    id 88
    label "marzenie_senne"
  ]
  node [
    id 89
    label "sen_wolnofalowy"
  ]
  node [
    id 90
    label "odpoczynek"
  ]
  node [
    id 91
    label "hipersomnia"
  ]
  node [
    id 92
    label "jen"
  ]
  node [
    id 93
    label "fun"
  ]
  node [
    id 94
    label "wymys&#322;"
  ]
  node [
    id 95
    label "nokturn"
  ]
  node [
    id 96
    label "sen_paradoksalny"
  ]
  node [
    id 97
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 98
    label "s&#261;d"
  ]
  node [
    id 99
    label "u&#347;miercenie"
  ]
  node [
    id 100
    label "fa&#322;szywo&#347;&#263;"
  ]
  node [
    id 101
    label "u&#347;mierci&#263;"
  ]
  node [
    id 102
    label "przeciwstawno&#347;&#263;"
  ]
  node [
    id 103
    label "fib"
  ]
  node [
    id 104
    label "doznawa&#263;"
  ]
  node [
    id 105
    label "my&#347;le&#263;"
  ]
  node [
    id 106
    label "pour"
  ]
  node [
    id 107
    label "dream"
  ]
  node [
    id 108
    label "nieprzerwany"
  ]
  node [
    id 109
    label "ci&#261;gle"
  ]
  node [
    id 110
    label "nieustannie"
  ]
  node [
    id 111
    label "setnie"
  ]
  node [
    id 112
    label "warto&#347;ciowy"
  ]
  node [
    id 113
    label "przedni"
  ]
  node [
    id 114
    label "stulecie"
  ]
  node [
    id 115
    label "kalendarz"
  ]
  node [
    id 116
    label "czas"
  ]
  node [
    id 117
    label "pora_roku"
  ]
  node [
    id 118
    label "cykl_astronomiczny"
  ]
  node [
    id 119
    label "p&#243;&#322;rocze"
  ]
  node [
    id 120
    label "grupa"
  ]
  node [
    id 121
    label "kwarta&#322;"
  ]
  node [
    id 122
    label "kurs"
  ]
  node [
    id 123
    label "jubileusz"
  ]
  node [
    id 124
    label "miesi&#261;c"
  ]
  node [
    id 125
    label "lata"
  ]
  node [
    id 126
    label "martwy_sezon"
  ]
  node [
    id 127
    label "ci&#261;g&#322;y"
  ]
  node [
    id 128
    label "stale"
  ]
  node [
    id 129
    label "sklep"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 10
    target 121
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 10
    target 125
  ]
  edge [
    source 10
    target 126
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 127
  ]
  edge [
    source 11
    target 128
  ]
  edge [
    source 12
    target 129
  ]
]
