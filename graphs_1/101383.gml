graph [
  maxDegree 18
  minDegree 1
  meanDegree 2.7142857142857144
  density 0.06620209059233449
  graphCliqueNumber 6
  node [
    id 0
    label "konwencja"
    origin "text"
  ]
  node [
    id 1
    label "ochrona"
    origin "text"
  ]
  node [
    id 2
    label "podwodne"
    origin "text"
  ]
  node [
    id 3
    label "dziedzictwo"
    origin "text"
  ]
  node [
    id 4
    label "kulturowy"
    origin "text"
  ]
  node [
    id 5
    label "uk&#322;ad"
  ]
  node [
    id 6
    label "zjazd"
  ]
  node [
    id 7
    label "Europejska_Konwencja_Praw_Cz&#322;owieka"
  ]
  node [
    id 8
    label "line"
  ]
  node [
    id 9
    label "zbi&#243;r"
  ]
  node [
    id 10
    label "kanon"
  ]
  node [
    id 11
    label "zwyczaj"
  ]
  node [
    id 12
    label "styl"
  ]
  node [
    id 13
    label "chemical_bond"
  ]
  node [
    id 14
    label "tarcza"
  ]
  node [
    id 15
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 16
    label "obiekt"
  ]
  node [
    id 17
    label "Stra&#380;_Ochrony_Kolei"
  ]
  node [
    id 18
    label "borowiec"
  ]
  node [
    id 19
    label "obstawienie"
  ]
  node [
    id 20
    label "formacja"
  ]
  node [
    id 21
    label "ubezpieczenie"
  ]
  node [
    id 22
    label "obstawia&#263;"
  ]
  node [
    id 23
    label "obstawianie"
  ]
  node [
    id 24
    label "transportacja"
  ]
  node [
    id 25
    label "wydziedziczy&#263;"
  ]
  node [
    id 26
    label "zachowek"
  ]
  node [
    id 27
    label "wydziedziczenie"
  ]
  node [
    id 28
    label "prawo"
  ]
  node [
    id 29
    label "mienie"
  ]
  node [
    id 30
    label "scheda_spadkowa"
  ]
  node [
    id 31
    label "sukcesja"
  ]
  node [
    id 32
    label "kulturowo"
  ]
  node [
    id 33
    label "ojciec"
  ]
  node [
    id 34
    label "podwodny"
  ]
  node [
    id 35
    label "morze"
  ]
  node [
    id 36
    label "polskie"
  ]
  node [
    id 37
    label "komitet"
  ]
  node [
    id 38
    label "do&#160;spraw"
  ]
  node [
    id 39
    label "UNESCO"
  ]
  node [
    id 40
    label "Zbigniew"
  ]
  node [
    id 41
    label "kobyli&#324;ski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 28
    target 33
  ]
  edge [
    source 28
    target 35
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 35
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 38
  ]
  edge [
    source 36
    target 39
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 39
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 40
    target 41
  ]
]
