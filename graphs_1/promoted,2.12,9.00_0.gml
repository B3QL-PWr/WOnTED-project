graph [
  maxDegree 39
  minDegree 1
  meanDegree 1.9595959595959596
  density 0.019995877138734283
  graphCliqueNumber 2
  node [
    id 0
    label "agnieszka"
    origin "text"
  ]
  node [
    id 1
    label "holland"
    origin "text"
  ]
  node [
    id 2
    label "facebook"
    origin "text"
  ]
  node [
    id 3
    label "bardzo"
    origin "text"
  ]
  node [
    id 4
    label "s&#322;aby"
    origin "text"
  ]
  node [
    id 5
    label "styl"
    origin "text"
  ]
  node [
    id 6
    label "hejtowa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "osoba"
    origin "text"
  ]
  node [
    id 8
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 9
    label "serial"
    origin "text"
  ]
  node [
    id 10
    label "si&#281;"
    origin "text"
  ]
  node [
    id 11
    label "podoba"
    origin "text"
  ]
  node [
    id 12
    label "wall"
  ]
  node [
    id 13
    label "konto"
  ]
  node [
    id 14
    label "w_chuj"
  ]
  node [
    id 15
    label "nieznaczny"
  ]
  node [
    id 16
    label "nieumiej&#281;tny"
  ]
  node [
    id 17
    label "marnie"
  ]
  node [
    id 18
    label "md&#322;y"
  ]
  node [
    id 19
    label "przemijaj&#261;cy"
  ]
  node [
    id 20
    label "zawodny"
  ]
  node [
    id 21
    label "delikatny"
  ]
  node [
    id 22
    label "&#322;agodny"
  ]
  node [
    id 23
    label "niedoskona&#322;y"
  ]
  node [
    id 24
    label "nietrwa&#322;y"
  ]
  node [
    id 25
    label "po&#347;ledni"
  ]
  node [
    id 26
    label "s&#322;abowity"
  ]
  node [
    id 27
    label "niefajny"
  ]
  node [
    id 28
    label "z&#322;y"
  ]
  node [
    id 29
    label "niemocny"
  ]
  node [
    id 30
    label "kiepsko"
  ]
  node [
    id 31
    label "niezdrowy"
  ]
  node [
    id 32
    label "lura"
  ]
  node [
    id 33
    label "s&#322;abo"
  ]
  node [
    id 34
    label "nieudany"
  ]
  node [
    id 35
    label "mizerny"
  ]
  node [
    id 36
    label "pisa&#263;"
  ]
  node [
    id 37
    label "reakcja"
  ]
  node [
    id 38
    label "zachowanie"
  ]
  node [
    id 39
    label "napisa&#263;"
  ]
  node [
    id 40
    label "natural_language"
  ]
  node [
    id 41
    label "charakter"
  ]
  node [
    id 42
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 43
    label "behawior"
  ]
  node [
    id 44
    label "line"
  ]
  node [
    id 45
    label "zbi&#243;r"
  ]
  node [
    id 46
    label "stroke"
  ]
  node [
    id 47
    label "stylik"
  ]
  node [
    id 48
    label "narz&#281;dzie"
  ]
  node [
    id 49
    label "dyscyplina_sportowa"
  ]
  node [
    id 50
    label "kanon"
  ]
  node [
    id 51
    label "spos&#243;b"
  ]
  node [
    id 52
    label "trzonek"
  ]
  node [
    id 53
    label "handle"
  ]
  node [
    id 54
    label "zwalcza&#263;"
  ]
  node [
    id 55
    label "Zgredek"
  ]
  node [
    id 56
    label "kategoria_gramatyczna"
  ]
  node [
    id 57
    label "Casanova"
  ]
  node [
    id 58
    label "Don_Juan"
  ]
  node [
    id 59
    label "Gargantua"
  ]
  node [
    id 60
    label "Faust"
  ]
  node [
    id 61
    label "profanum"
  ]
  node [
    id 62
    label "Chocho&#322;"
  ]
  node [
    id 63
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 64
    label "koniugacja"
  ]
  node [
    id 65
    label "Winnetou"
  ]
  node [
    id 66
    label "Dwukwiat"
  ]
  node [
    id 67
    label "homo_sapiens"
  ]
  node [
    id 68
    label "Edyp"
  ]
  node [
    id 69
    label "Herkules_Poirot"
  ]
  node [
    id 70
    label "ludzko&#347;&#263;"
  ]
  node [
    id 71
    label "mikrokosmos"
  ]
  node [
    id 72
    label "person"
  ]
  node [
    id 73
    label "Sherlock_Holmes"
  ]
  node [
    id 74
    label "portrecista"
  ]
  node [
    id 75
    label "Szwejk"
  ]
  node [
    id 76
    label "Hamlet"
  ]
  node [
    id 77
    label "duch"
  ]
  node [
    id 78
    label "g&#322;owa"
  ]
  node [
    id 79
    label "oddzia&#322;ywanie"
  ]
  node [
    id 80
    label "Quasimodo"
  ]
  node [
    id 81
    label "Dulcynea"
  ]
  node [
    id 82
    label "Don_Kiszot"
  ]
  node [
    id 83
    label "Wallenrod"
  ]
  node [
    id 84
    label "Plastu&#347;"
  ]
  node [
    id 85
    label "Harry_Potter"
  ]
  node [
    id 86
    label "figura"
  ]
  node [
    id 87
    label "parali&#380;owa&#263;"
  ]
  node [
    id 88
    label "istota"
  ]
  node [
    id 89
    label "Werter"
  ]
  node [
    id 90
    label "antropochoria"
  ]
  node [
    id 91
    label "posta&#263;"
  ]
  node [
    id 92
    label "seria"
  ]
  node [
    id 93
    label "Klan"
  ]
  node [
    id 94
    label "film"
  ]
  node [
    id 95
    label "Ranczo"
  ]
  node [
    id 96
    label "program_telewizyjny"
  ]
  node [
    id 97
    label "Agnieszka"
  ]
  node [
    id 98
    label "Holland"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 97
    target 98
  ]
]
