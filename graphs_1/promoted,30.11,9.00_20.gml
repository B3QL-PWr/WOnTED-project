graph [
  maxDegree 16
  minDegree 1
  meanDegree 1.9344262295081966
  density 0.03224043715846994
  graphCliqueNumber 2
  node [
    id 0
    label "lato"
    origin "text"
  ]
  node [
    id 1
    label "temu"
    origin "text"
  ]
  node [
    id 2
    label "podczas"
    origin "text"
  ]
  node [
    id 3
    label "wykopaliska"
    origin "text"
  ]
  node [
    id 4
    label "okolica"
    origin "text"
  ]
  node [
    id 5
    label "betlejem"
    origin "text"
  ]
  node [
    id 6
    label "odkry&#263;"
    origin "text"
  ]
  node [
    id 7
    label "wykona&#263;"
    origin "text"
  ]
  node [
    id 8
    label "br&#261;z"
    origin "text"
  ]
  node [
    id 9
    label "pier&#347;cie&#324;"
    origin "text"
  ]
  node [
    id 10
    label "pora_roku"
  ]
  node [
    id 11
    label "archeologia"
  ]
  node [
    id 12
    label "metoda"
  ]
  node [
    id 13
    label "miejsce"
  ]
  node [
    id 14
    label "obszar"
  ]
  node [
    id 15
    label "krajobraz"
  ]
  node [
    id 16
    label "grupa"
  ]
  node [
    id 17
    label "organ"
  ]
  node [
    id 18
    label "przyroda"
  ]
  node [
    id 19
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 20
    label "po_s&#261;siedzku"
  ]
  node [
    id 21
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 22
    label "pozna&#263;"
  ]
  node [
    id 23
    label "ukaza&#263;"
  ]
  node [
    id 24
    label "denounce"
  ]
  node [
    id 25
    label "podnie&#347;&#263;"
  ]
  node [
    id 26
    label "znale&#378;&#263;"
  ]
  node [
    id 27
    label "unwrap"
  ]
  node [
    id 28
    label "&#322;owca_talent&#243;w"
  ]
  node [
    id 29
    label "expose"
  ]
  node [
    id 30
    label "discover"
  ]
  node [
    id 31
    label "zsun&#261;&#263;"
  ]
  node [
    id 32
    label "zwr&#243;ci&#263;_uwag&#281;"
  ]
  node [
    id 33
    label "objawi&#263;"
  ]
  node [
    id 34
    label "poinformowa&#263;"
  ]
  node [
    id 35
    label "wytworzy&#263;"
  ]
  node [
    id 36
    label "manufacture"
  ]
  node [
    id 37
    label "zrobi&#263;"
  ]
  node [
    id 38
    label "picture"
  ]
  node [
    id 39
    label "stop"
  ]
  node [
    id 40
    label "metal_kolorowy"
  ]
  node [
    id 41
    label "tangent"
  ]
  node [
    id 42
    label "medal"
  ]
  node [
    id 43
    label "kolor"
  ]
  node [
    id 44
    label "band"
  ]
  node [
    id 45
    label "ring"
  ]
  node [
    id 46
    label "zwi&#261;zek_cykliczny"
  ]
  node [
    id 47
    label "Pier&#347;cie&#324;_W&#322;adzy"
  ]
  node [
    id 48
    label "section"
  ]
  node [
    id 49
    label "annulus"
  ]
  node [
    id 50
    label "okr&#261;g"
  ]
  node [
    id 51
    label "ozdoba"
  ]
  node [
    id 52
    label "bi&#380;uteria"
  ]
  node [
    id 53
    label "struktura_anatomiczna"
  ]
  node [
    id 54
    label "zbi&#243;r"
  ]
  node [
    id 55
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 56
    label "skleryt"
  ]
  node [
    id 57
    label "struktura"
  ]
  node [
    id 58
    label "pier&#347;cienica"
  ]
  node [
    id 59
    label "Poncjusz"
  ]
  node [
    id 60
    label "pi&#322;at"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 22
  ]
  edge [
    source 6
    target 23
  ]
  edge [
    source 6
    target 24
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 35
  ]
  edge [
    source 7
    target 36
  ]
  edge [
    source 7
    target 37
  ]
  edge [
    source 7
    target 38
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 39
  ]
  edge [
    source 8
    target 40
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 9
    target 44
  ]
  edge [
    source 9
    target 45
  ]
  edge [
    source 9
    target 46
  ]
  edge [
    source 9
    target 47
  ]
  edge [
    source 9
    target 48
  ]
  edge [
    source 9
    target 49
  ]
  edge [
    source 9
    target 50
  ]
  edge [
    source 9
    target 51
  ]
  edge [
    source 9
    target 52
  ]
  edge [
    source 9
    target 53
  ]
  edge [
    source 9
    target 54
  ]
  edge [
    source 9
    target 55
  ]
  edge [
    source 9
    target 56
  ]
  edge [
    source 9
    target 57
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 59
    target 60
  ]
]
