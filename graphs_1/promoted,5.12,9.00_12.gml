graph [
  maxDegree 10
  minDegree 1
  meanDegree 1.875
  density 0.125
  graphCliqueNumber 2
  node [
    id 0
    label "licytacja"
    origin "text"
  ]
  node [
    id 1
    label "z&#322;ot&#243;wka"
    origin "text"
  ]
  node [
    id 2
    label "gimbynieznajo"
    origin "text"
  ]
  node [
    id 3
    label "bryd&#380;"
  ]
  node [
    id 4
    label "rozdanie"
  ]
  node [
    id 5
    label "faza"
  ]
  node [
    id 6
    label "tysi&#261;c"
  ]
  node [
    id 7
    label "pas"
  ]
  node [
    id 8
    label "przetarg"
  ]
  node [
    id 9
    label "skat"
  ]
  node [
    id 10
    label "sprzeda&#380;"
  ]
  node [
    id 11
    label "gra_w_karty"
  ]
  node [
    id 12
    label "grosz"
  ]
  node [
    id 13
    label "pieni&#261;dz"
  ]
  node [
    id 14
    label "jednostka_monetarna"
  ]
  node [
    id 15
    label "Polska"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
]
