graph [
  maxDegree 33
  minDegree 1
  meanDegree 1.8888888888888888
  density 0.05396825396825397
  graphCliqueNumber 2
  node [
    id 0
    label "kultura"
    origin "text"
  ]
  node [
    id 1
    label "lendzielski"
    origin "text"
  ]
  node [
    id 2
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 3
    label "przedmiot"
  ]
  node [
    id 4
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 5
    label "Wsch&#243;d"
  ]
  node [
    id 6
    label "rzecz"
  ]
  node [
    id 7
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 8
    label "sztuka"
  ]
  node [
    id 9
    label "religia"
  ]
  node [
    id 10
    label "przejmowa&#263;"
  ]
  node [
    id 11
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 12
    label "makrokosmos"
  ]
  node [
    id 13
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 14
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 15
    label "zjawisko"
  ]
  node [
    id 16
    label "praca_rolnicza"
  ]
  node [
    id 17
    label "tradycja"
  ]
  node [
    id 18
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 19
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 20
    label "przejmowanie"
  ]
  node [
    id 21
    label "cecha"
  ]
  node [
    id 22
    label "asymilowanie_si&#281;"
  ]
  node [
    id 23
    label "przej&#261;&#263;"
  ]
  node [
    id 24
    label "hodowla"
  ]
  node [
    id 25
    label "brzoskwiniarnia"
  ]
  node [
    id 26
    label "populace"
  ]
  node [
    id 27
    label "konwencja"
  ]
  node [
    id 28
    label "propriety"
  ]
  node [
    id 29
    label "jako&#347;&#263;"
  ]
  node [
    id 30
    label "kuchnia"
  ]
  node [
    id 31
    label "zwyczaj"
  ]
  node [
    id 32
    label "przej&#281;cie"
  ]
  node [
    id 33
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 34
    label "g&#243;rny"
  ]
  node [
    id 35
    label "&#346;l&#261;sk"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 34
    target 35
  ]
]
