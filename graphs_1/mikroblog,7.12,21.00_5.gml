graph [
  maxDegree 28
  minDegree 1
  meanDegree 2.0930232558139537
  density 0.012239902080783354
  graphCliqueNumber 3
  node [
    id 0
    label "pracabaza"
    origin "text"
  ]
  node [
    id 1
    label "pracbaza"
    origin "text"
  ]
  node [
    id 2
    label "polak"
    origin "text"
  ]
  node [
    id 3
    label "heheszki"
    origin "text"
  ]
  node [
    id 4
    label "szef"
    origin "text"
  ]
  node [
    id 5
    label "dzwoni&#263;"
    origin "text"
  ]
  node [
    id 6
    label "pracownica"
    origin "text"
  ]
  node [
    id 7
    label "godzina"
    origin "text"
  ]
  node [
    id 8
    label "praca"
    origin "text"
  ]
  node [
    id 9
    label "m&#261;&#380;"
    origin "text"
  ]
  node [
    id 10
    label "odbiera&#263;"
    origin "text"
  ]
  node [
    id 11
    label "telefon"
    origin "text"
  ]
  node [
    id 12
    label "sekunda"
    origin "text"
  ]
  node [
    id 13
    label "yyy"
    origin "text"
  ]
  node [
    id 14
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 15
    label "rozmawia&#263;"
    origin "text"
  ]
  node [
    id 16
    label "xyz"
    origin "text"
  ]
  node [
    id 17
    label "metr"
    origin "text"
  ]
  node [
    id 18
    label "wyj&#347;&#263;"
    origin "text"
  ]
  node [
    id 19
    label "rano"
    origin "text"
  ]
  node [
    id 20
    label "polski"
  ]
  node [
    id 21
    label "cz&#322;owiek"
  ]
  node [
    id 22
    label "kierowa&#263;"
  ]
  node [
    id 23
    label "zwrot"
  ]
  node [
    id 24
    label "kierownictwo"
  ]
  node [
    id 25
    label "pryncypa&#322;"
  ]
  node [
    id 26
    label "dzwonek"
  ]
  node [
    id 27
    label "call"
  ]
  node [
    id 28
    label "brzmie&#263;"
  ]
  node [
    id 29
    label "drynda&#263;"
  ]
  node [
    id 30
    label "brz&#281;cze&#263;"
  ]
  node [
    id 31
    label "sound"
  ]
  node [
    id 32
    label "bi&#263;"
  ]
  node [
    id 33
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 34
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 35
    label "minuta"
  ]
  node [
    id 36
    label "doba"
  ]
  node [
    id 37
    label "czas"
  ]
  node [
    id 38
    label "p&#243;&#322;godzina"
  ]
  node [
    id 39
    label "kwadrans"
  ]
  node [
    id 40
    label "time"
  ]
  node [
    id 41
    label "jednostka_czasu"
  ]
  node [
    id 42
    label "stosunek_pracy"
  ]
  node [
    id 43
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 44
    label "benedykty&#324;ski"
  ]
  node [
    id 45
    label "pracowanie"
  ]
  node [
    id 46
    label "zaw&#243;d"
  ]
  node [
    id 47
    label "zmiana"
  ]
  node [
    id 48
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 49
    label "wytw&#243;r"
  ]
  node [
    id 50
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 51
    label "tynkarski"
  ]
  node [
    id 52
    label "czynnik_produkcji"
  ]
  node [
    id 53
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 54
    label "zobowi&#261;zanie"
  ]
  node [
    id 55
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 56
    label "czynno&#347;&#263;"
  ]
  node [
    id 57
    label "tyrka"
  ]
  node [
    id 58
    label "pracowa&#263;"
  ]
  node [
    id 59
    label "siedziba"
  ]
  node [
    id 60
    label "poda&#380;_pracy"
  ]
  node [
    id 61
    label "miejsce"
  ]
  node [
    id 62
    label "zak&#322;ad"
  ]
  node [
    id 63
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 64
    label "najem"
  ]
  node [
    id 65
    label "pan_domu"
  ]
  node [
    id 66
    label "ch&#322;op"
  ]
  node [
    id 67
    label "ma&#322;&#380;onek"
  ]
  node [
    id 68
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 69
    label "stary"
  ]
  node [
    id 70
    label "&#347;lubny"
  ]
  node [
    id 71
    label "m&#243;j"
  ]
  node [
    id 72
    label "pan_i_w&#322;adca"
  ]
  node [
    id 73
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 74
    label "pan_m&#322;ody"
  ]
  node [
    id 75
    label "radio"
  ]
  node [
    id 76
    label "liszy&#263;"
  ]
  node [
    id 77
    label "przyjmowa&#263;"
  ]
  node [
    id 78
    label "deprive"
  ]
  node [
    id 79
    label "zabiera&#263;"
  ]
  node [
    id 80
    label "odzyskiwa&#263;"
  ]
  node [
    id 81
    label "accept"
  ]
  node [
    id 82
    label "antena"
  ]
  node [
    id 83
    label "telewizor"
  ]
  node [
    id 84
    label "pozbawia&#263;"
  ]
  node [
    id 85
    label "doznawa&#263;"
  ]
  node [
    id 86
    label "fall"
  ]
  node [
    id 87
    label "bra&#263;"
  ]
  node [
    id 88
    label "konfiskowa&#263;"
  ]
  node [
    id 89
    label "zlecenie"
  ]
  node [
    id 90
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 91
    label "s&#322;uchawka_telefoniczna"
  ]
  node [
    id 92
    label "coalescence"
  ]
  node [
    id 93
    label "roz&#322;&#261;cza&#263;"
  ]
  node [
    id 94
    label "phreaker"
  ]
  node [
    id 95
    label "infrastruktura"
  ]
  node [
    id 96
    label "wy&#347;wietlacz"
  ]
  node [
    id 97
    label "provider"
  ]
  node [
    id 98
    label "dzwonienie"
  ]
  node [
    id 99
    label "zadzwoni&#263;"
  ]
  node [
    id 100
    label "kontakt"
  ]
  node [
    id 101
    label "mikrotelefon"
  ]
  node [
    id 102
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 103
    label "po&#322;&#261;czenie"
  ]
  node [
    id 104
    label "numer"
  ]
  node [
    id 105
    label "roz&#322;&#261;czanie"
  ]
  node [
    id 106
    label "instalacja"
  ]
  node [
    id 107
    label "billing"
  ]
  node [
    id 108
    label "urz&#261;dzenie"
  ]
  node [
    id 109
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 110
    label "tercja"
  ]
  node [
    id 111
    label "milisekunda"
  ]
  node [
    id 112
    label "nanosekunda"
  ]
  node [
    id 113
    label "uk&#322;ad_SI"
  ]
  node [
    id 114
    label "mikrosekunda"
  ]
  node [
    id 115
    label "jednostka"
  ]
  node [
    id 116
    label "by&#263;"
  ]
  node [
    id 117
    label "uprawi&#263;"
  ]
  node [
    id 118
    label "gotowy"
  ]
  node [
    id 119
    label "might"
  ]
  node [
    id 120
    label "talk"
  ]
  node [
    id 121
    label "gaworzy&#263;"
  ]
  node [
    id 122
    label "meter"
  ]
  node [
    id 123
    label "decymetr"
  ]
  node [
    id 124
    label "megabyte"
  ]
  node [
    id 125
    label "plon"
  ]
  node [
    id 126
    label "metrum"
  ]
  node [
    id 127
    label "dekametr"
  ]
  node [
    id 128
    label "jednostka_powierzchni"
  ]
  node [
    id 129
    label "literaturoznawstwo"
  ]
  node [
    id 130
    label "wiersz"
  ]
  node [
    id 131
    label "gigametr"
  ]
  node [
    id 132
    label "miara"
  ]
  node [
    id 133
    label "nauczyciel"
  ]
  node [
    id 134
    label "kilometr_kwadratowy"
  ]
  node [
    id 135
    label "jednostka_metryczna"
  ]
  node [
    id 136
    label "jednostka_masy"
  ]
  node [
    id 137
    label "centymetr_kwadratowy"
  ]
  node [
    id 138
    label "get"
  ]
  node [
    id 139
    label "opu&#347;ci&#263;"
  ]
  node [
    id 140
    label "ukaza&#263;_si&#281;_drukiem"
  ]
  node [
    id 141
    label "zej&#347;&#263;"
  ]
  node [
    id 142
    label "okaza&#263;_si&#281;"
  ]
  node [
    id 143
    label "powie&#347;&#263;_si&#281;"
  ]
  node [
    id 144
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 145
    label "sko&#324;czy&#263;"
  ]
  node [
    id 146
    label "ograniczenie"
  ]
  node [
    id 147
    label "ruszy&#263;"
  ]
  node [
    id 148
    label "wypa&#347;&#263;"
  ]
  node [
    id 149
    label "uko&#324;czy&#263;"
  ]
  node [
    id 150
    label "open"
  ]
  node [
    id 151
    label "moderate"
  ]
  node [
    id 152
    label "uzyska&#263;"
  ]
  node [
    id 153
    label "wydosta&#263;_si&#281;"
  ]
  node [
    id 154
    label "wywie&#347;&#263;_si&#281;"
  ]
  node [
    id 155
    label "mount"
  ]
  node [
    id 156
    label "leave"
  ]
  node [
    id 157
    label "drive"
  ]
  node [
    id 158
    label "zagra&#263;"
  ]
  node [
    id 159
    label "zademonstrowa&#263;"
  ]
  node [
    id 160
    label "wystarczy&#263;"
  ]
  node [
    id 161
    label "perform"
  ]
  node [
    id 162
    label "uwolni&#263;_si&#281;"
  ]
  node [
    id 163
    label "drop"
  ]
  node [
    id 164
    label "wsch&#243;d"
  ]
  node [
    id 165
    label "aurora"
  ]
  node [
    id 166
    label "pora"
  ]
  node [
    id 167
    label "zjawisko"
  ]
  node [
    id 168
    label "dzie&#324;"
  ]
  node [
    id 169
    label "XD"
  ]
  node [
    id 170
    label "szach"
  ]
  node [
    id 171
    label "mat"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 11
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 35
  ]
  edge [
    source 7
    target 36
  ]
  edge [
    source 7
    target 37
  ]
  edge [
    source 7
    target 38
  ]
  edge [
    source 7
    target 39
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 19
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 8
    target 46
  ]
  edge [
    source 8
    target 24
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 9
    target 21
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 11
    target 92
  ]
  edge [
    source 11
    target 93
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 11
    target 98
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 11
    target 100
  ]
  edge [
    source 11
    target 101
  ]
  edge [
    source 11
    target 102
  ]
  edge [
    source 11
    target 103
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 108
  ]
  edge [
    source 11
    target 109
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 35
  ]
  edge [
    source 12
    target 110
  ]
  edge [
    source 12
    target 111
  ]
  edge [
    source 12
    target 112
  ]
  edge [
    source 12
    target 113
  ]
  edge [
    source 12
    target 114
  ]
  edge [
    source 12
    target 40
  ]
  edge [
    source 12
    target 41
  ]
  edge [
    source 12
    target 115
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 116
  ]
  edge [
    source 14
    target 117
  ]
  edge [
    source 14
    target 118
  ]
  edge [
    source 14
    target 119
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 120
  ]
  edge [
    source 15
    target 121
  ]
  edge [
    source 15
    target 33
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 122
  ]
  edge [
    source 17
    target 123
  ]
  edge [
    source 17
    target 124
  ]
  edge [
    source 17
    target 125
  ]
  edge [
    source 17
    target 126
  ]
  edge [
    source 17
    target 127
  ]
  edge [
    source 17
    target 128
  ]
  edge [
    source 17
    target 113
  ]
  edge [
    source 17
    target 129
  ]
  edge [
    source 17
    target 130
  ]
  edge [
    source 17
    target 131
  ]
  edge [
    source 17
    target 132
  ]
  edge [
    source 17
    target 133
  ]
  edge [
    source 17
    target 134
  ]
  edge [
    source 17
    target 135
  ]
  edge [
    source 17
    target 136
  ]
  edge [
    source 17
    target 137
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 138
  ]
  edge [
    source 18
    target 139
  ]
  edge [
    source 18
    target 140
  ]
  edge [
    source 18
    target 141
  ]
  edge [
    source 18
    target 142
  ]
  edge [
    source 18
    target 143
  ]
  edge [
    source 18
    target 144
  ]
  edge [
    source 18
    target 145
  ]
  edge [
    source 18
    target 146
  ]
  edge [
    source 18
    target 147
  ]
  edge [
    source 18
    target 148
  ]
  edge [
    source 18
    target 149
  ]
  edge [
    source 18
    target 150
  ]
  edge [
    source 18
    target 151
  ]
  edge [
    source 18
    target 152
  ]
  edge [
    source 18
    target 153
  ]
  edge [
    source 18
    target 154
  ]
  edge [
    source 18
    target 155
  ]
  edge [
    source 18
    target 156
  ]
  edge [
    source 18
    target 157
  ]
  edge [
    source 18
    target 158
  ]
  edge [
    source 18
    target 159
  ]
  edge [
    source 18
    target 160
  ]
  edge [
    source 18
    target 161
  ]
  edge [
    source 18
    target 162
  ]
  edge [
    source 18
    target 163
  ]
  edge [
    source 19
    target 164
  ]
  edge [
    source 19
    target 165
  ]
  edge [
    source 19
    target 166
  ]
  edge [
    source 19
    target 167
  ]
  edge [
    source 19
    target 168
  ]
  edge [
    source 169
    target 170
  ]
  edge [
    source 169
    target 171
  ]
  edge [
    source 170
    target 171
  ]
]
