graph [
  maxDegree 29
  minDegree 1
  meanDegree 1.9701492537313432
  density 0.029850746268656716
  graphCliqueNumber 2
  node [
    id 0
    label "&#347;cisn&#261;&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "&#322;eba"
    origin "text"
  ]
  node [
    id 2
    label "farba"
    origin "text"
  ]
  node [
    id 3
    label "oko"
    origin "text"
  ]
  node [
    id 4
    label "p&#243;j&#347;&#263;"
    origin "text"
  ]
  node [
    id 5
    label "krycie"
  ]
  node [
    id 6
    label "krew"
  ]
  node [
    id 7
    label "punktowa&#263;"
  ]
  node [
    id 8
    label "pr&#243;szy&#263;"
  ]
  node [
    id 9
    label "kry&#263;"
  ]
  node [
    id 10
    label "podk&#322;ad"
  ]
  node [
    id 11
    label "blik"
  ]
  node [
    id 12
    label "zwierz&#281;"
  ]
  node [
    id 13
    label "substancja"
  ]
  node [
    id 14
    label "wypunktowa&#263;"
  ]
  node [
    id 15
    label "kolor"
  ]
  node [
    id 16
    label "pr&#243;szenie"
  ]
  node [
    id 17
    label "wypowied&#378;"
  ]
  node [
    id 18
    label "siniec"
  ]
  node [
    id 19
    label "uwaga"
  ]
  node [
    id 20
    label "rzecz"
  ]
  node [
    id 21
    label "&#380;y&#322;a_&#347;rodkowa_siatk&#243;wki"
  ]
  node [
    id 22
    label "powieka"
  ]
  node [
    id 23
    label "oczy"
  ]
  node [
    id 24
    label "&#347;lepko"
  ]
  node [
    id 25
    label "ga&#322;ka_oczna"
  ]
  node [
    id 26
    label "ser_&#380;&#243;&#322;ty"
  ]
  node [
    id 27
    label "&#347;lepie"
  ]
  node [
    id 28
    label "twarz"
  ]
  node [
    id 29
    label "organ"
  ]
  node [
    id 30
    label "worek_spoj&#243;wkowy"
  ]
  node [
    id 31
    label "nerw_wzrokowy"
  ]
  node [
    id 32
    label "wzrok"
  ]
  node [
    id 33
    label "&#378;renica"
  ]
  node [
    id 34
    label "spoj&#243;wka"
  ]
  node [
    id 35
    label "narz&#261;d_&#322;zowy"
  ]
  node [
    id 36
    label "nab&#322;onek_barwnikowy"
  ]
  node [
    id 37
    label "kaprawie&#263;"
  ]
  node [
    id 38
    label "kaprawienie"
  ]
  node [
    id 39
    label "spojrzenie"
  ]
  node [
    id 40
    label "net"
  ]
  node [
    id 41
    label "r&#261;bek_rog&#243;wki"
  ]
  node [
    id 42
    label "coloboma"
  ]
  node [
    id 43
    label "ros&#243;&#322;"
  ]
  node [
    id 44
    label "opu&#347;ci&#263;"
  ]
  node [
    id 45
    label "poruszy&#263;_si&#281;"
  ]
  node [
    id 46
    label "proceed"
  ]
  node [
    id 47
    label "powie&#347;&#263;_si&#281;"
  ]
  node [
    id 48
    label "popsu&#263;_si&#281;"
  ]
  node [
    id 49
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 50
    label "kopn&#261;&#263;_si&#281;"
  ]
  node [
    id 51
    label "zacz&#261;&#263;"
  ]
  node [
    id 52
    label "zmieni&#263;"
  ]
  node [
    id 53
    label "zosta&#263;"
  ]
  node [
    id 54
    label "sail"
  ]
  node [
    id 55
    label "leave"
  ]
  node [
    id 56
    label "uda&#263;_si&#281;"
  ]
  node [
    id 57
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 58
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 59
    label "zrobi&#263;"
  ]
  node [
    id 60
    label "wyp&#322;yn&#261;&#263;"
  ]
  node [
    id 61
    label "sprzeda&#263;_si&#281;"
  ]
  node [
    id 62
    label "przyj&#261;&#263;"
  ]
  node [
    id 63
    label "potoczy&#263;_si&#281;"
  ]
  node [
    id 64
    label "become"
  ]
  node [
    id 65
    label "play_along"
  ]
  node [
    id 66
    label "travel"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
]
