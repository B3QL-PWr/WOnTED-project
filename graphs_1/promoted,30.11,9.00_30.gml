graph [
  maxDegree 28
  minDegree 1
  meanDegree 2
  density 0.02666666666666667
  graphCliqueNumber 2
  node [
    id 0
    label "bestialski"
    origin "text"
  ]
  node [
    id 1
    label "zachowanie"
    origin "text"
  ]
  node [
    id 2
    label "w&#322;a&#347;cicielka"
    origin "text"
  ]
  node [
    id 3
    label "pies"
    origin "text"
  ]
  node [
    id 4
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 5
    label "dla"
    origin "text"
  ]
  node [
    id 6
    label "swoje"
    origin "text"
  ]
  node [
    id 7
    label "czworon&#243;g"
    origin "text"
  ]
  node [
    id 8
    label "okaza&#322;y"
    origin "text"
  ]
  node [
    id 9
    label "si&#281;"
    origin "text"
  ]
  node [
    id 10
    label "kat"
    origin "text"
  ]
  node [
    id 11
    label "z&#322;y"
  ]
  node [
    id 12
    label "okrutny"
  ]
  node [
    id 13
    label "zwierz&#281;cy"
  ]
  node [
    id 14
    label "bestialsko"
  ]
  node [
    id 15
    label "zwierz&#281;"
  ]
  node [
    id 16
    label "zrobienie"
  ]
  node [
    id 17
    label "podporz&#261;dkowanie_si&#281;"
  ]
  node [
    id 18
    label "podtrzymanie"
  ]
  node [
    id 19
    label "reakcja"
  ]
  node [
    id 20
    label "tajemnica"
  ]
  node [
    id 21
    label "zdyscyplinowanie"
  ]
  node [
    id 22
    label "observation"
  ]
  node [
    id 23
    label "behawior"
  ]
  node [
    id 24
    label "dieta"
  ]
  node [
    id 25
    label "bearing"
  ]
  node [
    id 26
    label "pochowanie"
  ]
  node [
    id 27
    label "wydarzenie"
  ]
  node [
    id 28
    label "przechowanie"
  ]
  node [
    id 29
    label "post&#261;pienie"
  ]
  node [
    id 30
    label "post"
  ]
  node [
    id 31
    label "struktura"
  ]
  node [
    id 32
    label "spos&#243;b"
  ]
  node [
    id 33
    label "etolog"
  ]
  node [
    id 34
    label "cz&#322;owiek"
  ]
  node [
    id 35
    label "wy&#263;"
  ]
  node [
    id 36
    label "ssak_drapie&#380;ny"
  ]
  node [
    id 37
    label "spragniony"
  ]
  node [
    id 38
    label "rakarz"
  ]
  node [
    id 39
    label "psowate"
  ]
  node [
    id 40
    label "istota_&#380;ywa"
  ]
  node [
    id 41
    label "kabanos"
  ]
  node [
    id 42
    label "mi&#322;o&#347;nik"
  ]
  node [
    id 43
    label "&#322;ajdak"
  ]
  node [
    id 44
    label "policjant"
  ]
  node [
    id 45
    label "szczucie"
  ]
  node [
    id 46
    label "s&#322;u&#380;enie"
  ]
  node [
    id 47
    label "sobaka"
  ]
  node [
    id 48
    label "dogoterapia"
  ]
  node [
    id 49
    label "Cerber"
  ]
  node [
    id 50
    label "wyzwisko"
  ]
  node [
    id 51
    label "szczu&#263;"
  ]
  node [
    id 52
    label "wycie"
  ]
  node [
    id 53
    label "szczeka&#263;"
  ]
  node [
    id 54
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 55
    label "trufla"
  ]
  node [
    id 56
    label "samiec"
  ]
  node [
    id 57
    label "piese&#322;"
  ]
  node [
    id 58
    label "zawy&#263;"
  ]
  node [
    id 59
    label "kr&#281;gowiec"
  ]
  node [
    id 60
    label "zwierz&#281;_domowe"
  ]
  node [
    id 61
    label "tetrapody"
  ]
  node [
    id 62
    label "critter"
  ]
  node [
    id 63
    label "bogaty"
  ]
  node [
    id 64
    label "okazale"
  ]
  node [
    id 65
    label "imponuj&#261;cy"
  ]
  node [
    id 66
    label "poka&#378;ny"
  ]
  node [
    id 67
    label "prze&#347;ladowca"
  ]
  node [
    id 68
    label "funkcjonariusz"
  ]
  node [
    id 69
    label "zwyrodnialec"
  ]
  node [
    id 70
    label "morderca"
  ]
  node [
    id 71
    label "okrutnik"
  ]
  node [
    id 72
    label "ryba"
  ]
  node [
    id 73
    label "ciernikowate"
  ]
  node [
    id 74
    label "ciemi&#281;zca"
  ]
  node [
    id 75
    label "kara_&#347;mierci"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 10
    target 69
  ]
  edge [
    source 10
    target 70
  ]
  edge [
    source 10
    target 71
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 75
  ]
]
