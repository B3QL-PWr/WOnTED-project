graph [
  maxDegree 38
  minDegree 1
  meanDegree 1.951219512195122
  density 0.04878048780487805
  graphCliqueNumber 2
  node [
    id 0
    label "osoba"
    origin "text"
  ]
  node [
    id 1
    label "lata"
    origin "text"
  ]
  node [
    id 2
    label "Zgredek"
  ]
  node [
    id 3
    label "kategoria_gramatyczna"
  ]
  node [
    id 4
    label "Casanova"
  ]
  node [
    id 5
    label "Don_Juan"
  ]
  node [
    id 6
    label "Gargantua"
  ]
  node [
    id 7
    label "Faust"
  ]
  node [
    id 8
    label "profanum"
  ]
  node [
    id 9
    label "Chocho&#322;"
  ]
  node [
    id 10
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 11
    label "koniugacja"
  ]
  node [
    id 12
    label "Winnetou"
  ]
  node [
    id 13
    label "Dwukwiat"
  ]
  node [
    id 14
    label "homo_sapiens"
  ]
  node [
    id 15
    label "Edyp"
  ]
  node [
    id 16
    label "Herkules_Poirot"
  ]
  node [
    id 17
    label "ludzko&#347;&#263;"
  ]
  node [
    id 18
    label "mikrokosmos"
  ]
  node [
    id 19
    label "person"
  ]
  node [
    id 20
    label "Szwejk"
  ]
  node [
    id 21
    label "portrecista"
  ]
  node [
    id 22
    label "Sherlock_Holmes"
  ]
  node [
    id 23
    label "Hamlet"
  ]
  node [
    id 24
    label "duch"
  ]
  node [
    id 25
    label "oddzia&#322;ywanie"
  ]
  node [
    id 26
    label "g&#322;owa"
  ]
  node [
    id 27
    label "Quasimodo"
  ]
  node [
    id 28
    label "Dulcynea"
  ]
  node [
    id 29
    label "Wallenrod"
  ]
  node [
    id 30
    label "Don_Kiszot"
  ]
  node [
    id 31
    label "Plastu&#347;"
  ]
  node [
    id 32
    label "Harry_Potter"
  ]
  node [
    id 33
    label "figura"
  ]
  node [
    id 34
    label "parali&#380;owa&#263;"
  ]
  node [
    id 35
    label "istota"
  ]
  node [
    id 36
    label "Werter"
  ]
  node [
    id 37
    label "antropochoria"
  ]
  node [
    id 38
    label "posta&#263;"
  ]
  node [
    id 39
    label "summer"
  ]
  node [
    id 40
    label "czas"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
]
