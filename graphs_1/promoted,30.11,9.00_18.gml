graph [
  maxDegree 38
  minDegree 1
  meanDegree 1.9565217391304348
  density 0.043478260869565216
  graphCliqueNumber 2
  node [
    id 0
    label "brawa"
    origin "text"
  ]
  node [
    id 1
    label "dla"
    origin "text"
  ]
  node [
    id 2
    label "pan"
    origin "text"
  ]
  node [
    id 3
    label "policjant"
    origin "text"
  ]
  node [
    id 4
    label "cz&#322;owiek"
  ]
  node [
    id 5
    label "profesor"
  ]
  node [
    id 6
    label "kszta&#322;ciciel"
  ]
  node [
    id 7
    label "jegomo&#347;&#263;"
  ]
  node [
    id 8
    label "zwrot"
  ]
  node [
    id 9
    label "pracodawca"
  ]
  node [
    id 10
    label "rz&#261;dzenie"
  ]
  node [
    id 11
    label "m&#261;&#380;"
  ]
  node [
    id 12
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 13
    label "ch&#322;opina"
  ]
  node [
    id 14
    label "bratek"
  ]
  node [
    id 15
    label "opiekun"
  ]
  node [
    id 16
    label "doros&#322;y"
  ]
  node [
    id 17
    label "preceptor"
  ]
  node [
    id 18
    label "Midas"
  ]
  node [
    id 19
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 20
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 21
    label "murza"
  ]
  node [
    id 22
    label "ojciec"
  ]
  node [
    id 23
    label "androlog"
  ]
  node [
    id 24
    label "pupil"
  ]
  node [
    id 25
    label "efendi"
  ]
  node [
    id 26
    label "nabab"
  ]
  node [
    id 27
    label "w&#322;odarz"
  ]
  node [
    id 28
    label "szkolnik"
  ]
  node [
    id 29
    label "pedagog"
  ]
  node [
    id 30
    label "popularyzator"
  ]
  node [
    id 31
    label "andropauza"
  ]
  node [
    id 32
    label "gra_w_karty"
  ]
  node [
    id 33
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 34
    label "Mieszko_I"
  ]
  node [
    id 35
    label "bogaty"
  ]
  node [
    id 36
    label "samiec"
  ]
  node [
    id 37
    label "przyw&#243;dca"
  ]
  node [
    id 38
    label "pa&#324;stwo"
  ]
  node [
    id 39
    label "belfer"
  ]
  node [
    id 40
    label "policja"
  ]
  node [
    id 41
    label "blacharz"
  ]
  node [
    id 42
    label "pa&#322;a"
  ]
  node [
    id 43
    label "mundurowy"
  ]
  node [
    id 44
    label "str&#243;&#380;"
  ]
  node [
    id 45
    label "glina"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
]
