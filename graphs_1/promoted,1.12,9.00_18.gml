graph [
  maxDegree 26
  minDegree 1
  meanDegree 2.017543859649123
  density 0.01785437043937277
  graphCliqueNumber 2
  node [
    id 0
    label "wed&#322;ug"
    origin "text"
  ]
  node [
    id 1
    label "statystyk"
    origin "text"
  ]
  node [
    id 2
    label "departament"
    origin "text"
  ]
  node [
    id 3
    label "praca"
    origin "text"
  ]
  node [
    id 4
    label "usa"
    origin "text"
  ]
  node [
    id 5
    label "dostawca"
    origin "text"
  ]
  node [
    id 6
    label "pizza"
    origin "text"
  ]
  node [
    id 7
    label "og&#243;lnie"
    origin "text"
  ]
  node [
    id 8
    label "driver"
    origin "text"
  ]
  node [
    id 9
    label "sales"
    origin "text"
  ]
  node [
    id 10
    label "workers"
    origin "text"
  ]
  node [
    id 11
    label "pi&#261;ty"
    origin "text"
  ]
  node [
    id 12
    label "najbardziej"
    origin "text"
  ]
  node [
    id 13
    label "niebezpieczny"
    origin "text"
  ]
  node [
    id 14
    label "zaw&#243;d"
    origin "text"
  ]
  node [
    id 15
    label "ameryka"
    origin "text"
  ]
  node [
    id 16
    label "by&#263;"
    origin "text"
  ]
  node [
    id 17
    label "zabija&#263;"
    origin "text"
  ]
  node [
    id 18
    label "bity"
    origin "text"
  ]
  node [
    id 19
    label "nieprzytomno&#347;&#263;"
    origin "text"
  ]
  node [
    id 20
    label "okrada&#263;"
    origin "text"
  ]
  node [
    id 21
    label "matematyk"
  ]
  node [
    id 22
    label "Gwadelupa"
  ]
  node [
    id 23
    label "jednostka_administracyjna"
  ]
  node [
    id 24
    label "relation"
  ]
  node [
    id 25
    label "jednostka_organizacyjna"
  ]
  node [
    id 26
    label "Moza"
  ]
  node [
    id 27
    label "Martynika"
  ]
  node [
    id 28
    label "ministerstwo"
  ]
  node [
    id 29
    label "podsekcja"
  ]
  node [
    id 30
    label "stosunek_pracy"
  ]
  node [
    id 31
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 32
    label "benedykty&#324;ski"
  ]
  node [
    id 33
    label "pracowanie"
  ]
  node [
    id 34
    label "kierownictwo"
  ]
  node [
    id 35
    label "zmiana"
  ]
  node [
    id 36
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 37
    label "wytw&#243;r"
  ]
  node [
    id 38
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 39
    label "tynkarski"
  ]
  node [
    id 40
    label "czynnik_produkcji"
  ]
  node [
    id 41
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 42
    label "zobowi&#261;zanie"
  ]
  node [
    id 43
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 44
    label "czynno&#347;&#263;"
  ]
  node [
    id 45
    label "tyrka"
  ]
  node [
    id 46
    label "pracowa&#263;"
  ]
  node [
    id 47
    label "siedziba"
  ]
  node [
    id 48
    label "poda&#380;_pracy"
  ]
  node [
    id 49
    label "miejsce"
  ]
  node [
    id 50
    label "zak&#322;ad"
  ]
  node [
    id 51
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 52
    label "najem"
  ]
  node [
    id 53
    label "podmiot_gospodarczy"
  ]
  node [
    id 54
    label "us&#322;ugodawca"
  ]
  node [
    id 55
    label "transportowiec"
  ]
  node [
    id 56
    label "wypiek"
  ]
  node [
    id 57
    label "fast_food"
  ]
  node [
    id 58
    label "sp&#243;d"
  ]
  node [
    id 59
    label "zbiorowo"
  ]
  node [
    id 60
    label "nadrz&#281;dnie"
  ]
  node [
    id 61
    label "generalny"
  ]
  node [
    id 62
    label "og&#243;lny"
  ]
  node [
    id 63
    label "posp&#243;lnie"
  ]
  node [
    id 64
    label "&#322;&#261;cznie"
  ]
  node [
    id 65
    label "program"
  ]
  node [
    id 66
    label "kierowca"
  ]
  node [
    id 67
    label "wood"
  ]
  node [
    id 68
    label "dzie&#324;"
  ]
  node [
    id 69
    label "gro&#378;ny"
  ]
  node [
    id 70
    label "k&#322;opotliwy"
  ]
  node [
    id 71
    label "niebezpiecznie"
  ]
  node [
    id 72
    label "kwalifikacje"
  ]
  node [
    id 73
    label "emocja"
  ]
  node [
    id 74
    label "zawodoznawstwo"
  ]
  node [
    id 75
    label "office"
  ]
  node [
    id 76
    label "craft"
  ]
  node [
    id 77
    label "si&#281;ga&#263;"
  ]
  node [
    id 78
    label "trwa&#263;"
  ]
  node [
    id 79
    label "obecno&#347;&#263;"
  ]
  node [
    id 80
    label "stan"
  ]
  node [
    id 81
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 82
    label "stand"
  ]
  node [
    id 83
    label "mie&#263;_miejsce"
  ]
  node [
    id 84
    label "uczestniczy&#263;"
  ]
  node [
    id 85
    label "chodzi&#263;"
  ]
  node [
    id 86
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 87
    label "equal"
  ]
  node [
    id 88
    label "niszczy&#263;"
  ]
  node [
    id 89
    label "krzywdzi&#263;"
  ]
  node [
    id 90
    label "rzuca&#263;_na_kolana"
  ]
  node [
    id 91
    label "os&#322;ania&#263;"
  ]
  node [
    id 92
    label "kill"
  ]
  node [
    id 93
    label "beat"
  ]
  node [
    id 94
    label "zako&#324;cza&#263;"
  ]
  node [
    id 95
    label "dispatch"
  ]
  node [
    id 96
    label "morzy&#263;"
  ]
  node [
    id 97
    label "bi&#263;"
  ]
  node [
    id 98
    label "mordowa&#263;"
  ]
  node [
    id 99
    label "powodowa&#263;_&#347;mier&#263;"
  ]
  node [
    id 100
    label "karci&#263;"
  ]
  node [
    id 101
    label "zakrywa&#263;"
  ]
  node [
    id 102
    label "rozbraja&#263;"
  ]
  node [
    id 103
    label "przybija&#263;"
  ]
  node [
    id 104
    label "zwalcza&#263;"
  ]
  node [
    id 105
    label "ca&#322;y"
  ]
  node [
    id 106
    label "utwardzony"
  ]
  node [
    id 107
    label "uszkodzony"
  ]
  node [
    id 108
    label "unconsciousness"
  ]
  node [
    id 109
    label "wyraz"
  ]
  node [
    id 110
    label "zabiera&#263;"
  ]
  node [
    id 111
    label "pamper"
  ]
  node [
    id 112
    label "obskakiwa&#263;"
  ]
  node [
    id 113
    label "obdziera&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 68
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 69
  ]
  edge [
    source 13
    target 70
  ]
  edge [
    source 13
    target 71
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 72
  ]
  edge [
    source 14
    target 73
  ]
  edge [
    source 14
    target 74
  ]
  edge [
    source 14
    target 75
  ]
  edge [
    source 14
    target 76
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 77
  ]
  edge [
    source 16
    target 78
  ]
  edge [
    source 16
    target 79
  ]
  edge [
    source 16
    target 80
  ]
  edge [
    source 16
    target 81
  ]
  edge [
    source 16
    target 82
  ]
  edge [
    source 16
    target 83
  ]
  edge [
    source 16
    target 84
  ]
  edge [
    source 16
    target 85
  ]
  edge [
    source 16
    target 86
  ]
  edge [
    source 16
    target 87
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 88
  ]
  edge [
    source 17
    target 89
  ]
  edge [
    source 17
    target 90
  ]
  edge [
    source 17
    target 91
  ]
  edge [
    source 17
    target 92
  ]
  edge [
    source 17
    target 93
  ]
  edge [
    source 17
    target 94
  ]
  edge [
    source 17
    target 95
  ]
  edge [
    source 17
    target 96
  ]
  edge [
    source 17
    target 97
  ]
  edge [
    source 17
    target 98
  ]
  edge [
    source 17
    target 99
  ]
  edge [
    source 17
    target 100
  ]
  edge [
    source 17
    target 101
  ]
  edge [
    source 17
    target 102
  ]
  edge [
    source 17
    target 103
  ]
  edge [
    source 17
    target 104
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 105
  ]
  edge [
    source 18
    target 106
  ]
  edge [
    source 18
    target 107
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 108
  ]
  edge [
    source 19
    target 109
  ]
  edge [
    source 19
    target 80
  ]
  edge [
    source 20
    target 110
  ]
  edge [
    source 20
    target 111
  ]
  edge [
    source 20
    target 112
  ]
  edge [
    source 20
    target 113
  ]
]
