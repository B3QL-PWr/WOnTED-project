graph [
  maxDegree 28
  minDegree 1
  meanDegree 1.9428571428571428
  density 0.05714285714285714
  graphCliqueNumber 2
  node [
    id 0
    label "zaras"
    origin "text"
  ]
  node [
    id 1
    label "spuszczem"
    origin "text"
  ]
  node [
    id 2
    label "pies"
    origin "text"
  ]
  node [
    id 3
    label "siem"
    origin "text"
  ]
  node [
    id 4
    label "uspokoi&#263;"
    origin "text"
  ]
  node [
    id 5
    label "cz&#322;owiek"
  ]
  node [
    id 6
    label "wy&#263;"
  ]
  node [
    id 7
    label "ssak_drapie&#380;ny"
  ]
  node [
    id 8
    label "spragniony"
  ]
  node [
    id 9
    label "rakarz"
  ]
  node [
    id 10
    label "psowate"
  ]
  node [
    id 11
    label "istota_&#380;ywa"
  ]
  node [
    id 12
    label "kabanos"
  ]
  node [
    id 13
    label "mi&#322;o&#347;nik"
  ]
  node [
    id 14
    label "&#322;ajdak"
  ]
  node [
    id 15
    label "czworon&#243;g"
  ]
  node [
    id 16
    label "policjant"
  ]
  node [
    id 17
    label "szczucie"
  ]
  node [
    id 18
    label "s&#322;u&#380;enie"
  ]
  node [
    id 19
    label "sobaka"
  ]
  node [
    id 20
    label "dogoterapia"
  ]
  node [
    id 21
    label "Cerber"
  ]
  node [
    id 22
    label "wyzwisko"
  ]
  node [
    id 23
    label "szczu&#263;"
  ]
  node [
    id 24
    label "wycie"
  ]
  node [
    id 25
    label "szczeka&#263;"
  ]
  node [
    id 26
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 27
    label "trufla"
  ]
  node [
    id 28
    label "samiec"
  ]
  node [
    id 29
    label "piese&#322;"
  ]
  node [
    id 30
    label "zawy&#263;"
  ]
  node [
    id 31
    label "zapanowa&#263;"
  ]
  node [
    id 32
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 33
    label "przywr&#243;ci&#263;"
  ]
  node [
    id 34
    label "accommodate"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
]
