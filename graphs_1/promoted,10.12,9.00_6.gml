graph [
  maxDegree 32
  minDegree 1
  meanDegree 2
  density 0.023255813953488372
  graphCliqueNumber 3
  node [
    id 0
    label "dynamiczny"
    origin "text"
  ]
  node [
    id 1
    label "rozw&#243;j"
    origin "text"
  ]
  node [
    id 2
    label "&#322;&#243;d&#378;"
    origin "text"
  ]
  node [
    id 3
    label "i&#347;&#263;"
    origin "text"
  ]
  node [
    id 4
    label "par"
    origin "text"
  ]
  node [
    id 5
    label "dba&#322;o&#347;&#263;"
    origin "text"
  ]
  node [
    id 6
    label "struktura"
    origin "text"
  ]
  node [
    id 7
    label "przestrzenny"
    origin "text"
  ]
  node [
    id 8
    label "dynamizowanie"
  ]
  node [
    id 9
    label "zdynamizowanie"
  ]
  node [
    id 10
    label "Tuesday"
  ]
  node [
    id 11
    label "dynamicznie"
  ]
  node [
    id 12
    label "energicznie"
  ]
  node [
    id 13
    label "&#380;ywy"
  ]
  node [
    id 14
    label "zmienny"
  ]
  node [
    id 15
    label "aktywny"
  ]
  node [
    id 16
    label "ostry"
  ]
  node [
    id 17
    label "mocny"
  ]
  node [
    id 18
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 19
    label "procedura"
  ]
  node [
    id 20
    label "process"
  ]
  node [
    id 21
    label "cycle"
  ]
  node [
    id 22
    label "proces"
  ]
  node [
    id 23
    label "&#380;ycie"
  ]
  node [
    id 24
    label "z&#322;ote_czasy"
  ]
  node [
    id 25
    label "proces_biologiczny"
  ]
  node [
    id 26
    label "regaty"
  ]
  node [
    id 27
    label "statek"
  ]
  node [
    id 28
    label "spalin&#243;wka"
  ]
  node [
    id 29
    label "pok&#322;ad"
  ]
  node [
    id 30
    label "ster"
  ]
  node [
    id 31
    label "kratownica"
  ]
  node [
    id 32
    label "pojazd_niemechaniczny"
  ]
  node [
    id 33
    label "drzewce"
  ]
  node [
    id 34
    label "impart"
  ]
  node [
    id 35
    label "proceed"
  ]
  node [
    id 36
    label "czas"
  ]
  node [
    id 37
    label "dzia&#263;_si&#281;"
  ]
  node [
    id 38
    label "lecie&#263;"
  ]
  node [
    id 39
    label "blend"
  ]
  node [
    id 40
    label "bangla&#263;"
  ]
  node [
    id 41
    label "trace"
  ]
  node [
    id 42
    label "describe"
  ]
  node [
    id 43
    label "wydostawa&#263;_si&#281;"
  ]
  node [
    id 44
    label "by&#263;"
  ]
  node [
    id 45
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 46
    label "post&#281;powa&#263;"
  ]
  node [
    id 47
    label "toczy&#263;_si&#281;"
  ]
  node [
    id 48
    label "tryb"
  ]
  node [
    id 49
    label "bie&#380;e&#263;"
  ]
  node [
    id 50
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 51
    label "atakowa&#263;"
  ]
  node [
    id 52
    label "continue"
  ]
  node [
    id 53
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 54
    label "try"
  ]
  node [
    id 55
    label "mie&#263;_miejsce"
  ]
  node [
    id 56
    label "boost"
  ]
  node [
    id 57
    label "draw"
  ]
  node [
    id 58
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 59
    label "zmienia&#263;_pozycj&#281;"
  ]
  node [
    id 60
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 61
    label "wyrusza&#263;"
  ]
  node [
    id 62
    label "dziama&#263;"
  ]
  node [
    id 63
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 64
    label "Izba_Par&#243;w"
  ]
  node [
    id 65
    label "lord"
  ]
  node [
    id 66
    label "Izba_Lord&#243;w"
  ]
  node [
    id 67
    label "parlamentarzysta"
  ]
  node [
    id 68
    label "lennik"
  ]
  node [
    id 69
    label "accuracy"
  ]
  node [
    id 70
    label "cecha"
  ]
  node [
    id 71
    label "system"
  ]
  node [
    id 72
    label "rozprz&#261;c"
  ]
  node [
    id 73
    label "konstrukcja"
  ]
  node [
    id 74
    label "zachowanie"
  ]
  node [
    id 75
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 76
    label "cybernetyk"
  ]
  node [
    id 77
    label "podsystem"
  ]
  node [
    id 78
    label "o&#347;"
  ]
  node [
    id 79
    label "konstelacja"
  ]
  node [
    id 80
    label "sk&#322;ad"
  ]
  node [
    id 81
    label "usenet"
  ]
  node [
    id 82
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 83
    label "mechanika"
  ]
  node [
    id 84
    label "systemat"
  ]
  node [
    id 85
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 86
    label "tr&#243;jwymiarowo"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 7
    target 86
  ]
]
