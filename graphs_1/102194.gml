graph [
  maxDegree 42
  minDegree 1
  meanDegree 1.9705882352941178
  density 0.029411764705882353
  graphCliqueNumber 2
  node [
    id 0
    label "budzi&#263;"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "jak"
    origin "text"
  ]
  node [
    id 3
    label "m&#322;ynarz"
    origin "text"
  ]
  node [
    id 4
    label "gdy"
    origin "text"
  ]
  node [
    id 5
    label "woda"
    origin "text"
  ]
  node [
    id 6
    label "hucze&#263;"
    origin "text"
  ]
  node [
    id 7
    label "przesta&#322;y"
    origin "text"
  ]
  node [
    id 8
    label "wywo&#322;ywa&#263;"
  ]
  node [
    id 9
    label "wyrywa&#263;"
  ]
  node [
    id 10
    label "prompt"
  ]
  node [
    id 11
    label "o&#380;ywia&#263;"
  ]
  node [
    id 12
    label "go"
  ]
  node [
    id 13
    label "byd&#322;o"
  ]
  node [
    id 14
    label "zobo"
  ]
  node [
    id 15
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 16
    label "yakalo"
  ]
  node [
    id 17
    label "dzo"
  ]
  node [
    id 18
    label "m&#322;ynarczyk"
  ]
  node [
    id 19
    label "rzemie&#347;lnik"
  ]
  node [
    id 20
    label "wypowied&#378;"
  ]
  node [
    id 21
    label "obiekt_naturalny"
  ]
  node [
    id 22
    label "bicie"
  ]
  node [
    id 23
    label "wysi&#281;k"
  ]
  node [
    id 24
    label "pustka"
  ]
  node [
    id 25
    label "woda_s&#322;odka"
  ]
  node [
    id 26
    label "p&#322;ycizna"
  ]
  node [
    id 27
    label "ciecz"
  ]
  node [
    id 28
    label "spi&#281;trza&#263;"
  ]
  node [
    id 29
    label "uj&#281;cie_wody"
  ]
  node [
    id 30
    label "chlasta&#263;"
  ]
  node [
    id 31
    label "spi&#281;trzy&#263;"
  ]
  node [
    id 32
    label "nap&#243;j"
  ]
  node [
    id 33
    label "bombast"
  ]
  node [
    id 34
    label "water"
  ]
  node [
    id 35
    label "kryptodepresja"
  ]
  node [
    id 36
    label "wodnik"
  ]
  node [
    id 37
    label "pojazd"
  ]
  node [
    id 38
    label "&#347;r&#243;dl&#261;dowy"
  ]
  node [
    id 39
    label "fala"
  ]
  node [
    id 40
    label "Waruna"
  ]
  node [
    id 41
    label "nap&#243;j_bezalkoholowy"
  ]
  node [
    id 42
    label "zrzut"
  ]
  node [
    id 43
    label "dotleni&#263;"
  ]
  node [
    id 44
    label "utylizator"
  ]
  node [
    id 45
    label "przyroda"
  ]
  node [
    id 46
    label "uci&#261;g"
  ]
  node [
    id 47
    label "wybrze&#380;e"
  ]
  node [
    id 48
    label "nabranie"
  ]
  node [
    id 49
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 50
    label "klarownik"
  ]
  node [
    id 51
    label "chlastanie"
  ]
  node [
    id 52
    label "przybrze&#380;e"
  ]
  node [
    id 53
    label "deklamacja"
  ]
  node [
    id 54
    label "spi&#281;trzenie"
  ]
  node [
    id 55
    label "przybieranie"
  ]
  node [
    id 56
    label "nabra&#263;"
  ]
  node [
    id 57
    label "tlenek"
  ]
  node [
    id 58
    label "spi&#281;trzanie"
  ]
  node [
    id 59
    label "l&#243;d"
  ]
  node [
    id 60
    label "rant"
  ]
  node [
    id 61
    label "m&#243;wi&#263;"
  ]
  node [
    id 62
    label "brzmie&#263;"
  ]
  node [
    id 63
    label "peal"
  ]
  node [
    id 64
    label "panowa&#263;"
  ]
  node [
    id 65
    label "przejrza&#322;y"
  ]
  node [
    id 66
    label "nie&#347;wie&#380;y"
  ]
  node [
    id 67
    label "przeterminowany"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
]
