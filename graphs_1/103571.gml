graph [
  maxDegree 12
  minDegree 1
  meanDegree 2
  density 0.05714285714285714
  graphCliqueNumber 2
  node [
    id 0
    label "tata"
    origin "text"
  ]
  node [
    id 1
    label "szepta&#263;"
    origin "text"
  ]
  node [
    id 2
    label "prosto"
    origin "text"
  ]
  node [
    id 3
    label "ucha"
    origin "text"
  ]
  node [
    id 4
    label "wstawa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "sp&#243;&#378;ni&#263;"
    origin "text"
  ]
  node [
    id 6
    label "stary"
  ]
  node [
    id 7
    label "papa"
  ]
  node [
    id 8
    label "ojciec"
  ]
  node [
    id 9
    label "kuwada"
  ]
  node [
    id 10
    label "ojczym"
  ]
  node [
    id 11
    label "przodek"
  ]
  node [
    id 12
    label "rodzice"
  ]
  node [
    id 13
    label "rodzic"
  ]
  node [
    id 14
    label "breathe"
  ]
  node [
    id 15
    label "plotkowa&#263;"
  ]
  node [
    id 16
    label "m&#243;wi&#263;"
  ]
  node [
    id 17
    label "elementarily"
  ]
  node [
    id 18
    label "bezpo&#347;rednio"
  ]
  node [
    id 19
    label "naturalnie"
  ]
  node [
    id 20
    label "skromnie"
  ]
  node [
    id 21
    label "prosty"
  ]
  node [
    id 22
    label "&#322;atwo"
  ]
  node [
    id 23
    label "niepozornie"
  ]
  node [
    id 24
    label "zupa_rybna"
  ]
  node [
    id 25
    label "rybny"
  ]
  node [
    id 26
    label "opuszcza&#263;"
  ]
  node [
    id 27
    label "rise"
  ]
  node [
    id 28
    label "stawa&#263;"
  ]
  node [
    id 29
    label "arise"
  ]
  node [
    id 30
    label "przestawa&#263;"
  ]
  node [
    id 31
    label "podnosi&#263;_si&#281;"
  ]
  node [
    id 32
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 33
    label "heighten"
  ]
  node [
    id 34
    label "wschodzi&#263;"
  ]
  node [
    id 35
    label "zdrowie&#263;"
  ]
  edge [
    source 0
    target 0
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
]
