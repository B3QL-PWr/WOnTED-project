graph [
  maxDegree 4
  minDegree 1
  meanDegree 1.4285714285714286
  density 0.23809523809523808
  graphCliqueNumber 2
  node [
    id 0
    label "christelle"
    origin "text"
  ]
  node [
    id 1
    label "gros"
    origin "text"
  ]
  node [
    id 2
    label "majority"
  ]
  node [
    id 3
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 4
    label "Christelle"
  ]
  node [
    id 5
    label "mistrzostwo"
  ]
  node [
    id 6
    label "&#347;wiat"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 5
    target 6
  ]
]
