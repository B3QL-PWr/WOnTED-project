graph [
  maxDegree 5
  minDegree 1
  meanDegree 1.6
  density 0.17777777777777778
  graphCliqueNumber 2
  node [
    id 0
    label "nazywa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "roman"
    origin "text"
  ]
  node [
    id 3
    label "maximov"
    origin "text"
  ]
  node [
    id 4
    label "give"
  ]
  node [
    id 5
    label "mieni&#263;"
  ]
  node [
    id 6
    label "okre&#347;la&#263;"
  ]
  node [
    id 7
    label "nadawa&#263;"
  ]
  node [
    id 8
    label "Roman"
  ]
  node [
    id 9
    label "Maximov"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 8
    target 9
  ]
]
