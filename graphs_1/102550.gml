graph [
  maxDegree 20
  minDegree 1
  meanDegree 1.9545454545454546
  density 0.045454545454545456
  graphCliqueNumber 2
  node [
    id 0
    label "po&#380;&#261;dany"
    origin "text"
  ]
  node [
    id 1
    label "umiej&#281;tno&#347;&#263;"
    origin "text"
  ]
  node [
    id 2
    label "zesp&#243;&#322;"
    origin "text"
  ]
  node [
    id 3
    label "realizowa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "okre&#347;lenie"
    origin "text"
  ]
  node [
    id 5
    label "wymagania"
    origin "text"
  ]
  node [
    id 6
    label "dobry"
  ]
  node [
    id 7
    label "zdolno&#347;&#263;"
  ]
  node [
    id 8
    label "cecha"
  ]
  node [
    id 9
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 10
    label "whole"
  ]
  node [
    id 11
    label "odm&#322;adza&#263;"
  ]
  node [
    id 12
    label "zabudowania"
  ]
  node [
    id 13
    label "odm&#322;odzenie"
  ]
  node [
    id 14
    label "zespolik"
  ]
  node [
    id 15
    label "skupienie"
  ]
  node [
    id 16
    label "schorzenie"
  ]
  node [
    id 17
    label "grupa"
  ]
  node [
    id 18
    label "Depeche_Mode"
  ]
  node [
    id 19
    label "Mazowsze"
  ]
  node [
    id 20
    label "ro&#347;lina"
  ]
  node [
    id 21
    label "zbi&#243;r"
  ]
  node [
    id 22
    label "The_Beatles"
  ]
  node [
    id 23
    label "group"
  ]
  node [
    id 24
    label "&#346;wietliki"
  ]
  node [
    id 25
    label "odm&#322;adzanie"
  ]
  node [
    id 26
    label "batch"
  ]
  node [
    id 27
    label "wykorzystywa&#263;"
  ]
  node [
    id 28
    label "przeprowadza&#263;"
  ]
  node [
    id 29
    label "spieni&#281;&#380;a&#263;"
  ]
  node [
    id 30
    label "prosecute"
  ]
  node [
    id 31
    label "create"
  ]
  node [
    id 32
    label "prawdzi&#263;"
  ]
  node [
    id 33
    label "tworzy&#263;"
  ]
  node [
    id 34
    label "zdecydowanie"
  ]
  node [
    id 35
    label "follow-up"
  ]
  node [
    id 36
    label "appointment"
  ]
  node [
    id 37
    label "ustalenie"
  ]
  node [
    id 38
    label "localization"
  ]
  node [
    id 39
    label "denomination"
  ]
  node [
    id 40
    label "wyra&#380;enie"
  ]
  node [
    id 41
    label "ozdobnik"
  ]
  node [
    id 42
    label "przewidzenie"
  ]
  node [
    id 43
    label "term"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
]
