graph [
  maxDegree 27
  minDegree 1
  meanDegree 1.9591836734693877
  density 0.04081632653061224
  graphCliqueNumber 2
  node [
    id 0
    label "polski"
    origin "text"
  ]
  node [
    id 1
    label "droga"
    origin "text"
  ]
  node [
    id 2
    label "lacki"
  ]
  node [
    id 3
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 4
    label "przedmiot"
  ]
  node [
    id 5
    label "sztajer"
  ]
  node [
    id 6
    label "drabant"
  ]
  node [
    id 7
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 8
    label "polak"
  ]
  node [
    id 9
    label "pierogi_ruskie"
  ]
  node [
    id 10
    label "krakowiak"
  ]
  node [
    id 11
    label "Polish"
  ]
  node [
    id 12
    label "j&#281;zyk"
  ]
  node [
    id 13
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 14
    label "oberek"
  ]
  node [
    id 15
    label "po_polsku"
  ]
  node [
    id 16
    label "mazur"
  ]
  node [
    id 17
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 18
    label "chodzony"
  ]
  node [
    id 19
    label "skoczny"
  ]
  node [
    id 20
    label "ryba_po_grecku"
  ]
  node [
    id 21
    label "goniony"
  ]
  node [
    id 22
    label "polsko"
  ]
  node [
    id 23
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 24
    label "journey"
  ]
  node [
    id 25
    label "podbieg"
  ]
  node [
    id 26
    label "bezsilnikowy"
  ]
  node [
    id 27
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 28
    label "wylot"
  ]
  node [
    id 29
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 30
    label "drogowskaz"
  ]
  node [
    id 31
    label "nawierzchnia"
  ]
  node [
    id 32
    label "turystyka"
  ]
  node [
    id 33
    label "budowla"
  ]
  node [
    id 34
    label "spos&#243;b"
  ]
  node [
    id 35
    label "passage"
  ]
  node [
    id 36
    label "marszrutyzacja"
  ]
  node [
    id 37
    label "zbior&#243;wka"
  ]
  node [
    id 38
    label "ekskursja"
  ]
  node [
    id 39
    label "rajza"
  ]
  node [
    id 40
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 41
    label "ruch"
  ]
  node [
    id 42
    label "trasa"
  ]
  node [
    id 43
    label "wyb&#243;j"
  ]
  node [
    id 44
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 45
    label "ekwipunek"
  ]
  node [
    id 46
    label "korona_drogi"
  ]
  node [
    id 47
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 48
    label "pobocze"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
]
