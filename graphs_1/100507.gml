graph [
  maxDegree 19
  minDegree 1
  meanDegree 1.9393939393939394
  density 0.06060606060606061
  graphCliqueNumber 2
  node [
    id 0
    label "wie&#380;a"
    origin "text"
  ]
  node [
    id 1
    label "szybowy"
    origin "text"
  ]
  node [
    id 2
    label "g&#243;rnictwo"
    origin "text"
  ]
  node [
    id 3
    label "zestaw"
  ]
  node [
    id 4
    label "urz&#261;dzenie_RTV"
  ]
  node [
    id 5
    label "odtwarzacz"
  ]
  node [
    id 6
    label "hejnalica"
  ]
  node [
    id 7
    label "budynek"
  ]
  node [
    id 8
    label "strzelec"
  ]
  node [
    id 9
    label "tuner"
  ]
  node [
    id 10
    label "sprz&#281;t_audio"
  ]
  node [
    id 11
    label "korektor"
  ]
  node [
    id 12
    label "wzmacniacz"
  ]
  node [
    id 13
    label "g&#243;rnik_do&#322;owy"
  ]
  node [
    id 14
    label "szklany"
  ]
  node [
    id 15
    label "przemys&#322;_ci&#281;&#380;ki"
  ]
  node [
    id 16
    label "wydobycie"
  ]
  node [
    id 17
    label "wiertnictwo"
  ]
  node [
    id 18
    label "wydobywanie"
  ]
  node [
    id 19
    label "krzeska"
  ]
  node [
    id 20
    label "solnictwo"
  ]
  node [
    id 21
    label "zgarniacz"
  ]
  node [
    id 22
    label "obrywak"
  ]
  node [
    id 23
    label "aerologia_g&#243;rnicza"
  ]
  node [
    id 24
    label "przesyp"
  ]
  node [
    id 25
    label "wydobywa&#263;"
  ]
  node [
    id 26
    label "rozpierak"
  ]
  node [
    id 27
    label "&#322;adownik"
  ]
  node [
    id 28
    label "wydoby&#263;"
  ]
  node [
    id 29
    label "odstrzeliwa&#263;"
  ]
  node [
    id 30
    label "odstrzeliwanie"
  ]
  node [
    id 31
    label "wcinka"
  ]
  node [
    id 32
    label "nauka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
]
