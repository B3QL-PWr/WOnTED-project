graph [
  maxDegree 11
  minDegree 1
  meanDegree 1.9487179487179487
  density 0.05128205128205128
  graphCliqueNumber 2
  node [
    id 0
    label "minione"
    origin "text"
  ]
  node [
    id 1
    label "tydzie&#324;"
    origin "text"
  ]
  node [
    id 2
    label "prezes"
    origin "text"
  ]
  node [
    id 3
    label "tauronu"
    origin "text"
  ]
  node [
    id 4
    label "pge"
    origin "text"
  ]
  node [
    id 5
    label "energi"
    origin "text"
  ]
  node [
    id 6
    label "enei"
    origin "text"
  ]
  node [
    id 7
    label "spotka&#263;"
    origin "text"
  ]
  node [
    id 8
    label "si&#281;"
    origin "text"
  ]
  node [
    id 9
    label "premier"
    origin "text"
  ]
  node [
    id 10
    label "mateusz"
    origin "text"
  ]
  node [
    id 11
    label "morawieckim"
    origin "text"
  ]
  node [
    id 12
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 13
    label "doba"
  ]
  node [
    id 14
    label "czas"
  ]
  node [
    id 15
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 16
    label "weekend"
  ]
  node [
    id 17
    label "miesi&#261;c"
  ]
  node [
    id 18
    label "gruba_ryba"
  ]
  node [
    id 19
    label "zwierzchnik"
  ]
  node [
    id 20
    label "befall"
  ]
  node [
    id 21
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 22
    label "pozna&#263;"
  ]
  node [
    id 23
    label "spowodowa&#263;"
  ]
  node [
    id 24
    label "go_steady"
  ]
  node [
    id 25
    label "insert"
  ]
  node [
    id 26
    label "znale&#378;&#263;"
  ]
  node [
    id 27
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 28
    label "visualize"
  ]
  node [
    id 29
    label "Jelcyn"
  ]
  node [
    id 30
    label "Sto&#322;ypin"
  ]
  node [
    id 31
    label "dostojnik"
  ]
  node [
    id 32
    label "Chruszczow"
  ]
  node [
    id 33
    label "Miko&#322;ajczyk"
  ]
  node [
    id 34
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 35
    label "Bismarck"
  ]
  node [
    id 36
    label "rz&#261;d"
  ]
  node [
    id 37
    label "Mateusz"
  ]
  node [
    id 38
    label "Morawieckim"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 20
  ]
  edge [
    source 7
    target 21
  ]
  edge [
    source 7
    target 22
  ]
  edge [
    source 7
    target 23
  ]
  edge [
    source 7
    target 24
  ]
  edge [
    source 7
    target 25
  ]
  edge [
    source 7
    target 26
  ]
  edge [
    source 7
    target 27
  ]
  edge [
    source 7
    target 28
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 29
  ]
  edge [
    source 9
    target 30
  ]
  edge [
    source 9
    target 31
  ]
  edge [
    source 9
    target 32
  ]
  edge [
    source 9
    target 33
  ]
  edge [
    source 9
    target 34
  ]
  edge [
    source 9
    target 35
  ]
  edge [
    source 9
    target 36
  ]
  edge [
    source 9
    target 19
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 37
    target 38
  ]
]
