graph [
  maxDegree 13
  minDegree 1
  meanDegree 1.935483870967742
  density 0.06451612903225806
  graphCliqueNumber 2
  node [
    id 0
    label "jak"
    origin "text"
  ]
  node [
    id 1
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "koszt"
    origin "text"
  ]
  node [
    id 4
    label "taki"
    origin "text"
  ]
  node [
    id 5
    label "tatua&#380;"
    origin "text"
  ]
  node [
    id 6
    label "byd&#322;o"
  ]
  node [
    id 7
    label "zobo"
  ]
  node [
    id 8
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 9
    label "yakalo"
  ]
  node [
    id 10
    label "dzo"
  ]
  node [
    id 11
    label "si&#281;ga&#263;"
  ]
  node [
    id 12
    label "trwa&#263;"
  ]
  node [
    id 13
    label "obecno&#347;&#263;"
  ]
  node [
    id 14
    label "stan"
  ]
  node [
    id 15
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 16
    label "stand"
  ]
  node [
    id 17
    label "mie&#263;_miejsce"
  ]
  node [
    id 18
    label "uczestniczy&#263;"
  ]
  node [
    id 19
    label "chodzi&#263;"
  ]
  node [
    id 20
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 21
    label "equal"
  ]
  node [
    id 22
    label "nak&#322;ad"
  ]
  node [
    id 23
    label "rachunek_koszt&#243;w"
  ]
  node [
    id 24
    label "sumpt"
  ]
  node [
    id 25
    label "wydatek"
  ]
  node [
    id 26
    label "okre&#347;lony"
  ]
  node [
    id 27
    label "jaki&#347;"
  ]
  node [
    id 28
    label "tattoo"
  ]
  node [
    id 29
    label "ozdoba"
  ]
  node [
    id 30
    label "technika"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
]
