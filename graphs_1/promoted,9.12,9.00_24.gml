graph [
  maxDegree 12
  minDegree 1
  meanDegree 1.9574468085106382
  density 0.0425531914893617
  graphCliqueNumber 2
  node [
    id 0
    label "brytyjski"
    origin "text"
  ]
  node [
    id 1
    label "parlament"
    origin "text"
  ]
  node [
    id 2
    label "upubliczni&#263;"
    origin "text"
  ]
  node [
    id 3
    label "tre&#347;&#263;"
    origin "text"
  ]
  node [
    id 4
    label "wewn&#281;trzny"
    origin "text"
  ]
  node [
    id 5
    label "komunikacja"
    origin "text"
  ]
  node [
    id 6
    label "facebook"
    origin "text"
  ]
  node [
    id 7
    label "angielsko"
  ]
  node [
    id 8
    label "po_brytyjsku"
  ]
  node [
    id 9
    label "europejski"
  ]
  node [
    id 10
    label "j&#281;zyk_celtycki"
  ]
  node [
    id 11
    label "j&#281;zyk_martwy"
  ]
  node [
    id 12
    label "j&#281;zyk_angielski"
  ]
  node [
    id 13
    label "zachodnioeuropejski"
  ]
  node [
    id 14
    label "morris"
  ]
  node [
    id 15
    label "angielski"
  ]
  node [
    id 16
    label "anglosaski"
  ]
  node [
    id 17
    label "brytyjsko"
  ]
  node [
    id 18
    label "grupa"
  ]
  node [
    id 19
    label "plankton_polityczny"
  ]
  node [
    id 20
    label "ustawodawca"
  ]
  node [
    id 21
    label "urz&#261;d"
  ]
  node [
    id 22
    label "europarlament"
  ]
  node [
    id 23
    label "grupa_bilateralna"
  ]
  node [
    id 24
    label "udost&#281;pni&#263;"
  ]
  node [
    id 25
    label "zawarto&#347;&#263;"
  ]
  node [
    id 26
    label "temat"
  ]
  node [
    id 27
    label "istota"
  ]
  node [
    id 28
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 29
    label "informacja"
  ]
  node [
    id 30
    label "wewn&#281;trznie"
  ]
  node [
    id 31
    label "wn&#281;trzny"
  ]
  node [
    id 32
    label "psychiczny"
  ]
  node [
    id 33
    label "numer"
  ]
  node [
    id 34
    label "oddzia&#322;"
  ]
  node [
    id 35
    label "wydeptanie"
  ]
  node [
    id 36
    label "wydeptywanie"
  ]
  node [
    id 37
    label "miejsce"
  ]
  node [
    id 38
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 39
    label "implicite"
  ]
  node [
    id 40
    label "skomunikowa&#263;_si&#281;"
  ]
  node [
    id 41
    label "transportation_system"
  ]
  node [
    id 42
    label "skomunikowanie_si&#281;"
  ]
  node [
    id 43
    label "explicite"
  ]
  node [
    id 44
    label "ekspedytor"
  ]
  node [
    id 45
    label "wall"
  ]
  node [
    id 46
    label "konto"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
]
