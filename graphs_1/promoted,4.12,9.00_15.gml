graph [
  maxDegree 19
  minDegree 1
  meanDegree 1.9
  density 0.1
  graphCliqueNumber 2
  node [
    id 0
    label "sta&#263;"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "pozostawa&#263;"
  ]
  node [
    id 3
    label "trwa&#263;"
  ]
  node [
    id 4
    label "by&#263;"
  ]
  node [
    id 5
    label "wystarcza&#263;"
  ]
  node [
    id 6
    label "obowi&#261;zywa&#263;"
  ]
  node [
    id 7
    label "czeka&#263;"
  ]
  node [
    id 8
    label "stand"
  ]
  node [
    id 9
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 10
    label "mieszka&#263;"
  ]
  node [
    id 11
    label "wystarczy&#263;"
  ]
  node [
    id 12
    label "sprawowa&#263;"
  ]
  node [
    id 13
    label "przebywa&#263;"
  ]
  node [
    id 14
    label "kosztowa&#263;"
  ]
  node [
    id 15
    label "undertaking"
  ]
  node [
    id 16
    label "wystawa&#263;"
  ]
  node [
    id 17
    label "base"
  ]
  node [
    id 18
    label "digest"
  ]
  node [
    id 19
    label "trzyma&#263;_si&#281;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
]
