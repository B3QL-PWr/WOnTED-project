graph [
  maxDegree 32
  minDegree 1
  meanDegree 9.051282051282051
  density 0.11754911754911755
  graphCliqueNumber 22
  node [
    id 0
    label "dziennik"
    origin "text"
  ]
  node [
    id 1
    label "ustawa"
    origin "text"
  ]
  node [
    id 2
    label "czerwiec"
    origin "text"
  ]
  node [
    id 3
    label "poz"
    origin "text"
  ]
  node [
    id 4
    label "spis"
  ]
  node [
    id 5
    label "sheet"
  ]
  node [
    id 6
    label "gazeta"
  ]
  node [
    id 7
    label "diariusz"
  ]
  node [
    id 8
    label "pami&#281;tnik"
  ]
  node [
    id 9
    label "journal"
  ]
  node [
    id 10
    label "ksi&#281;ga"
  ]
  node [
    id 11
    label "program_informacyjny"
  ]
  node [
    id 12
    label "Karta_Nauczyciela"
  ]
  node [
    id 13
    label "marc&#243;wka"
  ]
  node [
    id 14
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 15
    label "akt"
  ]
  node [
    id 16
    label "przej&#347;&#263;"
  ]
  node [
    id 17
    label "charter"
  ]
  node [
    id 18
    label "przej&#347;cie"
  ]
  node [
    id 19
    label "ro&#347;lina_zielna"
  ]
  node [
    id 20
    label "go&#378;dzikowate"
  ]
  node [
    id 21
    label "miesi&#261;c"
  ]
  node [
    id 22
    label "Bo&#380;e_Cia&#322;o"
  ]
  node [
    id 23
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 24
    label "ustawi&#263;"
  ]
  node [
    id 25
    label "trybuna&#322;"
  ]
  node [
    id 26
    label "konstytucyjny"
  ]
  node [
    id 27
    label "Marian"
  ]
  node [
    id 28
    label "grzybowski"
  ]
  node [
    id 29
    label "Jerzy"
  ]
  node [
    id 30
    label "St&#281;pie&#324;"
  ]
  node [
    id 31
    label "Gra&#380;yna"
  ]
  node [
    id 32
    label "Sza&#322;ygo"
  ]
  node [
    id 33
    label "marka"
  ]
  node [
    id 34
    label "Mazurkiewicz"
  ]
  node [
    id 35
    label "Jadwiga"
  ]
  node [
    id 36
    label "Sk&#243;rzewska"
  ]
  node [
    id 37
    label "&#321;osiak"
  ]
  node [
    id 38
    label "Janusz"
  ]
  node [
    id 39
    label "Niemcewicz"
  ]
  node [
    id 40
    label "prokurator"
  ]
  node [
    id 41
    label "generalny"
  ]
  node [
    id 42
    label "minister"
  ]
  node [
    id 43
    label "zdrowie"
  ]
  node [
    id 44
    label "Zbys&#322;awa"
  ]
  node [
    id 45
    label "kleczewski"
  ]
  node [
    id 46
    label "zeszyt"
  ]
  node [
    id 47
    label "dzie&#324;"
  ]
  node [
    id 48
    label "26"
  ]
  node [
    id 49
    label "pa&#378;dziernik"
  ]
  node [
    id 50
    label "1982"
  ]
  node [
    id 51
    label "rok"
  ]
  node [
    id 52
    label "ojciec"
  ]
  node [
    id 53
    label "wychowa&#263;"
  ]
  node [
    id 54
    label "wyspa"
  ]
  node [
    id 55
    label "trze&#378;wo&#347;&#263;"
  ]
  node [
    id 56
    label "i"
  ]
  node [
    id 57
    label "przeciwdzia&#322;a&#263;"
  ]
  node [
    id 58
    label "alkoholizm"
  ]
  node [
    id 59
    label "u"
  ]
  node [
    id 60
    label "rozporz&#261;dzi&#263;"
  ]
  node [
    id 61
    label "opieka"
  ]
  node [
    id 62
    label "spo&#322;eczny"
  ]
  node [
    id 63
    label "6"
  ]
  node [
    id 64
    label "maja"
  ]
  node [
    id 65
    label "1983"
  ]
  node [
    id 66
    label "sprawa"
  ]
  node [
    id 67
    label "warunki"
  ]
  node [
    id 68
    label "spos&#243;b"
  ]
  node [
    id 69
    label "dokonywa&#263;"
  ]
  node [
    id 70
    label "badanie"
  ]
  node [
    id 71
    label "na"
  ]
  node [
    id 72
    label "zawarto&#347;&#263;"
  ]
  node [
    id 73
    label "alkohol"
  ]
  node [
    id 74
    label "organizm"
  ]
  node [
    id 75
    label "konstytucja"
  ]
  node [
    id 76
    label "rzeczpospolita"
  ]
  node [
    id 77
    label "polski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 37
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 60
  ]
  edge [
    source 42
    target 56
  ]
  edge [
    source 42
    target 61
  ]
  edge [
    source 42
    target 62
  ]
  edge [
    source 42
    target 46
  ]
  edge [
    source 42
    target 47
  ]
  edge [
    source 42
    target 63
  ]
  edge [
    source 42
    target 64
  ]
  edge [
    source 42
    target 65
  ]
  edge [
    source 42
    target 51
  ]
  edge [
    source 42
    target 54
  ]
  edge [
    source 42
    target 66
  ]
  edge [
    source 42
    target 67
  ]
  edge [
    source 42
    target 68
  ]
  edge [
    source 42
    target 69
  ]
  edge [
    source 42
    target 70
  ]
  edge [
    source 42
    target 71
  ]
  edge [
    source 42
    target 72
  ]
  edge [
    source 42
    target 73
  ]
  edge [
    source 42
    target 74
  ]
  edge [
    source 43
    target 60
  ]
  edge [
    source 43
    target 56
  ]
  edge [
    source 43
    target 61
  ]
  edge [
    source 43
    target 62
  ]
  edge [
    source 43
    target 46
  ]
  edge [
    source 43
    target 47
  ]
  edge [
    source 43
    target 63
  ]
  edge [
    source 43
    target 64
  ]
  edge [
    source 43
    target 65
  ]
  edge [
    source 43
    target 51
  ]
  edge [
    source 43
    target 54
  ]
  edge [
    source 43
    target 66
  ]
  edge [
    source 43
    target 67
  ]
  edge [
    source 43
    target 68
  ]
  edge [
    source 43
    target 69
  ]
  edge [
    source 43
    target 70
  ]
  edge [
    source 43
    target 71
  ]
  edge [
    source 43
    target 72
  ]
  edge [
    source 43
    target 73
  ]
  edge [
    source 43
    target 74
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 48
  ]
  edge [
    source 46
    target 49
  ]
  edge [
    source 46
    target 50
  ]
  edge [
    source 46
    target 51
  ]
  edge [
    source 46
    target 52
  ]
  edge [
    source 46
    target 53
  ]
  edge [
    source 46
    target 54
  ]
  edge [
    source 46
    target 55
  ]
  edge [
    source 46
    target 56
  ]
  edge [
    source 46
    target 57
  ]
  edge [
    source 46
    target 58
  ]
  edge [
    source 46
    target 60
  ]
  edge [
    source 46
    target 61
  ]
  edge [
    source 46
    target 62
  ]
  edge [
    source 46
    target 63
  ]
  edge [
    source 46
    target 64
  ]
  edge [
    source 46
    target 65
  ]
  edge [
    source 46
    target 66
  ]
  edge [
    source 46
    target 67
  ]
  edge [
    source 46
    target 68
  ]
  edge [
    source 46
    target 69
  ]
  edge [
    source 46
    target 70
  ]
  edge [
    source 46
    target 71
  ]
  edge [
    source 46
    target 72
  ]
  edge [
    source 46
    target 73
  ]
  edge [
    source 46
    target 74
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 49
  ]
  edge [
    source 47
    target 50
  ]
  edge [
    source 47
    target 51
  ]
  edge [
    source 47
    target 52
  ]
  edge [
    source 47
    target 53
  ]
  edge [
    source 47
    target 54
  ]
  edge [
    source 47
    target 55
  ]
  edge [
    source 47
    target 56
  ]
  edge [
    source 47
    target 57
  ]
  edge [
    source 47
    target 58
  ]
  edge [
    source 47
    target 60
  ]
  edge [
    source 47
    target 61
  ]
  edge [
    source 47
    target 62
  ]
  edge [
    source 47
    target 63
  ]
  edge [
    source 47
    target 64
  ]
  edge [
    source 47
    target 65
  ]
  edge [
    source 47
    target 66
  ]
  edge [
    source 47
    target 67
  ]
  edge [
    source 47
    target 68
  ]
  edge [
    source 47
    target 69
  ]
  edge [
    source 47
    target 70
  ]
  edge [
    source 47
    target 71
  ]
  edge [
    source 47
    target 72
  ]
  edge [
    source 47
    target 73
  ]
  edge [
    source 47
    target 74
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 50
  ]
  edge [
    source 48
    target 51
  ]
  edge [
    source 48
    target 52
  ]
  edge [
    source 48
    target 53
  ]
  edge [
    source 48
    target 54
  ]
  edge [
    source 48
    target 55
  ]
  edge [
    source 48
    target 56
  ]
  edge [
    source 48
    target 57
  ]
  edge [
    source 48
    target 58
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 51
  ]
  edge [
    source 49
    target 52
  ]
  edge [
    source 49
    target 53
  ]
  edge [
    source 49
    target 54
  ]
  edge [
    source 49
    target 55
  ]
  edge [
    source 49
    target 56
  ]
  edge [
    source 49
    target 57
  ]
  edge [
    source 49
    target 58
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 52
  ]
  edge [
    source 50
    target 53
  ]
  edge [
    source 50
    target 54
  ]
  edge [
    source 50
    target 55
  ]
  edge [
    source 50
    target 56
  ]
  edge [
    source 50
    target 57
  ]
  edge [
    source 50
    target 58
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 53
  ]
  edge [
    source 51
    target 54
  ]
  edge [
    source 51
    target 55
  ]
  edge [
    source 51
    target 56
  ]
  edge [
    source 51
    target 57
  ]
  edge [
    source 51
    target 58
  ]
  edge [
    source 51
    target 60
  ]
  edge [
    source 51
    target 61
  ]
  edge [
    source 51
    target 62
  ]
  edge [
    source 51
    target 63
  ]
  edge [
    source 51
    target 64
  ]
  edge [
    source 51
    target 65
  ]
  edge [
    source 51
    target 66
  ]
  edge [
    source 51
    target 67
  ]
  edge [
    source 51
    target 68
  ]
  edge [
    source 51
    target 69
  ]
  edge [
    source 51
    target 70
  ]
  edge [
    source 51
    target 71
  ]
  edge [
    source 51
    target 72
  ]
  edge [
    source 51
    target 73
  ]
  edge [
    source 51
    target 74
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 54
  ]
  edge [
    source 52
    target 55
  ]
  edge [
    source 52
    target 56
  ]
  edge [
    source 52
    target 57
  ]
  edge [
    source 52
    target 58
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 55
  ]
  edge [
    source 53
    target 56
  ]
  edge [
    source 53
    target 57
  ]
  edge [
    source 53
    target 58
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 56
  ]
  edge [
    source 54
    target 57
  ]
  edge [
    source 54
    target 58
  ]
  edge [
    source 54
    target 60
  ]
  edge [
    source 54
    target 61
  ]
  edge [
    source 54
    target 62
  ]
  edge [
    source 54
    target 63
  ]
  edge [
    source 54
    target 64
  ]
  edge [
    source 54
    target 65
  ]
  edge [
    source 54
    target 66
  ]
  edge [
    source 54
    target 67
  ]
  edge [
    source 54
    target 68
  ]
  edge [
    source 54
    target 69
  ]
  edge [
    source 54
    target 70
  ]
  edge [
    source 54
    target 71
  ]
  edge [
    source 54
    target 72
  ]
  edge [
    source 54
    target 73
  ]
  edge [
    source 54
    target 54
  ]
  edge [
    source 54
    target 74
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 57
  ]
  edge [
    source 55
    target 58
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 58
  ]
  edge [
    source 56
    target 60
  ]
  edge [
    source 56
    target 61
  ]
  edge [
    source 56
    target 62
  ]
  edge [
    source 56
    target 63
  ]
  edge [
    source 56
    target 64
  ]
  edge [
    source 56
    target 65
  ]
  edge [
    source 56
    target 66
  ]
  edge [
    source 56
    target 67
  ]
  edge [
    source 56
    target 56
  ]
  edge [
    source 56
    target 68
  ]
  edge [
    source 56
    target 69
  ]
  edge [
    source 56
    target 70
  ]
  edge [
    source 56
    target 71
  ]
  edge [
    source 56
    target 72
  ]
  edge [
    source 56
    target 73
  ]
  edge [
    source 56
    target 74
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 62
  ]
  edge [
    source 60
    target 63
  ]
  edge [
    source 60
    target 64
  ]
  edge [
    source 60
    target 65
  ]
  edge [
    source 60
    target 66
  ]
  edge [
    source 60
    target 67
  ]
  edge [
    source 60
    target 68
  ]
  edge [
    source 60
    target 69
  ]
  edge [
    source 60
    target 70
  ]
  edge [
    source 60
    target 71
  ]
  edge [
    source 60
    target 72
  ]
  edge [
    source 60
    target 73
  ]
  edge [
    source 60
    target 74
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 63
  ]
  edge [
    source 61
    target 64
  ]
  edge [
    source 61
    target 65
  ]
  edge [
    source 61
    target 66
  ]
  edge [
    source 61
    target 67
  ]
  edge [
    source 61
    target 68
  ]
  edge [
    source 61
    target 69
  ]
  edge [
    source 61
    target 70
  ]
  edge [
    source 61
    target 71
  ]
  edge [
    source 61
    target 72
  ]
  edge [
    source 61
    target 73
  ]
  edge [
    source 61
    target 74
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 64
  ]
  edge [
    source 62
    target 65
  ]
  edge [
    source 62
    target 66
  ]
  edge [
    source 62
    target 67
  ]
  edge [
    source 62
    target 68
  ]
  edge [
    source 62
    target 69
  ]
  edge [
    source 62
    target 70
  ]
  edge [
    source 62
    target 71
  ]
  edge [
    source 62
    target 72
  ]
  edge [
    source 62
    target 73
  ]
  edge [
    source 62
    target 74
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 65
  ]
  edge [
    source 63
    target 66
  ]
  edge [
    source 63
    target 67
  ]
  edge [
    source 63
    target 68
  ]
  edge [
    source 63
    target 69
  ]
  edge [
    source 63
    target 70
  ]
  edge [
    source 63
    target 71
  ]
  edge [
    source 63
    target 72
  ]
  edge [
    source 63
    target 73
  ]
  edge [
    source 63
    target 74
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 66
  ]
  edge [
    source 64
    target 67
  ]
  edge [
    source 64
    target 68
  ]
  edge [
    source 64
    target 69
  ]
  edge [
    source 64
    target 70
  ]
  edge [
    source 64
    target 71
  ]
  edge [
    source 64
    target 72
  ]
  edge [
    source 64
    target 73
  ]
  edge [
    source 64
    target 74
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 67
  ]
  edge [
    source 65
    target 68
  ]
  edge [
    source 65
    target 69
  ]
  edge [
    source 65
    target 70
  ]
  edge [
    source 65
    target 71
  ]
  edge [
    source 65
    target 72
  ]
  edge [
    source 65
    target 73
  ]
  edge [
    source 65
    target 74
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 66
    target 68
  ]
  edge [
    source 66
    target 69
  ]
  edge [
    source 66
    target 70
  ]
  edge [
    source 66
    target 71
  ]
  edge [
    source 66
    target 72
  ]
  edge [
    source 66
    target 73
  ]
  edge [
    source 66
    target 74
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 67
    target 69
  ]
  edge [
    source 67
    target 70
  ]
  edge [
    source 67
    target 71
  ]
  edge [
    source 67
    target 72
  ]
  edge [
    source 67
    target 73
  ]
  edge [
    source 67
    target 74
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 68
    target 70
  ]
  edge [
    source 68
    target 71
  ]
  edge [
    source 68
    target 72
  ]
  edge [
    source 68
    target 73
  ]
  edge [
    source 68
    target 74
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 69
    target 71
  ]
  edge [
    source 69
    target 72
  ]
  edge [
    source 69
    target 73
  ]
  edge [
    source 69
    target 74
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 72
  ]
  edge [
    source 70
    target 73
  ]
  edge [
    source 70
    target 74
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 71
    target 73
  ]
  edge [
    source 71
    target 74
  ]
  edge [
    source 72
    target 73
  ]
  edge [
    source 72
    target 74
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 75
    target 76
  ]
  edge [
    source 75
    target 77
  ]
  edge [
    source 76
    target 77
  ]
]
