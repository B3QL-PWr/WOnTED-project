graph [
  maxDegree 13
  minDegree 1
  meanDegree 1.9523809523809523
  density 0.047619047619047616
  graphCliqueNumber 2
  node [
    id 0
    label "swietny"
    origin "text"
  ]
  node [
    id 1
    label "wywiad"
    origin "text"
  ]
  node [
    id 2
    label "petersonem"
    origin "text"
  ]
  node [
    id 3
    label "matecznik"
    origin "text"
  ]
  node [
    id 4
    label "poprawnosci"
    origin "text"
  ]
  node [
    id 5
    label "polityczny"
    origin "text"
  ]
  node [
    id 6
    label "przymusowy"
    origin "text"
  ]
  node [
    id 7
    label "r&#243;wnouprawnienie"
    origin "text"
  ]
  node [
    id 8
    label "diagnostyka"
  ]
  node [
    id 9
    label "rozmowa"
  ]
  node [
    id 10
    label "sonda&#380;"
  ]
  node [
    id 11
    label "autoryzowanie"
  ]
  node [
    id 12
    label "autoryzowa&#263;"
  ]
  node [
    id 13
    label "s&#322;u&#380;by_specjalne"
  ]
  node [
    id 14
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 15
    label "agent"
  ]
  node [
    id 16
    label "inquiry"
  ]
  node [
    id 17
    label "consultation"
  ]
  node [
    id 18
    label "s&#322;u&#380;ba"
  ]
  node [
    id 19
    label "obszar"
  ]
  node [
    id 20
    label "ro&#347;lina"
  ]
  node [
    id 21
    label "melisa"
  ]
  node [
    id 22
    label "puszcza"
  ]
  node [
    id 23
    label "kom&#243;rka"
  ]
  node [
    id 24
    label "ro&#347;lina_miododajna"
  ]
  node [
    id 25
    label "dzicz"
  ]
  node [
    id 26
    label "o&#347;rodek"
  ]
  node [
    id 27
    label "legowisko"
  ]
  node [
    id 28
    label "&#322;ono"
  ]
  node [
    id 29
    label "internowanie"
  ]
  node [
    id 30
    label "prorz&#261;dowy"
  ]
  node [
    id 31
    label "wi&#281;zie&#324;"
  ]
  node [
    id 32
    label "politycznie"
  ]
  node [
    id 33
    label "internowa&#263;"
  ]
  node [
    id 34
    label "ideologiczny"
  ]
  node [
    id 35
    label "konieczny"
  ]
  node [
    id 36
    label "przymusowo"
  ]
  node [
    id 37
    label "poniewolny"
  ]
  node [
    id 38
    label "zr&#243;wnanie"
  ]
  node [
    id 39
    label "czynno&#347;&#263;"
  ]
  node [
    id 40
    label "r&#243;wno&#347;&#263;"
  ]
  node [
    id 41
    label "spowodowanie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 7
    target 38
  ]
  edge [
    source 7
    target 39
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 7
    target 41
  ]
]
