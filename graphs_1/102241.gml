graph [
  maxDegree 11
  minDegree 1
  meanDegree 3.45
  density 0.08846153846153847
  graphCliqueNumber 10
  node [
    id 0
    label "krzysztof"
    origin "text"
  ]
  node [
    id 1
    label "kolasi&#324;ski"
    origin "text"
  ]
  node [
    id 2
    label "sprawozdawca"
    origin "text"
  ]
  node [
    id 3
    label "&#347;wiadek"
  ]
  node [
    id 4
    label "przekaziciel"
  ]
  node [
    id 5
    label "dziennikarz"
  ]
  node [
    id 6
    label "Krzysztofa"
  ]
  node [
    id 7
    label "Kolasi&#324;ski"
  ]
  node [
    id 8
    label "Janusz"
  ]
  node [
    id 9
    label "Niemcewicz"
  ]
  node [
    id 10
    label "Jadwiga"
  ]
  node [
    id 11
    label "Sk&#243;rzewska"
  ]
  node [
    id 12
    label "&#321;osiak"
  ]
  node [
    id 13
    label "Jerzy"
  ]
  node [
    id 14
    label "St&#281;pie&#324;"
  ]
  node [
    id 15
    label "Gra&#380;yna"
  ]
  node [
    id 16
    label "Sza&#322;ygo"
  ]
  node [
    id 17
    label "rzecznik"
  ]
  node [
    id 18
    label "prawy"
  ]
  node [
    id 19
    label "obywatelski"
  ]
  node [
    id 20
    label "prokurator"
  ]
  node [
    id 21
    label "generalny"
  ]
  node [
    id 22
    label "rzeczpospolita"
  ]
  node [
    id 23
    label "polski"
  ]
  node [
    id 24
    label "ustawa"
  ]
  node [
    id 25
    label "zeszyt"
  ]
  node [
    id 26
    label "dzie&#324;"
  ]
  node [
    id 27
    label "28"
  ]
  node [
    id 28
    label "lipiec"
  ]
  node [
    id 29
    label "1990"
  ]
  node [
    id 30
    label "rok"
  ]
  node [
    id 31
    label "ojciec"
  ]
  node [
    id 32
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 33
    label "ubezpieczeniowy"
  ]
  node [
    id 34
    label "dziennik"
  ]
  node [
    id 35
    label "u"
  ]
  node [
    id 36
    label "fundusz"
  ]
  node [
    id 37
    label "gwarancyjny"
  ]
  node [
    id 38
    label "konstytucja"
  ]
  node [
    id 39
    label "RP"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 19
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 38
  ]
  edge [
    source 23
    target 38
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 26
  ]
  edge [
    source 24
    target 27
  ]
  edge [
    source 24
    target 28
  ]
  edge [
    source 24
    target 29
  ]
  edge [
    source 24
    target 30
  ]
  edge [
    source 24
    target 31
  ]
  edge [
    source 24
    target 32
  ]
  edge [
    source 24
    target 33
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 27
  ]
  edge [
    source 25
    target 28
  ]
  edge [
    source 25
    target 29
  ]
  edge [
    source 25
    target 30
  ]
  edge [
    source 25
    target 31
  ]
  edge [
    source 25
    target 32
  ]
  edge [
    source 25
    target 33
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 28
  ]
  edge [
    source 26
    target 29
  ]
  edge [
    source 26
    target 30
  ]
  edge [
    source 26
    target 31
  ]
  edge [
    source 26
    target 32
  ]
  edge [
    source 26
    target 33
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 29
  ]
  edge [
    source 27
    target 30
  ]
  edge [
    source 27
    target 31
  ]
  edge [
    source 27
    target 32
  ]
  edge [
    source 27
    target 33
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 30
  ]
  edge [
    source 28
    target 31
  ]
  edge [
    source 28
    target 32
  ]
  edge [
    source 28
    target 33
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 31
  ]
  edge [
    source 29
    target 32
  ]
  edge [
    source 29
    target 33
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 32
  ]
  edge [
    source 30
    target 33
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 33
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 33
    target 36
  ]
  edge [
    source 33
    target 37
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 38
    target 39
  ]
]
