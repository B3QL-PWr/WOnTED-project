graph [
  maxDegree 5
  minDegree 1
  meanDegree 2.0952380952380953
  density 0.10476190476190476
  graphCliqueNumber 5
  node [
    id 0
    label "kondukt"
    origin "text"
  ]
  node [
    id 1
    label "pogrzebowy"
    origin "text"
  ]
  node [
    id 2
    label "&#380;a&#322;obnik"
  ]
  node [
    id 3
    label "orszak"
  ]
  node [
    id 4
    label "karawaniarz"
  ]
  node [
    id 5
    label "kirowy"
  ]
  node [
    id 6
    label "pogrzebowo"
  ]
  node [
    id 7
    label "&#380;a&#322;obny"
  ]
  node [
    id 8
    label "typowy"
  ]
  node [
    id 9
    label "mi&#322;owa&#263;"
  ]
  node [
    id 10
    label "pan"
  ]
  node [
    id 11
    label "ja"
  ]
  node [
    id 12
    label "by&#263;"
  ]
  node [
    id 13
    label "zmartwychwsta&#263;"
  ]
  node [
    id 14
    label "i"
  ]
  node [
    id 15
    label "&#380;y&#263;"
  ]
  node [
    id 16
    label "modlitwa"
  ]
  node [
    id 17
    label "pa&#324;ski"
  ]
  node [
    id 18
    label "wita&#263;"
  ]
  node [
    id 19
    label "kr&#243;lowa"
  ]
  node [
    id 20
    label "anio&#322;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 13
  ]
  edge [
    source 11
    target 14
  ]
  edge [
    source 11
    target 15
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 12
    target 15
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 20
  ]
  edge [
    source 18
    target 19
  ]
]
