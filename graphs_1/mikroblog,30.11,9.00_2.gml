graph [
  maxDegree 13
  minDegree 1
  meanDegree 1.9642857142857142
  density 0.03571428571428571
  graphCliqueNumber 2
  node [
    id 0
    label "chcie&#263;"
    origin "text"
  ]
  node [
    id 1
    label "przepis"
    origin "text"
  ]
  node [
    id 2
    label "tan"
    origin "text"
  ]
  node [
    id 3
    label "smaczny"
    origin "text"
  ]
  node [
    id 4
    label "&#380;arcie"
    origin "text"
  ]
  node [
    id 5
    label "poni&#380;ej"
    origin "text"
  ]
  node [
    id 6
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 7
    label "porcja"
    origin "text"
  ]
  node [
    id 8
    label "czu&#263;"
  ]
  node [
    id 9
    label "desire"
  ]
  node [
    id 10
    label "kcie&#263;"
  ]
  node [
    id 11
    label "przedawnienie_si&#281;"
  ]
  node [
    id 12
    label "recepta"
  ]
  node [
    id 13
    label "norma_prawna"
  ]
  node [
    id 14
    label "kodeks"
  ]
  node [
    id 15
    label "prawo"
  ]
  node [
    id 16
    label "regulation"
  ]
  node [
    id 17
    label "przedawni&#263;_si&#281;"
  ]
  node [
    id 18
    label "porada"
  ]
  node [
    id 19
    label "przedawnianie_si&#281;"
  ]
  node [
    id 20
    label "spos&#243;b"
  ]
  node [
    id 21
    label "przedawnia&#263;_si&#281;"
  ]
  node [
    id 22
    label "czynno&#347;&#263;"
  ]
  node [
    id 23
    label "parkiet"
  ]
  node [
    id 24
    label "ruch"
  ]
  node [
    id 25
    label "krok_taneczny"
  ]
  node [
    id 26
    label "posuwisto&#347;&#263;"
  ]
  node [
    id 27
    label "zbi&#243;r"
  ]
  node [
    id 28
    label "karnet"
  ]
  node [
    id 29
    label "krzepi&#261;cy"
  ]
  node [
    id 30
    label "ch&#281;dogi"
  ]
  node [
    id 31
    label "zdrowy"
  ]
  node [
    id 32
    label "po&#380;&#261;dany"
  ]
  node [
    id 33
    label "ciekawy"
  ]
  node [
    id 34
    label "kusz&#261;cy"
  ]
  node [
    id 35
    label "dobry"
  ]
  node [
    id 36
    label "smakowicie"
  ]
  node [
    id 37
    label "spokojny"
  ]
  node [
    id 38
    label "smakowny"
  ]
  node [
    id 39
    label "smacznie"
  ]
  node [
    id 40
    label "feed"
  ]
  node [
    id 41
    label "jedzenie"
  ]
  node [
    id 42
    label "nast&#281;pnie"
  ]
  node [
    id 43
    label "poni&#380;szy"
  ]
  node [
    id 44
    label "doros&#322;y"
  ]
  node [
    id 45
    label "wiele"
  ]
  node [
    id 46
    label "dorodny"
  ]
  node [
    id 47
    label "znaczny"
  ]
  node [
    id 48
    label "du&#380;o"
  ]
  node [
    id 49
    label "prawdziwy"
  ]
  node [
    id 50
    label "niema&#322;o"
  ]
  node [
    id 51
    label "wa&#380;ny"
  ]
  node [
    id 52
    label "rozwini&#281;ty"
  ]
  node [
    id 53
    label "&#380;o&#322;d"
  ]
  node [
    id 54
    label "ilo&#347;&#263;"
  ]
  node [
    id 55
    label "zas&#243;b"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
]
