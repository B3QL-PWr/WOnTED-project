graph [
  maxDegree 6
  minDegree 1
  meanDegree 1.8
  density 0.2
  graphCliqueNumber 2
  node [
    id 0
    label "czym"
    origin "text"
  ]
  node [
    id 1
    label "jeszcze"
    origin "text"
  ]
  node [
    id 2
    label "warto"
    origin "text"
  ]
  node [
    id 3
    label "wiedzie&#263;"
    origin "text"
  ]
  node [
    id 4
    label "ci&#261;gle"
  ]
  node [
    id 5
    label "przysparza&#263;"
  ]
  node [
    id 6
    label "give"
  ]
  node [
    id 7
    label "kali&#263;_si&#281;"
  ]
  node [
    id 8
    label "bonanza"
  ]
  node [
    id 9
    label "cognizance"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 3
    target 9
  ]
]
