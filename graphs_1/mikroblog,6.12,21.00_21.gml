graph [
  maxDegree 27
  minDegree 1
  meanDegree 2.020408163265306
  density 0.020828950136755734
  graphCliqueNumber 3
  node [
    id 0
    label "pami&#281;ta&#263;"
    origin "text"
  ]
  node [
    id 1
    label "motyw"
    origin "text"
  ]
  node [
    id 2
    label "sukienka"
    origin "text"
  ]
  node [
    id 3
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 4
    label "kolor"
    origin "text"
  ]
  node [
    id 5
    label "r&#243;&#380;ny"
    origin "text"
  ]
  node [
    id 6
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 7
    label "r&#243;&#380;nie"
    origin "text"
  ]
  node [
    id 8
    label "widzie&#263;"
    origin "text"
  ]
  node [
    id 9
    label "si&#281;ga&#263;"
  ]
  node [
    id 10
    label "zna&#263;"
  ]
  node [
    id 11
    label "troska&#263;_si&#281;"
  ]
  node [
    id 12
    label "zachowywa&#263;"
  ]
  node [
    id 13
    label "chowa&#263;"
  ]
  node [
    id 14
    label "think"
  ]
  node [
    id 15
    label "pilnowa&#263;"
  ]
  node [
    id 16
    label "robi&#263;"
  ]
  node [
    id 17
    label "recall"
  ]
  node [
    id 18
    label "echo"
  ]
  node [
    id 19
    label "take_care"
  ]
  node [
    id 20
    label "fraza"
  ]
  node [
    id 21
    label "melodia"
  ]
  node [
    id 22
    label "temat"
  ]
  node [
    id 23
    label "przyczyna"
  ]
  node [
    id 24
    label "cecha"
  ]
  node [
    id 25
    label "ozdoba"
  ]
  node [
    id 26
    label "wydarzenie"
  ]
  node [
    id 27
    label "sytuacja"
  ]
  node [
    id 28
    label "przedmiot"
  ]
  node [
    id 29
    label "element"
  ]
  node [
    id 30
    label "kiecka"
  ]
  node [
    id 31
    label "&#347;wieci&#263;"
  ]
  node [
    id 32
    label "prze&#322;ama&#263;_si&#281;"
  ]
  node [
    id 33
    label "prze&#322;amanie"
  ]
  node [
    id 34
    label "prze&#322;amywa&#263;"
  ]
  node [
    id 35
    label "prze&#322;amywa&#263;_si&#281;"
  ]
  node [
    id 36
    label "prze&#322;amywanie_si&#281;"
  ]
  node [
    id 37
    label "ubarwienie"
  ]
  node [
    id 38
    label "symbol"
  ]
  node [
    id 39
    label "prze&#322;amywanie"
  ]
  node [
    id 40
    label "struktura"
  ]
  node [
    id 41
    label "prze&#322;ama&#263;"
  ]
  node [
    id 42
    label "zblakn&#261;&#263;"
  ]
  node [
    id 43
    label "liczba_kwantowa"
  ]
  node [
    id 44
    label "blakn&#261;&#263;"
  ]
  node [
    id 45
    label "zblakni&#281;cie"
  ]
  node [
    id 46
    label "poker"
  ]
  node [
    id 47
    label "&#347;wiecenie"
  ]
  node [
    id 48
    label "blakni&#281;cie"
  ]
  node [
    id 49
    label "prze&#322;amanie_si&#281;"
  ]
  node [
    id 50
    label "inny"
  ]
  node [
    id 51
    label "jaki&#347;"
  ]
  node [
    id 52
    label "asymilowa&#263;"
  ]
  node [
    id 53
    label "wapniak"
  ]
  node [
    id 54
    label "dwun&#243;g"
  ]
  node [
    id 55
    label "polifag"
  ]
  node [
    id 56
    label "wz&#243;r"
  ]
  node [
    id 57
    label "profanum"
  ]
  node [
    id 58
    label "hominid"
  ]
  node [
    id 59
    label "homo_sapiens"
  ]
  node [
    id 60
    label "nasada"
  ]
  node [
    id 61
    label "podw&#322;adny"
  ]
  node [
    id 62
    label "ludzko&#347;&#263;"
  ]
  node [
    id 63
    label "os&#322;abianie"
  ]
  node [
    id 64
    label "mikrokosmos"
  ]
  node [
    id 65
    label "portrecista"
  ]
  node [
    id 66
    label "duch"
  ]
  node [
    id 67
    label "oddzia&#322;ywanie"
  ]
  node [
    id 68
    label "g&#322;owa"
  ]
  node [
    id 69
    label "asymilowanie"
  ]
  node [
    id 70
    label "osoba"
  ]
  node [
    id 71
    label "os&#322;abia&#263;"
  ]
  node [
    id 72
    label "figura"
  ]
  node [
    id 73
    label "Adam"
  ]
  node [
    id 74
    label "senior"
  ]
  node [
    id 75
    label "antropochoria"
  ]
  node [
    id 76
    label "posta&#263;"
  ]
  node [
    id 77
    label "inaczej"
  ]
  node [
    id 78
    label "osobnie"
  ]
  node [
    id 79
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 80
    label "perceive"
  ]
  node [
    id 81
    label "reagowa&#263;"
  ]
  node [
    id 82
    label "spowodowa&#263;"
  ]
  node [
    id 83
    label "male&#263;"
  ]
  node [
    id 84
    label "zmale&#263;"
  ]
  node [
    id 85
    label "spotka&#263;"
  ]
  node [
    id 86
    label "go_steady"
  ]
  node [
    id 87
    label "dostrzega&#263;"
  ]
  node [
    id 88
    label "zwi&#281;ksza&#263;_si&#281;"
  ]
  node [
    id 89
    label "zdawa&#263;_sobie_spraw&#281;"
  ]
  node [
    id 90
    label "ogl&#261;da&#263;"
  ]
  node [
    id 91
    label "os&#261;dza&#263;"
  ]
  node [
    id 92
    label "aprobowa&#263;"
  ]
  node [
    id 93
    label "punkt_widzenia"
  ]
  node [
    id 94
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 95
    label "wzrok"
  ]
  node [
    id 96
    label "postrzega&#263;"
  ]
  node [
    id 97
    label "notice"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
]
