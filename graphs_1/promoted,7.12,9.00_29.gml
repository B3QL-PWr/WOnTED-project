graph [
  maxDegree 15
  minDegree 1
  meanDegree 1.9310344827586208
  density 0.06896551724137931
  graphCliqueNumber 2
  node [
    id 0
    label "wk&#322;ada&#263;"
    origin "text"
  ]
  node [
    id 1
    label "parasol"
    origin "text"
  ]
  node [
    id 2
    label "gard&#322;o"
    origin "text"
  ]
  node [
    id 3
    label "inspirowa&#263;"
  ]
  node [
    id 4
    label "nosi&#263;"
  ]
  node [
    id 5
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 6
    label "introduce"
  ]
  node [
    id 7
    label "place"
  ]
  node [
    id 8
    label "wpaja&#263;"
  ]
  node [
    id 9
    label "wzbudza&#263;"
  ]
  node [
    id 10
    label "obleka&#263;"
  ]
  node [
    id 11
    label "umieszcza&#263;"
  ]
  node [
    id 12
    label "przekazywa&#263;"
  ]
  node [
    id 13
    label "pour"
  ]
  node [
    id 14
    label "odziewa&#263;"
  ]
  node [
    id 15
    label "ubiera&#263;"
  ]
  node [
    id 16
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 17
    label "deszczochron"
  ]
  node [
    id 18
    label "ciennik"
  ]
  node [
    id 19
    label "os&#322;ona"
  ]
  node [
    id 20
    label "miejsce"
  ]
  node [
    id 21
    label "zdarcie"
  ]
  node [
    id 22
    label "jama_gard&#322;owa"
  ]
  node [
    id 23
    label "zachy&#322;ek_gruszkowaty"
  ]
  node [
    id 24
    label "throat"
  ]
  node [
    id 25
    label "pier&#347;cie&#324;_gard&#322;owy_Waldeyera"
  ]
  node [
    id 26
    label "zedrze&#263;"
  ]
  node [
    id 27
    label "przew&#243;d_pokarmowy"
  ]
  node [
    id 28
    label "element_anatomiczny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
]
