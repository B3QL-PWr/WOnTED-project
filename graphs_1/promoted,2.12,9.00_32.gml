graph [
  maxDegree 35
  minDegree 1
  meanDegree 2
  density 0.02631578947368421
  graphCliqueNumber 2
  node [
    id 0
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 1
    label "sie&#263;"
    origin "text"
  ]
  node [
    id 2
    label "handlowy"
    origin "text"
  ]
  node [
    id 3
    label "rezygnowa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "sprzeda&#380;"
    origin "text"
  ]
  node [
    id 5
    label "&#380;ywy"
    origin "text"
  ]
  node [
    id 6
    label "karp"
    origin "text"
  ]
  node [
    id 7
    label "doros&#322;y"
  ]
  node [
    id 8
    label "wiele"
  ]
  node [
    id 9
    label "dorodny"
  ]
  node [
    id 10
    label "znaczny"
  ]
  node [
    id 11
    label "du&#380;o"
  ]
  node [
    id 12
    label "prawdziwy"
  ]
  node [
    id 13
    label "niema&#322;o"
  ]
  node [
    id 14
    label "wa&#380;ny"
  ]
  node [
    id 15
    label "rozwini&#281;ty"
  ]
  node [
    id 16
    label "hipertekst"
  ]
  node [
    id 17
    label "gauze"
  ]
  node [
    id 18
    label "nitka"
  ]
  node [
    id 19
    label "mesh"
  ]
  node [
    id 20
    label "e-hazard"
  ]
  node [
    id 21
    label "netbook"
  ]
  node [
    id 22
    label "cyberprzestrze&#324;"
  ]
  node [
    id 23
    label "biznes_elektroniczny"
  ]
  node [
    id 24
    label "snu&#263;"
  ]
  node [
    id 25
    label "organization"
  ]
  node [
    id 26
    label "zasadzka"
  ]
  node [
    id 27
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 28
    label "web"
  ]
  node [
    id 29
    label "provider"
  ]
  node [
    id 30
    label "struktura"
  ]
  node [
    id 31
    label "us&#322;uga_internetowa"
  ]
  node [
    id 32
    label "punkt_dost&#281;pu"
  ]
  node [
    id 33
    label "organizacja"
  ]
  node [
    id 34
    label "mem"
  ]
  node [
    id 35
    label "vane"
  ]
  node [
    id 36
    label "podcast"
  ]
  node [
    id 37
    label "grooming"
  ]
  node [
    id 38
    label "kszta&#322;t"
  ]
  node [
    id 39
    label "strona"
  ]
  node [
    id 40
    label "obiekt"
  ]
  node [
    id 41
    label "wysnu&#263;"
  ]
  node [
    id 42
    label "gra_sieciowa"
  ]
  node [
    id 43
    label "instalacja"
  ]
  node [
    id 44
    label "sie&#263;_komputerowa"
  ]
  node [
    id 45
    label "net"
  ]
  node [
    id 46
    label "plecionka"
  ]
  node [
    id 47
    label "media"
  ]
  node [
    id 48
    label "rozmieszczenie"
  ]
  node [
    id 49
    label "handlowo"
  ]
  node [
    id 50
    label "charge"
  ]
  node [
    id 51
    label "zrzeka&#263;_si&#281;"
  ]
  node [
    id 52
    label "przestawa&#263;"
  ]
  node [
    id 53
    label "transakcja"
  ]
  node [
    id 54
    label "sprzedaj&#261;cy"
  ]
  node [
    id 55
    label "przeniesienie_praw"
  ]
  node [
    id 56
    label "rabat"
  ]
  node [
    id 57
    label "przeda&#380;"
  ]
  node [
    id 58
    label "czynny"
  ]
  node [
    id 59
    label "cz&#322;owiek"
  ]
  node [
    id 60
    label "aktualny"
  ]
  node [
    id 61
    label "realistyczny"
  ]
  node [
    id 62
    label "silny"
  ]
  node [
    id 63
    label "&#380;ywotny"
  ]
  node [
    id 64
    label "g&#322;&#281;boki"
  ]
  node [
    id 65
    label "naturalny"
  ]
  node [
    id 66
    label "&#380;ycie"
  ]
  node [
    id 67
    label "ciekawy"
  ]
  node [
    id 68
    label "&#380;ywo"
  ]
  node [
    id 69
    label "zgrabny"
  ]
  node [
    id 70
    label "o&#380;ywianie"
  ]
  node [
    id 71
    label "szybki"
  ]
  node [
    id 72
    label "wyra&#378;ny"
  ]
  node [
    id 73
    label "energiczny"
  ]
  node [
    id 74
    label "carp"
  ]
  node [
    id 75
    label "karpiowate"
  ]
  node [
    id 76
    label "ryba"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 12
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
]
