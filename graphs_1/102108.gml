graph [
  maxDegree 19
  minDegree 1
  meanDegree 1.9411764705882353
  density 0.058823529411764705
  graphCliqueNumber 2
  node [
    id 0
    label "jak"
    origin "text"
  ]
  node [
    id 1
    label "wybra&#263;"
    origin "text"
  ]
  node [
    id 2
    label "model"
    origin "text"
  ]
  node [
    id 3
    label "byd&#322;o"
  ]
  node [
    id 4
    label "zobo"
  ]
  node [
    id 5
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 6
    label "yakalo"
  ]
  node [
    id 7
    label "dzo"
  ]
  node [
    id 8
    label "zu&#380;y&#263;"
  ]
  node [
    id 9
    label "distill"
  ]
  node [
    id 10
    label "wyj&#261;&#263;"
  ]
  node [
    id 11
    label "sie&#263;_rybacka"
  ]
  node [
    id 12
    label "powo&#322;a&#263;"
  ]
  node [
    id 13
    label "kotwica"
  ]
  node [
    id 14
    label "ustali&#263;"
  ]
  node [
    id 15
    label "pick"
  ]
  node [
    id 16
    label "typ"
  ]
  node [
    id 17
    label "cz&#322;owiek"
  ]
  node [
    id 18
    label "pozowa&#263;"
  ]
  node [
    id 19
    label "ideal"
  ]
  node [
    id 20
    label "matryca"
  ]
  node [
    id 21
    label "imitacja"
  ]
  node [
    id 22
    label "ruch"
  ]
  node [
    id 23
    label "motif"
  ]
  node [
    id 24
    label "pozowanie"
  ]
  node [
    id 25
    label "wz&#243;r"
  ]
  node [
    id 26
    label "miniatura"
  ]
  node [
    id 27
    label "prezenter"
  ]
  node [
    id 28
    label "facet"
  ]
  node [
    id 29
    label "orygina&#322;"
  ]
  node [
    id 30
    label "mildew"
  ]
  node [
    id 31
    label "spos&#243;b"
  ]
  node [
    id 32
    label "zi&#243;&#322;ko"
  ]
  node [
    id 33
    label "adaptation"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
]
