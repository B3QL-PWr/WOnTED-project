graph [
  maxDegree 31
  minDegree 1
  meanDegree 1.98
  density 0.02
  graphCliqueNumber 3
  node [
    id 0
    label "patostreamer"
    origin "text"
  ]
  node [
    id 1
    label "magical'"
    origin "text"
  ]
  node [
    id 2
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 3
    label "pom&#243;c"
    origin "text"
  ]
  node [
    id 4
    label "polski"
    origin "text"
  ]
  node [
    id 5
    label "policja"
    origin "text"
  ]
  node [
    id 6
    label "zatrzyma&#263;"
    origin "text"
  ]
  node [
    id 7
    label "pijany"
    origin "text"
  ]
  node [
    id 8
    label "kierowca"
    origin "text"
  ]
  node [
    id 9
    label "s&#322;o&#324;ce"
  ]
  node [
    id 10
    label "czynienie_si&#281;"
  ]
  node [
    id 11
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 12
    label "czas"
  ]
  node [
    id 13
    label "long_time"
  ]
  node [
    id 14
    label "przedpo&#322;udnie"
  ]
  node [
    id 15
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 16
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 17
    label "tydzie&#324;"
  ]
  node [
    id 18
    label "godzina"
  ]
  node [
    id 19
    label "t&#322;usty_czwartek"
  ]
  node [
    id 20
    label "wsta&#263;"
  ]
  node [
    id 21
    label "day"
  ]
  node [
    id 22
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 23
    label "przedwiecz&#243;r"
  ]
  node [
    id 24
    label "Sylwester"
  ]
  node [
    id 25
    label "po&#322;udnie"
  ]
  node [
    id 26
    label "wzej&#347;cie"
  ]
  node [
    id 27
    label "podwiecz&#243;r"
  ]
  node [
    id 28
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 29
    label "rano"
  ]
  node [
    id 30
    label "termin"
  ]
  node [
    id 31
    label "ranek"
  ]
  node [
    id 32
    label "doba"
  ]
  node [
    id 33
    label "wiecz&#243;r"
  ]
  node [
    id 34
    label "walentynki"
  ]
  node [
    id 35
    label "popo&#322;udnie"
  ]
  node [
    id 36
    label "noc"
  ]
  node [
    id 37
    label "wstanie"
  ]
  node [
    id 38
    label "help"
  ]
  node [
    id 39
    label "aid"
  ]
  node [
    id 40
    label "u&#322;atwi&#263;"
  ]
  node [
    id 41
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 42
    label "concur"
  ]
  node [
    id 43
    label "zrobi&#263;"
  ]
  node [
    id 44
    label "zaskutkowa&#263;"
  ]
  node [
    id 45
    label "lacki"
  ]
  node [
    id 46
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 47
    label "przedmiot"
  ]
  node [
    id 48
    label "sztajer"
  ]
  node [
    id 49
    label "drabant"
  ]
  node [
    id 50
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 51
    label "polak"
  ]
  node [
    id 52
    label "pierogi_ruskie"
  ]
  node [
    id 53
    label "krakowiak"
  ]
  node [
    id 54
    label "Polish"
  ]
  node [
    id 55
    label "j&#281;zyk"
  ]
  node [
    id 56
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 57
    label "oberek"
  ]
  node [
    id 58
    label "po_polsku"
  ]
  node [
    id 59
    label "mazur"
  ]
  node [
    id 60
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 61
    label "chodzony"
  ]
  node [
    id 62
    label "skoczny"
  ]
  node [
    id 63
    label "ryba_po_grecku"
  ]
  node [
    id 64
    label "goniony"
  ]
  node [
    id 65
    label "polsko"
  ]
  node [
    id 66
    label "komisariat"
  ]
  node [
    id 67
    label "psiarnia"
  ]
  node [
    id 68
    label "posterunek"
  ]
  node [
    id 69
    label "grupa"
  ]
  node [
    id 70
    label "organ"
  ]
  node [
    id 71
    label "s&#322;u&#380;ba"
  ]
  node [
    id 72
    label "continue"
  ]
  node [
    id 73
    label "zabra&#263;"
  ]
  node [
    id 74
    label "zaaresztowa&#263;"
  ]
  node [
    id 75
    label "uniemo&#380;liwi&#263;"
  ]
  node [
    id 76
    label "spowodowa&#263;"
  ]
  node [
    id 77
    label "przerwa&#263;"
  ]
  node [
    id 78
    label "zamkn&#261;&#263;"
  ]
  node [
    id 79
    label "bury"
  ]
  node [
    id 80
    label "komornik"
  ]
  node [
    id 81
    label "unieruchomi&#263;"
  ]
  node [
    id 82
    label "suspend"
  ]
  node [
    id 83
    label "give"
  ]
  node [
    id 84
    label "bankrupt"
  ]
  node [
    id 85
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 86
    label "zaczepi&#263;"
  ]
  node [
    id 87
    label "przechowa&#263;"
  ]
  node [
    id 88
    label "anticipate"
  ]
  node [
    id 89
    label "cz&#322;owiek"
  ]
  node [
    id 90
    label "upicie_si&#281;"
  ]
  node [
    id 91
    label "szalony"
  ]
  node [
    id 92
    label "d&#281;tka"
  ]
  node [
    id 93
    label "pij&#261;cy"
  ]
  node [
    id 94
    label "upijanie_si&#281;"
  ]
  node [
    id 95
    label "napi&#322;y"
  ]
  node [
    id 96
    label "nieprzytomny"
  ]
  node [
    id 97
    label "transportowiec"
  ]
  node [
    id 98
    label "Daniel"
  ]
  node [
    id 99
    label "Magical"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 98
    target 99
  ]
]
