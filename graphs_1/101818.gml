graph [
  maxDegree 40
  minDegree 1
  meanDegree 2.0165289256198347
  density 0.008367339940331264
  graphCliqueNumber 3
  node [
    id 0
    label "zwi&#261;zek"
    origin "text"
  ]
  node [
    id 1
    label "tym"
    origin "text"
  ]
  node [
    id 2
    label "moja"
    origin "text"
  ]
  node [
    id 3
    label "praca"
    origin "text"
  ]
  node [
    id 4
    label "nad"
    origin "text"
  ]
  node [
    id 5
    label "doktorat"
    origin "text"
  ]
  node [
    id 6
    label "zaczyna&#263;"
    origin "text"
  ]
  node [
    id 7
    label "powoli"
    origin "text"
  ]
  node [
    id 8
    label "wkracza&#263;"
    origin "text"
  ]
  node [
    id 9
    label "decyduj&#261;cy"
    origin "text"
  ]
  node [
    id 10
    label "faza"
    origin "text"
  ]
  node [
    id 11
    label "zacz&#261;&#263;by&#263;"
    origin "text"
  ]
  node [
    id 12
    label "coraz"
    origin "text"
  ]
  node [
    id 13
    label "du&#380;o"
    origin "text"
  ]
  node [
    id 14
    label "czyta&#263;"
    origin "text"
  ]
  node [
    id 15
    label "ksi&#261;&#380;ek"
    origin "text"
  ]
  node [
    id 16
    label "fan"
    origin "text"
  ]
  node [
    id 17
    label "sprowadza&#263;"
    origin "text"
  ]
  node [
    id 18
    label "czas"
    origin "text"
  ]
  node [
    id 19
    label "ci&#281;&#380;ki"
    origin "text"
  ]
  node [
    id 20
    label "pieni&#261;dz"
    origin "text"
  ]
  node [
    id 21
    label "stany"
    origin "text"
  ]
  node [
    id 22
    label "odwodnienie"
  ]
  node [
    id 23
    label "konstytucja"
  ]
  node [
    id 24
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 25
    label "substancja_chemiczna"
  ]
  node [
    id 26
    label "bratnia_dusza"
  ]
  node [
    id 27
    label "zwi&#261;zanie"
  ]
  node [
    id 28
    label "lokant"
  ]
  node [
    id 29
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 30
    label "zwi&#261;za&#263;"
  ]
  node [
    id 31
    label "organizacja"
  ]
  node [
    id 32
    label "odwadnia&#263;"
  ]
  node [
    id 33
    label "marriage"
  ]
  node [
    id 34
    label "marketing_afiliacyjny"
  ]
  node [
    id 35
    label "bearing"
  ]
  node [
    id 36
    label "wi&#261;zanie"
  ]
  node [
    id 37
    label "odwadnianie"
  ]
  node [
    id 38
    label "koligacja"
  ]
  node [
    id 39
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 40
    label "odwodni&#263;"
  ]
  node [
    id 41
    label "azeotrop"
  ]
  node [
    id 42
    label "powi&#261;zanie"
  ]
  node [
    id 43
    label "stosunek_pracy"
  ]
  node [
    id 44
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 45
    label "benedykty&#324;ski"
  ]
  node [
    id 46
    label "pracowanie"
  ]
  node [
    id 47
    label "zaw&#243;d"
  ]
  node [
    id 48
    label "kierownictwo"
  ]
  node [
    id 49
    label "zmiana"
  ]
  node [
    id 50
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 51
    label "wytw&#243;r"
  ]
  node [
    id 52
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 53
    label "tynkarski"
  ]
  node [
    id 54
    label "czynnik_produkcji"
  ]
  node [
    id 55
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 56
    label "zobowi&#261;zanie"
  ]
  node [
    id 57
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 58
    label "czynno&#347;&#263;"
  ]
  node [
    id 59
    label "tyrka"
  ]
  node [
    id 60
    label "pracowa&#263;"
  ]
  node [
    id 61
    label "siedziba"
  ]
  node [
    id 62
    label "poda&#380;_pracy"
  ]
  node [
    id 63
    label "miejsce"
  ]
  node [
    id 64
    label "zak&#322;ad"
  ]
  node [
    id 65
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 66
    label "najem"
  ]
  node [
    id 67
    label "dysertacja"
  ]
  node [
    id 68
    label "doctor's_degree"
  ]
  node [
    id 69
    label "stopie&#324;_naukowy"
  ]
  node [
    id 70
    label "obrona"
  ]
  node [
    id 71
    label "open"
  ]
  node [
    id 72
    label "odejmowa&#263;"
  ]
  node [
    id 73
    label "mie&#263;_miejsce"
  ]
  node [
    id 74
    label "robi&#263;_pierwszy_krok"
  ]
  node [
    id 75
    label "set_about"
  ]
  node [
    id 76
    label "begin"
  ]
  node [
    id 77
    label "post&#281;powa&#263;"
  ]
  node [
    id 78
    label "bankrupt"
  ]
  node [
    id 79
    label "wolny"
  ]
  node [
    id 80
    label "stopniowo"
  ]
  node [
    id 81
    label "wolniej"
  ]
  node [
    id 82
    label "niespiesznie"
  ]
  node [
    id 83
    label "bezproblemowo"
  ]
  node [
    id 84
    label "spokojny"
  ]
  node [
    id 85
    label "w&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 86
    label "zajmowa&#263;"
  ]
  node [
    id 87
    label "wchodzi&#263;"
  ]
  node [
    id 88
    label "porusza&#263;"
  ]
  node [
    id 89
    label "dochodzi&#263;"
  ]
  node [
    id 90
    label "invade"
  ]
  node [
    id 91
    label "intervene"
  ]
  node [
    id 92
    label "decyduj&#261;co"
  ]
  node [
    id 93
    label "najwa&#380;niejszy"
  ]
  node [
    id 94
    label "dw&#243;jnik"
  ]
  node [
    id 95
    label "fotoelement"
  ]
  node [
    id 96
    label "uk&#322;ad_fizyczny"
  ]
  node [
    id 97
    label "obsesja"
  ]
  node [
    id 98
    label "komutowa&#263;"
  ]
  node [
    id 99
    label "degree"
  ]
  node [
    id 100
    label "cykl_astronomiczny"
  ]
  node [
    id 101
    label "komutowanie"
  ]
  node [
    id 102
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 103
    label "coil"
  ]
  node [
    id 104
    label "przew&#243;d_elektryczny"
  ]
  node [
    id 105
    label "obw&#243;d"
  ]
  node [
    id 106
    label "zjawisko"
  ]
  node [
    id 107
    label "mieszalno&#347;&#263;"
  ]
  node [
    id 108
    label "stan_skupienia"
  ]
  node [
    id 109
    label "przyrz&#261;d_elektryczny"
  ]
  node [
    id 110
    label "nastr&#243;j"
  ]
  node [
    id 111
    label "przew&#243;d"
  ]
  node [
    id 112
    label "&#322;&#261;cznik_elektryczny"
  ]
  node [
    id 113
    label "kraw&#281;d&#378;"
  ]
  node [
    id 114
    label "okres"
  ]
  node [
    id 115
    label "po&#322;&#261;czenie_elektryczne"
  ]
  node [
    id 116
    label "przerywacz"
  ]
  node [
    id 117
    label "du&#380;y"
  ]
  node [
    id 118
    label "cz&#281;sto"
  ]
  node [
    id 119
    label "bardzo"
  ]
  node [
    id 120
    label "mocno"
  ]
  node [
    id 121
    label "wiela"
  ]
  node [
    id 122
    label "dysleksja"
  ]
  node [
    id 123
    label "umie&#263;"
  ]
  node [
    id 124
    label "wr&#243;&#380;y&#263;"
  ]
  node [
    id 125
    label "przetwarza&#263;"
  ]
  node [
    id 126
    label "read"
  ]
  node [
    id 127
    label "poznawa&#263;"
  ]
  node [
    id 128
    label "obserwowa&#263;"
  ]
  node [
    id 129
    label "odczytywa&#263;"
  ]
  node [
    id 130
    label "mi&#322;o&#347;nik"
  ]
  node [
    id 131
    label "fan_club"
  ]
  node [
    id 132
    label "fandom"
  ]
  node [
    id 133
    label "zmienia&#263;"
  ]
  node [
    id 134
    label "ogranicza&#263;"
  ]
  node [
    id 135
    label "kierowa&#263;"
  ]
  node [
    id 136
    label "upraszcza&#263;"
  ]
  node [
    id 137
    label "head"
  ]
  node [
    id 138
    label "reduce"
  ]
  node [
    id 139
    label "wprowadza&#263;"
  ]
  node [
    id 140
    label "deliver"
  ]
  node [
    id 141
    label "u&#322;amek"
  ]
  node [
    id 142
    label "powodowa&#263;"
  ]
  node [
    id 143
    label "go"
  ]
  node [
    id 144
    label "make"
  ]
  node [
    id 145
    label "prowadzi&#263;"
  ]
  node [
    id 146
    label "czasokres"
  ]
  node [
    id 147
    label "trawienie"
  ]
  node [
    id 148
    label "kategoria_gramatyczna"
  ]
  node [
    id 149
    label "period"
  ]
  node [
    id 150
    label "odczyt"
  ]
  node [
    id 151
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 152
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 153
    label "chwila"
  ]
  node [
    id 154
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 155
    label "poprzedzenie"
  ]
  node [
    id 156
    label "koniugacja"
  ]
  node [
    id 157
    label "dzieje"
  ]
  node [
    id 158
    label "poprzedzi&#263;"
  ]
  node [
    id 159
    label "przep&#322;ywanie"
  ]
  node [
    id 160
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 161
    label "odwlekanie_si&#281;"
  ]
  node [
    id 162
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 163
    label "Zeitgeist"
  ]
  node [
    id 164
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 165
    label "okres_czasu"
  ]
  node [
    id 166
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 167
    label "pochodzi&#263;"
  ]
  node [
    id 168
    label "schy&#322;ek"
  ]
  node [
    id 169
    label "czwarty_wymiar"
  ]
  node [
    id 170
    label "chronometria"
  ]
  node [
    id 171
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 172
    label "poprzedzanie"
  ]
  node [
    id 173
    label "pogoda"
  ]
  node [
    id 174
    label "zegar"
  ]
  node [
    id 175
    label "pochodzenie"
  ]
  node [
    id 176
    label "poprzedza&#263;"
  ]
  node [
    id 177
    label "trawi&#263;"
  ]
  node [
    id 178
    label "time_period"
  ]
  node [
    id 179
    label "rachuba_czasu"
  ]
  node [
    id 180
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 181
    label "czasoprzestrze&#324;"
  ]
  node [
    id 182
    label "laba"
  ]
  node [
    id 183
    label "nieprzejrzysty"
  ]
  node [
    id 184
    label "grubo"
  ]
  node [
    id 185
    label "przyswajalny"
  ]
  node [
    id 186
    label "masywny"
  ]
  node [
    id 187
    label "zbrojny"
  ]
  node [
    id 188
    label "gro&#378;ny"
  ]
  node [
    id 189
    label "trudny"
  ]
  node [
    id 190
    label "wymagaj&#261;cy"
  ]
  node [
    id 191
    label "ambitny"
  ]
  node [
    id 192
    label "monumentalny"
  ]
  node [
    id 193
    label "uci&#261;&#380;liwy"
  ]
  node [
    id 194
    label "niezgrabny"
  ]
  node [
    id 195
    label "charakterystyczny"
  ]
  node [
    id 196
    label "przyt&#322;aczaj&#261;cy"
  ]
  node [
    id 197
    label "k&#322;opotliwy"
  ]
  node [
    id 198
    label "dotkliwy"
  ]
  node [
    id 199
    label "nieudany"
  ]
  node [
    id 200
    label "mocny"
  ]
  node [
    id 201
    label "bojowy"
  ]
  node [
    id 202
    label "ci&#281;&#380;ko"
  ]
  node [
    id 203
    label "kompletny"
  ]
  node [
    id 204
    label "intensywny"
  ]
  node [
    id 205
    label "wielki"
  ]
  node [
    id 206
    label "oci&#281;&#380;a&#322;y"
  ]
  node [
    id 207
    label "liczny"
  ]
  node [
    id 208
    label "niedelikatny"
  ]
  node [
    id 209
    label "poda&#380;_pieni&#261;dza"
  ]
  node [
    id 210
    label "zdewaluowa&#263;"
  ]
  node [
    id 211
    label "moniak"
  ]
  node [
    id 212
    label "zdewaluowanie"
  ]
  node [
    id 213
    label "jednostka_monetarna"
  ]
  node [
    id 214
    label "&#347;rodek_p&#322;atniczy"
  ]
  node [
    id 215
    label "numizmat"
  ]
  node [
    id 216
    label "rozmienianie"
  ]
  node [
    id 217
    label "rozmienienie"
  ]
  node [
    id 218
    label "rozmieni&#263;"
  ]
  node [
    id 219
    label "dewaluowanie"
  ]
  node [
    id 220
    label "nomina&#322;"
  ]
  node [
    id 221
    label "coin"
  ]
  node [
    id 222
    label "dewaluowa&#263;"
  ]
  node [
    id 223
    label "pieni&#261;dze"
  ]
  node [
    id 224
    label "znak_pieni&#281;&#380;ny"
  ]
  node [
    id 225
    label "rozmienia&#263;"
  ]
  node [
    id 226
    label "Science"
  ]
  node [
    id 227
    label "Fiction"
  ]
  node [
    id 228
    label "Audiences"
  ]
  node [
    id 229
    label "John"
  ]
  node [
    id 230
    label "Tulloch"
  ]
  node [
    id 231
    label "henr"
  ]
  node [
    id 232
    label "Jenkins"
  ]
  node [
    id 233
    label "Doctor"
  ]
  node [
    id 234
    label "WHO"
  ]
  node [
    id 235
    label "star"
  ]
  node [
    id 236
    label "Trek"
  ]
  node [
    id 237
    label "David"
  ]
  node [
    id 238
    label "Morleya"
  ]
  node [
    id 239
    label "The"
  ]
  node [
    id 240
    label "Nationwide"
  ]
  node [
    id 241
    label "Audience"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 18
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 50
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 10
    target 105
  ]
  edge [
    source 10
    target 106
  ]
  edge [
    source 10
    target 107
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 10
    target 110
  ]
  edge [
    source 10
    target 111
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 13
    target 117
  ]
  edge [
    source 13
    target 118
  ]
  edge [
    source 13
    target 119
  ]
  edge [
    source 13
    target 120
  ]
  edge [
    source 13
    target 121
  ]
  edge [
    source 14
    target 122
  ]
  edge [
    source 14
    target 123
  ]
  edge [
    source 14
    target 124
  ]
  edge [
    source 14
    target 125
  ]
  edge [
    source 14
    target 126
  ]
  edge [
    source 14
    target 127
  ]
  edge [
    source 14
    target 128
  ]
  edge [
    source 14
    target 129
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 15
    target 20
  ]
  edge [
    source 16
    target 130
  ]
  edge [
    source 16
    target 131
  ]
  edge [
    source 16
    target 132
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 133
  ]
  edge [
    source 17
    target 134
  ]
  edge [
    source 17
    target 135
  ]
  edge [
    source 17
    target 136
  ]
  edge [
    source 17
    target 137
  ]
  edge [
    source 17
    target 138
  ]
  edge [
    source 17
    target 139
  ]
  edge [
    source 17
    target 140
  ]
  edge [
    source 17
    target 141
  ]
  edge [
    source 17
    target 142
  ]
  edge [
    source 17
    target 143
  ]
  edge [
    source 17
    target 144
  ]
  edge [
    source 17
    target 145
  ]
  edge [
    source 17
    target 21
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 146
  ]
  edge [
    source 18
    target 147
  ]
  edge [
    source 18
    target 148
  ]
  edge [
    source 18
    target 149
  ]
  edge [
    source 18
    target 150
  ]
  edge [
    source 18
    target 151
  ]
  edge [
    source 18
    target 152
  ]
  edge [
    source 18
    target 153
  ]
  edge [
    source 18
    target 154
  ]
  edge [
    source 18
    target 155
  ]
  edge [
    source 18
    target 156
  ]
  edge [
    source 18
    target 157
  ]
  edge [
    source 18
    target 158
  ]
  edge [
    source 18
    target 159
  ]
  edge [
    source 18
    target 160
  ]
  edge [
    source 18
    target 161
  ]
  edge [
    source 18
    target 162
  ]
  edge [
    source 18
    target 163
  ]
  edge [
    source 18
    target 164
  ]
  edge [
    source 18
    target 165
  ]
  edge [
    source 18
    target 166
  ]
  edge [
    source 18
    target 167
  ]
  edge [
    source 18
    target 168
  ]
  edge [
    source 18
    target 169
  ]
  edge [
    source 18
    target 170
  ]
  edge [
    source 18
    target 171
  ]
  edge [
    source 18
    target 172
  ]
  edge [
    source 18
    target 173
  ]
  edge [
    source 18
    target 174
  ]
  edge [
    source 18
    target 175
  ]
  edge [
    source 18
    target 176
  ]
  edge [
    source 18
    target 177
  ]
  edge [
    source 18
    target 178
  ]
  edge [
    source 18
    target 179
  ]
  edge [
    source 18
    target 180
  ]
  edge [
    source 18
    target 181
  ]
  edge [
    source 18
    target 182
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 183
  ]
  edge [
    source 19
    target 79
  ]
  edge [
    source 19
    target 184
  ]
  edge [
    source 19
    target 185
  ]
  edge [
    source 19
    target 186
  ]
  edge [
    source 19
    target 187
  ]
  edge [
    source 19
    target 188
  ]
  edge [
    source 19
    target 189
  ]
  edge [
    source 19
    target 190
  ]
  edge [
    source 19
    target 191
  ]
  edge [
    source 19
    target 192
  ]
  edge [
    source 19
    target 193
  ]
  edge [
    source 19
    target 194
  ]
  edge [
    source 19
    target 195
  ]
  edge [
    source 19
    target 196
  ]
  edge [
    source 19
    target 197
  ]
  edge [
    source 19
    target 198
  ]
  edge [
    source 19
    target 199
  ]
  edge [
    source 19
    target 200
  ]
  edge [
    source 19
    target 201
  ]
  edge [
    source 19
    target 202
  ]
  edge [
    source 19
    target 203
  ]
  edge [
    source 19
    target 204
  ]
  edge [
    source 19
    target 205
  ]
  edge [
    source 19
    target 206
  ]
  edge [
    source 19
    target 207
  ]
  edge [
    source 19
    target 208
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 209
  ]
  edge [
    source 20
    target 210
  ]
  edge [
    source 20
    target 211
  ]
  edge [
    source 20
    target 51
  ]
  edge [
    source 20
    target 212
  ]
  edge [
    source 20
    target 213
  ]
  edge [
    source 20
    target 214
  ]
  edge [
    source 20
    target 215
  ]
  edge [
    source 20
    target 216
  ]
  edge [
    source 20
    target 217
  ]
  edge [
    source 20
    target 218
  ]
  edge [
    source 20
    target 219
  ]
  edge [
    source 20
    target 220
  ]
  edge [
    source 20
    target 221
  ]
  edge [
    source 20
    target 222
  ]
  edge [
    source 20
    target 223
  ]
  edge [
    source 20
    target 224
  ]
  edge [
    source 20
    target 225
  ]
  edge [
    source 226
    target 227
  ]
  edge [
    source 226
    target 228
  ]
  edge [
    source 227
    target 228
  ]
  edge [
    source 229
    target 230
  ]
  edge [
    source 231
    target 232
  ]
  edge [
    source 233
    target 234
  ]
  edge [
    source 235
    target 236
  ]
  edge [
    source 237
    target 238
  ]
  edge [
    source 239
    target 240
  ]
  edge [
    source 239
    target 241
  ]
  edge [
    source 240
    target 241
  ]
]
