graph [
  maxDegree 39
  minDegree 1
  meanDegree 1.945945945945946
  density 0.026656793780081452
  graphCliqueNumber 2
  node [
    id 0
    label "anonimowemirkowyznania"
    origin "text"
  ]
  node [
    id 1
    label "&#380;ona"
    origin "text"
  ]
  node [
    id 2
    label "jeden"
    origin "text"
  ]
  node [
    id 3
    label "mirek"
    origin "text"
  ]
  node [
    id 4
    label "zdradza&#263;"
    origin "text"
  ]
  node [
    id 5
    label "kilka"
    origin "text"
  ]
  node [
    id 6
    label "osoba"
    origin "text"
  ]
  node [
    id 7
    label "jednocze&#347;cnie"
    origin "text"
  ]
  node [
    id 8
    label "ma&#322;&#380;onek"
  ]
  node [
    id 9
    label "panna_m&#322;oda"
  ]
  node [
    id 10
    label "partnerka"
  ]
  node [
    id 11
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 12
    label "&#347;lubna"
  ]
  node [
    id 13
    label "kobita"
  ]
  node [
    id 14
    label "kieliszek"
  ]
  node [
    id 15
    label "shot"
  ]
  node [
    id 16
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 17
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 18
    label "jaki&#347;"
  ]
  node [
    id 19
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 20
    label "jednolicie"
  ]
  node [
    id 21
    label "w&#243;dka"
  ]
  node [
    id 22
    label "ten"
  ]
  node [
    id 23
    label "ujednolicenie"
  ]
  node [
    id 24
    label "jednakowy"
  ]
  node [
    id 25
    label "informowa&#263;"
  ]
  node [
    id 26
    label "demaskator"
  ]
  node [
    id 27
    label "objawia&#263;"
  ]
  node [
    id 28
    label "unwrap"
  ]
  node [
    id 29
    label "indicate"
  ]
  node [
    id 30
    label "odst&#281;powa&#263;"
  ]
  node [
    id 31
    label "express"
  ]
  node [
    id 32
    label "narusza&#263;"
  ]
  node [
    id 33
    label "&#347;ledziowate"
  ]
  node [
    id 34
    label "ryba"
  ]
  node [
    id 35
    label "Zgredek"
  ]
  node [
    id 36
    label "kategoria_gramatyczna"
  ]
  node [
    id 37
    label "Casanova"
  ]
  node [
    id 38
    label "Don_Juan"
  ]
  node [
    id 39
    label "Gargantua"
  ]
  node [
    id 40
    label "Faust"
  ]
  node [
    id 41
    label "profanum"
  ]
  node [
    id 42
    label "Chocho&#322;"
  ]
  node [
    id 43
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 44
    label "koniugacja"
  ]
  node [
    id 45
    label "Winnetou"
  ]
  node [
    id 46
    label "Dwukwiat"
  ]
  node [
    id 47
    label "homo_sapiens"
  ]
  node [
    id 48
    label "Edyp"
  ]
  node [
    id 49
    label "Herkules_Poirot"
  ]
  node [
    id 50
    label "ludzko&#347;&#263;"
  ]
  node [
    id 51
    label "mikrokosmos"
  ]
  node [
    id 52
    label "person"
  ]
  node [
    id 53
    label "Sherlock_Holmes"
  ]
  node [
    id 54
    label "portrecista"
  ]
  node [
    id 55
    label "Szwejk"
  ]
  node [
    id 56
    label "Hamlet"
  ]
  node [
    id 57
    label "duch"
  ]
  node [
    id 58
    label "g&#322;owa"
  ]
  node [
    id 59
    label "oddzia&#322;ywanie"
  ]
  node [
    id 60
    label "Quasimodo"
  ]
  node [
    id 61
    label "Dulcynea"
  ]
  node [
    id 62
    label "Don_Kiszot"
  ]
  node [
    id 63
    label "Wallenrod"
  ]
  node [
    id 64
    label "Plastu&#347;"
  ]
  node [
    id 65
    label "Harry_Potter"
  ]
  node [
    id 66
    label "figura"
  ]
  node [
    id 67
    label "parali&#380;owa&#263;"
  ]
  node [
    id 68
    label "istota"
  ]
  node [
    id 69
    label "Werter"
  ]
  node [
    id 70
    label "antropochoria"
  ]
  node [
    id 71
    label "posta&#263;"
  ]
  node [
    id 72
    label "maciek"
  ]
  node [
    id 73
    label "Musia&#322;&#261;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 72
    target 73
  ]
]
