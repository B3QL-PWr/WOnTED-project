graph [
  maxDegree 3
  minDegree 1
  meanDegree 1.3333333333333333
  density 0.26666666666666666
  graphCliqueNumber 2
  node [
    id 0
    label "gwardia"
    origin "text"
  ]
  node [
    id 1
    label "lublin"
    origin "text"
  ]
  node [
    id 2
    label "ochrona"
  ]
  node [
    id 3
    label "formacja"
  ]
  node [
    id 4
    label "puchar"
  ]
  node [
    id 5
    label "polski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 4
    target 5
  ]
]
