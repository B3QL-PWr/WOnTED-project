graph [
  maxDegree 31
  minDegree 1
  meanDegree 2.0291970802919708
  density 0.014920566766852727
  graphCliqueNumber 2
  node [
    id 0
    label "podstawa"
    origin "text"
  ]
  node [
    id 1
    label "art"
    origin "text"
  ]
  node [
    id 2
    label "dawny"
    origin "text"
  ]
  node [
    id 3
    label "usta"
    origin "text"
  ]
  node [
    id 4
    label "ustawa"
    origin "text"
  ]
  node [
    id 5
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 6
    label "lipiec"
    origin "text"
  ]
  node [
    id 7
    label "rocznik"
    origin "text"
  ]
  node [
    id 8
    label "s&#322;u&#380;ba"
    origin "text"
  ]
  node [
    id 9
    label "celny"
    origin "text"
  ]
  node [
    id 10
    label "dziennik"
    origin "text"
  ]
  node [
    id 11
    label "poz"
    origin "text"
  ]
  node [
    id 12
    label "zarz&#261;dza&#263;"
    origin "text"
  ]
  node [
    id 13
    label "si&#281;"
    origin "text"
  ]
  node [
    id 14
    label "nast&#281;powa&#263;"
    origin "text"
  ]
  node [
    id 15
    label "podstawowy"
  ]
  node [
    id 16
    label "strategia"
  ]
  node [
    id 17
    label "pot&#281;ga"
  ]
  node [
    id 18
    label "zasadzenie"
  ]
  node [
    id 19
    label "za&#322;o&#380;enie"
  ]
  node [
    id 20
    label "&#347;ciana"
  ]
  node [
    id 21
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 22
    label "przedmiot"
  ]
  node [
    id 23
    label "documentation"
  ]
  node [
    id 24
    label "dzieci&#281;ctwo"
  ]
  node [
    id 25
    label "pomys&#322;"
  ]
  node [
    id 26
    label "bok"
  ]
  node [
    id 27
    label "d&#243;&#322;"
  ]
  node [
    id 28
    label "punkt_odniesienia"
  ]
  node [
    id 29
    label "column"
  ]
  node [
    id 30
    label "zasadzi&#263;"
  ]
  node [
    id 31
    label "background"
  ]
  node [
    id 32
    label "przesz&#322;y"
  ]
  node [
    id 33
    label "dawno"
  ]
  node [
    id 34
    label "dawniej"
  ]
  node [
    id 35
    label "kombatant"
  ]
  node [
    id 36
    label "stary"
  ]
  node [
    id 37
    label "odleg&#322;y"
  ]
  node [
    id 38
    label "anachroniczny"
  ]
  node [
    id 39
    label "przestarza&#322;y"
  ]
  node [
    id 40
    label "od_dawna"
  ]
  node [
    id 41
    label "poprzedni"
  ]
  node [
    id 42
    label "d&#322;ugoletni"
  ]
  node [
    id 43
    label "wcze&#347;niejszy"
  ]
  node [
    id 44
    label "niegdysiejszy"
  ]
  node [
    id 45
    label "warga_dolna"
  ]
  node [
    id 46
    label "ssa&#263;"
  ]
  node [
    id 47
    label "zaci&#261;&#263;"
  ]
  node [
    id 48
    label "ryjek"
  ]
  node [
    id 49
    label "twarz"
  ]
  node [
    id 50
    label "dzi&#243;b"
  ]
  node [
    id 51
    label "&#322;uk_Kupidyna"
  ]
  node [
    id 52
    label "ssanie"
  ]
  node [
    id 53
    label "zaci&#281;cie"
  ]
  node [
    id 54
    label "jadaczka"
  ]
  node [
    id 55
    label "zacinanie"
  ]
  node [
    id 56
    label "organ"
  ]
  node [
    id 57
    label "jama_ustna"
  ]
  node [
    id 58
    label "otw&#243;r_g&#281;bowy"
  ]
  node [
    id 59
    label "warga_g&#243;rna"
  ]
  node [
    id 60
    label "zacina&#263;"
  ]
  node [
    id 61
    label "Karta_Nauczyciela"
  ]
  node [
    id 62
    label "marc&#243;wka"
  ]
  node [
    id 63
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 64
    label "akt"
  ]
  node [
    id 65
    label "przej&#347;&#263;"
  ]
  node [
    id 66
    label "charter"
  ]
  node [
    id 67
    label "przej&#347;cie"
  ]
  node [
    id 68
    label "s&#322;o&#324;ce"
  ]
  node [
    id 69
    label "czynienie_si&#281;"
  ]
  node [
    id 70
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 71
    label "czas"
  ]
  node [
    id 72
    label "long_time"
  ]
  node [
    id 73
    label "przedpo&#322;udnie"
  ]
  node [
    id 74
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 75
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 76
    label "tydzie&#324;"
  ]
  node [
    id 77
    label "godzina"
  ]
  node [
    id 78
    label "t&#322;usty_czwartek"
  ]
  node [
    id 79
    label "wsta&#263;"
  ]
  node [
    id 80
    label "day"
  ]
  node [
    id 81
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 82
    label "przedwiecz&#243;r"
  ]
  node [
    id 83
    label "Sylwester"
  ]
  node [
    id 84
    label "po&#322;udnie"
  ]
  node [
    id 85
    label "wzej&#347;cie"
  ]
  node [
    id 86
    label "podwiecz&#243;r"
  ]
  node [
    id 87
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 88
    label "rano"
  ]
  node [
    id 89
    label "termin"
  ]
  node [
    id 90
    label "ranek"
  ]
  node [
    id 91
    label "doba"
  ]
  node [
    id 92
    label "wiecz&#243;r"
  ]
  node [
    id 93
    label "walentynki"
  ]
  node [
    id 94
    label "popo&#322;udnie"
  ]
  node [
    id 95
    label "noc"
  ]
  node [
    id 96
    label "wstanie"
  ]
  node [
    id 97
    label "miesi&#261;c"
  ]
  node [
    id 98
    label "formacja"
  ]
  node [
    id 99
    label "kronika"
  ]
  node [
    id 100
    label "czasopismo"
  ]
  node [
    id 101
    label "yearbook"
  ]
  node [
    id 102
    label "service"
  ]
  node [
    id 103
    label "ZOMO"
  ]
  node [
    id 104
    label "czworak"
  ]
  node [
    id 105
    label "zesp&#243;&#322;"
  ]
  node [
    id 106
    label "s&#322;u&#380;&#261;ca"
  ]
  node [
    id 107
    label "instytucja"
  ]
  node [
    id 108
    label "s&#322;u&#380;&#261;cy"
  ]
  node [
    id 109
    label "praca"
  ]
  node [
    id 110
    label "wys&#322;uga"
  ]
  node [
    id 111
    label "trafnie"
  ]
  node [
    id 112
    label "c&#322;owy"
  ]
  node [
    id 113
    label "celnie"
  ]
  node [
    id 114
    label "udany"
  ]
  node [
    id 115
    label "wybitny"
  ]
  node [
    id 116
    label "dobry"
  ]
  node [
    id 117
    label "s&#322;uszny"
  ]
  node [
    id 118
    label "spis"
  ]
  node [
    id 119
    label "sheet"
  ]
  node [
    id 120
    label "gazeta"
  ]
  node [
    id 121
    label "diariusz"
  ]
  node [
    id 122
    label "pami&#281;tnik"
  ]
  node [
    id 123
    label "journal"
  ]
  node [
    id 124
    label "ksi&#281;ga"
  ]
  node [
    id 125
    label "program_informacyjny"
  ]
  node [
    id 126
    label "control"
  ]
  node [
    id 127
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 128
    label "sprawowa&#263;"
  ]
  node [
    id 129
    label "poleca&#263;"
  ]
  node [
    id 130
    label "decydowa&#263;"
  ]
  node [
    id 131
    label "mie&#263;_miejsce"
  ]
  node [
    id 132
    label "chance"
  ]
  node [
    id 133
    label "zdarza&#263;_si&#281;"
  ]
  node [
    id 134
    label "alternate"
  ]
  node [
    id 135
    label "naciska&#263;"
  ]
  node [
    id 136
    label "atakowa&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 10
  ]
  edge [
    source 7
    target 11
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 9
    target 116
  ]
  edge [
    source 9
    target 117
  ]
  edge [
    source 9
    target 12
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 10
    target 121
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 10
    target 125
  ]
  edge [
    source 11
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 131
  ]
  edge [
    source 14
    target 132
  ]
  edge [
    source 14
    target 133
  ]
  edge [
    source 14
    target 134
  ]
  edge [
    source 14
    target 135
  ]
  edge [
    source 14
    target 136
  ]
]
