graph [
  maxDegree 13
  minDegree 1
  meanDegree 1.9444444444444444
  density 0.05555555555555555
  graphCliqueNumber 2
  node [
    id 0
    label "w&#322;a&#347;nie"
    origin "text"
  ]
  node [
    id 1
    label "wej&#347;&#263;by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "mieszkanie"
    origin "text"
  ]
  node [
    id 3
    label "raz"
    origin "text"
  ]
  node [
    id 4
    label "ent"
    origin "text"
  ]
  node [
    id 5
    label "moi"
    origin "text"
  ]
  node [
    id 6
    label "wsp&#243;&#322;lokator"
    origin "text"
  ]
  node [
    id 7
    label "mimo"
    origin "text"
  ]
  node [
    id 8
    label "uprzejmy"
    origin "text"
  ]
  node [
    id 9
    label "pro&#347;ba"
    origin "text"
  ]
  node [
    id 10
    label "nie"
    origin "text"
  ]
  node [
    id 11
    label "dok&#322;adnie"
  ]
  node [
    id 12
    label "stanie"
  ]
  node [
    id 13
    label "przebywanie"
  ]
  node [
    id 14
    label "panowanie"
  ]
  node [
    id 15
    label "zajmowanie"
  ]
  node [
    id 16
    label "pomieszkanie"
  ]
  node [
    id 17
    label "adjustment"
  ]
  node [
    id 18
    label "modu&#322;_mieszkalny"
  ]
  node [
    id 19
    label "lokal"
  ]
  node [
    id 20
    label "kwadrat"
  ]
  node [
    id 21
    label "animation"
  ]
  node [
    id 22
    label "dom"
  ]
  node [
    id 23
    label "chwila"
  ]
  node [
    id 24
    label "uderzenie"
  ]
  node [
    id 25
    label "cios"
  ]
  node [
    id 26
    label "time"
  ]
  node [
    id 27
    label "wsp&#243;&#322;mieszkaniec"
  ]
  node [
    id 28
    label "dobrze_wychowany"
  ]
  node [
    id 29
    label "grzeczny"
  ]
  node [
    id 30
    label "sk&#322;onny"
  ]
  node [
    id 31
    label "uprzejmie"
  ]
  node [
    id 32
    label "mi&#322;y"
  ]
  node [
    id 33
    label "solicitation"
  ]
  node [
    id 34
    label "wypowied&#378;"
  ]
  node [
    id 35
    label "sprzeciw"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 28
  ]
  edge [
    source 8
    target 29
  ]
  edge [
    source 8
    target 30
  ]
  edge [
    source 8
    target 31
  ]
  edge [
    source 8
    target 32
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 33
  ]
  edge [
    source 9
    target 34
  ]
  edge [
    source 10
    target 35
  ]
]
