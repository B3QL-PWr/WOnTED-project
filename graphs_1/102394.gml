graph [
  maxDegree 24
  minDegree 1
  meanDegree 2.1132075471698113
  density 0.013374731311201338
  graphCliqueNumber 3
  node [
    id 0
    label "dobre"
    origin "text"
  ]
  node [
    id 1
    label "spos&#243;b"
    origin "text"
  ]
  node [
    id 2
    label "nadanie"
    origin "text"
  ]
  node [
    id 3
    label "nasze"
    origin "text"
  ]
  node [
    id 4
    label "model"
    origin "text"
  ]
  node [
    id 5
    label "szczypt"
    origin "text"
  ]
  node [
    id 6
    label "indywidualizm"
    origin "text"
  ]
  node [
    id 7
    label "by&#263;"
    origin "text"
  ]
  node [
    id 8
    label "u&#380;ycie"
    origin "text"
  ]
  node [
    id 9
    label "felga"
    origin "text"
  ]
  node [
    id 10
    label "unikalny"
    origin "text"
  ]
  node [
    id 11
    label "kolor"
    origin "text"
  ]
  node [
    id 12
    label "efekt"
    origin "text"
  ]
  node [
    id 13
    label "taki"
    origin "text"
  ]
  node [
    id 14
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 15
    label "uzyska&#263;"
    origin "text"
  ]
  node [
    id 16
    label "kupowa&#263;"
    origin "text"
  ]
  node [
    id 17
    label "bia&#322;a"
    origin "text"
  ]
  node [
    id 18
    label "farbowa&#263;"
    origin "text"
  ]
  node [
    id 19
    label "wybrany"
    origin "text"
  ]
  node [
    id 20
    label "dodatkowy"
    origin "text"
  ]
  node [
    id 21
    label "zysk"
    origin "text"
  ]
  node [
    id 22
    label "zazwyczaj"
    origin "text"
  ]
  node [
    id 23
    label "tani"
    origin "text"
  ]
  node [
    id 24
    label "chromowa&#263;"
    origin "text"
  ]
  node [
    id 25
    label "odpowiednik"
    origin "text"
  ]
  node [
    id 26
    label "zbi&#243;r"
  ]
  node [
    id 27
    label "tryb"
  ]
  node [
    id 28
    label "narz&#281;dzie"
  ]
  node [
    id 29
    label "nature"
  ]
  node [
    id 30
    label "czynno&#347;&#263;"
  ]
  node [
    id 31
    label "spowodowanie"
  ]
  node [
    id 32
    label "nazwanie"
  ]
  node [
    id 33
    label "akt"
  ]
  node [
    id 34
    label "broadcast"
  ]
  node [
    id 35
    label "denomination"
  ]
  node [
    id 36
    label "przyznanie"
  ]
  node [
    id 37
    label "zdarzenie_si&#281;"
  ]
  node [
    id 38
    label "przes&#322;anie"
  ]
  node [
    id 39
    label "typ"
  ]
  node [
    id 40
    label "cz&#322;owiek"
  ]
  node [
    id 41
    label "pozowa&#263;"
  ]
  node [
    id 42
    label "ideal"
  ]
  node [
    id 43
    label "matryca"
  ]
  node [
    id 44
    label "imitacja"
  ]
  node [
    id 45
    label "ruch"
  ]
  node [
    id 46
    label "motif"
  ]
  node [
    id 47
    label "pozowanie"
  ]
  node [
    id 48
    label "wz&#243;r"
  ]
  node [
    id 49
    label "miniatura"
  ]
  node [
    id 50
    label "prezenter"
  ]
  node [
    id 51
    label "facet"
  ]
  node [
    id 52
    label "orygina&#322;"
  ]
  node [
    id 53
    label "mildew"
  ]
  node [
    id 54
    label "zi&#243;&#322;ko"
  ]
  node [
    id 55
    label "adaptation"
  ]
  node [
    id 56
    label "koncepcja"
  ]
  node [
    id 57
    label "postawa"
  ]
  node [
    id 58
    label "doktryna_filozoficzna"
  ]
  node [
    id 59
    label "individuality"
  ]
  node [
    id 60
    label "si&#281;ga&#263;"
  ]
  node [
    id 61
    label "trwa&#263;"
  ]
  node [
    id 62
    label "obecno&#347;&#263;"
  ]
  node [
    id 63
    label "stan"
  ]
  node [
    id 64
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 65
    label "stand"
  ]
  node [
    id 66
    label "mie&#263;_miejsce"
  ]
  node [
    id 67
    label "uczestniczy&#263;"
  ]
  node [
    id 68
    label "chodzi&#263;"
  ]
  node [
    id 69
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 70
    label "equal"
  ]
  node [
    id 71
    label "use"
  ]
  node [
    id 72
    label "zrobienie"
  ]
  node [
    id 73
    label "doznanie"
  ]
  node [
    id 74
    label "stosowanie"
  ]
  node [
    id 75
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 76
    label "nau&#380;ywanie_si&#281;"
  ]
  node [
    id 77
    label "enjoyment"
  ]
  node [
    id 78
    label "u&#380;yteczny"
  ]
  node [
    id 79
    label "zabawa"
  ]
  node [
    id 80
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 81
    label "obr&#281;cz"
  ]
  node [
    id 82
    label "ko&#322;o"
  ]
  node [
    id 83
    label "pojedynczy"
  ]
  node [
    id 84
    label "osobny"
  ]
  node [
    id 85
    label "wyj&#261;tkowy"
  ]
  node [
    id 86
    label "unikatowo"
  ]
  node [
    id 87
    label "specyficzny"
  ]
  node [
    id 88
    label "unikalnie"
  ]
  node [
    id 89
    label "&#347;wieci&#263;"
  ]
  node [
    id 90
    label "prze&#322;ama&#263;_si&#281;"
  ]
  node [
    id 91
    label "prze&#322;amanie"
  ]
  node [
    id 92
    label "prze&#322;amywa&#263;"
  ]
  node [
    id 93
    label "prze&#322;amywa&#263;_si&#281;"
  ]
  node [
    id 94
    label "prze&#322;amywanie_si&#281;"
  ]
  node [
    id 95
    label "ubarwienie"
  ]
  node [
    id 96
    label "symbol"
  ]
  node [
    id 97
    label "prze&#322;amywanie"
  ]
  node [
    id 98
    label "struktura"
  ]
  node [
    id 99
    label "prze&#322;ama&#263;"
  ]
  node [
    id 100
    label "zblakn&#261;&#263;"
  ]
  node [
    id 101
    label "cecha"
  ]
  node [
    id 102
    label "liczba_kwantowa"
  ]
  node [
    id 103
    label "blakn&#261;&#263;"
  ]
  node [
    id 104
    label "zblakni&#281;cie"
  ]
  node [
    id 105
    label "poker"
  ]
  node [
    id 106
    label "&#347;wiecenie"
  ]
  node [
    id 107
    label "blakni&#281;cie"
  ]
  node [
    id 108
    label "prze&#322;amanie_si&#281;"
  ]
  node [
    id 109
    label "dzia&#322;anie"
  ]
  node [
    id 110
    label "impression"
  ]
  node [
    id 111
    label "robienie_wra&#380;enia"
  ]
  node [
    id 112
    label "wra&#380;enie"
  ]
  node [
    id 113
    label "przyczyna"
  ]
  node [
    id 114
    label "zrobienie_wra&#380;enia"
  ]
  node [
    id 115
    label "robi&#263;_wra&#380;enie"
  ]
  node [
    id 116
    label "&#347;rodek"
  ]
  node [
    id 117
    label "event"
  ]
  node [
    id 118
    label "zrobi&#263;_wra&#380;enie"
  ]
  node [
    id 119
    label "rezultat"
  ]
  node [
    id 120
    label "okre&#347;lony"
  ]
  node [
    id 121
    label "jaki&#347;"
  ]
  node [
    id 122
    label "uprawi&#263;"
  ]
  node [
    id 123
    label "gotowy"
  ]
  node [
    id 124
    label "might"
  ]
  node [
    id 125
    label "promocja"
  ]
  node [
    id 126
    label "give_birth"
  ]
  node [
    id 127
    label "wytworzy&#263;"
  ]
  node [
    id 128
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 129
    label "realize"
  ]
  node [
    id 130
    label "zrobi&#263;"
  ]
  node [
    id 131
    label "make"
  ]
  node [
    id 132
    label "get"
  ]
  node [
    id 133
    label "ustawia&#263;"
  ]
  node [
    id 134
    label "wierzy&#263;"
  ]
  node [
    id 135
    label "przyjmowa&#263;"
  ]
  node [
    id 136
    label "pozyskiwa&#263;"
  ]
  node [
    id 137
    label "gra&#263;"
  ]
  node [
    id 138
    label "kupywa&#263;"
  ]
  node [
    id 139
    label "uznawa&#263;"
  ]
  node [
    id 140
    label "bra&#263;"
  ]
  node [
    id 141
    label "tint"
  ]
  node [
    id 142
    label "shed_blood"
  ]
  node [
    id 143
    label "barwi&#263;"
  ]
  node [
    id 144
    label "krwawi&#263;"
  ]
  node [
    id 145
    label "najmilszy"
  ]
  node [
    id 146
    label "poboczny"
  ]
  node [
    id 147
    label "dodatkowo"
  ]
  node [
    id 148
    label "uboczny"
  ]
  node [
    id 149
    label "dobro"
  ]
  node [
    id 150
    label "zaleta"
  ]
  node [
    id 151
    label "doch&#243;d"
  ]
  node [
    id 152
    label "zwykle"
  ]
  node [
    id 153
    label "niedrogo"
  ]
  node [
    id 154
    label "tanio"
  ]
  node [
    id 155
    label "tandetny"
  ]
  node [
    id 156
    label "chrome"
  ]
  node [
    id 157
    label "metalizowa&#263;"
  ]
  node [
    id 158
    label "odmiana"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 21
  ]
  edge [
    source 7
    target 17
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 7
    target 22
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 14
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 25
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 17
  ]
  edge [
    source 9
    target 18
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 12
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 19
  ]
  edge [
    source 11
    target 20
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 11
    target 92
  ]
  edge [
    source 11
    target 93
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 11
    target 98
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 11
    target 100
  ]
  edge [
    source 11
    target 101
  ]
  edge [
    source 11
    target 102
  ]
  edge [
    source 11
    target 103
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 108
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 109
  ]
  edge [
    source 12
    target 39
  ]
  edge [
    source 12
    target 110
  ]
  edge [
    source 12
    target 111
  ]
  edge [
    source 12
    target 112
  ]
  edge [
    source 12
    target 113
  ]
  edge [
    source 12
    target 114
  ]
  edge [
    source 12
    target 115
  ]
  edge [
    source 12
    target 116
  ]
  edge [
    source 12
    target 117
  ]
  edge [
    source 12
    target 118
  ]
  edge [
    source 12
    target 119
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 120
  ]
  edge [
    source 13
    target 121
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 122
  ]
  edge [
    source 14
    target 123
  ]
  edge [
    source 14
    target 124
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 125
  ]
  edge [
    source 15
    target 126
  ]
  edge [
    source 15
    target 127
  ]
  edge [
    source 15
    target 128
  ]
  edge [
    source 15
    target 129
  ]
  edge [
    source 15
    target 130
  ]
  edge [
    source 15
    target 131
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 132
  ]
  edge [
    source 16
    target 133
  ]
  edge [
    source 16
    target 134
  ]
  edge [
    source 16
    target 135
  ]
  edge [
    source 16
    target 136
  ]
  edge [
    source 16
    target 137
  ]
  edge [
    source 16
    target 138
  ]
  edge [
    source 16
    target 139
  ]
  edge [
    source 16
    target 140
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 141
  ]
  edge [
    source 18
    target 142
  ]
  edge [
    source 18
    target 143
  ]
  edge [
    source 18
    target 144
  ]
  edge [
    source 19
    target 145
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 146
  ]
  edge [
    source 20
    target 147
  ]
  edge [
    source 20
    target 148
  ]
  edge [
    source 21
    target 149
  ]
  edge [
    source 21
    target 150
  ]
  edge [
    source 21
    target 151
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 152
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 153
  ]
  edge [
    source 23
    target 154
  ]
  edge [
    source 23
    target 155
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 156
  ]
  edge [
    source 24
    target 157
  ]
  edge [
    source 25
    target 158
  ]
]
