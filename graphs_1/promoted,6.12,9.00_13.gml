graph [
  maxDegree 217
  minDegree 1
  meanDegree 2.0155844155844154
  density 0.005248917748917749
  graphCliqueNumber 2
  node [
    id 0
    label "pierwsza"
    origin "text"
  ]
  node [
    id 1
    label "po&#322;owa"
    origin "text"
  ]
  node [
    id 2
    label "przysz&#322;y"
    origin "text"
  ]
  node [
    id 3
    label "rok"
    origin "text"
  ]
  node [
    id 4
    label "s&#261;d"
    origin "text"
  ]
  node [
    id 5
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 6
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 7
    label "skierowa&#263;"
    origin "text"
  ]
  node [
    id 8
    label "pozew"
    origin "text"
  ]
  node [
    id 9
    label "zbiorowy"
    origin "text"
  ]
  node [
    id 10
    label "przeciwko"
    origin "text"
  ]
  node [
    id 11
    label "skarb"
    origin "text"
  ]
  node [
    id 12
    label "pa&#324;stwo"
    origin "text"
  ]
  node [
    id 13
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 14
    label "ustali&#263;"
    origin "text"
  ]
  node [
    id 15
    label "odpowiedzialno&#347;&#263;"
    origin "text"
  ]
  node [
    id 16
    label "stan"
    origin "text"
  ]
  node [
    id 17
    label "powietrze"
    origin "text"
  ]
  node [
    id 18
    label "polska"
    origin "text"
  ]
  node [
    id 19
    label "godzina"
  ]
  node [
    id 20
    label "medium"
  ]
  node [
    id 21
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 22
    label "czas"
  ]
  node [
    id 23
    label "kolejny"
  ]
  node [
    id 24
    label "stulecie"
  ]
  node [
    id 25
    label "kalendarz"
  ]
  node [
    id 26
    label "pora_roku"
  ]
  node [
    id 27
    label "cykl_astronomiczny"
  ]
  node [
    id 28
    label "p&#243;&#322;rocze"
  ]
  node [
    id 29
    label "grupa"
  ]
  node [
    id 30
    label "kwarta&#322;"
  ]
  node [
    id 31
    label "kurs"
  ]
  node [
    id 32
    label "jubileusz"
  ]
  node [
    id 33
    label "miesi&#261;c"
  ]
  node [
    id 34
    label "lata"
  ]
  node [
    id 35
    label "martwy_sezon"
  ]
  node [
    id 36
    label "procesowicz"
  ]
  node [
    id 37
    label "wypowied&#378;"
  ]
  node [
    id 38
    label "pods&#261;dny"
  ]
  node [
    id 39
    label "podejrzany"
  ]
  node [
    id 40
    label "broni&#263;"
  ]
  node [
    id 41
    label "bronienie"
  ]
  node [
    id 42
    label "system"
  ]
  node [
    id 43
    label "my&#347;l"
  ]
  node [
    id 44
    label "wytw&#243;r"
  ]
  node [
    id 45
    label "urz&#261;d"
  ]
  node [
    id 46
    label "konektyw"
  ]
  node [
    id 47
    label "court"
  ]
  node [
    id 48
    label "obrona"
  ]
  node [
    id 49
    label "s&#261;downictwo"
  ]
  node [
    id 50
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 51
    label "forum"
  ]
  node [
    id 52
    label "zesp&#243;&#322;"
  ]
  node [
    id 53
    label "post&#281;powanie"
  ]
  node [
    id 54
    label "skazany"
  ]
  node [
    id 55
    label "wydarzenie"
  ]
  node [
    id 56
    label "&#347;wiadek"
  ]
  node [
    id 57
    label "antylogizm"
  ]
  node [
    id 58
    label "strona"
  ]
  node [
    id 59
    label "oskar&#380;yciel"
  ]
  node [
    id 60
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 61
    label "biuro"
  ]
  node [
    id 62
    label "instytucja"
  ]
  node [
    id 63
    label "czyj&#347;"
  ]
  node [
    id 64
    label "m&#261;&#380;"
  ]
  node [
    id 65
    label "proceed"
  ]
  node [
    id 66
    label "catch"
  ]
  node [
    id 67
    label "pozosta&#263;"
  ]
  node [
    id 68
    label "osta&#263;_si&#281;"
  ]
  node [
    id 69
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 70
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 71
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 72
    label "change"
  ]
  node [
    id 73
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 74
    label "return"
  ]
  node [
    id 75
    label "podpowiedzie&#263;"
  ]
  node [
    id 76
    label "direct"
  ]
  node [
    id 77
    label "dispatch"
  ]
  node [
    id 78
    label "przeznaczy&#263;"
  ]
  node [
    id 79
    label "ustawi&#263;"
  ]
  node [
    id 80
    label "wys&#322;a&#263;"
  ]
  node [
    id 81
    label "precede"
  ]
  node [
    id 82
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 83
    label "set"
  ]
  node [
    id 84
    label "wniosek"
  ]
  node [
    id 85
    label "pow&#243;dztwo"
  ]
  node [
    id 86
    label "zbiorowo"
  ]
  node [
    id 87
    label "wsp&#243;lny"
  ]
  node [
    id 88
    label "cz&#322;owiek"
  ]
  node [
    id 89
    label "przedmiot"
  ]
  node [
    id 90
    label "mienie"
  ]
  node [
    id 91
    label "kapita&#322;"
  ]
  node [
    id 92
    label "brylant"
  ]
  node [
    id 93
    label "Filipiny"
  ]
  node [
    id 94
    label "Rwanda"
  ]
  node [
    id 95
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 96
    label "Monako"
  ]
  node [
    id 97
    label "Korea"
  ]
  node [
    id 98
    label "Ghana"
  ]
  node [
    id 99
    label "Czarnog&#243;ra"
  ]
  node [
    id 100
    label "Malawi"
  ]
  node [
    id 101
    label "Indonezja"
  ]
  node [
    id 102
    label "Bu&#322;garia"
  ]
  node [
    id 103
    label "Nauru"
  ]
  node [
    id 104
    label "Kenia"
  ]
  node [
    id 105
    label "Kambod&#380;a"
  ]
  node [
    id 106
    label "Mali"
  ]
  node [
    id 107
    label "Austria"
  ]
  node [
    id 108
    label "interior"
  ]
  node [
    id 109
    label "Armenia"
  ]
  node [
    id 110
    label "Fid&#380;i"
  ]
  node [
    id 111
    label "Tuwalu"
  ]
  node [
    id 112
    label "Etiopia"
  ]
  node [
    id 113
    label "Malta"
  ]
  node [
    id 114
    label "Malezja"
  ]
  node [
    id 115
    label "Grenada"
  ]
  node [
    id 116
    label "Tad&#380;ykistan"
  ]
  node [
    id 117
    label "Wehrlen"
  ]
  node [
    id 118
    label "para"
  ]
  node [
    id 119
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 120
    label "Rumunia"
  ]
  node [
    id 121
    label "Maroko"
  ]
  node [
    id 122
    label "Bhutan"
  ]
  node [
    id 123
    label "S&#322;owacja"
  ]
  node [
    id 124
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 125
    label "Seszele"
  ]
  node [
    id 126
    label "Kuwejt"
  ]
  node [
    id 127
    label "Arabia_Saudyjska"
  ]
  node [
    id 128
    label "Ekwador"
  ]
  node [
    id 129
    label "Kanada"
  ]
  node [
    id 130
    label "Japonia"
  ]
  node [
    id 131
    label "ziemia"
  ]
  node [
    id 132
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 133
    label "Hiszpania"
  ]
  node [
    id 134
    label "Wyspy_Marshalla"
  ]
  node [
    id 135
    label "Botswana"
  ]
  node [
    id 136
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 137
    label "D&#380;ibuti"
  ]
  node [
    id 138
    label "Wietnam"
  ]
  node [
    id 139
    label "Egipt"
  ]
  node [
    id 140
    label "Burkina_Faso"
  ]
  node [
    id 141
    label "Niemcy"
  ]
  node [
    id 142
    label "Khitai"
  ]
  node [
    id 143
    label "Macedonia"
  ]
  node [
    id 144
    label "Albania"
  ]
  node [
    id 145
    label "Madagaskar"
  ]
  node [
    id 146
    label "Bahrajn"
  ]
  node [
    id 147
    label "Jemen"
  ]
  node [
    id 148
    label "Lesoto"
  ]
  node [
    id 149
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 150
    label "Samoa"
  ]
  node [
    id 151
    label "Andora"
  ]
  node [
    id 152
    label "Chiny"
  ]
  node [
    id 153
    label "Cypr"
  ]
  node [
    id 154
    label "Wielka_Brytania"
  ]
  node [
    id 155
    label "Ukraina"
  ]
  node [
    id 156
    label "Paragwaj"
  ]
  node [
    id 157
    label "Trynidad_i_Tobago"
  ]
  node [
    id 158
    label "Libia"
  ]
  node [
    id 159
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 160
    label "Surinam"
  ]
  node [
    id 161
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 162
    label "Australia"
  ]
  node [
    id 163
    label "Nigeria"
  ]
  node [
    id 164
    label "Honduras"
  ]
  node [
    id 165
    label "Bangladesz"
  ]
  node [
    id 166
    label "Peru"
  ]
  node [
    id 167
    label "Kazachstan"
  ]
  node [
    id 168
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 169
    label "Irak"
  ]
  node [
    id 170
    label "holoarktyka"
  ]
  node [
    id 171
    label "USA"
  ]
  node [
    id 172
    label "Sudan"
  ]
  node [
    id 173
    label "Nepal"
  ]
  node [
    id 174
    label "San_Marino"
  ]
  node [
    id 175
    label "Burundi"
  ]
  node [
    id 176
    label "Dominikana"
  ]
  node [
    id 177
    label "Komory"
  ]
  node [
    id 178
    label "granica_pa&#324;stwa"
  ]
  node [
    id 179
    label "Gwatemala"
  ]
  node [
    id 180
    label "Antarktis"
  ]
  node [
    id 181
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 182
    label "Brunei"
  ]
  node [
    id 183
    label "Iran"
  ]
  node [
    id 184
    label "Zimbabwe"
  ]
  node [
    id 185
    label "Namibia"
  ]
  node [
    id 186
    label "Meksyk"
  ]
  node [
    id 187
    label "Kamerun"
  ]
  node [
    id 188
    label "zwrot"
  ]
  node [
    id 189
    label "Somalia"
  ]
  node [
    id 190
    label "Angola"
  ]
  node [
    id 191
    label "Gabon"
  ]
  node [
    id 192
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 193
    label "Mozambik"
  ]
  node [
    id 194
    label "Tajwan"
  ]
  node [
    id 195
    label "Tunezja"
  ]
  node [
    id 196
    label "Nowa_Zelandia"
  ]
  node [
    id 197
    label "Liban"
  ]
  node [
    id 198
    label "Jordania"
  ]
  node [
    id 199
    label "Tonga"
  ]
  node [
    id 200
    label "Czad"
  ]
  node [
    id 201
    label "Liberia"
  ]
  node [
    id 202
    label "Gwinea"
  ]
  node [
    id 203
    label "Belize"
  ]
  node [
    id 204
    label "&#321;otwa"
  ]
  node [
    id 205
    label "Syria"
  ]
  node [
    id 206
    label "Benin"
  ]
  node [
    id 207
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 208
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 209
    label "Dominika"
  ]
  node [
    id 210
    label "Antigua_i_Barbuda"
  ]
  node [
    id 211
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 212
    label "Hanower"
  ]
  node [
    id 213
    label "partia"
  ]
  node [
    id 214
    label "Afganistan"
  ]
  node [
    id 215
    label "Kiribati"
  ]
  node [
    id 216
    label "W&#322;ochy"
  ]
  node [
    id 217
    label "Szwajcaria"
  ]
  node [
    id 218
    label "Sahara_Zachodnia"
  ]
  node [
    id 219
    label "Chorwacja"
  ]
  node [
    id 220
    label "Tajlandia"
  ]
  node [
    id 221
    label "Salwador"
  ]
  node [
    id 222
    label "Bahamy"
  ]
  node [
    id 223
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 224
    label "S&#322;owenia"
  ]
  node [
    id 225
    label "Gambia"
  ]
  node [
    id 226
    label "Urugwaj"
  ]
  node [
    id 227
    label "Zair"
  ]
  node [
    id 228
    label "Erytrea"
  ]
  node [
    id 229
    label "Rosja"
  ]
  node [
    id 230
    label "Uganda"
  ]
  node [
    id 231
    label "Niger"
  ]
  node [
    id 232
    label "Mauritius"
  ]
  node [
    id 233
    label "Turkmenistan"
  ]
  node [
    id 234
    label "Turcja"
  ]
  node [
    id 235
    label "Irlandia"
  ]
  node [
    id 236
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 237
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 238
    label "Gwinea_Bissau"
  ]
  node [
    id 239
    label "Belgia"
  ]
  node [
    id 240
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 241
    label "Palau"
  ]
  node [
    id 242
    label "Barbados"
  ]
  node [
    id 243
    label "Chile"
  ]
  node [
    id 244
    label "Wenezuela"
  ]
  node [
    id 245
    label "W&#281;gry"
  ]
  node [
    id 246
    label "Argentyna"
  ]
  node [
    id 247
    label "Kolumbia"
  ]
  node [
    id 248
    label "Sierra_Leone"
  ]
  node [
    id 249
    label "Azerbejd&#380;an"
  ]
  node [
    id 250
    label "Kongo"
  ]
  node [
    id 251
    label "Pakistan"
  ]
  node [
    id 252
    label "Liechtenstein"
  ]
  node [
    id 253
    label "Nikaragua"
  ]
  node [
    id 254
    label "Senegal"
  ]
  node [
    id 255
    label "Indie"
  ]
  node [
    id 256
    label "Suazi"
  ]
  node [
    id 257
    label "Polska"
  ]
  node [
    id 258
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 259
    label "Algieria"
  ]
  node [
    id 260
    label "terytorium"
  ]
  node [
    id 261
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 262
    label "Jamajka"
  ]
  node [
    id 263
    label "Kostaryka"
  ]
  node [
    id 264
    label "Timor_Wschodni"
  ]
  node [
    id 265
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 266
    label "Kuba"
  ]
  node [
    id 267
    label "Mauretania"
  ]
  node [
    id 268
    label "Portoryko"
  ]
  node [
    id 269
    label "Brazylia"
  ]
  node [
    id 270
    label "Mo&#322;dawia"
  ]
  node [
    id 271
    label "organizacja"
  ]
  node [
    id 272
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 273
    label "Litwa"
  ]
  node [
    id 274
    label "Kirgistan"
  ]
  node [
    id 275
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 276
    label "Izrael"
  ]
  node [
    id 277
    label "Grecja"
  ]
  node [
    id 278
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 279
    label "Holandia"
  ]
  node [
    id 280
    label "Sri_Lanka"
  ]
  node [
    id 281
    label "Katar"
  ]
  node [
    id 282
    label "Mikronezja"
  ]
  node [
    id 283
    label "Mongolia"
  ]
  node [
    id 284
    label "Laos"
  ]
  node [
    id 285
    label "Malediwy"
  ]
  node [
    id 286
    label "Zambia"
  ]
  node [
    id 287
    label "Tanzania"
  ]
  node [
    id 288
    label "Gujana"
  ]
  node [
    id 289
    label "Czechy"
  ]
  node [
    id 290
    label "Panama"
  ]
  node [
    id 291
    label "Uzbekistan"
  ]
  node [
    id 292
    label "Gruzja"
  ]
  node [
    id 293
    label "Serbia"
  ]
  node [
    id 294
    label "Francja"
  ]
  node [
    id 295
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 296
    label "Togo"
  ]
  node [
    id 297
    label "Estonia"
  ]
  node [
    id 298
    label "Oman"
  ]
  node [
    id 299
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 300
    label "Portugalia"
  ]
  node [
    id 301
    label "Boliwia"
  ]
  node [
    id 302
    label "Luksemburg"
  ]
  node [
    id 303
    label "Haiti"
  ]
  node [
    id 304
    label "Wyspy_Salomona"
  ]
  node [
    id 305
    label "Birma"
  ]
  node [
    id 306
    label "Rodezja"
  ]
  node [
    id 307
    label "umocni&#263;"
  ]
  node [
    id 308
    label "bind"
  ]
  node [
    id 309
    label "zdecydowa&#263;"
  ]
  node [
    id 310
    label "spowodowa&#263;"
  ]
  node [
    id 311
    label "unwrap"
  ]
  node [
    id 312
    label "zrobi&#263;"
  ]
  node [
    id 313
    label "put"
  ]
  node [
    id 314
    label "wstyd"
  ]
  node [
    id 315
    label "konsekwencja"
  ]
  node [
    id 316
    label "cecha"
  ]
  node [
    id 317
    label "guilt"
  ]
  node [
    id 318
    label "liability"
  ]
  node [
    id 319
    label "obarczy&#263;"
  ]
  node [
    id 320
    label "obowi&#261;zek"
  ]
  node [
    id 321
    label "Arizona"
  ]
  node [
    id 322
    label "Georgia"
  ]
  node [
    id 323
    label "warstwa"
  ]
  node [
    id 324
    label "jednostka_administracyjna"
  ]
  node [
    id 325
    label "Hawaje"
  ]
  node [
    id 326
    label "Goa"
  ]
  node [
    id 327
    label "Floryda"
  ]
  node [
    id 328
    label "Oklahoma"
  ]
  node [
    id 329
    label "punkt"
  ]
  node [
    id 330
    label "Alaska"
  ]
  node [
    id 331
    label "wci&#281;cie"
  ]
  node [
    id 332
    label "Alabama"
  ]
  node [
    id 333
    label "Oregon"
  ]
  node [
    id 334
    label "poziom"
  ]
  node [
    id 335
    label "by&#263;"
  ]
  node [
    id 336
    label "Teksas"
  ]
  node [
    id 337
    label "Illinois"
  ]
  node [
    id 338
    label "Waszyngton"
  ]
  node [
    id 339
    label "Jukatan"
  ]
  node [
    id 340
    label "shape"
  ]
  node [
    id 341
    label "Nowy_Meksyk"
  ]
  node [
    id 342
    label "ilo&#347;&#263;"
  ]
  node [
    id 343
    label "state"
  ]
  node [
    id 344
    label "Nowy_York"
  ]
  node [
    id 345
    label "Arakan"
  ]
  node [
    id 346
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 347
    label "Kalifornia"
  ]
  node [
    id 348
    label "wektor"
  ]
  node [
    id 349
    label "Massachusetts"
  ]
  node [
    id 350
    label "miejsce"
  ]
  node [
    id 351
    label "Pensylwania"
  ]
  node [
    id 352
    label "Michigan"
  ]
  node [
    id 353
    label "Maryland"
  ]
  node [
    id 354
    label "Ohio"
  ]
  node [
    id 355
    label "Kansas"
  ]
  node [
    id 356
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 357
    label "Luizjana"
  ]
  node [
    id 358
    label "samopoczucie"
  ]
  node [
    id 359
    label "Wirginia"
  ]
  node [
    id 360
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 361
    label "mieszanina"
  ]
  node [
    id 362
    label "przewietrzy&#263;"
  ]
  node [
    id 363
    label "przewietrza&#263;"
  ]
  node [
    id 364
    label "tlen"
  ]
  node [
    id 365
    label "eter"
  ]
  node [
    id 366
    label "dmucha&#263;"
  ]
  node [
    id 367
    label "dmuchanie"
  ]
  node [
    id 368
    label "breeze"
  ]
  node [
    id 369
    label "pojazd"
  ]
  node [
    id 370
    label "pneumatyczny"
  ]
  node [
    id 371
    label "wydychanie"
  ]
  node [
    id 372
    label "podgrzew"
  ]
  node [
    id 373
    label "wdychanie"
  ]
  node [
    id 374
    label "luft"
  ]
  node [
    id 375
    label "geosystem"
  ]
  node [
    id 376
    label "dmuchn&#261;&#263;"
  ]
  node [
    id 377
    label "dmuchni&#281;cie"
  ]
  node [
    id 378
    label "&#380;ywio&#322;"
  ]
  node [
    id 379
    label "wdycha&#263;"
  ]
  node [
    id 380
    label "wydycha&#263;"
  ]
  node [
    id 381
    label "napowietrzy&#263;"
  ]
  node [
    id 382
    label "front"
  ]
  node [
    id 383
    label "przewietrzenie"
  ]
  node [
    id 384
    label "przewietrzanie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 13
  ]
  edge [
    source 5
    target 14
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 18
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 88
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 11
    target 92
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 12
    target 94
  ]
  edge [
    source 12
    target 95
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 12
    target 107
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 12
    target 109
  ]
  edge [
    source 12
    target 110
  ]
  edge [
    source 12
    target 111
  ]
  edge [
    source 12
    target 112
  ]
  edge [
    source 12
    target 113
  ]
  edge [
    source 12
    target 114
  ]
  edge [
    source 12
    target 115
  ]
  edge [
    source 12
    target 116
  ]
  edge [
    source 12
    target 117
  ]
  edge [
    source 12
    target 118
  ]
  edge [
    source 12
    target 119
  ]
  edge [
    source 12
    target 120
  ]
  edge [
    source 12
    target 121
  ]
  edge [
    source 12
    target 122
  ]
  edge [
    source 12
    target 123
  ]
  edge [
    source 12
    target 124
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 12
    target 131
  ]
  edge [
    source 12
    target 132
  ]
  edge [
    source 12
    target 133
  ]
  edge [
    source 12
    target 134
  ]
  edge [
    source 12
    target 135
  ]
  edge [
    source 12
    target 136
  ]
  edge [
    source 12
    target 137
  ]
  edge [
    source 12
    target 29
  ]
  edge [
    source 12
    target 138
  ]
  edge [
    source 12
    target 139
  ]
  edge [
    source 12
    target 140
  ]
  edge [
    source 12
    target 141
  ]
  edge [
    source 12
    target 142
  ]
  edge [
    source 12
    target 143
  ]
  edge [
    source 12
    target 144
  ]
  edge [
    source 12
    target 145
  ]
  edge [
    source 12
    target 146
  ]
  edge [
    source 12
    target 147
  ]
  edge [
    source 12
    target 148
  ]
  edge [
    source 12
    target 149
  ]
  edge [
    source 12
    target 150
  ]
  edge [
    source 12
    target 151
  ]
  edge [
    source 12
    target 152
  ]
  edge [
    source 12
    target 153
  ]
  edge [
    source 12
    target 154
  ]
  edge [
    source 12
    target 155
  ]
  edge [
    source 12
    target 156
  ]
  edge [
    source 12
    target 157
  ]
  edge [
    source 12
    target 158
  ]
  edge [
    source 12
    target 159
  ]
  edge [
    source 12
    target 160
  ]
  edge [
    source 12
    target 161
  ]
  edge [
    source 12
    target 162
  ]
  edge [
    source 12
    target 163
  ]
  edge [
    source 12
    target 164
  ]
  edge [
    source 12
    target 165
  ]
  edge [
    source 12
    target 166
  ]
  edge [
    source 12
    target 167
  ]
  edge [
    source 12
    target 168
  ]
  edge [
    source 12
    target 169
  ]
  edge [
    source 12
    target 170
  ]
  edge [
    source 12
    target 171
  ]
  edge [
    source 12
    target 172
  ]
  edge [
    source 12
    target 173
  ]
  edge [
    source 12
    target 174
  ]
  edge [
    source 12
    target 175
  ]
  edge [
    source 12
    target 176
  ]
  edge [
    source 12
    target 177
  ]
  edge [
    source 12
    target 178
  ]
  edge [
    source 12
    target 179
  ]
  edge [
    source 12
    target 180
  ]
  edge [
    source 12
    target 181
  ]
  edge [
    source 12
    target 182
  ]
  edge [
    source 12
    target 183
  ]
  edge [
    source 12
    target 184
  ]
  edge [
    source 12
    target 185
  ]
  edge [
    source 12
    target 186
  ]
  edge [
    source 12
    target 187
  ]
  edge [
    source 12
    target 188
  ]
  edge [
    source 12
    target 189
  ]
  edge [
    source 12
    target 190
  ]
  edge [
    source 12
    target 191
  ]
  edge [
    source 12
    target 192
  ]
  edge [
    source 12
    target 193
  ]
  edge [
    source 12
    target 194
  ]
  edge [
    source 12
    target 195
  ]
  edge [
    source 12
    target 196
  ]
  edge [
    source 12
    target 197
  ]
  edge [
    source 12
    target 198
  ]
  edge [
    source 12
    target 199
  ]
  edge [
    source 12
    target 200
  ]
  edge [
    source 12
    target 201
  ]
  edge [
    source 12
    target 202
  ]
  edge [
    source 12
    target 203
  ]
  edge [
    source 12
    target 204
  ]
  edge [
    source 12
    target 205
  ]
  edge [
    source 12
    target 206
  ]
  edge [
    source 12
    target 207
  ]
  edge [
    source 12
    target 208
  ]
  edge [
    source 12
    target 209
  ]
  edge [
    source 12
    target 210
  ]
  edge [
    source 12
    target 211
  ]
  edge [
    source 12
    target 212
  ]
  edge [
    source 12
    target 213
  ]
  edge [
    source 12
    target 214
  ]
  edge [
    source 12
    target 215
  ]
  edge [
    source 12
    target 216
  ]
  edge [
    source 12
    target 217
  ]
  edge [
    source 12
    target 218
  ]
  edge [
    source 12
    target 219
  ]
  edge [
    source 12
    target 220
  ]
  edge [
    source 12
    target 221
  ]
  edge [
    source 12
    target 222
  ]
  edge [
    source 12
    target 223
  ]
  edge [
    source 12
    target 224
  ]
  edge [
    source 12
    target 225
  ]
  edge [
    source 12
    target 226
  ]
  edge [
    source 12
    target 227
  ]
  edge [
    source 12
    target 228
  ]
  edge [
    source 12
    target 229
  ]
  edge [
    source 12
    target 230
  ]
  edge [
    source 12
    target 231
  ]
  edge [
    source 12
    target 232
  ]
  edge [
    source 12
    target 233
  ]
  edge [
    source 12
    target 234
  ]
  edge [
    source 12
    target 235
  ]
  edge [
    source 12
    target 236
  ]
  edge [
    source 12
    target 237
  ]
  edge [
    source 12
    target 238
  ]
  edge [
    source 12
    target 239
  ]
  edge [
    source 12
    target 240
  ]
  edge [
    source 12
    target 241
  ]
  edge [
    source 12
    target 242
  ]
  edge [
    source 12
    target 243
  ]
  edge [
    source 12
    target 244
  ]
  edge [
    source 12
    target 245
  ]
  edge [
    source 12
    target 246
  ]
  edge [
    source 12
    target 247
  ]
  edge [
    source 12
    target 248
  ]
  edge [
    source 12
    target 249
  ]
  edge [
    source 12
    target 250
  ]
  edge [
    source 12
    target 251
  ]
  edge [
    source 12
    target 252
  ]
  edge [
    source 12
    target 253
  ]
  edge [
    source 12
    target 254
  ]
  edge [
    source 12
    target 255
  ]
  edge [
    source 12
    target 256
  ]
  edge [
    source 12
    target 257
  ]
  edge [
    source 12
    target 258
  ]
  edge [
    source 12
    target 259
  ]
  edge [
    source 12
    target 260
  ]
  edge [
    source 12
    target 261
  ]
  edge [
    source 12
    target 262
  ]
  edge [
    source 12
    target 263
  ]
  edge [
    source 12
    target 264
  ]
  edge [
    source 12
    target 265
  ]
  edge [
    source 12
    target 266
  ]
  edge [
    source 12
    target 267
  ]
  edge [
    source 12
    target 268
  ]
  edge [
    source 12
    target 269
  ]
  edge [
    source 12
    target 270
  ]
  edge [
    source 12
    target 271
  ]
  edge [
    source 12
    target 272
  ]
  edge [
    source 12
    target 273
  ]
  edge [
    source 12
    target 274
  ]
  edge [
    source 12
    target 275
  ]
  edge [
    source 12
    target 276
  ]
  edge [
    source 12
    target 277
  ]
  edge [
    source 12
    target 278
  ]
  edge [
    source 12
    target 279
  ]
  edge [
    source 12
    target 280
  ]
  edge [
    source 12
    target 281
  ]
  edge [
    source 12
    target 282
  ]
  edge [
    source 12
    target 283
  ]
  edge [
    source 12
    target 284
  ]
  edge [
    source 12
    target 285
  ]
  edge [
    source 12
    target 286
  ]
  edge [
    source 12
    target 287
  ]
  edge [
    source 12
    target 288
  ]
  edge [
    source 12
    target 289
  ]
  edge [
    source 12
    target 290
  ]
  edge [
    source 12
    target 291
  ]
  edge [
    source 12
    target 292
  ]
  edge [
    source 12
    target 293
  ]
  edge [
    source 12
    target 294
  ]
  edge [
    source 12
    target 295
  ]
  edge [
    source 12
    target 296
  ]
  edge [
    source 12
    target 297
  ]
  edge [
    source 12
    target 298
  ]
  edge [
    source 12
    target 299
  ]
  edge [
    source 12
    target 300
  ]
  edge [
    source 12
    target 301
  ]
  edge [
    source 12
    target 302
  ]
  edge [
    source 12
    target 303
  ]
  edge [
    source 12
    target 304
  ]
  edge [
    source 12
    target 305
  ]
  edge [
    source 12
    target 306
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 307
  ]
  edge [
    source 14
    target 308
  ]
  edge [
    source 14
    target 309
  ]
  edge [
    source 14
    target 310
  ]
  edge [
    source 14
    target 311
  ]
  edge [
    source 14
    target 312
  ]
  edge [
    source 14
    target 313
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 314
  ]
  edge [
    source 15
    target 315
  ]
  edge [
    source 15
    target 316
  ]
  edge [
    source 15
    target 317
  ]
  edge [
    source 15
    target 318
  ]
  edge [
    source 15
    target 319
  ]
  edge [
    source 15
    target 320
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 321
  ]
  edge [
    source 16
    target 322
  ]
  edge [
    source 16
    target 323
  ]
  edge [
    source 16
    target 324
  ]
  edge [
    source 16
    target 325
  ]
  edge [
    source 16
    target 326
  ]
  edge [
    source 16
    target 327
  ]
  edge [
    source 16
    target 328
  ]
  edge [
    source 16
    target 329
  ]
  edge [
    source 16
    target 330
  ]
  edge [
    source 16
    target 331
  ]
  edge [
    source 16
    target 332
  ]
  edge [
    source 16
    target 333
  ]
  edge [
    source 16
    target 334
  ]
  edge [
    source 16
    target 335
  ]
  edge [
    source 16
    target 336
  ]
  edge [
    source 16
    target 337
  ]
  edge [
    source 16
    target 338
  ]
  edge [
    source 16
    target 339
  ]
  edge [
    source 16
    target 340
  ]
  edge [
    source 16
    target 341
  ]
  edge [
    source 16
    target 342
  ]
  edge [
    source 16
    target 343
  ]
  edge [
    source 16
    target 344
  ]
  edge [
    source 16
    target 345
  ]
  edge [
    source 16
    target 346
  ]
  edge [
    source 16
    target 347
  ]
  edge [
    source 16
    target 348
  ]
  edge [
    source 16
    target 349
  ]
  edge [
    source 16
    target 350
  ]
  edge [
    source 16
    target 351
  ]
  edge [
    source 16
    target 352
  ]
  edge [
    source 16
    target 353
  ]
  edge [
    source 16
    target 354
  ]
  edge [
    source 16
    target 355
  ]
  edge [
    source 16
    target 356
  ]
  edge [
    source 16
    target 357
  ]
  edge [
    source 16
    target 358
  ]
  edge [
    source 16
    target 359
  ]
  edge [
    source 16
    target 360
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 361
  ]
  edge [
    source 17
    target 362
  ]
  edge [
    source 17
    target 363
  ]
  edge [
    source 17
    target 364
  ]
  edge [
    source 17
    target 365
  ]
  edge [
    source 17
    target 366
  ]
  edge [
    source 17
    target 367
  ]
  edge [
    source 17
    target 368
  ]
  edge [
    source 17
    target 369
  ]
  edge [
    source 17
    target 370
  ]
  edge [
    source 17
    target 371
  ]
  edge [
    source 17
    target 372
  ]
  edge [
    source 17
    target 373
  ]
  edge [
    source 17
    target 374
  ]
  edge [
    source 17
    target 375
  ]
  edge [
    source 17
    target 376
  ]
  edge [
    source 17
    target 377
  ]
  edge [
    source 17
    target 378
  ]
  edge [
    source 17
    target 379
  ]
  edge [
    source 17
    target 380
  ]
  edge [
    source 17
    target 381
  ]
  edge [
    source 17
    target 382
  ]
  edge [
    source 17
    target 383
  ]
  edge [
    source 17
    target 384
  ]
]
