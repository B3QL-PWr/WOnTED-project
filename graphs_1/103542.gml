graph [
  maxDegree 20
  minDegree 1
  meanDegree 2
  density 0.044444444444444446
  graphCliqueNumber 3
  node [
    id 0
    label "adam"
    origin "text"
  ]
  node [
    id 1
    label "nawet"
    origin "text"
  ]
  node [
    id 2
    label "&#347;ciana"
    origin "text"
  ]
  node [
    id 3
    label "sufit"
    origin "text"
  ]
  node [
    id 4
    label "biega&#263;"
    origin "text"
  ]
  node [
    id 5
    label "co&#347;"
    origin "text"
  ]
  node [
    id 6
    label "taki"
    origin "text"
  ]
  node [
    id 7
    label "przegroda"
  ]
  node [
    id 8
    label "trudno&#347;&#263;"
  ]
  node [
    id 9
    label "miejsce"
  ]
  node [
    id 10
    label "bariera"
  ]
  node [
    id 11
    label "profil"
  ]
  node [
    id 12
    label "zbocze"
  ]
  node [
    id 13
    label "kres"
  ]
  node [
    id 14
    label "wielo&#347;cian"
  ]
  node [
    id 15
    label "wyrobisko"
  ]
  node [
    id 16
    label "facebook"
  ]
  node [
    id 17
    label "pow&#322;oka"
  ]
  node [
    id 18
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 19
    label "obstruction"
  ]
  node [
    id 20
    label "kszta&#322;t"
  ]
  node [
    id 21
    label "p&#322;aszczyzna"
  ]
  node [
    id 22
    label "kaseton"
  ]
  node [
    id 23
    label "pomieszczenie"
  ]
  node [
    id 24
    label "cieka&#263;"
  ]
  node [
    id 25
    label "dash"
  ]
  node [
    id 26
    label "up&#281;dza&#263;_si&#281;"
  ]
  node [
    id 27
    label "rush"
  ]
  node [
    id 28
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 29
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 30
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 31
    label "zaleca&#263;_si&#281;"
  ]
  node [
    id 32
    label "ucieka&#263;"
  ]
  node [
    id 33
    label "uprawia&#263;"
  ]
  node [
    id 34
    label "rozwija&#263;_si&#281;"
  ]
  node [
    id 35
    label "hula&#263;"
  ]
  node [
    id 36
    label "chodzi&#263;"
  ]
  node [
    id 37
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 38
    label "rozwolnienie"
  ]
  node [
    id 39
    label "biec"
  ]
  node [
    id 40
    label "chorowa&#263;"
  ]
  node [
    id 41
    label "&#347;ciga&#263;_si&#281;"
  ]
  node [
    id 42
    label "thing"
  ]
  node [
    id 43
    label "cosik"
  ]
  node [
    id 44
    label "okre&#347;lony"
  ]
  node [
    id 45
    label "jaki&#347;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
]
