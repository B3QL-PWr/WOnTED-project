graph [
  maxDegree 27
  minDegree 1
  meanDegree 1.9642857142857142
  density 0.03571428571428571
  graphCliqueNumber 2
  node [
    id 0
    label "reprezentowa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "interes"
    origin "text"
  ]
  node [
    id 2
    label "mieszkaniec"
    origin "text"
  ]
  node [
    id 3
    label "sp&#243;&#322;dzielnia"
    origin "text"
  ]
  node [
    id 4
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 5
    label "znika&#263;"
    origin "text"
  ]
  node [
    id 6
    label "milion"
    origin "text"
  ]
  node [
    id 7
    label "act"
  ]
  node [
    id 8
    label "sprawowa&#263;"
  ]
  node [
    id 9
    label "wyra&#380;a&#263;"
  ]
  node [
    id 10
    label "represent"
  ]
  node [
    id 11
    label "Hortex"
  ]
  node [
    id 12
    label "MAC"
  ]
  node [
    id 13
    label "reengineering"
  ]
  node [
    id 14
    label "podmiot_gospodarczy"
  ]
  node [
    id 15
    label "dobro"
  ]
  node [
    id 16
    label "Google"
  ]
  node [
    id 17
    label "zaleta"
  ]
  node [
    id 18
    label "networking"
  ]
  node [
    id 19
    label "zasoby_ludzkie"
  ]
  node [
    id 20
    label "przedsi&#281;wzi&#281;cie"
  ]
  node [
    id 21
    label "Canon"
  ]
  node [
    id 22
    label "object"
  ]
  node [
    id 23
    label "HP"
  ]
  node [
    id 24
    label "Baltona"
  ]
  node [
    id 25
    label "firma"
  ]
  node [
    id 26
    label "Pewex"
  ]
  node [
    id 27
    label "MAN_SE"
  ]
  node [
    id 28
    label "Apeks"
  ]
  node [
    id 29
    label "zasoby"
  ]
  node [
    id 30
    label "Orbis"
  ]
  node [
    id 31
    label "Spo&#322;em"
  ]
  node [
    id 32
    label "sprawa"
  ]
  node [
    id 33
    label "przedsi&#281;biorczo&#347;&#263;"
  ]
  node [
    id 34
    label "Orlen"
  ]
  node [
    id 35
    label "penis"
  ]
  node [
    id 36
    label "cz&#322;owiek"
  ]
  node [
    id 37
    label "ludno&#347;&#263;"
  ]
  node [
    id 38
    label "zwierz&#281;"
  ]
  node [
    id 39
    label "kooperatywa"
  ]
  node [
    id 40
    label "GS"
  ]
  node [
    id 41
    label "stowarzyszenie"
  ]
  node [
    id 42
    label "collective"
  ]
  node [
    id 43
    label "siedziba"
  ]
  node [
    id 44
    label "wychodzi&#263;"
  ]
  node [
    id 45
    label "decrease"
  ]
  node [
    id 46
    label "przepada&#263;"
  ]
  node [
    id 47
    label "kamforowy"
  ]
  node [
    id 48
    label "shrink"
  ]
  node [
    id 49
    label "gin&#261;&#263;"
  ]
  node [
    id 50
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 51
    label "die"
  ]
  node [
    id 52
    label "leak"
  ]
  node [
    id 53
    label "liczba"
  ]
  node [
    id 54
    label "miljon"
  ]
  node [
    id 55
    label "ba&#324;ka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
]
