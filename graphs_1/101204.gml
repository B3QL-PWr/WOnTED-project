graph [
  maxDegree 72
  minDegree 1
  meanDegree 2.0246913580246915
  density 0.025308641975308643
  graphCliqueNumber 4
  node [
    id 0
    label "puchar"
    origin "text"
  ]
  node [
    id 1
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 2
    label "wio&#347;larstwo"
    origin "text"
  ]
  node [
    id 3
    label "zawarto&#347;&#263;"
  ]
  node [
    id 4
    label "zawody"
  ]
  node [
    id 5
    label "zwyci&#281;stwo"
  ]
  node [
    id 6
    label "naczynie"
  ]
  node [
    id 7
    label "nagroda"
  ]
  node [
    id 8
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 9
    label "obszar"
  ]
  node [
    id 10
    label "obiekt_naturalny"
  ]
  node [
    id 11
    label "przedmiot"
  ]
  node [
    id 12
    label "Stary_&#346;wiat"
  ]
  node [
    id 13
    label "grupa"
  ]
  node [
    id 14
    label "stw&#243;r"
  ]
  node [
    id 15
    label "biosfera"
  ]
  node [
    id 16
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 17
    label "rzecz"
  ]
  node [
    id 18
    label "magnetosfera"
  ]
  node [
    id 19
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 20
    label "environment"
  ]
  node [
    id 21
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 22
    label "geosfera"
  ]
  node [
    id 23
    label "Nowy_&#346;wiat"
  ]
  node [
    id 24
    label "planeta"
  ]
  node [
    id 25
    label "przejmowa&#263;"
  ]
  node [
    id 26
    label "litosfera"
  ]
  node [
    id 27
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 28
    label "makrokosmos"
  ]
  node [
    id 29
    label "barysfera"
  ]
  node [
    id 30
    label "biota"
  ]
  node [
    id 31
    label "p&#243;&#322;noc"
  ]
  node [
    id 32
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 33
    label "fauna"
  ]
  node [
    id 34
    label "wszechstworzenie"
  ]
  node [
    id 35
    label "geotermia"
  ]
  node [
    id 36
    label "biegun"
  ]
  node [
    id 37
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 38
    label "ekosystem"
  ]
  node [
    id 39
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 40
    label "teren"
  ]
  node [
    id 41
    label "zjawisko"
  ]
  node [
    id 42
    label "p&#243;&#322;kula"
  ]
  node [
    id 43
    label "atmosfera"
  ]
  node [
    id 44
    label "mikrokosmos"
  ]
  node [
    id 45
    label "class"
  ]
  node [
    id 46
    label "po&#322;udnie"
  ]
  node [
    id 47
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 48
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 49
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 50
    label "przejmowanie"
  ]
  node [
    id 51
    label "przestrze&#324;"
  ]
  node [
    id 52
    label "asymilowanie_si&#281;"
  ]
  node [
    id 53
    label "przej&#261;&#263;"
  ]
  node [
    id 54
    label "ekosfera"
  ]
  node [
    id 55
    label "przyroda"
  ]
  node [
    id 56
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 57
    label "ciemna_materia"
  ]
  node [
    id 58
    label "geoida"
  ]
  node [
    id 59
    label "Wsch&#243;d"
  ]
  node [
    id 60
    label "populace"
  ]
  node [
    id 61
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 62
    label "huczek"
  ]
  node [
    id 63
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 64
    label "Ziemia"
  ]
  node [
    id 65
    label "universe"
  ]
  node [
    id 66
    label "ozonosfera"
  ]
  node [
    id 67
    label "rze&#378;ba"
  ]
  node [
    id 68
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 69
    label "zagranica"
  ]
  node [
    id 70
    label "hydrosfera"
  ]
  node [
    id 71
    label "woda"
  ]
  node [
    id 72
    label "kuchnia"
  ]
  node [
    id 73
    label "przej&#281;cie"
  ]
  node [
    id 74
    label "czarna_dziura"
  ]
  node [
    id 75
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 76
    label "morze"
  ]
  node [
    id 77
    label "sport_wodny"
  ]
  node [
    id 78
    label "wyspa"
  ]
  node [
    id 79
    label "igrzysko"
  ]
  node [
    id 80
    label "olimpijski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 79
    target 80
  ]
]
