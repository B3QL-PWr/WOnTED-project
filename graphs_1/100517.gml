graph [
  maxDegree 70
  minDegree 1
  meanDegree 2.3354430379746836
  density 0.007414104882459313
  graphCliqueNumber 3
  node [
    id 0
    label "zasada"
    origin "text"
  ]
  node [
    id 1
    label "wniosek"
    origin "text"
  ]
  node [
    id 2
    label "uznanie"
    origin "text"
  ]
  node [
    id 3
    label "zmar&#322;y"
    origin "text"
  ]
  node [
    id 4
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 5
    label "zg&#322;osi&#263;"
    origin "text"
  ]
  node [
    id 6
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 7
    label "rok"
    origin "text"
  ]
  node [
    id 8
    label "przed"
    origin "text"
  ]
  node [
    id 9
    label "koniec"
    origin "text"
  ]
  node [
    id 10
    label "termin"
    origin "text"
  ]
  node [
    id 11
    label "up&#322;yw"
    origin "text"
  ]
  node [
    id 12
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 13
    label "zaginiony"
    origin "text"
  ]
  node [
    id 14
    label "m&#243;cby"
    origin "text"
  ]
  node [
    id 15
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 16
    label "uznany"
    origin "text"
  ]
  node [
    id 17
    label "typowy"
    origin "text"
  ]
  node [
    id 18
    label "przypadek"
    origin "text"
  ]
  node [
    id 19
    label "gdy"
    origin "text"
  ]
  node [
    id 20
    label "czas"
    origin "text"
  ]
  node [
    id 21
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 22
    label "wynosi&#263;"
    origin "text"
  ]
  node [
    id 23
    label "lata"
    origin "text"
  ]
  node [
    id 24
    label "poczyna&#263;"
    origin "text"
  ]
  node [
    id 25
    label "gdyby"
    origin "text"
  ]
  node [
    id 26
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 27
    label "nast&#261;pi&#263;"
    origin "text"
  ]
  node [
    id 28
    label "lub"
    origin "text"
  ]
  node [
    id 29
    label "jeszcze"
    origin "text"
  ]
  node [
    id 30
    label "kr&#243;tki"
    origin "text"
  ]
  node [
    id 31
    label "okres"
    origin "text"
  ]
  node [
    id 32
    label "zdarzenie"
    origin "text"
  ]
  node [
    id 33
    label "uzasadnia&#263;"
    origin "text"
  ]
  node [
    id 34
    label "prawdopodobie&#324;stwo"
    origin "text"
  ]
  node [
    id 35
    label "&#347;mier&#263;"
    origin "text"
  ]
  node [
    id 36
    label "dopiero"
    origin "text"
  ]
  node [
    id 37
    label "obserwacja"
  ]
  node [
    id 38
    label "moralno&#347;&#263;"
  ]
  node [
    id 39
    label "podstawa"
  ]
  node [
    id 40
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 41
    label "umowa"
  ]
  node [
    id 42
    label "dominion"
  ]
  node [
    id 43
    label "qualification"
  ]
  node [
    id 44
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 45
    label "opis"
  ]
  node [
    id 46
    label "regu&#322;a_Allena"
  ]
  node [
    id 47
    label "normalizacja"
  ]
  node [
    id 48
    label "regu&#322;a_Glogera"
  ]
  node [
    id 49
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 50
    label "standard"
  ]
  node [
    id 51
    label "base"
  ]
  node [
    id 52
    label "substancja"
  ]
  node [
    id 53
    label "spos&#243;b"
  ]
  node [
    id 54
    label "prawid&#322;o"
  ]
  node [
    id 55
    label "prawo_Mendla"
  ]
  node [
    id 56
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 57
    label "criterion"
  ]
  node [
    id 58
    label "twierdzenie"
  ]
  node [
    id 59
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 60
    label "prawo"
  ]
  node [
    id 61
    label "occupation"
  ]
  node [
    id 62
    label "zasada_d'Alemberta"
  ]
  node [
    id 63
    label "my&#347;l"
  ]
  node [
    id 64
    label "wnioskowanie"
  ]
  node [
    id 65
    label "propozycja"
  ]
  node [
    id 66
    label "motion"
  ]
  node [
    id 67
    label "pismo"
  ]
  node [
    id 68
    label "prayer"
  ]
  node [
    id 69
    label "fame"
  ]
  node [
    id 70
    label "szanowa&#263;"
  ]
  node [
    id 71
    label "mniemanie"
  ]
  node [
    id 72
    label "ocenienie"
  ]
  node [
    id 73
    label "przechodzenie"
  ]
  node [
    id 74
    label "uhonorowanie"
  ]
  node [
    id 75
    label "respect"
  ]
  node [
    id 76
    label "honorowanie"
  ]
  node [
    id 77
    label "uszanowanie"
  ]
  node [
    id 78
    label "przej&#347;cie"
  ]
  node [
    id 79
    label "honorowa&#263;"
  ]
  node [
    id 80
    label "postawa"
  ]
  node [
    id 81
    label "zachwyt"
  ]
  node [
    id 82
    label "zaimponowanie"
  ]
  node [
    id 83
    label "zrobienie"
  ]
  node [
    id 84
    label "uszanowa&#263;"
  ]
  node [
    id 85
    label "imponowanie"
  ]
  node [
    id 86
    label "szacuneczek"
  ]
  node [
    id 87
    label "oznajmienie"
  ]
  node [
    id 88
    label "acclaim"
  ]
  node [
    id 89
    label "spowodowanie"
  ]
  node [
    id 90
    label "recognition"
  ]
  node [
    id 91
    label "rewerencja"
  ]
  node [
    id 92
    label "uhonorowa&#263;"
  ]
  node [
    id 93
    label "duch"
  ]
  node [
    id 94
    label "cz&#322;owiek"
  ]
  node [
    id 95
    label "umarlak"
  ]
  node [
    id 96
    label "zw&#322;oki"
  ]
  node [
    id 97
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 98
    label "chowanie"
  ]
  node [
    id 99
    label "nieumar&#322;y"
  ]
  node [
    id 100
    label "martwy"
  ]
  node [
    id 101
    label "free"
  ]
  node [
    id 102
    label "report"
  ]
  node [
    id 103
    label "announce"
  ]
  node [
    id 104
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 105
    label "write"
  ]
  node [
    id 106
    label "poinformowa&#263;"
  ]
  node [
    id 107
    label "stulecie"
  ]
  node [
    id 108
    label "kalendarz"
  ]
  node [
    id 109
    label "pora_roku"
  ]
  node [
    id 110
    label "cykl_astronomiczny"
  ]
  node [
    id 111
    label "p&#243;&#322;rocze"
  ]
  node [
    id 112
    label "grupa"
  ]
  node [
    id 113
    label "kwarta&#322;"
  ]
  node [
    id 114
    label "kurs"
  ]
  node [
    id 115
    label "jubileusz"
  ]
  node [
    id 116
    label "miesi&#261;c"
  ]
  node [
    id 117
    label "martwy_sezon"
  ]
  node [
    id 118
    label "defenestracja"
  ]
  node [
    id 119
    label "szereg"
  ]
  node [
    id 120
    label "dzia&#322;anie"
  ]
  node [
    id 121
    label "miejsce"
  ]
  node [
    id 122
    label "ostatnie_podrygi"
  ]
  node [
    id 123
    label "kres"
  ]
  node [
    id 124
    label "agonia"
  ]
  node [
    id 125
    label "visitation"
  ]
  node [
    id 126
    label "szeol"
  ]
  node [
    id 127
    label "mogi&#322;a"
  ]
  node [
    id 128
    label "chwila"
  ]
  node [
    id 129
    label "wydarzenie"
  ]
  node [
    id 130
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 131
    label "pogrzebanie"
  ]
  node [
    id 132
    label "punkt"
  ]
  node [
    id 133
    label "&#380;a&#322;oba"
  ]
  node [
    id 134
    label "zabicie"
  ]
  node [
    id 135
    label "kres_&#380;ycia"
  ]
  node [
    id 136
    label "przypadni&#281;cie"
  ]
  node [
    id 137
    label "chronogram"
  ]
  node [
    id 138
    label "nazewnictwo"
  ]
  node [
    id 139
    label "ekspiracja"
  ]
  node [
    id 140
    label "nazwa"
  ]
  node [
    id 141
    label "przypa&#347;&#263;"
  ]
  node [
    id 142
    label "praktyka"
  ]
  node [
    id 143
    label "term"
  ]
  node [
    id 144
    label "przebieg"
  ]
  node [
    id 145
    label "pass"
  ]
  node [
    id 146
    label "nieobecny"
  ]
  node [
    id 147
    label "proceed"
  ]
  node [
    id 148
    label "catch"
  ]
  node [
    id 149
    label "pozosta&#263;"
  ]
  node [
    id 150
    label "osta&#263;_si&#281;"
  ]
  node [
    id 151
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 152
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 153
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 154
    label "change"
  ]
  node [
    id 155
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 156
    label "ceniony"
  ]
  node [
    id 157
    label "typowo"
  ]
  node [
    id 158
    label "zwyczajny"
  ]
  node [
    id 159
    label "zwyk&#322;y"
  ]
  node [
    id 160
    label "cz&#281;sty"
  ]
  node [
    id 161
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 162
    label "pacjent"
  ]
  node [
    id 163
    label "kategoria_gramatyczna"
  ]
  node [
    id 164
    label "schorzenie"
  ]
  node [
    id 165
    label "przeznaczenie"
  ]
  node [
    id 166
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 167
    label "happening"
  ]
  node [
    id 168
    label "przyk&#322;ad"
  ]
  node [
    id 169
    label "czasokres"
  ]
  node [
    id 170
    label "trawienie"
  ]
  node [
    id 171
    label "period"
  ]
  node [
    id 172
    label "odczyt"
  ]
  node [
    id 173
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 174
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 175
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 176
    label "poprzedzenie"
  ]
  node [
    id 177
    label "koniugacja"
  ]
  node [
    id 178
    label "dzieje"
  ]
  node [
    id 179
    label "poprzedzi&#263;"
  ]
  node [
    id 180
    label "przep&#322;ywanie"
  ]
  node [
    id 181
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 182
    label "odwlekanie_si&#281;"
  ]
  node [
    id 183
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 184
    label "Zeitgeist"
  ]
  node [
    id 185
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 186
    label "okres_czasu"
  ]
  node [
    id 187
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 188
    label "pochodzi&#263;"
  ]
  node [
    id 189
    label "schy&#322;ek"
  ]
  node [
    id 190
    label "czwarty_wymiar"
  ]
  node [
    id 191
    label "chronometria"
  ]
  node [
    id 192
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 193
    label "poprzedzanie"
  ]
  node [
    id 194
    label "pogoda"
  ]
  node [
    id 195
    label "zegar"
  ]
  node [
    id 196
    label "pochodzenie"
  ]
  node [
    id 197
    label "poprzedza&#263;"
  ]
  node [
    id 198
    label "trawi&#263;"
  ]
  node [
    id 199
    label "time_period"
  ]
  node [
    id 200
    label "rachuba_czasu"
  ]
  node [
    id 201
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 202
    label "czasoprzestrze&#324;"
  ]
  node [
    id 203
    label "laba"
  ]
  node [
    id 204
    label "czyj&#347;"
  ]
  node [
    id 205
    label "m&#261;&#380;"
  ]
  node [
    id 206
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 207
    label "odsuwa&#263;"
  ]
  node [
    id 208
    label "zanosi&#263;"
  ]
  node [
    id 209
    label "nasi&#261;ka&#263;"
  ]
  node [
    id 210
    label "otrzymywa&#263;"
  ]
  node [
    id 211
    label "rozpowszechnia&#263;"
  ]
  node [
    id 212
    label "ujawnia&#263;"
  ]
  node [
    id 213
    label "kra&#347;&#263;"
  ]
  node [
    id 214
    label "kwota"
  ]
  node [
    id 215
    label "liczy&#263;"
  ]
  node [
    id 216
    label "raise"
  ]
  node [
    id 217
    label "podnosi&#263;"
  ]
  node [
    id 218
    label "summer"
  ]
  node [
    id 219
    label "robi&#263;_pierwszy_krok"
  ]
  node [
    id 220
    label "begin"
  ]
  node [
    id 221
    label "by&#263;"
  ]
  node [
    id 222
    label "uprawi&#263;"
  ]
  node [
    id 223
    label "gotowy"
  ]
  node [
    id 224
    label "might"
  ]
  node [
    id 225
    label "nacisn&#261;&#263;"
  ]
  node [
    id 226
    label "zaatakowa&#263;"
  ]
  node [
    id 227
    label "gamble"
  ]
  node [
    id 228
    label "supervene"
  ]
  node [
    id 229
    label "ci&#261;gle"
  ]
  node [
    id 230
    label "jednowyrazowy"
  ]
  node [
    id 231
    label "s&#322;aby"
  ]
  node [
    id 232
    label "bliski"
  ]
  node [
    id 233
    label "drobny"
  ]
  node [
    id 234
    label "kr&#243;tko"
  ]
  node [
    id 235
    label "ruch"
  ]
  node [
    id 236
    label "z&#322;y"
  ]
  node [
    id 237
    label "zwi&#281;z&#322;y"
  ]
  node [
    id 238
    label "szybki"
  ]
  node [
    id 239
    label "brak"
  ]
  node [
    id 240
    label "paleogen"
  ]
  node [
    id 241
    label "spell"
  ]
  node [
    id 242
    label "prekambr"
  ]
  node [
    id 243
    label "jura"
  ]
  node [
    id 244
    label "interstadia&#322;"
  ]
  node [
    id 245
    label "jednostka_geologiczna"
  ]
  node [
    id 246
    label "izochronizm"
  ]
  node [
    id 247
    label "okres_noachijski"
  ]
  node [
    id 248
    label "orosir"
  ]
  node [
    id 249
    label "kreda"
  ]
  node [
    id 250
    label "sten"
  ]
  node [
    id 251
    label "drugorz&#281;d"
  ]
  node [
    id 252
    label "semester"
  ]
  node [
    id 253
    label "trzeciorz&#281;d"
  ]
  node [
    id 254
    label "ton"
  ]
  node [
    id 255
    label "poprzednik"
  ]
  node [
    id 256
    label "ordowik"
  ]
  node [
    id 257
    label "karbon"
  ]
  node [
    id 258
    label "trias"
  ]
  node [
    id 259
    label "kalim"
  ]
  node [
    id 260
    label "stater"
  ]
  node [
    id 261
    label "era"
  ]
  node [
    id 262
    label "cykl"
  ]
  node [
    id 263
    label "p&#243;&#322;okres"
  ]
  node [
    id 264
    label "czwartorz&#281;d"
  ]
  node [
    id 265
    label "pulsacja"
  ]
  node [
    id 266
    label "okres_amazo&#324;ski"
  ]
  node [
    id 267
    label "kambr"
  ]
  node [
    id 268
    label "nast&#281;pnik"
  ]
  node [
    id 269
    label "kriogen"
  ]
  node [
    id 270
    label "glacja&#322;"
  ]
  node [
    id 271
    label "fala"
  ]
  node [
    id 272
    label "riak"
  ]
  node [
    id 273
    label "okres_hesperyjski"
  ]
  node [
    id 274
    label "sylur"
  ]
  node [
    id 275
    label "dewon"
  ]
  node [
    id 276
    label "ciota"
  ]
  node [
    id 277
    label "epoka"
  ]
  node [
    id 278
    label "pierwszorz&#281;d"
  ]
  node [
    id 279
    label "okres_halsztacki"
  ]
  node [
    id 280
    label "ektas"
  ]
  node [
    id 281
    label "zdanie"
  ]
  node [
    id 282
    label "condition"
  ]
  node [
    id 283
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 284
    label "rok_akademicki"
  ]
  node [
    id 285
    label "wapie&#324;_muszlowy"
  ]
  node [
    id 286
    label "postglacja&#322;"
  ]
  node [
    id 287
    label "faza"
  ]
  node [
    id 288
    label "proces_fizjologiczny"
  ]
  node [
    id 289
    label "ediakar"
  ]
  node [
    id 290
    label "ruch_drgaj&#261;cy"
  ]
  node [
    id 291
    label "perm"
  ]
  node [
    id 292
    label "rok_szkolny"
  ]
  node [
    id 293
    label "neogen"
  ]
  node [
    id 294
    label "sider"
  ]
  node [
    id 295
    label "flow"
  ]
  node [
    id 296
    label "podokres"
  ]
  node [
    id 297
    label "preglacja&#322;"
  ]
  node [
    id 298
    label "retoryka"
  ]
  node [
    id 299
    label "choroba_przyrodzona"
  ]
  node [
    id 300
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 301
    label "czynno&#347;&#263;"
  ]
  node [
    id 302
    label "motyw"
  ]
  node [
    id 303
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 304
    label "fabu&#322;a"
  ]
  node [
    id 305
    label "przebiec"
  ]
  node [
    id 306
    label "przebiegni&#281;cie"
  ]
  node [
    id 307
    label "charakter"
  ]
  node [
    id 308
    label "explain"
  ]
  node [
    id 309
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 310
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 311
    label "poj&#281;cie"
  ]
  node [
    id 312
    label "&#380;ycie"
  ]
  node [
    id 313
    label "pogrzeb"
  ]
  node [
    id 314
    label "istota_nadprzyrodzona"
  ]
  node [
    id 315
    label "upadek"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 11
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 11
  ]
  edge [
    source 7
    target 25
  ]
  edge [
    source 7
    target 28
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 20
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 7
    target 113
  ]
  edge [
    source 7
    target 114
  ]
  edge [
    source 7
    target 115
  ]
  edge [
    source 7
    target 116
  ]
  edge [
    source 7
    target 23
  ]
  edge [
    source 7
    target 117
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 35
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 9
    target 119
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 129
  ]
  edge [
    source 9
    target 130
  ]
  edge [
    source 9
    target 131
  ]
  edge [
    source 9
    target 132
  ]
  edge [
    source 9
    target 133
  ]
  edge [
    source 9
    target 134
  ]
  edge [
    source 9
    target 135
  ]
  edge [
    source 9
    target 30
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 136
  ]
  edge [
    source 10
    target 20
  ]
  edge [
    source 10
    target 137
  ]
  edge [
    source 10
    target 138
  ]
  edge [
    source 10
    target 139
  ]
  edge [
    source 10
    target 140
  ]
  edge [
    source 10
    target 141
  ]
  edge [
    source 10
    target 142
  ]
  edge [
    source 10
    target 143
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 19
  ]
  edge [
    source 11
    target 20
  ]
  edge [
    source 11
    target 24
  ]
  edge [
    source 11
    target 36
  ]
  edge [
    source 11
    target 144
  ]
  edge [
    source 11
    target 145
  ]
  edge [
    source 11
    target 14
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 32
  ]
  edge [
    source 12
    target 33
  ]
  edge [
    source 12
    target 18
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 35
  ]
  edge [
    source 13
    target 146
  ]
  edge [
    source 13
    target 29
  ]
  edge [
    source 13
    target 13
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 147
  ]
  edge [
    source 15
    target 148
  ]
  edge [
    source 15
    target 149
  ]
  edge [
    source 15
    target 150
  ]
  edge [
    source 15
    target 151
  ]
  edge [
    source 15
    target 152
  ]
  edge [
    source 15
    target 153
  ]
  edge [
    source 15
    target 154
  ]
  edge [
    source 15
    target 155
  ]
  edge [
    source 16
    target 156
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 157
  ]
  edge [
    source 17
    target 158
  ]
  edge [
    source 17
    target 159
  ]
  edge [
    source 17
    target 160
  ]
  edge [
    source 17
    target 161
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 162
  ]
  edge [
    source 18
    target 163
  ]
  edge [
    source 18
    target 164
  ]
  edge [
    source 18
    target 165
  ]
  edge [
    source 18
    target 166
  ]
  edge [
    source 18
    target 129
  ]
  edge [
    source 18
    target 167
  ]
  edge [
    source 18
    target 168
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 169
  ]
  edge [
    source 20
    target 170
  ]
  edge [
    source 20
    target 163
  ]
  edge [
    source 20
    target 171
  ]
  edge [
    source 20
    target 172
  ]
  edge [
    source 20
    target 173
  ]
  edge [
    source 20
    target 174
  ]
  edge [
    source 20
    target 128
  ]
  edge [
    source 20
    target 175
  ]
  edge [
    source 20
    target 176
  ]
  edge [
    source 20
    target 177
  ]
  edge [
    source 20
    target 178
  ]
  edge [
    source 20
    target 179
  ]
  edge [
    source 20
    target 180
  ]
  edge [
    source 20
    target 181
  ]
  edge [
    source 20
    target 182
  ]
  edge [
    source 20
    target 183
  ]
  edge [
    source 20
    target 184
  ]
  edge [
    source 20
    target 185
  ]
  edge [
    source 20
    target 186
  ]
  edge [
    source 20
    target 187
  ]
  edge [
    source 20
    target 188
  ]
  edge [
    source 20
    target 189
  ]
  edge [
    source 20
    target 190
  ]
  edge [
    source 20
    target 191
  ]
  edge [
    source 20
    target 192
  ]
  edge [
    source 20
    target 193
  ]
  edge [
    source 20
    target 194
  ]
  edge [
    source 20
    target 195
  ]
  edge [
    source 20
    target 196
  ]
  edge [
    source 20
    target 197
  ]
  edge [
    source 20
    target 198
  ]
  edge [
    source 20
    target 199
  ]
  edge [
    source 20
    target 200
  ]
  edge [
    source 20
    target 201
  ]
  edge [
    source 20
    target 202
  ]
  edge [
    source 20
    target 203
  ]
  edge [
    source 20
    target 23
  ]
  edge [
    source 20
    target 31
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 204
  ]
  edge [
    source 21
    target 205
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 206
  ]
  edge [
    source 22
    target 207
  ]
  edge [
    source 22
    target 208
  ]
  edge [
    source 22
    target 209
  ]
  edge [
    source 22
    target 210
  ]
  edge [
    source 22
    target 211
  ]
  edge [
    source 22
    target 212
  ]
  edge [
    source 22
    target 213
  ]
  edge [
    source 22
    target 214
  ]
  edge [
    source 22
    target 215
  ]
  edge [
    source 22
    target 216
  ]
  edge [
    source 22
    target 217
  ]
  edge [
    source 23
    target 218
  ]
  edge [
    source 24
    target 219
  ]
  edge [
    source 24
    target 220
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 221
  ]
  edge [
    source 26
    target 222
  ]
  edge [
    source 26
    target 223
  ]
  edge [
    source 26
    target 224
  ]
  edge [
    source 27
    target 225
  ]
  edge [
    source 27
    target 226
  ]
  edge [
    source 27
    target 227
  ]
  edge [
    source 27
    target 228
  ]
  edge [
    source 27
    target 153
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 229
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 230
  ]
  edge [
    source 30
    target 231
  ]
  edge [
    source 30
    target 232
  ]
  edge [
    source 30
    target 233
  ]
  edge [
    source 30
    target 234
  ]
  edge [
    source 30
    target 235
  ]
  edge [
    source 30
    target 236
  ]
  edge [
    source 30
    target 237
  ]
  edge [
    source 30
    target 238
  ]
  edge [
    source 30
    target 239
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 240
  ]
  edge [
    source 31
    target 241
  ]
  edge [
    source 31
    target 171
  ]
  edge [
    source 31
    target 242
  ]
  edge [
    source 31
    target 243
  ]
  edge [
    source 31
    target 244
  ]
  edge [
    source 31
    target 245
  ]
  edge [
    source 31
    target 246
  ]
  edge [
    source 31
    target 175
  ]
  edge [
    source 31
    target 247
  ]
  edge [
    source 31
    target 248
  ]
  edge [
    source 31
    target 249
  ]
  edge [
    source 31
    target 250
  ]
  edge [
    source 31
    target 251
  ]
  edge [
    source 31
    target 252
  ]
  edge [
    source 31
    target 253
  ]
  edge [
    source 31
    target 254
  ]
  edge [
    source 31
    target 178
  ]
  edge [
    source 31
    target 255
  ]
  edge [
    source 31
    target 256
  ]
  edge [
    source 31
    target 257
  ]
  edge [
    source 31
    target 258
  ]
  edge [
    source 31
    target 259
  ]
  edge [
    source 31
    target 260
  ]
  edge [
    source 31
    target 261
  ]
  edge [
    source 31
    target 262
  ]
  edge [
    source 31
    target 263
  ]
  edge [
    source 31
    target 264
  ]
  edge [
    source 31
    target 265
  ]
  edge [
    source 31
    target 266
  ]
  edge [
    source 31
    target 267
  ]
  edge [
    source 31
    target 184
  ]
  edge [
    source 31
    target 268
  ]
  edge [
    source 31
    target 269
  ]
  edge [
    source 31
    target 270
  ]
  edge [
    source 31
    target 271
  ]
  edge [
    source 31
    target 186
  ]
  edge [
    source 31
    target 272
  ]
  edge [
    source 31
    target 189
  ]
  edge [
    source 31
    target 273
  ]
  edge [
    source 31
    target 274
  ]
  edge [
    source 31
    target 275
  ]
  edge [
    source 31
    target 276
  ]
  edge [
    source 31
    target 277
  ]
  edge [
    source 31
    target 278
  ]
  edge [
    source 31
    target 279
  ]
  edge [
    source 31
    target 280
  ]
  edge [
    source 31
    target 281
  ]
  edge [
    source 31
    target 282
  ]
  edge [
    source 31
    target 283
  ]
  edge [
    source 31
    target 284
  ]
  edge [
    source 31
    target 285
  ]
  edge [
    source 31
    target 286
  ]
  edge [
    source 31
    target 287
  ]
  edge [
    source 31
    target 288
  ]
  edge [
    source 31
    target 289
  ]
  edge [
    source 31
    target 199
  ]
  edge [
    source 31
    target 290
  ]
  edge [
    source 31
    target 291
  ]
  edge [
    source 31
    target 292
  ]
  edge [
    source 31
    target 293
  ]
  edge [
    source 31
    target 294
  ]
  edge [
    source 31
    target 295
  ]
  edge [
    source 31
    target 296
  ]
  edge [
    source 31
    target 297
  ]
  edge [
    source 31
    target 298
  ]
  edge [
    source 31
    target 299
  ]
  edge [
    source 32
    target 300
  ]
  edge [
    source 32
    target 301
  ]
  edge [
    source 32
    target 302
  ]
  edge [
    source 32
    target 303
  ]
  edge [
    source 32
    target 304
  ]
  edge [
    source 32
    target 305
  ]
  edge [
    source 32
    target 306
  ]
  edge [
    source 32
    target 307
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 308
  ]
  edge [
    source 33
    target 309
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 310
  ]
  edge [
    source 34
    target 311
  ]
  edge [
    source 35
    target 135
  ]
  edge [
    source 35
    target 118
  ]
  edge [
    source 35
    target 123
  ]
  edge [
    source 35
    target 124
  ]
  edge [
    source 35
    target 312
  ]
  edge [
    source 35
    target 126
  ]
  edge [
    source 35
    target 127
  ]
  edge [
    source 35
    target 313
  ]
  edge [
    source 35
    target 314
  ]
  edge [
    source 35
    target 131
  ]
  edge [
    source 35
    target 133
  ]
  edge [
    source 35
    target 134
  ]
  edge [
    source 35
    target 315
  ]
]
