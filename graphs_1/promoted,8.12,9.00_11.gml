graph [
  maxDegree 22
  minDegree 1
  meanDegree 1.9710144927536233
  density 0.028985507246376812
  graphCliqueNumber 2
  node [
    id 0
    label "zwi&#261;zek"
    origin "text"
  ]
  node [
    id 1
    label "nazwa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "fruktanami"
    origin "text"
  ]
  node [
    id 3
    label "wywo&#322;ywa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "pacjent"
    origin "text"
  ]
  node [
    id 5
    label "uskar&#380;a&#263;"
    origin "text"
  ]
  node [
    id 6
    label "si&#281;"
    origin "text"
  ]
  node [
    id 7
    label "nietolerancja"
    origin "text"
  ]
  node [
    id 8
    label "odwodnienie"
  ]
  node [
    id 9
    label "konstytucja"
  ]
  node [
    id 10
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 11
    label "substancja_chemiczna"
  ]
  node [
    id 12
    label "bratnia_dusza"
  ]
  node [
    id 13
    label "zwi&#261;zanie"
  ]
  node [
    id 14
    label "lokant"
  ]
  node [
    id 15
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 16
    label "zwi&#261;za&#263;"
  ]
  node [
    id 17
    label "organizacja"
  ]
  node [
    id 18
    label "odwadnia&#263;"
  ]
  node [
    id 19
    label "marriage"
  ]
  node [
    id 20
    label "marketing_afiliacyjny"
  ]
  node [
    id 21
    label "bearing"
  ]
  node [
    id 22
    label "wi&#261;zanie"
  ]
  node [
    id 23
    label "odwadnianie"
  ]
  node [
    id 24
    label "koligacja"
  ]
  node [
    id 25
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 26
    label "odwodni&#263;"
  ]
  node [
    id 27
    label "azeotrop"
  ]
  node [
    id 28
    label "powi&#261;zanie"
  ]
  node [
    id 29
    label "broadcast"
  ]
  node [
    id 30
    label "okre&#347;li&#263;"
  ]
  node [
    id 31
    label "nada&#263;"
  ]
  node [
    id 32
    label "call"
  ]
  node [
    id 33
    label "oznajmia&#263;"
  ]
  node [
    id 34
    label "wzywa&#263;"
  ]
  node [
    id 35
    label "wydala&#263;"
  ]
  node [
    id 36
    label "przetwarza&#263;"
  ]
  node [
    id 37
    label "poleca&#263;"
  ]
  node [
    id 38
    label "create"
  ]
  node [
    id 39
    label "dispose"
  ]
  node [
    id 40
    label "powodowa&#263;"
  ]
  node [
    id 41
    label "od&#322;&#261;czenie"
  ]
  node [
    id 42
    label "cz&#322;owiek"
  ]
  node [
    id 43
    label "przymus_bezpo&#347;redni"
  ]
  node [
    id 44
    label "od&#322;&#261;czy&#263;"
  ]
  node [
    id 45
    label "od&#322;&#261;czanie"
  ]
  node [
    id 46
    label "klient"
  ]
  node [
    id 47
    label "chory"
  ]
  node [
    id 48
    label "szpitalnik"
  ]
  node [
    id 49
    label "od&#322;&#261;cza&#263;"
  ]
  node [
    id 50
    label "przypadek"
  ]
  node [
    id 51
    label "piel&#281;gniarz"
  ]
  node [
    id 52
    label "choroba_somatyczna"
  ]
  node [
    id 53
    label "reakcja"
  ]
  node [
    id 54
    label "odczulenie"
  ]
  node [
    id 55
    label "krzywda"
  ]
  node [
    id 56
    label "schorzenie"
  ]
  node [
    id 57
    label "sensybilizacja"
  ]
  node [
    id 58
    label "odczulanie"
  ]
  node [
    id 59
    label "rumie&#324;_wielopostaciowy"
  ]
  node [
    id 60
    label "uczuli&#263;"
  ]
  node [
    id 61
    label "odczula&#263;"
  ]
  node [
    id 62
    label "uczulenie"
  ]
  node [
    id 63
    label "uczula&#263;"
  ]
  node [
    id 64
    label "alergen"
  ]
  node [
    id 65
    label "kaszel"
  ]
  node [
    id 66
    label "intolerance"
  ]
  node [
    id 67
    label "uczulanie"
  ]
  node [
    id 68
    label "odczuli&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
]
