graph [
  maxDegree 42
  minDegree 1
  meanDegree 2.032258064516129
  density 0.016522423288749016
  graphCliqueNumber 3
  node [
    id 0
    label "mirka"
    origin "text"
  ]
  node [
    id 1
    label "by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "taka"
    origin "text"
  ]
  node [
    id 3
    label "sprawa"
    origin "text"
  ]
  node [
    id 4
    label "wychowywa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "brat"
    origin "text"
  ]
  node [
    id 6
    label "chodzi&#263;"
    origin "text"
  ]
  node [
    id 7
    label "klasa"
    origin "text"
  ]
  node [
    id 8
    label "nauczycielka"
    origin "text"
  ]
  node [
    id 9
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 10
    label "dyskryminowa&#263;"
    origin "text"
  ]
  node [
    id 11
    label "si&#281;ga&#263;"
  ]
  node [
    id 12
    label "trwa&#263;"
  ]
  node [
    id 13
    label "obecno&#347;&#263;"
  ]
  node [
    id 14
    label "stan"
  ]
  node [
    id 15
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 16
    label "stand"
  ]
  node [
    id 17
    label "mie&#263;_miejsce"
  ]
  node [
    id 18
    label "uczestniczy&#263;"
  ]
  node [
    id 19
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 20
    label "equal"
  ]
  node [
    id 21
    label "Bangladesz"
  ]
  node [
    id 22
    label "jednostka_monetarna"
  ]
  node [
    id 23
    label "temat"
  ]
  node [
    id 24
    label "kognicja"
  ]
  node [
    id 25
    label "idea"
  ]
  node [
    id 26
    label "szczeg&#243;&#322;"
  ]
  node [
    id 27
    label "rzecz"
  ]
  node [
    id 28
    label "wydarzenie"
  ]
  node [
    id 29
    label "przes&#322;anka"
  ]
  node [
    id 30
    label "rozprawa"
  ]
  node [
    id 31
    label "object"
  ]
  node [
    id 32
    label "proposition"
  ]
  node [
    id 33
    label "train"
  ]
  node [
    id 34
    label "szkoli&#263;"
  ]
  node [
    id 35
    label "opiekowa&#263;_si&#281;"
  ]
  node [
    id 36
    label "cz&#322;owiek"
  ]
  node [
    id 37
    label "cz&#322;onek"
  ]
  node [
    id 38
    label "mnich"
  ]
  node [
    id 39
    label "r&#243;wniacha"
  ]
  node [
    id 40
    label "zwrot"
  ]
  node [
    id 41
    label "bratanie_si&#281;"
  ]
  node [
    id 42
    label "zbratanie_si&#281;"
  ]
  node [
    id 43
    label "sw&#243;j"
  ]
  node [
    id 44
    label "&#347;w"
  ]
  node [
    id 45
    label "pobratymiec"
  ]
  node [
    id 46
    label "przyjaciel"
  ]
  node [
    id 47
    label "krewny"
  ]
  node [
    id 48
    label "wyznawca"
  ]
  node [
    id 49
    label "stryj"
  ]
  node [
    id 50
    label "zakon"
  ]
  node [
    id 51
    label "br"
  ]
  node [
    id 52
    label "rodze&#324;stwo"
  ]
  node [
    id 53
    label "bractwo"
  ]
  node [
    id 54
    label "proceed"
  ]
  node [
    id 55
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 56
    label "bangla&#263;"
  ]
  node [
    id 57
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 58
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 59
    label "run"
  ]
  node [
    id 60
    label "tryb"
  ]
  node [
    id 61
    label "p&#322;ywa&#263;"
  ]
  node [
    id 62
    label "continue"
  ]
  node [
    id 63
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 64
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 65
    label "przebiega&#263;"
  ]
  node [
    id 66
    label "wk&#322;ada&#263;"
  ]
  node [
    id 67
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 68
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 69
    label "para"
  ]
  node [
    id 70
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 71
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 72
    label "krok"
  ]
  node [
    id 73
    label "str&#243;j"
  ]
  node [
    id 74
    label "bywa&#263;"
  ]
  node [
    id 75
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 76
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 77
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 78
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 79
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 80
    label "dziama&#263;"
  ]
  node [
    id 81
    label "stara&#263;_si&#281;"
  ]
  node [
    id 82
    label "carry"
  ]
  node [
    id 83
    label "typ"
  ]
  node [
    id 84
    label "warstwa"
  ]
  node [
    id 85
    label "znak_jako&#347;ci"
  ]
  node [
    id 86
    label "przedmiot"
  ]
  node [
    id 87
    label "przepisa&#263;"
  ]
  node [
    id 88
    label "grupa"
  ]
  node [
    id 89
    label "pomoc"
  ]
  node [
    id 90
    label "arrangement"
  ]
  node [
    id 91
    label "wagon"
  ]
  node [
    id 92
    label "form"
  ]
  node [
    id 93
    label "zaleta"
  ]
  node [
    id 94
    label "poziom"
  ]
  node [
    id 95
    label "dziennik_lekcyjny"
  ]
  node [
    id 96
    label "&#347;rodowisko"
  ]
  node [
    id 97
    label "atak"
  ]
  node [
    id 98
    label "przepisanie"
  ]
  node [
    id 99
    label "szko&#322;a"
  ]
  node [
    id 100
    label "class"
  ]
  node [
    id 101
    label "organizacja"
  ]
  node [
    id 102
    label "obrona"
  ]
  node [
    id 103
    label "type"
  ]
  node [
    id 104
    label "promocja"
  ]
  node [
    id 105
    label "&#322;awka"
  ]
  node [
    id 106
    label "kurs"
  ]
  node [
    id 107
    label "botanika"
  ]
  node [
    id 108
    label "sala"
  ]
  node [
    id 109
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 110
    label "gromada"
  ]
  node [
    id 111
    label "obiekt"
  ]
  node [
    id 112
    label "Ekwici"
  ]
  node [
    id 113
    label "fakcja"
  ]
  node [
    id 114
    label "tablica"
  ]
  node [
    id 115
    label "programowanie_obiektowe"
  ]
  node [
    id 116
    label "wykrzyknik"
  ]
  node [
    id 117
    label "jednostka_systematyczna"
  ]
  node [
    id 118
    label "mecz_mistrzowski"
  ]
  node [
    id 119
    label "zbi&#243;r"
  ]
  node [
    id 120
    label "jako&#347;&#263;"
  ]
  node [
    id 121
    label "rezerwa"
  ]
  node [
    id 122
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 123
    label "krzywdzi&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 10
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 17
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 7
    target 113
  ]
  edge [
    source 7
    target 114
  ]
  edge [
    source 7
    target 115
  ]
  edge [
    source 7
    target 116
  ]
  edge [
    source 7
    target 117
  ]
  edge [
    source 7
    target 118
  ]
  edge [
    source 7
    target 119
  ]
  edge [
    source 7
    target 120
  ]
  edge [
    source 7
    target 121
  ]
  edge [
    source 7
    target 122
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 123
  ]
]
