graph [
  maxDegree 20
  minDegree 1
  meanDegree 2
  density 0.02857142857142857
  graphCliqueNumber 2
  node [
    id 0
    label "wiedzie&#263;"
    origin "text"
  ]
  node [
    id 1
    label "wpad&#322;y"
    origin "text"
  ]
  node [
    id 2
    label "ten"
    origin "text"
  ]
  node [
    id 3
    label "pomys&#322;"
    origin "text"
  ]
  node [
    id 4
    label "mama"
    origin "text"
  ]
  node [
    id 5
    label "nadzieja"
    origin "text"
  ]
  node [
    id 6
    label "koniec"
    origin "text"
  ]
  node [
    id 7
    label "si&#281;"
    origin "text"
  ]
  node [
    id 8
    label "udo"
    origin "text"
  ]
  node [
    id 9
    label "nape&#322;ni&#263;"
    origin "text"
  ]
  node [
    id 10
    label "wywrotka"
    origin "text"
  ]
  node [
    id 11
    label "cognizance"
  ]
  node [
    id 12
    label "okre&#347;lony"
  ]
  node [
    id 13
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 14
    label "system"
  ]
  node [
    id 15
    label "wytw&#243;r"
  ]
  node [
    id 16
    label "idea"
  ]
  node [
    id 17
    label "ukra&#347;&#263;"
  ]
  node [
    id 18
    label "ukradzenie"
  ]
  node [
    id 19
    label "pocz&#261;tki"
  ]
  node [
    id 20
    label "matczysko"
  ]
  node [
    id 21
    label "macierz"
  ]
  node [
    id 22
    label "przodkini"
  ]
  node [
    id 23
    label "Matka_Boska"
  ]
  node [
    id 24
    label "macocha"
  ]
  node [
    id 25
    label "matka_zast&#281;pcza"
  ]
  node [
    id 26
    label "stara"
  ]
  node [
    id 27
    label "rodzice"
  ]
  node [
    id 28
    label "rodzic"
  ]
  node [
    id 29
    label "&#347;wiate&#322;ko_w_tunelu"
  ]
  node [
    id 30
    label "wierzy&#263;"
  ]
  node [
    id 31
    label "szansa"
  ]
  node [
    id 32
    label "oczekiwanie"
  ]
  node [
    id 33
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 34
    label "spoczywa&#263;"
  ]
  node [
    id 35
    label "defenestracja"
  ]
  node [
    id 36
    label "szereg"
  ]
  node [
    id 37
    label "dzia&#322;anie"
  ]
  node [
    id 38
    label "miejsce"
  ]
  node [
    id 39
    label "ostatnie_podrygi"
  ]
  node [
    id 40
    label "kres"
  ]
  node [
    id 41
    label "agonia"
  ]
  node [
    id 42
    label "visitation"
  ]
  node [
    id 43
    label "szeol"
  ]
  node [
    id 44
    label "mogi&#322;a"
  ]
  node [
    id 45
    label "chwila"
  ]
  node [
    id 46
    label "wydarzenie"
  ]
  node [
    id 47
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 48
    label "pogrzebanie"
  ]
  node [
    id 49
    label "punkt"
  ]
  node [
    id 50
    label "&#380;a&#322;oba"
  ]
  node [
    id 51
    label "zabicie"
  ]
  node [
    id 52
    label "kres_&#380;ycia"
  ]
  node [
    id 53
    label "kr&#281;tarz"
  ]
  node [
    id 54
    label "mi&#281;sie&#324;_czworog&#322;owy"
  ]
  node [
    id 55
    label "mi&#281;sie&#324;_dwug&#322;owy_uda"
  ]
  node [
    id 56
    label "struktura_anatomiczna"
  ]
  node [
    id 57
    label "t&#281;tnica_udowa"
  ]
  node [
    id 58
    label "noga"
  ]
  node [
    id 59
    label "wywo&#322;a&#263;"
  ]
  node [
    id 60
    label "do"
  ]
  node [
    id 61
    label "umie&#347;ci&#263;"
  ]
  node [
    id 62
    label "perform"
  ]
  node [
    id 63
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 64
    label "sprawi&#263;"
  ]
  node [
    id 65
    label "wzbudzi&#263;"
  ]
  node [
    id 66
    label "przyczepa"
  ]
  node [
    id 67
    label "ci&#281;&#380;ki_sprz&#281;t"
  ]
  node [
    id 68
    label "samoch&#243;d_ci&#281;&#380;arowy"
  ]
  node [
    id 69
    label "ruch"
  ]
  node [
    id 70
    label "wagon"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 10
    target 69
  ]
  edge [
    source 10
    target 70
  ]
]
