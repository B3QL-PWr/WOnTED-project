graph [
  maxDegree 18
  minDegree 1
  meanDegree 1.9661016949152543
  density 0.03389830508474576
  graphCliqueNumber 2
  node [
    id 0
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 1
    label "graf"
    origin "text"
  ]
  node [
    id 2
    label "prosty"
    origin "text"
  ]
  node [
    id 3
    label "planarny"
    origin "text"
  ]
  node [
    id 4
    label "by&#263;"
    origin "text"
  ]
  node [
    id 5
    label "kolorowy"
    origin "text"
  ]
  node [
    id 6
    label "jaki&#347;"
  ]
  node [
    id 7
    label "wykres"
  ]
  node [
    id 8
    label "hrabia"
  ]
  node [
    id 9
    label "graph"
  ]
  node [
    id 10
    label "arystokrata"
  ]
  node [
    id 11
    label "kraw&#281;d&#378;"
  ]
  node [
    id 12
    label "tytu&#322;"
  ]
  node [
    id 13
    label "Fredro"
  ]
  node [
    id 14
    label "wierzcho&#322;ek"
  ]
  node [
    id 15
    label "poj&#281;cie"
  ]
  node [
    id 16
    label "&#322;atwy"
  ]
  node [
    id 17
    label "prostowanie_si&#281;"
  ]
  node [
    id 18
    label "rozprostowanie_si&#281;"
  ]
  node [
    id 19
    label "rozprostowanie"
  ]
  node [
    id 20
    label "niez&#322;o&#380;ony"
  ]
  node [
    id 21
    label "prostoduszny"
  ]
  node [
    id 22
    label "naturalny"
  ]
  node [
    id 23
    label "naiwny"
  ]
  node [
    id 24
    label "cios"
  ]
  node [
    id 25
    label "prostowanie"
  ]
  node [
    id 26
    label "niepozorny"
  ]
  node [
    id 27
    label "zwyk&#322;y"
  ]
  node [
    id 28
    label "prosto"
  ]
  node [
    id 29
    label "po_prostu"
  ]
  node [
    id 30
    label "skromny"
  ]
  node [
    id 31
    label "wyprostowanie_si&#281;"
  ]
  node [
    id 32
    label "si&#281;ga&#263;"
  ]
  node [
    id 33
    label "trwa&#263;"
  ]
  node [
    id 34
    label "obecno&#347;&#263;"
  ]
  node [
    id 35
    label "stan"
  ]
  node [
    id 36
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 37
    label "stand"
  ]
  node [
    id 38
    label "mie&#263;_miejsce"
  ]
  node [
    id 39
    label "uczestniczy&#263;"
  ]
  node [
    id 40
    label "chodzi&#263;"
  ]
  node [
    id 41
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 42
    label "equal"
  ]
  node [
    id 43
    label "cz&#322;owiek"
  ]
  node [
    id 44
    label "kolorowo"
  ]
  node [
    id 45
    label "barwienie_si&#281;"
  ]
  node [
    id 46
    label "pi&#281;kny"
  ]
  node [
    id 47
    label "przyjemny"
  ]
  node [
    id 48
    label "ubarwienie"
  ]
  node [
    id 49
    label "kolorowanie"
  ]
  node [
    id 50
    label "r&#243;&#380;nokolorowo"
  ]
  node [
    id 51
    label "r&#243;&#380;nofarbny"
  ]
  node [
    id 52
    label "barwisty"
  ]
  node [
    id 53
    label "ciekawy"
  ]
  node [
    id 54
    label "weso&#322;y"
  ]
  node [
    id 55
    label "zabarwienie_si&#281;"
  ]
  node [
    id 56
    label "zr&#243;&#380;nicowany"
  ]
  node [
    id 57
    label "barwienie"
  ]
  node [
    id 58
    label "barwnie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
]
