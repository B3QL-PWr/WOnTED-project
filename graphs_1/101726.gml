graph [
  maxDegree 49
  minDegree 1
  meanDegree 2.229268292682927
  density 0.0054505337229411414
  graphCliqueNumber 3
  node [
    id 0
    label "podstawa"
    origin "text"
  ]
  node [
    id 1
    label "art"
    origin "text"
  ]
  node [
    id 2
    label "pkt"
    origin "text"
  ]
  node [
    id 3
    label "ustawa"
    origin "text"
  ]
  node [
    id 4
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 5
    label "kwiecie&#324;"
    origin "text"
  ]
  node [
    id 6
    label "rocznik"
    origin "text"
  ]
  node [
    id 7
    label "narodowy"
    origin "text"
  ]
  node [
    id 8
    label "plan"
    origin "text"
  ]
  node [
    id 9
    label "rozw&#243;j"
    origin "text"
  ]
  node [
    id 10
    label "dziennik"
    origin "text"
  ]
  node [
    id 11
    label "poz"
    origin "text"
  ]
  node [
    id 12
    label "zarz&#261;dza&#263;"
    origin "text"
  ]
  node [
    id 13
    label "si&#281;"
    origin "text"
  ]
  node [
    id 14
    label "nast&#281;powa&#263;"
    origin "text"
  ]
  node [
    id 15
    label "rozporz&#261;dzenie"
    origin "text"
  ]
  node [
    id 16
    label "minister"
    origin "text"
  ]
  node [
    id 17
    label "rolnictwo"
    origin "text"
  ]
  node [
    id 18
    label "wsi"
    origin "text"
  ]
  node [
    id 19
    label "listopad"
    origin "text"
  ]
  node [
    id 20
    label "sprawa"
    origin "text"
  ]
  node [
    id 21
    label "tryb"
    origin "text"
  ]
  node [
    id 22
    label "sk&#322;ada&#263;"
    origin "text"
  ]
  node [
    id 23
    label "wz&#243;r"
    origin "text"
  ]
  node [
    id 24
    label "wniosek"
    origin "text"
  ]
  node [
    id 25
    label "dofinansowa&#263;"
    origin "text"
  ]
  node [
    id 26
    label "realizacja"
    origin "text"
  ]
  node [
    id 27
    label "projekt"
    origin "text"
  ]
  node [
    id 28
    label "rama"
    origin "text"
  ]
  node [
    id 29
    label "sektorowy"
    origin "text"
  ]
  node [
    id 30
    label "program"
    origin "text"
  ]
  node [
    id 31
    label "operacyjny"
    origin "text"
  ]
  node [
    id 32
    label "restrukturyzacja"
    origin "text"
  ]
  node [
    id 33
    label "modernizacja"
    origin "text"
  ]
  node [
    id 34
    label "sektor"
    origin "text"
  ]
  node [
    id 35
    label "&#380;ywno&#347;ciowy"
    origin "text"
  ]
  node [
    id 36
    label "obszar"
    origin "text"
  ]
  node [
    id 37
    label "wiejski"
    origin "text"
  ]
  node [
    id 38
    label "zakres"
    origin "text"
  ]
  node [
    id 39
    label "dzia&#322;ania"
    origin "text"
  ]
  node [
    id 40
    label "wsparcie"
    origin "text"
  ]
  node [
    id 41
    label "doradztwo"
    origin "text"
  ]
  node [
    id 42
    label "rolniczy"
    origin "text"
  ]
  node [
    id 43
    label "wprowadza&#263;"
    origin "text"
  ]
  node [
    id 44
    label "nast&#281;puj&#261;cy"
    origin "text"
  ]
  node [
    id 45
    label "zmiana"
    origin "text"
  ]
  node [
    id 46
    label "podstawowy"
  ]
  node [
    id 47
    label "strategia"
  ]
  node [
    id 48
    label "pot&#281;ga"
  ]
  node [
    id 49
    label "zasadzenie"
  ]
  node [
    id 50
    label "za&#322;o&#380;enie"
  ]
  node [
    id 51
    label "&#347;ciana"
  ]
  node [
    id 52
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 53
    label "przedmiot"
  ]
  node [
    id 54
    label "documentation"
  ]
  node [
    id 55
    label "dzieci&#281;ctwo"
  ]
  node [
    id 56
    label "pomys&#322;"
  ]
  node [
    id 57
    label "bok"
  ]
  node [
    id 58
    label "d&#243;&#322;"
  ]
  node [
    id 59
    label "punkt_odniesienia"
  ]
  node [
    id 60
    label "column"
  ]
  node [
    id 61
    label "zasadzi&#263;"
  ]
  node [
    id 62
    label "background"
  ]
  node [
    id 63
    label "Karta_Nauczyciela"
  ]
  node [
    id 64
    label "marc&#243;wka"
  ]
  node [
    id 65
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 66
    label "akt"
  ]
  node [
    id 67
    label "przej&#347;&#263;"
  ]
  node [
    id 68
    label "charter"
  ]
  node [
    id 69
    label "przej&#347;cie"
  ]
  node [
    id 70
    label "s&#322;o&#324;ce"
  ]
  node [
    id 71
    label "czynienie_si&#281;"
  ]
  node [
    id 72
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 73
    label "czas"
  ]
  node [
    id 74
    label "long_time"
  ]
  node [
    id 75
    label "przedpo&#322;udnie"
  ]
  node [
    id 76
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 77
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 78
    label "tydzie&#324;"
  ]
  node [
    id 79
    label "godzina"
  ]
  node [
    id 80
    label "t&#322;usty_czwartek"
  ]
  node [
    id 81
    label "wsta&#263;"
  ]
  node [
    id 82
    label "day"
  ]
  node [
    id 83
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 84
    label "przedwiecz&#243;r"
  ]
  node [
    id 85
    label "Sylwester"
  ]
  node [
    id 86
    label "po&#322;udnie"
  ]
  node [
    id 87
    label "wzej&#347;cie"
  ]
  node [
    id 88
    label "podwiecz&#243;r"
  ]
  node [
    id 89
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 90
    label "rano"
  ]
  node [
    id 91
    label "termin"
  ]
  node [
    id 92
    label "ranek"
  ]
  node [
    id 93
    label "doba"
  ]
  node [
    id 94
    label "wiecz&#243;r"
  ]
  node [
    id 95
    label "walentynki"
  ]
  node [
    id 96
    label "popo&#322;udnie"
  ]
  node [
    id 97
    label "noc"
  ]
  node [
    id 98
    label "wstanie"
  ]
  node [
    id 99
    label "miesi&#261;c"
  ]
  node [
    id 100
    label "formacja"
  ]
  node [
    id 101
    label "kronika"
  ]
  node [
    id 102
    label "czasopismo"
  ]
  node [
    id 103
    label "yearbook"
  ]
  node [
    id 104
    label "nacjonalistyczny"
  ]
  node [
    id 105
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 106
    label "narodowo"
  ]
  node [
    id 107
    label "wa&#380;ny"
  ]
  node [
    id 108
    label "device"
  ]
  node [
    id 109
    label "model"
  ]
  node [
    id 110
    label "wytw&#243;r"
  ]
  node [
    id 111
    label "obraz"
  ]
  node [
    id 112
    label "przestrze&#324;"
  ]
  node [
    id 113
    label "dekoracja"
  ]
  node [
    id 114
    label "intencja"
  ]
  node [
    id 115
    label "agreement"
  ]
  node [
    id 116
    label "punkt"
  ]
  node [
    id 117
    label "miejsce_pracy"
  ]
  node [
    id 118
    label "perspektywa"
  ]
  node [
    id 119
    label "rysunek"
  ]
  node [
    id 120
    label "reprezentacja"
  ]
  node [
    id 121
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 122
    label "procedura"
  ]
  node [
    id 123
    label "process"
  ]
  node [
    id 124
    label "cycle"
  ]
  node [
    id 125
    label "proces"
  ]
  node [
    id 126
    label "&#380;ycie"
  ]
  node [
    id 127
    label "z&#322;ote_czasy"
  ]
  node [
    id 128
    label "proces_biologiczny"
  ]
  node [
    id 129
    label "spis"
  ]
  node [
    id 130
    label "sheet"
  ]
  node [
    id 131
    label "gazeta"
  ]
  node [
    id 132
    label "diariusz"
  ]
  node [
    id 133
    label "pami&#281;tnik"
  ]
  node [
    id 134
    label "journal"
  ]
  node [
    id 135
    label "ksi&#281;ga"
  ]
  node [
    id 136
    label "program_informacyjny"
  ]
  node [
    id 137
    label "control"
  ]
  node [
    id 138
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 139
    label "sprawowa&#263;"
  ]
  node [
    id 140
    label "poleca&#263;"
  ]
  node [
    id 141
    label "decydowa&#263;"
  ]
  node [
    id 142
    label "mie&#263;_miejsce"
  ]
  node [
    id 143
    label "chance"
  ]
  node [
    id 144
    label "zdarza&#263;_si&#281;"
  ]
  node [
    id 145
    label "alternate"
  ]
  node [
    id 146
    label "naciska&#263;"
  ]
  node [
    id 147
    label "atakowa&#263;"
  ]
  node [
    id 148
    label "rule"
  ]
  node [
    id 149
    label "polecenie"
  ]
  node [
    id 150
    label "arrangement"
  ]
  node [
    id 151
    label "zarz&#261;dzenie"
  ]
  node [
    id 152
    label "commission"
  ]
  node [
    id 153
    label "ordonans"
  ]
  node [
    id 154
    label "Goebbels"
  ]
  node [
    id 155
    label "Sto&#322;ypin"
  ]
  node [
    id 156
    label "rz&#261;d"
  ]
  node [
    id 157
    label "dostojnik"
  ]
  node [
    id 158
    label "intensyfikacja"
  ]
  node [
    id 159
    label "agronomia"
  ]
  node [
    id 160
    label "gleboznawstwo"
  ]
  node [
    id 161
    label "&#322;&#261;karstwo"
  ]
  node [
    id 162
    label "sadownictwo"
  ]
  node [
    id 163
    label "&#322;owiectwo"
  ]
  node [
    id 164
    label "gospodarka"
  ]
  node [
    id 165
    label "nasiennictwo"
  ]
  node [
    id 166
    label "agroekologia"
  ]
  node [
    id 167
    label "uprawianie"
  ]
  node [
    id 168
    label "agrobiznes"
  ]
  node [
    id 169
    label "zgarniacz"
  ]
  node [
    id 170
    label "hodowla"
  ]
  node [
    id 171
    label "zootechnika"
  ]
  node [
    id 172
    label "farmerstwo"
  ]
  node [
    id 173
    label "agrochemia"
  ]
  node [
    id 174
    label "agrotechnika"
  ]
  node [
    id 175
    label "ogrodnictwo"
  ]
  node [
    id 176
    label "nauka"
  ]
  node [
    id 177
    label "temat"
  ]
  node [
    id 178
    label "kognicja"
  ]
  node [
    id 179
    label "idea"
  ]
  node [
    id 180
    label "szczeg&#243;&#322;"
  ]
  node [
    id 181
    label "rzecz"
  ]
  node [
    id 182
    label "wydarzenie"
  ]
  node [
    id 183
    label "przes&#322;anka"
  ]
  node [
    id 184
    label "rozprawa"
  ]
  node [
    id 185
    label "object"
  ]
  node [
    id 186
    label "proposition"
  ]
  node [
    id 187
    label "funkcjonowa&#263;"
  ]
  node [
    id 188
    label "kategoria_gramatyczna"
  ]
  node [
    id 189
    label "skala"
  ]
  node [
    id 190
    label "cecha"
  ]
  node [
    id 191
    label "ko&#322;o_z&#281;bate"
  ]
  node [
    id 192
    label "z&#261;b"
  ]
  node [
    id 193
    label "modalno&#347;&#263;"
  ]
  node [
    id 194
    label "koniugacja"
  ]
  node [
    id 195
    label "ko&#322;o"
  ]
  node [
    id 196
    label "spos&#243;b"
  ]
  node [
    id 197
    label "render"
  ]
  node [
    id 198
    label "zmienia&#263;"
  ]
  node [
    id 199
    label "zestaw"
  ]
  node [
    id 200
    label "train"
  ]
  node [
    id 201
    label "uk&#322;ada&#263;"
  ]
  node [
    id 202
    label "dzieli&#263;"
  ]
  node [
    id 203
    label "set"
  ]
  node [
    id 204
    label "przywraca&#263;"
  ]
  node [
    id 205
    label "dawa&#263;"
  ]
  node [
    id 206
    label "przyk&#322;ada&#263;"
  ]
  node [
    id 207
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 208
    label "zbiera&#263;"
  ]
  node [
    id 209
    label "convey"
  ]
  node [
    id 210
    label "opracowywa&#263;"
  ]
  node [
    id 211
    label "oszcz&#281;dza&#263;"
  ]
  node [
    id 212
    label "publicize"
  ]
  node [
    id 213
    label "przekazywa&#263;"
  ]
  node [
    id 214
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 215
    label "scala&#263;"
  ]
  node [
    id 216
    label "oddawa&#263;"
  ]
  node [
    id 217
    label "typ"
  ]
  node [
    id 218
    label "cz&#322;owiek"
  ]
  node [
    id 219
    label "zapis"
  ]
  node [
    id 220
    label "motyw"
  ]
  node [
    id 221
    label "ruch"
  ]
  node [
    id 222
    label "figure"
  ]
  node [
    id 223
    label "dekal"
  ]
  node [
    id 224
    label "ideal"
  ]
  node [
    id 225
    label "mildew"
  ]
  node [
    id 226
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 227
    label "twierdzenie"
  ]
  node [
    id 228
    label "my&#347;l"
  ]
  node [
    id 229
    label "wnioskowanie"
  ]
  node [
    id 230
    label "propozycja"
  ]
  node [
    id 231
    label "motion"
  ]
  node [
    id 232
    label "pismo"
  ]
  node [
    id 233
    label "prayer"
  ]
  node [
    id 234
    label "dop&#322;aci&#263;"
  ]
  node [
    id 235
    label "monta&#380;"
  ]
  node [
    id 236
    label "fabrication"
  ]
  node [
    id 237
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 238
    label "kreacja"
  ]
  node [
    id 239
    label "performance"
  ]
  node [
    id 240
    label "dzie&#322;o"
  ]
  node [
    id 241
    label "postprodukcja"
  ]
  node [
    id 242
    label "scheduling"
  ]
  node [
    id 243
    label "operacja"
  ]
  node [
    id 244
    label "dokument"
  ]
  node [
    id 245
    label "program_u&#380;ytkowy"
  ]
  node [
    id 246
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 247
    label "dokumentacja"
  ]
  node [
    id 248
    label "dodatek"
  ]
  node [
    id 249
    label "struktura"
  ]
  node [
    id 250
    label "stela&#380;"
  ]
  node [
    id 251
    label "human_body"
  ]
  node [
    id 252
    label "szablon"
  ]
  node [
    id 253
    label "oprawa"
  ]
  node [
    id 254
    label "paczka"
  ]
  node [
    id 255
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 256
    label "obramowanie"
  ]
  node [
    id 257
    label "pojazd"
  ]
  node [
    id 258
    label "postawa"
  ]
  node [
    id 259
    label "element_konstrukcyjny"
  ]
  node [
    id 260
    label "odinstalowanie"
  ]
  node [
    id 261
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 262
    label "emitowanie"
  ]
  node [
    id 263
    label "odinstalowywanie"
  ]
  node [
    id 264
    label "instrukcja"
  ]
  node [
    id 265
    label "teleferie"
  ]
  node [
    id 266
    label "emitowa&#263;"
  ]
  node [
    id 267
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 268
    label "sekcja_krytyczna"
  ]
  node [
    id 269
    label "prezentowa&#263;"
  ]
  node [
    id 270
    label "blok"
  ]
  node [
    id 271
    label "podprogram"
  ]
  node [
    id 272
    label "dzia&#322;"
  ]
  node [
    id 273
    label "broszura"
  ]
  node [
    id 274
    label "deklaracja"
  ]
  node [
    id 275
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 276
    label "struktura_organizacyjna"
  ]
  node [
    id 277
    label "zaprezentowanie"
  ]
  node [
    id 278
    label "informatyka"
  ]
  node [
    id 279
    label "booklet"
  ]
  node [
    id 280
    label "menu"
  ]
  node [
    id 281
    label "oprogramowanie"
  ]
  node [
    id 282
    label "instalowanie"
  ]
  node [
    id 283
    label "furkacja"
  ]
  node [
    id 284
    label "odinstalowa&#263;"
  ]
  node [
    id 285
    label "instalowa&#263;"
  ]
  node [
    id 286
    label "okno"
  ]
  node [
    id 287
    label "pirat"
  ]
  node [
    id 288
    label "zainstalowanie"
  ]
  node [
    id 289
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 290
    label "ogranicznik_referencyjny"
  ]
  node [
    id 291
    label "zainstalowa&#263;"
  ]
  node [
    id 292
    label "kana&#322;"
  ]
  node [
    id 293
    label "zaprezentowa&#263;"
  ]
  node [
    id 294
    label "interfejs"
  ]
  node [
    id 295
    label "odinstalowywa&#263;"
  ]
  node [
    id 296
    label "folder"
  ]
  node [
    id 297
    label "course_of_study"
  ]
  node [
    id 298
    label "ram&#243;wka"
  ]
  node [
    id 299
    label "prezentowanie"
  ]
  node [
    id 300
    label "oferta"
  ]
  node [
    id 301
    label "mo&#380;liwy"
  ]
  node [
    id 302
    label "medyczny"
  ]
  node [
    id 303
    label "operacyjnie"
  ]
  node [
    id 304
    label "restructure"
  ]
  node [
    id 305
    label "reorganizacja"
  ]
  node [
    id 306
    label "modernization"
  ]
  node [
    id 307
    label "ulepszenie"
  ]
  node [
    id 308
    label "klaster_dyskowy"
  ]
  node [
    id 309
    label "dziedzina"
  ]
  node [
    id 310
    label "widownia"
  ]
  node [
    id 311
    label "pas_planetoid"
  ]
  node [
    id 312
    label "wsch&#243;d"
  ]
  node [
    id 313
    label "Neogea"
  ]
  node [
    id 314
    label "holarktyka"
  ]
  node [
    id 315
    label "Rakowice"
  ]
  node [
    id 316
    label "Kosowo"
  ]
  node [
    id 317
    label "Syberia_Wschodnia"
  ]
  node [
    id 318
    label "wymiar"
  ]
  node [
    id 319
    label "p&#243;&#322;noc"
  ]
  node [
    id 320
    label "akrecja"
  ]
  node [
    id 321
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 322
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 323
    label "terytorium"
  ]
  node [
    id 324
    label "antroposfera"
  ]
  node [
    id 325
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 326
    label "zach&#243;d"
  ]
  node [
    id 327
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 328
    label "Olszanica"
  ]
  node [
    id 329
    label "Syberia_Zachodnia"
  ]
  node [
    id 330
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 331
    label "Ruda_Pabianicka"
  ]
  node [
    id 332
    label "Notogea"
  ]
  node [
    id 333
    label "&#321;&#281;g"
  ]
  node [
    id 334
    label "Antarktyka"
  ]
  node [
    id 335
    label "Piotrowo"
  ]
  node [
    id 336
    label "Zab&#322;ocie"
  ]
  node [
    id 337
    label "miejsce"
  ]
  node [
    id 338
    label "Pow&#261;zki"
  ]
  node [
    id 339
    label "Arktyka"
  ]
  node [
    id 340
    label "zbi&#243;r"
  ]
  node [
    id 341
    label "Ludwin&#243;w"
  ]
  node [
    id 342
    label "Zabu&#380;e"
  ]
  node [
    id 343
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 344
    label "Kresy_Zachodnie"
  ]
  node [
    id 345
    label "wsiowo"
  ]
  node [
    id 346
    label "po_wiejsku"
  ]
  node [
    id 347
    label "obciachowy"
  ]
  node [
    id 348
    label "wiejsko"
  ]
  node [
    id 349
    label "wsiowy"
  ]
  node [
    id 350
    label "nieatrakcyjny"
  ]
  node [
    id 351
    label "wie&#347;ny"
  ]
  node [
    id 352
    label "typowy"
  ]
  node [
    id 353
    label "wielko&#347;&#263;"
  ]
  node [
    id 354
    label "granica"
  ]
  node [
    id 355
    label "circle"
  ]
  node [
    id 356
    label "podzakres"
  ]
  node [
    id 357
    label "desygnat"
  ]
  node [
    id 358
    label "sfera"
  ]
  node [
    id 359
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 360
    label "ob&#322;awa_augustowska"
  ]
  node [
    id 361
    label "comfort"
  ]
  node [
    id 362
    label "u&#322;atwienie"
  ]
  node [
    id 363
    label "doch&#243;d"
  ]
  node [
    id 364
    label "oparcie"
  ]
  node [
    id 365
    label "telefon_zaufania"
  ]
  node [
    id 366
    label "dar"
  ]
  node [
    id 367
    label "zapomoga"
  ]
  node [
    id 368
    label "pocieszenie"
  ]
  node [
    id 369
    label "darowizna"
  ]
  node [
    id 370
    label "pomoc"
  ]
  node [
    id 371
    label "&#347;rodek"
  ]
  node [
    id 372
    label "wyci&#261;gni&#281;cie_pomocnej_d&#322;oni"
  ]
  node [
    id 373
    label "support"
  ]
  node [
    id 374
    label "consultancy"
  ]
  node [
    id 375
    label "us&#322;ugi"
  ]
  node [
    id 376
    label "specjalny"
  ]
  node [
    id 377
    label "rolniczo"
  ]
  node [
    id 378
    label "rynek"
  ]
  node [
    id 379
    label "zapoznawa&#263;"
  ]
  node [
    id 380
    label "take"
  ]
  node [
    id 381
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 382
    label "wchodzi&#263;"
  ]
  node [
    id 383
    label "begin"
  ]
  node [
    id 384
    label "wprawia&#263;"
  ]
  node [
    id 385
    label "schodzi&#263;"
  ]
  node [
    id 386
    label "wpisywa&#263;"
  ]
  node [
    id 387
    label "robi&#263;"
  ]
  node [
    id 388
    label "umieszcza&#263;"
  ]
  node [
    id 389
    label "induct"
  ]
  node [
    id 390
    label "zak&#322;&#243;ca&#263;"
  ]
  node [
    id 391
    label "zaczyna&#263;"
  ]
  node [
    id 392
    label "doprowadza&#263;"
  ]
  node [
    id 393
    label "inflict"
  ]
  node [
    id 394
    label "powodowa&#263;"
  ]
  node [
    id 395
    label "okre&#347;lony"
  ]
  node [
    id 396
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 397
    label "anatomopatolog"
  ]
  node [
    id 398
    label "rewizja"
  ]
  node [
    id 399
    label "oznaka"
  ]
  node [
    id 400
    label "ferment"
  ]
  node [
    id 401
    label "komplet"
  ]
  node [
    id 402
    label "tura"
  ]
  node [
    id 403
    label "amendment"
  ]
  node [
    id 404
    label "zmianka"
  ]
  node [
    id 405
    label "odmienianie"
  ]
  node [
    id 406
    label "passage"
  ]
  node [
    id 407
    label "zjawisko"
  ]
  node [
    id 408
    label "change"
  ]
  node [
    id 409
    label "praca"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 11
  ]
  edge [
    source 6
    target 19
  ]
  edge [
    source 6
    target 20
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 27
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 27
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 17
  ]
  edge [
    source 9
    target 18
  ]
  edge [
    source 9
    target 35
  ]
  edge [
    source 9
    target 36
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 33
  ]
  edge [
    source 9
    target 16
  ]
  edge [
    source 9
    target 29
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 42
  ]
  edge [
    source 10
    target 129
  ]
  edge [
    source 10
    target 130
  ]
  edge [
    source 10
    target 131
  ]
  edge [
    source 10
    target 132
  ]
  edge [
    source 10
    target 133
  ]
  edge [
    source 10
    target 134
  ]
  edge [
    source 10
    target 135
  ]
  edge [
    source 10
    target 136
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 43
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 137
  ]
  edge [
    source 12
    target 138
  ]
  edge [
    source 12
    target 139
  ]
  edge [
    source 12
    target 140
  ]
  edge [
    source 12
    target 141
  ]
  edge [
    source 12
    target 24
  ]
  edge [
    source 12
    target 44
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 43
  ]
  edge [
    source 13
    target 44
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 142
  ]
  edge [
    source 14
    target 143
  ]
  edge [
    source 14
    target 144
  ]
  edge [
    source 14
    target 145
  ]
  edge [
    source 14
    target 146
  ]
  edge [
    source 14
    target 147
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 148
  ]
  edge [
    source 15
    target 149
  ]
  edge [
    source 15
    target 66
  ]
  edge [
    source 15
    target 150
  ]
  edge [
    source 15
    target 151
  ]
  edge [
    source 15
    target 152
  ]
  edge [
    source 15
    target 153
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 154
  ]
  edge [
    source 16
    target 155
  ]
  edge [
    source 16
    target 156
  ]
  edge [
    source 16
    target 157
  ]
  edge [
    source 16
    target 29
  ]
  edge [
    source 17
    target 158
  ]
  edge [
    source 17
    target 159
  ]
  edge [
    source 17
    target 160
  ]
  edge [
    source 17
    target 161
  ]
  edge [
    source 17
    target 162
  ]
  edge [
    source 17
    target 163
  ]
  edge [
    source 17
    target 164
  ]
  edge [
    source 17
    target 165
  ]
  edge [
    source 17
    target 166
  ]
  edge [
    source 17
    target 167
  ]
  edge [
    source 17
    target 168
  ]
  edge [
    source 17
    target 169
  ]
  edge [
    source 17
    target 170
  ]
  edge [
    source 17
    target 171
  ]
  edge [
    source 17
    target 172
  ]
  edge [
    source 17
    target 173
  ]
  edge [
    source 17
    target 174
  ]
  edge [
    source 17
    target 175
  ]
  edge [
    source 17
    target 176
  ]
  edge [
    source 19
    target 99
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 177
  ]
  edge [
    source 20
    target 178
  ]
  edge [
    source 20
    target 179
  ]
  edge [
    source 20
    target 180
  ]
  edge [
    source 20
    target 181
  ]
  edge [
    source 20
    target 182
  ]
  edge [
    source 20
    target 183
  ]
  edge [
    source 20
    target 184
  ]
  edge [
    source 20
    target 185
  ]
  edge [
    source 20
    target 186
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 187
  ]
  edge [
    source 21
    target 188
  ]
  edge [
    source 21
    target 189
  ]
  edge [
    source 21
    target 190
  ]
  edge [
    source 21
    target 191
  ]
  edge [
    source 21
    target 192
  ]
  edge [
    source 21
    target 193
  ]
  edge [
    source 21
    target 194
  ]
  edge [
    source 21
    target 195
  ]
  edge [
    source 21
    target 196
  ]
  edge [
    source 21
    target 30
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 197
  ]
  edge [
    source 22
    target 198
  ]
  edge [
    source 22
    target 199
  ]
  edge [
    source 22
    target 200
  ]
  edge [
    source 22
    target 201
  ]
  edge [
    source 22
    target 202
  ]
  edge [
    source 22
    target 203
  ]
  edge [
    source 22
    target 204
  ]
  edge [
    source 22
    target 205
  ]
  edge [
    source 22
    target 206
  ]
  edge [
    source 22
    target 207
  ]
  edge [
    source 22
    target 208
  ]
  edge [
    source 22
    target 209
  ]
  edge [
    source 22
    target 210
  ]
  edge [
    source 22
    target 211
  ]
  edge [
    source 22
    target 212
  ]
  edge [
    source 22
    target 213
  ]
  edge [
    source 22
    target 214
  ]
  edge [
    source 22
    target 215
  ]
  edge [
    source 22
    target 216
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 217
  ]
  edge [
    source 23
    target 218
  ]
  edge [
    source 23
    target 148
  ]
  edge [
    source 23
    target 27
  ]
  edge [
    source 23
    target 219
  ]
  edge [
    source 23
    target 220
  ]
  edge [
    source 23
    target 221
  ]
  edge [
    source 23
    target 222
  ]
  edge [
    source 23
    target 223
  ]
  edge [
    source 23
    target 224
  ]
  edge [
    source 23
    target 225
  ]
  edge [
    source 23
    target 196
  ]
  edge [
    source 23
    target 226
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 227
  ]
  edge [
    source 24
    target 228
  ]
  edge [
    source 24
    target 229
  ]
  edge [
    source 24
    target 230
  ]
  edge [
    source 24
    target 231
  ]
  edge [
    source 24
    target 232
  ]
  edge [
    source 24
    target 233
  ]
  edge [
    source 24
    target 44
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 234
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 235
  ]
  edge [
    source 26
    target 236
  ]
  edge [
    source 26
    target 237
  ]
  edge [
    source 26
    target 238
  ]
  edge [
    source 26
    target 239
  ]
  edge [
    source 26
    target 240
  ]
  edge [
    source 26
    target 125
  ]
  edge [
    source 26
    target 241
  ]
  edge [
    source 26
    target 242
  ]
  edge [
    source 26
    target 243
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 244
  ]
  edge [
    source 27
    target 108
  ]
  edge [
    source 27
    target 245
  ]
  edge [
    source 27
    target 114
  ]
  edge [
    source 27
    target 115
  ]
  edge [
    source 27
    target 56
  ]
  edge [
    source 27
    target 246
  ]
  edge [
    source 27
    target 247
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 38
  ]
  edge [
    source 28
    target 248
  ]
  edge [
    source 28
    target 249
  ]
  edge [
    source 28
    target 250
  ]
  edge [
    source 28
    target 50
  ]
  edge [
    source 28
    target 251
  ]
  edge [
    source 28
    target 252
  ]
  edge [
    source 28
    target 253
  ]
  edge [
    source 28
    target 254
  ]
  edge [
    source 28
    target 255
  ]
  edge [
    source 28
    target 256
  ]
  edge [
    source 28
    target 257
  ]
  edge [
    source 28
    target 258
  ]
  edge [
    source 28
    target 259
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 129
  ]
  edge [
    source 30
    target 260
  ]
  edge [
    source 30
    target 261
  ]
  edge [
    source 30
    target 50
  ]
  edge [
    source 30
    target 262
  ]
  edge [
    source 30
    target 263
  ]
  edge [
    source 30
    target 264
  ]
  edge [
    source 30
    target 116
  ]
  edge [
    source 30
    target 265
  ]
  edge [
    source 30
    target 266
  ]
  edge [
    source 30
    target 110
  ]
  edge [
    source 30
    target 267
  ]
  edge [
    source 30
    target 268
  ]
  edge [
    source 30
    target 269
  ]
  edge [
    source 30
    target 270
  ]
  edge [
    source 30
    target 271
  ]
  edge [
    source 30
    target 272
  ]
  edge [
    source 30
    target 273
  ]
  edge [
    source 30
    target 274
  ]
  edge [
    source 30
    target 275
  ]
  edge [
    source 30
    target 276
  ]
  edge [
    source 30
    target 277
  ]
  edge [
    source 30
    target 278
  ]
  edge [
    source 30
    target 279
  ]
  edge [
    source 30
    target 280
  ]
  edge [
    source 30
    target 281
  ]
  edge [
    source 30
    target 282
  ]
  edge [
    source 30
    target 283
  ]
  edge [
    source 30
    target 284
  ]
  edge [
    source 30
    target 285
  ]
  edge [
    source 30
    target 286
  ]
  edge [
    source 30
    target 287
  ]
  edge [
    source 30
    target 288
  ]
  edge [
    source 30
    target 289
  ]
  edge [
    source 30
    target 290
  ]
  edge [
    source 30
    target 291
  ]
  edge [
    source 30
    target 292
  ]
  edge [
    source 30
    target 293
  ]
  edge [
    source 30
    target 294
  ]
  edge [
    source 30
    target 295
  ]
  edge [
    source 30
    target 296
  ]
  edge [
    source 30
    target 297
  ]
  edge [
    source 30
    target 298
  ]
  edge [
    source 30
    target 299
  ]
  edge [
    source 30
    target 300
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 301
  ]
  edge [
    source 31
    target 302
  ]
  edge [
    source 31
    target 303
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 304
  ]
  edge [
    source 32
    target 305
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 306
  ]
  edge [
    source 33
    target 307
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 308
  ]
  edge [
    source 34
    target 309
  ]
  edge [
    source 34
    target 255
  ]
  edge [
    source 34
    target 310
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 311
  ]
  edge [
    source 36
    target 312
  ]
  edge [
    source 36
    target 313
  ]
  edge [
    source 36
    target 314
  ]
  edge [
    source 36
    target 315
  ]
  edge [
    source 36
    target 316
  ]
  edge [
    source 36
    target 317
  ]
  edge [
    source 36
    target 318
  ]
  edge [
    source 36
    target 319
  ]
  edge [
    source 36
    target 320
  ]
  edge [
    source 36
    target 321
  ]
  edge [
    source 36
    target 322
  ]
  edge [
    source 36
    target 323
  ]
  edge [
    source 36
    target 324
  ]
  edge [
    source 36
    target 325
  ]
  edge [
    source 36
    target 86
  ]
  edge [
    source 36
    target 326
  ]
  edge [
    source 36
    target 327
  ]
  edge [
    source 36
    target 328
  ]
  edge [
    source 36
    target 329
  ]
  edge [
    source 36
    target 112
  ]
  edge [
    source 36
    target 330
  ]
  edge [
    source 36
    target 331
  ]
  edge [
    source 36
    target 332
  ]
  edge [
    source 36
    target 333
  ]
  edge [
    source 36
    target 334
  ]
  edge [
    source 36
    target 335
  ]
  edge [
    source 36
    target 336
  ]
  edge [
    source 36
    target 38
  ]
  edge [
    source 36
    target 337
  ]
  edge [
    source 36
    target 338
  ]
  edge [
    source 36
    target 339
  ]
  edge [
    source 36
    target 340
  ]
  edge [
    source 36
    target 341
  ]
  edge [
    source 36
    target 342
  ]
  edge [
    source 36
    target 343
  ]
  edge [
    source 36
    target 344
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 345
  ]
  edge [
    source 37
    target 346
  ]
  edge [
    source 37
    target 347
  ]
  edge [
    source 37
    target 348
  ]
  edge [
    source 37
    target 349
  ]
  edge [
    source 37
    target 350
  ]
  edge [
    source 37
    target 351
  ]
  edge [
    source 37
    target 352
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 353
  ]
  edge [
    source 38
    target 261
  ]
  edge [
    source 38
    target 354
  ]
  edge [
    source 38
    target 355
  ]
  edge [
    source 38
    target 356
  ]
  edge [
    source 38
    target 340
  ]
  edge [
    source 38
    target 357
  ]
  edge [
    source 38
    target 358
  ]
  edge [
    source 38
    target 309
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 47
  ]
  edge [
    source 39
    target 359
  ]
  edge [
    source 39
    target 237
  ]
  edge [
    source 39
    target 360
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 361
  ]
  edge [
    source 40
    target 362
  ]
  edge [
    source 40
    target 363
  ]
  edge [
    source 40
    target 364
  ]
  edge [
    source 40
    target 365
  ]
  edge [
    source 40
    target 366
  ]
  edge [
    source 40
    target 367
  ]
  edge [
    source 40
    target 368
  ]
  edge [
    source 40
    target 369
  ]
  edge [
    source 40
    target 370
  ]
  edge [
    source 40
    target 371
  ]
  edge [
    source 40
    target 372
  ]
  edge [
    source 40
    target 373
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 374
  ]
  edge [
    source 41
    target 375
  ]
  edge [
    source 41
    target 370
  ]
  edge [
    source 42
    target 376
  ]
  edge [
    source 42
    target 377
  ]
  edge [
    source 43
    target 378
  ]
  edge [
    source 43
    target 379
  ]
  edge [
    source 43
    target 380
  ]
  edge [
    source 43
    target 381
  ]
  edge [
    source 43
    target 382
  ]
  edge [
    source 43
    target 383
  ]
  edge [
    source 43
    target 384
  ]
  edge [
    source 43
    target 385
  ]
  edge [
    source 43
    target 386
  ]
  edge [
    source 43
    target 387
  ]
  edge [
    source 43
    target 388
  ]
  edge [
    source 43
    target 389
  ]
  edge [
    source 43
    target 390
  ]
  edge [
    source 43
    target 391
  ]
  edge [
    source 43
    target 392
  ]
  edge [
    source 43
    target 393
  ]
  edge [
    source 43
    target 394
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 395
  ]
  edge [
    source 44
    target 396
  ]
  edge [
    source 45
    target 397
  ]
  edge [
    source 45
    target 398
  ]
  edge [
    source 45
    target 399
  ]
  edge [
    source 45
    target 73
  ]
  edge [
    source 45
    target 400
  ]
  edge [
    source 45
    target 401
  ]
  edge [
    source 45
    target 402
  ]
  edge [
    source 45
    target 403
  ]
  edge [
    source 45
    target 404
  ]
  edge [
    source 45
    target 405
  ]
  edge [
    source 45
    target 406
  ]
  edge [
    source 45
    target 407
  ]
  edge [
    source 45
    target 408
  ]
  edge [
    source 45
    target 409
  ]
]
