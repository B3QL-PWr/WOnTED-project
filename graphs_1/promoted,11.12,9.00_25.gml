graph [
  maxDegree 42
  minDegree 1
  meanDegree 2
  density 0.020833333333333332
  graphCliqueNumber 3
  node [
    id 0
    label "telewizja"
    origin "text"
  ]
  node [
    id 1
    label "cnn"
    origin "text"
  ]
  node [
    id 2
    label "dotrze&#263;"
    origin "text"
  ]
  node [
    id 3
    label "nagranie"
    origin "text"
  ]
  node [
    id 4
    label "d&#378;wi&#281;kowy"
    origin "text"
  ]
  node [
    id 5
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 6
    label "dokumentowa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "ostatnie"
    origin "text"
  ]
  node [
    id 8
    label "chwila"
    origin "text"
  ]
  node [
    id 9
    label "&#380;ycie"
    origin "text"
  ]
  node [
    id 10
    label "d&#380;amala"
    origin "text"
  ]
  node [
    id 11
    label "chaszod&#380;d&#380;iego"
    origin "text"
  ]
  node [
    id 12
    label "Polsat"
  ]
  node [
    id 13
    label "paj&#281;czarz"
  ]
  node [
    id 14
    label "gadaj&#261;ca_g&#322;owa"
  ]
  node [
    id 15
    label "programowiec"
  ]
  node [
    id 16
    label "technologia"
  ]
  node [
    id 17
    label "urz&#261;dzenie_RTV"
  ]
  node [
    id 18
    label "Interwizja"
  ]
  node [
    id 19
    label "BBC"
  ]
  node [
    id 20
    label "ekran"
  ]
  node [
    id 21
    label "redakcja"
  ]
  node [
    id 22
    label "media"
  ]
  node [
    id 23
    label "odbieranie"
  ]
  node [
    id 24
    label "odbiera&#263;"
  ]
  node [
    id 25
    label "odbiornik"
  ]
  node [
    id 26
    label "instytucja"
  ]
  node [
    id 27
    label "du&#380;y_pok&#243;j"
  ]
  node [
    id 28
    label "studio"
  ]
  node [
    id 29
    label "telekomunikacja"
  ]
  node [
    id 30
    label "muza"
  ]
  node [
    id 31
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 32
    label "get"
  ]
  node [
    id 33
    label "utrze&#263;"
  ]
  node [
    id 34
    label "spowodowa&#263;"
  ]
  node [
    id 35
    label "catch"
  ]
  node [
    id 36
    label "wyg&#322;adzi&#263;"
  ]
  node [
    id 37
    label "become"
  ]
  node [
    id 38
    label "skontaktowa&#263;_si&#281;"
  ]
  node [
    id 39
    label "znale&#378;&#263;"
  ]
  node [
    id 40
    label "dorobi&#263;"
  ]
  node [
    id 41
    label "advance"
  ]
  node [
    id 42
    label "dopasowa&#263;"
  ]
  node [
    id 43
    label "rozprzestrzeni&#263;_si&#281;"
  ]
  node [
    id 44
    label "silnik"
  ]
  node [
    id 45
    label "wys&#322;uchanie"
  ]
  node [
    id 46
    label "wytw&#243;r"
  ]
  node [
    id 47
    label "wys&#322;ucha&#263;"
  ]
  node [
    id 48
    label "recording"
  ]
  node [
    id 49
    label "utrwalenie"
  ]
  node [
    id 50
    label "d&#378;wi&#281;kowo"
  ]
  node [
    id 51
    label "suffice"
  ]
  node [
    id 52
    label "akustycznie"
  ]
  node [
    id 53
    label "testify"
  ]
  node [
    id 54
    label "udowadnia&#263;"
  ]
  node [
    id 55
    label "potwierdza&#263;"
  ]
  node [
    id 56
    label "czas"
  ]
  node [
    id 57
    label "time"
  ]
  node [
    id 58
    label "energy"
  ]
  node [
    id 59
    label "bycie"
  ]
  node [
    id 60
    label "zegar_biologiczny"
  ]
  node [
    id 61
    label "okres_noworodkowy"
  ]
  node [
    id 62
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 63
    label "entity"
  ]
  node [
    id 64
    label "prze&#380;ywanie"
  ]
  node [
    id 65
    label "prze&#380;ycie"
  ]
  node [
    id 66
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 67
    label "wiek_matuzalemowy"
  ]
  node [
    id 68
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 69
    label "dzieci&#324;stwo"
  ]
  node [
    id 70
    label "power"
  ]
  node [
    id 71
    label "szwung"
  ]
  node [
    id 72
    label "menopauza"
  ]
  node [
    id 73
    label "umarcie"
  ]
  node [
    id 74
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 75
    label "life"
  ]
  node [
    id 76
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 77
    label "&#380;ywy"
  ]
  node [
    id 78
    label "rozw&#243;j"
  ]
  node [
    id 79
    label "po&#322;&#243;g"
  ]
  node [
    id 80
    label "byt"
  ]
  node [
    id 81
    label "przebywanie"
  ]
  node [
    id 82
    label "subsistence"
  ]
  node [
    id 83
    label "koleje_losu"
  ]
  node [
    id 84
    label "raj_utracony"
  ]
  node [
    id 85
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 86
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 87
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 88
    label "andropauza"
  ]
  node [
    id 89
    label "warunki"
  ]
  node [
    id 90
    label "do&#380;ywanie"
  ]
  node [
    id 91
    label "niemowl&#281;ctwo"
  ]
  node [
    id 92
    label "umieranie"
  ]
  node [
    id 93
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 94
    label "staro&#347;&#263;"
  ]
  node [
    id 95
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 96
    label "&#347;mier&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 9
    target 56
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 10
    target 11
  ]
]
