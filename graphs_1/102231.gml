graph [
  maxDegree 45
  minDegree 1
  meanDegree 2.18348623853211
  density 0.00669781054764451
  graphCliqueNumber 4
  node [
    id 0
    label "raz"
    origin "text"
  ]
  node [
    id 1
    label "konieczno&#347;&#263;"
    origin "text"
  ]
  node [
    id 2
    label "szpital"
    origin "text"
  ]
  node [
    id 3
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 4
    label "znajdowa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "si&#281;"
    origin "text"
  ]
  node [
    id 6
    label "szpitalny"
    origin "text"
  ]
  node [
    id 7
    label "oddzia&#322;"
    origin "text"
  ]
  node [
    id 8
    label "ratunkowy"
    origin "text"
  ]
  node [
    id 9
    label "lub"
    origin "text"
  ]
  node [
    id 10
    label "jednostka"
    origin "text"
  ]
  node [
    id 11
    label "organizacyjny"
    origin "text"
  ]
  node [
    id 12
    label "wyspecjalizowa&#263;"
    origin "text"
  ]
  node [
    id 13
    label "zakres"
    origin "text"
  ]
  node [
    id 14
    label "udziela&#263;"
    origin "text"
  ]
  node [
    id 15
    label "&#347;wiadczenie"
    origin "text"
  ]
  node [
    id 16
    label "zdrowotny"
    origin "text"
  ]
  node [
    id 17
    label "niezb&#281;dny"
    origin "text"
  ]
  node [
    id 18
    label "dla"
    origin "text"
  ]
  node [
    id 19
    label "ratownictwo"
    origin "text"
  ]
  node [
    id 20
    label "medyczny"
    origin "text"
  ]
  node [
    id 21
    label "zapewnia&#263;"
    origin "text"
  ]
  node [
    id 22
    label "niezw&#322;oczny"
    origin "text"
  ]
  node [
    id 23
    label "transport"
    origin "text"
  ]
  node [
    id 24
    label "sanitarny"
    origin "text"
  ]
  node [
    id 25
    label "osoba"
    origin "text"
  ]
  node [
    id 26
    label "stan"
    origin "text"
  ]
  node [
    id 27
    label "nag&#322;y"
    origin "text"
  ]
  node [
    id 28
    label "zagro&#380;enie"
    origin "text"
  ]
  node [
    id 29
    label "bliski"
    origin "text"
  ]
  node [
    id 30
    label "zak&#322;ad"
    origin "text"
  ]
  node [
    id 31
    label "opieka"
    origin "text"
  ]
  node [
    id 32
    label "odpowiedni"
    origin "text"
  ]
  node [
    id 33
    label "chwila"
  ]
  node [
    id 34
    label "uderzenie"
  ]
  node [
    id 35
    label "cios"
  ]
  node [
    id 36
    label "time"
  ]
  node [
    id 37
    label "przymus"
  ]
  node [
    id 38
    label "obligatoryjno&#347;&#263;"
  ]
  node [
    id 39
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 40
    label "wym&#243;g"
  ]
  node [
    id 41
    label "operator_modalny"
  ]
  node [
    id 42
    label "szpitalnictwo"
  ]
  node [
    id 43
    label "oddzia&#322;_septyczny"
  ]
  node [
    id 44
    label "klinicysta"
  ]
  node [
    id 45
    label "zabieg&#243;wka"
  ]
  node [
    id 46
    label "izba_chorych"
  ]
  node [
    id 47
    label "&#322;&#243;&#380;eczko_nadziei"
  ]
  node [
    id 48
    label "blok_operacyjny"
  ]
  node [
    id 49
    label "instytucja"
  ]
  node [
    id 50
    label "centrum_urazowe"
  ]
  node [
    id 51
    label "plac&#243;wka_s&#322;u&#380;by_zdrowia"
  ]
  node [
    id 52
    label "kostnica"
  ]
  node [
    id 53
    label "sala_chorych"
  ]
  node [
    id 54
    label "doznawa&#263;"
  ]
  node [
    id 55
    label "znachodzi&#263;"
  ]
  node [
    id 56
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 57
    label "pozyskiwa&#263;"
  ]
  node [
    id 58
    label "odzyskiwa&#263;"
  ]
  node [
    id 59
    label "os&#261;dza&#263;"
  ]
  node [
    id 60
    label "wykrywa&#263;"
  ]
  node [
    id 61
    label "unwrap"
  ]
  node [
    id 62
    label "detect"
  ]
  node [
    id 63
    label "wymy&#347;la&#263;"
  ]
  node [
    id 64
    label "powodowa&#263;"
  ]
  node [
    id 65
    label "szpitalnie"
  ]
  node [
    id 66
    label "typowy"
  ]
  node [
    id 67
    label "whole"
  ]
  node [
    id 68
    label "jednostka_geologiczna"
  ]
  node [
    id 69
    label "system"
  ]
  node [
    id 70
    label "poziom"
  ]
  node [
    id 71
    label "agencja"
  ]
  node [
    id 72
    label "dogger"
  ]
  node [
    id 73
    label "formacja"
  ]
  node [
    id 74
    label "pi&#281;tro"
  ]
  node [
    id 75
    label "filia"
  ]
  node [
    id 76
    label "dzia&#322;"
  ]
  node [
    id 77
    label "promocja"
  ]
  node [
    id 78
    label "zesp&#243;&#322;"
  ]
  node [
    id 79
    label "kurs"
  ]
  node [
    id 80
    label "przegrupowywa&#263;_si&#281;"
  ]
  node [
    id 81
    label "wojsko"
  ]
  node [
    id 82
    label "siedziba"
  ]
  node [
    id 83
    label "bank"
  ]
  node [
    id 84
    label "lias"
  ]
  node [
    id 85
    label "przegrupowywanie_si&#281;"
  ]
  node [
    id 86
    label "malm"
  ]
  node [
    id 87
    label "ajencja"
  ]
  node [
    id 88
    label "klasa"
  ]
  node [
    id 89
    label "infimum"
  ]
  node [
    id 90
    label "ewoluowanie"
  ]
  node [
    id 91
    label "przyswoi&#263;"
  ]
  node [
    id 92
    label "reakcja"
  ]
  node [
    id 93
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 94
    label "wyewoluowanie"
  ]
  node [
    id 95
    label "individual"
  ]
  node [
    id 96
    label "profanum"
  ]
  node [
    id 97
    label "starzenie_si&#281;"
  ]
  node [
    id 98
    label "homo_sapiens"
  ]
  node [
    id 99
    label "skala"
  ]
  node [
    id 100
    label "supremum"
  ]
  node [
    id 101
    label "przyswaja&#263;"
  ]
  node [
    id 102
    label "ludzko&#347;&#263;"
  ]
  node [
    id 103
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 104
    label "one"
  ]
  node [
    id 105
    label "funkcja"
  ]
  node [
    id 106
    label "przeliczenie"
  ]
  node [
    id 107
    label "przeliczanie"
  ]
  node [
    id 108
    label "mikrokosmos"
  ]
  node [
    id 109
    label "rzut"
  ]
  node [
    id 110
    label "portrecista"
  ]
  node [
    id 111
    label "przelicza&#263;"
  ]
  node [
    id 112
    label "przyswajanie"
  ]
  node [
    id 113
    label "duch"
  ]
  node [
    id 114
    label "wyewoluowa&#263;"
  ]
  node [
    id 115
    label "ewoluowa&#263;"
  ]
  node [
    id 116
    label "oddzia&#322;ywanie"
  ]
  node [
    id 117
    label "g&#322;owa"
  ]
  node [
    id 118
    label "liczba_naturalna"
  ]
  node [
    id 119
    label "poj&#281;cie"
  ]
  node [
    id 120
    label "figura"
  ]
  node [
    id 121
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 122
    label "obiekt"
  ]
  node [
    id 123
    label "matematyka"
  ]
  node [
    id 124
    label "przyswojenie"
  ]
  node [
    id 125
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 126
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 127
    label "czynnik_biotyczny"
  ]
  node [
    id 128
    label "przeliczy&#263;"
  ]
  node [
    id 129
    label "antropochoria"
  ]
  node [
    id 130
    label "ukierunkowa&#263;"
  ]
  node [
    id 131
    label "wyszkoli&#263;"
  ]
  node [
    id 132
    label "specify"
  ]
  node [
    id 133
    label "wielko&#347;&#263;"
  ]
  node [
    id 134
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 135
    label "granica"
  ]
  node [
    id 136
    label "circle"
  ]
  node [
    id 137
    label "podzakres"
  ]
  node [
    id 138
    label "zbi&#243;r"
  ]
  node [
    id 139
    label "desygnat"
  ]
  node [
    id 140
    label "sfera"
  ]
  node [
    id 141
    label "dziedzina"
  ]
  node [
    id 142
    label "zezwala&#263;"
  ]
  node [
    id 143
    label "dawa&#263;"
  ]
  node [
    id 144
    label "assign"
  ]
  node [
    id 145
    label "render"
  ]
  node [
    id 146
    label "accord"
  ]
  node [
    id 147
    label "odst&#281;powa&#263;"
  ]
  node [
    id 148
    label "przyznawa&#263;"
  ]
  node [
    id 149
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 150
    label "p&#322;acenie"
  ]
  node [
    id 151
    label "znaczenie"
  ]
  node [
    id 152
    label "sk&#322;adanie"
  ]
  node [
    id 153
    label "service"
  ]
  node [
    id 154
    label "czynienie_dobra"
  ]
  node [
    id 155
    label "informowanie"
  ]
  node [
    id 156
    label "command"
  ]
  node [
    id 157
    label "opowiadanie"
  ]
  node [
    id 158
    label "koszt_rodzajowy"
  ]
  node [
    id 159
    label "pracowanie"
  ]
  node [
    id 160
    label "przekonywanie"
  ]
  node [
    id 161
    label "wyraz"
  ]
  node [
    id 162
    label "us&#322;uga"
  ]
  node [
    id 163
    label "performance"
  ]
  node [
    id 164
    label "zobowi&#261;zanie"
  ]
  node [
    id 165
    label "zdrowy"
  ]
  node [
    id 166
    label "dobry"
  ]
  node [
    id 167
    label "prozdrowotny"
  ]
  node [
    id 168
    label "zdrowotnie"
  ]
  node [
    id 169
    label "niezb&#281;dnie"
  ]
  node [
    id 170
    label "GOPR"
  ]
  node [
    id 171
    label "us&#322;uga_spo&#322;eczna"
  ]
  node [
    id 172
    label "specjalny"
  ]
  node [
    id 173
    label "praktyczny"
  ]
  node [
    id 174
    label "paramedyczny"
  ]
  node [
    id 175
    label "zgodny"
  ]
  node [
    id 176
    label "profilowy"
  ]
  node [
    id 177
    label "leczniczy"
  ]
  node [
    id 178
    label "lekarsko"
  ]
  node [
    id 179
    label "bia&#322;y"
  ]
  node [
    id 180
    label "medycznie"
  ]
  node [
    id 181
    label "specjalistyczny"
  ]
  node [
    id 182
    label "utrzymywa&#263;"
  ]
  node [
    id 183
    label "dostarcza&#263;"
  ]
  node [
    id 184
    label "informowa&#263;"
  ]
  node [
    id 185
    label "deliver"
  ]
  node [
    id 186
    label "niezw&#322;ocznie"
  ]
  node [
    id 187
    label "natychmiastowy"
  ]
  node [
    id 188
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 189
    label "zawarto&#347;&#263;"
  ]
  node [
    id 190
    label "unos"
  ]
  node [
    id 191
    label "traffic"
  ]
  node [
    id 192
    label "za&#322;adunek"
  ]
  node [
    id 193
    label "gospodarka"
  ]
  node [
    id 194
    label "roz&#322;adunek"
  ]
  node [
    id 195
    label "grupa"
  ]
  node [
    id 196
    label "sprz&#281;t"
  ]
  node [
    id 197
    label "jednoszynowy"
  ]
  node [
    id 198
    label "cedu&#322;a"
  ]
  node [
    id 199
    label "tyfon"
  ]
  node [
    id 200
    label "prze&#322;adunek"
  ]
  node [
    id 201
    label "komunikacja"
  ]
  node [
    id 202
    label "towar"
  ]
  node [
    id 203
    label "Zgredek"
  ]
  node [
    id 204
    label "kategoria_gramatyczna"
  ]
  node [
    id 205
    label "Casanova"
  ]
  node [
    id 206
    label "Don_Juan"
  ]
  node [
    id 207
    label "Gargantua"
  ]
  node [
    id 208
    label "Faust"
  ]
  node [
    id 209
    label "Chocho&#322;"
  ]
  node [
    id 210
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 211
    label "koniugacja"
  ]
  node [
    id 212
    label "Winnetou"
  ]
  node [
    id 213
    label "Dwukwiat"
  ]
  node [
    id 214
    label "Edyp"
  ]
  node [
    id 215
    label "Herkules_Poirot"
  ]
  node [
    id 216
    label "person"
  ]
  node [
    id 217
    label "Sherlock_Holmes"
  ]
  node [
    id 218
    label "Szwejk"
  ]
  node [
    id 219
    label "Hamlet"
  ]
  node [
    id 220
    label "Quasimodo"
  ]
  node [
    id 221
    label "Dulcynea"
  ]
  node [
    id 222
    label "Don_Kiszot"
  ]
  node [
    id 223
    label "Wallenrod"
  ]
  node [
    id 224
    label "Plastu&#347;"
  ]
  node [
    id 225
    label "Harry_Potter"
  ]
  node [
    id 226
    label "parali&#380;owa&#263;"
  ]
  node [
    id 227
    label "istota"
  ]
  node [
    id 228
    label "Werter"
  ]
  node [
    id 229
    label "posta&#263;"
  ]
  node [
    id 230
    label "Arizona"
  ]
  node [
    id 231
    label "Georgia"
  ]
  node [
    id 232
    label "warstwa"
  ]
  node [
    id 233
    label "jednostka_administracyjna"
  ]
  node [
    id 234
    label "Hawaje"
  ]
  node [
    id 235
    label "Goa"
  ]
  node [
    id 236
    label "Floryda"
  ]
  node [
    id 237
    label "Oklahoma"
  ]
  node [
    id 238
    label "punkt"
  ]
  node [
    id 239
    label "Alaska"
  ]
  node [
    id 240
    label "wci&#281;cie"
  ]
  node [
    id 241
    label "Alabama"
  ]
  node [
    id 242
    label "Oregon"
  ]
  node [
    id 243
    label "by&#263;"
  ]
  node [
    id 244
    label "Teksas"
  ]
  node [
    id 245
    label "Illinois"
  ]
  node [
    id 246
    label "Waszyngton"
  ]
  node [
    id 247
    label "Jukatan"
  ]
  node [
    id 248
    label "shape"
  ]
  node [
    id 249
    label "Nowy_Meksyk"
  ]
  node [
    id 250
    label "ilo&#347;&#263;"
  ]
  node [
    id 251
    label "state"
  ]
  node [
    id 252
    label "Nowy_York"
  ]
  node [
    id 253
    label "Arakan"
  ]
  node [
    id 254
    label "Kalifornia"
  ]
  node [
    id 255
    label "wektor"
  ]
  node [
    id 256
    label "Massachusetts"
  ]
  node [
    id 257
    label "miejsce"
  ]
  node [
    id 258
    label "Pensylwania"
  ]
  node [
    id 259
    label "Michigan"
  ]
  node [
    id 260
    label "Maryland"
  ]
  node [
    id 261
    label "Ohio"
  ]
  node [
    id 262
    label "Kansas"
  ]
  node [
    id 263
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 264
    label "Luizjana"
  ]
  node [
    id 265
    label "samopoczucie"
  ]
  node [
    id 266
    label "Wirginia"
  ]
  node [
    id 267
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 268
    label "nieoczekiwany"
  ]
  node [
    id 269
    label "raptownie"
  ]
  node [
    id 270
    label "pilnie"
  ]
  node [
    id 271
    label "zawrze&#263;"
  ]
  node [
    id 272
    label "zawrzenie"
  ]
  node [
    id 273
    label "szybki"
  ]
  node [
    id 274
    label "uprzedzenie"
  ]
  node [
    id 275
    label "zawisa&#263;"
  ]
  node [
    id 276
    label "zawisanie"
  ]
  node [
    id 277
    label "szachowanie"
  ]
  node [
    id 278
    label "cecha"
  ]
  node [
    id 279
    label "zaszachowanie"
  ]
  node [
    id 280
    label "emergency"
  ]
  node [
    id 281
    label "zaistnienie"
  ]
  node [
    id 282
    label "czarny_punkt"
  ]
  node [
    id 283
    label "zagrozi&#263;"
  ]
  node [
    id 284
    label "cz&#322;owiek"
  ]
  node [
    id 285
    label "blisko"
  ]
  node [
    id 286
    label "przesz&#322;y"
  ]
  node [
    id 287
    label "gotowy"
  ]
  node [
    id 288
    label "dok&#322;adny"
  ]
  node [
    id 289
    label "kr&#243;tki"
  ]
  node [
    id 290
    label "znajomy"
  ]
  node [
    id 291
    label "przysz&#322;y"
  ]
  node [
    id 292
    label "oddalony"
  ]
  node [
    id 293
    label "silny"
  ]
  node [
    id 294
    label "zbli&#380;enie"
  ]
  node [
    id 295
    label "zwi&#261;zany"
  ]
  node [
    id 296
    label "nieodleg&#322;y"
  ]
  node [
    id 297
    label "ma&#322;y"
  ]
  node [
    id 298
    label "czyn"
  ]
  node [
    id 299
    label "company"
  ]
  node [
    id 300
    label "zak&#322;adka"
  ]
  node [
    id 301
    label "firma"
  ]
  node [
    id 302
    label "instytut"
  ]
  node [
    id 303
    label "wyko&#324;czenie"
  ]
  node [
    id 304
    label "jednostka_organizacyjna"
  ]
  node [
    id 305
    label "umowa"
  ]
  node [
    id 306
    label "miejsce_pracy"
  ]
  node [
    id 307
    label "opieku&#324;cze_skrzyd&#322;a"
  ]
  node [
    id 308
    label "pomoc"
  ]
  node [
    id 309
    label "nadz&#243;r"
  ]
  node [
    id 310
    label "staranie"
  ]
  node [
    id 311
    label "nale&#380;yty"
  ]
  node [
    id 312
    label "stosownie"
  ]
  node [
    id 313
    label "zdarzony"
  ]
  node [
    id 314
    label "odpowiednio"
  ]
  node [
    id 315
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 316
    label "odpowiadanie"
  ]
  node [
    id 317
    label "nale&#380;ny"
  ]
  node [
    id 318
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 319
    label "polskie"
  ]
  node [
    id 320
    label "norma"
  ]
  node [
    id 321
    label "prawo"
  ]
  node [
    id 322
    label "ojciec"
  ]
  node [
    id 323
    label "rucho"
  ]
  node [
    id 324
    label "drogowy"
  ]
  node [
    id 325
    label "dziennik"
  ]
  node [
    id 326
    label "u"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 20
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 10
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 10
    target 105
  ]
  edge [
    source 10
    target 106
  ]
  edge [
    source 10
    target 107
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 10
    target 110
  ]
  edge [
    source 10
    target 111
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 25
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 10
    target 121
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 10
    target 125
  ]
  edge [
    source 10
    target 126
  ]
  edge [
    source 10
    target 127
  ]
  edge [
    source 10
    target 128
  ]
  edge [
    source 10
    target 129
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 12
    target 131
  ]
  edge [
    source 12
    target 132
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 32
  ]
  edge [
    source 13
    target 133
  ]
  edge [
    source 13
    target 134
  ]
  edge [
    source 13
    target 135
  ]
  edge [
    source 13
    target 136
  ]
  edge [
    source 13
    target 137
  ]
  edge [
    source 13
    target 138
  ]
  edge [
    source 13
    target 139
  ]
  edge [
    source 13
    target 140
  ]
  edge [
    source 13
    target 141
  ]
  edge [
    source 13
    target 17
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 14
    target 142
  ]
  edge [
    source 14
    target 143
  ]
  edge [
    source 14
    target 144
  ]
  edge [
    source 14
    target 145
  ]
  edge [
    source 14
    target 146
  ]
  edge [
    source 14
    target 147
  ]
  edge [
    source 14
    target 148
  ]
  edge [
    source 14
    target 149
  ]
  edge [
    source 14
    target 20
  ]
  edge [
    source 14
    target 32
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 31
  ]
  edge [
    source 15
    target 150
  ]
  edge [
    source 15
    target 151
  ]
  edge [
    source 15
    target 152
  ]
  edge [
    source 15
    target 153
  ]
  edge [
    source 15
    target 154
  ]
  edge [
    source 15
    target 155
  ]
  edge [
    source 15
    target 156
  ]
  edge [
    source 15
    target 157
  ]
  edge [
    source 15
    target 158
  ]
  edge [
    source 15
    target 159
  ]
  edge [
    source 15
    target 160
  ]
  edge [
    source 15
    target 161
  ]
  edge [
    source 15
    target 162
  ]
  edge [
    source 15
    target 163
  ]
  edge [
    source 15
    target 164
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 28
  ]
  edge [
    source 16
    target 29
  ]
  edge [
    source 16
    target 31
  ]
  edge [
    source 16
    target 32
  ]
  edge [
    source 16
    target 165
  ]
  edge [
    source 16
    target 166
  ]
  edge [
    source 16
    target 167
  ]
  edge [
    source 16
    target 168
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 169
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 170
  ]
  edge [
    source 19
    target 171
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 172
  ]
  edge [
    source 20
    target 173
  ]
  edge [
    source 20
    target 174
  ]
  edge [
    source 20
    target 175
  ]
  edge [
    source 20
    target 176
  ]
  edge [
    source 20
    target 177
  ]
  edge [
    source 20
    target 178
  ]
  edge [
    source 20
    target 179
  ]
  edge [
    source 20
    target 180
  ]
  edge [
    source 20
    target 181
  ]
  edge [
    source 20
    target 24
  ]
  edge [
    source 20
    target 32
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 182
  ]
  edge [
    source 21
    target 183
  ]
  edge [
    source 21
    target 184
  ]
  edge [
    source 21
    target 185
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 186
  ]
  edge [
    source 22
    target 187
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 188
  ]
  edge [
    source 23
    target 189
  ]
  edge [
    source 23
    target 190
  ]
  edge [
    source 23
    target 191
  ]
  edge [
    source 23
    target 192
  ]
  edge [
    source 23
    target 193
  ]
  edge [
    source 23
    target 194
  ]
  edge [
    source 23
    target 195
  ]
  edge [
    source 23
    target 196
  ]
  edge [
    source 23
    target 197
  ]
  edge [
    source 23
    target 198
  ]
  edge [
    source 23
    target 199
  ]
  edge [
    source 23
    target 162
  ]
  edge [
    source 23
    target 200
  ]
  edge [
    source 23
    target 201
  ]
  edge [
    source 23
    target 202
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 203
  ]
  edge [
    source 25
    target 204
  ]
  edge [
    source 25
    target 205
  ]
  edge [
    source 25
    target 206
  ]
  edge [
    source 25
    target 207
  ]
  edge [
    source 25
    target 208
  ]
  edge [
    source 25
    target 96
  ]
  edge [
    source 25
    target 209
  ]
  edge [
    source 25
    target 210
  ]
  edge [
    source 25
    target 211
  ]
  edge [
    source 25
    target 212
  ]
  edge [
    source 25
    target 213
  ]
  edge [
    source 25
    target 98
  ]
  edge [
    source 25
    target 214
  ]
  edge [
    source 25
    target 215
  ]
  edge [
    source 25
    target 102
  ]
  edge [
    source 25
    target 108
  ]
  edge [
    source 25
    target 216
  ]
  edge [
    source 25
    target 217
  ]
  edge [
    source 25
    target 110
  ]
  edge [
    source 25
    target 218
  ]
  edge [
    source 25
    target 219
  ]
  edge [
    source 25
    target 113
  ]
  edge [
    source 25
    target 117
  ]
  edge [
    source 25
    target 116
  ]
  edge [
    source 25
    target 220
  ]
  edge [
    source 25
    target 221
  ]
  edge [
    source 25
    target 222
  ]
  edge [
    source 25
    target 223
  ]
  edge [
    source 25
    target 224
  ]
  edge [
    source 25
    target 225
  ]
  edge [
    source 25
    target 120
  ]
  edge [
    source 25
    target 226
  ]
  edge [
    source 25
    target 227
  ]
  edge [
    source 25
    target 228
  ]
  edge [
    source 25
    target 129
  ]
  edge [
    source 25
    target 229
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 230
  ]
  edge [
    source 26
    target 231
  ]
  edge [
    source 26
    target 232
  ]
  edge [
    source 26
    target 233
  ]
  edge [
    source 26
    target 234
  ]
  edge [
    source 26
    target 235
  ]
  edge [
    source 26
    target 236
  ]
  edge [
    source 26
    target 237
  ]
  edge [
    source 26
    target 238
  ]
  edge [
    source 26
    target 239
  ]
  edge [
    source 26
    target 240
  ]
  edge [
    source 26
    target 241
  ]
  edge [
    source 26
    target 242
  ]
  edge [
    source 26
    target 70
  ]
  edge [
    source 26
    target 243
  ]
  edge [
    source 26
    target 244
  ]
  edge [
    source 26
    target 245
  ]
  edge [
    source 26
    target 246
  ]
  edge [
    source 26
    target 247
  ]
  edge [
    source 26
    target 248
  ]
  edge [
    source 26
    target 249
  ]
  edge [
    source 26
    target 250
  ]
  edge [
    source 26
    target 251
  ]
  edge [
    source 26
    target 252
  ]
  edge [
    source 26
    target 253
  ]
  edge [
    source 26
    target 39
  ]
  edge [
    source 26
    target 254
  ]
  edge [
    source 26
    target 255
  ]
  edge [
    source 26
    target 256
  ]
  edge [
    source 26
    target 257
  ]
  edge [
    source 26
    target 258
  ]
  edge [
    source 26
    target 259
  ]
  edge [
    source 26
    target 260
  ]
  edge [
    source 26
    target 261
  ]
  edge [
    source 26
    target 262
  ]
  edge [
    source 26
    target 263
  ]
  edge [
    source 26
    target 264
  ]
  edge [
    source 26
    target 265
  ]
  edge [
    source 26
    target 266
  ]
  edge [
    source 26
    target 267
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 268
  ]
  edge [
    source 27
    target 269
  ]
  edge [
    source 27
    target 270
  ]
  edge [
    source 27
    target 271
  ]
  edge [
    source 27
    target 272
  ]
  edge [
    source 27
    target 273
  ]
  edge [
    source 28
    target 274
  ]
  edge [
    source 28
    target 275
  ]
  edge [
    source 28
    target 276
  ]
  edge [
    source 28
    target 277
  ]
  edge [
    source 28
    target 278
  ]
  edge [
    source 28
    target 279
  ]
  edge [
    source 28
    target 280
  ]
  edge [
    source 28
    target 281
  ]
  edge [
    source 28
    target 282
  ]
  edge [
    source 28
    target 283
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 284
  ]
  edge [
    source 29
    target 285
  ]
  edge [
    source 29
    target 286
  ]
  edge [
    source 29
    target 287
  ]
  edge [
    source 29
    target 288
  ]
  edge [
    source 29
    target 289
  ]
  edge [
    source 29
    target 290
  ]
  edge [
    source 29
    target 291
  ]
  edge [
    source 29
    target 292
  ]
  edge [
    source 29
    target 293
  ]
  edge [
    source 29
    target 294
  ]
  edge [
    source 29
    target 295
  ]
  edge [
    source 29
    target 296
  ]
  edge [
    source 29
    target 297
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 298
  ]
  edge [
    source 30
    target 299
  ]
  edge [
    source 30
    target 300
  ]
  edge [
    source 30
    target 301
  ]
  edge [
    source 30
    target 302
  ]
  edge [
    source 30
    target 303
  ]
  edge [
    source 30
    target 304
  ]
  edge [
    source 30
    target 305
  ]
  edge [
    source 30
    target 49
  ]
  edge [
    source 30
    target 306
  ]
  edge [
    source 31
    target 307
  ]
  edge [
    source 31
    target 308
  ]
  edge [
    source 31
    target 309
  ]
  edge [
    source 31
    target 310
  ]
  edge [
    source 32
    target 172
  ]
  edge [
    source 32
    target 311
  ]
  edge [
    source 32
    target 312
  ]
  edge [
    source 32
    target 313
  ]
  edge [
    source 32
    target 314
  ]
  edge [
    source 32
    target 315
  ]
  edge [
    source 32
    target 316
  ]
  edge [
    source 32
    target 317
  ]
  edge [
    source 32
    target 318
  ]
  edge [
    source 319
    target 320
  ]
  edge [
    source 321
    target 322
  ]
  edge [
    source 321
    target 323
  ]
  edge [
    source 321
    target 324
  ]
  edge [
    source 322
    target 323
  ]
  edge [
    source 322
    target 324
  ]
  edge [
    source 323
    target 324
  ]
  edge [
    source 325
    target 326
  ]
]
