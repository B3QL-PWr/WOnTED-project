graph [
  maxDegree 24
  minDegree 1
  meanDegree 2
  density 0.02
  graphCliqueNumber 2
  node [
    id 0
    label "plus"
    origin "text"
  ]
  node [
    id 1
    label "je&#380;eli"
    origin "text"
  ]
  node [
    id 2
    label "uwa&#380;a&#263;"
    origin "text"
  ]
  node [
    id 3
    label "taki"
    origin "text"
  ]
  node [
    id 4
    label "&#347;wiat&#322;a"
    origin "text"
  ]
  node [
    id 5
    label "powinny"
    origin "text"
  ]
  node [
    id 6
    label "zabiera&#263;"
    origin "text"
  ]
  node [
    id 7
    label "miejsce"
    origin "text"
  ]
  node [
    id 8
    label "dow&#243;d"
    origin "text"
  ]
  node [
    id 9
    label "rejestracyjny"
    origin "text"
  ]
  node [
    id 10
    label "samoch&#243;d"
    origin "text"
  ]
  node [
    id 11
    label "motoryzacja"
    origin "text"
  ]
  node [
    id 12
    label "warto&#347;&#263;"
  ]
  node [
    id 13
    label "rewaluowa&#263;"
  ]
  node [
    id 14
    label "wabik"
  ]
  node [
    id 15
    label "korzy&#347;&#263;"
  ]
  node [
    id 16
    label "dodawanie"
  ]
  node [
    id 17
    label "rewaluowanie"
  ]
  node [
    id 18
    label "stopie&#324;"
  ]
  node [
    id 19
    label "ocena"
  ]
  node [
    id 20
    label "zrewaluowa&#263;"
  ]
  node [
    id 21
    label "liczba"
  ]
  node [
    id 22
    label "znak_matematyczny"
  ]
  node [
    id 23
    label "strona"
  ]
  node [
    id 24
    label "zrewaluowanie"
  ]
  node [
    id 25
    label "continue"
  ]
  node [
    id 26
    label "str&#243;&#380;y&#263;"
  ]
  node [
    id 27
    label "consider"
  ]
  node [
    id 28
    label "my&#347;le&#263;"
  ]
  node [
    id 29
    label "pilnowa&#263;"
  ]
  node [
    id 30
    label "robi&#263;"
  ]
  node [
    id 31
    label "uznawa&#263;"
  ]
  node [
    id 32
    label "obserwowa&#263;"
  ]
  node [
    id 33
    label "skupia&#263;_si&#281;"
  ]
  node [
    id 34
    label "mie&#263;_na_baczenie"
  ]
  node [
    id 35
    label "deliver"
  ]
  node [
    id 36
    label "okre&#347;lony"
  ]
  node [
    id 37
    label "jaki&#347;"
  ]
  node [
    id 38
    label "sygnalizacja_&#347;wietlna"
  ]
  node [
    id 39
    label "skrzy&#380;owanie"
  ]
  node [
    id 40
    label "pasy"
  ]
  node [
    id 41
    label "nale&#380;ny"
  ]
  node [
    id 42
    label "zajmowa&#263;"
  ]
  node [
    id 43
    label "blurt_out"
  ]
  node [
    id 44
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 45
    label "liszy&#263;"
  ]
  node [
    id 46
    label "przenosi&#263;"
  ]
  node [
    id 47
    label "deprive"
  ]
  node [
    id 48
    label "poci&#261;ga&#263;"
  ]
  node [
    id 49
    label "abstract"
  ]
  node [
    id 50
    label "przesuwa&#263;"
  ]
  node [
    id 51
    label "&#322;apa&#263;"
  ]
  node [
    id 52
    label "fall"
  ]
  node [
    id 53
    label "konfiskowa&#263;"
  ]
  node [
    id 54
    label "prowadzi&#263;"
  ]
  node [
    id 55
    label "cia&#322;o"
  ]
  node [
    id 56
    label "plac"
  ]
  node [
    id 57
    label "cecha"
  ]
  node [
    id 58
    label "uwaga"
  ]
  node [
    id 59
    label "przestrze&#324;"
  ]
  node [
    id 60
    label "status"
  ]
  node [
    id 61
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 62
    label "chwila"
  ]
  node [
    id 63
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 64
    label "rz&#261;d"
  ]
  node [
    id 65
    label "praca"
  ]
  node [
    id 66
    label "location"
  ]
  node [
    id 67
    label "warunek_lokalowy"
  ]
  node [
    id 68
    label "dokument"
  ]
  node [
    id 69
    label "forsing"
  ]
  node [
    id 70
    label "certificate"
  ]
  node [
    id 71
    label "rewizja"
  ]
  node [
    id 72
    label "argument"
  ]
  node [
    id 73
    label "act"
  ]
  node [
    id 74
    label "rzecz"
  ]
  node [
    id 75
    label "&#347;rodek"
  ]
  node [
    id 76
    label "uzasadnienie"
  ]
  node [
    id 77
    label "baga&#380;nik"
  ]
  node [
    id 78
    label "immobilizer"
  ]
  node [
    id 79
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 80
    label "ko&#322;o_samochodowe"
  ]
  node [
    id 81
    label "poduszka_powietrzna"
  ]
  node [
    id 82
    label "dachowanie"
  ]
  node [
    id 83
    label "dwu&#347;lad"
  ]
  node [
    id 84
    label "deska_rozdzielcza"
  ]
  node [
    id 85
    label "poci&#261;g_drogowy"
  ]
  node [
    id 86
    label "kierownica"
  ]
  node [
    id 87
    label "pojazd_drogowy"
  ]
  node [
    id 88
    label "uk&#322;ad_kierowniczy"
  ]
  node [
    id 89
    label "pompa_wodna"
  ]
  node [
    id 90
    label "silnik"
  ]
  node [
    id 91
    label "wycieraczka"
  ]
  node [
    id 92
    label "bak"
  ]
  node [
    id 93
    label "ABS"
  ]
  node [
    id 94
    label "most"
  ]
  node [
    id 95
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 96
    label "spryskiwacz"
  ]
  node [
    id 97
    label "t&#322;umik"
  ]
  node [
    id 98
    label "tempomat"
  ]
  node [
    id 99
    label "transport"
  ]
  node [
    id 100
    label "modernizacja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 11
    target 100
  ]
]
