graph [
  maxDegree 23
  minDegree 1
  meanDegree 1.9473684210526316
  density 0.05263157894736842
  graphCliqueNumber 2
  node [
    id 0
    label "humor"
    origin "text"
  ]
  node [
    id 1
    label "humorobrazkowy"
    origin "text"
  ]
  node [
    id 2
    label "motoryzacja"
    origin "text"
  ]
  node [
    id 3
    label "samoch&#243;d"
    origin "text"
  ]
  node [
    id 4
    label "stan"
  ]
  node [
    id 5
    label "temper"
  ]
  node [
    id 6
    label "&#347;mieszno&#347;&#263;"
  ]
  node [
    id 7
    label "mechanizm_obronny"
  ]
  node [
    id 8
    label "nastr&#243;j"
  ]
  node [
    id 9
    label "state"
  ]
  node [
    id 10
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 11
    label "samopoczucie"
  ]
  node [
    id 12
    label "fondness"
  ]
  node [
    id 13
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 14
    label "transport"
  ]
  node [
    id 15
    label "modernizacja"
  ]
  node [
    id 16
    label "baga&#380;nik"
  ]
  node [
    id 17
    label "immobilizer"
  ]
  node [
    id 18
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 19
    label "ko&#322;o_samochodowe"
  ]
  node [
    id 20
    label "poduszka_powietrzna"
  ]
  node [
    id 21
    label "dachowanie"
  ]
  node [
    id 22
    label "dwu&#347;lad"
  ]
  node [
    id 23
    label "deska_rozdzielcza"
  ]
  node [
    id 24
    label "poci&#261;g_drogowy"
  ]
  node [
    id 25
    label "kierownica"
  ]
  node [
    id 26
    label "pojazd_drogowy"
  ]
  node [
    id 27
    label "uk&#322;ad_kierowniczy"
  ]
  node [
    id 28
    label "pompa_wodna"
  ]
  node [
    id 29
    label "silnik"
  ]
  node [
    id 30
    label "wycieraczka"
  ]
  node [
    id 31
    label "bak"
  ]
  node [
    id 32
    label "ABS"
  ]
  node [
    id 33
    label "most"
  ]
  node [
    id 34
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 35
    label "spryskiwacz"
  ]
  node [
    id 36
    label "t&#322;umik"
  ]
  node [
    id 37
    label "tempomat"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
]
