graph [
  maxDegree 40
  minDegree 1
  meanDegree 2.0259740259740258
  density 0.006599263928254156
  graphCliqueNumber 3
  node [
    id 0
    label "dwa"
    origin "text"
  ]
  node [
    id 1
    label "tydzie&#324;"
    origin "text"
  ]
  node [
    id 2
    label "temu"
    origin "text"
  ]
  node [
    id 3
    label "syn"
    origin "text"
  ]
  node [
    id 4
    label "nauczy&#263;"
    origin "text"
  ]
  node [
    id 5
    label "gra&#263;"
    origin "text"
  ]
  node [
    id 6
    label "magic"
    origin "text"
  ]
  node [
    id 7
    label "the"
    origin "text"
  ]
  node [
    id 8
    label "gathering"
    origin "text"
  ]
  node [
    id 9
    label "strategiczny"
    origin "text"
  ]
  node [
    id 10
    label "gra"
    origin "text"
  ]
  node [
    id 11
    label "karciany"
    origin "text"
  ]
  node [
    id 12
    label "by&#263;"
    origin "text"
  ]
  node [
    id 13
    label "niezmiernie"
    origin "text"
  ]
  node [
    id 14
    label "wci&#261;ga&#263;"
    origin "text"
  ]
  node [
    id 15
    label "zwi&#261;zek"
    origin "text"
  ]
  node [
    id 16
    label "czym"
    origin "text"
  ]
  node [
    id 17
    label "szybki"
    origin "text"
  ]
  node [
    id 18
    label "czas"
    origin "text"
  ]
  node [
    id 19
    label "zapami&#281;tywa&#263;"
    origin "text"
  ]
  node [
    id 20
    label "si&#281;"
    origin "text"
  ]
  node [
    id 21
    label "mn&#243;stwo"
    origin "text"
  ]
  node [
    id 22
    label "informacja"
    origin "text"
  ]
  node [
    id 23
    label "typ"
    origin "text"
  ]
  node [
    id 24
    label "zimny"
    origin "text"
  ]
  node [
    id 25
    label "oddech"
    origin "text"
  ]
  node [
    id 26
    label "blokowa&#263;"
    origin "text"
  ]
  node [
    id 27
    label "moc"
    origin "text"
  ]
  node [
    id 28
    label "ogie&#324;"
    origin "text"
  ]
  node [
    id 29
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 30
    label "doba"
  ]
  node [
    id 31
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 32
    label "weekend"
  ]
  node [
    id 33
    label "miesi&#261;c"
  ]
  node [
    id 34
    label "usynowienie"
  ]
  node [
    id 35
    label "usynawianie"
  ]
  node [
    id 36
    label "dziecko"
  ]
  node [
    id 37
    label "instruct"
  ]
  node [
    id 38
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 39
    label "wyszkoli&#263;"
  ]
  node [
    id 40
    label "&#347;wieci&#263;"
  ]
  node [
    id 41
    label "typify"
  ]
  node [
    id 42
    label "majaczy&#263;"
  ]
  node [
    id 43
    label "dzia&#322;a&#263;"
  ]
  node [
    id 44
    label "rola"
  ]
  node [
    id 45
    label "wykonywa&#263;"
  ]
  node [
    id 46
    label "tokowa&#263;"
  ]
  node [
    id 47
    label "prezentowa&#263;"
  ]
  node [
    id 48
    label "uzewn&#281;trznia&#263;_si&#281;"
  ]
  node [
    id 49
    label "rozgrywa&#263;"
  ]
  node [
    id 50
    label "przedstawia&#263;"
  ]
  node [
    id 51
    label "wykorzystywa&#263;"
  ]
  node [
    id 52
    label "wida&#263;"
  ]
  node [
    id 53
    label "brzmie&#263;"
  ]
  node [
    id 54
    label "dally"
  ]
  node [
    id 55
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 56
    label "robi&#263;"
  ]
  node [
    id 57
    label "do"
  ]
  node [
    id 58
    label "instrument_muzyczny"
  ]
  node [
    id 59
    label "play"
  ]
  node [
    id 60
    label "otwarcie"
  ]
  node [
    id 61
    label "szczeka&#263;"
  ]
  node [
    id 62
    label "cope"
  ]
  node [
    id 63
    label "pasowa&#263;"
  ]
  node [
    id 64
    label "napierdziela&#263;"
  ]
  node [
    id 65
    label "sound"
  ]
  node [
    id 66
    label "muzykowa&#263;"
  ]
  node [
    id 67
    label "stara&#263;_si&#281;"
  ]
  node [
    id 68
    label "i&#347;&#263;"
  ]
  node [
    id 69
    label "strategicznie"
  ]
  node [
    id 70
    label "ostro&#380;ny"
  ]
  node [
    id 71
    label "taktycznie"
  ]
  node [
    id 72
    label "wojskowy"
  ]
  node [
    id 73
    label "perspektywiczny"
  ]
  node [
    id 74
    label "zabawa"
  ]
  node [
    id 75
    label "rywalizacja"
  ]
  node [
    id 76
    label "czynno&#347;&#263;"
  ]
  node [
    id 77
    label "Pok&#233;mon"
  ]
  node [
    id 78
    label "synteza"
  ]
  node [
    id 79
    label "odtworzenie"
  ]
  node [
    id 80
    label "komplet"
  ]
  node [
    id 81
    label "rekwizyt_do_gry"
  ]
  node [
    id 82
    label "odg&#322;os"
  ]
  node [
    id 83
    label "rozgrywka"
  ]
  node [
    id 84
    label "post&#281;powanie"
  ]
  node [
    id 85
    label "wydarzenie"
  ]
  node [
    id 86
    label "apparent_motion"
  ]
  node [
    id 87
    label "game"
  ]
  node [
    id 88
    label "zmienno&#347;&#263;"
  ]
  node [
    id 89
    label "zasada"
  ]
  node [
    id 90
    label "akcja"
  ]
  node [
    id 91
    label "contest"
  ]
  node [
    id 92
    label "zbijany"
  ]
  node [
    id 93
    label "prawny"
  ]
  node [
    id 94
    label "si&#281;ga&#263;"
  ]
  node [
    id 95
    label "trwa&#263;"
  ]
  node [
    id 96
    label "obecno&#347;&#263;"
  ]
  node [
    id 97
    label "stan"
  ]
  node [
    id 98
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 99
    label "stand"
  ]
  node [
    id 100
    label "mie&#263;_miejsce"
  ]
  node [
    id 101
    label "uczestniczy&#263;"
  ]
  node [
    id 102
    label "chodzi&#263;"
  ]
  node [
    id 103
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 104
    label "equal"
  ]
  node [
    id 105
    label "szczytnie"
  ]
  node [
    id 106
    label "niezmierny"
  ]
  node [
    id 107
    label "ogromnie"
  ]
  node [
    id 108
    label "wprowadza&#263;"
  ]
  node [
    id 109
    label "pull"
  ]
  node [
    id 110
    label "draw"
  ]
  node [
    id 111
    label "powodowa&#263;"
  ]
  node [
    id 112
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 113
    label "odwodnienie"
  ]
  node [
    id 114
    label "konstytucja"
  ]
  node [
    id 115
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 116
    label "substancja_chemiczna"
  ]
  node [
    id 117
    label "bratnia_dusza"
  ]
  node [
    id 118
    label "zwi&#261;zanie"
  ]
  node [
    id 119
    label "lokant"
  ]
  node [
    id 120
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 121
    label "zwi&#261;za&#263;"
  ]
  node [
    id 122
    label "organizacja"
  ]
  node [
    id 123
    label "odwadnia&#263;"
  ]
  node [
    id 124
    label "marriage"
  ]
  node [
    id 125
    label "marketing_afiliacyjny"
  ]
  node [
    id 126
    label "bearing"
  ]
  node [
    id 127
    label "wi&#261;zanie"
  ]
  node [
    id 128
    label "odwadnianie"
  ]
  node [
    id 129
    label "koligacja"
  ]
  node [
    id 130
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 131
    label "odwodni&#263;"
  ]
  node [
    id 132
    label "azeotrop"
  ]
  node [
    id 133
    label "powi&#261;zanie"
  ]
  node [
    id 134
    label "bezpo&#347;redni"
  ]
  node [
    id 135
    label "kr&#243;tki"
  ]
  node [
    id 136
    label "temperamentny"
  ]
  node [
    id 137
    label "prosty"
  ]
  node [
    id 138
    label "intensywny"
  ]
  node [
    id 139
    label "dynamiczny"
  ]
  node [
    id 140
    label "szybko"
  ]
  node [
    id 141
    label "sprawny"
  ]
  node [
    id 142
    label "bystrolotny"
  ]
  node [
    id 143
    label "energiczny"
  ]
  node [
    id 144
    label "czasokres"
  ]
  node [
    id 145
    label "trawienie"
  ]
  node [
    id 146
    label "kategoria_gramatyczna"
  ]
  node [
    id 147
    label "period"
  ]
  node [
    id 148
    label "odczyt"
  ]
  node [
    id 149
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 150
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 151
    label "chwila"
  ]
  node [
    id 152
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 153
    label "poprzedzenie"
  ]
  node [
    id 154
    label "koniugacja"
  ]
  node [
    id 155
    label "dzieje"
  ]
  node [
    id 156
    label "poprzedzi&#263;"
  ]
  node [
    id 157
    label "przep&#322;ywanie"
  ]
  node [
    id 158
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 159
    label "odwlekanie_si&#281;"
  ]
  node [
    id 160
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 161
    label "Zeitgeist"
  ]
  node [
    id 162
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 163
    label "okres_czasu"
  ]
  node [
    id 164
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 165
    label "pochodzi&#263;"
  ]
  node [
    id 166
    label "schy&#322;ek"
  ]
  node [
    id 167
    label "czwarty_wymiar"
  ]
  node [
    id 168
    label "chronometria"
  ]
  node [
    id 169
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 170
    label "poprzedzanie"
  ]
  node [
    id 171
    label "pogoda"
  ]
  node [
    id 172
    label "zegar"
  ]
  node [
    id 173
    label "pochodzenie"
  ]
  node [
    id 174
    label "poprzedza&#263;"
  ]
  node [
    id 175
    label "trawi&#263;"
  ]
  node [
    id 176
    label "time_period"
  ]
  node [
    id 177
    label "rachuba_czasu"
  ]
  node [
    id 178
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 179
    label "czasoprzestrze&#324;"
  ]
  node [
    id 180
    label "laba"
  ]
  node [
    id 181
    label "enormousness"
  ]
  node [
    id 182
    label "ilo&#347;&#263;"
  ]
  node [
    id 183
    label "doj&#347;cie"
  ]
  node [
    id 184
    label "doj&#347;&#263;"
  ]
  node [
    id 185
    label "powzi&#261;&#263;"
  ]
  node [
    id 186
    label "wiedza"
  ]
  node [
    id 187
    label "sygna&#322;"
  ]
  node [
    id 188
    label "obiegni&#281;cie"
  ]
  node [
    id 189
    label "obieganie"
  ]
  node [
    id 190
    label "obiec"
  ]
  node [
    id 191
    label "dane"
  ]
  node [
    id 192
    label "obiega&#263;"
  ]
  node [
    id 193
    label "punkt"
  ]
  node [
    id 194
    label "publikacja"
  ]
  node [
    id 195
    label "powzi&#281;cie"
  ]
  node [
    id 196
    label "cz&#322;owiek"
  ]
  node [
    id 197
    label "gromada"
  ]
  node [
    id 198
    label "autorament"
  ]
  node [
    id 199
    label "przypuszczenie"
  ]
  node [
    id 200
    label "cynk"
  ]
  node [
    id 201
    label "rezultat"
  ]
  node [
    id 202
    label "jednostka_systematyczna"
  ]
  node [
    id 203
    label "kr&#243;lestwo"
  ]
  node [
    id 204
    label "obstawia&#263;"
  ]
  node [
    id 205
    label "design"
  ]
  node [
    id 206
    label "facet"
  ]
  node [
    id 207
    label "variety"
  ]
  node [
    id 208
    label "sztuka"
  ]
  node [
    id 209
    label "antycypacja"
  ]
  node [
    id 210
    label "zimno"
  ]
  node [
    id 211
    label "obumarcie"
  ]
  node [
    id 212
    label "niemi&#322;y"
  ]
  node [
    id 213
    label "znieczulanie"
  ]
  node [
    id 214
    label "znieczulenie"
  ]
  node [
    id 215
    label "ozi&#281;bienie"
  ]
  node [
    id 216
    label "obumieranie"
  ]
  node [
    id 217
    label "wiszenie"
  ]
  node [
    id 218
    label "umarcie"
  ]
  node [
    id 219
    label "martwo"
  ]
  node [
    id 220
    label "rozs&#261;dny"
  ]
  node [
    id 221
    label "umieranie"
  ]
  node [
    id 222
    label "z&#322;y"
  ]
  node [
    id 223
    label "opanowany"
  ]
  node [
    id 224
    label "niezadowalaj&#261;cy"
  ]
  node [
    id 225
    label "niech&#281;tny"
  ]
  node [
    id 226
    label "ch&#322;odno"
  ]
  node [
    id 227
    label "nieczu&#322;y"
  ]
  node [
    id 228
    label "duch"
  ]
  node [
    id 229
    label "ud&#322;awienie_si&#281;"
  ]
  node [
    id 230
    label "wdech"
  ]
  node [
    id 231
    label "zaprze&#263;_oddech"
  ]
  node [
    id 232
    label "rebirthing"
  ]
  node [
    id 233
    label "hipowentylacja"
  ]
  node [
    id 234
    label "zatykanie"
  ]
  node [
    id 235
    label "wydech"
  ]
  node [
    id 236
    label "zatyka&#263;"
  ]
  node [
    id 237
    label "zaparcie_oddechu"
  ]
  node [
    id 238
    label "ud&#322;awi&#263;_si&#281;"
  ]
  node [
    id 239
    label "zapieranie_oddechu"
  ]
  node [
    id 240
    label "zatka&#263;"
  ]
  node [
    id 241
    label "zatkanie"
  ]
  node [
    id 242
    label "&#347;wista&#263;"
  ]
  node [
    id 243
    label "oddychanie"
  ]
  node [
    id 244
    label "zapiera&#263;_oddech"
  ]
  node [
    id 245
    label "throng"
  ]
  node [
    id 246
    label "zajmowa&#263;"
  ]
  node [
    id 247
    label "przeszkadza&#263;"
  ]
  node [
    id 248
    label "zablokowywa&#263;"
  ]
  node [
    id 249
    label "sk&#322;ada&#263;"
  ]
  node [
    id 250
    label "walczy&#263;"
  ]
  node [
    id 251
    label "interlock"
  ]
  node [
    id 252
    label "wstrzymywa&#263;"
  ]
  node [
    id 253
    label "unieruchamia&#263;"
  ]
  node [
    id 254
    label "kiblowa&#263;"
  ]
  node [
    id 255
    label "zatrzymywa&#263;"
  ]
  node [
    id 256
    label "parry"
  ]
  node [
    id 257
    label "wa&#380;no&#347;&#263;"
  ]
  node [
    id 258
    label "nasycenie"
  ]
  node [
    id 259
    label "wuchta"
  ]
  node [
    id 260
    label "parametr"
  ]
  node [
    id 261
    label "immunity"
  ]
  node [
    id 262
    label "zdolno&#347;&#263;"
  ]
  node [
    id 263
    label "wytrzyma&#322;o&#347;&#263;"
  ]
  node [
    id 264
    label "poj&#281;cie"
  ]
  node [
    id 265
    label "potencja"
  ]
  node [
    id 266
    label "izotonia"
  ]
  node [
    id 267
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 268
    label "nefelometria"
  ]
  node [
    id 269
    label "zarzewie"
  ]
  node [
    id 270
    label "rozpalenie"
  ]
  node [
    id 271
    label "rozpalanie"
  ]
  node [
    id 272
    label "nowotw&#243;r_niez&#322;o&#347;liwy"
  ]
  node [
    id 273
    label "ardor"
  ]
  node [
    id 274
    label "atak"
  ]
  node [
    id 275
    label "palenie"
  ]
  node [
    id 276
    label "deszcz"
  ]
  node [
    id 277
    label "zapalenie"
  ]
  node [
    id 278
    label "light"
  ]
  node [
    id 279
    label "pali&#263;_si&#281;"
  ]
  node [
    id 280
    label "hell"
  ]
  node [
    id 281
    label "znami&#281;"
  ]
  node [
    id 282
    label "palenie_si&#281;"
  ]
  node [
    id 283
    label "sk&#243;ra"
  ]
  node [
    id 284
    label "iskra"
  ]
  node [
    id 285
    label "incandescence"
  ]
  node [
    id 286
    label "akcesorium"
  ]
  node [
    id 287
    label "&#347;wiat&#322;o"
  ]
  node [
    id 288
    label "co&#347;"
  ]
  node [
    id 289
    label "rumieniec"
  ]
  node [
    id 290
    label "fire"
  ]
  node [
    id 291
    label "przyp&#322;yw"
  ]
  node [
    id 292
    label "kolor"
  ]
  node [
    id 293
    label "&#380;ywio&#322;"
  ]
  node [
    id 294
    label "energia"
  ]
  node [
    id 295
    label "p&#322;omie&#324;"
  ]
  node [
    id 296
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 297
    label "ciep&#322;o"
  ]
  node [
    id 298
    label "war"
  ]
  node [
    id 299
    label "Magic"
  ]
  node [
    id 300
    label "Gathering"
  ]
  node [
    id 301
    label "Anshul"
  ]
  node [
    id 302
    label "samara"
  ]
  node [
    id 303
    label "World"
  ]
  node [
    id 304
    label "of"
  ]
  node [
    id 305
    label "Warcraft"
  ]
  node [
    id 306
    label "ko&#322;o"
  ]
  node [
    id 307
    label "fortuna"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 11
    target 93
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 94
  ]
  edge [
    source 12
    target 95
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 105
  ]
  edge [
    source 13
    target 106
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 108
  ]
  edge [
    source 14
    target 109
  ]
  edge [
    source 14
    target 56
  ]
  edge [
    source 14
    target 110
  ]
  edge [
    source 14
    target 111
  ]
  edge [
    source 14
    target 112
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 113
  ]
  edge [
    source 15
    target 114
  ]
  edge [
    source 15
    target 115
  ]
  edge [
    source 15
    target 116
  ]
  edge [
    source 15
    target 117
  ]
  edge [
    source 15
    target 118
  ]
  edge [
    source 15
    target 119
  ]
  edge [
    source 15
    target 120
  ]
  edge [
    source 15
    target 121
  ]
  edge [
    source 15
    target 122
  ]
  edge [
    source 15
    target 123
  ]
  edge [
    source 15
    target 124
  ]
  edge [
    source 15
    target 125
  ]
  edge [
    source 15
    target 126
  ]
  edge [
    source 15
    target 127
  ]
  edge [
    source 15
    target 128
  ]
  edge [
    source 15
    target 129
  ]
  edge [
    source 15
    target 130
  ]
  edge [
    source 15
    target 131
  ]
  edge [
    source 15
    target 132
  ]
  edge [
    source 15
    target 133
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 134
  ]
  edge [
    source 17
    target 135
  ]
  edge [
    source 17
    target 136
  ]
  edge [
    source 17
    target 137
  ]
  edge [
    source 17
    target 138
  ]
  edge [
    source 17
    target 139
  ]
  edge [
    source 17
    target 140
  ]
  edge [
    source 17
    target 141
  ]
  edge [
    source 17
    target 142
  ]
  edge [
    source 17
    target 143
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 144
  ]
  edge [
    source 18
    target 145
  ]
  edge [
    source 18
    target 146
  ]
  edge [
    source 18
    target 147
  ]
  edge [
    source 18
    target 148
  ]
  edge [
    source 18
    target 149
  ]
  edge [
    source 18
    target 150
  ]
  edge [
    source 18
    target 151
  ]
  edge [
    source 18
    target 152
  ]
  edge [
    source 18
    target 153
  ]
  edge [
    source 18
    target 154
  ]
  edge [
    source 18
    target 155
  ]
  edge [
    source 18
    target 156
  ]
  edge [
    source 18
    target 157
  ]
  edge [
    source 18
    target 158
  ]
  edge [
    source 18
    target 159
  ]
  edge [
    source 18
    target 160
  ]
  edge [
    source 18
    target 161
  ]
  edge [
    source 18
    target 162
  ]
  edge [
    source 18
    target 163
  ]
  edge [
    source 18
    target 164
  ]
  edge [
    source 18
    target 165
  ]
  edge [
    source 18
    target 166
  ]
  edge [
    source 18
    target 167
  ]
  edge [
    source 18
    target 168
  ]
  edge [
    source 18
    target 169
  ]
  edge [
    source 18
    target 170
  ]
  edge [
    source 18
    target 171
  ]
  edge [
    source 18
    target 172
  ]
  edge [
    source 18
    target 173
  ]
  edge [
    source 18
    target 174
  ]
  edge [
    source 18
    target 175
  ]
  edge [
    source 18
    target 176
  ]
  edge [
    source 18
    target 177
  ]
  edge [
    source 18
    target 178
  ]
  edge [
    source 18
    target 179
  ]
  edge [
    source 18
    target 180
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 22
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 181
  ]
  edge [
    source 21
    target 182
  ]
  edge [
    source 21
    target 27
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 183
  ]
  edge [
    source 22
    target 184
  ]
  edge [
    source 22
    target 185
  ]
  edge [
    source 22
    target 186
  ]
  edge [
    source 22
    target 187
  ]
  edge [
    source 22
    target 188
  ]
  edge [
    source 22
    target 189
  ]
  edge [
    source 22
    target 190
  ]
  edge [
    source 22
    target 191
  ]
  edge [
    source 22
    target 192
  ]
  edge [
    source 22
    target 193
  ]
  edge [
    source 22
    target 194
  ]
  edge [
    source 22
    target 195
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 196
  ]
  edge [
    source 23
    target 197
  ]
  edge [
    source 23
    target 198
  ]
  edge [
    source 23
    target 199
  ]
  edge [
    source 23
    target 200
  ]
  edge [
    source 23
    target 201
  ]
  edge [
    source 23
    target 202
  ]
  edge [
    source 23
    target 203
  ]
  edge [
    source 23
    target 204
  ]
  edge [
    source 23
    target 205
  ]
  edge [
    source 23
    target 206
  ]
  edge [
    source 23
    target 207
  ]
  edge [
    source 23
    target 208
  ]
  edge [
    source 23
    target 209
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 210
  ]
  edge [
    source 24
    target 211
  ]
  edge [
    source 24
    target 212
  ]
  edge [
    source 24
    target 213
  ]
  edge [
    source 24
    target 214
  ]
  edge [
    source 24
    target 215
  ]
  edge [
    source 24
    target 216
  ]
  edge [
    source 24
    target 217
  ]
  edge [
    source 24
    target 218
  ]
  edge [
    source 24
    target 219
  ]
  edge [
    source 24
    target 220
  ]
  edge [
    source 24
    target 221
  ]
  edge [
    source 24
    target 222
  ]
  edge [
    source 24
    target 223
  ]
  edge [
    source 24
    target 224
  ]
  edge [
    source 24
    target 225
  ]
  edge [
    source 24
    target 226
  ]
  edge [
    source 24
    target 227
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 228
  ]
  edge [
    source 25
    target 229
  ]
  edge [
    source 25
    target 230
  ]
  edge [
    source 25
    target 76
  ]
  edge [
    source 25
    target 231
  ]
  edge [
    source 25
    target 232
  ]
  edge [
    source 25
    target 233
  ]
  edge [
    source 25
    target 234
  ]
  edge [
    source 25
    target 235
  ]
  edge [
    source 25
    target 236
  ]
  edge [
    source 25
    target 237
  ]
  edge [
    source 25
    target 238
  ]
  edge [
    source 25
    target 239
  ]
  edge [
    source 25
    target 240
  ]
  edge [
    source 25
    target 241
  ]
  edge [
    source 25
    target 242
  ]
  edge [
    source 25
    target 243
  ]
  edge [
    source 25
    target 244
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 245
  ]
  edge [
    source 26
    target 246
  ]
  edge [
    source 26
    target 247
  ]
  edge [
    source 26
    target 248
  ]
  edge [
    source 26
    target 249
  ]
  edge [
    source 26
    target 250
  ]
  edge [
    source 26
    target 251
  ]
  edge [
    source 26
    target 252
  ]
  edge [
    source 26
    target 253
  ]
  edge [
    source 26
    target 254
  ]
  edge [
    source 26
    target 255
  ]
  edge [
    source 26
    target 256
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 257
  ]
  edge [
    source 27
    target 258
  ]
  edge [
    source 27
    target 259
  ]
  edge [
    source 27
    target 260
  ]
  edge [
    source 27
    target 261
  ]
  edge [
    source 27
    target 262
  ]
  edge [
    source 27
    target 263
  ]
  edge [
    source 27
    target 264
  ]
  edge [
    source 27
    target 265
  ]
  edge [
    source 27
    target 266
  ]
  edge [
    source 27
    target 267
  ]
  edge [
    source 27
    target 268
  ]
  edge [
    source 28
    target 269
  ]
  edge [
    source 28
    target 270
  ]
  edge [
    source 28
    target 271
  ]
  edge [
    source 28
    target 272
  ]
  edge [
    source 28
    target 273
  ]
  edge [
    source 28
    target 274
  ]
  edge [
    source 28
    target 275
  ]
  edge [
    source 28
    target 276
  ]
  edge [
    source 28
    target 277
  ]
  edge [
    source 28
    target 278
  ]
  edge [
    source 28
    target 279
  ]
  edge [
    source 28
    target 280
  ]
  edge [
    source 28
    target 281
  ]
  edge [
    source 28
    target 282
  ]
  edge [
    source 28
    target 283
  ]
  edge [
    source 28
    target 284
  ]
  edge [
    source 28
    target 285
  ]
  edge [
    source 28
    target 286
  ]
  edge [
    source 28
    target 287
  ]
  edge [
    source 28
    target 288
  ]
  edge [
    source 28
    target 289
  ]
  edge [
    source 28
    target 290
  ]
  edge [
    source 28
    target 291
  ]
  edge [
    source 28
    target 292
  ]
  edge [
    source 28
    target 293
  ]
  edge [
    source 28
    target 294
  ]
  edge [
    source 28
    target 295
  ]
  edge [
    source 28
    target 296
  ]
  edge [
    source 28
    target 297
  ]
  edge [
    source 28
    target 298
  ]
  edge [
    source 299
    target 300
  ]
  edge [
    source 301
    target 302
  ]
  edge [
    source 303
    target 304
  ]
  edge [
    source 303
    target 305
  ]
  edge [
    source 304
    target 305
  ]
  edge [
    source 306
    target 307
  ]
]
