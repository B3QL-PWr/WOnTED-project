graph [
  maxDegree 24
  minDegree 1
  meanDegree 1.9767441860465116
  density 0.023255813953488372
  graphCliqueNumber 2
  node [
    id 0
    label "kompletny"
    origin "text"
  ]
  node [
    id 1
    label "proces"
    origin "text"
  ]
  node [
    id 2
    label "tradycyjny"
    origin "text"
  ]
  node [
    id 3
    label "wytwarzanie"
    origin "text"
  ]
  node [
    id 4
    label "&#322;uk"
    origin "text"
  ]
  node [
    id 5
    label "kompozytowy"
    origin "text"
  ]
  node [
    id 6
    label "drewno"
    origin "text"
  ]
  node [
    id 7
    label "&#347;ci&#281;gno"
    origin "text"
  ]
  node [
    id 8
    label "r&#243;g"
    origin "text"
  ]
  node [
    id 9
    label "kompletnie"
  ]
  node [
    id 10
    label "w_pizdu"
  ]
  node [
    id 11
    label "zupe&#322;ny"
  ]
  node [
    id 12
    label "pe&#322;ny"
  ]
  node [
    id 13
    label "legislacyjnie"
  ]
  node [
    id 14
    label "kognicja"
  ]
  node [
    id 15
    label "przebieg"
  ]
  node [
    id 16
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 17
    label "wydarzenie"
  ]
  node [
    id 18
    label "przes&#322;anka"
  ]
  node [
    id 19
    label "rozprawa"
  ]
  node [
    id 20
    label "zjawisko"
  ]
  node [
    id 21
    label "nast&#281;pstwo"
  ]
  node [
    id 22
    label "nienowoczesny"
  ]
  node [
    id 23
    label "zachowawczy"
  ]
  node [
    id 24
    label "zwyk&#322;y"
  ]
  node [
    id 25
    label "zwyczajowy"
  ]
  node [
    id 26
    label "modelowy"
  ]
  node [
    id 27
    label "przyj&#281;ty"
  ]
  node [
    id 28
    label "wierny"
  ]
  node [
    id 29
    label "tradycyjnie"
  ]
  node [
    id 30
    label "surowy"
  ]
  node [
    id 31
    label "robienie"
  ]
  node [
    id 32
    label "fabrication"
  ]
  node [
    id 33
    label "tentegowanie"
  ]
  node [
    id 34
    label "bycie"
  ]
  node [
    id 35
    label "przedmiot"
  ]
  node [
    id 36
    label "lying"
  ]
  node [
    id 37
    label "zasiedzenie_si&#281;"
  ]
  node [
    id 38
    label "bro&#324;_sportowa"
  ]
  node [
    id 39
    label "bro&#324;"
  ]
  node [
    id 40
    label "element"
  ]
  node [
    id 41
    label "bro&#324;_miotaj&#261;ca"
  ]
  node [
    id 42
    label "znak_muzyczny"
  ]
  node [
    id 43
    label "strza&#322;ka"
  ]
  node [
    id 44
    label "&#322;ubia"
  ]
  node [
    id 45
    label "ci&#281;ciwa"
  ]
  node [
    id 46
    label "bow_and_arrow"
  ]
  node [
    id 47
    label "okr&#261;g"
  ]
  node [
    id 48
    label "para"
  ]
  node [
    id 49
    label "end"
  ]
  node [
    id 50
    label "poj&#281;cie"
  ]
  node [
    id 51
    label "ligature"
  ]
  node [
    id 52
    label "kszta&#322;t"
  ]
  node [
    id 53
    label "arkada"
  ]
  node [
    id 54
    label "pod&#322;ucze"
  ]
  node [
    id 55
    label "&#322;&#281;czysko"
  ]
  node [
    id 56
    label "affiliation"
  ]
  node [
    id 57
    label "graf"
  ]
  node [
    id 58
    label "ewolucja_narciarska"
  ]
  node [
    id 59
    label "&#322;&#281;k"
  ]
  node [
    id 60
    label "z&#322;o&#380;ony"
  ]
  node [
    id 61
    label "trachej"
  ]
  node [
    id 62
    label "surowiec"
  ]
  node [
    id 63
    label "ksylofag"
  ]
  node [
    id 64
    label "drewniany"
  ]
  node [
    id 65
    label "aktorzyna"
  ]
  node [
    id 66
    label "parzelnia"
  ]
  node [
    id 67
    label "&#380;ywica"
  ]
  node [
    id 68
    label "zacios"
  ]
  node [
    id 69
    label "mi&#281;kisz_drzewny"
  ]
  node [
    id 70
    label "tkanka_sta&#322;a"
  ]
  node [
    id 71
    label "mi&#281;sie&#324;"
  ]
  node [
    id 72
    label "poro&#380;e"
  ]
  node [
    id 73
    label "tworzywo"
  ]
  node [
    id 74
    label "miejsce"
  ]
  node [
    id 75
    label "zawarto&#347;&#263;"
  ]
  node [
    id 76
    label "podanie"
  ]
  node [
    id 77
    label "kraw&#281;d&#378;"
  ]
  node [
    id 78
    label "linia"
  ]
  node [
    id 79
    label "wyrostek"
  ]
  node [
    id 80
    label "naczynie"
  ]
  node [
    id 81
    label "zbieg"
  ]
  node [
    id 82
    label "instrument_d&#281;ty"
  ]
  node [
    id 83
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 84
    label "aut_bramkowy"
  ]
  node [
    id 85
    label "instrument_d&#281;ty_blaszany"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
]
