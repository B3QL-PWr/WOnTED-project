graph [
  maxDegree 21
  minDegree 1
  meanDegree 2.1379310344827585
  density 0.03750756200846945
  graphCliqueNumber 4
  node [
    id 0
    label "polsko"
    origin "text"
  ]
  node [
    id 1
    label "niemiecki"
    origin "text"
  ]
  node [
    id 2
    label "nagroda"
    origin "text"
  ]
  node [
    id 3
    label "dziennikarski"
    origin "text"
  ]
  node [
    id 4
    label "po_polsku"
  ]
  node [
    id 5
    label "polski"
  ]
  node [
    id 6
    label "europejsko"
  ]
  node [
    id 7
    label "szwabski"
  ]
  node [
    id 8
    label "po_niemiecku"
  ]
  node [
    id 9
    label "niemiec"
  ]
  node [
    id 10
    label "cenar"
  ]
  node [
    id 11
    label "j&#281;zyk"
  ]
  node [
    id 12
    label "europejski"
  ]
  node [
    id 13
    label "German"
  ]
  node [
    id 14
    label "pionier"
  ]
  node [
    id 15
    label "niemiecko"
  ]
  node [
    id 16
    label "j&#281;zyk_zachodniogerma&#324;ski"
  ]
  node [
    id 17
    label "zachodnioeuropejski"
  ]
  node [
    id 18
    label "strudel"
  ]
  node [
    id 19
    label "junkers"
  ]
  node [
    id 20
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 21
    label "return"
  ]
  node [
    id 22
    label "konsekwencja"
  ]
  node [
    id 23
    label "oskar"
  ]
  node [
    id 24
    label "wyr&#243;&#380;nienie"
  ]
  node [
    id 25
    label "zawodowy"
  ]
  node [
    id 26
    label "rzetelny"
  ]
  node [
    id 27
    label "tre&#347;ciwy"
  ]
  node [
    id 28
    label "po_dziennikarsku"
  ]
  node [
    id 29
    label "dziennikarsko"
  ]
  node [
    id 30
    label "obiektywny"
  ]
  node [
    id 31
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 32
    label "wzorowy"
  ]
  node [
    id 33
    label "typowy"
  ]
  node [
    id 34
    label "unia"
  ]
  node [
    id 35
    label "Meklemburgia"
  ]
  node [
    id 36
    label "pomorze"
  ]
  node [
    id 37
    label "przedni"
  ]
  node [
    id 38
    label "fundacja"
  ]
  node [
    id 39
    label "Roberto"
  ]
  node [
    id 40
    label "Boscha"
  ]
  node [
    id 41
    label "wsp&#243;&#322;praca"
  ]
  node [
    id 42
    label "dzie&#324;"
  ]
  node [
    id 43
    label "medium"
  ]
  node [
    id 44
    label "W&#322;odzimierz"
  ]
  node [
    id 45
    label "Kalicki"
  ]
  node [
    id 46
    label "Magdalena"
  ]
  node [
    id 47
    label "Grzeba&#322;kowska"
  ]
  node [
    id 48
    label "Adam"
  ]
  node [
    id 49
    label "Soboczy&#324;ski"
  ]
  node [
    id 50
    label "Helga"
  ]
  node [
    id 51
    label "Hirsch"
  ]
  node [
    id 52
    label "Maria"
  ]
  node [
    id 53
    label "Zmarz"
  ]
  node [
    id 54
    label "Koczanowicz"
  ]
  node [
    id 55
    label "nowak"
  ]
  node [
    id 56
    label "Tomasz"
  ]
  node [
    id 57
    label "sikora"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 12
    target 34
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 37
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 40
  ]
  edge [
    source 38
    target 41
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 55
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 54
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 56
    target 57
  ]
]
