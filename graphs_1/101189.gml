graph [
  maxDegree 5
  minDegree 1
  meanDegree 1.3846153846153846
  density 0.03643724696356275
  graphCliqueNumber 3
  node [
    id 0
    label "morten"
    origin "text"
  ]
  node [
    id 1
    label "skoubo"
    origin "text"
  ]
  node [
    id 2
    label "Morten"
  ]
  node [
    id 3
    label "Skoubo"
  ]
  node [
    id 4
    label "Struer"
  ]
  node [
    id 5
    label "Boldklub"
  ]
  node [
    id 6
    label "Holstebro"
  ]
  node [
    id 7
    label "Ikast"
  ]
  node [
    id 8
    label "fS"
  ]
  node [
    id 9
    label "FC"
  ]
  node [
    id 10
    label "Midjtylland"
  ]
  node [
    id 11
    label "Lyngby"
  ]
  node [
    id 12
    label "BK"
  ]
  node [
    id 13
    label "Borussii"
  ]
  node [
    id 14
    label "M&#246;nchengladbach"
  ]
  node [
    id 15
    label "1"
  ]
  node [
    id 16
    label "Kaiserslautern"
  ]
  node [
    id 17
    label "westa"
  ]
  node [
    id 18
    label "Bromwich"
  ]
  node [
    id 19
    label "Albion"
  ]
  node [
    id 20
    label "division"
  ]
  node [
    id 21
    label "on"
  ]
  node [
    id 22
    label "Br&#248;ndby"
  ]
  node [
    id 23
    label "IF"
  ]
  node [
    id 24
    label "Midtjylland"
  ]
  node [
    id 25
    label "Johanem"
  ]
  node [
    id 26
    label "Elmanderem"
  ]
  node [
    id 27
    label "puchar"
  ]
  node [
    id 28
    label "Dania"
  ]
  node [
    id 29
    label "real"
  ]
  node [
    id 30
    label "Sociedad"
  ]
  node [
    id 31
    label "Espanyolem"
  ]
  node [
    id 32
    label "Barcelona"
  ]
  node [
    id 33
    label "Primera"
  ]
  node [
    id 34
    label "Segunda"
  ]
  node [
    id 35
    label "utrecht"
  ]
  node [
    id 36
    label "Willem"
  ]
  node [
    id 37
    label "ii"
  ]
  node [
    id 38
    label "Tilburg"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 15
  ]
  edge [
    source 9
    target 16
  ]
  edge [
    source 9
    target 24
  ]
  edge [
    source 9
    target 35
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 19
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 33
  ]
  edge [
    source 20
    target 34
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 38
  ]
  edge [
    source 37
    target 38
  ]
]
