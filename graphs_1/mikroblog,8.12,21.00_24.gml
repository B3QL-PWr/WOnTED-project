graph [
  maxDegree 22
  minDegree 1
  meanDegree 2
  density 0.04878048780487805
  graphCliqueNumber 2
  node [
    id 0
    label "polski"
    origin "text"
  ]
  node [
    id 1
    label "&#322;aciata"
    origin "text"
  ]
  node [
    id 2
    label "chi&#324;ski"
    origin "text"
  ]
  node [
    id 3
    label "supermarket"
    origin "text"
  ]
  node [
    id 4
    label "guangzhou"
    origin "text"
  ]
  node [
    id 5
    label "oko&#322;o"
    origin "text"
  ]
  node [
    id 6
    label "matiwojwchinach"
    origin "text"
  ]
  node [
    id 7
    label "lacki"
  ]
  node [
    id 8
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 9
    label "przedmiot"
  ]
  node [
    id 10
    label "sztajer"
  ]
  node [
    id 11
    label "drabant"
  ]
  node [
    id 12
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 13
    label "polak"
  ]
  node [
    id 14
    label "pierogi_ruskie"
  ]
  node [
    id 15
    label "krakowiak"
  ]
  node [
    id 16
    label "Polish"
  ]
  node [
    id 17
    label "j&#281;zyk"
  ]
  node [
    id 18
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 19
    label "oberek"
  ]
  node [
    id 20
    label "po_polsku"
  ]
  node [
    id 21
    label "mazur"
  ]
  node [
    id 22
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 23
    label "chodzony"
  ]
  node [
    id 24
    label "skoczny"
  ]
  node [
    id 25
    label "ryba_po_grecku"
  ]
  node [
    id 26
    label "goniony"
  ]
  node [
    id 27
    label "polsko"
  ]
  node [
    id 28
    label "kitajski"
  ]
  node [
    id 29
    label "kaczka_po_peki&#324;sku"
  ]
  node [
    id 30
    label "dziwaczny"
  ]
  node [
    id 31
    label "tandetny"
  ]
  node [
    id 32
    label "makroj&#281;zyk"
  ]
  node [
    id 33
    label "chi&#324;sko"
  ]
  node [
    id 34
    label "po_chi&#324;sku"
  ]
  node [
    id 35
    label "j&#281;zyk_chi&#324;sko-tybeta&#324;ski"
  ]
  node [
    id 36
    label "azjatycki"
  ]
  node [
    id 37
    label "lipny"
  ]
  node [
    id 38
    label "go"
  ]
  node [
    id 39
    label "niedrogi"
  ]
  node [
    id 40
    label "dalekowschodni"
  ]
  node [
    id 41
    label "market"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
]
