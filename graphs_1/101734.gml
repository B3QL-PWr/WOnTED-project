graph [
  maxDegree 47
  minDegree 1
  meanDegree 2.0242424242424244
  density 0.012342941611234294
  graphCliqueNumber 3
  node [
    id 0
    label "art"
    origin "text"
  ]
  node [
    id 1
    label "decyzja"
    origin "text"
  ]
  node [
    id 2
    label "sprawa"
    origin "text"
  ]
  node [
    id 3
    label "ekwiwalent"
    origin "text"
  ]
  node [
    id 4
    label "wydawa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "ten"
    origin "text"
  ]
  node [
    id 6
    label "wyp&#322;aca&#263;"
    origin "text"
  ]
  node [
    id 7
    label "jednostka"
    origin "text"
  ]
  node [
    id 8
    label "organizacyjny"
    origin "text"
  ]
  node [
    id 9
    label "zus"
    origin "text"
  ]
  node [
    id 10
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 11
    label "ustali&#263;"
    origin "text"
  ]
  node [
    id 12
    label "prawo"
    origin "text"
  ]
  node [
    id 13
    label "emerytura"
    origin "text"
  ]
  node [
    id 14
    label "lub"
    origin "text"
  ]
  node [
    id 15
    label "renta"
    origin "text"
  ]
  node [
    id 16
    label "dokument"
  ]
  node [
    id 17
    label "resolution"
  ]
  node [
    id 18
    label "zdecydowanie"
  ]
  node [
    id 19
    label "wytw&#243;r"
  ]
  node [
    id 20
    label "podj&#281;cie_decyzji"
  ]
  node [
    id 21
    label "management"
  ]
  node [
    id 22
    label "temat"
  ]
  node [
    id 23
    label "kognicja"
  ]
  node [
    id 24
    label "idea"
  ]
  node [
    id 25
    label "szczeg&#243;&#322;"
  ]
  node [
    id 26
    label "rzecz"
  ]
  node [
    id 27
    label "wydarzenie"
  ]
  node [
    id 28
    label "przes&#322;anka"
  ]
  node [
    id 29
    label "rozprawa"
  ]
  node [
    id 30
    label "object"
  ]
  node [
    id 31
    label "proposition"
  ]
  node [
    id 32
    label "odpowiednik"
  ]
  node [
    id 33
    label "impart"
  ]
  node [
    id 34
    label "panna_na_wydaniu"
  ]
  node [
    id 35
    label "surrender"
  ]
  node [
    id 36
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 37
    label "train"
  ]
  node [
    id 38
    label "give"
  ]
  node [
    id 39
    label "wytwarza&#263;"
  ]
  node [
    id 40
    label "dawa&#263;"
  ]
  node [
    id 41
    label "zapach"
  ]
  node [
    id 42
    label "wprowadza&#263;"
  ]
  node [
    id 43
    label "ujawnia&#263;"
  ]
  node [
    id 44
    label "wydawnictwo"
  ]
  node [
    id 45
    label "powierza&#263;"
  ]
  node [
    id 46
    label "produkcja"
  ]
  node [
    id 47
    label "denuncjowa&#263;"
  ]
  node [
    id 48
    label "mie&#263;_miejsce"
  ]
  node [
    id 49
    label "plon"
  ]
  node [
    id 50
    label "reszta"
  ]
  node [
    id 51
    label "robi&#263;"
  ]
  node [
    id 52
    label "placard"
  ]
  node [
    id 53
    label "tajemnica"
  ]
  node [
    id 54
    label "wiano"
  ]
  node [
    id 55
    label "kojarzy&#263;"
  ]
  node [
    id 56
    label "d&#378;wi&#281;k"
  ]
  node [
    id 57
    label "podawa&#263;"
  ]
  node [
    id 58
    label "okre&#347;lony"
  ]
  node [
    id 59
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 60
    label "p&#322;aci&#263;"
  ]
  node [
    id 61
    label "odwzajemnia&#263;_si&#281;"
  ]
  node [
    id 62
    label "refund"
  ]
  node [
    id 63
    label "infimum"
  ]
  node [
    id 64
    label "ewoluowanie"
  ]
  node [
    id 65
    label "przyswoi&#263;"
  ]
  node [
    id 66
    label "reakcja"
  ]
  node [
    id 67
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 68
    label "wyewoluowanie"
  ]
  node [
    id 69
    label "individual"
  ]
  node [
    id 70
    label "profanum"
  ]
  node [
    id 71
    label "starzenie_si&#281;"
  ]
  node [
    id 72
    label "homo_sapiens"
  ]
  node [
    id 73
    label "skala"
  ]
  node [
    id 74
    label "supremum"
  ]
  node [
    id 75
    label "przyswaja&#263;"
  ]
  node [
    id 76
    label "ludzko&#347;&#263;"
  ]
  node [
    id 77
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 78
    label "one"
  ]
  node [
    id 79
    label "funkcja"
  ]
  node [
    id 80
    label "przeliczenie"
  ]
  node [
    id 81
    label "przeliczanie"
  ]
  node [
    id 82
    label "mikrokosmos"
  ]
  node [
    id 83
    label "rzut"
  ]
  node [
    id 84
    label "portrecista"
  ]
  node [
    id 85
    label "przelicza&#263;"
  ]
  node [
    id 86
    label "przyswajanie"
  ]
  node [
    id 87
    label "duch"
  ]
  node [
    id 88
    label "wyewoluowa&#263;"
  ]
  node [
    id 89
    label "ewoluowa&#263;"
  ]
  node [
    id 90
    label "oddzia&#322;ywanie"
  ]
  node [
    id 91
    label "g&#322;owa"
  ]
  node [
    id 92
    label "liczba_naturalna"
  ]
  node [
    id 93
    label "poj&#281;cie"
  ]
  node [
    id 94
    label "osoba"
  ]
  node [
    id 95
    label "figura"
  ]
  node [
    id 96
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 97
    label "obiekt"
  ]
  node [
    id 98
    label "matematyka"
  ]
  node [
    id 99
    label "przyswojenie"
  ]
  node [
    id 100
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 101
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 102
    label "czynnik_biotyczny"
  ]
  node [
    id 103
    label "przeliczy&#263;"
  ]
  node [
    id 104
    label "antropochoria"
  ]
  node [
    id 105
    label "sk&#322;adka"
  ]
  node [
    id 106
    label "umocni&#263;"
  ]
  node [
    id 107
    label "bind"
  ]
  node [
    id 108
    label "zdecydowa&#263;"
  ]
  node [
    id 109
    label "spowodowa&#263;"
  ]
  node [
    id 110
    label "unwrap"
  ]
  node [
    id 111
    label "zrobi&#263;"
  ]
  node [
    id 112
    label "put"
  ]
  node [
    id 113
    label "obserwacja"
  ]
  node [
    id 114
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 115
    label "nauka_prawa"
  ]
  node [
    id 116
    label "dominion"
  ]
  node [
    id 117
    label "normatywizm"
  ]
  node [
    id 118
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 119
    label "qualification"
  ]
  node [
    id 120
    label "opis"
  ]
  node [
    id 121
    label "regu&#322;a_Allena"
  ]
  node [
    id 122
    label "normalizacja"
  ]
  node [
    id 123
    label "kazuistyka"
  ]
  node [
    id 124
    label "regu&#322;a_Glogera"
  ]
  node [
    id 125
    label "kultura_duchowa"
  ]
  node [
    id 126
    label "prawo_karne"
  ]
  node [
    id 127
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 128
    label "standard"
  ]
  node [
    id 129
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 130
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 131
    label "struktura"
  ]
  node [
    id 132
    label "szko&#322;a"
  ]
  node [
    id 133
    label "prawo_karne_procesowe"
  ]
  node [
    id 134
    label "prawo_Mendla"
  ]
  node [
    id 135
    label "przepis"
  ]
  node [
    id 136
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 137
    label "criterion"
  ]
  node [
    id 138
    label "kanonistyka"
  ]
  node [
    id 139
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 140
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 141
    label "wykonawczy"
  ]
  node [
    id 142
    label "twierdzenie"
  ]
  node [
    id 143
    label "judykatura"
  ]
  node [
    id 144
    label "legislacyjnie"
  ]
  node [
    id 145
    label "umocowa&#263;"
  ]
  node [
    id 146
    label "podmiot"
  ]
  node [
    id 147
    label "procesualistyka"
  ]
  node [
    id 148
    label "kierunek"
  ]
  node [
    id 149
    label "kryminologia"
  ]
  node [
    id 150
    label "kryminalistyka"
  ]
  node [
    id 151
    label "cywilistyka"
  ]
  node [
    id 152
    label "law"
  ]
  node [
    id 153
    label "zasada_d'Alemberta"
  ]
  node [
    id 154
    label "jurisprudence"
  ]
  node [
    id 155
    label "zasada"
  ]
  node [
    id 156
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 157
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 158
    label "czas"
  ]
  node [
    id 159
    label "ubezpieczenie_emerytalne"
  ]
  node [
    id 160
    label "&#347;wiadczenie_spo&#322;eczne"
  ]
  node [
    id 161
    label "egzystencja"
  ]
  node [
    id 162
    label "retirement"
  ]
  node [
    id 163
    label "economic_rent"
  ]
  node [
    id 164
    label "doch&#243;d"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 108
  ]
  edge [
    source 11
    target 109
  ]
  edge [
    source 11
    target 110
  ]
  edge [
    source 11
    target 111
  ]
  edge [
    source 11
    target 112
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 113
  ]
  edge [
    source 12
    target 114
  ]
  edge [
    source 12
    target 115
  ]
  edge [
    source 12
    target 116
  ]
  edge [
    source 12
    target 117
  ]
  edge [
    source 12
    target 118
  ]
  edge [
    source 12
    target 119
  ]
  edge [
    source 12
    target 120
  ]
  edge [
    source 12
    target 121
  ]
  edge [
    source 12
    target 122
  ]
  edge [
    source 12
    target 123
  ]
  edge [
    source 12
    target 124
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 12
    target 131
  ]
  edge [
    source 12
    target 132
  ]
  edge [
    source 12
    target 133
  ]
  edge [
    source 12
    target 134
  ]
  edge [
    source 12
    target 135
  ]
  edge [
    source 12
    target 136
  ]
  edge [
    source 12
    target 137
  ]
  edge [
    source 12
    target 138
  ]
  edge [
    source 12
    target 139
  ]
  edge [
    source 12
    target 140
  ]
  edge [
    source 12
    target 141
  ]
  edge [
    source 12
    target 142
  ]
  edge [
    source 12
    target 143
  ]
  edge [
    source 12
    target 144
  ]
  edge [
    source 12
    target 145
  ]
  edge [
    source 12
    target 146
  ]
  edge [
    source 12
    target 147
  ]
  edge [
    source 12
    target 148
  ]
  edge [
    source 12
    target 149
  ]
  edge [
    source 12
    target 150
  ]
  edge [
    source 12
    target 151
  ]
  edge [
    source 12
    target 152
  ]
  edge [
    source 12
    target 153
  ]
  edge [
    source 12
    target 154
  ]
  edge [
    source 12
    target 155
  ]
  edge [
    source 12
    target 156
  ]
  edge [
    source 12
    target 157
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 158
  ]
  edge [
    source 13
    target 159
  ]
  edge [
    source 13
    target 160
  ]
  edge [
    source 13
    target 161
  ]
  edge [
    source 13
    target 162
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 163
  ]
  edge [
    source 15
    target 164
  ]
  edge [
    source 15
    target 160
  ]
]
