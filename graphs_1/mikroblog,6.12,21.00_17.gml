graph [
  maxDegree 16
  minDegree 1
  meanDegree 1.9285714285714286
  density 0.07142857142857142
  graphCliqueNumber 2
  node [
    id 0
    label "by&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "w&#322;a&#347;nie"
    origin "text"
  ]
  node [
    id 2
    label "zrobi&#263;"
    origin "text"
  ]
  node [
    id 3
    label "przedszkole"
    origin "text"
  ]
  node [
    id 4
    label "dok&#322;adnie"
  ]
  node [
    id 5
    label "zorganizowa&#263;"
  ]
  node [
    id 6
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 7
    label "przerobi&#263;"
  ]
  node [
    id 8
    label "wystylizowa&#263;"
  ]
  node [
    id 9
    label "cause"
  ]
  node [
    id 10
    label "wydali&#263;"
  ]
  node [
    id 11
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 12
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 13
    label "post&#261;pi&#263;"
  ]
  node [
    id 14
    label "appoint"
  ]
  node [
    id 15
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 16
    label "nabra&#263;"
  ]
  node [
    id 17
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 18
    label "make"
  ]
  node [
    id 19
    label "farsa"
  ]
  node [
    id 20
    label "cyrk"
  ]
  node [
    id 21
    label "plac_zabaw"
  ]
  node [
    id 22
    label "stopek"
  ]
  node [
    id 23
    label "grupa"
  ]
  node [
    id 24
    label "siedziba"
  ]
  node [
    id 25
    label "dziecinada"
  ]
  node [
    id 26
    label "warunki"
  ]
  node [
    id 27
    label "plac&#243;wka_opieku&#324;czo-wychowawcza"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
]
