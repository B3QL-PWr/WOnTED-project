graph [
  maxDegree 324
  minDegree 1
  meanDegree 2.893970893970894
  density 0.0060291060291060294
  graphCliqueNumber 6
  node [
    id 0
    label "zdanie"
    origin "text"
  ]
  node [
    id 1
    label "proca"
    origin "text"
  ]
  node [
    id 2
    label "pyta&#263;"
    origin "text"
  ]
  node [
    id 3
    label "francuz"
    origin "text"
  ]
  node [
    id 4
    label "imigracja"
    origin "text"
  ]
  node [
    id 5
    label "negatywnie"
    origin "text"
  ]
  node [
    id 6
    label "wp&#322;ywa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "wzrost"
    origin "text"
  ]
  node [
    id 8
    label "gospodarczy"
    origin "text"
  ]
  node [
    id 9
    label "kraj"
    origin "text"
  ]
  node [
    id 10
    label "przysz&#322;o&#347;&#263;"
    origin "text"
  ]
  node [
    id 11
    label "pa&#324;stwo"
    origin "text"
  ]
  node [
    id 12
    label "to&#380;samo&#347;&#263;"
    origin "text"
  ]
  node [
    id 13
    label "narodowy"
    origin "text"
  ]
  node [
    id 14
    label "poszanowanie"
    origin "text"
  ]
  node [
    id 15
    label "laicko&#347;&#263;"
    origin "text"
  ]
  node [
    id 16
    label "francja"
    origin "text"
  ]
  node [
    id 17
    label "sp&#243;jno&#347;&#263;"
    origin "text"
  ]
  node [
    id 18
    label "spo&#322;ecze&#324;stwo"
    origin "text"
  ]
  node [
    id 19
    label "bezpiecze&#324;stwo"
    origin "text"
  ]
  node [
    id 20
    label "attitude"
  ]
  node [
    id 21
    label "system"
  ]
  node [
    id 22
    label "przedstawienie"
  ]
  node [
    id 23
    label "fraza"
  ]
  node [
    id 24
    label "prison_term"
  ]
  node [
    id 25
    label "adjudication"
  ]
  node [
    id 26
    label "przekazanie"
  ]
  node [
    id 27
    label "pass"
  ]
  node [
    id 28
    label "wyra&#380;enie"
  ]
  node [
    id 29
    label "okres"
  ]
  node [
    id 30
    label "z&#322;o&#380;enie_egzaminu"
  ]
  node [
    id 31
    label "wypowiedzenie"
  ]
  node [
    id 32
    label "konektyw"
  ]
  node [
    id 33
    label "zaliczenie"
  ]
  node [
    id 34
    label "stanowisko"
  ]
  node [
    id 35
    label "powierzenie"
  ]
  node [
    id 36
    label "antylogizm"
  ]
  node [
    id 37
    label "zmuszenie"
  ]
  node [
    id 38
    label "szko&#322;a"
  ]
  node [
    id 39
    label "bro&#324;_miotaj&#261;ca"
  ]
  node [
    id 40
    label "zabawka"
  ]
  node [
    id 41
    label "bro&#324;_my&#347;liwska"
  ]
  node [
    id 42
    label "bro&#324;"
  ]
  node [
    id 43
    label "catapult"
  ]
  node [
    id 44
    label "urwis"
  ]
  node [
    id 45
    label "inspect"
  ]
  node [
    id 46
    label "przes&#322;uchiwa&#263;"
  ]
  node [
    id 47
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 48
    label "ankieter"
  ]
  node [
    id 49
    label "sprawdza&#263;"
  ]
  node [
    id 50
    label "question"
  ]
  node [
    id 51
    label "bu&#322;ka_paryska"
  ]
  node [
    id 52
    label "klucz_nastawny"
  ]
  node [
    id 53
    label "&#347;l&#261;ski"
  ]
  node [
    id 54
    label "francuski"
  ]
  node [
    id 55
    label "warkocz"
  ]
  node [
    id 56
    label "migracja"
  ]
  node [
    id 57
    label "nap&#322;yw"
  ]
  node [
    id 58
    label "immigration"
  ]
  node [
    id 59
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 60
    label "imigrant"
  ]
  node [
    id 61
    label "z&#322;y"
  ]
  node [
    id 62
    label "&#378;le"
  ]
  node [
    id 63
    label "negatywny"
  ]
  node [
    id 64
    label "ujemny"
  ]
  node [
    id 65
    label "zasila&#263;"
  ]
  node [
    id 66
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 67
    label "work"
  ]
  node [
    id 68
    label "przyp&#322;ywa&#263;"
  ]
  node [
    id 69
    label "kapita&#322;"
  ]
  node [
    id 70
    label "determine"
  ]
  node [
    id 71
    label "dochodzi&#263;"
  ]
  node [
    id 72
    label "pour"
  ]
  node [
    id 73
    label "ciek_wodny"
  ]
  node [
    id 74
    label "wegetacja"
  ]
  node [
    id 75
    label "wysoko&#347;&#263;"
  ]
  node [
    id 76
    label "increase"
  ]
  node [
    id 77
    label "rozw&#243;j"
  ]
  node [
    id 78
    label "zmiana"
  ]
  node [
    id 79
    label "gospodarski"
  ]
  node [
    id 80
    label "Skandynawia"
  ]
  node [
    id 81
    label "Rwanda"
  ]
  node [
    id 82
    label "Filipiny"
  ]
  node [
    id 83
    label "Yorkshire"
  ]
  node [
    id 84
    label "Kaukaz"
  ]
  node [
    id 85
    label "Podbeskidzie"
  ]
  node [
    id 86
    label "Toskania"
  ]
  node [
    id 87
    label "&#321;emkowszczyzna"
  ]
  node [
    id 88
    label "obszar"
  ]
  node [
    id 89
    label "Monako"
  ]
  node [
    id 90
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 91
    label "Amhara"
  ]
  node [
    id 92
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 93
    label "Lombardia"
  ]
  node [
    id 94
    label "Korea"
  ]
  node [
    id 95
    label "Kalabria"
  ]
  node [
    id 96
    label "Czarnog&#243;ra"
  ]
  node [
    id 97
    label "Ghana"
  ]
  node [
    id 98
    label "Tyrol"
  ]
  node [
    id 99
    label "Malawi"
  ]
  node [
    id 100
    label "Indonezja"
  ]
  node [
    id 101
    label "Bu&#322;garia"
  ]
  node [
    id 102
    label "Nauru"
  ]
  node [
    id 103
    label "Kenia"
  ]
  node [
    id 104
    label "Pamir"
  ]
  node [
    id 105
    label "Kambod&#380;a"
  ]
  node [
    id 106
    label "Lubelszczyzna"
  ]
  node [
    id 107
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 108
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 109
    label "Mali"
  ]
  node [
    id 110
    label "&#379;ywiecczyzna"
  ]
  node [
    id 111
    label "Austria"
  ]
  node [
    id 112
    label "interior"
  ]
  node [
    id 113
    label "Europa_Wschodnia"
  ]
  node [
    id 114
    label "Armenia"
  ]
  node [
    id 115
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 116
    label "Fid&#380;i"
  ]
  node [
    id 117
    label "Tuwalu"
  ]
  node [
    id 118
    label "Zabajkale"
  ]
  node [
    id 119
    label "Etiopia"
  ]
  node [
    id 120
    label "Malezja"
  ]
  node [
    id 121
    label "Malta"
  ]
  node [
    id 122
    label "Kaszuby"
  ]
  node [
    id 123
    label "Noworosja"
  ]
  node [
    id 124
    label "Bo&#347;nia"
  ]
  node [
    id 125
    label "Tad&#380;ykistan"
  ]
  node [
    id 126
    label "Grenada"
  ]
  node [
    id 127
    label "Ba&#322;kany"
  ]
  node [
    id 128
    label "Wehrlen"
  ]
  node [
    id 129
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 130
    label "Anglia"
  ]
  node [
    id 131
    label "Kielecczyzna"
  ]
  node [
    id 132
    label "Rumunia"
  ]
  node [
    id 133
    label "Pomorze_Zachodnie"
  ]
  node [
    id 134
    label "Maroko"
  ]
  node [
    id 135
    label "Bhutan"
  ]
  node [
    id 136
    label "Opolskie"
  ]
  node [
    id 137
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 138
    label "Ko&#322;yma"
  ]
  node [
    id 139
    label "Oksytania"
  ]
  node [
    id 140
    label "S&#322;owacja"
  ]
  node [
    id 141
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 142
    label "Seszele"
  ]
  node [
    id 143
    label "Syjon"
  ]
  node [
    id 144
    label "Kuwejt"
  ]
  node [
    id 145
    label "Arabia_Saudyjska"
  ]
  node [
    id 146
    label "Kociewie"
  ]
  node [
    id 147
    label "Kanada"
  ]
  node [
    id 148
    label "Ekwador"
  ]
  node [
    id 149
    label "ziemia"
  ]
  node [
    id 150
    label "Japonia"
  ]
  node [
    id 151
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 152
    label "Hiszpania"
  ]
  node [
    id 153
    label "Wyspy_Marshalla"
  ]
  node [
    id 154
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 155
    label "D&#380;ibuti"
  ]
  node [
    id 156
    label "Botswana"
  ]
  node [
    id 157
    label "Huculszczyzna"
  ]
  node [
    id 158
    label "Wietnam"
  ]
  node [
    id 159
    label "Egipt"
  ]
  node [
    id 160
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 161
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 162
    label "Burkina_Faso"
  ]
  node [
    id 163
    label "Bawaria"
  ]
  node [
    id 164
    label "Niemcy"
  ]
  node [
    id 165
    label "Khitai"
  ]
  node [
    id 166
    label "Macedonia"
  ]
  node [
    id 167
    label "Albania"
  ]
  node [
    id 168
    label "Madagaskar"
  ]
  node [
    id 169
    label "Bahrajn"
  ]
  node [
    id 170
    label "Jemen"
  ]
  node [
    id 171
    label "Lesoto"
  ]
  node [
    id 172
    label "Maghreb"
  ]
  node [
    id 173
    label "Samoa"
  ]
  node [
    id 174
    label "Andora"
  ]
  node [
    id 175
    label "Bory_Tucholskie"
  ]
  node [
    id 176
    label "Chiny"
  ]
  node [
    id 177
    label "Europa_Zachodnia"
  ]
  node [
    id 178
    label "Cypr"
  ]
  node [
    id 179
    label "Wielka_Brytania"
  ]
  node [
    id 180
    label "Kerala"
  ]
  node [
    id 181
    label "Podhale"
  ]
  node [
    id 182
    label "Kabylia"
  ]
  node [
    id 183
    label "Ukraina"
  ]
  node [
    id 184
    label "Paragwaj"
  ]
  node [
    id 185
    label "Trynidad_i_Tobago"
  ]
  node [
    id 186
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 187
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 188
    label "Ma&#322;opolska"
  ]
  node [
    id 189
    label "Polesie"
  ]
  node [
    id 190
    label "Liguria"
  ]
  node [
    id 191
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 192
    label "Libia"
  ]
  node [
    id 193
    label "&#321;&#243;dzkie"
  ]
  node [
    id 194
    label "Surinam"
  ]
  node [
    id 195
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 196
    label "Palestyna"
  ]
  node [
    id 197
    label "Nigeria"
  ]
  node [
    id 198
    label "Australia"
  ]
  node [
    id 199
    label "Honduras"
  ]
  node [
    id 200
    label "Bojkowszczyzna"
  ]
  node [
    id 201
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 202
    label "Karaiby"
  ]
  node [
    id 203
    label "Peru"
  ]
  node [
    id 204
    label "USA"
  ]
  node [
    id 205
    label "Bangladesz"
  ]
  node [
    id 206
    label "Kazachstan"
  ]
  node [
    id 207
    label "Nepal"
  ]
  node [
    id 208
    label "Irak"
  ]
  node [
    id 209
    label "Nadrenia"
  ]
  node [
    id 210
    label "Sudan"
  ]
  node [
    id 211
    label "S&#261;decczyzna"
  ]
  node [
    id 212
    label "Sand&#380;ak"
  ]
  node [
    id 213
    label "San_Marino"
  ]
  node [
    id 214
    label "Burundi"
  ]
  node [
    id 215
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 216
    label "Dominikana"
  ]
  node [
    id 217
    label "Komory"
  ]
  node [
    id 218
    label "Zakarpacie"
  ]
  node [
    id 219
    label "Gwatemala"
  ]
  node [
    id 220
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 221
    label "Zag&#243;rze"
  ]
  node [
    id 222
    label "Andaluzja"
  ]
  node [
    id 223
    label "granica_pa&#324;stwa"
  ]
  node [
    id 224
    label "Turkiestan"
  ]
  node [
    id 225
    label "Naddniestrze"
  ]
  node [
    id 226
    label "Hercegowina"
  ]
  node [
    id 227
    label "Brunei"
  ]
  node [
    id 228
    label "Iran"
  ]
  node [
    id 229
    label "jednostka_administracyjna"
  ]
  node [
    id 230
    label "Zimbabwe"
  ]
  node [
    id 231
    label "Namibia"
  ]
  node [
    id 232
    label "Meksyk"
  ]
  node [
    id 233
    label "Opolszczyzna"
  ]
  node [
    id 234
    label "Kamerun"
  ]
  node [
    id 235
    label "Afryka_Wschodnia"
  ]
  node [
    id 236
    label "Szlezwik"
  ]
  node [
    id 237
    label "Lotaryngia"
  ]
  node [
    id 238
    label "Somalia"
  ]
  node [
    id 239
    label "Angola"
  ]
  node [
    id 240
    label "Gabon"
  ]
  node [
    id 241
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 242
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 243
    label "Nowa_Zelandia"
  ]
  node [
    id 244
    label "Mozambik"
  ]
  node [
    id 245
    label "Tunezja"
  ]
  node [
    id 246
    label "Tajwan"
  ]
  node [
    id 247
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 248
    label "Liban"
  ]
  node [
    id 249
    label "Jordania"
  ]
  node [
    id 250
    label "Tonga"
  ]
  node [
    id 251
    label "Czad"
  ]
  node [
    id 252
    label "Gwinea"
  ]
  node [
    id 253
    label "Liberia"
  ]
  node [
    id 254
    label "Belize"
  ]
  node [
    id 255
    label "Mazowsze"
  ]
  node [
    id 256
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 257
    label "Benin"
  ]
  node [
    id 258
    label "&#321;otwa"
  ]
  node [
    id 259
    label "Syria"
  ]
  node [
    id 260
    label "Afryka_Zachodnia"
  ]
  node [
    id 261
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 262
    label "Dominika"
  ]
  node [
    id 263
    label "Antigua_i_Barbuda"
  ]
  node [
    id 264
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 265
    label "Hanower"
  ]
  node [
    id 266
    label "Galicja"
  ]
  node [
    id 267
    label "Szkocja"
  ]
  node [
    id 268
    label "Walia"
  ]
  node [
    id 269
    label "Afganistan"
  ]
  node [
    id 270
    label "W&#322;ochy"
  ]
  node [
    id 271
    label "Kiribati"
  ]
  node [
    id 272
    label "Szwajcaria"
  ]
  node [
    id 273
    label "Powi&#347;le"
  ]
  node [
    id 274
    label "Chorwacja"
  ]
  node [
    id 275
    label "Sahara_Zachodnia"
  ]
  node [
    id 276
    label "Tajlandia"
  ]
  node [
    id 277
    label "Salwador"
  ]
  node [
    id 278
    label "Bahamy"
  ]
  node [
    id 279
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 280
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 281
    label "Zamojszczyzna"
  ]
  node [
    id 282
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 283
    label "S&#322;owenia"
  ]
  node [
    id 284
    label "Gambia"
  ]
  node [
    id 285
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 286
    label "Urugwaj"
  ]
  node [
    id 287
    label "Podlasie"
  ]
  node [
    id 288
    label "Zair"
  ]
  node [
    id 289
    label "Erytrea"
  ]
  node [
    id 290
    label "Laponia"
  ]
  node [
    id 291
    label "Kujawy"
  ]
  node [
    id 292
    label "Umbria"
  ]
  node [
    id 293
    label "Rosja"
  ]
  node [
    id 294
    label "Mauritius"
  ]
  node [
    id 295
    label "Niger"
  ]
  node [
    id 296
    label "Uganda"
  ]
  node [
    id 297
    label "Turkmenistan"
  ]
  node [
    id 298
    label "Turcja"
  ]
  node [
    id 299
    label "Mezoameryka"
  ]
  node [
    id 300
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 301
    label "Irlandia"
  ]
  node [
    id 302
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 303
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 304
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 305
    label "Gwinea_Bissau"
  ]
  node [
    id 306
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 307
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 308
    label "Kurdystan"
  ]
  node [
    id 309
    label "Belgia"
  ]
  node [
    id 310
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 311
    label "Palau"
  ]
  node [
    id 312
    label "Barbados"
  ]
  node [
    id 313
    label "Wenezuela"
  ]
  node [
    id 314
    label "W&#281;gry"
  ]
  node [
    id 315
    label "Chile"
  ]
  node [
    id 316
    label "Argentyna"
  ]
  node [
    id 317
    label "Kolumbia"
  ]
  node [
    id 318
    label "Armagnac"
  ]
  node [
    id 319
    label "Kampania"
  ]
  node [
    id 320
    label "Sierra_Leone"
  ]
  node [
    id 321
    label "Azerbejd&#380;an"
  ]
  node [
    id 322
    label "Kongo"
  ]
  node [
    id 323
    label "Polinezja"
  ]
  node [
    id 324
    label "Warmia"
  ]
  node [
    id 325
    label "Pakistan"
  ]
  node [
    id 326
    label "Liechtenstein"
  ]
  node [
    id 327
    label "Wielkopolska"
  ]
  node [
    id 328
    label "Nikaragua"
  ]
  node [
    id 329
    label "Senegal"
  ]
  node [
    id 330
    label "brzeg"
  ]
  node [
    id 331
    label "Bordeaux"
  ]
  node [
    id 332
    label "Lauda"
  ]
  node [
    id 333
    label "Indie"
  ]
  node [
    id 334
    label "Mazury"
  ]
  node [
    id 335
    label "Suazi"
  ]
  node [
    id 336
    label "Polska"
  ]
  node [
    id 337
    label "Algieria"
  ]
  node [
    id 338
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 339
    label "Jamajka"
  ]
  node [
    id 340
    label "Timor_Wschodni"
  ]
  node [
    id 341
    label "Oceania"
  ]
  node [
    id 342
    label "Kostaryka"
  ]
  node [
    id 343
    label "Lasko"
  ]
  node [
    id 344
    label "Podkarpacie"
  ]
  node [
    id 345
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 346
    label "Kuba"
  ]
  node [
    id 347
    label "Mauretania"
  ]
  node [
    id 348
    label "Amazonia"
  ]
  node [
    id 349
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 350
    label "Portoryko"
  ]
  node [
    id 351
    label "Brazylia"
  ]
  node [
    id 352
    label "Mo&#322;dawia"
  ]
  node [
    id 353
    label "organizacja"
  ]
  node [
    id 354
    label "Litwa"
  ]
  node [
    id 355
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 356
    label "Kirgistan"
  ]
  node [
    id 357
    label "Izrael"
  ]
  node [
    id 358
    label "Grecja"
  ]
  node [
    id 359
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 360
    label "Kurpie"
  ]
  node [
    id 361
    label "Holandia"
  ]
  node [
    id 362
    label "Sri_Lanka"
  ]
  node [
    id 363
    label "Tonkin"
  ]
  node [
    id 364
    label "Katar"
  ]
  node [
    id 365
    label "Azja_Wschodnia"
  ]
  node [
    id 366
    label "Kaszmir"
  ]
  node [
    id 367
    label "Mikronezja"
  ]
  node [
    id 368
    label "Ukraina_Zachodnia"
  ]
  node [
    id 369
    label "Laos"
  ]
  node [
    id 370
    label "Mongolia"
  ]
  node [
    id 371
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 372
    label "Malediwy"
  ]
  node [
    id 373
    label "Zambia"
  ]
  node [
    id 374
    label "Turyngia"
  ]
  node [
    id 375
    label "Tanzania"
  ]
  node [
    id 376
    label "Gujana"
  ]
  node [
    id 377
    label "Apulia"
  ]
  node [
    id 378
    label "Uzbekistan"
  ]
  node [
    id 379
    label "Panama"
  ]
  node [
    id 380
    label "Czechy"
  ]
  node [
    id 381
    label "Gruzja"
  ]
  node [
    id 382
    label "Baszkiria"
  ]
  node [
    id 383
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 384
    label "Francja"
  ]
  node [
    id 385
    label "Serbia"
  ]
  node [
    id 386
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 387
    label "Togo"
  ]
  node [
    id 388
    label "Estonia"
  ]
  node [
    id 389
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 390
    label "Indochiny"
  ]
  node [
    id 391
    label "Boliwia"
  ]
  node [
    id 392
    label "Oman"
  ]
  node [
    id 393
    label "Portugalia"
  ]
  node [
    id 394
    label "Wyspy_Salomona"
  ]
  node [
    id 395
    label "Haiti"
  ]
  node [
    id 396
    label "Luksemburg"
  ]
  node [
    id 397
    label "Lubuskie"
  ]
  node [
    id 398
    label "Biskupizna"
  ]
  node [
    id 399
    label "Birma"
  ]
  node [
    id 400
    label "Rodezja"
  ]
  node [
    id 401
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 402
    label "jutro"
  ]
  node [
    id 403
    label "cel"
  ]
  node [
    id 404
    label "czas"
  ]
  node [
    id 405
    label "para"
  ]
  node [
    id 406
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 407
    label "grupa"
  ]
  node [
    id 408
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 409
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 410
    label "holoarktyka"
  ]
  node [
    id 411
    label "Antarktis"
  ]
  node [
    id 412
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 413
    label "zwrot"
  ]
  node [
    id 414
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 415
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 416
    label "partia"
  ]
  node [
    id 417
    label "terytorium"
  ]
  node [
    id 418
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 419
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 420
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 421
    label "identity"
  ]
  node [
    id 422
    label "pesel"
  ]
  node [
    id 423
    label "uniformizm"
  ]
  node [
    id 424
    label "imi&#281;"
  ]
  node [
    id 425
    label "depersonalizacja"
  ]
  node [
    id 426
    label "to&#380;samo&#347;&#263;_osobista"
  ]
  node [
    id 427
    label "dane"
  ]
  node [
    id 428
    label "adres"
  ]
  node [
    id 429
    label "nazwisko"
  ]
  node [
    id 430
    label "self-consciousness"
  ]
  node [
    id 431
    label "poj&#281;cie"
  ]
  node [
    id 432
    label "r&#243;wno&#347;&#263;"
  ]
  node [
    id 433
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 434
    label "NN"
  ]
  node [
    id 435
    label "nacjonalistyczny"
  ]
  node [
    id 436
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 437
    label "narodowo"
  ]
  node [
    id 438
    label "wa&#380;ny"
  ]
  node [
    id 439
    label "zaimponowanie"
  ]
  node [
    id 440
    label "szanowa&#263;"
  ]
  node [
    id 441
    label "uhonorowa&#263;"
  ]
  node [
    id 442
    label "honorowanie"
  ]
  node [
    id 443
    label "uszanowa&#263;"
  ]
  node [
    id 444
    label "rewerencja"
  ]
  node [
    id 445
    label "uszanowanie"
  ]
  node [
    id 446
    label "imponowanie"
  ]
  node [
    id 447
    label "uhonorowanie"
  ]
  node [
    id 448
    label "respect"
  ]
  node [
    id 449
    label "honorowa&#263;"
  ]
  node [
    id 450
    label "postawa"
  ]
  node [
    id 451
    label "szacuneczek"
  ]
  node [
    id 452
    label "amatorstwo"
  ]
  node [
    id 453
    label "harmonijno&#347;&#263;"
  ]
  node [
    id 454
    label "&#347;cis&#322;o&#347;&#263;"
  ]
  node [
    id 455
    label "jednorodno&#347;&#263;"
  ]
  node [
    id 456
    label "pole"
  ]
  node [
    id 457
    label "kastowo&#347;&#263;"
  ]
  node [
    id 458
    label "ludzie_pracy"
  ]
  node [
    id 459
    label "community"
  ]
  node [
    id 460
    label "status"
  ]
  node [
    id 461
    label "cywilizacja"
  ]
  node [
    id 462
    label "pozaklasowy"
  ]
  node [
    id 463
    label "aspo&#322;eczny"
  ]
  node [
    id 464
    label "uwarstwienie"
  ]
  node [
    id 465
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 466
    label "elita"
  ]
  node [
    id 467
    label "wspo&#322;ecze&#324;stwo"
  ]
  node [
    id 468
    label "klasa"
  ]
  node [
    id 469
    label "BHP"
  ]
  node [
    id 470
    label "katapultowa&#263;"
  ]
  node [
    id 471
    label "ubezpiecza&#263;"
  ]
  node [
    id 472
    label "stan"
  ]
  node [
    id 473
    label "ubezpieczenie"
  ]
  node [
    id 474
    label "cecha"
  ]
  node [
    id 475
    label "porz&#261;dek"
  ]
  node [
    id 476
    label "ubezpieczy&#263;"
  ]
  node [
    id 477
    label "safety"
  ]
  node [
    id 478
    label "katapultowanie"
  ]
  node [
    id 479
    label "ubezpieczanie"
  ]
  node [
    id 480
    label "test_zderzeniowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 9
    target 100
  ]
  edge [
    source 9
    target 101
  ]
  edge [
    source 9
    target 102
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 104
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 9
    target 116
  ]
  edge [
    source 9
    target 117
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 9
    target 119
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 129
  ]
  edge [
    source 9
    target 130
  ]
  edge [
    source 9
    target 131
  ]
  edge [
    source 9
    target 132
  ]
  edge [
    source 9
    target 133
  ]
  edge [
    source 9
    target 134
  ]
  edge [
    source 9
    target 135
  ]
  edge [
    source 9
    target 136
  ]
  edge [
    source 9
    target 137
  ]
  edge [
    source 9
    target 138
  ]
  edge [
    source 9
    target 139
  ]
  edge [
    source 9
    target 140
  ]
  edge [
    source 9
    target 141
  ]
  edge [
    source 9
    target 142
  ]
  edge [
    source 9
    target 143
  ]
  edge [
    source 9
    target 144
  ]
  edge [
    source 9
    target 145
  ]
  edge [
    source 9
    target 146
  ]
  edge [
    source 9
    target 147
  ]
  edge [
    source 9
    target 148
  ]
  edge [
    source 9
    target 149
  ]
  edge [
    source 9
    target 150
  ]
  edge [
    source 9
    target 151
  ]
  edge [
    source 9
    target 152
  ]
  edge [
    source 9
    target 153
  ]
  edge [
    source 9
    target 154
  ]
  edge [
    source 9
    target 155
  ]
  edge [
    source 9
    target 156
  ]
  edge [
    source 9
    target 157
  ]
  edge [
    source 9
    target 158
  ]
  edge [
    source 9
    target 159
  ]
  edge [
    source 9
    target 160
  ]
  edge [
    source 9
    target 161
  ]
  edge [
    source 9
    target 162
  ]
  edge [
    source 9
    target 163
  ]
  edge [
    source 9
    target 164
  ]
  edge [
    source 9
    target 165
  ]
  edge [
    source 9
    target 166
  ]
  edge [
    source 9
    target 167
  ]
  edge [
    source 9
    target 168
  ]
  edge [
    source 9
    target 169
  ]
  edge [
    source 9
    target 170
  ]
  edge [
    source 9
    target 171
  ]
  edge [
    source 9
    target 172
  ]
  edge [
    source 9
    target 173
  ]
  edge [
    source 9
    target 174
  ]
  edge [
    source 9
    target 175
  ]
  edge [
    source 9
    target 176
  ]
  edge [
    source 9
    target 177
  ]
  edge [
    source 9
    target 178
  ]
  edge [
    source 9
    target 179
  ]
  edge [
    source 9
    target 180
  ]
  edge [
    source 9
    target 181
  ]
  edge [
    source 9
    target 182
  ]
  edge [
    source 9
    target 183
  ]
  edge [
    source 9
    target 184
  ]
  edge [
    source 9
    target 185
  ]
  edge [
    source 9
    target 186
  ]
  edge [
    source 9
    target 187
  ]
  edge [
    source 9
    target 188
  ]
  edge [
    source 9
    target 189
  ]
  edge [
    source 9
    target 190
  ]
  edge [
    source 9
    target 191
  ]
  edge [
    source 9
    target 192
  ]
  edge [
    source 9
    target 193
  ]
  edge [
    source 9
    target 194
  ]
  edge [
    source 9
    target 195
  ]
  edge [
    source 9
    target 196
  ]
  edge [
    source 9
    target 197
  ]
  edge [
    source 9
    target 198
  ]
  edge [
    source 9
    target 199
  ]
  edge [
    source 9
    target 200
  ]
  edge [
    source 9
    target 201
  ]
  edge [
    source 9
    target 202
  ]
  edge [
    source 9
    target 203
  ]
  edge [
    source 9
    target 204
  ]
  edge [
    source 9
    target 205
  ]
  edge [
    source 9
    target 206
  ]
  edge [
    source 9
    target 207
  ]
  edge [
    source 9
    target 208
  ]
  edge [
    source 9
    target 209
  ]
  edge [
    source 9
    target 210
  ]
  edge [
    source 9
    target 211
  ]
  edge [
    source 9
    target 212
  ]
  edge [
    source 9
    target 213
  ]
  edge [
    source 9
    target 214
  ]
  edge [
    source 9
    target 215
  ]
  edge [
    source 9
    target 216
  ]
  edge [
    source 9
    target 217
  ]
  edge [
    source 9
    target 218
  ]
  edge [
    source 9
    target 219
  ]
  edge [
    source 9
    target 220
  ]
  edge [
    source 9
    target 221
  ]
  edge [
    source 9
    target 222
  ]
  edge [
    source 9
    target 223
  ]
  edge [
    source 9
    target 224
  ]
  edge [
    source 9
    target 225
  ]
  edge [
    source 9
    target 226
  ]
  edge [
    source 9
    target 227
  ]
  edge [
    source 9
    target 228
  ]
  edge [
    source 9
    target 229
  ]
  edge [
    source 9
    target 230
  ]
  edge [
    source 9
    target 231
  ]
  edge [
    source 9
    target 232
  ]
  edge [
    source 9
    target 233
  ]
  edge [
    source 9
    target 234
  ]
  edge [
    source 9
    target 235
  ]
  edge [
    source 9
    target 236
  ]
  edge [
    source 9
    target 237
  ]
  edge [
    source 9
    target 238
  ]
  edge [
    source 9
    target 239
  ]
  edge [
    source 9
    target 240
  ]
  edge [
    source 9
    target 241
  ]
  edge [
    source 9
    target 242
  ]
  edge [
    source 9
    target 243
  ]
  edge [
    source 9
    target 244
  ]
  edge [
    source 9
    target 245
  ]
  edge [
    source 9
    target 246
  ]
  edge [
    source 9
    target 247
  ]
  edge [
    source 9
    target 248
  ]
  edge [
    source 9
    target 249
  ]
  edge [
    source 9
    target 250
  ]
  edge [
    source 9
    target 251
  ]
  edge [
    source 9
    target 252
  ]
  edge [
    source 9
    target 253
  ]
  edge [
    source 9
    target 254
  ]
  edge [
    source 9
    target 255
  ]
  edge [
    source 9
    target 256
  ]
  edge [
    source 9
    target 257
  ]
  edge [
    source 9
    target 258
  ]
  edge [
    source 9
    target 259
  ]
  edge [
    source 9
    target 260
  ]
  edge [
    source 9
    target 261
  ]
  edge [
    source 9
    target 262
  ]
  edge [
    source 9
    target 263
  ]
  edge [
    source 9
    target 264
  ]
  edge [
    source 9
    target 265
  ]
  edge [
    source 9
    target 266
  ]
  edge [
    source 9
    target 267
  ]
  edge [
    source 9
    target 268
  ]
  edge [
    source 9
    target 269
  ]
  edge [
    source 9
    target 270
  ]
  edge [
    source 9
    target 271
  ]
  edge [
    source 9
    target 272
  ]
  edge [
    source 9
    target 273
  ]
  edge [
    source 9
    target 274
  ]
  edge [
    source 9
    target 275
  ]
  edge [
    source 9
    target 276
  ]
  edge [
    source 9
    target 277
  ]
  edge [
    source 9
    target 278
  ]
  edge [
    source 9
    target 279
  ]
  edge [
    source 9
    target 280
  ]
  edge [
    source 9
    target 281
  ]
  edge [
    source 9
    target 282
  ]
  edge [
    source 9
    target 283
  ]
  edge [
    source 9
    target 284
  ]
  edge [
    source 9
    target 285
  ]
  edge [
    source 9
    target 286
  ]
  edge [
    source 9
    target 287
  ]
  edge [
    source 9
    target 288
  ]
  edge [
    source 9
    target 289
  ]
  edge [
    source 9
    target 290
  ]
  edge [
    source 9
    target 291
  ]
  edge [
    source 9
    target 292
  ]
  edge [
    source 9
    target 293
  ]
  edge [
    source 9
    target 294
  ]
  edge [
    source 9
    target 295
  ]
  edge [
    source 9
    target 296
  ]
  edge [
    source 9
    target 297
  ]
  edge [
    source 9
    target 298
  ]
  edge [
    source 9
    target 299
  ]
  edge [
    source 9
    target 300
  ]
  edge [
    source 9
    target 301
  ]
  edge [
    source 9
    target 302
  ]
  edge [
    source 9
    target 303
  ]
  edge [
    source 9
    target 304
  ]
  edge [
    source 9
    target 305
  ]
  edge [
    source 9
    target 306
  ]
  edge [
    source 9
    target 307
  ]
  edge [
    source 9
    target 308
  ]
  edge [
    source 9
    target 309
  ]
  edge [
    source 9
    target 310
  ]
  edge [
    source 9
    target 311
  ]
  edge [
    source 9
    target 312
  ]
  edge [
    source 9
    target 313
  ]
  edge [
    source 9
    target 314
  ]
  edge [
    source 9
    target 315
  ]
  edge [
    source 9
    target 316
  ]
  edge [
    source 9
    target 317
  ]
  edge [
    source 9
    target 318
  ]
  edge [
    source 9
    target 319
  ]
  edge [
    source 9
    target 320
  ]
  edge [
    source 9
    target 321
  ]
  edge [
    source 9
    target 322
  ]
  edge [
    source 9
    target 323
  ]
  edge [
    source 9
    target 324
  ]
  edge [
    source 9
    target 325
  ]
  edge [
    source 9
    target 326
  ]
  edge [
    source 9
    target 327
  ]
  edge [
    source 9
    target 328
  ]
  edge [
    source 9
    target 329
  ]
  edge [
    source 9
    target 330
  ]
  edge [
    source 9
    target 331
  ]
  edge [
    source 9
    target 332
  ]
  edge [
    source 9
    target 333
  ]
  edge [
    source 9
    target 334
  ]
  edge [
    source 9
    target 335
  ]
  edge [
    source 9
    target 336
  ]
  edge [
    source 9
    target 337
  ]
  edge [
    source 9
    target 338
  ]
  edge [
    source 9
    target 339
  ]
  edge [
    source 9
    target 340
  ]
  edge [
    source 9
    target 341
  ]
  edge [
    source 9
    target 342
  ]
  edge [
    source 9
    target 343
  ]
  edge [
    source 9
    target 344
  ]
  edge [
    source 9
    target 345
  ]
  edge [
    source 9
    target 346
  ]
  edge [
    source 9
    target 347
  ]
  edge [
    source 9
    target 348
  ]
  edge [
    source 9
    target 349
  ]
  edge [
    source 9
    target 350
  ]
  edge [
    source 9
    target 351
  ]
  edge [
    source 9
    target 352
  ]
  edge [
    source 9
    target 353
  ]
  edge [
    source 9
    target 354
  ]
  edge [
    source 9
    target 355
  ]
  edge [
    source 9
    target 356
  ]
  edge [
    source 9
    target 357
  ]
  edge [
    source 9
    target 358
  ]
  edge [
    source 9
    target 359
  ]
  edge [
    source 9
    target 360
  ]
  edge [
    source 9
    target 361
  ]
  edge [
    source 9
    target 362
  ]
  edge [
    source 9
    target 363
  ]
  edge [
    source 9
    target 364
  ]
  edge [
    source 9
    target 365
  ]
  edge [
    source 9
    target 366
  ]
  edge [
    source 9
    target 367
  ]
  edge [
    source 9
    target 368
  ]
  edge [
    source 9
    target 369
  ]
  edge [
    source 9
    target 370
  ]
  edge [
    source 9
    target 371
  ]
  edge [
    source 9
    target 372
  ]
  edge [
    source 9
    target 373
  ]
  edge [
    source 9
    target 374
  ]
  edge [
    source 9
    target 375
  ]
  edge [
    source 9
    target 376
  ]
  edge [
    source 9
    target 377
  ]
  edge [
    source 9
    target 378
  ]
  edge [
    source 9
    target 379
  ]
  edge [
    source 9
    target 380
  ]
  edge [
    source 9
    target 381
  ]
  edge [
    source 9
    target 382
  ]
  edge [
    source 9
    target 383
  ]
  edge [
    source 9
    target 384
  ]
  edge [
    source 9
    target 385
  ]
  edge [
    source 9
    target 386
  ]
  edge [
    source 9
    target 387
  ]
  edge [
    source 9
    target 388
  ]
  edge [
    source 9
    target 389
  ]
  edge [
    source 9
    target 390
  ]
  edge [
    source 9
    target 391
  ]
  edge [
    source 9
    target 392
  ]
  edge [
    source 9
    target 393
  ]
  edge [
    source 9
    target 394
  ]
  edge [
    source 9
    target 395
  ]
  edge [
    source 9
    target 396
  ]
  edge [
    source 9
    target 397
  ]
  edge [
    source 9
    target 398
  ]
  edge [
    source 9
    target 399
  ]
  edge [
    source 9
    target 400
  ]
  edge [
    source 9
    target 401
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 402
  ]
  edge [
    source 10
    target 403
  ]
  edge [
    source 10
    target 404
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 10
    target 14
  ]
  edge [
    source 10
    target 17
  ]
  edge [
    source 10
    target 19
  ]
  edge [
    source 11
    target 81
  ]
  edge [
    source 11
    target 82
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 11
    target 100
  ]
  edge [
    source 11
    target 101
  ]
  edge [
    source 11
    target 102
  ]
  edge [
    source 11
    target 103
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 109
  ]
  edge [
    source 11
    target 111
  ]
  edge [
    source 11
    target 112
  ]
  edge [
    source 11
    target 114
  ]
  edge [
    source 11
    target 116
  ]
  edge [
    source 11
    target 117
  ]
  edge [
    source 11
    target 119
  ]
  edge [
    source 11
    target 120
  ]
  edge [
    source 11
    target 121
  ]
  edge [
    source 11
    target 125
  ]
  edge [
    source 11
    target 126
  ]
  edge [
    source 11
    target 128
  ]
  edge [
    source 11
    target 405
  ]
  edge [
    source 11
    target 406
  ]
  edge [
    source 11
    target 132
  ]
  edge [
    source 11
    target 134
  ]
  edge [
    source 11
    target 135
  ]
  edge [
    source 11
    target 140
  ]
  edge [
    source 11
    target 141
  ]
  edge [
    source 11
    target 142
  ]
  edge [
    source 11
    target 144
  ]
  edge [
    source 11
    target 145
  ]
  edge [
    source 11
    target 147
  ]
  edge [
    source 11
    target 148
  ]
  edge [
    source 11
    target 150
  ]
  edge [
    source 11
    target 149
  ]
  edge [
    source 11
    target 151
  ]
  edge [
    source 11
    target 152
  ]
  edge [
    source 11
    target 153
  ]
  edge [
    source 11
    target 154
  ]
  edge [
    source 11
    target 155
  ]
  edge [
    source 11
    target 156
  ]
  edge [
    source 11
    target 407
  ]
  edge [
    source 11
    target 158
  ]
  edge [
    source 11
    target 159
  ]
  edge [
    source 11
    target 162
  ]
  edge [
    source 11
    target 164
  ]
  edge [
    source 11
    target 165
  ]
  edge [
    source 11
    target 166
  ]
  edge [
    source 11
    target 167
  ]
  edge [
    source 11
    target 168
  ]
  edge [
    source 11
    target 169
  ]
  edge [
    source 11
    target 170
  ]
  edge [
    source 11
    target 171
  ]
  edge [
    source 11
    target 408
  ]
  edge [
    source 11
    target 173
  ]
  edge [
    source 11
    target 174
  ]
  edge [
    source 11
    target 176
  ]
  edge [
    source 11
    target 178
  ]
  edge [
    source 11
    target 179
  ]
  edge [
    source 11
    target 183
  ]
  edge [
    source 11
    target 184
  ]
  edge [
    source 11
    target 185
  ]
  edge [
    source 11
    target 191
  ]
  edge [
    source 11
    target 192
  ]
  edge [
    source 11
    target 194
  ]
  edge [
    source 11
    target 195
  ]
  edge [
    source 11
    target 197
  ]
  edge [
    source 11
    target 198
  ]
  edge [
    source 11
    target 199
  ]
  edge [
    source 11
    target 203
  ]
  edge [
    source 11
    target 204
  ]
  edge [
    source 11
    target 205
  ]
  edge [
    source 11
    target 409
  ]
  edge [
    source 11
    target 206
  ]
  edge [
    source 11
    target 410
  ]
  edge [
    source 11
    target 207
  ]
  edge [
    source 11
    target 210
  ]
  edge [
    source 11
    target 208
  ]
  edge [
    source 11
    target 213
  ]
  edge [
    source 11
    target 214
  ]
  edge [
    source 11
    target 216
  ]
  edge [
    source 11
    target 217
  ]
  edge [
    source 11
    target 223
  ]
  edge [
    source 11
    target 219
  ]
  edge [
    source 11
    target 411
  ]
  edge [
    source 11
    target 412
  ]
  edge [
    source 11
    target 227
  ]
  edge [
    source 11
    target 228
  ]
  edge [
    source 11
    target 230
  ]
  edge [
    source 11
    target 231
  ]
  edge [
    source 11
    target 232
  ]
  edge [
    source 11
    target 234
  ]
  edge [
    source 11
    target 413
  ]
  edge [
    source 11
    target 238
  ]
  edge [
    source 11
    target 239
  ]
  edge [
    source 11
    target 240
  ]
  edge [
    source 11
    target 241
  ]
  edge [
    source 11
    target 243
  ]
  edge [
    source 11
    target 244
  ]
  edge [
    source 11
    target 245
  ]
  edge [
    source 11
    target 246
  ]
  edge [
    source 11
    target 248
  ]
  edge [
    source 11
    target 249
  ]
  edge [
    source 11
    target 250
  ]
  edge [
    source 11
    target 251
  ]
  edge [
    source 11
    target 252
  ]
  edge [
    source 11
    target 253
  ]
  edge [
    source 11
    target 254
  ]
  edge [
    source 11
    target 257
  ]
  edge [
    source 11
    target 258
  ]
  edge [
    source 11
    target 259
  ]
  edge [
    source 11
    target 414
  ]
  edge [
    source 11
    target 415
  ]
  edge [
    source 11
    target 262
  ]
  edge [
    source 11
    target 263
  ]
  edge [
    source 11
    target 264
  ]
  edge [
    source 11
    target 265
  ]
  edge [
    source 11
    target 416
  ]
  edge [
    source 11
    target 269
  ]
  edge [
    source 11
    target 270
  ]
  edge [
    source 11
    target 271
  ]
  edge [
    source 11
    target 272
  ]
  edge [
    source 11
    target 274
  ]
  edge [
    source 11
    target 275
  ]
  edge [
    source 11
    target 276
  ]
  edge [
    source 11
    target 277
  ]
  edge [
    source 11
    target 278
  ]
  edge [
    source 11
    target 282
  ]
  edge [
    source 11
    target 283
  ]
  edge [
    source 11
    target 284
  ]
  edge [
    source 11
    target 286
  ]
  edge [
    source 11
    target 288
  ]
  edge [
    source 11
    target 289
  ]
  edge [
    source 11
    target 293
  ]
  edge [
    source 11
    target 294
  ]
  edge [
    source 11
    target 295
  ]
  edge [
    source 11
    target 296
  ]
  edge [
    source 11
    target 297
  ]
  edge [
    source 11
    target 298
  ]
  edge [
    source 11
    target 301
  ]
  edge [
    source 11
    target 302
  ]
  edge [
    source 11
    target 303
  ]
  edge [
    source 11
    target 305
  ]
  edge [
    source 11
    target 309
  ]
  edge [
    source 11
    target 310
  ]
  edge [
    source 11
    target 311
  ]
  edge [
    source 11
    target 312
  ]
  edge [
    source 11
    target 313
  ]
  edge [
    source 11
    target 314
  ]
  edge [
    source 11
    target 315
  ]
  edge [
    source 11
    target 316
  ]
  edge [
    source 11
    target 317
  ]
  edge [
    source 11
    target 320
  ]
  edge [
    source 11
    target 321
  ]
  edge [
    source 11
    target 322
  ]
  edge [
    source 11
    target 325
  ]
  edge [
    source 11
    target 326
  ]
  edge [
    source 11
    target 328
  ]
  edge [
    source 11
    target 329
  ]
  edge [
    source 11
    target 333
  ]
  edge [
    source 11
    target 335
  ]
  edge [
    source 11
    target 336
  ]
  edge [
    source 11
    target 337
  ]
  edge [
    source 11
    target 338
  ]
  edge [
    source 11
    target 417
  ]
  edge [
    source 11
    target 349
  ]
  edge [
    source 11
    target 339
  ]
  edge [
    source 11
    target 342
  ]
  edge [
    source 11
    target 340
  ]
  edge [
    source 11
    target 345
  ]
  edge [
    source 11
    target 346
  ]
  edge [
    source 11
    target 347
  ]
  edge [
    source 11
    target 350
  ]
  edge [
    source 11
    target 351
  ]
  edge [
    source 11
    target 352
  ]
  edge [
    source 11
    target 353
  ]
  edge [
    source 11
    target 418
  ]
  edge [
    source 11
    target 354
  ]
  edge [
    source 11
    target 355
  ]
  edge [
    source 11
    target 419
  ]
  edge [
    source 11
    target 357
  ]
  edge [
    source 11
    target 358
  ]
  edge [
    source 11
    target 356
  ]
  edge [
    source 11
    target 361
  ]
  edge [
    source 11
    target 362
  ]
  edge [
    source 11
    target 364
  ]
  edge [
    source 11
    target 367
  ]
  edge [
    source 11
    target 369
  ]
  edge [
    source 11
    target 370
  ]
  edge [
    source 11
    target 372
  ]
  edge [
    source 11
    target 373
  ]
  edge [
    source 11
    target 375
  ]
  edge [
    source 11
    target 376
  ]
  edge [
    source 11
    target 378
  ]
  edge [
    source 11
    target 379
  ]
  edge [
    source 11
    target 380
  ]
  edge [
    source 11
    target 381
  ]
  edge [
    source 11
    target 385
  ]
  edge [
    source 11
    target 384
  ]
  edge [
    source 11
    target 383
  ]
  edge [
    source 11
    target 387
  ]
  edge [
    source 11
    target 388
  ]
  edge [
    source 11
    target 391
  ]
  edge [
    source 11
    target 420
  ]
  edge [
    source 11
    target 392
  ]
  edge [
    source 11
    target 394
  ]
  edge [
    source 11
    target 395
  ]
  edge [
    source 11
    target 396
  ]
  edge [
    source 11
    target 393
  ]
  edge [
    source 11
    target 399
  ]
  edge [
    source 11
    target 400
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 421
  ]
  edge [
    source 12
    target 422
  ]
  edge [
    source 12
    target 423
  ]
  edge [
    source 12
    target 424
  ]
  edge [
    source 12
    target 425
  ]
  edge [
    source 12
    target 426
  ]
  edge [
    source 12
    target 427
  ]
  edge [
    source 12
    target 428
  ]
  edge [
    source 12
    target 429
  ]
  edge [
    source 12
    target 430
  ]
  edge [
    source 12
    target 431
  ]
  edge [
    source 12
    target 432
  ]
  edge [
    source 12
    target 433
  ]
  edge [
    source 12
    target 434
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 12
    target 17
  ]
  edge [
    source 12
    target 19
  ]
  edge [
    source 13
    target 435
  ]
  edge [
    source 13
    target 436
  ]
  edge [
    source 13
    target 437
  ]
  edge [
    source 13
    target 438
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 439
  ]
  edge [
    source 14
    target 440
  ]
  edge [
    source 14
    target 441
  ]
  edge [
    source 14
    target 442
  ]
  edge [
    source 14
    target 443
  ]
  edge [
    source 14
    target 444
  ]
  edge [
    source 14
    target 445
  ]
  edge [
    source 14
    target 446
  ]
  edge [
    source 14
    target 447
  ]
  edge [
    source 14
    target 448
  ]
  edge [
    source 14
    target 449
  ]
  edge [
    source 14
    target 450
  ]
  edge [
    source 14
    target 451
  ]
  edge [
    source 14
    target 17
  ]
  edge [
    source 14
    target 19
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 452
  ]
  edge [
    source 15
    target 450
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 453
  ]
  edge [
    source 17
    target 454
  ]
  edge [
    source 17
    target 455
  ]
  edge [
    source 17
    target 19
  ]
  edge [
    source 18
    target 456
  ]
  edge [
    source 18
    target 457
  ]
  edge [
    source 18
    target 458
  ]
  edge [
    source 18
    target 459
  ]
  edge [
    source 18
    target 460
  ]
  edge [
    source 18
    target 461
  ]
  edge [
    source 18
    target 462
  ]
  edge [
    source 18
    target 463
  ]
  edge [
    source 18
    target 464
  ]
  edge [
    source 18
    target 465
  ]
  edge [
    source 18
    target 466
  ]
  edge [
    source 18
    target 59
  ]
  edge [
    source 18
    target 467
  ]
  edge [
    source 18
    target 468
  ]
  edge [
    source 19
    target 469
  ]
  edge [
    source 19
    target 470
  ]
  edge [
    source 19
    target 471
  ]
  edge [
    source 19
    target 472
  ]
  edge [
    source 19
    target 473
  ]
  edge [
    source 19
    target 474
  ]
  edge [
    source 19
    target 475
  ]
  edge [
    source 19
    target 476
  ]
  edge [
    source 19
    target 477
  ]
  edge [
    source 19
    target 478
  ]
  edge [
    source 19
    target 479
  ]
  edge [
    source 19
    target 480
  ]
]
