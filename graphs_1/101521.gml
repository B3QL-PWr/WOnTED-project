graph [
  maxDegree 38
  minDegree 1
  meanDegree 2.045714285714286
  density 0.011756978653530377
  graphCliqueNumber 2
  node [
    id 0
    label "kto"
    origin "text"
  ]
  node [
    id 1
    label "bok"
    origin "text"
  ]
  node [
    id 2
    label "patrzy&#263;"
    origin "text"
  ]
  node [
    id 3
    label "niechybnie"
    origin "text"
  ]
  node [
    id 4
    label "wzi&#261;&#263;by"
    origin "text"
  ]
  node [
    id 5
    label "wariat"
    origin "text"
  ]
  node [
    id 6
    label "ci&#261;gle"
    origin "text"
  ]
  node [
    id 7
    label "sam"
    origin "text"
  ]
  node [
    id 8
    label "siebie"
    origin "text"
  ]
  node [
    id 9
    label "g&#322;os"
    origin "text"
  ]
  node [
    id 10
    label "rozmawia&#263;by&#263;"
    origin "text"
  ]
  node [
    id 11
    label "rado&#347;&#263;"
    origin "text"
  ]
  node [
    id 12
    label "tak"
    origin "text"
  ]
  node [
    id 13
    label "przepe&#322;ni&#263;"
    origin "text"
  ]
  node [
    id 14
    label "moja"
    origin "text"
  ]
  node [
    id 15
    label "dusza"
    origin "text"
  ]
  node [
    id 16
    label "&#380;em"
    origin "text"
  ]
  node [
    id 17
    label "musia&#322;"
    origin "text"
  ]
  node [
    id 18
    label "wygada&#263;"
    origin "text"
  ]
  node [
    id 19
    label "chocia&#380;"
    origin "text"
  ]
  node [
    id 20
    label "spos&#243;b"
    origin "text"
  ]
  node [
    id 21
    label "ul&#380;y&#263;"
    origin "text"
  ]
  node [
    id 22
    label "niejako"
    origin "text"
  ]
  node [
    id 23
    label "wezbrany"
    origin "text"
  ]
  node [
    id 24
    label "uczucie"
    origin "text"
  ]
  node [
    id 25
    label "strzelba"
  ]
  node [
    id 26
    label "wielok&#261;t"
  ]
  node [
    id 27
    label "&#347;ciana"
  ]
  node [
    id 28
    label "kierunek"
  ]
  node [
    id 29
    label "odcinek"
  ]
  node [
    id 30
    label "tu&#322;&#243;w"
  ]
  node [
    id 31
    label "bro&#324;_my&#347;liwska"
  ]
  node [
    id 32
    label "lufa"
  ]
  node [
    id 33
    label "strona"
  ]
  node [
    id 34
    label "koso"
  ]
  node [
    id 35
    label "szuka&#263;"
  ]
  node [
    id 36
    label "go_steady"
  ]
  node [
    id 37
    label "dba&#263;"
  ]
  node [
    id 38
    label "traktowa&#263;"
  ]
  node [
    id 39
    label "os&#261;dza&#263;"
  ]
  node [
    id 40
    label "punkt_widzenia"
  ]
  node [
    id 41
    label "robi&#263;"
  ]
  node [
    id 42
    label "uwa&#380;a&#263;"
  ]
  node [
    id 43
    label "look"
  ]
  node [
    id 44
    label "pogl&#261;da&#263;"
  ]
  node [
    id 45
    label "przygl&#261;da&#263;_si&#281;"
  ]
  node [
    id 46
    label "niechybny"
  ]
  node [
    id 47
    label "nieuniknienie"
  ]
  node [
    id 48
    label "cz&#322;owiek"
  ]
  node [
    id 49
    label "schizol"
  ]
  node [
    id 50
    label "chory_psychicznie"
  ]
  node [
    id 51
    label "stracenie_rozumu"
  ]
  node [
    id 52
    label "psychol"
  ]
  node [
    id 53
    label "zakr&#281;t"
  ]
  node [
    id 54
    label "orygina&#322;"
  ]
  node [
    id 55
    label "krejzol"
  ]
  node [
    id 56
    label "nieprzerwanie"
  ]
  node [
    id 57
    label "ci&#261;g&#322;y"
  ]
  node [
    id 58
    label "stale"
  ]
  node [
    id 59
    label "sklep"
  ]
  node [
    id 60
    label "opinion"
  ]
  node [
    id 61
    label "wypowied&#378;"
  ]
  node [
    id 62
    label "zmatowienie"
  ]
  node [
    id 63
    label "wpa&#347;&#263;"
  ]
  node [
    id 64
    label "grupa"
  ]
  node [
    id 65
    label "wokal"
  ]
  node [
    id 66
    label "note"
  ]
  node [
    id 67
    label "wydawa&#263;"
  ]
  node [
    id 68
    label "nakaz"
  ]
  node [
    id 69
    label "regestr"
  ]
  node [
    id 70
    label "&#347;piewak_operowy"
  ]
  node [
    id 71
    label "matowie&#263;"
  ]
  node [
    id 72
    label "wpada&#263;"
  ]
  node [
    id 73
    label "stanowisko"
  ]
  node [
    id 74
    label "zjawisko"
  ]
  node [
    id 75
    label "mutacja"
  ]
  node [
    id 76
    label "partia"
  ]
  node [
    id 77
    label "&#347;piewak"
  ]
  node [
    id 78
    label "emisja"
  ]
  node [
    id 79
    label "brzmienie"
  ]
  node [
    id 80
    label "zmatowie&#263;"
  ]
  node [
    id 81
    label "wydanie"
  ]
  node [
    id 82
    label "zesp&#243;&#322;"
  ]
  node [
    id 83
    label "wyda&#263;"
  ]
  node [
    id 84
    label "zdolno&#347;&#263;"
  ]
  node [
    id 85
    label "decyzja"
  ]
  node [
    id 86
    label "wpadni&#281;cie"
  ]
  node [
    id 87
    label "linia_melodyczna"
  ]
  node [
    id 88
    label "wpadanie"
  ]
  node [
    id 89
    label "onomatopeja"
  ]
  node [
    id 90
    label "sound"
  ]
  node [
    id 91
    label "matowienie"
  ]
  node [
    id 92
    label "ch&#243;rzysta"
  ]
  node [
    id 93
    label "d&#378;wi&#281;k"
  ]
  node [
    id 94
    label "foniatra"
  ]
  node [
    id 95
    label "&#347;piewaczka"
  ]
  node [
    id 96
    label "emocja"
  ]
  node [
    id 97
    label "nastr&#243;j"
  ]
  node [
    id 98
    label "pleasure"
  ]
  node [
    id 99
    label "oratorianin"
  ]
  node [
    id 100
    label "wype&#322;ni&#263;"
  ]
  node [
    id 101
    label "manipulate"
  ]
  node [
    id 102
    label "przesadzi&#263;"
  ]
  node [
    id 103
    label "spowodowa&#263;"
  ]
  node [
    id 104
    label "kompleks"
  ]
  node [
    id 105
    label "sfera_afektywna"
  ]
  node [
    id 106
    label "sumienie"
  ]
  node [
    id 107
    label "odwaga"
  ]
  node [
    id 108
    label "pupa"
  ]
  node [
    id 109
    label "sztuka"
  ]
  node [
    id 110
    label "core"
  ]
  node [
    id 111
    label "piek&#322;o"
  ]
  node [
    id 112
    label "pi&#243;ro"
  ]
  node [
    id 113
    label "shape"
  ]
  node [
    id 114
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 115
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 116
    label "charakter"
  ]
  node [
    id 117
    label "mind"
  ]
  node [
    id 118
    label "marrow"
  ]
  node [
    id 119
    label "mikrokosmos"
  ]
  node [
    id 120
    label "byt"
  ]
  node [
    id 121
    label "przestrze&#324;"
  ]
  node [
    id 122
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 123
    label "mi&#281;kisz"
  ]
  node [
    id 124
    label "ego"
  ]
  node [
    id 125
    label "motor"
  ]
  node [
    id 126
    label "osobowo&#347;&#263;"
  ]
  node [
    id 127
    label "rdze&#324;"
  ]
  node [
    id 128
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 129
    label "sztabka"
  ]
  node [
    id 130
    label "lina"
  ]
  node [
    id 131
    label "deformowa&#263;"
  ]
  node [
    id 132
    label "schody"
  ]
  node [
    id 133
    label "seksualno&#347;&#263;"
  ]
  node [
    id 134
    label "deformowanie"
  ]
  node [
    id 135
    label "&#380;elazko"
  ]
  node [
    id 136
    label "instrument_smyczkowy"
  ]
  node [
    id 137
    label "klocek"
  ]
  node [
    id 138
    label "wyzna&#263;"
  ]
  node [
    id 139
    label "pu&#347;ci&#263;_farb&#281;"
  ]
  node [
    id 140
    label "braid"
  ]
  node [
    id 141
    label "model"
  ]
  node [
    id 142
    label "zbi&#243;r"
  ]
  node [
    id 143
    label "tryb"
  ]
  node [
    id 144
    label "narz&#281;dzie"
  ]
  node [
    id 145
    label "nature"
  ]
  node [
    id 146
    label "ease"
  ]
  node [
    id 147
    label "help"
  ]
  node [
    id 148
    label "ponie&#347;&#263;"
  ]
  node [
    id 149
    label "pom&#243;c"
  ]
  node [
    id 150
    label "zmieniony"
  ]
  node [
    id 151
    label "wysoki"
  ]
  node [
    id 152
    label "pe&#322;ny"
  ]
  node [
    id 153
    label "zareagowanie"
  ]
  node [
    id 154
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 155
    label "opanowanie"
  ]
  node [
    id 156
    label "d&#322;awi&#263;"
  ]
  node [
    id 157
    label "os&#322;upienie"
  ]
  node [
    id 158
    label "zmys&#322;"
  ]
  node [
    id 159
    label "zaanga&#380;owanie"
  ]
  node [
    id 160
    label "smell"
  ]
  node [
    id 161
    label "zdarzenie_si&#281;"
  ]
  node [
    id 162
    label "ostygn&#261;&#263;"
  ]
  node [
    id 163
    label "afekt"
  ]
  node [
    id 164
    label "stan"
  ]
  node [
    id 165
    label "iskrzy&#263;"
  ]
  node [
    id 166
    label "afekcja"
  ]
  node [
    id 167
    label "przeczulica"
  ]
  node [
    id 168
    label "ow&#322;adni&#281;ty"
  ]
  node [
    id 169
    label "czucie"
  ]
  node [
    id 170
    label "doznanie"
  ]
  node [
    id 171
    label "ogrom"
  ]
  node [
    id 172
    label "stygn&#261;&#263;"
  ]
  node [
    id 173
    label "poczucie"
  ]
  node [
    id 174
    label "temperatura"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 11
    target 98
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 13
    target 101
  ]
  edge [
    source 13
    target 102
  ]
  edge [
    source 13
    target 103
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 48
  ]
  edge [
    source 15
    target 104
  ]
  edge [
    source 15
    target 105
  ]
  edge [
    source 15
    target 106
  ]
  edge [
    source 15
    target 107
  ]
  edge [
    source 15
    target 108
  ]
  edge [
    source 15
    target 109
  ]
  edge [
    source 15
    target 110
  ]
  edge [
    source 15
    target 111
  ]
  edge [
    source 15
    target 112
  ]
  edge [
    source 15
    target 113
  ]
  edge [
    source 15
    target 114
  ]
  edge [
    source 15
    target 115
  ]
  edge [
    source 15
    target 116
  ]
  edge [
    source 15
    target 117
  ]
  edge [
    source 15
    target 118
  ]
  edge [
    source 15
    target 119
  ]
  edge [
    source 15
    target 120
  ]
  edge [
    source 15
    target 121
  ]
  edge [
    source 15
    target 122
  ]
  edge [
    source 15
    target 123
  ]
  edge [
    source 15
    target 124
  ]
  edge [
    source 15
    target 125
  ]
  edge [
    source 15
    target 126
  ]
  edge [
    source 15
    target 127
  ]
  edge [
    source 15
    target 128
  ]
  edge [
    source 15
    target 129
  ]
  edge [
    source 15
    target 130
  ]
  edge [
    source 15
    target 131
  ]
  edge [
    source 15
    target 132
  ]
  edge [
    source 15
    target 133
  ]
  edge [
    source 15
    target 134
  ]
  edge [
    source 15
    target 135
  ]
  edge [
    source 15
    target 136
  ]
  edge [
    source 15
    target 137
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 138
  ]
  edge [
    source 18
    target 139
  ]
  edge [
    source 18
    target 83
  ]
  edge [
    source 18
    target 140
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 141
  ]
  edge [
    source 20
    target 142
  ]
  edge [
    source 20
    target 143
  ]
  edge [
    source 20
    target 144
  ]
  edge [
    source 20
    target 145
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 146
  ]
  edge [
    source 21
    target 147
  ]
  edge [
    source 21
    target 148
  ]
  edge [
    source 21
    target 149
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 150
  ]
  edge [
    source 23
    target 151
  ]
  edge [
    source 23
    target 152
  ]
  edge [
    source 24
    target 153
  ]
  edge [
    source 24
    target 63
  ]
  edge [
    source 24
    target 154
  ]
  edge [
    source 24
    target 155
  ]
  edge [
    source 24
    target 156
  ]
  edge [
    source 24
    target 72
  ]
  edge [
    source 24
    target 157
  ]
  edge [
    source 24
    target 158
  ]
  edge [
    source 24
    target 159
  ]
  edge [
    source 24
    target 160
  ]
  edge [
    source 24
    target 161
  ]
  edge [
    source 24
    target 162
  ]
  edge [
    source 24
    target 163
  ]
  edge [
    source 24
    target 164
  ]
  edge [
    source 24
    target 165
  ]
  edge [
    source 24
    target 166
  ]
  edge [
    source 24
    target 167
  ]
  edge [
    source 24
    target 168
  ]
  edge [
    source 24
    target 169
  ]
  edge [
    source 24
    target 170
  ]
  edge [
    source 24
    target 96
  ]
  edge [
    source 24
    target 171
  ]
  edge [
    source 24
    target 172
  ]
  edge [
    source 24
    target 173
  ]
  edge [
    source 24
    target 174
  ]
]
