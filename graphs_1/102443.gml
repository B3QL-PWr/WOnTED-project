graph [
  maxDegree 18
  minDegree 1
  meanDegree 2.07741935483871
  density 0.013489736070381233
  graphCliqueNumber 2
  node [
    id 0
    label "internet"
    origin "text"
  ]
  node [
    id 1
    label "czu&#263;"
    origin "text"
  ]
  node [
    id 2
    label "reperkusja"
    origin "text"
  ]
  node [
    id 3
    label "obecna"
    origin "text"
  ]
  node [
    id 4
    label "awantura"
    origin "text"
  ]
  node [
    id 5
    label "polityczny"
    origin "text"
  ]
  node [
    id 6
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 7
    label "sam"
    origin "text"
  ]
  node [
    id 8
    label "siebie"
    origin "text"
  ]
  node [
    id 9
    label "nasa"
    origin "text"
  ]
  node [
    id 10
    label "tym"
    origin "text"
  ]
  node [
    id 11
    label "blog"
    origin "text"
  ]
  node [
    id 12
    label "interesowa&#263;"
    origin "text"
  ]
  node [
    id 13
    label "wies&#322;aw"
    origin "text"
  ]
  node [
    id 14
    label "sadurski"
    origin "text"
  ]
  node [
    id 15
    label "zarzuci&#263;"
    origin "text"
  ]
  node [
    id 16
    label "pozostawa&#263;"
    origin "text"
  ]
  node [
    id 17
    label "lata"
    origin "text"
  ]
  node [
    id 18
    label "anonimowy"
    origin "text"
  ]
  node [
    id 19
    label "kataryna"
    origin "text"
  ]
  node [
    id 20
    label "przed"
    origin "text"
  ]
  node [
    id 21
    label "transfer"
    origin "text"
  ]
  node [
    id 22
    label "ocenia&#263;"
    origin "text"
  ]
  node [
    id 23
    label "inny"
    origin "text"
  ]
  node [
    id 24
    label "pod"
    origin "text"
  ]
  node [
    id 25
    label "pseudonim"
    origin "text"
  ]
  node [
    id 26
    label "by&#263;"
    origin "text"
  ]
  node [
    id 27
    label "tch&#243;rzliwy"
    origin "text"
  ]
  node [
    id 28
    label "odpowied&#378;"
    origin "text"
  ]
  node [
    id 29
    label "odej&#347;&#263;"
    origin "text"
  ]
  node [
    id 30
    label "salon"
    origin "text"
  ]
  node [
    id 31
    label "us&#322;uga_internetowa"
  ]
  node [
    id 32
    label "biznes_elektroniczny"
  ]
  node [
    id 33
    label "punkt_dost&#281;pu"
  ]
  node [
    id 34
    label "hipertekst"
  ]
  node [
    id 35
    label "gra_sieciowa"
  ]
  node [
    id 36
    label "mem"
  ]
  node [
    id 37
    label "e-hazard"
  ]
  node [
    id 38
    label "sie&#263;_komputerowa"
  ]
  node [
    id 39
    label "media"
  ]
  node [
    id 40
    label "podcast"
  ]
  node [
    id 41
    label "netbook"
  ]
  node [
    id 42
    label "provider"
  ]
  node [
    id 43
    label "cyberprzestrze&#324;"
  ]
  node [
    id 44
    label "grooming"
  ]
  node [
    id 45
    label "strona"
  ]
  node [
    id 46
    label "uczuwa&#263;"
  ]
  node [
    id 47
    label "przewidywa&#263;"
  ]
  node [
    id 48
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 49
    label "smell"
  ]
  node [
    id 50
    label "postrzega&#263;"
  ]
  node [
    id 51
    label "doznawa&#263;"
  ]
  node [
    id 52
    label "spirit"
  ]
  node [
    id 53
    label "anticipate"
  ]
  node [
    id 54
    label "konsekwencja"
  ]
  node [
    id 55
    label "recoil"
  ]
  node [
    id 56
    label "zaj&#347;cie"
  ]
  node [
    id 57
    label "cyrk"
  ]
  node [
    id 58
    label "argument"
  ]
  node [
    id 59
    label "scene"
  ]
  node [
    id 60
    label "sensacja"
  ]
  node [
    id 61
    label "smr&#243;d"
  ]
  node [
    id 62
    label "konflikt"
  ]
  node [
    id 63
    label "przygoda"
  ]
  node [
    id 64
    label "internowanie"
  ]
  node [
    id 65
    label "prorz&#261;dowy"
  ]
  node [
    id 66
    label "wi&#281;zie&#324;"
  ]
  node [
    id 67
    label "politycznie"
  ]
  node [
    id 68
    label "internowa&#263;"
  ]
  node [
    id 69
    label "ideologiczny"
  ]
  node [
    id 70
    label "sklep"
  ]
  node [
    id 71
    label "komcio"
  ]
  node [
    id 72
    label "blogosfera"
  ]
  node [
    id 73
    label "pami&#281;tnik"
  ]
  node [
    id 74
    label "rozciekawia&#263;"
  ]
  node [
    id 75
    label "sake"
  ]
  node [
    id 76
    label "zakomunikowa&#263;"
  ]
  node [
    id 77
    label "przeznaczy&#263;"
  ]
  node [
    id 78
    label "disapprove"
  ]
  node [
    id 79
    label "throw"
  ]
  node [
    id 80
    label "umie&#347;ci&#263;"
  ]
  node [
    id 81
    label "project"
  ]
  node [
    id 82
    label "ostawa&#263;_si&#281;"
  ]
  node [
    id 83
    label "stop"
  ]
  node [
    id 84
    label "utrzymywa&#263;_si&#281;"
  ]
  node [
    id 85
    label "przebywa&#263;"
  ]
  node [
    id 86
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 87
    label "support"
  ]
  node [
    id 88
    label "blend"
  ]
  node [
    id 89
    label "zachowywa&#263;_si&#281;"
  ]
  node [
    id 90
    label "prze&#380;ywa&#263;"
  ]
  node [
    id 91
    label "summer"
  ]
  node [
    id 92
    label "czas"
  ]
  node [
    id 93
    label "bezimienny"
  ]
  node [
    id 94
    label "bezimiennie"
  ]
  node [
    id 95
    label "anonimowo"
  ]
  node [
    id 96
    label "zamiana"
  ]
  node [
    id 97
    label "przekaz"
  ]
  node [
    id 98
    label "ilo&#347;&#263;"
  ]
  node [
    id 99
    label "release"
  ]
  node [
    id 100
    label "lista_transferowa"
  ]
  node [
    id 101
    label "award"
  ]
  node [
    id 102
    label "gauge"
  ]
  node [
    id 103
    label "s&#261;dzi&#263;"
  ]
  node [
    id 104
    label "strike"
  ]
  node [
    id 105
    label "wystawia&#263;"
  ]
  node [
    id 106
    label "okre&#347;la&#263;"
  ]
  node [
    id 107
    label "znajdowa&#263;"
  ]
  node [
    id 108
    label "kolejny"
  ]
  node [
    id 109
    label "inaczej"
  ]
  node [
    id 110
    label "r&#243;&#380;ny"
  ]
  node [
    id 111
    label "inszy"
  ]
  node [
    id 112
    label "osobno"
  ]
  node [
    id 113
    label "nazwa_w&#322;asna"
  ]
  node [
    id 114
    label "si&#281;ga&#263;"
  ]
  node [
    id 115
    label "trwa&#263;"
  ]
  node [
    id 116
    label "obecno&#347;&#263;"
  ]
  node [
    id 117
    label "stan"
  ]
  node [
    id 118
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 119
    label "stand"
  ]
  node [
    id 120
    label "mie&#263;_miejsce"
  ]
  node [
    id 121
    label "uczestniczy&#263;"
  ]
  node [
    id 122
    label "chodzi&#263;"
  ]
  node [
    id 123
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 124
    label "equal"
  ]
  node [
    id 125
    label "tch&#243;rzowsko"
  ]
  node [
    id 126
    label "strachliwy"
  ]
  node [
    id 127
    label "dokument"
  ]
  node [
    id 128
    label "rozmowa"
  ]
  node [
    id 129
    label "reakcja"
  ]
  node [
    id 130
    label "wyj&#347;cie"
  ]
  node [
    id 131
    label "react"
  ]
  node [
    id 132
    label "respondent"
  ]
  node [
    id 133
    label "replica"
  ]
  node [
    id 134
    label "opu&#347;ci&#263;"
  ]
  node [
    id 135
    label "zrezygnowa&#263;"
  ]
  node [
    id 136
    label "proceed"
  ]
  node [
    id 137
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 138
    label "leave_office"
  ]
  node [
    id 139
    label "retract"
  ]
  node [
    id 140
    label "min&#261;&#263;"
  ]
  node [
    id 141
    label "przesta&#263;"
  ]
  node [
    id 142
    label "odrzut"
  ]
  node [
    id 143
    label "ruszy&#263;"
  ]
  node [
    id 144
    label "drop"
  ]
  node [
    id 145
    label "zrobi&#263;"
  ]
  node [
    id 146
    label "die"
  ]
  node [
    id 147
    label "oddzieli&#263;_si&#281;"
  ]
  node [
    id 148
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 149
    label "pok&#243;j"
  ]
  node [
    id 150
    label "kanapa"
  ]
  node [
    id 151
    label "zak&#322;ad"
  ]
  node [
    id 152
    label "przyj&#281;cie"
  ]
  node [
    id 153
    label "telewizor"
  ]
  node [
    id 154
    label "komplet_wypoczynkowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 19
  ]
  edge [
    source 11
    target 20
  ]
  edge [
    source 11
    target 71
  ]
  edge [
    source 11
    target 45
  ]
  edge [
    source 11
    target 72
  ]
  edge [
    source 11
    target 73
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 74
  ]
  edge [
    source 12
    target 75
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 76
  ]
  edge [
    source 15
    target 77
  ]
  edge [
    source 15
    target 78
  ]
  edge [
    source 15
    target 79
  ]
  edge [
    source 15
    target 80
  ]
  edge [
    source 15
    target 29
  ]
  edge [
    source 15
    target 81
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 82
  ]
  edge [
    source 16
    target 83
  ]
  edge [
    source 16
    target 84
  ]
  edge [
    source 16
    target 85
  ]
  edge [
    source 16
    target 86
  ]
  edge [
    source 16
    target 87
  ]
  edge [
    source 16
    target 88
  ]
  edge [
    source 16
    target 89
  ]
  edge [
    source 16
    target 90
  ]
  edge [
    source 16
    target 19
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 91
  ]
  edge [
    source 17
    target 92
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 93
  ]
  edge [
    source 18
    target 94
  ]
  edge [
    source 18
    target 95
  ]
  edge [
    source 19
    target 28
  ]
  edge [
    source 19
    target 29
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 96
  ]
  edge [
    source 21
    target 97
  ]
  edge [
    source 21
    target 98
  ]
  edge [
    source 21
    target 99
  ]
  edge [
    source 21
    target 100
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 101
  ]
  edge [
    source 22
    target 102
  ]
  edge [
    source 22
    target 103
  ]
  edge [
    source 22
    target 104
  ]
  edge [
    source 22
    target 105
  ]
  edge [
    source 22
    target 106
  ]
  edge [
    source 22
    target 107
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 108
  ]
  edge [
    source 23
    target 109
  ]
  edge [
    source 23
    target 110
  ]
  edge [
    source 23
    target 111
  ]
  edge [
    source 23
    target 112
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 113
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 114
  ]
  edge [
    source 26
    target 115
  ]
  edge [
    source 26
    target 116
  ]
  edge [
    source 26
    target 117
  ]
  edge [
    source 26
    target 118
  ]
  edge [
    source 26
    target 119
  ]
  edge [
    source 26
    target 120
  ]
  edge [
    source 26
    target 121
  ]
  edge [
    source 26
    target 122
  ]
  edge [
    source 26
    target 123
  ]
  edge [
    source 26
    target 124
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 125
  ]
  edge [
    source 27
    target 126
  ]
  edge [
    source 28
    target 127
  ]
  edge [
    source 28
    target 128
  ]
  edge [
    source 28
    target 129
  ]
  edge [
    source 28
    target 130
  ]
  edge [
    source 28
    target 131
  ]
  edge [
    source 28
    target 132
  ]
  edge [
    source 28
    target 133
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 134
  ]
  edge [
    source 29
    target 135
  ]
  edge [
    source 29
    target 136
  ]
  edge [
    source 29
    target 137
  ]
  edge [
    source 29
    target 138
  ]
  edge [
    source 29
    target 139
  ]
  edge [
    source 29
    target 140
  ]
  edge [
    source 29
    target 141
  ]
  edge [
    source 29
    target 142
  ]
  edge [
    source 29
    target 143
  ]
  edge [
    source 29
    target 144
  ]
  edge [
    source 29
    target 145
  ]
  edge [
    source 29
    target 146
  ]
  edge [
    source 29
    target 147
  ]
  edge [
    source 29
    target 148
  ]
  edge [
    source 30
    target 149
  ]
  edge [
    source 30
    target 150
  ]
  edge [
    source 30
    target 70
  ]
  edge [
    source 30
    target 151
  ]
  edge [
    source 30
    target 152
  ]
  edge [
    source 30
    target 153
  ]
  edge [
    source 30
    target 154
  ]
]
