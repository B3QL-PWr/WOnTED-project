graph [
  maxDegree 15
  minDegree 1
  meanDegree 1.9565217391304348
  density 0.043478260869565216
  graphCliqueNumber 2
  node [
    id 0
    label "dla"
    origin "text"
  ]
  node [
    id 1
    label "benzo"
    origin "text"
  ]
  node [
    id 2
    label "pirenu"
    origin "text"
  ]
  node [
    id 3
    label "okre&#347;lony"
    origin "text"
  ]
  node [
    id 4
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 5
    label "st&#281;&#380;enie"
    origin "text"
  ]
  node [
    id 6
    label "dopuszczalny"
    origin "text"
  ]
  node [
    id 7
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 8
    label "wynosi&#263;"
    origin "text"
  ]
  node [
    id 9
    label "wiadomy"
  ]
  node [
    id 10
    label "proceed"
  ]
  node [
    id 11
    label "catch"
  ]
  node [
    id 12
    label "pozosta&#263;"
  ]
  node [
    id 13
    label "osta&#263;_si&#281;"
  ]
  node [
    id 14
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 15
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 16
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 17
    label "change"
  ]
  node [
    id 18
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 19
    label "concentration"
  ]
  node [
    id 20
    label "zg&#281;stnienie"
  ]
  node [
    id 21
    label "opanowanie"
  ]
  node [
    id 22
    label "nasycenie"
  ]
  node [
    id 23
    label "przybranie_na_sile"
  ]
  node [
    id 24
    label "wezbranie"
  ]
  node [
    id 25
    label "immunity"
  ]
  node [
    id 26
    label "&#347;ci&#281;cie"
  ]
  node [
    id 27
    label "znieruchomienie"
  ]
  node [
    id 28
    label "stanie_si&#281;"
  ]
  node [
    id 29
    label "izotonia"
  ]
  node [
    id 30
    label "stwardnienie"
  ]
  node [
    id 31
    label "nefelometria"
  ]
  node [
    id 32
    label "mo&#380;liwy"
  ]
  node [
    id 33
    label "dopuszczalnie"
  ]
  node [
    id 34
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 35
    label "odsuwa&#263;"
  ]
  node [
    id 36
    label "zanosi&#263;"
  ]
  node [
    id 37
    label "otrzymywa&#263;"
  ]
  node [
    id 38
    label "nasi&#261;ka&#263;"
  ]
  node [
    id 39
    label "rozpowszechnia&#263;"
  ]
  node [
    id 40
    label "ujawnia&#263;"
  ]
  node [
    id 41
    label "kra&#347;&#263;"
  ]
  node [
    id 42
    label "kwota"
  ]
  node [
    id 43
    label "liczy&#263;"
  ]
  node [
    id 44
    label "raise"
  ]
  node [
    id 45
    label "podnosi&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 10
  ]
  edge [
    source 4
    target 11
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 34
  ]
  edge [
    source 8
    target 35
  ]
  edge [
    source 8
    target 36
  ]
  edge [
    source 8
    target 37
  ]
  edge [
    source 8
    target 38
  ]
  edge [
    source 8
    target 39
  ]
  edge [
    source 8
    target 40
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 8
    target 45
  ]
]
