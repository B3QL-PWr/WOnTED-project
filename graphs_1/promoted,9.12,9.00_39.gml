graph [
  maxDegree 12
  minDegree 1
  meanDegree 1.945945945945946
  density 0.05405405405405406
  graphCliqueNumber 2
  node [
    id 0
    label "normalny"
    origin "text"
  ]
  node [
    id 1
    label "dzienia"
    origin "text"
  ]
  node [
    id 2
    label "tokijski"
    origin "text"
  ]
  node [
    id 3
    label "uniwersytet"
    origin "text"
  ]
  node [
    id 4
    label "dla"
    origin "text"
  ]
  node [
    id 5
    label "student"
    origin "text"
  ]
  node [
    id 6
    label "polonistyka"
    origin "text"
  ]
  node [
    id 7
    label "prawid&#322;owy"
  ]
  node [
    id 8
    label "zwyczajnie"
  ]
  node [
    id 9
    label "normalnie"
  ]
  node [
    id 10
    label "okre&#347;lony"
  ]
  node [
    id 11
    label "zdr&#243;w"
  ]
  node [
    id 12
    label "oczywisty"
  ]
  node [
    id 13
    label "zwyczajny"
  ]
  node [
    id 14
    label "przeci&#281;tny"
  ]
  node [
    id 15
    label "zwykle"
  ]
  node [
    id 16
    label "cz&#281;sty"
  ]
  node [
    id 17
    label "ku&#378;nia"
  ]
  node [
    id 18
    label "Harvard"
  ]
  node [
    id 19
    label "uczelnia"
  ]
  node [
    id 20
    label "Sorbona"
  ]
  node [
    id 21
    label "Uniwersytet_Oksfordzki"
  ]
  node [
    id 22
    label "Stanford"
  ]
  node [
    id 23
    label "Princeton"
  ]
  node [
    id 24
    label "academy"
  ]
  node [
    id 25
    label "szko&#322;a_wy&#380;sza"
  ]
  node [
    id 26
    label "wydzia&#322;"
  ]
  node [
    id 27
    label "tutor"
  ]
  node [
    id 28
    label "akademik"
  ]
  node [
    id 29
    label "immatrykulowanie"
  ]
  node [
    id 30
    label "s&#322;uchacz"
  ]
  node [
    id 31
    label "immatrykulowa&#263;"
  ]
  node [
    id 32
    label "absolwent"
  ]
  node [
    id 33
    label "indeks"
  ]
  node [
    id 34
    label "jednostka_organizacyjna"
  ]
  node [
    id 35
    label "filologia"
  ]
  node [
    id 36
    label "slawistyka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
]
