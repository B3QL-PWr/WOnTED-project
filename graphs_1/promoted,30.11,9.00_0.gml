graph [
  maxDegree 23
  minDegree 1
  meanDegree 2
  density 0.02531645569620253
  graphCliqueNumber 2
  node [
    id 0
    label "ponad"
    origin "text"
  ]
  node [
    id 1
    label "p&#243;&#322;tora"
    origin "text"
  ]
  node [
    id 2
    label "rok"
    origin "text"
  ]
  node [
    id 3
    label "potrzebowa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "alstom"
    origin "text"
  ]
  node [
    id 5
    label "pkp"
    origin "text"
  ]
  node [
    id 6
    label "intercity"
    origin "text"
  ]
  node [
    id 7
    label "&#380;eby"
    origin "text"
  ]
  node [
    id 8
    label "porozumie&#263;"
    origin "text"
  ]
  node [
    id 9
    label "si&#281;"
    origin "text"
  ]
  node [
    id 10
    label "sprawa"
    origin "text"
  ]
  node [
    id 11
    label "naprawa"
    origin "text"
  ]
  node [
    id 12
    label "uszkodzony"
    origin "text"
  ]
  node [
    id 13
    label "wypadek"
    origin "text"
  ]
  node [
    id 14
    label "ozimek"
    origin "text"
  ]
  node [
    id 15
    label "sk&#322;ad"
    origin "text"
  ]
  node [
    id 16
    label "pendolino"
    origin "text"
  ]
  node [
    id 17
    label "stulecie"
  ]
  node [
    id 18
    label "kalendarz"
  ]
  node [
    id 19
    label "czas"
  ]
  node [
    id 20
    label "pora_roku"
  ]
  node [
    id 21
    label "cykl_astronomiczny"
  ]
  node [
    id 22
    label "p&#243;&#322;rocze"
  ]
  node [
    id 23
    label "grupa"
  ]
  node [
    id 24
    label "kwarta&#322;"
  ]
  node [
    id 25
    label "kurs"
  ]
  node [
    id 26
    label "jubileusz"
  ]
  node [
    id 27
    label "miesi&#261;c"
  ]
  node [
    id 28
    label "lata"
  ]
  node [
    id 29
    label "martwy_sezon"
  ]
  node [
    id 30
    label "zapotrzebowa&#263;"
  ]
  node [
    id 31
    label "by&#263;"
  ]
  node [
    id 32
    label "chcie&#263;"
  ]
  node [
    id 33
    label "need"
  ]
  node [
    id 34
    label "claim"
  ]
  node [
    id 35
    label "temat"
  ]
  node [
    id 36
    label "kognicja"
  ]
  node [
    id 37
    label "idea"
  ]
  node [
    id 38
    label "szczeg&#243;&#322;"
  ]
  node [
    id 39
    label "rzecz"
  ]
  node [
    id 40
    label "wydarzenie"
  ]
  node [
    id 41
    label "przes&#322;anka"
  ]
  node [
    id 42
    label "rozprawa"
  ]
  node [
    id 43
    label "object"
  ]
  node [
    id 44
    label "proposition"
  ]
  node [
    id 45
    label "return"
  ]
  node [
    id 46
    label "zado&#347;&#263;uczynienie"
  ]
  node [
    id 47
    label "pogwarancyjny"
  ]
  node [
    id 48
    label "czynno&#347;&#263;"
  ]
  node [
    id 49
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 50
    label "motyw"
  ]
  node [
    id 51
    label "ubezpieczenie_wypadkowe"
  ]
  node [
    id 52
    label "fabu&#322;a"
  ]
  node [
    id 53
    label "przebiec"
  ]
  node [
    id 54
    label "happening"
  ]
  node [
    id 55
    label "przebiegni&#281;cie"
  ]
  node [
    id 56
    label "event"
  ]
  node [
    id 57
    label "charakter"
  ]
  node [
    id 58
    label "pole"
  ]
  node [
    id 59
    label "fabryka"
  ]
  node [
    id 60
    label "blokada"
  ]
  node [
    id 61
    label "pas"
  ]
  node [
    id 62
    label "pomieszczenie"
  ]
  node [
    id 63
    label "set"
  ]
  node [
    id 64
    label "constitution"
  ]
  node [
    id 65
    label "tekst"
  ]
  node [
    id 66
    label "struktura"
  ]
  node [
    id 67
    label "basic"
  ]
  node [
    id 68
    label "rank_and_file"
  ]
  node [
    id 69
    label "tabulacja"
  ]
  node [
    id 70
    label "hurtownia"
  ]
  node [
    id 71
    label "sklep"
  ]
  node [
    id 72
    label "&#347;wiat&#322;o"
  ]
  node [
    id 73
    label "zesp&#243;&#322;"
  ]
  node [
    id 74
    label "syf"
  ]
  node [
    id 75
    label "w&#322;a&#347;ciwo&#347;&#263;_chemiczna"
  ]
  node [
    id 76
    label "obr&#243;bka"
  ]
  node [
    id 77
    label "miejsce"
  ]
  node [
    id 78
    label "sk&#322;adnik"
  ]
  node [
    id 79
    label "PKP"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 35
  ]
  edge [
    source 10
    target 36
  ]
  edge [
    source 10
    target 37
  ]
  edge [
    source 10
    target 38
  ]
  edge [
    source 10
    target 39
  ]
  edge [
    source 10
    target 40
  ]
  edge [
    source 10
    target 41
  ]
  edge [
    source 10
    target 42
  ]
  edge [
    source 10
    target 43
  ]
  edge [
    source 10
    target 44
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 45
  ]
  edge [
    source 11
    target 46
  ]
  edge [
    source 11
    target 47
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 48
  ]
  edge [
    source 13
    target 49
  ]
  edge [
    source 13
    target 50
  ]
  edge [
    source 13
    target 51
  ]
  edge [
    source 13
    target 52
  ]
  edge [
    source 13
    target 53
  ]
  edge [
    source 13
    target 40
  ]
  edge [
    source 13
    target 54
  ]
  edge [
    source 13
    target 55
  ]
  edge [
    source 13
    target 56
  ]
  edge [
    source 13
    target 57
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 58
  ]
  edge [
    source 15
    target 59
  ]
  edge [
    source 15
    target 60
  ]
  edge [
    source 15
    target 61
  ]
  edge [
    source 15
    target 62
  ]
  edge [
    source 15
    target 63
  ]
  edge [
    source 15
    target 64
  ]
  edge [
    source 15
    target 65
  ]
  edge [
    source 15
    target 66
  ]
  edge [
    source 15
    target 67
  ]
  edge [
    source 15
    target 68
  ]
  edge [
    source 15
    target 69
  ]
  edge [
    source 15
    target 70
  ]
  edge [
    source 15
    target 71
  ]
  edge [
    source 15
    target 72
  ]
  edge [
    source 15
    target 73
  ]
  edge [
    source 15
    target 74
  ]
  edge [
    source 15
    target 75
  ]
  edge [
    source 15
    target 76
  ]
  edge [
    source 15
    target 77
  ]
  edge [
    source 15
    target 78
  ]
]
