graph [
  maxDegree 7
  minDegree 1
  meanDegree 2
  density 0.07692307692307693
  graphCliqueNumber 4
  node [
    id 0
    label "trafi&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "co&#347;"
    origin "text"
  ]
  node [
    id 2
    label "taki"
    origin "text"
  ]
  node [
    id 3
    label "jak"
    origin "text"
  ]
  node [
    id 4
    label "blog"
    origin "text"
  ]
  node [
    id 5
    label "b&#243;r"
    origin "text"
  ]
  node [
    id 6
    label "thing"
  ]
  node [
    id 7
    label "cosik"
  ]
  node [
    id 8
    label "okre&#347;lony"
  ]
  node [
    id 9
    label "jaki&#347;"
  ]
  node [
    id 10
    label "byd&#322;o"
  ]
  node [
    id 11
    label "zobo"
  ]
  node [
    id 12
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 13
    label "yakalo"
  ]
  node [
    id 14
    label "dzo"
  ]
  node [
    id 15
    label "komcio"
  ]
  node [
    id 16
    label "strona"
  ]
  node [
    id 17
    label "blogosfera"
  ]
  node [
    id 18
    label "pami&#281;tnik"
  ]
  node [
    id 19
    label "las"
  ]
  node [
    id 20
    label "przedborze"
  ]
  node [
    id 21
    label "boliblogpl"
  ]
  node [
    id 22
    label "B&#243;r"
  ]
  node [
    id 23
    label "Haha"
  ]
  node [
    id 24
    label "RY"
  ]
  node [
    id 25
    label "u"
  ]
  node [
    id 26
    label "CHY"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 25
  ]
  edge [
    source 23
    target 26
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 26
  ]
  edge [
    source 25
    target 26
  ]
]
