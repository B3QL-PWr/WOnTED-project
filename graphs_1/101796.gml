graph [
  maxDegree 11
  minDegree 1
  meanDegree 2.45
  density 0.06282051282051282
  graphCliqueNumber 7
  node [
    id 0
    label "hiszpania"
    origin "text"
  ]
  node [
    id 1
    label "policja"
    origin "text"
  ]
  node [
    id 2
    label "aresztowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "doradca"
    origin "text"
  ]
  node [
    id 4
    label "anta"
    origin "text"
  ]
  node [
    id 5
    label "kredytowy"
    origin "text"
  ]
  node [
    id 6
    label "komisariat"
  ]
  node [
    id 7
    label "psiarnia"
  ]
  node [
    id 8
    label "posterunek"
  ]
  node [
    id 9
    label "grupa"
  ]
  node [
    id 10
    label "organ"
  ]
  node [
    id 11
    label "s&#322;u&#380;ba"
  ]
  node [
    id 12
    label "zajmowa&#263;"
  ]
  node [
    id 13
    label "zaj&#261;&#263;"
  ]
  node [
    id 14
    label "spowodowa&#263;"
  ]
  node [
    id 15
    label "z&#322;apa&#263;"
  ]
  node [
    id 16
    label "stop"
  ]
  node [
    id 17
    label "seize"
  ]
  node [
    id 18
    label "robi&#263;"
  ]
  node [
    id 19
    label "&#322;apa&#263;"
  ]
  node [
    id 20
    label "powodowa&#263;"
  ]
  node [
    id 21
    label "czynnik"
  ]
  node [
    id 22
    label "pomocnik"
  ]
  node [
    id 23
    label "radziciel"
  ]
  node [
    id 24
    label "pilaster"
  ]
  node [
    id 25
    label "Enrica"
  ]
  node [
    id 26
    label "Durana"
  ]
  node [
    id 27
    label "kryzys"
  ]
  node [
    id 28
    label "darmowy"
  ]
  node [
    id 29
    label "pismo"
  ]
  node [
    id 30
    label "aby"
  ]
  node [
    id 31
    label "prze&#380;y&#263;"
  ]
  node [
    id 32
    label "ekonomiczny"
  ]
  node [
    id 33
    label "turbulencja"
  ]
  node [
    id 34
    label "Robin"
  ]
  node [
    id 35
    label "Hood"
  ]
  node [
    id 36
    label "Ameryka"
  ]
  node [
    id 37
    label "po&#322;udniowy"
  ]
  node [
    id 38
    label "da&#263;"
  ]
  node [
    id 39
    label "rada"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 29
  ]
  edge [
    source 27
    target 30
  ]
  edge [
    source 27
    target 31
  ]
  edge [
    source 27
    target 32
  ]
  edge [
    source 27
    target 33
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 30
  ]
  edge [
    source 28
    target 31
  ]
  edge [
    source 28
    target 32
  ]
  edge [
    source 28
    target 33
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 31
  ]
  edge [
    source 29
    target 32
  ]
  edge [
    source 29
    target 33
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 32
  ]
  edge [
    source 30
    target 33
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 33
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 38
    target 39
  ]
]
