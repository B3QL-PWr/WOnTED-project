graph [
  maxDegree 14
  minDegree 1
  meanDegree 1.9487179487179487
  density 0.05128205128205128
  graphCliqueNumber 2
  node [
    id 0
    label "wybra&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "polska"
    origin "text"
  ]
  node [
    id 3
    label "festiwal"
    origin "text"
  ]
  node [
    id 4
    label "melbourne"
    origin "text"
  ]
  node [
    id 5
    label "australia"
    origin "text"
  ]
  node [
    id 6
    label "nie"
    origin "text"
  ]
  node [
    id 7
    label "siebie"
    origin "text"
  ]
  node [
    id 8
    label "swoje"
    origin "text"
  ]
  node [
    id 9
    label "tajski"
    origin "text"
  ]
  node [
    id 10
    label "przyjaciel"
    origin "text"
  ]
  node [
    id 11
    label "Nowe_Horyzonty"
  ]
  node [
    id 12
    label "Interwizja"
  ]
  node [
    id 13
    label "Open'er"
  ]
  node [
    id 14
    label "Przystanek_Woodstock"
  ]
  node [
    id 15
    label "impreza"
  ]
  node [
    id 16
    label "Woodstock"
  ]
  node [
    id 17
    label "Metalmania"
  ]
  node [
    id 18
    label "Opole"
  ]
  node [
    id 19
    label "FAMA"
  ]
  node [
    id 20
    label "Eurowizja"
  ]
  node [
    id 21
    label "Brutal"
  ]
  node [
    id 22
    label "sprzeciw"
  ]
  node [
    id 23
    label "po_tajsku"
  ]
  node [
    id 24
    label "syjamski"
  ]
  node [
    id 25
    label "azjatycki"
  ]
  node [
    id 26
    label "etnolekt"
  ]
  node [
    id 27
    label "dalekowschodni"
  ]
  node [
    id 28
    label "kochanek"
  ]
  node [
    id 29
    label "przyja&#378;nienie_si&#281;"
  ]
  node [
    id 30
    label "zaprzyja&#378;nianie_si&#281;"
  ]
  node [
    id 31
    label "kum"
  ]
  node [
    id 32
    label "sympatyk"
  ]
  node [
    id 33
    label "bratnia_dusza"
  ]
  node [
    id 34
    label "amikus"
  ]
  node [
    id 35
    label "zaprzyja&#378;nienie_si&#281;"
  ]
  node [
    id 36
    label "pobratymiec"
  ]
  node [
    id 37
    label "drogi"
  ]
  node [
    id 38
    label "Polska"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 22
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 23
  ]
  edge [
    source 9
    target 24
  ]
  edge [
    source 9
    target 25
  ]
  edge [
    source 9
    target 26
  ]
  edge [
    source 9
    target 27
  ]
  edge [
    source 10
    target 28
  ]
  edge [
    source 10
    target 29
  ]
  edge [
    source 10
    target 30
  ]
  edge [
    source 10
    target 31
  ]
  edge [
    source 10
    target 32
  ]
  edge [
    source 10
    target 33
  ]
  edge [
    source 10
    target 34
  ]
  edge [
    source 10
    target 35
  ]
  edge [
    source 10
    target 36
  ]
  edge [
    source 10
    target 37
  ]
]
