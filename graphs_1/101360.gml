graph [
  maxDegree 9
  minDegree 1
  meanDegree 2.4545454545454546
  density 0.03776223776223776
  graphCliqueNumber 6
  node [
    id 0
    label "andrzej"
    origin "text"
  ]
  node [
    id 1
    label "ostoja"
    origin "text"
  ]
  node [
    id 2
    label "owsiany"
    origin "text"
  ]
  node [
    id 3
    label "rama"
  ]
  node [
    id 4
    label "miejsce"
  ]
  node [
    id 5
    label "pojazd_szynowy"
  ]
  node [
    id 6
    label "podstawa"
  ]
  node [
    id 7
    label "siedlisko"
  ]
  node [
    id 8
    label "podpora"
  ]
  node [
    id 9
    label "zbo&#380;owy"
  ]
  node [
    id 10
    label "Andrzej"
  ]
  node [
    id 11
    label "armia"
  ]
  node [
    id 12
    label "krajowy"
  ]
  node [
    id 13
    label "konspiracyjny"
  ]
  node [
    id 14
    label "wojsko"
  ]
  node [
    id 15
    label "polskie"
  ]
  node [
    id 16
    label "Stanis&#322;awa"
  ]
  node [
    id 17
    label "Sojczy&#324;skiego"
  ]
  node [
    id 18
    label "wydzia&#322;"
  ]
  node [
    id 19
    label "prawy"
  ]
  node [
    id 20
    label "uniwersytet"
  ]
  node [
    id 21
    label "&#322;&#243;dzki"
  ]
  node [
    id 22
    label "zwi&#261;zek"
  ]
  node [
    id 23
    label "m&#322;odzi"
  ]
  node [
    id 24
    label "demokrata"
  ]
  node [
    id 25
    label "NSZZ"
  ]
  node [
    id 26
    label "solidarno&#347;&#263;"
  ]
  node [
    id 27
    label "rucho"
  ]
  node [
    id 28
    label "obrona"
  ]
  node [
    id 29
    label "cz&#322;owiek"
  ]
  node [
    id 30
    label "i"
  ]
  node [
    id 31
    label "obywatel"
  ]
  node [
    id 32
    label "rada"
  ]
  node [
    id 33
    label "ZINO"
  ]
  node [
    id 34
    label "polityczny"
  ]
  node [
    id 35
    label "konfederacja"
  ]
  node [
    id 36
    label "polski"
  ]
  node [
    id 37
    label "niepodleg&#322;y"
  ]
  node [
    id 38
    label "spo&#322;eczny"
  ]
  node [
    id 39
    label "komitet"
  ]
  node [
    id 40
    label "pami&#281;&#263;"
  ]
  node [
    id 41
    label "marsza&#322;ek"
  ]
  node [
    id 42
    label "J&#243;zefa"
  ]
  node [
    id 43
    label "Pi&#322;sudski"
  ]
  node [
    id 44
    label "zielony"
  ]
  node [
    id 45
    label "planeta"
  ]
  node [
    id 46
    label "stowarzyszy&#263;"
  ]
  node [
    id 47
    label "pisarz"
  ]
  node [
    id 48
    label "literat"
  ]
  node [
    id 49
    label "miejski"
  ]
  node [
    id 50
    label "Hanna"
  ]
  node [
    id 51
    label "Suchocka"
  ]
  node [
    id 52
    label "akcja"
  ]
  node [
    id 53
    label "wyborczy"
  ]
  node [
    id 54
    label "forum"
  ]
  node [
    id 55
    label "obywatelski"
  ]
  node [
    id 56
    label "chrze&#347;cija&#324;ski"
  ]
  node [
    id 57
    label "demokracja"
  ]
  node [
    id 58
    label "Jerzy"
  ]
  node [
    id 59
    label "Kropiwnickiego"
  ]
  node [
    id 60
    label "lecha"
  ]
  node [
    id 61
    label "Kaczy&#324;ski"
  ]
  node [
    id 62
    label "order"
  ]
  node [
    id 63
    label "odrodzi&#263;"
  ]
  node [
    id 64
    label "starzy"
  ]
  node [
    id 65
    label "cmentarz"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 46
  ]
  edge [
    source 15
    target 47
  ]
  edge [
    source 15
    target 22
  ]
  edge [
    source 15
    target 48
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 27
  ]
  edge [
    source 19
    target 28
  ]
  edge [
    source 19
    target 29
  ]
  edge [
    source 19
    target 30
  ]
  edge [
    source 19
    target 31
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 24
  ]
  edge [
    source 22
    target 48
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 52
  ]
  edge [
    source 26
    target 53
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 29
  ]
  edge [
    source 27
    target 30
  ]
  edge [
    source 27
    target 31
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 30
  ]
  edge [
    source 28
    target 31
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 31
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 34
  ]
  edge [
    source 32
    target 49
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 37
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 62
  ]
  edge [
    source 36
    target 63
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 40
  ]
  edge [
    source 38
    target 41
  ]
  edge [
    source 38
    target 42
  ]
  edge [
    source 38
    target 43
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 41
  ]
  edge [
    source 39
    target 42
  ]
  edge [
    source 39
    target 43
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 42
  ]
  edge [
    source 40
    target 43
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 43
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 56
  ]
  edge [
    source 54
    target 57
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 57
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 64
    target 65
  ]
]
