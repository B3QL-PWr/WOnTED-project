graph [
  maxDegree 28
  minDegree 1
  meanDegree 1.986013986013986
  density 0.013986013986013986
  graphCliqueNumber 2
  node [
    id 0
    label "p&#243;&#322;metek"
    origin "text"
  ]
  node [
    id 1
    label "by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "dopiero"
    origin "text"
  ]
  node [
    id 3
    label "kapitalny"
    origin "text"
  ]
  node [
    id 4
    label "skok"
    origin "text"
  ]
  node [
    id 5
    label "fina&#322;"
    origin "text"
  ]
  node [
    id 6
    label "metr"
    origin "text"
  ]
  node [
    id 7
    label "pozwoli&#263;"
    origin "text"
  ]
  node [
    id 8
    label "piotr"
    origin "text"
  ]
  node [
    id 9
    label "&#380;y&#322;a"
    origin "text"
  ]
  node [
    id 10
    label "zaj&#261;&#263;"
    origin "text"
  ]
  node [
    id 11
    label "miejsce"
    origin "text"
  ]
  node [
    id 12
    label "niedzielna"
    origin "text"
  ]
  node [
    id 13
    label "konkurs"
    origin "text"
  ]
  node [
    id 14
    label "ni&#380;ny"
    origin "text"
  ]
  node [
    id 15
    label "tagile"
    origin "text"
  ]
  node [
    id 16
    label "impreza"
  ]
  node [
    id 17
    label "&#347;rodek"
  ]
  node [
    id 18
    label "si&#281;ga&#263;"
  ]
  node [
    id 19
    label "trwa&#263;"
  ]
  node [
    id 20
    label "obecno&#347;&#263;"
  ]
  node [
    id 21
    label "stan"
  ]
  node [
    id 22
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 23
    label "stand"
  ]
  node [
    id 24
    label "mie&#263;_miejsce"
  ]
  node [
    id 25
    label "uczestniczy&#263;"
  ]
  node [
    id 26
    label "chodzi&#263;"
  ]
  node [
    id 27
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 28
    label "equal"
  ]
  node [
    id 29
    label "dobry"
  ]
  node [
    id 30
    label "wspania&#322;y"
  ]
  node [
    id 31
    label "fundamentalny"
  ]
  node [
    id 32
    label "superancki"
  ]
  node [
    id 33
    label "kluczowy"
  ]
  node [
    id 34
    label "&#347;wietny"
  ]
  node [
    id 35
    label "kapitalnie"
  ]
  node [
    id 36
    label "wybicie"
  ]
  node [
    id 37
    label "konkurencja"
  ]
  node [
    id 38
    label "derail"
  ]
  node [
    id 39
    label "ptak"
  ]
  node [
    id 40
    label "ruch"
  ]
  node [
    id 41
    label "l&#261;dowanie"
  ]
  node [
    id 42
    label "&#322;apa"
  ]
  node [
    id 43
    label "struktura_anatomiczna"
  ]
  node [
    id 44
    label "stroke"
  ]
  node [
    id 45
    label "gimnastyka_przyrz&#261;dowa"
  ]
  node [
    id 46
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 47
    label "zmiana"
  ]
  node [
    id 48
    label "caper"
  ]
  node [
    id 49
    label "zaj&#261;c"
  ]
  node [
    id 50
    label "naskok"
  ]
  node [
    id 51
    label "napad"
  ]
  node [
    id 52
    label "ko&#347;&#263;_skokowa"
  ]
  node [
    id 53
    label "noga"
  ]
  node [
    id 54
    label "koniec"
  ]
  node [
    id 55
    label "conclusion"
  ]
  node [
    id 56
    label "coating"
  ]
  node [
    id 57
    label "runda"
  ]
  node [
    id 58
    label "meter"
  ]
  node [
    id 59
    label "decymetr"
  ]
  node [
    id 60
    label "megabyte"
  ]
  node [
    id 61
    label "plon"
  ]
  node [
    id 62
    label "metrum"
  ]
  node [
    id 63
    label "dekametr"
  ]
  node [
    id 64
    label "jednostka_powierzchni"
  ]
  node [
    id 65
    label "uk&#322;ad_SI"
  ]
  node [
    id 66
    label "literaturoznawstwo"
  ]
  node [
    id 67
    label "wiersz"
  ]
  node [
    id 68
    label "gigametr"
  ]
  node [
    id 69
    label "miara"
  ]
  node [
    id 70
    label "nauczyciel"
  ]
  node [
    id 71
    label "kilometr_kwadratowy"
  ]
  node [
    id 72
    label "jednostka_metryczna"
  ]
  node [
    id 73
    label "jednostka_masy"
  ]
  node [
    id 74
    label "centymetr_kwadratowy"
  ]
  node [
    id 75
    label "pofolgowa&#263;"
  ]
  node [
    id 76
    label "assent"
  ]
  node [
    id 77
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 78
    label "leave"
  ]
  node [
    id 79
    label "uzna&#263;"
  ]
  node [
    id 80
    label "cz&#322;owiek"
  ]
  node [
    id 81
    label "formacja_geologiczna"
  ]
  node [
    id 82
    label "vein"
  ]
  node [
    id 83
    label "chciwiec"
  ]
  node [
    id 84
    label "dost&#281;p_do&#380;ylny"
  ]
  node [
    id 85
    label "wymagaj&#261;cy"
  ]
  node [
    id 86
    label "lina"
  ]
  node [
    id 87
    label "materialista"
  ]
  node [
    id 88
    label "naczynie"
  ]
  node [
    id 89
    label "okrutnik"
  ]
  node [
    id 90
    label "sk&#261;piarz"
  ]
  node [
    id 91
    label "przew&#243;d"
  ]
  node [
    id 92
    label "sk&#261;py"
  ]
  node [
    id 93
    label "atleta"
  ]
  node [
    id 94
    label "dostarczy&#263;"
  ]
  node [
    id 95
    label "wype&#322;ni&#263;"
  ]
  node [
    id 96
    label "anektowa&#263;"
  ]
  node [
    id 97
    label "rozprzestrzeni&#263;_si&#281;"
  ]
  node [
    id 98
    label "obj&#261;&#263;"
  ]
  node [
    id 99
    label "zada&#263;"
  ]
  node [
    id 100
    label "sorb"
  ]
  node [
    id 101
    label "interest"
  ]
  node [
    id 102
    label "skorzysta&#263;"
  ]
  node [
    id 103
    label "usytuowa&#263;_si&#281;"
  ]
  node [
    id 104
    label "wzi&#261;&#263;"
  ]
  node [
    id 105
    label "employment"
  ]
  node [
    id 106
    label "zapanowa&#263;"
  ]
  node [
    id 107
    label "do"
  ]
  node [
    id 108
    label "klasyfikacja"
  ]
  node [
    id 109
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 110
    label "bankrupt"
  ]
  node [
    id 111
    label "zabra&#263;"
  ]
  node [
    id 112
    label "spowodowa&#263;"
  ]
  node [
    id 113
    label "ulokowa&#263;_si&#281;"
  ]
  node [
    id 114
    label "komornik"
  ]
  node [
    id 115
    label "prosecute"
  ]
  node [
    id 116
    label "seize"
  ]
  node [
    id 117
    label "topographic_point"
  ]
  node [
    id 118
    label "wzbudzi&#263;"
  ]
  node [
    id 119
    label "rozciekawi&#263;"
  ]
  node [
    id 120
    label "cia&#322;o"
  ]
  node [
    id 121
    label "plac"
  ]
  node [
    id 122
    label "cecha"
  ]
  node [
    id 123
    label "uwaga"
  ]
  node [
    id 124
    label "przestrze&#324;"
  ]
  node [
    id 125
    label "status"
  ]
  node [
    id 126
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 127
    label "chwila"
  ]
  node [
    id 128
    label "rz&#261;d"
  ]
  node [
    id 129
    label "praca"
  ]
  node [
    id 130
    label "location"
  ]
  node [
    id 131
    label "warunek_lokalowy"
  ]
  node [
    id 132
    label "eliminacje"
  ]
  node [
    id 133
    label "Interwizja"
  ]
  node [
    id 134
    label "emulation"
  ]
  node [
    id 135
    label "casting"
  ]
  node [
    id 136
    label "Eurowizja"
  ]
  node [
    id 137
    label "nab&#243;r"
  ]
  node [
    id 138
    label "Piotr"
  ]
  node [
    id 139
    label "Kamil"
  ]
  node [
    id 140
    label "Stoch"
  ]
  node [
    id 141
    label "Ryoyu"
  ]
  node [
    id 142
    label "Kobayashi"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 138
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 10
    target 105
  ]
  edge [
    source 10
    target 106
  ]
  edge [
    source 10
    target 107
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 10
    target 110
  ]
  edge [
    source 10
    target 111
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 120
  ]
  edge [
    source 11
    target 121
  ]
  edge [
    source 11
    target 122
  ]
  edge [
    source 11
    target 123
  ]
  edge [
    source 11
    target 124
  ]
  edge [
    source 11
    target 125
  ]
  edge [
    source 11
    target 126
  ]
  edge [
    source 11
    target 127
  ]
  edge [
    source 11
    target 46
  ]
  edge [
    source 11
    target 128
  ]
  edge [
    source 11
    target 129
  ]
  edge [
    source 11
    target 130
  ]
  edge [
    source 11
    target 131
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 132
  ]
  edge [
    source 13
    target 133
  ]
  edge [
    source 13
    target 134
  ]
  edge [
    source 13
    target 16
  ]
  edge [
    source 13
    target 135
  ]
  edge [
    source 13
    target 136
  ]
  edge [
    source 13
    target 137
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 139
    target 140
  ]
  edge [
    source 141
    target 142
  ]
]
