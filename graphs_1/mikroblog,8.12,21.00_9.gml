graph [
  maxDegree 16
  minDegree 1
  meanDegree 1.9545454545454546
  density 0.045454545454545456
  graphCliqueNumber 2
  node [
    id 0
    label "pn&#261;cz"
    origin "text"
  ]
  node [
    id 1
    label "odpad&#322;y"
    origin "text"
  ]
  node [
    id 2
    label "&#347;cian"
    origin "text"
  ]
  node [
    id 3
    label "budynek"
    origin "text"
  ]
  node [
    id 4
    label "pod"
    origin "text"
  ]
  node [
    id 5
    label "w&#322;asne"
    origin "text"
  ]
  node [
    id 6
    label "ci&#281;&#380;ar"
    origin "text"
  ]
  node [
    id 7
    label "estetyczneobrazki"
    origin "text"
  ]
  node [
    id 8
    label "nieboperfekcjonistow"
    origin "text"
  ]
  node [
    id 9
    label "architektura"
    origin "text"
  ]
  node [
    id 10
    label "kondygnacja"
  ]
  node [
    id 11
    label "skrzyd&#322;o"
  ]
  node [
    id 12
    label "dach"
  ]
  node [
    id 13
    label "balkon"
  ]
  node [
    id 14
    label "klatka_schodowa"
  ]
  node [
    id 15
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 16
    label "pod&#322;oga"
  ]
  node [
    id 17
    label "front"
  ]
  node [
    id 18
    label "strop"
  ]
  node [
    id 19
    label "alkierz"
  ]
  node [
    id 20
    label "budowla"
  ]
  node [
    id 21
    label "Pentagon"
  ]
  node [
    id 22
    label "przedpro&#380;e"
  ]
  node [
    id 23
    label "powinno&#347;&#263;"
  ]
  node [
    id 24
    label "warto&#347;&#263;"
  ]
  node [
    id 25
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 26
    label "weight"
  ]
  node [
    id 27
    label "zawa&#380;enie"
  ]
  node [
    id 28
    label "zawa&#380;y&#263;"
  ]
  node [
    id 29
    label "przedmiot"
  ]
  node [
    id 30
    label "hazard"
  ]
  node [
    id 31
    label "wym&#243;g"
  ]
  node [
    id 32
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 33
    label "hantla"
  ]
  node [
    id 34
    label "obarczy&#263;"
  ]
  node [
    id 35
    label "przeszkoda"
  ]
  node [
    id 36
    label "load"
  ]
  node [
    id 37
    label "styl_architektoniczny"
  ]
  node [
    id 38
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 39
    label "labirynt"
  ]
  node [
    id 40
    label "architektonika"
  ]
  node [
    id 41
    label "architektura_rezydencjonalna"
  ]
  node [
    id 42
    label "computer_architecture"
  ]
  node [
    id 43
    label "struktura"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 23
  ]
  edge [
    source 6
    target 24
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 37
  ]
  edge [
    source 9
    target 38
  ]
  edge [
    source 9
    target 39
  ]
  edge [
    source 9
    target 40
  ]
  edge [
    source 9
    target 41
  ]
  edge [
    source 9
    target 42
  ]
  edge [
    source 9
    target 43
  ]
]
