graph [
  maxDegree 3
  minDegree 1
  meanDegree 1.5555555555555556
  density 0.19444444444444445
  graphCliqueNumber 3
  node [
    id 0
    label "laskownica"
    origin "text"
  ]
  node [
    id 1
    label "ma&#322;a"
    origin "text"
  ]
  node [
    id 2
    label "dziewczynka"
  ]
  node [
    id 3
    label "dziewczyna"
  ]
  node [
    id 4
    label "Laskownica"
  ]
  node [
    id 5
    label "ma&#322;y"
  ]
  node [
    id 6
    label "Kcynia"
  ]
  node [
    id 7
    label "Go&#322;a&#324;cz"
  ]
  node [
    id 8
    label "W&#261;growiec"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 7
    target 8
  ]
]
