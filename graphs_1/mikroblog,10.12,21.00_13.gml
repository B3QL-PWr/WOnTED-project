graph [
  maxDegree 14
  minDegree 1
  meanDegree 1.9285714285714286
  density 0.07142857142857142
  graphCliqueNumber 2
  node [
    id 0
    label "pami&#281;ta&#263;"
    origin "text"
  ]
  node [
    id 1
    label "bajka"
    origin "text"
  ]
  node [
    id 2
    label "edda"
    origin "text"
  ]
  node [
    id 3
    label "si&#281;ga&#263;"
  ]
  node [
    id 4
    label "zna&#263;"
  ]
  node [
    id 5
    label "troska&#263;_si&#281;"
  ]
  node [
    id 6
    label "zachowywa&#263;"
  ]
  node [
    id 7
    label "chowa&#263;"
  ]
  node [
    id 8
    label "think"
  ]
  node [
    id 9
    label "pilnowa&#263;"
  ]
  node [
    id 10
    label "robi&#263;"
  ]
  node [
    id 11
    label "recall"
  ]
  node [
    id 12
    label "echo"
  ]
  node [
    id 13
    label "take_care"
  ]
  node [
    id 14
    label "apolog"
  ]
  node [
    id 15
    label "morfing"
  ]
  node [
    id 16
    label "film"
  ]
  node [
    id 17
    label "mora&#322;"
  ]
  node [
    id 18
    label "opowie&#347;&#263;"
  ]
  node [
    id 19
    label "narrative"
  ]
  node [
    id 20
    label "g&#322;upstwo"
  ]
  node [
    id 21
    label "utw&#243;r"
  ]
  node [
    id 22
    label "Pok&#233;mon"
  ]
  node [
    id 23
    label "epika"
  ]
  node [
    id 24
    label "komfort"
  ]
  node [
    id 25
    label "sytuacja"
  ]
  node [
    id 26
    label "Kambod&#380;a"
  ]
  node [
    id 27
    label "deska"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 2
    target 2
  ]
  edge [
    source 26
    target 27
  ]
]
