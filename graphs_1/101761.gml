graph [
  maxDegree 22
  minDegree 1
  meanDegree 2.0710059171597632
  density 0.01232741617357002
  graphCliqueNumber 3
  node [
    id 0
    label "okolica"
    origin "text"
  ]
  node [
    id 1
    label "obowi&#261;zkowo"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "gr&#243;b"
    origin "text"
  ]
  node [
    id 4
    label "hamlet"
    origin "text"
  ]
  node [
    id 5
    label "wiatrak"
    origin "text"
  ]
  node [
    id 6
    label "dom"
    origin "text"
  ]
  node [
    id 7
    label "kryty"
    origin "text"
  ]
  node [
    id 8
    label "strzecha"
    origin "text"
  ]
  node [
    id 9
    label "lub"
    origin "text"
  ]
  node [
    id 10
    label "azbest"
    origin "text"
  ]
  node [
    id 11
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 12
    label "nosi&#263;"
    origin "text"
  ]
  node [
    id 13
    label "siebie"
    origin "text"
  ]
  node [
    id 14
    label "termos"
    origin "text"
  ]
  node [
    id 15
    label "prohibicja"
    origin "text"
  ]
  node [
    id 16
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 17
    label "niewiele"
    origin "text"
  ]
  node [
    id 18
    label "wsp&#243;lny"
    origin "text"
  ]
  node [
    id 19
    label "&#347;rodek"
    origin "text"
  ]
  node [
    id 20
    label "kawa"
    origin "text"
  ]
  node [
    id 21
    label "bez"
    origin "text"
  ]
  node [
    id 22
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 23
    label "przeci&#281;tny"
    origin "text"
  ]
  node [
    id 24
    label "du&#324;czyk"
    origin "text"
  ]
  node [
    id 25
    label "przetrwa&#263;by"
    origin "text"
  ]
  node [
    id 26
    label "nawet"
    origin "text"
  ]
  node [
    id 27
    label "godzina"
    origin "text"
  ]
  node [
    id 28
    label "miejsce"
  ]
  node [
    id 29
    label "obszar"
  ]
  node [
    id 30
    label "krajobraz"
  ]
  node [
    id 31
    label "grupa"
  ]
  node [
    id 32
    label "organ"
  ]
  node [
    id 33
    label "przyroda"
  ]
  node [
    id 34
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 35
    label "po_s&#261;siedzku"
  ]
  node [
    id 36
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 37
    label "obligatoryjny"
  ]
  node [
    id 38
    label "niezb&#281;dnie"
  ]
  node [
    id 39
    label "si&#281;ga&#263;"
  ]
  node [
    id 40
    label "trwa&#263;"
  ]
  node [
    id 41
    label "obecno&#347;&#263;"
  ]
  node [
    id 42
    label "stan"
  ]
  node [
    id 43
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 44
    label "stand"
  ]
  node [
    id 45
    label "mie&#263;_miejsce"
  ]
  node [
    id 46
    label "uczestniczy&#263;"
  ]
  node [
    id 47
    label "chodzi&#263;"
  ]
  node [
    id 48
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 49
    label "equal"
  ]
  node [
    id 50
    label "defenestracja"
  ]
  node [
    id 51
    label "miejsce_poch&#243;wku"
  ]
  node [
    id 52
    label "nagrobek"
  ]
  node [
    id 53
    label "kres"
  ]
  node [
    id 54
    label "agonia"
  ]
  node [
    id 55
    label "spocz&#281;cie"
  ]
  node [
    id 56
    label "szeol"
  ]
  node [
    id 57
    label "spoczywanie"
  ]
  node [
    id 58
    label "mogi&#322;a"
  ]
  node [
    id 59
    label "spocz&#261;&#263;"
  ]
  node [
    id 60
    label "pochowanie"
  ]
  node [
    id 61
    label "pomnik"
  ]
  node [
    id 62
    label "prochowisko"
  ]
  node [
    id 63
    label "pogrzebanie"
  ]
  node [
    id 64
    label "chowanie"
  ]
  node [
    id 65
    label "park_sztywnych"
  ]
  node [
    id 66
    label "&#380;a&#322;oba"
  ]
  node [
    id 67
    label "zabicie"
  ]
  node [
    id 68
    label "spoczywa&#263;"
  ]
  node [
    id 69
    label "kres_&#380;ycia"
  ]
  node [
    id 70
    label "&#322;opatka"
  ]
  node [
    id 71
    label "&#347;mig&#322;o"
  ]
  node [
    id 72
    label "m&#322;yn"
  ]
  node [
    id 73
    label "fan"
  ]
  node [
    id 74
    label "budowla"
  ]
  node [
    id 75
    label "wentylator"
  ]
  node [
    id 76
    label "garderoba"
  ]
  node [
    id 77
    label "wiecha"
  ]
  node [
    id 78
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 79
    label "budynek"
  ]
  node [
    id 80
    label "fratria"
  ]
  node [
    id 81
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 82
    label "poj&#281;cie"
  ]
  node [
    id 83
    label "rodzina"
  ]
  node [
    id 84
    label "substancja_mieszkaniowa"
  ]
  node [
    id 85
    label "instytucja"
  ]
  node [
    id 86
    label "dom_rodzinny"
  ]
  node [
    id 87
    label "stead"
  ]
  node [
    id 88
    label "siedziba"
  ]
  node [
    id 89
    label "dach"
  ]
  node [
    id 90
    label "thatch"
  ]
  node [
    id 91
    label "przewr&#243;s&#322;o"
  ]
  node [
    id 92
    label "w&#322;osy"
  ]
  node [
    id 93
    label "tworzywo"
  ]
  node [
    id 94
    label "czynnik_rakotw&#243;rczy"
  ]
  node [
    id 95
    label "asbestos"
  ]
  node [
    id 96
    label "amfibol"
  ]
  node [
    id 97
    label "jaki&#347;"
  ]
  node [
    id 98
    label "str&#243;j"
  ]
  node [
    id 99
    label "mie&#263;"
  ]
  node [
    id 100
    label "wk&#322;ada&#263;"
  ]
  node [
    id 101
    label "przemieszcza&#263;"
  ]
  node [
    id 102
    label "posiada&#263;"
  ]
  node [
    id 103
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 104
    label "wear"
  ]
  node [
    id 105
    label "carry"
  ]
  node [
    id 106
    label "zawarto&#347;&#263;"
  ]
  node [
    id 107
    label "naczynie_Dewara"
  ]
  node [
    id 108
    label "naczynie"
  ]
  node [
    id 109
    label "zakaz"
  ]
  node [
    id 110
    label "czyj&#347;"
  ]
  node [
    id 111
    label "m&#261;&#380;"
  ]
  node [
    id 112
    label "nieistotnie"
  ]
  node [
    id 113
    label "nieznaczny"
  ]
  node [
    id 114
    label "pomiernie"
  ]
  node [
    id 115
    label "ma&#322;y"
  ]
  node [
    id 116
    label "spolny"
  ]
  node [
    id 117
    label "jeden"
  ]
  node [
    id 118
    label "sp&#243;lny"
  ]
  node [
    id 119
    label "wsp&#243;lnie"
  ]
  node [
    id 120
    label "uwsp&#243;lnianie"
  ]
  node [
    id 121
    label "uwsp&#243;lnienie"
  ]
  node [
    id 122
    label "czas"
  ]
  node [
    id 123
    label "abstrakcja"
  ]
  node [
    id 124
    label "punkt"
  ]
  node [
    id 125
    label "substancja"
  ]
  node [
    id 126
    label "spos&#243;b"
  ]
  node [
    id 127
    label "chemikalia"
  ]
  node [
    id 128
    label "u&#380;ywka"
  ]
  node [
    id 129
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 130
    label "dripper"
  ]
  node [
    id 131
    label "marzanowate"
  ]
  node [
    id 132
    label "produkt"
  ]
  node [
    id 133
    label "ziarno"
  ]
  node [
    id 134
    label "pestkowiec"
  ]
  node [
    id 135
    label "nap&#243;j"
  ]
  node [
    id 136
    label "jedzenie"
  ]
  node [
    id 137
    label "ro&#347;lina"
  ]
  node [
    id 138
    label "egzotyk"
  ]
  node [
    id 139
    label "porcja"
  ]
  node [
    id 140
    label "kofeina"
  ]
  node [
    id 141
    label "chemex"
  ]
  node [
    id 142
    label "ki&#347;&#263;"
  ]
  node [
    id 143
    label "krzew"
  ]
  node [
    id 144
    label "pi&#380;maczkowate"
  ]
  node [
    id 145
    label "kwiat"
  ]
  node [
    id 146
    label "owoc"
  ]
  node [
    id 147
    label "oliwkowate"
  ]
  node [
    id 148
    label "hy&#263;ka"
  ]
  node [
    id 149
    label "lilac"
  ]
  node [
    id 150
    label "delfinidyna"
  ]
  node [
    id 151
    label "orientacyjny"
  ]
  node [
    id 152
    label "&#347;rednio"
  ]
  node [
    id 153
    label "przeci&#281;tnie"
  ]
  node [
    id 154
    label "taki_sobie"
  ]
  node [
    id 155
    label "zwyczajny"
  ]
  node [
    id 156
    label "minuta"
  ]
  node [
    id 157
    label "doba"
  ]
  node [
    id 158
    label "p&#243;&#322;godzina"
  ]
  node [
    id 159
    label "kwadrans"
  ]
  node [
    id 160
    label "time"
  ]
  node [
    id 161
    label "jednostka_czasu"
  ]
  node [
    id 162
    label "Might"
  ]
  node [
    id 163
    label "Anda"
  ]
  node [
    id 164
    label "Magic"
  ]
  node [
    id 165
    label "unia"
  ]
  node [
    id 166
    label "europejski"
  ]
  node [
    id 167
    label "1"
  ]
  node [
    id 168
    label "0"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 106
  ]
  edge [
    source 14
    target 107
  ]
  edge [
    source 14
    target 108
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 109
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 110
  ]
  edge [
    source 16
    target 111
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 112
  ]
  edge [
    source 17
    target 113
  ]
  edge [
    source 17
    target 114
  ]
  edge [
    source 17
    target 115
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 116
  ]
  edge [
    source 18
    target 117
  ]
  edge [
    source 18
    target 118
  ]
  edge [
    source 18
    target 119
  ]
  edge [
    source 18
    target 120
  ]
  edge [
    source 18
    target 121
  ]
  edge [
    source 19
    target 28
  ]
  edge [
    source 19
    target 122
  ]
  edge [
    source 19
    target 123
  ]
  edge [
    source 19
    target 124
  ]
  edge [
    source 19
    target 125
  ]
  edge [
    source 19
    target 126
  ]
  edge [
    source 19
    target 127
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 128
  ]
  edge [
    source 20
    target 129
  ]
  edge [
    source 20
    target 130
  ]
  edge [
    source 20
    target 131
  ]
  edge [
    source 20
    target 132
  ]
  edge [
    source 20
    target 133
  ]
  edge [
    source 20
    target 134
  ]
  edge [
    source 20
    target 135
  ]
  edge [
    source 20
    target 136
  ]
  edge [
    source 20
    target 137
  ]
  edge [
    source 20
    target 138
  ]
  edge [
    source 20
    target 139
  ]
  edge [
    source 20
    target 140
  ]
  edge [
    source 20
    target 141
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 142
  ]
  edge [
    source 21
    target 129
  ]
  edge [
    source 21
    target 143
  ]
  edge [
    source 21
    target 144
  ]
  edge [
    source 21
    target 134
  ]
  edge [
    source 21
    target 145
  ]
  edge [
    source 21
    target 146
  ]
  edge [
    source 21
    target 147
  ]
  edge [
    source 21
    target 137
  ]
  edge [
    source 21
    target 148
  ]
  edge [
    source 21
    target 149
  ]
  edge [
    source 21
    target 150
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 151
  ]
  edge [
    source 23
    target 152
  ]
  edge [
    source 23
    target 153
  ]
  edge [
    source 23
    target 154
  ]
  edge [
    source 23
    target 155
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 156
  ]
  edge [
    source 27
    target 157
  ]
  edge [
    source 27
    target 122
  ]
  edge [
    source 27
    target 158
  ]
  edge [
    source 27
    target 159
  ]
  edge [
    source 27
    target 160
  ]
  edge [
    source 27
    target 161
  ]
  edge [
    source 162
    target 163
  ]
  edge [
    source 162
    target 164
  ]
  edge [
    source 163
    target 164
  ]
  edge [
    source 165
    target 166
  ]
  edge [
    source 167
    target 168
  ]
]
