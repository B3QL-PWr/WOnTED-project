graph [
  maxDegree 15
  minDegree 1
  meanDegree 1.9344262295081966
  density 0.03224043715846994
  graphCliqueNumber 2
  node [
    id 0
    label "nazywa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "cezary"
    origin "text"
  ]
  node [
    id 3
    label "baryka"
    origin "text"
  ]
  node [
    id 4
    label "dwadzie&#347;cia"
    origin "text"
  ]
  node [
    id 5
    label "minuta"
    origin "text"
  ]
  node [
    id 6
    label "by&#263;"
    origin "text"
  ]
  node [
    id 7
    label "w&#322;a&#347;ciciel"
    origin "text"
  ]
  node [
    id 8
    label "oto"
    origin "text"
  ]
  node [
    id 9
    label "szklany"
    origin "text"
  ]
  node [
    id 10
    label "dom"
    origin "text"
  ]
  node [
    id 11
    label "give"
  ]
  node [
    id 12
    label "mieni&#263;"
  ]
  node [
    id 13
    label "okre&#347;la&#263;"
  ]
  node [
    id 14
    label "nadawa&#263;"
  ]
  node [
    id 15
    label "zapis"
  ]
  node [
    id 16
    label "godzina"
  ]
  node [
    id 17
    label "sekunda"
  ]
  node [
    id 18
    label "kwadrans"
  ]
  node [
    id 19
    label "stopie&#324;"
  ]
  node [
    id 20
    label "design"
  ]
  node [
    id 21
    label "time"
  ]
  node [
    id 22
    label "jednostka"
  ]
  node [
    id 23
    label "si&#281;ga&#263;"
  ]
  node [
    id 24
    label "trwa&#263;"
  ]
  node [
    id 25
    label "obecno&#347;&#263;"
  ]
  node [
    id 26
    label "stan"
  ]
  node [
    id 27
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 28
    label "stand"
  ]
  node [
    id 29
    label "mie&#263;_miejsce"
  ]
  node [
    id 30
    label "uczestniczy&#263;"
  ]
  node [
    id 31
    label "chodzi&#263;"
  ]
  node [
    id 32
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 33
    label "equal"
  ]
  node [
    id 34
    label "wykupienie"
  ]
  node [
    id 35
    label "bycie_w_posiadaniu"
  ]
  node [
    id 36
    label "wykupywanie"
  ]
  node [
    id 37
    label "podmiot"
  ]
  node [
    id 38
    label "d&#378;wi&#281;czny"
  ]
  node [
    id 39
    label "pusty"
  ]
  node [
    id 40
    label "oboj&#281;tny"
  ]
  node [
    id 41
    label "szkli&#347;cie"
  ]
  node [
    id 42
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 43
    label "przezroczysty"
  ]
  node [
    id 44
    label "chorobliwy"
  ]
  node [
    id 45
    label "garderoba"
  ]
  node [
    id 46
    label "wiecha"
  ]
  node [
    id 47
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 48
    label "grupa"
  ]
  node [
    id 49
    label "budynek"
  ]
  node [
    id 50
    label "fratria"
  ]
  node [
    id 51
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 52
    label "poj&#281;cie"
  ]
  node [
    id 53
    label "rodzina"
  ]
  node [
    id 54
    label "substancja_mieszkaniowa"
  ]
  node [
    id 55
    label "instytucja"
  ]
  node [
    id 56
    label "dom_rodzinny"
  ]
  node [
    id 57
    label "stead"
  ]
  node [
    id 58
    label "siedziba"
  ]
  node [
    id 59
    label "Cezary"
  ]
  node [
    id 60
    label "Baryka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 15
  ]
  edge [
    source 5
    target 16
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 23
  ]
  edge [
    source 6
    target 24
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 34
  ]
  edge [
    source 7
    target 35
  ]
  edge [
    source 7
    target 36
  ]
  edge [
    source 7
    target 37
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 38
  ]
  edge [
    source 9
    target 39
  ]
  edge [
    source 9
    target 40
  ]
  edge [
    source 9
    target 41
  ]
  edge [
    source 9
    target 42
  ]
  edge [
    source 9
    target 43
  ]
  edge [
    source 9
    target 44
  ]
  edge [
    source 10
    target 45
  ]
  edge [
    source 10
    target 46
  ]
  edge [
    source 10
    target 47
  ]
  edge [
    source 10
    target 48
  ]
  edge [
    source 10
    target 49
  ]
  edge [
    source 10
    target 50
  ]
  edge [
    source 10
    target 51
  ]
  edge [
    source 10
    target 52
  ]
  edge [
    source 10
    target 53
  ]
  edge [
    source 10
    target 54
  ]
  edge [
    source 10
    target 55
  ]
  edge [
    source 10
    target 56
  ]
  edge [
    source 10
    target 57
  ]
  edge [
    source 10
    target 58
  ]
  edge [
    source 59
    target 60
  ]
]
