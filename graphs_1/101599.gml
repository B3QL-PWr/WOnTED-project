graph [
  maxDegree 39
  minDegree 1
  meanDegree 2.0913242009132422
  density 0.009593230279418542
  graphCliqueNumber 3
  node [
    id 0
    label "przyj&#347;&#263;"
    origin "text"
  ]
  node [
    id 1
    label "my&#347;l"
    origin "text"
  ]
  node [
    id 2
    label "ostatecznie"
    origin "text"
  ]
  node [
    id 3
    label "dla"
    origin "text"
  ]
  node [
    id 4
    label "tota"
    origin "text"
  ]
  node [
    id 5
    label "taka"
    origin "text"
  ]
  node [
    id 6
    label "przegrana"
    origin "text"
  ]
  node [
    id 7
    label "istotnie"
    origin "text"
  ]
  node [
    id 8
    label "nic"
    origin "text"
  ]
  node [
    id 9
    label "znaczy&#263;"
    origin "text"
  ]
  node [
    id 10
    label "dobrze"
    origin "text"
  ]
  node [
    id 11
    label "by&#263;by"
    origin "text"
  ]
  node [
    id 12
    label "cho&#263;"
    origin "text"
  ]
  node [
    id 13
    label "taki"
    origin "text"
  ]
  node [
    id 14
    label "spos&#243;b"
    origin "text"
  ]
  node [
    id 15
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 16
    label "ukara&#263;"
    origin "text"
  ]
  node [
    id 17
    label "sw&#243;j"
    origin "text"
  ]
  node [
    id 18
    label "zarozumia&#322;o&#347;&#263;"
    origin "text"
  ]
  node [
    id 19
    label "ten"
    origin "text"
  ]
  node [
    id 20
    label "g&#322;upia"
    origin "text"
  ]
  node [
    id 21
    label "muszka"
    origin "text"
  ]
  node [
    id 22
    label "d&#322;ugi"
    origin "text"
  ]
  node [
    id 23
    label "zastanowienie"
    origin "text"
  ]
  node [
    id 24
    label "doj&#347;&#263;by&#263;"
    origin "text"
  ]
  node [
    id 25
    label "przekonania"
    origin "text"
  ]
  node [
    id 26
    label "czu&#263;byby&#263;"
    origin "text"
  ]
  node [
    id 27
    label "siebie"
    origin "text"
  ]
  node [
    id 28
    label "wstr&#281;t"
    origin "text"
  ]
  node [
    id 29
    label "gdybyby&#263;"
    origin "text"
  ]
  node [
    id 30
    label "wzi&#261;&#263;"
    origin "text"
  ]
  node [
    id 31
    label "machinacja"
    origin "text"
  ]
  node [
    id 32
    label "stryj"
    origin "text"
  ]
  node [
    id 33
    label "powiedzie&#263;by&#263;"
    origin "text"
  ]
  node [
    id 34
    label "oczywi&#347;cie"
    origin "text"
  ]
  node [
    id 35
    label "si&#281;"
    origin "text"
  ]
  node [
    id 36
    label "zgadza&#263;"
    origin "text"
  ]
  node [
    id 37
    label "mie&#263;by&#263;"
    origin "text"
  ]
  node [
    id 38
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 39
    label "pomys&#322;"
    origin "text"
  ]
  node [
    id 40
    label "zaaran&#380;owa&#263;"
    origin "text"
  ]
  node [
    id 41
    label "sprawa"
    origin "text"
  ]
  node [
    id 42
    label "inny"
    origin "text"
  ]
  node [
    id 43
    label "sta&#263;_si&#281;"
  ]
  node [
    id 44
    label "zaistnie&#263;"
  ]
  node [
    id 45
    label "czas"
  ]
  node [
    id 46
    label "doj&#347;&#263;"
  ]
  node [
    id 47
    label "become"
  ]
  node [
    id 48
    label "line_up"
  ]
  node [
    id 49
    label "przyby&#263;"
  ]
  node [
    id 50
    label "system"
  ]
  node [
    id 51
    label "s&#261;d"
  ]
  node [
    id 52
    label "wytw&#243;r"
  ]
  node [
    id 53
    label "istota"
  ]
  node [
    id 54
    label "thinking"
  ]
  node [
    id 55
    label "idea"
  ]
  node [
    id 56
    label "political_orientation"
  ]
  node [
    id 57
    label "szko&#322;a"
  ]
  node [
    id 58
    label "umys&#322;"
  ]
  node [
    id 59
    label "fantomatyka"
  ]
  node [
    id 60
    label "t&#322;oczenie_si&#281;"
  ]
  node [
    id 61
    label "p&#322;&#243;d"
  ]
  node [
    id 62
    label "ostateczny"
  ]
  node [
    id 63
    label "finalnie"
  ]
  node [
    id 64
    label "zupe&#322;nie"
  ]
  node [
    id 65
    label "Bangladesz"
  ]
  node [
    id 66
    label "jednostka_monetarna"
  ]
  node [
    id 67
    label "gorzka_pigu&#322;ka"
  ]
  node [
    id 68
    label "przegra"
  ]
  node [
    id 69
    label "wysiadka"
  ]
  node [
    id 70
    label "passa"
  ]
  node [
    id 71
    label "niefortunno&#347;&#263;"
  ]
  node [
    id 72
    label "po&#322;o&#380;enie"
  ]
  node [
    id 73
    label "niepowodzenie"
  ]
  node [
    id 74
    label "strata"
  ]
  node [
    id 75
    label "reverse"
  ]
  node [
    id 76
    label "rezultat"
  ]
  node [
    id 77
    label "kwota"
  ]
  node [
    id 78
    label "k&#322;adzenie"
  ]
  node [
    id 79
    label "realnie"
  ]
  node [
    id 80
    label "importantly"
  ]
  node [
    id 81
    label "istotny"
  ]
  node [
    id 82
    label "wa&#380;ny"
  ]
  node [
    id 83
    label "miernota"
  ]
  node [
    id 84
    label "g&#243;wno"
  ]
  node [
    id 85
    label "love"
  ]
  node [
    id 86
    label "ilo&#347;&#263;"
  ]
  node [
    id 87
    label "brak"
  ]
  node [
    id 88
    label "ciura"
  ]
  node [
    id 89
    label "by&#263;"
  ]
  node [
    id 90
    label "spell"
  ]
  node [
    id 91
    label "odgrywa&#263;_rol&#281;"
  ]
  node [
    id 92
    label "zostawia&#263;"
  ]
  node [
    id 93
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 94
    label "represent"
  ]
  node [
    id 95
    label "count"
  ]
  node [
    id 96
    label "wyraz"
  ]
  node [
    id 97
    label "zdobi&#263;"
  ]
  node [
    id 98
    label "moralnie"
  ]
  node [
    id 99
    label "wiele"
  ]
  node [
    id 100
    label "lepiej"
  ]
  node [
    id 101
    label "korzystnie"
  ]
  node [
    id 102
    label "pomy&#347;lnie"
  ]
  node [
    id 103
    label "pozytywnie"
  ]
  node [
    id 104
    label "dobry"
  ]
  node [
    id 105
    label "dobroczynnie"
  ]
  node [
    id 106
    label "odpowiednio"
  ]
  node [
    id 107
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 108
    label "skutecznie"
  ]
  node [
    id 109
    label "okre&#347;lony"
  ]
  node [
    id 110
    label "jaki&#347;"
  ]
  node [
    id 111
    label "model"
  ]
  node [
    id 112
    label "zbi&#243;r"
  ]
  node [
    id 113
    label "tryb"
  ]
  node [
    id 114
    label "narz&#281;dzie"
  ]
  node [
    id 115
    label "nature"
  ]
  node [
    id 116
    label "proceed"
  ]
  node [
    id 117
    label "catch"
  ]
  node [
    id 118
    label "pozosta&#263;"
  ]
  node [
    id 119
    label "osta&#263;_si&#281;"
  ]
  node [
    id 120
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 121
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 122
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 123
    label "change"
  ]
  node [
    id 124
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 125
    label "inflict"
  ]
  node [
    id 126
    label "spowodowa&#263;"
  ]
  node [
    id 127
    label "cz&#322;owiek"
  ]
  node [
    id 128
    label "bli&#378;ni"
  ]
  node [
    id 129
    label "odpowiedni"
  ]
  node [
    id 130
    label "swojak"
  ]
  node [
    id 131
    label "samodzielny"
  ]
  node [
    id 132
    label "samochwalstwo"
  ]
  node [
    id 133
    label "pyszno&#347;&#263;"
  ]
  node [
    id 134
    label "cecha"
  ]
  node [
    id 135
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 136
    label "dodatek"
  ]
  node [
    id 137
    label "fly"
  ]
  node [
    id 138
    label "ozdoba"
  ]
  node [
    id 139
    label "przyn&#281;ta_sztuczna"
  ]
  node [
    id 140
    label "celownik"
  ]
  node [
    id 141
    label "punkt"
  ]
  node [
    id 142
    label "daleki"
  ]
  node [
    id 143
    label "d&#322;ugo"
  ]
  node [
    id 144
    label "ruch"
  ]
  node [
    id 145
    label "zastanowienie_si&#281;"
  ]
  node [
    id 146
    label "trud"
  ]
  node [
    id 147
    label "zainteresowanie"
  ]
  node [
    id 148
    label "consideration"
  ]
  node [
    id 149
    label "niech&#281;&#263;"
  ]
  node [
    id 150
    label "obrzyd&#322;o&#347;&#263;"
  ]
  node [
    id 151
    label "idiosynkrazja"
  ]
  node [
    id 152
    label "antipathy"
  ]
  node [
    id 153
    label "wej&#347;&#263;"
  ]
  node [
    id 154
    label "get"
  ]
  node [
    id 155
    label "wzi&#281;cie"
  ]
  node [
    id 156
    label "wyrucha&#263;"
  ]
  node [
    id 157
    label "uciec"
  ]
  node [
    id 158
    label "ruszy&#263;"
  ]
  node [
    id 159
    label "wygra&#263;"
  ]
  node [
    id 160
    label "obj&#261;&#263;"
  ]
  node [
    id 161
    label "zacz&#261;&#263;"
  ]
  node [
    id 162
    label "wyciupcia&#263;"
  ]
  node [
    id 163
    label "World_Health_Organization"
  ]
  node [
    id 164
    label "skorzysta&#263;"
  ]
  node [
    id 165
    label "pokona&#263;"
  ]
  node [
    id 166
    label "poczyta&#263;"
  ]
  node [
    id 167
    label "poruszy&#263;"
  ]
  node [
    id 168
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 169
    label "take"
  ]
  node [
    id 170
    label "aim"
  ]
  node [
    id 171
    label "arise"
  ]
  node [
    id 172
    label "u&#380;y&#263;"
  ]
  node [
    id 173
    label "zaatakowa&#263;"
  ]
  node [
    id 174
    label "receive"
  ]
  node [
    id 175
    label "uda&#263;_si&#281;"
  ]
  node [
    id 176
    label "dosta&#263;"
  ]
  node [
    id 177
    label "otrzyma&#263;"
  ]
  node [
    id 178
    label "obskoczy&#263;"
  ]
  node [
    id 179
    label "zaopatrzy&#263;_si&#281;"
  ]
  node [
    id 180
    label "zrobi&#263;"
  ]
  node [
    id 181
    label "bra&#263;"
  ]
  node [
    id 182
    label "nakaza&#263;"
  ]
  node [
    id 183
    label "chwyci&#263;"
  ]
  node [
    id 184
    label "przyj&#261;&#263;"
  ]
  node [
    id 185
    label "seize"
  ]
  node [
    id 186
    label "odziedziczy&#263;"
  ]
  node [
    id 187
    label "withdraw"
  ]
  node [
    id 188
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 189
    label "wch&#322;on&#261;&#263;"
  ]
  node [
    id 190
    label "kombinacja"
  ]
  node [
    id 191
    label "brat"
  ]
  node [
    id 192
    label "wujek"
  ]
  node [
    id 193
    label "zgodzi&#263;"
  ]
  node [
    id 194
    label "assent"
  ]
  node [
    id 195
    label "zgadzanie"
  ]
  node [
    id 196
    label "zatrudnia&#263;"
  ]
  node [
    id 197
    label "ukra&#347;&#263;"
  ]
  node [
    id 198
    label "ukradzenie"
  ]
  node [
    id 199
    label "pocz&#261;tki"
  ]
  node [
    id 200
    label "zorganizowa&#263;"
  ]
  node [
    id 201
    label "produce"
  ]
  node [
    id 202
    label "organize"
  ]
  node [
    id 203
    label "temat"
  ]
  node [
    id 204
    label "kognicja"
  ]
  node [
    id 205
    label "szczeg&#243;&#322;"
  ]
  node [
    id 206
    label "rzecz"
  ]
  node [
    id 207
    label "wydarzenie"
  ]
  node [
    id 208
    label "przes&#322;anka"
  ]
  node [
    id 209
    label "rozprawa"
  ]
  node [
    id 210
    label "object"
  ]
  node [
    id 211
    label "proposition"
  ]
  node [
    id 212
    label "kolejny"
  ]
  node [
    id 213
    label "inaczej"
  ]
  node [
    id 214
    label "r&#243;&#380;ny"
  ]
  node [
    id 215
    label "inszy"
  ]
  node [
    id 216
    label "osobno"
  ]
  node [
    id 217
    label "Roberto"
  ]
  node [
    id 218
    label "Tonnorem"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 10
    target 105
  ]
  edge [
    source 10
    target 106
  ]
  edge [
    source 10
    target 107
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 42
  ]
  edge [
    source 14
    target 111
  ]
  edge [
    source 14
    target 112
  ]
  edge [
    source 14
    target 113
  ]
  edge [
    source 14
    target 114
  ]
  edge [
    source 14
    target 115
  ]
  edge [
    source 14
    target 22
  ]
  edge [
    source 14
    target 28
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 116
  ]
  edge [
    source 15
    target 117
  ]
  edge [
    source 15
    target 118
  ]
  edge [
    source 15
    target 119
  ]
  edge [
    source 15
    target 120
  ]
  edge [
    source 15
    target 121
  ]
  edge [
    source 15
    target 122
  ]
  edge [
    source 15
    target 123
  ]
  edge [
    source 15
    target 124
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 125
  ]
  edge [
    source 16
    target 126
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 127
  ]
  edge [
    source 17
    target 128
  ]
  edge [
    source 17
    target 129
  ]
  edge [
    source 17
    target 130
  ]
  edge [
    source 17
    target 131
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 132
  ]
  edge [
    source 18
    target 133
  ]
  edge [
    source 18
    target 134
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 30
  ]
  edge [
    source 19
    target 31
  ]
  edge [
    source 19
    target 40
  ]
  edge [
    source 19
    target 41
  ]
  edge [
    source 19
    target 109
  ]
  edge [
    source 19
    target 135
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 136
  ]
  edge [
    source 21
    target 137
  ]
  edge [
    source 21
    target 138
  ]
  edge [
    source 21
    target 139
  ]
  edge [
    source 21
    target 140
  ]
  edge [
    source 21
    target 141
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 142
  ]
  edge [
    source 22
    target 143
  ]
  edge [
    source 22
    target 144
  ]
  edge [
    source 22
    target 28
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 145
  ]
  edge [
    source 23
    target 146
  ]
  edge [
    source 23
    target 147
  ]
  edge [
    source 23
    target 148
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 149
  ]
  edge [
    source 28
    target 150
  ]
  edge [
    source 28
    target 151
  ]
  edge [
    source 28
    target 152
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 153
  ]
  edge [
    source 30
    target 154
  ]
  edge [
    source 30
    target 155
  ]
  edge [
    source 30
    target 156
  ]
  edge [
    source 30
    target 157
  ]
  edge [
    source 30
    target 158
  ]
  edge [
    source 30
    target 159
  ]
  edge [
    source 30
    target 160
  ]
  edge [
    source 30
    target 161
  ]
  edge [
    source 30
    target 162
  ]
  edge [
    source 30
    target 163
  ]
  edge [
    source 30
    target 164
  ]
  edge [
    source 30
    target 165
  ]
  edge [
    source 30
    target 166
  ]
  edge [
    source 30
    target 167
  ]
  edge [
    source 30
    target 168
  ]
  edge [
    source 30
    target 169
  ]
  edge [
    source 30
    target 170
  ]
  edge [
    source 30
    target 171
  ]
  edge [
    source 30
    target 172
  ]
  edge [
    source 30
    target 173
  ]
  edge [
    source 30
    target 174
  ]
  edge [
    source 30
    target 175
  ]
  edge [
    source 30
    target 176
  ]
  edge [
    source 30
    target 177
  ]
  edge [
    source 30
    target 178
  ]
  edge [
    source 30
    target 179
  ]
  edge [
    source 30
    target 180
  ]
  edge [
    source 30
    target 181
  ]
  edge [
    source 30
    target 182
  ]
  edge [
    source 30
    target 183
  ]
  edge [
    source 30
    target 184
  ]
  edge [
    source 30
    target 185
  ]
  edge [
    source 30
    target 186
  ]
  edge [
    source 30
    target 187
  ]
  edge [
    source 30
    target 188
  ]
  edge [
    source 30
    target 189
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 190
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 191
  ]
  edge [
    source 32
    target 192
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 193
  ]
  edge [
    source 36
    target 194
  ]
  edge [
    source 36
    target 195
  ]
  edge [
    source 36
    target 196
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 50
  ]
  edge [
    source 39
    target 52
  ]
  edge [
    source 39
    target 55
  ]
  edge [
    source 39
    target 197
  ]
  edge [
    source 39
    target 198
  ]
  edge [
    source 39
    target 199
  ]
  edge [
    source 40
    target 200
  ]
  edge [
    source 40
    target 201
  ]
  edge [
    source 40
    target 202
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 203
  ]
  edge [
    source 41
    target 204
  ]
  edge [
    source 41
    target 55
  ]
  edge [
    source 41
    target 205
  ]
  edge [
    source 41
    target 206
  ]
  edge [
    source 41
    target 207
  ]
  edge [
    source 41
    target 208
  ]
  edge [
    source 41
    target 209
  ]
  edge [
    source 41
    target 210
  ]
  edge [
    source 41
    target 211
  ]
  edge [
    source 42
    target 212
  ]
  edge [
    source 42
    target 213
  ]
  edge [
    source 42
    target 214
  ]
  edge [
    source 42
    target 215
  ]
  edge [
    source 42
    target 216
  ]
  edge [
    source 217
    target 218
  ]
]
