graph [
  maxDegree 47
  minDegree 1
  meanDegree 2
  density 0.018518518518518517
  graphCliqueNumber 2
  node [
    id 0
    label "pouczaj&#261;cy"
    origin "text"
  ]
  node [
    id 1
    label "wyk&#322;ad"
    origin "text"
  ]
  node [
    id 2
    label "dla"
    origin "text"
  ]
  node [
    id 3
    label "osoba"
    origin "text"
  ]
  node [
    id 4
    label "pocz&#261;tkuj&#261;ca"
    origin "text"
  ]
  node [
    id 5
    label "&#347;rednio"
    origin "text"
  ]
  node [
    id 6
    label "zaawansowana"
    origin "text"
  ]
  node [
    id 7
    label "j&#281;zyk"
    origin "text"
  ]
  node [
    id 8
    label "angielski"
    origin "text"
  ]
  node [
    id 9
    label "pouczaj&#261;co"
  ]
  node [
    id 10
    label "u&#380;yteczny"
  ]
  node [
    id 11
    label "przem&#243;wienie"
  ]
  node [
    id 12
    label "lecture"
  ]
  node [
    id 13
    label "kurs"
  ]
  node [
    id 14
    label "t&#322;umaczenie"
  ]
  node [
    id 15
    label "Zgredek"
  ]
  node [
    id 16
    label "kategoria_gramatyczna"
  ]
  node [
    id 17
    label "Casanova"
  ]
  node [
    id 18
    label "Don_Juan"
  ]
  node [
    id 19
    label "Gargantua"
  ]
  node [
    id 20
    label "Faust"
  ]
  node [
    id 21
    label "profanum"
  ]
  node [
    id 22
    label "Chocho&#322;"
  ]
  node [
    id 23
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 24
    label "koniugacja"
  ]
  node [
    id 25
    label "Winnetou"
  ]
  node [
    id 26
    label "Dwukwiat"
  ]
  node [
    id 27
    label "homo_sapiens"
  ]
  node [
    id 28
    label "Edyp"
  ]
  node [
    id 29
    label "Herkules_Poirot"
  ]
  node [
    id 30
    label "ludzko&#347;&#263;"
  ]
  node [
    id 31
    label "mikrokosmos"
  ]
  node [
    id 32
    label "person"
  ]
  node [
    id 33
    label "Sherlock_Holmes"
  ]
  node [
    id 34
    label "portrecista"
  ]
  node [
    id 35
    label "Szwejk"
  ]
  node [
    id 36
    label "Hamlet"
  ]
  node [
    id 37
    label "duch"
  ]
  node [
    id 38
    label "g&#322;owa"
  ]
  node [
    id 39
    label "oddzia&#322;ywanie"
  ]
  node [
    id 40
    label "Quasimodo"
  ]
  node [
    id 41
    label "Dulcynea"
  ]
  node [
    id 42
    label "Don_Kiszot"
  ]
  node [
    id 43
    label "Wallenrod"
  ]
  node [
    id 44
    label "Plastu&#347;"
  ]
  node [
    id 45
    label "Harry_Potter"
  ]
  node [
    id 46
    label "figura"
  ]
  node [
    id 47
    label "parali&#380;owa&#263;"
  ]
  node [
    id 48
    label "istota"
  ]
  node [
    id 49
    label "Werter"
  ]
  node [
    id 50
    label "antropochoria"
  ]
  node [
    id 51
    label "posta&#263;"
  ]
  node [
    id 52
    label "orientacyjnie"
  ]
  node [
    id 53
    label "&#347;redni"
  ]
  node [
    id 54
    label "ma&#322;o"
  ]
  node [
    id 55
    label "bezbarwnie"
  ]
  node [
    id 56
    label "tak_sobie"
  ]
  node [
    id 57
    label "pisa&#263;"
  ]
  node [
    id 58
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 59
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 60
    label "ssanie"
  ]
  node [
    id 61
    label "po_koroniarsku"
  ]
  node [
    id 62
    label "przedmiot"
  ]
  node [
    id 63
    label "but"
  ]
  node [
    id 64
    label "m&#243;wienie"
  ]
  node [
    id 65
    label "rozumie&#263;"
  ]
  node [
    id 66
    label "formacja_geologiczna"
  ]
  node [
    id 67
    label "rozumienie"
  ]
  node [
    id 68
    label "m&#243;wi&#263;"
  ]
  node [
    id 69
    label "gramatyka"
  ]
  node [
    id 70
    label "pype&#263;"
  ]
  node [
    id 71
    label "makroglosja"
  ]
  node [
    id 72
    label "kawa&#322;ek"
  ]
  node [
    id 73
    label "artykulator"
  ]
  node [
    id 74
    label "kultura_duchowa"
  ]
  node [
    id 75
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 76
    label "jama_ustna"
  ]
  node [
    id 77
    label "spos&#243;b"
  ]
  node [
    id 78
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 79
    label "przet&#322;umaczenie"
  ]
  node [
    id 80
    label "language"
  ]
  node [
    id 81
    label "jeniec"
  ]
  node [
    id 82
    label "organ"
  ]
  node [
    id 83
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 84
    label "pismo"
  ]
  node [
    id 85
    label "formalizowanie"
  ]
  node [
    id 86
    label "fonetyka"
  ]
  node [
    id 87
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 88
    label "wokalizm"
  ]
  node [
    id 89
    label "liza&#263;"
  ]
  node [
    id 90
    label "s&#322;ownictwo"
  ]
  node [
    id 91
    label "napisa&#263;"
  ]
  node [
    id 92
    label "formalizowa&#263;"
  ]
  node [
    id 93
    label "natural_language"
  ]
  node [
    id 94
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 95
    label "stylik"
  ]
  node [
    id 96
    label "konsonantyzm"
  ]
  node [
    id 97
    label "urz&#261;dzenie"
  ]
  node [
    id 98
    label "ssa&#263;"
  ]
  node [
    id 99
    label "kod"
  ]
  node [
    id 100
    label "lizanie"
  ]
  node [
    id 101
    label "angielsko"
  ]
  node [
    id 102
    label "English"
  ]
  node [
    id 103
    label "anglicki"
  ]
  node [
    id 104
    label "angol"
  ]
  node [
    id 105
    label "j&#281;zyki_angielskie"
  ]
  node [
    id 106
    label "j&#281;zyk_zachodniogerma&#324;ski"
  ]
  node [
    id 107
    label "brytyjski"
  ]
  node [
    id 108
    label "po_angielsku"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 14
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
]
