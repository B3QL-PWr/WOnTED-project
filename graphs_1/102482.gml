graph [
  maxDegree 135
  minDegree 1
  meanDegree 2.0728155339805827
  density 0.005043346798006283
  graphCliqueNumber 3
  node [
    id 0
    label "jak"
    origin "text"
  ]
  node [
    id 1
    label "zwykle"
    origin "text"
  ]
  node [
    id 2
    label "zima"
    origin "text"
  ]
  node [
    id 3
    label "bywa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "przerwa"
    origin "text"
  ]
  node [
    id 5
    label "rozgrywka"
    origin "text"
  ]
  node [
    id 6
    label "ligowy"
    origin "text"
  ]
  node [
    id 7
    label "sk&#322;ania&#263;"
    origin "text"
  ]
  node [
    id 8
    label "wzi&#281;cie"
    origin "text"
  ]
  node [
    id 9
    label "udzia&#322;"
    origin "text"
  ]
  node [
    id 10
    label "turniej"
    origin "text"
  ]
  node [
    id 11
    label "halowy"
    origin "text"
  ]
  node [
    id 12
    label "pi&#322;ka"
    origin "text"
  ]
  node [
    id 13
    label "no&#380;ny"
    origin "text"
  ]
  node [
    id 14
    label "inaczej"
    origin "text"
  ]
  node [
    id 15
    label "by&#263;"
    origin "text"
  ]
  node [
    id 16
    label "przypadek"
    origin "text"
  ]
  node [
    id 17
    label "kotan"
    origin "text"
  ]
  node [
    id 18
    label "klub"
    origin "text"
  ]
  node [
    id 19
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 20
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 21
    label "litopada"
    origin "text"
  ]
  node [
    id 22
    label "dzi&#281;ki"
    origin "text"
  ]
  node [
    id 23
    label "uprzejmo&#347;&#263;"
    origin "text"
  ]
  node [
    id 24
    label "dyrekcja"
    origin "text"
  ]
  node [
    id 25
    label "szko&#322;a"
    origin "text"
  ]
  node [
    id 26
    label "podstawowy"
    origin "text"
  ]
  node [
    id 27
    label "&#322;&#243;d&#378;"
    origin "text"
  ]
  node [
    id 28
    label "choje&#324;ski"
    origin "text"
  ]
  node [
    id 29
    label "sportowy"
    origin "text"
  ]
  node [
    id 30
    label "trenowa&#263;"
    origin "text"
  ]
  node [
    id 31
    label "hala"
    origin "text"
  ]
  node [
    id 32
    label "przygotowywa&#263;"
    origin "text"
  ]
  node [
    id 33
    label "si&#281;"
    origin "text"
  ]
  node [
    id 34
    label "xxii"
    origin "text"
  ]
  node [
    id 35
    label "otwarty"
    origin "text"
  ]
  node [
    id 36
    label "mistrzostwa"
    origin "text"
  ]
  node [
    id 37
    label "region"
    origin "text"
  ]
  node [
    id 38
    label "&#322;&#243;dzkie"
    origin "text"
  ]
  node [
    id 39
    label "tkkf"
    origin "text"
  ]
  node [
    id 40
    label "golden"
    origin "text"
  ]
  node [
    id 41
    label "cup"
    origin "text"
  ]
  node [
    id 42
    label "byd&#322;o"
  ]
  node [
    id 43
    label "zobo"
  ]
  node [
    id 44
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 45
    label "yakalo"
  ]
  node [
    id 46
    label "dzo"
  ]
  node [
    id 47
    label "zwyk&#322;y"
  ]
  node [
    id 48
    label "cz&#281;sto"
  ]
  node [
    id 49
    label "pora_roku"
  ]
  node [
    id 50
    label "proceed"
  ]
  node [
    id 51
    label "pokazywa&#263;_si&#281;"
  ]
  node [
    id 52
    label "zdarza&#263;_si&#281;"
  ]
  node [
    id 53
    label "pozosta&#263;"
  ]
  node [
    id 54
    label "&#380;egna&#263;"
  ]
  node [
    id 55
    label "pauza"
  ]
  node [
    id 56
    label "miejsce"
  ]
  node [
    id 57
    label "przedzia&#322;"
  ]
  node [
    id 58
    label "czas"
  ]
  node [
    id 59
    label "euroliga"
  ]
  node [
    id 60
    label "zagrywka"
  ]
  node [
    id 61
    label "rewan&#380;owy"
  ]
  node [
    id 62
    label "faza"
  ]
  node [
    id 63
    label "interliga"
  ]
  node [
    id 64
    label "trafienie"
  ]
  node [
    id 65
    label "runda"
  ]
  node [
    id 66
    label "contest"
  ]
  node [
    id 67
    label "zach&#281;ca&#263;"
  ]
  node [
    id 68
    label "obni&#380;a&#263;"
  ]
  node [
    id 69
    label "refuse"
  ]
  node [
    id 70
    label "act"
  ]
  node [
    id 71
    label "wych&#281;do&#380;enie"
  ]
  node [
    id 72
    label "obj&#281;cie"
  ]
  node [
    id 73
    label "zniesienie"
  ]
  node [
    id 74
    label "odziedziczenie"
  ]
  node [
    id 75
    label "zaopatrzenie_si&#281;"
  ]
  node [
    id 76
    label "u&#380;ycie"
  ]
  node [
    id 77
    label "powodzenie"
  ]
  node [
    id 78
    label "wygranie"
  ]
  node [
    id 79
    label "zacz&#281;cie"
  ]
  node [
    id 80
    label "pickings"
  ]
  node [
    id 81
    label "ci&#261;gni&#281;cie"
  ]
  node [
    id 82
    label "uciekni&#281;cie"
  ]
  node [
    id 83
    label "branie"
  ]
  node [
    id 84
    label "poczytanie"
  ]
  node [
    id 85
    label "ruszenie"
  ]
  node [
    id 86
    label "take"
  ]
  node [
    id 87
    label "zrobienie"
  ]
  node [
    id 88
    label "capture"
  ]
  node [
    id 89
    label "niesienie"
  ]
  node [
    id 90
    label "wzi&#261;&#263;"
  ]
  node [
    id 91
    label "pokonanie"
  ]
  node [
    id 92
    label "wymienienie_si&#281;"
  ]
  node [
    id 93
    label "si&#281;gni&#281;cie"
  ]
  node [
    id 94
    label "wywiezienie"
  ]
  node [
    id 95
    label "dostanie"
  ]
  node [
    id 96
    label "pobranie"
  ]
  node [
    id 97
    label "wej&#347;cie"
  ]
  node [
    id 98
    label "bite"
  ]
  node [
    id 99
    label "udanie_si&#281;"
  ]
  node [
    id 100
    label "w&#322;o&#380;enie"
  ]
  node [
    id 101
    label "pozabieranie"
  ]
  node [
    id 102
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 103
    label "dmuchni&#281;cie"
  ]
  node [
    id 104
    label "wyruchanie"
  ]
  node [
    id 105
    label "przyj&#281;cie"
  ]
  node [
    id 106
    label "nakazanie"
  ]
  node [
    id 107
    label "wch&#322;oni&#281;cie"
  ]
  node [
    id 108
    label "otrzymanie"
  ]
  node [
    id 109
    label "kupienie"
  ]
  node [
    id 110
    label "obecno&#347;&#263;"
  ]
  node [
    id 111
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 112
    label "kwota"
  ]
  node [
    id 113
    label "ilo&#347;&#263;"
  ]
  node [
    id 114
    label "eliminacje"
  ]
  node [
    id 115
    label "zawody"
  ]
  node [
    id 116
    label "Wielki_Szlem"
  ]
  node [
    id 117
    label "drive"
  ]
  node [
    id 118
    label "impreza"
  ]
  node [
    id 119
    label "pojedynek"
  ]
  node [
    id 120
    label "tournament"
  ]
  node [
    id 121
    label "rywalizacja"
  ]
  node [
    id 122
    label "serwowanie"
  ]
  node [
    id 123
    label "sport"
  ]
  node [
    id 124
    label "do&#347;rodkowywa&#263;"
  ]
  node [
    id 125
    label "sport_zespo&#322;owy"
  ]
  node [
    id 126
    label "odbicie"
  ]
  node [
    id 127
    label "pi&#322;a_r&#281;czna"
  ]
  node [
    id 128
    label "rzucanka"
  ]
  node [
    id 129
    label "gra"
  ]
  node [
    id 130
    label "aut"
  ]
  node [
    id 131
    label "orb"
  ]
  node [
    id 132
    label "zaserwowanie"
  ]
  node [
    id 133
    label "do&#347;rodkowywanie"
  ]
  node [
    id 134
    label "zaserwowa&#263;"
  ]
  node [
    id 135
    label "kula"
  ]
  node [
    id 136
    label "&#347;wieca"
  ]
  node [
    id 137
    label "serwowa&#263;"
  ]
  node [
    id 138
    label "musket_ball"
  ]
  node [
    id 139
    label "niestandardowo"
  ]
  node [
    id 140
    label "inny"
  ]
  node [
    id 141
    label "si&#281;ga&#263;"
  ]
  node [
    id 142
    label "trwa&#263;"
  ]
  node [
    id 143
    label "stan"
  ]
  node [
    id 144
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 145
    label "stand"
  ]
  node [
    id 146
    label "mie&#263;_miejsce"
  ]
  node [
    id 147
    label "uczestniczy&#263;"
  ]
  node [
    id 148
    label "chodzi&#263;"
  ]
  node [
    id 149
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 150
    label "equal"
  ]
  node [
    id 151
    label "pacjent"
  ]
  node [
    id 152
    label "kategoria_gramatyczna"
  ]
  node [
    id 153
    label "schorzenie"
  ]
  node [
    id 154
    label "przeznaczenie"
  ]
  node [
    id 155
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 156
    label "wydarzenie"
  ]
  node [
    id 157
    label "happening"
  ]
  node [
    id 158
    label "przyk&#322;ad"
  ]
  node [
    id 159
    label "jednostka_osadnicza"
  ]
  node [
    id 160
    label "society"
  ]
  node [
    id 161
    label "jakobini"
  ]
  node [
    id 162
    label "klubista"
  ]
  node [
    id 163
    label "stowarzyszenie"
  ]
  node [
    id 164
    label "lokal"
  ]
  node [
    id 165
    label "od&#322;am"
  ]
  node [
    id 166
    label "siedziba"
  ]
  node [
    id 167
    label "bar"
  ]
  node [
    id 168
    label "czyn"
  ]
  node [
    id 169
    label "service"
  ]
  node [
    id 170
    label "favor"
  ]
  node [
    id 171
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 172
    label "&#380;yczliwo&#347;&#263;"
  ]
  node [
    id 173
    label "jednostka_administracyjna"
  ]
  node [
    id 174
    label "urz&#261;d"
  ]
  node [
    id 175
    label "biuro"
  ]
  node [
    id 176
    label "technika"
  ]
  node [
    id 177
    label "w&#322;adza"
  ]
  node [
    id 178
    label "directorship"
  ]
  node [
    id 179
    label "kierownictwo"
  ]
  node [
    id 180
    label "lead"
  ]
  node [
    id 181
    label "praca"
  ]
  node [
    id 182
    label "Mickiewicz"
  ]
  node [
    id 183
    label "szkolenie"
  ]
  node [
    id 184
    label "przepisa&#263;"
  ]
  node [
    id 185
    label "lesson"
  ]
  node [
    id 186
    label "grupa"
  ]
  node [
    id 187
    label "praktyka"
  ]
  node [
    id 188
    label "metoda"
  ]
  node [
    id 189
    label "niepokalanki"
  ]
  node [
    id 190
    label "kara"
  ]
  node [
    id 191
    label "zda&#263;"
  ]
  node [
    id 192
    label "form"
  ]
  node [
    id 193
    label "kwalifikacje"
  ]
  node [
    id 194
    label "system"
  ]
  node [
    id 195
    label "przepisanie"
  ]
  node [
    id 196
    label "sztuba"
  ]
  node [
    id 197
    label "wiedza"
  ]
  node [
    id 198
    label "stopek"
  ]
  node [
    id 199
    label "school"
  ]
  node [
    id 200
    label "absolwent"
  ]
  node [
    id 201
    label "urszulanki"
  ]
  node [
    id 202
    label "gabinet"
  ]
  node [
    id 203
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 204
    label "ideologia"
  ]
  node [
    id 205
    label "lekcja"
  ]
  node [
    id 206
    label "muzyka"
  ]
  node [
    id 207
    label "podr&#281;cznik"
  ]
  node [
    id 208
    label "zdanie"
  ]
  node [
    id 209
    label "sekretariat"
  ]
  node [
    id 210
    label "nauka"
  ]
  node [
    id 211
    label "do&#347;wiadczenie"
  ]
  node [
    id 212
    label "tablica"
  ]
  node [
    id 213
    label "teren_szko&#322;y"
  ]
  node [
    id 214
    label "instytucja"
  ]
  node [
    id 215
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 216
    label "skolaryzacja"
  ]
  node [
    id 217
    label "&#322;awa_szkolna"
  ]
  node [
    id 218
    label "klasa"
  ]
  node [
    id 219
    label "pocz&#261;tkowy"
  ]
  node [
    id 220
    label "podstawowo"
  ]
  node [
    id 221
    label "najwa&#380;niejszy"
  ]
  node [
    id 222
    label "niezaawansowany"
  ]
  node [
    id 223
    label "regaty"
  ]
  node [
    id 224
    label "statek"
  ]
  node [
    id 225
    label "spalin&#243;wka"
  ]
  node [
    id 226
    label "pok&#322;ad"
  ]
  node [
    id 227
    label "ster"
  ]
  node [
    id 228
    label "kratownica"
  ]
  node [
    id 229
    label "pojazd_niemechaniczny"
  ]
  node [
    id 230
    label "drzewce"
  ]
  node [
    id 231
    label "specjalny"
  ]
  node [
    id 232
    label "na_sportowo"
  ]
  node [
    id 233
    label "uczciwy"
  ]
  node [
    id 234
    label "wygodny"
  ]
  node [
    id 235
    label "pe&#322;ny"
  ]
  node [
    id 236
    label "sportowo"
  ]
  node [
    id 237
    label "doskonali&#263;"
  ]
  node [
    id 238
    label "&#263;wiczy&#263;"
  ]
  node [
    id 239
    label "ulepsza&#263;"
  ]
  node [
    id 240
    label "coach"
  ]
  node [
    id 241
    label "hone"
  ]
  node [
    id 242
    label "szkoli&#263;"
  ]
  node [
    id 243
    label "discipline"
  ]
  node [
    id 244
    label "kopalnia"
  ]
  node [
    id 245
    label "halizna"
  ]
  node [
    id 246
    label "fabryka"
  ]
  node [
    id 247
    label "huta"
  ]
  node [
    id 248
    label "pastwisko"
  ]
  node [
    id 249
    label "budynek"
  ]
  node [
    id 250
    label "pi&#281;tro"
  ]
  node [
    id 251
    label "oczyszczalnia"
  ]
  node [
    id 252
    label "dworzec"
  ]
  node [
    id 253
    label "lotnisko"
  ]
  node [
    id 254
    label "pomieszczenie"
  ]
  node [
    id 255
    label "wykonywa&#263;"
  ]
  node [
    id 256
    label "sposobi&#263;"
  ]
  node [
    id 257
    label "arrange"
  ]
  node [
    id 258
    label "pryczy&#263;"
  ]
  node [
    id 259
    label "train"
  ]
  node [
    id 260
    label "robi&#263;"
  ]
  node [
    id 261
    label "wytwarza&#263;"
  ]
  node [
    id 262
    label "usposabia&#263;"
  ]
  node [
    id 263
    label "ewidentny"
  ]
  node [
    id 264
    label "bezpo&#347;redni"
  ]
  node [
    id 265
    label "otwarcie"
  ]
  node [
    id 266
    label "nieograniczony"
  ]
  node [
    id 267
    label "zdecydowany"
  ]
  node [
    id 268
    label "gotowy"
  ]
  node [
    id 269
    label "aktualny"
  ]
  node [
    id 270
    label "prostoduszny"
  ]
  node [
    id 271
    label "jawnie"
  ]
  node [
    id 272
    label "otworzysty"
  ]
  node [
    id 273
    label "dost&#281;pny"
  ]
  node [
    id 274
    label "publiczny"
  ]
  node [
    id 275
    label "aktywny"
  ]
  node [
    id 276
    label "kontaktowy"
  ]
  node [
    id 277
    label "championship"
  ]
  node [
    id 278
    label "Formu&#322;a_1"
  ]
  node [
    id 279
    label "Skandynawia"
  ]
  node [
    id 280
    label "Yorkshire"
  ]
  node [
    id 281
    label "Kaukaz"
  ]
  node [
    id 282
    label "Kaszmir"
  ]
  node [
    id 283
    label "Toskania"
  ]
  node [
    id 284
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 285
    label "&#321;emkowszczyzna"
  ]
  node [
    id 286
    label "obszar"
  ]
  node [
    id 287
    label "Amhara"
  ]
  node [
    id 288
    label "Lombardia"
  ]
  node [
    id 289
    label "Podbeskidzie"
  ]
  node [
    id 290
    label "okr&#281;g"
  ]
  node [
    id 291
    label "Chiny_Wschodnie"
  ]
  node [
    id 292
    label "Kalabria"
  ]
  node [
    id 293
    label "Tyrol"
  ]
  node [
    id 294
    label "Pamir"
  ]
  node [
    id 295
    label "Lubelszczyzna"
  ]
  node [
    id 296
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 297
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 298
    label "&#379;ywiecczyzna"
  ]
  node [
    id 299
    label "Europa_Wschodnia"
  ]
  node [
    id 300
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 301
    label "Chiny_Zachodnie"
  ]
  node [
    id 302
    label "Zabajkale"
  ]
  node [
    id 303
    label "Kaszuby"
  ]
  node [
    id 304
    label "Bo&#347;nia"
  ]
  node [
    id 305
    label "Noworosja"
  ]
  node [
    id 306
    label "Ba&#322;kany"
  ]
  node [
    id 307
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 308
    label "Anglia"
  ]
  node [
    id 309
    label "Kielecczyzna"
  ]
  node [
    id 310
    label "Krajina"
  ]
  node [
    id 311
    label "Pomorze_Zachodnie"
  ]
  node [
    id 312
    label "Opolskie"
  ]
  node [
    id 313
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 314
    label "Ko&#322;yma"
  ]
  node [
    id 315
    label "Oksytania"
  ]
  node [
    id 316
    label "country"
  ]
  node [
    id 317
    label "Syjon"
  ]
  node [
    id 318
    label "Kociewie"
  ]
  node [
    id 319
    label "Huculszczyzna"
  ]
  node [
    id 320
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 321
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 322
    label "Bawaria"
  ]
  node [
    id 323
    label "Maghreb"
  ]
  node [
    id 324
    label "Bory_Tucholskie"
  ]
  node [
    id 325
    label "Europa_Zachodnia"
  ]
  node [
    id 326
    label "Kerala"
  ]
  node [
    id 327
    label "Burgundia"
  ]
  node [
    id 328
    label "Podhale"
  ]
  node [
    id 329
    label "Kabylia"
  ]
  node [
    id 330
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 331
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 332
    label "Ma&#322;opolska"
  ]
  node [
    id 333
    label "Polesie"
  ]
  node [
    id 334
    label "Liguria"
  ]
  node [
    id 335
    label "&#321;&#243;dzkie"
  ]
  node [
    id 336
    label "Palestyna"
  ]
  node [
    id 337
    label "Bojkowszczyzna"
  ]
  node [
    id 338
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 339
    label "Karaiby"
  ]
  node [
    id 340
    label "S&#261;decczyzna"
  ]
  node [
    id 341
    label "Sand&#380;ak"
  ]
  node [
    id 342
    label "Nadrenia"
  ]
  node [
    id 343
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 344
    label "Zakarpacie"
  ]
  node [
    id 345
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 346
    label "Zag&#243;rze"
  ]
  node [
    id 347
    label "Andaluzja"
  ]
  node [
    id 348
    label "&#379;mud&#378;"
  ]
  node [
    id 349
    label "Turkiestan"
  ]
  node [
    id 350
    label "Naddniestrze"
  ]
  node [
    id 351
    label "Hercegowina"
  ]
  node [
    id 352
    label "Opolszczyzna"
  ]
  node [
    id 353
    label "Lotaryngia"
  ]
  node [
    id 354
    label "Afryka_Wschodnia"
  ]
  node [
    id 355
    label "Szlezwik"
  ]
  node [
    id 356
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 357
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 358
    label "Mazowsze"
  ]
  node [
    id 359
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 360
    label "Afryka_Zachodnia"
  ]
  node [
    id 361
    label "Galicja"
  ]
  node [
    id 362
    label "Szkocja"
  ]
  node [
    id 363
    label "Walia"
  ]
  node [
    id 364
    label "Powi&#347;le"
  ]
  node [
    id 365
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 366
    label "Zamojszczyzna"
  ]
  node [
    id 367
    label "Kujawy"
  ]
  node [
    id 368
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 369
    label "Podlasie"
  ]
  node [
    id 370
    label "Laponia"
  ]
  node [
    id 371
    label "Umbria"
  ]
  node [
    id 372
    label "Mezoameryka"
  ]
  node [
    id 373
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 374
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 375
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 376
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 377
    label "Kraina"
  ]
  node [
    id 378
    label "Kurdystan"
  ]
  node [
    id 379
    label "Flandria"
  ]
  node [
    id 380
    label "Kampania"
  ]
  node [
    id 381
    label "Armagnac"
  ]
  node [
    id 382
    label "Polinezja"
  ]
  node [
    id 383
    label "Warmia"
  ]
  node [
    id 384
    label "Wielkopolska"
  ]
  node [
    id 385
    label "Bordeaux"
  ]
  node [
    id 386
    label "Lauda"
  ]
  node [
    id 387
    label "Mazury"
  ]
  node [
    id 388
    label "Podkarpacie"
  ]
  node [
    id 389
    label "Oceania"
  ]
  node [
    id 390
    label "Lasko"
  ]
  node [
    id 391
    label "Amazonia"
  ]
  node [
    id 392
    label "podregion"
  ]
  node [
    id 393
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 394
    label "Kurpie"
  ]
  node [
    id 395
    label "Tonkin"
  ]
  node [
    id 396
    label "Azja_Wschodnia"
  ]
  node [
    id 397
    label "Mikronezja"
  ]
  node [
    id 398
    label "Ukraina_Zachodnia"
  ]
  node [
    id 399
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 400
    label "subregion"
  ]
  node [
    id 401
    label "Turyngia"
  ]
  node [
    id 402
    label "Baszkiria"
  ]
  node [
    id 403
    label "Apulia"
  ]
  node [
    id 404
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 405
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 406
    label "Indochiny"
  ]
  node [
    id 407
    label "Biskupizna"
  ]
  node [
    id 408
    label "Lubuskie"
  ]
  node [
    id 409
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 410
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 411
    label "jab&#322;ko"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 33
  ]
  edge [
    source 9
    target 34
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 65
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 10
    target 121
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 35
  ]
  edge [
    source 11
    target 36
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 60
  ]
  edge [
    source 12
    target 122
  ]
  edge [
    source 12
    target 123
  ]
  edge [
    source 12
    target 124
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 12
    target 131
  ]
  edge [
    source 12
    target 132
  ]
  edge [
    source 12
    target 133
  ]
  edge [
    source 12
    target 134
  ]
  edge [
    source 12
    target 135
  ]
  edge [
    source 12
    target 136
  ]
  edge [
    source 12
    target 137
  ]
  edge [
    source 12
    target 138
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 139
  ]
  edge [
    source 14
    target 140
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 141
  ]
  edge [
    source 15
    target 142
  ]
  edge [
    source 15
    target 110
  ]
  edge [
    source 15
    target 143
  ]
  edge [
    source 15
    target 144
  ]
  edge [
    source 15
    target 145
  ]
  edge [
    source 15
    target 146
  ]
  edge [
    source 15
    target 147
  ]
  edge [
    source 15
    target 148
  ]
  edge [
    source 15
    target 149
  ]
  edge [
    source 15
    target 150
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 151
  ]
  edge [
    source 16
    target 152
  ]
  edge [
    source 16
    target 153
  ]
  edge [
    source 16
    target 154
  ]
  edge [
    source 16
    target 155
  ]
  edge [
    source 16
    target 156
  ]
  edge [
    source 16
    target 157
  ]
  edge [
    source 16
    target 158
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 159
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 28
  ]
  edge [
    source 18
    target 29
  ]
  edge [
    source 18
    target 160
  ]
  edge [
    source 18
    target 161
  ]
  edge [
    source 18
    target 162
  ]
  edge [
    source 18
    target 163
  ]
  edge [
    source 18
    target 164
  ]
  edge [
    source 18
    target 165
  ]
  edge [
    source 18
    target 166
  ]
  edge [
    source 18
    target 167
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 168
  ]
  edge [
    source 23
    target 169
  ]
  edge [
    source 23
    target 170
  ]
  edge [
    source 23
    target 171
  ]
  edge [
    source 23
    target 172
  ]
  edge [
    source 23
    target 36
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 173
  ]
  edge [
    source 24
    target 174
  ]
  edge [
    source 24
    target 175
  ]
  edge [
    source 24
    target 176
  ]
  edge [
    source 24
    target 177
  ]
  edge [
    source 24
    target 178
  ]
  edge [
    source 24
    target 179
  ]
  edge [
    source 24
    target 180
  ]
  edge [
    source 24
    target 181
  ]
  edge [
    source 24
    target 166
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 182
  ]
  edge [
    source 25
    target 58
  ]
  edge [
    source 25
    target 183
  ]
  edge [
    source 25
    target 184
  ]
  edge [
    source 25
    target 185
  ]
  edge [
    source 25
    target 186
  ]
  edge [
    source 25
    target 187
  ]
  edge [
    source 25
    target 188
  ]
  edge [
    source 25
    target 189
  ]
  edge [
    source 25
    target 190
  ]
  edge [
    source 25
    target 191
  ]
  edge [
    source 25
    target 192
  ]
  edge [
    source 25
    target 193
  ]
  edge [
    source 25
    target 194
  ]
  edge [
    source 25
    target 195
  ]
  edge [
    source 25
    target 196
  ]
  edge [
    source 25
    target 197
  ]
  edge [
    source 25
    target 198
  ]
  edge [
    source 25
    target 199
  ]
  edge [
    source 25
    target 200
  ]
  edge [
    source 25
    target 201
  ]
  edge [
    source 25
    target 202
  ]
  edge [
    source 25
    target 203
  ]
  edge [
    source 25
    target 204
  ]
  edge [
    source 25
    target 205
  ]
  edge [
    source 25
    target 206
  ]
  edge [
    source 25
    target 207
  ]
  edge [
    source 25
    target 208
  ]
  edge [
    source 25
    target 166
  ]
  edge [
    source 25
    target 209
  ]
  edge [
    source 25
    target 210
  ]
  edge [
    source 25
    target 211
  ]
  edge [
    source 25
    target 212
  ]
  edge [
    source 25
    target 213
  ]
  edge [
    source 25
    target 214
  ]
  edge [
    source 25
    target 215
  ]
  edge [
    source 25
    target 216
  ]
  edge [
    source 25
    target 217
  ]
  edge [
    source 25
    target 218
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 219
  ]
  edge [
    source 26
    target 220
  ]
  edge [
    source 26
    target 221
  ]
  edge [
    source 26
    target 222
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 29
  ]
  edge [
    source 27
    target 30
  ]
  edge [
    source 27
    target 36
  ]
  edge [
    source 27
    target 37
  ]
  edge [
    source 27
    target 223
  ]
  edge [
    source 27
    target 224
  ]
  edge [
    source 27
    target 225
  ]
  edge [
    source 27
    target 226
  ]
  edge [
    source 27
    target 227
  ]
  edge [
    source 27
    target 228
  ]
  edge [
    source 27
    target 229
  ]
  edge [
    source 27
    target 230
  ]
  edge [
    source 29
    target 231
  ]
  edge [
    source 29
    target 232
  ]
  edge [
    source 29
    target 233
  ]
  edge [
    source 29
    target 234
  ]
  edge [
    source 29
    target 235
  ]
  edge [
    source 29
    target 236
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 237
  ]
  edge [
    source 30
    target 238
  ]
  edge [
    source 30
    target 239
  ]
  edge [
    source 30
    target 240
  ]
  edge [
    source 30
    target 241
  ]
  edge [
    source 30
    target 242
  ]
  edge [
    source 30
    target 243
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 244
  ]
  edge [
    source 31
    target 245
  ]
  edge [
    source 31
    target 246
  ]
  edge [
    source 31
    target 247
  ]
  edge [
    source 31
    target 248
  ]
  edge [
    source 31
    target 249
  ]
  edge [
    source 31
    target 250
  ]
  edge [
    source 31
    target 251
  ]
  edge [
    source 31
    target 252
  ]
  edge [
    source 31
    target 253
  ]
  edge [
    source 31
    target 254
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 255
  ]
  edge [
    source 32
    target 256
  ]
  edge [
    source 32
    target 257
  ]
  edge [
    source 32
    target 258
  ]
  edge [
    source 32
    target 259
  ]
  edge [
    source 32
    target 260
  ]
  edge [
    source 32
    target 261
  ]
  edge [
    source 32
    target 242
  ]
  edge [
    source 32
    target 262
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 35
    target 263
  ]
  edge [
    source 35
    target 264
  ]
  edge [
    source 35
    target 265
  ]
  edge [
    source 35
    target 266
  ]
  edge [
    source 35
    target 267
  ]
  edge [
    source 35
    target 268
  ]
  edge [
    source 35
    target 269
  ]
  edge [
    source 35
    target 270
  ]
  edge [
    source 35
    target 271
  ]
  edge [
    source 35
    target 272
  ]
  edge [
    source 35
    target 273
  ]
  edge [
    source 35
    target 274
  ]
  edge [
    source 35
    target 275
  ]
  edge [
    source 35
    target 276
  ]
  edge [
    source 35
    target 38
  ]
  edge [
    source 36
    target 277
  ]
  edge [
    source 36
    target 278
  ]
  edge [
    source 36
    target 115
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 279
  ]
  edge [
    source 37
    target 280
  ]
  edge [
    source 37
    target 281
  ]
  edge [
    source 37
    target 282
  ]
  edge [
    source 37
    target 283
  ]
  edge [
    source 37
    target 284
  ]
  edge [
    source 37
    target 285
  ]
  edge [
    source 37
    target 286
  ]
  edge [
    source 37
    target 287
  ]
  edge [
    source 37
    target 288
  ]
  edge [
    source 37
    target 289
  ]
  edge [
    source 37
    target 290
  ]
  edge [
    source 37
    target 291
  ]
  edge [
    source 37
    target 292
  ]
  edge [
    source 37
    target 293
  ]
  edge [
    source 37
    target 294
  ]
  edge [
    source 37
    target 295
  ]
  edge [
    source 37
    target 296
  ]
  edge [
    source 37
    target 297
  ]
  edge [
    source 37
    target 298
  ]
  edge [
    source 37
    target 299
  ]
  edge [
    source 37
    target 300
  ]
  edge [
    source 37
    target 301
  ]
  edge [
    source 37
    target 302
  ]
  edge [
    source 37
    target 303
  ]
  edge [
    source 37
    target 304
  ]
  edge [
    source 37
    target 305
  ]
  edge [
    source 37
    target 306
  ]
  edge [
    source 37
    target 307
  ]
  edge [
    source 37
    target 308
  ]
  edge [
    source 37
    target 309
  ]
  edge [
    source 37
    target 310
  ]
  edge [
    source 37
    target 311
  ]
  edge [
    source 37
    target 312
  ]
  edge [
    source 37
    target 313
  ]
  edge [
    source 37
    target 314
  ]
  edge [
    source 37
    target 315
  ]
  edge [
    source 37
    target 316
  ]
  edge [
    source 37
    target 317
  ]
  edge [
    source 37
    target 318
  ]
  edge [
    source 37
    target 319
  ]
  edge [
    source 37
    target 320
  ]
  edge [
    source 37
    target 321
  ]
  edge [
    source 37
    target 322
  ]
  edge [
    source 37
    target 323
  ]
  edge [
    source 37
    target 324
  ]
  edge [
    source 37
    target 325
  ]
  edge [
    source 37
    target 326
  ]
  edge [
    source 37
    target 327
  ]
  edge [
    source 37
    target 328
  ]
  edge [
    source 37
    target 329
  ]
  edge [
    source 37
    target 330
  ]
  edge [
    source 37
    target 331
  ]
  edge [
    source 37
    target 332
  ]
  edge [
    source 37
    target 333
  ]
  edge [
    source 37
    target 334
  ]
  edge [
    source 37
    target 335
  ]
  edge [
    source 37
    target 336
  ]
  edge [
    source 37
    target 337
  ]
  edge [
    source 37
    target 338
  ]
  edge [
    source 37
    target 339
  ]
  edge [
    source 37
    target 340
  ]
  edge [
    source 37
    target 341
  ]
  edge [
    source 37
    target 342
  ]
  edge [
    source 37
    target 343
  ]
  edge [
    source 37
    target 344
  ]
  edge [
    source 37
    target 345
  ]
  edge [
    source 37
    target 346
  ]
  edge [
    source 37
    target 347
  ]
  edge [
    source 37
    target 348
  ]
  edge [
    source 37
    target 349
  ]
  edge [
    source 37
    target 350
  ]
  edge [
    source 37
    target 351
  ]
  edge [
    source 37
    target 352
  ]
  edge [
    source 37
    target 173
  ]
  edge [
    source 37
    target 353
  ]
  edge [
    source 37
    target 354
  ]
  edge [
    source 37
    target 355
  ]
  edge [
    source 37
    target 356
  ]
  edge [
    source 37
    target 357
  ]
  edge [
    source 37
    target 358
  ]
  edge [
    source 37
    target 359
  ]
  edge [
    source 37
    target 360
  ]
  edge [
    source 37
    target 361
  ]
  edge [
    source 37
    target 362
  ]
  edge [
    source 37
    target 363
  ]
  edge [
    source 37
    target 364
  ]
  edge [
    source 37
    target 365
  ]
  edge [
    source 37
    target 366
  ]
  edge [
    source 37
    target 367
  ]
  edge [
    source 37
    target 368
  ]
  edge [
    source 37
    target 369
  ]
  edge [
    source 37
    target 370
  ]
  edge [
    source 37
    target 371
  ]
  edge [
    source 37
    target 372
  ]
  edge [
    source 37
    target 373
  ]
  edge [
    source 37
    target 374
  ]
  edge [
    source 37
    target 375
  ]
  edge [
    source 37
    target 376
  ]
  edge [
    source 37
    target 377
  ]
  edge [
    source 37
    target 378
  ]
  edge [
    source 37
    target 379
  ]
  edge [
    source 37
    target 380
  ]
  edge [
    source 37
    target 381
  ]
  edge [
    source 37
    target 382
  ]
  edge [
    source 37
    target 383
  ]
  edge [
    source 37
    target 384
  ]
  edge [
    source 37
    target 385
  ]
  edge [
    source 37
    target 386
  ]
  edge [
    source 37
    target 387
  ]
  edge [
    source 37
    target 388
  ]
  edge [
    source 37
    target 389
  ]
  edge [
    source 37
    target 390
  ]
  edge [
    source 37
    target 391
  ]
  edge [
    source 37
    target 392
  ]
  edge [
    source 37
    target 393
  ]
  edge [
    source 37
    target 394
  ]
  edge [
    source 37
    target 395
  ]
  edge [
    source 37
    target 396
  ]
  edge [
    source 37
    target 397
  ]
  edge [
    source 37
    target 398
  ]
  edge [
    source 37
    target 399
  ]
  edge [
    source 37
    target 400
  ]
  edge [
    source 37
    target 401
  ]
  edge [
    source 37
    target 402
  ]
  edge [
    source 37
    target 403
  ]
  edge [
    source 37
    target 404
  ]
  edge [
    source 37
    target 405
  ]
  edge [
    source 37
    target 406
  ]
  edge [
    source 37
    target 407
  ]
  edge [
    source 37
    target 408
  ]
  edge [
    source 37
    target 409
  ]
  edge [
    source 37
    target 410
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 411
  ]
]
