graph [
  maxDegree 28
  minDegree 1
  meanDegree 2.152744630071599
  density 0.005150106770506218
  graphCliqueNumber 3
  node [
    id 0
    label "wies&#322;aw"
    origin "text"
  ]
  node [
    id 1
    label "nowicki"
    origin "text"
  ]
  node [
    id 2
    label "d&#322;ugo"
    origin "text"
  ]
  node [
    id 3
    label "sta&#263;"
    origin "text"
  ]
  node [
    id 4
    label "obrona"
    origin "text"
  ]
  node [
    id 5
    label "polski"
    origin "text"
  ]
  node [
    id 6
    label "przyroda"
    origin "text"
  ]
  node [
    id 7
    label "walczy&#263;"
    origin "text"
  ]
  node [
    id 8
    label "r&#243;&#380;ny"
    origin "text"
  ]
  node [
    id 9
    label "samorz&#261;d"
    origin "text"
  ]
  node [
    id 10
    label "polska"
    origin "text"
  ]
  node [
    id 11
    label "przez"
    origin "text"
  ]
  node [
    id 12
    label "wiele"
    origin "text"
  ]
  node [
    id 13
    label "lata"
    origin "text"
  ]
  node [
    id 14
    label "blokowa&#263;"
    origin "text"
  ]
  node [
    id 15
    label "budowa"
    origin "text"
  ]
  node [
    id 16
    label "most"
    origin "text"
  ]
  node [
    id 17
    label "p&#243;&#322;nocny"
    origin "text"
  ]
  node [
    id 18
    label "warszawa"
    origin "text"
  ]
  node [
    id 19
    label "kiedy"
    origin "text"
  ]
  node [
    id 20
    label "ulec"
    origin "text"
  ]
  node [
    id 21
    label "postanowi&#263;"
    origin "text"
  ]
  node [
    id 22
    label "skutecznie"
    origin "text"
  ]
  node [
    id 23
    label "doradzi&#263;"
    origin "text"
  ]
  node [
    id 24
    label "jak"
    origin "text"
  ]
  node [
    id 25
    label "usuwa&#263;"
    origin "text"
  ]
  node [
    id 26
    label "drzewo"
    origin "text"
  ]
  node [
    id 27
    label "zgodnie"
    origin "text"
  ]
  node [
    id 28
    label "natura"
    origin "text"
  ]
  node [
    id 29
    label "ustali&#263;"
    origin "text"
  ]
  node [
    id 30
    label "miejsce"
    origin "text"
  ]
  node [
    id 31
    label "wystawi&#263;"
    origin "text"
  ]
  node [
    id 32
    label "budka"
    origin "text"
  ]
  node [
    id 33
    label "l&#281;gowy"
    origin "text"
  ]
  node [
    id 34
    label "dla"
    origin "text"
  ]
  node [
    id 35
    label "ptak"
    origin "text"
  ]
  node [
    id 36
    label "dosta&#263;"
    origin "text"
  ]
  node [
    id 37
    label "wynagrodzenie"
    origin "text"
  ]
  node [
    id 38
    label "przepi&#281;kny"
    origin "text"
  ]
  node [
    id 39
    label "artyku&#322;"
    origin "text"
  ]
  node [
    id 40
    label "nasz"
    origin "text"
  ]
  node [
    id 41
    label "naczelny"
    origin "text"
  ]
  node [
    id 42
    label "narodowy"
    origin "text"
  ]
  node [
    id 43
    label "gazeta"
    origin "text"
  ]
  node [
    id 44
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 45
    label "skrytykowa&#263;"
    origin "text"
  ]
  node [
    id 46
    label "pob&#243;r"
    origin "text"
  ]
  node [
    id 47
    label "zwa&#263;"
    origin "text"
  ]
  node [
    id 48
    label "redaktor"
    origin "text"
  ]
  node [
    id 49
    label "jako"
    origin "text"
  ]
  node [
    id 50
    label "haracz"
    origin "text"
  ]
  node [
    id 51
    label "d&#322;ugi"
  ]
  node [
    id 52
    label "pozostawa&#263;"
  ]
  node [
    id 53
    label "trwa&#263;"
  ]
  node [
    id 54
    label "by&#263;"
  ]
  node [
    id 55
    label "wystarcza&#263;"
  ]
  node [
    id 56
    label "obowi&#261;zywa&#263;"
  ]
  node [
    id 57
    label "czeka&#263;"
  ]
  node [
    id 58
    label "stand"
  ]
  node [
    id 59
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 60
    label "mieszka&#263;"
  ]
  node [
    id 61
    label "wystarczy&#263;"
  ]
  node [
    id 62
    label "sprawowa&#263;"
  ]
  node [
    id 63
    label "przebywa&#263;"
  ]
  node [
    id 64
    label "kosztowa&#263;"
  ]
  node [
    id 65
    label "undertaking"
  ]
  node [
    id 66
    label "wystawa&#263;"
  ]
  node [
    id 67
    label "base"
  ]
  node [
    id 68
    label "digest"
  ]
  node [
    id 69
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 70
    label "manewr"
  ]
  node [
    id 71
    label "reakcja"
  ]
  node [
    id 72
    label "auspices"
  ]
  node [
    id 73
    label "mecz"
  ]
  node [
    id 74
    label "poparcie"
  ]
  node [
    id 75
    label "ochrona"
  ]
  node [
    id 76
    label "s&#261;d"
  ]
  node [
    id 77
    label "defensive_structure"
  ]
  node [
    id 78
    label "liga"
  ]
  node [
    id 79
    label "egzamin"
  ]
  node [
    id 80
    label "gracz"
  ]
  node [
    id 81
    label "defense"
  ]
  node [
    id 82
    label "walka"
  ]
  node [
    id 83
    label "post&#281;powanie"
  ]
  node [
    id 84
    label "wojsko"
  ]
  node [
    id 85
    label "protection"
  ]
  node [
    id 86
    label "poj&#281;cie"
  ]
  node [
    id 87
    label "guard_duty"
  ]
  node [
    id 88
    label "strona"
  ]
  node [
    id 89
    label "sp&#243;r"
  ]
  node [
    id 90
    label "gra"
  ]
  node [
    id 91
    label "lacki"
  ]
  node [
    id 92
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 93
    label "przedmiot"
  ]
  node [
    id 94
    label "sztajer"
  ]
  node [
    id 95
    label "drabant"
  ]
  node [
    id 96
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 97
    label "polak"
  ]
  node [
    id 98
    label "pierogi_ruskie"
  ]
  node [
    id 99
    label "krakowiak"
  ]
  node [
    id 100
    label "Polish"
  ]
  node [
    id 101
    label "j&#281;zyk"
  ]
  node [
    id 102
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 103
    label "oberek"
  ]
  node [
    id 104
    label "po_polsku"
  ]
  node [
    id 105
    label "mazur"
  ]
  node [
    id 106
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 107
    label "chodzony"
  ]
  node [
    id 108
    label "skoczny"
  ]
  node [
    id 109
    label "ryba_po_grecku"
  ]
  node [
    id 110
    label "goniony"
  ]
  node [
    id 111
    label "polsko"
  ]
  node [
    id 112
    label "biota"
  ]
  node [
    id 113
    label "wszechstworzenie"
  ]
  node [
    id 114
    label "obiekt_naturalny"
  ]
  node [
    id 115
    label "Ziemia"
  ]
  node [
    id 116
    label "fauna"
  ]
  node [
    id 117
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 118
    label "stw&#243;r"
  ]
  node [
    id 119
    label "ekosystem"
  ]
  node [
    id 120
    label "rzecz"
  ]
  node [
    id 121
    label "teren"
  ]
  node [
    id 122
    label "environment"
  ]
  node [
    id 123
    label "woda"
  ]
  node [
    id 124
    label "przyra"
  ]
  node [
    id 125
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 126
    label "mikrokosmos"
  ]
  node [
    id 127
    label "zmaga&#263;_si&#281;"
  ]
  node [
    id 128
    label "cope"
  ]
  node [
    id 129
    label "contend"
  ]
  node [
    id 130
    label "zawody"
  ]
  node [
    id 131
    label "opowiada&#263;_si&#281;"
  ]
  node [
    id 132
    label "dzia&#322;a&#263;"
  ]
  node [
    id 133
    label "wrestle"
  ]
  node [
    id 134
    label "sprzeciwia&#263;_si&#281;"
  ]
  node [
    id 135
    label "robi&#263;"
  ]
  node [
    id 136
    label "my&#347;lenie"
  ]
  node [
    id 137
    label "argue"
  ]
  node [
    id 138
    label "stara&#263;_si&#281;"
  ]
  node [
    id 139
    label "fight"
  ]
  node [
    id 140
    label "opiera&#263;_si&#281;"
  ]
  node [
    id 141
    label "r&#243;&#380;nie"
  ]
  node [
    id 142
    label "inny"
  ]
  node [
    id 143
    label "jaki&#347;"
  ]
  node [
    id 144
    label "autonomy"
  ]
  node [
    id 145
    label "organ"
  ]
  node [
    id 146
    label "wiela"
  ]
  node [
    id 147
    label "du&#380;y"
  ]
  node [
    id 148
    label "summer"
  ]
  node [
    id 149
    label "czas"
  ]
  node [
    id 150
    label "throng"
  ]
  node [
    id 151
    label "zajmowa&#263;"
  ]
  node [
    id 152
    label "przeszkadza&#263;"
  ]
  node [
    id 153
    label "zablokowywa&#263;"
  ]
  node [
    id 154
    label "sk&#322;ada&#263;"
  ]
  node [
    id 155
    label "interlock"
  ]
  node [
    id 156
    label "wstrzymywa&#263;"
  ]
  node [
    id 157
    label "unieruchamia&#263;"
  ]
  node [
    id 158
    label "kiblowa&#263;"
  ]
  node [
    id 159
    label "zatrzymywa&#263;"
  ]
  node [
    id 160
    label "parry"
  ]
  node [
    id 161
    label "figura"
  ]
  node [
    id 162
    label "wjazd"
  ]
  node [
    id 163
    label "struktura"
  ]
  node [
    id 164
    label "konstrukcja"
  ]
  node [
    id 165
    label "r&#243;w"
  ]
  node [
    id 166
    label "kreacja"
  ]
  node [
    id 167
    label "posesja"
  ]
  node [
    id 168
    label "cecha"
  ]
  node [
    id 169
    label "dokumentacja_powykonawcza"
  ]
  node [
    id 170
    label "mechanika"
  ]
  node [
    id 171
    label "zwierz&#281;"
  ]
  node [
    id 172
    label "miejsce_pracy"
  ]
  node [
    id 173
    label "praca"
  ]
  node [
    id 174
    label "constitution"
  ]
  node [
    id 175
    label "jarzmo_mostowe"
  ]
  node [
    id 176
    label "nap&#281;d"
  ]
  node [
    id 177
    label "obiekt_mostowy"
  ]
  node [
    id 178
    label "rzuci&#263;"
  ]
  node [
    id 179
    label "zam&#243;zgowie"
  ]
  node [
    id 180
    label "rzuca&#263;"
  ]
  node [
    id 181
    label "porozumienie"
  ]
  node [
    id 182
    label "pylon"
  ]
  node [
    id 183
    label "m&#243;zg"
  ]
  node [
    id 184
    label "trasa"
  ]
  node [
    id 185
    label "suwnica"
  ]
  node [
    id 186
    label "urz&#261;dzenie"
  ]
  node [
    id 187
    label "rzucenie"
  ]
  node [
    id 188
    label "szczelina_dylatacyjna"
  ]
  node [
    id 189
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 190
    label "prz&#281;s&#322;o"
  ]
  node [
    id 191
    label "samoch&#243;d"
  ]
  node [
    id 192
    label "rzucanie"
  ]
  node [
    id 193
    label "bridge"
  ]
  node [
    id 194
    label "zimowy"
  ]
  node [
    id 195
    label "nocny"
  ]
  node [
    id 196
    label "&#347;nie&#380;ny"
  ]
  node [
    id 197
    label "mro&#378;ny"
  ]
  node [
    id 198
    label "Warszawa"
  ]
  node [
    id 199
    label "fastback"
  ]
  node [
    id 200
    label "sta&#263;_si&#281;"
  ]
  node [
    id 201
    label "put_in"
  ]
  node [
    id 202
    label "podda&#263;"
  ]
  node [
    id 203
    label "kobieta"
  ]
  node [
    id 204
    label "pozwoli&#263;"
  ]
  node [
    id 205
    label "give"
  ]
  node [
    id 206
    label "podporz&#261;dkowa&#263;_si&#281;"
  ]
  node [
    id 207
    label "fall"
  ]
  node [
    id 208
    label "podda&#263;_si&#281;"
  ]
  node [
    id 209
    label "zrobi&#263;"
  ]
  node [
    id 210
    label "podj&#261;&#263;"
  ]
  node [
    id 211
    label "determine"
  ]
  node [
    id 212
    label "dobrze"
  ]
  node [
    id 213
    label "skuteczny"
  ]
  node [
    id 214
    label "poradzi&#263;"
  ]
  node [
    id 215
    label "consult"
  ]
  node [
    id 216
    label "byd&#322;o"
  ]
  node [
    id 217
    label "zobo"
  ]
  node [
    id 218
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 219
    label "yakalo"
  ]
  node [
    id 220
    label "dzo"
  ]
  node [
    id 221
    label "blurt_out"
  ]
  node [
    id 222
    label "przenosi&#263;"
  ]
  node [
    id 223
    label "rugowa&#263;"
  ]
  node [
    id 224
    label "pozbywa&#263;_si&#281;"
  ]
  node [
    id 225
    label "zabija&#263;"
  ]
  node [
    id 226
    label "przesuwa&#263;"
  ]
  node [
    id 227
    label "undo"
  ]
  node [
    id 228
    label "powodowa&#263;"
  ]
  node [
    id 229
    label "korona"
  ]
  node [
    id 230
    label "kora"
  ]
  node [
    id 231
    label "&#322;yko"
  ]
  node [
    id 232
    label "szpaler"
  ]
  node [
    id 233
    label "fanerofit"
  ]
  node [
    id 234
    label "drzewostan"
  ]
  node [
    id 235
    label "chodnik"
  ]
  node [
    id 236
    label "wykarczowanie"
  ]
  node [
    id 237
    label "surowiec"
  ]
  node [
    id 238
    label "las"
  ]
  node [
    id 239
    label "wykarczowa&#263;"
  ]
  node [
    id 240
    label "zacios"
  ]
  node [
    id 241
    label "brodaczka"
  ]
  node [
    id 242
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 243
    label "karczowa&#263;"
  ]
  node [
    id 244
    label "pie&#324;"
  ]
  node [
    id 245
    label "pier&#347;nica"
  ]
  node [
    id 246
    label "zadrzewienie"
  ]
  node [
    id 247
    label "karczowanie"
  ]
  node [
    id 248
    label "graf"
  ]
  node [
    id 249
    label "parzelnia"
  ]
  node [
    id 250
    label "&#380;ywica"
  ]
  node [
    id 251
    label "skupina"
  ]
  node [
    id 252
    label "jednakowo"
  ]
  node [
    id 253
    label "spokojnie"
  ]
  node [
    id 254
    label "zgodny"
  ]
  node [
    id 255
    label "zbie&#380;nie"
  ]
  node [
    id 256
    label "typ"
  ]
  node [
    id 257
    label "entity"
  ]
  node [
    id 258
    label "kompleksja"
  ]
  node [
    id 259
    label "realia"
  ]
  node [
    id 260
    label "psychika"
  ]
  node [
    id 261
    label "fizjonomia"
  ]
  node [
    id 262
    label "charakter"
  ]
  node [
    id 263
    label "osobowo&#347;&#263;"
  ]
  node [
    id 264
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 265
    label "absolut"
  ]
  node [
    id 266
    label "umocni&#263;"
  ]
  node [
    id 267
    label "bind"
  ]
  node [
    id 268
    label "zdecydowa&#263;"
  ]
  node [
    id 269
    label "spowodowa&#263;"
  ]
  node [
    id 270
    label "unwrap"
  ]
  node [
    id 271
    label "put"
  ]
  node [
    id 272
    label "cia&#322;o"
  ]
  node [
    id 273
    label "plac"
  ]
  node [
    id 274
    label "uwaga"
  ]
  node [
    id 275
    label "przestrze&#324;"
  ]
  node [
    id 276
    label "status"
  ]
  node [
    id 277
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 278
    label "chwila"
  ]
  node [
    id 279
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 280
    label "rz&#261;d"
  ]
  node [
    id 281
    label "location"
  ]
  node [
    id 282
    label "warunek_lokalowy"
  ]
  node [
    id 283
    label "wysun&#261;&#263;"
  ]
  node [
    id 284
    label "zbudowa&#263;"
  ]
  node [
    id 285
    label "wyrazi&#263;"
  ]
  node [
    id 286
    label "dopu&#347;ci&#263;"
  ]
  node [
    id 287
    label "przedstawi&#263;"
  ]
  node [
    id 288
    label "wypisa&#263;"
  ]
  node [
    id 289
    label "wskaza&#263;"
  ]
  node [
    id 290
    label "indicate"
  ]
  node [
    id 291
    label "wynie&#347;&#263;"
  ]
  node [
    id 292
    label "pies_my&#347;liwski"
  ]
  node [
    id 293
    label "zaproponowa&#263;"
  ]
  node [
    id 294
    label "wyeksponowa&#263;"
  ]
  node [
    id 295
    label "wychyli&#263;"
  ]
  node [
    id 296
    label "set"
  ]
  node [
    id 297
    label "wyj&#261;&#263;"
  ]
  node [
    id 298
    label "shelter"
  ]
  node [
    id 299
    label "pomieszczenie"
  ]
  node [
    id 300
    label "budynek"
  ]
  node [
    id 301
    label "kapelusz"
  ]
  node [
    id 302
    label "kuper"
  ]
  node [
    id 303
    label "dziobn&#261;&#263;"
  ]
  node [
    id 304
    label "grzebie&#324;"
  ]
  node [
    id 305
    label "skok"
  ]
  node [
    id 306
    label "ptactwo"
  ]
  node [
    id 307
    label "zaklekotanie"
  ]
  node [
    id 308
    label "dzi&#243;b"
  ]
  node [
    id 309
    label "pi&#243;ro"
  ]
  node [
    id 310
    label "owodniowiec"
  ]
  node [
    id 311
    label "hukni&#281;cie"
  ]
  node [
    id 312
    label "upierzenie"
  ]
  node [
    id 313
    label "dziobanie"
  ]
  node [
    id 314
    label "bird"
  ]
  node [
    id 315
    label "dziobni&#281;cie"
  ]
  node [
    id 316
    label "roz&#263;wierkanie_si&#281;"
  ]
  node [
    id 317
    label "wysiedzie&#263;"
  ]
  node [
    id 318
    label "dzi&#243;bn&#261;&#263;"
  ]
  node [
    id 319
    label "dzioba&#263;"
  ]
  node [
    id 320
    label "dzi&#243;bni&#281;cie"
  ]
  node [
    id 321
    label "ko&#347;&#263;_potyliczna"
  ]
  node [
    id 322
    label "kloaka"
  ]
  node [
    id 323
    label "wysiadywa&#263;"
  ]
  node [
    id 324
    label "ptaki"
  ]
  node [
    id 325
    label "skrzyd&#322;o"
  ]
  node [
    id 326
    label "pogwizdywanie"
  ]
  node [
    id 327
    label "ptasz&#281;"
  ]
  node [
    id 328
    label "get"
  ]
  node [
    id 329
    label "doczeka&#263;"
  ]
  node [
    id 330
    label "zwiastun"
  ]
  node [
    id 331
    label "si&#281;gn&#261;&#263;"
  ]
  node [
    id 332
    label "develop"
  ]
  node [
    id 333
    label "catch"
  ]
  node [
    id 334
    label "uzyska&#263;"
  ]
  node [
    id 335
    label "kupi&#263;"
  ]
  node [
    id 336
    label "wzi&#261;&#263;"
  ]
  node [
    id 337
    label "naby&#263;"
  ]
  node [
    id 338
    label "nabawienie_si&#281;"
  ]
  node [
    id 339
    label "obskoczy&#263;"
  ]
  node [
    id 340
    label "zapanowa&#263;"
  ]
  node [
    id 341
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 342
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 343
    label "nabawianie_si&#281;"
  ]
  node [
    id 344
    label "range"
  ]
  node [
    id 345
    label "schorzenie"
  ]
  node [
    id 346
    label "wysta&#263;"
  ]
  node [
    id 347
    label "liczenie"
  ]
  node [
    id 348
    label "return"
  ]
  node [
    id 349
    label "doch&#243;d"
  ]
  node [
    id 350
    label "zap&#322;ata"
  ]
  node [
    id 351
    label "wynagrodzenie_brutto"
  ]
  node [
    id 352
    label "ryzyko_niewygas&#322;e"
  ]
  node [
    id 353
    label "koszt_rodzajowy"
  ]
  node [
    id 354
    label "zado&#347;&#263;uczynienie"
  ]
  node [
    id 355
    label "danie"
  ]
  node [
    id 356
    label "policzenie"
  ]
  node [
    id 357
    label "policzy&#263;"
  ]
  node [
    id 358
    label "liczy&#263;"
  ]
  node [
    id 359
    label "refund"
  ]
  node [
    id 360
    label "bud&#380;et_domowy"
  ]
  node [
    id 361
    label "pay"
  ]
  node [
    id 362
    label "ordynaria"
  ]
  node [
    id 363
    label "przepi&#281;knie"
  ]
  node [
    id 364
    label "po&#380;&#261;dany"
  ]
  node [
    id 365
    label "wspania&#322;y"
  ]
  node [
    id 366
    label "dokument"
  ]
  node [
    id 367
    label "towar"
  ]
  node [
    id 368
    label "nag&#322;&#243;wek"
  ]
  node [
    id 369
    label "znak_j&#281;zykowy"
  ]
  node [
    id 370
    label "wyr&#243;b"
  ]
  node [
    id 371
    label "blok"
  ]
  node [
    id 372
    label "line"
  ]
  node [
    id 373
    label "paragraf"
  ]
  node [
    id 374
    label "rodzajnik"
  ]
  node [
    id 375
    label "prawda"
  ]
  node [
    id 376
    label "szkic"
  ]
  node [
    id 377
    label "tekst"
  ]
  node [
    id 378
    label "fragment"
  ]
  node [
    id 379
    label "czyj&#347;"
  ]
  node [
    id 380
    label "nadrz&#281;dny"
  ]
  node [
    id 381
    label "jeneralny"
  ]
  node [
    id 382
    label "zawo&#322;any"
  ]
  node [
    id 383
    label "naczelnie"
  ]
  node [
    id 384
    label "g&#322;&#243;wny"
  ]
  node [
    id 385
    label "Michnik"
  ]
  node [
    id 386
    label "znany"
  ]
  node [
    id 387
    label "zwierzchnik"
  ]
  node [
    id 388
    label "nacjonalistyczny"
  ]
  node [
    id 389
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 390
    label "narodowo"
  ]
  node [
    id 391
    label "wa&#380;ny"
  ]
  node [
    id 392
    label "prasa"
  ]
  node [
    id 393
    label "redakcja"
  ]
  node [
    id 394
    label "tytu&#322;"
  ]
  node [
    id 395
    label "rozk&#322;ad&#243;wka"
  ]
  node [
    id 396
    label "czasopismo"
  ]
  node [
    id 397
    label "proceed"
  ]
  node [
    id 398
    label "pozosta&#263;"
  ]
  node [
    id 399
    label "osta&#263;_si&#281;"
  ]
  node [
    id 400
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 401
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 402
    label "change"
  ]
  node [
    id 403
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 404
    label "oceni&#263;"
  ]
  node [
    id 405
    label "review"
  ]
  node [
    id 406
    label "zaopiniowa&#263;"
  ]
  node [
    id 407
    label "rekrutacja"
  ]
  node [
    id 408
    label "czynno&#347;&#263;"
  ]
  node [
    id 409
    label "nazywa&#263;"
  ]
  node [
    id 410
    label "cz&#322;owiek"
  ]
  node [
    id 411
    label "bran&#380;owiec"
  ]
  node [
    id 412
    label "wydawnictwo"
  ]
  node [
    id 413
    label "edytor"
  ]
  node [
    id 414
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 415
    label "op&#322;ata"
  ]
  node [
    id 416
    label "danina"
  ]
  node [
    id 417
    label "Wies&#322;awa"
  ]
  node [
    id 418
    label "Nowicki"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 127
  ]
  edge [
    source 7
    target 128
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 7
    target 130
  ]
  edge [
    source 7
    target 131
  ]
  edge [
    source 7
    target 132
  ]
  edge [
    source 7
    target 133
  ]
  edge [
    source 7
    target 134
  ]
  edge [
    source 7
    target 135
  ]
  edge [
    source 7
    target 136
  ]
  edge [
    source 7
    target 137
  ]
  edge [
    source 7
    target 138
  ]
  edge [
    source 7
    target 139
  ]
  edge [
    source 7
    target 140
  ]
  edge [
    source 7
    target 14
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 141
  ]
  edge [
    source 8
    target 142
  ]
  edge [
    source 8
    target 143
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 144
  ]
  edge [
    source 9
    target 145
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 47
  ]
  edge [
    source 11
    target 48
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 146
  ]
  edge [
    source 12
    target 147
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 148
  ]
  edge [
    source 13
    target 149
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 150
  ]
  edge [
    source 14
    target 151
  ]
  edge [
    source 14
    target 152
  ]
  edge [
    source 14
    target 153
  ]
  edge [
    source 14
    target 154
  ]
  edge [
    source 14
    target 155
  ]
  edge [
    source 14
    target 156
  ]
  edge [
    source 14
    target 157
  ]
  edge [
    source 14
    target 158
  ]
  edge [
    source 14
    target 159
  ]
  edge [
    source 14
    target 160
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 161
  ]
  edge [
    source 15
    target 162
  ]
  edge [
    source 15
    target 163
  ]
  edge [
    source 15
    target 164
  ]
  edge [
    source 15
    target 165
  ]
  edge [
    source 15
    target 166
  ]
  edge [
    source 15
    target 167
  ]
  edge [
    source 15
    target 168
  ]
  edge [
    source 15
    target 169
  ]
  edge [
    source 15
    target 145
  ]
  edge [
    source 15
    target 170
  ]
  edge [
    source 15
    target 171
  ]
  edge [
    source 15
    target 172
  ]
  edge [
    source 15
    target 173
  ]
  edge [
    source 15
    target 174
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 175
  ]
  edge [
    source 16
    target 176
  ]
  edge [
    source 16
    target 177
  ]
  edge [
    source 16
    target 178
  ]
  edge [
    source 16
    target 179
  ]
  edge [
    source 16
    target 180
  ]
  edge [
    source 16
    target 181
  ]
  edge [
    source 16
    target 182
  ]
  edge [
    source 16
    target 183
  ]
  edge [
    source 16
    target 184
  ]
  edge [
    source 16
    target 185
  ]
  edge [
    source 16
    target 186
  ]
  edge [
    source 16
    target 187
  ]
  edge [
    source 16
    target 188
  ]
  edge [
    source 16
    target 189
  ]
  edge [
    source 16
    target 190
  ]
  edge [
    source 16
    target 191
  ]
  edge [
    source 16
    target 192
  ]
  edge [
    source 16
    target 193
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 194
  ]
  edge [
    source 17
    target 195
  ]
  edge [
    source 17
    target 196
  ]
  edge [
    source 17
    target 197
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 198
  ]
  edge [
    source 18
    target 191
  ]
  edge [
    source 18
    target 199
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 200
  ]
  edge [
    source 20
    target 201
  ]
  edge [
    source 20
    target 202
  ]
  edge [
    source 20
    target 203
  ]
  edge [
    source 20
    target 204
  ]
  edge [
    source 20
    target 205
  ]
  edge [
    source 20
    target 206
  ]
  edge [
    source 20
    target 207
  ]
  edge [
    source 20
    target 208
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 200
  ]
  edge [
    source 21
    target 209
  ]
  edge [
    source 21
    target 210
  ]
  edge [
    source 21
    target 211
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 212
  ]
  edge [
    source 22
    target 213
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 214
  ]
  edge [
    source 23
    target 215
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 216
  ]
  edge [
    source 24
    target 217
  ]
  edge [
    source 24
    target 218
  ]
  edge [
    source 24
    target 219
  ]
  edge [
    source 24
    target 220
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 221
  ]
  edge [
    source 25
    target 222
  ]
  edge [
    source 25
    target 223
  ]
  edge [
    source 25
    target 224
  ]
  edge [
    source 25
    target 135
  ]
  edge [
    source 25
    target 225
  ]
  edge [
    source 25
    target 226
  ]
  edge [
    source 25
    target 227
  ]
  edge [
    source 25
    target 228
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 229
  ]
  edge [
    source 26
    target 230
  ]
  edge [
    source 26
    target 231
  ]
  edge [
    source 26
    target 232
  ]
  edge [
    source 26
    target 233
  ]
  edge [
    source 26
    target 234
  ]
  edge [
    source 26
    target 235
  ]
  edge [
    source 26
    target 236
  ]
  edge [
    source 26
    target 237
  ]
  edge [
    source 26
    target 168
  ]
  edge [
    source 26
    target 238
  ]
  edge [
    source 26
    target 239
  ]
  edge [
    source 26
    target 240
  ]
  edge [
    source 26
    target 241
  ]
  edge [
    source 26
    target 242
  ]
  edge [
    source 26
    target 243
  ]
  edge [
    source 26
    target 244
  ]
  edge [
    source 26
    target 245
  ]
  edge [
    source 26
    target 246
  ]
  edge [
    source 26
    target 247
  ]
  edge [
    source 26
    target 248
  ]
  edge [
    source 26
    target 249
  ]
  edge [
    source 26
    target 250
  ]
  edge [
    source 26
    target 251
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 252
  ]
  edge [
    source 27
    target 253
  ]
  edge [
    source 27
    target 254
  ]
  edge [
    source 27
    target 212
  ]
  edge [
    source 27
    target 255
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 256
  ]
  edge [
    source 28
    target 114
  ]
  edge [
    source 28
    target 117
  ]
  edge [
    source 28
    target 118
  ]
  edge [
    source 28
    target 120
  ]
  edge [
    source 28
    target 257
  ]
  edge [
    source 28
    target 122
  ]
  edge [
    source 28
    target 258
  ]
  edge [
    source 28
    target 259
  ]
  edge [
    source 28
    target 112
  ]
  edge [
    source 28
    target 113
  ]
  edge [
    source 28
    target 260
  ]
  edge [
    source 28
    target 116
  ]
  edge [
    source 28
    target 119
  ]
  edge [
    source 28
    target 121
  ]
  edge [
    source 28
    target 261
  ]
  edge [
    source 28
    target 262
  ]
  edge [
    source 28
    target 126
  ]
  edge [
    source 28
    target 168
  ]
  edge [
    source 28
    target 263
  ]
  edge [
    source 28
    target 264
  ]
  edge [
    source 28
    target 115
  ]
  edge [
    source 28
    target 265
  ]
  edge [
    source 28
    target 123
  ]
  edge [
    source 28
    target 125
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 266
  ]
  edge [
    source 29
    target 267
  ]
  edge [
    source 29
    target 268
  ]
  edge [
    source 29
    target 269
  ]
  edge [
    source 29
    target 270
  ]
  edge [
    source 29
    target 209
  ]
  edge [
    source 29
    target 271
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 272
  ]
  edge [
    source 30
    target 273
  ]
  edge [
    source 30
    target 168
  ]
  edge [
    source 30
    target 274
  ]
  edge [
    source 30
    target 275
  ]
  edge [
    source 30
    target 276
  ]
  edge [
    source 30
    target 277
  ]
  edge [
    source 30
    target 278
  ]
  edge [
    source 30
    target 279
  ]
  edge [
    source 30
    target 280
  ]
  edge [
    source 30
    target 173
  ]
  edge [
    source 30
    target 281
  ]
  edge [
    source 30
    target 282
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 283
  ]
  edge [
    source 31
    target 284
  ]
  edge [
    source 31
    target 285
  ]
  edge [
    source 31
    target 286
  ]
  edge [
    source 31
    target 287
  ]
  edge [
    source 31
    target 288
  ]
  edge [
    source 31
    target 289
  ]
  edge [
    source 31
    target 290
  ]
  edge [
    source 31
    target 291
  ]
  edge [
    source 31
    target 292
  ]
  edge [
    source 31
    target 293
  ]
  edge [
    source 31
    target 294
  ]
  edge [
    source 31
    target 295
  ]
  edge [
    source 31
    target 296
  ]
  edge [
    source 31
    target 297
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 298
  ]
  edge [
    source 32
    target 299
  ]
  edge [
    source 32
    target 300
  ]
  edge [
    source 32
    target 301
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 302
  ]
  edge [
    source 35
    target 303
  ]
  edge [
    source 35
    target 304
  ]
  edge [
    source 35
    target 305
  ]
  edge [
    source 35
    target 306
  ]
  edge [
    source 35
    target 307
  ]
  edge [
    source 35
    target 308
  ]
  edge [
    source 35
    target 309
  ]
  edge [
    source 35
    target 310
  ]
  edge [
    source 35
    target 311
  ]
  edge [
    source 35
    target 312
  ]
  edge [
    source 35
    target 313
  ]
  edge [
    source 35
    target 314
  ]
  edge [
    source 35
    target 315
  ]
  edge [
    source 35
    target 316
  ]
  edge [
    source 35
    target 317
  ]
  edge [
    source 35
    target 318
  ]
  edge [
    source 35
    target 319
  ]
  edge [
    source 35
    target 320
  ]
  edge [
    source 35
    target 321
  ]
  edge [
    source 35
    target 322
  ]
  edge [
    source 35
    target 323
  ]
  edge [
    source 35
    target 324
  ]
  edge [
    source 35
    target 325
  ]
  edge [
    source 35
    target 326
  ]
  edge [
    source 35
    target 327
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 328
  ]
  edge [
    source 36
    target 329
  ]
  edge [
    source 36
    target 330
  ]
  edge [
    source 36
    target 331
  ]
  edge [
    source 36
    target 332
  ]
  edge [
    source 36
    target 333
  ]
  edge [
    source 36
    target 334
  ]
  edge [
    source 36
    target 335
  ]
  edge [
    source 36
    target 336
  ]
  edge [
    source 36
    target 337
  ]
  edge [
    source 36
    target 338
  ]
  edge [
    source 36
    target 339
  ]
  edge [
    source 36
    target 340
  ]
  edge [
    source 36
    target 341
  ]
  edge [
    source 36
    target 342
  ]
  edge [
    source 36
    target 209
  ]
  edge [
    source 36
    target 343
  ]
  edge [
    source 36
    target 344
  ]
  edge [
    source 36
    target 345
  ]
  edge [
    source 36
    target 61
  ]
  edge [
    source 36
    target 346
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 46
  ]
  edge [
    source 37
    target 47
  ]
  edge [
    source 37
    target 347
  ]
  edge [
    source 37
    target 348
  ]
  edge [
    source 37
    target 349
  ]
  edge [
    source 37
    target 350
  ]
  edge [
    source 37
    target 351
  ]
  edge [
    source 37
    target 352
  ]
  edge [
    source 37
    target 353
  ]
  edge [
    source 37
    target 354
  ]
  edge [
    source 37
    target 355
  ]
  edge [
    source 37
    target 356
  ]
  edge [
    source 37
    target 357
  ]
  edge [
    source 37
    target 358
  ]
  edge [
    source 37
    target 359
  ]
  edge [
    source 37
    target 360
  ]
  edge [
    source 37
    target 361
  ]
  edge [
    source 37
    target 362
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 363
  ]
  edge [
    source 38
    target 364
  ]
  edge [
    source 38
    target 365
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 366
  ]
  edge [
    source 39
    target 367
  ]
  edge [
    source 39
    target 368
  ]
  edge [
    source 39
    target 369
  ]
  edge [
    source 39
    target 370
  ]
  edge [
    source 39
    target 371
  ]
  edge [
    source 39
    target 372
  ]
  edge [
    source 39
    target 373
  ]
  edge [
    source 39
    target 374
  ]
  edge [
    source 39
    target 375
  ]
  edge [
    source 39
    target 376
  ]
  edge [
    source 39
    target 377
  ]
  edge [
    source 39
    target 378
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 379
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 380
  ]
  edge [
    source 41
    target 381
  ]
  edge [
    source 41
    target 382
  ]
  edge [
    source 41
    target 383
  ]
  edge [
    source 41
    target 384
  ]
  edge [
    source 41
    target 385
  ]
  edge [
    source 41
    target 386
  ]
  edge [
    source 41
    target 48
  ]
  edge [
    source 41
    target 387
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 388
  ]
  edge [
    source 42
    target 389
  ]
  edge [
    source 42
    target 390
  ]
  edge [
    source 42
    target 391
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 392
  ]
  edge [
    source 43
    target 393
  ]
  edge [
    source 43
    target 394
  ]
  edge [
    source 43
    target 395
  ]
  edge [
    source 43
    target 396
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 397
  ]
  edge [
    source 44
    target 333
  ]
  edge [
    source 44
    target 398
  ]
  edge [
    source 44
    target 399
  ]
  edge [
    source 44
    target 400
  ]
  edge [
    source 44
    target 401
  ]
  edge [
    source 44
    target 342
  ]
  edge [
    source 44
    target 402
  ]
  edge [
    source 44
    target 403
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 404
  ]
  edge [
    source 45
    target 405
  ]
  edge [
    source 45
    target 406
  ]
  edge [
    source 46
    target 407
  ]
  edge [
    source 46
    target 408
  ]
  edge [
    source 47
    target 409
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 410
  ]
  edge [
    source 48
    target 411
  ]
  edge [
    source 48
    target 412
  ]
  edge [
    source 48
    target 413
  ]
  edge [
    source 48
    target 393
  ]
  edge [
    source 48
    target 414
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 50
    target 415
  ]
  edge [
    source 50
    target 416
  ]
  edge [
    source 417
    target 418
  ]
]
