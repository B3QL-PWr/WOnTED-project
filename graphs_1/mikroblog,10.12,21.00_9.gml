graph [
  maxDegree 70
  minDegree 1
  meanDegree 2.0387096774193547
  density 0.01323837452869711
  graphCliqueNumber 3
  node [
    id 0
    label "zacz&#261;&#263;"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "warszawa"
    origin "text"
  ]
  node [
    id 3
    label "wyj&#347;&#263;"
    origin "text"
  ]
  node [
    id 4
    label "&#380;&#243;&#322;ty"
    origin "text"
  ]
  node [
    id 5
    label "kamizelka"
    origin "text"
  ]
  node [
    id 6
    label "do&#347;&#263;"
    origin "text"
  ]
  node [
    id 7
    label "wra&#380;liwy"
    origin "text"
  ]
  node [
    id 8
    label "miejsce"
    origin "text"
  ]
  node [
    id 9
    label "dla"
    origin "text"
  ]
  node [
    id 10
    label "nowy"
    origin "text"
  ]
  node [
    id 11
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 12
    label "cause"
  ]
  node [
    id 13
    label "introduce"
  ]
  node [
    id 14
    label "begin"
  ]
  node [
    id 15
    label "odj&#261;&#263;"
  ]
  node [
    id 16
    label "post&#261;pi&#263;"
  ]
  node [
    id 17
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 18
    label "do"
  ]
  node [
    id 19
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 20
    label "zrobi&#263;"
  ]
  node [
    id 21
    label "Warszawa"
  ]
  node [
    id 22
    label "samoch&#243;d"
  ]
  node [
    id 23
    label "fastback"
  ]
  node [
    id 24
    label "get"
  ]
  node [
    id 25
    label "opu&#347;ci&#263;"
  ]
  node [
    id 26
    label "ukaza&#263;_si&#281;_drukiem"
  ]
  node [
    id 27
    label "zej&#347;&#263;"
  ]
  node [
    id 28
    label "okaza&#263;_si&#281;"
  ]
  node [
    id 29
    label "powie&#347;&#263;_si&#281;"
  ]
  node [
    id 30
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 31
    label "sko&#324;czy&#263;"
  ]
  node [
    id 32
    label "ograniczenie"
  ]
  node [
    id 33
    label "ruszy&#263;"
  ]
  node [
    id 34
    label "wypa&#347;&#263;"
  ]
  node [
    id 35
    label "uko&#324;czy&#263;"
  ]
  node [
    id 36
    label "open"
  ]
  node [
    id 37
    label "moderate"
  ]
  node [
    id 38
    label "uzyska&#263;"
  ]
  node [
    id 39
    label "wydosta&#263;_si&#281;"
  ]
  node [
    id 40
    label "wywie&#347;&#263;_si&#281;"
  ]
  node [
    id 41
    label "mount"
  ]
  node [
    id 42
    label "leave"
  ]
  node [
    id 43
    label "drive"
  ]
  node [
    id 44
    label "zagra&#263;"
  ]
  node [
    id 45
    label "zademonstrowa&#263;"
  ]
  node [
    id 46
    label "wystarczy&#263;"
  ]
  node [
    id 47
    label "perform"
  ]
  node [
    id 48
    label "uwolni&#263;_si&#281;"
  ]
  node [
    id 49
    label "drop"
  ]
  node [
    id 50
    label "jasny"
  ]
  node [
    id 51
    label "typ_mongoloidalny"
  ]
  node [
    id 52
    label "&#380;&#243;&#322;cenie"
  ]
  node [
    id 53
    label "kolorowy"
  ]
  node [
    id 54
    label "ciep&#322;y"
  ]
  node [
    id 55
    label "&#380;&#243;&#322;to"
  ]
  node [
    id 56
    label "westa"
  ]
  node [
    id 57
    label "g&#243;ra"
  ]
  node [
    id 58
    label "wra&#380;liwie"
  ]
  node [
    id 59
    label "wra&#378;liwy"
  ]
  node [
    id 60
    label "sk&#322;onny"
  ]
  node [
    id 61
    label "podatny"
  ]
  node [
    id 62
    label "wa&#380;ny"
  ]
  node [
    id 63
    label "nieoboj&#281;tny"
  ]
  node [
    id 64
    label "delikatny"
  ]
  node [
    id 65
    label "cia&#322;o"
  ]
  node [
    id 66
    label "plac"
  ]
  node [
    id 67
    label "cecha"
  ]
  node [
    id 68
    label "uwaga"
  ]
  node [
    id 69
    label "przestrze&#324;"
  ]
  node [
    id 70
    label "status"
  ]
  node [
    id 71
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 72
    label "chwila"
  ]
  node [
    id 73
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 74
    label "rz&#261;d"
  ]
  node [
    id 75
    label "praca"
  ]
  node [
    id 76
    label "location"
  ]
  node [
    id 77
    label "warunek_lokalowy"
  ]
  node [
    id 78
    label "cz&#322;owiek"
  ]
  node [
    id 79
    label "nowotny"
  ]
  node [
    id 80
    label "drugi"
  ]
  node [
    id 81
    label "kolejny"
  ]
  node [
    id 82
    label "bie&#380;&#261;cy"
  ]
  node [
    id 83
    label "nowo"
  ]
  node [
    id 84
    label "narybek"
  ]
  node [
    id 85
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 86
    label "obcy"
  ]
  node [
    id 87
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 88
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 89
    label "obszar"
  ]
  node [
    id 90
    label "obiekt_naturalny"
  ]
  node [
    id 91
    label "przedmiot"
  ]
  node [
    id 92
    label "biosfera"
  ]
  node [
    id 93
    label "grupa"
  ]
  node [
    id 94
    label "stw&#243;r"
  ]
  node [
    id 95
    label "Stary_&#346;wiat"
  ]
  node [
    id 96
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 97
    label "rzecz"
  ]
  node [
    id 98
    label "magnetosfera"
  ]
  node [
    id 99
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 100
    label "environment"
  ]
  node [
    id 101
    label "Nowy_&#346;wiat"
  ]
  node [
    id 102
    label "geosfera"
  ]
  node [
    id 103
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 104
    label "planeta"
  ]
  node [
    id 105
    label "przejmowa&#263;"
  ]
  node [
    id 106
    label "litosfera"
  ]
  node [
    id 107
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 108
    label "makrokosmos"
  ]
  node [
    id 109
    label "barysfera"
  ]
  node [
    id 110
    label "biota"
  ]
  node [
    id 111
    label "p&#243;&#322;noc"
  ]
  node [
    id 112
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 113
    label "fauna"
  ]
  node [
    id 114
    label "wszechstworzenie"
  ]
  node [
    id 115
    label "geotermia"
  ]
  node [
    id 116
    label "biegun"
  ]
  node [
    id 117
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 118
    label "ekosystem"
  ]
  node [
    id 119
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 120
    label "teren"
  ]
  node [
    id 121
    label "zjawisko"
  ]
  node [
    id 122
    label "p&#243;&#322;kula"
  ]
  node [
    id 123
    label "atmosfera"
  ]
  node [
    id 124
    label "mikrokosmos"
  ]
  node [
    id 125
    label "class"
  ]
  node [
    id 126
    label "po&#322;udnie"
  ]
  node [
    id 127
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 128
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 129
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 130
    label "przejmowanie"
  ]
  node [
    id 131
    label "asymilowanie_si&#281;"
  ]
  node [
    id 132
    label "przej&#261;&#263;"
  ]
  node [
    id 133
    label "ekosfera"
  ]
  node [
    id 134
    label "przyroda"
  ]
  node [
    id 135
    label "ciemna_materia"
  ]
  node [
    id 136
    label "geoida"
  ]
  node [
    id 137
    label "Wsch&#243;d"
  ]
  node [
    id 138
    label "populace"
  ]
  node [
    id 139
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 140
    label "huczek"
  ]
  node [
    id 141
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 142
    label "Ziemia"
  ]
  node [
    id 143
    label "universe"
  ]
  node [
    id 144
    label "ozonosfera"
  ]
  node [
    id 145
    label "rze&#378;ba"
  ]
  node [
    id 146
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 147
    label "zagranica"
  ]
  node [
    id 148
    label "hydrosfera"
  ]
  node [
    id 149
    label "woda"
  ]
  node [
    id 150
    label "kuchnia"
  ]
  node [
    id 151
    label "przej&#281;cie"
  ]
  node [
    id 152
    label "czarna_dziura"
  ]
  node [
    id 153
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 154
    label "morze"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 11
    target 87
  ]
  edge [
    source 11
    target 88
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 11
    target 92
  ]
  edge [
    source 11
    target 93
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 11
    target 98
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 11
    target 100
  ]
  edge [
    source 11
    target 101
  ]
  edge [
    source 11
    target 102
  ]
  edge [
    source 11
    target 103
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 108
  ]
  edge [
    source 11
    target 109
  ]
  edge [
    source 11
    target 110
  ]
  edge [
    source 11
    target 111
  ]
  edge [
    source 11
    target 112
  ]
  edge [
    source 11
    target 113
  ]
  edge [
    source 11
    target 114
  ]
  edge [
    source 11
    target 115
  ]
  edge [
    source 11
    target 116
  ]
  edge [
    source 11
    target 117
  ]
  edge [
    source 11
    target 118
  ]
  edge [
    source 11
    target 119
  ]
  edge [
    source 11
    target 120
  ]
  edge [
    source 11
    target 121
  ]
  edge [
    source 11
    target 122
  ]
  edge [
    source 11
    target 123
  ]
  edge [
    source 11
    target 124
  ]
  edge [
    source 11
    target 125
  ]
  edge [
    source 11
    target 126
  ]
  edge [
    source 11
    target 127
  ]
  edge [
    source 11
    target 128
  ]
  edge [
    source 11
    target 129
  ]
  edge [
    source 11
    target 130
  ]
  edge [
    source 11
    target 69
  ]
  edge [
    source 11
    target 131
  ]
  edge [
    source 11
    target 132
  ]
  edge [
    source 11
    target 133
  ]
  edge [
    source 11
    target 134
  ]
  edge [
    source 11
    target 135
  ]
  edge [
    source 11
    target 136
  ]
  edge [
    source 11
    target 137
  ]
  edge [
    source 11
    target 138
  ]
  edge [
    source 11
    target 139
  ]
  edge [
    source 11
    target 140
  ]
  edge [
    source 11
    target 141
  ]
  edge [
    source 11
    target 142
  ]
  edge [
    source 11
    target 143
  ]
  edge [
    source 11
    target 144
  ]
  edge [
    source 11
    target 145
  ]
  edge [
    source 11
    target 146
  ]
  edge [
    source 11
    target 147
  ]
  edge [
    source 11
    target 148
  ]
  edge [
    source 11
    target 149
  ]
  edge [
    source 11
    target 150
  ]
  edge [
    source 11
    target 151
  ]
  edge [
    source 11
    target 152
  ]
  edge [
    source 11
    target 153
  ]
  edge [
    source 11
    target 154
  ]
]
