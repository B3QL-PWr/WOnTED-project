graph [
  maxDegree 597
  minDegree 1
  meanDegree 2.1184407796101947
  density 0.0015892278916805663
  graphCliqueNumber 7
  node [
    id 0
    label "przypomnie&#263;"
    origin "text"
  ]
  node [
    id 1
    label "pa&#324;stwo"
    origin "text"
  ]
  node [
    id 2
    label "nawet"
    origin "text"
  ]
  node [
    id 3
    label "je&#347;li"
    origin "text"
  ]
  node [
    id 4
    label "chodzi&#263;"
    origin "text"
  ]
  node [
    id 5
    label "program"
    origin "text"
  ]
  node [
    id 6
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 7
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 8
    label "przyst&#261;pi&#263;"
    origin "text"
  ]
  node [
    id 9
    label "tryb"
    origin "text"
  ]
  node [
    id 10
    label "pozakonkursowy"
    origin "text"
  ]
  node [
    id 11
    label "bra&#263;"
    origin "text"
  ]
  node [
    id 12
    label "udzia&#322;"
    origin "text"
  ]
  node [
    id 13
    label "&#380;aden"
    origin "text"
  ]
  node [
    id 14
    label "konkurencja"
    origin "text"
  ]
  node [
    id 15
    label "musie&#263;"
    origin "text"
  ]
  node [
    id 16
    label "konkurowa&#263;"
    origin "text"
  ]
  node [
    id 17
    label "inny"
    origin "text"
  ]
  node [
    id 18
    label "miasto"
    origin "text"
  ]
  node [
    id 19
    label "taki"
    origin "text"
  ]
  node [
    id 20
    label "wskazany"
    origin "text"
  ]
  node [
    id 21
    label "jak"
    origin "text"
  ]
  node [
    id 22
    label "dotyczy&#263;"
    origin "text"
  ]
  node [
    id 23
    label "rozw&#243;j"
    origin "text"
  ]
  node [
    id 24
    label "polska"
    origin "text"
  ]
  node [
    id 25
    label "wschodni"
    origin "text"
  ]
  node [
    id 26
    label "przewidzie&#263;"
    origin "text"
  ]
  node [
    id 27
    label "dla"
    origin "text"
  ]
  node [
    id 28
    label "polski"
    origin "text"
  ]
  node [
    id 29
    label "biedny"
    origin "text"
  ]
  node [
    id 30
    label "wojew&#243;dztwo"
    origin "text"
  ]
  node [
    id 31
    label "ci&#261;g"
    origin "text"
  ]
  node [
    id 32
    label "ostatni"
    origin "text"
  ]
  node [
    id 33
    label "kilka"
    origin "text"
  ]
  node [
    id 34
    label "miesi&#261;c"
    origin "text"
  ]
  node [
    id 35
    label "olsztyn"
    origin "text"
  ]
  node [
    id 36
    label "wycofa&#263;"
    origin "text"
  ]
  node [
    id 37
    label "wniosek"
    origin "text"
  ]
  node [
    id 38
    label "uzbrojenie"
    origin "text"
  ]
  node [
    id 39
    label "teren"
    origin "text"
  ]
  node [
    id 40
    label "inwestycyjny"
    origin "text"
  ]
  node [
    id 41
    label "musza"
    origin "text"
  ]
  node [
    id 42
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 43
    label "wa&#380;ny"
    origin "text"
  ]
  node [
    id 44
    label "tak&#380;e"
    origin "text"
  ]
  node [
    id 45
    label "park"
    origin "text"
  ]
  node [
    id 46
    label "technologiczny"
    origin "text"
  ]
  node [
    id 47
    label "przesun&#261;&#263;"
    origin "text"
  ]
  node [
    id 48
    label "czas"
    origin "text"
  ]
  node [
    id 49
    label "rozpocz&#281;cie"
    origin "text"
  ]
  node [
    id 50
    label "rok"
    origin "text"
  ]
  node [
    id 51
    label "czyli"
    origin "text"
  ]
  node [
    id 52
    label "praktycznie"
    origin "text"
  ]
  node [
    id 53
    label "ten"
    origin "text"
  ]
  node [
    id 54
    label "bud&#380;et"
    origin "text"
  ]
  node [
    id 55
    label "&#347;rodek"
    origin "text"
  ]
  node [
    id 56
    label "przypadek"
    origin "text"
  ]
  node [
    id 57
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 58
    label "postawi&#263;"
    origin "text"
  ]
  node [
    id 59
    label "znak"
    origin "text"
  ]
  node [
    id 60
    label "poinformowa&#263;"
  ]
  node [
    id 61
    label "u&#347;wiadomi&#263;"
  ]
  node [
    id 62
    label "prompt"
  ]
  node [
    id 63
    label "Filipiny"
  ]
  node [
    id 64
    label "Rwanda"
  ]
  node [
    id 65
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 66
    label "Monako"
  ]
  node [
    id 67
    label "Korea"
  ]
  node [
    id 68
    label "Ghana"
  ]
  node [
    id 69
    label "Czarnog&#243;ra"
  ]
  node [
    id 70
    label "Malawi"
  ]
  node [
    id 71
    label "Indonezja"
  ]
  node [
    id 72
    label "Bu&#322;garia"
  ]
  node [
    id 73
    label "Nauru"
  ]
  node [
    id 74
    label "Kenia"
  ]
  node [
    id 75
    label "Kambod&#380;a"
  ]
  node [
    id 76
    label "Mali"
  ]
  node [
    id 77
    label "Austria"
  ]
  node [
    id 78
    label "interior"
  ]
  node [
    id 79
    label "Armenia"
  ]
  node [
    id 80
    label "Fid&#380;i"
  ]
  node [
    id 81
    label "Tuwalu"
  ]
  node [
    id 82
    label "Etiopia"
  ]
  node [
    id 83
    label "Malta"
  ]
  node [
    id 84
    label "Malezja"
  ]
  node [
    id 85
    label "Grenada"
  ]
  node [
    id 86
    label "Tad&#380;ykistan"
  ]
  node [
    id 87
    label "Wehrlen"
  ]
  node [
    id 88
    label "para"
  ]
  node [
    id 89
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 90
    label "Rumunia"
  ]
  node [
    id 91
    label "Maroko"
  ]
  node [
    id 92
    label "Bhutan"
  ]
  node [
    id 93
    label "S&#322;owacja"
  ]
  node [
    id 94
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 95
    label "Seszele"
  ]
  node [
    id 96
    label "Kuwejt"
  ]
  node [
    id 97
    label "Arabia_Saudyjska"
  ]
  node [
    id 98
    label "Ekwador"
  ]
  node [
    id 99
    label "Kanada"
  ]
  node [
    id 100
    label "Japonia"
  ]
  node [
    id 101
    label "ziemia"
  ]
  node [
    id 102
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 103
    label "Hiszpania"
  ]
  node [
    id 104
    label "Wyspy_Marshalla"
  ]
  node [
    id 105
    label "Botswana"
  ]
  node [
    id 106
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 107
    label "D&#380;ibuti"
  ]
  node [
    id 108
    label "grupa"
  ]
  node [
    id 109
    label "Wietnam"
  ]
  node [
    id 110
    label "Egipt"
  ]
  node [
    id 111
    label "Burkina_Faso"
  ]
  node [
    id 112
    label "Niemcy"
  ]
  node [
    id 113
    label "Khitai"
  ]
  node [
    id 114
    label "Macedonia"
  ]
  node [
    id 115
    label "Albania"
  ]
  node [
    id 116
    label "Madagaskar"
  ]
  node [
    id 117
    label "Bahrajn"
  ]
  node [
    id 118
    label "Jemen"
  ]
  node [
    id 119
    label "Lesoto"
  ]
  node [
    id 120
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 121
    label "Samoa"
  ]
  node [
    id 122
    label "Andora"
  ]
  node [
    id 123
    label "Chiny"
  ]
  node [
    id 124
    label "Cypr"
  ]
  node [
    id 125
    label "Wielka_Brytania"
  ]
  node [
    id 126
    label "Ukraina"
  ]
  node [
    id 127
    label "Paragwaj"
  ]
  node [
    id 128
    label "Trynidad_i_Tobago"
  ]
  node [
    id 129
    label "Libia"
  ]
  node [
    id 130
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 131
    label "Surinam"
  ]
  node [
    id 132
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 133
    label "Australia"
  ]
  node [
    id 134
    label "Nigeria"
  ]
  node [
    id 135
    label "Honduras"
  ]
  node [
    id 136
    label "Bangladesz"
  ]
  node [
    id 137
    label "Peru"
  ]
  node [
    id 138
    label "Kazachstan"
  ]
  node [
    id 139
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 140
    label "Irak"
  ]
  node [
    id 141
    label "holoarktyka"
  ]
  node [
    id 142
    label "USA"
  ]
  node [
    id 143
    label "Sudan"
  ]
  node [
    id 144
    label "Nepal"
  ]
  node [
    id 145
    label "San_Marino"
  ]
  node [
    id 146
    label "Burundi"
  ]
  node [
    id 147
    label "Dominikana"
  ]
  node [
    id 148
    label "Komory"
  ]
  node [
    id 149
    label "granica_pa&#324;stwa"
  ]
  node [
    id 150
    label "Gwatemala"
  ]
  node [
    id 151
    label "Antarktis"
  ]
  node [
    id 152
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 153
    label "Brunei"
  ]
  node [
    id 154
    label "Iran"
  ]
  node [
    id 155
    label "Zimbabwe"
  ]
  node [
    id 156
    label "Namibia"
  ]
  node [
    id 157
    label "Meksyk"
  ]
  node [
    id 158
    label "Kamerun"
  ]
  node [
    id 159
    label "zwrot"
  ]
  node [
    id 160
    label "Somalia"
  ]
  node [
    id 161
    label "Angola"
  ]
  node [
    id 162
    label "Gabon"
  ]
  node [
    id 163
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 164
    label "Mozambik"
  ]
  node [
    id 165
    label "Tajwan"
  ]
  node [
    id 166
    label "Tunezja"
  ]
  node [
    id 167
    label "Nowa_Zelandia"
  ]
  node [
    id 168
    label "Liban"
  ]
  node [
    id 169
    label "Jordania"
  ]
  node [
    id 170
    label "Tonga"
  ]
  node [
    id 171
    label "Czad"
  ]
  node [
    id 172
    label "Liberia"
  ]
  node [
    id 173
    label "Gwinea"
  ]
  node [
    id 174
    label "Belize"
  ]
  node [
    id 175
    label "&#321;otwa"
  ]
  node [
    id 176
    label "Syria"
  ]
  node [
    id 177
    label "Benin"
  ]
  node [
    id 178
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 179
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 180
    label "Dominika"
  ]
  node [
    id 181
    label "Antigua_i_Barbuda"
  ]
  node [
    id 182
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 183
    label "Hanower"
  ]
  node [
    id 184
    label "partia"
  ]
  node [
    id 185
    label "Afganistan"
  ]
  node [
    id 186
    label "Kiribati"
  ]
  node [
    id 187
    label "W&#322;ochy"
  ]
  node [
    id 188
    label "Szwajcaria"
  ]
  node [
    id 189
    label "Sahara_Zachodnia"
  ]
  node [
    id 190
    label "Chorwacja"
  ]
  node [
    id 191
    label "Tajlandia"
  ]
  node [
    id 192
    label "Salwador"
  ]
  node [
    id 193
    label "Bahamy"
  ]
  node [
    id 194
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 195
    label "S&#322;owenia"
  ]
  node [
    id 196
    label "Gambia"
  ]
  node [
    id 197
    label "Urugwaj"
  ]
  node [
    id 198
    label "Zair"
  ]
  node [
    id 199
    label "Erytrea"
  ]
  node [
    id 200
    label "Rosja"
  ]
  node [
    id 201
    label "Uganda"
  ]
  node [
    id 202
    label "Niger"
  ]
  node [
    id 203
    label "Mauritius"
  ]
  node [
    id 204
    label "Turkmenistan"
  ]
  node [
    id 205
    label "Turcja"
  ]
  node [
    id 206
    label "Irlandia"
  ]
  node [
    id 207
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 208
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 209
    label "Gwinea_Bissau"
  ]
  node [
    id 210
    label "Belgia"
  ]
  node [
    id 211
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 212
    label "Palau"
  ]
  node [
    id 213
    label "Barbados"
  ]
  node [
    id 214
    label "Chile"
  ]
  node [
    id 215
    label "Wenezuela"
  ]
  node [
    id 216
    label "W&#281;gry"
  ]
  node [
    id 217
    label "Argentyna"
  ]
  node [
    id 218
    label "Kolumbia"
  ]
  node [
    id 219
    label "Sierra_Leone"
  ]
  node [
    id 220
    label "Azerbejd&#380;an"
  ]
  node [
    id 221
    label "Kongo"
  ]
  node [
    id 222
    label "Pakistan"
  ]
  node [
    id 223
    label "Liechtenstein"
  ]
  node [
    id 224
    label "Nikaragua"
  ]
  node [
    id 225
    label "Senegal"
  ]
  node [
    id 226
    label "Indie"
  ]
  node [
    id 227
    label "Suazi"
  ]
  node [
    id 228
    label "Polska"
  ]
  node [
    id 229
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 230
    label "Algieria"
  ]
  node [
    id 231
    label "terytorium"
  ]
  node [
    id 232
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 233
    label "Jamajka"
  ]
  node [
    id 234
    label "Kostaryka"
  ]
  node [
    id 235
    label "Timor_Wschodni"
  ]
  node [
    id 236
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 237
    label "Kuba"
  ]
  node [
    id 238
    label "Mauretania"
  ]
  node [
    id 239
    label "Portoryko"
  ]
  node [
    id 240
    label "Brazylia"
  ]
  node [
    id 241
    label "Mo&#322;dawia"
  ]
  node [
    id 242
    label "organizacja"
  ]
  node [
    id 243
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 244
    label "Litwa"
  ]
  node [
    id 245
    label "Kirgistan"
  ]
  node [
    id 246
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 247
    label "Izrael"
  ]
  node [
    id 248
    label "Grecja"
  ]
  node [
    id 249
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 250
    label "Holandia"
  ]
  node [
    id 251
    label "Sri_Lanka"
  ]
  node [
    id 252
    label "Katar"
  ]
  node [
    id 253
    label "Mikronezja"
  ]
  node [
    id 254
    label "Mongolia"
  ]
  node [
    id 255
    label "Laos"
  ]
  node [
    id 256
    label "Malediwy"
  ]
  node [
    id 257
    label "Zambia"
  ]
  node [
    id 258
    label "Tanzania"
  ]
  node [
    id 259
    label "Gujana"
  ]
  node [
    id 260
    label "Czechy"
  ]
  node [
    id 261
    label "Panama"
  ]
  node [
    id 262
    label "Uzbekistan"
  ]
  node [
    id 263
    label "Gruzja"
  ]
  node [
    id 264
    label "Serbia"
  ]
  node [
    id 265
    label "Francja"
  ]
  node [
    id 266
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 267
    label "Togo"
  ]
  node [
    id 268
    label "Estonia"
  ]
  node [
    id 269
    label "Oman"
  ]
  node [
    id 270
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 271
    label "Portugalia"
  ]
  node [
    id 272
    label "Boliwia"
  ]
  node [
    id 273
    label "Luksemburg"
  ]
  node [
    id 274
    label "Haiti"
  ]
  node [
    id 275
    label "Wyspy_Salomona"
  ]
  node [
    id 276
    label "Birma"
  ]
  node [
    id 277
    label "Rodezja"
  ]
  node [
    id 278
    label "proceed"
  ]
  node [
    id 279
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 280
    label "bangla&#263;"
  ]
  node [
    id 281
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 282
    label "by&#263;"
  ]
  node [
    id 283
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 284
    label "run"
  ]
  node [
    id 285
    label "p&#322;ywa&#263;"
  ]
  node [
    id 286
    label "continue"
  ]
  node [
    id 287
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 288
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 289
    label "przebiega&#263;"
  ]
  node [
    id 290
    label "mie&#263;_miejsce"
  ]
  node [
    id 291
    label "wk&#322;ada&#263;"
  ]
  node [
    id 292
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 293
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 294
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 295
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 296
    label "krok"
  ]
  node [
    id 297
    label "str&#243;j"
  ]
  node [
    id 298
    label "bywa&#263;"
  ]
  node [
    id 299
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 300
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 301
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 302
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 303
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 304
    label "dziama&#263;"
  ]
  node [
    id 305
    label "stara&#263;_si&#281;"
  ]
  node [
    id 306
    label "carry"
  ]
  node [
    id 307
    label "spis"
  ]
  node [
    id 308
    label "odinstalowanie"
  ]
  node [
    id 309
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 310
    label "za&#322;o&#380;enie"
  ]
  node [
    id 311
    label "podstawa"
  ]
  node [
    id 312
    label "emitowanie"
  ]
  node [
    id 313
    label "odinstalowywanie"
  ]
  node [
    id 314
    label "instrukcja"
  ]
  node [
    id 315
    label "punkt"
  ]
  node [
    id 316
    label "teleferie"
  ]
  node [
    id 317
    label "emitowa&#263;"
  ]
  node [
    id 318
    label "wytw&#243;r"
  ]
  node [
    id 319
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 320
    label "sekcja_krytyczna"
  ]
  node [
    id 321
    label "prezentowa&#263;"
  ]
  node [
    id 322
    label "blok"
  ]
  node [
    id 323
    label "podprogram"
  ]
  node [
    id 324
    label "dzia&#322;"
  ]
  node [
    id 325
    label "broszura"
  ]
  node [
    id 326
    label "deklaracja"
  ]
  node [
    id 327
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 328
    label "struktura_organizacyjna"
  ]
  node [
    id 329
    label "zaprezentowanie"
  ]
  node [
    id 330
    label "informatyka"
  ]
  node [
    id 331
    label "booklet"
  ]
  node [
    id 332
    label "menu"
  ]
  node [
    id 333
    label "oprogramowanie"
  ]
  node [
    id 334
    label "instalowanie"
  ]
  node [
    id 335
    label "furkacja"
  ]
  node [
    id 336
    label "odinstalowa&#263;"
  ]
  node [
    id 337
    label "instalowa&#263;"
  ]
  node [
    id 338
    label "okno"
  ]
  node [
    id 339
    label "pirat"
  ]
  node [
    id 340
    label "zainstalowanie"
  ]
  node [
    id 341
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 342
    label "ogranicznik_referencyjny"
  ]
  node [
    id 343
    label "zainstalowa&#263;"
  ]
  node [
    id 344
    label "kana&#322;"
  ]
  node [
    id 345
    label "zaprezentowa&#263;"
  ]
  node [
    id 346
    label "interfejs"
  ]
  node [
    id 347
    label "odinstalowywa&#263;"
  ]
  node [
    id 348
    label "folder"
  ]
  node [
    id 349
    label "course_of_study"
  ]
  node [
    id 350
    label "ram&#243;wka"
  ]
  node [
    id 351
    label "prezentowanie"
  ]
  node [
    id 352
    label "oferta"
  ]
  node [
    id 353
    label "uprawi&#263;"
  ]
  node [
    id 354
    label "gotowy"
  ]
  node [
    id 355
    label "might"
  ]
  node [
    id 356
    label "wej&#347;&#263;"
  ]
  node [
    id 357
    label "cause"
  ]
  node [
    id 358
    label "zacz&#261;&#263;"
  ]
  node [
    id 359
    label "mount"
  ]
  node [
    id 360
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 361
    label "funkcjonowa&#263;"
  ]
  node [
    id 362
    label "kategoria_gramatyczna"
  ]
  node [
    id 363
    label "skala"
  ]
  node [
    id 364
    label "cecha"
  ]
  node [
    id 365
    label "ko&#322;o_z&#281;bate"
  ]
  node [
    id 366
    label "z&#261;b"
  ]
  node [
    id 367
    label "modalno&#347;&#263;"
  ]
  node [
    id 368
    label "koniugacja"
  ]
  node [
    id 369
    label "ko&#322;o"
  ]
  node [
    id 370
    label "spos&#243;b"
  ]
  node [
    id 371
    label "pozakonkursowo"
  ]
  node [
    id 372
    label "wyj&#261;tkowy"
  ]
  node [
    id 373
    label "dodatkowy"
  ]
  node [
    id 374
    label "get"
  ]
  node [
    id 375
    label "przewa&#380;a&#263;"
  ]
  node [
    id 376
    label "zobowi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 377
    label "poczytywa&#263;"
  ]
  node [
    id 378
    label "levy"
  ]
  node [
    id 379
    label "pokonywa&#263;"
  ]
  node [
    id 380
    label "u&#380;ywa&#263;"
  ]
  node [
    id 381
    label "rusza&#263;"
  ]
  node [
    id 382
    label "zalicza&#263;"
  ]
  node [
    id 383
    label "wygrywa&#263;"
  ]
  node [
    id 384
    label "open"
  ]
  node [
    id 385
    label "branie"
  ]
  node [
    id 386
    label "korzysta&#263;"
  ]
  node [
    id 387
    label "&#263;pa&#263;"
  ]
  node [
    id 388
    label "wch&#322;ania&#263;"
  ]
  node [
    id 389
    label "interpretowa&#263;"
  ]
  node [
    id 390
    label "atakowa&#263;"
  ]
  node [
    id 391
    label "prowadzi&#263;"
  ]
  node [
    id 392
    label "rucha&#263;"
  ]
  node [
    id 393
    label "take"
  ]
  node [
    id 394
    label "dostawa&#263;"
  ]
  node [
    id 395
    label "wzi&#261;&#263;"
  ]
  node [
    id 396
    label "chwyta&#263;"
  ]
  node [
    id 397
    label "arise"
  ]
  node [
    id 398
    label "za&#380;ywa&#263;"
  ]
  node [
    id 399
    label "uprawia&#263;_seks"
  ]
  node [
    id 400
    label "porywa&#263;"
  ]
  node [
    id 401
    label "robi&#263;"
  ]
  node [
    id 402
    label "grza&#263;"
  ]
  node [
    id 403
    label "abstract"
  ]
  node [
    id 404
    label "zaopatrywa&#263;_si&#281;"
  ]
  node [
    id 405
    label "wzmaga&#263;_si&#281;"
  ]
  node [
    id 406
    label "towarzystwo"
  ]
  node [
    id 407
    label "otrzymywa&#263;"
  ]
  node [
    id 408
    label "przyjmowa&#263;"
  ]
  node [
    id 409
    label "wchodzi&#263;"
  ]
  node [
    id 410
    label "ucieka&#263;"
  ]
  node [
    id 411
    label "odurza&#263;_si&#281;"
  ]
  node [
    id 412
    label "buzowa&#263;_si&#281;"
  ]
  node [
    id 413
    label "&#322;apa&#263;"
  ]
  node [
    id 414
    label "raise"
  ]
  node [
    id 415
    label "obecno&#347;&#263;"
  ]
  node [
    id 416
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 417
    label "kwota"
  ]
  node [
    id 418
    label "ilo&#347;&#263;"
  ]
  node [
    id 419
    label "nijaki"
  ]
  node [
    id 420
    label "dob&#243;r_naturalny"
  ]
  node [
    id 421
    label "firma"
  ]
  node [
    id 422
    label "dyscyplina_sportowa"
  ]
  node [
    id 423
    label "interakcja"
  ]
  node [
    id 424
    label "wydarzenie"
  ]
  node [
    id 425
    label "rywalizacja"
  ]
  node [
    id 426
    label "uczestnik"
  ]
  node [
    id 427
    label "contest"
  ]
  node [
    id 428
    label "need"
  ]
  node [
    id 429
    label "pragn&#261;&#263;"
  ]
  node [
    id 430
    label "zaleca&#263;_si&#281;"
  ]
  node [
    id 431
    label "cope"
  ]
  node [
    id 432
    label "kolejny"
  ]
  node [
    id 433
    label "inaczej"
  ]
  node [
    id 434
    label "r&#243;&#380;ny"
  ]
  node [
    id 435
    label "inszy"
  ]
  node [
    id 436
    label "osobno"
  ]
  node [
    id 437
    label "Brac&#322;aw"
  ]
  node [
    id 438
    label "Zb&#261;szy&#324;"
  ]
  node [
    id 439
    label "G&#322;uch&#243;w"
  ]
  node [
    id 440
    label "Hallstatt"
  ]
  node [
    id 441
    label "Zbara&#380;"
  ]
  node [
    id 442
    label "Wielkie_Tyrnowo"
  ]
  node [
    id 443
    label "Nachiczewan"
  ]
  node [
    id 444
    label "Suworow"
  ]
  node [
    id 445
    label "Halicz"
  ]
  node [
    id 446
    label "Gandawa"
  ]
  node [
    id 447
    label "Kamionka_Strumi&#322;owa"
  ]
  node [
    id 448
    label "Wismar"
  ]
  node [
    id 449
    label "Norymberga"
  ]
  node [
    id 450
    label "Ruciane-Nida"
  ]
  node [
    id 451
    label "Wia&#378;ma"
  ]
  node [
    id 452
    label "Sewilla"
  ]
  node [
    id 453
    label "Pietropaw&#322;owsk"
  ]
  node [
    id 454
    label "Kobry&#324;"
  ]
  node [
    id 455
    label "Brno"
  ]
  node [
    id 456
    label "Tomsk"
  ]
  node [
    id 457
    label "Poniatowa"
  ]
  node [
    id 458
    label "Hadziacz"
  ]
  node [
    id 459
    label "Tiume&#324;"
  ]
  node [
    id 460
    label "Karlsbad"
  ]
  node [
    id 461
    label "Drohobycz"
  ]
  node [
    id 462
    label "Lyon"
  ]
  node [
    id 463
    label "Dzier&#380;y&#324;sk"
  ]
  node [
    id 464
    label "K&#322;odawa"
  ]
  node [
    id 465
    label "Solikamsk"
  ]
  node [
    id 466
    label "Wolgast"
  ]
  node [
    id 467
    label "Saloniki"
  ]
  node [
    id 468
    label "Lw&#243;w"
  ]
  node [
    id 469
    label "Al-Kufa"
  ]
  node [
    id 470
    label "Hamburg"
  ]
  node [
    id 471
    label "Bogusz&#243;w-Gorce"
  ]
  node [
    id 472
    label "Nampula"
  ]
  node [
    id 473
    label "burmistrz"
  ]
  node [
    id 474
    label "D&#252;sseldorf"
  ]
  node [
    id 475
    label "Nowy_Orlean"
  ]
  node [
    id 476
    label "Bamberg"
  ]
  node [
    id 477
    label "Osaka"
  ]
  node [
    id 478
    label "Niko&#322;ajewsk_nad_Amurem"
  ]
  node [
    id 479
    label "Michalovce"
  ]
  node [
    id 480
    label "Fryburg"
  ]
  node [
    id 481
    label "Trabzon"
  ]
  node [
    id 482
    label "Wersal"
  ]
  node [
    id 483
    label "Swatowe"
  ]
  node [
    id 484
    label "Ka&#322;uga"
  ]
  node [
    id 485
    label "Dijon"
  ]
  node [
    id 486
    label "Cannes"
  ]
  node [
    id 487
    label "Borowsk"
  ]
  node [
    id 488
    label "Kursk"
  ]
  node [
    id 489
    label "Tyberiada"
  ]
  node [
    id 490
    label "Boden"
  ]
  node [
    id 491
    label "Dodona"
  ]
  node [
    id 492
    label "Vukovar"
  ]
  node [
    id 493
    label "Soleczniki"
  ]
  node [
    id 494
    label "Barcelona"
  ]
  node [
    id 495
    label "Oszmiana"
  ]
  node [
    id 496
    label "Stuttgart"
  ]
  node [
    id 497
    label "Nerczy&#324;sk"
  ]
  node [
    id 498
    label "Bijsk"
  ]
  node [
    id 499
    label "Essen"
  ]
  node [
    id 500
    label "Luboml"
  ]
  node [
    id 501
    label "Gr&#243;dek"
  ]
  node [
    id 502
    label "Orany"
  ]
  node [
    id 503
    label "Siedliszcze"
  ]
  node [
    id 504
    label "P&#322;owdiw"
  ]
  node [
    id 505
    label "A&#322;apajewsk"
  ]
  node [
    id 506
    label "Liverpool"
  ]
  node [
    id 507
    label "Ostrawa"
  ]
  node [
    id 508
    label "Penza"
  ]
  node [
    id 509
    label "Rudki"
  ]
  node [
    id 510
    label "Aktobe"
  ]
  node [
    id 511
    label "I&#322;awka"
  ]
  node [
    id 512
    label "Tolkmicko"
  ]
  node [
    id 513
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 514
    label "Sajgon"
  ]
  node [
    id 515
    label "Windawa"
  ]
  node [
    id 516
    label "Weimar"
  ]
  node [
    id 517
    label "Jekaterynburg"
  ]
  node [
    id 518
    label "Lejda"
  ]
  node [
    id 519
    label "Cremona"
  ]
  node [
    id 520
    label "Wo&#322;godo&#324;sk"
  ]
  node [
    id 521
    label "Kordoba"
  ]
  node [
    id 522
    label "urz&#261;d"
  ]
  node [
    id 523
    label "&#321;ohojsk"
  ]
  node [
    id 524
    label "Kalmar"
  ]
  node [
    id 525
    label "Akerman"
  ]
  node [
    id 526
    label "Locarno"
  ]
  node [
    id 527
    label "Bych&#243;w"
  ]
  node [
    id 528
    label "Toledo"
  ]
  node [
    id 529
    label "Minusi&#324;sk"
  ]
  node [
    id 530
    label "Szk&#322;&#243;w"
  ]
  node [
    id 531
    label "Wenecja"
  ]
  node [
    id 532
    label "Bazylea"
  ]
  node [
    id 533
    label "Peszt"
  ]
  node [
    id 534
    label "Piza"
  ]
  node [
    id 535
    label "Tanger"
  ]
  node [
    id 536
    label "Krzywi&#324;"
  ]
  node [
    id 537
    label "Eger"
  ]
  node [
    id 538
    label "Bogus&#322;aw"
  ]
  node [
    id 539
    label "Taganrog"
  ]
  node [
    id 540
    label "Oksford"
  ]
  node [
    id 541
    label "Gwardiejsk"
  ]
  node [
    id 542
    label "Tyraspol"
  ]
  node [
    id 543
    label "Kleczew"
  ]
  node [
    id 544
    label "Nowa_D&#281;ba"
  ]
  node [
    id 545
    label "Wilejka"
  ]
  node [
    id 546
    label "Modena"
  ]
  node [
    id 547
    label "Demmin"
  ]
  node [
    id 548
    label "Houston"
  ]
  node [
    id 549
    label "Rydu&#322;towy"
  ]
  node [
    id 550
    label "Bordeaux"
  ]
  node [
    id 551
    label "Schmalkalden"
  ]
  node [
    id 552
    label "O&#322;omuniec"
  ]
  node [
    id 553
    label "Tuluza"
  ]
  node [
    id 554
    label "tramwaj"
  ]
  node [
    id 555
    label "Nantes"
  ]
  node [
    id 556
    label "Debreczyn"
  ]
  node [
    id 557
    label "Kowel"
  ]
  node [
    id 558
    label "Witnica"
  ]
  node [
    id 559
    label "Stalingrad"
  ]
  node [
    id 560
    label "Drezno"
  ]
  node [
    id 561
    label "Perejas&#322;aw"
  ]
  node [
    id 562
    label "Luksor"
  ]
  node [
    id 563
    label "Ostaszk&#243;w"
  ]
  node [
    id 564
    label "Gettysburg"
  ]
  node [
    id 565
    label "Trydent"
  ]
  node [
    id 566
    label "Poczdam"
  ]
  node [
    id 567
    label "Mesyna"
  ]
  node [
    id 568
    label "Krasnogorsk"
  ]
  node [
    id 569
    label "Kars"
  ]
  node [
    id 570
    label "Darmstadt"
  ]
  node [
    id 571
    label "Rzg&#243;w"
  ]
  node [
    id 572
    label "Kar&#322;owice"
  ]
  node [
    id 573
    label "Czeskie_Budziejowice"
  ]
  node [
    id 574
    label "Buda"
  ]
  node [
    id 575
    label "Pardubice"
  ]
  node [
    id 576
    label "Pas&#322;&#281;k"
  ]
  node [
    id 577
    label "Fatima"
  ]
  node [
    id 578
    label "Bir&#380;e"
  ]
  node [
    id 579
    label "Wo&#322;oko&#322;amsk"
  ]
  node [
    id 580
    label "Wi&#322;komierz"
  ]
  node [
    id 581
    label "Opawa"
  ]
  node [
    id 582
    label "Mantua"
  ]
  node [
    id 583
    label "ulica"
  ]
  node [
    id 584
    label "Tarragona"
  ]
  node [
    id 585
    label "Antwerpia"
  ]
  node [
    id 586
    label "Asuan"
  ]
  node [
    id 587
    label "Korynt"
  ]
  node [
    id 588
    label "Budionnowsk"
  ]
  node [
    id 589
    label "Lengyel"
  ]
  node [
    id 590
    label "Betlejem"
  ]
  node [
    id 591
    label "Asy&#380;"
  ]
  node [
    id 592
    label "Batumi"
  ]
  node [
    id 593
    label "Paczk&#243;w"
  ]
  node [
    id 594
    label "Suczawa"
  ]
  node [
    id 595
    label "Nowogard"
  ]
  node [
    id 596
    label "Tyr"
  ]
  node [
    id 597
    label "Bria&#324;sk"
  ]
  node [
    id 598
    label "Bar"
  ]
  node [
    id 599
    label "Czerkiesk"
  ]
  node [
    id 600
    label "Ja&#322;ta"
  ]
  node [
    id 601
    label "Mo&#347;ciska"
  ]
  node [
    id 602
    label "Medyna"
  ]
  node [
    id 603
    label "Tartu"
  ]
  node [
    id 604
    label "Pemba"
  ]
  node [
    id 605
    label "Lipawa"
  ]
  node [
    id 606
    label "Tyl&#380;a"
  ]
  node [
    id 607
    label "Lipsk"
  ]
  node [
    id 608
    label "Dayton"
  ]
  node [
    id 609
    label "Rohatyn"
  ]
  node [
    id 610
    label "Peszawar"
  ]
  node [
    id 611
    label "Azow"
  ]
  node [
    id 612
    label "Adrianopol"
  ]
  node [
    id 613
    label "Iwano-Frankowsk"
  ]
  node [
    id 614
    label "Czarnobyl"
  ]
  node [
    id 615
    label "Rakoniewice"
  ]
  node [
    id 616
    label "Obuch&#243;w"
  ]
  node [
    id 617
    label "Orneta"
  ]
  node [
    id 618
    label "Koszyce"
  ]
  node [
    id 619
    label "Czeski_Cieszyn"
  ]
  node [
    id 620
    label "Zagorsk"
  ]
  node [
    id 621
    label "Nieder_Selters"
  ]
  node [
    id 622
    label "Ko&#322;omna"
  ]
  node [
    id 623
    label "Rost&#243;w"
  ]
  node [
    id 624
    label "Bolonia"
  ]
  node [
    id 625
    label "Rajgr&#243;d"
  ]
  node [
    id 626
    label "L&#252;neburg"
  ]
  node [
    id 627
    label "Brack"
  ]
  node [
    id 628
    label "Konstancja"
  ]
  node [
    id 629
    label "Koluszki"
  ]
  node [
    id 630
    label "Semipa&#322;aty&#324;sk"
  ]
  node [
    id 631
    label "Suez"
  ]
  node [
    id 632
    label "Mrocza"
  ]
  node [
    id 633
    label "Triest"
  ]
  node [
    id 634
    label "Murma&#324;sk"
  ]
  node [
    id 635
    label "Tu&#322;a"
  ]
  node [
    id 636
    label "Tarnogr&#243;d"
  ]
  node [
    id 637
    label "Radziech&#243;w"
  ]
  node [
    id 638
    label "Kokand"
  ]
  node [
    id 639
    label "Kircholm"
  ]
  node [
    id 640
    label "Nowa_Ruda"
  ]
  node [
    id 641
    label "Huma&#324;"
  ]
  node [
    id 642
    label "Turkiestan"
  ]
  node [
    id 643
    label "Kani&#243;w"
  ]
  node [
    id 644
    label "Pilzno"
  ]
  node [
    id 645
    label "Dubno"
  ]
  node [
    id 646
    label "Bras&#322;aw"
  ]
  node [
    id 647
    label "Korfant&#243;w"
  ]
  node [
    id 648
    label "Choroszcz"
  ]
  node [
    id 649
    label "Nowogr&#243;d"
  ]
  node [
    id 650
    label "Konotop"
  ]
  node [
    id 651
    label "M&#347;cis&#322;aw"
  ]
  node [
    id 652
    label "Jastarnia"
  ]
  node [
    id 653
    label "Ba&#324;ska_Bystrzyca"
  ]
  node [
    id 654
    label "Omsk"
  ]
  node [
    id 655
    label "Troick"
  ]
  node [
    id 656
    label "Koper"
  ]
  node [
    id 657
    label "Jenisejsk"
  ]
  node [
    id 658
    label "Frankfurt_nad_Odr&#261;"
  ]
  node [
    id 659
    label "Oldenburg_in_Holstein"
  ]
  node [
    id 660
    label "Trenczyn"
  ]
  node [
    id 661
    label "Wormacja"
  ]
  node [
    id 662
    label "Wagram"
  ]
  node [
    id 663
    label "Lubeka"
  ]
  node [
    id 664
    label "Genewa"
  ]
  node [
    id 665
    label "&#379;ydacz&#243;w"
  ]
  node [
    id 666
    label "Kleck"
  ]
  node [
    id 667
    label "G&#243;ra_Kalwaria"
  ]
  node [
    id 668
    label "Struga"
  ]
  node [
    id 669
    label "Izmir"
  ]
  node [
    id 670
    label "Dortmund"
  ]
  node [
    id 671
    label "Izbica_Kujawska"
  ]
  node [
    id 672
    label "Stalinogorsk"
  ]
  node [
    id 673
    label "Workuta"
  ]
  node [
    id 674
    label "Jerycho"
  ]
  node [
    id 675
    label "Brunszwik"
  ]
  node [
    id 676
    label "Aleksandria"
  ]
  node [
    id 677
    label "miejscowo&#347;&#263;"
  ]
  node [
    id 678
    label "Borys&#322;aw"
  ]
  node [
    id 679
    label "Zaleszczyki"
  ]
  node [
    id 680
    label "Z&#322;oczew"
  ]
  node [
    id 681
    label "Piast&#243;w"
  ]
  node [
    id 682
    label "Frankfurt_nad_Menem"
  ]
  node [
    id 683
    label "Bor"
  ]
  node [
    id 684
    label "Nazaret"
  ]
  node [
    id 685
    label "Sarat&#243;w"
  ]
  node [
    id 686
    label "Brasz&#243;w"
  ]
  node [
    id 687
    label "Malin"
  ]
  node [
    id 688
    label "Parma"
  ]
  node [
    id 689
    label "Wierchoja&#324;sk"
  ]
  node [
    id 690
    label "Tarent"
  ]
  node [
    id 691
    label "Mariampol"
  ]
  node [
    id 692
    label "Wuhan"
  ]
  node [
    id 693
    label "Split"
  ]
  node [
    id 694
    label "Baranowicze"
  ]
  node [
    id 695
    label "Marki"
  ]
  node [
    id 696
    label "Adana"
  ]
  node [
    id 697
    label "B&#322;aszki"
  ]
  node [
    id 698
    label "Lubecz"
  ]
  node [
    id 699
    label "Sulech&#243;w"
  ]
  node [
    id 700
    label "Borys&#243;w"
  ]
  node [
    id 701
    label "Homel"
  ]
  node [
    id 702
    label "Tours"
  ]
  node [
    id 703
    label "Kapsztad"
  ]
  node [
    id 704
    label "Edam"
  ]
  node [
    id 705
    label "Zaporo&#380;e"
  ]
  node [
    id 706
    label "Miedwie&#380;jegorsk"
  ]
  node [
    id 707
    label "Kamieniec_Podolski"
  ]
  node [
    id 708
    label "Chocim"
  ]
  node [
    id 709
    label "Mohylew"
  ]
  node [
    id 710
    label "Merseburg"
  ]
  node [
    id 711
    label "Konstantynopol"
  ]
  node [
    id 712
    label "Sambor"
  ]
  node [
    id 713
    label "Manchester"
  ]
  node [
    id 714
    label "Pi&#324;sk"
  ]
  node [
    id 715
    label "Ochryda"
  ]
  node [
    id 716
    label "Rybi&#324;sk"
  ]
  node [
    id 717
    label "Czadca"
  ]
  node [
    id 718
    label "Orenburg"
  ]
  node [
    id 719
    label "Krajowa"
  ]
  node [
    id 720
    label "Eleusis"
  ]
  node [
    id 721
    label "Awinion"
  ]
  node [
    id 722
    label "Rzeczyca"
  ]
  node [
    id 723
    label "Barczewo"
  ]
  node [
    id 724
    label "Lozanna"
  ]
  node [
    id 725
    label "&#379;migr&#243;d"
  ]
  node [
    id 726
    label "Chabarowsk"
  ]
  node [
    id 727
    label "Jena"
  ]
  node [
    id 728
    label "Xai-Xai"
  ]
  node [
    id 729
    label "Radk&#243;w"
  ]
  node [
    id 730
    label "Syrakuzy"
  ]
  node [
    id 731
    label "Zas&#322;aw"
  ]
  node [
    id 732
    label "Getynga"
  ]
  node [
    id 733
    label "Windsor"
  ]
  node [
    id 734
    label "Carrara"
  ]
  node [
    id 735
    label "Madras"
  ]
  node [
    id 736
    label "Nitra"
  ]
  node [
    id 737
    label "Kilonia"
  ]
  node [
    id 738
    label "Rawenna"
  ]
  node [
    id 739
    label "Stawropol"
  ]
  node [
    id 740
    label "Warna"
  ]
  node [
    id 741
    label "Ba&#322;tijsk"
  ]
  node [
    id 742
    label "Cumana"
  ]
  node [
    id 743
    label "Kostroma"
  ]
  node [
    id 744
    label "Bajonna"
  ]
  node [
    id 745
    label "Magadan"
  ]
  node [
    id 746
    label "Kercz"
  ]
  node [
    id 747
    label "Harbin"
  ]
  node [
    id 748
    label "Sankt_Florian"
  ]
  node [
    id 749
    label "Norak"
  ]
  node [
    id 750
    label "Wo&#322;kowysk"
  ]
  node [
    id 751
    label "Bia&#322;ogr&#243;d_nad_Dniestrem"
  ]
  node [
    id 752
    label "S&#232;vres"
  ]
  node [
    id 753
    label "Barwice"
  ]
  node [
    id 754
    label "Jutrosin"
  ]
  node [
    id 755
    label "Sumy"
  ]
  node [
    id 756
    label "Canterbury"
  ]
  node [
    id 757
    label "Czerkasy"
  ]
  node [
    id 758
    label "Troki"
  ]
  node [
    id 759
    label "Dnieprodzier&#380;y&#324;sk"
  ]
  node [
    id 760
    label "Turka"
  ]
  node [
    id 761
    label "Budziszyn"
  ]
  node [
    id 762
    label "A&#322;czewsk"
  ]
  node [
    id 763
    label "Chark&#243;w"
  ]
  node [
    id 764
    label "Go&#347;cino"
  ]
  node [
    id 765
    label "Ku&#378;nieck"
  ]
  node [
    id 766
    label "Wotki&#324;sk"
  ]
  node [
    id 767
    label "Symferopol"
  ]
  node [
    id 768
    label "Dmitrow"
  ]
  node [
    id 769
    label "Cherso&#324;"
  ]
  node [
    id 770
    label "zabudowa"
  ]
  node [
    id 771
    label "Nowogr&#243;dek"
  ]
  node [
    id 772
    label "Orlean"
  ]
  node [
    id 773
    label "Ku&#378;nia_Raciborska"
  ]
  node [
    id 774
    label "Berdia&#324;sk"
  ]
  node [
    id 775
    label "Szumsk"
  ]
  node [
    id 776
    label "Z&#322;ocz&#243;w"
  ]
  node [
    id 777
    label "Orsza"
  ]
  node [
    id 778
    label "Cluny"
  ]
  node [
    id 779
    label "Aralsk"
  ]
  node [
    id 780
    label "Stoczek_&#321;ukowski"
  ]
  node [
    id 781
    label "Bogumin"
  ]
  node [
    id 782
    label "Antiochia"
  ]
  node [
    id 783
    label "Inhambane"
  ]
  node [
    id 784
    label "S&#322;awia&#324;sk_nad_Kubaniem"
  ]
  node [
    id 785
    label "Wielki_Pres&#322;aw"
  ]
  node [
    id 786
    label "Trewir"
  ]
  node [
    id 787
    label "Nowogr&#243;d_Wielki"
  ]
  node [
    id 788
    label "Siewieromorsk"
  ]
  node [
    id 789
    label "Calais"
  ]
  node [
    id 790
    label "&#379;ytawa"
  ]
  node [
    id 791
    label "Eupatoria"
  ]
  node [
    id 792
    label "Twer"
  ]
  node [
    id 793
    label "Stara_Zagora"
  ]
  node [
    id 794
    label "Jastrowie"
  ]
  node [
    id 795
    label "Piatigorsk"
  ]
  node [
    id 796
    label "&#379;&#243;&#322;kiew"
  ]
  node [
    id 797
    label "Le&#324;sk"
  ]
  node [
    id 798
    label "Johannesburg"
  ]
  node [
    id 799
    label "Kaszyn"
  ]
  node [
    id 800
    label "&#346;wi&#281;ciany"
  ]
  node [
    id 801
    label "&#379;ylina"
  ]
  node [
    id 802
    label "Sewastopol"
  ]
  node [
    id 803
    label "Pietrozawodsk"
  ]
  node [
    id 804
    label "Bobolice"
  ]
  node [
    id 805
    label "Mosty"
  ]
  node [
    id 806
    label "Korsu&#324;_Szewczenkowski"
  ]
  node [
    id 807
    label "Karaganda"
  ]
  node [
    id 808
    label "Marsylia"
  ]
  node [
    id 809
    label "Buchara"
  ]
  node [
    id 810
    label "Dubrownik"
  ]
  node [
    id 811
    label "Be&#322;z"
  ]
  node [
    id 812
    label "Oran"
  ]
  node [
    id 813
    label "Regensburg"
  ]
  node [
    id 814
    label "Rotterdam"
  ]
  node [
    id 815
    label "Trembowla"
  ]
  node [
    id 816
    label "Woskriesiensk"
  ]
  node [
    id 817
    label "Po&#322;ock"
  ]
  node [
    id 818
    label "Poprad"
  ]
  node [
    id 819
    label "Los_Angeles"
  ]
  node [
    id 820
    label "Kronsztad"
  ]
  node [
    id 821
    label "U&#322;an_Ude"
  ]
  node [
    id 822
    label "U&#347;cie_nad_&#321;ab&#261;"
  ]
  node [
    id 823
    label "W&#322;adywostok"
  ]
  node [
    id 824
    label "Kandahar"
  ]
  node [
    id 825
    label "Tobolsk"
  ]
  node [
    id 826
    label "Boston"
  ]
  node [
    id 827
    label "Hawana"
  ]
  node [
    id 828
    label "Kis&#322;owodzk"
  ]
  node [
    id 829
    label "Tulon"
  ]
  node [
    id 830
    label "Utrecht"
  ]
  node [
    id 831
    label "Oleszyce"
  ]
  node [
    id 832
    label "Hradec_Kr&#225;lov&#233;"
  ]
  node [
    id 833
    label "Katania"
  ]
  node [
    id 834
    label "Teby"
  ]
  node [
    id 835
    label "Paw&#322;owo"
  ]
  node [
    id 836
    label "W&#252;rzburg"
  ]
  node [
    id 837
    label "Podiebrady"
  ]
  node [
    id 838
    label "Uppsala"
  ]
  node [
    id 839
    label "Poniewie&#380;"
  ]
  node [
    id 840
    label "Berezyna"
  ]
  node [
    id 841
    label "Aczy&#324;sk"
  ]
  node [
    id 842
    label "Niko&#322;ajewsk"
  ]
  node [
    id 843
    label "Ostr&#243;g"
  ]
  node [
    id 844
    label "Brze&#347;&#263;"
  ]
  node [
    id 845
    label "Stryj"
  ]
  node [
    id 846
    label "Lancaster"
  ]
  node [
    id 847
    label "Kozielsk"
  ]
  node [
    id 848
    label "Loreto"
  ]
  node [
    id 849
    label "B&#322;agowieszcze&#324;sk"
  ]
  node [
    id 850
    label "Hebron"
  ]
  node [
    id 851
    label "Kaspijsk"
  ]
  node [
    id 852
    label "Peczora"
  ]
  node [
    id 853
    label "Isfahan"
  ]
  node [
    id 854
    label "Chimoio"
  ]
  node [
    id 855
    label "Mory&#324;"
  ]
  node [
    id 856
    label "Kowno"
  ]
  node [
    id 857
    label "Nie&#347;wie&#380;"
  ]
  node [
    id 858
    label "Opalenica"
  ]
  node [
    id 859
    label "Kolonia"
  ]
  node [
    id 860
    label "Stary_Sambor"
  ]
  node [
    id 861
    label "Kolkata"
  ]
  node [
    id 862
    label "Turkmenbaszy"
  ]
  node [
    id 863
    label "Mohyl&#243;w_Podolski"
  ]
  node [
    id 864
    label "Nankin"
  ]
  node [
    id 865
    label "Krzanowice"
  ]
  node [
    id 866
    label "Efez"
  ]
  node [
    id 867
    label "Dobrodzie&#324;"
  ]
  node [
    id 868
    label "Neapol"
  ]
  node [
    id 869
    label "S&#322;uck"
  ]
  node [
    id 870
    label "Fryburg_Bryzgowijski"
  ]
  node [
    id 871
    label "W&#322;odzimierz_Wo&#322;y&#324;ski"
  ]
  node [
    id 872
    label "Frydek-Mistek"
  ]
  node [
    id 873
    label "Korsze"
  ]
  node [
    id 874
    label "T&#322;uszcz"
  ]
  node [
    id 875
    label "Soligorsk"
  ]
  node [
    id 876
    label "Kie&#380;mark"
  ]
  node [
    id 877
    label "Mannheim"
  ]
  node [
    id 878
    label "Ulm"
  ]
  node [
    id 879
    label "Podhajce"
  ]
  node [
    id 880
    label "Dniepropetrowsk"
  ]
  node [
    id 881
    label "Szamocin"
  ]
  node [
    id 882
    label "Ko&#322;omyja"
  ]
  node [
    id 883
    label "Buczacz"
  ]
  node [
    id 884
    label "M&#252;nster"
  ]
  node [
    id 885
    label "Brema"
  ]
  node [
    id 886
    label "Delhi"
  ]
  node [
    id 887
    label "Nicea"
  ]
  node [
    id 888
    label "&#346;niatyn"
  ]
  node [
    id 889
    label "Szawle"
  ]
  node [
    id 890
    label "Czerniowce"
  ]
  node [
    id 891
    label "Mi&#347;nia"
  ]
  node [
    id 892
    label "Sydney"
  ]
  node [
    id 893
    label "Moguncja"
  ]
  node [
    id 894
    label "Narbona"
  ]
  node [
    id 895
    label "Swiet&#322;ogorsk"
  ]
  node [
    id 896
    label "Wittenberga"
  ]
  node [
    id 897
    label "Uljanowsk"
  ]
  node [
    id 898
    label "Wyborg"
  ]
  node [
    id 899
    label "&#321;uga&#324;sk"
  ]
  node [
    id 900
    label "Trojan"
  ]
  node [
    id 901
    label "Kalwaria_Zebrzydowska"
  ]
  node [
    id 902
    label "Brandenburg"
  ]
  node [
    id 903
    label "Kemerowo"
  ]
  node [
    id 904
    label "Kaszgar"
  ]
  node [
    id 905
    label "Lenzen"
  ]
  node [
    id 906
    label "Nanning"
  ]
  node [
    id 907
    label "Gotha"
  ]
  node [
    id 908
    label "Zurych"
  ]
  node [
    id 909
    label "Baltimore"
  ]
  node [
    id 910
    label "&#321;uck"
  ]
  node [
    id 911
    label "Bristol"
  ]
  node [
    id 912
    label "Ferrara"
  ]
  node [
    id 913
    label "Mariupol"
  ]
  node [
    id 914
    label "przestrze&#324;_publiczna"
  ]
  node [
    id 915
    label "Filadelfia"
  ]
  node [
    id 916
    label "Czerniejewo"
  ]
  node [
    id 917
    label "Milan&#243;wek"
  ]
  node [
    id 918
    label "Lhasa"
  ]
  node [
    id 919
    label "Kanton"
  ]
  node [
    id 920
    label "Perwomajsk"
  ]
  node [
    id 921
    label "Nieftiegorsk"
  ]
  node [
    id 922
    label "Greifswald"
  ]
  node [
    id 923
    label "Pittsburgh"
  ]
  node [
    id 924
    label "Akwileja"
  ]
  node [
    id 925
    label "Norfolk"
  ]
  node [
    id 926
    label "Perm"
  ]
  node [
    id 927
    label "Fergana"
  ]
  node [
    id 928
    label "Detroit"
  ]
  node [
    id 929
    label "Starobielsk"
  ]
  node [
    id 930
    label "Wielsk"
  ]
  node [
    id 931
    label "Zaklik&#243;w"
  ]
  node [
    id 932
    label "Majsur"
  ]
  node [
    id 933
    label "Narwa"
  ]
  node [
    id 934
    label "Chicago"
  ]
  node [
    id 935
    label "Byczyna"
  ]
  node [
    id 936
    label "Mozyrz"
  ]
  node [
    id 937
    label "Konstantyn&#243;wka"
  ]
  node [
    id 938
    label "Miko&#322;aj&#243;w"
  ]
  node [
    id 939
    label "Megara"
  ]
  node [
    id 940
    label "Stralsund"
  ]
  node [
    id 941
    label "Wo&#322;gograd"
  ]
  node [
    id 942
    label "Lichinga"
  ]
  node [
    id 943
    label "Haga"
  ]
  node [
    id 944
    label "Tarnopol"
  ]
  node [
    id 945
    label "Nowomoskowsk"
  ]
  node [
    id 946
    label "K&#322;ajpeda"
  ]
  node [
    id 947
    label "Ussuryjsk"
  ]
  node [
    id 948
    label "Brugia"
  ]
  node [
    id 949
    label "Natal"
  ]
  node [
    id 950
    label "Kro&#347;niewice"
  ]
  node [
    id 951
    label "Edynburg"
  ]
  node [
    id 952
    label "Marburg"
  ]
  node [
    id 953
    label "Dalton"
  ]
  node [
    id 954
    label "S&#322;onim"
  ]
  node [
    id 955
    label "&#346;wiebodzice"
  ]
  node [
    id 956
    label "Smorgonie"
  ]
  node [
    id 957
    label "Orze&#322;"
  ]
  node [
    id 958
    label "Nowoku&#378;nieck"
  ]
  node [
    id 959
    label "Zadar"
  ]
  node [
    id 960
    label "Koprzywnica"
  ]
  node [
    id 961
    label "Angarsk"
  ]
  node [
    id 962
    label "Tel_Awiw-Jafa"
  ]
  node [
    id 963
    label "Mo&#380;ajsk"
  ]
  node [
    id 964
    label "Norylsk"
  ]
  node [
    id 965
    label "Akwizgran"
  ]
  node [
    id 966
    label "Jawor&#243;w"
  ]
  node [
    id 967
    label "weduta"
  ]
  node [
    id 968
    label "Jab&#322;onk&#243;w"
  ]
  node [
    id 969
    label "Suzdal"
  ]
  node [
    id 970
    label "W&#322;odzimierz"
  ]
  node [
    id 971
    label "Bujnaksk"
  ]
  node [
    id 972
    label "Beresteczko"
  ]
  node [
    id 973
    label "Strzelno"
  ]
  node [
    id 974
    label "Siewsk"
  ]
  node [
    id 975
    label "Cymlansk"
  ]
  node [
    id 976
    label "Trzyniec"
  ]
  node [
    id 977
    label "Wuppertal"
  ]
  node [
    id 978
    label "Sura&#380;"
  ]
  node [
    id 979
    label "Samara"
  ]
  node [
    id 980
    label "Winchester"
  ]
  node [
    id 981
    label "Krasnodar"
  ]
  node [
    id 982
    label "Sydon"
  ]
  node [
    id 983
    label "Worone&#380;"
  ]
  node [
    id 984
    label "Paw&#322;odar"
  ]
  node [
    id 985
    label "Czelabi&#324;sk"
  ]
  node [
    id 986
    label "Reda"
  ]
  node [
    id 987
    label "Karwina"
  ]
  node [
    id 988
    label "Wyszehrad"
  ]
  node [
    id 989
    label "Sara&#324;sk"
  ]
  node [
    id 990
    label "Koby&#322;ka"
  ]
  node [
    id 991
    label "Tambow"
  ]
  node [
    id 992
    label "Pyskowice"
  ]
  node [
    id 993
    label "Winnica"
  ]
  node [
    id 994
    label "Heidelberg"
  ]
  node [
    id 995
    label "Maribor"
  ]
  node [
    id 996
    label "Werona"
  ]
  node [
    id 997
    label "G&#322;uszyca"
  ]
  node [
    id 998
    label "Rostock"
  ]
  node [
    id 999
    label "Mekka"
  ]
  node [
    id 1000
    label "Liberec"
  ]
  node [
    id 1001
    label "Bie&#322;gorod"
  ]
  node [
    id 1002
    label "Berdycz&#243;w"
  ]
  node [
    id 1003
    label "Sierdobsk"
  ]
  node [
    id 1004
    label "Bobrujsk"
  ]
  node [
    id 1005
    label "Padwa"
  ]
  node [
    id 1006
    label "Chanty-Mansyjsk"
  ]
  node [
    id 1007
    label "Pasawa"
  ]
  node [
    id 1008
    label "Poczaj&#243;w"
  ]
  node [
    id 1009
    label "&#379;ar&#243;w"
  ]
  node [
    id 1010
    label "Barabi&#324;sk"
  ]
  node [
    id 1011
    label "Gorycja"
  ]
  node [
    id 1012
    label "Haarlem"
  ]
  node [
    id 1013
    label "Kiejdany"
  ]
  node [
    id 1014
    label "Chmielnicki"
  ]
  node [
    id 1015
    label "Siena"
  ]
  node [
    id 1016
    label "Burgas"
  ]
  node [
    id 1017
    label "Magnitogorsk"
  ]
  node [
    id 1018
    label "Korzec"
  ]
  node [
    id 1019
    label "Bonn"
  ]
  node [
    id 1020
    label "Pr&#243;szk&#243;w"
  ]
  node [
    id 1021
    label "Walencja"
  ]
  node [
    id 1022
    label "Mosina"
  ]
  node [
    id 1023
    label "okre&#347;lony"
  ]
  node [
    id 1024
    label "jaki&#347;"
  ]
  node [
    id 1025
    label "sensowny"
  ]
  node [
    id 1026
    label "rozs&#261;dny"
  ]
  node [
    id 1027
    label "byd&#322;o"
  ]
  node [
    id 1028
    label "zobo"
  ]
  node [
    id 1029
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 1030
    label "yakalo"
  ]
  node [
    id 1031
    label "dzo"
  ]
  node [
    id 1032
    label "bargain"
  ]
  node [
    id 1033
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 1034
    label "tycze&#263;"
  ]
  node [
    id 1035
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 1036
    label "procedura"
  ]
  node [
    id 1037
    label "process"
  ]
  node [
    id 1038
    label "cycle"
  ]
  node [
    id 1039
    label "proces"
  ]
  node [
    id 1040
    label "&#380;ycie"
  ]
  node [
    id 1041
    label "z&#322;ote_czasy"
  ]
  node [
    id 1042
    label "proces_biologiczny"
  ]
  node [
    id 1043
    label "syrniki"
  ]
  node [
    id 1044
    label "envision"
  ]
  node [
    id 1045
    label "zaplanowa&#263;"
  ]
  node [
    id 1046
    label "spodzia&#263;_si&#281;"
  ]
  node [
    id 1047
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 1048
    label "lacki"
  ]
  node [
    id 1049
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 1050
    label "przedmiot"
  ]
  node [
    id 1051
    label "sztajer"
  ]
  node [
    id 1052
    label "drabant"
  ]
  node [
    id 1053
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 1054
    label "polak"
  ]
  node [
    id 1055
    label "pierogi_ruskie"
  ]
  node [
    id 1056
    label "krakowiak"
  ]
  node [
    id 1057
    label "Polish"
  ]
  node [
    id 1058
    label "j&#281;zyk"
  ]
  node [
    id 1059
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 1060
    label "oberek"
  ]
  node [
    id 1061
    label "po_polsku"
  ]
  node [
    id 1062
    label "mazur"
  ]
  node [
    id 1063
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 1064
    label "chodzony"
  ]
  node [
    id 1065
    label "skoczny"
  ]
  node [
    id 1066
    label "ryba_po_grecku"
  ]
  node [
    id 1067
    label "goniony"
  ]
  node [
    id 1068
    label "polsko"
  ]
  node [
    id 1069
    label "cz&#322;owiek"
  ]
  node [
    id 1070
    label "ho&#322;ysz"
  ]
  node [
    id 1071
    label "s&#322;aby"
  ]
  node [
    id 1072
    label "ubo&#380;enie"
  ]
  node [
    id 1073
    label "nieszcz&#281;&#347;liwy"
  ]
  node [
    id 1074
    label "zubo&#380;anie"
  ]
  node [
    id 1075
    label "biedota"
  ]
  node [
    id 1076
    label "zubo&#380;enie"
  ]
  node [
    id 1077
    label "n&#281;dzny"
  ]
  node [
    id 1078
    label "bankrutowanie"
  ]
  node [
    id 1079
    label "proletariusz"
  ]
  node [
    id 1080
    label "go&#322;odupiec"
  ]
  node [
    id 1081
    label "biednie"
  ]
  node [
    id 1082
    label "zbiednienie"
  ]
  node [
    id 1083
    label "sytuowany"
  ]
  node [
    id 1084
    label "raw_material"
  ]
  node [
    id 1085
    label "jednostka_administracyjna"
  ]
  node [
    id 1086
    label "makroregion"
  ]
  node [
    id 1087
    label "powiat"
  ]
  node [
    id 1088
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 1089
    label "mikroregion"
  ]
  node [
    id 1090
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 1091
    label "si&#322;a"
  ]
  node [
    id 1092
    label "stan"
  ]
  node [
    id 1093
    label "lina"
  ]
  node [
    id 1094
    label "way"
  ]
  node [
    id 1095
    label "cable"
  ]
  node [
    id 1096
    label "przebieg"
  ]
  node [
    id 1097
    label "zbi&#243;r"
  ]
  node [
    id 1098
    label "ch&#243;d"
  ]
  node [
    id 1099
    label "trasa"
  ]
  node [
    id 1100
    label "rz&#261;d"
  ]
  node [
    id 1101
    label "k&#322;us"
  ]
  node [
    id 1102
    label "progression"
  ]
  node [
    id 1103
    label "current"
  ]
  node [
    id 1104
    label "pr&#261;d"
  ]
  node [
    id 1105
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 1106
    label "lot"
  ]
  node [
    id 1107
    label "istota_&#380;ywa"
  ]
  node [
    id 1108
    label "najgorszy"
  ]
  node [
    id 1109
    label "aktualny"
  ]
  node [
    id 1110
    label "ostatnio"
  ]
  node [
    id 1111
    label "niedawno"
  ]
  node [
    id 1112
    label "wi&#261;&#380;&#261;cy"
  ]
  node [
    id 1113
    label "sko&#324;czony"
  ]
  node [
    id 1114
    label "poprzedni"
  ]
  node [
    id 1115
    label "pozosta&#322;y"
  ]
  node [
    id 1116
    label "w&#261;tpliwy"
  ]
  node [
    id 1117
    label "&#347;ledziowate"
  ]
  node [
    id 1118
    label "ryba"
  ]
  node [
    id 1119
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 1120
    label "miech"
  ]
  node [
    id 1121
    label "kalendy"
  ]
  node [
    id 1122
    label "tydzie&#324;"
  ]
  node [
    id 1123
    label "retreat"
  ]
  node [
    id 1124
    label "recall"
  ]
  node [
    id 1125
    label "uniewa&#380;ni&#263;"
  ]
  node [
    id 1126
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 1127
    label "zmieni&#263;"
  ]
  node [
    id 1128
    label "twierdzenie"
  ]
  node [
    id 1129
    label "my&#347;l"
  ]
  node [
    id 1130
    label "wnioskowanie"
  ]
  node [
    id 1131
    label "propozycja"
  ]
  node [
    id 1132
    label "motion"
  ]
  node [
    id 1133
    label "pismo"
  ]
  node [
    id 1134
    label "prayer"
  ]
  node [
    id 1135
    label "amunicja"
  ]
  node [
    id 1136
    label "rozbroi&#263;"
  ]
  node [
    id 1137
    label "tackle"
  ]
  node [
    id 1138
    label "przyrz&#261;d"
  ]
  node [
    id 1139
    label "osprz&#281;t"
  ]
  node [
    id 1140
    label "instalacja"
  ]
  node [
    id 1141
    label "munition"
  ]
  node [
    id 1142
    label "wyposa&#380;enie"
  ]
  node [
    id 1143
    label "twornik"
  ]
  node [
    id 1144
    label "or&#281;&#380;"
  ]
  node [
    id 1145
    label "rozbraja&#263;"
  ]
  node [
    id 1146
    label "rozbrajanie"
  ]
  node [
    id 1147
    label "rozbrojenie"
  ]
  node [
    id 1148
    label "zakres"
  ]
  node [
    id 1149
    label "kontekst"
  ]
  node [
    id 1150
    label "wymiar"
  ]
  node [
    id 1151
    label "obszar"
  ]
  node [
    id 1152
    label "krajobraz"
  ]
  node [
    id 1153
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 1154
    label "w&#322;adza"
  ]
  node [
    id 1155
    label "nation"
  ]
  node [
    id 1156
    label "przyroda"
  ]
  node [
    id 1157
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 1158
    label "miejsce_pracy"
  ]
  node [
    id 1159
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 1160
    label "inwestycyjnie"
  ]
  node [
    id 1161
    label "remark"
  ]
  node [
    id 1162
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 1163
    label "okre&#347;la&#263;"
  ]
  node [
    id 1164
    label "say"
  ]
  node [
    id 1165
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 1166
    label "formu&#322;owa&#263;"
  ]
  node [
    id 1167
    label "talk"
  ]
  node [
    id 1168
    label "powiada&#263;"
  ]
  node [
    id 1169
    label "informowa&#263;"
  ]
  node [
    id 1170
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 1171
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 1172
    label "wydobywa&#263;"
  ]
  node [
    id 1173
    label "express"
  ]
  node [
    id 1174
    label "chew_the_fat"
  ]
  node [
    id 1175
    label "dysfonia"
  ]
  node [
    id 1176
    label "umie&#263;"
  ]
  node [
    id 1177
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 1178
    label "tell"
  ]
  node [
    id 1179
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 1180
    label "wyra&#380;a&#263;"
  ]
  node [
    id 1181
    label "gaworzy&#263;"
  ]
  node [
    id 1182
    label "rozmawia&#263;"
  ]
  node [
    id 1183
    label "prawi&#263;"
  ]
  node [
    id 1184
    label "silny"
  ]
  node [
    id 1185
    label "wa&#380;nie"
  ]
  node [
    id 1186
    label "eksponowany"
  ]
  node [
    id 1187
    label "istotnie"
  ]
  node [
    id 1188
    label "znaczny"
  ]
  node [
    id 1189
    label "dobry"
  ]
  node [
    id 1190
    label "wynios&#322;y"
  ]
  node [
    id 1191
    label "dono&#347;ny"
  ]
  node [
    id 1192
    label "teren_przemys&#322;owy"
  ]
  node [
    id 1193
    label "miejsce"
  ]
  node [
    id 1194
    label "ballpark"
  ]
  node [
    id 1195
    label "sprz&#281;t"
  ]
  node [
    id 1196
    label "tabor"
  ]
  node [
    id 1197
    label "&#321;azienki_Kr&#243;lewskie"
  ]
  node [
    id 1198
    label "teren_zielony"
  ]
  node [
    id 1199
    label "dostosowa&#263;"
  ]
  node [
    id 1200
    label "motivate"
  ]
  node [
    id 1201
    label "przenie&#347;&#263;"
  ]
  node [
    id 1202
    label "shift"
  ]
  node [
    id 1203
    label "deepen"
  ]
  node [
    id 1204
    label "transfer"
  ]
  node [
    id 1205
    label "ruszy&#263;"
  ]
  node [
    id 1206
    label "czasokres"
  ]
  node [
    id 1207
    label "trawienie"
  ]
  node [
    id 1208
    label "period"
  ]
  node [
    id 1209
    label "odczyt"
  ]
  node [
    id 1210
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 1211
    label "chwila"
  ]
  node [
    id 1212
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 1213
    label "poprzedzenie"
  ]
  node [
    id 1214
    label "dzieje"
  ]
  node [
    id 1215
    label "poprzedzi&#263;"
  ]
  node [
    id 1216
    label "przep&#322;ywanie"
  ]
  node [
    id 1217
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 1218
    label "odwlekanie_si&#281;"
  ]
  node [
    id 1219
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 1220
    label "Zeitgeist"
  ]
  node [
    id 1221
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 1222
    label "okres_czasu"
  ]
  node [
    id 1223
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 1224
    label "pochodzi&#263;"
  ]
  node [
    id 1225
    label "schy&#322;ek"
  ]
  node [
    id 1226
    label "czwarty_wymiar"
  ]
  node [
    id 1227
    label "chronometria"
  ]
  node [
    id 1228
    label "poprzedzanie"
  ]
  node [
    id 1229
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 1230
    label "pogoda"
  ]
  node [
    id 1231
    label "zegar"
  ]
  node [
    id 1232
    label "trawi&#263;"
  ]
  node [
    id 1233
    label "pochodzenie"
  ]
  node [
    id 1234
    label "poprzedza&#263;"
  ]
  node [
    id 1235
    label "time_period"
  ]
  node [
    id 1236
    label "rachuba_czasu"
  ]
  node [
    id 1237
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 1238
    label "czasoprzestrze&#324;"
  ]
  node [
    id 1239
    label "laba"
  ]
  node [
    id 1240
    label "dzia&#322;anie"
  ]
  node [
    id 1241
    label "zrobienie"
  ]
  node [
    id 1242
    label "start"
  ]
  node [
    id 1243
    label "znalezienie_si&#281;"
  ]
  node [
    id 1244
    label "zrobienie_pierwszego_kroku"
  ]
  node [
    id 1245
    label "zacz&#281;cie"
  ]
  node [
    id 1246
    label "pocz&#261;tek"
  ]
  node [
    id 1247
    label "opening"
  ]
  node [
    id 1248
    label "stulecie"
  ]
  node [
    id 1249
    label "kalendarz"
  ]
  node [
    id 1250
    label "pora_roku"
  ]
  node [
    id 1251
    label "cykl_astronomiczny"
  ]
  node [
    id 1252
    label "p&#243;&#322;rocze"
  ]
  node [
    id 1253
    label "kwarta&#322;"
  ]
  node [
    id 1254
    label "kurs"
  ]
  node [
    id 1255
    label "jubileusz"
  ]
  node [
    id 1256
    label "lata"
  ]
  node [
    id 1257
    label "martwy_sezon"
  ]
  node [
    id 1258
    label "u&#380;ytecznie"
  ]
  node [
    id 1259
    label "praktyczny"
  ]
  node [
    id 1260
    label "racjonalnie"
  ]
  node [
    id 1261
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 1262
    label "wydatki_bud&#380;etowe"
  ]
  node [
    id 1263
    label "etat"
  ]
  node [
    id 1264
    label "portfel"
  ]
  node [
    id 1265
    label "rezerwa_rewaluacyjna"
  ]
  node [
    id 1266
    label "abstrakcja"
  ]
  node [
    id 1267
    label "substancja"
  ]
  node [
    id 1268
    label "chemikalia"
  ]
  node [
    id 1269
    label "pacjent"
  ]
  node [
    id 1270
    label "schorzenie"
  ]
  node [
    id 1271
    label "przeznaczenie"
  ]
  node [
    id 1272
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 1273
    label "happening"
  ]
  node [
    id 1274
    label "przyk&#322;ad"
  ]
  node [
    id 1275
    label "free"
  ]
  node [
    id 1276
    label "establish"
  ]
  node [
    id 1277
    label "przypu&#347;ci&#263;"
  ]
  node [
    id 1278
    label "przyzna&#263;"
  ]
  node [
    id 1279
    label "wyobrazi&#263;_sobie"
  ]
  node [
    id 1280
    label "za&#322;o&#380;y&#263;_si&#281;"
  ]
  node [
    id 1281
    label "zdecydowa&#263;_si&#281;"
  ]
  node [
    id 1282
    label "post"
  ]
  node [
    id 1283
    label "set"
  ]
  node [
    id 1284
    label "spr&#243;bowa&#263;"
  ]
  node [
    id 1285
    label "oceni&#263;"
  ]
  node [
    id 1286
    label "stawi&#263;"
  ]
  node [
    id 1287
    label "umie&#347;ci&#263;"
  ]
  node [
    id 1288
    label "obra&#263;"
  ]
  node [
    id 1289
    label "wydoby&#263;"
  ]
  node [
    id 1290
    label "stanowisko"
  ]
  node [
    id 1291
    label "budowla"
  ]
  node [
    id 1292
    label "obstawi&#263;"
  ]
  node [
    id 1293
    label "pozostawi&#263;"
  ]
  node [
    id 1294
    label "wyda&#263;"
  ]
  node [
    id 1295
    label "uczyni&#263;"
  ]
  node [
    id 1296
    label "plant"
  ]
  node [
    id 1297
    label "uruchomi&#263;"
  ]
  node [
    id 1298
    label "zafundowa&#263;"
  ]
  node [
    id 1299
    label "spowodowa&#263;"
  ]
  node [
    id 1300
    label "u&#322;o&#380;y&#263;"
  ]
  node [
    id 1301
    label "przedstawi&#263;"
  ]
  node [
    id 1302
    label "wskaza&#263;"
  ]
  node [
    id 1303
    label "wytworzy&#263;"
  ]
  node [
    id 1304
    label "peddle"
  ]
  node [
    id 1305
    label "wyznaczy&#263;"
  ]
  node [
    id 1306
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 1307
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 1308
    label "implikowa&#263;"
  ]
  node [
    id 1309
    label "stawia&#263;"
  ]
  node [
    id 1310
    label "mark"
  ]
  node [
    id 1311
    label "kodzik"
  ]
  node [
    id 1312
    label "attribute"
  ]
  node [
    id 1313
    label "dow&#243;d"
  ]
  node [
    id 1314
    label "herb"
  ]
  node [
    id 1315
    label "fakt"
  ]
  node [
    id 1316
    label "oznakowanie"
  ]
  node [
    id 1317
    label "point"
  ]
  node [
    id 1318
    label "totalizator"
  ]
  node [
    id 1319
    label "sportowy"
  ]
  node [
    id 1320
    label "narodowy"
  ]
  node [
    id 1321
    label "fundusz"
  ]
  node [
    id 1322
    label "ochrona"
  ]
  node [
    id 1323
    label "&#347;rodowisko"
  ]
  node [
    id 1324
    label "i"
  ]
  node [
    id 1325
    label "gospodarka"
  ]
  node [
    id 1326
    label "wodny"
  ]
  node [
    id 1327
    label "warmi&#324;sko"
  ]
  node [
    id 1328
    label "mazurski"
  ]
  node [
    id 1329
    label "ministerstwo"
  ]
  node [
    id 1330
    label "Jerzy"
  ]
  node [
    id 1331
    label "Szmajdzi&#324;ski"
  ]
  node [
    id 1332
    label "Tadeusz"
  ]
  node [
    id 1333
    label "Iwi&#324;ski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 238
  ]
  edge [
    source 1
    target 239
  ]
  edge [
    source 1
    target 240
  ]
  edge [
    source 1
    target 241
  ]
  edge [
    source 1
    target 242
  ]
  edge [
    source 1
    target 243
  ]
  edge [
    source 1
    target 244
  ]
  edge [
    source 1
    target 245
  ]
  edge [
    source 1
    target 246
  ]
  edge [
    source 1
    target 247
  ]
  edge [
    source 1
    target 248
  ]
  edge [
    source 1
    target 249
  ]
  edge [
    source 1
    target 250
  ]
  edge [
    source 1
    target 251
  ]
  edge [
    source 1
    target 252
  ]
  edge [
    source 1
    target 253
  ]
  edge [
    source 1
    target 254
  ]
  edge [
    source 1
    target 255
  ]
  edge [
    source 1
    target 256
  ]
  edge [
    source 1
    target 257
  ]
  edge [
    source 1
    target 258
  ]
  edge [
    source 1
    target 259
  ]
  edge [
    source 1
    target 260
  ]
  edge [
    source 1
    target 261
  ]
  edge [
    source 1
    target 262
  ]
  edge [
    source 1
    target 263
  ]
  edge [
    source 1
    target 264
  ]
  edge [
    source 1
    target 265
  ]
  edge [
    source 1
    target 266
  ]
  edge [
    source 1
    target 267
  ]
  edge [
    source 1
    target 268
  ]
  edge [
    source 1
    target 269
  ]
  edge [
    source 1
    target 270
  ]
  edge [
    source 1
    target 271
  ]
  edge [
    source 1
    target 272
  ]
  edge [
    source 1
    target 273
  ]
  edge [
    source 1
    target 274
  ]
  edge [
    source 1
    target 275
  ]
  edge [
    source 1
    target 276
  ]
  edge [
    source 1
    target 277
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 356
  ]
  edge [
    source 8
    target 357
  ]
  edge [
    source 8
    target 358
  ]
  edge [
    source 8
    target 359
  ]
  edge [
    source 8
    target 360
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 361
  ]
  edge [
    source 9
    target 362
  ]
  edge [
    source 9
    target 363
  ]
  edge [
    source 9
    target 364
  ]
  edge [
    source 9
    target 365
  ]
  edge [
    source 9
    target 366
  ]
  edge [
    source 9
    target 367
  ]
  edge [
    source 9
    target 368
  ]
  edge [
    source 9
    target 369
  ]
  edge [
    source 9
    target 370
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 371
  ]
  edge [
    source 10
    target 372
  ]
  edge [
    source 10
    target 373
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 374
  ]
  edge [
    source 11
    target 375
  ]
  edge [
    source 11
    target 376
  ]
  edge [
    source 11
    target 377
  ]
  edge [
    source 11
    target 378
  ]
  edge [
    source 11
    target 379
  ]
  edge [
    source 11
    target 380
  ]
  edge [
    source 11
    target 381
  ]
  edge [
    source 11
    target 382
  ]
  edge [
    source 11
    target 282
  ]
  edge [
    source 11
    target 383
  ]
  edge [
    source 11
    target 384
  ]
  edge [
    source 11
    target 283
  ]
  edge [
    source 11
    target 385
  ]
  edge [
    source 11
    target 386
  ]
  edge [
    source 11
    target 387
  ]
  edge [
    source 11
    target 388
  ]
  edge [
    source 11
    target 389
  ]
  edge [
    source 11
    target 390
  ]
  edge [
    source 11
    target 391
  ]
  edge [
    source 11
    target 392
  ]
  edge [
    source 11
    target 393
  ]
  edge [
    source 11
    target 394
  ]
  edge [
    source 11
    target 395
  ]
  edge [
    source 11
    target 291
  ]
  edge [
    source 11
    target 396
  ]
  edge [
    source 11
    target 397
  ]
  edge [
    source 11
    target 398
  ]
  edge [
    source 11
    target 399
  ]
  edge [
    source 11
    target 400
  ]
  edge [
    source 11
    target 401
  ]
  edge [
    source 11
    target 402
  ]
  edge [
    source 11
    target 403
  ]
  edge [
    source 11
    target 404
  ]
  edge [
    source 11
    target 405
  ]
  edge [
    source 11
    target 406
  ]
  edge [
    source 11
    target 407
  ]
  edge [
    source 11
    target 408
  ]
  edge [
    source 11
    target 409
  ]
  edge [
    source 11
    target 410
  ]
  edge [
    source 11
    target 411
  ]
  edge [
    source 11
    target 412
  ]
  edge [
    source 11
    target 413
  ]
  edge [
    source 11
    target 414
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 415
  ]
  edge [
    source 12
    target 416
  ]
  edge [
    source 12
    target 417
  ]
  edge [
    source 12
    target 418
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 419
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 420
  ]
  edge [
    source 14
    target 421
  ]
  edge [
    source 14
    target 422
  ]
  edge [
    source 14
    target 423
  ]
  edge [
    source 14
    target 424
  ]
  edge [
    source 14
    target 425
  ]
  edge [
    source 14
    target 426
  ]
  edge [
    source 14
    target 427
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 428
  ]
  edge [
    source 15
    target 429
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 430
  ]
  edge [
    source 16
    target 305
  ]
  edge [
    source 16
    target 431
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 432
  ]
  edge [
    source 17
    target 433
  ]
  edge [
    source 17
    target 434
  ]
  edge [
    source 17
    target 435
  ]
  edge [
    source 17
    target 436
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 27
  ]
  edge [
    source 18
    target 44
  ]
  edge [
    source 18
    target 437
  ]
  edge [
    source 18
    target 438
  ]
  edge [
    source 18
    target 439
  ]
  edge [
    source 18
    target 440
  ]
  edge [
    source 18
    target 441
  ]
  edge [
    source 18
    target 442
  ]
  edge [
    source 18
    target 443
  ]
  edge [
    source 18
    target 444
  ]
  edge [
    source 18
    target 445
  ]
  edge [
    source 18
    target 446
  ]
  edge [
    source 18
    target 447
  ]
  edge [
    source 18
    target 448
  ]
  edge [
    source 18
    target 449
  ]
  edge [
    source 18
    target 450
  ]
  edge [
    source 18
    target 451
  ]
  edge [
    source 18
    target 452
  ]
  edge [
    source 18
    target 453
  ]
  edge [
    source 18
    target 454
  ]
  edge [
    source 18
    target 455
  ]
  edge [
    source 18
    target 456
  ]
  edge [
    source 18
    target 457
  ]
  edge [
    source 18
    target 458
  ]
  edge [
    source 18
    target 459
  ]
  edge [
    source 18
    target 460
  ]
  edge [
    source 18
    target 461
  ]
  edge [
    source 18
    target 462
  ]
  edge [
    source 18
    target 463
  ]
  edge [
    source 18
    target 464
  ]
  edge [
    source 18
    target 465
  ]
  edge [
    source 18
    target 466
  ]
  edge [
    source 18
    target 467
  ]
  edge [
    source 18
    target 468
  ]
  edge [
    source 18
    target 469
  ]
  edge [
    source 18
    target 470
  ]
  edge [
    source 18
    target 471
  ]
  edge [
    source 18
    target 472
  ]
  edge [
    source 18
    target 473
  ]
  edge [
    source 18
    target 474
  ]
  edge [
    source 18
    target 475
  ]
  edge [
    source 18
    target 476
  ]
  edge [
    source 18
    target 477
  ]
  edge [
    source 18
    target 478
  ]
  edge [
    source 18
    target 479
  ]
  edge [
    source 18
    target 480
  ]
  edge [
    source 18
    target 481
  ]
  edge [
    source 18
    target 482
  ]
  edge [
    source 18
    target 483
  ]
  edge [
    source 18
    target 484
  ]
  edge [
    source 18
    target 485
  ]
  edge [
    source 18
    target 486
  ]
  edge [
    source 18
    target 487
  ]
  edge [
    source 18
    target 488
  ]
  edge [
    source 18
    target 489
  ]
  edge [
    source 18
    target 490
  ]
  edge [
    source 18
    target 491
  ]
  edge [
    source 18
    target 492
  ]
  edge [
    source 18
    target 493
  ]
  edge [
    source 18
    target 494
  ]
  edge [
    source 18
    target 495
  ]
  edge [
    source 18
    target 496
  ]
  edge [
    source 18
    target 497
  ]
  edge [
    source 18
    target 498
  ]
  edge [
    source 18
    target 499
  ]
  edge [
    source 18
    target 500
  ]
  edge [
    source 18
    target 501
  ]
  edge [
    source 18
    target 502
  ]
  edge [
    source 18
    target 503
  ]
  edge [
    source 18
    target 504
  ]
  edge [
    source 18
    target 505
  ]
  edge [
    source 18
    target 506
  ]
  edge [
    source 18
    target 507
  ]
  edge [
    source 18
    target 508
  ]
  edge [
    source 18
    target 509
  ]
  edge [
    source 18
    target 510
  ]
  edge [
    source 18
    target 511
  ]
  edge [
    source 18
    target 512
  ]
  edge [
    source 18
    target 513
  ]
  edge [
    source 18
    target 514
  ]
  edge [
    source 18
    target 515
  ]
  edge [
    source 18
    target 516
  ]
  edge [
    source 18
    target 517
  ]
  edge [
    source 18
    target 518
  ]
  edge [
    source 18
    target 519
  ]
  edge [
    source 18
    target 520
  ]
  edge [
    source 18
    target 521
  ]
  edge [
    source 18
    target 522
  ]
  edge [
    source 18
    target 523
  ]
  edge [
    source 18
    target 524
  ]
  edge [
    source 18
    target 525
  ]
  edge [
    source 18
    target 526
  ]
  edge [
    source 18
    target 527
  ]
  edge [
    source 18
    target 528
  ]
  edge [
    source 18
    target 529
  ]
  edge [
    source 18
    target 530
  ]
  edge [
    source 18
    target 531
  ]
  edge [
    source 18
    target 532
  ]
  edge [
    source 18
    target 533
  ]
  edge [
    source 18
    target 534
  ]
  edge [
    source 18
    target 535
  ]
  edge [
    source 18
    target 536
  ]
  edge [
    source 18
    target 537
  ]
  edge [
    source 18
    target 538
  ]
  edge [
    source 18
    target 539
  ]
  edge [
    source 18
    target 540
  ]
  edge [
    source 18
    target 541
  ]
  edge [
    source 18
    target 542
  ]
  edge [
    source 18
    target 543
  ]
  edge [
    source 18
    target 544
  ]
  edge [
    source 18
    target 545
  ]
  edge [
    source 18
    target 546
  ]
  edge [
    source 18
    target 547
  ]
  edge [
    source 18
    target 548
  ]
  edge [
    source 18
    target 549
  ]
  edge [
    source 18
    target 550
  ]
  edge [
    source 18
    target 551
  ]
  edge [
    source 18
    target 552
  ]
  edge [
    source 18
    target 553
  ]
  edge [
    source 18
    target 554
  ]
  edge [
    source 18
    target 555
  ]
  edge [
    source 18
    target 556
  ]
  edge [
    source 18
    target 557
  ]
  edge [
    source 18
    target 558
  ]
  edge [
    source 18
    target 559
  ]
  edge [
    source 18
    target 560
  ]
  edge [
    source 18
    target 561
  ]
  edge [
    source 18
    target 562
  ]
  edge [
    source 18
    target 563
  ]
  edge [
    source 18
    target 564
  ]
  edge [
    source 18
    target 565
  ]
  edge [
    source 18
    target 566
  ]
  edge [
    source 18
    target 567
  ]
  edge [
    source 18
    target 568
  ]
  edge [
    source 18
    target 569
  ]
  edge [
    source 18
    target 570
  ]
  edge [
    source 18
    target 571
  ]
  edge [
    source 18
    target 572
  ]
  edge [
    source 18
    target 573
  ]
  edge [
    source 18
    target 574
  ]
  edge [
    source 18
    target 66
  ]
  edge [
    source 18
    target 575
  ]
  edge [
    source 18
    target 576
  ]
  edge [
    source 18
    target 577
  ]
  edge [
    source 18
    target 578
  ]
  edge [
    source 18
    target 579
  ]
  edge [
    source 18
    target 580
  ]
  edge [
    source 18
    target 581
  ]
  edge [
    source 18
    target 582
  ]
  edge [
    source 18
    target 583
  ]
  edge [
    source 18
    target 584
  ]
  edge [
    source 18
    target 585
  ]
  edge [
    source 18
    target 586
  ]
  edge [
    source 18
    target 587
  ]
  edge [
    source 18
    target 79
  ]
  edge [
    source 18
    target 588
  ]
  edge [
    source 18
    target 589
  ]
  edge [
    source 18
    target 590
  ]
  edge [
    source 18
    target 591
  ]
  edge [
    source 18
    target 592
  ]
  edge [
    source 18
    target 593
  ]
  edge [
    source 18
    target 85
  ]
  edge [
    source 18
    target 594
  ]
  edge [
    source 18
    target 595
  ]
  edge [
    source 18
    target 596
  ]
  edge [
    source 18
    target 597
  ]
  edge [
    source 18
    target 598
  ]
  edge [
    source 18
    target 599
  ]
  edge [
    source 18
    target 600
  ]
  edge [
    source 18
    target 601
  ]
  edge [
    source 18
    target 602
  ]
  edge [
    source 18
    target 603
  ]
  edge [
    source 18
    target 604
  ]
  edge [
    source 18
    target 605
  ]
  edge [
    source 18
    target 606
  ]
  edge [
    source 18
    target 607
  ]
  edge [
    source 18
    target 608
  ]
  edge [
    source 18
    target 609
  ]
  edge [
    source 18
    target 610
  ]
  edge [
    source 18
    target 611
  ]
  edge [
    source 18
    target 612
  ]
  edge [
    source 18
    target 613
  ]
  edge [
    source 18
    target 614
  ]
  edge [
    source 18
    target 615
  ]
  edge [
    source 18
    target 616
  ]
  edge [
    source 18
    target 617
  ]
  edge [
    source 18
    target 618
  ]
  edge [
    source 18
    target 619
  ]
  edge [
    source 18
    target 620
  ]
  edge [
    source 18
    target 621
  ]
  edge [
    source 18
    target 622
  ]
  edge [
    source 18
    target 623
  ]
  edge [
    source 18
    target 624
  ]
  edge [
    source 18
    target 625
  ]
  edge [
    source 18
    target 626
  ]
  edge [
    source 18
    target 627
  ]
  edge [
    source 18
    target 628
  ]
  edge [
    source 18
    target 629
  ]
  edge [
    source 18
    target 630
  ]
  edge [
    source 18
    target 631
  ]
  edge [
    source 18
    target 632
  ]
  edge [
    source 18
    target 633
  ]
  edge [
    source 18
    target 634
  ]
  edge [
    source 18
    target 635
  ]
  edge [
    source 18
    target 636
  ]
  edge [
    source 18
    target 637
  ]
  edge [
    source 18
    target 638
  ]
  edge [
    source 18
    target 639
  ]
  edge [
    source 18
    target 640
  ]
  edge [
    source 18
    target 641
  ]
  edge [
    source 18
    target 642
  ]
  edge [
    source 18
    target 643
  ]
  edge [
    source 18
    target 644
  ]
  edge [
    source 18
    target 645
  ]
  edge [
    source 18
    target 646
  ]
  edge [
    source 18
    target 647
  ]
  edge [
    source 18
    target 648
  ]
  edge [
    source 18
    target 649
  ]
  edge [
    source 18
    target 650
  ]
  edge [
    source 18
    target 651
  ]
  edge [
    source 18
    target 652
  ]
  edge [
    source 18
    target 653
  ]
  edge [
    source 18
    target 654
  ]
  edge [
    source 18
    target 655
  ]
  edge [
    source 18
    target 656
  ]
  edge [
    source 18
    target 657
  ]
  edge [
    source 18
    target 658
  ]
  edge [
    source 18
    target 659
  ]
  edge [
    source 18
    target 660
  ]
  edge [
    source 18
    target 661
  ]
  edge [
    source 18
    target 662
  ]
  edge [
    source 18
    target 663
  ]
  edge [
    source 18
    target 664
  ]
  edge [
    source 18
    target 665
  ]
  edge [
    source 18
    target 666
  ]
  edge [
    source 18
    target 667
  ]
  edge [
    source 18
    target 668
  ]
  edge [
    source 18
    target 669
  ]
  edge [
    source 18
    target 670
  ]
  edge [
    source 18
    target 671
  ]
  edge [
    source 18
    target 672
  ]
  edge [
    source 18
    target 673
  ]
  edge [
    source 18
    target 674
  ]
  edge [
    source 18
    target 675
  ]
  edge [
    source 18
    target 676
  ]
  edge [
    source 18
    target 677
  ]
  edge [
    source 18
    target 678
  ]
  edge [
    source 18
    target 679
  ]
  edge [
    source 18
    target 680
  ]
  edge [
    source 18
    target 681
  ]
  edge [
    source 18
    target 682
  ]
  edge [
    source 18
    target 683
  ]
  edge [
    source 18
    target 684
  ]
  edge [
    source 18
    target 685
  ]
  edge [
    source 18
    target 686
  ]
  edge [
    source 18
    target 687
  ]
  edge [
    source 18
    target 688
  ]
  edge [
    source 18
    target 689
  ]
  edge [
    source 18
    target 690
  ]
  edge [
    source 18
    target 691
  ]
  edge [
    source 18
    target 692
  ]
  edge [
    source 18
    target 693
  ]
  edge [
    source 18
    target 694
  ]
  edge [
    source 18
    target 695
  ]
  edge [
    source 18
    target 696
  ]
  edge [
    source 18
    target 697
  ]
  edge [
    source 18
    target 698
  ]
  edge [
    source 18
    target 699
  ]
  edge [
    source 18
    target 700
  ]
  edge [
    source 18
    target 701
  ]
  edge [
    source 18
    target 702
  ]
  edge [
    source 18
    target 703
  ]
  edge [
    source 18
    target 704
  ]
  edge [
    source 18
    target 705
  ]
  edge [
    source 18
    target 706
  ]
  edge [
    source 18
    target 707
  ]
  edge [
    source 18
    target 708
  ]
  edge [
    source 18
    target 709
  ]
  edge [
    source 18
    target 710
  ]
  edge [
    source 18
    target 711
  ]
  edge [
    source 18
    target 712
  ]
  edge [
    source 18
    target 713
  ]
  edge [
    source 18
    target 714
  ]
  edge [
    source 18
    target 715
  ]
  edge [
    source 18
    target 716
  ]
  edge [
    source 18
    target 717
  ]
  edge [
    source 18
    target 718
  ]
  edge [
    source 18
    target 719
  ]
  edge [
    source 18
    target 720
  ]
  edge [
    source 18
    target 721
  ]
  edge [
    source 18
    target 722
  ]
  edge [
    source 18
    target 723
  ]
  edge [
    source 18
    target 724
  ]
  edge [
    source 18
    target 725
  ]
  edge [
    source 18
    target 726
  ]
  edge [
    source 18
    target 727
  ]
  edge [
    source 18
    target 728
  ]
  edge [
    source 18
    target 729
  ]
  edge [
    source 18
    target 730
  ]
  edge [
    source 18
    target 731
  ]
  edge [
    source 18
    target 732
  ]
  edge [
    source 18
    target 733
  ]
  edge [
    source 18
    target 734
  ]
  edge [
    source 18
    target 735
  ]
  edge [
    source 18
    target 736
  ]
  edge [
    source 18
    target 737
  ]
  edge [
    source 18
    target 738
  ]
  edge [
    source 18
    target 739
  ]
  edge [
    source 18
    target 740
  ]
  edge [
    source 18
    target 741
  ]
  edge [
    source 18
    target 742
  ]
  edge [
    source 18
    target 743
  ]
  edge [
    source 18
    target 744
  ]
  edge [
    source 18
    target 745
  ]
  edge [
    source 18
    target 746
  ]
  edge [
    source 18
    target 747
  ]
  edge [
    source 18
    target 748
  ]
  edge [
    source 18
    target 749
  ]
  edge [
    source 18
    target 750
  ]
  edge [
    source 18
    target 751
  ]
  edge [
    source 18
    target 752
  ]
  edge [
    source 18
    target 753
  ]
  edge [
    source 18
    target 754
  ]
  edge [
    source 18
    target 755
  ]
  edge [
    source 18
    target 756
  ]
  edge [
    source 18
    target 757
  ]
  edge [
    source 18
    target 758
  ]
  edge [
    source 18
    target 759
  ]
  edge [
    source 18
    target 760
  ]
  edge [
    source 18
    target 761
  ]
  edge [
    source 18
    target 762
  ]
  edge [
    source 18
    target 763
  ]
  edge [
    source 18
    target 764
  ]
  edge [
    source 18
    target 765
  ]
  edge [
    source 18
    target 766
  ]
  edge [
    source 18
    target 767
  ]
  edge [
    source 18
    target 768
  ]
  edge [
    source 18
    target 769
  ]
  edge [
    source 18
    target 770
  ]
  edge [
    source 18
    target 771
  ]
  edge [
    source 18
    target 772
  ]
  edge [
    source 18
    target 773
  ]
  edge [
    source 18
    target 774
  ]
  edge [
    source 18
    target 775
  ]
  edge [
    source 18
    target 776
  ]
  edge [
    source 18
    target 777
  ]
  edge [
    source 18
    target 778
  ]
  edge [
    source 18
    target 779
  ]
  edge [
    source 18
    target 780
  ]
  edge [
    source 18
    target 781
  ]
  edge [
    source 18
    target 782
  ]
  edge [
    source 18
    target 108
  ]
  edge [
    source 18
    target 783
  ]
  edge [
    source 18
    target 784
  ]
  edge [
    source 18
    target 785
  ]
  edge [
    source 18
    target 786
  ]
  edge [
    source 18
    target 787
  ]
  edge [
    source 18
    target 788
  ]
  edge [
    source 18
    target 789
  ]
  edge [
    source 18
    target 790
  ]
  edge [
    source 18
    target 791
  ]
  edge [
    source 18
    target 792
  ]
  edge [
    source 18
    target 793
  ]
  edge [
    source 18
    target 794
  ]
  edge [
    source 18
    target 795
  ]
  edge [
    source 18
    target 796
  ]
  edge [
    source 18
    target 797
  ]
  edge [
    source 18
    target 798
  ]
  edge [
    source 18
    target 799
  ]
  edge [
    source 18
    target 800
  ]
  edge [
    source 18
    target 801
  ]
  edge [
    source 18
    target 802
  ]
  edge [
    source 18
    target 803
  ]
  edge [
    source 18
    target 804
  ]
  edge [
    source 18
    target 805
  ]
  edge [
    source 18
    target 806
  ]
  edge [
    source 18
    target 807
  ]
  edge [
    source 18
    target 808
  ]
  edge [
    source 18
    target 809
  ]
  edge [
    source 18
    target 810
  ]
  edge [
    source 18
    target 811
  ]
  edge [
    source 18
    target 812
  ]
  edge [
    source 18
    target 813
  ]
  edge [
    source 18
    target 814
  ]
  edge [
    source 18
    target 815
  ]
  edge [
    source 18
    target 816
  ]
  edge [
    source 18
    target 817
  ]
  edge [
    source 18
    target 818
  ]
  edge [
    source 18
    target 819
  ]
  edge [
    source 18
    target 820
  ]
  edge [
    source 18
    target 821
  ]
  edge [
    source 18
    target 822
  ]
  edge [
    source 18
    target 823
  ]
  edge [
    source 18
    target 824
  ]
  edge [
    source 18
    target 825
  ]
  edge [
    source 18
    target 826
  ]
  edge [
    source 18
    target 827
  ]
  edge [
    source 18
    target 828
  ]
  edge [
    source 18
    target 829
  ]
  edge [
    source 18
    target 830
  ]
  edge [
    source 18
    target 831
  ]
  edge [
    source 18
    target 832
  ]
  edge [
    source 18
    target 833
  ]
  edge [
    source 18
    target 834
  ]
  edge [
    source 18
    target 835
  ]
  edge [
    source 18
    target 836
  ]
  edge [
    source 18
    target 837
  ]
  edge [
    source 18
    target 838
  ]
  edge [
    source 18
    target 839
  ]
  edge [
    source 18
    target 840
  ]
  edge [
    source 18
    target 841
  ]
  edge [
    source 18
    target 842
  ]
  edge [
    source 18
    target 843
  ]
  edge [
    source 18
    target 844
  ]
  edge [
    source 18
    target 845
  ]
  edge [
    source 18
    target 846
  ]
  edge [
    source 18
    target 847
  ]
  edge [
    source 18
    target 848
  ]
  edge [
    source 18
    target 849
  ]
  edge [
    source 18
    target 850
  ]
  edge [
    source 18
    target 851
  ]
  edge [
    source 18
    target 852
  ]
  edge [
    source 18
    target 853
  ]
  edge [
    source 18
    target 854
  ]
  edge [
    source 18
    target 855
  ]
  edge [
    source 18
    target 856
  ]
  edge [
    source 18
    target 857
  ]
  edge [
    source 18
    target 858
  ]
  edge [
    source 18
    target 859
  ]
  edge [
    source 18
    target 860
  ]
  edge [
    source 18
    target 861
  ]
  edge [
    source 18
    target 862
  ]
  edge [
    source 18
    target 863
  ]
  edge [
    source 18
    target 864
  ]
  edge [
    source 18
    target 865
  ]
  edge [
    source 18
    target 866
  ]
  edge [
    source 18
    target 867
  ]
  edge [
    source 18
    target 868
  ]
  edge [
    source 18
    target 869
  ]
  edge [
    source 18
    target 870
  ]
  edge [
    source 18
    target 871
  ]
  edge [
    source 18
    target 872
  ]
  edge [
    source 18
    target 873
  ]
  edge [
    source 18
    target 874
  ]
  edge [
    source 18
    target 875
  ]
  edge [
    source 18
    target 876
  ]
  edge [
    source 18
    target 877
  ]
  edge [
    source 18
    target 878
  ]
  edge [
    source 18
    target 879
  ]
  edge [
    source 18
    target 880
  ]
  edge [
    source 18
    target 881
  ]
  edge [
    source 18
    target 882
  ]
  edge [
    source 18
    target 883
  ]
  edge [
    source 18
    target 884
  ]
  edge [
    source 18
    target 885
  ]
  edge [
    source 18
    target 886
  ]
  edge [
    source 18
    target 887
  ]
  edge [
    source 18
    target 888
  ]
  edge [
    source 18
    target 889
  ]
  edge [
    source 18
    target 890
  ]
  edge [
    source 18
    target 891
  ]
  edge [
    source 18
    target 892
  ]
  edge [
    source 18
    target 893
  ]
  edge [
    source 18
    target 894
  ]
  edge [
    source 18
    target 895
  ]
  edge [
    source 18
    target 896
  ]
  edge [
    source 18
    target 897
  ]
  edge [
    source 18
    target 898
  ]
  edge [
    source 18
    target 899
  ]
  edge [
    source 18
    target 900
  ]
  edge [
    source 18
    target 901
  ]
  edge [
    source 18
    target 902
  ]
  edge [
    source 18
    target 903
  ]
  edge [
    source 18
    target 904
  ]
  edge [
    source 18
    target 905
  ]
  edge [
    source 18
    target 906
  ]
  edge [
    source 18
    target 907
  ]
  edge [
    source 18
    target 908
  ]
  edge [
    source 18
    target 909
  ]
  edge [
    source 18
    target 910
  ]
  edge [
    source 18
    target 911
  ]
  edge [
    source 18
    target 912
  ]
  edge [
    source 18
    target 913
  ]
  edge [
    source 18
    target 914
  ]
  edge [
    source 18
    target 915
  ]
  edge [
    source 18
    target 916
  ]
  edge [
    source 18
    target 917
  ]
  edge [
    source 18
    target 918
  ]
  edge [
    source 18
    target 919
  ]
  edge [
    source 18
    target 920
  ]
  edge [
    source 18
    target 921
  ]
  edge [
    source 18
    target 922
  ]
  edge [
    source 18
    target 923
  ]
  edge [
    source 18
    target 924
  ]
  edge [
    source 18
    target 925
  ]
  edge [
    source 18
    target 926
  ]
  edge [
    source 18
    target 927
  ]
  edge [
    source 18
    target 928
  ]
  edge [
    source 18
    target 929
  ]
  edge [
    source 18
    target 930
  ]
  edge [
    source 18
    target 931
  ]
  edge [
    source 18
    target 932
  ]
  edge [
    source 18
    target 933
  ]
  edge [
    source 18
    target 934
  ]
  edge [
    source 18
    target 935
  ]
  edge [
    source 18
    target 936
  ]
  edge [
    source 18
    target 937
  ]
  edge [
    source 18
    target 938
  ]
  edge [
    source 18
    target 939
  ]
  edge [
    source 18
    target 940
  ]
  edge [
    source 18
    target 941
  ]
  edge [
    source 18
    target 942
  ]
  edge [
    source 18
    target 943
  ]
  edge [
    source 18
    target 944
  ]
  edge [
    source 18
    target 945
  ]
  edge [
    source 18
    target 946
  ]
  edge [
    source 18
    target 947
  ]
  edge [
    source 18
    target 948
  ]
  edge [
    source 18
    target 949
  ]
  edge [
    source 18
    target 950
  ]
  edge [
    source 18
    target 951
  ]
  edge [
    source 18
    target 952
  ]
  edge [
    source 18
    target 953
  ]
  edge [
    source 18
    target 954
  ]
  edge [
    source 18
    target 955
  ]
  edge [
    source 18
    target 956
  ]
  edge [
    source 18
    target 957
  ]
  edge [
    source 18
    target 958
  ]
  edge [
    source 18
    target 959
  ]
  edge [
    source 18
    target 960
  ]
  edge [
    source 18
    target 961
  ]
  edge [
    source 18
    target 962
  ]
  edge [
    source 18
    target 963
  ]
  edge [
    source 18
    target 964
  ]
  edge [
    source 18
    target 965
  ]
  edge [
    source 18
    target 966
  ]
  edge [
    source 18
    target 967
  ]
  edge [
    source 18
    target 968
  ]
  edge [
    source 18
    target 969
  ]
  edge [
    source 18
    target 970
  ]
  edge [
    source 18
    target 971
  ]
  edge [
    source 18
    target 972
  ]
  edge [
    source 18
    target 973
  ]
  edge [
    source 18
    target 974
  ]
  edge [
    source 18
    target 975
  ]
  edge [
    source 18
    target 976
  ]
  edge [
    source 18
    target 183
  ]
  edge [
    source 18
    target 977
  ]
  edge [
    source 18
    target 978
  ]
  edge [
    source 18
    target 979
  ]
  edge [
    source 18
    target 980
  ]
  edge [
    source 18
    target 981
  ]
  edge [
    source 18
    target 982
  ]
  edge [
    source 18
    target 983
  ]
  edge [
    source 18
    target 984
  ]
  edge [
    source 18
    target 985
  ]
  edge [
    source 18
    target 986
  ]
  edge [
    source 18
    target 987
  ]
  edge [
    source 18
    target 988
  ]
  edge [
    source 18
    target 989
  ]
  edge [
    source 18
    target 990
  ]
  edge [
    source 18
    target 991
  ]
  edge [
    source 18
    target 992
  ]
  edge [
    source 18
    target 993
  ]
  edge [
    source 18
    target 994
  ]
  edge [
    source 18
    target 995
  ]
  edge [
    source 18
    target 996
  ]
  edge [
    source 18
    target 997
  ]
  edge [
    source 18
    target 998
  ]
  edge [
    source 18
    target 999
  ]
  edge [
    source 18
    target 1000
  ]
  edge [
    source 18
    target 1001
  ]
  edge [
    source 18
    target 1002
  ]
  edge [
    source 18
    target 1003
  ]
  edge [
    source 18
    target 1004
  ]
  edge [
    source 18
    target 1005
  ]
  edge [
    source 18
    target 1006
  ]
  edge [
    source 18
    target 1007
  ]
  edge [
    source 18
    target 1008
  ]
  edge [
    source 18
    target 1009
  ]
  edge [
    source 18
    target 1010
  ]
  edge [
    source 18
    target 1011
  ]
  edge [
    source 18
    target 1012
  ]
  edge [
    source 18
    target 1013
  ]
  edge [
    source 18
    target 1014
  ]
  edge [
    source 18
    target 1015
  ]
  edge [
    source 18
    target 1016
  ]
  edge [
    source 18
    target 1017
  ]
  edge [
    source 18
    target 1018
  ]
  edge [
    source 18
    target 1019
  ]
  edge [
    source 18
    target 1020
  ]
  edge [
    source 18
    target 1021
  ]
  edge [
    source 18
    target 1022
  ]
  edge [
    source 18
    target 28
  ]
  edge [
    source 18
    target 55
  ]
  edge [
    source 19
    target 1023
  ]
  edge [
    source 19
    target 1024
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 1025
  ]
  edge [
    source 20
    target 1026
  ]
  edge [
    source 21
    target 42
  ]
  edge [
    source 21
    target 43
  ]
  edge [
    source 21
    target 1027
  ]
  edge [
    source 21
    target 1028
  ]
  edge [
    source 21
    target 1029
  ]
  edge [
    source 21
    target 1030
  ]
  edge [
    source 21
    target 1031
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 37
  ]
  edge [
    source 22
    target 38
  ]
  edge [
    source 22
    target 45
  ]
  edge [
    source 22
    target 1032
  ]
  edge [
    source 22
    target 1033
  ]
  edge [
    source 22
    target 1034
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 1035
  ]
  edge [
    source 23
    target 1036
  ]
  edge [
    source 23
    target 1037
  ]
  edge [
    source 23
    target 1038
  ]
  edge [
    source 23
    target 1039
  ]
  edge [
    source 23
    target 1040
  ]
  edge [
    source 23
    target 1041
  ]
  edge [
    source 23
    target 1042
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 26
  ]
  edge [
    source 24
    target 33
  ]
  edge [
    source 25
    target 1043
  ]
  edge [
    source 25
    target 28
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 1044
  ]
  edge [
    source 26
    target 1045
  ]
  edge [
    source 26
    target 1046
  ]
  edge [
    source 26
    target 1047
  ]
  edge [
    source 26
    target 33
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 37
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 1048
  ]
  edge [
    source 28
    target 1049
  ]
  edge [
    source 28
    target 1050
  ]
  edge [
    source 28
    target 1051
  ]
  edge [
    source 28
    target 1052
  ]
  edge [
    source 28
    target 1053
  ]
  edge [
    source 28
    target 1054
  ]
  edge [
    source 28
    target 1055
  ]
  edge [
    source 28
    target 1056
  ]
  edge [
    source 28
    target 1057
  ]
  edge [
    source 28
    target 1058
  ]
  edge [
    source 28
    target 1059
  ]
  edge [
    source 28
    target 1060
  ]
  edge [
    source 28
    target 1061
  ]
  edge [
    source 28
    target 1062
  ]
  edge [
    source 28
    target 1063
  ]
  edge [
    source 28
    target 1064
  ]
  edge [
    source 28
    target 1065
  ]
  edge [
    source 28
    target 1066
  ]
  edge [
    source 28
    target 1067
  ]
  edge [
    source 28
    target 1068
  ]
  edge [
    source 28
    target 55
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 1069
  ]
  edge [
    source 29
    target 1070
  ]
  edge [
    source 29
    target 1071
  ]
  edge [
    source 29
    target 1072
  ]
  edge [
    source 29
    target 1073
  ]
  edge [
    source 29
    target 1074
  ]
  edge [
    source 29
    target 1075
  ]
  edge [
    source 29
    target 1076
  ]
  edge [
    source 29
    target 1077
  ]
  edge [
    source 29
    target 1078
  ]
  edge [
    source 29
    target 1079
  ]
  edge [
    source 29
    target 1080
  ]
  edge [
    source 29
    target 1081
  ]
  edge [
    source 29
    target 1082
  ]
  edge [
    source 29
    target 1083
  ]
  edge [
    source 29
    target 1084
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 1085
  ]
  edge [
    source 30
    target 1086
  ]
  edge [
    source 30
    target 1087
  ]
  edge [
    source 30
    target 1088
  ]
  edge [
    source 30
    target 1089
  ]
  edge [
    source 30
    target 1090
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 1091
  ]
  edge [
    source 31
    target 1092
  ]
  edge [
    source 31
    target 1093
  ]
  edge [
    source 31
    target 1094
  ]
  edge [
    source 31
    target 1095
  ]
  edge [
    source 31
    target 1096
  ]
  edge [
    source 31
    target 1097
  ]
  edge [
    source 31
    target 1098
  ]
  edge [
    source 31
    target 1099
  ]
  edge [
    source 31
    target 1100
  ]
  edge [
    source 31
    target 1101
  ]
  edge [
    source 31
    target 1102
  ]
  edge [
    source 31
    target 1103
  ]
  edge [
    source 31
    target 1104
  ]
  edge [
    source 31
    target 1105
  ]
  edge [
    source 31
    target 424
  ]
  edge [
    source 31
    target 1106
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 1069
  ]
  edge [
    source 32
    target 432
  ]
  edge [
    source 32
    target 1107
  ]
  edge [
    source 32
    target 1108
  ]
  edge [
    source 32
    target 1109
  ]
  edge [
    source 32
    target 1110
  ]
  edge [
    source 32
    target 1111
  ]
  edge [
    source 32
    target 1112
  ]
  edge [
    source 32
    target 1113
  ]
  edge [
    source 32
    target 1114
  ]
  edge [
    source 32
    target 1115
  ]
  edge [
    source 32
    target 1116
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 1117
  ]
  edge [
    source 33
    target 1118
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 48
  ]
  edge [
    source 34
    target 1119
  ]
  edge [
    source 34
    target 50
  ]
  edge [
    source 34
    target 1120
  ]
  edge [
    source 34
    target 1121
  ]
  edge [
    source 34
    target 1122
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 1123
  ]
  edge [
    source 36
    target 1124
  ]
  edge [
    source 36
    target 1125
  ]
  edge [
    source 36
    target 1126
  ]
  edge [
    source 36
    target 1127
  ]
  edge [
    source 37
    target 43
  ]
  edge [
    source 37
    target 44
  ]
  edge [
    source 37
    target 56
  ]
  edge [
    source 37
    target 57
  ]
  edge [
    source 37
    target 1128
  ]
  edge [
    source 37
    target 1129
  ]
  edge [
    source 37
    target 1130
  ]
  edge [
    source 37
    target 1131
  ]
  edge [
    source 37
    target 1132
  ]
  edge [
    source 37
    target 1133
  ]
  edge [
    source 37
    target 1134
  ]
  edge [
    source 37
    target 52
  ]
  edge [
    source 37
    target 42
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 1135
  ]
  edge [
    source 38
    target 1136
  ]
  edge [
    source 38
    target 1137
  ]
  edge [
    source 38
    target 340
  ]
  edge [
    source 38
    target 1138
  ]
  edge [
    source 38
    target 1139
  ]
  edge [
    source 38
    target 1140
  ]
  edge [
    source 38
    target 1141
  ]
  edge [
    source 38
    target 1142
  ]
  edge [
    source 38
    target 1143
  ]
  edge [
    source 38
    target 1144
  ]
  edge [
    source 38
    target 1145
  ]
  edge [
    source 38
    target 1146
  ]
  edge [
    source 38
    target 1147
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 1148
  ]
  edge [
    source 39
    target 1149
  ]
  edge [
    source 39
    target 1150
  ]
  edge [
    source 39
    target 1151
  ]
  edge [
    source 39
    target 1152
  ]
  edge [
    source 39
    target 1153
  ]
  edge [
    source 39
    target 1154
  ]
  edge [
    source 39
    target 1155
  ]
  edge [
    source 39
    target 1156
  ]
  edge [
    source 39
    target 1157
  ]
  edge [
    source 39
    target 1158
  ]
  edge [
    source 39
    target 1159
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 1160
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 42
    target 54
  ]
  edge [
    source 42
    target 55
  ]
  edge [
    source 42
    target 1161
  ]
  edge [
    source 42
    target 1162
  ]
  edge [
    source 42
    target 380
  ]
  edge [
    source 42
    target 1163
  ]
  edge [
    source 42
    target 1058
  ]
  edge [
    source 42
    target 1164
  ]
  edge [
    source 42
    target 1165
  ]
  edge [
    source 42
    target 1166
  ]
  edge [
    source 42
    target 1167
  ]
  edge [
    source 42
    target 1168
  ]
  edge [
    source 42
    target 1169
  ]
  edge [
    source 42
    target 1170
  ]
  edge [
    source 42
    target 1171
  ]
  edge [
    source 42
    target 1172
  ]
  edge [
    source 42
    target 1173
  ]
  edge [
    source 42
    target 1174
  ]
  edge [
    source 42
    target 1175
  ]
  edge [
    source 42
    target 1176
  ]
  edge [
    source 42
    target 1177
  ]
  edge [
    source 42
    target 1178
  ]
  edge [
    source 42
    target 1179
  ]
  edge [
    source 42
    target 1180
  ]
  edge [
    source 42
    target 1181
  ]
  edge [
    source 42
    target 1182
  ]
  edge [
    source 42
    target 304
  ]
  edge [
    source 42
    target 1183
  ]
  edge [
    source 42
    target 52
  ]
  edge [
    source 43
    target 1184
  ]
  edge [
    source 43
    target 1185
  ]
  edge [
    source 43
    target 1186
  ]
  edge [
    source 43
    target 1187
  ]
  edge [
    source 43
    target 1188
  ]
  edge [
    source 43
    target 1189
  ]
  edge [
    source 43
    target 1190
  ]
  edge [
    source 43
    target 1191
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 1192
  ]
  edge [
    source 45
    target 1193
  ]
  edge [
    source 45
    target 1194
  ]
  edge [
    source 45
    target 1151
  ]
  edge [
    source 45
    target 1195
  ]
  edge [
    source 45
    target 1196
  ]
  edge [
    source 45
    target 1197
  ]
  edge [
    source 45
    target 1198
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 1199
  ]
  edge [
    source 47
    target 1200
  ]
  edge [
    source 47
    target 1201
  ]
  edge [
    source 47
    target 1202
  ]
  edge [
    source 47
    target 1203
  ]
  edge [
    source 47
    target 1204
  ]
  edge [
    source 47
    target 1205
  ]
  edge [
    source 47
    target 1126
  ]
  edge [
    source 47
    target 1127
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 1206
  ]
  edge [
    source 48
    target 1207
  ]
  edge [
    source 48
    target 362
  ]
  edge [
    source 48
    target 1208
  ]
  edge [
    source 48
    target 1209
  ]
  edge [
    source 48
    target 1210
  ]
  edge [
    source 48
    target 360
  ]
  edge [
    source 48
    target 1211
  ]
  edge [
    source 48
    target 1212
  ]
  edge [
    source 48
    target 1213
  ]
  edge [
    source 48
    target 368
  ]
  edge [
    source 48
    target 1214
  ]
  edge [
    source 48
    target 1215
  ]
  edge [
    source 48
    target 1216
  ]
  edge [
    source 48
    target 1217
  ]
  edge [
    source 48
    target 1218
  ]
  edge [
    source 48
    target 1219
  ]
  edge [
    source 48
    target 1220
  ]
  edge [
    source 48
    target 1221
  ]
  edge [
    source 48
    target 1222
  ]
  edge [
    source 48
    target 1223
  ]
  edge [
    source 48
    target 1224
  ]
  edge [
    source 48
    target 1225
  ]
  edge [
    source 48
    target 1226
  ]
  edge [
    source 48
    target 1227
  ]
  edge [
    source 48
    target 1228
  ]
  edge [
    source 48
    target 1229
  ]
  edge [
    source 48
    target 1230
  ]
  edge [
    source 48
    target 1231
  ]
  edge [
    source 48
    target 1232
  ]
  edge [
    source 48
    target 1233
  ]
  edge [
    source 48
    target 1234
  ]
  edge [
    source 48
    target 1235
  ]
  edge [
    source 48
    target 1236
  ]
  edge [
    source 48
    target 1237
  ]
  edge [
    source 48
    target 1238
  ]
  edge [
    source 48
    target 1239
  ]
  edge [
    source 48
    target 50
  ]
  edge [
    source 48
    target 55
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 1240
  ]
  edge [
    source 49
    target 1241
  ]
  edge [
    source 49
    target 1242
  ]
  edge [
    source 49
    target 1243
  ]
  edge [
    source 49
    target 1244
  ]
  edge [
    source 49
    target 1245
  ]
  edge [
    source 49
    target 424
  ]
  edge [
    source 49
    target 1246
  ]
  edge [
    source 49
    target 1247
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 1248
  ]
  edge [
    source 50
    target 1249
  ]
  edge [
    source 50
    target 1250
  ]
  edge [
    source 50
    target 1251
  ]
  edge [
    source 50
    target 1252
  ]
  edge [
    source 50
    target 108
  ]
  edge [
    source 50
    target 1253
  ]
  edge [
    source 50
    target 1254
  ]
  edge [
    source 50
    target 1255
  ]
  edge [
    source 50
    target 1256
  ]
  edge [
    source 50
    target 1257
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 52
    target 1258
  ]
  edge [
    source 52
    target 1259
  ]
  edge [
    source 52
    target 1260
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 1023
  ]
  edge [
    source 53
    target 1261
  ]
  edge [
    source 54
    target 1262
  ]
  edge [
    source 54
    target 1263
  ]
  edge [
    source 54
    target 1264
  ]
  edge [
    source 54
    target 1265
  ]
  edge [
    source 54
    target 417
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 1193
  ]
  edge [
    source 55
    target 1266
  ]
  edge [
    source 55
    target 315
  ]
  edge [
    source 55
    target 1267
  ]
  edge [
    source 55
    target 370
  ]
  edge [
    source 55
    target 1268
  ]
  edge [
    source 56
    target 1269
  ]
  edge [
    source 56
    target 362
  ]
  edge [
    source 56
    target 1270
  ]
  edge [
    source 56
    target 1271
  ]
  edge [
    source 56
    target 1272
  ]
  edge [
    source 56
    target 424
  ]
  edge [
    source 56
    target 1273
  ]
  edge [
    source 56
    target 1274
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 1275
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 1276
  ]
  edge [
    source 58
    target 1277
  ]
  edge [
    source 58
    target 1278
  ]
  edge [
    source 58
    target 1279
  ]
  edge [
    source 58
    target 1280
  ]
  edge [
    source 58
    target 1281
  ]
  edge [
    source 58
    target 1282
  ]
  edge [
    source 58
    target 1283
  ]
  edge [
    source 58
    target 1284
  ]
  edge [
    source 58
    target 1285
  ]
  edge [
    source 58
    target 1286
  ]
  edge [
    source 58
    target 1287
  ]
  edge [
    source 58
    target 1288
  ]
  edge [
    source 58
    target 1289
  ]
  edge [
    source 58
    target 1290
  ]
  edge [
    source 58
    target 1127
  ]
  edge [
    source 58
    target 1291
  ]
  edge [
    source 58
    target 1292
  ]
  edge [
    source 58
    target 1293
  ]
  edge [
    source 58
    target 1294
  ]
  edge [
    source 58
    target 1295
  ]
  edge [
    source 58
    target 1296
  ]
  edge [
    source 58
    target 1297
  ]
  edge [
    source 58
    target 1298
  ]
  edge [
    source 58
    target 1299
  ]
  edge [
    source 58
    target 1300
  ]
  edge [
    source 58
    target 1301
  ]
  edge [
    source 58
    target 1302
  ]
  edge [
    source 58
    target 1303
  ]
  edge [
    source 58
    target 1304
  ]
  edge [
    source 58
    target 1305
  ]
  edge [
    source 58
    target 1306
  ]
  edge [
    source 59
    target 1307
  ]
  edge [
    source 59
    target 318
  ]
  edge [
    source 59
    target 1308
  ]
  edge [
    source 59
    target 1309
  ]
  edge [
    source 59
    target 1310
  ]
  edge [
    source 59
    target 1311
  ]
  edge [
    source 59
    target 1312
  ]
  edge [
    source 59
    target 1313
  ]
  edge [
    source 59
    target 1314
  ]
  edge [
    source 59
    target 1315
  ]
  edge [
    source 59
    target 1316
  ]
  edge [
    source 59
    target 1317
  ]
  edge [
    source 1318
    target 1319
  ]
  edge [
    source 1320
    target 1321
  ]
  edge [
    source 1320
    target 1322
  ]
  edge [
    source 1320
    target 1323
  ]
  edge [
    source 1320
    target 1324
  ]
  edge [
    source 1320
    target 1325
  ]
  edge [
    source 1320
    target 1326
  ]
  edge [
    source 1321
    target 1322
  ]
  edge [
    source 1321
    target 1323
  ]
  edge [
    source 1321
    target 1324
  ]
  edge [
    source 1321
    target 1325
  ]
  edge [
    source 1321
    target 1326
  ]
  edge [
    source 1322
    target 1323
  ]
  edge [
    source 1322
    target 1324
  ]
  edge [
    source 1322
    target 1325
  ]
  edge [
    source 1322
    target 1326
  ]
  edge [
    source 1323
    target 1324
  ]
  edge [
    source 1323
    target 1325
  ]
  edge [
    source 1323
    target 1326
  ]
  edge [
    source 1323
    target 1329
  ]
  edge [
    source 1324
    target 1325
  ]
  edge [
    source 1324
    target 1326
  ]
  edge [
    source 1325
    target 1326
  ]
  edge [
    source 1327
    target 1328
  ]
  edge [
    source 1330
    target 1331
  ]
  edge [
    source 1332
    target 1333
  ]
]
