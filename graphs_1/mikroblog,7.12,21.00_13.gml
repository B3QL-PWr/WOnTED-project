graph [
  maxDegree 19
  minDegree 1
  meanDegree 2.8363636363636364
  density 0.052525252525252523
  graphCliqueNumber 8
  node [
    id 0
    label "nikt"
    origin "text"
  ]
  node [
    id 1
    label "od&#322;&#261;cza&#263;"
    origin "text"
  ]
  node [
    id 2
    label "internet"
    origin "text"
  ]
  node [
    id 3
    label "si&#281;"
    origin "text"
  ]
  node [
    id 4
    label "rodzi&#263;"
    origin "text"
  ]
  node [
    id 5
    label "fake"
    origin "text"
  ]
  node [
    id 6
    label "news"
    origin "text"
  ]
  node [
    id 7
    label "miernota"
  ]
  node [
    id 8
    label "ciura"
  ]
  node [
    id 9
    label "challenge"
  ]
  node [
    id 10
    label "oddziela&#263;_si&#281;"
  ]
  node [
    id 11
    label "odcina&#263;"
  ]
  node [
    id 12
    label "pacjent"
  ]
  node [
    id 13
    label "abstract"
  ]
  node [
    id 14
    label "oddziela&#263;"
  ]
  node [
    id 15
    label "przerywa&#263;"
  ]
  node [
    id 16
    label "take"
  ]
  node [
    id 17
    label "us&#322;uga_internetowa"
  ]
  node [
    id 18
    label "biznes_elektroniczny"
  ]
  node [
    id 19
    label "punkt_dost&#281;pu"
  ]
  node [
    id 20
    label "hipertekst"
  ]
  node [
    id 21
    label "gra_sieciowa"
  ]
  node [
    id 22
    label "mem"
  ]
  node [
    id 23
    label "e-hazard"
  ]
  node [
    id 24
    label "sie&#263;_komputerowa"
  ]
  node [
    id 25
    label "media"
  ]
  node [
    id 26
    label "podcast"
  ]
  node [
    id 27
    label "netbook"
  ]
  node [
    id 28
    label "provider"
  ]
  node [
    id 29
    label "cyberprzestrze&#324;"
  ]
  node [
    id 30
    label "grooming"
  ]
  node [
    id 31
    label "strona"
  ]
  node [
    id 32
    label "give"
  ]
  node [
    id 33
    label "create"
  ]
  node [
    id 34
    label "plon"
  ]
  node [
    id 35
    label "wytwarza&#263;"
  ]
  node [
    id 36
    label "system"
  ]
  node [
    id 37
    label "nowostka"
  ]
  node [
    id 38
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 39
    label "doniesienie"
  ]
  node [
    id 40
    label "informacja"
  ]
  node [
    id 41
    label "message"
  ]
  node [
    id 42
    label "nius"
  ]
  node [
    id 43
    label "Daniel"
  ]
  node [
    id 44
    label "Magicalowi"
  ]
  node [
    id 45
    label "rzecznik"
  ]
  node [
    id 46
    label "Orange"
  ]
  node [
    id 47
    label "Wojtek"
  ]
  node [
    id 48
    label "Jabczy&#324;skiego"
  ]
  node [
    id 49
    label "okr&#261;g&#322;y"
  ]
  node [
    id 50
    label "st&#243;&#322;"
  ]
  node [
    id 51
    label "prawy"
  ]
  node [
    id 52
    label "obywatelski"
  ]
  node [
    id 53
    label "deklaracja"
  ]
  node [
    id 54
    label "uczestnik"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 47
  ]
  edge [
    source 45
    target 48
  ]
  edge [
    source 45
    target 49
  ]
  edge [
    source 45
    target 50
  ]
  edge [
    source 45
    target 51
  ]
  edge [
    source 45
    target 52
  ]
  edge [
    source 45
    target 53
  ]
  edge [
    source 45
    target 54
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 48
  ]
  edge [
    source 46
    target 53
  ]
  edge [
    source 46
    target 54
  ]
  edge [
    source 46
    target 49
  ]
  edge [
    source 46
    target 50
  ]
  edge [
    source 46
    target 51
  ]
  edge [
    source 46
    target 52
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 51
  ]
  edge [
    source 49
    target 52
  ]
  edge [
    source 49
    target 53
  ]
  edge [
    source 49
    target 54
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 52
  ]
  edge [
    source 50
    target 53
  ]
  edge [
    source 50
    target 54
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 53
  ]
  edge [
    source 51
    target 54
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 54
  ]
  edge [
    source 53
    target 54
  ]
]
