graph [
  maxDegree 6
  minDegree 1
  meanDegree 1.6666666666666667
  density 0.15151515151515152
  graphCliqueNumber 2
  node [
    id 0
    label "probostwo"
    origin "text"
  ]
  node [
    id 1
    label "dolny"
    origin "text"
  ]
  node [
    id 2
    label "mieszkanie_s&#322;u&#380;bowe"
  ]
  node [
    id 3
    label "dom"
  ]
  node [
    id 4
    label "nisko"
  ]
  node [
    id 5
    label "zminimalizowanie"
  ]
  node [
    id 6
    label "minimalnie"
  ]
  node [
    id 7
    label "graniczny"
  ]
  node [
    id 8
    label "minimalizowanie"
  ]
  node [
    id 9
    label "kujawsko"
  ]
  node [
    id 10
    label "pomorski"
  ]
  node [
    id 11
    label "g&#243;rny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 9
    target 10
  ]
]
