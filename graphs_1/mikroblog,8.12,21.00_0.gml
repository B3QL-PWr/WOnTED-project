graph [
  maxDegree 6
  minDegree 1
  meanDegree 1.7
  density 0.08947368421052632
  graphCliqueNumber 2
  node [
    id 0
    label "piek&#322;o"
    origin "text"
  ]
  node [
    id 1
    label "polak"
    origin "text"
  ]
  node [
    id 2
    label "trzeba"
    origin "text"
  ]
  node [
    id 3
    label "pilnowa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "duch"
  ]
  node [
    id 5
    label "szeol"
  ]
  node [
    id 6
    label "horror"
  ]
  node [
    id 7
    label "za&#347;wiaty"
  ]
  node [
    id 8
    label "pokutowanie"
  ]
  node [
    id 9
    label "polski"
  ]
  node [
    id 10
    label "trza"
  ]
  node [
    id 11
    label "necessity"
  ]
  node [
    id 12
    label "zachowywa&#263;"
  ]
  node [
    id 13
    label "robi&#263;"
  ]
  node [
    id 14
    label "str&#243;&#380;y&#263;"
  ]
  node [
    id 15
    label "continue"
  ]
  node [
    id 16
    label "Polska"
  ]
  node [
    id 17
    label "bit"
  ]
  node [
    id 18
    label "urz&#261;d"
  ]
  node [
    id 19
    label "skarbowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 18
    target 19
  ]
]
