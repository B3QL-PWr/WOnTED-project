graph [
  maxDegree 24
  minDegree 1
  meanDegree 1.9714285714285715
  density 0.02857142857142857
  graphCliqueNumber 2
  node [
    id 0
    label "w&#322;och"
    origin "text"
  ]
  node [
    id 1
    label "chcie&#263;"
    origin "text"
  ]
  node [
    id 2
    label "wprowadzi&#263;"
    origin "text"
  ]
  node [
    id 3
    label "tak_zwany"
    origin "text"
  ]
  node [
    id 4
    label "ekopodatek"
    origin "text"
  ]
  node [
    id 5
    label "samoch&#243;d"
    origin "text"
  ]
  node [
    id 6
    label "tradycyjny"
    origin "text"
  ]
  node [
    id 7
    label "paliwo"
    origin "text"
  ]
  node [
    id 8
    label "czu&#263;"
  ]
  node [
    id 9
    label "desire"
  ]
  node [
    id 10
    label "kcie&#263;"
  ]
  node [
    id 11
    label "wej&#347;&#263;"
  ]
  node [
    id 12
    label "rynek"
  ]
  node [
    id 13
    label "zacz&#261;&#263;"
  ]
  node [
    id 14
    label "zej&#347;&#263;"
  ]
  node [
    id 15
    label "spowodowa&#263;"
  ]
  node [
    id 16
    label "wpisa&#263;"
  ]
  node [
    id 17
    label "insert"
  ]
  node [
    id 18
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 19
    label "testify"
  ]
  node [
    id 20
    label "indicate"
  ]
  node [
    id 21
    label "zapozna&#263;"
  ]
  node [
    id 22
    label "umie&#347;ci&#263;"
  ]
  node [
    id 23
    label "zak&#322;&#243;ci&#263;"
  ]
  node [
    id 24
    label "zrobi&#263;"
  ]
  node [
    id 25
    label "doprowadzi&#263;"
  ]
  node [
    id 26
    label "picture"
  ]
  node [
    id 27
    label "baga&#380;nik"
  ]
  node [
    id 28
    label "immobilizer"
  ]
  node [
    id 29
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 30
    label "ko&#322;o_samochodowe"
  ]
  node [
    id 31
    label "poduszka_powietrzna"
  ]
  node [
    id 32
    label "dachowanie"
  ]
  node [
    id 33
    label "dwu&#347;lad"
  ]
  node [
    id 34
    label "deska_rozdzielcza"
  ]
  node [
    id 35
    label "poci&#261;g_drogowy"
  ]
  node [
    id 36
    label "kierownica"
  ]
  node [
    id 37
    label "pojazd_drogowy"
  ]
  node [
    id 38
    label "uk&#322;ad_kierowniczy"
  ]
  node [
    id 39
    label "pompa_wodna"
  ]
  node [
    id 40
    label "silnik"
  ]
  node [
    id 41
    label "wycieraczka"
  ]
  node [
    id 42
    label "bak"
  ]
  node [
    id 43
    label "ABS"
  ]
  node [
    id 44
    label "most"
  ]
  node [
    id 45
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 46
    label "spryskiwacz"
  ]
  node [
    id 47
    label "t&#322;umik"
  ]
  node [
    id 48
    label "tempomat"
  ]
  node [
    id 49
    label "nienowoczesny"
  ]
  node [
    id 50
    label "zachowawczy"
  ]
  node [
    id 51
    label "zwyk&#322;y"
  ]
  node [
    id 52
    label "zwyczajowy"
  ]
  node [
    id 53
    label "modelowy"
  ]
  node [
    id 54
    label "przyj&#281;ty"
  ]
  node [
    id 55
    label "wierny"
  ]
  node [
    id 56
    label "tradycyjnie"
  ]
  node [
    id 57
    label "surowy"
  ]
  node [
    id 58
    label "spalenie"
  ]
  node [
    id 59
    label "spali&#263;"
  ]
  node [
    id 60
    label "fuel"
  ]
  node [
    id 61
    label "tankowanie"
  ]
  node [
    id 62
    label "zgazowa&#263;"
  ]
  node [
    id 63
    label "pompa_wtryskowa"
  ]
  node [
    id 64
    label "tankowa&#263;"
  ]
  node [
    id 65
    label "antydetonator"
  ]
  node [
    id 66
    label "Orlen"
  ]
  node [
    id 67
    label "spalanie"
  ]
  node [
    id 68
    label "spala&#263;"
  ]
  node [
    id 69
    label "substancja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
]
