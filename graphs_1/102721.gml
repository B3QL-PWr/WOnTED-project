graph [
  maxDegree 221
  minDegree 1
  meanDegree 2.23046875
  density 0.004364909491193738
  graphCliqueNumber 4
  node [
    id 0
    label "podstawa"
    origin "text"
  ]
  node [
    id 1
    label "usta"
    origin "text"
  ]
  node [
    id 2
    label "rozporz&#261;dzenie"
    origin "text"
  ]
  node [
    id 3
    label "rad"
    origin "text"
  ]
  node [
    id 4
    label "minister"
    origin "text"
  ]
  node [
    id 5
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 6
    label "grudzie&#324;"
    origin "text"
  ]
  node [
    id 7
    label "rocznik"
    origin "text"
  ]
  node [
    id 8
    label "sprawa"
    origin "text"
  ]
  node [
    id 9
    label "uposa&#380;enie"
    origin "text"
  ]
  node [
    id 10
    label "nale&#380;no&#347;&#263;"
    origin "text"
  ]
  node [
    id 11
    label "pieni&#281;&#380;ny"
    origin "text"
  ]
  node [
    id 12
    label "otrzymywa&#263;"
    origin "text"
  ]
  node [
    id 13
    label "przez"
    origin "text"
  ]
  node [
    id 14
    label "&#380;o&#322;nierz"
    origin "text"
  ]
  node [
    id 15
    label "wyznaczy&#263;"
    origin "text"
  ]
  node [
    id 16
    label "pe&#322;nie&#263;"
    origin "text"
  ]
  node [
    id 17
    label "s&#322;u&#380;ba"
    origin "text"
  ]
  node [
    id 18
    label "poza"
    origin "text"
  ]
  node [
    id 19
    label "granica"
    origin "text"
  ]
  node [
    id 20
    label "pa&#324;stwo"
    origin "text"
  ]
  node [
    id 21
    label "poz"
    origin "text"
  ]
  node [
    id 22
    label "&#347;wiadczenie"
    origin "text"
  ]
  node [
    id 23
    label "pracownik"
    origin "text"
  ]
  node [
    id 24
    label "zatrudniona"
    origin "text"
  ]
  node [
    id 25
    label "jednostka"
    origin "text"
  ]
  node [
    id 26
    label "wojskowy"
    origin "text"
  ]
  node [
    id 27
    label "wykonywa&#263;"
    origin "text"
  ]
  node [
    id 28
    label "zadanie"
    origin "text"
  ]
  node [
    id 29
    label "dziennik"
    origin "text"
  ]
  node [
    id 30
    label "ustala&#263;"
    origin "text"
  ]
  node [
    id 31
    label "podstawowy"
  ]
  node [
    id 32
    label "strategia"
  ]
  node [
    id 33
    label "pot&#281;ga"
  ]
  node [
    id 34
    label "zasadzenie"
  ]
  node [
    id 35
    label "za&#322;o&#380;enie"
  ]
  node [
    id 36
    label "&#347;ciana"
  ]
  node [
    id 37
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 38
    label "przedmiot"
  ]
  node [
    id 39
    label "documentation"
  ]
  node [
    id 40
    label "dzieci&#281;ctwo"
  ]
  node [
    id 41
    label "pomys&#322;"
  ]
  node [
    id 42
    label "bok"
  ]
  node [
    id 43
    label "d&#243;&#322;"
  ]
  node [
    id 44
    label "punkt_odniesienia"
  ]
  node [
    id 45
    label "column"
  ]
  node [
    id 46
    label "zasadzi&#263;"
  ]
  node [
    id 47
    label "background"
  ]
  node [
    id 48
    label "warga_dolna"
  ]
  node [
    id 49
    label "ryjek"
  ]
  node [
    id 50
    label "zaci&#261;&#263;"
  ]
  node [
    id 51
    label "ssa&#263;"
  ]
  node [
    id 52
    label "twarz"
  ]
  node [
    id 53
    label "dzi&#243;b"
  ]
  node [
    id 54
    label "&#322;uk_Kupidyna"
  ]
  node [
    id 55
    label "ssanie"
  ]
  node [
    id 56
    label "zaci&#281;cie"
  ]
  node [
    id 57
    label "jadaczka"
  ]
  node [
    id 58
    label "zacinanie"
  ]
  node [
    id 59
    label "organ"
  ]
  node [
    id 60
    label "jama_ustna"
  ]
  node [
    id 61
    label "otw&#243;r_g&#281;bowy"
  ]
  node [
    id 62
    label "warga_g&#243;rna"
  ]
  node [
    id 63
    label "zacina&#263;"
  ]
  node [
    id 64
    label "rule"
  ]
  node [
    id 65
    label "polecenie"
  ]
  node [
    id 66
    label "akt"
  ]
  node [
    id 67
    label "arrangement"
  ]
  node [
    id 68
    label "zarz&#261;dzenie"
  ]
  node [
    id 69
    label "commission"
  ]
  node [
    id 70
    label "ordonans"
  ]
  node [
    id 71
    label "berylowiec"
  ]
  node [
    id 72
    label "u&#347;miechni&#281;ty"
  ]
  node [
    id 73
    label "pierwiastek_promieniotw&#243;rczy"
  ]
  node [
    id 74
    label "mikroradian"
  ]
  node [
    id 75
    label "zadowolenie_si&#281;"
  ]
  node [
    id 76
    label "usatysfakcjonowanie_si&#281;"
  ]
  node [
    id 77
    label "content"
  ]
  node [
    id 78
    label "jednostka_promieniowania"
  ]
  node [
    id 79
    label "miliradian"
  ]
  node [
    id 80
    label "Goebbels"
  ]
  node [
    id 81
    label "Sto&#322;ypin"
  ]
  node [
    id 82
    label "rz&#261;d"
  ]
  node [
    id 83
    label "dostojnik"
  ]
  node [
    id 84
    label "s&#322;o&#324;ce"
  ]
  node [
    id 85
    label "czynienie_si&#281;"
  ]
  node [
    id 86
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 87
    label "czas"
  ]
  node [
    id 88
    label "long_time"
  ]
  node [
    id 89
    label "przedpo&#322;udnie"
  ]
  node [
    id 90
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 91
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 92
    label "tydzie&#324;"
  ]
  node [
    id 93
    label "godzina"
  ]
  node [
    id 94
    label "t&#322;usty_czwartek"
  ]
  node [
    id 95
    label "wsta&#263;"
  ]
  node [
    id 96
    label "day"
  ]
  node [
    id 97
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 98
    label "przedwiecz&#243;r"
  ]
  node [
    id 99
    label "Sylwester"
  ]
  node [
    id 100
    label "po&#322;udnie"
  ]
  node [
    id 101
    label "wzej&#347;cie"
  ]
  node [
    id 102
    label "podwiecz&#243;r"
  ]
  node [
    id 103
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 104
    label "rano"
  ]
  node [
    id 105
    label "termin"
  ]
  node [
    id 106
    label "ranek"
  ]
  node [
    id 107
    label "doba"
  ]
  node [
    id 108
    label "wiecz&#243;r"
  ]
  node [
    id 109
    label "walentynki"
  ]
  node [
    id 110
    label "popo&#322;udnie"
  ]
  node [
    id 111
    label "noc"
  ]
  node [
    id 112
    label "wstanie"
  ]
  node [
    id 113
    label "Bo&#380;e_Narodzenie"
  ]
  node [
    id 114
    label "miesi&#261;c"
  ]
  node [
    id 115
    label "Barb&#243;rka"
  ]
  node [
    id 116
    label "formacja"
  ]
  node [
    id 117
    label "kronika"
  ]
  node [
    id 118
    label "czasopismo"
  ]
  node [
    id 119
    label "yearbook"
  ]
  node [
    id 120
    label "temat"
  ]
  node [
    id 121
    label "kognicja"
  ]
  node [
    id 122
    label "idea"
  ]
  node [
    id 123
    label "szczeg&#243;&#322;"
  ]
  node [
    id 124
    label "rzecz"
  ]
  node [
    id 125
    label "wydarzenie"
  ]
  node [
    id 126
    label "przes&#322;anka"
  ]
  node [
    id 127
    label "rozprawa"
  ]
  node [
    id 128
    label "object"
  ]
  node [
    id 129
    label "proposition"
  ]
  node [
    id 130
    label "salarium"
  ]
  node [
    id 131
    label "wynagrodzenie"
  ]
  node [
    id 132
    label "wage"
  ]
  node [
    id 133
    label "wyposa&#380;enie"
  ]
  node [
    id 134
    label "kategoria_urz&#281;dnicza"
  ]
  node [
    id 135
    label "salariat"
  ]
  node [
    id 136
    label "&#347;ci&#261;ganie"
  ]
  node [
    id 137
    label "fee"
  ]
  node [
    id 138
    label "uregulowa&#263;"
  ]
  node [
    id 139
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 140
    label "kwota"
  ]
  node [
    id 141
    label "&#347;ci&#261;ga&#263;"
  ]
  node [
    id 142
    label "monetarny"
  ]
  node [
    id 143
    label "wytwarza&#263;"
  ]
  node [
    id 144
    label "take"
  ]
  node [
    id 145
    label "dostawa&#263;"
  ]
  node [
    id 146
    label "return"
  ]
  node [
    id 147
    label "cz&#322;owiek"
  ]
  node [
    id 148
    label "demobilizowa&#263;"
  ]
  node [
    id 149
    label "rota"
  ]
  node [
    id 150
    label "demobilizowanie"
  ]
  node [
    id 151
    label "walcz&#261;cy"
  ]
  node [
    id 152
    label "harcap"
  ]
  node [
    id 153
    label "&#380;o&#322;nierstwo"
  ]
  node [
    id 154
    label "&#380;o&#322;dowy"
  ]
  node [
    id 155
    label "elew"
  ]
  node [
    id 156
    label "zdemobilizowanie"
  ]
  node [
    id 157
    label "mundurowy"
  ]
  node [
    id 158
    label "s&#322;u&#380;y&#263;_w_wojsku"
  ]
  node [
    id 159
    label "so&#322;dat"
  ]
  node [
    id 160
    label "wojsko"
  ]
  node [
    id 161
    label "s&#322;u&#380;enie_w_wojsku"
  ]
  node [
    id 162
    label "zdemobilizowa&#263;"
  ]
  node [
    id 163
    label "Gurkha"
  ]
  node [
    id 164
    label "aim"
  ]
  node [
    id 165
    label "okre&#347;li&#263;"
  ]
  node [
    id 166
    label "wybra&#263;"
  ]
  node [
    id 167
    label "sign"
  ]
  node [
    id 168
    label "position"
  ]
  node [
    id 169
    label "zaznaczy&#263;"
  ]
  node [
    id 170
    label "ustali&#263;"
  ]
  node [
    id 171
    label "set"
  ]
  node [
    id 172
    label "service"
  ]
  node [
    id 173
    label "ZOMO"
  ]
  node [
    id 174
    label "czworak"
  ]
  node [
    id 175
    label "zesp&#243;&#322;"
  ]
  node [
    id 176
    label "s&#322;u&#380;&#261;ca"
  ]
  node [
    id 177
    label "instytucja"
  ]
  node [
    id 178
    label "s&#322;u&#380;&#261;cy"
  ]
  node [
    id 179
    label "praca"
  ]
  node [
    id 180
    label "wys&#322;uga"
  ]
  node [
    id 181
    label "mode"
  ]
  node [
    id 182
    label "gra"
  ]
  node [
    id 183
    label "przesada"
  ]
  node [
    id 184
    label "ustawienie"
  ]
  node [
    id 185
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 186
    label "zakres"
  ]
  node [
    id 187
    label "Ural"
  ]
  node [
    id 188
    label "koniec"
  ]
  node [
    id 189
    label "kres"
  ]
  node [
    id 190
    label "granice"
  ]
  node [
    id 191
    label "granica_pa&#324;stwa"
  ]
  node [
    id 192
    label "pu&#322;ap"
  ]
  node [
    id 193
    label "frontier"
  ]
  node [
    id 194
    label "end"
  ]
  node [
    id 195
    label "miara"
  ]
  node [
    id 196
    label "poj&#281;cie"
  ]
  node [
    id 197
    label "przej&#347;cie"
  ]
  node [
    id 198
    label "Filipiny"
  ]
  node [
    id 199
    label "Rwanda"
  ]
  node [
    id 200
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 201
    label "Monako"
  ]
  node [
    id 202
    label "Korea"
  ]
  node [
    id 203
    label "Ghana"
  ]
  node [
    id 204
    label "Czarnog&#243;ra"
  ]
  node [
    id 205
    label "Malawi"
  ]
  node [
    id 206
    label "Indonezja"
  ]
  node [
    id 207
    label "Bu&#322;garia"
  ]
  node [
    id 208
    label "Nauru"
  ]
  node [
    id 209
    label "Kenia"
  ]
  node [
    id 210
    label "Kambod&#380;a"
  ]
  node [
    id 211
    label "Mali"
  ]
  node [
    id 212
    label "Austria"
  ]
  node [
    id 213
    label "interior"
  ]
  node [
    id 214
    label "Armenia"
  ]
  node [
    id 215
    label "Fid&#380;i"
  ]
  node [
    id 216
    label "Tuwalu"
  ]
  node [
    id 217
    label "Etiopia"
  ]
  node [
    id 218
    label "Malta"
  ]
  node [
    id 219
    label "Malezja"
  ]
  node [
    id 220
    label "Grenada"
  ]
  node [
    id 221
    label "Tad&#380;ykistan"
  ]
  node [
    id 222
    label "Wehrlen"
  ]
  node [
    id 223
    label "para"
  ]
  node [
    id 224
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 225
    label "Rumunia"
  ]
  node [
    id 226
    label "Maroko"
  ]
  node [
    id 227
    label "Bhutan"
  ]
  node [
    id 228
    label "S&#322;owacja"
  ]
  node [
    id 229
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 230
    label "Seszele"
  ]
  node [
    id 231
    label "Kuwejt"
  ]
  node [
    id 232
    label "Arabia_Saudyjska"
  ]
  node [
    id 233
    label "Ekwador"
  ]
  node [
    id 234
    label "Kanada"
  ]
  node [
    id 235
    label "Japonia"
  ]
  node [
    id 236
    label "ziemia"
  ]
  node [
    id 237
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 238
    label "Hiszpania"
  ]
  node [
    id 239
    label "Wyspy_Marshalla"
  ]
  node [
    id 240
    label "Botswana"
  ]
  node [
    id 241
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 242
    label "D&#380;ibuti"
  ]
  node [
    id 243
    label "grupa"
  ]
  node [
    id 244
    label "Wietnam"
  ]
  node [
    id 245
    label "Egipt"
  ]
  node [
    id 246
    label "Burkina_Faso"
  ]
  node [
    id 247
    label "Niemcy"
  ]
  node [
    id 248
    label "Khitai"
  ]
  node [
    id 249
    label "Macedonia"
  ]
  node [
    id 250
    label "Albania"
  ]
  node [
    id 251
    label "Madagaskar"
  ]
  node [
    id 252
    label "Bahrajn"
  ]
  node [
    id 253
    label "Jemen"
  ]
  node [
    id 254
    label "Lesoto"
  ]
  node [
    id 255
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 256
    label "Samoa"
  ]
  node [
    id 257
    label "Andora"
  ]
  node [
    id 258
    label "Chiny"
  ]
  node [
    id 259
    label "Cypr"
  ]
  node [
    id 260
    label "Wielka_Brytania"
  ]
  node [
    id 261
    label "Ukraina"
  ]
  node [
    id 262
    label "Paragwaj"
  ]
  node [
    id 263
    label "Trynidad_i_Tobago"
  ]
  node [
    id 264
    label "Libia"
  ]
  node [
    id 265
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 266
    label "Surinam"
  ]
  node [
    id 267
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 268
    label "Australia"
  ]
  node [
    id 269
    label "Nigeria"
  ]
  node [
    id 270
    label "Honduras"
  ]
  node [
    id 271
    label "Bangladesz"
  ]
  node [
    id 272
    label "Peru"
  ]
  node [
    id 273
    label "Kazachstan"
  ]
  node [
    id 274
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 275
    label "Irak"
  ]
  node [
    id 276
    label "holoarktyka"
  ]
  node [
    id 277
    label "USA"
  ]
  node [
    id 278
    label "Sudan"
  ]
  node [
    id 279
    label "Nepal"
  ]
  node [
    id 280
    label "San_Marino"
  ]
  node [
    id 281
    label "Burundi"
  ]
  node [
    id 282
    label "Dominikana"
  ]
  node [
    id 283
    label "Komory"
  ]
  node [
    id 284
    label "Gwatemala"
  ]
  node [
    id 285
    label "Antarktis"
  ]
  node [
    id 286
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 287
    label "Brunei"
  ]
  node [
    id 288
    label "Iran"
  ]
  node [
    id 289
    label "Zimbabwe"
  ]
  node [
    id 290
    label "Namibia"
  ]
  node [
    id 291
    label "Meksyk"
  ]
  node [
    id 292
    label "Kamerun"
  ]
  node [
    id 293
    label "zwrot"
  ]
  node [
    id 294
    label "Somalia"
  ]
  node [
    id 295
    label "Angola"
  ]
  node [
    id 296
    label "Gabon"
  ]
  node [
    id 297
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 298
    label "Mozambik"
  ]
  node [
    id 299
    label "Tajwan"
  ]
  node [
    id 300
    label "Tunezja"
  ]
  node [
    id 301
    label "Nowa_Zelandia"
  ]
  node [
    id 302
    label "Liban"
  ]
  node [
    id 303
    label "Jordania"
  ]
  node [
    id 304
    label "Tonga"
  ]
  node [
    id 305
    label "Czad"
  ]
  node [
    id 306
    label "Liberia"
  ]
  node [
    id 307
    label "Gwinea"
  ]
  node [
    id 308
    label "Belize"
  ]
  node [
    id 309
    label "&#321;otwa"
  ]
  node [
    id 310
    label "Syria"
  ]
  node [
    id 311
    label "Benin"
  ]
  node [
    id 312
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 313
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 314
    label "Dominika"
  ]
  node [
    id 315
    label "Antigua_i_Barbuda"
  ]
  node [
    id 316
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 317
    label "Hanower"
  ]
  node [
    id 318
    label "partia"
  ]
  node [
    id 319
    label "Afganistan"
  ]
  node [
    id 320
    label "Kiribati"
  ]
  node [
    id 321
    label "W&#322;ochy"
  ]
  node [
    id 322
    label "Szwajcaria"
  ]
  node [
    id 323
    label "Sahara_Zachodnia"
  ]
  node [
    id 324
    label "Chorwacja"
  ]
  node [
    id 325
    label "Tajlandia"
  ]
  node [
    id 326
    label "Salwador"
  ]
  node [
    id 327
    label "Bahamy"
  ]
  node [
    id 328
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 329
    label "S&#322;owenia"
  ]
  node [
    id 330
    label "Gambia"
  ]
  node [
    id 331
    label "Urugwaj"
  ]
  node [
    id 332
    label "Zair"
  ]
  node [
    id 333
    label "Erytrea"
  ]
  node [
    id 334
    label "Rosja"
  ]
  node [
    id 335
    label "Uganda"
  ]
  node [
    id 336
    label "Niger"
  ]
  node [
    id 337
    label "Mauritius"
  ]
  node [
    id 338
    label "Turkmenistan"
  ]
  node [
    id 339
    label "Turcja"
  ]
  node [
    id 340
    label "Irlandia"
  ]
  node [
    id 341
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 342
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 343
    label "Gwinea_Bissau"
  ]
  node [
    id 344
    label "Belgia"
  ]
  node [
    id 345
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 346
    label "Palau"
  ]
  node [
    id 347
    label "Barbados"
  ]
  node [
    id 348
    label "Chile"
  ]
  node [
    id 349
    label "Wenezuela"
  ]
  node [
    id 350
    label "W&#281;gry"
  ]
  node [
    id 351
    label "Argentyna"
  ]
  node [
    id 352
    label "Kolumbia"
  ]
  node [
    id 353
    label "Sierra_Leone"
  ]
  node [
    id 354
    label "Azerbejd&#380;an"
  ]
  node [
    id 355
    label "Kongo"
  ]
  node [
    id 356
    label "Pakistan"
  ]
  node [
    id 357
    label "Liechtenstein"
  ]
  node [
    id 358
    label "Nikaragua"
  ]
  node [
    id 359
    label "Senegal"
  ]
  node [
    id 360
    label "Indie"
  ]
  node [
    id 361
    label "Suazi"
  ]
  node [
    id 362
    label "Polska"
  ]
  node [
    id 363
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 364
    label "Algieria"
  ]
  node [
    id 365
    label "terytorium"
  ]
  node [
    id 366
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 367
    label "Jamajka"
  ]
  node [
    id 368
    label "Kostaryka"
  ]
  node [
    id 369
    label "Timor_Wschodni"
  ]
  node [
    id 370
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 371
    label "Kuba"
  ]
  node [
    id 372
    label "Mauretania"
  ]
  node [
    id 373
    label "Portoryko"
  ]
  node [
    id 374
    label "Brazylia"
  ]
  node [
    id 375
    label "Mo&#322;dawia"
  ]
  node [
    id 376
    label "organizacja"
  ]
  node [
    id 377
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 378
    label "Litwa"
  ]
  node [
    id 379
    label "Kirgistan"
  ]
  node [
    id 380
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 381
    label "Izrael"
  ]
  node [
    id 382
    label "Grecja"
  ]
  node [
    id 383
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 384
    label "Holandia"
  ]
  node [
    id 385
    label "Sri_Lanka"
  ]
  node [
    id 386
    label "Katar"
  ]
  node [
    id 387
    label "Mikronezja"
  ]
  node [
    id 388
    label "Mongolia"
  ]
  node [
    id 389
    label "Laos"
  ]
  node [
    id 390
    label "Malediwy"
  ]
  node [
    id 391
    label "Zambia"
  ]
  node [
    id 392
    label "Tanzania"
  ]
  node [
    id 393
    label "Gujana"
  ]
  node [
    id 394
    label "Czechy"
  ]
  node [
    id 395
    label "Panama"
  ]
  node [
    id 396
    label "Uzbekistan"
  ]
  node [
    id 397
    label "Gruzja"
  ]
  node [
    id 398
    label "Serbia"
  ]
  node [
    id 399
    label "Francja"
  ]
  node [
    id 400
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 401
    label "Togo"
  ]
  node [
    id 402
    label "Estonia"
  ]
  node [
    id 403
    label "Oman"
  ]
  node [
    id 404
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 405
    label "Portugalia"
  ]
  node [
    id 406
    label "Boliwia"
  ]
  node [
    id 407
    label "Luksemburg"
  ]
  node [
    id 408
    label "Haiti"
  ]
  node [
    id 409
    label "Wyspy_Salomona"
  ]
  node [
    id 410
    label "Birma"
  ]
  node [
    id 411
    label "Rodezja"
  ]
  node [
    id 412
    label "p&#322;acenie"
  ]
  node [
    id 413
    label "znaczenie"
  ]
  node [
    id 414
    label "sk&#322;adanie"
  ]
  node [
    id 415
    label "czynienie_dobra"
  ]
  node [
    id 416
    label "informowanie"
  ]
  node [
    id 417
    label "command"
  ]
  node [
    id 418
    label "opowiadanie"
  ]
  node [
    id 419
    label "koszt_rodzajowy"
  ]
  node [
    id 420
    label "pracowanie"
  ]
  node [
    id 421
    label "przekonywanie"
  ]
  node [
    id 422
    label "wyraz"
  ]
  node [
    id 423
    label "us&#322;uga"
  ]
  node [
    id 424
    label "performance"
  ]
  node [
    id 425
    label "zobowi&#261;zanie"
  ]
  node [
    id 426
    label "cz&#322;owiek_pracy"
  ]
  node [
    id 427
    label "delegowa&#263;"
  ]
  node [
    id 428
    label "s&#322;u&#380;ba_cywilna"
  ]
  node [
    id 429
    label "pracu&#347;"
  ]
  node [
    id 430
    label "delegowanie"
  ]
  node [
    id 431
    label "r&#281;ka"
  ]
  node [
    id 432
    label "infimum"
  ]
  node [
    id 433
    label "przyswoi&#263;"
  ]
  node [
    id 434
    label "ewoluowanie"
  ]
  node [
    id 435
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 436
    label "reakcja"
  ]
  node [
    id 437
    label "wyewoluowanie"
  ]
  node [
    id 438
    label "individual"
  ]
  node [
    id 439
    label "profanum"
  ]
  node [
    id 440
    label "starzenie_si&#281;"
  ]
  node [
    id 441
    label "homo_sapiens"
  ]
  node [
    id 442
    label "skala"
  ]
  node [
    id 443
    label "supremum"
  ]
  node [
    id 444
    label "przyswaja&#263;"
  ]
  node [
    id 445
    label "ludzko&#347;&#263;"
  ]
  node [
    id 446
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 447
    label "one"
  ]
  node [
    id 448
    label "funkcja"
  ]
  node [
    id 449
    label "przeliczenie"
  ]
  node [
    id 450
    label "przeliczanie"
  ]
  node [
    id 451
    label "mikrokosmos"
  ]
  node [
    id 452
    label "rzut"
  ]
  node [
    id 453
    label "portrecista"
  ]
  node [
    id 454
    label "przelicza&#263;"
  ]
  node [
    id 455
    label "przyswajanie"
  ]
  node [
    id 456
    label "duch"
  ]
  node [
    id 457
    label "wyewoluowa&#263;"
  ]
  node [
    id 458
    label "ewoluowa&#263;"
  ]
  node [
    id 459
    label "g&#322;owa"
  ]
  node [
    id 460
    label "oddzia&#322;ywanie"
  ]
  node [
    id 461
    label "liczba_naturalna"
  ]
  node [
    id 462
    label "osoba"
  ]
  node [
    id 463
    label "figura"
  ]
  node [
    id 464
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 465
    label "obiekt"
  ]
  node [
    id 466
    label "matematyka"
  ]
  node [
    id 467
    label "przyswojenie"
  ]
  node [
    id 468
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 469
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 470
    label "czynnik_biotyczny"
  ]
  node [
    id 471
    label "przeliczy&#263;"
  ]
  node [
    id 472
    label "antropochoria"
  ]
  node [
    id 473
    label "militarnie"
  ]
  node [
    id 474
    label "typowy"
  ]
  node [
    id 475
    label "antybalistyczny"
  ]
  node [
    id 476
    label "specjalny"
  ]
  node [
    id 477
    label "podleg&#322;y"
  ]
  node [
    id 478
    label "wojskowo"
  ]
  node [
    id 479
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 480
    label "work"
  ]
  node [
    id 481
    label "robi&#263;"
  ]
  node [
    id 482
    label "muzyka"
  ]
  node [
    id 483
    label "rola"
  ]
  node [
    id 484
    label "create"
  ]
  node [
    id 485
    label "yield"
  ]
  node [
    id 486
    label "czynno&#347;&#263;"
  ]
  node [
    id 487
    label "problem"
  ]
  node [
    id 488
    label "przepisanie"
  ]
  node [
    id 489
    label "przepisa&#263;"
  ]
  node [
    id 490
    label "nakarmienie"
  ]
  node [
    id 491
    label "duty"
  ]
  node [
    id 492
    label "zbi&#243;r"
  ]
  node [
    id 493
    label "powierzanie"
  ]
  node [
    id 494
    label "zaszkodzenie"
  ]
  node [
    id 495
    label "d&#378;wigni&#281;cie"
  ]
  node [
    id 496
    label "zaj&#281;cie"
  ]
  node [
    id 497
    label "spis"
  ]
  node [
    id 498
    label "sheet"
  ]
  node [
    id 499
    label "gazeta"
  ]
  node [
    id 500
    label "diariusz"
  ]
  node [
    id 501
    label "pami&#281;tnik"
  ]
  node [
    id 502
    label "journal"
  ]
  node [
    id 503
    label "ksi&#281;ga"
  ]
  node [
    id 504
    label "program_informacyjny"
  ]
  node [
    id 505
    label "umacnia&#263;"
  ]
  node [
    id 506
    label "zmienia&#263;"
  ]
  node [
    id 507
    label "arrange"
  ]
  node [
    id 508
    label "unwrap"
  ]
  node [
    id 509
    label "decydowa&#263;"
  ]
  node [
    id 510
    label "peddle"
  ]
  node [
    id 511
    label "powodowa&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 16
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 13
  ]
  edge [
    source 6
    target 10
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 20
  ]
  edge [
    source 7
    target 21
  ]
  edge [
    source 7
    target 10
  ]
  edge [
    source 7
    target 116
  ]
  edge [
    source 7
    target 117
  ]
  edge [
    source 7
    target 118
  ]
  edge [
    source 7
    target 119
  ]
  edge [
    source 7
    target 14
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 7
    target 28
  ]
  edge [
    source 7
    target 19
  ]
  edge [
    source 7
    target 16
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 8
    target 122
  ]
  edge [
    source 8
    target 123
  ]
  edge [
    source 8
    target 124
  ]
  edge [
    source 8
    target 125
  ]
  edge [
    source 8
    target 126
  ]
  edge [
    source 8
    target 127
  ]
  edge [
    source 8
    target 128
  ]
  edge [
    source 8
    target 129
  ]
  edge [
    source 8
    target 15
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 130
  ]
  edge [
    source 9
    target 131
  ]
  edge [
    source 9
    target 132
  ]
  edge [
    source 9
    target 133
  ]
  edge [
    source 9
    target 134
  ]
  edge [
    source 9
    target 135
  ]
  edge [
    source 9
    target 16
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 136
  ]
  edge [
    source 10
    target 137
  ]
  edge [
    source 10
    target 138
  ]
  edge [
    source 10
    target 139
  ]
  edge [
    source 10
    target 140
  ]
  edge [
    source 10
    target 141
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 22
  ]
  edge [
    source 11
    target 142
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 143
  ]
  edge [
    source 12
    target 144
  ]
  edge [
    source 12
    target 145
  ]
  edge [
    source 12
    target 146
  ]
  edge [
    source 12
    target 17
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 22
  ]
  edge [
    source 13
    target 19
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 147
  ]
  edge [
    source 14
    target 148
  ]
  edge [
    source 14
    target 149
  ]
  edge [
    source 14
    target 150
  ]
  edge [
    source 14
    target 151
  ]
  edge [
    source 14
    target 152
  ]
  edge [
    source 14
    target 153
  ]
  edge [
    source 14
    target 154
  ]
  edge [
    source 14
    target 155
  ]
  edge [
    source 14
    target 156
  ]
  edge [
    source 14
    target 157
  ]
  edge [
    source 14
    target 158
  ]
  edge [
    source 14
    target 159
  ]
  edge [
    source 14
    target 160
  ]
  edge [
    source 14
    target 161
  ]
  edge [
    source 14
    target 162
  ]
  edge [
    source 14
    target 163
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 164
  ]
  edge [
    source 15
    target 165
  ]
  edge [
    source 15
    target 166
  ]
  edge [
    source 15
    target 167
  ]
  edge [
    source 15
    target 168
  ]
  edge [
    source 15
    target 169
  ]
  edge [
    source 15
    target 170
  ]
  edge [
    source 15
    target 171
  ]
  edge [
    source 15
    target 23
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 24
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 172
  ]
  edge [
    source 17
    target 173
  ]
  edge [
    source 17
    target 174
  ]
  edge [
    source 17
    target 175
  ]
  edge [
    source 17
    target 176
  ]
  edge [
    source 17
    target 177
  ]
  edge [
    source 17
    target 178
  ]
  edge [
    source 17
    target 179
  ]
  edge [
    source 17
    target 180
  ]
  edge [
    source 17
    target 25
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 28
  ]
  edge [
    source 18
    target 181
  ]
  edge [
    source 18
    target 182
  ]
  edge [
    source 18
    target 183
  ]
  edge [
    source 18
    target 184
  ]
  edge [
    source 18
    target 185
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 186
  ]
  edge [
    source 19
    target 187
  ]
  edge [
    source 19
    target 188
  ]
  edge [
    source 19
    target 189
  ]
  edge [
    source 19
    target 190
  ]
  edge [
    source 19
    target 191
  ]
  edge [
    source 19
    target 192
  ]
  edge [
    source 19
    target 193
  ]
  edge [
    source 19
    target 194
  ]
  edge [
    source 19
    target 195
  ]
  edge [
    source 19
    target 196
  ]
  edge [
    source 19
    target 197
  ]
  edge [
    source 19
    target 26
  ]
  edge [
    source 20
    target 23
  ]
  edge [
    source 20
    target 29
  ]
  edge [
    source 20
    target 198
  ]
  edge [
    source 20
    target 199
  ]
  edge [
    source 20
    target 200
  ]
  edge [
    source 20
    target 201
  ]
  edge [
    source 20
    target 202
  ]
  edge [
    source 20
    target 203
  ]
  edge [
    source 20
    target 204
  ]
  edge [
    source 20
    target 205
  ]
  edge [
    source 20
    target 206
  ]
  edge [
    source 20
    target 207
  ]
  edge [
    source 20
    target 208
  ]
  edge [
    source 20
    target 209
  ]
  edge [
    source 20
    target 210
  ]
  edge [
    source 20
    target 211
  ]
  edge [
    source 20
    target 212
  ]
  edge [
    source 20
    target 213
  ]
  edge [
    source 20
    target 214
  ]
  edge [
    source 20
    target 215
  ]
  edge [
    source 20
    target 216
  ]
  edge [
    source 20
    target 217
  ]
  edge [
    source 20
    target 218
  ]
  edge [
    source 20
    target 219
  ]
  edge [
    source 20
    target 220
  ]
  edge [
    source 20
    target 221
  ]
  edge [
    source 20
    target 222
  ]
  edge [
    source 20
    target 223
  ]
  edge [
    source 20
    target 224
  ]
  edge [
    source 20
    target 225
  ]
  edge [
    source 20
    target 226
  ]
  edge [
    source 20
    target 227
  ]
  edge [
    source 20
    target 228
  ]
  edge [
    source 20
    target 229
  ]
  edge [
    source 20
    target 230
  ]
  edge [
    source 20
    target 231
  ]
  edge [
    source 20
    target 232
  ]
  edge [
    source 20
    target 233
  ]
  edge [
    source 20
    target 234
  ]
  edge [
    source 20
    target 235
  ]
  edge [
    source 20
    target 236
  ]
  edge [
    source 20
    target 237
  ]
  edge [
    source 20
    target 238
  ]
  edge [
    source 20
    target 239
  ]
  edge [
    source 20
    target 240
  ]
  edge [
    source 20
    target 241
  ]
  edge [
    source 20
    target 242
  ]
  edge [
    source 20
    target 243
  ]
  edge [
    source 20
    target 244
  ]
  edge [
    source 20
    target 245
  ]
  edge [
    source 20
    target 246
  ]
  edge [
    source 20
    target 247
  ]
  edge [
    source 20
    target 248
  ]
  edge [
    source 20
    target 249
  ]
  edge [
    source 20
    target 250
  ]
  edge [
    source 20
    target 251
  ]
  edge [
    source 20
    target 252
  ]
  edge [
    source 20
    target 253
  ]
  edge [
    source 20
    target 254
  ]
  edge [
    source 20
    target 255
  ]
  edge [
    source 20
    target 256
  ]
  edge [
    source 20
    target 257
  ]
  edge [
    source 20
    target 258
  ]
  edge [
    source 20
    target 259
  ]
  edge [
    source 20
    target 260
  ]
  edge [
    source 20
    target 261
  ]
  edge [
    source 20
    target 262
  ]
  edge [
    source 20
    target 263
  ]
  edge [
    source 20
    target 264
  ]
  edge [
    source 20
    target 265
  ]
  edge [
    source 20
    target 266
  ]
  edge [
    source 20
    target 267
  ]
  edge [
    source 20
    target 268
  ]
  edge [
    source 20
    target 269
  ]
  edge [
    source 20
    target 270
  ]
  edge [
    source 20
    target 271
  ]
  edge [
    source 20
    target 272
  ]
  edge [
    source 20
    target 273
  ]
  edge [
    source 20
    target 274
  ]
  edge [
    source 20
    target 275
  ]
  edge [
    source 20
    target 276
  ]
  edge [
    source 20
    target 277
  ]
  edge [
    source 20
    target 278
  ]
  edge [
    source 20
    target 279
  ]
  edge [
    source 20
    target 280
  ]
  edge [
    source 20
    target 281
  ]
  edge [
    source 20
    target 282
  ]
  edge [
    source 20
    target 283
  ]
  edge [
    source 20
    target 191
  ]
  edge [
    source 20
    target 284
  ]
  edge [
    source 20
    target 285
  ]
  edge [
    source 20
    target 286
  ]
  edge [
    source 20
    target 287
  ]
  edge [
    source 20
    target 288
  ]
  edge [
    source 20
    target 289
  ]
  edge [
    source 20
    target 290
  ]
  edge [
    source 20
    target 291
  ]
  edge [
    source 20
    target 292
  ]
  edge [
    source 20
    target 293
  ]
  edge [
    source 20
    target 294
  ]
  edge [
    source 20
    target 295
  ]
  edge [
    source 20
    target 296
  ]
  edge [
    source 20
    target 297
  ]
  edge [
    source 20
    target 298
  ]
  edge [
    source 20
    target 299
  ]
  edge [
    source 20
    target 300
  ]
  edge [
    source 20
    target 301
  ]
  edge [
    source 20
    target 302
  ]
  edge [
    source 20
    target 303
  ]
  edge [
    source 20
    target 304
  ]
  edge [
    source 20
    target 305
  ]
  edge [
    source 20
    target 306
  ]
  edge [
    source 20
    target 307
  ]
  edge [
    source 20
    target 308
  ]
  edge [
    source 20
    target 309
  ]
  edge [
    source 20
    target 310
  ]
  edge [
    source 20
    target 311
  ]
  edge [
    source 20
    target 312
  ]
  edge [
    source 20
    target 313
  ]
  edge [
    source 20
    target 314
  ]
  edge [
    source 20
    target 315
  ]
  edge [
    source 20
    target 316
  ]
  edge [
    source 20
    target 317
  ]
  edge [
    source 20
    target 318
  ]
  edge [
    source 20
    target 319
  ]
  edge [
    source 20
    target 320
  ]
  edge [
    source 20
    target 321
  ]
  edge [
    source 20
    target 322
  ]
  edge [
    source 20
    target 323
  ]
  edge [
    source 20
    target 324
  ]
  edge [
    source 20
    target 325
  ]
  edge [
    source 20
    target 326
  ]
  edge [
    source 20
    target 327
  ]
  edge [
    source 20
    target 328
  ]
  edge [
    source 20
    target 329
  ]
  edge [
    source 20
    target 330
  ]
  edge [
    source 20
    target 331
  ]
  edge [
    source 20
    target 332
  ]
  edge [
    source 20
    target 333
  ]
  edge [
    source 20
    target 334
  ]
  edge [
    source 20
    target 335
  ]
  edge [
    source 20
    target 336
  ]
  edge [
    source 20
    target 337
  ]
  edge [
    source 20
    target 338
  ]
  edge [
    source 20
    target 339
  ]
  edge [
    source 20
    target 340
  ]
  edge [
    source 20
    target 341
  ]
  edge [
    source 20
    target 342
  ]
  edge [
    source 20
    target 343
  ]
  edge [
    source 20
    target 344
  ]
  edge [
    source 20
    target 345
  ]
  edge [
    source 20
    target 346
  ]
  edge [
    source 20
    target 347
  ]
  edge [
    source 20
    target 348
  ]
  edge [
    source 20
    target 349
  ]
  edge [
    source 20
    target 350
  ]
  edge [
    source 20
    target 351
  ]
  edge [
    source 20
    target 352
  ]
  edge [
    source 20
    target 353
  ]
  edge [
    source 20
    target 354
  ]
  edge [
    source 20
    target 355
  ]
  edge [
    source 20
    target 356
  ]
  edge [
    source 20
    target 357
  ]
  edge [
    source 20
    target 358
  ]
  edge [
    source 20
    target 359
  ]
  edge [
    source 20
    target 360
  ]
  edge [
    source 20
    target 361
  ]
  edge [
    source 20
    target 362
  ]
  edge [
    source 20
    target 363
  ]
  edge [
    source 20
    target 364
  ]
  edge [
    source 20
    target 365
  ]
  edge [
    source 20
    target 366
  ]
  edge [
    source 20
    target 367
  ]
  edge [
    source 20
    target 368
  ]
  edge [
    source 20
    target 369
  ]
  edge [
    source 20
    target 370
  ]
  edge [
    source 20
    target 371
  ]
  edge [
    source 20
    target 372
  ]
  edge [
    source 20
    target 373
  ]
  edge [
    source 20
    target 374
  ]
  edge [
    source 20
    target 375
  ]
  edge [
    source 20
    target 376
  ]
  edge [
    source 20
    target 377
  ]
  edge [
    source 20
    target 378
  ]
  edge [
    source 20
    target 379
  ]
  edge [
    source 20
    target 380
  ]
  edge [
    source 20
    target 381
  ]
  edge [
    source 20
    target 382
  ]
  edge [
    source 20
    target 383
  ]
  edge [
    source 20
    target 384
  ]
  edge [
    source 20
    target 385
  ]
  edge [
    source 20
    target 386
  ]
  edge [
    source 20
    target 387
  ]
  edge [
    source 20
    target 388
  ]
  edge [
    source 20
    target 389
  ]
  edge [
    source 20
    target 390
  ]
  edge [
    source 20
    target 391
  ]
  edge [
    source 20
    target 392
  ]
  edge [
    source 20
    target 393
  ]
  edge [
    source 20
    target 394
  ]
  edge [
    source 20
    target 395
  ]
  edge [
    source 20
    target 396
  ]
  edge [
    source 20
    target 397
  ]
  edge [
    source 20
    target 398
  ]
  edge [
    source 20
    target 399
  ]
  edge [
    source 20
    target 400
  ]
  edge [
    source 20
    target 401
  ]
  edge [
    source 20
    target 402
  ]
  edge [
    source 20
    target 403
  ]
  edge [
    source 20
    target 404
  ]
  edge [
    source 20
    target 405
  ]
  edge [
    source 20
    target 406
  ]
  edge [
    source 20
    target 407
  ]
  edge [
    source 20
    target 408
  ]
  edge [
    source 20
    target 409
  ]
  edge [
    source 20
    target 410
  ]
  edge [
    source 20
    target 411
  ]
  edge [
    source 20
    target 27
  ]
  edge [
    source 21
    target 29
  ]
  edge [
    source 21
    target 30
  ]
  edge [
    source 22
    target 412
  ]
  edge [
    source 22
    target 413
  ]
  edge [
    source 22
    target 414
  ]
  edge [
    source 22
    target 172
  ]
  edge [
    source 22
    target 415
  ]
  edge [
    source 22
    target 416
  ]
  edge [
    source 22
    target 417
  ]
  edge [
    source 22
    target 418
  ]
  edge [
    source 22
    target 419
  ]
  edge [
    source 22
    target 420
  ]
  edge [
    source 22
    target 421
  ]
  edge [
    source 22
    target 422
  ]
  edge [
    source 22
    target 423
  ]
  edge [
    source 22
    target 424
  ]
  edge [
    source 22
    target 425
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 426
  ]
  edge [
    source 23
    target 147
  ]
  edge [
    source 23
    target 427
  ]
  edge [
    source 23
    target 428
  ]
  edge [
    source 23
    target 429
  ]
  edge [
    source 23
    target 430
  ]
  edge [
    source 23
    target 431
  ]
  edge [
    source 23
    target 135
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 432
  ]
  edge [
    source 25
    target 433
  ]
  edge [
    source 25
    target 434
  ]
  edge [
    source 25
    target 435
  ]
  edge [
    source 25
    target 436
  ]
  edge [
    source 25
    target 437
  ]
  edge [
    source 25
    target 438
  ]
  edge [
    source 25
    target 439
  ]
  edge [
    source 25
    target 440
  ]
  edge [
    source 25
    target 441
  ]
  edge [
    source 25
    target 442
  ]
  edge [
    source 25
    target 443
  ]
  edge [
    source 25
    target 444
  ]
  edge [
    source 25
    target 445
  ]
  edge [
    source 25
    target 446
  ]
  edge [
    source 25
    target 447
  ]
  edge [
    source 25
    target 448
  ]
  edge [
    source 25
    target 449
  ]
  edge [
    source 25
    target 450
  ]
  edge [
    source 25
    target 451
  ]
  edge [
    source 25
    target 452
  ]
  edge [
    source 25
    target 453
  ]
  edge [
    source 25
    target 454
  ]
  edge [
    source 25
    target 455
  ]
  edge [
    source 25
    target 456
  ]
  edge [
    source 25
    target 457
  ]
  edge [
    source 25
    target 458
  ]
  edge [
    source 25
    target 459
  ]
  edge [
    source 25
    target 460
  ]
  edge [
    source 25
    target 461
  ]
  edge [
    source 25
    target 196
  ]
  edge [
    source 25
    target 462
  ]
  edge [
    source 25
    target 463
  ]
  edge [
    source 25
    target 464
  ]
  edge [
    source 25
    target 465
  ]
  edge [
    source 25
    target 466
  ]
  edge [
    source 25
    target 467
  ]
  edge [
    source 25
    target 468
  ]
  edge [
    source 25
    target 469
  ]
  edge [
    source 25
    target 470
  ]
  edge [
    source 25
    target 471
  ]
  edge [
    source 25
    target 472
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 147
  ]
  edge [
    source 26
    target 149
  ]
  edge [
    source 26
    target 156
  ]
  edge [
    source 26
    target 473
  ]
  edge [
    source 26
    target 158
  ]
  edge [
    source 26
    target 163
  ]
  edge [
    source 26
    target 150
  ]
  edge [
    source 26
    target 151
  ]
  edge [
    source 26
    target 152
  ]
  edge [
    source 26
    target 154
  ]
  edge [
    source 26
    target 157
  ]
  edge [
    source 26
    target 161
  ]
  edge [
    source 26
    target 162
  ]
  edge [
    source 26
    target 474
  ]
  edge [
    source 26
    target 475
  ]
  edge [
    source 26
    target 476
  ]
  edge [
    source 26
    target 477
  ]
  edge [
    source 26
    target 153
  ]
  edge [
    source 26
    target 155
  ]
  edge [
    source 26
    target 159
  ]
  edge [
    source 26
    target 160
  ]
  edge [
    source 26
    target 478
  ]
  edge [
    source 26
    target 148
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 479
  ]
  edge [
    source 27
    target 480
  ]
  edge [
    source 27
    target 481
  ]
  edge [
    source 27
    target 482
  ]
  edge [
    source 27
    target 483
  ]
  edge [
    source 27
    target 484
  ]
  edge [
    source 27
    target 143
  ]
  edge [
    source 27
    target 179
  ]
  edge [
    source 28
    target 485
  ]
  edge [
    source 28
    target 486
  ]
  edge [
    source 28
    target 487
  ]
  edge [
    source 28
    target 488
  ]
  edge [
    source 28
    target 489
  ]
  edge [
    source 28
    target 35
  ]
  edge [
    source 28
    target 480
  ]
  edge [
    source 28
    target 490
  ]
  edge [
    source 28
    target 491
  ]
  edge [
    source 28
    target 492
  ]
  edge [
    source 28
    target 493
  ]
  edge [
    source 28
    target 494
  ]
  edge [
    source 28
    target 495
  ]
  edge [
    source 28
    target 496
  ]
  edge [
    source 28
    target 425
  ]
  edge [
    source 29
    target 497
  ]
  edge [
    source 29
    target 498
  ]
  edge [
    source 29
    target 499
  ]
  edge [
    source 29
    target 500
  ]
  edge [
    source 29
    target 501
  ]
  edge [
    source 29
    target 502
  ]
  edge [
    source 29
    target 503
  ]
  edge [
    source 29
    target 504
  ]
  edge [
    source 30
    target 505
  ]
  edge [
    source 30
    target 506
  ]
  edge [
    source 30
    target 507
  ]
  edge [
    source 30
    target 481
  ]
  edge [
    source 30
    target 508
  ]
  edge [
    source 30
    target 509
  ]
  edge [
    source 30
    target 510
  ]
  edge [
    source 30
    target 511
  ]
]
