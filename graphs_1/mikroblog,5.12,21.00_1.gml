graph [
  maxDegree 23
  minDegree 1
  meanDegree 1.967741935483871
  density 0.03225806451612903
  graphCliqueNumber 2
  node [
    id 0
    label "takaprawda"
    origin "text"
  ]
  node [
    id 1
    label "heheszki"
    origin "text"
  ]
  node [
    id 2
    label "humorobrazkowy"
    origin "text"
  ]
  node [
    id 3
    label "polski"
    origin "text"
  ]
  node [
    id 4
    label "edukacja"
    origin "text"
  ]
  node [
    id 5
    label "kurde"
    origin "text"
  ]
  node [
    id 6
    label "znowu"
    origin "text"
  ]
  node [
    id 7
    label "k&#380;yrzakuw"
    origin "text"
  ]
  node [
    id 8
    label "prukwa"
    origin "text"
  ]
  node [
    id 9
    label "&#322;ikend"
    origin "text"
  ]
  node [
    id 10
    label "zada&#263;"
    origin "text"
  ]
  node [
    id 11
    label "lacki"
  ]
  node [
    id 12
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 13
    label "przedmiot"
  ]
  node [
    id 14
    label "sztajer"
  ]
  node [
    id 15
    label "drabant"
  ]
  node [
    id 16
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 17
    label "polak"
  ]
  node [
    id 18
    label "pierogi_ruskie"
  ]
  node [
    id 19
    label "krakowiak"
  ]
  node [
    id 20
    label "Polish"
  ]
  node [
    id 21
    label "j&#281;zyk"
  ]
  node [
    id 22
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 23
    label "oberek"
  ]
  node [
    id 24
    label "po_polsku"
  ]
  node [
    id 25
    label "mazur"
  ]
  node [
    id 26
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 27
    label "chodzony"
  ]
  node [
    id 28
    label "skoczny"
  ]
  node [
    id 29
    label "ryba_po_grecku"
  ]
  node [
    id 30
    label "goniony"
  ]
  node [
    id 31
    label "polsko"
  ]
  node [
    id 32
    label "kwalifikacje"
  ]
  node [
    id 33
    label "Karta_Nauczyciela"
  ]
  node [
    id 34
    label "szkolnictwo"
  ]
  node [
    id 35
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 36
    label "program_nauczania"
  ]
  node [
    id 37
    label "formation"
  ]
  node [
    id 38
    label "miasteczko_rowerowe"
  ]
  node [
    id 39
    label "gospodarka"
  ]
  node [
    id 40
    label "urszulanki"
  ]
  node [
    id 41
    label "wiedza"
  ]
  node [
    id 42
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 43
    label "skolaryzacja"
  ]
  node [
    id 44
    label "proces"
  ]
  node [
    id 45
    label "niepokalanki"
  ]
  node [
    id 46
    label "heureza"
  ]
  node [
    id 47
    label "form"
  ]
  node [
    id 48
    label "nauka"
  ]
  node [
    id 49
    label "&#322;awa_szkolna"
  ]
  node [
    id 50
    label "kurwa"
  ]
  node [
    id 51
    label "babsztyl"
  ]
  node [
    id 52
    label "d&#378;wign&#261;&#263;"
  ]
  node [
    id 53
    label "zaj&#261;&#263;"
  ]
  node [
    id 54
    label "zaszkodzi&#263;"
  ]
  node [
    id 55
    label "distribute"
  ]
  node [
    id 56
    label "za&#322;o&#380;y&#263;"
  ]
  node [
    id 57
    label "nakarmi&#263;"
  ]
  node [
    id 58
    label "deal"
  ]
  node [
    id 59
    label "put"
  ]
  node [
    id 60
    label "set"
  ]
  node [
    id 61
    label "zobowi&#261;za&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 52
  ]
  edge [
    source 10
    target 53
  ]
  edge [
    source 10
    target 54
  ]
  edge [
    source 10
    target 55
  ]
  edge [
    source 10
    target 56
  ]
  edge [
    source 10
    target 57
  ]
  edge [
    source 10
    target 58
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 10
    target 60
  ]
  edge [
    source 10
    target 61
  ]
]
