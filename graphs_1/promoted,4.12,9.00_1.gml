graph [
  maxDegree 27
  minDegree 1
  meanDegree 1.95
  density 0.05
  graphCliqueNumber 3
  node [
    id 0
    label "dlaczego"
    origin "text"
  ]
  node [
    id 1
    label "nic"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "miernota"
  ]
  node [
    id 5
    label "g&#243;wno"
  ]
  node [
    id 6
    label "love"
  ]
  node [
    id 7
    label "ilo&#347;&#263;"
  ]
  node [
    id 8
    label "brak"
  ]
  node [
    id 9
    label "ciura"
  ]
  node [
    id 10
    label "remark"
  ]
  node [
    id 11
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 12
    label "u&#380;ywa&#263;"
  ]
  node [
    id 13
    label "okre&#347;la&#263;"
  ]
  node [
    id 14
    label "j&#281;zyk"
  ]
  node [
    id 15
    label "say"
  ]
  node [
    id 16
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 17
    label "formu&#322;owa&#263;"
  ]
  node [
    id 18
    label "talk"
  ]
  node [
    id 19
    label "powiada&#263;"
  ]
  node [
    id 20
    label "informowa&#263;"
  ]
  node [
    id 21
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 22
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 23
    label "wydobywa&#263;"
  ]
  node [
    id 24
    label "express"
  ]
  node [
    id 25
    label "chew_the_fat"
  ]
  node [
    id 26
    label "dysfonia"
  ]
  node [
    id 27
    label "umie&#263;"
  ]
  node [
    id 28
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 29
    label "tell"
  ]
  node [
    id 30
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 31
    label "wyra&#380;a&#263;"
  ]
  node [
    id 32
    label "gaworzy&#263;"
  ]
  node [
    id 33
    label "rozmawia&#263;"
  ]
  node [
    id 34
    label "dziama&#263;"
  ]
  node [
    id 35
    label "prawi&#263;"
  ]
  node [
    id 36
    label "instytut"
  ]
  node [
    id 37
    label "polski"
  ]
  node [
    id 38
    label "festiwal"
  ]
  node [
    id 39
    label "kultura"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 39
  ]
  edge [
    source 38
    target 39
  ]
]
