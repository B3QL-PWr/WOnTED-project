graph [
  maxDegree 17
  minDegree 1
  meanDegree 1.9428571428571428
  density 0.05714285714285714
  graphCliqueNumber 2
  node [
    id 0
    label "&#347;rednio"
    origin "text"
  ]
  node [
    id 1
    label "ujmowa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "odkrycie"
    origin "text"
  ]
  node [
    id 3
    label "orientacyjnie"
  ]
  node [
    id 4
    label "&#347;redni"
  ]
  node [
    id 5
    label "ma&#322;o"
  ]
  node [
    id 6
    label "bezbarwnie"
  ]
  node [
    id 7
    label "tak_sobie"
  ]
  node [
    id 8
    label "chwyta&#263;"
  ]
  node [
    id 9
    label "allude"
  ]
  node [
    id 10
    label "zabiera&#263;"
  ]
  node [
    id 11
    label "wzbudza&#263;"
  ]
  node [
    id 12
    label "zamyka&#263;"
  ]
  node [
    id 13
    label "reduce"
  ]
  node [
    id 14
    label "komunikowa&#263;"
  ]
  node [
    id 15
    label "convey"
  ]
  node [
    id 16
    label "&#322;apa&#263;"
  ]
  node [
    id 17
    label "zgarnia&#263;"
  ]
  node [
    id 18
    label "raise"
  ]
  node [
    id 19
    label "objawienie"
  ]
  node [
    id 20
    label "jawny"
  ]
  node [
    id 21
    label "zsuni&#281;cie"
  ]
  node [
    id 22
    label "znalezienie"
  ]
  node [
    id 23
    label "poinformowanie"
  ]
  node [
    id 24
    label "novum"
  ]
  node [
    id 25
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 26
    label "zwr&#243;cenie_uwagi"
  ]
  node [
    id 27
    label "ukazanie"
  ]
  node [
    id 28
    label "disclosure"
  ]
  node [
    id 29
    label "ods&#322;oni&#281;cie_si&#281;"
  ]
  node [
    id 30
    label "poznanie"
  ]
  node [
    id 31
    label "podniesienie"
  ]
  node [
    id 32
    label "detection"
  ]
  node [
    id 33
    label "discovery"
  ]
  node [
    id 34
    label "niespodzianka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
]
