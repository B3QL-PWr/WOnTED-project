graph [
  maxDegree 22
  minDegree 1
  meanDegree 2.1
  density 0.05384615384615385
  graphCliqueNumber 3
  node [
    id 0
    label "by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "milion"
    origin "text"
  ]
  node [
    id 2
    label "og&#243;&#322;"
    origin "text"
  ]
  node [
    id 3
    label "ludno&#347;&#263;"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;ga&#263;"
  ]
  node [
    id 5
    label "trwa&#263;"
  ]
  node [
    id 6
    label "obecno&#347;&#263;"
  ]
  node [
    id 7
    label "stan"
  ]
  node [
    id 8
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 9
    label "stand"
  ]
  node [
    id 10
    label "mie&#263;_miejsce"
  ]
  node [
    id 11
    label "uczestniczy&#263;"
  ]
  node [
    id 12
    label "chodzi&#263;"
  ]
  node [
    id 13
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 14
    label "equal"
  ]
  node [
    id 15
    label "liczba"
  ]
  node [
    id 16
    label "miljon"
  ]
  node [
    id 17
    label "ba&#324;ka"
  ]
  node [
    id 18
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 19
    label "pole"
  ]
  node [
    id 20
    label "kastowo&#347;&#263;"
  ]
  node [
    id 21
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 22
    label "ludzie_pracy"
  ]
  node [
    id 23
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 24
    label "community"
  ]
  node [
    id 25
    label "Fremeni"
  ]
  node [
    id 26
    label "status"
  ]
  node [
    id 27
    label "pozaklasowy"
  ]
  node [
    id 28
    label "aspo&#322;eczny"
  ]
  node [
    id 29
    label "ilo&#347;&#263;"
  ]
  node [
    id 30
    label "pe&#322;ny"
  ]
  node [
    id 31
    label "uwarstwienie"
  ]
  node [
    id 32
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 33
    label "zlewanie_si&#281;"
  ]
  node [
    id 34
    label "elita"
  ]
  node [
    id 35
    label "cywilizacja"
  ]
  node [
    id 36
    label "wspo&#322;ecze&#324;stwo"
  ]
  node [
    id 37
    label "klasa"
  ]
  node [
    id 38
    label "ch&#322;opstwo"
  ]
  node [
    id 39
    label "innowierstwo"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
]
