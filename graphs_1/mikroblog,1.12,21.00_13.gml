graph [
  maxDegree 10
  minDegree 1
  meanDegree 1.6923076923076923
  density 0.14102564102564102
  graphCliqueNumber 2
  node [
    id 0
    label "heheszki"
    origin "text"
  ]
  node [
    id 1
    label "by&#322;y"
    origin "text"
  ]
  node [
    id 2
    label "dawny"
  ]
  node [
    id 3
    label "rozw&#243;d"
  ]
  node [
    id 4
    label "rozwodzi&#263;_si&#281;"
  ]
  node [
    id 5
    label "eksprezydent"
  ]
  node [
    id 6
    label "rozwie&#347;&#263;_si&#281;"
  ]
  node [
    id 7
    label "partner"
  ]
  node [
    id 8
    label "rozwiedzenie_si&#281;"
  ]
  node [
    id 9
    label "rozwodzenie_si&#281;"
  ]
  node [
    id 10
    label "wcze&#347;niejszy"
  ]
  node [
    id 11
    label "Newa"
  ]
  node [
    id 12
    label "Dehli"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 11
    target 12
  ]
]
