graph [
  maxDegree 30
  minDegree 1
  meanDegree 2.1589958158995817
  density 0.009071410991174712
  graphCliqueNumber 6
  node [
    id 0
    label "europejski"
    origin "text"
  ]
  node [
    id 1
    label "oddzia&#322;"
    origin "text"
  ]
  node [
    id 2
    label "sparc"
    origin "text"
  ]
  node [
    id 3
    label "scholarly"
    origin "text"
  ]
  node [
    id 4
    label "publishing"
    origin "text"
  ]
  node [
    id 5
    label "anda"
    origin "text"
  ]
  node [
    id 6
    label "academic"
    origin "text"
  ]
  node [
    id 7
    label "resources"
    origin "text"
  ]
  node [
    id 8
    label "coalition"
    origin "text"
  ]
  node [
    id 9
    label "wsparcie"
    origin "text"
  ]
  node [
    id 10
    label "szereg"
    origin "text"
  ]
  node [
    id 11
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 12
    label "organizacja"
    origin "text"
  ]
  node [
    id 13
    label "wspiera&#263;"
    origin "text"
  ]
  node [
    id 14
    label "badanie"
    origin "text"
  ]
  node [
    id 15
    label "naukowy"
    origin "text"
  ]
  node [
    id 16
    label "dwa"
    origin "text"
  ]
  node [
    id 17
    label "tydzie&#324;"
    origin "text"
  ]
  node [
    id 18
    label "zbiera&#263;"
    origin "text"
  ]
  node [
    id 19
    label "podpis"
    origin "text"
  ]
  node [
    id 20
    label "pod"
    origin "text"
  ]
  node [
    id 21
    label "petycja"
    origin "text"
  ]
  node [
    id 22
    label "wzywa&#263;"
    origin "text"
  ]
  node [
    id 23
    label "unia"
    origin "text"
  ]
  node [
    id 24
    label "zapewnienie"
    origin "text"
  ]
  node [
    id 25
    label "publiczny"
    origin "text"
  ]
  node [
    id 26
    label "dost&#281;p"
    origin "text"
  ]
  node [
    id 27
    label "wynik"
    origin "text"
  ]
  node [
    id 28
    label "finansowa&#263;"
    origin "text"
  ]
  node [
    id 29
    label "publicznie"
    origin "text"
  ]
  node [
    id 30
    label "European"
  ]
  node [
    id 31
    label "po_europejsku"
  ]
  node [
    id 32
    label "charakterystyczny"
  ]
  node [
    id 33
    label "europejsko"
  ]
  node [
    id 34
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 35
    label "typowy"
  ]
  node [
    id 36
    label "whole"
  ]
  node [
    id 37
    label "jednostka"
  ]
  node [
    id 38
    label "jednostka_geologiczna"
  ]
  node [
    id 39
    label "system"
  ]
  node [
    id 40
    label "poziom"
  ]
  node [
    id 41
    label "agencja"
  ]
  node [
    id 42
    label "dogger"
  ]
  node [
    id 43
    label "formacja"
  ]
  node [
    id 44
    label "pi&#281;tro"
  ]
  node [
    id 45
    label "filia"
  ]
  node [
    id 46
    label "dzia&#322;"
  ]
  node [
    id 47
    label "promocja"
  ]
  node [
    id 48
    label "zesp&#243;&#322;"
  ]
  node [
    id 49
    label "kurs"
  ]
  node [
    id 50
    label "przegrupowywa&#263;_si&#281;"
  ]
  node [
    id 51
    label "wojsko"
  ]
  node [
    id 52
    label "siedziba"
  ]
  node [
    id 53
    label "bank"
  ]
  node [
    id 54
    label "lias"
  ]
  node [
    id 55
    label "szpital"
  ]
  node [
    id 56
    label "przegrupowywanie_si&#281;"
  ]
  node [
    id 57
    label "malm"
  ]
  node [
    id 58
    label "ajencja"
  ]
  node [
    id 59
    label "klasa"
  ]
  node [
    id 60
    label "comfort"
  ]
  node [
    id 61
    label "u&#322;atwienie"
  ]
  node [
    id 62
    label "doch&#243;d"
  ]
  node [
    id 63
    label "oparcie"
  ]
  node [
    id 64
    label "telefon_zaufania"
  ]
  node [
    id 65
    label "dar"
  ]
  node [
    id 66
    label "zapomoga"
  ]
  node [
    id 67
    label "pocieszenie"
  ]
  node [
    id 68
    label "darowizna"
  ]
  node [
    id 69
    label "pomoc"
  ]
  node [
    id 70
    label "&#347;rodek"
  ]
  node [
    id 71
    label "wyci&#261;gni&#281;cie_pomocnej_d&#322;oni"
  ]
  node [
    id 72
    label "support"
  ]
  node [
    id 73
    label "koniec"
  ]
  node [
    id 74
    label "unit"
  ]
  node [
    id 75
    label "uporz&#261;dkowanie"
  ]
  node [
    id 76
    label "szpaler"
  ]
  node [
    id 77
    label "tract"
  ]
  node [
    id 78
    label "wyra&#380;enie"
  ]
  node [
    id 79
    label "zbi&#243;r"
  ]
  node [
    id 80
    label "mn&#243;stwo"
  ]
  node [
    id 81
    label "rozmieszczenie"
  ]
  node [
    id 82
    label "column"
  ]
  node [
    id 83
    label "doros&#322;y"
  ]
  node [
    id 84
    label "wiele"
  ]
  node [
    id 85
    label "dorodny"
  ]
  node [
    id 86
    label "znaczny"
  ]
  node [
    id 87
    label "du&#380;o"
  ]
  node [
    id 88
    label "prawdziwy"
  ]
  node [
    id 89
    label "niema&#322;o"
  ]
  node [
    id 90
    label "wa&#380;ny"
  ]
  node [
    id 91
    label "rozwini&#281;ty"
  ]
  node [
    id 92
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 93
    label "endecki"
  ]
  node [
    id 94
    label "komitet_koordynacyjny"
  ]
  node [
    id 95
    label "przybud&#243;wka"
  ]
  node [
    id 96
    label "ZOMO"
  ]
  node [
    id 97
    label "podmiot"
  ]
  node [
    id 98
    label "boj&#243;wka"
  ]
  node [
    id 99
    label "organization"
  ]
  node [
    id 100
    label "TOPR"
  ]
  node [
    id 101
    label "jednostka_organizacyjna"
  ]
  node [
    id 102
    label "przedstawicielstwo"
  ]
  node [
    id 103
    label "Cepelia"
  ]
  node [
    id 104
    label "GOPR"
  ]
  node [
    id 105
    label "ZMP"
  ]
  node [
    id 106
    label "ZBoWiD"
  ]
  node [
    id 107
    label "struktura"
  ]
  node [
    id 108
    label "od&#322;am"
  ]
  node [
    id 109
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 110
    label "centrala"
  ]
  node [
    id 111
    label "sprzyja&#263;"
  ]
  node [
    id 112
    label "back"
  ]
  node [
    id 113
    label "pociesza&#263;"
  ]
  node [
    id 114
    label "Warszawa"
  ]
  node [
    id 115
    label "u&#322;atwia&#263;"
  ]
  node [
    id 116
    label "opiera&#263;"
  ]
  node [
    id 117
    label "usi&#322;owanie"
  ]
  node [
    id 118
    label "examination"
  ]
  node [
    id 119
    label "investigation"
  ]
  node [
    id 120
    label "ustalenie"
  ]
  node [
    id 121
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 122
    label "ustalanie"
  ]
  node [
    id 123
    label "bia&#322;a_niedziela"
  ]
  node [
    id 124
    label "analysis"
  ]
  node [
    id 125
    label "rozpatrywanie"
  ]
  node [
    id 126
    label "wziernikowanie"
  ]
  node [
    id 127
    label "obserwowanie"
  ]
  node [
    id 128
    label "omawianie"
  ]
  node [
    id 129
    label "sprawdzanie"
  ]
  node [
    id 130
    label "udowadnianie"
  ]
  node [
    id 131
    label "diagnostyka"
  ]
  node [
    id 132
    label "czynno&#347;&#263;"
  ]
  node [
    id 133
    label "macanie"
  ]
  node [
    id 134
    label "rektalny"
  ]
  node [
    id 135
    label "penetrowanie"
  ]
  node [
    id 136
    label "krytykowanie"
  ]
  node [
    id 137
    label "kontrola"
  ]
  node [
    id 138
    label "dociekanie"
  ]
  node [
    id 139
    label "zrecenzowanie"
  ]
  node [
    id 140
    label "praca"
  ]
  node [
    id 141
    label "rezultat"
  ]
  node [
    id 142
    label "specjalny"
  ]
  node [
    id 143
    label "edukacyjnie"
  ]
  node [
    id 144
    label "intelektualny"
  ]
  node [
    id 145
    label "skomplikowany"
  ]
  node [
    id 146
    label "zgodny"
  ]
  node [
    id 147
    label "naukowo"
  ]
  node [
    id 148
    label "scjentyficzny"
  ]
  node [
    id 149
    label "teoretyczny"
  ]
  node [
    id 150
    label "specjalistyczny"
  ]
  node [
    id 151
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 152
    label "doba"
  ]
  node [
    id 153
    label "czas"
  ]
  node [
    id 154
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 155
    label "weekend"
  ]
  node [
    id 156
    label "miesi&#261;c"
  ]
  node [
    id 157
    label "consolidate"
  ]
  node [
    id 158
    label "przejmowa&#263;"
  ]
  node [
    id 159
    label "dostawa&#263;"
  ]
  node [
    id 160
    label "mie&#263;_miejsce"
  ]
  node [
    id 161
    label "sprz&#261;ta&#263;"
  ]
  node [
    id 162
    label "oszcz&#281;dza&#263;"
  ]
  node [
    id 163
    label "pozyskiwa&#263;"
  ]
  node [
    id 164
    label "meet"
  ]
  node [
    id 165
    label "congregate"
  ]
  node [
    id 166
    label "poci&#261;ga&#263;"
  ]
  node [
    id 167
    label "robi&#263;"
  ]
  node [
    id 168
    label "uk&#322;ada&#263;"
  ]
  node [
    id 169
    label "umieszcza&#263;"
  ]
  node [
    id 170
    label "wzbiera&#263;"
  ]
  node [
    id 171
    label "gromadzi&#263;"
  ]
  node [
    id 172
    label "powodowa&#263;"
  ]
  node [
    id 173
    label "bra&#263;"
  ]
  node [
    id 174
    label "nat&#281;&#380;a&#263;"
  ]
  node [
    id 175
    label "w&#322;asnor&#281;czny"
  ]
  node [
    id 176
    label "napis"
  ]
  node [
    id 177
    label "obja&#347;nienie"
  ]
  node [
    id 178
    label "sign"
  ]
  node [
    id 179
    label "potwierdzenie"
  ]
  node [
    id 180
    label "signal"
  ]
  node [
    id 181
    label "znak"
  ]
  node [
    id 182
    label "pismo"
  ]
  node [
    id 183
    label "pro&#347;ba"
  ]
  node [
    id 184
    label "nakazywa&#263;"
  ]
  node [
    id 185
    label "cry"
  ]
  node [
    id 186
    label "pobudza&#263;"
  ]
  node [
    id 187
    label "invite"
  ]
  node [
    id 188
    label "donosi&#263;"
  ]
  node [
    id 189
    label "order"
  ]
  node [
    id 190
    label "prosi&#263;"
  ]
  node [
    id 191
    label "address"
  ]
  node [
    id 192
    label "uk&#322;ad"
  ]
  node [
    id 193
    label "partia"
  ]
  node [
    id 194
    label "Unia_Wolno&#347;ci"
  ]
  node [
    id 195
    label "Unia_Europejska"
  ]
  node [
    id 196
    label "combination"
  ]
  node [
    id 197
    label "union"
  ]
  node [
    id 198
    label "Unia"
  ]
  node [
    id 199
    label "zrobienie"
  ]
  node [
    id 200
    label "obietnica"
  ]
  node [
    id 201
    label "spowodowanie"
  ]
  node [
    id 202
    label "automatyczny"
  ]
  node [
    id 203
    label "poinformowanie"
  ]
  node [
    id 204
    label "zapowied&#378;"
  ]
  node [
    id 205
    label "statement"
  ]
  node [
    id 206
    label "security"
  ]
  node [
    id 207
    label "za&#347;wiadczenie"
  ]
  node [
    id 208
    label "proposition"
  ]
  node [
    id 209
    label "jawny"
  ]
  node [
    id 210
    label "upublicznienie"
  ]
  node [
    id 211
    label "upublicznianie"
  ]
  node [
    id 212
    label "miejsce"
  ]
  node [
    id 213
    label "konto"
  ]
  node [
    id 214
    label "informatyka"
  ]
  node [
    id 215
    label "has&#322;o"
  ]
  node [
    id 216
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 217
    label "operacja"
  ]
  node [
    id 218
    label "typ"
  ]
  node [
    id 219
    label "dzia&#322;anie"
  ]
  node [
    id 220
    label "przyczyna"
  ]
  node [
    id 221
    label "zaokr&#261;gli&#263;"
  ]
  node [
    id 222
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 223
    label "zaokr&#261;glenie"
  ]
  node [
    id 224
    label "event"
  ]
  node [
    id 225
    label "finance"
  ]
  node [
    id 226
    label "p&#322;aci&#263;"
  ]
  node [
    id 227
    label "jawnie"
  ]
  node [
    id 228
    label "Scholarly"
  ]
  node [
    id 229
    label "Publishing"
  ]
  node [
    id 230
    label "Anda"
  ]
  node [
    id 231
    label "Academic"
  ]
  node [
    id 232
    label "Resources"
  ]
  node [
    id 233
    label "Coalition"
  ]
  node [
    id 234
    label "european"
  ]
  node [
    id 235
    label "Research"
  ]
  node [
    id 236
    label "Advisory"
  ]
  node [
    id 237
    label "Board"
  ]
  node [
    id 238
    label "Council"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 83
  ]
  edge [
    source 11
    target 84
  ]
  edge [
    source 11
    target 85
  ]
  edge [
    source 11
    target 86
  ]
  edge [
    source 11
    target 87
  ]
  edge [
    source 11
    target 88
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 12
    target 94
  ]
  edge [
    source 12
    target 95
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 12
    target 48
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 12
    target 107
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 12
    target 109
  ]
  edge [
    source 12
    target 110
  ]
  edge [
    source 12
    target 23
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 13
    target 113
  ]
  edge [
    source 13
    target 114
  ]
  edge [
    source 13
    target 115
  ]
  edge [
    source 13
    target 116
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 27
  ]
  edge [
    source 14
    target 28
  ]
  edge [
    source 14
    target 117
  ]
  edge [
    source 14
    target 118
  ]
  edge [
    source 14
    target 119
  ]
  edge [
    source 14
    target 120
  ]
  edge [
    source 14
    target 121
  ]
  edge [
    source 14
    target 122
  ]
  edge [
    source 14
    target 123
  ]
  edge [
    source 14
    target 124
  ]
  edge [
    source 14
    target 125
  ]
  edge [
    source 14
    target 126
  ]
  edge [
    source 14
    target 127
  ]
  edge [
    source 14
    target 128
  ]
  edge [
    source 14
    target 129
  ]
  edge [
    source 14
    target 130
  ]
  edge [
    source 14
    target 131
  ]
  edge [
    source 14
    target 132
  ]
  edge [
    source 14
    target 133
  ]
  edge [
    source 14
    target 134
  ]
  edge [
    source 14
    target 135
  ]
  edge [
    source 14
    target 136
  ]
  edge [
    source 14
    target 137
  ]
  edge [
    source 14
    target 138
  ]
  edge [
    source 14
    target 139
  ]
  edge [
    source 14
    target 140
  ]
  edge [
    source 14
    target 141
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 142
  ]
  edge [
    source 15
    target 143
  ]
  edge [
    source 15
    target 144
  ]
  edge [
    source 15
    target 145
  ]
  edge [
    source 15
    target 146
  ]
  edge [
    source 15
    target 147
  ]
  edge [
    source 15
    target 148
  ]
  edge [
    source 15
    target 149
  ]
  edge [
    source 15
    target 150
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 151
  ]
  edge [
    source 17
    target 152
  ]
  edge [
    source 17
    target 153
  ]
  edge [
    source 17
    target 154
  ]
  edge [
    source 17
    target 155
  ]
  edge [
    source 17
    target 156
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 157
  ]
  edge [
    source 18
    target 158
  ]
  edge [
    source 18
    target 159
  ]
  edge [
    source 18
    target 160
  ]
  edge [
    source 18
    target 161
  ]
  edge [
    source 18
    target 162
  ]
  edge [
    source 18
    target 163
  ]
  edge [
    source 18
    target 164
  ]
  edge [
    source 18
    target 165
  ]
  edge [
    source 18
    target 166
  ]
  edge [
    source 18
    target 167
  ]
  edge [
    source 18
    target 168
  ]
  edge [
    source 18
    target 169
  ]
  edge [
    source 18
    target 170
  ]
  edge [
    source 18
    target 171
  ]
  edge [
    source 18
    target 172
  ]
  edge [
    source 18
    target 173
  ]
  edge [
    source 18
    target 174
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 175
  ]
  edge [
    source 19
    target 176
  ]
  edge [
    source 19
    target 177
  ]
  edge [
    source 19
    target 178
  ]
  edge [
    source 19
    target 179
  ]
  edge [
    source 19
    target 180
  ]
  edge [
    source 19
    target 181
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 182
  ]
  edge [
    source 21
    target 183
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 184
  ]
  edge [
    source 22
    target 185
  ]
  edge [
    source 22
    target 186
  ]
  edge [
    source 22
    target 187
  ]
  edge [
    source 22
    target 188
  ]
  edge [
    source 22
    target 189
  ]
  edge [
    source 22
    target 190
  ]
  edge [
    source 22
    target 191
  ]
  edge [
    source 23
    target 192
  ]
  edge [
    source 23
    target 193
  ]
  edge [
    source 23
    target 194
  ]
  edge [
    source 23
    target 195
  ]
  edge [
    source 23
    target 196
  ]
  edge [
    source 23
    target 197
  ]
  edge [
    source 23
    target 198
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 199
  ]
  edge [
    source 24
    target 200
  ]
  edge [
    source 24
    target 201
  ]
  edge [
    source 24
    target 202
  ]
  edge [
    source 24
    target 203
  ]
  edge [
    source 24
    target 204
  ]
  edge [
    source 24
    target 205
  ]
  edge [
    source 24
    target 206
  ]
  edge [
    source 24
    target 207
  ]
  edge [
    source 24
    target 208
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 209
  ]
  edge [
    source 25
    target 210
  ]
  edge [
    source 25
    target 211
  ]
  edge [
    source 25
    target 29
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 212
  ]
  edge [
    source 26
    target 213
  ]
  edge [
    source 26
    target 214
  ]
  edge [
    source 26
    target 215
  ]
  edge [
    source 26
    target 216
  ]
  edge [
    source 26
    target 217
  ]
  edge [
    source 27
    target 218
  ]
  edge [
    source 27
    target 219
  ]
  edge [
    source 27
    target 220
  ]
  edge [
    source 27
    target 221
  ]
  edge [
    source 27
    target 222
  ]
  edge [
    source 27
    target 223
  ]
  edge [
    source 27
    target 224
  ]
  edge [
    source 27
    target 141
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 225
  ]
  edge [
    source 28
    target 226
  ]
  edge [
    source 29
    target 227
  ]
  edge [
    source 228
    target 229
  ]
  edge [
    source 228
    target 230
  ]
  edge [
    source 228
    target 231
  ]
  edge [
    source 228
    target 232
  ]
  edge [
    source 228
    target 233
  ]
  edge [
    source 229
    target 230
  ]
  edge [
    source 229
    target 231
  ]
  edge [
    source 229
    target 232
  ]
  edge [
    source 229
    target 233
  ]
  edge [
    source 230
    target 231
  ]
  edge [
    source 230
    target 232
  ]
  edge [
    source 230
    target 233
  ]
  edge [
    source 231
    target 232
  ]
  edge [
    source 231
    target 233
  ]
  edge [
    source 232
    target 233
  ]
  edge [
    source 234
    target 235
  ]
  edge [
    source 234
    target 236
  ]
  edge [
    source 234
    target 237
  ]
  edge [
    source 234
    target 238
  ]
  edge [
    source 235
    target 236
  ]
  edge [
    source 235
    target 237
  ]
  edge [
    source 235
    target 238
  ]
  edge [
    source 236
    target 237
  ]
]
