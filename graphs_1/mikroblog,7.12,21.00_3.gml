graph [
  maxDegree 12
  minDegree 1
  meanDegree 1.8823529411764706
  density 0.11764705882352941
  graphCliqueNumber 2
  node [
    id 0
    label "raz"
    origin "text"
  ]
  node [
    id 1
    label "dobrze"
    origin "text"
  ]
  node [
    id 2
    label "chwila"
  ]
  node [
    id 3
    label "uderzenie"
  ]
  node [
    id 4
    label "cios"
  ]
  node [
    id 5
    label "time"
  ]
  node [
    id 6
    label "moralnie"
  ]
  node [
    id 7
    label "wiele"
  ]
  node [
    id 8
    label "lepiej"
  ]
  node [
    id 9
    label "korzystnie"
  ]
  node [
    id 10
    label "pomy&#347;lnie"
  ]
  node [
    id 11
    label "pozytywnie"
  ]
  node [
    id 12
    label "dobry"
  ]
  node [
    id 13
    label "dobroczynnie"
  ]
  node [
    id 14
    label "odpowiednio"
  ]
  node [
    id 15
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 16
    label "skutecznie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
]
