graph [
  maxDegree 8
  minDegree 1
  meanDegree 1.8666666666666667
  density 0.13333333333333333
  graphCliqueNumber 2
  node [
    id 0
    label "koga"
    origin "text"
  ]
  node [
    id 1
    label "zawo&#322;a&#263;"
    origin "text"
  ]
  node [
    id 2
    label "lato"
    origin "text"
  ]
  node [
    id 3
    label "nef"
  ]
  node [
    id 4
    label "statek_handlowy"
  ]
  node [
    id 5
    label "&#380;aglowiec"
  ]
  node [
    id 6
    label "kasztel"
  ]
  node [
    id 7
    label "hulk"
  ]
  node [
    id 8
    label "cry"
  ]
  node [
    id 9
    label "powiedzie&#263;"
  ]
  node [
    id 10
    label "invite"
  ]
  node [
    id 11
    label "krzykn&#261;&#263;"
  ]
  node [
    id 12
    label "przewo&#322;a&#263;"
  ]
  node [
    id 13
    label "nakaza&#263;"
  ]
  node [
    id 14
    label "pora_roku"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 2
    target 14
  ]
]
