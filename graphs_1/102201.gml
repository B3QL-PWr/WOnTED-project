graph [
  maxDegree 152
  minDegree 1
  meanDegree 1.9903846153846154
  density 0.009615384615384616
  graphCliqueNumber 2
  node [
    id 0
    label "ten"
    origin "text"
  ]
  node [
    id 1
    label "sklep"
    origin "text"
  ]
  node [
    id 2
    label "kolonialny"
    origin "text"
  ]
  node [
    id 3
    label "odeprze&#263;"
    origin "text"
  ]
  node [
    id 4
    label "domy&#347;lny"
    origin "text"
  ]
  node [
    id 5
    label "biograf"
    origin "text"
  ]
  node [
    id 6
    label "dyskretnie"
    origin "text"
  ]
  node [
    id 7
    label "patrze&#263;"
    origin "text"
  ]
  node [
    id 8
    label "ziemia"
    origin "text"
  ]
  node [
    id 9
    label "okre&#347;lony"
  ]
  node [
    id 10
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 11
    label "stoisko"
  ]
  node [
    id 12
    label "sk&#322;ad"
  ]
  node [
    id 13
    label "firma"
  ]
  node [
    id 14
    label "lokal_u&#380;ytkowy"
  ]
  node [
    id 15
    label "witryna"
  ]
  node [
    id 16
    label "obiekt_handlowy"
  ]
  node [
    id 17
    label "zaplecze"
  ]
  node [
    id 18
    label "p&#243;&#322;ka"
  ]
  node [
    id 19
    label "delikatesowy"
  ]
  node [
    id 20
    label "egzotyczny"
  ]
  node [
    id 21
    label "typowy"
  ]
  node [
    id 22
    label "pogardliwy"
  ]
  node [
    id 23
    label "tael"
  ]
  node [
    id 24
    label "da&#263;"
  ]
  node [
    id 25
    label "odblokowa&#263;"
  ]
  node [
    id 26
    label "odpowiedzie&#263;"
  ]
  node [
    id 27
    label "zareagowa&#263;"
  ]
  node [
    id 28
    label "oddali&#263;"
  ]
  node [
    id 29
    label "z&#322;ama&#263;"
  ]
  node [
    id 30
    label "agreement"
  ]
  node [
    id 31
    label "tax_return"
  ]
  node [
    id 32
    label "reject"
  ]
  node [
    id 33
    label "stawi&#263;_czo&#322;a"
  ]
  node [
    id 34
    label "wiadomy"
  ]
  node [
    id 35
    label "znacz&#261;cy"
  ]
  node [
    id 36
    label "ustalony"
  ]
  node [
    id 37
    label "dorozumiany"
  ]
  node [
    id 38
    label "inteligentny"
  ]
  node [
    id 39
    label "pisarz"
  ]
  node [
    id 40
    label "nieznacznie"
  ]
  node [
    id 41
    label "discreetly"
  ]
  node [
    id 42
    label "taktownie"
  ]
  node [
    id 43
    label "indiscreetly"
  ]
  node [
    id 44
    label "dyskretny"
  ]
  node [
    id 45
    label "koso"
  ]
  node [
    id 46
    label "szuka&#263;"
  ]
  node [
    id 47
    label "go_steady"
  ]
  node [
    id 48
    label "dba&#263;"
  ]
  node [
    id 49
    label "traktowa&#263;"
  ]
  node [
    id 50
    label "os&#261;dza&#263;"
  ]
  node [
    id 51
    label "punkt_widzenia"
  ]
  node [
    id 52
    label "robi&#263;"
  ]
  node [
    id 53
    label "uwa&#380;a&#263;"
  ]
  node [
    id 54
    label "look"
  ]
  node [
    id 55
    label "pogl&#261;da&#263;"
  ]
  node [
    id 56
    label "przygl&#261;da&#263;_si&#281;"
  ]
  node [
    id 57
    label "Skandynawia"
  ]
  node [
    id 58
    label "Yorkshire"
  ]
  node [
    id 59
    label "Kaukaz"
  ]
  node [
    id 60
    label "Kaszmir"
  ]
  node [
    id 61
    label "Toskania"
  ]
  node [
    id 62
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 63
    label "&#321;emkowszczyzna"
  ]
  node [
    id 64
    label "obszar"
  ]
  node [
    id 65
    label "Amhara"
  ]
  node [
    id 66
    label "Lombardia"
  ]
  node [
    id 67
    label "Podbeskidzie"
  ]
  node [
    id 68
    label "Kalabria"
  ]
  node [
    id 69
    label "kort"
  ]
  node [
    id 70
    label "Tyrol"
  ]
  node [
    id 71
    label "Pamir"
  ]
  node [
    id 72
    label "Lubelszczyzna"
  ]
  node [
    id 73
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 74
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 75
    label "&#379;ywiecczyzna"
  ]
  node [
    id 76
    label "ryzosfera"
  ]
  node [
    id 77
    label "Europa_Wschodnia"
  ]
  node [
    id 78
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 79
    label "Zabajkale"
  ]
  node [
    id 80
    label "Kaszuby"
  ]
  node [
    id 81
    label "Bo&#347;nia"
  ]
  node [
    id 82
    label "Noworosja"
  ]
  node [
    id 83
    label "Ba&#322;kany"
  ]
  node [
    id 84
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 85
    label "Anglia"
  ]
  node [
    id 86
    label "Kielecczyzna"
  ]
  node [
    id 87
    label "Pomorze_Zachodnie"
  ]
  node [
    id 88
    label "Opolskie"
  ]
  node [
    id 89
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 90
    label "skorupa_ziemska"
  ]
  node [
    id 91
    label "Ko&#322;yma"
  ]
  node [
    id 92
    label "Oksytania"
  ]
  node [
    id 93
    label "Syjon"
  ]
  node [
    id 94
    label "posadzka"
  ]
  node [
    id 95
    label "pa&#324;stwo"
  ]
  node [
    id 96
    label "Kociewie"
  ]
  node [
    id 97
    label "Huculszczyzna"
  ]
  node [
    id 98
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 99
    label "budynek"
  ]
  node [
    id 100
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 101
    label "Bawaria"
  ]
  node [
    id 102
    label "pomieszczenie"
  ]
  node [
    id 103
    label "pr&#243;chnica"
  ]
  node [
    id 104
    label "glinowanie"
  ]
  node [
    id 105
    label "Maghreb"
  ]
  node [
    id 106
    label "Bory_Tucholskie"
  ]
  node [
    id 107
    label "Europa_Zachodnia"
  ]
  node [
    id 108
    label "Kerala"
  ]
  node [
    id 109
    label "Podhale"
  ]
  node [
    id 110
    label "Kabylia"
  ]
  node [
    id 111
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 112
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 113
    label "Ma&#322;opolska"
  ]
  node [
    id 114
    label "Polesie"
  ]
  node [
    id 115
    label "Liguria"
  ]
  node [
    id 116
    label "&#321;&#243;dzkie"
  ]
  node [
    id 117
    label "geosystem"
  ]
  node [
    id 118
    label "Palestyna"
  ]
  node [
    id 119
    label "Bojkowszczyzna"
  ]
  node [
    id 120
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 121
    label "Karaiby"
  ]
  node [
    id 122
    label "S&#261;decczyzna"
  ]
  node [
    id 123
    label "Sand&#380;ak"
  ]
  node [
    id 124
    label "Nadrenia"
  ]
  node [
    id 125
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 126
    label "Zakarpacie"
  ]
  node [
    id 127
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 128
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 129
    label "Zag&#243;rze"
  ]
  node [
    id 130
    label "Andaluzja"
  ]
  node [
    id 131
    label "Turkiestan"
  ]
  node [
    id 132
    label "Naddniestrze"
  ]
  node [
    id 133
    label "Hercegowina"
  ]
  node [
    id 134
    label "p&#322;aszczyzna"
  ]
  node [
    id 135
    label "Opolszczyzna"
  ]
  node [
    id 136
    label "jednostka_administracyjna"
  ]
  node [
    id 137
    label "Lotaryngia"
  ]
  node [
    id 138
    label "Afryka_Wschodnia"
  ]
  node [
    id 139
    label "Szlezwik"
  ]
  node [
    id 140
    label "powierzchnia"
  ]
  node [
    id 141
    label "glinowa&#263;"
  ]
  node [
    id 142
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 143
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 144
    label "podglebie"
  ]
  node [
    id 145
    label "Mazowsze"
  ]
  node [
    id 146
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 147
    label "teren"
  ]
  node [
    id 148
    label "Afryka_Zachodnia"
  ]
  node [
    id 149
    label "czynnik_produkcji"
  ]
  node [
    id 150
    label "Galicja"
  ]
  node [
    id 151
    label "Szkocja"
  ]
  node [
    id 152
    label "Walia"
  ]
  node [
    id 153
    label "Powi&#347;le"
  ]
  node [
    id 154
    label "penetrator"
  ]
  node [
    id 155
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 156
    label "kompleks_sorpcyjny"
  ]
  node [
    id 157
    label "Zamojszczyzna"
  ]
  node [
    id 158
    label "Kujawy"
  ]
  node [
    id 159
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 160
    label "Podlasie"
  ]
  node [
    id 161
    label "Laponia"
  ]
  node [
    id 162
    label "Umbria"
  ]
  node [
    id 163
    label "plantowa&#263;"
  ]
  node [
    id 164
    label "Mezoameryka"
  ]
  node [
    id 165
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 166
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 167
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 168
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 169
    label "Kurdystan"
  ]
  node [
    id 170
    label "Kampania"
  ]
  node [
    id 171
    label "Armagnac"
  ]
  node [
    id 172
    label "Polinezja"
  ]
  node [
    id 173
    label "Warmia"
  ]
  node [
    id 174
    label "Wielkopolska"
  ]
  node [
    id 175
    label "litosfera"
  ]
  node [
    id 176
    label "Bordeaux"
  ]
  node [
    id 177
    label "Lauda"
  ]
  node [
    id 178
    label "Mazury"
  ]
  node [
    id 179
    label "Podkarpacie"
  ]
  node [
    id 180
    label "Oceania"
  ]
  node [
    id 181
    label "Lasko"
  ]
  node [
    id 182
    label "Amazonia"
  ]
  node [
    id 183
    label "pojazd"
  ]
  node [
    id 184
    label "glej"
  ]
  node [
    id 185
    label "martwica"
  ]
  node [
    id 186
    label "zapadnia"
  ]
  node [
    id 187
    label "przestrze&#324;"
  ]
  node [
    id 188
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 189
    label "dotleni&#263;"
  ]
  node [
    id 190
    label "Kurpie"
  ]
  node [
    id 191
    label "Tonkin"
  ]
  node [
    id 192
    label "Azja_Wschodnia"
  ]
  node [
    id 193
    label "Mikronezja"
  ]
  node [
    id 194
    label "Ukraina_Zachodnia"
  ]
  node [
    id 195
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 196
    label "Turyngia"
  ]
  node [
    id 197
    label "Baszkiria"
  ]
  node [
    id 198
    label "Apulia"
  ]
  node [
    id 199
    label "miejsce"
  ]
  node [
    id 200
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 201
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 202
    label "Indochiny"
  ]
  node [
    id 203
    label "Biskupizna"
  ]
  node [
    id 204
    label "Lubuskie"
  ]
  node [
    id 205
    label "domain"
  ]
  node [
    id 206
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 207
    label "Ameryka_&#321;aci&#324;ska"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 8
    target 122
  ]
  edge [
    source 8
    target 123
  ]
  edge [
    source 8
    target 124
  ]
  edge [
    source 8
    target 125
  ]
  edge [
    source 8
    target 126
  ]
  edge [
    source 8
    target 127
  ]
  edge [
    source 8
    target 128
  ]
  edge [
    source 8
    target 129
  ]
  edge [
    source 8
    target 130
  ]
  edge [
    source 8
    target 131
  ]
  edge [
    source 8
    target 132
  ]
  edge [
    source 8
    target 133
  ]
  edge [
    source 8
    target 134
  ]
  edge [
    source 8
    target 135
  ]
  edge [
    source 8
    target 136
  ]
  edge [
    source 8
    target 137
  ]
  edge [
    source 8
    target 138
  ]
  edge [
    source 8
    target 139
  ]
  edge [
    source 8
    target 140
  ]
  edge [
    source 8
    target 141
  ]
  edge [
    source 8
    target 142
  ]
  edge [
    source 8
    target 143
  ]
  edge [
    source 8
    target 144
  ]
  edge [
    source 8
    target 145
  ]
  edge [
    source 8
    target 146
  ]
  edge [
    source 8
    target 147
  ]
  edge [
    source 8
    target 148
  ]
  edge [
    source 8
    target 149
  ]
  edge [
    source 8
    target 150
  ]
  edge [
    source 8
    target 151
  ]
  edge [
    source 8
    target 152
  ]
  edge [
    source 8
    target 153
  ]
  edge [
    source 8
    target 154
  ]
  edge [
    source 8
    target 155
  ]
  edge [
    source 8
    target 156
  ]
  edge [
    source 8
    target 157
  ]
  edge [
    source 8
    target 158
  ]
  edge [
    source 8
    target 159
  ]
  edge [
    source 8
    target 160
  ]
  edge [
    source 8
    target 161
  ]
  edge [
    source 8
    target 162
  ]
  edge [
    source 8
    target 163
  ]
  edge [
    source 8
    target 164
  ]
  edge [
    source 8
    target 165
  ]
  edge [
    source 8
    target 166
  ]
  edge [
    source 8
    target 167
  ]
  edge [
    source 8
    target 168
  ]
  edge [
    source 8
    target 169
  ]
  edge [
    source 8
    target 170
  ]
  edge [
    source 8
    target 171
  ]
  edge [
    source 8
    target 172
  ]
  edge [
    source 8
    target 173
  ]
  edge [
    source 8
    target 174
  ]
  edge [
    source 8
    target 175
  ]
  edge [
    source 8
    target 176
  ]
  edge [
    source 8
    target 177
  ]
  edge [
    source 8
    target 178
  ]
  edge [
    source 8
    target 179
  ]
  edge [
    source 8
    target 180
  ]
  edge [
    source 8
    target 181
  ]
  edge [
    source 8
    target 182
  ]
  edge [
    source 8
    target 183
  ]
  edge [
    source 8
    target 184
  ]
  edge [
    source 8
    target 185
  ]
  edge [
    source 8
    target 186
  ]
  edge [
    source 8
    target 187
  ]
  edge [
    source 8
    target 188
  ]
  edge [
    source 8
    target 189
  ]
  edge [
    source 8
    target 190
  ]
  edge [
    source 8
    target 191
  ]
  edge [
    source 8
    target 192
  ]
  edge [
    source 8
    target 193
  ]
  edge [
    source 8
    target 194
  ]
  edge [
    source 8
    target 195
  ]
  edge [
    source 8
    target 196
  ]
  edge [
    source 8
    target 197
  ]
  edge [
    source 8
    target 198
  ]
  edge [
    source 8
    target 199
  ]
  edge [
    source 8
    target 200
  ]
  edge [
    source 8
    target 201
  ]
  edge [
    source 8
    target 202
  ]
  edge [
    source 8
    target 203
  ]
  edge [
    source 8
    target 204
  ]
  edge [
    source 8
    target 205
  ]
  edge [
    source 8
    target 206
  ]
  edge [
    source 8
    target 207
  ]
]
