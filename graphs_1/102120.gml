graph [
  maxDegree 34
  minDegree 1
  meanDegree 1.9863945578231292
  density 0.013605442176870748
  graphCliqueNumber 2
  node [
    id 0
    label "spotkanie"
    origin "text"
  ]
  node [
    id 1
    label "go&#347;cinnie"
    origin "text"
  ]
  node [
    id 2
    label "pojawi&#263;"
    origin "text"
  ]
  node [
    id 3
    label "si&#281;"
    origin "text"
  ]
  node [
    id 4
    label "samolot"
    origin "text"
  ]
  node [
    id 5
    label "adam"
    origin "text"
  ]
  node [
    id 6
    label "napedzany"
    origin "text"
  ]
  node [
    id 7
    label "silnik"
    origin "text"
  ]
  node [
    id 8
    label "elektryczny"
    origin "text"
  ]
  node [
    id 9
    label "model"
    origin "text"
  ]
  node [
    id 10
    label "can"
    origin "text"
  ]
  node [
    id 11
    label "wzbudzi&#263;"
    origin "text"
  ]
  node [
    id 12
    label "spory"
    origin "text"
  ]
  node [
    id 13
    label "zainteresowanie"
    origin "text"
  ]
  node [
    id 14
    label "chocia&#380;"
    origin "text"
  ]
  node [
    id 15
    label "warunek"
    origin "text"
  ]
  node [
    id 16
    label "pogodowy"
    origin "text"
  ]
  node [
    id 17
    label "pozwoli&#263;"
    origin "text"
  ]
  node [
    id 18
    label "polata&#263;"
    origin "text"
  ]
  node [
    id 19
    label "nim"
    origin "text"
  ]
  node [
    id 20
    label "zbyt"
    origin "text"
  ]
  node [
    id 21
    label "d&#322;ugo"
    origin "text"
  ]
  node [
    id 22
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 23
    label "po&#380;egnanie"
  ]
  node [
    id 24
    label "spowodowanie"
  ]
  node [
    id 25
    label "znalezienie"
  ]
  node [
    id 26
    label "znajomy"
  ]
  node [
    id 27
    label "doznanie"
  ]
  node [
    id 28
    label "employment"
  ]
  node [
    id 29
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 30
    label "gather"
  ]
  node [
    id 31
    label "powitanie"
  ]
  node [
    id 32
    label "spotykanie"
  ]
  node [
    id 33
    label "wydarzenie"
  ]
  node [
    id 34
    label "gathering"
  ]
  node [
    id 35
    label "spotkanie_si&#281;"
  ]
  node [
    id 36
    label "zdarzenie_si&#281;"
  ]
  node [
    id 37
    label "match"
  ]
  node [
    id 38
    label "zawarcie"
  ]
  node [
    id 39
    label "go&#347;cinny"
  ]
  node [
    id 40
    label "uprzejmie"
  ]
  node [
    id 41
    label "okazjonalnie"
  ]
  node [
    id 42
    label "przyja&#378;nie"
  ]
  node [
    id 43
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 44
    label "p&#322;atowiec"
  ]
  node [
    id 45
    label "lecenie"
  ]
  node [
    id 46
    label "gondola"
  ]
  node [
    id 47
    label "wylecie&#263;"
  ]
  node [
    id 48
    label "kapotowanie"
  ]
  node [
    id 49
    label "wylatywa&#263;"
  ]
  node [
    id 50
    label "katapulta"
  ]
  node [
    id 51
    label "dzi&#243;b"
  ]
  node [
    id 52
    label "sterownica"
  ]
  node [
    id 53
    label "kad&#322;ub"
  ]
  node [
    id 54
    label "kapotowa&#263;"
  ]
  node [
    id 55
    label "p&#322;at_no&#347;ny"
  ]
  node [
    id 56
    label "fotel_lotniczy"
  ]
  node [
    id 57
    label "kabina"
  ]
  node [
    id 58
    label "wylatywanie"
  ]
  node [
    id 59
    label "pilot_automatyczny"
  ]
  node [
    id 60
    label "inhalator_tlenowy"
  ]
  node [
    id 61
    label "kapota&#380;"
  ]
  node [
    id 62
    label "pok&#322;ad"
  ]
  node [
    id 63
    label "sta&#322;op&#322;at"
  ]
  node [
    id 64
    label "&#380;yroskop"
  ]
  node [
    id 65
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 66
    label "wy&#347;lizg"
  ]
  node [
    id 67
    label "miejsce_siedz&#261;ce"
  ]
  node [
    id 68
    label "skrzyd&#322;o"
  ]
  node [
    id 69
    label "wiatrochron"
  ]
  node [
    id 70
    label "spalin&#243;wka"
  ]
  node [
    id 71
    label "czarna_skrzynka"
  ]
  node [
    id 72
    label "kapot"
  ]
  node [
    id 73
    label "wylecenie"
  ]
  node [
    id 74
    label "kabinka"
  ]
  node [
    id 75
    label "dotarcie"
  ]
  node [
    id 76
    label "wyci&#261;garka"
  ]
  node [
    id 77
    label "biblioteka"
  ]
  node [
    id 78
    label "aerosanie"
  ]
  node [
    id 79
    label "podgrzewacz"
  ]
  node [
    id 80
    label "bombowiec"
  ]
  node [
    id 81
    label "dociera&#263;"
  ]
  node [
    id 82
    label "gniazdo_zaworowe"
  ]
  node [
    id 83
    label "motor&#243;wka"
  ]
  node [
    id 84
    label "nap&#281;d"
  ]
  node [
    id 85
    label "perpetuum_mobile"
  ]
  node [
    id 86
    label "rz&#281;&#380;enie"
  ]
  node [
    id 87
    label "mechanizm"
  ]
  node [
    id 88
    label "gondola_silnikowa"
  ]
  node [
    id 89
    label "docieranie"
  ]
  node [
    id 90
    label "uk&#322;ad_zasilania"
  ]
  node [
    id 91
    label "rz&#281;zi&#263;"
  ]
  node [
    id 92
    label "motoszybowiec"
  ]
  node [
    id 93
    label "motogodzina"
  ]
  node [
    id 94
    label "samoch&#243;d"
  ]
  node [
    id 95
    label "dotrze&#263;"
  ]
  node [
    id 96
    label "radiator"
  ]
  node [
    id 97
    label "program"
  ]
  node [
    id 98
    label "elektrycznie"
  ]
  node [
    id 99
    label "typ"
  ]
  node [
    id 100
    label "cz&#322;owiek"
  ]
  node [
    id 101
    label "pozowa&#263;"
  ]
  node [
    id 102
    label "ideal"
  ]
  node [
    id 103
    label "matryca"
  ]
  node [
    id 104
    label "imitacja"
  ]
  node [
    id 105
    label "ruch"
  ]
  node [
    id 106
    label "motif"
  ]
  node [
    id 107
    label "pozowanie"
  ]
  node [
    id 108
    label "wz&#243;r"
  ]
  node [
    id 109
    label "miniatura"
  ]
  node [
    id 110
    label "prezenter"
  ]
  node [
    id 111
    label "facet"
  ]
  node [
    id 112
    label "orygina&#322;"
  ]
  node [
    id 113
    label "mildew"
  ]
  node [
    id 114
    label "spos&#243;b"
  ]
  node [
    id 115
    label "zi&#243;&#322;ko"
  ]
  node [
    id 116
    label "adaptation"
  ]
  node [
    id 117
    label "wywo&#322;a&#263;"
  ]
  node [
    id 118
    label "arouse"
  ]
  node [
    id 119
    label "sporo"
  ]
  node [
    id 120
    label "intensywny"
  ]
  node [
    id 121
    label "wa&#380;ny"
  ]
  node [
    id 122
    label "ciekawo&#347;&#263;"
  ]
  node [
    id 123
    label "care"
  ]
  node [
    id 124
    label "emocja"
  ]
  node [
    id 125
    label "zainteresowanie_si&#281;"
  ]
  node [
    id 126
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 127
    label "love"
  ]
  node [
    id 128
    label "wzbudzenie"
  ]
  node [
    id 129
    label "za&#322;o&#380;enie"
  ]
  node [
    id 130
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 131
    label "umowa"
  ]
  node [
    id 132
    label "agent"
  ]
  node [
    id 133
    label "condition"
  ]
  node [
    id 134
    label "ekspozycja"
  ]
  node [
    id 135
    label "faktor"
  ]
  node [
    id 136
    label "pofolgowa&#263;"
  ]
  node [
    id 137
    label "assent"
  ]
  node [
    id 138
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 139
    label "leave"
  ]
  node [
    id 140
    label "uzna&#263;"
  ]
  node [
    id 141
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 142
    label "gra_planszowa"
  ]
  node [
    id 143
    label "nadmiernie"
  ]
  node [
    id 144
    label "sprzedawanie"
  ]
  node [
    id 145
    label "sprzeda&#380;"
  ]
  node [
    id 146
    label "d&#322;ugi"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 9
    target 100
  ]
  edge [
    source 9
    target 101
  ]
  edge [
    source 9
    target 102
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 104
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 9
    target 116
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 117
  ]
  edge [
    source 11
    target 118
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 119
  ]
  edge [
    source 12
    target 120
  ]
  edge [
    source 12
    target 121
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 122
  ]
  edge [
    source 13
    target 123
  ]
  edge [
    source 13
    target 124
  ]
  edge [
    source 13
    target 125
  ]
  edge [
    source 13
    target 126
  ]
  edge [
    source 13
    target 127
  ]
  edge [
    source 13
    target 128
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 129
  ]
  edge [
    source 15
    target 130
  ]
  edge [
    source 15
    target 131
  ]
  edge [
    source 15
    target 132
  ]
  edge [
    source 15
    target 133
  ]
  edge [
    source 15
    target 134
  ]
  edge [
    source 15
    target 135
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 136
  ]
  edge [
    source 17
    target 137
  ]
  edge [
    source 17
    target 138
  ]
  edge [
    source 17
    target 139
  ]
  edge [
    source 17
    target 140
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 141
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 142
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 143
  ]
  edge [
    source 20
    target 144
  ]
  edge [
    source 20
    target 145
  ]
  edge [
    source 21
    target 146
  ]
]
