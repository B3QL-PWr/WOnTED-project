graph [
  maxDegree 16
  minDegree 1
  meanDegree 2.473684210526316
  density 0.021891010712622262
  graphCliqueNumber 6
  node [
    id 0
    label "jura"
    origin "text"
  ]
  node [
    id 1
    label "postanowi&#263;"
    origin "text"
  ]
  node [
    id 2
    label "przyzna&#263;"
    origin "text"
  ]
  node [
    id 3
    label "wszystek"
    origin "text"
  ]
  node [
    id 4
    label "przewidzie&#263;"
    origin "text"
  ]
  node [
    id 5
    label "regulamin"
    origin "text"
  ]
  node [
    id 6
    label "miejsce"
    origin "text"
  ]
  node [
    id 7
    label "rama"
    origin "text"
  ]
  node [
    id 8
    label "dwa"
    origin "text"
  ]
  node [
    id 9
    label "kategoria"
    origin "text"
  ]
  node [
    id 10
    label "formacja_geologiczna"
  ]
  node [
    id 11
    label "jura_&#347;rodkowa"
  ]
  node [
    id 12
    label "dogger"
  ]
  node [
    id 13
    label "jura_p&#243;&#378;na"
  ]
  node [
    id 14
    label "era_mezozoiczna"
  ]
  node [
    id 15
    label "plezjozaur"
  ]
  node [
    id 16
    label "euoplocefal"
  ]
  node [
    id 17
    label "jura_wczesna"
  ]
  node [
    id 18
    label "sta&#263;_si&#281;"
  ]
  node [
    id 19
    label "zrobi&#263;"
  ]
  node [
    id 20
    label "podj&#261;&#263;"
  ]
  node [
    id 21
    label "determine"
  ]
  node [
    id 22
    label "da&#263;"
  ]
  node [
    id 23
    label "nada&#263;"
  ]
  node [
    id 24
    label "pozwoli&#263;"
  ]
  node [
    id 25
    label "give"
  ]
  node [
    id 26
    label "stwierdzi&#263;"
  ]
  node [
    id 27
    label "ca&#322;y"
  ]
  node [
    id 28
    label "envision"
  ]
  node [
    id 29
    label "zaplanowa&#263;"
  ]
  node [
    id 30
    label "spodzia&#263;_si&#281;"
  ]
  node [
    id 31
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 32
    label "zapis"
  ]
  node [
    id 33
    label "zbi&#243;r"
  ]
  node [
    id 34
    label "regulator"
  ]
  node [
    id 35
    label "cia&#322;o"
  ]
  node [
    id 36
    label "plac"
  ]
  node [
    id 37
    label "cecha"
  ]
  node [
    id 38
    label "uwaga"
  ]
  node [
    id 39
    label "przestrze&#324;"
  ]
  node [
    id 40
    label "status"
  ]
  node [
    id 41
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 42
    label "chwila"
  ]
  node [
    id 43
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 44
    label "rz&#261;d"
  ]
  node [
    id 45
    label "praca"
  ]
  node [
    id 46
    label "location"
  ]
  node [
    id 47
    label "warunek_lokalowy"
  ]
  node [
    id 48
    label "zakres"
  ]
  node [
    id 49
    label "dodatek"
  ]
  node [
    id 50
    label "struktura"
  ]
  node [
    id 51
    label "stela&#380;"
  ]
  node [
    id 52
    label "za&#322;o&#380;enie"
  ]
  node [
    id 53
    label "human_body"
  ]
  node [
    id 54
    label "szablon"
  ]
  node [
    id 55
    label "oprawa"
  ]
  node [
    id 56
    label "paczka"
  ]
  node [
    id 57
    label "obramowanie"
  ]
  node [
    id 58
    label "pojazd"
  ]
  node [
    id 59
    label "postawa"
  ]
  node [
    id 60
    label "element_konstrukcyjny"
  ]
  node [
    id 61
    label "forma"
  ]
  node [
    id 62
    label "wytw&#243;r"
  ]
  node [
    id 63
    label "type"
  ]
  node [
    id 64
    label "teoria"
  ]
  node [
    id 65
    label "poj&#281;cie"
  ]
  node [
    id 66
    label "klasa"
  ]
  node [
    id 67
    label "mig"
  ]
  node [
    id 68
    label "13"
  ]
  node [
    id 69
    label "publiczny"
  ]
  node [
    id 70
    label "gimnazjum"
  ]
  node [
    id 71
    label "nr"
  ]
  node [
    id 72
    label "wyspa"
  ]
  node [
    id 73
    label "Wa&#322;brzych"
  ]
  node [
    id 74
    label "&#321;ochowie"
  ]
  node [
    id 75
    label "Nihil"
  ]
  node [
    id 76
    label "novi"
  ]
  node [
    id 77
    label "zesp&#243;&#322;"
  ]
  node [
    id 78
    label "szko&#322;a"
  ]
  node [
    id 79
    label "Rosochatem"
  ]
  node [
    id 80
    label "Ko&#347;cielnem"
  ]
  node [
    id 81
    label "zeszyt"
  ]
  node [
    id 82
    label "ostatni"
  ]
  node [
    id 83
    label "&#322;awka"
  ]
  node [
    id 84
    label "og&#243;lnokszta&#322;c&#261;cy"
  ]
  node [
    id 85
    label "S&#322;omniki"
  ]
  node [
    id 86
    label "beza"
  ]
  node [
    id 87
    label "tytu&#322;"
  ]
  node [
    id 88
    label "reaktywacja"
  ]
  node [
    id 89
    label "Raci&#261;&#380;"
  ]
  node [
    id 90
    label "olimpiada"
  ]
  node [
    id 91
    label "medialny"
  ]
  node [
    id 92
    label "fundacja"
  ]
  node [
    id 93
    label "nowy"
  ]
  node [
    id 94
    label "medium"
  ]
  node [
    id 95
    label "wysoki"
  ]
  node [
    id 96
    label "psychologia"
  ]
  node [
    id 97
    label "spo&#322;eczny"
  ]
  node [
    id 98
    label "mie&#263;"
  ]
  node [
    id 99
    label "forum"
  ]
  node [
    id 100
    label "pismak"
  ]
  node [
    id 101
    label "wydawnictwo"
  ]
  node [
    id 102
    label "szkolny"
  ]
  node [
    id 103
    label "PWN"
  ]
  node [
    id 104
    label "m&#322;odzie&#380;owy"
  ]
  node [
    id 105
    label "akcja"
  ]
  node [
    id 106
    label "multimedialny"
  ]
  node [
    id 107
    label "Fryderyka"
  ]
  node [
    id 108
    label "Chopin"
  ]
  node [
    id 109
    label "internetowy"
  ]
  node [
    id 110
    label "wirtualny"
  ]
  node [
    id 111
    label "konkurs"
  ]
  node [
    id 112
    label "chopinowski"
  ]
  node [
    id 113
    label "rok"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 33
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 68
    target 70
  ]
  edge [
    source 68
    target 71
  ]
  edge [
    source 68
    target 72
  ]
  edge [
    source 68
    target 73
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 69
    target 71
  ]
  edge [
    source 69
    target 72
  ]
  edge [
    source 69
    target 73
  ]
  edge [
    source 69
    target 74
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 72
  ]
  edge [
    source 70
    target 73
  ]
  edge [
    source 70
    target 74
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 71
    target 73
  ]
  edge [
    source 72
    target 73
  ]
  edge [
    source 72
    target 74
  ]
  edge [
    source 72
    target 77
  ]
  edge [
    source 72
    target 78
  ]
  edge [
    source 72
    target 79
  ]
  edge [
    source 72
    target 80
  ]
  edge [
    source 72
    target 84
  ]
  edge [
    source 72
    target 85
  ]
  edge [
    source 72
    target 89
  ]
  edge [
    source 75
    target 76
  ]
  edge [
    source 77
    target 78
  ]
  edge [
    source 77
    target 79
  ]
  edge [
    source 77
    target 80
  ]
  edge [
    source 77
    target 84
  ]
  edge [
    source 77
    target 85
  ]
  edge [
    source 77
    target 89
  ]
  edge [
    source 78
    target 79
  ]
  edge [
    source 78
    target 80
  ]
  edge [
    source 78
    target 84
  ]
  edge [
    source 78
    target 85
  ]
  edge [
    source 78
    target 89
  ]
  edge [
    source 78
    target 95
  ]
  edge [
    source 78
    target 96
  ]
  edge [
    source 78
    target 97
  ]
  edge [
    source 79
    target 80
  ]
  edge [
    source 81
    target 82
  ]
  edge [
    source 81
    target 83
  ]
  edge [
    source 82
    target 83
  ]
  edge [
    source 84
    target 85
  ]
  edge [
    source 86
    target 87
  ]
  edge [
    source 86
    target 88
  ]
  edge [
    source 87
    target 88
  ]
  edge [
    source 90
    target 91
  ]
  edge [
    source 92
    target 93
  ]
  edge [
    source 92
    target 94
  ]
  edge [
    source 93
    target 94
  ]
  edge [
    source 95
    target 96
  ]
  edge [
    source 95
    target 97
  ]
  edge [
    source 96
    target 97
  ]
  edge [
    source 98
    target 99
  ]
  edge [
    source 98
    target 100
  ]
  edge [
    source 99
    target 100
  ]
  edge [
    source 101
    target 102
  ]
  edge [
    source 101
    target 103
  ]
  edge [
    source 102
    target 103
  ]
  edge [
    source 104
    target 105
  ]
  edge [
    source 104
    target 106
  ]
  edge [
    source 105
    target 106
  ]
  edge [
    source 107
    target 108
  ]
  edge [
    source 109
    target 110
  ]
  edge [
    source 109
    target 111
  ]
  edge [
    source 109
    target 112
  ]
  edge [
    source 110
    target 111
  ]
  edge [
    source 110
    target 112
  ]
  edge [
    source 111
    target 112
  ]
  edge [
    source 112
    target 113
  ]
]
