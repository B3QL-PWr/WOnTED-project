graph [
  maxDegree 45
  minDegree 1
  meanDegree 2.049382716049383
  density 0.012729085192853309
  graphCliqueNumber 3
  node [
    id 0
    label "maj"
    origin "text"
  ]
  node [
    id 1
    label "jeszcze"
    origin "text"
  ]
  node [
    id 2
    label "nigdy"
    origin "text"
  ]
  node [
    id 3
    label "by&#322;a"
    origin "text"
  ]
  node [
    id 4
    label "dentysta"
    origin "text"
  ]
  node [
    id 5
    label "tata"
    origin "text"
  ]
  node [
    id 6
    label "wojtek"
    origin "text"
  ]
  node [
    id 7
    label "postanowi&#263;"
    origin "text"
  ]
  node [
    id 8
    label "p&#243;j&#347;&#263;"
    origin "text"
  ]
  node [
    id 9
    label "c&#243;rka"
    origin "text"
  ]
  node [
    id 10
    label "&#380;eby"
    origin "text"
  ]
  node [
    id 11
    label "przekona&#263;"
    origin "text"
  ]
  node [
    id 12
    label "si&#281;"
    origin "text"
  ]
  node [
    id 13
    label "chodzenie"
    origin "text"
  ]
  node [
    id 14
    label "nic"
    origin "text"
  ]
  node [
    id 15
    label "straszny"
    origin "text"
  ]
  node [
    id 16
    label "zarejestrowa&#263;"
    origin "text"
  ]
  node [
    id 17
    label "przychodnia"
    origin "text"
  ]
  node [
    id 18
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 19
    label "te&#380;"
    origin "text"
  ]
  node [
    id 20
    label "by&#263;"
    origin "text"
  ]
  node [
    id 21
    label "pacjent"
    origin "text"
  ]
  node [
    id 22
    label "&#346;wi&#281;to_Pracy"
  ]
  node [
    id 23
    label "miesi&#261;c"
  ]
  node [
    id 24
    label "ci&#261;gle"
  ]
  node [
    id 25
    label "kompletnie"
  ]
  node [
    id 26
    label "partnerka"
  ]
  node [
    id 27
    label "lekarz"
  ]
  node [
    id 28
    label "borowa&#263;"
  ]
  node [
    id 29
    label "wyrwiz&#261;b"
  ]
  node [
    id 30
    label "borowanie"
  ]
  node [
    id 31
    label "stary"
  ]
  node [
    id 32
    label "papa"
  ]
  node [
    id 33
    label "ojciec"
  ]
  node [
    id 34
    label "kuwada"
  ]
  node [
    id 35
    label "ojczym"
  ]
  node [
    id 36
    label "przodek"
  ]
  node [
    id 37
    label "rodzice"
  ]
  node [
    id 38
    label "rodzic"
  ]
  node [
    id 39
    label "sta&#263;_si&#281;"
  ]
  node [
    id 40
    label "zrobi&#263;"
  ]
  node [
    id 41
    label "podj&#261;&#263;"
  ]
  node [
    id 42
    label "determine"
  ]
  node [
    id 43
    label "opu&#347;ci&#263;"
  ]
  node [
    id 44
    label "poruszy&#263;_si&#281;"
  ]
  node [
    id 45
    label "proceed"
  ]
  node [
    id 46
    label "powie&#347;&#263;_si&#281;"
  ]
  node [
    id 47
    label "popsu&#263;_si&#281;"
  ]
  node [
    id 48
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 49
    label "kopn&#261;&#263;_si&#281;"
  ]
  node [
    id 50
    label "zacz&#261;&#263;"
  ]
  node [
    id 51
    label "zmieni&#263;"
  ]
  node [
    id 52
    label "zosta&#263;"
  ]
  node [
    id 53
    label "sail"
  ]
  node [
    id 54
    label "leave"
  ]
  node [
    id 55
    label "uda&#263;_si&#281;"
  ]
  node [
    id 56
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 57
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 58
    label "wyp&#322;yn&#261;&#263;"
  ]
  node [
    id 59
    label "sprzeda&#263;_si&#281;"
  ]
  node [
    id 60
    label "przyj&#261;&#263;"
  ]
  node [
    id 61
    label "potoczy&#263;_si&#281;"
  ]
  node [
    id 62
    label "become"
  ]
  node [
    id 63
    label "play_along"
  ]
  node [
    id 64
    label "travel"
  ]
  node [
    id 65
    label "cz&#322;owiek"
  ]
  node [
    id 66
    label "dziewka"
  ]
  node [
    id 67
    label "potomkini"
  ]
  node [
    id 68
    label "dziewoja"
  ]
  node [
    id 69
    label "dziunia"
  ]
  node [
    id 70
    label "dziecko"
  ]
  node [
    id 71
    label "dziewczynina"
  ]
  node [
    id 72
    label "siksa"
  ]
  node [
    id 73
    label "dziewcz&#281;"
  ]
  node [
    id 74
    label "kora"
  ]
  node [
    id 75
    label "m&#322;&#243;dka"
  ]
  node [
    id 76
    label "dziecina"
  ]
  node [
    id 77
    label "sikorka"
  ]
  node [
    id 78
    label "get"
  ]
  node [
    id 79
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 80
    label "nak&#322;oni&#263;"
  ]
  node [
    id 81
    label "uchodzenie_si&#281;"
  ]
  node [
    id 82
    label "uruchamianie"
  ]
  node [
    id 83
    label "przechodzenie"
  ]
  node [
    id 84
    label "ubieranie"
  ]
  node [
    id 85
    label "w&#322;&#261;czanie"
  ]
  node [
    id 86
    label "ucz&#281;szczanie"
  ]
  node [
    id 87
    label "noszenie"
  ]
  node [
    id 88
    label "rozdeptywanie"
  ]
  node [
    id 89
    label "wydeptanie"
  ]
  node [
    id 90
    label "wydeptywanie"
  ]
  node [
    id 91
    label "ko&#322;atanie_si&#281;"
  ]
  node [
    id 92
    label "zdeptanie"
  ]
  node [
    id 93
    label "uganianie_si&#281;"
  ]
  node [
    id 94
    label "p&#322;ywanie"
  ]
  node [
    id 95
    label "jitter"
  ]
  node [
    id 96
    label "funkcja"
  ]
  node [
    id 97
    label "dzianie_si&#281;"
  ]
  node [
    id 98
    label "zmierzanie"
  ]
  node [
    id 99
    label "wear"
  ]
  node [
    id 100
    label "deptanie"
  ]
  node [
    id 101
    label "rozpowszechnianie_si&#281;"
  ]
  node [
    id 102
    label "rozdeptanie"
  ]
  node [
    id 103
    label "ubieranie_si&#281;"
  ]
  node [
    id 104
    label "znoszenie"
  ]
  node [
    id 105
    label "staranie_si&#281;"
  ]
  node [
    id 106
    label "podtrzymywanie"
  ]
  node [
    id 107
    label "przenoszenie"
  ]
  node [
    id 108
    label "poruszanie_si&#281;"
  ]
  node [
    id 109
    label "nakr&#281;canie"
  ]
  node [
    id 110
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 111
    label "pochodzenie"
  ]
  node [
    id 112
    label "uruchomienie"
  ]
  node [
    id 113
    label "bywanie"
  ]
  node [
    id 114
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 115
    label "tr&#243;jstronny"
  ]
  node [
    id 116
    label "impact"
  ]
  node [
    id 117
    label "dochodzenie"
  ]
  node [
    id 118
    label "w&#322;&#261;czenie"
  ]
  node [
    id 119
    label "zatrzymanie"
  ]
  node [
    id 120
    label "obchodzenie"
  ]
  node [
    id 121
    label "donoszenie"
  ]
  node [
    id 122
    label "nakr&#281;cenie"
  ]
  node [
    id 123
    label "donaszanie"
  ]
  node [
    id 124
    label "miernota"
  ]
  node [
    id 125
    label "g&#243;wno"
  ]
  node [
    id 126
    label "love"
  ]
  node [
    id 127
    label "ilo&#347;&#263;"
  ]
  node [
    id 128
    label "brak"
  ]
  node [
    id 129
    label "ciura"
  ]
  node [
    id 130
    label "niemoralny"
  ]
  node [
    id 131
    label "niegrzeczny"
  ]
  node [
    id 132
    label "strasznie"
  ]
  node [
    id 133
    label "kurewski"
  ]
  node [
    id 134
    label "olbrzymi"
  ]
  node [
    id 135
    label "notice"
  ]
  node [
    id 136
    label "logarithm"
  ]
  node [
    id 137
    label "wpisa&#263;"
  ]
  node [
    id 138
    label "plac&#243;wka_s&#322;u&#380;by_zdrowia"
  ]
  node [
    id 139
    label "lekarz_rodzinny"
  ]
  node [
    id 140
    label "instytucja"
  ]
  node [
    id 141
    label "si&#281;ga&#263;"
  ]
  node [
    id 142
    label "trwa&#263;"
  ]
  node [
    id 143
    label "obecno&#347;&#263;"
  ]
  node [
    id 144
    label "stan"
  ]
  node [
    id 145
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 146
    label "stand"
  ]
  node [
    id 147
    label "mie&#263;_miejsce"
  ]
  node [
    id 148
    label "uczestniczy&#263;"
  ]
  node [
    id 149
    label "chodzi&#263;"
  ]
  node [
    id 150
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 151
    label "equal"
  ]
  node [
    id 152
    label "od&#322;&#261;czenie"
  ]
  node [
    id 153
    label "przymus_bezpo&#347;redni"
  ]
  node [
    id 154
    label "od&#322;&#261;czanie"
  ]
  node [
    id 155
    label "od&#322;&#261;czy&#263;"
  ]
  node [
    id 156
    label "klient"
  ]
  node [
    id 157
    label "od&#322;&#261;cza&#263;"
  ]
  node [
    id 158
    label "szpitalnik"
  ]
  node [
    id 159
    label "przypadek"
  ]
  node [
    id 160
    label "chory"
  ]
  node [
    id 161
    label "piel&#281;gniarz"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 39
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 7
    target 21
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 8
    target 46
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 40
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 78
  ]
  edge [
    source 11
    target 79
  ]
  edge [
    source 11
    target 80
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 81
  ]
  edge [
    source 13
    target 82
  ]
  edge [
    source 13
    target 83
  ]
  edge [
    source 13
    target 84
  ]
  edge [
    source 13
    target 85
  ]
  edge [
    source 13
    target 86
  ]
  edge [
    source 13
    target 87
  ]
  edge [
    source 13
    target 88
  ]
  edge [
    source 13
    target 89
  ]
  edge [
    source 13
    target 90
  ]
  edge [
    source 13
    target 91
  ]
  edge [
    source 13
    target 92
  ]
  edge [
    source 13
    target 93
  ]
  edge [
    source 13
    target 94
  ]
  edge [
    source 13
    target 95
  ]
  edge [
    source 13
    target 96
  ]
  edge [
    source 13
    target 97
  ]
  edge [
    source 13
    target 98
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 13
    target 101
  ]
  edge [
    source 13
    target 102
  ]
  edge [
    source 13
    target 103
  ]
  edge [
    source 13
    target 104
  ]
  edge [
    source 13
    target 105
  ]
  edge [
    source 13
    target 106
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 13
    target 113
  ]
  edge [
    source 13
    target 114
  ]
  edge [
    source 13
    target 115
  ]
  edge [
    source 13
    target 116
  ]
  edge [
    source 13
    target 117
  ]
  edge [
    source 13
    target 118
  ]
  edge [
    source 13
    target 119
  ]
  edge [
    source 13
    target 120
  ]
  edge [
    source 13
    target 121
  ]
  edge [
    source 13
    target 122
  ]
  edge [
    source 13
    target 123
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 124
  ]
  edge [
    source 14
    target 125
  ]
  edge [
    source 14
    target 126
  ]
  edge [
    source 14
    target 127
  ]
  edge [
    source 14
    target 128
  ]
  edge [
    source 14
    target 129
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 130
  ]
  edge [
    source 15
    target 131
  ]
  edge [
    source 15
    target 132
  ]
  edge [
    source 15
    target 133
  ]
  edge [
    source 15
    target 134
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 57
  ]
  edge [
    source 16
    target 135
  ]
  edge [
    source 16
    target 136
  ]
  edge [
    source 16
    target 137
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 138
  ]
  edge [
    source 17
    target 139
  ]
  edge [
    source 17
    target 140
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 141
  ]
  edge [
    source 20
    target 142
  ]
  edge [
    source 20
    target 143
  ]
  edge [
    source 20
    target 144
  ]
  edge [
    source 20
    target 145
  ]
  edge [
    source 20
    target 146
  ]
  edge [
    source 20
    target 147
  ]
  edge [
    source 20
    target 148
  ]
  edge [
    source 20
    target 149
  ]
  edge [
    source 20
    target 150
  ]
  edge [
    source 20
    target 151
  ]
  edge [
    source 21
    target 152
  ]
  edge [
    source 21
    target 65
  ]
  edge [
    source 21
    target 153
  ]
  edge [
    source 21
    target 154
  ]
  edge [
    source 21
    target 155
  ]
  edge [
    source 21
    target 156
  ]
  edge [
    source 21
    target 157
  ]
  edge [
    source 21
    target 158
  ]
  edge [
    source 21
    target 159
  ]
  edge [
    source 21
    target 160
  ]
  edge [
    source 21
    target 161
  ]
]
