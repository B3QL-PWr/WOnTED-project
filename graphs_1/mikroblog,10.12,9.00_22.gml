graph [
  maxDegree 15
  minDegree 1
  meanDegree 1.92
  density 0.08
  graphCliqueNumber 2
  node [
    id 0
    label "zrobi&#263;"
    origin "text"
  ]
  node [
    id 1
    label "nowom"
    origin "text"
  ]
  node [
    id 2
    label "wersja"
    origin "text"
  ]
  node [
    id 3
    label "moje"
    origin "text"
  ]
  node [
    id 4
    label "ulubiony"
    origin "text"
  ]
  node [
    id 5
    label "mema"
    origin "text"
  ]
  node [
    id 6
    label "aaaaaa"
    origin "text"
  ]
  node [
    id 7
    label "zorganizowa&#263;"
  ]
  node [
    id 8
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 9
    label "przerobi&#263;"
  ]
  node [
    id 10
    label "wystylizowa&#263;"
  ]
  node [
    id 11
    label "cause"
  ]
  node [
    id 12
    label "wydali&#263;"
  ]
  node [
    id 13
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 14
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 15
    label "post&#261;pi&#263;"
  ]
  node [
    id 16
    label "appoint"
  ]
  node [
    id 17
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 18
    label "nabra&#263;"
  ]
  node [
    id 19
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 20
    label "make"
  ]
  node [
    id 21
    label "typ"
  ]
  node [
    id 22
    label "posta&#263;"
  ]
  node [
    id 23
    label "wyj&#261;tkowy"
  ]
  node [
    id 24
    label "faworytny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 5
    target 6
  ]
]
