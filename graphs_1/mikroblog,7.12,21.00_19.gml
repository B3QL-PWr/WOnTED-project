graph [
  maxDegree 7
  minDegree 1
  meanDegree 1.9130434782608696
  density 0.08695652173913043
  graphCliqueNumber 2
  node [
    id 0
    label "byleby&#263;"
    origin "text"
  ]
  node [
    id 1
    label "dzisiaj"
    origin "text"
  ]
  node [
    id 2
    label "zje&#347;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "siebie"
    origin "text"
  ]
  node [
    id 4
    label "sp&#243;&#378;niony"
    origin "text"
  ]
  node [
    id 5
    label "&#347;niadanie"
    origin "text"
  ]
  node [
    id 6
    label "mcdonald's"
    origin "text"
  ]
  node [
    id 7
    label "wycieczka"
    origin "text"
  ]
  node [
    id 8
    label "doba"
  ]
  node [
    id 9
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 10
    label "dzi&#347;"
  ]
  node [
    id 11
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 12
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 13
    label "ze&#380;re&#263;"
  ]
  node [
    id 14
    label "spo&#380;y&#263;"
  ]
  node [
    id 15
    label "zrobi&#263;"
  ]
  node [
    id 16
    label "absorb"
  ]
  node [
    id 17
    label "p&#243;&#378;ny"
  ]
  node [
    id 18
    label "posi&#322;ek"
  ]
  node [
    id 19
    label "wyjazd"
  ]
  node [
    id 20
    label "grupa"
  ]
  node [
    id 21
    label "odpoczynek"
  ]
  node [
    id 22
    label "chadzka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 19
  ]
  edge [
    source 7
    target 20
  ]
  edge [
    source 7
    target 21
  ]
  edge [
    source 7
    target 22
  ]
]
