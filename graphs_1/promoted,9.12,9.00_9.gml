graph [
  maxDegree 30
  minDegree 1
  meanDegree 2.097560975609756
  density 0.012868472243004638
  graphCliqueNumber 3
  node [
    id 0
    label "ostrze&#380;enie"
    origin "text"
  ]
  node [
    id 1
    label "sklep"
    origin "text"
  ]
  node [
    id 2
    label "internetowy"
    origin "text"
  ]
  node [
    id 3
    label "morel"
    origin "text"
  ]
  node [
    id 4
    label "oszukiwa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "klient"
    origin "text"
  ]
  node [
    id 6
    label "sprzedawa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "telefon"
    origin "text"
  ]
  node [
    id 8
    label "samsung"
    origin "text"
  ]
  node [
    id 9
    label "zagraniczny"
    origin "text"
  ]
  node [
    id 10
    label "dystrybucja"
    origin "text"
  ]
  node [
    id 11
    label "spe&#322;nia&#263;"
    origin "text"
  ]
  node [
    id 12
    label "warunek"
    origin "text"
  ]
  node [
    id 13
    label "promocja"
    origin "text"
  ]
  node [
    id 14
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 15
    label "reklamowa&#263;"
    origin "text"
  ]
  node [
    id 16
    label "oficjalny"
    origin "text"
  ]
  node [
    id 17
    label "partner"
    origin "text"
  ]
  node [
    id 18
    label "firma"
    origin "text"
  ]
  node [
    id 19
    label "admonition"
  ]
  node [
    id 20
    label "uprzedzenie"
  ]
  node [
    id 21
    label "uwaga"
  ]
  node [
    id 22
    label "stoisko"
  ]
  node [
    id 23
    label "sk&#322;ad"
  ]
  node [
    id 24
    label "lokal_u&#380;ytkowy"
  ]
  node [
    id 25
    label "witryna"
  ]
  node [
    id 26
    label "obiekt_handlowy"
  ]
  node [
    id 27
    label "zaplecze"
  ]
  node [
    id 28
    label "p&#243;&#322;ka"
  ]
  node [
    id 29
    label "nowoczesny"
  ]
  node [
    id 30
    label "elektroniczny"
  ]
  node [
    id 31
    label "sieciowo"
  ]
  node [
    id 32
    label "netowy"
  ]
  node [
    id 33
    label "internetowo"
  ]
  node [
    id 34
    label "orzyna&#263;"
  ]
  node [
    id 35
    label "oszwabia&#263;"
  ]
  node [
    id 36
    label "obje&#380;d&#380;a&#263;"
  ]
  node [
    id 37
    label "cheat"
  ]
  node [
    id 38
    label "dopuszcza&#263;_si&#281;"
  ]
  node [
    id 39
    label "wkr&#281;ca&#263;"
  ]
  node [
    id 40
    label "cz&#322;owiek"
  ]
  node [
    id 41
    label "bratek"
  ]
  node [
    id 42
    label "klientela"
  ]
  node [
    id 43
    label "szlachcic"
  ]
  node [
    id 44
    label "agent_rozliczeniowy"
  ]
  node [
    id 45
    label "komputer_cyfrowy"
  ]
  node [
    id 46
    label "program"
  ]
  node [
    id 47
    label "us&#322;ugobiorca"
  ]
  node [
    id 48
    label "Rzymianin"
  ]
  node [
    id 49
    label "obywatel"
  ]
  node [
    id 50
    label "op&#281;dza&#263;"
  ]
  node [
    id 51
    label "oferowa&#263;"
  ]
  node [
    id 52
    label "oddawa&#263;"
  ]
  node [
    id 53
    label "handlowa&#263;"
  ]
  node [
    id 54
    label "sell"
  ]
  node [
    id 55
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 56
    label "s&#322;uchawka_telefoniczna"
  ]
  node [
    id 57
    label "coalescence"
  ]
  node [
    id 58
    label "roz&#322;&#261;cza&#263;"
  ]
  node [
    id 59
    label "phreaker"
  ]
  node [
    id 60
    label "infrastruktura"
  ]
  node [
    id 61
    label "wy&#347;wietlacz"
  ]
  node [
    id 62
    label "provider"
  ]
  node [
    id 63
    label "dzwonienie"
  ]
  node [
    id 64
    label "zadzwoni&#263;"
  ]
  node [
    id 65
    label "dzwoni&#263;"
  ]
  node [
    id 66
    label "kontakt"
  ]
  node [
    id 67
    label "mikrotelefon"
  ]
  node [
    id 68
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 69
    label "po&#322;&#261;czenie"
  ]
  node [
    id 70
    label "numer"
  ]
  node [
    id 71
    label "roz&#322;&#261;czanie"
  ]
  node [
    id 72
    label "instalacja"
  ]
  node [
    id 73
    label "billing"
  ]
  node [
    id 74
    label "urz&#261;dzenie"
  ]
  node [
    id 75
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 76
    label "zagranicznie"
  ]
  node [
    id 77
    label "obcy"
  ]
  node [
    id 78
    label "podzia&#322;"
  ]
  node [
    id 79
    label "robi&#263;"
  ]
  node [
    id 80
    label "close"
  ]
  node [
    id 81
    label "perform"
  ]
  node [
    id 82
    label "urzeczywistnia&#263;"
  ]
  node [
    id 83
    label "za&#322;o&#380;enie"
  ]
  node [
    id 84
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 85
    label "umowa"
  ]
  node [
    id 86
    label "agent"
  ]
  node [
    id 87
    label "condition"
  ]
  node [
    id 88
    label "ekspozycja"
  ]
  node [
    id 89
    label "faktor"
  ]
  node [
    id 90
    label "nominacja"
  ]
  node [
    id 91
    label "sprzeda&#380;"
  ]
  node [
    id 92
    label "zamiana"
  ]
  node [
    id 93
    label "graduacja"
  ]
  node [
    id 94
    label "&#347;wiadectwo"
  ]
  node [
    id 95
    label "gradation"
  ]
  node [
    id 96
    label "brief"
  ]
  node [
    id 97
    label "uzyska&#263;"
  ]
  node [
    id 98
    label "promotion"
  ]
  node [
    id 99
    label "promowa&#263;"
  ]
  node [
    id 100
    label "klasa"
  ]
  node [
    id 101
    label "akcja"
  ]
  node [
    id 102
    label "wypromowa&#263;"
  ]
  node [
    id 103
    label "warcaby"
  ]
  node [
    id 104
    label "popularyzacja"
  ]
  node [
    id 105
    label "bran&#380;a"
  ]
  node [
    id 106
    label "informacja"
  ]
  node [
    id 107
    label "impreza"
  ]
  node [
    id 108
    label "decyzja"
  ]
  node [
    id 109
    label "okazja"
  ]
  node [
    id 110
    label "commencement"
  ]
  node [
    id 111
    label "udzieli&#263;"
  ]
  node [
    id 112
    label "szachy"
  ]
  node [
    id 113
    label "damka"
  ]
  node [
    id 114
    label "ask"
  ]
  node [
    id 115
    label "wywy&#380;sza&#263;"
  ]
  node [
    id 116
    label "sk&#322;ada&#263;"
  ]
  node [
    id 117
    label "domaga&#263;_si&#281;"
  ]
  node [
    id 118
    label "zachwala&#263;"
  ]
  node [
    id 119
    label "nagradza&#263;"
  ]
  node [
    id 120
    label "publicize"
  ]
  node [
    id 121
    label "glorify"
  ]
  node [
    id 122
    label "jawny"
  ]
  node [
    id 123
    label "oficjalnie"
  ]
  node [
    id 124
    label "legalny"
  ]
  node [
    id 125
    label "sformalizowanie"
  ]
  node [
    id 126
    label "formalizowanie"
  ]
  node [
    id 127
    label "pow&#347;ci&#261;gliwy"
  ]
  node [
    id 128
    label "formalnie"
  ]
  node [
    id 129
    label "sp&#243;lnik"
  ]
  node [
    id 130
    label "uczestniczenie"
  ]
  node [
    id 131
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 132
    label "przedsi&#281;biorca"
  ]
  node [
    id 133
    label "prowadzi&#263;"
  ]
  node [
    id 134
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 135
    label "kolaborator"
  ]
  node [
    id 136
    label "pracownik"
  ]
  node [
    id 137
    label "wsp&#243;&#322;partner"
  ]
  node [
    id 138
    label "aktor"
  ]
  node [
    id 139
    label "Hortex"
  ]
  node [
    id 140
    label "MAC"
  ]
  node [
    id 141
    label "reengineering"
  ]
  node [
    id 142
    label "nazwa_w&#322;asna"
  ]
  node [
    id 143
    label "podmiot_gospodarczy"
  ]
  node [
    id 144
    label "Google"
  ]
  node [
    id 145
    label "zaufanie"
  ]
  node [
    id 146
    label "biurowiec"
  ]
  node [
    id 147
    label "networking"
  ]
  node [
    id 148
    label "zasoby_ludzkie"
  ]
  node [
    id 149
    label "interes"
  ]
  node [
    id 150
    label "paczkarnia"
  ]
  node [
    id 151
    label "Canon"
  ]
  node [
    id 152
    label "HP"
  ]
  node [
    id 153
    label "Baltona"
  ]
  node [
    id 154
    label "Pewex"
  ]
  node [
    id 155
    label "MAN_SE"
  ]
  node [
    id 156
    label "Apeks"
  ]
  node [
    id 157
    label "zasoby"
  ]
  node [
    id 158
    label "Orbis"
  ]
  node [
    id 159
    label "miejsce_pracy"
  ]
  node [
    id 160
    label "siedziba"
  ]
  node [
    id 161
    label "Spo&#322;em"
  ]
  node [
    id 162
    label "przedsi&#281;biorczo&#347;&#263;"
  ]
  node [
    id 163
    label "Orlen"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 18
  ]
  edge [
    source 8
    target 13
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 8
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 79
  ]
  edge [
    source 11
    target 80
  ]
  edge [
    source 11
    target 81
  ]
  edge [
    source 11
    target 82
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 83
  ]
  edge [
    source 12
    target 84
  ]
  edge [
    source 12
    target 85
  ]
  edge [
    source 12
    target 86
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 12
    target 89
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 90
  ]
  edge [
    source 13
    target 91
  ]
  edge [
    source 13
    target 92
  ]
  edge [
    source 13
    target 93
  ]
  edge [
    source 13
    target 94
  ]
  edge [
    source 13
    target 95
  ]
  edge [
    source 13
    target 96
  ]
  edge [
    source 13
    target 97
  ]
  edge [
    source 13
    target 98
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 13
    target 101
  ]
  edge [
    source 13
    target 102
  ]
  edge [
    source 13
    target 103
  ]
  edge [
    source 13
    target 104
  ]
  edge [
    source 13
    target 105
  ]
  edge [
    source 13
    target 106
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 13
    target 113
  ]
  edge [
    source 13
    target 13
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 114
  ]
  edge [
    source 15
    target 115
  ]
  edge [
    source 15
    target 116
  ]
  edge [
    source 15
    target 117
  ]
  edge [
    source 15
    target 118
  ]
  edge [
    source 15
    target 119
  ]
  edge [
    source 15
    target 120
  ]
  edge [
    source 15
    target 121
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 122
  ]
  edge [
    source 16
    target 123
  ]
  edge [
    source 16
    target 124
  ]
  edge [
    source 16
    target 125
  ]
  edge [
    source 16
    target 126
  ]
  edge [
    source 16
    target 127
  ]
  edge [
    source 16
    target 128
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 129
  ]
  edge [
    source 17
    target 40
  ]
  edge [
    source 17
    target 130
  ]
  edge [
    source 17
    target 131
  ]
  edge [
    source 17
    target 132
  ]
  edge [
    source 17
    target 133
  ]
  edge [
    source 17
    target 134
  ]
  edge [
    source 17
    target 135
  ]
  edge [
    source 17
    target 136
  ]
  edge [
    source 17
    target 137
  ]
  edge [
    source 17
    target 138
  ]
  edge [
    source 18
    target 40
  ]
  edge [
    source 18
    target 139
  ]
  edge [
    source 18
    target 140
  ]
  edge [
    source 18
    target 141
  ]
  edge [
    source 18
    target 142
  ]
  edge [
    source 18
    target 143
  ]
  edge [
    source 18
    target 144
  ]
  edge [
    source 18
    target 145
  ]
  edge [
    source 18
    target 146
  ]
  edge [
    source 18
    target 147
  ]
  edge [
    source 18
    target 148
  ]
  edge [
    source 18
    target 149
  ]
  edge [
    source 18
    target 150
  ]
  edge [
    source 18
    target 151
  ]
  edge [
    source 18
    target 152
  ]
  edge [
    source 18
    target 153
  ]
  edge [
    source 18
    target 154
  ]
  edge [
    source 18
    target 155
  ]
  edge [
    source 18
    target 156
  ]
  edge [
    source 18
    target 157
  ]
  edge [
    source 18
    target 158
  ]
  edge [
    source 18
    target 159
  ]
  edge [
    source 18
    target 160
  ]
  edge [
    source 18
    target 161
  ]
  edge [
    source 18
    target 162
  ]
  edge [
    source 18
    target 163
  ]
  edge [
    source 18
    target 100
  ]
]
