graph [
  maxDegree 14
  minDegree 1
  meanDegree 1.9487179487179487
  density 0.05128205128205128
  graphCliqueNumber 2
  node [
    id 0
    label "serial"
    origin "text"
  ]
  node [
    id 1
    label "przyjaciel"
    origin "text"
  ]
  node [
    id 2
    label "znikn&#261;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "serwis"
    origin "text"
  ]
  node [
    id 4
    label "netflix"
    origin "text"
  ]
  node [
    id 5
    label "seria"
  ]
  node [
    id 6
    label "Klan"
  ]
  node [
    id 7
    label "film"
  ]
  node [
    id 8
    label "Ranczo"
  ]
  node [
    id 9
    label "program_telewizyjny"
  ]
  node [
    id 10
    label "kochanek"
  ]
  node [
    id 11
    label "zaprzyja&#378;nianie_si&#281;"
  ]
  node [
    id 12
    label "przyja&#378;nienie_si&#281;"
  ]
  node [
    id 13
    label "kum"
  ]
  node [
    id 14
    label "sympatyk"
  ]
  node [
    id 15
    label "bratnia_dusza"
  ]
  node [
    id 16
    label "amikus"
  ]
  node [
    id 17
    label "zaprzyja&#378;nienie_si&#281;"
  ]
  node [
    id 18
    label "pobratymiec"
  ]
  node [
    id 19
    label "drogi"
  ]
  node [
    id 20
    label "sta&#263;_si&#281;"
  ]
  node [
    id 21
    label "wyj&#347;&#263;"
  ]
  node [
    id 22
    label "dissolve"
  ]
  node [
    id 23
    label "zgin&#261;&#263;"
  ]
  node [
    id 24
    label "vanish"
  ]
  node [
    id 25
    label "przepa&#347;&#263;"
  ]
  node [
    id 26
    label "die"
  ]
  node [
    id 27
    label "mecz"
  ]
  node [
    id 28
    label "service"
  ]
  node [
    id 29
    label "wytw&#243;r"
  ]
  node [
    id 30
    label "zak&#322;ad"
  ]
  node [
    id 31
    label "us&#322;uga"
  ]
  node [
    id 32
    label "uderzenie"
  ]
  node [
    id 33
    label "doniesienie"
  ]
  node [
    id 34
    label "zastawa"
  ]
  node [
    id 35
    label "YouTube"
  ]
  node [
    id 36
    label "punkt"
  ]
  node [
    id 37
    label "porcja"
  ]
  node [
    id 38
    label "strona"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
]
