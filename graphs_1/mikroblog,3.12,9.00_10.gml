graph [
  maxDegree 11
  minDegree 1
  meanDegree 1.9375
  density 0.0625
  graphCliqueNumber 2
  node [
    id 0
    label "le&#380;a"
    origin "text"
  ]
  node [
    id 1
    label "pot&#281;&#380;nie"
    origin "text"
  ]
  node [
    id 2
    label "chcie&#263;"
    origin "text"
  ]
  node [
    id 3
    label "si&#281;"
    origin "text"
  ]
  node [
    id 4
    label "rucha&#263;"
    origin "text"
  ]
  node [
    id 5
    label "mama"
    origin "text"
  ]
  node [
    id 6
    label "kac"
    origin "text"
  ]
  node [
    id 7
    label "drewno"
  ]
  node [
    id 8
    label "legowisko"
  ]
  node [
    id 9
    label "pot&#281;&#380;ny"
  ]
  node [
    id 10
    label "okazale"
  ]
  node [
    id 11
    label "bardzo"
  ]
  node [
    id 12
    label "konkretnie"
  ]
  node [
    id 13
    label "ogromnie"
  ]
  node [
    id 14
    label "intensywnie"
  ]
  node [
    id 15
    label "olbrzymi"
  ]
  node [
    id 16
    label "czu&#263;"
  ]
  node [
    id 17
    label "desire"
  ]
  node [
    id 18
    label "kcie&#263;"
  ]
  node [
    id 19
    label "bra&#263;"
  ]
  node [
    id 20
    label "matczysko"
  ]
  node [
    id 21
    label "macierz"
  ]
  node [
    id 22
    label "przodkini"
  ]
  node [
    id 23
    label "Matka_Boska"
  ]
  node [
    id 24
    label "macocha"
  ]
  node [
    id 25
    label "matka_zast&#281;pcza"
  ]
  node [
    id 26
    label "stara"
  ]
  node [
    id 27
    label "rodzice"
  ]
  node [
    id 28
    label "rodzic"
  ]
  node [
    id 29
    label "aldehyd_octowy"
  ]
  node [
    id 30
    label "klin"
  ]
  node [
    id 31
    label "doznanie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
]
