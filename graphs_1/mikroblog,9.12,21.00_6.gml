graph [
  maxDegree 23
  minDegree 1
  meanDegree 1.9636363636363636
  density 0.03636363636363636
  graphCliqueNumber 2
  node [
    id 0
    label "matematyk"
    origin "text"
  ]
  node [
    id 1
    label "magia"
    origin "text"
  ]
  node [
    id 2
    label "nauka"
    origin "text"
  ]
  node [
    id 3
    label "Berkeley"
  ]
  node [
    id 4
    label "Ptolemeusz"
  ]
  node [
    id 5
    label "Euklides"
  ]
  node [
    id 6
    label "Gauss"
  ]
  node [
    id 7
    label "Doppler"
  ]
  node [
    id 8
    label "Archimedes"
  ]
  node [
    id 9
    label "Maxwell"
  ]
  node [
    id 10
    label "Borel"
  ]
  node [
    id 11
    label "Pascal"
  ]
  node [
    id 12
    label "Galileusz"
  ]
  node [
    id 13
    label "Newton"
  ]
  node [
    id 14
    label "Kartezjusz"
  ]
  node [
    id 15
    label "Kepler"
  ]
  node [
    id 16
    label "naukowiec"
  ]
  node [
    id 17
    label "Fourier"
  ]
  node [
    id 18
    label "nauczyciel"
  ]
  node [
    id 19
    label "Bayes"
  ]
  node [
    id 20
    label "Pitagoras"
  ]
  node [
    id 21
    label "Biot"
  ]
  node [
    id 22
    label "Laplace"
  ]
  node [
    id 23
    label "praktyki"
  ]
  node [
    id 24
    label "czar"
  ]
  node [
    id 25
    label "wikkanin"
  ]
  node [
    id 26
    label "czarodziejka"
  ]
  node [
    id 27
    label "cecha"
  ]
  node [
    id 28
    label "niecodzienno&#347;&#263;"
  ]
  node [
    id 29
    label "czarownica"
  ]
  node [
    id 30
    label "czarodziej"
  ]
  node [
    id 31
    label "zjawisko"
  ]
  node [
    id 32
    label "agreeableness"
  ]
  node [
    id 33
    label "nauki_o_Ziemi"
  ]
  node [
    id 34
    label "teoria_naukowa"
  ]
  node [
    id 35
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 36
    label "nauki_o_poznaniu"
  ]
  node [
    id 37
    label "nomotetyczny"
  ]
  node [
    id 38
    label "metodologia"
  ]
  node [
    id 39
    label "przem&#243;wienie"
  ]
  node [
    id 40
    label "wiedza"
  ]
  node [
    id 41
    label "kultura_duchowa"
  ]
  node [
    id 42
    label "nauki_penalne"
  ]
  node [
    id 43
    label "systematyka"
  ]
  node [
    id 44
    label "inwentyka"
  ]
  node [
    id 45
    label "dziedzina"
  ]
  node [
    id 46
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 47
    label "miasteczko_rowerowe"
  ]
  node [
    id 48
    label "fotowoltaika"
  ]
  node [
    id 49
    label "porada"
  ]
  node [
    id 50
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 51
    label "proces"
  ]
  node [
    id 52
    label "imagineskopia"
  ]
  node [
    id 53
    label "typologia"
  ]
  node [
    id 54
    label "&#322;awa_szkolna"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
]
