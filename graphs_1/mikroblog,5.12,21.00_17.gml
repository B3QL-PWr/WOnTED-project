graph [
  maxDegree 14
  minDegree 1
  meanDegree 1.9298245614035088
  density 0.03446115288220551
  graphCliqueNumber 2
  node [
    id 0
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 1
    label "&#380;ywo"
    origin "text"
  ]
  node [
    id 2
    label "jeszcze"
    origin "text"
  ]
  node [
    id 3
    label "dobrze"
    origin "text"
  ]
  node [
    id 4
    label "wygl&#261;da&#263;"
    origin "text"
  ]
  node [
    id 5
    label "by&#263;"
    origin "text"
  ]
  node [
    id 6
    label "fchuj"
    origin "text"
  ]
  node [
    id 7
    label "zadowolony"
    origin "text"
  ]
  node [
    id 8
    label "czyj&#347;"
  ]
  node [
    id 9
    label "m&#261;&#380;"
  ]
  node [
    id 10
    label "silnie"
  ]
  node [
    id 11
    label "wyra&#378;nie"
  ]
  node [
    id 12
    label "naturalnie"
  ]
  node [
    id 13
    label "ciekawie"
  ]
  node [
    id 14
    label "&#380;ywy"
  ]
  node [
    id 15
    label "zgrabnie"
  ]
  node [
    id 16
    label "szybko"
  ]
  node [
    id 17
    label "prawdziwie"
  ]
  node [
    id 18
    label "realistycznie"
  ]
  node [
    id 19
    label "nasycony"
  ]
  node [
    id 20
    label "energicznie"
  ]
  node [
    id 21
    label "g&#322;&#281;boko"
  ]
  node [
    id 22
    label "ci&#261;gle"
  ]
  node [
    id 23
    label "moralnie"
  ]
  node [
    id 24
    label "wiele"
  ]
  node [
    id 25
    label "lepiej"
  ]
  node [
    id 26
    label "korzystnie"
  ]
  node [
    id 27
    label "pomy&#347;lnie"
  ]
  node [
    id 28
    label "pozytywnie"
  ]
  node [
    id 29
    label "dobry"
  ]
  node [
    id 30
    label "dobroczynnie"
  ]
  node [
    id 31
    label "odpowiednio"
  ]
  node [
    id 32
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 33
    label "skutecznie"
  ]
  node [
    id 34
    label "czeka&#263;"
  ]
  node [
    id 35
    label "lookout"
  ]
  node [
    id 36
    label "wyziera&#263;"
  ]
  node [
    id 37
    label "peep"
  ]
  node [
    id 38
    label "look"
  ]
  node [
    id 39
    label "patrze&#263;"
  ]
  node [
    id 40
    label "si&#281;ga&#263;"
  ]
  node [
    id 41
    label "trwa&#263;"
  ]
  node [
    id 42
    label "obecno&#347;&#263;"
  ]
  node [
    id 43
    label "stan"
  ]
  node [
    id 44
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 45
    label "stand"
  ]
  node [
    id 46
    label "mie&#263;_miejsce"
  ]
  node [
    id 47
    label "uczestniczy&#263;"
  ]
  node [
    id 48
    label "chodzi&#263;"
  ]
  node [
    id 49
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 50
    label "equal"
  ]
  node [
    id 51
    label "u&#347;miechni&#281;ty"
  ]
  node [
    id 52
    label "usatysfakcjonowanie_si&#281;"
  ]
  node [
    id 53
    label "zadowolenie_si&#281;"
  ]
  node [
    id 54
    label "pogodny"
  ]
  node [
    id 55
    label "D"
  ]
  node [
    id 56
    label "ja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 55
    target 56
  ]
]
