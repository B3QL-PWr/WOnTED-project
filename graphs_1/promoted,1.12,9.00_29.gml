graph [
  maxDegree 36
  minDegree 1
  meanDegree 2
  density 0.013157894736842105
  graphCliqueNumber 2
  node [
    id 0
    label "zapad&#322;y"
    origin "text"
  ]
  node [
    id 1
    label "ostateczny"
    origin "text"
  ]
  node [
    id 2
    label "decyzja"
    origin "text"
  ]
  node [
    id 3
    label "inwestycyjny"
    origin "text"
  ]
  node [
    id 4
    label "powstanie"
    origin "text"
  ]
  node [
    id 5
    label "projekt"
    origin "text"
  ]
  node [
    id 6
    label "baltic"
    origin "text"
  ]
  node [
    id 7
    label "pipe"
    origin "text"
  ]
  node [
    id 8
    label "strona"
    origin "text"
  ]
  node [
    id 9
    label "rzeczpospolita"
    origin "text"
  ]
  node [
    id 10
    label "polski"
    origin "text"
  ]
  node [
    id 11
    label "kr&#243;lestwo"
    origin "text"
  ]
  node [
    id 12
    label "dania"
    origin "text"
  ]
  node [
    id 13
    label "daleki"
  ]
  node [
    id 14
    label "zniszczony"
  ]
  node [
    id 15
    label "wkl&#281;s&#322;y"
  ]
  node [
    id 16
    label "konieczny"
  ]
  node [
    id 17
    label "skrajny"
  ]
  node [
    id 18
    label "zupe&#322;ny"
  ]
  node [
    id 19
    label "ostatecznie"
  ]
  node [
    id 20
    label "dokument"
  ]
  node [
    id 21
    label "resolution"
  ]
  node [
    id 22
    label "zdecydowanie"
  ]
  node [
    id 23
    label "wytw&#243;r"
  ]
  node [
    id 24
    label "podj&#281;cie_decyzji"
  ]
  node [
    id 25
    label "management"
  ]
  node [
    id 26
    label "inwestycyjnie"
  ]
  node [
    id 27
    label "uniesienie_si&#281;"
  ]
  node [
    id 28
    label "geneza"
  ]
  node [
    id 29
    label "chmielnicczyzna"
  ]
  node [
    id 30
    label "beginning"
  ]
  node [
    id 31
    label "orgy"
  ]
  node [
    id 32
    label "Ko&#347;ciuszko"
  ]
  node [
    id 33
    label "sprzeciwienie_si&#281;"
  ]
  node [
    id 34
    label "utworzenie"
  ]
  node [
    id 35
    label "powstanie_listopadowe"
  ]
  node [
    id 36
    label "potworzenie_si&#281;"
  ]
  node [
    id 37
    label "powstanie_warszawskie"
  ]
  node [
    id 38
    label "kl&#281;czenie"
  ]
  node [
    id 39
    label "zbuntowanie_si&#281;"
  ]
  node [
    id 40
    label "siedzenie"
  ]
  node [
    id 41
    label "stworzenie"
  ]
  node [
    id 42
    label "walka"
  ]
  node [
    id 43
    label "zaistnienie"
  ]
  node [
    id 44
    label "odbudowanie_si&#281;"
  ]
  node [
    id 45
    label "pierwocina"
  ]
  node [
    id 46
    label "origin"
  ]
  node [
    id 47
    label "koliszczyzna"
  ]
  node [
    id 48
    label "powstanie_tambowskie"
  ]
  node [
    id 49
    label "&#380;akieria"
  ]
  node [
    id 50
    label "le&#380;enie"
  ]
  node [
    id 51
    label "device"
  ]
  node [
    id 52
    label "program_u&#380;ytkowy"
  ]
  node [
    id 53
    label "intencja"
  ]
  node [
    id 54
    label "agreement"
  ]
  node [
    id 55
    label "pomys&#322;"
  ]
  node [
    id 56
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 57
    label "plan"
  ]
  node [
    id 58
    label "dokumentacja"
  ]
  node [
    id 59
    label "skr&#281;canie"
  ]
  node [
    id 60
    label "voice"
  ]
  node [
    id 61
    label "forma"
  ]
  node [
    id 62
    label "internet"
  ]
  node [
    id 63
    label "skr&#281;ci&#263;"
  ]
  node [
    id 64
    label "kartka"
  ]
  node [
    id 65
    label "orientowa&#263;"
  ]
  node [
    id 66
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 67
    label "powierzchnia"
  ]
  node [
    id 68
    label "plik"
  ]
  node [
    id 69
    label "bok"
  ]
  node [
    id 70
    label "pagina"
  ]
  node [
    id 71
    label "orientowanie"
  ]
  node [
    id 72
    label "fragment"
  ]
  node [
    id 73
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 74
    label "s&#261;d"
  ]
  node [
    id 75
    label "skr&#281;ca&#263;"
  ]
  node [
    id 76
    label "g&#243;ra"
  ]
  node [
    id 77
    label "serwis_internetowy"
  ]
  node [
    id 78
    label "orientacja"
  ]
  node [
    id 79
    label "linia"
  ]
  node [
    id 80
    label "skr&#281;cenie"
  ]
  node [
    id 81
    label "layout"
  ]
  node [
    id 82
    label "zorientowa&#263;"
  ]
  node [
    id 83
    label "zorientowanie"
  ]
  node [
    id 84
    label "obiekt"
  ]
  node [
    id 85
    label "podmiot"
  ]
  node [
    id 86
    label "ty&#322;"
  ]
  node [
    id 87
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 88
    label "logowanie"
  ]
  node [
    id 89
    label "adres_internetowy"
  ]
  node [
    id 90
    label "uj&#281;cie"
  ]
  node [
    id 91
    label "prz&#243;d"
  ]
  node [
    id 92
    label "posta&#263;"
  ]
  node [
    id 93
    label "Buriacja"
  ]
  node [
    id 94
    label "Abchazja"
  ]
  node [
    id 95
    label "Inguszetia"
  ]
  node [
    id 96
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 97
    label "Nachiczewan"
  ]
  node [
    id 98
    label "Karaka&#322;pacja"
  ]
  node [
    id 99
    label "Jakucja"
  ]
  node [
    id 100
    label "Singapur"
  ]
  node [
    id 101
    label "Komi"
  ]
  node [
    id 102
    label "Karelia"
  ]
  node [
    id 103
    label "Tatarstan"
  ]
  node [
    id 104
    label "Chakasja"
  ]
  node [
    id 105
    label "Dagestan"
  ]
  node [
    id 106
    label "Mordowia"
  ]
  node [
    id 107
    label "Ka&#322;mucja"
  ]
  node [
    id 108
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 109
    label "Republika_Cisalpi&#324;ska"
  ]
  node [
    id 110
    label "Baszkiria"
  ]
  node [
    id 111
    label "pa&#324;stwo_demokratyczne"
  ]
  node [
    id 112
    label "Mari_El"
  ]
  node [
    id 113
    label "Ad&#380;aria"
  ]
  node [
    id 114
    label "Czuwaszja"
  ]
  node [
    id 115
    label "Tuwa"
  ]
  node [
    id 116
    label "Czeczenia"
  ]
  node [
    id 117
    label "Udmurcja"
  ]
  node [
    id 118
    label "pa&#324;stwo"
  ]
  node [
    id 119
    label "lacki"
  ]
  node [
    id 120
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 121
    label "przedmiot"
  ]
  node [
    id 122
    label "sztajer"
  ]
  node [
    id 123
    label "drabant"
  ]
  node [
    id 124
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 125
    label "polak"
  ]
  node [
    id 126
    label "pierogi_ruskie"
  ]
  node [
    id 127
    label "krakowiak"
  ]
  node [
    id 128
    label "Polish"
  ]
  node [
    id 129
    label "j&#281;zyk"
  ]
  node [
    id 130
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 131
    label "oberek"
  ]
  node [
    id 132
    label "po_polsku"
  ]
  node [
    id 133
    label "mazur"
  ]
  node [
    id 134
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 135
    label "chodzony"
  ]
  node [
    id 136
    label "skoczny"
  ]
  node [
    id 137
    label "ryba_po_grecku"
  ]
  node [
    id 138
    label "goniony"
  ]
  node [
    id 139
    label "polsko"
  ]
  node [
    id 140
    label "Arktogea"
  ]
  node [
    id 141
    label "kategoria_systematyczna"
  ]
  node [
    id 142
    label "domena"
  ]
  node [
    id 143
    label "protisty"
  ]
  node [
    id 144
    label "prokarioty"
  ]
  node [
    id 145
    label "grzyby"
  ]
  node [
    id 146
    label "ro&#347;liny"
  ]
  node [
    id 147
    label "terytorium"
  ]
  node [
    id 148
    label "Kr&#243;lestwo_Prus"
  ]
  node [
    id 149
    label "zwierz&#281;ta"
  ]
  node [
    id 150
    label "Baltic"
  ]
  node [
    id 151
    label "Pipe"
  ]
  node [
    id 152
    label "Dania"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 9
    target 100
  ]
  edge [
    source 9
    target 101
  ]
  edge [
    source 9
    target 102
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 104
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 9
    target 116
  ]
  edge [
    source 9
    target 117
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 10
    target 121
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 10
    target 125
  ]
  edge [
    source 10
    target 126
  ]
  edge [
    source 10
    target 127
  ]
  edge [
    source 10
    target 128
  ]
  edge [
    source 10
    target 129
  ]
  edge [
    source 10
    target 130
  ]
  edge [
    source 10
    target 131
  ]
  edge [
    source 10
    target 132
  ]
  edge [
    source 10
    target 133
  ]
  edge [
    source 10
    target 134
  ]
  edge [
    source 10
    target 135
  ]
  edge [
    source 10
    target 136
  ]
  edge [
    source 10
    target 137
  ]
  edge [
    source 10
    target 138
  ]
  edge [
    source 10
    target 139
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 140
  ]
  edge [
    source 11
    target 141
  ]
  edge [
    source 11
    target 142
  ]
  edge [
    source 11
    target 143
  ]
  edge [
    source 11
    target 144
  ]
  edge [
    source 11
    target 145
  ]
  edge [
    source 11
    target 146
  ]
  edge [
    source 11
    target 147
  ]
  edge [
    source 11
    target 148
  ]
  edge [
    source 11
    target 118
  ]
  edge [
    source 11
    target 149
  ]
  edge [
    source 11
    target 152
  ]
  edge [
    source 150
    target 151
  ]
]
