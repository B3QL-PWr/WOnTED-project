graph [
  maxDegree 20
  minDegree 1
  meanDegree 1.9714285714285715
  density 0.02857142857142857
  graphCliqueNumber 2
  node [
    id 0
    label "obl&#281;&#380;enie"
    origin "text"
  ]
  node [
    id 1
    label "sowiecki"
    origin "text"
  ]
  node [
    id 2
    label "doprowadzi&#263;"
    origin "text"
  ]
  node [
    id 3
    label "ogromny"
    origin "text"
  ]
  node [
    id 4
    label "zniszczenie"
    origin "text"
  ]
  node [
    id 5
    label "&#347;mier&#263;"
    origin "text"
  ]
  node [
    id 6
    label "ponad"
    origin "text"
  ]
  node [
    id 7
    label "tys"
    origin "text"
  ]
  node [
    id 8
    label "cywil"
    origin "text"
  ]
  node [
    id 9
    label "t&#322;ok"
  ]
  node [
    id 10
    label "inwazja"
  ]
  node [
    id 11
    label "investment"
  ]
  node [
    id 12
    label "socjalistyczny"
  ]
  node [
    id 13
    label "rosyjski"
  ]
  node [
    id 14
    label "po_sowiecku"
  ]
  node [
    id 15
    label "radziecki"
  ]
  node [
    id 16
    label "pozosta&#322;y"
  ]
  node [
    id 17
    label "poprowadzi&#263;"
  ]
  node [
    id 18
    label "spowodowa&#263;"
  ]
  node [
    id 19
    label "pos&#322;a&#263;"
  ]
  node [
    id 20
    label "have_a_bun_in_the_oven"
  ]
  node [
    id 21
    label "wykona&#263;"
  ]
  node [
    id 22
    label "wzbudzi&#263;"
  ]
  node [
    id 23
    label "wprowadzi&#263;"
  ]
  node [
    id 24
    label "set"
  ]
  node [
    id 25
    label "take"
  ]
  node [
    id 26
    label "carry"
  ]
  node [
    id 27
    label "olbrzymio"
  ]
  node [
    id 28
    label "wyj&#261;tkowy"
  ]
  node [
    id 29
    label "ogromnie"
  ]
  node [
    id 30
    label "znaczny"
  ]
  node [
    id 31
    label "jebitny"
  ]
  node [
    id 32
    label "prawdziwy"
  ]
  node [
    id 33
    label "wa&#380;ny"
  ]
  node [
    id 34
    label "liczny"
  ]
  node [
    id 35
    label "dono&#347;ny"
  ]
  node [
    id 36
    label "spl&#261;drowanie"
  ]
  node [
    id 37
    label "czynno&#347;&#263;"
  ]
  node [
    id 38
    label "spowodowanie"
  ]
  node [
    id 39
    label "zu&#380;ycie"
  ]
  node [
    id 40
    label "ruin"
  ]
  node [
    id 41
    label "poniszczenie"
  ]
  node [
    id 42
    label "podpalenie"
  ]
  node [
    id 43
    label "destruction"
  ]
  node [
    id 44
    label "os&#322;abienie"
  ]
  node [
    id 45
    label "zdrowie"
  ]
  node [
    id 46
    label "stanie_si&#281;"
  ]
  node [
    id 47
    label "attrition"
  ]
  node [
    id 48
    label "strata"
  ]
  node [
    id 49
    label "kondycja_fizyczna"
  ]
  node [
    id 50
    label "zaszkodzenie"
  ]
  node [
    id 51
    label "wear"
  ]
  node [
    id 52
    label "rezultat"
  ]
  node [
    id 53
    label "poniszczenie_si&#281;"
  ]
  node [
    id 54
    label "kres_&#380;ycia"
  ]
  node [
    id 55
    label "defenestracja"
  ]
  node [
    id 56
    label "kres"
  ]
  node [
    id 57
    label "agonia"
  ]
  node [
    id 58
    label "&#380;ycie"
  ]
  node [
    id 59
    label "szeol"
  ]
  node [
    id 60
    label "mogi&#322;a"
  ]
  node [
    id 61
    label "pogrzeb"
  ]
  node [
    id 62
    label "istota_nadprzyrodzona"
  ]
  node [
    id 63
    label "pogrzebanie"
  ]
  node [
    id 64
    label "&#380;a&#322;oba"
  ]
  node [
    id 65
    label "zabicie"
  ]
  node [
    id 66
    label "upadek"
  ]
  node [
    id 67
    label "&#347;lub"
  ]
  node [
    id 68
    label "stan"
  ]
  node [
    id 69
    label "obywatel"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
]
