graph [
  maxDegree 3
  minDegree 1
  meanDegree 1.5
  density 0.5
  graphCliqueNumber 2
  node [
    id 0
    label "wkr&#243;tce"
    origin "text"
  ]
  node [
    id 1
    label "wpr&#281;dce"
  ]
  node [
    id 2
    label "blisko"
  ]
  node [
    id 3
    label "nied&#322;ugi"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
]
