graph [
  maxDegree 7
  minDegree 1
  meanDegree 1.75
  density 0.25
  graphCliqueNumber 2
  node [
    id 0
    label "dzienia"
    origin "text"
  ]
  node [
    id 1
    label "noc"
    origin "text"
  ]
  node [
    id 2
    label "doba"
  ]
  node [
    id 3
    label "czas"
  ]
  node [
    id 4
    label "p&#243;&#322;noc"
  ]
  node [
    id 5
    label "nokturn"
  ]
  node [
    id 6
    label "zjawisko"
  ]
  node [
    id 7
    label "night"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
]
