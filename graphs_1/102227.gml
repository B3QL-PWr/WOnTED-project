graph [
  maxDegree 9
  minDegree 1
  meanDegree 1.76
  density 0.07333333333333333
  graphCliqueNumber 2
  node [
    id 0
    label "umowa"
    origin "text"
  ]
  node [
    id 1
    label "tym"
    origin "text"
  ]
  node [
    id 2
    label "za&#322;&#261;cznik"
    origin "text"
  ]
  node [
    id 3
    label "nast&#281;puj&#261;cy"
    origin "text"
  ]
  node [
    id 4
    label "protok&#243;&#322;"
    origin "text"
  ]
  node [
    id 5
    label "czyn"
  ]
  node [
    id 6
    label "contract"
  ]
  node [
    id 7
    label "gestia_transportowa"
  ]
  node [
    id 8
    label "zawrze&#263;"
  ]
  node [
    id 9
    label "klauzula"
  ]
  node [
    id 10
    label "porozumienie"
  ]
  node [
    id 11
    label "warunek"
  ]
  node [
    id 12
    label "zawarcie"
  ]
  node [
    id 13
    label "dodatek"
  ]
  node [
    id 14
    label "okre&#347;lony"
  ]
  node [
    id 15
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 16
    label "komunikacja_zintegrowana"
  ]
  node [
    id 17
    label "akt"
  ]
  node [
    id 18
    label "relacja"
  ]
  node [
    id 19
    label "etykieta"
  ]
  node [
    id 20
    label "zasada"
  ]
  node [
    id 21
    label "republika"
  ]
  node [
    id 22
    label "Armenia"
  ]
  node [
    id 23
    label "rada"
  ]
  node [
    id 24
    label "europ"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 23
    target 24
  ]
]
