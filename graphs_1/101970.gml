graph [
  maxDegree 48
  minDegree 1
  meanDegree 2.2634032634032635
  density 0.0026410773201905055
  graphCliqueNumber 3
  node [
    id 0
    label "zaistnia&#322;y"
    origin "text"
  ]
  node [
    id 1
    label "okoliczno&#347;&#263;"
    origin "text"
  ]
  node [
    id 2
    label "nieodparcie"
    origin "text"
  ]
  node [
    id 3
    label "prowokowa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "przegl&#261;d"
    origin "text"
  ]
  node [
    id 5
    label "dzie&#322;o"
    origin "text"
  ]
  node [
    id 6
    label "inny"
    origin "text"
  ]
  node [
    id 7
    label "noblista"
    origin "text"
  ]
  node [
    id 8
    label "chodzi&#263;"
    origin "text"
  ]
  node [
    id 9
    label "bynajmniej"
    origin "text"
  ]
  node [
    id 10
    label "polityczny"
    origin "text"
  ]
  node [
    id 11
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 12
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 13
    label "dawny"
    origin "text"
  ]
  node [
    id 14
    label "kojarzy&#263;"
    origin "text"
  ]
  node [
    id 15
    label "si&#281;"
    origin "text"
  ]
  node [
    id 16
    label "tytu&#322;"
    origin "text"
  ]
  node [
    id 17
    label "ha&#324;ba"
    origin "text"
  ]
  node [
    id 18
    label "przyroda"
    origin "text"
  ]
  node [
    id 19
    label "&#347;nieg"
    origin "text"
  ]
  node [
    id 20
    label "ujawni&#263;"
    origin "text"
  ]
  node [
    id 21
    label "chyba"
    origin "text"
  ]
  node [
    id 22
    label "wielki"
    origin "text"
  ]
  node [
    id 23
    label "tajemnica"
    origin "text"
  ]
  node [
    id 24
    label "je&#347;li"
    origin "text"
  ]
  node [
    id 25
    label "powiedzie&#263;"
    origin "text"
  ]
  node [
    id 26
    label "powie&#347;&#263;"
    origin "text"
  ]
  node [
    id 27
    label "ten"
    origin "text"
  ]
  node [
    id 28
    label "mama"
    origin "text"
  ]
  node [
    id 29
    label "czynienie"
    origin "text"
  ]
  node [
    id 30
    label "tytu&#322;owy"
    origin "text"
  ]
  node [
    id 31
    label "opad"
    origin "text"
  ]
  node [
    id 32
    label "atmosferyczny"
    origin "text"
  ]
  node [
    id 33
    label "niewinny"
    origin "text"
  ]
  node [
    id 34
    label "stan"
    origin "text"
  ]
  node [
    id 35
    label "t&#322;o"
    origin "text"
  ]
  node [
    id 36
    label "dla"
    origin "text"
  ]
  node [
    id 37
    label "wydarzenia"
    origin "text"
  ]
  node [
    id 38
    label "tak"
    origin "text"
  ]
  node [
    id 39
    label "ma&#322;o"
    origin "text"
  ]
  node [
    id 40
    label "jak"
    origin "text"
  ]
  node [
    id 41
    label "krwawy"
    origin "text"
  ]
  node [
    id 42
    label "prowokacja"
    origin "text"
  ]
  node [
    id 43
    label "przeobra&#380;a&#263;"
    origin "text"
  ]
  node [
    id 44
    label "przeci&#281;tna"
    origin "text"
  ]
  node [
    id 45
    label "pok&#243;j"
    origin "text"
  ]
  node [
    id 46
    label "mi&#322;owa&#263;"
    origin "text"
  ]
  node [
    id 47
    label "obywatel"
    origin "text"
  ]
  node [
    id 48
    label "tch&#243;rzliwy"
    origin "text"
  ]
  node [
    id 49
    label "fanatyk"
    origin "text"
  ]
  node [
    id 50
    label "antyczny"
    origin "text"
  ]
  node [
    id 51
    label "konflikt"
    origin "text"
  ]
  node [
    id 52
    label "uniemo&#380;liwia&#263;"
    origin "text"
  ]
  node [
    id 53
    label "osi&#261;gni&#281;cie"
    origin "text"
  ]
  node [
    id 54
    label "mi&#322;osny"
    origin "text"
  ]
  node [
    id 55
    label "szcz&#281;&#347;cie"
    origin "text"
  ]
  node [
    id 56
    label "oczywi&#347;cie"
    origin "text"
  ]
  node [
    id 57
    label "wszystko"
    origin "text"
  ]
  node [
    id 58
    label "uproszczenie"
    origin "text"
  ]
  node [
    id 59
    label "bowiem"
    origin "text"
  ]
  node [
    id 60
    label "pan"
    origin "text"
  ]
  node [
    id 61
    label "pamuk"
    origin "text"
  ]
  node [
    id 62
    label "pe&#322;ny"
    origin "text"
  ]
  node [
    id 63
    label "rezygnacja"
    origin "text"
  ]
  node [
    id 64
    label "melancholia"
    origin "text"
  ]
  node [
    id 65
    label "opisywa&#263;"
    origin "text"
  ]
  node [
    id 66
    label "prosta"
    origin "text"
  ]
  node [
    id 67
    label "sum"
    origin "text"
  ]
  node [
    id 68
    label "historia"
    origin "text"
  ]
  node [
    id 69
    label "ca&#322;y"
    origin "text"
  ]
  node [
    id 70
    label "bogactwo"
    origin "text"
  ]
  node [
    id 71
    label "sk&#322;ada&#263;"
    origin "text"
  ]
  node [
    id 72
    label "motyw"
    origin "text"
  ]
  node [
    id 73
    label "b&#322;&#261;d"
    origin "text"
  ]
  node [
    id 74
    label "zwyci&#281;stwo"
    origin "text"
  ]
  node [
    id 75
    label "pora&#380;ka"
    origin "text"
  ]
  node [
    id 76
    label "du&#380;o"
    origin "text"
  ]
  node [
    id 77
    label "udawa&#263;"
    origin "text"
  ]
  node [
    id 78
    label "utrzyma&#263;"
    origin "text"
  ]
  node [
    id 79
    label "przy"
    origin "text"
  ]
  node [
    id 80
    label "tym"
    origin "text"
  ]
  node [
    id 81
    label "zainteresowanie"
    origin "text"
  ]
  node [
    id 82
    label "osoba"
    origin "text"
  ]
  node [
    id 83
    label "czyta&#263;"
    origin "text"
  ]
  node [
    id 84
    label "nikt"
    origin "text"
  ]
  node [
    id 85
    label "by&#263;"
    origin "text"
  ]
  node [
    id 86
    label "g&#243;ra"
    origin "text"
  ]
  node [
    id 87
    label "nic"
    origin "text"
  ]
  node [
    id 88
    label "te&#380;"
    origin "text"
  ]
  node [
    id 89
    label "koniec"
    origin "text"
  ]
  node [
    id 90
    label "usprawiedliwi&#263;"
    origin "text"
  ]
  node [
    id 91
    label "niewinno&#347;&#263;"
    origin "text"
  ]
  node [
    id 92
    label "intencja"
    origin "text"
  ]
  node [
    id 93
    label "przykrywka"
    origin "text"
  ]
  node [
    id 94
    label "tymczasowo"
    origin "text"
  ]
  node [
    id 95
    label "zas&#322;a&#263;"
    origin "text"
  ]
  node [
    id 96
    label "ale"
    origin "text"
  ]
  node [
    id 97
    label "stopnie&#263;"
    origin "text"
  ]
  node [
    id 98
    label "przesta&#263;"
    origin "text"
  ]
  node [
    id 99
    label "liczy&#263;"
    origin "text"
  ]
  node [
    id 100
    label "bardzo"
    origin "text"
  ]
  node [
    id 101
    label "fajny"
    origin "text"
  ]
  node [
    id 102
    label "ksi&#261;&#380;ek"
    origin "text"
  ]
  node [
    id 103
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 104
    label "wszyscy"
    origin "text"
  ]
  node [
    id 105
    label "raz"
    origin "text"
  ]
  node [
    id 106
    label "chcie&#263;"
    origin "text"
  ]
  node [
    id 107
    label "mie&#263;"
    origin "text"
  ]
  node [
    id 108
    label "racja"
    origin "text"
  ]
  node [
    id 109
    label "powinny"
    origin "text"
  ]
  node [
    id 110
    label "&#380;y&#263;"
    origin "text"
  ]
  node [
    id 111
    label "kilka"
    origin "text"
  ]
  node [
    id 112
    label "wcale"
    origin "text"
  ]
  node [
    id 113
    label "taki"
    origin "text"
  ]
  node [
    id 114
    label "genialny"
    origin "text"
  ]
  node [
    id 115
    label "aktualny"
  ]
  node [
    id 116
    label "wydarzenie"
  ]
  node [
    id 117
    label "sk&#322;adnik"
  ]
  node [
    id 118
    label "warunki"
  ]
  node [
    id 119
    label "sytuacja"
  ]
  node [
    id 120
    label "niepodwa&#380;alnie"
  ]
  node [
    id 121
    label "nieodparty"
  ]
  node [
    id 122
    label "przekonuj&#261;co"
  ]
  node [
    id 123
    label "bezspornie"
  ]
  node [
    id 124
    label "nieprzezwyci&#281;&#380;enie"
  ]
  node [
    id 125
    label "robi&#263;"
  ]
  node [
    id 126
    label "powodowa&#263;"
  ]
  node [
    id 127
    label "prompt"
  ]
  node [
    id 128
    label "impreza"
  ]
  node [
    id 129
    label "kontrola"
  ]
  node [
    id 130
    label "inspection"
  ]
  node [
    id 131
    label "creation"
  ]
  node [
    id 132
    label "forma"
  ]
  node [
    id 133
    label "retrospektywa"
  ]
  node [
    id 134
    label "tre&#347;&#263;"
  ]
  node [
    id 135
    label "tetralogia"
  ]
  node [
    id 136
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 137
    label "dorobek"
  ]
  node [
    id 138
    label "obrazowanie"
  ]
  node [
    id 139
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 140
    label "komunikat"
  ]
  node [
    id 141
    label "praca"
  ]
  node [
    id 142
    label "works"
  ]
  node [
    id 143
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 144
    label "tekst"
  ]
  node [
    id 145
    label "kolejny"
  ]
  node [
    id 146
    label "inaczej"
  ]
  node [
    id 147
    label "r&#243;&#380;ny"
  ]
  node [
    id 148
    label "inszy"
  ]
  node [
    id 149
    label "osobno"
  ]
  node [
    id 150
    label "Arafat"
  ]
  node [
    id 151
    label "Mi&#322;osz"
  ]
  node [
    id 152
    label "Bergson"
  ]
  node [
    id 153
    label "Fo"
  ]
  node [
    id 154
    label "Reymont"
  ]
  node [
    id 155
    label "laureat"
  ]
  node [
    id 156
    label "Sienkiewicz"
  ]
  node [
    id 157
    label "Wa&#322;&#281;sa"
  ]
  node [
    id 158
    label "Gorbaczow"
  ]
  node [
    id 159
    label "Einstein"
  ]
  node [
    id 160
    label "Winston_Churchill"
  ]
  node [
    id 161
    label "proceed"
  ]
  node [
    id 162
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 163
    label "bangla&#263;"
  ]
  node [
    id 164
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 165
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 166
    label "run"
  ]
  node [
    id 167
    label "tryb"
  ]
  node [
    id 168
    label "p&#322;ywa&#263;"
  ]
  node [
    id 169
    label "continue"
  ]
  node [
    id 170
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 171
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 172
    label "przebiega&#263;"
  ]
  node [
    id 173
    label "mie&#263;_miejsce"
  ]
  node [
    id 174
    label "wk&#322;ada&#263;"
  ]
  node [
    id 175
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 176
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 177
    label "para"
  ]
  node [
    id 178
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 179
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 180
    label "krok"
  ]
  node [
    id 181
    label "str&#243;j"
  ]
  node [
    id 182
    label "bywa&#263;"
  ]
  node [
    id 183
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 184
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 185
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 186
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 187
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 188
    label "dziama&#263;"
  ]
  node [
    id 189
    label "stara&#263;_si&#281;"
  ]
  node [
    id 190
    label "carry"
  ]
  node [
    id 191
    label "internowanie"
  ]
  node [
    id 192
    label "prorz&#261;dowy"
  ]
  node [
    id 193
    label "wi&#281;zie&#324;"
  ]
  node [
    id 194
    label "politycznie"
  ]
  node [
    id 195
    label "internowa&#263;"
  ]
  node [
    id 196
    label "ideologiczny"
  ]
  node [
    id 197
    label "przesz&#322;y"
  ]
  node [
    id 198
    label "dawno"
  ]
  node [
    id 199
    label "dawniej"
  ]
  node [
    id 200
    label "kombatant"
  ]
  node [
    id 201
    label "stary"
  ]
  node [
    id 202
    label "odleg&#322;y"
  ]
  node [
    id 203
    label "anachroniczny"
  ]
  node [
    id 204
    label "przestarza&#322;y"
  ]
  node [
    id 205
    label "od_dawna"
  ]
  node [
    id 206
    label "poprzedni"
  ]
  node [
    id 207
    label "d&#322;ugoletni"
  ]
  node [
    id 208
    label "wcze&#347;niejszy"
  ]
  node [
    id 209
    label "niegdysiejszy"
  ]
  node [
    id 210
    label "cover"
  ]
  node [
    id 211
    label "rozumie&#263;"
  ]
  node [
    id 212
    label "zaskakiwa&#263;"
  ]
  node [
    id 213
    label "swat"
  ]
  node [
    id 214
    label "relate"
  ]
  node [
    id 215
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 216
    label "podtytu&#322;"
  ]
  node [
    id 217
    label "debit"
  ]
  node [
    id 218
    label "szata_graficzna"
  ]
  node [
    id 219
    label "elevation"
  ]
  node [
    id 220
    label "wyda&#263;"
  ]
  node [
    id 221
    label "nadtytu&#322;"
  ]
  node [
    id 222
    label "tytulatura"
  ]
  node [
    id 223
    label "nazwa"
  ]
  node [
    id 224
    label "wydawa&#263;"
  ]
  node [
    id 225
    label "redaktor"
  ]
  node [
    id 226
    label "druk"
  ]
  node [
    id 227
    label "mianowaniec"
  ]
  node [
    id 228
    label "poster"
  ]
  node [
    id 229
    label "publikacja"
  ]
  node [
    id 230
    label "upokorzenie"
  ]
  node [
    id 231
    label "shame"
  ]
  node [
    id 232
    label "wstyd"
  ]
  node [
    id 233
    label "biota"
  ]
  node [
    id 234
    label "wszechstworzenie"
  ]
  node [
    id 235
    label "obiekt_naturalny"
  ]
  node [
    id 236
    label "Ziemia"
  ]
  node [
    id 237
    label "fauna"
  ]
  node [
    id 238
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 239
    label "przedmiot"
  ]
  node [
    id 240
    label "stw&#243;r"
  ]
  node [
    id 241
    label "ekosystem"
  ]
  node [
    id 242
    label "rzecz"
  ]
  node [
    id 243
    label "teren"
  ]
  node [
    id 244
    label "environment"
  ]
  node [
    id 245
    label "woda"
  ]
  node [
    id 246
    label "przyra"
  ]
  node [
    id 247
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 248
    label "mikrokosmos"
  ]
  node [
    id 249
    label "sypni&#281;cie"
  ]
  node [
    id 250
    label "pow&#322;oka"
  ]
  node [
    id 251
    label "sypn&#261;&#263;"
  ]
  node [
    id 252
    label "rakieta"
  ]
  node [
    id 253
    label "kokaina"
  ]
  node [
    id 254
    label "dostrzec"
  ]
  node [
    id 255
    label "denounce"
  ]
  node [
    id 256
    label "discover"
  ]
  node [
    id 257
    label "objawi&#263;"
  ]
  node [
    id 258
    label "poinformowa&#263;"
  ]
  node [
    id 259
    label "dupny"
  ]
  node [
    id 260
    label "wysoce"
  ]
  node [
    id 261
    label "wyj&#261;tkowy"
  ]
  node [
    id 262
    label "wybitny"
  ]
  node [
    id 263
    label "znaczny"
  ]
  node [
    id 264
    label "prawdziwy"
  ]
  node [
    id 265
    label "wa&#380;ny"
  ]
  node [
    id 266
    label "nieprzeci&#281;tny"
  ]
  node [
    id 267
    label "dyskrecja"
  ]
  node [
    id 268
    label "secret"
  ]
  node [
    id 269
    label "zachowa&#263;"
  ]
  node [
    id 270
    label "zachowanie"
  ]
  node [
    id 271
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 272
    label "zachowywa&#263;"
  ]
  node [
    id 273
    label "wiedza"
  ]
  node [
    id 274
    label "informacja"
  ]
  node [
    id 275
    label "enigmat"
  ]
  node [
    id 276
    label "zachowywanie"
  ]
  node [
    id 277
    label "wypaplanie"
  ]
  node [
    id 278
    label "taj&#324;"
  ]
  node [
    id 279
    label "spos&#243;b"
  ]
  node [
    id 280
    label "obowi&#261;zek"
  ]
  node [
    id 281
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 282
    label "express"
  ]
  node [
    id 283
    label "rzekn&#261;&#263;"
  ]
  node [
    id 284
    label "okre&#347;li&#263;"
  ]
  node [
    id 285
    label "wyrazi&#263;"
  ]
  node [
    id 286
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 287
    label "unwrap"
  ]
  node [
    id 288
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 289
    label "convey"
  ]
  node [
    id 290
    label "wydoby&#263;"
  ]
  node [
    id 291
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 292
    label "poda&#263;"
  ]
  node [
    id 293
    label "moderate"
  ]
  node [
    id 294
    label "powie&#347;&#263;_cykliczna"
  ]
  node [
    id 295
    label "proza"
  ]
  node [
    id 296
    label "gatunek_literacki"
  ]
  node [
    id 297
    label "utw&#243;r_epicki"
  ]
  node [
    id 298
    label "doprowadzi&#263;"
  ]
  node [
    id 299
    label "marynistyczny"
  ]
  node [
    id 300
    label "okre&#347;lony"
  ]
  node [
    id 301
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 302
    label "matczysko"
  ]
  node [
    id 303
    label "macierz"
  ]
  node [
    id 304
    label "przodkini"
  ]
  node [
    id 305
    label "Matka_Boska"
  ]
  node [
    id 306
    label "macocha"
  ]
  node [
    id 307
    label "matka_zast&#281;pcza"
  ]
  node [
    id 308
    label "stara"
  ]
  node [
    id 309
    label "rodzice"
  ]
  node [
    id 310
    label "rodzic"
  ]
  node [
    id 311
    label "robienie"
  ]
  node [
    id 312
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 313
    label "porobienie"
  ]
  node [
    id 314
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 315
    label "act"
  ]
  node [
    id 316
    label "campaign"
  ]
  node [
    id 317
    label "m&#281;czenie_si&#281;"
  ]
  node [
    id 318
    label "g&#322;&#243;wny"
  ]
  node [
    id 319
    label "zjawisko"
  ]
  node [
    id 320
    label "zawalny"
  ]
  node [
    id 321
    label "pluwia&#322;"
  ]
  node [
    id 322
    label "&#263;wiczenie"
  ]
  node [
    id 323
    label "substancja"
  ]
  node [
    id 324
    label "fall"
  ]
  node [
    id 325
    label "nimbus"
  ]
  node [
    id 326
    label "g&#322;upi"
  ]
  node [
    id 327
    label "uniewinnianie"
  ]
  node [
    id 328
    label "uniewinnia&#263;"
  ]
  node [
    id 329
    label "uniewinnienie"
  ]
  node [
    id 330
    label "dobry"
  ]
  node [
    id 331
    label "bezpieczny"
  ]
  node [
    id 332
    label "uniewinni&#263;"
  ]
  node [
    id 333
    label "niewinnie"
  ]
  node [
    id 334
    label "Arizona"
  ]
  node [
    id 335
    label "Georgia"
  ]
  node [
    id 336
    label "warstwa"
  ]
  node [
    id 337
    label "jednostka_administracyjna"
  ]
  node [
    id 338
    label "Hawaje"
  ]
  node [
    id 339
    label "Goa"
  ]
  node [
    id 340
    label "Floryda"
  ]
  node [
    id 341
    label "Oklahoma"
  ]
  node [
    id 342
    label "punkt"
  ]
  node [
    id 343
    label "Alaska"
  ]
  node [
    id 344
    label "wci&#281;cie"
  ]
  node [
    id 345
    label "Alabama"
  ]
  node [
    id 346
    label "Oregon"
  ]
  node [
    id 347
    label "poziom"
  ]
  node [
    id 348
    label "Teksas"
  ]
  node [
    id 349
    label "Illinois"
  ]
  node [
    id 350
    label "Waszyngton"
  ]
  node [
    id 351
    label "Jukatan"
  ]
  node [
    id 352
    label "shape"
  ]
  node [
    id 353
    label "Nowy_Meksyk"
  ]
  node [
    id 354
    label "ilo&#347;&#263;"
  ]
  node [
    id 355
    label "state"
  ]
  node [
    id 356
    label "Nowy_York"
  ]
  node [
    id 357
    label "Arakan"
  ]
  node [
    id 358
    label "Kalifornia"
  ]
  node [
    id 359
    label "wektor"
  ]
  node [
    id 360
    label "Massachusetts"
  ]
  node [
    id 361
    label "miejsce"
  ]
  node [
    id 362
    label "Pensylwania"
  ]
  node [
    id 363
    label "Michigan"
  ]
  node [
    id 364
    label "Maryland"
  ]
  node [
    id 365
    label "Ohio"
  ]
  node [
    id 366
    label "Kansas"
  ]
  node [
    id 367
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 368
    label "Luizjana"
  ]
  node [
    id 369
    label "samopoczucie"
  ]
  node [
    id 370
    label "Wirginia"
  ]
  node [
    id 371
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 372
    label "melodia"
  ]
  node [
    id 373
    label "partia"
  ]
  node [
    id 374
    label "&#347;rodowisko"
  ]
  node [
    id 375
    label "obiekt"
  ]
  node [
    id 376
    label "ubarwienie"
  ]
  node [
    id 377
    label "lingwistyka_kognitywna"
  ]
  node [
    id 378
    label "gestaltyzm"
  ]
  node [
    id 379
    label "obraz"
  ]
  node [
    id 380
    label "pod&#322;o&#380;e"
  ]
  node [
    id 381
    label "causal_agent"
  ]
  node [
    id 382
    label "dalszoplanowy"
  ]
  node [
    id 383
    label "layer"
  ]
  node [
    id 384
    label "plan"
  ]
  node [
    id 385
    label "background"
  ]
  node [
    id 386
    label "p&#322;aszczyzna"
  ]
  node [
    id 387
    label "mo&#380;liwie"
  ]
  node [
    id 388
    label "nieznaczny"
  ]
  node [
    id 389
    label "kr&#243;tko"
  ]
  node [
    id 390
    label "nieistotnie"
  ]
  node [
    id 391
    label "nieliczny"
  ]
  node [
    id 392
    label "mikroskopijnie"
  ]
  node [
    id 393
    label "pomiernie"
  ]
  node [
    id 394
    label "ma&#322;y"
  ]
  node [
    id 395
    label "byd&#322;o"
  ]
  node [
    id 396
    label "zobo"
  ]
  node [
    id 397
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 398
    label "yakalo"
  ]
  node [
    id 399
    label "dzo"
  ]
  node [
    id 400
    label "czerwony"
  ]
  node [
    id 401
    label "nabieg&#322;y"
  ]
  node [
    id 402
    label "kator&#380;niczy"
  ]
  node [
    id 403
    label "krwawo"
  ]
  node [
    id 404
    label "okrutny"
  ]
  node [
    id 405
    label "naturalistyczny"
  ]
  node [
    id 406
    label "krwistoczerwono"
  ]
  node [
    id 407
    label "tragiczny"
  ]
  node [
    id 408
    label "bolesny"
  ]
  node [
    id 409
    label "mocny"
  ]
  node [
    id 410
    label "za&#380;arty"
  ]
  node [
    id 411
    label "challenge"
  ]
  node [
    id 412
    label "fortel"
  ]
  node [
    id 413
    label "akcja"
  ]
  node [
    id 414
    label "podst&#281;p"
  ]
  node [
    id 415
    label "przekszta&#322;ca&#263;"
  ]
  node [
    id 416
    label "warto&#347;&#263;"
  ]
  node [
    id 417
    label "uk&#322;ad"
  ]
  node [
    id 418
    label "spok&#243;j"
  ]
  node [
    id 419
    label "preliminarium_pokojowe"
  ]
  node [
    id 420
    label "grupa"
  ]
  node [
    id 421
    label "mir"
  ]
  node [
    id 422
    label "pacyfista"
  ]
  node [
    id 423
    label "pomieszczenie"
  ]
  node [
    id 424
    label "kocha&#263;"
  ]
  node [
    id 425
    label "przedstawiciel"
  ]
  node [
    id 426
    label "miastowy"
  ]
  node [
    id 427
    label "mieszkaniec"
  ]
  node [
    id 428
    label "pa&#324;stwo"
  ]
  node [
    id 429
    label "tch&#243;rzowsko"
  ]
  node [
    id 430
    label "strachliwy"
  ]
  node [
    id 431
    label "zwolennik"
  ]
  node [
    id 432
    label "oszo&#322;om"
  ]
  node [
    id 433
    label "pasjonat"
  ]
  node [
    id 434
    label "radyka&#322;"
  ]
  node [
    id 435
    label "obsesjonista"
  ]
  node [
    id 436
    label "zabytkowy"
  ]
  node [
    id 437
    label "klasyczny"
  ]
  node [
    id 438
    label "armilla"
  ]
  node [
    id 439
    label "antycznie"
  ]
  node [
    id 440
    label "staro&#380;ytny"
  ]
  node [
    id 441
    label "clash"
  ]
  node [
    id 442
    label "przeciwstawno&#347;&#263;"
  ]
  node [
    id 443
    label "przeszkadza&#263;"
  ]
  node [
    id 444
    label "anticipate"
  ]
  node [
    id 445
    label "zas&#322;u&#380;ony"
  ]
  node [
    id 446
    label "dotarcie"
  ]
  node [
    id 447
    label "sukces"
  ]
  node [
    id 448
    label "dochrapanie_si&#281;"
  ]
  node [
    id 449
    label "skill"
  ]
  node [
    id 450
    label "accomplishment"
  ]
  node [
    id 451
    label "zdarzenie_si&#281;"
  ]
  node [
    id 452
    label "uzyskanie"
  ]
  node [
    id 453
    label "zaawansowanie"
  ]
  node [
    id 454
    label "sercowy"
  ]
  node [
    id 455
    label "mi&#322;o&#347;ny"
  ]
  node [
    id 456
    label "mi&#322;o&#347;nie"
  ]
  node [
    id 457
    label "czu&#322;y"
  ]
  node [
    id 458
    label "erotycznie"
  ]
  node [
    id 459
    label "wra&#380;enie"
  ]
  node [
    id 460
    label "przeznaczenie"
  ]
  node [
    id 461
    label "dobrodziejstwo"
  ]
  node [
    id 462
    label "dobro"
  ]
  node [
    id 463
    label "przypadek"
  ]
  node [
    id 464
    label "lock"
  ]
  node [
    id 465
    label "absolut"
  ]
  node [
    id 466
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 467
    label "reduction"
  ]
  node [
    id 468
    label "u&#322;atwienie"
  ]
  node [
    id 469
    label "ulepszenie"
  ]
  node [
    id 470
    label "zdeformowanie"
  ]
  node [
    id 471
    label "prostszy"
  ]
  node [
    id 472
    label "simplification"
  ]
  node [
    id 473
    label "rezultat"
  ]
  node [
    id 474
    label "profesor"
  ]
  node [
    id 475
    label "kszta&#322;ciciel"
  ]
  node [
    id 476
    label "jegomo&#347;&#263;"
  ]
  node [
    id 477
    label "zwrot"
  ]
  node [
    id 478
    label "pracodawca"
  ]
  node [
    id 479
    label "rz&#261;dzenie"
  ]
  node [
    id 480
    label "m&#261;&#380;"
  ]
  node [
    id 481
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 482
    label "ch&#322;opina"
  ]
  node [
    id 483
    label "bratek"
  ]
  node [
    id 484
    label "opiekun"
  ]
  node [
    id 485
    label "doros&#322;y"
  ]
  node [
    id 486
    label "preceptor"
  ]
  node [
    id 487
    label "Midas"
  ]
  node [
    id 488
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 489
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 490
    label "murza"
  ]
  node [
    id 491
    label "ojciec"
  ]
  node [
    id 492
    label "androlog"
  ]
  node [
    id 493
    label "pupil"
  ]
  node [
    id 494
    label "efendi"
  ]
  node [
    id 495
    label "nabab"
  ]
  node [
    id 496
    label "w&#322;odarz"
  ]
  node [
    id 497
    label "szkolnik"
  ]
  node [
    id 498
    label "pedagog"
  ]
  node [
    id 499
    label "popularyzator"
  ]
  node [
    id 500
    label "andropauza"
  ]
  node [
    id 501
    label "gra_w_karty"
  ]
  node [
    id 502
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 503
    label "Mieszko_I"
  ]
  node [
    id 504
    label "bogaty"
  ]
  node [
    id 505
    label "samiec"
  ]
  node [
    id 506
    label "przyw&#243;dca"
  ]
  node [
    id 507
    label "belfer"
  ]
  node [
    id 508
    label "szcz&#281;&#347;liwy"
  ]
  node [
    id 509
    label "nieograniczony"
  ]
  node [
    id 510
    label "nape&#322;nienie_si&#281;"
  ]
  node [
    id 511
    label "kompletny"
  ]
  node [
    id 512
    label "r&#243;wny"
  ]
  node [
    id 513
    label "wype&#322;nianie_si&#281;"
  ]
  node [
    id 514
    label "bezwzgl&#281;dny"
  ]
  node [
    id 515
    label "zupe&#322;ny"
  ]
  node [
    id 516
    label "satysfakcja"
  ]
  node [
    id 517
    label "wszechogarniaj&#261;cy"
  ]
  node [
    id 518
    label "pe&#322;no"
  ]
  node [
    id 519
    label "wype&#322;nienie"
  ]
  node [
    id 520
    label "otwarty"
  ]
  node [
    id 521
    label "orzeczenie"
  ]
  node [
    id 522
    label "submission"
  ]
  node [
    id 523
    label "smutek"
  ]
  node [
    id 524
    label "zniech&#281;cenie"
  ]
  node [
    id 525
    label "decyzja"
  ]
  node [
    id 526
    label "zaduma"
  ]
  node [
    id 527
    label "za&#322;amanie"
  ]
  node [
    id 528
    label "zapoznawa&#263;"
  ]
  node [
    id 529
    label "represent"
  ]
  node [
    id 530
    label "czas"
  ]
  node [
    id 531
    label "odcinek"
  ]
  node [
    id 532
    label "proste_sko&#347;ne"
  ]
  node [
    id 533
    label "straight_line"
  ]
  node [
    id 534
    label "trasa"
  ]
  node [
    id 535
    label "krzywa"
  ]
  node [
    id 536
    label "sumowate"
  ]
  node [
    id 537
    label "Uzbekistan"
  ]
  node [
    id 538
    label "catfish"
  ]
  node [
    id 539
    label "ryba"
  ]
  node [
    id 540
    label "jednostka_monetarna"
  ]
  node [
    id 541
    label "report"
  ]
  node [
    id 542
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 543
    label "wypowied&#378;"
  ]
  node [
    id 544
    label "neografia"
  ]
  node [
    id 545
    label "papirologia"
  ]
  node [
    id 546
    label "historia_gospodarcza"
  ]
  node [
    id 547
    label "przebiec"
  ]
  node [
    id 548
    label "hista"
  ]
  node [
    id 549
    label "nauka_humanistyczna"
  ]
  node [
    id 550
    label "filigranistyka"
  ]
  node [
    id 551
    label "dyplomatyka"
  ]
  node [
    id 552
    label "annalistyka"
  ]
  node [
    id 553
    label "historyka"
  ]
  node [
    id 554
    label "heraldyka"
  ]
  node [
    id 555
    label "fabu&#322;a"
  ]
  node [
    id 556
    label "muzealnictwo"
  ]
  node [
    id 557
    label "varsavianistyka"
  ]
  node [
    id 558
    label "prezentyzm"
  ]
  node [
    id 559
    label "mediewistyka"
  ]
  node [
    id 560
    label "przebiegni&#281;cie"
  ]
  node [
    id 561
    label "charakter"
  ]
  node [
    id 562
    label "paleografia"
  ]
  node [
    id 563
    label "genealogia"
  ]
  node [
    id 564
    label "czynno&#347;&#263;"
  ]
  node [
    id 565
    label "prozopografia"
  ]
  node [
    id 566
    label "nautologia"
  ]
  node [
    id 567
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 568
    label "epoka"
  ]
  node [
    id 569
    label "numizmatyka"
  ]
  node [
    id 570
    label "ruralistyka"
  ]
  node [
    id 571
    label "epigrafika"
  ]
  node [
    id 572
    label "&#378;r&#243;d&#322;oznawstwo"
  ]
  node [
    id 573
    label "historiografia"
  ]
  node [
    id 574
    label "bizantynistyka"
  ]
  node [
    id 575
    label "weksylologia"
  ]
  node [
    id 576
    label "kierunek"
  ]
  node [
    id 577
    label "ikonografia"
  ]
  node [
    id 578
    label "chronologia"
  ]
  node [
    id 579
    label "archiwistyka"
  ]
  node [
    id 580
    label "sfragistyka"
  ]
  node [
    id 581
    label "zabytkoznawstwo"
  ]
  node [
    id 582
    label "historia_sztuki"
  ]
  node [
    id 583
    label "du&#380;y"
  ]
  node [
    id 584
    label "jedyny"
  ]
  node [
    id 585
    label "zdr&#243;w"
  ]
  node [
    id 586
    label "&#380;ywy"
  ]
  node [
    id 587
    label "ca&#322;o"
  ]
  node [
    id 588
    label "calu&#347;ko"
  ]
  node [
    id 589
    label "podobny"
  ]
  node [
    id 590
    label "podostatek"
  ]
  node [
    id 591
    label "fortune"
  ]
  node [
    id 592
    label "cecha"
  ]
  node [
    id 593
    label "wysyp"
  ]
  node [
    id 594
    label "zr&#243;&#380;nicowanie"
  ]
  node [
    id 595
    label "mienie"
  ]
  node [
    id 596
    label "fullness"
  ]
  node [
    id 597
    label "z&#322;ote_czasy"
  ]
  node [
    id 598
    label "render"
  ]
  node [
    id 599
    label "zmienia&#263;"
  ]
  node [
    id 600
    label "zestaw"
  ]
  node [
    id 601
    label "train"
  ]
  node [
    id 602
    label "uk&#322;ada&#263;"
  ]
  node [
    id 603
    label "dzieli&#263;"
  ]
  node [
    id 604
    label "set"
  ]
  node [
    id 605
    label "przywraca&#263;"
  ]
  node [
    id 606
    label "dawa&#263;"
  ]
  node [
    id 607
    label "przyk&#322;ada&#263;"
  ]
  node [
    id 608
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 609
    label "zbiera&#263;"
  ]
  node [
    id 610
    label "opracowywa&#263;"
  ]
  node [
    id 611
    label "oszcz&#281;dza&#263;"
  ]
  node [
    id 612
    label "publicize"
  ]
  node [
    id 613
    label "przekazywa&#263;"
  ]
  node [
    id 614
    label "scala&#263;"
  ]
  node [
    id 615
    label "oddawa&#263;"
  ]
  node [
    id 616
    label "fraza"
  ]
  node [
    id 617
    label "temat"
  ]
  node [
    id 618
    label "przyczyna"
  ]
  node [
    id 619
    label "ozdoba"
  ]
  node [
    id 620
    label "baseball"
  ]
  node [
    id 621
    label "czyn"
  ]
  node [
    id 622
    label "error"
  ]
  node [
    id 623
    label "pomyli&#263;_si&#281;"
  ]
  node [
    id 624
    label "pomylenie_si&#281;"
  ]
  node [
    id 625
    label "mniemanie"
  ]
  node [
    id 626
    label "byk"
  ]
  node [
    id 627
    label "nieprawid&#322;owo&#347;&#263;"
  ]
  node [
    id 628
    label "puchar"
  ]
  node [
    id 629
    label "beat"
  ]
  node [
    id 630
    label "conquest"
  ]
  node [
    id 631
    label "poradzenie_sobie"
  ]
  node [
    id 632
    label "gorzka_pigu&#322;ka"
  ]
  node [
    id 633
    label "lipa"
  ]
  node [
    id 634
    label "przegra"
  ]
  node [
    id 635
    label "wysiadka"
  ]
  node [
    id 636
    label "passa"
  ]
  node [
    id 637
    label "niefortunno&#347;&#263;"
  ]
  node [
    id 638
    label "po&#322;o&#380;enie"
  ]
  node [
    id 639
    label "niepowodzenie"
  ]
  node [
    id 640
    label "reverse"
  ]
  node [
    id 641
    label "k&#322;adzenie"
  ]
  node [
    id 642
    label "cz&#281;sto"
  ]
  node [
    id 643
    label "mocno"
  ]
  node [
    id 644
    label "wiela"
  ]
  node [
    id 645
    label "dally"
  ]
  node [
    id 646
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 647
    label "kopiowa&#263;"
  ]
  node [
    id 648
    label "sztuczny"
  ]
  node [
    id 649
    label "mock"
  ]
  node [
    id 650
    label "manewr"
  ]
  node [
    id 651
    label "byt"
  ]
  node [
    id 652
    label "zdo&#322;a&#263;"
  ]
  node [
    id 653
    label "op&#322;aci&#263;"
  ]
  node [
    id 654
    label "foster"
  ]
  node [
    id 655
    label "feed"
  ]
  node [
    id 656
    label "podtrzyma&#263;"
  ]
  node [
    id 657
    label "preserve"
  ]
  node [
    id 658
    label "obroni&#263;"
  ]
  node [
    id 659
    label "przetrzyma&#263;"
  ]
  node [
    id 660
    label "zrobi&#263;"
  ]
  node [
    id 661
    label "potrzyma&#263;"
  ]
  node [
    id 662
    label "unie&#347;&#263;"
  ]
  node [
    id 663
    label "zapewni&#263;"
  ]
  node [
    id 664
    label "ciekawo&#347;&#263;"
  ]
  node [
    id 665
    label "care"
  ]
  node [
    id 666
    label "emocja"
  ]
  node [
    id 667
    label "zainteresowanie_si&#281;"
  ]
  node [
    id 668
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 669
    label "love"
  ]
  node [
    id 670
    label "wzbudzenie"
  ]
  node [
    id 671
    label "Zgredek"
  ]
  node [
    id 672
    label "kategoria_gramatyczna"
  ]
  node [
    id 673
    label "Casanova"
  ]
  node [
    id 674
    label "Don_Juan"
  ]
  node [
    id 675
    label "Gargantua"
  ]
  node [
    id 676
    label "Faust"
  ]
  node [
    id 677
    label "profanum"
  ]
  node [
    id 678
    label "Chocho&#322;"
  ]
  node [
    id 679
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 680
    label "koniugacja"
  ]
  node [
    id 681
    label "Winnetou"
  ]
  node [
    id 682
    label "Dwukwiat"
  ]
  node [
    id 683
    label "homo_sapiens"
  ]
  node [
    id 684
    label "Edyp"
  ]
  node [
    id 685
    label "Herkules_Poirot"
  ]
  node [
    id 686
    label "ludzko&#347;&#263;"
  ]
  node [
    id 687
    label "person"
  ]
  node [
    id 688
    label "Sherlock_Holmes"
  ]
  node [
    id 689
    label "portrecista"
  ]
  node [
    id 690
    label "Szwejk"
  ]
  node [
    id 691
    label "Hamlet"
  ]
  node [
    id 692
    label "duch"
  ]
  node [
    id 693
    label "g&#322;owa"
  ]
  node [
    id 694
    label "oddzia&#322;ywanie"
  ]
  node [
    id 695
    label "Quasimodo"
  ]
  node [
    id 696
    label "Dulcynea"
  ]
  node [
    id 697
    label "Don_Kiszot"
  ]
  node [
    id 698
    label "Wallenrod"
  ]
  node [
    id 699
    label "Plastu&#347;"
  ]
  node [
    id 700
    label "Harry_Potter"
  ]
  node [
    id 701
    label "figura"
  ]
  node [
    id 702
    label "parali&#380;owa&#263;"
  ]
  node [
    id 703
    label "istota"
  ]
  node [
    id 704
    label "Werter"
  ]
  node [
    id 705
    label "antropochoria"
  ]
  node [
    id 706
    label "posta&#263;"
  ]
  node [
    id 707
    label "dysleksja"
  ]
  node [
    id 708
    label "umie&#263;"
  ]
  node [
    id 709
    label "wr&#243;&#380;y&#263;"
  ]
  node [
    id 710
    label "przetwarza&#263;"
  ]
  node [
    id 711
    label "read"
  ]
  node [
    id 712
    label "poznawa&#263;"
  ]
  node [
    id 713
    label "obserwowa&#263;"
  ]
  node [
    id 714
    label "odczytywa&#263;"
  ]
  node [
    id 715
    label "miernota"
  ]
  node [
    id 716
    label "ciura"
  ]
  node [
    id 717
    label "si&#281;ga&#263;"
  ]
  node [
    id 718
    label "trwa&#263;"
  ]
  node [
    id 719
    label "obecno&#347;&#263;"
  ]
  node [
    id 720
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 721
    label "stand"
  ]
  node [
    id 722
    label "uczestniczy&#263;"
  ]
  node [
    id 723
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 724
    label "equal"
  ]
  node [
    id 725
    label "element"
  ]
  node [
    id 726
    label "przele&#378;&#263;"
  ]
  node [
    id 727
    label "pi&#281;tro"
  ]
  node [
    id 728
    label "karczek"
  ]
  node [
    id 729
    label "wysoki"
  ]
  node [
    id 730
    label "rami&#261;czko"
  ]
  node [
    id 731
    label "Ropa"
  ]
  node [
    id 732
    label "Jaworze"
  ]
  node [
    id 733
    label "Synaj"
  ]
  node [
    id 734
    label "wzniesienie"
  ]
  node [
    id 735
    label "przelezienie"
  ]
  node [
    id 736
    label "&#347;piew"
  ]
  node [
    id 737
    label "kupa"
  ]
  node [
    id 738
    label "Ma&#322;a_Rawka"
  ]
  node [
    id 739
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 740
    label "d&#378;wi&#281;k"
  ]
  node [
    id 741
    label "Kreml"
  ]
  node [
    id 742
    label "g&#243;wno"
  ]
  node [
    id 743
    label "brak"
  ]
  node [
    id 744
    label "defenestracja"
  ]
  node [
    id 745
    label "szereg"
  ]
  node [
    id 746
    label "dzia&#322;anie"
  ]
  node [
    id 747
    label "ostatnie_podrygi"
  ]
  node [
    id 748
    label "kres"
  ]
  node [
    id 749
    label "agonia"
  ]
  node [
    id 750
    label "visitation"
  ]
  node [
    id 751
    label "szeol"
  ]
  node [
    id 752
    label "mogi&#322;a"
  ]
  node [
    id 753
    label "chwila"
  ]
  node [
    id 754
    label "pogrzebanie"
  ]
  node [
    id 755
    label "&#380;a&#322;oba"
  ]
  node [
    id 756
    label "zabicie"
  ]
  node [
    id 757
    label "kres_&#380;ycia"
  ]
  node [
    id 758
    label "clear"
  ]
  node [
    id 759
    label "uzasadni&#263;"
  ]
  node [
    id 760
    label "potwierdzi&#263;"
  ]
  node [
    id 761
    label "fineness"
  ]
  node [
    id 762
    label "skromno&#347;&#263;"
  ]
  node [
    id 763
    label "dobro&#263;"
  ]
  node [
    id 764
    label "wytw&#243;r"
  ]
  node [
    id 765
    label "thinking"
  ]
  node [
    id 766
    label "organizacja"
  ]
  node [
    id 767
    label "pokrywa"
  ]
  node [
    id 768
    label "pokrywka"
  ]
  node [
    id 769
    label "lid"
  ]
  node [
    id 770
    label "pretekst"
  ]
  node [
    id 771
    label "czasowy"
  ]
  node [
    id 772
    label "temporarily"
  ]
  node [
    id 773
    label "&#322;&#243;&#380;ko"
  ]
  node [
    id 774
    label "sprz&#261;tn&#261;&#263;"
  ]
  node [
    id 775
    label "piwo"
  ]
  node [
    id 776
    label "sta&#263;_si&#281;"
  ]
  node [
    id 777
    label "pu&#347;ci&#263;"
  ]
  node [
    id 778
    label "zatrze&#263;_si&#281;"
  ]
  node [
    id 779
    label "deliquesce"
  ]
  node [
    id 780
    label "zmi&#281;kn&#261;&#263;"
  ]
  node [
    id 781
    label "uby&#263;"
  ]
  node [
    id 782
    label "mellow"
  ]
  node [
    id 783
    label "fail"
  ]
  node [
    id 784
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 785
    label "leave_office"
  ]
  node [
    id 786
    label "sko&#324;czy&#263;"
  ]
  node [
    id 787
    label "coating"
  ]
  node [
    id 788
    label "drop"
  ]
  node [
    id 789
    label "dodawa&#263;"
  ]
  node [
    id 790
    label "wymienia&#263;"
  ]
  node [
    id 791
    label "okre&#347;la&#263;"
  ]
  node [
    id 792
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 793
    label "dyskalkulia"
  ]
  node [
    id 794
    label "wynagrodzenie"
  ]
  node [
    id 795
    label "admit"
  ]
  node [
    id 796
    label "osi&#261;ga&#263;"
  ]
  node [
    id 797
    label "wyznacza&#263;"
  ]
  node [
    id 798
    label "posiada&#263;"
  ]
  node [
    id 799
    label "mierzy&#263;"
  ]
  node [
    id 800
    label "odlicza&#263;"
  ]
  node [
    id 801
    label "bra&#263;"
  ]
  node [
    id 802
    label "wycenia&#263;"
  ]
  node [
    id 803
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 804
    label "rachowa&#263;"
  ]
  node [
    id 805
    label "tell"
  ]
  node [
    id 806
    label "uwzgl&#281;dnia&#263;"
  ]
  node [
    id 807
    label "policza&#263;"
  ]
  node [
    id 808
    label "count"
  ]
  node [
    id 809
    label "w_chuj"
  ]
  node [
    id 810
    label "fajnie"
  ]
  node [
    id 811
    label "byczy"
  ]
  node [
    id 812
    label "klawy"
  ]
  node [
    id 813
    label "asymilowa&#263;"
  ]
  node [
    id 814
    label "wapniak"
  ]
  node [
    id 815
    label "dwun&#243;g"
  ]
  node [
    id 816
    label "polifag"
  ]
  node [
    id 817
    label "wz&#243;r"
  ]
  node [
    id 818
    label "hominid"
  ]
  node [
    id 819
    label "nasada"
  ]
  node [
    id 820
    label "podw&#322;adny"
  ]
  node [
    id 821
    label "os&#322;abianie"
  ]
  node [
    id 822
    label "asymilowanie"
  ]
  node [
    id 823
    label "os&#322;abia&#263;"
  ]
  node [
    id 824
    label "Adam"
  ]
  node [
    id 825
    label "senior"
  ]
  node [
    id 826
    label "uderzenie"
  ]
  node [
    id 827
    label "cios"
  ]
  node [
    id 828
    label "time"
  ]
  node [
    id 829
    label "czu&#263;"
  ]
  node [
    id 830
    label "desire"
  ]
  node [
    id 831
    label "kcie&#263;"
  ]
  node [
    id 832
    label "need"
  ]
  node [
    id 833
    label "hide"
  ]
  node [
    id 834
    label "support"
  ]
  node [
    id 835
    label "s&#261;d"
  ]
  node [
    id 836
    label "argument"
  ]
  node [
    id 837
    label "porcja"
  ]
  node [
    id 838
    label "prawda"
  ]
  node [
    id 839
    label "nale&#380;ny"
  ]
  node [
    id 840
    label "pause"
  ]
  node [
    id 841
    label "stay"
  ]
  node [
    id 842
    label "consist"
  ]
  node [
    id 843
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 844
    label "zachowywa&#263;_si&#281;"
  ]
  node [
    id 845
    label "istnie&#263;"
  ]
  node [
    id 846
    label "&#347;ledziowate"
  ]
  node [
    id 847
    label "ni_chuja"
  ]
  node [
    id 848
    label "ca&#322;kiem"
  ]
  node [
    id 849
    label "zupe&#322;nie"
  ]
  node [
    id 850
    label "jaki&#347;"
  ]
  node [
    id 851
    label "imponuj&#261;cy"
  ]
  node [
    id 852
    label "b&#322;yskotliwy"
  ]
  node [
    id 853
    label "genialnie"
  ]
  node [
    id 854
    label "ponadprzeci&#281;tny"
  ]
  node [
    id 855
    label "utalentowany"
  ]
  node [
    id 856
    label "mistrzowski"
  ]
  node [
    id 857
    label "w&#322;a&#347;ciwy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 150
  ]
  edge [
    source 7
    target 151
  ]
  edge [
    source 7
    target 152
  ]
  edge [
    source 7
    target 153
  ]
  edge [
    source 7
    target 154
  ]
  edge [
    source 7
    target 155
  ]
  edge [
    source 7
    target 156
  ]
  edge [
    source 7
    target 157
  ]
  edge [
    source 7
    target 158
  ]
  edge [
    source 7
    target 159
  ]
  edge [
    source 7
    target 160
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 161
  ]
  edge [
    source 8
    target 162
  ]
  edge [
    source 8
    target 163
  ]
  edge [
    source 8
    target 164
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 165
  ]
  edge [
    source 8
    target 166
  ]
  edge [
    source 8
    target 167
  ]
  edge [
    source 8
    target 168
  ]
  edge [
    source 8
    target 169
  ]
  edge [
    source 8
    target 170
  ]
  edge [
    source 8
    target 171
  ]
  edge [
    source 8
    target 172
  ]
  edge [
    source 8
    target 173
  ]
  edge [
    source 8
    target 174
  ]
  edge [
    source 8
    target 175
  ]
  edge [
    source 8
    target 176
  ]
  edge [
    source 8
    target 177
  ]
  edge [
    source 8
    target 178
  ]
  edge [
    source 8
    target 179
  ]
  edge [
    source 8
    target 180
  ]
  edge [
    source 8
    target 181
  ]
  edge [
    source 8
    target 182
  ]
  edge [
    source 8
    target 183
  ]
  edge [
    source 8
    target 184
  ]
  edge [
    source 8
    target 185
  ]
  edge [
    source 8
    target 186
  ]
  edge [
    source 8
    target 187
  ]
  edge [
    source 8
    target 188
  ]
  edge [
    source 8
    target 189
  ]
  edge [
    source 8
    target 190
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 191
  ]
  edge [
    source 10
    target 192
  ]
  edge [
    source 10
    target 193
  ]
  edge [
    source 10
    target 194
  ]
  edge [
    source 10
    target 195
  ]
  edge [
    source 10
    target 196
  ]
  edge [
    source 10
    target 19
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 19
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 11
    target 103
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 11
    target 111
  ]
  edge [
    source 11
    target 112
  ]
  edge [
    source 11
    target 114
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 197
  ]
  edge [
    source 13
    target 198
  ]
  edge [
    source 13
    target 199
  ]
  edge [
    source 13
    target 200
  ]
  edge [
    source 13
    target 201
  ]
  edge [
    source 13
    target 202
  ]
  edge [
    source 13
    target 203
  ]
  edge [
    source 13
    target 204
  ]
  edge [
    source 13
    target 205
  ]
  edge [
    source 13
    target 206
  ]
  edge [
    source 13
    target 207
  ]
  edge [
    source 13
    target 208
  ]
  edge [
    source 13
    target 209
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 210
  ]
  edge [
    source 14
    target 211
  ]
  edge [
    source 14
    target 212
  ]
  edge [
    source 14
    target 213
  ]
  edge [
    source 14
    target 214
  ]
  edge [
    source 14
    target 215
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 71
  ]
  edge [
    source 15
    target 72
  ]
  edge [
    source 15
    target 77
  ]
  edge [
    source 15
    target 78
  ]
  edge [
    source 15
    target 98
  ]
  edge [
    source 15
    target 99
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 216
  ]
  edge [
    source 16
    target 217
  ]
  edge [
    source 16
    target 218
  ]
  edge [
    source 16
    target 219
  ]
  edge [
    source 16
    target 220
  ]
  edge [
    source 16
    target 221
  ]
  edge [
    source 16
    target 222
  ]
  edge [
    source 16
    target 223
  ]
  edge [
    source 16
    target 224
  ]
  edge [
    source 16
    target 225
  ]
  edge [
    source 16
    target 226
  ]
  edge [
    source 16
    target 227
  ]
  edge [
    source 16
    target 228
  ]
  edge [
    source 16
    target 229
  ]
  edge [
    source 17
    target 230
  ]
  edge [
    source 17
    target 231
  ]
  edge [
    source 17
    target 232
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 233
  ]
  edge [
    source 18
    target 234
  ]
  edge [
    source 18
    target 235
  ]
  edge [
    source 18
    target 236
  ]
  edge [
    source 18
    target 237
  ]
  edge [
    source 18
    target 238
  ]
  edge [
    source 18
    target 239
  ]
  edge [
    source 18
    target 240
  ]
  edge [
    source 18
    target 241
  ]
  edge [
    source 18
    target 242
  ]
  edge [
    source 18
    target 243
  ]
  edge [
    source 18
    target 244
  ]
  edge [
    source 18
    target 245
  ]
  edge [
    source 18
    target 246
  ]
  edge [
    source 18
    target 247
  ]
  edge [
    source 18
    target 248
  ]
  edge [
    source 19
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 40
  ]
  edge [
    source 19
    target 249
  ]
  edge [
    source 19
    target 31
  ]
  edge [
    source 19
    target 250
  ]
  edge [
    source 19
    target 251
  ]
  edge [
    source 19
    target 252
  ]
  edge [
    source 19
    target 253
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 254
  ]
  edge [
    source 20
    target 255
  ]
  edge [
    source 20
    target 256
  ]
  edge [
    source 20
    target 257
  ]
  edge [
    source 20
    target 258
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 259
  ]
  edge [
    source 22
    target 260
  ]
  edge [
    source 22
    target 261
  ]
  edge [
    source 22
    target 262
  ]
  edge [
    source 22
    target 263
  ]
  edge [
    source 22
    target 264
  ]
  edge [
    source 22
    target 265
  ]
  edge [
    source 22
    target 266
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 267
  ]
  edge [
    source 23
    target 268
  ]
  edge [
    source 23
    target 269
  ]
  edge [
    source 23
    target 270
  ]
  edge [
    source 23
    target 271
  ]
  edge [
    source 23
    target 272
  ]
  edge [
    source 23
    target 273
  ]
  edge [
    source 23
    target 220
  ]
  edge [
    source 23
    target 274
  ]
  edge [
    source 23
    target 242
  ]
  edge [
    source 23
    target 275
  ]
  edge [
    source 23
    target 276
  ]
  edge [
    source 23
    target 224
  ]
  edge [
    source 23
    target 277
  ]
  edge [
    source 23
    target 278
  ]
  edge [
    source 23
    target 279
  ]
  edge [
    source 23
    target 280
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 281
  ]
  edge [
    source 25
    target 282
  ]
  edge [
    source 25
    target 283
  ]
  edge [
    source 25
    target 284
  ]
  edge [
    source 25
    target 285
  ]
  edge [
    source 25
    target 286
  ]
  edge [
    source 25
    target 287
  ]
  edge [
    source 25
    target 288
  ]
  edge [
    source 25
    target 289
  ]
  edge [
    source 25
    target 256
  ]
  edge [
    source 25
    target 290
  ]
  edge [
    source 25
    target 291
  ]
  edge [
    source 25
    target 292
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 293
  ]
  edge [
    source 26
    target 294
  ]
  edge [
    source 26
    target 295
  ]
  edge [
    source 26
    target 296
  ]
  edge [
    source 26
    target 297
  ]
  edge [
    source 26
    target 298
  ]
  edge [
    source 26
    target 299
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 68
  ]
  edge [
    source 27
    target 84
  ]
  edge [
    source 27
    target 300
  ]
  edge [
    source 27
    target 301
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 302
  ]
  edge [
    source 28
    target 303
  ]
  edge [
    source 28
    target 304
  ]
  edge [
    source 28
    target 305
  ]
  edge [
    source 28
    target 306
  ]
  edge [
    source 28
    target 307
  ]
  edge [
    source 28
    target 308
  ]
  edge [
    source 28
    target 309
  ]
  edge [
    source 28
    target 310
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 311
  ]
  edge [
    source 29
    target 312
  ]
  edge [
    source 29
    target 313
  ]
  edge [
    source 29
    target 314
  ]
  edge [
    source 29
    target 315
  ]
  edge [
    source 29
    target 316
  ]
  edge [
    source 29
    target 317
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 318
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 33
  ]
  edge [
    source 31
    target 34
  ]
  edge [
    source 31
    target 319
  ]
  edge [
    source 31
    target 320
  ]
  edge [
    source 31
    target 321
  ]
  edge [
    source 31
    target 322
  ]
  edge [
    source 31
    target 323
  ]
  edge [
    source 31
    target 324
  ]
  edge [
    source 31
    target 325
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 33
    target 39
  ]
  edge [
    source 33
    target 40
  ]
  edge [
    source 33
    target 326
  ]
  edge [
    source 33
    target 327
  ]
  edge [
    source 33
    target 328
  ]
  edge [
    source 33
    target 329
  ]
  edge [
    source 33
    target 330
  ]
  edge [
    source 33
    target 331
  ]
  edge [
    source 33
    target 332
  ]
  edge [
    source 33
    target 333
  ]
  edge [
    source 33
    target 109
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 334
  ]
  edge [
    source 34
    target 335
  ]
  edge [
    source 34
    target 336
  ]
  edge [
    source 34
    target 337
  ]
  edge [
    source 34
    target 338
  ]
  edge [
    source 34
    target 339
  ]
  edge [
    source 34
    target 340
  ]
  edge [
    source 34
    target 341
  ]
  edge [
    source 34
    target 342
  ]
  edge [
    source 34
    target 343
  ]
  edge [
    source 34
    target 344
  ]
  edge [
    source 34
    target 345
  ]
  edge [
    source 34
    target 346
  ]
  edge [
    source 34
    target 347
  ]
  edge [
    source 34
    target 85
  ]
  edge [
    source 34
    target 348
  ]
  edge [
    source 34
    target 349
  ]
  edge [
    source 34
    target 350
  ]
  edge [
    source 34
    target 351
  ]
  edge [
    source 34
    target 352
  ]
  edge [
    source 34
    target 353
  ]
  edge [
    source 34
    target 354
  ]
  edge [
    source 34
    target 355
  ]
  edge [
    source 34
    target 356
  ]
  edge [
    source 34
    target 357
  ]
  edge [
    source 34
    target 358
  ]
  edge [
    source 34
    target 359
  ]
  edge [
    source 34
    target 360
  ]
  edge [
    source 34
    target 361
  ]
  edge [
    source 34
    target 362
  ]
  edge [
    source 34
    target 363
  ]
  edge [
    source 34
    target 364
  ]
  edge [
    source 34
    target 365
  ]
  edge [
    source 34
    target 366
  ]
  edge [
    source 34
    target 367
  ]
  edge [
    source 34
    target 368
  ]
  edge [
    source 34
    target 369
  ]
  edge [
    source 34
    target 370
  ]
  edge [
    source 34
    target 371
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 372
  ]
  edge [
    source 35
    target 373
  ]
  edge [
    source 35
    target 374
  ]
  edge [
    source 35
    target 375
  ]
  edge [
    source 35
    target 376
  ]
  edge [
    source 35
    target 377
  ]
  edge [
    source 35
    target 378
  ]
  edge [
    source 35
    target 379
  ]
  edge [
    source 35
    target 380
  ]
  edge [
    source 35
    target 274
  ]
  edge [
    source 35
    target 381
  ]
  edge [
    source 35
    target 382
  ]
  edge [
    source 35
    target 383
  ]
  edge [
    source 35
    target 384
  ]
  edge [
    source 35
    target 118
  ]
  edge [
    source 35
    target 385
  ]
  edge [
    source 35
    target 386
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 39
    target 387
  ]
  edge [
    source 39
    target 388
  ]
  edge [
    source 39
    target 389
  ]
  edge [
    source 39
    target 390
  ]
  edge [
    source 39
    target 391
  ]
  edge [
    source 39
    target 392
  ]
  edge [
    source 39
    target 393
  ]
  edge [
    source 39
    target 394
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 93
  ]
  edge [
    source 40
    target 108
  ]
  edge [
    source 40
    target 395
  ]
  edge [
    source 40
    target 396
  ]
  edge [
    source 40
    target 397
  ]
  edge [
    source 40
    target 398
  ]
  edge [
    source 40
    target 399
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 400
  ]
  edge [
    source 41
    target 401
  ]
  edge [
    source 41
    target 402
  ]
  edge [
    source 41
    target 403
  ]
  edge [
    source 41
    target 404
  ]
  edge [
    source 41
    target 405
  ]
  edge [
    source 41
    target 406
  ]
  edge [
    source 41
    target 62
  ]
  edge [
    source 41
    target 407
  ]
  edge [
    source 41
    target 408
  ]
  edge [
    source 41
    target 409
  ]
  edge [
    source 41
    target 410
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 411
  ]
  edge [
    source 42
    target 412
  ]
  edge [
    source 42
    target 413
  ]
  edge [
    source 42
    target 414
  ]
  edge [
    source 42
    target 45
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 415
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 416
  ]
  edge [
    source 44
    target 52
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 417
  ]
  edge [
    source 45
    target 418
  ]
  edge [
    source 45
    target 419
  ]
  edge [
    source 45
    target 420
  ]
  edge [
    source 45
    target 421
  ]
  edge [
    source 45
    target 422
  ]
  edge [
    source 45
    target 423
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 424
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 103
  ]
  edge [
    source 47
    target 425
  ]
  edge [
    source 47
    target 426
  ]
  edge [
    source 47
    target 427
  ]
  edge [
    source 47
    target 428
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 429
  ]
  edge [
    source 48
    target 430
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 431
  ]
  edge [
    source 49
    target 103
  ]
  edge [
    source 49
    target 432
  ]
  edge [
    source 49
    target 433
  ]
  edge [
    source 49
    target 434
  ]
  edge [
    source 49
    target 435
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 436
  ]
  edge [
    source 50
    target 437
  ]
  edge [
    source 50
    target 438
  ]
  edge [
    source 50
    target 439
  ]
  edge [
    source 50
    target 440
  ]
  edge [
    source 50
    target 53
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 441
  ]
  edge [
    source 51
    target 116
  ]
  edge [
    source 51
    target 442
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 95
  ]
  edge [
    source 52
    target 96
  ]
  edge [
    source 52
    target 443
  ]
  edge [
    source 52
    target 444
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 445
  ]
  edge [
    source 53
    target 446
  ]
  edge [
    source 53
    target 447
  ]
  edge [
    source 53
    target 448
  ]
  edge [
    source 53
    target 449
  ]
  edge [
    source 53
    target 450
  ]
  edge [
    source 53
    target 315
  ]
  edge [
    source 53
    target 451
  ]
  edge [
    source 53
    target 452
  ]
  edge [
    source 53
    target 453
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 454
  ]
  edge [
    source 54
    target 455
  ]
  edge [
    source 54
    target 62
  ]
  edge [
    source 54
    target 456
  ]
  edge [
    source 54
    target 457
  ]
  edge [
    source 54
    target 458
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 459
  ]
  edge [
    source 55
    target 460
  ]
  edge [
    source 55
    target 461
  ]
  edge [
    source 55
    target 462
  ]
  edge [
    source 55
    target 463
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 464
  ]
  edge [
    source 57
    target 465
  ]
  edge [
    source 57
    target 466
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 467
  ]
  edge [
    source 58
    target 468
  ]
  edge [
    source 58
    target 469
  ]
  edge [
    source 58
    target 470
  ]
  edge [
    source 58
    target 471
  ]
  edge [
    source 58
    target 472
  ]
  edge [
    source 58
    target 319
  ]
  edge [
    source 58
    target 473
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 103
  ]
  edge [
    source 60
    target 474
  ]
  edge [
    source 60
    target 475
  ]
  edge [
    source 60
    target 476
  ]
  edge [
    source 60
    target 477
  ]
  edge [
    source 60
    target 478
  ]
  edge [
    source 60
    target 479
  ]
  edge [
    source 60
    target 480
  ]
  edge [
    source 60
    target 481
  ]
  edge [
    source 60
    target 482
  ]
  edge [
    source 60
    target 483
  ]
  edge [
    source 60
    target 484
  ]
  edge [
    source 60
    target 485
  ]
  edge [
    source 60
    target 486
  ]
  edge [
    source 60
    target 487
  ]
  edge [
    source 60
    target 488
  ]
  edge [
    source 60
    target 489
  ]
  edge [
    source 60
    target 490
  ]
  edge [
    source 60
    target 491
  ]
  edge [
    source 60
    target 492
  ]
  edge [
    source 60
    target 493
  ]
  edge [
    source 60
    target 494
  ]
  edge [
    source 60
    target 495
  ]
  edge [
    source 60
    target 496
  ]
  edge [
    source 60
    target 497
  ]
  edge [
    source 60
    target 498
  ]
  edge [
    source 60
    target 499
  ]
  edge [
    source 60
    target 500
  ]
  edge [
    source 60
    target 501
  ]
  edge [
    source 60
    target 502
  ]
  edge [
    source 60
    target 503
  ]
  edge [
    source 60
    target 504
  ]
  edge [
    source 60
    target 505
  ]
  edge [
    source 60
    target 506
  ]
  edge [
    source 60
    target 428
  ]
  edge [
    source 60
    target 507
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 508
  ]
  edge [
    source 62
    target 509
  ]
  edge [
    source 62
    target 510
  ]
  edge [
    source 62
    target 511
  ]
  edge [
    source 62
    target 466
  ]
  edge [
    source 62
    target 512
  ]
  edge [
    source 62
    target 513
  ]
  edge [
    source 62
    target 514
  ]
  edge [
    source 62
    target 515
  ]
  edge [
    source 62
    target 69
  ]
  edge [
    source 62
    target 516
  ]
  edge [
    source 62
    target 517
  ]
  edge [
    source 62
    target 518
  ]
  edge [
    source 62
    target 519
  ]
  edge [
    source 62
    target 520
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 521
  ]
  edge [
    source 63
    target 522
  ]
  edge [
    source 63
    target 523
  ]
  edge [
    source 63
    target 524
  ]
  edge [
    source 63
    target 525
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 114
  ]
  edge [
    source 64
    target 523
  ]
  edge [
    source 64
    target 526
  ]
  edge [
    source 64
    target 527
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 528
  ]
  edge [
    source 65
    target 529
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 66
    target 530
  ]
  edge [
    source 66
    target 531
  ]
  edge [
    source 66
    target 532
  ]
  edge [
    source 66
    target 533
  ]
  edge [
    source 66
    target 342
  ]
  edge [
    source 66
    target 534
  ]
  edge [
    source 66
    target 535
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 67
    target 536
  ]
  edge [
    source 67
    target 537
  ]
  edge [
    source 67
    target 538
  ]
  edge [
    source 67
    target 539
  ]
  edge [
    source 67
    target 540
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 68
    target 83
  ]
  edge [
    source 68
    target 541
  ]
  edge [
    source 68
    target 542
  ]
  edge [
    source 68
    target 543
  ]
  edge [
    source 68
    target 544
  ]
  edge [
    source 68
    target 239
  ]
  edge [
    source 68
    target 545
  ]
  edge [
    source 68
    target 546
  ]
  edge [
    source 68
    target 547
  ]
  edge [
    source 68
    target 548
  ]
  edge [
    source 68
    target 549
  ]
  edge [
    source 68
    target 550
  ]
  edge [
    source 68
    target 551
  ]
  edge [
    source 68
    target 552
  ]
  edge [
    source 68
    target 553
  ]
  edge [
    source 68
    target 554
  ]
  edge [
    source 68
    target 555
  ]
  edge [
    source 68
    target 556
  ]
  edge [
    source 68
    target 557
  ]
  edge [
    source 68
    target 558
  ]
  edge [
    source 68
    target 559
  ]
  edge [
    source 68
    target 560
  ]
  edge [
    source 68
    target 561
  ]
  edge [
    source 68
    target 562
  ]
  edge [
    source 68
    target 563
  ]
  edge [
    source 68
    target 564
  ]
  edge [
    source 68
    target 565
  ]
  edge [
    source 68
    target 72
  ]
  edge [
    source 68
    target 566
  ]
  edge [
    source 68
    target 567
  ]
  edge [
    source 68
    target 568
  ]
  edge [
    source 68
    target 569
  ]
  edge [
    source 68
    target 570
  ]
  edge [
    source 68
    target 571
  ]
  edge [
    source 68
    target 572
  ]
  edge [
    source 68
    target 573
  ]
  edge [
    source 68
    target 574
  ]
  edge [
    source 68
    target 575
  ]
  edge [
    source 68
    target 576
  ]
  edge [
    source 68
    target 577
  ]
  edge [
    source 68
    target 578
  ]
  edge [
    source 68
    target 579
  ]
  edge [
    source 68
    target 580
  ]
  edge [
    source 68
    target 581
  ]
  edge [
    source 68
    target 582
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 69
    target 583
  ]
  edge [
    source 69
    target 584
  ]
  edge [
    source 69
    target 511
  ]
  edge [
    source 69
    target 585
  ]
  edge [
    source 69
    target 586
  ]
  edge [
    source 69
    target 587
  ]
  edge [
    source 69
    target 588
  ]
  edge [
    source 69
    target 589
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 590
  ]
  edge [
    source 70
    target 591
  ]
  edge [
    source 70
    target 592
  ]
  edge [
    source 70
    target 593
  ]
  edge [
    source 70
    target 594
  ]
  edge [
    source 70
    target 595
  ]
  edge [
    source 70
    target 596
  ]
  edge [
    source 70
    target 354
  ]
  edge [
    source 70
    target 119
  ]
  edge [
    source 70
    target 597
  ]
  edge [
    source 71
    target 598
  ]
  edge [
    source 71
    target 599
  ]
  edge [
    source 71
    target 600
  ]
  edge [
    source 71
    target 601
  ]
  edge [
    source 71
    target 602
  ]
  edge [
    source 71
    target 603
  ]
  edge [
    source 71
    target 604
  ]
  edge [
    source 71
    target 605
  ]
  edge [
    source 71
    target 606
  ]
  edge [
    source 71
    target 607
  ]
  edge [
    source 71
    target 608
  ]
  edge [
    source 71
    target 609
  ]
  edge [
    source 71
    target 289
  ]
  edge [
    source 71
    target 610
  ]
  edge [
    source 71
    target 611
  ]
  edge [
    source 71
    target 612
  ]
  edge [
    source 71
    target 613
  ]
  edge [
    source 71
    target 215
  ]
  edge [
    source 71
    target 614
  ]
  edge [
    source 71
    target 615
  ]
  edge [
    source 72
    target 73
  ]
  edge [
    source 72
    target 616
  ]
  edge [
    source 72
    target 372
  ]
  edge [
    source 72
    target 617
  ]
  edge [
    source 72
    target 618
  ]
  edge [
    source 72
    target 592
  ]
  edge [
    source 72
    target 619
  ]
  edge [
    source 72
    target 116
  ]
  edge [
    source 72
    target 119
  ]
  edge [
    source 72
    target 106
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 73
    target 620
  ]
  edge [
    source 73
    target 621
  ]
  edge [
    source 73
    target 622
  ]
  edge [
    source 73
    target 623
  ]
  edge [
    source 73
    target 624
  ]
  edge [
    source 73
    target 625
  ]
  edge [
    source 73
    target 626
  ]
  edge [
    source 73
    target 627
  ]
  edge [
    source 73
    target 473
  ]
  edge [
    source 74
    target 75
  ]
  edge [
    source 74
    target 628
  ]
  edge [
    source 74
    target 629
  ]
  edge [
    source 74
    target 447
  ]
  edge [
    source 74
    target 630
  ]
  edge [
    source 74
    target 631
  ]
  edge [
    source 75
    target 76
  ]
  edge [
    source 75
    target 632
  ]
  edge [
    source 75
    target 633
  ]
  edge [
    source 75
    target 634
  ]
  edge [
    source 75
    target 635
  ]
  edge [
    source 75
    target 636
  ]
  edge [
    source 75
    target 637
  ]
  edge [
    source 75
    target 638
  ]
  edge [
    source 75
    target 639
  ]
  edge [
    source 75
    target 640
  ]
  edge [
    source 75
    target 473
  ]
  edge [
    source 75
    target 641
  ]
  edge [
    source 76
    target 77
  ]
  edge [
    source 76
    target 583
  ]
  edge [
    source 76
    target 642
  ]
  edge [
    source 76
    target 100
  ]
  edge [
    source 76
    target 643
  ]
  edge [
    source 76
    target 644
  ]
  edge [
    source 77
    target 645
  ]
  edge [
    source 77
    target 646
  ]
  edge [
    source 77
    target 647
  ]
  edge [
    source 77
    target 125
  ]
  edge [
    source 77
    target 648
  ]
  edge [
    source 77
    target 649
  ]
  edge [
    source 78
    target 79
  ]
  edge [
    source 78
    target 650
  ]
  edge [
    source 78
    target 651
  ]
  edge [
    source 78
    target 269
  ]
  edge [
    source 78
    target 652
  ]
  edge [
    source 78
    target 653
  ]
  edge [
    source 78
    target 654
  ]
  edge [
    source 78
    target 655
  ]
  edge [
    source 78
    target 656
  ]
  edge [
    source 78
    target 657
  ]
  edge [
    source 78
    target 658
  ]
  edge [
    source 78
    target 659
  ]
  edge [
    source 78
    target 660
  ]
  edge [
    source 78
    target 661
  ]
  edge [
    source 78
    target 662
  ]
  edge [
    source 78
    target 663
  ]
  edge [
    source 79
    target 80
  ]
  edge [
    source 80
    target 81
  ]
  edge [
    source 81
    target 82
  ]
  edge [
    source 81
    target 664
  ]
  edge [
    source 81
    target 665
  ]
  edge [
    source 81
    target 666
  ]
  edge [
    source 81
    target 667
  ]
  edge [
    source 81
    target 668
  ]
  edge [
    source 81
    target 669
  ]
  edge [
    source 81
    target 670
  ]
  edge [
    source 82
    target 83
  ]
  edge [
    source 82
    target 671
  ]
  edge [
    source 82
    target 672
  ]
  edge [
    source 82
    target 673
  ]
  edge [
    source 82
    target 674
  ]
  edge [
    source 82
    target 675
  ]
  edge [
    source 82
    target 676
  ]
  edge [
    source 82
    target 677
  ]
  edge [
    source 82
    target 678
  ]
  edge [
    source 82
    target 679
  ]
  edge [
    source 82
    target 680
  ]
  edge [
    source 82
    target 681
  ]
  edge [
    source 82
    target 682
  ]
  edge [
    source 82
    target 683
  ]
  edge [
    source 82
    target 684
  ]
  edge [
    source 82
    target 685
  ]
  edge [
    source 82
    target 686
  ]
  edge [
    source 82
    target 248
  ]
  edge [
    source 82
    target 687
  ]
  edge [
    source 82
    target 688
  ]
  edge [
    source 82
    target 689
  ]
  edge [
    source 82
    target 690
  ]
  edge [
    source 82
    target 691
  ]
  edge [
    source 82
    target 692
  ]
  edge [
    source 82
    target 693
  ]
  edge [
    source 82
    target 694
  ]
  edge [
    source 82
    target 695
  ]
  edge [
    source 82
    target 696
  ]
  edge [
    source 82
    target 697
  ]
  edge [
    source 82
    target 698
  ]
  edge [
    source 82
    target 699
  ]
  edge [
    source 82
    target 700
  ]
  edge [
    source 82
    target 701
  ]
  edge [
    source 82
    target 702
  ]
  edge [
    source 82
    target 703
  ]
  edge [
    source 82
    target 704
  ]
  edge [
    source 82
    target 705
  ]
  edge [
    source 82
    target 706
  ]
  edge [
    source 82
    target 103
  ]
  edge [
    source 83
    target 707
  ]
  edge [
    source 83
    target 708
  ]
  edge [
    source 83
    target 709
  ]
  edge [
    source 83
    target 710
  ]
  edge [
    source 83
    target 711
  ]
  edge [
    source 83
    target 712
  ]
  edge [
    source 83
    target 713
  ]
  edge [
    source 83
    target 714
  ]
  edge [
    source 84
    target 85
  ]
  edge [
    source 84
    target 715
  ]
  edge [
    source 84
    target 716
  ]
  edge [
    source 85
    target 86
  ]
  edge [
    source 85
    target 88
  ]
  edge [
    source 85
    target 89
  ]
  edge [
    source 85
    target 717
  ]
  edge [
    source 85
    target 718
  ]
  edge [
    source 85
    target 719
  ]
  edge [
    source 85
    target 720
  ]
  edge [
    source 85
    target 721
  ]
  edge [
    source 85
    target 173
  ]
  edge [
    source 85
    target 722
  ]
  edge [
    source 85
    target 723
  ]
  edge [
    source 85
    target 724
  ]
  edge [
    source 86
    target 87
  ]
  edge [
    source 86
    target 239
  ]
  edge [
    source 86
    target 420
  ]
  edge [
    source 86
    target 725
  ]
  edge [
    source 86
    target 726
  ]
  edge [
    source 86
    target 727
  ]
  edge [
    source 86
    target 728
  ]
  edge [
    source 86
    target 729
  ]
  edge [
    source 86
    target 730
  ]
  edge [
    source 86
    target 731
  ]
  edge [
    source 86
    target 732
  ]
  edge [
    source 86
    target 733
  ]
  edge [
    source 86
    target 734
  ]
  edge [
    source 86
    target 735
  ]
  edge [
    source 86
    target 736
  ]
  edge [
    source 86
    target 737
  ]
  edge [
    source 86
    target 576
  ]
  edge [
    source 86
    target 738
  ]
  edge [
    source 86
    target 739
  ]
  edge [
    source 86
    target 740
  ]
  edge [
    source 86
    target 741
  ]
  edge [
    source 87
    target 88
  ]
  edge [
    source 87
    target 715
  ]
  edge [
    source 87
    target 742
  ]
  edge [
    source 87
    target 669
  ]
  edge [
    source 87
    target 354
  ]
  edge [
    source 87
    target 743
  ]
  edge [
    source 87
    target 716
  ]
  edge [
    source 89
    target 90
  ]
  edge [
    source 89
    target 96
  ]
  edge [
    source 89
    target 97
  ]
  edge [
    source 89
    target 744
  ]
  edge [
    source 89
    target 745
  ]
  edge [
    source 89
    target 746
  ]
  edge [
    source 89
    target 361
  ]
  edge [
    source 89
    target 747
  ]
  edge [
    source 89
    target 748
  ]
  edge [
    source 89
    target 749
  ]
  edge [
    source 89
    target 750
  ]
  edge [
    source 89
    target 751
  ]
  edge [
    source 89
    target 752
  ]
  edge [
    source 89
    target 753
  ]
  edge [
    source 89
    target 116
  ]
  edge [
    source 89
    target 739
  ]
  edge [
    source 89
    target 754
  ]
  edge [
    source 89
    target 342
  ]
  edge [
    source 89
    target 755
  ]
  edge [
    source 89
    target 756
  ]
  edge [
    source 89
    target 757
  ]
  edge [
    source 90
    target 91
  ]
  edge [
    source 90
    target 758
  ]
  edge [
    source 90
    target 759
  ]
  edge [
    source 90
    target 658
  ]
  edge [
    source 90
    target 760
  ]
  edge [
    source 91
    target 92
  ]
  edge [
    source 91
    target 761
  ]
  edge [
    source 91
    target 762
  ]
  edge [
    source 91
    target 763
  ]
  edge [
    source 91
    target 592
  ]
  edge [
    source 92
    target 93
  ]
  edge [
    source 92
    target 764
  ]
  edge [
    source 92
    target 765
  ]
  edge [
    source 92
    target 108
  ]
  edge [
    source 93
    target 766
  ]
  edge [
    source 93
    target 767
  ]
  edge [
    source 93
    target 768
  ]
  edge [
    source 93
    target 769
  ]
  edge [
    source 93
    target 770
  ]
  edge [
    source 94
    target 95
  ]
  edge [
    source 94
    target 771
  ]
  edge [
    source 94
    target 772
  ]
  edge [
    source 94
    target 100
  ]
  edge [
    source 95
    target 541
  ]
  edge [
    source 95
    target 773
  ]
  edge [
    source 95
    target 774
  ]
  edge [
    source 96
    target 775
  ]
  edge [
    source 97
    target 98
  ]
  edge [
    source 97
    target 776
  ]
  edge [
    source 97
    target 777
  ]
  edge [
    source 97
    target 778
  ]
  edge [
    source 97
    target 779
  ]
  edge [
    source 97
    target 780
  ]
  edge [
    source 97
    target 781
  ]
  edge [
    source 97
    target 782
  ]
  edge [
    source 98
    target 783
  ]
  edge [
    source 98
    target 784
  ]
  edge [
    source 98
    target 785
  ]
  edge [
    source 98
    target 786
  ]
  edge [
    source 98
    target 787
  ]
  edge [
    source 98
    target 788
  ]
  edge [
    source 98
    target 660
  ]
  edge [
    source 99
    target 100
  ]
  edge [
    source 99
    target 541
  ]
  edge [
    source 99
    target 789
  ]
  edge [
    source 99
    target 790
  ]
  edge [
    source 99
    target 791
  ]
  edge [
    source 99
    target 792
  ]
  edge [
    source 99
    target 793
  ]
  edge [
    source 99
    target 794
  ]
  edge [
    source 99
    target 795
  ]
  edge [
    source 99
    target 796
  ]
  edge [
    source 99
    target 797
  ]
  edge [
    source 99
    target 798
  ]
  edge [
    source 99
    target 799
  ]
  edge [
    source 99
    target 800
  ]
  edge [
    source 99
    target 801
  ]
  edge [
    source 99
    target 802
  ]
  edge [
    source 99
    target 803
  ]
  edge [
    source 99
    target 804
  ]
  edge [
    source 99
    target 805
  ]
  edge [
    source 99
    target 806
  ]
  edge [
    source 99
    target 807
  ]
  edge [
    source 99
    target 808
  ]
  edge [
    source 100
    target 101
  ]
  edge [
    source 100
    target 809
  ]
  edge [
    source 101
    target 102
  ]
  edge [
    source 101
    target 810
  ]
  edge [
    source 101
    target 330
  ]
  edge [
    source 101
    target 811
  ]
  edge [
    source 101
    target 812
  ]
  edge [
    source 102
    target 103
  ]
  edge [
    source 103
    target 813
  ]
  edge [
    source 103
    target 814
  ]
  edge [
    source 103
    target 815
  ]
  edge [
    source 103
    target 816
  ]
  edge [
    source 103
    target 817
  ]
  edge [
    source 103
    target 677
  ]
  edge [
    source 103
    target 818
  ]
  edge [
    source 103
    target 683
  ]
  edge [
    source 103
    target 819
  ]
  edge [
    source 103
    target 820
  ]
  edge [
    source 103
    target 686
  ]
  edge [
    source 103
    target 821
  ]
  edge [
    source 103
    target 248
  ]
  edge [
    source 103
    target 689
  ]
  edge [
    source 103
    target 692
  ]
  edge [
    source 103
    target 694
  ]
  edge [
    source 103
    target 693
  ]
  edge [
    source 103
    target 822
  ]
  edge [
    source 103
    target 823
  ]
  edge [
    source 103
    target 701
  ]
  edge [
    source 103
    target 824
  ]
  edge [
    source 103
    target 825
  ]
  edge [
    source 103
    target 705
  ]
  edge [
    source 103
    target 706
  ]
  edge [
    source 104
    target 105
  ]
  edge [
    source 105
    target 106
  ]
  edge [
    source 105
    target 753
  ]
  edge [
    source 105
    target 826
  ]
  edge [
    source 105
    target 827
  ]
  edge [
    source 105
    target 828
  ]
  edge [
    source 106
    target 107
  ]
  edge [
    source 106
    target 114
  ]
  edge [
    source 106
    target 829
  ]
  edge [
    source 106
    target 830
  ]
  edge [
    source 106
    target 831
  ]
  edge [
    source 107
    target 108
  ]
  edge [
    source 107
    target 720
  ]
  edge [
    source 107
    target 829
  ]
  edge [
    source 107
    target 832
  ]
  edge [
    source 107
    target 833
  ]
  edge [
    source 107
    target 834
  ]
  edge [
    source 108
    target 113
  ]
  edge [
    source 108
    target 835
  ]
  edge [
    source 108
    target 836
  ]
  edge [
    source 108
    target 618
  ]
  edge [
    source 108
    target 837
  ]
  edge [
    source 108
    target 838
  ]
  edge [
    source 109
    target 110
  ]
  edge [
    source 109
    target 839
  ]
  edge [
    source 110
    target 111
  ]
  edge [
    source 110
    target 840
  ]
  edge [
    source 110
    target 841
  ]
  edge [
    source 110
    target 842
  ]
  edge [
    source 110
    target 843
  ]
  edge [
    source 110
    target 844
  ]
  edge [
    source 110
    target 845
  ]
  edge [
    source 111
    target 846
  ]
  edge [
    source 111
    target 539
  ]
  edge [
    source 112
    target 113
  ]
  edge [
    source 112
    target 847
  ]
  edge [
    source 112
    target 848
  ]
  edge [
    source 112
    target 849
  ]
  edge [
    source 113
    target 300
  ]
  edge [
    source 113
    target 850
  ]
  edge [
    source 114
    target 851
  ]
  edge [
    source 114
    target 852
  ]
  edge [
    source 114
    target 853
  ]
  edge [
    source 114
    target 854
  ]
  edge [
    source 114
    target 262
  ]
  edge [
    source 114
    target 855
  ]
  edge [
    source 114
    target 856
  ]
  edge [
    source 114
    target 857
  ]
]
