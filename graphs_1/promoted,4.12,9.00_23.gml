graph [
  maxDegree 14
  minDegree 1
  meanDegree 1.9393939393939394
  density 0.06060606060606061
  graphCliqueNumber 2
  node [
    id 0
    label "imigrant"
    origin "text"
  ]
  node [
    id 1
    label "mongolia"
    origin "text"
  ]
  node [
    id 2
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 3
    label "zatrzymany"
    origin "text"
  ]
  node [
    id 4
    label "granica"
    origin "text"
  ]
  node [
    id 5
    label "bieszczady"
    origin "text"
  ]
  node [
    id 6
    label "cudzoziemiec"
  ]
  node [
    id 7
    label "przybysz"
  ]
  node [
    id 8
    label "migrant"
  ]
  node [
    id 9
    label "imigracja"
  ]
  node [
    id 10
    label "ochrona_uzupe&#322;niaj&#261;ca"
  ]
  node [
    id 11
    label "proceed"
  ]
  node [
    id 12
    label "catch"
  ]
  node [
    id 13
    label "pozosta&#263;"
  ]
  node [
    id 14
    label "osta&#263;_si&#281;"
  ]
  node [
    id 15
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 16
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 17
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 18
    label "change"
  ]
  node [
    id 19
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 20
    label "cz&#322;owiek"
  ]
  node [
    id 21
    label "zakres"
  ]
  node [
    id 22
    label "Ural"
  ]
  node [
    id 23
    label "koniec"
  ]
  node [
    id 24
    label "kres"
  ]
  node [
    id 25
    label "granice"
  ]
  node [
    id 26
    label "granica_pa&#324;stwa"
  ]
  node [
    id 27
    label "pu&#322;ap"
  ]
  node [
    id 28
    label "frontier"
  ]
  node [
    id 29
    label "end"
  ]
  node [
    id 30
    label "miara"
  ]
  node [
    id 31
    label "poj&#281;cie"
  ]
  node [
    id 32
    label "przej&#347;cie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
]
