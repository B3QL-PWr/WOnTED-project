graph [
  maxDegree 11
  minDegree 1
  meanDegree 1.875
  density 0.125
  graphCliqueNumber 2
  node [
    id 0
    label "kr&#243;tki"
    origin "text"
  ]
  node [
    id 1
    label "opis"
    origin "text"
  ]
  node [
    id 2
    label "jednowyrazowy"
  ]
  node [
    id 3
    label "s&#322;aby"
  ]
  node [
    id 4
    label "bliski"
  ]
  node [
    id 5
    label "drobny"
  ]
  node [
    id 6
    label "kr&#243;tko"
  ]
  node [
    id 7
    label "ruch"
  ]
  node [
    id 8
    label "z&#322;y"
  ]
  node [
    id 9
    label "zwi&#281;z&#322;y"
  ]
  node [
    id 10
    label "szybki"
  ]
  node [
    id 11
    label "brak"
  ]
  node [
    id 12
    label "exposition"
  ]
  node [
    id 13
    label "czynno&#347;&#263;"
  ]
  node [
    id 14
    label "wypowied&#378;"
  ]
  node [
    id 15
    label "obja&#347;nienie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
]
