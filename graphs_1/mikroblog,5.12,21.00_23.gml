graph [
  maxDegree 13
  minDegree 1
  meanDegree 1.92
  density 0.08
  graphCliqueNumber 2
  node [
    id 0
    label "kto&#347;"
    origin "text"
  ]
  node [
    id 1
    label "by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "geniusz"
    origin "text"
  ]
  node [
    id 3
    label "xdd"
    origin "text"
  ]
  node [
    id 4
    label "cz&#322;owiek"
  ]
  node [
    id 5
    label "znaczenie"
  ]
  node [
    id 6
    label "go&#347;&#263;"
  ]
  node [
    id 7
    label "osoba"
  ]
  node [
    id 8
    label "posta&#263;"
  ]
  node [
    id 9
    label "si&#281;ga&#263;"
  ]
  node [
    id 10
    label "trwa&#263;"
  ]
  node [
    id 11
    label "obecno&#347;&#263;"
  ]
  node [
    id 12
    label "stan"
  ]
  node [
    id 13
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 14
    label "stand"
  ]
  node [
    id 15
    label "mie&#263;_miejsce"
  ]
  node [
    id 16
    label "uczestniczy&#263;"
  ]
  node [
    id 17
    label "chodzi&#263;"
  ]
  node [
    id 18
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 19
    label "equal"
  ]
  node [
    id 20
    label "doskona&#322;o&#347;&#263;"
  ]
  node [
    id 21
    label "nadcz&#322;owiek"
  ]
  node [
    id 22
    label "duch_opieku&#324;czy"
  ]
  node [
    id 23
    label "m&#243;zg"
  ]
  node [
    id 24
    label "talent"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
]
