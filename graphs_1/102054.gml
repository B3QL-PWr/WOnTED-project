graph [
  maxDegree 21
  minDegree 1
  meanDegree 1.9761904761904763
  density 0.023809523809523808
  graphCliqueNumber 2
  node [
    id 0
    label "oto"
    origin "text"
  ]
  node [
    id 1
    label "dobry"
    origin "text"
  ]
  node [
    id 2
    label "w&#347;r&#243;d"
    origin "text"
  ]
  node [
    id 3
    label "qmamowc&#243;w"
    origin "text"
  ]
  node [
    id 4
    label "laureat"
    origin "text"
  ]
  node [
    id 5
    label "miesi&#281;czny"
    origin "text"
  ]
  node [
    id 6
    label "wyb&#243;r"
    origin "text"
  ]
  node [
    id 7
    label "konkurs"
    origin "text"
  ]
  node [
    id 8
    label "qmam"
    origin "text"
  ]
  node [
    id 9
    label "media"
    origin "text"
  ]
  node [
    id 10
    label "jooke"
    origin "text"
  ]
  node [
    id 11
    label "zszywka"
    origin "text"
  ]
  node [
    id 12
    label "szczepan"
    origin "text"
  ]
  node [
    id 13
    label "by&#263;"
    origin "text"
  ]
  node [
    id 14
    label "mix"
    origin "text"
  ]
  node [
    id 15
    label "szkolny"
    origin "text"
  ]
  node [
    id 16
    label "d&#380;ungla"
    origin "text"
  ]
  node [
    id 17
    label "pomy&#347;lny"
  ]
  node [
    id 18
    label "skuteczny"
  ]
  node [
    id 19
    label "moralny"
  ]
  node [
    id 20
    label "korzystny"
  ]
  node [
    id 21
    label "odpowiedni"
  ]
  node [
    id 22
    label "zwrot"
  ]
  node [
    id 23
    label "dobrze"
  ]
  node [
    id 24
    label "pozytywny"
  ]
  node [
    id 25
    label "grzeczny"
  ]
  node [
    id 26
    label "powitanie"
  ]
  node [
    id 27
    label "mi&#322;y"
  ]
  node [
    id 28
    label "dobroczynny"
  ]
  node [
    id 29
    label "pos&#322;uszny"
  ]
  node [
    id 30
    label "ca&#322;y"
  ]
  node [
    id 31
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 32
    label "czw&#243;rka"
  ]
  node [
    id 33
    label "spokojny"
  ]
  node [
    id 34
    label "&#347;mieszny"
  ]
  node [
    id 35
    label "drogi"
  ]
  node [
    id 36
    label "zdobywca"
  ]
  node [
    id 37
    label "jasny"
  ]
  node [
    id 38
    label "miesi&#281;cznie"
  ]
  node [
    id 39
    label "trzydziestodniowy"
  ]
  node [
    id 40
    label "kilkudziesi&#281;ciodniowy"
  ]
  node [
    id 41
    label "czynno&#347;&#263;"
  ]
  node [
    id 42
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 43
    label "decyzja"
  ]
  node [
    id 44
    label "ewentualno&#347;&#263;"
  ]
  node [
    id 45
    label "pick"
  ]
  node [
    id 46
    label "eliminacje"
  ]
  node [
    id 47
    label "Interwizja"
  ]
  node [
    id 48
    label "emulation"
  ]
  node [
    id 49
    label "impreza"
  ]
  node [
    id 50
    label "casting"
  ]
  node [
    id 51
    label "Eurowizja"
  ]
  node [
    id 52
    label "nab&#243;r"
  ]
  node [
    id 53
    label "przekazior"
  ]
  node [
    id 54
    label "mass-media"
  ]
  node [
    id 55
    label "uzbrajanie"
  ]
  node [
    id 56
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 57
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 58
    label "medium"
  ]
  node [
    id 59
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 60
    label "tom"
  ]
  node [
    id 61
    label "zszywacz"
  ]
  node [
    id 62
    label "drucik"
  ]
  node [
    id 63
    label "si&#281;ga&#263;"
  ]
  node [
    id 64
    label "trwa&#263;"
  ]
  node [
    id 65
    label "obecno&#347;&#263;"
  ]
  node [
    id 66
    label "stan"
  ]
  node [
    id 67
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 68
    label "stand"
  ]
  node [
    id 69
    label "mie&#263;_miejsce"
  ]
  node [
    id 70
    label "uczestniczy&#263;"
  ]
  node [
    id 71
    label "chodzi&#263;"
  ]
  node [
    id 72
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 73
    label "equal"
  ]
  node [
    id 74
    label "szkolnie"
  ]
  node [
    id 75
    label "szkoleniowy"
  ]
  node [
    id 76
    label "podstawowy"
  ]
  node [
    id 77
    label "prosty"
  ]
  node [
    id 78
    label "formacja_ro&#347;linna"
  ]
  node [
    id 79
    label "miejsce"
  ]
  node [
    id 80
    label "skupienie"
  ]
  node [
    id 81
    label "las"
  ]
  node [
    id 82
    label "forest"
  ]
  node [
    id 83
    label "pl&#261;tanina"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 53
  ]
  edge [
    source 9
    target 54
  ]
  edge [
    source 9
    target 55
  ]
  edge [
    source 9
    target 56
  ]
  edge [
    source 9
    target 57
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 60
  ]
  edge [
    source 11
    target 61
  ]
  edge [
    source 11
    target 62
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 63
  ]
  edge [
    source 13
    target 64
  ]
  edge [
    source 13
    target 65
  ]
  edge [
    source 13
    target 66
  ]
  edge [
    source 13
    target 67
  ]
  edge [
    source 13
    target 68
  ]
  edge [
    source 13
    target 69
  ]
  edge [
    source 13
    target 70
  ]
  edge [
    source 13
    target 71
  ]
  edge [
    source 13
    target 72
  ]
  edge [
    source 13
    target 73
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 74
  ]
  edge [
    source 15
    target 75
  ]
  edge [
    source 15
    target 76
  ]
  edge [
    source 15
    target 77
  ]
  edge [
    source 16
    target 78
  ]
  edge [
    source 16
    target 79
  ]
  edge [
    source 16
    target 80
  ]
  edge [
    source 16
    target 81
  ]
  edge [
    source 16
    target 82
  ]
  edge [
    source 16
    target 83
  ]
]
