graph [
  maxDegree 20
  minDegree 1
  meanDegree 1.9230769230769231
  density 0.07692307692307693
  graphCliqueNumber 2
  node [
    id 0
    label "link"
    origin "text"
  ]
  node [
    id 1
    label "piotrzyla"
    origin "text"
  ]
  node [
    id 2
    label "troch&#281;"
    origin "text"
  ]
  node [
    id 3
    label "skok"
    origin "text"
  ]
  node [
    id 4
    label "pewno"
    origin "text"
  ]
  node [
    id 5
    label "logikarozowychpaskow"
    origin "text"
  ]
  node [
    id 6
    label "buton"
  ]
  node [
    id 7
    label "odsy&#322;acz"
  ]
  node [
    id 8
    label "wybicie"
  ]
  node [
    id 9
    label "konkurencja"
  ]
  node [
    id 10
    label "derail"
  ]
  node [
    id 11
    label "ptak"
  ]
  node [
    id 12
    label "ruch"
  ]
  node [
    id 13
    label "l&#261;dowanie"
  ]
  node [
    id 14
    label "&#322;apa"
  ]
  node [
    id 15
    label "struktura_anatomiczna"
  ]
  node [
    id 16
    label "stroke"
  ]
  node [
    id 17
    label "gimnastyka_przyrz&#261;dowa"
  ]
  node [
    id 18
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 19
    label "zmiana"
  ]
  node [
    id 20
    label "caper"
  ]
  node [
    id 21
    label "zaj&#261;c"
  ]
  node [
    id 22
    label "naskok"
  ]
  node [
    id 23
    label "napad"
  ]
  node [
    id 24
    label "ko&#347;&#263;_skokowa"
  ]
  node [
    id 25
    label "noga"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 4
    target 5
  ]
]
