graph [
  maxDegree 22
  minDegree 1
  meanDegree 2
  density 0.023255813953488372
  graphCliqueNumber 2
  node [
    id 0
    label "anonimowemirkowyznania"
    origin "text"
  ]
  node [
    id 1
    label "byleby&#263;"
    origin "text"
  ]
  node [
    id 2
    label "zwiazku"
    origin "text"
  ]
  node [
    id 3
    label "dziewczyna"
    origin "text"
  ]
  node [
    id 4
    label "rok"
    origin "text"
  ]
  node [
    id 5
    label "wyjazd"
    origin "text"
  ]
  node [
    id 6
    label "holandia"
    origin "text"
  ]
  node [
    id 7
    label "odjebala"
    origin "text"
  ]
  node [
    id 8
    label "taka"
    origin "text"
  ]
  node [
    id 9
    label "akcja"
    origin "text"
  ]
  node [
    id 10
    label "szok"
    origin "text"
  ]
  node [
    id 11
    label "ale"
    origin "text"
  ]
  node [
    id 12
    label "kolej"
    origin "text"
  ]
  node [
    id 13
    label "cz&#322;owiek"
  ]
  node [
    id 14
    label "dziewka"
  ]
  node [
    id 15
    label "dziewoja"
  ]
  node [
    id 16
    label "siksa"
  ]
  node [
    id 17
    label "partnerka"
  ]
  node [
    id 18
    label "dziewczynina"
  ]
  node [
    id 19
    label "dziunia"
  ]
  node [
    id 20
    label "sympatia"
  ]
  node [
    id 21
    label "dziewcz&#281;"
  ]
  node [
    id 22
    label "s&#322;u&#380;&#261;ca"
  ]
  node [
    id 23
    label "kora"
  ]
  node [
    id 24
    label "m&#322;&#243;dka"
  ]
  node [
    id 25
    label "dziecina"
  ]
  node [
    id 26
    label "sikorka"
  ]
  node [
    id 27
    label "stulecie"
  ]
  node [
    id 28
    label "kalendarz"
  ]
  node [
    id 29
    label "czas"
  ]
  node [
    id 30
    label "pora_roku"
  ]
  node [
    id 31
    label "cykl_astronomiczny"
  ]
  node [
    id 32
    label "p&#243;&#322;rocze"
  ]
  node [
    id 33
    label "grupa"
  ]
  node [
    id 34
    label "kwarta&#322;"
  ]
  node [
    id 35
    label "kurs"
  ]
  node [
    id 36
    label "jubileusz"
  ]
  node [
    id 37
    label "miesi&#261;c"
  ]
  node [
    id 38
    label "lata"
  ]
  node [
    id 39
    label "martwy_sezon"
  ]
  node [
    id 40
    label "podr&#243;&#380;"
  ]
  node [
    id 41
    label "digression"
  ]
  node [
    id 42
    label "Bangladesz"
  ]
  node [
    id 43
    label "jednostka_monetarna"
  ]
  node [
    id 44
    label "zagrywka"
  ]
  node [
    id 45
    label "czyn"
  ]
  node [
    id 46
    label "czynno&#347;&#263;"
  ]
  node [
    id 47
    label "wysoko&#347;&#263;"
  ]
  node [
    id 48
    label "stock"
  ]
  node [
    id 49
    label "gra"
  ]
  node [
    id 50
    label "w&#281;ze&#322;"
  ]
  node [
    id 51
    label "instrument_strunowy"
  ]
  node [
    id 52
    label "dywidenda"
  ]
  node [
    id 53
    label "przebieg"
  ]
  node [
    id 54
    label "udzia&#322;"
  ]
  node [
    id 55
    label "occupation"
  ]
  node [
    id 56
    label "jazda"
  ]
  node [
    id 57
    label "wydarzenie"
  ]
  node [
    id 58
    label "commotion"
  ]
  node [
    id 59
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 60
    label "sp&#243;&#322;ka_akcyjna"
  ]
  node [
    id 61
    label "operacja"
  ]
  node [
    id 62
    label "shock_absorber"
  ]
  node [
    id 63
    label "wstrz&#261;s"
  ]
  node [
    id 64
    label "zaskoczenie"
  ]
  node [
    id 65
    label "prze&#380;ycie"
  ]
  node [
    id 66
    label "piwo"
  ]
  node [
    id 67
    label "pojazd_kolejowy"
  ]
  node [
    id 68
    label "tor"
  ]
  node [
    id 69
    label "tender"
  ]
  node [
    id 70
    label "droga"
  ]
  node [
    id 71
    label "blokada"
  ]
  node [
    id 72
    label "wagon"
  ]
  node [
    id 73
    label "run"
  ]
  node [
    id 74
    label "cedu&#322;a"
  ]
  node [
    id 75
    label "kolejno&#347;&#263;"
  ]
  node [
    id 76
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 77
    label "skomunikowanie_si&#281;"
  ]
  node [
    id 78
    label "trakcja"
  ]
  node [
    id 79
    label "linia"
  ]
  node [
    id 80
    label "cug"
  ]
  node [
    id 81
    label "poci&#261;g"
  ]
  node [
    id 82
    label "pocz&#261;tek"
  ]
  node [
    id 83
    label "nast&#281;pstwo"
  ]
  node [
    id 84
    label "lokomotywa"
  ]
  node [
    id 85
    label "skomunikowa&#263;_si&#281;"
  ]
  node [
    id 86
    label "proces"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 44
  ]
  edge [
    source 9
    target 45
  ]
  edge [
    source 9
    target 46
  ]
  edge [
    source 9
    target 47
  ]
  edge [
    source 9
    target 48
  ]
  edge [
    source 9
    target 49
  ]
  edge [
    source 9
    target 50
  ]
  edge [
    source 9
    target 51
  ]
  edge [
    source 9
    target 52
  ]
  edge [
    source 9
    target 53
  ]
  edge [
    source 9
    target 54
  ]
  edge [
    source 9
    target 55
  ]
  edge [
    source 9
    target 56
  ]
  edge [
    source 9
    target 57
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 62
  ]
  edge [
    source 10
    target 63
  ]
  edge [
    source 10
    target 64
  ]
  edge [
    source 10
    target 65
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 66
  ]
  edge [
    source 12
    target 67
  ]
  edge [
    source 12
    target 29
  ]
  edge [
    source 12
    target 68
  ]
  edge [
    source 12
    target 69
  ]
  edge [
    source 12
    target 70
  ]
  edge [
    source 12
    target 71
  ]
  edge [
    source 12
    target 72
  ]
  edge [
    source 12
    target 73
  ]
  edge [
    source 12
    target 74
  ]
  edge [
    source 12
    target 75
  ]
  edge [
    source 12
    target 76
  ]
  edge [
    source 12
    target 77
  ]
  edge [
    source 12
    target 78
  ]
  edge [
    source 12
    target 79
  ]
  edge [
    source 12
    target 80
  ]
  edge [
    source 12
    target 81
  ]
  edge [
    source 12
    target 82
  ]
  edge [
    source 12
    target 83
  ]
  edge [
    source 12
    target 84
  ]
  edge [
    source 12
    target 85
  ]
  edge [
    source 12
    target 86
  ]
]
