graph [
  maxDegree 7
  minDegree 1
  meanDegree 1.75
  density 0.25
  graphCliqueNumber 2
  node [
    id 0
    label "kopiec"
    origin "text"
  ]
  node [
    id 1
    label "dwumianowy"
    origin "text"
  ]
  node [
    id 2
    label "struktura"
  ]
  node [
    id 3
    label "Kopiec_Pi&#322;sudskiego"
  ]
  node [
    id 4
    label "usypisko"
  ]
  node [
    id 5
    label "kszta&#322;t"
  ]
  node [
    id 6
    label "knoll"
  ]
  node [
    id 7
    label "pryzma"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
]
