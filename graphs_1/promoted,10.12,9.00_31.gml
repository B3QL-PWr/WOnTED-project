graph [
  maxDegree 17
  minDegree 1
  meanDegree 1.9770114942528736
  density 0.022988505747126436
  graphCliqueNumber 2
  node [
    id 0
    label "trzy"
    origin "text"
  ]
  node [
    id 1
    label "polak"
    origin "text"
  ]
  node [
    id 2
    label "siedzie&#263;"
    origin "text"
  ]
  node [
    id 3
    label "areszt"
    origin "text"
  ]
  node [
    id 4
    label "pow&#243;d"
    origin "text"
  ]
  node [
    id 5
    label "wypadek"
    origin "text"
  ]
  node [
    id 6
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 7
    label "zgin&#261;&#263;"
    origin "text"
  ]
  node [
    id 8
    label "letni"
    origin "text"
  ]
  node [
    id 9
    label "m&#281;&#380;czyzna"
    origin "text"
  ]
  node [
    id 10
    label "polski"
  ]
  node [
    id 11
    label "trwa&#263;"
  ]
  node [
    id 12
    label "sit"
  ]
  node [
    id 13
    label "pause"
  ]
  node [
    id 14
    label "ptak"
  ]
  node [
    id 15
    label "garowa&#263;"
  ]
  node [
    id 16
    label "mieszka&#263;"
  ]
  node [
    id 17
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 18
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 19
    label "przebywa&#263;"
  ]
  node [
    id 20
    label "brood"
  ]
  node [
    id 21
    label "zwierz&#281;"
  ]
  node [
    id 22
    label "doprowadza&#263;"
  ]
  node [
    id 23
    label "spoczywa&#263;"
  ]
  node [
    id 24
    label "tkwi&#263;"
  ]
  node [
    id 25
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 26
    label "ul"
  ]
  node [
    id 27
    label "zarz&#261;d"
  ]
  node [
    id 28
    label "pozbawienie_wolno&#347;ci"
  ]
  node [
    id 29
    label "procedura"
  ]
  node [
    id 30
    label "miejsce_odosobnienia"
  ]
  node [
    id 31
    label "ograniczenie"
  ]
  node [
    id 32
    label "poj&#281;cie"
  ]
  node [
    id 33
    label "strona"
  ]
  node [
    id 34
    label "przyczyna"
  ]
  node [
    id 35
    label "matuszka"
  ]
  node [
    id 36
    label "geneza"
  ]
  node [
    id 37
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 38
    label "czynnik"
  ]
  node [
    id 39
    label "poci&#261;ganie"
  ]
  node [
    id 40
    label "rezultat"
  ]
  node [
    id 41
    label "uprz&#261;&#380;"
  ]
  node [
    id 42
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 43
    label "subject"
  ]
  node [
    id 44
    label "czynno&#347;&#263;"
  ]
  node [
    id 45
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 46
    label "motyw"
  ]
  node [
    id 47
    label "ubezpieczenie_wypadkowe"
  ]
  node [
    id 48
    label "fabu&#322;a"
  ]
  node [
    id 49
    label "przebiec"
  ]
  node [
    id 50
    label "wydarzenie"
  ]
  node [
    id 51
    label "happening"
  ]
  node [
    id 52
    label "przebiegni&#281;cie"
  ]
  node [
    id 53
    label "event"
  ]
  node [
    id 54
    label "charakter"
  ]
  node [
    id 55
    label "fail"
  ]
  node [
    id 56
    label "os&#322;abn&#261;&#263;"
  ]
  node [
    id 57
    label "gulf"
  ]
  node [
    id 58
    label "po&#380;egna&#263;_si&#281;_z_&#380;yciem"
  ]
  node [
    id 59
    label "przesta&#263;"
  ]
  node [
    id 60
    label "przepa&#347;&#263;"
  ]
  node [
    id 61
    label "sko&#324;czy&#263;"
  ]
  node [
    id 62
    label "znikn&#261;&#263;"
  ]
  node [
    id 63
    label "przegra&#263;"
  ]
  node [
    id 64
    label "die"
  ]
  node [
    id 65
    label "nijaki"
  ]
  node [
    id 66
    label "sezonowy"
  ]
  node [
    id 67
    label "letnio"
  ]
  node [
    id 68
    label "s&#322;oneczny"
  ]
  node [
    id 69
    label "weso&#322;y"
  ]
  node [
    id 70
    label "oboj&#281;tny"
  ]
  node [
    id 71
    label "latowy"
  ]
  node [
    id 72
    label "ciep&#322;y"
  ]
  node [
    id 73
    label "typowy"
  ]
  node [
    id 74
    label "ch&#322;opina"
  ]
  node [
    id 75
    label "cz&#322;owiek"
  ]
  node [
    id 76
    label "bratek"
  ]
  node [
    id 77
    label "jegomo&#347;&#263;"
  ]
  node [
    id 78
    label "doros&#322;y"
  ]
  node [
    id 79
    label "samiec"
  ]
  node [
    id 80
    label "ojciec"
  ]
  node [
    id 81
    label "twardziel"
  ]
  node [
    id 82
    label "androlog"
  ]
  node [
    id 83
    label "pa&#324;stwo"
  ]
  node [
    id 84
    label "m&#261;&#380;"
  ]
  node [
    id 85
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 86
    label "andropauza"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
]
