graph [
  maxDegree 7
  minDegree 1
  meanDegree 1.8
  density 0.2
  graphCliqueNumber 2
  node [
    id 0
    label "portal"
    origin "text"
  ]
  node [
    id 1
    label "sp&#281;dzi&#263;by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "lato"
    origin "text"
  ]
  node [
    id 3
    label "obramienie"
  ]
  node [
    id 4
    label "forum"
  ]
  node [
    id 5
    label "serwis_internetowy"
  ]
  node [
    id 6
    label "wej&#347;cie"
  ]
  node [
    id 7
    label "Onet"
  ]
  node [
    id 8
    label "archiwolta"
  ]
  node [
    id 9
    label "pora_roku"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 9
  ]
]
