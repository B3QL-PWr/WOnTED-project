graph [
  maxDegree 27
  minDegree 1
  meanDegree 2.0256410256410255
  density 0.013068651778329199
  graphCliqueNumber 3
  node [
    id 0
    label "bardzo"
    origin "text"
  ]
  node [
    id 1
    label "interesowa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "kolekcjoner"
    origin "text"
  ]
  node [
    id 3
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 4
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 5
    label "zapewne"
    origin "text"
  ]
  node [
    id 6
    label "magazynowa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "ilo&#347;&#263;"
    origin "text"
  ]
  node [
    id 8
    label "tre&#347;&#263;"
    origin "text"
  ]
  node [
    id 9
    label "wielokrotnie"
    origin "text"
  ]
  node [
    id 10
    label "przekracza&#263;"
    origin "text"
  ]
  node [
    id 11
    label "indywidualny"
    origin "text"
  ]
  node [
    id 12
    label "mo&#380;liwo&#347;ci"
    origin "text"
  ]
  node [
    id 13
    label "przetrawi&#263;"
    origin "text"
  ]
  node [
    id 14
    label "ale"
    origin "text"
  ]
  node [
    id 15
    label "robi&#263;"
    origin "text"
  ]
  node [
    id 16
    label "wiara"
    origin "text"
  ]
  node [
    id 17
    label "naiwny"
    origin "text"
  ]
  node [
    id 18
    label "&#347;ci&#261;ga&#263;"
    origin "text"
  ]
  node [
    id 19
    label "s&#322;u&#380;y&#263;"
    origin "text"
  ]
  node [
    id 20
    label "ogl&#261;da&#263;"
    origin "text"
  ]
  node [
    id 21
    label "w_chuj"
  ]
  node [
    id 22
    label "rozciekawia&#263;"
  ]
  node [
    id 23
    label "sake"
  ]
  node [
    id 24
    label "hobbysta"
  ]
  node [
    id 25
    label "zbieracz"
  ]
  node [
    id 26
    label "asymilowa&#263;"
  ]
  node [
    id 27
    label "wapniak"
  ]
  node [
    id 28
    label "dwun&#243;g"
  ]
  node [
    id 29
    label "polifag"
  ]
  node [
    id 30
    label "wz&#243;r"
  ]
  node [
    id 31
    label "profanum"
  ]
  node [
    id 32
    label "hominid"
  ]
  node [
    id 33
    label "homo_sapiens"
  ]
  node [
    id 34
    label "nasada"
  ]
  node [
    id 35
    label "podw&#322;adny"
  ]
  node [
    id 36
    label "ludzko&#347;&#263;"
  ]
  node [
    id 37
    label "os&#322;abianie"
  ]
  node [
    id 38
    label "mikrokosmos"
  ]
  node [
    id 39
    label "portrecista"
  ]
  node [
    id 40
    label "duch"
  ]
  node [
    id 41
    label "oddzia&#322;ywanie"
  ]
  node [
    id 42
    label "g&#322;owa"
  ]
  node [
    id 43
    label "asymilowanie"
  ]
  node [
    id 44
    label "osoba"
  ]
  node [
    id 45
    label "os&#322;abia&#263;"
  ]
  node [
    id 46
    label "figura"
  ]
  node [
    id 47
    label "Adam"
  ]
  node [
    id 48
    label "senior"
  ]
  node [
    id 49
    label "antropochoria"
  ]
  node [
    id 50
    label "posta&#263;"
  ]
  node [
    id 51
    label "throng"
  ]
  node [
    id 52
    label "gromadzi&#263;"
  ]
  node [
    id 53
    label "przechowywa&#263;"
  ]
  node [
    id 54
    label "rozmiar"
  ]
  node [
    id 55
    label "part"
  ]
  node [
    id 56
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 57
    label "zawarto&#347;&#263;"
  ]
  node [
    id 58
    label "temat"
  ]
  node [
    id 59
    label "istota"
  ]
  node [
    id 60
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 61
    label "informacja"
  ]
  node [
    id 62
    label "cz&#281;stokro&#263;"
  ]
  node [
    id 63
    label "tylekro&#263;"
  ]
  node [
    id 64
    label "du&#380;o"
  ]
  node [
    id 65
    label "wielekro&#263;"
  ]
  node [
    id 66
    label "wielokrotny"
  ]
  node [
    id 67
    label "transgress"
  ]
  node [
    id 68
    label "osi&#261;ga&#263;"
  ]
  node [
    id 69
    label "przebywa&#263;"
  ]
  node [
    id 70
    label "conflict"
  ]
  node [
    id 71
    label "mija&#263;"
  ]
  node [
    id 72
    label "ograniczenie"
  ]
  node [
    id 73
    label "appear"
  ]
  node [
    id 74
    label "swoisty"
  ]
  node [
    id 75
    label "indywidualnie"
  ]
  node [
    id 76
    label "osobny"
  ]
  node [
    id 77
    label "capability"
  ]
  node [
    id 78
    label "zdolno&#347;&#263;"
  ]
  node [
    id 79
    label "potencja&#322;"
  ]
  node [
    id 80
    label "zniszczy&#263;"
  ]
  node [
    id 81
    label "przerobi&#263;"
  ]
  node [
    id 82
    label "poch&#322;on&#261;&#263;"
  ]
  node [
    id 83
    label "roz&#322;o&#380;y&#263;"
  ]
  node [
    id 84
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 85
    label "przemy&#347;le&#263;"
  ]
  node [
    id 86
    label "digest"
  ]
  node [
    id 87
    label "piwo"
  ]
  node [
    id 88
    label "tentegowa&#263;"
  ]
  node [
    id 89
    label "urz&#261;dza&#263;"
  ]
  node [
    id 90
    label "give"
  ]
  node [
    id 91
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 92
    label "czyni&#263;"
  ]
  node [
    id 93
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 94
    label "post&#281;powa&#263;"
  ]
  node [
    id 95
    label "wydala&#263;"
  ]
  node [
    id 96
    label "oszukiwa&#263;"
  ]
  node [
    id 97
    label "organizowa&#263;"
  ]
  node [
    id 98
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 99
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 100
    label "work"
  ]
  node [
    id 101
    label "przerabia&#263;"
  ]
  node [
    id 102
    label "stylizowa&#263;"
  ]
  node [
    id 103
    label "falowa&#263;"
  ]
  node [
    id 104
    label "act"
  ]
  node [
    id 105
    label "peddle"
  ]
  node [
    id 106
    label "ukazywa&#263;"
  ]
  node [
    id 107
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 108
    label "praca"
  ]
  node [
    id 109
    label "przes&#261;dny"
  ]
  node [
    id 110
    label "konwikcja"
  ]
  node [
    id 111
    label "belief"
  ]
  node [
    id 112
    label "pogl&#261;d"
  ]
  node [
    id 113
    label "faith"
  ]
  node [
    id 114
    label "postawa"
  ]
  node [
    id 115
    label "naiwnie"
  ]
  node [
    id 116
    label "g&#322;upi"
  ]
  node [
    id 117
    label "prostoduszny"
  ]
  node [
    id 118
    label "poczciwy"
  ]
  node [
    id 119
    label "zmusza&#263;"
  ]
  node [
    id 120
    label "stiffen"
  ]
  node [
    id 121
    label "bind"
  ]
  node [
    id 122
    label "przepisywa&#263;"
  ]
  node [
    id 123
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 124
    label "nale&#380;no&#347;&#263;"
  ]
  node [
    id 125
    label "sprawdzian"
  ]
  node [
    id 126
    label "pozyskiwa&#263;"
  ]
  node [
    id 127
    label "znosi&#263;"
  ]
  node [
    id 128
    label "kurczy&#263;"
  ]
  node [
    id 129
    label "odprowadza&#263;"
  ]
  node [
    id 130
    label "kopiowa&#263;"
  ]
  node [
    id 131
    label "przewi&#261;zywa&#263;"
  ]
  node [
    id 132
    label "kra&#347;&#263;"
  ]
  node [
    id 133
    label "clamp"
  ]
  node [
    id 134
    label "gromadzi&#263;_si&#281;"
  ]
  node [
    id 135
    label "powodowa&#263;"
  ]
  node [
    id 136
    label "zdejmowa&#263;"
  ]
  node [
    id 137
    label "ciecz"
  ]
  node [
    id 138
    label "use"
  ]
  node [
    id 139
    label "trwa&#263;"
  ]
  node [
    id 140
    label "by&#263;"
  ]
  node [
    id 141
    label "&#380;o&#322;nierz"
  ]
  node [
    id 142
    label "pies"
  ]
  node [
    id 143
    label "wait"
  ]
  node [
    id 144
    label "pomaga&#263;"
  ]
  node [
    id 145
    label "cel"
  ]
  node [
    id 146
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 147
    label "przydawa&#263;_si&#281;"
  ]
  node [
    id 148
    label "pracowa&#263;"
  ]
  node [
    id 149
    label "suffice"
  ]
  node [
    id 150
    label "match"
  ]
  node [
    id 151
    label "s&#322;ugiwa&#263;"
  ]
  node [
    id 152
    label "notice"
  ]
  node [
    id 153
    label "zapoznawa&#263;_si&#281;"
  ]
  node [
    id 154
    label "styka&#263;_si&#281;"
  ]
  node [
    id 155
    label "w&#322;&#261;cza&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 10
    target 69
  ]
  edge [
    source 10
    target 15
  ]
  edge [
    source 10
    target 70
  ]
  edge [
    source 10
    target 71
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 74
  ]
  edge [
    source 11
    target 75
  ]
  edge [
    source 11
    target 76
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 77
  ]
  edge [
    source 12
    target 78
  ]
  edge [
    source 12
    target 79
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 80
  ]
  edge [
    source 13
    target 81
  ]
  edge [
    source 13
    target 82
  ]
  edge [
    source 13
    target 83
  ]
  edge [
    source 13
    target 84
  ]
  edge [
    source 13
    target 85
  ]
  edge [
    source 13
    target 86
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 87
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 88
  ]
  edge [
    source 15
    target 89
  ]
  edge [
    source 15
    target 90
  ]
  edge [
    source 15
    target 91
  ]
  edge [
    source 15
    target 92
  ]
  edge [
    source 15
    target 93
  ]
  edge [
    source 15
    target 94
  ]
  edge [
    source 15
    target 95
  ]
  edge [
    source 15
    target 96
  ]
  edge [
    source 15
    target 97
  ]
  edge [
    source 15
    target 98
  ]
  edge [
    source 15
    target 99
  ]
  edge [
    source 15
    target 100
  ]
  edge [
    source 15
    target 101
  ]
  edge [
    source 15
    target 102
  ]
  edge [
    source 15
    target 103
  ]
  edge [
    source 15
    target 104
  ]
  edge [
    source 15
    target 105
  ]
  edge [
    source 15
    target 106
  ]
  edge [
    source 15
    target 107
  ]
  edge [
    source 15
    target 108
  ]
  edge [
    source 15
    target 18
  ]
  edge [
    source 15
    target 19
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 109
  ]
  edge [
    source 16
    target 110
  ]
  edge [
    source 16
    target 111
  ]
  edge [
    source 16
    target 112
  ]
  edge [
    source 16
    target 113
  ]
  edge [
    source 16
    target 114
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 115
  ]
  edge [
    source 17
    target 116
  ]
  edge [
    source 17
    target 117
  ]
  edge [
    source 17
    target 118
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 119
  ]
  edge [
    source 18
    target 120
  ]
  edge [
    source 18
    target 121
  ]
  edge [
    source 18
    target 122
  ]
  edge [
    source 18
    target 123
  ]
  edge [
    source 18
    target 124
  ]
  edge [
    source 18
    target 125
  ]
  edge [
    source 18
    target 126
  ]
  edge [
    source 18
    target 127
  ]
  edge [
    source 18
    target 128
  ]
  edge [
    source 18
    target 129
  ]
  edge [
    source 18
    target 130
  ]
  edge [
    source 18
    target 131
  ]
  edge [
    source 18
    target 132
  ]
  edge [
    source 18
    target 133
  ]
  edge [
    source 18
    target 134
  ]
  edge [
    source 18
    target 135
  ]
  edge [
    source 18
    target 136
  ]
  edge [
    source 18
    target 137
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 138
  ]
  edge [
    source 19
    target 139
  ]
  edge [
    source 19
    target 140
  ]
  edge [
    source 19
    target 141
  ]
  edge [
    source 19
    target 142
  ]
  edge [
    source 19
    target 143
  ]
  edge [
    source 19
    target 144
  ]
  edge [
    source 19
    target 145
  ]
  edge [
    source 19
    target 146
  ]
  edge [
    source 19
    target 147
  ]
  edge [
    source 19
    target 148
  ]
  edge [
    source 19
    target 149
  ]
  edge [
    source 19
    target 150
  ]
  edge [
    source 19
    target 151
  ]
  edge [
    source 20
    target 152
  ]
  edge [
    source 20
    target 153
  ]
  edge [
    source 20
    target 154
  ]
  edge [
    source 20
    target 155
  ]
]
