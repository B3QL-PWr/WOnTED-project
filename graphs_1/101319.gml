graph [
  maxDegree 33
  minDegree 1
  meanDegree 2.2083333333333335
  density 0.015442890442890442
  graphCliqueNumber 4
  node [
    id 0
    label "saab"
    origin "text"
  ]
  node [
    id 1
    label "samoch&#243;d"
    origin "text"
  ]
  node [
    id 2
    label "osobowy"
    origin "text"
  ]
  node [
    id 3
    label "przez"
    origin "text"
  ]
  node [
    id 4
    label "firma"
    origin "text"
  ]
  node [
    id 5
    label "lato"
    origin "text"
  ]
  node [
    id 6
    label "odpowiednik"
    origin "text"
  ]
  node [
    id 7
    label "wersja"
    origin "text"
  ]
  node [
    id 8
    label "kombi"
    origin "text"
  ]
  node [
    id 9
    label "by&#263;"
    origin "text"
  ]
  node [
    id 10
    label "model"
    origin "text"
  ]
  node [
    id 11
    label "ten"
    origin "text"
  ]
  node [
    id 12
    label "powstawa&#263;"
    origin "text"
  ]
  node [
    id 13
    label "uusikaupunki"
    origin "text"
  ]
  node [
    id 14
    label "finlandia"
    origin "text"
  ]
  node [
    id 15
    label "zak&#322;ad"
    origin "text"
  ]
  node [
    id 16
    label "valmet"
    origin "text"
  ]
  node [
    id 17
    label "automotive"
    origin "text"
  ]
  node [
    id 18
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 19
    label "mie&#263;"
    origin "text"
  ]
  node [
    id 20
    label "udzia&#322;"
    origin "text"
  ]
  node [
    id 21
    label "samoch&#243;d_osobowy"
  ]
  node [
    id 22
    label "Saab"
  ]
  node [
    id 23
    label "baga&#380;nik"
  ]
  node [
    id 24
    label "immobilizer"
  ]
  node [
    id 25
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 26
    label "ko&#322;o_samochodowe"
  ]
  node [
    id 27
    label "poduszka_powietrzna"
  ]
  node [
    id 28
    label "dachowanie"
  ]
  node [
    id 29
    label "dwu&#347;lad"
  ]
  node [
    id 30
    label "deska_rozdzielcza"
  ]
  node [
    id 31
    label "poci&#261;g_drogowy"
  ]
  node [
    id 32
    label "kierownica"
  ]
  node [
    id 33
    label "pojazd_drogowy"
  ]
  node [
    id 34
    label "uk&#322;ad_kierowniczy"
  ]
  node [
    id 35
    label "pompa_wodna"
  ]
  node [
    id 36
    label "silnik"
  ]
  node [
    id 37
    label "wycieraczka"
  ]
  node [
    id 38
    label "bak"
  ]
  node [
    id 39
    label "ABS"
  ]
  node [
    id 40
    label "most"
  ]
  node [
    id 41
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 42
    label "spryskiwacz"
  ]
  node [
    id 43
    label "t&#322;umik"
  ]
  node [
    id 44
    label "tempomat"
  ]
  node [
    id 45
    label "osobowo"
  ]
  node [
    id 46
    label "cz&#322;owiek"
  ]
  node [
    id 47
    label "MAC"
  ]
  node [
    id 48
    label "Hortex"
  ]
  node [
    id 49
    label "reengineering"
  ]
  node [
    id 50
    label "nazwa_w&#322;asna"
  ]
  node [
    id 51
    label "podmiot_gospodarczy"
  ]
  node [
    id 52
    label "Google"
  ]
  node [
    id 53
    label "zaufanie"
  ]
  node [
    id 54
    label "biurowiec"
  ]
  node [
    id 55
    label "interes"
  ]
  node [
    id 56
    label "zasoby_ludzkie"
  ]
  node [
    id 57
    label "networking"
  ]
  node [
    id 58
    label "paczkarnia"
  ]
  node [
    id 59
    label "Canon"
  ]
  node [
    id 60
    label "HP"
  ]
  node [
    id 61
    label "Baltona"
  ]
  node [
    id 62
    label "Pewex"
  ]
  node [
    id 63
    label "MAN_SE"
  ]
  node [
    id 64
    label "Apeks"
  ]
  node [
    id 65
    label "zasoby"
  ]
  node [
    id 66
    label "Orbis"
  ]
  node [
    id 67
    label "miejsce_pracy"
  ]
  node [
    id 68
    label "siedziba"
  ]
  node [
    id 69
    label "Spo&#322;em"
  ]
  node [
    id 70
    label "przedsi&#281;biorczo&#347;&#263;"
  ]
  node [
    id 71
    label "Orlen"
  ]
  node [
    id 72
    label "klasa"
  ]
  node [
    id 73
    label "pora_roku"
  ]
  node [
    id 74
    label "odmiana"
  ]
  node [
    id 75
    label "typ"
  ]
  node [
    id 76
    label "posta&#263;"
  ]
  node [
    id 77
    label "nadwozie"
  ]
  node [
    id 78
    label "si&#281;ga&#263;"
  ]
  node [
    id 79
    label "trwa&#263;"
  ]
  node [
    id 80
    label "obecno&#347;&#263;"
  ]
  node [
    id 81
    label "stan"
  ]
  node [
    id 82
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 83
    label "stand"
  ]
  node [
    id 84
    label "mie&#263;_miejsce"
  ]
  node [
    id 85
    label "uczestniczy&#263;"
  ]
  node [
    id 86
    label "chodzi&#263;"
  ]
  node [
    id 87
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 88
    label "equal"
  ]
  node [
    id 89
    label "pozowa&#263;"
  ]
  node [
    id 90
    label "ideal"
  ]
  node [
    id 91
    label "matryca"
  ]
  node [
    id 92
    label "imitacja"
  ]
  node [
    id 93
    label "ruch"
  ]
  node [
    id 94
    label "motif"
  ]
  node [
    id 95
    label "pozowanie"
  ]
  node [
    id 96
    label "wz&#243;r"
  ]
  node [
    id 97
    label "miniatura"
  ]
  node [
    id 98
    label "prezenter"
  ]
  node [
    id 99
    label "facet"
  ]
  node [
    id 100
    label "orygina&#322;"
  ]
  node [
    id 101
    label "mildew"
  ]
  node [
    id 102
    label "spos&#243;b"
  ]
  node [
    id 103
    label "zi&#243;&#322;ko"
  ]
  node [
    id 104
    label "adaptation"
  ]
  node [
    id 105
    label "okre&#347;lony"
  ]
  node [
    id 106
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 107
    label "buntowa&#263;_si&#281;"
  ]
  node [
    id 108
    label "rise"
  ]
  node [
    id 109
    label "spring"
  ]
  node [
    id 110
    label "stawa&#263;"
  ]
  node [
    id 111
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 112
    label "sprzeciwia&#263;_si&#281;"
  ]
  node [
    id 113
    label "plot"
  ]
  node [
    id 114
    label "podnosi&#263;_si&#281;"
  ]
  node [
    id 115
    label "uwalnia&#263;_si&#281;"
  ]
  node [
    id 116
    label "publish"
  ]
  node [
    id 117
    label "czyn"
  ]
  node [
    id 118
    label "company"
  ]
  node [
    id 119
    label "zak&#322;adka"
  ]
  node [
    id 120
    label "instytut"
  ]
  node [
    id 121
    label "wyko&#324;czenie"
  ]
  node [
    id 122
    label "jednostka_organizacyjna"
  ]
  node [
    id 123
    label "umowa"
  ]
  node [
    id 124
    label "instytucja"
  ]
  node [
    id 125
    label "czu&#263;"
  ]
  node [
    id 126
    label "need"
  ]
  node [
    id 127
    label "hide"
  ]
  node [
    id 128
    label "support"
  ]
  node [
    id 129
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 130
    label "kwota"
  ]
  node [
    id 131
    label "ilo&#347;&#263;"
  ]
  node [
    id 132
    label "96"
  ]
  node [
    id 133
    label "95"
  ]
  node [
    id 134
    label "Valmet"
  ]
  node [
    id 135
    label "Automotive"
  ]
  node [
    id 136
    label "93"
  ]
  node [
    id 137
    label "93c"
  ]
  node [
    id 138
    label "93f"
  ]
  node [
    id 139
    label "V4"
  ]
  node [
    id 140
    label "ford"
  ]
  node [
    id 141
    label "Taunusa"
  ]
  node [
    id 142
    label "zjednoczy&#263;"
  ]
  node [
    id 143
    label "99"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 8
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 10
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 13
  ]
  edge [
    source 7
    target 15
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 21
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 10
    target 46
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 107
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 12
    target 109
  ]
  edge [
    source 12
    target 110
  ]
  edge [
    source 12
    target 111
  ]
  edge [
    source 12
    target 112
  ]
  edge [
    source 12
    target 113
  ]
  edge [
    source 12
    target 114
  ]
  edge [
    source 12
    target 115
  ]
  edge [
    source 12
    target 116
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 117
  ]
  edge [
    source 15
    target 118
  ]
  edge [
    source 15
    target 119
  ]
  edge [
    source 15
    target 120
  ]
  edge [
    source 15
    target 121
  ]
  edge [
    source 15
    target 122
  ]
  edge [
    source 15
    target 123
  ]
  edge [
    source 15
    target 124
  ]
  edge [
    source 15
    target 67
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 82
  ]
  edge [
    source 19
    target 125
  ]
  edge [
    source 19
    target 126
  ]
  edge [
    source 19
    target 127
  ]
  edge [
    source 19
    target 128
  ]
  edge [
    source 20
    target 80
  ]
  edge [
    source 20
    target 129
  ]
  edge [
    source 20
    target 130
  ]
  edge [
    source 20
    target 131
  ]
  edge [
    source 81
    target 142
  ]
  edge [
    source 132
    target 139
  ]
  edge [
    source 134
    target 135
  ]
  edge [
    source 140
    target 141
  ]
]
