graph [
  maxDegree 153
  minDegree 1
  meanDegree 2.0134228187919465
  density 0.006779201410073894
  graphCliqueNumber 3
  node [
    id 0
    label "og&#243;&#322;"
    origin "text"
  ]
  node [
    id 1
    label "zawsze"
    origin "text"
  ]
  node [
    id 2
    label "kurwa"
    origin "text"
  ]
  node [
    id 3
    label "stan&#261;&#263;"
    origin "text"
  ]
  node [
    id 4
    label "siebie"
    origin "text"
  ]
  node [
    id 5
    label "przed"
    origin "text"
  ]
  node [
    id 6
    label "galeria"
    origin "text"
  ]
  node [
    id 7
    label "krakowski"
    origin "text"
  ]
  node [
    id 8
    label "odpali&#263;"
    origin "text"
  ]
  node [
    id 9
    label "papierosek"
    origin "text"
  ]
  node [
    id 10
    label "sekunda"
    origin "text"
  ]
  node [
    id 11
    label "spod"
    origin "text"
  ]
  node [
    id 12
    label "ziemia"
    origin "text"
  ]
  node [
    id 13
    label "wychodzi&#263;"
    origin "text"
  ]
  node [
    id 14
    label "&#380;ul"
    origin "text"
  ]
  node [
    id 15
    label "chcie&#263;"
    origin "text"
  ]
  node [
    id 16
    label "szlug"
    origin "text"
  ]
  node [
    id 17
    label "z&#322;ot&#243;wka"
    origin "text"
  ]
  node [
    id 18
    label "w&#243;da"
    origin "text"
  ]
  node [
    id 19
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 20
    label "pole"
  ]
  node [
    id 21
    label "kastowo&#347;&#263;"
  ]
  node [
    id 22
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 23
    label "ludzie_pracy"
  ]
  node [
    id 24
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 25
    label "community"
  ]
  node [
    id 26
    label "Fremeni"
  ]
  node [
    id 27
    label "status"
  ]
  node [
    id 28
    label "pozaklasowy"
  ]
  node [
    id 29
    label "aspo&#322;eczny"
  ]
  node [
    id 30
    label "ilo&#347;&#263;"
  ]
  node [
    id 31
    label "pe&#322;ny"
  ]
  node [
    id 32
    label "uwarstwienie"
  ]
  node [
    id 33
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 34
    label "zlewanie_si&#281;"
  ]
  node [
    id 35
    label "elita"
  ]
  node [
    id 36
    label "cywilizacja"
  ]
  node [
    id 37
    label "wspo&#322;ecze&#324;stwo"
  ]
  node [
    id 38
    label "klasa"
  ]
  node [
    id 39
    label "zaw&#380;dy"
  ]
  node [
    id 40
    label "ci&#261;gle"
  ]
  node [
    id 41
    label "na_zawsze"
  ]
  node [
    id 42
    label "cz&#281;sto"
  ]
  node [
    id 43
    label "szmaciarz"
  ]
  node [
    id 44
    label "zo&#322;za"
  ]
  node [
    id 45
    label "skurwienie_si&#281;"
  ]
  node [
    id 46
    label "kurwienie_si&#281;"
  ]
  node [
    id 47
    label "karze&#322;"
  ]
  node [
    id 48
    label "kurcz&#281;"
  ]
  node [
    id 49
    label "wyzwisko"
  ]
  node [
    id 50
    label "przekle&#324;stwo"
  ]
  node [
    id 51
    label "prostytutka"
  ]
  node [
    id 52
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 53
    label "obj&#261;&#263;"
  ]
  node [
    id 54
    label "reserve"
  ]
  node [
    id 55
    label "poruszy&#263;_si&#281;"
  ]
  node [
    id 56
    label "zosta&#263;"
  ]
  node [
    id 57
    label "originate"
  ]
  node [
    id 58
    label "zatrzyma&#263;_si&#281;"
  ]
  node [
    id 59
    label "przyj&#261;&#263;"
  ]
  node [
    id 60
    label "wystarczy&#263;"
  ]
  node [
    id 61
    label "przesta&#263;"
  ]
  node [
    id 62
    label "unie&#347;&#263;_si&#281;"
  ]
  node [
    id 63
    label "zmieni&#263;"
  ]
  node [
    id 64
    label "przyby&#263;"
  ]
  node [
    id 65
    label "sklep"
  ]
  node [
    id 66
    label "eskalator"
  ]
  node [
    id 67
    label "wystawa"
  ]
  node [
    id 68
    label "balkon"
  ]
  node [
    id 69
    label "centrum_handlowe"
  ]
  node [
    id 70
    label "ci&#261;g_komunikacyjny"
  ]
  node [
    id 71
    label "Galeria_Arsena&#322;"
  ]
  node [
    id 72
    label "publiczno&#347;&#263;"
  ]
  node [
    id 73
    label "zbi&#243;r"
  ]
  node [
    id 74
    label "sala"
  ]
  node [
    id 75
    label "&#322;&#261;cznik"
  ]
  node [
    id 76
    label "muzeum"
  ]
  node [
    id 77
    label "g&#322;&#243;g"
  ]
  node [
    id 78
    label "ma&#322;opolski"
  ]
  node [
    id 79
    label "po_krakowsku"
  ]
  node [
    id 80
    label "da&#263;"
  ]
  node [
    id 81
    label "za&#347;wieci&#263;"
  ]
  node [
    id 82
    label "odpowiedzie&#263;"
  ]
  node [
    id 83
    label "fuel"
  ]
  node [
    id 84
    label "strzeli&#263;"
  ]
  node [
    id 85
    label "arouse"
  ]
  node [
    id 86
    label "reject"
  ]
  node [
    id 87
    label "zapali&#263;"
  ]
  node [
    id 88
    label "fire"
  ]
  node [
    id 89
    label "resist"
  ]
  node [
    id 90
    label "fajka"
  ]
  node [
    id 91
    label "papieros"
  ]
  node [
    id 92
    label "zadzia&#322;a&#263;"
  ]
  node [
    id 93
    label "paln&#261;&#263;"
  ]
  node [
    id 94
    label "minuta"
  ]
  node [
    id 95
    label "tercja"
  ]
  node [
    id 96
    label "milisekunda"
  ]
  node [
    id 97
    label "nanosekunda"
  ]
  node [
    id 98
    label "uk&#322;ad_SI"
  ]
  node [
    id 99
    label "mikrosekunda"
  ]
  node [
    id 100
    label "time"
  ]
  node [
    id 101
    label "jednostka_czasu"
  ]
  node [
    id 102
    label "jednostka"
  ]
  node [
    id 103
    label "Skandynawia"
  ]
  node [
    id 104
    label "Yorkshire"
  ]
  node [
    id 105
    label "Kaukaz"
  ]
  node [
    id 106
    label "Kaszmir"
  ]
  node [
    id 107
    label "Podbeskidzie"
  ]
  node [
    id 108
    label "Toskania"
  ]
  node [
    id 109
    label "&#321;emkowszczyzna"
  ]
  node [
    id 110
    label "obszar"
  ]
  node [
    id 111
    label "Amhara"
  ]
  node [
    id 112
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 113
    label "Lombardia"
  ]
  node [
    id 114
    label "Kalabria"
  ]
  node [
    id 115
    label "kort"
  ]
  node [
    id 116
    label "Tyrol"
  ]
  node [
    id 117
    label "Pamir"
  ]
  node [
    id 118
    label "Lubelszczyzna"
  ]
  node [
    id 119
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 120
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 121
    label "&#379;ywiecczyzna"
  ]
  node [
    id 122
    label "ryzosfera"
  ]
  node [
    id 123
    label "Europa_Wschodnia"
  ]
  node [
    id 124
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 125
    label "Zabajkale"
  ]
  node [
    id 126
    label "Kaszuby"
  ]
  node [
    id 127
    label "Noworosja"
  ]
  node [
    id 128
    label "Bo&#347;nia"
  ]
  node [
    id 129
    label "Ba&#322;kany"
  ]
  node [
    id 130
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 131
    label "Anglia"
  ]
  node [
    id 132
    label "Kielecczyzna"
  ]
  node [
    id 133
    label "Pomorze_Zachodnie"
  ]
  node [
    id 134
    label "Opolskie"
  ]
  node [
    id 135
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 136
    label "skorupa_ziemska"
  ]
  node [
    id 137
    label "Ko&#322;yma"
  ]
  node [
    id 138
    label "Oksytania"
  ]
  node [
    id 139
    label "Syjon"
  ]
  node [
    id 140
    label "posadzka"
  ]
  node [
    id 141
    label "pa&#324;stwo"
  ]
  node [
    id 142
    label "Kociewie"
  ]
  node [
    id 143
    label "Huculszczyzna"
  ]
  node [
    id 144
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 145
    label "budynek"
  ]
  node [
    id 146
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 147
    label "Bawaria"
  ]
  node [
    id 148
    label "pomieszczenie"
  ]
  node [
    id 149
    label "pr&#243;chnica"
  ]
  node [
    id 150
    label "glinowanie"
  ]
  node [
    id 151
    label "Maghreb"
  ]
  node [
    id 152
    label "Bory_Tucholskie"
  ]
  node [
    id 153
    label "Europa_Zachodnia"
  ]
  node [
    id 154
    label "Kerala"
  ]
  node [
    id 155
    label "Podhale"
  ]
  node [
    id 156
    label "Kabylia"
  ]
  node [
    id 157
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 158
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 159
    label "Ma&#322;opolska"
  ]
  node [
    id 160
    label "Polesie"
  ]
  node [
    id 161
    label "Liguria"
  ]
  node [
    id 162
    label "&#321;&#243;dzkie"
  ]
  node [
    id 163
    label "geosystem"
  ]
  node [
    id 164
    label "Palestyna"
  ]
  node [
    id 165
    label "Bojkowszczyzna"
  ]
  node [
    id 166
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 167
    label "Karaiby"
  ]
  node [
    id 168
    label "S&#261;decczyzna"
  ]
  node [
    id 169
    label "Sand&#380;ak"
  ]
  node [
    id 170
    label "Nadrenia"
  ]
  node [
    id 171
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 172
    label "Zakarpacie"
  ]
  node [
    id 173
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 174
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 175
    label "Zag&#243;rze"
  ]
  node [
    id 176
    label "Andaluzja"
  ]
  node [
    id 177
    label "Turkiestan"
  ]
  node [
    id 178
    label "Naddniestrze"
  ]
  node [
    id 179
    label "Hercegowina"
  ]
  node [
    id 180
    label "p&#322;aszczyzna"
  ]
  node [
    id 181
    label "Opolszczyzna"
  ]
  node [
    id 182
    label "jednostka_administracyjna"
  ]
  node [
    id 183
    label "Lotaryngia"
  ]
  node [
    id 184
    label "Afryka_Wschodnia"
  ]
  node [
    id 185
    label "Szlezwik"
  ]
  node [
    id 186
    label "powierzchnia"
  ]
  node [
    id 187
    label "glinowa&#263;"
  ]
  node [
    id 188
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 189
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 190
    label "podglebie"
  ]
  node [
    id 191
    label "Mazowsze"
  ]
  node [
    id 192
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 193
    label "teren"
  ]
  node [
    id 194
    label "Afryka_Zachodnia"
  ]
  node [
    id 195
    label "czynnik_produkcji"
  ]
  node [
    id 196
    label "Galicja"
  ]
  node [
    id 197
    label "Szkocja"
  ]
  node [
    id 198
    label "Walia"
  ]
  node [
    id 199
    label "Powi&#347;le"
  ]
  node [
    id 200
    label "penetrator"
  ]
  node [
    id 201
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 202
    label "kompleks_sorpcyjny"
  ]
  node [
    id 203
    label "Zamojszczyzna"
  ]
  node [
    id 204
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 205
    label "Kujawy"
  ]
  node [
    id 206
    label "Podlasie"
  ]
  node [
    id 207
    label "Laponia"
  ]
  node [
    id 208
    label "Umbria"
  ]
  node [
    id 209
    label "plantowa&#263;"
  ]
  node [
    id 210
    label "Mezoameryka"
  ]
  node [
    id 211
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 212
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 213
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 214
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 215
    label "Kurdystan"
  ]
  node [
    id 216
    label "Kampania"
  ]
  node [
    id 217
    label "Armagnac"
  ]
  node [
    id 218
    label "Polinezja"
  ]
  node [
    id 219
    label "Warmia"
  ]
  node [
    id 220
    label "Wielkopolska"
  ]
  node [
    id 221
    label "litosfera"
  ]
  node [
    id 222
    label "Bordeaux"
  ]
  node [
    id 223
    label "Lauda"
  ]
  node [
    id 224
    label "Mazury"
  ]
  node [
    id 225
    label "Podkarpacie"
  ]
  node [
    id 226
    label "Oceania"
  ]
  node [
    id 227
    label "Lasko"
  ]
  node [
    id 228
    label "Amazonia"
  ]
  node [
    id 229
    label "pojazd"
  ]
  node [
    id 230
    label "glej"
  ]
  node [
    id 231
    label "martwica"
  ]
  node [
    id 232
    label "zapadnia"
  ]
  node [
    id 233
    label "przestrze&#324;"
  ]
  node [
    id 234
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 235
    label "dotleni&#263;"
  ]
  node [
    id 236
    label "Tonkin"
  ]
  node [
    id 237
    label "Kurpie"
  ]
  node [
    id 238
    label "Azja_Wschodnia"
  ]
  node [
    id 239
    label "Mikronezja"
  ]
  node [
    id 240
    label "Ukraina_Zachodnia"
  ]
  node [
    id 241
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 242
    label "Turyngia"
  ]
  node [
    id 243
    label "Baszkiria"
  ]
  node [
    id 244
    label "Apulia"
  ]
  node [
    id 245
    label "miejsce"
  ]
  node [
    id 246
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 247
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 248
    label "Indochiny"
  ]
  node [
    id 249
    label "Lubuskie"
  ]
  node [
    id 250
    label "Biskupizna"
  ]
  node [
    id 251
    label "domain"
  ]
  node [
    id 252
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 253
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 254
    label "uzyskiwa&#263;"
  ]
  node [
    id 255
    label "impart"
  ]
  node [
    id 256
    label "proceed"
  ]
  node [
    id 257
    label "blend"
  ]
  node [
    id 258
    label "give"
  ]
  node [
    id 259
    label "ograniczenie"
  ]
  node [
    id 260
    label "wydostawa&#263;_si&#281;"
  ]
  node [
    id 261
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 262
    label "za&#322;atwi&#263;"
  ]
  node [
    id 263
    label "schodzi&#263;"
  ]
  node [
    id 264
    label "gra&#263;"
  ]
  node [
    id 265
    label "osi&#261;ga&#263;"
  ]
  node [
    id 266
    label "okazywa&#263;_si&#281;"
  ]
  node [
    id 267
    label "seclude"
  ]
  node [
    id 268
    label "strona_&#347;wiata"
  ]
  node [
    id 269
    label "przedk&#322;ada&#263;"
  ]
  node [
    id 270
    label "przedstawia&#263;"
  ]
  node [
    id 271
    label "appear"
  ]
  node [
    id 272
    label "publish"
  ]
  node [
    id 273
    label "ko&#324;czy&#263;"
  ]
  node [
    id 274
    label "wypada&#263;"
  ]
  node [
    id 275
    label "pochodzi&#263;"
  ]
  node [
    id 276
    label "ukazywa&#263;_si&#281;_drukiem"
  ]
  node [
    id 277
    label "uwalnia&#263;_si&#281;"
  ]
  node [
    id 278
    label "wygl&#261;da&#263;"
  ]
  node [
    id 279
    label "opuszcza&#263;"
  ]
  node [
    id 280
    label "wystarcza&#263;"
  ]
  node [
    id 281
    label "wyrusza&#263;"
  ]
  node [
    id 282
    label "perform"
  ]
  node [
    id 283
    label "heighten"
  ]
  node [
    id 284
    label "lump"
  ]
  node [
    id 285
    label "obszczymur"
  ]
  node [
    id 286
    label "&#380;ulernia"
  ]
  node [
    id 287
    label "alkoholik"
  ]
  node [
    id 288
    label "czu&#263;"
  ]
  node [
    id 289
    label "desire"
  ]
  node [
    id 290
    label "kcie&#263;"
  ]
  node [
    id 291
    label "zajaranie"
  ]
  node [
    id 292
    label "grosz"
  ]
  node [
    id 293
    label "pieni&#261;dz"
  ]
  node [
    id 294
    label "jednostka_monetarna"
  ]
  node [
    id 295
    label "Polska"
  ]
  node [
    id 296
    label "xDDDD"
  ]
  node [
    id 297
    label "pierwszy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 8
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 12
    target 107
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 12
    target 109
  ]
  edge [
    source 12
    target 110
  ]
  edge [
    source 12
    target 111
  ]
  edge [
    source 12
    target 112
  ]
  edge [
    source 12
    target 113
  ]
  edge [
    source 12
    target 114
  ]
  edge [
    source 12
    target 115
  ]
  edge [
    source 12
    target 116
  ]
  edge [
    source 12
    target 117
  ]
  edge [
    source 12
    target 118
  ]
  edge [
    source 12
    target 119
  ]
  edge [
    source 12
    target 120
  ]
  edge [
    source 12
    target 121
  ]
  edge [
    source 12
    target 122
  ]
  edge [
    source 12
    target 123
  ]
  edge [
    source 12
    target 124
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 12
    target 131
  ]
  edge [
    source 12
    target 132
  ]
  edge [
    source 12
    target 133
  ]
  edge [
    source 12
    target 134
  ]
  edge [
    source 12
    target 135
  ]
  edge [
    source 12
    target 136
  ]
  edge [
    source 12
    target 137
  ]
  edge [
    source 12
    target 138
  ]
  edge [
    source 12
    target 139
  ]
  edge [
    source 12
    target 140
  ]
  edge [
    source 12
    target 141
  ]
  edge [
    source 12
    target 142
  ]
  edge [
    source 12
    target 143
  ]
  edge [
    source 12
    target 144
  ]
  edge [
    source 12
    target 145
  ]
  edge [
    source 12
    target 146
  ]
  edge [
    source 12
    target 147
  ]
  edge [
    source 12
    target 148
  ]
  edge [
    source 12
    target 149
  ]
  edge [
    source 12
    target 150
  ]
  edge [
    source 12
    target 151
  ]
  edge [
    source 12
    target 152
  ]
  edge [
    source 12
    target 153
  ]
  edge [
    source 12
    target 154
  ]
  edge [
    source 12
    target 155
  ]
  edge [
    source 12
    target 156
  ]
  edge [
    source 12
    target 157
  ]
  edge [
    source 12
    target 158
  ]
  edge [
    source 12
    target 159
  ]
  edge [
    source 12
    target 160
  ]
  edge [
    source 12
    target 161
  ]
  edge [
    source 12
    target 162
  ]
  edge [
    source 12
    target 163
  ]
  edge [
    source 12
    target 164
  ]
  edge [
    source 12
    target 165
  ]
  edge [
    source 12
    target 166
  ]
  edge [
    source 12
    target 167
  ]
  edge [
    source 12
    target 168
  ]
  edge [
    source 12
    target 169
  ]
  edge [
    source 12
    target 170
  ]
  edge [
    source 12
    target 171
  ]
  edge [
    source 12
    target 172
  ]
  edge [
    source 12
    target 173
  ]
  edge [
    source 12
    target 174
  ]
  edge [
    source 12
    target 175
  ]
  edge [
    source 12
    target 176
  ]
  edge [
    source 12
    target 177
  ]
  edge [
    source 12
    target 178
  ]
  edge [
    source 12
    target 179
  ]
  edge [
    source 12
    target 180
  ]
  edge [
    source 12
    target 181
  ]
  edge [
    source 12
    target 182
  ]
  edge [
    source 12
    target 183
  ]
  edge [
    source 12
    target 184
  ]
  edge [
    source 12
    target 185
  ]
  edge [
    source 12
    target 186
  ]
  edge [
    source 12
    target 187
  ]
  edge [
    source 12
    target 188
  ]
  edge [
    source 12
    target 189
  ]
  edge [
    source 12
    target 190
  ]
  edge [
    source 12
    target 191
  ]
  edge [
    source 12
    target 192
  ]
  edge [
    source 12
    target 193
  ]
  edge [
    source 12
    target 194
  ]
  edge [
    source 12
    target 195
  ]
  edge [
    source 12
    target 196
  ]
  edge [
    source 12
    target 197
  ]
  edge [
    source 12
    target 198
  ]
  edge [
    source 12
    target 199
  ]
  edge [
    source 12
    target 200
  ]
  edge [
    source 12
    target 201
  ]
  edge [
    source 12
    target 202
  ]
  edge [
    source 12
    target 203
  ]
  edge [
    source 12
    target 204
  ]
  edge [
    source 12
    target 205
  ]
  edge [
    source 12
    target 206
  ]
  edge [
    source 12
    target 207
  ]
  edge [
    source 12
    target 208
  ]
  edge [
    source 12
    target 209
  ]
  edge [
    source 12
    target 210
  ]
  edge [
    source 12
    target 211
  ]
  edge [
    source 12
    target 212
  ]
  edge [
    source 12
    target 213
  ]
  edge [
    source 12
    target 214
  ]
  edge [
    source 12
    target 215
  ]
  edge [
    source 12
    target 216
  ]
  edge [
    source 12
    target 217
  ]
  edge [
    source 12
    target 218
  ]
  edge [
    source 12
    target 219
  ]
  edge [
    source 12
    target 220
  ]
  edge [
    source 12
    target 221
  ]
  edge [
    source 12
    target 222
  ]
  edge [
    source 12
    target 223
  ]
  edge [
    source 12
    target 224
  ]
  edge [
    source 12
    target 225
  ]
  edge [
    source 12
    target 226
  ]
  edge [
    source 12
    target 227
  ]
  edge [
    source 12
    target 228
  ]
  edge [
    source 12
    target 229
  ]
  edge [
    source 12
    target 230
  ]
  edge [
    source 12
    target 231
  ]
  edge [
    source 12
    target 232
  ]
  edge [
    source 12
    target 233
  ]
  edge [
    source 12
    target 234
  ]
  edge [
    source 12
    target 235
  ]
  edge [
    source 12
    target 236
  ]
  edge [
    source 12
    target 237
  ]
  edge [
    source 12
    target 238
  ]
  edge [
    source 12
    target 239
  ]
  edge [
    source 12
    target 240
  ]
  edge [
    source 12
    target 241
  ]
  edge [
    source 12
    target 242
  ]
  edge [
    source 12
    target 243
  ]
  edge [
    source 12
    target 244
  ]
  edge [
    source 12
    target 245
  ]
  edge [
    source 12
    target 246
  ]
  edge [
    source 12
    target 247
  ]
  edge [
    source 12
    target 248
  ]
  edge [
    source 12
    target 249
  ]
  edge [
    source 12
    target 250
  ]
  edge [
    source 12
    target 251
  ]
  edge [
    source 12
    target 252
  ]
  edge [
    source 12
    target 253
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 254
  ]
  edge [
    source 13
    target 255
  ]
  edge [
    source 13
    target 256
  ]
  edge [
    source 13
    target 257
  ]
  edge [
    source 13
    target 258
  ]
  edge [
    source 13
    target 259
  ]
  edge [
    source 13
    target 260
  ]
  edge [
    source 13
    target 261
  ]
  edge [
    source 13
    target 262
  ]
  edge [
    source 13
    target 263
  ]
  edge [
    source 13
    target 264
  ]
  edge [
    source 13
    target 265
  ]
  edge [
    source 13
    target 266
  ]
  edge [
    source 13
    target 267
  ]
  edge [
    source 13
    target 268
  ]
  edge [
    source 13
    target 269
  ]
  edge [
    source 13
    target 270
  ]
  edge [
    source 13
    target 271
  ]
  edge [
    source 13
    target 272
  ]
  edge [
    source 13
    target 273
  ]
  edge [
    source 13
    target 274
  ]
  edge [
    source 13
    target 275
  ]
  edge [
    source 13
    target 276
  ]
  edge [
    source 13
    target 277
  ]
  edge [
    source 13
    target 278
  ]
  edge [
    source 13
    target 279
  ]
  edge [
    source 13
    target 280
  ]
  edge [
    source 13
    target 281
  ]
  edge [
    source 13
    target 282
  ]
  edge [
    source 13
    target 283
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 284
  ]
  edge [
    source 14
    target 285
  ]
  edge [
    source 14
    target 286
  ]
  edge [
    source 14
    target 287
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 288
  ]
  edge [
    source 15
    target 289
  ]
  edge [
    source 15
    target 290
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 291
  ]
  edge [
    source 16
    target 91
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 292
  ]
  edge [
    source 17
    target 293
  ]
  edge [
    source 17
    target 294
  ]
  edge [
    source 17
    target 295
  ]
  edge [
    source 296
    target 297
  ]
]
