graph [
  maxDegree 12
  minDegree 1
  meanDegree 1.75
  density 0.11666666666666667
  graphCliqueNumber 2
  node [
    id 0
    label "star"
    origin "text"
  ]
  node [
    id 1
    label "wiesia"
    origin "text"
  ]
  node [
    id 2
    label "gmina"
    origin "text"
  ]
  node [
    id 3
    label "kro&#347;niewice"
    origin "text"
  ]
  node [
    id 4
    label "rada_gminy"
  ]
  node [
    id 5
    label "Wielka_Wie&#347;"
  ]
  node [
    id 6
    label "jednostka_administracyjna"
  ]
  node [
    id 7
    label "powiat"
  ]
  node [
    id 8
    label "Dobro&#324;"
  ]
  node [
    id 9
    label "Karlsbad"
  ]
  node [
    id 10
    label "urz&#261;d"
  ]
  node [
    id 11
    label "Biskupice"
  ]
  node [
    id 12
    label "radny"
  ]
  node [
    id 13
    label "organizacja_religijna"
  ]
  node [
    id 14
    label "stary"
  ]
  node [
    id 15
    label "wie&#347;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 14
    target 15
  ]
]
