graph [
  maxDegree 11
  minDegree 1
  meanDegree 1.8461538461538463
  density 0.15384615384615385
  graphCliqueNumber 2
  node [
    id 0
    label "wiek"
    origin "text"
  ]
  node [
    id 1
    label "lato"
    origin "text"
  ]
  node [
    id 2
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 3
    label "czas"
  ]
  node [
    id 4
    label "period"
  ]
  node [
    id 5
    label "rok"
  ]
  node [
    id 6
    label "cecha"
  ]
  node [
    id 7
    label "long_time"
  ]
  node [
    id 8
    label "choroba_wieku"
  ]
  node [
    id 9
    label "jednostka_geologiczna"
  ]
  node [
    id 10
    label "chron"
  ]
  node [
    id 11
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 12
    label "pora_roku"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 12
  ]
]
