graph [
  maxDegree 11
  minDegree 1
  meanDegree 1.9310344827586208
  density 0.06896551724137931
  graphCliqueNumber 2
  node [
    id 0
    label "nyc"
    origin "text"
  ]
  node [
    id 1
    label "mieszek"
    origin "text"
  ]
  node [
    id 2
    label "tys"
    origin "text"
  ]
  node [
    id 3
    label "ortodoksyjny"
    origin "text"
  ]
  node [
    id 4
    label "&#380;yd"
    origin "text"
  ]
  node [
    id 5
    label "woreczek"
  ]
  node [
    id 6
    label "przedmiot"
  ]
  node [
    id 7
    label "czujnik"
  ]
  node [
    id 8
    label "owoc"
  ]
  node [
    id 9
    label "kapsa"
  ]
  node [
    id 10
    label "element"
  ]
  node [
    id 11
    label "aparat_fotograficzny"
  ]
  node [
    id 12
    label "urz&#261;dzenie"
  ]
  node [
    id 13
    label "piter"
  ]
  node [
    id 14
    label "ortodoksyjnie"
  ]
  node [
    id 15
    label "charakterystyczny"
  ]
  node [
    id 16
    label "konserwatywny"
  ]
  node [
    id 17
    label "wyznaniowy"
  ]
  node [
    id 18
    label "chrze&#347;cija&#324;ski"
  ]
  node [
    id 19
    label "chciwiec"
  ]
  node [
    id 20
    label "judenrat"
  ]
  node [
    id 21
    label "materialista"
  ]
  node [
    id 22
    label "sk&#261;piarz"
  ]
  node [
    id 23
    label "wyznawca"
  ]
  node [
    id 24
    label "&#379;ydziak"
  ]
  node [
    id 25
    label "sk&#261;py"
  ]
  node [
    id 26
    label "szmonces"
  ]
  node [
    id 27
    label "monoteista"
  ]
  node [
    id 28
    label "mosiek"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
]
