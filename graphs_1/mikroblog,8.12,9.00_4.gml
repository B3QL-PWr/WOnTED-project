graph [
  maxDegree 13
  minDegree 1
  meanDegree 1.95
  density 0.05
  graphCliqueNumber 2
  node [
    id 0
    label "kiedy"
    origin "text"
  ]
  node [
    id 1
    label "kto&#347;"
    origin "text"
  ]
  node [
    id 2
    label "pierdoli&#263;"
    origin "text"
  ]
  node [
    id 3
    label "russel"
    origin "text"
  ]
  node [
    id 4
    label "by&#263;"
    origin "text"
  ]
  node [
    id 5
    label "szybki"
    origin "text"
  ]
  node [
    id 6
    label "robert"
    origin "text"
  ]
  node [
    id 7
    label "kubica"
    origin "text"
  ]
  node [
    id 8
    label "heheszki"
    origin "text"
  ]
  node [
    id 9
    label "humorobrazkowy"
    origin "text"
  ]
  node [
    id 10
    label "cz&#322;owiek"
  ]
  node [
    id 11
    label "znaczenie"
  ]
  node [
    id 12
    label "go&#347;&#263;"
  ]
  node [
    id 13
    label "osoba"
  ]
  node [
    id 14
    label "posta&#263;"
  ]
  node [
    id 15
    label "talk_through_one's_hat"
  ]
  node [
    id 16
    label "mie&#263;_gdzie&#347;"
  ]
  node [
    id 17
    label "bra&#263;"
  ]
  node [
    id 18
    label "ple&#347;&#263;"
  ]
  node [
    id 19
    label "si&#281;ga&#263;"
  ]
  node [
    id 20
    label "trwa&#263;"
  ]
  node [
    id 21
    label "obecno&#347;&#263;"
  ]
  node [
    id 22
    label "stan"
  ]
  node [
    id 23
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 24
    label "stand"
  ]
  node [
    id 25
    label "mie&#263;_miejsce"
  ]
  node [
    id 26
    label "uczestniczy&#263;"
  ]
  node [
    id 27
    label "chodzi&#263;"
  ]
  node [
    id 28
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 29
    label "equal"
  ]
  node [
    id 30
    label "bezpo&#347;redni"
  ]
  node [
    id 31
    label "kr&#243;tki"
  ]
  node [
    id 32
    label "temperamentny"
  ]
  node [
    id 33
    label "prosty"
  ]
  node [
    id 34
    label "intensywny"
  ]
  node [
    id 35
    label "dynamiczny"
  ]
  node [
    id 36
    label "szybko"
  ]
  node [
    id 37
    label "sprawny"
  ]
  node [
    id 38
    label "bystrolotny"
  ]
  node [
    id 39
    label "energiczny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
]
