graph [
  maxDegree 3
  minDegree 1
  meanDegree 1.6666666666666667
  density 0.3333333333333333
  graphCliqueNumber 2
  node [
    id 0
    label "g&#322;up"
    origin "text"
  ]
  node [
    id 1
    label "szmul"
    origin "text"
  ]
  node [
    id 2
    label "tinder"
    origin "text"
  ]
  node [
    id 3
    label "heheszki"
    origin "text"
  ]
  node [
    id 4
    label "g&#322;upek"
  ]
  node [
    id 5
    label "g&#322;owa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
]
