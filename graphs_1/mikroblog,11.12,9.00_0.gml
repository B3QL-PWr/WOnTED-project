graph [
  maxDegree 7
  minDegree 1
  meanDegree 2
  density 0.4
  graphCliqueNumber 2
  node [
    id 0
    label "mus"
    origin "text"
  ]
  node [
    id 1
    label "&#7576;&#7506;&#7461;&#7506;&#7557;"
    origin "text"
  ]
  node [
    id 2
    label "potrzeba"
  ]
  node [
    id 3
    label "potrawa"
  ]
  node [
    id 4
    label "tysi&#261;c"
  ]
  node [
    id 5
    label "przymus"
  ]
  edge [
    source 0
    target 0
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
]
