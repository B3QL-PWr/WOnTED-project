graph [
  maxDegree 42
  minDegree 1
  meanDegree 2.051282051282051
  density 0.013234077750206782
  graphCliqueNumber 3
  node [
    id 0
    label "charakter"
    origin "text"
  ]
  node [
    id 1
    label "s&#322;u&#380;ba"
    origin "text"
  ]
  node [
    id 2
    label "policjant"
    origin "text"
  ]
  node [
    id 3
    label "zadanie"
    origin "text"
  ]
  node [
    id 4
    label "wykonywa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "nara&#380;enie"
    origin "text"
  ]
  node [
    id 6
    label "&#380;ycie"
    origin "text"
  ]
  node [
    id 7
    label "wymaga&#263;"
    origin "text"
  ]
  node [
    id 8
    label "&#380;eby"
    origin "text"
  ]
  node [
    id 9
    label "wymoga"
    origin "text"
  ]
  node [
    id 10
    label "obywatelstwo"
    origin "text"
  ]
  node [
    id 11
    label "polski"
    origin "text"
  ]
  node [
    id 12
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 13
    label "zachowa&#263;"
    origin "text"
  ]
  node [
    id 14
    label "cz&#322;owiek"
  ]
  node [
    id 15
    label "zjawisko"
  ]
  node [
    id 16
    label "psychika"
  ]
  node [
    id 17
    label "przedmiot"
  ]
  node [
    id 18
    label "cecha"
  ]
  node [
    id 19
    label "entity"
  ]
  node [
    id 20
    label "zbi&#243;r"
  ]
  node [
    id 21
    label "wydarzenie"
  ]
  node [
    id 22
    label "kompleksja"
  ]
  node [
    id 23
    label "osobowo&#347;&#263;"
  ]
  node [
    id 24
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 25
    label "posta&#263;"
  ]
  node [
    id 26
    label "fizjonomia"
  ]
  node [
    id 27
    label "service"
  ]
  node [
    id 28
    label "ZOMO"
  ]
  node [
    id 29
    label "czworak"
  ]
  node [
    id 30
    label "zesp&#243;&#322;"
  ]
  node [
    id 31
    label "s&#322;u&#380;&#261;ca"
  ]
  node [
    id 32
    label "instytucja"
  ]
  node [
    id 33
    label "s&#322;u&#380;&#261;cy"
  ]
  node [
    id 34
    label "praca"
  ]
  node [
    id 35
    label "wys&#322;uga"
  ]
  node [
    id 36
    label "policja"
  ]
  node [
    id 37
    label "blacharz"
  ]
  node [
    id 38
    label "pa&#322;a"
  ]
  node [
    id 39
    label "mundurowy"
  ]
  node [
    id 40
    label "str&#243;&#380;"
  ]
  node [
    id 41
    label "glina"
  ]
  node [
    id 42
    label "yield"
  ]
  node [
    id 43
    label "czynno&#347;&#263;"
  ]
  node [
    id 44
    label "problem"
  ]
  node [
    id 45
    label "przepisanie"
  ]
  node [
    id 46
    label "przepisa&#263;"
  ]
  node [
    id 47
    label "za&#322;o&#380;enie"
  ]
  node [
    id 48
    label "work"
  ]
  node [
    id 49
    label "nakarmienie"
  ]
  node [
    id 50
    label "duty"
  ]
  node [
    id 51
    label "powierzanie"
  ]
  node [
    id 52
    label "zaszkodzenie"
  ]
  node [
    id 53
    label "d&#378;wigni&#281;cie"
  ]
  node [
    id 54
    label "zaj&#281;cie"
  ]
  node [
    id 55
    label "zobowi&#261;zanie"
  ]
  node [
    id 56
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 57
    label "robi&#263;"
  ]
  node [
    id 58
    label "muzyka"
  ]
  node [
    id 59
    label "rola"
  ]
  node [
    id 60
    label "create"
  ]
  node [
    id 61
    label "wytwarza&#263;"
  ]
  node [
    id 62
    label "wystawienie"
  ]
  node [
    id 63
    label "expose"
  ]
  node [
    id 64
    label "energy"
  ]
  node [
    id 65
    label "czas"
  ]
  node [
    id 66
    label "bycie"
  ]
  node [
    id 67
    label "zegar_biologiczny"
  ]
  node [
    id 68
    label "okres_noworodkowy"
  ]
  node [
    id 69
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 70
    label "prze&#380;ywanie"
  ]
  node [
    id 71
    label "prze&#380;ycie"
  ]
  node [
    id 72
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 73
    label "wiek_matuzalemowy"
  ]
  node [
    id 74
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 75
    label "dzieci&#324;stwo"
  ]
  node [
    id 76
    label "power"
  ]
  node [
    id 77
    label "szwung"
  ]
  node [
    id 78
    label "menopauza"
  ]
  node [
    id 79
    label "umarcie"
  ]
  node [
    id 80
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 81
    label "life"
  ]
  node [
    id 82
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 83
    label "&#380;ywy"
  ]
  node [
    id 84
    label "rozw&#243;j"
  ]
  node [
    id 85
    label "po&#322;&#243;g"
  ]
  node [
    id 86
    label "byt"
  ]
  node [
    id 87
    label "przebywanie"
  ]
  node [
    id 88
    label "subsistence"
  ]
  node [
    id 89
    label "koleje_losu"
  ]
  node [
    id 90
    label "raj_utracony"
  ]
  node [
    id 91
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 92
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 93
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 94
    label "andropauza"
  ]
  node [
    id 95
    label "warunki"
  ]
  node [
    id 96
    label "do&#380;ywanie"
  ]
  node [
    id 97
    label "niemowl&#281;ctwo"
  ]
  node [
    id 98
    label "umieranie"
  ]
  node [
    id 99
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 100
    label "staro&#347;&#263;"
  ]
  node [
    id 101
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 102
    label "&#347;mier&#263;"
  ]
  node [
    id 103
    label "zmusza&#263;"
  ]
  node [
    id 104
    label "by&#263;"
  ]
  node [
    id 105
    label "&#380;&#261;da&#263;"
  ]
  node [
    id 106
    label "claim"
  ]
  node [
    id 107
    label "force"
  ]
  node [
    id 108
    label "take"
  ]
  node [
    id 109
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 110
    label "indygenat"
  ]
  node [
    id 111
    label "prawa_cz&#322;owieka"
  ]
  node [
    id 112
    label "lacki"
  ]
  node [
    id 113
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 114
    label "sztajer"
  ]
  node [
    id 115
    label "drabant"
  ]
  node [
    id 116
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 117
    label "polak"
  ]
  node [
    id 118
    label "pierogi_ruskie"
  ]
  node [
    id 119
    label "krakowiak"
  ]
  node [
    id 120
    label "Polish"
  ]
  node [
    id 121
    label "j&#281;zyk"
  ]
  node [
    id 122
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 123
    label "oberek"
  ]
  node [
    id 124
    label "po_polsku"
  ]
  node [
    id 125
    label "mazur"
  ]
  node [
    id 126
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 127
    label "chodzony"
  ]
  node [
    id 128
    label "skoczny"
  ]
  node [
    id 129
    label "ryba_po_grecku"
  ]
  node [
    id 130
    label "goniony"
  ]
  node [
    id 131
    label "polsko"
  ]
  node [
    id 132
    label "proceed"
  ]
  node [
    id 133
    label "catch"
  ]
  node [
    id 134
    label "pozosta&#263;"
  ]
  node [
    id 135
    label "osta&#263;_si&#281;"
  ]
  node [
    id 136
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 137
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 138
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 139
    label "change"
  ]
  node [
    id 140
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 141
    label "tajemnica"
  ]
  node [
    id 142
    label "pami&#281;&#263;"
  ]
  node [
    id 143
    label "bury"
  ]
  node [
    id 144
    label "zdyscyplinowanie"
  ]
  node [
    id 145
    label "podtrzyma&#263;"
  ]
  node [
    id 146
    label "preserve"
  ]
  node [
    id 147
    label "post&#261;pi&#263;"
  ]
  node [
    id 148
    label "post"
  ]
  node [
    id 149
    label "zrobi&#263;"
  ]
  node [
    id 150
    label "podporz&#261;dkowa&#263;_si&#281;"
  ]
  node [
    id 151
    label "przechowa&#263;"
  ]
  node [
    id 152
    label "dieta"
  ]
  node [
    id 153
    label "komisja"
  ]
  node [
    id 154
    label "dospraw"
  ]
  node [
    id 155
    label "petycja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 19
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 10
    target 110
  ]
  edge [
    source 10
    target 111
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 112
  ]
  edge [
    source 11
    target 113
  ]
  edge [
    source 11
    target 17
  ]
  edge [
    source 11
    target 114
  ]
  edge [
    source 11
    target 115
  ]
  edge [
    source 11
    target 116
  ]
  edge [
    source 11
    target 117
  ]
  edge [
    source 11
    target 118
  ]
  edge [
    source 11
    target 119
  ]
  edge [
    source 11
    target 120
  ]
  edge [
    source 11
    target 121
  ]
  edge [
    source 11
    target 122
  ]
  edge [
    source 11
    target 123
  ]
  edge [
    source 11
    target 124
  ]
  edge [
    source 11
    target 125
  ]
  edge [
    source 11
    target 126
  ]
  edge [
    source 11
    target 127
  ]
  edge [
    source 11
    target 128
  ]
  edge [
    source 11
    target 129
  ]
  edge [
    source 11
    target 130
  ]
  edge [
    source 11
    target 131
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 132
  ]
  edge [
    source 12
    target 133
  ]
  edge [
    source 12
    target 134
  ]
  edge [
    source 12
    target 135
  ]
  edge [
    source 12
    target 136
  ]
  edge [
    source 12
    target 137
  ]
  edge [
    source 12
    target 138
  ]
  edge [
    source 12
    target 139
  ]
  edge [
    source 12
    target 140
  ]
  edge [
    source 13
    target 141
  ]
  edge [
    source 13
    target 142
  ]
  edge [
    source 13
    target 143
  ]
  edge [
    source 13
    target 144
  ]
  edge [
    source 13
    target 145
  ]
  edge [
    source 13
    target 146
  ]
  edge [
    source 13
    target 147
  ]
  edge [
    source 13
    target 148
  ]
  edge [
    source 13
    target 149
  ]
  edge [
    source 13
    target 150
  ]
  edge [
    source 13
    target 151
  ]
  edge [
    source 13
    target 152
  ]
  edge [
    source 153
    target 154
  ]
  edge [
    source 153
    target 155
  ]
  edge [
    source 154
    target 155
  ]
]
