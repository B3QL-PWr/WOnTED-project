graph [
  maxDegree 17
  minDegree 1
  meanDegree 2.0217391304347827
  density 0.022216913521261348
  graphCliqueNumber 2
  node [
    id 0
    label "rocznica"
    origin "text"
  ]
  node [
    id 1
    label "podpisa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "deklaracja"
    origin "text"
  ]
  node [
    id 3
    label "waszyngto&#324;ski"
    origin "text"
  ]
  node [
    id 4
    label "dotyczy&#263;"
    origin "text"
  ]
  node [
    id 5
    label "zwrot"
    origin "text"
  ]
  node [
    id 6
    label "zrabowa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "przez"
    origin "text"
  ]
  node [
    id 8
    label "nazistowski"
    origin "text"
  ]
  node [
    id 9
    label "niemcy"
    origin "text"
  ]
  node [
    id 10
    label "dzie&#322;o"
    origin "text"
  ]
  node [
    id 11
    label "sztuk"
    origin "text"
  ]
  node [
    id 12
    label "berlin"
    origin "text"
  ]
  node [
    id 13
    label "odby&#263;"
    origin "text"
  ]
  node [
    id 14
    label "si&#281;"
    origin "text"
  ]
  node [
    id 15
    label "konferencja"
    origin "text"
  ]
  node [
    id 16
    label "po&#347;wi&#281;cony"
    origin "text"
  ]
  node [
    id 17
    label "realizacja"
    origin "text"
  ]
  node [
    id 18
    label "wytyczna"
    origin "text"
  ]
  node [
    id 19
    label "termin"
  ]
  node [
    id 20
    label "obchody"
  ]
  node [
    id 21
    label "za&#347;wiadczy&#263;"
  ]
  node [
    id 22
    label "postawi&#263;"
  ]
  node [
    id 23
    label "sign"
  ]
  node [
    id 24
    label "opatrzy&#263;"
  ]
  node [
    id 25
    label "dokument"
  ]
  node [
    id 26
    label "obietnica"
  ]
  node [
    id 27
    label "konstrukcja"
  ]
  node [
    id 28
    label "announcement"
  ]
  node [
    id 29
    label "formularz"
  ]
  node [
    id 30
    label "akt"
  ]
  node [
    id 31
    label "o&#347;wiadczenie"
  ]
  node [
    id 32
    label "statement"
  ]
  node [
    id 33
    label "o&#347;wiadczyny"
  ]
  node [
    id 34
    label "digest"
  ]
  node [
    id 35
    label "bargain"
  ]
  node [
    id 36
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 37
    label "tycze&#263;"
  ]
  node [
    id 38
    label "turn"
  ]
  node [
    id 39
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 40
    label "skr&#281;t"
  ]
  node [
    id 41
    label "jednostka_leksykalna"
  ]
  node [
    id 42
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 43
    label "obr&#243;t"
  ]
  node [
    id 44
    label "fraza_czasownikowa"
  ]
  node [
    id 45
    label "wyra&#380;enie"
  ]
  node [
    id 46
    label "punkt"
  ]
  node [
    id 47
    label "zmiana"
  ]
  node [
    id 48
    label "turning"
  ]
  node [
    id 49
    label "ukra&#347;&#263;"
  ]
  node [
    id 50
    label "&#322;up"
  ]
  node [
    id 51
    label "overcharge"
  ]
  node [
    id 52
    label "anty&#380;ydowski"
  ]
  node [
    id 53
    label "faszystowski"
  ]
  node [
    id 54
    label "hitlerowsko"
  ]
  node [
    id 55
    label "rasistowski"
  ]
  node [
    id 56
    label "antykomunistyczny"
  ]
  node [
    id 57
    label "po_nazistowsku"
  ]
  node [
    id 58
    label "creation"
  ]
  node [
    id 59
    label "forma"
  ]
  node [
    id 60
    label "retrospektywa"
  ]
  node [
    id 61
    label "tre&#347;&#263;"
  ]
  node [
    id 62
    label "tetralogia"
  ]
  node [
    id 63
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 64
    label "dorobek"
  ]
  node [
    id 65
    label "obrazowanie"
  ]
  node [
    id 66
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 67
    label "komunikat"
  ]
  node [
    id 68
    label "praca"
  ]
  node [
    id 69
    label "works"
  ]
  node [
    id 70
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 71
    label "tekst"
  ]
  node [
    id 72
    label "reserve"
  ]
  node [
    id 73
    label "przej&#347;&#263;"
  ]
  node [
    id 74
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 75
    label "konferencyjka"
  ]
  node [
    id 76
    label "Poczdam"
  ]
  node [
    id 77
    label "conference"
  ]
  node [
    id 78
    label "spotkanie"
  ]
  node [
    id 79
    label "grusza_pospolita"
  ]
  node [
    id 80
    label "Ja&#322;ta"
  ]
  node [
    id 81
    label "oddany"
  ]
  node [
    id 82
    label "monta&#380;"
  ]
  node [
    id 83
    label "fabrication"
  ]
  node [
    id 84
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 85
    label "kreacja"
  ]
  node [
    id 86
    label "performance"
  ]
  node [
    id 87
    label "proces"
  ]
  node [
    id 88
    label "postprodukcja"
  ]
  node [
    id 89
    label "scheduling"
  ]
  node [
    id 90
    label "operacja"
  ]
  node [
    id 91
    label "za&#322;o&#380;enie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 58
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 10
    target 60
  ]
  edge [
    source 10
    target 61
  ]
  edge [
    source 10
    target 62
  ]
  edge [
    source 10
    target 63
  ]
  edge [
    source 10
    target 64
  ]
  edge [
    source 10
    target 65
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 10
    target 69
  ]
  edge [
    source 10
    target 70
  ]
  edge [
    source 10
    target 71
  ]
  edge [
    source 10
    target 17
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 72
  ]
  edge [
    source 13
    target 73
  ]
  edge [
    source 13
    target 74
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 75
  ]
  edge [
    source 15
    target 76
  ]
  edge [
    source 15
    target 77
  ]
  edge [
    source 15
    target 78
  ]
  edge [
    source 15
    target 79
  ]
  edge [
    source 15
    target 80
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 81
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 82
  ]
  edge [
    source 17
    target 83
  ]
  edge [
    source 17
    target 84
  ]
  edge [
    source 17
    target 85
  ]
  edge [
    source 17
    target 86
  ]
  edge [
    source 17
    target 87
  ]
  edge [
    source 17
    target 88
  ]
  edge [
    source 17
    target 89
  ]
  edge [
    source 17
    target 90
  ]
  edge [
    source 18
    target 91
  ]
]
