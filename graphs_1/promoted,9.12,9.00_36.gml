graph [
  maxDegree 30
  minDegree 1
  meanDegree 1.9833333333333334
  density 0.016666666666666666
  graphCliqueNumber 2
  node [
    id 0
    label "urz&#281;dnik"
    origin "text"
  ]
  node [
    id 1
    label "wroc&#322;aw"
    origin "text"
  ]
  node [
    id 2
    label "wyda&#263;"
    origin "text"
  ]
  node [
    id 3
    label "pozwolenie"
    origin "text"
  ]
  node [
    id 4
    label "manifestacja"
    origin "text"
  ]
  node [
    id 5
    label "antyaborcyjny"
    origin "text"
  ]
  node [
    id 6
    label "miko&#322;ajek"
    origin "text"
  ]
  node [
    id 7
    label "sam"
    origin "text"
  ]
  node [
    id 8
    label "centrum"
    origin "text"
  ]
  node [
    id 9
    label "jarmark"
    origin "text"
  ]
  node [
    id 10
    label "&#347;wi&#261;teczny"
    origin "text"
  ]
  node [
    id 11
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 12
    label "przychodzi&#263;"
    origin "text"
  ]
  node [
    id 13
    label "dziecko"
    origin "text"
  ]
  node [
    id 14
    label "pracownik"
  ]
  node [
    id 15
    label "pragmatyka"
  ]
  node [
    id 16
    label "korpus_urz&#281;dniczy"
  ]
  node [
    id 17
    label "impart"
  ]
  node [
    id 18
    label "panna_na_wydaniu"
  ]
  node [
    id 19
    label "translate"
  ]
  node [
    id 20
    label "give"
  ]
  node [
    id 21
    label "pieni&#261;dze"
  ]
  node [
    id 22
    label "supply"
  ]
  node [
    id 23
    label "wprowadzi&#263;"
  ]
  node [
    id 24
    label "da&#263;"
  ]
  node [
    id 25
    label "zapach"
  ]
  node [
    id 26
    label "wydawnictwo"
  ]
  node [
    id 27
    label "powierzy&#263;"
  ]
  node [
    id 28
    label "produkcja"
  ]
  node [
    id 29
    label "poda&#263;"
  ]
  node [
    id 30
    label "skojarzy&#263;"
  ]
  node [
    id 31
    label "dress"
  ]
  node [
    id 32
    label "plon"
  ]
  node [
    id 33
    label "ujawni&#263;"
  ]
  node [
    id 34
    label "reszta"
  ]
  node [
    id 35
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 36
    label "zadenuncjowa&#263;"
  ]
  node [
    id 37
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 38
    label "zrobi&#263;"
  ]
  node [
    id 39
    label "tajemnica"
  ]
  node [
    id 40
    label "wiano"
  ]
  node [
    id 41
    label "gor&#261;cy_pieni&#261;dz"
  ]
  node [
    id 42
    label "wytworzy&#263;"
  ]
  node [
    id 43
    label "d&#378;wi&#281;k"
  ]
  node [
    id 44
    label "picture"
  ]
  node [
    id 45
    label "dokument"
  ]
  node [
    id 46
    label "zrobienie"
  ]
  node [
    id 47
    label "zwolnienie_si&#281;"
  ]
  node [
    id 48
    label "zwalnianie_si&#281;"
  ]
  node [
    id 49
    label "odpowied&#378;"
  ]
  node [
    id 50
    label "umo&#380;liwienie"
  ]
  node [
    id 51
    label "wiedza"
  ]
  node [
    id 52
    label "pozwole&#324;stwo"
  ]
  node [
    id 53
    label "franchise"
  ]
  node [
    id 54
    label "pofolgowanie"
  ]
  node [
    id 55
    label "zwolni&#263;_si&#281;"
  ]
  node [
    id 56
    label "zwalnia&#263;_si&#281;"
  ]
  node [
    id 57
    label "decyzja"
  ]
  node [
    id 58
    label "license"
  ]
  node [
    id 59
    label "bycie_w_stanie"
  ]
  node [
    id 60
    label "odwieszenie"
  ]
  node [
    id 61
    label "uznanie"
  ]
  node [
    id 62
    label "koncesjonowanie"
  ]
  node [
    id 63
    label "authorization"
  ]
  node [
    id 64
    label "exhibition"
  ]
  node [
    id 65
    label "grupa"
  ]
  node [
    id 66
    label "zgromadzenie"
  ]
  node [
    id 67
    label "show"
  ]
  node [
    id 68
    label "pokaz"
  ]
  node [
    id 69
    label "przeciwny"
  ]
  node [
    id 70
    label "bylina"
  ]
  node [
    id 71
    label "selerowate"
  ]
  node [
    id 72
    label "sklep"
  ]
  node [
    id 73
    label "miejsce"
  ]
  node [
    id 74
    label "centroprawica"
  ]
  node [
    id 75
    label "core"
  ]
  node [
    id 76
    label "Hollywood"
  ]
  node [
    id 77
    label "blok"
  ]
  node [
    id 78
    label "centrolew"
  ]
  node [
    id 79
    label "sejm"
  ]
  node [
    id 80
    label "punkt"
  ]
  node [
    id 81
    label "o&#347;rodek"
  ]
  node [
    id 82
    label "stoisko"
  ]
  node [
    id 83
    label "plac"
  ]
  node [
    id 84
    label "kram"
  ]
  node [
    id 85
    label "market"
  ]
  node [
    id 86
    label "obiekt_handlowy"
  ]
  node [
    id 87
    label "targowica"
  ]
  node [
    id 88
    label "targ"
  ]
  node [
    id 89
    label "dzie&#324;_wolny"
  ]
  node [
    id 90
    label "&#347;wi&#281;tny"
  ]
  node [
    id 91
    label "&#347;wi&#261;tecznie"
  ]
  node [
    id 92
    label "wyj&#261;tkowy"
  ]
  node [
    id 93
    label "od&#347;wi&#281;tny"
  ]
  node [
    id 94
    label "od&#347;wi&#281;tnie"
  ]
  node [
    id 95
    label "obrz&#281;dowy"
  ]
  node [
    id 96
    label "uroczysty"
  ]
  node [
    id 97
    label "przybywa&#263;"
  ]
  node [
    id 98
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 99
    label "dochodzi&#263;"
  ]
  node [
    id 100
    label "cz&#322;owiek"
  ]
  node [
    id 101
    label "potomstwo"
  ]
  node [
    id 102
    label "organizm"
  ]
  node [
    id 103
    label "sraluch"
  ]
  node [
    id 104
    label "utulanie"
  ]
  node [
    id 105
    label "pediatra"
  ]
  node [
    id 106
    label "dzieciarnia"
  ]
  node [
    id 107
    label "m&#322;odziak"
  ]
  node [
    id 108
    label "dzieciak"
  ]
  node [
    id 109
    label "utula&#263;"
  ]
  node [
    id 110
    label "potomek"
  ]
  node [
    id 111
    label "pedofil"
  ]
  node [
    id 112
    label "entliczek-pentliczek"
  ]
  node [
    id 113
    label "m&#322;odzik"
  ]
  node [
    id 114
    label "cz&#322;owieczek"
  ]
  node [
    id 115
    label "zwierz&#281;"
  ]
  node [
    id 116
    label "niepe&#322;noletni"
  ]
  node [
    id 117
    label "fledgling"
  ]
  node [
    id 118
    label "utuli&#263;"
  ]
  node [
    id 119
    label "utulenie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 13
    target 101
  ]
  edge [
    source 13
    target 102
  ]
  edge [
    source 13
    target 103
  ]
  edge [
    source 13
    target 104
  ]
  edge [
    source 13
    target 105
  ]
  edge [
    source 13
    target 106
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 13
    target 113
  ]
  edge [
    source 13
    target 114
  ]
  edge [
    source 13
    target 115
  ]
  edge [
    source 13
    target 116
  ]
  edge [
    source 13
    target 117
  ]
  edge [
    source 13
    target 118
  ]
  edge [
    source 13
    target 119
  ]
]
