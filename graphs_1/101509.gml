graph [
  maxDegree 13
  minDegree 1
  meanDegree 2
  density 0.02857142857142857
  graphCliqueNumber 2
  node [
    id 0
    label "lato"
    origin "text"
  ]
  node [
    id 1
    label "profesor"
    origin "text"
  ]
  node [
    id 2
    label "albert"
    origin "text"
  ]
  node [
    id 3
    label "bandura"
    origin "text"
  ]
  node [
    id 4
    label "uniwersytet"
    origin "text"
  ]
  node [
    id 5
    label "stanfordzkiego"
    origin "text"
  ]
  node [
    id 6
    label "przeprowadzi&#263;"
    origin "text"
  ]
  node [
    id 7
    label "badan"
    origin "text"
  ]
  node [
    id 8
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 9
    label "dzisiaj"
    origin "text"
  ]
  node [
    id 10
    label "znana"
    origin "text"
  ]
  node [
    id 11
    label "by&#263;"
    origin "text"
  ]
  node [
    id 12
    label "jako"
    origin "text"
  ]
  node [
    id 13
    label "lalka"
    origin "text"
  ]
  node [
    id 14
    label "bobo"
    origin "text"
  ]
  node [
    id 15
    label "experiment"
    origin "text"
  ]
  node [
    id 16
    label "pora_roku"
  ]
  node [
    id 17
    label "wirtuoz"
  ]
  node [
    id 18
    label "nauczyciel_akademicki"
  ]
  node [
    id 19
    label "tytu&#322;"
  ]
  node [
    id 20
    label "konsulent"
  ]
  node [
    id 21
    label "nauczyciel"
  ]
  node [
    id 22
    label "stopie&#324;_naukowy"
  ]
  node [
    id 23
    label "profesura"
  ]
  node [
    id 24
    label "chordofon_szarpany"
  ]
  node [
    id 25
    label "ku&#378;nia"
  ]
  node [
    id 26
    label "Harvard"
  ]
  node [
    id 27
    label "uczelnia"
  ]
  node [
    id 28
    label "Sorbona"
  ]
  node [
    id 29
    label "Uniwersytet_Oksfordzki"
  ]
  node [
    id 30
    label "Stanford"
  ]
  node [
    id 31
    label "Princeton"
  ]
  node [
    id 32
    label "academy"
  ]
  node [
    id 33
    label "szko&#322;a_wy&#380;sza"
  ]
  node [
    id 34
    label "wydzia&#322;"
  ]
  node [
    id 35
    label "pom&#243;c"
  ]
  node [
    id 36
    label "zbudowa&#263;"
  ]
  node [
    id 37
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 38
    label "leave"
  ]
  node [
    id 39
    label "przewie&#347;&#263;"
  ]
  node [
    id 40
    label "wykona&#263;"
  ]
  node [
    id 41
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 42
    label "draw"
  ]
  node [
    id 43
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 44
    label "carry"
  ]
  node [
    id 45
    label "doba"
  ]
  node [
    id 46
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 47
    label "dzi&#347;"
  ]
  node [
    id 48
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 49
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 50
    label "si&#281;ga&#263;"
  ]
  node [
    id 51
    label "trwa&#263;"
  ]
  node [
    id 52
    label "obecno&#347;&#263;"
  ]
  node [
    id 53
    label "stan"
  ]
  node [
    id 54
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 55
    label "stand"
  ]
  node [
    id 56
    label "mie&#263;_miejsce"
  ]
  node [
    id 57
    label "uczestniczy&#263;"
  ]
  node [
    id 58
    label "chodzi&#263;"
  ]
  node [
    id 59
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 60
    label "equal"
  ]
  node [
    id 61
    label "lalkarstwo"
  ]
  node [
    id 62
    label "statuetka"
  ]
  node [
    id 63
    label "zabawka"
  ]
  node [
    id 64
    label "matrioszka"
  ]
  node [
    id 65
    label "dummy"
  ]
  node [
    id 66
    label "duch"
  ]
  node [
    id 67
    label "niemowl&#281;"
  ]
  node [
    id 68
    label "dziecko"
  ]
  node [
    id 69
    label "bobok"
  ]
  node [
    id 70
    label "Stanfordzkiego"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 45
  ]
  edge [
    source 9
    target 46
  ]
  edge [
    source 9
    target 47
  ]
  edge [
    source 9
    target 48
  ]
  edge [
    source 9
    target 49
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 50
  ]
  edge [
    source 11
    target 51
  ]
  edge [
    source 11
    target 52
  ]
  edge [
    source 11
    target 53
  ]
  edge [
    source 11
    target 54
  ]
  edge [
    source 11
    target 55
  ]
  edge [
    source 11
    target 56
  ]
  edge [
    source 11
    target 57
  ]
  edge [
    source 11
    target 58
  ]
  edge [
    source 11
    target 59
  ]
  edge [
    source 11
    target 60
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 61
  ]
  edge [
    source 13
    target 62
  ]
  edge [
    source 13
    target 63
  ]
  edge [
    source 13
    target 64
  ]
  edge [
    source 13
    target 65
  ]
  edge [
    source 14
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 66
  ]
  edge [
    source 14
    target 67
  ]
  edge [
    source 14
    target 68
  ]
  edge [
    source 14
    target 69
  ]
]
