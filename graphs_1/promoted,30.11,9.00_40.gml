graph [
  maxDegree 13
  minDegree 1
  meanDegree 2.048780487804878
  density 0.05121951219512195
  graphCliqueNumber 3
  node [
    id 0
    label "nalot"
    origin "text"
  ]
  node [
    id 1
    label "policja"
    origin "text"
  ]
  node [
    id 2
    label "siedziba"
    origin "text"
  ]
  node [
    id 3
    label "deutsche"
    origin "text"
  ]
  node [
    id 4
    label "bank"
    origin "text"
  ]
  node [
    id 5
    label "niemcy"
    origin "text"
  ]
  node [
    id 6
    label "outbreak"
  ]
  node [
    id 7
    label "nieoczekiwany"
  ]
  node [
    id 8
    label "przybycie"
  ]
  node [
    id 9
    label "inpouring"
  ]
  node [
    id 10
    label "t&#322;um"
  ]
  node [
    id 11
    label "pow&#322;oka"
  ]
  node [
    id 12
    label "koniofagia"
  ]
  node [
    id 13
    label "organ"
  ]
  node [
    id 14
    label "wydarzenie"
  ]
  node [
    id 15
    label "wp&#322;yw"
  ]
  node [
    id 16
    label "py&#322;"
  ]
  node [
    id 17
    label "meszek"
  ]
  node [
    id 18
    label "komisariat"
  ]
  node [
    id 19
    label "psiarnia"
  ]
  node [
    id 20
    label "posterunek"
  ]
  node [
    id 21
    label "grupa"
  ]
  node [
    id 22
    label "s&#322;u&#380;ba"
  ]
  node [
    id 23
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 24
    label "Kreml"
  ]
  node [
    id 25
    label "miejsce"
  ]
  node [
    id 26
    label "budynek"
  ]
  node [
    id 27
    label "Bia&#322;y_Dom"
  ]
  node [
    id 28
    label "&#321;ubianka"
  ]
  node [
    id 29
    label "sadowisko"
  ]
  node [
    id 30
    label "dzia&#322;_personalny"
  ]
  node [
    id 31
    label "miejsce_pracy"
  ]
  node [
    id 32
    label "wk&#322;adca"
  ]
  node [
    id 33
    label "agencja"
  ]
  node [
    id 34
    label "konto"
  ]
  node [
    id 35
    label "agent_rozliczeniowy"
  ]
  node [
    id 36
    label "eurorynek"
  ]
  node [
    id 37
    label "zbi&#243;r"
  ]
  node [
    id 38
    label "instytucja"
  ]
  node [
    id 39
    label "kwota"
  ]
  node [
    id 40
    label "Deutsche"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
]
