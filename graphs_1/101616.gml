graph [
  maxDegree 10
  minDegree 1
  meanDegree 1.84
  density 0.07666666666666666
  graphCliqueNumber 2
  node [
    id 0
    label "wtem"
    origin "text"
  ]
  node [
    id 1
    label "spojrze&#263;"
    origin "text"
  ]
  node [
    id 2
    label "przysta&#324;"
    origin "text"
  ]
  node [
    id 3
    label "nedia"
    origin "text"
  ]
  node [
    id 4
    label "zawo&#322;a&#263;"
    origin "text"
  ]
  node [
    id 5
    label "niespodziewanie"
  ]
  node [
    id 6
    label "raptownie"
  ]
  node [
    id 7
    label "pojrze&#263;"
  ]
  node [
    id 8
    label "popatrze&#263;"
  ]
  node [
    id 9
    label "zinterpretowa&#263;"
  ]
  node [
    id 10
    label "spoziera&#263;"
  ]
  node [
    id 11
    label "pogl&#261;dn&#261;&#263;"
  ]
  node [
    id 12
    label "sp&#243;jrze&#263;"
  ]
  node [
    id 13
    label "see"
  ]
  node [
    id 14
    label "peek"
  ]
  node [
    id 15
    label "port"
  ]
  node [
    id 16
    label "slip"
  ]
  node [
    id 17
    label "cry"
  ]
  node [
    id 18
    label "powiedzie&#263;"
  ]
  node [
    id 19
    label "invite"
  ]
  node [
    id 20
    label "krzykn&#261;&#263;"
  ]
  node [
    id 21
    label "przewo&#322;a&#263;"
  ]
  node [
    id 22
    label "nakaza&#263;"
  ]
  node [
    id 23
    label "zatoka"
  ]
  node [
    id 24
    label "odeski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 23
    target 24
  ]
]
