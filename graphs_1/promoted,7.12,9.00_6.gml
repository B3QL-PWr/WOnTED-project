graph [
  maxDegree 22
  minDegree 1
  meanDegree 1.986013986013986
  density 0.013986013986013986
  graphCliqueNumber 2
  node [
    id 0
    label "wojciech"
    origin "text"
  ]
  node [
    id 1
    label "jabczy&#324;ski"
    origin "text"
  ]
  node [
    id 2
    label "zapowiedzie&#263;"
    origin "text"
  ]
  node [
    id 3
    label "dzia&#322;"
    origin "text"
  ]
  node [
    id 4
    label "prawny"
    origin "text"
  ]
  node [
    id 5
    label "bezpiecze&#324;stwo"
    origin "text"
  ]
  node [
    id 6
    label "rozwa&#380;y&#263;"
    origin "text"
  ]
  node [
    id 7
    label "odpowiedni"
    origin "text"
  ]
  node [
    id 8
    label "krok"
    origin "text"
  ]
  node [
    id 9
    label "przeciwko"
    origin "text"
  ]
  node [
    id 10
    label "patostrimerowi"
    origin "text"
  ]
  node [
    id 11
    label "daniel"
    origin "text"
  ]
  node [
    id 12
    label "magicalowi"
    origin "text"
  ]
  node [
    id 13
    label "korzysta&#263;"
    origin "text"
  ]
  node [
    id 14
    label "&#347;wiad&#322;owodu"
    origin "text"
  ]
  node [
    id 15
    label "orange"
    origin "text"
  ]
  node [
    id 16
    label "szerzy&#263;"
    origin "text"
  ]
  node [
    id 17
    label "patologiczny"
    origin "text"
  ]
  node [
    id 18
    label "tre&#347;&#263;"
    origin "text"
  ]
  node [
    id 19
    label "internet"
    origin "text"
  ]
  node [
    id 20
    label "info"
    origin "text"
  ]
  node [
    id 21
    label "komentarz"
    origin "text"
  ]
  node [
    id 22
    label "announce"
  ]
  node [
    id 23
    label "spowodowa&#263;"
  ]
  node [
    id 24
    label "przestrzec"
  ]
  node [
    id 25
    label "sign"
  ]
  node [
    id 26
    label "poinformowa&#263;"
  ]
  node [
    id 27
    label "og&#322;osi&#263;"
  ]
  node [
    id 28
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 29
    label "zakres"
  ]
  node [
    id 30
    label "whole"
  ]
  node [
    id 31
    label "wytw&#243;r"
  ]
  node [
    id 32
    label "column"
  ]
  node [
    id 33
    label "distribution"
  ]
  node [
    id 34
    label "bezdro&#380;e"
  ]
  node [
    id 35
    label "competence"
  ]
  node [
    id 36
    label "zesp&#243;&#322;"
  ]
  node [
    id 37
    label "urz&#261;d"
  ]
  node [
    id 38
    label "jednostka_organizacyjna"
  ]
  node [
    id 39
    label "stopie&#324;"
  ]
  node [
    id 40
    label "insourcing"
  ]
  node [
    id 41
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 42
    label "sfera"
  ]
  node [
    id 43
    label "miejsce_pracy"
  ]
  node [
    id 44
    label "poddzia&#322;"
  ]
  node [
    id 45
    label "prawniczo"
  ]
  node [
    id 46
    label "prawnie"
  ]
  node [
    id 47
    label "konstytucyjnoprawny"
  ]
  node [
    id 48
    label "legalny"
  ]
  node [
    id 49
    label "jurydyczny"
  ]
  node [
    id 50
    label "BHP"
  ]
  node [
    id 51
    label "katapultowa&#263;"
  ]
  node [
    id 52
    label "ubezpiecza&#263;"
  ]
  node [
    id 53
    label "stan"
  ]
  node [
    id 54
    label "ubezpieczenie"
  ]
  node [
    id 55
    label "cecha"
  ]
  node [
    id 56
    label "porz&#261;dek"
  ]
  node [
    id 57
    label "ubezpieczy&#263;"
  ]
  node [
    id 58
    label "safety"
  ]
  node [
    id 59
    label "katapultowanie"
  ]
  node [
    id 60
    label "ubezpieczanie"
  ]
  node [
    id 61
    label "test_zderzeniowy"
  ]
  node [
    id 62
    label "przeprowadzi&#263;"
  ]
  node [
    id 63
    label "reason"
  ]
  node [
    id 64
    label "reflect"
  ]
  node [
    id 65
    label "przygl&#261;dn&#261;&#263;_si&#281;"
  ]
  node [
    id 66
    label "wyporcjowa&#263;"
  ]
  node [
    id 67
    label "specjalny"
  ]
  node [
    id 68
    label "nale&#380;yty"
  ]
  node [
    id 69
    label "stosownie"
  ]
  node [
    id 70
    label "zdarzony"
  ]
  node [
    id 71
    label "odpowiednio"
  ]
  node [
    id 72
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 73
    label "odpowiadanie"
  ]
  node [
    id 74
    label "nale&#380;ny"
  ]
  node [
    id 75
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 76
    label "pace"
  ]
  node [
    id 77
    label "czyn"
  ]
  node [
    id 78
    label "passus"
  ]
  node [
    id 79
    label "measurement"
  ]
  node [
    id 80
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 81
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 82
    label "ruch"
  ]
  node [
    id 83
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 84
    label "action"
  ]
  node [
    id 85
    label "chodzi&#263;"
  ]
  node [
    id 86
    label "tu&#322;&#243;w"
  ]
  node [
    id 87
    label "skejt"
  ]
  node [
    id 88
    label "step"
  ]
  node [
    id 89
    label "becze&#263;"
  ]
  node [
    id 90
    label "zabecze&#263;"
  ]
  node [
    id 91
    label "&#347;wiece"
  ]
  node [
    id 92
    label "kwiat"
  ]
  node [
    id 93
    label "zwierz&#281;_&#322;owne"
  ]
  node [
    id 94
    label "zwierzyna_p&#322;owa"
  ]
  node [
    id 95
    label "prze&#380;uwacz"
  ]
  node [
    id 96
    label "jeleniowate"
  ]
  node [
    id 97
    label "badyl"
  ]
  node [
    id 98
    label "use"
  ]
  node [
    id 99
    label "uzyskiwa&#263;"
  ]
  node [
    id 100
    label "u&#380;ywa&#263;"
  ]
  node [
    id 101
    label "generalize"
  ]
  node [
    id 102
    label "sprawia&#263;"
  ]
  node [
    id 103
    label "zaburzony"
  ]
  node [
    id 104
    label "patologicznie"
  ]
  node [
    id 105
    label "chorobliwie"
  ]
  node [
    id 106
    label "straszny"
  ]
  node [
    id 107
    label "chory"
  ]
  node [
    id 108
    label "problematyczny"
  ]
  node [
    id 109
    label "nienormalny"
  ]
  node [
    id 110
    label "dysfunkcyjny"
  ]
  node [
    id 111
    label "chorobliwy"
  ]
  node [
    id 112
    label "zawarto&#347;&#263;"
  ]
  node [
    id 113
    label "temat"
  ]
  node [
    id 114
    label "istota"
  ]
  node [
    id 115
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 116
    label "informacja"
  ]
  node [
    id 117
    label "us&#322;uga_internetowa"
  ]
  node [
    id 118
    label "biznes_elektroniczny"
  ]
  node [
    id 119
    label "punkt_dost&#281;pu"
  ]
  node [
    id 120
    label "hipertekst"
  ]
  node [
    id 121
    label "gra_sieciowa"
  ]
  node [
    id 122
    label "mem"
  ]
  node [
    id 123
    label "e-hazard"
  ]
  node [
    id 124
    label "sie&#263;_komputerowa"
  ]
  node [
    id 125
    label "media"
  ]
  node [
    id 126
    label "podcast"
  ]
  node [
    id 127
    label "netbook"
  ]
  node [
    id 128
    label "provider"
  ]
  node [
    id 129
    label "cyberprzestrze&#324;"
  ]
  node [
    id 130
    label "grooming"
  ]
  node [
    id 131
    label "strona"
  ]
  node [
    id 132
    label "comment"
  ]
  node [
    id 133
    label "artyku&#322;"
  ]
  node [
    id 134
    label "ocena"
  ]
  node [
    id 135
    label "gossip"
  ]
  node [
    id 136
    label "interpretacja"
  ]
  node [
    id 137
    label "audycja"
  ]
  node [
    id 138
    label "tekst"
  ]
  node [
    id 139
    label "Wojciech"
  ]
  node [
    id 140
    label "Jabczy&#324;ski"
  ]
  node [
    id 141
    label "Daniel"
  ]
  node [
    id 142
    label "Magicalowi"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 3
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 11
    target 92
  ]
  edge [
    source 11
    target 93
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 98
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 101
  ]
  edge [
    source 16
    target 102
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 103
  ]
  edge [
    source 17
    target 104
  ]
  edge [
    source 17
    target 105
  ]
  edge [
    source 17
    target 106
  ]
  edge [
    source 17
    target 107
  ]
  edge [
    source 17
    target 108
  ]
  edge [
    source 17
    target 109
  ]
  edge [
    source 17
    target 110
  ]
  edge [
    source 17
    target 111
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 112
  ]
  edge [
    source 18
    target 113
  ]
  edge [
    source 18
    target 114
  ]
  edge [
    source 18
    target 115
  ]
  edge [
    source 18
    target 116
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 117
  ]
  edge [
    source 19
    target 118
  ]
  edge [
    source 19
    target 119
  ]
  edge [
    source 19
    target 120
  ]
  edge [
    source 19
    target 121
  ]
  edge [
    source 19
    target 122
  ]
  edge [
    source 19
    target 123
  ]
  edge [
    source 19
    target 124
  ]
  edge [
    source 19
    target 125
  ]
  edge [
    source 19
    target 126
  ]
  edge [
    source 19
    target 127
  ]
  edge [
    source 19
    target 128
  ]
  edge [
    source 19
    target 129
  ]
  edge [
    source 19
    target 130
  ]
  edge [
    source 19
    target 131
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 132
  ]
  edge [
    source 21
    target 133
  ]
  edge [
    source 21
    target 134
  ]
  edge [
    source 21
    target 135
  ]
  edge [
    source 21
    target 136
  ]
  edge [
    source 21
    target 137
  ]
  edge [
    source 21
    target 138
  ]
  edge [
    source 139
    target 140
  ]
  edge [
    source 141
    target 142
  ]
]
