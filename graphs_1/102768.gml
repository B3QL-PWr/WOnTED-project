graph [
  maxDegree 18
  minDegree 1
  meanDegree 1.9487179487179487
  density 0.05128205128205128
  graphCliqueNumber 2
  node [
    id 0
    label "dziennik"
    origin "text"
  ]
  node [
    id 1
    label "urz&#281;dowy"
    origin "text"
  ]
  node [
    id 2
    label "minister"
    origin "text"
  ]
  node [
    id 3
    label "finanse"
    origin "text"
  ]
  node [
    id 4
    label "maj"
    origin "text"
  ]
  node [
    id 5
    label "poz"
    origin "text"
  ]
  node [
    id 6
    label "spis"
  ]
  node [
    id 7
    label "sheet"
  ]
  node [
    id 8
    label "gazeta"
  ]
  node [
    id 9
    label "diariusz"
  ]
  node [
    id 10
    label "pami&#281;tnik"
  ]
  node [
    id 11
    label "journal"
  ]
  node [
    id 12
    label "ksi&#281;ga"
  ]
  node [
    id 13
    label "program_informacyjny"
  ]
  node [
    id 14
    label "urz&#281;dowo"
  ]
  node [
    id 15
    label "oficjalny"
  ]
  node [
    id 16
    label "formalny"
  ]
  node [
    id 17
    label "Goebbels"
  ]
  node [
    id 18
    label "Sto&#322;ypin"
  ]
  node [
    id 19
    label "rz&#261;d"
  ]
  node [
    id 20
    label "dostojnik"
  ]
  node [
    id 21
    label "uruchomienie"
  ]
  node [
    id 22
    label "nap&#322;yn&#261;&#263;"
  ]
  node [
    id 23
    label "nauka_ekonomiczna"
  ]
  node [
    id 24
    label "supernadz&#243;r"
  ]
  node [
    id 25
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 26
    label "nap&#322;yni&#281;cie"
  ]
  node [
    id 27
    label "absolutorium"
  ]
  node [
    id 28
    label "podupada&#263;"
  ]
  node [
    id 29
    label "nap&#322;ywanie"
  ]
  node [
    id 30
    label "podupadanie"
  ]
  node [
    id 31
    label "kwestor"
  ]
  node [
    id 32
    label "uruchamia&#263;"
  ]
  node [
    id 33
    label "mienie"
  ]
  node [
    id 34
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 35
    label "uruchamianie"
  ]
  node [
    id 36
    label "czynnik_produkcji"
  ]
  node [
    id 37
    label "&#346;wi&#281;to_Pracy"
  ]
  node [
    id 38
    label "miesi&#261;c"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
]
