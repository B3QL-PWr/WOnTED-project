graph [
  maxDegree 15
  minDegree 1
  meanDegree 2
  density 0.045454545454545456
  graphCliqueNumber 2
  node [
    id 0
    label "pisa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "list"
    origin "text"
  ]
  node [
    id 2
    label "edycja"
    origin "text"
  ]
  node [
    id 3
    label "zapami&#281;ta&#263;"
    origin "text"
  ]
  node [
    id 4
    label "ten"
    origin "text"
  ]
  node [
    id 5
    label "wpis"
    origin "text"
  ]
  node [
    id 6
    label "ozdabia&#263;"
  ]
  node [
    id 7
    label "dysgrafia"
  ]
  node [
    id 8
    label "prasa"
  ]
  node [
    id 9
    label "spell"
  ]
  node [
    id 10
    label "skryba"
  ]
  node [
    id 11
    label "donosi&#263;"
  ]
  node [
    id 12
    label "code"
  ]
  node [
    id 13
    label "tekst"
  ]
  node [
    id 14
    label "dysortografia"
  ]
  node [
    id 15
    label "read"
  ]
  node [
    id 16
    label "tworzy&#263;"
  ]
  node [
    id 17
    label "formu&#322;owa&#263;"
  ]
  node [
    id 18
    label "styl"
  ]
  node [
    id 19
    label "stawia&#263;"
  ]
  node [
    id 20
    label "li&#347;&#263;"
  ]
  node [
    id 21
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 22
    label "poczta"
  ]
  node [
    id 23
    label "epistolografia"
  ]
  node [
    id 24
    label "przesy&#322;ka"
  ]
  node [
    id 25
    label "poczta_elektroniczna"
  ]
  node [
    id 26
    label "znaczek_pocztowy"
  ]
  node [
    id 27
    label "impression"
  ]
  node [
    id 28
    label "odmiana"
  ]
  node [
    id 29
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 30
    label "notification"
  ]
  node [
    id 31
    label "cykl"
  ]
  node [
    id 32
    label "zmiana"
  ]
  node [
    id 33
    label "produkcja"
  ]
  node [
    id 34
    label "egzemplarz"
  ]
  node [
    id 35
    label "zachowa&#263;"
  ]
  node [
    id 36
    label "set"
  ]
  node [
    id 37
    label "mnemotechnika"
  ]
  node [
    id 38
    label "okre&#347;lony"
  ]
  node [
    id 39
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 40
    label "czynno&#347;&#263;"
  ]
  node [
    id 41
    label "entrance"
  ]
  node [
    id 42
    label "inscription"
  ]
  node [
    id 43
    label "akt"
  ]
  node [
    id 44
    label "op&#322;ata"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 13
  ]
]
