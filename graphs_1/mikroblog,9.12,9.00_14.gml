graph [
  maxDegree 2
  minDegree 1
  meanDegree 1.6666666666666667
  density 0.3333333333333333
  graphCliqueNumber 2
  node [
    id 0
    label "przeprasza&#263;"
    origin "text"
  ]
  node [
    id 1
    label "wszyscy"
    origin "text"
  ]
  node [
    id 2
    label "tak"
    origin "text"
  ]
  node [
    id 3
    label "p&#243;&#378;no"
    origin "text"
  ]
  node [
    id 4
    label "sk&#322;ada&#263;"
  ]
  node [
    id 5
    label "p&#243;&#378;ny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 5
  ]
]
