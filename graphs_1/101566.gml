graph [
  maxDegree 14
  minDegree 1
  meanDegree 2.036036036036036
  density 0.01850941850941851
  graphCliqueNumber 2
  node [
    id 0
    label "znaczy&#263;"
    origin "text"
  ]
  node [
    id 1
    label "dlaczego"
    origin "text"
  ]
  node [
    id 2
    label "kaza&#263;"
    origin "text"
  ]
  node [
    id 3
    label "ucieka&#263;"
    origin "text"
  ]
  node [
    id 4
    label "warszawa"
    origin "text"
  ]
  node [
    id 5
    label "ach"
    origin "text"
  ]
  node [
    id 6
    label "rozumie&#263;"
    origin "text"
  ]
  node [
    id 7
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 8
    label "m&#281;czy&#263;"
    origin "text"
  ]
  node [
    id 9
    label "bakalarstwo"
    origin "text"
  ]
  node [
    id 10
    label "tym"
    origin "text"
  ]
  node [
    id 11
    label "du&#380;o"
    origin "text"
  ]
  node [
    id 12
    label "dzi&#347;"
    origin "text"
  ]
  node [
    id 13
    label "kiedy"
    origin "text"
  ]
  node [
    id 14
    label "wsi"
    origin "text"
  ]
  node [
    id 15
    label "wpad&#322;y"
    origin "text"
  ]
  node [
    id 16
    label "chaos"
    origin "text"
  ]
  node [
    id 17
    label "ale"
    origin "text"
  ]
  node [
    id 18
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 19
    label "s&#322;uszno&#347;&#263;"
    origin "text"
  ]
  node [
    id 20
    label "by&#263;"
    origin "text"
  ]
  node [
    id 21
    label "niezno&#347;ny"
    origin "text"
  ]
  node [
    id 22
    label "spell"
  ]
  node [
    id 23
    label "odgrywa&#263;_rol&#281;"
  ]
  node [
    id 24
    label "zostawia&#263;"
  ]
  node [
    id 25
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 26
    label "represent"
  ]
  node [
    id 27
    label "count"
  ]
  node [
    id 28
    label "wyraz"
  ]
  node [
    id 29
    label "zdobi&#263;"
  ]
  node [
    id 30
    label "zmusza&#263;"
  ]
  node [
    id 31
    label "nakazywa&#263;"
  ]
  node [
    id 32
    label "wymaga&#263;"
  ]
  node [
    id 33
    label "zmusi&#263;"
  ]
  node [
    id 34
    label "wyg&#322;asza&#263;"
  ]
  node [
    id 35
    label "command"
  ]
  node [
    id 36
    label "order"
  ]
  node [
    id 37
    label "say"
  ]
  node [
    id 38
    label "wyg&#322;osi&#263;"
  ]
  node [
    id 39
    label "nakaza&#263;"
  ]
  node [
    id 40
    label "spieprza&#263;"
  ]
  node [
    id 41
    label "unika&#263;"
  ]
  node [
    id 42
    label "zwiewa&#263;"
  ]
  node [
    id 43
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 44
    label "wymyka&#263;_si&#281;"
  ]
  node [
    id 45
    label "pali&#263;_wrotki"
  ]
  node [
    id 46
    label "wydostawa&#263;_si&#281;"
  ]
  node [
    id 47
    label "bra&#263;"
  ]
  node [
    id 48
    label "blow"
  ]
  node [
    id 49
    label "Warszawa"
  ]
  node [
    id 50
    label "samoch&#243;d"
  ]
  node [
    id 51
    label "fastback"
  ]
  node [
    id 52
    label "wiedzie&#263;"
  ]
  node [
    id 53
    label "zna&#263;"
  ]
  node [
    id 54
    label "czu&#263;"
  ]
  node [
    id 55
    label "j&#281;zyk"
  ]
  node [
    id 56
    label "kuma&#263;"
  ]
  node [
    id 57
    label "give"
  ]
  node [
    id 58
    label "odbiera&#263;"
  ]
  node [
    id 59
    label "empatia"
  ]
  node [
    id 60
    label "see"
  ]
  node [
    id 61
    label "match"
  ]
  node [
    id 62
    label "dziama&#263;"
  ]
  node [
    id 63
    label "nudzi&#263;"
  ]
  node [
    id 64
    label "krzywdzi&#263;"
  ]
  node [
    id 65
    label "wywo&#322;ywa&#263;"
  ]
  node [
    id 66
    label "tease"
  ]
  node [
    id 67
    label "mistreat"
  ]
  node [
    id 68
    label "nauczycielstwo"
  ]
  node [
    id 69
    label "du&#380;y"
  ]
  node [
    id 70
    label "cz&#281;sto"
  ]
  node [
    id 71
    label "bardzo"
  ]
  node [
    id 72
    label "mocno"
  ]
  node [
    id 73
    label "wiela"
  ]
  node [
    id 74
    label "doba"
  ]
  node [
    id 75
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 76
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 77
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 78
    label "&#380;mij"
  ]
  node [
    id 79
    label "zdezorganizowanie"
  ]
  node [
    id 80
    label "cecha"
  ]
  node [
    id 81
    label "disorder"
  ]
  node [
    id 82
    label "materia"
  ]
  node [
    id 83
    label "rozpierducha"
  ]
  node [
    id 84
    label "ba&#322;agan"
  ]
  node [
    id 85
    label "ko&#322;omyja"
  ]
  node [
    id 86
    label "sytuacja"
  ]
  node [
    id 87
    label "piwo"
  ]
  node [
    id 88
    label "czyj&#347;"
  ]
  node [
    id 89
    label "m&#261;&#380;"
  ]
  node [
    id 90
    label "prawda"
  ]
  node [
    id 91
    label "prawdziwo&#347;&#263;"
  ]
  node [
    id 92
    label "zasadno&#347;&#263;"
  ]
  node [
    id 93
    label "w&#322;a&#347;ciwo&#347;&#263;"
  ]
  node [
    id 94
    label "si&#281;ga&#263;"
  ]
  node [
    id 95
    label "trwa&#263;"
  ]
  node [
    id 96
    label "obecno&#347;&#263;"
  ]
  node [
    id 97
    label "stan"
  ]
  node [
    id 98
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 99
    label "stand"
  ]
  node [
    id 100
    label "mie&#263;_miejsce"
  ]
  node [
    id 101
    label "uczestniczy&#263;"
  ]
  node [
    id 102
    label "chodzi&#263;"
  ]
  node [
    id 103
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 104
    label "equal"
  ]
  node [
    id 105
    label "dokuczliwy"
  ]
  node [
    id 106
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 107
    label "niegrzeczny"
  ]
  node [
    id 108
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 109
    label "niezno&#347;nie"
  ]
  node [
    id 110
    label "uci&#261;&#380;liwy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 69
  ]
  edge [
    source 11
    target 70
  ]
  edge [
    source 11
    target 71
  ]
  edge [
    source 11
    target 72
  ]
  edge [
    source 11
    target 73
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 74
  ]
  edge [
    source 12
    target 75
  ]
  edge [
    source 12
    target 76
  ]
  edge [
    source 12
    target 77
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 78
  ]
  edge [
    source 16
    target 79
  ]
  edge [
    source 16
    target 80
  ]
  edge [
    source 16
    target 81
  ]
  edge [
    source 16
    target 82
  ]
  edge [
    source 16
    target 83
  ]
  edge [
    source 16
    target 84
  ]
  edge [
    source 16
    target 85
  ]
  edge [
    source 16
    target 86
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 87
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 88
  ]
  edge [
    source 18
    target 89
  ]
  edge [
    source 19
    target 90
  ]
  edge [
    source 19
    target 91
  ]
  edge [
    source 19
    target 92
  ]
  edge [
    source 19
    target 93
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 94
  ]
  edge [
    source 20
    target 95
  ]
  edge [
    source 20
    target 96
  ]
  edge [
    source 20
    target 97
  ]
  edge [
    source 20
    target 98
  ]
  edge [
    source 20
    target 99
  ]
  edge [
    source 20
    target 100
  ]
  edge [
    source 20
    target 101
  ]
  edge [
    source 20
    target 102
  ]
  edge [
    source 20
    target 103
  ]
  edge [
    source 20
    target 104
  ]
  edge [
    source 21
    target 105
  ]
  edge [
    source 21
    target 106
  ]
  edge [
    source 21
    target 107
  ]
  edge [
    source 21
    target 108
  ]
  edge [
    source 21
    target 109
  ]
  edge [
    source 21
    target 110
  ]
]
