graph [
  maxDegree 1
  minDegree 1
  meanDegree 1
  density 1.0
  graphCliqueNumber 2
  node [
    id 0
    label "taga"
    origin "text"
  ]
  node [
    id 1
    label "jeb&#322;em"
    origin "text"
  ]
  edge [
    source 0
    target 1
  ]
]
