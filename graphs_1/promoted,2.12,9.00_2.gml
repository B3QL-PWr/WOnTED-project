graph [
  maxDegree 12
  minDegree 1
  meanDegree 1.9090909090909092
  density 0.09090909090909091
  graphCliqueNumber 2
  node [
    id 0
    label "uwaga"
    origin "text"
  ]
  node [
    id 1
    label "klient"
    origin "text"
  ]
  node [
    id 2
    label "inga"
    origin "text"
  ]
  node [
    id 3
    label "nagana"
  ]
  node [
    id 4
    label "wypowied&#378;"
  ]
  node [
    id 5
    label "stan"
  ]
  node [
    id 6
    label "dzienniczek"
  ]
  node [
    id 7
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 8
    label "wzgl&#261;d"
  ]
  node [
    id 9
    label "gossip"
  ]
  node [
    id 10
    label "upomnienie"
  ]
  node [
    id 11
    label "tekst"
  ]
  node [
    id 12
    label "cz&#322;owiek"
  ]
  node [
    id 13
    label "bratek"
  ]
  node [
    id 14
    label "klientela"
  ]
  node [
    id 15
    label "szlachcic"
  ]
  node [
    id 16
    label "agent_rozliczeniowy"
  ]
  node [
    id 17
    label "komputer_cyfrowy"
  ]
  node [
    id 18
    label "program"
  ]
  node [
    id 19
    label "us&#322;ugobiorca"
  ]
  node [
    id 20
    label "Rzymianin"
  ]
  node [
    id 21
    label "obywatel"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
]
