graph [
  maxDegree 71
  minDegree 1
  meanDegree 2.0448430493273544
  density 0.009211004726699794
  graphCliqueNumber 3
  node [
    id 0
    label "nowy"
    origin "text"
  ]
  node [
    id 1
    label "tydzie&#324;"
    origin "text"
  ]
  node [
    id 2
    label "nowa"
    origin "text"
  ]
  node [
    id 3
    label "wie&#347;&#263;"
    origin "text"
  ]
  node [
    id 4
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 5
    label "qmam&#243;w"
    origin "text"
  ]
  node [
    id 6
    label "oto"
    origin "text"
  ]
  node [
    id 7
    label "kolejny"
    origin "text"
  ]
  node [
    id 8
    label "laureat"
    origin "text"
  ]
  node [
    id 9
    label "miesi&#281;czny"
    origin "text"
  ]
  node [
    id 10
    label "wyb&#243;r"
    origin "text"
  ]
  node [
    id 11
    label "konkurs"
    origin "text"
  ]
  node [
    id 12
    label "qmam"
    origin "text"
  ]
  node [
    id 13
    label "media"
    origin "text"
  ]
  node [
    id 14
    label "szkolny"
    origin "text"
  ]
  node [
    id 15
    label "mur"
    origin "text"
  ]
  node [
    id 16
    label "m&#322;ynek"
    origin "text"
  ]
  node [
    id 17
    label "&#380;abi"
    origin "text"
  ]
  node [
    id 18
    label "g&#322;os"
    origin "text"
  ]
  node [
    id 19
    label "gimnews"
    origin "text"
  ]
  node [
    id 20
    label "przecinek"
    origin "text"
  ]
  node [
    id 21
    label "m&#322;odzie&#380;"
    origin "text"
  ]
  node [
    id 22
    label "omnibus"
    origin "text"
  ]
  node [
    id 23
    label "cz&#322;owiek"
  ]
  node [
    id 24
    label "nowotny"
  ]
  node [
    id 25
    label "drugi"
  ]
  node [
    id 26
    label "bie&#380;&#261;cy"
  ]
  node [
    id 27
    label "nowo"
  ]
  node [
    id 28
    label "narybek"
  ]
  node [
    id 29
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 30
    label "obcy"
  ]
  node [
    id 31
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 32
    label "doba"
  ]
  node [
    id 33
    label "czas"
  ]
  node [
    id 34
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 35
    label "weekend"
  ]
  node [
    id 36
    label "miesi&#261;c"
  ]
  node [
    id 37
    label "gwiazda"
  ]
  node [
    id 38
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 39
    label "control"
  ]
  node [
    id 40
    label "zniesienie"
  ]
  node [
    id 41
    label "manage"
  ]
  node [
    id 42
    label "depesza_emska"
  ]
  node [
    id 43
    label "message"
  ]
  node [
    id 44
    label "signal"
  ]
  node [
    id 45
    label "zarys"
  ]
  node [
    id 46
    label "komunikat"
  ]
  node [
    id 47
    label "prowadzi&#263;"
  ]
  node [
    id 48
    label "kierowa&#263;"
  ]
  node [
    id 49
    label "znosi&#263;"
  ]
  node [
    id 50
    label "informacja"
  ]
  node [
    id 51
    label "znie&#347;&#263;"
  ]
  node [
    id 52
    label "manipulate"
  ]
  node [
    id 53
    label "communication"
  ]
  node [
    id 54
    label "znoszenie"
  ]
  node [
    id 55
    label "navigate"
  ]
  node [
    id 56
    label "przewodniczy&#263;"
  ]
  node [
    id 57
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 58
    label "cope"
  ]
  node [
    id 59
    label "nap&#322;ywanie"
  ]
  node [
    id 60
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 61
    label "powodowa&#263;"
  ]
  node [
    id 62
    label "popycha&#263;"
  ]
  node [
    id 63
    label "p&#281;dzi&#263;"
  ]
  node [
    id 64
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 65
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 66
    label "obszar"
  ]
  node [
    id 67
    label "obiekt_naturalny"
  ]
  node [
    id 68
    label "przedmiot"
  ]
  node [
    id 69
    label "biosfera"
  ]
  node [
    id 70
    label "grupa"
  ]
  node [
    id 71
    label "stw&#243;r"
  ]
  node [
    id 72
    label "Stary_&#346;wiat"
  ]
  node [
    id 73
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 74
    label "rzecz"
  ]
  node [
    id 75
    label "magnetosfera"
  ]
  node [
    id 76
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 77
    label "environment"
  ]
  node [
    id 78
    label "Nowy_&#346;wiat"
  ]
  node [
    id 79
    label "geosfera"
  ]
  node [
    id 80
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 81
    label "planeta"
  ]
  node [
    id 82
    label "przejmowa&#263;"
  ]
  node [
    id 83
    label "litosfera"
  ]
  node [
    id 84
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 85
    label "makrokosmos"
  ]
  node [
    id 86
    label "barysfera"
  ]
  node [
    id 87
    label "biota"
  ]
  node [
    id 88
    label "p&#243;&#322;noc"
  ]
  node [
    id 89
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 90
    label "fauna"
  ]
  node [
    id 91
    label "wszechstworzenie"
  ]
  node [
    id 92
    label "geotermia"
  ]
  node [
    id 93
    label "biegun"
  ]
  node [
    id 94
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 95
    label "ekosystem"
  ]
  node [
    id 96
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 97
    label "teren"
  ]
  node [
    id 98
    label "zjawisko"
  ]
  node [
    id 99
    label "p&#243;&#322;kula"
  ]
  node [
    id 100
    label "atmosfera"
  ]
  node [
    id 101
    label "mikrokosmos"
  ]
  node [
    id 102
    label "class"
  ]
  node [
    id 103
    label "po&#322;udnie"
  ]
  node [
    id 104
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 105
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 106
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 107
    label "przejmowanie"
  ]
  node [
    id 108
    label "przestrze&#324;"
  ]
  node [
    id 109
    label "asymilowanie_si&#281;"
  ]
  node [
    id 110
    label "przej&#261;&#263;"
  ]
  node [
    id 111
    label "ekosfera"
  ]
  node [
    id 112
    label "przyroda"
  ]
  node [
    id 113
    label "ciemna_materia"
  ]
  node [
    id 114
    label "geoida"
  ]
  node [
    id 115
    label "Wsch&#243;d"
  ]
  node [
    id 116
    label "populace"
  ]
  node [
    id 117
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 118
    label "huczek"
  ]
  node [
    id 119
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 120
    label "Ziemia"
  ]
  node [
    id 121
    label "universe"
  ]
  node [
    id 122
    label "ozonosfera"
  ]
  node [
    id 123
    label "rze&#378;ba"
  ]
  node [
    id 124
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 125
    label "zagranica"
  ]
  node [
    id 126
    label "hydrosfera"
  ]
  node [
    id 127
    label "woda"
  ]
  node [
    id 128
    label "kuchnia"
  ]
  node [
    id 129
    label "przej&#281;cie"
  ]
  node [
    id 130
    label "czarna_dziura"
  ]
  node [
    id 131
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 132
    label "morze"
  ]
  node [
    id 133
    label "inny"
  ]
  node [
    id 134
    label "nast&#281;pnie"
  ]
  node [
    id 135
    label "kt&#243;ry&#347;"
  ]
  node [
    id 136
    label "kolejno"
  ]
  node [
    id 137
    label "nastopny"
  ]
  node [
    id 138
    label "zdobywca"
  ]
  node [
    id 139
    label "jasny"
  ]
  node [
    id 140
    label "miesi&#281;cznie"
  ]
  node [
    id 141
    label "trzydziestodniowy"
  ]
  node [
    id 142
    label "kilkudziesi&#281;ciodniowy"
  ]
  node [
    id 143
    label "czynno&#347;&#263;"
  ]
  node [
    id 144
    label "decyzja"
  ]
  node [
    id 145
    label "ewentualno&#347;&#263;"
  ]
  node [
    id 146
    label "pick"
  ]
  node [
    id 147
    label "eliminacje"
  ]
  node [
    id 148
    label "Interwizja"
  ]
  node [
    id 149
    label "emulation"
  ]
  node [
    id 150
    label "impreza"
  ]
  node [
    id 151
    label "casting"
  ]
  node [
    id 152
    label "Eurowizja"
  ]
  node [
    id 153
    label "nab&#243;r"
  ]
  node [
    id 154
    label "przekazior"
  ]
  node [
    id 155
    label "mass-media"
  ]
  node [
    id 156
    label "uzbrajanie"
  ]
  node [
    id 157
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 158
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 159
    label "medium"
  ]
  node [
    id 160
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 161
    label "szkolnie"
  ]
  node [
    id 162
    label "szkoleniowy"
  ]
  node [
    id 163
    label "podstawowy"
  ]
  node [
    id 164
    label "prosty"
  ]
  node [
    id 165
    label "trudno&#347;&#263;"
  ]
  node [
    id 166
    label "konstrukcja"
  ]
  node [
    id 167
    label "tynk"
  ]
  node [
    id 168
    label "belkowanie"
  ]
  node [
    id 169
    label "&#347;ciana"
  ]
  node [
    id 170
    label "fola"
  ]
  node [
    id 171
    label "futr&#243;wka"
  ]
  node [
    id 172
    label "rz&#261;d"
  ]
  node [
    id 173
    label "obstruction"
  ]
  node [
    id 174
    label "futbolista"
  ]
  node [
    id 175
    label "budowla"
  ]
  node [
    id 176
    label "gzyms"
  ]
  node [
    id 177
    label "ceg&#322;a"
  ]
  node [
    id 178
    label "mill"
  ]
  node [
    id 179
    label "urz&#261;dzenie_kuchenne"
  ]
  node [
    id 180
    label "obr&#243;t"
  ]
  node [
    id 181
    label "sprz&#281;t_AGD"
  ]
  node [
    id 182
    label "gra_planszowa"
  ]
  node [
    id 183
    label "opinion"
  ]
  node [
    id 184
    label "wypowied&#378;"
  ]
  node [
    id 185
    label "zmatowienie"
  ]
  node [
    id 186
    label "wpa&#347;&#263;"
  ]
  node [
    id 187
    label "wokal"
  ]
  node [
    id 188
    label "note"
  ]
  node [
    id 189
    label "wydawa&#263;"
  ]
  node [
    id 190
    label "nakaz"
  ]
  node [
    id 191
    label "regestr"
  ]
  node [
    id 192
    label "&#347;piewak_operowy"
  ]
  node [
    id 193
    label "matowie&#263;"
  ]
  node [
    id 194
    label "wpada&#263;"
  ]
  node [
    id 195
    label "stanowisko"
  ]
  node [
    id 196
    label "mutacja"
  ]
  node [
    id 197
    label "partia"
  ]
  node [
    id 198
    label "&#347;piewak"
  ]
  node [
    id 199
    label "emisja"
  ]
  node [
    id 200
    label "brzmienie"
  ]
  node [
    id 201
    label "zmatowie&#263;"
  ]
  node [
    id 202
    label "wydanie"
  ]
  node [
    id 203
    label "zesp&#243;&#322;"
  ]
  node [
    id 204
    label "wyda&#263;"
  ]
  node [
    id 205
    label "zdolno&#347;&#263;"
  ]
  node [
    id 206
    label "wpadni&#281;cie"
  ]
  node [
    id 207
    label "linia_melodyczna"
  ]
  node [
    id 208
    label "wpadanie"
  ]
  node [
    id 209
    label "onomatopeja"
  ]
  node [
    id 210
    label "sound"
  ]
  node [
    id 211
    label "matowienie"
  ]
  node [
    id 212
    label "ch&#243;rzysta"
  ]
  node [
    id 213
    label "d&#378;wi&#281;k"
  ]
  node [
    id 214
    label "foniatra"
  ]
  node [
    id 215
    label "&#347;piewaczka"
  ]
  node [
    id 216
    label "znak_interpunkcyjny"
  ]
  node [
    id 217
    label "znak_diakrytyczny"
  ]
  node [
    id 218
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 219
    label "potomstwo"
  ]
  node [
    id 220
    label "smarkateria"
  ]
  node [
    id 221
    label "pojazd_niemechaniczny"
  ]
  node [
    id 222
    label "erudyta"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 133
  ]
  edge [
    source 7
    target 134
  ]
  edge [
    source 7
    target 135
  ]
  edge [
    source 7
    target 136
  ]
  edge [
    source 7
    target 137
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 138
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 139
  ]
  edge [
    source 9
    target 140
  ]
  edge [
    source 9
    target 141
  ]
  edge [
    source 9
    target 142
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 143
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 144
  ]
  edge [
    source 10
    target 145
  ]
  edge [
    source 10
    target 146
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 147
  ]
  edge [
    source 11
    target 148
  ]
  edge [
    source 11
    target 149
  ]
  edge [
    source 11
    target 150
  ]
  edge [
    source 11
    target 151
  ]
  edge [
    source 11
    target 152
  ]
  edge [
    source 11
    target 153
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 154
  ]
  edge [
    source 13
    target 155
  ]
  edge [
    source 13
    target 156
  ]
  edge [
    source 13
    target 157
  ]
  edge [
    source 13
    target 158
  ]
  edge [
    source 13
    target 159
  ]
  edge [
    source 13
    target 160
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 161
  ]
  edge [
    source 14
    target 162
  ]
  edge [
    source 14
    target 163
  ]
  edge [
    source 14
    target 164
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 165
  ]
  edge [
    source 15
    target 166
  ]
  edge [
    source 15
    target 167
  ]
  edge [
    source 15
    target 168
  ]
  edge [
    source 15
    target 169
  ]
  edge [
    source 15
    target 170
  ]
  edge [
    source 15
    target 171
  ]
  edge [
    source 15
    target 172
  ]
  edge [
    source 15
    target 173
  ]
  edge [
    source 15
    target 174
  ]
  edge [
    source 15
    target 175
  ]
  edge [
    source 15
    target 176
  ]
  edge [
    source 15
    target 177
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 178
  ]
  edge [
    source 16
    target 179
  ]
  edge [
    source 16
    target 180
  ]
  edge [
    source 16
    target 181
  ]
  edge [
    source 16
    target 182
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 20
  ]
  edge [
    source 18
    target 21
  ]
  edge [
    source 18
    target 183
  ]
  edge [
    source 18
    target 184
  ]
  edge [
    source 18
    target 185
  ]
  edge [
    source 18
    target 186
  ]
  edge [
    source 18
    target 70
  ]
  edge [
    source 18
    target 187
  ]
  edge [
    source 18
    target 188
  ]
  edge [
    source 18
    target 189
  ]
  edge [
    source 18
    target 190
  ]
  edge [
    source 18
    target 191
  ]
  edge [
    source 18
    target 192
  ]
  edge [
    source 18
    target 193
  ]
  edge [
    source 18
    target 194
  ]
  edge [
    source 18
    target 195
  ]
  edge [
    source 18
    target 98
  ]
  edge [
    source 18
    target 196
  ]
  edge [
    source 18
    target 197
  ]
  edge [
    source 18
    target 198
  ]
  edge [
    source 18
    target 199
  ]
  edge [
    source 18
    target 200
  ]
  edge [
    source 18
    target 201
  ]
  edge [
    source 18
    target 202
  ]
  edge [
    source 18
    target 203
  ]
  edge [
    source 18
    target 204
  ]
  edge [
    source 18
    target 205
  ]
  edge [
    source 18
    target 144
  ]
  edge [
    source 18
    target 206
  ]
  edge [
    source 18
    target 207
  ]
  edge [
    source 18
    target 208
  ]
  edge [
    source 18
    target 209
  ]
  edge [
    source 18
    target 210
  ]
  edge [
    source 18
    target 211
  ]
  edge [
    source 18
    target 212
  ]
  edge [
    source 18
    target 213
  ]
  edge [
    source 18
    target 214
  ]
  edge [
    source 18
    target 215
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 216
  ]
  edge [
    source 20
    target 217
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 218
  ]
  edge [
    source 21
    target 219
  ]
  edge [
    source 21
    target 220
  ]
  edge [
    source 22
    target 221
  ]
  edge [
    source 22
    target 222
  ]
]
