graph [
  maxDegree 46
  minDegree 1
  meanDegree 2.1333333333333333
  density 0.004915514592933948
  graphCliqueNumber 3
  node [
    id 0
    label "usi&#261;&#347;&#263;"
    origin "text"
  ]
  node [
    id 1
    label "krzes&#322;o"
    origin "text"
  ]
  node [
    id 2
    label "wstawi&#263;"
    origin "text"
  ]
  node [
    id 3
    label "framuga"
    origin "text"
  ]
  node [
    id 4
    label "okno"
    origin "text"
  ]
  node [
    id 5
    label "oprze&#263;"
    origin "text"
  ]
  node [
    id 6
    label "&#322;okie&#263;"
    origin "text"
  ]
  node [
    id 7
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 8
    label "zsun&#261;&#263;"
    origin "text"
  ]
  node [
    id 9
    label "si&#281;"
    origin "text"
  ]
  node [
    id 10
    label "niebieski"
    origin "text"
  ]
  node [
    id 11
    label "flanelowy"
    origin "text"
  ]
  node [
    id 12
    label "matinka"
    origin "text"
  ]
  node [
    id 13
    label "patrze&#263;"
    origin "text"
  ]
  node [
    id 14
    label "ci&#261;gle"
    origin "text"
  ]
  node [
    id 15
    label "przestrze&#324;"
    origin "text"
  ]
  node [
    id 16
    label "teraz"
    origin "text"
  ]
  node [
    id 17
    label "liczy&#263;"
    origin "text"
  ]
  node [
    id 18
    label "lampa"
    origin "text"
  ]
  node [
    id 19
    label "r&#243;wna&#263;"
    origin "text"
  ]
  node [
    id 20
    label "si&#322;a"
    origin "text"
  ]
  node [
    id 21
    label "&#347;wiat&#322;a"
    origin "text"
  ]
  node [
    id 22
    label "&#347;wiat&#322;o"
    origin "text"
  ]
  node [
    id 23
    label "p&#322;on&#261;&#263;"
    origin "text"
  ]
  node [
    id 24
    label "ulica"
    origin "text"
  ]
  node [
    id 25
    label "warszawa"
    origin "text"
  ]
  node [
    id 26
    label "cofn&#261;&#263;"
    origin "text"
  ]
  node [
    id 27
    label "jakby"
    origin "text"
  ]
  node [
    id 28
    label "wstecz"
    origin "text"
  ]
  node [
    id 29
    label "g&#322;&#261;b"
    origin "text"
  ]
  node [
    id 30
    label "siebie"
    origin "text"
  ]
  node [
    id 31
    label "mimo"
    origin "text"
  ]
  node [
    id 32
    label "ch&#281;&#263;"
    origin "text"
  ]
  node [
    id 33
    label "wola"
    origin "text"
  ]
  node [
    id 34
    label "zacz&#261;&#263;"
    origin "text"
  ]
  node [
    id 35
    label "prze&#380;ywa&#263;"
    origin "text"
  ]
  node [
    id 36
    label "swoje"
    origin "text"
  ]
  node [
    id 37
    label "codzienny"
    origin "text"
  ]
  node [
    id 38
    label "zwyk&#322;y"
    origin "text"
  ]
  node [
    id 39
    label "&#380;ycie"
    origin "text"
  ]
  node [
    id 40
    label "tam"
    origin "text"
  ]
  node [
    id 41
    label "warecki"
    origin "text"
  ]
  node [
    id 42
    label "druga"
    origin "text"
  ]
  node [
    id 43
    label "pi&#281;tro"
    origin "text"
  ]
  node [
    id 44
    label "wn&#281;trze"
    origin "text"
  ]
  node [
    id 45
    label "niewielki"
    origin "text"
  ]
  node [
    id 46
    label "mieszkanie"
    origin "text"
  ]
  node [
    id 47
    label "t&#322;oczy&#263;"
    origin "text"
  ]
  node [
    id 48
    label "kilka"
    origin "text"
  ]
  node [
    id 49
    label "zawsze"
    origin "text"
  ]
  node [
    id 50
    label "skryty"
    origin "text"
  ]
  node [
    id 51
    label "nieufny"
    origin "text"
  ]
  node [
    id 52
    label "wszyscy"
    origin "text"
  ]
  node [
    id 53
    label "niedom&#243;wienie"
    origin "text"
  ]
  node [
    id 54
    label "domys&#322;"
    origin "text"
  ]
  node [
    id 55
    label "pogr&#261;&#380;y&#263;"
    origin "text"
  ]
  node [
    id 56
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 57
    label "zaj&#261;&#263;"
  ]
  node [
    id 58
    label "poruszy&#263;_si&#281;"
  ]
  node [
    id 59
    label "uplasowa&#263;_si&#281;"
  ]
  node [
    id 60
    label "przyj&#261;&#263;"
  ]
  node [
    id 61
    label "mount"
  ]
  node [
    id 62
    label "spocz&#261;&#263;"
  ]
  node [
    id 63
    label "wyl&#261;dowa&#263;"
  ]
  node [
    id 64
    label "zmieni&#263;"
  ]
  node [
    id 65
    label "przyst&#261;pi&#263;"
  ]
  node [
    id 66
    label "sie&#347;&#263;"
  ]
  node [
    id 67
    label "oparcie"
  ]
  node [
    id 68
    label "zaplecek"
  ]
  node [
    id 69
    label "siedzenie"
  ]
  node [
    id 70
    label "siedzisko"
  ]
  node [
    id 71
    label "&#322;awka"
  ]
  node [
    id 72
    label "krzes&#322;o_elektryczne"
  ]
  node [
    id 73
    label "por&#281;cz"
  ]
  node [
    id 74
    label "mebel"
  ]
  node [
    id 75
    label "noga"
  ]
  node [
    id 76
    label "insert"
  ]
  node [
    id 77
    label "plant"
  ]
  node [
    id 78
    label "umie&#347;ci&#263;"
  ]
  node [
    id 79
    label "rama"
  ]
  node [
    id 80
    label "drzwi"
  ]
  node [
    id 81
    label "frame"
  ]
  node [
    id 82
    label "ambrazura"
  ]
  node [
    id 83
    label "&#347;lemi&#281;"
  ]
  node [
    id 84
    label "wn&#281;ka"
  ]
  node [
    id 85
    label "pr&#243;g"
  ]
  node [
    id 86
    label "lufcik"
  ]
  node [
    id 87
    label "pasek_narz&#281;dzi"
  ]
  node [
    id 88
    label "parapet"
  ]
  node [
    id 89
    label "otw&#243;r"
  ]
  node [
    id 90
    label "skrzyd&#322;o"
  ]
  node [
    id 91
    label "kwatera_okienna"
  ]
  node [
    id 92
    label "szyba"
  ]
  node [
    id 93
    label "futryna"
  ]
  node [
    id 94
    label "nadokiennik"
  ]
  node [
    id 95
    label "interfejs"
  ]
  node [
    id 96
    label "inspekt"
  ]
  node [
    id 97
    label "program"
  ]
  node [
    id 98
    label "casement"
  ]
  node [
    id 99
    label "prze&#347;wit"
  ]
  node [
    id 100
    label "menad&#380;er_okien"
  ]
  node [
    id 101
    label "pulpit"
  ]
  node [
    id 102
    label "transenna"
  ]
  node [
    id 103
    label "nora"
  ]
  node [
    id 104
    label "okiennica"
  ]
  node [
    id 105
    label "establish"
  ]
  node [
    id 106
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 107
    label "recline"
  ]
  node [
    id 108
    label "podstawa"
  ]
  node [
    id 109
    label "ustawi&#263;"
  ]
  node [
    id 110
    label "osnowa&#263;"
  ]
  node [
    id 111
    label "zgi&#281;cie_&#322;okciowe"
  ]
  node [
    id 112
    label "r&#281;kaw"
  ]
  node [
    id 113
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 114
    label "d&#243;&#322;_&#322;okciowy"
  ]
  node [
    id 115
    label "miara"
  ]
  node [
    id 116
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 117
    label "listewka"
  ]
  node [
    id 118
    label "r&#281;ka"
  ]
  node [
    id 119
    label "z&#322;&#261;czy&#263;"
  ]
  node [
    id 120
    label "opu&#347;ci&#263;"
  ]
  node [
    id 121
    label "zdj&#261;&#263;"
  ]
  node [
    id 122
    label "ch&#322;odny"
  ]
  node [
    id 123
    label "siny"
  ]
  node [
    id 124
    label "niebiesko"
  ]
  node [
    id 125
    label "niebieszczenie"
  ]
  node [
    id 126
    label "bawe&#322;niany"
  ]
  node [
    id 127
    label "szlafrok"
  ]
  node [
    id 128
    label "koso"
  ]
  node [
    id 129
    label "szuka&#263;"
  ]
  node [
    id 130
    label "go_steady"
  ]
  node [
    id 131
    label "dba&#263;"
  ]
  node [
    id 132
    label "traktowa&#263;"
  ]
  node [
    id 133
    label "os&#261;dza&#263;"
  ]
  node [
    id 134
    label "punkt_widzenia"
  ]
  node [
    id 135
    label "robi&#263;"
  ]
  node [
    id 136
    label "uwa&#380;a&#263;"
  ]
  node [
    id 137
    label "look"
  ]
  node [
    id 138
    label "pogl&#261;da&#263;"
  ]
  node [
    id 139
    label "przygl&#261;da&#263;_si&#281;"
  ]
  node [
    id 140
    label "nieprzerwanie"
  ]
  node [
    id 141
    label "ci&#261;g&#322;y"
  ]
  node [
    id 142
    label "stale"
  ]
  node [
    id 143
    label "oktant"
  ]
  node [
    id 144
    label "niezmierzony"
  ]
  node [
    id 145
    label "miejsce"
  ]
  node [
    id 146
    label "bezbrze&#380;e"
  ]
  node [
    id 147
    label "przedzieli&#263;"
  ]
  node [
    id 148
    label "rozdzielanie"
  ]
  node [
    id 149
    label "rozdziela&#263;"
  ]
  node [
    id 150
    label "zbi&#243;r"
  ]
  node [
    id 151
    label "punkt"
  ]
  node [
    id 152
    label "przestw&#243;r"
  ]
  node [
    id 153
    label "przedzielenie"
  ]
  node [
    id 154
    label "nielito&#347;ciwy"
  ]
  node [
    id 155
    label "czasoprzestrze&#324;"
  ]
  node [
    id 156
    label "chwila"
  ]
  node [
    id 157
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 158
    label "report"
  ]
  node [
    id 159
    label "dodawa&#263;"
  ]
  node [
    id 160
    label "wymienia&#263;"
  ]
  node [
    id 161
    label "okre&#347;la&#263;"
  ]
  node [
    id 162
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 163
    label "dyskalkulia"
  ]
  node [
    id 164
    label "wynagrodzenie"
  ]
  node [
    id 165
    label "admit"
  ]
  node [
    id 166
    label "osi&#261;ga&#263;"
  ]
  node [
    id 167
    label "wyznacza&#263;"
  ]
  node [
    id 168
    label "posiada&#263;"
  ]
  node [
    id 169
    label "mierzy&#263;"
  ]
  node [
    id 170
    label "odlicza&#263;"
  ]
  node [
    id 171
    label "bra&#263;"
  ]
  node [
    id 172
    label "wycenia&#263;"
  ]
  node [
    id 173
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 174
    label "rachowa&#263;"
  ]
  node [
    id 175
    label "tell"
  ]
  node [
    id 176
    label "uwzgl&#281;dnia&#263;"
  ]
  node [
    id 177
    label "policza&#263;"
  ]
  node [
    id 178
    label "count"
  ]
  node [
    id 179
    label "iluminowa&#263;"
  ]
  node [
    id 180
    label "&#380;ar&#243;wka"
  ]
  node [
    id 181
    label "sztuczne_&#378;r&#243;d&#322;o_&#347;wiat&#322;a"
  ]
  node [
    id 182
    label "o&#347;wietla&#263;"
  ]
  node [
    id 183
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 184
    label "level"
  ]
  node [
    id 185
    label "wojsko"
  ]
  node [
    id 186
    label "magnitude"
  ]
  node [
    id 187
    label "energia"
  ]
  node [
    id 188
    label "capacity"
  ]
  node [
    id 189
    label "wuchta"
  ]
  node [
    id 190
    label "cecha"
  ]
  node [
    id 191
    label "parametr"
  ]
  node [
    id 192
    label "moment_si&#322;y"
  ]
  node [
    id 193
    label "przemoc"
  ]
  node [
    id 194
    label "zdolno&#347;&#263;"
  ]
  node [
    id 195
    label "mn&#243;stwo"
  ]
  node [
    id 196
    label "wektorowa_wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 197
    label "rozwi&#261;zanie"
  ]
  node [
    id 198
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 199
    label "potencja"
  ]
  node [
    id 200
    label "zjawisko"
  ]
  node [
    id 201
    label "zaleta"
  ]
  node [
    id 202
    label "sygnalizacja_&#347;wietlna"
  ]
  node [
    id 203
    label "skrzy&#380;owanie"
  ]
  node [
    id 204
    label "pasy"
  ]
  node [
    id 205
    label "&#347;wieci&#263;"
  ]
  node [
    id 206
    label "o&#347;wietlenie"
  ]
  node [
    id 207
    label "wpa&#347;&#263;"
  ]
  node [
    id 208
    label "&#347;wiat&#322;ocie&#324;"
  ]
  node [
    id 209
    label "sk&#322;ad_drukarski"
  ]
  node [
    id 210
    label "rozja&#347;nia&#263;"
  ]
  node [
    id 211
    label "obsadnik"
  ]
  node [
    id 212
    label "rzuca&#263;"
  ]
  node [
    id 213
    label "rzucenie"
  ]
  node [
    id 214
    label "promieniowanie_optyczne"
  ]
  node [
    id 215
    label "promieniowanie_elektromagnetyczne"
  ]
  node [
    id 216
    label "light"
  ]
  node [
    id 217
    label "wpada&#263;"
  ]
  node [
    id 218
    label "przy&#263;mienie"
  ]
  node [
    id 219
    label "odst&#281;p"
  ]
  node [
    id 220
    label "interpretacja"
  ]
  node [
    id 221
    label "przeb&#322;yskiwanie"
  ]
  node [
    id 222
    label "przy&#263;miewa&#263;"
  ]
  node [
    id 223
    label "licznik_energii_elektrycznej"
  ]
  node [
    id 224
    label "lighting"
  ]
  node [
    id 225
    label "rzuci&#263;"
  ]
  node [
    id 226
    label "plama"
  ]
  node [
    id 227
    label "radiance"
  ]
  node [
    id 228
    label "przy&#263;miewanie"
  ]
  node [
    id 229
    label "fotokataliza"
  ]
  node [
    id 230
    label "ja&#347;nia"
  ]
  node [
    id 231
    label "m&#261;drze"
  ]
  node [
    id 232
    label "rzucanie"
  ]
  node [
    id 233
    label "wpadni&#281;cie"
  ]
  node [
    id 234
    label "&#347;wiecenie"
  ]
  node [
    id 235
    label "&#347;rednica"
  ]
  node [
    id 236
    label "b&#322;ysk"
  ]
  node [
    id 237
    label "wpadanie"
  ]
  node [
    id 238
    label "promie&#324;"
  ]
  node [
    id 239
    label "instalacja"
  ]
  node [
    id 240
    label "przy&#263;mi&#263;"
  ]
  node [
    id 241
    label "lighter"
  ]
  node [
    id 242
    label "&#347;wiat&#322;y"
  ]
  node [
    id 243
    label "przeb&#322;yskiwa&#263;"
  ]
  node [
    id 244
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 245
    label "by&#263;"
  ]
  node [
    id 246
    label "zapala&#263;_si&#281;"
  ]
  node [
    id 247
    label "zapali&#263;_si&#281;"
  ]
  node [
    id 248
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 249
    label "czerwieni&#263;_si&#281;"
  ]
  node [
    id 250
    label "t&#281;tni&#263;"
  ]
  node [
    id 251
    label "niepokoi&#263;"
  ]
  node [
    id 252
    label "b&#322;yszcze&#263;"
  ]
  node [
    id 253
    label "sear"
  ]
  node [
    id 254
    label "burn"
  ]
  node [
    id 255
    label "pa&#322;a&#263;"
  ]
  node [
    id 256
    label "ulega&#263;"
  ]
  node [
    id 257
    label "gorze&#263;"
  ]
  node [
    id 258
    label "ogie&#324;"
  ]
  node [
    id 259
    label "niszczy&#263;_si&#281;"
  ]
  node [
    id 260
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 261
    label "&#347;rodowisko"
  ]
  node [
    id 262
    label "miasteczko"
  ]
  node [
    id 263
    label "streetball"
  ]
  node [
    id 264
    label "pierzeja"
  ]
  node [
    id 265
    label "grupa"
  ]
  node [
    id 266
    label "pas_ruchu"
  ]
  node [
    id 267
    label "jezdnia"
  ]
  node [
    id 268
    label "pas_rozdzielczy"
  ]
  node [
    id 269
    label "droga"
  ]
  node [
    id 270
    label "korona_drogi"
  ]
  node [
    id 271
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 272
    label "chodnik"
  ]
  node [
    id 273
    label "arteria"
  ]
  node [
    id 274
    label "Broadway"
  ]
  node [
    id 275
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 276
    label "wysepka"
  ]
  node [
    id 277
    label "autostrada"
  ]
  node [
    id 278
    label "Warszawa"
  ]
  node [
    id 279
    label "samoch&#243;d"
  ]
  node [
    id 280
    label "fastback"
  ]
  node [
    id 281
    label "wznowi&#263;"
  ]
  node [
    id 282
    label "spowodowa&#263;"
  ]
  node [
    id 283
    label "przywr&#243;ci&#263;"
  ]
  node [
    id 284
    label "retract"
  ]
  node [
    id 285
    label "oddali&#263;"
  ]
  node [
    id 286
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 287
    label "retreat"
  ]
  node [
    id 288
    label "recall"
  ]
  node [
    id 289
    label "przestawi&#263;"
  ]
  node [
    id 290
    label "uniewa&#380;ni&#263;"
  ]
  node [
    id 291
    label "przekierowa&#263;"
  ]
  node [
    id 292
    label "wa&#380;nie"
  ]
  node [
    id 293
    label "wsteczny"
  ]
  node [
    id 294
    label "t&#281;pak"
  ]
  node [
    id 295
    label "ignorant"
  ]
  node [
    id 296
    label "baran"
  ]
  node [
    id 297
    label "mlon"
  ]
  node [
    id 298
    label "&#322;odyga"
  ]
  node [
    id 299
    label "kapusta"
  ]
  node [
    id 300
    label "oskoma"
  ]
  node [
    id 301
    label "wytw&#243;r"
  ]
  node [
    id 302
    label "thinking"
  ]
  node [
    id 303
    label "emocja"
  ]
  node [
    id 304
    label "inclination"
  ]
  node [
    id 305
    label "zajawka"
  ]
  node [
    id 306
    label "wish"
  ]
  node [
    id 307
    label "mniemanie"
  ]
  node [
    id 308
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 309
    label "cause"
  ]
  node [
    id 310
    label "introduce"
  ]
  node [
    id 311
    label "begin"
  ]
  node [
    id 312
    label "odj&#261;&#263;"
  ]
  node [
    id 313
    label "post&#261;pi&#263;"
  ]
  node [
    id 314
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 315
    label "do"
  ]
  node [
    id 316
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 317
    label "zrobi&#263;"
  ]
  node [
    id 318
    label "trwa&#263;"
  ]
  node [
    id 319
    label "&#380;y&#263;"
  ]
  node [
    id 320
    label "wytrzymywa&#263;"
  ]
  node [
    id 321
    label "przechodzi&#263;"
  ]
  node [
    id 322
    label "experience"
  ]
  node [
    id 323
    label "doznawa&#263;"
  ]
  node [
    id 324
    label "cykliczny"
  ]
  node [
    id 325
    label "codziennie"
  ]
  node [
    id 326
    label "powszedny"
  ]
  node [
    id 327
    label "prozaiczny"
  ]
  node [
    id 328
    label "cz&#281;sty"
  ]
  node [
    id 329
    label "pospolity"
  ]
  node [
    id 330
    label "sta&#322;y"
  ]
  node [
    id 331
    label "regularny"
  ]
  node [
    id 332
    label "zwyczajnie"
  ]
  node [
    id 333
    label "okre&#347;lony"
  ]
  node [
    id 334
    label "zwykle"
  ]
  node [
    id 335
    label "przeci&#281;tny"
  ]
  node [
    id 336
    label "energy"
  ]
  node [
    id 337
    label "czas"
  ]
  node [
    id 338
    label "bycie"
  ]
  node [
    id 339
    label "zegar_biologiczny"
  ]
  node [
    id 340
    label "okres_noworodkowy"
  ]
  node [
    id 341
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 342
    label "entity"
  ]
  node [
    id 343
    label "prze&#380;ywanie"
  ]
  node [
    id 344
    label "prze&#380;ycie"
  ]
  node [
    id 345
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 346
    label "wiek_matuzalemowy"
  ]
  node [
    id 347
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 348
    label "dzieci&#324;stwo"
  ]
  node [
    id 349
    label "power"
  ]
  node [
    id 350
    label "szwung"
  ]
  node [
    id 351
    label "menopauza"
  ]
  node [
    id 352
    label "umarcie"
  ]
  node [
    id 353
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 354
    label "life"
  ]
  node [
    id 355
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 356
    label "&#380;ywy"
  ]
  node [
    id 357
    label "rozw&#243;j"
  ]
  node [
    id 358
    label "po&#322;&#243;g"
  ]
  node [
    id 359
    label "byt"
  ]
  node [
    id 360
    label "przebywanie"
  ]
  node [
    id 361
    label "subsistence"
  ]
  node [
    id 362
    label "koleje_losu"
  ]
  node [
    id 363
    label "raj_utracony"
  ]
  node [
    id 364
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 365
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 366
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 367
    label "andropauza"
  ]
  node [
    id 368
    label "warunki"
  ]
  node [
    id 369
    label "do&#380;ywanie"
  ]
  node [
    id 370
    label "niemowl&#281;ctwo"
  ]
  node [
    id 371
    label "umieranie"
  ]
  node [
    id 372
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 373
    label "staro&#347;&#263;"
  ]
  node [
    id 374
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 375
    label "&#347;mier&#263;"
  ]
  node [
    id 376
    label "tu"
  ]
  node [
    id 377
    label "godzina"
  ]
  node [
    id 378
    label "kondygnacja"
  ]
  node [
    id 379
    label "floor"
  ]
  node [
    id 380
    label "eta&#380;"
  ]
  node [
    id 381
    label "budynek"
  ]
  node [
    id 382
    label "jednostka_geologiczna"
  ]
  node [
    id 383
    label "chronozona"
  ]
  node [
    id 384
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 385
    label "oddzia&#322;"
  ]
  node [
    id 386
    label "p&#322;aszczyzna"
  ]
  node [
    id 387
    label "umeblowanie"
  ]
  node [
    id 388
    label "psychologia"
  ]
  node [
    id 389
    label "esteta"
  ]
  node [
    id 390
    label "osobowo&#347;&#263;"
  ]
  node [
    id 391
    label "umys&#322;"
  ]
  node [
    id 392
    label "pomieszczenie"
  ]
  node [
    id 393
    label "ma&#322;o"
  ]
  node [
    id 394
    label "nielicznie"
  ]
  node [
    id 395
    label "ma&#322;y"
  ]
  node [
    id 396
    label "niewa&#380;ny"
  ]
  node [
    id 397
    label "stanie"
  ]
  node [
    id 398
    label "panowanie"
  ]
  node [
    id 399
    label "zajmowanie"
  ]
  node [
    id 400
    label "pomieszkanie"
  ]
  node [
    id 401
    label "adjustment"
  ]
  node [
    id 402
    label "modu&#322;_mieszkalny"
  ]
  node [
    id 403
    label "lokal"
  ]
  node [
    id 404
    label "kwadrat"
  ]
  node [
    id 405
    label "animation"
  ]
  node [
    id 406
    label "dom"
  ]
  node [
    id 407
    label "throng"
  ]
  node [
    id 408
    label "produkowa&#263;"
  ]
  node [
    id 409
    label "stamp"
  ]
  node [
    id 410
    label "przemieszcza&#263;"
  ]
  node [
    id 411
    label "drukowa&#263;"
  ]
  node [
    id 412
    label "wyciska&#263;"
  ]
  node [
    id 413
    label "odciska&#263;"
  ]
  node [
    id 414
    label "&#347;ledziowate"
  ]
  node [
    id 415
    label "ryba"
  ]
  node [
    id 416
    label "zaw&#380;dy"
  ]
  node [
    id 417
    label "na_zawsze"
  ]
  node [
    id 418
    label "cz&#281;sto"
  ]
  node [
    id 419
    label "potajemny"
  ]
  node [
    id 420
    label "introwertyczny"
  ]
  node [
    id 421
    label "skrycie"
  ]
  node [
    id 422
    label "pow&#347;ci&#261;gliwy"
  ]
  node [
    id 423
    label "kryjomy"
  ]
  node [
    id 424
    label "nieufnie"
  ]
  node [
    id 425
    label "dziki"
  ]
  node [
    id 426
    label "monosylaba"
  ]
  node [
    id 427
    label "figura_my&#347;li"
  ]
  node [
    id 428
    label "niejasno&#347;&#263;"
  ]
  node [
    id 429
    label "poch&#322;on&#261;&#263;"
  ]
  node [
    id 430
    label "dip"
  ]
  node [
    id 431
    label "zanurzy&#263;"
  ]
  node [
    id 432
    label "zag&#322;ada"
  ]
  node [
    id 433
    label "wprowadzi&#263;"
  ]
  node [
    id 434
    label "doprowadzi&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 26
  ]
  edge [
    source 9
    target 27
  ]
  edge [
    source 9
    target 47
  ]
  edge [
    source 9
    target 48
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 10
    target 125
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 126
  ]
  edge [
    source 11
    target 30
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 128
  ]
  edge [
    source 13
    target 129
  ]
  edge [
    source 13
    target 130
  ]
  edge [
    source 13
    target 131
  ]
  edge [
    source 13
    target 132
  ]
  edge [
    source 13
    target 133
  ]
  edge [
    source 13
    target 134
  ]
  edge [
    source 13
    target 135
  ]
  edge [
    source 13
    target 136
  ]
  edge [
    source 13
    target 137
  ]
  edge [
    source 13
    target 138
  ]
  edge [
    source 13
    target 139
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 140
  ]
  edge [
    source 14
    target 141
  ]
  edge [
    source 14
    target 142
  ]
  edge [
    source 14
    target 49
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 143
  ]
  edge [
    source 15
    target 144
  ]
  edge [
    source 15
    target 145
  ]
  edge [
    source 15
    target 146
  ]
  edge [
    source 15
    target 147
  ]
  edge [
    source 15
    target 148
  ]
  edge [
    source 15
    target 149
  ]
  edge [
    source 15
    target 150
  ]
  edge [
    source 15
    target 151
  ]
  edge [
    source 15
    target 152
  ]
  edge [
    source 15
    target 153
  ]
  edge [
    source 15
    target 154
  ]
  edge [
    source 15
    target 155
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 156
  ]
  edge [
    source 16
    target 157
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 158
  ]
  edge [
    source 17
    target 159
  ]
  edge [
    source 17
    target 160
  ]
  edge [
    source 17
    target 161
  ]
  edge [
    source 17
    target 162
  ]
  edge [
    source 17
    target 163
  ]
  edge [
    source 17
    target 164
  ]
  edge [
    source 17
    target 165
  ]
  edge [
    source 17
    target 166
  ]
  edge [
    source 17
    target 167
  ]
  edge [
    source 17
    target 168
  ]
  edge [
    source 17
    target 169
  ]
  edge [
    source 17
    target 170
  ]
  edge [
    source 17
    target 171
  ]
  edge [
    source 17
    target 172
  ]
  edge [
    source 17
    target 173
  ]
  edge [
    source 17
    target 174
  ]
  edge [
    source 17
    target 175
  ]
  edge [
    source 17
    target 176
  ]
  edge [
    source 17
    target 177
  ]
  edge [
    source 17
    target 178
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 22
  ]
  edge [
    source 18
    target 23
  ]
  edge [
    source 18
    target 179
  ]
  edge [
    source 18
    target 180
  ]
  edge [
    source 18
    target 181
  ]
  edge [
    source 18
    target 182
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 183
  ]
  edge [
    source 19
    target 184
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 185
  ]
  edge [
    source 20
    target 186
  ]
  edge [
    source 20
    target 187
  ]
  edge [
    source 20
    target 188
  ]
  edge [
    source 20
    target 189
  ]
  edge [
    source 20
    target 190
  ]
  edge [
    source 20
    target 191
  ]
  edge [
    source 20
    target 192
  ]
  edge [
    source 20
    target 193
  ]
  edge [
    source 20
    target 194
  ]
  edge [
    source 20
    target 195
  ]
  edge [
    source 20
    target 196
  ]
  edge [
    source 20
    target 197
  ]
  edge [
    source 20
    target 198
  ]
  edge [
    source 20
    target 199
  ]
  edge [
    source 20
    target 200
  ]
  edge [
    source 20
    target 201
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 202
  ]
  edge [
    source 21
    target 145
  ]
  edge [
    source 21
    target 203
  ]
  edge [
    source 21
    target 204
  ]
  edge [
    source 21
    target 25
  ]
  edge [
    source 22
    target 205
  ]
  edge [
    source 22
    target 206
  ]
  edge [
    source 22
    target 207
  ]
  edge [
    source 22
    target 208
  ]
  edge [
    source 22
    target 209
  ]
  edge [
    source 22
    target 210
  ]
  edge [
    source 22
    target 134
  ]
  edge [
    source 22
    target 211
  ]
  edge [
    source 22
    target 212
  ]
  edge [
    source 22
    target 213
  ]
  edge [
    source 22
    target 214
  ]
  edge [
    source 22
    target 215
  ]
  edge [
    source 22
    target 216
  ]
  edge [
    source 22
    target 217
  ]
  edge [
    source 22
    target 218
  ]
  edge [
    source 22
    target 219
  ]
  edge [
    source 22
    target 200
  ]
  edge [
    source 22
    target 220
  ]
  edge [
    source 22
    target 221
  ]
  edge [
    source 22
    target 222
  ]
  edge [
    source 22
    target 223
  ]
  edge [
    source 22
    target 224
  ]
  edge [
    source 22
    target 225
  ]
  edge [
    source 22
    target 190
  ]
  edge [
    source 22
    target 226
  ]
  edge [
    source 22
    target 227
  ]
  edge [
    source 22
    target 228
  ]
  edge [
    source 22
    target 229
  ]
  edge [
    source 22
    target 230
  ]
  edge [
    source 22
    target 231
  ]
  edge [
    source 22
    target 232
  ]
  edge [
    source 22
    target 233
  ]
  edge [
    source 22
    target 234
  ]
  edge [
    source 22
    target 235
  ]
  edge [
    source 22
    target 187
  ]
  edge [
    source 22
    target 236
  ]
  edge [
    source 22
    target 237
  ]
  edge [
    source 22
    target 238
  ]
  edge [
    source 22
    target 239
  ]
  edge [
    source 22
    target 240
  ]
  edge [
    source 22
    target 241
  ]
  edge [
    source 22
    target 242
  ]
  edge [
    source 22
    target 243
  ]
  edge [
    source 22
    target 244
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 245
  ]
  edge [
    source 23
    target 246
  ]
  edge [
    source 23
    target 247
  ]
  edge [
    source 23
    target 248
  ]
  edge [
    source 23
    target 249
  ]
  edge [
    source 23
    target 250
  ]
  edge [
    source 23
    target 251
  ]
  edge [
    source 23
    target 252
  ]
  edge [
    source 23
    target 253
  ]
  edge [
    source 23
    target 254
  ]
  edge [
    source 23
    target 255
  ]
  edge [
    source 23
    target 256
  ]
  edge [
    source 23
    target 257
  ]
  edge [
    source 23
    target 258
  ]
  edge [
    source 23
    target 259
  ]
  edge [
    source 23
    target 260
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 261
  ]
  edge [
    source 24
    target 262
  ]
  edge [
    source 24
    target 263
  ]
  edge [
    source 24
    target 264
  ]
  edge [
    source 24
    target 265
  ]
  edge [
    source 24
    target 266
  ]
  edge [
    source 24
    target 267
  ]
  edge [
    source 24
    target 268
  ]
  edge [
    source 24
    target 269
  ]
  edge [
    source 24
    target 270
  ]
  edge [
    source 24
    target 271
  ]
  edge [
    source 24
    target 272
  ]
  edge [
    source 24
    target 273
  ]
  edge [
    source 24
    target 274
  ]
  edge [
    source 24
    target 275
  ]
  edge [
    source 24
    target 276
  ]
  edge [
    source 24
    target 277
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 278
  ]
  edge [
    source 25
    target 279
  ]
  edge [
    source 25
    target 280
  ]
  edge [
    source 26
    target 281
  ]
  edge [
    source 26
    target 58
  ]
  edge [
    source 26
    target 282
  ]
  edge [
    source 26
    target 283
  ]
  edge [
    source 26
    target 284
  ]
  edge [
    source 26
    target 285
  ]
  edge [
    source 26
    target 60
  ]
  edge [
    source 26
    target 286
  ]
  edge [
    source 26
    target 287
  ]
  edge [
    source 26
    target 288
  ]
  edge [
    source 26
    target 289
  ]
  edge [
    source 26
    target 290
  ]
  edge [
    source 26
    target 291
  ]
  edge [
    source 26
    target 64
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 29
  ]
  edge [
    source 27
    target 51
  ]
  edge [
    source 27
    target 52
  ]
  edge [
    source 28
    target 292
  ]
  edge [
    source 28
    target 293
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 294
  ]
  edge [
    source 29
    target 295
  ]
  edge [
    source 29
    target 296
  ]
  edge [
    source 29
    target 297
  ]
  edge [
    source 29
    target 298
  ]
  edge [
    source 29
    target 299
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 300
  ]
  edge [
    source 32
    target 301
  ]
  edge [
    source 32
    target 302
  ]
  edge [
    source 32
    target 303
  ]
  edge [
    source 32
    target 304
  ]
  edge [
    source 32
    target 305
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 300
  ]
  edge [
    source 33
    target 306
  ]
  edge [
    source 33
    target 303
  ]
  edge [
    source 33
    target 307
  ]
  edge [
    source 33
    target 308
  ]
  edge [
    source 33
    target 304
  ]
  edge [
    source 33
    target 305
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 309
  ]
  edge [
    source 34
    target 310
  ]
  edge [
    source 34
    target 311
  ]
  edge [
    source 34
    target 312
  ]
  edge [
    source 34
    target 313
  ]
  edge [
    source 34
    target 314
  ]
  edge [
    source 34
    target 315
  ]
  edge [
    source 34
    target 316
  ]
  edge [
    source 34
    target 317
  ]
  edge [
    source 34
    target 36
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 318
  ]
  edge [
    source 35
    target 319
  ]
  edge [
    source 35
    target 320
  ]
  edge [
    source 35
    target 321
  ]
  edge [
    source 35
    target 322
  ]
  edge [
    source 35
    target 323
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 324
  ]
  edge [
    source 37
    target 325
  ]
  edge [
    source 37
    target 326
  ]
  edge [
    source 37
    target 327
  ]
  edge [
    source 37
    target 328
  ]
  edge [
    source 37
    target 329
  ]
  edge [
    source 37
    target 330
  ]
  edge [
    source 37
    target 331
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 332
  ]
  edge [
    source 38
    target 333
  ]
  edge [
    source 38
    target 334
  ]
  edge [
    source 38
    target 335
  ]
  edge [
    source 38
    target 328
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 336
  ]
  edge [
    source 39
    target 337
  ]
  edge [
    source 39
    target 338
  ]
  edge [
    source 39
    target 339
  ]
  edge [
    source 39
    target 340
  ]
  edge [
    source 39
    target 341
  ]
  edge [
    source 39
    target 342
  ]
  edge [
    source 39
    target 343
  ]
  edge [
    source 39
    target 344
  ]
  edge [
    source 39
    target 345
  ]
  edge [
    source 39
    target 346
  ]
  edge [
    source 39
    target 347
  ]
  edge [
    source 39
    target 348
  ]
  edge [
    source 39
    target 349
  ]
  edge [
    source 39
    target 350
  ]
  edge [
    source 39
    target 351
  ]
  edge [
    source 39
    target 352
  ]
  edge [
    source 39
    target 353
  ]
  edge [
    source 39
    target 354
  ]
  edge [
    source 39
    target 355
  ]
  edge [
    source 39
    target 356
  ]
  edge [
    source 39
    target 357
  ]
  edge [
    source 39
    target 358
  ]
  edge [
    source 39
    target 359
  ]
  edge [
    source 39
    target 360
  ]
  edge [
    source 39
    target 361
  ]
  edge [
    source 39
    target 362
  ]
  edge [
    source 39
    target 363
  ]
  edge [
    source 39
    target 364
  ]
  edge [
    source 39
    target 365
  ]
  edge [
    source 39
    target 366
  ]
  edge [
    source 39
    target 367
  ]
  edge [
    source 39
    target 368
  ]
  edge [
    source 39
    target 369
  ]
  edge [
    source 39
    target 370
  ]
  edge [
    source 39
    target 371
  ]
  edge [
    source 39
    target 372
  ]
  edge [
    source 39
    target 373
  ]
  edge [
    source 39
    target 374
  ]
  edge [
    source 39
    target 375
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 376
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 377
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 378
  ]
  edge [
    source 43
    target 379
  ]
  edge [
    source 43
    target 380
  ]
  edge [
    source 43
    target 265
  ]
  edge [
    source 43
    target 381
  ]
  edge [
    source 43
    target 382
  ]
  edge [
    source 43
    target 383
  ]
  edge [
    source 43
    target 384
  ]
  edge [
    source 43
    target 385
  ]
  edge [
    source 43
    target 386
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 145
  ]
  edge [
    source 44
    target 387
  ]
  edge [
    source 44
    target 388
  ]
  edge [
    source 44
    target 190
  ]
  edge [
    source 44
    target 389
  ]
  edge [
    source 44
    target 150
  ]
  edge [
    source 44
    target 390
  ]
  edge [
    source 44
    target 391
  ]
  edge [
    source 44
    target 392
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 393
  ]
  edge [
    source 45
    target 394
  ]
  edge [
    source 45
    target 395
  ]
  edge [
    source 45
    target 396
  ]
  edge [
    source 46
    target 397
  ]
  edge [
    source 46
    target 360
  ]
  edge [
    source 46
    target 398
  ]
  edge [
    source 46
    target 399
  ]
  edge [
    source 46
    target 400
  ]
  edge [
    source 46
    target 401
  ]
  edge [
    source 46
    target 402
  ]
  edge [
    source 46
    target 403
  ]
  edge [
    source 46
    target 404
  ]
  edge [
    source 46
    target 405
  ]
  edge [
    source 46
    target 406
  ]
  edge [
    source 47
    target 407
  ]
  edge [
    source 47
    target 408
  ]
  edge [
    source 47
    target 409
  ]
  edge [
    source 47
    target 410
  ]
  edge [
    source 47
    target 411
  ]
  edge [
    source 47
    target 412
  ]
  edge [
    source 47
    target 183
  ]
  edge [
    source 47
    target 413
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 414
  ]
  edge [
    source 48
    target 415
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 416
  ]
  edge [
    source 49
    target 417
  ]
  edge [
    source 49
    target 418
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 419
  ]
  edge [
    source 50
    target 420
  ]
  edge [
    source 50
    target 421
  ]
  edge [
    source 50
    target 422
  ]
  edge [
    source 50
    target 423
  ]
  edge [
    source 51
    target 424
  ]
  edge [
    source 51
    target 425
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 426
  ]
  edge [
    source 53
    target 427
  ]
  edge [
    source 53
    target 428
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 301
  ]
  edge [
    source 55
    target 429
  ]
  edge [
    source 55
    target 430
  ]
  edge [
    source 55
    target 431
  ]
  edge [
    source 55
    target 432
  ]
  edge [
    source 55
    target 433
  ]
  edge [
    source 55
    target 434
  ]
]
