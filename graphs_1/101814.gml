graph [
  maxDegree 30
  minDegree 1
  meanDegree 2.0481927710843375
  density 0.008258841818888456
  graphCliqueNumber 6
  node [
    id 0
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 1
    label "rocznik"
    origin "text"
  ]
  node [
    id 2
    label "mok"
    origin "text"
  ]
  node [
    id 3
    label "ozorkowie"
    origin "text"
  ]
  node [
    id 4
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 5
    label "rozegrana"
    origin "text"
  ]
  node [
    id 6
    label "mistrzostwa"
    origin "text"
  ]
  node [
    id 7
    label "powiat"
    origin "text"
  ]
  node [
    id 8
    label "zgierski"
    origin "text"
  ]
  node [
    id 9
    label "bryd&#380;"
    origin "text"
  ]
  node [
    id 10
    label "sportowy"
    origin "text"
  ]
  node [
    id 11
    label "zg&#322;osi&#263;"
    origin "text"
  ]
  node [
    id 12
    label "si&#281;"
    origin "text"
  ]
  node [
    id 13
    label "par"
    origin "text"
  ]
  node [
    id 14
    label "puchar"
    origin "text"
  ]
  node [
    id 15
    label "tytu&#322;"
    origin "text"
  ]
  node [
    id 16
    label "mistrz"
    origin "text"
  ]
  node [
    id 17
    label "kategoria"
    origin "text"
  ]
  node [
    id 18
    label "open"
    origin "text"
  ]
  node [
    id 19
    label "zdoby&#263;"
    origin "text"
  ]
  node [
    id 20
    label "guthorn"
    origin "text"
  ]
  node [
    id 21
    label "hensel"
    origin "text"
  ]
  node [
    id 22
    label "sylwester"
    origin "text"
  ]
  node [
    id 23
    label "ka&#380;mierczak"
    origin "text"
  ]
  node [
    id 24
    label "dwa"
    origin "text"
  ]
  node [
    id 25
    label "kolejny"
    origin "text"
  ]
  node [
    id 26
    label "miejsce"
    origin "text"
  ]
  node [
    id 27
    label "podium"
    origin "text"
  ]
  node [
    id 28
    label "zaj&#261;&#263;"
    origin "text"
  ]
  node [
    id 29
    label "stanis&#322;aw"
    origin "text"
  ]
  node [
    id 30
    label "garczy&#324;ski"
    origin "text"
  ]
  node [
    id 31
    label "jacek"
    origin "text"
  ]
  node [
    id 32
    label "chmielecki"
    origin "text"
  ]
  node [
    id 33
    label "w&#322;odzimierz"
    origin "text"
  ]
  node [
    id 34
    label "choinkowski"
    origin "text"
  ]
  node [
    id 35
    label "grzegorz"
    origin "text"
  ]
  node [
    id 36
    label "b&#261;k"
    origin "text"
  ]
  node [
    id 37
    label "s&#322;o&#324;ce"
  ]
  node [
    id 38
    label "czynienie_si&#281;"
  ]
  node [
    id 39
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 40
    label "czas"
  ]
  node [
    id 41
    label "long_time"
  ]
  node [
    id 42
    label "przedpo&#322;udnie"
  ]
  node [
    id 43
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 44
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 45
    label "tydzie&#324;"
  ]
  node [
    id 46
    label "godzina"
  ]
  node [
    id 47
    label "t&#322;usty_czwartek"
  ]
  node [
    id 48
    label "wsta&#263;"
  ]
  node [
    id 49
    label "day"
  ]
  node [
    id 50
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 51
    label "przedwiecz&#243;r"
  ]
  node [
    id 52
    label "Sylwester"
  ]
  node [
    id 53
    label "po&#322;udnie"
  ]
  node [
    id 54
    label "wzej&#347;cie"
  ]
  node [
    id 55
    label "podwiecz&#243;r"
  ]
  node [
    id 56
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 57
    label "rano"
  ]
  node [
    id 58
    label "termin"
  ]
  node [
    id 59
    label "ranek"
  ]
  node [
    id 60
    label "doba"
  ]
  node [
    id 61
    label "wiecz&#243;r"
  ]
  node [
    id 62
    label "walentynki"
  ]
  node [
    id 63
    label "popo&#322;udnie"
  ]
  node [
    id 64
    label "noc"
  ]
  node [
    id 65
    label "wstanie"
  ]
  node [
    id 66
    label "formacja"
  ]
  node [
    id 67
    label "kronika"
  ]
  node [
    id 68
    label "czasopismo"
  ]
  node [
    id 69
    label "yearbook"
  ]
  node [
    id 70
    label "proceed"
  ]
  node [
    id 71
    label "catch"
  ]
  node [
    id 72
    label "pozosta&#263;"
  ]
  node [
    id 73
    label "osta&#263;_si&#281;"
  ]
  node [
    id 74
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 75
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 76
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 77
    label "change"
  ]
  node [
    id 78
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 79
    label "championship"
  ]
  node [
    id 80
    label "Formu&#322;a_1"
  ]
  node [
    id 81
    label "zawody"
  ]
  node [
    id 82
    label "gmina"
  ]
  node [
    id 83
    label "jednostka_administracyjna"
  ]
  node [
    id 84
    label "wojew&#243;dztwo"
  ]
  node [
    id 85
    label "sport"
  ]
  node [
    id 86
    label "longer"
  ]
  node [
    id 87
    label "odzywa&#263;_si&#281;"
  ]
  node [
    id 88
    label "odwrotka"
  ]
  node [
    id 89
    label "kontrakt"
  ]
  node [
    id 90
    label "licytacja"
  ]
  node [
    id 91
    label "odzywanie_si&#281;"
  ]
  node [
    id 92
    label "odezwanie_si&#281;"
  ]
  node [
    id 93
    label "rober"
  ]
  node [
    id 94
    label "odezwa&#263;_si&#281;"
  ]
  node [
    id 95
    label "korona"
  ]
  node [
    id 96
    label "inwit"
  ]
  node [
    id 97
    label "gra_w_karty"
  ]
  node [
    id 98
    label "rekontra"
  ]
  node [
    id 99
    label "specjalny"
  ]
  node [
    id 100
    label "na_sportowo"
  ]
  node [
    id 101
    label "uczciwy"
  ]
  node [
    id 102
    label "wygodny"
  ]
  node [
    id 103
    label "pe&#322;ny"
  ]
  node [
    id 104
    label "sportowo"
  ]
  node [
    id 105
    label "report"
  ]
  node [
    id 106
    label "announce"
  ]
  node [
    id 107
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 108
    label "write"
  ]
  node [
    id 109
    label "poinformowa&#263;"
  ]
  node [
    id 110
    label "Izba_Par&#243;w"
  ]
  node [
    id 111
    label "lord"
  ]
  node [
    id 112
    label "Izba_Lord&#243;w"
  ]
  node [
    id 113
    label "parlamentarzysta"
  ]
  node [
    id 114
    label "lennik"
  ]
  node [
    id 115
    label "zawarto&#347;&#263;"
  ]
  node [
    id 116
    label "zwyci&#281;stwo"
  ]
  node [
    id 117
    label "naczynie"
  ]
  node [
    id 118
    label "nagroda"
  ]
  node [
    id 119
    label "podtytu&#322;"
  ]
  node [
    id 120
    label "debit"
  ]
  node [
    id 121
    label "szata_graficzna"
  ]
  node [
    id 122
    label "elevation"
  ]
  node [
    id 123
    label "wyda&#263;"
  ]
  node [
    id 124
    label "nadtytu&#322;"
  ]
  node [
    id 125
    label "tytulatura"
  ]
  node [
    id 126
    label "nazwa"
  ]
  node [
    id 127
    label "wydawa&#263;"
  ]
  node [
    id 128
    label "redaktor"
  ]
  node [
    id 129
    label "druk"
  ]
  node [
    id 130
    label "mianowaniec"
  ]
  node [
    id 131
    label "poster"
  ]
  node [
    id 132
    label "publikacja"
  ]
  node [
    id 133
    label "miszczu"
  ]
  node [
    id 134
    label "rzemie&#347;lnik"
  ]
  node [
    id 135
    label "znakomito&#347;&#263;"
  ]
  node [
    id 136
    label "doradca"
  ]
  node [
    id 137
    label "werkmistrz"
  ]
  node [
    id 138
    label "majstersztyk"
  ]
  node [
    id 139
    label "zwyci&#281;zca"
  ]
  node [
    id 140
    label "agent"
  ]
  node [
    id 141
    label "kozak"
  ]
  node [
    id 142
    label "autorytet"
  ]
  node [
    id 143
    label "zwierzchnik"
  ]
  node [
    id 144
    label "Towia&#324;ski"
  ]
  node [
    id 145
    label "forma"
  ]
  node [
    id 146
    label "wytw&#243;r"
  ]
  node [
    id 147
    label "type"
  ]
  node [
    id 148
    label "teoria"
  ]
  node [
    id 149
    label "zbi&#243;r"
  ]
  node [
    id 150
    label "poj&#281;cie"
  ]
  node [
    id 151
    label "klasa"
  ]
  node [
    id 152
    label "uzyska&#263;"
  ]
  node [
    id 153
    label "stage"
  ]
  node [
    id 154
    label "dosta&#263;"
  ]
  node [
    id 155
    label "manipulate"
  ]
  node [
    id 156
    label "realize"
  ]
  node [
    id 157
    label "podporz&#261;dkowa&#263;"
  ]
  node [
    id 158
    label "impreza"
  ]
  node [
    id 159
    label "inny"
  ]
  node [
    id 160
    label "nast&#281;pnie"
  ]
  node [
    id 161
    label "kt&#243;ry&#347;"
  ]
  node [
    id 162
    label "kolejno"
  ]
  node [
    id 163
    label "nastopny"
  ]
  node [
    id 164
    label "cia&#322;o"
  ]
  node [
    id 165
    label "plac"
  ]
  node [
    id 166
    label "cecha"
  ]
  node [
    id 167
    label "uwaga"
  ]
  node [
    id 168
    label "przestrze&#324;"
  ]
  node [
    id 169
    label "status"
  ]
  node [
    id 170
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 171
    label "chwila"
  ]
  node [
    id 172
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 173
    label "rz&#261;d"
  ]
  node [
    id 174
    label "praca"
  ]
  node [
    id 175
    label "location"
  ]
  node [
    id 176
    label "warunek_lokalowy"
  ]
  node [
    id 177
    label "pud&#322;o"
  ]
  node [
    id 178
    label "podwy&#380;szenie"
  ]
  node [
    id 179
    label "sukces"
  ]
  node [
    id 180
    label "dostarczy&#263;"
  ]
  node [
    id 181
    label "wype&#322;ni&#263;"
  ]
  node [
    id 182
    label "anektowa&#263;"
  ]
  node [
    id 183
    label "rozprzestrzeni&#263;_si&#281;"
  ]
  node [
    id 184
    label "obj&#261;&#263;"
  ]
  node [
    id 185
    label "zada&#263;"
  ]
  node [
    id 186
    label "sorb"
  ]
  node [
    id 187
    label "interest"
  ]
  node [
    id 188
    label "skorzysta&#263;"
  ]
  node [
    id 189
    label "usytuowa&#263;_si&#281;"
  ]
  node [
    id 190
    label "wzi&#261;&#263;"
  ]
  node [
    id 191
    label "employment"
  ]
  node [
    id 192
    label "zapanowa&#263;"
  ]
  node [
    id 193
    label "do"
  ]
  node [
    id 194
    label "klasyfikacja"
  ]
  node [
    id 195
    label "bankrupt"
  ]
  node [
    id 196
    label "zabra&#263;"
  ]
  node [
    id 197
    label "spowodowa&#263;"
  ]
  node [
    id 198
    label "ulokowa&#263;_si&#281;"
  ]
  node [
    id 199
    label "komornik"
  ]
  node [
    id 200
    label "prosecute"
  ]
  node [
    id 201
    label "seize"
  ]
  node [
    id 202
    label "topographic_point"
  ]
  node [
    id 203
    label "wzbudzi&#263;"
  ]
  node [
    id 204
    label "rozciekawi&#263;"
  ]
  node [
    id 205
    label "trzmiele_i_trzmielce"
  ]
  node [
    id 206
    label "ptak_wodno-b&#322;otny"
  ]
  node [
    id 207
    label "zabawka"
  ]
  node [
    id 208
    label "pierd"
  ]
  node [
    id 209
    label "bittern"
  ]
  node [
    id 210
    label "dziecko"
  ]
  node [
    id 211
    label "b&#261;kanie"
  ]
  node [
    id 212
    label "wydalina"
  ]
  node [
    id 213
    label "&#380;yroskop"
  ]
  node [
    id 214
    label "bry&#322;a_sztywna"
  ]
  node [
    id 215
    label "&#322;&#243;d&#378;_wios&#322;owa"
  ]
  node [
    id 216
    label "ptak_w&#281;drowny"
  ]
  node [
    id 217
    label "much&#243;wka"
  ]
  node [
    id 218
    label "czaplowate"
  ]
  node [
    id 219
    label "knot"
  ]
  node [
    id 220
    label "b&#261;kowate"
  ]
  node [
    id 221
    label "&#380;&#261;d&#322;&#243;wka"
  ]
  node [
    id 222
    label "b&#261;kni&#281;cie"
  ]
  node [
    id 223
    label "mistrzostwo"
  ]
  node [
    id 224
    label "wyspa"
  ]
  node [
    id 225
    label "Guthorn"
  ]
  node [
    id 226
    label "Hensel"
  ]
  node [
    id 227
    label "Ka&#380;mierczak"
  ]
  node [
    id 228
    label "Stanis&#322;awa"
  ]
  node [
    id 229
    label "Garczy&#324;ski"
  ]
  node [
    id 230
    label "W&#322;odzimierz"
  ]
  node [
    id 231
    label "Choinkowski"
  ]
  node [
    id 232
    label "Grzegorz"
  ]
  node [
    id 233
    label "Jacek"
  ]
  node [
    id 234
    label "Chmielecki"
  ]
  node [
    id 235
    label "&#321;ukasz"
  ]
  node [
    id 236
    label "kosma"
  ]
  node [
    id 237
    label "Mateusz"
  ]
  node [
    id 238
    label "Sobczak"
  ]
  node [
    id 239
    label "Adam"
  ]
  node [
    id 240
    label "walczak"
  ]
  node [
    id 241
    label "Marcin"
  ]
  node [
    id 242
    label "bojarski"
  ]
  node [
    id 243
    label "Maciej"
  ]
  node [
    id 244
    label "J&#243;zefa"
  ]
  node [
    id 245
    label "Cie&#347;lak"
  ]
  node [
    id 246
    label "Jaros&#322;awa"
  ]
  node [
    id 247
    label "Przemys&#322;awa"
  ]
  node [
    id 248
    label "Sawicki"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 16
  ]
  edge [
    source 7
    target 17
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 223
  ]
  edge [
    source 7
    target 224
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 7
    target 10
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 20
  ]
  edge [
    source 8
    target 223
  ]
  edge [
    source 8
    target 224
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 9
    target 223
  ]
  edge [
    source 9
    target 224
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 10
    target 223
  ]
  edge [
    source 10
    target 224
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 108
  ]
  edge [
    source 11
    target 109
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 19
  ]
  edge [
    source 13
    target 20
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 13
    target 113
  ]
  edge [
    source 13
    target 114
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 115
  ]
  edge [
    source 14
    target 81
  ]
  edge [
    source 14
    target 116
  ]
  edge [
    source 14
    target 117
  ]
  edge [
    source 14
    target 118
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 119
  ]
  edge [
    source 15
    target 120
  ]
  edge [
    source 15
    target 121
  ]
  edge [
    source 15
    target 122
  ]
  edge [
    source 15
    target 123
  ]
  edge [
    source 15
    target 124
  ]
  edge [
    source 15
    target 125
  ]
  edge [
    source 15
    target 126
  ]
  edge [
    source 15
    target 127
  ]
  edge [
    source 15
    target 128
  ]
  edge [
    source 15
    target 129
  ]
  edge [
    source 15
    target 130
  ]
  edge [
    source 15
    target 131
  ]
  edge [
    source 15
    target 132
  ]
  edge [
    source 16
    target 133
  ]
  edge [
    source 16
    target 134
  ]
  edge [
    source 16
    target 135
  ]
  edge [
    source 16
    target 136
  ]
  edge [
    source 16
    target 137
  ]
  edge [
    source 16
    target 138
  ]
  edge [
    source 16
    target 139
  ]
  edge [
    source 16
    target 140
  ]
  edge [
    source 16
    target 141
  ]
  edge [
    source 16
    target 142
  ]
  edge [
    source 16
    target 143
  ]
  edge [
    source 16
    target 144
  ]
  edge [
    source 16
    target 25
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 145
  ]
  edge [
    source 17
    target 146
  ]
  edge [
    source 17
    target 147
  ]
  edge [
    source 17
    target 148
  ]
  edge [
    source 17
    target 149
  ]
  edge [
    source 17
    target 150
  ]
  edge [
    source 17
    target 151
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 152
  ]
  edge [
    source 19
    target 153
  ]
  edge [
    source 19
    target 154
  ]
  edge [
    source 19
    target 155
  ]
  edge [
    source 19
    target 156
  ]
  edge [
    source 19
    target 157
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 158
  ]
  edge [
    source 22
    target 227
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 159
  ]
  edge [
    source 25
    target 160
  ]
  edge [
    source 25
    target 161
  ]
  edge [
    source 25
    target 162
  ]
  edge [
    source 25
    target 163
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 164
  ]
  edge [
    source 26
    target 165
  ]
  edge [
    source 26
    target 166
  ]
  edge [
    source 26
    target 167
  ]
  edge [
    source 26
    target 168
  ]
  edge [
    source 26
    target 169
  ]
  edge [
    source 26
    target 170
  ]
  edge [
    source 26
    target 171
  ]
  edge [
    source 26
    target 172
  ]
  edge [
    source 26
    target 173
  ]
  edge [
    source 26
    target 174
  ]
  edge [
    source 26
    target 175
  ]
  edge [
    source 26
    target 176
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 177
  ]
  edge [
    source 27
    target 178
  ]
  edge [
    source 27
    target 179
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 180
  ]
  edge [
    source 28
    target 181
  ]
  edge [
    source 28
    target 182
  ]
  edge [
    source 28
    target 183
  ]
  edge [
    source 28
    target 184
  ]
  edge [
    source 28
    target 185
  ]
  edge [
    source 28
    target 186
  ]
  edge [
    source 28
    target 187
  ]
  edge [
    source 28
    target 188
  ]
  edge [
    source 28
    target 189
  ]
  edge [
    source 28
    target 190
  ]
  edge [
    source 28
    target 191
  ]
  edge [
    source 28
    target 192
  ]
  edge [
    source 28
    target 193
  ]
  edge [
    source 28
    target 194
  ]
  edge [
    source 28
    target 76
  ]
  edge [
    source 28
    target 195
  ]
  edge [
    source 28
    target 196
  ]
  edge [
    source 28
    target 197
  ]
  edge [
    source 28
    target 198
  ]
  edge [
    source 28
    target 199
  ]
  edge [
    source 28
    target 200
  ]
  edge [
    source 28
    target 201
  ]
  edge [
    source 28
    target 202
  ]
  edge [
    source 28
    target 203
  ]
  edge [
    source 28
    target 204
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 36
    target 205
  ]
  edge [
    source 36
    target 206
  ]
  edge [
    source 36
    target 207
  ]
  edge [
    source 36
    target 208
  ]
  edge [
    source 36
    target 209
  ]
  edge [
    source 36
    target 210
  ]
  edge [
    source 36
    target 211
  ]
  edge [
    source 36
    target 212
  ]
  edge [
    source 36
    target 213
  ]
  edge [
    source 36
    target 214
  ]
  edge [
    source 36
    target 215
  ]
  edge [
    source 36
    target 216
  ]
  edge [
    source 36
    target 217
  ]
  edge [
    source 36
    target 218
  ]
  edge [
    source 36
    target 219
  ]
  edge [
    source 36
    target 220
  ]
  edge [
    source 36
    target 221
  ]
  edge [
    source 36
    target 222
  ]
  edge [
    source 36
    target 232
  ]
  edge [
    source 223
    target 224
  ]
  edge [
    source 225
    target 226
  ]
  edge [
    source 228
    target 229
  ]
  edge [
    source 230
    target 231
  ]
  edge [
    source 233
    target 234
  ]
  edge [
    source 235
    target 236
  ]
  edge [
    source 235
    target 238
  ]
  edge [
    source 237
    target 238
  ]
  edge [
    source 239
    target 240
  ]
  edge [
    source 241
    target 242
  ]
  edge [
    source 242
    target 243
  ]
  edge [
    source 244
    target 245
  ]
  edge [
    source 245
    target 246
  ]
  edge [
    source 247
    target 248
  ]
]
