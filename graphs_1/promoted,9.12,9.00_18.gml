graph [
  maxDegree 20
  minDegree 1
  meanDegree 2
  density 0.028985507246376812
  graphCliqueNumber 2
  node [
    id 0
    label "ninja"
    origin "text"
  ]
  node [
    id 1
    label "styl"
    origin "text"
  ]
  node [
    id 2
    label "cygan"
    origin "text"
  ]
  node [
    id 3
    label "kra&#347;&#263;"
    origin "text"
  ]
  node [
    id 4
    label "iphone'a"
    origin "text"
  ]
  node [
    id 5
    label "dziewczyna"
    origin "text"
  ]
  node [
    id 6
    label "metr"
    origin "text"
  ]
  node [
    id 7
    label "wojownik"
  ]
  node [
    id 8
    label "morderca"
  ]
  node [
    id 9
    label "pisa&#263;"
  ]
  node [
    id 10
    label "reakcja"
  ]
  node [
    id 11
    label "zachowanie"
  ]
  node [
    id 12
    label "napisa&#263;"
  ]
  node [
    id 13
    label "natural_language"
  ]
  node [
    id 14
    label "charakter"
  ]
  node [
    id 15
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 16
    label "behawior"
  ]
  node [
    id 17
    label "line"
  ]
  node [
    id 18
    label "zbi&#243;r"
  ]
  node [
    id 19
    label "stroke"
  ]
  node [
    id 20
    label "stylik"
  ]
  node [
    id 21
    label "narz&#281;dzie"
  ]
  node [
    id 22
    label "dyscyplina_sportowa"
  ]
  node [
    id 23
    label "kanon"
  ]
  node [
    id 24
    label "spos&#243;b"
  ]
  node [
    id 25
    label "trzonek"
  ]
  node [
    id 26
    label "handle"
  ]
  node [
    id 27
    label "cz&#322;owiek"
  ]
  node [
    id 28
    label "ward&#281;ga"
  ]
  node [
    id 29
    label "istota_&#380;ywa"
  ]
  node [
    id 30
    label "oszust"
  ]
  node [
    id 31
    label "chachar"
  ]
  node [
    id 32
    label "mie&#263;_lepkie_r&#281;ce"
  ]
  node [
    id 33
    label "r&#261;ba&#263;"
  ]
  node [
    id 34
    label "podsuwa&#263;"
  ]
  node [
    id 35
    label "przyw&#322;aszcza&#263;"
  ]
  node [
    id 36
    label "robi&#263;"
  ]
  node [
    id 37
    label "zar&#261;bywa&#263;"
  ]
  node [
    id 38
    label "overcharge"
  ]
  node [
    id 39
    label "podpierdala&#263;"
  ]
  node [
    id 40
    label "dziewka"
  ]
  node [
    id 41
    label "dziewoja"
  ]
  node [
    id 42
    label "dziunia"
  ]
  node [
    id 43
    label "partnerka"
  ]
  node [
    id 44
    label "dziewczynina"
  ]
  node [
    id 45
    label "siksa"
  ]
  node [
    id 46
    label "sympatia"
  ]
  node [
    id 47
    label "dziewcz&#281;"
  ]
  node [
    id 48
    label "s&#322;u&#380;&#261;ca"
  ]
  node [
    id 49
    label "kora"
  ]
  node [
    id 50
    label "m&#322;&#243;dka"
  ]
  node [
    id 51
    label "dziecina"
  ]
  node [
    id 52
    label "sikorka"
  ]
  node [
    id 53
    label "meter"
  ]
  node [
    id 54
    label "decymetr"
  ]
  node [
    id 55
    label "megabyte"
  ]
  node [
    id 56
    label "plon"
  ]
  node [
    id 57
    label "metrum"
  ]
  node [
    id 58
    label "dekametr"
  ]
  node [
    id 59
    label "jednostka_powierzchni"
  ]
  node [
    id 60
    label "uk&#322;ad_SI"
  ]
  node [
    id 61
    label "literaturoznawstwo"
  ]
  node [
    id 62
    label "wiersz"
  ]
  node [
    id 63
    label "gigametr"
  ]
  node [
    id 64
    label "miara"
  ]
  node [
    id 65
    label "nauczyciel"
  ]
  node [
    id 66
    label "kilometr_kwadratowy"
  ]
  node [
    id 67
    label "jednostka_metryczna"
  ]
  node [
    id 68
    label "jednostka_masy"
  ]
  node [
    id 69
    label "centymetr_kwadratowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
]
