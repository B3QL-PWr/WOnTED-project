graph [
  maxDegree 29
  minDegree 1
  meanDegree 2.050632911392405
  density 0.013061356123518504
  graphCliqueNumber 3
  node [
    id 0
    label "totolotek"
    origin "text"
  ]
  node [
    id 1
    label "polska"
    origin "text"
  ]
  node [
    id 2
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 3
    label "g&#243;wno"
    origin "text"
  ]
  node [
    id 4
    label "histori"
    origin "text"
  ]
  node [
    id 5
    label "polski"
    origin "text"
  ]
  node [
    id 6
    label "bukmacherka"
    origin "text"
  ]
  node [
    id 7
    label "wchodzi&#263;"
    origin "text"
  ]
  node [
    id 8
    label "kupon"
    origin "text"
  ]
  node [
    id 9
    label "wygrana"
    origin "text"
  ]
  node [
    id 10
    label "stwierdza&#263;"
    origin "text"
  ]
  node [
    id 11
    label "byl"
    origin "text"
  ]
  node [
    id 12
    label "b&#322;&#261;d"
    origin "text"
  ]
  node [
    id 13
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 14
    label "zawrze&#263;"
    origin "text"
  ]
  node [
    id 15
    label "gdzie"
    origin "text"
  ]
  node [
    id 16
    label "by&#263;"
    origin "text"
  ]
  node [
    id 17
    label "potwierdzenie"
    origin "text"
  ]
  node [
    id 18
    label "zaakceptowa&#263;"
    origin "text"
  ]
  node [
    id 19
    label "totalizator"
  ]
  node [
    id 20
    label "pi&#261;tka"
  ]
  node [
    id 21
    label "tr&#243;jka"
  ]
  node [
    id 22
    label "czw&#243;rka"
  ]
  node [
    id 23
    label "sz&#243;stka"
  ]
  node [
    id 24
    label "doros&#322;y"
  ]
  node [
    id 25
    label "wiele"
  ]
  node [
    id 26
    label "dorodny"
  ]
  node [
    id 27
    label "znaczny"
  ]
  node [
    id 28
    label "du&#380;o"
  ]
  node [
    id 29
    label "prawdziwy"
  ]
  node [
    id 30
    label "niema&#322;o"
  ]
  node [
    id 31
    label "wa&#380;ny"
  ]
  node [
    id 32
    label "rozwini&#281;ty"
  ]
  node [
    id 33
    label "ka&#322;"
  ]
  node [
    id 34
    label "zero"
  ]
  node [
    id 35
    label "drobiazg"
  ]
  node [
    id 36
    label "tandeta"
  ]
  node [
    id 37
    label "lacki"
  ]
  node [
    id 38
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 39
    label "przedmiot"
  ]
  node [
    id 40
    label "sztajer"
  ]
  node [
    id 41
    label "drabant"
  ]
  node [
    id 42
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 43
    label "polak"
  ]
  node [
    id 44
    label "pierogi_ruskie"
  ]
  node [
    id 45
    label "krakowiak"
  ]
  node [
    id 46
    label "Polish"
  ]
  node [
    id 47
    label "j&#281;zyk"
  ]
  node [
    id 48
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 49
    label "oberek"
  ]
  node [
    id 50
    label "po_polsku"
  ]
  node [
    id 51
    label "mazur"
  ]
  node [
    id 52
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 53
    label "chodzony"
  ]
  node [
    id 54
    label "skoczny"
  ]
  node [
    id 55
    label "ryba_po_grecku"
  ]
  node [
    id 56
    label "goniony"
  ]
  node [
    id 57
    label "polsko"
  ]
  node [
    id 58
    label "zak&#322;ad_wzajemny"
  ]
  node [
    id 59
    label "po&#347;rednictwo"
  ]
  node [
    id 60
    label "w&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 61
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 62
    label "przenika&#263;"
  ]
  node [
    id 63
    label "przekracza&#263;"
  ]
  node [
    id 64
    label "nast&#281;powa&#263;"
  ]
  node [
    id 65
    label "dochodzi&#263;"
  ]
  node [
    id 66
    label "zag&#322;&#281;bia&#263;_si&#281;"
  ]
  node [
    id 67
    label "intervene"
  ]
  node [
    id 68
    label "scale"
  ]
  node [
    id 69
    label "drapa&#263;_si&#281;"
  ]
  node [
    id 70
    label "&#322;oi&#263;"
  ]
  node [
    id 71
    label "osi&#261;ga&#263;"
  ]
  node [
    id 72
    label "przy&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 73
    label "poznawa&#263;"
  ]
  node [
    id 74
    label "go"
  ]
  node [
    id 75
    label "atakowa&#263;"
  ]
  node [
    id 76
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 77
    label "mount"
  ]
  node [
    id 78
    label "invade"
  ]
  node [
    id 79
    label "bra&#263;"
  ]
  node [
    id 80
    label "wnika&#263;"
  ]
  node [
    id 81
    label "move"
  ]
  node [
    id 82
    label "zaziera&#263;"
  ]
  node [
    id 83
    label "wznosi&#263;_si&#281;"
  ]
  node [
    id 84
    label "spotyka&#263;"
  ]
  node [
    id 85
    label "zaczyna&#263;"
  ]
  node [
    id 86
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 87
    label "formularz"
  ]
  node [
    id 88
    label "odcinek"
  ]
  node [
    id 89
    label "puchar"
  ]
  node [
    id 90
    label "korzy&#347;&#263;"
  ]
  node [
    id 91
    label "sukces"
  ]
  node [
    id 92
    label "conquest"
  ]
  node [
    id 93
    label "uznawa&#263;"
  ]
  node [
    id 94
    label "oznajmia&#263;"
  ]
  node [
    id 95
    label "attest"
  ]
  node [
    id 96
    label "baseball"
  ]
  node [
    id 97
    label "czyn"
  ]
  node [
    id 98
    label "error"
  ]
  node [
    id 99
    label "pomyli&#263;_si&#281;"
  ]
  node [
    id 100
    label "pomylenie_si&#281;"
  ]
  node [
    id 101
    label "mniemanie"
  ]
  node [
    id 102
    label "byk"
  ]
  node [
    id 103
    label "nieprawid&#322;owo&#347;&#263;"
  ]
  node [
    id 104
    label "rezultat"
  ]
  node [
    id 105
    label "proceed"
  ]
  node [
    id 106
    label "catch"
  ]
  node [
    id 107
    label "pozosta&#263;"
  ]
  node [
    id 108
    label "osta&#263;_si&#281;"
  ]
  node [
    id 109
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 110
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 111
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 112
    label "change"
  ]
  node [
    id 113
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 114
    label "uk&#322;ad"
  ]
  node [
    id 115
    label "sta&#263;_si&#281;"
  ]
  node [
    id 116
    label "raptowny"
  ]
  node [
    id 117
    label "embrace"
  ]
  node [
    id 118
    label "pozna&#263;"
  ]
  node [
    id 119
    label "straci&#263;_g&#322;ow&#281;"
  ]
  node [
    id 120
    label "insert"
  ]
  node [
    id 121
    label "ugotowa&#263;_si&#281;"
  ]
  node [
    id 122
    label "admit"
  ]
  node [
    id 123
    label "boil"
  ]
  node [
    id 124
    label "umowa"
  ]
  node [
    id 125
    label "zmie&#347;ci&#263;"
  ]
  node [
    id 126
    label "incorporate"
  ]
  node [
    id 127
    label "wezbra&#263;"
  ]
  node [
    id 128
    label "ustali&#263;"
  ]
  node [
    id 129
    label "spot&#281;gowa&#263;_si&#281;"
  ]
  node [
    id 130
    label "zamkn&#261;&#263;"
  ]
  node [
    id 131
    label "si&#281;ga&#263;"
  ]
  node [
    id 132
    label "trwa&#263;"
  ]
  node [
    id 133
    label "obecno&#347;&#263;"
  ]
  node [
    id 134
    label "stan"
  ]
  node [
    id 135
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 136
    label "stand"
  ]
  node [
    id 137
    label "mie&#263;_miejsce"
  ]
  node [
    id 138
    label "uczestniczy&#263;"
  ]
  node [
    id 139
    label "chodzi&#263;"
  ]
  node [
    id 140
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 141
    label "equal"
  ]
  node [
    id 142
    label "dokument"
  ]
  node [
    id 143
    label "sanction"
  ]
  node [
    id 144
    label "certificate"
  ]
  node [
    id 145
    label "przy&#347;wiadczenie"
  ]
  node [
    id 146
    label "o&#347;wiadczenie"
  ]
  node [
    id 147
    label "stwierdzenie"
  ]
  node [
    id 148
    label "kontrasygnowanie"
  ]
  node [
    id 149
    label "zgodzenie_si&#281;"
  ]
  node [
    id 150
    label "dramatize"
  ]
  node [
    id 151
    label "pofolgowa&#263;"
  ]
  node [
    id 152
    label "pozwoli&#263;"
  ]
  node [
    id 153
    label "leave"
  ]
  node [
    id 154
    label "receive"
  ]
  node [
    id 155
    label "uzna&#263;"
  ]
  node [
    id 156
    label "pogodzi&#263;_si&#281;"
  ]
  node [
    id 157
    label "Polska"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 12
  ]
  edge [
    source 8
    target 13
  ]
  edge [
    source 8
    target 17
  ]
  edge [
    source 8
    target 18
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 39
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 15
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 105
  ]
  edge [
    source 13
    target 106
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 13
    target 113
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 114
  ]
  edge [
    source 14
    target 115
  ]
  edge [
    source 14
    target 116
  ]
  edge [
    source 14
    target 117
  ]
  edge [
    source 14
    target 118
  ]
  edge [
    source 14
    target 119
  ]
  edge [
    source 14
    target 120
  ]
  edge [
    source 14
    target 121
  ]
  edge [
    source 14
    target 122
  ]
  edge [
    source 14
    target 123
  ]
  edge [
    source 14
    target 111
  ]
  edge [
    source 14
    target 124
  ]
  edge [
    source 14
    target 125
  ]
  edge [
    source 14
    target 126
  ]
  edge [
    source 14
    target 127
  ]
  edge [
    source 14
    target 128
  ]
  edge [
    source 14
    target 129
  ]
  edge [
    source 14
    target 130
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 131
  ]
  edge [
    source 16
    target 132
  ]
  edge [
    source 16
    target 133
  ]
  edge [
    source 16
    target 134
  ]
  edge [
    source 16
    target 135
  ]
  edge [
    source 16
    target 136
  ]
  edge [
    source 16
    target 137
  ]
  edge [
    source 16
    target 138
  ]
  edge [
    source 16
    target 139
  ]
  edge [
    source 16
    target 140
  ]
  edge [
    source 16
    target 141
  ]
  edge [
    source 17
    target 142
  ]
  edge [
    source 17
    target 143
  ]
  edge [
    source 17
    target 144
  ]
  edge [
    source 17
    target 145
  ]
  edge [
    source 17
    target 146
  ]
  edge [
    source 17
    target 147
  ]
  edge [
    source 17
    target 148
  ]
  edge [
    source 17
    target 149
  ]
  edge [
    source 18
    target 150
  ]
  edge [
    source 18
    target 151
  ]
  edge [
    source 18
    target 152
  ]
  edge [
    source 18
    target 153
  ]
  edge [
    source 18
    target 154
  ]
  edge [
    source 18
    target 155
  ]
  edge [
    source 18
    target 156
  ]
]
