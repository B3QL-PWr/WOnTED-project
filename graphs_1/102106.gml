graph [
  maxDegree 48
  minDegree 1
  meanDegree 1.9649122807017543
  density 0.03508771929824561
  graphCliqueNumber 2
  node [
    id 0
    label "program"
    origin "text"
  ]
  node [
    id 1
    label "dni"
    origin "text"
  ]
  node [
    id 2
    label "parz&#281;czewa"
    origin "text"
  ]
  node [
    id 3
    label "czerwiec"
    origin "text"
  ]
  node [
    id 4
    label "spis"
  ]
  node [
    id 5
    label "odinstalowanie"
  ]
  node [
    id 6
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 7
    label "za&#322;o&#380;enie"
  ]
  node [
    id 8
    label "podstawa"
  ]
  node [
    id 9
    label "emitowanie"
  ]
  node [
    id 10
    label "odinstalowywanie"
  ]
  node [
    id 11
    label "instrukcja"
  ]
  node [
    id 12
    label "punkt"
  ]
  node [
    id 13
    label "teleferie"
  ]
  node [
    id 14
    label "emitowa&#263;"
  ]
  node [
    id 15
    label "wytw&#243;r"
  ]
  node [
    id 16
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 17
    label "sekcja_krytyczna"
  ]
  node [
    id 18
    label "oferta"
  ]
  node [
    id 19
    label "prezentowa&#263;"
  ]
  node [
    id 20
    label "blok"
  ]
  node [
    id 21
    label "podprogram"
  ]
  node [
    id 22
    label "tryb"
  ]
  node [
    id 23
    label "dzia&#322;"
  ]
  node [
    id 24
    label "broszura"
  ]
  node [
    id 25
    label "deklaracja"
  ]
  node [
    id 26
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 27
    label "struktura_organizacyjna"
  ]
  node [
    id 28
    label "zaprezentowanie"
  ]
  node [
    id 29
    label "informatyka"
  ]
  node [
    id 30
    label "booklet"
  ]
  node [
    id 31
    label "menu"
  ]
  node [
    id 32
    label "oprogramowanie"
  ]
  node [
    id 33
    label "instalowanie"
  ]
  node [
    id 34
    label "furkacja"
  ]
  node [
    id 35
    label "odinstalowa&#263;"
  ]
  node [
    id 36
    label "instalowa&#263;"
  ]
  node [
    id 37
    label "pirat"
  ]
  node [
    id 38
    label "zainstalowanie"
  ]
  node [
    id 39
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 40
    label "ogranicznik_referencyjny"
  ]
  node [
    id 41
    label "zainstalowa&#263;"
  ]
  node [
    id 42
    label "kana&#322;"
  ]
  node [
    id 43
    label "zaprezentowa&#263;"
  ]
  node [
    id 44
    label "interfejs"
  ]
  node [
    id 45
    label "odinstalowywa&#263;"
  ]
  node [
    id 46
    label "folder"
  ]
  node [
    id 47
    label "course_of_study"
  ]
  node [
    id 48
    label "ram&#243;wka"
  ]
  node [
    id 49
    label "prezentowanie"
  ]
  node [
    id 50
    label "okno"
  ]
  node [
    id 51
    label "czas"
  ]
  node [
    id 52
    label "ro&#347;lina_zielna"
  ]
  node [
    id 53
    label "go&#378;dzikowate"
  ]
  node [
    id 54
    label "miesi&#261;c"
  ]
  node [
    id 55
    label "Bo&#380;e_Cia&#322;o"
  ]
  node [
    id 56
    label "&#347;wi&#281;ty_Jan"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
]
