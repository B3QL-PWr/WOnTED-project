graph [
  maxDegree 9
  minDegree 1
  meanDegree 1.8518518518518519
  density 0.07122507122507123
  graphCliqueNumber 2
  node [
    id 0
    label "nasa"
    origin "text"
  ]
  node [
    id 1
    label "opublikowa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "zdj&#281;cia"
    origin "text"
  ]
  node [
    id 3
    label "przes&#322;a&#263;"
    origin "text"
  ]
  node [
    id 4
    label "mars"
    origin "text"
  ]
  node [
    id 5
    label "przez"
    origin "text"
  ]
  node [
    id 6
    label "sonda"
    origin "text"
  ]
  node [
    id 7
    label "insight"
    origin "text"
  ]
  node [
    id 8
    label "upubliczni&#263;"
  ]
  node [
    id 9
    label "wydawnictwo"
  ]
  node [
    id 10
    label "wprowadzi&#263;"
  ]
  node [
    id 11
    label "picture"
  ]
  node [
    id 12
    label "grant"
  ]
  node [
    id 13
    label "convey"
  ]
  node [
    id 14
    label "przekaza&#263;"
  ]
  node [
    id 15
    label "maszt"
  ]
  node [
    id 16
    label "mina"
  ]
  node [
    id 17
    label "platforma"
  ]
  node [
    id 18
    label "sonda&#380;"
  ]
  node [
    id 19
    label "przyrz&#261;d"
  ]
  node [
    id 20
    label "leash"
  ]
  node [
    id 21
    label "badanie"
  ]
  node [
    id 22
    label "narz&#281;dzie"
  ]
  node [
    id 23
    label "lead"
  ]
  node [
    id 24
    label "statek_kosmiczny"
  ]
  node [
    id 25
    label "Mars"
  ]
  node [
    id 26
    label "InSight"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 18
  ]
  edge [
    source 6
    target 19
  ]
  edge [
    source 6
    target 20
  ]
  edge [
    source 6
    target 21
  ]
  edge [
    source 6
    target 22
  ]
  edge [
    source 6
    target 23
  ]
  edge [
    source 6
    target 24
  ]
  edge [
    source 25
    target 26
  ]
]
