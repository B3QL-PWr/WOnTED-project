graph [
  maxDegree 3
  minDegree 1
  meanDegree 1.6
  density 0.4
  graphCliqueNumber 2
  node [
    id 0
    label "motoryzacja"
    origin "text"
  ]
  node [
    id 1
    label "heheszki"
    origin "text"
  ]
  node [
    id 2
    label "humorobrazkowy"
    origin "text"
  ]
  node [
    id 3
    label "transport"
  ]
  node [
    id 4
    label "modernizacja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 1
    target 2
  ]
]
