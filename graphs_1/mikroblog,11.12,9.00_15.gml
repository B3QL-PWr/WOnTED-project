graph [
  maxDegree 26
  minDegree 1
  meanDegree 2
  density 0.02564102564102564
  graphCliqueNumber 2
  node [
    id 0
    label "wszyscy"
    origin "text"
  ]
  node [
    id 1
    label "ochoczo"
    origin "text"
  ]
  node [
    id 2
    label "&#347;mieszkuj&#261;"
    origin "text"
  ]
  node [
    id 3
    label "mirka"
    origin "text"
  ]
  node [
    id 4
    label "prawda"
    origin "text"
  ]
  node [
    id 5
    label "by&#263;"
    origin "text"
  ]
  node [
    id 6
    label "taka"
    origin "text"
  ]
  node [
    id 7
    label "wi&#281;kszo&#347;&#263;"
    origin "text"
  ]
  node [
    id 8
    label "siedzie&#263;"
    origin "text"
  ]
  node [
    id 9
    label "smutny"
    origin "text"
  ]
  node [
    id 10
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 11
    label "ch&#281;tnie"
  ]
  node [
    id 12
    label "weso&#322;o"
  ]
  node [
    id 13
    label "ochoczy"
  ]
  node [
    id 14
    label "dziarsko"
  ]
  node [
    id 15
    label "s&#261;d"
  ]
  node [
    id 16
    label "truth"
  ]
  node [
    id 17
    label "nieprawdziwy"
  ]
  node [
    id 18
    label "za&#322;o&#380;enie"
  ]
  node [
    id 19
    label "prawdziwy"
  ]
  node [
    id 20
    label "realia"
  ]
  node [
    id 21
    label "si&#281;ga&#263;"
  ]
  node [
    id 22
    label "trwa&#263;"
  ]
  node [
    id 23
    label "obecno&#347;&#263;"
  ]
  node [
    id 24
    label "stan"
  ]
  node [
    id 25
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 26
    label "stand"
  ]
  node [
    id 27
    label "mie&#263;_miejsce"
  ]
  node [
    id 28
    label "uczestniczy&#263;"
  ]
  node [
    id 29
    label "chodzi&#263;"
  ]
  node [
    id 30
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 31
    label "equal"
  ]
  node [
    id 32
    label "Bangladesz"
  ]
  node [
    id 33
    label "jednostka_monetarna"
  ]
  node [
    id 34
    label "majority"
  ]
  node [
    id 35
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 36
    label "sit"
  ]
  node [
    id 37
    label "pause"
  ]
  node [
    id 38
    label "ptak"
  ]
  node [
    id 39
    label "garowa&#263;"
  ]
  node [
    id 40
    label "mieszka&#263;"
  ]
  node [
    id 41
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 42
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 43
    label "przebywa&#263;"
  ]
  node [
    id 44
    label "brood"
  ]
  node [
    id 45
    label "zwierz&#281;"
  ]
  node [
    id 46
    label "doprowadza&#263;"
  ]
  node [
    id 47
    label "spoczywa&#263;"
  ]
  node [
    id 48
    label "tkwi&#263;"
  ]
  node [
    id 49
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 50
    label "z&#322;y"
  ]
  node [
    id 51
    label "smutno"
  ]
  node [
    id 52
    label "negatywny"
  ]
  node [
    id 53
    label "przykry"
  ]
  node [
    id 54
    label "asymilowa&#263;"
  ]
  node [
    id 55
    label "wapniak"
  ]
  node [
    id 56
    label "dwun&#243;g"
  ]
  node [
    id 57
    label "polifag"
  ]
  node [
    id 58
    label "wz&#243;r"
  ]
  node [
    id 59
    label "profanum"
  ]
  node [
    id 60
    label "hominid"
  ]
  node [
    id 61
    label "homo_sapiens"
  ]
  node [
    id 62
    label "nasada"
  ]
  node [
    id 63
    label "podw&#322;adny"
  ]
  node [
    id 64
    label "ludzko&#347;&#263;"
  ]
  node [
    id 65
    label "os&#322;abianie"
  ]
  node [
    id 66
    label "mikrokosmos"
  ]
  node [
    id 67
    label "portrecista"
  ]
  node [
    id 68
    label "duch"
  ]
  node [
    id 69
    label "oddzia&#322;ywanie"
  ]
  node [
    id 70
    label "g&#322;owa"
  ]
  node [
    id 71
    label "asymilowanie"
  ]
  node [
    id 72
    label "osoba"
  ]
  node [
    id 73
    label "os&#322;abia&#263;"
  ]
  node [
    id 74
    label "figura"
  ]
  node [
    id 75
    label "Adam"
  ]
  node [
    id 76
    label "senior"
  ]
  node [
    id 77
    label "antropochoria"
  ]
  node [
    id 78
    label "posta&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 34
  ]
  edge [
    source 7
    target 35
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 22
  ]
  edge [
    source 8
    target 36
  ]
  edge [
    source 8
    target 37
  ]
  edge [
    source 8
    target 38
  ]
  edge [
    source 8
    target 39
  ]
  edge [
    source 8
    target 40
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 8
    target 46
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 50
  ]
  edge [
    source 9
    target 51
  ]
  edge [
    source 9
    target 52
  ]
  edge [
    source 9
    target 53
  ]
  edge [
    source 10
    target 54
  ]
  edge [
    source 10
    target 55
  ]
  edge [
    source 10
    target 56
  ]
  edge [
    source 10
    target 57
  ]
  edge [
    source 10
    target 58
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 10
    target 60
  ]
  edge [
    source 10
    target 61
  ]
  edge [
    source 10
    target 62
  ]
  edge [
    source 10
    target 63
  ]
  edge [
    source 10
    target 64
  ]
  edge [
    source 10
    target 65
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 10
    target 69
  ]
  edge [
    source 10
    target 70
  ]
  edge [
    source 10
    target 71
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 78
  ]
]
