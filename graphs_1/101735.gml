graph [
  maxDegree 9
  minDegree 1
  meanDegree 1.9130434782608696
  density 0.08695652173913043
  graphCliqueNumber 2
  node [
    id 0
    label "dziennik"
    origin "text"
  ]
  node [
    id 1
    label "ustawa"
    origin "text"
  ]
  node [
    id 2
    label "grudzie&#324;"
    origin "text"
  ]
  node [
    id 3
    label "spis"
  ]
  node [
    id 4
    label "sheet"
  ]
  node [
    id 5
    label "gazeta"
  ]
  node [
    id 6
    label "diariusz"
  ]
  node [
    id 7
    label "pami&#281;tnik"
  ]
  node [
    id 8
    label "journal"
  ]
  node [
    id 9
    label "ksi&#281;ga"
  ]
  node [
    id 10
    label "program_informacyjny"
  ]
  node [
    id 11
    label "Karta_Nauczyciela"
  ]
  node [
    id 12
    label "marc&#243;wka"
  ]
  node [
    id 13
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 14
    label "akt"
  ]
  node [
    id 15
    label "przej&#347;&#263;"
  ]
  node [
    id 16
    label "charter"
  ]
  node [
    id 17
    label "przej&#347;cie"
  ]
  node [
    id 18
    label "Bo&#380;e_Narodzenie"
  ]
  node [
    id 19
    label "miesi&#261;c"
  ]
  node [
    id 20
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 21
    label "Barb&#243;rka"
  ]
  node [
    id 22
    label "Sylwester"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
]
