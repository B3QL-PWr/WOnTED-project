graph [
  maxDegree 10
  minDegree 1
  meanDegree 1.8461538461538463
  density 0.15384615384615385
  graphCliqueNumber 2
  node [
    id 0
    label "szczepienie"
    origin "text"
  ]
  node [
    id 1
    label "antyszczepionkowcy"
    origin "text"
  ]
  node [
    id 2
    label "niedojebaniemozgowe"
    origin "text"
  ]
  node [
    id 3
    label "heheszki"
    origin "text"
  ]
  node [
    id 4
    label "wariolacja"
  ]
  node [
    id 5
    label "immunizacja"
  ]
  node [
    id 6
    label "zabieg"
  ]
  node [
    id 7
    label "uodpornianie"
  ]
  node [
    id 8
    label "inoculation"
  ]
  node [
    id 9
    label "uszlachetnianie"
  ]
  node [
    id 10
    label "uprawianie"
  ]
  node [
    id 11
    label "metoda"
  ]
  node [
    id 12
    label "immunizacja_czynna"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
]
