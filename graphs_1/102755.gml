graph [
  maxDegree 12
  minDegree 1
  meanDegree 1.9230769230769231
  density 0.07692307692307693
  graphCliqueNumber 2
  node [
    id 0
    label "decyzja"
    origin "text"
  ]
  node [
    id 1
    label "minister"
    origin "text"
  ]
  node [
    id 2
    label "sprawa"
    origin "text"
  ]
  node [
    id 3
    label "zagraniczny"
    origin "text"
  ]
  node [
    id 4
    label "dokument"
  ]
  node [
    id 5
    label "resolution"
  ]
  node [
    id 6
    label "zdecydowanie"
  ]
  node [
    id 7
    label "wytw&#243;r"
  ]
  node [
    id 8
    label "podj&#281;cie_decyzji"
  ]
  node [
    id 9
    label "management"
  ]
  node [
    id 10
    label "Goebbels"
  ]
  node [
    id 11
    label "Sto&#322;ypin"
  ]
  node [
    id 12
    label "rz&#261;d"
  ]
  node [
    id 13
    label "dostojnik"
  ]
  node [
    id 14
    label "temat"
  ]
  node [
    id 15
    label "kognicja"
  ]
  node [
    id 16
    label "idea"
  ]
  node [
    id 17
    label "szczeg&#243;&#322;"
  ]
  node [
    id 18
    label "rzecz"
  ]
  node [
    id 19
    label "wydarzenie"
  ]
  node [
    id 20
    label "przes&#322;anka"
  ]
  node [
    id 21
    label "rozprawa"
  ]
  node [
    id 22
    label "object"
  ]
  node [
    id 23
    label "proposition"
  ]
  node [
    id 24
    label "zagranicznie"
  ]
  node [
    id 25
    label "obcy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
]
