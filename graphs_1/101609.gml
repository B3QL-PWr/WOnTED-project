graph [
  maxDegree 29
  minDegree 1
  meanDegree 2.1879518072289157
  density 0.005284907746929748
  graphCliqueNumber 3
  node [
    id 0
    label "ojciec"
    origin "text"
  ]
  node [
    id 1
    label "by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "przera&#380;ony"
    origin "text"
  ]
  node [
    id 3
    label "ale"
    origin "text"
  ]
  node [
    id 4
    label "jedno"
    origin "text"
  ]
  node [
    id 5
    label "pon&#281;tny"
    origin "text"
  ]
  node [
    id 6
    label "wejrzenie"
    origin "text"
  ]
  node [
    id 7
    label "panna"
    origin "text"
  ]
  node [
    id 8
    label "cimiento"
    origin "text"
  ]
  node [
    id 9
    label "przywr&#243;ci&#263;"
    origin "text"
  ]
  node [
    id 10
    label "odwaga"
    origin "text"
  ]
  node [
    id 11
    label "p&#243;j&#347;&#263;"
    origin "text"
  ]
  node [
    id 12
    label "rozgo&#347;ci&#263;"
    origin "text"
  ]
  node [
    id 13
    label "si&#281;"
    origin "text"
  ]
  node [
    id 14
    label "nowa"
    origin "text"
  ]
  node [
    id 15
    label "swoje"
    origin "text"
  ]
  node [
    id 16
    label "mieszkanie"
    origin "text"
  ]
  node [
    id 17
    label "nied&#322;ugo"
    origin "text"
  ]
  node [
    id 18
    label "nim"
    origin "text"
  ]
  node [
    id 19
    label "sam"
    origin "text"
  ]
  node [
    id 20
    label "zostawa&#263;"
    origin "text"
  ]
  node [
    id 21
    label "pani"
    origin "text"
  ]
  node [
    id 22
    label "przysz&#322;y"
    origin "text"
  ]
  node [
    id 23
    label "powiedzie&#263;"
    origin "text"
  ]
  node [
    id 24
    label "naradzi&#263;"
    origin "text"
  ]
  node [
    id 25
    label "bratanica"
    origin "text"
  ]
  node [
    id 26
    label "postanowi&#263;"
    origin "text"
  ]
  node [
    id 27
    label "odda&#263;"
    origin "text"
  ]
  node [
    id 28
    label "cuarto"
    origin "text"
  ]
  node [
    id 29
    label "principal"
    origin "text"
  ]
  node [
    id 30
    label "wychodz&#261;ca"
    origin "text"
  ]
  node [
    id 31
    label "ulica"
    origin "text"
  ]
  node [
    id 32
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 33
    label "upodobanie"
    origin "text"
  ]
  node [
    id 34
    label "liczy&#263;"
    origin "text"
  ]
  node [
    id 35
    label "przechodzie&#324;"
    origin "text"
  ]
  node [
    id 36
    label "lub"
    origin "text"
  ]
  node [
    id 37
    label "dach&#243;wka"
    origin "text"
  ]
  node [
    id 38
    label "pa&#322;ac"
    origin "text"
  ]
  node [
    id 39
    label "ksi&#261;&#380;&#281;"
    origin "text"
  ]
  node [
    id 40
    label "alba"
    origin "text"
  ]
  node [
    id 41
    label "ochota"
    origin "text"
  ]
  node [
    id 42
    label "przysta&#263;"
    origin "text"
  ]
  node [
    id 43
    label "zamiana"
    origin "text"
  ]
  node [
    id 44
    label "prosi&#263;"
    origin "text"
  ]
  node [
    id 45
    label "tylko"
    origin "text"
  ]
  node [
    id 46
    label "pozwolenie"
    origin "text"
  ]
  node [
    id 47
    label "zostawi&#263;"
    origin "text"
  ]
  node [
    id 48
    label "kolorowy"
    origin "text"
  ]
  node [
    id 49
    label "atrament"
    origin "text"
  ]
  node [
    id 50
    label "dawny"
    origin "text"
  ]
  node [
    id 51
    label "miejsce"
    origin "text"
  ]
  node [
    id 52
    label "kiwni&#281;cie"
    origin "text"
  ]
  node [
    id 53
    label "wyrazi&#263;"
    origin "text"
  ]
  node [
    id 54
    label "sw&#243;j"
    origin "text"
  ]
  node [
    id 55
    label "zgoda"
    origin "text"
  ]
  node [
    id 56
    label "pomys&#322;odawca"
  ]
  node [
    id 57
    label "kszta&#322;ciciel"
  ]
  node [
    id 58
    label "tworzyciel"
  ]
  node [
    id 59
    label "ojczym"
  ]
  node [
    id 60
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 61
    label "stary"
  ]
  node [
    id 62
    label "samiec"
  ]
  node [
    id 63
    label "papa"
  ]
  node [
    id 64
    label "&#347;w"
  ]
  node [
    id 65
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 66
    label "zakonnik"
  ]
  node [
    id 67
    label "kuwada"
  ]
  node [
    id 68
    label "przodek"
  ]
  node [
    id 69
    label "wykonawca"
  ]
  node [
    id 70
    label "rodzice"
  ]
  node [
    id 71
    label "rodzic"
  ]
  node [
    id 72
    label "si&#281;ga&#263;"
  ]
  node [
    id 73
    label "trwa&#263;"
  ]
  node [
    id 74
    label "obecno&#347;&#263;"
  ]
  node [
    id 75
    label "stan"
  ]
  node [
    id 76
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 77
    label "stand"
  ]
  node [
    id 78
    label "mie&#263;_miejsce"
  ]
  node [
    id 79
    label "uczestniczy&#263;"
  ]
  node [
    id 80
    label "chodzi&#263;"
  ]
  node [
    id 81
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 82
    label "equal"
  ]
  node [
    id 83
    label "piwo"
  ]
  node [
    id 84
    label "powabny"
  ]
  node [
    id 85
    label "apetycznie"
  ]
  node [
    id 86
    label "pon&#281;tnie"
  ]
  node [
    id 87
    label "kusz&#261;cy"
  ]
  node [
    id 88
    label "expression"
  ]
  node [
    id 89
    label "zobaczenie"
  ]
  node [
    id 90
    label "spojrzenie"
  ]
  node [
    id 91
    label "m&#281;tnienie"
  ]
  node [
    id 92
    label "wzrok"
  ]
  node [
    id 93
    label "wej&#347;cie"
  ]
  node [
    id 94
    label "m&#281;tnie&#263;"
  ]
  node [
    id 95
    label "glance"
  ]
  node [
    id 96
    label "kontakt"
  ]
  node [
    id 97
    label "podlotek"
  ]
  node [
    id 98
    label "cz&#322;owiek"
  ]
  node [
    id 99
    label "donna"
  ]
  node [
    id 100
    label "sp&#243;dniczka"
  ]
  node [
    id 101
    label "zwrot"
  ]
  node [
    id 102
    label "dziewica"
  ]
  node [
    id 103
    label "partnerka"
  ]
  node [
    id 104
    label "sympatia"
  ]
  node [
    id 105
    label "dziewczyna"
  ]
  node [
    id 106
    label "sikorka"
  ]
  node [
    id 107
    label "niezam&#281;&#380;na"
  ]
  node [
    id 108
    label "doprowadzi&#263;"
  ]
  node [
    id 109
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 110
    label "cecha"
  ]
  node [
    id 111
    label "courage"
  ]
  node [
    id 112
    label "dusza"
  ]
  node [
    id 113
    label "opu&#347;ci&#263;"
  ]
  node [
    id 114
    label "poruszy&#263;_si&#281;"
  ]
  node [
    id 115
    label "proceed"
  ]
  node [
    id 116
    label "powie&#347;&#263;_si&#281;"
  ]
  node [
    id 117
    label "popsu&#263;_si&#281;"
  ]
  node [
    id 118
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 119
    label "kopn&#261;&#263;_si&#281;"
  ]
  node [
    id 120
    label "zacz&#261;&#263;"
  ]
  node [
    id 121
    label "zmieni&#263;"
  ]
  node [
    id 122
    label "zosta&#263;"
  ]
  node [
    id 123
    label "sail"
  ]
  node [
    id 124
    label "leave"
  ]
  node [
    id 125
    label "uda&#263;_si&#281;"
  ]
  node [
    id 126
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 127
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 128
    label "zrobi&#263;"
  ]
  node [
    id 129
    label "wyp&#322;yn&#261;&#263;"
  ]
  node [
    id 130
    label "sprzeda&#263;_si&#281;"
  ]
  node [
    id 131
    label "przyj&#261;&#263;"
  ]
  node [
    id 132
    label "potoczy&#263;_si&#281;"
  ]
  node [
    id 133
    label "become"
  ]
  node [
    id 134
    label "play_along"
  ]
  node [
    id 135
    label "travel"
  ]
  node [
    id 136
    label "gwiazda"
  ]
  node [
    id 137
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 138
    label "stanie"
  ]
  node [
    id 139
    label "przebywanie"
  ]
  node [
    id 140
    label "panowanie"
  ]
  node [
    id 141
    label "zajmowanie"
  ]
  node [
    id 142
    label "pomieszkanie"
  ]
  node [
    id 143
    label "adjustment"
  ]
  node [
    id 144
    label "modu&#322;_mieszkalny"
  ]
  node [
    id 145
    label "lokal"
  ]
  node [
    id 146
    label "kwadrat"
  ]
  node [
    id 147
    label "animation"
  ]
  node [
    id 148
    label "dom"
  ]
  node [
    id 149
    label "wpr&#281;dce"
  ]
  node [
    id 150
    label "blisko"
  ]
  node [
    id 151
    label "nied&#322;ugi"
  ]
  node [
    id 152
    label "kr&#243;tki"
  ]
  node [
    id 153
    label "gra_planszowa"
  ]
  node [
    id 154
    label "sklep"
  ]
  node [
    id 155
    label "pozostawa&#263;"
  ]
  node [
    id 156
    label "zmienia&#263;_si&#281;"
  ]
  node [
    id 157
    label "ostawa&#263;_si&#281;"
  ]
  node [
    id 158
    label "stop"
  ]
  node [
    id 159
    label "przebywa&#263;"
  ]
  node [
    id 160
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 161
    label "blend"
  ]
  node [
    id 162
    label "zachowywa&#263;_si&#281;"
  ]
  node [
    id 163
    label "change"
  ]
  node [
    id 164
    label "przekwitanie"
  ]
  node [
    id 165
    label "uleganie"
  ]
  node [
    id 166
    label "ulega&#263;"
  ]
  node [
    id 167
    label "partner"
  ]
  node [
    id 168
    label "doros&#322;y"
  ]
  node [
    id 169
    label "przyw&#243;dczyni"
  ]
  node [
    id 170
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 171
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 172
    label "ulec"
  ]
  node [
    id 173
    label "kobita"
  ]
  node [
    id 174
    label "&#322;ono"
  ]
  node [
    id 175
    label "kobieta"
  ]
  node [
    id 176
    label "w&#322;a&#347;cicielka"
  ]
  node [
    id 177
    label "m&#281;&#380;yna"
  ]
  node [
    id 178
    label "babka"
  ]
  node [
    id 179
    label "samica"
  ]
  node [
    id 180
    label "pa&#324;stwo"
  ]
  node [
    id 181
    label "ulegni&#281;cie"
  ]
  node [
    id 182
    label "menopauza"
  ]
  node [
    id 183
    label "kolejny"
  ]
  node [
    id 184
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 185
    label "express"
  ]
  node [
    id 186
    label "rzekn&#261;&#263;"
  ]
  node [
    id 187
    label "okre&#347;li&#263;"
  ]
  node [
    id 188
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 189
    label "unwrap"
  ]
  node [
    id 190
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 191
    label "convey"
  ]
  node [
    id 192
    label "discover"
  ]
  node [
    id 193
    label "wydoby&#263;"
  ]
  node [
    id 194
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 195
    label "poda&#263;"
  ]
  node [
    id 196
    label "krewna"
  ]
  node [
    id 197
    label "bratanka"
  ]
  node [
    id 198
    label "sta&#263;_si&#281;"
  ]
  node [
    id 199
    label "podj&#261;&#263;"
  ]
  node [
    id 200
    label "determine"
  ]
  node [
    id 201
    label "restore"
  ]
  node [
    id 202
    label "da&#263;"
  ]
  node [
    id 203
    label "dostarczy&#263;"
  ]
  node [
    id 204
    label "odst&#261;pi&#263;"
  ]
  node [
    id 205
    label "sacrifice"
  ]
  node [
    id 206
    label "odpowiedzie&#263;"
  ]
  node [
    id 207
    label "sprzeda&#263;"
  ]
  node [
    id 208
    label "reflect"
  ]
  node [
    id 209
    label "przedstawi&#263;"
  ]
  node [
    id 210
    label "transfer"
  ]
  node [
    id 211
    label "give"
  ]
  node [
    id 212
    label "za&#322;atwi&#263;_si&#281;"
  ]
  node [
    id 213
    label "umie&#347;ci&#263;"
  ]
  node [
    id 214
    label "przekaza&#263;"
  ]
  node [
    id 215
    label "z_powrotem"
  ]
  node [
    id 216
    label "picture"
  ]
  node [
    id 217
    label "deliver"
  ]
  node [
    id 218
    label "&#347;rodowisko"
  ]
  node [
    id 219
    label "miasteczko"
  ]
  node [
    id 220
    label "streetball"
  ]
  node [
    id 221
    label "pierzeja"
  ]
  node [
    id 222
    label "grupa"
  ]
  node [
    id 223
    label "pas_ruchu"
  ]
  node [
    id 224
    label "pas_rozdzielczy"
  ]
  node [
    id 225
    label "jezdnia"
  ]
  node [
    id 226
    label "droga"
  ]
  node [
    id 227
    label "korona_drogi"
  ]
  node [
    id 228
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 229
    label "chodnik"
  ]
  node [
    id 230
    label "arteria"
  ]
  node [
    id 231
    label "Broadway"
  ]
  node [
    id 232
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 233
    label "wysepka"
  ]
  node [
    id 234
    label "autostrada"
  ]
  node [
    id 235
    label "tendency"
  ]
  node [
    id 236
    label "feblik"
  ]
  node [
    id 237
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 238
    label "wyr&#380;n&#261;&#263;"
  ]
  node [
    id 239
    label "zajawka"
  ]
  node [
    id 240
    label "report"
  ]
  node [
    id 241
    label "dodawa&#263;"
  ]
  node [
    id 242
    label "wymienia&#263;"
  ]
  node [
    id 243
    label "okre&#347;la&#263;"
  ]
  node [
    id 244
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 245
    label "dyskalkulia"
  ]
  node [
    id 246
    label "wynagrodzenie"
  ]
  node [
    id 247
    label "admit"
  ]
  node [
    id 248
    label "osi&#261;ga&#263;"
  ]
  node [
    id 249
    label "wyznacza&#263;"
  ]
  node [
    id 250
    label "posiada&#263;"
  ]
  node [
    id 251
    label "mierzy&#263;"
  ]
  node [
    id 252
    label "odlicza&#263;"
  ]
  node [
    id 253
    label "bra&#263;"
  ]
  node [
    id 254
    label "wycenia&#263;"
  ]
  node [
    id 255
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 256
    label "rachowa&#263;"
  ]
  node [
    id 257
    label "tell"
  ]
  node [
    id 258
    label "uwzgl&#281;dnia&#263;"
  ]
  node [
    id 259
    label "policza&#263;"
  ]
  node [
    id 260
    label "count"
  ]
  node [
    id 261
    label "pieszy"
  ]
  node [
    id 262
    label "pokrycie"
  ]
  node [
    id 263
    label "Wersal"
  ]
  node [
    id 264
    label "Belweder"
  ]
  node [
    id 265
    label "budynek"
  ]
  node [
    id 266
    label "w&#322;adza"
  ]
  node [
    id 267
    label "rezydencja"
  ]
  node [
    id 268
    label "Hamlet"
  ]
  node [
    id 269
    label "Kazimierz_II_Sprawiedliwy"
  ]
  node [
    id 270
    label "Piast"
  ]
  node [
    id 271
    label "arystokrata"
  ]
  node [
    id 272
    label "tytu&#322;"
  ]
  node [
    id 273
    label "kochanie"
  ]
  node [
    id 274
    label "Bismarck"
  ]
  node [
    id 275
    label "Herman"
  ]
  node [
    id 276
    label "Mieszko_I"
  ]
  node [
    id 277
    label "w&#322;adca"
  ]
  node [
    id 278
    label "fircyk"
  ]
  node [
    id 279
    label "szata_liturgiczna"
  ]
  node [
    id 280
    label "pie&#347;&#324;"
  ]
  node [
    id 281
    label "oskoma"
  ]
  node [
    id 282
    label "uczta"
  ]
  node [
    id 283
    label "emocja"
  ]
  node [
    id 284
    label "inclination"
  ]
  node [
    id 285
    label "wej&#347;&#263;"
  ]
  node [
    id 286
    label "trza"
  ]
  node [
    id 287
    label "pause"
  ]
  node [
    id 288
    label "pofolgowa&#263;"
  ]
  node [
    id 289
    label "mount"
  ]
  node [
    id 290
    label "uzna&#263;"
  ]
  node [
    id 291
    label "przylgn&#261;&#263;"
  ]
  node [
    id 292
    label "necessity"
  ]
  node [
    id 293
    label "zmianka"
  ]
  node [
    id 294
    label "exchange"
  ]
  node [
    id 295
    label "wydarzenie"
  ]
  node [
    id 296
    label "odmienianie"
  ]
  node [
    id 297
    label "zmiana"
  ]
  node [
    id 298
    label "zezwala&#263;"
  ]
  node [
    id 299
    label "ask"
  ]
  node [
    id 300
    label "invite"
  ]
  node [
    id 301
    label "zach&#281;ca&#263;"
  ]
  node [
    id 302
    label "preach"
  ]
  node [
    id 303
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 304
    label "pies"
  ]
  node [
    id 305
    label "ujmowa&#263;_si&#281;"
  ]
  node [
    id 306
    label "poleca&#263;"
  ]
  node [
    id 307
    label "zaprasza&#263;"
  ]
  node [
    id 308
    label "suffice"
  ]
  node [
    id 309
    label "dziwi&#263;_si&#281;"
  ]
  node [
    id 310
    label "dokument"
  ]
  node [
    id 311
    label "zrobienie"
  ]
  node [
    id 312
    label "zwolnienie_si&#281;"
  ]
  node [
    id 313
    label "zwalnianie_si&#281;"
  ]
  node [
    id 314
    label "odpowied&#378;"
  ]
  node [
    id 315
    label "umo&#380;liwienie"
  ]
  node [
    id 316
    label "wiedza"
  ]
  node [
    id 317
    label "pozwole&#324;stwo"
  ]
  node [
    id 318
    label "franchise"
  ]
  node [
    id 319
    label "pofolgowanie"
  ]
  node [
    id 320
    label "zwolni&#263;_si&#281;"
  ]
  node [
    id 321
    label "zwalnia&#263;_si&#281;"
  ]
  node [
    id 322
    label "decyzja"
  ]
  node [
    id 323
    label "license"
  ]
  node [
    id 324
    label "bycie_w_stanie"
  ]
  node [
    id 325
    label "odwieszenie"
  ]
  node [
    id 326
    label "uznanie"
  ]
  node [
    id 327
    label "koncesjonowanie"
  ]
  node [
    id 328
    label "authorization"
  ]
  node [
    id 329
    label "skrzywdzi&#263;"
  ]
  node [
    id 330
    label "impart"
  ]
  node [
    id 331
    label "liszy&#263;"
  ]
  node [
    id 332
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 333
    label "zachowa&#263;"
  ]
  node [
    id 334
    label "stworzy&#263;"
  ]
  node [
    id 335
    label "overhaul"
  ]
  node [
    id 336
    label "permit"
  ]
  node [
    id 337
    label "przewy&#380;szy&#263;"
  ]
  node [
    id 338
    label "wyda&#263;"
  ]
  node [
    id 339
    label "wyznaczy&#263;"
  ]
  node [
    id 340
    label "zerwa&#263;"
  ]
  node [
    id 341
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 342
    label "zaplanowa&#263;"
  ]
  node [
    id 343
    label "zabra&#263;"
  ]
  node [
    id 344
    label "shove"
  ]
  node [
    id 345
    label "spowodowa&#263;"
  ]
  node [
    id 346
    label "zrezygnowa&#263;"
  ]
  node [
    id 347
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 348
    label "drop"
  ]
  node [
    id 349
    label "release"
  ]
  node [
    id 350
    label "shelve"
  ]
  node [
    id 351
    label "kolorowo"
  ]
  node [
    id 352
    label "barwienie_si&#281;"
  ]
  node [
    id 353
    label "pi&#281;kny"
  ]
  node [
    id 354
    label "przyjemny"
  ]
  node [
    id 355
    label "ubarwienie"
  ]
  node [
    id 356
    label "kolorowanie"
  ]
  node [
    id 357
    label "r&#243;&#380;nokolorowo"
  ]
  node [
    id 358
    label "barwisty"
  ]
  node [
    id 359
    label "r&#243;&#380;nofarbny"
  ]
  node [
    id 360
    label "ciekawy"
  ]
  node [
    id 361
    label "weso&#322;y"
  ]
  node [
    id 362
    label "zabarwienie_si&#281;"
  ]
  node [
    id 363
    label "zr&#243;&#380;nicowany"
  ]
  node [
    id 364
    label "barwienie"
  ]
  node [
    id 365
    label "barwnie"
  ]
  node [
    id 366
    label "farba"
  ]
  node [
    id 367
    label "ka&#322;amarz"
  ]
  node [
    id 368
    label "inkaust"
  ]
  node [
    id 369
    label "pi&#243;ro"
  ]
  node [
    id 370
    label "roztw&#243;r"
  ]
  node [
    id 371
    label "sp&#322;ywak"
  ]
  node [
    id 372
    label "przesz&#322;y"
  ]
  node [
    id 373
    label "dawno"
  ]
  node [
    id 374
    label "dawniej"
  ]
  node [
    id 375
    label "kombatant"
  ]
  node [
    id 376
    label "odleg&#322;y"
  ]
  node [
    id 377
    label "anachroniczny"
  ]
  node [
    id 378
    label "przestarza&#322;y"
  ]
  node [
    id 379
    label "od_dawna"
  ]
  node [
    id 380
    label "poprzedni"
  ]
  node [
    id 381
    label "d&#322;ugoletni"
  ]
  node [
    id 382
    label "wcze&#347;niejszy"
  ]
  node [
    id 383
    label "niegdysiejszy"
  ]
  node [
    id 384
    label "cia&#322;o"
  ]
  node [
    id 385
    label "plac"
  ]
  node [
    id 386
    label "uwaga"
  ]
  node [
    id 387
    label "przestrze&#324;"
  ]
  node [
    id 388
    label "status"
  ]
  node [
    id 389
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 390
    label "chwila"
  ]
  node [
    id 391
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 392
    label "rz&#261;d"
  ]
  node [
    id 393
    label "praca"
  ]
  node [
    id 394
    label "location"
  ]
  node [
    id 395
    label "warunek_lokalowy"
  ]
  node [
    id 396
    label "gest"
  ]
  node [
    id 397
    label "gesture"
  ]
  node [
    id 398
    label "pokiwanie"
  ]
  node [
    id 399
    label "machni&#281;cie"
  ]
  node [
    id 400
    label "nabranie"
  ]
  node [
    id 401
    label "zdradzi&#263;_si&#281;"
  ]
  node [
    id 402
    label "zakomunikowa&#263;"
  ]
  node [
    id 403
    label "vent"
  ]
  node [
    id 404
    label "oznaczy&#263;"
  ]
  node [
    id 405
    label "testify"
  ]
  node [
    id 406
    label "bli&#378;ni"
  ]
  node [
    id 407
    label "odpowiedni"
  ]
  node [
    id 408
    label "swojak"
  ]
  node [
    id 409
    label "samodzielny"
  ]
  node [
    id 410
    label "spok&#243;j"
  ]
  node [
    id 411
    label "entity"
  ]
  node [
    id 412
    label "agreement"
  ]
  node [
    id 413
    label "consensus"
  ]
  node [
    id 414
    label "jednomy&#347;lno&#347;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 21
  ]
  edge [
    source 8
    target 22
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 10
    target 110
  ]
  edge [
    source 10
    target 111
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 28
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 113
  ]
  edge [
    source 11
    target 114
  ]
  edge [
    source 11
    target 115
  ]
  edge [
    source 11
    target 116
  ]
  edge [
    source 11
    target 117
  ]
  edge [
    source 11
    target 118
  ]
  edge [
    source 11
    target 119
  ]
  edge [
    source 11
    target 120
  ]
  edge [
    source 11
    target 121
  ]
  edge [
    source 11
    target 122
  ]
  edge [
    source 11
    target 123
  ]
  edge [
    source 11
    target 124
  ]
  edge [
    source 11
    target 125
  ]
  edge [
    source 11
    target 126
  ]
  edge [
    source 11
    target 127
  ]
  edge [
    source 11
    target 128
  ]
  edge [
    source 11
    target 129
  ]
  edge [
    source 11
    target 130
  ]
  edge [
    source 11
    target 131
  ]
  edge [
    source 11
    target 132
  ]
  edge [
    source 11
    target 133
  ]
  edge [
    source 11
    target 134
  ]
  edge [
    source 11
    target 135
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 24
  ]
  edge [
    source 13
    target 25
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 136
  ]
  edge [
    source 14
    target 137
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 30
  ]
  edge [
    source 16
    target 138
  ]
  edge [
    source 16
    target 139
  ]
  edge [
    source 16
    target 140
  ]
  edge [
    source 16
    target 141
  ]
  edge [
    source 16
    target 142
  ]
  edge [
    source 16
    target 143
  ]
  edge [
    source 16
    target 144
  ]
  edge [
    source 16
    target 145
  ]
  edge [
    source 16
    target 146
  ]
  edge [
    source 16
    target 147
  ]
  edge [
    source 16
    target 148
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 149
  ]
  edge [
    source 17
    target 150
  ]
  edge [
    source 17
    target 151
  ]
  edge [
    source 17
    target 152
  ]
  edge [
    source 17
    target 31
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 153
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 154
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 155
  ]
  edge [
    source 20
    target 156
  ]
  edge [
    source 20
    target 157
  ]
  edge [
    source 20
    target 158
  ]
  edge [
    source 20
    target 159
  ]
  edge [
    source 20
    target 160
  ]
  edge [
    source 20
    target 161
  ]
  edge [
    source 20
    target 162
  ]
  edge [
    source 20
    target 163
  ]
  edge [
    source 21
    target 98
  ]
  edge [
    source 21
    target 164
  ]
  edge [
    source 21
    target 101
  ]
  edge [
    source 21
    target 165
  ]
  edge [
    source 21
    target 166
  ]
  edge [
    source 21
    target 167
  ]
  edge [
    source 21
    target 168
  ]
  edge [
    source 21
    target 169
  ]
  edge [
    source 21
    target 170
  ]
  edge [
    source 21
    target 171
  ]
  edge [
    source 21
    target 172
  ]
  edge [
    source 21
    target 173
  ]
  edge [
    source 21
    target 174
  ]
  edge [
    source 21
    target 175
  ]
  edge [
    source 21
    target 176
  ]
  edge [
    source 21
    target 177
  ]
  edge [
    source 21
    target 178
  ]
  edge [
    source 21
    target 179
  ]
  edge [
    source 21
    target 180
  ]
  edge [
    source 21
    target 181
  ]
  edge [
    source 21
    target 182
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 183
  ]
  edge [
    source 22
    target 38
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 184
  ]
  edge [
    source 23
    target 185
  ]
  edge [
    source 23
    target 186
  ]
  edge [
    source 23
    target 187
  ]
  edge [
    source 23
    target 53
  ]
  edge [
    source 23
    target 188
  ]
  edge [
    source 23
    target 189
  ]
  edge [
    source 23
    target 190
  ]
  edge [
    source 23
    target 191
  ]
  edge [
    source 23
    target 192
  ]
  edge [
    source 23
    target 193
  ]
  edge [
    source 23
    target 194
  ]
  edge [
    source 23
    target 195
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 196
  ]
  edge [
    source 25
    target 197
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 198
  ]
  edge [
    source 26
    target 128
  ]
  edge [
    source 26
    target 199
  ]
  edge [
    source 26
    target 200
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 201
  ]
  edge [
    source 27
    target 202
  ]
  edge [
    source 27
    target 203
  ]
  edge [
    source 27
    target 204
  ]
  edge [
    source 27
    target 205
  ]
  edge [
    source 27
    target 206
  ]
  edge [
    source 27
    target 207
  ]
  edge [
    source 27
    target 208
  ]
  edge [
    source 27
    target 209
  ]
  edge [
    source 27
    target 210
  ]
  edge [
    source 27
    target 211
  ]
  edge [
    source 27
    target 212
  ]
  edge [
    source 27
    target 191
  ]
  edge [
    source 27
    target 213
  ]
  edge [
    source 27
    target 214
  ]
  edge [
    source 27
    target 215
  ]
  edge [
    source 27
    target 128
  ]
  edge [
    source 27
    target 216
  ]
  edge [
    source 27
    target 217
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 31
    target 218
  ]
  edge [
    source 31
    target 219
  ]
  edge [
    source 31
    target 220
  ]
  edge [
    source 31
    target 221
  ]
  edge [
    source 31
    target 222
  ]
  edge [
    source 31
    target 223
  ]
  edge [
    source 31
    target 224
  ]
  edge [
    source 31
    target 225
  ]
  edge [
    source 31
    target 226
  ]
  edge [
    source 31
    target 227
  ]
  edge [
    source 31
    target 228
  ]
  edge [
    source 31
    target 229
  ]
  edge [
    source 31
    target 230
  ]
  edge [
    source 31
    target 231
  ]
  edge [
    source 31
    target 232
  ]
  edge [
    source 31
    target 233
  ]
  edge [
    source 31
    target 234
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 235
  ]
  edge [
    source 33
    target 236
  ]
  edge [
    source 33
    target 237
  ]
  edge [
    source 33
    target 238
  ]
  edge [
    source 33
    target 239
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 240
  ]
  edge [
    source 34
    target 241
  ]
  edge [
    source 34
    target 242
  ]
  edge [
    source 34
    target 243
  ]
  edge [
    source 34
    target 244
  ]
  edge [
    source 34
    target 245
  ]
  edge [
    source 34
    target 246
  ]
  edge [
    source 34
    target 247
  ]
  edge [
    source 34
    target 248
  ]
  edge [
    source 34
    target 249
  ]
  edge [
    source 34
    target 250
  ]
  edge [
    source 34
    target 251
  ]
  edge [
    source 34
    target 252
  ]
  edge [
    source 34
    target 253
  ]
  edge [
    source 34
    target 254
  ]
  edge [
    source 34
    target 255
  ]
  edge [
    source 34
    target 256
  ]
  edge [
    source 34
    target 257
  ]
  edge [
    source 34
    target 258
  ]
  edge [
    source 34
    target 259
  ]
  edge [
    source 34
    target 260
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 261
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 262
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 263
  ]
  edge [
    source 38
    target 264
  ]
  edge [
    source 38
    target 265
  ]
  edge [
    source 38
    target 266
  ]
  edge [
    source 38
    target 267
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 268
  ]
  edge [
    source 39
    target 269
  ]
  edge [
    source 39
    target 270
  ]
  edge [
    source 39
    target 271
  ]
  edge [
    source 39
    target 272
  ]
  edge [
    source 39
    target 273
  ]
  edge [
    source 39
    target 274
  ]
  edge [
    source 39
    target 275
  ]
  edge [
    source 39
    target 276
  ]
  edge [
    source 39
    target 277
  ]
  edge [
    source 39
    target 278
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 279
  ]
  edge [
    source 40
    target 280
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 281
  ]
  edge [
    source 41
    target 282
  ]
  edge [
    source 41
    target 283
  ]
  edge [
    source 41
    target 284
  ]
  edge [
    source 41
    target 239
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 285
  ]
  edge [
    source 42
    target 286
  ]
  edge [
    source 42
    target 287
  ]
  edge [
    source 42
    target 288
  ]
  edge [
    source 42
    target 289
  ]
  edge [
    source 42
    target 124
  ]
  edge [
    source 42
    target 290
  ]
  edge [
    source 42
    target 291
  ]
  edge [
    source 42
    target 292
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 293
  ]
  edge [
    source 43
    target 294
  ]
  edge [
    source 43
    target 295
  ]
  edge [
    source 43
    target 296
  ]
  edge [
    source 43
    target 297
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 73
  ]
  edge [
    source 44
    target 298
  ]
  edge [
    source 44
    target 299
  ]
  edge [
    source 44
    target 300
  ]
  edge [
    source 44
    target 301
  ]
  edge [
    source 44
    target 302
  ]
  edge [
    source 44
    target 303
  ]
  edge [
    source 44
    target 304
  ]
  edge [
    source 44
    target 305
  ]
  edge [
    source 44
    target 306
  ]
  edge [
    source 44
    target 307
  ]
  edge [
    source 44
    target 308
  ]
  edge [
    source 44
    target 309
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 310
  ]
  edge [
    source 46
    target 311
  ]
  edge [
    source 46
    target 312
  ]
  edge [
    source 46
    target 313
  ]
  edge [
    source 46
    target 314
  ]
  edge [
    source 46
    target 315
  ]
  edge [
    source 46
    target 316
  ]
  edge [
    source 46
    target 317
  ]
  edge [
    source 46
    target 318
  ]
  edge [
    source 46
    target 319
  ]
  edge [
    source 46
    target 320
  ]
  edge [
    source 46
    target 321
  ]
  edge [
    source 46
    target 322
  ]
  edge [
    source 46
    target 323
  ]
  edge [
    source 46
    target 324
  ]
  edge [
    source 46
    target 325
  ]
  edge [
    source 46
    target 326
  ]
  edge [
    source 46
    target 327
  ]
  edge [
    source 46
    target 328
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 329
  ]
  edge [
    source 47
    target 330
  ]
  edge [
    source 47
    target 331
  ]
  edge [
    source 47
    target 332
  ]
  edge [
    source 47
    target 108
  ]
  edge [
    source 47
    target 202
  ]
  edge [
    source 47
    target 333
  ]
  edge [
    source 47
    target 334
  ]
  edge [
    source 47
    target 335
  ]
  edge [
    source 47
    target 336
  ]
  edge [
    source 47
    target 337
  ]
  edge [
    source 47
    target 214
  ]
  edge [
    source 47
    target 338
  ]
  edge [
    source 47
    target 339
  ]
  edge [
    source 47
    target 127
  ]
  edge [
    source 47
    target 128
  ]
  edge [
    source 47
    target 340
  ]
  edge [
    source 47
    target 341
  ]
  edge [
    source 47
    target 342
  ]
  edge [
    source 47
    target 343
  ]
  edge [
    source 47
    target 344
  ]
  edge [
    source 47
    target 345
  ]
  edge [
    source 47
    target 346
  ]
  edge [
    source 47
    target 347
  ]
  edge [
    source 47
    target 348
  ]
  edge [
    source 47
    target 349
  ]
  edge [
    source 47
    target 350
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 98
  ]
  edge [
    source 48
    target 351
  ]
  edge [
    source 48
    target 352
  ]
  edge [
    source 48
    target 353
  ]
  edge [
    source 48
    target 354
  ]
  edge [
    source 48
    target 355
  ]
  edge [
    source 48
    target 356
  ]
  edge [
    source 48
    target 357
  ]
  edge [
    source 48
    target 358
  ]
  edge [
    source 48
    target 359
  ]
  edge [
    source 48
    target 360
  ]
  edge [
    source 48
    target 361
  ]
  edge [
    source 48
    target 362
  ]
  edge [
    source 48
    target 363
  ]
  edge [
    source 48
    target 364
  ]
  edge [
    source 48
    target 365
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 366
  ]
  edge [
    source 49
    target 367
  ]
  edge [
    source 49
    target 368
  ]
  edge [
    source 49
    target 369
  ]
  edge [
    source 49
    target 370
  ]
  edge [
    source 49
    target 371
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 372
  ]
  edge [
    source 50
    target 373
  ]
  edge [
    source 50
    target 374
  ]
  edge [
    source 50
    target 375
  ]
  edge [
    source 50
    target 61
  ]
  edge [
    source 50
    target 376
  ]
  edge [
    source 50
    target 377
  ]
  edge [
    source 50
    target 378
  ]
  edge [
    source 50
    target 379
  ]
  edge [
    source 50
    target 380
  ]
  edge [
    source 50
    target 381
  ]
  edge [
    source 50
    target 382
  ]
  edge [
    source 50
    target 383
  ]
  edge [
    source 51
    target 384
  ]
  edge [
    source 51
    target 385
  ]
  edge [
    source 51
    target 110
  ]
  edge [
    source 51
    target 386
  ]
  edge [
    source 51
    target 387
  ]
  edge [
    source 51
    target 388
  ]
  edge [
    source 51
    target 389
  ]
  edge [
    source 51
    target 390
  ]
  edge [
    source 51
    target 391
  ]
  edge [
    source 51
    target 392
  ]
  edge [
    source 51
    target 393
  ]
  edge [
    source 51
    target 394
  ]
  edge [
    source 51
    target 395
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 396
  ]
  edge [
    source 52
    target 397
  ]
  edge [
    source 52
    target 398
  ]
  edge [
    source 52
    target 399
  ]
  edge [
    source 52
    target 400
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 401
  ]
  edge [
    source 53
    target 402
  ]
  edge [
    source 53
    target 403
  ]
  edge [
    source 53
    target 404
  ]
  edge [
    source 53
    target 405
  ]
  edge [
    source 53
    target 127
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 98
  ]
  edge [
    source 54
    target 406
  ]
  edge [
    source 54
    target 407
  ]
  edge [
    source 54
    target 408
  ]
  edge [
    source 54
    target 409
  ]
  edge [
    source 55
    target 312
  ]
  edge [
    source 55
    target 410
  ]
  edge [
    source 55
    target 313
  ]
  edge [
    source 55
    target 314
  ]
  edge [
    source 55
    target 316
  ]
  edge [
    source 55
    target 321
  ]
  edge [
    source 55
    target 320
  ]
  edge [
    source 55
    target 411
  ]
  edge [
    source 55
    target 412
  ]
  edge [
    source 55
    target 413
  ]
  edge [
    source 55
    target 322
  ]
  edge [
    source 55
    target 414
  ]
  edge [
    source 55
    target 323
  ]
  edge [
    source 55
    target 317
  ]
]
