graph [
  maxDegree 38
  minDegree 1
  meanDegree 2
  density 0.018691588785046728
  graphCliqueNumber 2
  node [
    id 0
    label "dzienia"
    origin "text"
  ]
  node [
    id 1
    label "dobry"
    origin "text"
  ]
  node [
    id 2
    label "prosi&#263;"
    origin "text"
  ]
  node [
    id 3
    label "pan"
    origin "text"
  ]
  node [
    id 4
    label "karolin"
    origin "text"
  ]
  node [
    id 5
    label "tutaj"
    origin "text"
  ]
  node [
    id 6
    label "&#347;wi&#281;tokrzyski"
    origin "text"
  ]
  node [
    id 7
    label "sto"
    origin "text"
  ]
  node [
    id 8
    label "pi&#281;&#263;dziesi&#261;t"
    origin "text"
  ]
  node [
    id 9
    label "pi&#281;&#263;"
    origin "text"
  ]
  node [
    id 10
    label "jecha&#263;"
    origin "text"
  ]
  node [
    id 11
    label "prawda"
    origin "text"
  ]
  node [
    id 12
    label "pomy&#347;lny"
  ]
  node [
    id 13
    label "skuteczny"
  ]
  node [
    id 14
    label "moralny"
  ]
  node [
    id 15
    label "korzystny"
  ]
  node [
    id 16
    label "odpowiedni"
  ]
  node [
    id 17
    label "zwrot"
  ]
  node [
    id 18
    label "dobrze"
  ]
  node [
    id 19
    label "pozytywny"
  ]
  node [
    id 20
    label "grzeczny"
  ]
  node [
    id 21
    label "powitanie"
  ]
  node [
    id 22
    label "mi&#322;y"
  ]
  node [
    id 23
    label "dobroczynny"
  ]
  node [
    id 24
    label "pos&#322;uszny"
  ]
  node [
    id 25
    label "ca&#322;y"
  ]
  node [
    id 26
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 27
    label "czw&#243;rka"
  ]
  node [
    id 28
    label "spokojny"
  ]
  node [
    id 29
    label "&#347;mieszny"
  ]
  node [
    id 30
    label "drogi"
  ]
  node [
    id 31
    label "trwa&#263;"
  ]
  node [
    id 32
    label "zezwala&#263;"
  ]
  node [
    id 33
    label "ask"
  ]
  node [
    id 34
    label "invite"
  ]
  node [
    id 35
    label "zach&#281;ca&#263;"
  ]
  node [
    id 36
    label "preach"
  ]
  node [
    id 37
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 38
    label "pies"
  ]
  node [
    id 39
    label "ujmowa&#263;_si&#281;"
  ]
  node [
    id 40
    label "poleca&#263;"
  ]
  node [
    id 41
    label "zaprasza&#263;"
  ]
  node [
    id 42
    label "suffice"
  ]
  node [
    id 43
    label "dziwi&#263;_si&#281;"
  ]
  node [
    id 44
    label "cz&#322;owiek"
  ]
  node [
    id 45
    label "profesor"
  ]
  node [
    id 46
    label "kszta&#322;ciciel"
  ]
  node [
    id 47
    label "jegomo&#347;&#263;"
  ]
  node [
    id 48
    label "pracodawca"
  ]
  node [
    id 49
    label "rz&#261;dzenie"
  ]
  node [
    id 50
    label "m&#261;&#380;"
  ]
  node [
    id 51
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 52
    label "ch&#322;opina"
  ]
  node [
    id 53
    label "bratek"
  ]
  node [
    id 54
    label "opiekun"
  ]
  node [
    id 55
    label "doros&#322;y"
  ]
  node [
    id 56
    label "preceptor"
  ]
  node [
    id 57
    label "Midas"
  ]
  node [
    id 58
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 59
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 60
    label "murza"
  ]
  node [
    id 61
    label "ojciec"
  ]
  node [
    id 62
    label "androlog"
  ]
  node [
    id 63
    label "pupil"
  ]
  node [
    id 64
    label "efendi"
  ]
  node [
    id 65
    label "nabab"
  ]
  node [
    id 66
    label "w&#322;odarz"
  ]
  node [
    id 67
    label "szkolnik"
  ]
  node [
    id 68
    label "pedagog"
  ]
  node [
    id 69
    label "popularyzator"
  ]
  node [
    id 70
    label "andropauza"
  ]
  node [
    id 71
    label "gra_w_karty"
  ]
  node [
    id 72
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 73
    label "Mieszko_I"
  ]
  node [
    id 74
    label "bogaty"
  ]
  node [
    id 75
    label "samiec"
  ]
  node [
    id 76
    label "przyw&#243;dca"
  ]
  node [
    id 77
    label "pa&#324;stwo"
  ]
  node [
    id 78
    label "belfer"
  ]
  node [
    id 79
    label "tam"
  ]
  node [
    id 80
    label "famu&#322;a"
  ]
  node [
    id 81
    label "proceed"
  ]
  node [
    id 82
    label "napada&#263;"
  ]
  node [
    id 83
    label "wyz&#322;o&#347;liwia&#263;_si&#281;"
  ]
  node [
    id 84
    label "wy&#347;miewa&#263;"
  ]
  node [
    id 85
    label "wykonywa&#263;"
  ]
  node [
    id 86
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 87
    label "czu&#263;"
  ]
  node [
    id 88
    label "overdrive"
  ]
  node [
    id 89
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 90
    label "ride"
  ]
  node [
    id 91
    label "korzysta&#263;"
  ]
  node [
    id 92
    label "go"
  ]
  node [
    id 93
    label "prowadzi&#263;"
  ]
  node [
    id 94
    label "continue"
  ]
  node [
    id 95
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 96
    label "drive"
  ]
  node [
    id 97
    label "kontynuowa&#263;"
  ]
  node [
    id 98
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 99
    label "odbywa&#263;"
  ]
  node [
    id 100
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 101
    label "carry"
  ]
  node [
    id 102
    label "s&#261;d"
  ]
  node [
    id 103
    label "truth"
  ]
  node [
    id 104
    label "nieprawdziwy"
  ]
  node [
    id 105
    label "za&#322;o&#380;enie"
  ]
  node [
    id 106
    label "prawdziwy"
  ]
  node [
    id 107
    label "realia"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 11
    target 102
  ]
  edge [
    source 11
    target 103
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 11
    target 107
  ]
]
