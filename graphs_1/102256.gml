graph [
  maxDegree 52
  minDegree 1
  meanDegree 2.3316782522343593
  density 0.0023177716224993634
  graphCliqueNumber 3
  node [
    id 0
    label "edwin"
    origin "text"
  ]
  node [
    id 1
    label "bendyk"
    origin "text"
  ]
  node [
    id 2
    label "opisywa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "copyright"
    origin "text"
  ]
  node [
    id 4
    label "summit"
    origin "text"
  ]
  node [
    id 5
    label "bruksela"
    origin "text"
  ]
  node [
    id 6
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 7
    label "przedstawiciel"
    origin "text"
  ]
  node [
    id 8
    label "przemys&#322;"
    origin "text"
  ]
  node [
    id 9
    label "wyrazi&#263;"
    origin "text"
  ]
  node [
    id 10
    label "obawa"
    origin "text"
  ]
  node [
    id 11
    label "zwi&#261;zek"
    origin "text"
  ]
  node [
    id 12
    label "istnienie"
    origin "text"
  ]
  node [
    id 13
    label "creative"
    origin "text"
  ]
  node [
    id 14
    label "commons"
    origin "text"
  ]
  node [
    id 15
    label "ciekawy"
    origin "text"
  ]
  node [
    id 16
    label "by&#263;"
    origin "text"
  ]
  node [
    id 17
    label "uzasadnienie"
    origin "text"
  ]
  node [
    id 18
    label "cytowa&#263;"
    origin "text"
  ]
  node [
    id 19
    label "erik"
    origin "text"
  ]
  node [
    id 20
    label "baptista"
    origin "text"
  ]
  node [
    id 21
    label "cisac"
    origin "text"
  ]
  node [
    id 22
    label "moja"
    origin "text"
  ]
  node [
    id 23
    label "uwaga"
    origin "text"
  ]
  node [
    id 24
    label "zwr&#243;ci&#263;"
    origin "text"
  ]
  node [
    id 25
    label "nast&#281;puj&#261;cy"
    origin "text"
  ]
  node [
    id 26
    label "zdanie"
    origin "text"
  ]
  node [
    id 27
    label "gdy"
    origin "text"
  ]
  node [
    id 28
    label "autor"
    origin "text"
  ]
  node [
    id 29
    label "zdecydowa&#263;"
    origin "text"
  ]
  node [
    id 30
    label "si&#281;"
    origin "text"
  ]
  node [
    id 31
    label "raz"
    origin "text"
  ]
  node [
    id 32
    label "og&#322;osi&#263;"
    origin "text"
  ]
  node [
    id 33
    label "utw&#243;r"
    origin "text"
  ]
  node [
    id 34
    label "rama"
    origin "text"
  ]
  node [
    id 35
    label "licencja"
    origin "text"
  ]
  node [
    id 36
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 37
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 38
    label "p&#243;&#378;no"
    origin "text"
  ]
  node [
    id 39
    label "zmieni&#263;"
    origin "text"
  ]
  node [
    id 40
    label "status"
    origin "text"
  ]
  node [
    id 41
    label "nawet"
    origin "text"
  ]
  node [
    id 42
    label "cho&#263;by"
    origin "text"
  ]
  node [
    id 43
    label "chcie&#263;"
    origin "text"
  ]
  node [
    id 44
    label "stan"
    origin "text"
  ]
  node [
    id 45
    label "chcie&#263;by"
    origin "text"
  ]
  node [
    id 46
    label "dobrze"
    origin "text"
  ]
  node [
    id 47
    label "pilnowa&#263;"
    origin "text"
  ]
  node [
    id 48
    label "integralno&#347;&#263;"
    origin "text"
  ]
  node [
    id 49
    label "sw&#243;j"
    origin "text"
  ]
  node [
    id 50
    label "tw&#243;rczo&#347;&#263;"
    origin "text"
  ]
  node [
    id 51
    label "pomija&#263;"
    origin "text"
  ]
  node [
    id 52
    label "w&#261;tpliwy"
    origin "text"
  ]
  node [
    id 53
    label "meritum"
    origin "text"
  ]
  node [
    id 54
    label "ten"
    origin "text"
  ]
  node [
    id 55
    label "opinia"
    origin "text"
  ]
  node [
    id 56
    label "stosowa&#263;"
    origin "text"
  ]
  node [
    id 57
    label "forma"
    origin "text"
  ]
  node [
    id 58
    label "typowy"
    origin "text"
  ]
  node [
    id 59
    label "dla"
    origin "text"
  ]
  node [
    id 60
    label "producent"
    origin "text"
  ]
  node [
    id 61
    label "organizacja"
    origin "text"
  ]
  node [
    id 62
    label "zarz&#261;dzanie"
    origin "text"
  ]
  node [
    id 63
    label "prawo"
    origin "text"
  ]
  node [
    id 64
    label "dlaczego"
    origin "text"
  ]
  node [
    id 65
    label "powiedzie&#263;"
    origin "text"
  ]
  node [
    id 66
    label "przyk&#322;ad"
    origin "text"
  ]
  node [
    id 67
    label "tak"
    origin "text"
  ]
  node [
    id 68
    label "konkurencja"
    origin "text"
  ]
  node [
    id 69
    label "tradycyjny"
    origin "text"
  ]
  node [
    id 70
    label "model"
    origin "text"
  ]
  node [
    id 71
    label "prawa"
    origin "text"
  ]
  node [
    id 72
    label "autorski"
    origin "text"
  ]
  node [
    id 73
    label "bra&#263;"
    origin "text"
  ]
  node [
    id 74
    label "sprawa"
    origin "text"
  ]
  node [
    id 75
    label "swoje"
    origin "text"
  ]
  node [
    id 76
    label "r&#281;ka"
    origin "text"
  ]
  node [
    id 77
    label "rezygnowa&#263;"
    origin "text"
  ]
  node [
    id 78
    label "po&#347;rednictwo"
    origin "text"
  ]
  node [
    id 79
    label "wp&#322;ywa&#263;"
    origin "text"
  ]
  node [
    id 80
    label "spadek"
    origin "text"
  ]
  node [
    id 81
    label "nasi"
    origin "text"
  ]
  node [
    id 82
    label "doch&#243;d"
    origin "text"
  ]
  node [
    id 83
    label "zmusza&#263;"
    origin "text"
  ]
  node [
    id 84
    label "nasa"
    origin "text"
  ]
  node [
    id 85
    label "szuka&#263;"
    origin "text"
  ]
  node [
    id 86
    label "nowa"
    origin "text"
  ]
  node [
    id 87
    label "biznesowy"
    origin "text"
  ]
  node [
    id 88
    label "lobbowa&#263;"
    origin "text"
  ]
  node [
    id 89
    label "zmiana"
    origin "text"
  ]
  node [
    id 90
    label "prawie"
    origin "text"
  ]
  node [
    id 91
    label "niestety"
    origin "text"
  ]
  node [
    id 92
    label "pokazywa&#263;"
    origin "text"
  ]
  node [
    id 93
    label "tylko"
    origin "text"
  ]
  node [
    id 94
    label "przypadek"
    origin "text"
  ]
  node [
    id 95
    label "oprogramowanie"
    origin "text"
  ]
  node [
    id 96
    label "tw&#243;rca"
    origin "text"
  ]
  node [
    id 97
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 98
    label "czerpa&#263;"
    origin "text"
  ]
  node [
    id 99
    label "dzia&#322;ania"
    origin "text"
  ]
  node [
    id 100
    label "inny"
    origin "text"
  ]
  node [
    id 101
    label "ni&#380;"
    origin "text"
  ]
  node [
    id 102
    label "sprzeda&#380;"
    origin "text"
  ]
  node [
    id 103
    label "koncert"
    origin "text"
  ]
  node [
    id 104
    label "dobrowolny"
    origin "text"
  ]
  node [
    id 105
    label "wp&#322;ata"
    origin "text"
  ]
  node [
    id 106
    label "naprawd&#281;"
    origin "text"
  ]
  node [
    id 107
    label "realny"
    origin "text"
  ]
  node [
    id 108
    label "zagro&#380;enie"
    origin "text"
  ]
  node [
    id 109
    label "taki"
    origin "text"
  ]
  node [
    id 110
    label "cora"
    origin "text"
  ]
  node [
    id 111
    label "doctorow"
    origin "text"
  ]
  node [
    id 112
    label "chwali&#263;"
    origin "text"
  ]
  node [
    id 113
    label "mimo"
    origin "text"
  ]
  node [
    id 114
    label "udost&#281;pnia&#263;"
    origin "text"
  ]
  node [
    id 115
    label "legalnie"
    origin "text"
  ]
  node [
    id 116
    label "ksi&#261;&#380;ka"
    origin "text"
  ]
  node [
    id 117
    label "internet"
    origin "text"
  ]
  node [
    id 118
    label "drukowa&#263;"
    origin "text"
  ]
  node [
    id 119
    label "sz&#243;sta"
    origin "text"
  ]
  node [
    id 120
    label "wydanie"
    origin "text"
  ]
  node [
    id 121
    label "ale"
    origin "text"
  ]
  node [
    id 122
    label "oczywi&#347;cie"
    origin "text"
  ]
  node [
    id 123
    label "biedny"
    origin "text"
  ]
  node [
    id 124
    label "wiedzie&#263;"
    origin "text"
  ]
  node [
    id 125
    label "czyni&#263;"
    origin "text"
  ]
  node [
    id 126
    label "zmusi&#263;"
    origin "text"
  ]
  node [
    id 127
    label "przez"
    origin "text"
  ]
  node [
    id 128
    label "lessigist&#243;w"
    origin "text"
  ]
  node [
    id 129
    label "podpisywa&#263;"
    origin "text"
  ]
  node [
    id 130
    label "cyrograf"
    origin "text"
  ]
  node [
    id 131
    label "koniec"
    origin "text"
  ]
  node [
    id 132
    label "&#380;ycie"
    origin "text"
  ]
  node [
    id 133
    label "musia&#322;"
    origin "text"
  ]
  node [
    id 134
    label "tworzy&#263;"
    origin "text"
  ]
  node [
    id 135
    label "pirat"
    origin "text"
  ]
  node [
    id 136
    label "tymczasem"
    origin "text"
  ]
  node [
    id 137
    label "czeka&#263;"
    origin "text"
  ]
  node [
    id 138
    label "dok&#322;adnie"
    origin "text"
  ]
  node [
    id 139
    label "wyt&#322;umaczy&#263;"
    origin "text"
  ]
  node [
    id 140
    label "bez"
    origin "text"
  ]
  node [
    id 141
    label "tani"
    origin "text"
  ]
  node [
    id 142
    label "sztuczka"
    origin "text"
  ]
  node [
    id 143
    label "zapoznawa&#263;"
  ]
  node [
    id 144
    label "represent"
  ]
  node [
    id 145
    label "cz&#322;owiek"
  ]
  node [
    id 146
    label "cz&#322;onek"
  ]
  node [
    id 147
    label "substytuowanie"
  ]
  node [
    id 148
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 149
    label "zast&#281;pca"
  ]
  node [
    id 150
    label "substytuowa&#263;"
  ]
  node [
    id 151
    label "gospodarka"
  ]
  node [
    id 152
    label "przechowalnictwo"
  ]
  node [
    id 153
    label "uprzemys&#322;owienie"
  ]
  node [
    id 154
    label "przemys&#322;&#243;wka"
  ]
  node [
    id 155
    label "uprzemys&#322;owienie_si&#281;"
  ]
  node [
    id 156
    label "uprzemys&#322;awianie_si&#281;"
  ]
  node [
    id 157
    label "uprzemys&#322;awianie"
  ]
  node [
    id 158
    label "zdradzi&#263;_si&#281;"
  ]
  node [
    id 159
    label "zakomunikowa&#263;"
  ]
  node [
    id 160
    label "vent"
  ]
  node [
    id 161
    label "oznaczy&#263;"
  ]
  node [
    id 162
    label "testify"
  ]
  node [
    id 163
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 164
    label "emocja"
  ]
  node [
    id 165
    label "w&#261;tpliwo&#347;&#263;"
  ]
  node [
    id 166
    label "akatyzja"
  ]
  node [
    id 167
    label "niespokojno&#347;&#263;"
  ]
  node [
    id 168
    label "odwodnienie"
  ]
  node [
    id 169
    label "konstytucja"
  ]
  node [
    id 170
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 171
    label "substancja_chemiczna"
  ]
  node [
    id 172
    label "bratnia_dusza"
  ]
  node [
    id 173
    label "zwi&#261;zanie"
  ]
  node [
    id 174
    label "lokant"
  ]
  node [
    id 175
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 176
    label "zwi&#261;za&#263;"
  ]
  node [
    id 177
    label "odwadnia&#263;"
  ]
  node [
    id 178
    label "marriage"
  ]
  node [
    id 179
    label "marketing_afiliacyjny"
  ]
  node [
    id 180
    label "bearing"
  ]
  node [
    id 181
    label "wi&#261;zanie"
  ]
  node [
    id 182
    label "odwadnianie"
  ]
  node [
    id 183
    label "koligacja"
  ]
  node [
    id 184
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 185
    label "odwodni&#263;"
  ]
  node [
    id 186
    label "azeotrop"
  ]
  node [
    id 187
    label "powi&#261;zanie"
  ]
  node [
    id 188
    label "produkowanie"
  ]
  node [
    id 189
    label "byt"
  ]
  node [
    id 190
    label "robienie"
  ]
  node [
    id 191
    label "utrzymywanie"
  ]
  node [
    id 192
    label "bycie"
  ]
  node [
    id 193
    label "utrzymywa&#263;"
  ]
  node [
    id 194
    label "znikni&#281;cie"
  ]
  node [
    id 195
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 196
    label "urzeczywistnianie"
  ]
  node [
    id 197
    label "entity"
  ]
  node [
    id 198
    label "egzystencja"
  ]
  node [
    id 199
    label "wyprodukowanie"
  ]
  node [
    id 200
    label "dematerializowanie_si&#281;"
  ]
  node [
    id 201
    label "utrzymanie"
  ]
  node [
    id 202
    label "utrzyma&#263;"
  ]
  node [
    id 203
    label "being"
  ]
  node [
    id 204
    label "swoisty"
  ]
  node [
    id 205
    label "interesowanie"
  ]
  node [
    id 206
    label "nietuzinkowy"
  ]
  node [
    id 207
    label "ciekawie"
  ]
  node [
    id 208
    label "indagator"
  ]
  node [
    id 209
    label "interesuj&#261;cy"
  ]
  node [
    id 210
    label "dziwny"
  ]
  node [
    id 211
    label "intryguj&#261;cy"
  ]
  node [
    id 212
    label "ch&#281;tny"
  ]
  node [
    id 213
    label "si&#281;ga&#263;"
  ]
  node [
    id 214
    label "trwa&#263;"
  ]
  node [
    id 215
    label "obecno&#347;&#263;"
  ]
  node [
    id 216
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 217
    label "stand"
  ]
  node [
    id 218
    label "mie&#263;_miejsce"
  ]
  node [
    id 219
    label "uczestniczy&#263;"
  ]
  node [
    id 220
    label "chodzi&#263;"
  ]
  node [
    id 221
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 222
    label "equal"
  ]
  node [
    id 223
    label "justyfikacja"
  ]
  node [
    id 224
    label "apologetyk"
  ]
  node [
    id 225
    label "informacja"
  ]
  node [
    id 226
    label "gossip"
  ]
  node [
    id 227
    label "wyja&#347;nienie"
  ]
  node [
    id 228
    label "wymienia&#263;"
  ]
  node [
    id 229
    label "quote"
  ]
  node [
    id 230
    label "przytacza&#263;"
  ]
  node [
    id 231
    label "nagana"
  ]
  node [
    id 232
    label "wypowied&#378;"
  ]
  node [
    id 233
    label "dzienniczek"
  ]
  node [
    id 234
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 235
    label "wzgl&#261;d"
  ]
  node [
    id 236
    label "upomnienie"
  ]
  node [
    id 237
    label "tekst"
  ]
  node [
    id 238
    label "return"
  ]
  node [
    id 239
    label "rzygn&#261;&#263;"
  ]
  node [
    id 240
    label "shaftowa&#263;_si&#281;"
  ]
  node [
    id 241
    label "wydali&#263;"
  ]
  node [
    id 242
    label "direct"
  ]
  node [
    id 243
    label "zespawa&#263;_si&#281;"
  ]
  node [
    id 244
    label "przeznaczy&#263;"
  ]
  node [
    id 245
    label "give"
  ]
  node [
    id 246
    label "ustawi&#263;"
  ]
  node [
    id 247
    label "przekaza&#263;"
  ]
  node [
    id 248
    label "regenerate"
  ]
  node [
    id 249
    label "z_powrotem"
  ]
  node [
    id 250
    label "set"
  ]
  node [
    id 251
    label "okre&#347;lony"
  ]
  node [
    id 252
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 253
    label "attitude"
  ]
  node [
    id 254
    label "system"
  ]
  node [
    id 255
    label "przedstawienie"
  ]
  node [
    id 256
    label "fraza"
  ]
  node [
    id 257
    label "prison_term"
  ]
  node [
    id 258
    label "adjudication"
  ]
  node [
    id 259
    label "przekazanie"
  ]
  node [
    id 260
    label "pass"
  ]
  node [
    id 261
    label "wyra&#380;enie"
  ]
  node [
    id 262
    label "okres"
  ]
  node [
    id 263
    label "z&#322;o&#380;enie_egzaminu"
  ]
  node [
    id 264
    label "wypowiedzenie"
  ]
  node [
    id 265
    label "konektyw"
  ]
  node [
    id 266
    label "zaliczenie"
  ]
  node [
    id 267
    label "stanowisko"
  ]
  node [
    id 268
    label "powierzenie"
  ]
  node [
    id 269
    label "antylogizm"
  ]
  node [
    id 270
    label "zmuszenie"
  ]
  node [
    id 271
    label "szko&#322;a"
  ]
  node [
    id 272
    label "pomys&#322;odawca"
  ]
  node [
    id 273
    label "kszta&#322;ciciel"
  ]
  node [
    id 274
    label "tworzyciel"
  ]
  node [
    id 275
    label "&#347;w"
  ]
  node [
    id 276
    label "wykonawca"
  ]
  node [
    id 277
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 278
    label "sta&#263;_si&#281;"
  ]
  node [
    id 279
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 280
    label "podj&#261;&#263;"
  ]
  node [
    id 281
    label "determine"
  ]
  node [
    id 282
    label "decide"
  ]
  node [
    id 283
    label "zrobi&#263;"
  ]
  node [
    id 284
    label "chwila"
  ]
  node [
    id 285
    label "uderzenie"
  ]
  node [
    id 286
    label "cios"
  ]
  node [
    id 287
    label "time"
  ]
  node [
    id 288
    label "communicate"
  ]
  node [
    id 289
    label "opublikowa&#263;"
  ]
  node [
    id 290
    label "obwo&#322;a&#263;"
  ]
  node [
    id 291
    label "poda&#263;"
  ]
  node [
    id 292
    label "publish"
  ]
  node [
    id 293
    label "declare"
  ]
  node [
    id 294
    label "tre&#347;&#263;"
  ]
  node [
    id 295
    label "obrazowanie"
  ]
  node [
    id 296
    label "part"
  ]
  node [
    id 297
    label "organ"
  ]
  node [
    id 298
    label "komunikat"
  ]
  node [
    id 299
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 300
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 301
    label "element_anatomiczny"
  ]
  node [
    id 302
    label "zakres"
  ]
  node [
    id 303
    label "dodatek"
  ]
  node [
    id 304
    label "struktura"
  ]
  node [
    id 305
    label "stela&#380;"
  ]
  node [
    id 306
    label "za&#322;o&#380;enie"
  ]
  node [
    id 307
    label "human_body"
  ]
  node [
    id 308
    label "szablon"
  ]
  node [
    id 309
    label "oprawa"
  ]
  node [
    id 310
    label "paczka"
  ]
  node [
    id 311
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 312
    label "obramowanie"
  ]
  node [
    id 313
    label "pojazd"
  ]
  node [
    id 314
    label "postawa"
  ]
  node [
    id 315
    label "element_konstrukcyjny"
  ]
  node [
    id 316
    label "licencjonowa&#263;"
  ]
  node [
    id 317
    label "pozwolenie"
  ]
  node [
    id 318
    label "hodowla"
  ]
  node [
    id 319
    label "rasowy"
  ]
  node [
    id 320
    label "license"
  ]
  node [
    id 321
    label "zezwolenie"
  ]
  node [
    id 322
    label "za&#347;wiadczenie"
  ]
  node [
    id 323
    label "p&#243;&#378;ny"
  ]
  node [
    id 324
    label "come_up"
  ]
  node [
    id 325
    label "straci&#263;"
  ]
  node [
    id 326
    label "przej&#347;&#263;"
  ]
  node [
    id 327
    label "zast&#261;pi&#263;"
  ]
  node [
    id 328
    label "sprawi&#263;"
  ]
  node [
    id 329
    label "zyska&#263;"
  ]
  node [
    id 330
    label "change"
  ]
  node [
    id 331
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 332
    label "znaczenie"
  ]
  node [
    id 333
    label "awans"
  ]
  node [
    id 334
    label "awansowanie"
  ]
  node [
    id 335
    label "awansowa&#263;"
  ]
  node [
    id 336
    label "condition"
  ]
  node [
    id 337
    label "podmiotowo"
  ]
  node [
    id 338
    label "sytuacja"
  ]
  node [
    id 339
    label "czu&#263;"
  ]
  node [
    id 340
    label "desire"
  ]
  node [
    id 341
    label "kcie&#263;"
  ]
  node [
    id 342
    label "Arizona"
  ]
  node [
    id 343
    label "Georgia"
  ]
  node [
    id 344
    label "warstwa"
  ]
  node [
    id 345
    label "jednostka_administracyjna"
  ]
  node [
    id 346
    label "Goa"
  ]
  node [
    id 347
    label "Hawaje"
  ]
  node [
    id 348
    label "Floryda"
  ]
  node [
    id 349
    label "Oklahoma"
  ]
  node [
    id 350
    label "punkt"
  ]
  node [
    id 351
    label "Alaska"
  ]
  node [
    id 352
    label "Alabama"
  ]
  node [
    id 353
    label "wci&#281;cie"
  ]
  node [
    id 354
    label "Oregon"
  ]
  node [
    id 355
    label "poziom"
  ]
  node [
    id 356
    label "Teksas"
  ]
  node [
    id 357
    label "Illinois"
  ]
  node [
    id 358
    label "Jukatan"
  ]
  node [
    id 359
    label "Waszyngton"
  ]
  node [
    id 360
    label "shape"
  ]
  node [
    id 361
    label "Nowy_Meksyk"
  ]
  node [
    id 362
    label "ilo&#347;&#263;"
  ]
  node [
    id 363
    label "state"
  ]
  node [
    id 364
    label "Nowy_York"
  ]
  node [
    id 365
    label "Arakan"
  ]
  node [
    id 366
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 367
    label "Kalifornia"
  ]
  node [
    id 368
    label "wektor"
  ]
  node [
    id 369
    label "Massachusetts"
  ]
  node [
    id 370
    label "miejsce"
  ]
  node [
    id 371
    label "Pensylwania"
  ]
  node [
    id 372
    label "Maryland"
  ]
  node [
    id 373
    label "Michigan"
  ]
  node [
    id 374
    label "Ohio"
  ]
  node [
    id 375
    label "Kansas"
  ]
  node [
    id 376
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 377
    label "Luizjana"
  ]
  node [
    id 378
    label "samopoczucie"
  ]
  node [
    id 379
    label "Wirginia"
  ]
  node [
    id 380
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 381
    label "moralnie"
  ]
  node [
    id 382
    label "wiele"
  ]
  node [
    id 383
    label "lepiej"
  ]
  node [
    id 384
    label "korzystnie"
  ]
  node [
    id 385
    label "pomy&#347;lnie"
  ]
  node [
    id 386
    label "pozytywnie"
  ]
  node [
    id 387
    label "dobry"
  ]
  node [
    id 388
    label "dobroczynnie"
  ]
  node [
    id 389
    label "odpowiednio"
  ]
  node [
    id 390
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 391
    label "skutecznie"
  ]
  node [
    id 392
    label "zachowywa&#263;"
  ]
  node [
    id 393
    label "robi&#263;"
  ]
  node [
    id 394
    label "str&#243;&#380;y&#263;"
  ]
  node [
    id 395
    label "continue"
  ]
  node [
    id 396
    label "bli&#378;ni"
  ]
  node [
    id 397
    label "odpowiedni"
  ]
  node [
    id 398
    label "swojak"
  ]
  node [
    id 399
    label "samodzielny"
  ]
  node [
    id 400
    label "creation"
  ]
  node [
    id 401
    label "tworzenie"
  ]
  node [
    id 402
    label "kreacja"
  ]
  node [
    id 403
    label "dorobek"
  ]
  node [
    id 404
    label "kultura"
  ]
  node [
    id 405
    label "zbi&#243;r"
  ]
  node [
    id 406
    label "omija&#263;"
  ]
  node [
    id 407
    label "zbywa&#263;"
  ]
  node [
    id 408
    label "traktowa&#263;"
  ]
  node [
    id 409
    label "dotyczy&#263;"
  ]
  node [
    id 410
    label "omission"
  ]
  node [
    id 411
    label "pozorny"
  ]
  node [
    id 412
    label "w&#261;tpliwie"
  ]
  node [
    id 413
    label "dokument"
  ]
  node [
    id 414
    label "rehabilitowanie_si&#281;"
  ]
  node [
    id 415
    label "reputacja"
  ]
  node [
    id 416
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 417
    label "rehabilitowa&#263;_si&#281;"
  ]
  node [
    id 418
    label "cecha"
  ]
  node [
    id 419
    label "zrehabilitowanie_si&#281;"
  ]
  node [
    id 420
    label "sofcik"
  ]
  node [
    id 421
    label "appraisal"
  ]
  node [
    id 422
    label "ekspertyza"
  ]
  node [
    id 423
    label "zrehabilitowa&#263;_si&#281;"
  ]
  node [
    id 424
    label "pogl&#261;d"
  ]
  node [
    id 425
    label "kryterium"
  ]
  node [
    id 426
    label "wielko&#347;&#263;"
  ]
  node [
    id 427
    label "u&#380;ywa&#263;"
  ]
  node [
    id 428
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 429
    label "punkt_widzenia"
  ]
  node [
    id 430
    label "do&#322;ek"
  ]
  node [
    id 431
    label "formality"
  ]
  node [
    id 432
    label "wz&#243;r"
  ]
  node [
    id 433
    label "kantyzm"
  ]
  node [
    id 434
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 435
    label "ornamentyka"
  ]
  node [
    id 436
    label "odmiana"
  ]
  node [
    id 437
    label "mode"
  ]
  node [
    id 438
    label "style"
  ]
  node [
    id 439
    label "formacja"
  ]
  node [
    id 440
    label "maszyna_drukarska"
  ]
  node [
    id 441
    label "poznanie"
  ]
  node [
    id 442
    label "spirala"
  ]
  node [
    id 443
    label "blaszka"
  ]
  node [
    id 444
    label "linearno&#347;&#263;"
  ]
  node [
    id 445
    label "temat"
  ]
  node [
    id 446
    label "g&#322;owa"
  ]
  node [
    id 447
    label "kielich"
  ]
  node [
    id 448
    label "zdolno&#347;&#263;"
  ]
  node [
    id 449
    label "poj&#281;cie"
  ]
  node [
    id 450
    label "kszta&#322;t"
  ]
  node [
    id 451
    label "pasmo"
  ]
  node [
    id 452
    label "rdze&#324;"
  ]
  node [
    id 453
    label "leksem"
  ]
  node [
    id 454
    label "dyspozycja"
  ]
  node [
    id 455
    label "wygl&#261;d"
  ]
  node [
    id 456
    label "October"
  ]
  node [
    id 457
    label "zawarto&#347;&#263;"
  ]
  node [
    id 458
    label "p&#281;tla"
  ]
  node [
    id 459
    label "p&#322;at"
  ]
  node [
    id 460
    label "gwiazda"
  ]
  node [
    id 461
    label "arystotelizm"
  ]
  node [
    id 462
    label "obiekt"
  ]
  node [
    id 463
    label "dzie&#322;o"
  ]
  node [
    id 464
    label "naczynie"
  ]
  node [
    id 465
    label "jednostka_systematyczna"
  ]
  node [
    id 466
    label "miniatura"
  ]
  node [
    id 467
    label "zwyczaj"
  ]
  node [
    id 468
    label "morfem"
  ]
  node [
    id 469
    label "posta&#263;"
  ]
  node [
    id 470
    label "typowo"
  ]
  node [
    id 471
    label "zwyczajny"
  ]
  node [
    id 472
    label "zwyk&#322;y"
  ]
  node [
    id 473
    label "cz&#281;sty"
  ]
  node [
    id 474
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 475
    label "rynek"
  ]
  node [
    id 476
    label "podmiot"
  ]
  node [
    id 477
    label "&#322;a&#324;cuch_pokarmowy"
  ]
  node [
    id 478
    label "manufacturer"
  ]
  node [
    id 479
    label "bran&#380;owiec"
  ]
  node [
    id 480
    label "artel"
  ]
  node [
    id 481
    label "filmowiec"
  ]
  node [
    id 482
    label "muzyk"
  ]
  node [
    id 483
    label "Canon"
  ]
  node [
    id 484
    label "autotrof"
  ]
  node [
    id 485
    label "Wedel"
  ]
  node [
    id 486
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 487
    label "endecki"
  ]
  node [
    id 488
    label "komitet_koordynacyjny"
  ]
  node [
    id 489
    label "przybud&#243;wka"
  ]
  node [
    id 490
    label "ZOMO"
  ]
  node [
    id 491
    label "boj&#243;wka"
  ]
  node [
    id 492
    label "zesp&#243;&#322;"
  ]
  node [
    id 493
    label "organization"
  ]
  node [
    id 494
    label "TOPR"
  ]
  node [
    id 495
    label "jednostka_organizacyjna"
  ]
  node [
    id 496
    label "przedstawicielstwo"
  ]
  node [
    id 497
    label "Cepelia"
  ]
  node [
    id 498
    label "GOPR"
  ]
  node [
    id 499
    label "ZMP"
  ]
  node [
    id 500
    label "ZBoWiD"
  ]
  node [
    id 501
    label "od&#322;am"
  ]
  node [
    id 502
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 503
    label "centrala"
  ]
  node [
    id 504
    label "nauka_ekonomiczna"
  ]
  node [
    id 505
    label "polecanie"
  ]
  node [
    id 506
    label "charakteryzowanie_si&#281;"
  ]
  node [
    id 507
    label "dominance"
  ]
  node [
    id 508
    label "trzymanie"
  ]
  node [
    id 509
    label "dawanie"
  ]
  node [
    id 510
    label "nauka_humanistyczna"
  ]
  node [
    id 511
    label "commission"
  ]
  node [
    id 512
    label "pozarz&#261;dzanie"
  ]
  node [
    id 513
    label "dysponowanie"
  ]
  node [
    id 514
    label "obserwacja"
  ]
  node [
    id 515
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 516
    label "nauka_prawa"
  ]
  node [
    id 517
    label "dominion"
  ]
  node [
    id 518
    label "normatywizm"
  ]
  node [
    id 519
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 520
    label "qualification"
  ]
  node [
    id 521
    label "opis"
  ]
  node [
    id 522
    label "regu&#322;a_Allena"
  ]
  node [
    id 523
    label "normalizacja"
  ]
  node [
    id 524
    label "kazuistyka"
  ]
  node [
    id 525
    label "regu&#322;a_Glogera"
  ]
  node [
    id 526
    label "kultura_duchowa"
  ]
  node [
    id 527
    label "prawo_karne"
  ]
  node [
    id 528
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 529
    label "standard"
  ]
  node [
    id 530
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 531
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 532
    label "prawo_karne_procesowe"
  ]
  node [
    id 533
    label "prawo_Mendla"
  ]
  node [
    id 534
    label "przepis"
  ]
  node [
    id 535
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 536
    label "criterion"
  ]
  node [
    id 537
    label "kanonistyka"
  ]
  node [
    id 538
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 539
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 540
    label "wykonawczy"
  ]
  node [
    id 541
    label "twierdzenie"
  ]
  node [
    id 542
    label "judykatura"
  ]
  node [
    id 543
    label "legislacyjnie"
  ]
  node [
    id 544
    label "umocowa&#263;"
  ]
  node [
    id 545
    label "procesualistyka"
  ]
  node [
    id 546
    label "kierunek"
  ]
  node [
    id 547
    label "kryminologia"
  ]
  node [
    id 548
    label "kryminalistyka"
  ]
  node [
    id 549
    label "cywilistyka"
  ]
  node [
    id 550
    label "law"
  ]
  node [
    id 551
    label "zasada_d'Alemberta"
  ]
  node [
    id 552
    label "jurisprudence"
  ]
  node [
    id 553
    label "zasada"
  ]
  node [
    id 554
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 555
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 556
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 557
    label "express"
  ]
  node [
    id 558
    label "rzekn&#261;&#263;"
  ]
  node [
    id 559
    label "okre&#347;li&#263;"
  ]
  node [
    id 560
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 561
    label "unwrap"
  ]
  node [
    id 562
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 563
    label "convey"
  ]
  node [
    id 564
    label "discover"
  ]
  node [
    id 565
    label "wydoby&#263;"
  ]
  node [
    id 566
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 567
    label "czyn"
  ]
  node [
    id 568
    label "ilustracja"
  ]
  node [
    id 569
    label "fakt"
  ]
  node [
    id 570
    label "dob&#243;r_naturalny"
  ]
  node [
    id 571
    label "firma"
  ]
  node [
    id 572
    label "dyscyplina_sportowa"
  ]
  node [
    id 573
    label "interakcja"
  ]
  node [
    id 574
    label "wydarzenie"
  ]
  node [
    id 575
    label "rywalizacja"
  ]
  node [
    id 576
    label "uczestnik"
  ]
  node [
    id 577
    label "contest"
  ]
  node [
    id 578
    label "nienowoczesny"
  ]
  node [
    id 579
    label "zachowawczy"
  ]
  node [
    id 580
    label "zwyczajowy"
  ]
  node [
    id 581
    label "modelowy"
  ]
  node [
    id 582
    label "przyj&#281;ty"
  ]
  node [
    id 583
    label "wierny"
  ]
  node [
    id 584
    label "tradycyjnie"
  ]
  node [
    id 585
    label "surowy"
  ]
  node [
    id 586
    label "typ"
  ]
  node [
    id 587
    label "pozowa&#263;"
  ]
  node [
    id 588
    label "ideal"
  ]
  node [
    id 589
    label "matryca"
  ]
  node [
    id 590
    label "imitacja"
  ]
  node [
    id 591
    label "ruch"
  ]
  node [
    id 592
    label "motif"
  ]
  node [
    id 593
    label "pozowanie"
  ]
  node [
    id 594
    label "prezenter"
  ]
  node [
    id 595
    label "facet"
  ]
  node [
    id 596
    label "orygina&#322;"
  ]
  node [
    id 597
    label "mildew"
  ]
  node [
    id 598
    label "spos&#243;b"
  ]
  node [
    id 599
    label "zi&#243;&#322;ko"
  ]
  node [
    id 600
    label "adaptation"
  ]
  node [
    id 601
    label "w&#322;asny"
  ]
  node [
    id 602
    label "oryginalny"
  ]
  node [
    id 603
    label "autorsko"
  ]
  node [
    id 604
    label "get"
  ]
  node [
    id 605
    label "przewa&#380;a&#263;"
  ]
  node [
    id 606
    label "zobowi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 607
    label "poczytywa&#263;"
  ]
  node [
    id 608
    label "levy"
  ]
  node [
    id 609
    label "pokonywa&#263;"
  ]
  node [
    id 610
    label "rusza&#263;"
  ]
  node [
    id 611
    label "zalicza&#263;"
  ]
  node [
    id 612
    label "wygrywa&#263;"
  ]
  node [
    id 613
    label "open"
  ]
  node [
    id 614
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 615
    label "branie"
  ]
  node [
    id 616
    label "korzysta&#263;"
  ]
  node [
    id 617
    label "&#263;pa&#263;"
  ]
  node [
    id 618
    label "wch&#322;ania&#263;"
  ]
  node [
    id 619
    label "interpretowa&#263;"
  ]
  node [
    id 620
    label "atakowa&#263;"
  ]
  node [
    id 621
    label "prowadzi&#263;"
  ]
  node [
    id 622
    label "rucha&#263;"
  ]
  node [
    id 623
    label "take"
  ]
  node [
    id 624
    label "dostawa&#263;"
  ]
  node [
    id 625
    label "wzi&#261;&#263;"
  ]
  node [
    id 626
    label "wk&#322;ada&#263;"
  ]
  node [
    id 627
    label "chwyta&#263;"
  ]
  node [
    id 628
    label "arise"
  ]
  node [
    id 629
    label "za&#380;ywa&#263;"
  ]
  node [
    id 630
    label "uprawia&#263;_seks"
  ]
  node [
    id 631
    label "porywa&#263;"
  ]
  node [
    id 632
    label "grza&#263;"
  ]
  node [
    id 633
    label "abstract"
  ]
  node [
    id 634
    label "zaopatrywa&#263;_si&#281;"
  ]
  node [
    id 635
    label "wzmaga&#263;_si&#281;"
  ]
  node [
    id 636
    label "towarzystwo"
  ]
  node [
    id 637
    label "otrzymywa&#263;"
  ]
  node [
    id 638
    label "przyjmowa&#263;"
  ]
  node [
    id 639
    label "wchodzi&#263;"
  ]
  node [
    id 640
    label "ucieka&#263;"
  ]
  node [
    id 641
    label "odurza&#263;_si&#281;"
  ]
  node [
    id 642
    label "buzowa&#263;_si&#281;"
  ]
  node [
    id 643
    label "&#322;apa&#263;"
  ]
  node [
    id 644
    label "raise"
  ]
  node [
    id 645
    label "kognicja"
  ]
  node [
    id 646
    label "idea"
  ]
  node [
    id 647
    label "szczeg&#243;&#322;"
  ]
  node [
    id 648
    label "rzecz"
  ]
  node [
    id 649
    label "przes&#322;anka"
  ]
  node [
    id 650
    label "rozprawa"
  ]
  node [
    id 651
    label "object"
  ]
  node [
    id 652
    label "proposition"
  ]
  node [
    id 653
    label "krzy&#380;"
  ]
  node [
    id 654
    label "paw"
  ]
  node [
    id 655
    label "rami&#281;"
  ]
  node [
    id 656
    label "gestykulowanie"
  ]
  node [
    id 657
    label "pi&#322;ka_no&#380;na"
  ]
  node [
    id 658
    label "pracownik"
  ]
  node [
    id 659
    label "ko&#324;czyna_g&#243;rna"
  ]
  node [
    id 660
    label "bramkarz"
  ]
  node [
    id 661
    label "&#347;r&#243;dr&#281;cze"
  ]
  node [
    id 662
    label "handwriting"
  ]
  node [
    id 663
    label "hasta"
  ]
  node [
    id 664
    label "pi&#322;ka"
  ]
  node [
    id 665
    label "&#322;okie&#263;"
  ]
  node [
    id 666
    label "zagrywka"
  ]
  node [
    id 667
    label "obietnica"
  ]
  node [
    id 668
    label "przedrami&#281;"
  ]
  node [
    id 669
    label "r&#261;czyna"
  ]
  node [
    id 670
    label "wykroczenie"
  ]
  node [
    id 671
    label "kroki"
  ]
  node [
    id 672
    label "palec"
  ]
  node [
    id 673
    label "&#380;&#243;&#322;ta_kartka"
  ]
  node [
    id 674
    label "graba"
  ]
  node [
    id 675
    label "hand"
  ]
  node [
    id 676
    label "nadgarstek"
  ]
  node [
    id 677
    label "pomocnik"
  ]
  node [
    id 678
    label "k&#322;&#261;b"
  ]
  node [
    id 679
    label "hazena"
  ]
  node [
    id 680
    label "gestykulowa&#263;"
  ]
  node [
    id 681
    label "cmoknonsens"
  ]
  node [
    id 682
    label "d&#322;o&#324;"
  ]
  node [
    id 683
    label "chwytanie"
  ]
  node [
    id 684
    label "czerwona_kartka"
  ]
  node [
    id 685
    label "charge"
  ]
  node [
    id 686
    label "zrzeka&#263;_si&#281;"
  ]
  node [
    id 687
    label "przestawa&#263;"
  ]
  node [
    id 688
    label "metier"
  ]
  node [
    id 689
    label "zasila&#263;"
  ]
  node [
    id 690
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 691
    label "work"
  ]
  node [
    id 692
    label "przyp&#322;ywa&#263;"
  ]
  node [
    id 693
    label "kapita&#322;"
  ]
  node [
    id 694
    label "dochodzi&#263;"
  ]
  node [
    id 695
    label "pour"
  ]
  node [
    id 696
    label "ciek_wodny"
  ]
  node [
    id 697
    label "ton"
  ]
  node [
    id 698
    label "wydziedziczy&#263;"
  ]
  node [
    id 699
    label "zachowek"
  ]
  node [
    id 700
    label "wydziedziczenie"
  ]
  node [
    id 701
    label "mienie"
  ]
  node [
    id 702
    label "scheda_spadkowa"
  ]
  node [
    id 703
    label "camber"
  ]
  node [
    id 704
    label "sukcesja"
  ]
  node [
    id 705
    label "p&#322;aszczyzna"
  ]
  node [
    id 706
    label "korzy&#347;&#263;"
  ]
  node [
    id 707
    label "krzywa_Engla"
  ]
  node [
    id 708
    label "dochodowa_elastyczno&#347;&#263;_popytu"
  ]
  node [
    id 709
    label "wp&#322;yw"
  ]
  node [
    id 710
    label "income"
  ]
  node [
    id 711
    label "stopa_procentowa"
  ]
  node [
    id 712
    label "powodowa&#263;"
  ]
  node [
    id 713
    label "zadawa&#263;_gwa&#322;t"
  ]
  node [
    id 714
    label "sandbag"
  ]
  node [
    id 715
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 716
    label "ask"
  ]
  node [
    id 717
    label "try"
  ]
  node [
    id 718
    label "&#322;azi&#263;"
  ]
  node [
    id 719
    label "sprawdza&#263;"
  ]
  node [
    id 720
    label "stara&#263;_si&#281;"
  ]
  node [
    id 721
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 722
    label "zawodowy"
  ]
  node [
    id 723
    label "biznesowo"
  ]
  node [
    id 724
    label "anatomopatolog"
  ]
  node [
    id 725
    label "rewizja"
  ]
  node [
    id 726
    label "oznaka"
  ]
  node [
    id 727
    label "czas"
  ]
  node [
    id 728
    label "ferment"
  ]
  node [
    id 729
    label "komplet"
  ]
  node [
    id 730
    label "tura"
  ]
  node [
    id 731
    label "amendment"
  ]
  node [
    id 732
    label "zmianka"
  ]
  node [
    id 733
    label "odmienianie"
  ]
  node [
    id 734
    label "passage"
  ]
  node [
    id 735
    label "zjawisko"
  ]
  node [
    id 736
    label "praca"
  ]
  node [
    id 737
    label "przeszkala&#263;"
  ]
  node [
    id 738
    label "warto&#347;&#263;"
  ]
  node [
    id 739
    label "informowa&#263;"
  ]
  node [
    id 740
    label "introduce"
  ]
  node [
    id 741
    label "bespeak"
  ]
  node [
    id 742
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 743
    label "indicate"
  ]
  node [
    id 744
    label "wyraz"
  ]
  node [
    id 745
    label "wyra&#380;a&#263;"
  ]
  node [
    id 746
    label "exhibit"
  ]
  node [
    id 747
    label "podawa&#263;"
  ]
  node [
    id 748
    label "exsert"
  ]
  node [
    id 749
    label "przedstawia&#263;"
  ]
  node [
    id 750
    label "pacjent"
  ]
  node [
    id 751
    label "kategoria_gramatyczna"
  ]
  node [
    id 752
    label "schorzenie"
  ]
  node [
    id 753
    label "przeznaczenie"
  ]
  node [
    id 754
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 755
    label "happening"
  ]
  node [
    id 756
    label "program"
  ]
  node [
    id 757
    label "reengineering"
  ]
  node [
    id 758
    label "uprawi&#263;"
  ]
  node [
    id 759
    label "gotowy"
  ]
  node [
    id 760
    label "might"
  ]
  node [
    id 761
    label "strategia"
  ]
  node [
    id 762
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 763
    label "ob&#322;awa_augustowska"
  ]
  node [
    id 764
    label "kolejny"
  ]
  node [
    id 765
    label "inaczej"
  ]
  node [
    id 766
    label "r&#243;&#380;ny"
  ]
  node [
    id 767
    label "inszy"
  ]
  node [
    id 768
    label "osobno"
  ]
  node [
    id 769
    label "faza"
  ]
  node [
    id 770
    label "depression"
  ]
  node [
    id 771
    label "nizina"
  ]
  node [
    id 772
    label "transakcja"
  ]
  node [
    id 773
    label "sprzedaj&#261;cy"
  ]
  node [
    id 774
    label "przeniesienie_praw"
  ]
  node [
    id 775
    label "rabat"
  ]
  node [
    id 776
    label "przeda&#380;"
  ]
  node [
    id 777
    label "p&#322;acz"
  ]
  node [
    id 778
    label "wyst&#281;p"
  ]
  node [
    id 779
    label "performance"
  ]
  node [
    id 780
    label "bogactwo"
  ]
  node [
    id 781
    label "mn&#243;stwo"
  ]
  node [
    id 782
    label "show"
  ]
  node [
    id 783
    label "pokaz"
  ]
  node [
    id 784
    label "szale&#324;stwo"
  ]
  node [
    id 785
    label "swobodny"
  ]
  node [
    id 786
    label "dobrowolnie"
  ]
  node [
    id 787
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 788
    label "kwota"
  ]
  node [
    id 789
    label "prawdziwy"
  ]
  node [
    id 790
    label "mo&#380;liwy"
  ]
  node [
    id 791
    label "realnie"
  ]
  node [
    id 792
    label "podobny"
  ]
  node [
    id 793
    label "uprzedzenie"
  ]
  node [
    id 794
    label "zawisa&#263;"
  ]
  node [
    id 795
    label "zawisanie"
  ]
  node [
    id 796
    label "zaszachowanie"
  ]
  node [
    id 797
    label "szachowanie"
  ]
  node [
    id 798
    label "emergency"
  ]
  node [
    id 799
    label "zaistnienie"
  ]
  node [
    id 800
    label "czarny_punkt"
  ]
  node [
    id 801
    label "zagrozi&#263;"
  ]
  node [
    id 802
    label "jaki&#347;"
  ]
  node [
    id 803
    label "wys&#322;awia&#263;"
  ]
  node [
    id 804
    label "wywy&#380;sza&#263;"
  ]
  node [
    id 805
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 806
    label "bless"
  ]
  node [
    id 807
    label "patrze&#263;_jak_w_&#347;wi&#281;ty_obraz"
  ]
  node [
    id 808
    label "nagradza&#263;"
  ]
  node [
    id 809
    label "wielmo&#380;y&#263;"
  ]
  node [
    id 810
    label "glorify"
  ]
  node [
    id 811
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 812
    label "legalny"
  ]
  node [
    id 813
    label "legally"
  ]
  node [
    id 814
    label "ok&#322;adka"
  ]
  node [
    id 815
    label "zak&#322;adka"
  ]
  node [
    id 816
    label "ekslibris"
  ]
  node [
    id 817
    label "wk&#322;ad"
  ]
  node [
    id 818
    label "przek&#322;adacz"
  ]
  node [
    id 819
    label "wydawnictwo"
  ]
  node [
    id 820
    label "grzbiet_ksi&#261;&#380;ki"
  ]
  node [
    id 821
    label "tytu&#322;"
  ]
  node [
    id 822
    label "bibliofilstwo"
  ]
  node [
    id 823
    label "falc"
  ]
  node [
    id 824
    label "nomina&#322;"
  ]
  node [
    id 825
    label "pagina"
  ]
  node [
    id 826
    label "rozdzia&#322;"
  ]
  node [
    id 827
    label "egzemplarz"
  ]
  node [
    id 828
    label "zw&#243;j"
  ]
  node [
    id 829
    label "us&#322;uga_internetowa"
  ]
  node [
    id 830
    label "biznes_elektroniczny"
  ]
  node [
    id 831
    label "punkt_dost&#281;pu"
  ]
  node [
    id 832
    label "hipertekst"
  ]
  node [
    id 833
    label "gra_sieciowa"
  ]
  node [
    id 834
    label "mem"
  ]
  node [
    id 835
    label "e-hazard"
  ]
  node [
    id 836
    label "sie&#263;_komputerowa"
  ]
  node [
    id 837
    label "media"
  ]
  node [
    id 838
    label "podcast"
  ]
  node [
    id 839
    label "netbook"
  ]
  node [
    id 840
    label "provider"
  ]
  node [
    id 841
    label "cyberprzestrze&#324;"
  ]
  node [
    id 842
    label "grooming"
  ]
  node [
    id 843
    label "strona"
  ]
  node [
    id 844
    label "og&#322;asza&#263;"
  ]
  node [
    id 845
    label "odbija&#263;"
  ]
  node [
    id 846
    label "powiela&#263;"
  ]
  node [
    id 847
    label "godzina"
  ]
  node [
    id 848
    label "delivery"
  ]
  node [
    id 849
    label "podanie"
  ]
  node [
    id 850
    label "issue"
  ]
  node [
    id 851
    label "danie"
  ]
  node [
    id 852
    label "rendition"
  ]
  node [
    id 853
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 854
    label "impression"
  ]
  node [
    id 855
    label "zapach"
  ]
  node [
    id 856
    label "wytworzenie"
  ]
  node [
    id 857
    label "wprowadzenie"
  ]
  node [
    id 858
    label "zdarzenie_si&#281;"
  ]
  node [
    id 859
    label "zrobienie"
  ]
  node [
    id 860
    label "ujawnienie"
  ]
  node [
    id 861
    label "reszta"
  ]
  node [
    id 862
    label "ukazanie_si&#281;_drukiem"
  ]
  node [
    id 863
    label "zadenuncjowanie"
  ]
  node [
    id 864
    label "czasopismo"
  ]
  node [
    id 865
    label "urz&#261;dzenie"
  ]
  node [
    id 866
    label "d&#378;wi&#281;k"
  ]
  node [
    id 867
    label "publikacja"
  ]
  node [
    id 868
    label "piwo"
  ]
  node [
    id 869
    label "ho&#322;ysz"
  ]
  node [
    id 870
    label "s&#322;aby"
  ]
  node [
    id 871
    label "ubo&#380;enie"
  ]
  node [
    id 872
    label "nieszcz&#281;&#347;liwy"
  ]
  node [
    id 873
    label "zubo&#380;anie"
  ]
  node [
    id 874
    label "biedota"
  ]
  node [
    id 875
    label "zubo&#380;enie"
  ]
  node [
    id 876
    label "n&#281;dzny"
  ]
  node [
    id 877
    label "bankrutowanie"
  ]
  node [
    id 878
    label "proletariusz"
  ]
  node [
    id 879
    label "go&#322;odupiec"
  ]
  node [
    id 880
    label "biednie"
  ]
  node [
    id 881
    label "zbiednienie"
  ]
  node [
    id 882
    label "sytuowany"
  ]
  node [
    id 883
    label "raw_material"
  ]
  node [
    id 884
    label "cognizance"
  ]
  node [
    id 885
    label "post&#281;powa&#263;"
  ]
  node [
    id 886
    label "sprawia&#263;"
  ]
  node [
    id 887
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 888
    label "ukazywa&#263;"
  ]
  node [
    id 889
    label "pop&#281;dzi&#263;_kota"
  ]
  node [
    id 890
    label "zada&#263;_gwa&#322;t"
  ]
  node [
    id 891
    label "spowodowa&#263;"
  ]
  node [
    id 892
    label "force"
  ]
  node [
    id 893
    label "sign"
  ]
  node [
    id 894
    label "opatrywa&#263;"
  ]
  node [
    id 895
    label "stawia&#263;"
  ]
  node [
    id 896
    label "deklaracja"
  ]
  node [
    id 897
    label "assurance"
  ]
  node [
    id 898
    label "defenestracja"
  ]
  node [
    id 899
    label "szereg"
  ]
  node [
    id 900
    label "dzia&#322;anie"
  ]
  node [
    id 901
    label "ostatnie_podrygi"
  ]
  node [
    id 902
    label "kres"
  ]
  node [
    id 903
    label "agonia"
  ]
  node [
    id 904
    label "visitation"
  ]
  node [
    id 905
    label "szeol"
  ]
  node [
    id 906
    label "mogi&#322;a"
  ]
  node [
    id 907
    label "pogrzebanie"
  ]
  node [
    id 908
    label "&#380;a&#322;oba"
  ]
  node [
    id 909
    label "zabicie"
  ]
  node [
    id 910
    label "kres_&#380;ycia"
  ]
  node [
    id 911
    label "energy"
  ]
  node [
    id 912
    label "zegar_biologiczny"
  ]
  node [
    id 913
    label "okres_noworodkowy"
  ]
  node [
    id 914
    label "prze&#380;ywanie"
  ]
  node [
    id 915
    label "prze&#380;ycie"
  ]
  node [
    id 916
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 917
    label "wiek_matuzalemowy"
  ]
  node [
    id 918
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 919
    label "dzieci&#324;stwo"
  ]
  node [
    id 920
    label "power"
  ]
  node [
    id 921
    label "szwung"
  ]
  node [
    id 922
    label "menopauza"
  ]
  node [
    id 923
    label "umarcie"
  ]
  node [
    id 924
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 925
    label "life"
  ]
  node [
    id 926
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 927
    label "&#380;ywy"
  ]
  node [
    id 928
    label "rozw&#243;j"
  ]
  node [
    id 929
    label "po&#322;&#243;g"
  ]
  node [
    id 930
    label "przebywanie"
  ]
  node [
    id 931
    label "subsistence"
  ]
  node [
    id 932
    label "koleje_losu"
  ]
  node [
    id 933
    label "raj_utracony"
  ]
  node [
    id 934
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 935
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 936
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 937
    label "andropauza"
  ]
  node [
    id 938
    label "warunki"
  ]
  node [
    id 939
    label "do&#380;ywanie"
  ]
  node [
    id 940
    label "niemowl&#281;ctwo"
  ]
  node [
    id 941
    label "umieranie"
  ]
  node [
    id 942
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 943
    label "staro&#347;&#263;"
  ]
  node [
    id 944
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 945
    label "&#347;mier&#263;"
  ]
  node [
    id 946
    label "consist"
  ]
  node [
    id 947
    label "pope&#322;nia&#263;"
  ]
  node [
    id 948
    label "wytwarza&#263;"
  ]
  node [
    id 949
    label "stanowi&#263;"
  ]
  node [
    id 950
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 951
    label "&#380;agl&#243;wka"
  ]
  node [
    id 952
    label "rozb&#243;jnik"
  ]
  node [
    id 953
    label "kieruj&#261;cy"
  ]
  node [
    id 954
    label "podr&#243;bka"
  ]
  node [
    id 955
    label "rum"
  ]
  node [
    id 956
    label "kopiowa&#263;"
  ]
  node [
    id 957
    label "przest&#281;pca"
  ]
  node [
    id 958
    label "postrzeleniec"
  ]
  node [
    id 959
    label "czasowo"
  ]
  node [
    id 960
    label "wtedy"
  ]
  node [
    id 961
    label "hold"
  ]
  node [
    id 962
    label "sp&#281;dza&#263;"
  ]
  node [
    id 963
    label "look"
  ]
  node [
    id 964
    label "decydowa&#263;"
  ]
  node [
    id 965
    label "oczekiwa&#263;"
  ]
  node [
    id 966
    label "pauzowa&#263;"
  ]
  node [
    id 967
    label "anticipate"
  ]
  node [
    id 968
    label "punctiliously"
  ]
  node [
    id 969
    label "dok&#322;adny"
  ]
  node [
    id 970
    label "meticulously"
  ]
  node [
    id 971
    label "precyzyjnie"
  ]
  node [
    id 972
    label "rzetelnie"
  ]
  node [
    id 973
    label "frame"
  ]
  node [
    id 974
    label "zinterpretowa&#263;"
  ]
  node [
    id 975
    label "przekona&#263;"
  ]
  node [
    id 976
    label "uzasadni&#263;"
  ]
  node [
    id 977
    label "przedstawi&#263;"
  ]
  node [
    id 978
    label "poja&#347;ni&#263;"
  ]
  node [
    id 979
    label "obroni&#263;"
  ]
  node [
    id 980
    label "u&#322;atwi&#263;"
  ]
  node [
    id 981
    label "clear"
  ]
  node [
    id 982
    label "ki&#347;&#263;"
  ]
  node [
    id 983
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 984
    label "krzew"
  ]
  node [
    id 985
    label "pi&#380;maczkowate"
  ]
  node [
    id 986
    label "pestkowiec"
  ]
  node [
    id 987
    label "kwiat"
  ]
  node [
    id 988
    label "owoc"
  ]
  node [
    id 989
    label "oliwkowate"
  ]
  node [
    id 990
    label "ro&#347;lina"
  ]
  node [
    id 991
    label "hy&#263;ka"
  ]
  node [
    id 992
    label "lilac"
  ]
  node [
    id 993
    label "delfinidyna"
  ]
  node [
    id 994
    label "niedrogo"
  ]
  node [
    id 995
    label "tanio"
  ]
  node [
    id 996
    label "tandetny"
  ]
  node [
    id 997
    label "manewr"
  ]
  node [
    id 998
    label "podchwyt"
  ]
  node [
    id 999
    label "popis"
  ]
  node [
    id 1000
    label "kuglarz"
  ]
  node [
    id 1001
    label "fortel"
  ]
  node [
    id 1002
    label "game"
  ]
  node [
    id 1003
    label "magic_trick"
  ]
  node [
    id 1004
    label "chwyt"
  ]
  node [
    id 1005
    label "numer"
  ]
  node [
    id 1006
    label "sztuka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 21
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 145
  ]
  edge [
    source 7
    target 146
  ]
  edge [
    source 7
    target 147
  ]
  edge [
    source 7
    target 148
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 149
  ]
  edge [
    source 7
    target 150
  ]
  edge [
    source 7
    target 28
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 151
  ]
  edge [
    source 8
    target 152
  ]
  edge [
    source 8
    target 153
  ]
  edge [
    source 8
    target 154
  ]
  edge [
    source 8
    target 155
  ]
  edge [
    source 8
    target 156
  ]
  edge [
    source 8
    target 157
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 158
  ]
  edge [
    source 9
    target 159
  ]
  edge [
    source 9
    target 160
  ]
  edge [
    source 9
    target 161
  ]
  edge [
    source 9
    target 162
  ]
  edge [
    source 9
    target 163
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 164
  ]
  edge [
    source 10
    target 165
  ]
  edge [
    source 10
    target 166
  ]
  edge [
    source 10
    target 167
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 168
  ]
  edge [
    source 11
    target 169
  ]
  edge [
    source 11
    target 170
  ]
  edge [
    source 11
    target 171
  ]
  edge [
    source 11
    target 172
  ]
  edge [
    source 11
    target 173
  ]
  edge [
    source 11
    target 174
  ]
  edge [
    source 11
    target 175
  ]
  edge [
    source 11
    target 176
  ]
  edge [
    source 11
    target 61
  ]
  edge [
    source 11
    target 177
  ]
  edge [
    source 11
    target 178
  ]
  edge [
    source 11
    target 179
  ]
  edge [
    source 11
    target 180
  ]
  edge [
    source 11
    target 181
  ]
  edge [
    source 11
    target 182
  ]
  edge [
    source 11
    target 183
  ]
  edge [
    source 11
    target 184
  ]
  edge [
    source 11
    target 185
  ]
  edge [
    source 11
    target 186
  ]
  edge [
    source 11
    target 187
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 188
  ]
  edge [
    source 12
    target 189
  ]
  edge [
    source 12
    target 190
  ]
  edge [
    source 12
    target 191
  ]
  edge [
    source 12
    target 192
  ]
  edge [
    source 12
    target 193
  ]
  edge [
    source 12
    target 194
  ]
  edge [
    source 12
    target 195
  ]
  edge [
    source 12
    target 196
  ]
  edge [
    source 12
    target 197
  ]
  edge [
    source 12
    target 198
  ]
  edge [
    source 12
    target 199
  ]
  edge [
    source 12
    target 200
  ]
  edge [
    source 12
    target 201
  ]
  edge [
    source 12
    target 202
  ]
  edge [
    source 12
    target 203
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 35
  ]
  edge [
    source 13
    target 67
  ]
  edge [
    source 13
    target 43
  ]
  edge [
    source 13
    target 80
  ]
  edge [
    source 13
    target 138
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 36
  ]
  edge [
    source 14
    target 44
  ]
  edge [
    source 14
    target 45
  ]
  edge [
    source 14
    target 82
  ]
  edge [
    source 14
    target 57
  ]
  edge [
    source 14
    target 142
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 204
  ]
  edge [
    source 15
    target 145
  ]
  edge [
    source 15
    target 205
  ]
  edge [
    source 15
    target 206
  ]
  edge [
    source 15
    target 207
  ]
  edge [
    source 15
    target 208
  ]
  edge [
    source 15
    target 209
  ]
  edge [
    source 15
    target 210
  ]
  edge [
    source 15
    target 211
  ]
  edge [
    source 15
    target 212
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 132
  ]
  edge [
    source 16
    target 133
  ]
  edge [
    source 16
    target 213
  ]
  edge [
    source 16
    target 214
  ]
  edge [
    source 16
    target 215
  ]
  edge [
    source 16
    target 44
  ]
  edge [
    source 16
    target 216
  ]
  edge [
    source 16
    target 217
  ]
  edge [
    source 16
    target 218
  ]
  edge [
    source 16
    target 219
  ]
  edge [
    source 16
    target 220
  ]
  edge [
    source 16
    target 221
  ]
  edge [
    source 16
    target 222
  ]
  edge [
    source 16
    target 73
  ]
  edge [
    source 16
    target 92
  ]
  edge [
    source 16
    target 97
  ]
  edge [
    source 16
    target 137
  ]
  edge [
    source 17
    target 223
  ]
  edge [
    source 17
    target 224
  ]
  edge [
    source 17
    target 225
  ]
  edge [
    source 17
    target 226
  ]
  edge [
    source 17
    target 227
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 228
  ]
  edge [
    source 18
    target 229
  ]
  edge [
    source 18
    target 230
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 55
  ]
  edge [
    source 21
    target 56
  ]
  edge [
    source 21
    target 33
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 231
  ]
  edge [
    source 23
    target 232
  ]
  edge [
    source 23
    target 44
  ]
  edge [
    source 23
    target 233
  ]
  edge [
    source 23
    target 234
  ]
  edge [
    source 23
    target 235
  ]
  edge [
    source 23
    target 226
  ]
  edge [
    source 23
    target 236
  ]
  edge [
    source 23
    target 237
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 238
  ]
  edge [
    source 24
    target 239
  ]
  edge [
    source 24
    target 240
  ]
  edge [
    source 24
    target 241
  ]
  edge [
    source 24
    target 242
  ]
  edge [
    source 24
    target 243
  ]
  edge [
    source 24
    target 244
  ]
  edge [
    source 24
    target 245
  ]
  edge [
    source 24
    target 246
  ]
  edge [
    source 24
    target 247
  ]
  edge [
    source 24
    target 248
  ]
  edge [
    source 24
    target 249
  ]
  edge [
    source 24
    target 250
  ]
  edge [
    source 24
    target 68
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 251
  ]
  edge [
    source 25
    target 252
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 253
  ]
  edge [
    source 26
    target 254
  ]
  edge [
    source 26
    target 255
  ]
  edge [
    source 26
    target 256
  ]
  edge [
    source 26
    target 257
  ]
  edge [
    source 26
    target 258
  ]
  edge [
    source 26
    target 259
  ]
  edge [
    source 26
    target 260
  ]
  edge [
    source 26
    target 261
  ]
  edge [
    source 26
    target 262
  ]
  edge [
    source 26
    target 263
  ]
  edge [
    source 26
    target 264
  ]
  edge [
    source 26
    target 265
  ]
  edge [
    source 26
    target 266
  ]
  edge [
    source 26
    target 267
  ]
  edge [
    source 26
    target 268
  ]
  edge [
    source 26
    target 269
  ]
  edge [
    source 26
    target 270
  ]
  edge [
    source 26
    target 271
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 43
  ]
  edge [
    source 27
    target 44
  ]
  edge [
    source 27
    target 108
  ]
  edge [
    source 27
    target 109
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 72
  ]
  edge [
    source 28
    target 73
  ]
  edge [
    source 28
    target 123
  ]
  edge [
    source 28
    target 124
  ]
  edge [
    source 28
    target 272
  ]
  edge [
    source 28
    target 273
  ]
  edge [
    source 28
    target 274
  ]
  edge [
    source 28
    target 275
  ]
  edge [
    source 28
    target 276
  ]
  edge [
    source 28
    target 277
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 278
  ]
  edge [
    source 29
    target 279
  ]
  edge [
    source 29
    target 280
  ]
  edge [
    source 29
    target 281
  ]
  edge [
    source 29
    target 282
  ]
  edge [
    source 29
    target 283
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 44
  ]
  edge [
    source 30
    target 45
  ]
  edge [
    source 30
    target 112
  ]
  edge [
    source 30
    target 113
  ]
  edge [
    source 30
    target 118
  ]
  edge [
    source 30
    target 37
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 284
  ]
  edge [
    source 31
    target 285
  ]
  edge [
    source 31
    target 286
  ]
  edge [
    source 31
    target 287
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 288
  ]
  edge [
    source 32
    target 289
  ]
  edge [
    source 32
    target 290
  ]
  edge [
    source 32
    target 291
  ]
  edge [
    source 32
    target 292
  ]
  edge [
    source 32
    target 293
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 294
  ]
  edge [
    source 33
    target 50
  ]
  edge [
    source 33
    target 295
  ]
  edge [
    source 33
    target 296
  ]
  edge [
    source 33
    target 297
  ]
  edge [
    source 33
    target 298
  ]
  edge [
    source 33
    target 237
  ]
  edge [
    source 33
    target 299
  ]
  edge [
    source 33
    target 300
  ]
  edge [
    source 33
    target 301
  ]
  edge [
    source 33
    target 103
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 302
  ]
  edge [
    source 34
    target 303
  ]
  edge [
    source 34
    target 304
  ]
  edge [
    source 34
    target 305
  ]
  edge [
    source 34
    target 306
  ]
  edge [
    source 34
    target 307
  ]
  edge [
    source 34
    target 308
  ]
  edge [
    source 34
    target 309
  ]
  edge [
    source 34
    target 310
  ]
  edge [
    source 34
    target 311
  ]
  edge [
    source 34
    target 312
  ]
  edge [
    source 34
    target 313
  ]
  edge [
    source 34
    target 314
  ]
  edge [
    source 34
    target 315
  ]
  edge [
    source 35
    target 102
  ]
  edge [
    source 35
    target 103
  ]
  edge [
    source 35
    target 63
  ]
  edge [
    source 35
    target 316
  ]
  edge [
    source 35
    target 317
  ]
  edge [
    source 35
    target 318
  ]
  edge [
    source 35
    target 319
  ]
  edge [
    source 35
    target 320
  ]
  edge [
    source 35
    target 321
  ]
  edge [
    source 35
    target 322
  ]
  edge [
    source 35
    target 91
  ]
  edge [
    source 35
    target 123
  ]
  edge [
    source 35
    target 78
  ]
  edge [
    source 35
    target 89
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 64
  ]
  edge [
    source 36
    target 65
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 119
  ]
  edge [
    source 37
    target 130
  ]
  edge [
    source 37
    target 131
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 323
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 324
  ]
  edge [
    source 39
    target 325
  ]
  edge [
    source 39
    target 326
  ]
  edge [
    source 39
    target 327
  ]
  edge [
    source 39
    target 328
  ]
  edge [
    source 39
    target 329
  ]
  edge [
    source 39
    target 283
  ]
  edge [
    source 39
    target 330
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 331
  ]
  edge [
    source 40
    target 332
  ]
  edge [
    source 40
    target 333
  ]
  edge [
    source 40
    target 44
  ]
  edge [
    source 40
    target 334
  ]
  edge [
    source 40
    target 335
  ]
  edge [
    source 40
    target 336
  ]
  edge [
    source 40
    target 337
  ]
  edge [
    source 40
    target 338
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 130
  ]
  edge [
    source 43
    target 339
  ]
  edge [
    source 43
    target 340
  ]
  edge [
    source 43
    target 341
  ]
  edge [
    source 43
    target 80
  ]
  edge [
    source 44
    target 68
  ]
  edge [
    source 44
    target 342
  ]
  edge [
    source 44
    target 343
  ]
  edge [
    source 44
    target 344
  ]
  edge [
    source 44
    target 345
  ]
  edge [
    source 44
    target 346
  ]
  edge [
    source 44
    target 347
  ]
  edge [
    source 44
    target 348
  ]
  edge [
    source 44
    target 349
  ]
  edge [
    source 44
    target 350
  ]
  edge [
    source 44
    target 351
  ]
  edge [
    source 44
    target 352
  ]
  edge [
    source 44
    target 353
  ]
  edge [
    source 44
    target 354
  ]
  edge [
    source 44
    target 355
  ]
  edge [
    source 44
    target 356
  ]
  edge [
    source 44
    target 357
  ]
  edge [
    source 44
    target 358
  ]
  edge [
    source 44
    target 359
  ]
  edge [
    source 44
    target 360
  ]
  edge [
    source 44
    target 361
  ]
  edge [
    source 44
    target 362
  ]
  edge [
    source 44
    target 363
  ]
  edge [
    source 44
    target 364
  ]
  edge [
    source 44
    target 365
  ]
  edge [
    source 44
    target 366
  ]
  edge [
    source 44
    target 367
  ]
  edge [
    source 44
    target 368
  ]
  edge [
    source 44
    target 369
  ]
  edge [
    source 44
    target 370
  ]
  edge [
    source 44
    target 371
  ]
  edge [
    source 44
    target 372
  ]
  edge [
    source 44
    target 373
  ]
  edge [
    source 44
    target 374
  ]
  edge [
    source 44
    target 375
  ]
  edge [
    source 44
    target 376
  ]
  edge [
    source 44
    target 377
  ]
  edge [
    source 44
    target 378
  ]
  edge [
    source 44
    target 379
  ]
  edge [
    source 44
    target 380
  ]
  edge [
    source 44
    target 57
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 82
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 122
  ]
  edge [
    source 46
    target 65
  ]
  edge [
    source 46
    target 381
  ]
  edge [
    source 46
    target 382
  ]
  edge [
    source 46
    target 383
  ]
  edge [
    source 46
    target 384
  ]
  edge [
    source 46
    target 385
  ]
  edge [
    source 46
    target 386
  ]
  edge [
    source 46
    target 387
  ]
  edge [
    source 46
    target 388
  ]
  edge [
    source 46
    target 389
  ]
  edge [
    source 46
    target 390
  ]
  edge [
    source 46
    target 391
  ]
  edge [
    source 46
    target 61
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 392
  ]
  edge [
    source 47
    target 393
  ]
  edge [
    source 47
    target 394
  ]
  edge [
    source 47
    target 395
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 195
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 145
  ]
  edge [
    source 49
    target 396
  ]
  edge [
    source 49
    target 397
  ]
  edge [
    source 49
    target 398
  ]
  edge [
    source 49
    target 399
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 400
  ]
  edge [
    source 50
    target 401
  ]
  edge [
    source 50
    target 402
  ]
  edge [
    source 50
    target 403
  ]
  edge [
    source 50
    target 404
  ]
  edge [
    source 50
    target 405
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 406
  ]
  edge [
    source 51
    target 407
  ]
  edge [
    source 51
    target 408
  ]
  edge [
    source 51
    target 409
  ]
  edge [
    source 51
    target 410
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 411
  ]
  edge [
    source 52
    target 412
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 332
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 251
  ]
  edge [
    source 54
    target 252
  ]
  edge [
    source 55
    target 413
  ]
  edge [
    source 55
    target 414
  ]
  edge [
    source 55
    target 415
  ]
  edge [
    source 55
    target 416
  ]
  edge [
    source 55
    target 417
  ]
  edge [
    source 55
    target 418
  ]
  edge [
    source 55
    target 419
  ]
  edge [
    source 55
    target 225
  ]
  edge [
    source 55
    target 420
  ]
  edge [
    source 55
    target 421
  ]
  edge [
    source 55
    target 422
  ]
  edge [
    source 55
    target 423
  ]
  edge [
    source 55
    target 424
  ]
  edge [
    source 55
    target 425
  ]
  edge [
    source 55
    target 426
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 427
  ]
  edge [
    source 56
    target 83
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 428
  ]
  edge [
    source 57
    target 429
  ]
  edge [
    source 57
    target 430
  ]
  edge [
    source 57
    target 431
  ]
  edge [
    source 57
    target 432
  ]
  edge [
    source 57
    target 433
  ]
  edge [
    source 57
    target 434
  ]
  edge [
    source 57
    target 435
  ]
  edge [
    source 57
    target 436
  ]
  edge [
    source 57
    target 437
  ]
  edge [
    source 57
    target 438
  ]
  edge [
    source 57
    target 439
  ]
  edge [
    source 57
    target 440
  ]
  edge [
    source 57
    target 441
  ]
  edge [
    source 57
    target 308
  ]
  edge [
    source 57
    target 304
  ]
  edge [
    source 57
    target 442
  ]
  edge [
    source 57
    target 443
  ]
  edge [
    source 57
    target 444
  ]
  edge [
    source 57
    target 445
  ]
  edge [
    source 57
    target 418
  ]
  edge [
    source 57
    target 446
  ]
  edge [
    source 57
    target 447
  ]
  edge [
    source 57
    target 448
  ]
  edge [
    source 57
    target 449
  ]
  edge [
    source 57
    target 450
  ]
  edge [
    source 57
    target 451
  ]
  edge [
    source 57
    target 452
  ]
  edge [
    source 57
    target 453
  ]
  edge [
    source 57
    target 454
  ]
  edge [
    source 57
    target 455
  ]
  edge [
    source 57
    target 456
  ]
  edge [
    source 57
    target 457
  ]
  edge [
    source 57
    target 400
  ]
  edge [
    source 57
    target 458
  ]
  edge [
    source 57
    target 459
  ]
  edge [
    source 57
    target 460
  ]
  edge [
    source 57
    target 461
  ]
  edge [
    source 57
    target 462
  ]
  edge [
    source 57
    target 463
  ]
  edge [
    source 57
    target 464
  ]
  edge [
    source 57
    target 261
  ]
  edge [
    source 57
    target 465
  ]
  edge [
    source 57
    target 466
  ]
  edge [
    source 57
    target 467
  ]
  edge [
    source 57
    target 468
  ]
  edge [
    source 57
    target 469
  ]
  edge [
    source 57
    target 142
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 470
  ]
  edge [
    source 58
    target 471
  ]
  edge [
    source 58
    target 472
  ]
  edge [
    source 58
    target 473
  ]
  edge [
    source 58
    target 474
  ]
  edge [
    source 59
    target 68
  ]
  edge [
    source 59
    target 69
  ]
  edge [
    source 59
    target 105
  ]
  edge [
    source 59
    target 84
  ]
  edge [
    source 59
    target 134
  ]
  edge [
    source 59
    target 135
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 78
  ]
  edge [
    source 60
    target 475
  ]
  edge [
    source 60
    target 476
  ]
  edge [
    source 60
    target 477
  ]
  edge [
    source 60
    target 478
  ]
  edge [
    source 60
    target 479
  ]
  edge [
    source 60
    target 480
  ]
  edge [
    source 60
    target 481
  ]
  edge [
    source 60
    target 482
  ]
  edge [
    source 60
    target 483
  ]
  edge [
    source 60
    target 276
  ]
  edge [
    source 60
    target 484
  ]
  edge [
    source 60
    target 485
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 137
  ]
  edge [
    source 61
    target 486
  ]
  edge [
    source 61
    target 487
  ]
  edge [
    source 61
    target 488
  ]
  edge [
    source 61
    target 489
  ]
  edge [
    source 61
    target 490
  ]
  edge [
    source 61
    target 476
  ]
  edge [
    source 61
    target 491
  ]
  edge [
    source 61
    target 492
  ]
  edge [
    source 61
    target 493
  ]
  edge [
    source 61
    target 494
  ]
  edge [
    source 61
    target 495
  ]
  edge [
    source 61
    target 496
  ]
  edge [
    source 61
    target 497
  ]
  edge [
    source 61
    target 498
  ]
  edge [
    source 61
    target 499
  ]
  edge [
    source 61
    target 500
  ]
  edge [
    source 61
    target 304
  ]
  edge [
    source 61
    target 501
  ]
  edge [
    source 61
    target 502
  ]
  edge [
    source 61
    target 503
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 504
  ]
  edge [
    source 62
    target 505
  ]
  edge [
    source 62
    target 506
  ]
  edge [
    source 62
    target 507
  ]
  edge [
    source 62
    target 508
  ]
  edge [
    source 62
    target 509
  ]
  edge [
    source 62
    target 510
  ]
  edge [
    source 62
    target 511
  ]
  edge [
    source 62
    target 512
  ]
  edge [
    source 62
    target 513
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 79
  ]
  edge [
    source 63
    target 514
  ]
  edge [
    source 63
    target 515
  ]
  edge [
    source 63
    target 516
  ]
  edge [
    source 63
    target 517
  ]
  edge [
    source 63
    target 518
  ]
  edge [
    source 63
    target 519
  ]
  edge [
    source 63
    target 520
  ]
  edge [
    source 63
    target 521
  ]
  edge [
    source 63
    target 522
  ]
  edge [
    source 63
    target 523
  ]
  edge [
    source 63
    target 524
  ]
  edge [
    source 63
    target 525
  ]
  edge [
    source 63
    target 526
  ]
  edge [
    source 63
    target 527
  ]
  edge [
    source 63
    target 528
  ]
  edge [
    source 63
    target 529
  ]
  edge [
    source 63
    target 530
  ]
  edge [
    source 63
    target 531
  ]
  edge [
    source 63
    target 304
  ]
  edge [
    source 63
    target 271
  ]
  edge [
    source 63
    target 532
  ]
  edge [
    source 63
    target 533
  ]
  edge [
    source 63
    target 534
  ]
  edge [
    source 63
    target 535
  ]
  edge [
    source 63
    target 536
  ]
  edge [
    source 63
    target 537
  ]
  edge [
    source 63
    target 538
  ]
  edge [
    source 63
    target 539
  ]
  edge [
    source 63
    target 540
  ]
  edge [
    source 63
    target 541
  ]
  edge [
    source 63
    target 542
  ]
  edge [
    source 63
    target 543
  ]
  edge [
    source 63
    target 544
  ]
  edge [
    source 63
    target 476
  ]
  edge [
    source 63
    target 545
  ]
  edge [
    source 63
    target 546
  ]
  edge [
    source 63
    target 547
  ]
  edge [
    source 63
    target 548
  ]
  edge [
    source 63
    target 549
  ]
  edge [
    source 63
    target 550
  ]
  edge [
    source 63
    target 551
  ]
  edge [
    source 63
    target 552
  ]
  edge [
    source 63
    target 553
  ]
  edge [
    source 63
    target 554
  ]
  edge [
    source 63
    target 555
  ]
  edge [
    source 64
    target 139
  ]
  edge [
    source 64
    target 129
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 123
  ]
  edge [
    source 65
    target 556
  ]
  edge [
    source 65
    target 557
  ]
  edge [
    source 65
    target 558
  ]
  edge [
    source 65
    target 559
  ]
  edge [
    source 65
    target 560
  ]
  edge [
    source 65
    target 561
  ]
  edge [
    source 65
    target 562
  ]
  edge [
    source 65
    target 563
  ]
  edge [
    source 65
    target 564
  ]
  edge [
    source 65
    target 565
  ]
  edge [
    source 65
    target 566
  ]
  edge [
    source 65
    target 291
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 66
    target 145
  ]
  edge [
    source 66
    target 567
  ]
  edge [
    source 66
    target 568
  ]
  edge [
    source 66
    target 569
  ]
  edge [
    source 66
    target 94
  ]
  edge [
    source 68
    target 570
  ]
  edge [
    source 68
    target 571
  ]
  edge [
    source 68
    target 572
  ]
  edge [
    source 68
    target 573
  ]
  edge [
    source 68
    target 574
  ]
  edge [
    source 68
    target 575
  ]
  edge [
    source 68
    target 576
  ]
  edge [
    source 68
    target 577
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 69
    target 578
  ]
  edge [
    source 69
    target 579
  ]
  edge [
    source 69
    target 472
  ]
  edge [
    source 69
    target 580
  ]
  edge [
    source 69
    target 581
  ]
  edge [
    source 69
    target 582
  ]
  edge [
    source 69
    target 583
  ]
  edge [
    source 69
    target 584
  ]
  edge [
    source 69
    target 585
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 86
  ]
  edge [
    source 70
    target 87
  ]
  edge [
    source 70
    target 586
  ]
  edge [
    source 70
    target 145
  ]
  edge [
    source 70
    target 587
  ]
  edge [
    source 70
    target 588
  ]
  edge [
    source 70
    target 589
  ]
  edge [
    source 70
    target 590
  ]
  edge [
    source 70
    target 591
  ]
  edge [
    source 70
    target 592
  ]
  edge [
    source 70
    target 593
  ]
  edge [
    source 70
    target 432
  ]
  edge [
    source 70
    target 466
  ]
  edge [
    source 70
    target 594
  ]
  edge [
    source 70
    target 595
  ]
  edge [
    source 70
    target 596
  ]
  edge [
    source 70
    target 597
  ]
  edge [
    source 70
    target 598
  ]
  edge [
    source 70
    target 599
  ]
  edge [
    source 70
    target 600
  ]
  edge [
    source 70
    target 72
  ]
  edge [
    source 70
    target 134
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 72
    target 601
  ]
  edge [
    source 72
    target 602
  ]
  edge [
    source 72
    target 603
  ]
  edge [
    source 72
    target 134
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 73
    target 604
  ]
  edge [
    source 73
    target 605
  ]
  edge [
    source 73
    target 606
  ]
  edge [
    source 73
    target 607
  ]
  edge [
    source 73
    target 608
  ]
  edge [
    source 73
    target 609
  ]
  edge [
    source 73
    target 427
  ]
  edge [
    source 73
    target 610
  ]
  edge [
    source 73
    target 611
  ]
  edge [
    source 73
    target 612
  ]
  edge [
    source 73
    target 613
  ]
  edge [
    source 73
    target 614
  ]
  edge [
    source 73
    target 615
  ]
  edge [
    source 73
    target 616
  ]
  edge [
    source 73
    target 617
  ]
  edge [
    source 73
    target 618
  ]
  edge [
    source 73
    target 619
  ]
  edge [
    source 73
    target 620
  ]
  edge [
    source 73
    target 621
  ]
  edge [
    source 73
    target 622
  ]
  edge [
    source 73
    target 623
  ]
  edge [
    source 73
    target 624
  ]
  edge [
    source 73
    target 625
  ]
  edge [
    source 73
    target 626
  ]
  edge [
    source 73
    target 627
  ]
  edge [
    source 73
    target 628
  ]
  edge [
    source 73
    target 629
  ]
  edge [
    source 73
    target 630
  ]
  edge [
    source 73
    target 631
  ]
  edge [
    source 73
    target 393
  ]
  edge [
    source 73
    target 632
  ]
  edge [
    source 73
    target 633
  ]
  edge [
    source 73
    target 634
  ]
  edge [
    source 73
    target 635
  ]
  edge [
    source 73
    target 636
  ]
  edge [
    source 73
    target 637
  ]
  edge [
    source 73
    target 638
  ]
  edge [
    source 73
    target 639
  ]
  edge [
    source 73
    target 640
  ]
  edge [
    source 73
    target 641
  ]
  edge [
    source 73
    target 642
  ]
  edge [
    source 73
    target 643
  ]
  edge [
    source 73
    target 644
  ]
  edge [
    source 73
    target 98
  ]
  edge [
    source 74
    target 75
  ]
  edge [
    source 74
    target 445
  ]
  edge [
    source 74
    target 645
  ]
  edge [
    source 74
    target 646
  ]
  edge [
    source 74
    target 647
  ]
  edge [
    source 74
    target 648
  ]
  edge [
    source 74
    target 574
  ]
  edge [
    source 74
    target 649
  ]
  edge [
    source 74
    target 650
  ]
  edge [
    source 74
    target 651
  ]
  edge [
    source 74
    target 652
  ]
  edge [
    source 74
    target 82
  ]
  edge [
    source 75
    target 76
  ]
  edge [
    source 75
    target 99
  ]
  edge [
    source 76
    target 77
  ]
  edge [
    source 76
    target 653
  ]
  edge [
    source 76
    target 654
  ]
  edge [
    source 76
    target 655
  ]
  edge [
    source 76
    target 656
  ]
  edge [
    source 76
    target 657
  ]
  edge [
    source 76
    target 658
  ]
  edge [
    source 76
    target 659
  ]
  edge [
    source 76
    target 660
  ]
  edge [
    source 76
    target 661
  ]
  edge [
    source 76
    target 662
  ]
  edge [
    source 76
    target 663
  ]
  edge [
    source 76
    target 664
  ]
  edge [
    source 76
    target 665
  ]
  edge [
    source 76
    target 598
  ]
  edge [
    source 76
    target 666
  ]
  edge [
    source 76
    target 667
  ]
  edge [
    source 76
    target 668
  ]
  edge [
    source 76
    target 627
  ]
  edge [
    source 76
    target 669
  ]
  edge [
    source 76
    target 418
  ]
  edge [
    source 76
    target 670
  ]
  edge [
    source 76
    target 671
  ]
  edge [
    source 76
    target 672
  ]
  edge [
    source 76
    target 673
  ]
  edge [
    source 76
    target 674
  ]
  edge [
    source 76
    target 675
  ]
  edge [
    source 76
    target 676
  ]
  edge [
    source 76
    target 677
  ]
  edge [
    source 76
    target 678
  ]
  edge [
    source 76
    target 679
  ]
  edge [
    source 76
    target 680
  ]
  edge [
    source 76
    target 681
  ]
  edge [
    source 76
    target 682
  ]
  edge [
    source 76
    target 683
  ]
  edge [
    source 76
    target 684
  ]
  edge [
    source 77
    target 78
  ]
  edge [
    source 77
    target 685
  ]
  edge [
    source 77
    target 686
  ]
  edge [
    source 77
    target 687
  ]
  edge [
    source 77
    target 102
  ]
  edge [
    source 78
    target 688
  ]
  edge [
    source 78
    target 486
  ]
  edge [
    source 78
    target 89
  ]
  edge [
    source 79
    target 80
  ]
  edge [
    source 79
    target 689
  ]
  edge [
    source 79
    target 690
  ]
  edge [
    source 79
    target 691
  ]
  edge [
    source 79
    target 692
  ]
  edge [
    source 79
    target 693
  ]
  edge [
    source 79
    target 281
  ]
  edge [
    source 79
    target 694
  ]
  edge [
    source 79
    target 695
  ]
  edge [
    source 79
    target 696
  ]
  edge [
    source 80
    target 81
  ]
  edge [
    source 80
    target 697
  ]
  edge [
    source 80
    target 698
  ]
  edge [
    source 80
    target 699
  ]
  edge [
    source 80
    target 700
  ]
  edge [
    source 80
    target 591
  ]
  edge [
    source 80
    target 701
  ]
  edge [
    source 80
    target 702
  ]
  edge [
    source 80
    target 89
  ]
  edge [
    source 80
    target 703
  ]
  edge [
    source 80
    target 704
  ]
  edge [
    source 80
    target 705
  ]
  edge [
    source 81
    target 82
  ]
  edge [
    source 82
    target 83
  ]
  edge [
    source 82
    target 98
  ]
  edge [
    source 82
    target 99
  ]
  edge [
    source 82
    target 706
  ]
  edge [
    source 82
    target 707
  ]
  edge [
    source 82
    target 708
  ]
  edge [
    source 82
    target 709
  ]
  edge [
    source 82
    target 710
  ]
  edge [
    source 82
    target 711
  ]
  edge [
    source 83
    target 84
  ]
  edge [
    source 83
    target 712
  ]
  edge [
    source 83
    target 713
  ]
  edge [
    source 83
    target 714
  ]
  edge [
    source 83
    target 715
  ]
  edge [
    source 84
    target 85
  ]
  edge [
    source 84
    target 106
  ]
  edge [
    source 85
    target 86
  ]
  edge [
    source 85
    target 716
  ]
  edge [
    source 85
    target 717
  ]
  edge [
    source 85
    target 718
  ]
  edge [
    source 85
    target 719
  ]
  edge [
    source 85
    target 720
  ]
  edge [
    source 86
    target 460
  ]
  edge [
    source 86
    target 721
  ]
  edge [
    source 87
    target 88
  ]
  edge [
    source 87
    target 722
  ]
  edge [
    source 87
    target 723
  ]
  edge [
    source 87
    target 474
  ]
  edge [
    source 88
    target 89
  ]
  edge [
    source 88
    target 715
  ]
  edge [
    source 88
    target 114
  ]
  edge [
    source 89
    target 90
  ]
  edge [
    source 89
    target 724
  ]
  edge [
    source 89
    target 725
  ]
  edge [
    source 89
    target 726
  ]
  edge [
    source 89
    target 727
  ]
  edge [
    source 89
    target 728
  ]
  edge [
    source 89
    target 729
  ]
  edge [
    source 89
    target 730
  ]
  edge [
    source 89
    target 731
  ]
  edge [
    source 89
    target 732
  ]
  edge [
    source 89
    target 733
  ]
  edge [
    source 89
    target 734
  ]
  edge [
    source 89
    target 735
  ]
  edge [
    source 89
    target 330
  ]
  edge [
    source 89
    target 736
  ]
  edge [
    source 90
    target 91
  ]
  edge [
    source 91
    target 92
  ]
  edge [
    source 91
    target 123
  ]
  edge [
    source 92
    target 93
  ]
  edge [
    source 92
    target 737
  ]
  edge [
    source 92
    target 738
  ]
  edge [
    source 92
    target 739
  ]
  edge [
    source 92
    target 740
  ]
  edge [
    source 92
    target 741
  ]
  edge [
    source 92
    target 742
  ]
  edge [
    source 92
    target 144
  ]
  edge [
    source 92
    target 743
  ]
  edge [
    source 92
    target 744
  ]
  edge [
    source 92
    target 745
  ]
  edge [
    source 92
    target 746
  ]
  edge [
    source 92
    target 712
  ]
  edge [
    source 92
    target 747
  ]
  edge [
    source 92
    target 748
  ]
  edge [
    source 92
    target 749
  ]
  edge [
    source 93
    target 94
  ]
  edge [
    source 94
    target 95
  ]
  edge [
    source 94
    target 750
  ]
  edge [
    source 94
    target 751
  ]
  edge [
    source 94
    target 752
  ]
  edge [
    source 94
    target 753
  ]
  edge [
    source 94
    target 754
  ]
  edge [
    source 94
    target 574
  ]
  edge [
    source 94
    target 755
  ]
  edge [
    source 95
    target 96
  ]
  edge [
    source 95
    target 756
  ]
  edge [
    source 95
    target 405
  ]
  edge [
    source 95
    target 757
  ]
  edge [
    source 96
    target 97
  ]
  edge [
    source 96
    target 272
  ]
  edge [
    source 96
    target 273
  ]
  edge [
    source 96
    target 274
  ]
  edge [
    source 96
    target 275
  ]
  edge [
    source 96
    target 276
  ]
  edge [
    source 96
    target 277
  ]
  edge [
    source 97
    target 98
  ]
  edge [
    source 97
    target 758
  ]
  edge [
    source 97
    target 759
  ]
  edge [
    source 97
    target 760
  ]
  edge [
    source 98
    target 604
  ]
  edge [
    source 98
    target 616
  ]
  edge [
    source 98
    target 644
  ]
  edge [
    source 98
    target 618
  ]
  edge [
    source 99
    target 100
  ]
  edge [
    source 99
    target 761
  ]
  edge [
    source 99
    target 762
  ]
  edge [
    source 99
    target 486
  ]
  edge [
    source 99
    target 763
  ]
  edge [
    source 100
    target 101
  ]
  edge [
    source 100
    target 764
  ]
  edge [
    source 100
    target 765
  ]
  edge [
    source 100
    target 766
  ]
  edge [
    source 100
    target 767
  ]
  edge [
    source 100
    target 768
  ]
  edge [
    source 101
    target 102
  ]
  edge [
    source 101
    target 355
  ]
  edge [
    source 101
    target 769
  ]
  edge [
    source 101
    target 770
  ]
  edge [
    source 101
    target 735
  ]
  edge [
    source 101
    target 771
  ]
  edge [
    source 102
    target 772
  ]
  edge [
    source 102
    target 773
  ]
  edge [
    source 102
    target 774
  ]
  edge [
    source 102
    target 775
  ]
  edge [
    source 102
    target 776
  ]
  edge [
    source 103
    target 104
  ]
  edge [
    source 103
    target 777
  ]
  edge [
    source 103
    target 778
  ]
  edge [
    source 103
    target 779
  ]
  edge [
    source 103
    target 780
  ]
  edge [
    source 103
    target 781
  ]
  edge [
    source 103
    target 782
  ]
  edge [
    source 103
    target 783
  ]
  edge [
    source 103
    target 735
  ]
  edge [
    source 103
    target 784
  ]
  edge [
    source 104
    target 105
  ]
  edge [
    source 104
    target 785
  ]
  edge [
    source 104
    target 786
  ]
  edge [
    source 105
    target 787
  ]
  edge [
    source 105
    target 788
  ]
  edge [
    source 106
    target 107
  ]
  edge [
    source 106
    target 789
  ]
  edge [
    source 106
    target 125
  ]
  edge [
    source 107
    target 108
  ]
  edge [
    source 107
    target 790
  ]
  edge [
    source 107
    target 791
  ]
  edge [
    source 107
    target 789
  ]
  edge [
    source 107
    target 792
  ]
  edge [
    source 108
    target 793
  ]
  edge [
    source 108
    target 794
  ]
  edge [
    source 108
    target 795
  ]
  edge [
    source 108
    target 796
  ]
  edge [
    source 108
    target 418
  ]
  edge [
    source 108
    target 797
  ]
  edge [
    source 108
    target 798
  ]
  edge [
    source 108
    target 799
  ]
  edge [
    source 108
    target 800
  ]
  edge [
    source 108
    target 801
  ]
  edge [
    source 108
    target 135
  ]
  edge [
    source 109
    target 110
  ]
  edge [
    source 109
    target 251
  ]
  edge [
    source 109
    target 802
  ]
  edge [
    source 110
    target 111
  ]
  edge [
    source 111
    target 112
  ]
  edge [
    source 112
    target 803
  ]
  edge [
    source 112
    target 804
  ]
  edge [
    source 112
    target 805
  ]
  edge [
    source 112
    target 806
  ]
  edge [
    source 112
    target 807
  ]
  edge [
    source 112
    target 808
  ]
  edge [
    source 112
    target 809
  ]
  edge [
    source 112
    target 810
  ]
  edge [
    source 113
    target 114
  ]
  edge [
    source 114
    target 115
  ]
  edge [
    source 114
    target 811
  ]
  edge [
    source 115
    target 116
  ]
  edge [
    source 115
    target 812
  ]
  edge [
    source 115
    target 813
  ]
  edge [
    source 116
    target 117
  ]
  edge [
    source 116
    target 814
  ]
  edge [
    source 116
    target 815
  ]
  edge [
    source 116
    target 816
  ]
  edge [
    source 116
    target 817
  ]
  edge [
    source 116
    target 818
  ]
  edge [
    source 116
    target 819
  ]
  edge [
    source 116
    target 820
  ]
  edge [
    source 116
    target 821
  ]
  edge [
    source 116
    target 822
  ]
  edge [
    source 116
    target 823
  ]
  edge [
    source 116
    target 824
  ]
  edge [
    source 116
    target 825
  ]
  edge [
    source 116
    target 826
  ]
  edge [
    source 116
    target 827
  ]
  edge [
    source 116
    target 828
  ]
  edge [
    source 116
    target 237
  ]
  edge [
    source 117
    target 118
  ]
  edge [
    source 117
    target 829
  ]
  edge [
    source 117
    target 830
  ]
  edge [
    source 117
    target 831
  ]
  edge [
    source 117
    target 832
  ]
  edge [
    source 117
    target 833
  ]
  edge [
    source 117
    target 834
  ]
  edge [
    source 117
    target 835
  ]
  edge [
    source 117
    target 836
  ]
  edge [
    source 117
    target 837
  ]
  edge [
    source 117
    target 838
  ]
  edge [
    source 117
    target 839
  ]
  edge [
    source 117
    target 840
  ]
  edge [
    source 117
    target 841
  ]
  edge [
    source 117
    target 842
  ]
  edge [
    source 117
    target 843
  ]
  edge [
    source 118
    target 844
  ]
  edge [
    source 118
    target 845
  ]
  edge [
    source 118
    target 292
  ]
  edge [
    source 118
    target 846
  ]
  edge [
    source 119
    target 120
  ]
  edge [
    source 119
    target 847
  ]
  edge [
    source 120
    target 121
  ]
  edge [
    source 120
    target 848
  ]
  edge [
    source 120
    target 849
  ]
  edge [
    source 120
    target 850
  ]
  edge [
    source 120
    target 851
  ]
  edge [
    source 120
    target 852
  ]
  edge [
    source 120
    target 827
  ]
  edge [
    source 120
    target 853
  ]
  edge [
    source 120
    target 854
  ]
  edge [
    source 120
    target 436
  ]
  edge [
    source 120
    target 855
  ]
  edge [
    source 120
    target 856
  ]
  edge [
    source 120
    target 857
  ]
  edge [
    source 120
    target 858
  ]
  edge [
    source 120
    target 859
  ]
  edge [
    source 120
    target 860
  ]
  edge [
    source 120
    target 861
  ]
  edge [
    source 120
    target 862
  ]
  edge [
    source 120
    target 863
  ]
  edge [
    source 120
    target 864
  ]
  edge [
    source 120
    target 865
  ]
  edge [
    source 120
    target 866
  ]
  edge [
    source 120
    target 867
  ]
  edge [
    source 121
    target 122
  ]
  edge [
    source 121
    target 868
  ]
  edge [
    source 123
    target 145
  ]
  edge [
    source 123
    target 869
  ]
  edge [
    source 123
    target 870
  ]
  edge [
    source 123
    target 871
  ]
  edge [
    source 123
    target 872
  ]
  edge [
    source 123
    target 873
  ]
  edge [
    source 123
    target 874
  ]
  edge [
    source 123
    target 875
  ]
  edge [
    source 123
    target 876
  ]
  edge [
    source 123
    target 877
  ]
  edge [
    source 123
    target 878
  ]
  edge [
    source 123
    target 879
  ]
  edge [
    source 123
    target 880
  ]
  edge [
    source 123
    target 881
  ]
  edge [
    source 123
    target 882
  ]
  edge [
    source 123
    target 883
  ]
  edge [
    source 124
    target 125
  ]
  edge [
    source 124
    target 884
  ]
  edge [
    source 125
    target 126
  ]
  edge [
    source 125
    target 885
  ]
  edge [
    source 125
    target 742
  ]
  edge [
    source 125
    target 886
  ]
  edge [
    source 125
    target 393
  ]
  edge [
    source 125
    target 887
  ]
  edge [
    source 125
    target 888
  ]
  edge [
    source 126
    target 127
  ]
  edge [
    source 126
    target 889
  ]
  edge [
    source 126
    target 890
  ]
  edge [
    source 126
    target 891
  ]
  edge [
    source 126
    target 892
  ]
  edge [
    source 126
    target 714
  ]
  edge [
    source 126
    target 279
  ]
  edge [
    source 127
    target 128
  ]
  edge [
    source 127
    target 138
  ]
  edge [
    source 128
    target 129
  ]
  edge [
    source 129
    target 130
  ]
  edge [
    source 129
    target 140
  ]
  edge [
    source 129
    target 893
  ]
  edge [
    source 129
    target 894
  ]
  edge [
    source 129
    target 742
  ]
  edge [
    source 129
    target 895
  ]
  edge [
    source 130
    target 896
  ]
  edge [
    source 130
    target 897
  ]
  edge [
    source 131
    target 132
  ]
  edge [
    source 131
    target 898
  ]
  edge [
    source 131
    target 899
  ]
  edge [
    source 131
    target 900
  ]
  edge [
    source 131
    target 370
  ]
  edge [
    source 131
    target 901
  ]
  edge [
    source 131
    target 902
  ]
  edge [
    source 131
    target 903
  ]
  edge [
    source 131
    target 904
  ]
  edge [
    source 131
    target 905
  ]
  edge [
    source 131
    target 906
  ]
  edge [
    source 131
    target 284
  ]
  edge [
    source 131
    target 574
  ]
  edge [
    source 131
    target 311
  ]
  edge [
    source 131
    target 907
  ]
  edge [
    source 131
    target 350
  ]
  edge [
    source 131
    target 908
  ]
  edge [
    source 131
    target 909
  ]
  edge [
    source 131
    target 910
  ]
  edge [
    source 132
    target 911
  ]
  edge [
    source 132
    target 727
  ]
  edge [
    source 132
    target 192
  ]
  edge [
    source 132
    target 912
  ]
  edge [
    source 132
    target 913
  ]
  edge [
    source 132
    target 762
  ]
  edge [
    source 132
    target 197
  ]
  edge [
    source 132
    target 914
  ]
  edge [
    source 132
    target 915
  ]
  edge [
    source 132
    target 916
  ]
  edge [
    source 132
    target 917
  ]
  edge [
    source 132
    target 918
  ]
  edge [
    source 132
    target 919
  ]
  edge [
    source 132
    target 920
  ]
  edge [
    source 132
    target 921
  ]
  edge [
    source 132
    target 922
  ]
  edge [
    source 132
    target 923
  ]
  edge [
    source 132
    target 924
  ]
  edge [
    source 132
    target 925
  ]
  edge [
    source 132
    target 926
  ]
  edge [
    source 132
    target 927
  ]
  edge [
    source 132
    target 928
  ]
  edge [
    source 132
    target 929
  ]
  edge [
    source 132
    target 189
  ]
  edge [
    source 132
    target 930
  ]
  edge [
    source 132
    target 931
  ]
  edge [
    source 132
    target 932
  ]
  edge [
    source 132
    target 933
  ]
  edge [
    source 132
    target 934
  ]
  edge [
    source 132
    target 935
  ]
  edge [
    source 132
    target 936
  ]
  edge [
    source 132
    target 937
  ]
  edge [
    source 132
    target 938
  ]
  edge [
    source 132
    target 939
  ]
  edge [
    source 132
    target 940
  ]
  edge [
    source 132
    target 941
  ]
  edge [
    source 132
    target 942
  ]
  edge [
    source 132
    target 943
  ]
  edge [
    source 132
    target 944
  ]
  edge [
    source 132
    target 945
  ]
  edge [
    source 133
    target 134
  ]
  edge [
    source 134
    target 604
  ]
  edge [
    source 134
    target 946
  ]
  edge [
    source 134
    target 644
  ]
  edge [
    source 134
    target 393
  ]
  edge [
    source 134
    target 947
  ]
  edge [
    source 134
    target 948
  ]
  edge [
    source 134
    target 949
  ]
  edge [
    source 134
    target 950
  ]
  edge [
    source 135
    target 136
  ]
  edge [
    source 135
    target 951
  ]
  edge [
    source 135
    target 952
  ]
  edge [
    source 135
    target 953
  ]
  edge [
    source 135
    target 954
  ]
  edge [
    source 135
    target 955
  ]
  edge [
    source 135
    target 756
  ]
  edge [
    source 135
    target 956
  ]
  edge [
    source 135
    target 957
  ]
  edge [
    source 135
    target 958
  ]
  edge [
    source 136
    target 137
  ]
  edge [
    source 136
    target 959
  ]
  edge [
    source 136
    target 960
  ]
  edge [
    source 137
    target 961
  ]
  edge [
    source 137
    target 962
  ]
  edge [
    source 137
    target 963
  ]
  edge [
    source 137
    target 964
  ]
  edge [
    source 137
    target 965
  ]
  edge [
    source 137
    target 966
  ]
  edge [
    source 137
    target 967
  ]
  edge [
    source 138
    target 139
  ]
  edge [
    source 138
    target 968
  ]
  edge [
    source 138
    target 969
  ]
  edge [
    source 138
    target 970
  ]
  edge [
    source 138
    target 971
  ]
  edge [
    source 138
    target 972
  ]
  edge [
    source 139
    target 973
  ]
  edge [
    source 139
    target 974
  ]
  edge [
    source 139
    target 975
  ]
  edge [
    source 139
    target 976
  ]
  edge [
    source 139
    target 977
  ]
  edge [
    source 139
    target 978
  ]
  edge [
    source 139
    target 979
  ]
  edge [
    source 139
    target 980
  ]
  edge [
    source 139
    target 981
  ]
  edge [
    source 140
    target 141
  ]
  edge [
    source 140
    target 982
  ]
  edge [
    source 140
    target 983
  ]
  edge [
    source 140
    target 984
  ]
  edge [
    source 140
    target 985
  ]
  edge [
    source 140
    target 986
  ]
  edge [
    source 140
    target 987
  ]
  edge [
    source 140
    target 988
  ]
  edge [
    source 140
    target 989
  ]
  edge [
    source 140
    target 990
  ]
  edge [
    source 140
    target 991
  ]
  edge [
    source 140
    target 992
  ]
  edge [
    source 140
    target 993
  ]
  edge [
    source 141
    target 142
  ]
  edge [
    source 141
    target 994
  ]
  edge [
    source 141
    target 995
  ]
  edge [
    source 141
    target 996
  ]
  edge [
    source 142
    target 997
  ]
  edge [
    source 142
    target 998
  ]
  edge [
    source 142
    target 999
  ]
  edge [
    source 142
    target 1000
  ]
  edge [
    source 142
    target 1001
  ]
  edge [
    source 142
    target 1002
  ]
  edge [
    source 142
    target 1003
  ]
  edge [
    source 142
    target 1004
  ]
  edge [
    source 142
    target 1005
  ]
  edge [
    source 142
    target 1006
  ]
]
