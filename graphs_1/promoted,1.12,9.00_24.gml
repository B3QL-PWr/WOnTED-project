graph [
  maxDegree 42
  minDegree 1
  meanDegree 2.0330578512396693
  density 0.016942148760330577
  graphCliqueNumber 3
  node [
    id 0
    label "letni"
    origin "text"
  ]
  node [
    id 1
    label "yara"
    origin "text"
  ]
  node [
    id 2
    label "zacz&#261;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "rok"
    origin "text"
  ]
  node [
    id 4
    label "szkolny"
    origin "text"
  ]
  node [
    id 5
    label "klasa"
    origin "text"
  ]
  node [
    id 6
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 7
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 8
    label "niemiecki"
    origin "text"
  ]
  node [
    id 9
    label "nijaki"
  ]
  node [
    id 10
    label "sezonowy"
  ]
  node [
    id 11
    label "letnio"
  ]
  node [
    id 12
    label "s&#322;oneczny"
  ]
  node [
    id 13
    label "weso&#322;y"
  ]
  node [
    id 14
    label "oboj&#281;tny"
  ]
  node [
    id 15
    label "latowy"
  ]
  node [
    id 16
    label "ciep&#322;y"
  ]
  node [
    id 17
    label "typowy"
  ]
  node [
    id 18
    label "cause"
  ]
  node [
    id 19
    label "introduce"
  ]
  node [
    id 20
    label "begin"
  ]
  node [
    id 21
    label "odj&#261;&#263;"
  ]
  node [
    id 22
    label "post&#261;pi&#263;"
  ]
  node [
    id 23
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 24
    label "do"
  ]
  node [
    id 25
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 26
    label "zrobi&#263;"
  ]
  node [
    id 27
    label "stulecie"
  ]
  node [
    id 28
    label "kalendarz"
  ]
  node [
    id 29
    label "czas"
  ]
  node [
    id 30
    label "pora_roku"
  ]
  node [
    id 31
    label "cykl_astronomiczny"
  ]
  node [
    id 32
    label "p&#243;&#322;rocze"
  ]
  node [
    id 33
    label "grupa"
  ]
  node [
    id 34
    label "kwarta&#322;"
  ]
  node [
    id 35
    label "kurs"
  ]
  node [
    id 36
    label "jubileusz"
  ]
  node [
    id 37
    label "miesi&#261;c"
  ]
  node [
    id 38
    label "lata"
  ]
  node [
    id 39
    label "martwy_sezon"
  ]
  node [
    id 40
    label "szkolnie"
  ]
  node [
    id 41
    label "szkoleniowy"
  ]
  node [
    id 42
    label "podstawowy"
  ]
  node [
    id 43
    label "prosty"
  ]
  node [
    id 44
    label "typ"
  ]
  node [
    id 45
    label "warstwa"
  ]
  node [
    id 46
    label "znak_jako&#347;ci"
  ]
  node [
    id 47
    label "przedmiot"
  ]
  node [
    id 48
    label "przepisa&#263;"
  ]
  node [
    id 49
    label "pomoc"
  ]
  node [
    id 50
    label "arrangement"
  ]
  node [
    id 51
    label "wagon"
  ]
  node [
    id 52
    label "form"
  ]
  node [
    id 53
    label "zaleta"
  ]
  node [
    id 54
    label "poziom"
  ]
  node [
    id 55
    label "dziennik_lekcyjny"
  ]
  node [
    id 56
    label "&#347;rodowisko"
  ]
  node [
    id 57
    label "atak"
  ]
  node [
    id 58
    label "przepisanie"
  ]
  node [
    id 59
    label "szko&#322;a"
  ]
  node [
    id 60
    label "class"
  ]
  node [
    id 61
    label "organizacja"
  ]
  node [
    id 62
    label "obrona"
  ]
  node [
    id 63
    label "type"
  ]
  node [
    id 64
    label "promocja"
  ]
  node [
    id 65
    label "&#322;awka"
  ]
  node [
    id 66
    label "botanika"
  ]
  node [
    id 67
    label "sala"
  ]
  node [
    id 68
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 69
    label "gromada"
  ]
  node [
    id 70
    label "obiekt"
  ]
  node [
    id 71
    label "Ekwici"
  ]
  node [
    id 72
    label "fakcja"
  ]
  node [
    id 73
    label "tablica"
  ]
  node [
    id 74
    label "programowanie_obiektowe"
  ]
  node [
    id 75
    label "wykrzyknik"
  ]
  node [
    id 76
    label "jednostka_systematyczna"
  ]
  node [
    id 77
    label "mecz_mistrzowski"
  ]
  node [
    id 78
    label "zbi&#243;r"
  ]
  node [
    id 79
    label "jako&#347;&#263;"
  ]
  node [
    id 80
    label "rezerwa"
  ]
  node [
    id 81
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 82
    label "remark"
  ]
  node [
    id 83
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 84
    label "u&#380;ywa&#263;"
  ]
  node [
    id 85
    label "okre&#347;la&#263;"
  ]
  node [
    id 86
    label "j&#281;zyk"
  ]
  node [
    id 87
    label "say"
  ]
  node [
    id 88
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 89
    label "formu&#322;owa&#263;"
  ]
  node [
    id 90
    label "talk"
  ]
  node [
    id 91
    label "powiada&#263;"
  ]
  node [
    id 92
    label "informowa&#263;"
  ]
  node [
    id 93
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 94
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 95
    label "wydobywa&#263;"
  ]
  node [
    id 96
    label "express"
  ]
  node [
    id 97
    label "chew_the_fat"
  ]
  node [
    id 98
    label "dysfonia"
  ]
  node [
    id 99
    label "umie&#263;"
  ]
  node [
    id 100
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 101
    label "tell"
  ]
  node [
    id 102
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 103
    label "wyra&#380;a&#263;"
  ]
  node [
    id 104
    label "gaworzy&#263;"
  ]
  node [
    id 105
    label "rozmawia&#263;"
  ]
  node [
    id 106
    label "dziama&#263;"
  ]
  node [
    id 107
    label "prawi&#263;"
  ]
  node [
    id 108
    label "szwabski"
  ]
  node [
    id 109
    label "po_niemiecku"
  ]
  node [
    id 110
    label "niemiec"
  ]
  node [
    id 111
    label "cenar"
  ]
  node [
    id 112
    label "europejski"
  ]
  node [
    id 113
    label "German"
  ]
  node [
    id 114
    label "pionier"
  ]
  node [
    id 115
    label "niemiecko"
  ]
  node [
    id 116
    label "j&#281;zyk_zachodniogerma&#324;ski"
  ]
  node [
    id 117
    label "zachodnioeuropejski"
  ]
  node [
    id 118
    label "strudel"
  ]
  node [
    id 119
    label "junkers"
  ]
  node [
    id 120
    label "&#347;rodkowoeuropejski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
]
