graph [
  maxDegree 52
  minDegree 1
  meanDegree 2.050955414012739
  density 0.01314715008982525
  graphCliqueNumber 2
  node [
    id 0
    label "rzeczywi&#347;cie"
    origin "text"
  ]
  node [
    id 1
    label "wczoraj"
    origin "text"
  ]
  node [
    id 2
    label "min&#261;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "rok"
    origin "text"
  ]
  node [
    id 4
    label "wiedzie&#263;"
    origin "text"
  ]
  node [
    id 5
    label "bardzo"
    origin "text"
  ]
  node [
    id 6
    label "napisa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "pami&#281;ta&#263;"
    origin "text"
  ]
  node [
    id 8
    label "byby&#263;"
    origin "text"
  ]
  node [
    id 9
    label "deklarowa&#263;"
    origin "text"
  ]
  node [
    id 10
    label "si&#281;"
    origin "text"
  ]
  node [
    id 11
    label "pokaza&#263;"
    origin "text"
  ]
  node [
    id 12
    label "forma"
    origin "text"
  ]
  node [
    id 13
    label "tylko"
    origin "text"
  ]
  node [
    id 14
    label "us&#322;ysze&#263;by&#263;"
    origin "text"
  ]
  node [
    id 15
    label "nie"
    origin "text"
  ]
  node [
    id 16
    label "dama"
    origin "text"
  ]
  node [
    id 17
    label "taki"
    origin "text"
  ]
  node [
    id 18
    label "z&#322;apa&#263;"
    origin "text"
  ]
  node [
    id 19
    label "realny"
  ]
  node [
    id 20
    label "naprawd&#281;"
  ]
  node [
    id 21
    label "doba"
  ]
  node [
    id 22
    label "dawno"
  ]
  node [
    id 23
    label "niedawno"
  ]
  node [
    id 24
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 25
    label "omin&#261;&#263;"
  ]
  node [
    id 26
    label "spowodowa&#263;"
  ]
  node [
    id 27
    label "run"
  ]
  node [
    id 28
    label "przesta&#263;"
  ]
  node [
    id 29
    label "przej&#347;&#263;"
  ]
  node [
    id 30
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 31
    label "die"
  ]
  node [
    id 32
    label "overwhelm"
  ]
  node [
    id 33
    label "stulecie"
  ]
  node [
    id 34
    label "kalendarz"
  ]
  node [
    id 35
    label "czas"
  ]
  node [
    id 36
    label "pora_roku"
  ]
  node [
    id 37
    label "cykl_astronomiczny"
  ]
  node [
    id 38
    label "p&#243;&#322;rocze"
  ]
  node [
    id 39
    label "grupa"
  ]
  node [
    id 40
    label "kwarta&#322;"
  ]
  node [
    id 41
    label "kurs"
  ]
  node [
    id 42
    label "jubileusz"
  ]
  node [
    id 43
    label "miesi&#261;c"
  ]
  node [
    id 44
    label "lata"
  ]
  node [
    id 45
    label "martwy_sezon"
  ]
  node [
    id 46
    label "cognizance"
  ]
  node [
    id 47
    label "w_chuj"
  ]
  node [
    id 48
    label "postawi&#263;"
  ]
  node [
    id 49
    label "prasa"
  ]
  node [
    id 50
    label "stworzy&#263;"
  ]
  node [
    id 51
    label "donie&#347;&#263;"
  ]
  node [
    id 52
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 53
    label "write"
  ]
  node [
    id 54
    label "styl"
  ]
  node [
    id 55
    label "read"
  ]
  node [
    id 56
    label "si&#281;ga&#263;"
  ]
  node [
    id 57
    label "zna&#263;"
  ]
  node [
    id 58
    label "troska&#263;_si&#281;"
  ]
  node [
    id 59
    label "zachowywa&#263;"
  ]
  node [
    id 60
    label "chowa&#263;"
  ]
  node [
    id 61
    label "think"
  ]
  node [
    id 62
    label "pilnowa&#263;"
  ]
  node [
    id 63
    label "robi&#263;"
  ]
  node [
    id 64
    label "recall"
  ]
  node [
    id 65
    label "echo"
  ]
  node [
    id 66
    label "take_care"
  ]
  node [
    id 67
    label "zapewnia&#263;"
  ]
  node [
    id 68
    label "poda&#263;"
  ]
  node [
    id 69
    label "obiecywa&#263;"
  ]
  node [
    id 70
    label "obieca&#263;"
  ]
  node [
    id 71
    label "bespeak"
  ]
  node [
    id 72
    label "sign"
  ]
  node [
    id 73
    label "podawa&#263;"
  ]
  node [
    id 74
    label "zapewni&#263;"
  ]
  node [
    id 75
    label "wyrazi&#263;"
  ]
  node [
    id 76
    label "da&#263;_w_ko&#347;&#263;"
  ]
  node [
    id 77
    label "przedstawi&#263;"
  ]
  node [
    id 78
    label "testify"
  ]
  node [
    id 79
    label "indicate"
  ]
  node [
    id 80
    label "przeszkoli&#263;"
  ]
  node [
    id 81
    label "udowodni&#263;"
  ]
  node [
    id 82
    label "poinformowa&#263;"
  ]
  node [
    id 83
    label "point"
  ]
  node [
    id 84
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 85
    label "punkt_widzenia"
  ]
  node [
    id 86
    label "do&#322;ek"
  ]
  node [
    id 87
    label "formality"
  ]
  node [
    id 88
    label "wz&#243;r"
  ]
  node [
    id 89
    label "kantyzm"
  ]
  node [
    id 90
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 91
    label "ornamentyka"
  ]
  node [
    id 92
    label "odmiana"
  ]
  node [
    id 93
    label "mode"
  ]
  node [
    id 94
    label "style"
  ]
  node [
    id 95
    label "formacja"
  ]
  node [
    id 96
    label "maszyna_drukarska"
  ]
  node [
    id 97
    label "poznanie"
  ]
  node [
    id 98
    label "szablon"
  ]
  node [
    id 99
    label "struktura"
  ]
  node [
    id 100
    label "spirala"
  ]
  node [
    id 101
    label "blaszka"
  ]
  node [
    id 102
    label "linearno&#347;&#263;"
  ]
  node [
    id 103
    label "stan"
  ]
  node [
    id 104
    label "temat"
  ]
  node [
    id 105
    label "cecha"
  ]
  node [
    id 106
    label "g&#322;owa"
  ]
  node [
    id 107
    label "kielich"
  ]
  node [
    id 108
    label "zdolno&#347;&#263;"
  ]
  node [
    id 109
    label "poj&#281;cie"
  ]
  node [
    id 110
    label "kszta&#322;t"
  ]
  node [
    id 111
    label "pasmo"
  ]
  node [
    id 112
    label "rdze&#324;"
  ]
  node [
    id 113
    label "leksem"
  ]
  node [
    id 114
    label "dyspozycja"
  ]
  node [
    id 115
    label "wygl&#261;d"
  ]
  node [
    id 116
    label "October"
  ]
  node [
    id 117
    label "zawarto&#347;&#263;"
  ]
  node [
    id 118
    label "creation"
  ]
  node [
    id 119
    label "gwiazda"
  ]
  node [
    id 120
    label "p&#281;tla"
  ]
  node [
    id 121
    label "p&#322;at"
  ]
  node [
    id 122
    label "arystotelizm"
  ]
  node [
    id 123
    label "obiekt"
  ]
  node [
    id 124
    label "dzie&#322;o"
  ]
  node [
    id 125
    label "naczynie"
  ]
  node [
    id 126
    label "wyra&#380;enie"
  ]
  node [
    id 127
    label "jednostka_systematyczna"
  ]
  node [
    id 128
    label "miniatura"
  ]
  node [
    id 129
    label "zwyczaj"
  ]
  node [
    id 130
    label "morfem"
  ]
  node [
    id 131
    label "posta&#263;"
  ]
  node [
    id 132
    label "sprzeciw"
  ]
  node [
    id 133
    label "cz&#322;owiek"
  ]
  node [
    id 134
    label "warcaby"
  ]
  node [
    id 135
    label "szlachcianka"
  ]
  node [
    id 136
    label "kobieta"
  ]
  node [
    id 137
    label "promocja"
  ]
  node [
    id 138
    label "strzelec"
  ]
  node [
    id 139
    label "figura_karciana"
  ]
  node [
    id 140
    label "pion"
  ]
  node [
    id 141
    label "okre&#347;lony"
  ]
  node [
    id 142
    label "jaki&#347;"
  ]
  node [
    id 143
    label "capture"
  ]
  node [
    id 144
    label "chwyci&#263;"
  ]
  node [
    id 145
    label "zaskoczy&#263;"
  ]
  node [
    id 146
    label "uniemo&#380;liwi&#263;"
  ]
  node [
    id 147
    label "dupn&#261;&#263;"
  ]
  node [
    id 148
    label "attack"
  ]
  node [
    id 149
    label "fascinate"
  ]
  node [
    id 150
    label "dorwa&#263;"
  ]
  node [
    id 151
    label "uzyska&#263;"
  ]
  node [
    id 152
    label "ogarn&#261;&#263;"
  ]
  node [
    id 153
    label "ensnare"
  ]
  node [
    id 154
    label "zarazi&#263;_si&#281;"
  ]
  node [
    id 155
    label "konus"
  ]
  node [
    id 156
    label "McDonald"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 26
  ]
  edge [
    source 11
    target 75
  ]
  edge [
    source 11
    target 76
  ]
  edge [
    source 11
    target 77
  ]
  edge [
    source 11
    target 78
  ]
  edge [
    source 11
    target 79
  ]
  edge [
    source 11
    target 80
  ]
  edge [
    source 11
    target 81
  ]
  edge [
    source 11
    target 82
  ]
  edge [
    source 11
    target 68
  ]
  edge [
    source 11
    target 83
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 17
  ]
  edge [
    source 12
    target 18
  ]
  edge [
    source 12
    target 84
  ]
  edge [
    source 12
    target 85
  ]
  edge [
    source 12
    target 86
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 12
    target 89
  ]
  edge [
    source 12
    target 90
  ]
  edge [
    source 12
    target 91
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 12
    target 94
  ]
  edge [
    source 12
    target 95
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 12
    target 107
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 12
    target 109
  ]
  edge [
    source 12
    target 110
  ]
  edge [
    source 12
    target 111
  ]
  edge [
    source 12
    target 112
  ]
  edge [
    source 12
    target 113
  ]
  edge [
    source 12
    target 114
  ]
  edge [
    source 12
    target 115
  ]
  edge [
    source 12
    target 116
  ]
  edge [
    source 12
    target 117
  ]
  edge [
    source 12
    target 118
  ]
  edge [
    source 12
    target 119
  ]
  edge [
    source 12
    target 120
  ]
  edge [
    source 12
    target 121
  ]
  edge [
    source 12
    target 122
  ]
  edge [
    source 12
    target 123
  ]
  edge [
    source 12
    target 124
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 12
    target 131
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 132
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 133
  ]
  edge [
    source 16
    target 134
  ]
  edge [
    source 16
    target 135
  ]
  edge [
    source 16
    target 136
  ]
  edge [
    source 16
    target 137
  ]
  edge [
    source 16
    target 138
  ]
  edge [
    source 16
    target 139
  ]
  edge [
    source 16
    target 140
  ]
  edge [
    source 17
    target 141
  ]
  edge [
    source 17
    target 142
  ]
  edge [
    source 18
    target 143
  ]
  edge [
    source 18
    target 144
  ]
  edge [
    source 18
    target 145
  ]
  edge [
    source 18
    target 146
  ]
  edge [
    source 18
    target 147
  ]
  edge [
    source 18
    target 148
  ]
  edge [
    source 18
    target 149
  ]
  edge [
    source 18
    target 150
  ]
  edge [
    source 18
    target 151
  ]
  edge [
    source 18
    target 152
  ]
  edge [
    source 18
    target 153
  ]
  edge [
    source 18
    target 154
  ]
  edge [
    source 155
    target 156
  ]
]
