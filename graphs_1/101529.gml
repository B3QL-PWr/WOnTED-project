graph [
  maxDegree 13
  minDegree 1
  meanDegree 2.0689655172413794
  density 0.07389162561576355
  graphCliqueNumber 4
  node [
    id 0
    label "pose&#322;"
    origin "text"
  ]
  node [
    id 1
    label "jan"
    origin "text"
  ]
  node [
    id 2
    label "filip"
    origin "text"
  ]
  node [
    id 3
    label "libicki"
    origin "text"
  ]
  node [
    id 4
    label "dyplomata"
  ]
  node [
    id 5
    label "wys&#322;annik"
  ]
  node [
    id 6
    label "przedstawiciel"
  ]
  node [
    id 7
    label "kurier_dyplomatyczny"
  ]
  node [
    id 8
    label "ablegat"
  ]
  node [
    id 9
    label "klubista"
  ]
  node [
    id 10
    label "Miko&#322;ajczyk"
  ]
  node [
    id 11
    label "Korwin"
  ]
  node [
    id 12
    label "parlamentarzysta"
  ]
  node [
    id 13
    label "dyscyplina_partyjna"
  ]
  node [
    id 14
    label "izba_ni&#380;sza"
  ]
  node [
    id 15
    label "poselstwo"
  ]
  node [
    id 16
    label "Jan"
  ]
  node [
    id 17
    label "Libicki"
  ]
  node [
    id 18
    label "Benedykt"
  ]
  node [
    id 19
    label "XVI"
  ]
  node [
    id 20
    label "benedykta"
  ]
  node [
    id 21
    label "Vittorio"
  ]
  node [
    id 22
    label "Messorim"
  ]
  node [
    id 23
    label "kongregacja"
  ]
  node [
    id 24
    label "nauka"
  ]
  node [
    id 25
    label "wiara"
  ]
  node [
    id 26
    label "raport"
  ]
  node [
    id 27
    label "ojciec"
  ]
  node [
    id 28
    label "sta&#324;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 25
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 27
  ]
  edge [
    source 25
    target 28
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 28
  ]
  edge [
    source 27
    target 28
  ]
]
