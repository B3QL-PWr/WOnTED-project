graph [
  maxDegree 23
  minDegree 1
  meanDegree 1.981818181818182
  density 0.01818181818181818
  graphCliqueNumber 2
  node [
    id 0
    label "dlatego"
    origin "text"
  ]
  node [
    id 1
    label "uwa&#380;a&#263;"
    origin "text"
  ]
  node [
    id 2
    label "polak"
    origin "text"
  ]
  node [
    id 3
    label "biedaka"
    origin "text"
  ]
  node [
    id 4
    label "wzgl&#261;d"
    origin "text"
  ]
  node [
    id 5
    label "mentalno&#347;&#263;"
    origin "text"
  ]
  node [
    id 6
    label "s&#322;aby"
    origin "text"
  ]
  node [
    id 7
    label "wykszta&#322;cenie"
    origin "text"
  ]
  node [
    id 8
    label "brak"
    origin "text"
  ]
  node [
    id 9
    label "podstawa"
    origin "text"
  ]
  node [
    id 10
    label "ekonomia"
    origin "text"
  ]
  node [
    id 11
    label "continue"
  ]
  node [
    id 12
    label "str&#243;&#380;y&#263;"
  ]
  node [
    id 13
    label "consider"
  ]
  node [
    id 14
    label "my&#347;le&#263;"
  ]
  node [
    id 15
    label "pilnowa&#263;"
  ]
  node [
    id 16
    label "robi&#263;"
  ]
  node [
    id 17
    label "uznawa&#263;"
  ]
  node [
    id 18
    label "obserwowa&#263;"
  ]
  node [
    id 19
    label "skupia&#263;_si&#281;"
  ]
  node [
    id 20
    label "mie&#263;_na_baczenie"
  ]
  node [
    id 21
    label "deliver"
  ]
  node [
    id 22
    label "polski"
  ]
  node [
    id 23
    label "przyczyna"
  ]
  node [
    id 24
    label "uwaga"
  ]
  node [
    id 25
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 26
    label "punkt_widzenia"
  ]
  node [
    id 27
    label "kulturowo&#347;&#263;"
  ]
  node [
    id 28
    label "osobowo&#347;&#263;"
  ]
  node [
    id 29
    label "nieznaczny"
  ]
  node [
    id 30
    label "nieumiej&#281;tny"
  ]
  node [
    id 31
    label "marnie"
  ]
  node [
    id 32
    label "md&#322;y"
  ]
  node [
    id 33
    label "przemijaj&#261;cy"
  ]
  node [
    id 34
    label "zawodny"
  ]
  node [
    id 35
    label "delikatny"
  ]
  node [
    id 36
    label "&#322;agodny"
  ]
  node [
    id 37
    label "niedoskona&#322;y"
  ]
  node [
    id 38
    label "nietrwa&#322;y"
  ]
  node [
    id 39
    label "po&#347;ledni"
  ]
  node [
    id 40
    label "s&#322;abowity"
  ]
  node [
    id 41
    label "niefajny"
  ]
  node [
    id 42
    label "z&#322;y"
  ]
  node [
    id 43
    label "niemocny"
  ]
  node [
    id 44
    label "kiepsko"
  ]
  node [
    id 45
    label "niezdrowy"
  ]
  node [
    id 46
    label "lura"
  ]
  node [
    id 47
    label "s&#322;abo"
  ]
  node [
    id 48
    label "nieudany"
  ]
  node [
    id 49
    label "mizerny"
  ]
  node [
    id 50
    label "kwalifikacje"
  ]
  node [
    id 51
    label "niepokalanki"
  ]
  node [
    id 52
    label "sophistication"
  ]
  node [
    id 53
    label "form"
  ]
  node [
    id 54
    label "training"
  ]
  node [
    id 55
    label "pomo&#380;enie"
  ]
  node [
    id 56
    label "wiedza"
  ]
  node [
    id 57
    label "zapoznanie"
  ]
  node [
    id 58
    label "rozwini&#281;cie"
  ]
  node [
    id 59
    label "wys&#322;anie"
  ]
  node [
    id 60
    label "o&#347;wiecenie"
  ]
  node [
    id 61
    label "skolaryzacja"
  ]
  node [
    id 62
    label "udoskonalenie"
  ]
  node [
    id 63
    label "urszulanki"
  ]
  node [
    id 64
    label "prywatywny"
  ]
  node [
    id 65
    label "defect"
  ]
  node [
    id 66
    label "odej&#347;cie"
  ]
  node [
    id 67
    label "gap"
  ]
  node [
    id 68
    label "kr&#243;tki"
  ]
  node [
    id 69
    label "wyr&#243;b"
  ]
  node [
    id 70
    label "nieistnienie"
  ]
  node [
    id 71
    label "wada"
  ]
  node [
    id 72
    label "odej&#347;&#263;"
  ]
  node [
    id 73
    label "odchodzenie"
  ]
  node [
    id 74
    label "odchodzi&#263;"
  ]
  node [
    id 75
    label "podstawowy"
  ]
  node [
    id 76
    label "strategia"
  ]
  node [
    id 77
    label "pot&#281;ga"
  ]
  node [
    id 78
    label "zasadzenie"
  ]
  node [
    id 79
    label "przedmiot"
  ]
  node [
    id 80
    label "za&#322;o&#380;enie"
  ]
  node [
    id 81
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 82
    label "&#347;ciana"
  ]
  node [
    id 83
    label "documentation"
  ]
  node [
    id 84
    label "dzieci&#281;ctwo"
  ]
  node [
    id 85
    label "pomys&#322;"
  ]
  node [
    id 86
    label "bok"
  ]
  node [
    id 87
    label "d&#243;&#322;"
  ]
  node [
    id 88
    label "punkt_odniesienia"
  ]
  node [
    id 89
    label "column"
  ]
  node [
    id 90
    label "zasadzi&#263;"
  ]
  node [
    id 91
    label "background"
  ]
  node [
    id 92
    label "portfel_zam&#243;wie&#324;"
  ]
  node [
    id 93
    label "dysponowa&#263;"
  ]
  node [
    id 94
    label "nauka_ekonomiczna"
  ]
  node [
    id 95
    label "sprawno&#347;&#263;"
  ]
  node [
    id 96
    label "makroekonomia"
  ]
  node [
    id 97
    label "ekonomika"
  ]
  node [
    id 98
    label "ekonometria"
  ]
  node [
    id 99
    label "kierunek"
  ]
  node [
    id 100
    label "nominalizm"
  ]
  node [
    id 101
    label "farmakoekonomika"
  ]
  node [
    id 102
    label "katalaksja"
  ]
  node [
    id 103
    label "neuroekonimia"
  ]
  node [
    id 104
    label "ekonomia_gospodarstw_domowych"
  ]
  node [
    id 105
    label "bankowo&#347;&#263;"
  ]
  node [
    id 106
    label "ekonomia_instytucjonalna"
  ]
  node [
    id 107
    label "book-building"
  ]
  node [
    id 108
    label "mikroekonomia"
  ]
  node [
    id 109
    label "wydzia&#322;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 10
    target 105
  ]
  edge [
    source 10
    target 106
  ]
  edge [
    source 10
    target 107
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 109
  ]
]
