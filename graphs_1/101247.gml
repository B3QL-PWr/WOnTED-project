graph [
  maxDegree 24
  minDegree 1
  meanDegree 1.7746478873239437
  density 0.012586155229247827
  graphCliqueNumber 6
  node [
    id 0
    label "muzyka"
    origin "text"
  ]
  node [
    id 1
    label "wys&#322;ucha&#263;"
  ]
  node [
    id 2
    label "przedmiot"
  ]
  node [
    id 3
    label "kontrapunkt"
  ]
  node [
    id 4
    label "set"
  ]
  node [
    id 5
    label "sztuka"
  ]
  node [
    id 6
    label "muza"
  ]
  node [
    id 7
    label "wykonywa&#263;"
  ]
  node [
    id 8
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 9
    label "wytw&#243;r"
  ]
  node [
    id 10
    label "notacja_muzyczna"
  ]
  node [
    id 11
    label "britpop"
  ]
  node [
    id 12
    label "instrumentalistyka"
  ]
  node [
    id 13
    label "zjawisko"
  ]
  node [
    id 14
    label "szko&#322;a"
  ]
  node [
    id 15
    label "komponowanie"
  ]
  node [
    id 16
    label "wys&#322;uchanie"
  ]
  node [
    id 17
    label "beatbox"
  ]
  node [
    id 18
    label "wokalistyka"
  ]
  node [
    id 19
    label "nauka"
  ]
  node [
    id 20
    label "pasa&#380;"
  ]
  node [
    id 21
    label "wykonywanie"
  ]
  node [
    id 22
    label "harmonia"
  ]
  node [
    id 23
    label "komponowa&#263;"
  ]
  node [
    id 24
    label "kapela"
  ]
  node [
    id 25
    label "Paula"
  ]
  node [
    id 26
    label "McCartney"
  ]
  node [
    id 27
    label "Linda"
  ]
  node [
    id 28
    label "Eastman"
  ]
  node [
    id 29
    label "George"
  ]
  node [
    id 30
    label "Harrison"
  ]
  node [
    id 31
    label "John"
  ]
  node [
    id 32
    label "Lennon"
  ]
  node [
    id 33
    label "Yoko"
  ]
  node [
    id 34
    label "on"
  ]
  node [
    id 35
    label "Bob"
  ]
  node [
    id 36
    label "Dylan"
  ]
  node [
    id 37
    label "Nashville"
  ]
  node [
    id 38
    label "Skyline"
  ]
  node [
    id 39
    label "Winston"
  ]
  node [
    id 40
    label "nowy"
  ]
  node [
    id 41
    label "Jork"
  ]
  node [
    id 42
    label "Woodstock"
  ]
  node [
    id 43
    label "Music"
  ]
  node [
    id 44
    label "Anda"
  ]
  node [
    id 45
    label "Art"
  ]
  node [
    id 46
    label "Festival"
  ]
  node [
    id 47
    label "Nash"
  ]
  node [
    id 48
    label "Young"
  ]
  node [
    id 49
    label "ten"
  ]
  node [
    id 50
    label "Years"
  ]
  node [
    id 51
    label "After"
  ]
  node [
    id 52
    label "Sly"
  ]
  node [
    id 53
    label "the"
  ]
  node [
    id 54
    label "Family"
  ]
  node [
    id 55
    label "Stone"
  ]
  node [
    id 56
    label "country"
  ]
  node [
    id 57
    label "Joe"
  ]
  node [
    id 58
    label "Fish"
  ]
  node [
    id 59
    label "Jefferson"
  ]
  node [
    id 60
    label "Airplane"
  ]
  node [
    id 61
    label "Jimi"
  ]
  node [
    id 62
    label "Hendrix"
  ]
  node [
    id 63
    label "Janis"
  ]
  node [
    id 64
    label "Joplin"
  ]
  node [
    id 65
    label "Joan"
  ]
  node [
    id 66
    label "Baez"
  ]
  node [
    id 67
    label "The"
  ]
  node [
    id 68
    label "WHO"
  ]
  node [
    id 69
    label "Plastic"
  ]
  node [
    id 70
    label "People"
  ]
  node [
    id 71
    label "of"
  ]
  node [
    id 72
    label "Universe"
  ]
  node [
    id 73
    label "David"
  ]
  node [
    id 74
    label "Bowiego"
  ]
  node [
    id 75
    label "Space"
  ]
  node [
    id 76
    label "Oddity"
  ]
  node [
    id 77
    label "James"
  ]
  node [
    id 78
    label "Carter"
  ]
  node [
    id 79
    label "Marilyn"
  ]
  node [
    id 80
    label "Manson"
  ]
  node [
    id 81
    label "Tomasz"
  ]
  node [
    id 82
    label "Hernik"
  ]
  node [
    id 83
    label "Joshua"
  ]
  node [
    id 84
    label "Redman"
  ]
  node [
    id 85
    label "Miros&#322;awa"
  ]
  node [
    id 86
    label "Kami&#324;ski"
  ]
  node [
    id 87
    label "Bonarowski"
  ]
  node [
    id 88
    label "Timo"
  ]
  node [
    id 89
    label "Kotipelto"
  ]
  node [
    id 90
    label "Prodigy"
  ]
  node [
    id 91
    label "Keith"
  ]
  node [
    id 92
    label "flinta"
  ]
  node [
    id 93
    label "Jacek"
  ]
  node [
    id 94
    label "Otr&#281;ba"
  ]
  node [
    id 95
    label "dulka"
  ]
  node [
    id 96
    label "Pontes"
  ]
  node [
    id 97
    label "wojtek"
  ]
  node [
    id 98
    label "Pilichowski"
  ]
  node [
    id 99
    label "Maciej"
  ]
  node [
    id 100
    label "g&#322;adysz"
  ]
  node [
    id 101
    label "Mindi"
  ]
  node [
    id 102
    label "Abair"
  ]
  node [
    id 103
    label "Adam"
  ]
  node [
    id 104
    label "Burzy&#324;ski"
  ]
  node [
    id 105
    label "Krzysztofa"
  ]
  node [
    id 106
    label "Respondek"
  ]
  node [
    id 107
    label "Jennifer"
  ]
  node [
    id 108
    label "Lopez"
  ]
  node [
    id 109
    label "Max"
  ]
  node [
    id 110
    label "Cavalera"
  ]
  node [
    id 111
    label "lipa"
  ]
  node [
    id 112
    label "Lipnicki"
  ]
  node [
    id 113
    label "Zygmunt"
  ]
  node [
    id 114
    label "Kukla"
  ]
  node [
    id 115
    label "Mikis"
  ]
  node [
    id 116
    label "Cupas"
  ]
  node [
    id 117
    label "Patrick"
  ]
  node [
    id 118
    label "Fiori"
  ]
  node [
    id 119
    label "no"
  ]
  node [
    id 120
    label "Doubt"
  ]
  node [
    id 121
    label "Gwen"
  ]
  node [
    id 122
    label "Stefani"
  ]
  node [
    id 123
    label "Agressiva"
  ]
  node [
    id 124
    label "69"
  ]
  node [
    id 125
    label "Bogus&#322;awa"
  ]
  node [
    id 126
    label "Pezda"
  ]
  node [
    id 127
    label "marka"
  ]
  node [
    id 128
    label "Napi&#243;rkowski"
  ]
  node [
    id 129
    label "Gra&#380;yna"
  ]
  node [
    id 130
    label "Bacewicz"
  ]
  node [
    id 131
    label "Komeda"
  ]
  node [
    id 132
    label "Coleman"
  ]
  node [
    id 133
    label "Hawkins"
  ]
  node [
    id 134
    label "Juda"
  ]
  node [
    id 135
    label "Garland"
  ]
  node [
    id 136
    label "Rolling"
  ]
  node [
    id 137
    label "Stones"
  ]
  node [
    id 138
    label "Brian"
  ]
  node [
    id 139
    label "Jones"
  ]
  node [
    id 140
    label "Joseph"
  ]
  node [
    id 141
    label "kosma"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 34
  ]
  edge [
    source 31
    target 39
  ]
  edge [
    source 32
    target 34
  ]
  edge [
    source 32
    target 39
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 44
  ]
  edge [
    source 42
    target 45
  ]
  edge [
    source 42
    target 46
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 45
  ]
  edge [
    source 43
    target 46
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 46
  ]
  edge [
    source 44
    target 56
  ]
  edge [
    source 44
    target 57
  ]
  edge [
    source 44
    target 53
  ]
  edge [
    source 44
    target 58
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 51
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 54
  ]
  edge [
    source 52
    target 55
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 55
  ]
  edge [
    source 53
    target 56
  ]
  edge [
    source 53
    target 57
  ]
  edge [
    source 53
    target 58
  ]
  edge [
    source 53
    target 67
  ]
  edge [
    source 53
    target 69
  ]
  edge [
    source 53
    target 70
  ]
  edge [
    source 53
    target 71
  ]
  edge [
    source 53
    target 72
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 58
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 67
    target 69
  ]
  edge [
    source 67
    target 70
  ]
  edge [
    source 67
    target 71
  ]
  edge [
    source 67
    target 72
  ]
  edge [
    source 67
    target 90
  ]
  edge [
    source 67
    target 136
  ]
  edge [
    source 67
    target 137
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 69
    target 71
  ]
  edge [
    source 69
    target 72
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 72
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 75
    target 76
  ]
  edge [
    source 77
    target 78
  ]
  edge [
    source 79
    target 80
  ]
  edge [
    source 81
    target 82
  ]
  edge [
    source 81
    target 87
  ]
  edge [
    source 81
    target 111
  ]
  edge [
    source 81
    target 112
  ]
  edge [
    source 83
    target 84
  ]
  edge [
    source 85
    target 86
  ]
  edge [
    source 88
    target 89
  ]
  edge [
    source 91
    target 92
  ]
  edge [
    source 93
    target 94
  ]
  edge [
    source 95
    target 96
  ]
  edge [
    source 97
    target 98
  ]
  edge [
    source 99
    target 100
  ]
  edge [
    source 101
    target 102
  ]
  edge [
    source 103
    target 104
  ]
  edge [
    source 105
    target 106
  ]
  edge [
    source 105
    target 131
  ]
  edge [
    source 107
    target 108
  ]
  edge [
    source 109
    target 110
  ]
  edge [
    source 111
    target 112
  ]
  edge [
    source 113
    target 114
  ]
  edge [
    source 115
    target 116
  ]
  edge [
    source 117
    target 118
  ]
  edge [
    source 119
    target 120
  ]
  edge [
    source 121
    target 122
  ]
  edge [
    source 123
    target 124
  ]
  edge [
    source 125
    target 126
  ]
  edge [
    source 127
    target 128
  ]
  edge [
    source 129
    target 130
  ]
  edge [
    source 132
    target 133
  ]
  edge [
    source 134
    target 135
  ]
  edge [
    source 136
    target 137
  ]
  edge [
    source 138
    target 139
  ]
  edge [
    source 140
    target 141
  ]
]
