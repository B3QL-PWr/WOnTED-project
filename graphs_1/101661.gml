graph [
  maxDegree 71
  minDegree 1
  meanDegree 2.2057142857142855
  density 0.0042093784078516905
  graphCliqueNumber 5
  node [
    id 0
    label "pan"
    origin "text"
  ]
  node [
    id 1
    label "marsza&#322;ek"
    origin "text"
  ]
  node [
    id 2
    label "wysoki"
    origin "text"
  ]
  node [
    id 3
    label "izba"
    origin "text"
  ]
  node [
    id 4
    label "poselski"
    origin "text"
  ]
  node [
    id 5
    label "projekt"
    origin "text"
  ]
  node [
    id 6
    label "ustawa"
    origin "text"
  ]
  node [
    id 7
    label "zmiana"
    origin "text"
  ]
  node [
    id 8
    label "kodeks"
    origin "text"
  ]
  node [
    id 9
    label "praca"
    origin "text"
  ]
  node [
    id 10
    label "niekt&#243;ry"
    origin "text"
  ]
  node [
    id 11
    label "inny"
    origin "text"
  ]
  node [
    id 12
    label "art"
    origin "text"
  ]
  node [
    id 13
    label "stan"
    origin "text"
  ]
  node [
    id 14
    label "pracodawca"
    origin "text"
  ]
  node [
    id 15
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 16
    label "wypowiedzie&#263;"
    origin "text"
  ]
  node [
    id 17
    label "ani"
    origin "text"
  ]
  node [
    id 18
    label "rozwi&#261;za&#263;"
    origin "text"
  ]
  node [
    id 19
    label "umowa"
    origin "text"
  ]
  node [
    id 20
    label "okres"
    origin "text"
  ]
  node [
    id 21
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 22
    label "z&#322;o&#380;enie"
    origin "text"
  ]
  node [
    id 23
    label "przez"
    origin "text"
  ]
  node [
    id 24
    label "pracownik"
    origin "text"
  ]
  node [
    id 25
    label "wniosek"
    origin "text"
  ]
  node [
    id 26
    label "obni&#380;enie"
    origin "text"
  ]
  node [
    id 27
    label "wymiar"
    origin "text"
  ]
  node [
    id 28
    label "czas"
    origin "text"
  ]
  node [
    id 29
    label "powr&#243;t"
    origin "text"
  ]
  node [
    id 30
    label "obni&#380;y&#263;"
    origin "text"
  ]
  node [
    id 31
    label "rozwi&#261;zanie"
    origin "text"
  ]
  node [
    id 32
    label "tym"
    origin "text"
  ]
  node [
    id 33
    label "by&#263;"
    origin "text"
  ]
  node [
    id 34
    label "dopuszczalny"
    origin "text"
  ]
  node [
    id 35
    label "tylko"
    origin "text"
  ]
  node [
    id 36
    label "raz"
    origin "text"
  ]
  node [
    id 37
    label "og&#322;oszenie"
    origin "text"
  ]
  node [
    id 38
    label "upad&#322;o&#347;&#263;"
    origin "text"
  ]
  node [
    id 39
    label "lub"
    origin "text"
  ]
  node [
    id 40
    label "likwidacja"
    origin "text"
  ]
  node [
    id 41
    label "tak&#380;e"
    origin "text"
  ]
  node [
    id 42
    label "gdy"
    origin "text"
  ]
  node [
    id 43
    label "zachodzi&#263;"
    origin "text"
  ]
  node [
    id 44
    label "przyczyna"
    origin "text"
  ]
  node [
    id 45
    label "uzasadnia&#263;"
    origin "text"
  ]
  node [
    id 46
    label "wina"
    origin "text"
  ]
  node [
    id 47
    label "cz&#322;owiek"
  ]
  node [
    id 48
    label "profesor"
  ]
  node [
    id 49
    label "kszta&#322;ciciel"
  ]
  node [
    id 50
    label "jegomo&#347;&#263;"
  ]
  node [
    id 51
    label "zwrot"
  ]
  node [
    id 52
    label "rz&#261;dzenie"
  ]
  node [
    id 53
    label "m&#261;&#380;"
  ]
  node [
    id 54
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 55
    label "ch&#322;opina"
  ]
  node [
    id 56
    label "bratek"
  ]
  node [
    id 57
    label "opiekun"
  ]
  node [
    id 58
    label "doros&#322;y"
  ]
  node [
    id 59
    label "preceptor"
  ]
  node [
    id 60
    label "Midas"
  ]
  node [
    id 61
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 62
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 63
    label "murza"
  ]
  node [
    id 64
    label "ojciec"
  ]
  node [
    id 65
    label "androlog"
  ]
  node [
    id 66
    label "pupil"
  ]
  node [
    id 67
    label "efendi"
  ]
  node [
    id 68
    label "nabab"
  ]
  node [
    id 69
    label "w&#322;odarz"
  ]
  node [
    id 70
    label "szkolnik"
  ]
  node [
    id 71
    label "pedagog"
  ]
  node [
    id 72
    label "popularyzator"
  ]
  node [
    id 73
    label "andropauza"
  ]
  node [
    id 74
    label "gra_w_karty"
  ]
  node [
    id 75
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 76
    label "Mieszko_I"
  ]
  node [
    id 77
    label "bogaty"
  ]
  node [
    id 78
    label "samiec"
  ]
  node [
    id 79
    label "przyw&#243;dca"
  ]
  node [
    id 80
    label "pa&#324;stwo"
  ]
  node [
    id 81
    label "belfer"
  ]
  node [
    id 82
    label "stopie&#324;_wojskowy"
  ]
  node [
    id 83
    label "dostojnik"
  ]
  node [
    id 84
    label "oficer"
  ]
  node [
    id 85
    label "parlamentarzysta"
  ]
  node [
    id 86
    label "Pi&#322;sudski"
  ]
  node [
    id 87
    label "warto&#347;ciowy"
  ]
  node [
    id 88
    label "du&#380;y"
  ]
  node [
    id 89
    label "wysoce"
  ]
  node [
    id 90
    label "daleki"
  ]
  node [
    id 91
    label "znaczny"
  ]
  node [
    id 92
    label "wysoko"
  ]
  node [
    id 93
    label "szczytnie"
  ]
  node [
    id 94
    label "wznios&#322;y"
  ]
  node [
    id 95
    label "wyrafinowany"
  ]
  node [
    id 96
    label "z_wysoka"
  ]
  node [
    id 97
    label "chwalebny"
  ]
  node [
    id 98
    label "uprzywilejowany"
  ]
  node [
    id 99
    label "niepo&#347;ledni"
  ]
  node [
    id 100
    label "pok&#243;j"
  ]
  node [
    id 101
    label "parlament"
  ]
  node [
    id 102
    label "zwi&#261;zek"
  ]
  node [
    id 103
    label "NIK"
  ]
  node [
    id 104
    label "urz&#261;d"
  ]
  node [
    id 105
    label "organ"
  ]
  node [
    id 106
    label "pomieszczenie"
  ]
  node [
    id 107
    label "dokument"
  ]
  node [
    id 108
    label "device"
  ]
  node [
    id 109
    label "program_u&#380;ytkowy"
  ]
  node [
    id 110
    label "intencja"
  ]
  node [
    id 111
    label "agreement"
  ]
  node [
    id 112
    label "pomys&#322;"
  ]
  node [
    id 113
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 114
    label "plan"
  ]
  node [
    id 115
    label "dokumentacja"
  ]
  node [
    id 116
    label "Karta_Nauczyciela"
  ]
  node [
    id 117
    label "marc&#243;wka"
  ]
  node [
    id 118
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 119
    label "akt"
  ]
  node [
    id 120
    label "przej&#347;&#263;"
  ]
  node [
    id 121
    label "charter"
  ]
  node [
    id 122
    label "przej&#347;cie"
  ]
  node [
    id 123
    label "anatomopatolog"
  ]
  node [
    id 124
    label "rewizja"
  ]
  node [
    id 125
    label "oznaka"
  ]
  node [
    id 126
    label "ferment"
  ]
  node [
    id 127
    label "komplet"
  ]
  node [
    id 128
    label "tura"
  ]
  node [
    id 129
    label "amendment"
  ]
  node [
    id 130
    label "zmianka"
  ]
  node [
    id 131
    label "odmienianie"
  ]
  node [
    id 132
    label "passage"
  ]
  node [
    id 133
    label "zjawisko"
  ]
  node [
    id 134
    label "change"
  ]
  node [
    id 135
    label "r&#281;kopis"
  ]
  node [
    id 136
    label "Justynian"
  ]
  node [
    id 137
    label "kodeks_morski"
  ]
  node [
    id 138
    label "code"
  ]
  node [
    id 139
    label "przepis"
  ]
  node [
    id 140
    label "obwiniony"
  ]
  node [
    id 141
    label "kodeks_karny"
  ]
  node [
    id 142
    label "zbi&#243;r"
  ]
  node [
    id 143
    label "kodeks_drogowy"
  ]
  node [
    id 144
    label "zasada"
  ]
  node [
    id 145
    label "kodeks_pracy"
  ]
  node [
    id 146
    label "kodeks_cywilny"
  ]
  node [
    id 147
    label "kodeks_wykrocze&#324;"
  ]
  node [
    id 148
    label "kodeks_dyplomatyczny"
  ]
  node [
    id 149
    label "kodeks_rodzinny"
  ]
  node [
    id 150
    label "stosunek_pracy"
  ]
  node [
    id 151
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 152
    label "benedykty&#324;ski"
  ]
  node [
    id 153
    label "pracowanie"
  ]
  node [
    id 154
    label "zaw&#243;d"
  ]
  node [
    id 155
    label "kierownictwo"
  ]
  node [
    id 156
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 157
    label "wytw&#243;r"
  ]
  node [
    id 158
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 159
    label "tynkarski"
  ]
  node [
    id 160
    label "czynnik_produkcji"
  ]
  node [
    id 161
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 162
    label "zobowi&#261;zanie"
  ]
  node [
    id 163
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 164
    label "czynno&#347;&#263;"
  ]
  node [
    id 165
    label "tyrka"
  ]
  node [
    id 166
    label "pracowa&#263;"
  ]
  node [
    id 167
    label "siedziba"
  ]
  node [
    id 168
    label "poda&#380;_pracy"
  ]
  node [
    id 169
    label "miejsce"
  ]
  node [
    id 170
    label "zak&#322;ad"
  ]
  node [
    id 171
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 172
    label "najem"
  ]
  node [
    id 173
    label "jaki&#347;"
  ]
  node [
    id 174
    label "kolejny"
  ]
  node [
    id 175
    label "inaczej"
  ]
  node [
    id 176
    label "r&#243;&#380;ny"
  ]
  node [
    id 177
    label "inszy"
  ]
  node [
    id 178
    label "osobno"
  ]
  node [
    id 179
    label "Arizona"
  ]
  node [
    id 180
    label "Georgia"
  ]
  node [
    id 181
    label "warstwa"
  ]
  node [
    id 182
    label "jednostka_administracyjna"
  ]
  node [
    id 183
    label "Hawaje"
  ]
  node [
    id 184
    label "Goa"
  ]
  node [
    id 185
    label "Floryda"
  ]
  node [
    id 186
    label "Oklahoma"
  ]
  node [
    id 187
    label "punkt"
  ]
  node [
    id 188
    label "Alaska"
  ]
  node [
    id 189
    label "wci&#281;cie"
  ]
  node [
    id 190
    label "Alabama"
  ]
  node [
    id 191
    label "Oregon"
  ]
  node [
    id 192
    label "poziom"
  ]
  node [
    id 193
    label "Teksas"
  ]
  node [
    id 194
    label "Illinois"
  ]
  node [
    id 195
    label "Waszyngton"
  ]
  node [
    id 196
    label "Jukatan"
  ]
  node [
    id 197
    label "shape"
  ]
  node [
    id 198
    label "Nowy_Meksyk"
  ]
  node [
    id 199
    label "ilo&#347;&#263;"
  ]
  node [
    id 200
    label "state"
  ]
  node [
    id 201
    label "Nowy_York"
  ]
  node [
    id 202
    label "Arakan"
  ]
  node [
    id 203
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 204
    label "Kalifornia"
  ]
  node [
    id 205
    label "wektor"
  ]
  node [
    id 206
    label "Massachusetts"
  ]
  node [
    id 207
    label "Pensylwania"
  ]
  node [
    id 208
    label "Michigan"
  ]
  node [
    id 209
    label "Maryland"
  ]
  node [
    id 210
    label "Ohio"
  ]
  node [
    id 211
    label "Kansas"
  ]
  node [
    id 212
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 213
    label "Luizjana"
  ]
  node [
    id 214
    label "samopoczucie"
  ]
  node [
    id 215
    label "Wirginia"
  ]
  node [
    id 216
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 217
    label "p&#322;atnik"
  ]
  node [
    id 218
    label "zwierzchnik"
  ]
  node [
    id 219
    label "ponadzak&#322;adowy_uk&#322;ad_zbiorowy_pracy"
  ]
  node [
    id 220
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 221
    label "express"
  ]
  node [
    id 222
    label "order"
  ]
  node [
    id 223
    label "zwerbalizowa&#263;"
  ]
  node [
    id 224
    label "wydoby&#263;"
  ]
  node [
    id 225
    label "denounce"
  ]
  node [
    id 226
    label "unloose"
  ]
  node [
    id 227
    label "urzeczywistni&#263;"
  ]
  node [
    id 228
    label "usun&#261;&#263;"
  ]
  node [
    id 229
    label "wymy&#347;li&#263;"
  ]
  node [
    id 230
    label "bring"
  ]
  node [
    id 231
    label "przesta&#263;"
  ]
  node [
    id 232
    label "undo"
  ]
  node [
    id 233
    label "uniewa&#380;ni&#263;"
  ]
  node [
    id 234
    label "czyn"
  ]
  node [
    id 235
    label "contract"
  ]
  node [
    id 236
    label "gestia_transportowa"
  ]
  node [
    id 237
    label "zawrze&#263;"
  ]
  node [
    id 238
    label "klauzula"
  ]
  node [
    id 239
    label "porozumienie"
  ]
  node [
    id 240
    label "warunek"
  ]
  node [
    id 241
    label "zawarcie"
  ]
  node [
    id 242
    label "paleogen"
  ]
  node [
    id 243
    label "spell"
  ]
  node [
    id 244
    label "period"
  ]
  node [
    id 245
    label "prekambr"
  ]
  node [
    id 246
    label "jura"
  ]
  node [
    id 247
    label "interstadia&#322;"
  ]
  node [
    id 248
    label "jednostka_geologiczna"
  ]
  node [
    id 249
    label "izochronizm"
  ]
  node [
    id 250
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 251
    label "okres_noachijski"
  ]
  node [
    id 252
    label "orosir"
  ]
  node [
    id 253
    label "sten"
  ]
  node [
    id 254
    label "kreda"
  ]
  node [
    id 255
    label "drugorz&#281;d"
  ]
  node [
    id 256
    label "semester"
  ]
  node [
    id 257
    label "trzeciorz&#281;d"
  ]
  node [
    id 258
    label "ton"
  ]
  node [
    id 259
    label "dzieje"
  ]
  node [
    id 260
    label "poprzednik"
  ]
  node [
    id 261
    label "kalim"
  ]
  node [
    id 262
    label "ordowik"
  ]
  node [
    id 263
    label "karbon"
  ]
  node [
    id 264
    label "trias"
  ]
  node [
    id 265
    label "stater"
  ]
  node [
    id 266
    label "era"
  ]
  node [
    id 267
    label "cykl"
  ]
  node [
    id 268
    label "p&#243;&#322;okres"
  ]
  node [
    id 269
    label "czwartorz&#281;d"
  ]
  node [
    id 270
    label "pulsacja"
  ]
  node [
    id 271
    label "okres_amazo&#324;ski"
  ]
  node [
    id 272
    label "kambr"
  ]
  node [
    id 273
    label "Zeitgeist"
  ]
  node [
    id 274
    label "nast&#281;pnik"
  ]
  node [
    id 275
    label "kriogen"
  ]
  node [
    id 276
    label "glacja&#322;"
  ]
  node [
    id 277
    label "fala"
  ]
  node [
    id 278
    label "okres_czasu"
  ]
  node [
    id 279
    label "riak"
  ]
  node [
    id 280
    label "schy&#322;ek"
  ]
  node [
    id 281
    label "okres_hesperyjski"
  ]
  node [
    id 282
    label "sylur"
  ]
  node [
    id 283
    label "dewon"
  ]
  node [
    id 284
    label "ciota"
  ]
  node [
    id 285
    label "epoka"
  ]
  node [
    id 286
    label "pierwszorz&#281;d"
  ]
  node [
    id 287
    label "okres_halsztacki"
  ]
  node [
    id 288
    label "ektas"
  ]
  node [
    id 289
    label "zdanie"
  ]
  node [
    id 290
    label "condition"
  ]
  node [
    id 291
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 292
    label "rok_akademicki"
  ]
  node [
    id 293
    label "wapie&#324;_muszlowy"
  ]
  node [
    id 294
    label "postglacja&#322;"
  ]
  node [
    id 295
    label "faza"
  ]
  node [
    id 296
    label "proces_fizjologiczny"
  ]
  node [
    id 297
    label "ediakar"
  ]
  node [
    id 298
    label "time_period"
  ]
  node [
    id 299
    label "ruch_drgaj&#261;cy"
  ]
  node [
    id 300
    label "perm"
  ]
  node [
    id 301
    label "rok_szkolny"
  ]
  node [
    id 302
    label "neogen"
  ]
  node [
    id 303
    label "sider"
  ]
  node [
    id 304
    label "flow"
  ]
  node [
    id 305
    label "podokres"
  ]
  node [
    id 306
    label "preglacja&#322;"
  ]
  node [
    id 307
    label "retoryka"
  ]
  node [
    id 308
    label "choroba_przyrodzona"
  ]
  node [
    id 309
    label "s&#322;o&#324;ce"
  ]
  node [
    id 310
    label "czynienie_si&#281;"
  ]
  node [
    id 311
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 312
    label "long_time"
  ]
  node [
    id 313
    label "przedpo&#322;udnie"
  ]
  node [
    id 314
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 315
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 316
    label "tydzie&#324;"
  ]
  node [
    id 317
    label "godzina"
  ]
  node [
    id 318
    label "t&#322;usty_czwartek"
  ]
  node [
    id 319
    label "wsta&#263;"
  ]
  node [
    id 320
    label "day"
  ]
  node [
    id 321
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 322
    label "przedwiecz&#243;r"
  ]
  node [
    id 323
    label "Sylwester"
  ]
  node [
    id 324
    label "po&#322;udnie"
  ]
  node [
    id 325
    label "wzej&#347;cie"
  ]
  node [
    id 326
    label "podwiecz&#243;r"
  ]
  node [
    id 327
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 328
    label "rano"
  ]
  node [
    id 329
    label "termin"
  ]
  node [
    id 330
    label "ranek"
  ]
  node [
    id 331
    label "doba"
  ]
  node [
    id 332
    label "wiecz&#243;r"
  ]
  node [
    id 333
    label "walentynki"
  ]
  node [
    id 334
    label "popo&#322;udnie"
  ]
  node [
    id 335
    label "noc"
  ]
  node [
    id 336
    label "wstanie"
  ]
  node [
    id 337
    label "opracowanie"
  ]
  node [
    id 338
    label "zgi&#281;cie"
  ]
  node [
    id 339
    label "stage_set"
  ]
  node [
    id 340
    label "posk&#322;adanie"
  ]
  node [
    id 341
    label "zestawienie"
  ]
  node [
    id 342
    label "danie"
  ]
  node [
    id 343
    label "lodging"
  ]
  node [
    id 344
    label "zgromadzenie"
  ]
  node [
    id 345
    label "powiedzenie"
  ]
  node [
    id 346
    label "blend"
  ]
  node [
    id 347
    label "przekazanie"
  ]
  node [
    id 348
    label "derywat_z&#322;o&#380;ony"
  ]
  node [
    id 349
    label "fold"
  ]
  node [
    id 350
    label "pay"
  ]
  node [
    id 351
    label "leksem"
  ]
  node [
    id 352
    label "set"
  ]
  node [
    id 353
    label "przy&#322;o&#380;enie"
  ]
  node [
    id 354
    label "removal"
  ]
  node [
    id 355
    label "cz&#322;owiek_pracy"
  ]
  node [
    id 356
    label "delegowa&#263;"
  ]
  node [
    id 357
    label "s&#322;u&#380;ba_cywilna"
  ]
  node [
    id 358
    label "pracu&#347;"
  ]
  node [
    id 359
    label "delegowanie"
  ]
  node [
    id 360
    label "r&#281;ka"
  ]
  node [
    id 361
    label "salariat"
  ]
  node [
    id 362
    label "twierdzenie"
  ]
  node [
    id 363
    label "my&#347;l"
  ]
  node [
    id 364
    label "wnioskowanie"
  ]
  node [
    id 365
    label "propozycja"
  ]
  node [
    id 366
    label "motion"
  ]
  node [
    id 367
    label "pismo"
  ]
  node [
    id 368
    label "prayer"
  ]
  node [
    id 369
    label "ni&#380;szy"
  ]
  node [
    id 370
    label "spowodowanie"
  ]
  node [
    id 371
    label "zabrzmienie"
  ]
  node [
    id 372
    label "zmniejszenie"
  ]
  node [
    id 373
    label "suspension"
  ]
  node [
    id 374
    label "pad&#243;&#322;"
  ]
  node [
    id 375
    label "niski"
  ]
  node [
    id 376
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 377
    label "snub"
  ]
  node [
    id 378
    label "ukszta&#322;towanie_terenu"
  ]
  node [
    id 379
    label "kszta&#322;t"
  ]
  node [
    id 380
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 381
    label "znaczenie"
  ]
  node [
    id 382
    label "dymensja"
  ]
  node [
    id 383
    label "strona"
  ]
  node [
    id 384
    label "cecha"
  ]
  node [
    id 385
    label "parametr"
  ]
  node [
    id 386
    label "dane"
  ]
  node [
    id 387
    label "liczba"
  ]
  node [
    id 388
    label "wielko&#347;&#263;"
  ]
  node [
    id 389
    label "warunek_lokalowy"
  ]
  node [
    id 390
    label "czasokres"
  ]
  node [
    id 391
    label "trawienie"
  ]
  node [
    id 392
    label "kategoria_gramatyczna"
  ]
  node [
    id 393
    label "odczyt"
  ]
  node [
    id 394
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 395
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 396
    label "chwila"
  ]
  node [
    id 397
    label "poprzedzenie"
  ]
  node [
    id 398
    label "koniugacja"
  ]
  node [
    id 399
    label "poprzedzi&#263;"
  ]
  node [
    id 400
    label "przep&#322;ywanie"
  ]
  node [
    id 401
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 402
    label "odwlekanie_si&#281;"
  ]
  node [
    id 403
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 404
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 405
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 406
    label "pochodzi&#263;"
  ]
  node [
    id 407
    label "czwarty_wymiar"
  ]
  node [
    id 408
    label "chronometria"
  ]
  node [
    id 409
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 410
    label "poprzedzanie"
  ]
  node [
    id 411
    label "pogoda"
  ]
  node [
    id 412
    label "zegar"
  ]
  node [
    id 413
    label "pochodzenie"
  ]
  node [
    id 414
    label "poprzedza&#263;"
  ]
  node [
    id 415
    label "trawi&#263;"
  ]
  node [
    id 416
    label "rachuba_czasu"
  ]
  node [
    id 417
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 418
    label "czasoprzestrze&#324;"
  ]
  node [
    id 419
    label "laba"
  ]
  node [
    id 420
    label "return"
  ]
  node [
    id 421
    label "odyseja"
  ]
  node [
    id 422
    label "para"
  ]
  node [
    id 423
    label "wydarzenie"
  ]
  node [
    id 424
    label "rektyfikacja"
  ]
  node [
    id 425
    label "zmniejszy&#263;"
  ]
  node [
    id 426
    label "zabrzmie&#263;"
  ]
  node [
    id 427
    label "sink"
  ]
  node [
    id 428
    label "refuse"
  ]
  node [
    id 429
    label "zmieni&#263;"
  ]
  node [
    id 430
    label "fall"
  ]
  node [
    id 431
    label "wynik"
  ]
  node [
    id 432
    label "wyj&#347;cie"
  ]
  node [
    id 433
    label "spe&#322;nienie"
  ]
  node [
    id 434
    label "cesarskie_ci&#281;cie"
  ]
  node [
    id 435
    label "po&#322;o&#380;na"
  ]
  node [
    id 436
    label "przestanie"
  ]
  node [
    id 437
    label "usuni&#281;cie"
  ]
  node [
    id 438
    label "uniewa&#380;nienie"
  ]
  node [
    id 439
    label "birth"
  ]
  node [
    id 440
    label "wymy&#347;lenie"
  ]
  node [
    id 441
    label "po&#322;&#243;g"
  ]
  node [
    id 442
    label "szok_poporodowy"
  ]
  node [
    id 443
    label "event"
  ]
  node [
    id 444
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 445
    label "spos&#243;b"
  ]
  node [
    id 446
    label "rozwi&#261;zywanie_si&#281;"
  ]
  node [
    id 447
    label "dula"
  ]
  node [
    id 448
    label "si&#281;ga&#263;"
  ]
  node [
    id 449
    label "trwa&#263;"
  ]
  node [
    id 450
    label "obecno&#347;&#263;"
  ]
  node [
    id 451
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 452
    label "stand"
  ]
  node [
    id 453
    label "mie&#263;_miejsce"
  ]
  node [
    id 454
    label "uczestniczy&#263;"
  ]
  node [
    id 455
    label "chodzi&#263;"
  ]
  node [
    id 456
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 457
    label "equal"
  ]
  node [
    id 458
    label "mo&#380;liwy"
  ]
  node [
    id 459
    label "dopuszczalnie"
  ]
  node [
    id 460
    label "uderzenie"
  ]
  node [
    id 461
    label "cios"
  ]
  node [
    id 462
    label "time"
  ]
  node [
    id 463
    label "remark"
  ]
  node [
    id 464
    label "anons"
  ]
  node [
    id 465
    label "podanie"
  ]
  node [
    id 466
    label "wydanie"
  ]
  node [
    id 467
    label "issue"
  ]
  node [
    id 468
    label "promotion"
  ]
  node [
    id 469
    label "obwo&#322;anie"
  ]
  node [
    id 470
    label "zawiadomienie"
  ]
  node [
    id 471
    label "signal"
  ]
  node [
    id 472
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 473
    label "rujnowanie"
  ]
  node [
    id 474
    label "syndyk"
  ]
  node [
    id 475
    label "propozycja_uk&#322;adowa"
  ]
  node [
    id 476
    label "pogorszenie"
  ]
  node [
    id 477
    label "padaka"
  ]
  node [
    id 478
    label "sytuacja"
  ]
  node [
    id 479
    label "sp&#322;ata"
  ]
  node [
    id 480
    label "disposal"
  ]
  node [
    id 481
    label "ukrywa&#263;_si&#281;"
  ]
  node [
    id 482
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 483
    label "foray"
  ]
  node [
    id 484
    label "reach"
  ]
  node [
    id 485
    label "przebiega&#263;"
  ]
  node [
    id 486
    label "wpada&#263;"
  ]
  node [
    id 487
    label "nak&#322;ada&#263;_si&#281;"
  ]
  node [
    id 488
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 489
    label "intervene"
  ]
  node [
    id 490
    label "pokrywa&#263;"
  ]
  node [
    id 491
    label "dochodzi&#263;"
  ]
  node [
    id 492
    label "zachodzi&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 493
    label "przys&#322;ania&#263;"
  ]
  node [
    id 494
    label "pokrywa&#263;_si&#281;"
  ]
  node [
    id 495
    label "wsp&#243;&#322;wyst&#281;powa&#263;"
  ]
  node [
    id 496
    label "istnie&#263;"
  ]
  node [
    id 497
    label "podchodzi&#263;"
  ]
  node [
    id 498
    label "matuszka"
  ]
  node [
    id 499
    label "geneza"
  ]
  node [
    id 500
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 501
    label "czynnik"
  ]
  node [
    id 502
    label "poci&#261;ganie"
  ]
  node [
    id 503
    label "rezultat"
  ]
  node [
    id 504
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 505
    label "subject"
  ]
  node [
    id 506
    label "explain"
  ]
  node [
    id 507
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 508
    label "wstyd"
  ]
  node [
    id 509
    label "konsekwencja"
  ]
  node [
    id 510
    label "guilt"
  ]
  node [
    id 511
    label "lutnia"
  ]
  node [
    id 512
    label "Stefan"
  ]
  node [
    id 513
    label "Niesio&#322;owski"
  ]
  node [
    id 514
    label "El&#380;bieta"
  ]
  node [
    id 515
    label "Rafalska"
  ]
  node [
    id 516
    label "prawo"
  ]
  node [
    id 517
    label "i"
  ]
  node [
    id 518
    label "sprawiedliwo&#347;&#263;"
  ]
  node [
    id 519
    label "platforma"
  ]
  node [
    id 520
    label "obywatelski"
  ]
  node [
    id 521
    label "ch&#322;on&#261;&#263;"
  ]
  node [
    id 522
    label "Domi&#324;czak"
  ]
  node [
    id 523
    label "Jaros&#322;awa"
  ]
  node [
    id 524
    label "pi&#281;ta"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 6
    target 11
  ]
  edge [
    source 6
    target 12
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 9
  ]
  edge [
    source 6
    target 18
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 14
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 6
  ]
  edge [
    source 7
    target 123
  ]
  edge [
    source 7
    target 124
  ]
  edge [
    source 7
    target 125
  ]
  edge [
    source 7
    target 28
  ]
  edge [
    source 7
    target 126
  ]
  edge [
    source 7
    target 127
  ]
  edge [
    source 7
    target 128
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 7
    target 130
  ]
  edge [
    source 7
    target 131
  ]
  edge [
    source 7
    target 132
  ]
  edge [
    source 7
    target 133
  ]
  edge [
    source 7
    target 134
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 135
  ]
  edge [
    source 8
    target 136
  ]
  edge [
    source 8
    target 137
  ]
  edge [
    source 8
    target 138
  ]
  edge [
    source 8
    target 139
  ]
  edge [
    source 8
    target 140
  ]
  edge [
    source 8
    target 141
  ]
  edge [
    source 8
    target 142
  ]
  edge [
    source 8
    target 143
  ]
  edge [
    source 8
    target 144
  ]
  edge [
    source 8
    target 145
  ]
  edge [
    source 8
    target 146
  ]
  edge [
    source 8
    target 147
  ]
  edge [
    source 8
    target 148
  ]
  edge [
    source 8
    target 149
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 19
  ]
  edge [
    source 9
    target 20
  ]
  edge [
    source 9
    target 28
  ]
  edge [
    source 9
    target 21
  ]
  edge [
    source 9
    target 31
  ]
  edge [
    source 9
    target 46
  ]
  edge [
    source 9
    target 150
  ]
  edge [
    source 9
    target 151
  ]
  edge [
    source 9
    target 152
  ]
  edge [
    source 9
    target 153
  ]
  edge [
    source 9
    target 154
  ]
  edge [
    source 9
    target 155
  ]
  edge [
    source 9
    target 156
  ]
  edge [
    source 9
    target 157
  ]
  edge [
    source 9
    target 158
  ]
  edge [
    source 9
    target 159
  ]
  edge [
    source 9
    target 160
  ]
  edge [
    source 9
    target 161
  ]
  edge [
    source 9
    target 162
  ]
  edge [
    source 9
    target 163
  ]
  edge [
    source 9
    target 164
  ]
  edge [
    source 9
    target 165
  ]
  edge [
    source 9
    target 166
  ]
  edge [
    source 9
    target 167
  ]
  edge [
    source 9
    target 168
  ]
  edge [
    source 9
    target 169
  ]
  edge [
    source 9
    target 170
  ]
  edge [
    source 9
    target 171
  ]
  edge [
    source 9
    target 172
  ]
  edge [
    source 9
    target 18
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 173
  ]
  edge [
    source 11
    target 174
  ]
  edge [
    source 11
    target 175
  ]
  edge [
    source 11
    target 176
  ]
  edge [
    source 11
    target 177
  ]
  edge [
    source 11
    target 178
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 179
  ]
  edge [
    source 13
    target 180
  ]
  edge [
    source 13
    target 181
  ]
  edge [
    source 13
    target 182
  ]
  edge [
    source 13
    target 183
  ]
  edge [
    source 13
    target 184
  ]
  edge [
    source 13
    target 185
  ]
  edge [
    source 13
    target 186
  ]
  edge [
    source 13
    target 187
  ]
  edge [
    source 13
    target 188
  ]
  edge [
    source 13
    target 189
  ]
  edge [
    source 13
    target 190
  ]
  edge [
    source 13
    target 191
  ]
  edge [
    source 13
    target 192
  ]
  edge [
    source 13
    target 33
  ]
  edge [
    source 13
    target 193
  ]
  edge [
    source 13
    target 194
  ]
  edge [
    source 13
    target 195
  ]
  edge [
    source 13
    target 196
  ]
  edge [
    source 13
    target 197
  ]
  edge [
    source 13
    target 198
  ]
  edge [
    source 13
    target 199
  ]
  edge [
    source 13
    target 200
  ]
  edge [
    source 13
    target 201
  ]
  edge [
    source 13
    target 202
  ]
  edge [
    source 13
    target 203
  ]
  edge [
    source 13
    target 204
  ]
  edge [
    source 13
    target 205
  ]
  edge [
    source 13
    target 206
  ]
  edge [
    source 13
    target 169
  ]
  edge [
    source 13
    target 207
  ]
  edge [
    source 13
    target 208
  ]
  edge [
    source 13
    target 209
  ]
  edge [
    source 13
    target 210
  ]
  edge [
    source 13
    target 211
  ]
  edge [
    source 13
    target 212
  ]
  edge [
    source 13
    target 213
  ]
  edge [
    source 13
    target 214
  ]
  edge [
    source 13
    target 215
  ]
  edge [
    source 13
    target 216
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 23
  ]
  edge [
    source 14
    target 19
  ]
  edge [
    source 14
    target 40
  ]
  edge [
    source 14
    target 41
  ]
  edge [
    source 14
    target 217
  ]
  edge [
    source 14
    target 218
  ]
  edge [
    source 14
    target 219
  ]
  edge [
    source 14
    target 26
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 18
  ]
  edge [
    source 16
    target 220
  ]
  edge [
    source 16
    target 221
  ]
  edge [
    source 16
    target 222
  ]
  edge [
    source 16
    target 223
  ]
  edge [
    source 16
    target 224
  ]
  edge [
    source 16
    target 225
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 226
  ]
  edge [
    source 18
    target 227
  ]
  edge [
    source 18
    target 228
  ]
  edge [
    source 18
    target 229
  ]
  edge [
    source 18
    target 230
  ]
  edge [
    source 18
    target 231
  ]
  edge [
    source 18
    target 232
  ]
  edge [
    source 18
    target 233
  ]
  edge [
    source 19
    target 32
  ]
  edge [
    source 19
    target 31
  ]
  edge [
    source 19
    target 234
  ]
  edge [
    source 19
    target 235
  ]
  edge [
    source 19
    target 236
  ]
  edge [
    source 19
    target 237
  ]
  edge [
    source 19
    target 238
  ]
  edge [
    source 19
    target 239
  ]
  edge [
    source 19
    target 240
  ]
  edge [
    source 19
    target 241
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 242
  ]
  edge [
    source 20
    target 243
  ]
  edge [
    source 20
    target 28
  ]
  edge [
    source 20
    target 244
  ]
  edge [
    source 20
    target 245
  ]
  edge [
    source 20
    target 246
  ]
  edge [
    source 20
    target 247
  ]
  edge [
    source 20
    target 248
  ]
  edge [
    source 20
    target 249
  ]
  edge [
    source 20
    target 250
  ]
  edge [
    source 20
    target 251
  ]
  edge [
    source 20
    target 252
  ]
  edge [
    source 20
    target 253
  ]
  edge [
    source 20
    target 254
  ]
  edge [
    source 20
    target 255
  ]
  edge [
    source 20
    target 256
  ]
  edge [
    source 20
    target 257
  ]
  edge [
    source 20
    target 258
  ]
  edge [
    source 20
    target 259
  ]
  edge [
    source 20
    target 260
  ]
  edge [
    source 20
    target 261
  ]
  edge [
    source 20
    target 262
  ]
  edge [
    source 20
    target 263
  ]
  edge [
    source 20
    target 264
  ]
  edge [
    source 20
    target 265
  ]
  edge [
    source 20
    target 266
  ]
  edge [
    source 20
    target 267
  ]
  edge [
    source 20
    target 268
  ]
  edge [
    source 20
    target 269
  ]
  edge [
    source 20
    target 270
  ]
  edge [
    source 20
    target 271
  ]
  edge [
    source 20
    target 272
  ]
  edge [
    source 20
    target 273
  ]
  edge [
    source 20
    target 274
  ]
  edge [
    source 20
    target 275
  ]
  edge [
    source 20
    target 276
  ]
  edge [
    source 20
    target 277
  ]
  edge [
    source 20
    target 278
  ]
  edge [
    source 20
    target 279
  ]
  edge [
    source 20
    target 280
  ]
  edge [
    source 20
    target 281
  ]
  edge [
    source 20
    target 282
  ]
  edge [
    source 20
    target 283
  ]
  edge [
    source 20
    target 284
  ]
  edge [
    source 20
    target 285
  ]
  edge [
    source 20
    target 286
  ]
  edge [
    source 20
    target 287
  ]
  edge [
    source 20
    target 288
  ]
  edge [
    source 20
    target 289
  ]
  edge [
    source 20
    target 290
  ]
  edge [
    source 20
    target 291
  ]
  edge [
    source 20
    target 292
  ]
  edge [
    source 20
    target 293
  ]
  edge [
    source 20
    target 294
  ]
  edge [
    source 20
    target 295
  ]
  edge [
    source 20
    target 296
  ]
  edge [
    source 20
    target 297
  ]
  edge [
    source 20
    target 298
  ]
  edge [
    source 20
    target 299
  ]
  edge [
    source 20
    target 300
  ]
  edge [
    source 20
    target 301
  ]
  edge [
    source 20
    target 302
  ]
  edge [
    source 20
    target 303
  ]
  edge [
    source 20
    target 304
  ]
  edge [
    source 20
    target 305
  ]
  edge [
    source 20
    target 306
  ]
  edge [
    source 20
    target 307
  ]
  edge [
    source 20
    target 308
  ]
  edge [
    source 20
    target 43
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 29
  ]
  edge [
    source 21
    target 309
  ]
  edge [
    source 21
    target 310
  ]
  edge [
    source 21
    target 311
  ]
  edge [
    source 21
    target 28
  ]
  edge [
    source 21
    target 312
  ]
  edge [
    source 21
    target 313
  ]
  edge [
    source 21
    target 314
  ]
  edge [
    source 21
    target 315
  ]
  edge [
    source 21
    target 316
  ]
  edge [
    source 21
    target 317
  ]
  edge [
    source 21
    target 318
  ]
  edge [
    source 21
    target 319
  ]
  edge [
    source 21
    target 320
  ]
  edge [
    source 21
    target 321
  ]
  edge [
    source 21
    target 322
  ]
  edge [
    source 21
    target 323
  ]
  edge [
    source 21
    target 324
  ]
  edge [
    source 21
    target 325
  ]
  edge [
    source 21
    target 326
  ]
  edge [
    source 21
    target 327
  ]
  edge [
    source 21
    target 328
  ]
  edge [
    source 21
    target 329
  ]
  edge [
    source 21
    target 330
  ]
  edge [
    source 21
    target 331
  ]
  edge [
    source 21
    target 332
  ]
  edge [
    source 21
    target 333
  ]
  edge [
    source 21
    target 334
  ]
  edge [
    source 21
    target 335
  ]
  edge [
    source 21
    target 336
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 337
  ]
  edge [
    source 22
    target 338
  ]
  edge [
    source 22
    target 339
  ]
  edge [
    source 22
    target 340
  ]
  edge [
    source 22
    target 341
  ]
  edge [
    source 22
    target 342
  ]
  edge [
    source 22
    target 343
  ]
  edge [
    source 22
    target 344
  ]
  edge [
    source 22
    target 345
  ]
  edge [
    source 22
    target 346
  ]
  edge [
    source 22
    target 347
  ]
  edge [
    source 22
    target 348
  ]
  edge [
    source 22
    target 349
  ]
  edge [
    source 22
    target 350
  ]
  edge [
    source 22
    target 351
  ]
  edge [
    source 22
    target 352
  ]
  edge [
    source 22
    target 353
  ]
  edge [
    source 22
    target 354
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 31
  ]
  edge [
    source 23
    target 38
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 46
  ]
  edge [
    source 24
    target 355
  ]
  edge [
    source 24
    target 47
  ]
  edge [
    source 24
    target 356
  ]
  edge [
    source 24
    target 357
  ]
  edge [
    source 24
    target 358
  ]
  edge [
    source 24
    target 359
  ]
  edge [
    source 24
    target 360
  ]
  edge [
    source 24
    target 361
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 362
  ]
  edge [
    source 25
    target 363
  ]
  edge [
    source 25
    target 364
  ]
  edge [
    source 25
    target 365
  ]
  edge [
    source 25
    target 366
  ]
  edge [
    source 25
    target 367
  ]
  edge [
    source 25
    target 368
  ]
  edge [
    source 25
    target 44
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 369
  ]
  edge [
    source 26
    target 164
  ]
  edge [
    source 26
    target 370
  ]
  edge [
    source 26
    target 371
  ]
  edge [
    source 26
    target 169
  ]
  edge [
    source 26
    target 372
  ]
  edge [
    source 26
    target 373
  ]
  edge [
    source 26
    target 374
  ]
  edge [
    source 26
    target 375
  ]
  edge [
    source 26
    target 376
  ]
  edge [
    source 26
    target 377
  ]
  edge [
    source 26
    target 378
  ]
  edge [
    source 26
    target 379
  ]
  edge [
    source 26
    target 380
  ]
  edge [
    source 26
    target 40
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 30
  ]
  edge [
    source 27
    target 192
  ]
  edge [
    source 27
    target 381
  ]
  edge [
    source 27
    target 382
  ]
  edge [
    source 27
    target 383
  ]
  edge [
    source 27
    target 384
  ]
  edge [
    source 27
    target 385
  ]
  edge [
    source 27
    target 386
  ]
  edge [
    source 27
    target 387
  ]
  edge [
    source 27
    target 199
  ]
  edge [
    source 27
    target 388
  ]
  edge [
    source 27
    target 389
  ]
  edge [
    source 28
    target 32
  ]
  edge [
    source 28
    target 33
  ]
  edge [
    source 28
    target 390
  ]
  edge [
    source 28
    target 391
  ]
  edge [
    source 28
    target 392
  ]
  edge [
    source 28
    target 244
  ]
  edge [
    source 28
    target 393
  ]
  edge [
    source 28
    target 394
  ]
  edge [
    source 28
    target 395
  ]
  edge [
    source 28
    target 396
  ]
  edge [
    source 28
    target 250
  ]
  edge [
    source 28
    target 397
  ]
  edge [
    source 28
    target 398
  ]
  edge [
    source 28
    target 259
  ]
  edge [
    source 28
    target 399
  ]
  edge [
    source 28
    target 400
  ]
  edge [
    source 28
    target 401
  ]
  edge [
    source 28
    target 402
  ]
  edge [
    source 28
    target 403
  ]
  edge [
    source 28
    target 273
  ]
  edge [
    source 28
    target 404
  ]
  edge [
    source 28
    target 278
  ]
  edge [
    source 28
    target 405
  ]
  edge [
    source 28
    target 406
  ]
  edge [
    source 28
    target 280
  ]
  edge [
    source 28
    target 407
  ]
  edge [
    source 28
    target 408
  ]
  edge [
    source 28
    target 409
  ]
  edge [
    source 28
    target 410
  ]
  edge [
    source 28
    target 411
  ]
  edge [
    source 28
    target 412
  ]
  edge [
    source 28
    target 413
  ]
  edge [
    source 28
    target 414
  ]
  edge [
    source 28
    target 415
  ]
  edge [
    source 28
    target 298
  ]
  edge [
    source 28
    target 416
  ]
  edge [
    source 28
    target 417
  ]
  edge [
    source 28
    target 418
  ]
  edge [
    source 28
    target 419
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 420
  ]
  edge [
    source 29
    target 421
  ]
  edge [
    source 29
    target 422
  ]
  edge [
    source 29
    target 423
  ]
  edge [
    source 29
    target 424
  ]
  edge [
    source 29
    target 41
  ]
  edge [
    source 30
    target 425
  ]
  edge [
    source 30
    target 426
  ]
  edge [
    source 30
    target 427
  ]
  edge [
    source 30
    target 428
  ]
  edge [
    source 30
    target 429
  ]
  edge [
    source 30
    target 430
  ]
  edge [
    source 31
    target 45
  ]
  edge [
    source 31
    target 431
  ]
  edge [
    source 31
    target 432
  ]
  edge [
    source 31
    target 433
  ]
  edge [
    source 31
    target 434
  ]
  edge [
    source 31
    target 435
  ]
  edge [
    source 31
    target 296
  ]
  edge [
    source 31
    target 436
  ]
  edge [
    source 31
    target 117
  ]
  edge [
    source 31
    target 437
  ]
  edge [
    source 31
    target 438
  ]
  edge [
    source 31
    target 112
  ]
  edge [
    source 31
    target 439
  ]
  edge [
    source 31
    target 440
  ]
  edge [
    source 31
    target 441
  ]
  edge [
    source 31
    target 442
  ]
  edge [
    source 31
    target 443
  ]
  edge [
    source 31
    target 444
  ]
  edge [
    source 31
    target 445
  ]
  edge [
    source 31
    target 446
  ]
  edge [
    source 31
    target 447
  ]
  edge [
    source 31
    target 37
  ]
  edge [
    source 31
    target 38
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 448
  ]
  edge [
    source 33
    target 449
  ]
  edge [
    source 33
    target 450
  ]
  edge [
    source 33
    target 451
  ]
  edge [
    source 33
    target 452
  ]
  edge [
    source 33
    target 453
  ]
  edge [
    source 33
    target 454
  ]
  edge [
    source 33
    target 455
  ]
  edge [
    source 33
    target 456
  ]
  edge [
    source 33
    target 457
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 458
  ]
  edge [
    source 34
    target 459
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 396
  ]
  edge [
    source 36
    target 460
  ]
  edge [
    source 36
    target 461
  ]
  edge [
    source 36
    target 462
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 463
  ]
  edge [
    source 37
    target 464
  ]
  edge [
    source 37
    target 465
  ]
  edge [
    source 37
    target 466
  ]
  edge [
    source 37
    target 467
  ]
  edge [
    source 37
    target 468
  ]
  edge [
    source 37
    target 469
  ]
  edge [
    source 37
    target 470
  ]
  edge [
    source 37
    target 471
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 472
  ]
  edge [
    source 38
    target 473
  ]
  edge [
    source 38
    target 384
  ]
  edge [
    source 38
    target 474
  ]
  edge [
    source 38
    target 475
  ]
  edge [
    source 38
    target 476
  ]
  edge [
    source 38
    target 477
  ]
  edge [
    source 38
    target 478
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 40
    target 479
  ]
  edge [
    source 40
    target 423
  ]
  edge [
    source 40
    target 480
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 481
  ]
  edge [
    source 43
    target 482
  ]
  edge [
    source 43
    target 483
  ]
  edge [
    source 43
    target 484
  ]
  edge [
    source 43
    target 485
  ]
  edge [
    source 43
    target 453
  ]
  edge [
    source 43
    target 486
  ]
  edge [
    source 43
    target 487
  ]
  edge [
    source 43
    target 488
  ]
  edge [
    source 43
    target 489
  ]
  edge [
    source 43
    target 490
  ]
  edge [
    source 43
    target 491
  ]
  edge [
    source 43
    target 492
  ]
  edge [
    source 43
    target 493
  ]
  edge [
    source 43
    target 494
  ]
  edge [
    source 43
    target 495
  ]
  edge [
    source 43
    target 496
  ]
  edge [
    source 43
    target 497
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 498
  ]
  edge [
    source 44
    target 499
  ]
  edge [
    source 44
    target 500
  ]
  edge [
    source 44
    target 501
  ]
  edge [
    source 44
    target 502
  ]
  edge [
    source 44
    target 503
  ]
  edge [
    source 44
    target 504
  ]
  edge [
    source 44
    target 505
  ]
  edge [
    source 45
    target 506
  ]
  edge [
    source 45
    target 507
  ]
  edge [
    source 46
    target 508
  ]
  edge [
    source 46
    target 509
  ]
  edge [
    source 46
    target 510
  ]
  edge [
    source 46
    target 511
  ]
  edge [
    source 512
    target 513
  ]
  edge [
    source 514
    target 515
  ]
  edge [
    source 516
    target 517
  ]
  edge [
    source 516
    target 518
  ]
  edge [
    source 517
    target 518
  ]
  edge [
    source 519
    target 520
  ]
  edge [
    source 521
    target 522
  ]
  edge [
    source 523
    target 524
  ]
]
