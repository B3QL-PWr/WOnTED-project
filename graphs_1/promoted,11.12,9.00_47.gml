graph [
  maxDegree 16
  minDegree 1
  meanDegree 1.9090909090909092
  density 0.09090909090909091
  graphCliqueNumber 2
  node [
    id 0
    label "cze&#347;&#263;"
    origin "text"
  ]
  node [
    id 1
    label "wita&#263;"
    origin "text"
  ]
  node [
    id 2
    label "zaimponowanie"
  ]
  node [
    id 3
    label "szanowa&#263;"
  ]
  node [
    id 4
    label "uhonorowa&#263;"
  ]
  node [
    id 5
    label "honorowanie"
  ]
  node [
    id 6
    label "uszanowa&#263;"
  ]
  node [
    id 7
    label "rewerencja"
  ]
  node [
    id 8
    label "uszanowanie"
  ]
  node [
    id 9
    label "imponowanie"
  ]
  node [
    id 10
    label "dobro"
  ]
  node [
    id 11
    label "uhonorowanie"
  ]
  node [
    id 12
    label "respect"
  ]
  node [
    id 13
    label "honorowa&#263;"
  ]
  node [
    id 14
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 15
    label "szacuneczek"
  ]
  node [
    id 16
    label "postawa"
  ]
  node [
    id 17
    label "pozdrawia&#263;"
  ]
  node [
    id 18
    label "&#347;wi&#281;towa&#263;"
  ]
  node [
    id 19
    label "greet"
  ]
  node [
    id 20
    label "uzewn&#281;trznia&#263;_si&#281;"
  ]
  node [
    id 21
    label "welcome"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
]
