graph [
  maxDegree 52
  minDegree 1
  meanDegree 2.1652173913043478
  density 0.009455097778621606
  graphCliqueNumber 4
  node [
    id 0
    label "uog&#243;lnienie"
    origin "text"
  ]
  node [
    id 1
    label "klasa"
    origin "text"
  ]
  node [
    id 2
    label "opiewa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "definicja"
    origin "text"
  ]
  node [
    id 4
    label "abstrakcyjny"
    origin "text"
  ]
  node [
    id 5
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 6
    label "reprezentowa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "obiekt"
    origin "text"
  ]
  node [
    id 8
    label "dziedziczy&#263;"
    origin "text"
  ]
  node [
    id 9
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 10
    label "skonkretyzowa&#263;"
    origin "text"
  ]
  node [
    id 11
    label "niekt&#243;ry"
    origin "text"
  ]
  node [
    id 12
    label "cech"
    origin "text"
  ]
  node [
    id 13
    label "zachowanie"
    origin "text"
  ]
  node [
    id 14
    label "metoda"
    origin "text"
  ]
  node [
    id 15
    label "s&#322;u&#380;y&#263;"
    origin "text"
  ]
  node [
    id 16
    label "okre&#347;lenie"
    origin "text"
  ]
  node [
    id 17
    label "interfejs"
    origin "text"
  ]
  node [
    id 18
    label "posiada&#263;"
    origin "text"
  ]
  node [
    id 19
    label "cia&#322;o"
    origin "text"
  ]
  node [
    id 20
    label "metody"
    origin "text"
  ]
  node [
    id 21
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 22
    label "wywo&#322;a&#263;"
    origin "text"
  ]
  node [
    id 23
    label "by&#263;"
    origin "text"
  ]
  node [
    id 24
    label "r&#243;wnie&#380;"
    origin "text"
  ]
  node [
    id 25
    label "twierdzenie"
  ]
  node [
    id 26
    label "powi&#261;zanie"
  ]
  node [
    id 27
    label "abstraction"
  ]
  node [
    id 28
    label "typ"
  ]
  node [
    id 29
    label "warstwa"
  ]
  node [
    id 30
    label "znak_jako&#347;ci"
  ]
  node [
    id 31
    label "przedmiot"
  ]
  node [
    id 32
    label "przepisa&#263;"
  ]
  node [
    id 33
    label "grupa"
  ]
  node [
    id 34
    label "pomoc"
  ]
  node [
    id 35
    label "arrangement"
  ]
  node [
    id 36
    label "wagon"
  ]
  node [
    id 37
    label "form"
  ]
  node [
    id 38
    label "zaleta"
  ]
  node [
    id 39
    label "poziom"
  ]
  node [
    id 40
    label "dziennik_lekcyjny"
  ]
  node [
    id 41
    label "&#347;rodowisko"
  ]
  node [
    id 42
    label "atak"
  ]
  node [
    id 43
    label "przepisanie"
  ]
  node [
    id 44
    label "szko&#322;a"
  ]
  node [
    id 45
    label "class"
  ]
  node [
    id 46
    label "organizacja"
  ]
  node [
    id 47
    label "obrona"
  ]
  node [
    id 48
    label "type"
  ]
  node [
    id 49
    label "promocja"
  ]
  node [
    id 50
    label "&#322;awka"
  ]
  node [
    id 51
    label "kurs"
  ]
  node [
    id 52
    label "botanika"
  ]
  node [
    id 53
    label "sala"
  ]
  node [
    id 54
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 55
    label "gromada"
  ]
  node [
    id 56
    label "Ekwici"
  ]
  node [
    id 57
    label "fakcja"
  ]
  node [
    id 58
    label "tablica"
  ]
  node [
    id 59
    label "programowanie_obiektowe"
  ]
  node [
    id 60
    label "wykrzyknik"
  ]
  node [
    id 61
    label "jednostka_systematyczna"
  ]
  node [
    id 62
    label "mecz_mistrzowski"
  ]
  node [
    id 63
    label "zbi&#243;r"
  ]
  node [
    id 64
    label "jako&#347;&#263;"
  ]
  node [
    id 65
    label "rezerwa"
  ]
  node [
    id 66
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 67
    label "chwali&#263;"
  ]
  node [
    id 68
    label "pia&#263;"
  ]
  node [
    id 69
    label "express"
  ]
  node [
    id 70
    label "rozg&#322;asza&#263;"
  ]
  node [
    id 71
    label "podawa&#263;"
  ]
  node [
    id 72
    label "os&#322;awia&#263;"
  ]
  node [
    id 73
    label "read"
  ]
  node [
    id 74
    label "definiens"
  ]
  node [
    id 75
    label "definiendum"
  ]
  node [
    id 76
    label "obja&#347;nienie"
  ]
  node [
    id 77
    label "definition"
  ]
  node [
    id 78
    label "nierealistyczny"
  ]
  node [
    id 79
    label "abstrakcyjnie"
  ]
  node [
    id 80
    label "teoretyczny"
  ]
  node [
    id 81
    label "my&#347;lowy"
  ]
  node [
    id 82
    label "niekonwencjonalny"
  ]
  node [
    id 83
    label "oryginalny"
  ]
  node [
    id 84
    label "act"
  ]
  node [
    id 85
    label "sprawowa&#263;"
  ]
  node [
    id 86
    label "wyra&#380;a&#263;"
  ]
  node [
    id 87
    label "represent"
  ]
  node [
    id 88
    label "co&#347;"
  ]
  node [
    id 89
    label "budynek"
  ]
  node [
    id 90
    label "program"
  ]
  node [
    id 91
    label "rzecz"
  ]
  node [
    id 92
    label "thing"
  ]
  node [
    id 93
    label "poj&#281;cie"
  ]
  node [
    id 94
    label "strona"
  ]
  node [
    id 95
    label "mie&#263;_miejsce"
  ]
  node [
    id 96
    label "gen"
  ]
  node [
    id 97
    label "dostawa&#263;"
  ]
  node [
    id 98
    label "uprawi&#263;"
  ]
  node [
    id 99
    label "gotowy"
  ]
  node [
    id 100
    label "might"
  ]
  node [
    id 101
    label "urzeczywistni&#263;"
  ]
  node [
    id 102
    label "clear"
  ]
  node [
    id 103
    label "sprecyzowa&#263;"
  ]
  node [
    id 104
    label "jaki&#347;"
  ]
  node [
    id 105
    label "cechmistrz"
  ]
  node [
    id 106
    label "czeladnik"
  ]
  node [
    id 107
    label "stowarzyszenie"
  ]
  node [
    id 108
    label "club"
  ]
  node [
    id 109
    label "zwierz&#281;"
  ]
  node [
    id 110
    label "zrobienie"
  ]
  node [
    id 111
    label "podporz&#261;dkowanie_si&#281;"
  ]
  node [
    id 112
    label "podtrzymanie"
  ]
  node [
    id 113
    label "reakcja"
  ]
  node [
    id 114
    label "tajemnica"
  ]
  node [
    id 115
    label "zdyscyplinowanie"
  ]
  node [
    id 116
    label "observation"
  ]
  node [
    id 117
    label "behawior"
  ]
  node [
    id 118
    label "dieta"
  ]
  node [
    id 119
    label "bearing"
  ]
  node [
    id 120
    label "pochowanie"
  ]
  node [
    id 121
    label "wydarzenie"
  ]
  node [
    id 122
    label "przechowanie"
  ]
  node [
    id 123
    label "post&#261;pienie"
  ]
  node [
    id 124
    label "post"
  ]
  node [
    id 125
    label "struktura"
  ]
  node [
    id 126
    label "spos&#243;b"
  ]
  node [
    id 127
    label "etolog"
  ]
  node [
    id 128
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 129
    label "method"
  ]
  node [
    id 130
    label "doktryna"
  ]
  node [
    id 131
    label "use"
  ]
  node [
    id 132
    label "trwa&#263;"
  ]
  node [
    id 133
    label "&#380;o&#322;nierz"
  ]
  node [
    id 134
    label "pies"
  ]
  node [
    id 135
    label "robi&#263;"
  ]
  node [
    id 136
    label "wait"
  ]
  node [
    id 137
    label "pomaga&#263;"
  ]
  node [
    id 138
    label "cel"
  ]
  node [
    id 139
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 140
    label "przydawa&#263;_si&#281;"
  ]
  node [
    id 141
    label "pracowa&#263;"
  ]
  node [
    id 142
    label "suffice"
  ]
  node [
    id 143
    label "match"
  ]
  node [
    id 144
    label "s&#322;ugiwa&#263;"
  ]
  node [
    id 145
    label "zdecydowanie"
  ]
  node [
    id 146
    label "follow-up"
  ]
  node [
    id 147
    label "appointment"
  ]
  node [
    id 148
    label "ustalenie"
  ]
  node [
    id 149
    label "localization"
  ]
  node [
    id 150
    label "denomination"
  ]
  node [
    id 151
    label "wyra&#380;enie"
  ]
  node [
    id 152
    label "ozdobnik"
  ]
  node [
    id 153
    label "przewidzenie"
  ]
  node [
    id 154
    label "term"
  ]
  node [
    id 155
    label "urz&#261;dzenie"
  ]
  node [
    id 156
    label "wiedzie&#263;"
  ]
  node [
    id 157
    label "mie&#263;"
  ]
  node [
    id 158
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 159
    label "keep_open"
  ]
  node [
    id 160
    label "zawiera&#263;"
  ]
  node [
    id 161
    label "support"
  ]
  node [
    id 162
    label "zdolno&#347;&#263;"
  ]
  node [
    id 163
    label "balsamowa&#263;"
  ]
  node [
    id 164
    label "Komitet_Region&#243;w"
  ]
  node [
    id 165
    label "pochowa&#263;"
  ]
  node [
    id 166
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 167
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 168
    label "odwodnienie"
  ]
  node [
    id 169
    label "otworzenie"
  ]
  node [
    id 170
    label "zabalsamowanie"
  ]
  node [
    id 171
    label "tanatoplastyk"
  ]
  node [
    id 172
    label "biorytm"
  ]
  node [
    id 173
    label "istota_&#380;ywa"
  ]
  node [
    id 174
    label "zabalsamowa&#263;"
  ]
  node [
    id 175
    label "pogrzeb"
  ]
  node [
    id 176
    label "otwieranie"
  ]
  node [
    id 177
    label "tanatoplastyka"
  ]
  node [
    id 178
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 179
    label "l&#281;d&#378;wie"
  ]
  node [
    id 180
    label "sk&#243;ra"
  ]
  node [
    id 181
    label "nieumar&#322;y"
  ]
  node [
    id 182
    label "unerwienie"
  ]
  node [
    id 183
    label "sekcja"
  ]
  node [
    id 184
    label "ow&#322;osienie"
  ]
  node [
    id 185
    label "odwadnia&#263;"
  ]
  node [
    id 186
    label "zesp&#243;&#322;"
  ]
  node [
    id 187
    label "ekshumowa&#263;"
  ]
  node [
    id 188
    label "jednostka_organizacyjna"
  ]
  node [
    id 189
    label "kremacja"
  ]
  node [
    id 190
    label "ekshumowanie"
  ]
  node [
    id 191
    label "otworzy&#263;"
  ]
  node [
    id 192
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 193
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 194
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 195
    label "balsamowanie"
  ]
  node [
    id 196
    label "Izba_Konsyliarska"
  ]
  node [
    id 197
    label "odwadnianie"
  ]
  node [
    id 198
    label "uk&#322;ad"
  ]
  node [
    id 199
    label "cz&#322;onek"
  ]
  node [
    id 200
    label "miejsce"
  ]
  node [
    id 201
    label "szkielet"
  ]
  node [
    id 202
    label "odwodni&#263;"
  ]
  node [
    id 203
    label "ty&#322;"
  ]
  node [
    id 204
    label "materia"
  ]
  node [
    id 205
    label "temperatura"
  ]
  node [
    id 206
    label "staw"
  ]
  node [
    id 207
    label "mi&#281;so"
  ]
  node [
    id 208
    label "prz&#243;d"
  ]
  node [
    id 209
    label "otwiera&#263;"
  ]
  node [
    id 210
    label "p&#322;aszczyzna"
  ]
  node [
    id 211
    label "free"
  ]
  node [
    id 212
    label "trip"
  ]
  node [
    id 213
    label "spowodowa&#263;"
  ]
  node [
    id 214
    label "przetworzy&#263;"
  ]
  node [
    id 215
    label "wydali&#263;"
  ]
  node [
    id 216
    label "wezwa&#263;"
  ]
  node [
    id 217
    label "revolutionize"
  ]
  node [
    id 218
    label "arouse"
  ]
  node [
    id 219
    label "train"
  ]
  node [
    id 220
    label "oznajmi&#263;"
  ]
  node [
    id 221
    label "poleci&#263;"
  ]
  node [
    id 222
    label "si&#281;ga&#263;"
  ]
  node [
    id 223
    label "obecno&#347;&#263;"
  ]
  node [
    id 224
    label "stan"
  ]
  node [
    id 225
    label "stand"
  ]
  node [
    id 226
    label "uczestniczy&#263;"
  ]
  node [
    id 227
    label "chodzi&#263;"
  ]
  node [
    id 228
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 229
    label "equal"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 0
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 4
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 23
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 9
    target 100
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 12
    target 107
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 13
    target 113
  ]
  edge [
    source 13
    target 114
  ]
  edge [
    source 13
    target 115
  ]
  edge [
    source 13
    target 116
  ]
  edge [
    source 13
    target 117
  ]
  edge [
    source 13
    target 118
  ]
  edge [
    source 13
    target 119
  ]
  edge [
    source 13
    target 120
  ]
  edge [
    source 13
    target 121
  ]
  edge [
    source 13
    target 122
  ]
  edge [
    source 13
    target 123
  ]
  edge [
    source 13
    target 124
  ]
  edge [
    source 13
    target 125
  ]
  edge [
    source 13
    target 126
  ]
  edge [
    source 13
    target 127
  ]
  edge [
    source 14
    target 128
  ]
  edge [
    source 14
    target 129
  ]
  edge [
    source 14
    target 126
  ]
  edge [
    source 14
    target 130
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 131
  ]
  edge [
    source 15
    target 132
  ]
  edge [
    source 15
    target 23
  ]
  edge [
    source 15
    target 133
  ]
  edge [
    source 15
    target 134
  ]
  edge [
    source 15
    target 135
  ]
  edge [
    source 15
    target 136
  ]
  edge [
    source 15
    target 137
  ]
  edge [
    source 15
    target 138
  ]
  edge [
    source 15
    target 139
  ]
  edge [
    source 15
    target 140
  ]
  edge [
    source 15
    target 141
  ]
  edge [
    source 15
    target 142
  ]
  edge [
    source 15
    target 143
  ]
  edge [
    source 15
    target 144
  ]
  edge [
    source 15
    target 24
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 145
  ]
  edge [
    source 16
    target 146
  ]
  edge [
    source 16
    target 147
  ]
  edge [
    source 16
    target 148
  ]
  edge [
    source 16
    target 149
  ]
  edge [
    source 16
    target 150
  ]
  edge [
    source 16
    target 151
  ]
  edge [
    source 16
    target 152
  ]
  edge [
    source 16
    target 153
  ]
  edge [
    source 16
    target 154
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 90
  ]
  edge [
    source 17
    target 155
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 156
  ]
  edge [
    source 18
    target 157
  ]
  edge [
    source 18
    target 158
  ]
  edge [
    source 18
    target 159
  ]
  edge [
    source 18
    target 160
  ]
  edge [
    source 18
    target 161
  ]
  edge [
    source 18
    target 162
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 163
  ]
  edge [
    source 19
    target 164
  ]
  edge [
    source 19
    target 165
  ]
  edge [
    source 19
    target 166
  ]
  edge [
    source 19
    target 167
  ]
  edge [
    source 19
    target 168
  ]
  edge [
    source 19
    target 169
  ]
  edge [
    source 19
    target 170
  ]
  edge [
    source 19
    target 171
  ]
  edge [
    source 19
    target 172
  ]
  edge [
    source 19
    target 173
  ]
  edge [
    source 19
    target 174
  ]
  edge [
    source 19
    target 175
  ]
  edge [
    source 19
    target 176
  ]
  edge [
    source 19
    target 177
  ]
  edge [
    source 19
    target 178
  ]
  edge [
    source 19
    target 179
  ]
  edge [
    source 19
    target 180
  ]
  edge [
    source 19
    target 181
  ]
  edge [
    source 19
    target 182
  ]
  edge [
    source 19
    target 183
  ]
  edge [
    source 19
    target 184
  ]
  edge [
    source 19
    target 185
  ]
  edge [
    source 19
    target 186
  ]
  edge [
    source 19
    target 187
  ]
  edge [
    source 19
    target 188
  ]
  edge [
    source 19
    target 189
  ]
  edge [
    source 19
    target 120
  ]
  edge [
    source 19
    target 190
  ]
  edge [
    source 19
    target 191
  ]
  edge [
    source 19
    target 192
  ]
  edge [
    source 19
    target 193
  ]
  edge [
    source 19
    target 194
  ]
  edge [
    source 19
    target 195
  ]
  edge [
    source 19
    target 196
  ]
  edge [
    source 19
    target 197
  ]
  edge [
    source 19
    target 198
  ]
  edge [
    source 19
    target 199
  ]
  edge [
    source 19
    target 200
  ]
  edge [
    source 19
    target 201
  ]
  edge [
    source 19
    target 202
  ]
  edge [
    source 19
    target 203
  ]
  edge [
    source 19
    target 204
  ]
  edge [
    source 19
    target 63
  ]
  edge [
    source 19
    target 205
  ]
  edge [
    source 19
    target 206
  ]
  edge [
    source 19
    target 207
  ]
  edge [
    source 19
    target 208
  ]
  edge [
    source 19
    target 209
  ]
  edge [
    source 19
    target 210
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 211
  ]
  edge [
    source 22
    target 212
  ]
  edge [
    source 22
    target 213
  ]
  edge [
    source 22
    target 214
  ]
  edge [
    source 22
    target 215
  ]
  edge [
    source 22
    target 216
  ]
  edge [
    source 22
    target 217
  ]
  edge [
    source 22
    target 218
  ]
  edge [
    source 22
    target 219
  ]
  edge [
    source 22
    target 220
  ]
  edge [
    source 22
    target 221
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 222
  ]
  edge [
    source 23
    target 132
  ]
  edge [
    source 23
    target 223
  ]
  edge [
    source 23
    target 224
  ]
  edge [
    source 23
    target 158
  ]
  edge [
    source 23
    target 225
  ]
  edge [
    source 23
    target 95
  ]
  edge [
    source 23
    target 226
  ]
  edge [
    source 23
    target 227
  ]
  edge [
    source 23
    target 228
  ]
  edge [
    source 23
    target 229
  ]
]
