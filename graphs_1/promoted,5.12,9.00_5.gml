graph [
  maxDegree 29
  minDegree 1
  meanDegree 2.16
  density 0.02918918918918919
  graphCliqueNumber 5
  node [
    id 0
    label "niezapomniany"
    origin "text"
  ]
  node [
    id 1
    label "irena"
    origin "text"
  ]
  node [
    id 2
    label "kwiatowska"
    origin "text"
  ]
  node [
    id 3
    label "kobieta"
    origin "text"
  ]
  node [
    id 4
    label "pracuj&#261;ca"
    origin "text"
  ]
  node [
    id 5
    label "leonard"
    origin "text"
  ]
  node [
    id 6
    label "pietraszak"
    origin "text"
  ]
  node [
    id 7
    label "znakomity"
    origin "text"
  ]
  node [
    id 8
    label "scena"
    origin "text"
  ]
  node [
    id 9
    label "pami&#281;tny"
  ]
  node [
    id 10
    label "pami&#281;tnie"
  ]
  node [
    id 11
    label "wa&#380;ny"
  ]
  node [
    id 12
    label "cz&#322;owiek"
  ]
  node [
    id 13
    label "przekwitanie"
  ]
  node [
    id 14
    label "m&#281;&#380;yna"
  ]
  node [
    id 15
    label "babka"
  ]
  node [
    id 16
    label "samica"
  ]
  node [
    id 17
    label "doros&#322;y"
  ]
  node [
    id 18
    label "ulec"
  ]
  node [
    id 19
    label "uleganie"
  ]
  node [
    id 20
    label "partnerka"
  ]
  node [
    id 21
    label "&#380;ona"
  ]
  node [
    id 22
    label "ulega&#263;"
  ]
  node [
    id 23
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 24
    label "pa&#324;stwo"
  ]
  node [
    id 25
    label "ulegni&#281;cie"
  ]
  node [
    id 26
    label "menopauza"
  ]
  node [
    id 27
    label "&#322;ono"
  ]
  node [
    id 28
    label "pomy&#347;lny"
  ]
  node [
    id 29
    label "znacz&#261;cy"
  ]
  node [
    id 30
    label "rewelacyjnie"
  ]
  node [
    id 31
    label "pozytywny"
  ]
  node [
    id 32
    label "wspaniale"
  ]
  node [
    id 33
    label "dobry"
  ]
  node [
    id 34
    label "wspania&#322;y"
  ]
  node [
    id 35
    label "znakomicie"
  ]
  node [
    id 36
    label "istotny"
  ]
  node [
    id 37
    label "&#347;wietnie"
  ]
  node [
    id 38
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 39
    label "arcydzielny"
  ]
  node [
    id 40
    label "skuteczny"
  ]
  node [
    id 41
    label "zajebisty"
  ]
  node [
    id 42
    label "spania&#322;y"
  ]
  node [
    id 43
    label "proscenium"
  ]
  node [
    id 44
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 45
    label "Teatr_&#321;a&#378;nia_Nowa"
  ]
  node [
    id 46
    label "stadium"
  ]
  node [
    id 47
    label "antyteatr"
  ]
  node [
    id 48
    label "sztuka"
  ]
  node [
    id 49
    label "fragment"
  ]
  node [
    id 50
    label "sznurownia"
  ]
  node [
    id 51
    label "widzownia"
  ]
  node [
    id 52
    label "kiesze&#324;"
  ]
  node [
    id 53
    label "teren"
  ]
  node [
    id 54
    label "horyzont"
  ]
  node [
    id 55
    label "podest"
  ]
  node [
    id 56
    label "przedstawia&#263;"
  ]
  node [
    id 57
    label "podwy&#380;szenie"
  ]
  node [
    id 58
    label "budka_suflera"
  ]
  node [
    id 59
    label "akt"
  ]
  node [
    id 60
    label "przedstawianie"
  ]
  node [
    id 61
    label "wydarzenie"
  ]
  node [
    id 62
    label "kurtyna"
  ]
  node [
    id 63
    label "epizod"
  ]
  node [
    id 64
    label "sphere"
  ]
  node [
    id 65
    label "przedstawienie"
  ]
  node [
    id 66
    label "nadscenie"
  ]
  node [
    id 67
    label "film"
  ]
  node [
    id 68
    label "k&#322;&#243;tnia"
  ]
  node [
    id 69
    label "instytucja"
  ]
  node [
    id 70
    label "dramaturgy"
  ]
  node [
    id 71
    label "Irena"
  ]
  node [
    id 72
    label "Kwiatowska"
  ]
  node [
    id 73
    label "Leonard"
  ]
  node [
    id 74
    label "Pietraszak"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 28
  ]
  edge [
    source 7
    target 29
  ]
  edge [
    source 7
    target 30
  ]
  edge [
    source 7
    target 31
  ]
  edge [
    source 7
    target 32
  ]
  edge [
    source 7
    target 33
  ]
  edge [
    source 7
    target 34
  ]
  edge [
    source 7
    target 35
  ]
  edge [
    source 7
    target 36
  ]
  edge [
    source 7
    target 11
  ]
  edge [
    source 7
    target 37
  ]
  edge [
    source 7
    target 38
  ]
  edge [
    source 7
    target 39
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 8
    target 46
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 73
    target 74
  ]
]
