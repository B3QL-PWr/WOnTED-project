graph [
  maxDegree 17
  minDegree 1
  meanDegree 7.384615384615385
  density 0.19433198380566802
  graphCliqueNumber 14
  node [
    id 0
    label "dziennik"
    origin "text"
  ]
  node [
    id 1
    label "ustawa"
    origin "text"
  ]
  node [
    id 2
    label "kwiecie&#324;"
    origin "text"
  ]
  node [
    id 3
    label "poz"
    origin "text"
  ]
  node [
    id 4
    label "spis"
  ]
  node [
    id 5
    label "sheet"
  ]
  node [
    id 6
    label "gazeta"
  ]
  node [
    id 7
    label "diariusz"
  ]
  node [
    id 8
    label "pami&#281;tnik"
  ]
  node [
    id 9
    label "journal"
  ]
  node [
    id 10
    label "ksi&#281;ga"
  ]
  node [
    id 11
    label "program_informacyjny"
  ]
  node [
    id 12
    label "Karta_Nauczyciela"
  ]
  node [
    id 13
    label "marc&#243;wka"
  ]
  node [
    id 14
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 15
    label "akt"
  ]
  node [
    id 16
    label "przej&#347;&#263;"
  ]
  node [
    id 17
    label "charter"
  ]
  node [
    id 18
    label "przej&#347;cie"
  ]
  node [
    id 19
    label "miesi&#261;c"
  ]
  node [
    id 20
    label "ustawi&#263;"
  ]
  node [
    id 21
    label "rzeczpospolita"
  ]
  node [
    id 22
    label "polski"
  ]
  node [
    id 23
    label "republika"
  ]
  node [
    id 24
    label "federalny"
  ]
  node [
    id 25
    label "niemiec"
  ]
  node [
    id 26
    label "prezydent"
  ]
  node [
    id 27
    label "umowy"
  ]
  node [
    id 28
    label "mi&#281;dzy"
  ]
  node [
    id 29
    label "rz&#261;d"
  ]
  node [
    id 30
    label "albo"
  ]
  node [
    id 31
    label "ojciec"
  ]
  node [
    id 32
    label "wsp&#243;&#322;praca"
  ]
  node [
    id 33
    label "kulturalny"
  ]
  node [
    id 34
    label "traktat"
  ]
  node [
    id 35
    label "dobry"
  ]
  node [
    id 36
    label "s&#261;siedztwo"
  ]
  node [
    id 37
    label "i"
  ]
  node [
    id 38
    label "przyjazny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 26
  ]
  edge [
    source 21
    target 27
  ]
  edge [
    source 21
    target 28
  ]
  edge [
    source 21
    target 29
  ]
  edge [
    source 21
    target 30
  ]
  edge [
    source 21
    target 23
  ]
  edge [
    source 21
    target 24
  ]
  edge [
    source 21
    target 25
  ]
  edge [
    source 21
    target 31
  ]
  edge [
    source 21
    target 32
  ]
  edge [
    source 21
    target 33
  ]
  edge [
    source 21
    target 34
  ]
  edge [
    source 21
    target 35
  ]
  edge [
    source 21
    target 36
  ]
  edge [
    source 21
    target 37
  ]
  edge [
    source 21
    target 38
  ]
  edge [
    source 22
    target 26
  ]
  edge [
    source 22
    target 27
  ]
  edge [
    source 22
    target 28
  ]
  edge [
    source 22
    target 29
  ]
  edge [
    source 22
    target 30
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 24
  ]
  edge [
    source 22
    target 25
  ]
  edge [
    source 22
    target 31
  ]
  edge [
    source 22
    target 32
  ]
  edge [
    source 22
    target 33
  ]
  edge [
    source 22
    target 34
  ]
  edge [
    source 22
    target 35
  ]
  edge [
    source 22
    target 36
  ]
  edge [
    source 22
    target 37
  ]
  edge [
    source 22
    target 38
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 25
  ]
  edge [
    source 23
    target 27
  ]
  edge [
    source 23
    target 28
  ]
  edge [
    source 23
    target 29
  ]
  edge [
    source 23
    target 30
  ]
  edge [
    source 23
    target 31
  ]
  edge [
    source 23
    target 32
  ]
  edge [
    source 23
    target 33
  ]
  edge [
    source 23
    target 34
  ]
  edge [
    source 23
    target 35
  ]
  edge [
    source 23
    target 36
  ]
  edge [
    source 23
    target 37
  ]
  edge [
    source 23
    target 38
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 27
  ]
  edge [
    source 24
    target 28
  ]
  edge [
    source 24
    target 29
  ]
  edge [
    source 24
    target 30
  ]
  edge [
    source 24
    target 31
  ]
  edge [
    source 24
    target 32
  ]
  edge [
    source 24
    target 33
  ]
  edge [
    source 24
    target 34
  ]
  edge [
    source 24
    target 35
  ]
  edge [
    source 24
    target 36
  ]
  edge [
    source 24
    target 37
  ]
  edge [
    source 24
    target 38
  ]
  edge [
    source 25
    target 27
  ]
  edge [
    source 25
    target 28
  ]
  edge [
    source 25
    target 29
  ]
  edge [
    source 25
    target 30
  ]
  edge [
    source 25
    target 31
  ]
  edge [
    source 25
    target 32
  ]
  edge [
    source 25
    target 33
  ]
  edge [
    source 25
    target 34
  ]
  edge [
    source 25
    target 35
  ]
  edge [
    source 25
    target 36
  ]
  edge [
    source 25
    target 37
  ]
  edge [
    source 25
    target 38
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 29
  ]
  edge [
    source 27
    target 30
  ]
  edge [
    source 27
    target 31
  ]
  edge [
    source 27
    target 32
  ]
  edge [
    source 27
    target 33
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 30
  ]
  edge [
    source 28
    target 31
  ]
  edge [
    source 28
    target 32
  ]
  edge [
    source 28
    target 33
  ]
  edge [
    source 28
    target 34
  ]
  edge [
    source 28
    target 35
  ]
  edge [
    source 28
    target 36
  ]
  edge [
    source 28
    target 37
  ]
  edge [
    source 28
    target 38
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 29
  ]
  edge [
    source 29
    target 31
  ]
  edge [
    source 29
    target 32
  ]
  edge [
    source 29
    target 33
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 32
  ]
  edge [
    source 30
    target 33
  ]
  edge [
    source 30
    target 34
  ]
  edge [
    source 30
    target 35
  ]
  edge [
    source 30
    target 36
  ]
  edge [
    source 30
    target 37
  ]
  edge [
    source 30
    target 38
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 33
  ]
  edge [
    source 31
    target 34
  ]
  edge [
    source 31
    target 35
  ]
  edge [
    source 31
    target 36
  ]
  edge [
    source 31
    target 37
  ]
  edge [
    source 31
    target 38
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 34
  ]
  edge [
    source 32
    target 35
  ]
  edge [
    source 32
    target 36
  ]
  edge [
    source 32
    target 37
  ]
  edge [
    source 32
    target 38
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 36
  ]
  edge [
    source 34
    target 37
  ]
  edge [
    source 34
    target 38
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 37
  ]
  edge [
    source 35
    target 38
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 38
  ]
  edge [
    source 37
    target 38
  ]
]
