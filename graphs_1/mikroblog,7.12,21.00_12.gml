graph [
  maxDegree 15
  minDegree 1
  meanDegree 1.9636363636363636
  density 0.03636363636363636
  graphCliqueNumber 2
  node [
    id 0
    label "ile"
    origin "text"
  ]
  node [
    id 1
    label "zak&#322;ad"
    origin "text"
  ]
  node [
    id 2
    label "ten"
    origin "text"
  ]
  node [
    id 3
    label "jegomo&#347;&#263;"
    origin "text"
  ]
  node [
    id 4
    label "by&#263;"
    origin "text"
  ]
  node [
    id 5
    label "aktywny"
    origin "text"
  ]
  node [
    id 6
    label "pod"
    origin "text"
  ]
  node [
    id 7
    label "tag"
    origin "text"
  ]
  node [
    id 8
    label "przegryw"
    origin "text"
  ]
  node [
    id 9
    label "czyn"
  ]
  node [
    id 10
    label "company"
  ]
  node [
    id 11
    label "zak&#322;adka"
  ]
  node [
    id 12
    label "firma"
  ]
  node [
    id 13
    label "instytut"
  ]
  node [
    id 14
    label "wyko&#324;czenie"
  ]
  node [
    id 15
    label "jednostka_organizacyjna"
  ]
  node [
    id 16
    label "umowa"
  ]
  node [
    id 17
    label "instytucja"
  ]
  node [
    id 18
    label "miejsce_pracy"
  ]
  node [
    id 19
    label "okre&#347;lony"
  ]
  node [
    id 20
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 21
    label "mo&#347;&#263;"
  ]
  node [
    id 22
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 23
    label "si&#281;ga&#263;"
  ]
  node [
    id 24
    label "trwa&#263;"
  ]
  node [
    id 25
    label "obecno&#347;&#263;"
  ]
  node [
    id 26
    label "stan"
  ]
  node [
    id 27
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 28
    label "stand"
  ]
  node [
    id 29
    label "mie&#263;_miejsce"
  ]
  node [
    id 30
    label "uczestniczy&#263;"
  ]
  node [
    id 31
    label "chodzi&#263;"
  ]
  node [
    id 32
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 33
    label "equal"
  ]
  node [
    id 34
    label "czynny"
  ]
  node [
    id 35
    label "uczynnienie"
  ]
  node [
    id 36
    label "dzia&#322;anie"
  ]
  node [
    id 37
    label "czynnie"
  ]
  node [
    id 38
    label "realny"
  ]
  node [
    id 39
    label "aktywnie"
  ]
  node [
    id 40
    label "wa&#380;ny"
  ]
  node [
    id 41
    label "ciekawy"
  ]
  node [
    id 42
    label "intensywny"
  ]
  node [
    id 43
    label "faktyczny"
  ]
  node [
    id 44
    label "istotny"
  ]
  node [
    id 45
    label "zdolny"
  ]
  node [
    id 46
    label "uczynnianie"
  ]
  node [
    id 47
    label "identyfikator"
  ]
  node [
    id 48
    label "napis"
  ]
  node [
    id 49
    label "nerd"
  ]
  node [
    id 50
    label "znacznik"
  ]
  node [
    id 51
    label "komnatowy"
  ]
  node [
    id 52
    label "sport_elektroniczny"
  ]
  node [
    id 53
    label "nieudacznik"
  ]
  node [
    id 54
    label "przegraniec"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
]
