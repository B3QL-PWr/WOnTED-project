graph [
  maxDegree 26
  minDegree 1
  meanDegree 1.974025974025974
  density 0.025974025974025976
  graphCliqueNumber 2
  node [
    id 0
    label "oto"
    origin "text"
  ]
  node [
    id 1
    label "chi&#324;ski"
    origin "text"
  ]
  node [
    id 2
    label "bajka"
    origin "text"
  ]
  node [
    id 3
    label "robi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 5
    label "kitajski"
  ]
  node [
    id 6
    label "j&#281;zyk"
  ]
  node [
    id 7
    label "kaczka_po_peki&#324;sku"
  ]
  node [
    id 8
    label "dziwaczny"
  ]
  node [
    id 9
    label "tandetny"
  ]
  node [
    id 10
    label "makroj&#281;zyk"
  ]
  node [
    id 11
    label "chi&#324;sko"
  ]
  node [
    id 12
    label "po_chi&#324;sku"
  ]
  node [
    id 13
    label "j&#281;zyk_chi&#324;sko-tybeta&#324;ski"
  ]
  node [
    id 14
    label "azjatycki"
  ]
  node [
    id 15
    label "lipny"
  ]
  node [
    id 16
    label "go"
  ]
  node [
    id 17
    label "niedrogi"
  ]
  node [
    id 18
    label "dalekowschodni"
  ]
  node [
    id 19
    label "apolog"
  ]
  node [
    id 20
    label "morfing"
  ]
  node [
    id 21
    label "film"
  ]
  node [
    id 22
    label "mora&#322;"
  ]
  node [
    id 23
    label "opowie&#347;&#263;"
  ]
  node [
    id 24
    label "narrative"
  ]
  node [
    id 25
    label "g&#322;upstwo"
  ]
  node [
    id 26
    label "utw&#243;r"
  ]
  node [
    id 27
    label "Pok&#233;mon"
  ]
  node [
    id 28
    label "epika"
  ]
  node [
    id 29
    label "komfort"
  ]
  node [
    id 30
    label "sytuacja"
  ]
  node [
    id 31
    label "tentegowa&#263;"
  ]
  node [
    id 32
    label "urz&#261;dza&#263;"
  ]
  node [
    id 33
    label "give"
  ]
  node [
    id 34
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 35
    label "czyni&#263;"
  ]
  node [
    id 36
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 37
    label "post&#281;powa&#263;"
  ]
  node [
    id 38
    label "wydala&#263;"
  ]
  node [
    id 39
    label "oszukiwa&#263;"
  ]
  node [
    id 40
    label "organizowa&#263;"
  ]
  node [
    id 41
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 42
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 43
    label "work"
  ]
  node [
    id 44
    label "przerabia&#263;"
  ]
  node [
    id 45
    label "stylizowa&#263;"
  ]
  node [
    id 46
    label "falowa&#263;"
  ]
  node [
    id 47
    label "act"
  ]
  node [
    id 48
    label "peddle"
  ]
  node [
    id 49
    label "ukazywa&#263;"
  ]
  node [
    id 50
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 51
    label "praca"
  ]
  node [
    id 52
    label "asymilowa&#263;"
  ]
  node [
    id 53
    label "wapniak"
  ]
  node [
    id 54
    label "dwun&#243;g"
  ]
  node [
    id 55
    label "polifag"
  ]
  node [
    id 56
    label "wz&#243;r"
  ]
  node [
    id 57
    label "profanum"
  ]
  node [
    id 58
    label "hominid"
  ]
  node [
    id 59
    label "homo_sapiens"
  ]
  node [
    id 60
    label "nasada"
  ]
  node [
    id 61
    label "podw&#322;adny"
  ]
  node [
    id 62
    label "ludzko&#347;&#263;"
  ]
  node [
    id 63
    label "os&#322;abianie"
  ]
  node [
    id 64
    label "mikrokosmos"
  ]
  node [
    id 65
    label "portrecista"
  ]
  node [
    id 66
    label "duch"
  ]
  node [
    id 67
    label "oddzia&#322;ywanie"
  ]
  node [
    id 68
    label "g&#322;owa"
  ]
  node [
    id 69
    label "asymilowanie"
  ]
  node [
    id 70
    label "osoba"
  ]
  node [
    id 71
    label "os&#322;abia&#263;"
  ]
  node [
    id 72
    label "figura"
  ]
  node [
    id 73
    label "Adam"
  ]
  node [
    id 74
    label "senior"
  ]
  node [
    id 75
    label "antropochoria"
  ]
  node [
    id 76
    label "posta&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
]
