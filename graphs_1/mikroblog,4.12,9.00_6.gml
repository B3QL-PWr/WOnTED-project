graph [
  maxDegree 26
  minDegree 1
  meanDegree 1.9710144927536233
  density 0.014386967100391411
  graphCliqueNumber 3
  node [
    id 0
    label "programowa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "elektronik"
    origin "text"
  ]
  node [
    id 2
    label "elektronikadiy"
    origin "text"
  ]
  node [
    id 3
    label "telefon"
    origin "text"
  ]
  node [
    id 4
    label "symbian"
    origin "text"
  ]
  node [
    id 5
    label "cze&#347;&#263;"
    origin "text"
  ]
  node [
    id 6
    label "zainteresowanie"
    origin "text"
  ]
  node [
    id 7
    label "przekroczy&#263;"
    origin "text"
  ]
  node [
    id 8
    label "moja"
    origin "text"
  ]
  node [
    id 9
    label "&#347;mia&#322;y"
    origin "text"
  ]
  node [
    id 10
    label "oczekiwanie"
    origin "text"
  ]
  node [
    id 11
    label "ponad"
    origin "text"
  ]
  node [
    id 12
    label "plus"
    origin "text"
  ]
  node [
    id 13
    label "praca"
    origin "text"
  ]
  node [
    id 14
    label "nad"
    origin "text"
  ]
  node [
    id 15
    label "taki"
    origin "text"
  ]
  node [
    id 16
    label "szrot"
    origin "text"
  ]
  node [
    id 17
    label "jaki&#347;"
    origin "text"
  ]
  node [
    id 18
    label "masakra"
    origin "text"
  ]
  node [
    id 19
    label "my&#347;le&#263;"
  ]
  node [
    id 20
    label "project"
  ]
  node [
    id 21
    label "in&#380;ynier"
  ]
  node [
    id 22
    label "technik"
  ]
  node [
    id 23
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 24
    label "s&#322;uchawka_telefoniczna"
  ]
  node [
    id 25
    label "coalescence"
  ]
  node [
    id 26
    label "roz&#322;&#261;cza&#263;"
  ]
  node [
    id 27
    label "phreaker"
  ]
  node [
    id 28
    label "infrastruktura"
  ]
  node [
    id 29
    label "wy&#347;wietlacz"
  ]
  node [
    id 30
    label "provider"
  ]
  node [
    id 31
    label "dzwonienie"
  ]
  node [
    id 32
    label "zadzwoni&#263;"
  ]
  node [
    id 33
    label "dzwoni&#263;"
  ]
  node [
    id 34
    label "kontakt"
  ]
  node [
    id 35
    label "mikrotelefon"
  ]
  node [
    id 36
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 37
    label "po&#322;&#261;czenie"
  ]
  node [
    id 38
    label "numer"
  ]
  node [
    id 39
    label "roz&#322;&#261;czanie"
  ]
  node [
    id 40
    label "instalacja"
  ]
  node [
    id 41
    label "billing"
  ]
  node [
    id 42
    label "urz&#261;dzenie"
  ]
  node [
    id 43
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 44
    label "zaimponowanie"
  ]
  node [
    id 45
    label "szanowa&#263;"
  ]
  node [
    id 46
    label "uhonorowa&#263;"
  ]
  node [
    id 47
    label "honorowanie"
  ]
  node [
    id 48
    label "uszanowa&#263;"
  ]
  node [
    id 49
    label "rewerencja"
  ]
  node [
    id 50
    label "uszanowanie"
  ]
  node [
    id 51
    label "imponowanie"
  ]
  node [
    id 52
    label "dobro"
  ]
  node [
    id 53
    label "uhonorowanie"
  ]
  node [
    id 54
    label "respect"
  ]
  node [
    id 55
    label "honorowa&#263;"
  ]
  node [
    id 56
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 57
    label "szacuneczek"
  ]
  node [
    id 58
    label "postawa"
  ]
  node [
    id 59
    label "ciekawo&#347;&#263;"
  ]
  node [
    id 60
    label "care"
  ]
  node [
    id 61
    label "emocja"
  ]
  node [
    id 62
    label "zainteresowanie_si&#281;"
  ]
  node [
    id 63
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 64
    label "love"
  ]
  node [
    id 65
    label "wzbudzenie"
  ]
  node [
    id 66
    label "open"
  ]
  node [
    id 67
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 68
    label "min&#261;&#263;"
  ]
  node [
    id 69
    label "cut"
  ]
  node [
    id 70
    label "ograniczenie"
  ]
  node [
    id 71
    label "zrobi&#263;"
  ]
  node [
    id 72
    label "przeby&#263;"
  ]
  node [
    id 73
    label "pique"
  ]
  node [
    id 74
    label "&#347;mia&#322;o"
  ]
  node [
    id 75
    label "dziarski"
  ]
  node [
    id 76
    label "odwa&#380;ny"
  ]
  node [
    id 77
    label "spodziewanie_si&#281;"
  ]
  node [
    id 78
    label "anticipation"
  ]
  node [
    id 79
    label "wytrzymywanie"
  ]
  node [
    id 80
    label "czekanie"
  ]
  node [
    id 81
    label "wait"
  ]
  node [
    id 82
    label "spotykanie"
  ]
  node [
    id 83
    label "przewidywanie"
  ]
  node [
    id 84
    label "wytrzymanie"
  ]
  node [
    id 85
    label "warto&#347;&#263;"
  ]
  node [
    id 86
    label "wabik"
  ]
  node [
    id 87
    label "rewaluowa&#263;"
  ]
  node [
    id 88
    label "korzy&#347;&#263;"
  ]
  node [
    id 89
    label "dodawanie"
  ]
  node [
    id 90
    label "rewaluowanie"
  ]
  node [
    id 91
    label "stopie&#324;"
  ]
  node [
    id 92
    label "ocena"
  ]
  node [
    id 93
    label "zrewaluowa&#263;"
  ]
  node [
    id 94
    label "liczba"
  ]
  node [
    id 95
    label "znak_matematyczny"
  ]
  node [
    id 96
    label "strona"
  ]
  node [
    id 97
    label "zrewaluowanie"
  ]
  node [
    id 98
    label "stosunek_pracy"
  ]
  node [
    id 99
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 100
    label "benedykty&#324;ski"
  ]
  node [
    id 101
    label "pracowanie"
  ]
  node [
    id 102
    label "zaw&#243;d"
  ]
  node [
    id 103
    label "kierownictwo"
  ]
  node [
    id 104
    label "zmiana"
  ]
  node [
    id 105
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 106
    label "wytw&#243;r"
  ]
  node [
    id 107
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 108
    label "tynkarski"
  ]
  node [
    id 109
    label "czynnik_produkcji"
  ]
  node [
    id 110
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 111
    label "zobowi&#261;zanie"
  ]
  node [
    id 112
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 113
    label "czynno&#347;&#263;"
  ]
  node [
    id 114
    label "tyrka"
  ]
  node [
    id 115
    label "pracowa&#263;"
  ]
  node [
    id 116
    label "siedziba"
  ]
  node [
    id 117
    label "poda&#380;_pracy"
  ]
  node [
    id 118
    label "miejsce"
  ]
  node [
    id 119
    label "zak&#322;ad"
  ]
  node [
    id 120
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 121
    label "najem"
  ]
  node [
    id 122
    label "okre&#347;lony"
  ]
  node [
    id 123
    label "z&#322;omowisko"
  ]
  node [
    id 124
    label "jako&#347;"
  ]
  node [
    id 125
    label "charakterystyczny"
  ]
  node [
    id 126
    label "ciekawy"
  ]
  node [
    id 127
    label "jako_tako"
  ]
  node [
    id 128
    label "dziwny"
  ]
  node [
    id 129
    label "niez&#322;y"
  ]
  node [
    id 130
    label "przyzwoity"
  ]
  node [
    id 131
    label "zbrodnia"
  ]
  node [
    id 132
    label "genocide"
  ]
  node [
    id 133
    label "zag&#322;ada"
  ]
  node [
    id 134
    label "Siemens"
  ]
  node [
    id 135
    label "SX1"
  ]
  node [
    id 136
    label "Debian"
  ]
  node [
    id 137
    label "Armel"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 85
  ]
  edge [
    source 12
    target 86
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 12
    target 89
  ]
  edge [
    source 12
    target 90
  ]
  edge [
    source 12
    target 91
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 12
    target 94
  ]
  edge [
    source 12
    target 95
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 98
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 13
    target 101
  ]
  edge [
    source 13
    target 102
  ]
  edge [
    source 13
    target 103
  ]
  edge [
    source 13
    target 104
  ]
  edge [
    source 13
    target 105
  ]
  edge [
    source 13
    target 106
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 13
    target 113
  ]
  edge [
    source 13
    target 114
  ]
  edge [
    source 13
    target 115
  ]
  edge [
    source 13
    target 116
  ]
  edge [
    source 13
    target 117
  ]
  edge [
    source 13
    target 118
  ]
  edge [
    source 13
    target 119
  ]
  edge [
    source 13
    target 120
  ]
  edge [
    source 13
    target 121
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 122
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 123
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 124
  ]
  edge [
    source 17
    target 125
  ]
  edge [
    source 17
    target 126
  ]
  edge [
    source 17
    target 127
  ]
  edge [
    source 17
    target 128
  ]
  edge [
    source 17
    target 129
  ]
  edge [
    source 17
    target 130
  ]
  edge [
    source 18
    target 131
  ]
  edge [
    source 18
    target 132
  ]
  edge [
    source 18
    target 133
  ]
  edge [
    source 134
    target 135
  ]
  edge [
    source 136
    target 137
  ]
]
