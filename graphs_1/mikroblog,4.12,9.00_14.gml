graph [
  maxDegree 2
  minDegree 1
  meanDegree 1.3333333333333333
  density 0.6666666666666666
  graphCliqueNumber 2
  node [
    id 0
    label "dobranoc"
    origin "text"
  ]
  node [
    id 1
    label "gownowpis"
    origin "text"
  ]
  node [
    id 2
    label "pro&#347;ba"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
]
