graph [
  maxDegree 24
  minDegree 1
  meanDegree 2.0163934426229506
  density 0.016664408616718602
  graphCliqueNumber 3
  node [
    id 0
    label "wniosek"
    origin "text"
  ]
  node [
    id 1
    label "organizacja"
    origin "text"
  ]
  node [
    id 2
    label "pozarz&#261;dowy"
    origin "text"
  ]
  node [
    id 3
    label "sprawa"
    origin "text"
  ]
  node [
    id 4
    label "pozyskanie"
    origin "text"
  ]
  node [
    id 5
    label "najem"
    origin "text"
  ]
  node [
    id 6
    label "lokal"
    origin "text"
  ]
  node [
    id 7
    label "u&#380;ytkowy"
    origin "text"
  ]
  node [
    id 8
    label "zasoby"
    origin "text"
  ]
  node [
    id 9
    label "gmin"
    origin "text"
  ]
  node [
    id 10
    label "wroc&#322;aw"
    origin "text"
  ]
  node [
    id 11
    label "tryb"
    origin "text"
  ]
  node [
    id 12
    label "bezprzetargowy"
    origin "text"
  ]
  node [
    id 13
    label "przyjmowa&#263;"
    origin "text"
  ]
  node [
    id 14
    label "opiniowa&#263;"
    origin "text"
  ]
  node [
    id 15
    label "biuro"
    origin "text"
  ]
  node [
    id 16
    label "wsp&#243;&#322;praca"
    origin "text"
  ]
  node [
    id 17
    label "bwo"
    origin "text"
  ]
  node [
    id 18
    label "twierdzenie"
  ]
  node [
    id 19
    label "my&#347;l"
  ]
  node [
    id 20
    label "wnioskowanie"
  ]
  node [
    id 21
    label "propozycja"
  ]
  node [
    id 22
    label "motion"
  ]
  node [
    id 23
    label "pismo"
  ]
  node [
    id 24
    label "prayer"
  ]
  node [
    id 25
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 26
    label "endecki"
  ]
  node [
    id 27
    label "komitet_koordynacyjny"
  ]
  node [
    id 28
    label "przybud&#243;wka"
  ]
  node [
    id 29
    label "ZOMO"
  ]
  node [
    id 30
    label "podmiot"
  ]
  node [
    id 31
    label "boj&#243;wka"
  ]
  node [
    id 32
    label "zesp&#243;&#322;"
  ]
  node [
    id 33
    label "organization"
  ]
  node [
    id 34
    label "TOPR"
  ]
  node [
    id 35
    label "jednostka_organizacyjna"
  ]
  node [
    id 36
    label "przedstawicielstwo"
  ]
  node [
    id 37
    label "Cepelia"
  ]
  node [
    id 38
    label "GOPR"
  ]
  node [
    id 39
    label "ZMP"
  ]
  node [
    id 40
    label "ZBoWiD"
  ]
  node [
    id 41
    label "struktura"
  ]
  node [
    id 42
    label "od&#322;am"
  ]
  node [
    id 43
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 44
    label "centrala"
  ]
  node [
    id 45
    label "pozainstytucjonalny"
  ]
  node [
    id 46
    label "pozarz&#261;dowo"
  ]
  node [
    id 47
    label "temat"
  ]
  node [
    id 48
    label "kognicja"
  ]
  node [
    id 49
    label "idea"
  ]
  node [
    id 50
    label "szczeg&#243;&#322;"
  ]
  node [
    id 51
    label "rzecz"
  ]
  node [
    id 52
    label "wydarzenie"
  ]
  node [
    id 53
    label "przes&#322;anka"
  ]
  node [
    id 54
    label "rozprawa"
  ]
  node [
    id 55
    label "object"
  ]
  node [
    id 56
    label "proposition"
  ]
  node [
    id 57
    label "return"
  ]
  node [
    id 58
    label "obtainment"
  ]
  node [
    id 59
    label "wykonanie"
  ]
  node [
    id 60
    label "uzyskanie"
  ]
  node [
    id 61
    label "bycie_w_posiadaniu"
  ]
  node [
    id 62
    label "umowa"
  ]
  node [
    id 63
    label "praca"
  ]
  node [
    id 64
    label "transakcja"
  ]
  node [
    id 65
    label "gastronomia"
  ]
  node [
    id 66
    label "miejsce"
  ]
  node [
    id 67
    label "zak&#322;ad"
  ]
  node [
    id 68
    label "u&#380;ytkowo"
  ]
  node [
    id 69
    label "podmiot_gospodarczy"
  ]
  node [
    id 70
    label "z&#322;o&#380;e"
  ]
  node [
    id 71
    label "zasoby_kopalin"
  ]
  node [
    id 72
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 73
    label "stan_trzeci"
  ]
  node [
    id 74
    label "stan"
  ]
  node [
    id 75
    label "gminno&#347;&#263;"
  ]
  node [
    id 76
    label "funkcjonowa&#263;"
  ]
  node [
    id 77
    label "kategoria_gramatyczna"
  ]
  node [
    id 78
    label "skala"
  ]
  node [
    id 79
    label "cecha"
  ]
  node [
    id 80
    label "ko&#322;o_z&#281;bate"
  ]
  node [
    id 81
    label "z&#261;b"
  ]
  node [
    id 82
    label "modalno&#347;&#263;"
  ]
  node [
    id 83
    label "koniugacja"
  ]
  node [
    id 84
    label "ko&#322;o"
  ]
  node [
    id 85
    label "spos&#243;b"
  ]
  node [
    id 86
    label "bezprzetargowo"
  ]
  node [
    id 87
    label "zobowi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 88
    label "poch&#322;ania&#263;"
  ]
  node [
    id 89
    label "dostarcza&#263;"
  ]
  node [
    id 90
    label "umieszcza&#263;"
  ]
  node [
    id 91
    label "uznawa&#263;"
  ]
  node [
    id 92
    label "swallow"
  ]
  node [
    id 93
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 94
    label "admit"
  ]
  node [
    id 95
    label "fall"
  ]
  node [
    id 96
    label "undertake"
  ]
  node [
    id 97
    label "dopuszcza&#263;"
  ]
  node [
    id 98
    label "wyprawia&#263;"
  ]
  node [
    id 99
    label "robi&#263;"
  ]
  node [
    id 100
    label "wpuszcza&#263;"
  ]
  node [
    id 101
    label "close"
  ]
  node [
    id 102
    label "przyjmowanie"
  ]
  node [
    id 103
    label "obiera&#263;"
  ]
  node [
    id 104
    label "pracowa&#263;"
  ]
  node [
    id 105
    label "bra&#263;"
  ]
  node [
    id 106
    label "zgadza&#263;_si&#281;"
  ]
  node [
    id 107
    label "odbiera&#263;"
  ]
  node [
    id 108
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 109
    label "stwierdza&#263;"
  ]
  node [
    id 110
    label "os&#261;dza&#263;"
  ]
  node [
    id 111
    label "s&#261;d"
  ]
  node [
    id 112
    label "biurko"
  ]
  node [
    id 113
    label "palestra"
  ]
  node [
    id 114
    label "pomieszczenie"
  ]
  node [
    id 115
    label "agency"
  ]
  node [
    id 116
    label "Biuro_Lustracyjne"
  ]
  node [
    id 117
    label "instytucja"
  ]
  node [
    id 118
    label "board"
  ]
  node [
    id 119
    label "dzia&#322;"
  ]
  node [
    id 120
    label "boks"
  ]
  node [
    id 121
    label "wsp&#243;&#322;pracownictwo"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 76
  ]
  edge [
    source 11
    target 77
  ]
  edge [
    source 11
    target 78
  ]
  edge [
    source 11
    target 79
  ]
  edge [
    source 11
    target 80
  ]
  edge [
    source 11
    target 81
  ]
  edge [
    source 11
    target 82
  ]
  edge [
    source 11
    target 83
  ]
  edge [
    source 11
    target 84
  ]
  edge [
    source 11
    target 85
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 86
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 87
  ]
  edge [
    source 13
    target 88
  ]
  edge [
    source 13
    target 89
  ]
  edge [
    source 13
    target 90
  ]
  edge [
    source 13
    target 91
  ]
  edge [
    source 13
    target 92
  ]
  edge [
    source 13
    target 93
  ]
  edge [
    source 13
    target 94
  ]
  edge [
    source 13
    target 95
  ]
  edge [
    source 13
    target 96
  ]
  edge [
    source 13
    target 97
  ]
  edge [
    source 13
    target 98
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 13
    target 101
  ]
  edge [
    source 13
    target 102
  ]
  edge [
    source 13
    target 103
  ]
  edge [
    source 13
    target 104
  ]
  edge [
    source 13
    target 105
  ]
  edge [
    source 13
    target 106
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 109
  ]
  edge [
    source 14
    target 110
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 111
  ]
  edge [
    source 15
    target 112
  ]
  edge [
    source 15
    target 113
  ]
  edge [
    source 15
    target 114
  ]
  edge [
    source 15
    target 115
  ]
  edge [
    source 15
    target 116
  ]
  edge [
    source 15
    target 117
  ]
  edge [
    source 15
    target 118
  ]
  edge [
    source 15
    target 119
  ]
  edge [
    source 15
    target 120
  ]
  edge [
    source 16
    target 121
  ]
  edge [
    source 16
    target 25
  ]
]
