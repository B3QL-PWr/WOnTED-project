graph [
  maxDegree 28
  minDegree 1
  meanDegree 2.040201005025126
  density 0.010304045479924877
  graphCliqueNumber 3
  node [
    id 0
    label "stoisko"
    origin "text"
  ]
  node [
    id 1
    label "rybny"
    origin "text"
  ]
  node [
    id 2
    label "zajmowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "hiszpa&#324;ski"
    origin "text"
  ]
  node [
    id 4
    label "sklep"
    origin "text"
  ]
  node [
    id 5
    label "hala"
    origin "text"
  ]
  node [
    id 6
    label "targowy"
    origin "text"
  ]
  node [
    id 7
    label "powierzchnia"
    origin "text"
  ]
  node [
    id 8
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 9
    label "nawet"
    origin "text"
  ]
  node [
    id 10
    label "ni&#380;"
    origin "text"
  ]
  node [
    id 11
    label "wszechobecny"
    origin "text"
  ]
  node [
    id 12
    label "przed"
    origin "text"
  ]
  node [
    id 13
    label "&#347;wi&#281;to"
    origin "text"
  ]
  node [
    id 14
    label "szynk"
    origin "text"
  ]
  node [
    id 15
    label "tychy"
    origin "text"
  ]
  node [
    id 16
    label "zreszt&#261;"
    origin "text"
  ]
  node [
    id 17
    label "zawsze"
    origin "text"
  ]
  node [
    id 18
    label "pe&#322;no"
    origin "text"
  ]
  node [
    id 19
    label "turrony"
    origin "text"
  ]
  node [
    id 20
    label "bez"
    origin "text"
  ]
  node [
    id 21
    label "wikipedii"
    origin "text"
  ]
  node [
    id 22
    label "ci&#281;&#380;ko"
    origin "text"
  ]
  node [
    id 23
    label "by&#263;by"
    origin "text"
  ]
  node [
    id 24
    label "si&#281;"
    origin "text"
  ]
  node [
    id 25
    label "zorientowa&#263;"
    origin "text"
  ]
  node [
    id 26
    label "te&#380;"
    origin "text"
  ]
  node [
    id 27
    label "taki"
    origin "text"
  ]
  node [
    id 28
    label "w&#322;a&#347;nie"
    origin "text"
  ]
  node [
    id 29
    label "wrzuci&#263;by&#263;"
    origin "text"
  ]
  node [
    id 30
    label "patelnia"
    origin "text"
  ]
  node [
    id 31
    label "kolejny"
    origin "text"
  ]
  node [
    id 32
    label "subiektywny"
    origin "text"
  ]
  node [
    id 33
    label "przegl&#261;d"
    origin "text"
  ]
  node [
    id 34
    label "obiekt_handlowy"
  ]
  node [
    id 35
    label "swoisty"
  ]
  node [
    id 36
    label "obfituj&#261;cy"
  ]
  node [
    id 37
    label "mi&#281;sny"
  ]
  node [
    id 38
    label "charakterystyczny"
  ]
  node [
    id 39
    label "rybio"
  ]
  node [
    id 40
    label "return"
  ]
  node [
    id 41
    label "dostarcza&#263;"
  ]
  node [
    id 42
    label "anektowa&#263;"
  ]
  node [
    id 43
    label "trwa&#263;"
  ]
  node [
    id 44
    label "pali&#263;_si&#281;"
  ]
  node [
    id 45
    label "lokowa&#263;_si&#281;"
  ]
  node [
    id 46
    label "korzysta&#263;"
  ]
  node [
    id 47
    label "fill"
  ]
  node [
    id 48
    label "aim"
  ]
  node [
    id 49
    label "rozciekawia&#263;"
  ]
  node [
    id 50
    label "zadawa&#263;"
  ]
  node [
    id 51
    label "robi&#263;"
  ]
  node [
    id 52
    label "do"
  ]
  node [
    id 53
    label "klasyfikacja"
  ]
  node [
    id 54
    label "rozprzestrzenia&#263;_si&#281;"
  ]
  node [
    id 55
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 56
    label "bra&#263;"
  ]
  node [
    id 57
    label "obejmowa&#263;"
  ]
  node [
    id 58
    label "sake"
  ]
  node [
    id 59
    label "sytuowa&#263;_si&#281;"
  ]
  node [
    id 60
    label "schorzenie"
  ]
  node [
    id 61
    label "zabiera&#263;"
  ]
  node [
    id 62
    label "komornik"
  ]
  node [
    id 63
    label "prosecute"
  ]
  node [
    id 64
    label "topographic_point"
  ]
  node [
    id 65
    label "powodowa&#263;"
  ]
  node [
    id 66
    label "&#347;r&#243;dziemnomorski"
  ]
  node [
    id 67
    label "po_hiszpa&#324;sku"
  ]
  node [
    id 68
    label "fandango"
  ]
  node [
    id 69
    label "nami&#281;tny"
  ]
  node [
    id 70
    label "j&#281;zyk"
  ]
  node [
    id 71
    label "europejski"
  ]
  node [
    id 72
    label "hispanoj&#281;zyczny"
  ]
  node [
    id 73
    label "po&#322;udniowoeuropejski"
  ]
  node [
    id 74
    label "paso_doble"
  ]
  node [
    id 75
    label "hispanistyka"
  ]
  node [
    id 76
    label "Spanish"
  ]
  node [
    id 77
    label "j&#281;zyk_roma&#324;ski"
  ]
  node [
    id 78
    label "sarabanda"
  ]
  node [
    id 79
    label "pawana"
  ]
  node [
    id 80
    label "hiszpan"
  ]
  node [
    id 81
    label "sk&#322;ad"
  ]
  node [
    id 82
    label "firma"
  ]
  node [
    id 83
    label "lokal_u&#380;ytkowy"
  ]
  node [
    id 84
    label "witryna"
  ]
  node [
    id 85
    label "zaplecze"
  ]
  node [
    id 86
    label "p&#243;&#322;ka"
  ]
  node [
    id 87
    label "halizna"
  ]
  node [
    id 88
    label "fabryka"
  ]
  node [
    id 89
    label "huta"
  ]
  node [
    id 90
    label "pastwisko"
  ]
  node [
    id 91
    label "budynek"
  ]
  node [
    id 92
    label "pi&#281;tro"
  ]
  node [
    id 93
    label "oczyszczalnia"
  ]
  node [
    id 94
    label "kopalnia"
  ]
  node [
    id 95
    label "lotnisko"
  ]
  node [
    id 96
    label "dworzec"
  ]
  node [
    id 97
    label "pomieszczenie"
  ]
  node [
    id 98
    label "capacity"
  ]
  node [
    id 99
    label "obszar"
  ]
  node [
    id 100
    label "zwierciad&#322;o"
  ]
  node [
    id 101
    label "rozmiar"
  ]
  node [
    id 102
    label "liofilowo&#347;&#263;"
  ]
  node [
    id 103
    label "poj&#281;cie"
  ]
  node [
    id 104
    label "plane"
  ]
  node [
    id 105
    label "doros&#322;y"
  ]
  node [
    id 106
    label "wiele"
  ]
  node [
    id 107
    label "dorodny"
  ]
  node [
    id 108
    label "znaczny"
  ]
  node [
    id 109
    label "du&#380;o"
  ]
  node [
    id 110
    label "prawdziwy"
  ]
  node [
    id 111
    label "niema&#322;o"
  ]
  node [
    id 112
    label "wa&#380;ny"
  ]
  node [
    id 113
    label "rozwini&#281;ty"
  ]
  node [
    id 114
    label "poziom"
  ]
  node [
    id 115
    label "faza"
  ]
  node [
    id 116
    label "depression"
  ]
  node [
    id 117
    label "zjawisko"
  ]
  node [
    id 118
    label "nizina"
  ]
  node [
    id 119
    label "wsz&#281;dobylsko"
  ]
  node [
    id 120
    label "rozleg&#322;y"
  ]
  node [
    id 121
    label "&#346;wi&#281;to_Pracy"
  ]
  node [
    id 122
    label "czas"
  ]
  node [
    id 123
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 124
    label "Nowy_Rok"
  ]
  node [
    id 125
    label "Godowe_&#346;wi&#281;to"
  ]
  node [
    id 126
    label "dzie&#324;_&#347;wi&#261;teczny"
  ]
  node [
    id 127
    label "Barb&#243;rka"
  ]
  node [
    id 128
    label "ramadan"
  ]
  node [
    id 129
    label "gastronomia"
  ]
  node [
    id 130
    label "barroom"
  ]
  node [
    id 131
    label "zak&#322;ad"
  ]
  node [
    id 132
    label "zaw&#380;dy"
  ]
  node [
    id 133
    label "ci&#261;gle"
  ]
  node [
    id 134
    label "na_zawsze"
  ]
  node [
    id 135
    label "cz&#281;sto"
  ]
  node [
    id 136
    label "pe&#322;ny"
  ]
  node [
    id 137
    label "ki&#347;&#263;"
  ]
  node [
    id 138
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 139
    label "krzew"
  ]
  node [
    id 140
    label "pi&#380;maczkowate"
  ]
  node [
    id 141
    label "pestkowiec"
  ]
  node [
    id 142
    label "kwiat"
  ]
  node [
    id 143
    label "owoc"
  ]
  node [
    id 144
    label "oliwkowate"
  ]
  node [
    id 145
    label "ro&#347;lina"
  ]
  node [
    id 146
    label "hy&#263;ka"
  ]
  node [
    id 147
    label "lilac"
  ]
  node [
    id 148
    label "delfinidyna"
  ]
  node [
    id 149
    label "trudny"
  ]
  node [
    id 150
    label "monumentalnie"
  ]
  node [
    id 151
    label "hard"
  ]
  node [
    id 152
    label "mocno"
  ]
  node [
    id 153
    label "masywnie"
  ]
  node [
    id 154
    label "wolno"
  ]
  node [
    id 155
    label "niedelikatnie"
  ]
  node [
    id 156
    label "&#378;le"
  ]
  node [
    id 157
    label "kompletnie"
  ]
  node [
    id 158
    label "dotkliwie"
  ]
  node [
    id 159
    label "przyt&#322;aczaj&#261;co"
  ]
  node [
    id 160
    label "charakterystycznie"
  ]
  node [
    id 161
    label "intensywnie"
  ]
  node [
    id 162
    label "gro&#378;nie"
  ]
  node [
    id 163
    label "heavily"
  ]
  node [
    id 164
    label "niezgrabnie"
  ]
  node [
    id 165
    label "nieudanie"
  ]
  node [
    id 166
    label "uci&#261;&#380;liwie"
  ]
  node [
    id 167
    label "ci&#281;&#380;ki"
  ]
  node [
    id 168
    label "orient"
  ]
  node [
    id 169
    label "eastern_hemisphere"
  ]
  node [
    id 170
    label "kierunek"
  ]
  node [
    id 171
    label "zwr&#243;ci&#263;"
  ]
  node [
    id 172
    label "wyznaczy&#263;"
  ]
  node [
    id 173
    label "przyczyni&#263;_si&#281;"
  ]
  node [
    id 174
    label "set"
  ]
  node [
    id 175
    label "okre&#347;lony"
  ]
  node [
    id 176
    label "jaki&#347;"
  ]
  node [
    id 177
    label "dok&#322;adnie"
  ]
  node [
    id 178
    label "r&#261;czka"
  ]
  node [
    id 179
    label "zawarto&#347;&#263;"
  ]
  node [
    id 180
    label "miejsce"
  ]
  node [
    id 181
    label "pot"
  ]
  node [
    id 182
    label "naczynie"
  ]
  node [
    id 183
    label "cunnilingus"
  ]
  node [
    id 184
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 185
    label "porcja"
  ]
  node [
    id 186
    label "inny"
  ]
  node [
    id 187
    label "nast&#281;pnie"
  ]
  node [
    id 188
    label "kt&#243;ry&#347;"
  ]
  node [
    id 189
    label "kolejno"
  ]
  node [
    id 190
    label "nastopny"
  ]
  node [
    id 191
    label "nieobiektywny"
  ]
  node [
    id 192
    label "indywidualny"
  ]
  node [
    id 193
    label "zsubiektywizowanie"
  ]
  node [
    id 194
    label "subiektywizowanie"
  ]
  node [
    id 195
    label "subiektywnie"
  ]
  node [
    id 196
    label "impreza"
  ]
  node [
    id 197
    label "kontrola"
  ]
  node [
    id 198
    label "inspection"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 119
  ]
  edge [
    source 11
    target 120
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 121
  ]
  edge [
    source 13
    target 122
  ]
  edge [
    source 13
    target 123
  ]
  edge [
    source 13
    target 124
  ]
  edge [
    source 13
    target 125
  ]
  edge [
    source 13
    target 126
  ]
  edge [
    source 13
    target 127
  ]
  edge [
    source 13
    target 128
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 129
  ]
  edge [
    source 14
    target 130
  ]
  edge [
    source 14
    target 131
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 132
  ]
  edge [
    source 17
    target 133
  ]
  edge [
    source 17
    target 134
  ]
  edge [
    source 17
    target 135
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 136
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 33
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 137
  ]
  edge [
    source 20
    target 138
  ]
  edge [
    source 20
    target 139
  ]
  edge [
    source 20
    target 140
  ]
  edge [
    source 20
    target 141
  ]
  edge [
    source 20
    target 142
  ]
  edge [
    source 20
    target 143
  ]
  edge [
    source 20
    target 144
  ]
  edge [
    source 20
    target 145
  ]
  edge [
    source 20
    target 146
  ]
  edge [
    source 20
    target 147
  ]
  edge [
    source 20
    target 148
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 149
  ]
  edge [
    source 22
    target 150
  ]
  edge [
    source 22
    target 151
  ]
  edge [
    source 22
    target 152
  ]
  edge [
    source 22
    target 153
  ]
  edge [
    source 22
    target 154
  ]
  edge [
    source 22
    target 155
  ]
  edge [
    source 22
    target 156
  ]
  edge [
    source 22
    target 157
  ]
  edge [
    source 22
    target 158
  ]
  edge [
    source 22
    target 159
  ]
  edge [
    source 22
    target 160
  ]
  edge [
    source 22
    target 161
  ]
  edge [
    source 22
    target 162
  ]
  edge [
    source 22
    target 163
  ]
  edge [
    source 22
    target 164
  ]
  edge [
    source 22
    target 165
  ]
  edge [
    source 22
    target 166
  ]
  edge [
    source 22
    target 167
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 48
  ]
  edge [
    source 25
    target 168
  ]
  edge [
    source 25
    target 169
  ]
  edge [
    source 25
    target 170
  ]
  edge [
    source 25
    target 171
  ]
  edge [
    source 25
    target 172
  ]
  edge [
    source 25
    target 173
  ]
  edge [
    source 25
    target 174
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 175
  ]
  edge [
    source 27
    target 176
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 177
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 178
  ]
  edge [
    source 30
    target 179
  ]
  edge [
    source 30
    target 180
  ]
  edge [
    source 30
    target 181
  ]
  edge [
    source 30
    target 182
  ]
  edge [
    source 30
    target 183
  ]
  edge [
    source 30
    target 184
  ]
  edge [
    source 30
    target 185
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 186
  ]
  edge [
    source 31
    target 187
  ]
  edge [
    source 31
    target 188
  ]
  edge [
    source 31
    target 189
  ]
  edge [
    source 31
    target 190
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 191
  ]
  edge [
    source 32
    target 192
  ]
  edge [
    source 32
    target 193
  ]
  edge [
    source 32
    target 194
  ]
  edge [
    source 32
    target 195
  ]
  edge [
    source 33
    target 196
  ]
  edge [
    source 33
    target 197
  ]
  edge [
    source 33
    target 198
  ]
]
