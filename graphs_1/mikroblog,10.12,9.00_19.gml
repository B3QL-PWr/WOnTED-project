graph [
  maxDegree 5
  minDegree 1
  meanDegree 1.6666666666666667
  density 0.3333333333333333
  graphCliqueNumber 2
  node [
    id 0
    label "wita&#263;"
    origin "text"
  ]
  node [
    id 1
    label "pozdrawia&#263;"
  ]
  node [
    id 2
    label "&#347;wi&#281;towa&#263;"
  ]
  node [
    id 3
    label "greet"
  ]
  node [
    id 4
    label "uzewn&#281;trznia&#263;_si&#281;"
  ]
  node [
    id 5
    label "welcome"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
]
