graph [
  maxDegree 56
  minDegree 1
  meanDegree 2.014705882352941
  density 0.014923747276688453
  graphCliqueNumber 2
  node [
    id 0
    label "mened&#380;er"
    origin "text"
  ]
  node [
    id 1
    label "izraelski"
    origin "text"
  ]
  node [
    id 2
    label "rze&#378;nia"
    origin "text"
  ]
  node [
    id 3
    label "promowa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "w&#347;r&#243;d"
    origin "text"
  ]
  node [
    id 5
    label "pracownik"
    origin "text"
  ]
  node [
    id 6
    label "kultura"
    origin "text"
  ]
  node [
    id 7
    label "bestialstwo"
    origin "text"
  ]
  node [
    id 8
    label "wobec"
    origin "text"
  ]
  node [
    id 9
    label "zwierz&#281;"
    origin "text"
  ]
  node [
    id 10
    label "kontrakt"
  ]
  node [
    id 11
    label "program"
  ]
  node [
    id 12
    label "kierownictwo"
  ]
  node [
    id 13
    label "agent"
  ]
  node [
    id 14
    label "zwierzchnik"
  ]
  node [
    id 15
    label "bliskowschodni"
  ]
  node [
    id 16
    label "zachodnioazjatycki"
  ]
  node [
    id 17
    label "moszaw"
  ]
  node [
    id 18
    label "azjatycki"
  ]
  node [
    id 19
    label "po_izraelsku"
  ]
  node [
    id 20
    label "zag&#322;ada"
  ]
  node [
    id 21
    label "zbrodnia"
  ]
  node [
    id 22
    label "genocide"
  ]
  node [
    id 23
    label "&#347;lizgownica"
  ]
  node [
    id 24
    label "szlachtuz"
  ]
  node [
    id 25
    label "gilotyniarz"
  ]
  node [
    id 26
    label "wytw&#243;rnia"
  ]
  node [
    id 27
    label "rzezalnia"
  ]
  node [
    id 28
    label "wypromowywa&#263;"
  ]
  node [
    id 29
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 30
    label "rozpowszechnia&#263;"
  ]
  node [
    id 31
    label "nada&#263;"
  ]
  node [
    id 32
    label "zach&#281;ca&#263;"
  ]
  node [
    id 33
    label "promocja"
  ]
  node [
    id 34
    label "udzieli&#263;"
  ]
  node [
    id 35
    label "udziela&#263;"
  ]
  node [
    id 36
    label "pomaga&#263;"
  ]
  node [
    id 37
    label "advance"
  ]
  node [
    id 38
    label "doprowadza&#263;"
  ]
  node [
    id 39
    label "reklama"
  ]
  node [
    id 40
    label "nadawa&#263;"
  ]
  node [
    id 41
    label "cz&#322;owiek_pracy"
  ]
  node [
    id 42
    label "cz&#322;owiek"
  ]
  node [
    id 43
    label "delegowa&#263;"
  ]
  node [
    id 44
    label "s&#322;u&#380;ba_cywilna"
  ]
  node [
    id 45
    label "pracu&#347;"
  ]
  node [
    id 46
    label "delegowanie"
  ]
  node [
    id 47
    label "r&#281;ka"
  ]
  node [
    id 48
    label "salariat"
  ]
  node [
    id 49
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 50
    label "przedmiot"
  ]
  node [
    id 51
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 52
    label "Wsch&#243;d"
  ]
  node [
    id 53
    label "rzecz"
  ]
  node [
    id 54
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 55
    label "sztuka"
  ]
  node [
    id 56
    label "religia"
  ]
  node [
    id 57
    label "przejmowa&#263;"
  ]
  node [
    id 58
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 59
    label "makrokosmos"
  ]
  node [
    id 60
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 61
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 62
    label "zjawisko"
  ]
  node [
    id 63
    label "praca_rolnicza"
  ]
  node [
    id 64
    label "tradycja"
  ]
  node [
    id 65
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 66
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 67
    label "przejmowanie"
  ]
  node [
    id 68
    label "cecha"
  ]
  node [
    id 69
    label "asymilowanie_si&#281;"
  ]
  node [
    id 70
    label "przej&#261;&#263;"
  ]
  node [
    id 71
    label "hodowla"
  ]
  node [
    id 72
    label "brzoskwiniarnia"
  ]
  node [
    id 73
    label "populace"
  ]
  node [
    id 74
    label "konwencja"
  ]
  node [
    id 75
    label "propriety"
  ]
  node [
    id 76
    label "jako&#347;&#263;"
  ]
  node [
    id 77
    label "kuchnia"
  ]
  node [
    id 78
    label "zwyczaj"
  ]
  node [
    id 79
    label "przej&#281;cie"
  ]
  node [
    id 80
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 81
    label "bestiality"
  ]
  node [
    id 82
    label "okrucie&#324;stwo"
  ]
  node [
    id 83
    label "grzbiet"
  ]
  node [
    id 84
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 85
    label "zachowanie"
  ]
  node [
    id 86
    label "fukanie"
  ]
  node [
    id 87
    label "popapraniec"
  ]
  node [
    id 88
    label "tresowa&#263;"
  ]
  node [
    id 89
    label "siedzie&#263;"
  ]
  node [
    id 90
    label "oswaja&#263;"
  ]
  node [
    id 91
    label "napa&#347;&#263;_si&#281;"
  ]
  node [
    id 92
    label "poskramia&#263;"
  ]
  node [
    id 93
    label "zwyrol"
  ]
  node [
    id 94
    label "animalista"
  ]
  node [
    id 95
    label "skubn&#261;&#263;"
  ]
  node [
    id 96
    label "fukni&#281;cie"
  ]
  node [
    id 97
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 98
    label "zar&#380;ni&#281;cie"
  ]
  node [
    id 99
    label "farba"
  ]
  node [
    id 100
    label "istota_&#380;ywa"
  ]
  node [
    id 101
    label "budowa"
  ]
  node [
    id 102
    label "pa&#347;&#263;_si&#281;"
  ]
  node [
    id 103
    label "budowa_cia&#322;a"
  ]
  node [
    id 104
    label "monogamia"
  ]
  node [
    id 105
    label "sodomita"
  ]
  node [
    id 106
    label "dor&#380;n&#261;&#263;"
  ]
  node [
    id 107
    label "oz&#243;r"
  ]
  node [
    id 108
    label "gad"
  ]
  node [
    id 109
    label "&#322;eb"
  ]
  node [
    id 110
    label "treser"
  ]
  node [
    id 111
    label "fauna"
  ]
  node [
    id 112
    label "pasienie_si&#281;"
  ]
  node [
    id 113
    label "degenerat"
  ]
  node [
    id 114
    label "czerniak"
  ]
  node [
    id 115
    label "siedzenie"
  ]
  node [
    id 116
    label "wiwarium"
  ]
  node [
    id 117
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 118
    label "weterynarz"
  ]
  node [
    id 119
    label "wios&#322;owa&#263;"
  ]
  node [
    id 120
    label "le&#380;e&#263;"
  ]
  node [
    id 121
    label "skuba&#263;"
  ]
  node [
    id 122
    label "skubni&#281;cie"
  ]
  node [
    id 123
    label "poligamia"
  ]
  node [
    id 124
    label "przyssawka"
  ]
  node [
    id 125
    label "agresja"
  ]
  node [
    id 126
    label "niecz&#322;owiek"
  ]
  node [
    id 127
    label "skubanie"
  ]
  node [
    id 128
    label "wios&#322;owanie"
  ]
  node [
    id 129
    label "napasienie_si&#281;"
  ]
  node [
    id 130
    label "okrutnik"
  ]
  node [
    id 131
    label "wylinka"
  ]
  node [
    id 132
    label "paszcza"
  ]
  node [
    id 133
    label "bestia"
  ]
  node [
    id 134
    label "zwierz&#281;ta"
  ]
  node [
    id 135
    label "le&#380;enie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 42
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 9
    target 100
  ]
  edge [
    source 9
    target 101
  ]
  edge [
    source 9
    target 102
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 104
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 9
    target 116
  ]
  edge [
    source 9
    target 117
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 9
    target 119
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 129
  ]
  edge [
    source 9
    target 130
  ]
  edge [
    source 9
    target 131
  ]
  edge [
    source 9
    target 132
  ]
  edge [
    source 9
    target 133
  ]
  edge [
    source 9
    target 134
  ]
  edge [
    source 9
    target 135
  ]
]
