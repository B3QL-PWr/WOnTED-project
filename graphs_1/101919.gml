graph [
  maxDegree 217
  minDegree 1
  meanDegree 2.1567877629063097
  density 0.004131777323575306
  graphCliqueNumber 4
  node [
    id 0
    label "finanse"
    origin "text"
  ]
  node [
    id 1
    label "samorz&#261;d"
    origin "text"
  ]
  node [
    id 2
    label "terytorialny"
    origin "text"
  ]
  node [
    id 3
    label "by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "integralny"
    origin "text"
  ]
  node [
    id 5
    label "cz&#281;&#347;&#263;"
    origin "text"
  ]
  node [
    id 6
    label "publiczny"
    origin "text"
  ]
  node [
    id 7
    label "system"
    origin "text"
  ]
  node [
    id 8
    label "finansowy"
    origin "text"
  ]
  node [
    id 9
    label "obejmowa&#263;"
    origin "text"
  ]
  node [
    id 10
    label "proces"
    origin "text"
  ]
  node [
    id 11
    label "zwi&#261;za&#263;"
    origin "text"
  ]
  node [
    id 12
    label "gromadzi&#263;"
    origin "text"
  ]
  node [
    id 13
    label "&#347;rodek"
    origin "text"
  ]
  node [
    id 14
    label "rozdysponowa&#263;"
    origin "text"
  ]
  node [
    id 15
    label "odpowiednio"
    origin "text"
  ]
  node [
    id 16
    label "poj&#281;cie"
    origin "text"
  ]
  node [
    id 17
    label "okre&#347;la&#263;"
    origin "text"
  ]
  node [
    id 18
    label "jako"
    origin "text"
  ]
  node [
    id 19
    label "zasoby"
    origin "text"
  ]
  node [
    id 20
    label "pieni&#281;&#380;ny"
    origin "text"
  ]
  node [
    id 21
    label "operacja"
    origin "text"
  ]
  node [
    id 22
    label "polega&#263;"
    origin "text"
  ]
  node [
    id 23
    label "doch&#243;d"
    origin "text"
  ]
  node [
    id 24
    label "w&#322;asne"
    origin "text"
  ]
  node [
    id 25
    label "wyr&#243;wnawczy"
    origin "text"
  ]
  node [
    id 26
    label "przychod"
    origin "text"
  ]
  node [
    id 27
    label "tak&#380;e"
    origin "text"
  ]
  node [
    id 28
    label "dokonywa&#263;"
    origin "text"
  ]
  node [
    id 29
    label "wydatek"
    origin "text"
  ]
  node [
    id 30
    label "rozch&#243;d"
    origin "text"
  ]
  node [
    id 31
    label "przez"
    origin "text"
  ]
  node [
    id 32
    label "jednostka"
    origin "text"
  ]
  node [
    id 33
    label "cela"
    origin "text"
  ]
  node [
    id 34
    label "sfinansowa&#263;"
    origin "text"
  ]
  node [
    id 35
    label "zadanie"
    origin "text"
  ]
  node [
    id 36
    label "zleci&#263;"
    origin "text"
  ]
  node [
    id 37
    label "pa&#324;stwo"
    origin "text"
  ]
  node [
    id 38
    label "wsp&#243;lny"
    origin "text"
  ]
  node [
    id 39
    label "uruchomienie"
  ]
  node [
    id 40
    label "nap&#322;yn&#261;&#263;"
  ]
  node [
    id 41
    label "nauka_ekonomiczna"
  ]
  node [
    id 42
    label "supernadz&#243;r"
  ]
  node [
    id 43
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 44
    label "nap&#322;yni&#281;cie"
  ]
  node [
    id 45
    label "absolutorium"
  ]
  node [
    id 46
    label "podupada&#263;"
  ]
  node [
    id 47
    label "nap&#322;ywanie"
  ]
  node [
    id 48
    label "podupadanie"
  ]
  node [
    id 49
    label "kwestor"
  ]
  node [
    id 50
    label "uruchamia&#263;"
  ]
  node [
    id 51
    label "mienie"
  ]
  node [
    id 52
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 53
    label "uruchamianie"
  ]
  node [
    id 54
    label "czynnik_produkcji"
  ]
  node [
    id 55
    label "autonomy"
  ]
  node [
    id 56
    label "organ"
  ]
  node [
    id 57
    label "terytorialnie"
  ]
  node [
    id 58
    label "si&#281;ga&#263;"
  ]
  node [
    id 59
    label "trwa&#263;"
  ]
  node [
    id 60
    label "obecno&#347;&#263;"
  ]
  node [
    id 61
    label "stan"
  ]
  node [
    id 62
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 63
    label "stand"
  ]
  node [
    id 64
    label "mie&#263;_miejsce"
  ]
  node [
    id 65
    label "uczestniczy&#263;"
  ]
  node [
    id 66
    label "chodzi&#263;"
  ]
  node [
    id 67
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 68
    label "equal"
  ]
  node [
    id 69
    label "wszystek"
  ]
  node [
    id 70
    label "niepodzielnie"
  ]
  node [
    id 71
    label "integralnie"
  ]
  node [
    id 72
    label "whole"
  ]
  node [
    id 73
    label "Rzym_Zachodni"
  ]
  node [
    id 74
    label "element"
  ]
  node [
    id 75
    label "ilo&#347;&#263;"
  ]
  node [
    id 76
    label "urz&#261;dzenie"
  ]
  node [
    id 77
    label "Rzym_Wschodni"
  ]
  node [
    id 78
    label "jawny"
  ]
  node [
    id 79
    label "upublicznienie"
  ]
  node [
    id 80
    label "upublicznianie"
  ]
  node [
    id 81
    label "publicznie"
  ]
  node [
    id 82
    label "model"
  ]
  node [
    id 83
    label "sk&#322;ad"
  ]
  node [
    id 84
    label "zachowanie"
  ]
  node [
    id 85
    label "podstawa"
  ]
  node [
    id 86
    label "porz&#261;dek"
  ]
  node [
    id 87
    label "Android"
  ]
  node [
    id 88
    label "przyn&#281;ta"
  ]
  node [
    id 89
    label "jednostka_geologiczna"
  ]
  node [
    id 90
    label "metoda"
  ]
  node [
    id 91
    label "podsystem"
  ]
  node [
    id 92
    label "p&#322;&#243;d"
  ]
  node [
    id 93
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 94
    label "s&#261;d"
  ]
  node [
    id 95
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 96
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 97
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 98
    label "j&#261;dro"
  ]
  node [
    id 99
    label "eratem"
  ]
  node [
    id 100
    label "ryba"
  ]
  node [
    id 101
    label "pulpit"
  ]
  node [
    id 102
    label "struktura"
  ]
  node [
    id 103
    label "spos&#243;b"
  ]
  node [
    id 104
    label "oddzia&#322;"
  ]
  node [
    id 105
    label "usenet"
  ]
  node [
    id 106
    label "o&#347;"
  ]
  node [
    id 107
    label "oprogramowanie"
  ]
  node [
    id 108
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 109
    label "w&#281;dkarstwo"
  ]
  node [
    id 110
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 111
    label "Leopard"
  ]
  node [
    id 112
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 113
    label "systemik"
  ]
  node [
    id 114
    label "rozprz&#261;c"
  ]
  node [
    id 115
    label "cybernetyk"
  ]
  node [
    id 116
    label "konstelacja"
  ]
  node [
    id 117
    label "doktryna"
  ]
  node [
    id 118
    label "net"
  ]
  node [
    id 119
    label "zbi&#243;r"
  ]
  node [
    id 120
    label "method"
  ]
  node [
    id 121
    label "systemat"
  ]
  node [
    id 122
    label "mi&#281;dzybankowy"
  ]
  node [
    id 123
    label "finansowo"
  ]
  node [
    id 124
    label "fizyczny"
  ]
  node [
    id 125
    label "pozamaterialny"
  ]
  node [
    id 126
    label "materjalny"
  ]
  node [
    id 127
    label "dotyka&#263;"
  ]
  node [
    id 128
    label "cover"
  ]
  node [
    id 129
    label "obj&#261;&#263;"
  ]
  node [
    id 130
    label "zagarnia&#263;"
  ]
  node [
    id 131
    label "involve"
  ]
  node [
    id 132
    label "mie&#263;"
  ]
  node [
    id 133
    label "zobowi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 134
    label "embrace"
  ]
  node [
    id 135
    label "meet"
  ]
  node [
    id 136
    label "fold"
  ]
  node [
    id 137
    label "senator"
  ]
  node [
    id 138
    label "dotyczy&#263;"
  ]
  node [
    id 139
    label "rozumie&#263;"
  ]
  node [
    id 140
    label "obejmowanie"
  ]
  node [
    id 141
    label "zaskakiwa&#263;"
  ]
  node [
    id 142
    label "rozprzestrzenia&#263;_si&#281;"
  ]
  node [
    id 143
    label "powodowa&#263;"
  ]
  node [
    id 144
    label "podejmowa&#263;"
  ]
  node [
    id 145
    label "legislacyjnie"
  ]
  node [
    id 146
    label "kognicja"
  ]
  node [
    id 147
    label "przebieg"
  ]
  node [
    id 148
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 149
    label "wydarzenie"
  ]
  node [
    id 150
    label "przes&#322;anka"
  ]
  node [
    id 151
    label "rozprawa"
  ]
  node [
    id 152
    label "zjawisko"
  ]
  node [
    id 153
    label "nast&#281;pstwo"
  ]
  node [
    id 154
    label "st&#281;&#380;e&#263;"
  ]
  node [
    id 155
    label "tobo&#322;ek"
  ]
  node [
    id 156
    label "obezw&#322;adni&#263;"
  ]
  node [
    id 157
    label "scali&#263;"
  ]
  node [
    id 158
    label "zawi&#261;za&#263;"
  ]
  node [
    id 159
    label "zatrzyma&#263;"
  ]
  node [
    id 160
    label "form"
  ]
  node [
    id 161
    label "bind"
  ]
  node [
    id 162
    label "wi&#261;zanie_chemiczne"
  ]
  node [
    id 163
    label "unify"
  ]
  node [
    id 164
    label "consort"
  ]
  node [
    id 165
    label "incorporate"
  ]
  node [
    id 166
    label "wi&#281;&#378;"
  ]
  node [
    id 167
    label "zobowi&#261;za&#263;"
  ]
  node [
    id 168
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 169
    label "w&#281;ze&#322;"
  ]
  node [
    id 170
    label "cz&#261;steczka_chemiczna"
  ]
  node [
    id 171
    label "powi&#261;za&#263;"
  ]
  node [
    id 172
    label "opakowa&#263;"
  ]
  node [
    id 173
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 174
    label "cement"
  ]
  node [
    id 175
    label "zaprawa"
  ]
  node [
    id 176
    label "relate"
  ]
  node [
    id 177
    label "zbiera&#263;"
  ]
  node [
    id 178
    label "congregate"
  ]
  node [
    id 179
    label "poci&#261;ga&#263;"
  ]
  node [
    id 180
    label "robi&#263;"
  ]
  node [
    id 181
    label "posiada&#263;"
  ]
  node [
    id 182
    label "miejsce"
  ]
  node [
    id 183
    label "czas"
  ]
  node [
    id 184
    label "abstrakcja"
  ]
  node [
    id 185
    label "punkt"
  ]
  node [
    id 186
    label "substancja"
  ]
  node [
    id 187
    label "chemikalia"
  ]
  node [
    id 188
    label "allocate"
  ]
  node [
    id 189
    label "rozdzieli&#263;"
  ]
  node [
    id 190
    label "stosowny"
  ]
  node [
    id 191
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 192
    label "nale&#380;ycie"
  ]
  node [
    id 193
    label "odpowiedni"
  ]
  node [
    id 194
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 195
    label "nale&#380;nie"
  ]
  node [
    id 196
    label "skumanie"
  ]
  node [
    id 197
    label "zorientowanie"
  ]
  node [
    id 198
    label "forma"
  ]
  node [
    id 199
    label "wytw&#243;r"
  ]
  node [
    id 200
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 201
    label "clasp"
  ]
  node [
    id 202
    label "teoria"
  ]
  node [
    id 203
    label "pos&#322;uchanie"
  ]
  node [
    id 204
    label "orientacja"
  ]
  node [
    id 205
    label "przem&#243;wienie"
  ]
  node [
    id 206
    label "signify"
  ]
  node [
    id 207
    label "decydowa&#263;"
  ]
  node [
    id 208
    label "style"
  ]
  node [
    id 209
    label "podmiot_gospodarczy"
  ]
  node [
    id 210
    label "z&#322;o&#380;e"
  ]
  node [
    id 211
    label "zasoby_kopalin"
  ]
  node [
    id 212
    label "monetarny"
  ]
  node [
    id 213
    label "infimum"
  ]
  node [
    id 214
    label "laparotomia"
  ]
  node [
    id 215
    label "chirurg"
  ]
  node [
    id 216
    label "manewr_Pringle&#8217;a"
  ]
  node [
    id 217
    label "supremum"
  ]
  node [
    id 218
    label "torakotomia"
  ]
  node [
    id 219
    label "funkcja"
  ]
  node [
    id 220
    label "strategia"
  ]
  node [
    id 221
    label "szew"
  ]
  node [
    id 222
    label "rzut"
  ]
  node [
    id 223
    label "liczenie"
  ]
  node [
    id 224
    label "ob&#322;awa_augustowska"
  ]
  node [
    id 225
    label "proces_my&#347;lowy"
  ]
  node [
    id 226
    label "czyn"
  ]
  node [
    id 227
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 228
    label "matematyka"
  ]
  node [
    id 229
    label "zabieg"
  ]
  node [
    id 230
    label "mathematical_process"
  ]
  node [
    id 231
    label "liczy&#263;"
  ]
  node [
    id 232
    label "ufa&#263;"
  ]
  node [
    id 233
    label "consist"
  ]
  node [
    id 234
    label "trust"
  ]
  node [
    id 235
    label "opiera&#263;_si&#281;"
  ]
  node [
    id 236
    label "korzy&#347;&#263;"
  ]
  node [
    id 237
    label "krzywa_Engla"
  ]
  node [
    id 238
    label "dochodowa_elastyczno&#347;&#263;_popytu"
  ]
  node [
    id 239
    label "wp&#322;yw"
  ]
  node [
    id 240
    label "income"
  ]
  node [
    id 241
    label "stopa_procentowa"
  ]
  node [
    id 242
    label "determine"
  ]
  node [
    id 243
    label "przestawa&#263;"
  ]
  node [
    id 244
    label "make"
  ]
  node [
    id 245
    label "nak&#322;ad"
  ]
  node [
    id 246
    label "koszt"
  ]
  node [
    id 247
    label "wych&#243;d"
  ]
  node [
    id 248
    label "ubytek"
  ]
  node [
    id 249
    label "ewoluowanie"
  ]
  node [
    id 250
    label "przyswoi&#263;"
  ]
  node [
    id 251
    label "reakcja"
  ]
  node [
    id 252
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 253
    label "wyewoluowanie"
  ]
  node [
    id 254
    label "individual"
  ]
  node [
    id 255
    label "profanum"
  ]
  node [
    id 256
    label "starzenie_si&#281;"
  ]
  node [
    id 257
    label "homo_sapiens"
  ]
  node [
    id 258
    label "skala"
  ]
  node [
    id 259
    label "przyswaja&#263;"
  ]
  node [
    id 260
    label "ludzko&#347;&#263;"
  ]
  node [
    id 261
    label "one"
  ]
  node [
    id 262
    label "przeliczenie"
  ]
  node [
    id 263
    label "przeliczanie"
  ]
  node [
    id 264
    label "mikrokosmos"
  ]
  node [
    id 265
    label "portrecista"
  ]
  node [
    id 266
    label "przelicza&#263;"
  ]
  node [
    id 267
    label "przyswajanie"
  ]
  node [
    id 268
    label "duch"
  ]
  node [
    id 269
    label "wyewoluowa&#263;"
  ]
  node [
    id 270
    label "ewoluowa&#263;"
  ]
  node [
    id 271
    label "oddzia&#322;ywanie"
  ]
  node [
    id 272
    label "g&#322;owa"
  ]
  node [
    id 273
    label "liczba_naturalna"
  ]
  node [
    id 274
    label "osoba"
  ]
  node [
    id 275
    label "figura"
  ]
  node [
    id 276
    label "obiekt"
  ]
  node [
    id 277
    label "przyswojenie"
  ]
  node [
    id 278
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 279
    label "czynnik_biotyczny"
  ]
  node [
    id 280
    label "przeliczy&#263;"
  ]
  node [
    id 281
    label "antropochoria"
  ]
  node [
    id 282
    label "pomieszczenie"
  ]
  node [
    id 283
    label "klasztor"
  ]
  node [
    id 284
    label "pay"
  ]
  node [
    id 285
    label "zap&#322;aci&#263;"
  ]
  node [
    id 286
    label "yield"
  ]
  node [
    id 287
    label "czynno&#347;&#263;"
  ]
  node [
    id 288
    label "problem"
  ]
  node [
    id 289
    label "przepisanie"
  ]
  node [
    id 290
    label "przepisa&#263;"
  ]
  node [
    id 291
    label "za&#322;o&#380;enie"
  ]
  node [
    id 292
    label "work"
  ]
  node [
    id 293
    label "nakarmienie"
  ]
  node [
    id 294
    label "duty"
  ]
  node [
    id 295
    label "powierzanie"
  ]
  node [
    id 296
    label "zaszkodzenie"
  ]
  node [
    id 297
    label "d&#378;wigni&#281;cie"
  ]
  node [
    id 298
    label "zaj&#281;cie"
  ]
  node [
    id 299
    label "zobowi&#261;zanie"
  ]
  node [
    id 300
    label "teach"
  ]
  node [
    id 301
    label "poleci&#263;"
  ]
  node [
    id 302
    label "Filipiny"
  ]
  node [
    id 303
    label "Rwanda"
  ]
  node [
    id 304
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 305
    label "Monako"
  ]
  node [
    id 306
    label "Korea"
  ]
  node [
    id 307
    label "Ghana"
  ]
  node [
    id 308
    label "Czarnog&#243;ra"
  ]
  node [
    id 309
    label "Malawi"
  ]
  node [
    id 310
    label "Indonezja"
  ]
  node [
    id 311
    label "Bu&#322;garia"
  ]
  node [
    id 312
    label "Nauru"
  ]
  node [
    id 313
    label "Kenia"
  ]
  node [
    id 314
    label "Kambod&#380;a"
  ]
  node [
    id 315
    label "Mali"
  ]
  node [
    id 316
    label "Austria"
  ]
  node [
    id 317
    label "interior"
  ]
  node [
    id 318
    label "Armenia"
  ]
  node [
    id 319
    label "Fid&#380;i"
  ]
  node [
    id 320
    label "Tuwalu"
  ]
  node [
    id 321
    label "Etiopia"
  ]
  node [
    id 322
    label "Malta"
  ]
  node [
    id 323
    label "Malezja"
  ]
  node [
    id 324
    label "Grenada"
  ]
  node [
    id 325
    label "Tad&#380;ykistan"
  ]
  node [
    id 326
    label "Wehrlen"
  ]
  node [
    id 327
    label "para"
  ]
  node [
    id 328
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 329
    label "Rumunia"
  ]
  node [
    id 330
    label "Maroko"
  ]
  node [
    id 331
    label "Bhutan"
  ]
  node [
    id 332
    label "S&#322;owacja"
  ]
  node [
    id 333
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 334
    label "Seszele"
  ]
  node [
    id 335
    label "Kuwejt"
  ]
  node [
    id 336
    label "Arabia_Saudyjska"
  ]
  node [
    id 337
    label "Ekwador"
  ]
  node [
    id 338
    label "Kanada"
  ]
  node [
    id 339
    label "Japonia"
  ]
  node [
    id 340
    label "ziemia"
  ]
  node [
    id 341
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 342
    label "Hiszpania"
  ]
  node [
    id 343
    label "Wyspy_Marshalla"
  ]
  node [
    id 344
    label "Botswana"
  ]
  node [
    id 345
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 346
    label "D&#380;ibuti"
  ]
  node [
    id 347
    label "grupa"
  ]
  node [
    id 348
    label "Wietnam"
  ]
  node [
    id 349
    label "Egipt"
  ]
  node [
    id 350
    label "Burkina_Faso"
  ]
  node [
    id 351
    label "Niemcy"
  ]
  node [
    id 352
    label "Khitai"
  ]
  node [
    id 353
    label "Macedonia"
  ]
  node [
    id 354
    label "Albania"
  ]
  node [
    id 355
    label "Madagaskar"
  ]
  node [
    id 356
    label "Bahrajn"
  ]
  node [
    id 357
    label "Jemen"
  ]
  node [
    id 358
    label "Lesoto"
  ]
  node [
    id 359
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 360
    label "Samoa"
  ]
  node [
    id 361
    label "Andora"
  ]
  node [
    id 362
    label "Chiny"
  ]
  node [
    id 363
    label "Cypr"
  ]
  node [
    id 364
    label "Wielka_Brytania"
  ]
  node [
    id 365
    label "Ukraina"
  ]
  node [
    id 366
    label "Paragwaj"
  ]
  node [
    id 367
    label "Trynidad_i_Tobago"
  ]
  node [
    id 368
    label "Libia"
  ]
  node [
    id 369
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 370
    label "Surinam"
  ]
  node [
    id 371
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 372
    label "Australia"
  ]
  node [
    id 373
    label "Nigeria"
  ]
  node [
    id 374
    label "Honduras"
  ]
  node [
    id 375
    label "Bangladesz"
  ]
  node [
    id 376
    label "Peru"
  ]
  node [
    id 377
    label "Kazachstan"
  ]
  node [
    id 378
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 379
    label "Irak"
  ]
  node [
    id 380
    label "holoarktyka"
  ]
  node [
    id 381
    label "USA"
  ]
  node [
    id 382
    label "Sudan"
  ]
  node [
    id 383
    label "Nepal"
  ]
  node [
    id 384
    label "San_Marino"
  ]
  node [
    id 385
    label "Burundi"
  ]
  node [
    id 386
    label "Dominikana"
  ]
  node [
    id 387
    label "Komory"
  ]
  node [
    id 388
    label "granica_pa&#324;stwa"
  ]
  node [
    id 389
    label "Gwatemala"
  ]
  node [
    id 390
    label "Antarktis"
  ]
  node [
    id 391
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 392
    label "Brunei"
  ]
  node [
    id 393
    label "Iran"
  ]
  node [
    id 394
    label "Zimbabwe"
  ]
  node [
    id 395
    label "Namibia"
  ]
  node [
    id 396
    label "Meksyk"
  ]
  node [
    id 397
    label "Kamerun"
  ]
  node [
    id 398
    label "zwrot"
  ]
  node [
    id 399
    label "Somalia"
  ]
  node [
    id 400
    label "Angola"
  ]
  node [
    id 401
    label "Gabon"
  ]
  node [
    id 402
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 403
    label "Mozambik"
  ]
  node [
    id 404
    label "Tajwan"
  ]
  node [
    id 405
    label "Tunezja"
  ]
  node [
    id 406
    label "Nowa_Zelandia"
  ]
  node [
    id 407
    label "Liban"
  ]
  node [
    id 408
    label "Jordania"
  ]
  node [
    id 409
    label "Tonga"
  ]
  node [
    id 410
    label "Czad"
  ]
  node [
    id 411
    label "Liberia"
  ]
  node [
    id 412
    label "Gwinea"
  ]
  node [
    id 413
    label "Belize"
  ]
  node [
    id 414
    label "&#321;otwa"
  ]
  node [
    id 415
    label "Syria"
  ]
  node [
    id 416
    label "Benin"
  ]
  node [
    id 417
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 418
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 419
    label "Dominika"
  ]
  node [
    id 420
    label "Antigua_i_Barbuda"
  ]
  node [
    id 421
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 422
    label "Hanower"
  ]
  node [
    id 423
    label "partia"
  ]
  node [
    id 424
    label "Afganistan"
  ]
  node [
    id 425
    label "Kiribati"
  ]
  node [
    id 426
    label "W&#322;ochy"
  ]
  node [
    id 427
    label "Szwajcaria"
  ]
  node [
    id 428
    label "Sahara_Zachodnia"
  ]
  node [
    id 429
    label "Chorwacja"
  ]
  node [
    id 430
    label "Tajlandia"
  ]
  node [
    id 431
    label "Salwador"
  ]
  node [
    id 432
    label "Bahamy"
  ]
  node [
    id 433
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 434
    label "S&#322;owenia"
  ]
  node [
    id 435
    label "Gambia"
  ]
  node [
    id 436
    label "Urugwaj"
  ]
  node [
    id 437
    label "Zair"
  ]
  node [
    id 438
    label "Erytrea"
  ]
  node [
    id 439
    label "Rosja"
  ]
  node [
    id 440
    label "Uganda"
  ]
  node [
    id 441
    label "Niger"
  ]
  node [
    id 442
    label "Mauritius"
  ]
  node [
    id 443
    label "Turkmenistan"
  ]
  node [
    id 444
    label "Turcja"
  ]
  node [
    id 445
    label "Irlandia"
  ]
  node [
    id 446
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 447
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 448
    label "Gwinea_Bissau"
  ]
  node [
    id 449
    label "Belgia"
  ]
  node [
    id 450
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 451
    label "Palau"
  ]
  node [
    id 452
    label "Barbados"
  ]
  node [
    id 453
    label "Chile"
  ]
  node [
    id 454
    label "Wenezuela"
  ]
  node [
    id 455
    label "W&#281;gry"
  ]
  node [
    id 456
    label "Argentyna"
  ]
  node [
    id 457
    label "Kolumbia"
  ]
  node [
    id 458
    label "Sierra_Leone"
  ]
  node [
    id 459
    label "Azerbejd&#380;an"
  ]
  node [
    id 460
    label "Kongo"
  ]
  node [
    id 461
    label "Pakistan"
  ]
  node [
    id 462
    label "Liechtenstein"
  ]
  node [
    id 463
    label "Nikaragua"
  ]
  node [
    id 464
    label "Senegal"
  ]
  node [
    id 465
    label "Indie"
  ]
  node [
    id 466
    label "Suazi"
  ]
  node [
    id 467
    label "Polska"
  ]
  node [
    id 468
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 469
    label "Algieria"
  ]
  node [
    id 470
    label "terytorium"
  ]
  node [
    id 471
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 472
    label "Jamajka"
  ]
  node [
    id 473
    label "Kostaryka"
  ]
  node [
    id 474
    label "Timor_Wschodni"
  ]
  node [
    id 475
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 476
    label "Kuba"
  ]
  node [
    id 477
    label "Mauretania"
  ]
  node [
    id 478
    label "Portoryko"
  ]
  node [
    id 479
    label "Brazylia"
  ]
  node [
    id 480
    label "Mo&#322;dawia"
  ]
  node [
    id 481
    label "organizacja"
  ]
  node [
    id 482
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 483
    label "Litwa"
  ]
  node [
    id 484
    label "Kirgistan"
  ]
  node [
    id 485
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 486
    label "Izrael"
  ]
  node [
    id 487
    label "Grecja"
  ]
  node [
    id 488
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 489
    label "Holandia"
  ]
  node [
    id 490
    label "Sri_Lanka"
  ]
  node [
    id 491
    label "Katar"
  ]
  node [
    id 492
    label "Mikronezja"
  ]
  node [
    id 493
    label "Mongolia"
  ]
  node [
    id 494
    label "Laos"
  ]
  node [
    id 495
    label "Malediwy"
  ]
  node [
    id 496
    label "Zambia"
  ]
  node [
    id 497
    label "Tanzania"
  ]
  node [
    id 498
    label "Gujana"
  ]
  node [
    id 499
    label "Czechy"
  ]
  node [
    id 500
    label "Panama"
  ]
  node [
    id 501
    label "Uzbekistan"
  ]
  node [
    id 502
    label "Gruzja"
  ]
  node [
    id 503
    label "Serbia"
  ]
  node [
    id 504
    label "Francja"
  ]
  node [
    id 505
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 506
    label "Togo"
  ]
  node [
    id 507
    label "Estonia"
  ]
  node [
    id 508
    label "Oman"
  ]
  node [
    id 509
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 510
    label "Portugalia"
  ]
  node [
    id 511
    label "Boliwia"
  ]
  node [
    id 512
    label "Luksemburg"
  ]
  node [
    id 513
    label "Haiti"
  ]
  node [
    id 514
    label "Wyspy_Salomona"
  ]
  node [
    id 515
    label "Birma"
  ]
  node [
    id 516
    label "Rodezja"
  ]
  node [
    id 517
    label "spolny"
  ]
  node [
    id 518
    label "jeden"
  ]
  node [
    id 519
    label "sp&#243;lny"
  ]
  node [
    id 520
    label "wsp&#243;lnie"
  ]
  node [
    id 521
    label "uwsp&#243;lnianie"
  ]
  node [
    id 522
    label "uwsp&#243;lnienie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 13
  ]
  edge [
    source 6
    target 14
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 16
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 7
    target 113
  ]
  edge [
    source 7
    target 114
  ]
  edge [
    source 7
    target 115
  ]
  edge [
    source 7
    target 116
  ]
  edge [
    source 7
    target 117
  ]
  edge [
    source 7
    target 118
  ]
  edge [
    source 7
    target 119
  ]
  edge [
    source 7
    target 120
  ]
  edge [
    source 7
    target 121
  ]
  edge [
    source 7
    target 14
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 21
  ]
  edge [
    source 8
    target 22
  ]
  edge [
    source 8
    target 122
  ]
  edge [
    source 8
    target 123
  ]
  edge [
    source 8
    target 124
  ]
  edge [
    source 8
    target 125
  ]
  edge [
    source 8
    target 126
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 129
  ]
  edge [
    source 9
    target 130
  ]
  edge [
    source 9
    target 131
  ]
  edge [
    source 9
    target 132
  ]
  edge [
    source 9
    target 133
  ]
  edge [
    source 9
    target 134
  ]
  edge [
    source 9
    target 135
  ]
  edge [
    source 9
    target 136
  ]
  edge [
    source 9
    target 137
  ]
  edge [
    source 9
    target 138
  ]
  edge [
    source 9
    target 139
  ]
  edge [
    source 9
    target 140
  ]
  edge [
    source 9
    target 141
  ]
  edge [
    source 9
    target 142
  ]
  edge [
    source 9
    target 143
  ]
  edge [
    source 9
    target 144
  ]
  edge [
    source 9
    target 23
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 145
  ]
  edge [
    source 10
    target 146
  ]
  edge [
    source 10
    target 147
  ]
  edge [
    source 10
    target 148
  ]
  edge [
    source 10
    target 149
  ]
  edge [
    source 10
    target 150
  ]
  edge [
    source 10
    target 151
  ]
  edge [
    source 10
    target 152
  ]
  edge [
    source 10
    target 153
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 154
  ]
  edge [
    source 11
    target 155
  ]
  edge [
    source 11
    target 156
  ]
  edge [
    source 11
    target 157
  ]
  edge [
    source 11
    target 158
  ]
  edge [
    source 11
    target 159
  ]
  edge [
    source 11
    target 160
  ]
  edge [
    source 11
    target 161
  ]
  edge [
    source 11
    target 162
  ]
  edge [
    source 11
    target 163
  ]
  edge [
    source 11
    target 164
  ]
  edge [
    source 11
    target 165
  ]
  edge [
    source 11
    target 166
  ]
  edge [
    source 11
    target 167
  ]
  edge [
    source 11
    target 168
  ]
  edge [
    source 11
    target 169
  ]
  edge [
    source 11
    target 170
  ]
  edge [
    source 11
    target 171
  ]
  edge [
    source 11
    target 172
  ]
  edge [
    source 11
    target 173
  ]
  edge [
    source 11
    target 174
  ]
  edge [
    source 11
    target 175
  ]
  edge [
    source 11
    target 176
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 22
  ]
  edge [
    source 12
    target 23
  ]
  edge [
    source 12
    target 177
  ]
  edge [
    source 12
    target 178
  ]
  edge [
    source 12
    target 179
  ]
  edge [
    source 12
    target 180
  ]
  edge [
    source 12
    target 181
  ]
  edge [
    source 12
    target 143
  ]
  edge [
    source 13
    target 19
  ]
  edge [
    source 13
    target 20
  ]
  edge [
    source 13
    target 182
  ]
  edge [
    source 13
    target 183
  ]
  edge [
    source 13
    target 184
  ]
  edge [
    source 13
    target 185
  ]
  edge [
    source 13
    target 186
  ]
  edge [
    source 13
    target 103
  ]
  edge [
    source 13
    target 187
  ]
  edge [
    source 13
    target 23
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 188
  ]
  edge [
    source 14
    target 189
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 190
  ]
  edge [
    source 15
    target 191
  ]
  edge [
    source 15
    target 192
  ]
  edge [
    source 15
    target 193
  ]
  edge [
    source 15
    target 194
  ]
  edge [
    source 15
    target 195
  ]
  edge [
    source 16
    target 196
  ]
  edge [
    source 16
    target 197
  ]
  edge [
    source 16
    target 198
  ]
  edge [
    source 16
    target 199
  ]
  edge [
    source 16
    target 200
  ]
  edge [
    source 16
    target 201
  ]
  edge [
    source 16
    target 202
  ]
  edge [
    source 16
    target 203
  ]
  edge [
    source 16
    target 204
  ]
  edge [
    source 16
    target 205
  ]
  edge [
    source 16
    target 32
  ]
  edge [
    source 17
    target 206
  ]
  edge [
    source 17
    target 143
  ]
  edge [
    source 17
    target 207
  ]
  edge [
    source 17
    target 208
  ]
  edge [
    source 17
    target 24
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 209
  ]
  edge [
    source 19
    target 210
  ]
  edge [
    source 19
    target 211
  ]
  edge [
    source 19
    target 97
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 212
  ]
  edge [
    source 21
    target 213
  ]
  edge [
    source 21
    target 214
  ]
  edge [
    source 21
    target 215
  ]
  edge [
    source 21
    target 216
  ]
  edge [
    source 21
    target 217
  ]
  edge [
    source 21
    target 218
  ]
  edge [
    source 21
    target 219
  ]
  edge [
    source 21
    target 220
  ]
  edge [
    source 21
    target 221
  ]
  edge [
    source 21
    target 222
  ]
  edge [
    source 21
    target 223
  ]
  edge [
    source 21
    target 224
  ]
  edge [
    source 21
    target 225
  ]
  edge [
    source 21
    target 226
  ]
  edge [
    source 21
    target 227
  ]
  edge [
    source 21
    target 228
  ]
  edge [
    source 21
    target 229
  ]
  edge [
    source 21
    target 230
  ]
  edge [
    source 21
    target 231
  ]
  edge [
    source 21
    target 32
  ]
  edge [
    source 22
    target 232
  ]
  edge [
    source 22
    target 233
  ]
  edge [
    source 22
    target 234
  ]
  edge [
    source 22
    target 235
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 25
  ]
  edge [
    source 23
    target 236
  ]
  edge [
    source 23
    target 237
  ]
  edge [
    source 23
    target 238
  ]
  edge [
    source 23
    target 239
  ]
  edge [
    source 23
    target 240
  ]
  edge [
    source 23
    target 241
  ]
  edge [
    source 24
    target 35
  ]
  edge [
    source 24
    target 36
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 27
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 35
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 180
  ]
  edge [
    source 28
    target 242
  ]
  edge [
    source 28
    target 243
  ]
  edge [
    source 28
    target 244
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 245
  ]
  edge [
    source 29
    target 246
  ]
  edge [
    source 29
    target 247
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 248
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 36
  ]
  edge [
    source 31
    target 37
  ]
  edge [
    source 32
    target 213
  ]
  edge [
    source 32
    target 249
  ]
  edge [
    source 32
    target 250
  ]
  edge [
    source 32
    target 251
  ]
  edge [
    source 32
    target 252
  ]
  edge [
    source 32
    target 253
  ]
  edge [
    source 32
    target 254
  ]
  edge [
    source 32
    target 255
  ]
  edge [
    source 32
    target 256
  ]
  edge [
    source 32
    target 257
  ]
  edge [
    source 32
    target 258
  ]
  edge [
    source 32
    target 217
  ]
  edge [
    source 32
    target 259
  ]
  edge [
    source 32
    target 260
  ]
  edge [
    source 32
    target 97
  ]
  edge [
    source 32
    target 261
  ]
  edge [
    source 32
    target 219
  ]
  edge [
    source 32
    target 262
  ]
  edge [
    source 32
    target 263
  ]
  edge [
    source 32
    target 264
  ]
  edge [
    source 32
    target 222
  ]
  edge [
    source 32
    target 265
  ]
  edge [
    source 32
    target 266
  ]
  edge [
    source 32
    target 267
  ]
  edge [
    source 32
    target 268
  ]
  edge [
    source 32
    target 269
  ]
  edge [
    source 32
    target 270
  ]
  edge [
    source 32
    target 271
  ]
  edge [
    source 32
    target 272
  ]
  edge [
    source 32
    target 273
  ]
  edge [
    source 32
    target 274
  ]
  edge [
    source 32
    target 275
  ]
  edge [
    source 32
    target 227
  ]
  edge [
    source 32
    target 276
  ]
  edge [
    source 32
    target 228
  ]
  edge [
    source 32
    target 277
  ]
  edge [
    source 32
    target 278
  ]
  edge [
    source 32
    target 279
  ]
  edge [
    source 32
    target 280
  ]
  edge [
    source 32
    target 281
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 282
  ]
  edge [
    source 33
    target 283
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 284
  ]
  edge [
    source 34
    target 285
  ]
  edge [
    source 35
    target 37
  ]
  edge [
    source 35
    target 38
  ]
  edge [
    source 35
    target 286
  ]
  edge [
    source 35
    target 287
  ]
  edge [
    source 35
    target 288
  ]
  edge [
    source 35
    target 289
  ]
  edge [
    source 35
    target 290
  ]
  edge [
    source 35
    target 291
  ]
  edge [
    source 35
    target 292
  ]
  edge [
    source 35
    target 293
  ]
  edge [
    source 35
    target 294
  ]
  edge [
    source 35
    target 119
  ]
  edge [
    source 35
    target 295
  ]
  edge [
    source 35
    target 296
  ]
  edge [
    source 35
    target 297
  ]
  edge [
    source 35
    target 298
  ]
  edge [
    source 35
    target 299
  ]
  edge [
    source 36
    target 300
  ]
  edge [
    source 36
    target 301
  ]
  edge [
    source 37
    target 302
  ]
  edge [
    source 37
    target 303
  ]
  edge [
    source 37
    target 304
  ]
  edge [
    source 37
    target 305
  ]
  edge [
    source 37
    target 306
  ]
  edge [
    source 37
    target 307
  ]
  edge [
    source 37
    target 308
  ]
  edge [
    source 37
    target 309
  ]
  edge [
    source 37
    target 310
  ]
  edge [
    source 37
    target 311
  ]
  edge [
    source 37
    target 312
  ]
  edge [
    source 37
    target 313
  ]
  edge [
    source 37
    target 314
  ]
  edge [
    source 37
    target 315
  ]
  edge [
    source 37
    target 316
  ]
  edge [
    source 37
    target 317
  ]
  edge [
    source 37
    target 318
  ]
  edge [
    source 37
    target 319
  ]
  edge [
    source 37
    target 320
  ]
  edge [
    source 37
    target 321
  ]
  edge [
    source 37
    target 322
  ]
  edge [
    source 37
    target 323
  ]
  edge [
    source 37
    target 324
  ]
  edge [
    source 37
    target 325
  ]
  edge [
    source 37
    target 326
  ]
  edge [
    source 37
    target 327
  ]
  edge [
    source 37
    target 328
  ]
  edge [
    source 37
    target 329
  ]
  edge [
    source 37
    target 330
  ]
  edge [
    source 37
    target 331
  ]
  edge [
    source 37
    target 332
  ]
  edge [
    source 37
    target 333
  ]
  edge [
    source 37
    target 334
  ]
  edge [
    source 37
    target 335
  ]
  edge [
    source 37
    target 336
  ]
  edge [
    source 37
    target 337
  ]
  edge [
    source 37
    target 338
  ]
  edge [
    source 37
    target 339
  ]
  edge [
    source 37
    target 340
  ]
  edge [
    source 37
    target 341
  ]
  edge [
    source 37
    target 342
  ]
  edge [
    source 37
    target 343
  ]
  edge [
    source 37
    target 344
  ]
  edge [
    source 37
    target 345
  ]
  edge [
    source 37
    target 346
  ]
  edge [
    source 37
    target 347
  ]
  edge [
    source 37
    target 348
  ]
  edge [
    source 37
    target 349
  ]
  edge [
    source 37
    target 350
  ]
  edge [
    source 37
    target 351
  ]
  edge [
    source 37
    target 352
  ]
  edge [
    source 37
    target 353
  ]
  edge [
    source 37
    target 354
  ]
  edge [
    source 37
    target 355
  ]
  edge [
    source 37
    target 356
  ]
  edge [
    source 37
    target 357
  ]
  edge [
    source 37
    target 358
  ]
  edge [
    source 37
    target 359
  ]
  edge [
    source 37
    target 360
  ]
  edge [
    source 37
    target 361
  ]
  edge [
    source 37
    target 362
  ]
  edge [
    source 37
    target 363
  ]
  edge [
    source 37
    target 364
  ]
  edge [
    source 37
    target 365
  ]
  edge [
    source 37
    target 366
  ]
  edge [
    source 37
    target 367
  ]
  edge [
    source 37
    target 368
  ]
  edge [
    source 37
    target 369
  ]
  edge [
    source 37
    target 370
  ]
  edge [
    source 37
    target 371
  ]
  edge [
    source 37
    target 372
  ]
  edge [
    source 37
    target 373
  ]
  edge [
    source 37
    target 374
  ]
  edge [
    source 37
    target 375
  ]
  edge [
    source 37
    target 376
  ]
  edge [
    source 37
    target 377
  ]
  edge [
    source 37
    target 378
  ]
  edge [
    source 37
    target 379
  ]
  edge [
    source 37
    target 380
  ]
  edge [
    source 37
    target 381
  ]
  edge [
    source 37
    target 382
  ]
  edge [
    source 37
    target 383
  ]
  edge [
    source 37
    target 384
  ]
  edge [
    source 37
    target 385
  ]
  edge [
    source 37
    target 386
  ]
  edge [
    source 37
    target 387
  ]
  edge [
    source 37
    target 388
  ]
  edge [
    source 37
    target 389
  ]
  edge [
    source 37
    target 390
  ]
  edge [
    source 37
    target 391
  ]
  edge [
    source 37
    target 392
  ]
  edge [
    source 37
    target 393
  ]
  edge [
    source 37
    target 394
  ]
  edge [
    source 37
    target 395
  ]
  edge [
    source 37
    target 396
  ]
  edge [
    source 37
    target 397
  ]
  edge [
    source 37
    target 398
  ]
  edge [
    source 37
    target 399
  ]
  edge [
    source 37
    target 400
  ]
  edge [
    source 37
    target 401
  ]
  edge [
    source 37
    target 402
  ]
  edge [
    source 37
    target 403
  ]
  edge [
    source 37
    target 404
  ]
  edge [
    source 37
    target 405
  ]
  edge [
    source 37
    target 406
  ]
  edge [
    source 37
    target 407
  ]
  edge [
    source 37
    target 408
  ]
  edge [
    source 37
    target 409
  ]
  edge [
    source 37
    target 410
  ]
  edge [
    source 37
    target 411
  ]
  edge [
    source 37
    target 412
  ]
  edge [
    source 37
    target 413
  ]
  edge [
    source 37
    target 414
  ]
  edge [
    source 37
    target 415
  ]
  edge [
    source 37
    target 416
  ]
  edge [
    source 37
    target 417
  ]
  edge [
    source 37
    target 418
  ]
  edge [
    source 37
    target 419
  ]
  edge [
    source 37
    target 420
  ]
  edge [
    source 37
    target 421
  ]
  edge [
    source 37
    target 422
  ]
  edge [
    source 37
    target 423
  ]
  edge [
    source 37
    target 424
  ]
  edge [
    source 37
    target 425
  ]
  edge [
    source 37
    target 426
  ]
  edge [
    source 37
    target 427
  ]
  edge [
    source 37
    target 428
  ]
  edge [
    source 37
    target 429
  ]
  edge [
    source 37
    target 430
  ]
  edge [
    source 37
    target 431
  ]
  edge [
    source 37
    target 432
  ]
  edge [
    source 37
    target 433
  ]
  edge [
    source 37
    target 434
  ]
  edge [
    source 37
    target 435
  ]
  edge [
    source 37
    target 436
  ]
  edge [
    source 37
    target 437
  ]
  edge [
    source 37
    target 438
  ]
  edge [
    source 37
    target 439
  ]
  edge [
    source 37
    target 440
  ]
  edge [
    source 37
    target 441
  ]
  edge [
    source 37
    target 442
  ]
  edge [
    source 37
    target 443
  ]
  edge [
    source 37
    target 444
  ]
  edge [
    source 37
    target 445
  ]
  edge [
    source 37
    target 446
  ]
  edge [
    source 37
    target 447
  ]
  edge [
    source 37
    target 448
  ]
  edge [
    source 37
    target 449
  ]
  edge [
    source 37
    target 450
  ]
  edge [
    source 37
    target 451
  ]
  edge [
    source 37
    target 452
  ]
  edge [
    source 37
    target 453
  ]
  edge [
    source 37
    target 454
  ]
  edge [
    source 37
    target 455
  ]
  edge [
    source 37
    target 456
  ]
  edge [
    source 37
    target 457
  ]
  edge [
    source 37
    target 458
  ]
  edge [
    source 37
    target 459
  ]
  edge [
    source 37
    target 460
  ]
  edge [
    source 37
    target 461
  ]
  edge [
    source 37
    target 462
  ]
  edge [
    source 37
    target 463
  ]
  edge [
    source 37
    target 464
  ]
  edge [
    source 37
    target 465
  ]
  edge [
    source 37
    target 466
  ]
  edge [
    source 37
    target 467
  ]
  edge [
    source 37
    target 468
  ]
  edge [
    source 37
    target 469
  ]
  edge [
    source 37
    target 470
  ]
  edge [
    source 37
    target 471
  ]
  edge [
    source 37
    target 472
  ]
  edge [
    source 37
    target 473
  ]
  edge [
    source 37
    target 474
  ]
  edge [
    source 37
    target 475
  ]
  edge [
    source 37
    target 476
  ]
  edge [
    source 37
    target 477
  ]
  edge [
    source 37
    target 478
  ]
  edge [
    source 37
    target 479
  ]
  edge [
    source 37
    target 480
  ]
  edge [
    source 37
    target 481
  ]
  edge [
    source 37
    target 482
  ]
  edge [
    source 37
    target 483
  ]
  edge [
    source 37
    target 484
  ]
  edge [
    source 37
    target 485
  ]
  edge [
    source 37
    target 486
  ]
  edge [
    source 37
    target 487
  ]
  edge [
    source 37
    target 488
  ]
  edge [
    source 37
    target 489
  ]
  edge [
    source 37
    target 490
  ]
  edge [
    source 37
    target 491
  ]
  edge [
    source 37
    target 492
  ]
  edge [
    source 37
    target 493
  ]
  edge [
    source 37
    target 494
  ]
  edge [
    source 37
    target 495
  ]
  edge [
    source 37
    target 496
  ]
  edge [
    source 37
    target 497
  ]
  edge [
    source 37
    target 498
  ]
  edge [
    source 37
    target 499
  ]
  edge [
    source 37
    target 500
  ]
  edge [
    source 37
    target 501
  ]
  edge [
    source 37
    target 502
  ]
  edge [
    source 37
    target 503
  ]
  edge [
    source 37
    target 504
  ]
  edge [
    source 37
    target 505
  ]
  edge [
    source 37
    target 506
  ]
  edge [
    source 37
    target 507
  ]
  edge [
    source 37
    target 508
  ]
  edge [
    source 37
    target 509
  ]
  edge [
    source 37
    target 510
  ]
  edge [
    source 37
    target 511
  ]
  edge [
    source 37
    target 512
  ]
  edge [
    source 37
    target 513
  ]
  edge [
    source 37
    target 514
  ]
  edge [
    source 37
    target 515
  ]
  edge [
    source 37
    target 516
  ]
  edge [
    source 38
    target 517
  ]
  edge [
    source 38
    target 518
  ]
  edge [
    source 38
    target 519
  ]
  edge [
    source 38
    target 520
  ]
  edge [
    source 38
    target 521
  ]
  edge [
    source 38
    target 522
  ]
]
