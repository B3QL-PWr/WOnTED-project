graph [
  maxDegree 6
  minDegree 1
  meanDegree 1.7142857142857142
  density 0.2857142857142857
  graphCliqueNumber 2
  node [
    id 0
    label "osprz&#281;t"
    origin "text"
  ]
  node [
    id 1
    label "appointment"
  ]
  node [
    id 2
    label "olinowanie"
  ]
  node [
    id 3
    label "o&#380;aglowanie"
  ]
  node [
    id 4
    label "omasztowanie"
  ]
  node [
    id 5
    label "wyposa&#380;enie"
  ]
  node [
    id 6
    label "&#380;aglowiec"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
]
