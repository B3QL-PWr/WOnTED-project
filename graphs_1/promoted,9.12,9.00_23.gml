graph [
  maxDegree 23
  minDegree 1
  meanDegree 1.962962962962963
  density 0.037037037037037035
  graphCliqueNumber 2
  node [
    id 0
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 1
    label "ch&#322;opiec"
    origin "text"
  ]
  node [
    id 2
    label "robi&#263;"
    origin "text"
  ]
  node [
    id 3
    label "gara&#380;"
    origin "text"
  ]
  node [
    id 4
    label "opr&#243;cz"
    origin "text"
  ]
  node [
    id 5
    label "bimber"
    origin "text"
  ]
  node [
    id 6
    label "doros&#322;y"
  ]
  node [
    id 7
    label "wiele"
  ]
  node [
    id 8
    label "dorodny"
  ]
  node [
    id 9
    label "znaczny"
  ]
  node [
    id 10
    label "du&#380;o"
  ]
  node [
    id 11
    label "prawdziwy"
  ]
  node [
    id 12
    label "niema&#322;o"
  ]
  node [
    id 13
    label "wa&#380;ny"
  ]
  node [
    id 14
    label "rozwini&#281;ty"
  ]
  node [
    id 15
    label "cz&#322;owiek"
  ]
  node [
    id 16
    label "pomocnik"
  ]
  node [
    id 17
    label "g&#243;wniarz"
  ]
  node [
    id 18
    label "&#347;l&#261;ski"
  ]
  node [
    id 19
    label "m&#322;odzieniec"
  ]
  node [
    id 20
    label "kajtek"
  ]
  node [
    id 21
    label "kawaler"
  ]
  node [
    id 22
    label "usynawianie"
  ]
  node [
    id 23
    label "dziecko"
  ]
  node [
    id 24
    label "okrzos"
  ]
  node [
    id 25
    label "usynowienie"
  ]
  node [
    id 26
    label "sympatia"
  ]
  node [
    id 27
    label "pederasta"
  ]
  node [
    id 28
    label "synek"
  ]
  node [
    id 29
    label "boyfriend"
  ]
  node [
    id 30
    label "tentegowa&#263;"
  ]
  node [
    id 31
    label "urz&#261;dza&#263;"
  ]
  node [
    id 32
    label "give"
  ]
  node [
    id 33
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 34
    label "czyni&#263;"
  ]
  node [
    id 35
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 36
    label "post&#281;powa&#263;"
  ]
  node [
    id 37
    label "wydala&#263;"
  ]
  node [
    id 38
    label "oszukiwa&#263;"
  ]
  node [
    id 39
    label "organizowa&#263;"
  ]
  node [
    id 40
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 41
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 42
    label "work"
  ]
  node [
    id 43
    label "przerabia&#263;"
  ]
  node [
    id 44
    label "stylizowa&#263;"
  ]
  node [
    id 45
    label "falowa&#263;"
  ]
  node [
    id 46
    label "act"
  ]
  node [
    id 47
    label "peddle"
  ]
  node [
    id 48
    label "ukazywa&#263;"
  ]
  node [
    id 49
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 50
    label "praca"
  ]
  node [
    id 51
    label "warsztat"
  ]
  node [
    id 52
    label "pomieszczenie"
  ]
  node [
    id 53
    label "w&#243;dka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 53
  ]
]
