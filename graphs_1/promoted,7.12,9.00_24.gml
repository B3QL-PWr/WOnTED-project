graph [
  maxDegree 17
  minDegree 1
  meanDegree 2
  density 0.04878048780487805
  graphCliqueNumber 2
  node [
    id 0
    label "wymieni&#263;"
    origin "text"
  ]
  node [
    id 1
    label "fuga"
    origin "text"
  ]
  node [
    id 2
    label "instrukcja"
    origin "text"
  ]
  node [
    id 3
    label "krok"
    origin "text"
  ]
  node [
    id 4
    label "zmieni&#263;"
  ]
  node [
    id 5
    label "spowodowa&#263;"
  ]
  node [
    id 6
    label "style"
  ]
  node [
    id 7
    label "tell"
  ]
  node [
    id 8
    label "come_up"
  ]
  node [
    id 9
    label "komunikowa&#263;"
  ]
  node [
    id 10
    label "skontaktowa&#263;_si&#281;"
  ]
  node [
    id 11
    label "zrobi&#263;"
  ]
  node [
    id 12
    label "poda&#263;"
  ]
  node [
    id 13
    label "pomieni&#263;"
  ]
  node [
    id 14
    label "Bach"
  ]
  node [
    id 15
    label "z&#322;&#261;czenie"
  ]
  node [
    id 16
    label "szczelina"
  ]
  node [
    id 17
    label "bruzda"
  ]
  node [
    id 18
    label "polifonia"
  ]
  node [
    id 19
    label "zaprawa"
  ]
  node [
    id 20
    label "utw&#243;r"
  ]
  node [
    id 21
    label "dysocjacja"
  ]
  node [
    id 22
    label "ekspozycja"
  ]
  node [
    id 23
    label "ulotka"
  ]
  node [
    id 24
    label "program"
  ]
  node [
    id 25
    label "instruktarz"
  ]
  node [
    id 26
    label "zbi&#243;r"
  ]
  node [
    id 27
    label "wskaz&#243;wka"
  ]
  node [
    id 28
    label "pace"
  ]
  node [
    id 29
    label "czyn"
  ]
  node [
    id 30
    label "passus"
  ]
  node [
    id 31
    label "measurement"
  ]
  node [
    id 32
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 33
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 34
    label "ruch"
  ]
  node [
    id 35
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 36
    label "action"
  ]
  node [
    id 37
    label "chodzi&#263;"
  ]
  node [
    id 38
    label "tu&#322;&#243;w"
  ]
  node [
    id 39
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 40
    label "skejt"
  ]
  node [
    id 41
    label "step"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 3
    target 3
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
]
