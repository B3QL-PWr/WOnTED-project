graph [
  maxDegree 2
  minDegree 1
  meanDegree 1.3333333333333333
  density 0.6666666666666666
  graphCliqueNumber 2
  node [
    id 0
    label "wzi&#261;&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "siebie"
    origin "text"
  ]
  node [
    id 2
    label "ubuntu"
    origin "text"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
]
