graph [
  maxDegree 34
  minDegree 1
  meanDegree 2
  density 0.01834862385321101
  graphCliqueNumber 2
  node [
    id 0
    label "mireczka"
    origin "text"
  ]
  node [
    id 1
    label "wed&#322;ug"
    origin "text"
  ]
  node [
    id 2
    label "taki"
    origin "text"
  ]
  node [
    id 3
    label "kozak"
    origin "text"
  ]
  node [
    id 4
    label "podchodzi&#263;"
    origin "text"
  ]
  node [
    id 5
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 6
    label "pod"
    origin "text"
  ]
  node [
    id 7
    label "kategoria"
    origin "text"
  ]
  node [
    id 8
    label "pani"
    origin "text"
  ]
  node [
    id 9
    label "lekki"
    origin "text"
  ]
  node [
    id 10
    label "obyczaj"
    origin "text"
  ]
  node [
    id 11
    label "okre&#347;lony"
  ]
  node [
    id 12
    label "jaki&#347;"
  ]
  node [
    id 13
    label "ko&#378;larz"
  ]
  node [
    id 14
    label "cz&#322;owiek"
  ]
  node [
    id 15
    label "melodia"
  ]
  node [
    id 16
    label "taniec_ludowy"
  ]
  node [
    id 17
    label "popisywa&#263;_si&#281;"
  ]
  node [
    id 18
    label "but"
  ]
  node [
    id 19
    label "kure&#324;"
  ]
  node [
    id 20
    label "sotnia"
  ]
  node [
    id 21
    label "taniec"
  ]
  node [
    id 22
    label "rzecz"
  ]
  node [
    id 23
    label "ko&#378;larz_babka"
  ]
  node [
    id 24
    label "Kozak"
  ]
  node [
    id 25
    label "kawalerzysta"
  ]
  node [
    id 26
    label "ludowy"
  ]
  node [
    id 27
    label "prysiudy"
  ]
  node [
    id 28
    label "&#347;mia&#322;ek"
  ]
  node [
    id 29
    label "postrzeleniec"
  ]
  node [
    id 30
    label "przyst&#281;powa&#263;"
  ]
  node [
    id 31
    label "przygotowywa&#263;_si&#281;"
  ]
  node [
    id 32
    label "approach"
  ]
  node [
    id 33
    label "sprawdzian"
  ]
  node [
    id 34
    label "set_about"
  ]
  node [
    id 35
    label "traktowa&#263;"
  ]
  node [
    id 36
    label "zalicza&#263;_si&#281;"
  ]
  node [
    id 37
    label "zmienia&#263;_pozycj&#281;"
  ]
  node [
    id 38
    label "wspina&#263;_si&#281;"
  ]
  node [
    id 39
    label "nasyca&#263;_si&#281;"
  ]
  node [
    id 40
    label "wype&#322;nia&#263;"
  ]
  node [
    id 41
    label "dochodzi&#263;"
  ]
  node [
    id 42
    label "dociera&#263;"
  ]
  node [
    id 43
    label "oszukiwa&#263;"
  ]
  node [
    id 44
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 45
    label "odpowiada&#263;"
  ]
  node [
    id 46
    label "ciecz"
  ]
  node [
    id 47
    label "forma"
  ]
  node [
    id 48
    label "wytw&#243;r"
  ]
  node [
    id 49
    label "type"
  ]
  node [
    id 50
    label "teoria"
  ]
  node [
    id 51
    label "zbi&#243;r"
  ]
  node [
    id 52
    label "poj&#281;cie"
  ]
  node [
    id 53
    label "klasa"
  ]
  node [
    id 54
    label "przekwitanie"
  ]
  node [
    id 55
    label "zwrot"
  ]
  node [
    id 56
    label "uleganie"
  ]
  node [
    id 57
    label "ulega&#263;"
  ]
  node [
    id 58
    label "partner"
  ]
  node [
    id 59
    label "doros&#322;y"
  ]
  node [
    id 60
    label "przyw&#243;dczyni"
  ]
  node [
    id 61
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 62
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 63
    label "ulec"
  ]
  node [
    id 64
    label "kobita"
  ]
  node [
    id 65
    label "&#322;ono"
  ]
  node [
    id 66
    label "kobieta"
  ]
  node [
    id 67
    label "w&#322;a&#347;cicielka"
  ]
  node [
    id 68
    label "m&#281;&#380;yna"
  ]
  node [
    id 69
    label "babka"
  ]
  node [
    id 70
    label "samica"
  ]
  node [
    id 71
    label "pa&#324;stwo"
  ]
  node [
    id 72
    label "ulegni&#281;cie"
  ]
  node [
    id 73
    label "menopauza"
  ]
  node [
    id 74
    label "letki"
  ]
  node [
    id 75
    label "nieznacznie"
  ]
  node [
    id 76
    label "subtelny"
  ]
  node [
    id 77
    label "prosty"
  ]
  node [
    id 78
    label "piaszczysty"
  ]
  node [
    id 79
    label "przyswajalny"
  ]
  node [
    id 80
    label "dietetyczny"
  ]
  node [
    id 81
    label "g&#322;adko"
  ]
  node [
    id 82
    label "bezpieczny"
  ]
  node [
    id 83
    label "delikatny"
  ]
  node [
    id 84
    label "p&#322;ynny"
  ]
  node [
    id 85
    label "s&#322;aby"
  ]
  node [
    id 86
    label "przyjemny"
  ]
  node [
    id 87
    label "zr&#281;czny"
  ]
  node [
    id 88
    label "nierozwa&#380;ny"
  ]
  node [
    id 89
    label "snadny"
  ]
  node [
    id 90
    label "nieg&#322;&#281;boko"
  ]
  node [
    id 91
    label "&#322;atwo"
  ]
  node [
    id 92
    label "&#322;atwy"
  ]
  node [
    id 93
    label "polotny"
  ]
  node [
    id 94
    label "cienki"
  ]
  node [
    id 95
    label "beztroskliwy"
  ]
  node [
    id 96
    label "beztrosko"
  ]
  node [
    id 97
    label "lekko"
  ]
  node [
    id 98
    label "ubogi"
  ]
  node [
    id 99
    label "zgrabny"
  ]
  node [
    id 100
    label "przewiewny"
  ]
  node [
    id 101
    label "suchy"
  ]
  node [
    id 102
    label "lekkozbrojny"
  ]
  node [
    id 103
    label "delikatnie"
  ]
  node [
    id 104
    label "&#322;acny"
  ]
  node [
    id 105
    label "zwinnie"
  ]
  node [
    id 106
    label "kultura"
  ]
  node [
    id 107
    label "obyczajowo&#347;&#263;"
  ]
  node [
    id 108
    label "kultura_duchowa"
  ]
  node [
    id 109
    label "ceremony"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 14
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 9
    target 100
  ]
  edge [
    source 9
    target 101
  ]
  edge [
    source 9
    target 102
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 104
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 10
    target 106
  ]
  edge [
    source 10
    target 107
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 109
  ]
]
