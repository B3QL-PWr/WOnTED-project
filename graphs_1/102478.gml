graph [
  maxDegree 24
  minDegree 1
  meanDegree 1.9607843137254901
  density 0.0392156862745098
  graphCliqueNumber 2
  node [
    id 0
    label "ma&#322;gorzata"
    origin "text"
  ]
  node [
    id 1
    label "ostrowska"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "gwiazda"
    origin "text"
  ]
  node [
    id 4
    label "tegoroczny"
    origin "text"
  ]
  node [
    id 5
    label "dni"
    origin "text"
  ]
  node [
    id 6
    label "ozorkowy"
    origin "text"
  ]
  node [
    id 7
    label "artystka"
    origin "text"
  ]
  node [
    id 8
    label "zawity"
    origin "text"
  ]
  node [
    id 9
    label "nasa"
    origin "text"
  ]
  node [
    id 10
    label "maj"
    origin "text"
  ]
  node [
    id 11
    label "si&#281;ga&#263;"
  ]
  node [
    id 12
    label "trwa&#263;"
  ]
  node [
    id 13
    label "obecno&#347;&#263;"
  ]
  node [
    id 14
    label "stan"
  ]
  node [
    id 15
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 16
    label "stand"
  ]
  node [
    id 17
    label "mie&#263;_miejsce"
  ]
  node [
    id 18
    label "uczestniczy&#263;"
  ]
  node [
    id 19
    label "chodzi&#263;"
  ]
  node [
    id 20
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 21
    label "equal"
  ]
  node [
    id 22
    label "S&#322;o&#324;ce"
  ]
  node [
    id 23
    label "uk&#322;ad_gwiazd"
  ]
  node [
    id 24
    label "Arktur"
  ]
  node [
    id 25
    label "star"
  ]
  node [
    id 26
    label "delta_Scuti"
  ]
  node [
    id 27
    label "agregatka"
  ]
  node [
    id 28
    label "s&#322;awa"
  ]
  node [
    id 29
    label "zlot_gwia&#378;dzisty"
  ]
  node [
    id 30
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 31
    label "gwiazdosz"
  ]
  node [
    id 32
    label "&#347;wiat&#322;o"
  ]
  node [
    id 33
    label "Nibiru"
  ]
  node [
    id 34
    label "ornament"
  ]
  node [
    id 35
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 36
    label "Gwiazda_Polarna"
  ]
  node [
    id 37
    label "kszta&#322;t"
  ]
  node [
    id 38
    label "supergrupa"
  ]
  node [
    id 39
    label "gromada"
  ]
  node [
    id 40
    label "obiekt"
  ]
  node [
    id 41
    label "promie&#324;"
  ]
  node [
    id 42
    label "konstelacja"
  ]
  node [
    id 43
    label "asocjacja_gwiazd"
  ]
  node [
    id 44
    label "tegorocznie"
  ]
  node [
    id 45
    label "bie&#380;&#261;cy"
  ]
  node [
    id 46
    label "czas"
  ]
  node [
    id 47
    label "mi&#281;sny"
  ]
  node [
    id 48
    label "cz&#322;owiek"
  ]
  node [
    id 49
    label "&#346;wi&#281;to_Pracy"
  ]
  node [
    id 50
    label "miesi&#261;c"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 49
  ]
  edge [
    source 10
    target 50
  ]
]
