graph [
  maxDegree 29
  minDegree 1
  meanDegree 1.9615384615384615
  density 0.038461538461538464
  graphCliqueNumber 2
  node [
    id 0
    label "farmio"
    origin "text"
  ]
  node [
    id 1
    label "firma"
    origin "text"
  ]
  node [
    id 2
    label "znana"
    origin "text"
  ]
  node [
    id 3
    label "sprzeda&#380;"
    origin "text"
  ]
  node [
    id 4
    label "jajko"
    origin "text"
  ]
  node [
    id 5
    label "cz&#322;owiek"
  ]
  node [
    id 6
    label "Hortex"
  ]
  node [
    id 7
    label "MAC"
  ]
  node [
    id 8
    label "reengineering"
  ]
  node [
    id 9
    label "nazwa_w&#322;asna"
  ]
  node [
    id 10
    label "podmiot_gospodarczy"
  ]
  node [
    id 11
    label "Google"
  ]
  node [
    id 12
    label "zaufanie"
  ]
  node [
    id 13
    label "biurowiec"
  ]
  node [
    id 14
    label "networking"
  ]
  node [
    id 15
    label "zasoby_ludzkie"
  ]
  node [
    id 16
    label "interes"
  ]
  node [
    id 17
    label "paczkarnia"
  ]
  node [
    id 18
    label "Canon"
  ]
  node [
    id 19
    label "HP"
  ]
  node [
    id 20
    label "Baltona"
  ]
  node [
    id 21
    label "Pewex"
  ]
  node [
    id 22
    label "MAN_SE"
  ]
  node [
    id 23
    label "Apeks"
  ]
  node [
    id 24
    label "zasoby"
  ]
  node [
    id 25
    label "Orbis"
  ]
  node [
    id 26
    label "miejsce_pracy"
  ]
  node [
    id 27
    label "siedziba"
  ]
  node [
    id 28
    label "Spo&#322;em"
  ]
  node [
    id 29
    label "przedsi&#281;biorczo&#347;&#263;"
  ]
  node [
    id 30
    label "Orlen"
  ]
  node [
    id 31
    label "klasa"
  ]
  node [
    id 32
    label "transakcja"
  ]
  node [
    id 33
    label "sprzedaj&#261;cy"
  ]
  node [
    id 34
    label "przeniesienie_praw"
  ]
  node [
    id 35
    label "rabat"
  ]
  node [
    id 36
    label "przeda&#380;"
  ]
  node [
    id 37
    label "pisanka"
  ]
  node [
    id 38
    label "bia&#322;ko"
  ]
  node [
    id 39
    label "produkt"
  ]
  node [
    id 40
    label "skorupka"
  ]
  node [
    id 41
    label "owoskop"
  ]
  node [
    id 42
    label "&#380;&#243;&#322;tko"
  ]
  node [
    id 43
    label "zniesienie"
  ]
  node [
    id 44
    label "jajo"
  ]
  node [
    id 45
    label "nabia&#322;"
  ]
  node [
    id 46
    label "wyt&#322;aczanka"
  ]
  node [
    id 47
    label "znoszenie"
  ]
  node [
    id 48
    label "rozbijarka"
  ]
  node [
    id 49
    label "ball"
  ]
  node [
    id 50
    label "kszta&#322;t"
  ]
  node [
    id 51
    label "ryboflawina"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
]
