graph [
  maxDegree 39
  minDegree 1
  meanDegree 2.2413793103448274
  density 0.004302071612945926
  graphCliqueNumber 4
  node [
    id 0
    label "zesp&#243;&#322;"
    origin "text"
  ]
  node [
    id 1
    label "orchid"
    origin "text"
  ]
  node [
    id 2
    label "gra"
    origin "text"
  ]
  node [
    id 3
    label "alternatywny"
    origin "text"
  ]
  node [
    id 4
    label "pop"
    origin "text"
  ]
  node [
    id 5
    label "rock"
    origin "text"
  ]
  node [
    id 6
    label "u&#380;ycie"
    origin "text"
  ]
  node [
    id 7
    label "gitara"
    origin "text"
  ]
  node [
    id 8
    label "perkusja"
    origin "text"
  ]
  node [
    id 9
    label "ma&#322;o"
    origin "text"
  ]
  node [
    id 10
    label "skomplikowany"
    origin "text"
  ]
  node [
    id 11
    label "modu&#322;"
    origin "text"
  ]
  node [
    id 12
    label "klawiszowy"
    origin "text"
  ]
  node [
    id 13
    label "cz&#322;onek"
    origin "text"
  ]
  node [
    id 14
    label "by&#263;"
    origin "text"
  ]
  node [
    id 15
    label "m&#322;ody"
    origin "text"
  ]
  node [
    id 16
    label "proporcja"
    origin "text"
  ]
  node [
    id 17
    label "p&#322;ciowy"
    origin "text"
  ]
  node [
    id 18
    label "uk&#322;ada&#263;"
    origin "text"
  ]
  node [
    id 19
    label "si&#281;"
    origin "text"
  ]
  node [
    id 20
    label "jaki&#347;"
    origin "text"
  ]
  node [
    id 21
    label "czas"
    origin "text"
  ]
  node [
    id 22
    label "temu"
    origin "text"
  ]
  node [
    id 23
    label "korespondowa&#263;by&#263;"
    origin "text"
  ]
  node [
    id 24
    label "temat"
    origin "text"
  ]
  node [
    id 25
    label "u&#380;ywanie"
    origin "text"
  ]
  node [
    id 26
    label "licencja"
    origin "text"
  ]
  node [
    id 27
    label "epka"
    origin "text"
  ]
  node [
    id 28
    label "swobodnie"
    origin "text"
  ]
  node [
    id 29
    label "dost&#281;pny"
    origin "text"
  ]
  node [
    id 30
    label "creative"
    origin "text"
  ]
  node [
    id 31
    label "commons"
    origin "text"
  ]
  node [
    id 32
    label "serwis"
    origin "text"
  ]
  node [
    id 33
    label "jamendo"
    origin "text"
  ]
  node [
    id 34
    label "przy"
    origin "text"
  ]
  node [
    id 35
    label "okazja"
    origin "text"
  ]
  node [
    id 36
    label "pos&#322;ucha&#263;by&#263;"
    origin "text"
  ]
  node [
    id 37
    label "piosenka"
    origin "text"
  ]
  node [
    id 38
    label "spodoba&#263;"
    origin "text"
  ]
  node [
    id 39
    label "wyra&#378;ny"
    origin "text"
  ]
  node [
    id 40
    label "lekko&#347;&#263;"
    origin "text"
  ]
  node [
    id 41
    label "jaka"
    origin "text"
  ]
  node [
    id 42
    label "ca&#322;o&#347;&#263;"
    origin "text"
  ]
  node [
    id 43
    label "dzieje"
    origin "text"
  ]
  node [
    id 44
    label "dla"
    origin "text"
  ]
  node [
    id 45
    label "oznacza&#263;"
    origin "text"
  ]
  node [
    id 46
    label "dobry"
    origin "text"
  ]
  node [
    id 47
    label "s&#322;owo"
    origin "text"
  ]
  node [
    id 48
    label "znaczenie"
    origin "text"
  ]
  node [
    id 49
    label "teraz"
    origin "text"
  ]
  node [
    id 50
    label "zakwalifikowa&#263;"
    origin "text"
  ]
  node [
    id 51
    label "fina&#322;"
    origin "text"
  ]
  node [
    id 52
    label "festiwal"
    origin "text"
  ]
  node [
    id 53
    label "vena"
    origin "text"
  ]
  node [
    id 54
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 55
    label "nagroda"
    origin "text"
  ]
  node [
    id 56
    label "z&#322;oty"
    origin "text"
  ]
  node [
    id 57
    label "zach&#281;ca&#263;"
    origin "text"
  ]
  node [
    id 58
    label "wszyscy"
    origin "text"
  ]
  node [
    id 59
    label "g&#322;osowanie"
    origin "text"
  ]
  node [
    id 60
    label "conajmniej"
    origin "text"
  ]
  node [
    id 61
    label "dwa"
    origin "text"
  ]
  node [
    id 62
    label "pow&#243;d"
    origin "text"
  ]
  node [
    id 63
    label "muzyk"
    origin "text"
  ]
  node [
    id 64
    label "bardzo"
    origin "text"
  ]
  node [
    id 65
    label "dobra"
    origin "text"
  ]
  node [
    id 66
    label "warto"
    origin "text"
  ]
  node [
    id 67
    label "wspiera&#263;"
    origin "text"
  ]
  node [
    id 68
    label "artysta"
    origin "text"
  ]
  node [
    id 69
    label "korzysta&#263;"
    origin "text"
  ]
  node [
    id 70
    label "jeden"
    origin "text"
  ]
  node [
    id 71
    label "cz&#322;onkini"
    origin "text"
  ]
  node [
    id 72
    label "zapewnia&#263;"
    origin "text"
  ]
  node [
    id 73
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 74
    label "whole"
  ]
  node [
    id 75
    label "odm&#322;adza&#263;"
  ]
  node [
    id 76
    label "zabudowania"
  ]
  node [
    id 77
    label "odm&#322;odzenie"
  ]
  node [
    id 78
    label "zespolik"
  ]
  node [
    id 79
    label "skupienie"
  ]
  node [
    id 80
    label "schorzenie"
  ]
  node [
    id 81
    label "grupa"
  ]
  node [
    id 82
    label "Depeche_Mode"
  ]
  node [
    id 83
    label "Mazowsze"
  ]
  node [
    id 84
    label "ro&#347;lina"
  ]
  node [
    id 85
    label "zbi&#243;r"
  ]
  node [
    id 86
    label "The_Beatles"
  ]
  node [
    id 87
    label "group"
  ]
  node [
    id 88
    label "&#346;wietliki"
  ]
  node [
    id 89
    label "odm&#322;adzanie"
  ]
  node [
    id 90
    label "batch"
  ]
  node [
    id 91
    label "zabawa"
  ]
  node [
    id 92
    label "rywalizacja"
  ]
  node [
    id 93
    label "czynno&#347;&#263;"
  ]
  node [
    id 94
    label "Pok&#233;mon"
  ]
  node [
    id 95
    label "synteza"
  ]
  node [
    id 96
    label "odtworzenie"
  ]
  node [
    id 97
    label "komplet"
  ]
  node [
    id 98
    label "rekwizyt_do_gry"
  ]
  node [
    id 99
    label "odg&#322;os"
  ]
  node [
    id 100
    label "rozgrywka"
  ]
  node [
    id 101
    label "post&#281;powanie"
  ]
  node [
    id 102
    label "wydarzenie"
  ]
  node [
    id 103
    label "apparent_motion"
  ]
  node [
    id 104
    label "game"
  ]
  node [
    id 105
    label "zmienno&#347;&#263;"
  ]
  node [
    id 106
    label "zasada"
  ]
  node [
    id 107
    label "akcja"
  ]
  node [
    id 108
    label "play"
  ]
  node [
    id 109
    label "contest"
  ]
  node [
    id 110
    label "zbijany"
  ]
  node [
    id 111
    label "inny"
  ]
  node [
    id 112
    label "dwojaki"
  ]
  node [
    id 113
    label "alternatywnie"
  ]
  node [
    id 114
    label "niekonwencjonalny"
  ]
  node [
    id 115
    label "undergroundowy"
  ]
  node [
    id 116
    label "Ko&#347;ci&#243;&#322;_greckokatolicki"
  ]
  node [
    id 117
    label "muzyka_rozrywkowa"
  ]
  node [
    id 118
    label "duchowny"
  ]
  node [
    id 119
    label "boysband"
  ]
  node [
    id 120
    label "girlsband"
  ]
  node [
    id 121
    label "Ko&#347;ci&#243;&#322;_prawos&#322;awny"
  ]
  node [
    id 122
    label "batiuszka"
  ]
  node [
    id 123
    label "riff"
  ]
  node [
    id 124
    label "use"
  ]
  node [
    id 125
    label "zrobienie"
  ]
  node [
    id 126
    label "doznanie"
  ]
  node [
    id 127
    label "stosowanie"
  ]
  node [
    id 128
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 129
    label "nau&#380;ywanie_si&#281;"
  ]
  node [
    id 130
    label "enjoyment"
  ]
  node [
    id 131
    label "u&#380;yteczny"
  ]
  node [
    id 132
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 133
    label "palc&#243;wka"
  ]
  node [
    id 134
    label "chordofon_szarpany"
  ]
  node [
    id 135
    label "fajnie"
  ]
  node [
    id 136
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 137
    label "drum"
  ]
  node [
    id 138
    label "instrument_perkusyjny"
  ]
  node [
    id 139
    label "mo&#380;liwie"
  ]
  node [
    id 140
    label "nieznaczny"
  ]
  node [
    id 141
    label "kr&#243;tko"
  ]
  node [
    id 142
    label "nieistotnie"
  ]
  node [
    id 143
    label "nieliczny"
  ]
  node [
    id 144
    label "mikroskopijnie"
  ]
  node [
    id 145
    label "pomiernie"
  ]
  node [
    id 146
    label "ma&#322;y"
  ]
  node [
    id 147
    label "skomplikowanie"
  ]
  node [
    id 148
    label "trudny"
  ]
  node [
    id 149
    label "module"
  ]
  node [
    id 150
    label "struktura_algebraiczna"
  ]
  node [
    id 151
    label "plik"
  ]
  node [
    id 152
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 153
    label "element"
  ]
  node [
    id 154
    label "cz&#322;owiek"
  ]
  node [
    id 155
    label "cia&#322;o"
  ]
  node [
    id 156
    label "organizacja"
  ]
  node [
    id 157
    label "przedstawiciel"
  ]
  node [
    id 158
    label "shaft"
  ]
  node [
    id 159
    label "podmiot"
  ]
  node [
    id 160
    label "fiut"
  ]
  node [
    id 161
    label "przyrodzenie"
  ]
  node [
    id 162
    label "wchodzenie"
  ]
  node [
    id 163
    label "ptaszek"
  ]
  node [
    id 164
    label "organ"
  ]
  node [
    id 165
    label "wej&#347;cie"
  ]
  node [
    id 166
    label "narz&#261;d_kopulacyjny"
  ]
  node [
    id 167
    label "element_anatomiczny"
  ]
  node [
    id 168
    label "si&#281;ga&#263;"
  ]
  node [
    id 169
    label "trwa&#263;"
  ]
  node [
    id 170
    label "obecno&#347;&#263;"
  ]
  node [
    id 171
    label "stan"
  ]
  node [
    id 172
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 173
    label "stand"
  ]
  node [
    id 174
    label "mie&#263;_miejsce"
  ]
  node [
    id 175
    label "uczestniczy&#263;"
  ]
  node [
    id 176
    label "chodzi&#263;"
  ]
  node [
    id 177
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 178
    label "equal"
  ]
  node [
    id 179
    label "nie&#380;onaty"
  ]
  node [
    id 180
    label "wczesny"
  ]
  node [
    id 181
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 182
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 183
    label "charakterystyczny"
  ]
  node [
    id 184
    label "nowo&#380;eniec"
  ]
  node [
    id 185
    label "m&#261;&#380;"
  ]
  node [
    id 186
    label "m&#322;odo"
  ]
  node [
    id 187
    label "nowy"
  ]
  node [
    id 188
    label "stosunek"
  ]
  node [
    id 189
    label "relationship"
  ]
  node [
    id 190
    label "cecha"
  ]
  node [
    id 191
    label "porz&#261;dek"
  ]
  node [
    id 192
    label "ilo&#347;&#263;"
  ]
  node [
    id 193
    label "wyraz_skrajny"
  ]
  node [
    id 194
    label "iloraz"
  ]
  node [
    id 195
    label "seksualny"
  ]
  node [
    id 196
    label "erotyczny"
  ]
  node [
    id 197
    label "p&#322;ciowo"
  ]
  node [
    id 198
    label "seksualnie"
  ]
  node [
    id 199
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 200
    label "zbiera&#263;"
  ]
  node [
    id 201
    label "kszta&#322;ci&#263;"
  ]
  node [
    id 202
    label "zmienia&#263;_pozycj&#281;"
  ]
  node [
    id 203
    label "przygotowywa&#263;"
  ]
  node [
    id 204
    label "umieszcza&#263;"
  ]
  node [
    id 205
    label "digest"
  ]
  node [
    id 206
    label "treser"
  ]
  node [
    id 207
    label "uczy&#263;"
  ]
  node [
    id 208
    label "tworzy&#263;"
  ]
  node [
    id 209
    label "porz&#261;dkowa&#263;"
  ]
  node [
    id 210
    label "dispose"
  ]
  node [
    id 211
    label "raise"
  ]
  node [
    id 212
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 213
    label "jako&#347;"
  ]
  node [
    id 214
    label "ciekawy"
  ]
  node [
    id 215
    label "jako_tako"
  ]
  node [
    id 216
    label "dziwny"
  ]
  node [
    id 217
    label "niez&#322;y"
  ]
  node [
    id 218
    label "przyzwoity"
  ]
  node [
    id 219
    label "czasokres"
  ]
  node [
    id 220
    label "trawienie"
  ]
  node [
    id 221
    label "kategoria_gramatyczna"
  ]
  node [
    id 222
    label "period"
  ]
  node [
    id 223
    label "odczyt"
  ]
  node [
    id 224
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 225
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 226
    label "chwila"
  ]
  node [
    id 227
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 228
    label "poprzedzenie"
  ]
  node [
    id 229
    label "koniugacja"
  ]
  node [
    id 230
    label "poprzedzi&#263;"
  ]
  node [
    id 231
    label "przep&#322;ywanie"
  ]
  node [
    id 232
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 233
    label "odwlekanie_si&#281;"
  ]
  node [
    id 234
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 235
    label "Zeitgeist"
  ]
  node [
    id 236
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 237
    label "okres_czasu"
  ]
  node [
    id 238
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 239
    label "pochodzi&#263;"
  ]
  node [
    id 240
    label "schy&#322;ek"
  ]
  node [
    id 241
    label "czwarty_wymiar"
  ]
  node [
    id 242
    label "chronometria"
  ]
  node [
    id 243
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 244
    label "poprzedzanie"
  ]
  node [
    id 245
    label "pogoda"
  ]
  node [
    id 246
    label "zegar"
  ]
  node [
    id 247
    label "pochodzenie"
  ]
  node [
    id 248
    label "poprzedza&#263;"
  ]
  node [
    id 249
    label "trawi&#263;"
  ]
  node [
    id 250
    label "time_period"
  ]
  node [
    id 251
    label "rachuba_czasu"
  ]
  node [
    id 252
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 253
    label "czasoprzestrze&#324;"
  ]
  node [
    id 254
    label "laba"
  ]
  node [
    id 255
    label "fraza"
  ]
  node [
    id 256
    label "forma"
  ]
  node [
    id 257
    label "melodia"
  ]
  node [
    id 258
    label "rzecz"
  ]
  node [
    id 259
    label "zbacza&#263;"
  ]
  node [
    id 260
    label "entity"
  ]
  node [
    id 261
    label "omawia&#263;"
  ]
  node [
    id 262
    label "topik"
  ]
  node [
    id 263
    label "wyraz_pochodny"
  ]
  node [
    id 264
    label "om&#243;wi&#263;"
  ]
  node [
    id 265
    label "omawianie"
  ]
  node [
    id 266
    label "w&#261;tek"
  ]
  node [
    id 267
    label "forum"
  ]
  node [
    id 268
    label "zboczenie"
  ]
  node [
    id 269
    label "zbaczanie"
  ]
  node [
    id 270
    label "tre&#347;&#263;"
  ]
  node [
    id 271
    label "tematyka"
  ]
  node [
    id 272
    label "sprawa"
  ]
  node [
    id 273
    label "istota"
  ]
  node [
    id 274
    label "otoczka"
  ]
  node [
    id 275
    label "zboczy&#263;"
  ]
  node [
    id 276
    label "om&#243;wienie"
  ]
  node [
    id 277
    label "robienie"
  ]
  node [
    id 278
    label "exercise"
  ]
  node [
    id 279
    label "zniszczenie"
  ]
  node [
    id 280
    label "przejaskrawianie"
  ]
  node [
    id 281
    label "zu&#380;ywanie"
  ]
  node [
    id 282
    label "relish"
  ]
  node [
    id 283
    label "zaznawanie"
  ]
  node [
    id 284
    label "prawo"
  ]
  node [
    id 285
    label "licencjonowa&#263;"
  ]
  node [
    id 286
    label "pozwolenie"
  ]
  node [
    id 287
    label "hodowla"
  ]
  node [
    id 288
    label "rasowy"
  ]
  node [
    id 289
    label "license"
  ]
  node [
    id 290
    label "zezwolenie"
  ]
  node [
    id 291
    label "za&#347;wiadczenie"
  ]
  node [
    id 292
    label "mini-album"
  ]
  node [
    id 293
    label "album"
  ]
  node [
    id 294
    label "wolny"
  ]
  node [
    id 295
    label "lu&#378;no"
  ]
  node [
    id 296
    label "naturalnie"
  ]
  node [
    id 297
    label "swobodny"
  ]
  node [
    id 298
    label "wolnie"
  ]
  node [
    id 299
    label "dowolnie"
  ]
  node [
    id 300
    label "&#322;atwy"
  ]
  node [
    id 301
    label "mo&#380;liwy"
  ]
  node [
    id 302
    label "dost&#281;pnie"
  ]
  node [
    id 303
    label "ods&#322;oni&#281;cie_si&#281;"
  ]
  node [
    id 304
    label "przyst&#281;pnie"
  ]
  node [
    id 305
    label "zrozumia&#322;y"
  ]
  node [
    id 306
    label "odblokowywanie_si&#281;"
  ]
  node [
    id 307
    label "odblokowanie_si&#281;"
  ]
  node [
    id 308
    label "ods&#322;anianie_si&#281;"
  ]
  node [
    id 309
    label "mecz"
  ]
  node [
    id 310
    label "service"
  ]
  node [
    id 311
    label "wytw&#243;r"
  ]
  node [
    id 312
    label "zak&#322;ad"
  ]
  node [
    id 313
    label "us&#322;uga"
  ]
  node [
    id 314
    label "uderzenie"
  ]
  node [
    id 315
    label "doniesienie"
  ]
  node [
    id 316
    label "zastawa"
  ]
  node [
    id 317
    label "YouTube"
  ]
  node [
    id 318
    label "punkt"
  ]
  node [
    id 319
    label "porcja"
  ]
  node [
    id 320
    label "strona"
  ]
  node [
    id 321
    label "atrakcyjny"
  ]
  node [
    id 322
    label "oferta"
  ]
  node [
    id 323
    label "adeptness"
  ]
  node [
    id 324
    label "okazka"
  ]
  node [
    id 325
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 326
    label "podw&#243;zka"
  ]
  node [
    id 327
    label "autostop"
  ]
  node [
    id 328
    label "sytuacja"
  ]
  node [
    id 329
    label "zwrotka"
  ]
  node [
    id 330
    label "nuci&#263;"
  ]
  node [
    id 331
    label "zanuci&#263;"
  ]
  node [
    id 332
    label "utw&#243;r"
  ]
  node [
    id 333
    label "zanucenie"
  ]
  node [
    id 334
    label "nucenie"
  ]
  node [
    id 335
    label "tekst"
  ]
  node [
    id 336
    label "piosnka"
  ]
  node [
    id 337
    label "nieoboj&#281;tny"
  ]
  node [
    id 338
    label "zauwa&#380;alny"
  ]
  node [
    id 339
    label "wyra&#378;nie"
  ]
  node [
    id 340
    label "zdecydowany"
  ]
  node [
    id 341
    label "dietetyczno&#347;&#263;"
  ]
  node [
    id 342
    label "kaftan"
  ]
  node [
    id 343
    label "uk&#322;ad"
  ]
  node [
    id 344
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 345
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 346
    label "integer"
  ]
  node [
    id 347
    label "liczba"
  ]
  node [
    id 348
    label "pe&#322;ny"
  ]
  node [
    id 349
    label "zlewanie_si&#281;"
  ]
  node [
    id 350
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 351
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 352
    label "epoka"
  ]
  node [
    id 353
    label "okre&#347;la&#263;"
  ]
  node [
    id 354
    label "represent"
  ]
  node [
    id 355
    label "wyraz"
  ]
  node [
    id 356
    label "wskazywa&#263;"
  ]
  node [
    id 357
    label "stanowi&#263;"
  ]
  node [
    id 358
    label "signify"
  ]
  node [
    id 359
    label "set"
  ]
  node [
    id 360
    label "ustala&#263;"
  ]
  node [
    id 361
    label "pomy&#347;lny"
  ]
  node [
    id 362
    label "skuteczny"
  ]
  node [
    id 363
    label "moralny"
  ]
  node [
    id 364
    label "korzystny"
  ]
  node [
    id 365
    label "odpowiedni"
  ]
  node [
    id 366
    label "zwrot"
  ]
  node [
    id 367
    label "dobrze"
  ]
  node [
    id 368
    label "pozytywny"
  ]
  node [
    id 369
    label "grzeczny"
  ]
  node [
    id 370
    label "powitanie"
  ]
  node [
    id 371
    label "mi&#322;y"
  ]
  node [
    id 372
    label "dobroczynny"
  ]
  node [
    id 373
    label "pos&#322;uszny"
  ]
  node [
    id 374
    label "ca&#322;y"
  ]
  node [
    id 375
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 376
    label "czw&#243;rka"
  ]
  node [
    id 377
    label "spokojny"
  ]
  node [
    id 378
    label "&#347;mieszny"
  ]
  node [
    id 379
    label "drogi"
  ]
  node [
    id 380
    label "obietnica"
  ]
  node [
    id 381
    label "bit"
  ]
  node [
    id 382
    label "s&#322;ownictwo"
  ]
  node [
    id 383
    label "jednostka_leksykalna"
  ]
  node [
    id 384
    label "pisanie_si&#281;"
  ]
  node [
    id 385
    label "wykrzyknik"
  ]
  node [
    id 386
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 387
    label "pole_semantyczne"
  ]
  node [
    id 388
    label "&#347;r&#243;dg&#322;os"
  ]
  node [
    id 389
    label "komunikat"
  ]
  node [
    id 390
    label "pisa&#263;_si&#281;"
  ]
  node [
    id 391
    label "wypowiedzenie"
  ]
  node [
    id 392
    label "nag&#322;os"
  ]
  node [
    id 393
    label "wordnet"
  ]
  node [
    id 394
    label "morfem"
  ]
  node [
    id 395
    label "czasownik"
  ]
  node [
    id 396
    label "wyg&#322;os"
  ]
  node [
    id 397
    label "jednostka_informacji"
  ]
  node [
    id 398
    label "gravity"
  ]
  node [
    id 399
    label "okre&#347;lanie"
  ]
  node [
    id 400
    label "liczenie"
  ]
  node [
    id 401
    label "odgrywanie_roli"
  ]
  node [
    id 402
    label "wskazywanie"
  ]
  node [
    id 403
    label "bycie"
  ]
  node [
    id 404
    label "weight"
  ]
  node [
    id 405
    label "command"
  ]
  node [
    id 406
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 407
    label "informacja"
  ]
  node [
    id 408
    label "odk&#322;adanie"
  ]
  node [
    id 409
    label "wyra&#380;enie"
  ]
  node [
    id 410
    label "assay"
  ]
  node [
    id 411
    label "condition"
  ]
  node [
    id 412
    label "kto&#347;"
  ]
  node [
    id 413
    label "stawianie"
  ]
  node [
    id 414
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 415
    label "score"
  ]
  node [
    id 416
    label "wydzieli&#263;"
  ]
  node [
    id 417
    label "przyporz&#261;dkowa&#263;"
  ]
  node [
    id 418
    label "policzy&#263;"
  ]
  node [
    id 419
    label "stwierdzi&#263;"
  ]
  node [
    id 420
    label "pigeonhole"
  ]
  node [
    id 421
    label "koniec"
  ]
  node [
    id 422
    label "conclusion"
  ]
  node [
    id 423
    label "coating"
  ]
  node [
    id 424
    label "runda"
  ]
  node [
    id 425
    label "Nowe_Horyzonty"
  ]
  node [
    id 426
    label "Interwizja"
  ]
  node [
    id 427
    label "Open'er"
  ]
  node [
    id 428
    label "Przystanek_Woodstock"
  ]
  node [
    id 429
    label "impreza"
  ]
  node [
    id 430
    label "Woodstock"
  ]
  node [
    id 431
    label "Metalmania"
  ]
  node [
    id 432
    label "Opole"
  ]
  node [
    id 433
    label "FAMA"
  ]
  node [
    id 434
    label "Eurowizja"
  ]
  node [
    id 435
    label "Brutal"
  ]
  node [
    id 436
    label "return"
  ]
  node [
    id 437
    label "konsekwencja"
  ]
  node [
    id 438
    label "oskar"
  ]
  node [
    id 439
    label "wyr&#243;&#380;nienie"
  ]
  node [
    id 440
    label "szlachetny"
  ]
  node [
    id 441
    label "metaliczny"
  ]
  node [
    id 442
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 443
    label "z&#322;oci&#347;cie"
  ]
  node [
    id 444
    label "grosz"
  ]
  node [
    id 445
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 446
    label "utytu&#322;owany"
  ]
  node [
    id 447
    label "poz&#322;ocenie"
  ]
  node [
    id 448
    label "Polska"
  ]
  node [
    id 449
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 450
    label "wspania&#322;y"
  ]
  node [
    id 451
    label "doskona&#322;y"
  ]
  node [
    id 452
    label "kochany"
  ]
  node [
    id 453
    label "jednostka_monetarna"
  ]
  node [
    id 454
    label "z&#322;ocenie"
  ]
  node [
    id 455
    label "pozyskiwa&#263;"
  ]
  node [
    id 456
    label "act"
  ]
  node [
    id 457
    label "przeg&#322;osowywanie"
  ]
  node [
    id 458
    label "wybranie"
  ]
  node [
    id 459
    label "decydowanie"
  ]
  node [
    id 460
    label "wypowiadanie_si&#281;"
  ]
  node [
    id 461
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 462
    label "reasumowa&#263;"
  ]
  node [
    id 463
    label "poll"
  ]
  node [
    id 464
    label "wi&#281;kszo&#347;&#263;_bezwzgl&#281;dna"
  ]
  node [
    id 465
    label "vote"
  ]
  node [
    id 466
    label "przeg&#322;osowanie"
  ]
  node [
    id 467
    label "reasumowanie"
  ]
  node [
    id 468
    label "powodowanie"
  ]
  node [
    id 469
    label "wybieranie"
  ]
  node [
    id 470
    label "przyczyna"
  ]
  node [
    id 471
    label "matuszka"
  ]
  node [
    id 472
    label "geneza"
  ]
  node [
    id 473
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 474
    label "czynnik"
  ]
  node [
    id 475
    label "poci&#261;ganie"
  ]
  node [
    id 476
    label "rezultat"
  ]
  node [
    id 477
    label "uprz&#261;&#380;"
  ]
  node [
    id 478
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 479
    label "subject"
  ]
  node [
    id 480
    label "wykonawca"
  ]
  node [
    id 481
    label "nauczyciel"
  ]
  node [
    id 482
    label "w_chuj"
  ]
  node [
    id 483
    label "frymark"
  ]
  node [
    id 484
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 485
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 486
    label "commodity"
  ]
  node [
    id 487
    label "mienie"
  ]
  node [
    id 488
    label "Wilko"
  ]
  node [
    id 489
    label "centym"
  ]
  node [
    id 490
    label "przysparza&#263;"
  ]
  node [
    id 491
    label "give"
  ]
  node [
    id 492
    label "kali&#263;_si&#281;"
  ]
  node [
    id 493
    label "bonanza"
  ]
  node [
    id 494
    label "sprzyja&#263;"
  ]
  node [
    id 495
    label "back"
  ]
  node [
    id 496
    label "pociesza&#263;"
  ]
  node [
    id 497
    label "Warszawa"
  ]
  node [
    id 498
    label "u&#322;atwia&#263;"
  ]
  node [
    id 499
    label "opiera&#263;"
  ]
  node [
    id 500
    label "zamilkn&#261;&#263;"
  ]
  node [
    id 501
    label "nicpo&#324;"
  ]
  node [
    id 502
    label "zamilkni&#281;cie"
  ]
  node [
    id 503
    label "agent"
  ]
  node [
    id 504
    label "autor"
  ]
  node [
    id 505
    label "mistrz"
  ]
  node [
    id 506
    label "uzyskiwa&#263;"
  ]
  node [
    id 507
    label "u&#380;ywa&#263;"
  ]
  node [
    id 508
    label "kieliszek"
  ]
  node [
    id 509
    label "shot"
  ]
  node [
    id 510
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 511
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 512
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 513
    label "jednolicie"
  ]
  node [
    id 514
    label "w&#243;dka"
  ]
  node [
    id 515
    label "ten"
  ]
  node [
    id 516
    label "ujednolicenie"
  ]
  node [
    id 517
    label "jednakowy"
  ]
  node [
    id 518
    label "utrzymywa&#263;"
  ]
  node [
    id 519
    label "dostarcza&#263;"
  ]
  node [
    id 520
    label "informowa&#263;"
  ]
  node [
    id 521
    label "deliver"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 133
  ]
  edge [
    source 7
    target 134
  ]
  edge [
    source 7
    target 135
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 136
  ]
  edge [
    source 8
    target 137
  ]
  edge [
    source 8
    target 138
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 139
  ]
  edge [
    source 9
    target 140
  ]
  edge [
    source 9
    target 141
  ]
  edge [
    source 9
    target 142
  ]
  edge [
    source 9
    target 143
  ]
  edge [
    source 9
    target 144
  ]
  edge [
    source 9
    target 145
  ]
  edge [
    source 9
    target 146
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 147
  ]
  edge [
    source 10
    target 148
  ]
  edge [
    source 10
    target 32
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 149
  ]
  edge [
    source 11
    target 150
  ]
  edge [
    source 11
    target 151
  ]
  edge [
    source 11
    target 152
  ]
  edge [
    source 11
    target 153
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 23
  ]
  edge [
    source 13
    target 154
  ]
  edge [
    source 13
    target 155
  ]
  edge [
    source 13
    target 156
  ]
  edge [
    source 13
    target 157
  ]
  edge [
    source 13
    target 158
  ]
  edge [
    source 13
    target 159
  ]
  edge [
    source 13
    target 160
  ]
  edge [
    source 13
    target 161
  ]
  edge [
    source 13
    target 162
  ]
  edge [
    source 13
    target 81
  ]
  edge [
    source 13
    target 163
  ]
  edge [
    source 13
    target 164
  ]
  edge [
    source 13
    target 165
  ]
  edge [
    source 13
    target 166
  ]
  edge [
    source 13
    target 167
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 28
  ]
  edge [
    source 14
    target 55
  ]
  edge [
    source 14
    target 56
  ]
  edge [
    source 14
    target 63
  ]
  edge [
    source 14
    target 64
  ]
  edge [
    source 14
    target 168
  ]
  edge [
    source 14
    target 169
  ]
  edge [
    source 14
    target 170
  ]
  edge [
    source 14
    target 171
  ]
  edge [
    source 14
    target 172
  ]
  edge [
    source 14
    target 173
  ]
  edge [
    source 14
    target 174
  ]
  edge [
    source 14
    target 175
  ]
  edge [
    source 14
    target 176
  ]
  edge [
    source 14
    target 177
  ]
  edge [
    source 14
    target 178
  ]
  edge [
    source 14
    target 45
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 154
  ]
  edge [
    source 15
    target 179
  ]
  edge [
    source 15
    target 180
  ]
  edge [
    source 15
    target 181
  ]
  edge [
    source 15
    target 182
  ]
  edge [
    source 15
    target 183
  ]
  edge [
    source 15
    target 184
  ]
  edge [
    source 15
    target 185
  ]
  edge [
    source 15
    target 186
  ]
  edge [
    source 15
    target 187
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 188
  ]
  edge [
    source 16
    target 189
  ]
  edge [
    source 16
    target 190
  ]
  edge [
    source 16
    target 191
  ]
  edge [
    source 16
    target 192
  ]
  edge [
    source 16
    target 193
  ]
  edge [
    source 16
    target 194
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 195
  ]
  edge [
    source 17
    target 196
  ]
  edge [
    source 17
    target 197
  ]
  edge [
    source 17
    target 198
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 199
  ]
  edge [
    source 18
    target 200
  ]
  edge [
    source 18
    target 201
  ]
  edge [
    source 18
    target 202
  ]
  edge [
    source 18
    target 203
  ]
  edge [
    source 18
    target 204
  ]
  edge [
    source 18
    target 205
  ]
  edge [
    source 18
    target 206
  ]
  edge [
    source 18
    target 207
  ]
  edge [
    source 18
    target 208
  ]
  edge [
    source 18
    target 209
  ]
  edge [
    source 18
    target 210
  ]
  edge [
    source 18
    target 211
  ]
  edge [
    source 18
    target 212
  ]
  edge [
    source 18
    target 26
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 37
  ]
  edge [
    source 19
    target 38
  ]
  edge [
    source 19
    target 42
  ]
  edge [
    source 19
    target 43
  ]
  edge [
    source 19
    target 50
  ]
  edge [
    source 19
    target 51
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 213
  ]
  edge [
    source 20
    target 183
  ]
  edge [
    source 20
    target 214
  ]
  edge [
    source 20
    target 215
  ]
  edge [
    source 20
    target 216
  ]
  edge [
    source 20
    target 217
  ]
  edge [
    source 20
    target 218
  ]
  edge [
    source 20
    target 70
  ]
  edge [
    source 20
    target 37
  ]
  edge [
    source 20
    target 72
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 219
  ]
  edge [
    source 21
    target 220
  ]
  edge [
    source 21
    target 221
  ]
  edge [
    source 21
    target 222
  ]
  edge [
    source 21
    target 223
  ]
  edge [
    source 21
    target 224
  ]
  edge [
    source 21
    target 225
  ]
  edge [
    source 21
    target 226
  ]
  edge [
    source 21
    target 227
  ]
  edge [
    source 21
    target 228
  ]
  edge [
    source 21
    target 229
  ]
  edge [
    source 21
    target 43
  ]
  edge [
    source 21
    target 230
  ]
  edge [
    source 21
    target 231
  ]
  edge [
    source 21
    target 232
  ]
  edge [
    source 21
    target 233
  ]
  edge [
    source 21
    target 234
  ]
  edge [
    source 21
    target 235
  ]
  edge [
    source 21
    target 236
  ]
  edge [
    source 21
    target 237
  ]
  edge [
    source 21
    target 238
  ]
  edge [
    source 21
    target 239
  ]
  edge [
    source 21
    target 240
  ]
  edge [
    source 21
    target 241
  ]
  edge [
    source 21
    target 242
  ]
  edge [
    source 21
    target 243
  ]
  edge [
    source 21
    target 244
  ]
  edge [
    source 21
    target 245
  ]
  edge [
    source 21
    target 246
  ]
  edge [
    source 21
    target 247
  ]
  edge [
    source 21
    target 248
  ]
  edge [
    source 21
    target 249
  ]
  edge [
    source 21
    target 250
  ]
  edge [
    source 21
    target 251
  ]
  edge [
    source 21
    target 252
  ]
  edge [
    source 21
    target 253
  ]
  edge [
    source 21
    target 254
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 255
  ]
  edge [
    source 24
    target 256
  ]
  edge [
    source 24
    target 257
  ]
  edge [
    source 24
    target 258
  ]
  edge [
    source 24
    target 259
  ]
  edge [
    source 24
    target 260
  ]
  edge [
    source 24
    target 261
  ]
  edge [
    source 24
    target 262
  ]
  edge [
    source 24
    target 263
  ]
  edge [
    source 24
    target 264
  ]
  edge [
    source 24
    target 265
  ]
  edge [
    source 24
    target 266
  ]
  edge [
    source 24
    target 267
  ]
  edge [
    source 24
    target 190
  ]
  edge [
    source 24
    target 268
  ]
  edge [
    source 24
    target 269
  ]
  edge [
    source 24
    target 270
  ]
  edge [
    source 24
    target 271
  ]
  edge [
    source 24
    target 272
  ]
  edge [
    source 24
    target 273
  ]
  edge [
    source 24
    target 274
  ]
  edge [
    source 24
    target 275
  ]
  edge [
    source 24
    target 276
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 124
  ]
  edge [
    source 25
    target 277
  ]
  edge [
    source 25
    target 93
  ]
  edge [
    source 25
    target 278
  ]
  edge [
    source 25
    target 279
  ]
  edge [
    source 25
    target 127
  ]
  edge [
    source 25
    target 128
  ]
  edge [
    source 25
    target 280
  ]
  edge [
    source 25
    target 129
  ]
  edge [
    source 25
    target 281
  ]
  edge [
    source 25
    target 131
  ]
  edge [
    source 25
    target 282
  ]
  edge [
    source 25
    target 283
  ]
  edge [
    source 25
    target 56
  ]
  edge [
    source 25
    target 68
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 29
  ]
  edge [
    source 26
    target 30
  ]
  edge [
    source 26
    target 69
  ]
  edge [
    source 26
    target 70
  ]
  edge [
    source 26
    target 284
  ]
  edge [
    source 26
    target 285
  ]
  edge [
    source 26
    target 286
  ]
  edge [
    source 26
    target 287
  ]
  edge [
    source 26
    target 288
  ]
  edge [
    source 26
    target 289
  ]
  edge [
    source 26
    target 290
  ]
  edge [
    source 26
    target 291
  ]
  edge [
    source 27
    target 292
  ]
  edge [
    source 27
    target 293
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 294
  ]
  edge [
    source 28
    target 295
  ]
  edge [
    source 28
    target 296
  ]
  edge [
    source 28
    target 297
  ]
  edge [
    source 28
    target 298
  ]
  edge [
    source 28
    target 299
  ]
  edge [
    source 29
    target 300
  ]
  edge [
    source 29
    target 301
  ]
  edge [
    source 29
    target 302
  ]
  edge [
    source 29
    target 303
  ]
  edge [
    source 29
    target 304
  ]
  edge [
    source 29
    target 305
  ]
  edge [
    source 29
    target 306
  ]
  edge [
    source 29
    target 307
  ]
  edge [
    source 29
    target 308
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 36
  ]
  edge [
    source 30
    target 65
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 309
  ]
  edge [
    source 32
    target 310
  ]
  edge [
    source 32
    target 311
  ]
  edge [
    source 32
    target 312
  ]
  edge [
    source 32
    target 313
  ]
  edge [
    source 32
    target 314
  ]
  edge [
    source 32
    target 315
  ]
  edge [
    source 32
    target 316
  ]
  edge [
    source 32
    target 317
  ]
  edge [
    source 32
    target 318
  ]
  edge [
    source 32
    target 319
  ]
  edge [
    source 32
    target 320
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 40
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 321
  ]
  edge [
    source 35
    target 322
  ]
  edge [
    source 35
    target 323
  ]
  edge [
    source 35
    target 324
  ]
  edge [
    source 35
    target 102
  ]
  edge [
    source 35
    target 325
  ]
  edge [
    source 35
    target 326
  ]
  edge [
    source 35
    target 327
  ]
  edge [
    source 35
    target 328
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 65
  ]
  edge [
    source 37
    target 329
  ]
  edge [
    source 37
    target 330
  ]
  edge [
    source 37
    target 331
  ]
  edge [
    source 37
    target 332
  ]
  edge [
    source 37
    target 333
  ]
  edge [
    source 37
    target 334
  ]
  edge [
    source 37
    target 335
  ]
  edge [
    source 37
    target 336
  ]
  edge [
    source 37
    target 72
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 337
  ]
  edge [
    source 39
    target 338
  ]
  edge [
    source 39
    target 339
  ]
  edge [
    source 39
    target 340
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 190
  ]
  edge [
    source 40
    target 341
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 342
  ]
  edge [
    source 42
    target 343
  ]
  edge [
    source 42
    target 344
  ]
  edge [
    source 42
    target 345
  ]
  edge [
    source 42
    target 346
  ]
  edge [
    source 42
    target 347
  ]
  edge [
    source 42
    target 192
  ]
  edge [
    source 42
    target 348
  ]
  edge [
    source 42
    target 349
  ]
  edge [
    source 42
    target 350
  ]
  edge [
    source 42
    target 65
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 351
  ]
  edge [
    source 43
    target 352
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 45
    target 353
  ]
  edge [
    source 45
    target 354
  ]
  edge [
    source 45
    target 355
  ]
  edge [
    source 45
    target 356
  ]
  edge [
    source 45
    target 357
  ]
  edge [
    source 45
    target 358
  ]
  edge [
    source 45
    target 359
  ]
  edge [
    source 45
    target 360
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 361
  ]
  edge [
    source 46
    target 362
  ]
  edge [
    source 46
    target 363
  ]
  edge [
    source 46
    target 364
  ]
  edge [
    source 46
    target 365
  ]
  edge [
    source 46
    target 366
  ]
  edge [
    source 46
    target 367
  ]
  edge [
    source 46
    target 368
  ]
  edge [
    source 46
    target 369
  ]
  edge [
    source 46
    target 370
  ]
  edge [
    source 46
    target 371
  ]
  edge [
    source 46
    target 372
  ]
  edge [
    source 46
    target 373
  ]
  edge [
    source 46
    target 374
  ]
  edge [
    source 46
    target 375
  ]
  edge [
    source 46
    target 376
  ]
  edge [
    source 46
    target 377
  ]
  edge [
    source 46
    target 378
  ]
  edge [
    source 46
    target 379
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 380
  ]
  edge [
    source 47
    target 381
  ]
  edge [
    source 47
    target 382
  ]
  edge [
    source 47
    target 383
  ]
  edge [
    source 47
    target 384
  ]
  edge [
    source 47
    target 385
  ]
  edge [
    source 47
    target 386
  ]
  edge [
    source 47
    target 387
  ]
  edge [
    source 47
    target 388
  ]
  edge [
    source 47
    target 389
  ]
  edge [
    source 47
    target 390
  ]
  edge [
    source 47
    target 391
  ]
  edge [
    source 47
    target 392
  ]
  edge [
    source 47
    target 393
  ]
  edge [
    source 47
    target 394
  ]
  edge [
    source 47
    target 395
  ]
  edge [
    source 47
    target 396
  ]
  edge [
    source 47
    target 397
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 398
  ]
  edge [
    source 48
    target 399
  ]
  edge [
    source 48
    target 400
  ]
  edge [
    source 48
    target 401
  ]
  edge [
    source 48
    target 402
  ]
  edge [
    source 48
    target 403
  ]
  edge [
    source 48
    target 404
  ]
  edge [
    source 48
    target 405
  ]
  edge [
    source 48
    target 273
  ]
  edge [
    source 48
    target 190
  ]
  edge [
    source 48
    target 406
  ]
  edge [
    source 48
    target 407
  ]
  edge [
    source 48
    target 408
  ]
  edge [
    source 48
    target 409
  ]
  edge [
    source 48
    target 355
  ]
  edge [
    source 48
    target 410
  ]
  edge [
    source 48
    target 411
  ]
  edge [
    source 48
    target 412
  ]
  edge [
    source 48
    target 413
  ]
  edge [
    source 49
    target 226
  ]
  edge [
    source 49
    target 414
  ]
  edge [
    source 50
    target 415
  ]
  edge [
    source 50
    target 416
  ]
  edge [
    source 50
    target 417
  ]
  edge [
    source 50
    target 418
  ]
  edge [
    source 50
    target 419
  ]
  edge [
    source 50
    target 420
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 421
  ]
  edge [
    source 51
    target 422
  ]
  edge [
    source 51
    target 423
  ]
  edge [
    source 51
    target 424
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 425
  ]
  edge [
    source 52
    target 426
  ]
  edge [
    source 52
    target 427
  ]
  edge [
    source 52
    target 428
  ]
  edge [
    source 52
    target 429
  ]
  edge [
    source 52
    target 430
  ]
  edge [
    source 52
    target 431
  ]
  edge [
    source 52
    target 432
  ]
  edge [
    source 52
    target 433
  ]
  edge [
    source 52
    target 434
  ]
  edge [
    source 52
    target 435
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 55
    target 436
  ]
  edge [
    source 55
    target 437
  ]
  edge [
    source 55
    target 438
  ]
  edge [
    source 55
    target 439
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 440
  ]
  edge [
    source 56
    target 441
  ]
  edge [
    source 56
    target 442
  ]
  edge [
    source 56
    target 443
  ]
  edge [
    source 56
    target 444
  ]
  edge [
    source 56
    target 445
  ]
  edge [
    source 56
    target 446
  ]
  edge [
    source 56
    target 447
  ]
  edge [
    source 56
    target 448
  ]
  edge [
    source 56
    target 449
  ]
  edge [
    source 56
    target 450
  ]
  edge [
    source 56
    target 451
  ]
  edge [
    source 56
    target 452
  ]
  edge [
    source 56
    target 453
  ]
  edge [
    source 56
    target 454
  ]
  edge [
    source 56
    target 68
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 455
  ]
  edge [
    source 57
    target 456
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 59
    target 457
  ]
  edge [
    source 59
    target 458
  ]
  edge [
    source 59
    target 459
  ]
  edge [
    source 59
    target 460
  ]
  edge [
    source 59
    target 461
  ]
  edge [
    source 59
    target 462
  ]
  edge [
    source 59
    target 463
  ]
  edge [
    source 59
    target 464
  ]
  edge [
    source 59
    target 465
  ]
  edge [
    source 59
    target 107
  ]
  edge [
    source 59
    target 466
  ]
  edge [
    source 59
    target 467
  ]
  edge [
    source 59
    target 468
  ]
  edge [
    source 59
    target 469
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 320
  ]
  edge [
    source 62
    target 470
  ]
  edge [
    source 62
    target 471
  ]
  edge [
    source 62
    target 472
  ]
  edge [
    source 62
    target 473
  ]
  edge [
    source 62
    target 474
  ]
  edge [
    source 62
    target 475
  ]
  edge [
    source 62
    target 476
  ]
  edge [
    source 62
    target 477
  ]
  edge [
    source 62
    target 478
  ]
  edge [
    source 62
    target 479
  ]
  edge [
    source 63
    target 68
  ]
  edge [
    source 63
    target 480
  ]
  edge [
    source 63
    target 481
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 482
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 483
  ]
  edge [
    source 65
    target 484
  ]
  edge [
    source 65
    target 485
  ]
  edge [
    source 65
    target 486
  ]
  edge [
    source 65
    target 487
  ]
  edge [
    source 65
    target 488
  ]
  edge [
    source 65
    target 453
  ]
  edge [
    source 65
    target 489
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 66
    target 490
  ]
  edge [
    source 66
    target 491
  ]
  edge [
    source 66
    target 492
  ]
  edge [
    source 66
    target 493
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 67
    target 494
  ]
  edge [
    source 67
    target 495
  ]
  edge [
    source 67
    target 496
  ]
  edge [
    source 67
    target 497
  ]
  edge [
    source 67
    target 498
  ]
  edge [
    source 67
    target 499
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 68
    target 500
  ]
  edge [
    source 68
    target 501
  ]
  edge [
    source 68
    target 502
  ]
  edge [
    source 68
    target 503
  ]
  edge [
    source 68
    target 504
  ]
  edge [
    source 68
    target 505
  ]
  edge [
    source 69
    target 124
  ]
  edge [
    source 69
    target 506
  ]
  edge [
    source 69
    target 507
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 508
  ]
  edge [
    source 70
    target 509
  ]
  edge [
    source 70
    target 510
  ]
  edge [
    source 70
    target 511
  ]
  edge [
    source 70
    target 512
  ]
  edge [
    source 70
    target 513
  ]
  edge [
    source 70
    target 514
  ]
  edge [
    source 70
    target 515
  ]
  edge [
    source 70
    target 516
  ]
  edge [
    source 70
    target 517
  ]
  edge [
    source 72
    target 518
  ]
  edge [
    source 72
    target 519
  ]
  edge [
    source 72
    target 520
  ]
  edge [
    source 72
    target 521
  ]
]
