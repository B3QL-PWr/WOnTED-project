graph [
  maxDegree 15
  minDegree 1
  meanDegree 1.8857142857142857
  density 0.05546218487394958
  graphCliqueNumber 2
  node [
    id 0
    label "wyobra&#380;enie"
    origin "text"
  ]
  node [
    id 1
    label "ile"
    origin "text"
  ]
  node [
    id 2
    label "z&#322;oto"
    origin "text"
  ]
  node [
    id 3
    label "wy&#322;adowa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "odessa"
    origin "text"
  ]
  node [
    id 5
    label "zderzenie_si&#281;"
  ]
  node [
    id 6
    label "s&#261;d"
  ]
  node [
    id 7
    label "teoria_Arrheniusa"
  ]
  node [
    id 8
    label "wytw&#243;r"
  ]
  node [
    id 9
    label "zderzy&#263;_si&#281;"
  ]
  node [
    id 10
    label "teologicznie"
  ]
  node [
    id 11
    label "idea"
  ]
  node [
    id 12
    label "obraz"
  ]
  node [
    id 13
    label "belief"
  ]
  node [
    id 14
    label "pogl&#261;d"
  ]
  node [
    id 15
    label "posta&#263;"
  ]
  node [
    id 16
    label "cz&#322;owiek"
  ]
  node [
    id 17
    label "amber"
  ]
  node [
    id 18
    label "z&#322;ocisty"
  ]
  node [
    id 19
    label "metalicznie"
  ]
  node [
    id 20
    label "p&#322;uczkarnia"
  ]
  node [
    id 21
    label "miedziowiec"
  ]
  node [
    id 22
    label "bi&#380;uteria"
  ]
  node [
    id 23
    label "metal_szlachetny"
  ]
  node [
    id 24
    label "pieni&#261;dze"
  ]
  node [
    id 25
    label "p&#322;ucznia"
  ]
  node [
    id 26
    label "medal"
  ]
  node [
    id 27
    label "&#380;&#243;&#322;&#263;"
  ]
  node [
    id 28
    label "&#380;&#243;&#322;to"
  ]
  node [
    id 29
    label "discharge"
  ]
  node [
    id 30
    label "pack"
  ]
  node [
    id 31
    label "odreagowa&#263;"
  ]
  node [
    id 32
    label "opr&#243;&#380;ni&#263;"
  ]
  node [
    id 33
    label "plac"
  ]
  node [
    id 34
    label "czerwona"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 33
    target 34
  ]
]
