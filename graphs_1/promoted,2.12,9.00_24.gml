graph [
  maxDegree 27
  minDegree 1
  meanDegree 1.9682539682539681
  density 0.031746031746031744
  graphCliqueNumber 2
  node [
    id 0
    label "nastolatek"
    origin "text"
  ]
  node [
    id 1
    label "nowa"
    origin "text"
  ]
  node [
    id 2
    label "skalmierzyce"
    origin "text"
  ]
  node [
    id 3
    label "woj"
    origin "text"
  ]
  node [
    id 4
    label "wielkopolski"
    origin "text"
  ]
  node [
    id 5
    label "zabi&#263;"
    origin "text"
  ]
  node [
    id 6
    label "swoje"
    origin "text"
  ]
  node [
    id 7
    label "letni"
    origin "text"
  ]
  node [
    id 8
    label "ojczym"
    origin "text"
  ]
  node [
    id 9
    label "m&#322;odzieniec"
  ]
  node [
    id 10
    label "dorastaj&#261;cy"
  ]
  node [
    id 11
    label "gwiazda"
  ]
  node [
    id 12
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 13
    label "&#380;o&#322;nierz"
  ]
  node [
    id 14
    label "wite&#378;"
  ]
  node [
    id 15
    label "wojownik"
  ]
  node [
    id 16
    label "po_wielkopolsku"
  ]
  node [
    id 17
    label "knypek"
  ]
  node [
    id 18
    label "kwyrla"
  ]
  node [
    id 19
    label "bimba"
  ]
  node [
    id 20
    label "hy&#263;ka"
  ]
  node [
    id 21
    label "plyndz"
  ]
  node [
    id 22
    label "regionalny"
  ]
  node [
    id 23
    label "gzik"
  ]
  node [
    id 24
    label "polski"
  ]
  node [
    id 25
    label "myrdyrda"
  ]
  node [
    id 26
    label "skrzywdzi&#263;"
  ]
  node [
    id 27
    label "kill"
  ]
  node [
    id 28
    label "poder&#380;n&#261;&#263;"
  ]
  node [
    id 29
    label "skarci&#263;"
  ]
  node [
    id 30
    label "wyko&#324;czy&#263;"
  ]
  node [
    id 31
    label "przybi&#263;"
  ]
  node [
    id 32
    label "zakry&#263;"
  ]
  node [
    id 33
    label "rozbroi&#263;"
  ]
  node [
    id 34
    label "zmordowa&#263;"
  ]
  node [
    id 35
    label "break"
  ]
  node [
    id 36
    label "zastrzeli&#263;"
  ]
  node [
    id 37
    label "u&#347;mierci&#263;"
  ]
  node [
    id 38
    label "zadzwoni&#263;"
  ]
  node [
    id 39
    label "pomacha&#263;"
  ]
  node [
    id 40
    label "po&#322;o&#380;y&#263;_na_kolana"
  ]
  node [
    id 41
    label "zwalczy&#263;"
  ]
  node [
    id 42
    label "zniszczy&#263;"
  ]
  node [
    id 43
    label "os&#322;oni&#263;"
  ]
  node [
    id 44
    label "dispatch"
  ]
  node [
    id 45
    label "uderzy&#263;"
  ]
  node [
    id 46
    label "zapulsowa&#263;"
  ]
  node [
    id 47
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 48
    label "skrzywi&#263;"
  ]
  node [
    id 49
    label "zako&#324;czy&#263;"
  ]
  node [
    id 50
    label "zbi&#263;"
  ]
  node [
    id 51
    label "nijaki"
  ]
  node [
    id 52
    label "sezonowy"
  ]
  node [
    id 53
    label "letnio"
  ]
  node [
    id 54
    label "s&#322;oneczny"
  ]
  node [
    id 55
    label "weso&#322;y"
  ]
  node [
    id 56
    label "oboj&#281;tny"
  ]
  node [
    id 57
    label "latowy"
  ]
  node [
    id 58
    label "ciep&#322;y"
  ]
  node [
    id 59
    label "typowy"
  ]
  node [
    id 60
    label "ojciec"
  ]
  node [
    id 61
    label "&#380;onaty"
  ]
  node [
    id 62
    label "Skalmierzyce"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
]
