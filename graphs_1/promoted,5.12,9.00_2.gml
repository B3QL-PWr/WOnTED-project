graph [
  maxDegree 22
  minDegree 1
  meanDegree 1.9024390243902438
  density 0.0475609756097561
  graphCliqueNumber 2
  node [
    id 0
    label "obarczy&#263;"
    origin "text"
  ]
  node [
    id 1
    label "wina"
    origin "text"
  ]
  node [
    id 2
    label "fiasko"
    origin "text"
  ]
  node [
    id 3
    label "operacja"
    origin "text"
  ]
  node [
    id 4
    label "garden"
    origin "text"
  ]
  node [
    id 5
    label "blame"
  ]
  node [
    id 6
    label "obci&#261;&#380;y&#263;"
  ]
  node [
    id 7
    label "charge"
  ]
  node [
    id 8
    label "oskar&#380;y&#263;"
  ]
  node [
    id 9
    label "odpowiedzialno&#347;&#263;"
  ]
  node [
    id 10
    label "obowi&#261;zek"
  ]
  node [
    id 11
    label "zobowi&#261;za&#263;"
  ]
  node [
    id 12
    label "load"
  ]
  node [
    id 13
    label "wstyd"
  ]
  node [
    id 14
    label "konsekwencja"
  ]
  node [
    id 15
    label "guilt"
  ]
  node [
    id 16
    label "lutnia"
  ]
  node [
    id 17
    label "visitation"
  ]
  node [
    id 18
    label "wydarzenie"
  ]
  node [
    id 19
    label "infimum"
  ]
  node [
    id 20
    label "laparotomia"
  ]
  node [
    id 21
    label "chirurg"
  ]
  node [
    id 22
    label "manewr_Pringle&#8217;a"
  ]
  node [
    id 23
    label "supremum"
  ]
  node [
    id 24
    label "torakotomia"
  ]
  node [
    id 25
    label "funkcja"
  ]
  node [
    id 26
    label "strategia"
  ]
  node [
    id 27
    label "szew"
  ]
  node [
    id 28
    label "rzut"
  ]
  node [
    id 29
    label "liczenie"
  ]
  node [
    id 30
    label "ob&#322;awa_augustowska"
  ]
  node [
    id 31
    label "proces_my&#347;lowy"
  ]
  node [
    id 32
    label "czyn"
  ]
  node [
    id 33
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 34
    label "matematyka"
  ]
  node [
    id 35
    label "zabieg"
  ]
  node [
    id 36
    label "mathematical_process"
  ]
  node [
    id 37
    label "liczy&#263;"
  ]
  node [
    id 38
    label "jednostka"
  ]
  node [
    id 39
    label "market"
  ]
  node [
    id 40
    label "Garden"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 39
    target 40
  ]
]
