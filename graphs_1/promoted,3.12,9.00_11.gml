graph [
  maxDegree 38
  minDegree 1
  meanDegree 2.0396039603960396
  density 0.020396039603960397
  graphCliqueNumber 2
  node [
    id 0
    label "pies"
    origin "text"
  ]
  node [
    id 1
    label "poznawa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "swoje"
    origin "text"
  ]
  node [
    id 3
    label "pan"
    origin "text"
  ]
  node [
    id 4
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 5
    label "zrzuci&#263;"
    origin "text"
  ]
  node [
    id 6
    label "sporo"
    origin "text"
  ]
  node [
    id 7
    label "waga"
    origin "text"
  ]
  node [
    id 8
    label "cz&#322;owiek"
  ]
  node [
    id 9
    label "wy&#263;"
  ]
  node [
    id 10
    label "ssak_drapie&#380;ny"
  ]
  node [
    id 11
    label "spragniony"
  ]
  node [
    id 12
    label "rakarz"
  ]
  node [
    id 13
    label "psowate"
  ]
  node [
    id 14
    label "istota_&#380;ywa"
  ]
  node [
    id 15
    label "kabanos"
  ]
  node [
    id 16
    label "mi&#322;o&#347;nik"
  ]
  node [
    id 17
    label "&#322;ajdak"
  ]
  node [
    id 18
    label "czworon&#243;g"
  ]
  node [
    id 19
    label "policjant"
  ]
  node [
    id 20
    label "szczucie"
  ]
  node [
    id 21
    label "s&#322;u&#380;enie"
  ]
  node [
    id 22
    label "sobaka"
  ]
  node [
    id 23
    label "dogoterapia"
  ]
  node [
    id 24
    label "Cerber"
  ]
  node [
    id 25
    label "wyzwisko"
  ]
  node [
    id 26
    label "szczu&#263;"
  ]
  node [
    id 27
    label "wycie"
  ]
  node [
    id 28
    label "szczeka&#263;"
  ]
  node [
    id 29
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 30
    label "trufla"
  ]
  node [
    id 31
    label "samiec"
  ]
  node [
    id 32
    label "piese&#322;"
  ]
  node [
    id 33
    label "zawy&#263;"
  ]
  node [
    id 34
    label "styka&#263;_si&#281;"
  ]
  node [
    id 35
    label "go_steady"
  ]
  node [
    id 36
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 37
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 38
    label "zawiera&#263;"
  ]
  node [
    id 39
    label "hurt"
  ]
  node [
    id 40
    label "detect"
  ]
  node [
    id 41
    label "cognizance"
  ]
  node [
    id 42
    label "rozr&#243;&#380;nia&#263;"
  ]
  node [
    id 43
    label "szkoli&#263;_si&#281;"
  ]
  node [
    id 44
    label "make"
  ]
  node [
    id 45
    label "profesor"
  ]
  node [
    id 46
    label "kszta&#322;ciciel"
  ]
  node [
    id 47
    label "jegomo&#347;&#263;"
  ]
  node [
    id 48
    label "zwrot"
  ]
  node [
    id 49
    label "pracodawca"
  ]
  node [
    id 50
    label "rz&#261;dzenie"
  ]
  node [
    id 51
    label "m&#261;&#380;"
  ]
  node [
    id 52
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 53
    label "ch&#322;opina"
  ]
  node [
    id 54
    label "bratek"
  ]
  node [
    id 55
    label "opiekun"
  ]
  node [
    id 56
    label "doros&#322;y"
  ]
  node [
    id 57
    label "preceptor"
  ]
  node [
    id 58
    label "Midas"
  ]
  node [
    id 59
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 60
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 61
    label "murza"
  ]
  node [
    id 62
    label "ojciec"
  ]
  node [
    id 63
    label "androlog"
  ]
  node [
    id 64
    label "pupil"
  ]
  node [
    id 65
    label "efendi"
  ]
  node [
    id 66
    label "nabab"
  ]
  node [
    id 67
    label "w&#322;odarz"
  ]
  node [
    id 68
    label "szkolnik"
  ]
  node [
    id 69
    label "pedagog"
  ]
  node [
    id 70
    label "popularyzator"
  ]
  node [
    id 71
    label "andropauza"
  ]
  node [
    id 72
    label "gra_w_karty"
  ]
  node [
    id 73
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 74
    label "Mieszko_I"
  ]
  node [
    id 75
    label "bogaty"
  ]
  node [
    id 76
    label "przyw&#243;dca"
  ]
  node [
    id 77
    label "pa&#324;stwo"
  ]
  node [
    id 78
    label "belfer"
  ]
  node [
    id 79
    label "str&#243;j"
  ]
  node [
    id 80
    label "zdj&#261;&#263;"
  ]
  node [
    id 81
    label "drop"
  ]
  node [
    id 82
    label "odprowadzi&#263;"
  ]
  node [
    id 83
    label "spill"
  ]
  node [
    id 84
    label "zgromadzi&#263;"
  ]
  node [
    id 85
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 86
    label "spory"
  ]
  node [
    id 87
    label "report"
  ]
  node [
    id 88
    label "odwa&#380;nik"
  ]
  node [
    id 89
    label "kategoria"
  ]
  node [
    id 90
    label "weight"
  ]
  node [
    id 91
    label "zawa&#380;enie"
  ]
  node [
    id 92
    label "zawa&#380;y&#263;"
  ]
  node [
    id 93
    label "j&#281;zyczek_u_wagi"
  ]
  node [
    id 94
    label "cecha"
  ]
  node [
    id 95
    label "szala"
  ]
  node [
    id 96
    label "&#263;wiczenie"
  ]
  node [
    id 97
    label "urz&#261;dzenie"
  ]
  node [
    id 98
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 99
    label "pomiar"
  ]
  node [
    id 100
    label "load"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
]
