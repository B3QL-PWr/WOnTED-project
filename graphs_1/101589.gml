graph [
  maxDegree 20
  minDegree 1
  meanDegree 1.9411764705882353
  density 0.0192195690157251
  graphCliqueNumber 2
  node [
    id 0
    label "chcie&#263;byby&#263;"
    origin "text"
  ]
  node [
    id 1
    label "r&#243;wnie&#380;"
    origin "text"
  ]
  node [
    id 2
    label "powiedzie&#263;"
    origin "text"
  ]
  node [
    id 3
    label "zmusi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "by&#263;"
    origin "text"
  ]
  node [
    id 5
    label "je&#378;dzi&#263;"
    origin "text"
  ]
  node [
    id 6
    label "aby"
    origin "text"
  ]
  node [
    id 7
    label "si&#281;"
    origin "text"
  ]
  node [
    id 8
    label "po&#322;udnie"
    origin "text"
  ]
  node [
    id 9
    label "rozpatrywa&#263;"
    origin "text"
  ]
  node [
    id 10
    label "sprawa"
    origin "text"
  ]
  node [
    id 11
    label "p&#243;&#322;noc"
    origin "text"
  ]
  node [
    id 12
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 13
    label "express"
  ]
  node [
    id 14
    label "rzekn&#261;&#263;"
  ]
  node [
    id 15
    label "okre&#347;li&#263;"
  ]
  node [
    id 16
    label "wyrazi&#263;"
  ]
  node [
    id 17
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 18
    label "unwrap"
  ]
  node [
    id 19
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 20
    label "convey"
  ]
  node [
    id 21
    label "discover"
  ]
  node [
    id 22
    label "wydoby&#263;"
  ]
  node [
    id 23
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 24
    label "poda&#263;"
  ]
  node [
    id 25
    label "pop&#281;dzi&#263;_kota"
  ]
  node [
    id 26
    label "zada&#263;_gwa&#322;t"
  ]
  node [
    id 27
    label "spowodowa&#263;"
  ]
  node [
    id 28
    label "force"
  ]
  node [
    id 29
    label "sandbag"
  ]
  node [
    id 30
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 31
    label "si&#281;ga&#263;"
  ]
  node [
    id 32
    label "trwa&#263;"
  ]
  node [
    id 33
    label "obecno&#347;&#263;"
  ]
  node [
    id 34
    label "stan"
  ]
  node [
    id 35
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 36
    label "stand"
  ]
  node [
    id 37
    label "mie&#263;_miejsce"
  ]
  node [
    id 38
    label "uczestniczy&#263;"
  ]
  node [
    id 39
    label "chodzi&#263;"
  ]
  node [
    id 40
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 41
    label "equal"
  ]
  node [
    id 42
    label "continue"
  ]
  node [
    id 43
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 44
    label "umie&#263;"
  ]
  node [
    id 45
    label "napada&#263;"
  ]
  node [
    id 46
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 47
    label "proceed"
  ]
  node [
    id 48
    label "przybywa&#263;"
  ]
  node [
    id 49
    label "uprawia&#263;"
  ]
  node [
    id 50
    label "drive"
  ]
  node [
    id 51
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 52
    label "wyz&#322;o&#347;liwia&#263;_si&#281;"
  ]
  node [
    id 53
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 54
    label "ride"
  ]
  node [
    id 55
    label "wy&#347;miewa&#263;"
  ]
  node [
    id 56
    label "carry"
  ]
  node [
    id 57
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 58
    label "prowadzi&#263;"
  ]
  node [
    id 59
    label "troch&#281;"
  ]
  node [
    id 60
    label "dwunasta"
  ]
  node [
    id 61
    label "obszar"
  ]
  node [
    id 62
    label "Ziemia"
  ]
  node [
    id 63
    label "godzina"
  ]
  node [
    id 64
    label "strona_&#347;wiata"
  ]
  node [
    id 65
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 66
    label "&#347;rodek"
  ]
  node [
    id 67
    label "pora"
  ]
  node [
    id 68
    label "dzie&#324;"
  ]
  node [
    id 69
    label "consider"
  ]
  node [
    id 70
    label "przygl&#261;da&#263;_si&#281;"
  ]
  node [
    id 71
    label "przeprowadza&#263;"
  ]
  node [
    id 72
    label "temat"
  ]
  node [
    id 73
    label "kognicja"
  ]
  node [
    id 74
    label "idea"
  ]
  node [
    id 75
    label "szczeg&#243;&#322;"
  ]
  node [
    id 76
    label "rzecz"
  ]
  node [
    id 77
    label "wydarzenie"
  ]
  node [
    id 78
    label "przes&#322;anka"
  ]
  node [
    id 79
    label "rozprawa"
  ]
  node [
    id 80
    label "object"
  ]
  node [
    id 81
    label "proposition"
  ]
  node [
    id 82
    label "&#347;wiat"
  ]
  node [
    id 83
    label "p&#243;&#322;nocek"
  ]
  node [
    id 84
    label "noc"
  ]
  node [
    id 85
    label "Boreasz"
  ]
  node [
    id 86
    label "Romualda"
  ]
  node [
    id 87
    label "Ajchler"
  ]
  node [
    id 88
    label "Ewa"
  ]
  node [
    id 89
    label "Kierzkowska"
  ]
  node [
    id 90
    label "Jan"
  ]
  node [
    id 91
    label "Kochanowski"
  ]
  node [
    id 92
    label "ministerstwo"
  ]
  node [
    id 93
    label "sprawiedliwo&#347;&#263;"
  ]
  node [
    id 94
    label "Piotr"
  ]
  node [
    id 95
    label "kluza"
  ]
  node [
    id 96
    label "Micha&#322;"
  ]
  node [
    id 97
    label "Wojtkiewicz"
  ]
  node [
    id 98
    label "Gorz&#243;w"
  ]
  node [
    id 99
    label "wielkopolski"
  ]
  node [
    id 100
    label "jeleni"
  ]
  node [
    id 101
    label "g&#243;ra"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 10
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 11
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 11
    target 61
  ]
  edge [
    source 11
    target 62
  ]
  edge [
    source 11
    target 63
  ]
  edge [
    source 11
    target 82
  ]
  edge [
    source 11
    target 83
  ]
  edge [
    source 11
    target 64
  ]
  edge [
    source 11
    target 65
  ]
  edge [
    source 11
    target 84
  ]
  edge [
    source 11
    target 85
  ]
  edge [
    source 86
    target 87
  ]
  edge [
    source 88
    target 89
  ]
  edge [
    source 90
    target 91
  ]
  edge [
    source 92
    target 93
  ]
  edge [
    source 94
    target 95
  ]
  edge [
    source 96
    target 97
  ]
  edge [
    source 98
    target 99
  ]
  edge [
    source 100
    target 101
  ]
]
