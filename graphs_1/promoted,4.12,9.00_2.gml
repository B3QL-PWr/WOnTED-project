graph [
  maxDegree 17
  minDegree 1
  meanDegree 1.9743589743589745
  density 0.02564102564102564
  graphCliqueNumber 2
  node [
    id 0
    label "mossakowski"
    origin "text"
  ]
  node [
    id 1
    label "&#347;wiadomie"
    origin "text"
  ]
  node [
    id 2
    label "pos&#322;u&#380;y&#263;"
    origin "text"
  ]
  node [
    id 3
    label "si&#281;"
    origin "text"
  ]
  node [
    id 4
    label "sfa&#322;szowa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "dokument"
    origin "text"
  ]
  node [
    id 6
    label "aby"
    origin "text"
  ]
  node [
    id 7
    label "uzyska&#263;"
    origin "text"
  ]
  node [
    id 8
    label "prawa"
    origin "text"
  ]
  node [
    id 9
    label "kamienica"
    origin "text"
  ]
  node [
    id 10
    label "warto&#347;&#263;"
    origin "text"
  ]
  node [
    id 11
    label "milion"
    origin "text"
  ]
  node [
    id 12
    label "z&#322;oty"
    origin "text"
  ]
  node [
    id 13
    label "rozs&#261;dnie"
  ]
  node [
    id 14
    label "nieg&#322;upio"
  ]
  node [
    id 15
    label "&#347;wiadomo"
  ]
  node [
    id 16
    label "wittingly"
  ]
  node [
    id 17
    label "&#347;wiadomy"
  ]
  node [
    id 18
    label "przyda&#263;_si&#281;"
  ]
  node [
    id 19
    label "manufacture"
  ]
  node [
    id 20
    label "podrobi&#263;"
  ]
  node [
    id 21
    label "record"
  ]
  node [
    id 22
    label "wytw&#243;r"
  ]
  node [
    id 23
    label "&#347;wiadectwo"
  ]
  node [
    id 24
    label "zapis"
  ]
  node [
    id 25
    label "fascyku&#322;"
  ]
  node [
    id 26
    label "raport&#243;wka"
  ]
  node [
    id 27
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 28
    label "artyku&#322;"
  ]
  node [
    id 29
    label "plik"
  ]
  node [
    id 30
    label "writing"
  ]
  node [
    id 31
    label "utw&#243;r"
  ]
  node [
    id 32
    label "dokumentacja"
  ]
  node [
    id 33
    label "parafa"
  ]
  node [
    id 34
    label "sygnatariusz"
  ]
  node [
    id 35
    label "registratura"
  ]
  node [
    id 36
    label "troch&#281;"
  ]
  node [
    id 37
    label "promocja"
  ]
  node [
    id 38
    label "give_birth"
  ]
  node [
    id 39
    label "wytworzy&#263;"
  ]
  node [
    id 40
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 41
    label "realize"
  ]
  node [
    id 42
    label "zrobi&#263;"
  ]
  node [
    id 43
    label "make"
  ]
  node [
    id 44
    label "dom_wielorodzinny"
  ]
  node [
    id 45
    label "rewaluowa&#263;"
  ]
  node [
    id 46
    label "wabik"
  ]
  node [
    id 47
    label "wskazywanie"
  ]
  node [
    id 48
    label "korzy&#347;&#263;"
  ]
  node [
    id 49
    label "worth"
  ]
  node [
    id 50
    label "rewaluowanie"
  ]
  node [
    id 51
    label "cecha"
  ]
  node [
    id 52
    label "zmienna"
  ]
  node [
    id 53
    label "zrewaluowa&#263;"
  ]
  node [
    id 54
    label "rozmiar"
  ]
  node [
    id 55
    label "poj&#281;cie"
  ]
  node [
    id 56
    label "cel"
  ]
  node [
    id 57
    label "wskazywa&#263;"
  ]
  node [
    id 58
    label "strona"
  ]
  node [
    id 59
    label "zrewaluowanie"
  ]
  node [
    id 60
    label "liczba"
  ]
  node [
    id 61
    label "miljon"
  ]
  node [
    id 62
    label "ba&#324;ka"
  ]
  node [
    id 63
    label "szlachetny"
  ]
  node [
    id 64
    label "metaliczny"
  ]
  node [
    id 65
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 66
    label "z&#322;oci&#347;cie"
  ]
  node [
    id 67
    label "grosz"
  ]
  node [
    id 68
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 69
    label "utytu&#322;owany"
  ]
  node [
    id 70
    label "poz&#322;ocenie"
  ]
  node [
    id 71
    label "Polska"
  ]
  node [
    id 72
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 73
    label "wspania&#322;y"
  ]
  node [
    id 74
    label "doskona&#322;y"
  ]
  node [
    id 75
    label "kochany"
  ]
  node [
    id 76
    label "jednostka_monetarna"
  ]
  node [
    id 77
    label "z&#322;ocenie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 37
  ]
  edge [
    source 7
    target 38
  ]
  edge [
    source 7
    target 39
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 44
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 45
  ]
  edge [
    source 10
    target 46
  ]
  edge [
    source 10
    target 47
  ]
  edge [
    source 10
    target 48
  ]
  edge [
    source 10
    target 49
  ]
  edge [
    source 10
    target 50
  ]
  edge [
    source 10
    target 51
  ]
  edge [
    source 10
    target 52
  ]
  edge [
    source 10
    target 53
  ]
  edge [
    source 10
    target 54
  ]
  edge [
    source 10
    target 55
  ]
  edge [
    source 10
    target 56
  ]
  edge [
    source 10
    target 57
  ]
  edge [
    source 10
    target 58
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 60
  ]
  edge [
    source 11
    target 61
  ]
  edge [
    source 11
    target 62
  ]
  edge [
    source 12
    target 63
  ]
  edge [
    source 12
    target 64
  ]
  edge [
    source 12
    target 65
  ]
  edge [
    source 12
    target 66
  ]
  edge [
    source 12
    target 67
  ]
  edge [
    source 12
    target 68
  ]
  edge [
    source 12
    target 69
  ]
  edge [
    source 12
    target 70
  ]
  edge [
    source 12
    target 71
  ]
  edge [
    source 12
    target 72
  ]
  edge [
    source 12
    target 73
  ]
  edge [
    source 12
    target 74
  ]
  edge [
    source 12
    target 75
  ]
  edge [
    source 12
    target 76
  ]
  edge [
    source 12
    target 77
  ]
]
