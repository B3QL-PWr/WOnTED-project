graph [
  maxDegree 31
  minDegree 1
  meanDegree 10.448979591836734
  density 0.21768707482993196
  graphCliqueNumber 18
  node [
    id 0
    label "stanis&#322;aw"
    origin "text"
  ]
  node [
    id 1
    label "czajczy&#324;ski"
    origin "text"
  ]
  node [
    id 2
    label "Stanis&#322;awa"
  ]
  node [
    id 3
    label "Czajczy&#324;ski"
  ]
  node [
    id 4
    label "J&#243;zefa"
  ]
  node [
    id 5
    label "zak&#322;ad"
  ]
  node [
    id 6
    label "us&#322;ugi"
  ]
  node [
    id 7
    label "Techniczno"
  ]
  node [
    id 8
    label "handlowy"
  ]
  node [
    id 9
    label "Polmex"
  ]
  node [
    id 10
    label "sojusz"
  ]
  node [
    id 11
    label "lewica"
  ]
  node [
    id 12
    label "demokratyczny"
  ]
  node [
    id 13
    label "komisja"
  ]
  node [
    id 14
    label "nadzwyczajny"
  ]
  node [
    id 15
    label "do"
  ]
  node [
    id 16
    label "zbada&#263;"
  ]
  node [
    id 17
    label "zasadno&#347;&#263;"
  ]
  node [
    id 18
    label "zarzut"
  ]
  node [
    id 19
    label "wysoki"
  ]
  node [
    id 20
    label "izba"
  ]
  node [
    id 21
    label "kontrola"
  ]
  node [
    id 22
    label "wyspa"
  ]
  node [
    id 23
    label "stosunek"
  ]
  node [
    id 24
    label "niekt&#243;ry"
  ]
  node [
    id 25
    label "by&#322;y"
  ]
  node [
    id 26
    label "minister"
  ]
  node [
    id 27
    label "wsp&#243;&#322;praca"
  ]
  node [
    id 28
    label "gospodarczy"
  ]
  node [
    id 29
    label "zeszyt"
  ]
  node [
    id 30
    label "zagranica"
  ]
  node [
    id 31
    label "rozpatrzy&#263;"
  ]
  node [
    id 32
    label "pilny"
  ]
  node [
    id 33
    label "rz&#261;dowy"
  ]
  node [
    id 34
    label "oraz"
  ]
  node [
    id 35
    label "poselski"
  ]
  node [
    id 36
    label "projekt"
  ]
  node [
    id 37
    label "ustawi&#263;"
  ]
  node [
    id 38
    label "ojciec"
  ]
  node [
    id 39
    label "zam&#243;wienie"
  ]
  node [
    id 40
    label "publiczny"
  ]
  node [
    id 41
    label "stosunki"
  ]
  node [
    id 42
    label "zdrowie"
  ]
  node [
    id 43
    label "integracja"
  ]
  node [
    id 44
    label "europejski"
  ]
  node [
    id 45
    label "unia"
  ]
  node [
    id 46
    label "praca"
  ]
  node [
    id 47
    label "krzy&#380;mo"
  ]
  node [
    id 48
    label "zas&#322;uga"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 6
    target 9
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 13
    target 16
  ]
  edge [
    source 13
    target 17
  ]
  edge [
    source 13
    target 18
  ]
  edge [
    source 13
    target 19
  ]
  edge [
    source 13
    target 20
  ]
  edge [
    source 13
    target 21
  ]
  edge [
    source 13
    target 22
  ]
  edge [
    source 13
    target 23
  ]
  edge [
    source 13
    target 24
  ]
  edge [
    source 13
    target 25
  ]
  edge [
    source 13
    target 26
  ]
  edge [
    source 13
    target 27
  ]
  edge [
    source 13
    target 28
  ]
  edge [
    source 13
    target 29
  ]
  edge [
    source 13
    target 30
  ]
  edge [
    source 13
    target 31
  ]
  edge [
    source 13
    target 32
  ]
  edge [
    source 13
    target 33
  ]
  edge [
    source 13
    target 34
  ]
  edge [
    source 13
    target 35
  ]
  edge [
    source 13
    target 36
  ]
  edge [
    source 13
    target 37
  ]
  edge [
    source 13
    target 38
  ]
  edge [
    source 13
    target 39
  ]
  edge [
    source 13
    target 40
  ]
  edge [
    source 13
    target 41
  ]
  edge [
    source 13
    target 42
  ]
  edge [
    source 13
    target 43
  ]
  edge [
    source 13
    target 44
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 14
    target 17
  ]
  edge [
    source 14
    target 18
  ]
  edge [
    source 14
    target 19
  ]
  edge [
    source 14
    target 20
  ]
  edge [
    source 14
    target 21
  ]
  edge [
    source 14
    target 22
  ]
  edge [
    source 14
    target 23
  ]
  edge [
    source 14
    target 24
  ]
  edge [
    source 14
    target 25
  ]
  edge [
    source 14
    target 26
  ]
  edge [
    source 14
    target 27
  ]
  edge [
    source 14
    target 28
  ]
  edge [
    source 14
    target 29
  ]
  edge [
    source 14
    target 30
  ]
  edge [
    source 14
    target 31
  ]
  edge [
    source 14
    target 32
  ]
  edge [
    source 14
    target 33
  ]
  edge [
    source 14
    target 34
  ]
  edge [
    source 14
    target 35
  ]
  edge [
    source 14
    target 36
  ]
  edge [
    source 14
    target 37
  ]
  edge [
    source 14
    target 38
  ]
  edge [
    source 14
    target 39
  ]
  edge [
    source 14
    target 40
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 15
    target 18
  ]
  edge [
    source 15
    target 19
  ]
  edge [
    source 15
    target 20
  ]
  edge [
    source 15
    target 21
  ]
  edge [
    source 15
    target 22
  ]
  edge [
    source 15
    target 23
  ]
  edge [
    source 15
    target 15
  ]
  edge [
    source 15
    target 24
  ]
  edge [
    source 15
    target 25
  ]
  edge [
    source 15
    target 26
  ]
  edge [
    source 15
    target 27
  ]
  edge [
    source 15
    target 28
  ]
  edge [
    source 15
    target 29
  ]
  edge [
    source 15
    target 30
  ]
  edge [
    source 15
    target 31
  ]
  edge [
    source 15
    target 32
  ]
  edge [
    source 15
    target 33
  ]
  edge [
    source 15
    target 34
  ]
  edge [
    source 15
    target 35
  ]
  edge [
    source 15
    target 36
  ]
  edge [
    source 15
    target 37
  ]
  edge [
    source 15
    target 38
  ]
  edge [
    source 15
    target 39
  ]
  edge [
    source 15
    target 40
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 18
  ]
  edge [
    source 16
    target 19
  ]
  edge [
    source 16
    target 20
  ]
  edge [
    source 16
    target 21
  ]
  edge [
    source 16
    target 22
  ]
  edge [
    source 16
    target 23
  ]
  edge [
    source 16
    target 24
  ]
  edge [
    source 16
    target 25
  ]
  edge [
    source 16
    target 26
  ]
  edge [
    source 16
    target 27
  ]
  edge [
    source 16
    target 28
  ]
  edge [
    source 16
    target 29
  ]
  edge [
    source 16
    target 30
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 19
  ]
  edge [
    source 17
    target 20
  ]
  edge [
    source 17
    target 21
  ]
  edge [
    source 17
    target 22
  ]
  edge [
    source 17
    target 23
  ]
  edge [
    source 17
    target 24
  ]
  edge [
    source 17
    target 25
  ]
  edge [
    source 17
    target 26
  ]
  edge [
    source 17
    target 27
  ]
  edge [
    source 17
    target 28
  ]
  edge [
    source 17
    target 29
  ]
  edge [
    source 17
    target 30
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 20
  ]
  edge [
    source 18
    target 21
  ]
  edge [
    source 18
    target 22
  ]
  edge [
    source 18
    target 23
  ]
  edge [
    source 18
    target 24
  ]
  edge [
    source 18
    target 25
  ]
  edge [
    source 18
    target 26
  ]
  edge [
    source 18
    target 27
  ]
  edge [
    source 18
    target 28
  ]
  edge [
    source 18
    target 29
  ]
  edge [
    source 18
    target 30
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 21
  ]
  edge [
    source 19
    target 22
  ]
  edge [
    source 19
    target 23
  ]
  edge [
    source 19
    target 24
  ]
  edge [
    source 19
    target 25
  ]
  edge [
    source 19
    target 26
  ]
  edge [
    source 19
    target 27
  ]
  edge [
    source 19
    target 28
  ]
  edge [
    source 19
    target 29
  ]
  edge [
    source 19
    target 30
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 22
  ]
  edge [
    source 20
    target 23
  ]
  edge [
    source 20
    target 24
  ]
  edge [
    source 20
    target 25
  ]
  edge [
    source 20
    target 26
  ]
  edge [
    source 20
    target 27
  ]
  edge [
    source 20
    target 28
  ]
  edge [
    source 20
    target 29
  ]
  edge [
    source 20
    target 30
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 23
  ]
  edge [
    source 21
    target 24
  ]
  edge [
    source 21
    target 25
  ]
  edge [
    source 21
    target 26
  ]
  edge [
    source 21
    target 27
  ]
  edge [
    source 21
    target 28
  ]
  edge [
    source 21
    target 29
  ]
  edge [
    source 21
    target 30
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 24
  ]
  edge [
    source 22
    target 25
  ]
  edge [
    source 22
    target 26
  ]
  edge [
    source 22
    target 27
  ]
  edge [
    source 22
    target 28
  ]
  edge [
    source 22
    target 29
  ]
  edge [
    source 22
    target 30
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 25
  ]
  edge [
    source 23
    target 26
  ]
  edge [
    source 23
    target 27
  ]
  edge [
    source 23
    target 28
  ]
  edge [
    source 23
    target 29
  ]
  edge [
    source 23
    target 30
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 26
  ]
  edge [
    source 24
    target 27
  ]
  edge [
    source 24
    target 28
  ]
  edge [
    source 24
    target 29
  ]
  edge [
    source 24
    target 30
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 27
  ]
  edge [
    source 25
    target 28
  ]
  edge [
    source 25
    target 29
  ]
  edge [
    source 25
    target 30
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 28
  ]
  edge [
    source 26
    target 29
  ]
  edge [
    source 26
    target 30
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 29
  ]
  edge [
    source 27
    target 30
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 30
  ]
  edge [
    source 28
    target 41
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 41
  ]
  edge [
    source 30
    target 41
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 33
  ]
  edge [
    source 31
    target 34
  ]
  edge [
    source 31
    target 35
  ]
  edge [
    source 31
    target 36
  ]
  edge [
    source 31
    target 37
  ]
  edge [
    source 31
    target 38
  ]
  edge [
    source 31
    target 39
  ]
  edge [
    source 31
    target 40
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 34
  ]
  edge [
    source 32
    target 35
  ]
  edge [
    source 32
    target 36
  ]
  edge [
    source 32
    target 37
  ]
  edge [
    source 32
    target 38
  ]
  edge [
    source 32
    target 39
  ]
  edge [
    source 32
    target 40
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 35
  ]
  edge [
    source 33
    target 36
  ]
  edge [
    source 33
    target 37
  ]
  edge [
    source 33
    target 38
  ]
  edge [
    source 33
    target 39
  ]
  edge [
    source 33
    target 40
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 36
  ]
  edge [
    source 34
    target 37
  ]
  edge [
    source 34
    target 38
  ]
  edge [
    source 34
    target 39
  ]
  edge [
    source 34
    target 40
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 37
  ]
  edge [
    source 35
    target 38
  ]
  edge [
    source 35
    target 39
  ]
  edge [
    source 35
    target 40
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 38
  ]
  edge [
    source 36
    target 39
  ]
  edge [
    source 36
    target 40
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 39
  ]
  edge [
    source 37
    target 40
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 40
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 47
    target 48
  ]
]
