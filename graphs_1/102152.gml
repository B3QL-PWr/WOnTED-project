graph [
  maxDegree 41
  minDegree 1
  meanDegree 2.08
  density 0.011954022988505748
  graphCliqueNumber 3
  node [
    id 0
    label "klasa"
    origin "text"
  ]
  node [
    id 1
    label "przechowywa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "reprezentowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "dana"
    origin "text"
  ]
  node [
    id 4
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 5
    label "musza"
    origin "text"
  ]
  node [
    id 6
    label "by&#263;"
    origin "text"
  ]
  node [
    id 7
    label "przez"
    origin "text"
  ]
  node [
    id 8
    label "d&#322;ugi"
    origin "text"
  ]
  node [
    id 9
    label "czas"
    origin "text"
  ]
  node [
    id 10
    label "przyk&#322;ad"
    origin "text"
  ]
  node [
    id 11
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 12
    label "zawarto&#347;&#263;"
    origin "text"
  ]
  node [
    id 13
    label "tablica"
    origin "text"
  ]
  node [
    id 14
    label "baza"
    origin "text"
  ]
  node [
    id 15
    label "plik"
    origin "text"
  ]
  node [
    id 16
    label "typ"
  ]
  node [
    id 17
    label "warstwa"
  ]
  node [
    id 18
    label "znak_jako&#347;ci"
  ]
  node [
    id 19
    label "przedmiot"
  ]
  node [
    id 20
    label "przepisa&#263;"
  ]
  node [
    id 21
    label "grupa"
  ]
  node [
    id 22
    label "pomoc"
  ]
  node [
    id 23
    label "arrangement"
  ]
  node [
    id 24
    label "wagon"
  ]
  node [
    id 25
    label "form"
  ]
  node [
    id 26
    label "zaleta"
  ]
  node [
    id 27
    label "poziom"
  ]
  node [
    id 28
    label "dziennik_lekcyjny"
  ]
  node [
    id 29
    label "&#347;rodowisko"
  ]
  node [
    id 30
    label "atak"
  ]
  node [
    id 31
    label "przepisanie"
  ]
  node [
    id 32
    label "szko&#322;a"
  ]
  node [
    id 33
    label "class"
  ]
  node [
    id 34
    label "organizacja"
  ]
  node [
    id 35
    label "obrona"
  ]
  node [
    id 36
    label "type"
  ]
  node [
    id 37
    label "promocja"
  ]
  node [
    id 38
    label "&#322;awka"
  ]
  node [
    id 39
    label "kurs"
  ]
  node [
    id 40
    label "botanika"
  ]
  node [
    id 41
    label "sala"
  ]
  node [
    id 42
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 43
    label "gromada"
  ]
  node [
    id 44
    label "obiekt"
  ]
  node [
    id 45
    label "Ekwici"
  ]
  node [
    id 46
    label "fakcja"
  ]
  node [
    id 47
    label "programowanie_obiektowe"
  ]
  node [
    id 48
    label "wykrzyknik"
  ]
  node [
    id 49
    label "jednostka_systematyczna"
  ]
  node [
    id 50
    label "mecz_mistrzowski"
  ]
  node [
    id 51
    label "zbi&#243;r"
  ]
  node [
    id 52
    label "jako&#347;&#263;"
  ]
  node [
    id 53
    label "rezerwa"
  ]
  node [
    id 54
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 55
    label "continue"
  ]
  node [
    id 56
    label "hold"
  ]
  node [
    id 57
    label "zachowywa&#263;"
  ]
  node [
    id 58
    label "przetrzymywa&#263;"
  ]
  node [
    id 59
    label "chroni&#263;"
  ]
  node [
    id 60
    label "podtrzymywa&#263;"
  ]
  node [
    id 61
    label "act"
  ]
  node [
    id 62
    label "sprawowa&#263;"
  ]
  node [
    id 63
    label "wyra&#380;a&#263;"
  ]
  node [
    id 64
    label "represent"
  ]
  node [
    id 65
    label "dar"
  ]
  node [
    id 66
    label "cnota"
  ]
  node [
    id 67
    label "buddyzm"
  ]
  node [
    id 68
    label "si&#281;ga&#263;"
  ]
  node [
    id 69
    label "trwa&#263;"
  ]
  node [
    id 70
    label "obecno&#347;&#263;"
  ]
  node [
    id 71
    label "stan"
  ]
  node [
    id 72
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 73
    label "stand"
  ]
  node [
    id 74
    label "mie&#263;_miejsce"
  ]
  node [
    id 75
    label "uczestniczy&#263;"
  ]
  node [
    id 76
    label "chodzi&#263;"
  ]
  node [
    id 77
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 78
    label "equal"
  ]
  node [
    id 79
    label "daleki"
  ]
  node [
    id 80
    label "d&#322;ugo"
  ]
  node [
    id 81
    label "ruch"
  ]
  node [
    id 82
    label "czasokres"
  ]
  node [
    id 83
    label "trawienie"
  ]
  node [
    id 84
    label "kategoria_gramatyczna"
  ]
  node [
    id 85
    label "period"
  ]
  node [
    id 86
    label "odczyt"
  ]
  node [
    id 87
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 88
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 89
    label "chwila"
  ]
  node [
    id 90
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 91
    label "poprzedzenie"
  ]
  node [
    id 92
    label "koniugacja"
  ]
  node [
    id 93
    label "dzieje"
  ]
  node [
    id 94
    label "poprzedzi&#263;"
  ]
  node [
    id 95
    label "przep&#322;ywanie"
  ]
  node [
    id 96
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 97
    label "odwlekanie_si&#281;"
  ]
  node [
    id 98
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 99
    label "Zeitgeist"
  ]
  node [
    id 100
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 101
    label "okres_czasu"
  ]
  node [
    id 102
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 103
    label "pochodzi&#263;"
  ]
  node [
    id 104
    label "schy&#322;ek"
  ]
  node [
    id 105
    label "czwarty_wymiar"
  ]
  node [
    id 106
    label "chronometria"
  ]
  node [
    id 107
    label "poprzedzanie"
  ]
  node [
    id 108
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 109
    label "pogoda"
  ]
  node [
    id 110
    label "zegar"
  ]
  node [
    id 111
    label "trawi&#263;"
  ]
  node [
    id 112
    label "pochodzenie"
  ]
  node [
    id 113
    label "poprzedza&#263;"
  ]
  node [
    id 114
    label "time_period"
  ]
  node [
    id 115
    label "rachuba_czasu"
  ]
  node [
    id 116
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 117
    label "czasoprzestrze&#324;"
  ]
  node [
    id 118
    label "laba"
  ]
  node [
    id 119
    label "cz&#322;owiek"
  ]
  node [
    id 120
    label "czyn"
  ]
  node [
    id 121
    label "przedstawiciel"
  ]
  node [
    id 122
    label "ilustracja"
  ]
  node [
    id 123
    label "fakt"
  ]
  node [
    id 124
    label "wn&#281;trze"
  ]
  node [
    id 125
    label "temat"
  ]
  node [
    id 126
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 127
    label "informacja"
  ]
  node [
    id 128
    label "ilo&#347;&#263;"
  ]
  node [
    id 129
    label "spis"
  ]
  node [
    id 130
    label "uk&#322;ad"
  ]
  node [
    id 131
    label "kosz"
  ]
  node [
    id 132
    label "tarcza"
  ]
  node [
    id 133
    label "konstrukcja"
  ]
  node [
    id 134
    label "plate"
  ]
  node [
    id 135
    label "transparent"
  ]
  node [
    id 136
    label "kontener"
  ]
  node [
    id 137
    label "chart"
  ]
  node [
    id 138
    label "rubryka"
  ]
  node [
    id 139
    label "szachownica_Punnetta"
  ]
  node [
    id 140
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 141
    label "rozmiar&#243;wka"
  ]
  node [
    id 142
    label "p&#322;aszczyzna"
  ]
  node [
    id 143
    label "pole"
  ]
  node [
    id 144
    label "s&#322;owo_kluczowe"
  ]
  node [
    id 145
    label "za&#322;o&#380;enie"
  ]
  node [
    id 146
    label "podstawa"
  ]
  node [
    id 147
    label "rekord"
  ]
  node [
    id 148
    label "system_bazy_danych"
  ]
  node [
    id 149
    label "kosmetyk"
  ]
  node [
    id 150
    label "zasadzenie"
  ]
  node [
    id 151
    label "kolumna"
  ]
  node [
    id 152
    label "documentation"
  ]
  node [
    id 153
    label "base"
  ]
  node [
    id 154
    label "zasadzi&#263;"
  ]
  node [
    id 155
    label "baseball"
  ]
  node [
    id 156
    label "stacjonowanie"
  ]
  node [
    id 157
    label "informatyka"
  ]
  node [
    id 158
    label "punkt_odniesienia"
  ]
  node [
    id 159
    label "poj&#281;cie"
  ]
  node [
    id 160
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 161
    label "boisko"
  ]
  node [
    id 162
    label "miejsce"
  ]
  node [
    id 163
    label "podstawowy"
  ]
  node [
    id 164
    label "dokument"
  ]
  node [
    id 165
    label "menad&#380;er_plik&#243;w"
  ]
  node [
    id 166
    label "nadpisywanie"
  ]
  node [
    id 167
    label "nadpisanie"
  ]
  node [
    id 168
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 169
    label "nadpisa&#263;"
  ]
  node [
    id 170
    label "paczka"
  ]
  node [
    id 171
    label "podkatalog"
  ]
  node [
    id 172
    label "bundle"
  ]
  node [
    id 173
    label "folder"
  ]
  node [
    id 174
    label "nadpisywa&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 11
  ]
  edge [
    source 6
    target 12
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 9
    target 100
  ]
  edge [
    source 9
    target 101
  ]
  edge [
    source 9
    target 102
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 104
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 9
    target 116
  ]
  edge [
    source 9
    target 117
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 10
    target 121
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 124
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 129
  ]
  edge [
    source 13
    target 130
  ]
  edge [
    source 13
    target 131
  ]
  edge [
    source 13
    target 132
  ]
  edge [
    source 13
    target 133
  ]
  edge [
    source 13
    target 19
  ]
  edge [
    source 13
    target 134
  ]
  edge [
    source 13
    target 135
  ]
  edge [
    source 13
    target 136
  ]
  edge [
    source 13
    target 137
  ]
  edge [
    source 13
    target 138
  ]
  edge [
    source 13
    target 139
  ]
  edge [
    source 13
    target 140
  ]
  edge [
    source 13
    target 141
  ]
  edge [
    source 13
    target 32
  ]
  edge [
    source 13
    target 142
  ]
  edge [
    source 14
    target 143
  ]
  edge [
    source 14
    target 144
  ]
  edge [
    source 14
    target 145
  ]
  edge [
    source 14
    target 146
  ]
  edge [
    source 14
    target 147
  ]
  edge [
    source 14
    target 148
  ]
  edge [
    source 14
    target 149
  ]
  edge [
    source 14
    target 150
  ]
  edge [
    source 14
    target 151
  ]
  edge [
    source 14
    target 152
  ]
  edge [
    source 14
    target 153
  ]
  edge [
    source 14
    target 154
  ]
  edge [
    source 14
    target 155
  ]
  edge [
    source 14
    target 156
  ]
  edge [
    source 14
    target 157
  ]
  edge [
    source 14
    target 158
  ]
  edge [
    source 14
    target 159
  ]
  edge [
    source 14
    target 160
  ]
  edge [
    source 14
    target 161
  ]
  edge [
    source 14
    target 162
  ]
  edge [
    source 14
    target 163
  ]
  edge [
    source 14
    target 51
  ]
  edge [
    source 15
    target 164
  ]
  edge [
    source 15
    target 165
  ]
  edge [
    source 15
    target 166
  ]
  edge [
    source 15
    target 167
  ]
  edge [
    source 15
    target 168
  ]
  edge [
    source 15
    target 169
  ]
  edge [
    source 15
    target 170
  ]
  edge [
    source 15
    target 171
  ]
  edge [
    source 15
    target 172
  ]
  edge [
    source 15
    target 173
  ]
  edge [
    source 15
    target 174
  ]
]
