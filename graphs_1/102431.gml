graph [
  maxDegree 7
  minDegree 1
  meanDegree 1.9047619047619047
  density 0.09523809523809523
  graphCliqueNumber 2
  node [
    id 0
    label "licklider"
    origin "text"
  ]
  node [
    id 1
    label "taylor"
    origin "text"
  ]
  node [
    id 2
    label "nasta&#263;"
    origin "text"
  ]
  node [
    id 3
    label "powszechny"
    origin "text"
  ]
  node [
    id 4
    label "cyfrowy"
    origin "text"
  ]
  node [
    id 5
    label "robot"
    origin "text"
  ]
  node [
    id 6
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 7
    label "supervene"
  ]
  node [
    id 8
    label "zbiorowy"
  ]
  node [
    id 9
    label "generalny"
  ]
  node [
    id 10
    label "cz&#281;sty"
  ]
  node [
    id 11
    label "znany"
  ]
  node [
    id 12
    label "powszechnie"
  ]
  node [
    id 13
    label "cyfrowo"
  ]
  node [
    id 14
    label "sygna&#322;_cyfrowy"
  ]
  node [
    id 15
    label "elektroniczny"
  ]
  node [
    id 16
    label "cyfryzacja"
  ]
  node [
    id 17
    label "urz&#261;dzenie_kuchenne"
  ]
  node [
    id 18
    label "maszyna"
  ]
  node [
    id 19
    label "sprz&#281;t_AGD"
  ]
  node [
    id 20
    label "automat"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 20
  ]
]
