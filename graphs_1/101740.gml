graph [
  maxDegree 324
  minDegree 1
  meanDegree 2.0869565217391304
  density 0.004132587171760654
  graphCliqueNumber 4
  node [
    id 0
    label "recenzowa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "gatunek"
    origin "text"
  ]
  node [
    id 2
    label "flora"
    origin "text"
  ]
  node [
    id 3
    label "ober"
    origin "text"
  ]
  node [
    id 4
    label "troszk&#281;"
    origin "text"
  ]
  node [
    id 5
    label "test"
    origin "text"
  ]
  node [
    id 6
    label "zaszkodzi&#263;"
    origin "text"
  ]
  node [
    id 7
    label "ostatnio"
    origin "text"
  ]
  node [
    id 8
    label "pija&#263;"
    origin "text"
  ]
  node [
    id 9
    label "przer&#243;&#380;ny"
    origin "text"
  ]
  node [
    id 10
    label "yerba"
    origin "text"
  ]
  node [
    id 11
    label "mate"
    origin "text"
  ]
  node [
    id 12
    label "postanowi&#263;by&#263;"
    origin "text"
  ]
  node [
    id 13
    label "stworzy&#263;"
    origin "text"
  ]
  node [
    id 14
    label "cykl"
    origin "text"
  ]
  node [
    id 15
    label "cela"
    origin "text"
  ]
  node [
    id 16
    label "przedstawi&#263;"
    origin "text"
  ]
  node [
    id 17
    label "moja"
    origin "text"
  ]
  node [
    id 18
    label "subiektywny"
    origin "text"
  ]
  node [
    id 19
    label "odczucie"
    origin "text"
  ]
  node [
    id 20
    label "temat"
    origin "text"
  ]
  node [
    id 21
    label "r&#243;&#380;niastych"
    origin "text"
  ]
  node [
    id 22
    label "pocz&#261;tek"
    origin "text"
  ]
  node [
    id 23
    label "chcie&#263;byby&#263;"
    origin "text"
  ]
  node [
    id 24
    label "opisa&#263;"
    origin "text"
  ]
  node [
    id 25
    label "ma&#322;o"
    origin "text"
  ]
  node [
    id 26
    label "popularny"
    origin "text"
  ]
  node [
    id 27
    label "nasi"
    origin "text"
  ]
  node [
    id 28
    label "kraj"
    origin "text"
  ]
  node [
    id 29
    label "mianowicie"
    origin "text"
  ]
  node [
    id 30
    label "rap"
  ]
  node [
    id 31
    label "opiniowa&#263;"
  ]
  node [
    id 32
    label "autorament"
  ]
  node [
    id 33
    label "znak_jako&#347;ci"
  ]
  node [
    id 34
    label "jednostka_systematyczna"
  ]
  node [
    id 35
    label "rodzaj"
  ]
  node [
    id 36
    label "human_body"
  ]
  node [
    id 37
    label "jako&#347;&#263;"
  ]
  node [
    id 38
    label "variety"
  ]
  node [
    id 39
    label "filiacja"
  ]
  node [
    id 40
    label "formacja_ro&#347;linna"
  ]
  node [
    id 41
    label "biom"
  ]
  node [
    id 42
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 43
    label "zielono&#347;&#263;"
  ]
  node [
    id 44
    label "szata_ro&#347;linna"
  ]
  node [
    id 45
    label "geosystem"
  ]
  node [
    id 46
    label "ro&#347;lina"
  ]
  node [
    id 47
    label "przyroda"
  ]
  node [
    id 48
    label "zbi&#243;r"
  ]
  node [
    id 49
    label "plant"
  ]
  node [
    id 50
    label "pi&#281;tro"
  ]
  node [
    id 51
    label "kelner"
  ]
  node [
    id 52
    label "do&#347;wiadczenie"
  ]
  node [
    id 53
    label "arkusz"
  ]
  node [
    id 54
    label "sprawdzian"
  ]
  node [
    id 55
    label "quiz"
  ]
  node [
    id 56
    label "przechodzi&#263;"
  ]
  node [
    id 57
    label "przechodzenie"
  ]
  node [
    id 58
    label "badanie"
  ]
  node [
    id 59
    label "narz&#281;dzie"
  ]
  node [
    id 60
    label "sytuacja"
  ]
  node [
    id 61
    label "zrobi&#263;"
  ]
  node [
    id 62
    label "hurt"
  ]
  node [
    id 63
    label "spowodowa&#263;"
  ]
  node [
    id 64
    label "injury"
  ]
  node [
    id 65
    label "ostatni"
  ]
  node [
    id 66
    label "poprzednio"
  ]
  node [
    id 67
    label "aktualnie"
  ]
  node [
    id 68
    label "drink"
  ]
  node [
    id 69
    label "spo&#380;ywa&#263;"
  ]
  node [
    id 70
    label "jaki&#347;"
  ]
  node [
    id 71
    label "przer&#243;&#380;nie"
  ]
  node [
    id 72
    label "u&#380;ywka"
  ]
  node [
    id 73
    label "herbata"
  ]
  node [
    id 74
    label "kofeina"
  ]
  node [
    id 75
    label "egzotyk"
  ]
  node [
    id 76
    label "bombilla"
  ]
  node [
    id 77
    label "kwas_chlorogenowy"
  ]
  node [
    id 78
    label "ostrokrzew"
  ]
  node [
    id 79
    label "susz"
  ]
  node [
    id 80
    label "drzewo"
  ]
  node [
    id 81
    label "zawarto&#347;&#263;"
  ]
  node [
    id 82
    label "naczynie"
  ]
  node [
    id 83
    label "przygotowa&#263;"
  ]
  node [
    id 84
    label "specjalista_od_public_relations"
  ]
  node [
    id 85
    label "create"
  ]
  node [
    id 86
    label "wizerunek"
  ]
  node [
    id 87
    label "sekwencja"
  ]
  node [
    id 88
    label "czas"
  ]
  node [
    id 89
    label "edycja"
  ]
  node [
    id 90
    label "przebieg"
  ]
  node [
    id 91
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 92
    label "okres"
  ]
  node [
    id 93
    label "cykl_p&#322;ciowy"
  ]
  node [
    id 94
    label "cycle"
  ]
  node [
    id 95
    label "owulacja"
  ]
  node [
    id 96
    label "miesi&#261;czka"
  ]
  node [
    id 97
    label "set"
  ]
  node [
    id 98
    label "pomieszczenie"
  ]
  node [
    id 99
    label "klasztor"
  ]
  node [
    id 100
    label "przedstawienie"
  ]
  node [
    id 101
    label "express"
  ]
  node [
    id 102
    label "typify"
  ]
  node [
    id 103
    label "ukaza&#263;"
  ]
  node [
    id 104
    label "pokaza&#263;"
  ]
  node [
    id 105
    label "represent"
  ]
  node [
    id 106
    label "zapozna&#263;"
  ]
  node [
    id 107
    label "zaproponowa&#263;"
  ]
  node [
    id 108
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 109
    label "zademonstrowa&#263;"
  ]
  node [
    id 110
    label "poda&#263;"
  ]
  node [
    id 111
    label "nieobiektywny"
  ]
  node [
    id 112
    label "indywidualny"
  ]
  node [
    id 113
    label "zsubiektywizowanie"
  ]
  node [
    id 114
    label "subiektywizowanie"
  ]
  node [
    id 115
    label "subiektywnie"
  ]
  node [
    id 116
    label "feel"
  ]
  node [
    id 117
    label "reakcja"
  ]
  node [
    id 118
    label "odczucia"
  ]
  node [
    id 119
    label "zareagowanie"
  ]
  node [
    id 120
    label "postrze&#380;enie"
  ]
  node [
    id 121
    label "opanowanie"
  ]
  node [
    id 122
    label "doznanie"
  ]
  node [
    id 123
    label "czucie"
  ]
  node [
    id 124
    label "konsekwencja"
  ]
  node [
    id 125
    label "poczucie"
  ]
  node [
    id 126
    label "os&#322;upienie"
  ]
  node [
    id 127
    label "przeczulica"
  ]
  node [
    id 128
    label "zmys&#322;"
  ]
  node [
    id 129
    label "proces"
  ]
  node [
    id 130
    label "zdarzenie_si&#281;"
  ]
  node [
    id 131
    label "smell"
  ]
  node [
    id 132
    label "zjawisko"
  ]
  node [
    id 133
    label "fraza"
  ]
  node [
    id 134
    label "forma"
  ]
  node [
    id 135
    label "melodia"
  ]
  node [
    id 136
    label "rzecz"
  ]
  node [
    id 137
    label "zbacza&#263;"
  ]
  node [
    id 138
    label "entity"
  ]
  node [
    id 139
    label "omawia&#263;"
  ]
  node [
    id 140
    label "topik"
  ]
  node [
    id 141
    label "wyraz_pochodny"
  ]
  node [
    id 142
    label "om&#243;wi&#263;"
  ]
  node [
    id 143
    label "omawianie"
  ]
  node [
    id 144
    label "w&#261;tek"
  ]
  node [
    id 145
    label "forum"
  ]
  node [
    id 146
    label "cecha"
  ]
  node [
    id 147
    label "zboczenie"
  ]
  node [
    id 148
    label "zbaczanie"
  ]
  node [
    id 149
    label "tre&#347;&#263;"
  ]
  node [
    id 150
    label "tematyka"
  ]
  node [
    id 151
    label "sprawa"
  ]
  node [
    id 152
    label "istota"
  ]
  node [
    id 153
    label "otoczka"
  ]
  node [
    id 154
    label "zboczy&#263;"
  ]
  node [
    id 155
    label "om&#243;wienie"
  ]
  node [
    id 156
    label "miejsce"
  ]
  node [
    id 157
    label "faza"
  ]
  node [
    id 158
    label "upgrade"
  ]
  node [
    id 159
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 160
    label "pierworodztwo"
  ]
  node [
    id 161
    label "nast&#281;pstwo"
  ]
  node [
    id 162
    label "zinterpretowa&#263;"
  ]
  node [
    id 163
    label "relate"
  ]
  node [
    id 164
    label "delineate"
  ]
  node [
    id 165
    label "mo&#380;liwie"
  ]
  node [
    id 166
    label "nieznaczny"
  ]
  node [
    id 167
    label "kr&#243;tko"
  ]
  node [
    id 168
    label "nieistotnie"
  ]
  node [
    id 169
    label "nieliczny"
  ]
  node [
    id 170
    label "mikroskopijnie"
  ]
  node [
    id 171
    label "pomiernie"
  ]
  node [
    id 172
    label "ma&#322;y"
  ]
  node [
    id 173
    label "przyst&#281;pny"
  ]
  node [
    id 174
    label "&#322;atwy"
  ]
  node [
    id 175
    label "popularnie"
  ]
  node [
    id 176
    label "znany"
  ]
  node [
    id 177
    label "Skandynawia"
  ]
  node [
    id 178
    label "Filipiny"
  ]
  node [
    id 179
    label "Rwanda"
  ]
  node [
    id 180
    label "Kaukaz"
  ]
  node [
    id 181
    label "Kaszmir"
  ]
  node [
    id 182
    label "Toskania"
  ]
  node [
    id 183
    label "Yorkshire"
  ]
  node [
    id 184
    label "&#321;emkowszczyzna"
  ]
  node [
    id 185
    label "obszar"
  ]
  node [
    id 186
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 187
    label "Monako"
  ]
  node [
    id 188
    label "Amhara"
  ]
  node [
    id 189
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 190
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 191
    label "Lombardia"
  ]
  node [
    id 192
    label "Korea"
  ]
  node [
    id 193
    label "Kalabria"
  ]
  node [
    id 194
    label "Ghana"
  ]
  node [
    id 195
    label "Czarnog&#243;ra"
  ]
  node [
    id 196
    label "Tyrol"
  ]
  node [
    id 197
    label "Malawi"
  ]
  node [
    id 198
    label "Indonezja"
  ]
  node [
    id 199
    label "Bu&#322;garia"
  ]
  node [
    id 200
    label "Nauru"
  ]
  node [
    id 201
    label "Kenia"
  ]
  node [
    id 202
    label "Pamir"
  ]
  node [
    id 203
    label "Kambod&#380;a"
  ]
  node [
    id 204
    label "Lubelszczyzna"
  ]
  node [
    id 205
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 206
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 207
    label "Mali"
  ]
  node [
    id 208
    label "&#379;ywiecczyzna"
  ]
  node [
    id 209
    label "Austria"
  ]
  node [
    id 210
    label "interior"
  ]
  node [
    id 211
    label "Europa_Wschodnia"
  ]
  node [
    id 212
    label "Armenia"
  ]
  node [
    id 213
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 214
    label "Fid&#380;i"
  ]
  node [
    id 215
    label "Tuwalu"
  ]
  node [
    id 216
    label "Zabajkale"
  ]
  node [
    id 217
    label "Etiopia"
  ]
  node [
    id 218
    label "Malta"
  ]
  node [
    id 219
    label "Malezja"
  ]
  node [
    id 220
    label "Kaszuby"
  ]
  node [
    id 221
    label "Bo&#347;nia"
  ]
  node [
    id 222
    label "Noworosja"
  ]
  node [
    id 223
    label "Grenada"
  ]
  node [
    id 224
    label "Tad&#380;ykistan"
  ]
  node [
    id 225
    label "Ba&#322;kany"
  ]
  node [
    id 226
    label "Wehrlen"
  ]
  node [
    id 227
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 228
    label "Anglia"
  ]
  node [
    id 229
    label "Kielecczyzna"
  ]
  node [
    id 230
    label "Rumunia"
  ]
  node [
    id 231
    label "Pomorze_Zachodnie"
  ]
  node [
    id 232
    label "Maroko"
  ]
  node [
    id 233
    label "Bhutan"
  ]
  node [
    id 234
    label "Opolskie"
  ]
  node [
    id 235
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 236
    label "Ko&#322;yma"
  ]
  node [
    id 237
    label "Oksytania"
  ]
  node [
    id 238
    label "S&#322;owacja"
  ]
  node [
    id 239
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 240
    label "Seszele"
  ]
  node [
    id 241
    label "Syjon"
  ]
  node [
    id 242
    label "Kuwejt"
  ]
  node [
    id 243
    label "Arabia_Saudyjska"
  ]
  node [
    id 244
    label "Kociewie"
  ]
  node [
    id 245
    label "Ekwador"
  ]
  node [
    id 246
    label "Kanada"
  ]
  node [
    id 247
    label "ziemia"
  ]
  node [
    id 248
    label "Japonia"
  ]
  node [
    id 249
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 250
    label "Hiszpania"
  ]
  node [
    id 251
    label "Wyspy_Marshalla"
  ]
  node [
    id 252
    label "Botswana"
  ]
  node [
    id 253
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 254
    label "D&#380;ibuti"
  ]
  node [
    id 255
    label "Huculszczyzna"
  ]
  node [
    id 256
    label "Wietnam"
  ]
  node [
    id 257
    label "Egipt"
  ]
  node [
    id 258
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 259
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 260
    label "Burkina_Faso"
  ]
  node [
    id 261
    label "Bawaria"
  ]
  node [
    id 262
    label "Niemcy"
  ]
  node [
    id 263
    label "Khitai"
  ]
  node [
    id 264
    label "Macedonia"
  ]
  node [
    id 265
    label "Albania"
  ]
  node [
    id 266
    label "Madagaskar"
  ]
  node [
    id 267
    label "Bahrajn"
  ]
  node [
    id 268
    label "Jemen"
  ]
  node [
    id 269
    label "Lesoto"
  ]
  node [
    id 270
    label "Maghreb"
  ]
  node [
    id 271
    label "Samoa"
  ]
  node [
    id 272
    label "Andora"
  ]
  node [
    id 273
    label "Bory_Tucholskie"
  ]
  node [
    id 274
    label "Chiny"
  ]
  node [
    id 275
    label "Europa_Zachodnia"
  ]
  node [
    id 276
    label "Cypr"
  ]
  node [
    id 277
    label "Wielka_Brytania"
  ]
  node [
    id 278
    label "Kerala"
  ]
  node [
    id 279
    label "Podhale"
  ]
  node [
    id 280
    label "Kabylia"
  ]
  node [
    id 281
    label "Ukraina"
  ]
  node [
    id 282
    label "Paragwaj"
  ]
  node [
    id 283
    label "Trynidad_i_Tobago"
  ]
  node [
    id 284
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 285
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 286
    label "Ma&#322;opolska"
  ]
  node [
    id 287
    label "Polesie"
  ]
  node [
    id 288
    label "Liguria"
  ]
  node [
    id 289
    label "Libia"
  ]
  node [
    id 290
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 291
    label "&#321;&#243;dzkie"
  ]
  node [
    id 292
    label "Surinam"
  ]
  node [
    id 293
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 294
    label "Palestyna"
  ]
  node [
    id 295
    label "Australia"
  ]
  node [
    id 296
    label "Nigeria"
  ]
  node [
    id 297
    label "Honduras"
  ]
  node [
    id 298
    label "Bojkowszczyzna"
  ]
  node [
    id 299
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 300
    label "Karaiby"
  ]
  node [
    id 301
    label "Bangladesz"
  ]
  node [
    id 302
    label "Peru"
  ]
  node [
    id 303
    label "Kazachstan"
  ]
  node [
    id 304
    label "USA"
  ]
  node [
    id 305
    label "Irak"
  ]
  node [
    id 306
    label "Nepal"
  ]
  node [
    id 307
    label "S&#261;decczyzna"
  ]
  node [
    id 308
    label "Sudan"
  ]
  node [
    id 309
    label "Sand&#380;ak"
  ]
  node [
    id 310
    label "Nadrenia"
  ]
  node [
    id 311
    label "San_Marino"
  ]
  node [
    id 312
    label "Burundi"
  ]
  node [
    id 313
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 314
    label "Dominikana"
  ]
  node [
    id 315
    label "Komory"
  ]
  node [
    id 316
    label "Zakarpacie"
  ]
  node [
    id 317
    label "Gwatemala"
  ]
  node [
    id 318
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 319
    label "Zag&#243;rze"
  ]
  node [
    id 320
    label "Andaluzja"
  ]
  node [
    id 321
    label "granica_pa&#324;stwa"
  ]
  node [
    id 322
    label "Turkiestan"
  ]
  node [
    id 323
    label "Naddniestrze"
  ]
  node [
    id 324
    label "Hercegowina"
  ]
  node [
    id 325
    label "Brunei"
  ]
  node [
    id 326
    label "Iran"
  ]
  node [
    id 327
    label "jednostka_administracyjna"
  ]
  node [
    id 328
    label "Zimbabwe"
  ]
  node [
    id 329
    label "Namibia"
  ]
  node [
    id 330
    label "Meksyk"
  ]
  node [
    id 331
    label "Lotaryngia"
  ]
  node [
    id 332
    label "Kamerun"
  ]
  node [
    id 333
    label "Opolszczyzna"
  ]
  node [
    id 334
    label "Afryka_Wschodnia"
  ]
  node [
    id 335
    label "Szlezwik"
  ]
  node [
    id 336
    label "Somalia"
  ]
  node [
    id 337
    label "Angola"
  ]
  node [
    id 338
    label "Gabon"
  ]
  node [
    id 339
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 340
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 341
    label "Mozambik"
  ]
  node [
    id 342
    label "Tajwan"
  ]
  node [
    id 343
    label "Tunezja"
  ]
  node [
    id 344
    label "Nowa_Zelandia"
  ]
  node [
    id 345
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 346
    label "Podbeskidzie"
  ]
  node [
    id 347
    label "Liban"
  ]
  node [
    id 348
    label "Jordania"
  ]
  node [
    id 349
    label "Tonga"
  ]
  node [
    id 350
    label "Czad"
  ]
  node [
    id 351
    label "Liberia"
  ]
  node [
    id 352
    label "Gwinea"
  ]
  node [
    id 353
    label "Belize"
  ]
  node [
    id 354
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 355
    label "Mazowsze"
  ]
  node [
    id 356
    label "&#321;otwa"
  ]
  node [
    id 357
    label "Syria"
  ]
  node [
    id 358
    label "Benin"
  ]
  node [
    id 359
    label "Afryka_Zachodnia"
  ]
  node [
    id 360
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 361
    label "Dominika"
  ]
  node [
    id 362
    label "Antigua_i_Barbuda"
  ]
  node [
    id 363
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 364
    label "Hanower"
  ]
  node [
    id 365
    label "Galicja"
  ]
  node [
    id 366
    label "Szkocja"
  ]
  node [
    id 367
    label "Walia"
  ]
  node [
    id 368
    label "Afganistan"
  ]
  node [
    id 369
    label "Kiribati"
  ]
  node [
    id 370
    label "W&#322;ochy"
  ]
  node [
    id 371
    label "Szwajcaria"
  ]
  node [
    id 372
    label "Powi&#347;le"
  ]
  node [
    id 373
    label "Sahara_Zachodnia"
  ]
  node [
    id 374
    label "Chorwacja"
  ]
  node [
    id 375
    label "Tajlandia"
  ]
  node [
    id 376
    label "Salwador"
  ]
  node [
    id 377
    label "Bahamy"
  ]
  node [
    id 378
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 379
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 380
    label "Zamojszczyzna"
  ]
  node [
    id 381
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 382
    label "S&#322;owenia"
  ]
  node [
    id 383
    label "Gambia"
  ]
  node [
    id 384
    label "Kujawy"
  ]
  node [
    id 385
    label "Urugwaj"
  ]
  node [
    id 386
    label "Podlasie"
  ]
  node [
    id 387
    label "Zair"
  ]
  node [
    id 388
    label "Erytrea"
  ]
  node [
    id 389
    label "Laponia"
  ]
  node [
    id 390
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 391
    label "Umbria"
  ]
  node [
    id 392
    label "Rosja"
  ]
  node [
    id 393
    label "Uganda"
  ]
  node [
    id 394
    label "Niger"
  ]
  node [
    id 395
    label "Mauritius"
  ]
  node [
    id 396
    label "Turkmenistan"
  ]
  node [
    id 397
    label "Turcja"
  ]
  node [
    id 398
    label "Mezoameryka"
  ]
  node [
    id 399
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 400
    label "Irlandia"
  ]
  node [
    id 401
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 402
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 403
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 404
    label "Gwinea_Bissau"
  ]
  node [
    id 405
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 406
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 407
    label "Kurdystan"
  ]
  node [
    id 408
    label "Belgia"
  ]
  node [
    id 409
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 410
    label "Palau"
  ]
  node [
    id 411
    label "Barbados"
  ]
  node [
    id 412
    label "Chile"
  ]
  node [
    id 413
    label "Wenezuela"
  ]
  node [
    id 414
    label "W&#281;gry"
  ]
  node [
    id 415
    label "Argentyna"
  ]
  node [
    id 416
    label "Kolumbia"
  ]
  node [
    id 417
    label "Kampania"
  ]
  node [
    id 418
    label "Armagnac"
  ]
  node [
    id 419
    label "Sierra_Leone"
  ]
  node [
    id 420
    label "Azerbejd&#380;an"
  ]
  node [
    id 421
    label "Kongo"
  ]
  node [
    id 422
    label "Polinezja"
  ]
  node [
    id 423
    label "Warmia"
  ]
  node [
    id 424
    label "Pakistan"
  ]
  node [
    id 425
    label "Liechtenstein"
  ]
  node [
    id 426
    label "Wielkopolska"
  ]
  node [
    id 427
    label "Nikaragua"
  ]
  node [
    id 428
    label "Senegal"
  ]
  node [
    id 429
    label "brzeg"
  ]
  node [
    id 430
    label "Bordeaux"
  ]
  node [
    id 431
    label "Lauda"
  ]
  node [
    id 432
    label "Indie"
  ]
  node [
    id 433
    label "Mazury"
  ]
  node [
    id 434
    label "Suazi"
  ]
  node [
    id 435
    label "Polska"
  ]
  node [
    id 436
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 437
    label "Algieria"
  ]
  node [
    id 438
    label "Jamajka"
  ]
  node [
    id 439
    label "Timor_Wschodni"
  ]
  node [
    id 440
    label "Oceania"
  ]
  node [
    id 441
    label "Kostaryka"
  ]
  node [
    id 442
    label "Podkarpacie"
  ]
  node [
    id 443
    label "Lasko"
  ]
  node [
    id 444
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 445
    label "Kuba"
  ]
  node [
    id 446
    label "Mauretania"
  ]
  node [
    id 447
    label "Amazonia"
  ]
  node [
    id 448
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 449
    label "Portoryko"
  ]
  node [
    id 450
    label "Brazylia"
  ]
  node [
    id 451
    label "Mo&#322;dawia"
  ]
  node [
    id 452
    label "organizacja"
  ]
  node [
    id 453
    label "Litwa"
  ]
  node [
    id 454
    label "Kirgistan"
  ]
  node [
    id 455
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 456
    label "Izrael"
  ]
  node [
    id 457
    label "Grecja"
  ]
  node [
    id 458
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 459
    label "Kurpie"
  ]
  node [
    id 460
    label "Holandia"
  ]
  node [
    id 461
    label "Sri_Lanka"
  ]
  node [
    id 462
    label "Tonkin"
  ]
  node [
    id 463
    label "Katar"
  ]
  node [
    id 464
    label "Azja_Wschodnia"
  ]
  node [
    id 465
    label "Mikronezja"
  ]
  node [
    id 466
    label "Ukraina_Zachodnia"
  ]
  node [
    id 467
    label "Laos"
  ]
  node [
    id 468
    label "Mongolia"
  ]
  node [
    id 469
    label "Turyngia"
  ]
  node [
    id 470
    label "Malediwy"
  ]
  node [
    id 471
    label "Zambia"
  ]
  node [
    id 472
    label "Baszkiria"
  ]
  node [
    id 473
    label "Tanzania"
  ]
  node [
    id 474
    label "Gujana"
  ]
  node [
    id 475
    label "Apulia"
  ]
  node [
    id 476
    label "Czechy"
  ]
  node [
    id 477
    label "Panama"
  ]
  node [
    id 478
    label "Uzbekistan"
  ]
  node [
    id 479
    label "Gruzja"
  ]
  node [
    id 480
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 481
    label "Serbia"
  ]
  node [
    id 482
    label "Francja"
  ]
  node [
    id 483
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 484
    label "Togo"
  ]
  node [
    id 485
    label "Estonia"
  ]
  node [
    id 486
    label "Indochiny"
  ]
  node [
    id 487
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 488
    label "Oman"
  ]
  node [
    id 489
    label "Boliwia"
  ]
  node [
    id 490
    label "Portugalia"
  ]
  node [
    id 491
    label "Wyspy_Salomona"
  ]
  node [
    id 492
    label "Luksemburg"
  ]
  node [
    id 493
    label "Haiti"
  ]
  node [
    id 494
    label "Biskupizna"
  ]
  node [
    id 495
    label "Lubuskie"
  ]
  node [
    id 496
    label "Birma"
  ]
  node [
    id 497
    label "Rodezja"
  ]
  node [
    id 498
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 499
    label "de"
  ]
  node [
    id 500
    label "la"
  ]
  node [
    id 501
    label "mulat"
  ]
  node [
    id 502
    label "aleja"
  ]
  node [
    id 503
    label "17"
  ]
  node [
    id 504
    label "&#347;wi&#281;ty"
  ]
  node [
    id 505
    label "Tomasz"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 499
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 499
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 10
    target 46
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 72
  ]
  edge [
    source 11
    target 81
  ]
  edge [
    source 11
    target 82
  ]
  edge [
    source 11
    target 46
  ]
  edge [
    source 11
    target 73
  ]
  edge [
    source 11
    target 74
  ]
  edge [
    source 11
    target 75
  ]
  edge [
    source 11
    target 76
  ]
  edge [
    source 11
    target 77
  ]
  edge [
    source 11
    target 78
  ]
  edge [
    source 11
    target 79
  ]
  edge [
    source 11
    target 80
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 29
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 83
  ]
  edge [
    source 13
    target 84
  ]
  edge [
    source 13
    target 85
  ]
  edge [
    source 13
    target 61
  ]
  edge [
    source 13
    target 86
  ]
  edge [
    source 13
    target 16
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 87
  ]
  edge [
    source 14
    target 88
  ]
  edge [
    source 14
    target 89
  ]
  edge [
    source 14
    target 90
  ]
  edge [
    source 14
    target 91
  ]
  edge [
    source 14
    target 92
  ]
  edge [
    source 14
    target 93
  ]
  edge [
    source 14
    target 94
  ]
  edge [
    source 14
    target 95
  ]
  edge [
    source 14
    target 96
  ]
  edge [
    source 14
    target 97
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 98
  ]
  edge [
    source 15
    target 99
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 100
  ]
  edge [
    source 16
    target 101
  ]
  edge [
    source 16
    target 102
  ]
  edge [
    source 16
    target 24
  ]
  edge [
    source 16
    target 103
  ]
  edge [
    source 16
    target 104
  ]
  edge [
    source 16
    target 105
  ]
  edge [
    source 16
    target 106
  ]
  edge [
    source 16
    target 107
  ]
  edge [
    source 16
    target 108
  ]
  edge [
    source 16
    target 109
  ]
  edge [
    source 16
    target 110
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 111
  ]
  edge [
    source 18
    target 112
  ]
  edge [
    source 18
    target 113
  ]
  edge [
    source 18
    target 114
  ]
  edge [
    source 18
    target 115
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 116
  ]
  edge [
    source 19
    target 117
  ]
  edge [
    source 19
    target 118
  ]
  edge [
    source 19
    target 119
  ]
  edge [
    source 19
    target 120
  ]
  edge [
    source 19
    target 121
  ]
  edge [
    source 19
    target 122
  ]
  edge [
    source 19
    target 123
  ]
  edge [
    source 19
    target 124
  ]
  edge [
    source 19
    target 125
  ]
  edge [
    source 19
    target 126
  ]
  edge [
    source 19
    target 127
  ]
  edge [
    source 19
    target 128
  ]
  edge [
    source 19
    target 129
  ]
  edge [
    source 19
    target 130
  ]
  edge [
    source 19
    target 131
  ]
  edge [
    source 19
    target 132
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 133
  ]
  edge [
    source 20
    target 134
  ]
  edge [
    source 20
    target 135
  ]
  edge [
    source 20
    target 136
  ]
  edge [
    source 20
    target 137
  ]
  edge [
    source 20
    target 138
  ]
  edge [
    source 20
    target 139
  ]
  edge [
    source 20
    target 140
  ]
  edge [
    source 20
    target 141
  ]
  edge [
    source 20
    target 142
  ]
  edge [
    source 20
    target 143
  ]
  edge [
    source 20
    target 144
  ]
  edge [
    source 20
    target 145
  ]
  edge [
    source 20
    target 146
  ]
  edge [
    source 20
    target 147
  ]
  edge [
    source 20
    target 148
  ]
  edge [
    source 20
    target 149
  ]
  edge [
    source 20
    target 150
  ]
  edge [
    source 20
    target 151
  ]
  edge [
    source 20
    target 152
  ]
  edge [
    source 20
    target 153
  ]
  edge [
    source 20
    target 154
  ]
  edge [
    source 20
    target 155
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 156
  ]
  edge [
    source 22
    target 157
  ]
  edge [
    source 22
    target 158
  ]
  edge [
    source 22
    target 159
  ]
  edge [
    source 22
    target 160
  ]
  edge [
    source 22
    target 161
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 162
  ]
  edge [
    source 24
    target 163
  ]
  edge [
    source 24
    target 106
  ]
  edge [
    source 24
    target 164
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 165
  ]
  edge [
    source 25
    target 166
  ]
  edge [
    source 25
    target 167
  ]
  edge [
    source 25
    target 168
  ]
  edge [
    source 25
    target 169
  ]
  edge [
    source 25
    target 170
  ]
  edge [
    source 25
    target 171
  ]
  edge [
    source 25
    target 172
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 173
  ]
  edge [
    source 26
    target 174
  ]
  edge [
    source 26
    target 175
  ]
  edge [
    source 26
    target 176
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 28
    target 177
  ]
  edge [
    source 28
    target 178
  ]
  edge [
    source 28
    target 179
  ]
  edge [
    source 28
    target 180
  ]
  edge [
    source 28
    target 181
  ]
  edge [
    source 28
    target 182
  ]
  edge [
    source 28
    target 183
  ]
  edge [
    source 28
    target 184
  ]
  edge [
    source 28
    target 185
  ]
  edge [
    source 28
    target 186
  ]
  edge [
    source 28
    target 187
  ]
  edge [
    source 28
    target 188
  ]
  edge [
    source 28
    target 189
  ]
  edge [
    source 28
    target 190
  ]
  edge [
    source 28
    target 191
  ]
  edge [
    source 28
    target 192
  ]
  edge [
    source 28
    target 193
  ]
  edge [
    source 28
    target 194
  ]
  edge [
    source 28
    target 195
  ]
  edge [
    source 28
    target 196
  ]
  edge [
    source 28
    target 197
  ]
  edge [
    source 28
    target 198
  ]
  edge [
    source 28
    target 199
  ]
  edge [
    source 28
    target 200
  ]
  edge [
    source 28
    target 201
  ]
  edge [
    source 28
    target 202
  ]
  edge [
    source 28
    target 203
  ]
  edge [
    source 28
    target 204
  ]
  edge [
    source 28
    target 205
  ]
  edge [
    source 28
    target 206
  ]
  edge [
    source 28
    target 207
  ]
  edge [
    source 28
    target 208
  ]
  edge [
    source 28
    target 209
  ]
  edge [
    source 28
    target 210
  ]
  edge [
    source 28
    target 211
  ]
  edge [
    source 28
    target 212
  ]
  edge [
    source 28
    target 213
  ]
  edge [
    source 28
    target 214
  ]
  edge [
    source 28
    target 215
  ]
  edge [
    source 28
    target 216
  ]
  edge [
    source 28
    target 217
  ]
  edge [
    source 28
    target 218
  ]
  edge [
    source 28
    target 219
  ]
  edge [
    source 28
    target 220
  ]
  edge [
    source 28
    target 221
  ]
  edge [
    source 28
    target 222
  ]
  edge [
    source 28
    target 223
  ]
  edge [
    source 28
    target 224
  ]
  edge [
    source 28
    target 225
  ]
  edge [
    source 28
    target 226
  ]
  edge [
    source 28
    target 227
  ]
  edge [
    source 28
    target 228
  ]
  edge [
    source 28
    target 229
  ]
  edge [
    source 28
    target 230
  ]
  edge [
    source 28
    target 231
  ]
  edge [
    source 28
    target 232
  ]
  edge [
    source 28
    target 233
  ]
  edge [
    source 28
    target 234
  ]
  edge [
    source 28
    target 235
  ]
  edge [
    source 28
    target 236
  ]
  edge [
    source 28
    target 237
  ]
  edge [
    source 28
    target 238
  ]
  edge [
    source 28
    target 239
  ]
  edge [
    source 28
    target 240
  ]
  edge [
    source 28
    target 241
  ]
  edge [
    source 28
    target 242
  ]
  edge [
    source 28
    target 243
  ]
  edge [
    source 28
    target 244
  ]
  edge [
    source 28
    target 245
  ]
  edge [
    source 28
    target 246
  ]
  edge [
    source 28
    target 247
  ]
  edge [
    source 28
    target 248
  ]
  edge [
    source 28
    target 249
  ]
  edge [
    source 28
    target 250
  ]
  edge [
    source 28
    target 251
  ]
  edge [
    source 28
    target 252
  ]
  edge [
    source 28
    target 253
  ]
  edge [
    source 28
    target 254
  ]
  edge [
    source 28
    target 255
  ]
  edge [
    source 28
    target 256
  ]
  edge [
    source 28
    target 257
  ]
  edge [
    source 28
    target 258
  ]
  edge [
    source 28
    target 259
  ]
  edge [
    source 28
    target 260
  ]
  edge [
    source 28
    target 261
  ]
  edge [
    source 28
    target 262
  ]
  edge [
    source 28
    target 263
  ]
  edge [
    source 28
    target 264
  ]
  edge [
    source 28
    target 265
  ]
  edge [
    source 28
    target 266
  ]
  edge [
    source 28
    target 267
  ]
  edge [
    source 28
    target 268
  ]
  edge [
    source 28
    target 269
  ]
  edge [
    source 28
    target 270
  ]
  edge [
    source 28
    target 271
  ]
  edge [
    source 28
    target 272
  ]
  edge [
    source 28
    target 273
  ]
  edge [
    source 28
    target 274
  ]
  edge [
    source 28
    target 275
  ]
  edge [
    source 28
    target 276
  ]
  edge [
    source 28
    target 277
  ]
  edge [
    source 28
    target 278
  ]
  edge [
    source 28
    target 279
  ]
  edge [
    source 28
    target 280
  ]
  edge [
    source 28
    target 281
  ]
  edge [
    source 28
    target 282
  ]
  edge [
    source 28
    target 283
  ]
  edge [
    source 28
    target 284
  ]
  edge [
    source 28
    target 285
  ]
  edge [
    source 28
    target 286
  ]
  edge [
    source 28
    target 287
  ]
  edge [
    source 28
    target 288
  ]
  edge [
    source 28
    target 289
  ]
  edge [
    source 28
    target 290
  ]
  edge [
    source 28
    target 291
  ]
  edge [
    source 28
    target 292
  ]
  edge [
    source 28
    target 293
  ]
  edge [
    source 28
    target 294
  ]
  edge [
    source 28
    target 295
  ]
  edge [
    source 28
    target 296
  ]
  edge [
    source 28
    target 297
  ]
  edge [
    source 28
    target 298
  ]
  edge [
    source 28
    target 299
  ]
  edge [
    source 28
    target 300
  ]
  edge [
    source 28
    target 301
  ]
  edge [
    source 28
    target 302
  ]
  edge [
    source 28
    target 303
  ]
  edge [
    source 28
    target 304
  ]
  edge [
    source 28
    target 305
  ]
  edge [
    source 28
    target 306
  ]
  edge [
    source 28
    target 307
  ]
  edge [
    source 28
    target 308
  ]
  edge [
    source 28
    target 309
  ]
  edge [
    source 28
    target 310
  ]
  edge [
    source 28
    target 311
  ]
  edge [
    source 28
    target 312
  ]
  edge [
    source 28
    target 313
  ]
  edge [
    source 28
    target 314
  ]
  edge [
    source 28
    target 315
  ]
  edge [
    source 28
    target 316
  ]
  edge [
    source 28
    target 317
  ]
  edge [
    source 28
    target 318
  ]
  edge [
    source 28
    target 319
  ]
  edge [
    source 28
    target 320
  ]
  edge [
    source 28
    target 321
  ]
  edge [
    source 28
    target 322
  ]
  edge [
    source 28
    target 323
  ]
  edge [
    source 28
    target 324
  ]
  edge [
    source 28
    target 325
  ]
  edge [
    source 28
    target 326
  ]
  edge [
    source 28
    target 327
  ]
  edge [
    source 28
    target 328
  ]
  edge [
    source 28
    target 329
  ]
  edge [
    source 28
    target 330
  ]
  edge [
    source 28
    target 331
  ]
  edge [
    source 28
    target 332
  ]
  edge [
    source 28
    target 333
  ]
  edge [
    source 28
    target 334
  ]
  edge [
    source 28
    target 335
  ]
  edge [
    source 28
    target 336
  ]
  edge [
    source 28
    target 337
  ]
  edge [
    source 28
    target 338
  ]
  edge [
    source 28
    target 339
  ]
  edge [
    source 28
    target 340
  ]
  edge [
    source 28
    target 341
  ]
  edge [
    source 28
    target 342
  ]
  edge [
    source 28
    target 343
  ]
  edge [
    source 28
    target 344
  ]
  edge [
    source 28
    target 345
  ]
  edge [
    source 28
    target 346
  ]
  edge [
    source 28
    target 347
  ]
  edge [
    source 28
    target 348
  ]
  edge [
    source 28
    target 349
  ]
  edge [
    source 28
    target 350
  ]
  edge [
    source 28
    target 351
  ]
  edge [
    source 28
    target 352
  ]
  edge [
    source 28
    target 353
  ]
  edge [
    source 28
    target 354
  ]
  edge [
    source 28
    target 355
  ]
  edge [
    source 28
    target 356
  ]
  edge [
    source 28
    target 357
  ]
  edge [
    source 28
    target 358
  ]
  edge [
    source 28
    target 359
  ]
  edge [
    source 28
    target 360
  ]
  edge [
    source 28
    target 361
  ]
  edge [
    source 28
    target 362
  ]
  edge [
    source 28
    target 363
  ]
  edge [
    source 28
    target 364
  ]
  edge [
    source 28
    target 365
  ]
  edge [
    source 28
    target 366
  ]
  edge [
    source 28
    target 367
  ]
  edge [
    source 28
    target 368
  ]
  edge [
    source 28
    target 369
  ]
  edge [
    source 28
    target 370
  ]
  edge [
    source 28
    target 371
  ]
  edge [
    source 28
    target 372
  ]
  edge [
    source 28
    target 373
  ]
  edge [
    source 28
    target 374
  ]
  edge [
    source 28
    target 375
  ]
  edge [
    source 28
    target 376
  ]
  edge [
    source 28
    target 377
  ]
  edge [
    source 28
    target 378
  ]
  edge [
    source 28
    target 379
  ]
  edge [
    source 28
    target 380
  ]
  edge [
    source 28
    target 381
  ]
  edge [
    source 28
    target 382
  ]
  edge [
    source 28
    target 383
  ]
  edge [
    source 28
    target 384
  ]
  edge [
    source 28
    target 385
  ]
  edge [
    source 28
    target 386
  ]
  edge [
    source 28
    target 387
  ]
  edge [
    source 28
    target 388
  ]
  edge [
    source 28
    target 389
  ]
  edge [
    source 28
    target 390
  ]
  edge [
    source 28
    target 391
  ]
  edge [
    source 28
    target 392
  ]
  edge [
    source 28
    target 393
  ]
  edge [
    source 28
    target 394
  ]
  edge [
    source 28
    target 395
  ]
  edge [
    source 28
    target 396
  ]
  edge [
    source 28
    target 397
  ]
  edge [
    source 28
    target 398
  ]
  edge [
    source 28
    target 399
  ]
  edge [
    source 28
    target 400
  ]
  edge [
    source 28
    target 401
  ]
  edge [
    source 28
    target 402
  ]
  edge [
    source 28
    target 403
  ]
  edge [
    source 28
    target 404
  ]
  edge [
    source 28
    target 405
  ]
  edge [
    source 28
    target 406
  ]
  edge [
    source 28
    target 407
  ]
  edge [
    source 28
    target 408
  ]
  edge [
    source 28
    target 409
  ]
  edge [
    source 28
    target 410
  ]
  edge [
    source 28
    target 411
  ]
  edge [
    source 28
    target 412
  ]
  edge [
    source 28
    target 413
  ]
  edge [
    source 28
    target 414
  ]
  edge [
    source 28
    target 415
  ]
  edge [
    source 28
    target 416
  ]
  edge [
    source 28
    target 417
  ]
  edge [
    source 28
    target 418
  ]
  edge [
    source 28
    target 419
  ]
  edge [
    source 28
    target 420
  ]
  edge [
    source 28
    target 421
  ]
  edge [
    source 28
    target 422
  ]
  edge [
    source 28
    target 423
  ]
  edge [
    source 28
    target 424
  ]
  edge [
    source 28
    target 425
  ]
  edge [
    source 28
    target 426
  ]
  edge [
    source 28
    target 427
  ]
  edge [
    source 28
    target 428
  ]
  edge [
    source 28
    target 429
  ]
  edge [
    source 28
    target 430
  ]
  edge [
    source 28
    target 431
  ]
  edge [
    source 28
    target 432
  ]
  edge [
    source 28
    target 433
  ]
  edge [
    source 28
    target 434
  ]
  edge [
    source 28
    target 435
  ]
  edge [
    source 28
    target 436
  ]
  edge [
    source 28
    target 437
  ]
  edge [
    source 28
    target 438
  ]
  edge [
    source 28
    target 439
  ]
  edge [
    source 28
    target 440
  ]
  edge [
    source 28
    target 441
  ]
  edge [
    source 28
    target 442
  ]
  edge [
    source 28
    target 443
  ]
  edge [
    source 28
    target 444
  ]
  edge [
    source 28
    target 445
  ]
  edge [
    source 28
    target 446
  ]
  edge [
    source 28
    target 447
  ]
  edge [
    source 28
    target 448
  ]
  edge [
    source 28
    target 449
  ]
  edge [
    source 28
    target 450
  ]
  edge [
    source 28
    target 451
  ]
  edge [
    source 28
    target 452
  ]
  edge [
    source 28
    target 453
  ]
  edge [
    source 28
    target 454
  ]
  edge [
    source 28
    target 455
  ]
  edge [
    source 28
    target 456
  ]
  edge [
    source 28
    target 457
  ]
  edge [
    source 28
    target 458
  ]
  edge [
    source 28
    target 459
  ]
  edge [
    source 28
    target 460
  ]
  edge [
    source 28
    target 461
  ]
  edge [
    source 28
    target 462
  ]
  edge [
    source 28
    target 463
  ]
  edge [
    source 28
    target 464
  ]
  edge [
    source 28
    target 465
  ]
  edge [
    source 28
    target 466
  ]
  edge [
    source 28
    target 467
  ]
  edge [
    source 28
    target 468
  ]
  edge [
    source 28
    target 469
  ]
  edge [
    source 28
    target 470
  ]
  edge [
    source 28
    target 471
  ]
  edge [
    source 28
    target 472
  ]
  edge [
    source 28
    target 473
  ]
  edge [
    source 28
    target 474
  ]
  edge [
    source 28
    target 475
  ]
  edge [
    source 28
    target 476
  ]
  edge [
    source 28
    target 477
  ]
  edge [
    source 28
    target 478
  ]
  edge [
    source 28
    target 479
  ]
  edge [
    source 28
    target 480
  ]
  edge [
    source 28
    target 481
  ]
  edge [
    source 28
    target 482
  ]
  edge [
    source 28
    target 483
  ]
  edge [
    source 28
    target 484
  ]
  edge [
    source 28
    target 485
  ]
  edge [
    source 28
    target 486
  ]
  edge [
    source 28
    target 487
  ]
  edge [
    source 28
    target 488
  ]
  edge [
    source 28
    target 489
  ]
  edge [
    source 28
    target 490
  ]
  edge [
    source 28
    target 491
  ]
  edge [
    source 28
    target 492
  ]
  edge [
    source 28
    target 493
  ]
  edge [
    source 28
    target 494
  ]
  edge [
    source 28
    target 495
  ]
  edge [
    source 28
    target 496
  ]
  edge [
    source 28
    target 497
  ]
  edge [
    source 28
    target 498
  ]
  edge [
    source 500
    target 501
  ]
  edge [
    source 500
    target 502
  ]
  edge [
    source 500
    target 503
  ]
  edge [
    source 501
    target 502
  ]
  edge [
    source 501
    target 503
  ]
  edge [
    source 502
    target 503
  ]
  edge [
    source 504
    target 505
  ]
]
