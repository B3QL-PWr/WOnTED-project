graph [
  maxDegree 54
  minDegree 1
  meanDegree 2.056338028169014
  density 0.014583957646588753
  graphCliqueNumber 3
  node [
    id 0
    label "informacja"
    origin "text"
  ]
  node [
    id 1
    label "niejawny"
    origin "text"
  ]
  node [
    id 2
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 3
    label "mie&#263;"
    origin "text"
  ]
  node [
    id 4
    label "forma"
    origin "text"
  ]
  node [
    id 5
    label "ustny"
    origin "text"
  ]
  node [
    id 6
    label "wizualny"
    origin "text"
  ]
  node [
    id 7
    label "lub"
    origin "text"
  ]
  node [
    id 8
    label "dokument"
    origin "text"
  ]
  node [
    id 9
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 10
    label "inny"
    origin "text"
  ]
  node [
    id 11
    label "tym"
    origin "text"
  ]
  node [
    id 12
    label "tak&#380;e"
    origin "text"
  ]
  node [
    id 13
    label "sprz&#281;t"
    origin "text"
  ]
  node [
    id 14
    label "technologia"
    origin "text"
  ]
  node [
    id 15
    label "doj&#347;cie"
  ]
  node [
    id 16
    label "doj&#347;&#263;"
  ]
  node [
    id 17
    label "powzi&#261;&#263;"
  ]
  node [
    id 18
    label "wiedza"
  ]
  node [
    id 19
    label "sygna&#322;"
  ]
  node [
    id 20
    label "obiegni&#281;cie"
  ]
  node [
    id 21
    label "obieganie"
  ]
  node [
    id 22
    label "obiec"
  ]
  node [
    id 23
    label "dane"
  ]
  node [
    id 24
    label "obiega&#263;"
  ]
  node [
    id 25
    label "punkt"
  ]
  node [
    id 26
    label "publikacja"
  ]
  node [
    id 27
    label "powzi&#281;cie"
  ]
  node [
    id 28
    label "nieprzejrzysty"
  ]
  node [
    id 29
    label "zakryty"
  ]
  node [
    id 30
    label "nieoficjalny"
  ]
  node [
    id 31
    label "ukryty"
  ]
  node [
    id 32
    label "niejawnie"
  ]
  node [
    id 33
    label "by&#263;"
  ]
  node [
    id 34
    label "uprawi&#263;"
  ]
  node [
    id 35
    label "gotowy"
  ]
  node [
    id 36
    label "might"
  ]
  node [
    id 37
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 38
    label "czu&#263;"
  ]
  node [
    id 39
    label "need"
  ]
  node [
    id 40
    label "hide"
  ]
  node [
    id 41
    label "support"
  ]
  node [
    id 42
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 43
    label "punkt_widzenia"
  ]
  node [
    id 44
    label "do&#322;ek"
  ]
  node [
    id 45
    label "formality"
  ]
  node [
    id 46
    label "wz&#243;r"
  ]
  node [
    id 47
    label "kantyzm"
  ]
  node [
    id 48
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 49
    label "ornamentyka"
  ]
  node [
    id 50
    label "odmiana"
  ]
  node [
    id 51
    label "mode"
  ]
  node [
    id 52
    label "style"
  ]
  node [
    id 53
    label "formacja"
  ]
  node [
    id 54
    label "maszyna_drukarska"
  ]
  node [
    id 55
    label "poznanie"
  ]
  node [
    id 56
    label "szablon"
  ]
  node [
    id 57
    label "struktura"
  ]
  node [
    id 58
    label "spirala"
  ]
  node [
    id 59
    label "blaszka"
  ]
  node [
    id 60
    label "linearno&#347;&#263;"
  ]
  node [
    id 61
    label "stan"
  ]
  node [
    id 62
    label "temat"
  ]
  node [
    id 63
    label "cecha"
  ]
  node [
    id 64
    label "g&#322;owa"
  ]
  node [
    id 65
    label "kielich"
  ]
  node [
    id 66
    label "zdolno&#347;&#263;"
  ]
  node [
    id 67
    label "poj&#281;cie"
  ]
  node [
    id 68
    label "kszta&#322;t"
  ]
  node [
    id 69
    label "pasmo"
  ]
  node [
    id 70
    label "rdze&#324;"
  ]
  node [
    id 71
    label "leksem"
  ]
  node [
    id 72
    label "dyspozycja"
  ]
  node [
    id 73
    label "wygl&#261;d"
  ]
  node [
    id 74
    label "October"
  ]
  node [
    id 75
    label "zawarto&#347;&#263;"
  ]
  node [
    id 76
    label "creation"
  ]
  node [
    id 77
    label "p&#281;tla"
  ]
  node [
    id 78
    label "p&#322;at"
  ]
  node [
    id 79
    label "gwiazda"
  ]
  node [
    id 80
    label "arystotelizm"
  ]
  node [
    id 81
    label "obiekt"
  ]
  node [
    id 82
    label "dzie&#322;o"
  ]
  node [
    id 83
    label "naczynie"
  ]
  node [
    id 84
    label "wyra&#380;enie"
  ]
  node [
    id 85
    label "jednostka_systematyczna"
  ]
  node [
    id 86
    label "miniatura"
  ]
  node [
    id 87
    label "zwyczaj"
  ]
  node [
    id 88
    label "morfem"
  ]
  node [
    id 89
    label "posta&#263;"
  ]
  node [
    id 90
    label "egzamin"
  ]
  node [
    id 91
    label "ustnie"
  ]
  node [
    id 92
    label "wzrokowo"
  ]
  node [
    id 93
    label "wzrokowy"
  ]
  node [
    id 94
    label "wizualnie"
  ]
  node [
    id 95
    label "record"
  ]
  node [
    id 96
    label "wytw&#243;r"
  ]
  node [
    id 97
    label "&#347;wiadectwo"
  ]
  node [
    id 98
    label "zapis"
  ]
  node [
    id 99
    label "raport&#243;wka"
  ]
  node [
    id 100
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 101
    label "artyku&#322;"
  ]
  node [
    id 102
    label "plik"
  ]
  node [
    id 103
    label "writing"
  ]
  node [
    id 104
    label "utw&#243;r"
  ]
  node [
    id 105
    label "dokumentacja"
  ]
  node [
    id 106
    label "registratura"
  ]
  node [
    id 107
    label "parafa"
  ]
  node [
    id 108
    label "sygnatariusz"
  ]
  node [
    id 109
    label "fascyku&#322;"
  ]
  node [
    id 110
    label "jaki&#347;"
  ]
  node [
    id 111
    label "kolejny"
  ]
  node [
    id 112
    label "inaczej"
  ]
  node [
    id 113
    label "r&#243;&#380;ny"
  ]
  node [
    id 114
    label "inszy"
  ]
  node [
    id 115
    label "osobno"
  ]
  node [
    id 116
    label "przedmiot"
  ]
  node [
    id 117
    label "kolekcja"
  ]
  node [
    id 118
    label "sprz&#281;cior"
  ]
  node [
    id 119
    label "furniture"
  ]
  node [
    id 120
    label "equipment"
  ]
  node [
    id 121
    label "penis"
  ]
  node [
    id 122
    label "zbi&#243;r"
  ]
  node [
    id 123
    label "sprz&#281;cik"
  ]
  node [
    id 124
    label "engineering"
  ]
  node [
    id 125
    label "technika"
  ]
  node [
    id 126
    label "mikrotechnologia"
  ]
  node [
    id 127
    label "technologia_nieorganiczna"
  ]
  node [
    id 128
    label "biotechnologia"
  ]
  node [
    id 129
    label "spos&#243;b"
  ]
  node [
    id 130
    label "rzeczpospolita"
  ]
  node [
    id 131
    label "polski"
  ]
  node [
    id 132
    label "agencja"
  ]
  node [
    id 133
    label "bezpiecze&#324;stwo"
  ]
  node [
    id 134
    label "wewn&#281;trzny"
  ]
  node [
    id 135
    label "s&#322;u&#380;ba"
  ]
  node [
    id 136
    label "kontrwywiad"
  ]
  node [
    id 137
    label "wojskowy"
  ]
  node [
    id 138
    label "zjednoczy&#263;"
  ]
  node [
    id 139
    label "ameryk"
  ]
  node [
    id 140
    label "departament"
  ]
  node [
    id 141
    label "obrona"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 4
    target 8
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 12
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 7
    target 13
  ]
  edge [
    source 7
    target 14
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 13
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 111
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 13
    target 116
  ]
  edge [
    source 13
    target 117
  ]
  edge [
    source 13
    target 118
  ]
  edge [
    source 13
    target 119
  ]
  edge [
    source 13
    target 120
  ]
  edge [
    source 13
    target 121
  ]
  edge [
    source 13
    target 122
  ]
  edge [
    source 13
    target 123
  ]
  edge [
    source 14
    target 124
  ]
  edge [
    source 14
    target 125
  ]
  edge [
    source 14
    target 126
  ]
  edge [
    source 14
    target 127
  ]
  edge [
    source 14
    target 128
  ]
  edge [
    source 14
    target 129
  ]
  edge [
    source 61
    target 138
  ]
  edge [
    source 61
    target 139
  ]
  edge [
    source 130
    target 131
  ]
  edge [
    source 132
    target 133
  ]
  edge [
    source 132
    target 134
  ]
  edge [
    source 133
    target 134
  ]
  edge [
    source 135
    target 136
  ]
  edge [
    source 135
    target 137
  ]
  edge [
    source 136
    target 137
  ]
  edge [
    source 138
    target 139
  ]
  edge [
    source 140
    target 141
  ]
]
