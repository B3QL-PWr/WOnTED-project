graph [
  maxDegree 5
  minDegree 1
  meanDegree 2
  density 0.2857142857142857
  graphCliqueNumber 3
  node [
    id 0
    label "ktora"
    origin "text"
  ]
  node [
    id 1
    label "rozowa"
    origin "text"
  ]
  node [
    id 2
    label "wrzucila"
    origin "text"
  ]
  node [
    id 3
    label "zaraz"
    origin "text"
  ]
  node [
    id 4
    label "usunela"
    origin "text"
  ]
  node [
    id 5
    label "blisko"
  ]
  node [
    id 6
    label "zara"
  ]
  node [
    id 7
    label "nied&#322;ugo"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 7
  ]
]
