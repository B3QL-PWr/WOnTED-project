graph [
  maxDegree 42
  minDegree 1
  meanDegree 2.019607843137255
  density 0.019996117258784703
  graphCliqueNumber 2
  node [
    id 0
    label "sobota"
    origin "text"
  ]
  node [
    id 1
    label "grudzie&#324;"
    origin "text"
  ]
  node [
    id 2
    label "oko&#322;o"
    origin "text"
  ]
  node [
    id 3
    label "godzina"
    origin "text"
  ]
  node [
    id 4
    label "oznakowa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "przej&#347;cie"
    origin "text"
  ]
  node [
    id 6
    label "dla"
    origin "text"
  ]
  node [
    id 7
    label "pieszy"
    origin "text"
  ]
  node [
    id 8
    label "przy"
    origin "text"
  ]
  node [
    id 9
    label "katowicki"
    origin "text"
  ]
  node [
    id 10
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 11
    label "potr&#261;ci&#263;"
    origin "text"
  ]
  node [
    id 12
    label "letni"
    origin "text"
  ]
  node [
    id 13
    label "weekend"
  ]
  node [
    id 14
    label "dzie&#324;_powszedni"
  ]
  node [
    id 15
    label "Wielka_Sobota"
  ]
  node [
    id 16
    label "Bo&#380;e_Narodzenie"
  ]
  node [
    id 17
    label "miesi&#261;c"
  ]
  node [
    id 18
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 19
    label "Barb&#243;rka"
  ]
  node [
    id 20
    label "Sylwester"
  ]
  node [
    id 21
    label "minuta"
  ]
  node [
    id 22
    label "doba"
  ]
  node [
    id 23
    label "czas"
  ]
  node [
    id 24
    label "p&#243;&#322;godzina"
  ]
  node [
    id 25
    label "kwadrans"
  ]
  node [
    id 26
    label "time"
  ]
  node [
    id 27
    label "jednostka_czasu"
  ]
  node [
    id 28
    label "oznaczy&#263;"
  ]
  node [
    id 29
    label "stamp"
  ]
  node [
    id 30
    label "nale&#380;enie"
  ]
  node [
    id 31
    label "odmienienie"
  ]
  node [
    id 32
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 33
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 34
    label "mini&#281;cie"
  ]
  node [
    id 35
    label "prze&#380;ycie"
  ]
  node [
    id 36
    label "strain"
  ]
  node [
    id 37
    label "przerobienie"
  ]
  node [
    id 38
    label "stanie_si&#281;"
  ]
  node [
    id 39
    label "dostanie_si&#281;"
  ]
  node [
    id 40
    label "wydeptanie"
  ]
  node [
    id 41
    label "wydeptywanie"
  ]
  node [
    id 42
    label "offense"
  ]
  node [
    id 43
    label "wymienienie"
  ]
  node [
    id 44
    label "zacz&#281;cie"
  ]
  node [
    id 45
    label "trwanie"
  ]
  node [
    id 46
    label "przepojenie"
  ]
  node [
    id 47
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 48
    label "zaliczenie"
  ]
  node [
    id 49
    label "zdarzenie_si&#281;"
  ]
  node [
    id 50
    label "uznanie"
  ]
  node [
    id 51
    label "nasycenie_si&#281;"
  ]
  node [
    id 52
    label "przemokni&#281;cie"
  ]
  node [
    id 53
    label "nas&#261;czenie"
  ]
  node [
    id 54
    label "mienie"
  ]
  node [
    id 55
    label "ustawa"
  ]
  node [
    id 56
    label "experience"
  ]
  node [
    id 57
    label "przewy&#380;szenie"
  ]
  node [
    id 58
    label "miejsce"
  ]
  node [
    id 59
    label "faza"
  ]
  node [
    id 60
    label "doznanie"
  ]
  node [
    id 61
    label "przestanie"
  ]
  node [
    id 62
    label "traversal"
  ]
  node [
    id 63
    label "przebycie"
  ]
  node [
    id 64
    label "przedostanie_si&#281;"
  ]
  node [
    id 65
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 66
    label "wstawka"
  ]
  node [
    id 67
    label "przepuszczenie"
  ]
  node [
    id 68
    label "wytyczenie"
  ]
  node [
    id 69
    label "crack"
  ]
  node [
    id 70
    label "specjalny"
  ]
  node [
    id 71
    label "cz&#322;owiek"
  ]
  node [
    id 72
    label "w&#281;drowiec"
  ]
  node [
    id 73
    label "pieszo"
  ]
  node [
    id 74
    label "piechotny"
  ]
  node [
    id 75
    label "chodnik"
  ]
  node [
    id 76
    label "&#347;l&#261;ski"
  ]
  node [
    id 77
    label "po_katowicku"
  ]
  node [
    id 78
    label "proceed"
  ]
  node [
    id 79
    label "catch"
  ]
  node [
    id 80
    label "pozosta&#263;"
  ]
  node [
    id 81
    label "osta&#263;_si&#281;"
  ]
  node [
    id 82
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 83
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 84
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 85
    label "change"
  ]
  node [
    id 86
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 87
    label "precipitate"
  ]
  node [
    id 88
    label "allude"
  ]
  node [
    id 89
    label "odliczy&#263;"
  ]
  node [
    id 90
    label "uderzy&#263;"
  ]
  node [
    id 91
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 92
    label "smell"
  ]
  node [
    id 93
    label "nijaki"
  ]
  node [
    id 94
    label "sezonowy"
  ]
  node [
    id 95
    label "letnio"
  ]
  node [
    id 96
    label "s&#322;oneczny"
  ]
  node [
    id 97
    label "weso&#322;y"
  ]
  node [
    id 98
    label "oboj&#281;tny"
  ]
  node [
    id 99
    label "latowy"
  ]
  node [
    id 100
    label "ciep&#322;y"
  ]
  node [
    id 101
    label "typowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 12
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 87
  ]
  edge [
    source 11
    target 88
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 11
    target 92
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 12
    target 94
  ]
  edge [
    source 12
    target 95
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 12
    target 101
  ]
]
