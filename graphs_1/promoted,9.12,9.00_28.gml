graph [
  maxDegree 8
  minDegree 1
  meanDegree 1.7777777777777777
  density 0.2222222222222222
  graphCliqueNumber 2
  node [
    id 0
    label "drogi"
    origin "text"
  ]
  node [
    id 1
    label "wykopowicze"
    origin "text"
  ]
  node [
    id 2
    label "cz&#322;owiek"
  ]
  node [
    id 3
    label "warto&#347;ciowy"
  ]
  node [
    id 4
    label "bliski"
  ]
  node [
    id 5
    label "drogo"
  ]
  node [
    id 6
    label "przyjaciel"
  ]
  node [
    id 7
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 8
    label "mi&#322;y"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
]
