graph [
  maxDegree 33
  minDegree 1
  meanDegree 2
  density 0.02631578947368421
  graphCliqueNumber 2
  node [
    id 0
    label "wyobrazi&#263;"
    origin "text"
  ]
  node [
    id 1
    label "siebie"
    origin "text"
  ]
  node [
    id 2
    label "ten"
    origin "text"
  ]
  node [
    id 3
    label "syk"
    origin "text"
  ]
  node [
    id 4
    label "&#347;wierszcz"
    origin "text"
  ]
  node [
    id 5
    label "stopie&#324;"
    origin "text"
  ]
  node [
    id 6
    label "dawa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "kark"
    origin "text"
  ]
  node [
    id 8
    label "okre&#347;lony"
  ]
  node [
    id 9
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 10
    label "d&#378;wi&#281;k"
  ]
  node [
    id 11
    label "sp&#243;&#322;g&#322;oska_sycz&#261;ca"
  ]
  node [
    id 12
    label "cykni&#281;cie"
  ]
  node [
    id 13
    label "&#347;wierszczowate"
  ]
  node [
    id 14
    label "owad_prostoskrzyd&#322;y"
  ]
  node [
    id 15
    label "cricket"
  ]
  node [
    id 16
    label "zacyka&#263;"
  ]
  node [
    id 17
    label "cyka&#263;"
  ]
  node [
    id 18
    label "minuta"
  ]
  node [
    id 19
    label "forma"
  ]
  node [
    id 20
    label "znaczenie"
  ]
  node [
    id 21
    label "kategoria_gramatyczna"
  ]
  node [
    id 22
    label "przys&#322;&#243;wek"
  ]
  node [
    id 23
    label "szczebel"
  ]
  node [
    id 24
    label "element"
  ]
  node [
    id 25
    label "poziom"
  ]
  node [
    id 26
    label "degree"
  ]
  node [
    id 27
    label "podn&#243;&#380;ek"
  ]
  node [
    id 28
    label "rank"
  ]
  node [
    id 29
    label "przymiotnik"
  ]
  node [
    id 30
    label "podzia&#322;"
  ]
  node [
    id 31
    label "ocena"
  ]
  node [
    id 32
    label "kszta&#322;t"
  ]
  node [
    id 33
    label "wschodek"
  ]
  node [
    id 34
    label "miejsce"
  ]
  node [
    id 35
    label "schody"
  ]
  node [
    id 36
    label "gama"
  ]
  node [
    id 37
    label "podstopie&#324;"
  ]
  node [
    id 38
    label "wielko&#347;&#263;"
  ]
  node [
    id 39
    label "jednostka"
  ]
  node [
    id 40
    label "render"
  ]
  node [
    id 41
    label "hold"
  ]
  node [
    id 42
    label "surrender"
  ]
  node [
    id 43
    label "traktowa&#263;"
  ]
  node [
    id 44
    label "dostarcza&#263;"
  ]
  node [
    id 45
    label "tender"
  ]
  node [
    id 46
    label "train"
  ]
  node [
    id 47
    label "give"
  ]
  node [
    id 48
    label "umieszcza&#263;"
  ]
  node [
    id 49
    label "nalewa&#263;"
  ]
  node [
    id 50
    label "przeznacza&#263;"
  ]
  node [
    id 51
    label "p&#322;aci&#263;"
  ]
  node [
    id 52
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 53
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 54
    label "powierza&#263;"
  ]
  node [
    id 55
    label "hold_out"
  ]
  node [
    id 56
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 57
    label "wyrzeka&#263;_si&#281;"
  ]
  node [
    id 58
    label "mie&#263;_miejsce"
  ]
  node [
    id 59
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 60
    label "robi&#263;"
  ]
  node [
    id 61
    label "t&#322;uc"
  ]
  node [
    id 62
    label "wpiernicza&#263;"
  ]
  node [
    id 63
    label "przekazywa&#263;"
  ]
  node [
    id 64
    label "puszcza&#263;_si&#281;"
  ]
  node [
    id 65
    label "zezwala&#263;"
  ]
  node [
    id 66
    label "rap"
  ]
  node [
    id 67
    label "obiecywa&#263;"
  ]
  node [
    id 68
    label "&#322;adowa&#263;"
  ]
  node [
    id 69
    label "odst&#281;powa&#263;"
  ]
  node [
    id 70
    label "exsert"
  ]
  node [
    id 71
    label "cz&#322;owiek"
  ]
  node [
    id 72
    label "kulturysta"
  ]
  node [
    id 73
    label "mu&#322;y"
  ]
  node [
    id 74
    label "g&#243;ra_mi&#281;sa"
  ]
  node [
    id 75
    label "szyja"
  ]
  node [
    id 76
    label "sze&#347;ciopak"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 10
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
]
