graph [
  maxDegree 33
  minDegree 1
  meanDegree 2.127659574468085
  density 0.007571742257893541
  graphCliqueNumber 3
  node [
    id 0
    label "ruszy&#263;"
    origin "text"
  ]
  node [
    id 1
    label "w&#322;a&#347;nie"
    origin "text"
  ]
  node [
    id 2
    label "kampania"
    origin "text"
  ]
  node [
    id 3
    label "spo&#322;eczny"
    origin "text"
  ]
  node [
    id 4
    label "rzecz"
    origin "text"
  ]
  node [
    id 5
    label "zebranie"
    origin "text"
  ]
  node [
    id 6
    label "podpis"
    origin "text"
  ]
  node [
    id 7
    label "pod"
    origin "text"
  ]
  node [
    id 8
    label "obywatelski"
    origin "text"
  ]
  node [
    id 9
    label "inicjatywa"
    origin "text"
  ]
  node [
    id 10
    label "ustawodawczy"
    origin "text"
  ]
  node [
    id 11
    label "zmiana"
    origin "text"
  ]
  node [
    id 12
    label "ustawa"
    origin "text"
  ]
  node [
    id 13
    label "ochrona"
    origin "text"
  ]
  node [
    id 14
    label "przyroda"
    origin "text"
  ]
  node [
    id 15
    label "cel"
    origin "text"
  ]
  node [
    id 16
    label "by&#263;"
    origin "text"
  ]
  node [
    id 17
    label "wprowadzenie"
    origin "text"
  ]
  node [
    id 18
    label "taki"
    origin "text"
  ]
  node [
    id 19
    label "samorz&#261;d"
    origin "text"
  ]
  node [
    id 20
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 21
    label "blokowa&#263;"
    origin "text"
  ]
  node [
    id 22
    label "rozw&#243;j"
    origin "text"
  ]
  node [
    id 23
    label "park"
    origin "text"
  ]
  node [
    id 24
    label "narodowy"
    origin "text"
  ]
  node [
    id 25
    label "przed"
    origin "text"
  ]
  node [
    id 26
    label "wszyscy"
    origin "text"
  ]
  node [
    id 27
    label "chodzi&#263;"
    origin "text"
  ]
  node [
    id 28
    label "poszerzenie"
    origin "text"
  ]
  node [
    id 29
    label "granica"
    origin "text"
  ]
  node [
    id 30
    label "bia&#322;owieski"
    origin "text"
  ]
  node [
    id 31
    label "skutecznie"
    origin "text"
  ]
  node [
    id 32
    label "lata"
    origin "text"
  ]
  node [
    id 33
    label "lokalny"
    origin "text"
  ]
  node [
    id 34
    label "ale"
    origin "text"
  ]
  node [
    id 35
    label "te&#380;"
    origin "text"
  ]
  node [
    id 36
    label "utworzy&#263;"
    origin "text"
  ]
  node [
    id 37
    label "nowa"
    origin "text"
  ]
  node [
    id 38
    label "motivate"
  ]
  node [
    id 39
    label "zabra&#263;"
  ]
  node [
    id 40
    label "wzbudzi&#263;"
  ]
  node [
    id 41
    label "zacz&#261;&#263;"
  ]
  node [
    id 42
    label "spowodowa&#263;"
  ]
  node [
    id 43
    label "allude"
  ]
  node [
    id 44
    label "stimulate"
  ]
  node [
    id 45
    label "cut"
  ]
  node [
    id 46
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 47
    label "zrobi&#263;"
  ]
  node [
    id 48
    label "go"
  ]
  node [
    id 49
    label "dok&#322;adnie"
  ]
  node [
    id 50
    label "dzia&#322;anie"
  ]
  node [
    id 51
    label "campaign"
  ]
  node [
    id 52
    label "wydarzenie"
  ]
  node [
    id 53
    label "akcja"
  ]
  node [
    id 54
    label "Kampania_Wrze&#347;niowa"
  ]
  node [
    id 55
    label "niepubliczny"
  ]
  node [
    id 56
    label "spo&#322;ecznie"
  ]
  node [
    id 57
    label "publiczny"
  ]
  node [
    id 58
    label "obiekt"
  ]
  node [
    id 59
    label "temat"
  ]
  node [
    id 60
    label "istota"
  ]
  node [
    id 61
    label "wpa&#347;&#263;"
  ]
  node [
    id 62
    label "wpadanie"
  ]
  node [
    id 63
    label "przedmiot"
  ]
  node [
    id 64
    label "wpada&#263;"
  ]
  node [
    id 65
    label "kultura"
  ]
  node [
    id 66
    label "mienie"
  ]
  node [
    id 67
    label "object"
  ]
  node [
    id 68
    label "wpadni&#281;cie"
  ]
  node [
    id 69
    label "wzi&#281;cie"
  ]
  node [
    id 70
    label "dostawanie"
  ]
  node [
    id 71
    label "po&#347;ci&#261;ganie"
  ]
  node [
    id 72
    label "trafienie"
  ]
  node [
    id 73
    label "gather"
  ]
  node [
    id 74
    label "spotkanie"
  ]
  node [
    id 75
    label "ocenienie"
  ]
  node [
    id 76
    label "skupienie"
  ]
  node [
    id 77
    label "zdarzenie_si&#281;"
  ]
  node [
    id 78
    label "merging"
  ]
  node [
    id 79
    label "konwentykiel"
  ]
  node [
    id 80
    label "party"
  ]
  node [
    id 81
    label "czynno&#347;&#263;"
  ]
  node [
    id 82
    label "zgromadzenie"
  ]
  node [
    id 83
    label "gathering"
  ]
  node [
    id 84
    label "concourse"
  ]
  node [
    id 85
    label "caucus"
  ]
  node [
    id 86
    label "spowodowanie"
  ]
  node [
    id 87
    label "uderzenie"
  ]
  node [
    id 88
    label "sprz&#261;tni&#281;cie"
  ]
  node [
    id 89
    label "&#347;ci&#261;gni&#281;cie"
  ]
  node [
    id 90
    label "templum"
  ]
  node [
    id 91
    label "pozyskanie"
  ]
  node [
    id 92
    label "w&#322;asnor&#281;czny"
  ]
  node [
    id 93
    label "napis"
  ]
  node [
    id 94
    label "obja&#347;nienie"
  ]
  node [
    id 95
    label "sign"
  ]
  node [
    id 96
    label "potwierdzenie"
  ]
  node [
    id 97
    label "signal"
  ]
  node [
    id 98
    label "znak"
  ]
  node [
    id 99
    label "odpowiedzialny"
  ]
  node [
    id 100
    label "obywatelsko"
  ]
  node [
    id 101
    label "s&#322;uszny"
  ]
  node [
    id 102
    label "oddolny"
  ]
  node [
    id 103
    label "plan"
  ]
  node [
    id 104
    label "propozycja"
  ]
  node [
    id 105
    label "samodzielno&#347;&#263;"
  ]
  node [
    id 106
    label "anatomopatolog"
  ]
  node [
    id 107
    label "rewizja"
  ]
  node [
    id 108
    label "oznaka"
  ]
  node [
    id 109
    label "czas"
  ]
  node [
    id 110
    label "ferment"
  ]
  node [
    id 111
    label "komplet"
  ]
  node [
    id 112
    label "tura"
  ]
  node [
    id 113
    label "amendment"
  ]
  node [
    id 114
    label "zmianka"
  ]
  node [
    id 115
    label "odmienianie"
  ]
  node [
    id 116
    label "passage"
  ]
  node [
    id 117
    label "zjawisko"
  ]
  node [
    id 118
    label "change"
  ]
  node [
    id 119
    label "praca"
  ]
  node [
    id 120
    label "Karta_Nauczyciela"
  ]
  node [
    id 121
    label "marc&#243;wka"
  ]
  node [
    id 122
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 123
    label "akt"
  ]
  node [
    id 124
    label "przej&#347;&#263;"
  ]
  node [
    id 125
    label "charter"
  ]
  node [
    id 126
    label "przej&#347;cie"
  ]
  node [
    id 127
    label "chemical_bond"
  ]
  node [
    id 128
    label "tarcza"
  ]
  node [
    id 129
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 130
    label "Stra&#380;_Ochrony_Kolei"
  ]
  node [
    id 131
    label "borowiec"
  ]
  node [
    id 132
    label "obstawienie"
  ]
  node [
    id 133
    label "formacja"
  ]
  node [
    id 134
    label "ubezpieczenie"
  ]
  node [
    id 135
    label "obstawia&#263;"
  ]
  node [
    id 136
    label "obstawianie"
  ]
  node [
    id 137
    label "transportacja"
  ]
  node [
    id 138
    label "biota"
  ]
  node [
    id 139
    label "wszechstworzenie"
  ]
  node [
    id 140
    label "obiekt_naturalny"
  ]
  node [
    id 141
    label "Ziemia"
  ]
  node [
    id 142
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 143
    label "fauna"
  ]
  node [
    id 144
    label "stw&#243;r"
  ]
  node [
    id 145
    label "ekosystem"
  ]
  node [
    id 146
    label "teren"
  ]
  node [
    id 147
    label "environment"
  ]
  node [
    id 148
    label "woda"
  ]
  node [
    id 149
    label "przyra"
  ]
  node [
    id 150
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 151
    label "mikrokosmos"
  ]
  node [
    id 152
    label "miejsce"
  ]
  node [
    id 153
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 154
    label "przysz&#322;o&#347;&#263;"
  ]
  node [
    id 155
    label "punkt"
  ]
  node [
    id 156
    label "thing"
  ]
  node [
    id 157
    label "rezultat"
  ]
  node [
    id 158
    label "si&#281;ga&#263;"
  ]
  node [
    id 159
    label "trwa&#263;"
  ]
  node [
    id 160
    label "obecno&#347;&#263;"
  ]
  node [
    id 161
    label "stan"
  ]
  node [
    id 162
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 163
    label "stand"
  ]
  node [
    id 164
    label "mie&#263;_miejsce"
  ]
  node [
    id 165
    label "uczestniczy&#263;"
  ]
  node [
    id 166
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 167
    label "equal"
  ]
  node [
    id 168
    label "rynek"
  ]
  node [
    id 169
    label "issue"
  ]
  node [
    id 170
    label "evocation"
  ]
  node [
    id 171
    label "wst&#281;p"
  ]
  node [
    id 172
    label "nuklearyzacja"
  ]
  node [
    id 173
    label "umo&#380;liwienie"
  ]
  node [
    id 174
    label "zacz&#281;cie"
  ]
  node [
    id 175
    label "wpisanie"
  ]
  node [
    id 176
    label "zapoznanie"
  ]
  node [
    id 177
    label "zrobienie"
  ]
  node [
    id 178
    label "entrance"
  ]
  node [
    id 179
    label "wej&#347;cie"
  ]
  node [
    id 180
    label "podstawy"
  ]
  node [
    id 181
    label "zak&#322;&#243;cenie"
  ]
  node [
    id 182
    label "w&#322;&#261;czenie"
  ]
  node [
    id 183
    label "doprowadzenie"
  ]
  node [
    id 184
    label "przewietrzenie"
  ]
  node [
    id 185
    label "deduction"
  ]
  node [
    id 186
    label "umieszczenie"
  ]
  node [
    id 187
    label "okre&#347;lony"
  ]
  node [
    id 188
    label "jaki&#347;"
  ]
  node [
    id 189
    label "autonomy"
  ]
  node [
    id 190
    label "organ"
  ]
  node [
    id 191
    label "uprawi&#263;"
  ]
  node [
    id 192
    label "gotowy"
  ]
  node [
    id 193
    label "might"
  ]
  node [
    id 194
    label "throng"
  ]
  node [
    id 195
    label "zajmowa&#263;"
  ]
  node [
    id 196
    label "przeszkadza&#263;"
  ]
  node [
    id 197
    label "zablokowywa&#263;"
  ]
  node [
    id 198
    label "sk&#322;ada&#263;"
  ]
  node [
    id 199
    label "walczy&#263;"
  ]
  node [
    id 200
    label "interlock"
  ]
  node [
    id 201
    label "wstrzymywa&#263;"
  ]
  node [
    id 202
    label "unieruchamia&#263;"
  ]
  node [
    id 203
    label "kiblowa&#263;"
  ]
  node [
    id 204
    label "zatrzymywa&#263;"
  ]
  node [
    id 205
    label "parry"
  ]
  node [
    id 206
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 207
    label "procedura"
  ]
  node [
    id 208
    label "process"
  ]
  node [
    id 209
    label "cycle"
  ]
  node [
    id 210
    label "proces"
  ]
  node [
    id 211
    label "&#380;ycie"
  ]
  node [
    id 212
    label "z&#322;ote_czasy"
  ]
  node [
    id 213
    label "proces_biologiczny"
  ]
  node [
    id 214
    label "teren_przemys&#322;owy"
  ]
  node [
    id 215
    label "ballpark"
  ]
  node [
    id 216
    label "obszar"
  ]
  node [
    id 217
    label "sprz&#281;t"
  ]
  node [
    id 218
    label "tabor"
  ]
  node [
    id 219
    label "&#321;azienki_Kr&#243;lewskie"
  ]
  node [
    id 220
    label "teren_zielony"
  ]
  node [
    id 221
    label "nacjonalistyczny"
  ]
  node [
    id 222
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 223
    label "narodowo"
  ]
  node [
    id 224
    label "wa&#380;ny"
  ]
  node [
    id 225
    label "proceed"
  ]
  node [
    id 226
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 227
    label "bangla&#263;"
  ]
  node [
    id 228
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 229
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 230
    label "run"
  ]
  node [
    id 231
    label "tryb"
  ]
  node [
    id 232
    label "p&#322;ywa&#263;"
  ]
  node [
    id 233
    label "continue"
  ]
  node [
    id 234
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 235
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 236
    label "przebiega&#263;"
  ]
  node [
    id 237
    label "wk&#322;ada&#263;"
  ]
  node [
    id 238
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 239
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 240
    label "para"
  ]
  node [
    id 241
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 242
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 243
    label "krok"
  ]
  node [
    id 244
    label "str&#243;j"
  ]
  node [
    id 245
    label "bywa&#263;"
  ]
  node [
    id 246
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 247
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 248
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 249
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 250
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 251
    label "dziama&#263;"
  ]
  node [
    id 252
    label "stara&#263;_si&#281;"
  ]
  node [
    id 253
    label "carry"
  ]
  node [
    id 254
    label "powi&#281;kszenie"
  ]
  node [
    id 255
    label "szerszy"
  ]
  node [
    id 256
    label "prolongation"
  ]
  node [
    id 257
    label "zakres"
  ]
  node [
    id 258
    label "Ural"
  ]
  node [
    id 259
    label "koniec"
  ]
  node [
    id 260
    label "kres"
  ]
  node [
    id 261
    label "granice"
  ]
  node [
    id 262
    label "granica_pa&#324;stwa"
  ]
  node [
    id 263
    label "pu&#322;ap"
  ]
  node [
    id 264
    label "frontier"
  ]
  node [
    id 265
    label "end"
  ]
  node [
    id 266
    label "miara"
  ]
  node [
    id 267
    label "poj&#281;cie"
  ]
  node [
    id 268
    label "dobrze"
  ]
  node [
    id 269
    label "skuteczny"
  ]
  node [
    id 270
    label "summer"
  ]
  node [
    id 271
    label "lokalnie"
  ]
  node [
    id 272
    label "piwo"
  ]
  node [
    id 273
    label "zorganizowa&#263;"
  ]
  node [
    id 274
    label "sta&#263;_si&#281;"
  ]
  node [
    id 275
    label "compose"
  ]
  node [
    id 276
    label "przygotowa&#263;"
  ]
  node [
    id 277
    label "cause"
  ]
  node [
    id 278
    label "ukszta&#322;towa&#263;"
  ]
  node [
    id 279
    label "create"
  ]
  node [
    id 280
    label "gwiazda"
  ]
  node [
    id 281
    label "gwiazda_podw&#243;jna"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 10
  ]
  edge [
    source 4
    target 11
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 104
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 18
  ]
  edge [
    source 11
    target 19
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 108
  ]
  edge [
    source 11
    target 109
  ]
  edge [
    source 11
    target 110
  ]
  edge [
    source 11
    target 111
  ]
  edge [
    source 11
    target 112
  ]
  edge [
    source 11
    target 113
  ]
  edge [
    source 11
    target 114
  ]
  edge [
    source 11
    target 115
  ]
  edge [
    source 11
    target 116
  ]
  edge [
    source 11
    target 117
  ]
  edge [
    source 11
    target 118
  ]
  edge [
    source 11
    target 119
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 120
  ]
  edge [
    source 12
    target 121
  ]
  edge [
    source 12
    target 122
  ]
  edge [
    source 12
    target 123
  ]
  edge [
    source 12
    target 124
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 12
    target 24
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 127
  ]
  edge [
    source 13
    target 128
  ]
  edge [
    source 13
    target 129
  ]
  edge [
    source 13
    target 58
  ]
  edge [
    source 13
    target 130
  ]
  edge [
    source 13
    target 131
  ]
  edge [
    source 13
    target 132
  ]
  edge [
    source 13
    target 133
  ]
  edge [
    source 13
    target 134
  ]
  edge [
    source 13
    target 135
  ]
  edge [
    source 13
    target 136
  ]
  edge [
    source 13
    target 137
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 138
  ]
  edge [
    source 14
    target 139
  ]
  edge [
    source 14
    target 140
  ]
  edge [
    source 14
    target 141
  ]
  edge [
    source 14
    target 142
  ]
  edge [
    source 14
    target 143
  ]
  edge [
    source 14
    target 63
  ]
  edge [
    source 14
    target 144
  ]
  edge [
    source 14
    target 145
  ]
  edge [
    source 14
    target 146
  ]
  edge [
    source 14
    target 147
  ]
  edge [
    source 14
    target 148
  ]
  edge [
    source 14
    target 149
  ]
  edge [
    source 14
    target 150
  ]
  edge [
    source 14
    target 151
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 152
  ]
  edge [
    source 15
    target 153
  ]
  edge [
    source 15
    target 154
  ]
  edge [
    source 15
    target 155
  ]
  edge [
    source 15
    target 156
  ]
  edge [
    source 15
    target 157
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 158
  ]
  edge [
    source 16
    target 159
  ]
  edge [
    source 16
    target 160
  ]
  edge [
    source 16
    target 161
  ]
  edge [
    source 16
    target 162
  ]
  edge [
    source 16
    target 163
  ]
  edge [
    source 16
    target 164
  ]
  edge [
    source 16
    target 165
  ]
  edge [
    source 16
    target 27
  ]
  edge [
    source 16
    target 166
  ]
  edge [
    source 16
    target 167
  ]
  edge [
    source 16
    target 20
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 168
  ]
  edge [
    source 17
    target 169
  ]
  edge [
    source 17
    target 170
  ]
  edge [
    source 17
    target 171
  ]
  edge [
    source 17
    target 172
  ]
  edge [
    source 17
    target 173
  ]
  edge [
    source 17
    target 174
  ]
  edge [
    source 17
    target 175
  ]
  edge [
    source 17
    target 176
  ]
  edge [
    source 17
    target 177
  ]
  edge [
    source 17
    target 81
  ]
  edge [
    source 17
    target 178
  ]
  edge [
    source 17
    target 179
  ]
  edge [
    source 17
    target 180
  ]
  edge [
    source 17
    target 86
  ]
  edge [
    source 17
    target 181
  ]
  edge [
    source 17
    target 182
  ]
  edge [
    source 17
    target 183
  ]
  edge [
    source 17
    target 184
  ]
  edge [
    source 17
    target 185
  ]
  edge [
    source 17
    target 186
  ]
  edge [
    source 18
    target 187
  ]
  edge [
    source 18
    target 188
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 33
  ]
  edge [
    source 19
    target 34
  ]
  edge [
    source 19
    target 189
  ]
  edge [
    source 19
    target 190
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 191
  ]
  edge [
    source 20
    target 192
  ]
  edge [
    source 20
    target 193
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 32
  ]
  edge [
    source 21
    target 33
  ]
  edge [
    source 21
    target 194
  ]
  edge [
    source 21
    target 195
  ]
  edge [
    source 21
    target 196
  ]
  edge [
    source 21
    target 197
  ]
  edge [
    source 21
    target 198
  ]
  edge [
    source 21
    target 199
  ]
  edge [
    source 21
    target 200
  ]
  edge [
    source 21
    target 201
  ]
  edge [
    source 21
    target 202
  ]
  edge [
    source 21
    target 203
  ]
  edge [
    source 21
    target 204
  ]
  edge [
    source 21
    target 205
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 206
  ]
  edge [
    source 22
    target 207
  ]
  edge [
    source 22
    target 208
  ]
  edge [
    source 22
    target 209
  ]
  edge [
    source 22
    target 210
  ]
  edge [
    source 22
    target 211
  ]
  edge [
    source 22
    target 212
  ]
  edge [
    source 22
    target 213
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 30
  ]
  edge [
    source 23
    target 37
  ]
  edge [
    source 23
    target 214
  ]
  edge [
    source 23
    target 152
  ]
  edge [
    source 23
    target 215
  ]
  edge [
    source 23
    target 216
  ]
  edge [
    source 23
    target 217
  ]
  edge [
    source 23
    target 218
  ]
  edge [
    source 23
    target 219
  ]
  edge [
    source 23
    target 220
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 31
  ]
  edge [
    source 24
    target 221
  ]
  edge [
    source 24
    target 222
  ]
  edge [
    source 24
    target 223
  ]
  edge [
    source 24
    target 224
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 225
  ]
  edge [
    source 27
    target 226
  ]
  edge [
    source 27
    target 227
  ]
  edge [
    source 27
    target 228
  ]
  edge [
    source 27
    target 229
  ]
  edge [
    source 27
    target 230
  ]
  edge [
    source 27
    target 231
  ]
  edge [
    source 27
    target 232
  ]
  edge [
    source 27
    target 233
  ]
  edge [
    source 27
    target 234
  ]
  edge [
    source 27
    target 235
  ]
  edge [
    source 27
    target 236
  ]
  edge [
    source 27
    target 164
  ]
  edge [
    source 27
    target 237
  ]
  edge [
    source 27
    target 238
  ]
  edge [
    source 27
    target 239
  ]
  edge [
    source 27
    target 240
  ]
  edge [
    source 27
    target 241
  ]
  edge [
    source 27
    target 242
  ]
  edge [
    source 27
    target 243
  ]
  edge [
    source 27
    target 244
  ]
  edge [
    source 27
    target 245
  ]
  edge [
    source 27
    target 246
  ]
  edge [
    source 27
    target 247
  ]
  edge [
    source 27
    target 248
  ]
  edge [
    source 27
    target 249
  ]
  edge [
    source 27
    target 250
  ]
  edge [
    source 27
    target 251
  ]
  edge [
    source 27
    target 252
  ]
  edge [
    source 27
    target 253
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 254
  ]
  edge [
    source 28
    target 255
  ]
  edge [
    source 28
    target 256
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 257
  ]
  edge [
    source 29
    target 258
  ]
  edge [
    source 29
    target 259
  ]
  edge [
    source 29
    target 260
  ]
  edge [
    source 29
    target 261
  ]
  edge [
    source 29
    target 262
  ]
  edge [
    source 29
    target 263
  ]
  edge [
    source 29
    target 264
  ]
  edge [
    source 29
    target 265
  ]
  edge [
    source 29
    target 266
  ]
  edge [
    source 29
    target 267
  ]
  edge [
    source 29
    target 126
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 268
  ]
  edge [
    source 31
    target 269
  ]
  edge [
    source 32
    target 270
  ]
  edge [
    source 32
    target 109
  ]
  edge [
    source 33
    target 271
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 272
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 273
  ]
  edge [
    source 36
    target 274
  ]
  edge [
    source 36
    target 275
  ]
  edge [
    source 36
    target 276
  ]
  edge [
    source 36
    target 277
  ]
  edge [
    source 36
    target 278
  ]
  edge [
    source 36
    target 279
  ]
  edge [
    source 37
    target 280
  ]
  edge [
    source 37
    target 281
  ]
]
