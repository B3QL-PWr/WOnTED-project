graph [
  maxDegree 20
  minDegree 1
  meanDegree 2
  density 0.03225806451612903
  graphCliqueNumber 3
  node [
    id 0
    label "rok"
    origin "text"
  ]
  node [
    id 1
    label "podatek"
    origin "text"
  ]
  node [
    id 2
    label "mleko"
    origin "text"
  ]
  node [
    id 3
    label "ro&#347;linny"
    origin "text"
  ]
  node [
    id 4
    label "wzrosn&#261;&#263;"
    origin "text"
  ]
  node [
    id 5
    label "proca"
    origin "text"
  ]
  node [
    id 6
    label "stulecie"
  ]
  node [
    id 7
    label "kalendarz"
  ]
  node [
    id 8
    label "czas"
  ]
  node [
    id 9
    label "pora_roku"
  ]
  node [
    id 10
    label "cykl_astronomiczny"
  ]
  node [
    id 11
    label "p&#243;&#322;rocze"
  ]
  node [
    id 12
    label "grupa"
  ]
  node [
    id 13
    label "kwarta&#322;"
  ]
  node [
    id 14
    label "kurs"
  ]
  node [
    id 15
    label "jubileusz"
  ]
  node [
    id 16
    label "miesi&#261;c"
  ]
  node [
    id 17
    label "lata"
  ]
  node [
    id 18
    label "martwy_sezon"
  ]
  node [
    id 19
    label "bilans_handlowy"
  ]
  node [
    id 20
    label "op&#322;ata"
  ]
  node [
    id 21
    label "danina"
  ]
  node [
    id 22
    label "trybut"
  ]
  node [
    id 23
    label "tr&#243;jk&#261;t_Harbergera"
  ]
  node [
    id 24
    label "ssa&#263;"
  ]
  node [
    id 25
    label "laktoferyna"
  ]
  node [
    id 26
    label "bia&#322;ko"
  ]
  node [
    id 27
    label "produkt"
  ]
  node [
    id 28
    label "warzenie_si&#281;"
  ]
  node [
    id 29
    label "szkopek"
  ]
  node [
    id 30
    label "zawiesina"
  ]
  node [
    id 31
    label "jedzenie"
  ]
  node [
    id 32
    label "mg&#322;a"
  ]
  node [
    id 33
    label "laktoza"
  ]
  node [
    id 34
    label "milk"
  ]
  node [
    id 35
    label "nabia&#322;"
  ]
  node [
    id 36
    label "porcja"
  ]
  node [
    id 37
    label "laktacja"
  ]
  node [
    id 38
    label "wydzielina"
  ]
  node [
    id 39
    label "kazeina"
  ]
  node [
    id 40
    label "ryboflawina"
  ]
  node [
    id 41
    label "ciecz"
  ]
  node [
    id 42
    label "ro&#347;linnie"
  ]
  node [
    id 43
    label "naturalny"
  ]
  node [
    id 44
    label "przypominaj&#261;cy"
  ]
  node [
    id 45
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 46
    label "sta&#263;_si&#281;"
  ]
  node [
    id 47
    label "zrodzi&#263;_si&#281;"
  ]
  node [
    id 48
    label "rozwin&#261;&#263;_si&#281;"
  ]
  node [
    id 49
    label "rise"
  ]
  node [
    id 50
    label "increase"
  ]
  node [
    id 51
    label "urosn&#261;&#263;"
  ]
  node [
    id 52
    label "narosn&#261;&#263;"
  ]
  node [
    id 53
    label "wzbi&#263;_si&#281;"
  ]
  node [
    id 54
    label "zrobi&#263;_si&#281;"
  ]
  node [
    id 55
    label "bro&#324;_miotaj&#261;ca"
  ]
  node [
    id 56
    label "zabawka"
  ]
  node [
    id 57
    label "bro&#324;_my&#347;liwska"
  ]
  node [
    id 58
    label "bro&#324;"
  ]
  node [
    id 59
    label "catapult"
  ]
  node [
    id 60
    label "urwis"
  ]
  node [
    id 61
    label "donosi&#263;"
  ]
  node [
    id 62
    label "rzeczpospolita"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 33
    target 61
  ]
  edge [
    source 33
    target 62
  ]
  edge [
    source 61
    target 62
  ]
]
