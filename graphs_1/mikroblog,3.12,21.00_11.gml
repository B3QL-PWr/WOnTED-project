graph [
  maxDegree 39
  minDegree 1
  meanDegree 1.9753086419753085
  density 0.024691358024691357
  graphCliqueNumber 2
  node [
    id 0
    label "d&#322;ugi"
    origin "text"
  ]
  node [
    id 1
    label "czas"
    origin "text"
  ]
  node [
    id 2
    label "podrywa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "pewien"
    origin "text"
  ]
  node [
    id 4
    label "kobieta"
    origin "text"
  ]
  node [
    id 5
    label "daleki"
  ]
  node [
    id 6
    label "d&#322;ugo"
  ]
  node [
    id 7
    label "ruch"
  ]
  node [
    id 8
    label "czasokres"
  ]
  node [
    id 9
    label "trawienie"
  ]
  node [
    id 10
    label "kategoria_gramatyczna"
  ]
  node [
    id 11
    label "period"
  ]
  node [
    id 12
    label "odczyt"
  ]
  node [
    id 13
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 14
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 15
    label "chwila"
  ]
  node [
    id 16
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 17
    label "poprzedzenie"
  ]
  node [
    id 18
    label "koniugacja"
  ]
  node [
    id 19
    label "dzieje"
  ]
  node [
    id 20
    label "poprzedzi&#263;"
  ]
  node [
    id 21
    label "przep&#322;ywanie"
  ]
  node [
    id 22
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 23
    label "odwlekanie_si&#281;"
  ]
  node [
    id 24
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 25
    label "Zeitgeist"
  ]
  node [
    id 26
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 27
    label "okres_czasu"
  ]
  node [
    id 28
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 29
    label "pochodzi&#263;"
  ]
  node [
    id 30
    label "schy&#322;ek"
  ]
  node [
    id 31
    label "czwarty_wymiar"
  ]
  node [
    id 32
    label "chronometria"
  ]
  node [
    id 33
    label "poprzedzanie"
  ]
  node [
    id 34
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 35
    label "pogoda"
  ]
  node [
    id 36
    label "zegar"
  ]
  node [
    id 37
    label "trawi&#263;"
  ]
  node [
    id 38
    label "pochodzenie"
  ]
  node [
    id 39
    label "poprzedza&#263;"
  ]
  node [
    id 40
    label "time_period"
  ]
  node [
    id 41
    label "rachuba_czasu"
  ]
  node [
    id 42
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 43
    label "czasoprzestrze&#324;"
  ]
  node [
    id 44
    label "laba"
  ]
  node [
    id 45
    label "podmywa&#263;"
  ]
  node [
    id 46
    label "startowa&#263;"
  ]
  node [
    id 47
    label "konkurowa&#263;"
  ]
  node [
    id 48
    label "nadwyr&#281;&#380;a&#263;"
  ]
  node [
    id 49
    label "pobudza&#263;"
  ]
  node [
    id 50
    label "uderza&#263;_do_panny"
  ]
  node [
    id 51
    label "nadszarpywa&#263;"
  ]
  node [
    id 52
    label "woo"
  ]
  node [
    id 53
    label "porusza&#263;"
  ]
  node [
    id 54
    label "rwa&#263;"
  ]
  node [
    id 55
    label "sabotage"
  ]
  node [
    id 56
    label "stara&#263;_si&#281;"
  ]
  node [
    id 57
    label "podnosi&#263;"
  ]
  node [
    id 58
    label "upewnienie_si&#281;"
  ]
  node [
    id 59
    label "wierzenie"
  ]
  node [
    id 60
    label "mo&#380;liwy"
  ]
  node [
    id 61
    label "ufanie"
  ]
  node [
    id 62
    label "jaki&#347;"
  ]
  node [
    id 63
    label "spokojny"
  ]
  node [
    id 64
    label "upewnianie_si&#281;"
  ]
  node [
    id 65
    label "cz&#322;owiek"
  ]
  node [
    id 66
    label "przekwitanie"
  ]
  node [
    id 67
    label "m&#281;&#380;yna"
  ]
  node [
    id 68
    label "babka"
  ]
  node [
    id 69
    label "samica"
  ]
  node [
    id 70
    label "doros&#322;y"
  ]
  node [
    id 71
    label "ulec"
  ]
  node [
    id 72
    label "uleganie"
  ]
  node [
    id 73
    label "partnerka"
  ]
  node [
    id 74
    label "&#380;ona"
  ]
  node [
    id 75
    label "ulega&#263;"
  ]
  node [
    id 76
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 77
    label "pa&#324;stwo"
  ]
  node [
    id 78
    label "ulegni&#281;cie"
  ]
  node [
    id 79
    label "menopauza"
  ]
  node [
    id 80
    label "&#322;ono"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
]
