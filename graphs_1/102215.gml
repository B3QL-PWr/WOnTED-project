graph [
  maxDegree 33
  minDegree 1
  meanDegree 10.133333333333333
  density 0.13693693693693693
  graphCliqueNumber 26
  node [
    id 0
    label "uwzgl&#281;dnia&#263;"
    origin "text"
  ]
  node [
    id 1
    label "traktat"
    origin "text"
  ]
  node [
    id 2
    label "ustanawia&#263;"
    origin "text"
  ]
  node [
    id 3
    label "wsp&#243;lnota"
    origin "text"
  ]
  node [
    id 4
    label "europejski"
    origin "text"
  ]
  node [
    id 5
    label "my&#347;le&#263;"
  ]
  node [
    id 6
    label "involve"
  ]
  node [
    id 7
    label "opracowanie"
  ]
  node [
    id 8
    label "traktat_wersalski"
  ]
  node [
    id 9
    label "ONZ"
  ]
  node [
    id 10
    label "zawrze&#263;"
  ]
  node [
    id 11
    label "treaty"
  ]
  node [
    id 12
    label "obja&#347;nienie"
  ]
  node [
    id 13
    label "umowa"
  ]
  node [
    id 14
    label "rozumowanie"
  ]
  node [
    id 15
    label "NATO"
  ]
  node [
    id 16
    label "zawarcie"
  ]
  node [
    id 17
    label "tekst"
  ]
  node [
    id 18
    label "cytat"
  ]
  node [
    id 19
    label "robi&#263;"
  ]
  node [
    id 20
    label "wskazywa&#263;"
  ]
  node [
    id 21
    label "powodowa&#263;"
  ]
  node [
    id 22
    label "set"
  ]
  node [
    id 23
    label "ustala&#263;"
  ]
  node [
    id 24
    label "Skandynawia"
  ]
  node [
    id 25
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 26
    label "partnership"
  ]
  node [
    id 27
    label "zwi&#261;zek"
  ]
  node [
    id 28
    label "zwi&#261;za&#263;"
  ]
  node [
    id 29
    label "Walencja"
  ]
  node [
    id 30
    label "society"
  ]
  node [
    id 31
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 32
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 33
    label "bratnia_dusza"
  ]
  node [
    id 34
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 35
    label "marriage"
  ]
  node [
    id 36
    label "zwi&#261;zanie"
  ]
  node [
    id 37
    label "Ba&#322;kany"
  ]
  node [
    id 38
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 39
    label "wi&#261;zanie"
  ]
  node [
    id 40
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 41
    label "podobie&#324;stwo"
  ]
  node [
    id 42
    label "European"
  ]
  node [
    id 43
    label "po_europejsku"
  ]
  node [
    id 44
    label "charakterystyczny"
  ]
  node [
    id 45
    label "europejsko"
  ]
  node [
    id 46
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 47
    label "typowy"
  ]
  node [
    id 48
    label "rozporz&#261;dzi&#263;"
  ]
  node [
    id 49
    label "rada"
  ]
  node [
    id 50
    label "we"
  ]
  node [
    id 51
    label "nr"
  ]
  node [
    id 52
    label "58"
  ]
  node [
    id 53
    label "2003"
  ]
  node [
    id 54
    label "zeszyt"
  ]
  node [
    id 55
    label "dzie&#324;"
  ]
  node [
    id 56
    label "19"
  ]
  node [
    id 57
    label "grudzie&#324;"
  ]
  node [
    id 58
    label "2002"
  ]
  node [
    id 59
    label "rok"
  ]
  node [
    id 60
    label "statut"
  ]
  node [
    id 61
    label "agencja"
  ]
  node [
    id 62
    label "wykonawczy"
  ]
  node [
    id 63
    label "kt&#243;ry"
  ]
  node [
    id 64
    label "zosta&#263;"
  ]
  node [
    id 65
    label "powierzy&#263;"
  ]
  node [
    id 66
    label "niekt&#243;ry"
  ]
  node [
    id 67
    label "zada&#263;"
  ]
  node [
    id 68
    label "wyspa"
  ]
  node [
    id 69
    label "zakres"
  ]
  node [
    id 70
    label "zarz&#261;dza&#263;"
  ]
  node [
    id 71
    label "program"
  ]
  node [
    id 72
    label "wsp&#243;lnotowy"
  ]
  node [
    id 73
    label "komitet"
  ]
  node [
    id 74
    label "do&#160;spraw"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 50
  ]
  edge [
    source 48
    target 51
  ]
  edge [
    source 48
    target 52
  ]
  edge [
    source 48
    target 53
  ]
  edge [
    source 48
    target 54
  ]
  edge [
    source 48
    target 55
  ]
  edge [
    source 48
    target 56
  ]
  edge [
    source 48
    target 57
  ]
  edge [
    source 48
    target 58
  ]
  edge [
    source 48
    target 59
  ]
  edge [
    source 48
    target 60
  ]
  edge [
    source 48
    target 61
  ]
  edge [
    source 48
    target 62
  ]
  edge [
    source 48
    target 63
  ]
  edge [
    source 48
    target 64
  ]
  edge [
    source 48
    target 65
  ]
  edge [
    source 48
    target 66
  ]
  edge [
    source 48
    target 67
  ]
  edge [
    source 48
    target 68
  ]
  edge [
    source 48
    target 69
  ]
  edge [
    source 48
    target 70
  ]
  edge [
    source 48
    target 71
  ]
  edge [
    source 48
    target 72
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 51
  ]
  edge [
    source 49
    target 52
  ]
  edge [
    source 49
    target 53
  ]
  edge [
    source 49
    target 54
  ]
  edge [
    source 49
    target 55
  ]
  edge [
    source 49
    target 56
  ]
  edge [
    source 49
    target 57
  ]
  edge [
    source 49
    target 58
  ]
  edge [
    source 49
    target 59
  ]
  edge [
    source 49
    target 60
  ]
  edge [
    source 49
    target 61
  ]
  edge [
    source 49
    target 62
  ]
  edge [
    source 49
    target 63
  ]
  edge [
    source 49
    target 64
  ]
  edge [
    source 49
    target 65
  ]
  edge [
    source 49
    target 66
  ]
  edge [
    source 49
    target 67
  ]
  edge [
    source 49
    target 68
  ]
  edge [
    source 49
    target 69
  ]
  edge [
    source 49
    target 70
  ]
  edge [
    source 49
    target 71
  ]
  edge [
    source 49
    target 72
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 52
  ]
  edge [
    source 50
    target 53
  ]
  edge [
    source 50
    target 54
  ]
  edge [
    source 50
    target 55
  ]
  edge [
    source 50
    target 56
  ]
  edge [
    source 50
    target 57
  ]
  edge [
    source 50
    target 58
  ]
  edge [
    source 50
    target 59
  ]
  edge [
    source 50
    target 60
  ]
  edge [
    source 50
    target 61
  ]
  edge [
    source 50
    target 62
  ]
  edge [
    source 50
    target 63
  ]
  edge [
    source 50
    target 64
  ]
  edge [
    source 50
    target 65
  ]
  edge [
    source 50
    target 66
  ]
  edge [
    source 50
    target 67
  ]
  edge [
    source 50
    target 68
  ]
  edge [
    source 50
    target 69
  ]
  edge [
    source 50
    target 70
  ]
  edge [
    source 50
    target 71
  ]
  edge [
    source 50
    target 72
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 53
  ]
  edge [
    source 51
    target 54
  ]
  edge [
    source 51
    target 55
  ]
  edge [
    source 51
    target 56
  ]
  edge [
    source 51
    target 57
  ]
  edge [
    source 51
    target 58
  ]
  edge [
    source 51
    target 59
  ]
  edge [
    source 51
    target 60
  ]
  edge [
    source 51
    target 61
  ]
  edge [
    source 51
    target 62
  ]
  edge [
    source 51
    target 63
  ]
  edge [
    source 51
    target 64
  ]
  edge [
    source 51
    target 65
  ]
  edge [
    source 51
    target 66
  ]
  edge [
    source 51
    target 67
  ]
  edge [
    source 51
    target 68
  ]
  edge [
    source 51
    target 69
  ]
  edge [
    source 51
    target 70
  ]
  edge [
    source 51
    target 71
  ]
  edge [
    source 51
    target 72
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 54
  ]
  edge [
    source 52
    target 55
  ]
  edge [
    source 52
    target 56
  ]
  edge [
    source 52
    target 57
  ]
  edge [
    source 52
    target 58
  ]
  edge [
    source 52
    target 59
  ]
  edge [
    source 52
    target 60
  ]
  edge [
    source 52
    target 61
  ]
  edge [
    source 52
    target 62
  ]
  edge [
    source 52
    target 63
  ]
  edge [
    source 52
    target 64
  ]
  edge [
    source 52
    target 65
  ]
  edge [
    source 52
    target 66
  ]
  edge [
    source 52
    target 67
  ]
  edge [
    source 52
    target 68
  ]
  edge [
    source 52
    target 69
  ]
  edge [
    source 52
    target 70
  ]
  edge [
    source 52
    target 71
  ]
  edge [
    source 52
    target 72
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 55
  ]
  edge [
    source 53
    target 56
  ]
  edge [
    source 53
    target 57
  ]
  edge [
    source 53
    target 58
  ]
  edge [
    source 53
    target 59
  ]
  edge [
    source 53
    target 60
  ]
  edge [
    source 53
    target 61
  ]
  edge [
    source 53
    target 62
  ]
  edge [
    source 53
    target 63
  ]
  edge [
    source 53
    target 64
  ]
  edge [
    source 53
    target 65
  ]
  edge [
    source 53
    target 66
  ]
  edge [
    source 53
    target 67
  ]
  edge [
    source 53
    target 68
  ]
  edge [
    source 53
    target 69
  ]
  edge [
    source 53
    target 70
  ]
  edge [
    source 53
    target 71
  ]
  edge [
    source 53
    target 72
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 56
  ]
  edge [
    source 54
    target 57
  ]
  edge [
    source 54
    target 58
  ]
  edge [
    source 54
    target 59
  ]
  edge [
    source 54
    target 60
  ]
  edge [
    source 54
    target 61
  ]
  edge [
    source 54
    target 62
  ]
  edge [
    source 54
    target 63
  ]
  edge [
    source 54
    target 64
  ]
  edge [
    source 54
    target 65
  ]
  edge [
    source 54
    target 66
  ]
  edge [
    source 54
    target 67
  ]
  edge [
    source 54
    target 68
  ]
  edge [
    source 54
    target 69
  ]
  edge [
    source 54
    target 70
  ]
  edge [
    source 54
    target 71
  ]
  edge [
    source 54
    target 72
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 57
  ]
  edge [
    source 55
    target 58
  ]
  edge [
    source 55
    target 59
  ]
  edge [
    source 55
    target 60
  ]
  edge [
    source 55
    target 61
  ]
  edge [
    source 55
    target 62
  ]
  edge [
    source 55
    target 63
  ]
  edge [
    source 55
    target 64
  ]
  edge [
    source 55
    target 65
  ]
  edge [
    source 55
    target 66
  ]
  edge [
    source 55
    target 67
  ]
  edge [
    source 55
    target 68
  ]
  edge [
    source 55
    target 69
  ]
  edge [
    source 55
    target 70
  ]
  edge [
    source 55
    target 71
  ]
  edge [
    source 55
    target 72
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 58
  ]
  edge [
    source 56
    target 59
  ]
  edge [
    source 56
    target 60
  ]
  edge [
    source 56
    target 61
  ]
  edge [
    source 56
    target 62
  ]
  edge [
    source 56
    target 63
  ]
  edge [
    source 56
    target 64
  ]
  edge [
    source 56
    target 65
  ]
  edge [
    source 56
    target 66
  ]
  edge [
    source 56
    target 67
  ]
  edge [
    source 56
    target 68
  ]
  edge [
    source 56
    target 69
  ]
  edge [
    source 56
    target 70
  ]
  edge [
    source 56
    target 71
  ]
  edge [
    source 56
    target 72
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 59
  ]
  edge [
    source 57
    target 60
  ]
  edge [
    source 57
    target 61
  ]
  edge [
    source 57
    target 62
  ]
  edge [
    source 57
    target 63
  ]
  edge [
    source 57
    target 64
  ]
  edge [
    source 57
    target 65
  ]
  edge [
    source 57
    target 66
  ]
  edge [
    source 57
    target 67
  ]
  edge [
    source 57
    target 68
  ]
  edge [
    source 57
    target 69
  ]
  edge [
    source 57
    target 70
  ]
  edge [
    source 57
    target 71
  ]
  edge [
    source 57
    target 72
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 60
  ]
  edge [
    source 58
    target 61
  ]
  edge [
    source 58
    target 62
  ]
  edge [
    source 58
    target 63
  ]
  edge [
    source 58
    target 64
  ]
  edge [
    source 58
    target 65
  ]
  edge [
    source 58
    target 66
  ]
  edge [
    source 58
    target 67
  ]
  edge [
    source 58
    target 68
  ]
  edge [
    source 58
    target 69
  ]
  edge [
    source 58
    target 70
  ]
  edge [
    source 58
    target 71
  ]
  edge [
    source 58
    target 72
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 61
  ]
  edge [
    source 59
    target 62
  ]
  edge [
    source 59
    target 63
  ]
  edge [
    source 59
    target 64
  ]
  edge [
    source 59
    target 65
  ]
  edge [
    source 59
    target 66
  ]
  edge [
    source 59
    target 67
  ]
  edge [
    source 59
    target 68
  ]
  edge [
    source 59
    target 69
  ]
  edge [
    source 59
    target 70
  ]
  edge [
    source 59
    target 71
  ]
  edge [
    source 59
    target 72
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 62
  ]
  edge [
    source 60
    target 63
  ]
  edge [
    source 60
    target 64
  ]
  edge [
    source 60
    target 65
  ]
  edge [
    source 60
    target 66
  ]
  edge [
    source 60
    target 67
  ]
  edge [
    source 60
    target 68
  ]
  edge [
    source 60
    target 69
  ]
  edge [
    source 60
    target 70
  ]
  edge [
    source 60
    target 71
  ]
  edge [
    source 60
    target 72
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 63
  ]
  edge [
    source 61
    target 64
  ]
  edge [
    source 61
    target 65
  ]
  edge [
    source 61
    target 66
  ]
  edge [
    source 61
    target 67
  ]
  edge [
    source 61
    target 68
  ]
  edge [
    source 61
    target 69
  ]
  edge [
    source 61
    target 70
  ]
  edge [
    source 61
    target 71
  ]
  edge [
    source 61
    target 72
  ]
  edge [
    source 61
    target 73
  ]
  edge [
    source 61
    target 74
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 64
  ]
  edge [
    source 62
    target 65
  ]
  edge [
    source 62
    target 66
  ]
  edge [
    source 62
    target 67
  ]
  edge [
    source 62
    target 68
  ]
  edge [
    source 62
    target 69
  ]
  edge [
    source 62
    target 70
  ]
  edge [
    source 62
    target 71
  ]
  edge [
    source 62
    target 72
  ]
  edge [
    source 62
    target 73
  ]
  edge [
    source 62
    target 74
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 65
  ]
  edge [
    source 63
    target 66
  ]
  edge [
    source 63
    target 67
  ]
  edge [
    source 63
    target 68
  ]
  edge [
    source 63
    target 69
  ]
  edge [
    source 63
    target 70
  ]
  edge [
    source 63
    target 71
  ]
  edge [
    source 63
    target 72
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 66
  ]
  edge [
    source 64
    target 67
  ]
  edge [
    source 64
    target 68
  ]
  edge [
    source 64
    target 69
  ]
  edge [
    source 64
    target 70
  ]
  edge [
    source 64
    target 71
  ]
  edge [
    source 64
    target 72
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 67
  ]
  edge [
    source 65
    target 68
  ]
  edge [
    source 65
    target 69
  ]
  edge [
    source 65
    target 70
  ]
  edge [
    source 65
    target 71
  ]
  edge [
    source 65
    target 72
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 66
    target 68
  ]
  edge [
    source 66
    target 69
  ]
  edge [
    source 66
    target 70
  ]
  edge [
    source 66
    target 71
  ]
  edge [
    source 66
    target 72
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 67
    target 69
  ]
  edge [
    source 67
    target 70
  ]
  edge [
    source 67
    target 71
  ]
  edge [
    source 67
    target 72
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 68
    target 70
  ]
  edge [
    source 68
    target 71
  ]
  edge [
    source 68
    target 72
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 69
    target 71
  ]
  edge [
    source 69
    target 72
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 72
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 73
    target 74
  ]
]
