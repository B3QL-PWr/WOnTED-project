graph [
  maxDegree 14
  minDegree 1
  meanDegree 1.9166666666666667
  density 0.08333333333333333
  graphCliqueNumber 4
  node [
    id 0
    label "nowy"
    origin "text"
  ]
  node [
    id 1
    label "&#347;wi&#281;t&#243;w"
    origin "text"
  ]
  node [
    id 2
    label "cz&#322;owiek"
  ]
  node [
    id 3
    label "nowotny"
  ]
  node [
    id 4
    label "drugi"
  ]
  node [
    id 5
    label "kolejny"
  ]
  node [
    id 6
    label "bie&#380;&#261;cy"
  ]
  node [
    id 7
    label "nowo"
  ]
  node [
    id 8
    label "narybek"
  ]
  node [
    id 9
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 10
    label "obcy"
  ]
  node [
    id 11
    label "&#346;wi&#281;t&#243;w"
  ]
  node [
    id 12
    label "Deutsch"
  ]
  node [
    id 13
    label "Wette"
  ]
  node [
    id 14
    label "Swetow"
  ]
  node [
    id 15
    label "thewtonicalis"
  ]
  node [
    id 16
    label "wojna"
  ]
  node [
    id 17
    label "30"
  ]
  node [
    id 18
    label "letni"
  ]
  node [
    id 19
    label "&#346;wi&#281;towa"
  ]
  node [
    id 20
    label "Rac&#322;awice"
  ]
  node [
    id 21
    label "&#347;l&#261;ski"
  ]
  node [
    id 22
    label "kluba"
  ]
  node [
    id 23
    label "sportowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 11
    target 22
  ]
  edge [
    source 11
    target 23
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 18
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 22
    target 23
  ]
]
