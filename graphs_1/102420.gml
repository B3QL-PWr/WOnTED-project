graph [
  maxDegree 39
  minDegree 1
  meanDegree 2.0840336134453783
  density 0.017661301808859137
  graphCliqueNumber 2
  node [
    id 0
    label "tym"
    origin "text"
  ]
  node [
    id 1
    label "miesi&#261;c"
    origin "text"
  ]
  node [
    id 2
    label "odby&#263;"
    origin "text"
  ]
  node [
    id 3
    label "si&#281;"
    origin "text"
  ]
  node [
    id 4
    label "spotkanie"
    origin "text"
  ]
  node [
    id 5
    label "cykl"
    origin "text"
  ]
  node [
    id 6
    label "szczeg&#243;lnie"
    origin "text"
  ]
  node [
    id 7
    label "przeprasza&#263;"
    origin "text"
  ]
  node [
    id 8
    label "osoba"
    origin "text"
  ]
  node [
    id 9
    label "dopytywa&#263;"
    origin "text"
  ]
  node [
    id 10
    label "nasa"
    origin "text"
  ]
  node [
    id 11
    label "termin"
    origin "text"
  ]
  node [
    id 12
    label "spodziewa&#263;"
    origin "text"
  ]
  node [
    id 13
    label "co&#347;"
    origin "text"
  ]
  node [
    id 14
    label "bliski"
    origin "text"
  ]
  node [
    id 15
    label "wtorek"
    origin "text"
  ]
  node [
    id 16
    label "czas"
  ]
  node [
    id 17
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 18
    label "rok"
  ]
  node [
    id 19
    label "miech"
  ]
  node [
    id 20
    label "kalendy"
  ]
  node [
    id 21
    label "tydzie&#324;"
  ]
  node [
    id 22
    label "reserve"
  ]
  node [
    id 23
    label "przej&#347;&#263;"
  ]
  node [
    id 24
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 25
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 26
    label "po&#380;egnanie"
  ]
  node [
    id 27
    label "spowodowanie"
  ]
  node [
    id 28
    label "znalezienie"
  ]
  node [
    id 29
    label "znajomy"
  ]
  node [
    id 30
    label "doznanie"
  ]
  node [
    id 31
    label "employment"
  ]
  node [
    id 32
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 33
    label "gather"
  ]
  node [
    id 34
    label "powitanie"
  ]
  node [
    id 35
    label "spotykanie"
  ]
  node [
    id 36
    label "wydarzenie"
  ]
  node [
    id 37
    label "gathering"
  ]
  node [
    id 38
    label "spotkanie_si&#281;"
  ]
  node [
    id 39
    label "zdarzenie_si&#281;"
  ]
  node [
    id 40
    label "match"
  ]
  node [
    id 41
    label "zawarcie"
  ]
  node [
    id 42
    label "sekwencja"
  ]
  node [
    id 43
    label "edycja"
  ]
  node [
    id 44
    label "przebieg"
  ]
  node [
    id 45
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 46
    label "okres"
  ]
  node [
    id 47
    label "cykl_p&#322;ciowy"
  ]
  node [
    id 48
    label "cycle"
  ]
  node [
    id 49
    label "owulacja"
  ]
  node [
    id 50
    label "miesi&#261;czka"
  ]
  node [
    id 51
    label "set"
  ]
  node [
    id 52
    label "szczeg&#243;lny"
  ]
  node [
    id 53
    label "osobnie"
  ]
  node [
    id 54
    label "wyj&#261;tkowo"
  ]
  node [
    id 55
    label "specially"
  ]
  node [
    id 56
    label "sk&#322;ada&#263;"
  ]
  node [
    id 57
    label "Zgredek"
  ]
  node [
    id 58
    label "kategoria_gramatyczna"
  ]
  node [
    id 59
    label "Casanova"
  ]
  node [
    id 60
    label "Don_Juan"
  ]
  node [
    id 61
    label "Gargantua"
  ]
  node [
    id 62
    label "Faust"
  ]
  node [
    id 63
    label "profanum"
  ]
  node [
    id 64
    label "Chocho&#322;"
  ]
  node [
    id 65
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 66
    label "koniugacja"
  ]
  node [
    id 67
    label "Winnetou"
  ]
  node [
    id 68
    label "Dwukwiat"
  ]
  node [
    id 69
    label "homo_sapiens"
  ]
  node [
    id 70
    label "Edyp"
  ]
  node [
    id 71
    label "Herkules_Poirot"
  ]
  node [
    id 72
    label "ludzko&#347;&#263;"
  ]
  node [
    id 73
    label "mikrokosmos"
  ]
  node [
    id 74
    label "person"
  ]
  node [
    id 75
    label "Sherlock_Holmes"
  ]
  node [
    id 76
    label "portrecista"
  ]
  node [
    id 77
    label "Szwejk"
  ]
  node [
    id 78
    label "Hamlet"
  ]
  node [
    id 79
    label "duch"
  ]
  node [
    id 80
    label "g&#322;owa"
  ]
  node [
    id 81
    label "oddzia&#322;ywanie"
  ]
  node [
    id 82
    label "Quasimodo"
  ]
  node [
    id 83
    label "Dulcynea"
  ]
  node [
    id 84
    label "Don_Kiszot"
  ]
  node [
    id 85
    label "Wallenrod"
  ]
  node [
    id 86
    label "Plastu&#347;"
  ]
  node [
    id 87
    label "Harry_Potter"
  ]
  node [
    id 88
    label "figura"
  ]
  node [
    id 89
    label "parali&#380;owa&#263;"
  ]
  node [
    id 90
    label "istota"
  ]
  node [
    id 91
    label "Werter"
  ]
  node [
    id 92
    label "antropochoria"
  ]
  node [
    id 93
    label "posta&#263;"
  ]
  node [
    id 94
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 95
    label "przypadni&#281;cie"
  ]
  node [
    id 96
    label "chronogram"
  ]
  node [
    id 97
    label "nazewnictwo"
  ]
  node [
    id 98
    label "ekspiracja"
  ]
  node [
    id 99
    label "nazwa"
  ]
  node [
    id 100
    label "przypa&#347;&#263;"
  ]
  node [
    id 101
    label "praktyka"
  ]
  node [
    id 102
    label "term"
  ]
  node [
    id 103
    label "thing"
  ]
  node [
    id 104
    label "cosik"
  ]
  node [
    id 105
    label "cz&#322;owiek"
  ]
  node [
    id 106
    label "blisko"
  ]
  node [
    id 107
    label "przesz&#322;y"
  ]
  node [
    id 108
    label "gotowy"
  ]
  node [
    id 109
    label "dok&#322;adny"
  ]
  node [
    id 110
    label "kr&#243;tki"
  ]
  node [
    id 111
    label "przysz&#322;y"
  ]
  node [
    id 112
    label "oddalony"
  ]
  node [
    id 113
    label "silny"
  ]
  node [
    id 114
    label "zbli&#380;enie"
  ]
  node [
    id 115
    label "zwi&#261;zany"
  ]
  node [
    id 116
    label "nieodleg&#322;y"
  ]
  node [
    id 117
    label "ma&#322;y"
  ]
  node [
    id 118
    label "dzie&#324;_powszedni"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 16
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 15
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 11
    target 16
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 11
    target 98
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 11
    target 100
  ]
  edge [
    source 11
    target 101
  ]
  edge [
    source 11
    target 102
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 103
  ]
  edge [
    source 13
    target 104
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 105
  ]
  edge [
    source 14
    target 106
  ]
  edge [
    source 14
    target 107
  ]
  edge [
    source 14
    target 108
  ]
  edge [
    source 14
    target 109
  ]
  edge [
    source 14
    target 110
  ]
  edge [
    source 14
    target 29
  ]
  edge [
    source 14
    target 111
  ]
  edge [
    source 14
    target 112
  ]
  edge [
    source 14
    target 113
  ]
  edge [
    source 14
    target 114
  ]
  edge [
    source 14
    target 115
  ]
  edge [
    source 14
    target 116
  ]
  edge [
    source 14
    target 117
  ]
  edge [
    source 15
    target 118
  ]
  edge [
    source 15
    target 21
  ]
]
