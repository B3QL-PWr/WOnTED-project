graph [
  maxDegree 37
  minDegree 1
  meanDegree 1.9545454545454546
  density 0.045454545454545456
  graphCliqueNumber 2
  node [
    id 0
    label "pan"
    origin "text"
  ]
  node [
    id 1
    label "wrona"
    origin "text"
  ]
  node [
    id 2
    label "cz&#322;owiek"
  ]
  node [
    id 3
    label "profesor"
  ]
  node [
    id 4
    label "kszta&#322;ciciel"
  ]
  node [
    id 5
    label "jegomo&#347;&#263;"
  ]
  node [
    id 6
    label "zwrot"
  ]
  node [
    id 7
    label "pracodawca"
  ]
  node [
    id 8
    label "rz&#261;dzenie"
  ]
  node [
    id 9
    label "m&#261;&#380;"
  ]
  node [
    id 10
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 11
    label "ch&#322;opina"
  ]
  node [
    id 12
    label "bratek"
  ]
  node [
    id 13
    label "opiekun"
  ]
  node [
    id 14
    label "doros&#322;y"
  ]
  node [
    id 15
    label "preceptor"
  ]
  node [
    id 16
    label "Midas"
  ]
  node [
    id 17
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 18
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 19
    label "murza"
  ]
  node [
    id 20
    label "ojciec"
  ]
  node [
    id 21
    label "androlog"
  ]
  node [
    id 22
    label "pupil"
  ]
  node [
    id 23
    label "efendi"
  ]
  node [
    id 24
    label "nabab"
  ]
  node [
    id 25
    label "w&#322;odarz"
  ]
  node [
    id 26
    label "szkolnik"
  ]
  node [
    id 27
    label "pedagog"
  ]
  node [
    id 28
    label "popularyzator"
  ]
  node [
    id 29
    label "andropauza"
  ]
  node [
    id 30
    label "gra_w_karty"
  ]
  node [
    id 31
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 32
    label "Mieszko_I"
  ]
  node [
    id 33
    label "bogaty"
  ]
  node [
    id 34
    label "samiec"
  ]
  node [
    id 35
    label "przyw&#243;dca"
  ]
  node [
    id 36
    label "pa&#324;stwo"
  ]
  node [
    id 37
    label "belfer"
  ]
  node [
    id 38
    label "klacz"
  ]
  node [
    id 39
    label "zakraka&#263;"
  ]
  node [
    id 40
    label "kraka&#263;"
  ]
  node [
    id 41
    label "gapa"
  ]
  node [
    id 42
    label "ptak_pospolity"
  ]
  node [
    id 43
    label "krukowate"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
]
