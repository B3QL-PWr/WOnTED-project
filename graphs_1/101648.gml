graph [
  maxDegree 28
  minDegree 1
  meanDegree 2.128301886792453
  density 0.008061749571183533
  graphCliqueNumber 3
  node [
    id 0
    label "nasi"
    origin "text"
  ]
  node [
    id 1
    label "zdanie"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "domniemany"
    origin "text"
  ]
  node [
    id 4
    label "uprawnienie"
    origin "text"
  ]
  node [
    id 5
    label "gmin"
    origin "text"
  ]
  node [
    id 6
    label "ten"
    origin "text"
  ]
  node [
    id 7
    label "chwila"
    origin "text"
  ]
  node [
    id 8
    label "cz&#281;&#347;&#263;"
    origin "text"
  ]
  node [
    id 9
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 10
    label "skorzysta&#263;"
    origin "text"
  ]
  node [
    id 11
    label "nawet"
    origin "text"
  ]
  node [
    id 12
    label "je&#380;eli"
    origin "text"
  ]
  node [
    id 13
    label "wyst&#261;pi&#263;"
    origin "text"
  ]
  node [
    id 14
    label "taka"
    origin "text"
  ]
  node [
    id 15
    label "sytuacja"
    origin "text"
  ]
  node [
    id 16
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 17
    label "odnosi&#263;"
    origin "text"
  ]
  node [
    id 18
    label "si&#281;"
    origin "text"
  ]
  node [
    id 19
    label "wyrok"
    origin "text"
  ]
  node [
    id 20
    label "trybuna&#322;"
    origin "text"
  ]
  node [
    id 21
    label "konstytucyjny"
    origin "text"
  ]
  node [
    id 22
    label "stwierdzi&#263;"
    origin "text"
  ]
  node [
    id 23
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 24
    label "pobiera&#263;"
    origin "text"
  ]
  node [
    id 25
    label "podatek"
    origin "text"
  ]
  node [
    id 26
    label "nieruchomo&#347;&#263;"
    origin "text"
  ]
  node [
    id 27
    label "op&#322;ata"
    origin "text"
  ]
  node [
    id 28
    label "targowy"
    origin "text"
  ]
  node [
    id 29
    label "tutaj"
    origin "text"
  ]
  node [
    id 30
    label "by&#263;by"
    origin "text"
  ]
  node [
    id 31
    label "pewne"
    origin "text"
  ]
  node [
    id 32
    label "ubytek"
    origin "text"
  ]
  node [
    id 33
    label "tym"
    origin "text"
  ]
  node [
    id 34
    label "nie"
    origin "text"
  ]
  node [
    id 35
    label "wydawa&#263;"
    origin "text"
  ]
  node [
    id 36
    label "aby"
    origin "text"
  ]
  node [
    id 37
    label "znacz&#261;cy"
    origin "text"
  ]
  node [
    id 38
    label "zr&#243;&#380;nicowanie"
    origin "text"
  ]
  node [
    id 39
    label "wysoko&#347;&#263;"
    origin "text"
  ]
  node [
    id 40
    label "stawek"
    origin "text"
  ]
  node [
    id 41
    label "doch&#243;d"
    origin "text"
  ]
  node [
    id 42
    label "tytu&#322;"
    origin "text"
  ]
  node [
    id 43
    label "nieco"
    origin "text"
  ]
  node [
    id 44
    label "wysoki"
    origin "text"
  ]
  node [
    id 45
    label "dzi&#281;kowa&#263;"
    origin "text"
  ]
  node [
    id 46
    label "bardzo"
    origin "text"
  ]
  node [
    id 47
    label "attitude"
  ]
  node [
    id 48
    label "system"
  ]
  node [
    id 49
    label "przedstawienie"
  ]
  node [
    id 50
    label "fraza"
  ]
  node [
    id 51
    label "prison_term"
  ]
  node [
    id 52
    label "adjudication"
  ]
  node [
    id 53
    label "przekazanie"
  ]
  node [
    id 54
    label "pass"
  ]
  node [
    id 55
    label "wyra&#380;enie"
  ]
  node [
    id 56
    label "okres"
  ]
  node [
    id 57
    label "z&#322;o&#380;enie_egzaminu"
  ]
  node [
    id 58
    label "wypowiedzenie"
  ]
  node [
    id 59
    label "konektyw"
  ]
  node [
    id 60
    label "zaliczenie"
  ]
  node [
    id 61
    label "stanowisko"
  ]
  node [
    id 62
    label "powierzenie"
  ]
  node [
    id 63
    label "antylogizm"
  ]
  node [
    id 64
    label "zmuszenie"
  ]
  node [
    id 65
    label "szko&#322;a"
  ]
  node [
    id 66
    label "si&#281;ga&#263;"
  ]
  node [
    id 67
    label "trwa&#263;"
  ]
  node [
    id 68
    label "obecno&#347;&#263;"
  ]
  node [
    id 69
    label "stan"
  ]
  node [
    id 70
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 71
    label "stand"
  ]
  node [
    id 72
    label "mie&#263;_miejsce"
  ]
  node [
    id 73
    label "uczestniczy&#263;"
  ]
  node [
    id 74
    label "chodzi&#263;"
  ]
  node [
    id 75
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 76
    label "equal"
  ]
  node [
    id 77
    label "przypuszczalny"
  ]
  node [
    id 78
    label "hipotetyczny"
  ]
  node [
    id 79
    label "dokument"
  ]
  node [
    id 80
    label "czynno&#347;&#263;"
  ]
  node [
    id 81
    label "spowodowanie"
  ]
  node [
    id 82
    label "title"
  ]
  node [
    id 83
    label "law"
  ]
  node [
    id 84
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 85
    label "authorization"
  ]
  node [
    id 86
    label "stan_trzeci"
  ]
  node [
    id 87
    label "gminno&#347;&#263;"
  ]
  node [
    id 88
    label "okre&#347;lony"
  ]
  node [
    id 89
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 90
    label "czas"
  ]
  node [
    id 91
    label "time"
  ]
  node [
    id 92
    label "whole"
  ]
  node [
    id 93
    label "Rzym_Zachodni"
  ]
  node [
    id 94
    label "element"
  ]
  node [
    id 95
    label "ilo&#347;&#263;"
  ]
  node [
    id 96
    label "urz&#261;dzenie"
  ]
  node [
    id 97
    label "Rzym_Wschodni"
  ]
  node [
    id 98
    label "zrobi&#263;"
  ]
  node [
    id 99
    label "u&#380;y&#263;"
  ]
  node [
    id 100
    label "uzyska&#263;"
  ]
  node [
    id 101
    label "utilize"
  ]
  node [
    id 102
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 103
    label "odst&#261;pi&#263;"
  ]
  node [
    id 104
    label "zacz&#261;&#263;"
  ]
  node [
    id 105
    label "wyj&#347;&#263;"
  ]
  node [
    id 106
    label "zrezygnowa&#263;"
  ]
  node [
    id 107
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 108
    label "happen"
  ]
  node [
    id 109
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 110
    label "perform"
  ]
  node [
    id 111
    label "nak&#322;oni&#263;"
  ]
  node [
    id 112
    label "appear"
  ]
  node [
    id 113
    label "Bangladesz"
  ]
  node [
    id 114
    label "jednostka_monetarna"
  ]
  node [
    id 115
    label "szczeg&#243;&#322;"
  ]
  node [
    id 116
    label "motyw"
  ]
  node [
    id 117
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 118
    label "state"
  ]
  node [
    id 119
    label "realia"
  ]
  node [
    id 120
    label "warunki"
  ]
  node [
    id 121
    label "get"
  ]
  node [
    id 122
    label "catch"
  ]
  node [
    id 123
    label "oddawa&#263;"
  ]
  node [
    id 124
    label "dostarcza&#263;"
  ]
  node [
    id 125
    label "osi&#261;ga&#263;"
  ]
  node [
    id 126
    label "doznawa&#263;"
  ]
  node [
    id 127
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 128
    label "orzeczenie"
  ]
  node [
    id 129
    label "order"
  ]
  node [
    id 130
    label "wydarzenie"
  ]
  node [
    id 131
    label "kara"
  ]
  node [
    id 132
    label "judgment"
  ]
  node [
    id 133
    label "sentencja"
  ]
  node [
    id 134
    label "Europejski_Trybuna&#322;_Praw_Cz&#322;owieka"
  ]
  node [
    id 135
    label "s&#261;d"
  ]
  node [
    id 136
    label "ustawowy"
  ]
  node [
    id 137
    label "konstytucyjnie"
  ]
  node [
    id 138
    label "powiedzie&#263;"
  ]
  node [
    id 139
    label "testify"
  ]
  node [
    id 140
    label "uzna&#263;"
  ]
  node [
    id 141
    label "oznajmi&#263;"
  ]
  node [
    id 142
    label "declare"
  ]
  node [
    id 143
    label "free"
  ]
  node [
    id 144
    label "wycina&#263;"
  ]
  node [
    id 145
    label "open"
  ]
  node [
    id 146
    label "otrzymywa&#263;"
  ]
  node [
    id 147
    label "arise"
  ]
  node [
    id 148
    label "kopiowa&#263;"
  ]
  node [
    id 149
    label "raise"
  ]
  node [
    id 150
    label "wch&#322;ania&#263;"
  ]
  node [
    id 151
    label "bra&#263;"
  ]
  node [
    id 152
    label "pr&#243;bka"
  ]
  node [
    id 153
    label "bilans_handlowy"
  ]
  node [
    id 154
    label "danina"
  ]
  node [
    id 155
    label "trybut"
  ]
  node [
    id 156
    label "tr&#243;jk&#261;t_Harbergera"
  ]
  node [
    id 157
    label "immoblizacja"
  ]
  node [
    id 158
    label "rzecz"
  ]
  node [
    id 159
    label "mienie"
  ]
  node [
    id 160
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 161
    label "zasta&#322;o&#347;&#263;"
  ]
  node [
    id 162
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 163
    label "kwota"
  ]
  node [
    id 164
    label "tam"
  ]
  node [
    id 165
    label "otw&#243;r"
  ]
  node [
    id 166
    label "z&#261;b"
  ]
  node [
    id 167
    label "spadek"
  ]
  node [
    id 168
    label "r&#243;&#380;nica"
  ]
  node [
    id 169
    label "pr&#243;chnica"
  ]
  node [
    id 170
    label "brak"
  ]
  node [
    id 171
    label "sprzeciw"
  ]
  node [
    id 172
    label "impart"
  ]
  node [
    id 173
    label "panna_na_wydaniu"
  ]
  node [
    id 174
    label "surrender"
  ]
  node [
    id 175
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 176
    label "train"
  ]
  node [
    id 177
    label "give"
  ]
  node [
    id 178
    label "wytwarza&#263;"
  ]
  node [
    id 179
    label "dawa&#263;"
  ]
  node [
    id 180
    label "zapach"
  ]
  node [
    id 181
    label "wprowadza&#263;"
  ]
  node [
    id 182
    label "ujawnia&#263;"
  ]
  node [
    id 183
    label "wydawnictwo"
  ]
  node [
    id 184
    label "powierza&#263;"
  ]
  node [
    id 185
    label "produkcja"
  ]
  node [
    id 186
    label "denuncjowa&#263;"
  ]
  node [
    id 187
    label "plon"
  ]
  node [
    id 188
    label "reszta"
  ]
  node [
    id 189
    label "robi&#263;"
  ]
  node [
    id 190
    label "placard"
  ]
  node [
    id 191
    label "tajemnica"
  ]
  node [
    id 192
    label "wiano"
  ]
  node [
    id 193
    label "kojarzy&#263;"
  ]
  node [
    id 194
    label "d&#378;wi&#281;k"
  ]
  node [
    id 195
    label "podawa&#263;"
  ]
  node [
    id 196
    label "troch&#281;"
  ]
  node [
    id 197
    label "istotnie"
  ]
  node [
    id 198
    label "znaczny"
  ]
  node [
    id 199
    label "znacz&#261;co"
  ]
  node [
    id 200
    label "dono&#347;ny"
  ]
  node [
    id 201
    label "zrobienie"
  ]
  node [
    id 202
    label "rozdzielenie"
  ]
  node [
    id 203
    label "diverseness"
  ]
  node [
    id 204
    label "rozpraszanie_si&#281;"
  ]
  node [
    id 205
    label "discrimination"
  ]
  node [
    id 206
    label "bogactwo"
  ]
  node [
    id 207
    label "multikulturalizm"
  ]
  node [
    id 208
    label "cecha"
  ]
  node [
    id 209
    label "rozproszenie_si&#281;"
  ]
  node [
    id 210
    label "rozr&#243;&#380;nienie"
  ]
  node [
    id 211
    label "eklektyk"
  ]
  node [
    id 212
    label "podzielenie"
  ]
  node [
    id 213
    label "differentiation"
  ]
  node [
    id 214
    label "nadanie"
  ]
  node [
    id 215
    label "tallness"
  ]
  node [
    id 216
    label "sum"
  ]
  node [
    id 217
    label "degree"
  ]
  node [
    id 218
    label "brzmienie"
  ]
  node [
    id 219
    label "altitude"
  ]
  node [
    id 220
    label "odcinek"
  ]
  node [
    id 221
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 222
    label "rozmiar"
  ]
  node [
    id 223
    label "cz&#281;stotliwo&#347;&#263;"
  ]
  node [
    id 224
    label "k&#261;t"
  ]
  node [
    id 225
    label "wielko&#347;&#263;"
  ]
  node [
    id 226
    label "korzy&#347;&#263;"
  ]
  node [
    id 227
    label "krzywa_Engla"
  ]
  node [
    id 228
    label "dochodowa_elastyczno&#347;&#263;_popytu"
  ]
  node [
    id 229
    label "wp&#322;yw"
  ]
  node [
    id 230
    label "income"
  ]
  node [
    id 231
    label "stopa_procentowa"
  ]
  node [
    id 232
    label "podtytu&#322;"
  ]
  node [
    id 233
    label "debit"
  ]
  node [
    id 234
    label "szata_graficzna"
  ]
  node [
    id 235
    label "elevation"
  ]
  node [
    id 236
    label "wyda&#263;"
  ]
  node [
    id 237
    label "nadtytu&#322;"
  ]
  node [
    id 238
    label "tytulatura"
  ]
  node [
    id 239
    label "nazwa"
  ]
  node [
    id 240
    label "redaktor"
  ]
  node [
    id 241
    label "druk"
  ]
  node [
    id 242
    label "mianowaniec"
  ]
  node [
    id 243
    label "poster"
  ]
  node [
    id 244
    label "publikacja"
  ]
  node [
    id 245
    label "warto&#347;ciowy"
  ]
  node [
    id 246
    label "du&#380;y"
  ]
  node [
    id 247
    label "wysoce"
  ]
  node [
    id 248
    label "daleki"
  ]
  node [
    id 249
    label "wysoko"
  ]
  node [
    id 250
    label "szczytnie"
  ]
  node [
    id 251
    label "wznios&#322;y"
  ]
  node [
    id 252
    label "wyrafinowany"
  ]
  node [
    id 253
    label "z_wysoka"
  ]
  node [
    id 254
    label "chwalebny"
  ]
  node [
    id 255
    label "uprzywilejowany"
  ]
  node [
    id 256
    label "niepo&#347;ledni"
  ]
  node [
    id 257
    label "odmawia&#263;"
  ]
  node [
    id 258
    label "wycofywa&#263;_si&#281;"
  ]
  node [
    id 259
    label "sk&#322;ada&#263;"
  ]
  node [
    id 260
    label "thank"
  ]
  node [
    id 261
    label "odwdzi&#281;cza&#263;_si&#281;"
  ]
  node [
    id 262
    label "wyra&#380;a&#263;"
  ]
  node [
    id 263
    label "etykieta"
  ]
  node [
    id 264
    label "w_chuj"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 12
  ]
  edge [
    source 9
    target 13
  ]
  edge [
    source 9
    target 41
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 15
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 28
  ]
  edge [
    source 12
    target 29
  ]
  edge [
    source 12
    target 30
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 102
  ]
  edge [
    source 13
    target 103
  ]
  edge [
    source 13
    target 104
  ]
  edge [
    source 13
    target 105
  ]
  edge [
    source 13
    target 106
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 113
  ]
  edge [
    source 14
    target 114
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 115
  ]
  edge [
    source 15
    target 116
  ]
  edge [
    source 15
    target 117
  ]
  edge [
    source 15
    target 118
  ]
  edge [
    source 15
    target 119
  ]
  edge [
    source 15
    target 120
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 121
  ]
  edge [
    source 17
    target 122
  ]
  edge [
    source 17
    target 123
  ]
  edge [
    source 17
    target 124
  ]
  edge [
    source 17
    target 125
  ]
  edge [
    source 17
    target 126
  ]
  edge [
    source 17
    target 127
  ]
  edge [
    source 17
    target 20
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 35
  ]
  edge [
    source 18
    target 36
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 128
  ]
  edge [
    source 19
    target 129
  ]
  edge [
    source 19
    target 130
  ]
  edge [
    source 19
    target 131
  ]
  edge [
    source 19
    target 132
  ]
  edge [
    source 19
    target 133
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 22
  ]
  edge [
    source 20
    target 134
  ]
  edge [
    source 20
    target 135
  ]
  edge [
    source 21
    target 136
  ]
  edge [
    source 21
    target 137
  ]
  edge [
    source 21
    target 41
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 138
  ]
  edge [
    source 22
    target 139
  ]
  edge [
    source 22
    target 140
  ]
  edge [
    source 22
    target 141
  ]
  edge [
    source 22
    target 142
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 143
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 144
  ]
  edge [
    source 24
    target 145
  ]
  edge [
    source 24
    target 146
  ]
  edge [
    source 24
    target 147
  ]
  edge [
    source 24
    target 148
  ]
  edge [
    source 24
    target 149
  ]
  edge [
    source 24
    target 150
  ]
  edge [
    source 24
    target 151
  ]
  edge [
    source 24
    target 152
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 153
  ]
  edge [
    source 25
    target 27
  ]
  edge [
    source 25
    target 154
  ]
  edge [
    source 25
    target 155
  ]
  edge [
    source 25
    target 156
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 69
  ]
  edge [
    source 26
    target 157
  ]
  edge [
    source 26
    target 158
  ]
  edge [
    source 26
    target 159
  ]
  edge [
    source 26
    target 160
  ]
  edge [
    source 26
    target 161
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 162
  ]
  edge [
    source 27
    target 163
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 164
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 165
  ]
  edge [
    source 32
    target 166
  ]
  edge [
    source 32
    target 167
  ]
  edge [
    source 32
    target 168
  ]
  edge [
    source 32
    target 169
  ]
  edge [
    source 32
    target 170
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 171
  ]
  edge [
    source 35
    target 172
  ]
  edge [
    source 35
    target 173
  ]
  edge [
    source 35
    target 174
  ]
  edge [
    source 35
    target 175
  ]
  edge [
    source 35
    target 176
  ]
  edge [
    source 35
    target 177
  ]
  edge [
    source 35
    target 178
  ]
  edge [
    source 35
    target 179
  ]
  edge [
    source 35
    target 180
  ]
  edge [
    source 35
    target 181
  ]
  edge [
    source 35
    target 182
  ]
  edge [
    source 35
    target 183
  ]
  edge [
    source 35
    target 184
  ]
  edge [
    source 35
    target 185
  ]
  edge [
    source 35
    target 186
  ]
  edge [
    source 35
    target 72
  ]
  edge [
    source 35
    target 187
  ]
  edge [
    source 35
    target 188
  ]
  edge [
    source 35
    target 189
  ]
  edge [
    source 35
    target 190
  ]
  edge [
    source 35
    target 191
  ]
  edge [
    source 35
    target 192
  ]
  edge [
    source 35
    target 193
  ]
  edge [
    source 35
    target 194
  ]
  edge [
    source 35
    target 195
  ]
  edge [
    source 35
    target 42
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 196
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 197
  ]
  edge [
    source 37
    target 198
  ]
  edge [
    source 37
    target 199
  ]
  edge [
    source 37
    target 200
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 201
  ]
  edge [
    source 38
    target 202
  ]
  edge [
    source 38
    target 203
  ]
  edge [
    source 38
    target 204
  ]
  edge [
    source 38
    target 205
  ]
  edge [
    source 38
    target 206
  ]
  edge [
    source 38
    target 207
  ]
  edge [
    source 38
    target 208
  ]
  edge [
    source 38
    target 209
  ]
  edge [
    source 38
    target 210
  ]
  edge [
    source 38
    target 211
  ]
  edge [
    source 38
    target 212
  ]
  edge [
    source 38
    target 213
  ]
  edge [
    source 38
    target 214
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 215
  ]
  edge [
    source 39
    target 216
  ]
  edge [
    source 39
    target 217
  ]
  edge [
    source 39
    target 218
  ]
  edge [
    source 39
    target 219
  ]
  edge [
    source 39
    target 220
  ]
  edge [
    source 39
    target 221
  ]
  edge [
    source 39
    target 222
  ]
  edge [
    source 39
    target 223
  ]
  edge [
    source 39
    target 224
  ]
  edge [
    source 39
    target 225
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 226
  ]
  edge [
    source 41
    target 227
  ]
  edge [
    source 41
    target 228
  ]
  edge [
    source 41
    target 229
  ]
  edge [
    source 41
    target 230
  ]
  edge [
    source 41
    target 231
  ]
  edge [
    source 42
    target 232
  ]
  edge [
    source 42
    target 233
  ]
  edge [
    source 42
    target 234
  ]
  edge [
    source 42
    target 235
  ]
  edge [
    source 42
    target 236
  ]
  edge [
    source 42
    target 237
  ]
  edge [
    source 42
    target 238
  ]
  edge [
    source 42
    target 239
  ]
  edge [
    source 42
    target 240
  ]
  edge [
    source 42
    target 241
  ]
  edge [
    source 42
    target 242
  ]
  edge [
    source 42
    target 243
  ]
  edge [
    source 42
    target 244
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 245
  ]
  edge [
    source 44
    target 246
  ]
  edge [
    source 44
    target 247
  ]
  edge [
    source 44
    target 248
  ]
  edge [
    source 44
    target 198
  ]
  edge [
    source 44
    target 249
  ]
  edge [
    source 44
    target 250
  ]
  edge [
    source 44
    target 251
  ]
  edge [
    source 44
    target 252
  ]
  edge [
    source 44
    target 253
  ]
  edge [
    source 44
    target 254
  ]
  edge [
    source 44
    target 255
  ]
  edge [
    source 44
    target 256
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 257
  ]
  edge [
    source 45
    target 258
  ]
  edge [
    source 45
    target 259
  ]
  edge [
    source 45
    target 260
  ]
  edge [
    source 45
    target 261
  ]
  edge [
    source 45
    target 262
  ]
  edge [
    source 45
    target 263
  ]
  edge [
    source 46
    target 264
  ]
]
