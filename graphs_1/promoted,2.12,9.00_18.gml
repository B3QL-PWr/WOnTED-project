graph [
  maxDegree 9
  minDegree 1
  meanDegree 1.9375
  density 0.0625
  graphCliqueNumber 2
  node [
    id 0
    label "airbus"
    origin "text"
  ]
  node [
    id 1
    label "helicopters"
    origin "text"
  ]
  node [
    id 2
    label "wycofywa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "si&#281;"
    origin "text"
  ]
  node [
    id 4
    label "przetarg"
    origin "text"
  ]
  node [
    id 5
    label "&#347;mig&#322;owiec"
    origin "text"
  ]
  node [
    id 6
    label "dla"
    origin "text"
  ]
  node [
    id 7
    label "marynarka"
    origin "text"
  ]
  node [
    id 8
    label "wojenny"
    origin "text"
  ]
  node [
    id 9
    label "samolot_pasa&#380;erski"
  ]
  node [
    id 10
    label "seclude"
  ]
  node [
    id 11
    label "uniewa&#380;nia&#263;"
  ]
  node [
    id 12
    label "konkurs"
  ]
  node [
    id 13
    label "licytacja"
  ]
  node [
    id 14
    label "przybitka"
  ]
  node [
    id 15
    label "auction"
  ]
  node [
    id 16
    label "sprzeda&#380;"
  ]
  node [
    id 17
    label "sale"
  ]
  node [
    id 18
    label "sp&#243;r"
  ]
  node [
    id 19
    label "wirop&#322;at"
  ]
  node [
    id 20
    label "p&#322;at_no&#347;ny"
  ]
  node [
    id 21
    label "&#347;mig&#322;o"
  ]
  node [
    id 22
    label "g&#243;ra"
  ]
  node [
    id 23
    label "r&#281;kaw"
  ]
  node [
    id 24
    label "United_States_Navy"
  ]
  node [
    id 25
    label "zbi&#243;r"
  ]
  node [
    id 26
    label "guzik"
  ]
  node [
    id 27
    label "wojsko"
  ]
  node [
    id 28
    label "klapa"
  ]
  node [
    id 29
    label "bojowo"
  ]
  node [
    id 30
    label "typowy"
  ]
  node [
    id 31
    label "Helicopters"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 22
  ]
  edge [
    source 7
    target 23
  ]
  edge [
    source 7
    target 24
  ]
  edge [
    source 7
    target 25
  ]
  edge [
    source 7
    target 26
  ]
  edge [
    source 7
    target 27
  ]
  edge [
    source 7
    target 28
  ]
  edge [
    source 8
    target 29
  ]
  edge [
    source 8
    target 30
  ]
]
