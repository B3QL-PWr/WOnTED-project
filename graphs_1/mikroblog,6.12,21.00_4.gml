graph [
  maxDegree 5
  minDegree 1
  meanDegree 1.6666666666666667
  density 0.3333333333333333
  graphCliqueNumber 2
  node [
    id 0
    label "si&#281;"
    origin "text"
  ]
  node [
    id 1
    label "wczoraj"
    origin "text"
  ]
  node [
    id 2
    label "odjeba&#263;"
    origin "text"
  ]
  node [
    id 3
    label "doba"
  ]
  node [
    id 4
    label "dawno"
  ]
  node [
    id 5
    label "niedawno"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
]
