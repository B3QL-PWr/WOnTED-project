graph [
  maxDegree 21
  minDegree 1
  meanDegree 1.9333333333333333
  density 0.06666666666666667
  graphCliqueNumber 2
  node [
    id 0
    label "dzienia"
    origin "text"
  ]
  node [
    id 1
    label "dobry"
    origin "text"
  ]
  node [
    id 2
    label "xxx"
    origin "text"
  ]
  node [
    id 3
    label "s&#322;ucha&#263;"
    origin "text"
  ]
  node [
    id 4
    label "pomy&#347;lny"
  ]
  node [
    id 5
    label "skuteczny"
  ]
  node [
    id 6
    label "moralny"
  ]
  node [
    id 7
    label "korzystny"
  ]
  node [
    id 8
    label "odpowiedni"
  ]
  node [
    id 9
    label "zwrot"
  ]
  node [
    id 10
    label "dobrze"
  ]
  node [
    id 11
    label "pozytywny"
  ]
  node [
    id 12
    label "grzeczny"
  ]
  node [
    id 13
    label "powitanie"
  ]
  node [
    id 14
    label "mi&#322;y"
  ]
  node [
    id 15
    label "dobroczynny"
  ]
  node [
    id 16
    label "pos&#322;uszny"
  ]
  node [
    id 17
    label "ca&#322;y"
  ]
  node [
    id 18
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 19
    label "czw&#243;rka"
  ]
  node [
    id 20
    label "spokojny"
  ]
  node [
    id 21
    label "&#347;mieszny"
  ]
  node [
    id 22
    label "drogi"
  ]
  node [
    id 23
    label "continue"
  ]
  node [
    id 24
    label "podporz&#261;dkowywa&#263;_si&#281;"
  ]
  node [
    id 25
    label "lubi&#263;"
  ]
  node [
    id 26
    label "odbiera&#263;"
  ]
  node [
    id 27
    label "wybiera&#263;"
  ]
  node [
    id 28
    label "odtwarza&#263;"
  ]
  node [
    id 29
    label "nat&#281;&#380;a&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
]
