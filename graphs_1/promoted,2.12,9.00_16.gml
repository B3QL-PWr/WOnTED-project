graph [
  maxDegree 217
  minDegree 1
  meanDegree 2.0193548387096776
  density 0.006535128927863034
  graphCliqueNumber 3
  node [
    id 0
    label "pierwsza"
    origin "text"
  ]
  node [
    id 1
    label "pr&#243;ba"
    origin "text"
  ]
  node [
    id 2
    label "masowy"
    origin "text"
  ]
  node [
    id 3
    label "przew&#243;z"
    origin "text"
  ]
  node [
    id 4
    label "towar"
    origin "text"
  ]
  node [
    id 5
    label "trasa"
    origin "text"
  ]
  node [
    id 6
    label "drogowy"
    origin "text"
  ]
  node [
    id 7
    label "chiny"
    origin "text"
  ]
  node [
    id 8
    label "polska"
    origin "text"
  ]
  node [
    id 9
    label "ucieszy&#263;"
    origin "text"
  ]
  node [
    id 10
    label "organizator"
    origin "text"
  ]
  node [
    id 11
    label "test"
    origin "text"
  ]
  node [
    id 12
    label "pa&#324;stwo"
    origin "text"
  ]
  node [
    id 13
    label "&#347;rodek"
    origin "text"
  ]
  node [
    id 14
    label "godzina"
  ]
  node [
    id 15
    label "usi&#322;owanie"
  ]
  node [
    id 16
    label "pobiera&#263;"
  ]
  node [
    id 17
    label "spotkanie"
  ]
  node [
    id 18
    label "analiza_chemiczna"
  ]
  node [
    id 19
    label "znak"
  ]
  node [
    id 20
    label "item"
  ]
  node [
    id 21
    label "ilo&#347;&#263;"
  ]
  node [
    id 22
    label "effort"
  ]
  node [
    id 23
    label "czynno&#347;&#263;"
  ]
  node [
    id 24
    label "metal_szlachetny"
  ]
  node [
    id 25
    label "pobranie"
  ]
  node [
    id 26
    label "pobieranie"
  ]
  node [
    id 27
    label "sytuacja"
  ]
  node [
    id 28
    label "do&#347;wiadczenie"
  ]
  node [
    id 29
    label "probiernictwo"
  ]
  node [
    id 30
    label "zbi&#243;r"
  ]
  node [
    id 31
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 32
    label "pobra&#263;"
  ]
  node [
    id 33
    label "rezultat"
  ]
  node [
    id 34
    label "niski"
  ]
  node [
    id 35
    label "popularny"
  ]
  node [
    id 36
    label "masowo"
  ]
  node [
    id 37
    label "seryjny"
  ]
  node [
    id 38
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 39
    label "za&#322;adunek"
  ]
  node [
    id 40
    label "roz&#322;adunek"
  ]
  node [
    id 41
    label "jednoszynowy"
  ]
  node [
    id 42
    label "cedu&#322;a"
  ]
  node [
    id 43
    label "us&#322;uga"
  ]
  node [
    id 44
    label "prze&#322;adunek"
  ]
  node [
    id 45
    label "komunikacja"
  ]
  node [
    id 46
    label "cz&#322;owiek"
  ]
  node [
    id 47
    label "obr&#243;t_handlowy"
  ]
  node [
    id 48
    label "rzuca&#263;"
  ]
  node [
    id 49
    label "tkanina"
  ]
  node [
    id 50
    label "rzucenie"
  ]
  node [
    id 51
    label "tandeta"
  ]
  node [
    id 52
    label "naszprycowa&#263;"
  ]
  node [
    id 53
    label "&#322;&#243;dzki"
  ]
  node [
    id 54
    label "&#347;rodek_psychoaktywny"
  ]
  node [
    id 55
    label "naszprycowanie"
  ]
  node [
    id 56
    label "za&#322;adownia"
  ]
  node [
    id 57
    label "szprycowa&#263;"
  ]
  node [
    id 58
    label "rzuci&#263;"
  ]
  node [
    id 59
    label "wyr&#243;b"
  ]
  node [
    id 60
    label "szprycowanie"
  ]
  node [
    id 61
    label "asortyment"
  ]
  node [
    id 62
    label "poci&#261;gaj&#261;cy"
  ]
  node [
    id 63
    label "rzucanie"
  ]
  node [
    id 64
    label "narkobiznes"
  ]
  node [
    id 65
    label "metka"
  ]
  node [
    id 66
    label "marszrutyzacja"
  ]
  node [
    id 67
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 68
    label "w&#281;ze&#322;"
  ]
  node [
    id 69
    label "przebieg"
  ]
  node [
    id 70
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 71
    label "droga"
  ]
  node [
    id 72
    label "infrastruktura"
  ]
  node [
    id 73
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 74
    label "podbieg"
  ]
  node [
    id 75
    label "dro&#380;ny"
  ]
  node [
    id 76
    label "wzbudzi&#263;"
  ]
  node [
    id 77
    label "obradowa&#263;"
  ]
  node [
    id 78
    label "delight"
  ]
  node [
    id 79
    label "spiritus_movens"
  ]
  node [
    id 80
    label "realizator"
  ]
  node [
    id 81
    label "arkusz"
  ]
  node [
    id 82
    label "sprawdzian"
  ]
  node [
    id 83
    label "quiz"
  ]
  node [
    id 84
    label "przechodzi&#263;"
  ]
  node [
    id 85
    label "przechodzenie"
  ]
  node [
    id 86
    label "badanie"
  ]
  node [
    id 87
    label "narz&#281;dzie"
  ]
  node [
    id 88
    label "Rwanda"
  ]
  node [
    id 89
    label "Filipiny"
  ]
  node [
    id 90
    label "Monako"
  ]
  node [
    id 91
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 92
    label "Korea"
  ]
  node [
    id 93
    label "Czarnog&#243;ra"
  ]
  node [
    id 94
    label "Ghana"
  ]
  node [
    id 95
    label "Malawi"
  ]
  node [
    id 96
    label "Indonezja"
  ]
  node [
    id 97
    label "Bu&#322;garia"
  ]
  node [
    id 98
    label "Nauru"
  ]
  node [
    id 99
    label "Kenia"
  ]
  node [
    id 100
    label "Kambod&#380;a"
  ]
  node [
    id 101
    label "Mali"
  ]
  node [
    id 102
    label "Austria"
  ]
  node [
    id 103
    label "interior"
  ]
  node [
    id 104
    label "Armenia"
  ]
  node [
    id 105
    label "Fid&#380;i"
  ]
  node [
    id 106
    label "Tuwalu"
  ]
  node [
    id 107
    label "Etiopia"
  ]
  node [
    id 108
    label "Malezja"
  ]
  node [
    id 109
    label "Malta"
  ]
  node [
    id 110
    label "Tad&#380;ykistan"
  ]
  node [
    id 111
    label "Grenada"
  ]
  node [
    id 112
    label "Wehrlen"
  ]
  node [
    id 113
    label "para"
  ]
  node [
    id 114
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 115
    label "Rumunia"
  ]
  node [
    id 116
    label "Maroko"
  ]
  node [
    id 117
    label "Bhutan"
  ]
  node [
    id 118
    label "S&#322;owacja"
  ]
  node [
    id 119
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 120
    label "Seszele"
  ]
  node [
    id 121
    label "Kuwejt"
  ]
  node [
    id 122
    label "Arabia_Saudyjska"
  ]
  node [
    id 123
    label "Kanada"
  ]
  node [
    id 124
    label "Ekwador"
  ]
  node [
    id 125
    label "Japonia"
  ]
  node [
    id 126
    label "ziemia"
  ]
  node [
    id 127
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 128
    label "Hiszpania"
  ]
  node [
    id 129
    label "Wyspy_Marshalla"
  ]
  node [
    id 130
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 131
    label "D&#380;ibuti"
  ]
  node [
    id 132
    label "Botswana"
  ]
  node [
    id 133
    label "grupa"
  ]
  node [
    id 134
    label "Wietnam"
  ]
  node [
    id 135
    label "Egipt"
  ]
  node [
    id 136
    label "Burkina_Faso"
  ]
  node [
    id 137
    label "Niemcy"
  ]
  node [
    id 138
    label "Khitai"
  ]
  node [
    id 139
    label "Macedonia"
  ]
  node [
    id 140
    label "Albania"
  ]
  node [
    id 141
    label "Madagaskar"
  ]
  node [
    id 142
    label "Bahrajn"
  ]
  node [
    id 143
    label "Jemen"
  ]
  node [
    id 144
    label "Lesoto"
  ]
  node [
    id 145
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 146
    label "Samoa"
  ]
  node [
    id 147
    label "Andora"
  ]
  node [
    id 148
    label "Chiny"
  ]
  node [
    id 149
    label "Cypr"
  ]
  node [
    id 150
    label "Wielka_Brytania"
  ]
  node [
    id 151
    label "Ukraina"
  ]
  node [
    id 152
    label "Paragwaj"
  ]
  node [
    id 153
    label "Trynidad_i_Tobago"
  ]
  node [
    id 154
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 155
    label "Libia"
  ]
  node [
    id 156
    label "Surinam"
  ]
  node [
    id 157
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 158
    label "Nigeria"
  ]
  node [
    id 159
    label "Australia"
  ]
  node [
    id 160
    label "Honduras"
  ]
  node [
    id 161
    label "Peru"
  ]
  node [
    id 162
    label "USA"
  ]
  node [
    id 163
    label "Bangladesz"
  ]
  node [
    id 164
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 165
    label "Kazachstan"
  ]
  node [
    id 166
    label "holoarktyka"
  ]
  node [
    id 167
    label "Nepal"
  ]
  node [
    id 168
    label "Sudan"
  ]
  node [
    id 169
    label "Irak"
  ]
  node [
    id 170
    label "San_Marino"
  ]
  node [
    id 171
    label "Burundi"
  ]
  node [
    id 172
    label "Dominikana"
  ]
  node [
    id 173
    label "Komory"
  ]
  node [
    id 174
    label "granica_pa&#324;stwa"
  ]
  node [
    id 175
    label "Gwatemala"
  ]
  node [
    id 176
    label "Antarktis"
  ]
  node [
    id 177
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 178
    label "Brunei"
  ]
  node [
    id 179
    label "Iran"
  ]
  node [
    id 180
    label "Zimbabwe"
  ]
  node [
    id 181
    label "Namibia"
  ]
  node [
    id 182
    label "Meksyk"
  ]
  node [
    id 183
    label "Kamerun"
  ]
  node [
    id 184
    label "zwrot"
  ]
  node [
    id 185
    label "Somalia"
  ]
  node [
    id 186
    label "Angola"
  ]
  node [
    id 187
    label "Gabon"
  ]
  node [
    id 188
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 189
    label "Nowa_Zelandia"
  ]
  node [
    id 190
    label "Mozambik"
  ]
  node [
    id 191
    label "Tunezja"
  ]
  node [
    id 192
    label "Tajwan"
  ]
  node [
    id 193
    label "Liban"
  ]
  node [
    id 194
    label "Jordania"
  ]
  node [
    id 195
    label "Tonga"
  ]
  node [
    id 196
    label "Czad"
  ]
  node [
    id 197
    label "Gwinea"
  ]
  node [
    id 198
    label "Liberia"
  ]
  node [
    id 199
    label "Belize"
  ]
  node [
    id 200
    label "Benin"
  ]
  node [
    id 201
    label "&#321;otwa"
  ]
  node [
    id 202
    label "Syria"
  ]
  node [
    id 203
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 204
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 205
    label "Dominika"
  ]
  node [
    id 206
    label "Antigua_i_Barbuda"
  ]
  node [
    id 207
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 208
    label "Hanower"
  ]
  node [
    id 209
    label "partia"
  ]
  node [
    id 210
    label "Afganistan"
  ]
  node [
    id 211
    label "W&#322;ochy"
  ]
  node [
    id 212
    label "Kiribati"
  ]
  node [
    id 213
    label "Szwajcaria"
  ]
  node [
    id 214
    label "Chorwacja"
  ]
  node [
    id 215
    label "Sahara_Zachodnia"
  ]
  node [
    id 216
    label "Tajlandia"
  ]
  node [
    id 217
    label "Salwador"
  ]
  node [
    id 218
    label "Bahamy"
  ]
  node [
    id 219
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 220
    label "S&#322;owenia"
  ]
  node [
    id 221
    label "Gambia"
  ]
  node [
    id 222
    label "Urugwaj"
  ]
  node [
    id 223
    label "Zair"
  ]
  node [
    id 224
    label "Erytrea"
  ]
  node [
    id 225
    label "Rosja"
  ]
  node [
    id 226
    label "Mauritius"
  ]
  node [
    id 227
    label "Niger"
  ]
  node [
    id 228
    label "Uganda"
  ]
  node [
    id 229
    label "Turkmenistan"
  ]
  node [
    id 230
    label "Turcja"
  ]
  node [
    id 231
    label "Irlandia"
  ]
  node [
    id 232
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 233
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 234
    label "Gwinea_Bissau"
  ]
  node [
    id 235
    label "Belgia"
  ]
  node [
    id 236
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 237
    label "Palau"
  ]
  node [
    id 238
    label "Barbados"
  ]
  node [
    id 239
    label "Wenezuela"
  ]
  node [
    id 240
    label "W&#281;gry"
  ]
  node [
    id 241
    label "Chile"
  ]
  node [
    id 242
    label "Argentyna"
  ]
  node [
    id 243
    label "Kolumbia"
  ]
  node [
    id 244
    label "Sierra_Leone"
  ]
  node [
    id 245
    label "Azerbejd&#380;an"
  ]
  node [
    id 246
    label "Kongo"
  ]
  node [
    id 247
    label "Pakistan"
  ]
  node [
    id 248
    label "Liechtenstein"
  ]
  node [
    id 249
    label "Nikaragua"
  ]
  node [
    id 250
    label "Senegal"
  ]
  node [
    id 251
    label "Indie"
  ]
  node [
    id 252
    label "Suazi"
  ]
  node [
    id 253
    label "Polska"
  ]
  node [
    id 254
    label "Algieria"
  ]
  node [
    id 255
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 256
    label "terytorium"
  ]
  node [
    id 257
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 258
    label "Jamajka"
  ]
  node [
    id 259
    label "Kostaryka"
  ]
  node [
    id 260
    label "Timor_Wschodni"
  ]
  node [
    id 261
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 262
    label "Kuba"
  ]
  node [
    id 263
    label "Mauretania"
  ]
  node [
    id 264
    label "Portoryko"
  ]
  node [
    id 265
    label "Brazylia"
  ]
  node [
    id 266
    label "Mo&#322;dawia"
  ]
  node [
    id 267
    label "organizacja"
  ]
  node [
    id 268
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 269
    label "Litwa"
  ]
  node [
    id 270
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 271
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 272
    label "Izrael"
  ]
  node [
    id 273
    label "Grecja"
  ]
  node [
    id 274
    label "Kirgistan"
  ]
  node [
    id 275
    label "Holandia"
  ]
  node [
    id 276
    label "Sri_Lanka"
  ]
  node [
    id 277
    label "Katar"
  ]
  node [
    id 278
    label "Mikronezja"
  ]
  node [
    id 279
    label "Laos"
  ]
  node [
    id 280
    label "Mongolia"
  ]
  node [
    id 281
    label "Malediwy"
  ]
  node [
    id 282
    label "Zambia"
  ]
  node [
    id 283
    label "Tanzania"
  ]
  node [
    id 284
    label "Gujana"
  ]
  node [
    id 285
    label "Uzbekistan"
  ]
  node [
    id 286
    label "Panama"
  ]
  node [
    id 287
    label "Czechy"
  ]
  node [
    id 288
    label "Gruzja"
  ]
  node [
    id 289
    label "Serbia"
  ]
  node [
    id 290
    label "Francja"
  ]
  node [
    id 291
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 292
    label "Togo"
  ]
  node [
    id 293
    label "Estonia"
  ]
  node [
    id 294
    label "Boliwia"
  ]
  node [
    id 295
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 296
    label "Oman"
  ]
  node [
    id 297
    label "Wyspy_Salomona"
  ]
  node [
    id 298
    label "Haiti"
  ]
  node [
    id 299
    label "Luksemburg"
  ]
  node [
    id 300
    label "Portugalia"
  ]
  node [
    id 301
    label "Birma"
  ]
  node [
    id 302
    label "Rodezja"
  ]
  node [
    id 303
    label "miejsce"
  ]
  node [
    id 304
    label "czas"
  ]
  node [
    id 305
    label "abstrakcja"
  ]
  node [
    id 306
    label "punkt"
  ]
  node [
    id 307
    label "substancja"
  ]
  node [
    id 308
    label "spos&#243;b"
  ]
  node [
    id 309
    label "chemikalia"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 28
  ]
  edge [
    source 11
    target 81
  ]
  edge [
    source 11
    target 82
  ]
  edge [
    source 11
    target 83
  ]
  edge [
    source 11
    target 84
  ]
  edge [
    source 11
    target 85
  ]
  edge [
    source 11
    target 86
  ]
  edge [
    source 11
    target 87
  ]
  edge [
    source 11
    target 27
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 12
    target 89
  ]
  edge [
    source 12
    target 90
  ]
  edge [
    source 12
    target 91
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 12
    target 94
  ]
  edge [
    source 12
    target 95
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 12
    target 107
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 12
    target 109
  ]
  edge [
    source 12
    target 110
  ]
  edge [
    source 12
    target 111
  ]
  edge [
    source 12
    target 112
  ]
  edge [
    source 12
    target 113
  ]
  edge [
    source 12
    target 114
  ]
  edge [
    source 12
    target 115
  ]
  edge [
    source 12
    target 116
  ]
  edge [
    source 12
    target 117
  ]
  edge [
    source 12
    target 118
  ]
  edge [
    source 12
    target 119
  ]
  edge [
    source 12
    target 120
  ]
  edge [
    source 12
    target 121
  ]
  edge [
    source 12
    target 122
  ]
  edge [
    source 12
    target 123
  ]
  edge [
    source 12
    target 124
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 12
    target 131
  ]
  edge [
    source 12
    target 132
  ]
  edge [
    source 12
    target 133
  ]
  edge [
    source 12
    target 134
  ]
  edge [
    source 12
    target 135
  ]
  edge [
    source 12
    target 136
  ]
  edge [
    source 12
    target 137
  ]
  edge [
    source 12
    target 138
  ]
  edge [
    source 12
    target 139
  ]
  edge [
    source 12
    target 140
  ]
  edge [
    source 12
    target 141
  ]
  edge [
    source 12
    target 142
  ]
  edge [
    source 12
    target 143
  ]
  edge [
    source 12
    target 144
  ]
  edge [
    source 12
    target 145
  ]
  edge [
    source 12
    target 146
  ]
  edge [
    source 12
    target 147
  ]
  edge [
    source 12
    target 148
  ]
  edge [
    source 12
    target 149
  ]
  edge [
    source 12
    target 150
  ]
  edge [
    source 12
    target 151
  ]
  edge [
    source 12
    target 152
  ]
  edge [
    source 12
    target 153
  ]
  edge [
    source 12
    target 154
  ]
  edge [
    source 12
    target 155
  ]
  edge [
    source 12
    target 156
  ]
  edge [
    source 12
    target 157
  ]
  edge [
    source 12
    target 158
  ]
  edge [
    source 12
    target 159
  ]
  edge [
    source 12
    target 160
  ]
  edge [
    source 12
    target 161
  ]
  edge [
    source 12
    target 162
  ]
  edge [
    source 12
    target 163
  ]
  edge [
    source 12
    target 164
  ]
  edge [
    source 12
    target 165
  ]
  edge [
    source 12
    target 166
  ]
  edge [
    source 12
    target 167
  ]
  edge [
    source 12
    target 168
  ]
  edge [
    source 12
    target 169
  ]
  edge [
    source 12
    target 170
  ]
  edge [
    source 12
    target 171
  ]
  edge [
    source 12
    target 172
  ]
  edge [
    source 12
    target 173
  ]
  edge [
    source 12
    target 174
  ]
  edge [
    source 12
    target 175
  ]
  edge [
    source 12
    target 176
  ]
  edge [
    source 12
    target 177
  ]
  edge [
    source 12
    target 178
  ]
  edge [
    source 12
    target 179
  ]
  edge [
    source 12
    target 180
  ]
  edge [
    source 12
    target 181
  ]
  edge [
    source 12
    target 182
  ]
  edge [
    source 12
    target 183
  ]
  edge [
    source 12
    target 184
  ]
  edge [
    source 12
    target 185
  ]
  edge [
    source 12
    target 186
  ]
  edge [
    source 12
    target 187
  ]
  edge [
    source 12
    target 188
  ]
  edge [
    source 12
    target 189
  ]
  edge [
    source 12
    target 190
  ]
  edge [
    source 12
    target 191
  ]
  edge [
    source 12
    target 192
  ]
  edge [
    source 12
    target 193
  ]
  edge [
    source 12
    target 194
  ]
  edge [
    source 12
    target 195
  ]
  edge [
    source 12
    target 196
  ]
  edge [
    source 12
    target 197
  ]
  edge [
    source 12
    target 198
  ]
  edge [
    source 12
    target 199
  ]
  edge [
    source 12
    target 200
  ]
  edge [
    source 12
    target 201
  ]
  edge [
    source 12
    target 202
  ]
  edge [
    source 12
    target 203
  ]
  edge [
    source 12
    target 204
  ]
  edge [
    source 12
    target 205
  ]
  edge [
    source 12
    target 206
  ]
  edge [
    source 12
    target 207
  ]
  edge [
    source 12
    target 208
  ]
  edge [
    source 12
    target 209
  ]
  edge [
    source 12
    target 210
  ]
  edge [
    source 12
    target 211
  ]
  edge [
    source 12
    target 212
  ]
  edge [
    source 12
    target 213
  ]
  edge [
    source 12
    target 214
  ]
  edge [
    source 12
    target 215
  ]
  edge [
    source 12
    target 216
  ]
  edge [
    source 12
    target 217
  ]
  edge [
    source 12
    target 218
  ]
  edge [
    source 12
    target 219
  ]
  edge [
    source 12
    target 220
  ]
  edge [
    source 12
    target 221
  ]
  edge [
    source 12
    target 222
  ]
  edge [
    source 12
    target 223
  ]
  edge [
    source 12
    target 224
  ]
  edge [
    source 12
    target 225
  ]
  edge [
    source 12
    target 226
  ]
  edge [
    source 12
    target 227
  ]
  edge [
    source 12
    target 228
  ]
  edge [
    source 12
    target 229
  ]
  edge [
    source 12
    target 230
  ]
  edge [
    source 12
    target 231
  ]
  edge [
    source 12
    target 232
  ]
  edge [
    source 12
    target 233
  ]
  edge [
    source 12
    target 234
  ]
  edge [
    source 12
    target 235
  ]
  edge [
    source 12
    target 236
  ]
  edge [
    source 12
    target 237
  ]
  edge [
    source 12
    target 238
  ]
  edge [
    source 12
    target 239
  ]
  edge [
    source 12
    target 240
  ]
  edge [
    source 12
    target 241
  ]
  edge [
    source 12
    target 242
  ]
  edge [
    source 12
    target 243
  ]
  edge [
    source 12
    target 244
  ]
  edge [
    source 12
    target 245
  ]
  edge [
    source 12
    target 246
  ]
  edge [
    source 12
    target 247
  ]
  edge [
    source 12
    target 248
  ]
  edge [
    source 12
    target 249
  ]
  edge [
    source 12
    target 250
  ]
  edge [
    source 12
    target 251
  ]
  edge [
    source 12
    target 252
  ]
  edge [
    source 12
    target 253
  ]
  edge [
    source 12
    target 254
  ]
  edge [
    source 12
    target 255
  ]
  edge [
    source 12
    target 256
  ]
  edge [
    source 12
    target 257
  ]
  edge [
    source 12
    target 258
  ]
  edge [
    source 12
    target 259
  ]
  edge [
    source 12
    target 260
  ]
  edge [
    source 12
    target 261
  ]
  edge [
    source 12
    target 262
  ]
  edge [
    source 12
    target 263
  ]
  edge [
    source 12
    target 264
  ]
  edge [
    source 12
    target 265
  ]
  edge [
    source 12
    target 266
  ]
  edge [
    source 12
    target 267
  ]
  edge [
    source 12
    target 268
  ]
  edge [
    source 12
    target 269
  ]
  edge [
    source 12
    target 270
  ]
  edge [
    source 12
    target 271
  ]
  edge [
    source 12
    target 272
  ]
  edge [
    source 12
    target 273
  ]
  edge [
    source 12
    target 274
  ]
  edge [
    source 12
    target 275
  ]
  edge [
    source 12
    target 276
  ]
  edge [
    source 12
    target 277
  ]
  edge [
    source 12
    target 278
  ]
  edge [
    source 12
    target 279
  ]
  edge [
    source 12
    target 280
  ]
  edge [
    source 12
    target 281
  ]
  edge [
    source 12
    target 282
  ]
  edge [
    source 12
    target 283
  ]
  edge [
    source 12
    target 284
  ]
  edge [
    source 12
    target 285
  ]
  edge [
    source 12
    target 286
  ]
  edge [
    source 12
    target 287
  ]
  edge [
    source 12
    target 288
  ]
  edge [
    source 12
    target 289
  ]
  edge [
    source 12
    target 290
  ]
  edge [
    source 12
    target 291
  ]
  edge [
    source 12
    target 292
  ]
  edge [
    source 12
    target 293
  ]
  edge [
    source 12
    target 294
  ]
  edge [
    source 12
    target 295
  ]
  edge [
    source 12
    target 296
  ]
  edge [
    source 12
    target 297
  ]
  edge [
    source 12
    target 298
  ]
  edge [
    source 12
    target 299
  ]
  edge [
    source 12
    target 300
  ]
  edge [
    source 12
    target 301
  ]
  edge [
    source 12
    target 302
  ]
  edge [
    source 13
    target 303
  ]
  edge [
    source 13
    target 304
  ]
  edge [
    source 13
    target 305
  ]
  edge [
    source 13
    target 306
  ]
  edge [
    source 13
    target 307
  ]
  edge [
    source 13
    target 308
  ]
  edge [
    source 13
    target 309
  ]
]
