graph [
  maxDegree 43
  minDegree 1
  meanDegree 3.588235294117647
  density 0.035527082119976704
  graphCliqueNumber 14
  node [
    id 0
    label "imienie"
    origin "text"
  ]
  node [
    id 1
    label "rzeczpospolita"
    origin "text"
  ]
  node [
    id 2
    label "polski"
    origin "text"
  ]
  node [
    id 3
    label "prezydent"
    origin "text"
  ]
  node [
    id 4
    label "podawa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "powszechny"
    origin "text"
  ]
  node [
    id 6
    label "wiadomo&#347;ci"
    origin "text"
  ]
  node [
    id 7
    label "Buriacja"
  ]
  node [
    id 8
    label "Abchazja"
  ]
  node [
    id 9
    label "Inguszetia"
  ]
  node [
    id 10
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 11
    label "Nachiczewan"
  ]
  node [
    id 12
    label "Karaka&#322;pacja"
  ]
  node [
    id 13
    label "Jakucja"
  ]
  node [
    id 14
    label "Singapur"
  ]
  node [
    id 15
    label "Komi"
  ]
  node [
    id 16
    label "Karelia"
  ]
  node [
    id 17
    label "Tatarstan"
  ]
  node [
    id 18
    label "Chakasja"
  ]
  node [
    id 19
    label "Dagestan"
  ]
  node [
    id 20
    label "Mordowia"
  ]
  node [
    id 21
    label "Ka&#322;mucja"
  ]
  node [
    id 22
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 23
    label "Republika_Cisalpi&#324;ska"
  ]
  node [
    id 24
    label "Baszkiria"
  ]
  node [
    id 25
    label "pa&#324;stwo_demokratyczne"
  ]
  node [
    id 26
    label "Mari_El"
  ]
  node [
    id 27
    label "Ad&#380;aria"
  ]
  node [
    id 28
    label "Czuwaszja"
  ]
  node [
    id 29
    label "Tuwa"
  ]
  node [
    id 30
    label "Czeczenia"
  ]
  node [
    id 31
    label "Udmurcja"
  ]
  node [
    id 32
    label "pa&#324;stwo"
  ]
  node [
    id 33
    label "lacki"
  ]
  node [
    id 34
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 35
    label "przedmiot"
  ]
  node [
    id 36
    label "sztajer"
  ]
  node [
    id 37
    label "drabant"
  ]
  node [
    id 38
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 39
    label "polak"
  ]
  node [
    id 40
    label "pierogi_ruskie"
  ]
  node [
    id 41
    label "krakowiak"
  ]
  node [
    id 42
    label "Polish"
  ]
  node [
    id 43
    label "j&#281;zyk"
  ]
  node [
    id 44
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 45
    label "oberek"
  ]
  node [
    id 46
    label "po_polsku"
  ]
  node [
    id 47
    label "mazur"
  ]
  node [
    id 48
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 49
    label "chodzony"
  ]
  node [
    id 50
    label "skoczny"
  ]
  node [
    id 51
    label "ryba_po_grecku"
  ]
  node [
    id 52
    label "goniony"
  ]
  node [
    id 53
    label "polsko"
  ]
  node [
    id 54
    label "Jelcyn"
  ]
  node [
    id 55
    label "Roosevelt"
  ]
  node [
    id 56
    label "Clinton"
  ]
  node [
    id 57
    label "dostojnik"
  ]
  node [
    id 58
    label "Nixon"
  ]
  node [
    id 59
    label "Tito"
  ]
  node [
    id 60
    label "de_Gaulle"
  ]
  node [
    id 61
    label "gruba_ryba"
  ]
  node [
    id 62
    label "Gorbaczow"
  ]
  node [
    id 63
    label "Wa&#322;&#281;sa"
  ]
  node [
    id 64
    label "Putin"
  ]
  node [
    id 65
    label "Naser"
  ]
  node [
    id 66
    label "samorz&#261;dowiec"
  ]
  node [
    id 67
    label "Kemal"
  ]
  node [
    id 68
    label "g&#322;owa_pa&#324;stwa"
  ]
  node [
    id 69
    label "zwierzchnik"
  ]
  node [
    id 70
    label "Bierut"
  ]
  node [
    id 71
    label "tenis"
  ]
  node [
    id 72
    label "cover"
  ]
  node [
    id 73
    label "siatk&#243;wka"
  ]
  node [
    id 74
    label "dawa&#263;"
  ]
  node [
    id 75
    label "faszerowa&#263;"
  ]
  node [
    id 76
    label "informowa&#263;"
  ]
  node [
    id 77
    label "introduce"
  ]
  node [
    id 78
    label "jedzenie"
  ]
  node [
    id 79
    label "tender"
  ]
  node [
    id 80
    label "deal"
  ]
  node [
    id 81
    label "kelner"
  ]
  node [
    id 82
    label "serwowa&#263;"
  ]
  node [
    id 83
    label "rozgrywa&#263;"
  ]
  node [
    id 84
    label "stawia&#263;"
  ]
  node [
    id 85
    label "zbiorowy"
  ]
  node [
    id 86
    label "generalny"
  ]
  node [
    id 87
    label "cz&#281;sty"
  ]
  node [
    id 88
    label "znany"
  ]
  node [
    id 89
    label "powszechnie"
  ]
  node [
    id 90
    label "umowy"
  ]
  node [
    id 91
    label "mi&#281;dzy"
  ]
  node [
    id 92
    label "rz&#261;d"
  ]
  node [
    id 93
    label "albo"
  ]
  node [
    id 94
    label "ukraina"
  ]
  node [
    id 95
    label "ojciec"
  ]
  node [
    id 96
    label "wsp&#243;&#322;praca"
  ]
  node [
    id 97
    label "wyspa"
  ]
  node [
    id 98
    label "zakres"
  ]
  node [
    id 99
    label "zwalcza&#263;"
  ]
  node [
    id 100
    label "przest&#281;pczo&#347;&#263;"
  ]
  node [
    id 101
    label "zorganizowa&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 1
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 2
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 90
    target 91
  ]
  edge [
    source 90
    target 92
  ]
  edge [
    source 90
    target 93
  ]
  edge [
    source 90
    target 94
  ]
  edge [
    source 90
    target 95
  ]
  edge [
    source 90
    target 96
  ]
  edge [
    source 90
    target 97
  ]
  edge [
    source 90
    target 98
  ]
  edge [
    source 90
    target 99
  ]
  edge [
    source 90
    target 100
  ]
  edge [
    source 90
    target 101
  ]
  edge [
    source 91
    target 92
  ]
  edge [
    source 91
    target 93
  ]
  edge [
    source 91
    target 94
  ]
  edge [
    source 91
    target 95
  ]
  edge [
    source 91
    target 96
  ]
  edge [
    source 91
    target 97
  ]
  edge [
    source 91
    target 98
  ]
  edge [
    source 91
    target 99
  ]
  edge [
    source 91
    target 100
  ]
  edge [
    source 91
    target 101
  ]
  edge [
    source 92
    target 93
  ]
  edge [
    source 92
    target 92
  ]
  edge [
    source 92
    target 94
  ]
  edge [
    source 92
    target 95
  ]
  edge [
    source 92
    target 96
  ]
  edge [
    source 92
    target 97
  ]
  edge [
    source 92
    target 98
  ]
  edge [
    source 92
    target 99
  ]
  edge [
    source 92
    target 100
  ]
  edge [
    source 92
    target 101
  ]
  edge [
    source 93
    target 94
  ]
  edge [
    source 93
    target 95
  ]
  edge [
    source 93
    target 96
  ]
  edge [
    source 93
    target 97
  ]
  edge [
    source 93
    target 98
  ]
  edge [
    source 93
    target 99
  ]
  edge [
    source 93
    target 100
  ]
  edge [
    source 93
    target 101
  ]
  edge [
    source 94
    target 95
  ]
  edge [
    source 94
    target 96
  ]
  edge [
    source 94
    target 97
  ]
  edge [
    source 94
    target 98
  ]
  edge [
    source 94
    target 99
  ]
  edge [
    source 94
    target 100
  ]
  edge [
    source 94
    target 101
  ]
  edge [
    source 95
    target 96
  ]
  edge [
    source 95
    target 97
  ]
  edge [
    source 95
    target 98
  ]
  edge [
    source 95
    target 99
  ]
  edge [
    source 95
    target 100
  ]
  edge [
    source 95
    target 101
  ]
  edge [
    source 96
    target 97
  ]
  edge [
    source 96
    target 98
  ]
  edge [
    source 96
    target 99
  ]
  edge [
    source 96
    target 100
  ]
  edge [
    source 96
    target 101
  ]
  edge [
    source 97
    target 98
  ]
  edge [
    source 97
    target 99
  ]
  edge [
    source 97
    target 100
  ]
  edge [
    source 97
    target 101
  ]
  edge [
    source 98
    target 99
  ]
  edge [
    source 98
    target 100
  ]
  edge [
    source 98
    target 101
  ]
  edge [
    source 99
    target 100
  ]
  edge [
    source 99
    target 101
  ]
  edge [
    source 100
    target 101
  ]
]
