graph [
  maxDegree 36
  minDegree 1
  meanDegree 2.3958333333333335
  density 0.012543630017452007
  graphCliqueNumber 5
  node [
    id 0
    label "dyrekcja"
    origin "text"
  ]
  node [
    id 1
    label "zak&#322;ad"
    origin "text"
  ]
  node [
    id 2
    label "mechaniczny"
    origin "text"
  ]
  node [
    id 3
    label "plage"
    origin "text"
  ]
  node [
    id 4
    label "la&#347;kiewicz"
    origin "text"
  ]
  node [
    id 5
    label "zrezygnowa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "jeszcze"
    origin "text"
  ]
  node [
    id 7
    label "kwiecie&#324;"
    origin "text"
  ]
  node [
    id 8
    label "rok"
    origin "text"
  ]
  node [
    id 9
    label "zwr&#243;ci&#263;"
    origin "text"
  ]
  node [
    id 10
    label "si&#281;"
    origin "text"
  ]
  node [
    id 11
    label "kierownictwo"
    origin "text"
  ]
  node [
    id 12
    label "marynarka"
    origin "text"
  ]
  node [
    id 13
    label "wojenny"
    origin "text"
  ]
  node [
    id 14
    label "oferta"
    origin "text"
  ]
  node [
    id 15
    label "przebudowa"
    origin "text"
  ]
  node [
    id 16
    label "samolot"
    origin "text"
  ]
  node [
    id 17
    label "lublin"
    origin "text"
  ]
  node [
    id 18
    label "rocznik"
    origin "text"
  ]
  node [
    id 19
    label "viii"
    origin "text"
  ]
  node [
    id 20
    label "wodnosamolot"
    origin "text"
  ]
  node [
    id 21
    label "p&#322;ywakowy"
    origin "text"
  ]
  node [
    id 22
    label "zapozna&#263;"
    origin "text"
  ]
  node [
    id 23
    label "zam&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 24
    label "umowa"
    origin "text"
  ]
  node [
    id 25
    label "podpisa&#263;"
    origin "text"
  ]
  node [
    id 26
    label "luty"
    origin "text"
  ]
  node [
    id 27
    label "jednostka_administracyjna"
  ]
  node [
    id 28
    label "urz&#261;d"
  ]
  node [
    id 29
    label "biuro"
  ]
  node [
    id 30
    label "technika"
  ]
  node [
    id 31
    label "w&#322;adza"
  ]
  node [
    id 32
    label "directorship"
  ]
  node [
    id 33
    label "lead"
  ]
  node [
    id 34
    label "praca"
  ]
  node [
    id 35
    label "siedziba"
  ]
  node [
    id 36
    label "czyn"
  ]
  node [
    id 37
    label "company"
  ]
  node [
    id 38
    label "zak&#322;adka"
  ]
  node [
    id 39
    label "firma"
  ]
  node [
    id 40
    label "instytut"
  ]
  node [
    id 41
    label "wyko&#324;czenie"
  ]
  node [
    id 42
    label "jednostka_organizacyjna"
  ]
  node [
    id 43
    label "instytucja"
  ]
  node [
    id 44
    label "miejsce_pracy"
  ]
  node [
    id 45
    label "mechanicznie"
  ]
  node [
    id 46
    label "bezwiednie"
  ]
  node [
    id 47
    label "nie&#347;wiadomy"
  ]
  node [
    id 48
    label "samoistny"
  ]
  node [
    id 49
    label "typowy"
  ]
  node [
    id 50
    label "poniewolny"
  ]
  node [
    id 51
    label "przesta&#263;"
  ]
  node [
    id 52
    label "drop"
  ]
  node [
    id 53
    label "ci&#261;gle"
  ]
  node [
    id 54
    label "miesi&#261;c"
  ]
  node [
    id 55
    label "stulecie"
  ]
  node [
    id 56
    label "kalendarz"
  ]
  node [
    id 57
    label "czas"
  ]
  node [
    id 58
    label "pora_roku"
  ]
  node [
    id 59
    label "cykl_astronomiczny"
  ]
  node [
    id 60
    label "p&#243;&#322;rocze"
  ]
  node [
    id 61
    label "grupa"
  ]
  node [
    id 62
    label "kwarta&#322;"
  ]
  node [
    id 63
    label "kurs"
  ]
  node [
    id 64
    label "jubileusz"
  ]
  node [
    id 65
    label "lata"
  ]
  node [
    id 66
    label "martwy_sezon"
  ]
  node [
    id 67
    label "return"
  ]
  node [
    id 68
    label "rzygn&#261;&#263;"
  ]
  node [
    id 69
    label "shaftowa&#263;_si&#281;"
  ]
  node [
    id 70
    label "wydali&#263;"
  ]
  node [
    id 71
    label "direct"
  ]
  node [
    id 72
    label "zespawa&#263;_si&#281;"
  ]
  node [
    id 73
    label "przeznaczy&#263;"
  ]
  node [
    id 74
    label "give"
  ]
  node [
    id 75
    label "ustawi&#263;"
  ]
  node [
    id 76
    label "przekaza&#263;"
  ]
  node [
    id 77
    label "regenerate"
  ]
  node [
    id 78
    label "z_powrotem"
  ]
  node [
    id 79
    label "set"
  ]
  node [
    id 80
    label "zesp&#243;&#322;"
  ]
  node [
    id 81
    label "g&#243;ra"
  ]
  node [
    id 82
    label "r&#281;kaw"
  ]
  node [
    id 83
    label "United_States_Navy"
  ]
  node [
    id 84
    label "zbi&#243;r"
  ]
  node [
    id 85
    label "guzik"
  ]
  node [
    id 86
    label "wojsko"
  ]
  node [
    id 87
    label "klapa"
  ]
  node [
    id 88
    label "bojowo"
  ]
  node [
    id 89
    label "propozycja"
  ]
  node [
    id 90
    label "offer"
  ]
  node [
    id 91
    label "budowa"
  ]
  node [
    id 92
    label "conversion"
  ]
  node [
    id 93
    label "zmiana"
  ]
  node [
    id 94
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 95
    label "p&#322;atowiec"
  ]
  node [
    id 96
    label "lecenie"
  ]
  node [
    id 97
    label "gondola"
  ]
  node [
    id 98
    label "wylecie&#263;"
  ]
  node [
    id 99
    label "kapotowanie"
  ]
  node [
    id 100
    label "wylatywa&#263;"
  ]
  node [
    id 101
    label "katapulta"
  ]
  node [
    id 102
    label "dzi&#243;b"
  ]
  node [
    id 103
    label "sterownica"
  ]
  node [
    id 104
    label "kad&#322;ub"
  ]
  node [
    id 105
    label "kapotowa&#263;"
  ]
  node [
    id 106
    label "p&#322;at_no&#347;ny"
  ]
  node [
    id 107
    label "fotel_lotniczy"
  ]
  node [
    id 108
    label "kabina"
  ]
  node [
    id 109
    label "wylatywanie"
  ]
  node [
    id 110
    label "pilot_automatyczny"
  ]
  node [
    id 111
    label "inhalator_tlenowy"
  ]
  node [
    id 112
    label "kapota&#380;"
  ]
  node [
    id 113
    label "pok&#322;ad"
  ]
  node [
    id 114
    label "&#380;yroskop"
  ]
  node [
    id 115
    label "sta&#322;op&#322;at"
  ]
  node [
    id 116
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 117
    label "wy&#347;lizg"
  ]
  node [
    id 118
    label "miejsce_siedz&#261;ce"
  ]
  node [
    id 119
    label "skrzyd&#322;o"
  ]
  node [
    id 120
    label "wiatrochron"
  ]
  node [
    id 121
    label "spalin&#243;wka"
  ]
  node [
    id 122
    label "czarna_skrzynka"
  ]
  node [
    id 123
    label "kapot"
  ]
  node [
    id 124
    label "wylecenie"
  ]
  node [
    id 125
    label "kabinka"
  ]
  node [
    id 126
    label "formacja"
  ]
  node [
    id 127
    label "kronika"
  ]
  node [
    id 128
    label "czasopismo"
  ]
  node [
    id 129
    label "yearbook"
  ]
  node [
    id 130
    label "pod&#322;odzie"
  ]
  node [
    id 131
    label "wodowisko"
  ]
  node [
    id 132
    label "pozna&#263;"
  ]
  node [
    id 133
    label "zawrze&#263;"
  ]
  node [
    id 134
    label "teach"
  ]
  node [
    id 135
    label "insert"
  ]
  node [
    id 136
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 137
    label "poinformowa&#263;"
  ]
  node [
    id 138
    label "obznajomi&#263;"
  ]
  node [
    id 139
    label "poleci&#263;"
  ]
  node [
    id 140
    label "zarezerwowa&#263;"
  ]
  node [
    id 141
    label "zamawianie"
  ]
  node [
    id 142
    label "zaczarowa&#263;"
  ]
  node [
    id 143
    label "order"
  ]
  node [
    id 144
    label "zam&#243;wienie"
  ]
  node [
    id 145
    label "bespeak"
  ]
  node [
    id 146
    label "indent"
  ]
  node [
    id 147
    label "indenture"
  ]
  node [
    id 148
    label "zamawia&#263;"
  ]
  node [
    id 149
    label "zg&#322;osi&#263;"
  ]
  node [
    id 150
    label "appoint"
  ]
  node [
    id 151
    label "um&#243;wi&#263;_si&#281;"
  ]
  node [
    id 152
    label "zleci&#263;"
  ]
  node [
    id 153
    label "contract"
  ]
  node [
    id 154
    label "gestia_transportowa"
  ]
  node [
    id 155
    label "klauzula"
  ]
  node [
    id 156
    label "porozumienie"
  ]
  node [
    id 157
    label "warunek"
  ]
  node [
    id 158
    label "zawarcie"
  ]
  node [
    id 159
    label "za&#347;wiadczy&#263;"
  ]
  node [
    id 160
    label "postawi&#263;"
  ]
  node [
    id 161
    label "sign"
  ]
  node [
    id 162
    label "opatrzy&#263;"
  ]
  node [
    id 163
    label "Matka_Boska_Gromniczna"
  ]
  node [
    id 164
    label "walentynki"
  ]
  node [
    id 165
    label "Plage"
  ]
  node [
    id 166
    label "i"
  ]
  node [
    id 167
    label "La&#347;kiewicz"
  ]
  node [
    id 168
    label "VIII"
  ]
  node [
    id 169
    label "Lorraine"
  ]
  node [
    id 170
    label "Dietrich"
  ]
  node [
    id 171
    label "bis"
  ]
  node [
    id 172
    label "Hispano"
  ]
  node [
    id 173
    label "Suizo"
  ]
  node [
    id 174
    label "ter"
  ]
  node [
    id 175
    label "hydra"
  ]
  node [
    id 176
    label "XIII"
  ]
  node [
    id 177
    label "morski"
  ]
  node [
    id 178
    label "dywizjon"
  ]
  node [
    id 179
    label "lotniczy"
  ]
  node [
    id 180
    label "p&#243;&#322;wyspa"
  ]
  node [
    id 181
    label "helski"
  ]
  node [
    id 182
    label "lot"
  ]
  node [
    id 183
    label "ma&#322;y"
  ]
  node [
    id 184
    label "Entanty"
  ]
  node [
    id 185
    label "polski"
  ]
  node [
    id 186
    label "2"
  ]
  node [
    id 187
    label "albo"
  ]
  node [
    id 188
    label "hour"
  ]
  node [
    id 189
    label "Skrzypi&#324;skiego"
  ]
  node [
    id 190
    label "e"
  ]
  node [
    id 191
    label "Wyrwickiego"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 26
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 17
  ]
  edge [
    source 8
    target 168
  ]
  edge [
    source 8
    target 171
  ]
  edge [
    source 8
    target 174
  ]
  edge [
    source 8
    target 175
  ]
  edge [
    source 8
    target 176
  ]
  edge [
    source 8
    target 186
  ]
  edge [
    source 8
    target 187
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 22
  ]
  edge [
    source 10
    target 14
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 80
  ]
  edge [
    source 11
    target 29
  ]
  edge [
    source 11
    target 31
  ]
  edge [
    source 11
    target 33
  ]
  edge [
    source 11
    target 34
  ]
  edge [
    source 11
    target 35
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 21
  ]
  edge [
    source 12
    target 81
  ]
  edge [
    source 12
    target 82
  ]
  edge [
    source 12
    target 83
  ]
  edge [
    source 12
    target 84
  ]
  edge [
    source 12
    target 85
  ]
  edge [
    source 12
    target 86
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 20
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 22
  ]
  edge [
    source 13
    target 88
  ]
  edge [
    source 13
    target 49
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 23
  ]
  edge [
    source 14
    target 89
  ]
  edge [
    source 14
    target 90
  ]
  edge [
    source 14
    target 14
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 91
  ]
  edge [
    source 15
    target 92
  ]
  edge [
    source 15
    target 93
  ]
  edge [
    source 15
    target 23
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 94
  ]
  edge [
    source 16
    target 95
  ]
  edge [
    source 16
    target 96
  ]
  edge [
    source 16
    target 97
  ]
  edge [
    source 16
    target 98
  ]
  edge [
    source 16
    target 99
  ]
  edge [
    source 16
    target 100
  ]
  edge [
    source 16
    target 101
  ]
  edge [
    source 16
    target 102
  ]
  edge [
    source 16
    target 103
  ]
  edge [
    source 16
    target 104
  ]
  edge [
    source 16
    target 105
  ]
  edge [
    source 16
    target 106
  ]
  edge [
    source 16
    target 107
  ]
  edge [
    source 16
    target 108
  ]
  edge [
    source 16
    target 109
  ]
  edge [
    source 16
    target 110
  ]
  edge [
    source 16
    target 111
  ]
  edge [
    source 16
    target 112
  ]
  edge [
    source 16
    target 113
  ]
  edge [
    source 16
    target 114
  ]
  edge [
    source 16
    target 115
  ]
  edge [
    source 16
    target 116
  ]
  edge [
    source 16
    target 117
  ]
  edge [
    source 16
    target 118
  ]
  edge [
    source 16
    target 119
  ]
  edge [
    source 16
    target 120
  ]
  edge [
    source 16
    target 121
  ]
  edge [
    source 16
    target 122
  ]
  edge [
    source 16
    target 123
  ]
  edge [
    source 16
    target 124
  ]
  edge [
    source 16
    target 125
  ]
  edge [
    source 16
    target 20
  ]
  edge [
    source 16
    target 24
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 168
  ]
  edge [
    source 17
    target 175
  ]
  edge [
    source 17
    target 176
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 126
  ]
  edge [
    source 18
    target 127
  ]
  edge [
    source 18
    target 128
  ]
  edge [
    source 18
    target 129
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 24
  ]
  edge [
    source 20
    target 130
  ]
  edge [
    source 20
    target 131
  ]
  edge [
    source 22
    target 132
  ]
  edge [
    source 22
    target 133
  ]
  edge [
    source 22
    target 134
  ]
  edge [
    source 22
    target 135
  ]
  edge [
    source 22
    target 136
  ]
  edge [
    source 22
    target 137
  ]
  edge [
    source 22
    target 138
  ]
  edge [
    source 23
    target 139
  ]
  edge [
    source 23
    target 140
  ]
  edge [
    source 23
    target 141
  ]
  edge [
    source 23
    target 142
  ]
  edge [
    source 23
    target 143
  ]
  edge [
    source 23
    target 144
  ]
  edge [
    source 23
    target 145
  ]
  edge [
    source 23
    target 146
  ]
  edge [
    source 23
    target 147
  ]
  edge [
    source 23
    target 148
  ]
  edge [
    source 23
    target 149
  ]
  edge [
    source 23
    target 150
  ]
  edge [
    source 23
    target 151
  ]
  edge [
    source 23
    target 152
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 36
  ]
  edge [
    source 24
    target 153
  ]
  edge [
    source 24
    target 154
  ]
  edge [
    source 24
    target 133
  ]
  edge [
    source 24
    target 155
  ]
  edge [
    source 24
    target 156
  ]
  edge [
    source 24
    target 157
  ]
  edge [
    source 24
    target 158
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 159
  ]
  edge [
    source 25
    target 160
  ]
  edge [
    source 25
    target 161
  ]
  edge [
    source 25
    target 162
  ]
  edge [
    source 26
    target 54
  ]
  edge [
    source 26
    target 163
  ]
  edge [
    source 26
    target 164
  ]
  edge [
    source 165
    target 166
  ]
  edge [
    source 165
    target 167
  ]
  edge [
    source 166
    target 167
  ]
  edge [
    source 166
    target 182
  ]
  edge [
    source 166
    target 183
  ]
  edge [
    source 166
    target 184
  ]
  edge [
    source 166
    target 185
  ]
  edge [
    source 168
    target 171
  ]
  edge [
    source 168
    target 174
  ]
  edge [
    source 168
    target 175
  ]
  edge [
    source 168
    target 186
  ]
  edge [
    source 168
    target 187
  ]
  edge [
    source 169
    target 170
  ]
  edge [
    source 172
    target 173
  ]
  edge [
    source 177
    target 178
  ]
  edge [
    source 177
    target 179
  ]
  edge [
    source 178
    target 179
  ]
  edge [
    source 180
    target 181
  ]
  edge [
    source 182
    target 183
  ]
  edge [
    source 182
    target 184
  ]
  edge [
    source 182
    target 185
  ]
  edge [
    source 183
    target 184
  ]
  edge [
    source 183
    target 185
  ]
  edge [
    source 184
    target 185
  ]
  edge [
    source 188
    target 189
  ]
  edge [
    source 190
    target 191
  ]
]
