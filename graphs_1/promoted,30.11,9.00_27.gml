graph [
  maxDegree 217
  minDegree 1
  meanDegree 1.9928057553956835
  density 0.007194244604316547
  graphCliqueNumber 2
  node [
    id 0
    label "ponad"
    origin "text"
  ]
  node [
    id 1
    label "po&#322;owa"
    origin "text"
  ]
  node [
    id 2
    label "pa&#324;stwo"
    origin "text"
  ]
  node [
    id 3
    label "wykorzystywa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "cenzura"
    origin "text"
  ]
  node [
    id 5
    label "n&#281;kanie"
    origin "text"
  ]
  node [
    id 6
    label "przemoc"
    origin "text"
  ]
  node [
    id 7
    label "t&#322;umi&#263;"
    origin "text"
  ]
  node [
    id 8
    label "debata"
    origin "text"
  ]
  node [
    id 9
    label "publiczny"
    origin "text"
  ]
  node [
    id 10
    label "medium"
  ]
  node [
    id 11
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 12
    label "czas"
  ]
  node [
    id 13
    label "Filipiny"
  ]
  node [
    id 14
    label "Rwanda"
  ]
  node [
    id 15
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 16
    label "Monako"
  ]
  node [
    id 17
    label "Korea"
  ]
  node [
    id 18
    label "Ghana"
  ]
  node [
    id 19
    label "Czarnog&#243;ra"
  ]
  node [
    id 20
    label "Malawi"
  ]
  node [
    id 21
    label "Indonezja"
  ]
  node [
    id 22
    label "Bu&#322;garia"
  ]
  node [
    id 23
    label "Nauru"
  ]
  node [
    id 24
    label "Kenia"
  ]
  node [
    id 25
    label "Kambod&#380;a"
  ]
  node [
    id 26
    label "Mali"
  ]
  node [
    id 27
    label "Austria"
  ]
  node [
    id 28
    label "interior"
  ]
  node [
    id 29
    label "Armenia"
  ]
  node [
    id 30
    label "Fid&#380;i"
  ]
  node [
    id 31
    label "Tuwalu"
  ]
  node [
    id 32
    label "Etiopia"
  ]
  node [
    id 33
    label "Malta"
  ]
  node [
    id 34
    label "Malezja"
  ]
  node [
    id 35
    label "Grenada"
  ]
  node [
    id 36
    label "Tad&#380;ykistan"
  ]
  node [
    id 37
    label "Wehrlen"
  ]
  node [
    id 38
    label "para"
  ]
  node [
    id 39
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 40
    label "Rumunia"
  ]
  node [
    id 41
    label "Maroko"
  ]
  node [
    id 42
    label "Bhutan"
  ]
  node [
    id 43
    label "S&#322;owacja"
  ]
  node [
    id 44
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 45
    label "Seszele"
  ]
  node [
    id 46
    label "Kuwejt"
  ]
  node [
    id 47
    label "Arabia_Saudyjska"
  ]
  node [
    id 48
    label "Ekwador"
  ]
  node [
    id 49
    label "Kanada"
  ]
  node [
    id 50
    label "Japonia"
  ]
  node [
    id 51
    label "ziemia"
  ]
  node [
    id 52
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 53
    label "Hiszpania"
  ]
  node [
    id 54
    label "Wyspy_Marshalla"
  ]
  node [
    id 55
    label "Botswana"
  ]
  node [
    id 56
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 57
    label "D&#380;ibuti"
  ]
  node [
    id 58
    label "grupa"
  ]
  node [
    id 59
    label "Wietnam"
  ]
  node [
    id 60
    label "Egipt"
  ]
  node [
    id 61
    label "Burkina_Faso"
  ]
  node [
    id 62
    label "Niemcy"
  ]
  node [
    id 63
    label "Khitai"
  ]
  node [
    id 64
    label "Macedonia"
  ]
  node [
    id 65
    label "Albania"
  ]
  node [
    id 66
    label "Madagaskar"
  ]
  node [
    id 67
    label "Bahrajn"
  ]
  node [
    id 68
    label "Jemen"
  ]
  node [
    id 69
    label "Lesoto"
  ]
  node [
    id 70
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 71
    label "Samoa"
  ]
  node [
    id 72
    label "Andora"
  ]
  node [
    id 73
    label "Chiny"
  ]
  node [
    id 74
    label "Cypr"
  ]
  node [
    id 75
    label "Wielka_Brytania"
  ]
  node [
    id 76
    label "Ukraina"
  ]
  node [
    id 77
    label "Paragwaj"
  ]
  node [
    id 78
    label "Trynidad_i_Tobago"
  ]
  node [
    id 79
    label "Libia"
  ]
  node [
    id 80
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 81
    label "Surinam"
  ]
  node [
    id 82
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 83
    label "Australia"
  ]
  node [
    id 84
    label "Nigeria"
  ]
  node [
    id 85
    label "Honduras"
  ]
  node [
    id 86
    label "Bangladesz"
  ]
  node [
    id 87
    label "Peru"
  ]
  node [
    id 88
    label "Kazachstan"
  ]
  node [
    id 89
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 90
    label "Irak"
  ]
  node [
    id 91
    label "holoarktyka"
  ]
  node [
    id 92
    label "USA"
  ]
  node [
    id 93
    label "Sudan"
  ]
  node [
    id 94
    label "Nepal"
  ]
  node [
    id 95
    label "San_Marino"
  ]
  node [
    id 96
    label "Burundi"
  ]
  node [
    id 97
    label "Dominikana"
  ]
  node [
    id 98
    label "Komory"
  ]
  node [
    id 99
    label "granica_pa&#324;stwa"
  ]
  node [
    id 100
    label "Gwatemala"
  ]
  node [
    id 101
    label "Antarktis"
  ]
  node [
    id 102
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 103
    label "Brunei"
  ]
  node [
    id 104
    label "Iran"
  ]
  node [
    id 105
    label "Zimbabwe"
  ]
  node [
    id 106
    label "Namibia"
  ]
  node [
    id 107
    label "Meksyk"
  ]
  node [
    id 108
    label "Kamerun"
  ]
  node [
    id 109
    label "zwrot"
  ]
  node [
    id 110
    label "Somalia"
  ]
  node [
    id 111
    label "Angola"
  ]
  node [
    id 112
    label "Gabon"
  ]
  node [
    id 113
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 114
    label "Mozambik"
  ]
  node [
    id 115
    label "Tajwan"
  ]
  node [
    id 116
    label "Tunezja"
  ]
  node [
    id 117
    label "Nowa_Zelandia"
  ]
  node [
    id 118
    label "Liban"
  ]
  node [
    id 119
    label "Jordania"
  ]
  node [
    id 120
    label "Tonga"
  ]
  node [
    id 121
    label "Czad"
  ]
  node [
    id 122
    label "Liberia"
  ]
  node [
    id 123
    label "Gwinea"
  ]
  node [
    id 124
    label "Belize"
  ]
  node [
    id 125
    label "&#321;otwa"
  ]
  node [
    id 126
    label "Syria"
  ]
  node [
    id 127
    label "Benin"
  ]
  node [
    id 128
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 129
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 130
    label "Dominika"
  ]
  node [
    id 131
    label "Antigua_i_Barbuda"
  ]
  node [
    id 132
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 133
    label "Hanower"
  ]
  node [
    id 134
    label "partia"
  ]
  node [
    id 135
    label "Afganistan"
  ]
  node [
    id 136
    label "Kiribati"
  ]
  node [
    id 137
    label "W&#322;ochy"
  ]
  node [
    id 138
    label "Szwajcaria"
  ]
  node [
    id 139
    label "Sahara_Zachodnia"
  ]
  node [
    id 140
    label "Chorwacja"
  ]
  node [
    id 141
    label "Tajlandia"
  ]
  node [
    id 142
    label "Salwador"
  ]
  node [
    id 143
    label "Bahamy"
  ]
  node [
    id 144
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 145
    label "S&#322;owenia"
  ]
  node [
    id 146
    label "Gambia"
  ]
  node [
    id 147
    label "Urugwaj"
  ]
  node [
    id 148
    label "Zair"
  ]
  node [
    id 149
    label "Erytrea"
  ]
  node [
    id 150
    label "Rosja"
  ]
  node [
    id 151
    label "Uganda"
  ]
  node [
    id 152
    label "Niger"
  ]
  node [
    id 153
    label "Mauritius"
  ]
  node [
    id 154
    label "Turkmenistan"
  ]
  node [
    id 155
    label "Turcja"
  ]
  node [
    id 156
    label "Irlandia"
  ]
  node [
    id 157
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 158
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 159
    label "Gwinea_Bissau"
  ]
  node [
    id 160
    label "Belgia"
  ]
  node [
    id 161
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 162
    label "Palau"
  ]
  node [
    id 163
    label "Barbados"
  ]
  node [
    id 164
    label "Chile"
  ]
  node [
    id 165
    label "Wenezuela"
  ]
  node [
    id 166
    label "W&#281;gry"
  ]
  node [
    id 167
    label "Argentyna"
  ]
  node [
    id 168
    label "Kolumbia"
  ]
  node [
    id 169
    label "Sierra_Leone"
  ]
  node [
    id 170
    label "Azerbejd&#380;an"
  ]
  node [
    id 171
    label "Kongo"
  ]
  node [
    id 172
    label "Pakistan"
  ]
  node [
    id 173
    label "Liechtenstein"
  ]
  node [
    id 174
    label "Nikaragua"
  ]
  node [
    id 175
    label "Senegal"
  ]
  node [
    id 176
    label "Indie"
  ]
  node [
    id 177
    label "Suazi"
  ]
  node [
    id 178
    label "Polska"
  ]
  node [
    id 179
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 180
    label "Algieria"
  ]
  node [
    id 181
    label "terytorium"
  ]
  node [
    id 182
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 183
    label "Jamajka"
  ]
  node [
    id 184
    label "Kostaryka"
  ]
  node [
    id 185
    label "Timor_Wschodni"
  ]
  node [
    id 186
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 187
    label "Kuba"
  ]
  node [
    id 188
    label "Mauretania"
  ]
  node [
    id 189
    label "Portoryko"
  ]
  node [
    id 190
    label "Brazylia"
  ]
  node [
    id 191
    label "Mo&#322;dawia"
  ]
  node [
    id 192
    label "organizacja"
  ]
  node [
    id 193
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 194
    label "Litwa"
  ]
  node [
    id 195
    label "Kirgistan"
  ]
  node [
    id 196
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 197
    label "Izrael"
  ]
  node [
    id 198
    label "Grecja"
  ]
  node [
    id 199
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 200
    label "Holandia"
  ]
  node [
    id 201
    label "Sri_Lanka"
  ]
  node [
    id 202
    label "Katar"
  ]
  node [
    id 203
    label "Mikronezja"
  ]
  node [
    id 204
    label "Mongolia"
  ]
  node [
    id 205
    label "Laos"
  ]
  node [
    id 206
    label "Malediwy"
  ]
  node [
    id 207
    label "Zambia"
  ]
  node [
    id 208
    label "Tanzania"
  ]
  node [
    id 209
    label "Gujana"
  ]
  node [
    id 210
    label "Czechy"
  ]
  node [
    id 211
    label "Panama"
  ]
  node [
    id 212
    label "Uzbekistan"
  ]
  node [
    id 213
    label "Gruzja"
  ]
  node [
    id 214
    label "Serbia"
  ]
  node [
    id 215
    label "Francja"
  ]
  node [
    id 216
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 217
    label "Togo"
  ]
  node [
    id 218
    label "Estonia"
  ]
  node [
    id 219
    label "Oman"
  ]
  node [
    id 220
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 221
    label "Portugalia"
  ]
  node [
    id 222
    label "Boliwia"
  ]
  node [
    id 223
    label "Luksemburg"
  ]
  node [
    id 224
    label "Haiti"
  ]
  node [
    id 225
    label "Wyspy_Salomona"
  ]
  node [
    id 226
    label "Birma"
  ]
  node [
    id 227
    label "Rodezja"
  ]
  node [
    id 228
    label "use"
  ]
  node [
    id 229
    label "krzywdzi&#263;"
  ]
  node [
    id 230
    label "wyr&#281;cza&#263;_si&#281;"
  ]
  node [
    id 231
    label "distribute"
  ]
  node [
    id 232
    label "wsp&#243;&#322;&#380;y&#263;"
  ]
  node [
    id 233
    label "give"
  ]
  node [
    id 234
    label "liga&#263;"
  ]
  node [
    id 235
    label "u&#380;ywa&#263;"
  ]
  node [
    id 236
    label "korzysta&#263;"
  ]
  node [
    id 237
    label "zdj&#281;cie"
  ]
  node [
    id 238
    label "zdj&#261;&#263;"
  ]
  node [
    id 239
    label "bell_ringer"
  ]
  node [
    id 240
    label "&#347;wiadectwo"
  ]
  node [
    id 241
    label "ekskomunikowa&#263;"
  ]
  node [
    id 242
    label "zdejmowanie"
  ]
  node [
    id 243
    label "crisscross"
  ]
  node [
    id 244
    label "urz&#261;d"
  ]
  node [
    id 245
    label "mark"
  ]
  node [
    id 246
    label "drugi_obieg"
  ]
  node [
    id 247
    label "krytyka"
  ]
  node [
    id 248
    label "proces"
  ]
  node [
    id 249
    label "ekskomunikowanie"
  ]
  node [
    id 250
    label "kontrola"
  ]
  node [
    id 251
    label "zjawisko"
  ]
  node [
    id 252
    label "kara"
  ]
  node [
    id 253
    label "p&#243;&#322;kownik"
  ]
  node [
    id 254
    label "zdejmowa&#263;"
  ]
  node [
    id 255
    label "dokuczanie"
  ]
  node [
    id 256
    label "m&#281;czenie"
  ]
  node [
    id 257
    label "niepokojenie"
  ]
  node [
    id 258
    label "dr&#281;czenie"
  ]
  node [
    id 259
    label "przewaga"
  ]
  node [
    id 260
    label "drastyczny"
  ]
  node [
    id 261
    label "agresja"
  ]
  node [
    id 262
    label "patologia"
  ]
  node [
    id 263
    label "os&#322;abia&#263;"
  ]
  node [
    id 264
    label "frown"
  ]
  node [
    id 265
    label "miarkowa&#263;"
  ]
  node [
    id 266
    label "opanowywa&#263;"
  ]
  node [
    id 267
    label "suspend"
  ]
  node [
    id 268
    label "suppress"
  ]
  node [
    id 269
    label "zmniejsza&#263;"
  ]
  node [
    id 270
    label "zwalcza&#263;"
  ]
  node [
    id 271
    label "rozmowa"
  ]
  node [
    id 272
    label "sympozjon"
  ]
  node [
    id 273
    label "conference"
  ]
  node [
    id 274
    label "jawny"
  ]
  node [
    id 275
    label "upublicznienie"
  ]
  node [
    id 276
    label "upublicznianie"
  ]
  node [
    id 277
    label "publicznie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 265
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 267
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 269
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 271
  ]
  edge [
    source 8
    target 272
  ]
  edge [
    source 8
    target 273
  ]
  edge [
    source 9
    target 274
  ]
  edge [
    source 9
    target 275
  ]
  edge [
    source 9
    target 276
  ]
  edge [
    source 9
    target 277
  ]
]
