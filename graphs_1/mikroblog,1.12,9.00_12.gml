graph [
  maxDegree 11
  minDegree 1
  meanDegree 1.9090909090909092
  density 0.09090909090909091
  graphCliqueNumber 2
  node [
    id 0
    label "dobra"
    origin "text"
  ]
  node [
    id 1
    label "ucieka&#263;"
    origin "text"
  ]
  node [
    id 2
    label "dobranoc"
    origin "text"
  ]
  node [
    id 3
    label "frymark"
  ]
  node [
    id 4
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 5
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 6
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 7
    label "commodity"
  ]
  node [
    id 8
    label "mienie"
  ]
  node [
    id 9
    label "Wilko"
  ]
  node [
    id 10
    label "jednostka_monetarna"
  ]
  node [
    id 11
    label "centym"
  ]
  node [
    id 12
    label "spieprza&#263;"
  ]
  node [
    id 13
    label "unika&#263;"
  ]
  node [
    id 14
    label "zwiewa&#263;"
  ]
  node [
    id 15
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 16
    label "wymyka&#263;_si&#281;"
  ]
  node [
    id 17
    label "pali&#263;_wrotki"
  ]
  node [
    id 18
    label "wydostawa&#263;_si&#281;"
  ]
  node [
    id 19
    label "bra&#263;"
  ]
  node [
    id 20
    label "blow"
  ]
  node [
    id 21
    label "pro&#347;ba"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 2
    target 21
  ]
]
