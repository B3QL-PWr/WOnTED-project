graph [
  maxDegree 13
  minDegree 1
  meanDegree 1.9333333333333333
  density 0.06666666666666667
  graphCliqueNumber 2
  node [
    id 0
    label "pose&#322;"
    origin "text"
  ]
  node [
    id 1
    label "aleksander"
    origin "text"
  ]
  node [
    id 2
    label "marek"
    origin "text"
  ]
  node [
    id 3
    label "skorupa"
    origin "text"
  ]
  node [
    id 4
    label "dyplomata"
  ]
  node [
    id 5
    label "wys&#322;annik"
  ]
  node [
    id 6
    label "przedstawiciel"
  ]
  node [
    id 7
    label "kurier_dyplomatyczny"
  ]
  node [
    id 8
    label "ablegat"
  ]
  node [
    id 9
    label "klubista"
  ]
  node [
    id 10
    label "Miko&#322;ajczyk"
  ]
  node [
    id 11
    label "Korwin"
  ]
  node [
    id 12
    label "parlamentarzysta"
  ]
  node [
    id 13
    label "dyscyplina_partyjna"
  ]
  node [
    id 14
    label "izba_ni&#380;sza"
  ]
  node [
    id 15
    label "poselstwo"
  ]
  node [
    id 16
    label "selerowate"
  ]
  node [
    id 17
    label "bylina"
  ]
  node [
    id 18
    label "hygrofit"
  ]
  node [
    id 19
    label "pr&#261;&#380;kowie"
  ]
  node [
    id 20
    label "warstwa"
  ]
  node [
    id 21
    label "&#347;mie&#263;"
  ]
  node [
    id 22
    label "pow&#347;ci&#261;gliwo&#347;&#263;"
  ]
  node [
    id 23
    label "naczynie"
  ]
  node [
    id 24
    label "kawa&#322;ek"
  ]
  node [
    id 25
    label "os&#322;ona_cia&#322;a"
  ]
  node [
    id 26
    label "wstydliwo&#347;&#263;"
  ]
  node [
    id 27
    label "j&#261;dro_soczewkowate"
  ]
  node [
    id 28
    label "hull"
  ]
  node [
    id 29
    label "scale"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
]
