graph [
  maxDegree 12
  minDegree 1
  meanDegree 1.894736842105263
  density 0.10526315789473684
  graphCliqueNumber 2
  node [
    id 0
    label "black"
    origin "text"
  ]
  node [
    id 1
    label "dove"
    origin "text"
  ]
  node [
    id 2
    label "ten"
    origin "text"
  ]
  node [
    id 3
    label "sam"
    origin "text"
  ]
  node [
    id 4
    label "szablon"
    origin "text"
  ]
  node [
    id 5
    label "stocka"
    origin "text"
  ]
  node [
    id 6
    label "okre&#347;lony"
  ]
  node [
    id 7
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 8
    label "sklep"
  ]
  node [
    id 9
    label "drabina_analgetyczna"
  ]
  node [
    id 10
    label "model"
  ]
  node [
    id 11
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 12
    label "exemplar"
  ]
  node [
    id 13
    label "jig"
  ]
  node [
    id 14
    label "C"
  ]
  node [
    id 15
    label "D"
  ]
  node [
    id 16
    label "wz&#243;r"
  ]
  node [
    id 17
    label "struktura"
  ]
  node [
    id 18
    label "mildew"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 4
    target 10
  ]
  edge [
    source 4
    target 11
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
]
