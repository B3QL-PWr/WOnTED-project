graph [
  maxDegree 17
  minDegree 1
  meanDegree 1.9444444444444444
  density 0.05555555555555555
  graphCliqueNumber 2
  node [
    id 0
    label "epidemia"
    origin "text"
  ]
  node [
    id 1
    label "kr&#243;lik"
    origin "text"
  ]
  node [
    id 2
    label "zacz&#261;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "si&#281;"
    origin "text"
  ]
  node [
    id 4
    label "bardzo"
    origin "text"
  ]
  node [
    id 5
    label "wcze&#347;nie"
    origin "text"
  ]
  node [
    id 6
    label "atakowanie"
  ]
  node [
    id 7
    label "plaga"
  ]
  node [
    id 8
    label "atakowa&#263;"
  ]
  node [
    id 9
    label "ro&#347;lino&#380;erca"
  ]
  node [
    id 10
    label "omyk"
  ]
  node [
    id 11
    label "comber"
  ]
  node [
    id 12
    label "futro"
  ]
  node [
    id 13
    label "&#322;o&#380;yskowiec"
  ]
  node [
    id 14
    label "kicaj"
  ]
  node [
    id 15
    label "mysikr&#243;lik"
  ]
  node [
    id 16
    label "wirus"
  ]
  node [
    id 17
    label "program"
  ]
  node [
    id 18
    label "zwierz&#281;_futerkowe"
  ]
  node [
    id 19
    label "turzyca"
  ]
  node [
    id 20
    label "zaj&#261;cowate"
  ]
  node [
    id 21
    label "tuszka"
  ]
  node [
    id 22
    label "trusia"
  ]
  node [
    id 23
    label "mi&#281;so"
  ]
  node [
    id 24
    label "cause"
  ]
  node [
    id 25
    label "introduce"
  ]
  node [
    id 26
    label "begin"
  ]
  node [
    id 27
    label "odj&#261;&#263;"
  ]
  node [
    id 28
    label "post&#261;pi&#263;"
  ]
  node [
    id 29
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 30
    label "do"
  ]
  node [
    id 31
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 32
    label "zrobi&#263;"
  ]
  node [
    id 33
    label "w_chuj"
  ]
  node [
    id 34
    label "wczesno"
  ]
  node [
    id 35
    label "wczesny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
]
