graph [
  maxDegree 14
  minDegree 1
  meanDegree 2
  density 0.03389830508474576
  graphCliqueNumber 3
  node [
    id 0
    label "emanowa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "pozytywnyi"
    origin "text"
  ]
  node [
    id 2
    label "emocja"
    origin "text"
  ]
  node [
    id 3
    label "ciep&#322;o"
    origin "text"
  ]
  node [
    id 4
    label "optymizm"
    origin "text"
  ]
  node [
    id 5
    label "reklama"
    origin "text"
  ]
  node [
    id 6
    label "zach&#281;caj&#261;cy"
    origin "text"
  ]
  node [
    id 7
    label "odwiedzanie"
    origin "text"
  ]
  node [
    id 8
    label "islandia"
    origin "text"
  ]
  node [
    id 9
    label "przyda&#263;by"
    origin "text"
  ]
  node [
    id 10
    label "si&#281;"
    origin "text"
  ]
  node [
    id 11
    label "polska"
    origin "text"
  ]
  node [
    id 12
    label "pokazywa&#263;"
  ]
  node [
    id 13
    label "uzewn&#281;trznia&#263;_si&#281;"
  ]
  node [
    id 14
    label "emit"
  ]
  node [
    id 15
    label "wydziela&#263;"
  ]
  node [
    id 16
    label "air"
  ]
  node [
    id 17
    label "wydziela&#263;_si&#281;"
  ]
  node [
    id 18
    label "radio_beam"
  ]
  node [
    id 19
    label "ostygn&#261;&#263;"
  ]
  node [
    id 20
    label "afekt"
  ]
  node [
    id 21
    label "stan"
  ]
  node [
    id 22
    label "d&#322;awi&#263;"
  ]
  node [
    id 23
    label "wpa&#347;&#263;"
  ]
  node [
    id 24
    label "wpada&#263;"
  ]
  node [
    id 25
    label "iskrzy&#263;"
  ]
  node [
    id 26
    label "ogrom"
  ]
  node [
    id 27
    label "stygn&#261;&#263;"
  ]
  node [
    id 28
    label "temperatura"
  ]
  node [
    id 29
    label "ow&#322;adni&#281;ty"
  ]
  node [
    id 30
    label "mi&#322;o"
  ]
  node [
    id 31
    label "cecha"
  ]
  node [
    id 32
    label "geotermia"
  ]
  node [
    id 33
    label "przyjemnie"
  ]
  node [
    id 34
    label "ciep&#322;y"
  ]
  node [
    id 35
    label "heat"
  ]
  node [
    id 36
    label "emisyjno&#347;&#263;"
  ]
  node [
    id 37
    label "zjawisko"
  ]
  node [
    id 38
    label "pogoda"
  ]
  node [
    id 39
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 40
    label "&#380;yczliwo&#347;&#263;"
  ]
  node [
    id 41
    label "postawa"
  ]
  node [
    id 42
    label "buoyancy"
  ]
  node [
    id 43
    label "copywriting"
  ]
  node [
    id 44
    label "brief"
  ]
  node [
    id 45
    label "bran&#380;a"
  ]
  node [
    id 46
    label "informacja"
  ]
  node [
    id 47
    label "promowa&#263;"
  ]
  node [
    id 48
    label "akcja"
  ]
  node [
    id 49
    label "wypromowa&#263;"
  ]
  node [
    id 50
    label "samplowanie"
  ]
  node [
    id 51
    label "tekst"
  ]
  node [
    id 52
    label "mobilizuj&#261;cy"
  ]
  node [
    id 53
    label "przyjazny"
  ]
  node [
    id 54
    label "przyjemny"
  ]
  node [
    id 55
    label "zach&#281;caj&#261;co"
  ]
  node [
    id 56
    label "dobry"
  ]
  node [
    id 57
    label "mi&#322;y"
  ]
  node [
    id 58
    label "&#347;ci&#261;ganie"
  ]
  node [
    id 59
    label "przybywanie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
]
