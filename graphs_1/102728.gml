graph [
  maxDegree 12
  minDegree 1
  meanDegree 1.9487179487179487
  density 0.05128205128205128
  graphCliqueNumber 2
  node [
    id 0
    label "sprawa"
    origin "text"
  ]
  node [
    id 1
    label "ewidencjonowa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "wykaz"
    origin "text"
  ]
  node [
    id 3
    label "med"
    origin "text"
  ]
  node [
    id 4
    label "stosowa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "si&#281;"
    origin "text"
  ]
  node [
    id 6
    label "przepis"
    origin "text"
  ]
  node [
    id 7
    label "temat"
  ]
  node [
    id 8
    label "kognicja"
  ]
  node [
    id 9
    label "idea"
  ]
  node [
    id 10
    label "szczeg&#243;&#322;"
  ]
  node [
    id 11
    label "rzecz"
  ]
  node [
    id 12
    label "wydarzenie"
  ]
  node [
    id 13
    label "przes&#322;anka"
  ]
  node [
    id 14
    label "rozprawa"
  ]
  node [
    id 15
    label "object"
  ]
  node [
    id 16
    label "proposition"
  ]
  node [
    id 17
    label "rejestrowa&#263;"
  ]
  node [
    id 18
    label "wyliczanka"
  ]
  node [
    id 19
    label "catalog"
  ]
  node [
    id 20
    label "stock"
  ]
  node [
    id 21
    label "figurowa&#263;"
  ]
  node [
    id 22
    label "zbi&#243;r"
  ]
  node [
    id 23
    label "book"
  ]
  node [
    id 24
    label "pozycja"
  ]
  node [
    id 25
    label "tekst"
  ]
  node [
    id 26
    label "sumariusz"
  ]
  node [
    id 27
    label "u&#380;ywa&#263;"
  ]
  node [
    id 28
    label "przedawnienie_si&#281;"
  ]
  node [
    id 29
    label "recepta"
  ]
  node [
    id 30
    label "norma_prawna"
  ]
  node [
    id 31
    label "kodeks"
  ]
  node [
    id 32
    label "prawo"
  ]
  node [
    id 33
    label "regulation"
  ]
  node [
    id 34
    label "przedawni&#263;_si&#281;"
  ]
  node [
    id 35
    label "porada"
  ]
  node [
    id 36
    label "przedawnianie_si&#281;"
  ]
  node [
    id 37
    label "spos&#243;b"
  ]
  node [
    id 38
    label "przedawnia&#263;_si&#281;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
]
