graph [
  maxDegree 39
  minDegree 1
  meanDegree 2.116591928251121
  density 0.004756386355620497
  graphCliqueNumber 3
  node [
    id 0
    label "choroba"
    origin "text"
  ]
  node [
    id 1
    label "by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "rzeczywi&#347;cie"
    origin "text"
  ]
  node [
    id 3
    label "do&#347;&#263;"
    origin "text"
  ]
  node [
    id 4
    label "paskudny"
    origin "text"
  ]
  node [
    id 5
    label "patrze&#263;"
    origin "text"
  ]
  node [
    id 6
    label "niemi&#322;y"
    origin "text"
  ]
  node [
    id 7
    label "zdj&#281;cie"
    origin "text"
  ]
  node [
    id 8
    label "ale"
    origin "text"
  ]
  node [
    id 9
    label "znana"
    origin "text"
  ]
  node [
    id 10
    label "dawny"
    origin "text"
  ]
  node [
    id 11
    label "&#380;aden"
    origin "text"
  ]
  node [
    id 12
    label "wypadek"
    origin "text"
  ]
  node [
    id 13
    label "wiadomo"
    origin "text"
  ]
  node [
    id 14
    label "zreszt&#261;"
    origin "text"
  ]
  node [
    id 15
    label "z&#322;ocisty"
    origin "text"
  ]
  node [
    id 16
    label "og&#243;&#322;"
    origin "text"
  ]
  node [
    id 17
    label "wyj&#261;tkowo"
    origin "text"
  ]
  node [
    id 18
    label "wredny"
    origin "text"
  ]
  node [
    id 19
    label "bydl&#281;"
    origin "text"
  ]
  node [
    id 20
    label "czas"
    origin "text"
  ]
  node [
    id 21
    label "faktycznie"
    origin "text"
  ]
  node [
    id 22
    label "u&#347;mierca&#263;"
    origin "text"
  ]
  node [
    id 23
    label "grypa"
    origin "text"
  ]
  node [
    id 24
    label "te&#380;"
    origin "text"
  ]
  node [
    id 25
    label "dodatek"
    origin "text"
  ]
  node [
    id 26
    label "przeczyta&#263;"
    origin "text"
  ]
  node [
    id 27
    label "przez"
    origin "text"
  ]
  node [
    id 28
    label "tekst"
    origin "text"
  ]
  node [
    id 29
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 30
    label "mowa"
    origin "text"
  ]
  node [
    id 31
    label "przenosi&#263;"
    origin "text"
  ]
  node [
    id 32
    label "bakteria"
    origin "text"
  ]
  node [
    id 33
    label "droga"
    origin "text"
  ]
  node [
    id 34
    label "p&#322;ciowy"
    origin "text"
  ]
  node [
    id 35
    label "szczeg&#243;lno&#347;&#263;"
    origin "text"
  ]
  node [
    id 36
    label "podczas"
    origin "text"
  ]
  node [
    id 37
    label "stosunek"
    origin "text"
  ]
  node [
    id 38
    label "homoseksualny"
    origin "text"
  ]
  node [
    id 39
    label "pisa&#263;"
    origin "text"
  ]
  node [
    id 40
    label "taki"
    origin "text"
  ]
  node [
    id 41
    label "sprawdzi&#263;"
    origin "text"
  ]
  node [
    id 42
    label "bajka"
    origin "text"
  ]
  node [
    id 43
    label "jak"
    origin "text"
  ]
  node [
    id 44
    label "si&#281;"
    origin "text"
  ]
  node [
    id 45
    label "rzetelno&#347;&#263;"
    origin "text"
  ]
  node [
    id 46
    label "dziennikarski"
    origin "text"
  ]
  node [
    id 47
    label "tylko"
    origin "text"
  ]
  node [
    id 48
    label "dlatego"
    origin "text"
  ]
  node [
    id 49
    label "wiadomo&#347;ci"
    origin "text"
  ]
  node [
    id 50
    label "katoendeckiej"
    origin "text"
  ]
  node [
    id 51
    label "gazeta"
    origin "text"
  ]
  node [
    id 52
    label "postraszy&#263;"
    origin "text"
  ]
  node [
    id 53
    label "ognisko"
  ]
  node [
    id 54
    label "odezwanie_si&#281;"
  ]
  node [
    id 55
    label "zaostrzy&#263;_si&#281;"
  ]
  node [
    id 56
    label "z&#322;&#243;g_wapniowy"
  ]
  node [
    id 57
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 58
    label "przypadek"
  ]
  node [
    id 59
    label "zajmowa&#263;"
  ]
  node [
    id 60
    label "zajmowanie"
  ]
  node [
    id 61
    label "badanie_histopatologiczne"
  ]
  node [
    id 62
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 63
    label "atakowanie"
  ]
  node [
    id 64
    label "odezwa&#263;_si&#281;"
  ]
  node [
    id 65
    label "bol&#261;czka"
  ]
  node [
    id 66
    label "remisja"
  ]
  node [
    id 67
    label "grupa_ryzyka"
  ]
  node [
    id 68
    label "atakowa&#263;"
  ]
  node [
    id 69
    label "kryzys"
  ]
  node [
    id 70
    label "nabawienie_si&#281;"
  ]
  node [
    id 71
    label "chor&#243;bka"
  ]
  node [
    id 72
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 73
    label "ubezpieczenie_chorobowe"
  ]
  node [
    id 74
    label "inkubacja"
  ]
  node [
    id 75
    label "powalenie"
  ]
  node [
    id 76
    label "cholera"
  ]
  node [
    id 77
    label "nabawianie_si&#281;"
  ]
  node [
    id 78
    label "odzywa&#263;_si&#281;"
  ]
  node [
    id 79
    label "odzywanie_si&#281;"
  ]
  node [
    id 80
    label "diagnoza"
  ]
  node [
    id 81
    label "zaostrza&#263;_si&#281;"
  ]
  node [
    id 82
    label "powali&#263;"
  ]
  node [
    id 83
    label "zaburzenie"
  ]
  node [
    id 84
    label "si&#281;ga&#263;"
  ]
  node [
    id 85
    label "trwa&#263;"
  ]
  node [
    id 86
    label "obecno&#347;&#263;"
  ]
  node [
    id 87
    label "stan"
  ]
  node [
    id 88
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 89
    label "stand"
  ]
  node [
    id 90
    label "mie&#263;_miejsce"
  ]
  node [
    id 91
    label "uczestniczy&#263;"
  ]
  node [
    id 92
    label "chodzi&#263;"
  ]
  node [
    id 93
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 94
    label "equal"
  ]
  node [
    id 95
    label "realny"
  ]
  node [
    id 96
    label "naprawd&#281;"
  ]
  node [
    id 97
    label "parchaty"
  ]
  node [
    id 98
    label "paskudnie"
  ]
  node [
    id 99
    label "niezno&#347;ny"
  ]
  node [
    id 100
    label "okropnie"
  ]
  node [
    id 101
    label "z&#322;y"
  ]
  node [
    id 102
    label "straszny"
  ]
  node [
    id 103
    label "koszmarny"
  ]
  node [
    id 104
    label "brzydki"
  ]
  node [
    id 105
    label "koso"
  ]
  node [
    id 106
    label "szuka&#263;"
  ]
  node [
    id 107
    label "go_steady"
  ]
  node [
    id 108
    label "dba&#263;"
  ]
  node [
    id 109
    label "traktowa&#263;"
  ]
  node [
    id 110
    label "os&#261;dza&#263;"
  ]
  node [
    id 111
    label "punkt_widzenia"
  ]
  node [
    id 112
    label "robi&#263;"
  ]
  node [
    id 113
    label "uwa&#380;a&#263;"
  ]
  node [
    id 114
    label "look"
  ]
  node [
    id 115
    label "pogl&#261;da&#263;"
  ]
  node [
    id 116
    label "przygl&#261;da&#263;_si&#281;"
  ]
  node [
    id 117
    label "niemile"
  ]
  node [
    id 118
    label "rozd&#281;cie_si&#281;"
  ]
  node [
    id 119
    label "niezgodny"
  ]
  node [
    id 120
    label "niew&#322;a&#347;ciwy"
  ]
  node [
    id 121
    label "nieprzyjemnie"
  ]
  node [
    id 122
    label "wyretuszowanie"
  ]
  node [
    id 123
    label "podlew"
  ]
  node [
    id 124
    label "po&#347;ci&#261;ganie"
  ]
  node [
    id 125
    label "cenzura"
  ]
  node [
    id 126
    label "legitymacja"
  ]
  node [
    id 127
    label "uniewa&#380;nienie"
  ]
  node [
    id 128
    label "abolicjonista"
  ]
  node [
    id 129
    label "withdrawal"
  ]
  node [
    id 130
    label "uwolnienie"
  ]
  node [
    id 131
    label "picture"
  ]
  node [
    id 132
    label "retuszowa&#263;"
  ]
  node [
    id 133
    label "fota"
  ]
  node [
    id 134
    label "obraz"
  ]
  node [
    id 135
    label "fototeka"
  ]
  node [
    id 136
    label "zrobienie"
  ]
  node [
    id 137
    label "retuszowanie"
  ]
  node [
    id 138
    label "monid&#322;o"
  ]
  node [
    id 139
    label "talbotypia"
  ]
  node [
    id 140
    label "relief"
  ]
  node [
    id 141
    label "wyretuszowa&#263;"
  ]
  node [
    id 142
    label "photograph"
  ]
  node [
    id 143
    label "zabronienie"
  ]
  node [
    id 144
    label "ziarno"
  ]
  node [
    id 145
    label "przepa&#322;"
  ]
  node [
    id 146
    label "fotogaleria"
  ]
  node [
    id 147
    label "cinch"
  ]
  node [
    id 148
    label "odsuni&#281;cie"
  ]
  node [
    id 149
    label "rozpakowanie"
  ]
  node [
    id 150
    label "piwo"
  ]
  node [
    id 151
    label "przesz&#322;y"
  ]
  node [
    id 152
    label "dawno"
  ]
  node [
    id 153
    label "dawniej"
  ]
  node [
    id 154
    label "kombatant"
  ]
  node [
    id 155
    label "stary"
  ]
  node [
    id 156
    label "odleg&#322;y"
  ]
  node [
    id 157
    label "anachroniczny"
  ]
  node [
    id 158
    label "przestarza&#322;y"
  ]
  node [
    id 159
    label "od_dawna"
  ]
  node [
    id 160
    label "poprzedni"
  ]
  node [
    id 161
    label "d&#322;ugoletni"
  ]
  node [
    id 162
    label "wcze&#347;niejszy"
  ]
  node [
    id 163
    label "niegdysiejszy"
  ]
  node [
    id 164
    label "nijaki"
  ]
  node [
    id 165
    label "czynno&#347;&#263;"
  ]
  node [
    id 166
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 167
    label "motyw"
  ]
  node [
    id 168
    label "ubezpieczenie_wypadkowe"
  ]
  node [
    id 169
    label "fabu&#322;a"
  ]
  node [
    id 170
    label "przebiec"
  ]
  node [
    id 171
    label "wydarzenie"
  ]
  node [
    id 172
    label "happening"
  ]
  node [
    id 173
    label "przebiegni&#281;cie"
  ]
  node [
    id 174
    label "event"
  ]
  node [
    id 175
    label "charakter"
  ]
  node [
    id 176
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 177
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 178
    label "poz&#322;ocenie"
  ]
  node [
    id 179
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 180
    label "z&#322;oci&#347;cie"
  ]
  node [
    id 181
    label "z&#322;ocenie"
  ]
  node [
    id 182
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 183
    label "pole"
  ]
  node [
    id 184
    label "kastowo&#347;&#263;"
  ]
  node [
    id 185
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 186
    label "ludzie_pracy"
  ]
  node [
    id 187
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 188
    label "community"
  ]
  node [
    id 189
    label "Fremeni"
  ]
  node [
    id 190
    label "status"
  ]
  node [
    id 191
    label "pozaklasowy"
  ]
  node [
    id 192
    label "aspo&#322;eczny"
  ]
  node [
    id 193
    label "pe&#322;ny"
  ]
  node [
    id 194
    label "ilo&#347;&#263;"
  ]
  node [
    id 195
    label "uwarstwienie"
  ]
  node [
    id 196
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 197
    label "zlewanie_si&#281;"
  ]
  node [
    id 198
    label "elita"
  ]
  node [
    id 199
    label "cywilizacja"
  ]
  node [
    id 200
    label "wspo&#322;ecze&#324;stwo"
  ]
  node [
    id 201
    label "klasa"
  ]
  node [
    id 202
    label "niestandardowo"
  ]
  node [
    id 203
    label "wyj&#261;tkowy"
  ]
  node [
    id 204
    label "niezwykle"
  ]
  node [
    id 205
    label "wrednie"
  ]
  node [
    id 206
    label "przekl&#281;ty"
  ]
  node [
    id 207
    label "z&#322;o&#347;liwy"
  ]
  node [
    id 208
    label "ma&#322;y"
  ]
  node [
    id 209
    label "zwierz&#281;_gospodarskie"
  ]
  node [
    id 210
    label "&#322;ajdak"
  ]
  node [
    id 211
    label "byd&#322;o_domowe"
  ]
  node [
    id 212
    label "zwierz&#281;"
  ]
  node [
    id 213
    label "prze&#380;uwacz"
  ]
  node [
    id 214
    label "czasokres"
  ]
  node [
    id 215
    label "trawienie"
  ]
  node [
    id 216
    label "kategoria_gramatyczna"
  ]
  node [
    id 217
    label "period"
  ]
  node [
    id 218
    label "odczyt"
  ]
  node [
    id 219
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 220
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 221
    label "chwila"
  ]
  node [
    id 222
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 223
    label "poprzedzenie"
  ]
  node [
    id 224
    label "koniugacja"
  ]
  node [
    id 225
    label "dzieje"
  ]
  node [
    id 226
    label "poprzedzi&#263;"
  ]
  node [
    id 227
    label "przep&#322;ywanie"
  ]
  node [
    id 228
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 229
    label "odwlekanie_si&#281;"
  ]
  node [
    id 230
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 231
    label "Zeitgeist"
  ]
  node [
    id 232
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 233
    label "okres_czasu"
  ]
  node [
    id 234
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 235
    label "pochodzi&#263;"
  ]
  node [
    id 236
    label "schy&#322;ek"
  ]
  node [
    id 237
    label "czwarty_wymiar"
  ]
  node [
    id 238
    label "chronometria"
  ]
  node [
    id 239
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 240
    label "poprzedzanie"
  ]
  node [
    id 241
    label "pogoda"
  ]
  node [
    id 242
    label "zegar"
  ]
  node [
    id 243
    label "pochodzenie"
  ]
  node [
    id 244
    label "poprzedza&#263;"
  ]
  node [
    id 245
    label "trawi&#263;"
  ]
  node [
    id 246
    label "time_period"
  ]
  node [
    id 247
    label "rachuba_czasu"
  ]
  node [
    id 248
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 249
    label "czasoprzestrze&#324;"
  ]
  node [
    id 250
    label "laba"
  ]
  node [
    id 251
    label "realnie"
  ]
  node [
    id 252
    label "faktyczny"
  ]
  node [
    id 253
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 254
    label "powodowa&#263;"
  ]
  node [
    id 255
    label "wirus_grypy"
  ]
  node [
    id 256
    label "influenca"
  ]
  node [
    id 257
    label "choroba_zara&#378;liwa"
  ]
  node [
    id 258
    label "kaszel"
  ]
  node [
    id 259
    label "choroba_wirusowa"
  ]
  node [
    id 260
    label "doj&#347;cie"
  ]
  node [
    id 261
    label "doch&#243;d"
  ]
  node [
    id 262
    label "doj&#347;&#263;"
  ]
  node [
    id 263
    label "przedmiot"
  ]
  node [
    id 264
    label "dochodzenie"
  ]
  node [
    id 265
    label "dziennik"
  ]
  node [
    id 266
    label "aneks"
  ]
  node [
    id 267
    label "rzecz"
  ]
  node [
    id 268
    label "element"
  ]
  node [
    id 269
    label "galanteria"
  ]
  node [
    id 270
    label "pozna&#263;"
  ]
  node [
    id 271
    label "wywr&#243;&#380;y&#263;"
  ]
  node [
    id 272
    label "przetworzy&#263;"
  ]
  node [
    id 273
    label "read"
  ]
  node [
    id 274
    label "zaobserwowa&#263;"
  ]
  node [
    id 275
    label "odczyta&#263;"
  ]
  node [
    id 276
    label "odmianka"
  ]
  node [
    id 277
    label "opu&#347;ci&#263;"
  ]
  node [
    id 278
    label "wypowied&#378;"
  ]
  node [
    id 279
    label "wytw&#243;r"
  ]
  node [
    id 280
    label "koniektura"
  ]
  node [
    id 281
    label "preparacja"
  ]
  node [
    id 282
    label "ekscerpcja"
  ]
  node [
    id 283
    label "j&#281;zykowo"
  ]
  node [
    id 284
    label "obelga"
  ]
  node [
    id 285
    label "dzie&#322;o"
  ]
  node [
    id 286
    label "redakcja"
  ]
  node [
    id 287
    label "pomini&#281;cie"
  ]
  node [
    id 288
    label "czyj&#347;"
  ]
  node [
    id 289
    label "m&#261;&#380;"
  ]
  node [
    id 290
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 291
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 292
    label "po_koroniarsku"
  ]
  node [
    id 293
    label "m&#243;wienie"
  ]
  node [
    id 294
    label "rozumie&#263;"
  ]
  node [
    id 295
    label "komunikacja"
  ]
  node [
    id 296
    label "rozumienie"
  ]
  node [
    id 297
    label "m&#243;wi&#263;"
  ]
  node [
    id 298
    label "gramatyka"
  ]
  node [
    id 299
    label "address"
  ]
  node [
    id 300
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 301
    label "przet&#322;umaczenie"
  ]
  node [
    id 302
    label "tongue"
  ]
  node [
    id 303
    label "t&#322;umaczenie"
  ]
  node [
    id 304
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 305
    label "pismo"
  ]
  node [
    id 306
    label "zdolno&#347;&#263;"
  ]
  node [
    id 307
    label "fonetyka"
  ]
  node [
    id 308
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 309
    label "wokalizm"
  ]
  node [
    id 310
    label "s&#322;ownictwo"
  ]
  node [
    id 311
    label "konsonantyzm"
  ]
  node [
    id 312
    label "kod"
  ]
  node [
    id 313
    label "infest"
  ]
  node [
    id 314
    label "zmienia&#263;"
  ]
  node [
    id 315
    label "wytrzyma&#263;"
  ]
  node [
    id 316
    label "dostosowywa&#263;"
  ]
  node [
    id 317
    label "rozpowszechnia&#263;"
  ]
  node [
    id 318
    label "ponosi&#263;"
  ]
  node [
    id 319
    label "przemieszcza&#263;"
  ]
  node [
    id 320
    label "move"
  ]
  node [
    id 321
    label "przelatywa&#263;"
  ]
  node [
    id 322
    label "strzela&#263;"
  ]
  node [
    id 323
    label "kopiowa&#263;"
  ]
  node [
    id 324
    label "transfer"
  ]
  node [
    id 325
    label "umieszcza&#263;"
  ]
  node [
    id 326
    label "pocisk"
  ]
  node [
    id 327
    label "circulate"
  ]
  node [
    id 328
    label "estrange"
  ]
  node [
    id 329
    label "go"
  ]
  node [
    id 330
    label "rozprzestrzenia&#263;"
  ]
  node [
    id 331
    label "istota_&#380;ywa"
  ]
  node [
    id 332
    label "prokariont"
  ]
  node [
    id 333
    label "wirus"
  ]
  node [
    id 334
    label "program"
  ]
  node [
    id 335
    label "plechowiec"
  ]
  node [
    id 336
    label "bakterie"
  ]
  node [
    id 337
    label "ryzosfera"
  ]
  node [
    id 338
    label "beta-laktamaza"
  ]
  node [
    id 339
    label "enterotoksyna"
  ]
  node [
    id 340
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 341
    label "journey"
  ]
  node [
    id 342
    label "podbieg"
  ]
  node [
    id 343
    label "bezsilnikowy"
  ]
  node [
    id 344
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 345
    label "wylot"
  ]
  node [
    id 346
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 347
    label "drogowskaz"
  ]
  node [
    id 348
    label "nawierzchnia"
  ]
  node [
    id 349
    label "turystyka"
  ]
  node [
    id 350
    label "budowla"
  ]
  node [
    id 351
    label "spos&#243;b"
  ]
  node [
    id 352
    label "passage"
  ]
  node [
    id 353
    label "marszrutyzacja"
  ]
  node [
    id 354
    label "zbior&#243;wka"
  ]
  node [
    id 355
    label "ekskursja"
  ]
  node [
    id 356
    label "rajza"
  ]
  node [
    id 357
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 358
    label "ruch"
  ]
  node [
    id 359
    label "trasa"
  ]
  node [
    id 360
    label "wyb&#243;j"
  ]
  node [
    id 361
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 362
    label "ekwipunek"
  ]
  node [
    id 363
    label "korona_drogi"
  ]
  node [
    id 364
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 365
    label "pobocze"
  ]
  node [
    id 366
    label "seksualny"
  ]
  node [
    id 367
    label "erotyczny"
  ]
  node [
    id 368
    label "p&#322;ciowo"
  ]
  node [
    id 369
    label "seksualnie"
  ]
  node [
    id 370
    label "wyj&#261;tkowo&#347;&#263;"
  ]
  node [
    id 371
    label "erotyka"
  ]
  node [
    id 372
    label "podniecanie"
  ]
  node [
    id 373
    label "wzw&#243;d"
  ]
  node [
    id 374
    label "rozmna&#380;anie"
  ]
  node [
    id 375
    label "po&#380;&#261;danie"
  ]
  node [
    id 376
    label "imisja"
  ]
  node [
    id 377
    label "po&#380;ycie"
  ]
  node [
    id 378
    label "pozycja_misjonarska"
  ]
  node [
    id 379
    label "podnieci&#263;"
  ]
  node [
    id 380
    label "podnieca&#263;"
  ]
  node [
    id 381
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 382
    label "iloraz"
  ]
  node [
    id 383
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 384
    label "gra_wst&#281;pna"
  ]
  node [
    id 385
    label "podej&#347;cie"
  ]
  node [
    id 386
    label "cecha"
  ]
  node [
    id 387
    label "wyraz_skrajny"
  ]
  node [
    id 388
    label "numer"
  ]
  node [
    id 389
    label "ruch_frykcyjny"
  ]
  node [
    id 390
    label "baraszki"
  ]
  node [
    id 391
    label "powaga"
  ]
  node [
    id 392
    label "na_pieska"
  ]
  node [
    id 393
    label "z&#322;&#261;czenie"
  ]
  node [
    id 394
    label "relacja"
  ]
  node [
    id 395
    label "seks"
  ]
  node [
    id 396
    label "podniecenie"
  ]
  node [
    id 397
    label "homoseksualnie"
  ]
  node [
    id 398
    label "ozdabia&#263;"
  ]
  node [
    id 399
    label "dysgrafia"
  ]
  node [
    id 400
    label "prasa"
  ]
  node [
    id 401
    label "spell"
  ]
  node [
    id 402
    label "skryba"
  ]
  node [
    id 403
    label "donosi&#263;"
  ]
  node [
    id 404
    label "code"
  ]
  node [
    id 405
    label "dysortografia"
  ]
  node [
    id 406
    label "tworzy&#263;"
  ]
  node [
    id 407
    label "formu&#322;owa&#263;"
  ]
  node [
    id 408
    label "styl"
  ]
  node [
    id 409
    label "stawia&#263;"
  ]
  node [
    id 410
    label "okre&#347;lony"
  ]
  node [
    id 411
    label "jaki&#347;"
  ]
  node [
    id 412
    label "examine"
  ]
  node [
    id 413
    label "zrobi&#263;"
  ]
  node [
    id 414
    label "apolog"
  ]
  node [
    id 415
    label "morfing"
  ]
  node [
    id 416
    label "film"
  ]
  node [
    id 417
    label "mora&#322;"
  ]
  node [
    id 418
    label "opowie&#347;&#263;"
  ]
  node [
    id 419
    label "narrative"
  ]
  node [
    id 420
    label "g&#322;upstwo"
  ]
  node [
    id 421
    label "utw&#243;r"
  ]
  node [
    id 422
    label "Pok&#233;mon"
  ]
  node [
    id 423
    label "epika"
  ]
  node [
    id 424
    label "komfort"
  ]
  node [
    id 425
    label "sytuacja"
  ]
  node [
    id 426
    label "byd&#322;o"
  ]
  node [
    id 427
    label "zobo"
  ]
  node [
    id 428
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 429
    label "yakalo"
  ]
  node [
    id 430
    label "dzo"
  ]
  node [
    id 431
    label "nastawienie"
  ]
  node [
    id 432
    label "zawodowy"
  ]
  node [
    id 433
    label "tre&#347;ciwy"
  ]
  node [
    id 434
    label "rzetelny"
  ]
  node [
    id 435
    label "po_dziennikarsku"
  ]
  node [
    id 436
    label "dziennikarsko"
  ]
  node [
    id 437
    label "obiektywny"
  ]
  node [
    id 438
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 439
    label "wzorowy"
  ]
  node [
    id 440
    label "typowy"
  ]
  node [
    id 441
    label "tytu&#322;"
  ]
  node [
    id 442
    label "rozk&#322;ad&#243;wka"
  ]
  node [
    id 443
    label "czasopismo"
  ]
  node [
    id 444
    label "threaten"
  ]
  node [
    id 445
    label "zagrozi&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 9
  ]
  edge [
    source 6
    target 22
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 122
  ]
  edge [
    source 7
    target 123
  ]
  edge [
    source 7
    target 124
  ]
  edge [
    source 7
    target 125
  ]
  edge [
    source 7
    target 126
  ]
  edge [
    source 7
    target 127
  ]
  edge [
    source 7
    target 128
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 7
    target 130
  ]
  edge [
    source 7
    target 131
  ]
  edge [
    source 7
    target 132
  ]
  edge [
    source 7
    target 133
  ]
  edge [
    source 7
    target 134
  ]
  edge [
    source 7
    target 135
  ]
  edge [
    source 7
    target 136
  ]
  edge [
    source 7
    target 137
  ]
  edge [
    source 7
    target 138
  ]
  edge [
    source 7
    target 139
  ]
  edge [
    source 7
    target 140
  ]
  edge [
    source 7
    target 141
  ]
  edge [
    source 7
    target 142
  ]
  edge [
    source 7
    target 143
  ]
  edge [
    source 7
    target 144
  ]
  edge [
    source 7
    target 145
  ]
  edge [
    source 7
    target 146
  ]
  edge [
    source 7
    target 147
  ]
  edge [
    source 7
    target 148
  ]
  edge [
    source 7
    target 149
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 22
  ]
  edge [
    source 8
    target 23
  ]
  edge [
    source 8
    target 150
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 22
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 151
  ]
  edge [
    source 10
    target 152
  ]
  edge [
    source 10
    target 153
  ]
  edge [
    source 10
    target 154
  ]
  edge [
    source 10
    target 155
  ]
  edge [
    source 10
    target 156
  ]
  edge [
    source 10
    target 157
  ]
  edge [
    source 10
    target 158
  ]
  edge [
    source 10
    target 159
  ]
  edge [
    source 10
    target 160
  ]
  edge [
    source 10
    target 161
  ]
  edge [
    source 10
    target 162
  ]
  edge [
    source 10
    target 163
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 25
  ]
  edge [
    source 11
    target 26
  ]
  edge [
    source 11
    target 164
  ]
  edge [
    source 11
    target 28
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 165
  ]
  edge [
    source 12
    target 166
  ]
  edge [
    source 12
    target 167
  ]
  edge [
    source 12
    target 168
  ]
  edge [
    source 12
    target 169
  ]
  edge [
    source 12
    target 170
  ]
  edge [
    source 12
    target 171
  ]
  edge [
    source 12
    target 172
  ]
  edge [
    source 12
    target 173
  ]
  edge [
    source 12
    target 174
  ]
  edge [
    source 12
    target 175
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 34
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 176
  ]
  edge [
    source 15
    target 177
  ]
  edge [
    source 15
    target 178
  ]
  edge [
    source 15
    target 179
  ]
  edge [
    source 15
    target 180
  ]
  edge [
    source 15
    target 181
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 182
  ]
  edge [
    source 16
    target 183
  ]
  edge [
    source 16
    target 184
  ]
  edge [
    source 16
    target 185
  ]
  edge [
    source 16
    target 186
  ]
  edge [
    source 16
    target 187
  ]
  edge [
    source 16
    target 188
  ]
  edge [
    source 16
    target 189
  ]
  edge [
    source 16
    target 190
  ]
  edge [
    source 16
    target 191
  ]
  edge [
    source 16
    target 192
  ]
  edge [
    source 16
    target 193
  ]
  edge [
    source 16
    target 194
  ]
  edge [
    source 16
    target 195
  ]
  edge [
    source 16
    target 196
  ]
  edge [
    source 16
    target 197
  ]
  edge [
    source 16
    target 198
  ]
  edge [
    source 16
    target 199
  ]
  edge [
    source 16
    target 200
  ]
  edge [
    source 16
    target 201
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 202
  ]
  edge [
    source 17
    target 203
  ]
  edge [
    source 17
    target 204
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 205
  ]
  edge [
    source 18
    target 206
  ]
  edge [
    source 18
    target 207
  ]
  edge [
    source 18
    target 208
  ]
  edge [
    source 18
    target 32
  ]
  edge [
    source 19
    target 209
  ]
  edge [
    source 19
    target 210
  ]
  edge [
    source 19
    target 211
  ]
  edge [
    source 19
    target 212
  ]
  edge [
    source 19
    target 213
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 214
  ]
  edge [
    source 20
    target 215
  ]
  edge [
    source 20
    target 216
  ]
  edge [
    source 20
    target 217
  ]
  edge [
    source 20
    target 218
  ]
  edge [
    source 20
    target 219
  ]
  edge [
    source 20
    target 220
  ]
  edge [
    source 20
    target 221
  ]
  edge [
    source 20
    target 222
  ]
  edge [
    source 20
    target 223
  ]
  edge [
    source 20
    target 224
  ]
  edge [
    source 20
    target 225
  ]
  edge [
    source 20
    target 226
  ]
  edge [
    source 20
    target 227
  ]
  edge [
    source 20
    target 228
  ]
  edge [
    source 20
    target 229
  ]
  edge [
    source 20
    target 230
  ]
  edge [
    source 20
    target 231
  ]
  edge [
    source 20
    target 232
  ]
  edge [
    source 20
    target 233
  ]
  edge [
    source 20
    target 234
  ]
  edge [
    source 20
    target 235
  ]
  edge [
    source 20
    target 236
  ]
  edge [
    source 20
    target 237
  ]
  edge [
    source 20
    target 238
  ]
  edge [
    source 20
    target 239
  ]
  edge [
    source 20
    target 240
  ]
  edge [
    source 20
    target 241
  ]
  edge [
    source 20
    target 242
  ]
  edge [
    source 20
    target 243
  ]
  edge [
    source 20
    target 244
  ]
  edge [
    source 20
    target 245
  ]
  edge [
    source 20
    target 246
  ]
  edge [
    source 20
    target 247
  ]
  edge [
    source 20
    target 248
  ]
  edge [
    source 20
    target 249
  ]
  edge [
    source 20
    target 250
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 251
  ]
  edge [
    source 21
    target 252
  ]
  edge [
    source 22
    target 253
  ]
  edge [
    source 22
    target 254
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 255
  ]
  edge [
    source 23
    target 256
  ]
  edge [
    source 23
    target 257
  ]
  edge [
    source 23
    target 258
  ]
  edge [
    source 23
    target 259
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 260
  ]
  edge [
    source 25
    target 261
  ]
  edge [
    source 25
    target 262
  ]
  edge [
    source 25
    target 263
  ]
  edge [
    source 25
    target 264
  ]
  edge [
    source 25
    target 265
  ]
  edge [
    source 25
    target 266
  ]
  edge [
    source 25
    target 267
  ]
  edge [
    source 25
    target 268
  ]
  edge [
    source 25
    target 269
  ]
  edge [
    source 25
    target 31
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 270
  ]
  edge [
    source 26
    target 271
  ]
  edge [
    source 26
    target 272
  ]
  edge [
    source 26
    target 273
  ]
  edge [
    source 26
    target 274
  ]
  edge [
    source 26
    target 275
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 39
  ]
  edge [
    source 28
    target 276
  ]
  edge [
    source 28
    target 277
  ]
  edge [
    source 28
    target 278
  ]
  edge [
    source 28
    target 279
  ]
  edge [
    source 28
    target 280
  ]
  edge [
    source 28
    target 281
  ]
  edge [
    source 28
    target 282
  ]
  edge [
    source 28
    target 283
  ]
  edge [
    source 28
    target 284
  ]
  edge [
    source 28
    target 285
  ]
  edge [
    source 28
    target 286
  ]
  edge [
    source 28
    target 287
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 44
  ]
  edge [
    source 29
    target 45
  ]
  edge [
    source 29
    target 288
  ]
  edge [
    source 29
    target 289
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 278
  ]
  edge [
    source 30
    target 290
  ]
  edge [
    source 30
    target 291
  ]
  edge [
    source 30
    target 292
  ]
  edge [
    source 30
    target 293
  ]
  edge [
    source 30
    target 294
  ]
  edge [
    source 30
    target 295
  ]
  edge [
    source 30
    target 296
  ]
  edge [
    source 30
    target 297
  ]
  edge [
    source 30
    target 298
  ]
  edge [
    source 30
    target 299
  ]
  edge [
    source 30
    target 300
  ]
  edge [
    source 30
    target 301
  ]
  edge [
    source 30
    target 165
  ]
  edge [
    source 30
    target 302
  ]
  edge [
    source 30
    target 303
  ]
  edge [
    source 30
    target 304
  ]
  edge [
    source 30
    target 305
  ]
  edge [
    source 30
    target 306
  ]
  edge [
    source 30
    target 307
  ]
  edge [
    source 30
    target 308
  ]
  edge [
    source 30
    target 309
  ]
  edge [
    source 30
    target 310
  ]
  edge [
    source 30
    target 311
  ]
  edge [
    source 30
    target 312
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 313
  ]
  edge [
    source 31
    target 314
  ]
  edge [
    source 31
    target 315
  ]
  edge [
    source 31
    target 316
  ]
  edge [
    source 31
    target 317
  ]
  edge [
    source 31
    target 318
  ]
  edge [
    source 31
    target 319
  ]
  edge [
    source 31
    target 320
  ]
  edge [
    source 31
    target 321
  ]
  edge [
    source 31
    target 322
  ]
  edge [
    source 31
    target 323
  ]
  edge [
    source 31
    target 324
  ]
  edge [
    source 31
    target 325
  ]
  edge [
    source 31
    target 326
  ]
  edge [
    source 31
    target 327
  ]
  edge [
    source 31
    target 328
  ]
  edge [
    source 31
    target 329
  ]
  edge [
    source 31
    target 330
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 331
  ]
  edge [
    source 32
    target 332
  ]
  edge [
    source 32
    target 333
  ]
  edge [
    source 32
    target 334
  ]
  edge [
    source 32
    target 335
  ]
  edge [
    source 32
    target 336
  ]
  edge [
    source 32
    target 337
  ]
  edge [
    source 32
    target 338
  ]
  edge [
    source 32
    target 339
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 340
  ]
  edge [
    source 33
    target 341
  ]
  edge [
    source 33
    target 342
  ]
  edge [
    source 33
    target 343
  ]
  edge [
    source 33
    target 344
  ]
  edge [
    source 33
    target 345
  ]
  edge [
    source 33
    target 346
  ]
  edge [
    source 33
    target 347
  ]
  edge [
    source 33
    target 348
  ]
  edge [
    source 33
    target 349
  ]
  edge [
    source 33
    target 350
  ]
  edge [
    source 33
    target 351
  ]
  edge [
    source 33
    target 352
  ]
  edge [
    source 33
    target 353
  ]
  edge [
    source 33
    target 354
  ]
  edge [
    source 33
    target 355
  ]
  edge [
    source 33
    target 356
  ]
  edge [
    source 33
    target 357
  ]
  edge [
    source 33
    target 358
  ]
  edge [
    source 33
    target 359
  ]
  edge [
    source 33
    target 360
  ]
  edge [
    source 33
    target 361
  ]
  edge [
    source 33
    target 362
  ]
  edge [
    source 33
    target 363
  ]
  edge [
    source 33
    target 364
  ]
  edge [
    source 33
    target 365
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 366
  ]
  edge [
    source 34
    target 367
  ]
  edge [
    source 34
    target 368
  ]
  edge [
    source 34
    target 369
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 370
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 371
  ]
  edge [
    source 37
    target 372
  ]
  edge [
    source 37
    target 373
  ]
  edge [
    source 37
    target 374
  ]
  edge [
    source 37
    target 375
  ]
  edge [
    source 37
    target 376
  ]
  edge [
    source 37
    target 377
  ]
  edge [
    source 37
    target 378
  ]
  edge [
    source 37
    target 379
  ]
  edge [
    source 37
    target 380
  ]
  edge [
    source 37
    target 381
  ]
  edge [
    source 37
    target 382
  ]
  edge [
    source 37
    target 165
  ]
  edge [
    source 37
    target 383
  ]
  edge [
    source 37
    target 384
  ]
  edge [
    source 37
    target 385
  ]
  edge [
    source 37
    target 386
  ]
  edge [
    source 37
    target 387
  ]
  edge [
    source 37
    target 388
  ]
  edge [
    source 37
    target 389
  ]
  edge [
    source 37
    target 390
  ]
  edge [
    source 37
    target 391
  ]
  edge [
    source 37
    target 392
  ]
  edge [
    source 37
    target 393
  ]
  edge [
    source 37
    target 394
  ]
  edge [
    source 37
    target 395
  ]
  edge [
    source 37
    target 396
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 397
  ]
  edge [
    source 38
    target 367
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 398
  ]
  edge [
    source 39
    target 399
  ]
  edge [
    source 39
    target 400
  ]
  edge [
    source 39
    target 401
  ]
  edge [
    source 39
    target 402
  ]
  edge [
    source 39
    target 403
  ]
  edge [
    source 39
    target 404
  ]
  edge [
    source 39
    target 405
  ]
  edge [
    source 39
    target 273
  ]
  edge [
    source 39
    target 406
  ]
  edge [
    source 39
    target 407
  ]
  edge [
    source 39
    target 408
  ]
  edge [
    source 39
    target 409
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 48
  ]
  edge [
    source 40
    target 49
  ]
  edge [
    source 40
    target 410
  ]
  edge [
    source 40
    target 411
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 412
  ]
  edge [
    source 41
    target 413
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 414
  ]
  edge [
    source 42
    target 415
  ]
  edge [
    source 42
    target 416
  ]
  edge [
    source 42
    target 417
  ]
  edge [
    source 42
    target 418
  ]
  edge [
    source 42
    target 419
  ]
  edge [
    source 42
    target 420
  ]
  edge [
    source 42
    target 421
  ]
  edge [
    source 42
    target 422
  ]
  edge [
    source 42
    target 423
  ]
  edge [
    source 42
    target 424
  ]
  edge [
    source 42
    target 425
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 426
  ]
  edge [
    source 43
    target 427
  ]
  edge [
    source 43
    target 428
  ]
  edge [
    source 43
    target 429
  ]
  edge [
    source 43
    target 430
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 431
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 432
  ]
  edge [
    source 46
    target 433
  ]
  edge [
    source 46
    target 434
  ]
  edge [
    source 46
    target 435
  ]
  edge [
    source 46
    target 436
  ]
  edge [
    source 46
    target 437
  ]
  edge [
    source 46
    target 438
  ]
  edge [
    source 46
    target 439
  ]
  edge [
    source 46
    target 440
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 400
  ]
  edge [
    source 51
    target 286
  ]
  edge [
    source 51
    target 441
  ]
  edge [
    source 51
    target 442
  ]
  edge [
    source 51
    target 443
  ]
  edge [
    source 52
    target 444
  ]
  edge [
    source 52
    target 445
  ]
]
