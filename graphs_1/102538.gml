graph [
  maxDegree 324
  minDegree 1
  meanDegree 2.4094604582409462
  density 0.0017821453093498122
  graphCliqueNumber 7
  node [
    id 0
    label "czas"
    origin "text"
  ]
  node [
    id 1
    label "przedhistoryczny"
    origin "text"
  ]
  node [
    id 2
    label "obszar"
    origin "text"
  ]
  node [
    id 3
    label "tym"
    origin "text"
  ]
  node [
    id 4
    label "wydobywa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "obrabia&#263;"
    origin "text"
  ]
  node [
    id 6
    label "krzemie&#324;"
    origin "text"
  ]
  node [
    id 7
    label "wytapia&#263;"
    origin "text"
  ]
  node [
    id 8
    label "tak&#380;e"
    origin "text"
  ]
  node [
    id 9
    label "&#380;elazo"
    origin "text"
  ]
  node [
    id 10
    label "metr"
    origin "text"
  ]
  node [
    id 11
    label "nowa"
    origin "text"
  ]
  node [
    id 12
    label "s&#322;upia"
    origin "text"
  ]
  node [
    id 13
    label "gdzie"
    origin "text"
  ]
  node [
    id 14
    label "znale&#378;&#263;"
    origin "text"
  ]
  node [
    id 15
    label "piecowisko"
    origin "text"
  ]
  node [
    id 16
    label "z&#322;o&#380;one"
    origin "text"
  ]
  node [
    id 17
    label "piec"
    origin "text"
  ]
  node [
    id 18
    label "hutniczy"
    origin "text"
  ]
  node [
    id 19
    label "dymarka"
    origin "text"
  ]
  node [
    id 20
    label "tzw"
    origin "text"
  ]
  node [
    id 21
    label "&#380;u&#380;el"
    origin "text"
  ]
  node [
    id 22
    label "pierwotny"
    origin "text"
  ]
  node [
    id 23
    label "pochodzi&#263;"
    origin "text"
  ]
  node [
    id 24
    label "okres"
    origin "text"
  ]
  node [
    id 25
    label "versus"
    origin "text"
  ]
  node [
    id 26
    label "wiek"
    origin "text"
  ]
  node [
    id 27
    label "ksi&#261;dz"
    origin "text"
  ]
  node [
    id 28
    label "odbywa&#263;"
    origin "text"
  ]
  node [
    id 29
    label "si&#281;"
    origin "text"
  ]
  node [
    id 30
    label "tam"
    origin "text"
  ]
  node [
    id 31
    label "coroczny"
    origin "text"
  ]
  node [
    id 32
    label "&#347;wi&#281;tokrzyski"
    origin "text"
  ]
  node [
    id 33
    label "pierwsza"
    origin "text"
  ]
  node [
    id 34
    label "zak&#322;ad"
    origin "text"
  ]
  node [
    id 35
    label "przemys&#322;owy"
    origin "text"
  ]
  node [
    id 36
    label "powsta&#322;y"
    origin "text"
  ]
  node [
    id 37
    label "terenia"
    origin "text"
  ]
  node [
    id 38
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 39
    label "&#347;redniowiecze"
    origin "text"
  ]
  node [
    id 40
    label "rozwija&#263;"
    origin "text"
  ]
  node [
    id 41
    label "g&#243;rnictwo"
    origin "text"
  ]
  node [
    id 42
    label "hutnictwo"
    origin "text"
  ]
  node [
    id 43
    label "przetw&#243;rstwo"
    origin "text"
  ]
  node [
    id 44
    label "metal"
    origin "text"
  ]
  node [
    id 45
    label "mi&#281;dzy"
    origin "text"
  ]
  node [
    id 46
    label "inny"
    origin "text"
  ]
  node [
    id 47
    label "produkcja"
    origin "text"
  ]
  node [
    id 48
    label "bronia"
    origin "text"
  ]
  node [
    id 49
    label "ruda"
    origin "text"
  ]
  node [
    id 50
    label "o&#322;&#243;w"
    origin "text"
  ]
  node [
    id 51
    label "mied&#378;"
    origin "text"
  ]
  node [
    id 52
    label "srebro"
    origin "text"
  ]
  node [
    id 53
    label "polska"
    origin "text"
  ]
  node [
    id 54
    label "przedrozbiorowy"
    origin "text"
  ]
  node [
    id 55
    label "zag&#322;&#281;bie"
    origin "text"
  ]
  node [
    id 56
    label "staropolski"
    origin "text"
  ]
  node [
    id 57
    label "by&#263;"
    origin "text"
  ]
  node [
    id 58
    label "wa&#380;ny"
    origin "text"
  ]
  node [
    id 59
    label "okr&#261;g"
    origin "text"
  ]
  node [
    id 60
    label "kraj"
    origin "text"
  ]
  node [
    id 61
    label "xvi"
    origin "text"
  ]
  node [
    id 62
    label "funkcjonowa&#263;"
    origin "text"
  ]
  node [
    id 63
    label "przesz&#322;o"
    origin "text"
  ]
  node [
    id 64
    label "ku&#378;nica"
    origin "text"
  ]
  node [
    id 65
    label "xvii"
    origin "text"
  ]
  node [
    id 66
    label "wielki"
    origin "text"
  ]
  node [
    id 67
    label "samson"
    origin "text"
  ]
  node [
    id 68
    label "rocznik"
    origin "text"
  ]
  node [
    id 69
    label "tutaj"
    origin "text"
  ]
  node [
    id 70
    label "og&#243;lny"
    origin "text"
  ]
  node [
    id 71
    label "liczba"
    origin "text"
  ]
  node [
    id 72
    label "znajdowa&#263;"
    origin "text"
  ]
  node [
    id 73
    label "kiedy"
    origin "text"
  ]
  node [
    id 74
    label "wyczerpa&#263;"
    origin "text"
  ]
  node [
    id 75
    label "z&#322;o&#380;e"
    origin "text"
  ]
  node [
    id 76
    label "zacz&#261;&#263;"
    origin "text"
  ]
  node [
    id 77
    label "dominowa&#263;"
    origin "text"
  ]
  node [
    id 78
    label "granica"
    origin "text"
  ]
  node [
    id 79
    label "istnie&#263;"
    origin "text"
  ]
  node [
    id 80
    label "r&#243;wnie&#380;"
    origin "text"
  ]
  node [
    id 81
    label "o&#347;rodek"
    origin "text"
  ]
  node [
    id 82
    label "szk&#322;a"
    origin "text"
  ]
  node [
    id 83
    label "rok"
    origin "text"
  ]
  node [
    id 84
    label "dzia&#322;a&#263;"
    origin "text"
  ]
  node [
    id 85
    label "istniejacych"
    origin "text"
  ]
  node [
    id 86
    label "druga"
    origin "text"
  ]
  node [
    id 87
    label "po&#322;owa"
    origin "text"
  ]
  node [
    id 88
    label "xviii"
    origin "text"
  ]
  node [
    id 89
    label "przynie&#347;&#263;"
    origin "text"
  ]
  node [
    id 90
    label "zmiana"
    origin "text"
  ]
  node [
    id 91
    label "spos&#243;b"
    origin "text"
  ]
  node [
    id 92
    label "pozyskiwa&#263;"
    origin "text"
  ]
  node [
    id 93
    label "sur&#243;wka"
    origin "text"
  ]
  node [
    id 94
    label "wytop"
    origin "text"
  ]
  node [
    id 95
    label "oprze&#263;"
    origin "text"
  ]
  node [
    id 96
    label "wtedy"
    origin "text"
  ]
  node [
    id 97
    label "proces"
    origin "text"
  ]
  node [
    id 98
    label "wielkopiecowy"
    origin "text"
  ]
  node [
    id 99
    label "taka"
    origin "text"
  ]
  node [
    id 100
    label "technologia"
    origin "text"
  ]
  node [
    id 101
    label "znacznie"
    origin "text"
  ]
  node [
    id 102
    label "przewy&#380;sza&#263;"
    origin "text"
  ]
  node [
    id 103
    label "znana"
    origin "text"
  ]
  node [
    id 104
    label "dot&#261;d"
    origin "text"
  ]
  node [
    id 105
    label "metoda"
    origin "text"
  ]
  node [
    id 106
    label "stosowany"
    origin "text"
  ]
  node [
    id 107
    label "cho&#263;"
    origin "text"
  ]
  node [
    id 108
    label "nadal"
    origin "text"
  ]
  node [
    id 109
    label "pracuj&#261;cy"
    origin "text"
  ]
  node [
    id 110
    label "w&#281;giel"
    origin "text"
  ]
  node [
    id 111
    label "drzewny"
    origin "text"
  ]
  node [
    id 112
    label "stan"
    origin "text"
  ]
  node [
    id 113
    label "wyprodukowa&#263;"
    origin "text"
  ]
  node [
    id 114
    label "rocznie"
    origin "text"
  ]
  node [
    id 115
    label "ton"
    origin "text"
  ]
  node [
    id 116
    label "xix"
    origin "text"
  ]
  node [
    id 117
    label "przemys&#322;"
    origin "text"
  ]
  node [
    id 118
    label "metalowy"
    origin "text"
  ]
  node [
    id 119
    label "potrzeba"
    origin "text"
  ]
  node [
    id 120
    label "budowa&#263;"
    origin "text"
  ]
  node [
    id 121
    label "tartak"
    origin "text"
  ]
  node [
    id 122
    label "cegielnia"
    origin "text"
  ]
  node [
    id 123
    label "stanowi&#263;"
    origin "text"
  ]
  node [
    id 124
    label "bezpo&#347;redni"
    origin "text"
  ]
  node [
    id 125
    label "zaplecze"
    origin "text"
  ]
  node [
    id 126
    label "materia&#322;owo"
    origin "text"
  ]
  node [
    id 127
    label "budowlany"
    origin "text"
  ]
  node [
    id 128
    label "dla"
    origin "text"
  ]
  node [
    id 129
    label "okoliczny"
    origin "text"
  ]
  node [
    id 130
    label "metalurgiczny"
    origin "text"
  ]
  node [
    id 131
    label "nast&#261;pi&#263;"
    origin "text"
  ]
  node [
    id 132
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 133
    label "rozkwit"
    origin "text"
  ]
  node [
    id 134
    label "zwi&#261;za&#263;"
    origin "text"
  ]
  node [
    id 135
    label "dzia&#322;lno&#347;ci&#261;"
    origin "text"
  ]
  node [
    id 136
    label "stanis&#322;aw"
    origin "text"
  ]
  node [
    id 137
    label "staszic"
    origin "text"
  ]
  node [
    id 138
    label "franciszek"
    origin "text"
  ]
  node [
    id 139
    label "ksawery"
    origin "text"
  ]
  node [
    id 140
    label "drucki"
    origin "text"
  ]
  node [
    id 141
    label "lubecki"
    origin "text"
  ]
  node [
    id 142
    label "wed&#322;ug"
    origin "text"
  ]
  node [
    id 143
    label "plan"
    origin "text"
  ]
  node [
    id 144
    label "mia&#322;"
    origin "text"
  ]
  node [
    id 145
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 146
    label "wybudowa&#263;"
    origin "text"
  ]
  node [
    id 147
    label "wzd&#322;u&#380;"
    origin "text"
  ]
  node [
    id 148
    label "rzeka"
    origin "text"
  ]
  node [
    id 149
    label "kamienny"
    origin "text"
  ]
  node [
    id 150
    label "duha"
    origin "text"
  ]
  node [
    id 151
    label "zrealizowa&#263;"
    origin "text"
  ]
  node [
    id 152
    label "tylko"
    origin "text"
  ]
  node [
    id 153
    label "cz&#281;&#347;ciowo"
    origin "text"
  ]
  node [
    id 154
    label "czasokres"
  ]
  node [
    id 155
    label "trawienie"
  ]
  node [
    id 156
    label "kategoria_gramatyczna"
  ]
  node [
    id 157
    label "period"
  ]
  node [
    id 158
    label "odczyt"
  ]
  node [
    id 159
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 160
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 161
    label "chwila"
  ]
  node [
    id 162
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 163
    label "poprzedzenie"
  ]
  node [
    id 164
    label "koniugacja"
  ]
  node [
    id 165
    label "dzieje"
  ]
  node [
    id 166
    label "poprzedzi&#263;"
  ]
  node [
    id 167
    label "przep&#322;ywanie"
  ]
  node [
    id 168
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 169
    label "odwlekanie_si&#281;"
  ]
  node [
    id 170
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 171
    label "Zeitgeist"
  ]
  node [
    id 172
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 173
    label "okres_czasu"
  ]
  node [
    id 174
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 175
    label "schy&#322;ek"
  ]
  node [
    id 176
    label "czwarty_wymiar"
  ]
  node [
    id 177
    label "chronometria"
  ]
  node [
    id 178
    label "poprzedzanie"
  ]
  node [
    id 179
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 180
    label "pogoda"
  ]
  node [
    id 181
    label "zegar"
  ]
  node [
    id 182
    label "trawi&#263;"
  ]
  node [
    id 183
    label "pochodzenie"
  ]
  node [
    id 184
    label "poprzedza&#263;"
  ]
  node [
    id 185
    label "time_period"
  ]
  node [
    id 186
    label "rachuba_czasu"
  ]
  node [
    id 187
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 188
    label "czasoprzestrze&#324;"
  ]
  node [
    id 189
    label "laba"
  ]
  node [
    id 190
    label "stary"
  ]
  node [
    id 191
    label "historyczny"
  ]
  node [
    id 192
    label "przedpotopowy"
  ]
  node [
    id 193
    label "starodawny"
  ]
  node [
    id 194
    label "pas_planetoid"
  ]
  node [
    id 195
    label "wsch&#243;d"
  ]
  node [
    id 196
    label "Neogea"
  ]
  node [
    id 197
    label "holarktyka"
  ]
  node [
    id 198
    label "Rakowice"
  ]
  node [
    id 199
    label "Kosowo"
  ]
  node [
    id 200
    label "Syberia_Wschodnia"
  ]
  node [
    id 201
    label "wymiar"
  ]
  node [
    id 202
    label "p&#243;&#322;noc"
  ]
  node [
    id 203
    label "akrecja"
  ]
  node [
    id 204
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 205
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 206
    label "terytorium"
  ]
  node [
    id 207
    label "antroposfera"
  ]
  node [
    id 208
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 209
    label "po&#322;udnie"
  ]
  node [
    id 210
    label "zach&#243;d"
  ]
  node [
    id 211
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 212
    label "Olszanica"
  ]
  node [
    id 213
    label "Syberia_Zachodnia"
  ]
  node [
    id 214
    label "przestrze&#324;"
  ]
  node [
    id 215
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 216
    label "Ruda_Pabianicka"
  ]
  node [
    id 217
    label "Notogea"
  ]
  node [
    id 218
    label "&#321;&#281;g"
  ]
  node [
    id 219
    label "Antarktyka"
  ]
  node [
    id 220
    label "Piotrowo"
  ]
  node [
    id 221
    label "Zab&#322;ocie"
  ]
  node [
    id 222
    label "zakres"
  ]
  node [
    id 223
    label "miejsce"
  ]
  node [
    id 224
    label "Pow&#261;zki"
  ]
  node [
    id 225
    label "Arktyka"
  ]
  node [
    id 226
    label "zbi&#243;r"
  ]
  node [
    id 227
    label "Ludwin&#243;w"
  ]
  node [
    id 228
    label "Zabu&#380;e"
  ]
  node [
    id 229
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 230
    label "Kresy_Zachodnie"
  ]
  node [
    id 231
    label "eksploatowa&#263;"
  ]
  node [
    id 232
    label "ocala&#263;"
  ]
  node [
    id 233
    label "uzyskiwa&#263;"
  ]
  node [
    id 234
    label "wyjmowa&#263;"
  ]
  node [
    id 235
    label "dobywa&#263;"
  ]
  node [
    id 236
    label "wydostawa&#263;"
  ]
  node [
    id 237
    label "train"
  ]
  node [
    id 238
    label "excavate"
  ]
  node [
    id 239
    label "wydawa&#263;"
  ]
  node [
    id 240
    label "raise"
  ]
  node [
    id 241
    label "u&#380;ytkowa&#263;"
  ]
  node [
    id 242
    label "uwydatnia&#263;"
  ]
  node [
    id 243
    label "rabowa&#263;"
  ]
  node [
    id 244
    label "okrada&#263;"
  ]
  node [
    id 245
    label "obrabia&#263;_dup&#281;"
  ]
  node [
    id 246
    label "&#322;oi&#263;"
  ]
  node [
    id 247
    label "wyka&#324;cza&#263;"
  ]
  node [
    id 248
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 249
    label "work"
  ]
  node [
    id 250
    label "slur"
  ]
  node [
    id 251
    label "plotkowa&#263;"
  ]
  node [
    id 252
    label "poddawa&#263;"
  ]
  node [
    id 253
    label "krytykowa&#263;"
  ]
  node [
    id 254
    label "overcharge"
  ]
  node [
    id 255
    label "ska&#322;a_osadowa"
  ]
  node [
    id 256
    label "kurkowy_mechanizm_uderzeniowy"
  ]
  node [
    id 257
    label "roztapia&#263;"
  ]
  node [
    id 258
    label "give"
  ]
  node [
    id 259
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 260
    label "tworzy&#263;"
  ]
  node [
    id 261
    label "irons"
  ]
  node [
    id 262
    label "stop"
  ]
  node [
    id 263
    label "dymarstwo"
  ]
  node [
    id 264
    label "iron"
  ]
  node [
    id 265
    label "ferromagnetyk"
  ]
  node [
    id 266
    label "&#380;elazowiec"
  ]
  node [
    id 267
    label "mikroelement"
  ]
  node [
    id 268
    label "meter"
  ]
  node [
    id 269
    label "decymetr"
  ]
  node [
    id 270
    label "megabyte"
  ]
  node [
    id 271
    label "plon"
  ]
  node [
    id 272
    label "metrum"
  ]
  node [
    id 273
    label "dekametr"
  ]
  node [
    id 274
    label "jednostka_powierzchni"
  ]
  node [
    id 275
    label "uk&#322;ad_SI"
  ]
  node [
    id 276
    label "literaturoznawstwo"
  ]
  node [
    id 277
    label "wiersz"
  ]
  node [
    id 278
    label "gigametr"
  ]
  node [
    id 279
    label "miara"
  ]
  node [
    id 280
    label "nauczyciel"
  ]
  node [
    id 281
    label "kilometr_kwadratowy"
  ]
  node [
    id 282
    label "jednostka_metryczna"
  ]
  node [
    id 283
    label "jednostka_masy"
  ]
  node [
    id 284
    label "centymetr_kwadratowy"
  ]
  node [
    id 285
    label "gwiazda"
  ]
  node [
    id 286
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 287
    label "odzyska&#263;"
  ]
  node [
    id 288
    label "devise"
  ]
  node [
    id 289
    label "oceni&#263;"
  ]
  node [
    id 290
    label "znaj&#347;&#263;"
  ]
  node [
    id 291
    label "wymy&#347;li&#263;"
  ]
  node [
    id 292
    label "invent"
  ]
  node [
    id 293
    label "pozyska&#263;"
  ]
  node [
    id 294
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 295
    label "wykry&#263;"
  ]
  node [
    id 296
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 297
    label "dozna&#263;"
  ]
  node [
    id 298
    label "palenisko"
  ]
  node [
    id 299
    label "plewinka"
  ]
  node [
    id 300
    label "astrowce"
  ]
  node [
    id 301
    label "fajerka"
  ]
  node [
    id 302
    label "bole&#263;"
  ]
  node [
    id 303
    label "urz&#261;dzenie_przemys&#322;owe"
  ]
  node [
    id 304
    label "wzmacniacz_elektryczny"
  ]
  node [
    id 305
    label "ridicule"
  ]
  node [
    id 306
    label "popielnik"
  ]
  node [
    id 307
    label "nalepa"
  ]
  node [
    id 308
    label "przestron"
  ]
  node [
    id 309
    label "przyrz&#261;dza&#263;"
  ]
  node [
    id 310
    label "centralne_ogrzewanie"
  ]
  node [
    id 311
    label "furnace"
  ]
  node [
    id 312
    label "luft"
  ]
  node [
    id 313
    label "dra&#380;ni&#263;"
  ]
  node [
    id 314
    label "wypalacz"
  ]
  node [
    id 315
    label "ruszt"
  ]
  node [
    id 316
    label "kaflowy"
  ]
  node [
    id 317
    label "inculcate"
  ]
  node [
    id 318
    label "obmurze"
  ]
  node [
    id 319
    label "czopuch"
  ]
  node [
    id 320
    label "uszkadza&#263;"
  ]
  node [
    id 321
    label "urz&#261;dzenie"
  ]
  node [
    id 322
    label "hajcowanie"
  ]
  node [
    id 323
    label "Turniej_Gwiazdkowy"
  ]
  node [
    id 324
    label "popi&#243;&#322;"
  ]
  node [
    id 325
    label "sport_motorowy"
  ]
  node [
    id 326
    label "&#380;u&#380;lobeton"
  ]
  node [
    id 327
    label "produkt_uboczny"
  ]
  node [
    id 328
    label "podstawowy"
  ]
  node [
    id 329
    label "pradawny"
  ]
  node [
    id 330
    label "pocz&#261;tkowy"
  ]
  node [
    id 331
    label "naturalny"
  ]
  node [
    id 332
    label "g&#322;&#243;wny"
  ]
  node [
    id 333
    label "prymarnie"
  ]
  node [
    id 334
    label "dziki"
  ]
  node [
    id 335
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 336
    label "pierwotnie"
  ]
  node [
    id 337
    label "dziewiczy"
  ]
  node [
    id 338
    label "date"
  ]
  node [
    id 339
    label "str&#243;j"
  ]
  node [
    id 340
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 341
    label "spowodowa&#263;"
  ]
  node [
    id 342
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 343
    label "uda&#263;_si&#281;"
  ]
  node [
    id 344
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 345
    label "poby&#263;"
  ]
  node [
    id 346
    label "pop&#322;ywa&#263;"
  ]
  node [
    id 347
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 348
    label "wynika&#263;"
  ]
  node [
    id 349
    label "fall"
  ]
  node [
    id 350
    label "bolt"
  ]
  node [
    id 351
    label "paleogen"
  ]
  node [
    id 352
    label "spell"
  ]
  node [
    id 353
    label "prekambr"
  ]
  node [
    id 354
    label "jura"
  ]
  node [
    id 355
    label "interstadia&#322;"
  ]
  node [
    id 356
    label "jednostka_geologiczna"
  ]
  node [
    id 357
    label "izochronizm"
  ]
  node [
    id 358
    label "okres_noachijski"
  ]
  node [
    id 359
    label "orosir"
  ]
  node [
    id 360
    label "kreda"
  ]
  node [
    id 361
    label "sten"
  ]
  node [
    id 362
    label "drugorz&#281;d"
  ]
  node [
    id 363
    label "semester"
  ]
  node [
    id 364
    label "trzeciorz&#281;d"
  ]
  node [
    id 365
    label "poprzednik"
  ]
  node [
    id 366
    label "ordowik"
  ]
  node [
    id 367
    label "karbon"
  ]
  node [
    id 368
    label "trias"
  ]
  node [
    id 369
    label "kalim"
  ]
  node [
    id 370
    label "stater"
  ]
  node [
    id 371
    label "era"
  ]
  node [
    id 372
    label "cykl"
  ]
  node [
    id 373
    label "p&#243;&#322;okres"
  ]
  node [
    id 374
    label "czwartorz&#281;d"
  ]
  node [
    id 375
    label "pulsacja"
  ]
  node [
    id 376
    label "okres_amazo&#324;ski"
  ]
  node [
    id 377
    label "kambr"
  ]
  node [
    id 378
    label "nast&#281;pnik"
  ]
  node [
    id 379
    label "kriogen"
  ]
  node [
    id 380
    label "glacja&#322;"
  ]
  node [
    id 381
    label "fala"
  ]
  node [
    id 382
    label "riak"
  ]
  node [
    id 383
    label "okres_hesperyjski"
  ]
  node [
    id 384
    label "sylur"
  ]
  node [
    id 385
    label "dewon"
  ]
  node [
    id 386
    label "ciota"
  ]
  node [
    id 387
    label "epoka"
  ]
  node [
    id 388
    label "pierwszorz&#281;d"
  ]
  node [
    id 389
    label "okres_halsztacki"
  ]
  node [
    id 390
    label "ektas"
  ]
  node [
    id 391
    label "zdanie"
  ]
  node [
    id 392
    label "condition"
  ]
  node [
    id 393
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 394
    label "rok_akademicki"
  ]
  node [
    id 395
    label "wapie&#324;_muszlowy"
  ]
  node [
    id 396
    label "postglacja&#322;"
  ]
  node [
    id 397
    label "faza"
  ]
  node [
    id 398
    label "proces_fizjologiczny"
  ]
  node [
    id 399
    label "ediakar"
  ]
  node [
    id 400
    label "ruch_drgaj&#261;cy"
  ]
  node [
    id 401
    label "perm"
  ]
  node [
    id 402
    label "rok_szkolny"
  ]
  node [
    id 403
    label "neogen"
  ]
  node [
    id 404
    label "sider"
  ]
  node [
    id 405
    label "flow"
  ]
  node [
    id 406
    label "podokres"
  ]
  node [
    id 407
    label "preglacja&#322;"
  ]
  node [
    id 408
    label "retoryka"
  ]
  node [
    id 409
    label "choroba_przyrodzona"
  ]
  node [
    id 410
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 411
    label "cecha"
  ]
  node [
    id 412
    label "long_time"
  ]
  node [
    id 413
    label "choroba_wieku"
  ]
  node [
    id 414
    label "chron"
  ]
  node [
    id 415
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 416
    label "klecha"
  ]
  node [
    id 417
    label "eklezjasta"
  ]
  node [
    id 418
    label "rozgrzeszanie"
  ]
  node [
    id 419
    label "duszpasterstwo"
  ]
  node [
    id 420
    label "rozgrzesza&#263;"
  ]
  node [
    id 421
    label "duchowny"
  ]
  node [
    id 422
    label "ksi&#281;&#380;a"
  ]
  node [
    id 423
    label "kap&#322;an"
  ]
  node [
    id 424
    label "kol&#281;da"
  ]
  node [
    id 425
    label "seminarzysta"
  ]
  node [
    id 426
    label "pasterz"
  ]
  node [
    id 427
    label "uczestniczy&#263;"
  ]
  node [
    id 428
    label "przechodzi&#263;"
  ]
  node [
    id 429
    label "hold"
  ]
  node [
    id 430
    label "tu"
  ]
  node [
    id 431
    label "cykliczny"
  ]
  node [
    id 432
    label "corocznie"
  ]
  node [
    id 433
    label "famu&#322;a"
  ]
  node [
    id 434
    label "godzina"
  ]
  node [
    id 435
    label "czyn"
  ]
  node [
    id 436
    label "company"
  ]
  node [
    id 437
    label "zak&#322;adka"
  ]
  node [
    id 438
    label "firma"
  ]
  node [
    id 439
    label "instytut"
  ]
  node [
    id 440
    label "wyko&#324;czenie"
  ]
  node [
    id 441
    label "jednostka_organizacyjna"
  ]
  node [
    id 442
    label "umowa"
  ]
  node [
    id 443
    label "instytucja"
  ]
  node [
    id 444
    label "miejsce_pracy"
  ]
  node [
    id 445
    label "przemys&#322;owo"
  ]
  node [
    id 446
    label "masowy"
  ]
  node [
    id 447
    label "korabnik"
  ]
  node [
    id 448
    label "gotyk"
  ]
  node [
    id 449
    label "podesta"
  ]
  node [
    id 450
    label "Imperium_Karoli&#324;skie"
  ]
  node [
    id 451
    label "kompozycja"
  ]
  node [
    id 452
    label "mediewistyka"
  ]
  node [
    id 453
    label "protorenesans"
  ]
  node [
    id 454
    label "mamotrept"
  ]
  node [
    id 455
    label "rozstawia&#263;"
  ]
  node [
    id 456
    label "puszcza&#263;"
  ]
  node [
    id 457
    label "rozk&#322;ada&#263;"
  ]
  node [
    id 458
    label "dopowiada&#263;"
  ]
  node [
    id 459
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 460
    label "omawia&#263;"
  ]
  node [
    id 461
    label "rozpakowywa&#263;"
  ]
  node [
    id 462
    label "zwi&#281;ksza&#263;"
  ]
  node [
    id 463
    label "inflate"
  ]
  node [
    id 464
    label "stawia&#263;"
  ]
  node [
    id 465
    label "przemys&#322;_ci&#281;&#380;ki"
  ]
  node [
    id 466
    label "wydobycie"
  ]
  node [
    id 467
    label "wiertnictwo"
  ]
  node [
    id 468
    label "wydobywanie"
  ]
  node [
    id 469
    label "krzeska"
  ]
  node [
    id 470
    label "solnictwo"
  ]
  node [
    id 471
    label "obrywak"
  ]
  node [
    id 472
    label "zgarniacz"
  ]
  node [
    id 473
    label "aerologia_g&#243;rnicza"
  ]
  node [
    id 474
    label "przesyp"
  ]
  node [
    id 475
    label "rozpierak"
  ]
  node [
    id 476
    label "&#322;adownik"
  ]
  node [
    id 477
    label "wydoby&#263;"
  ]
  node [
    id 478
    label "odstrzeliwa&#263;"
  ]
  node [
    id 479
    label "odstrzeliwanie"
  ]
  node [
    id 480
    label "wcinka"
  ]
  node [
    id 481
    label "nauka"
  ]
  node [
    id 482
    label "stalownictwo"
  ]
  node [
    id 483
    label "uzysk"
  ]
  node [
    id 484
    label "lateryt"
  ]
  node [
    id 485
    label "g&#281;&#347;"
  ]
  node [
    id 486
    label "walcownictwo"
  ]
  node [
    id 487
    label "walcowa&#263;"
  ]
  node [
    id 488
    label "wsad"
  ]
  node [
    id 489
    label "gardziel"
  ]
  node [
    id 490
    label "metallurgy"
  ]
  node [
    id 491
    label "ta&#347;ma"
  ]
  node [
    id 492
    label "metalurgia"
  ]
  node [
    id 493
    label "ga&#322;&#261;&#378;_przemys&#322;u"
  ]
  node [
    id 494
    label "cz&#322;owiek"
  ]
  node [
    id 495
    label "odlewalnia"
  ]
  node [
    id 496
    label "rock"
  ]
  node [
    id 497
    label "pieszczocha"
  ]
  node [
    id 498
    label "orygina&#322;"
  ]
  node [
    id 499
    label "metallic_element"
  ]
  node [
    id 500
    label "przeci&#261;gn&#261;&#263;"
  ]
  node [
    id 501
    label "pogo"
  ]
  node [
    id 502
    label "pierwiastek"
  ]
  node [
    id 503
    label "przebijarka"
  ]
  node [
    id 504
    label "ku&#263;"
  ]
  node [
    id 505
    label "sk&#243;ra"
  ]
  node [
    id 506
    label "przeci&#261;gni&#281;cie"
  ]
  node [
    id 507
    label "pi&#243;ra"
  ]
  node [
    id 508
    label "wytrawialnia"
  ]
  node [
    id 509
    label "wytrawia&#263;"
  ]
  node [
    id 510
    label "kuc"
  ]
  node [
    id 511
    label "fan"
  ]
  node [
    id 512
    label "kucie"
  ]
  node [
    id 513
    label "przedstawiciel"
  ]
  node [
    id 514
    label "pogowa&#263;"
  ]
  node [
    id 515
    label "topialnia"
  ]
  node [
    id 516
    label "naszywka"
  ]
  node [
    id 517
    label "kolejny"
  ]
  node [
    id 518
    label "inaczej"
  ]
  node [
    id 519
    label "r&#243;&#380;ny"
  ]
  node [
    id 520
    label "inszy"
  ]
  node [
    id 521
    label "osobno"
  ]
  node [
    id 522
    label "tingel-tangel"
  ]
  node [
    id 523
    label "odtworzenie"
  ]
  node [
    id 524
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 525
    label "realizacja"
  ]
  node [
    id 526
    label "monta&#380;"
  ]
  node [
    id 527
    label "rozw&#243;j"
  ]
  node [
    id 528
    label "fabrication"
  ]
  node [
    id 529
    label "kreacja"
  ]
  node [
    id 530
    label "dorobek"
  ]
  node [
    id 531
    label "wyda&#263;"
  ]
  node [
    id 532
    label "impreza"
  ]
  node [
    id 533
    label "postprodukcja"
  ]
  node [
    id 534
    label "numer"
  ]
  node [
    id 535
    label "kooperowa&#263;"
  ]
  node [
    id 536
    label "creation"
  ]
  node [
    id 537
    label "trema"
  ]
  node [
    id 538
    label "product"
  ]
  node [
    id 539
    label "performance"
  ]
  node [
    id 540
    label "aglomerownia"
  ]
  node [
    id 541
    label "kopalina_podstawowa"
  ]
  node [
    id 542
    label "wypa&#322;ek"
  ]
  node [
    id 543
    label "pra&#380;alnia"
  ]
  node [
    id 544
    label "ore"
  ]
  node [
    id 545
    label "atakamit"
  ]
  node [
    id 546
    label "w&#281;glowiec"
  ]
  node [
    id 547
    label "przedmiot"
  ]
  node [
    id 548
    label "metal_kolorowy"
  ]
  node [
    id 549
    label "ci&#281;&#380;ar"
  ]
  node [
    id 550
    label "lead"
  ]
  node [
    id 551
    label "metal_p&#243;&#322;szlachetny"
  ]
  node [
    id 552
    label "miedziowiec"
  ]
  node [
    id 553
    label "rudo&#347;&#263;"
  ]
  node [
    id 554
    label "kopr"
  ]
  node [
    id 555
    label "kotlarstwo"
  ]
  node [
    id 556
    label "silver"
  ]
  node [
    id 557
    label "katalizator"
  ]
  node [
    id 558
    label "wyr&#243;b"
  ]
  node [
    id 559
    label "po&#322;ysk"
  ]
  node [
    id 560
    label "metal_szlachetny"
  ]
  node [
    id 561
    label "silver_medal"
  ]
  node [
    id 562
    label "pieni&#261;dze"
  ]
  node [
    id 563
    label "medal"
  ]
  node [
    id 564
    label "barwno&#347;&#263;"
  ]
  node [
    id 565
    label "kolor"
  ]
  node [
    id 566
    label "region"
  ]
  node [
    id 567
    label "skupisko"
  ]
  node [
    id 568
    label "kopalnia"
  ]
  node [
    id 569
    label "dawny"
  ]
  node [
    id 570
    label "chmiel"
  ]
  node [
    id 571
    label "polski"
  ]
  node [
    id 572
    label "staros&#322;owia&#324;ski"
  ]
  node [
    id 573
    label "arumszalc"
  ]
  node [
    id 574
    label "po_staropolsku"
  ]
  node [
    id 575
    label "j&#281;zyk_polski"
  ]
  node [
    id 576
    label "si&#281;ga&#263;"
  ]
  node [
    id 577
    label "trwa&#263;"
  ]
  node [
    id 578
    label "obecno&#347;&#263;"
  ]
  node [
    id 579
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 580
    label "stand"
  ]
  node [
    id 581
    label "mie&#263;_miejsce"
  ]
  node [
    id 582
    label "chodzi&#263;"
  ]
  node [
    id 583
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 584
    label "equal"
  ]
  node [
    id 585
    label "silny"
  ]
  node [
    id 586
    label "wa&#380;nie"
  ]
  node [
    id 587
    label "eksponowany"
  ]
  node [
    id 588
    label "istotnie"
  ]
  node [
    id 589
    label "znaczny"
  ]
  node [
    id 590
    label "dobry"
  ]
  node [
    id 591
    label "wynios&#322;y"
  ]
  node [
    id 592
    label "dono&#347;ny"
  ]
  node [
    id 593
    label "&#322;uk"
  ]
  node [
    id 594
    label "circumference"
  ]
  node [
    id 595
    label "figura_p&#322;aska"
  ]
  node [
    id 596
    label "circle"
  ]
  node [
    id 597
    label "figura_geometryczna"
  ]
  node [
    id 598
    label "ko&#322;o"
  ]
  node [
    id 599
    label "Skandynawia"
  ]
  node [
    id 600
    label "Filipiny"
  ]
  node [
    id 601
    label "Rwanda"
  ]
  node [
    id 602
    label "Kaukaz"
  ]
  node [
    id 603
    label "Kaszmir"
  ]
  node [
    id 604
    label "Toskania"
  ]
  node [
    id 605
    label "Yorkshire"
  ]
  node [
    id 606
    label "&#321;emkowszczyzna"
  ]
  node [
    id 607
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 608
    label "Monako"
  ]
  node [
    id 609
    label "Amhara"
  ]
  node [
    id 610
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 611
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 612
    label "Lombardia"
  ]
  node [
    id 613
    label "Korea"
  ]
  node [
    id 614
    label "Kalabria"
  ]
  node [
    id 615
    label "Ghana"
  ]
  node [
    id 616
    label "Czarnog&#243;ra"
  ]
  node [
    id 617
    label "Tyrol"
  ]
  node [
    id 618
    label "Malawi"
  ]
  node [
    id 619
    label "Indonezja"
  ]
  node [
    id 620
    label "Bu&#322;garia"
  ]
  node [
    id 621
    label "Nauru"
  ]
  node [
    id 622
    label "Kenia"
  ]
  node [
    id 623
    label "Pamir"
  ]
  node [
    id 624
    label "Kambod&#380;a"
  ]
  node [
    id 625
    label "Lubelszczyzna"
  ]
  node [
    id 626
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 627
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 628
    label "Mali"
  ]
  node [
    id 629
    label "&#379;ywiecczyzna"
  ]
  node [
    id 630
    label "Austria"
  ]
  node [
    id 631
    label "interior"
  ]
  node [
    id 632
    label "Europa_Wschodnia"
  ]
  node [
    id 633
    label "Armenia"
  ]
  node [
    id 634
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 635
    label "Fid&#380;i"
  ]
  node [
    id 636
    label "Tuwalu"
  ]
  node [
    id 637
    label "Zabajkale"
  ]
  node [
    id 638
    label "Etiopia"
  ]
  node [
    id 639
    label "Malta"
  ]
  node [
    id 640
    label "Malezja"
  ]
  node [
    id 641
    label "Kaszuby"
  ]
  node [
    id 642
    label "Bo&#347;nia"
  ]
  node [
    id 643
    label "Noworosja"
  ]
  node [
    id 644
    label "Grenada"
  ]
  node [
    id 645
    label "Tad&#380;ykistan"
  ]
  node [
    id 646
    label "Ba&#322;kany"
  ]
  node [
    id 647
    label "Wehrlen"
  ]
  node [
    id 648
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 649
    label "Anglia"
  ]
  node [
    id 650
    label "Kielecczyzna"
  ]
  node [
    id 651
    label "Rumunia"
  ]
  node [
    id 652
    label "Pomorze_Zachodnie"
  ]
  node [
    id 653
    label "Maroko"
  ]
  node [
    id 654
    label "Bhutan"
  ]
  node [
    id 655
    label "Opolskie"
  ]
  node [
    id 656
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 657
    label "Ko&#322;yma"
  ]
  node [
    id 658
    label "Oksytania"
  ]
  node [
    id 659
    label "S&#322;owacja"
  ]
  node [
    id 660
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 661
    label "Seszele"
  ]
  node [
    id 662
    label "Syjon"
  ]
  node [
    id 663
    label "Kuwejt"
  ]
  node [
    id 664
    label "Arabia_Saudyjska"
  ]
  node [
    id 665
    label "Kociewie"
  ]
  node [
    id 666
    label "Ekwador"
  ]
  node [
    id 667
    label "Kanada"
  ]
  node [
    id 668
    label "ziemia"
  ]
  node [
    id 669
    label "Japonia"
  ]
  node [
    id 670
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 671
    label "Hiszpania"
  ]
  node [
    id 672
    label "Wyspy_Marshalla"
  ]
  node [
    id 673
    label "Botswana"
  ]
  node [
    id 674
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 675
    label "D&#380;ibuti"
  ]
  node [
    id 676
    label "Huculszczyzna"
  ]
  node [
    id 677
    label "Wietnam"
  ]
  node [
    id 678
    label "Egipt"
  ]
  node [
    id 679
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 680
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 681
    label "Burkina_Faso"
  ]
  node [
    id 682
    label "Bawaria"
  ]
  node [
    id 683
    label "Niemcy"
  ]
  node [
    id 684
    label "Khitai"
  ]
  node [
    id 685
    label "Macedonia"
  ]
  node [
    id 686
    label "Albania"
  ]
  node [
    id 687
    label "Madagaskar"
  ]
  node [
    id 688
    label "Bahrajn"
  ]
  node [
    id 689
    label "Jemen"
  ]
  node [
    id 690
    label "Lesoto"
  ]
  node [
    id 691
    label "Maghreb"
  ]
  node [
    id 692
    label "Samoa"
  ]
  node [
    id 693
    label "Andora"
  ]
  node [
    id 694
    label "Bory_Tucholskie"
  ]
  node [
    id 695
    label "Chiny"
  ]
  node [
    id 696
    label "Europa_Zachodnia"
  ]
  node [
    id 697
    label "Cypr"
  ]
  node [
    id 698
    label "Wielka_Brytania"
  ]
  node [
    id 699
    label "Kerala"
  ]
  node [
    id 700
    label "Podhale"
  ]
  node [
    id 701
    label "Kabylia"
  ]
  node [
    id 702
    label "Ukraina"
  ]
  node [
    id 703
    label "Paragwaj"
  ]
  node [
    id 704
    label "Trynidad_i_Tobago"
  ]
  node [
    id 705
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 706
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 707
    label "Ma&#322;opolska"
  ]
  node [
    id 708
    label "Polesie"
  ]
  node [
    id 709
    label "Liguria"
  ]
  node [
    id 710
    label "Libia"
  ]
  node [
    id 711
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 712
    label "&#321;&#243;dzkie"
  ]
  node [
    id 713
    label "Surinam"
  ]
  node [
    id 714
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 715
    label "Palestyna"
  ]
  node [
    id 716
    label "Australia"
  ]
  node [
    id 717
    label "Nigeria"
  ]
  node [
    id 718
    label "Honduras"
  ]
  node [
    id 719
    label "Bojkowszczyzna"
  ]
  node [
    id 720
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 721
    label "Karaiby"
  ]
  node [
    id 722
    label "Bangladesz"
  ]
  node [
    id 723
    label "Peru"
  ]
  node [
    id 724
    label "Kazachstan"
  ]
  node [
    id 725
    label "USA"
  ]
  node [
    id 726
    label "Irak"
  ]
  node [
    id 727
    label "Nepal"
  ]
  node [
    id 728
    label "S&#261;decczyzna"
  ]
  node [
    id 729
    label "Sudan"
  ]
  node [
    id 730
    label "Sand&#380;ak"
  ]
  node [
    id 731
    label "Nadrenia"
  ]
  node [
    id 732
    label "San_Marino"
  ]
  node [
    id 733
    label "Burundi"
  ]
  node [
    id 734
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 735
    label "Dominikana"
  ]
  node [
    id 736
    label "Komory"
  ]
  node [
    id 737
    label "Zakarpacie"
  ]
  node [
    id 738
    label "Gwatemala"
  ]
  node [
    id 739
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 740
    label "Zag&#243;rze"
  ]
  node [
    id 741
    label "Andaluzja"
  ]
  node [
    id 742
    label "granica_pa&#324;stwa"
  ]
  node [
    id 743
    label "Turkiestan"
  ]
  node [
    id 744
    label "Naddniestrze"
  ]
  node [
    id 745
    label "Hercegowina"
  ]
  node [
    id 746
    label "Brunei"
  ]
  node [
    id 747
    label "Iran"
  ]
  node [
    id 748
    label "jednostka_administracyjna"
  ]
  node [
    id 749
    label "Zimbabwe"
  ]
  node [
    id 750
    label "Namibia"
  ]
  node [
    id 751
    label "Meksyk"
  ]
  node [
    id 752
    label "Lotaryngia"
  ]
  node [
    id 753
    label "Kamerun"
  ]
  node [
    id 754
    label "Opolszczyzna"
  ]
  node [
    id 755
    label "Afryka_Wschodnia"
  ]
  node [
    id 756
    label "Szlezwik"
  ]
  node [
    id 757
    label "Somalia"
  ]
  node [
    id 758
    label "Angola"
  ]
  node [
    id 759
    label "Gabon"
  ]
  node [
    id 760
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 761
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 762
    label "Mozambik"
  ]
  node [
    id 763
    label "Tajwan"
  ]
  node [
    id 764
    label "Tunezja"
  ]
  node [
    id 765
    label "Nowa_Zelandia"
  ]
  node [
    id 766
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 767
    label "Podbeskidzie"
  ]
  node [
    id 768
    label "Liban"
  ]
  node [
    id 769
    label "Jordania"
  ]
  node [
    id 770
    label "Tonga"
  ]
  node [
    id 771
    label "Czad"
  ]
  node [
    id 772
    label "Liberia"
  ]
  node [
    id 773
    label "Gwinea"
  ]
  node [
    id 774
    label "Belize"
  ]
  node [
    id 775
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 776
    label "Mazowsze"
  ]
  node [
    id 777
    label "&#321;otwa"
  ]
  node [
    id 778
    label "Syria"
  ]
  node [
    id 779
    label "Benin"
  ]
  node [
    id 780
    label "Afryka_Zachodnia"
  ]
  node [
    id 781
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 782
    label "Dominika"
  ]
  node [
    id 783
    label "Antigua_i_Barbuda"
  ]
  node [
    id 784
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 785
    label "Hanower"
  ]
  node [
    id 786
    label "Galicja"
  ]
  node [
    id 787
    label "Szkocja"
  ]
  node [
    id 788
    label "Walia"
  ]
  node [
    id 789
    label "Afganistan"
  ]
  node [
    id 790
    label "Kiribati"
  ]
  node [
    id 791
    label "W&#322;ochy"
  ]
  node [
    id 792
    label "Szwajcaria"
  ]
  node [
    id 793
    label "Powi&#347;le"
  ]
  node [
    id 794
    label "Sahara_Zachodnia"
  ]
  node [
    id 795
    label "Chorwacja"
  ]
  node [
    id 796
    label "Tajlandia"
  ]
  node [
    id 797
    label "Salwador"
  ]
  node [
    id 798
    label "Bahamy"
  ]
  node [
    id 799
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 800
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 801
    label "Zamojszczyzna"
  ]
  node [
    id 802
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 803
    label "S&#322;owenia"
  ]
  node [
    id 804
    label "Gambia"
  ]
  node [
    id 805
    label "Kujawy"
  ]
  node [
    id 806
    label "Urugwaj"
  ]
  node [
    id 807
    label "Podlasie"
  ]
  node [
    id 808
    label "Zair"
  ]
  node [
    id 809
    label "Erytrea"
  ]
  node [
    id 810
    label "Laponia"
  ]
  node [
    id 811
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 812
    label "Umbria"
  ]
  node [
    id 813
    label "Rosja"
  ]
  node [
    id 814
    label "Uganda"
  ]
  node [
    id 815
    label "Niger"
  ]
  node [
    id 816
    label "Mauritius"
  ]
  node [
    id 817
    label "Turkmenistan"
  ]
  node [
    id 818
    label "Turcja"
  ]
  node [
    id 819
    label "Mezoameryka"
  ]
  node [
    id 820
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 821
    label "Irlandia"
  ]
  node [
    id 822
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 823
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 824
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 825
    label "Gwinea_Bissau"
  ]
  node [
    id 826
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 827
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 828
    label "Kurdystan"
  ]
  node [
    id 829
    label "Belgia"
  ]
  node [
    id 830
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 831
    label "Palau"
  ]
  node [
    id 832
    label "Barbados"
  ]
  node [
    id 833
    label "Chile"
  ]
  node [
    id 834
    label "Wenezuela"
  ]
  node [
    id 835
    label "W&#281;gry"
  ]
  node [
    id 836
    label "Argentyna"
  ]
  node [
    id 837
    label "Kolumbia"
  ]
  node [
    id 838
    label "Kampania"
  ]
  node [
    id 839
    label "Armagnac"
  ]
  node [
    id 840
    label "Sierra_Leone"
  ]
  node [
    id 841
    label "Azerbejd&#380;an"
  ]
  node [
    id 842
    label "Kongo"
  ]
  node [
    id 843
    label "Polinezja"
  ]
  node [
    id 844
    label "Warmia"
  ]
  node [
    id 845
    label "Pakistan"
  ]
  node [
    id 846
    label "Liechtenstein"
  ]
  node [
    id 847
    label "Wielkopolska"
  ]
  node [
    id 848
    label "Nikaragua"
  ]
  node [
    id 849
    label "Senegal"
  ]
  node [
    id 850
    label "brzeg"
  ]
  node [
    id 851
    label "Bordeaux"
  ]
  node [
    id 852
    label "Lauda"
  ]
  node [
    id 853
    label "Indie"
  ]
  node [
    id 854
    label "Mazury"
  ]
  node [
    id 855
    label "Suazi"
  ]
  node [
    id 856
    label "Polska"
  ]
  node [
    id 857
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 858
    label "Algieria"
  ]
  node [
    id 859
    label "Jamajka"
  ]
  node [
    id 860
    label "Timor_Wschodni"
  ]
  node [
    id 861
    label "Oceania"
  ]
  node [
    id 862
    label "Kostaryka"
  ]
  node [
    id 863
    label "Podkarpacie"
  ]
  node [
    id 864
    label "Lasko"
  ]
  node [
    id 865
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 866
    label "Kuba"
  ]
  node [
    id 867
    label "Mauretania"
  ]
  node [
    id 868
    label "Amazonia"
  ]
  node [
    id 869
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 870
    label "Portoryko"
  ]
  node [
    id 871
    label "Brazylia"
  ]
  node [
    id 872
    label "Mo&#322;dawia"
  ]
  node [
    id 873
    label "organizacja"
  ]
  node [
    id 874
    label "Litwa"
  ]
  node [
    id 875
    label "Kirgistan"
  ]
  node [
    id 876
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 877
    label "Izrael"
  ]
  node [
    id 878
    label "Grecja"
  ]
  node [
    id 879
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 880
    label "Kurpie"
  ]
  node [
    id 881
    label "Holandia"
  ]
  node [
    id 882
    label "Sri_Lanka"
  ]
  node [
    id 883
    label "Tonkin"
  ]
  node [
    id 884
    label "Katar"
  ]
  node [
    id 885
    label "Azja_Wschodnia"
  ]
  node [
    id 886
    label "Mikronezja"
  ]
  node [
    id 887
    label "Ukraina_Zachodnia"
  ]
  node [
    id 888
    label "Laos"
  ]
  node [
    id 889
    label "Mongolia"
  ]
  node [
    id 890
    label "Turyngia"
  ]
  node [
    id 891
    label "Malediwy"
  ]
  node [
    id 892
    label "Zambia"
  ]
  node [
    id 893
    label "Baszkiria"
  ]
  node [
    id 894
    label "Tanzania"
  ]
  node [
    id 895
    label "Gujana"
  ]
  node [
    id 896
    label "Apulia"
  ]
  node [
    id 897
    label "Czechy"
  ]
  node [
    id 898
    label "Panama"
  ]
  node [
    id 899
    label "Uzbekistan"
  ]
  node [
    id 900
    label "Gruzja"
  ]
  node [
    id 901
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 902
    label "Serbia"
  ]
  node [
    id 903
    label "Francja"
  ]
  node [
    id 904
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 905
    label "Togo"
  ]
  node [
    id 906
    label "Estonia"
  ]
  node [
    id 907
    label "Indochiny"
  ]
  node [
    id 908
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 909
    label "Oman"
  ]
  node [
    id 910
    label "Boliwia"
  ]
  node [
    id 911
    label "Portugalia"
  ]
  node [
    id 912
    label "Wyspy_Salomona"
  ]
  node [
    id 913
    label "Luksemburg"
  ]
  node [
    id 914
    label "Haiti"
  ]
  node [
    id 915
    label "Biskupizna"
  ]
  node [
    id 916
    label "Lubuskie"
  ]
  node [
    id 917
    label "Birma"
  ]
  node [
    id 918
    label "Rodezja"
  ]
  node [
    id 919
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 920
    label "bangla&#263;"
  ]
  node [
    id 921
    label "dziama&#263;"
  ]
  node [
    id 922
    label "tryb"
  ]
  node [
    id 923
    label "m&#322;ot"
  ]
  node [
    id 924
    label "piec_dymarski"
  ]
  node [
    id 925
    label "manufaktura"
  ]
  node [
    id 926
    label "dupny"
  ]
  node [
    id 927
    label "wysoce"
  ]
  node [
    id 928
    label "wyj&#261;tkowy"
  ]
  node [
    id 929
    label "wybitny"
  ]
  node [
    id 930
    label "prawdziwy"
  ]
  node [
    id 931
    label "nieprzeci&#281;tny"
  ]
  node [
    id 932
    label "formacja"
  ]
  node [
    id 933
    label "kronika"
  ]
  node [
    id 934
    label "czasopismo"
  ]
  node [
    id 935
    label "yearbook"
  ]
  node [
    id 936
    label "nadrz&#281;dny"
  ]
  node [
    id 937
    label "zbiorowy"
  ]
  node [
    id 938
    label "&#322;&#261;czny"
  ]
  node [
    id 939
    label "kompletny"
  ]
  node [
    id 940
    label "og&#243;lnie"
  ]
  node [
    id 941
    label "ca&#322;y"
  ]
  node [
    id 942
    label "og&#243;&#322;owy"
  ]
  node [
    id 943
    label "powszechnie"
  ]
  node [
    id 944
    label "kategoria"
  ]
  node [
    id 945
    label "kwadrat_magiczny"
  ]
  node [
    id 946
    label "grupa"
  ]
  node [
    id 947
    label "wyra&#380;enie"
  ]
  node [
    id 948
    label "rozmiar"
  ]
  node [
    id 949
    label "number"
  ]
  node [
    id 950
    label "poj&#281;cie"
  ]
  node [
    id 951
    label "doznawa&#263;"
  ]
  node [
    id 952
    label "znachodzi&#263;"
  ]
  node [
    id 953
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 954
    label "odzyskiwa&#263;"
  ]
  node [
    id 955
    label "os&#261;dza&#263;"
  ]
  node [
    id 956
    label "wykrywa&#263;"
  ]
  node [
    id 957
    label "unwrap"
  ]
  node [
    id 958
    label "detect"
  ]
  node [
    id 959
    label "wymy&#347;la&#263;"
  ]
  node [
    id 960
    label "powodowa&#263;"
  ]
  node [
    id 961
    label "zu&#380;y&#263;"
  ]
  node [
    id 962
    label "try"
  ]
  node [
    id 963
    label "scoop"
  ]
  node [
    id 964
    label "wybra&#263;"
  ]
  node [
    id 965
    label "wyko&#324;czy&#263;"
  ]
  node [
    id 966
    label "opracowa&#263;"
  ]
  node [
    id 967
    label "zm&#281;czy&#263;"
  ]
  node [
    id 968
    label "zalega&#263;"
  ]
  node [
    id 969
    label "zasoby_kopalin"
  ]
  node [
    id 970
    label "skupienie"
  ]
  node [
    id 971
    label "bilansowo&#347;&#263;"
  ]
  node [
    id 972
    label "zaleganie"
  ]
  node [
    id 973
    label "wychodnia"
  ]
  node [
    id 974
    label "cause"
  ]
  node [
    id 975
    label "introduce"
  ]
  node [
    id 976
    label "begin"
  ]
  node [
    id 977
    label "odj&#261;&#263;"
  ]
  node [
    id 978
    label "post&#261;pi&#263;"
  ]
  node [
    id 979
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 980
    label "do"
  ]
  node [
    id 981
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 982
    label "zrobi&#263;"
  ]
  node [
    id 983
    label "control"
  ]
  node [
    id 984
    label "Ural"
  ]
  node [
    id 985
    label "koniec"
  ]
  node [
    id 986
    label "kres"
  ]
  node [
    id 987
    label "granice"
  ]
  node [
    id 988
    label "pu&#322;ap"
  ]
  node [
    id 989
    label "frontier"
  ]
  node [
    id 990
    label "end"
  ]
  node [
    id 991
    label "przej&#347;cie"
  ]
  node [
    id 992
    label "Hollywood"
  ]
  node [
    id 993
    label "zal&#261;&#380;ek"
  ]
  node [
    id 994
    label "otoczenie"
  ]
  node [
    id 995
    label "o&#347;rodkowy_uk&#322;ad_nerwowy"
  ]
  node [
    id 996
    label "&#347;rodek"
  ]
  node [
    id 997
    label "center"
  ]
  node [
    id 998
    label "warunki"
  ]
  node [
    id 999
    label "zausznik"
  ]
  node [
    id 1000
    label "mostek"
  ]
  node [
    id 1001
    label "dodatek"
  ]
  node [
    id 1002
    label "soczewka"
  ]
  node [
    id 1003
    label "przyrz&#261;d"
  ]
  node [
    id 1004
    label "pingle"
  ]
  node [
    id 1005
    label "oprawka"
  ]
  node [
    id 1006
    label "cyngle"
  ]
  node [
    id 1007
    label "bryle"
  ]
  node [
    id 1008
    label "para"
  ]
  node [
    id 1009
    label "nano&#347;nik"
  ]
  node [
    id 1010
    label "okulista"
  ]
  node [
    id 1011
    label "stulecie"
  ]
  node [
    id 1012
    label "kalendarz"
  ]
  node [
    id 1013
    label "pora_roku"
  ]
  node [
    id 1014
    label "cykl_astronomiczny"
  ]
  node [
    id 1015
    label "p&#243;&#322;rocze"
  ]
  node [
    id 1016
    label "kwarta&#322;"
  ]
  node [
    id 1017
    label "kurs"
  ]
  node [
    id 1018
    label "jubileusz"
  ]
  node [
    id 1019
    label "miesi&#261;c"
  ]
  node [
    id 1020
    label "lata"
  ]
  node [
    id 1021
    label "martwy_sezon"
  ]
  node [
    id 1022
    label "reakcja_chemiczna"
  ]
  node [
    id 1023
    label "function"
  ]
  node [
    id 1024
    label "commit"
  ]
  node [
    id 1025
    label "robi&#263;"
  ]
  node [
    id 1026
    label "determine"
  ]
  node [
    id 1027
    label "medium"
  ]
  node [
    id 1028
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 1029
    label "zwi&#281;kszy&#263;"
  ]
  node [
    id 1030
    label "da&#263;"
  ]
  node [
    id 1031
    label "ponie&#347;&#263;"
  ]
  node [
    id 1032
    label "get"
  ]
  node [
    id 1033
    label "przytacha&#263;"
  ]
  node [
    id 1034
    label "increase"
  ]
  node [
    id 1035
    label "doda&#263;"
  ]
  node [
    id 1036
    label "zanie&#347;&#263;"
  ]
  node [
    id 1037
    label "poda&#263;"
  ]
  node [
    id 1038
    label "carry"
  ]
  node [
    id 1039
    label "anatomopatolog"
  ]
  node [
    id 1040
    label "rewizja"
  ]
  node [
    id 1041
    label "oznaka"
  ]
  node [
    id 1042
    label "ferment"
  ]
  node [
    id 1043
    label "komplet"
  ]
  node [
    id 1044
    label "tura"
  ]
  node [
    id 1045
    label "amendment"
  ]
  node [
    id 1046
    label "zmianka"
  ]
  node [
    id 1047
    label "odmienianie"
  ]
  node [
    id 1048
    label "passage"
  ]
  node [
    id 1049
    label "zjawisko"
  ]
  node [
    id 1050
    label "change"
  ]
  node [
    id 1051
    label "praca"
  ]
  node [
    id 1052
    label "model"
  ]
  node [
    id 1053
    label "narz&#281;dzie"
  ]
  node [
    id 1054
    label "nature"
  ]
  node [
    id 1055
    label "tease"
  ]
  node [
    id 1056
    label "wytwarza&#263;"
  ]
  node [
    id 1057
    label "take"
  ]
  node [
    id 1058
    label "jarzynka"
  ]
  node [
    id 1059
    label "casting"
  ]
  node [
    id 1060
    label "wytw&#243;r"
  ]
  node [
    id 1061
    label "mold"
  ]
  node [
    id 1062
    label "establish"
  ]
  node [
    id 1063
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 1064
    label "recline"
  ]
  node [
    id 1065
    label "podstawa"
  ]
  node [
    id 1066
    label "ustawi&#263;"
  ]
  node [
    id 1067
    label "osnowa&#263;"
  ]
  node [
    id 1068
    label "kiedy&#347;"
  ]
  node [
    id 1069
    label "legislacyjnie"
  ]
  node [
    id 1070
    label "kognicja"
  ]
  node [
    id 1071
    label "przebieg"
  ]
  node [
    id 1072
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 1073
    label "wydarzenie"
  ]
  node [
    id 1074
    label "przes&#322;anka"
  ]
  node [
    id 1075
    label "rozprawa"
  ]
  node [
    id 1076
    label "nast&#281;pstwo"
  ]
  node [
    id 1077
    label "jednostka_monetarna"
  ]
  node [
    id 1078
    label "engineering"
  ]
  node [
    id 1079
    label "technika"
  ]
  node [
    id 1080
    label "mikrotechnologia"
  ]
  node [
    id 1081
    label "technologia_nieorganiczna"
  ]
  node [
    id 1082
    label "biotechnologia"
  ]
  node [
    id 1083
    label "zauwa&#380;alnie"
  ]
  node [
    id 1084
    label "wygrywa&#263;"
  ]
  node [
    id 1085
    label "przekracza&#263;"
  ]
  node [
    id 1086
    label "chop"
  ]
  node [
    id 1087
    label "base_on_balls"
  ]
  node [
    id 1088
    label "undertaking"
  ]
  node [
    id 1089
    label "wyprzedza&#263;"
  ]
  node [
    id 1090
    label "prowadzi&#263;"
  ]
  node [
    id 1091
    label "dotychczasowy"
  ]
  node [
    id 1092
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 1093
    label "method"
  ]
  node [
    id 1094
    label "doktryna"
  ]
  node [
    id 1095
    label "praktyczny"
  ]
  node [
    id 1096
    label "surowiec_energetyczny"
  ]
  node [
    id 1097
    label "w&#281;glowodan"
  ]
  node [
    id 1098
    label "coal"
  ]
  node [
    id 1099
    label "przybory_do_pisania"
  ]
  node [
    id 1100
    label "makroelement"
  ]
  node [
    id 1101
    label "niemetal"
  ]
  node [
    id 1102
    label "zsypnik"
  ]
  node [
    id 1103
    label "w&#281;glarka"
  ]
  node [
    id 1104
    label "zw&#281;glenie_si&#281;"
  ]
  node [
    id 1105
    label "zw&#281;glanie_si&#281;"
  ]
  node [
    id 1106
    label "coil"
  ]
  node [
    id 1107
    label "bry&#322;a"
  ]
  node [
    id 1108
    label "ska&#322;a"
  ]
  node [
    id 1109
    label "carbon"
  ]
  node [
    id 1110
    label "fulleren"
  ]
  node [
    id 1111
    label "rysunek"
  ]
  node [
    id 1112
    label "drewny"
  ]
  node [
    id 1113
    label "drewniany"
  ]
  node [
    id 1114
    label "Arizona"
  ]
  node [
    id 1115
    label "Georgia"
  ]
  node [
    id 1116
    label "warstwa"
  ]
  node [
    id 1117
    label "Hawaje"
  ]
  node [
    id 1118
    label "Goa"
  ]
  node [
    id 1119
    label "Floryda"
  ]
  node [
    id 1120
    label "Oklahoma"
  ]
  node [
    id 1121
    label "punkt"
  ]
  node [
    id 1122
    label "Alaska"
  ]
  node [
    id 1123
    label "wci&#281;cie"
  ]
  node [
    id 1124
    label "Alabama"
  ]
  node [
    id 1125
    label "Oregon"
  ]
  node [
    id 1126
    label "poziom"
  ]
  node [
    id 1127
    label "Teksas"
  ]
  node [
    id 1128
    label "Illinois"
  ]
  node [
    id 1129
    label "Waszyngton"
  ]
  node [
    id 1130
    label "Jukatan"
  ]
  node [
    id 1131
    label "shape"
  ]
  node [
    id 1132
    label "Nowy_Meksyk"
  ]
  node [
    id 1133
    label "ilo&#347;&#263;"
  ]
  node [
    id 1134
    label "state"
  ]
  node [
    id 1135
    label "Nowy_York"
  ]
  node [
    id 1136
    label "Arakan"
  ]
  node [
    id 1137
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 1138
    label "Kalifornia"
  ]
  node [
    id 1139
    label "wektor"
  ]
  node [
    id 1140
    label "Massachusetts"
  ]
  node [
    id 1141
    label "Pensylwania"
  ]
  node [
    id 1142
    label "Michigan"
  ]
  node [
    id 1143
    label "Maryland"
  ]
  node [
    id 1144
    label "Ohio"
  ]
  node [
    id 1145
    label "Kansas"
  ]
  node [
    id 1146
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 1147
    label "Luizjana"
  ]
  node [
    id 1148
    label "samopoczucie"
  ]
  node [
    id 1149
    label "Wirginia"
  ]
  node [
    id 1150
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 1151
    label "wytworzy&#263;"
  ]
  node [
    id 1152
    label "manufacture"
  ]
  node [
    id 1153
    label "stworzy&#263;"
  ]
  node [
    id 1154
    label "seria"
  ]
  node [
    id 1155
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 1156
    label "d&#378;wi&#281;k"
  ]
  node [
    id 1157
    label "formality"
  ]
  node [
    id 1158
    label "heksachord"
  ]
  node [
    id 1159
    label "note"
  ]
  node [
    id 1160
    label "akcent"
  ]
  node [
    id 1161
    label "chromatyczno&#347;&#263;"
  ]
  node [
    id 1162
    label "ubarwienie"
  ]
  node [
    id 1163
    label "tone"
  ]
  node [
    id 1164
    label "rejestr"
  ]
  node [
    id 1165
    label "neoproterozoik"
  ]
  node [
    id 1166
    label "interwa&#322;"
  ]
  node [
    id 1167
    label "wieloton"
  ]
  node [
    id 1168
    label "r&#243;&#380;nica"
  ]
  node [
    id 1169
    label "kolorystyka"
  ]
  node [
    id 1170
    label "modalizm"
  ]
  node [
    id 1171
    label "glinka"
  ]
  node [
    id 1172
    label "zabarwienie"
  ]
  node [
    id 1173
    label "sound"
  ]
  node [
    id 1174
    label "repetycja"
  ]
  node [
    id 1175
    label "zwyczaj"
  ]
  node [
    id 1176
    label "tu&#324;czyk"
  ]
  node [
    id 1177
    label "solmizacja"
  ]
  node [
    id 1178
    label "jednostka"
  ]
  node [
    id 1179
    label "gospodarka"
  ]
  node [
    id 1180
    label "przechowalnictwo"
  ]
  node [
    id 1181
    label "uprzemys&#322;owienie"
  ]
  node [
    id 1182
    label "przemys&#322;&#243;wka"
  ]
  node [
    id 1183
    label "uprzemys&#322;owienie_si&#281;"
  ]
  node [
    id 1184
    label "uprzemys&#322;awianie_si&#281;"
  ]
  node [
    id 1185
    label "uprzemys&#322;awianie"
  ]
  node [
    id 1186
    label "rockowy"
  ]
  node [
    id 1187
    label "metaloplastyczny"
  ]
  node [
    id 1188
    label "need"
  ]
  node [
    id 1189
    label "wym&#243;g"
  ]
  node [
    id 1190
    label "necessity"
  ]
  node [
    id 1191
    label "pragnienie"
  ]
  node [
    id 1192
    label "sytuacja"
  ]
  node [
    id 1193
    label "planowa&#263;"
  ]
  node [
    id 1194
    label "dodawa&#263;_otuchy"
  ]
  node [
    id 1195
    label "consist"
  ]
  node [
    id 1196
    label "tracz"
  ]
  node [
    id 1197
    label "wytw&#243;rnia"
  ]
  node [
    id 1198
    label "deskowisko"
  ]
  node [
    id 1199
    label "typify"
  ]
  node [
    id 1200
    label "represent"
  ]
  node [
    id 1201
    label "decydowa&#263;"
  ]
  node [
    id 1202
    label "decide"
  ]
  node [
    id 1203
    label "zatrzymywa&#263;"
  ]
  node [
    id 1204
    label "pies_my&#347;liwski"
  ]
  node [
    id 1205
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 1206
    label "szczery"
  ]
  node [
    id 1207
    label "bliski"
  ]
  node [
    id 1208
    label "bezpo&#347;rednio"
  ]
  node [
    id 1209
    label "wyposa&#380;enie"
  ]
  node [
    id 1210
    label "infrastruktura"
  ]
  node [
    id 1211
    label "pomieszczenie"
  ]
  node [
    id 1212
    label "sklep"
  ]
  node [
    id 1213
    label "specjalny"
  ]
  node [
    id 1214
    label "robotnik"
  ]
  node [
    id 1215
    label "budowy"
  ]
  node [
    id 1216
    label "tutejszy"
  ]
  node [
    id 1217
    label "okolicznie"
  ]
  node [
    id 1218
    label "okoliczno&#347;ciowy"
  ]
  node [
    id 1219
    label "sytuacyjny"
  ]
  node [
    id 1220
    label "pobliski"
  ]
  node [
    id 1221
    label "nacisn&#261;&#263;"
  ]
  node [
    id 1222
    label "zaatakowa&#263;"
  ]
  node [
    id 1223
    label "gamble"
  ]
  node [
    id 1224
    label "supervene"
  ]
  node [
    id 1225
    label "doros&#322;y"
  ]
  node [
    id 1226
    label "wiele"
  ]
  node [
    id 1227
    label "dorodny"
  ]
  node [
    id 1228
    label "du&#380;o"
  ]
  node [
    id 1229
    label "niema&#322;o"
  ]
  node [
    id 1230
    label "rozwini&#281;ty"
  ]
  node [
    id 1231
    label "wegetacja"
  ]
  node [
    id 1232
    label "blooming"
  ]
  node [
    id 1233
    label "st&#281;&#380;e&#263;"
  ]
  node [
    id 1234
    label "tobo&#322;ek"
  ]
  node [
    id 1235
    label "obezw&#322;adni&#263;"
  ]
  node [
    id 1236
    label "scali&#263;"
  ]
  node [
    id 1237
    label "zawi&#261;za&#263;"
  ]
  node [
    id 1238
    label "zatrzyma&#263;"
  ]
  node [
    id 1239
    label "form"
  ]
  node [
    id 1240
    label "bind"
  ]
  node [
    id 1241
    label "wi&#261;zanie_chemiczne"
  ]
  node [
    id 1242
    label "unify"
  ]
  node [
    id 1243
    label "consort"
  ]
  node [
    id 1244
    label "incorporate"
  ]
  node [
    id 1245
    label "wi&#281;&#378;"
  ]
  node [
    id 1246
    label "zobowi&#261;za&#263;"
  ]
  node [
    id 1247
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 1248
    label "w&#281;ze&#322;"
  ]
  node [
    id 1249
    label "cz&#261;steczka_chemiczna"
  ]
  node [
    id 1250
    label "powi&#261;za&#263;"
  ]
  node [
    id 1251
    label "opakowa&#263;"
  ]
  node [
    id 1252
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 1253
    label "cement"
  ]
  node [
    id 1254
    label "zaprawa"
  ]
  node [
    id 1255
    label "relate"
  ]
  node [
    id 1256
    label "niemiecki"
  ]
  node [
    id 1257
    label "device"
  ]
  node [
    id 1258
    label "obraz"
  ]
  node [
    id 1259
    label "dekoracja"
  ]
  node [
    id 1260
    label "intencja"
  ]
  node [
    id 1261
    label "agreement"
  ]
  node [
    id 1262
    label "pomys&#322;"
  ]
  node [
    id 1263
    label "perspektywa"
  ]
  node [
    id 1264
    label "reprezentacja"
  ]
  node [
    id 1265
    label "proszek"
  ]
  node [
    id 1266
    label "proceed"
  ]
  node [
    id 1267
    label "catch"
  ]
  node [
    id 1268
    label "pozosta&#263;"
  ]
  node [
    id 1269
    label "osta&#263;_si&#281;"
  ]
  node [
    id 1270
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 1271
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 1272
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 1273
    label "budowla"
  ]
  node [
    id 1274
    label "pod&#322;u&#380;nie"
  ]
  node [
    id 1275
    label "Izera"
  ]
  node [
    id 1276
    label "Orla"
  ]
  node [
    id 1277
    label "Amazonka"
  ]
  node [
    id 1278
    label "Kaczawa"
  ]
  node [
    id 1279
    label "Hudson"
  ]
  node [
    id 1280
    label "Drina"
  ]
  node [
    id 1281
    label "Windawa"
  ]
  node [
    id 1282
    label "Wereszyca"
  ]
  node [
    id 1283
    label "Wis&#322;a"
  ]
  node [
    id 1284
    label "Peczora"
  ]
  node [
    id 1285
    label "Pad"
  ]
  node [
    id 1286
    label "ciek_wodny"
  ]
  node [
    id 1287
    label "Ren"
  ]
  node [
    id 1288
    label "S&#322;upia"
  ]
  node [
    id 1289
    label "Dunaj"
  ]
  node [
    id 1290
    label "Orinoko"
  ]
  node [
    id 1291
    label "Wia&#378;ma"
  ]
  node [
    id 1292
    label "Zyrianka"
  ]
  node [
    id 1293
    label "Turiec"
  ]
  node [
    id 1294
    label "Pia&#347;nica"
  ]
  node [
    id 1295
    label "Cisa"
  ]
  node [
    id 1296
    label "Dniepr"
  ]
  node [
    id 1297
    label "Odra"
  ]
  node [
    id 1298
    label "Sekwana"
  ]
  node [
    id 1299
    label "Dniestr"
  ]
  node [
    id 1300
    label "Supra&#347;l"
  ]
  node [
    id 1301
    label "Wieprza"
  ]
  node [
    id 1302
    label "Nil"
  ]
  node [
    id 1303
    label "D&#378;wina"
  ]
  node [
    id 1304
    label "woda_powierzchniowa"
  ]
  node [
    id 1305
    label "Anadyr"
  ]
  node [
    id 1306
    label "Amur"
  ]
  node [
    id 1307
    label "Podkamienna_Tunguzka"
  ]
  node [
    id 1308
    label "So&#322;a"
  ]
  node [
    id 1309
    label "Zi&#281;bina"
  ]
  node [
    id 1310
    label "Ropa"
  ]
  node [
    id 1311
    label "Styks"
  ]
  node [
    id 1312
    label "Mozela"
  ]
  node [
    id 1313
    label "Witim"
  ]
  node [
    id 1314
    label "Don"
  ]
  node [
    id 1315
    label "Sanica"
  ]
  node [
    id 1316
    label "potamoplankton"
  ]
  node [
    id 1317
    label "Wo&#322;ga"
  ]
  node [
    id 1318
    label "Moza"
  ]
  node [
    id 1319
    label "Niemen"
  ]
  node [
    id 1320
    label "Lena"
  ]
  node [
    id 1321
    label "Dwina"
  ]
  node [
    id 1322
    label "Pas&#322;&#281;ka"
  ]
  node [
    id 1323
    label "Zarycz"
  ]
  node [
    id 1324
    label "Brze&#378;niczanka"
  ]
  node [
    id 1325
    label "odp&#322;ywanie"
  ]
  node [
    id 1326
    label "Jenisej"
  ]
  node [
    id 1327
    label "Ussuri"
  ]
  node [
    id 1328
    label "wpadni&#281;cie"
  ]
  node [
    id 1329
    label "Pr&#261;dnik"
  ]
  node [
    id 1330
    label "Lete"
  ]
  node [
    id 1331
    label "&#321;aba"
  ]
  node [
    id 1332
    label "Ob"
  ]
  node [
    id 1333
    label "Berezyna"
  ]
  node [
    id 1334
    label "Rega"
  ]
  node [
    id 1335
    label "Widawa"
  ]
  node [
    id 1336
    label "Newa"
  ]
  node [
    id 1337
    label "wpadanie"
  ]
  node [
    id 1338
    label "&#321;upawa"
  ]
  node [
    id 1339
    label "strumie&#324;"
  ]
  node [
    id 1340
    label "Pars&#281;ta"
  ]
  node [
    id 1341
    label "ghaty"
  ]
  node [
    id 1342
    label "niewzruszony"
  ]
  node [
    id 1343
    label "twardy"
  ]
  node [
    id 1344
    label "kamiennie"
  ]
  node [
    id 1345
    label "g&#322;&#281;boki"
  ]
  node [
    id 1346
    label "mineralny"
  ]
  node [
    id 1347
    label "ch&#322;odny"
  ]
  node [
    id 1348
    label "actualize"
  ]
  node [
    id 1349
    label "realize"
  ]
  node [
    id 1350
    label "wykorzysta&#263;"
  ]
  node [
    id 1351
    label "spieni&#281;&#380;y&#263;"
  ]
  node [
    id 1352
    label "nieca&#322;y"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 257
  ]
  edge [
    source 7
    target 258
  ]
  edge [
    source 7
    target 259
  ]
  edge [
    source 7
    target 260
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 19
  ]
  edge [
    source 8
    target 20
  ]
  edge [
    source 8
    target 40
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 43
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 53
  ]
  edge [
    source 9
    target 49
  ]
  edge [
    source 9
    target 51
  ]
  edge [
    source 9
    target 261
  ]
  edge [
    source 9
    target 262
  ]
  edge [
    source 9
    target 263
  ]
  edge [
    source 9
    target 264
  ]
  edge [
    source 9
    target 265
  ]
  edge [
    source 9
    target 266
  ]
  edge [
    source 9
    target 267
  ]
  edge [
    source 9
    target 13
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 56
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 55
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 41
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 268
  ]
  edge [
    source 10
    target 269
  ]
  edge [
    source 10
    target 270
  ]
  edge [
    source 10
    target 271
  ]
  edge [
    source 10
    target 272
  ]
  edge [
    source 10
    target 273
  ]
  edge [
    source 10
    target 274
  ]
  edge [
    source 10
    target 275
  ]
  edge [
    source 10
    target 276
  ]
  edge [
    source 10
    target 277
  ]
  edge [
    source 10
    target 278
  ]
  edge [
    source 10
    target 279
  ]
  edge [
    source 10
    target 280
  ]
  edge [
    source 10
    target 281
  ]
  edge [
    source 10
    target 282
  ]
  edge [
    source 10
    target 283
  ]
  edge [
    source 10
    target 284
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 285
  ]
  edge [
    source 11
    target 286
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 25
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 153
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 67
  ]
  edge [
    source 13
    target 68
  ]
  edge [
    source 13
    target 88
  ]
  edge [
    source 13
    target 89
  ]
  edge [
    source 13
    target 41
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 287
  ]
  edge [
    source 14
    target 288
  ]
  edge [
    source 14
    target 289
  ]
  edge [
    source 14
    target 290
  ]
  edge [
    source 14
    target 291
  ]
  edge [
    source 14
    target 292
  ]
  edge [
    source 14
    target 293
  ]
  edge [
    source 14
    target 294
  ]
  edge [
    source 14
    target 295
  ]
  edge [
    source 14
    target 296
  ]
  edge [
    source 14
    target 297
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 15
    target 298
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 299
  ]
  edge [
    source 16
    target 300
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 66
  ]
  edge [
    source 17
    target 67
  ]
  edge [
    source 17
    target 96
  ]
  edge [
    source 17
    target 107
  ]
  edge [
    source 17
    target 301
  ]
  edge [
    source 17
    target 302
  ]
  edge [
    source 17
    target 303
  ]
  edge [
    source 17
    target 304
  ]
  edge [
    source 17
    target 305
  ]
  edge [
    source 17
    target 306
  ]
  edge [
    source 17
    target 307
  ]
  edge [
    source 17
    target 308
  ]
  edge [
    source 17
    target 309
  ]
  edge [
    source 17
    target 310
  ]
  edge [
    source 17
    target 311
  ]
  edge [
    source 17
    target 312
  ]
  edge [
    source 17
    target 313
  ]
  edge [
    source 17
    target 314
  ]
  edge [
    source 17
    target 315
  ]
  edge [
    source 17
    target 316
  ]
  edge [
    source 17
    target 223
  ]
  edge [
    source 17
    target 317
  ]
  edge [
    source 17
    target 318
  ]
  edge [
    source 17
    target 319
  ]
  edge [
    source 17
    target 320
  ]
  edge [
    source 17
    target 321
  ]
  edge [
    source 17
    target 322
  ]
  edge [
    source 17
    target 19
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 70
  ]
  edge [
    source 18
    target 40
  ]
  edge [
    source 19
    target 31
  ]
  edge [
    source 19
    target 32
  ]
  edge [
    source 19
    target 64
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 323
  ]
  edge [
    source 21
    target 324
  ]
  edge [
    source 21
    target 325
  ]
  edge [
    source 21
    target 326
  ]
  edge [
    source 21
    target 327
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 124
  ]
  edge [
    source 22
    target 328
  ]
  edge [
    source 22
    target 329
  ]
  edge [
    source 22
    target 330
  ]
  edge [
    source 22
    target 331
  ]
  edge [
    source 22
    target 332
  ]
  edge [
    source 22
    target 333
  ]
  edge [
    source 22
    target 334
  ]
  edge [
    source 22
    target 335
  ]
  edge [
    source 22
    target 336
  ]
  edge [
    source 22
    target 337
  ]
  edge [
    source 22
    target 35
  ]
  edge [
    source 22
    target 109
  ]
  edge [
    source 22
    target 87
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 338
  ]
  edge [
    source 23
    target 339
  ]
  edge [
    source 23
    target 340
  ]
  edge [
    source 23
    target 341
  ]
  edge [
    source 23
    target 342
  ]
  edge [
    source 23
    target 343
  ]
  edge [
    source 23
    target 344
  ]
  edge [
    source 23
    target 345
  ]
  edge [
    source 23
    target 346
  ]
  edge [
    source 23
    target 347
  ]
  edge [
    source 23
    target 348
  ]
  edge [
    source 23
    target 349
  ]
  edge [
    source 23
    target 350
  ]
  edge [
    source 23
    target 58
  ]
  edge [
    source 23
    target 110
  ]
  edge [
    source 23
    target 121
  ]
  edge [
    source 23
    target 135
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 351
  ]
  edge [
    source 24
    target 352
  ]
  edge [
    source 24
    target 157
  ]
  edge [
    source 24
    target 353
  ]
  edge [
    source 24
    target 354
  ]
  edge [
    source 24
    target 355
  ]
  edge [
    source 24
    target 356
  ]
  edge [
    source 24
    target 357
  ]
  edge [
    source 24
    target 162
  ]
  edge [
    source 24
    target 358
  ]
  edge [
    source 24
    target 359
  ]
  edge [
    source 24
    target 360
  ]
  edge [
    source 24
    target 361
  ]
  edge [
    source 24
    target 362
  ]
  edge [
    source 24
    target 363
  ]
  edge [
    source 24
    target 364
  ]
  edge [
    source 24
    target 115
  ]
  edge [
    source 24
    target 165
  ]
  edge [
    source 24
    target 365
  ]
  edge [
    source 24
    target 366
  ]
  edge [
    source 24
    target 367
  ]
  edge [
    source 24
    target 368
  ]
  edge [
    source 24
    target 369
  ]
  edge [
    source 24
    target 370
  ]
  edge [
    source 24
    target 371
  ]
  edge [
    source 24
    target 372
  ]
  edge [
    source 24
    target 373
  ]
  edge [
    source 24
    target 374
  ]
  edge [
    source 24
    target 375
  ]
  edge [
    source 24
    target 376
  ]
  edge [
    source 24
    target 377
  ]
  edge [
    source 24
    target 171
  ]
  edge [
    source 24
    target 378
  ]
  edge [
    source 24
    target 379
  ]
  edge [
    source 24
    target 380
  ]
  edge [
    source 24
    target 381
  ]
  edge [
    source 24
    target 173
  ]
  edge [
    source 24
    target 382
  ]
  edge [
    source 24
    target 175
  ]
  edge [
    source 24
    target 383
  ]
  edge [
    source 24
    target 384
  ]
  edge [
    source 24
    target 385
  ]
  edge [
    source 24
    target 386
  ]
  edge [
    source 24
    target 387
  ]
  edge [
    source 24
    target 388
  ]
  edge [
    source 24
    target 389
  ]
  edge [
    source 24
    target 390
  ]
  edge [
    source 24
    target 391
  ]
  edge [
    source 24
    target 392
  ]
  edge [
    source 24
    target 393
  ]
  edge [
    source 24
    target 394
  ]
  edge [
    source 24
    target 395
  ]
  edge [
    source 24
    target 396
  ]
  edge [
    source 24
    target 397
  ]
  edge [
    source 24
    target 398
  ]
  edge [
    source 24
    target 399
  ]
  edge [
    source 24
    target 185
  ]
  edge [
    source 24
    target 400
  ]
  edge [
    source 24
    target 401
  ]
  edge [
    source 24
    target 402
  ]
  edge [
    source 24
    target 403
  ]
  edge [
    source 24
    target 404
  ]
  edge [
    source 24
    target 405
  ]
  edge [
    source 24
    target 406
  ]
  edge [
    source 24
    target 407
  ]
  edge [
    source 24
    target 408
  ]
  edge [
    source 24
    target 409
  ]
  edge [
    source 24
    target 42
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 103
  ]
  edge [
    source 25
    target 153
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 28
  ]
  edge [
    source 26
    target 65
  ]
  edge [
    source 26
    target 78
  ]
  edge [
    source 26
    target 88
  ]
  edge [
    source 26
    target 89
  ]
  edge [
    source 26
    target 116
  ]
  edge [
    source 26
    target 37
  ]
  edge [
    source 26
    target 131
  ]
  edge [
    source 26
    target 410
  ]
  edge [
    source 26
    target 157
  ]
  edge [
    source 26
    target 411
  ]
  edge [
    source 26
    target 83
  ]
  edge [
    source 26
    target 412
  ]
  edge [
    source 26
    target 413
  ]
  edge [
    source 26
    target 356
  ]
  edge [
    source 26
    target 414
  ]
  edge [
    source 26
    target 415
  ]
  edge [
    source 27
    target 416
  ]
  edge [
    source 27
    target 417
  ]
  edge [
    source 27
    target 418
  ]
  edge [
    source 27
    target 419
  ]
  edge [
    source 27
    target 420
  ]
  edge [
    source 27
    target 421
  ]
  edge [
    source 27
    target 422
  ]
  edge [
    source 27
    target 423
  ]
  edge [
    source 27
    target 424
  ]
  edge [
    source 27
    target 425
  ]
  edge [
    source 27
    target 426
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 427
  ]
  edge [
    source 28
    target 428
  ]
  edge [
    source 28
    target 429
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 40
  ]
  edge [
    source 29
    target 41
  ]
  edge [
    source 29
    target 72
  ]
  edge [
    source 29
    target 53
  ]
  edge [
    source 29
    target 74
  ]
  edge [
    source 29
    target 75
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 430
  ]
  edge [
    source 30
    target 69
  ]
  edge [
    source 31
    target 431
  ]
  edge [
    source 31
    target 432
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 433
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 36
  ]
  edge [
    source 33
    target 66
  ]
  edge [
    source 33
    target 130
  ]
  edge [
    source 33
    target 87
  ]
  edge [
    source 33
    target 434
  ]
  edge [
    source 33
    target 85
  ]
  edge [
    source 33
    target 120
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 129
  ]
  edge [
    source 34
    target 130
  ]
  edge [
    source 34
    target 150
  ]
  edge [
    source 34
    target 435
  ]
  edge [
    source 34
    target 436
  ]
  edge [
    source 34
    target 437
  ]
  edge [
    source 34
    target 438
  ]
  edge [
    source 34
    target 439
  ]
  edge [
    source 34
    target 440
  ]
  edge [
    source 34
    target 441
  ]
  edge [
    source 34
    target 442
  ]
  edge [
    source 34
    target 443
  ]
  edge [
    source 34
    target 444
  ]
  edge [
    source 34
    target 36
  ]
  edge [
    source 34
    target 96
  ]
  edge [
    source 34
    target 56
  ]
  edge [
    source 34
    target 146
  ]
  edge [
    source 34
    target 64
  ]
  edge [
    source 34
    target 93
  ]
  edge [
    source 34
    target 105
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 59
  ]
  edge [
    source 35
    target 60
  ]
  edge [
    source 35
    target 79
  ]
  edge [
    source 35
    target 445
  ]
  edge [
    source 35
    target 446
  ]
  edge [
    source 35
    target 109
  ]
  edge [
    source 35
    target 87
  ]
  edge [
    source 36
    target 65
  ]
  edge [
    source 36
    target 96
  ]
  edge [
    source 36
    target 56
  ]
  edge [
    source 36
    target 146
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 55
  ]
  edge [
    source 37
    target 142
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 57
  ]
  edge [
    source 38
    target 69
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 447
  ]
  edge [
    source 39
    target 448
  ]
  edge [
    source 39
    target 449
  ]
  edge [
    source 39
    target 450
  ]
  edge [
    source 39
    target 451
  ]
  edge [
    source 39
    target 387
  ]
  edge [
    source 39
    target 452
  ]
  edge [
    source 39
    target 453
  ]
  edge [
    source 39
    target 454
  ]
  edge [
    source 39
    target 55
  ]
  edge [
    source 39
    target 70
  ]
  edge [
    source 39
    target 86
  ]
  edge [
    source 40
    target 119
  ]
  edge [
    source 40
    target 455
  ]
  edge [
    source 40
    target 456
  ]
  edge [
    source 40
    target 237
  ]
  edge [
    source 40
    target 457
  ]
  edge [
    source 40
    target 458
  ]
  edge [
    source 40
    target 459
  ]
  edge [
    source 40
    target 460
  ]
  edge [
    source 40
    target 461
  ]
  edge [
    source 40
    target 462
  ]
  edge [
    source 40
    target 463
  ]
  edge [
    source 40
    target 464
  ]
  edge [
    source 40
    target 141
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 49
  ]
  edge [
    source 41
    target 465
  ]
  edge [
    source 41
    target 466
  ]
  edge [
    source 41
    target 467
  ]
  edge [
    source 41
    target 468
  ]
  edge [
    source 41
    target 469
  ]
  edge [
    source 41
    target 470
  ]
  edge [
    source 41
    target 471
  ]
  edge [
    source 41
    target 472
  ]
  edge [
    source 41
    target 473
  ]
  edge [
    source 41
    target 474
  ]
  edge [
    source 41
    target 475
  ]
  edge [
    source 41
    target 476
  ]
  edge [
    source 41
    target 477
  ]
  edge [
    source 41
    target 478
  ]
  edge [
    source 41
    target 479
  ]
  edge [
    source 41
    target 480
  ]
  edge [
    source 41
    target 481
  ]
  edge [
    source 41
    target 67
  ]
  edge [
    source 41
    target 68
  ]
  edge [
    source 41
    target 88
  ]
  edge [
    source 41
    target 89
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 465
  ]
  edge [
    source 42
    target 482
  ]
  edge [
    source 42
    target 483
  ]
  edge [
    source 42
    target 484
  ]
  edge [
    source 42
    target 485
  ]
  edge [
    source 42
    target 486
  ]
  edge [
    source 42
    target 487
  ]
  edge [
    source 42
    target 488
  ]
  edge [
    source 42
    target 489
  ]
  edge [
    source 42
    target 490
  ]
  edge [
    source 42
    target 491
  ]
  edge [
    source 42
    target 492
  ]
  edge [
    source 42
    target 481
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 77
  ]
  edge [
    source 43
    target 493
  ]
  edge [
    source 43
    target 117
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 494
  ]
  edge [
    source 44
    target 495
  ]
  edge [
    source 44
    target 496
  ]
  edge [
    source 44
    target 497
  ]
  edge [
    source 44
    target 498
  ]
  edge [
    source 44
    target 499
  ]
  edge [
    source 44
    target 500
  ]
  edge [
    source 44
    target 501
  ]
  edge [
    source 44
    target 502
  ]
  edge [
    source 44
    target 503
  ]
  edge [
    source 44
    target 504
  ]
  edge [
    source 44
    target 505
  ]
  edge [
    source 44
    target 506
  ]
  edge [
    source 44
    target 507
  ]
  edge [
    source 44
    target 508
  ]
  edge [
    source 44
    target 509
  ]
  edge [
    source 44
    target 510
  ]
  edge [
    source 44
    target 511
  ]
  edge [
    source 44
    target 512
  ]
  edge [
    source 44
    target 513
  ]
  edge [
    source 44
    target 514
  ]
  edge [
    source 44
    target 515
  ]
  edge [
    source 44
    target 516
  ]
  edge [
    source 44
    target 56
  ]
  edge [
    source 44
    target 87
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 517
  ]
  edge [
    source 46
    target 518
  ]
  edge [
    source 46
    target 519
  ]
  edge [
    source 46
    target 520
  ]
  edge [
    source 46
    target 521
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 81
  ]
  edge [
    source 47
    target 82
  ]
  edge [
    source 47
    target 105
  ]
  edge [
    source 47
    target 106
  ]
  edge [
    source 47
    target 522
  ]
  edge [
    source 47
    target 523
  ]
  edge [
    source 47
    target 524
  ]
  edge [
    source 47
    target 239
  ]
  edge [
    source 47
    target 525
  ]
  edge [
    source 47
    target 526
  ]
  edge [
    source 47
    target 527
  ]
  edge [
    source 47
    target 528
  ]
  edge [
    source 47
    target 529
  ]
  edge [
    source 47
    target 483
  ]
  edge [
    source 47
    target 530
  ]
  edge [
    source 47
    target 531
  ]
  edge [
    source 47
    target 532
  ]
  edge [
    source 47
    target 533
  ]
  edge [
    source 47
    target 534
  ]
  edge [
    source 47
    target 535
  ]
  edge [
    source 47
    target 536
  ]
  edge [
    source 47
    target 537
  ]
  edge [
    source 47
    target 226
  ]
  edge [
    source 47
    target 538
  ]
  edge [
    source 47
    target 539
  ]
  edge [
    source 47
    target 94
  ]
  edge [
    source 47
    target 51
  ]
  edge [
    source 47
    target 101
  ]
  edge [
    source 47
    target 52
  ]
  edge [
    source 48
    target 78
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 540
  ]
  edge [
    source 49
    target 541
  ]
  edge [
    source 49
    target 542
  ]
  edge [
    source 49
    target 543
  ]
  edge [
    source 49
    target 544
  ]
  edge [
    source 49
    target 545
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 55
  ]
  edge [
    source 50
    target 546
  ]
  edge [
    source 50
    target 547
  ]
  edge [
    source 50
    target 548
  ]
  edge [
    source 50
    target 549
  ]
  edge [
    source 50
    target 550
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 75
  ]
  edge [
    source 51
    target 120
  ]
  edge [
    source 51
    target 547
  ]
  edge [
    source 51
    target 551
  ]
  edge [
    source 51
    target 552
  ]
  edge [
    source 51
    target 553
  ]
  edge [
    source 51
    target 554
  ]
  edge [
    source 51
    target 548
  ]
  edge [
    source 51
    target 555
  ]
  edge [
    source 51
    target 267
  ]
  edge [
    source 51
    target 147
  ]
  edge [
    source 51
    target 101
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 556
  ]
  edge [
    source 52
    target 557
  ]
  edge [
    source 52
    target 558
  ]
  edge [
    source 52
    target 559
  ]
  edge [
    source 52
    target 552
  ]
  edge [
    source 52
    target 560
  ]
  edge [
    source 52
    target 561
  ]
  edge [
    source 52
    target 562
  ]
  edge [
    source 52
    target 563
  ]
  edge [
    source 52
    target 564
  ]
  edge [
    source 52
    target 565
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 73
  ]
  edge [
    source 53
    target 85
  ]
  edge [
    source 53
    target 86
  ]
  edge [
    source 53
    target 95
  ]
  edge [
    source 53
    target 91
  ]
  edge [
    source 53
    target 98
  ]
  edge [
    source 53
    target 149
  ]
  edge [
    source 53
    target 56
  ]
  edge [
    source 53
    target 55
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 152
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 566
  ]
  edge [
    source 55
    target 567
  ]
  edge [
    source 55
    target 568
  ]
  edge [
    source 55
    target 70
  ]
  edge [
    source 55
    target 86
  ]
  edge [
    source 55
    target 95
  ]
  edge [
    source 55
    target 145
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 76
  ]
  edge [
    source 56
    target 78
  ]
  edge [
    source 56
    target 59
  ]
  edge [
    source 56
    target 77
  ]
  edge [
    source 56
    target 569
  ]
  edge [
    source 56
    target 570
  ]
  edge [
    source 56
    target 571
  ]
  edge [
    source 56
    target 572
  ]
  edge [
    source 56
    target 573
  ]
  edge [
    source 56
    target 574
  ]
  edge [
    source 56
    target 575
  ]
  edge [
    source 56
    target 58
  ]
  edge [
    source 56
    target 95
  ]
  edge [
    source 56
    target 87
  ]
  edge [
    source 56
    target 96
  ]
  edge [
    source 56
    target 146
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 68
  ]
  edge [
    source 57
    target 111
  ]
  edge [
    source 57
    target 112
  ]
  edge [
    source 57
    target 59
  ]
  edge [
    source 57
    target 134
  ]
  edge [
    source 57
    target 576
  ]
  edge [
    source 57
    target 577
  ]
  edge [
    source 57
    target 578
  ]
  edge [
    source 57
    target 579
  ]
  edge [
    source 57
    target 580
  ]
  edge [
    source 57
    target 581
  ]
  edge [
    source 57
    target 427
  ]
  edge [
    source 57
    target 582
  ]
  edge [
    source 57
    target 583
  ]
  edge [
    source 57
    target 584
  ]
  edge [
    source 57
    target 123
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 585
  ]
  edge [
    source 58
    target 586
  ]
  edge [
    source 58
    target 587
  ]
  edge [
    source 58
    target 588
  ]
  edge [
    source 58
    target 589
  ]
  edge [
    source 58
    target 590
  ]
  edge [
    source 58
    target 591
  ]
  edge [
    source 58
    target 592
  ]
  edge [
    source 58
    target 66
  ]
  edge [
    source 58
    target 132
  ]
  edge [
    source 58
    target 110
  ]
  edge [
    source 58
    target 130
  ]
  edge [
    source 59
    target 133
  ]
  edge [
    source 59
    target 593
  ]
  edge [
    source 59
    target 594
  ]
  edge [
    source 59
    target 595
  ]
  edge [
    source 59
    target 596
  ]
  edge [
    source 59
    target 597
  ]
  edge [
    source 59
    target 598
  ]
  edge [
    source 59
    target 94
  ]
  edge [
    source 59
    target 108
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 599
  ]
  edge [
    source 60
    target 600
  ]
  edge [
    source 60
    target 601
  ]
  edge [
    source 60
    target 602
  ]
  edge [
    source 60
    target 603
  ]
  edge [
    source 60
    target 604
  ]
  edge [
    source 60
    target 605
  ]
  edge [
    source 60
    target 606
  ]
  edge [
    source 60
    target 607
  ]
  edge [
    source 60
    target 608
  ]
  edge [
    source 60
    target 609
  ]
  edge [
    source 60
    target 610
  ]
  edge [
    source 60
    target 611
  ]
  edge [
    source 60
    target 612
  ]
  edge [
    source 60
    target 613
  ]
  edge [
    source 60
    target 614
  ]
  edge [
    source 60
    target 615
  ]
  edge [
    source 60
    target 616
  ]
  edge [
    source 60
    target 617
  ]
  edge [
    source 60
    target 618
  ]
  edge [
    source 60
    target 619
  ]
  edge [
    source 60
    target 620
  ]
  edge [
    source 60
    target 621
  ]
  edge [
    source 60
    target 622
  ]
  edge [
    source 60
    target 623
  ]
  edge [
    source 60
    target 624
  ]
  edge [
    source 60
    target 625
  ]
  edge [
    source 60
    target 626
  ]
  edge [
    source 60
    target 627
  ]
  edge [
    source 60
    target 628
  ]
  edge [
    source 60
    target 629
  ]
  edge [
    source 60
    target 630
  ]
  edge [
    source 60
    target 631
  ]
  edge [
    source 60
    target 632
  ]
  edge [
    source 60
    target 633
  ]
  edge [
    source 60
    target 634
  ]
  edge [
    source 60
    target 635
  ]
  edge [
    source 60
    target 636
  ]
  edge [
    source 60
    target 637
  ]
  edge [
    source 60
    target 638
  ]
  edge [
    source 60
    target 639
  ]
  edge [
    source 60
    target 640
  ]
  edge [
    source 60
    target 641
  ]
  edge [
    source 60
    target 642
  ]
  edge [
    source 60
    target 643
  ]
  edge [
    source 60
    target 644
  ]
  edge [
    source 60
    target 645
  ]
  edge [
    source 60
    target 646
  ]
  edge [
    source 60
    target 647
  ]
  edge [
    source 60
    target 648
  ]
  edge [
    source 60
    target 649
  ]
  edge [
    source 60
    target 650
  ]
  edge [
    source 60
    target 651
  ]
  edge [
    source 60
    target 652
  ]
  edge [
    source 60
    target 653
  ]
  edge [
    source 60
    target 654
  ]
  edge [
    source 60
    target 655
  ]
  edge [
    source 60
    target 656
  ]
  edge [
    source 60
    target 657
  ]
  edge [
    source 60
    target 658
  ]
  edge [
    source 60
    target 659
  ]
  edge [
    source 60
    target 660
  ]
  edge [
    source 60
    target 661
  ]
  edge [
    source 60
    target 662
  ]
  edge [
    source 60
    target 663
  ]
  edge [
    source 60
    target 664
  ]
  edge [
    source 60
    target 665
  ]
  edge [
    source 60
    target 666
  ]
  edge [
    source 60
    target 667
  ]
  edge [
    source 60
    target 668
  ]
  edge [
    source 60
    target 669
  ]
  edge [
    source 60
    target 670
  ]
  edge [
    source 60
    target 671
  ]
  edge [
    source 60
    target 672
  ]
  edge [
    source 60
    target 673
  ]
  edge [
    source 60
    target 674
  ]
  edge [
    source 60
    target 675
  ]
  edge [
    source 60
    target 676
  ]
  edge [
    source 60
    target 677
  ]
  edge [
    source 60
    target 678
  ]
  edge [
    source 60
    target 679
  ]
  edge [
    source 60
    target 680
  ]
  edge [
    source 60
    target 681
  ]
  edge [
    source 60
    target 682
  ]
  edge [
    source 60
    target 683
  ]
  edge [
    source 60
    target 684
  ]
  edge [
    source 60
    target 685
  ]
  edge [
    source 60
    target 686
  ]
  edge [
    source 60
    target 687
  ]
  edge [
    source 60
    target 688
  ]
  edge [
    source 60
    target 689
  ]
  edge [
    source 60
    target 690
  ]
  edge [
    source 60
    target 691
  ]
  edge [
    source 60
    target 692
  ]
  edge [
    source 60
    target 693
  ]
  edge [
    source 60
    target 694
  ]
  edge [
    source 60
    target 695
  ]
  edge [
    source 60
    target 696
  ]
  edge [
    source 60
    target 697
  ]
  edge [
    source 60
    target 698
  ]
  edge [
    source 60
    target 699
  ]
  edge [
    source 60
    target 700
  ]
  edge [
    source 60
    target 701
  ]
  edge [
    source 60
    target 702
  ]
  edge [
    source 60
    target 703
  ]
  edge [
    source 60
    target 704
  ]
  edge [
    source 60
    target 705
  ]
  edge [
    source 60
    target 706
  ]
  edge [
    source 60
    target 707
  ]
  edge [
    source 60
    target 708
  ]
  edge [
    source 60
    target 709
  ]
  edge [
    source 60
    target 710
  ]
  edge [
    source 60
    target 711
  ]
  edge [
    source 60
    target 712
  ]
  edge [
    source 60
    target 713
  ]
  edge [
    source 60
    target 714
  ]
  edge [
    source 60
    target 715
  ]
  edge [
    source 60
    target 716
  ]
  edge [
    source 60
    target 717
  ]
  edge [
    source 60
    target 718
  ]
  edge [
    source 60
    target 719
  ]
  edge [
    source 60
    target 720
  ]
  edge [
    source 60
    target 721
  ]
  edge [
    source 60
    target 722
  ]
  edge [
    source 60
    target 723
  ]
  edge [
    source 60
    target 724
  ]
  edge [
    source 60
    target 725
  ]
  edge [
    source 60
    target 726
  ]
  edge [
    source 60
    target 727
  ]
  edge [
    source 60
    target 728
  ]
  edge [
    source 60
    target 729
  ]
  edge [
    source 60
    target 730
  ]
  edge [
    source 60
    target 731
  ]
  edge [
    source 60
    target 732
  ]
  edge [
    source 60
    target 733
  ]
  edge [
    source 60
    target 734
  ]
  edge [
    source 60
    target 735
  ]
  edge [
    source 60
    target 736
  ]
  edge [
    source 60
    target 737
  ]
  edge [
    source 60
    target 738
  ]
  edge [
    source 60
    target 739
  ]
  edge [
    source 60
    target 740
  ]
  edge [
    source 60
    target 741
  ]
  edge [
    source 60
    target 742
  ]
  edge [
    source 60
    target 743
  ]
  edge [
    source 60
    target 744
  ]
  edge [
    source 60
    target 745
  ]
  edge [
    source 60
    target 746
  ]
  edge [
    source 60
    target 747
  ]
  edge [
    source 60
    target 748
  ]
  edge [
    source 60
    target 749
  ]
  edge [
    source 60
    target 750
  ]
  edge [
    source 60
    target 751
  ]
  edge [
    source 60
    target 752
  ]
  edge [
    source 60
    target 753
  ]
  edge [
    source 60
    target 754
  ]
  edge [
    source 60
    target 755
  ]
  edge [
    source 60
    target 756
  ]
  edge [
    source 60
    target 757
  ]
  edge [
    source 60
    target 758
  ]
  edge [
    source 60
    target 759
  ]
  edge [
    source 60
    target 760
  ]
  edge [
    source 60
    target 761
  ]
  edge [
    source 60
    target 762
  ]
  edge [
    source 60
    target 763
  ]
  edge [
    source 60
    target 764
  ]
  edge [
    source 60
    target 765
  ]
  edge [
    source 60
    target 766
  ]
  edge [
    source 60
    target 767
  ]
  edge [
    source 60
    target 768
  ]
  edge [
    source 60
    target 769
  ]
  edge [
    source 60
    target 770
  ]
  edge [
    source 60
    target 771
  ]
  edge [
    source 60
    target 772
  ]
  edge [
    source 60
    target 773
  ]
  edge [
    source 60
    target 774
  ]
  edge [
    source 60
    target 775
  ]
  edge [
    source 60
    target 776
  ]
  edge [
    source 60
    target 777
  ]
  edge [
    source 60
    target 778
  ]
  edge [
    source 60
    target 779
  ]
  edge [
    source 60
    target 780
  ]
  edge [
    source 60
    target 781
  ]
  edge [
    source 60
    target 782
  ]
  edge [
    source 60
    target 783
  ]
  edge [
    source 60
    target 784
  ]
  edge [
    source 60
    target 785
  ]
  edge [
    source 60
    target 786
  ]
  edge [
    source 60
    target 787
  ]
  edge [
    source 60
    target 788
  ]
  edge [
    source 60
    target 789
  ]
  edge [
    source 60
    target 790
  ]
  edge [
    source 60
    target 791
  ]
  edge [
    source 60
    target 792
  ]
  edge [
    source 60
    target 793
  ]
  edge [
    source 60
    target 794
  ]
  edge [
    source 60
    target 795
  ]
  edge [
    source 60
    target 796
  ]
  edge [
    source 60
    target 797
  ]
  edge [
    source 60
    target 798
  ]
  edge [
    source 60
    target 799
  ]
  edge [
    source 60
    target 800
  ]
  edge [
    source 60
    target 801
  ]
  edge [
    source 60
    target 802
  ]
  edge [
    source 60
    target 803
  ]
  edge [
    source 60
    target 804
  ]
  edge [
    source 60
    target 805
  ]
  edge [
    source 60
    target 806
  ]
  edge [
    source 60
    target 807
  ]
  edge [
    source 60
    target 808
  ]
  edge [
    source 60
    target 809
  ]
  edge [
    source 60
    target 810
  ]
  edge [
    source 60
    target 811
  ]
  edge [
    source 60
    target 812
  ]
  edge [
    source 60
    target 813
  ]
  edge [
    source 60
    target 814
  ]
  edge [
    source 60
    target 815
  ]
  edge [
    source 60
    target 816
  ]
  edge [
    source 60
    target 817
  ]
  edge [
    source 60
    target 818
  ]
  edge [
    source 60
    target 819
  ]
  edge [
    source 60
    target 820
  ]
  edge [
    source 60
    target 821
  ]
  edge [
    source 60
    target 822
  ]
  edge [
    source 60
    target 823
  ]
  edge [
    source 60
    target 824
  ]
  edge [
    source 60
    target 825
  ]
  edge [
    source 60
    target 826
  ]
  edge [
    source 60
    target 827
  ]
  edge [
    source 60
    target 828
  ]
  edge [
    source 60
    target 829
  ]
  edge [
    source 60
    target 830
  ]
  edge [
    source 60
    target 831
  ]
  edge [
    source 60
    target 832
  ]
  edge [
    source 60
    target 833
  ]
  edge [
    source 60
    target 834
  ]
  edge [
    source 60
    target 835
  ]
  edge [
    source 60
    target 836
  ]
  edge [
    source 60
    target 837
  ]
  edge [
    source 60
    target 838
  ]
  edge [
    source 60
    target 839
  ]
  edge [
    source 60
    target 840
  ]
  edge [
    source 60
    target 841
  ]
  edge [
    source 60
    target 842
  ]
  edge [
    source 60
    target 843
  ]
  edge [
    source 60
    target 844
  ]
  edge [
    source 60
    target 845
  ]
  edge [
    source 60
    target 846
  ]
  edge [
    source 60
    target 847
  ]
  edge [
    source 60
    target 848
  ]
  edge [
    source 60
    target 849
  ]
  edge [
    source 60
    target 850
  ]
  edge [
    source 60
    target 851
  ]
  edge [
    source 60
    target 852
  ]
  edge [
    source 60
    target 853
  ]
  edge [
    source 60
    target 854
  ]
  edge [
    source 60
    target 855
  ]
  edge [
    source 60
    target 856
  ]
  edge [
    source 60
    target 857
  ]
  edge [
    source 60
    target 858
  ]
  edge [
    source 60
    target 859
  ]
  edge [
    source 60
    target 860
  ]
  edge [
    source 60
    target 861
  ]
  edge [
    source 60
    target 862
  ]
  edge [
    source 60
    target 863
  ]
  edge [
    source 60
    target 864
  ]
  edge [
    source 60
    target 865
  ]
  edge [
    source 60
    target 866
  ]
  edge [
    source 60
    target 867
  ]
  edge [
    source 60
    target 868
  ]
  edge [
    source 60
    target 869
  ]
  edge [
    source 60
    target 870
  ]
  edge [
    source 60
    target 871
  ]
  edge [
    source 60
    target 872
  ]
  edge [
    source 60
    target 873
  ]
  edge [
    source 60
    target 874
  ]
  edge [
    source 60
    target 875
  ]
  edge [
    source 60
    target 876
  ]
  edge [
    source 60
    target 877
  ]
  edge [
    source 60
    target 878
  ]
  edge [
    source 60
    target 879
  ]
  edge [
    source 60
    target 880
  ]
  edge [
    source 60
    target 881
  ]
  edge [
    source 60
    target 882
  ]
  edge [
    source 60
    target 883
  ]
  edge [
    source 60
    target 884
  ]
  edge [
    source 60
    target 885
  ]
  edge [
    source 60
    target 886
  ]
  edge [
    source 60
    target 887
  ]
  edge [
    source 60
    target 888
  ]
  edge [
    source 60
    target 889
  ]
  edge [
    source 60
    target 890
  ]
  edge [
    source 60
    target 891
  ]
  edge [
    source 60
    target 892
  ]
  edge [
    source 60
    target 893
  ]
  edge [
    source 60
    target 894
  ]
  edge [
    source 60
    target 895
  ]
  edge [
    source 60
    target 896
  ]
  edge [
    source 60
    target 897
  ]
  edge [
    source 60
    target 898
  ]
  edge [
    source 60
    target 899
  ]
  edge [
    source 60
    target 900
  ]
  edge [
    source 60
    target 901
  ]
  edge [
    source 60
    target 902
  ]
  edge [
    source 60
    target 903
  ]
  edge [
    source 60
    target 904
  ]
  edge [
    source 60
    target 905
  ]
  edge [
    source 60
    target 906
  ]
  edge [
    source 60
    target 907
  ]
  edge [
    source 60
    target 908
  ]
  edge [
    source 60
    target 909
  ]
  edge [
    source 60
    target 910
  ]
  edge [
    source 60
    target 911
  ]
  edge [
    source 60
    target 912
  ]
  edge [
    source 60
    target 913
  ]
  edge [
    source 60
    target 914
  ]
  edge [
    source 60
    target 915
  ]
  edge [
    source 60
    target 916
  ]
  edge [
    source 60
    target 917
  ]
  edge [
    source 60
    target 918
  ]
  edge [
    source 60
    target 919
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 65
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 581
  ]
  edge [
    source 62
    target 920
  ]
  edge [
    source 62
    target 921
  ]
  edge [
    source 62
    target 922
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 923
  ]
  edge [
    source 64
    target 924
  ]
  edge [
    source 64
    target 925
  ]
  edge [
    source 64
    target 105
  ]
  edge [
    source 66
    target 84
  ]
  edge [
    source 66
    target 926
  ]
  edge [
    source 66
    target 927
  ]
  edge [
    source 66
    target 928
  ]
  edge [
    source 66
    target 929
  ]
  edge [
    source 66
    target 589
  ]
  edge [
    source 66
    target 930
  ]
  edge [
    source 66
    target 931
  ]
  edge [
    source 66
    target 113
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 67
    target 88
  ]
  edge [
    source 67
    target 89
  ]
  edge [
    source 68
    target 932
  ]
  edge [
    source 68
    target 933
  ]
  edge [
    source 68
    target 934
  ]
  edge [
    source 68
    target 935
  ]
  edge [
    source 68
    target 88
  ]
  edge [
    source 68
    target 89
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 936
  ]
  edge [
    source 70
    target 937
  ]
  edge [
    source 70
    target 938
  ]
  edge [
    source 70
    target 939
  ]
  edge [
    source 70
    target 940
  ]
  edge [
    source 70
    target 941
  ]
  edge [
    source 70
    target 942
  ]
  edge [
    source 70
    target 943
  ]
  edge [
    source 70
    target 93
  ]
  edge [
    source 70
    target 86
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 71
    target 85
  ]
  edge [
    source 71
    target 944
  ]
  edge [
    source 71
    target 156
  ]
  edge [
    source 71
    target 945
  ]
  edge [
    source 71
    target 411
  ]
  edge [
    source 71
    target 946
  ]
  edge [
    source 71
    target 947
  ]
  edge [
    source 71
    target 502
  ]
  edge [
    source 71
    target 948
  ]
  edge [
    source 71
    target 949
  ]
  edge [
    source 71
    target 950
  ]
  edge [
    source 71
    target 164
  ]
  edge [
    source 71
    target 93
  ]
  edge [
    source 72
    target 951
  ]
  edge [
    source 72
    target 952
  ]
  edge [
    source 72
    target 953
  ]
  edge [
    source 72
    target 92
  ]
  edge [
    source 72
    target 954
  ]
  edge [
    source 72
    target 955
  ]
  edge [
    source 72
    target 956
  ]
  edge [
    source 72
    target 957
  ]
  edge [
    source 72
    target 958
  ]
  edge [
    source 72
    target 959
  ]
  edge [
    source 72
    target 960
  ]
  edge [
    source 72
    target 90
  ]
  edge [
    source 72
    target 148
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 73
    target 81
  ]
  edge [
    source 73
    target 136
  ]
  edge [
    source 74
    target 961
  ]
  edge [
    source 74
    target 962
  ]
  edge [
    source 74
    target 963
  ]
  edge [
    source 74
    target 964
  ]
  edge [
    source 74
    target 965
  ]
  edge [
    source 74
    target 966
  ]
  edge [
    source 74
    target 967
  ]
  edge [
    source 75
    target 968
  ]
  edge [
    source 75
    target 969
  ]
  edge [
    source 75
    target 970
  ]
  edge [
    source 75
    target 971
  ]
  edge [
    source 75
    target 972
  ]
  edge [
    source 75
    target 973
  ]
  edge [
    source 75
    target 100
  ]
  edge [
    source 76
    target 77
  ]
  edge [
    source 76
    target 974
  ]
  edge [
    source 76
    target 975
  ]
  edge [
    source 76
    target 976
  ]
  edge [
    source 76
    target 977
  ]
  edge [
    source 76
    target 978
  ]
  edge [
    source 76
    target 979
  ]
  edge [
    source 76
    target 980
  ]
  edge [
    source 76
    target 981
  ]
  edge [
    source 76
    target 982
  ]
  edge [
    source 76
    target 102
  ]
  edge [
    source 76
    target 96
  ]
  edge [
    source 77
    target 117
  ]
  edge [
    source 77
    target 983
  ]
  edge [
    source 78
    target 222
  ]
  edge [
    source 78
    target 984
  ]
  edge [
    source 78
    target 985
  ]
  edge [
    source 78
    target 986
  ]
  edge [
    source 78
    target 987
  ]
  edge [
    source 78
    target 742
  ]
  edge [
    source 78
    target 988
  ]
  edge [
    source 78
    target 989
  ]
  edge [
    source 78
    target 990
  ]
  edge [
    source 78
    target 279
  ]
  edge [
    source 78
    target 950
  ]
  edge [
    source 78
    target 991
  ]
  edge [
    source 79
    target 80
  ]
  edge [
    source 79
    target 580
  ]
  edge [
    source 79
    target 84
  ]
  edge [
    source 80
    target 81
  ]
  edge [
    source 80
    target 120
  ]
  edge [
    source 80
    target 121
  ]
  edge [
    source 81
    target 223
  ]
  edge [
    source 81
    target 992
  ]
  edge [
    source 81
    target 993
  ]
  edge [
    source 81
    target 994
  ]
  edge [
    source 81
    target 995
  ]
  edge [
    source 81
    target 996
  ]
  edge [
    source 81
    target 997
  ]
  edge [
    source 81
    target 443
  ]
  edge [
    source 81
    target 567
  ]
  edge [
    source 81
    target 998
  ]
  edge [
    source 81
    target 136
  ]
  edge [
    source 82
    target 83
  ]
  edge [
    source 82
    target 999
  ]
  edge [
    source 82
    target 1000
  ]
  edge [
    source 82
    target 1001
  ]
  edge [
    source 82
    target 1002
  ]
  edge [
    source 82
    target 1003
  ]
  edge [
    source 82
    target 1004
  ]
  edge [
    source 82
    target 1005
  ]
  edge [
    source 82
    target 1006
  ]
  edge [
    source 82
    target 1007
  ]
  edge [
    source 82
    target 1008
  ]
  edge [
    source 82
    target 1009
  ]
  edge [
    source 82
    target 1010
  ]
  edge [
    source 83
    target 84
  ]
  edge [
    source 83
    target 1011
  ]
  edge [
    source 83
    target 1012
  ]
  edge [
    source 83
    target 1013
  ]
  edge [
    source 83
    target 1014
  ]
  edge [
    source 83
    target 1015
  ]
  edge [
    source 83
    target 946
  ]
  edge [
    source 83
    target 1016
  ]
  edge [
    source 83
    target 1017
  ]
  edge [
    source 83
    target 1018
  ]
  edge [
    source 83
    target 1019
  ]
  edge [
    source 83
    target 1020
  ]
  edge [
    source 83
    target 1021
  ]
  edge [
    source 84
    target 581
  ]
  edge [
    source 84
    target 249
  ]
  edge [
    source 84
    target 1022
  ]
  edge [
    source 84
    target 1023
  ]
  edge [
    source 84
    target 1024
  ]
  edge [
    source 84
    target 920
  ]
  edge [
    source 84
    target 1025
  ]
  edge [
    source 84
    target 1026
  ]
  edge [
    source 84
    target 922
  ]
  edge [
    source 84
    target 960
  ]
  edge [
    source 84
    target 921
  ]
  edge [
    source 84
    target 111
  ]
  edge [
    source 85
    target 120
  ]
  edge [
    source 86
    target 87
  ]
  edge [
    source 86
    target 434
  ]
  edge [
    source 87
    target 88
  ]
  edge [
    source 87
    target 93
  ]
  edge [
    source 87
    target 116
  ]
  edge [
    source 87
    target 1027
  ]
  edge [
    source 87
    target 1028
  ]
  edge [
    source 87
    target 109
  ]
  edge [
    source 88
    target 89
  ]
  edge [
    source 89
    target 90
  ]
  edge [
    source 89
    target 1029
  ]
  edge [
    source 89
    target 1030
  ]
  edge [
    source 89
    target 1031
  ]
  edge [
    source 89
    target 1032
  ]
  edge [
    source 89
    target 1033
  ]
  edge [
    source 89
    target 1034
  ]
  edge [
    source 89
    target 1035
  ]
  edge [
    source 89
    target 1036
  ]
  edge [
    source 89
    target 1037
  ]
  edge [
    source 89
    target 1038
  ]
  edge [
    source 90
    target 91
  ]
  edge [
    source 90
    target 1039
  ]
  edge [
    source 90
    target 1040
  ]
  edge [
    source 90
    target 1041
  ]
  edge [
    source 90
    target 1042
  ]
  edge [
    source 90
    target 1043
  ]
  edge [
    source 90
    target 1044
  ]
  edge [
    source 90
    target 1045
  ]
  edge [
    source 90
    target 1046
  ]
  edge [
    source 90
    target 1047
  ]
  edge [
    source 90
    target 1048
  ]
  edge [
    source 90
    target 1049
  ]
  edge [
    source 90
    target 1050
  ]
  edge [
    source 90
    target 1051
  ]
  edge [
    source 90
    target 148
  ]
  edge [
    source 91
    target 92
  ]
  edge [
    source 91
    target 1052
  ]
  edge [
    source 91
    target 226
  ]
  edge [
    source 91
    target 922
  ]
  edge [
    source 91
    target 1053
  ]
  edge [
    source 91
    target 1054
  ]
  edge [
    source 91
    target 100
  ]
  edge [
    source 91
    target 105
  ]
  edge [
    source 91
    target 98
  ]
  edge [
    source 91
    target 149
  ]
  edge [
    source 92
    target 93
  ]
  edge [
    source 92
    target 233
  ]
  edge [
    source 92
    target 1055
  ]
  edge [
    source 92
    target 459
  ]
  edge [
    source 92
    target 1056
  ]
  edge [
    source 92
    target 1057
  ]
  edge [
    source 93
    target 94
  ]
  edge [
    source 93
    target 115
  ]
  edge [
    source 93
    target 262
  ]
  edge [
    source 93
    target 1058
  ]
  edge [
    source 94
    target 1059
  ]
  edge [
    source 94
    target 1060
  ]
  edge [
    source 94
    target 1061
  ]
  edge [
    source 95
    target 96
  ]
  edge [
    source 95
    target 1062
  ]
  edge [
    source 95
    target 1063
  ]
  edge [
    source 95
    target 1064
  ]
  edge [
    source 95
    target 1065
  ]
  edge [
    source 95
    target 1066
  ]
  edge [
    source 95
    target 1067
  ]
  edge [
    source 96
    target 97
  ]
  edge [
    source 96
    target 106
  ]
  edge [
    source 96
    target 1068
  ]
  edge [
    source 96
    target 146
  ]
  edge [
    source 97
    target 98
  ]
  edge [
    source 97
    target 1069
  ]
  edge [
    source 97
    target 1070
  ]
  edge [
    source 97
    target 1071
  ]
  edge [
    source 97
    target 1072
  ]
  edge [
    source 97
    target 1073
  ]
  edge [
    source 97
    target 1074
  ]
  edge [
    source 97
    target 1075
  ]
  edge [
    source 97
    target 1049
  ]
  edge [
    source 97
    target 1076
  ]
  edge [
    source 98
    target 99
  ]
  edge [
    source 98
    target 149
  ]
  edge [
    source 99
    target 100
  ]
  edge [
    source 99
    target 722
  ]
  edge [
    source 99
    target 1077
  ]
  edge [
    source 100
    target 101
  ]
  edge [
    source 100
    target 1078
  ]
  edge [
    source 100
    target 1079
  ]
  edge [
    source 100
    target 1080
  ]
  edge [
    source 100
    target 1081
  ]
  edge [
    source 100
    target 1082
  ]
  edge [
    source 101
    target 102
  ]
  edge [
    source 101
    target 589
  ]
  edge [
    source 101
    target 1083
  ]
  edge [
    source 102
    target 103
  ]
  edge [
    source 102
    target 1084
  ]
  edge [
    source 102
    target 1085
  ]
  edge [
    source 102
    target 1086
  ]
  edge [
    source 102
    target 1087
  ]
  edge [
    source 102
    target 1088
  ]
  edge [
    source 102
    target 1089
  ]
  edge [
    source 102
    target 1090
  ]
  edge [
    source 103
    target 104
  ]
  edge [
    source 103
    target 153
  ]
  edge [
    source 104
    target 105
  ]
  edge [
    source 104
    target 1091
  ]
  edge [
    source 105
    target 1092
  ]
  edge [
    source 105
    target 1093
  ]
  edge [
    source 105
    target 1094
  ]
  edge [
    source 106
    target 1095
  ]
  edge [
    source 107
    target 108
  ]
  edge [
    source 108
    target 109
  ]
  edge [
    source 109
    target 110
  ]
  edge [
    source 110
    target 111
  ]
  edge [
    source 110
    target 1096
  ]
  edge [
    source 110
    target 546
  ]
  edge [
    source 110
    target 1097
  ]
  edge [
    source 110
    target 541
  ]
  edge [
    source 110
    target 1098
  ]
  edge [
    source 110
    target 1099
  ]
  edge [
    source 110
    target 1100
  ]
  edge [
    source 110
    target 1101
  ]
  edge [
    source 110
    target 1102
  ]
  edge [
    source 110
    target 1103
  ]
  edge [
    source 110
    target 1104
  ]
  edge [
    source 110
    target 1105
  ]
  edge [
    source 110
    target 1106
  ]
  edge [
    source 110
    target 1107
  ]
  edge [
    source 110
    target 1108
  ]
  edge [
    source 110
    target 1109
  ]
  edge [
    source 110
    target 1110
  ]
  edge [
    source 110
    target 1111
  ]
  edge [
    source 110
    target 121
  ]
  edge [
    source 110
    target 135
  ]
  edge [
    source 111
    target 1112
  ]
  edge [
    source 111
    target 1113
  ]
  edge [
    source 112
    target 113
  ]
  edge [
    source 112
    target 1114
  ]
  edge [
    source 112
    target 1115
  ]
  edge [
    source 112
    target 1116
  ]
  edge [
    source 112
    target 748
  ]
  edge [
    source 112
    target 1117
  ]
  edge [
    source 112
    target 1118
  ]
  edge [
    source 112
    target 1119
  ]
  edge [
    source 112
    target 1120
  ]
  edge [
    source 112
    target 1121
  ]
  edge [
    source 112
    target 1122
  ]
  edge [
    source 112
    target 1123
  ]
  edge [
    source 112
    target 1124
  ]
  edge [
    source 112
    target 1125
  ]
  edge [
    source 112
    target 1126
  ]
  edge [
    source 112
    target 1127
  ]
  edge [
    source 112
    target 1128
  ]
  edge [
    source 112
    target 1129
  ]
  edge [
    source 112
    target 1130
  ]
  edge [
    source 112
    target 1131
  ]
  edge [
    source 112
    target 1132
  ]
  edge [
    source 112
    target 1133
  ]
  edge [
    source 112
    target 1134
  ]
  edge [
    source 112
    target 1135
  ]
  edge [
    source 112
    target 1136
  ]
  edge [
    source 112
    target 1137
  ]
  edge [
    source 112
    target 1138
  ]
  edge [
    source 112
    target 1139
  ]
  edge [
    source 112
    target 1140
  ]
  edge [
    source 112
    target 223
  ]
  edge [
    source 112
    target 1141
  ]
  edge [
    source 112
    target 1142
  ]
  edge [
    source 112
    target 1143
  ]
  edge [
    source 112
    target 1144
  ]
  edge [
    source 112
    target 1145
  ]
  edge [
    source 112
    target 1146
  ]
  edge [
    source 112
    target 1147
  ]
  edge [
    source 112
    target 1148
  ]
  edge [
    source 112
    target 1149
  ]
  edge [
    source 112
    target 1150
  ]
  edge [
    source 113
    target 114
  ]
  edge [
    source 113
    target 1151
  ]
  edge [
    source 113
    target 1152
  ]
  edge [
    source 113
    target 982
  ]
  edge [
    source 113
    target 1153
  ]
  edge [
    source 114
    target 115
  ]
  edge [
    source 115
    target 1154
  ]
  edge [
    source 115
    target 1155
  ]
  edge [
    source 115
    target 1156
  ]
  edge [
    source 115
    target 1157
  ]
  edge [
    source 115
    target 1158
  ]
  edge [
    source 115
    target 1159
  ]
  edge [
    source 115
    target 1160
  ]
  edge [
    source 115
    target 1161
  ]
  edge [
    source 115
    target 1162
  ]
  edge [
    source 115
    target 1163
  ]
  edge [
    source 115
    target 1164
  ]
  edge [
    source 115
    target 1165
  ]
  edge [
    source 115
    target 1166
  ]
  edge [
    source 115
    target 1167
  ]
  edge [
    source 115
    target 411
  ]
  edge [
    source 115
    target 1168
  ]
  edge [
    source 115
    target 1169
  ]
  edge [
    source 115
    target 1170
  ]
  edge [
    source 115
    target 1171
  ]
  edge [
    source 115
    target 1172
  ]
  edge [
    source 115
    target 1173
  ]
  edge [
    source 115
    target 1174
  ]
  edge [
    source 115
    target 1175
  ]
  edge [
    source 115
    target 1176
  ]
  edge [
    source 115
    target 1177
  ]
  edge [
    source 115
    target 1178
  ]
  edge [
    source 117
    target 118
  ]
  edge [
    source 117
    target 1179
  ]
  edge [
    source 117
    target 1180
  ]
  edge [
    source 117
    target 1181
  ]
  edge [
    source 117
    target 1182
  ]
  edge [
    source 117
    target 1183
  ]
  edge [
    source 117
    target 1184
  ]
  edge [
    source 117
    target 1185
  ]
  edge [
    source 118
    target 119
  ]
  edge [
    source 118
    target 1186
  ]
  edge [
    source 118
    target 1187
  ]
  edge [
    source 119
    target 1188
  ]
  edge [
    source 119
    target 1189
  ]
  edge [
    source 119
    target 1190
  ]
  edge [
    source 119
    target 1191
  ]
  edge [
    source 119
    target 1192
  ]
  edge [
    source 120
    target 1193
  ]
  edge [
    source 120
    target 1194
  ]
  edge [
    source 120
    target 1195
  ]
  edge [
    source 120
    target 237
  ]
  edge [
    source 120
    target 260
  ]
  edge [
    source 120
    target 1056
  ]
  edge [
    source 120
    target 240
  ]
  edge [
    source 120
    target 123
  ]
  edge [
    source 121
    target 122
  ]
  edge [
    source 121
    target 1196
  ]
  edge [
    source 121
    target 1197
  ]
  edge [
    source 121
    target 1198
  ]
  edge [
    source 122
    target 123
  ]
  edge [
    source 122
    target 1197
  ]
  edge [
    source 123
    target 124
  ]
  edge [
    source 123
    target 1199
  ]
  edge [
    source 123
    target 1200
  ]
  edge [
    source 123
    target 1201
  ]
  edge [
    source 123
    target 459
  ]
  edge [
    source 123
    target 1202
  ]
  edge [
    source 123
    target 1203
  ]
  edge [
    source 123
    target 1204
  ]
  edge [
    source 123
    target 1205
  ]
  edge [
    source 124
    target 125
  ]
  edge [
    source 124
    target 1206
  ]
  edge [
    source 124
    target 1207
  ]
  edge [
    source 124
    target 1208
  ]
  edge [
    source 125
    target 126
  ]
  edge [
    source 125
    target 1209
  ]
  edge [
    source 125
    target 1210
  ]
  edge [
    source 125
    target 1211
  ]
  edge [
    source 125
    target 1212
  ]
  edge [
    source 126
    target 127
  ]
  edge [
    source 127
    target 128
  ]
  edge [
    source 127
    target 1213
  ]
  edge [
    source 127
    target 1214
  ]
  edge [
    source 127
    target 1215
  ]
  edge [
    source 128
    target 129
  ]
  edge [
    source 129
    target 1216
  ]
  edge [
    source 129
    target 1217
  ]
  edge [
    source 129
    target 1218
  ]
  edge [
    source 129
    target 1219
  ]
  edge [
    source 129
    target 1220
  ]
  edge [
    source 130
    target 143
  ]
  edge [
    source 131
    target 132
  ]
  edge [
    source 131
    target 1221
  ]
  edge [
    source 131
    target 1222
  ]
  edge [
    source 131
    target 1223
  ]
  edge [
    source 131
    target 1224
  ]
  edge [
    source 131
    target 296
  ]
  edge [
    source 132
    target 133
  ]
  edge [
    source 132
    target 1225
  ]
  edge [
    source 132
    target 1226
  ]
  edge [
    source 132
    target 1227
  ]
  edge [
    source 132
    target 589
  ]
  edge [
    source 132
    target 1228
  ]
  edge [
    source 132
    target 930
  ]
  edge [
    source 132
    target 1229
  ]
  edge [
    source 132
    target 1230
  ]
  edge [
    source 133
    target 527
  ]
  edge [
    source 133
    target 1231
  ]
  edge [
    source 133
    target 1232
  ]
  edge [
    source 134
    target 135
  ]
  edge [
    source 134
    target 1233
  ]
  edge [
    source 134
    target 1234
  ]
  edge [
    source 134
    target 1235
  ]
  edge [
    source 134
    target 1236
  ]
  edge [
    source 134
    target 1237
  ]
  edge [
    source 134
    target 1238
  ]
  edge [
    source 134
    target 1239
  ]
  edge [
    source 134
    target 1240
  ]
  edge [
    source 134
    target 1241
  ]
  edge [
    source 134
    target 1242
  ]
  edge [
    source 134
    target 1243
  ]
  edge [
    source 134
    target 1244
  ]
  edge [
    source 134
    target 1245
  ]
  edge [
    source 134
    target 1246
  ]
  edge [
    source 134
    target 1247
  ]
  edge [
    source 134
    target 1248
  ]
  edge [
    source 134
    target 1249
  ]
  edge [
    source 134
    target 1250
  ]
  edge [
    source 134
    target 1251
  ]
  edge [
    source 134
    target 1252
  ]
  edge [
    source 134
    target 1253
  ]
  edge [
    source 134
    target 1254
  ]
  edge [
    source 134
    target 1255
  ]
  edge [
    source 135
    target 136
  ]
  edge [
    source 136
    target 137
  ]
  edge [
    source 137
    target 138
  ]
  edge [
    source 138
    target 139
  ]
  edge [
    source 139
    target 140
  ]
  edge [
    source 140
    target 141
  ]
  edge [
    source 141
    target 142
  ]
  edge [
    source 141
    target 1256
  ]
  edge [
    source 142
    target 143
  ]
  edge [
    source 143
    target 144
  ]
  edge [
    source 143
    target 151
  ]
  edge [
    source 143
    target 1257
  ]
  edge [
    source 143
    target 1052
  ]
  edge [
    source 143
    target 1060
  ]
  edge [
    source 143
    target 1258
  ]
  edge [
    source 143
    target 214
  ]
  edge [
    source 143
    target 1259
  ]
  edge [
    source 143
    target 1260
  ]
  edge [
    source 143
    target 1261
  ]
  edge [
    source 143
    target 1262
  ]
  edge [
    source 143
    target 1121
  ]
  edge [
    source 143
    target 444
  ]
  edge [
    source 143
    target 1263
  ]
  edge [
    source 143
    target 1111
  ]
  edge [
    source 143
    target 1264
  ]
  edge [
    source 144
    target 145
  ]
  edge [
    source 144
    target 1265
  ]
  edge [
    source 145
    target 146
  ]
  edge [
    source 145
    target 151
  ]
  edge [
    source 145
    target 152
  ]
  edge [
    source 145
    target 1266
  ]
  edge [
    source 145
    target 1267
  ]
  edge [
    source 145
    target 1268
  ]
  edge [
    source 145
    target 1269
  ]
  edge [
    source 145
    target 1270
  ]
  edge [
    source 145
    target 1271
  ]
  edge [
    source 145
    target 296
  ]
  edge [
    source 145
    target 1050
  ]
  edge [
    source 145
    target 1272
  ]
  edge [
    source 146
    target 147
  ]
  edge [
    source 146
    target 1151
  ]
  edge [
    source 146
    target 1062
  ]
  edge [
    source 146
    target 1273
  ]
  edge [
    source 147
    target 148
  ]
  edge [
    source 147
    target 1274
  ]
  edge [
    source 148
    target 149
  ]
  edge [
    source 148
    target 1275
  ]
  edge [
    source 148
    target 1276
  ]
  edge [
    source 148
    target 1277
  ]
  edge [
    source 148
    target 842
  ]
  edge [
    source 148
    target 1278
  ]
  edge [
    source 148
    target 1279
  ]
  edge [
    source 148
    target 1280
  ]
  edge [
    source 148
    target 1281
  ]
  edge [
    source 148
    target 1282
  ]
  edge [
    source 148
    target 1283
  ]
  edge [
    source 148
    target 1284
  ]
  edge [
    source 148
    target 1285
  ]
  edge [
    source 148
    target 1286
  ]
  edge [
    source 148
    target 1124
  ]
  edge [
    source 148
    target 1287
  ]
  edge [
    source 148
    target 1288
  ]
  edge [
    source 148
    target 1289
  ]
  edge [
    source 148
    target 1290
  ]
  edge [
    source 148
    target 1291
  ]
  edge [
    source 148
    target 1292
  ]
  edge [
    source 148
    target 1293
  ]
  edge [
    source 148
    target 1294
  ]
  edge [
    source 148
    target 1295
  ]
  edge [
    source 148
    target 1296
  ]
  edge [
    source 148
    target 1297
  ]
  edge [
    source 148
    target 1298
  ]
  edge [
    source 148
    target 1299
  ]
  edge [
    source 148
    target 1300
  ]
  edge [
    source 148
    target 1301
  ]
  edge [
    source 148
    target 1302
  ]
  edge [
    source 148
    target 1303
  ]
  edge [
    source 148
    target 1304
  ]
  edge [
    source 148
    target 1305
  ]
  edge [
    source 148
    target 1306
  ]
  edge [
    source 148
    target 1307
  ]
  edge [
    source 148
    target 1133
  ]
  edge [
    source 148
    target 1308
  ]
  edge [
    source 148
    target 1309
  ]
  edge [
    source 148
    target 1310
  ]
  edge [
    source 148
    target 1311
  ]
  edge [
    source 148
    target 1312
  ]
  edge [
    source 148
    target 1313
  ]
  edge [
    source 148
    target 1314
  ]
  edge [
    source 148
    target 1315
  ]
  edge [
    source 148
    target 1316
  ]
  edge [
    source 148
    target 1317
  ]
  edge [
    source 148
    target 1318
  ]
  edge [
    source 148
    target 1319
  ]
  edge [
    source 148
    target 1320
  ]
  edge [
    source 148
    target 1321
  ]
  edge [
    source 148
    target 1322
  ]
  edge [
    source 148
    target 1323
  ]
  edge [
    source 148
    target 1324
  ]
  edge [
    source 148
    target 1325
  ]
  edge [
    source 148
    target 1326
  ]
  edge [
    source 148
    target 1327
  ]
  edge [
    source 148
    target 1328
  ]
  edge [
    source 148
    target 1329
  ]
  edge [
    source 148
    target 1330
  ]
  edge [
    source 148
    target 1331
  ]
  edge [
    source 148
    target 1332
  ]
  edge [
    source 148
    target 1333
  ]
  edge [
    source 148
    target 1334
  ]
  edge [
    source 148
    target 1335
  ]
  edge [
    source 148
    target 1336
  ]
  edge [
    source 148
    target 657
  ]
  edge [
    source 148
    target 1337
  ]
  edge [
    source 148
    target 1338
  ]
  edge [
    source 148
    target 1339
  ]
  edge [
    source 148
    target 1340
  ]
  edge [
    source 148
    target 1341
  ]
  edge [
    source 149
    target 150
  ]
  edge [
    source 149
    target 1342
  ]
  edge [
    source 149
    target 1343
  ]
  edge [
    source 149
    target 1344
  ]
  edge [
    source 149
    target 331
  ]
  edge [
    source 149
    target 1345
  ]
  edge [
    source 149
    target 1346
  ]
  edge [
    source 149
    target 1347
  ]
  edge [
    source 149
    target 1341
  ]
  edge [
    source 151
    target 341
  ]
  edge [
    source 151
    target 1153
  ]
  edge [
    source 151
    target 1152
  ]
  edge [
    source 151
    target 1348
  ]
  edge [
    source 151
    target 1349
  ]
  edge [
    source 151
    target 1350
  ]
  edge [
    source 151
    target 1351
  ]
  edge [
    source 152
    target 153
  ]
  edge [
    source 153
    target 1352
  ]
]
