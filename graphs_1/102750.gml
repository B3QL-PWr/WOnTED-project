graph [
  maxDegree 22
  minDegree 1
  meanDegree 2.0317460317460316
  density 0.016253968253968253
  graphCliqueNumber 3
  node [
    id 0
    label "zapotrzebowanie"
    origin "text"
  ]
  node [
    id 1
    label "prasa"
    origin "text"
  ]
  node [
    id 2
    label "kupowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "w&#322;asne"
    origin "text"
  ]
  node [
    id 4
    label "&#347;rodek"
    origin "text"
  ]
  node [
    id 5
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 6
    label "sk&#322;ada&#263;"
    origin "text"
  ]
  node [
    id 7
    label "pod"
    origin "text"
  ]
  node [
    id 8
    label "warunek"
    origin "text"
  ]
  node [
    id 9
    label "zabezpieczenie"
    origin "text"
  ]
  node [
    id 10
    label "finansowy"
    origin "text"
  ]
  node [
    id 11
    label "zakup"
    origin "text"
  ]
  node [
    id 12
    label "przez"
    origin "text"
  ]
  node [
    id 13
    label "w&#322;a&#347;ciwy"
    origin "text"
  ]
  node [
    id 14
    label "dysponent"
    origin "text"
  ]
  node [
    id 15
    label "bud&#380;etowy"
    origin "text"
  ]
  node [
    id 16
    label "elastyczno&#347;&#263;_popytu"
  ]
  node [
    id 17
    label "krzywa_popytu"
  ]
  node [
    id 18
    label "popyt_zagregowany"
  ]
  node [
    id 19
    label "mechanizm_mno&#380;nikowy"
  ]
  node [
    id 20
    label "proces_ekonomiczny"
  ]
  node [
    id 21
    label "handel"
  ]
  node [
    id 22
    label "nawis_inflacyjny"
  ]
  node [
    id 23
    label "lepko&#347;&#263;_cen"
  ]
  node [
    id 24
    label "cena_r&#243;wnowagi_rynkowej"
  ]
  node [
    id 25
    label "czynnik_niecenowy"
  ]
  node [
    id 26
    label "pisa&#263;"
  ]
  node [
    id 27
    label "dziennikarz_prasowy"
  ]
  node [
    id 28
    label "gazeta"
  ]
  node [
    id 29
    label "napisa&#263;"
  ]
  node [
    id 30
    label "maszyna_rolnicza"
  ]
  node [
    id 31
    label "zesp&#243;&#322;"
  ]
  node [
    id 32
    label "t&#322;oczysko"
  ]
  node [
    id 33
    label "depesza"
  ]
  node [
    id 34
    label "maszyna"
  ]
  node [
    id 35
    label "czasopismo"
  ]
  node [
    id 36
    label "media"
  ]
  node [
    id 37
    label "kiosk"
  ]
  node [
    id 38
    label "get"
  ]
  node [
    id 39
    label "ustawia&#263;"
  ]
  node [
    id 40
    label "wierzy&#263;"
  ]
  node [
    id 41
    label "przyjmowa&#263;"
  ]
  node [
    id 42
    label "pozyskiwa&#263;"
  ]
  node [
    id 43
    label "gra&#263;"
  ]
  node [
    id 44
    label "kupywa&#263;"
  ]
  node [
    id 45
    label "uznawa&#263;"
  ]
  node [
    id 46
    label "bra&#263;"
  ]
  node [
    id 47
    label "miejsce"
  ]
  node [
    id 48
    label "czas"
  ]
  node [
    id 49
    label "abstrakcja"
  ]
  node [
    id 50
    label "punkt"
  ]
  node [
    id 51
    label "substancja"
  ]
  node [
    id 52
    label "spos&#243;b"
  ]
  node [
    id 53
    label "chemikalia"
  ]
  node [
    id 54
    label "free"
  ]
  node [
    id 55
    label "render"
  ]
  node [
    id 56
    label "zmienia&#263;"
  ]
  node [
    id 57
    label "zestaw"
  ]
  node [
    id 58
    label "train"
  ]
  node [
    id 59
    label "uk&#322;ada&#263;"
  ]
  node [
    id 60
    label "dzieli&#263;"
  ]
  node [
    id 61
    label "set"
  ]
  node [
    id 62
    label "przywraca&#263;"
  ]
  node [
    id 63
    label "dawa&#263;"
  ]
  node [
    id 64
    label "przyk&#322;ada&#263;"
  ]
  node [
    id 65
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 66
    label "zbiera&#263;"
  ]
  node [
    id 67
    label "convey"
  ]
  node [
    id 68
    label "opracowywa&#263;"
  ]
  node [
    id 69
    label "oszcz&#281;dza&#263;"
  ]
  node [
    id 70
    label "publicize"
  ]
  node [
    id 71
    label "przekazywa&#263;"
  ]
  node [
    id 72
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 73
    label "scala&#263;"
  ]
  node [
    id 74
    label "oddawa&#263;"
  ]
  node [
    id 75
    label "za&#322;o&#380;enie"
  ]
  node [
    id 76
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 77
    label "umowa"
  ]
  node [
    id 78
    label "agent"
  ]
  node [
    id 79
    label "condition"
  ]
  node [
    id 80
    label "ekspozycja"
  ]
  node [
    id 81
    label "faktor"
  ]
  node [
    id 82
    label "zabezpiecza&#263;_si&#281;"
  ]
  node [
    id 83
    label "cover"
  ]
  node [
    id 84
    label "czynno&#347;&#263;"
  ]
  node [
    id 85
    label "spowodowanie"
  ]
  node [
    id 86
    label "obiekt"
  ]
  node [
    id 87
    label "chroniony"
  ]
  node [
    id 88
    label "por&#281;czenie"
  ]
  node [
    id 89
    label "zainstalowanie"
  ]
  node [
    id 90
    label "bro&#324;_palna"
  ]
  node [
    id 91
    label "pistolet"
  ]
  node [
    id 92
    label "guarantee"
  ]
  node [
    id 93
    label "tarcza"
  ]
  node [
    id 94
    label "ubezpieczenie"
  ]
  node [
    id 95
    label "zabezpieczanie_si&#281;"
  ]
  node [
    id 96
    label "zaplecze"
  ]
  node [
    id 97
    label "zastaw"
  ]
  node [
    id 98
    label "metoda"
  ]
  node [
    id 99
    label "zapewnienie"
  ]
  node [
    id 100
    label "mi&#281;dzybankowy"
  ]
  node [
    id 101
    label "finansowo"
  ]
  node [
    id 102
    label "fizyczny"
  ]
  node [
    id 103
    label "pozamaterialny"
  ]
  node [
    id 104
    label "materjalny"
  ]
  node [
    id 105
    label "sprzedaj&#261;cy"
  ]
  node [
    id 106
    label "dobro"
  ]
  node [
    id 107
    label "transakcja"
  ]
  node [
    id 108
    label "taki"
  ]
  node [
    id 109
    label "nale&#380;yty"
  ]
  node [
    id 110
    label "charakterystyczny"
  ]
  node [
    id 111
    label "stosownie"
  ]
  node [
    id 112
    label "dobry"
  ]
  node [
    id 113
    label "prawdziwy"
  ]
  node [
    id 114
    label "ten"
  ]
  node [
    id 115
    label "uprawniony"
  ]
  node [
    id 116
    label "zasadniczy"
  ]
  node [
    id 117
    label "typowy"
  ]
  node [
    id 118
    label "nale&#380;ny"
  ]
  node [
    id 119
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 120
    label "urz&#281;dnik"
  ]
  node [
    id 121
    label "zwierzchnik"
  ]
  node [
    id 122
    label "w&#322;odarz"
  ]
  node [
    id 123
    label "etatowy"
  ]
  node [
    id 124
    label "bud&#380;etowo"
  ]
  node [
    id 125
    label "budgetary"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 4
    target 10
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 10
    target 15
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 13
    target 113
  ]
  edge [
    source 13
    target 114
  ]
  edge [
    source 13
    target 115
  ]
  edge [
    source 13
    target 116
  ]
  edge [
    source 13
    target 117
  ]
  edge [
    source 13
    target 118
  ]
  edge [
    source 13
    target 119
  ]
  edge [
    source 14
    target 120
  ]
  edge [
    source 14
    target 121
  ]
  edge [
    source 14
    target 122
  ]
  edge [
    source 15
    target 123
  ]
  edge [
    source 15
    target 124
  ]
  edge [
    source 15
    target 125
  ]
]
