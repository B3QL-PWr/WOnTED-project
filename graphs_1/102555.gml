graph [
  maxDegree 27
  minDegree 1
  meanDegree 1.9661016949152543
  density 0.03389830508474576
  graphCliqueNumber 2
  node [
    id 0
    label "sekcja"
    origin "text"
  ]
  node [
    id 1
    label "przedmiot"
    origin "text"
  ]
  node [
    id 2
    label "zam&#243;wienie"
    origin "text"
  ]
  node [
    id 3
    label "podsekcja"
  ]
  node [
    id 4
    label "whole"
  ]
  node [
    id 5
    label "relation"
  ]
  node [
    id 6
    label "zw&#322;oki"
  ]
  node [
    id 7
    label "grupa"
  ]
  node [
    id 8
    label "zesp&#243;&#322;"
  ]
  node [
    id 9
    label "orkiestra"
  ]
  node [
    id 10
    label "urz&#261;d"
  ]
  node [
    id 11
    label "jednostka_organizacyjna"
  ]
  node [
    id 12
    label "autopsy"
  ]
  node [
    id 13
    label "insourcing"
  ]
  node [
    id 14
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 15
    label "badanie"
  ]
  node [
    id 16
    label "ministerstwo"
  ]
  node [
    id 17
    label "miejsce_pracy"
  ]
  node [
    id 18
    label "dzia&#322;"
  ]
  node [
    id 19
    label "robienie"
  ]
  node [
    id 20
    label "kr&#261;&#380;enie"
  ]
  node [
    id 21
    label "rzecz"
  ]
  node [
    id 22
    label "zbacza&#263;"
  ]
  node [
    id 23
    label "entity"
  ]
  node [
    id 24
    label "element"
  ]
  node [
    id 25
    label "omawia&#263;"
  ]
  node [
    id 26
    label "om&#243;wi&#263;"
  ]
  node [
    id 27
    label "sponiewiera&#263;"
  ]
  node [
    id 28
    label "sponiewieranie"
  ]
  node [
    id 29
    label "omawianie"
  ]
  node [
    id 30
    label "charakter"
  ]
  node [
    id 31
    label "program_nauczania"
  ]
  node [
    id 32
    label "w&#261;tek"
  ]
  node [
    id 33
    label "thing"
  ]
  node [
    id 34
    label "zboczenie"
  ]
  node [
    id 35
    label "zbaczanie"
  ]
  node [
    id 36
    label "tre&#347;&#263;"
  ]
  node [
    id 37
    label "tematyka"
  ]
  node [
    id 38
    label "istota"
  ]
  node [
    id 39
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 40
    label "kultura"
  ]
  node [
    id 41
    label "zboczy&#263;"
  ]
  node [
    id 42
    label "discipline"
  ]
  node [
    id 43
    label "om&#243;wienie"
  ]
  node [
    id 44
    label "zg&#322;oszenie"
  ]
  node [
    id 45
    label "zaczarowanie"
  ]
  node [
    id 46
    label "zamawianie"
  ]
  node [
    id 47
    label "portfel_zam&#243;wie&#324;"
  ]
  node [
    id 48
    label "rozdysponowanie"
  ]
  node [
    id 49
    label "polecenie"
  ]
  node [
    id 50
    label "transakcja"
  ]
  node [
    id 51
    label "order"
  ]
  node [
    id 52
    label "zamawia&#263;"
  ]
  node [
    id 53
    label "indent"
  ]
  node [
    id 54
    label "perpetration"
  ]
  node [
    id 55
    label "um&#243;wienie_si&#281;"
  ]
  node [
    id 56
    label "zarezerwowanie"
  ]
  node [
    id 57
    label "zam&#243;wi&#263;"
  ]
  node [
    id 58
    label "zlecenie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
]
