graph [
  maxDegree 11
  minDegree 1
  meanDegree 1.8333333333333333
  density 0.16666666666666666
  graphCliqueNumber 2
  node [
    id 0
    label "heheszki"
    origin "text"
  ]
  node [
    id 1
    label "humor"
    origin "text"
  ]
  node [
    id 2
    label "stan"
  ]
  node [
    id 3
    label "temper"
  ]
  node [
    id 4
    label "&#347;mieszno&#347;&#263;"
  ]
  node [
    id 5
    label "mechanizm_obronny"
  ]
  node [
    id 6
    label "nastr&#243;j"
  ]
  node [
    id 7
    label "state"
  ]
  node [
    id 8
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 9
    label "samopoczucie"
  ]
  node [
    id 10
    label "fondness"
  ]
  node [
    id 11
    label "p&#322;yn_ustrojowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
]
