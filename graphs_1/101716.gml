graph [
  maxDegree 28
  minDegree 1
  meanDegree 2.0377358490566038
  density 0.01289706233580129
  graphCliqueNumber 3
  node [
    id 0
    label "grunt"
    origin "text"
  ]
  node [
    id 1
    label "rzecz"
    origin "text"
  ]
  node [
    id 2
    label "przedstawia&#263;"
    origin "text"
  ]
  node [
    id 3
    label "stephen"
    origin "text"
  ]
  node [
    id 4
    label "worth"
    origin "text"
  ]
  node [
    id 5
    label "pewien"
    origin "text"
  ]
  node [
    id 6
    label "wizja"
    origin "text"
  ]
  node [
    id 7
    label "medialny"
    origin "text"
  ]
  node [
    id 8
    label "dieta"
    origin "text"
  ]
  node [
    id 9
    label "warto"
    origin "text"
  ]
  node [
    id 10
    label "konsumowa&#263;"
    origin "text"
  ]
  node [
    id 11
    label "tre&#347;&#263;"
    origin "text"
  ]
  node [
    id 12
    label "tworzy&#263;"
    origin "text"
  ]
  node [
    id 13
    label "przez"
    origin "text"
  ]
  node [
    id 14
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 15
    label "obdarzy&#263;"
    origin "text"
  ]
  node [
    id 16
    label "talent"
    origin "text"
  ]
  node [
    id 17
    label "umiej&#281;tno&#347;&#263;"
    origin "text"
  ]
  node [
    id 18
    label "kreatywno&#347;&#263;"
    origin "text"
  ]
  node [
    id 19
    label "za&#322;o&#380;enie"
  ]
  node [
    id 20
    label "powierzchnia"
  ]
  node [
    id 21
    label "glinowa&#263;"
  ]
  node [
    id 22
    label "pr&#243;chnica"
  ]
  node [
    id 23
    label "podglebie"
  ]
  node [
    id 24
    label "glinowanie"
  ]
  node [
    id 25
    label "litosfera"
  ]
  node [
    id 26
    label "zasadzenie"
  ]
  node [
    id 27
    label "documentation"
  ]
  node [
    id 28
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 29
    label "teren"
  ]
  node [
    id 30
    label "ryzosfera"
  ]
  node [
    id 31
    label "czynnik_produkcji"
  ]
  node [
    id 32
    label "zasadzi&#263;"
  ]
  node [
    id 33
    label "glej"
  ]
  node [
    id 34
    label "martwica"
  ]
  node [
    id 35
    label "geosystem"
  ]
  node [
    id 36
    label "przestrze&#324;"
  ]
  node [
    id 37
    label "dotleni&#263;"
  ]
  node [
    id 38
    label "penetrator"
  ]
  node [
    id 39
    label "punkt_odniesienia"
  ]
  node [
    id 40
    label "kompleks_sorpcyjny"
  ]
  node [
    id 41
    label "podstawowy"
  ]
  node [
    id 42
    label "plantowa&#263;"
  ]
  node [
    id 43
    label "podk&#322;ad"
  ]
  node [
    id 44
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 45
    label "dno"
  ]
  node [
    id 46
    label "obiekt"
  ]
  node [
    id 47
    label "temat"
  ]
  node [
    id 48
    label "istota"
  ]
  node [
    id 49
    label "wpa&#347;&#263;"
  ]
  node [
    id 50
    label "wpadanie"
  ]
  node [
    id 51
    label "przedmiot"
  ]
  node [
    id 52
    label "wpada&#263;"
  ]
  node [
    id 53
    label "kultura"
  ]
  node [
    id 54
    label "przyroda"
  ]
  node [
    id 55
    label "mienie"
  ]
  node [
    id 56
    label "object"
  ]
  node [
    id 57
    label "wpadni&#281;cie"
  ]
  node [
    id 58
    label "przedstawienie"
  ]
  node [
    id 59
    label "pokazywa&#263;"
  ]
  node [
    id 60
    label "zapoznawa&#263;"
  ]
  node [
    id 61
    label "typify"
  ]
  node [
    id 62
    label "opisywa&#263;"
  ]
  node [
    id 63
    label "teatr"
  ]
  node [
    id 64
    label "podawa&#263;"
  ]
  node [
    id 65
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 66
    label "demonstrowa&#263;"
  ]
  node [
    id 67
    label "represent"
  ]
  node [
    id 68
    label "ukazywa&#263;"
  ]
  node [
    id 69
    label "attest"
  ]
  node [
    id 70
    label "exhibit"
  ]
  node [
    id 71
    label "stanowi&#263;"
  ]
  node [
    id 72
    label "zg&#322;asza&#263;"
  ]
  node [
    id 73
    label "display"
  ]
  node [
    id 74
    label "upewnienie_si&#281;"
  ]
  node [
    id 75
    label "wierzenie"
  ]
  node [
    id 76
    label "mo&#380;liwy"
  ]
  node [
    id 77
    label "ufanie"
  ]
  node [
    id 78
    label "jaki&#347;"
  ]
  node [
    id 79
    label "spokojny"
  ]
  node [
    id 80
    label "upewnianie_si&#281;"
  ]
  node [
    id 81
    label "projekcja"
  ]
  node [
    id 82
    label "idea"
  ]
  node [
    id 83
    label "obraz"
  ]
  node [
    id 84
    label "przywidzenie"
  ]
  node [
    id 85
    label "widok"
  ]
  node [
    id 86
    label "przeplot"
  ]
  node [
    id 87
    label "ostro&#347;&#263;"
  ]
  node [
    id 88
    label "ziarno"
  ]
  node [
    id 89
    label "u&#322;uda"
  ]
  node [
    id 90
    label "nieprawdziwy"
  ]
  node [
    id 91
    label "medialnie"
  ]
  node [
    id 92
    label "popularny"
  ]
  node [
    id 93
    label "&#347;rodkowy"
  ]
  node [
    id 94
    label "zachowanie"
  ]
  node [
    id 95
    label "zachowa&#263;"
  ]
  node [
    id 96
    label "wynagrodzenie"
  ]
  node [
    id 97
    label "zachowywa&#263;"
  ]
  node [
    id 98
    label "r&#243;wnowarto&#347;&#263;"
  ]
  node [
    id 99
    label "od&#380;ywianie_si&#281;"
  ]
  node [
    id 100
    label "chart"
  ]
  node [
    id 101
    label "regimen"
  ]
  node [
    id 102
    label "zachowywanie"
  ]
  node [
    id 103
    label "terapia"
  ]
  node [
    id 104
    label "spos&#243;b"
  ]
  node [
    id 105
    label "przysparza&#263;"
  ]
  node [
    id 106
    label "give"
  ]
  node [
    id 107
    label "kali&#263;_si&#281;"
  ]
  node [
    id 108
    label "bonanza"
  ]
  node [
    id 109
    label "zu&#380;ywa&#263;"
  ]
  node [
    id 110
    label "zawarto&#347;&#263;"
  ]
  node [
    id 111
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 112
    label "informacja"
  ]
  node [
    id 113
    label "get"
  ]
  node [
    id 114
    label "consist"
  ]
  node [
    id 115
    label "raise"
  ]
  node [
    id 116
    label "robi&#263;"
  ]
  node [
    id 117
    label "pope&#322;nia&#263;"
  ]
  node [
    id 118
    label "wytwarza&#263;"
  ]
  node [
    id 119
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 120
    label "asymilowa&#263;"
  ]
  node [
    id 121
    label "wapniak"
  ]
  node [
    id 122
    label "dwun&#243;g"
  ]
  node [
    id 123
    label "polifag"
  ]
  node [
    id 124
    label "wz&#243;r"
  ]
  node [
    id 125
    label "profanum"
  ]
  node [
    id 126
    label "hominid"
  ]
  node [
    id 127
    label "homo_sapiens"
  ]
  node [
    id 128
    label "nasada"
  ]
  node [
    id 129
    label "podw&#322;adny"
  ]
  node [
    id 130
    label "ludzko&#347;&#263;"
  ]
  node [
    id 131
    label "os&#322;abianie"
  ]
  node [
    id 132
    label "mikrokosmos"
  ]
  node [
    id 133
    label "portrecista"
  ]
  node [
    id 134
    label "duch"
  ]
  node [
    id 135
    label "g&#322;owa"
  ]
  node [
    id 136
    label "oddzia&#322;ywanie"
  ]
  node [
    id 137
    label "asymilowanie"
  ]
  node [
    id 138
    label "osoba"
  ]
  node [
    id 139
    label "os&#322;abia&#263;"
  ]
  node [
    id 140
    label "figura"
  ]
  node [
    id 141
    label "Adam"
  ]
  node [
    id 142
    label "senior"
  ]
  node [
    id 143
    label "antropochoria"
  ]
  node [
    id 144
    label "posta&#263;"
  ]
  node [
    id 145
    label "da&#263;"
  ]
  node [
    id 146
    label "udarowa&#263;"
  ]
  node [
    id 147
    label "zacz&#261;&#263;"
  ]
  node [
    id 148
    label "bestow"
  ]
  node [
    id 149
    label "&#322;atwo&#347;&#263;"
  ]
  node [
    id 150
    label "stygmat"
  ]
  node [
    id 151
    label "faculty"
  ]
  node [
    id 152
    label "brylant"
  ]
  node [
    id 153
    label "gigant"
  ]
  node [
    id 154
    label "moneta"
  ]
  node [
    id 155
    label "dyspozycja"
  ]
  node [
    id 156
    label "zdolno&#347;&#263;"
  ]
  node [
    id 157
    label "cecha"
  ]
  node [
    id 158
    label "potencja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 110
  ]
  edge [
    source 11
    target 47
  ]
  edge [
    source 11
    target 48
  ]
  edge [
    source 11
    target 111
  ]
  edge [
    source 11
    target 112
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 113
  ]
  edge [
    source 12
    target 114
  ]
  edge [
    source 12
    target 115
  ]
  edge [
    source 12
    target 116
  ]
  edge [
    source 12
    target 117
  ]
  edge [
    source 12
    target 118
  ]
  edge [
    source 12
    target 71
  ]
  edge [
    source 12
    target 119
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 120
  ]
  edge [
    source 14
    target 121
  ]
  edge [
    source 14
    target 122
  ]
  edge [
    source 14
    target 123
  ]
  edge [
    source 14
    target 124
  ]
  edge [
    source 14
    target 125
  ]
  edge [
    source 14
    target 126
  ]
  edge [
    source 14
    target 127
  ]
  edge [
    source 14
    target 128
  ]
  edge [
    source 14
    target 129
  ]
  edge [
    source 14
    target 130
  ]
  edge [
    source 14
    target 131
  ]
  edge [
    source 14
    target 132
  ]
  edge [
    source 14
    target 133
  ]
  edge [
    source 14
    target 134
  ]
  edge [
    source 14
    target 135
  ]
  edge [
    source 14
    target 136
  ]
  edge [
    source 14
    target 137
  ]
  edge [
    source 14
    target 138
  ]
  edge [
    source 14
    target 139
  ]
  edge [
    source 14
    target 140
  ]
  edge [
    source 14
    target 141
  ]
  edge [
    source 14
    target 142
  ]
  edge [
    source 14
    target 143
  ]
  edge [
    source 14
    target 144
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 145
  ]
  edge [
    source 15
    target 146
  ]
  edge [
    source 15
    target 147
  ]
  edge [
    source 15
    target 148
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 149
  ]
  edge [
    source 16
    target 150
  ]
  edge [
    source 16
    target 151
  ]
  edge [
    source 16
    target 152
  ]
  edge [
    source 16
    target 153
  ]
  edge [
    source 16
    target 154
  ]
  edge [
    source 16
    target 155
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 156
  ]
  edge [
    source 17
    target 157
  ]
  edge [
    source 18
    target 158
  ]
]
