graph [
  maxDegree 32
  minDegree 1
  meanDegree 2.0634920634920637
  density 0.033282130056323606
  graphCliqueNumber 3
  node [
    id 0
    label "okazowa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "chwila"
    origin "text"
  ]
  node [
    id 3
    label "przed"
    origin "text"
  ]
  node [
    id 4
    label "wybuch"
    origin "text"
  ]
  node [
    id 5
    label "po&#380;ar"
    origin "text"
  ]
  node [
    id 6
    label "doj&#347;&#263;"
    origin "text"
  ]
  node [
    id 7
    label "w&#322;amanie"
    origin "text"
  ]
  node [
    id 8
    label "czas"
  ]
  node [
    id 9
    label "time"
  ]
  node [
    id 10
    label "fit"
  ]
  node [
    id 11
    label "wydarzenie"
  ]
  node [
    id 12
    label "przyp&#322;yw"
  ]
  node [
    id 13
    label "pocz&#261;tek"
  ]
  node [
    id 14
    label "kryzys"
  ]
  node [
    id 15
    label "zap&#322;on"
  ]
  node [
    id 16
    label "miazmaty"
  ]
  node [
    id 17
    label "zalew"
  ]
  node [
    id 18
    label "podpalenie"
  ]
  node [
    id 19
    label "wojna"
  ]
  node [
    id 20
    label "p&#322;omie&#324;"
  ]
  node [
    id 21
    label "stra&#380;ak"
  ]
  node [
    id 22
    label "nawa&#322;no&#347;&#263;"
  ]
  node [
    id 23
    label "fire"
  ]
  node [
    id 24
    label "kl&#281;ska"
  ]
  node [
    id 25
    label "pogorzelec"
  ]
  node [
    id 26
    label "zapr&#243;szenie"
  ]
  node [
    id 27
    label "ogie&#324;"
  ]
  node [
    id 28
    label "get"
  ]
  node [
    id 29
    label "zaj&#347;&#263;"
  ]
  node [
    id 30
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 31
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 32
    label "dop&#322;ata"
  ]
  node [
    id 33
    label "supervene"
  ]
  node [
    id 34
    label "heed"
  ]
  node [
    id 35
    label "dodatek"
  ]
  node [
    id 36
    label "catch"
  ]
  node [
    id 37
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 38
    label "uzyska&#263;"
  ]
  node [
    id 39
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 40
    label "orgazm"
  ]
  node [
    id 41
    label "dozna&#263;"
  ]
  node [
    id 42
    label "sta&#263;_si&#281;"
  ]
  node [
    id 43
    label "bodziec"
  ]
  node [
    id 44
    label "drive"
  ]
  node [
    id 45
    label "informacja"
  ]
  node [
    id 46
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 47
    label "spowodowa&#263;"
  ]
  node [
    id 48
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 49
    label "dotrze&#263;"
  ]
  node [
    id 50
    label "postrzega&#263;"
  ]
  node [
    id 51
    label "become"
  ]
  node [
    id 52
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 53
    label "dop&#322;yn&#261;&#263;"
  ]
  node [
    id 54
    label "przesy&#322;ka"
  ]
  node [
    id 55
    label "dolecie&#263;"
  ]
  node [
    id 56
    label "przy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 57
    label "dokoptowa&#263;"
  ]
  node [
    id 58
    label "porcelanka"
  ]
  node [
    id 59
    label "burglary"
  ]
  node [
    id 60
    label "w&#322;am"
  ]
  node [
    id 61
    label "przest&#281;pstwo"
  ]
  node [
    id 62
    label "crack"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 10
  ]
  edge [
    source 4
    target 11
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 14
  ]
  edge [
    source 5
    target 15
  ]
  edge [
    source 5
    target 16
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 11
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 12
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 11
  ]
]
