graph [
  maxDegree 22
  minDegree 1
  meanDegree 1.9642857142857142
  density 0.03571428571428571
  graphCliqueNumber 2
  node [
    id 0
    label "przeprasza&#263;"
    origin "text"
  ]
  node [
    id 1
    label "by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "zbyt"
    origin "text"
  ]
  node [
    id 3
    label "g&#322;upi"
    origin "text"
  ]
  node [
    id 4
    label "zrobi&#263;"
    origin "text"
  ]
  node [
    id 5
    label "dobre"
    origin "text"
  ]
  node [
    id 6
    label "tldw"
    origin "text"
  ]
  node [
    id 7
    label "sk&#322;ada&#263;"
  ]
  node [
    id 8
    label "si&#281;ga&#263;"
  ]
  node [
    id 9
    label "trwa&#263;"
  ]
  node [
    id 10
    label "obecno&#347;&#263;"
  ]
  node [
    id 11
    label "stan"
  ]
  node [
    id 12
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 13
    label "stand"
  ]
  node [
    id 14
    label "mie&#263;_miejsce"
  ]
  node [
    id 15
    label "uczestniczy&#263;"
  ]
  node [
    id 16
    label "chodzi&#263;"
  ]
  node [
    id 17
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 18
    label "equal"
  ]
  node [
    id 19
    label "nadmiernie"
  ]
  node [
    id 20
    label "sprzedawanie"
  ]
  node [
    id 21
    label "sprzeda&#380;"
  ]
  node [
    id 22
    label "g&#322;upiec"
  ]
  node [
    id 23
    label "g&#322;upienie"
  ]
  node [
    id 24
    label "cz&#322;owiek"
  ]
  node [
    id 25
    label "mondzio&#322;"
  ]
  node [
    id 26
    label "istota_&#380;ywa"
  ]
  node [
    id 27
    label "nierozwa&#380;ny"
  ]
  node [
    id 28
    label "niezr&#281;czny"
  ]
  node [
    id 29
    label "nadaremny"
  ]
  node [
    id 30
    label "bezmy&#347;lny"
  ]
  node [
    id 31
    label "bezcelowy"
  ]
  node [
    id 32
    label "uprzykrzony"
  ]
  node [
    id 33
    label "g&#322;upio"
  ]
  node [
    id 34
    label "niem&#261;dry"
  ]
  node [
    id 35
    label "niewa&#380;ny"
  ]
  node [
    id 36
    label "g&#322;uptas"
  ]
  node [
    id 37
    label "ma&#322;y"
  ]
  node [
    id 38
    label "&#347;mieszny"
  ]
  node [
    id 39
    label "zg&#322;upienie"
  ]
  node [
    id 40
    label "bezwolny"
  ]
  node [
    id 41
    label "bezsensowny"
  ]
  node [
    id 42
    label "zorganizowa&#263;"
  ]
  node [
    id 43
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 44
    label "przerobi&#263;"
  ]
  node [
    id 45
    label "wystylizowa&#263;"
  ]
  node [
    id 46
    label "cause"
  ]
  node [
    id 47
    label "wydali&#263;"
  ]
  node [
    id 48
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 49
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 50
    label "post&#261;pi&#263;"
  ]
  node [
    id 51
    label "appoint"
  ]
  node [
    id 52
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 53
    label "nabra&#263;"
  ]
  node [
    id 54
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 55
    label "make"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 5
    target 6
  ]
]
