graph [
  maxDegree 7
  minDegree 1
  meanDegree 1.8
  density 0.2
  graphCliqueNumber 2
  node [
    id 0
    label "kleje&#324;"
    origin "text"
  ]
  node [
    id 1
    label "opona"
    origin "text"
  ]
  node [
    id 2
    label "tkanka_nowotworowa"
  ]
  node [
    id 3
    label "ciecz"
  ]
  node [
    id 4
    label "wa&#322;ek"
  ]
  node [
    id 5
    label "ogumienie"
  ]
  node [
    id 6
    label "meninx"
  ]
  node [
    id 7
    label "b&#322;ona"
  ]
  node [
    id 8
    label "ko&#322;o"
  ]
  node [
    id 9
    label "bie&#380;nik"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
]
