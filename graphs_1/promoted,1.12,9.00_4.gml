graph [
  maxDegree 216
  minDegree 1
  meanDegree 1.984251968503937
  density 0.007842893156142044
  graphCliqueNumber 2
  node [
    id 0
    label "pa&#324;stwo"
    origin "text"
  ]
  node [
    id 1
    label "g&#243;rnica"
    origin "text"
  ]
  node [
    id 2
    label "du&#380;o"
    origin "text"
  ]
  node [
    id 3
    label "w&#281;giel"
    origin "text"
  ]
  node [
    id 4
    label "potrzeba"
    origin "text"
  ]
  node [
    id 5
    label "Rwanda"
  ]
  node [
    id 6
    label "Filipiny"
  ]
  node [
    id 7
    label "Monako"
  ]
  node [
    id 8
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 9
    label "Korea"
  ]
  node [
    id 10
    label "Czarnog&#243;ra"
  ]
  node [
    id 11
    label "Ghana"
  ]
  node [
    id 12
    label "Malawi"
  ]
  node [
    id 13
    label "Indonezja"
  ]
  node [
    id 14
    label "Bu&#322;garia"
  ]
  node [
    id 15
    label "Nauru"
  ]
  node [
    id 16
    label "Kenia"
  ]
  node [
    id 17
    label "Kambod&#380;a"
  ]
  node [
    id 18
    label "Mali"
  ]
  node [
    id 19
    label "Austria"
  ]
  node [
    id 20
    label "interior"
  ]
  node [
    id 21
    label "Armenia"
  ]
  node [
    id 22
    label "Fid&#380;i"
  ]
  node [
    id 23
    label "Tuwalu"
  ]
  node [
    id 24
    label "Etiopia"
  ]
  node [
    id 25
    label "Malezja"
  ]
  node [
    id 26
    label "Malta"
  ]
  node [
    id 27
    label "Tad&#380;ykistan"
  ]
  node [
    id 28
    label "Grenada"
  ]
  node [
    id 29
    label "Wehrlen"
  ]
  node [
    id 30
    label "para"
  ]
  node [
    id 31
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 32
    label "Rumunia"
  ]
  node [
    id 33
    label "Maroko"
  ]
  node [
    id 34
    label "Bhutan"
  ]
  node [
    id 35
    label "S&#322;owacja"
  ]
  node [
    id 36
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 37
    label "Seszele"
  ]
  node [
    id 38
    label "Kuwejt"
  ]
  node [
    id 39
    label "Arabia_Saudyjska"
  ]
  node [
    id 40
    label "Kanada"
  ]
  node [
    id 41
    label "Ekwador"
  ]
  node [
    id 42
    label "Japonia"
  ]
  node [
    id 43
    label "ziemia"
  ]
  node [
    id 44
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 45
    label "Hiszpania"
  ]
  node [
    id 46
    label "Wyspy_Marshalla"
  ]
  node [
    id 47
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 48
    label "D&#380;ibuti"
  ]
  node [
    id 49
    label "Botswana"
  ]
  node [
    id 50
    label "grupa"
  ]
  node [
    id 51
    label "Wietnam"
  ]
  node [
    id 52
    label "Egipt"
  ]
  node [
    id 53
    label "Burkina_Faso"
  ]
  node [
    id 54
    label "Niemcy"
  ]
  node [
    id 55
    label "Khitai"
  ]
  node [
    id 56
    label "Macedonia"
  ]
  node [
    id 57
    label "Albania"
  ]
  node [
    id 58
    label "Madagaskar"
  ]
  node [
    id 59
    label "Bahrajn"
  ]
  node [
    id 60
    label "Jemen"
  ]
  node [
    id 61
    label "Lesoto"
  ]
  node [
    id 62
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 63
    label "Samoa"
  ]
  node [
    id 64
    label "Andora"
  ]
  node [
    id 65
    label "Chiny"
  ]
  node [
    id 66
    label "Cypr"
  ]
  node [
    id 67
    label "Wielka_Brytania"
  ]
  node [
    id 68
    label "Ukraina"
  ]
  node [
    id 69
    label "Paragwaj"
  ]
  node [
    id 70
    label "Trynidad_i_Tobago"
  ]
  node [
    id 71
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 72
    label "Libia"
  ]
  node [
    id 73
    label "Surinam"
  ]
  node [
    id 74
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 75
    label "Nigeria"
  ]
  node [
    id 76
    label "Australia"
  ]
  node [
    id 77
    label "Honduras"
  ]
  node [
    id 78
    label "Peru"
  ]
  node [
    id 79
    label "USA"
  ]
  node [
    id 80
    label "Bangladesz"
  ]
  node [
    id 81
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 82
    label "Kazachstan"
  ]
  node [
    id 83
    label "holoarktyka"
  ]
  node [
    id 84
    label "Nepal"
  ]
  node [
    id 85
    label "Sudan"
  ]
  node [
    id 86
    label "Irak"
  ]
  node [
    id 87
    label "San_Marino"
  ]
  node [
    id 88
    label "Burundi"
  ]
  node [
    id 89
    label "Dominikana"
  ]
  node [
    id 90
    label "Komory"
  ]
  node [
    id 91
    label "granica_pa&#324;stwa"
  ]
  node [
    id 92
    label "Gwatemala"
  ]
  node [
    id 93
    label "Antarktis"
  ]
  node [
    id 94
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 95
    label "Brunei"
  ]
  node [
    id 96
    label "Iran"
  ]
  node [
    id 97
    label "Zimbabwe"
  ]
  node [
    id 98
    label "Namibia"
  ]
  node [
    id 99
    label "Meksyk"
  ]
  node [
    id 100
    label "Kamerun"
  ]
  node [
    id 101
    label "zwrot"
  ]
  node [
    id 102
    label "Somalia"
  ]
  node [
    id 103
    label "Angola"
  ]
  node [
    id 104
    label "Gabon"
  ]
  node [
    id 105
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 106
    label "Nowa_Zelandia"
  ]
  node [
    id 107
    label "Mozambik"
  ]
  node [
    id 108
    label "Tunezja"
  ]
  node [
    id 109
    label "Tajwan"
  ]
  node [
    id 110
    label "Liban"
  ]
  node [
    id 111
    label "Jordania"
  ]
  node [
    id 112
    label "Tonga"
  ]
  node [
    id 113
    label "Czad"
  ]
  node [
    id 114
    label "Gwinea"
  ]
  node [
    id 115
    label "Liberia"
  ]
  node [
    id 116
    label "Belize"
  ]
  node [
    id 117
    label "Benin"
  ]
  node [
    id 118
    label "&#321;otwa"
  ]
  node [
    id 119
    label "Syria"
  ]
  node [
    id 120
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 121
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 122
    label "Dominika"
  ]
  node [
    id 123
    label "Antigua_i_Barbuda"
  ]
  node [
    id 124
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 125
    label "Hanower"
  ]
  node [
    id 126
    label "partia"
  ]
  node [
    id 127
    label "Afganistan"
  ]
  node [
    id 128
    label "W&#322;ochy"
  ]
  node [
    id 129
    label "Kiribati"
  ]
  node [
    id 130
    label "Szwajcaria"
  ]
  node [
    id 131
    label "Chorwacja"
  ]
  node [
    id 132
    label "Sahara_Zachodnia"
  ]
  node [
    id 133
    label "Tajlandia"
  ]
  node [
    id 134
    label "Salwador"
  ]
  node [
    id 135
    label "Bahamy"
  ]
  node [
    id 136
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 137
    label "S&#322;owenia"
  ]
  node [
    id 138
    label "Gambia"
  ]
  node [
    id 139
    label "Urugwaj"
  ]
  node [
    id 140
    label "Zair"
  ]
  node [
    id 141
    label "Erytrea"
  ]
  node [
    id 142
    label "Rosja"
  ]
  node [
    id 143
    label "Mauritius"
  ]
  node [
    id 144
    label "Niger"
  ]
  node [
    id 145
    label "Uganda"
  ]
  node [
    id 146
    label "Turkmenistan"
  ]
  node [
    id 147
    label "Turcja"
  ]
  node [
    id 148
    label "Irlandia"
  ]
  node [
    id 149
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 150
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 151
    label "Gwinea_Bissau"
  ]
  node [
    id 152
    label "Belgia"
  ]
  node [
    id 153
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 154
    label "Palau"
  ]
  node [
    id 155
    label "Barbados"
  ]
  node [
    id 156
    label "Wenezuela"
  ]
  node [
    id 157
    label "W&#281;gry"
  ]
  node [
    id 158
    label "Chile"
  ]
  node [
    id 159
    label "Argentyna"
  ]
  node [
    id 160
    label "Kolumbia"
  ]
  node [
    id 161
    label "Sierra_Leone"
  ]
  node [
    id 162
    label "Azerbejd&#380;an"
  ]
  node [
    id 163
    label "Kongo"
  ]
  node [
    id 164
    label "Pakistan"
  ]
  node [
    id 165
    label "Liechtenstein"
  ]
  node [
    id 166
    label "Nikaragua"
  ]
  node [
    id 167
    label "Senegal"
  ]
  node [
    id 168
    label "Indie"
  ]
  node [
    id 169
    label "Suazi"
  ]
  node [
    id 170
    label "Polska"
  ]
  node [
    id 171
    label "Algieria"
  ]
  node [
    id 172
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 173
    label "terytorium"
  ]
  node [
    id 174
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 175
    label "Jamajka"
  ]
  node [
    id 176
    label "Kostaryka"
  ]
  node [
    id 177
    label "Timor_Wschodni"
  ]
  node [
    id 178
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 179
    label "Kuba"
  ]
  node [
    id 180
    label "Mauretania"
  ]
  node [
    id 181
    label "Portoryko"
  ]
  node [
    id 182
    label "Brazylia"
  ]
  node [
    id 183
    label "Mo&#322;dawia"
  ]
  node [
    id 184
    label "organizacja"
  ]
  node [
    id 185
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 186
    label "Litwa"
  ]
  node [
    id 187
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 188
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 189
    label "Izrael"
  ]
  node [
    id 190
    label "Grecja"
  ]
  node [
    id 191
    label "Kirgistan"
  ]
  node [
    id 192
    label "Holandia"
  ]
  node [
    id 193
    label "Sri_Lanka"
  ]
  node [
    id 194
    label "Katar"
  ]
  node [
    id 195
    label "Mikronezja"
  ]
  node [
    id 196
    label "Laos"
  ]
  node [
    id 197
    label "Mongolia"
  ]
  node [
    id 198
    label "Malediwy"
  ]
  node [
    id 199
    label "Zambia"
  ]
  node [
    id 200
    label "Tanzania"
  ]
  node [
    id 201
    label "Gujana"
  ]
  node [
    id 202
    label "Uzbekistan"
  ]
  node [
    id 203
    label "Panama"
  ]
  node [
    id 204
    label "Czechy"
  ]
  node [
    id 205
    label "Gruzja"
  ]
  node [
    id 206
    label "Serbia"
  ]
  node [
    id 207
    label "Francja"
  ]
  node [
    id 208
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 209
    label "Togo"
  ]
  node [
    id 210
    label "Estonia"
  ]
  node [
    id 211
    label "Boliwia"
  ]
  node [
    id 212
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 213
    label "Oman"
  ]
  node [
    id 214
    label "Wyspy_Salomona"
  ]
  node [
    id 215
    label "Haiti"
  ]
  node [
    id 216
    label "Luksemburg"
  ]
  node [
    id 217
    label "Portugalia"
  ]
  node [
    id 218
    label "Birma"
  ]
  node [
    id 219
    label "Rodezja"
  ]
  node [
    id 220
    label "mi&#281;so"
  ]
  node [
    id 221
    label "wo&#322;owina"
  ]
  node [
    id 222
    label "tusza"
  ]
  node [
    id 223
    label "konina"
  ]
  node [
    id 224
    label "du&#380;y"
  ]
  node [
    id 225
    label "cz&#281;sto"
  ]
  node [
    id 226
    label "bardzo"
  ]
  node [
    id 227
    label "mocno"
  ]
  node [
    id 228
    label "wiela"
  ]
  node [
    id 229
    label "surowiec_energetyczny"
  ]
  node [
    id 230
    label "w&#281;glowiec"
  ]
  node [
    id 231
    label "w&#281;glowodan"
  ]
  node [
    id 232
    label "kopalina_podstawowa"
  ]
  node [
    id 233
    label "coal"
  ]
  node [
    id 234
    label "przybory_do_pisania"
  ]
  node [
    id 235
    label "makroelement"
  ]
  node [
    id 236
    label "niemetal"
  ]
  node [
    id 237
    label "zsypnik"
  ]
  node [
    id 238
    label "w&#281;glarka"
  ]
  node [
    id 239
    label "zw&#281;glenie_si&#281;"
  ]
  node [
    id 240
    label "zw&#281;glanie_si&#281;"
  ]
  node [
    id 241
    label "coil"
  ]
  node [
    id 242
    label "bry&#322;a"
  ]
  node [
    id 243
    label "ska&#322;a"
  ]
  node [
    id 244
    label "carbon"
  ]
  node [
    id 245
    label "fulleren"
  ]
  node [
    id 246
    label "rysunek"
  ]
  node [
    id 247
    label "need"
  ]
  node [
    id 248
    label "wym&#243;g"
  ]
  node [
    id 249
    label "necessity"
  ]
  node [
    id 250
    label "pragnienie"
  ]
  node [
    id 251
    label "sytuacja"
  ]
  node [
    id 252
    label "Krzysztof"
  ]
  node [
    id 253
    label "Tch&#243;rzewski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 0
    target 202
  ]
  edge [
    source 0
    target 203
  ]
  edge [
    source 0
    target 204
  ]
  edge [
    source 0
    target 205
  ]
  edge [
    source 0
    target 206
  ]
  edge [
    source 0
    target 207
  ]
  edge [
    source 0
    target 208
  ]
  edge [
    source 0
    target 209
  ]
  edge [
    source 0
    target 210
  ]
  edge [
    source 0
    target 211
  ]
  edge [
    source 0
    target 212
  ]
  edge [
    source 0
    target 213
  ]
  edge [
    source 0
    target 214
  ]
  edge [
    source 0
    target 215
  ]
  edge [
    source 0
    target 216
  ]
  edge [
    source 0
    target 217
  ]
  edge [
    source 0
    target 218
  ]
  edge [
    source 0
    target 219
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 252
    target 253
  ]
]
