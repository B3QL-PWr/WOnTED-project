graph [
  maxDegree 13
  minDegree 1
  meanDegree 2.3225806451612905
  density 0.07741935483870968
  graphCliqueNumber 6
  node [
    id 0
    label "artyku&#322;"
    origin "text"
  ]
  node [
    id 1
    label "dokument"
  ]
  node [
    id 2
    label "towar"
  ]
  node [
    id 3
    label "nag&#322;&#243;wek"
  ]
  node [
    id 4
    label "znak_j&#281;zykowy"
  ]
  node [
    id 5
    label "wyr&#243;b"
  ]
  node [
    id 6
    label "blok"
  ]
  node [
    id 7
    label "line"
  ]
  node [
    id 8
    label "paragraf"
  ]
  node [
    id 9
    label "rodzajnik"
  ]
  node [
    id 10
    label "prawda"
  ]
  node [
    id 11
    label "szkic"
  ]
  node [
    id 12
    label "tekst"
  ]
  node [
    id 13
    label "fragment"
  ]
  node [
    id 14
    label "wsp&#243;&#322;praca"
  ]
  node [
    id 15
    label "transgraniczny"
  ]
  node [
    id 16
    label "PHARE"
  ]
  node [
    id 17
    label "perspektywa"
  ]
  node [
    id 18
    label "finansowy"
  ]
  node [
    id 19
    label "porozumie&#263;"
  ]
  node [
    id 20
    label "Mi&#281;dzyinstytucjonalnego"
  ]
  node [
    id 21
    label "europejski"
  ]
  node [
    id 22
    label "fundusz"
  ]
  node [
    id 23
    label "orientacja"
  ]
  node [
    id 24
    label "i"
  ]
  node [
    id 25
    label "gwarancja"
  ]
  node [
    id 26
    label "rolny"
  ]
  node [
    id 27
    label "sekcja"
  ]
  node [
    id 28
    label "wsp&#243;lnota"
  ]
  node [
    id 29
    label "rada"
  ]
  node [
    id 30
    label "stowarzyszy&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 23
  ]
  edge [
    source 21
    target 24
  ]
  edge [
    source 21
    target 25
  ]
  edge [
    source 21
    target 26
  ]
  edge [
    source 21
    target 28
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 24
  ]
  edge [
    source 22
    target 25
  ]
  edge [
    source 22
    target 26
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 25
  ]
  edge [
    source 23
    target 26
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 26
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 27
  ]
  edge [
    source 29
    target 30
  ]
]
