graph [
  maxDegree 39
  minDegree 1
  meanDegree 1.9838709677419355
  density 0.016129032258064516
  graphCliqueNumber 2
  node [
    id 0
    label "kotek"
    origin "text"
  ]
  node [
    id 1
    label "wynik"
    origin "text"
  ]
  node [
    id 2
    label "mocny"
    origin "text"
  ]
  node [
    id 3
    label "cios"
    origin "text"
  ]
  node [
    id 4
    label "g&#322;owa"
    origin "text"
  ]
  node [
    id 5
    label "straci&#263;"
    origin "text"
  ]
  node [
    id 6
    label "oko"
    origin "text"
  ]
  node [
    id 7
    label "kochanie"
  ]
  node [
    id 8
    label "typ"
  ]
  node [
    id 9
    label "dzia&#322;anie"
  ]
  node [
    id 10
    label "przyczyna"
  ]
  node [
    id 11
    label "zaokr&#261;gli&#263;"
  ]
  node [
    id 12
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 13
    label "zaokr&#261;glenie"
  ]
  node [
    id 14
    label "event"
  ]
  node [
    id 15
    label "rezultat"
  ]
  node [
    id 16
    label "przekonuj&#261;cy"
  ]
  node [
    id 17
    label "trudny"
  ]
  node [
    id 18
    label "szczery"
  ]
  node [
    id 19
    label "du&#380;y"
  ]
  node [
    id 20
    label "zdecydowany"
  ]
  node [
    id 21
    label "krzepki"
  ]
  node [
    id 22
    label "silny"
  ]
  node [
    id 23
    label "niepodwa&#380;alny"
  ]
  node [
    id 24
    label "wzmocni&#263;"
  ]
  node [
    id 25
    label "stabilny"
  ]
  node [
    id 26
    label "wzmacnia&#263;"
  ]
  node [
    id 27
    label "silnie"
  ]
  node [
    id 28
    label "wytrzyma&#322;y"
  ]
  node [
    id 29
    label "wyrazisty"
  ]
  node [
    id 30
    label "konkretny"
  ]
  node [
    id 31
    label "widoczny"
  ]
  node [
    id 32
    label "meflochina"
  ]
  node [
    id 33
    label "dobry"
  ]
  node [
    id 34
    label "intensywnie"
  ]
  node [
    id 35
    label "mocno"
  ]
  node [
    id 36
    label "&#322;upliwo&#347;&#263;"
  ]
  node [
    id 37
    label "nieszcz&#281;&#347;cie"
  ]
  node [
    id 38
    label "shot"
  ]
  node [
    id 39
    label "struktura_geologiczna"
  ]
  node [
    id 40
    label "uderzenie"
  ]
  node [
    id 41
    label "siekacz"
  ]
  node [
    id 42
    label "blok"
  ]
  node [
    id 43
    label "pr&#243;ba"
  ]
  node [
    id 44
    label "coup"
  ]
  node [
    id 45
    label "time"
  ]
  node [
    id 46
    label "r&#243;&#380;a_skalna"
  ]
  node [
    id 47
    label "cz&#322;owiek"
  ]
  node [
    id 48
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 49
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 50
    label "ucho"
  ]
  node [
    id 51
    label "makrocefalia"
  ]
  node [
    id 52
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 53
    label "m&#243;zg"
  ]
  node [
    id 54
    label "kierownictwo"
  ]
  node [
    id 55
    label "czaszka"
  ]
  node [
    id 56
    label "dekiel"
  ]
  node [
    id 57
    label "umys&#322;"
  ]
  node [
    id 58
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 59
    label "&#347;ci&#281;cie"
  ]
  node [
    id 60
    label "sztuka"
  ]
  node [
    id 61
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 62
    label "g&#243;ra"
  ]
  node [
    id 63
    label "byd&#322;o"
  ]
  node [
    id 64
    label "alkohol"
  ]
  node [
    id 65
    label "wiedza"
  ]
  node [
    id 66
    label "ro&#347;lina"
  ]
  node [
    id 67
    label "&#347;ci&#281;gno"
  ]
  node [
    id 68
    label "&#380;ycie"
  ]
  node [
    id 69
    label "pryncypa&#322;"
  ]
  node [
    id 70
    label "fryzura"
  ]
  node [
    id 71
    label "noosfera"
  ]
  node [
    id 72
    label "kierowa&#263;"
  ]
  node [
    id 73
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 74
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 75
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 76
    label "cecha"
  ]
  node [
    id 77
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 78
    label "zdolno&#347;&#263;"
  ]
  node [
    id 79
    label "kszta&#322;t"
  ]
  node [
    id 80
    label "cz&#322;onek"
  ]
  node [
    id 81
    label "cia&#322;o"
  ]
  node [
    id 82
    label "obiekt"
  ]
  node [
    id 83
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 84
    label "omin&#261;&#263;"
  ]
  node [
    id 85
    label "spowodowa&#263;"
  ]
  node [
    id 86
    label "leave_office"
  ]
  node [
    id 87
    label "forfeit"
  ]
  node [
    id 88
    label "stracenie"
  ]
  node [
    id 89
    label "execute"
  ]
  node [
    id 90
    label "pogorszy&#263;_si&#281;"
  ]
  node [
    id 91
    label "zabi&#263;"
  ]
  node [
    id 92
    label "wytraci&#263;"
  ]
  node [
    id 93
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 94
    label "liszy&#263;_si&#281;"
  ]
  node [
    id 95
    label "przegra&#263;"
  ]
  node [
    id 96
    label "waste"
  ]
  node [
    id 97
    label "wypowied&#378;"
  ]
  node [
    id 98
    label "siniec"
  ]
  node [
    id 99
    label "uwaga"
  ]
  node [
    id 100
    label "rzecz"
  ]
  node [
    id 101
    label "&#380;y&#322;a_&#347;rodkowa_siatk&#243;wki"
  ]
  node [
    id 102
    label "powieka"
  ]
  node [
    id 103
    label "oczy"
  ]
  node [
    id 104
    label "&#347;lepko"
  ]
  node [
    id 105
    label "ga&#322;ka_oczna"
  ]
  node [
    id 106
    label "ser_&#380;&#243;&#322;ty"
  ]
  node [
    id 107
    label "&#347;lepie"
  ]
  node [
    id 108
    label "twarz"
  ]
  node [
    id 109
    label "worek_spoj&#243;wkowy"
  ]
  node [
    id 110
    label "organ"
  ]
  node [
    id 111
    label "nerw_wzrokowy"
  ]
  node [
    id 112
    label "wzrok"
  ]
  node [
    id 113
    label "spoj&#243;wka"
  ]
  node [
    id 114
    label "&#378;renica"
  ]
  node [
    id 115
    label "narz&#261;d_&#322;zowy"
  ]
  node [
    id 116
    label "nab&#322;onek_barwnikowy"
  ]
  node [
    id 117
    label "kaprawie&#263;"
  ]
  node [
    id 118
    label "kaprawienie"
  ]
  node [
    id 119
    label "spojrzenie"
  ]
  node [
    id 120
    label "net"
  ]
  node [
    id 121
    label "r&#261;bek_rog&#243;wki"
  ]
  node [
    id 122
    label "coloboma"
  ]
  node [
    id 123
    label "ros&#243;&#322;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
]
