graph [
  maxDegree 28
  minDegree 1
  meanDegree 2.018018018018018
  density 0.018345618345618344
  graphCliqueNumber 3
  node [
    id 0
    label "dlaczego"
    origin "text"
  ]
  node [
    id 1
    label "dyskusja"
    origin "text"
  ]
  node [
    id 2
    label "emisja"
    origin "text"
  ]
  node [
    id 3
    label "gaz"
    origin "text"
  ]
  node [
    id 4
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 5
    label "si&#281;"
    origin "text"
  ]
  node [
    id 6
    label "du&#380;o"
    origin "text"
  ]
  node [
    id 7
    label "samoch&#243;d"
    origin "text"
  ]
  node [
    id 8
    label "paliwo"
    origin "text"
  ]
  node [
    id 9
    label "przemys&#322;"
    origin "text"
  ]
  node [
    id 10
    label "mi&#281;sny"
    origin "text"
  ]
  node [
    id 11
    label "rozmowa"
  ]
  node [
    id 12
    label "sympozjon"
  ]
  node [
    id 13
    label "conference"
  ]
  node [
    id 14
    label "expense"
  ]
  node [
    id 15
    label "consequence"
  ]
  node [
    id 16
    label "wydobywanie"
  ]
  node [
    id 17
    label "g&#322;os"
  ]
  node [
    id 18
    label "introdukcja"
  ]
  node [
    id 19
    label "przesy&#322;"
  ]
  node [
    id 20
    label "wydzielanie"
  ]
  node [
    id 21
    label "zjawisko"
  ]
  node [
    id 22
    label "publikacja"
  ]
  node [
    id 23
    label "cia&#322;o"
  ]
  node [
    id 24
    label "peda&#322;"
  ]
  node [
    id 25
    label "przy&#347;piesznik"
  ]
  node [
    id 26
    label "p&#322;omie&#324;"
  ]
  node [
    id 27
    label "instalacja"
  ]
  node [
    id 28
    label "gas"
  ]
  node [
    id 29
    label "bro&#324;"
  ]
  node [
    id 30
    label "accelerator"
  ]
  node [
    id 31
    label "rozpr&#281;&#380;liwo&#347;&#263;"
  ]
  node [
    id 32
    label "substancja"
  ]
  node [
    id 33
    label "stan_skupienia"
  ]
  node [
    id 34
    label "termojonizacja"
  ]
  node [
    id 35
    label "remark"
  ]
  node [
    id 36
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 37
    label "u&#380;ywa&#263;"
  ]
  node [
    id 38
    label "okre&#347;la&#263;"
  ]
  node [
    id 39
    label "j&#281;zyk"
  ]
  node [
    id 40
    label "say"
  ]
  node [
    id 41
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 42
    label "formu&#322;owa&#263;"
  ]
  node [
    id 43
    label "talk"
  ]
  node [
    id 44
    label "powiada&#263;"
  ]
  node [
    id 45
    label "informowa&#263;"
  ]
  node [
    id 46
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 47
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 48
    label "wydobywa&#263;"
  ]
  node [
    id 49
    label "express"
  ]
  node [
    id 50
    label "chew_the_fat"
  ]
  node [
    id 51
    label "dysfonia"
  ]
  node [
    id 52
    label "umie&#263;"
  ]
  node [
    id 53
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 54
    label "tell"
  ]
  node [
    id 55
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 56
    label "wyra&#380;a&#263;"
  ]
  node [
    id 57
    label "gaworzy&#263;"
  ]
  node [
    id 58
    label "rozmawia&#263;"
  ]
  node [
    id 59
    label "dziama&#263;"
  ]
  node [
    id 60
    label "prawi&#263;"
  ]
  node [
    id 61
    label "du&#380;y"
  ]
  node [
    id 62
    label "cz&#281;sto"
  ]
  node [
    id 63
    label "bardzo"
  ]
  node [
    id 64
    label "mocno"
  ]
  node [
    id 65
    label "wiela"
  ]
  node [
    id 66
    label "baga&#380;nik"
  ]
  node [
    id 67
    label "immobilizer"
  ]
  node [
    id 68
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 69
    label "ko&#322;o_samochodowe"
  ]
  node [
    id 70
    label "poduszka_powietrzna"
  ]
  node [
    id 71
    label "dachowanie"
  ]
  node [
    id 72
    label "dwu&#347;lad"
  ]
  node [
    id 73
    label "deska_rozdzielcza"
  ]
  node [
    id 74
    label "poci&#261;g_drogowy"
  ]
  node [
    id 75
    label "kierownica"
  ]
  node [
    id 76
    label "pojazd_drogowy"
  ]
  node [
    id 77
    label "uk&#322;ad_kierowniczy"
  ]
  node [
    id 78
    label "pompa_wodna"
  ]
  node [
    id 79
    label "silnik"
  ]
  node [
    id 80
    label "wycieraczka"
  ]
  node [
    id 81
    label "bak"
  ]
  node [
    id 82
    label "ABS"
  ]
  node [
    id 83
    label "most"
  ]
  node [
    id 84
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 85
    label "spryskiwacz"
  ]
  node [
    id 86
    label "t&#322;umik"
  ]
  node [
    id 87
    label "tempomat"
  ]
  node [
    id 88
    label "spalenie"
  ]
  node [
    id 89
    label "spali&#263;"
  ]
  node [
    id 90
    label "fuel"
  ]
  node [
    id 91
    label "tankowanie"
  ]
  node [
    id 92
    label "zgazowa&#263;"
  ]
  node [
    id 93
    label "pompa_wtryskowa"
  ]
  node [
    id 94
    label "tankowa&#263;"
  ]
  node [
    id 95
    label "antydetonator"
  ]
  node [
    id 96
    label "Orlen"
  ]
  node [
    id 97
    label "spalanie"
  ]
  node [
    id 98
    label "spala&#263;"
  ]
  node [
    id 99
    label "gospodarka"
  ]
  node [
    id 100
    label "przechowalnictwo"
  ]
  node [
    id 101
    label "uprzemys&#322;owienie"
  ]
  node [
    id 102
    label "przemys&#322;&#243;wka"
  ]
  node [
    id 103
    label "uprzemys&#322;owienie_si&#281;"
  ]
  node [
    id 104
    label "uprzemys&#322;awianie_si&#281;"
  ]
  node [
    id 105
    label "uprzemys&#322;awianie"
  ]
  node [
    id 106
    label "specjalny"
  ]
  node [
    id 107
    label "bia&#322;kowy"
  ]
  node [
    id 108
    label "sklep"
  ]
  node [
    id 109
    label "naturalny"
  ]
  node [
    id 110
    label "hodowlany"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 32
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 9
    target 100
  ]
  edge [
    source 9
    target 101
  ]
  edge [
    source 9
    target 102
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 104
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 10
    target 106
  ]
  edge [
    source 10
    target 107
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 10
    target 110
  ]
]
