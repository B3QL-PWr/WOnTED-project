graph [
  maxDegree 14
  minDegree 1
  meanDegree 1.9230769230769231
  density 0.07692307692307693
  graphCliqueNumber 2
  node [
    id 0
    label "rok"
    origin "text"
  ]
  node [
    id 1
    label "by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "stulecie"
  ]
  node [
    id 3
    label "kalendarz"
  ]
  node [
    id 4
    label "czas"
  ]
  node [
    id 5
    label "pora_roku"
  ]
  node [
    id 6
    label "cykl_astronomiczny"
  ]
  node [
    id 7
    label "p&#243;&#322;rocze"
  ]
  node [
    id 8
    label "grupa"
  ]
  node [
    id 9
    label "kwarta&#322;"
  ]
  node [
    id 10
    label "kurs"
  ]
  node [
    id 11
    label "jubileusz"
  ]
  node [
    id 12
    label "miesi&#261;c"
  ]
  node [
    id 13
    label "lata"
  ]
  node [
    id 14
    label "martwy_sezon"
  ]
  node [
    id 15
    label "si&#281;ga&#263;"
  ]
  node [
    id 16
    label "trwa&#263;"
  ]
  node [
    id 17
    label "obecno&#347;&#263;"
  ]
  node [
    id 18
    label "stan"
  ]
  node [
    id 19
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 20
    label "stand"
  ]
  node [
    id 21
    label "mie&#263;_miejsce"
  ]
  node [
    id 22
    label "uczestniczy&#263;"
  ]
  node [
    id 23
    label "chodzi&#263;"
  ]
  node [
    id 24
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 25
    label "equal"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
]
