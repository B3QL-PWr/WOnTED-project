graph [
  maxDegree 18
  minDegree 1
  meanDegree 1.9411764705882353
  density 0.058823529411764705
  graphCliqueNumber 2
  node [
    id 0
    label "wszystek"
    origin "text"
  ]
  node [
    id 1
    label "kobieta"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "pi&#281;kne"
    origin "text"
  ]
  node [
    id 4
    label "chyba"
    origin "text"
  ]
  node [
    id 5
    label "przesadzi&#263;by&#263;"
    origin "text"
  ]
  node [
    id 6
    label "ca&#322;y"
  ]
  node [
    id 7
    label "cz&#322;owiek"
  ]
  node [
    id 8
    label "przekwitanie"
  ]
  node [
    id 9
    label "m&#281;&#380;yna"
  ]
  node [
    id 10
    label "babka"
  ]
  node [
    id 11
    label "samica"
  ]
  node [
    id 12
    label "doros&#322;y"
  ]
  node [
    id 13
    label "ulec"
  ]
  node [
    id 14
    label "uleganie"
  ]
  node [
    id 15
    label "partnerka"
  ]
  node [
    id 16
    label "&#380;ona"
  ]
  node [
    id 17
    label "ulega&#263;"
  ]
  node [
    id 18
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 19
    label "pa&#324;stwo"
  ]
  node [
    id 20
    label "ulegni&#281;cie"
  ]
  node [
    id 21
    label "menopauza"
  ]
  node [
    id 22
    label "&#322;ono"
  ]
  node [
    id 23
    label "si&#281;ga&#263;"
  ]
  node [
    id 24
    label "trwa&#263;"
  ]
  node [
    id 25
    label "obecno&#347;&#263;"
  ]
  node [
    id 26
    label "stan"
  ]
  node [
    id 27
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 28
    label "stand"
  ]
  node [
    id 29
    label "mie&#263;_miejsce"
  ]
  node [
    id 30
    label "uczestniczy&#263;"
  ]
  node [
    id 31
    label "chodzi&#263;"
  ]
  node [
    id 32
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 33
    label "equal"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
]
