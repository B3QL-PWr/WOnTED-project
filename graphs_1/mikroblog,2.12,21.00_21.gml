graph [
  maxDegree 19
  minDegree 1
  meanDegree 2.0208333333333335
  density 0.021271929824561404
  graphCliqueNumber 2
  node [
    id 0
    label "pope&#322;ni&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "ilustracja"
    origin "text"
  ]
  node [
    id 2
    label "kartka"
    origin "text"
  ]
  node [
    id 3
    label "&#347;wi&#261;teczny"
    origin "text"
  ]
  node [
    id 4
    label "dla"
    origin "text"
  ]
  node [
    id 5
    label "m&#322;ody"
    origin "text"
  ]
  node [
    id 6
    label "kuzyn"
    origin "text"
  ]
  node [
    id 7
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 8
    label "trafi&#263;"
    origin "text"
  ]
  node [
    id 9
    label "nasi"
    origin "text"
  ]
  node [
    id 10
    label "wojak"
    origin "text"
  ]
  node [
    id 11
    label "okazja"
    origin "text"
  ]
  node [
    id 12
    label "&#347;wi&#281;to"
    origin "text"
  ]
  node [
    id 13
    label "photograph"
  ]
  node [
    id 14
    label "szata_graficzna"
  ]
  node [
    id 15
    label "materia&#322;"
  ]
  node [
    id 16
    label "obrazek"
  ]
  node [
    id 17
    label "ticket"
  ]
  node [
    id 18
    label "kartonik"
  ]
  node [
    id 19
    label "arkusz"
  ]
  node [
    id 20
    label "bon"
  ]
  node [
    id 21
    label "wk&#322;ad"
  ]
  node [
    id 22
    label "faul"
  ]
  node [
    id 23
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 24
    label "s&#281;dzia"
  ]
  node [
    id 25
    label "kara"
  ]
  node [
    id 26
    label "strona"
  ]
  node [
    id 27
    label "dzie&#324;_wolny"
  ]
  node [
    id 28
    label "&#347;wi&#281;tny"
  ]
  node [
    id 29
    label "&#347;wi&#261;tecznie"
  ]
  node [
    id 30
    label "wyj&#261;tkowy"
  ]
  node [
    id 31
    label "od&#347;wi&#281;tny"
  ]
  node [
    id 32
    label "od&#347;wi&#281;tnie"
  ]
  node [
    id 33
    label "obrz&#281;dowy"
  ]
  node [
    id 34
    label "uroczysty"
  ]
  node [
    id 35
    label "cz&#322;owiek"
  ]
  node [
    id 36
    label "nie&#380;onaty"
  ]
  node [
    id 37
    label "wczesny"
  ]
  node [
    id 38
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 39
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 40
    label "charakterystyczny"
  ]
  node [
    id 41
    label "nowo&#380;eniec"
  ]
  node [
    id 42
    label "m&#261;&#380;"
  ]
  node [
    id 43
    label "m&#322;odo"
  ]
  node [
    id 44
    label "nowy"
  ]
  node [
    id 45
    label "organizm"
  ]
  node [
    id 46
    label "krewny"
  ]
  node [
    id 47
    label "kuzynostwo"
  ]
  node [
    id 48
    label "czyj&#347;"
  ]
  node [
    id 49
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 50
    label "przypasowa&#263;"
  ]
  node [
    id 51
    label "wpa&#347;&#263;"
  ]
  node [
    id 52
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 53
    label "spotka&#263;"
  ]
  node [
    id 54
    label "dotrze&#263;"
  ]
  node [
    id 55
    label "zjawi&#263;_si&#281;"
  ]
  node [
    id 56
    label "happen"
  ]
  node [
    id 57
    label "znale&#378;&#263;"
  ]
  node [
    id 58
    label "hit"
  ]
  node [
    id 59
    label "pocisk"
  ]
  node [
    id 60
    label "stumble"
  ]
  node [
    id 61
    label "dolecie&#263;"
  ]
  node [
    id 62
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 63
    label "demobilizowa&#263;"
  ]
  node [
    id 64
    label "rota"
  ]
  node [
    id 65
    label "demobilizowanie"
  ]
  node [
    id 66
    label "walcz&#261;cy"
  ]
  node [
    id 67
    label "harcap"
  ]
  node [
    id 68
    label "&#380;o&#322;nierstwo"
  ]
  node [
    id 69
    label "&#380;o&#322;dowy"
  ]
  node [
    id 70
    label "elew"
  ]
  node [
    id 71
    label "zdemobilizowanie"
  ]
  node [
    id 72
    label "mundurowy"
  ]
  node [
    id 73
    label "s&#322;u&#380;y&#263;_w_wojsku"
  ]
  node [
    id 74
    label "so&#322;dat"
  ]
  node [
    id 75
    label "wojsko"
  ]
  node [
    id 76
    label "s&#322;u&#380;enie_w_wojsku"
  ]
  node [
    id 77
    label "zdemobilizowa&#263;"
  ]
  node [
    id 78
    label "Gurkha"
  ]
  node [
    id 79
    label "atrakcyjny"
  ]
  node [
    id 80
    label "oferta"
  ]
  node [
    id 81
    label "adeptness"
  ]
  node [
    id 82
    label "okazka"
  ]
  node [
    id 83
    label "wydarzenie"
  ]
  node [
    id 84
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 85
    label "podw&#243;zka"
  ]
  node [
    id 86
    label "autostop"
  ]
  node [
    id 87
    label "sytuacja"
  ]
  node [
    id 88
    label "&#346;wi&#281;to_Pracy"
  ]
  node [
    id 89
    label "czas"
  ]
  node [
    id 90
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 91
    label "Nowy_Rok"
  ]
  node [
    id 92
    label "Godowe_&#346;wi&#281;to"
  ]
  node [
    id 93
    label "dzie&#324;_&#347;wi&#261;teczny"
  ]
  node [
    id 94
    label "Barb&#243;rka"
  ]
  node [
    id 95
    label "ramadan"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 35
  ]
  edge [
    source 10
    target 63
  ]
  edge [
    source 10
    target 64
  ]
  edge [
    source 10
    target 65
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 10
    target 69
  ]
  edge [
    source 10
    target 70
  ]
  edge [
    source 10
    target 71
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 79
  ]
  edge [
    source 11
    target 80
  ]
  edge [
    source 11
    target 81
  ]
  edge [
    source 11
    target 82
  ]
  edge [
    source 11
    target 83
  ]
  edge [
    source 11
    target 84
  ]
  edge [
    source 11
    target 85
  ]
  edge [
    source 11
    target 86
  ]
  edge [
    source 11
    target 87
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 12
    target 89
  ]
  edge [
    source 12
    target 90
  ]
  edge [
    source 12
    target 91
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 12
    target 94
  ]
  edge [
    source 12
    target 95
  ]
]
