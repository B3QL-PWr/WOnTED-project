graph [
  maxDegree 28
  minDegree 1
  meanDegree 1.9705882352941178
  density 0.029411764705882353
  graphCliqueNumber 2
  node [
    id 0
    label "rz&#261;dowy"
    origin "text"
  ]
  node [
    id 1
    label "pomys&#322;"
    origin "text"
  ]
  node [
    id 2
    label "dodatkowy"
    origin "text"
  ]
  node [
    id 3
    label "opodatkowanie"
    origin "text"
  ]
  node [
    id 4
    label "pojazd"
    origin "text"
  ]
  node [
    id 5
    label "pojemno&#347;&#263;"
    origin "text"
  ]
  node [
    id 6
    label "powy&#380;ej"
    origin "text"
  ]
  node [
    id 7
    label "zmieni&#263;"
    origin "text"
  ]
  node [
    id 8
    label "si&#281;"
    origin "text"
  ]
  node [
    id 9
    label "diametralnie"
    origin "text"
  ]
  node [
    id 10
    label "wsp&#243;lny"
  ]
  node [
    id 11
    label "system"
  ]
  node [
    id 12
    label "wytw&#243;r"
  ]
  node [
    id 13
    label "idea"
  ]
  node [
    id 14
    label "ukra&#347;&#263;"
  ]
  node [
    id 15
    label "ukradzenie"
  ]
  node [
    id 16
    label "pocz&#261;tki"
  ]
  node [
    id 17
    label "poboczny"
  ]
  node [
    id 18
    label "dodatkowo"
  ]
  node [
    id 19
    label "uboczny"
  ]
  node [
    id 20
    label "op&#322;ata"
  ]
  node [
    id 21
    label "danina"
  ]
  node [
    id 22
    label "tax"
  ]
  node [
    id 23
    label "trybut"
  ]
  node [
    id 24
    label "obci&#261;&#380;enie"
  ]
  node [
    id 25
    label "tr&#243;jk&#261;t_Harbergera"
  ]
  node [
    id 26
    label "fukanie"
  ]
  node [
    id 27
    label "przeszklenie"
  ]
  node [
    id 28
    label "pod&#322;oga"
  ]
  node [
    id 29
    label "odzywka"
  ]
  node [
    id 30
    label "powietrze"
  ]
  node [
    id 31
    label "przyholowanie"
  ]
  node [
    id 32
    label "fukni&#281;cie"
  ]
  node [
    id 33
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 34
    label "przestrze&#324;_pozaziemska"
  ]
  node [
    id 35
    label "hamulec"
  ]
  node [
    id 36
    label "nadwozie"
  ]
  node [
    id 37
    label "zielona_karta"
  ]
  node [
    id 38
    label "przyholowywa&#263;"
  ]
  node [
    id 39
    label "test_zderzeniowy"
  ]
  node [
    id 40
    label "odholowanie"
  ]
  node [
    id 41
    label "tabor"
  ]
  node [
    id 42
    label "odholowywanie"
  ]
  node [
    id 43
    label "przyholowywanie"
  ]
  node [
    id 44
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 45
    label "l&#261;d"
  ]
  node [
    id 46
    label "przyholowa&#263;"
  ]
  node [
    id 47
    label "odholowa&#263;"
  ]
  node [
    id 48
    label "podwozie"
  ]
  node [
    id 49
    label "woda"
  ]
  node [
    id 50
    label "odholowywa&#263;"
  ]
  node [
    id 51
    label "prowadzenie_si&#281;"
  ]
  node [
    id 52
    label "rozmiar"
  ]
  node [
    id 53
    label "cyrkumferencja"
  ]
  node [
    id 54
    label "circumference"
  ]
  node [
    id 55
    label "powy&#380;szy"
  ]
  node [
    id 56
    label "wcze&#347;niej"
  ]
  node [
    id 57
    label "come_up"
  ]
  node [
    id 58
    label "straci&#263;"
  ]
  node [
    id 59
    label "przej&#347;&#263;"
  ]
  node [
    id 60
    label "zast&#261;pi&#263;"
  ]
  node [
    id 61
    label "sprawi&#263;"
  ]
  node [
    id 62
    label "zyska&#263;"
  ]
  node [
    id 63
    label "zrobi&#263;"
  ]
  node [
    id 64
    label "change"
  ]
  node [
    id 65
    label "diametralny"
  ]
  node [
    id 66
    label "intensywnie"
  ]
  node [
    id 67
    label "zupe&#322;nie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 9
    target 67
  ]
]
