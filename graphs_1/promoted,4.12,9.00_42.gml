graph [
  maxDegree 15
  minDegree 1
  meanDegree 1.9333333333333333
  density 0.06666666666666667
  graphCliqueNumber 2
  node [
    id 0
    label "prowadz&#261;ca"
    origin "text"
  ]
  node [
    id 1
    label "gastronomia"
    origin "text"
  ]
  node [
    id 2
    label "czeka&#263;"
    origin "text"
  ]
  node [
    id 3
    label "zmiana"
    origin "text"
  ]
  node [
    id 4
    label "horeca"
  ]
  node [
    id 5
    label "us&#322;ugi"
  ]
  node [
    id 6
    label "kuchnia"
  ]
  node [
    id 7
    label "sztuka"
  ]
  node [
    id 8
    label "by&#263;"
  ]
  node [
    id 9
    label "hold"
  ]
  node [
    id 10
    label "sp&#281;dza&#263;"
  ]
  node [
    id 11
    label "look"
  ]
  node [
    id 12
    label "decydowa&#263;"
  ]
  node [
    id 13
    label "oczekiwa&#263;"
  ]
  node [
    id 14
    label "pauzowa&#263;"
  ]
  node [
    id 15
    label "anticipate"
  ]
  node [
    id 16
    label "anatomopatolog"
  ]
  node [
    id 17
    label "rewizja"
  ]
  node [
    id 18
    label "oznaka"
  ]
  node [
    id 19
    label "czas"
  ]
  node [
    id 20
    label "ferment"
  ]
  node [
    id 21
    label "komplet"
  ]
  node [
    id 22
    label "tura"
  ]
  node [
    id 23
    label "amendment"
  ]
  node [
    id 24
    label "zmianka"
  ]
  node [
    id 25
    label "odmienianie"
  ]
  node [
    id 26
    label "passage"
  ]
  node [
    id 27
    label "zjawisko"
  ]
  node [
    id 28
    label "change"
  ]
  node [
    id 29
    label "praca"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
]
