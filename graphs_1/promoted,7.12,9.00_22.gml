graph [
  maxDegree 38
  minDegree 1
  meanDegree 1.9154929577464788
  density 0.027364185110663984
  graphCliqueNumber 2
  node [
    id 0
    label "pa&#378;dziernik"
    origin "text"
  ]
  node [
    id 1
    label "polska"
    origin "text"
  ]
  node [
    id 2
    label "wypadek"
    origin "text"
  ]
  node [
    id 3
    label "drogowe"
    origin "text"
  ]
  node [
    id 4
    label "zgin&#261;&#263;"
    origin "text"
  ]
  node [
    id 5
    label "osoba"
    origin "text"
  ]
  node [
    id 6
    label "Dzie&#324;_Papieski"
  ]
  node [
    id 7
    label "oktober"
  ]
  node [
    id 8
    label "miesi&#261;c"
  ]
  node [
    id 9
    label "czynno&#347;&#263;"
  ]
  node [
    id 10
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 11
    label "motyw"
  ]
  node [
    id 12
    label "ubezpieczenie_wypadkowe"
  ]
  node [
    id 13
    label "fabu&#322;a"
  ]
  node [
    id 14
    label "przebiec"
  ]
  node [
    id 15
    label "wydarzenie"
  ]
  node [
    id 16
    label "happening"
  ]
  node [
    id 17
    label "przebiegni&#281;cie"
  ]
  node [
    id 18
    label "event"
  ]
  node [
    id 19
    label "charakter"
  ]
  node [
    id 20
    label "fail"
  ]
  node [
    id 21
    label "os&#322;abn&#261;&#263;"
  ]
  node [
    id 22
    label "gulf"
  ]
  node [
    id 23
    label "po&#380;egna&#263;_si&#281;_z_&#380;yciem"
  ]
  node [
    id 24
    label "przesta&#263;"
  ]
  node [
    id 25
    label "przepa&#347;&#263;"
  ]
  node [
    id 26
    label "sko&#324;czy&#263;"
  ]
  node [
    id 27
    label "znikn&#261;&#263;"
  ]
  node [
    id 28
    label "przegra&#263;"
  ]
  node [
    id 29
    label "die"
  ]
  node [
    id 30
    label "Zgredek"
  ]
  node [
    id 31
    label "kategoria_gramatyczna"
  ]
  node [
    id 32
    label "Casanova"
  ]
  node [
    id 33
    label "Don_Juan"
  ]
  node [
    id 34
    label "Gargantua"
  ]
  node [
    id 35
    label "Faust"
  ]
  node [
    id 36
    label "profanum"
  ]
  node [
    id 37
    label "Chocho&#322;"
  ]
  node [
    id 38
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 39
    label "koniugacja"
  ]
  node [
    id 40
    label "Winnetou"
  ]
  node [
    id 41
    label "Dwukwiat"
  ]
  node [
    id 42
    label "homo_sapiens"
  ]
  node [
    id 43
    label "Edyp"
  ]
  node [
    id 44
    label "Herkules_Poirot"
  ]
  node [
    id 45
    label "ludzko&#347;&#263;"
  ]
  node [
    id 46
    label "mikrokosmos"
  ]
  node [
    id 47
    label "person"
  ]
  node [
    id 48
    label "Szwejk"
  ]
  node [
    id 49
    label "portrecista"
  ]
  node [
    id 50
    label "Sherlock_Holmes"
  ]
  node [
    id 51
    label "Hamlet"
  ]
  node [
    id 52
    label "duch"
  ]
  node [
    id 53
    label "oddzia&#322;ywanie"
  ]
  node [
    id 54
    label "g&#322;owa"
  ]
  node [
    id 55
    label "Quasimodo"
  ]
  node [
    id 56
    label "Dulcynea"
  ]
  node [
    id 57
    label "Wallenrod"
  ]
  node [
    id 58
    label "Don_Kiszot"
  ]
  node [
    id 59
    label "Plastu&#347;"
  ]
  node [
    id 60
    label "Harry_Potter"
  ]
  node [
    id 61
    label "figura"
  ]
  node [
    id 62
    label "parali&#380;owa&#263;"
  ]
  node [
    id 63
    label "istota"
  ]
  node [
    id 64
    label "Werter"
  ]
  node [
    id 65
    label "antropochoria"
  ]
  node [
    id 66
    label "posta&#263;"
  ]
  node [
    id 67
    label "ambasada"
  ]
  node [
    id 68
    label "Szwecja"
  ]
  node [
    id 69
    label "Erik"
  ]
  node [
    id 70
    label "Friberg"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 69
    target 70
  ]
]
