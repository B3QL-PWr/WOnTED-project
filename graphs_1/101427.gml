graph [
  maxDegree 10
  minDegree 1
  meanDegree 2.076923076923077
  density 0.08307692307692308
  graphCliqueNumber 4
  node [
    id 0
    label "katastrofa"
    origin "text"
  ]
  node [
    id 1
    label "lot"
    origin "text"
  ]
  node [
    id 2
    label "china"
    origin "text"
  ]
  node [
    id 3
    label "northwest"
    origin "text"
  ]
  node [
    id 4
    label "airlines"
    origin "text"
  ]
  node [
    id 5
    label "Smole&#324;sk"
  ]
  node [
    id 6
    label "nieszcz&#281;&#347;cie"
  ]
  node [
    id 7
    label "visitation"
  ]
  node [
    id 8
    label "wydarzenie"
  ]
  node [
    id 9
    label "chronometra&#380;ysta"
  ]
  node [
    id 10
    label "start"
  ]
  node [
    id 11
    label "ruch"
  ]
  node [
    id 12
    label "ci&#261;g"
  ]
  node [
    id 13
    label "l&#261;dowanie"
  ]
  node [
    id 14
    label "odlot"
  ]
  node [
    id 15
    label "podr&#243;&#380;"
  ]
  node [
    id 16
    label "flight"
  ]
  node [
    id 17
    label "talerz_perkusyjny"
  ]
  node [
    id 18
    label "Northwest"
  ]
  node [
    id 19
    label "Airlines"
  ]
  node [
    id 20
    label "2303"
  ]
  node [
    id 21
    label "Tupolev"
  ]
  node [
    id 22
    label "tu"
  ]
  node [
    id 23
    label "154M"
  ]
  node [
    id 24
    label "Xianyang"
  ]
  node [
    id 25
    label "Airport"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 20
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 23
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 24
    target 25
  ]
]
