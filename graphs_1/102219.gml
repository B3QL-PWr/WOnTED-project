graph [
  maxDegree 36
  minDegree 1
  meanDegree 18.714285714285715
  density 0.34025974025974026
  graphCliqueNumber 31
  node [
    id 0
    label "dziennik"
    origin "text"
  ]
  node [
    id 1
    label "ustawa"
    origin "text"
  ]
  node [
    id 2
    label "wrzesie&#324;"
    origin "text"
  ]
  node [
    id 3
    label "poz"
    origin "text"
  ]
  node [
    id 4
    label "spis"
  ]
  node [
    id 5
    label "sheet"
  ]
  node [
    id 6
    label "gazeta"
  ]
  node [
    id 7
    label "diariusz"
  ]
  node [
    id 8
    label "pami&#281;tnik"
  ]
  node [
    id 9
    label "journal"
  ]
  node [
    id 10
    label "ksi&#281;ga"
  ]
  node [
    id 11
    label "program_informacyjny"
  ]
  node [
    id 12
    label "Karta_Nauczyciela"
  ]
  node [
    id 13
    label "marc&#243;wka"
  ]
  node [
    id 14
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 15
    label "akt"
  ]
  node [
    id 16
    label "przej&#347;&#263;"
  ]
  node [
    id 17
    label "charter"
  ]
  node [
    id 18
    label "przej&#347;cie"
  ]
  node [
    id 19
    label "miesi&#261;c"
  ]
  node [
    id 20
    label "ustawi&#263;"
  ]
  node [
    id 21
    label "rozporz&#261;dzi&#263;"
  ]
  node [
    id 22
    label "krajowy"
  ]
  node [
    id 23
    label "rada"
  ]
  node [
    id 24
    label "radiofonia"
  ]
  node [
    id 25
    label "i"
  ]
  node [
    id 26
    label "telewizja"
  ]
  node [
    id 27
    label "zeszyt"
  ]
  node [
    id 28
    label "dzie&#324;"
  ]
  node [
    id 29
    label "29"
  ]
  node [
    id 30
    label "grudzie&#324;"
  ]
  node [
    id 31
    label "1992"
  ]
  node [
    id 32
    label "rok"
  ]
  node [
    id 33
    label "ojciec"
  ]
  node [
    id 34
    label "u"
  ]
  node [
    id 35
    label "4"
  ]
  node [
    id 36
    label "stycze&#324;"
  ]
  node [
    id 37
    label "2007"
  ]
  node [
    id 38
    label "wyspa"
  ]
  node [
    id 39
    label "sprawa"
  ]
  node [
    id 40
    label "zawarto&#347;&#263;"
  ]
  node [
    id 41
    label "wniosek"
  ]
  node [
    id 42
    label "udzieli&#263;"
  ]
  node [
    id 43
    label "koncesja"
  ]
  node [
    id 44
    label "oraz"
  ]
  node [
    id 45
    label "szczeg&#243;&#322;owy"
  ]
  node [
    id 46
    label "tryb"
  ]
  node [
    id 47
    label "post&#281;powa&#263;"
  ]
  node [
    id 48
    label "udziela&#263;"
  ]
  node [
    id 49
    label "cofa&#263;"
  ]
  node [
    id 50
    label "na"
  ]
  node [
    id 51
    label "rozpowszechnia&#263;"
  ]
  node [
    id 52
    label "rozprowadza&#263;"
  ]
  node [
    id 53
    label "program"
  ]
  node [
    id 54
    label "radiofoniczny"
  ]
  node [
    id 55
    label "telewizyjny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 23
  ]
  edge [
    source 21
    target 24
  ]
  edge [
    source 21
    target 25
  ]
  edge [
    source 21
    target 26
  ]
  edge [
    source 21
    target 27
  ]
  edge [
    source 21
    target 28
  ]
  edge [
    source 21
    target 35
  ]
  edge [
    source 21
    target 36
  ]
  edge [
    source 21
    target 37
  ]
  edge [
    source 21
    target 32
  ]
  edge [
    source 21
    target 38
  ]
  edge [
    source 21
    target 39
  ]
  edge [
    source 21
    target 40
  ]
  edge [
    source 21
    target 41
  ]
  edge [
    source 21
    target 33
  ]
  edge [
    source 21
    target 42
  ]
  edge [
    source 21
    target 43
  ]
  edge [
    source 21
    target 44
  ]
  edge [
    source 21
    target 45
  ]
  edge [
    source 21
    target 46
  ]
  edge [
    source 21
    target 47
  ]
  edge [
    source 21
    target 48
  ]
  edge [
    source 21
    target 49
  ]
  edge [
    source 21
    target 50
  ]
  edge [
    source 21
    target 51
  ]
  edge [
    source 21
    target 52
  ]
  edge [
    source 21
    target 53
  ]
  edge [
    source 21
    target 54
  ]
  edge [
    source 21
    target 55
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 24
  ]
  edge [
    source 22
    target 25
  ]
  edge [
    source 22
    target 26
  ]
  edge [
    source 22
    target 27
  ]
  edge [
    source 22
    target 28
  ]
  edge [
    source 22
    target 35
  ]
  edge [
    source 22
    target 36
  ]
  edge [
    source 22
    target 37
  ]
  edge [
    source 22
    target 32
  ]
  edge [
    source 22
    target 38
  ]
  edge [
    source 22
    target 39
  ]
  edge [
    source 22
    target 40
  ]
  edge [
    source 22
    target 41
  ]
  edge [
    source 22
    target 33
  ]
  edge [
    source 22
    target 42
  ]
  edge [
    source 22
    target 43
  ]
  edge [
    source 22
    target 44
  ]
  edge [
    source 22
    target 45
  ]
  edge [
    source 22
    target 46
  ]
  edge [
    source 22
    target 47
  ]
  edge [
    source 22
    target 48
  ]
  edge [
    source 22
    target 49
  ]
  edge [
    source 22
    target 50
  ]
  edge [
    source 22
    target 51
  ]
  edge [
    source 22
    target 52
  ]
  edge [
    source 22
    target 53
  ]
  edge [
    source 22
    target 54
  ]
  edge [
    source 22
    target 55
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 25
  ]
  edge [
    source 23
    target 26
  ]
  edge [
    source 23
    target 27
  ]
  edge [
    source 23
    target 28
  ]
  edge [
    source 23
    target 35
  ]
  edge [
    source 23
    target 36
  ]
  edge [
    source 23
    target 37
  ]
  edge [
    source 23
    target 32
  ]
  edge [
    source 23
    target 38
  ]
  edge [
    source 23
    target 39
  ]
  edge [
    source 23
    target 40
  ]
  edge [
    source 23
    target 41
  ]
  edge [
    source 23
    target 33
  ]
  edge [
    source 23
    target 42
  ]
  edge [
    source 23
    target 43
  ]
  edge [
    source 23
    target 44
  ]
  edge [
    source 23
    target 45
  ]
  edge [
    source 23
    target 46
  ]
  edge [
    source 23
    target 47
  ]
  edge [
    source 23
    target 48
  ]
  edge [
    source 23
    target 49
  ]
  edge [
    source 23
    target 50
  ]
  edge [
    source 23
    target 51
  ]
  edge [
    source 23
    target 52
  ]
  edge [
    source 23
    target 53
  ]
  edge [
    source 23
    target 54
  ]
  edge [
    source 23
    target 55
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 26
  ]
  edge [
    source 24
    target 27
  ]
  edge [
    source 24
    target 28
  ]
  edge [
    source 24
    target 29
  ]
  edge [
    source 24
    target 30
  ]
  edge [
    source 24
    target 31
  ]
  edge [
    source 24
    target 32
  ]
  edge [
    source 24
    target 33
  ]
  edge [
    source 24
    target 35
  ]
  edge [
    source 24
    target 36
  ]
  edge [
    source 24
    target 37
  ]
  edge [
    source 24
    target 38
  ]
  edge [
    source 24
    target 39
  ]
  edge [
    source 24
    target 40
  ]
  edge [
    source 24
    target 41
  ]
  edge [
    source 24
    target 42
  ]
  edge [
    source 24
    target 43
  ]
  edge [
    source 24
    target 44
  ]
  edge [
    source 24
    target 45
  ]
  edge [
    source 24
    target 46
  ]
  edge [
    source 24
    target 47
  ]
  edge [
    source 24
    target 48
  ]
  edge [
    source 24
    target 49
  ]
  edge [
    source 24
    target 50
  ]
  edge [
    source 24
    target 51
  ]
  edge [
    source 24
    target 52
  ]
  edge [
    source 24
    target 53
  ]
  edge [
    source 24
    target 54
  ]
  edge [
    source 24
    target 55
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 27
  ]
  edge [
    source 25
    target 28
  ]
  edge [
    source 25
    target 29
  ]
  edge [
    source 25
    target 30
  ]
  edge [
    source 25
    target 31
  ]
  edge [
    source 25
    target 32
  ]
  edge [
    source 25
    target 33
  ]
  edge [
    source 25
    target 35
  ]
  edge [
    source 25
    target 36
  ]
  edge [
    source 25
    target 37
  ]
  edge [
    source 25
    target 38
  ]
  edge [
    source 25
    target 39
  ]
  edge [
    source 25
    target 40
  ]
  edge [
    source 25
    target 41
  ]
  edge [
    source 25
    target 42
  ]
  edge [
    source 25
    target 43
  ]
  edge [
    source 25
    target 44
  ]
  edge [
    source 25
    target 45
  ]
  edge [
    source 25
    target 46
  ]
  edge [
    source 25
    target 47
  ]
  edge [
    source 25
    target 48
  ]
  edge [
    source 25
    target 25
  ]
  edge [
    source 25
    target 49
  ]
  edge [
    source 25
    target 50
  ]
  edge [
    source 25
    target 51
  ]
  edge [
    source 25
    target 52
  ]
  edge [
    source 25
    target 53
  ]
  edge [
    source 25
    target 54
  ]
  edge [
    source 25
    target 55
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 28
  ]
  edge [
    source 26
    target 29
  ]
  edge [
    source 26
    target 30
  ]
  edge [
    source 26
    target 31
  ]
  edge [
    source 26
    target 32
  ]
  edge [
    source 26
    target 33
  ]
  edge [
    source 26
    target 35
  ]
  edge [
    source 26
    target 36
  ]
  edge [
    source 26
    target 37
  ]
  edge [
    source 26
    target 38
  ]
  edge [
    source 26
    target 39
  ]
  edge [
    source 26
    target 40
  ]
  edge [
    source 26
    target 41
  ]
  edge [
    source 26
    target 42
  ]
  edge [
    source 26
    target 43
  ]
  edge [
    source 26
    target 44
  ]
  edge [
    source 26
    target 45
  ]
  edge [
    source 26
    target 46
  ]
  edge [
    source 26
    target 47
  ]
  edge [
    source 26
    target 48
  ]
  edge [
    source 26
    target 49
  ]
  edge [
    source 26
    target 50
  ]
  edge [
    source 26
    target 51
  ]
  edge [
    source 26
    target 52
  ]
  edge [
    source 26
    target 53
  ]
  edge [
    source 26
    target 54
  ]
  edge [
    source 26
    target 55
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 29
  ]
  edge [
    source 27
    target 30
  ]
  edge [
    source 27
    target 31
  ]
  edge [
    source 27
    target 32
  ]
  edge [
    source 27
    target 33
  ]
  edge [
    source 27
    target 35
  ]
  edge [
    source 27
    target 36
  ]
  edge [
    source 27
    target 37
  ]
  edge [
    source 27
    target 38
  ]
  edge [
    source 27
    target 39
  ]
  edge [
    source 27
    target 40
  ]
  edge [
    source 27
    target 41
  ]
  edge [
    source 27
    target 42
  ]
  edge [
    source 27
    target 43
  ]
  edge [
    source 27
    target 44
  ]
  edge [
    source 27
    target 45
  ]
  edge [
    source 27
    target 46
  ]
  edge [
    source 27
    target 47
  ]
  edge [
    source 27
    target 48
  ]
  edge [
    source 27
    target 49
  ]
  edge [
    source 27
    target 50
  ]
  edge [
    source 27
    target 51
  ]
  edge [
    source 27
    target 52
  ]
  edge [
    source 27
    target 53
  ]
  edge [
    source 27
    target 54
  ]
  edge [
    source 27
    target 55
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 30
  ]
  edge [
    source 28
    target 31
  ]
  edge [
    source 28
    target 32
  ]
  edge [
    source 28
    target 33
  ]
  edge [
    source 28
    target 35
  ]
  edge [
    source 28
    target 36
  ]
  edge [
    source 28
    target 37
  ]
  edge [
    source 28
    target 38
  ]
  edge [
    source 28
    target 39
  ]
  edge [
    source 28
    target 40
  ]
  edge [
    source 28
    target 41
  ]
  edge [
    source 28
    target 42
  ]
  edge [
    source 28
    target 43
  ]
  edge [
    source 28
    target 44
  ]
  edge [
    source 28
    target 45
  ]
  edge [
    source 28
    target 46
  ]
  edge [
    source 28
    target 47
  ]
  edge [
    source 28
    target 48
  ]
  edge [
    source 28
    target 49
  ]
  edge [
    source 28
    target 50
  ]
  edge [
    source 28
    target 51
  ]
  edge [
    source 28
    target 52
  ]
  edge [
    source 28
    target 53
  ]
  edge [
    source 28
    target 54
  ]
  edge [
    source 28
    target 55
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 31
  ]
  edge [
    source 29
    target 32
  ]
  edge [
    source 29
    target 33
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 32
  ]
  edge [
    source 30
    target 33
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 33
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 35
  ]
  edge [
    source 32
    target 36
  ]
  edge [
    source 32
    target 37
  ]
  edge [
    source 32
    target 38
  ]
  edge [
    source 32
    target 39
  ]
  edge [
    source 32
    target 40
  ]
  edge [
    source 32
    target 41
  ]
  edge [
    source 32
    target 42
  ]
  edge [
    source 32
    target 43
  ]
  edge [
    source 32
    target 44
  ]
  edge [
    source 32
    target 45
  ]
  edge [
    source 32
    target 46
  ]
  edge [
    source 32
    target 47
  ]
  edge [
    source 32
    target 48
  ]
  edge [
    source 32
    target 49
  ]
  edge [
    source 32
    target 50
  ]
  edge [
    source 32
    target 51
  ]
  edge [
    source 32
    target 52
  ]
  edge [
    source 32
    target 53
  ]
  edge [
    source 32
    target 54
  ]
  edge [
    source 32
    target 55
  ]
  edge [
    source 33
    target 35
  ]
  edge [
    source 33
    target 36
  ]
  edge [
    source 33
    target 37
  ]
  edge [
    source 33
    target 38
  ]
  edge [
    source 33
    target 39
  ]
  edge [
    source 33
    target 40
  ]
  edge [
    source 33
    target 41
  ]
  edge [
    source 33
    target 42
  ]
  edge [
    source 33
    target 43
  ]
  edge [
    source 33
    target 44
  ]
  edge [
    source 33
    target 45
  ]
  edge [
    source 33
    target 46
  ]
  edge [
    source 33
    target 47
  ]
  edge [
    source 33
    target 48
  ]
  edge [
    source 33
    target 49
  ]
  edge [
    source 33
    target 50
  ]
  edge [
    source 33
    target 51
  ]
  edge [
    source 33
    target 52
  ]
  edge [
    source 33
    target 53
  ]
  edge [
    source 33
    target 54
  ]
  edge [
    source 33
    target 55
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 37
  ]
  edge [
    source 35
    target 38
  ]
  edge [
    source 35
    target 39
  ]
  edge [
    source 35
    target 40
  ]
  edge [
    source 35
    target 41
  ]
  edge [
    source 35
    target 42
  ]
  edge [
    source 35
    target 43
  ]
  edge [
    source 35
    target 44
  ]
  edge [
    source 35
    target 45
  ]
  edge [
    source 35
    target 46
  ]
  edge [
    source 35
    target 47
  ]
  edge [
    source 35
    target 48
  ]
  edge [
    source 35
    target 49
  ]
  edge [
    source 35
    target 50
  ]
  edge [
    source 35
    target 51
  ]
  edge [
    source 35
    target 52
  ]
  edge [
    source 35
    target 53
  ]
  edge [
    source 35
    target 54
  ]
  edge [
    source 35
    target 55
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 38
  ]
  edge [
    source 36
    target 39
  ]
  edge [
    source 36
    target 40
  ]
  edge [
    source 36
    target 41
  ]
  edge [
    source 36
    target 42
  ]
  edge [
    source 36
    target 43
  ]
  edge [
    source 36
    target 44
  ]
  edge [
    source 36
    target 45
  ]
  edge [
    source 36
    target 46
  ]
  edge [
    source 36
    target 47
  ]
  edge [
    source 36
    target 48
  ]
  edge [
    source 36
    target 49
  ]
  edge [
    source 36
    target 50
  ]
  edge [
    source 36
    target 51
  ]
  edge [
    source 36
    target 52
  ]
  edge [
    source 36
    target 53
  ]
  edge [
    source 36
    target 54
  ]
  edge [
    source 36
    target 55
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 39
  ]
  edge [
    source 37
    target 40
  ]
  edge [
    source 37
    target 41
  ]
  edge [
    source 37
    target 42
  ]
  edge [
    source 37
    target 43
  ]
  edge [
    source 37
    target 44
  ]
  edge [
    source 37
    target 45
  ]
  edge [
    source 37
    target 46
  ]
  edge [
    source 37
    target 47
  ]
  edge [
    source 37
    target 48
  ]
  edge [
    source 37
    target 49
  ]
  edge [
    source 37
    target 50
  ]
  edge [
    source 37
    target 51
  ]
  edge [
    source 37
    target 52
  ]
  edge [
    source 37
    target 53
  ]
  edge [
    source 37
    target 54
  ]
  edge [
    source 37
    target 55
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 40
  ]
  edge [
    source 38
    target 41
  ]
  edge [
    source 38
    target 42
  ]
  edge [
    source 38
    target 43
  ]
  edge [
    source 38
    target 44
  ]
  edge [
    source 38
    target 45
  ]
  edge [
    source 38
    target 46
  ]
  edge [
    source 38
    target 47
  ]
  edge [
    source 38
    target 38
  ]
  edge [
    source 38
    target 48
  ]
  edge [
    source 38
    target 49
  ]
  edge [
    source 38
    target 50
  ]
  edge [
    source 38
    target 51
  ]
  edge [
    source 38
    target 52
  ]
  edge [
    source 38
    target 53
  ]
  edge [
    source 38
    target 54
  ]
  edge [
    source 38
    target 55
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 41
  ]
  edge [
    source 39
    target 42
  ]
  edge [
    source 39
    target 43
  ]
  edge [
    source 39
    target 44
  ]
  edge [
    source 39
    target 45
  ]
  edge [
    source 39
    target 46
  ]
  edge [
    source 39
    target 47
  ]
  edge [
    source 39
    target 39
  ]
  edge [
    source 39
    target 48
  ]
  edge [
    source 39
    target 49
  ]
  edge [
    source 39
    target 50
  ]
  edge [
    source 39
    target 51
  ]
  edge [
    source 39
    target 52
  ]
  edge [
    source 39
    target 53
  ]
  edge [
    source 39
    target 54
  ]
  edge [
    source 39
    target 55
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 42
  ]
  edge [
    source 40
    target 43
  ]
  edge [
    source 40
    target 44
  ]
  edge [
    source 40
    target 45
  ]
  edge [
    source 40
    target 46
  ]
  edge [
    source 40
    target 47
  ]
  edge [
    source 40
    target 48
  ]
  edge [
    source 40
    target 49
  ]
  edge [
    source 40
    target 50
  ]
  edge [
    source 40
    target 51
  ]
  edge [
    source 40
    target 52
  ]
  edge [
    source 40
    target 53
  ]
  edge [
    source 40
    target 54
  ]
  edge [
    source 40
    target 55
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 43
  ]
  edge [
    source 41
    target 44
  ]
  edge [
    source 41
    target 45
  ]
  edge [
    source 41
    target 46
  ]
  edge [
    source 41
    target 47
  ]
  edge [
    source 41
    target 48
  ]
  edge [
    source 41
    target 49
  ]
  edge [
    source 41
    target 50
  ]
  edge [
    source 41
    target 51
  ]
  edge [
    source 41
    target 52
  ]
  edge [
    source 41
    target 53
  ]
  edge [
    source 41
    target 54
  ]
  edge [
    source 41
    target 55
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 44
  ]
  edge [
    source 42
    target 45
  ]
  edge [
    source 42
    target 46
  ]
  edge [
    source 42
    target 47
  ]
  edge [
    source 42
    target 48
  ]
  edge [
    source 42
    target 49
  ]
  edge [
    source 42
    target 50
  ]
  edge [
    source 42
    target 51
  ]
  edge [
    source 42
    target 52
  ]
  edge [
    source 42
    target 53
  ]
  edge [
    source 42
    target 54
  ]
  edge [
    source 42
    target 55
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 45
  ]
  edge [
    source 43
    target 46
  ]
  edge [
    source 43
    target 47
  ]
  edge [
    source 43
    target 48
  ]
  edge [
    source 43
    target 49
  ]
  edge [
    source 43
    target 43
  ]
  edge [
    source 43
    target 50
  ]
  edge [
    source 43
    target 51
  ]
  edge [
    source 43
    target 52
  ]
  edge [
    source 43
    target 53
  ]
  edge [
    source 43
    target 54
  ]
  edge [
    source 43
    target 55
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 46
  ]
  edge [
    source 44
    target 47
  ]
  edge [
    source 44
    target 48
  ]
  edge [
    source 44
    target 49
  ]
  edge [
    source 44
    target 50
  ]
  edge [
    source 44
    target 51
  ]
  edge [
    source 44
    target 52
  ]
  edge [
    source 44
    target 53
  ]
  edge [
    source 44
    target 54
  ]
  edge [
    source 44
    target 55
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 47
  ]
  edge [
    source 45
    target 48
  ]
  edge [
    source 45
    target 49
  ]
  edge [
    source 45
    target 50
  ]
  edge [
    source 45
    target 51
  ]
  edge [
    source 45
    target 52
  ]
  edge [
    source 45
    target 53
  ]
  edge [
    source 45
    target 54
  ]
  edge [
    source 45
    target 55
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 48
  ]
  edge [
    source 46
    target 49
  ]
  edge [
    source 46
    target 50
  ]
  edge [
    source 46
    target 51
  ]
  edge [
    source 46
    target 52
  ]
  edge [
    source 46
    target 53
  ]
  edge [
    source 46
    target 54
  ]
  edge [
    source 46
    target 55
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 49
  ]
  edge [
    source 47
    target 50
  ]
  edge [
    source 47
    target 51
  ]
  edge [
    source 47
    target 52
  ]
  edge [
    source 47
    target 53
  ]
  edge [
    source 47
    target 54
  ]
  edge [
    source 47
    target 55
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 50
  ]
  edge [
    source 48
    target 51
  ]
  edge [
    source 48
    target 52
  ]
  edge [
    source 48
    target 53
  ]
  edge [
    source 48
    target 54
  ]
  edge [
    source 48
    target 55
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 51
  ]
  edge [
    source 49
    target 52
  ]
  edge [
    source 49
    target 53
  ]
  edge [
    source 49
    target 54
  ]
  edge [
    source 49
    target 55
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 52
  ]
  edge [
    source 50
    target 53
  ]
  edge [
    source 50
    target 54
  ]
  edge [
    source 50
    target 55
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 53
  ]
  edge [
    source 51
    target 54
  ]
  edge [
    source 51
    target 55
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 54
  ]
  edge [
    source 52
    target 55
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 55
  ]
  edge [
    source 54
    target 55
  ]
]
