graph [
  maxDegree 15
  minDegree 1
  meanDegree 1.8888888888888888
  density 0.1111111111111111
  graphCliqueNumber 2
  node [
    id 0
    label "kole&#380;anka"
    origin "text"
  ]
  node [
    id 1
    label "prosi&#263;"
    origin "text"
  ]
  node [
    id 2
    label "udost&#281;pnienie"
    origin "text"
  ]
  node [
    id 3
    label "kuma"
  ]
  node [
    id 4
    label "trwa&#263;"
  ]
  node [
    id 5
    label "zezwala&#263;"
  ]
  node [
    id 6
    label "ask"
  ]
  node [
    id 7
    label "invite"
  ]
  node [
    id 8
    label "zach&#281;ca&#263;"
  ]
  node [
    id 9
    label "preach"
  ]
  node [
    id 10
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 11
    label "pies"
  ]
  node [
    id 12
    label "ujmowa&#263;_si&#281;"
  ]
  node [
    id 13
    label "poleca&#263;"
  ]
  node [
    id 14
    label "zaprasza&#263;"
  ]
  node [
    id 15
    label "suffice"
  ]
  node [
    id 16
    label "dziwi&#263;_si&#281;"
  ]
  node [
    id 17
    label "umo&#380;liwienie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 17
  ]
]
