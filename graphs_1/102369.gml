graph [
  maxDegree 70
  minDegree 1
  meanDegree 2.195275590551181
  density 0.0034625797958220523
  graphCliqueNumber 3
  node [
    id 0
    label "by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "wielki"
    origin "text"
  ]
  node [
    id 2
    label "fan"
    origin "text"
  ]
  node [
    id 3
    label "pottera"
    origin "text"
  ]
  node [
    id 4
    label "uwielbia&#263;"
    origin "text"
  ]
  node [
    id 5
    label "serial"
    origin "text"
  ]
  node [
    id 6
    label "weeds"
    origin "text"
  ]
  node [
    id 7
    label "niecierpliwo&#347;&#263;"
    origin "text"
  ]
  node [
    id 8
    label "czeka&#322;o"
    origin "text"
  ]
  node [
    id 9
    label "pocz&#261;tek"
    origin "text"
  ]
  node [
    id 10
    label "trzeci"
    origin "text"
  ]
  node [
    id 11
    label "sezon"
    origin "text"
  ]
  node [
    id 12
    label "pierwsza"
    origin "text"
  ]
  node [
    id 13
    label "emisja"
    origin "text"
  ]
  node [
    id 14
    label "ameryka&#324;ski"
    origin "text"
  ]
  node [
    id 15
    label "telewizja"
    origin "text"
  ]
  node [
    id 16
    label "zapowiedzie&#263;"
    origin "text"
  ]
  node [
    id 17
    label "sierpie&#324;"
    origin "text"
  ]
  node [
    id 18
    label "cho&#263;"
    origin "text"
  ]
  node [
    id 19
    label "jeszcze"
    origin "text"
  ]
  node [
    id 20
    label "trzy"
    origin "text"
  ]
  node [
    id 21
    label "tydzie&#324;"
    origin "text"
  ]
  node [
    id 22
    label "widzie&#263;by&#263;"
    origin "text"
  ]
  node [
    id 23
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 24
    label "odcinek"
    origin "text"
  ]
  node [
    id 25
    label "pojawi&#263;"
    origin "text"
  ]
  node [
    id 26
    label "si&#281;"
    origin "text"
  ]
  node [
    id 27
    label "sie&#263;"
    origin "text"
  ]
  node [
    id 28
    label "zreszt&#261;"
    origin "text"
  ]
  node [
    id 29
    label "tylko"
    origin "text"
  ]
  node [
    id 30
    label "wymian"
    origin "text"
  ]
  node [
    id 31
    label "plik"
    origin "text"
  ]
  node [
    id 32
    label "coraz"
    origin "text"
  ]
  node [
    id 33
    label "du&#380;o"
    origin "text"
  ]
  node [
    id 34
    label "oznaczony"
    origin "text"
  ]
  node [
    id 35
    label "jako"
    origin "text"
  ]
  node [
    id 36
    label "premiera"
    origin "text"
  ]
  node [
    id 37
    label "wydawa&#263;"
    origin "text"
  ]
  node [
    id 38
    label "amerykanin"
    origin "text"
  ]
  node [
    id 39
    label "kojarzy&#263;"
    origin "text"
  ]
  node [
    id 40
    label "radykalny"
    origin "text"
  ]
  node [
    id 41
    label "podej&#347;cie"
    origin "text"
  ]
  node [
    id 42
    label "prawa"
    origin "text"
  ]
  node [
    id 43
    label "autorski"
    origin "text"
  ]
  node [
    id 44
    label "wykazywa&#263;"
    origin "text"
  ]
  node [
    id 45
    label "dystans"
    origin "text"
  ]
  node [
    id 46
    label "ni&#380;"
    origin "text"
  ]
  node [
    id 47
    label "polski"
    origin "text"
  ]
  node [
    id 48
    label "firma"
    origin "text"
  ]
  node [
    id 49
    label "zwi&#261;za&#263;"
    origin "text"
  ]
  node [
    id 50
    label "przemys&#322;"
    origin "text"
  ]
  node [
    id 51
    label "kultura"
    origin "text"
  ]
  node [
    id 52
    label "jesie&#324;"
    origin "text"
  ]
  node [
    id 53
    label "burzliwy"
    origin "text"
  ]
  node [
    id 54
    label "okres"
    origin "text"
  ]
  node [
    id 55
    label "gdy"
    origin "text"
  ]
  node [
    id 56
    label "stacja"
    origin "text"
  ]
  node [
    id 57
    label "wakacyjny"
    origin "text"
  ]
  node [
    id 58
    label "przerwa"
    origin "text"
  ]
  node [
    id 59
    label "zdwoi&#263;"
    origin "text"
  ]
  node [
    id 60
    label "energia"
    origin "text"
  ]
  node [
    id 61
    label "rusza&#263;"
    origin "text"
  ]
  node [
    id 62
    label "walka"
    origin "text"
  ]
  node [
    id 63
    label "widz"
    origin "text"
  ]
  node [
    id 64
    label "rynek"
    origin "text"
  ]
  node [
    id 65
    label "trafia&#263;"
    origin "text"
  ]
  node [
    id 66
    label "te&#380;"
    origin "text"
  ]
  node [
    id 67
    label "p&#322;yt"
    origin "text"
  ]
  node [
    id 68
    label "dvd"
    origin "text"
  ]
  node [
    id 69
    label "nagranie"
    origin "text"
  ]
  node [
    id 70
    label "starsi"
    origin "text"
  ]
  node [
    id 71
    label "dlatego"
    origin "text"
  ]
  node [
    id 72
    label "r&#243;wnie&#380;"
    origin "text"
  ]
  node [
    id 73
    label "czas"
    origin "text"
  ]
  node [
    id 74
    label "wzm&#243;c"
    origin "text"
  ]
  node [
    id 75
    label "promocja"
    origin "text"
  ]
  node [
    id 76
    label "si&#281;ga&#263;"
  ]
  node [
    id 77
    label "trwa&#263;"
  ]
  node [
    id 78
    label "obecno&#347;&#263;"
  ]
  node [
    id 79
    label "stan"
  ]
  node [
    id 80
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 81
    label "stand"
  ]
  node [
    id 82
    label "mie&#263;_miejsce"
  ]
  node [
    id 83
    label "uczestniczy&#263;"
  ]
  node [
    id 84
    label "chodzi&#263;"
  ]
  node [
    id 85
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 86
    label "equal"
  ]
  node [
    id 87
    label "dupny"
  ]
  node [
    id 88
    label "wysoce"
  ]
  node [
    id 89
    label "wyj&#261;tkowy"
  ]
  node [
    id 90
    label "wybitny"
  ]
  node [
    id 91
    label "znaczny"
  ]
  node [
    id 92
    label "prawdziwy"
  ]
  node [
    id 93
    label "wa&#380;ny"
  ]
  node [
    id 94
    label "nieprzeci&#281;tny"
  ]
  node [
    id 95
    label "mi&#322;o&#347;nik"
  ]
  node [
    id 96
    label "fan_club"
  ]
  node [
    id 97
    label "fandom"
  ]
  node [
    id 98
    label "love"
  ]
  node [
    id 99
    label "w&#347;cieka&#263;_si&#281;"
  ]
  node [
    id 100
    label "lubi&#263;"
  ]
  node [
    id 101
    label "seria"
  ]
  node [
    id 102
    label "Klan"
  ]
  node [
    id 103
    label "film"
  ]
  node [
    id 104
    label "Ranczo"
  ]
  node [
    id 105
    label "program_telewizyjny"
  ]
  node [
    id 106
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 107
    label "nierozwa&#380;no&#347;&#263;"
  ]
  node [
    id 108
    label "brak"
  ]
  node [
    id 109
    label "niespokojno&#347;&#263;"
  ]
  node [
    id 110
    label "miejsce"
  ]
  node [
    id 111
    label "faza"
  ]
  node [
    id 112
    label "upgrade"
  ]
  node [
    id 113
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 114
    label "pierworodztwo"
  ]
  node [
    id 115
    label "nast&#281;pstwo"
  ]
  node [
    id 116
    label "cz&#322;owiek"
  ]
  node [
    id 117
    label "neutralny"
  ]
  node [
    id 118
    label "przypadkowy"
  ]
  node [
    id 119
    label "dzie&#324;"
  ]
  node [
    id 120
    label "postronnie"
  ]
  node [
    id 121
    label "season"
  ]
  node [
    id 122
    label "rok"
  ]
  node [
    id 123
    label "godzina"
  ]
  node [
    id 124
    label "expense"
  ]
  node [
    id 125
    label "consequence"
  ]
  node [
    id 126
    label "wydobywanie"
  ]
  node [
    id 127
    label "g&#322;os"
  ]
  node [
    id 128
    label "introdukcja"
  ]
  node [
    id 129
    label "przesy&#322;"
  ]
  node [
    id 130
    label "wydzielanie"
  ]
  node [
    id 131
    label "zjawisko"
  ]
  node [
    id 132
    label "publikacja"
  ]
  node [
    id 133
    label "nowoczesny"
  ]
  node [
    id 134
    label "p&#243;&#322;nocnoameryka&#324;ski"
  ]
  node [
    id 135
    label "po_ameryka&#324;sku"
  ]
  node [
    id 136
    label "boston"
  ]
  node [
    id 137
    label "cake-walk"
  ]
  node [
    id 138
    label "charakterystyczny"
  ]
  node [
    id 139
    label "placek_ameryka&#324;ski"
  ]
  node [
    id 140
    label "fajny"
  ]
  node [
    id 141
    label "j&#281;zyk_angielski"
  ]
  node [
    id 142
    label "Princeton"
  ]
  node [
    id 143
    label "pepperoni"
  ]
  node [
    id 144
    label "sa&#322;atka_Snickers"
  ]
  node [
    id 145
    label "zachodni"
  ]
  node [
    id 146
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 147
    label "anglosaski"
  ]
  node [
    id 148
    label "typowy"
  ]
  node [
    id 149
    label "Polsat"
  ]
  node [
    id 150
    label "paj&#281;czarz"
  ]
  node [
    id 151
    label "gadaj&#261;ca_g&#322;owa"
  ]
  node [
    id 152
    label "programowiec"
  ]
  node [
    id 153
    label "technologia"
  ]
  node [
    id 154
    label "urz&#261;dzenie_RTV"
  ]
  node [
    id 155
    label "Interwizja"
  ]
  node [
    id 156
    label "BBC"
  ]
  node [
    id 157
    label "ekran"
  ]
  node [
    id 158
    label "redakcja"
  ]
  node [
    id 159
    label "media"
  ]
  node [
    id 160
    label "odbieranie"
  ]
  node [
    id 161
    label "odbiera&#263;"
  ]
  node [
    id 162
    label "odbiornik"
  ]
  node [
    id 163
    label "instytucja"
  ]
  node [
    id 164
    label "du&#380;y_pok&#243;j"
  ]
  node [
    id 165
    label "studio"
  ]
  node [
    id 166
    label "telekomunikacja"
  ]
  node [
    id 167
    label "muza"
  ]
  node [
    id 168
    label "announce"
  ]
  node [
    id 169
    label "spowodowa&#263;"
  ]
  node [
    id 170
    label "przestrzec"
  ]
  node [
    id 171
    label "sign"
  ]
  node [
    id 172
    label "poinformowa&#263;"
  ]
  node [
    id 173
    label "og&#322;osi&#263;"
  ]
  node [
    id 174
    label "Wniebowzi&#281;cie_Naj&#347;wi&#281;tszej_Maryi_Panny"
  ]
  node [
    id 175
    label "miesi&#261;c"
  ]
  node [
    id 176
    label "Sierpie&#324;"
  ]
  node [
    id 177
    label "ci&#261;gle"
  ]
  node [
    id 178
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 179
    label "doba"
  ]
  node [
    id 180
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 181
    label "weekend"
  ]
  node [
    id 182
    label "pole"
  ]
  node [
    id 183
    label "part"
  ]
  node [
    id 184
    label "pokwitowanie"
  ]
  node [
    id 185
    label "kawa&#322;ek"
  ]
  node [
    id 186
    label "coupon"
  ]
  node [
    id 187
    label "teren"
  ]
  node [
    id 188
    label "line"
  ]
  node [
    id 189
    label "epizod"
  ]
  node [
    id 190
    label "moneta"
  ]
  node [
    id 191
    label "fragment"
  ]
  node [
    id 192
    label "hipertekst"
  ]
  node [
    id 193
    label "gauze"
  ]
  node [
    id 194
    label "nitka"
  ]
  node [
    id 195
    label "mesh"
  ]
  node [
    id 196
    label "e-hazard"
  ]
  node [
    id 197
    label "netbook"
  ]
  node [
    id 198
    label "cyberprzestrze&#324;"
  ]
  node [
    id 199
    label "biznes_elektroniczny"
  ]
  node [
    id 200
    label "snu&#263;"
  ]
  node [
    id 201
    label "organization"
  ]
  node [
    id 202
    label "zasadzka"
  ]
  node [
    id 203
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 204
    label "web"
  ]
  node [
    id 205
    label "provider"
  ]
  node [
    id 206
    label "struktura"
  ]
  node [
    id 207
    label "us&#322;uga_internetowa"
  ]
  node [
    id 208
    label "punkt_dost&#281;pu"
  ]
  node [
    id 209
    label "organizacja"
  ]
  node [
    id 210
    label "mem"
  ]
  node [
    id 211
    label "vane"
  ]
  node [
    id 212
    label "podcast"
  ]
  node [
    id 213
    label "grooming"
  ]
  node [
    id 214
    label "kszta&#322;t"
  ]
  node [
    id 215
    label "strona"
  ]
  node [
    id 216
    label "obiekt"
  ]
  node [
    id 217
    label "wysnu&#263;"
  ]
  node [
    id 218
    label "gra_sieciowa"
  ]
  node [
    id 219
    label "instalacja"
  ]
  node [
    id 220
    label "sie&#263;_komputerowa"
  ]
  node [
    id 221
    label "net"
  ]
  node [
    id 222
    label "plecionka"
  ]
  node [
    id 223
    label "rozmieszczenie"
  ]
  node [
    id 224
    label "budownictwo"
  ]
  node [
    id 225
    label "belka"
  ]
  node [
    id 226
    label "dokument"
  ]
  node [
    id 227
    label "menad&#380;er_plik&#243;w"
  ]
  node [
    id 228
    label "nadpisywanie"
  ]
  node [
    id 229
    label "nadpisanie"
  ]
  node [
    id 230
    label "nadpisa&#263;"
  ]
  node [
    id 231
    label "paczka"
  ]
  node [
    id 232
    label "podkatalog"
  ]
  node [
    id 233
    label "bundle"
  ]
  node [
    id 234
    label "folder"
  ]
  node [
    id 235
    label "nadpisywa&#263;"
  ]
  node [
    id 236
    label "du&#380;y"
  ]
  node [
    id 237
    label "cz&#281;sto"
  ]
  node [
    id 238
    label "bardzo"
  ]
  node [
    id 239
    label "mocno"
  ]
  node [
    id 240
    label "wiela"
  ]
  node [
    id 241
    label "przedstawienie"
  ]
  node [
    id 242
    label "premiere"
  ]
  node [
    id 243
    label "impart"
  ]
  node [
    id 244
    label "panna_na_wydaniu"
  ]
  node [
    id 245
    label "surrender"
  ]
  node [
    id 246
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 247
    label "train"
  ]
  node [
    id 248
    label "give"
  ]
  node [
    id 249
    label "wytwarza&#263;"
  ]
  node [
    id 250
    label "dawa&#263;"
  ]
  node [
    id 251
    label "zapach"
  ]
  node [
    id 252
    label "wprowadza&#263;"
  ]
  node [
    id 253
    label "ujawnia&#263;"
  ]
  node [
    id 254
    label "wydawnictwo"
  ]
  node [
    id 255
    label "powierza&#263;"
  ]
  node [
    id 256
    label "produkcja"
  ]
  node [
    id 257
    label "denuncjowa&#263;"
  ]
  node [
    id 258
    label "plon"
  ]
  node [
    id 259
    label "reszta"
  ]
  node [
    id 260
    label "robi&#263;"
  ]
  node [
    id 261
    label "placard"
  ]
  node [
    id 262
    label "tajemnica"
  ]
  node [
    id 263
    label "wiano"
  ]
  node [
    id 264
    label "d&#378;wi&#281;k"
  ]
  node [
    id 265
    label "podawa&#263;"
  ]
  node [
    id 266
    label "cover"
  ]
  node [
    id 267
    label "rozumie&#263;"
  ]
  node [
    id 268
    label "zaskakiwa&#263;"
  ]
  node [
    id 269
    label "swat"
  ]
  node [
    id 270
    label "relate"
  ]
  node [
    id 271
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 272
    label "gruntowny"
  ]
  node [
    id 273
    label "konsekwentny"
  ]
  node [
    id 274
    label "bezkompromisowy"
  ]
  node [
    id 275
    label "radykalnie"
  ]
  node [
    id 276
    label "powaga"
  ]
  node [
    id 277
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 278
    label "cecha"
  ]
  node [
    id 279
    label "droga"
  ]
  node [
    id 280
    label "ploy"
  ]
  node [
    id 281
    label "nastawienie"
  ]
  node [
    id 282
    label "potraktowanie"
  ]
  node [
    id 283
    label "nasi&#261;kni&#281;cie"
  ]
  node [
    id 284
    label "nabranie"
  ]
  node [
    id 285
    label "w&#322;asny"
  ]
  node [
    id 286
    label "oryginalny"
  ]
  node [
    id 287
    label "autorsko"
  ]
  node [
    id 288
    label "uzasadnia&#263;"
  ]
  node [
    id 289
    label "testify"
  ]
  node [
    id 290
    label "court"
  ]
  node [
    id 291
    label "wyra&#380;a&#263;"
  ]
  node [
    id 292
    label "stwierdza&#263;"
  ]
  node [
    id 293
    label "ostro&#380;no&#347;&#263;"
  ]
  node [
    id 294
    label "stosunek"
  ]
  node [
    id 295
    label "nieufno&#347;&#263;"
  ]
  node [
    id 296
    label "przek&#322;adka"
  ]
  node [
    id 297
    label "pow&#347;ci&#261;gliwo&#347;&#263;"
  ]
  node [
    id 298
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 299
    label "punkt_widzenia"
  ]
  node [
    id 300
    label "rozmiar"
  ]
  node [
    id 301
    label "trasa"
  ]
  node [
    id 302
    label "r&#243;&#380;nica"
  ]
  node [
    id 303
    label "element_konstrukcyjny"
  ]
  node [
    id 304
    label "poziom"
  ]
  node [
    id 305
    label "depression"
  ]
  node [
    id 306
    label "nizina"
  ]
  node [
    id 307
    label "lacki"
  ]
  node [
    id 308
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 309
    label "przedmiot"
  ]
  node [
    id 310
    label "sztajer"
  ]
  node [
    id 311
    label "drabant"
  ]
  node [
    id 312
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 313
    label "polak"
  ]
  node [
    id 314
    label "pierogi_ruskie"
  ]
  node [
    id 315
    label "krakowiak"
  ]
  node [
    id 316
    label "Polish"
  ]
  node [
    id 317
    label "j&#281;zyk"
  ]
  node [
    id 318
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 319
    label "oberek"
  ]
  node [
    id 320
    label "po_polsku"
  ]
  node [
    id 321
    label "mazur"
  ]
  node [
    id 322
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 323
    label "chodzony"
  ]
  node [
    id 324
    label "skoczny"
  ]
  node [
    id 325
    label "ryba_po_grecku"
  ]
  node [
    id 326
    label "goniony"
  ]
  node [
    id 327
    label "polsko"
  ]
  node [
    id 328
    label "Hortex"
  ]
  node [
    id 329
    label "MAC"
  ]
  node [
    id 330
    label "reengineering"
  ]
  node [
    id 331
    label "nazwa_w&#322;asna"
  ]
  node [
    id 332
    label "podmiot_gospodarczy"
  ]
  node [
    id 333
    label "Google"
  ]
  node [
    id 334
    label "zaufanie"
  ]
  node [
    id 335
    label "biurowiec"
  ]
  node [
    id 336
    label "networking"
  ]
  node [
    id 337
    label "zasoby_ludzkie"
  ]
  node [
    id 338
    label "interes"
  ]
  node [
    id 339
    label "paczkarnia"
  ]
  node [
    id 340
    label "Canon"
  ]
  node [
    id 341
    label "HP"
  ]
  node [
    id 342
    label "Baltona"
  ]
  node [
    id 343
    label "Pewex"
  ]
  node [
    id 344
    label "MAN_SE"
  ]
  node [
    id 345
    label "Apeks"
  ]
  node [
    id 346
    label "zasoby"
  ]
  node [
    id 347
    label "Orbis"
  ]
  node [
    id 348
    label "miejsce_pracy"
  ]
  node [
    id 349
    label "siedziba"
  ]
  node [
    id 350
    label "Spo&#322;em"
  ]
  node [
    id 351
    label "przedsi&#281;biorczo&#347;&#263;"
  ]
  node [
    id 352
    label "Orlen"
  ]
  node [
    id 353
    label "klasa"
  ]
  node [
    id 354
    label "st&#281;&#380;e&#263;"
  ]
  node [
    id 355
    label "tobo&#322;ek"
  ]
  node [
    id 356
    label "obezw&#322;adni&#263;"
  ]
  node [
    id 357
    label "scali&#263;"
  ]
  node [
    id 358
    label "zawi&#261;za&#263;"
  ]
  node [
    id 359
    label "zatrzyma&#263;"
  ]
  node [
    id 360
    label "form"
  ]
  node [
    id 361
    label "bind"
  ]
  node [
    id 362
    label "wi&#261;zanie_chemiczne"
  ]
  node [
    id 363
    label "unify"
  ]
  node [
    id 364
    label "consort"
  ]
  node [
    id 365
    label "incorporate"
  ]
  node [
    id 366
    label "wi&#281;&#378;"
  ]
  node [
    id 367
    label "zobowi&#261;za&#263;"
  ]
  node [
    id 368
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 369
    label "w&#281;ze&#322;"
  ]
  node [
    id 370
    label "cz&#261;steczka_chemiczna"
  ]
  node [
    id 371
    label "powi&#261;za&#263;"
  ]
  node [
    id 372
    label "opakowa&#263;"
  ]
  node [
    id 373
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 374
    label "cement"
  ]
  node [
    id 375
    label "zaprawa"
  ]
  node [
    id 376
    label "gospodarka"
  ]
  node [
    id 377
    label "przechowalnictwo"
  ]
  node [
    id 378
    label "uprzemys&#322;owienie"
  ]
  node [
    id 379
    label "przemys&#322;&#243;wka"
  ]
  node [
    id 380
    label "uprzemys&#322;owienie_si&#281;"
  ]
  node [
    id 381
    label "uprzemys&#322;awianie_si&#281;"
  ]
  node [
    id 382
    label "uprzemys&#322;awianie"
  ]
  node [
    id 383
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 384
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 385
    label "Wsch&#243;d"
  ]
  node [
    id 386
    label "rzecz"
  ]
  node [
    id 387
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 388
    label "sztuka"
  ]
  node [
    id 389
    label "religia"
  ]
  node [
    id 390
    label "przejmowa&#263;"
  ]
  node [
    id 391
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 392
    label "makrokosmos"
  ]
  node [
    id 393
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 394
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 395
    label "praca_rolnicza"
  ]
  node [
    id 396
    label "tradycja"
  ]
  node [
    id 397
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 398
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 399
    label "przejmowanie"
  ]
  node [
    id 400
    label "asymilowanie_si&#281;"
  ]
  node [
    id 401
    label "przej&#261;&#263;"
  ]
  node [
    id 402
    label "hodowla"
  ]
  node [
    id 403
    label "brzoskwiniarnia"
  ]
  node [
    id 404
    label "populace"
  ]
  node [
    id 405
    label "konwencja"
  ]
  node [
    id 406
    label "propriety"
  ]
  node [
    id 407
    label "jako&#347;&#263;"
  ]
  node [
    id 408
    label "kuchnia"
  ]
  node [
    id 409
    label "zwyczaj"
  ]
  node [
    id 410
    label "przej&#281;cie"
  ]
  node [
    id 411
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 412
    label "pora_roku"
  ]
  node [
    id 413
    label "summer"
  ]
  node [
    id 414
    label "porywczy"
  ]
  node [
    id 415
    label "niesta&#322;y"
  ]
  node [
    id 416
    label "nerwowy"
  ]
  node [
    id 417
    label "niespokojny"
  ]
  node [
    id 418
    label "zmienny"
  ]
  node [
    id 419
    label "pe&#322;ny"
  ]
  node [
    id 420
    label "burzliwie"
  ]
  node [
    id 421
    label "wzburzony"
  ]
  node [
    id 422
    label "g&#322;o&#347;ny"
  ]
  node [
    id 423
    label "paleogen"
  ]
  node [
    id 424
    label "spell"
  ]
  node [
    id 425
    label "period"
  ]
  node [
    id 426
    label "prekambr"
  ]
  node [
    id 427
    label "jura"
  ]
  node [
    id 428
    label "interstadia&#322;"
  ]
  node [
    id 429
    label "jednostka_geologiczna"
  ]
  node [
    id 430
    label "izochronizm"
  ]
  node [
    id 431
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 432
    label "okres_noachijski"
  ]
  node [
    id 433
    label "orosir"
  ]
  node [
    id 434
    label "sten"
  ]
  node [
    id 435
    label "kreda"
  ]
  node [
    id 436
    label "drugorz&#281;d"
  ]
  node [
    id 437
    label "semester"
  ]
  node [
    id 438
    label "trzeciorz&#281;d"
  ]
  node [
    id 439
    label "ton"
  ]
  node [
    id 440
    label "dzieje"
  ]
  node [
    id 441
    label "poprzednik"
  ]
  node [
    id 442
    label "kalim"
  ]
  node [
    id 443
    label "ordowik"
  ]
  node [
    id 444
    label "karbon"
  ]
  node [
    id 445
    label "trias"
  ]
  node [
    id 446
    label "stater"
  ]
  node [
    id 447
    label "era"
  ]
  node [
    id 448
    label "cykl"
  ]
  node [
    id 449
    label "p&#243;&#322;okres"
  ]
  node [
    id 450
    label "czwartorz&#281;d"
  ]
  node [
    id 451
    label "pulsacja"
  ]
  node [
    id 452
    label "okres_amazo&#324;ski"
  ]
  node [
    id 453
    label "kambr"
  ]
  node [
    id 454
    label "Zeitgeist"
  ]
  node [
    id 455
    label "nast&#281;pnik"
  ]
  node [
    id 456
    label "kriogen"
  ]
  node [
    id 457
    label "glacja&#322;"
  ]
  node [
    id 458
    label "fala"
  ]
  node [
    id 459
    label "okres_czasu"
  ]
  node [
    id 460
    label "riak"
  ]
  node [
    id 461
    label "schy&#322;ek"
  ]
  node [
    id 462
    label "okres_hesperyjski"
  ]
  node [
    id 463
    label "sylur"
  ]
  node [
    id 464
    label "dewon"
  ]
  node [
    id 465
    label "ciota"
  ]
  node [
    id 466
    label "epoka"
  ]
  node [
    id 467
    label "pierwszorz&#281;d"
  ]
  node [
    id 468
    label "okres_halsztacki"
  ]
  node [
    id 469
    label "ektas"
  ]
  node [
    id 470
    label "zdanie"
  ]
  node [
    id 471
    label "condition"
  ]
  node [
    id 472
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 473
    label "rok_akademicki"
  ]
  node [
    id 474
    label "wapie&#324;_muszlowy"
  ]
  node [
    id 475
    label "postglacja&#322;"
  ]
  node [
    id 476
    label "proces_fizjologiczny"
  ]
  node [
    id 477
    label "ediakar"
  ]
  node [
    id 478
    label "time_period"
  ]
  node [
    id 479
    label "ruch_drgaj&#261;cy"
  ]
  node [
    id 480
    label "perm"
  ]
  node [
    id 481
    label "rok_szkolny"
  ]
  node [
    id 482
    label "neogen"
  ]
  node [
    id 483
    label "sider"
  ]
  node [
    id 484
    label "flow"
  ]
  node [
    id 485
    label "podokres"
  ]
  node [
    id 486
    label "preglacja&#322;"
  ]
  node [
    id 487
    label "retoryka"
  ]
  node [
    id 488
    label "choroba_przyrodzona"
  ]
  node [
    id 489
    label "droga_krzy&#380;owa"
  ]
  node [
    id 490
    label "punkt"
  ]
  node [
    id 491
    label "urz&#261;dzenie"
  ]
  node [
    id 492
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 493
    label "weso&#322;y"
  ]
  node [
    id 494
    label "wakacyjnie"
  ]
  node [
    id 495
    label "pauza"
  ]
  node [
    id 496
    label "przedzia&#322;"
  ]
  node [
    id 497
    label "zwielokrotni&#263;"
  ]
  node [
    id 498
    label "double_over"
  ]
  node [
    id 499
    label "egzergia"
  ]
  node [
    id 500
    label "kwant_energii"
  ]
  node [
    id 501
    label "szwung"
  ]
  node [
    id 502
    label "energy"
  ]
  node [
    id 503
    label "licznik_energii_elektrycznej"
  ]
  node [
    id 504
    label "emitowanie"
  ]
  node [
    id 505
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 506
    label "power"
  ]
  node [
    id 507
    label "emitowa&#263;"
  ]
  node [
    id 508
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 509
    label "podnosi&#263;"
  ]
  node [
    id 510
    label "meet"
  ]
  node [
    id 511
    label "work"
  ]
  node [
    id 512
    label "act"
  ]
  node [
    id 513
    label "begin"
  ]
  node [
    id 514
    label "zabiera&#263;"
  ]
  node [
    id 515
    label "drive"
  ]
  node [
    id 516
    label "wzbudza&#263;"
  ]
  node [
    id 517
    label "zaczyna&#263;"
  ]
  node [
    id 518
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 519
    label "go"
  ]
  node [
    id 520
    label "powodowa&#263;"
  ]
  node [
    id 521
    label "czyn"
  ]
  node [
    id 522
    label "trudno&#347;&#263;"
  ]
  node [
    id 523
    label "obrona"
  ]
  node [
    id 524
    label "zaatakowanie"
  ]
  node [
    id 525
    label "konfrontacyjny"
  ]
  node [
    id 526
    label "military_action"
  ]
  node [
    id 527
    label "wrestle"
  ]
  node [
    id 528
    label "action"
  ]
  node [
    id 529
    label "wydarzenie"
  ]
  node [
    id 530
    label "rywalizacja"
  ]
  node [
    id 531
    label "sambo"
  ]
  node [
    id 532
    label "contest"
  ]
  node [
    id 533
    label "sp&#243;r"
  ]
  node [
    id 534
    label "publiczno&#347;&#263;"
  ]
  node [
    id 535
    label "odbiorca"
  ]
  node [
    id 536
    label "ogl&#261;dacz"
  ]
  node [
    id 537
    label "widownia"
  ]
  node [
    id 538
    label "stoisko"
  ]
  node [
    id 539
    label "plac"
  ]
  node [
    id 540
    label "targowica"
  ]
  node [
    id 541
    label "wprowadzanie"
  ]
  node [
    id 542
    label "wprowadzi&#263;"
  ]
  node [
    id 543
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 544
    label "rynek_wt&#243;rny"
  ]
  node [
    id 545
    label "wprowadzenie"
  ]
  node [
    id 546
    label "kram"
  ]
  node [
    id 547
    label "pojawienie_si&#281;"
  ]
  node [
    id 548
    label "rynek_podstawowy"
  ]
  node [
    id 549
    label "biznes"
  ]
  node [
    id 550
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 551
    label "obiekt_handlowy"
  ]
  node [
    id 552
    label "konsument"
  ]
  node [
    id 553
    label "wytw&#243;rca"
  ]
  node [
    id 554
    label "segment_rynku"
  ]
  node [
    id 555
    label "wska&#378;nik_rynkowy"
  ]
  node [
    id 556
    label "rynek_r&#243;wnoleg&#322;y"
  ]
  node [
    id 557
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 558
    label "wpada&#263;"
  ]
  node [
    id 559
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 560
    label "dolatywa&#263;"
  ]
  node [
    id 561
    label "happen"
  ]
  node [
    id 562
    label "zjawia&#263;_si&#281;"
  ]
  node [
    id 563
    label "hit"
  ]
  node [
    id 564
    label "spotyka&#263;"
  ]
  node [
    id 565
    label "indicate"
  ]
  node [
    id 566
    label "znajdowa&#263;"
  ]
  node [
    id 567
    label "dosi&#281;ga&#263;"
  ]
  node [
    id 568
    label "pocisk"
  ]
  node [
    id 569
    label "dociera&#263;"
  ]
  node [
    id 570
    label "dopasowywa&#263;_si&#281;"
  ]
  node [
    id 571
    label "wys&#322;uchanie"
  ]
  node [
    id 572
    label "wytw&#243;r"
  ]
  node [
    id 573
    label "wys&#322;ucha&#263;"
  ]
  node [
    id 574
    label "recording"
  ]
  node [
    id 575
    label "utrwalenie"
  ]
  node [
    id 576
    label "stary"
  ]
  node [
    id 577
    label "stara"
  ]
  node [
    id 578
    label "rodzice"
  ]
  node [
    id 579
    label "czasokres"
  ]
  node [
    id 580
    label "trawienie"
  ]
  node [
    id 581
    label "kategoria_gramatyczna"
  ]
  node [
    id 582
    label "odczyt"
  ]
  node [
    id 583
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 584
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 585
    label "chwila"
  ]
  node [
    id 586
    label "poprzedzenie"
  ]
  node [
    id 587
    label "koniugacja"
  ]
  node [
    id 588
    label "poprzedzi&#263;"
  ]
  node [
    id 589
    label "przep&#322;ywanie"
  ]
  node [
    id 590
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 591
    label "odwlekanie_si&#281;"
  ]
  node [
    id 592
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 593
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 594
    label "pochodzi&#263;"
  ]
  node [
    id 595
    label "czwarty_wymiar"
  ]
  node [
    id 596
    label "chronometria"
  ]
  node [
    id 597
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 598
    label "poprzedzanie"
  ]
  node [
    id 599
    label "pogoda"
  ]
  node [
    id 600
    label "zegar"
  ]
  node [
    id 601
    label "pochodzenie"
  ]
  node [
    id 602
    label "poprzedza&#263;"
  ]
  node [
    id 603
    label "trawi&#263;"
  ]
  node [
    id 604
    label "rachuba_czasu"
  ]
  node [
    id 605
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 606
    label "czasoprzestrze&#324;"
  ]
  node [
    id 607
    label "laba"
  ]
  node [
    id 608
    label "zwi&#281;kszy&#263;"
  ]
  node [
    id 609
    label "increase"
  ]
  node [
    id 610
    label "wake_up"
  ]
  node [
    id 611
    label "pobudzi&#263;"
  ]
  node [
    id 612
    label "nominacja"
  ]
  node [
    id 613
    label "sprzeda&#380;"
  ]
  node [
    id 614
    label "zamiana"
  ]
  node [
    id 615
    label "graduacja"
  ]
  node [
    id 616
    label "&#347;wiadectwo"
  ]
  node [
    id 617
    label "gradation"
  ]
  node [
    id 618
    label "brief"
  ]
  node [
    id 619
    label "uzyska&#263;"
  ]
  node [
    id 620
    label "promotion"
  ]
  node [
    id 621
    label "promowa&#263;"
  ]
  node [
    id 622
    label "akcja"
  ]
  node [
    id 623
    label "wypromowa&#263;"
  ]
  node [
    id 624
    label "warcaby"
  ]
  node [
    id 625
    label "popularyzacja"
  ]
  node [
    id 626
    label "bran&#380;a"
  ]
  node [
    id 627
    label "informacja"
  ]
  node [
    id 628
    label "impreza"
  ]
  node [
    id 629
    label "decyzja"
  ]
  node [
    id 630
    label "okazja"
  ]
  node [
    id 631
    label "commencement"
  ]
  node [
    id 632
    label "udzieli&#263;"
  ]
  node [
    id 633
    label "szachy"
  ]
  node [
    id 634
    label "damka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 11
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 14
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 37
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 121
  ]
  edge [
    source 11
    target 101
  ]
  edge [
    source 11
    target 73
  ]
  edge [
    source 11
    target 122
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 23
  ]
  edge [
    source 12
    target 24
  ]
  edge [
    source 12
    target 123
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 124
  ]
  edge [
    source 13
    target 125
  ]
  edge [
    source 13
    target 126
  ]
  edge [
    source 13
    target 127
  ]
  edge [
    source 13
    target 128
  ]
  edge [
    source 13
    target 129
  ]
  edge [
    source 13
    target 130
  ]
  edge [
    source 13
    target 131
  ]
  edge [
    source 13
    target 132
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 52
  ]
  edge [
    source 14
    target 133
  ]
  edge [
    source 14
    target 134
  ]
  edge [
    source 14
    target 135
  ]
  edge [
    source 14
    target 136
  ]
  edge [
    source 14
    target 137
  ]
  edge [
    source 14
    target 138
  ]
  edge [
    source 14
    target 139
  ]
  edge [
    source 14
    target 140
  ]
  edge [
    source 14
    target 141
  ]
  edge [
    source 14
    target 142
  ]
  edge [
    source 14
    target 143
  ]
  edge [
    source 14
    target 144
  ]
  edge [
    source 14
    target 145
  ]
  edge [
    source 14
    target 146
  ]
  edge [
    source 14
    target 147
  ]
  edge [
    source 14
    target 148
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 53
  ]
  edge [
    source 15
    target 149
  ]
  edge [
    source 15
    target 150
  ]
  edge [
    source 15
    target 151
  ]
  edge [
    source 15
    target 152
  ]
  edge [
    source 15
    target 153
  ]
  edge [
    source 15
    target 154
  ]
  edge [
    source 15
    target 155
  ]
  edge [
    source 15
    target 156
  ]
  edge [
    source 15
    target 157
  ]
  edge [
    source 15
    target 158
  ]
  edge [
    source 15
    target 159
  ]
  edge [
    source 15
    target 160
  ]
  edge [
    source 15
    target 161
  ]
  edge [
    source 15
    target 162
  ]
  edge [
    source 15
    target 163
  ]
  edge [
    source 15
    target 164
  ]
  edge [
    source 15
    target 165
  ]
  edge [
    source 15
    target 166
  ]
  edge [
    source 15
    target 167
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 168
  ]
  edge [
    source 16
    target 169
  ]
  edge [
    source 16
    target 170
  ]
  edge [
    source 16
    target 171
  ]
  edge [
    source 16
    target 172
  ]
  edge [
    source 16
    target 173
  ]
  edge [
    source 16
    target 70
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 174
  ]
  edge [
    source 17
    target 175
  ]
  edge [
    source 17
    target 176
  ]
  edge [
    source 17
    target 24
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 177
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 178
  ]
  edge [
    source 21
    target 179
  ]
  edge [
    source 21
    target 73
  ]
  edge [
    source 21
    target 180
  ]
  edge [
    source 21
    target 181
  ]
  edge [
    source 21
    target 175
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 34
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 33
  ]
  edge [
    source 24
    target 70
  ]
  edge [
    source 24
    target 71
  ]
  edge [
    source 24
    target 182
  ]
  edge [
    source 24
    target 183
  ]
  edge [
    source 24
    target 184
  ]
  edge [
    source 24
    target 185
  ]
  edge [
    source 24
    target 186
  ]
  edge [
    source 24
    target 187
  ]
  edge [
    source 24
    target 188
  ]
  edge [
    source 24
    target 113
  ]
  edge [
    source 24
    target 189
  ]
  edge [
    source 24
    target 190
  ]
  edge [
    source 24
    target 191
  ]
  edge [
    source 24
    target 45
  ]
  edge [
    source 24
    target 50
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 37
  ]
  edge [
    source 26
    target 38
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 30
  ]
  edge [
    source 27
    target 192
  ]
  edge [
    source 27
    target 193
  ]
  edge [
    source 27
    target 194
  ]
  edge [
    source 27
    target 195
  ]
  edge [
    source 27
    target 196
  ]
  edge [
    source 27
    target 197
  ]
  edge [
    source 27
    target 198
  ]
  edge [
    source 27
    target 199
  ]
  edge [
    source 27
    target 200
  ]
  edge [
    source 27
    target 201
  ]
  edge [
    source 27
    target 202
  ]
  edge [
    source 27
    target 203
  ]
  edge [
    source 27
    target 204
  ]
  edge [
    source 27
    target 205
  ]
  edge [
    source 27
    target 206
  ]
  edge [
    source 27
    target 207
  ]
  edge [
    source 27
    target 208
  ]
  edge [
    source 27
    target 209
  ]
  edge [
    source 27
    target 210
  ]
  edge [
    source 27
    target 211
  ]
  edge [
    source 27
    target 212
  ]
  edge [
    source 27
    target 213
  ]
  edge [
    source 27
    target 214
  ]
  edge [
    source 27
    target 215
  ]
  edge [
    source 27
    target 216
  ]
  edge [
    source 27
    target 217
  ]
  edge [
    source 27
    target 218
  ]
  edge [
    source 27
    target 219
  ]
  edge [
    source 27
    target 220
  ]
  edge [
    source 27
    target 221
  ]
  edge [
    source 27
    target 222
  ]
  edge [
    source 27
    target 159
  ]
  edge [
    source 27
    target 223
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 224
  ]
  edge [
    source 30
    target 225
  ]
  edge [
    source 30
    target 43
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 226
  ]
  edge [
    source 31
    target 227
  ]
  edge [
    source 31
    target 228
  ]
  edge [
    source 31
    target 229
  ]
  edge [
    source 31
    target 203
  ]
  edge [
    source 31
    target 230
  ]
  edge [
    source 31
    target 231
  ]
  edge [
    source 31
    target 232
  ]
  edge [
    source 31
    target 233
  ]
  edge [
    source 31
    target 234
  ]
  edge [
    source 31
    target 235
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 33
    target 44
  ]
  edge [
    source 33
    target 45
  ]
  edge [
    source 33
    target 236
  ]
  edge [
    source 33
    target 237
  ]
  edge [
    source 33
    target 238
  ]
  edge [
    source 33
    target 239
  ]
  edge [
    source 33
    target 240
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 241
  ]
  edge [
    source 36
    target 242
  ]
  edge [
    source 37
    target 243
  ]
  edge [
    source 37
    target 244
  ]
  edge [
    source 37
    target 245
  ]
  edge [
    source 37
    target 246
  ]
  edge [
    source 37
    target 247
  ]
  edge [
    source 37
    target 248
  ]
  edge [
    source 37
    target 249
  ]
  edge [
    source 37
    target 250
  ]
  edge [
    source 37
    target 251
  ]
  edge [
    source 37
    target 252
  ]
  edge [
    source 37
    target 253
  ]
  edge [
    source 37
    target 254
  ]
  edge [
    source 37
    target 255
  ]
  edge [
    source 37
    target 256
  ]
  edge [
    source 37
    target 257
  ]
  edge [
    source 37
    target 82
  ]
  edge [
    source 37
    target 258
  ]
  edge [
    source 37
    target 259
  ]
  edge [
    source 37
    target 260
  ]
  edge [
    source 37
    target 261
  ]
  edge [
    source 37
    target 262
  ]
  edge [
    source 37
    target 263
  ]
  edge [
    source 37
    target 39
  ]
  edge [
    source 37
    target 264
  ]
  edge [
    source 37
    target 265
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 266
  ]
  edge [
    source 39
    target 267
  ]
  edge [
    source 39
    target 268
  ]
  edge [
    source 39
    target 269
  ]
  edge [
    source 39
    target 270
  ]
  edge [
    source 39
    target 271
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 272
  ]
  edge [
    source 40
    target 273
  ]
  edge [
    source 40
    target 274
  ]
  edge [
    source 40
    target 275
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 276
  ]
  edge [
    source 41
    target 277
  ]
  edge [
    source 41
    target 278
  ]
  edge [
    source 41
    target 279
  ]
  edge [
    source 41
    target 280
  ]
  edge [
    source 41
    target 281
  ]
  edge [
    source 41
    target 282
  ]
  edge [
    source 41
    target 283
  ]
  edge [
    source 41
    target 284
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 57
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 285
  ]
  edge [
    source 43
    target 286
  ]
  edge [
    source 43
    target 287
  ]
  edge [
    source 44
    target 288
  ]
  edge [
    source 44
    target 289
  ]
  edge [
    source 44
    target 290
  ]
  edge [
    source 44
    target 291
  ]
  edge [
    source 44
    target 292
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 293
  ]
  edge [
    source 45
    target 294
  ]
  edge [
    source 45
    target 295
  ]
  edge [
    source 45
    target 296
  ]
  edge [
    source 45
    target 297
  ]
  edge [
    source 45
    target 298
  ]
  edge [
    source 45
    target 299
  ]
  edge [
    source 45
    target 300
  ]
  edge [
    source 45
    target 301
  ]
  edge [
    source 45
    target 302
  ]
  edge [
    source 45
    target 303
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 304
  ]
  edge [
    source 46
    target 111
  ]
  edge [
    source 46
    target 305
  ]
  edge [
    source 46
    target 131
  ]
  edge [
    source 46
    target 306
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 307
  ]
  edge [
    source 47
    target 308
  ]
  edge [
    source 47
    target 309
  ]
  edge [
    source 47
    target 310
  ]
  edge [
    source 47
    target 311
  ]
  edge [
    source 47
    target 312
  ]
  edge [
    source 47
    target 313
  ]
  edge [
    source 47
    target 314
  ]
  edge [
    source 47
    target 315
  ]
  edge [
    source 47
    target 316
  ]
  edge [
    source 47
    target 317
  ]
  edge [
    source 47
    target 318
  ]
  edge [
    source 47
    target 319
  ]
  edge [
    source 47
    target 320
  ]
  edge [
    source 47
    target 321
  ]
  edge [
    source 47
    target 322
  ]
  edge [
    source 47
    target 323
  ]
  edge [
    source 47
    target 324
  ]
  edge [
    source 47
    target 325
  ]
  edge [
    source 47
    target 326
  ]
  edge [
    source 47
    target 327
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 116
  ]
  edge [
    source 48
    target 328
  ]
  edge [
    source 48
    target 329
  ]
  edge [
    source 48
    target 330
  ]
  edge [
    source 48
    target 331
  ]
  edge [
    source 48
    target 332
  ]
  edge [
    source 48
    target 333
  ]
  edge [
    source 48
    target 334
  ]
  edge [
    source 48
    target 335
  ]
  edge [
    source 48
    target 336
  ]
  edge [
    source 48
    target 337
  ]
  edge [
    source 48
    target 338
  ]
  edge [
    source 48
    target 339
  ]
  edge [
    source 48
    target 340
  ]
  edge [
    source 48
    target 341
  ]
  edge [
    source 48
    target 342
  ]
  edge [
    source 48
    target 343
  ]
  edge [
    source 48
    target 344
  ]
  edge [
    source 48
    target 345
  ]
  edge [
    source 48
    target 346
  ]
  edge [
    source 48
    target 347
  ]
  edge [
    source 48
    target 348
  ]
  edge [
    source 48
    target 349
  ]
  edge [
    source 48
    target 350
  ]
  edge [
    source 48
    target 351
  ]
  edge [
    source 48
    target 352
  ]
  edge [
    source 48
    target 353
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 354
  ]
  edge [
    source 49
    target 355
  ]
  edge [
    source 49
    target 356
  ]
  edge [
    source 49
    target 357
  ]
  edge [
    source 49
    target 358
  ]
  edge [
    source 49
    target 359
  ]
  edge [
    source 49
    target 360
  ]
  edge [
    source 49
    target 361
  ]
  edge [
    source 49
    target 362
  ]
  edge [
    source 49
    target 363
  ]
  edge [
    source 49
    target 364
  ]
  edge [
    source 49
    target 365
  ]
  edge [
    source 49
    target 366
  ]
  edge [
    source 49
    target 367
  ]
  edge [
    source 49
    target 368
  ]
  edge [
    source 49
    target 369
  ]
  edge [
    source 49
    target 370
  ]
  edge [
    source 49
    target 371
  ]
  edge [
    source 49
    target 372
  ]
  edge [
    source 49
    target 373
  ]
  edge [
    source 49
    target 374
  ]
  edge [
    source 49
    target 375
  ]
  edge [
    source 49
    target 270
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 376
  ]
  edge [
    source 50
    target 377
  ]
  edge [
    source 50
    target 378
  ]
  edge [
    source 50
    target 379
  ]
  edge [
    source 50
    target 380
  ]
  edge [
    source 50
    target 381
  ]
  edge [
    source 50
    target 382
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 383
  ]
  edge [
    source 51
    target 309
  ]
  edge [
    source 51
    target 384
  ]
  edge [
    source 51
    target 385
  ]
  edge [
    source 51
    target 386
  ]
  edge [
    source 51
    target 387
  ]
  edge [
    source 51
    target 388
  ]
  edge [
    source 51
    target 389
  ]
  edge [
    source 51
    target 390
  ]
  edge [
    source 51
    target 391
  ]
  edge [
    source 51
    target 392
  ]
  edge [
    source 51
    target 393
  ]
  edge [
    source 51
    target 394
  ]
  edge [
    source 51
    target 131
  ]
  edge [
    source 51
    target 395
  ]
  edge [
    source 51
    target 396
  ]
  edge [
    source 51
    target 397
  ]
  edge [
    source 51
    target 398
  ]
  edge [
    source 51
    target 399
  ]
  edge [
    source 51
    target 278
  ]
  edge [
    source 51
    target 400
  ]
  edge [
    source 51
    target 401
  ]
  edge [
    source 51
    target 402
  ]
  edge [
    source 51
    target 403
  ]
  edge [
    source 51
    target 404
  ]
  edge [
    source 51
    target 405
  ]
  edge [
    source 51
    target 406
  ]
  edge [
    source 51
    target 407
  ]
  edge [
    source 51
    target 408
  ]
  edge [
    source 51
    target 409
  ]
  edge [
    source 51
    target 410
  ]
  edge [
    source 51
    target 411
  ]
  edge [
    source 52
    target 412
  ]
  edge [
    source 52
    target 413
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 414
  ]
  edge [
    source 53
    target 415
  ]
  edge [
    source 53
    target 416
  ]
  edge [
    source 53
    target 417
  ]
  edge [
    source 53
    target 418
  ]
  edge [
    source 53
    target 419
  ]
  edge [
    source 53
    target 420
  ]
  edge [
    source 53
    target 421
  ]
  edge [
    source 53
    target 422
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 423
  ]
  edge [
    source 54
    target 424
  ]
  edge [
    source 54
    target 73
  ]
  edge [
    source 54
    target 425
  ]
  edge [
    source 54
    target 426
  ]
  edge [
    source 54
    target 427
  ]
  edge [
    source 54
    target 428
  ]
  edge [
    source 54
    target 429
  ]
  edge [
    source 54
    target 430
  ]
  edge [
    source 54
    target 431
  ]
  edge [
    source 54
    target 432
  ]
  edge [
    source 54
    target 433
  ]
  edge [
    source 54
    target 434
  ]
  edge [
    source 54
    target 435
  ]
  edge [
    source 54
    target 436
  ]
  edge [
    source 54
    target 437
  ]
  edge [
    source 54
    target 438
  ]
  edge [
    source 54
    target 439
  ]
  edge [
    source 54
    target 440
  ]
  edge [
    source 54
    target 441
  ]
  edge [
    source 54
    target 442
  ]
  edge [
    source 54
    target 443
  ]
  edge [
    source 54
    target 444
  ]
  edge [
    source 54
    target 445
  ]
  edge [
    source 54
    target 446
  ]
  edge [
    source 54
    target 447
  ]
  edge [
    source 54
    target 448
  ]
  edge [
    source 54
    target 449
  ]
  edge [
    source 54
    target 450
  ]
  edge [
    source 54
    target 451
  ]
  edge [
    source 54
    target 452
  ]
  edge [
    source 54
    target 453
  ]
  edge [
    source 54
    target 454
  ]
  edge [
    source 54
    target 455
  ]
  edge [
    source 54
    target 456
  ]
  edge [
    source 54
    target 457
  ]
  edge [
    source 54
    target 458
  ]
  edge [
    source 54
    target 459
  ]
  edge [
    source 54
    target 460
  ]
  edge [
    source 54
    target 461
  ]
  edge [
    source 54
    target 462
  ]
  edge [
    source 54
    target 463
  ]
  edge [
    source 54
    target 464
  ]
  edge [
    source 54
    target 465
  ]
  edge [
    source 54
    target 466
  ]
  edge [
    source 54
    target 467
  ]
  edge [
    source 54
    target 468
  ]
  edge [
    source 54
    target 469
  ]
  edge [
    source 54
    target 470
  ]
  edge [
    source 54
    target 471
  ]
  edge [
    source 54
    target 472
  ]
  edge [
    source 54
    target 473
  ]
  edge [
    source 54
    target 474
  ]
  edge [
    source 54
    target 475
  ]
  edge [
    source 54
    target 111
  ]
  edge [
    source 54
    target 476
  ]
  edge [
    source 54
    target 477
  ]
  edge [
    source 54
    target 478
  ]
  edge [
    source 54
    target 479
  ]
  edge [
    source 54
    target 480
  ]
  edge [
    source 54
    target 481
  ]
  edge [
    source 54
    target 482
  ]
  edge [
    source 54
    target 483
  ]
  edge [
    source 54
    target 484
  ]
  edge [
    source 54
    target 485
  ]
  edge [
    source 54
    target 486
  ]
  edge [
    source 54
    target 487
  ]
  edge [
    source 54
    target 488
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 110
  ]
  edge [
    source 56
    target 163
  ]
  edge [
    source 56
    target 489
  ]
  edge [
    source 56
    target 490
  ]
  edge [
    source 56
    target 491
  ]
  edge [
    source 56
    target 492
  ]
  edge [
    source 56
    target 349
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 493
  ]
  edge [
    source 57
    target 148
  ]
  edge [
    source 57
    target 494
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 495
  ]
  edge [
    source 58
    target 110
  ]
  edge [
    source 58
    target 496
  ]
  edge [
    source 58
    target 73
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 497
  ]
  edge [
    source 59
    target 498
  ]
  edge [
    source 59
    target 74
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 499
  ]
  edge [
    source 60
    target 500
  ]
  edge [
    source 60
    target 501
  ]
  edge [
    source 60
    target 502
  ]
  edge [
    source 60
    target 503
  ]
  edge [
    source 60
    target 278
  ]
  edge [
    source 60
    target 504
  ]
  edge [
    source 60
    target 505
  ]
  edge [
    source 60
    target 506
  ]
  edge [
    source 60
    target 131
  ]
  edge [
    source 60
    target 507
  ]
  edge [
    source 60
    target 508
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 509
  ]
  edge [
    source 61
    target 510
  ]
  edge [
    source 61
    target 511
  ]
  edge [
    source 61
    target 512
  ]
  edge [
    source 61
    target 513
  ]
  edge [
    source 61
    target 514
  ]
  edge [
    source 61
    target 515
  ]
  edge [
    source 61
    target 516
  ]
  edge [
    source 61
    target 260
  ]
  edge [
    source 61
    target 517
  ]
  edge [
    source 61
    target 518
  ]
  edge [
    source 61
    target 519
  ]
  edge [
    source 61
    target 520
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 521
  ]
  edge [
    source 62
    target 522
  ]
  edge [
    source 62
    target 523
  ]
  edge [
    source 62
    target 524
  ]
  edge [
    source 62
    target 525
  ]
  edge [
    source 62
    target 526
  ]
  edge [
    source 62
    target 527
  ]
  edge [
    source 62
    target 528
  ]
  edge [
    source 62
    target 529
  ]
  edge [
    source 62
    target 530
  ]
  edge [
    source 62
    target 531
  ]
  edge [
    source 62
    target 532
  ]
  edge [
    source 62
    target 533
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 534
  ]
  edge [
    source 63
    target 535
  ]
  edge [
    source 63
    target 536
  ]
  edge [
    source 63
    target 537
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 538
  ]
  edge [
    source 64
    target 539
  ]
  edge [
    source 64
    target 504
  ]
  edge [
    source 64
    target 540
  ]
  edge [
    source 64
    target 541
  ]
  edge [
    source 64
    target 507
  ]
  edge [
    source 64
    target 542
  ]
  edge [
    source 64
    target 543
  ]
  edge [
    source 64
    target 544
  ]
  edge [
    source 64
    target 545
  ]
  edge [
    source 64
    target 546
  ]
  edge [
    source 64
    target 252
  ]
  edge [
    source 64
    target 547
  ]
  edge [
    source 64
    target 548
  ]
  edge [
    source 64
    target 549
  ]
  edge [
    source 64
    target 376
  ]
  edge [
    source 64
    target 550
  ]
  edge [
    source 64
    target 551
  ]
  edge [
    source 64
    target 552
  ]
  edge [
    source 64
    target 553
  ]
  edge [
    source 64
    target 554
  ]
  edge [
    source 64
    target 555
  ]
  edge [
    source 64
    target 556
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 557
  ]
  edge [
    source 65
    target 558
  ]
  edge [
    source 65
    target 559
  ]
  edge [
    source 65
    target 560
  ]
  edge [
    source 65
    target 561
  ]
  edge [
    source 65
    target 562
  ]
  edge [
    source 65
    target 563
  ]
  edge [
    source 65
    target 564
  ]
  edge [
    source 65
    target 565
  ]
  edge [
    source 65
    target 566
  ]
  edge [
    source 65
    target 567
  ]
  edge [
    source 65
    target 568
  ]
  edge [
    source 65
    target 569
  ]
  edge [
    source 65
    target 570
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 69
    target 571
  ]
  edge [
    source 69
    target 572
  ]
  edge [
    source 69
    target 573
  ]
  edge [
    source 69
    target 574
  ]
  edge [
    source 69
    target 575
  ]
  edge [
    source 70
    target 576
  ]
  edge [
    source 70
    target 577
  ]
  edge [
    source 70
    target 578
  ]
  edge [
    source 72
    target 73
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 73
    target 579
  ]
  edge [
    source 73
    target 580
  ]
  edge [
    source 73
    target 581
  ]
  edge [
    source 73
    target 425
  ]
  edge [
    source 73
    target 582
  ]
  edge [
    source 73
    target 583
  ]
  edge [
    source 73
    target 584
  ]
  edge [
    source 73
    target 585
  ]
  edge [
    source 73
    target 431
  ]
  edge [
    source 73
    target 586
  ]
  edge [
    source 73
    target 587
  ]
  edge [
    source 73
    target 440
  ]
  edge [
    source 73
    target 588
  ]
  edge [
    source 73
    target 589
  ]
  edge [
    source 73
    target 590
  ]
  edge [
    source 73
    target 591
  ]
  edge [
    source 73
    target 592
  ]
  edge [
    source 73
    target 454
  ]
  edge [
    source 73
    target 593
  ]
  edge [
    source 73
    target 459
  ]
  edge [
    source 73
    target 277
  ]
  edge [
    source 73
    target 594
  ]
  edge [
    source 73
    target 461
  ]
  edge [
    source 73
    target 595
  ]
  edge [
    source 73
    target 596
  ]
  edge [
    source 73
    target 597
  ]
  edge [
    source 73
    target 598
  ]
  edge [
    source 73
    target 599
  ]
  edge [
    source 73
    target 600
  ]
  edge [
    source 73
    target 601
  ]
  edge [
    source 73
    target 602
  ]
  edge [
    source 73
    target 603
  ]
  edge [
    source 73
    target 478
  ]
  edge [
    source 73
    target 604
  ]
  edge [
    source 73
    target 605
  ]
  edge [
    source 73
    target 606
  ]
  edge [
    source 73
    target 607
  ]
  edge [
    source 74
    target 75
  ]
  edge [
    source 74
    target 608
  ]
  edge [
    source 74
    target 609
  ]
  edge [
    source 74
    target 610
  ]
  edge [
    source 74
    target 611
  ]
  edge [
    source 75
    target 612
  ]
  edge [
    source 75
    target 613
  ]
  edge [
    source 75
    target 614
  ]
  edge [
    source 75
    target 615
  ]
  edge [
    source 75
    target 616
  ]
  edge [
    source 75
    target 617
  ]
  edge [
    source 75
    target 618
  ]
  edge [
    source 75
    target 619
  ]
  edge [
    source 75
    target 620
  ]
  edge [
    source 75
    target 621
  ]
  edge [
    source 75
    target 353
  ]
  edge [
    source 75
    target 622
  ]
  edge [
    source 75
    target 623
  ]
  edge [
    source 75
    target 624
  ]
  edge [
    source 75
    target 625
  ]
  edge [
    source 75
    target 626
  ]
  edge [
    source 75
    target 627
  ]
  edge [
    source 75
    target 628
  ]
  edge [
    source 75
    target 629
  ]
  edge [
    source 75
    target 630
  ]
  edge [
    source 75
    target 631
  ]
  edge [
    source 75
    target 632
  ]
  edge [
    source 75
    target 633
  ]
  edge [
    source 75
    target 634
  ]
]
