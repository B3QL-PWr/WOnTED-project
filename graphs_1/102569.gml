graph [
  maxDegree 39
  minDegree 1
  meanDegree 2.04
  density 0.010251256281407035
  graphCliqueNumber 2
  node [
    id 0
    label "by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "wsp&#243;lnota"
    origin "text"
  ]
  node [
    id 2
    label "samorz&#261;dowy"
    origin "text"
  ]
  node [
    id 3
    label "osoba"
    origin "text"
  ]
  node [
    id 4
    label "zamieszkiwa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "terenia"
    origin "text"
  ]
  node [
    id 6
    label "zielony"
    origin "text"
  ]
  node [
    id 7
    label "g&#243;ra"
    origin "text"
  ]
  node [
    id 8
    label "posiadaj&#261;cy"
    origin "text"
  ]
  node [
    id 9
    label "osobowo&#347;&#263;"
    origin "text"
  ]
  node [
    id 10
    label "prawny"
    origin "text"
  ]
  node [
    id 11
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 12
    label "swoje"
    origin "text"
  ]
  node [
    id 13
    label "zadanie"
    origin "text"
  ]
  node [
    id 14
    label "publiczny"
    origin "text"
  ]
  node [
    id 15
    label "wykonywa&#263;"
    origin "text"
  ]
  node [
    id 16
    label "imienie"
    origin "text"
  ]
  node [
    id 17
    label "w&#322;asne"
    origin "text"
  ]
  node [
    id 18
    label "w&#322;asny"
    origin "text"
  ]
  node [
    id 19
    label "odpowiedzialno&#347;&#263;"
    origin "text"
  ]
  node [
    id 20
    label "mieszkaniec"
    origin "text"
  ]
  node [
    id 21
    label "si&#281;ga&#263;"
  ]
  node [
    id 22
    label "trwa&#263;"
  ]
  node [
    id 23
    label "obecno&#347;&#263;"
  ]
  node [
    id 24
    label "stan"
  ]
  node [
    id 25
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 26
    label "stand"
  ]
  node [
    id 27
    label "mie&#263;_miejsce"
  ]
  node [
    id 28
    label "uczestniczy&#263;"
  ]
  node [
    id 29
    label "chodzi&#263;"
  ]
  node [
    id 30
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 31
    label "equal"
  ]
  node [
    id 32
    label "Skandynawia"
  ]
  node [
    id 33
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 34
    label "partnership"
  ]
  node [
    id 35
    label "zwi&#261;zek"
  ]
  node [
    id 36
    label "zwi&#261;za&#263;"
  ]
  node [
    id 37
    label "Walencja"
  ]
  node [
    id 38
    label "society"
  ]
  node [
    id 39
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 40
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 41
    label "bratnia_dusza"
  ]
  node [
    id 42
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 43
    label "marriage"
  ]
  node [
    id 44
    label "zwi&#261;zanie"
  ]
  node [
    id 45
    label "Ba&#322;kany"
  ]
  node [
    id 46
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 47
    label "wi&#261;zanie"
  ]
  node [
    id 48
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 49
    label "podobie&#324;stwo"
  ]
  node [
    id 50
    label "Zgredek"
  ]
  node [
    id 51
    label "kategoria_gramatyczna"
  ]
  node [
    id 52
    label "Casanova"
  ]
  node [
    id 53
    label "Don_Juan"
  ]
  node [
    id 54
    label "Gargantua"
  ]
  node [
    id 55
    label "Faust"
  ]
  node [
    id 56
    label "profanum"
  ]
  node [
    id 57
    label "Chocho&#322;"
  ]
  node [
    id 58
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 59
    label "koniugacja"
  ]
  node [
    id 60
    label "Winnetou"
  ]
  node [
    id 61
    label "Dwukwiat"
  ]
  node [
    id 62
    label "homo_sapiens"
  ]
  node [
    id 63
    label "Edyp"
  ]
  node [
    id 64
    label "Herkules_Poirot"
  ]
  node [
    id 65
    label "ludzko&#347;&#263;"
  ]
  node [
    id 66
    label "mikrokosmos"
  ]
  node [
    id 67
    label "person"
  ]
  node [
    id 68
    label "Sherlock_Holmes"
  ]
  node [
    id 69
    label "portrecista"
  ]
  node [
    id 70
    label "Szwejk"
  ]
  node [
    id 71
    label "Hamlet"
  ]
  node [
    id 72
    label "duch"
  ]
  node [
    id 73
    label "g&#322;owa"
  ]
  node [
    id 74
    label "oddzia&#322;ywanie"
  ]
  node [
    id 75
    label "Quasimodo"
  ]
  node [
    id 76
    label "Dulcynea"
  ]
  node [
    id 77
    label "Don_Kiszot"
  ]
  node [
    id 78
    label "Wallenrod"
  ]
  node [
    id 79
    label "Plastu&#347;"
  ]
  node [
    id 80
    label "Harry_Potter"
  ]
  node [
    id 81
    label "figura"
  ]
  node [
    id 82
    label "parali&#380;owa&#263;"
  ]
  node [
    id 83
    label "istota"
  ]
  node [
    id 84
    label "Werter"
  ]
  node [
    id 85
    label "antropochoria"
  ]
  node [
    id 86
    label "posta&#263;"
  ]
  node [
    id 87
    label "fall"
  ]
  node [
    id 88
    label "zajmowa&#263;"
  ]
  node [
    id 89
    label "sta&#263;"
  ]
  node [
    id 90
    label "osiedla&#263;_si&#281;"
  ]
  node [
    id 91
    label "Palau"
  ]
  node [
    id 92
    label "zwolennik"
  ]
  node [
    id 93
    label "zieloni"
  ]
  node [
    id 94
    label "zielenienie"
  ]
  node [
    id 95
    label "Zimbabwe"
  ]
  node [
    id 96
    label "zazielenianie"
  ]
  node [
    id 97
    label "Wyspy_Marshalla"
  ]
  node [
    id 98
    label "nie&#347;wie&#380;y"
  ]
  node [
    id 99
    label "niedojrza&#322;y"
  ]
  node [
    id 100
    label "zazielenienie"
  ]
  node [
    id 101
    label "ch&#322;odny"
  ]
  node [
    id 102
    label "pokryty"
  ]
  node [
    id 103
    label "blady"
  ]
  node [
    id 104
    label "Mariany_P&#243;&#322;nocne"
  ]
  node [
    id 105
    label "naturalny"
  ]
  node [
    id 106
    label "&#380;ywy"
  ]
  node [
    id 107
    label "Timor_Wschodni"
  ]
  node [
    id 108
    label "Portoryko"
  ]
  node [
    id 109
    label "zielono"
  ]
  node [
    id 110
    label "Sint_Eustatius"
  ]
  node [
    id 111
    label "Salwador"
  ]
  node [
    id 112
    label "Saba"
  ]
  node [
    id 113
    label "Bonaire"
  ]
  node [
    id 114
    label "dzia&#322;acz"
  ]
  node [
    id 115
    label "&#347;wie&#380;y"
  ]
  node [
    id 116
    label "Mikronezja"
  ]
  node [
    id 117
    label "USA"
  ]
  node [
    id 118
    label "Panama"
  ]
  node [
    id 119
    label "zzielenienie"
  ]
  node [
    id 120
    label "dolar"
  ]
  node [
    id 121
    label "socjalista"
  ]
  node [
    id 122
    label "polityk"
  ]
  node [
    id 123
    label "majny"
  ]
  node [
    id 124
    label "Ekwador"
  ]
  node [
    id 125
    label "przedmiot"
  ]
  node [
    id 126
    label "grupa"
  ]
  node [
    id 127
    label "element"
  ]
  node [
    id 128
    label "przele&#378;&#263;"
  ]
  node [
    id 129
    label "pi&#281;tro"
  ]
  node [
    id 130
    label "karczek"
  ]
  node [
    id 131
    label "wysoki"
  ]
  node [
    id 132
    label "rami&#261;czko"
  ]
  node [
    id 133
    label "Ropa"
  ]
  node [
    id 134
    label "Jaworze"
  ]
  node [
    id 135
    label "Synaj"
  ]
  node [
    id 136
    label "wzniesienie"
  ]
  node [
    id 137
    label "przelezienie"
  ]
  node [
    id 138
    label "&#347;piew"
  ]
  node [
    id 139
    label "kupa"
  ]
  node [
    id 140
    label "kierunek"
  ]
  node [
    id 141
    label "Ma&#322;a_Rawka"
  ]
  node [
    id 142
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 143
    label "d&#378;wi&#281;k"
  ]
  node [
    id 144
    label "Kreml"
  ]
  node [
    id 145
    label "cz&#322;owiek"
  ]
  node [
    id 146
    label "byt"
  ]
  node [
    id 147
    label "wn&#281;trze"
  ]
  node [
    id 148
    label "psychika"
  ]
  node [
    id 149
    label "podmiot"
  ]
  node [
    id 150
    label "cecha"
  ]
  node [
    id 151
    label "wyj&#261;tkowy"
  ]
  node [
    id 152
    label "self"
  ]
  node [
    id 153
    label "superego"
  ]
  node [
    id 154
    label "charakter"
  ]
  node [
    id 155
    label "mentalno&#347;&#263;"
  ]
  node [
    id 156
    label "prawniczo"
  ]
  node [
    id 157
    label "prawnie"
  ]
  node [
    id 158
    label "konstytucyjnoprawny"
  ]
  node [
    id 159
    label "legalny"
  ]
  node [
    id 160
    label "jurydyczny"
  ]
  node [
    id 161
    label "yield"
  ]
  node [
    id 162
    label "czynno&#347;&#263;"
  ]
  node [
    id 163
    label "problem"
  ]
  node [
    id 164
    label "przepisanie"
  ]
  node [
    id 165
    label "przepisa&#263;"
  ]
  node [
    id 166
    label "za&#322;o&#380;enie"
  ]
  node [
    id 167
    label "work"
  ]
  node [
    id 168
    label "nakarmienie"
  ]
  node [
    id 169
    label "duty"
  ]
  node [
    id 170
    label "zbi&#243;r"
  ]
  node [
    id 171
    label "powierzanie"
  ]
  node [
    id 172
    label "zaszkodzenie"
  ]
  node [
    id 173
    label "d&#378;wigni&#281;cie"
  ]
  node [
    id 174
    label "zaj&#281;cie"
  ]
  node [
    id 175
    label "zobowi&#261;zanie"
  ]
  node [
    id 176
    label "jawny"
  ]
  node [
    id 177
    label "upublicznienie"
  ]
  node [
    id 178
    label "upublicznianie"
  ]
  node [
    id 179
    label "publicznie"
  ]
  node [
    id 180
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 181
    label "robi&#263;"
  ]
  node [
    id 182
    label "muzyka"
  ]
  node [
    id 183
    label "rola"
  ]
  node [
    id 184
    label "create"
  ]
  node [
    id 185
    label "wytwarza&#263;"
  ]
  node [
    id 186
    label "praca"
  ]
  node [
    id 187
    label "swoisty"
  ]
  node [
    id 188
    label "czyj&#347;"
  ]
  node [
    id 189
    label "osobny"
  ]
  node [
    id 190
    label "zwi&#261;zany"
  ]
  node [
    id 191
    label "samodzielny"
  ]
  node [
    id 192
    label "wstyd"
  ]
  node [
    id 193
    label "konsekwencja"
  ]
  node [
    id 194
    label "guilt"
  ]
  node [
    id 195
    label "liability"
  ]
  node [
    id 196
    label "obarczy&#263;"
  ]
  node [
    id 197
    label "obowi&#261;zek"
  ]
  node [
    id 198
    label "ludno&#347;&#263;"
  ]
  node [
    id 199
    label "zwierz&#281;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 125
  ]
  edge [
    source 7
    target 126
  ]
  edge [
    source 7
    target 127
  ]
  edge [
    source 7
    target 128
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 7
    target 130
  ]
  edge [
    source 7
    target 131
  ]
  edge [
    source 7
    target 132
  ]
  edge [
    source 7
    target 133
  ]
  edge [
    source 7
    target 134
  ]
  edge [
    source 7
    target 135
  ]
  edge [
    source 7
    target 136
  ]
  edge [
    source 7
    target 137
  ]
  edge [
    source 7
    target 138
  ]
  edge [
    source 7
    target 139
  ]
  edge [
    source 7
    target 140
  ]
  edge [
    source 7
    target 141
  ]
  edge [
    source 7
    target 142
  ]
  edge [
    source 7
    target 143
  ]
  edge [
    source 7
    target 144
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 145
  ]
  edge [
    source 9
    target 146
  ]
  edge [
    source 9
    target 147
  ]
  edge [
    source 9
    target 148
  ]
  edge [
    source 9
    target 149
  ]
  edge [
    source 9
    target 150
  ]
  edge [
    source 9
    target 151
  ]
  edge [
    source 9
    target 152
  ]
  edge [
    source 9
    target 153
  ]
  edge [
    source 9
    target 154
  ]
  edge [
    source 9
    target 155
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 156
  ]
  edge [
    source 10
    target 157
  ]
  edge [
    source 10
    target 158
  ]
  edge [
    source 10
    target 159
  ]
  edge [
    source 10
    target 160
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 161
  ]
  edge [
    source 13
    target 162
  ]
  edge [
    source 13
    target 163
  ]
  edge [
    source 13
    target 164
  ]
  edge [
    source 13
    target 165
  ]
  edge [
    source 13
    target 166
  ]
  edge [
    source 13
    target 167
  ]
  edge [
    source 13
    target 168
  ]
  edge [
    source 13
    target 169
  ]
  edge [
    source 13
    target 170
  ]
  edge [
    source 13
    target 171
  ]
  edge [
    source 13
    target 172
  ]
  edge [
    source 13
    target 173
  ]
  edge [
    source 13
    target 174
  ]
  edge [
    source 13
    target 175
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 176
  ]
  edge [
    source 14
    target 177
  ]
  edge [
    source 14
    target 178
  ]
  edge [
    source 14
    target 179
  ]
  edge [
    source 14
    target 19
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 180
  ]
  edge [
    source 15
    target 167
  ]
  edge [
    source 15
    target 181
  ]
  edge [
    source 15
    target 182
  ]
  edge [
    source 15
    target 183
  ]
  edge [
    source 15
    target 184
  ]
  edge [
    source 15
    target 185
  ]
  edge [
    source 15
    target 186
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 19
  ]
  edge [
    source 16
    target 20
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 187
  ]
  edge [
    source 18
    target 188
  ]
  edge [
    source 18
    target 189
  ]
  edge [
    source 18
    target 190
  ]
  edge [
    source 18
    target 191
  ]
  edge [
    source 19
    target 192
  ]
  edge [
    source 19
    target 193
  ]
  edge [
    source 19
    target 150
  ]
  edge [
    source 19
    target 194
  ]
  edge [
    source 19
    target 195
  ]
  edge [
    source 19
    target 196
  ]
  edge [
    source 19
    target 197
  ]
  edge [
    source 20
    target 145
  ]
  edge [
    source 20
    target 198
  ]
  edge [
    source 20
    target 199
  ]
]
