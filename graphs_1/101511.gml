graph [
  maxDegree 155
  minDegree 1
  meanDegree 2.0089285714285716
  density 0.009008648302370276
  graphCliqueNumber 3
  node [
    id 0
    label "zadanie"
    origin "text"
  ]
  node [
    id 1
    label "powierzchnia"
    origin "text"
  ]
  node [
    id 2
    label "polska"
    origin "text"
  ]
  node [
    id 3
    label "wynosi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "ziemia"
    origin "text"
  ]
  node [
    id 5
    label "ile"
    origin "text"
  ]
  node [
    id 6
    label "raz"
    origin "text"
  ]
  node [
    id 7
    label "by&#263;"
    origin "text"
  ]
  node [
    id 8
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 9
    label "yield"
  ]
  node [
    id 10
    label "czynno&#347;&#263;"
  ]
  node [
    id 11
    label "problem"
  ]
  node [
    id 12
    label "przepisanie"
  ]
  node [
    id 13
    label "przepisa&#263;"
  ]
  node [
    id 14
    label "za&#322;o&#380;enie"
  ]
  node [
    id 15
    label "work"
  ]
  node [
    id 16
    label "nakarmienie"
  ]
  node [
    id 17
    label "duty"
  ]
  node [
    id 18
    label "zbi&#243;r"
  ]
  node [
    id 19
    label "powierzanie"
  ]
  node [
    id 20
    label "zaszkodzenie"
  ]
  node [
    id 21
    label "d&#378;wigni&#281;cie"
  ]
  node [
    id 22
    label "zaj&#281;cie"
  ]
  node [
    id 23
    label "zobowi&#261;zanie"
  ]
  node [
    id 24
    label "capacity"
  ]
  node [
    id 25
    label "obszar"
  ]
  node [
    id 26
    label "zwierciad&#322;o"
  ]
  node [
    id 27
    label "rozmiar"
  ]
  node [
    id 28
    label "liofilowo&#347;&#263;"
  ]
  node [
    id 29
    label "poj&#281;cie"
  ]
  node [
    id 30
    label "plane"
  ]
  node [
    id 31
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 32
    label "odsuwa&#263;"
  ]
  node [
    id 33
    label "zanosi&#263;"
  ]
  node [
    id 34
    label "nasi&#261;ka&#263;"
  ]
  node [
    id 35
    label "otrzymywa&#263;"
  ]
  node [
    id 36
    label "rozpowszechnia&#263;"
  ]
  node [
    id 37
    label "ujawnia&#263;"
  ]
  node [
    id 38
    label "kra&#347;&#263;"
  ]
  node [
    id 39
    label "kwota"
  ]
  node [
    id 40
    label "liczy&#263;"
  ]
  node [
    id 41
    label "raise"
  ]
  node [
    id 42
    label "podnosi&#263;"
  ]
  node [
    id 43
    label "Skandynawia"
  ]
  node [
    id 44
    label "Yorkshire"
  ]
  node [
    id 45
    label "Kaukaz"
  ]
  node [
    id 46
    label "Kaszmir"
  ]
  node [
    id 47
    label "Podbeskidzie"
  ]
  node [
    id 48
    label "Toskania"
  ]
  node [
    id 49
    label "&#321;emkowszczyzna"
  ]
  node [
    id 50
    label "Amhara"
  ]
  node [
    id 51
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 52
    label "Lombardia"
  ]
  node [
    id 53
    label "Kalabria"
  ]
  node [
    id 54
    label "kort"
  ]
  node [
    id 55
    label "Tyrol"
  ]
  node [
    id 56
    label "Pamir"
  ]
  node [
    id 57
    label "Lubelszczyzna"
  ]
  node [
    id 58
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 59
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 60
    label "&#379;ywiecczyzna"
  ]
  node [
    id 61
    label "ryzosfera"
  ]
  node [
    id 62
    label "Europa_Wschodnia"
  ]
  node [
    id 63
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 64
    label "Zabajkale"
  ]
  node [
    id 65
    label "Kaszuby"
  ]
  node [
    id 66
    label "Noworosja"
  ]
  node [
    id 67
    label "Bo&#347;nia"
  ]
  node [
    id 68
    label "Ba&#322;kany"
  ]
  node [
    id 69
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 70
    label "Anglia"
  ]
  node [
    id 71
    label "Kielecczyzna"
  ]
  node [
    id 72
    label "Pomorze_Zachodnie"
  ]
  node [
    id 73
    label "Opolskie"
  ]
  node [
    id 74
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 75
    label "skorupa_ziemska"
  ]
  node [
    id 76
    label "Ko&#322;yma"
  ]
  node [
    id 77
    label "Oksytania"
  ]
  node [
    id 78
    label "Syjon"
  ]
  node [
    id 79
    label "posadzka"
  ]
  node [
    id 80
    label "pa&#324;stwo"
  ]
  node [
    id 81
    label "Kociewie"
  ]
  node [
    id 82
    label "Huculszczyzna"
  ]
  node [
    id 83
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 84
    label "budynek"
  ]
  node [
    id 85
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 86
    label "Bawaria"
  ]
  node [
    id 87
    label "pomieszczenie"
  ]
  node [
    id 88
    label "pr&#243;chnica"
  ]
  node [
    id 89
    label "glinowanie"
  ]
  node [
    id 90
    label "Maghreb"
  ]
  node [
    id 91
    label "Bory_Tucholskie"
  ]
  node [
    id 92
    label "Europa_Zachodnia"
  ]
  node [
    id 93
    label "Kerala"
  ]
  node [
    id 94
    label "Podhale"
  ]
  node [
    id 95
    label "Kabylia"
  ]
  node [
    id 96
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 97
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 98
    label "Ma&#322;opolska"
  ]
  node [
    id 99
    label "Polesie"
  ]
  node [
    id 100
    label "Liguria"
  ]
  node [
    id 101
    label "&#321;&#243;dzkie"
  ]
  node [
    id 102
    label "geosystem"
  ]
  node [
    id 103
    label "Palestyna"
  ]
  node [
    id 104
    label "Bojkowszczyzna"
  ]
  node [
    id 105
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 106
    label "Karaiby"
  ]
  node [
    id 107
    label "S&#261;decczyzna"
  ]
  node [
    id 108
    label "Sand&#380;ak"
  ]
  node [
    id 109
    label "Nadrenia"
  ]
  node [
    id 110
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 111
    label "Zakarpacie"
  ]
  node [
    id 112
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 113
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 114
    label "Zag&#243;rze"
  ]
  node [
    id 115
    label "Andaluzja"
  ]
  node [
    id 116
    label "Turkiestan"
  ]
  node [
    id 117
    label "Naddniestrze"
  ]
  node [
    id 118
    label "Hercegowina"
  ]
  node [
    id 119
    label "p&#322;aszczyzna"
  ]
  node [
    id 120
    label "Opolszczyzna"
  ]
  node [
    id 121
    label "jednostka_administracyjna"
  ]
  node [
    id 122
    label "Lotaryngia"
  ]
  node [
    id 123
    label "Afryka_Wschodnia"
  ]
  node [
    id 124
    label "Szlezwik"
  ]
  node [
    id 125
    label "glinowa&#263;"
  ]
  node [
    id 126
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 127
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 128
    label "podglebie"
  ]
  node [
    id 129
    label "Mazowsze"
  ]
  node [
    id 130
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 131
    label "teren"
  ]
  node [
    id 132
    label "Afryka_Zachodnia"
  ]
  node [
    id 133
    label "czynnik_produkcji"
  ]
  node [
    id 134
    label "Galicja"
  ]
  node [
    id 135
    label "Szkocja"
  ]
  node [
    id 136
    label "Walia"
  ]
  node [
    id 137
    label "Powi&#347;le"
  ]
  node [
    id 138
    label "penetrator"
  ]
  node [
    id 139
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 140
    label "kompleks_sorpcyjny"
  ]
  node [
    id 141
    label "Zamojszczyzna"
  ]
  node [
    id 142
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 143
    label "Kujawy"
  ]
  node [
    id 144
    label "Podlasie"
  ]
  node [
    id 145
    label "Laponia"
  ]
  node [
    id 146
    label "Umbria"
  ]
  node [
    id 147
    label "plantowa&#263;"
  ]
  node [
    id 148
    label "Mezoameryka"
  ]
  node [
    id 149
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 150
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 151
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 152
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 153
    label "Kurdystan"
  ]
  node [
    id 154
    label "Kampania"
  ]
  node [
    id 155
    label "Armagnac"
  ]
  node [
    id 156
    label "Polinezja"
  ]
  node [
    id 157
    label "Warmia"
  ]
  node [
    id 158
    label "Wielkopolska"
  ]
  node [
    id 159
    label "litosfera"
  ]
  node [
    id 160
    label "Bordeaux"
  ]
  node [
    id 161
    label "Lauda"
  ]
  node [
    id 162
    label "Mazury"
  ]
  node [
    id 163
    label "Podkarpacie"
  ]
  node [
    id 164
    label "Oceania"
  ]
  node [
    id 165
    label "Lasko"
  ]
  node [
    id 166
    label "Amazonia"
  ]
  node [
    id 167
    label "pojazd"
  ]
  node [
    id 168
    label "glej"
  ]
  node [
    id 169
    label "martwica"
  ]
  node [
    id 170
    label "zapadnia"
  ]
  node [
    id 171
    label "przestrze&#324;"
  ]
  node [
    id 172
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 173
    label "dotleni&#263;"
  ]
  node [
    id 174
    label "Tonkin"
  ]
  node [
    id 175
    label "Kurpie"
  ]
  node [
    id 176
    label "Azja_Wschodnia"
  ]
  node [
    id 177
    label "Mikronezja"
  ]
  node [
    id 178
    label "Ukraina_Zachodnia"
  ]
  node [
    id 179
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 180
    label "Turyngia"
  ]
  node [
    id 181
    label "Baszkiria"
  ]
  node [
    id 182
    label "Apulia"
  ]
  node [
    id 183
    label "miejsce"
  ]
  node [
    id 184
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 185
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 186
    label "Indochiny"
  ]
  node [
    id 187
    label "Lubuskie"
  ]
  node [
    id 188
    label "Biskupizna"
  ]
  node [
    id 189
    label "domain"
  ]
  node [
    id 190
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 191
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 192
    label "chwila"
  ]
  node [
    id 193
    label "uderzenie"
  ]
  node [
    id 194
    label "cios"
  ]
  node [
    id 195
    label "time"
  ]
  node [
    id 196
    label "si&#281;ga&#263;"
  ]
  node [
    id 197
    label "trwa&#263;"
  ]
  node [
    id 198
    label "obecno&#347;&#263;"
  ]
  node [
    id 199
    label "stan"
  ]
  node [
    id 200
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 201
    label "stand"
  ]
  node [
    id 202
    label "mie&#263;_miejsce"
  ]
  node [
    id 203
    label "uczestniczy&#263;"
  ]
  node [
    id 204
    label "chodzi&#263;"
  ]
  node [
    id 205
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 206
    label "equal"
  ]
  node [
    id 207
    label "doros&#322;y"
  ]
  node [
    id 208
    label "wiele"
  ]
  node [
    id 209
    label "dorodny"
  ]
  node [
    id 210
    label "znaczny"
  ]
  node [
    id 211
    label "du&#380;o"
  ]
  node [
    id 212
    label "prawdziwy"
  ]
  node [
    id 213
    label "niema&#322;o"
  ]
  node [
    id 214
    label "wa&#380;ny"
  ]
  node [
    id 215
    label "rozwini&#281;ty"
  ]
  node [
    id 216
    label "uk&#322;a&#347;&#263;"
  ]
  node [
    id 217
    label "s&#322;oneczny"
  ]
  node [
    id 218
    label "Proxima"
  ]
  node [
    id 219
    label "Centauri"
  ]
  node [
    id 220
    label "Ukladu"
  ]
  node [
    id 221
    label "S&#322;onczenego"
  ]
  node [
    id 222
    label "drogi"
  ]
  node [
    id 223
    label "mleczny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 196
  ]
  edge [
    source 7
    target 197
  ]
  edge [
    source 7
    target 198
  ]
  edge [
    source 7
    target 199
  ]
  edge [
    source 7
    target 200
  ]
  edge [
    source 7
    target 201
  ]
  edge [
    source 7
    target 202
  ]
  edge [
    source 7
    target 203
  ]
  edge [
    source 7
    target 204
  ]
  edge [
    source 7
    target 205
  ]
  edge [
    source 7
    target 206
  ]
  edge [
    source 8
    target 207
  ]
  edge [
    source 8
    target 208
  ]
  edge [
    source 8
    target 209
  ]
  edge [
    source 8
    target 210
  ]
  edge [
    source 8
    target 211
  ]
  edge [
    source 8
    target 212
  ]
  edge [
    source 8
    target 213
  ]
  edge [
    source 8
    target 214
  ]
  edge [
    source 8
    target 215
  ]
  edge [
    source 216
    target 217
  ]
  edge [
    source 218
    target 219
  ]
  edge [
    source 220
    target 221
  ]
  edge [
    source 222
    target 223
  ]
]
