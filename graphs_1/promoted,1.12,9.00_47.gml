graph [
  maxDegree 1
  minDegree 1
  meanDegree 1
  density 1.0
  graphCliqueNumber 2
  node [
    id 0
    label "&#1095;&#1091;&#1076;&#1086;"
    origin "text"
  ]
  node [
    id 1
    label "&#1087;&#1088;&#1080;&#1088;&#1086;&#1076;&#1099;"
    origin "text"
  ]
  edge [
    source 0
    target 1
  ]
]
