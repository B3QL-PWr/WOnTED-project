graph [
  maxDegree 6
  minDegree 1
  meanDegree 1.7777777777777777
  density 0.2222222222222222
  graphCliqueNumber 2
  node [
    id 0
    label "satysfakcjonuj&#261;cy"
    origin "text"
  ]
  node [
    id 1
    label "wideo"
    origin "text"
  ]
  node [
    id 2
    label "satysfakcjonuj&#261;co"
  ]
  node [
    id 3
    label "zadowalaj&#261;cy"
  ]
  node [
    id 4
    label "odtwarzacz"
  ]
  node [
    id 5
    label "g&#322;owica_elektromagnetyczna"
  ]
  node [
    id 6
    label "film"
  ]
  node [
    id 7
    label "technika"
  ]
  node [
    id 8
    label "wideokaseta"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
]
