graph [
  maxDegree 324
  minDegree 1
  meanDegree 2.035225048923679
  density 0.003990637350830743
  graphCliqueNumber 3
  node [
    id 0
    label "prezydent"
    origin "text"
  ]
  node [
    id 1
    label "filipina"
    origin "text"
  ]
  node [
    id 2
    label "rodrigo"
    origin "text"
  ]
  node [
    id 3
    label "duterte"
    origin "text"
  ]
  node [
    id 4
    label "raz"
    origin "text"
  ]
  node [
    id 5
    label "dwa"
    origin "text"
  ]
  node [
    id 6
    label "zab&#322;ysn&#261;&#263;"
    origin "text"
  ]
  node [
    id 7
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 8
    label "niekonwencjonalny"
    origin "text"
  ]
  node [
    id 9
    label "wypowied&#378;"
    origin "text"
  ]
  node [
    id 10
    label "plan"
    origin "text"
  ]
  node [
    id 11
    label "poprawa"
    origin "text"
  ]
  node [
    id 12
    label "byt"
    origin "text"
  ]
  node [
    id 13
    label "kraj"
    origin "text"
  ]
  node [
    id 14
    label "na_przyk&#322;ad"
    origin "text"
  ]
  node [
    id 15
    label "chcie&#263;"
    origin "text"
  ]
  node [
    id 16
    label "zrzuca&#263;"
    origin "text"
  ]
  node [
    id 17
    label "helikopter"
    origin "text"
  ]
  node [
    id 18
    label "gro&#378;ny"
    origin "text"
  ]
  node [
    id 19
    label "przest&#281;pca"
    origin "text"
  ]
  node [
    id 20
    label "te&#380;"
    origin "text"
  ]
  node [
    id 21
    label "o&#347;wiadczy&#263;"
    origin "text"
  ]
  node [
    id 22
    label "zrzec"
    origin "text"
  ]
  node [
    id 23
    label "si&#281;"
    origin "text"
  ]
  node [
    id 24
    label "urz&#261;d"
    origin "text"
  ]
  node [
    id 25
    label "moment"
    origin "text"
  ]
  node [
    id 26
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 27
    label "kto&#347;"
    origin "text"
  ]
  node [
    id 28
    label "udowodni&#263;"
    origin "text"
  ]
  node [
    id 29
    label "istnienie"
    origin "text"
  ]
  node [
    id 30
    label "b&#243;g"
    origin "text"
  ]
  node [
    id 31
    label "Jelcyn"
  ]
  node [
    id 32
    label "Roosevelt"
  ]
  node [
    id 33
    label "Clinton"
  ]
  node [
    id 34
    label "dostojnik"
  ]
  node [
    id 35
    label "Nixon"
  ]
  node [
    id 36
    label "Tito"
  ]
  node [
    id 37
    label "de_Gaulle"
  ]
  node [
    id 38
    label "gruba_ryba"
  ]
  node [
    id 39
    label "Gorbaczow"
  ]
  node [
    id 40
    label "Wa&#322;&#281;sa"
  ]
  node [
    id 41
    label "Putin"
  ]
  node [
    id 42
    label "Naser"
  ]
  node [
    id 43
    label "samorz&#261;dowiec"
  ]
  node [
    id 44
    label "Kemal"
  ]
  node [
    id 45
    label "g&#322;owa_pa&#324;stwa"
  ]
  node [
    id 46
    label "zwierzchnik"
  ]
  node [
    id 47
    label "Bierut"
  ]
  node [
    id 48
    label "chwila"
  ]
  node [
    id 49
    label "uderzenie"
  ]
  node [
    id 50
    label "cios"
  ]
  node [
    id 51
    label "time"
  ]
  node [
    id 52
    label "za&#347;wieci&#263;_si&#281;"
  ]
  node [
    id 53
    label "flash"
  ]
  node [
    id 54
    label "ukaza&#263;_si&#281;"
  ]
  node [
    id 55
    label "zago&#347;ci&#263;"
  ]
  node [
    id 56
    label "wyr&#243;&#380;ni&#263;_si&#281;"
  ]
  node [
    id 57
    label "niepospolity"
  ]
  node [
    id 58
    label "bezpretensjonalny"
  ]
  node [
    id 59
    label "niestandardowy"
  ]
  node [
    id 60
    label "oryginalny"
  ]
  node [
    id 61
    label "niekonwencjonalnie"
  ]
  node [
    id 62
    label "parafrazowa&#263;"
  ]
  node [
    id 63
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 64
    label "sparafrazowa&#263;"
  ]
  node [
    id 65
    label "s&#261;d"
  ]
  node [
    id 66
    label "trawestowanie"
  ]
  node [
    id 67
    label "sparafrazowanie"
  ]
  node [
    id 68
    label "strawestowa&#263;"
  ]
  node [
    id 69
    label "sformu&#322;owanie"
  ]
  node [
    id 70
    label "strawestowanie"
  ]
  node [
    id 71
    label "komunikat"
  ]
  node [
    id 72
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 73
    label "delimitacja"
  ]
  node [
    id 74
    label "trawestowa&#263;"
  ]
  node [
    id 75
    label "parafrazowanie"
  ]
  node [
    id 76
    label "stylizacja"
  ]
  node [
    id 77
    label "ozdobnik"
  ]
  node [
    id 78
    label "pos&#322;uchanie"
  ]
  node [
    id 79
    label "rezultat"
  ]
  node [
    id 80
    label "device"
  ]
  node [
    id 81
    label "model"
  ]
  node [
    id 82
    label "wytw&#243;r"
  ]
  node [
    id 83
    label "obraz"
  ]
  node [
    id 84
    label "przestrze&#324;"
  ]
  node [
    id 85
    label "dekoracja"
  ]
  node [
    id 86
    label "intencja"
  ]
  node [
    id 87
    label "agreement"
  ]
  node [
    id 88
    label "pomys&#322;"
  ]
  node [
    id 89
    label "punkt"
  ]
  node [
    id 90
    label "miejsce_pracy"
  ]
  node [
    id 91
    label "perspektywa"
  ]
  node [
    id 92
    label "rysunek"
  ]
  node [
    id 93
    label "reprezentacja"
  ]
  node [
    id 94
    label "ulepszenie"
  ]
  node [
    id 95
    label "alteration"
  ]
  node [
    id 96
    label "zmiana"
  ]
  node [
    id 97
    label "sprawdzian"
  ]
  node [
    id 98
    label "ontologicznie"
  ]
  node [
    id 99
    label "utrzymywanie"
  ]
  node [
    id 100
    label "bycie"
  ]
  node [
    id 101
    label "utrzymywa&#263;"
  ]
  node [
    id 102
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 103
    label "entity"
  ]
  node [
    id 104
    label "wy&#380;ywienie"
  ]
  node [
    id 105
    label "egzystencja"
  ]
  node [
    id 106
    label "utrzymanie"
  ]
  node [
    id 107
    label "potencja"
  ]
  node [
    id 108
    label "utrzyma&#263;"
  ]
  node [
    id 109
    label "subsystencja"
  ]
  node [
    id 110
    label "Skandynawia"
  ]
  node [
    id 111
    label "Filipiny"
  ]
  node [
    id 112
    label "Rwanda"
  ]
  node [
    id 113
    label "Kaukaz"
  ]
  node [
    id 114
    label "Kaszmir"
  ]
  node [
    id 115
    label "Toskania"
  ]
  node [
    id 116
    label "Yorkshire"
  ]
  node [
    id 117
    label "&#321;emkowszczyzna"
  ]
  node [
    id 118
    label "obszar"
  ]
  node [
    id 119
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 120
    label "Monako"
  ]
  node [
    id 121
    label "Amhara"
  ]
  node [
    id 122
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 123
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 124
    label "Lombardia"
  ]
  node [
    id 125
    label "Korea"
  ]
  node [
    id 126
    label "Kalabria"
  ]
  node [
    id 127
    label "Ghana"
  ]
  node [
    id 128
    label "Czarnog&#243;ra"
  ]
  node [
    id 129
    label "Tyrol"
  ]
  node [
    id 130
    label "Malawi"
  ]
  node [
    id 131
    label "Indonezja"
  ]
  node [
    id 132
    label "Bu&#322;garia"
  ]
  node [
    id 133
    label "Nauru"
  ]
  node [
    id 134
    label "Kenia"
  ]
  node [
    id 135
    label "Pamir"
  ]
  node [
    id 136
    label "Kambod&#380;a"
  ]
  node [
    id 137
    label "Lubelszczyzna"
  ]
  node [
    id 138
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 139
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 140
    label "Mali"
  ]
  node [
    id 141
    label "&#379;ywiecczyzna"
  ]
  node [
    id 142
    label "Austria"
  ]
  node [
    id 143
    label "interior"
  ]
  node [
    id 144
    label "Europa_Wschodnia"
  ]
  node [
    id 145
    label "Armenia"
  ]
  node [
    id 146
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 147
    label "Fid&#380;i"
  ]
  node [
    id 148
    label "Tuwalu"
  ]
  node [
    id 149
    label "Zabajkale"
  ]
  node [
    id 150
    label "Etiopia"
  ]
  node [
    id 151
    label "Malta"
  ]
  node [
    id 152
    label "Malezja"
  ]
  node [
    id 153
    label "Kaszuby"
  ]
  node [
    id 154
    label "Bo&#347;nia"
  ]
  node [
    id 155
    label "Noworosja"
  ]
  node [
    id 156
    label "Grenada"
  ]
  node [
    id 157
    label "Tad&#380;ykistan"
  ]
  node [
    id 158
    label "Ba&#322;kany"
  ]
  node [
    id 159
    label "Wehrlen"
  ]
  node [
    id 160
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 161
    label "Anglia"
  ]
  node [
    id 162
    label "Kielecczyzna"
  ]
  node [
    id 163
    label "Rumunia"
  ]
  node [
    id 164
    label "Pomorze_Zachodnie"
  ]
  node [
    id 165
    label "Maroko"
  ]
  node [
    id 166
    label "Bhutan"
  ]
  node [
    id 167
    label "Opolskie"
  ]
  node [
    id 168
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 169
    label "Ko&#322;yma"
  ]
  node [
    id 170
    label "Oksytania"
  ]
  node [
    id 171
    label "S&#322;owacja"
  ]
  node [
    id 172
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 173
    label "Seszele"
  ]
  node [
    id 174
    label "Syjon"
  ]
  node [
    id 175
    label "Kuwejt"
  ]
  node [
    id 176
    label "Arabia_Saudyjska"
  ]
  node [
    id 177
    label "Kociewie"
  ]
  node [
    id 178
    label "Ekwador"
  ]
  node [
    id 179
    label "Kanada"
  ]
  node [
    id 180
    label "ziemia"
  ]
  node [
    id 181
    label "Japonia"
  ]
  node [
    id 182
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 183
    label "Hiszpania"
  ]
  node [
    id 184
    label "Wyspy_Marshalla"
  ]
  node [
    id 185
    label "Botswana"
  ]
  node [
    id 186
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 187
    label "D&#380;ibuti"
  ]
  node [
    id 188
    label "Huculszczyzna"
  ]
  node [
    id 189
    label "Wietnam"
  ]
  node [
    id 190
    label "Egipt"
  ]
  node [
    id 191
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 192
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 193
    label "Burkina_Faso"
  ]
  node [
    id 194
    label "Bawaria"
  ]
  node [
    id 195
    label "Niemcy"
  ]
  node [
    id 196
    label "Khitai"
  ]
  node [
    id 197
    label "Macedonia"
  ]
  node [
    id 198
    label "Albania"
  ]
  node [
    id 199
    label "Madagaskar"
  ]
  node [
    id 200
    label "Bahrajn"
  ]
  node [
    id 201
    label "Jemen"
  ]
  node [
    id 202
    label "Lesoto"
  ]
  node [
    id 203
    label "Maghreb"
  ]
  node [
    id 204
    label "Samoa"
  ]
  node [
    id 205
    label "Andora"
  ]
  node [
    id 206
    label "Bory_Tucholskie"
  ]
  node [
    id 207
    label "Chiny"
  ]
  node [
    id 208
    label "Europa_Zachodnia"
  ]
  node [
    id 209
    label "Cypr"
  ]
  node [
    id 210
    label "Wielka_Brytania"
  ]
  node [
    id 211
    label "Kerala"
  ]
  node [
    id 212
    label "Podhale"
  ]
  node [
    id 213
    label "Kabylia"
  ]
  node [
    id 214
    label "Ukraina"
  ]
  node [
    id 215
    label "Paragwaj"
  ]
  node [
    id 216
    label "Trynidad_i_Tobago"
  ]
  node [
    id 217
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 218
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 219
    label "Ma&#322;opolska"
  ]
  node [
    id 220
    label "Polesie"
  ]
  node [
    id 221
    label "Liguria"
  ]
  node [
    id 222
    label "Libia"
  ]
  node [
    id 223
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 224
    label "&#321;&#243;dzkie"
  ]
  node [
    id 225
    label "Surinam"
  ]
  node [
    id 226
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 227
    label "Palestyna"
  ]
  node [
    id 228
    label "Australia"
  ]
  node [
    id 229
    label "Nigeria"
  ]
  node [
    id 230
    label "Honduras"
  ]
  node [
    id 231
    label "Bojkowszczyzna"
  ]
  node [
    id 232
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 233
    label "Karaiby"
  ]
  node [
    id 234
    label "Bangladesz"
  ]
  node [
    id 235
    label "Peru"
  ]
  node [
    id 236
    label "Kazachstan"
  ]
  node [
    id 237
    label "USA"
  ]
  node [
    id 238
    label "Irak"
  ]
  node [
    id 239
    label "Nepal"
  ]
  node [
    id 240
    label "S&#261;decczyzna"
  ]
  node [
    id 241
    label "Sudan"
  ]
  node [
    id 242
    label "Sand&#380;ak"
  ]
  node [
    id 243
    label "Nadrenia"
  ]
  node [
    id 244
    label "San_Marino"
  ]
  node [
    id 245
    label "Burundi"
  ]
  node [
    id 246
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 247
    label "Dominikana"
  ]
  node [
    id 248
    label "Komory"
  ]
  node [
    id 249
    label "Zakarpacie"
  ]
  node [
    id 250
    label "Gwatemala"
  ]
  node [
    id 251
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 252
    label "Zag&#243;rze"
  ]
  node [
    id 253
    label "Andaluzja"
  ]
  node [
    id 254
    label "granica_pa&#324;stwa"
  ]
  node [
    id 255
    label "Turkiestan"
  ]
  node [
    id 256
    label "Naddniestrze"
  ]
  node [
    id 257
    label "Hercegowina"
  ]
  node [
    id 258
    label "Brunei"
  ]
  node [
    id 259
    label "Iran"
  ]
  node [
    id 260
    label "jednostka_administracyjna"
  ]
  node [
    id 261
    label "Zimbabwe"
  ]
  node [
    id 262
    label "Namibia"
  ]
  node [
    id 263
    label "Meksyk"
  ]
  node [
    id 264
    label "Lotaryngia"
  ]
  node [
    id 265
    label "Kamerun"
  ]
  node [
    id 266
    label "Opolszczyzna"
  ]
  node [
    id 267
    label "Afryka_Wschodnia"
  ]
  node [
    id 268
    label "Szlezwik"
  ]
  node [
    id 269
    label "Somalia"
  ]
  node [
    id 270
    label "Angola"
  ]
  node [
    id 271
    label "Gabon"
  ]
  node [
    id 272
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 273
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 274
    label "Mozambik"
  ]
  node [
    id 275
    label "Tajwan"
  ]
  node [
    id 276
    label "Tunezja"
  ]
  node [
    id 277
    label "Nowa_Zelandia"
  ]
  node [
    id 278
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 279
    label "Podbeskidzie"
  ]
  node [
    id 280
    label "Liban"
  ]
  node [
    id 281
    label "Jordania"
  ]
  node [
    id 282
    label "Tonga"
  ]
  node [
    id 283
    label "Czad"
  ]
  node [
    id 284
    label "Liberia"
  ]
  node [
    id 285
    label "Gwinea"
  ]
  node [
    id 286
    label "Belize"
  ]
  node [
    id 287
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 288
    label "Mazowsze"
  ]
  node [
    id 289
    label "&#321;otwa"
  ]
  node [
    id 290
    label "Syria"
  ]
  node [
    id 291
    label "Benin"
  ]
  node [
    id 292
    label "Afryka_Zachodnia"
  ]
  node [
    id 293
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 294
    label "Dominika"
  ]
  node [
    id 295
    label "Antigua_i_Barbuda"
  ]
  node [
    id 296
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 297
    label "Hanower"
  ]
  node [
    id 298
    label "Galicja"
  ]
  node [
    id 299
    label "Szkocja"
  ]
  node [
    id 300
    label "Walia"
  ]
  node [
    id 301
    label "Afganistan"
  ]
  node [
    id 302
    label "Kiribati"
  ]
  node [
    id 303
    label "W&#322;ochy"
  ]
  node [
    id 304
    label "Szwajcaria"
  ]
  node [
    id 305
    label "Powi&#347;le"
  ]
  node [
    id 306
    label "Sahara_Zachodnia"
  ]
  node [
    id 307
    label "Chorwacja"
  ]
  node [
    id 308
    label "Tajlandia"
  ]
  node [
    id 309
    label "Salwador"
  ]
  node [
    id 310
    label "Bahamy"
  ]
  node [
    id 311
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 312
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 313
    label "Zamojszczyzna"
  ]
  node [
    id 314
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 315
    label "S&#322;owenia"
  ]
  node [
    id 316
    label "Gambia"
  ]
  node [
    id 317
    label "Kujawy"
  ]
  node [
    id 318
    label "Urugwaj"
  ]
  node [
    id 319
    label "Podlasie"
  ]
  node [
    id 320
    label "Zair"
  ]
  node [
    id 321
    label "Erytrea"
  ]
  node [
    id 322
    label "Laponia"
  ]
  node [
    id 323
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 324
    label "Umbria"
  ]
  node [
    id 325
    label "Rosja"
  ]
  node [
    id 326
    label "Uganda"
  ]
  node [
    id 327
    label "Niger"
  ]
  node [
    id 328
    label "Mauritius"
  ]
  node [
    id 329
    label "Turkmenistan"
  ]
  node [
    id 330
    label "Turcja"
  ]
  node [
    id 331
    label "Mezoameryka"
  ]
  node [
    id 332
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 333
    label "Irlandia"
  ]
  node [
    id 334
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 335
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 336
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 337
    label "Gwinea_Bissau"
  ]
  node [
    id 338
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 339
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 340
    label "Kurdystan"
  ]
  node [
    id 341
    label "Belgia"
  ]
  node [
    id 342
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 343
    label "Palau"
  ]
  node [
    id 344
    label "Barbados"
  ]
  node [
    id 345
    label "Chile"
  ]
  node [
    id 346
    label "Wenezuela"
  ]
  node [
    id 347
    label "W&#281;gry"
  ]
  node [
    id 348
    label "Argentyna"
  ]
  node [
    id 349
    label "Kolumbia"
  ]
  node [
    id 350
    label "Kampania"
  ]
  node [
    id 351
    label "Armagnac"
  ]
  node [
    id 352
    label "Sierra_Leone"
  ]
  node [
    id 353
    label "Azerbejd&#380;an"
  ]
  node [
    id 354
    label "Kongo"
  ]
  node [
    id 355
    label "Polinezja"
  ]
  node [
    id 356
    label "Warmia"
  ]
  node [
    id 357
    label "Pakistan"
  ]
  node [
    id 358
    label "Liechtenstein"
  ]
  node [
    id 359
    label "Wielkopolska"
  ]
  node [
    id 360
    label "Nikaragua"
  ]
  node [
    id 361
    label "Senegal"
  ]
  node [
    id 362
    label "brzeg"
  ]
  node [
    id 363
    label "Bordeaux"
  ]
  node [
    id 364
    label "Lauda"
  ]
  node [
    id 365
    label "Indie"
  ]
  node [
    id 366
    label "Mazury"
  ]
  node [
    id 367
    label "Suazi"
  ]
  node [
    id 368
    label "Polska"
  ]
  node [
    id 369
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 370
    label "Algieria"
  ]
  node [
    id 371
    label "Jamajka"
  ]
  node [
    id 372
    label "Timor_Wschodni"
  ]
  node [
    id 373
    label "Oceania"
  ]
  node [
    id 374
    label "Kostaryka"
  ]
  node [
    id 375
    label "Podkarpacie"
  ]
  node [
    id 376
    label "Lasko"
  ]
  node [
    id 377
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 378
    label "Kuba"
  ]
  node [
    id 379
    label "Mauretania"
  ]
  node [
    id 380
    label "Amazonia"
  ]
  node [
    id 381
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 382
    label "Portoryko"
  ]
  node [
    id 383
    label "Brazylia"
  ]
  node [
    id 384
    label "Mo&#322;dawia"
  ]
  node [
    id 385
    label "organizacja"
  ]
  node [
    id 386
    label "Litwa"
  ]
  node [
    id 387
    label "Kirgistan"
  ]
  node [
    id 388
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 389
    label "Izrael"
  ]
  node [
    id 390
    label "Grecja"
  ]
  node [
    id 391
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 392
    label "Kurpie"
  ]
  node [
    id 393
    label "Holandia"
  ]
  node [
    id 394
    label "Sri_Lanka"
  ]
  node [
    id 395
    label "Tonkin"
  ]
  node [
    id 396
    label "Katar"
  ]
  node [
    id 397
    label "Azja_Wschodnia"
  ]
  node [
    id 398
    label "Mikronezja"
  ]
  node [
    id 399
    label "Ukraina_Zachodnia"
  ]
  node [
    id 400
    label "Laos"
  ]
  node [
    id 401
    label "Mongolia"
  ]
  node [
    id 402
    label "Turyngia"
  ]
  node [
    id 403
    label "Malediwy"
  ]
  node [
    id 404
    label "Zambia"
  ]
  node [
    id 405
    label "Baszkiria"
  ]
  node [
    id 406
    label "Tanzania"
  ]
  node [
    id 407
    label "Gujana"
  ]
  node [
    id 408
    label "Apulia"
  ]
  node [
    id 409
    label "Czechy"
  ]
  node [
    id 410
    label "Panama"
  ]
  node [
    id 411
    label "Uzbekistan"
  ]
  node [
    id 412
    label "Gruzja"
  ]
  node [
    id 413
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 414
    label "Serbia"
  ]
  node [
    id 415
    label "Francja"
  ]
  node [
    id 416
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 417
    label "Togo"
  ]
  node [
    id 418
    label "Estonia"
  ]
  node [
    id 419
    label "Indochiny"
  ]
  node [
    id 420
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 421
    label "Oman"
  ]
  node [
    id 422
    label "Boliwia"
  ]
  node [
    id 423
    label "Portugalia"
  ]
  node [
    id 424
    label "Wyspy_Salomona"
  ]
  node [
    id 425
    label "Luksemburg"
  ]
  node [
    id 426
    label "Haiti"
  ]
  node [
    id 427
    label "Biskupizna"
  ]
  node [
    id 428
    label "Lubuskie"
  ]
  node [
    id 429
    label "Birma"
  ]
  node [
    id 430
    label "Rodezja"
  ]
  node [
    id 431
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 432
    label "czu&#263;"
  ]
  node [
    id 433
    label "desire"
  ]
  node [
    id 434
    label "kcie&#263;"
  ]
  node [
    id 435
    label "przemieszcza&#263;"
  ]
  node [
    id 436
    label "spill"
  ]
  node [
    id 437
    label "odprowadza&#263;"
  ]
  node [
    id 438
    label "give"
  ]
  node [
    id 439
    label "umieszcza&#263;"
  ]
  node [
    id 440
    label "zdejmowa&#263;"
  ]
  node [
    id 441
    label "wirop&#322;at"
  ]
  node [
    id 442
    label "p&#322;at_no&#347;ny"
  ]
  node [
    id 443
    label "&#347;mig&#322;o"
  ]
  node [
    id 444
    label "nad&#261;sany"
  ]
  node [
    id 445
    label "niebezpieczny"
  ]
  node [
    id 446
    label "gro&#378;nie"
  ]
  node [
    id 447
    label "z&#322;oczy&#324;ca"
  ]
  node [
    id 448
    label "pogwa&#322;ciciel"
  ]
  node [
    id 449
    label "advise"
  ]
  node [
    id 450
    label "poinformowa&#263;"
  ]
  node [
    id 451
    label "organ"
  ]
  node [
    id 452
    label "w&#322;adza"
  ]
  node [
    id 453
    label "instytucja"
  ]
  node [
    id 454
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 455
    label "mianowaniec"
  ]
  node [
    id 456
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 457
    label "stanowisko"
  ]
  node [
    id 458
    label "position"
  ]
  node [
    id 459
    label "dzia&#322;"
  ]
  node [
    id 460
    label "siedziba"
  ]
  node [
    id 461
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 462
    label "okienko"
  ]
  node [
    id 463
    label "okres_czasu"
  ]
  node [
    id 464
    label "minute"
  ]
  node [
    id 465
    label "jednostka_geologiczna"
  ]
  node [
    id 466
    label "warto&#347;&#263;_oczekiwana"
  ]
  node [
    id 467
    label "chron"
  ]
  node [
    id 468
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 469
    label "fragment"
  ]
  node [
    id 470
    label "cz&#322;owiek"
  ]
  node [
    id 471
    label "znaczenie"
  ]
  node [
    id 472
    label "go&#347;&#263;"
  ]
  node [
    id 473
    label "osoba"
  ]
  node [
    id 474
    label "posta&#263;"
  ]
  node [
    id 475
    label "testify"
  ]
  node [
    id 476
    label "uzasadni&#263;"
  ]
  node [
    id 477
    label "produkowanie"
  ]
  node [
    id 478
    label "robienie"
  ]
  node [
    id 479
    label "znikni&#281;cie"
  ]
  node [
    id 480
    label "urzeczywistnianie"
  ]
  node [
    id 481
    label "wyprodukowanie"
  ]
  node [
    id 482
    label "dematerializowanie_si&#281;"
  ]
  node [
    id 483
    label "being"
  ]
  node [
    id 484
    label "Dionizos"
  ]
  node [
    id 485
    label "Neptun"
  ]
  node [
    id 486
    label "Hesperos"
  ]
  node [
    id 487
    label "ba&#322;wan"
  ]
  node [
    id 488
    label "niebiosa"
  ]
  node [
    id 489
    label "Ereb"
  ]
  node [
    id 490
    label "Sylen"
  ]
  node [
    id 491
    label "uwielbienie"
  ]
  node [
    id 492
    label "s&#261;d_ostateczny"
  ]
  node [
    id 493
    label "idol"
  ]
  node [
    id 494
    label "Bachus"
  ]
  node [
    id 495
    label "ofiarowa&#263;"
  ]
  node [
    id 496
    label "tr&#243;jca"
  ]
  node [
    id 497
    label "Waruna"
  ]
  node [
    id 498
    label "ofiarowanie"
  ]
  node [
    id 499
    label "igrzyska_greckie"
  ]
  node [
    id 500
    label "Janus"
  ]
  node [
    id 501
    label "Kupidyn"
  ]
  node [
    id 502
    label "ofiarowywanie"
  ]
  node [
    id 503
    label "gigant"
  ]
  node [
    id 504
    label "Boreasz"
  ]
  node [
    id 505
    label "politeizm"
  ]
  node [
    id 506
    label "istota_nadprzyrodzona"
  ]
  node [
    id 507
    label "ofiarowywa&#263;"
  ]
  node [
    id 508
    label "Posejdon"
  ]
  node [
    id 509
    label "Rodrigo"
  ]
  node [
    id 510
    label "Duterte"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 12
    target 107
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 12
    target 109
  ]
  edge [
    source 12
    target 29
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 13
    target 113
  ]
  edge [
    source 13
    target 114
  ]
  edge [
    source 13
    target 115
  ]
  edge [
    source 13
    target 116
  ]
  edge [
    source 13
    target 117
  ]
  edge [
    source 13
    target 118
  ]
  edge [
    source 13
    target 119
  ]
  edge [
    source 13
    target 120
  ]
  edge [
    source 13
    target 121
  ]
  edge [
    source 13
    target 122
  ]
  edge [
    source 13
    target 123
  ]
  edge [
    source 13
    target 124
  ]
  edge [
    source 13
    target 125
  ]
  edge [
    source 13
    target 126
  ]
  edge [
    source 13
    target 127
  ]
  edge [
    source 13
    target 128
  ]
  edge [
    source 13
    target 129
  ]
  edge [
    source 13
    target 130
  ]
  edge [
    source 13
    target 131
  ]
  edge [
    source 13
    target 132
  ]
  edge [
    source 13
    target 133
  ]
  edge [
    source 13
    target 134
  ]
  edge [
    source 13
    target 135
  ]
  edge [
    source 13
    target 136
  ]
  edge [
    source 13
    target 137
  ]
  edge [
    source 13
    target 138
  ]
  edge [
    source 13
    target 139
  ]
  edge [
    source 13
    target 140
  ]
  edge [
    source 13
    target 141
  ]
  edge [
    source 13
    target 142
  ]
  edge [
    source 13
    target 143
  ]
  edge [
    source 13
    target 144
  ]
  edge [
    source 13
    target 145
  ]
  edge [
    source 13
    target 146
  ]
  edge [
    source 13
    target 147
  ]
  edge [
    source 13
    target 148
  ]
  edge [
    source 13
    target 149
  ]
  edge [
    source 13
    target 150
  ]
  edge [
    source 13
    target 151
  ]
  edge [
    source 13
    target 152
  ]
  edge [
    source 13
    target 153
  ]
  edge [
    source 13
    target 154
  ]
  edge [
    source 13
    target 155
  ]
  edge [
    source 13
    target 156
  ]
  edge [
    source 13
    target 157
  ]
  edge [
    source 13
    target 158
  ]
  edge [
    source 13
    target 159
  ]
  edge [
    source 13
    target 160
  ]
  edge [
    source 13
    target 161
  ]
  edge [
    source 13
    target 162
  ]
  edge [
    source 13
    target 163
  ]
  edge [
    source 13
    target 164
  ]
  edge [
    source 13
    target 165
  ]
  edge [
    source 13
    target 166
  ]
  edge [
    source 13
    target 167
  ]
  edge [
    source 13
    target 168
  ]
  edge [
    source 13
    target 169
  ]
  edge [
    source 13
    target 170
  ]
  edge [
    source 13
    target 171
  ]
  edge [
    source 13
    target 172
  ]
  edge [
    source 13
    target 173
  ]
  edge [
    source 13
    target 174
  ]
  edge [
    source 13
    target 175
  ]
  edge [
    source 13
    target 176
  ]
  edge [
    source 13
    target 177
  ]
  edge [
    source 13
    target 178
  ]
  edge [
    source 13
    target 179
  ]
  edge [
    source 13
    target 180
  ]
  edge [
    source 13
    target 181
  ]
  edge [
    source 13
    target 182
  ]
  edge [
    source 13
    target 183
  ]
  edge [
    source 13
    target 184
  ]
  edge [
    source 13
    target 185
  ]
  edge [
    source 13
    target 186
  ]
  edge [
    source 13
    target 187
  ]
  edge [
    source 13
    target 188
  ]
  edge [
    source 13
    target 189
  ]
  edge [
    source 13
    target 190
  ]
  edge [
    source 13
    target 191
  ]
  edge [
    source 13
    target 192
  ]
  edge [
    source 13
    target 193
  ]
  edge [
    source 13
    target 194
  ]
  edge [
    source 13
    target 195
  ]
  edge [
    source 13
    target 196
  ]
  edge [
    source 13
    target 197
  ]
  edge [
    source 13
    target 198
  ]
  edge [
    source 13
    target 199
  ]
  edge [
    source 13
    target 200
  ]
  edge [
    source 13
    target 201
  ]
  edge [
    source 13
    target 202
  ]
  edge [
    source 13
    target 203
  ]
  edge [
    source 13
    target 204
  ]
  edge [
    source 13
    target 205
  ]
  edge [
    source 13
    target 206
  ]
  edge [
    source 13
    target 207
  ]
  edge [
    source 13
    target 208
  ]
  edge [
    source 13
    target 209
  ]
  edge [
    source 13
    target 210
  ]
  edge [
    source 13
    target 211
  ]
  edge [
    source 13
    target 212
  ]
  edge [
    source 13
    target 213
  ]
  edge [
    source 13
    target 214
  ]
  edge [
    source 13
    target 215
  ]
  edge [
    source 13
    target 216
  ]
  edge [
    source 13
    target 217
  ]
  edge [
    source 13
    target 218
  ]
  edge [
    source 13
    target 219
  ]
  edge [
    source 13
    target 220
  ]
  edge [
    source 13
    target 221
  ]
  edge [
    source 13
    target 222
  ]
  edge [
    source 13
    target 223
  ]
  edge [
    source 13
    target 224
  ]
  edge [
    source 13
    target 225
  ]
  edge [
    source 13
    target 226
  ]
  edge [
    source 13
    target 227
  ]
  edge [
    source 13
    target 228
  ]
  edge [
    source 13
    target 229
  ]
  edge [
    source 13
    target 230
  ]
  edge [
    source 13
    target 231
  ]
  edge [
    source 13
    target 232
  ]
  edge [
    source 13
    target 233
  ]
  edge [
    source 13
    target 234
  ]
  edge [
    source 13
    target 235
  ]
  edge [
    source 13
    target 236
  ]
  edge [
    source 13
    target 237
  ]
  edge [
    source 13
    target 238
  ]
  edge [
    source 13
    target 239
  ]
  edge [
    source 13
    target 240
  ]
  edge [
    source 13
    target 241
  ]
  edge [
    source 13
    target 242
  ]
  edge [
    source 13
    target 243
  ]
  edge [
    source 13
    target 244
  ]
  edge [
    source 13
    target 245
  ]
  edge [
    source 13
    target 246
  ]
  edge [
    source 13
    target 247
  ]
  edge [
    source 13
    target 248
  ]
  edge [
    source 13
    target 249
  ]
  edge [
    source 13
    target 250
  ]
  edge [
    source 13
    target 251
  ]
  edge [
    source 13
    target 252
  ]
  edge [
    source 13
    target 253
  ]
  edge [
    source 13
    target 254
  ]
  edge [
    source 13
    target 255
  ]
  edge [
    source 13
    target 256
  ]
  edge [
    source 13
    target 257
  ]
  edge [
    source 13
    target 258
  ]
  edge [
    source 13
    target 259
  ]
  edge [
    source 13
    target 260
  ]
  edge [
    source 13
    target 261
  ]
  edge [
    source 13
    target 262
  ]
  edge [
    source 13
    target 263
  ]
  edge [
    source 13
    target 264
  ]
  edge [
    source 13
    target 265
  ]
  edge [
    source 13
    target 266
  ]
  edge [
    source 13
    target 267
  ]
  edge [
    source 13
    target 268
  ]
  edge [
    source 13
    target 269
  ]
  edge [
    source 13
    target 270
  ]
  edge [
    source 13
    target 271
  ]
  edge [
    source 13
    target 272
  ]
  edge [
    source 13
    target 273
  ]
  edge [
    source 13
    target 274
  ]
  edge [
    source 13
    target 275
  ]
  edge [
    source 13
    target 276
  ]
  edge [
    source 13
    target 277
  ]
  edge [
    source 13
    target 278
  ]
  edge [
    source 13
    target 279
  ]
  edge [
    source 13
    target 280
  ]
  edge [
    source 13
    target 281
  ]
  edge [
    source 13
    target 282
  ]
  edge [
    source 13
    target 283
  ]
  edge [
    source 13
    target 284
  ]
  edge [
    source 13
    target 285
  ]
  edge [
    source 13
    target 286
  ]
  edge [
    source 13
    target 287
  ]
  edge [
    source 13
    target 288
  ]
  edge [
    source 13
    target 289
  ]
  edge [
    source 13
    target 290
  ]
  edge [
    source 13
    target 291
  ]
  edge [
    source 13
    target 292
  ]
  edge [
    source 13
    target 293
  ]
  edge [
    source 13
    target 294
  ]
  edge [
    source 13
    target 295
  ]
  edge [
    source 13
    target 296
  ]
  edge [
    source 13
    target 297
  ]
  edge [
    source 13
    target 298
  ]
  edge [
    source 13
    target 299
  ]
  edge [
    source 13
    target 300
  ]
  edge [
    source 13
    target 301
  ]
  edge [
    source 13
    target 302
  ]
  edge [
    source 13
    target 303
  ]
  edge [
    source 13
    target 304
  ]
  edge [
    source 13
    target 305
  ]
  edge [
    source 13
    target 306
  ]
  edge [
    source 13
    target 307
  ]
  edge [
    source 13
    target 308
  ]
  edge [
    source 13
    target 309
  ]
  edge [
    source 13
    target 310
  ]
  edge [
    source 13
    target 311
  ]
  edge [
    source 13
    target 312
  ]
  edge [
    source 13
    target 313
  ]
  edge [
    source 13
    target 314
  ]
  edge [
    source 13
    target 315
  ]
  edge [
    source 13
    target 316
  ]
  edge [
    source 13
    target 317
  ]
  edge [
    source 13
    target 318
  ]
  edge [
    source 13
    target 319
  ]
  edge [
    source 13
    target 320
  ]
  edge [
    source 13
    target 321
  ]
  edge [
    source 13
    target 322
  ]
  edge [
    source 13
    target 323
  ]
  edge [
    source 13
    target 324
  ]
  edge [
    source 13
    target 325
  ]
  edge [
    source 13
    target 326
  ]
  edge [
    source 13
    target 327
  ]
  edge [
    source 13
    target 328
  ]
  edge [
    source 13
    target 329
  ]
  edge [
    source 13
    target 330
  ]
  edge [
    source 13
    target 331
  ]
  edge [
    source 13
    target 332
  ]
  edge [
    source 13
    target 333
  ]
  edge [
    source 13
    target 334
  ]
  edge [
    source 13
    target 335
  ]
  edge [
    source 13
    target 336
  ]
  edge [
    source 13
    target 337
  ]
  edge [
    source 13
    target 338
  ]
  edge [
    source 13
    target 339
  ]
  edge [
    source 13
    target 340
  ]
  edge [
    source 13
    target 341
  ]
  edge [
    source 13
    target 342
  ]
  edge [
    source 13
    target 343
  ]
  edge [
    source 13
    target 344
  ]
  edge [
    source 13
    target 345
  ]
  edge [
    source 13
    target 346
  ]
  edge [
    source 13
    target 347
  ]
  edge [
    source 13
    target 348
  ]
  edge [
    source 13
    target 349
  ]
  edge [
    source 13
    target 350
  ]
  edge [
    source 13
    target 351
  ]
  edge [
    source 13
    target 352
  ]
  edge [
    source 13
    target 353
  ]
  edge [
    source 13
    target 354
  ]
  edge [
    source 13
    target 355
  ]
  edge [
    source 13
    target 356
  ]
  edge [
    source 13
    target 357
  ]
  edge [
    source 13
    target 358
  ]
  edge [
    source 13
    target 359
  ]
  edge [
    source 13
    target 360
  ]
  edge [
    source 13
    target 361
  ]
  edge [
    source 13
    target 362
  ]
  edge [
    source 13
    target 363
  ]
  edge [
    source 13
    target 364
  ]
  edge [
    source 13
    target 365
  ]
  edge [
    source 13
    target 366
  ]
  edge [
    source 13
    target 367
  ]
  edge [
    source 13
    target 368
  ]
  edge [
    source 13
    target 369
  ]
  edge [
    source 13
    target 370
  ]
  edge [
    source 13
    target 371
  ]
  edge [
    source 13
    target 372
  ]
  edge [
    source 13
    target 373
  ]
  edge [
    source 13
    target 374
  ]
  edge [
    source 13
    target 375
  ]
  edge [
    source 13
    target 376
  ]
  edge [
    source 13
    target 377
  ]
  edge [
    source 13
    target 378
  ]
  edge [
    source 13
    target 379
  ]
  edge [
    source 13
    target 380
  ]
  edge [
    source 13
    target 381
  ]
  edge [
    source 13
    target 382
  ]
  edge [
    source 13
    target 383
  ]
  edge [
    source 13
    target 384
  ]
  edge [
    source 13
    target 385
  ]
  edge [
    source 13
    target 386
  ]
  edge [
    source 13
    target 387
  ]
  edge [
    source 13
    target 388
  ]
  edge [
    source 13
    target 389
  ]
  edge [
    source 13
    target 390
  ]
  edge [
    source 13
    target 391
  ]
  edge [
    source 13
    target 392
  ]
  edge [
    source 13
    target 393
  ]
  edge [
    source 13
    target 394
  ]
  edge [
    source 13
    target 395
  ]
  edge [
    source 13
    target 396
  ]
  edge [
    source 13
    target 397
  ]
  edge [
    source 13
    target 398
  ]
  edge [
    source 13
    target 399
  ]
  edge [
    source 13
    target 400
  ]
  edge [
    source 13
    target 401
  ]
  edge [
    source 13
    target 402
  ]
  edge [
    source 13
    target 403
  ]
  edge [
    source 13
    target 404
  ]
  edge [
    source 13
    target 405
  ]
  edge [
    source 13
    target 406
  ]
  edge [
    source 13
    target 407
  ]
  edge [
    source 13
    target 408
  ]
  edge [
    source 13
    target 409
  ]
  edge [
    source 13
    target 410
  ]
  edge [
    source 13
    target 411
  ]
  edge [
    source 13
    target 412
  ]
  edge [
    source 13
    target 413
  ]
  edge [
    source 13
    target 414
  ]
  edge [
    source 13
    target 415
  ]
  edge [
    source 13
    target 416
  ]
  edge [
    source 13
    target 417
  ]
  edge [
    source 13
    target 418
  ]
  edge [
    source 13
    target 419
  ]
  edge [
    source 13
    target 420
  ]
  edge [
    source 13
    target 421
  ]
  edge [
    source 13
    target 422
  ]
  edge [
    source 13
    target 423
  ]
  edge [
    source 13
    target 424
  ]
  edge [
    source 13
    target 425
  ]
  edge [
    source 13
    target 426
  ]
  edge [
    source 13
    target 427
  ]
  edge [
    source 13
    target 428
  ]
  edge [
    source 13
    target 429
  ]
  edge [
    source 13
    target 430
  ]
  edge [
    source 13
    target 431
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 432
  ]
  edge [
    source 15
    target 433
  ]
  edge [
    source 15
    target 434
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 435
  ]
  edge [
    source 16
    target 436
  ]
  edge [
    source 16
    target 437
  ]
  edge [
    source 16
    target 438
  ]
  edge [
    source 16
    target 439
  ]
  edge [
    source 16
    target 440
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 441
  ]
  edge [
    source 17
    target 442
  ]
  edge [
    source 17
    target 443
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 444
  ]
  edge [
    source 18
    target 445
  ]
  edge [
    source 18
    target 446
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 447
  ]
  edge [
    source 19
    target 448
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 449
  ]
  edge [
    source 21
    target 450
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 451
  ]
  edge [
    source 24
    target 452
  ]
  edge [
    source 24
    target 453
  ]
  edge [
    source 24
    target 454
  ]
  edge [
    source 24
    target 455
  ]
  edge [
    source 24
    target 456
  ]
  edge [
    source 24
    target 457
  ]
  edge [
    source 24
    target 458
  ]
  edge [
    source 24
    target 459
  ]
  edge [
    source 24
    target 460
  ]
  edge [
    source 24
    target 461
  ]
  edge [
    source 24
    target 462
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 463
  ]
  edge [
    source 25
    target 464
  ]
  edge [
    source 25
    target 465
  ]
  edge [
    source 25
    target 466
  ]
  edge [
    source 25
    target 51
  ]
  edge [
    source 25
    target 467
  ]
  edge [
    source 25
    target 468
  ]
  edge [
    source 25
    target 469
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 470
  ]
  edge [
    source 27
    target 471
  ]
  edge [
    source 27
    target 472
  ]
  edge [
    source 27
    target 473
  ]
  edge [
    source 27
    target 474
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 475
  ]
  edge [
    source 28
    target 476
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 477
  ]
  edge [
    source 29
    target 478
  ]
  edge [
    source 29
    target 99
  ]
  edge [
    source 29
    target 100
  ]
  edge [
    source 29
    target 101
  ]
  edge [
    source 29
    target 479
  ]
  edge [
    source 29
    target 102
  ]
  edge [
    source 29
    target 480
  ]
  edge [
    source 29
    target 103
  ]
  edge [
    source 29
    target 105
  ]
  edge [
    source 29
    target 481
  ]
  edge [
    source 29
    target 482
  ]
  edge [
    source 29
    target 106
  ]
  edge [
    source 29
    target 108
  ]
  edge [
    source 29
    target 483
  ]
  edge [
    source 30
    target 484
  ]
  edge [
    source 30
    target 485
  ]
  edge [
    source 30
    target 486
  ]
  edge [
    source 30
    target 487
  ]
  edge [
    source 30
    target 488
  ]
  edge [
    source 30
    target 489
  ]
  edge [
    source 30
    target 490
  ]
  edge [
    source 30
    target 491
  ]
  edge [
    source 30
    target 492
  ]
  edge [
    source 30
    target 493
  ]
  edge [
    source 30
    target 494
  ]
  edge [
    source 30
    target 495
  ]
  edge [
    source 30
    target 496
  ]
  edge [
    source 30
    target 497
  ]
  edge [
    source 30
    target 498
  ]
  edge [
    source 30
    target 499
  ]
  edge [
    source 30
    target 500
  ]
  edge [
    source 30
    target 501
  ]
  edge [
    source 30
    target 502
  ]
  edge [
    source 30
    target 473
  ]
  edge [
    source 30
    target 503
  ]
  edge [
    source 30
    target 504
  ]
  edge [
    source 30
    target 505
  ]
  edge [
    source 30
    target 506
  ]
  edge [
    source 30
    target 507
  ]
  edge [
    source 30
    target 508
  ]
  edge [
    source 509
    target 510
  ]
]
