graph [
  maxDegree 49
  minDegree 1
  meanDegree 2.2338072669826223
  density 0.003534505169276301
  graphCliqueNumber 4
  node [
    id 0
    label "dyrektor"
    origin "text"
  ]
  node [
    id 1
    label "szko&#322;a"
    origin "text"
  ]
  node [
    id 2
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 3
    label "zapomnia&#322;y"
    origin "text"
  ]
  node [
    id 4
    label "zam&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 5
    label "test"
    origin "text"
  ]
  node [
    id 6
    label "egzamin"
    origin "text"
  ]
  node [
    id 7
    label "ko&#324;cowy"
    origin "text"
  ]
  node [
    id 8
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 9
    label "ukara&#263;"
    origin "text"
  ]
  node [
    id 10
    label "wina"
    origin "text"
  ]
  node [
    id 11
    label "by&#263;"
    origin "text"
  ]
  node [
    id 12
    label "taka"
    origin "text"
  ]
  node [
    id 13
    label "zachodzi&#263;"
    origin "text"
  ]
  node [
    id 14
    label "przes&#322;anka"
    origin "text"
  ]
  node [
    id 15
    label "rozwi&#261;zanie"
    origin "text"
  ]
  node [
    id 16
    label "umowa"
    origin "text"
  ]
  node [
    id 17
    label "praca"
    origin "text"
  ]
  node [
    id 18
    label "wyci&#261;gn&#261;&#263;by&#263;"
    origin "text"
  ]
  node [
    id 19
    label "konsekwencja"
    origin "text"
  ]
  node [
    id 20
    label "dyscyplinarny"
    origin "text"
  ]
  node [
    id 21
    label "nie"
    origin "text"
  ]
  node [
    id 22
    label "szczeg&#243;&#322;"
    origin "text"
  ]
  node [
    id 23
    label "zabra&#263;"
    origin "text"
  ]
  node [
    id 24
    label "ustawa"
    origin "text"
  ]
  node [
    id 25
    label "ochrona"
    origin "text"
  ]
  node [
    id 26
    label "dana"
    origin "text"
  ]
  node [
    id 27
    label "osobowy"
    origin "text"
  ]
  node [
    id 28
    label "powiedzie&#263;"
    origin "text"
  ]
  node [
    id 29
    label "wczoraj"
    origin "text"
  ]
  node [
    id 30
    label "grzegorz"
    origin "text"
  ]
  node [
    id 31
    label "le&#347;niewicz"
    origin "text"
  ]
  node [
    id 32
    label "starosta"
    origin "text"
  ]
  node [
    id 33
    label "zgierski"
    origin "text"
  ]
  node [
    id 34
    label "podlega&#263;"
    origin "text"
  ]
  node [
    id 35
    label "ozorkowski"
    origin "text"
  ]
  node [
    id 36
    label "podawa&#263;"
    origin "text"
  ]
  node [
    id 37
    label "r&#243;wnie&#380;"
    origin "text"
  ]
  node [
    id 38
    label "dyrektorek"
    origin "text"
  ]
  node [
    id 39
    label "dorota"
    origin "text"
  ]
  node [
    id 40
    label "ryk"
    origin "text"
  ]
  node [
    id 41
    label "wasz"
    origin "text"
  ]
  node [
    id 42
    label "publikacja"
    origin "text"
  ]
  node [
    id 43
    label "zosta&#263;by&#263;"
    origin "text"
  ]
  node [
    id 44
    label "gwiazda"
    origin "text"
  ]
  node [
    id 45
    label "medialny"
    origin "text"
  ]
  node [
    id 46
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 47
    label "czyta&#263;"
    origin "text"
  ]
  node [
    id 48
    label "gazeta"
    origin "text"
  ]
  node [
    id 49
    label "informacja"
    origin "text"
  ]
  node [
    id 50
    label "siebie"
    origin "text"
  ]
  node [
    id 51
    label "wczesno"
    origin "text"
  ]
  node [
    id 52
    label "ba&#263;"
    origin "text"
  ]
  node [
    id 53
    label "si&#281;"
    origin "text"
  ]
  node [
    id 54
    label "zwolni&#263;"
    origin "text"
  ]
  node [
    id 55
    label "dlatego"
    origin "text"
  ]
  node [
    id 56
    label "jak"
    origin "text"
  ]
  node [
    id 57
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 58
    label "teraz"
    origin "text"
  ]
  node [
    id 59
    label "akceptowa&#263;"
    origin "text"
  ]
  node [
    id 60
    label "tajemniczy"
    origin "text"
  ]
  node [
    id 61
    label "kara"
    origin "text"
  ]
  node [
    id 62
    label "odwo&#322;ywa&#263;"
    origin "text"
  ]
  node [
    id 63
    label "tydzie&#324;"
    origin "text"
  ]
  node [
    id 64
    label "temu"
    origin "text"
  ]
  node [
    id 65
    label "napisa&#263;by&#263;"
    origin "text"
  ]
  node [
    id 66
    label "podstawowy"
    origin "text"
  ]
  node [
    id 67
    label "ozorkowie"
    origin "text"
  ]
  node [
    id 68
    label "kwiecie&#324;"
    origin "text"
  ]
  node [
    id 69
    label "pisa&#263;"
    origin "text"
  ]
  node [
    id 70
    label "sz&#243;stoklasista"
    origin "text"
  ]
  node [
    id 71
    label "ca&#322;y"
    origin "text"
  ]
  node [
    id 72
    label "polska"
    origin "text"
  ]
  node [
    id 73
    label "ten"
    origin "text"
  ]
  node [
    id 74
    label "wa&#380;ny"
    origin "text"
  ]
  node [
    id 75
    label "wydarzenie"
    origin "text"
  ]
  node [
    id 76
    label "podstaw&#243;wka"
    origin "text"
  ]
  node [
    id 77
    label "dawa&#263;"
    origin "text"
  ]
  node [
    id 78
    label "prawo"
    origin "text"
  ]
  node [
    id 79
    label "kontynuowa&#263;"
    origin "text"
  ]
  node [
    id 80
    label "nauka"
    origin "text"
  ]
  node [
    id 81
    label "gimnazjum"
    origin "text"
  ]
  node [
    id 82
    label "trzeba"
    origin "text"
  ]
  node [
    id 83
    label "okr&#281;gowy"
    origin "text"
  ]
  node [
    id 84
    label "komisja"
    origin "text"
  ]
  node [
    id 85
    label "egzaminacyjny"
    origin "text"
  ]
  node [
    id 86
    label "pa&#378;dziernik"
    origin "text"
  ]
  node [
    id 87
    label "rocznik"
    origin "text"
  ]
  node [
    id 88
    label "dyro"
  ]
  node [
    id 89
    label "dyrektoriat"
  ]
  node [
    id 90
    label "dyrygent"
  ]
  node [
    id 91
    label "Tadeusz_Rydzyk"
  ]
  node [
    id 92
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 93
    label "zwierzchnik"
  ]
  node [
    id 94
    label "Mickiewicz"
  ]
  node [
    id 95
    label "czas"
  ]
  node [
    id 96
    label "szkolenie"
  ]
  node [
    id 97
    label "przepisa&#263;"
  ]
  node [
    id 98
    label "lesson"
  ]
  node [
    id 99
    label "grupa"
  ]
  node [
    id 100
    label "praktyka"
  ]
  node [
    id 101
    label "metoda"
  ]
  node [
    id 102
    label "niepokalanki"
  ]
  node [
    id 103
    label "zda&#263;"
  ]
  node [
    id 104
    label "form"
  ]
  node [
    id 105
    label "kwalifikacje"
  ]
  node [
    id 106
    label "system"
  ]
  node [
    id 107
    label "przepisanie"
  ]
  node [
    id 108
    label "sztuba"
  ]
  node [
    id 109
    label "wiedza"
  ]
  node [
    id 110
    label "stopek"
  ]
  node [
    id 111
    label "school"
  ]
  node [
    id 112
    label "absolwent"
  ]
  node [
    id 113
    label "urszulanki"
  ]
  node [
    id 114
    label "gabinet"
  ]
  node [
    id 115
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 116
    label "ideologia"
  ]
  node [
    id 117
    label "lekcja"
  ]
  node [
    id 118
    label "muzyka"
  ]
  node [
    id 119
    label "podr&#281;cznik"
  ]
  node [
    id 120
    label "zdanie"
  ]
  node [
    id 121
    label "siedziba"
  ]
  node [
    id 122
    label "sekretariat"
  ]
  node [
    id 123
    label "do&#347;wiadczenie"
  ]
  node [
    id 124
    label "tablica"
  ]
  node [
    id 125
    label "teren_szko&#322;y"
  ]
  node [
    id 126
    label "instytucja"
  ]
  node [
    id 127
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 128
    label "skolaryzacja"
  ]
  node [
    id 129
    label "&#322;awa_szkolna"
  ]
  node [
    id 130
    label "klasa"
  ]
  node [
    id 131
    label "poleci&#263;"
  ]
  node [
    id 132
    label "zarezerwowa&#263;"
  ]
  node [
    id 133
    label "zamawianie"
  ]
  node [
    id 134
    label "zaczarowa&#263;"
  ]
  node [
    id 135
    label "order"
  ]
  node [
    id 136
    label "zam&#243;wienie"
  ]
  node [
    id 137
    label "bespeak"
  ]
  node [
    id 138
    label "indent"
  ]
  node [
    id 139
    label "indenture"
  ]
  node [
    id 140
    label "zamawia&#263;"
  ]
  node [
    id 141
    label "zg&#322;osi&#263;"
  ]
  node [
    id 142
    label "appoint"
  ]
  node [
    id 143
    label "um&#243;wi&#263;_si&#281;"
  ]
  node [
    id 144
    label "zleci&#263;"
  ]
  node [
    id 145
    label "arkusz"
  ]
  node [
    id 146
    label "sprawdzian"
  ]
  node [
    id 147
    label "quiz"
  ]
  node [
    id 148
    label "przechodzi&#263;"
  ]
  node [
    id 149
    label "przechodzenie"
  ]
  node [
    id 150
    label "badanie"
  ]
  node [
    id 151
    label "narz&#281;dzie"
  ]
  node [
    id 152
    label "sytuacja"
  ]
  node [
    id 153
    label "magiel"
  ]
  node [
    id 154
    label "faza"
  ]
  node [
    id 155
    label "praca_pisemna"
  ]
  node [
    id 156
    label "oblewanie"
  ]
  node [
    id 157
    label "examination"
  ]
  node [
    id 158
    label "pr&#243;ba"
  ]
  node [
    id 159
    label "oblewa&#263;"
  ]
  node [
    id 160
    label "sesja_egzaminacyjna"
  ]
  node [
    id 161
    label "ostatni"
  ]
  node [
    id 162
    label "ko&#324;cowo"
  ]
  node [
    id 163
    label "proceed"
  ]
  node [
    id 164
    label "catch"
  ]
  node [
    id 165
    label "pozosta&#263;"
  ]
  node [
    id 166
    label "osta&#263;_si&#281;"
  ]
  node [
    id 167
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 168
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 169
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 170
    label "change"
  ]
  node [
    id 171
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 172
    label "inflict"
  ]
  node [
    id 173
    label "spowodowa&#263;"
  ]
  node [
    id 174
    label "wstyd"
  ]
  node [
    id 175
    label "guilt"
  ]
  node [
    id 176
    label "lutnia"
  ]
  node [
    id 177
    label "si&#281;ga&#263;"
  ]
  node [
    id 178
    label "trwa&#263;"
  ]
  node [
    id 179
    label "obecno&#347;&#263;"
  ]
  node [
    id 180
    label "stan"
  ]
  node [
    id 181
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 182
    label "stand"
  ]
  node [
    id 183
    label "mie&#263;_miejsce"
  ]
  node [
    id 184
    label "uczestniczy&#263;"
  ]
  node [
    id 185
    label "chodzi&#263;"
  ]
  node [
    id 186
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 187
    label "equal"
  ]
  node [
    id 188
    label "Bangladesz"
  ]
  node [
    id 189
    label "jednostka_monetarna"
  ]
  node [
    id 190
    label "ukrywa&#263;_si&#281;"
  ]
  node [
    id 191
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 192
    label "foray"
  ]
  node [
    id 193
    label "reach"
  ]
  node [
    id 194
    label "przebiega&#263;"
  ]
  node [
    id 195
    label "wpada&#263;"
  ]
  node [
    id 196
    label "nak&#322;ada&#263;_si&#281;"
  ]
  node [
    id 197
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 198
    label "intervene"
  ]
  node [
    id 199
    label "pokrywa&#263;"
  ]
  node [
    id 200
    label "dochodzi&#263;"
  ]
  node [
    id 201
    label "zachodzi&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 202
    label "przys&#322;ania&#263;"
  ]
  node [
    id 203
    label "pokrywa&#263;_si&#281;"
  ]
  node [
    id 204
    label "wsp&#243;&#322;wyst&#281;powa&#263;"
  ]
  node [
    id 205
    label "istnie&#263;"
  ]
  node [
    id 206
    label "podchodzi&#263;"
  ]
  node [
    id 207
    label "s&#261;d"
  ]
  node [
    id 208
    label "wnioskowanie"
  ]
  node [
    id 209
    label "przyczyna"
  ]
  node [
    id 210
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 211
    label "proces"
  ]
  node [
    id 212
    label "fakt"
  ]
  node [
    id 213
    label "wynik"
  ]
  node [
    id 214
    label "wyj&#347;cie"
  ]
  node [
    id 215
    label "spe&#322;nienie"
  ]
  node [
    id 216
    label "cesarskie_ci&#281;cie"
  ]
  node [
    id 217
    label "po&#322;o&#380;na"
  ]
  node [
    id 218
    label "proces_fizjologiczny"
  ]
  node [
    id 219
    label "przestanie"
  ]
  node [
    id 220
    label "marc&#243;wka"
  ]
  node [
    id 221
    label "usuni&#281;cie"
  ]
  node [
    id 222
    label "uniewa&#380;nienie"
  ]
  node [
    id 223
    label "pomys&#322;"
  ]
  node [
    id 224
    label "birth"
  ]
  node [
    id 225
    label "wymy&#347;lenie"
  ]
  node [
    id 226
    label "po&#322;&#243;g"
  ]
  node [
    id 227
    label "szok_poporodowy"
  ]
  node [
    id 228
    label "event"
  ]
  node [
    id 229
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 230
    label "spos&#243;b"
  ]
  node [
    id 231
    label "rozwi&#261;zywanie_si&#281;"
  ]
  node [
    id 232
    label "dula"
  ]
  node [
    id 233
    label "czyn"
  ]
  node [
    id 234
    label "contract"
  ]
  node [
    id 235
    label "gestia_transportowa"
  ]
  node [
    id 236
    label "zawrze&#263;"
  ]
  node [
    id 237
    label "klauzula"
  ]
  node [
    id 238
    label "porozumienie"
  ]
  node [
    id 239
    label "warunek"
  ]
  node [
    id 240
    label "zawarcie"
  ]
  node [
    id 241
    label "stosunek_pracy"
  ]
  node [
    id 242
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 243
    label "benedykty&#324;ski"
  ]
  node [
    id 244
    label "pracowanie"
  ]
  node [
    id 245
    label "zaw&#243;d"
  ]
  node [
    id 246
    label "kierownictwo"
  ]
  node [
    id 247
    label "zmiana"
  ]
  node [
    id 248
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 249
    label "wytw&#243;r"
  ]
  node [
    id 250
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 251
    label "tynkarski"
  ]
  node [
    id 252
    label "czynnik_produkcji"
  ]
  node [
    id 253
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 254
    label "zobowi&#261;zanie"
  ]
  node [
    id 255
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 256
    label "czynno&#347;&#263;"
  ]
  node [
    id 257
    label "tyrka"
  ]
  node [
    id 258
    label "pracowa&#263;"
  ]
  node [
    id 259
    label "poda&#380;_pracy"
  ]
  node [
    id 260
    label "miejsce"
  ]
  node [
    id 261
    label "zak&#322;ad"
  ]
  node [
    id 262
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 263
    label "najem"
  ]
  node [
    id 264
    label "zwi&#261;zek_przyczynowy"
  ]
  node [
    id 265
    label "skrupia&#263;_si&#281;"
  ]
  node [
    id 266
    label "skrupianie_si&#281;"
  ]
  node [
    id 267
    label "odczuwanie"
  ]
  node [
    id 268
    label "skrupienie_si&#281;"
  ]
  node [
    id 269
    label "skrupi&#263;_si&#281;"
  ]
  node [
    id 270
    label "odczucie"
  ]
  node [
    id 271
    label "odczuwa&#263;"
  ]
  node [
    id 272
    label "odczu&#263;"
  ]
  node [
    id 273
    label "rezultat"
  ]
  node [
    id 274
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 275
    label "koszula_Dejaniry"
  ]
  node [
    id 276
    label "dyscyplinarnie"
  ]
  node [
    id 277
    label "sprzeciw"
  ]
  node [
    id 278
    label "niuansowa&#263;_si&#281;"
  ]
  node [
    id 279
    label "niuansowa&#263;"
  ]
  node [
    id 280
    label "zniuansowa&#263;"
  ]
  node [
    id 281
    label "element"
  ]
  node [
    id 282
    label "sk&#322;adnik"
  ]
  node [
    id 283
    label "zniuansowa&#263;_si&#281;"
  ]
  node [
    id 284
    label "consume"
  ]
  node [
    id 285
    label "zaj&#261;&#263;"
  ]
  node [
    id 286
    label "wzi&#261;&#263;"
  ]
  node [
    id 287
    label "przenie&#347;&#263;"
  ]
  node [
    id 288
    label "z&#322;apa&#263;"
  ]
  node [
    id 289
    label "przesun&#261;&#263;"
  ]
  node [
    id 290
    label "deprive"
  ]
  node [
    id 291
    label "uda&#263;_si&#281;"
  ]
  node [
    id 292
    label "poci&#261;gn&#261;&#263;"
  ]
  node [
    id 293
    label "abstract"
  ]
  node [
    id 294
    label "withdraw"
  ]
  node [
    id 295
    label "doprowadzi&#263;"
  ]
  node [
    id 296
    label "Karta_Nauczyciela"
  ]
  node [
    id 297
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 298
    label "akt"
  ]
  node [
    id 299
    label "przej&#347;&#263;"
  ]
  node [
    id 300
    label "charter"
  ]
  node [
    id 301
    label "przej&#347;cie"
  ]
  node [
    id 302
    label "chemical_bond"
  ]
  node [
    id 303
    label "tarcza"
  ]
  node [
    id 304
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 305
    label "obiekt"
  ]
  node [
    id 306
    label "Stra&#380;_Ochrony_Kolei"
  ]
  node [
    id 307
    label "borowiec"
  ]
  node [
    id 308
    label "obstawienie"
  ]
  node [
    id 309
    label "formacja"
  ]
  node [
    id 310
    label "ubezpieczenie"
  ]
  node [
    id 311
    label "obstawia&#263;"
  ]
  node [
    id 312
    label "obstawianie"
  ]
  node [
    id 313
    label "transportacja"
  ]
  node [
    id 314
    label "dar"
  ]
  node [
    id 315
    label "cnota"
  ]
  node [
    id 316
    label "buddyzm"
  ]
  node [
    id 317
    label "osobowo"
  ]
  node [
    id 318
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 319
    label "express"
  ]
  node [
    id 320
    label "rzekn&#261;&#263;"
  ]
  node [
    id 321
    label "okre&#347;li&#263;"
  ]
  node [
    id 322
    label "wyrazi&#263;"
  ]
  node [
    id 323
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 324
    label "unwrap"
  ]
  node [
    id 325
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 326
    label "convey"
  ]
  node [
    id 327
    label "discover"
  ]
  node [
    id 328
    label "wydoby&#263;"
  ]
  node [
    id 329
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 330
    label "poda&#263;"
  ]
  node [
    id 331
    label "doba"
  ]
  node [
    id 332
    label "dawno"
  ]
  node [
    id 333
    label "niedawno"
  ]
  node [
    id 334
    label "przedstawiciel"
  ]
  node [
    id 335
    label "w&#322;odarz"
  ]
  node [
    id 336
    label "podstaro&#347;ci"
  ]
  node [
    id 337
    label "burgrabia"
  ]
  node [
    id 338
    label "urz&#281;dnik"
  ]
  node [
    id 339
    label "samorz&#261;dowiec"
  ]
  node [
    id 340
    label "czu&#263;"
  ]
  node [
    id 341
    label "zale&#380;e&#263;"
  ]
  node [
    id 342
    label "doznawa&#263;"
  ]
  node [
    id 343
    label "tenis"
  ]
  node [
    id 344
    label "cover"
  ]
  node [
    id 345
    label "siatk&#243;wka"
  ]
  node [
    id 346
    label "faszerowa&#263;"
  ]
  node [
    id 347
    label "informowa&#263;"
  ]
  node [
    id 348
    label "introduce"
  ]
  node [
    id 349
    label "jedzenie"
  ]
  node [
    id 350
    label "tender"
  ]
  node [
    id 351
    label "deal"
  ]
  node [
    id 352
    label "kelner"
  ]
  node [
    id 353
    label "serwowa&#263;"
  ]
  node [
    id 354
    label "rozgrywa&#263;"
  ]
  node [
    id 355
    label "stawia&#263;"
  ]
  node [
    id 356
    label "p&#322;acz"
  ]
  node [
    id 357
    label "gwa&#322;towno&#347;&#263;"
  ]
  node [
    id 358
    label "gust"
  ]
  node [
    id 359
    label "d&#378;wi&#281;k"
  ]
  node [
    id 360
    label "krzyk"
  ]
  node [
    id 361
    label "zjawisko"
  ]
  node [
    id 362
    label "&#347;miech"
  ]
  node [
    id 363
    label "czyj&#347;"
  ]
  node [
    id 364
    label "produkcja"
  ]
  node [
    id 365
    label "druk"
  ]
  node [
    id 366
    label "notification"
  ]
  node [
    id 367
    label "tekst"
  ]
  node [
    id 368
    label "S&#322;o&#324;ce"
  ]
  node [
    id 369
    label "uk&#322;ad_gwiazd"
  ]
  node [
    id 370
    label "Arktur"
  ]
  node [
    id 371
    label "star"
  ]
  node [
    id 372
    label "delta_Scuti"
  ]
  node [
    id 373
    label "agregatka"
  ]
  node [
    id 374
    label "s&#322;awa"
  ]
  node [
    id 375
    label "zlot_gwia&#378;dzisty"
  ]
  node [
    id 376
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 377
    label "gwiazdosz"
  ]
  node [
    id 378
    label "&#347;wiat&#322;o"
  ]
  node [
    id 379
    label "Nibiru"
  ]
  node [
    id 380
    label "ornament"
  ]
  node [
    id 381
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 382
    label "Gwiazda_Polarna"
  ]
  node [
    id 383
    label "kszta&#322;t"
  ]
  node [
    id 384
    label "supergrupa"
  ]
  node [
    id 385
    label "gromada"
  ]
  node [
    id 386
    label "promie&#324;"
  ]
  node [
    id 387
    label "konstelacja"
  ]
  node [
    id 388
    label "asocjacja_gwiazd"
  ]
  node [
    id 389
    label "nieprawdziwy"
  ]
  node [
    id 390
    label "medialnie"
  ]
  node [
    id 391
    label "popularny"
  ]
  node [
    id 392
    label "&#347;rodkowy"
  ]
  node [
    id 393
    label "dysleksja"
  ]
  node [
    id 394
    label "umie&#263;"
  ]
  node [
    id 395
    label "wr&#243;&#380;y&#263;"
  ]
  node [
    id 396
    label "przetwarza&#263;"
  ]
  node [
    id 397
    label "read"
  ]
  node [
    id 398
    label "poznawa&#263;"
  ]
  node [
    id 399
    label "obserwowa&#263;"
  ]
  node [
    id 400
    label "odczytywa&#263;"
  ]
  node [
    id 401
    label "prasa"
  ]
  node [
    id 402
    label "redakcja"
  ]
  node [
    id 403
    label "tytu&#322;"
  ]
  node [
    id 404
    label "rozk&#322;ad&#243;wka"
  ]
  node [
    id 405
    label "czasopismo"
  ]
  node [
    id 406
    label "doj&#347;cie"
  ]
  node [
    id 407
    label "doj&#347;&#263;"
  ]
  node [
    id 408
    label "powzi&#261;&#263;"
  ]
  node [
    id 409
    label "sygna&#322;"
  ]
  node [
    id 410
    label "obiegni&#281;cie"
  ]
  node [
    id 411
    label "obieganie"
  ]
  node [
    id 412
    label "obiec"
  ]
  node [
    id 413
    label "dane"
  ]
  node [
    id 414
    label "obiega&#263;"
  ]
  node [
    id 415
    label "punkt"
  ]
  node [
    id 416
    label "powzi&#281;cie"
  ]
  node [
    id 417
    label "wcze&#347;nie"
  ]
  node [
    id 418
    label "sta&#263;_si&#281;"
  ]
  node [
    id 419
    label "advise"
  ]
  node [
    id 420
    label "render"
  ]
  node [
    id 421
    label "wypu&#347;ci&#263;"
  ]
  node [
    id 422
    label "uprzedzi&#263;"
  ]
  node [
    id 423
    label "oddali&#263;"
  ]
  node [
    id 424
    label "wyla&#263;"
  ]
  node [
    id 425
    label "da&#263;_spok&#243;j"
  ]
  node [
    id 426
    label "wypowiedzie&#263;"
  ]
  node [
    id 427
    label "sprawi&#263;"
  ]
  node [
    id 428
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 429
    label "usprawiedliwi&#263;"
  ]
  node [
    id 430
    label "poluzowa&#263;"
  ]
  node [
    id 431
    label "deliver"
  ]
  node [
    id 432
    label "byd&#322;o"
  ]
  node [
    id 433
    label "zobo"
  ]
  node [
    id 434
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 435
    label "yakalo"
  ]
  node [
    id 436
    label "dzo"
  ]
  node [
    id 437
    label "remark"
  ]
  node [
    id 438
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 439
    label "u&#380;ywa&#263;"
  ]
  node [
    id 440
    label "okre&#347;la&#263;"
  ]
  node [
    id 441
    label "j&#281;zyk"
  ]
  node [
    id 442
    label "say"
  ]
  node [
    id 443
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 444
    label "formu&#322;owa&#263;"
  ]
  node [
    id 445
    label "talk"
  ]
  node [
    id 446
    label "powiada&#263;"
  ]
  node [
    id 447
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 448
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 449
    label "wydobywa&#263;"
  ]
  node [
    id 450
    label "chew_the_fat"
  ]
  node [
    id 451
    label "dysfonia"
  ]
  node [
    id 452
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 453
    label "tell"
  ]
  node [
    id 454
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 455
    label "wyra&#380;a&#263;"
  ]
  node [
    id 456
    label "gaworzy&#263;"
  ]
  node [
    id 457
    label "rozmawia&#263;"
  ]
  node [
    id 458
    label "dziama&#263;"
  ]
  node [
    id 459
    label "prawi&#263;"
  ]
  node [
    id 460
    label "chwila"
  ]
  node [
    id 461
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 462
    label "zezwala&#263;"
  ]
  node [
    id 463
    label "godzi&#263;_si&#281;"
  ]
  node [
    id 464
    label "assent"
  ]
  node [
    id 465
    label "authorize"
  ]
  node [
    id 466
    label "uznawa&#263;"
  ]
  node [
    id 467
    label "ciekawy"
  ]
  node [
    id 468
    label "dziwny"
  ]
  node [
    id 469
    label "nastrojowy"
  ]
  node [
    id 470
    label "nieznany"
  ]
  node [
    id 471
    label "intryguj&#261;cy"
  ]
  node [
    id 472
    label "tajemniczo"
  ]
  node [
    id 473
    label "skryty"
  ]
  node [
    id 474
    label "niejednoznaczny"
  ]
  node [
    id 475
    label "klacz"
  ]
  node [
    id 476
    label "punishment"
  ]
  node [
    id 477
    label "forfeit"
  ]
  node [
    id 478
    label "roboty_przymusowe"
  ]
  node [
    id 479
    label "nemezis"
  ]
  node [
    id 480
    label "kwota"
  ]
  node [
    id 481
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 482
    label "zwalnia&#263;"
  ]
  node [
    id 483
    label "seclude"
  ]
  node [
    id 484
    label "uniewa&#380;nia&#263;"
  ]
  node [
    id 485
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 486
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 487
    label "weekend"
  ]
  node [
    id 488
    label "miesi&#261;c"
  ]
  node [
    id 489
    label "pocz&#261;tkowy"
  ]
  node [
    id 490
    label "podstawowo"
  ]
  node [
    id 491
    label "najwa&#380;niejszy"
  ]
  node [
    id 492
    label "niezaawansowany"
  ]
  node [
    id 493
    label "ozdabia&#263;"
  ]
  node [
    id 494
    label "dysgrafia"
  ]
  node [
    id 495
    label "spell"
  ]
  node [
    id 496
    label "skryba"
  ]
  node [
    id 497
    label "donosi&#263;"
  ]
  node [
    id 498
    label "code"
  ]
  node [
    id 499
    label "dysortografia"
  ]
  node [
    id 500
    label "tworzy&#263;"
  ]
  node [
    id 501
    label "styl"
  ]
  node [
    id 502
    label "dziecko"
  ]
  node [
    id 503
    label "ucze&#324;"
  ]
  node [
    id 504
    label "du&#380;y"
  ]
  node [
    id 505
    label "jedyny"
  ]
  node [
    id 506
    label "kompletny"
  ]
  node [
    id 507
    label "zdr&#243;w"
  ]
  node [
    id 508
    label "&#380;ywy"
  ]
  node [
    id 509
    label "ca&#322;o"
  ]
  node [
    id 510
    label "pe&#322;ny"
  ]
  node [
    id 511
    label "calu&#347;ko"
  ]
  node [
    id 512
    label "podobny"
  ]
  node [
    id 513
    label "okre&#347;lony"
  ]
  node [
    id 514
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 515
    label "silny"
  ]
  node [
    id 516
    label "wa&#380;nie"
  ]
  node [
    id 517
    label "eksponowany"
  ]
  node [
    id 518
    label "istotnie"
  ]
  node [
    id 519
    label "znaczny"
  ]
  node [
    id 520
    label "dobry"
  ]
  node [
    id 521
    label "wynios&#322;y"
  ]
  node [
    id 522
    label "dono&#347;ny"
  ]
  node [
    id 523
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 524
    label "motyw"
  ]
  node [
    id 525
    label "fabu&#322;a"
  ]
  node [
    id 526
    label "przebiec"
  ]
  node [
    id 527
    label "przebiegni&#281;cie"
  ]
  node [
    id 528
    label "charakter"
  ]
  node [
    id 529
    label "powszechniak"
  ]
  node [
    id 530
    label "wykszta&#322;cenie_podstawowe"
  ]
  node [
    id 531
    label "hold"
  ]
  node [
    id 532
    label "surrender"
  ]
  node [
    id 533
    label "traktowa&#263;"
  ]
  node [
    id 534
    label "dostarcza&#263;"
  ]
  node [
    id 535
    label "train"
  ]
  node [
    id 536
    label "give"
  ]
  node [
    id 537
    label "umieszcza&#263;"
  ]
  node [
    id 538
    label "nalewa&#263;"
  ]
  node [
    id 539
    label "przeznacza&#263;"
  ]
  node [
    id 540
    label "p&#322;aci&#263;"
  ]
  node [
    id 541
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 542
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 543
    label "powierza&#263;"
  ]
  node [
    id 544
    label "hold_out"
  ]
  node [
    id 545
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 546
    label "wyrzeka&#263;_si&#281;"
  ]
  node [
    id 547
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 548
    label "robi&#263;"
  ]
  node [
    id 549
    label "t&#322;uc"
  ]
  node [
    id 550
    label "wpiernicza&#263;"
  ]
  node [
    id 551
    label "przekazywa&#263;"
  ]
  node [
    id 552
    label "puszcza&#263;_si&#281;"
  ]
  node [
    id 553
    label "rap"
  ]
  node [
    id 554
    label "obiecywa&#263;"
  ]
  node [
    id 555
    label "&#322;adowa&#263;"
  ]
  node [
    id 556
    label "odst&#281;powa&#263;"
  ]
  node [
    id 557
    label "exsert"
  ]
  node [
    id 558
    label "obserwacja"
  ]
  node [
    id 559
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 560
    label "nauka_prawa"
  ]
  node [
    id 561
    label "dominion"
  ]
  node [
    id 562
    label "normatywizm"
  ]
  node [
    id 563
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 564
    label "qualification"
  ]
  node [
    id 565
    label "opis"
  ]
  node [
    id 566
    label "regu&#322;a_Allena"
  ]
  node [
    id 567
    label "normalizacja"
  ]
  node [
    id 568
    label "kazuistyka"
  ]
  node [
    id 569
    label "regu&#322;a_Glogera"
  ]
  node [
    id 570
    label "kultura_duchowa"
  ]
  node [
    id 571
    label "prawo_karne"
  ]
  node [
    id 572
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 573
    label "standard"
  ]
  node [
    id 574
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 575
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 576
    label "struktura"
  ]
  node [
    id 577
    label "prawo_karne_procesowe"
  ]
  node [
    id 578
    label "prawo_Mendla"
  ]
  node [
    id 579
    label "przepis"
  ]
  node [
    id 580
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 581
    label "criterion"
  ]
  node [
    id 582
    label "kanonistyka"
  ]
  node [
    id 583
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 584
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 585
    label "wykonawczy"
  ]
  node [
    id 586
    label "twierdzenie"
  ]
  node [
    id 587
    label "judykatura"
  ]
  node [
    id 588
    label "legislacyjnie"
  ]
  node [
    id 589
    label "umocowa&#263;"
  ]
  node [
    id 590
    label "podmiot"
  ]
  node [
    id 591
    label "procesualistyka"
  ]
  node [
    id 592
    label "kierunek"
  ]
  node [
    id 593
    label "kryminologia"
  ]
  node [
    id 594
    label "kryminalistyka"
  ]
  node [
    id 595
    label "cywilistyka"
  ]
  node [
    id 596
    label "law"
  ]
  node [
    id 597
    label "zasada_d'Alemberta"
  ]
  node [
    id 598
    label "jurisprudence"
  ]
  node [
    id 599
    label "zasada"
  ]
  node [
    id 600
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 601
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 602
    label "prosecute"
  ]
  node [
    id 603
    label "nauki_o_Ziemi"
  ]
  node [
    id 604
    label "teoria_naukowa"
  ]
  node [
    id 605
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 606
    label "nauki_o_poznaniu"
  ]
  node [
    id 607
    label "nomotetyczny"
  ]
  node [
    id 608
    label "metodologia"
  ]
  node [
    id 609
    label "przem&#243;wienie"
  ]
  node [
    id 610
    label "nauki_penalne"
  ]
  node [
    id 611
    label "systematyka"
  ]
  node [
    id 612
    label "inwentyka"
  ]
  node [
    id 613
    label "dziedzina"
  ]
  node [
    id 614
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 615
    label "miasteczko_rowerowe"
  ]
  node [
    id 616
    label "fotowoltaika"
  ]
  node [
    id 617
    label "porada"
  ]
  node [
    id 618
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 619
    label "imagineskopia"
  ]
  node [
    id 620
    label "typologia"
  ]
  node [
    id 621
    label "szko&#322;a_ponadpodstawowa"
  ]
  node [
    id 622
    label "trza"
  ]
  node [
    id 623
    label "necessity"
  ]
  node [
    id 624
    label "obrady"
  ]
  node [
    id 625
    label "zesp&#243;&#322;"
  ]
  node [
    id 626
    label "organ"
  ]
  node [
    id 627
    label "Komisja_Europejska"
  ]
  node [
    id 628
    label "podkomisja"
  ]
  node [
    id 629
    label "Dzie&#324;_Papieski"
  ]
  node [
    id 630
    label "oktober"
  ]
  node [
    id 631
    label "kronika"
  ]
  node [
    id 632
    label "yearbook"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 11
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 161
  ]
  edge [
    source 7
    target 162
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 163
  ]
  edge [
    source 8
    target 164
  ]
  edge [
    source 8
    target 165
  ]
  edge [
    source 8
    target 166
  ]
  edge [
    source 8
    target 167
  ]
  edge [
    source 8
    target 168
  ]
  edge [
    source 8
    target 169
  ]
  edge [
    source 8
    target 170
  ]
  edge [
    source 8
    target 171
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 37
  ]
  edge [
    source 9
    target 38
  ]
  edge [
    source 9
    target 172
  ]
  edge [
    source 9
    target 173
  ]
  edge [
    source 9
    target 15
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 174
  ]
  edge [
    source 10
    target 19
  ]
  edge [
    source 10
    target 175
  ]
  edge [
    source 10
    target 176
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 61
  ]
  edge [
    source 11
    target 53
  ]
  edge [
    source 11
    target 82
  ]
  edge [
    source 11
    target 177
  ]
  edge [
    source 11
    target 178
  ]
  edge [
    source 11
    target 179
  ]
  edge [
    source 11
    target 180
  ]
  edge [
    source 11
    target 181
  ]
  edge [
    source 11
    target 182
  ]
  edge [
    source 11
    target 183
  ]
  edge [
    source 11
    target 184
  ]
  edge [
    source 11
    target 185
  ]
  edge [
    source 11
    target 186
  ]
  edge [
    source 11
    target 187
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 188
  ]
  edge [
    source 12
    target 189
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 190
  ]
  edge [
    source 13
    target 191
  ]
  edge [
    source 13
    target 192
  ]
  edge [
    source 13
    target 193
  ]
  edge [
    source 13
    target 183
  ]
  edge [
    source 13
    target 194
  ]
  edge [
    source 13
    target 195
  ]
  edge [
    source 13
    target 196
  ]
  edge [
    source 13
    target 197
  ]
  edge [
    source 13
    target 198
  ]
  edge [
    source 13
    target 199
  ]
  edge [
    source 13
    target 200
  ]
  edge [
    source 13
    target 201
  ]
  edge [
    source 13
    target 202
  ]
  edge [
    source 13
    target 203
  ]
  edge [
    source 13
    target 204
  ]
  edge [
    source 13
    target 205
  ]
  edge [
    source 13
    target 206
  ]
  edge [
    source 13
    target 45
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 207
  ]
  edge [
    source 14
    target 208
  ]
  edge [
    source 14
    target 209
  ]
  edge [
    source 14
    target 210
  ]
  edge [
    source 14
    target 211
  ]
  edge [
    source 14
    target 212
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 213
  ]
  edge [
    source 15
    target 214
  ]
  edge [
    source 15
    target 215
  ]
  edge [
    source 15
    target 216
  ]
  edge [
    source 15
    target 217
  ]
  edge [
    source 15
    target 218
  ]
  edge [
    source 15
    target 219
  ]
  edge [
    source 15
    target 220
  ]
  edge [
    source 15
    target 221
  ]
  edge [
    source 15
    target 222
  ]
  edge [
    source 15
    target 223
  ]
  edge [
    source 15
    target 224
  ]
  edge [
    source 15
    target 225
  ]
  edge [
    source 15
    target 226
  ]
  edge [
    source 15
    target 227
  ]
  edge [
    source 15
    target 228
  ]
  edge [
    source 15
    target 229
  ]
  edge [
    source 15
    target 230
  ]
  edge [
    source 15
    target 231
  ]
  edge [
    source 15
    target 232
  ]
  edge [
    source 15
    target 67
  ]
  edge [
    source 15
    target 74
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 233
  ]
  edge [
    source 16
    target 234
  ]
  edge [
    source 16
    target 235
  ]
  edge [
    source 16
    target 236
  ]
  edge [
    source 16
    target 237
  ]
  edge [
    source 16
    target 238
  ]
  edge [
    source 16
    target 239
  ]
  edge [
    source 16
    target 240
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 241
  ]
  edge [
    source 17
    target 242
  ]
  edge [
    source 17
    target 243
  ]
  edge [
    source 17
    target 244
  ]
  edge [
    source 17
    target 245
  ]
  edge [
    source 17
    target 246
  ]
  edge [
    source 17
    target 247
  ]
  edge [
    source 17
    target 248
  ]
  edge [
    source 17
    target 249
  ]
  edge [
    source 17
    target 250
  ]
  edge [
    source 17
    target 251
  ]
  edge [
    source 17
    target 252
  ]
  edge [
    source 17
    target 253
  ]
  edge [
    source 17
    target 254
  ]
  edge [
    source 17
    target 255
  ]
  edge [
    source 17
    target 256
  ]
  edge [
    source 17
    target 257
  ]
  edge [
    source 17
    target 258
  ]
  edge [
    source 17
    target 121
  ]
  edge [
    source 17
    target 259
  ]
  edge [
    source 17
    target 260
  ]
  edge [
    source 17
    target 261
  ]
  edge [
    source 17
    target 262
  ]
  edge [
    source 17
    target 263
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 264
  ]
  edge [
    source 19
    target 265
  ]
  edge [
    source 19
    target 266
  ]
  edge [
    source 19
    target 267
  ]
  edge [
    source 19
    target 268
  ]
  edge [
    source 19
    target 269
  ]
  edge [
    source 19
    target 270
  ]
  edge [
    source 19
    target 271
  ]
  edge [
    source 19
    target 272
  ]
  edge [
    source 19
    target 228
  ]
  edge [
    source 19
    target 273
  ]
  edge [
    source 19
    target 274
  ]
  edge [
    source 19
    target 275
  ]
  edge [
    source 19
    target 61
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 276
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 45
  ]
  edge [
    source 21
    target 46
  ]
  edge [
    source 21
    target 277
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 36
  ]
  edge [
    source 22
    target 278
  ]
  edge [
    source 22
    target 279
  ]
  edge [
    source 22
    target 280
  ]
  edge [
    source 22
    target 281
  ]
  edge [
    source 22
    target 282
  ]
  edge [
    source 22
    target 283
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 284
  ]
  edge [
    source 23
    target 285
  ]
  edge [
    source 23
    target 286
  ]
  edge [
    source 23
    target 287
  ]
  edge [
    source 23
    target 173
  ]
  edge [
    source 23
    target 288
  ]
  edge [
    source 23
    target 289
  ]
  edge [
    source 23
    target 290
  ]
  edge [
    source 23
    target 291
  ]
  edge [
    source 23
    target 292
  ]
  edge [
    source 23
    target 293
  ]
  edge [
    source 23
    target 294
  ]
  edge [
    source 23
    target 295
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 296
  ]
  edge [
    source 24
    target 220
  ]
  edge [
    source 24
    target 297
  ]
  edge [
    source 24
    target 298
  ]
  edge [
    source 24
    target 299
  ]
  edge [
    source 24
    target 300
  ]
  edge [
    source 24
    target 301
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 302
  ]
  edge [
    source 25
    target 303
  ]
  edge [
    source 25
    target 304
  ]
  edge [
    source 25
    target 305
  ]
  edge [
    source 25
    target 306
  ]
  edge [
    source 25
    target 307
  ]
  edge [
    source 25
    target 308
  ]
  edge [
    source 25
    target 309
  ]
  edge [
    source 25
    target 310
  ]
  edge [
    source 25
    target 311
  ]
  edge [
    source 25
    target 312
  ]
  edge [
    source 25
    target 313
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 314
  ]
  edge [
    source 26
    target 315
  ]
  edge [
    source 26
    target 316
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 317
  ]
  edge [
    source 27
    target 57
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 318
  ]
  edge [
    source 28
    target 319
  ]
  edge [
    source 28
    target 320
  ]
  edge [
    source 28
    target 321
  ]
  edge [
    source 28
    target 322
  ]
  edge [
    source 28
    target 323
  ]
  edge [
    source 28
    target 324
  ]
  edge [
    source 28
    target 325
  ]
  edge [
    source 28
    target 326
  ]
  edge [
    source 28
    target 327
  ]
  edge [
    source 28
    target 328
  ]
  edge [
    source 28
    target 329
  ]
  edge [
    source 28
    target 330
  ]
  edge [
    source 28
    target 42
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 331
  ]
  edge [
    source 29
    target 332
  ]
  edge [
    source 29
    target 333
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 334
  ]
  edge [
    source 32
    target 335
  ]
  edge [
    source 32
    target 336
  ]
  edge [
    source 32
    target 337
  ]
  edge [
    source 32
    target 338
  ]
  edge [
    source 32
    target 339
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 340
  ]
  edge [
    source 34
    target 341
  ]
  edge [
    source 34
    target 342
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 343
  ]
  edge [
    source 36
    target 344
  ]
  edge [
    source 36
    target 345
  ]
  edge [
    source 36
    target 77
  ]
  edge [
    source 36
    target 346
  ]
  edge [
    source 36
    target 347
  ]
  edge [
    source 36
    target 348
  ]
  edge [
    source 36
    target 349
  ]
  edge [
    source 36
    target 350
  ]
  edge [
    source 36
    target 351
  ]
  edge [
    source 36
    target 352
  ]
  edge [
    source 36
    target 353
  ]
  edge [
    source 36
    target 354
  ]
  edge [
    source 36
    target 355
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 80
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 356
  ]
  edge [
    source 40
    target 357
  ]
  edge [
    source 40
    target 358
  ]
  edge [
    source 40
    target 359
  ]
  edge [
    source 40
    target 360
  ]
  edge [
    source 40
    target 361
  ]
  edge [
    source 40
    target 362
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 363
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 364
  ]
  edge [
    source 42
    target 365
  ]
  edge [
    source 42
    target 366
  ]
  edge [
    source 42
    target 367
  ]
  edge [
    source 42
    target 49
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 368
  ]
  edge [
    source 44
    target 369
  ]
  edge [
    source 44
    target 370
  ]
  edge [
    source 44
    target 371
  ]
  edge [
    source 44
    target 372
  ]
  edge [
    source 44
    target 373
  ]
  edge [
    source 44
    target 374
  ]
  edge [
    source 44
    target 375
  ]
  edge [
    source 44
    target 376
  ]
  edge [
    source 44
    target 377
  ]
  edge [
    source 44
    target 378
  ]
  edge [
    source 44
    target 379
  ]
  edge [
    source 44
    target 380
  ]
  edge [
    source 44
    target 381
  ]
  edge [
    source 44
    target 382
  ]
  edge [
    source 44
    target 383
  ]
  edge [
    source 44
    target 384
  ]
  edge [
    source 44
    target 385
  ]
  edge [
    source 44
    target 305
  ]
  edge [
    source 44
    target 386
  ]
  edge [
    source 44
    target 387
  ]
  edge [
    source 44
    target 388
  ]
  edge [
    source 45
    target 389
  ]
  edge [
    source 45
    target 390
  ]
  edge [
    source 45
    target 391
  ]
  edge [
    source 45
    target 392
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 393
  ]
  edge [
    source 47
    target 394
  ]
  edge [
    source 47
    target 395
  ]
  edge [
    source 47
    target 396
  ]
  edge [
    source 47
    target 397
  ]
  edge [
    source 47
    target 398
  ]
  edge [
    source 47
    target 399
  ]
  edge [
    source 47
    target 400
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 401
  ]
  edge [
    source 48
    target 402
  ]
  edge [
    source 48
    target 403
  ]
  edge [
    source 48
    target 404
  ]
  edge [
    source 48
    target 405
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 406
  ]
  edge [
    source 49
    target 407
  ]
  edge [
    source 49
    target 408
  ]
  edge [
    source 49
    target 109
  ]
  edge [
    source 49
    target 409
  ]
  edge [
    source 49
    target 410
  ]
  edge [
    source 49
    target 411
  ]
  edge [
    source 49
    target 412
  ]
  edge [
    source 49
    target 413
  ]
  edge [
    source 49
    target 414
  ]
  edge [
    source 49
    target 415
  ]
  edge [
    source 49
    target 416
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 417
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 53
    target 62
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 418
  ]
  edge [
    source 54
    target 419
  ]
  edge [
    source 54
    target 420
  ]
  edge [
    source 54
    target 173
  ]
  edge [
    source 54
    target 421
  ]
  edge [
    source 54
    target 422
  ]
  edge [
    source 54
    target 423
  ]
  edge [
    source 54
    target 424
  ]
  edge [
    source 54
    target 425
  ]
  edge [
    source 54
    target 426
  ]
  edge [
    source 54
    target 427
  ]
  edge [
    source 54
    target 428
  ]
  edge [
    source 54
    target 429
  ]
  edge [
    source 54
    target 430
  ]
  edge [
    source 54
    target 431
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 432
  ]
  edge [
    source 56
    target 433
  ]
  edge [
    source 56
    target 434
  ]
  edge [
    source 56
    target 435
  ]
  edge [
    source 56
    target 436
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 437
  ]
  edge [
    source 57
    target 438
  ]
  edge [
    source 57
    target 439
  ]
  edge [
    source 57
    target 440
  ]
  edge [
    source 57
    target 441
  ]
  edge [
    source 57
    target 442
  ]
  edge [
    source 57
    target 443
  ]
  edge [
    source 57
    target 444
  ]
  edge [
    source 57
    target 445
  ]
  edge [
    source 57
    target 446
  ]
  edge [
    source 57
    target 347
  ]
  edge [
    source 57
    target 447
  ]
  edge [
    source 57
    target 448
  ]
  edge [
    source 57
    target 449
  ]
  edge [
    source 57
    target 319
  ]
  edge [
    source 57
    target 450
  ]
  edge [
    source 57
    target 451
  ]
  edge [
    source 57
    target 394
  ]
  edge [
    source 57
    target 452
  ]
  edge [
    source 57
    target 453
  ]
  edge [
    source 57
    target 454
  ]
  edge [
    source 57
    target 455
  ]
  edge [
    source 57
    target 456
  ]
  edge [
    source 57
    target 457
  ]
  edge [
    source 57
    target 458
  ]
  edge [
    source 57
    target 459
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 460
  ]
  edge [
    source 58
    target 461
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 462
  ]
  edge [
    source 59
    target 463
  ]
  edge [
    source 59
    target 464
  ]
  edge [
    source 59
    target 465
  ]
  edge [
    source 59
    target 466
  ]
  edge [
    source 59
    target 87
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 467
  ]
  edge [
    source 60
    target 468
  ]
  edge [
    source 60
    target 469
  ]
  edge [
    source 60
    target 470
  ]
  edge [
    source 60
    target 471
  ]
  edge [
    source 60
    target 472
  ]
  edge [
    source 60
    target 473
  ]
  edge [
    source 60
    target 474
  ]
  edge [
    source 61
    target 475
  ]
  edge [
    source 61
    target 476
  ]
  edge [
    source 61
    target 477
  ]
  edge [
    source 61
    target 478
  ]
  edge [
    source 61
    target 479
  ]
  edge [
    source 61
    target 480
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 481
  ]
  edge [
    source 62
    target 482
  ]
  edge [
    source 62
    target 483
  ]
  edge [
    source 62
    target 484
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 485
  ]
  edge [
    source 63
    target 331
  ]
  edge [
    source 63
    target 95
  ]
  edge [
    source 63
    target 486
  ]
  edge [
    source 63
    target 487
  ]
  edge [
    source 63
    target 488
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 66
    target 489
  ]
  edge [
    source 66
    target 490
  ]
  edge [
    source 66
    target 491
  ]
  edge [
    source 66
    target 492
  ]
  edge [
    source 66
    target 75
  ]
  edge [
    source 67
    target 74
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 68
    target 488
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 69
    target 493
  ]
  edge [
    source 69
    target 494
  ]
  edge [
    source 69
    target 401
  ]
  edge [
    source 69
    target 495
  ]
  edge [
    source 69
    target 496
  ]
  edge [
    source 69
    target 497
  ]
  edge [
    source 69
    target 498
  ]
  edge [
    source 69
    target 367
  ]
  edge [
    source 69
    target 499
  ]
  edge [
    source 69
    target 397
  ]
  edge [
    source 69
    target 500
  ]
  edge [
    source 69
    target 444
  ]
  edge [
    source 69
    target 501
  ]
  edge [
    source 69
    target 355
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 502
  ]
  edge [
    source 70
    target 503
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 71
    target 504
  ]
  edge [
    source 71
    target 505
  ]
  edge [
    source 71
    target 506
  ]
  edge [
    source 71
    target 507
  ]
  edge [
    source 71
    target 508
  ]
  edge [
    source 71
    target 509
  ]
  edge [
    source 71
    target 510
  ]
  edge [
    source 71
    target 511
  ]
  edge [
    source 71
    target 512
  ]
  edge [
    source 72
    target 73
  ]
  edge [
    source 73
    target 513
  ]
  edge [
    source 73
    target 514
  ]
  edge [
    source 74
    target 75
  ]
  edge [
    source 74
    target 515
  ]
  edge [
    source 74
    target 516
  ]
  edge [
    source 74
    target 517
  ]
  edge [
    source 74
    target 518
  ]
  edge [
    source 74
    target 519
  ]
  edge [
    source 74
    target 520
  ]
  edge [
    source 74
    target 521
  ]
  edge [
    source 74
    target 522
  ]
  edge [
    source 75
    target 76
  ]
  edge [
    source 75
    target 256
  ]
  edge [
    source 75
    target 523
  ]
  edge [
    source 75
    target 524
  ]
  edge [
    source 75
    target 525
  ]
  edge [
    source 75
    target 526
  ]
  edge [
    source 75
    target 527
  ]
  edge [
    source 75
    target 528
  ]
  edge [
    source 76
    target 77
  ]
  edge [
    source 76
    target 529
  ]
  edge [
    source 76
    target 530
  ]
  edge [
    source 77
    target 78
  ]
  edge [
    source 77
    target 420
  ]
  edge [
    source 77
    target 531
  ]
  edge [
    source 77
    target 532
  ]
  edge [
    source 77
    target 533
  ]
  edge [
    source 77
    target 534
  ]
  edge [
    source 77
    target 350
  ]
  edge [
    source 77
    target 535
  ]
  edge [
    source 77
    target 536
  ]
  edge [
    source 77
    target 537
  ]
  edge [
    source 77
    target 538
  ]
  edge [
    source 77
    target 539
  ]
  edge [
    source 77
    target 540
  ]
  edge [
    source 77
    target 541
  ]
  edge [
    source 77
    target 542
  ]
  edge [
    source 77
    target 543
  ]
  edge [
    source 77
    target 544
  ]
  edge [
    source 77
    target 545
  ]
  edge [
    source 77
    target 546
  ]
  edge [
    source 77
    target 183
  ]
  edge [
    source 77
    target 547
  ]
  edge [
    source 77
    target 548
  ]
  edge [
    source 77
    target 549
  ]
  edge [
    source 77
    target 550
  ]
  edge [
    source 77
    target 551
  ]
  edge [
    source 77
    target 552
  ]
  edge [
    source 77
    target 462
  ]
  edge [
    source 77
    target 553
  ]
  edge [
    source 77
    target 554
  ]
  edge [
    source 77
    target 555
  ]
  edge [
    source 77
    target 556
  ]
  edge [
    source 77
    target 557
  ]
  edge [
    source 78
    target 79
  ]
  edge [
    source 78
    target 558
  ]
  edge [
    source 78
    target 559
  ]
  edge [
    source 78
    target 560
  ]
  edge [
    source 78
    target 561
  ]
  edge [
    source 78
    target 562
  ]
  edge [
    source 78
    target 563
  ]
  edge [
    source 78
    target 564
  ]
  edge [
    source 78
    target 565
  ]
  edge [
    source 78
    target 566
  ]
  edge [
    source 78
    target 567
  ]
  edge [
    source 78
    target 568
  ]
  edge [
    source 78
    target 569
  ]
  edge [
    source 78
    target 570
  ]
  edge [
    source 78
    target 571
  ]
  edge [
    source 78
    target 572
  ]
  edge [
    source 78
    target 573
  ]
  edge [
    source 78
    target 574
  ]
  edge [
    source 78
    target 575
  ]
  edge [
    source 78
    target 576
  ]
  edge [
    source 78
    target 577
  ]
  edge [
    source 78
    target 578
  ]
  edge [
    source 78
    target 579
  ]
  edge [
    source 78
    target 580
  ]
  edge [
    source 78
    target 581
  ]
  edge [
    source 78
    target 582
  ]
  edge [
    source 78
    target 583
  ]
  edge [
    source 78
    target 584
  ]
  edge [
    source 78
    target 585
  ]
  edge [
    source 78
    target 586
  ]
  edge [
    source 78
    target 587
  ]
  edge [
    source 78
    target 588
  ]
  edge [
    source 78
    target 589
  ]
  edge [
    source 78
    target 590
  ]
  edge [
    source 78
    target 591
  ]
  edge [
    source 78
    target 592
  ]
  edge [
    source 78
    target 593
  ]
  edge [
    source 78
    target 594
  ]
  edge [
    source 78
    target 595
  ]
  edge [
    source 78
    target 596
  ]
  edge [
    source 78
    target 597
  ]
  edge [
    source 78
    target 598
  ]
  edge [
    source 78
    target 599
  ]
  edge [
    source 78
    target 600
  ]
  edge [
    source 78
    target 601
  ]
  edge [
    source 79
    target 80
  ]
  edge [
    source 79
    target 548
  ]
  edge [
    source 79
    target 602
  ]
  edge [
    source 80
    target 81
  ]
  edge [
    source 80
    target 603
  ]
  edge [
    source 80
    target 604
  ]
  edge [
    source 80
    target 605
  ]
  edge [
    source 80
    target 606
  ]
  edge [
    source 80
    target 607
  ]
  edge [
    source 80
    target 608
  ]
  edge [
    source 80
    target 609
  ]
  edge [
    source 80
    target 109
  ]
  edge [
    source 80
    target 570
  ]
  edge [
    source 80
    target 610
  ]
  edge [
    source 80
    target 611
  ]
  edge [
    source 80
    target 612
  ]
  edge [
    source 80
    target 613
  ]
  edge [
    source 80
    target 614
  ]
  edge [
    source 80
    target 615
  ]
  edge [
    source 80
    target 616
  ]
  edge [
    source 80
    target 617
  ]
  edge [
    source 80
    target 618
  ]
  edge [
    source 80
    target 211
  ]
  edge [
    source 80
    target 619
  ]
  edge [
    source 80
    target 620
  ]
  edge [
    source 80
    target 129
  ]
  edge [
    source 81
    target 260
  ]
  edge [
    source 81
    target 621
  ]
  edge [
    source 82
    target 622
  ]
  edge [
    source 82
    target 623
  ]
  edge [
    source 83
    target 84
  ]
  edge [
    source 84
    target 85
  ]
  edge [
    source 84
    target 624
  ]
  edge [
    source 84
    target 625
  ]
  edge [
    source 84
    target 626
  ]
  edge [
    source 84
    target 627
  ]
  edge [
    source 84
    target 628
  ]
  edge [
    source 85
    target 86
  ]
  edge [
    source 86
    target 87
  ]
  edge [
    source 86
    target 629
  ]
  edge [
    source 86
    target 630
  ]
  edge [
    source 86
    target 488
  ]
  edge [
    source 87
    target 309
  ]
  edge [
    source 87
    target 631
  ]
  edge [
    source 87
    target 405
  ]
  edge [
    source 87
    target 632
  ]
]
