graph [
  maxDegree 46
  minDegree 1
  meanDegree 1.9746835443037976
  density 0.02531645569620253
  graphCliqueNumber 2
  node [
    id 0
    label "ko&#324;"
    origin "text"
  ]
  node [
    id 1
    label "chroni&#263;"
    origin "text"
  ]
  node [
    id 2
    label "ranczer"
    origin "text"
  ]
  node [
    id 3
    label "przed"
    origin "text"
  ]
  node [
    id 4
    label "mama"
    origin "text"
  ]
  node [
    id 5
    label "krowa"
    origin "text"
  ]
  node [
    id 6
    label "pok&#322;usowa&#263;"
  ]
  node [
    id 7
    label "czo&#322;dar"
  ]
  node [
    id 8
    label "os&#322;omu&#322;"
  ]
  node [
    id 9
    label "kawalerzysta"
  ]
  node [
    id 10
    label "k&#322;usowanie"
  ]
  node [
    id 11
    label "pok&#322;usowanie"
  ]
  node [
    id 12
    label "zwierz&#281;_wierzchowe"
  ]
  node [
    id 13
    label "zar&#380;e&#263;"
  ]
  node [
    id 14
    label "karmiak"
  ]
  node [
    id 15
    label "galopowa&#263;"
  ]
  node [
    id 16
    label "k&#322;usowa&#263;"
  ]
  node [
    id 17
    label "przegalopowa&#263;"
  ]
  node [
    id 18
    label "hipoterapeuta"
  ]
  node [
    id 19
    label "osadzenie_si&#281;"
  ]
  node [
    id 20
    label "remuda"
  ]
  node [
    id 21
    label "znarowienie"
  ]
  node [
    id 22
    label "r&#380;e&#263;"
  ]
  node [
    id 23
    label "zebroid"
  ]
  node [
    id 24
    label "r&#380;enie"
  ]
  node [
    id 25
    label "podkuwanie"
  ]
  node [
    id 26
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 27
    label "podkuwa&#263;"
  ]
  node [
    id 28
    label "narowi&#263;"
  ]
  node [
    id 29
    label "zebrula"
  ]
  node [
    id 30
    label "&#322;ykawo&#347;&#263;"
  ]
  node [
    id 31
    label "zaci&#261;&#263;"
  ]
  node [
    id 32
    label "nar&#243;w"
  ]
  node [
    id 33
    label "przegalopowanie"
  ]
  node [
    id 34
    label "figura"
  ]
  node [
    id 35
    label "lansada"
  ]
  node [
    id 36
    label "dosiad"
  ]
  node [
    id 37
    label "narowienie"
  ]
  node [
    id 38
    label "zaci&#281;cie"
  ]
  node [
    id 39
    label "koniowate"
  ]
  node [
    id 40
    label "pogalopowanie"
  ]
  node [
    id 41
    label "uje&#380;d&#380;anie"
  ]
  node [
    id 42
    label "ko&#324;_dziki"
  ]
  node [
    id 43
    label "pogalopowa&#263;"
  ]
  node [
    id 44
    label "penis"
  ]
  node [
    id 45
    label "hipoterapia"
  ]
  node [
    id 46
    label "osadza&#263;_si&#281;"
  ]
  node [
    id 47
    label "osadzi&#263;_si&#281;"
  ]
  node [
    id 48
    label "galopowanie"
  ]
  node [
    id 49
    label "osadzanie_si&#281;"
  ]
  node [
    id 50
    label "znarowi&#263;"
  ]
  node [
    id 51
    label "report"
  ]
  node [
    id 52
    label "kultywowa&#263;"
  ]
  node [
    id 53
    label "sprawowa&#263;"
  ]
  node [
    id 54
    label "robi&#263;"
  ]
  node [
    id 55
    label "czuwa&#263;"
  ]
  node [
    id 56
    label "koniarz"
  ]
  node [
    id 57
    label "matczysko"
  ]
  node [
    id 58
    label "macierz"
  ]
  node [
    id 59
    label "przodkini"
  ]
  node [
    id 60
    label "Matka_Boska"
  ]
  node [
    id 61
    label "macocha"
  ]
  node [
    id 62
    label "matka_zast&#281;pcza"
  ]
  node [
    id 63
    label "stara"
  ]
  node [
    id 64
    label "rodzice"
  ]
  node [
    id 65
    label "rodzic"
  ]
  node [
    id 66
    label "szkarada"
  ]
  node [
    id 67
    label "wymi&#281;"
  ]
  node [
    id 68
    label "samica"
  ]
  node [
    id 69
    label "baba"
  ]
  node [
    id 70
    label "berek"
  ]
  node [
    id 71
    label "mucze&#263;"
  ]
  node [
    id 72
    label "muczenie"
  ]
  node [
    id 73
    label "mleczno&#347;&#263;"
  ]
  node [
    id 74
    label "krasula"
  ]
  node [
    id 75
    label "miotacz"
  ]
  node [
    id 76
    label "gigant"
  ]
  node [
    id 77
    label "zamucze&#263;"
  ]
  node [
    id 78
    label "bydl&#281;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
]
