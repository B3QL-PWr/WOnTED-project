graph [
  maxDegree 7
  minDegree 1
  meanDegree 1.75
  density 0.25
  graphCliqueNumber 2
  node [
    id 0
    label "lacazette"
    origin "text"
  ]
  node [
    id 1
    label "mecz"
    origin "text"
  ]
  node [
    id 2
    label "meczgif"
    origin "text"
  ]
  node [
    id 3
    label "obrona"
  ]
  node [
    id 4
    label "gra"
  ]
  node [
    id 5
    label "dwumecz"
  ]
  node [
    id 6
    label "game"
  ]
  node [
    id 7
    label "serw"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
]
