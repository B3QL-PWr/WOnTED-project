graph [
  maxDegree 29
  minDegree 1
  meanDegree 1.9848484848484849
  density 0.015151515151515152
  graphCliqueNumber 2
  node [
    id 0
    label "truna"
    origin "text"
  ]
  node [
    id 1
    label "pojawi&#263;"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "wezwanie"
    origin "text"
  ]
  node [
    id 4
    label "przekr&#281;ci&#263;"
    origin "text"
  ]
  node [
    id 5
    label "zamek"
    origin "text"
  ]
  node [
    id 6
    label "klucz"
    origin "text"
  ]
  node [
    id 7
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 8
    label "nosi&#263;"
    origin "text"
  ]
  node [
    id 9
    label "pier&#347;"
    origin "text"
  ]
  node [
    id 10
    label "przesun&#261;&#263;"
    origin "text"
  ]
  node [
    id 11
    label "d&#378;wignia"
    origin "text"
  ]
  node [
    id 12
    label "steruj&#261;cy"
    origin "text"
  ]
  node [
    id 13
    label "grod&#378;"
    origin "text"
  ]
  node [
    id 14
    label "zwo&#322;anie_si&#281;"
  ]
  node [
    id 15
    label "admonition"
  ]
  node [
    id 16
    label "pismo_urz&#281;dowe"
  ]
  node [
    id 17
    label "poinformowanie"
  ]
  node [
    id 18
    label "patron"
  ]
  node [
    id 19
    label "bid"
  ]
  node [
    id 20
    label "zach&#281;cenie"
  ]
  node [
    id 21
    label "poproszenie"
  ]
  node [
    id 22
    label "apostrofa"
  ]
  node [
    id 23
    label "nakazanie"
  ]
  node [
    id 24
    label "pro&#347;ba"
  ]
  node [
    id 25
    label "nazwa"
  ]
  node [
    id 26
    label "wst&#281;p"
  ]
  node [
    id 27
    label "summons"
  ]
  node [
    id 28
    label "nakaz"
  ]
  node [
    id 29
    label "turn"
  ]
  node [
    id 30
    label "zemle&#263;"
  ]
  node [
    id 31
    label "obr&#243;ci&#263;"
  ]
  node [
    id 32
    label "zwr&#243;ci&#263;"
  ]
  node [
    id 33
    label "wrench"
  ]
  node [
    id 34
    label "zepsu&#263;"
  ]
  node [
    id 35
    label "zadzwoni&#263;"
  ]
  node [
    id 36
    label "Windsor"
  ]
  node [
    id 37
    label "budynek"
  ]
  node [
    id 38
    label "bramka"
  ]
  node [
    id 39
    label "fortyfikacja"
  ]
  node [
    id 40
    label "blokada"
  ]
  node [
    id 41
    label "iglica"
  ]
  node [
    id 42
    label "fastener"
  ]
  node [
    id 43
    label "baszta"
  ]
  node [
    id 44
    label "budowla"
  ]
  node [
    id 45
    label "zagrywka"
  ]
  node [
    id 46
    label "hokej"
  ]
  node [
    id 47
    label "wjazd"
  ]
  node [
    id 48
    label "mechanizm"
  ]
  node [
    id 49
    label "bro&#324;_palna"
  ]
  node [
    id 50
    label "stra&#380;nica"
  ]
  node [
    id 51
    label "blockage"
  ]
  node [
    id 52
    label "informatyka"
  ]
  node [
    id 53
    label "Wawel"
  ]
  node [
    id 54
    label "brama"
  ]
  node [
    id 55
    label "ekspres"
  ]
  node [
    id 56
    label "rezydencja"
  ]
  node [
    id 57
    label "Zamek_Ogrodzieniec"
  ]
  node [
    id 58
    label "drzwi"
  ]
  node [
    id 59
    label "komora_zamkowa"
  ]
  node [
    id 60
    label "zamkni&#281;cie"
  ]
  node [
    id 61
    label "tercja"
  ]
  node [
    id 62
    label "zapi&#281;cie"
  ]
  node [
    id 63
    label "spis"
  ]
  node [
    id 64
    label "kompleks"
  ]
  node [
    id 65
    label "podstawa"
  ]
  node [
    id 66
    label "obja&#347;nienie"
  ]
  node [
    id 67
    label "narz&#281;dzie"
  ]
  node [
    id 68
    label "ochrona"
  ]
  node [
    id 69
    label "za&#322;&#261;cznik"
  ]
  node [
    id 70
    label "znak_muzyczny"
  ]
  node [
    id 71
    label "przycisk"
  ]
  node [
    id 72
    label "nakr&#281;ca&#263;"
  ]
  node [
    id 73
    label "spos&#243;b"
  ]
  node [
    id 74
    label "kliniec"
  ]
  node [
    id 75
    label "code"
  ]
  node [
    id 76
    label "szyfr"
  ]
  node [
    id 77
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 78
    label "instrument_strunowy"
  ]
  node [
    id 79
    label "z&#322;&#261;czenie"
  ]
  node [
    id 80
    label "zbi&#243;r"
  ]
  node [
    id 81
    label "szyk"
  ]
  node [
    id 82
    label "radical"
  ]
  node [
    id 83
    label "kod"
  ]
  node [
    id 84
    label "str&#243;j"
  ]
  node [
    id 85
    label "mie&#263;"
  ]
  node [
    id 86
    label "wk&#322;ada&#263;"
  ]
  node [
    id 87
    label "przemieszcza&#263;"
  ]
  node [
    id 88
    label "posiada&#263;"
  ]
  node [
    id 89
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 90
    label "wear"
  ]
  node [
    id 91
    label "carry"
  ]
  node [
    id 92
    label "&#380;ebro"
  ]
  node [
    id 93
    label "&#347;r&#243;dpiersie"
  ]
  node [
    id 94
    label "zast&#243;j"
  ]
  node [
    id 95
    label "tu&#322;&#243;w"
  ]
  node [
    id 96
    label "dydka"
  ]
  node [
    id 97
    label "tuszka"
  ]
  node [
    id 98
    label "cycek"
  ]
  node [
    id 99
    label "mi&#281;sie&#324;_z&#281;baty_tylny_dolny"
  ]
  node [
    id 100
    label "sutek"
  ]
  node [
    id 101
    label "laktator"
  ]
  node [
    id 102
    label "biust"
  ]
  node [
    id 103
    label "przedpiersie"
  ]
  node [
    id 104
    label "mi&#281;sie&#324;_z&#281;baty_przedni"
  ]
  node [
    id 105
    label "mi&#281;sie&#324;_oddechowy"
  ]
  node [
    id 106
    label "mostek"
  ]
  node [
    id 107
    label "filet"
  ]
  node [
    id 108
    label "mi&#281;sie&#324;_z&#281;baty_tylny_g&#243;rny"
  ]
  node [
    id 109
    label "gruczo&#322;_sutkowy"
  ]
  node [
    id 110
    label "dr&#243;b"
  ]
  node [
    id 111
    label "cycuch"
  ]
  node [
    id 112
    label "mastektomia"
  ]
  node [
    id 113
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 114
    label "decha"
  ]
  node [
    id 115
    label "&#380;y&#322;a_ramienno-&#322;okciowa"
  ]
  node [
    id 116
    label "mi&#281;so"
  ]
  node [
    id 117
    label "dostosowa&#263;"
  ]
  node [
    id 118
    label "motivate"
  ]
  node [
    id 119
    label "przenie&#347;&#263;"
  ]
  node [
    id 120
    label "shift"
  ]
  node [
    id 121
    label "deepen"
  ]
  node [
    id 122
    label "transfer"
  ]
  node [
    id 123
    label "ruszy&#263;"
  ]
  node [
    id 124
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 125
    label "zmieni&#263;"
  ]
  node [
    id 126
    label "czynnik"
  ]
  node [
    id 127
    label "przyrz&#261;d_mechaniczny"
  ]
  node [
    id 128
    label "przegroda"
  ]
  node [
    id 129
    label "bulkhead"
  ]
  node [
    id 130
    label "&#347;ciana"
  ]
  node [
    id 131
    label "kad&#322;ub"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 9
    target 100
  ]
  edge [
    source 9
    target 101
  ]
  edge [
    source 9
    target 102
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 104
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 9
    target 116
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 10
    target 121
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 10
    target 125
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 126
  ]
  edge [
    source 11
    target 127
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 128
  ]
  edge [
    source 13
    target 129
  ]
  edge [
    source 13
    target 130
  ]
  edge [
    source 13
    target 131
  ]
]
