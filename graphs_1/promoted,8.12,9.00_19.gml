graph [
  maxDegree 10
  minDegree 1
  meanDegree 1.9090909090909092
  density 0.09090909090909091
  graphCliqueNumber 2
  node [
    id 0
    label "tak"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "bawi&#263;"
    origin "text"
  ]
  node [
    id 3
    label "nasi"
    origin "text"
  ]
  node [
    id 4
    label "rodak"
    origin "text"
  ]
  node [
    id 5
    label "obczyzna"
    origin "text"
  ]
  node [
    id 6
    label "ubawia&#263;"
  ]
  node [
    id 7
    label "amuse"
  ]
  node [
    id 8
    label "zajmowa&#263;"
  ]
  node [
    id 9
    label "wzbudza&#263;"
  ]
  node [
    id 10
    label "przebywa&#263;"
  ]
  node [
    id 11
    label "sprawia&#263;"
  ]
  node [
    id 12
    label "zabawia&#263;"
  ]
  node [
    id 13
    label "play"
  ]
  node [
    id 14
    label "wsp&#243;&#322;obywatel"
  ]
  node [
    id 15
    label "pobratymca"
  ]
  node [
    id 16
    label "ojczyc"
  ]
  node [
    id 17
    label "plemiennik"
  ]
  node [
    id 18
    label "wsp&#243;&#322;ziomek"
  ]
  node [
    id 19
    label "obszar"
  ]
  node [
    id 20
    label "granica_pa&#324;stwa"
  ]
  node [
    id 21
    label "&#347;wiat"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 21
  ]
]
