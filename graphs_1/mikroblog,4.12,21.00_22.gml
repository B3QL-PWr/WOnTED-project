graph [
  maxDegree 40
  minDegree 1
  meanDegree 1.981651376146789
  density 0.01834862385321101
  graphCliqueNumber 2
  node [
    id 0
    label "elo"
    origin "text"
  ]
  node [
    id 1
    label "mirka"
    origin "text"
  ]
  node [
    id 2
    label "mirabelka"
    origin "text"
  ]
  node [
    id 3
    label "nagra&#263;by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "ostatnio"
    origin "text"
  ]
  node [
    id 5
    label "weso&#322;y"
    origin "text"
  ]
  node [
    id 6
    label "moment"
    origin "text"
  ]
  node [
    id 7
    label "&#347;mieszkow&#261;"
    origin "text"
  ]
  node [
    id 8
    label "epka"
    origin "text"
  ]
  node [
    id 9
    label "ods&#322;uch"
    origin "text"
  ]
  node [
    id 10
    label "poni&#380;ej"
    origin "text"
  ]
  node [
    id 11
    label "zrobi&#263;by&#263;"
    origin "text"
  ]
  node [
    id 12
    label "nawet"
    origin "text"
  ]
  node [
    id 13
    label "sto"
    origin "text"
  ]
  node [
    id 14
    label "sztuk"
    origin "text"
  ]
  node [
    id 15
    label "jeden"
    origin "text"
  ]
  node [
    id 16
    label "je&#347;li"
    origin "text"
  ]
  node [
    id 17
    label "by&#263;"
    origin "text"
  ]
  node [
    id 18
    label "dwa"
    origin "text"
  ]
  node [
    id 19
    label "sztuka"
    origin "text"
  ]
  node [
    id 20
    label "rozda&#263;"
    origin "text"
  ]
  node [
    id 21
    label "losowy"
    origin "text"
  ]
  node [
    id 22
    label "&#347;liwka"
  ]
  node [
    id 23
    label "&#347;liwa_domowa"
  ]
  node [
    id 24
    label "ostatni"
  ]
  node [
    id 25
    label "poprzednio"
  ]
  node [
    id 26
    label "aktualnie"
  ]
  node [
    id 27
    label "beztroski"
  ]
  node [
    id 28
    label "pozytywny"
  ]
  node [
    id 29
    label "pijany"
  ]
  node [
    id 30
    label "dobry"
  ]
  node [
    id 31
    label "weso&#322;o"
  ]
  node [
    id 32
    label "okres_czasu"
  ]
  node [
    id 33
    label "minute"
  ]
  node [
    id 34
    label "jednostka_geologiczna"
  ]
  node [
    id 35
    label "warto&#347;&#263;_oczekiwana"
  ]
  node [
    id 36
    label "time"
  ]
  node [
    id 37
    label "chron"
  ]
  node [
    id 38
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 39
    label "fragment"
  ]
  node [
    id 40
    label "mini-album"
  ]
  node [
    id 41
    label "album"
  ]
  node [
    id 42
    label "g&#322;o&#347;nik"
  ]
  node [
    id 43
    label "nast&#281;pnie"
  ]
  node [
    id 44
    label "poni&#380;szy"
  ]
  node [
    id 45
    label "kieliszek"
  ]
  node [
    id 46
    label "shot"
  ]
  node [
    id 47
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 48
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 49
    label "jaki&#347;"
  ]
  node [
    id 50
    label "jednolicie"
  ]
  node [
    id 51
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 52
    label "w&#243;dka"
  ]
  node [
    id 53
    label "ten"
  ]
  node [
    id 54
    label "ujednolicenie"
  ]
  node [
    id 55
    label "jednakowy"
  ]
  node [
    id 56
    label "si&#281;ga&#263;"
  ]
  node [
    id 57
    label "trwa&#263;"
  ]
  node [
    id 58
    label "obecno&#347;&#263;"
  ]
  node [
    id 59
    label "stan"
  ]
  node [
    id 60
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 61
    label "stand"
  ]
  node [
    id 62
    label "mie&#263;_miejsce"
  ]
  node [
    id 63
    label "uczestniczy&#263;"
  ]
  node [
    id 64
    label "chodzi&#263;"
  ]
  node [
    id 65
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 66
    label "equal"
  ]
  node [
    id 67
    label "theatrical_performance"
  ]
  node [
    id 68
    label "cz&#322;owiek"
  ]
  node [
    id 69
    label "sprawno&#347;&#263;"
  ]
  node [
    id 70
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 71
    label "ods&#322;ona"
  ]
  node [
    id 72
    label "przedmiot"
  ]
  node [
    id 73
    label "Faust"
  ]
  node [
    id 74
    label "jednostka"
  ]
  node [
    id 75
    label "fortel"
  ]
  node [
    id 76
    label "environment"
  ]
  node [
    id 77
    label "rola"
  ]
  node [
    id 78
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 79
    label "utw&#243;r"
  ]
  node [
    id 80
    label "egzemplarz"
  ]
  node [
    id 81
    label "realizacja"
  ]
  node [
    id 82
    label "turn"
  ]
  node [
    id 83
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 84
    label "scenografia"
  ]
  node [
    id 85
    label "kultura_duchowa"
  ]
  node [
    id 86
    label "ilo&#347;&#263;"
  ]
  node [
    id 87
    label "pokaz"
  ]
  node [
    id 88
    label "przedstawia&#263;"
  ]
  node [
    id 89
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 90
    label "pr&#243;bowanie"
  ]
  node [
    id 91
    label "kobieta"
  ]
  node [
    id 92
    label "scena"
  ]
  node [
    id 93
    label "przedstawianie"
  ]
  node [
    id 94
    label "scenariusz"
  ]
  node [
    id 95
    label "didaskalia"
  ]
  node [
    id 96
    label "Apollo"
  ]
  node [
    id 97
    label "czyn"
  ]
  node [
    id 98
    label "przedstawienie"
  ]
  node [
    id 99
    label "head"
  ]
  node [
    id 100
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 101
    label "przedstawi&#263;"
  ]
  node [
    id 102
    label "ambala&#380;"
  ]
  node [
    id 103
    label "kultura"
  ]
  node [
    id 104
    label "towar"
  ]
  node [
    id 105
    label "da&#263;"
  ]
  node [
    id 106
    label "distribute"
  ]
  node [
    id 107
    label "losowo"
  ]
  node [
    id 108
    label "przypadkowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 40
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 42
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 43
  ]
  edge [
    source 10
    target 44
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 45
  ]
  edge [
    source 15
    target 46
  ]
  edge [
    source 15
    target 47
  ]
  edge [
    source 15
    target 48
  ]
  edge [
    source 15
    target 49
  ]
  edge [
    source 15
    target 50
  ]
  edge [
    source 15
    target 51
  ]
  edge [
    source 15
    target 52
  ]
  edge [
    source 15
    target 53
  ]
  edge [
    source 15
    target 54
  ]
  edge [
    source 15
    target 55
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 56
  ]
  edge [
    source 17
    target 57
  ]
  edge [
    source 17
    target 58
  ]
  edge [
    source 17
    target 59
  ]
  edge [
    source 17
    target 60
  ]
  edge [
    source 17
    target 61
  ]
  edge [
    source 17
    target 62
  ]
  edge [
    source 17
    target 63
  ]
  edge [
    source 17
    target 64
  ]
  edge [
    source 17
    target 65
  ]
  edge [
    source 17
    target 66
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 67
  ]
  edge [
    source 19
    target 68
  ]
  edge [
    source 19
    target 69
  ]
  edge [
    source 19
    target 70
  ]
  edge [
    source 19
    target 71
  ]
  edge [
    source 19
    target 72
  ]
  edge [
    source 19
    target 73
  ]
  edge [
    source 19
    target 74
  ]
  edge [
    source 19
    target 75
  ]
  edge [
    source 19
    target 76
  ]
  edge [
    source 19
    target 77
  ]
  edge [
    source 19
    target 78
  ]
  edge [
    source 19
    target 79
  ]
  edge [
    source 19
    target 80
  ]
  edge [
    source 19
    target 81
  ]
  edge [
    source 19
    target 82
  ]
  edge [
    source 19
    target 83
  ]
  edge [
    source 19
    target 84
  ]
  edge [
    source 19
    target 85
  ]
  edge [
    source 19
    target 86
  ]
  edge [
    source 19
    target 87
  ]
  edge [
    source 19
    target 88
  ]
  edge [
    source 19
    target 89
  ]
  edge [
    source 19
    target 90
  ]
  edge [
    source 19
    target 91
  ]
  edge [
    source 19
    target 92
  ]
  edge [
    source 19
    target 93
  ]
  edge [
    source 19
    target 94
  ]
  edge [
    source 19
    target 95
  ]
  edge [
    source 19
    target 96
  ]
  edge [
    source 19
    target 97
  ]
  edge [
    source 19
    target 98
  ]
  edge [
    source 19
    target 99
  ]
  edge [
    source 19
    target 100
  ]
  edge [
    source 19
    target 101
  ]
  edge [
    source 19
    target 102
  ]
  edge [
    source 19
    target 103
  ]
  edge [
    source 19
    target 104
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 105
  ]
  edge [
    source 20
    target 106
  ]
  edge [
    source 21
    target 107
  ]
  edge [
    source 21
    target 108
  ]
]
