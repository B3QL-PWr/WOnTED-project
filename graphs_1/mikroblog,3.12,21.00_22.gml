graph [
  maxDegree 26
  minDegree 1
  meanDegree 2.057142857142857
  density 0.011822660098522168
  graphCliqueNumber 3
  node [
    id 0
    label "troch&#281;"
    origin "text"
  ]
  node [
    id 1
    label "dupa"
    origin "text"
  ]
  node [
    id 2
    label "pytanie"
    origin "text"
  ]
  node [
    id 3
    label "kierowniczka"
    origin "text"
  ]
  node [
    id 4
    label "praca"
    origin "text"
  ]
  node [
    id 5
    label "zleci&#263;"
    origin "text"
  ]
  node [
    id 6
    label "wymiana"
    origin "text"
  ]
  node [
    id 7
    label "kilka"
    origin "text"
  ]
  node [
    id 8
    label "&#380;ar&#243;wka"
    origin "text"
  ]
  node [
    id 9
    label "&#347;wietl&#243;wka"
    origin "text"
  ]
  node [
    id 10
    label "powiedzie&#263;"
    origin "text"
  ]
  node [
    id 11
    label "by&#263;"
    origin "text"
  ]
  node [
    id 12
    label "drabina"
    origin "text"
  ]
  node [
    id 13
    label "itd"
    origin "text"
  ]
  node [
    id 14
    label "szczerze"
    origin "text"
  ]
  node [
    id 15
    label "srogi"
    origin "text"
  ]
  node [
    id 16
    label "wyjebundo"
    origin "text"
  ]
  node [
    id 17
    label "zastanawia&#263;"
    origin "text"
  ]
  node [
    id 18
    label "musza"
    origin "text"
  ]
  node [
    id 19
    label "zrobi&#263;"
    origin "text"
  ]
  node [
    id 20
    label "te&#380;"
    origin "text"
  ]
  node [
    id 21
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 22
    label "boja"
    origin "text"
  ]
  node [
    id 23
    label "si&#281;"
    origin "text"
  ]
  node [
    id 24
    label "spadne"
    origin "text"
  ]
  node [
    id 25
    label "ten"
    origin "text"
  ]
  node [
    id 26
    label "wysoko"
    origin "text"
  ]
  node [
    id 27
    label "dusza_wo&#322;owa"
  ]
  node [
    id 28
    label "srom"
  ]
  node [
    id 29
    label "kobieta"
  ]
  node [
    id 30
    label "kochanka"
  ]
  node [
    id 31
    label "oferma"
  ]
  node [
    id 32
    label "pupa"
  ]
  node [
    id 33
    label "sk&#243;ra"
  ]
  node [
    id 34
    label "sprawa"
  ]
  node [
    id 35
    label "zadanie"
  ]
  node [
    id 36
    label "wypowied&#378;"
  ]
  node [
    id 37
    label "problemat"
  ]
  node [
    id 38
    label "rozpytywanie"
  ]
  node [
    id 39
    label "sprawdzian"
  ]
  node [
    id 40
    label "przes&#322;uchiwanie"
  ]
  node [
    id 41
    label "wypytanie"
  ]
  node [
    id 42
    label "zwracanie_si&#281;"
  ]
  node [
    id 43
    label "wypowiedzenie"
  ]
  node [
    id 44
    label "wywo&#322;ywanie"
  ]
  node [
    id 45
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 46
    label "problematyka"
  ]
  node [
    id 47
    label "question"
  ]
  node [
    id 48
    label "sprawdzanie"
  ]
  node [
    id 49
    label "odpowiadanie"
  ]
  node [
    id 50
    label "survey"
  ]
  node [
    id 51
    label "odpowiada&#263;"
  ]
  node [
    id 52
    label "egzaminowanie"
  ]
  node [
    id 53
    label "stosunek_pracy"
  ]
  node [
    id 54
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 55
    label "benedykty&#324;ski"
  ]
  node [
    id 56
    label "pracowanie"
  ]
  node [
    id 57
    label "zaw&#243;d"
  ]
  node [
    id 58
    label "kierownictwo"
  ]
  node [
    id 59
    label "zmiana"
  ]
  node [
    id 60
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 61
    label "wytw&#243;r"
  ]
  node [
    id 62
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 63
    label "tynkarski"
  ]
  node [
    id 64
    label "czynnik_produkcji"
  ]
  node [
    id 65
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 66
    label "zobowi&#261;zanie"
  ]
  node [
    id 67
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 68
    label "czynno&#347;&#263;"
  ]
  node [
    id 69
    label "tyrka"
  ]
  node [
    id 70
    label "pracowa&#263;"
  ]
  node [
    id 71
    label "siedziba"
  ]
  node [
    id 72
    label "poda&#380;_pracy"
  ]
  node [
    id 73
    label "miejsce"
  ]
  node [
    id 74
    label "zak&#322;ad"
  ]
  node [
    id 75
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 76
    label "najem"
  ]
  node [
    id 77
    label "teach"
  ]
  node [
    id 78
    label "poleci&#263;"
  ]
  node [
    id 79
    label "zjawisko_fonetyczne"
  ]
  node [
    id 80
    label "zamiana"
  ]
  node [
    id 81
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 82
    label "implicite"
  ]
  node [
    id 83
    label "ruch"
  ]
  node [
    id 84
    label "handel"
  ]
  node [
    id 85
    label "deal"
  ]
  node [
    id 86
    label "exchange"
  ]
  node [
    id 87
    label "wydarzenie"
  ]
  node [
    id 88
    label "explicite"
  ]
  node [
    id 89
    label "szachy"
  ]
  node [
    id 90
    label "&#347;ledziowate"
  ]
  node [
    id 91
    label "ryba"
  ]
  node [
    id 92
    label "jarzeni&#243;wka"
  ]
  node [
    id 93
    label "lampa"
  ]
  node [
    id 94
    label "banieczka"
  ]
  node [
    id 95
    label "sztuczne_&#378;r&#243;d&#322;o_&#347;wiat&#322;a"
  ]
  node [
    id 96
    label "zap&#322;onnik"
  ]
  node [
    id 97
    label "lampa_wy&#322;adowcza"
  ]
  node [
    id 98
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 99
    label "express"
  ]
  node [
    id 100
    label "rzekn&#261;&#263;"
  ]
  node [
    id 101
    label "okre&#347;li&#263;"
  ]
  node [
    id 102
    label "wyrazi&#263;"
  ]
  node [
    id 103
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 104
    label "unwrap"
  ]
  node [
    id 105
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 106
    label "convey"
  ]
  node [
    id 107
    label "discover"
  ]
  node [
    id 108
    label "wydoby&#263;"
  ]
  node [
    id 109
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 110
    label "poda&#263;"
  ]
  node [
    id 111
    label "si&#281;ga&#263;"
  ]
  node [
    id 112
    label "trwa&#263;"
  ]
  node [
    id 113
    label "obecno&#347;&#263;"
  ]
  node [
    id 114
    label "stan"
  ]
  node [
    id 115
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 116
    label "stand"
  ]
  node [
    id 117
    label "mie&#263;_miejsce"
  ]
  node [
    id 118
    label "uczestniczy&#263;"
  ]
  node [
    id 119
    label "chodzi&#263;"
  ]
  node [
    id 120
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 121
    label "equal"
  ]
  node [
    id 122
    label "rama"
  ]
  node [
    id 123
    label "szczebel"
  ]
  node [
    id 124
    label "przyrz&#261;d"
  ]
  node [
    id 125
    label "drabka"
  ]
  node [
    id 126
    label "ladder"
  ]
  node [
    id 127
    label "szczery"
  ]
  node [
    id 128
    label "szczero"
  ]
  node [
    id 129
    label "s&#322;usznie"
  ]
  node [
    id 130
    label "bluffly"
  ]
  node [
    id 131
    label "przekonuj&#261;co"
  ]
  node [
    id 132
    label "uczciwie"
  ]
  node [
    id 133
    label "outspokenly"
  ]
  node [
    id 134
    label "hojnie"
  ]
  node [
    id 135
    label "honestly"
  ]
  node [
    id 136
    label "artlessly"
  ]
  node [
    id 137
    label "powa&#380;ny"
  ]
  node [
    id 138
    label "silny"
  ]
  node [
    id 139
    label "twardy"
  ]
  node [
    id 140
    label "surowo"
  ]
  node [
    id 141
    label "gro&#378;nie"
  ]
  node [
    id 142
    label "ostry"
  ]
  node [
    id 143
    label "sierdzisty"
  ]
  node [
    id 144
    label "strike"
  ]
  node [
    id 145
    label "interesowa&#263;"
  ]
  node [
    id 146
    label "zorganizowa&#263;"
  ]
  node [
    id 147
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 148
    label "przerobi&#263;"
  ]
  node [
    id 149
    label "wystylizowa&#263;"
  ]
  node [
    id 150
    label "cause"
  ]
  node [
    id 151
    label "wydali&#263;"
  ]
  node [
    id 152
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 153
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 154
    label "post&#261;pi&#263;"
  ]
  node [
    id 155
    label "appoint"
  ]
  node [
    id 156
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 157
    label "nabra&#263;"
  ]
  node [
    id 158
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 159
    label "make"
  ]
  node [
    id 160
    label "uprawi&#263;"
  ]
  node [
    id 161
    label "gotowy"
  ]
  node [
    id 162
    label "might"
  ]
  node [
    id 163
    label "float"
  ]
  node [
    id 164
    label "znak_nawigacyjny"
  ]
  node [
    id 165
    label "p&#322;ywak"
  ]
  node [
    id 166
    label "okre&#347;lony"
  ]
  node [
    id 167
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 168
    label "chwalebnie"
  ]
  node [
    id 169
    label "wznio&#347;le"
  ]
  node [
    id 170
    label "g&#243;rno"
  ]
  node [
    id 171
    label "daleko"
  ]
  node [
    id 172
    label "niepo&#347;lednio"
  ]
  node [
    id 173
    label "wysoki"
  ]
  node [
    id 174
    label "szczytny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 20
  ]
  edge [
    source 8
    target 11
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 21
  ]
  edge [
    source 10
    target 22
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 10
    target 105
  ]
  edge [
    source 10
    target 106
  ]
  edge [
    source 10
    target 107
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 10
    target 110
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 26
  ]
  edge [
    source 11
    target 111
  ]
  edge [
    source 11
    target 112
  ]
  edge [
    source 11
    target 113
  ]
  edge [
    source 11
    target 114
  ]
  edge [
    source 11
    target 115
  ]
  edge [
    source 11
    target 116
  ]
  edge [
    source 11
    target 117
  ]
  edge [
    source 11
    target 118
  ]
  edge [
    source 11
    target 119
  ]
  edge [
    source 11
    target 120
  ]
  edge [
    source 11
    target 121
  ]
  edge [
    source 11
    target 21
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 25
  ]
  edge [
    source 12
    target 20
  ]
  edge [
    source 12
    target 122
  ]
  edge [
    source 12
    target 123
  ]
  edge [
    source 12
    target 124
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 127
  ]
  edge [
    source 14
    target 128
  ]
  edge [
    source 14
    target 129
  ]
  edge [
    source 14
    target 130
  ]
  edge [
    source 14
    target 131
  ]
  edge [
    source 14
    target 132
  ]
  edge [
    source 14
    target 133
  ]
  edge [
    source 14
    target 134
  ]
  edge [
    source 14
    target 135
  ]
  edge [
    source 14
    target 136
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 137
  ]
  edge [
    source 15
    target 138
  ]
  edge [
    source 15
    target 139
  ]
  edge [
    source 15
    target 140
  ]
  edge [
    source 15
    target 141
  ]
  edge [
    source 15
    target 142
  ]
  edge [
    source 15
    target 143
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 144
  ]
  edge [
    source 17
    target 145
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 146
  ]
  edge [
    source 19
    target 147
  ]
  edge [
    source 19
    target 148
  ]
  edge [
    source 19
    target 149
  ]
  edge [
    source 19
    target 150
  ]
  edge [
    source 19
    target 151
  ]
  edge [
    source 19
    target 152
  ]
  edge [
    source 19
    target 153
  ]
  edge [
    source 19
    target 154
  ]
  edge [
    source 19
    target 155
  ]
  edge [
    source 19
    target 156
  ]
  edge [
    source 19
    target 157
  ]
  edge [
    source 19
    target 158
  ]
  edge [
    source 19
    target 159
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 160
  ]
  edge [
    source 21
    target 161
  ]
  edge [
    source 21
    target 162
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 163
  ]
  edge [
    source 22
    target 164
  ]
  edge [
    source 22
    target 165
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 166
  ]
  edge [
    source 25
    target 167
  ]
  edge [
    source 26
    target 168
  ]
  edge [
    source 26
    target 169
  ]
  edge [
    source 26
    target 170
  ]
  edge [
    source 26
    target 171
  ]
  edge [
    source 26
    target 172
  ]
  edge [
    source 26
    target 173
  ]
  edge [
    source 26
    target 174
  ]
]
