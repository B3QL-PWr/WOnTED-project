graph [
  maxDegree 12
  minDegree 1
  meanDegree 1.92
  density 0.08
  graphCliqueNumber 2
  node [
    id 0
    label "pokaza&#263;"
    origin "text"
  ]
  node [
    id 1
    label "ile"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "spowodowa&#263;"
  ]
  node [
    id 4
    label "wyrazi&#263;"
  ]
  node [
    id 5
    label "da&#263;_w_ko&#347;&#263;"
  ]
  node [
    id 6
    label "przedstawi&#263;"
  ]
  node [
    id 7
    label "testify"
  ]
  node [
    id 8
    label "indicate"
  ]
  node [
    id 9
    label "przeszkoli&#263;"
  ]
  node [
    id 10
    label "udowodni&#263;"
  ]
  node [
    id 11
    label "poinformowa&#263;"
  ]
  node [
    id 12
    label "poda&#263;"
  ]
  node [
    id 13
    label "point"
  ]
  node [
    id 14
    label "si&#281;ga&#263;"
  ]
  node [
    id 15
    label "trwa&#263;"
  ]
  node [
    id 16
    label "obecno&#347;&#263;"
  ]
  node [
    id 17
    label "stan"
  ]
  node [
    id 18
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 19
    label "stand"
  ]
  node [
    id 20
    label "mie&#263;_miejsce"
  ]
  node [
    id 21
    label "uczestniczy&#263;"
  ]
  node [
    id 22
    label "chodzi&#263;"
  ]
  node [
    id 23
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 24
    label "equal"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
]
