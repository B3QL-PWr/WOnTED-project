graph [
  maxDegree 12
  minDegree 1
  meanDegree 1.935483870967742
  density 0.06451612903225806
  graphCliqueNumber 2
  node [
    id 0
    label "izraelski"
    origin "text"
  ]
  node [
    id 1
    label "prawnik"
    origin "text"
  ]
  node [
    id 2
    label "odzyskiwa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "kamienica"
    origin "text"
  ]
  node [
    id 4
    label "kuchnia"
    origin "text"
  ]
  node [
    id 5
    label "bliskowschodni"
  ]
  node [
    id 6
    label "zachodnioazjatycki"
  ]
  node [
    id 7
    label "moszaw"
  ]
  node [
    id 8
    label "azjatycki"
  ]
  node [
    id 9
    label "po_izraelsku"
  ]
  node [
    id 10
    label "prawnicy"
  ]
  node [
    id 11
    label "Machiavelli"
  ]
  node [
    id 12
    label "specjalista"
  ]
  node [
    id 13
    label "aplikant"
  ]
  node [
    id 14
    label "student"
  ]
  node [
    id 15
    label "jurysta"
  ]
  node [
    id 16
    label "sum_up"
  ]
  node [
    id 17
    label "przychodzi&#263;"
  ]
  node [
    id 18
    label "recur"
  ]
  node [
    id 19
    label "dom_wielorodzinny"
  ]
  node [
    id 20
    label "st&#243;&#322;_kuchenny"
  ]
  node [
    id 21
    label "zlewozmywak"
  ]
  node [
    id 22
    label "tajniki"
  ]
  node [
    id 23
    label "jedzenie"
  ]
  node [
    id 24
    label "instytucja"
  ]
  node [
    id 25
    label "gotowa&#263;"
  ]
  node [
    id 26
    label "kultura"
  ]
  node [
    id 27
    label "zaplecze"
  ]
  node [
    id 28
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 29
    label "pomieszczenie"
  ]
  node [
    id 30
    label "zaj&#281;cie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
]
