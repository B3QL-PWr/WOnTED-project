graph [
  maxDegree 28
  minDegree 1
  meanDegree 2
  density 0.01680672268907563
  graphCliqueNumber 2
  node [
    id 0
    label "pobli&#380;e"
    origin "text"
  ]
  node [
    id 1
    label "jeden"
    origin "text"
  ]
  node [
    id 2
    label "spalony"
    origin "text"
  ]
  node [
    id 3
    label "dom"
    origin "text"
  ]
  node [
    id 4
    label "hrabstwo"
    origin "text"
  ]
  node [
    id 5
    label "butte"
    origin "text"
  ]
  node [
    id 6
    label "p&#243;&#322;nocny"
    origin "text"
  ]
  node [
    id 7
    label "kalifornia"
    origin "text"
  ]
  node [
    id 8
    label "znale&#378;&#263;"
    origin "text"
  ]
  node [
    id 9
    label "pies"
    origin "text"
  ]
  node [
    id 10
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 11
    label "przez"
    origin "text"
  ]
  node [
    id 12
    label "kilka"
    origin "text"
  ]
  node [
    id 13
    label "tydzie&#324;"
    origin "text"
  ]
  node [
    id 14
    label "pilnowa&#263;"
    origin "text"
  ]
  node [
    id 15
    label "zgliszcze"
    origin "text"
  ]
  node [
    id 16
    label "budynek"
    origin "text"
  ]
  node [
    id 17
    label "obszar"
  ]
  node [
    id 18
    label "po_s&#261;siedzku"
  ]
  node [
    id 19
    label "kieliszek"
  ]
  node [
    id 20
    label "shot"
  ]
  node [
    id 21
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 22
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 23
    label "jaki&#347;"
  ]
  node [
    id 24
    label "jednolicie"
  ]
  node [
    id 25
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 26
    label "w&#243;dka"
  ]
  node [
    id 27
    label "ten"
  ]
  node [
    id 28
    label "ujednolicenie"
  ]
  node [
    id 29
    label "jednakowy"
  ]
  node [
    id 30
    label "zagrywka"
  ]
  node [
    id 31
    label "zgorza&#322;y"
  ]
  node [
    id 32
    label "zniszczony"
  ]
  node [
    id 33
    label "wykroczenie"
  ]
  node [
    id 34
    label "garderoba"
  ]
  node [
    id 35
    label "wiecha"
  ]
  node [
    id 36
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 37
    label "grupa"
  ]
  node [
    id 38
    label "fratria"
  ]
  node [
    id 39
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 40
    label "poj&#281;cie"
  ]
  node [
    id 41
    label "rodzina"
  ]
  node [
    id 42
    label "substancja_mieszkaniowa"
  ]
  node [
    id 43
    label "instytucja"
  ]
  node [
    id 44
    label "dom_rodzinny"
  ]
  node [
    id 45
    label "stead"
  ]
  node [
    id 46
    label "siedziba"
  ]
  node [
    id 47
    label "Yorkshire"
  ]
  node [
    id 48
    label "jednostka_administracyjna"
  ]
  node [
    id 49
    label "Norfolk"
  ]
  node [
    id 50
    label "Kornwalia"
  ]
  node [
    id 51
    label "maj&#261;tek"
  ]
  node [
    id 52
    label "mro&#378;ny"
  ]
  node [
    id 53
    label "nocny"
  ]
  node [
    id 54
    label "&#347;nie&#380;ny"
  ]
  node [
    id 55
    label "zimowy"
  ]
  node [
    id 56
    label "odzyska&#263;"
  ]
  node [
    id 57
    label "devise"
  ]
  node [
    id 58
    label "oceni&#263;"
  ]
  node [
    id 59
    label "znaj&#347;&#263;"
  ]
  node [
    id 60
    label "wymy&#347;li&#263;"
  ]
  node [
    id 61
    label "invent"
  ]
  node [
    id 62
    label "pozyska&#263;"
  ]
  node [
    id 63
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 64
    label "wykry&#263;"
  ]
  node [
    id 65
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 66
    label "dozna&#263;"
  ]
  node [
    id 67
    label "cz&#322;owiek"
  ]
  node [
    id 68
    label "wy&#263;"
  ]
  node [
    id 69
    label "ssak_drapie&#380;ny"
  ]
  node [
    id 70
    label "spragniony"
  ]
  node [
    id 71
    label "rakarz"
  ]
  node [
    id 72
    label "psowate"
  ]
  node [
    id 73
    label "istota_&#380;ywa"
  ]
  node [
    id 74
    label "kabanos"
  ]
  node [
    id 75
    label "mi&#322;o&#347;nik"
  ]
  node [
    id 76
    label "&#322;ajdak"
  ]
  node [
    id 77
    label "czworon&#243;g"
  ]
  node [
    id 78
    label "policjant"
  ]
  node [
    id 79
    label "szczucie"
  ]
  node [
    id 80
    label "s&#322;u&#380;enie"
  ]
  node [
    id 81
    label "sobaka"
  ]
  node [
    id 82
    label "dogoterapia"
  ]
  node [
    id 83
    label "Cerber"
  ]
  node [
    id 84
    label "wyzwisko"
  ]
  node [
    id 85
    label "szczu&#263;"
  ]
  node [
    id 86
    label "wycie"
  ]
  node [
    id 87
    label "szczeka&#263;"
  ]
  node [
    id 88
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 89
    label "trufla"
  ]
  node [
    id 90
    label "samiec"
  ]
  node [
    id 91
    label "piese&#322;"
  ]
  node [
    id 92
    label "zawy&#263;"
  ]
  node [
    id 93
    label "&#347;ledziowate"
  ]
  node [
    id 94
    label "ryba"
  ]
  node [
    id 95
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 96
    label "doba"
  ]
  node [
    id 97
    label "czas"
  ]
  node [
    id 98
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 99
    label "weekend"
  ]
  node [
    id 100
    label "miesi&#261;c"
  ]
  node [
    id 101
    label "zachowywa&#263;"
  ]
  node [
    id 102
    label "robi&#263;"
  ]
  node [
    id 103
    label "str&#243;&#380;y&#263;"
  ]
  node [
    id 104
    label "continue"
  ]
  node [
    id 105
    label "miejsce"
  ]
  node [
    id 106
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 107
    label "kondygnacja"
  ]
  node [
    id 108
    label "skrzyd&#322;o"
  ]
  node [
    id 109
    label "dach"
  ]
  node [
    id 110
    label "balkon"
  ]
  node [
    id 111
    label "klatka_schodowa"
  ]
  node [
    id 112
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 113
    label "pod&#322;oga"
  ]
  node [
    id 114
    label "front"
  ]
  node [
    id 115
    label "strop"
  ]
  node [
    id 116
    label "alkierz"
  ]
  node [
    id 117
    label "budowla"
  ]
  node [
    id 118
    label "Pentagon"
  ]
  node [
    id 119
    label "przedpro&#380;e"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 12
    target 94
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 95
  ]
  edge [
    source 13
    target 96
  ]
  edge [
    source 13
    target 97
  ]
  edge [
    source 13
    target 98
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 101
  ]
  edge [
    source 14
    target 102
  ]
  edge [
    source 14
    target 103
  ]
  edge [
    source 14
    target 104
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 105
  ]
  edge [
    source 15
    target 106
  ]
  edge [
    source 16
    target 107
  ]
  edge [
    source 16
    target 108
  ]
  edge [
    source 16
    target 109
  ]
  edge [
    source 16
    target 110
  ]
  edge [
    source 16
    target 111
  ]
  edge [
    source 16
    target 112
  ]
  edge [
    source 16
    target 113
  ]
  edge [
    source 16
    target 114
  ]
  edge [
    source 16
    target 115
  ]
  edge [
    source 16
    target 116
  ]
  edge [
    source 16
    target 117
  ]
  edge [
    source 16
    target 118
  ]
  edge [
    source 16
    target 119
  ]
]
