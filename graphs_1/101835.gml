graph [
  maxDegree 13
  minDegree 1
  meanDegree 2.297872340425532
  density 0.04995374653098982
  graphCliqueNumber 5
  node [
    id 0
    label "viii"
    origin "text"
  ]
  node [
    id 1
    label "mi&#281;dzynarodowy"
    origin "text"
  ]
  node [
    id 2
    label "festiwal"
    origin "text"
  ]
  node [
    id 3
    label "konkurs"
    origin "text"
  ]
  node [
    id 4
    label "m&#322;odzie&#380;owy"
    origin "text"
  ]
  node [
    id 5
    label "dziennikarstwo"
    origin "text"
  ]
  node [
    id 6
    label "sk&#322;on"
    origin "text"
  ]
  node [
    id 7
    label "dniepra"
    origin "text"
  ]
  node [
    id 8
    label "rocznik"
    origin "text"
  ]
  node [
    id 9
    label "mi&#281;dzynarodowo"
  ]
  node [
    id 10
    label "umi&#281;dzynarodowienie"
  ]
  node [
    id 11
    label "umi&#281;dzynaradawianie"
  ]
  node [
    id 12
    label "Nowe_Horyzonty"
  ]
  node [
    id 13
    label "Interwizja"
  ]
  node [
    id 14
    label "Open'er"
  ]
  node [
    id 15
    label "Przystanek_Woodstock"
  ]
  node [
    id 16
    label "impreza"
  ]
  node [
    id 17
    label "Woodstock"
  ]
  node [
    id 18
    label "Metalmania"
  ]
  node [
    id 19
    label "Opole"
  ]
  node [
    id 20
    label "FAMA"
  ]
  node [
    id 21
    label "Eurowizja"
  ]
  node [
    id 22
    label "Brutal"
  ]
  node [
    id 23
    label "eliminacje"
  ]
  node [
    id 24
    label "emulation"
  ]
  node [
    id 25
    label "casting"
  ]
  node [
    id 26
    label "nab&#243;r"
  ]
  node [
    id 27
    label "m&#322;odzie&#380;owo"
  ]
  node [
    id 28
    label "nauka_humanistyczna"
  ]
  node [
    id 29
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 30
    label "medioznawca"
  ]
  node [
    id 31
    label "ruch"
  ]
  node [
    id 32
    label "&#263;wiczenie"
  ]
  node [
    id 33
    label "slope"
  ]
  node [
    id 34
    label "formacja"
  ]
  node [
    id 35
    label "kronika"
  ]
  node [
    id 36
    label "czasopismo"
  ]
  node [
    id 37
    label "yearbook"
  ]
  node [
    id 38
    label "prasa"
  ]
  node [
    id 39
    label "wiosna"
  ]
  node [
    id 40
    label "na"
  ]
  node [
    id 41
    label "Dniepra"
  ]
  node [
    id 42
    label "junior"
  ]
  node [
    id 43
    label "PRES"
  ]
  node [
    id 44
    label "fundacja"
  ]
  node [
    id 45
    label "nowy"
  ]
  node [
    id 46
    label "medium"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 34
  ]
  edge [
    source 8
    target 35
  ]
  edge [
    source 8
    target 36
  ]
  edge [
    source 8
    target 37
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 40
  ]
  edge [
    source 38
    target 41
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 41
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 46
  ]
  edge [
    source 45
    target 46
  ]
]
