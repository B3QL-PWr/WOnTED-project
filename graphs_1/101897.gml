graph [
  maxDegree 153
  minDegree 1
  meanDegree 2.2222222222222223
  density 0.004417936823503424
  graphCliqueNumber 3
  node [
    id 0
    label "europa"
    origin "text"
  ]
  node [
    id 1
    label "by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "tym"
    origin "text"
  ]
  node [
    id 3
    label "kontynent"
    origin "text"
  ]
  node [
    id 4
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 5
    label "wyst&#281;powa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 7
    label "odsetek"
    origin "text"
  ]
  node [
    id 8
    label "grunt"
    origin "text"
  ]
  node [
    id 9
    label "orny"
    origin "text"
  ]
  node [
    id 10
    label "przeznaczy&#263;"
    origin "text"
  ]
  node [
    id 11
    label "pod"
    origin "text"
  ]
  node [
    id 12
    label "uprawa"
    origin "text"
  ]
  node [
    id 13
    label "ponad"
    origin "text"
  ]
  node [
    id 14
    label "powierzchnia"
    origin "text"
  ]
  node [
    id 15
    label "charakteryzowa&#263;"
    origin "text"
  ]
  node [
    id 16
    label "si&#281;"
    origin "text"
  ]
  node [
    id 17
    label "typ"
    origin "text"
  ]
  node [
    id 18
    label "u&#380;ytkowanie"
    origin "text"
  ]
  node [
    id 19
    label "ziemia"
    origin "text"
  ]
  node [
    id 20
    label "oznacza&#263;"
    origin "text"
  ]
  node [
    id 21
    label "znaczny"
    origin "text"
  ]
  node [
    id 22
    label "obszar"
    origin "text"
  ]
  node [
    id 23
    label "maja"
    origin "text"
  ]
  node [
    id 24
    label "dogodny"
    origin "text"
  ]
  node [
    id 25
    label "warunek"
    origin "text"
  ]
  node [
    id 26
    label "dla"
    origin "text"
  ]
  node [
    id 27
    label "prowadzenie"
    origin "text"
  ]
  node [
    id 28
    label "gospodarstwo"
    origin "text"
  ]
  node [
    id 29
    label "rolny"
    origin "text"
  ]
  node [
    id 30
    label "rozleg&#322;y"
    origin "text"
  ]
  node [
    id 31
    label "nizina"
    origin "text"
  ]
  node [
    id 32
    label "zach&#243;d"
    origin "text"
  ]
  node [
    id 33
    label "znajdowa&#263;"
    origin "text"
  ]
  node [
    id 34
    label "strefa"
    origin "text"
  ]
  node [
    id 35
    label "wp&#322;yw"
    origin "text"
  ]
  node [
    id 36
    label "klimat"
    origin "text"
  ]
  node [
    id 37
    label "umiarkowany"
    origin "text"
  ]
  node [
    id 38
    label "ciep&#322;y"
    origin "text"
  ]
  node [
    id 39
    label "morski"
    origin "text"
  ]
  node [
    id 40
    label "po&#322;udniowy"
    origin "text"
  ]
  node [
    id 41
    label "cz&#281;&#347;&#263;"
    origin "text"
  ]
  node [
    id 42
    label "pada&#324;ski"
    origin "text"
  ]
  node [
    id 43
    label "w&#281;gierski"
    origin "text"
  ]
  node [
    id 44
    label "sprzyja&#263;"
    origin "text"
  ]
  node [
    id 45
    label "zbo&#380;e"
    origin "text"
  ]
  node [
    id 46
    label "pszenica"
    origin "text"
  ]
  node [
    id 47
    label "cukrowy"
    origin "text"
  ]
  node [
    id 48
    label "si&#281;ga&#263;"
  ]
  node [
    id 49
    label "trwa&#263;"
  ]
  node [
    id 50
    label "obecno&#347;&#263;"
  ]
  node [
    id 51
    label "stan"
  ]
  node [
    id 52
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 53
    label "stand"
  ]
  node [
    id 54
    label "mie&#263;_miejsce"
  ]
  node [
    id 55
    label "uczestniczy&#263;"
  ]
  node [
    id 56
    label "chodzi&#263;"
  ]
  node [
    id 57
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 58
    label "equal"
  ]
  node [
    id 59
    label "Stary_&#346;wiat"
  ]
  node [
    id 60
    label "Antarktyda"
  ]
  node [
    id 61
    label "Germania"
  ]
  node [
    id 62
    label "Dunaj"
  ]
  node [
    id 63
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 64
    label "Europa_Zachodnia"
  ]
  node [
    id 65
    label "Afryka"
  ]
  node [
    id 66
    label "Ameryka"
  ]
  node [
    id 67
    label "Azja"
  ]
  node [
    id 68
    label "Europa_Wschodnia"
  ]
  node [
    id 69
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 70
    label "epejroforeza"
  ]
  node [
    id 71
    label "Ameryka_Centralna"
  ]
  node [
    id 72
    label "Australia"
  ]
  node [
    id 73
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 74
    label "Moza"
  ]
  node [
    id 75
    label "blok_kontynentalny"
  ]
  node [
    id 76
    label "Eurazja"
  ]
  node [
    id 77
    label "l&#261;d"
  ]
  node [
    id 78
    label "palearktyka"
  ]
  node [
    id 79
    label "Europa"
  ]
  node [
    id 80
    label "Ren"
  ]
  node [
    id 81
    label "dopiera&#263;_si&#281;"
  ]
  node [
    id 82
    label "wychodzi&#263;"
  ]
  node [
    id 83
    label "dzia&#322;a&#263;"
  ]
  node [
    id 84
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 85
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 86
    label "act"
  ]
  node [
    id 87
    label "zjawia&#263;_si&#281;"
  ]
  node [
    id 88
    label "unwrap"
  ]
  node [
    id 89
    label "seclude"
  ]
  node [
    id 90
    label "perform"
  ]
  node [
    id 91
    label "odst&#281;powa&#263;"
  ]
  node [
    id 92
    label "rezygnowa&#263;"
  ]
  node [
    id 93
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 94
    label "overture"
  ]
  node [
    id 95
    label "nak&#322;ania&#263;"
  ]
  node [
    id 96
    label "appear"
  ]
  node [
    id 97
    label "doros&#322;y"
  ]
  node [
    id 98
    label "wiele"
  ]
  node [
    id 99
    label "dorodny"
  ]
  node [
    id 100
    label "du&#380;o"
  ]
  node [
    id 101
    label "prawdziwy"
  ]
  node [
    id 102
    label "niema&#322;o"
  ]
  node [
    id 103
    label "wa&#380;ny"
  ]
  node [
    id 104
    label "rozwini&#281;ty"
  ]
  node [
    id 105
    label "ilo&#347;&#263;"
  ]
  node [
    id 106
    label "za&#322;o&#380;enie"
  ]
  node [
    id 107
    label "glinowa&#263;"
  ]
  node [
    id 108
    label "pr&#243;chnica"
  ]
  node [
    id 109
    label "podglebie"
  ]
  node [
    id 110
    label "glinowanie"
  ]
  node [
    id 111
    label "litosfera"
  ]
  node [
    id 112
    label "zasadzenie"
  ]
  node [
    id 113
    label "documentation"
  ]
  node [
    id 114
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 115
    label "teren"
  ]
  node [
    id 116
    label "ryzosfera"
  ]
  node [
    id 117
    label "czynnik_produkcji"
  ]
  node [
    id 118
    label "zasadzi&#263;"
  ]
  node [
    id 119
    label "glej"
  ]
  node [
    id 120
    label "martwica"
  ]
  node [
    id 121
    label "geosystem"
  ]
  node [
    id 122
    label "przestrze&#324;"
  ]
  node [
    id 123
    label "dotleni&#263;"
  ]
  node [
    id 124
    label "penetrator"
  ]
  node [
    id 125
    label "punkt_odniesienia"
  ]
  node [
    id 126
    label "kompleks_sorpcyjny"
  ]
  node [
    id 127
    label "podstawowy"
  ]
  node [
    id 128
    label "plantowa&#263;"
  ]
  node [
    id 129
    label "podk&#322;ad"
  ]
  node [
    id 130
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 131
    label "dno"
  ]
  node [
    id 132
    label "oromy"
  ]
  node [
    id 133
    label "uprawny"
  ]
  node [
    id 134
    label "sta&#263;_si&#281;"
  ]
  node [
    id 135
    label "appoint"
  ]
  node [
    id 136
    label "zrobi&#263;"
  ]
  node [
    id 137
    label "ustali&#263;"
  ]
  node [
    id 138
    label "oblat"
  ]
  node [
    id 139
    label "praca_rolnicza"
  ]
  node [
    id 140
    label "bezglebowy"
  ]
  node [
    id 141
    label "uprawa_roli"
  ]
  node [
    id 142
    label "brzoskwiniarnia"
  ]
  node [
    id 143
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 144
    label "capacity"
  ]
  node [
    id 145
    label "zwierciad&#322;o"
  ]
  node [
    id 146
    label "rozmiar"
  ]
  node [
    id 147
    label "liofilowo&#347;&#263;"
  ]
  node [
    id 148
    label "poj&#281;cie"
  ]
  node [
    id 149
    label "plane"
  ]
  node [
    id 150
    label "report"
  ]
  node [
    id 151
    label "opisywa&#263;"
  ]
  node [
    id 152
    label "mark"
  ]
  node [
    id 153
    label "przygotowywa&#263;"
  ]
  node [
    id 154
    label "cechowa&#263;"
  ]
  node [
    id 155
    label "cz&#322;owiek"
  ]
  node [
    id 156
    label "gromada"
  ]
  node [
    id 157
    label "autorament"
  ]
  node [
    id 158
    label "przypuszczenie"
  ]
  node [
    id 159
    label "cynk"
  ]
  node [
    id 160
    label "rezultat"
  ]
  node [
    id 161
    label "jednostka_systematyczna"
  ]
  node [
    id 162
    label "kr&#243;lestwo"
  ]
  node [
    id 163
    label "obstawia&#263;"
  ]
  node [
    id 164
    label "design"
  ]
  node [
    id 165
    label "facet"
  ]
  node [
    id 166
    label "variety"
  ]
  node [
    id 167
    label "sztuka"
  ]
  node [
    id 168
    label "antycypacja"
  ]
  node [
    id 169
    label "use"
  ]
  node [
    id 170
    label "wydobywanie"
  ]
  node [
    id 171
    label "stosowanie"
  ]
  node [
    id 172
    label "zaje&#380;d&#380;anie"
  ]
  node [
    id 173
    label "anektowanie"
  ]
  node [
    id 174
    label "occupation"
  ]
  node [
    id 175
    label "Skandynawia"
  ]
  node [
    id 176
    label "Yorkshire"
  ]
  node [
    id 177
    label "Kaukaz"
  ]
  node [
    id 178
    label "Kaszmir"
  ]
  node [
    id 179
    label "Toskania"
  ]
  node [
    id 180
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 181
    label "&#321;emkowszczyzna"
  ]
  node [
    id 182
    label "Amhara"
  ]
  node [
    id 183
    label "Lombardia"
  ]
  node [
    id 184
    label "Podbeskidzie"
  ]
  node [
    id 185
    label "Kalabria"
  ]
  node [
    id 186
    label "kort"
  ]
  node [
    id 187
    label "Tyrol"
  ]
  node [
    id 188
    label "Pamir"
  ]
  node [
    id 189
    label "Lubelszczyzna"
  ]
  node [
    id 190
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 191
    label "&#379;ywiecczyzna"
  ]
  node [
    id 192
    label "Zabajkale"
  ]
  node [
    id 193
    label "Kaszuby"
  ]
  node [
    id 194
    label "Bo&#347;nia"
  ]
  node [
    id 195
    label "Noworosja"
  ]
  node [
    id 196
    label "Ba&#322;kany"
  ]
  node [
    id 197
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 198
    label "Anglia"
  ]
  node [
    id 199
    label "Kielecczyzna"
  ]
  node [
    id 200
    label "Pomorze_Zachodnie"
  ]
  node [
    id 201
    label "Opolskie"
  ]
  node [
    id 202
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 203
    label "skorupa_ziemska"
  ]
  node [
    id 204
    label "Ko&#322;yma"
  ]
  node [
    id 205
    label "Oksytania"
  ]
  node [
    id 206
    label "Syjon"
  ]
  node [
    id 207
    label "posadzka"
  ]
  node [
    id 208
    label "pa&#324;stwo"
  ]
  node [
    id 209
    label "Kociewie"
  ]
  node [
    id 210
    label "Huculszczyzna"
  ]
  node [
    id 211
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 212
    label "budynek"
  ]
  node [
    id 213
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 214
    label "Bawaria"
  ]
  node [
    id 215
    label "pomieszczenie"
  ]
  node [
    id 216
    label "Maghreb"
  ]
  node [
    id 217
    label "Bory_Tucholskie"
  ]
  node [
    id 218
    label "Kerala"
  ]
  node [
    id 219
    label "Podhale"
  ]
  node [
    id 220
    label "Kabylia"
  ]
  node [
    id 221
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 222
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 223
    label "Ma&#322;opolska"
  ]
  node [
    id 224
    label "Polesie"
  ]
  node [
    id 225
    label "Liguria"
  ]
  node [
    id 226
    label "&#321;&#243;dzkie"
  ]
  node [
    id 227
    label "Palestyna"
  ]
  node [
    id 228
    label "Bojkowszczyzna"
  ]
  node [
    id 229
    label "Karaiby"
  ]
  node [
    id 230
    label "S&#261;decczyzna"
  ]
  node [
    id 231
    label "Sand&#380;ak"
  ]
  node [
    id 232
    label "Nadrenia"
  ]
  node [
    id 233
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 234
    label "Zakarpacie"
  ]
  node [
    id 235
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 236
    label "Zag&#243;rze"
  ]
  node [
    id 237
    label "Andaluzja"
  ]
  node [
    id 238
    label "Turkiestan"
  ]
  node [
    id 239
    label "Naddniestrze"
  ]
  node [
    id 240
    label "Hercegowina"
  ]
  node [
    id 241
    label "p&#322;aszczyzna"
  ]
  node [
    id 242
    label "Opolszczyzna"
  ]
  node [
    id 243
    label "jednostka_administracyjna"
  ]
  node [
    id 244
    label "Lotaryngia"
  ]
  node [
    id 245
    label "Afryka_Wschodnia"
  ]
  node [
    id 246
    label "Szlezwik"
  ]
  node [
    id 247
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 248
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 249
    label "Mazowsze"
  ]
  node [
    id 250
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 251
    label "Afryka_Zachodnia"
  ]
  node [
    id 252
    label "Galicja"
  ]
  node [
    id 253
    label "Szkocja"
  ]
  node [
    id 254
    label "Walia"
  ]
  node [
    id 255
    label "Powi&#347;le"
  ]
  node [
    id 256
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 257
    label "Zamojszczyzna"
  ]
  node [
    id 258
    label "Kujawy"
  ]
  node [
    id 259
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 260
    label "Podlasie"
  ]
  node [
    id 261
    label "Laponia"
  ]
  node [
    id 262
    label "Umbria"
  ]
  node [
    id 263
    label "Mezoameryka"
  ]
  node [
    id 264
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 265
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 266
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 267
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 268
    label "Kurdystan"
  ]
  node [
    id 269
    label "Kampania"
  ]
  node [
    id 270
    label "Armagnac"
  ]
  node [
    id 271
    label "Polinezja"
  ]
  node [
    id 272
    label "Warmia"
  ]
  node [
    id 273
    label "Wielkopolska"
  ]
  node [
    id 274
    label "Bordeaux"
  ]
  node [
    id 275
    label "Lauda"
  ]
  node [
    id 276
    label "Mazury"
  ]
  node [
    id 277
    label "Podkarpacie"
  ]
  node [
    id 278
    label "Oceania"
  ]
  node [
    id 279
    label "Lasko"
  ]
  node [
    id 280
    label "Amazonia"
  ]
  node [
    id 281
    label "pojazd"
  ]
  node [
    id 282
    label "zapadnia"
  ]
  node [
    id 283
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 284
    label "Kurpie"
  ]
  node [
    id 285
    label "Tonkin"
  ]
  node [
    id 286
    label "Azja_Wschodnia"
  ]
  node [
    id 287
    label "Mikronezja"
  ]
  node [
    id 288
    label "Ukraina_Zachodnia"
  ]
  node [
    id 289
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 290
    label "Turyngia"
  ]
  node [
    id 291
    label "Baszkiria"
  ]
  node [
    id 292
    label "Apulia"
  ]
  node [
    id 293
    label "miejsce"
  ]
  node [
    id 294
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 295
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 296
    label "Indochiny"
  ]
  node [
    id 297
    label "Biskupizna"
  ]
  node [
    id 298
    label "Lubuskie"
  ]
  node [
    id 299
    label "domain"
  ]
  node [
    id 300
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 301
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 302
    label "okre&#347;la&#263;"
  ]
  node [
    id 303
    label "represent"
  ]
  node [
    id 304
    label "wyraz"
  ]
  node [
    id 305
    label "wskazywa&#263;"
  ]
  node [
    id 306
    label "stanowi&#263;"
  ]
  node [
    id 307
    label "signify"
  ]
  node [
    id 308
    label "set"
  ]
  node [
    id 309
    label "ustala&#263;"
  ]
  node [
    id 310
    label "znacznie"
  ]
  node [
    id 311
    label "zauwa&#380;alny"
  ]
  node [
    id 312
    label "pas_planetoid"
  ]
  node [
    id 313
    label "wsch&#243;d"
  ]
  node [
    id 314
    label "Neogea"
  ]
  node [
    id 315
    label "holarktyka"
  ]
  node [
    id 316
    label "Rakowice"
  ]
  node [
    id 317
    label "Kosowo"
  ]
  node [
    id 318
    label "Syberia_Wschodnia"
  ]
  node [
    id 319
    label "wymiar"
  ]
  node [
    id 320
    label "p&#243;&#322;noc"
  ]
  node [
    id 321
    label "akrecja"
  ]
  node [
    id 322
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 323
    label "terytorium"
  ]
  node [
    id 324
    label "antroposfera"
  ]
  node [
    id 325
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 326
    label "po&#322;udnie"
  ]
  node [
    id 327
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 328
    label "Olszanica"
  ]
  node [
    id 329
    label "Syberia_Zachodnia"
  ]
  node [
    id 330
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 331
    label "Ruda_Pabianicka"
  ]
  node [
    id 332
    label "Notogea"
  ]
  node [
    id 333
    label "&#321;&#281;g"
  ]
  node [
    id 334
    label "Antarktyka"
  ]
  node [
    id 335
    label "Piotrowo"
  ]
  node [
    id 336
    label "Zab&#322;ocie"
  ]
  node [
    id 337
    label "zakres"
  ]
  node [
    id 338
    label "Pow&#261;zki"
  ]
  node [
    id 339
    label "Arktyka"
  ]
  node [
    id 340
    label "zbi&#243;r"
  ]
  node [
    id 341
    label "Ludwin&#243;w"
  ]
  node [
    id 342
    label "Zabu&#380;e"
  ]
  node [
    id 343
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 344
    label "Kresy_Zachodnie"
  ]
  node [
    id 345
    label "wedyzm"
  ]
  node [
    id 346
    label "energia"
  ]
  node [
    id 347
    label "buddyzm"
  ]
  node [
    id 348
    label "odpowiedni"
  ]
  node [
    id 349
    label "dobry"
  ]
  node [
    id 350
    label "dogodnie"
  ]
  node [
    id 351
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 352
    label "umowa"
  ]
  node [
    id 353
    label "agent"
  ]
  node [
    id 354
    label "condition"
  ]
  node [
    id 355
    label "ekspozycja"
  ]
  node [
    id 356
    label "faktor"
  ]
  node [
    id 357
    label "robienie"
  ]
  node [
    id 358
    label "przywodzenie"
  ]
  node [
    id 359
    label "prowadzanie"
  ]
  node [
    id 360
    label "ukierunkowywanie"
  ]
  node [
    id 361
    label "kszta&#322;towanie"
  ]
  node [
    id 362
    label "poprowadzenie"
  ]
  node [
    id 363
    label "wprowadzanie"
  ]
  node [
    id 364
    label "dysponowanie"
  ]
  node [
    id 365
    label "przeci&#261;ganie"
  ]
  node [
    id 366
    label "doprowadzanie"
  ]
  node [
    id 367
    label "wprowadzenie"
  ]
  node [
    id 368
    label "eksponowanie"
  ]
  node [
    id 369
    label "oprowadzenie"
  ]
  node [
    id 370
    label "trzymanie"
  ]
  node [
    id 371
    label "ta&#324;czenie"
  ]
  node [
    id 372
    label "przeci&#281;cie"
  ]
  node [
    id 373
    label "przewy&#380;szanie"
  ]
  node [
    id 374
    label "prowadzi&#263;"
  ]
  node [
    id 375
    label "aim"
  ]
  node [
    id 376
    label "czynno&#347;&#263;"
  ]
  node [
    id 377
    label "zwracanie"
  ]
  node [
    id 378
    label "ci&#261;gni&#281;cie_si&#281;"
  ]
  node [
    id 379
    label "przecinanie"
  ]
  node [
    id 380
    label "sterowanie"
  ]
  node [
    id 381
    label "drive"
  ]
  node [
    id 382
    label "kre&#347;lenie"
  ]
  node [
    id 383
    label "management"
  ]
  node [
    id 384
    label "dawanie"
  ]
  node [
    id 385
    label "oprowadzanie"
  ]
  node [
    id 386
    label "pozarz&#261;dzanie"
  ]
  node [
    id 387
    label "g&#243;rowanie"
  ]
  node [
    id 388
    label "linia_melodyczna"
  ]
  node [
    id 389
    label "granie"
  ]
  node [
    id 390
    label "doprowadzenie"
  ]
  node [
    id 391
    label "kierowanie"
  ]
  node [
    id 392
    label "zaprowadzanie"
  ]
  node [
    id 393
    label "lead"
  ]
  node [
    id 394
    label "powodowanie"
  ]
  node [
    id 395
    label "krzywa"
  ]
  node [
    id 396
    label "gospodarowanie"
  ]
  node [
    id 397
    label "pole"
  ]
  node [
    id 398
    label "miejsce_pracy"
  ]
  node [
    id 399
    label "grupa"
  ]
  node [
    id 400
    label "gospodarowa&#263;"
  ]
  node [
    id 401
    label "spichlerz"
  ]
  node [
    id 402
    label "mienie"
  ]
  node [
    id 403
    label "inwentarz"
  ]
  node [
    id 404
    label "dom_rodzinny"
  ]
  node [
    id 405
    label "dom"
  ]
  node [
    id 406
    label "obora"
  ]
  node [
    id 407
    label "stodo&#322;a"
  ]
  node [
    id 408
    label "specjalny"
  ]
  node [
    id 409
    label "rolniczo"
  ]
  node [
    id 410
    label "szeroki"
  ]
  node [
    id 411
    label "rozlegle"
  ]
  node [
    id 412
    label "Pampa"
  ]
  node [
    id 413
    label "Nizina_Nadwi&#347;la&#324;ska"
  ]
  node [
    id 414
    label "Wybrze&#380;e_Koromandelskie"
  ]
  node [
    id 415
    label "s&#322;o&#324;ce"
  ]
  node [
    id 416
    label "usi&#322;owanie"
  ]
  node [
    id 417
    label "trud"
  ]
  node [
    id 418
    label "sunset"
  ]
  node [
    id 419
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 420
    label "wiecz&#243;r"
  ]
  node [
    id 421
    label "strona_&#347;wiata"
  ]
  node [
    id 422
    label "pora"
  ]
  node [
    id 423
    label "zjawisko"
  ]
  node [
    id 424
    label "szar&#243;wka"
  ]
  node [
    id 425
    label "doznawa&#263;"
  ]
  node [
    id 426
    label "znachodzi&#263;"
  ]
  node [
    id 427
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 428
    label "pozyskiwa&#263;"
  ]
  node [
    id 429
    label "odzyskiwa&#263;"
  ]
  node [
    id 430
    label "os&#261;dza&#263;"
  ]
  node [
    id 431
    label "wykrywa&#263;"
  ]
  node [
    id 432
    label "detect"
  ]
  node [
    id 433
    label "wymy&#347;la&#263;"
  ]
  node [
    id 434
    label "powodowa&#263;"
  ]
  node [
    id 435
    label "obrona_strefowa"
  ]
  node [
    id 436
    label "&#347;lad"
  ]
  node [
    id 437
    label "doch&#243;d_narodowy"
  ]
  node [
    id 438
    label "kwota"
  ]
  node [
    id 439
    label "lobbysta"
  ]
  node [
    id 440
    label "zesp&#243;&#322;"
  ]
  node [
    id 441
    label "styl"
  ]
  node [
    id 442
    label "atmosfera"
  ]
  node [
    id 443
    label "umiarkowanie"
  ]
  node [
    id 444
    label "&#347;rednio"
  ]
  node [
    id 445
    label "ostro&#380;ny"
  ]
  node [
    id 446
    label "rozwa&#380;ny"
  ]
  node [
    id 447
    label "zagrzanie"
  ]
  node [
    id 448
    label "przyjemny"
  ]
  node [
    id 449
    label "ocieplenie"
  ]
  node [
    id 450
    label "korzystny"
  ]
  node [
    id 451
    label "ocieplanie_si&#281;"
  ]
  node [
    id 452
    label "grzanie"
  ]
  node [
    id 453
    label "ocieplenie_si&#281;"
  ]
  node [
    id 454
    label "ocieplanie"
  ]
  node [
    id 455
    label "mi&#322;y"
  ]
  node [
    id 456
    label "ciep&#322;o"
  ]
  node [
    id 457
    label "niebieski"
  ]
  node [
    id 458
    label "nadmorski"
  ]
  node [
    id 459
    label "morsko"
  ]
  node [
    id 460
    label "wodny"
  ]
  node [
    id 461
    label "s&#322;ony"
  ]
  node [
    id 462
    label "zielony"
  ]
  node [
    id 463
    label "przypominaj&#261;cy"
  ]
  node [
    id 464
    label "typowy"
  ]
  node [
    id 465
    label "gor&#261;cy"
  ]
  node [
    id 466
    label "s&#322;oneczny"
  ]
  node [
    id 467
    label "po&#322;udniowo"
  ]
  node [
    id 468
    label "whole"
  ]
  node [
    id 469
    label "Rzym_Zachodni"
  ]
  node [
    id 470
    label "element"
  ]
  node [
    id 471
    label "urz&#261;dzenie"
  ]
  node [
    id 472
    label "Rzym_Wschodni"
  ]
  node [
    id 473
    label "w&#281;giersko"
  ]
  node [
    id 474
    label "czardasz"
  ]
  node [
    id 475
    label "salami"
  ]
  node [
    id 476
    label "toka&#324;"
  ]
  node [
    id 477
    label "j&#281;zyk"
  ]
  node [
    id 478
    label "europejski"
  ]
  node [
    id 479
    label "nale&#347;nik_Gundel"
  ]
  node [
    id 480
    label "po_w&#281;giersku"
  ]
  node [
    id 481
    label "turanizm"
  ]
  node [
    id 482
    label "Hungarian"
  ]
  node [
    id 483
    label "j&#281;zyk_ugryjski"
  ]
  node [
    id 484
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 485
    label "czu&#263;"
  ]
  node [
    id 486
    label "chowa&#263;"
  ]
  node [
    id 487
    label "gluten"
  ]
  node [
    id 488
    label "produkt"
  ]
  node [
    id 489
    label "ziarno"
  ]
  node [
    id 490
    label "k&#322;os"
  ]
  node [
    id 491
    label "ro&#347;lina"
  ]
  node [
    id 492
    label "mlewnik"
  ]
  node [
    id 493
    label "s&#261;siek"
  ]
  node [
    id 494
    label "trawa"
  ]
  node [
    id 495
    label "pszen&#380;yto"
  ]
  node [
    id 496
    label "s&#322;odki"
  ]
  node [
    id 497
    label "europ"
  ]
  node [
    id 498
    label "wschodni"
  ]
  node [
    id 499
    label "wielki"
  ]
  node [
    id 500
    label "brytania"
  ]
  node [
    id 501
    label "p&#243;&#322;wysep"
  ]
  node [
    id 502
    label "skandynawski"
  ]
  node [
    id 503
    label "wschodnioeuropejski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 21
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 14
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 8
    target 122
  ]
  edge [
    source 8
    target 123
  ]
  edge [
    source 8
    target 124
  ]
  edge [
    source 8
    target 125
  ]
  edge [
    source 8
    target 126
  ]
  edge [
    source 8
    target 127
  ]
  edge [
    source 8
    target 128
  ]
  edge [
    source 8
    target 129
  ]
  edge [
    source 8
    target 130
  ]
  edge [
    source 8
    target 131
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 132
  ]
  edge [
    source 9
    target 133
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 134
  ]
  edge [
    source 10
    target 135
  ]
  edge [
    source 10
    target 136
  ]
  edge [
    source 10
    target 137
  ]
  edge [
    source 10
    target 138
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 44
  ]
  edge [
    source 12
    target 45
  ]
  edge [
    source 12
    target 139
  ]
  edge [
    source 12
    target 140
  ]
  edge [
    source 12
    target 141
  ]
  edge [
    source 12
    target 115
  ]
  edge [
    source 12
    target 142
  ]
  edge [
    source 12
    target 143
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 144
  ]
  edge [
    source 14
    target 22
  ]
  edge [
    source 14
    target 145
  ]
  edge [
    source 14
    target 146
  ]
  edge [
    source 14
    target 147
  ]
  edge [
    source 14
    target 148
  ]
  edge [
    source 14
    target 149
  ]
  edge [
    source 14
    target 19
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 150
  ]
  edge [
    source 15
    target 151
  ]
  edge [
    source 15
    target 152
  ]
  edge [
    source 15
    target 153
  ]
  edge [
    source 15
    target 154
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 33
  ]
  edge [
    source 16
    target 34
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 155
  ]
  edge [
    source 17
    target 156
  ]
  edge [
    source 17
    target 157
  ]
  edge [
    source 17
    target 158
  ]
  edge [
    source 17
    target 159
  ]
  edge [
    source 17
    target 160
  ]
  edge [
    source 17
    target 161
  ]
  edge [
    source 17
    target 162
  ]
  edge [
    source 17
    target 163
  ]
  edge [
    source 17
    target 164
  ]
  edge [
    source 17
    target 165
  ]
  edge [
    source 17
    target 166
  ]
  edge [
    source 17
    target 167
  ]
  edge [
    source 17
    target 168
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 169
  ]
  edge [
    source 18
    target 170
  ]
  edge [
    source 18
    target 171
  ]
  edge [
    source 18
    target 172
  ]
  edge [
    source 18
    target 173
  ]
  edge [
    source 18
    target 174
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 175
  ]
  edge [
    source 19
    target 176
  ]
  edge [
    source 19
    target 177
  ]
  edge [
    source 19
    target 178
  ]
  edge [
    source 19
    target 179
  ]
  edge [
    source 19
    target 180
  ]
  edge [
    source 19
    target 181
  ]
  edge [
    source 19
    target 22
  ]
  edge [
    source 19
    target 182
  ]
  edge [
    source 19
    target 183
  ]
  edge [
    source 19
    target 184
  ]
  edge [
    source 19
    target 185
  ]
  edge [
    source 19
    target 186
  ]
  edge [
    source 19
    target 187
  ]
  edge [
    source 19
    target 188
  ]
  edge [
    source 19
    target 189
  ]
  edge [
    source 19
    target 190
  ]
  edge [
    source 19
    target 63
  ]
  edge [
    source 19
    target 191
  ]
  edge [
    source 19
    target 116
  ]
  edge [
    source 19
    target 68
  ]
  edge [
    source 19
    target 69
  ]
  edge [
    source 19
    target 192
  ]
  edge [
    source 19
    target 193
  ]
  edge [
    source 19
    target 194
  ]
  edge [
    source 19
    target 195
  ]
  edge [
    source 19
    target 196
  ]
  edge [
    source 19
    target 197
  ]
  edge [
    source 19
    target 198
  ]
  edge [
    source 19
    target 199
  ]
  edge [
    source 19
    target 200
  ]
  edge [
    source 19
    target 201
  ]
  edge [
    source 19
    target 202
  ]
  edge [
    source 19
    target 203
  ]
  edge [
    source 19
    target 204
  ]
  edge [
    source 19
    target 205
  ]
  edge [
    source 19
    target 206
  ]
  edge [
    source 19
    target 207
  ]
  edge [
    source 19
    target 208
  ]
  edge [
    source 19
    target 209
  ]
  edge [
    source 19
    target 210
  ]
  edge [
    source 19
    target 211
  ]
  edge [
    source 19
    target 212
  ]
  edge [
    source 19
    target 213
  ]
  edge [
    source 19
    target 214
  ]
  edge [
    source 19
    target 215
  ]
  edge [
    source 19
    target 108
  ]
  edge [
    source 19
    target 110
  ]
  edge [
    source 19
    target 216
  ]
  edge [
    source 19
    target 217
  ]
  edge [
    source 19
    target 64
  ]
  edge [
    source 19
    target 218
  ]
  edge [
    source 19
    target 219
  ]
  edge [
    source 19
    target 220
  ]
  edge [
    source 19
    target 221
  ]
  edge [
    source 19
    target 222
  ]
  edge [
    source 19
    target 223
  ]
  edge [
    source 19
    target 224
  ]
  edge [
    source 19
    target 225
  ]
  edge [
    source 19
    target 226
  ]
  edge [
    source 19
    target 121
  ]
  edge [
    source 19
    target 227
  ]
  edge [
    source 19
    target 228
  ]
  edge [
    source 19
    target 73
  ]
  edge [
    source 19
    target 229
  ]
  edge [
    source 19
    target 230
  ]
  edge [
    source 19
    target 231
  ]
  edge [
    source 19
    target 232
  ]
  edge [
    source 19
    target 233
  ]
  edge [
    source 19
    target 234
  ]
  edge [
    source 19
    target 130
  ]
  edge [
    source 19
    target 235
  ]
  edge [
    source 19
    target 236
  ]
  edge [
    source 19
    target 237
  ]
  edge [
    source 19
    target 238
  ]
  edge [
    source 19
    target 239
  ]
  edge [
    source 19
    target 240
  ]
  edge [
    source 19
    target 241
  ]
  edge [
    source 19
    target 242
  ]
  edge [
    source 19
    target 243
  ]
  edge [
    source 19
    target 244
  ]
  edge [
    source 19
    target 245
  ]
  edge [
    source 19
    target 246
  ]
  edge [
    source 19
    target 107
  ]
  edge [
    source 19
    target 247
  ]
  edge [
    source 19
    target 248
  ]
  edge [
    source 19
    target 109
  ]
  edge [
    source 19
    target 249
  ]
  edge [
    source 19
    target 250
  ]
  edge [
    source 19
    target 115
  ]
  edge [
    source 19
    target 251
  ]
  edge [
    source 19
    target 117
  ]
  edge [
    source 19
    target 252
  ]
  edge [
    source 19
    target 253
  ]
  edge [
    source 19
    target 254
  ]
  edge [
    source 19
    target 255
  ]
  edge [
    source 19
    target 124
  ]
  edge [
    source 19
    target 256
  ]
  edge [
    source 19
    target 126
  ]
  edge [
    source 19
    target 257
  ]
  edge [
    source 19
    target 258
  ]
  edge [
    source 19
    target 259
  ]
  edge [
    source 19
    target 260
  ]
  edge [
    source 19
    target 261
  ]
  edge [
    source 19
    target 262
  ]
  edge [
    source 19
    target 128
  ]
  edge [
    source 19
    target 263
  ]
  edge [
    source 19
    target 264
  ]
  edge [
    source 19
    target 265
  ]
  edge [
    source 19
    target 266
  ]
  edge [
    source 19
    target 267
  ]
  edge [
    source 19
    target 268
  ]
  edge [
    source 19
    target 269
  ]
  edge [
    source 19
    target 270
  ]
  edge [
    source 19
    target 271
  ]
  edge [
    source 19
    target 272
  ]
  edge [
    source 19
    target 273
  ]
  edge [
    source 19
    target 111
  ]
  edge [
    source 19
    target 274
  ]
  edge [
    source 19
    target 275
  ]
  edge [
    source 19
    target 276
  ]
  edge [
    source 19
    target 277
  ]
  edge [
    source 19
    target 278
  ]
  edge [
    source 19
    target 279
  ]
  edge [
    source 19
    target 280
  ]
  edge [
    source 19
    target 281
  ]
  edge [
    source 19
    target 119
  ]
  edge [
    source 19
    target 120
  ]
  edge [
    source 19
    target 282
  ]
  edge [
    source 19
    target 122
  ]
  edge [
    source 19
    target 283
  ]
  edge [
    source 19
    target 123
  ]
  edge [
    source 19
    target 284
  ]
  edge [
    source 19
    target 285
  ]
  edge [
    source 19
    target 286
  ]
  edge [
    source 19
    target 287
  ]
  edge [
    source 19
    target 288
  ]
  edge [
    source 19
    target 289
  ]
  edge [
    source 19
    target 290
  ]
  edge [
    source 19
    target 291
  ]
  edge [
    source 19
    target 292
  ]
  edge [
    source 19
    target 293
  ]
  edge [
    source 19
    target 294
  ]
  edge [
    source 19
    target 295
  ]
  edge [
    source 19
    target 296
  ]
  edge [
    source 19
    target 297
  ]
  edge [
    source 19
    target 298
  ]
  edge [
    source 19
    target 299
  ]
  edge [
    source 19
    target 300
  ]
  edge [
    source 19
    target 301
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 302
  ]
  edge [
    source 20
    target 303
  ]
  edge [
    source 20
    target 304
  ]
  edge [
    source 20
    target 305
  ]
  edge [
    source 20
    target 306
  ]
  edge [
    source 20
    target 307
  ]
  edge [
    source 20
    target 308
  ]
  edge [
    source 20
    target 309
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 310
  ]
  edge [
    source 21
    target 311
  ]
  edge [
    source 21
    target 103
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 312
  ]
  edge [
    source 22
    target 313
  ]
  edge [
    source 22
    target 314
  ]
  edge [
    source 22
    target 315
  ]
  edge [
    source 22
    target 316
  ]
  edge [
    source 22
    target 317
  ]
  edge [
    source 22
    target 318
  ]
  edge [
    source 22
    target 319
  ]
  edge [
    source 22
    target 320
  ]
  edge [
    source 22
    target 321
  ]
  edge [
    source 22
    target 322
  ]
  edge [
    source 22
    target 114
  ]
  edge [
    source 22
    target 323
  ]
  edge [
    source 22
    target 324
  ]
  edge [
    source 22
    target 325
  ]
  edge [
    source 22
    target 326
  ]
  edge [
    source 22
    target 32
  ]
  edge [
    source 22
    target 327
  ]
  edge [
    source 22
    target 328
  ]
  edge [
    source 22
    target 329
  ]
  edge [
    source 22
    target 122
  ]
  edge [
    source 22
    target 330
  ]
  edge [
    source 22
    target 331
  ]
  edge [
    source 22
    target 332
  ]
  edge [
    source 22
    target 333
  ]
  edge [
    source 22
    target 334
  ]
  edge [
    source 22
    target 335
  ]
  edge [
    source 22
    target 336
  ]
  edge [
    source 22
    target 337
  ]
  edge [
    source 22
    target 293
  ]
  edge [
    source 22
    target 338
  ]
  edge [
    source 22
    target 339
  ]
  edge [
    source 22
    target 340
  ]
  edge [
    source 22
    target 341
  ]
  edge [
    source 22
    target 342
  ]
  edge [
    source 22
    target 343
  ]
  edge [
    source 22
    target 344
  ]
  edge [
    source 22
    target 31
  ]
  edge [
    source 22
    target 34
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 345
  ]
  edge [
    source 23
    target 346
  ]
  edge [
    source 23
    target 347
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 348
  ]
  edge [
    source 24
    target 349
  ]
  edge [
    source 24
    target 350
  ]
  edge [
    source 24
    target 42
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 106
  ]
  edge [
    source 25
    target 351
  ]
  edge [
    source 25
    target 352
  ]
  edge [
    source 25
    target 353
  ]
  edge [
    source 25
    target 354
  ]
  edge [
    source 25
    target 355
  ]
  edge [
    source 25
    target 356
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 357
  ]
  edge [
    source 27
    target 358
  ]
  edge [
    source 27
    target 359
  ]
  edge [
    source 27
    target 360
  ]
  edge [
    source 27
    target 361
  ]
  edge [
    source 27
    target 362
  ]
  edge [
    source 27
    target 363
  ]
  edge [
    source 27
    target 364
  ]
  edge [
    source 27
    target 365
  ]
  edge [
    source 27
    target 366
  ]
  edge [
    source 27
    target 367
  ]
  edge [
    source 27
    target 368
  ]
  edge [
    source 27
    target 369
  ]
  edge [
    source 27
    target 370
  ]
  edge [
    source 27
    target 371
  ]
  edge [
    source 27
    target 372
  ]
  edge [
    source 27
    target 373
  ]
  edge [
    source 27
    target 374
  ]
  edge [
    source 27
    target 375
  ]
  edge [
    source 27
    target 376
  ]
  edge [
    source 27
    target 377
  ]
  edge [
    source 27
    target 378
  ]
  edge [
    source 27
    target 379
  ]
  edge [
    source 27
    target 380
  ]
  edge [
    source 27
    target 381
  ]
  edge [
    source 27
    target 382
  ]
  edge [
    source 27
    target 383
  ]
  edge [
    source 27
    target 384
  ]
  edge [
    source 27
    target 385
  ]
  edge [
    source 27
    target 386
  ]
  edge [
    source 27
    target 387
  ]
  edge [
    source 27
    target 388
  ]
  edge [
    source 27
    target 389
  ]
  edge [
    source 27
    target 390
  ]
  edge [
    source 27
    target 391
  ]
  edge [
    source 27
    target 392
  ]
  edge [
    source 27
    target 393
  ]
  edge [
    source 27
    target 394
  ]
  edge [
    source 27
    target 395
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 396
  ]
  edge [
    source 28
    target 397
  ]
  edge [
    source 28
    target 398
  ]
  edge [
    source 28
    target 399
  ]
  edge [
    source 28
    target 400
  ]
  edge [
    source 28
    target 401
  ]
  edge [
    source 28
    target 402
  ]
  edge [
    source 28
    target 403
  ]
  edge [
    source 28
    target 404
  ]
  edge [
    source 28
    target 405
  ]
  edge [
    source 28
    target 406
  ]
  edge [
    source 28
    target 407
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 408
  ]
  edge [
    source 29
    target 409
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 410
  ]
  edge [
    source 30
    target 411
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 39
  ]
  edge [
    source 31
    target 40
  ]
  edge [
    source 31
    target 412
  ]
  edge [
    source 31
    target 413
  ]
  edge [
    source 31
    target 77
  ]
  edge [
    source 31
    target 414
  ]
  edge [
    source 31
    target 503
  ]
  edge [
    source 32
    target 415
  ]
  edge [
    source 32
    target 416
  ]
  edge [
    source 32
    target 417
  ]
  edge [
    source 32
    target 418
  ]
  edge [
    source 32
    target 419
  ]
  edge [
    source 32
    target 420
  ]
  edge [
    source 32
    target 421
  ]
  edge [
    source 32
    target 41
  ]
  edge [
    source 32
    target 422
  ]
  edge [
    source 32
    target 423
  ]
  edge [
    source 32
    target 424
  ]
  edge [
    source 33
    target 425
  ]
  edge [
    source 33
    target 426
  ]
  edge [
    source 33
    target 427
  ]
  edge [
    source 33
    target 428
  ]
  edge [
    source 33
    target 429
  ]
  edge [
    source 33
    target 430
  ]
  edge [
    source 33
    target 431
  ]
  edge [
    source 33
    target 88
  ]
  edge [
    source 33
    target 432
  ]
  edge [
    source 33
    target 433
  ]
  edge [
    source 33
    target 434
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 41
  ]
  edge [
    source 34
    target 435
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 436
  ]
  edge [
    source 35
    target 437
  ]
  edge [
    source 35
    target 423
  ]
  edge [
    source 35
    target 160
  ]
  edge [
    source 35
    target 438
  ]
  edge [
    source 35
    target 439
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 440
  ]
  edge [
    source 36
    target 441
  ]
  edge [
    source 36
    target 442
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 443
  ]
  edge [
    source 37
    target 444
  ]
  edge [
    source 37
    target 445
  ]
  edge [
    source 37
    target 446
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 447
  ]
  edge [
    source 38
    target 448
  ]
  edge [
    source 38
    target 449
  ]
  edge [
    source 38
    target 450
  ]
  edge [
    source 38
    target 451
  ]
  edge [
    source 38
    target 452
  ]
  edge [
    source 38
    target 349
  ]
  edge [
    source 38
    target 453
  ]
  edge [
    source 38
    target 454
  ]
  edge [
    source 38
    target 455
  ]
  edge [
    source 38
    target 456
  ]
  edge [
    source 39
    target 408
  ]
  edge [
    source 39
    target 457
  ]
  edge [
    source 39
    target 458
  ]
  edge [
    source 39
    target 459
  ]
  edge [
    source 39
    target 460
  ]
  edge [
    source 39
    target 461
  ]
  edge [
    source 39
    target 462
  ]
  edge [
    source 39
    target 463
  ]
  edge [
    source 39
    target 464
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 465
  ]
  edge [
    source 40
    target 466
  ]
  edge [
    source 40
    target 467
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 468
  ]
  edge [
    source 41
    target 469
  ]
  edge [
    source 41
    target 470
  ]
  edge [
    source 41
    target 105
  ]
  edge [
    source 41
    target 471
  ]
  edge [
    source 41
    target 472
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 473
  ]
  edge [
    source 43
    target 474
  ]
  edge [
    source 43
    target 475
  ]
  edge [
    source 43
    target 476
  ]
  edge [
    source 43
    target 477
  ]
  edge [
    source 43
    target 478
  ]
  edge [
    source 43
    target 479
  ]
  edge [
    source 43
    target 480
  ]
  edge [
    source 43
    target 481
  ]
  edge [
    source 43
    target 482
  ]
  edge [
    source 43
    target 483
  ]
  edge [
    source 43
    target 484
  ]
  edge [
    source 44
    target 485
  ]
  edge [
    source 44
    target 486
  ]
  edge [
    source 44
    target 306
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 487
  ]
  edge [
    source 45
    target 488
  ]
  edge [
    source 45
    target 489
  ]
  edge [
    source 45
    target 490
  ]
  edge [
    source 45
    target 491
  ]
  edge [
    source 45
    target 401
  ]
  edge [
    source 45
    target 492
  ]
  edge [
    source 45
    target 493
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 494
  ]
  edge [
    source 46
    target 495
  ]
  edge [
    source 47
    target 408
  ]
  edge [
    source 47
    target 496
  ]
  edge [
    source 497
    target 498
  ]
  edge [
    source 499
    target 500
  ]
  edge [
    source 501
    target 502
  ]
]
