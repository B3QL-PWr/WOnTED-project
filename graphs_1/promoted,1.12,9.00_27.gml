graph [
  maxDegree 15
  minDegree 1
  meanDegree 1.9574468085106382
  density 0.0425531914893617
  graphCliqueNumber 2
  node [
    id 0
    label "policjant"
    origin "text"
  ]
  node [
    id 1
    label "toru&#324;ski"
    origin "text"
  ]
  node [
    id 2
    label "komenda"
    origin "text"
  ]
  node [
    id 3
    label "poszukiwa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "dwa"
    origin "text"
  ]
  node [
    id 5
    label "m&#281;&#380;czyzna"
    origin "text"
  ]
  node [
    id 6
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 7
    label "dokona&#263;"
    origin "text"
  ]
  node [
    id 8
    label "kradzie&#380;"
    origin "text"
  ]
  node [
    id 9
    label "rozb&#243;jniczy"
    origin "text"
  ]
  node [
    id 10
    label "policja"
  ]
  node [
    id 11
    label "blacharz"
  ]
  node [
    id 12
    label "pa&#322;a"
  ]
  node [
    id 13
    label "mundurowy"
  ]
  node [
    id 14
    label "str&#243;&#380;"
  ]
  node [
    id 15
    label "glina"
  ]
  node [
    id 16
    label "psiarnia"
  ]
  node [
    id 17
    label "formu&#322;a"
  ]
  node [
    id 18
    label "posterunek"
  ]
  node [
    id 19
    label "sygna&#322;"
  ]
  node [
    id 20
    label "komender&#243;wka"
  ]
  node [
    id 21
    label "polecenie"
  ]
  node [
    id 22
    label "direction"
  ]
  node [
    id 23
    label "ask"
  ]
  node [
    id 24
    label "stara&#263;_si&#281;"
  ]
  node [
    id 25
    label "szuka&#263;"
  ]
  node [
    id 26
    label "look"
  ]
  node [
    id 27
    label "ch&#322;opina"
  ]
  node [
    id 28
    label "cz&#322;owiek"
  ]
  node [
    id 29
    label "bratek"
  ]
  node [
    id 30
    label "jegomo&#347;&#263;"
  ]
  node [
    id 31
    label "doros&#322;y"
  ]
  node [
    id 32
    label "samiec"
  ]
  node [
    id 33
    label "ojciec"
  ]
  node [
    id 34
    label "twardziel"
  ]
  node [
    id 35
    label "androlog"
  ]
  node [
    id 36
    label "pa&#324;stwo"
  ]
  node [
    id 37
    label "m&#261;&#380;"
  ]
  node [
    id 38
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 39
    label "andropauza"
  ]
  node [
    id 40
    label "przesta&#263;"
  ]
  node [
    id 41
    label "zrobi&#263;"
  ]
  node [
    id 42
    label "cause"
  ]
  node [
    id 43
    label "communicate"
  ]
  node [
    id 44
    label "plundering"
  ]
  node [
    id 45
    label "wydarzenie"
  ]
  node [
    id 46
    label "przest&#281;pstwo"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 8
    target 46
  ]
]
