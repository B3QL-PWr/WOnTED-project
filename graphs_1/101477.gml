graph [
  maxDegree 5
  minDegree 1
  meanDegree 1.8461538461538463
  density 0.15384615384615385
  graphCliqueNumber 3
  node [
    id 0
    label "osiek"
    origin "text"
  ]
  node [
    id 1
    label "powiat"
    origin "text"
  ]
  node [
    id 2
    label "o&#347;wi&#281;cimski"
    origin "text"
  ]
  node [
    id 3
    label "gmina"
  ]
  node [
    id 4
    label "jednostka_administracyjna"
  ]
  node [
    id 5
    label "wojew&#243;dztwo"
  ]
  node [
    id 6
    label "obozowy"
  ]
  node [
    id 7
    label "typowy"
  ]
  node [
    id 8
    label "ma&#322;opolski"
  ]
  node [
    id 9
    label "po_o&#347;wi&#281;cimsku"
  ]
  node [
    id 10
    label "&#347;wi&#281;ty"
  ]
  node [
    id 11
    label "ojciec"
  ]
  node [
    id 12
    label "Pio"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 11
    target 12
  ]
]
