graph [
  maxDegree 42
  minDegree 1
  meanDegree 2.045045045045045
  density 0.009253597488891607
  graphCliqueNumber 3
  node [
    id 0
    label "miejski"
    origin "text"
  ]
  node [
    id 1
    label "biblioteka"
    origin "text"
  ]
  node [
    id 2
    label "publiczny"
    origin "text"
  ]
  node [
    id 3
    label "towarzystwo"
    origin "text"
  ]
  node [
    id 4
    label "przyjaciel"
    origin "text"
  ]
  node [
    id 5
    label "ozorkowy"
    origin "text"
  ]
  node [
    id 6
    label "zaprasza&#263;"
    origin "text"
  ]
  node [
    id 7
    label "prelekcja"
    origin "text"
  ]
  node [
    id 8
    label "pi&#261;tek"
    origin "text"
  ]
  node [
    id 9
    label "&#347;lad"
    origin "text"
  ]
  node [
    id 10
    label "noblista"
    origin "text"
  ]
  node [
    id 11
    label "polski"
    origin "text"
  ]
  node [
    id 12
    label "&#380;ycie"
    origin "text"
  ]
  node [
    id 13
    label "tw&#243;rczo&#347;&#263;"
    origin "text"
  ]
  node [
    id 14
    label "hora"
    origin "text"
  ]
  node [
    id 15
    label "sienkiewicz"
    origin "text"
  ]
  node [
    id 16
    label "w&#322;&#261;cznie"
    origin "text"
  ]
  node [
    id 17
    label "staro"
    origin "text"
  ]
  node [
    id 18
    label "reymont"
    origin "text"
  ]
  node [
    id 19
    label "uwzgl&#281;dnienie"
    origin "text"
  ]
  node [
    id 20
    label "w&#261;tek"
    origin "text"
  ]
  node [
    id 21
    label "ozorkowski"
    origin "text"
  ]
  node [
    id 22
    label "spotkanie"
    origin "text"
  ]
  node [
    id 23
    label "odby&#263;"
    origin "text"
  ]
  node [
    id 24
    label "si&#281;"
    origin "text"
  ]
  node [
    id 25
    label "pa&#378;dziernik"
    origin "text"
  ]
  node [
    id 26
    label "&#347;roda"
    origin "text"
  ]
  node [
    id 27
    label "godz"
    origin "text"
  ]
  node [
    id 28
    label "czytelnia"
    origin "text"
  ]
  node [
    id 29
    label "mbp"
    origin "text"
  ]
  node [
    id 30
    label "przy"
    origin "text"
  ]
  node [
    id 31
    label "ula"
    origin "text"
  ]
  node [
    id 32
    label "listopadowy"
    origin "text"
  ]
  node [
    id 33
    label "miejsko"
  ]
  node [
    id 34
    label "miastowy"
  ]
  node [
    id 35
    label "typowy"
  ]
  node [
    id 36
    label "pok&#243;j"
  ]
  node [
    id 37
    label "rewers"
  ]
  node [
    id 38
    label "informatorium"
  ]
  node [
    id 39
    label "kolekcja"
  ]
  node [
    id 40
    label "czytelnik"
  ]
  node [
    id 41
    label "budynek"
  ]
  node [
    id 42
    label "zbi&#243;r"
  ]
  node [
    id 43
    label "programowanie"
  ]
  node [
    id 44
    label "ksi&#281;gozbi&#243;r_podr&#281;czny"
  ]
  node [
    id 45
    label "library"
  ]
  node [
    id 46
    label "instytucja"
  ]
  node [
    id 47
    label "jawny"
  ]
  node [
    id 48
    label "upublicznienie"
  ]
  node [
    id 49
    label "upublicznianie"
  ]
  node [
    id 50
    label "publicznie"
  ]
  node [
    id 51
    label "Ku&#378;nica_Ko&#322;&#322;&#261;tajowska"
  ]
  node [
    id 52
    label "obecno&#347;&#263;"
  ]
  node [
    id 53
    label "Krakowska_Kongregacja_Kupiecka"
  ]
  node [
    id 54
    label "organizacja"
  ]
  node [
    id 55
    label "Eleusis"
  ]
  node [
    id 56
    label "asystencja"
  ]
  node [
    id 57
    label "Rzeczpospolita_babi&#324;ska"
  ]
  node [
    id 58
    label "Polskie_Towarzystwo_Turystyczno-Krajoznawcze"
  ]
  node [
    id 59
    label "grupa"
  ]
  node [
    id 60
    label "fabianie"
  ]
  node [
    id 61
    label "Chewra_Kadisza"
  ]
  node [
    id 62
    label "grono"
  ]
  node [
    id 63
    label "umowa_stowarzyszeniowa"
  ]
  node [
    id 64
    label "wi&#281;&#378;"
  ]
  node [
    id 65
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 66
    label "partnership"
  ]
  node [
    id 67
    label "Monar"
  ]
  node [
    id 68
    label "Rotary_International"
  ]
  node [
    id 69
    label "kochanek"
  ]
  node [
    id 70
    label "przyja&#378;nienie_si&#281;"
  ]
  node [
    id 71
    label "zaprzyja&#378;nianie_si&#281;"
  ]
  node [
    id 72
    label "kum"
  ]
  node [
    id 73
    label "sympatyk"
  ]
  node [
    id 74
    label "bratnia_dusza"
  ]
  node [
    id 75
    label "amikus"
  ]
  node [
    id 76
    label "zaprzyja&#378;nienie_si&#281;"
  ]
  node [
    id 77
    label "pobratymiec"
  ]
  node [
    id 78
    label "drogi"
  ]
  node [
    id 79
    label "mi&#281;sny"
  ]
  node [
    id 80
    label "invite"
  ]
  node [
    id 81
    label "ask"
  ]
  node [
    id 82
    label "oferowa&#263;"
  ]
  node [
    id 83
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 84
    label "blok"
  ]
  node [
    id 85
    label "handout"
  ]
  node [
    id 86
    label "lecture"
  ]
  node [
    id 87
    label "wyk&#322;ad"
  ]
  node [
    id 88
    label "dzie&#324;_powszedni"
  ]
  node [
    id 89
    label "Wielki_Pi&#261;tek"
  ]
  node [
    id 90
    label "tydzie&#324;"
  ]
  node [
    id 91
    label "sznurowanie"
  ]
  node [
    id 92
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 93
    label "odrobina"
  ]
  node [
    id 94
    label "sznurowa&#263;"
  ]
  node [
    id 95
    label "attribute"
  ]
  node [
    id 96
    label "wp&#322;yw"
  ]
  node [
    id 97
    label "odcisk"
  ]
  node [
    id 98
    label "skutek"
  ]
  node [
    id 99
    label "Arafat"
  ]
  node [
    id 100
    label "Mi&#322;osz"
  ]
  node [
    id 101
    label "Bergson"
  ]
  node [
    id 102
    label "Fo"
  ]
  node [
    id 103
    label "Reymont"
  ]
  node [
    id 104
    label "laureat"
  ]
  node [
    id 105
    label "Sienkiewicz"
  ]
  node [
    id 106
    label "Wa&#322;&#281;sa"
  ]
  node [
    id 107
    label "Gorbaczow"
  ]
  node [
    id 108
    label "Einstein"
  ]
  node [
    id 109
    label "Winston_Churchill"
  ]
  node [
    id 110
    label "lacki"
  ]
  node [
    id 111
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 112
    label "przedmiot"
  ]
  node [
    id 113
    label "sztajer"
  ]
  node [
    id 114
    label "drabant"
  ]
  node [
    id 115
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 116
    label "polak"
  ]
  node [
    id 117
    label "pierogi_ruskie"
  ]
  node [
    id 118
    label "krakowiak"
  ]
  node [
    id 119
    label "Polish"
  ]
  node [
    id 120
    label "j&#281;zyk"
  ]
  node [
    id 121
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 122
    label "oberek"
  ]
  node [
    id 123
    label "po_polsku"
  ]
  node [
    id 124
    label "mazur"
  ]
  node [
    id 125
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 126
    label "chodzony"
  ]
  node [
    id 127
    label "skoczny"
  ]
  node [
    id 128
    label "ryba_po_grecku"
  ]
  node [
    id 129
    label "goniony"
  ]
  node [
    id 130
    label "polsko"
  ]
  node [
    id 131
    label "energy"
  ]
  node [
    id 132
    label "czas"
  ]
  node [
    id 133
    label "bycie"
  ]
  node [
    id 134
    label "zegar_biologiczny"
  ]
  node [
    id 135
    label "okres_noworodkowy"
  ]
  node [
    id 136
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 137
    label "entity"
  ]
  node [
    id 138
    label "prze&#380;ywanie"
  ]
  node [
    id 139
    label "prze&#380;ycie"
  ]
  node [
    id 140
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 141
    label "wiek_matuzalemowy"
  ]
  node [
    id 142
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 143
    label "dzieci&#324;stwo"
  ]
  node [
    id 144
    label "power"
  ]
  node [
    id 145
    label "szwung"
  ]
  node [
    id 146
    label "menopauza"
  ]
  node [
    id 147
    label "umarcie"
  ]
  node [
    id 148
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 149
    label "life"
  ]
  node [
    id 150
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 151
    label "&#380;ywy"
  ]
  node [
    id 152
    label "rozw&#243;j"
  ]
  node [
    id 153
    label "po&#322;&#243;g"
  ]
  node [
    id 154
    label "byt"
  ]
  node [
    id 155
    label "przebywanie"
  ]
  node [
    id 156
    label "subsistence"
  ]
  node [
    id 157
    label "koleje_losu"
  ]
  node [
    id 158
    label "raj_utracony"
  ]
  node [
    id 159
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 160
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 161
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 162
    label "andropauza"
  ]
  node [
    id 163
    label "warunki"
  ]
  node [
    id 164
    label "do&#380;ywanie"
  ]
  node [
    id 165
    label "niemowl&#281;ctwo"
  ]
  node [
    id 166
    label "umieranie"
  ]
  node [
    id 167
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 168
    label "staro&#347;&#263;"
  ]
  node [
    id 169
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 170
    label "&#347;mier&#263;"
  ]
  node [
    id 171
    label "creation"
  ]
  node [
    id 172
    label "tworzenie"
  ]
  node [
    id 173
    label "kreacja"
  ]
  node [
    id 174
    label "dorobek"
  ]
  node [
    id 175
    label "kultura"
  ]
  node [
    id 176
    label "melodia"
  ]
  node [
    id 177
    label "taniec_ludowy"
  ]
  node [
    id 178
    label "taniec"
  ]
  node [
    id 179
    label "nabo&#380;e&#324;stwo"
  ]
  node [
    id 180
    label "stary"
  ]
  node [
    id 181
    label "charakterystycznie"
  ]
  node [
    id 182
    label "staro&#380;ytnie"
  ]
  node [
    id 183
    label "starczy"
  ]
  node [
    id 184
    label "nie&#347;wie&#380;o"
  ]
  node [
    id 185
    label "wzi&#281;cie"
  ]
  node [
    id 186
    label "acknowledgment"
  ]
  node [
    id 187
    label "splot"
  ]
  node [
    id 188
    label "wytw&#243;r"
  ]
  node [
    id 189
    label "temat"
  ]
  node [
    id 190
    label "matter"
  ]
  node [
    id 191
    label "forum"
  ]
  node [
    id 192
    label "fabu&#322;a"
  ]
  node [
    id 193
    label "rozmieszczenie"
  ]
  node [
    id 194
    label "ceg&#322;a"
  ]
  node [
    id 195
    label "socket"
  ]
  node [
    id 196
    label "topik"
  ]
  node [
    id 197
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 198
    label "po&#380;egnanie"
  ]
  node [
    id 199
    label "spowodowanie"
  ]
  node [
    id 200
    label "znalezienie"
  ]
  node [
    id 201
    label "znajomy"
  ]
  node [
    id 202
    label "doznanie"
  ]
  node [
    id 203
    label "employment"
  ]
  node [
    id 204
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 205
    label "gather"
  ]
  node [
    id 206
    label "powitanie"
  ]
  node [
    id 207
    label "spotykanie"
  ]
  node [
    id 208
    label "wydarzenie"
  ]
  node [
    id 209
    label "gathering"
  ]
  node [
    id 210
    label "spotkanie_si&#281;"
  ]
  node [
    id 211
    label "zdarzenie_si&#281;"
  ]
  node [
    id 212
    label "match"
  ]
  node [
    id 213
    label "zawarcie"
  ]
  node [
    id 214
    label "reserve"
  ]
  node [
    id 215
    label "przej&#347;&#263;"
  ]
  node [
    id 216
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 217
    label "Dzie&#324;_Papieski"
  ]
  node [
    id 218
    label "oktober"
  ]
  node [
    id 219
    label "miesi&#261;c"
  ]
  node [
    id 220
    label "Popielec"
  ]
  node [
    id 221
    label "pomieszczenie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 10
    target 105
  ]
  edge [
    source 10
    target 106
  ]
  edge [
    source 10
    target 107
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 110
  ]
  edge [
    source 11
    target 111
  ]
  edge [
    source 11
    target 112
  ]
  edge [
    source 11
    target 113
  ]
  edge [
    source 11
    target 114
  ]
  edge [
    source 11
    target 115
  ]
  edge [
    source 11
    target 116
  ]
  edge [
    source 11
    target 117
  ]
  edge [
    source 11
    target 118
  ]
  edge [
    source 11
    target 119
  ]
  edge [
    source 11
    target 120
  ]
  edge [
    source 11
    target 121
  ]
  edge [
    source 11
    target 122
  ]
  edge [
    source 11
    target 123
  ]
  edge [
    source 11
    target 124
  ]
  edge [
    source 11
    target 125
  ]
  edge [
    source 11
    target 126
  ]
  edge [
    source 11
    target 127
  ]
  edge [
    source 11
    target 128
  ]
  edge [
    source 11
    target 129
  ]
  edge [
    source 11
    target 130
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 131
  ]
  edge [
    source 12
    target 132
  ]
  edge [
    source 12
    target 133
  ]
  edge [
    source 12
    target 134
  ]
  edge [
    source 12
    target 135
  ]
  edge [
    source 12
    target 136
  ]
  edge [
    source 12
    target 137
  ]
  edge [
    source 12
    target 138
  ]
  edge [
    source 12
    target 139
  ]
  edge [
    source 12
    target 140
  ]
  edge [
    source 12
    target 141
  ]
  edge [
    source 12
    target 142
  ]
  edge [
    source 12
    target 143
  ]
  edge [
    source 12
    target 144
  ]
  edge [
    source 12
    target 145
  ]
  edge [
    source 12
    target 146
  ]
  edge [
    source 12
    target 147
  ]
  edge [
    source 12
    target 148
  ]
  edge [
    source 12
    target 149
  ]
  edge [
    source 12
    target 150
  ]
  edge [
    source 12
    target 151
  ]
  edge [
    source 12
    target 152
  ]
  edge [
    source 12
    target 153
  ]
  edge [
    source 12
    target 154
  ]
  edge [
    source 12
    target 155
  ]
  edge [
    source 12
    target 156
  ]
  edge [
    source 12
    target 157
  ]
  edge [
    source 12
    target 158
  ]
  edge [
    source 12
    target 159
  ]
  edge [
    source 12
    target 160
  ]
  edge [
    source 12
    target 161
  ]
  edge [
    source 12
    target 162
  ]
  edge [
    source 12
    target 163
  ]
  edge [
    source 12
    target 164
  ]
  edge [
    source 12
    target 165
  ]
  edge [
    source 12
    target 166
  ]
  edge [
    source 12
    target 167
  ]
  edge [
    source 12
    target 168
  ]
  edge [
    source 12
    target 169
  ]
  edge [
    source 12
    target 170
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 171
  ]
  edge [
    source 13
    target 172
  ]
  edge [
    source 13
    target 173
  ]
  edge [
    source 13
    target 174
  ]
  edge [
    source 13
    target 175
  ]
  edge [
    source 13
    target 42
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 176
  ]
  edge [
    source 14
    target 177
  ]
  edge [
    source 14
    target 178
  ]
  edge [
    source 14
    target 179
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 180
  ]
  edge [
    source 17
    target 181
  ]
  edge [
    source 17
    target 182
  ]
  edge [
    source 17
    target 183
  ]
  edge [
    source 17
    target 184
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 185
  ]
  edge [
    source 19
    target 186
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 187
  ]
  edge [
    source 20
    target 188
  ]
  edge [
    source 20
    target 189
  ]
  edge [
    source 20
    target 190
  ]
  edge [
    source 20
    target 191
  ]
  edge [
    source 20
    target 192
  ]
  edge [
    source 20
    target 193
  ]
  edge [
    source 20
    target 194
  ]
  edge [
    source 20
    target 195
  ]
  edge [
    source 20
    target 196
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 197
  ]
  edge [
    source 22
    target 198
  ]
  edge [
    source 22
    target 199
  ]
  edge [
    source 22
    target 200
  ]
  edge [
    source 22
    target 201
  ]
  edge [
    source 22
    target 202
  ]
  edge [
    source 22
    target 203
  ]
  edge [
    source 22
    target 204
  ]
  edge [
    source 22
    target 205
  ]
  edge [
    source 22
    target 206
  ]
  edge [
    source 22
    target 207
  ]
  edge [
    source 22
    target 208
  ]
  edge [
    source 22
    target 209
  ]
  edge [
    source 22
    target 210
  ]
  edge [
    source 22
    target 211
  ]
  edge [
    source 22
    target 212
  ]
  edge [
    source 22
    target 213
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 214
  ]
  edge [
    source 23
    target 215
  ]
  edge [
    source 23
    target 216
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 217
  ]
  edge [
    source 25
    target 218
  ]
  edge [
    source 25
    target 219
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 220
  ]
  edge [
    source 26
    target 88
  ]
  edge [
    source 26
    target 90
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 221
  ]
  edge [
    source 28
    target 38
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 31
    target 32
  ]
]
