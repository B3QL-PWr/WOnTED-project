graph [
  maxDegree 20
  minDegree 1
  meanDegree 2.0377358490566038
  density 0.03918722786647315
  graphCliqueNumber 3
  node [
    id 0
    label "wygrana"
    origin "text"
  ]
  node [
    id 1
    label "karowy"
    origin "text"
  ]
  node [
    id 2
    label "rz&#261;d"
    origin "text"
  ]
  node [
    id 3
    label "rajd"
    origin "text"
  ]
  node [
    id 4
    label "barb&#243;rka"
    origin "text"
  ]
  node [
    id 5
    label "kajetanowicz"
    origin "text"
  ]
  node [
    id 6
    label "szczepaniak"
    origin "text"
  ]
  node [
    id 7
    label "triumfowa&#263;"
    origin "text"
  ]
  node [
    id 8
    label "warszawa"
    origin "text"
  ]
  node [
    id 9
    label "puchar"
  ]
  node [
    id 10
    label "korzy&#347;&#263;"
  ]
  node [
    id 11
    label "sukces"
  ]
  node [
    id 12
    label "przedmiot"
  ]
  node [
    id 13
    label "conquest"
  ]
  node [
    id 14
    label "kwadratowy"
  ]
  node [
    id 15
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 16
    label "kategoria"
  ]
  node [
    id 17
    label "egzekutywa"
  ]
  node [
    id 18
    label "gabinet_cieni"
  ]
  node [
    id 19
    label "gromada"
  ]
  node [
    id 20
    label "premier"
  ]
  node [
    id 21
    label "Londyn"
  ]
  node [
    id 22
    label "Konsulat"
  ]
  node [
    id 23
    label "uporz&#261;dkowanie"
  ]
  node [
    id 24
    label "jednostka_systematyczna"
  ]
  node [
    id 25
    label "szpaler"
  ]
  node [
    id 26
    label "przybli&#380;enie"
  ]
  node [
    id 27
    label "tract"
  ]
  node [
    id 28
    label "number"
  ]
  node [
    id 29
    label "lon&#380;a"
  ]
  node [
    id 30
    label "w&#322;adza"
  ]
  node [
    id 31
    label "instytucja"
  ]
  node [
    id 32
    label "klasa"
  ]
  node [
    id 33
    label "foray"
  ]
  node [
    id 34
    label "wy&#347;cig"
  ]
  node [
    id 35
    label "Rajd_Arsena&#322;"
  ]
  node [
    id 36
    label "czo&#322;&#243;wka"
  ]
  node [
    id 37
    label "Rajd_Dakar"
  ]
  node [
    id 38
    label "gra_MMORPG"
  ]
  node [
    id 39
    label "rozgrywka"
  ]
  node [
    id 40
    label "rally"
  ]
  node [
    id 41
    label "podr&#243;&#380;"
  ]
  node [
    id 42
    label "Rajd_Barb&#243;rka"
  ]
  node [
    id 43
    label "boss"
  ]
  node [
    id 44
    label "impreza"
  ]
  node [
    id 45
    label "gloat"
  ]
  node [
    id 46
    label "przewa&#380;a&#263;"
  ]
  node [
    id 47
    label "wygrywa&#263;"
  ]
  node [
    id 48
    label "chwali&#263;_si&#281;"
  ]
  node [
    id 49
    label "Warszawa"
  ]
  node [
    id 50
    label "samoch&#243;d"
  ]
  node [
    id 51
    label "fastback"
  ]
  node [
    id 52
    label "Kajetanowicz"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 0
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
]
