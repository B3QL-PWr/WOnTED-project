graph [
  maxDegree 1
  minDegree 1
  meanDegree 1
  density 0.3333333333333333
  graphCliqueNumber 2
  node [
    id 0
    label "serowy"
    origin "text"
  ]
  node [
    id 1
    label "ciep&#322;y"
  ]
  node [
    id 2
    label "dystrykt"
  ]
  node [
    id 3
    label "centralny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
]
