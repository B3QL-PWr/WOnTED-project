graph [
  maxDegree 3
  minDegree 1
  meanDegree 1.3333333333333333
  density 0.26666666666666666
  graphCliqueNumber 2
  node [
    id 0
    label "siemanko"
    origin "text"
  ]
  node [
    id 1
    label "wykopki"
    origin "text"
  ]
  node [
    id 2
    label "zbi&#243;r"
  ]
  node [
    id 3
    label "sadzeniak"
  ]
  node [
    id 4
    label "szko&#322;a"
  ]
  node [
    id 5
    label "policja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 4
    target 5
  ]
]
