graph [
  maxDegree 51
  minDegree 1
  meanDegree 2.2459546925566345
  density 0.0024254370329985253
  graphCliqueNumber 4
  node [
    id 0
    label "min&#261;&#263;"
    origin "text"
  ]
  node [
    id 1
    label "wiele"
    origin "text"
  ]
  node [
    id 2
    label "czas"
    origin "text"
  ]
  node [
    id 3
    label "nagroda"
    origin "text"
  ]
  node [
    id 4
    label "nobel"
    origin "text"
  ]
  node [
    id 5
    label "gdy"
    origin "text"
  ]
  node [
    id 6
    label "pani"
    origin "text"
  ]
  node [
    id 7
    label "jelinek"
    origin "text"
  ]
  node [
    id 8
    label "zn&#243;w"
    origin "text"
  ]
  node [
    id 9
    label "zrobi&#263;"
    origin "text"
  ]
  node [
    id 10
    label "si&#281;"
    origin "text"
  ]
  node [
    id 11
    label "troch&#281;"
    origin "text"
  ]
  node [
    id 12
    label "zapomnie&#263;"
    origin "text"
  ]
  node [
    id 13
    label "zreszt&#261;"
    origin "text"
  ]
  node [
    id 14
    label "te&#380;"
    origin "text"
  ]
  node [
    id 15
    label "przyzna&#263;"
    origin "text"
  ]
  node [
    id 16
    label "jako&#347;"
    origin "text"
  ]
  node [
    id 17
    label "tak"
    origin "text"
  ]
  node [
    id 18
    label "p&#243;&#322;g&#281;bkiem"
    origin "text"
  ]
  node [
    id 19
    label "przy"
    origin "text"
  ]
  node [
    id 20
    label "akompaniament"
    origin "text"
  ]
  node [
    id 21
    label "szowinistyczny"
    origin "text"
  ]
  node [
    id 22
    label "wyzwiska"
    origin "text"
  ]
  node [
    id 23
    label "wzi&#261;&#263;"
    origin "text"
  ]
  node [
    id 24
    label "taka"
    origin "text"
  ]
  node [
    id 25
    label "pianistka"
    origin "text"
  ]
  node [
    id 26
    label "by&#263;"
    origin "text"
  ]
  node [
    id 27
    label "raczej"
    origin "text"
  ]
  node [
    id 28
    label "ksi&#261;&#380;ek"
    origin "text"
  ]
  node [
    id 29
    label "om&#243;wienie"
    origin "text"
  ]
  node [
    id 30
    label "rodzinny"
    origin "text"
  ]
  node [
    id 31
    label "obiad"
    origin "text"
  ]
  node [
    id 32
    label "wujostwo"
    origin "text"
  ]
  node [
    id 33
    label "porusza&#263;"
    origin "text"
  ]
  node [
    id 34
    label "temat"
    origin "text"
  ]
  node [
    id 35
    label "g&#243;rnolotny"
    origin "text"
  ]
  node [
    id 36
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 37
    label "dobrze"
    origin "text"
  ]
  node [
    id 38
    label "upozowa&#263;"
    origin "text"
  ]
  node [
    id 39
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 40
    label "wra&#380;liwy"
    origin "text"
  ]
  node [
    id 41
    label "przenikliwy"
    origin "text"
  ]
  node [
    id 42
    label "gdzie&#347;"
    origin "text"
  ]
  node [
    id 43
    label "pomi&#281;dzy"
    origin "text"
  ]
  node [
    id 44
    label "kaczka"
    origin "text"
  ]
  node [
    id 45
    label "jab&#322;ko"
    origin "text"
  ]
  node [
    id 46
    label "likier"
    origin "text"
  ]
  node [
    id 47
    label "bolesny"
    origin "text"
  ]
  node [
    id 48
    label "ciemny"
    origin "text"
  ]
  node [
    id 49
    label "czym"
    origin "text"
  ]
  node [
    id 50
    label "uwodzicielsko"
    origin "text"
  ]
  node [
    id 51
    label "dawkowa&#263;"
    origin "text"
  ]
  node [
    id 52
    label "napi&#281;cie"
    origin "text"
  ]
  node [
    id 53
    label "sprawnie"
    origin "text"
  ]
  node [
    id 54
    label "snu&#263;"
    origin "text"
  ]
  node [
    id 55
    label "historia"
    origin "text"
  ]
  node [
    id 56
    label "tworzy&#263;"
    origin "text"
  ]
  node [
    id 57
    label "ponury"
    origin "text"
  ]
  node [
    id 58
    label "z&#322;o&#380;ony"
    origin "text"
  ]
  node [
    id 59
    label "struktura"
    origin "text"
  ]
  node [
    id 60
    label "powi&#261;zanie"
    origin "text"
  ]
  node [
    id 61
    label "mi&#281;dzy"
    origin "text"
  ]
  node [
    id 62
    label "bohater"
    origin "text"
  ]
  node [
    id 63
    label "p&#243;&#378;no"
    origin "text"
  ]
  node [
    id 64
    label "r&#243;wnie"
    origin "text"
  ]
  node [
    id 65
    label "ponuro"
    origin "text"
  ]
  node [
    id 66
    label "rozpl&#261;tywa&#263;"
    origin "text"
  ]
  node [
    id 67
    label "og&#243;&#322;"
    origin "text"
  ]
  node [
    id 68
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 69
    label "znaczy&#263;"
    origin "text"
  ]
  node [
    id 70
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 71
    label "oderwa&#263;"
    origin "text"
  ]
  node [
    id 72
    label "powie&#347;&#263;"
    origin "text"
  ]
  node [
    id 73
    label "przemoc"
    origin "text"
  ]
  node [
    id 74
    label "psychiczny"
    origin "text"
  ]
  node [
    id 75
    label "seksualny"
    origin "text"
  ]
  node [
    id 76
    label "du&#380;o"
    origin "text"
  ]
  node [
    id 77
    label "jeszcze"
    origin "text"
  ]
  node [
    id 78
    label "uzna&#263;"
    origin "text"
  ]
  node [
    id 79
    label "co&#347;"
    origin "text"
  ]
  node [
    id 80
    label "ni&#380;"
    origin "text"
  ]
  node [
    id 81
    label "tychy"
    origin "text"
  ]
  node [
    id 82
    label "konkretnie"
    origin "text"
  ]
  node [
    id 83
    label "posta&#263;"
    origin "text"
  ]
  node [
    id 84
    label "pokazywa&#263;"
    origin "text"
  ]
  node [
    id 85
    label "pewne"
    origin "text"
  ]
  node [
    id 86
    label "uniwersalny"
    origin "text"
  ]
  node [
    id 87
    label "sytuacja"
    origin "text"
  ]
  node [
    id 88
    label "jak"
    origin "text"
  ]
  node [
    id 89
    label "dobry"
    origin "text"
  ]
  node [
    id 90
    label "literatura"
    origin "text"
  ]
  node [
    id 91
    label "bywa&#263;"
    origin "text"
  ]
  node [
    id 92
    label "biada&#263;"
    origin "text"
  ]
  node [
    id 93
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 94
    label "obrzydzi&#263;"
    origin "text"
  ]
  node [
    id 95
    label "deser"
    origin "text"
  ]
  node [
    id 96
    label "lub"
    origin "text"
  ]
  node [
    id 97
    label "sprawi&#263;"
    origin "text"
  ]
  node [
    id 98
    label "zamy&#347;li&#263;"
    origin "text"
  ]
  node [
    id 99
    label "herbata"
    origin "text"
  ]
  node [
    id 100
    label "wystygn&#261;&#263;"
    origin "text"
  ]
  node [
    id 101
    label "pi&#281;kny"
    origin "text"
  ]
  node [
    id 102
    label "but"
    origin "text"
  ]
  node [
    id 103
    label "cisn&#261;&#263;"
    origin "text"
  ]
  node [
    id 104
    label "trzyma&#263;"
    origin "text"
  ]
  node [
    id 105
    label "taki"
    origin "text"
  ]
  node [
    id 106
    label "widok"
    origin "text"
  ]
  node [
    id 107
    label "cierpie&#263;"
    origin "text"
  ]
  node [
    id 108
    label "pow&#243;d"
    origin "text"
  ]
  node [
    id 109
    label "pytanie"
    origin "text"
  ]
  node [
    id 110
    label "jaki"
    origin "text"
  ]
  node [
    id 111
    label "pi&#281;kne"
    origin "text"
  ]
  node [
    id 112
    label "czemu"
    origin "text"
  ]
  node [
    id 113
    label "nosi&#263;"
    origin "text"
  ]
  node [
    id 114
    label "wyrzuci&#263;"
    origin "text"
  ]
  node [
    id 115
    label "&#347;mietnik"
    origin "text"
  ]
  node [
    id 116
    label "chyba"
    origin "text"
  ]
  node [
    id 117
    label "przypadek"
    origin "text"
  ]
  node [
    id 118
    label "tam"
    origin "text"
  ]
  node [
    id 119
    label "buszowa&#263;"
    origin "text"
  ]
  node [
    id 120
    label "jaki&#347;"
    origin "text"
  ]
  node [
    id 121
    label "niepowa&#380;ny"
    origin "text"
  ]
  node [
    id 122
    label "szwedzki"
    origin "text"
  ]
  node [
    id 123
    label "akademik"
    origin "text"
  ]
  node [
    id 124
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 125
    label "omin&#261;&#263;"
  ]
  node [
    id 126
    label "spowodowa&#263;"
  ]
  node [
    id 127
    label "run"
  ]
  node [
    id 128
    label "przesta&#263;"
  ]
  node [
    id 129
    label "przej&#347;&#263;"
  ]
  node [
    id 130
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 131
    label "die"
  ]
  node [
    id 132
    label "overwhelm"
  ]
  node [
    id 133
    label "wiela"
  ]
  node [
    id 134
    label "du&#380;y"
  ]
  node [
    id 135
    label "czasokres"
  ]
  node [
    id 136
    label "trawienie"
  ]
  node [
    id 137
    label "kategoria_gramatyczna"
  ]
  node [
    id 138
    label "period"
  ]
  node [
    id 139
    label "odczyt"
  ]
  node [
    id 140
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 141
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 142
    label "chwila"
  ]
  node [
    id 143
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 144
    label "poprzedzenie"
  ]
  node [
    id 145
    label "koniugacja"
  ]
  node [
    id 146
    label "dzieje"
  ]
  node [
    id 147
    label "poprzedzi&#263;"
  ]
  node [
    id 148
    label "przep&#322;ywanie"
  ]
  node [
    id 149
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 150
    label "odwlekanie_si&#281;"
  ]
  node [
    id 151
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 152
    label "Zeitgeist"
  ]
  node [
    id 153
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 154
    label "okres_czasu"
  ]
  node [
    id 155
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 156
    label "pochodzi&#263;"
  ]
  node [
    id 157
    label "schy&#322;ek"
  ]
  node [
    id 158
    label "czwarty_wymiar"
  ]
  node [
    id 159
    label "chronometria"
  ]
  node [
    id 160
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 161
    label "poprzedzanie"
  ]
  node [
    id 162
    label "pogoda"
  ]
  node [
    id 163
    label "zegar"
  ]
  node [
    id 164
    label "pochodzenie"
  ]
  node [
    id 165
    label "poprzedza&#263;"
  ]
  node [
    id 166
    label "trawi&#263;"
  ]
  node [
    id 167
    label "time_period"
  ]
  node [
    id 168
    label "rachuba_czasu"
  ]
  node [
    id 169
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 170
    label "czasoprzestrze&#324;"
  ]
  node [
    id 171
    label "laba"
  ]
  node [
    id 172
    label "return"
  ]
  node [
    id 173
    label "konsekwencja"
  ]
  node [
    id 174
    label "oskar"
  ]
  node [
    id 175
    label "wyr&#243;&#380;nienie"
  ]
  node [
    id 176
    label "moneta"
  ]
  node [
    id 177
    label "transuranowiec"
  ]
  node [
    id 178
    label "aktynowiec"
  ]
  node [
    id 179
    label "przekwitanie"
  ]
  node [
    id 180
    label "zwrot"
  ]
  node [
    id 181
    label "uleganie"
  ]
  node [
    id 182
    label "ulega&#263;"
  ]
  node [
    id 183
    label "partner"
  ]
  node [
    id 184
    label "doros&#322;y"
  ]
  node [
    id 185
    label "przyw&#243;dczyni"
  ]
  node [
    id 186
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 187
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 188
    label "ulec"
  ]
  node [
    id 189
    label "kobita"
  ]
  node [
    id 190
    label "&#322;ono"
  ]
  node [
    id 191
    label "kobieta"
  ]
  node [
    id 192
    label "w&#322;a&#347;cicielka"
  ]
  node [
    id 193
    label "m&#281;&#380;yna"
  ]
  node [
    id 194
    label "babka"
  ]
  node [
    id 195
    label "samica"
  ]
  node [
    id 196
    label "pa&#324;stwo"
  ]
  node [
    id 197
    label "ulegni&#281;cie"
  ]
  node [
    id 198
    label "menopauza"
  ]
  node [
    id 199
    label "zorganizowa&#263;"
  ]
  node [
    id 200
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 201
    label "przerobi&#263;"
  ]
  node [
    id 202
    label "wystylizowa&#263;"
  ]
  node [
    id 203
    label "cause"
  ]
  node [
    id 204
    label "wydali&#263;"
  ]
  node [
    id 205
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 206
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 207
    label "post&#261;pi&#263;"
  ]
  node [
    id 208
    label "appoint"
  ]
  node [
    id 209
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 210
    label "nabra&#263;"
  ]
  node [
    id 211
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 212
    label "make"
  ]
  node [
    id 213
    label "porzuci&#263;"
  ]
  node [
    id 214
    label "opu&#347;ci&#263;"
  ]
  node [
    id 215
    label "pozostawi&#263;"
  ]
  node [
    id 216
    label "screw"
  ]
  node [
    id 217
    label "fuck"
  ]
  node [
    id 218
    label "straci&#263;"
  ]
  node [
    id 219
    label "zdolno&#347;&#263;"
  ]
  node [
    id 220
    label "wybaczy&#263;"
  ]
  node [
    id 221
    label "zabaczy&#263;"
  ]
  node [
    id 222
    label "da&#263;"
  ]
  node [
    id 223
    label "nada&#263;"
  ]
  node [
    id 224
    label "pozwoli&#263;"
  ]
  node [
    id 225
    label "give"
  ]
  node [
    id 226
    label "stwierdzi&#263;"
  ]
  node [
    id 227
    label "dziwnie"
  ]
  node [
    id 228
    label "w_miar&#281;"
  ]
  node [
    id 229
    label "przyzwoicie"
  ]
  node [
    id 230
    label "jako_taki"
  ]
  node [
    id 231
    label "nie&#378;le"
  ]
  node [
    id 232
    label "be&#322;kotliwie"
  ]
  node [
    id 233
    label "skrycie"
  ]
  node [
    id 234
    label "niewyra&#378;nie"
  ]
  node [
    id 235
    label "nieporz&#261;dnie"
  ]
  node [
    id 236
    label "chy&#322;kiem"
  ]
  node [
    id 237
    label "d&#378;wi&#281;k"
  ]
  node [
    id 238
    label "czynno&#347;&#263;"
  ]
  node [
    id 239
    label "podk&#322;ad"
  ]
  node [
    id 240
    label "nacjonalistyczny"
  ]
  node [
    id 241
    label "nietolerancyjny"
  ]
  node [
    id 242
    label "szowinistycznie"
  ]
  node [
    id 243
    label "dyskryminacyjny"
  ]
  node [
    id 244
    label "wej&#347;&#263;"
  ]
  node [
    id 245
    label "get"
  ]
  node [
    id 246
    label "wzi&#281;cie"
  ]
  node [
    id 247
    label "wyrucha&#263;"
  ]
  node [
    id 248
    label "uciec"
  ]
  node [
    id 249
    label "ruszy&#263;"
  ]
  node [
    id 250
    label "wygra&#263;"
  ]
  node [
    id 251
    label "obj&#261;&#263;"
  ]
  node [
    id 252
    label "zacz&#261;&#263;"
  ]
  node [
    id 253
    label "wyciupcia&#263;"
  ]
  node [
    id 254
    label "World_Health_Organization"
  ]
  node [
    id 255
    label "skorzysta&#263;"
  ]
  node [
    id 256
    label "pokona&#263;"
  ]
  node [
    id 257
    label "poczyta&#263;"
  ]
  node [
    id 258
    label "poruszy&#263;"
  ]
  node [
    id 259
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 260
    label "take"
  ]
  node [
    id 261
    label "aim"
  ]
  node [
    id 262
    label "arise"
  ]
  node [
    id 263
    label "u&#380;y&#263;"
  ]
  node [
    id 264
    label "zaatakowa&#263;"
  ]
  node [
    id 265
    label "receive"
  ]
  node [
    id 266
    label "uda&#263;_si&#281;"
  ]
  node [
    id 267
    label "dosta&#263;"
  ]
  node [
    id 268
    label "otrzyma&#263;"
  ]
  node [
    id 269
    label "obskoczy&#263;"
  ]
  node [
    id 270
    label "zaopatrzy&#263;_si&#281;"
  ]
  node [
    id 271
    label "bra&#263;"
  ]
  node [
    id 272
    label "nakaza&#263;"
  ]
  node [
    id 273
    label "chwyci&#263;"
  ]
  node [
    id 274
    label "przyj&#261;&#263;"
  ]
  node [
    id 275
    label "seize"
  ]
  node [
    id 276
    label "odziedziczy&#263;"
  ]
  node [
    id 277
    label "withdraw"
  ]
  node [
    id 278
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 279
    label "wch&#322;on&#261;&#263;"
  ]
  node [
    id 280
    label "Bangladesz"
  ]
  node [
    id 281
    label "jednostka_monetarna"
  ]
  node [
    id 282
    label "pianistyka"
  ]
  node [
    id 283
    label "si&#281;ga&#263;"
  ]
  node [
    id 284
    label "trwa&#263;"
  ]
  node [
    id 285
    label "obecno&#347;&#263;"
  ]
  node [
    id 286
    label "stan"
  ]
  node [
    id 287
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 288
    label "stand"
  ]
  node [
    id 289
    label "mie&#263;_miejsce"
  ]
  node [
    id 290
    label "uczestniczy&#263;"
  ]
  node [
    id 291
    label "chodzi&#263;"
  ]
  node [
    id 292
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 293
    label "equal"
  ]
  node [
    id 294
    label "discussion"
  ]
  node [
    id 295
    label "figura_stylistyczna"
  ]
  node [
    id 296
    label "omowny"
  ]
  node [
    id 297
    label "sformu&#322;owanie"
  ]
  node [
    id 298
    label "familijnie"
  ]
  node [
    id 299
    label "rodzinnie"
  ]
  node [
    id 300
    label "przyjemny"
  ]
  node [
    id 301
    label "wsp&#243;lny"
  ]
  node [
    id 302
    label "charakterystyczny"
  ]
  node [
    id 303
    label "swobodny"
  ]
  node [
    id 304
    label "zwi&#261;zany"
  ]
  node [
    id 305
    label "towarzyski"
  ]
  node [
    id 306
    label "ciep&#322;y"
  ]
  node [
    id 307
    label "posi&#322;ek"
  ]
  node [
    id 308
    label "meal"
  ]
  node [
    id 309
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 310
    label "podnosi&#263;"
  ]
  node [
    id 311
    label "meet"
  ]
  node [
    id 312
    label "move"
  ]
  node [
    id 313
    label "act"
  ]
  node [
    id 314
    label "wzbudza&#263;"
  ]
  node [
    id 315
    label "porobi&#263;"
  ]
  node [
    id 316
    label "drive"
  ]
  node [
    id 317
    label "robi&#263;"
  ]
  node [
    id 318
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 319
    label "go"
  ]
  node [
    id 320
    label "powodowa&#263;"
  ]
  node [
    id 321
    label "fraza"
  ]
  node [
    id 322
    label "forma"
  ]
  node [
    id 323
    label "melodia"
  ]
  node [
    id 324
    label "rzecz"
  ]
  node [
    id 325
    label "zbacza&#263;"
  ]
  node [
    id 326
    label "entity"
  ]
  node [
    id 327
    label "omawia&#263;"
  ]
  node [
    id 328
    label "topik"
  ]
  node [
    id 329
    label "wyraz_pochodny"
  ]
  node [
    id 330
    label "om&#243;wi&#263;"
  ]
  node [
    id 331
    label "omawianie"
  ]
  node [
    id 332
    label "w&#261;tek"
  ]
  node [
    id 333
    label "forum"
  ]
  node [
    id 334
    label "cecha"
  ]
  node [
    id 335
    label "zboczenie"
  ]
  node [
    id 336
    label "zbaczanie"
  ]
  node [
    id 337
    label "tre&#347;&#263;"
  ]
  node [
    id 338
    label "tematyka"
  ]
  node [
    id 339
    label "sprawa"
  ]
  node [
    id 340
    label "istota"
  ]
  node [
    id 341
    label "otoczka"
  ]
  node [
    id 342
    label "zboczy&#263;"
  ]
  node [
    id 343
    label "g&#243;rnolotnie"
  ]
  node [
    id 344
    label "literacki"
  ]
  node [
    id 345
    label "wznios&#322;y"
  ]
  node [
    id 346
    label "moralnie"
  ]
  node [
    id 347
    label "lepiej"
  ]
  node [
    id 348
    label "korzystnie"
  ]
  node [
    id 349
    label "pomy&#347;lnie"
  ]
  node [
    id 350
    label "pozytywnie"
  ]
  node [
    id 351
    label "dobroczynnie"
  ]
  node [
    id 352
    label "odpowiednio"
  ]
  node [
    id 353
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 354
    label "skutecznie"
  ]
  node [
    id 355
    label "asymilowa&#263;"
  ]
  node [
    id 356
    label "wapniak"
  ]
  node [
    id 357
    label "dwun&#243;g"
  ]
  node [
    id 358
    label "polifag"
  ]
  node [
    id 359
    label "wz&#243;r"
  ]
  node [
    id 360
    label "profanum"
  ]
  node [
    id 361
    label "hominid"
  ]
  node [
    id 362
    label "homo_sapiens"
  ]
  node [
    id 363
    label "nasada"
  ]
  node [
    id 364
    label "podw&#322;adny"
  ]
  node [
    id 365
    label "ludzko&#347;&#263;"
  ]
  node [
    id 366
    label "os&#322;abianie"
  ]
  node [
    id 367
    label "mikrokosmos"
  ]
  node [
    id 368
    label "portrecista"
  ]
  node [
    id 369
    label "duch"
  ]
  node [
    id 370
    label "oddzia&#322;ywanie"
  ]
  node [
    id 371
    label "g&#322;owa"
  ]
  node [
    id 372
    label "asymilowanie"
  ]
  node [
    id 373
    label "osoba"
  ]
  node [
    id 374
    label "os&#322;abia&#263;"
  ]
  node [
    id 375
    label "figura"
  ]
  node [
    id 376
    label "Adam"
  ]
  node [
    id 377
    label "senior"
  ]
  node [
    id 378
    label "antropochoria"
  ]
  node [
    id 379
    label "wra&#380;liwie"
  ]
  node [
    id 380
    label "wra&#378;liwy"
  ]
  node [
    id 381
    label "sk&#322;onny"
  ]
  node [
    id 382
    label "podatny"
  ]
  node [
    id 383
    label "wa&#380;ny"
  ]
  node [
    id 384
    label "nieoboj&#281;tny"
  ]
  node [
    id 385
    label "delikatny"
  ]
  node [
    id 386
    label "uwa&#380;ny"
  ]
  node [
    id 387
    label "badawczo"
  ]
  node [
    id 388
    label "przenikliwie"
  ]
  node [
    id 389
    label "&#380;ywy"
  ]
  node [
    id 390
    label "wnikliwy"
  ]
  node [
    id 391
    label "dojmuj&#261;co"
  ]
  node [
    id 392
    label "inteligentny"
  ]
  node [
    id 393
    label "dojmuj&#261;cy"
  ]
  node [
    id 394
    label "dociekliwy"
  ]
  node [
    id 395
    label "uk&#322;ad"
  ]
  node [
    id 396
    label "zawarto&#347;&#263;"
  ]
  node [
    id 397
    label "kaczki"
  ]
  node [
    id 398
    label "Jaros&#322;aw_Kaczy&#324;ski"
  ]
  node [
    id 399
    label "zakwaka&#263;"
  ]
  node [
    id 400
    label "naczynie"
  ]
  node [
    id 401
    label "dr&#243;b"
  ]
  node [
    id 402
    label "kwakanie"
  ]
  node [
    id 403
    label "samolot"
  ]
  node [
    id 404
    label "mi&#281;siwo"
  ]
  node [
    id 405
    label "efekt_gitarowy"
  ]
  node [
    id 406
    label "szybowiec"
  ]
  node [
    id 407
    label "kwakni&#281;cie"
  ]
  node [
    id 408
    label "kwaka&#263;"
  ]
  node [
    id 409
    label "ptak_wodny"
  ]
  node [
    id 410
    label "wielopestkowiec"
  ]
  node [
    id 411
    label "owoc"
  ]
  node [
    id 412
    label "regalia"
  ]
  node [
    id 413
    label "klejnot"
  ]
  node [
    id 414
    label "ogonek"
  ]
  node [
    id 415
    label "sk&#243;ra"
  ]
  node [
    id 416
    label "alkohol"
  ]
  node [
    id 417
    label "bolesno"
  ]
  node [
    id 418
    label "tkliwy"
  ]
  node [
    id 419
    label "bole&#347;nie"
  ]
  node [
    id 420
    label "niezadowolony"
  ]
  node [
    id 421
    label "przykry"
  ]
  node [
    id 422
    label "dotkliwy"
  ]
  node [
    id 423
    label "bole&#347;ny"
  ]
  node [
    id 424
    label "cierpi&#261;cy"
  ]
  node [
    id 425
    label "&#380;a&#322;osny"
  ]
  node [
    id 426
    label "g&#322;upi"
  ]
  node [
    id 427
    label "nieprzejrzysty"
  ]
  node [
    id 428
    label "ciemnow&#322;osy"
  ]
  node [
    id 429
    label "podejrzanie"
  ]
  node [
    id 430
    label "&#263;my"
  ]
  node [
    id 431
    label "zdrowy"
  ]
  node [
    id 432
    label "zacofany"
  ]
  node [
    id 433
    label "t&#281;py"
  ]
  node [
    id 434
    label "nierozumny"
  ]
  node [
    id 435
    label "niepewny"
  ]
  node [
    id 436
    label "z&#322;y"
  ]
  node [
    id 437
    label "pe&#322;ny"
  ]
  node [
    id 438
    label "&#347;niady"
  ]
  node [
    id 439
    label "niewykszta&#322;cony"
  ]
  node [
    id 440
    label "ciemno"
  ]
  node [
    id 441
    label "uwodzicielski"
  ]
  node [
    id 442
    label "wzmaga&#263;"
  ]
  node [
    id 443
    label "dose"
  ]
  node [
    id 444
    label "odmierza&#263;"
  ]
  node [
    id 445
    label "usztywnienie"
  ]
  node [
    id 446
    label "napr&#281;&#380;enie"
  ]
  node [
    id 447
    label "striving"
  ]
  node [
    id 448
    label "nastr&#243;j"
  ]
  node [
    id 449
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 450
    label "tension"
  ]
  node [
    id 451
    label "zdrowo"
  ]
  node [
    id 452
    label "kompetentnie"
  ]
  node [
    id 453
    label "udanie"
  ]
  node [
    id 454
    label "szybko"
  ]
  node [
    id 455
    label "funkcjonalnie"
  ]
  node [
    id 456
    label "sprawny"
  ]
  node [
    id 457
    label "umiej&#281;tnie"
  ]
  node [
    id 458
    label "produkowa&#263;"
  ]
  node [
    id 459
    label "wyjmowa&#263;"
  ]
  node [
    id 460
    label "sie&#263;"
  ]
  node [
    id 461
    label "my&#347;le&#263;"
  ]
  node [
    id 462
    label "paj&#261;k"
  ]
  node [
    id 463
    label "uk&#322;ada&#263;"
  ]
  node [
    id 464
    label "devise"
  ]
  node [
    id 465
    label "project"
  ]
  node [
    id 466
    label "report"
  ]
  node [
    id 467
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 468
    label "wypowied&#378;"
  ]
  node [
    id 469
    label "neografia"
  ]
  node [
    id 470
    label "przedmiot"
  ]
  node [
    id 471
    label "papirologia"
  ]
  node [
    id 472
    label "historia_gospodarcza"
  ]
  node [
    id 473
    label "przebiec"
  ]
  node [
    id 474
    label "hista"
  ]
  node [
    id 475
    label "nauka_humanistyczna"
  ]
  node [
    id 476
    label "filigranistyka"
  ]
  node [
    id 477
    label "dyplomatyka"
  ]
  node [
    id 478
    label "annalistyka"
  ]
  node [
    id 479
    label "historyka"
  ]
  node [
    id 480
    label "heraldyka"
  ]
  node [
    id 481
    label "fabu&#322;a"
  ]
  node [
    id 482
    label "muzealnictwo"
  ]
  node [
    id 483
    label "varsavianistyka"
  ]
  node [
    id 484
    label "mediewistyka"
  ]
  node [
    id 485
    label "prezentyzm"
  ]
  node [
    id 486
    label "przebiegni&#281;cie"
  ]
  node [
    id 487
    label "charakter"
  ]
  node [
    id 488
    label "paleografia"
  ]
  node [
    id 489
    label "genealogia"
  ]
  node [
    id 490
    label "prozopografia"
  ]
  node [
    id 491
    label "motyw"
  ]
  node [
    id 492
    label "nautologia"
  ]
  node [
    id 493
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 494
    label "epoka"
  ]
  node [
    id 495
    label "numizmatyka"
  ]
  node [
    id 496
    label "ruralistyka"
  ]
  node [
    id 497
    label "&#378;r&#243;d&#322;oznawstwo"
  ]
  node [
    id 498
    label "epigrafika"
  ]
  node [
    id 499
    label "historiografia"
  ]
  node [
    id 500
    label "bizantynistyka"
  ]
  node [
    id 501
    label "weksylologia"
  ]
  node [
    id 502
    label "kierunek"
  ]
  node [
    id 503
    label "ikonografia"
  ]
  node [
    id 504
    label "chronologia"
  ]
  node [
    id 505
    label "archiwistyka"
  ]
  node [
    id 506
    label "sfragistyka"
  ]
  node [
    id 507
    label "zabytkoznawstwo"
  ]
  node [
    id 508
    label "historia_sztuki"
  ]
  node [
    id 509
    label "consist"
  ]
  node [
    id 510
    label "raise"
  ]
  node [
    id 511
    label "pope&#322;nia&#263;"
  ]
  node [
    id 512
    label "wytwarza&#263;"
  ]
  node [
    id 513
    label "stanowi&#263;"
  ]
  node [
    id 514
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 515
    label "chmurno"
  ]
  node [
    id 516
    label "s&#281;pny"
  ]
  node [
    id 517
    label "pos&#281;pnie"
  ]
  node [
    id 518
    label "smutny"
  ]
  node [
    id 519
    label "skomplikowanie"
  ]
  node [
    id 520
    label "trudny"
  ]
  node [
    id 521
    label "system"
  ]
  node [
    id 522
    label "rozprz&#261;c"
  ]
  node [
    id 523
    label "konstrukcja"
  ]
  node [
    id 524
    label "zachowanie"
  ]
  node [
    id 525
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 526
    label "cybernetyk"
  ]
  node [
    id 527
    label "podsystem"
  ]
  node [
    id 528
    label "o&#347;"
  ]
  node [
    id 529
    label "konstelacja"
  ]
  node [
    id 530
    label "sk&#322;ad"
  ]
  node [
    id 531
    label "usenet"
  ]
  node [
    id 532
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 533
    label "mechanika"
  ]
  node [
    id 534
    label "systemat"
  ]
  node [
    id 535
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 536
    label "zwi&#261;zek"
  ]
  node [
    id 537
    label "relatywizowa&#263;"
  ]
  node [
    id 538
    label "tying"
  ]
  node [
    id 539
    label "po&#322;&#261;czenie"
  ]
  node [
    id 540
    label "zrelatywizowanie"
  ]
  node [
    id 541
    label "mention"
  ]
  node [
    id 542
    label "wi&#281;&#378;"
  ]
  node [
    id 543
    label "relatywizowanie"
  ]
  node [
    id 544
    label "pomy&#347;lenie"
  ]
  node [
    id 545
    label "zrelatywizowa&#263;"
  ]
  node [
    id 546
    label "kontakt"
  ]
  node [
    id 547
    label "bohaterski"
  ]
  node [
    id 548
    label "Zgredek"
  ]
  node [
    id 549
    label "Herkules"
  ]
  node [
    id 550
    label "Casanova"
  ]
  node [
    id 551
    label "Borewicz"
  ]
  node [
    id 552
    label "Don_Juan"
  ]
  node [
    id 553
    label "posta&#263;_mitologiczna"
  ]
  node [
    id 554
    label "Winnetou"
  ]
  node [
    id 555
    label "niezwyci&#281;&#380;ony"
  ]
  node [
    id 556
    label "Messi"
  ]
  node [
    id 557
    label "Herkules_Poirot"
  ]
  node [
    id 558
    label "cz&#322;owiek_wielkiego_ducha"
  ]
  node [
    id 559
    label "Szwejk"
  ]
  node [
    id 560
    label "Sherlock_Holmes"
  ]
  node [
    id 561
    label "Fantastyczna_Czw&#243;rka"
  ]
  node [
    id 562
    label "Hamlet"
  ]
  node [
    id 563
    label "Asterix"
  ]
  node [
    id 564
    label "Quasimodo"
  ]
  node [
    id 565
    label "Don_Kiszot"
  ]
  node [
    id 566
    label "Wallenrod"
  ]
  node [
    id 567
    label "uczestnik"
  ]
  node [
    id 568
    label "&#347;mia&#322;ek"
  ]
  node [
    id 569
    label "Harry_Potter"
  ]
  node [
    id 570
    label "podmiot"
  ]
  node [
    id 571
    label "Achilles"
  ]
  node [
    id 572
    label "Werter"
  ]
  node [
    id 573
    label "Mario"
  ]
  node [
    id 574
    label "p&#243;&#378;ny"
  ]
  node [
    id 575
    label "r&#243;wny"
  ]
  node [
    id 576
    label "dok&#322;adnie"
  ]
  node [
    id 577
    label "pos&#281;pny"
  ]
  node [
    id 578
    label "smutno"
  ]
  node [
    id 579
    label "sourly"
  ]
  node [
    id 580
    label "dourly"
  ]
  node [
    id 581
    label "moodily"
  ]
  node [
    id 582
    label "gloweringly"
  ]
  node [
    id 583
    label "dispiritedly"
  ]
  node [
    id 584
    label "pos&#281;pno"
  ]
  node [
    id 585
    label "darkly"
  ]
  node [
    id 586
    label "morosely"
  ]
  node [
    id 587
    label "unravel"
  ]
  node [
    id 588
    label "rozwi&#261;zywa&#263;"
  ]
  node [
    id 589
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 590
    label "pole"
  ]
  node [
    id 591
    label "kastowo&#347;&#263;"
  ]
  node [
    id 592
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 593
    label "ludzie_pracy"
  ]
  node [
    id 594
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 595
    label "community"
  ]
  node [
    id 596
    label "Fremeni"
  ]
  node [
    id 597
    label "status"
  ]
  node [
    id 598
    label "pozaklasowy"
  ]
  node [
    id 599
    label "aspo&#322;eczny"
  ]
  node [
    id 600
    label "ilo&#347;&#263;"
  ]
  node [
    id 601
    label "uwarstwienie"
  ]
  node [
    id 602
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 603
    label "zlewanie_si&#281;"
  ]
  node [
    id 604
    label "elita"
  ]
  node [
    id 605
    label "cywilizacja"
  ]
  node [
    id 606
    label "wspo&#322;ecze&#324;stwo"
  ]
  node [
    id 607
    label "klasa"
  ]
  node [
    id 608
    label "czyj&#347;"
  ]
  node [
    id 609
    label "m&#261;&#380;"
  ]
  node [
    id 610
    label "spell"
  ]
  node [
    id 611
    label "odgrywa&#263;_rol&#281;"
  ]
  node [
    id 612
    label "zostawia&#263;"
  ]
  node [
    id 613
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 614
    label "represent"
  ]
  node [
    id 615
    label "count"
  ]
  node [
    id 616
    label "wyraz"
  ]
  node [
    id 617
    label "zdobi&#263;"
  ]
  node [
    id 618
    label "free"
  ]
  node [
    id 619
    label "amuse"
  ]
  node [
    id 620
    label "od&#322;&#261;czy&#263;"
  ]
  node [
    id 621
    label "oddali&#263;"
  ]
  node [
    id 622
    label "detach"
  ]
  node [
    id 623
    label "przeszkodzi&#263;"
  ]
  node [
    id 624
    label "pluck"
  ]
  node [
    id 625
    label "tear"
  ]
  node [
    id 626
    label "podzieli&#263;"
  ]
  node [
    id 627
    label "moderate"
  ]
  node [
    id 628
    label "powie&#347;&#263;_cykliczna"
  ]
  node [
    id 629
    label "proza"
  ]
  node [
    id 630
    label "gatunek_literacki"
  ]
  node [
    id 631
    label "utw&#243;r_epicki"
  ]
  node [
    id 632
    label "doprowadzi&#263;"
  ]
  node [
    id 633
    label "marynistyczny"
  ]
  node [
    id 634
    label "przewaga"
  ]
  node [
    id 635
    label "drastyczny"
  ]
  node [
    id 636
    label "agresja"
  ]
  node [
    id 637
    label "patologia"
  ]
  node [
    id 638
    label "odciele&#347;nienie_si&#281;"
  ]
  node [
    id 639
    label "nerwowo_chory"
  ]
  node [
    id 640
    label "odciele&#347;nianie_si&#281;"
  ]
  node [
    id 641
    label "niematerialny"
  ]
  node [
    id 642
    label "niezr&#243;wnowa&#380;ony"
  ]
  node [
    id 643
    label "psychiatra"
  ]
  node [
    id 644
    label "nienormalny"
  ]
  node [
    id 645
    label "psychicznie"
  ]
  node [
    id 646
    label "p&#322;ciowo"
  ]
  node [
    id 647
    label "seksualnie"
  ]
  node [
    id 648
    label "seksowny"
  ]
  node [
    id 649
    label "erotyczny"
  ]
  node [
    id 650
    label "cz&#281;sto"
  ]
  node [
    id 651
    label "bardzo"
  ]
  node [
    id 652
    label "mocno"
  ]
  node [
    id 653
    label "ci&#261;gle"
  ]
  node [
    id 654
    label "rede"
  ]
  node [
    id 655
    label "assent"
  ]
  node [
    id 656
    label "oceni&#263;"
  ]
  node [
    id 657
    label "see"
  ]
  node [
    id 658
    label "thing"
  ]
  node [
    id 659
    label "cosik"
  ]
  node [
    id 660
    label "poziom"
  ]
  node [
    id 661
    label "faza"
  ]
  node [
    id 662
    label "depression"
  ]
  node [
    id 663
    label "zjawisko"
  ]
  node [
    id 664
    label "nizina"
  ]
  node [
    id 665
    label "solidny"
  ]
  node [
    id 666
    label "tre&#347;ciwie"
  ]
  node [
    id 667
    label "&#322;adnie"
  ]
  node [
    id 668
    label "konkretny"
  ]
  node [
    id 669
    label "jasno"
  ]
  node [
    id 670
    label "posilnie"
  ]
  node [
    id 671
    label "po&#380;ywnie"
  ]
  node [
    id 672
    label "Aspazja"
  ]
  node [
    id 673
    label "charakterystyka"
  ]
  node [
    id 674
    label "punkt_widzenia"
  ]
  node [
    id 675
    label "poby&#263;"
  ]
  node [
    id 676
    label "kompleksja"
  ]
  node [
    id 677
    label "Osjan"
  ]
  node [
    id 678
    label "wytw&#243;r"
  ]
  node [
    id 679
    label "budowa"
  ]
  node [
    id 680
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 681
    label "formacja"
  ]
  node [
    id 682
    label "pozosta&#263;"
  ]
  node [
    id 683
    label "point"
  ]
  node [
    id 684
    label "zaistnie&#263;"
  ]
  node [
    id 685
    label "go&#347;&#263;"
  ]
  node [
    id 686
    label "osobowo&#347;&#263;"
  ]
  node [
    id 687
    label "trim"
  ]
  node [
    id 688
    label "wygl&#261;d"
  ]
  node [
    id 689
    label "przedstawienie"
  ]
  node [
    id 690
    label "wytrzyma&#263;"
  ]
  node [
    id 691
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 692
    label "kto&#347;"
  ]
  node [
    id 693
    label "przeszkala&#263;"
  ]
  node [
    id 694
    label "warto&#347;&#263;"
  ]
  node [
    id 695
    label "informowa&#263;"
  ]
  node [
    id 696
    label "introduce"
  ]
  node [
    id 697
    label "bespeak"
  ]
  node [
    id 698
    label "indicate"
  ]
  node [
    id 699
    label "wyra&#380;a&#263;"
  ]
  node [
    id 700
    label "exhibit"
  ]
  node [
    id 701
    label "podawa&#263;"
  ]
  node [
    id 702
    label "exsert"
  ]
  node [
    id 703
    label "przedstawia&#263;"
  ]
  node [
    id 704
    label "uniwersalnie"
  ]
  node [
    id 705
    label "generalny"
  ]
  node [
    id 706
    label "wszechstronnie"
  ]
  node [
    id 707
    label "szczeg&#243;&#322;"
  ]
  node [
    id 708
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 709
    label "state"
  ]
  node [
    id 710
    label "realia"
  ]
  node [
    id 711
    label "warunki"
  ]
  node [
    id 712
    label "byd&#322;o"
  ]
  node [
    id 713
    label "zobo"
  ]
  node [
    id 714
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 715
    label "yakalo"
  ]
  node [
    id 716
    label "dzo"
  ]
  node [
    id 717
    label "pomy&#347;lny"
  ]
  node [
    id 718
    label "skuteczny"
  ]
  node [
    id 719
    label "moralny"
  ]
  node [
    id 720
    label "korzystny"
  ]
  node [
    id 721
    label "odpowiedni"
  ]
  node [
    id 722
    label "pozytywny"
  ]
  node [
    id 723
    label "grzeczny"
  ]
  node [
    id 724
    label "powitanie"
  ]
  node [
    id 725
    label "mi&#322;y"
  ]
  node [
    id 726
    label "dobroczynny"
  ]
  node [
    id 727
    label "pos&#322;uszny"
  ]
  node [
    id 728
    label "ca&#322;y"
  ]
  node [
    id 729
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 730
    label "czw&#243;rka"
  ]
  node [
    id 731
    label "spokojny"
  ]
  node [
    id 732
    label "&#347;mieszny"
  ]
  node [
    id 733
    label "drogi"
  ]
  node [
    id 734
    label "dokument"
  ]
  node [
    id 735
    label "amorfizm"
  ]
  node [
    id 736
    label "liryka"
  ]
  node [
    id 737
    label "bibliografia"
  ]
  node [
    id 738
    label "translator"
  ]
  node [
    id 739
    label "dramat"
  ]
  node [
    id 740
    label "pi&#347;miennictwo"
  ]
  node [
    id 741
    label "zoologia_fantastyczna"
  ]
  node [
    id 742
    label "pisarstwo"
  ]
  node [
    id 743
    label "epika"
  ]
  node [
    id 744
    label "sztuka"
  ]
  node [
    id 745
    label "proceed"
  ]
  node [
    id 746
    label "pokazywa&#263;_si&#281;"
  ]
  node [
    id 747
    label "zdarza&#263;_si&#281;"
  ]
  node [
    id 748
    label "&#380;egna&#263;"
  ]
  node [
    id 749
    label "cudowa&#263;"
  ]
  node [
    id 750
    label "narzeka&#263;"
  ]
  node [
    id 751
    label "sting"
  ]
  node [
    id 752
    label "zniech&#281;ci&#263;"
  ]
  node [
    id 753
    label "potrawa"
  ]
  node [
    id 754
    label "danie"
  ]
  node [
    id 755
    label "fina&#322;"
  ]
  node [
    id 756
    label "wyrobi&#263;"
  ]
  node [
    id 757
    label "przygotowa&#263;"
  ]
  node [
    id 758
    label "catch"
  ]
  node [
    id 759
    label "frame"
  ]
  node [
    id 760
    label "map"
  ]
  node [
    id 761
    label "u&#380;ywka"
  ]
  node [
    id 762
    label "teina"
  ]
  node [
    id 763
    label "kamelia"
  ]
  node [
    id 764
    label "krzew"
  ]
  node [
    id 765
    label "nap&#243;j"
  ]
  node [
    id 766
    label "ro&#347;lina"
  ]
  node [
    id 767
    label "egzotyk"
  ]
  node [
    id 768
    label "napar"
  ]
  node [
    id 769
    label "teofilina"
  ]
  node [
    id 770
    label "parzy&#263;"
  ]
  node [
    id 771
    label "susz"
  ]
  node [
    id 772
    label "och&#322;odzi&#263;_si&#281;"
  ]
  node [
    id 773
    label "zanikn&#261;&#263;"
  ]
  node [
    id 774
    label "emocja"
  ]
  node [
    id 775
    label "wypi&#281;knienie"
  ]
  node [
    id 776
    label "pi&#281;knienie"
  ]
  node [
    id 777
    label "szlachetnie"
  ]
  node [
    id 778
    label "po&#380;&#261;dany"
  ]
  node [
    id 779
    label "cudowny"
  ]
  node [
    id 780
    label "wspania&#322;y"
  ]
  node [
    id 781
    label "skandaliczny"
  ]
  node [
    id 782
    label "pi&#281;knie"
  ]
  node [
    id 783
    label "gor&#261;cy"
  ]
  node [
    id 784
    label "okaza&#322;y"
  ]
  node [
    id 785
    label "wzruszaj&#261;cy"
  ]
  node [
    id 786
    label "zapi&#281;tek"
  ]
  node [
    id 787
    label "zel&#243;wka"
  ]
  node [
    id 788
    label "cholewka"
  ]
  node [
    id 789
    label "wzu&#263;"
  ]
  node [
    id 790
    label "obcas"
  ]
  node [
    id 791
    label "j&#281;zyk"
  ]
  node [
    id 792
    label "raki"
  ]
  node [
    id 793
    label "sznurowad&#322;o"
  ]
  node [
    id 794
    label "wzucie"
  ]
  node [
    id 795
    label "napi&#281;tek"
  ]
  node [
    id 796
    label "podeszwa"
  ]
  node [
    id 797
    label "obuwie"
  ]
  node [
    id 798
    label "wzuwanie"
  ]
  node [
    id 799
    label "rozbijarka"
  ]
  node [
    id 800
    label "cholewa"
  ]
  node [
    id 801
    label "przyszwa"
  ]
  node [
    id 802
    label "majdn&#261;&#263;"
  ]
  node [
    id 803
    label "pierdoln&#261;&#263;"
  ]
  node [
    id 804
    label "polewa&#263;"
  ]
  node [
    id 805
    label "sygn&#261;&#263;"
  ]
  node [
    id 806
    label "peddle"
  ]
  node [
    id 807
    label "zmieni&#263;"
  ]
  node [
    id 808
    label "continue"
  ]
  node [
    id 809
    label "zmusza&#263;"
  ]
  node [
    id 810
    label "uniemo&#380;liwia&#263;"
  ]
  node [
    id 811
    label "sympatyzowa&#263;"
  ]
  node [
    id 812
    label "pozostawa&#263;"
  ]
  node [
    id 813
    label "zachowywa&#263;"
  ]
  node [
    id 814
    label "utrzymywa&#263;"
  ]
  node [
    id 815
    label "utrzymywa&#263;_si&#281;"
  ]
  node [
    id 816
    label "treat"
  ]
  node [
    id 817
    label "przetrzymywa&#263;"
  ]
  node [
    id 818
    label "sprawowa&#263;"
  ]
  node [
    id 819
    label "administrowa&#263;"
  ]
  node [
    id 820
    label "adhere"
  ]
  node [
    id 821
    label "dzier&#380;y&#263;"
  ]
  node [
    id 822
    label "podtrzymywa&#263;"
  ]
  node [
    id 823
    label "argue"
  ]
  node [
    id 824
    label "hodowa&#263;"
  ]
  node [
    id 825
    label "wychowywa&#263;"
  ]
  node [
    id 826
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 827
    label "okre&#347;lony"
  ]
  node [
    id 828
    label "obraz"
  ]
  node [
    id 829
    label "przestrze&#324;"
  ]
  node [
    id 830
    label "teren"
  ]
  node [
    id 831
    label "perspektywa"
  ]
  node [
    id 832
    label "j&#281;cze&#263;"
  ]
  node [
    id 833
    label "martwi&#263;_si&#281;"
  ]
  node [
    id 834
    label "wytrzymywa&#263;"
  ]
  node [
    id 835
    label "czu&#263;"
  ]
  node [
    id 836
    label "pochyla&#263;_si&#281;"
  ]
  node [
    id 837
    label "traci&#263;"
  ]
  node [
    id 838
    label "doznawa&#263;"
  ]
  node [
    id 839
    label "hurt"
  ]
  node [
    id 840
    label "boryka&#263;_si&#281;"
  ]
  node [
    id 841
    label "strona"
  ]
  node [
    id 842
    label "przyczyna"
  ]
  node [
    id 843
    label "matuszka"
  ]
  node [
    id 844
    label "geneza"
  ]
  node [
    id 845
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 846
    label "czynnik"
  ]
  node [
    id 847
    label "poci&#261;ganie"
  ]
  node [
    id 848
    label "rezultat"
  ]
  node [
    id 849
    label "uprz&#261;&#380;"
  ]
  node [
    id 850
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 851
    label "subject"
  ]
  node [
    id 852
    label "zadanie"
  ]
  node [
    id 853
    label "problemat"
  ]
  node [
    id 854
    label "rozpytywanie"
  ]
  node [
    id 855
    label "sprawdzian"
  ]
  node [
    id 856
    label "przes&#322;uchiwanie"
  ]
  node [
    id 857
    label "wypytanie"
  ]
  node [
    id 858
    label "zwracanie_si&#281;"
  ]
  node [
    id 859
    label "wypowiedzenie"
  ]
  node [
    id 860
    label "wywo&#322;ywanie"
  ]
  node [
    id 861
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 862
    label "problematyka"
  ]
  node [
    id 863
    label "question"
  ]
  node [
    id 864
    label "sprawdzanie"
  ]
  node [
    id 865
    label "odpowiadanie"
  ]
  node [
    id 866
    label "survey"
  ]
  node [
    id 867
    label "odpowiada&#263;"
  ]
  node [
    id 868
    label "egzaminowanie"
  ]
  node [
    id 869
    label "str&#243;j"
  ]
  node [
    id 870
    label "mie&#263;"
  ]
  node [
    id 871
    label "wk&#322;ada&#263;"
  ]
  node [
    id 872
    label "przemieszcza&#263;"
  ]
  node [
    id 873
    label "posiada&#263;"
  ]
  node [
    id 874
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 875
    label "wear"
  ]
  node [
    id 876
    label "carry"
  ]
  node [
    id 877
    label "roll"
  ]
  node [
    id 878
    label "wypierdoli&#263;"
  ]
  node [
    id 879
    label "powiedzie&#263;"
  ]
  node [
    id 880
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 881
    label "pozby&#263;_si&#281;"
  ]
  node [
    id 882
    label "usun&#261;&#263;"
  ]
  node [
    id 883
    label "arouse"
  ]
  node [
    id 884
    label "wychrzani&#263;"
  ]
  node [
    id 885
    label "wypierniczy&#263;"
  ]
  node [
    id 886
    label "wysadzi&#263;"
  ]
  node [
    id 887
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 888
    label "reproach"
  ]
  node [
    id 889
    label "&#347;miecisko"
  ]
  node [
    id 890
    label "pojemnik"
  ]
  node [
    id 891
    label "zbiornik"
  ]
  node [
    id 892
    label "sk&#322;adowisko"
  ]
  node [
    id 893
    label "pacjent"
  ]
  node [
    id 894
    label "schorzenie"
  ]
  node [
    id 895
    label "przeznaczenie"
  ]
  node [
    id 896
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 897
    label "wydarzenie"
  ]
  node [
    id 898
    label "happening"
  ]
  node [
    id 899
    label "przyk&#322;ad"
  ]
  node [
    id 900
    label "tu"
  ]
  node [
    id 901
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 902
    label "sp&#281;dza&#263;"
  ]
  node [
    id 903
    label "overcharge"
  ]
  node [
    id 904
    label "przeszukiwa&#263;"
  ]
  node [
    id 905
    label "ciekawy"
  ]
  node [
    id 906
    label "jako_tako"
  ]
  node [
    id 907
    label "dziwny"
  ]
  node [
    id 908
    label "niez&#322;y"
  ]
  node [
    id 909
    label "przyzwoity"
  ]
  node [
    id 910
    label "beztroskliwy"
  ]
  node [
    id 911
    label "niewa&#380;ny"
  ]
  node [
    id 912
    label "letki"
  ]
  node [
    id 913
    label "nierozwa&#380;ny"
  ]
  node [
    id 914
    label "podniecaj&#261;cy"
  ]
  node [
    id 915
    label "Swedish"
  ]
  node [
    id 916
    label "skandynawski"
  ]
  node [
    id 917
    label "po_szwedzku"
  ]
  node [
    id 918
    label "j&#281;zyk_germa&#324;ski"
  ]
  node [
    id 919
    label "cz&#322;onek"
  ]
  node [
    id 920
    label "przedstawiciel"
  ]
  node [
    id 921
    label "reprezentant"
  ]
  node [
    id 922
    label "akademia"
  ]
  node [
    id 923
    label "pracownik_naukowy"
  ]
  node [
    id 924
    label "artysta"
  ]
  node [
    id 925
    label "student"
  ]
  node [
    id 926
    label "dom"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 26
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 199
  ]
  edge [
    source 9
    target 200
  ]
  edge [
    source 9
    target 201
  ]
  edge [
    source 9
    target 202
  ]
  edge [
    source 9
    target 203
  ]
  edge [
    source 9
    target 204
  ]
  edge [
    source 9
    target 205
  ]
  edge [
    source 9
    target 206
  ]
  edge [
    source 9
    target 207
  ]
  edge [
    source 9
    target 208
  ]
  edge [
    source 9
    target 209
  ]
  edge [
    source 9
    target 210
  ]
  edge [
    source 9
    target 211
  ]
  edge [
    source 9
    target 212
  ]
  edge [
    source 9
    target 12
  ]
  edge [
    source 9
    target 23
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 38
  ]
  edge [
    source 10
    target 39
  ]
  edge [
    source 10
    target 70
  ]
  edge [
    source 10
    target 71
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 17
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 115
  ]
  edge [
    source 12
    target 116
  ]
  edge [
    source 12
    target 213
  ]
  edge [
    source 12
    target 214
  ]
  edge [
    source 12
    target 215
  ]
  edge [
    source 12
    target 216
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 12
    target 217
  ]
  edge [
    source 12
    target 218
  ]
  edge [
    source 12
    target 219
  ]
  edge [
    source 12
    target 220
  ]
  edge [
    source 12
    target 221
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 222
  ]
  edge [
    source 15
    target 223
  ]
  edge [
    source 15
    target 224
  ]
  edge [
    source 15
    target 225
  ]
  edge [
    source 15
    target 226
  ]
  edge [
    source 15
    target 78
  ]
  edge [
    source 15
    target 33
  ]
  edge [
    source 15
    target 89
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 227
  ]
  edge [
    source 16
    target 228
  ]
  edge [
    source 16
    target 229
  ]
  edge [
    source 16
    target 230
  ]
  edge [
    source 16
    target 231
  ]
  edge [
    source 16
    target 120
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 99
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 232
  ]
  edge [
    source 18
    target 233
  ]
  edge [
    source 18
    target 234
  ]
  edge [
    source 18
    target 235
  ]
  edge [
    source 18
    target 236
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 29
  ]
  edge [
    source 19
    target 30
  ]
  edge [
    source 19
    target 35
  ]
  edge [
    source 19
    target 36
  ]
  edge [
    source 19
    target 48
  ]
  edge [
    source 19
    target 49
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 237
  ]
  edge [
    source 20
    target 238
  ]
  edge [
    source 20
    target 239
  ]
  edge [
    source 20
    target 22
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 240
  ]
  edge [
    source 21
    target 241
  ]
  edge [
    source 21
    target 242
  ]
  edge [
    source 21
    target 243
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 244
  ]
  edge [
    source 23
    target 245
  ]
  edge [
    source 23
    target 246
  ]
  edge [
    source 23
    target 247
  ]
  edge [
    source 23
    target 248
  ]
  edge [
    source 23
    target 249
  ]
  edge [
    source 23
    target 250
  ]
  edge [
    source 23
    target 251
  ]
  edge [
    source 23
    target 252
  ]
  edge [
    source 23
    target 253
  ]
  edge [
    source 23
    target 254
  ]
  edge [
    source 23
    target 255
  ]
  edge [
    source 23
    target 256
  ]
  edge [
    source 23
    target 257
  ]
  edge [
    source 23
    target 258
  ]
  edge [
    source 23
    target 259
  ]
  edge [
    source 23
    target 260
  ]
  edge [
    source 23
    target 261
  ]
  edge [
    source 23
    target 262
  ]
  edge [
    source 23
    target 263
  ]
  edge [
    source 23
    target 264
  ]
  edge [
    source 23
    target 265
  ]
  edge [
    source 23
    target 266
  ]
  edge [
    source 23
    target 267
  ]
  edge [
    source 23
    target 268
  ]
  edge [
    source 23
    target 269
  ]
  edge [
    source 23
    target 270
  ]
  edge [
    source 23
    target 271
  ]
  edge [
    source 23
    target 272
  ]
  edge [
    source 23
    target 273
  ]
  edge [
    source 23
    target 274
  ]
  edge [
    source 23
    target 275
  ]
  edge [
    source 23
    target 276
  ]
  edge [
    source 23
    target 277
  ]
  edge [
    source 23
    target 278
  ]
  edge [
    source 23
    target 279
  ]
  edge [
    source 23
    target 97
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 280
  ]
  edge [
    source 24
    target 281
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 282
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 55
  ]
  edge [
    source 26
    target 79
  ]
  edge [
    source 26
    target 88
  ]
  edge [
    source 26
    target 117
  ]
  edge [
    source 26
    target 118
  ]
  edge [
    source 26
    target 283
  ]
  edge [
    source 26
    target 284
  ]
  edge [
    source 26
    target 285
  ]
  edge [
    source 26
    target 286
  ]
  edge [
    source 26
    target 287
  ]
  edge [
    source 26
    target 288
  ]
  edge [
    source 26
    target 289
  ]
  edge [
    source 26
    target 290
  ]
  edge [
    source 26
    target 291
  ]
  edge [
    source 26
    target 292
  ]
  edge [
    source 26
    target 293
  ]
  edge [
    source 26
    target 69
  ]
  edge [
    source 26
    target 84
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 115
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 294
  ]
  edge [
    source 29
    target 295
  ]
  edge [
    source 29
    target 296
  ]
  edge [
    source 29
    target 34
  ]
  edge [
    source 29
    target 297
  ]
  edge [
    source 29
    target 40
  ]
  edge [
    source 29
    target 33
  ]
  edge [
    source 29
    target 36
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 298
  ]
  edge [
    source 30
    target 299
  ]
  edge [
    source 30
    target 300
  ]
  edge [
    source 30
    target 301
  ]
  edge [
    source 30
    target 302
  ]
  edge [
    source 30
    target 303
  ]
  edge [
    source 30
    target 304
  ]
  edge [
    source 30
    target 305
  ]
  edge [
    source 30
    target 306
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 307
  ]
  edge [
    source 31
    target 308
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 309
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 46
  ]
  edge [
    source 33
    target 49
  ]
  edge [
    source 33
    target 50
  ]
  edge [
    source 33
    target 310
  ]
  edge [
    source 33
    target 311
  ]
  edge [
    source 33
    target 312
  ]
  edge [
    source 33
    target 313
  ]
  edge [
    source 33
    target 314
  ]
  edge [
    source 33
    target 315
  ]
  edge [
    source 33
    target 316
  ]
  edge [
    source 33
    target 317
  ]
  edge [
    source 33
    target 318
  ]
  edge [
    source 33
    target 319
  ]
  edge [
    source 33
    target 320
  ]
  edge [
    source 33
    target 40
  ]
  edge [
    source 33
    target 36
  ]
  edge [
    source 33
    target 89
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 47
  ]
  edge [
    source 34
    target 321
  ]
  edge [
    source 34
    target 322
  ]
  edge [
    source 34
    target 323
  ]
  edge [
    source 34
    target 324
  ]
  edge [
    source 34
    target 325
  ]
  edge [
    source 34
    target 326
  ]
  edge [
    source 34
    target 327
  ]
  edge [
    source 34
    target 328
  ]
  edge [
    source 34
    target 329
  ]
  edge [
    source 34
    target 330
  ]
  edge [
    source 34
    target 331
  ]
  edge [
    source 34
    target 332
  ]
  edge [
    source 34
    target 333
  ]
  edge [
    source 34
    target 334
  ]
  edge [
    source 34
    target 335
  ]
  edge [
    source 34
    target 336
  ]
  edge [
    source 34
    target 337
  ]
  edge [
    source 34
    target 338
  ]
  edge [
    source 34
    target 339
  ]
  edge [
    source 34
    target 340
  ]
  edge [
    source 34
    target 341
  ]
  edge [
    source 34
    target 342
  ]
  edge [
    source 34
    target 78
  ]
  edge [
    source 35
    target 343
  ]
  edge [
    source 35
    target 344
  ]
  edge [
    source 35
    target 345
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 62
  ]
  edge [
    source 36
    target 63
  ]
  edge [
    source 36
    target 102
  ]
  edge [
    source 36
    target 103
  ]
  edge [
    source 36
    target 36
  ]
  edge [
    source 36
    target 40
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 113
  ]
  edge [
    source 37
    target 114
  ]
  edge [
    source 37
    target 346
  ]
  edge [
    source 37
    target 347
  ]
  edge [
    source 37
    target 348
  ]
  edge [
    source 37
    target 349
  ]
  edge [
    source 37
    target 350
  ]
  edge [
    source 37
    target 89
  ]
  edge [
    source 37
    target 351
  ]
  edge [
    source 37
    target 352
  ]
  edge [
    source 37
    target 353
  ]
  edge [
    source 37
    target 354
  ]
  edge [
    source 37
    target 53
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 355
  ]
  edge [
    source 39
    target 356
  ]
  edge [
    source 39
    target 357
  ]
  edge [
    source 39
    target 358
  ]
  edge [
    source 39
    target 359
  ]
  edge [
    source 39
    target 360
  ]
  edge [
    source 39
    target 361
  ]
  edge [
    source 39
    target 362
  ]
  edge [
    source 39
    target 363
  ]
  edge [
    source 39
    target 364
  ]
  edge [
    source 39
    target 365
  ]
  edge [
    source 39
    target 366
  ]
  edge [
    source 39
    target 367
  ]
  edge [
    source 39
    target 368
  ]
  edge [
    source 39
    target 369
  ]
  edge [
    source 39
    target 370
  ]
  edge [
    source 39
    target 371
  ]
  edge [
    source 39
    target 372
  ]
  edge [
    source 39
    target 373
  ]
  edge [
    source 39
    target 374
  ]
  edge [
    source 39
    target 375
  ]
  edge [
    source 39
    target 376
  ]
  edge [
    source 39
    target 377
  ]
  edge [
    source 39
    target 378
  ]
  edge [
    source 39
    target 83
  ]
  edge [
    source 39
    target 62
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 379
  ]
  edge [
    source 40
    target 380
  ]
  edge [
    source 40
    target 381
  ]
  edge [
    source 40
    target 382
  ]
  edge [
    source 40
    target 383
  ]
  edge [
    source 40
    target 384
  ]
  edge [
    source 40
    target 385
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 386
  ]
  edge [
    source 41
    target 387
  ]
  edge [
    source 41
    target 388
  ]
  edge [
    source 41
    target 389
  ]
  edge [
    source 41
    target 390
  ]
  edge [
    source 41
    target 391
  ]
  edge [
    source 41
    target 392
  ]
  edge [
    source 41
    target 393
  ]
  edge [
    source 41
    target 394
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 395
  ]
  edge [
    source 44
    target 396
  ]
  edge [
    source 44
    target 397
  ]
  edge [
    source 44
    target 398
  ]
  edge [
    source 44
    target 195
  ]
  edge [
    source 44
    target 399
  ]
  edge [
    source 44
    target 400
  ]
  edge [
    source 44
    target 401
  ]
  edge [
    source 44
    target 402
  ]
  edge [
    source 44
    target 403
  ]
  edge [
    source 44
    target 404
  ]
  edge [
    source 44
    target 405
  ]
  edge [
    source 44
    target 406
  ]
  edge [
    source 44
    target 407
  ]
  edge [
    source 44
    target 408
  ]
  edge [
    source 44
    target 409
  ]
  edge [
    source 44
    target 59
  ]
  edge [
    source 44
    target 64
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 410
  ]
  edge [
    source 45
    target 411
  ]
  edge [
    source 45
    target 412
  ]
  edge [
    source 45
    target 413
  ]
  edge [
    source 45
    target 414
  ]
  edge [
    source 45
    target 415
  ]
  edge [
    source 45
    target 60
  ]
  edge [
    source 46
    target 416
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 417
  ]
  edge [
    source 47
    target 418
  ]
  edge [
    source 47
    target 419
  ]
  edge [
    source 47
    target 420
  ]
  edge [
    source 47
    target 421
  ]
  edge [
    source 47
    target 422
  ]
  edge [
    source 47
    target 423
  ]
  edge [
    source 47
    target 424
  ]
  edge [
    source 47
    target 425
  ]
  edge [
    source 47
    target 393
  ]
  edge [
    source 48
    target 426
  ]
  edge [
    source 48
    target 427
  ]
  edge [
    source 48
    target 428
  ]
  edge [
    source 48
    target 429
  ]
  edge [
    source 48
    target 430
  ]
  edge [
    source 48
    target 431
  ]
  edge [
    source 48
    target 432
  ]
  edge [
    source 48
    target 433
  ]
  edge [
    source 48
    target 434
  ]
  edge [
    source 48
    target 435
  ]
  edge [
    source 48
    target 436
  ]
  edge [
    source 48
    target 437
  ]
  edge [
    source 48
    target 438
  ]
  edge [
    source 48
    target 439
  ]
  edge [
    source 48
    target 440
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 441
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 442
  ]
  edge [
    source 51
    target 443
  ]
  edge [
    source 51
    target 444
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 65
  ]
  edge [
    source 52
    target 66
  ]
  edge [
    source 52
    target 74
  ]
  edge [
    source 52
    target 75
  ]
  edge [
    source 52
    target 445
  ]
  edge [
    source 52
    target 446
  ]
  edge [
    source 52
    target 447
  ]
  edge [
    source 52
    target 448
  ]
  edge [
    source 52
    target 449
  ]
  edge [
    source 52
    target 450
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 451
  ]
  edge [
    source 53
    target 452
  ]
  edge [
    source 53
    target 453
  ]
  edge [
    source 53
    target 454
  ]
  edge [
    source 53
    target 455
  ]
  edge [
    source 53
    target 456
  ]
  edge [
    source 53
    target 457
  ]
  edge [
    source 53
    target 354
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 458
  ]
  edge [
    source 54
    target 459
  ]
  edge [
    source 54
    target 460
  ]
  edge [
    source 54
    target 461
  ]
  edge [
    source 54
    target 462
  ]
  edge [
    source 54
    target 463
  ]
  edge [
    source 54
    target 56
  ]
  edge [
    source 54
    target 464
  ]
  edge [
    source 54
    target 465
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 78
  ]
  edge [
    source 55
    target 80
  ]
  edge [
    source 55
    target 81
  ]
  edge [
    source 55
    target 466
  ]
  edge [
    source 55
    target 467
  ]
  edge [
    source 55
    target 468
  ]
  edge [
    source 55
    target 469
  ]
  edge [
    source 55
    target 470
  ]
  edge [
    source 55
    target 471
  ]
  edge [
    source 55
    target 472
  ]
  edge [
    source 55
    target 473
  ]
  edge [
    source 55
    target 474
  ]
  edge [
    source 55
    target 475
  ]
  edge [
    source 55
    target 476
  ]
  edge [
    source 55
    target 477
  ]
  edge [
    source 55
    target 478
  ]
  edge [
    source 55
    target 479
  ]
  edge [
    source 55
    target 480
  ]
  edge [
    source 55
    target 481
  ]
  edge [
    source 55
    target 482
  ]
  edge [
    source 55
    target 483
  ]
  edge [
    source 55
    target 484
  ]
  edge [
    source 55
    target 485
  ]
  edge [
    source 55
    target 486
  ]
  edge [
    source 55
    target 487
  ]
  edge [
    source 55
    target 488
  ]
  edge [
    source 55
    target 489
  ]
  edge [
    source 55
    target 238
  ]
  edge [
    source 55
    target 490
  ]
  edge [
    source 55
    target 491
  ]
  edge [
    source 55
    target 492
  ]
  edge [
    source 55
    target 493
  ]
  edge [
    source 55
    target 494
  ]
  edge [
    source 55
    target 495
  ]
  edge [
    source 55
    target 496
  ]
  edge [
    source 55
    target 497
  ]
  edge [
    source 55
    target 498
  ]
  edge [
    source 55
    target 499
  ]
  edge [
    source 55
    target 500
  ]
  edge [
    source 55
    target 501
  ]
  edge [
    source 55
    target 502
  ]
  edge [
    source 55
    target 503
  ]
  edge [
    source 55
    target 504
  ]
  edge [
    source 55
    target 505
  ]
  edge [
    source 55
    target 506
  ]
  edge [
    source 55
    target 507
  ]
  edge [
    source 55
    target 508
  ]
  edge [
    source 55
    target 70
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 245
  ]
  edge [
    source 56
    target 509
  ]
  edge [
    source 56
    target 510
  ]
  edge [
    source 56
    target 317
  ]
  edge [
    source 56
    target 511
  ]
  edge [
    source 56
    target 512
  ]
  edge [
    source 56
    target 513
  ]
  edge [
    source 56
    target 514
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 515
  ]
  edge [
    source 57
    target 516
  ]
  edge [
    source 57
    target 517
  ]
  edge [
    source 57
    target 65
  ]
  edge [
    source 57
    target 518
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 519
  ]
  edge [
    source 58
    target 520
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 521
  ]
  edge [
    source 59
    target 522
  ]
  edge [
    source 59
    target 523
  ]
  edge [
    source 59
    target 524
  ]
  edge [
    source 59
    target 525
  ]
  edge [
    source 59
    target 526
  ]
  edge [
    source 59
    target 527
  ]
  edge [
    source 59
    target 528
  ]
  edge [
    source 59
    target 529
  ]
  edge [
    source 59
    target 530
  ]
  edge [
    source 59
    target 334
  ]
  edge [
    source 59
    target 531
  ]
  edge [
    source 59
    target 532
  ]
  edge [
    source 59
    target 533
  ]
  edge [
    source 59
    target 534
  ]
  edge [
    source 59
    target 535
  ]
  edge [
    source 59
    target 64
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 536
  ]
  edge [
    source 60
    target 537
  ]
  edge [
    source 60
    target 538
  ]
  edge [
    source 60
    target 539
  ]
  edge [
    source 60
    target 540
  ]
  edge [
    source 60
    target 541
  ]
  edge [
    source 60
    target 542
  ]
  edge [
    source 60
    target 543
  ]
  edge [
    source 60
    target 544
  ]
  edge [
    source 60
    target 545
  ]
  edge [
    source 60
    target 546
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 62
    target 547
  ]
  edge [
    source 62
    target 548
  ]
  edge [
    source 62
    target 549
  ]
  edge [
    source 62
    target 550
  ]
  edge [
    source 62
    target 551
  ]
  edge [
    source 62
    target 552
  ]
  edge [
    source 62
    target 553
  ]
  edge [
    source 62
    target 554
  ]
  edge [
    source 62
    target 555
  ]
  edge [
    source 62
    target 556
  ]
  edge [
    source 62
    target 557
  ]
  edge [
    source 62
    target 558
  ]
  edge [
    source 62
    target 559
  ]
  edge [
    source 62
    target 560
  ]
  edge [
    source 62
    target 561
  ]
  edge [
    source 62
    target 562
  ]
  edge [
    source 62
    target 563
  ]
  edge [
    source 62
    target 564
  ]
  edge [
    source 62
    target 565
  ]
  edge [
    source 62
    target 566
  ]
  edge [
    source 62
    target 567
  ]
  edge [
    source 62
    target 568
  ]
  edge [
    source 62
    target 569
  ]
  edge [
    source 62
    target 570
  ]
  edge [
    source 62
    target 571
  ]
  edge [
    source 62
    target 572
  ]
  edge [
    source 62
    target 573
  ]
  edge [
    source 62
    target 83
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 574
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 575
  ]
  edge [
    source 64
    target 576
  ]
  edge [
    source 65
    target 577
  ]
  edge [
    source 65
    target 578
  ]
  edge [
    source 65
    target 579
  ]
  edge [
    source 65
    target 580
  ]
  edge [
    source 65
    target 581
  ]
  edge [
    source 65
    target 582
  ]
  edge [
    source 65
    target 583
  ]
  edge [
    source 65
    target 584
  ]
  edge [
    source 65
    target 585
  ]
  edge [
    source 65
    target 586
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 66
    target 587
  ]
  edge [
    source 66
    target 588
  ]
  edge [
    source 66
    target 82
  ]
  edge [
    source 66
    target 95
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 67
    target 589
  ]
  edge [
    source 67
    target 590
  ]
  edge [
    source 67
    target 591
  ]
  edge [
    source 67
    target 592
  ]
  edge [
    source 67
    target 593
  ]
  edge [
    source 67
    target 594
  ]
  edge [
    source 67
    target 595
  ]
  edge [
    source 67
    target 596
  ]
  edge [
    source 67
    target 597
  ]
  edge [
    source 67
    target 598
  ]
  edge [
    source 67
    target 599
  ]
  edge [
    source 67
    target 437
  ]
  edge [
    source 67
    target 600
  ]
  edge [
    source 67
    target 601
  ]
  edge [
    source 67
    target 602
  ]
  edge [
    source 67
    target 603
  ]
  edge [
    source 67
    target 604
  ]
  edge [
    source 67
    target 605
  ]
  edge [
    source 67
    target 606
  ]
  edge [
    source 67
    target 607
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 68
    target 608
  ]
  edge [
    source 68
    target 609
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 69
    target 610
  ]
  edge [
    source 69
    target 611
  ]
  edge [
    source 69
    target 612
  ]
  edge [
    source 69
    target 613
  ]
  edge [
    source 69
    target 614
  ]
  edge [
    source 69
    target 615
  ]
  edge [
    source 69
    target 616
  ]
  edge [
    source 69
    target 617
  ]
  edge [
    source 70
    target 76
  ]
  edge [
    source 70
    target 77
  ]
  edge [
    source 70
    target 618
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 71
    target 619
  ]
  edge [
    source 71
    target 620
  ]
  edge [
    source 71
    target 621
  ]
  edge [
    source 71
    target 622
  ]
  edge [
    source 71
    target 623
  ]
  edge [
    source 71
    target 624
  ]
  edge [
    source 71
    target 625
  ]
  edge [
    source 71
    target 626
  ]
  edge [
    source 71
    target 101
  ]
  edge [
    source 71
    target 107
  ]
  edge [
    source 72
    target 73
  ]
  edge [
    source 72
    target 627
  ]
  edge [
    source 72
    target 628
  ]
  edge [
    source 72
    target 629
  ]
  edge [
    source 72
    target 630
  ]
  edge [
    source 72
    target 631
  ]
  edge [
    source 72
    target 632
  ]
  edge [
    source 72
    target 633
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 73
    target 634
  ]
  edge [
    source 73
    target 635
  ]
  edge [
    source 73
    target 636
  ]
  edge [
    source 73
    target 637
  ]
  edge [
    source 74
    target 638
  ]
  edge [
    source 74
    target 639
  ]
  edge [
    source 74
    target 640
  ]
  edge [
    source 74
    target 641
  ]
  edge [
    source 74
    target 642
  ]
  edge [
    source 74
    target 643
  ]
  edge [
    source 74
    target 644
  ]
  edge [
    source 74
    target 645
  ]
  edge [
    source 75
    target 76
  ]
  edge [
    source 75
    target 646
  ]
  edge [
    source 75
    target 647
  ]
  edge [
    source 75
    target 648
  ]
  edge [
    source 75
    target 649
  ]
  edge [
    source 76
    target 79
  ]
  edge [
    source 76
    target 80
  ]
  edge [
    source 76
    target 134
  ]
  edge [
    source 76
    target 650
  ]
  edge [
    source 76
    target 651
  ]
  edge [
    source 76
    target 652
  ]
  edge [
    source 76
    target 133
  ]
  edge [
    source 77
    target 78
  ]
  edge [
    source 77
    target 653
  ]
  edge [
    source 78
    target 654
  ]
  edge [
    source 78
    target 655
  ]
  edge [
    source 78
    target 656
  ]
  edge [
    source 78
    target 226
  ]
  edge [
    source 78
    target 657
  ]
  edge [
    source 79
    target 658
  ]
  edge [
    source 79
    target 659
  ]
  edge [
    source 80
    target 660
  ]
  edge [
    source 80
    target 661
  ]
  edge [
    source 80
    target 662
  ]
  edge [
    source 80
    target 663
  ]
  edge [
    source 80
    target 664
  ]
  edge [
    source 81
    target 82
  ]
  edge [
    source 82
    target 83
  ]
  edge [
    source 82
    target 576
  ]
  edge [
    source 82
    target 665
  ]
  edge [
    source 82
    target 666
  ]
  edge [
    source 82
    target 667
  ]
  edge [
    source 82
    target 231
  ]
  edge [
    source 82
    target 668
  ]
  edge [
    source 82
    target 669
  ]
  edge [
    source 82
    target 670
  ]
  edge [
    source 82
    target 671
  ]
  edge [
    source 82
    target 95
  ]
  edge [
    source 83
    target 84
  ]
  edge [
    source 83
    target 672
  ]
  edge [
    source 83
    target 673
  ]
  edge [
    source 83
    target 674
  ]
  edge [
    source 83
    target 675
  ]
  edge [
    source 83
    target 676
  ]
  edge [
    source 83
    target 677
  ]
  edge [
    source 83
    target 678
  ]
  edge [
    source 83
    target 679
  ]
  edge [
    source 83
    target 680
  ]
  edge [
    source 83
    target 681
  ]
  edge [
    source 83
    target 682
  ]
  edge [
    source 83
    target 683
  ]
  edge [
    source 83
    target 684
  ]
  edge [
    source 83
    target 685
  ]
  edge [
    source 83
    target 334
  ]
  edge [
    source 83
    target 686
  ]
  edge [
    source 83
    target 687
  ]
  edge [
    source 83
    target 688
  ]
  edge [
    source 83
    target 689
  ]
  edge [
    source 83
    target 690
  ]
  edge [
    source 83
    target 691
  ]
  edge [
    source 83
    target 692
  ]
  edge [
    source 84
    target 85
  ]
  edge [
    source 84
    target 693
  ]
  edge [
    source 84
    target 694
  ]
  edge [
    source 84
    target 695
  ]
  edge [
    source 84
    target 696
  ]
  edge [
    source 84
    target 697
  ]
  edge [
    source 84
    target 613
  ]
  edge [
    source 84
    target 614
  ]
  edge [
    source 84
    target 698
  ]
  edge [
    source 84
    target 616
  ]
  edge [
    source 84
    target 699
  ]
  edge [
    source 84
    target 700
  ]
  edge [
    source 84
    target 320
  ]
  edge [
    source 84
    target 701
  ]
  edge [
    source 84
    target 702
  ]
  edge [
    source 84
    target 703
  ]
  edge [
    source 85
    target 86
  ]
  edge [
    source 86
    target 87
  ]
  edge [
    source 86
    target 704
  ]
  edge [
    source 86
    target 705
  ]
  edge [
    source 86
    target 706
  ]
  edge [
    source 87
    target 88
  ]
  edge [
    source 87
    target 707
  ]
  edge [
    source 87
    target 491
  ]
  edge [
    source 87
    target 708
  ]
  edge [
    source 87
    target 709
  ]
  edge [
    source 87
    target 710
  ]
  edge [
    source 87
    target 711
  ]
  edge [
    source 87
    target 99
  ]
  edge [
    source 88
    target 89
  ]
  edge [
    source 88
    target 101
  ]
  edge [
    source 88
    target 712
  ]
  edge [
    source 88
    target 713
  ]
  edge [
    source 88
    target 714
  ]
  edge [
    source 88
    target 715
  ]
  edge [
    source 88
    target 716
  ]
  edge [
    source 89
    target 90
  ]
  edge [
    source 89
    target 717
  ]
  edge [
    source 89
    target 718
  ]
  edge [
    source 89
    target 719
  ]
  edge [
    source 89
    target 720
  ]
  edge [
    source 89
    target 721
  ]
  edge [
    source 89
    target 180
  ]
  edge [
    source 89
    target 722
  ]
  edge [
    source 89
    target 723
  ]
  edge [
    source 89
    target 724
  ]
  edge [
    source 89
    target 725
  ]
  edge [
    source 89
    target 726
  ]
  edge [
    source 89
    target 727
  ]
  edge [
    source 89
    target 728
  ]
  edge [
    source 89
    target 729
  ]
  edge [
    source 89
    target 730
  ]
  edge [
    source 89
    target 731
  ]
  edge [
    source 89
    target 732
  ]
  edge [
    source 89
    target 733
  ]
  edge [
    source 89
    target 101
  ]
  edge [
    source 90
    target 734
  ]
  edge [
    source 90
    target 735
  ]
  edge [
    source 90
    target 736
  ]
  edge [
    source 90
    target 737
  ]
  edge [
    source 90
    target 738
  ]
  edge [
    source 90
    target 739
  ]
  edge [
    source 90
    target 740
  ]
  edge [
    source 90
    target 741
  ]
  edge [
    source 90
    target 742
  ]
  edge [
    source 90
    target 743
  ]
  edge [
    source 90
    target 744
  ]
  edge [
    source 91
    target 92
  ]
  edge [
    source 91
    target 745
  ]
  edge [
    source 91
    target 746
  ]
  edge [
    source 91
    target 747
  ]
  edge [
    source 91
    target 682
  ]
  edge [
    source 91
    target 748
  ]
  edge [
    source 92
    target 93
  ]
  edge [
    source 92
    target 749
  ]
  edge [
    source 92
    target 750
  ]
  edge [
    source 92
    target 751
  ]
  edge [
    source 93
    target 94
  ]
  edge [
    source 94
    target 95
  ]
  edge [
    source 94
    target 752
  ]
  edge [
    source 94
    target 126
  ]
  edge [
    source 95
    target 96
  ]
  edge [
    source 95
    target 753
  ]
  edge [
    source 95
    target 754
  ]
  edge [
    source 95
    target 755
  ]
  edge [
    source 96
    target 97
  ]
  edge [
    source 97
    target 98
  ]
  edge [
    source 97
    target 756
  ]
  edge [
    source 97
    target 757
  ]
  edge [
    source 97
    target 758
  ]
  edge [
    source 97
    target 126
  ]
  edge [
    source 97
    target 759
  ]
  edge [
    source 98
    target 760
  ]
  edge [
    source 98
    target 104
  ]
  edge [
    source 99
    target 100
  ]
  edge [
    source 99
    target 761
  ]
  edge [
    source 99
    target 762
  ]
  edge [
    source 99
    target 763
  ]
  edge [
    source 99
    target 764
  ]
  edge [
    source 99
    target 765
  ]
  edge [
    source 99
    target 766
  ]
  edge [
    source 99
    target 767
  ]
  edge [
    source 99
    target 768
  ]
  edge [
    source 99
    target 769
  ]
  edge [
    source 99
    target 770
  ]
  edge [
    source 99
    target 771
  ]
  edge [
    source 100
    target 772
  ]
  edge [
    source 100
    target 773
  ]
  edge [
    source 100
    target 774
  ]
  edge [
    source 101
    target 102
  ]
  edge [
    source 101
    target 775
  ]
  edge [
    source 101
    target 776
  ]
  edge [
    source 101
    target 777
  ]
  edge [
    source 101
    target 778
  ]
  edge [
    source 101
    target 436
  ]
  edge [
    source 101
    target 779
  ]
  edge [
    source 101
    target 780
  ]
  edge [
    source 101
    target 781
  ]
  edge [
    source 101
    target 782
  ]
  edge [
    source 101
    target 783
  ]
  edge [
    source 101
    target 784
  ]
  edge [
    source 101
    target 785
  ]
  edge [
    source 101
    target 107
  ]
  edge [
    source 102
    target 105
  ]
  edge [
    source 102
    target 106
  ]
  edge [
    source 102
    target 786
  ]
  edge [
    source 102
    target 787
  ]
  edge [
    source 102
    target 788
  ]
  edge [
    source 102
    target 678
  ]
  edge [
    source 102
    target 789
  ]
  edge [
    source 102
    target 790
  ]
  edge [
    source 102
    target 791
  ]
  edge [
    source 102
    target 792
  ]
  edge [
    source 102
    target 793
  ]
  edge [
    source 102
    target 794
  ]
  edge [
    source 102
    target 795
  ]
  edge [
    source 102
    target 796
  ]
  edge [
    source 102
    target 797
  ]
  edge [
    source 102
    target 798
  ]
  edge [
    source 102
    target 799
  ]
  edge [
    source 102
    target 800
  ]
  edge [
    source 102
    target 801
  ]
  edge [
    source 103
    target 104
  ]
  edge [
    source 103
    target 802
  ]
  edge [
    source 103
    target 803
  ]
  edge [
    source 103
    target 804
  ]
  edge [
    source 103
    target 805
  ]
  edge [
    source 103
    target 806
  ]
  edge [
    source 103
    target 807
  ]
  edge [
    source 104
    target 105
  ]
  edge [
    source 104
    target 808
  ]
  edge [
    source 104
    target 809
  ]
  edge [
    source 104
    target 810
  ]
  edge [
    source 104
    target 811
  ]
  edge [
    source 104
    target 812
  ]
  edge [
    source 104
    target 813
  ]
  edge [
    source 104
    target 814
  ]
  edge [
    source 104
    target 815
  ]
  edge [
    source 104
    target 816
  ]
  edge [
    source 104
    target 817
  ]
  edge [
    source 104
    target 818
  ]
  edge [
    source 104
    target 819
  ]
  edge [
    source 104
    target 317
  ]
  edge [
    source 104
    target 820
  ]
  edge [
    source 104
    target 821
  ]
  edge [
    source 104
    target 822
  ]
  edge [
    source 104
    target 823
  ]
  edge [
    source 104
    target 824
  ]
  edge [
    source 104
    target 825
  ]
  edge [
    source 104
    target 826
  ]
  edge [
    source 105
    target 827
  ]
  edge [
    source 105
    target 120
  ]
  edge [
    source 106
    target 107
  ]
  edge [
    source 106
    target 688
  ]
  edge [
    source 106
    target 828
  ]
  edge [
    source 106
    target 334
  ]
  edge [
    source 106
    target 829
  ]
  edge [
    source 106
    target 830
  ]
  edge [
    source 106
    target 831
  ]
  edge [
    source 107
    target 108
  ]
  edge [
    source 107
    target 832
  ]
  edge [
    source 107
    target 833
  ]
  edge [
    source 107
    target 834
  ]
  edge [
    source 107
    target 835
  ]
  edge [
    source 107
    target 614
  ]
  edge [
    source 107
    target 836
  ]
  edge [
    source 107
    target 837
  ]
  edge [
    source 107
    target 750
  ]
  edge [
    source 107
    target 751
  ]
  edge [
    source 107
    target 838
  ]
  edge [
    source 107
    target 839
  ]
  edge [
    source 107
    target 840
  ]
  edge [
    source 108
    target 109
  ]
  edge [
    source 108
    target 841
  ]
  edge [
    source 108
    target 842
  ]
  edge [
    source 108
    target 843
  ]
  edge [
    source 108
    target 844
  ]
  edge [
    source 108
    target 845
  ]
  edge [
    source 108
    target 846
  ]
  edge [
    source 108
    target 847
  ]
  edge [
    source 108
    target 848
  ]
  edge [
    source 108
    target 849
  ]
  edge [
    source 108
    target 850
  ]
  edge [
    source 108
    target 851
  ]
  edge [
    source 109
    target 110
  ]
  edge [
    source 109
    target 339
  ]
  edge [
    source 109
    target 852
  ]
  edge [
    source 109
    target 468
  ]
  edge [
    source 109
    target 853
  ]
  edge [
    source 109
    target 854
  ]
  edge [
    source 109
    target 855
  ]
  edge [
    source 109
    target 856
  ]
  edge [
    source 109
    target 857
  ]
  edge [
    source 109
    target 858
  ]
  edge [
    source 109
    target 859
  ]
  edge [
    source 109
    target 860
  ]
  edge [
    source 109
    target 861
  ]
  edge [
    source 109
    target 862
  ]
  edge [
    source 109
    target 863
  ]
  edge [
    source 109
    target 864
  ]
  edge [
    source 109
    target 865
  ]
  edge [
    source 109
    target 866
  ]
  edge [
    source 109
    target 867
  ]
  edge [
    source 109
    target 868
  ]
  edge [
    source 110
    target 111
  ]
  edge [
    source 111
    target 112
  ]
  edge [
    source 112
    target 113
  ]
  edge [
    source 113
    target 869
  ]
  edge [
    source 113
    target 870
  ]
  edge [
    source 113
    target 871
  ]
  edge [
    source 113
    target 872
  ]
  edge [
    source 113
    target 873
  ]
  edge [
    source 113
    target 874
  ]
  edge [
    source 113
    target 875
  ]
  edge [
    source 113
    target 876
  ]
  edge [
    source 114
    target 115
  ]
  edge [
    source 114
    target 877
  ]
  edge [
    source 114
    target 878
  ]
  edge [
    source 114
    target 759
  ]
  edge [
    source 114
    target 879
  ]
  edge [
    source 114
    target 880
  ]
  edge [
    source 114
    target 881
  ]
  edge [
    source 114
    target 882
  ]
  edge [
    source 114
    target 883
  ]
  edge [
    source 114
    target 884
  ]
  edge [
    source 114
    target 885
  ]
  edge [
    source 114
    target 886
  ]
  edge [
    source 114
    target 249
  ]
  edge [
    source 114
    target 887
  ]
  edge [
    source 114
    target 888
  ]
  edge [
    source 115
    target 889
  ]
  edge [
    source 115
    target 890
  ]
  edge [
    source 115
    target 891
  ]
  edge [
    source 115
    target 892
  ]
  edge [
    source 116
    target 117
  ]
  edge [
    source 117
    target 893
  ]
  edge [
    source 117
    target 137
  ]
  edge [
    source 117
    target 894
  ]
  edge [
    source 117
    target 895
  ]
  edge [
    source 117
    target 896
  ]
  edge [
    source 117
    target 897
  ]
  edge [
    source 117
    target 898
  ]
  edge [
    source 117
    target 899
  ]
  edge [
    source 118
    target 119
  ]
  edge [
    source 118
    target 900
  ]
  edge [
    source 119
    target 120
  ]
  edge [
    source 119
    target 901
  ]
  edge [
    source 119
    target 902
  ]
  edge [
    source 119
    target 903
  ]
  edge [
    source 119
    target 904
  ]
  edge [
    source 120
    target 121
  ]
  edge [
    source 120
    target 302
  ]
  edge [
    source 120
    target 905
  ]
  edge [
    source 120
    target 906
  ]
  edge [
    source 120
    target 907
  ]
  edge [
    source 120
    target 908
  ]
  edge [
    source 120
    target 909
  ]
  edge [
    source 121
    target 122
  ]
  edge [
    source 121
    target 910
  ]
  edge [
    source 121
    target 911
  ]
  edge [
    source 121
    target 912
  ]
  edge [
    source 121
    target 913
  ]
  edge [
    source 122
    target 123
  ]
  edge [
    source 122
    target 914
  ]
  edge [
    source 122
    target 791
  ]
  edge [
    source 122
    target 915
  ]
  edge [
    source 122
    target 916
  ]
  edge [
    source 122
    target 917
  ]
  edge [
    source 122
    target 918
  ]
  edge [
    source 123
    target 919
  ]
  edge [
    source 123
    target 920
  ]
  edge [
    source 123
    target 921
  ]
  edge [
    source 123
    target 922
  ]
  edge [
    source 123
    target 923
  ]
  edge [
    source 123
    target 924
  ]
  edge [
    source 123
    target 925
  ]
  edge [
    source 123
    target 926
  ]
]
