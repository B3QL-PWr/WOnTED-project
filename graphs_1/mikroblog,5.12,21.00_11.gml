graph [
  maxDegree 70
  minDegree 1
  meanDegree 2
  density 0.012048192771084338
  graphCliqueNumber 3
  node [
    id 0
    label "mia&#322;"
    origin "text"
  ]
  node [
    id 1
    label "taki"
    origin "text"
  ]
  node [
    id 2
    label "okres"
    origin "text"
  ]
  node [
    id 3
    label "&#380;ycie"
    origin "text"
  ]
  node [
    id 4
    label "dzwoni&#263;by&#263;"
    origin "text"
  ]
  node [
    id 5
    label "pizza"
    origin "text"
  ]
  node [
    id 6
    label "s&#322;owo"
    origin "text"
  ]
  node [
    id 7
    label "kornixpl"
    origin "text"
  ]
  node [
    id 8
    label "numer"
    origin "text"
  ]
  node [
    id 9
    label "czy"
    origin "text"
  ]
  node [
    id 10
    label "co&#347;"
    origin "text"
  ]
  node [
    id 11
    label "inny"
    origin "text"
  ]
  node [
    id 12
    label "proszek"
  ]
  node [
    id 13
    label "okre&#347;lony"
  ]
  node [
    id 14
    label "jaki&#347;"
  ]
  node [
    id 15
    label "paleogen"
  ]
  node [
    id 16
    label "spell"
  ]
  node [
    id 17
    label "czas"
  ]
  node [
    id 18
    label "period"
  ]
  node [
    id 19
    label "prekambr"
  ]
  node [
    id 20
    label "jura"
  ]
  node [
    id 21
    label "interstadia&#322;"
  ]
  node [
    id 22
    label "jednostka_geologiczna"
  ]
  node [
    id 23
    label "izochronizm"
  ]
  node [
    id 24
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 25
    label "okres_noachijski"
  ]
  node [
    id 26
    label "orosir"
  ]
  node [
    id 27
    label "kreda"
  ]
  node [
    id 28
    label "sten"
  ]
  node [
    id 29
    label "drugorz&#281;d"
  ]
  node [
    id 30
    label "semester"
  ]
  node [
    id 31
    label "trzeciorz&#281;d"
  ]
  node [
    id 32
    label "ton"
  ]
  node [
    id 33
    label "dzieje"
  ]
  node [
    id 34
    label "poprzednik"
  ]
  node [
    id 35
    label "ordowik"
  ]
  node [
    id 36
    label "karbon"
  ]
  node [
    id 37
    label "trias"
  ]
  node [
    id 38
    label "kalim"
  ]
  node [
    id 39
    label "stater"
  ]
  node [
    id 40
    label "era"
  ]
  node [
    id 41
    label "cykl"
  ]
  node [
    id 42
    label "p&#243;&#322;okres"
  ]
  node [
    id 43
    label "czwartorz&#281;d"
  ]
  node [
    id 44
    label "pulsacja"
  ]
  node [
    id 45
    label "okres_amazo&#324;ski"
  ]
  node [
    id 46
    label "kambr"
  ]
  node [
    id 47
    label "Zeitgeist"
  ]
  node [
    id 48
    label "nast&#281;pnik"
  ]
  node [
    id 49
    label "kriogen"
  ]
  node [
    id 50
    label "glacja&#322;"
  ]
  node [
    id 51
    label "fala"
  ]
  node [
    id 52
    label "okres_czasu"
  ]
  node [
    id 53
    label "riak"
  ]
  node [
    id 54
    label "schy&#322;ek"
  ]
  node [
    id 55
    label "okres_hesperyjski"
  ]
  node [
    id 56
    label "sylur"
  ]
  node [
    id 57
    label "dewon"
  ]
  node [
    id 58
    label "ciota"
  ]
  node [
    id 59
    label "epoka"
  ]
  node [
    id 60
    label "pierwszorz&#281;d"
  ]
  node [
    id 61
    label "okres_halsztacki"
  ]
  node [
    id 62
    label "ektas"
  ]
  node [
    id 63
    label "zdanie"
  ]
  node [
    id 64
    label "condition"
  ]
  node [
    id 65
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 66
    label "rok_akademicki"
  ]
  node [
    id 67
    label "wapie&#324;_muszlowy"
  ]
  node [
    id 68
    label "postglacja&#322;"
  ]
  node [
    id 69
    label "faza"
  ]
  node [
    id 70
    label "proces_fizjologiczny"
  ]
  node [
    id 71
    label "ediakar"
  ]
  node [
    id 72
    label "time_period"
  ]
  node [
    id 73
    label "ruch_drgaj&#261;cy"
  ]
  node [
    id 74
    label "perm"
  ]
  node [
    id 75
    label "rok_szkolny"
  ]
  node [
    id 76
    label "neogen"
  ]
  node [
    id 77
    label "sider"
  ]
  node [
    id 78
    label "flow"
  ]
  node [
    id 79
    label "podokres"
  ]
  node [
    id 80
    label "preglacja&#322;"
  ]
  node [
    id 81
    label "retoryka"
  ]
  node [
    id 82
    label "choroba_przyrodzona"
  ]
  node [
    id 83
    label "energy"
  ]
  node [
    id 84
    label "bycie"
  ]
  node [
    id 85
    label "zegar_biologiczny"
  ]
  node [
    id 86
    label "okres_noworodkowy"
  ]
  node [
    id 87
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 88
    label "entity"
  ]
  node [
    id 89
    label "prze&#380;ywanie"
  ]
  node [
    id 90
    label "prze&#380;ycie"
  ]
  node [
    id 91
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 92
    label "wiek_matuzalemowy"
  ]
  node [
    id 93
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 94
    label "dzieci&#324;stwo"
  ]
  node [
    id 95
    label "power"
  ]
  node [
    id 96
    label "szwung"
  ]
  node [
    id 97
    label "menopauza"
  ]
  node [
    id 98
    label "umarcie"
  ]
  node [
    id 99
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 100
    label "life"
  ]
  node [
    id 101
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 102
    label "&#380;ywy"
  ]
  node [
    id 103
    label "rozw&#243;j"
  ]
  node [
    id 104
    label "po&#322;&#243;g"
  ]
  node [
    id 105
    label "byt"
  ]
  node [
    id 106
    label "przebywanie"
  ]
  node [
    id 107
    label "subsistence"
  ]
  node [
    id 108
    label "koleje_losu"
  ]
  node [
    id 109
    label "raj_utracony"
  ]
  node [
    id 110
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 111
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 112
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 113
    label "andropauza"
  ]
  node [
    id 114
    label "warunki"
  ]
  node [
    id 115
    label "do&#380;ywanie"
  ]
  node [
    id 116
    label "niemowl&#281;ctwo"
  ]
  node [
    id 117
    label "umieranie"
  ]
  node [
    id 118
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 119
    label "staro&#347;&#263;"
  ]
  node [
    id 120
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 121
    label "&#347;mier&#263;"
  ]
  node [
    id 122
    label "wypiek"
  ]
  node [
    id 123
    label "fast_food"
  ]
  node [
    id 124
    label "sp&#243;d"
  ]
  node [
    id 125
    label "obietnica"
  ]
  node [
    id 126
    label "bit"
  ]
  node [
    id 127
    label "s&#322;ownictwo"
  ]
  node [
    id 128
    label "jednostka_leksykalna"
  ]
  node [
    id 129
    label "pisanie_si&#281;"
  ]
  node [
    id 130
    label "wykrzyknik"
  ]
  node [
    id 131
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 132
    label "pole_semantyczne"
  ]
  node [
    id 133
    label "&#347;r&#243;dg&#322;os"
  ]
  node [
    id 134
    label "komunikat"
  ]
  node [
    id 135
    label "pisa&#263;_si&#281;"
  ]
  node [
    id 136
    label "wypowiedzenie"
  ]
  node [
    id 137
    label "nag&#322;os"
  ]
  node [
    id 138
    label "wordnet"
  ]
  node [
    id 139
    label "morfem"
  ]
  node [
    id 140
    label "czasownik"
  ]
  node [
    id 141
    label "wyg&#322;os"
  ]
  node [
    id 142
    label "jednostka_informacji"
  ]
  node [
    id 143
    label "manewr"
  ]
  node [
    id 144
    label "sztos"
  ]
  node [
    id 145
    label "pok&#243;j"
  ]
  node [
    id 146
    label "facet"
  ]
  node [
    id 147
    label "wyst&#281;p"
  ]
  node [
    id 148
    label "turn"
  ]
  node [
    id 149
    label "impression"
  ]
  node [
    id 150
    label "hotel"
  ]
  node [
    id 151
    label "liczba"
  ]
  node [
    id 152
    label "punkt"
  ]
  node [
    id 153
    label "czasopismo"
  ]
  node [
    id 154
    label "&#380;art"
  ]
  node [
    id 155
    label "orygina&#322;"
  ]
  node [
    id 156
    label "oznaczenie"
  ]
  node [
    id 157
    label "zi&#243;&#322;ko"
  ]
  node [
    id 158
    label "akt_p&#322;ciowy"
  ]
  node [
    id 159
    label "publikacja"
  ]
  node [
    id 160
    label "thing"
  ]
  node [
    id 161
    label "cosik"
  ]
  node [
    id 162
    label "kolejny"
  ]
  node [
    id 163
    label "inaczej"
  ]
  node [
    id 164
    label "r&#243;&#380;ny"
  ]
  node [
    id 165
    label "inszy"
  ]
  node [
    id 166
    label "osobno"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 143
  ]
  edge [
    source 8
    target 144
  ]
  edge [
    source 8
    target 145
  ]
  edge [
    source 8
    target 146
  ]
  edge [
    source 8
    target 147
  ]
  edge [
    source 8
    target 148
  ]
  edge [
    source 8
    target 149
  ]
  edge [
    source 8
    target 150
  ]
  edge [
    source 8
    target 151
  ]
  edge [
    source 8
    target 152
  ]
  edge [
    source 8
    target 153
  ]
  edge [
    source 8
    target 154
  ]
  edge [
    source 8
    target 155
  ]
  edge [
    source 8
    target 156
  ]
  edge [
    source 8
    target 157
  ]
  edge [
    source 8
    target 158
  ]
  edge [
    source 8
    target 159
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 160
  ]
  edge [
    source 10
    target 161
  ]
  edge [
    source 11
    target 162
  ]
  edge [
    source 11
    target 163
  ]
  edge [
    source 11
    target 164
  ]
  edge [
    source 11
    target 165
  ]
  edge [
    source 11
    target 166
  ]
]
