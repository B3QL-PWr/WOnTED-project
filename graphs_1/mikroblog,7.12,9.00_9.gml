graph [
  maxDegree 13
  minDegree 1
  meanDegree 1.95
  density 0.05
  graphCliqueNumber 2
  node [
    id 0
    label "zaproszenie"
    origin "text"
  ]
  node [
    id 1
    label "kilka"
    origin "text"
  ]
  node [
    id 2
    label "lasek"
    origin "text"
  ]
  node [
    id 3
    label "tindera"
    origin "text"
  ]
  node [
    id 4
    label "impreze"
    origin "text"
  ]
  node [
    id 5
    label "ktorej"
    origin "text"
  ]
  node [
    id 6
    label "gram"
    origin "text"
  ]
  node [
    id 7
    label "set"
    origin "text"
  ]
  node [
    id 8
    label "by&#263;"
    origin "text"
  ]
  node [
    id 9
    label "dobre"
    origin "text"
  ]
  node [
    id 10
    label "pomyslem"
    origin "text"
  ]
  node [
    id 11
    label "zaproponowanie"
  ]
  node [
    id 12
    label "propozycja"
  ]
  node [
    id 13
    label "pro&#347;ba"
  ]
  node [
    id 14
    label "invitation"
  ]
  node [
    id 15
    label "druk"
  ]
  node [
    id 16
    label "karteczka"
  ]
  node [
    id 17
    label "&#347;ledziowate"
  ]
  node [
    id 18
    label "ryba"
  ]
  node [
    id 19
    label "dekagram"
  ]
  node [
    id 20
    label "megagram"
  ]
  node [
    id 21
    label "centygram"
  ]
  node [
    id 22
    label "miligram"
  ]
  node [
    id 23
    label "metryczna_jednostka_masy"
  ]
  node [
    id 24
    label "zestaw"
  ]
  node [
    id 25
    label "kompozycja"
  ]
  node [
    id 26
    label "gem"
  ]
  node [
    id 27
    label "muzyka"
  ]
  node [
    id 28
    label "runda"
  ]
  node [
    id 29
    label "si&#281;ga&#263;"
  ]
  node [
    id 30
    label "trwa&#263;"
  ]
  node [
    id 31
    label "obecno&#347;&#263;"
  ]
  node [
    id 32
    label "stan"
  ]
  node [
    id 33
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 34
    label "stand"
  ]
  node [
    id 35
    label "mie&#263;_miejsce"
  ]
  node [
    id 36
    label "uczestniczy&#263;"
  ]
  node [
    id 37
    label "chodzi&#263;"
  ]
  node [
    id 38
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 39
    label "equal"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 19
  ]
  edge [
    source 6
    target 20
  ]
  edge [
    source 6
    target 21
  ]
  edge [
    source 6
    target 22
  ]
  edge [
    source 6
    target 23
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 24
  ]
  edge [
    source 7
    target 25
  ]
  edge [
    source 7
    target 26
  ]
  edge [
    source 7
    target 27
  ]
  edge [
    source 7
    target 28
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 29
  ]
  edge [
    source 8
    target 30
  ]
  edge [
    source 8
    target 31
  ]
  edge [
    source 8
    target 32
  ]
  edge [
    source 8
    target 33
  ]
  edge [
    source 8
    target 34
  ]
  edge [
    source 8
    target 35
  ]
  edge [
    source 8
    target 36
  ]
  edge [
    source 8
    target 37
  ]
  edge [
    source 8
    target 38
  ]
  edge [
    source 8
    target 39
  ]
  edge [
    source 9
    target 10
  ]
]
