graph [
  maxDegree 12
  minDegree 1
  meanDegree 1.8461538461538463
  density 0.15384615384615385
  graphCliqueNumber 2
  node [
    id 0
    label "by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "miko&#322;aj"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;ga&#263;"
  ]
  node [
    id 3
    label "trwa&#263;"
  ]
  node [
    id 4
    label "obecno&#347;&#263;"
  ]
  node [
    id 5
    label "stan"
  ]
  node [
    id 6
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 7
    label "stand"
  ]
  node [
    id 8
    label "mie&#263;_miejsce"
  ]
  node [
    id 9
    label "uczestniczy&#263;"
  ]
  node [
    id 10
    label "chodzi&#263;"
  ]
  node [
    id 11
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 12
    label "equal"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
]
