graph [
  maxDegree 38
  minDegree 1
  meanDegree 2.1169590643274856
  density 0.012452700378396972
  graphCliqueNumber 5
  node [
    id 0
    label "stwierdza&#263;"
    origin "text"
  ]
  node [
    id 1
    label "sejm"
    origin "text"
  ]
  node [
    id 2
    label "wys&#322;ucha&#263;"
    origin "text"
  ]
  node [
    id 3
    label "sprawozdanie"
    origin "text"
  ]
  node [
    id 4
    label "dzia&#322;alno&#347;&#263;"
    origin "text"
  ]
  node [
    id 5
    label "narodowy"
    origin "text"
  ]
  node [
    id 6
    label "bank"
    origin "text"
  ]
  node [
    id 7
    label "polski"
    origin "text"
  ]
  node [
    id 8
    label "rok"
    origin "text"
  ]
  node [
    id 9
    label "przedstawi&#263;"
    origin "text"
  ]
  node [
    id 10
    label "przez"
    origin "text"
  ]
  node [
    id 11
    label "pan"
    origin "text"
  ]
  node [
    id 12
    label "prezes"
    origin "text"
  ]
  node [
    id 13
    label "s&#322;awomir"
    origin "text"
  ]
  node [
    id 14
    label "skrzypek"
    origin "text"
  ]
  node [
    id 15
    label "przeprowadzi&#263;"
    origin "text"
  ]
  node [
    id 16
    label "dyskusja"
    origin "text"
  ]
  node [
    id 17
    label "uznawa&#263;"
  ]
  node [
    id 18
    label "oznajmia&#263;"
  ]
  node [
    id 19
    label "attest"
  ]
  node [
    id 20
    label "parlament"
  ]
  node [
    id 21
    label "obrady"
  ]
  node [
    id 22
    label "grupa"
  ]
  node [
    id 23
    label "lewica"
  ]
  node [
    id 24
    label "zgromadzenie"
  ]
  node [
    id 25
    label "prawica"
  ]
  node [
    id 26
    label "centrum"
  ]
  node [
    id 27
    label "izba_ni&#380;sza"
  ]
  node [
    id 28
    label "siedziba"
  ]
  node [
    id 29
    label "parliament"
  ]
  node [
    id 30
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 31
    label "muzyka"
  ]
  node [
    id 32
    label "nagranie"
  ]
  node [
    id 33
    label "spe&#322;ni&#263;"
  ]
  node [
    id 34
    label "message"
  ]
  node [
    id 35
    label "korespondent"
  ]
  node [
    id 36
    label "wypowied&#378;"
  ]
  node [
    id 37
    label "sprawko"
  ]
  node [
    id 38
    label "dzia&#322;anie"
  ]
  node [
    id 39
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 40
    label "absolutorium"
  ]
  node [
    id 41
    label "activity"
  ]
  node [
    id 42
    label "nacjonalistyczny"
  ]
  node [
    id 43
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 44
    label "narodowo"
  ]
  node [
    id 45
    label "wa&#380;ny"
  ]
  node [
    id 46
    label "wk&#322;adca"
  ]
  node [
    id 47
    label "agencja"
  ]
  node [
    id 48
    label "konto"
  ]
  node [
    id 49
    label "agent_rozliczeniowy"
  ]
  node [
    id 50
    label "eurorynek"
  ]
  node [
    id 51
    label "zbi&#243;r"
  ]
  node [
    id 52
    label "instytucja"
  ]
  node [
    id 53
    label "kwota"
  ]
  node [
    id 54
    label "lacki"
  ]
  node [
    id 55
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 56
    label "przedmiot"
  ]
  node [
    id 57
    label "sztajer"
  ]
  node [
    id 58
    label "drabant"
  ]
  node [
    id 59
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 60
    label "polak"
  ]
  node [
    id 61
    label "pierogi_ruskie"
  ]
  node [
    id 62
    label "krakowiak"
  ]
  node [
    id 63
    label "Polish"
  ]
  node [
    id 64
    label "j&#281;zyk"
  ]
  node [
    id 65
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 66
    label "oberek"
  ]
  node [
    id 67
    label "po_polsku"
  ]
  node [
    id 68
    label "mazur"
  ]
  node [
    id 69
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 70
    label "chodzony"
  ]
  node [
    id 71
    label "skoczny"
  ]
  node [
    id 72
    label "ryba_po_grecku"
  ]
  node [
    id 73
    label "goniony"
  ]
  node [
    id 74
    label "polsko"
  ]
  node [
    id 75
    label "stulecie"
  ]
  node [
    id 76
    label "kalendarz"
  ]
  node [
    id 77
    label "czas"
  ]
  node [
    id 78
    label "pora_roku"
  ]
  node [
    id 79
    label "cykl_astronomiczny"
  ]
  node [
    id 80
    label "p&#243;&#322;rocze"
  ]
  node [
    id 81
    label "kwarta&#322;"
  ]
  node [
    id 82
    label "kurs"
  ]
  node [
    id 83
    label "jubileusz"
  ]
  node [
    id 84
    label "miesi&#261;c"
  ]
  node [
    id 85
    label "lata"
  ]
  node [
    id 86
    label "martwy_sezon"
  ]
  node [
    id 87
    label "przedstawienie"
  ]
  node [
    id 88
    label "express"
  ]
  node [
    id 89
    label "typify"
  ]
  node [
    id 90
    label "ukaza&#263;"
  ]
  node [
    id 91
    label "opisa&#263;"
  ]
  node [
    id 92
    label "pokaza&#263;"
  ]
  node [
    id 93
    label "represent"
  ]
  node [
    id 94
    label "zapozna&#263;"
  ]
  node [
    id 95
    label "zaproponowa&#263;"
  ]
  node [
    id 96
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 97
    label "zademonstrowa&#263;"
  ]
  node [
    id 98
    label "poda&#263;"
  ]
  node [
    id 99
    label "cz&#322;owiek"
  ]
  node [
    id 100
    label "profesor"
  ]
  node [
    id 101
    label "kszta&#322;ciciel"
  ]
  node [
    id 102
    label "jegomo&#347;&#263;"
  ]
  node [
    id 103
    label "zwrot"
  ]
  node [
    id 104
    label "pracodawca"
  ]
  node [
    id 105
    label "rz&#261;dzenie"
  ]
  node [
    id 106
    label "m&#261;&#380;"
  ]
  node [
    id 107
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 108
    label "ch&#322;opina"
  ]
  node [
    id 109
    label "bratek"
  ]
  node [
    id 110
    label "opiekun"
  ]
  node [
    id 111
    label "doros&#322;y"
  ]
  node [
    id 112
    label "preceptor"
  ]
  node [
    id 113
    label "Midas"
  ]
  node [
    id 114
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 115
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 116
    label "murza"
  ]
  node [
    id 117
    label "ojciec"
  ]
  node [
    id 118
    label "androlog"
  ]
  node [
    id 119
    label "pupil"
  ]
  node [
    id 120
    label "efendi"
  ]
  node [
    id 121
    label "nabab"
  ]
  node [
    id 122
    label "w&#322;odarz"
  ]
  node [
    id 123
    label "szkolnik"
  ]
  node [
    id 124
    label "pedagog"
  ]
  node [
    id 125
    label "popularyzator"
  ]
  node [
    id 126
    label "andropauza"
  ]
  node [
    id 127
    label "gra_w_karty"
  ]
  node [
    id 128
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 129
    label "Mieszko_I"
  ]
  node [
    id 130
    label "bogaty"
  ]
  node [
    id 131
    label "samiec"
  ]
  node [
    id 132
    label "przyw&#243;dca"
  ]
  node [
    id 133
    label "pa&#324;stwo"
  ]
  node [
    id 134
    label "belfer"
  ]
  node [
    id 135
    label "gruba_ryba"
  ]
  node [
    id 136
    label "zwierzchnik"
  ]
  node [
    id 137
    label "instrumentalista"
  ]
  node [
    id 138
    label "pom&#243;c"
  ]
  node [
    id 139
    label "zbudowa&#263;"
  ]
  node [
    id 140
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 141
    label "leave"
  ]
  node [
    id 142
    label "przewie&#347;&#263;"
  ]
  node [
    id 143
    label "wykona&#263;"
  ]
  node [
    id 144
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 145
    label "draw"
  ]
  node [
    id 146
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 147
    label "carry"
  ]
  node [
    id 148
    label "rozmowa"
  ]
  node [
    id 149
    label "sympozjon"
  ]
  node [
    id 150
    label "conference"
  ]
  node [
    id 151
    label "polskie"
  ]
  node [
    id 152
    label "S&#322;awomira"
  ]
  node [
    id 153
    label "skrzypka"
  ]
  node [
    id 154
    label "gospodarstwo"
  ]
  node [
    id 155
    label "krajowy"
  ]
  node [
    id 156
    label "fundusz"
  ]
  node [
    id 157
    label "por&#281;czenie"
  ]
  node [
    id 158
    label "kredytowy"
  ]
  node [
    id 159
    label "skarb"
  ]
  node [
    id 160
    label "wysoki"
  ]
  node [
    id 161
    label "izba"
  ]
  node [
    id 162
    label "kontrola"
  ]
  node [
    id 163
    label "komisja"
  ]
  node [
    id 164
    label "do"
  ]
  node [
    id 165
    label "sprawi&#263;"
  ]
  node [
    id 166
    label "pa&#324;stwowy"
  ]
  node [
    id 167
    label "Jack"
  ]
  node [
    id 168
    label "jezierski"
  ]
  node [
    id 169
    label "Jacek"
  ]
  node [
    id 170
    label "rzeczpospolita"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 151
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 170
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 22
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 11
    target 100
  ]
  edge [
    source 11
    target 101
  ]
  edge [
    source 11
    target 102
  ]
  edge [
    source 11
    target 103
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 108
  ]
  edge [
    source 11
    target 109
  ]
  edge [
    source 11
    target 110
  ]
  edge [
    source 11
    target 111
  ]
  edge [
    source 11
    target 112
  ]
  edge [
    source 11
    target 113
  ]
  edge [
    source 11
    target 114
  ]
  edge [
    source 11
    target 115
  ]
  edge [
    source 11
    target 116
  ]
  edge [
    source 11
    target 117
  ]
  edge [
    source 11
    target 118
  ]
  edge [
    source 11
    target 119
  ]
  edge [
    source 11
    target 120
  ]
  edge [
    source 11
    target 121
  ]
  edge [
    source 11
    target 122
  ]
  edge [
    source 11
    target 123
  ]
  edge [
    source 11
    target 124
  ]
  edge [
    source 11
    target 125
  ]
  edge [
    source 11
    target 126
  ]
  edge [
    source 11
    target 127
  ]
  edge [
    source 11
    target 128
  ]
  edge [
    source 11
    target 129
  ]
  edge [
    source 11
    target 130
  ]
  edge [
    source 11
    target 131
  ]
  edge [
    source 11
    target 132
  ]
  edge [
    source 11
    target 133
  ]
  edge [
    source 11
    target 134
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 135
  ]
  edge [
    source 12
    target 136
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 137
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 138
  ]
  edge [
    source 15
    target 139
  ]
  edge [
    source 15
    target 140
  ]
  edge [
    source 15
    target 141
  ]
  edge [
    source 15
    target 142
  ]
  edge [
    source 15
    target 143
  ]
  edge [
    source 15
    target 144
  ]
  edge [
    source 15
    target 145
  ]
  edge [
    source 15
    target 146
  ]
  edge [
    source 15
    target 147
  ]
  edge [
    source 16
    target 148
  ]
  edge [
    source 16
    target 149
  ]
  edge [
    source 16
    target 150
  ]
  edge [
    source 133
    target 159
  ]
  edge [
    source 152
    target 153
  ]
  edge [
    source 154
    target 155
  ]
  edge [
    source 155
    target 156
  ]
  edge [
    source 155
    target 157
  ]
  edge [
    source 155
    target 158
  ]
  edge [
    source 156
    target 157
  ]
  edge [
    source 156
    target 158
  ]
  edge [
    source 157
    target 158
  ]
  edge [
    source 160
    target 161
  ]
  edge [
    source 160
    target 162
  ]
  edge [
    source 161
    target 162
  ]
  edge [
    source 162
    target 163
  ]
  edge [
    source 162
    target 164
  ]
  edge [
    source 162
    target 165
  ]
  edge [
    source 162
    target 166
  ]
  edge [
    source 163
    target 164
  ]
  edge [
    source 163
    target 165
  ]
  edge [
    source 163
    target 166
  ]
  edge [
    source 164
    target 165
  ]
  edge [
    source 164
    target 166
  ]
  edge [
    source 165
    target 166
  ]
  edge [
    source 167
    target 168
  ]
  edge [
    source 168
    target 169
  ]
]
