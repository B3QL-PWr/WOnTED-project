graph [
  maxDegree 24
  minDegree 1
  meanDegree 2.0606060606060606
  density 0.06439393939393939
  graphCliqueNumber 4
  node [
    id 0
    label "most"
    origin "text"
  ]
  node [
    id 1
    label "&#347;redzkie"
    origin "text"
  ]
  node [
    id 2
    label "jarzmo_mostowe"
  ]
  node [
    id 3
    label "nap&#281;d"
  ]
  node [
    id 4
    label "obiekt_mostowy"
  ]
  node [
    id 5
    label "rzuci&#263;"
  ]
  node [
    id 6
    label "zam&#243;zgowie"
  ]
  node [
    id 7
    label "rzuca&#263;"
  ]
  node [
    id 8
    label "porozumienie"
  ]
  node [
    id 9
    label "pylon"
  ]
  node [
    id 10
    label "m&#243;zg"
  ]
  node [
    id 11
    label "trasa"
  ]
  node [
    id 12
    label "suwnica"
  ]
  node [
    id 13
    label "urz&#261;dzenie"
  ]
  node [
    id 14
    label "rzucenie"
  ]
  node [
    id 15
    label "szczelina_dylatacyjna"
  ]
  node [
    id 16
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 17
    label "prz&#281;s&#322;o"
  ]
  node [
    id 18
    label "samoch&#243;d"
  ]
  node [
    id 19
    label "rzucanie"
  ]
  node [
    id 20
    label "bridge"
  ]
  node [
    id 21
    label "&#346;redzkie"
  ]
  node [
    id 22
    label "drogi"
  ]
  node [
    id 23
    label "krajowy"
  ]
  node [
    id 24
    label "nr"
  ]
  node [
    id 25
    label "94"
  ]
  node [
    id 26
    label "m&#322;yn"
  ]
  node [
    id 27
    label "Le&#347;nica"
  ]
  node [
    id 28
    label "&#346;redzki"
  ]
  node [
    id 29
    label "wschodni"
  ]
  node [
    id 30
    label "zachodny"
  ]
  node [
    id 31
    label "Weistritz"
  ]
  node [
    id 32
    label "Br&#252;cke"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 24
  ]
  edge [
    source 22
    target 25
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 25
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 30
  ]
  edge [
    source 31
    target 32
  ]
]
