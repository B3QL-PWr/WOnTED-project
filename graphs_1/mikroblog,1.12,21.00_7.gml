graph [
  maxDegree 4
  minDegree 1
  meanDegree 1.6666666666666667
  density 0.3333333333333333
  graphCliqueNumber 2
  node [
    id 0
    label "heheszki"
    origin "text"
  ]
  node [
    id 1
    label "przegryw"
    origin "text"
  ]
  node [
    id 2
    label "tfwnogf"
    origin "text"
  ]
  node [
    id 3
    label "feels"
    origin "text"
  ]
  node [
    id 4
    label "nieudacznik"
  ]
  node [
    id 5
    label "przegraniec"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 2
    target 3
  ]
]
