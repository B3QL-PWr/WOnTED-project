graph [
  maxDegree 20
  minDegree 1
  meanDegree 1.9090909090909092
  density 0.09090909090909091
  graphCliqueNumber 2
  node [
    id 0
    label "siebie"
    origin "text"
  ]
  node [
    id 1
    label "karta"
    origin "text"
  ]
  node [
    id 2
    label "zrobi&#263;by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "kurfa"
    origin "text"
  ]
  node [
    id 4
    label "ticket"
  ]
  node [
    id 5
    label "kartonik"
  ]
  node [
    id 6
    label "charter"
  ]
  node [
    id 7
    label "Europejska_Karta_Ubezpieczenia_Zdrowotnego"
  ]
  node [
    id 8
    label "kartka"
  ]
  node [
    id 9
    label "formularz"
  ]
  node [
    id 10
    label "restauracja"
  ]
  node [
    id 11
    label "chart"
  ]
  node [
    id 12
    label "menu"
  ]
  node [
    id 13
    label "danie"
  ]
  node [
    id 14
    label "circuit_board"
  ]
  node [
    id 15
    label "&#347;rodek_p&#322;atniczy"
  ]
  node [
    id 16
    label "komputer"
  ]
  node [
    id 17
    label "p&#322;ytka"
  ]
  node [
    id 18
    label "urz&#261;dzenie"
  ]
  node [
    id 19
    label "zezwolenie"
  ]
  node [
    id 20
    label "cennik"
  ]
  node [
    id 21
    label "oferta"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 2
    target 3
  ]
]
