graph [
  maxDegree 31
  minDegree 1
  meanDegree 2.1
  density 0.008108108108108109
  graphCliqueNumber 2
  node [
    id 0
    label "ostatni"
    origin "text"
  ]
  node [
    id 1
    label "mecz"
    origin "text"
  ]
  node [
    id 2
    label "ozorkowie"
    origin "text"
  ]
  node [
    id 3
    label "runda"
    origin "text"
  ]
  node [
    id 4
    label "jesienny"
    origin "text"
  ]
  node [
    id 5
    label "pi&#322;karz"
    origin "text"
  ]
  node [
    id 6
    label "bzura"
    origin "text"
  ]
  node [
    id 7
    label "ozorek"
    origin "text"
  ]
  node [
    id 8
    label "wygra&#263;"
    origin "text"
  ]
  node [
    id 9
    label "bohater"
    origin "text"
  ]
  node [
    id 10
    label "spotkanie"
    origin "text"
  ]
  node [
    id 11
    label "okaza&#263;"
    origin "text"
  ]
  node [
    id 12
    label "si&#281;"
    origin "text"
  ]
  node [
    id 13
    label "wychowanek"
    origin "text"
  ]
  node [
    id 14
    label "widzew"
    origin "text"
  ]
  node [
    id 15
    label "circa"
    origin "text"
  ]
  node [
    id 16
    label "codzie&#324;"
    origin "text"
  ]
  node [
    id 17
    label "gra&#263;"
    origin "text"
  ]
  node [
    id 18
    label "halowy"
    origin "text"
  ]
  node [
    id 19
    label "dru&#380;yna"
    origin "text"
  ]
  node [
    id 20
    label "owlex"
    origin "text"
  ]
  node [
    id 21
    label "&#322;&#243;dz"
    origin "text"
  ]
  node [
    id 22
    label "marcin"
    origin "text"
  ]
  node [
    id 23
    label "janeczka"
    origin "text"
  ]
  node [
    id 24
    label "obchodzi&#263;"
    origin "text"
  ]
  node [
    id 25
    label "swoje"
    origin "text"
  ]
  node [
    id 26
    label "imieniny"
    origin "text"
  ]
  node [
    id 27
    label "strzeli&#263;"
    origin "text"
  ]
  node [
    id 28
    label "dwa"
    origin "text"
  ]
  node [
    id 29
    label "bramka"
    origin "text"
  ]
  node [
    id 30
    label "lista"
    origin "text"
  ]
  node [
    id 31
    label "strzelec"
    origin "text"
  ]
  node [
    id 32
    label "wpisa&#263;"
    origin "text"
  ]
  node [
    id 33
    label "tak&#380;e"
    origin "text"
  ]
  node [
    id 34
    label "dobry"
    origin "text"
  ]
  node [
    id 35
    label "strzelce"
    origin "text"
  ]
  node [
    id 36
    label "mai&#263;"
    origin "text"
  ]
  node [
    id 37
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 38
    label "&#322;ukasz"
    origin "text"
  ]
  node [
    id 39
    label "olczyk"
    origin "text"
  ]
  node [
    id 40
    label "cz&#322;owiek"
  ]
  node [
    id 41
    label "kolejny"
  ]
  node [
    id 42
    label "istota_&#380;ywa"
  ]
  node [
    id 43
    label "najgorszy"
  ]
  node [
    id 44
    label "aktualny"
  ]
  node [
    id 45
    label "ostatnio"
  ]
  node [
    id 46
    label "niedawno"
  ]
  node [
    id 47
    label "wi&#261;&#380;&#261;cy"
  ]
  node [
    id 48
    label "sko&#324;czony"
  ]
  node [
    id 49
    label "poprzedni"
  ]
  node [
    id 50
    label "pozosta&#322;y"
  ]
  node [
    id 51
    label "w&#261;tpliwy"
  ]
  node [
    id 52
    label "obrona"
  ]
  node [
    id 53
    label "gra"
  ]
  node [
    id 54
    label "dwumecz"
  ]
  node [
    id 55
    label "game"
  ]
  node [
    id 56
    label "serw"
  ]
  node [
    id 57
    label "seria"
  ]
  node [
    id 58
    label "faza"
  ]
  node [
    id 59
    label "czas"
  ]
  node [
    id 60
    label "okr&#261;&#380;enie"
  ]
  node [
    id 61
    label "rhythm"
  ]
  node [
    id 62
    label "rozgrywka"
  ]
  node [
    id 63
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 64
    label "turniej"
  ]
  node [
    id 65
    label "jesiennie"
  ]
  node [
    id 66
    label "sezonowy"
  ]
  node [
    id 67
    label "zajesienienie_si&#281;"
  ]
  node [
    id 68
    label "ciep&#322;y"
  ]
  node [
    id 69
    label "typowy"
  ]
  node [
    id 70
    label "jesienienie_si&#281;"
  ]
  node [
    id 71
    label "gracz"
  ]
  node [
    id 72
    label "legionista"
  ]
  node [
    id 73
    label "sportowiec"
  ]
  node [
    id 74
    label "Daniel_Dubicki"
  ]
  node [
    id 75
    label "pieczarkowiec"
  ]
  node [
    id 76
    label "ozorkowate"
  ]
  node [
    id 77
    label "saprotrof"
  ]
  node [
    id 78
    label "grzyb"
  ]
  node [
    id 79
    label "paso&#380;yt"
  ]
  node [
    id 80
    label "podroby"
  ]
  node [
    id 81
    label "score"
  ]
  node [
    id 82
    label "okaza&#263;_si&#281;"
  ]
  node [
    id 83
    label "zwojowa&#263;"
  ]
  node [
    id 84
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 85
    label "leave"
  ]
  node [
    id 86
    label "znie&#347;&#263;"
  ]
  node [
    id 87
    label "zagwarantowa&#263;"
  ]
  node [
    id 88
    label "instrument_muzyczny"
  ]
  node [
    id 89
    label "zagra&#263;"
  ]
  node [
    id 90
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 91
    label "zrobi&#263;"
  ]
  node [
    id 92
    label "net_income"
  ]
  node [
    id 93
    label "bohaterski"
  ]
  node [
    id 94
    label "Zgredek"
  ]
  node [
    id 95
    label "Herkules"
  ]
  node [
    id 96
    label "Casanova"
  ]
  node [
    id 97
    label "Borewicz"
  ]
  node [
    id 98
    label "Don_Juan"
  ]
  node [
    id 99
    label "posta&#263;_mitologiczna"
  ]
  node [
    id 100
    label "Winnetou"
  ]
  node [
    id 101
    label "niezwyci&#281;&#380;ony"
  ]
  node [
    id 102
    label "Messi"
  ]
  node [
    id 103
    label "Herkules_Poirot"
  ]
  node [
    id 104
    label "cz&#322;owiek_wielkiego_ducha"
  ]
  node [
    id 105
    label "Szwejk"
  ]
  node [
    id 106
    label "Sherlock_Holmes"
  ]
  node [
    id 107
    label "Fantastyczna_Czw&#243;rka"
  ]
  node [
    id 108
    label "Hamlet"
  ]
  node [
    id 109
    label "Asterix"
  ]
  node [
    id 110
    label "Quasimodo"
  ]
  node [
    id 111
    label "Wallenrod"
  ]
  node [
    id 112
    label "Don_Kiszot"
  ]
  node [
    id 113
    label "uczestnik"
  ]
  node [
    id 114
    label "&#347;mia&#322;ek"
  ]
  node [
    id 115
    label "Harry_Potter"
  ]
  node [
    id 116
    label "podmiot"
  ]
  node [
    id 117
    label "Achilles"
  ]
  node [
    id 118
    label "Werter"
  ]
  node [
    id 119
    label "Mario"
  ]
  node [
    id 120
    label "posta&#263;"
  ]
  node [
    id 121
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 122
    label "po&#380;egnanie"
  ]
  node [
    id 123
    label "spowodowanie"
  ]
  node [
    id 124
    label "znalezienie"
  ]
  node [
    id 125
    label "znajomy"
  ]
  node [
    id 126
    label "doznanie"
  ]
  node [
    id 127
    label "employment"
  ]
  node [
    id 128
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 129
    label "gather"
  ]
  node [
    id 130
    label "powitanie"
  ]
  node [
    id 131
    label "spotykanie"
  ]
  node [
    id 132
    label "wydarzenie"
  ]
  node [
    id 133
    label "gathering"
  ]
  node [
    id 134
    label "spotkanie_si&#281;"
  ]
  node [
    id 135
    label "zdarzenie_si&#281;"
  ]
  node [
    id 136
    label "match"
  ]
  node [
    id 137
    label "zawarcie"
  ]
  node [
    id 138
    label "pokaza&#263;"
  ]
  node [
    id 139
    label "zdradzi&#263;_si&#281;"
  ]
  node [
    id 140
    label "testify"
  ]
  node [
    id 141
    label "give"
  ]
  node [
    id 142
    label "ucze&#324;"
  ]
  node [
    id 143
    label "podopieczny"
  ]
  node [
    id 144
    label "student"
  ]
  node [
    id 145
    label "szko&#322;a"
  ]
  node [
    id 146
    label "&#347;wieci&#263;"
  ]
  node [
    id 147
    label "typify"
  ]
  node [
    id 148
    label "majaczy&#263;"
  ]
  node [
    id 149
    label "dzia&#322;a&#263;"
  ]
  node [
    id 150
    label "rola"
  ]
  node [
    id 151
    label "wykonywa&#263;"
  ]
  node [
    id 152
    label "tokowa&#263;"
  ]
  node [
    id 153
    label "prezentowa&#263;"
  ]
  node [
    id 154
    label "uzewn&#281;trznia&#263;_si&#281;"
  ]
  node [
    id 155
    label "rozgrywa&#263;"
  ]
  node [
    id 156
    label "przedstawia&#263;"
  ]
  node [
    id 157
    label "wykorzystywa&#263;"
  ]
  node [
    id 158
    label "wida&#263;"
  ]
  node [
    id 159
    label "brzmie&#263;"
  ]
  node [
    id 160
    label "dally"
  ]
  node [
    id 161
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 162
    label "robi&#263;"
  ]
  node [
    id 163
    label "do"
  ]
  node [
    id 164
    label "play"
  ]
  node [
    id 165
    label "otwarcie"
  ]
  node [
    id 166
    label "szczeka&#263;"
  ]
  node [
    id 167
    label "cope"
  ]
  node [
    id 168
    label "pasowa&#263;"
  ]
  node [
    id 169
    label "napierdziela&#263;"
  ]
  node [
    id 170
    label "sound"
  ]
  node [
    id 171
    label "muzykowa&#263;"
  ]
  node [
    id 172
    label "stara&#263;_si&#281;"
  ]
  node [
    id 173
    label "i&#347;&#263;"
  ]
  node [
    id 174
    label "whole"
  ]
  node [
    id 175
    label "szczep"
  ]
  node [
    id 176
    label "dublet"
  ]
  node [
    id 177
    label "pluton"
  ]
  node [
    id 178
    label "formacja"
  ]
  node [
    id 179
    label "zesp&#243;&#322;"
  ]
  node [
    id 180
    label "force"
  ]
  node [
    id 181
    label "zast&#281;p"
  ]
  node [
    id 182
    label "pododdzia&#322;"
  ]
  node [
    id 183
    label "omija&#263;"
  ]
  node [
    id 184
    label "okr&#261;&#380;a&#263;"
  ]
  node [
    id 185
    label "sp&#281;dza&#263;"
  ]
  node [
    id 186
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 187
    label "interesowa&#263;"
  ]
  node [
    id 188
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 189
    label "post&#281;powa&#263;"
  ]
  node [
    id 190
    label "feast"
  ]
  node [
    id 191
    label "chodzi&#263;"
  ]
  node [
    id 192
    label "wymija&#263;"
  ]
  node [
    id 193
    label "odwiedza&#263;"
  ]
  node [
    id 194
    label "prowadzi&#263;"
  ]
  node [
    id 195
    label "impreza"
  ]
  node [
    id 196
    label "dzie&#324;"
  ]
  node [
    id 197
    label "rap"
  ]
  node [
    id 198
    label "blast"
  ]
  node [
    id 199
    label "trafi&#263;"
  ]
  node [
    id 200
    label "uderzy&#263;"
  ]
  node [
    id 201
    label "draw"
  ]
  node [
    id 202
    label "plun&#261;&#263;"
  ]
  node [
    id 203
    label "odpali&#263;"
  ]
  node [
    id 204
    label "zdoby&#263;"
  ]
  node [
    id 205
    label "obstawi&#263;"
  ]
  node [
    id 206
    label "zamek"
  ]
  node [
    id 207
    label "p&#322;ot"
  ]
  node [
    id 208
    label "obstawienie"
  ]
  node [
    id 209
    label "przedmiot"
  ]
  node [
    id 210
    label "goal"
  ]
  node [
    id 211
    label "trafienie"
  ]
  node [
    id 212
    label "siatka"
  ]
  node [
    id 213
    label "poprzeczka"
  ]
  node [
    id 214
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 215
    label "zawiasy"
  ]
  node [
    id 216
    label "obstawia&#263;"
  ]
  node [
    id 217
    label "wej&#347;cie"
  ]
  node [
    id 218
    label "brama"
  ]
  node [
    id 219
    label "s&#322;upek"
  ]
  node [
    id 220
    label "obstawianie"
  ]
  node [
    id 221
    label "boisko"
  ]
  node [
    id 222
    label "ogrodzenie"
  ]
  node [
    id 223
    label "przeszkoda"
  ]
  node [
    id 224
    label "wyliczanka"
  ]
  node [
    id 225
    label "catalog"
  ]
  node [
    id 226
    label "stock"
  ]
  node [
    id 227
    label "figurowa&#263;"
  ]
  node [
    id 228
    label "zbi&#243;r"
  ]
  node [
    id 229
    label "book"
  ]
  node [
    id 230
    label "pozycja"
  ]
  node [
    id 231
    label "tekst"
  ]
  node [
    id 232
    label "sumariusz"
  ]
  node [
    id 233
    label "figura"
  ]
  node [
    id 234
    label "Renata_Mauer"
  ]
  node [
    id 235
    label "&#380;o&#322;nierz"
  ]
  node [
    id 236
    label "futbolista"
  ]
  node [
    id 237
    label "napisa&#263;"
  ]
  node [
    id 238
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 239
    label "write"
  ]
  node [
    id 240
    label "wprowadzi&#263;"
  ]
  node [
    id 241
    label "pomy&#347;lny"
  ]
  node [
    id 242
    label "skuteczny"
  ]
  node [
    id 243
    label "moralny"
  ]
  node [
    id 244
    label "korzystny"
  ]
  node [
    id 245
    label "odpowiedni"
  ]
  node [
    id 246
    label "zwrot"
  ]
  node [
    id 247
    label "dobrze"
  ]
  node [
    id 248
    label "pozytywny"
  ]
  node [
    id 249
    label "grzeczny"
  ]
  node [
    id 250
    label "mi&#322;y"
  ]
  node [
    id 251
    label "dobroczynny"
  ]
  node [
    id 252
    label "pos&#322;uszny"
  ]
  node [
    id 253
    label "ca&#322;y"
  ]
  node [
    id 254
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 255
    label "czw&#243;rka"
  ]
  node [
    id 256
    label "spokojny"
  ]
  node [
    id 257
    label "&#347;mieszny"
  ]
  node [
    id 258
    label "drogi"
  ]
  node [
    id 259
    label "ozdabia&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 15
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 40
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 9
    target 100
  ]
  edge [
    source 9
    target 101
  ]
  edge [
    source 9
    target 102
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 104
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 9
    target 116
  ]
  edge [
    source 9
    target 117
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 9
    target 119
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 121
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 10
    target 125
  ]
  edge [
    source 10
    target 126
  ]
  edge [
    source 10
    target 127
  ]
  edge [
    source 10
    target 128
  ]
  edge [
    source 10
    target 129
  ]
  edge [
    source 10
    target 130
  ]
  edge [
    source 10
    target 131
  ]
  edge [
    source 10
    target 132
  ]
  edge [
    source 10
    target 133
  ]
  edge [
    source 10
    target 134
  ]
  edge [
    source 10
    target 135
  ]
  edge [
    source 10
    target 136
  ]
  edge [
    source 10
    target 137
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 138
  ]
  edge [
    source 11
    target 139
  ]
  edge [
    source 11
    target 140
  ]
  edge [
    source 11
    target 141
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 32
  ]
  edge [
    source 12
    target 33
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 40
  ]
  edge [
    source 13
    target 142
  ]
  edge [
    source 13
    target 143
  ]
  edge [
    source 13
    target 144
  ]
  edge [
    source 13
    target 145
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 38
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 146
  ]
  edge [
    source 17
    target 147
  ]
  edge [
    source 17
    target 148
  ]
  edge [
    source 17
    target 149
  ]
  edge [
    source 17
    target 150
  ]
  edge [
    source 17
    target 151
  ]
  edge [
    source 17
    target 152
  ]
  edge [
    source 17
    target 153
  ]
  edge [
    source 17
    target 154
  ]
  edge [
    source 17
    target 155
  ]
  edge [
    source 17
    target 156
  ]
  edge [
    source 17
    target 157
  ]
  edge [
    source 17
    target 158
  ]
  edge [
    source 17
    target 159
  ]
  edge [
    source 17
    target 160
  ]
  edge [
    source 17
    target 161
  ]
  edge [
    source 17
    target 162
  ]
  edge [
    source 17
    target 163
  ]
  edge [
    source 17
    target 88
  ]
  edge [
    source 17
    target 164
  ]
  edge [
    source 17
    target 165
  ]
  edge [
    source 17
    target 166
  ]
  edge [
    source 17
    target 167
  ]
  edge [
    source 17
    target 168
  ]
  edge [
    source 17
    target 169
  ]
  edge [
    source 17
    target 170
  ]
  edge [
    source 17
    target 171
  ]
  edge [
    source 17
    target 172
  ]
  edge [
    source 17
    target 173
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 35
  ]
  edge [
    source 19
    target 36
  ]
  edge [
    source 19
    target 174
  ]
  edge [
    source 19
    target 175
  ]
  edge [
    source 19
    target 176
  ]
  edge [
    source 19
    target 177
  ]
  edge [
    source 19
    target 178
  ]
  edge [
    source 19
    target 179
  ]
  edge [
    source 19
    target 180
  ]
  edge [
    source 19
    target 181
  ]
  edge [
    source 19
    target 182
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 157
  ]
  edge [
    source 24
    target 183
  ]
  edge [
    source 24
    target 184
  ]
  edge [
    source 24
    target 185
  ]
  edge [
    source 24
    target 186
  ]
  edge [
    source 24
    target 187
  ]
  edge [
    source 24
    target 188
  ]
  edge [
    source 24
    target 189
  ]
  edge [
    source 24
    target 190
  ]
  edge [
    source 24
    target 191
  ]
  edge [
    source 24
    target 192
  ]
  edge [
    source 24
    target 193
  ]
  edge [
    source 24
    target 194
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 195
  ]
  edge [
    source 26
    target 196
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 91
  ]
  edge [
    source 27
    target 197
  ]
  edge [
    source 27
    target 198
  ]
  edge [
    source 27
    target 199
  ]
  edge [
    source 27
    target 200
  ]
  edge [
    source 27
    target 201
  ]
  edge [
    source 27
    target 202
  ]
  edge [
    source 27
    target 203
  ]
  edge [
    source 27
    target 204
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 37
  ]
  edge [
    source 29
    target 38
  ]
  edge [
    source 29
    target 205
  ]
  edge [
    source 29
    target 206
  ]
  edge [
    source 29
    target 207
  ]
  edge [
    source 29
    target 208
  ]
  edge [
    source 29
    target 209
  ]
  edge [
    source 29
    target 210
  ]
  edge [
    source 29
    target 211
  ]
  edge [
    source 29
    target 212
  ]
  edge [
    source 29
    target 213
  ]
  edge [
    source 29
    target 214
  ]
  edge [
    source 29
    target 215
  ]
  edge [
    source 29
    target 216
  ]
  edge [
    source 29
    target 217
  ]
  edge [
    source 29
    target 218
  ]
  edge [
    source 29
    target 219
  ]
  edge [
    source 29
    target 220
  ]
  edge [
    source 29
    target 221
  ]
  edge [
    source 29
    target 222
  ]
  edge [
    source 29
    target 223
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 224
  ]
  edge [
    source 30
    target 225
  ]
  edge [
    source 30
    target 226
  ]
  edge [
    source 30
    target 227
  ]
  edge [
    source 30
    target 228
  ]
  edge [
    source 30
    target 229
  ]
  edge [
    source 30
    target 230
  ]
  edge [
    source 30
    target 231
  ]
  edge [
    source 30
    target 232
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 233
  ]
  edge [
    source 31
    target 40
  ]
  edge [
    source 31
    target 234
  ]
  edge [
    source 31
    target 235
  ]
  edge [
    source 31
    target 236
  ]
  edge [
    source 31
    target 73
  ]
  edge [
    source 32
    target 237
  ]
  edge [
    source 32
    target 238
  ]
  edge [
    source 32
    target 201
  ]
  edge [
    source 32
    target 239
  ]
  edge [
    source 32
    target 240
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 241
  ]
  edge [
    source 34
    target 242
  ]
  edge [
    source 34
    target 243
  ]
  edge [
    source 34
    target 244
  ]
  edge [
    source 34
    target 245
  ]
  edge [
    source 34
    target 246
  ]
  edge [
    source 34
    target 247
  ]
  edge [
    source 34
    target 248
  ]
  edge [
    source 34
    target 249
  ]
  edge [
    source 34
    target 130
  ]
  edge [
    source 34
    target 250
  ]
  edge [
    source 34
    target 251
  ]
  edge [
    source 34
    target 252
  ]
  edge [
    source 34
    target 253
  ]
  edge [
    source 34
    target 254
  ]
  edge [
    source 34
    target 255
  ]
  edge [
    source 34
    target 256
  ]
  edge [
    source 34
    target 257
  ]
  edge [
    source 34
    target 258
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 259
  ]
  edge [
    source 38
    target 39
  ]
]
