graph [
  maxDegree 8
  minDegree 1
  meanDegree 2.2941176470588234
  density 0.06951871657754011
  graphCliqueNumber 5
  node [
    id 0
    label "media"
    origin "text"
  ]
  node [
    id 1
    label "spo&#322;eczny"
    origin "text"
  ]
  node [
    id 2
    label "nowo&#347;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "przekazior"
  ]
  node [
    id 4
    label "mass-media"
  ]
  node [
    id 5
    label "uzbrajanie"
  ]
  node [
    id 6
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 7
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 8
    label "medium"
  ]
  node [
    id 9
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 10
    label "niepubliczny"
  ]
  node [
    id 11
    label "spo&#322;ecznie"
  ]
  node [
    id 12
    label "publiczny"
  ]
  node [
    id 13
    label "urozmaicenie"
  ]
  node [
    id 14
    label "newness"
  ]
  node [
    id 15
    label "wiek"
  ]
  node [
    id 16
    label "Scott"
  ]
  node [
    id 17
    label "Berkun"
  ]
  node [
    id 18
    label "Calling"
  ]
  node [
    id 19
    label "Bullshit"
  ]
  node [
    id 20
    label "on"
  ]
  node [
    id 21
    label "Social"
  ]
  node [
    id 22
    label "ojciec"
  ]
  node [
    id 23
    label "&#8217;"
  ]
  node [
    id 24
    label "Reilly"
  ]
  node [
    id 25
    label "radar"
  ]
  node [
    id 26
    label "Joshua"
  ]
  node [
    id 27
    label "Michele"
  ]
  node [
    id 28
    label "Ross"
  ]
  node [
    id 29
    label "Andrew"
  ]
  node [
    id 30
    label "Keena"
  ]
  node [
    id 31
    label "weba"
  ]
  node [
    id 32
    label "2"
  ]
  node [
    id 33
    label "0"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 8
    target 18
  ]
  edge [
    source 8
    target 19
  ]
  edge [
    source 8
    target 20
  ]
  edge [
    source 8
    target 21
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 20
  ]
  edge [
    source 18
    target 21
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 21
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 24
  ]
  edge [
    source 22
    target 25
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 25
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 28
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 33
  ]
  edge [
    source 32
    target 33
  ]
]
