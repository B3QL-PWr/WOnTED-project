graph [
  maxDegree 18
  minDegree 1
  meanDegree 1.9310344827586208
  density 0.0338777979431337
  graphCliqueNumber 2
  node [
    id 0
    label "by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "przez"
    origin "text"
  ]
  node [
    id 2
    label "lato"
    origin "text"
  ]
  node [
    id 3
    label "wolny"
    origin "text"
  ]
  node [
    id 4
    label "polska"
    origin "text"
  ]
  node [
    id 5
    label "taki"
    origin "text"
  ]
  node [
    id 6
    label "zbrodniarz"
    origin "text"
  ]
  node [
    id 7
    label "nie"
    origin "text"
  ]
  node [
    id 8
    label "powiedzie&#263;"
    origin "text"
  ]
  node [
    id 9
    label "jaros&#322;aw"
    origin "text"
  ]
  node [
    id 10
    label "szarek"
    origin "text"
  ]
  node [
    id 11
    label "si&#281;ga&#263;"
  ]
  node [
    id 12
    label "trwa&#263;"
  ]
  node [
    id 13
    label "obecno&#347;&#263;"
  ]
  node [
    id 14
    label "stan"
  ]
  node [
    id 15
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 16
    label "stand"
  ]
  node [
    id 17
    label "mie&#263;_miejsce"
  ]
  node [
    id 18
    label "uczestniczy&#263;"
  ]
  node [
    id 19
    label "chodzi&#263;"
  ]
  node [
    id 20
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 21
    label "equal"
  ]
  node [
    id 22
    label "pora_roku"
  ]
  node [
    id 23
    label "niezale&#380;ny"
  ]
  node [
    id 24
    label "swobodnie"
  ]
  node [
    id 25
    label "niespieszny"
  ]
  node [
    id 26
    label "rozrzedzanie"
  ]
  node [
    id 27
    label "zwolnienie_si&#281;"
  ]
  node [
    id 28
    label "wolno"
  ]
  node [
    id 29
    label "rozrzedzenie"
  ]
  node [
    id 30
    label "lu&#378;no"
  ]
  node [
    id 31
    label "zwalnianie_si&#281;"
  ]
  node [
    id 32
    label "wolnie"
  ]
  node [
    id 33
    label "strza&#322;"
  ]
  node [
    id 34
    label "rozwodnienie"
  ]
  node [
    id 35
    label "wakowa&#263;"
  ]
  node [
    id 36
    label "rozwadnianie"
  ]
  node [
    id 37
    label "rzedni&#281;cie"
  ]
  node [
    id 38
    label "zrzedni&#281;cie"
  ]
  node [
    id 39
    label "okre&#347;lony"
  ]
  node [
    id 40
    label "jaki&#347;"
  ]
  node [
    id 41
    label "przest&#281;pca"
  ]
  node [
    id 42
    label "sprzeciw"
  ]
  node [
    id 43
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 44
    label "express"
  ]
  node [
    id 45
    label "rzekn&#261;&#263;"
  ]
  node [
    id 46
    label "okre&#347;li&#263;"
  ]
  node [
    id 47
    label "wyrazi&#263;"
  ]
  node [
    id 48
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 49
    label "unwrap"
  ]
  node [
    id 50
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 51
    label "convey"
  ]
  node [
    id 52
    label "discover"
  ]
  node [
    id 53
    label "wydoby&#263;"
  ]
  node [
    id 54
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 55
    label "poda&#263;"
  ]
  node [
    id 56
    label "Jaros&#322;aw"
  ]
  node [
    id 57
    label "Szarek"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 8
    target 46
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 56
    target 57
  ]
]
