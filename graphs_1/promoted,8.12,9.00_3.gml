graph [
  maxDegree 42
  minDegree 1
  meanDegree 2.0168067226890756
  density 0.017091582395670133
  graphCliqueNumber 3
  node [
    id 0
    label "tata"
    origin "text"
  ]
  node [
    id 1
    label "za&#322;o&#380;y&#263;"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "c&#243;rka"
    origin "text"
  ]
  node [
    id 4
    label "zd&#261;&#380;y&#263;"
    origin "text"
  ]
  node [
    id 5
    label "wytrze&#263;"
    origin "text"
  ]
  node [
    id 6
    label "woda"
    origin "text"
  ]
  node [
    id 7
    label "pod&#322;oga"
    origin "text"
  ]
  node [
    id 8
    label "nim"
    origin "text"
  ]
  node [
    id 9
    label "uk&#322;u&#263;"
    origin "text"
  ]
  node [
    id 10
    label "widelec"
    origin "text"
  ]
  node [
    id 11
    label "sound"
    origin "text"
  ]
  node [
    id 12
    label "stary"
  ]
  node [
    id 13
    label "papa"
  ]
  node [
    id 14
    label "ojciec"
  ]
  node [
    id 15
    label "kuwada"
  ]
  node [
    id 16
    label "ojczym"
  ]
  node [
    id 17
    label "przodek"
  ]
  node [
    id 18
    label "rodzice"
  ]
  node [
    id 19
    label "rodzic"
  ]
  node [
    id 20
    label "insert"
  ]
  node [
    id 21
    label "utworzy&#263;"
  ]
  node [
    id 22
    label "ubra&#263;"
  ]
  node [
    id 23
    label "set"
  ]
  node [
    id 24
    label "invest"
  ]
  node [
    id 25
    label "pokry&#263;"
  ]
  node [
    id 26
    label "przewidzie&#263;"
  ]
  node [
    id 27
    label "umie&#347;ci&#263;"
  ]
  node [
    id 28
    label "map"
  ]
  node [
    id 29
    label "load"
  ]
  node [
    id 30
    label "zap&#322;aci&#263;"
  ]
  node [
    id 31
    label "oblec_si&#281;"
  ]
  node [
    id 32
    label "podwin&#261;&#263;"
  ]
  node [
    id 33
    label "plant"
  ]
  node [
    id 34
    label "create"
  ]
  node [
    id 35
    label "zrobi&#263;"
  ]
  node [
    id 36
    label "str&#243;j"
  ]
  node [
    id 37
    label "jell"
  ]
  node [
    id 38
    label "spowodowa&#263;"
  ]
  node [
    id 39
    label "oblec"
  ]
  node [
    id 40
    label "przyodzia&#263;"
  ]
  node [
    id 41
    label "install"
  ]
  node [
    id 42
    label "cz&#322;owiek"
  ]
  node [
    id 43
    label "dziewka"
  ]
  node [
    id 44
    label "potomkini"
  ]
  node [
    id 45
    label "dziewoja"
  ]
  node [
    id 46
    label "dziecko"
  ]
  node [
    id 47
    label "siksa"
  ]
  node [
    id 48
    label "dziewczynina"
  ]
  node [
    id 49
    label "dziunia"
  ]
  node [
    id 50
    label "dziewcz&#281;"
  ]
  node [
    id 51
    label "kora"
  ]
  node [
    id 52
    label "m&#322;&#243;dka"
  ]
  node [
    id 53
    label "dziecina"
  ]
  node [
    id 54
    label "sikorka"
  ]
  node [
    id 55
    label "spieszy&#263;_si&#281;"
  ]
  node [
    id 56
    label "po&#347;pie&#263;"
  ]
  node [
    id 57
    label "zjawi&#263;_si&#281;"
  ]
  node [
    id 58
    label "sko&#324;czy&#263;"
  ]
  node [
    id 59
    label "utrzyma&#263;"
  ]
  node [
    id 60
    label "wyczy&#347;ci&#263;"
  ]
  node [
    id 61
    label "mule"
  ]
  node [
    id 62
    label "zniszczy&#263;"
  ]
  node [
    id 63
    label "osuszy&#263;"
  ]
  node [
    id 64
    label "pout"
  ]
  node [
    id 65
    label "wypowied&#378;"
  ]
  node [
    id 66
    label "obiekt_naturalny"
  ]
  node [
    id 67
    label "bicie"
  ]
  node [
    id 68
    label "wysi&#281;k"
  ]
  node [
    id 69
    label "pustka"
  ]
  node [
    id 70
    label "woda_s&#322;odka"
  ]
  node [
    id 71
    label "p&#322;ycizna"
  ]
  node [
    id 72
    label "ciecz"
  ]
  node [
    id 73
    label "spi&#281;trza&#263;"
  ]
  node [
    id 74
    label "uj&#281;cie_wody"
  ]
  node [
    id 75
    label "chlasta&#263;"
  ]
  node [
    id 76
    label "spi&#281;trzy&#263;"
  ]
  node [
    id 77
    label "nap&#243;j"
  ]
  node [
    id 78
    label "bombast"
  ]
  node [
    id 79
    label "water"
  ]
  node [
    id 80
    label "kryptodepresja"
  ]
  node [
    id 81
    label "wodnik"
  ]
  node [
    id 82
    label "pojazd"
  ]
  node [
    id 83
    label "&#347;r&#243;dl&#261;dowy"
  ]
  node [
    id 84
    label "fala"
  ]
  node [
    id 85
    label "Waruna"
  ]
  node [
    id 86
    label "nap&#243;j_bezalkoholowy"
  ]
  node [
    id 87
    label "zrzut"
  ]
  node [
    id 88
    label "dotleni&#263;"
  ]
  node [
    id 89
    label "utylizator"
  ]
  node [
    id 90
    label "przyroda"
  ]
  node [
    id 91
    label "uci&#261;g"
  ]
  node [
    id 92
    label "wybrze&#380;e"
  ]
  node [
    id 93
    label "nabranie"
  ]
  node [
    id 94
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 95
    label "chlastanie"
  ]
  node [
    id 96
    label "klarownik"
  ]
  node [
    id 97
    label "przybrze&#380;e"
  ]
  node [
    id 98
    label "deklamacja"
  ]
  node [
    id 99
    label "spi&#281;trzenie"
  ]
  node [
    id 100
    label "przybieranie"
  ]
  node [
    id 101
    label "nabra&#263;"
  ]
  node [
    id 102
    label "tlenek"
  ]
  node [
    id 103
    label "spi&#281;trzanie"
  ]
  node [
    id 104
    label "l&#243;d"
  ]
  node [
    id 105
    label "zapadnia"
  ]
  node [
    id 106
    label "pomieszczenie"
  ]
  node [
    id 107
    label "budynek"
  ]
  node [
    id 108
    label "posadzka"
  ]
  node [
    id 109
    label "p&#322;aszczyzna"
  ]
  node [
    id 110
    label "gra_planszowa"
  ]
  node [
    id 111
    label "urazi&#263;"
  ]
  node [
    id 112
    label "incision"
  ]
  node [
    id 113
    label "prick"
  ]
  node [
    id 114
    label "przeszkodzi&#263;"
  ]
  node [
    id 115
    label "sting"
  ]
  node [
    id 116
    label "wbi&#263;"
  ]
  node [
    id 117
    label "niezb&#281;dnik"
  ]
  node [
    id 118
    label "sztuciec"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 8
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 9
    target 116
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 118
  ]
]
