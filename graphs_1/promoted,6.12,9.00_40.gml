graph [
  maxDegree 11
  minDegree 1
  meanDegree 1.9310344827586208
  density 0.06896551724137931
  graphCliqueNumber 2
  node [
    id 0
    label "ostatnio"
    origin "text"
  ]
  node [
    id 1
    label "coraz"
    origin "text"
  ]
  node [
    id 2
    label "cz&#281;sto"
    origin "text"
  ]
  node [
    id 3
    label "s&#322;ysze&#263;"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "taki"
    origin "text"
  ]
  node [
    id 6
    label "sprawa"
    origin "text"
  ]
  node [
    id 7
    label "ostatni"
  ]
  node [
    id 8
    label "poprzednio"
  ]
  node [
    id 9
    label "aktualnie"
  ]
  node [
    id 10
    label "cz&#281;stokro&#263;"
  ]
  node [
    id 11
    label "cz&#281;sty"
  ]
  node [
    id 12
    label "listen"
  ]
  node [
    id 13
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 14
    label "postrzega&#263;"
  ]
  node [
    id 15
    label "s&#322;ycha&#263;"
  ]
  node [
    id 16
    label "read"
  ]
  node [
    id 17
    label "okre&#347;lony"
  ]
  node [
    id 18
    label "jaki&#347;"
  ]
  node [
    id 19
    label "temat"
  ]
  node [
    id 20
    label "kognicja"
  ]
  node [
    id 21
    label "idea"
  ]
  node [
    id 22
    label "szczeg&#243;&#322;"
  ]
  node [
    id 23
    label "rzecz"
  ]
  node [
    id 24
    label "wydarzenie"
  ]
  node [
    id 25
    label "przes&#322;anka"
  ]
  node [
    id 26
    label "rozprawa"
  ]
  node [
    id 27
    label "object"
  ]
  node [
    id 28
    label "proposition"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 6
    target 19
  ]
  edge [
    source 6
    target 20
  ]
  edge [
    source 6
    target 21
  ]
  edge [
    source 6
    target 22
  ]
  edge [
    source 6
    target 23
  ]
  edge [
    source 6
    target 24
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 28
  ]
]
