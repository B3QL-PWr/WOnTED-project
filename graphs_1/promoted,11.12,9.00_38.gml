graph [
  maxDegree 22
  minDegree 1
  meanDegree 1.9622641509433962
  density 0.03773584905660377
  graphCliqueNumber 2
  node [
    id 0
    label "powspomina&#263;"
    origin "text"
  ]
  node [
    id 1
    label "gra"
    origin "text"
  ]
  node [
    id 2
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 3
    label "wychowa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "ca&#322;y"
    origin "text"
  ]
  node [
    id 6
    label "pokolenie"
    origin "text"
  ]
  node [
    id 7
    label "gracz"
    origin "text"
  ]
  node [
    id 8
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 9
    label "zabawa"
  ]
  node [
    id 10
    label "rywalizacja"
  ]
  node [
    id 11
    label "czynno&#347;&#263;"
  ]
  node [
    id 12
    label "Pok&#233;mon"
  ]
  node [
    id 13
    label "synteza"
  ]
  node [
    id 14
    label "odtworzenie"
  ]
  node [
    id 15
    label "komplet"
  ]
  node [
    id 16
    label "rekwizyt_do_gry"
  ]
  node [
    id 17
    label "odg&#322;os"
  ]
  node [
    id 18
    label "rozgrywka"
  ]
  node [
    id 19
    label "post&#281;powanie"
  ]
  node [
    id 20
    label "wydarzenie"
  ]
  node [
    id 21
    label "apparent_motion"
  ]
  node [
    id 22
    label "game"
  ]
  node [
    id 23
    label "zmienno&#347;&#263;"
  ]
  node [
    id 24
    label "zasada"
  ]
  node [
    id 25
    label "akcja"
  ]
  node [
    id 26
    label "play"
  ]
  node [
    id 27
    label "contest"
  ]
  node [
    id 28
    label "zbijany"
  ]
  node [
    id 29
    label "train"
  ]
  node [
    id 30
    label "wyszkoli&#263;"
  ]
  node [
    id 31
    label "zapewni&#263;"
  ]
  node [
    id 32
    label "du&#380;y"
  ]
  node [
    id 33
    label "jedyny"
  ]
  node [
    id 34
    label "kompletny"
  ]
  node [
    id 35
    label "zdr&#243;w"
  ]
  node [
    id 36
    label "&#380;ywy"
  ]
  node [
    id 37
    label "ca&#322;o"
  ]
  node [
    id 38
    label "pe&#322;ny"
  ]
  node [
    id 39
    label "calu&#347;ko"
  ]
  node [
    id 40
    label "podobny"
  ]
  node [
    id 41
    label "rocznik"
  ]
  node [
    id 42
    label "zbi&#243;r"
  ]
  node [
    id 43
    label "epoka"
  ]
  node [
    id 44
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 45
    label "drzewo_genealogiczne"
  ]
  node [
    id 46
    label "cz&#322;owiek"
  ]
  node [
    id 47
    label "bohater"
  ]
  node [
    id 48
    label "rozdawa&#263;_karty"
  ]
  node [
    id 49
    label "spryciarz"
  ]
  node [
    id 50
    label "zwierz&#281;"
  ]
  node [
    id 51
    label "uczestnik"
  ]
  node [
    id 52
    label "posta&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
]
