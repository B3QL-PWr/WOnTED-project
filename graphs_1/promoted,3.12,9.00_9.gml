graph [
  maxDegree 31
  minDegree 1
  meanDegree 2
  density 0.023255813953488372
  graphCliqueNumber 2
  node [
    id 0
    label "kiedy"
    origin "text"
  ]
  node [
    id 1
    label "znale&#378;&#263;"
    origin "text"
  ]
  node [
    id 2
    label "zw&#322;ok"
    origin "text"
  ]
  node [
    id 3
    label "by&#322;y"
    origin "text"
  ]
  node [
    id 4
    label "stopie&#324;"
    origin "text"
  ]
  node [
    id 5
    label "zw&#281;gli&#263;"
    origin "text"
  ]
  node [
    id 6
    label "da&#263;"
    origin "text"
  ]
  node [
    id 7
    label "si&#281;"
    origin "text"
  ]
  node [
    id 8
    label "zidentyfikowa&#263;"
    origin "text"
  ]
  node [
    id 9
    label "odzyska&#263;"
  ]
  node [
    id 10
    label "devise"
  ]
  node [
    id 11
    label "oceni&#263;"
  ]
  node [
    id 12
    label "znaj&#347;&#263;"
  ]
  node [
    id 13
    label "wymy&#347;li&#263;"
  ]
  node [
    id 14
    label "invent"
  ]
  node [
    id 15
    label "pozyska&#263;"
  ]
  node [
    id 16
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 17
    label "wykry&#263;"
  ]
  node [
    id 18
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 19
    label "dozna&#263;"
  ]
  node [
    id 20
    label "dawny"
  ]
  node [
    id 21
    label "rozw&#243;d"
  ]
  node [
    id 22
    label "rozwodzi&#263;_si&#281;"
  ]
  node [
    id 23
    label "eksprezydent"
  ]
  node [
    id 24
    label "rozwie&#347;&#263;_si&#281;"
  ]
  node [
    id 25
    label "partner"
  ]
  node [
    id 26
    label "rozwiedzenie_si&#281;"
  ]
  node [
    id 27
    label "rozwodzenie_si&#281;"
  ]
  node [
    id 28
    label "wcze&#347;niejszy"
  ]
  node [
    id 29
    label "minuta"
  ]
  node [
    id 30
    label "forma"
  ]
  node [
    id 31
    label "znaczenie"
  ]
  node [
    id 32
    label "kategoria_gramatyczna"
  ]
  node [
    id 33
    label "przys&#322;&#243;wek"
  ]
  node [
    id 34
    label "szczebel"
  ]
  node [
    id 35
    label "element"
  ]
  node [
    id 36
    label "poziom"
  ]
  node [
    id 37
    label "degree"
  ]
  node [
    id 38
    label "podn&#243;&#380;ek"
  ]
  node [
    id 39
    label "rank"
  ]
  node [
    id 40
    label "przymiotnik"
  ]
  node [
    id 41
    label "podzia&#322;"
  ]
  node [
    id 42
    label "ocena"
  ]
  node [
    id 43
    label "kszta&#322;t"
  ]
  node [
    id 44
    label "wschodek"
  ]
  node [
    id 45
    label "miejsce"
  ]
  node [
    id 46
    label "schody"
  ]
  node [
    id 47
    label "gama"
  ]
  node [
    id 48
    label "podstopie&#324;"
  ]
  node [
    id 49
    label "d&#378;wi&#281;k"
  ]
  node [
    id 50
    label "wielko&#347;&#263;"
  ]
  node [
    id 51
    label "jednostka"
  ]
  node [
    id 52
    label "spali&#263;"
  ]
  node [
    id 53
    label "char"
  ]
  node [
    id 54
    label "zmieni&#263;_w&#322;a&#347;ciwo&#347;ci_fizyczne_lub_chemiczne_czego&#347;"
  ]
  node [
    id 55
    label "spowodowa&#263;"
  ]
  node [
    id 56
    label "dostarczy&#263;"
  ]
  node [
    id 57
    label "obieca&#263;"
  ]
  node [
    id 58
    label "pozwoli&#263;"
  ]
  node [
    id 59
    label "przeznaczy&#263;"
  ]
  node [
    id 60
    label "doda&#263;"
  ]
  node [
    id 61
    label "give"
  ]
  node [
    id 62
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 63
    label "wyrzec_si&#281;"
  ]
  node [
    id 64
    label "supply"
  ]
  node [
    id 65
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 66
    label "zada&#263;"
  ]
  node [
    id 67
    label "odst&#261;pi&#263;"
  ]
  node [
    id 68
    label "feed"
  ]
  node [
    id 69
    label "testify"
  ]
  node [
    id 70
    label "powierzy&#263;"
  ]
  node [
    id 71
    label "convey"
  ]
  node [
    id 72
    label "przekaza&#263;"
  ]
  node [
    id 73
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 74
    label "zap&#322;aci&#263;"
  ]
  node [
    id 75
    label "dress"
  ]
  node [
    id 76
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 77
    label "udost&#281;pni&#263;"
  ]
  node [
    id 78
    label "sztachn&#261;&#263;"
  ]
  node [
    id 79
    label "zrobi&#263;"
  ]
  node [
    id 80
    label "przywali&#263;"
  ]
  node [
    id 81
    label "rap"
  ]
  node [
    id 82
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 83
    label "picture"
  ]
  node [
    id 84
    label "rozpozna&#263;"
  ]
  node [
    id 85
    label "tag"
  ]
  node [
    id 86
    label "ujednolici&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 18
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
]
