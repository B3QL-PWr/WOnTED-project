graph [
  maxDegree 8
  minDegree 1
  meanDegree 1.9166666666666667
  density 0.08333333333333333
  graphCliqueNumber 3
  node [
    id 0
    label "wybuch&#322;y"
    origin "text"
  ]
  node [
    id 1
    label "epidemia"
    origin "text"
  ]
  node [
    id 2
    label "odra"
    origin "text"
  ]
  node [
    id 3
    label "spo&#322;eczno&#347;&#263;"
    origin "text"
  ]
  node [
    id 4
    label "&#380;ydowski"
    origin "text"
  ]
  node [
    id 5
    label "londyn"
    origin "text"
  ]
  node [
    id 6
    label "atakowanie"
  ]
  node [
    id 7
    label "plaga"
  ]
  node [
    id 8
    label "atakowa&#263;"
  ]
  node [
    id 9
    label "choroba_somatyczna"
  ]
  node [
    id 10
    label "wirus_odry"
  ]
  node [
    id 11
    label "choroba_zara&#378;liwa"
  ]
  node [
    id 12
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 13
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 14
    label "Fremeni"
  ]
  node [
    id 15
    label "parchaty"
  ]
  node [
    id 16
    label "pejsaty"
  ]
  node [
    id 17
    label "semicki"
  ]
  node [
    id 18
    label "charakterystyczny"
  ]
  node [
    id 19
    label "&#380;ydowsko"
  ]
  node [
    id 20
    label "moszaw"
  ]
  node [
    id 21
    label "&#347;wiatowy"
  ]
  node [
    id 22
    label "organizacja"
  ]
  node [
    id 23
    label "zdrowie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 23
  ]
  edge [
    source 22
    target 23
  ]
]
