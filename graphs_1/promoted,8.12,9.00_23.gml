graph [
  maxDegree 46
  minDegree 1
  meanDegree 2
  density 0.014598540145985401
  graphCliqueNumber 3
  node [
    id 0
    label "autentyczny"
    origin "text"
  ]
  node [
    id 1
    label "historia"
    origin "text"
  ]
  node [
    id 2
    label "wiktor"
    origin "text"
  ]
  node [
    id 3
    label "suworowa"
    origin "text"
  ]
  node [
    id 4
    label "oficer"
    origin "text"
  ]
  node [
    id 5
    label "gru"
    origin "text"
  ]
  node [
    id 6
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 7
    label "przej&#347;&#263;"
    origin "text"
  ]
  node [
    id 8
    label "strona"
    origin "text"
  ]
  node [
    id 9
    label "zach&#243;d"
    origin "text"
  ]
  node [
    id 10
    label "szczery"
  ]
  node [
    id 11
    label "istny"
  ]
  node [
    id 12
    label "wyj&#261;tkowy"
  ]
  node [
    id 13
    label "naturalny"
  ]
  node [
    id 14
    label "prawdziwie"
  ]
  node [
    id 15
    label "prawdziwy"
  ]
  node [
    id 16
    label "autentycznie"
  ]
  node [
    id 17
    label "&#380;ywny"
  ]
  node [
    id 18
    label "report"
  ]
  node [
    id 19
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 20
    label "wypowied&#378;"
  ]
  node [
    id 21
    label "neografia"
  ]
  node [
    id 22
    label "przedmiot"
  ]
  node [
    id 23
    label "papirologia"
  ]
  node [
    id 24
    label "historia_gospodarcza"
  ]
  node [
    id 25
    label "przebiec"
  ]
  node [
    id 26
    label "hista"
  ]
  node [
    id 27
    label "nauka_humanistyczna"
  ]
  node [
    id 28
    label "filigranistyka"
  ]
  node [
    id 29
    label "dyplomatyka"
  ]
  node [
    id 30
    label "annalistyka"
  ]
  node [
    id 31
    label "historyka"
  ]
  node [
    id 32
    label "heraldyka"
  ]
  node [
    id 33
    label "fabu&#322;a"
  ]
  node [
    id 34
    label "muzealnictwo"
  ]
  node [
    id 35
    label "varsavianistyka"
  ]
  node [
    id 36
    label "prezentyzm"
  ]
  node [
    id 37
    label "mediewistyka"
  ]
  node [
    id 38
    label "przebiegni&#281;cie"
  ]
  node [
    id 39
    label "charakter"
  ]
  node [
    id 40
    label "paleografia"
  ]
  node [
    id 41
    label "genealogia"
  ]
  node [
    id 42
    label "czynno&#347;&#263;"
  ]
  node [
    id 43
    label "prozopografia"
  ]
  node [
    id 44
    label "motyw"
  ]
  node [
    id 45
    label "nautologia"
  ]
  node [
    id 46
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 47
    label "epoka"
  ]
  node [
    id 48
    label "numizmatyka"
  ]
  node [
    id 49
    label "ruralistyka"
  ]
  node [
    id 50
    label "epigrafika"
  ]
  node [
    id 51
    label "&#378;r&#243;d&#322;oznawstwo"
  ]
  node [
    id 52
    label "historiografia"
  ]
  node [
    id 53
    label "bizantynistyka"
  ]
  node [
    id 54
    label "weksylologia"
  ]
  node [
    id 55
    label "kierunek"
  ]
  node [
    id 56
    label "ikonografia"
  ]
  node [
    id 57
    label "chronologia"
  ]
  node [
    id 58
    label "archiwistyka"
  ]
  node [
    id 59
    label "sfragistyka"
  ]
  node [
    id 60
    label "zabytkoznawstwo"
  ]
  node [
    id 61
    label "historia_sztuki"
  ]
  node [
    id 62
    label "podoficer"
  ]
  node [
    id 63
    label "podchor&#261;&#380;y"
  ]
  node [
    id 64
    label "mundurowy"
  ]
  node [
    id 65
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 66
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 67
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 68
    label "happen"
  ]
  node [
    id 69
    label "pass"
  ]
  node [
    id 70
    label "przeby&#263;"
  ]
  node [
    id 71
    label "pique"
  ]
  node [
    id 72
    label "podlec"
  ]
  node [
    id 73
    label "zacz&#261;&#263;"
  ]
  node [
    id 74
    label "nasyci&#263;_si&#281;"
  ]
  node [
    id 75
    label "przerobi&#263;"
  ]
  node [
    id 76
    label "min&#261;&#263;"
  ]
  node [
    id 77
    label "przewy&#380;szy&#263;"
  ]
  node [
    id 78
    label "zmieni&#263;"
  ]
  node [
    id 79
    label "dozna&#263;"
  ]
  node [
    id 80
    label "die"
  ]
  node [
    id 81
    label "zapozna&#263;_si&#281;"
  ]
  node [
    id 82
    label "beat"
  ]
  node [
    id 83
    label "absorb"
  ]
  node [
    id 84
    label "przedosta&#263;_si&#281;"
  ]
  node [
    id 85
    label "zaliczy&#263;"
  ]
  node [
    id 86
    label "mienie"
  ]
  node [
    id 87
    label "ustawa"
  ]
  node [
    id 88
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 89
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 90
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 91
    label "przesta&#263;"
  ]
  node [
    id 92
    label "skr&#281;canie"
  ]
  node [
    id 93
    label "voice"
  ]
  node [
    id 94
    label "forma"
  ]
  node [
    id 95
    label "internet"
  ]
  node [
    id 96
    label "skr&#281;ci&#263;"
  ]
  node [
    id 97
    label "kartka"
  ]
  node [
    id 98
    label "orientowa&#263;"
  ]
  node [
    id 99
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 100
    label "powierzchnia"
  ]
  node [
    id 101
    label "plik"
  ]
  node [
    id 102
    label "bok"
  ]
  node [
    id 103
    label "pagina"
  ]
  node [
    id 104
    label "orientowanie"
  ]
  node [
    id 105
    label "fragment"
  ]
  node [
    id 106
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 107
    label "s&#261;d"
  ]
  node [
    id 108
    label "skr&#281;ca&#263;"
  ]
  node [
    id 109
    label "g&#243;ra"
  ]
  node [
    id 110
    label "serwis_internetowy"
  ]
  node [
    id 111
    label "orientacja"
  ]
  node [
    id 112
    label "linia"
  ]
  node [
    id 113
    label "skr&#281;cenie"
  ]
  node [
    id 114
    label "layout"
  ]
  node [
    id 115
    label "zorientowa&#263;"
  ]
  node [
    id 116
    label "zorientowanie"
  ]
  node [
    id 117
    label "obiekt"
  ]
  node [
    id 118
    label "podmiot"
  ]
  node [
    id 119
    label "ty&#322;"
  ]
  node [
    id 120
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 121
    label "logowanie"
  ]
  node [
    id 122
    label "adres_internetowy"
  ]
  node [
    id 123
    label "uj&#281;cie"
  ]
  node [
    id 124
    label "prz&#243;d"
  ]
  node [
    id 125
    label "posta&#263;"
  ]
  node [
    id 126
    label "s&#322;o&#324;ce"
  ]
  node [
    id 127
    label "usi&#322;owanie"
  ]
  node [
    id 128
    label "trud"
  ]
  node [
    id 129
    label "obszar"
  ]
  node [
    id 130
    label "sunset"
  ]
  node [
    id 131
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 132
    label "wiecz&#243;r"
  ]
  node [
    id 133
    label "strona_&#347;wiata"
  ]
  node [
    id 134
    label "pora"
  ]
  node [
    id 135
    label "zjawisko"
  ]
  node [
    id 136
    label "szar&#243;wka"
  ]
  node [
    id 137
    label "Suworowa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 8
    target 122
  ]
  edge [
    source 8
    target 123
  ]
  edge [
    source 8
    target 124
  ]
  edge [
    source 8
    target 125
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 129
  ]
  edge [
    source 9
    target 130
  ]
  edge [
    source 9
    target 131
  ]
  edge [
    source 9
    target 132
  ]
  edge [
    source 9
    target 133
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 134
  ]
  edge [
    source 9
    target 135
  ]
  edge [
    source 9
    target 136
  ]
]
