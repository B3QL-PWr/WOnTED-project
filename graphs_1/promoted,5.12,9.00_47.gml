graph [
  maxDegree 16
  minDegree 1
  meanDegree 1.9523809523809523
  density 0.047619047619047616
  graphCliqueNumber 2
  node [
    id 0
    label "rpo"
    origin "text"
  ]
  node [
    id 1
    label "analizowa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "dewiza"
    origin "text"
  ]
  node [
    id 3
    label "powinny"
    origin "text"
  ]
  node [
    id 4
    label "znale&#378;&#263;"
    origin "text"
  ]
  node [
    id 5
    label "si&#281;"
    origin "text"
  ]
  node [
    id 6
    label "dokument"
    origin "text"
  ]
  node [
    id 7
    label "badany"
  ]
  node [
    id 8
    label "consider"
  ]
  node [
    id 9
    label "poddawa&#263;"
  ]
  node [
    id 10
    label "bada&#263;"
  ]
  node [
    id 11
    label "rozpatrywa&#263;"
  ]
  node [
    id 12
    label "idea"
  ]
  node [
    id 13
    label "powiedzenie"
  ]
  node [
    id 14
    label "zasada"
  ]
  node [
    id 15
    label "nale&#380;ny"
  ]
  node [
    id 16
    label "odzyska&#263;"
  ]
  node [
    id 17
    label "devise"
  ]
  node [
    id 18
    label "oceni&#263;"
  ]
  node [
    id 19
    label "znaj&#347;&#263;"
  ]
  node [
    id 20
    label "wymy&#347;li&#263;"
  ]
  node [
    id 21
    label "invent"
  ]
  node [
    id 22
    label "pozyska&#263;"
  ]
  node [
    id 23
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 24
    label "wykry&#263;"
  ]
  node [
    id 25
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 26
    label "dozna&#263;"
  ]
  node [
    id 27
    label "record"
  ]
  node [
    id 28
    label "wytw&#243;r"
  ]
  node [
    id 29
    label "&#347;wiadectwo"
  ]
  node [
    id 30
    label "zapis"
  ]
  node [
    id 31
    label "fascyku&#322;"
  ]
  node [
    id 32
    label "raport&#243;wka"
  ]
  node [
    id 33
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 34
    label "artyku&#322;"
  ]
  node [
    id 35
    label "plik"
  ]
  node [
    id 36
    label "writing"
  ]
  node [
    id 37
    label "utw&#243;r"
  ]
  node [
    id 38
    label "dokumentacja"
  ]
  node [
    id 39
    label "parafa"
  ]
  node [
    id 40
    label "sygnatariusz"
  ]
  node [
    id 41
    label "registratura"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
]
