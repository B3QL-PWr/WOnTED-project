graph [
  maxDegree 597
  minDegree 1
  meanDegree 2.0499405469678953
  density 0.0024404054130570184
  graphCliqueNumber 3
  node [
    id 0
    label "uprawnienie"
    origin "text"
  ]
  node [
    id 1
    label "mieszkaniec"
    origin "text"
  ]
  node [
    id 2
    label "miasto"
    origin "text"
  ]
  node [
    id 3
    label "opole"
    origin "text"
  ]
  node [
    id 4
    label "organizacja"
    origin "text"
  ]
  node [
    id 5
    label "zakres"
    origin "text"
  ]
  node [
    id 6
    label "kierowa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "rad"
    origin "text"
  ]
  node [
    id 8
    label "wniosek"
    origin "text"
  ]
  node [
    id 9
    label "podejmowa&#263;"
    origin "text"
  ]
  node [
    id 10
    label "uchwa&#322;a"
    origin "text"
  ]
  node [
    id 11
    label "przedmiot"
    origin "text"
  ]
  node [
    id 12
    label "zastrzec"
    origin "text"
  ]
  node [
    id 13
    label "w&#322;a&#347;ciwo&#347;&#263;"
    origin "text"
  ]
  node [
    id 14
    label "inny"
    origin "text"
  ]
  node [
    id 15
    label "organ"
    origin "text"
  ]
  node [
    id 16
    label "podmiot"
    origin "text"
  ]
  node [
    id 17
    label "regulowa&#263;"
    origin "text"
  ]
  node [
    id 18
    label "xxvi"
    origin "text"
  ]
  node [
    id 19
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 20
    label "luty"
    origin "text"
  ]
  node [
    id 21
    label "rocznik"
    origin "text"
  ]
  node [
    id 22
    label "sprawa"
    origin "text"
  ]
  node [
    id 23
    label "uchwali&#263;"
    origin "text"
  ]
  node [
    id 24
    label "statut"
    origin "text"
  ]
  node [
    id 25
    label "p&#243;&#378;ny"
    origin "text"
  ]
  node [
    id 26
    label "zmiana"
    origin "text"
  ]
  node [
    id 27
    label "dokument"
  ]
  node [
    id 28
    label "czynno&#347;&#263;"
  ]
  node [
    id 29
    label "spowodowanie"
  ]
  node [
    id 30
    label "title"
  ]
  node [
    id 31
    label "law"
  ]
  node [
    id 32
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 33
    label "authorization"
  ]
  node [
    id 34
    label "cz&#322;owiek"
  ]
  node [
    id 35
    label "ludno&#347;&#263;"
  ]
  node [
    id 36
    label "zwierz&#281;"
  ]
  node [
    id 37
    label "Brac&#322;aw"
  ]
  node [
    id 38
    label "Zb&#261;szy&#324;"
  ]
  node [
    id 39
    label "G&#322;uch&#243;w"
  ]
  node [
    id 40
    label "Hallstatt"
  ]
  node [
    id 41
    label "Zbara&#380;"
  ]
  node [
    id 42
    label "Wielkie_Tyrnowo"
  ]
  node [
    id 43
    label "Nachiczewan"
  ]
  node [
    id 44
    label "Suworow"
  ]
  node [
    id 45
    label "Halicz"
  ]
  node [
    id 46
    label "Gandawa"
  ]
  node [
    id 47
    label "Kamionka_Strumi&#322;owa"
  ]
  node [
    id 48
    label "Wismar"
  ]
  node [
    id 49
    label "Norymberga"
  ]
  node [
    id 50
    label "Ruciane-Nida"
  ]
  node [
    id 51
    label "Wia&#378;ma"
  ]
  node [
    id 52
    label "Sewilla"
  ]
  node [
    id 53
    label "Pietropaw&#322;owsk"
  ]
  node [
    id 54
    label "Kobry&#324;"
  ]
  node [
    id 55
    label "Brno"
  ]
  node [
    id 56
    label "Tomsk"
  ]
  node [
    id 57
    label "Poniatowa"
  ]
  node [
    id 58
    label "Hadziacz"
  ]
  node [
    id 59
    label "Tiume&#324;"
  ]
  node [
    id 60
    label "Karlsbad"
  ]
  node [
    id 61
    label "Drohobycz"
  ]
  node [
    id 62
    label "Lyon"
  ]
  node [
    id 63
    label "Dzier&#380;y&#324;sk"
  ]
  node [
    id 64
    label "K&#322;odawa"
  ]
  node [
    id 65
    label "Solikamsk"
  ]
  node [
    id 66
    label "Wolgast"
  ]
  node [
    id 67
    label "Saloniki"
  ]
  node [
    id 68
    label "Lw&#243;w"
  ]
  node [
    id 69
    label "Al-Kufa"
  ]
  node [
    id 70
    label "Hamburg"
  ]
  node [
    id 71
    label "Bogusz&#243;w-Gorce"
  ]
  node [
    id 72
    label "Nampula"
  ]
  node [
    id 73
    label "burmistrz"
  ]
  node [
    id 74
    label "D&#252;sseldorf"
  ]
  node [
    id 75
    label "Nowy_Orlean"
  ]
  node [
    id 76
    label "Bamberg"
  ]
  node [
    id 77
    label "Osaka"
  ]
  node [
    id 78
    label "Michalovce"
  ]
  node [
    id 79
    label "Niko&#322;ajewsk_nad_Amurem"
  ]
  node [
    id 80
    label "Fryburg"
  ]
  node [
    id 81
    label "Trabzon"
  ]
  node [
    id 82
    label "Wersal"
  ]
  node [
    id 83
    label "Swatowe"
  ]
  node [
    id 84
    label "Ka&#322;uga"
  ]
  node [
    id 85
    label "Dijon"
  ]
  node [
    id 86
    label "Cannes"
  ]
  node [
    id 87
    label "Borowsk"
  ]
  node [
    id 88
    label "Kursk"
  ]
  node [
    id 89
    label "Tyberiada"
  ]
  node [
    id 90
    label "Boden"
  ]
  node [
    id 91
    label "Dodona"
  ]
  node [
    id 92
    label "Vukovar"
  ]
  node [
    id 93
    label "Soleczniki"
  ]
  node [
    id 94
    label "Barcelona"
  ]
  node [
    id 95
    label "Oszmiana"
  ]
  node [
    id 96
    label "Stuttgart"
  ]
  node [
    id 97
    label "Nerczy&#324;sk"
  ]
  node [
    id 98
    label "Essen"
  ]
  node [
    id 99
    label "Bijsk"
  ]
  node [
    id 100
    label "Luboml"
  ]
  node [
    id 101
    label "Gr&#243;dek"
  ]
  node [
    id 102
    label "Orany"
  ]
  node [
    id 103
    label "Siedliszcze"
  ]
  node [
    id 104
    label "P&#322;owdiw"
  ]
  node [
    id 105
    label "A&#322;apajewsk"
  ]
  node [
    id 106
    label "Liverpool"
  ]
  node [
    id 107
    label "Ostrawa"
  ]
  node [
    id 108
    label "Penza"
  ]
  node [
    id 109
    label "Rudki"
  ]
  node [
    id 110
    label "Aktobe"
  ]
  node [
    id 111
    label "I&#322;awka"
  ]
  node [
    id 112
    label "Tolkmicko"
  ]
  node [
    id 113
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 114
    label "Sajgon"
  ]
  node [
    id 115
    label "Windawa"
  ]
  node [
    id 116
    label "Weimar"
  ]
  node [
    id 117
    label "Jekaterynburg"
  ]
  node [
    id 118
    label "Lejda"
  ]
  node [
    id 119
    label "Cremona"
  ]
  node [
    id 120
    label "Wo&#322;godo&#324;sk"
  ]
  node [
    id 121
    label "Kordoba"
  ]
  node [
    id 122
    label "urz&#261;d"
  ]
  node [
    id 123
    label "&#321;ohojsk"
  ]
  node [
    id 124
    label "Kalmar"
  ]
  node [
    id 125
    label "Akerman"
  ]
  node [
    id 126
    label "Locarno"
  ]
  node [
    id 127
    label "Bych&#243;w"
  ]
  node [
    id 128
    label "Toledo"
  ]
  node [
    id 129
    label "Minusi&#324;sk"
  ]
  node [
    id 130
    label "Szk&#322;&#243;w"
  ]
  node [
    id 131
    label "Wenecja"
  ]
  node [
    id 132
    label "Bazylea"
  ]
  node [
    id 133
    label "Peszt"
  ]
  node [
    id 134
    label "Piza"
  ]
  node [
    id 135
    label "Tanger"
  ]
  node [
    id 136
    label "Krzywi&#324;"
  ]
  node [
    id 137
    label "Eger"
  ]
  node [
    id 138
    label "Bogus&#322;aw"
  ]
  node [
    id 139
    label "Taganrog"
  ]
  node [
    id 140
    label "Oksford"
  ]
  node [
    id 141
    label "Gwardiejsk"
  ]
  node [
    id 142
    label "Tyraspol"
  ]
  node [
    id 143
    label "Kleczew"
  ]
  node [
    id 144
    label "Nowa_D&#281;ba"
  ]
  node [
    id 145
    label "Wilejka"
  ]
  node [
    id 146
    label "Modena"
  ]
  node [
    id 147
    label "Demmin"
  ]
  node [
    id 148
    label "Houston"
  ]
  node [
    id 149
    label "Rydu&#322;towy"
  ]
  node [
    id 150
    label "Bordeaux"
  ]
  node [
    id 151
    label "Schmalkalden"
  ]
  node [
    id 152
    label "O&#322;omuniec"
  ]
  node [
    id 153
    label "Tuluza"
  ]
  node [
    id 154
    label "tramwaj"
  ]
  node [
    id 155
    label "Nantes"
  ]
  node [
    id 156
    label "Debreczyn"
  ]
  node [
    id 157
    label "Kowel"
  ]
  node [
    id 158
    label "Witnica"
  ]
  node [
    id 159
    label "Stalingrad"
  ]
  node [
    id 160
    label "Drezno"
  ]
  node [
    id 161
    label "Perejas&#322;aw"
  ]
  node [
    id 162
    label "Luksor"
  ]
  node [
    id 163
    label "Ostaszk&#243;w"
  ]
  node [
    id 164
    label "Gettysburg"
  ]
  node [
    id 165
    label "Trydent"
  ]
  node [
    id 166
    label "Poczdam"
  ]
  node [
    id 167
    label "Mesyna"
  ]
  node [
    id 168
    label "Krasnogorsk"
  ]
  node [
    id 169
    label "Kars"
  ]
  node [
    id 170
    label "Darmstadt"
  ]
  node [
    id 171
    label "Rzg&#243;w"
  ]
  node [
    id 172
    label "Kar&#322;owice"
  ]
  node [
    id 173
    label "Czeskie_Budziejowice"
  ]
  node [
    id 174
    label "Buda"
  ]
  node [
    id 175
    label "Monako"
  ]
  node [
    id 176
    label "Pardubice"
  ]
  node [
    id 177
    label "Pas&#322;&#281;k"
  ]
  node [
    id 178
    label "Fatima"
  ]
  node [
    id 179
    label "Wo&#322;oko&#322;amsk"
  ]
  node [
    id 180
    label "Bir&#380;e"
  ]
  node [
    id 181
    label "Wi&#322;komierz"
  ]
  node [
    id 182
    label "Opawa"
  ]
  node [
    id 183
    label "Mantua"
  ]
  node [
    id 184
    label "ulica"
  ]
  node [
    id 185
    label "Tarragona"
  ]
  node [
    id 186
    label "Antwerpia"
  ]
  node [
    id 187
    label "Asuan"
  ]
  node [
    id 188
    label "Korynt"
  ]
  node [
    id 189
    label "Armenia"
  ]
  node [
    id 190
    label "Budionnowsk"
  ]
  node [
    id 191
    label "Lengyel"
  ]
  node [
    id 192
    label "Betlejem"
  ]
  node [
    id 193
    label "Asy&#380;"
  ]
  node [
    id 194
    label "Batumi"
  ]
  node [
    id 195
    label "Paczk&#243;w"
  ]
  node [
    id 196
    label "Grenada"
  ]
  node [
    id 197
    label "Suczawa"
  ]
  node [
    id 198
    label "Nowogard"
  ]
  node [
    id 199
    label "Tyr"
  ]
  node [
    id 200
    label "Bria&#324;sk"
  ]
  node [
    id 201
    label "Bar"
  ]
  node [
    id 202
    label "Czerkiesk"
  ]
  node [
    id 203
    label "Ja&#322;ta"
  ]
  node [
    id 204
    label "Mo&#347;ciska"
  ]
  node [
    id 205
    label "Medyna"
  ]
  node [
    id 206
    label "Tartu"
  ]
  node [
    id 207
    label "Pemba"
  ]
  node [
    id 208
    label "Lipawa"
  ]
  node [
    id 209
    label "Tyl&#380;a"
  ]
  node [
    id 210
    label "Dayton"
  ]
  node [
    id 211
    label "Lipsk"
  ]
  node [
    id 212
    label "Rohatyn"
  ]
  node [
    id 213
    label "Peszawar"
  ]
  node [
    id 214
    label "Adrianopol"
  ]
  node [
    id 215
    label "Azow"
  ]
  node [
    id 216
    label "Iwano-Frankowsk"
  ]
  node [
    id 217
    label "Czarnobyl"
  ]
  node [
    id 218
    label "Rakoniewice"
  ]
  node [
    id 219
    label "Obuch&#243;w"
  ]
  node [
    id 220
    label "Orneta"
  ]
  node [
    id 221
    label "Koszyce"
  ]
  node [
    id 222
    label "Czeski_Cieszyn"
  ]
  node [
    id 223
    label "Zagorsk"
  ]
  node [
    id 224
    label "Nieder_Selters"
  ]
  node [
    id 225
    label "Ko&#322;omna"
  ]
  node [
    id 226
    label "Rost&#243;w"
  ]
  node [
    id 227
    label "Bolonia"
  ]
  node [
    id 228
    label "Rajgr&#243;d"
  ]
  node [
    id 229
    label "L&#252;neburg"
  ]
  node [
    id 230
    label "Brack"
  ]
  node [
    id 231
    label "Konstancja"
  ]
  node [
    id 232
    label "Koluszki"
  ]
  node [
    id 233
    label "Semipa&#322;aty&#324;sk"
  ]
  node [
    id 234
    label "Suez"
  ]
  node [
    id 235
    label "Mrocza"
  ]
  node [
    id 236
    label "Triest"
  ]
  node [
    id 237
    label "Murma&#324;sk"
  ]
  node [
    id 238
    label "Tu&#322;a"
  ]
  node [
    id 239
    label "Tarnogr&#243;d"
  ]
  node [
    id 240
    label "Radziech&#243;w"
  ]
  node [
    id 241
    label "Kokand"
  ]
  node [
    id 242
    label "Kircholm"
  ]
  node [
    id 243
    label "Nowa_Ruda"
  ]
  node [
    id 244
    label "Huma&#324;"
  ]
  node [
    id 245
    label "Turkiestan"
  ]
  node [
    id 246
    label "Kani&#243;w"
  ]
  node [
    id 247
    label "Pilzno"
  ]
  node [
    id 248
    label "Korfant&#243;w"
  ]
  node [
    id 249
    label "Dubno"
  ]
  node [
    id 250
    label "Bras&#322;aw"
  ]
  node [
    id 251
    label "Choroszcz"
  ]
  node [
    id 252
    label "Nowogr&#243;d"
  ]
  node [
    id 253
    label "Konotop"
  ]
  node [
    id 254
    label "M&#347;cis&#322;aw"
  ]
  node [
    id 255
    label "Jastarnia"
  ]
  node [
    id 256
    label "Ba&#324;ska_Bystrzyca"
  ]
  node [
    id 257
    label "Omsk"
  ]
  node [
    id 258
    label "Troick"
  ]
  node [
    id 259
    label "Koper"
  ]
  node [
    id 260
    label "Jenisejsk"
  ]
  node [
    id 261
    label "Frankfurt_nad_Odr&#261;"
  ]
  node [
    id 262
    label "Oldenburg_in_Holstein"
  ]
  node [
    id 263
    label "Trenczyn"
  ]
  node [
    id 264
    label "Wormacja"
  ]
  node [
    id 265
    label "Wagram"
  ]
  node [
    id 266
    label "Lubeka"
  ]
  node [
    id 267
    label "Genewa"
  ]
  node [
    id 268
    label "&#379;ydacz&#243;w"
  ]
  node [
    id 269
    label "Kleck"
  ]
  node [
    id 270
    label "G&#243;ra_Kalwaria"
  ]
  node [
    id 271
    label "Struga"
  ]
  node [
    id 272
    label "Izbica_Kujawska"
  ]
  node [
    id 273
    label "Stalinogorsk"
  ]
  node [
    id 274
    label "Izmir"
  ]
  node [
    id 275
    label "Dortmund"
  ]
  node [
    id 276
    label "Workuta"
  ]
  node [
    id 277
    label "Jerycho"
  ]
  node [
    id 278
    label "Brunszwik"
  ]
  node [
    id 279
    label "Aleksandria"
  ]
  node [
    id 280
    label "miejscowo&#347;&#263;"
  ]
  node [
    id 281
    label "Borys&#322;aw"
  ]
  node [
    id 282
    label "Zaleszczyki"
  ]
  node [
    id 283
    label "Z&#322;oczew"
  ]
  node [
    id 284
    label "Piast&#243;w"
  ]
  node [
    id 285
    label "Frankfurt_nad_Menem"
  ]
  node [
    id 286
    label "Bor"
  ]
  node [
    id 287
    label "Nazaret"
  ]
  node [
    id 288
    label "Sarat&#243;w"
  ]
  node [
    id 289
    label "Brasz&#243;w"
  ]
  node [
    id 290
    label "Malin"
  ]
  node [
    id 291
    label "Parma"
  ]
  node [
    id 292
    label "Wierchoja&#324;sk"
  ]
  node [
    id 293
    label "Tarent"
  ]
  node [
    id 294
    label "Mariampol"
  ]
  node [
    id 295
    label "Wuhan"
  ]
  node [
    id 296
    label "Split"
  ]
  node [
    id 297
    label "Baranowicze"
  ]
  node [
    id 298
    label "Marki"
  ]
  node [
    id 299
    label "Adana"
  ]
  node [
    id 300
    label "B&#322;aszki"
  ]
  node [
    id 301
    label "Lubecz"
  ]
  node [
    id 302
    label "Sulech&#243;w"
  ]
  node [
    id 303
    label "Borys&#243;w"
  ]
  node [
    id 304
    label "Homel"
  ]
  node [
    id 305
    label "Tours"
  ]
  node [
    id 306
    label "Zaporo&#380;e"
  ]
  node [
    id 307
    label "Edam"
  ]
  node [
    id 308
    label "Kamieniec_Podolski"
  ]
  node [
    id 309
    label "Miedwie&#380;jegorsk"
  ]
  node [
    id 310
    label "Konstantynopol"
  ]
  node [
    id 311
    label "Chocim"
  ]
  node [
    id 312
    label "Mohylew"
  ]
  node [
    id 313
    label "Merseburg"
  ]
  node [
    id 314
    label "Kapsztad"
  ]
  node [
    id 315
    label "Sambor"
  ]
  node [
    id 316
    label "Manchester"
  ]
  node [
    id 317
    label "Pi&#324;sk"
  ]
  node [
    id 318
    label "Ochryda"
  ]
  node [
    id 319
    label "Rybi&#324;sk"
  ]
  node [
    id 320
    label "Czadca"
  ]
  node [
    id 321
    label "Orenburg"
  ]
  node [
    id 322
    label "Krajowa"
  ]
  node [
    id 323
    label "Eleusis"
  ]
  node [
    id 324
    label "Awinion"
  ]
  node [
    id 325
    label "Rzeczyca"
  ]
  node [
    id 326
    label "Lozanna"
  ]
  node [
    id 327
    label "Barczewo"
  ]
  node [
    id 328
    label "&#379;migr&#243;d"
  ]
  node [
    id 329
    label "Chabarowsk"
  ]
  node [
    id 330
    label "Jena"
  ]
  node [
    id 331
    label "Xai-Xai"
  ]
  node [
    id 332
    label "Radk&#243;w"
  ]
  node [
    id 333
    label "Syrakuzy"
  ]
  node [
    id 334
    label "Zas&#322;aw"
  ]
  node [
    id 335
    label "Windsor"
  ]
  node [
    id 336
    label "Getynga"
  ]
  node [
    id 337
    label "Carrara"
  ]
  node [
    id 338
    label "Madras"
  ]
  node [
    id 339
    label "Nitra"
  ]
  node [
    id 340
    label "Kilonia"
  ]
  node [
    id 341
    label "Rawenna"
  ]
  node [
    id 342
    label "Stawropol"
  ]
  node [
    id 343
    label "Warna"
  ]
  node [
    id 344
    label "Ba&#322;tijsk"
  ]
  node [
    id 345
    label "Cumana"
  ]
  node [
    id 346
    label "Kostroma"
  ]
  node [
    id 347
    label "Bajonna"
  ]
  node [
    id 348
    label "Magadan"
  ]
  node [
    id 349
    label "Kercz"
  ]
  node [
    id 350
    label "Harbin"
  ]
  node [
    id 351
    label "Sankt_Florian"
  ]
  node [
    id 352
    label "Bia&#322;ogr&#243;d_nad_Dniestrem"
  ]
  node [
    id 353
    label "Wo&#322;kowysk"
  ]
  node [
    id 354
    label "Norak"
  ]
  node [
    id 355
    label "S&#232;vres"
  ]
  node [
    id 356
    label "Barwice"
  ]
  node [
    id 357
    label "Sumy"
  ]
  node [
    id 358
    label "Jutrosin"
  ]
  node [
    id 359
    label "Canterbury"
  ]
  node [
    id 360
    label "Czerkasy"
  ]
  node [
    id 361
    label "Troki"
  ]
  node [
    id 362
    label "Dnieprodzier&#380;y&#324;sk"
  ]
  node [
    id 363
    label "Turka"
  ]
  node [
    id 364
    label "Budziszyn"
  ]
  node [
    id 365
    label "A&#322;czewsk"
  ]
  node [
    id 366
    label "Chark&#243;w"
  ]
  node [
    id 367
    label "Go&#347;cino"
  ]
  node [
    id 368
    label "Ku&#378;nieck"
  ]
  node [
    id 369
    label "Wotki&#324;sk"
  ]
  node [
    id 370
    label "Symferopol"
  ]
  node [
    id 371
    label "Dmitrow"
  ]
  node [
    id 372
    label "Cherso&#324;"
  ]
  node [
    id 373
    label "zabudowa"
  ]
  node [
    id 374
    label "Orlean"
  ]
  node [
    id 375
    label "Nowogr&#243;dek"
  ]
  node [
    id 376
    label "Ku&#378;nia_Raciborska"
  ]
  node [
    id 377
    label "Berdia&#324;sk"
  ]
  node [
    id 378
    label "Szumsk"
  ]
  node [
    id 379
    label "Z&#322;ocz&#243;w"
  ]
  node [
    id 380
    label "Orsza"
  ]
  node [
    id 381
    label "Cluny"
  ]
  node [
    id 382
    label "Aralsk"
  ]
  node [
    id 383
    label "Stoczek_&#321;ukowski"
  ]
  node [
    id 384
    label "Bogumin"
  ]
  node [
    id 385
    label "Antiochia"
  ]
  node [
    id 386
    label "grupa"
  ]
  node [
    id 387
    label "Inhambane"
  ]
  node [
    id 388
    label "S&#322;awia&#324;sk_nad_Kubaniem"
  ]
  node [
    id 389
    label "Wielki_Pres&#322;aw"
  ]
  node [
    id 390
    label "Trewir"
  ]
  node [
    id 391
    label "Nowogr&#243;d_Wielki"
  ]
  node [
    id 392
    label "Siewieromorsk"
  ]
  node [
    id 393
    label "Calais"
  ]
  node [
    id 394
    label "Twer"
  ]
  node [
    id 395
    label "&#379;ytawa"
  ]
  node [
    id 396
    label "Eupatoria"
  ]
  node [
    id 397
    label "Stara_Zagora"
  ]
  node [
    id 398
    label "Jastrowie"
  ]
  node [
    id 399
    label "Piatigorsk"
  ]
  node [
    id 400
    label "&#379;&#243;&#322;kiew"
  ]
  node [
    id 401
    label "Le&#324;sk"
  ]
  node [
    id 402
    label "Johannesburg"
  ]
  node [
    id 403
    label "Kaszyn"
  ]
  node [
    id 404
    label "&#346;wi&#281;ciany"
  ]
  node [
    id 405
    label "&#379;ylina"
  ]
  node [
    id 406
    label "Sewastopol"
  ]
  node [
    id 407
    label "Pietrozawodsk"
  ]
  node [
    id 408
    label "Bobolice"
  ]
  node [
    id 409
    label "Mosty"
  ]
  node [
    id 410
    label "Korsu&#324;_Szewczenkowski"
  ]
  node [
    id 411
    label "Karaganda"
  ]
  node [
    id 412
    label "Marsylia"
  ]
  node [
    id 413
    label "Buchara"
  ]
  node [
    id 414
    label "Dubrownik"
  ]
  node [
    id 415
    label "Be&#322;z"
  ]
  node [
    id 416
    label "Oran"
  ]
  node [
    id 417
    label "Regensburg"
  ]
  node [
    id 418
    label "Rotterdam"
  ]
  node [
    id 419
    label "Trembowla"
  ]
  node [
    id 420
    label "Woskriesiensk"
  ]
  node [
    id 421
    label "Po&#322;ock"
  ]
  node [
    id 422
    label "Poprad"
  ]
  node [
    id 423
    label "Kronsztad"
  ]
  node [
    id 424
    label "Los_Angeles"
  ]
  node [
    id 425
    label "U&#322;an_Ude"
  ]
  node [
    id 426
    label "U&#347;cie_nad_&#321;ab&#261;"
  ]
  node [
    id 427
    label "W&#322;adywostok"
  ]
  node [
    id 428
    label "Kandahar"
  ]
  node [
    id 429
    label "Tobolsk"
  ]
  node [
    id 430
    label "Boston"
  ]
  node [
    id 431
    label "Hawana"
  ]
  node [
    id 432
    label "Kis&#322;owodzk"
  ]
  node [
    id 433
    label "Tulon"
  ]
  node [
    id 434
    label "Utrecht"
  ]
  node [
    id 435
    label "Oleszyce"
  ]
  node [
    id 436
    label "Hradec_Kr&#225;lov&#233;"
  ]
  node [
    id 437
    label "Katania"
  ]
  node [
    id 438
    label "Teby"
  ]
  node [
    id 439
    label "Paw&#322;owo"
  ]
  node [
    id 440
    label "W&#252;rzburg"
  ]
  node [
    id 441
    label "Podiebrady"
  ]
  node [
    id 442
    label "Uppsala"
  ]
  node [
    id 443
    label "Poniewie&#380;"
  ]
  node [
    id 444
    label "Niko&#322;ajewsk"
  ]
  node [
    id 445
    label "Aczy&#324;sk"
  ]
  node [
    id 446
    label "Berezyna"
  ]
  node [
    id 447
    label "Ostr&#243;g"
  ]
  node [
    id 448
    label "Brze&#347;&#263;"
  ]
  node [
    id 449
    label "Lancaster"
  ]
  node [
    id 450
    label "Stryj"
  ]
  node [
    id 451
    label "Kozielsk"
  ]
  node [
    id 452
    label "Loreto"
  ]
  node [
    id 453
    label "B&#322;agowieszcze&#324;sk"
  ]
  node [
    id 454
    label "Hebron"
  ]
  node [
    id 455
    label "Kaspijsk"
  ]
  node [
    id 456
    label "Peczora"
  ]
  node [
    id 457
    label "Isfahan"
  ]
  node [
    id 458
    label "Chimoio"
  ]
  node [
    id 459
    label "Mory&#324;"
  ]
  node [
    id 460
    label "Kowno"
  ]
  node [
    id 461
    label "Nie&#347;wie&#380;"
  ]
  node [
    id 462
    label "Opalenica"
  ]
  node [
    id 463
    label "Kolonia"
  ]
  node [
    id 464
    label "Stary_Sambor"
  ]
  node [
    id 465
    label "Kolkata"
  ]
  node [
    id 466
    label "Turkmenbaszy"
  ]
  node [
    id 467
    label "Mohyl&#243;w_Podolski"
  ]
  node [
    id 468
    label "Nankin"
  ]
  node [
    id 469
    label "Krzanowice"
  ]
  node [
    id 470
    label "Efez"
  ]
  node [
    id 471
    label "Dobrodzie&#324;"
  ]
  node [
    id 472
    label "Neapol"
  ]
  node [
    id 473
    label "S&#322;uck"
  ]
  node [
    id 474
    label "Fryburg_Bryzgowijski"
  ]
  node [
    id 475
    label "W&#322;odzimierz_Wo&#322;y&#324;ski"
  ]
  node [
    id 476
    label "Frydek-Mistek"
  ]
  node [
    id 477
    label "Korsze"
  ]
  node [
    id 478
    label "T&#322;uszcz"
  ]
  node [
    id 479
    label "Soligorsk"
  ]
  node [
    id 480
    label "Kie&#380;mark"
  ]
  node [
    id 481
    label "Mannheim"
  ]
  node [
    id 482
    label "Ulm"
  ]
  node [
    id 483
    label "Podhajce"
  ]
  node [
    id 484
    label "Dniepropetrowsk"
  ]
  node [
    id 485
    label "Szamocin"
  ]
  node [
    id 486
    label "Ko&#322;omyja"
  ]
  node [
    id 487
    label "Buczacz"
  ]
  node [
    id 488
    label "M&#252;nster"
  ]
  node [
    id 489
    label "Brema"
  ]
  node [
    id 490
    label "Delhi"
  ]
  node [
    id 491
    label "&#346;niatyn"
  ]
  node [
    id 492
    label "Nicea"
  ]
  node [
    id 493
    label "Szawle"
  ]
  node [
    id 494
    label "Czerniowce"
  ]
  node [
    id 495
    label "Mi&#347;nia"
  ]
  node [
    id 496
    label "Sydney"
  ]
  node [
    id 497
    label "Moguncja"
  ]
  node [
    id 498
    label "Narbona"
  ]
  node [
    id 499
    label "Swiet&#322;ogorsk"
  ]
  node [
    id 500
    label "Wittenberga"
  ]
  node [
    id 501
    label "Uljanowsk"
  ]
  node [
    id 502
    label "&#321;uga&#324;sk"
  ]
  node [
    id 503
    label "Wyborg"
  ]
  node [
    id 504
    label "Trojan"
  ]
  node [
    id 505
    label "Kalwaria_Zebrzydowska"
  ]
  node [
    id 506
    label "Brandenburg"
  ]
  node [
    id 507
    label "Kemerowo"
  ]
  node [
    id 508
    label "Kaszgar"
  ]
  node [
    id 509
    label "Lenzen"
  ]
  node [
    id 510
    label "Nanning"
  ]
  node [
    id 511
    label "Gotha"
  ]
  node [
    id 512
    label "Zurych"
  ]
  node [
    id 513
    label "Baltimore"
  ]
  node [
    id 514
    label "&#321;uck"
  ]
  node [
    id 515
    label "Bristol"
  ]
  node [
    id 516
    label "Ferrara"
  ]
  node [
    id 517
    label "Mariupol"
  ]
  node [
    id 518
    label "przestrze&#324;_publiczna"
  ]
  node [
    id 519
    label "Lhasa"
  ]
  node [
    id 520
    label "Czerniejewo"
  ]
  node [
    id 521
    label "Filadelfia"
  ]
  node [
    id 522
    label "Kanton"
  ]
  node [
    id 523
    label "Milan&#243;wek"
  ]
  node [
    id 524
    label "Perwomajsk"
  ]
  node [
    id 525
    label "Nieftiegorsk"
  ]
  node [
    id 526
    label "Pittsburgh"
  ]
  node [
    id 527
    label "Greifswald"
  ]
  node [
    id 528
    label "Akwileja"
  ]
  node [
    id 529
    label "Norfolk"
  ]
  node [
    id 530
    label "Perm"
  ]
  node [
    id 531
    label "Detroit"
  ]
  node [
    id 532
    label "Fergana"
  ]
  node [
    id 533
    label "Starobielsk"
  ]
  node [
    id 534
    label "Wielsk"
  ]
  node [
    id 535
    label "Zaklik&#243;w"
  ]
  node [
    id 536
    label "Majsur"
  ]
  node [
    id 537
    label "Narwa"
  ]
  node [
    id 538
    label "Chicago"
  ]
  node [
    id 539
    label "Byczyna"
  ]
  node [
    id 540
    label "Mozyrz"
  ]
  node [
    id 541
    label "Konstantyn&#243;wka"
  ]
  node [
    id 542
    label "Miko&#322;aj&#243;w"
  ]
  node [
    id 543
    label "Megara"
  ]
  node [
    id 544
    label "Stralsund"
  ]
  node [
    id 545
    label "Wo&#322;gograd"
  ]
  node [
    id 546
    label "Lichinga"
  ]
  node [
    id 547
    label "Haga"
  ]
  node [
    id 548
    label "Tarnopol"
  ]
  node [
    id 549
    label "K&#322;ajpeda"
  ]
  node [
    id 550
    label "Nowomoskowsk"
  ]
  node [
    id 551
    label "Ussuryjsk"
  ]
  node [
    id 552
    label "Brugia"
  ]
  node [
    id 553
    label "Natal"
  ]
  node [
    id 554
    label "Kro&#347;niewice"
  ]
  node [
    id 555
    label "Edynburg"
  ]
  node [
    id 556
    label "Marburg"
  ]
  node [
    id 557
    label "&#346;wiebodzice"
  ]
  node [
    id 558
    label "S&#322;onim"
  ]
  node [
    id 559
    label "Dalton"
  ]
  node [
    id 560
    label "Smorgonie"
  ]
  node [
    id 561
    label "Orze&#322;"
  ]
  node [
    id 562
    label "Nowoku&#378;nieck"
  ]
  node [
    id 563
    label "Zadar"
  ]
  node [
    id 564
    label "Tel_Awiw-Jafa"
  ]
  node [
    id 565
    label "Koprzywnica"
  ]
  node [
    id 566
    label "Angarsk"
  ]
  node [
    id 567
    label "Mo&#380;ajsk"
  ]
  node [
    id 568
    label "Akwizgran"
  ]
  node [
    id 569
    label "Norylsk"
  ]
  node [
    id 570
    label "Jawor&#243;w"
  ]
  node [
    id 571
    label "weduta"
  ]
  node [
    id 572
    label "Jab&#322;onk&#243;w"
  ]
  node [
    id 573
    label "Suzdal"
  ]
  node [
    id 574
    label "W&#322;odzimierz"
  ]
  node [
    id 575
    label "Bujnaksk"
  ]
  node [
    id 576
    label "Beresteczko"
  ]
  node [
    id 577
    label "Strzelno"
  ]
  node [
    id 578
    label "Siewsk"
  ]
  node [
    id 579
    label "Cymlansk"
  ]
  node [
    id 580
    label "Trzyniec"
  ]
  node [
    id 581
    label "Hanower"
  ]
  node [
    id 582
    label "Wuppertal"
  ]
  node [
    id 583
    label "Sura&#380;"
  ]
  node [
    id 584
    label "Winchester"
  ]
  node [
    id 585
    label "Samara"
  ]
  node [
    id 586
    label "Sydon"
  ]
  node [
    id 587
    label "Krasnodar"
  ]
  node [
    id 588
    label "Worone&#380;"
  ]
  node [
    id 589
    label "Paw&#322;odar"
  ]
  node [
    id 590
    label "Czelabi&#324;sk"
  ]
  node [
    id 591
    label "Reda"
  ]
  node [
    id 592
    label "Karwina"
  ]
  node [
    id 593
    label "Wyszehrad"
  ]
  node [
    id 594
    label "Sara&#324;sk"
  ]
  node [
    id 595
    label "Koby&#322;ka"
  ]
  node [
    id 596
    label "Winnica"
  ]
  node [
    id 597
    label "Tambow"
  ]
  node [
    id 598
    label "Pyskowice"
  ]
  node [
    id 599
    label "Heidelberg"
  ]
  node [
    id 600
    label "Maribor"
  ]
  node [
    id 601
    label "Werona"
  ]
  node [
    id 602
    label "G&#322;uszyca"
  ]
  node [
    id 603
    label "Rostock"
  ]
  node [
    id 604
    label "Mekka"
  ]
  node [
    id 605
    label "Liberec"
  ]
  node [
    id 606
    label "Bie&#322;gorod"
  ]
  node [
    id 607
    label "Berdycz&#243;w"
  ]
  node [
    id 608
    label "Sierdobsk"
  ]
  node [
    id 609
    label "Bobrujsk"
  ]
  node [
    id 610
    label "Padwa"
  ]
  node [
    id 611
    label "Pasawa"
  ]
  node [
    id 612
    label "Chanty-Mansyjsk"
  ]
  node [
    id 613
    label "&#379;ar&#243;w"
  ]
  node [
    id 614
    label "Poczaj&#243;w"
  ]
  node [
    id 615
    label "Barabi&#324;sk"
  ]
  node [
    id 616
    label "Gorycja"
  ]
  node [
    id 617
    label "Haarlem"
  ]
  node [
    id 618
    label "Kiejdany"
  ]
  node [
    id 619
    label "Chmielnicki"
  ]
  node [
    id 620
    label "Magnitogorsk"
  ]
  node [
    id 621
    label "Burgas"
  ]
  node [
    id 622
    label "Siena"
  ]
  node [
    id 623
    label "Korzec"
  ]
  node [
    id 624
    label "Bonn"
  ]
  node [
    id 625
    label "Pr&#243;szk&#243;w"
  ]
  node [
    id 626
    label "Walencja"
  ]
  node [
    id 627
    label "Mosina"
  ]
  node [
    id 628
    label "wsp&#243;lnota"
  ]
  node [
    id 629
    label "jednostka"
  ]
  node [
    id 630
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 631
    label "endecki"
  ]
  node [
    id 632
    label "komitet_koordynacyjny"
  ]
  node [
    id 633
    label "przybud&#243;wka"
  ]
  node [
    id 634
    label "ZOMO"
  ]
  node [
    id 635
    label "boj&#243;wka"
  ]
  node [
    id 636
    label "zesp&#243;&#322;"
  ]
  node [
    id 637
    label "organization"
  ]
  node [
    id 638
    label "TOPR"
  ]
  node [
    id 639
    label "jednostka_organizacyjna"
  ]
  node [
    id 640
    label "przedstawicielstwo"
  ]
  node [
    id 641
    label "Cepelia"
  ]
  node [
    id 642
    label "GOPR"
  ]
  node [
    id 643
    label "ZMP"
  ]
  node [
    id 644
    label "ZBoWiD"
  ]
  node [
    id 645
    label "struktura"
  ]
  node [
    id 646
    label "od&#322;am"
  ]
  node [
    id 647
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 648
    label "centrala"
  ]
  node [
    id 649
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 650
    label "granica"
  ]
  node [
    id 651
    label "circle"
  ]
  node [
    id 652
    label "podzakres"
  ]
  node [
    id 653
    label "zbi&#243;r"
  ]
  node [
    id 654
    label "dziedzina"
  ]
  node [
    id 655
    label "desygnat"
  ]
  node [
    id 656
    label "sfera"
  ]
  node [
    id 657
    label "wielko&#347;&#263;"
  ]
  node [
    id 658
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 659
    label "control"
  ]
  node [
    id 660
    label "ustawia&#263;"
  ]
  node [
    id 661
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 662
    label "motywowa&#263;"
  ]
  node [
    id 663
    label "order"
  ]
  node [
    id 664
    label "administrowa&#263;"
  ]
  node [
    id 665
    label "manipulate"
  ]
  node [
    id 666
    label "give"
  ]
  node [
    id 667
    label "indicate"
  ]
  node [
    id 668
    label "przeznacza&#263;"
  ]
  node [
    id 669
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 670
    label "match"
  ]
  node [
    id 671
    label "sterowa&#263;"
  ]
  node [
    id 672
    label "wysy&#322;a&#263;"
  ]
  node [
    id 673
    label "zwierzchnik"
  ]
  node [
    id 674
    label "zarz&#261;dza&#263;"
  ]
  node [
    id 675
    label "berylowiec"
  ]
  node [
    id 676
    label "u&#347;miechni&#281;ty"
  ]
  node [
    id 677
    label "pierwiastek_promieniotw&#243;rczy"
  ]
  node [
    id 678
    label "mikroradian"
  ]
  node [
    id 679
    label "zadowolenie_si&#281;"
  ]
  node [
    id 680
    label "usatysfakcjonowanie_si&#281;"
  ]
  node [
    id 681
    label "content"
  ]
  node [
    id 682
    label "jednostka_promieniowania"
  ]
  node [
    id 683
    label "miliradian"
  ]
  node [
    id 684
    label "twierdzenie"
  ]
  node [
    id 685
    label "my&#347;l"
  ]
  node [
    id 686
    label "wnioskowanie"
  ]
  node [
    id 687
    label "propozycja"
  ]
  node [
    id 688
    label "motion"
  ]
  node [
    id 689
    label "pismo"
  ]
  node [
    id 690
    label "prayer"
  ]
  node [
    id 691
    label "zmienia&#263;"
  ]
  node [
    id 692
    label "reagowa&#263;"
  ]
  node [
    id 693
    label "rise"
  ]
  node [
    id 694
    label "admit"
  ]
  node [
    id 695
    label "drive"
  ]
  node [
    id 696
    label "robi&#263;"
  ]
  node [
    id 697
    label "draw"
  ]
  node [
    id 698
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 699
    label "podnosi&#263;"
  ]
  node [
    id 700
    label "resolution"
  ]
  node [
    id 701
    label "akt"
  ]
  node [
    id 702
    label "robienie"
  ]
  node [
    id 703
    label "kr&#261;&#380;enie"
  ]
  node [
    id 704
    label "rzecz"
  ]
  node [
    id 705
    label "zbacza&#263;"
  ]
  node [
    id 706
    label "entity"
  ]
  node [
    id 707
    label "element"
  ]
  node [
    id 708
    label "omawia&#263;"
  ]
  node [
    id 709
    label "om&#243;wi&#263;"
  ]
  node [
    id 710
    label "sponiewiera&#263;"
  ]
  node [
    id 711
    label "sponiewieranie"
  ]
  node [
    id 712
    label "omawianie"
  ]
  node [
    id 713
    label "charakter"
  ]
  node [
    id 714
    label "program_nauczania"
  ]
  node [
    id 715
    label "w&#261;tek"
  ]
  node [
    id 716
    label "thing"
  ]
  node [
    id 717
    label "zboczenie"
  ]
  node [
    id 718
    label "zbaczanie"
  ]
  node [
    id 719
    label "tre&#347;&#263;"
  ]
  node [
    id 720
    label "tematyka"
  ]
  node [
    id 721
    label "istota"
  ]
  node [
    id 722
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 723
    label "kultura"
  ]
  node [
    id 724
    label "zboczy&#263;"
  ]
  node [
    id 725
    label "discipline"
  ]
  node [
    id 726
    label "om&#243;wienie"
  ]
  node [
    id 727
    label "wym&#243;wi&#263;"
  ]
  node [
    id 728
    label "uprzedzi&#263;"
  ]
  node [
    id 729
    label "condition"
  ]
  node [
    id 730
    label "zapewni&#263;"
  ]
  node [
    id 731
    label "wyregulowanie"
  ]
  node [
    id 732
    label "warto&#347;&#263;"
  ]
  node [
    id 733
    label "charakterystyka"
  ]
  node [
    id 734
    label "kompetencja"
  ]
  node [
    id 735
    label "cecha"
  ]
  node [
    id 736
    label "regulowanie"
  ]
  node [
    id 737
    label "feature"
  ]
  node [
    id 738
    label "wyregulowa&#263;"
  ]
  node [
    id 739
    label "standard"
  ]
  node [
    id 740
    label "attribute"
  ]
  node [
    id 741
    label "zaleta"
  ]
  node [
    id 742
    label "kolejny"
  ]
  node [
    id 743
    label "inaczej"
  ]
  node [
    id 744
    label "r&#243;&#380;ny"
  ]
  node [
    id 745
    label "inszy"
  ]
  node [
    id 746
    label "osobno"
  ]
  node [
    id 747
    label "uk&#322;ad"
  ]
  node [
    id 748
    label "organogeneza"
  ]
  node [
    id 749
    label "Komitet_Region&#243;w"
  ]
  node [
    id 750
    label "Izba_Konsyliarska"
  ]
  node [
    id 751
    label "budowa"
  ]
  node [
    id 752
    label "okolica"
  ]
  node [
    id 753
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 754
    label "dekortykacja"
  ]
  node [
    id 755
    label "struktura_anatomiczna"
  ]
  node [
    id 756
    label "tkanka"
  ]
  node [
    id 757
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 758
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 759
    label "stomia"
  ]
  node [
    id 760
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 761
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 762
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 763
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 764
    label "tw&#243;r"
  ]
  node [
    id 765
    label "byt"
  ]
  node [
    id 766
    label "prawo"
  ]
  node [
    id 767
    label "nauka_prawa"
  ]
  node [
    id 768
    label "osobowo&#347;&#263;"
  ]
  node [
    id 769
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 770
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 771
    label "nastawia&#263;"
  ]
  node [
    id 772
    label "tune"
  ]
  node [
    id 773
    label "normalize"
  ]
  node [
    id 774
    label "proces_fizjologiczny"
  ]
  node [
    id 775
    label "ulepsza&#263;"
  ]
  node [
    id 776
    label "op&#322;aca&#263;"
  ]
  node [
    id 777
    label "usprawnia&#263;"
  ]
  node [
    id 778
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 779
    label "s&#322;o&#324;ce"
  ]
  node [
    id 780
    label "czynienie_si&#281;"
  ]
  node [
    id 781
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 782
    label "czas"
  ]
  node [
    id 783
    label "long_time"
  ]
  node [
    id 784
    label "przedpo&#322;udnie"
  ]
  node [
    id 785
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 786
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 787
    label "tydzie&#324;"
  ]
  node [
    id 788
    label "godzina"
  ]
  node [
    id 789
    label "t&#322;usty_czwartek"
  ]
  node [
    id 790
    label "wsta&#263;"
  ]
  node [
    id 791
    label "day"
  ]
  node [
    id 792
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 793
    label "przedwiecz&#243;r"
  ]
  node [
    id 794
    label "Sylwester"
  ]
  node [
    id 795
    label "po&#322;udnie"
  ]
  node [
    id 796
    label "wzej&#347;cie"
  ]
  node [
    id 797
    label "podwiecz&#243;r"
  ]
  node [
    id 798
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 799
    label "rano"
  ]
  node [
    id 800
    label "termin"
  ]
  node [
    id 801
    label "ranek"
  ]
  node [
    id 802
    label "doba"
  ]
  node [
    id 803
    label "wiecz&#243;r"
  ]
  node [
    id 804
    label "walentynki"
  ]
  node [
    id 805
    label "popo&#322;udnie"
  ]
  node [
    id 806
    label "noc"
  ]
  node [
    id 807
    label "wstanie"
  ]
  node [
    id 808
    label "miesi&#261;c"
  ]
  node [
    id 809
    label "Matka_Boska_Gromniczna"
  ]
  node [
    id 810
    label "formacja"
  ]
  node [
    id 811
    label "kronika"
  ]
  node [
    id 812
    label "czasopismo"
  ]
  node [
    id 813
    label "yearbook"
  ]
  node [
    id 814
    label "temat"
  ]
  node [
    id 815
    label "kognicja"
  ]
  node [
    id 816
    label "idea"
  ]
  node [
    id 817
    label "szczeg&#243;&#322;"
  ]
  node [
    id 818
    label "wydarzenie"
  ]
  node [
    id 819
    label "przes&#322;anka"
  ]
  node [
    id 820
    label "rozprawa"
  ]
  node [
    id 821
    label "object"
  ]
  node [
    id 822
    label "proposition"
  ]
  node [
    id 823
    label "ustanowi&#263;"
  ]
  node [
    id 824
    label "resoluteness"
  ]
  node [
    id 825
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 826
    label "p&#243;&#378;no"
  ]
  node [
    id 827
    label "do_p&#243;&#378;na"
  ]
  node [
    id 828
    label "anatomopatolog"
  ]
  node [
    id 829
    label "rewizja"
  ]
  node [
    id 830
    label "oznaka"
  ]
  node [
    id 831
    label "ferment"
  ]
  node [
    id 832
    label "komplet"
  ]
  node [
    id 833
    label "tura"
  ]
  node [
    id 834
    label "amendment"
  ]
  node [
    id 835
    label "zmianka"
  ]
  node [
    id 836
    label "odmienianie"
  ]
  node [
    id 837
    label "passage"
  ]
  node [
    id 838
    label "zjawisko"
  ]
  node [
    id 839
    label "change"
  ]
  node [
    id 840
    label "praca"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 2
    target 259
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 263
  ]
  edge [
    source 2
    target 264
  ]
  edge [
    source 2
    target 265
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 2
    target 271
  ]
  edge [
    source 2
    target 272
  ]
  edge [
    source 2
    target 273
  ]
  edge [
    source 2
    target 274
  ]
  edge [
    source 2
    target 275
  ]
  edge [
    source 2
    target 276
  ]
  edge [
    source 2
    target 277
  ]
  edge [
    source 2
    target 278
  ]
  edge [
    source 2
    target 279
  ]
  edge [
    source 2
    target 280
  ]
  edge [
    source 2
    target 281
  ]
  edge [
    source 2
    target 282
  ]
  edge [
    source 2
    target 283
  ]
  edge [
    source 2
    target 284
  ]
  edge [
    source 2
    target 285
  ]
  edge [
    source 2
    target 286
  ]
  edge [
    source 2
    target 287
  ]
  edge [
    source 2
    target 288
  ]
  edge [
    source 2
    target 289
  ]
  edge [
    source 2
    target 290
  ]
  edge [
    source 2
    target 291
  ]
  edge [
    source 2
    target 292
  ]
  edge [
    source 2
    target 293
  ]
  edge [
    source 2
    target 294
  ]
  edge [
    source 2
    target 295
  ]
  edge [
    source 2
    target 296
  ]
  edge [
    source 2
    target 297
  ]
  edge [
    source 2
    target 298
  ]
  edge [
    source 2
    target 299
  ]
  edge [
    source 2
    target 300
  ]
  edge [
    source 2
    target 301
  ]
  edge [
    source 2
    target 302
  ]
  edge [
    source 2
    target 303
  ]
  edge [
    source 2
    target 304
  ]
  edge [
    source 2
    target 305
  ]
  edge [
    source 2
    target 306
  ]
  edge [
    source 2
    target 307
  ]
  edge [
    source 2
    target 308
  ]
  edge [
    source 2
    target 309
  ]
  edge [
    source 2
    target 310
  ]
  edge [
    source 2
    target 311
  ]
  edge [
    source 2
    target 312
  ]
  edge [
    source 2
    target 313
  ]
  edge [
    source 2
    target 314
  ]
  edge [
    source 2
    target 315
  ]
  edge [
    source 2
    target 316
  ]
  edge [
    source 2
    target 317
  ]
  edge [
    source 2
    target 318
  ]
  edge [
    source 2
    target 319
  ]
  edge [
    source 2
    target 320
  ]
  edge [
    source 2
    target 321
  ]
  edge [
    source 2
    target 322
  ]
  edge [
    source 2
    target 323
  ]
  edge [
    source 2
    target 324
  ]
  edge [
    source 2
    target 325
  ]
  edge [
    source 2
    target 326
  ]
  edge [
    source 2
    target 327
  ]
  edge [
    source 2
    target 328
  ]
  edge [
    source 2
    target 329
  ]
  edge [
    source 2
    target 330
  ]
  edge [
    source 2
    target 331
  ]
  edge [
    source 2
    target 332
  ]
  edge [
    source 2
    target 333
  ]
  edge [
    source 2
    target 334
  ]
  edge [
    source 2
    target 335
  ]
  edge [
    source 2
    target 336
  ]
  edge [
    source 2
    target 337
  ]
  edge [
    source 2
    target 338
  ]
  edge [
    source 2
    target 339
  ]
  edge [
    source 2
    target 340
  ]
  edge [
    source 2
    target 341
  ]
  edge [
    source 2
    target 342
  ]
  edge [
    source 2
    target 343
  ]
  edge [
    source 2
    target 344
  ]
  edge [
    source 2
    target 345
  ]
  edge [
    source 2
    target 346
  ]
  edge [
    source 2
    target 347
  ]
  edge [
    source 2
    target 348
  ]
  edge [
    source 2
    target 349
  ]
  edge [
    source 2
    target 350
  ]
  edge [
    source 2
    target 351
  ]
  edge [
    source 2
    target 352
  ]
  edge [
    source 2
    target 353
  ]
  edge [
    source 2
    target 354
  ]
  edge [
    source 2
    target 355
  ]
  edge [
    source 2
    target 356
  ]
  edge [
    source 2
    target 357
  ]
  edge [
    source 2
    target 358
  ]
  edge [
    source 2
    target 359
  ]
  edge [
    source 2
    target 360
  ]
  edge [
    source 2
    target 361
  ]
  edge [
    source 2
    target 362
  ]
  edge [
    source 2
    target 363
  ]
  edge [
    source 2
    target 364
  ]
  edge [
    source 2
    target 365
  ]
  edge [
    source 2
    target 366
  ]
  edge [
    source 2
    target 367
  ]
  edge [
    source 2
    target 368
  ]
  edge [
    source 2
    target 369
  ]
  edge [
    source 2
    target 370
  ]
  edge [
    source 2
    target 371
  ]
  edge [
    source 2
    target 372
  ]
  edge [
    source 2
    target 373
  ]
  edge [
    source 2
    target 374
  ]
  edge [
    source 2
    target 375
  ]
  edge [
    source 2
    target 376
  ]
  edge [
    source 2
    target 377
  ]
  edge [
    source 2
    target 378
  ]
  edge [
    source 2
    target 379
  ]
  edge [
    source 2
    target 380
  ]
  edge [
    source 2
    target 381
  ]
  edge [
    source 2
    target 382
  ]
  edge [
    source 2
    target 383
  ]
  edge [
    source 2
    target 384
  ]
  edge [
    source 2
    target 385
  ]
  edge [
    source 2
    target 386
  ]
  edge [
    source 2
    target 387
  ]
  edge [
    source 2
    target 388
  ]
  edge [
    source 2
    target 389
  ]
  edge [
    source 2
    target 390
  ]
  edge [
    source 2
    target 391
  ]
  edge [
    source 2
    target 392
  ]
  edge [
    source 2
    target 393
  ]
  edge [
    source 2
    target 394
  ]
  edge [
    source 2
    target 395
  ]
  edge [
    source 2
    target 396
  ]
  edge [
    source 2
    target 397
  ]
  edge [
    source 2
    target 398
  ]
  edge [
    source 2
    target 399
  ]
  edge [
    source 2
    target 400
  ]
  edge [
    source 2
    target 401
  ]
  edge [
    source 2
    target 402
  ]
  edge [
    source 2
    target 403
  ]
  edge [
    source 2
    target 404
  ]
  edge [
    source 2
    target 405
  ]
  edge [
    source 2
    target 406
  ]
  edge [
    source 2
    target 407
  ]
  edge [
    source 2
    target 408
  ]
  edge [
    source 2
    target 409
  ]
  edge [
    source 2
    target 410
  ]
  edge [
    source 2
    target 411
  ]
  edge [
    source 2
    target 412
  ]
  edge [
    source 2
    target 413
  ]
  edge [
    source 2
    target 414
  ]
  edge [
    source 2
    target 415
  ]
  edge [
    source 2
    target 416
  ]
  edge [
    source 2
    target 417
  ]
  edge [
    source 2
    target 418
  ]
  edge [
    source 2
    target 419
  ]
  edge [
    source 2
    target 420
  ]
  edge [
    source 2
    target 421
  ]
  edge [
    source 2
    target 422
  ]
  edge [
    source 2
    target 423
  ]
  edge [
    source 2
    target 424
  ]
  edge [
    source 2
    target 425
  ]
  edge [
    source 2
    target 426
  ]
  edge [
    source 2
    target 427
  ]
  edge [
    source 2
    target 428
  ]
  edge [
    source 2
    target 429
  ]
  edge [
    source 2
    target 430
  ]
  edge [
    source 2
    target 431
  ]
  edge [
    source 2
    target 432
  ]
  edge [
    source 2
    target 433
  ]
  edge [
    source 2
    target 434
  ]
  edge [
    source 2
    target 435
  ]
  edge [
    source 2
    target 436
  ]
  edge [
    source 2
    target 437
  ]
  edge [
    source 2
    target 438
  ]
  edge [
    source 2
    target 439
  ]
  edge [
    source 2
    target 440
  ]
  edge [
    source 2
    target 441
  ]
  edge [
    source 2
    target 442
  ]
  edge [
    source 2
    target 443
  ]
  edge [
    source 2
    target 444
  ]
  edge [
    source 2
    target 445
  ]
  edge [
    source 2
    target 446
  ]
  edge [
    source 2
    target 447
  ]
  edge [
    source 2
    target 448
  ]
  edge [
    source 2
    target 449
  ]
  edge [
    source 2
    target 450
  ]
  edge [
    source 2
    target 451
  ]
  edge [
    source 2
    target 452
  ]
  edge [
    source 2
    target 453
  ]
  edge [
    source 2
    target 454
  ]
  edge [
    source 2
    target 455
  ]
  edge [
    source 2
    target 456
  ]
  edge [
    source 2
    target 457
  ]
  edge [
    source 2
    target 458
  ]
  edge [
    source 2
    target 459
  ]
  edge [
    source 2
    target 460
  ]
  edge [
    source 2
    target 461
  ]
  edge [
    source 2
    target 462
  ]
  edge [
    source 2
    target 463
  ]
  edge [
    source 2
    target 464
  ]
  edge [
    source 2
    target 465
  ]
  edge [
    source 2
    target 466
  ]
  edge [
    source 2
    target 467
  ]
  edge [
    source 2
    target 468
  ]
  edge [
    source 2
    target 469
  ]
  edge [
    source 2
    target 470
  ]
  edge [
    source 2
    target 471
  ]
  edge [
    source 2
    target 472
  ]
  edge [
    source 2
    target 473
  ]
  edge [
    source 2
    target 474
  ]
  edge [
    source 2
    target 475
  ]
  edge [
    source 2
    target 476
  ]
  edge [
    source 2
    target 477
  ]
  edge [
    source 2
    target 478
  ]
  edge [
    source 2
    target 479
  ]
  edge [
    source 2
    target 480
  ]
  edge [
    source 2
    target 481
  ]
  edge [
    source 2
    target 482
  ]
  edge [
    source 2
    target 483
  ]
  edge [
    source 2
    target 484
  ]
  edge [
    source 2
    target 485
  ]
  edge [
    source 2
    target 486
  ]
  edge [
    source 2
    target 487
  ]
  edge [
    source 2
    target 488
  ]
  edge [
    source 2
    target 489
  ]
  edge [
    source 2
    target 490
  ]
  edge [
    source 2
    target 491
  ]
  edge [
    source 2
    target 492
  ]
  edge [
    source 2
    target 493
  ]
  edge [
    source 2
    target 494
  ]
  edge [
    source 2
    target 495
  ]
  edge [
    source 2
    target 496
  ]
  edge [
    source 2
    target 497
  ]
  edge [
    source 2
    target 498
  ]
  edge [
    source 2
    target 499
  ]
  edge [
    source 2
    target 500
  ]
  edge [
    source 2
    target 501
  ]
  edge [
    source 2
    target 502
  ]
  edge [
    source 2
    target 503
  ]
  edge [
    source 2
    target 504
  ]
  edge [
    source 2
    target 505
  ]
  edge [
    source 2
    target 506
  ]
  edge [
    source 2
    target 507
  ]
  edge [
    source 2
    target 508
  ]
  edge [
    source 2
    target 509
  ]
  edge [
    source 2
    target 510
  ]
  edge [
    source 2
    target 511
  ]
  edge [
    source 2
    target 512
  ]
  edge [
    source 2
    target 513
  ]
  edge [
    source 2
    target 514
  ]
  edge [
    source 2
    target 515
  ]
  edge [
    source 2
    target 516
  ]
  edge [
    source 2
    target 517
  ]
  edge [
    source 2
    target 518
  ]
  edge [
    source 2
    target 519
  ]
  edge [
    source 2
    target 520
  ]
  edge [
    source 2
    target 521
  ]
  edge [
    source 2
    target 522
  ]
  edge [
    source 2
    target 523
  ]
  edge [
    source 2
    target 524
  ]
  edge [
    source 2
    target 525
  ]
  edge [
    source 2
    target 526
  ]
  edge [
    source 2
    target 527
  ]
  edge [
    source 2
    target 528
  ]
  edge [
    source 2
    target 529
  ]
  edge [
    source 2
    target 530
  ]
  edge [
    source 2
    target 531
  ]
  edge [
    source 2
    target 532
  ]
  edge [
    source 2
    target 533
  ]
  edge [
    source 2
    target 534
  ]
  edge [
    source 2
    target 535
  ]
  edge [
    source 2
    target 536
  ]
  edge [
    source 2
    target 537
  ]
  edge [
    source 2
    target 538
  ]
  edge [
    source 2
    target 539
  ]
  edge [
    source 2
    target 540
  ]
  edge [
    source 2
    target 541
  ]
  edge [
    source 2
    target 542
  ]
  edge [
    source 2
    target 543
  ]
  edge [
    source 2
    target 544
  ]
  edge [
    source 2
    target 545
  ]
  edge [
    source 2
    target 546
  ]
  edge [
    source 2
    target 547
  ]
  edge [
    source 2
    target 548
  ]
  edge [
    source 2
    target 549
  ]
  edge [
    source 2
    target 550
  ]
  edge [
    source 2
    target 551
  ]
  edge [
    source 2
    target 552
  ]
  edge [
    source 2
    target 553
  ]
  edge [
    source 2
    target 554
  ]
  edge [
    source 2
    target 555
  ]
  edge [
    source 2
    target 556
  ]
  edge [
    source 2
    target 557
  ]
  edge [
    source 2
    target 558
  ]
  edge [
    source 2
    target 559
  ]
  edge [
    source 2
    target 560
  ]
  edge [
    source 2
    target 561
  ]
  edge [
    source 2
    target 562
  ]
  edge [
    source 2
    target 563
  ]
  edge [
    source 2
    target 564
  ]
  edge [
    source 2
    target 565
  ]
  edge [
    source 2
    target 566
  ]
  edge [
    source 2
    target 567
  ]
  edge [
    source 2
    target 568
  ]
  edge [
    source 2
    target 569
  ]
  edge [
    source 2
    target 570
  ]
  edge [
    source 2
    target 571
  ]
  edge [
    source 2
    target 572
  ]
  edge [
    source 2
    target 573
  ]
  edge [
    source 2
    target 574
  ]
  edge [
    source 2
    target 575
  ]
  edge [
    source 2
    target 576
  ]
  edge [
    source 2
    target 577
  ]
  edge [
    source 2
    target 578
  ]
  edge [
    source 2
    target 579
  ]
  edge [
    source 2
    target 580
  ]
  edge [
    source 2
    target 581
  ]
  edge [
    source 2
    target 582
  ]
  edge [
    source 2
    target 583
  ]
  edge [
    source 2
    target 584
  ]
  edge [
    source 2
    target 585
  ]
  edge [
    source 2
    target 586
  ]
  edge [
    source 2
    target 587
  ]
  edge [
    source 2
    target 588
  ]
  edge [
    source 2
    target 589
  ]
  edge [
    source 2
    target 590
  ]
  edge [
    source 2
    target 591
  ]
  edge [
    source 2
    target 592
  ]
  edge [
    source 2
    target 593
  ]
  edge [
    source 2
    target 594
  ]
  edge [
    source 2
    target 595
  ]
  edge [
    source 2
    target 596
  ]
  edge [
    source 2
    target 597
  ]
  edge [
    source 2
    target 598
  ]
  edge [
    source 2
    target 599
  ]
  edge [
    source 2
    target 600
  ]
  edge [
    source 2
    target 601
  ]
  edge [
    source 2
    target 602
  ]
  edge [
    source 2
    target 603
  ]
  edge [
    source 2
    target 604
  ]
  edge [
    source 2
    target 605
  ]
  edge [
    source 2
    target 606
  ]
  edge [
    source 2
    target 607
  ]
  edge [
    source 2
    target 608
  ]
  edge [
    source 2
    target 609
  ]
  edge [
    source 2
    target 610
  ]
  edge [
    source 2
    target 611
  ]
  edge [
    source 2
    target 612
  ]
  edge [
    source 2
    target 613
  ]
  edge [
    source 2
    target 614
  ]
  edge [
    source 2
    target 615
  ]
  edge [
    source 2
    target 616
  ]
  edge [
    source 2
    target 617
  ]
  edge [
    source 2
    target 618
  ]
  edge [
    source 2
    target 619
  ]
  edge [
    source 2
    target 620
  ]
  edge [
    source 2
    target 621
  ]
  edge [
    source 2
    target 622
  ]
  edge [
    source 2
    target 623
  ]
  edge [
    source 2
    target 624
  ]
  edge [
    source 2
    target 625
  ]
  edge [
    source 2
    target 626
  ]
  edge [
    source 2
    target 627
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 628
  ]
  edge [
    source 3
    target 629
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 630
  ]
  edge [
    source 4
    target 631
  ]
  edge [
    source 4
    target 632
  ]
  edge [
    source 4
    target 633
  ]
  edge [
    source 4
    target 634
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 635
  ]
  edge [
    source 4
    target 636
  ]
  edge [
    source 4
    target 637
  ]
  edge [
    source 4
    target 638
  ]
  edge [
    source 4
    target 639
  ]
  edge [
    source 4
    target 640
  ]
  edge [
    source 4
    target 641
  ]
  edge [
    source 4
    target 642
  ]
  edge [
    source 4
    target 643
  ]
  edge [
    source 4
    target 644
  ]
  edge [
    source 4
    target 645
  ]
  edge [
    source 4
    target 646
  ]
  edge [
    source 4
    target 647
  ]
  edge [
    source 4
    target 648
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 649
  ]
  edge [
    source 5
    target 650
  ]
  edge [
    source 5
    target 651
  ]
  edge [
    source 5
    target 652
  ]
  edge [
    source 5
    target 653
  ]
  edge [
    source 5
    target 654
  ]
  edge [
    source 5
    target 655
  ]
  edge [
    source 5
    target 656
  ]
  edge [
    source 5
    target 657
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 658
  ]
  edge [
    source 6
    target 659
  ]
  edge [
    source 6
    target 660
  ]
  edge [
    source 6
    target 661
  ]
  edge [
    source 6
    target 662
  ]
  edge [
    source 6
    target 663
  ]
  edge [
    source 6
    target 664
  ]
  edge [
    source 6
    target 665
  ]
  edge [
    source 6
    target 666
  ]
  edge [
    source 6
    target 667
  ]
  edge [
    source 6
    target 668
  ]
  edge [
    source 6
    target 669
  ]
  edge [
    source 6
    target 670
  ]
  edge [
    source 6
    target 671
  ]
  edge [
    source 6
    target 672
  ]
  edge [
    source 6
    target 673
  ]
  edge [
    source 6
    target 674
  ]
  edge [
    source 7
    target 18
  ]
  edge [
    source 7
    target 675
  ]
  edge [
    source 7
    target 676
  ]
  edge [
    source 7
    target 677
  ]
  edge [
    source 7
    target 678
  ]
  edge [
    source 7
    target 679
  ]
  edge [
    source 7
    target 680
  ]
  edge [
    source 7
    target 681
  ]
  edge [
    source 7
    target 682
  ]
  edge [
    source 7
    target 683
  ]
  edge [
    source 7
    target 629
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 684
  ]
  edge [
    source 8
    target 685
  ]
  edge [
    source 8
    target 686
  ]
  edge [
    source 8
    target 687
  ]
  edge [
    source 8
    target 688
  ]
  edge [
    source 8
    target 689
  ]
  edge [
    source 8
    target 690
  ]
  edge [
    source 8
    target 23
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 691
  ]
  edge [
    source 9
    target 692
  ]
  edge [
    source 9
    target 693
  ]
  edge [
    source 9
    target 694
  ]
  edge [
    source 9
    target 695
  ]
  edge [
    source 9
    target 696
  ]
  edge [
    source 9
    target 697
  ]
  edge [
    source 9
    target 698
  ]
  edge [
    source 9
    target 699
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 17
  ]
  edge [
    source 10
    target 18
  ]
  edge [
    source 10
    target 700
  ]
  edge [
    source 10
    target 701
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 702
  ]
  edge [
    source 11
    target 703
  ]
  edge [
    source 11
    target 704
  ]
  edge [
    source 11
    target 705
  ]
  edge [
    source 11
    target 706
  ]
  edge [
    source 11
    target 707
  ]
  edge [
    source 11
    target 708
  ]
  edge [
    source 11
    target 709
  ]
  edge [
    source 11
    target 710
  ]
  edge [
    source 11
    target 711
  ]
  edge [
    source 11
    target 712
  ]
  edge [
    source 11
    target 713
  ]
  edge [
    source 11
    target 714
  ]
  edge [
    source 11
    target 715
  ]
  edge [
    source 11
    target 716
  ]
  edge [
    source 11
    target 717
  ]
  edge [
    source 11
    target 718
  ]
  edge [
    source 11
    target 719
  ]
  edge [
    source 11
    target 720
  ]
  edge [
    source 11
    target 721
  ]
  edge [
    source 11
    target 722
  ]
  edge [
    source 11
    target 723
  ]
  edge [
    source 11
    target 724
  ]
  edge [
    source 11
    target 725
  ]
  edge [
    source 11
    target 726
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 727
  ]
  edge [
    source 12
    target 728
  ]
  edge [
    source 12
    target 729
  ]
  edge [
    source 12
    target 730
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 731
  ]
  edge [
    source 13
    target 732
  ]
  edge [
    source 13
    target 733
  ]
  edge [
    source 13
    target 734
  ]
  edge [
    source 13
    target 735
  ]
  edge [
    source 13
    target 736
  ]
  edge [
    source 13
    target 737
  ]
  edge [
    source 13
    target 738
  ]
  edge [
    source 13
    target 17
  ]
  edge [
    source 13
    target 739
  ]
  edge [
    source 13
    target 740
  ]
  edge [
    source 13
    target 741
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 742
  ]
  edge [
    source 14
    target 743
  ]
  edge [
    source 14
    target 744
  ]
  edge [
    source 14
    target 745
  ]
  edge [
    source 14
    target 746
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 747
  ]
  edge [
    source 15
    target 748
  ]
  edge [
    source 15
    target 749
  ]
  edge [
    source 15
    target 750
  ]
  edge [
    source 15
    target 751
  ]
  edge [
    source 15
    target 752
  ]
  edge [
    source 15
    target 636
  ]
  edge [
    source 15
    target 753
  ]
  edge [
    source 15
    target 639
  ]
  edge [
    source 15
    target 754
  ]
  edge [
    source 15
    target 755
  ]
  edge [
    source 15
    target 756
  ]
  edge [
    source 15
    target 757
  ]
  edge [
    source 15
    target 758
  ]
  edge [
    source 15
    target 759
  ]
  edge [
    source 15
    target 760
  ]
  edge [
    source 15
    target 761
  ]
  edge [
    source 15
    target 762
  ]
  edge [
    source 15
    target 763
  ]
  edge [
    source 15
    target 764
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 34
  ]
  edge [
    source 16
    target 765
  ]
  edge [
    source 16
    target 766
  ]
  edge [
    source 16
    target 767
  ]
  edge [
    source 16
    target 768
  ]
  edge [
    source 16
    target 769
  ]
  edge [
    source 16
    target 770
  ]
  edge [
    source 17
    target 771
  ]
  edge [
    source 17
    target 772
  ]
  edge [
    source 17
    target 660
  ]
  edge [
    source 17
    target 773
  ]
  edge [
    source 17
    target 774
  ]
  edge [
    source 17
    target 775
  ]
  edge [
    source 17
    target 776
  ]
  edge [
    source 17
    target 665
  ]
  edge [
    source 17
    target 777
  ]
  edge [
    source 17
    target 778
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 779
  ]
  edge [
    source 19
    target 780
  ]
  edge [
    source 19
    target 781
  ]
  edge [
    source 19
    target 782
  ]
  edge [
    source 19
    target 783
  ]
  edge [
    source 19
    target 784
  ]
  edge [
    source 19
    target 785
  ]
  edge [
    source 19
    target 786
  ]
  edge [
    source 19
    target 787
  ]
  edge [
    source 19
    target 788
  ]
  edge [
    source 19
    target 789
  ]
  edge [
    source 19
    target 790
  ]
  edge [
    source 19
    target 791
  ]
  edge [
    source 19
    target 792
  ]
  edge [
    source 19
    target 793
  ]
  edge [
    source 19
    target 794
  ]
  edge [
    source 19
    target 795
  ]
  edge [
    source 19
    target 796
  ]
  edge [
    source 19
    target 797
  ]
  edge [
    source 19
    target 798
  ]
  edge [
    source 19
    target 799
  ]
  edge [
    source 19
    target 800
  ]
  edge [
    source 19
    target 801
  ]
  edge [
    source 19
    target 802
  ]
  edge [
    source 19
    target 803
  ]
  edge [
    source 19
    target 804
  ]
  edge [
    source 19
    target 805
  ]
  edge [
    source 19
    target 806
  ]
  edge [
    source 19
    target 807
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 808
  ]
  edge [
    source 20
    target 809
  ]
  edge [
    source 20
    target 804
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 810
  ]
  edge [
    source 21
    target 811
  ]
  edge [
    source 21
    target 812
  ]
  edge [
    source 21
    target 813
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 814
  ]
  edge [
    source 22
    target 815
  ]
  edge [
    source 22
    target 816
  ]
  edge [
    source 22
    target 817
  ]
  edge [
    source 22
    target 704
  ]
  edge [
    source 22
    target 818
  ]
  edge [
    source 22
    target 819
  ]
  edge [
    source 22
    target 820
  ]
  edge [
    source 22
    target 821
  ]
  edge [
    source 22
    target 822
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 823
  ]
  edge [
    source 23
    target 824
  ]
  edge [
    source 24
    target 825
  ]
  edge [
    source 24
    target 653
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 826
  ]
  edge [
    source 25
    target 827
  ]
  edge [
    source 26
    target 828
  ]
  edge [
    source 26
    target 829
  ]
  edge [
    source 26
    target 830
  ]
  edge [
    source 26
    target 782
  ]
  edge [
    source 26
    target 831
  ]
  edge [
    source 26
    target 832
  ]
  edge [
    source 26
    target 833
  ]
  edge [
    source 26
    target 834
  ]
  edge [
    source 26
    target 835
  ]
  edge [
    source 26
    target 836
  ]
  edge [
    source 26
    target 837
  ]
  edge [
    source 26
    target 838
  ]
  edge [
    source 26
    target 839
  ]
  edge [
    source 26
    target 840
  ]
]
