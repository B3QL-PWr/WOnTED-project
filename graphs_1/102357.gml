graph [
  maxDegree 40
  minDegree 1
  meanDegree 2.190231362467866
  density 0.00564492619192749
  graphCliqueNumber 3
  node [
    id 0
    label "autor"
    origin "text"
  ]
  node [
    id 1
    label "niniejszy"
    origin "text"
  ]
  node [
    id 2
    label "blog"
    origin "text"
  ]
  node [
    id 3
    label "urodzi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "dawno"
    origin "text"
  ]
  node [
    id 6
    label "rok"
    origin "text"
  ]
  node [
    id 7
    label "by&#263;"
    origin "text"
  ]
  node [
    id 8
    label "mocno"
    origin "text"
  ]
  node [
    id 9
    label "przedwojenny"
    origin "text"
  ]
  node [
    id 10
    label "wykszta&#322;cenie"
    origin "text"
  ]
  node [
    id 11
    label "matematyk"
    origin "text"
  ]
  node [
    id 12
    label "przez"
    origin "text"
  ]
  node [
    id 13
    label "wiele"
    origin "text"
  ]
  node [
    id 14
    label "lata"
    origin "text"
  ]
  node [
    id 15
    label "wykonywa&#263;"
    origin "text"
  ]
  node [
    id 16
    label "ten"
    origin "text"
  ]
  node [
    id 17
    label "zaw&#243;d"
    origin "text"
  ]
  node [
    id 18
    label "jako"
    origin "text"
  ]
  node [
    id 19
    label "nauczyciel"
    origin "text"
  ]
  node [
    id 20
    label "akademicki"
    origin "text"
  ]
  node [
    id 21
    label "kilka"
    origin "text"
  ]
  node [
    id 22
    label "wysoki"
    origin "text"
  ]
  node [
    id 23
    label "uczelnia"
    origin "text"
  ]
  node [
    id 24
    label "tym"
    origin "text"
  ]
  node [
    id 25
    label "siebie"
    origin "text"
  ]
  node [
    id 26
    label "ceni&#263;"
    origin "text"
  ]
  node [
    id 27
    label "wysoce"
    origin "text"
  ]
  node [
    id 28
    label "r&#243;wnie&#380;"
    origin "text"
  ]
  node [
    id 29
    label "pracownik"
    origin "text"
  ]
  node [
    id 30
    label "pan"
    origin "text"
  ]
  node [
    id 31
    label "pi&#281;&#263;"
    origin "text"
  ]
  node [
    id 32
    label "mia&#322;"
    origin "text"
  ]
  node [
    id 33
    label "zaszczyt"
    origin "text"
  ]
  node [
    id 34
    label "tak&#380;e"
    origin "text"
  ]
  node [
    id 35
    label "cz&#322;onek"
    origin "text"
  ]
  node [
    id 36
    label "komitet"
    origin "text"
  ]
  node [
    id 37
    label "prognoza"
    origin "text"
  ]
  node [
    id 38
    label "polska"
    origin "text"
  ]
  node [
    id 39
    label "plus"
    origin "text"
  ]
  node [
    id 40
    label "szczyci&#263;"
    origin "text"
  ]
  node [
    id 41
    label "wchodzi&#263;"
    origin "text"
  ]
  node [
    id 42
    label "sk&#322;ad"
    origin "text"
  ]
  node [
    id 43
    label "zesp&#243;&#322;"
    origin "text"
  ]
  node [
    id 44
    label "programista"
    origin "text"
  ]
  node [
    id 45
    label "pierwsza"
    origin "text"
  ]
  node [
    id 46
    label "polski"
    origin "text"
  ]
  node [
    id 47
    label "maszyna"
    origin "text"
  ]
  node [
    id 48
    label "matematyczny"
    origin "text"
  ]
  node [
    id 49
    label "xyz"
    origin "text"
  ]
  node [
    id 50
    label "pomys&#322;odawca"
  ]
  node [
    id 51
    label "kszta&#322;ciciel"
  ]
  node [
    id 52
    label "tworzyciel"
  ]
  node [
    id 53
    label "&#347;w"
  ]
  node [
    id 54
    label "wykonawca"
  ]
  node [
    id 55
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 56
    label "komcio"
  ]
  node [
    id 57
    label "strona"
  ]
  node [
    id 58
    label "blogosfera"
  ]
  node [
    id 59
    label "pami&#281;tnik"
  ]
  node [
    id 60
    label "narodzi&#263;"
  ]
  node [
    id 61
    label "zlec"
  ]
  node [
    id 62
    label "engender"
  ]
  node [
    id 63
    label "powi&#263;"
  ]
  node [
    id 64
    label "zrobi&#263;"
  ]
  node [
    id 65
    label "porodzi&#263;"
  ]
  node [
    id 66
    label "dawny"
  ]
  node [
    id 67
    label "ongi&#347;"
  ]
  node [
    id 68
    label "dawnie"
  ]
  node [
    id 69
    label "wcze&#347;niej"
  ]
  node [
    id 70
    label "d&#322;ugotrwale"
  ]
  node [
    id 71
    label "stulecie"
  ]
  node [
    id 72
    label "kalendarz"
  ]
  node [
    id 73
    label "czas"
  ]
  node [
    id 74
    label "pora_roku"
  ]
  node [
    id 75
    label "cykl_astronomiczny"
  ]
  node [
    id 76
    label "p&#243;&#322;rocze"
  ]
  node [
    id 77
    label "grupa"
  ]
  node [
    id 78
    label "kwarta&#322;"
  ]
  node [
    id 79
    label "kurs"
  ]
  node [
    id 80
    label "jubileusz"
  ]
  node [
    id 81
    label "miesi&#261;c"
  ]
  node [
    id 82
    label "martwy_sezon"
  ]
  node [
    id 83
    label "si&#281;ga&#263;"
  ]
  node [
    id 84
    label "trwa&#263;"
  ]
  node [
    id 85
    label "obecno&#347;&#263;"
  ]
  node [
    id 86
    label "stan"
  ]
  node [
    id 87
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 88
    label "stand"
  ]
  node [
    id 89
    label "mie&#263;_miejsce"
  ]
  node [
    id 90
    label "uczestniczy&#263;"
  ]
  node [
    id 91
    label "chodzi&#263;"
  ]
  node [
    id 92
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 93
    label "equal"
  ]
  node [
    id 94
    label "zdecydowanie"
  ]
  node [
    id 95
    label "stabilnie"
  ]
  node [
    id 96
    label "widocznie"
  ]
  node [
    id 97
    label "silny"
  ]
  node [
    id 98
    label "silnie"
  ]
  node [
    id 99
    label "niepodwa&#380;alnie"
  ]
  node [
    id 100
    label "konkretnie"
  ]
  node [
    id 101
    label "intensywny"
  ]
  node [
    id 102
    label "przekonuj&#261;co"
  ]
  node [
    id 103
    label "strongly"
  ]
  node [
    id 104
    label "niema&#322;o"
  ]
  node [
    id 105
    label "szczerze"
  ]
  node [
    id 106
    label "mocny"
  ]
  node [
    id 107
    label "powerfully"
  ]
  node [
    id 108
    label "kwalifikacje"
  ]
  node [
    id 109
    label "niepokalanki"
  ]
  node [
    id 110
    label "sophistication"
  ]
  node [
    id 111
    label "form"
  ]
  node [
    id 112
    label "training"
  ]
  node [
    id 113
    label "pomo&#380;enie"
  ]
  node [
    id 114
    label "wiedza"
  ]
  node [
    id 115
    label "zapoznanie"
  ]
  node [
    id 116
    label "rozwini&#281;cie"
  ]
  node [
    id 117
    label "wys&#322;anie"
  ]
  node [
    id 118
    label "o&#347;wiecenie"
  ]
  node [
    id 119
    label "skolaryzacja"
  ]
  node [
    id 120
    label "udoskonalenie"
  ]
  node [
    id 121
    label "urszulanki"
  ]
  node [
    id 122
    label "Gauss"
  ]
  node [
    id 123
    label "Euklides"
  ]
  node [
    id 124
    label "Berkeley"
  ]
  node [
    id 125
    label "Ptolemeusz"
  ]
  node [
    id 126
    label "Doppler"
  ]
  node [
    id 127
    label "Archimedes"
  ]
  node [
    id 128
    label "Maxwell"
  ]
  node [
    id 129
    label "Bayes"
  ]
  node [
    id 130
    label "Pascal"
  ]
  node [
    id 131
    label "Borel"
  ]
  node [
    id 132
    label "Newton"
  ]
  node [
    id 133
    label "Kartezjusz"
  ]
  node [
    id 134
    label "Kepler"
  ]
  node [
    id 135
    label "naukowiec"
  ]
  node [
    id 136
    label "Fourier"
  ]
  node [
    id 137
    label "Pitagoras"
  ]
  node [
    id 138
    label "Galileusz"
  ]
  node [
    id 139
    label "Biot"
  ]
  node [
    id 140
    label "Laplace"
  ]
  node [
    id 141
    label "wiela"
  ]
  node [
    id 142
    label "du&#380;y"
  ]
  node [
    id 143
    label "summer"
  ]
  node [
    id 144
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 145
    label "work"
  ]
  node [
    id 146
    label "robi&#263;"
  ]
  node [
    id 147
    label "muzyka"
  ]
  node [
    id 148
    label "rola"
  ]
  node [
    id 149
    label "create"
  ]
  node [
    id 150
    label "wytwarza&#263;"
  ]
  node [
    id 151
    label "praca"
  ]
  node [
    id 152
    label "okre&#347;lony"
  ]
  node [
    id 153
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 154
    label "emocja"
  ]
  node [
    id 155
    label "zawodoznawstwo"
  ]
  node [
    id 156
    label "office"
  ]
  node [
    id 157
    label "craft"
  ]
  node [
    id 158
    label "profesor"
  ]
  node [
    id 159
    label "szkolnik"
  ]
  node [
    id 160
    label "preceptor"
  ]
  node [
    id 161
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 162
    label "pedagog"
  ]
  node [
    id 163
    label "popularyzator"
  ]
  node [
    id 164
    label "belfer"
  ]
  node [
    id 165
    label "prawid&#322;owy"
  ]
  node [
    id 166
    label "tradycyjny"
  ]
  node [
    id 167
    label "intelektualny"
  ]
  node [
    id 168
    label "akademiczny"
  ]
  node [
    id 169
    label "szkolny"
  ]
  node [
    id 170
    label "po_akademicku"
  ]
  node [
    id 171
    label "naukowy"
  ]
  node [
    id 172
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 173
    label "akademicko"
  ]
  node [
    id 174
    label "teoretyczny"
  ]
  node [
    id 175
    label "studencki"
  ]
  node [
    id 176
    label "odpowiedni"
  ]
  node [
    id 177
    label "&#347;ledziowate"
  ]
  node [
    id 178
    label "ryba"
  ]
  node [
    id 179
    label "warto&#347;ciowy"
  ]
  node [
    id 180
    label "daleki"
  ]
  node [
    id 181
    label "znaczny"
  ]
  node [
    id 182
    label "wysoko"
  ]
  node [
    id 183
    label "szczytnie"
  ]
  node [
    id 184
    label "wznios&#322;y"
  ]
  node [
    id 185
    label "wyrafinowany"
  ]
  node [
    id 186
    label "z_wysoka"
  ]
  node [
    id 187
    label "chwalebny"
  ]
  node [
    id 188
    label "uprzywilejowany"
  ]
  node [
    id 189
    label "niepo&#347;ledni"
  ]
  node [
    id 190
    label "rektorat"
  ]
  node [
    id 191
    label "podkanclerz"
  ]
  node [
    id 192
    label "kanclerz"
  ]
  node [
    id 193
    label "kwestura"
  ]
  node [
    id 194
    label "miasteczko_studenckie"
  ]
  node [
    id 195
    label "school"
  ]
  node [
    id 196
    label "senat"
  ]
  node [
    id 197
    label "wyk&#322;ada&#263;"
  ]
  node [
    id 198
    label "wyk&#322;adanie"
  ]
  node [
    id 199
    label "promotorstwo"
  ]
  node [
    id 200
    label "szko&#322;a"
  ]
  node [
    id 201
    label "szanowa&#263;"
  ]
  node [
    id 202
    label "prize"
  ]
  node [
    id 203
    label "appreciate"
  ]
  node [
    id 204
    label "uznawa&#263;"
  ]
  node [
    id 205
    label "liczy&#263;"
  ]
  node [
    id 206
    label "ustala&#263;"
  ]
  node [
    id 207
    label "intensywnie"
  ]
  node [
    id 208
    label "wielki"
  ]
  node [
    id 209
    label "cz&#322;owiek_pracy"
  ]
  node [
    id 210
    label "cz&#322;owiek"
  ]
  node [
    id 211
    label "delegowa&#263;"
  ]
  node [
    id 212
    label "s&#322;u&#380;ba_cywilna"
  ]
  node [
    id 213
    label "pracu&#347;"
  ]
  node [
    id 214
    label "delegowanie"
  ]
  node [
    id 215
    label "r&#281;ka"
  ]
  node [
    id 216
    label "salariat"
  ]
  node [
    id 217
    label "jegomo&#347;&#263;"
  ]
  node [
    id 218
    label "zwrot"
  ]
  node [
    id 219
    label "pracodawca"
  ]
  node [
    id 220
    label "rz&#261;dzenie"
  ]
  node [
    id 221
    label "m&#261;&#380;"
  ]
  node [
    id 222
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 223
    label "ch&#322;opina"
  ]
  node [
    id 224
    label "bratek"
  ]
  node [
    id 225
    label "opiekun"
  ]
  node [
    id 226
    label "doros&#322;y"
  ]
  node [
    id 227
    label "Midas"
  ]
  node [
    id 228
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 229
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 230
    label "murza"
  ]
  node [
    id 231
    label "ojciec"
  ]
  node [
    id 232
    label "androlog"
  ]
  node [
    id 233
    label "pupil"
  ]
  node [
    id 234
    label "efendi"
  ]
  node [
    id 235
    label "nabab"
  ]
  node [
    id 236
    label "w&#322;odarz"
  ]
  node [
    id 237
    label "andropauza"
  ]
  node [
    id 238
    label "gra_w_karty"
  ]
  node [
    id 239
    label "Mieszko_I"
  ]
  node [
    id 240
    label "bogaty"
  ]
  node [
    id 241
    label "samiec"
  ]
  node [
    id 242
    label "przyw&#243;dca"
  ]
  node [
    id 243
    label "pa&#324;stwo"
  ]
  node [
    id 244
    label "proszek"
  ]
  node [
    id 245
    label "honours"
  ]
  node [
    id 246
    label "wyr&#243;&#380;nienie"
  ]
  node [
    id 247
    label "cia&#322;o"
  ]
  node [
    id 248
    label "organizacja"
  ]
  node [
    id 249
    label "przedstawiciel"
  ]
  node [
    id 250
    label "shaft"
  ]
  node [
    id 251
    label "podmiot"
  ]
  node [
    id 252
    label "fiut"
  ]
  node [
    id 253
    label "przyrodzenie"
  ]
  node [
    id 254
    label "wchodzenie"
  ]
  node [
    id 255
    label "ptaszek"
  ]
  node [
    id 256
    label "organ"
  ]
  node [
    id 257
    label "wej&#347;cie"
  ]
  node [
    id 258
    label "narz&#261;d_kopulacyjny"
  ]
  node [
    id 259
    label "element_anatomiczny"
  ]
  node [
    id 260
    label "Komitet_Obrony_Robotnik&#243;w"
  ]
  node [
    id 261
    label "diagnoza"
  ]
  node [
    id 262
    label "perspektywa"
  ]
  node [
    id 263
    label "przewidywanie"
  ]
  node [
    id 264
    label "warto&#347;&#263;"
  ]
  node [
    id 265
    label "wabik"
  ]
  node [
    id 266
    label "rewaluowa&#263;"
  ]
  node [
    id 267
    label "korzy&#347;&#263;"
  ]
  node [
    id 268
    label "dodawanie"
  ]
  node [
    id 269
    label "rewaluowanie"
  ]
  node [
    id 270
    label "stopie&#324;"
  ]
  node [
    id 271
    label "ocena"
  ]
  node [
    id 272
    label "zrewaluowa&#263;"
  ]
  node [
    id 273
    label "liczba"
  ]
  node [
    id 274
    label "znak_matematyczny"
  ]
  node [
    id 275
    label "zrewaluowanie"
  ]
  node [
    id 276
    label "w&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 277
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 278
    label "przenika&#263;"
  ]
  node [
    id 279
    label "przekracza&#263;"
  ]
  node [
    id 280
    label "nast&#281;powa&#263;"
  ]
  node [
    id 281
    label "dochodzi&#263;"
  ]
  node [
    id 282
    label "zag&#322;&#281;bia&#263;_si&#281;"
  ]
  node [
    id 283
    label "intervene"
  ]
  node [
    id 284
    label "scale"
  ]
  node [
    id 285
    label "drapa&#263;_si&#281;"
  ]
  node [
    id 286
    label "&#322;oi&#263;"
  ]
  node [
    id 287
    label "osi&#261;ga&#263;"
  ]
  node [
    id 288
    label "przy&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 289
    label "poznawa&#263;"
  ]
  node [
    id 290
    label "go"
  ]
  node [
    id 291
    label "atakowa&#263;"
  ]
  node [
    id 292
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 293
    label "mount"
  ]
  node [
    id 294
    label "invade"
  ]
  node [
    id 295
    label "bra&#263;"
  ]
  node [
    id 296
    label "wnika&#263;"
  ]
  node [
    id 297
    label "move"
  ]
  node [
    id 298
    label "zaziera&#263;"
  ]
  node [
    id 299
    label "wznosi&#263;_si&#281;"
  ]
  node [
    id 300
    label "spotyka&#263;"
  ]
  node [
    id 301
    label "zaczyna&#263;"
  ]
  node [
    id 302
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 303
    label "pole"
  ]
  node [
    id 304
    label "fabryka"
  ]
  node [
    id 305
    label "blokada"
  ]
  node [
    id 306
    label "pas"
  ]
  node [
    id 307
    label "pomieszczenie"
  ]
  node [
    id 308
    label "set"
  ]
  node [
    id 309
    label "constitution"
  ]
  node [
    id 310
    label "tekst"
  ]
  node [
    id 311
    label "struktura"
  ]
  node [
    id 312
    label "basic"
  ]
  node [
    id 313
    label "rank_and_file"
  ]
  node [
    id 314
    label "tabulacja"
  ]
  node [
    id 315
    label "hurtownia"
  ]
  node [
    id 316
    label "sklep"
  ]
  node [
    id 317
    label "&#347;wiat&#322;o"
  ]
  node [
    id 318
    label "syf"
  ]
  node [
    id 319
    label "w&#322;a&#347;ciwo&#347;&#263;_chemiczna"
  ]
  node [
    id 320
    label "obr&#243;bka"
  ]
  node [
    id 321
    label "miejsce"
  ]
  node [
    id 322
    label "sk&#322;adnik"
  ]
  node [
    id 323
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 324
    label "whole"
  ]
  node [
    id 325
    label "odm&#322;adza&#263;"
  ]
  node [
    id 326
    label "zabudowania"
  ]
  node [
    id 327
    label "odm&#322;odzenie"
  ]
  node [
    id 328
    label "zespolik"
  ]
  node [
    id 329
    label "skupienie"
  ]
  node [
    id 330
    label "schorzenie"
  ]
  node [
    id 331
    label "Depeche_Mode"
  ]
  node [
    id 332
    label "Mazowsze"
  ]
  node [
    id 333
    label "ro&#347;lina"
  ]
  node [
    id 334
    label "zbi&#243;r"
  ]
  node [
    id 335
    label "The_Beatles"
  ]
  node [
    id 336
    label "group"
  ]
  node [
    id 337
    label "&#346;wietliki"
  ]
  node [
    id 338
    label "odm&#322;adzanie"
  ]
  node [
    id 339
    label "batch"
  ]
  node [
    id 340
    label "informatyk"
  ]
  node [
    id 341
    label "godzina"
  ]
  node [
    id 342
    label "lacki"
  ]
  node [
    id 343
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 344
    label "przedmiot"
  ]
  node [
    id 345
    label "sztajer"
  ]
  node [
    id 346
    label "drabant"
  ]
  node [
    id 347
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 348
    label "polak"
  ]
  node [
    id 349
    label "pierogi_ruskie"
  ]
  node [
    id 350
    label "krakowiak"
  ]
  node [
    id 351
    label "Polish"
  ]
  node [
    id 352
    label "j&#281;zyk"
  ]
  node [
    id 353
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 354
    label "oberek"
  ]
  node [
    id 355
    label "po_polsku"
  ]
  node [
    id 356
    label "mazur"
  ]
  node [
    id 357
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 358
    label "chodzony"
  ]
  node [
    id 359
    label "skoczny"
  ]
  node [
    id 360
    label "ryba_po_grecku"
  ]
  node [
    id 361
    label "goniony"
  ]
  node [
    id 362
    label "polsko"
  ]
  node [
    id 363
    label "rami&#281;"
  ]
  node [
    id 364
    label "maszyneria"
  ]
  node [
    id 365
    label "pracowanie"
  ]
  node [
    id 366
    label "przeci&#261;&#380;alno&#347;&#263;"
  ]
  node [
    id 367
    label "tuleja"
  ]
  node [
    id 368
    label "b&#281;benek"
  ]
  node [
    id 369
    label "dehumanizacja"
  ]
  node [
    id 370
    label "b&#281;ben"
  ]
  node [
    id 371
    label "wa&#322;"
  ]
  node [
    id 372
    label "kolumna"
  ]
  node [
    id 373
    label "kad&#322;ub"
  ]
  node [
    id 374
    label "mimo&#347;r&#243;d"
  ]
  node [
    id 375
    label "rz&#281;&#380;enie"
  ]
  node [
    id 376
    label "mechanizm"
  ]
  node [
    id 377
    label "t&#322;ok"
  ]
  node [
    id 378
    label "pracowa&#263;"
  ]
  node [
    id 379
    label "przyk&#322;adka"
  ]
  node [
    id 380
    label "rz&#281;zi&#263;"
  ]
  node [
    id 381
    label "trawers"
  ]
  node [
    id 382
    label "wa&#322;ek"
  ]
  node [
    id 383
    label "n&#243;&#380;"
  ]
  node [
    id 384
    label "prototypownia"
  ]
  node [
    id 385
    label "urz&#261;dzenie"
  ]
  node [
    id 386
    label "deflektor"
  ]
  node [
    id 387
    label "dok&#322;adny"
  ]
  node [
    id 388
    label "matematycznie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 14
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 10
  ]
  edge [
    source 7
    target 11
  ]
  edge [
    source 7
    target 27
  ]
  edge [
    source 7
    target 28
  ]
  edge [
    source 7
    target 33
  ]
  edge [
    source 7
    target 34
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 10
    target 110
  ]
  edge [
    source 10
    target 111
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 10
    target 121
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 122
  ]
  edge [
    source 11
    target 123
  ]
  edge [
    source 11
    target 124
  ]
  edge [
    source 11
    target 125
  ]
  edge [
    source 11
    target 126
  ]
  edge [
    source 11
    target 127
  ]
  edge [
    source 11
    target 128
  ]
  edge [
    source 11
    target 129
  ]
  edge [
    source 11
    target 130
  ]
  edge [
    source 11
    target 131
  ]
  edge [
    source 11
    target 132
  ]
  edge [
    source 11
    target 133
  ]
  edge [
    source 11
    target 134
  ]
  edge [
    source 11
    target 135
  ]
  edge [
    source 11
    target 19
  ]
  edge [
    source 11
    target 136
  ]
  edge [
    source 11
    target 137
  ]
  edge [
    source 11
    target 138
  ]
  edge [
    source 11
    target 139
  ]
  edge [
    source 11
    target 140
  ]
  edge [
    source 11
    target 17
  ]
  edge [
    source 11
    target 40
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 30
  ]
  edge [
    source 12
    target 31
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 141
  ]
  edge [
    source 13
    target 142
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 31
  ]
  edge [
    source 14
    target 32
  ]
  edge [
    source 14
    target 143
  ]
  edge [
    source 14
    target 73
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 144
  ]
  edge [
    source 15
    target 145
  ]
  edge [
    source 15
    target 146
  ]
  edge [
    source 15
    target 147
  ]
  edge [
    source 15
    target 148
  ]
  edge [
    source 15
    target 149
  ]
  edge [
    source 15
    target 150
  ]
  edge [
    source 15
    target 151
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 152
  ]
  edge [
    source 16
    target 153
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 108
  ]
  edge [
    source 17
    target 154
  ]
  edge [
    source 17
    target 155
  ]
  edge [
    source 17
    target 156
  ]
  edge [
    source 17
    target 151
  ]
  edge [
    source 17
    target 157
  ]
  edge [
    source 17
    target 40
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 158
  ]
  edge [
    source 19
    target 51
  ]
  edge [
    source 19
    target 159
  ]
  edge [
    source 19
    target 160
  ]
  edge [
    source 19
    target 161
  ]
  edge [
    source 19
    target 162
  ]
  edge [
    source 19
    target 163
  ]
  edge [
    source 19
    target 164
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 165
  ]
  edge [
    source 20
    target 166
  ]
  edge [
    source 20
    target 167
  ]
  edge [
    source 20
    target 168
  ]
  edge [
    source 20
    target 169
  ]
  edge [
    source 20
    target 170
  ]
  edge [
    source 20
    target 171
  ]
  edge [
    source 20
    target 172
  ]
  edge [
    source 20
    target 173
  ]
  edge [
    source 20
    target 174
  ]
  edge [
    source 20
    target 175
  ]
  edge [
    source 20
    target 176
  ]
  edge [
    source 20
    target 44
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 177
  ]
  edge [
    source 21
    target 178
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 179
  ]
  edge [
    source 22
    target 142
  ]
  edge [
    source 22
    target 27
  ]
  edge [
    source 22
    target 180
  ]
  edge [
    source 22
    target 181
  ]
  edge [
    source 22
    target 182
  ]
  edge [
    source 22
    target 183
  ]
  edge [
    source 22
    target 184
  ]
  edge [
    source 22
    target 185
  ]
  edge [
    source 22
    target 186
  ]
  edge [
    source 22
    target 187
  ]
  edge [
    source 22
    target 188
  ]
  edge [
    source 22
    target 189
  ]
  edge [
    source 22
    target 46
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 190
  ]
  edge [
    source 23
    target 191
  ]
  edge [
    source 23
    target 192
  ]
  edge [
    source 23
    target 193
  ]
  edge [
    source 23
    target 194
  ]
  edge [
    source 23
    target 195
  ]
  edge [
    source 23
    target 196
  ]
  edge [
    source 23
    target 197
  ]
  edge [
    source 23
    target 198
  ]
  edge [
    source 23
    target 199
  ]
  edge [
    source 23
    target 200
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 41
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 201
  ]
  edge [
    source 26
    target 202
  ]
  edge [
    source 26
    target 203
  ]
  edge [
    source 26
    target 204
  ]
  edge [
    source 26
    target 205
  ]
  edge [
    source 26
    target 206
  ]
  edge [
    source 27
    target 207
  ]
  edge [
    source 27
    target 208
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 209
  ]
  edge [
    source 29
    target 210
  ]
  edge [
    source 29
    target 211
  ]
  edge [
    source 29
    target 212
  ]
  edge [
    source 29
    target 213
  ]
  edge [
    source 29
    target 214
  ]
  edge [
    source 29
    target 215
  ]
  edge [
    source 29
    target 216
  ]
  edge [
    source 30
    target 37
  ]
  edge [
    source 30
    target 38
  ]
  edge [
    source 30
    target 210
  ]
  edge [
    source 30
    target 158
  ]
  edge [
    source 30
    target 51
  ]
  edge [
    source 30
    target 217
  ]
  edge [
    source 30
    target 218
  ]
  edge [
    source 30
    target 219
  ]
  edge [
    source 30
    target 220
  ]
  edge [
    source 30
    target 221
  ]
  edge [
    source 30
    target 222
  ]
  edge [
    source 30
    target 223
  ]
  edge [
    source 30
    target 224
  ]
  edge [
    source 30
    target 225
  ]
  edge [
    source 30
    target 226
  ]
  edge [
    source 30
    target 160
  ]
  edge [
    source 30
    target 227
  ]
  edge [
    source 30
    target 228
  ]
  edge [
    source 30
    target 229
  ]
  edge [
    source 30
    target 230
  ]
  edge [
    source 30
    target 231
  ]
  edge [
    source 30
    target 232
  ]
  edge [
    source 30
    target 233
  ]
  edge [
    source 30
    target 234
  ]
  edge [
    source 30
    target 235
  ]
  edge [
    source 30
    target 236
  ]
  edge [
    source 30
    target 159
  ]
  edge [
    source 30
    target 162
  ]
  edge [
    source 30
    target 163
  ]
  edge [
    source 30
    target 237
  ]
  edge [
    source 30
    target 238
  ]
  edge [
    source 30
    target 161
  ]
  edge [
    source 30
    target 239
  ]
  edge [
    source 30
    target 240
  ]
  edge [
    source 30
    target 241
  ]
  edge [
    source 30
    target 242
  ]
  edge [
    source 30
    target 243
  ]
  edge [
    source 30
    target 164
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 244
  ]
  edge [
    source 33
    target 245
  ]
  edge [
    source 33
    target 246
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 210
  ]
  edge [
    source 35
    target 247
  ]
  edge [
    source 35
    target 248
  ]
  edge [
    source 35
    target 249
  ]
  edge [
    source 35
    target 250
  ]
  edge [
    source 35
    target 251
  ]
  edge [
    source 35
    target 252
  ]
  edge [
    source 35
    target 253
  ]
  edge [
    source 35
    target 254
  ]
  edge [
    source 35
    target 77
  ]
  edge [
    source 35
    target 255
  ]
  edge [
    source 35
    target 256
  ]
  edge [
    source 35
    target 257
  ]
  edge [
    source 35
    target 258
  ]
  edge [
    source 35
    target 259
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 260
  ]
  edge [
    source 36
    target 43
  ]
  edge [
    source 36
    target 256
  ]
  edge [
    source 37
    target 261
  ]
  edge [
    source 37
    target 262
  ]
  edge [
    source 37
    target 263
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 264
  ]
  edge [
    source 39
    target 265
  ]
  edge [
    source 39
    target 266
  ]
  edge [
    source 39
    target 267
  ]
  edge [
    source 39
    target 268
  ]
  edge [
    source 39
    target 269
  ]
  edge [
    source 39
    target 270
  ]
  edge [
    source 39
    target 271
  ]
  edge [
    source 39
    target 272
  ]
  edge [
    source 39
    target 273
  ]
  edge [
    source 39
    target 274
  ]
  edge [
    source 39
    target 57
  ]
  edge [
    source 39
    target 275
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 276
  ]
  edge [
    source 41
    target 277
  ]
  edge [
    source 41
    target 278
  ]
  edge [
    source 41
    target 279
  ]
  edge [
    source 41
    target 280
  ]
  edge [
    source 41
    target 281
  ]
  edge [
    source 41
    target 282
  ]
  edge [
    source 41
    target 283
  ]
  edge [
    source 41
    target 284
  ]
  edge [
    source 41
    target 285
  ]
  edge [
    source 41
    target 286
  ]
  edge [
    source 41
    target 287
  ]
  edge [
    source 41
    target 288
  ]
  edge [
    source 41
    target 289
  ]
  edge [
    source 41
    target 290
  ]
  edge [
    source 41
    target 291
  ]
  edge [
    source 41
    target 292
  ]
  edge [
    source 41
    target 293
  ]
  edge [
    source 41
    target 294
  ]
  edge [
    source 41
    target 295
  ]
  edge [
    source 41
    target 296
  ]
  edge [
    source 41
    target 297
  ]
  edge [
    source 41
    target 298
  ]
  edge [
    source 41
    target 299
  ]
  edge [
    source 41
    target 300
  ]
  edge [
    source 41
    target 301
  ]
  edge [
    source 41
    target 302
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 303
  ]
  edge [
    source 42
    target 304
  ]
  edge [
    source 42
    target 305
  ]
  edge [
    source 42
    target 306
  ]
  edge [
    source 42
    target 307
  ]
  edge [
    source 42
    target 308
  ]
  edge [
    source 42
    target 309
  ]
  edge [
    source 42
    target 310
  ]
  edge [
    source 42
    target 311
  ]
  edge [
    source 42
    target 312
  ]
  edge [
    source 42
    target 313
  ]
  edge [
    source 42
    target 314
  ]
  edge [
    source 42
    target 315
  ]
  edge [
    source 42
    target 316
  ]
  edge [
    source 42
    target 317
  ]
  edge [
    source 42
    target 318
  ]
  edge [
    source 42
    target 319
  ]
  edge [
    source 42
    target 320
  ]
  edge [
    source 42
    target 321
  ]
  edge [
    source 42
    target 322
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 323
  ]
  edge [
    source 43
    target 324
  ]
  edge [
    source 43
    target 325
  ]
  edge [
    source 43
    target 326
  ]
  edge [
    source 43
    target 327
  ]
  edge [
    source 43
    target 328
  ]
  edge [
    source 43
    target 329
  ]
  edge [
    source 43
    target 330
  ]
  edge [
    source 43
    target 77
  ]
  edge [
    source 43
    target 331
  ]
  edge [
    source 43
    target 332
  ]
  edge [
    source 43
    target 333
  ]
  edge [
    source 43
    target 334
  ]
  edge [
    source 43
    target 335
  ]
  edge [
    source 43
    target 336
  ]
  edge [
    source 43
    target 337
  ]
  edge [
    source 43
    target 338
  ]
  edge [
    source 43
    target 339
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 340
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 341
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 342
  ]
  edge [
    source 46
    target 343
  ]
  edge [
    source 46
    target 344
  ]
  edge [
    source 46
    target 345
  ]
  edge [
    source 46
    target 346
  ]
  edge [
    source 46
    target 347
  ]
  edge [
    source 46
    target 348
  ]
  edge [
    source 46
    target 349
  ]
  edge [
    source 46
    target 350
  ]
  edge [
    source 46
    target 351
  ]
  edge [
    source 46
    target 352
  ]
  edge [
    source 46
    target 353
  ]
  edge [
    source 46
    target 354
  ]
  edge [
    source 46
    target 355
  ]
  edge [
    source 46
    target 356
  ]
  edge [
    source 46
    target 357
  ]
  edge [
    source 46
    target 358
  ]
  edge [
    source 46
    target 359
  ]
  edge [
    source 46
    target 360
  ]
  edge [
    source 46
    target 361
  ]
  edge [
    source 46
    target 362
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 210
  ]
  edge [
    source 47
    target 363
  ]
  edge [
    source 47
    target 364
  ]
  edge [
    source 47
    target 365
  ]
  edge [
    source 47
    target 366
  ]
  edge [
    source 47
    target 367
  ]
  edge [
    source 47
    target 368
  ]
  edge [
    source 47
    target 369
  ]
  edge [
    source 47
    target 370
  ]
  edge [
    source 47
    target 371
  ]
  edge [
    source 47
    target 372
  ]
  edge [
    source 47
    target 373
  ]
  edge [
    source 47
    target 374
  ]
  edge [
    source 47
    target 375
  ]
  edge [
    source 47
    target 376
  ]
  edge [
    source 47
    target 377
  ]
  edge [
    source 47
    target 378
  ]
  edge [
    source 47
    target 379
  ]
  edge [
    source 47
    target 380
  ]
  edge [
    source 47
    target 381
  ]
  edge [
    source 47
    target 382
  ]
  edge [
    source 47
    target 383
  ]
  edge [
    source 47
    target 384
  ]
  edge [
    source 47
    target 385
  ]
  edge [
    source 47
    target 386
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 387
  ]
  edge [
    source 48
    target 388
  ]
]
