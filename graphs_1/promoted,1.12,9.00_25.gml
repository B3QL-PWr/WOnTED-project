graph [
  maxDegree 16
  minDegree 1
  meanDegree 1.92
  density 0.08
  graphCliqueNumber 2
  node [
    id 0
    label "cze&#347;&#263;"
    origin "text"
  ]
  node [
    id 1
    label "drogi"
    origin "text"
  ]
  node [
    id 2
    label "mirka"
    origin "text"
  ]
  node [
    id 3
    label "zaimponowanie"
  ]
  node [
    id 4
    label "szanowa&#263;"
  ]
  node [
    id 5
    label "uhonorowa&#263;"
  ]
  node [
    id 6
    label "honorowanie"
  ]
  node [
    id 7
    label "uszanowa&#263;"
  ]
  node [
    id 8
    label "rewerencja"
  ]
  node [
    id 9
    label "uszanowanie"
  ]
  node [
    id 10
    label "imponowanie"
  ]
  node [
    id 11
    label "dobro"
  ]
  node [
    id 12
    label "uhonorowanie"
  ]
  node [
    id 13
    label "respect"
  ]
  node [
    id 14
    label "honorowa&#263;"
  ]
  node [
    id 15
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 16
    label "szacuneczek"
  ]
  node [
    id 17
    label "postawa"
  ]
  node [
    id 18
    label "cz&#322;owiek"
  ]
  node [
    id 19
    label "warto&#347;ciowy"
  ]
  node [
    id 20
    label "bliski"
  ]
  node [
    id 21
    label "drogo"
  ]
  node [
    id 22
    label "przyjaciel"
  ]
  node [
    id 23
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 24
    label "mi&#322;y"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
]
