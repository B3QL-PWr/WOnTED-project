graph [
  maxDegree 13
  minDegree 1
  meanDegree 1.9259259259259258
  density 0.07407407407407407
  graphCliqueNumber 2
  node [
    id 0
    label "myslalem"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "udo"
    origin "text"
  ]
  node [
    id 3
    label "by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "zadowolony"
    origin "text"
  ]
  node [
    id 5
    label "kr&#281;tarz"
  ]
  node [
    id 6
    label "mi&#281;sie&#324;_czworog&#322;owy"
  ]
  node [
    id 7
    label "mi&#281;sie&#324;_dwug&#322;owy_uda"
  ]
  node [
    id 8
    label "struktura_anatomiczna"
  ]
  node [
    id 9
    label "t&#281;tnica_udowa"
  ]
  node [
    id 10
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 11
    label "noga"
  ]
  node [
    id 12
    label "si&#281;ga&#263;"
  ]
  node [
    id 13
    label "trwa&#263;"
  ]
  node [
    id 14
    label "obecno&#347;&#263;"
  ]
  node [
    id 15
    label "stan"
  ]
  node [
    id 16
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 17
    label "stand"
  ]
  node [
    id 18
    label "mie&#263;_miejsce"
  ]
  node [
    id 19
    label "uczestniczy&#263;"
  ]
  node [
    id 20
    label "chodzi&#263;"
  ]
  node [
    id 21
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 22
    label "equal"
  ]
  node [
    id 23
    label "u&#347;miechni&#281;ty"
  ]
  node [
    id 24
    label "usatysfakcjonowanie_si&#281;"
  ]
  node [
    id 25
    label "zadowolenie_si&#281;"
  ]
  node [
    id 26
    label "pogodny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
]
