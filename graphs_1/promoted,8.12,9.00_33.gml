graph [
  maxDegree 28
  minDegree 1
  meanDegree 1.9361702127659575
  density 0.02081903454587051
  graphCliqueNumber 3
  node [
    id 0
    label "bandyta"
    origin "text"
  ]
  node [
    id 1
    label "&#347;ciga&#263;"
    origin "text"
  ]
  node [
    id 2
    label "ameryka&#324;ski"
    origin "text"
  ]
  node [
    id 3
    label "para"
    origin "text"
  ]
  node [
    id 4
    label "zakurzy&#263;"
    origin "text"
  ]
  node [
    id 5
    label "droga"
    origin "text"
  ]
  node [
    id 6
    label "kenia"
    origin "text"
  ]
  node [
    id 7
    label "apasz"
  ]
  node [
    id 8
    label "przest&#281;pca"
  ]
  node [
    id 9
    label "typ_spod_ciemnej_gwiazdy"
  ]
  node [
    id 10
    label "up&#281;dza&#263;_si&#281;"
  ]
  node [
    id 11
    label "straszy&#263;"
  ]
  node [
    id 12
    label "prosecute"
  ]
  node [
    id 13
    label "kara&#263;"
  ]
  node [
    id 14
    label "usi&#322;owa&#263;"
  ]
  node [
    id 15
    label "poszukiwa&#263;"
  ]
  node [
    id 16
    label "nowoczesny"
  ]
  node [
    id 17
    label "p&#243;&#322;nocnoameryka&#324;ski"
  ]
  node [
    id 18
    label "boston"
  ]
  node [
    id 19
    label "po_ameryka&#324;sku"
  ]
  node [
    id 20
    label "cake-walk"
  ]
  node [
    id 21
    label "charakterystyczny"
  ]
  node [
    id 22
    label "placek_ameryka&#324;ski"
  ]
  node [
    id 23
    label "fajny"
  ]
  node [
    id 24
    label "j&#281;zyk_angielski"
  ]
  node [
    id 25
    label "Princeton"
  ]
  node [
    id 26
    label "pepperoni"
  ]
  node [
    id 27
    label "sa&#322;atka_Snickers"
  ]
  node [
    id 28
    label "zachodni"
  ]
  node [
    id 29
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 30
    label "anglosaski"
  ]
  node [
    id 31
    label "typowy"
  ]
  node [
    id 32
    label "gaz_cieplarniany"
  ]
  node [
    id 33
    label "grupa"
  ]
  node [
    id 34
    label "smoke"
  ]
  node [
    id 35
    label "pair"
  ]
  node [
    id 36
    label "sztuka"
  ]
  node [
    id 37
    label "Albania"
  ]
  node [
    id 38
    label "dodatek"
  ]
  node [
    id 39
    label "odparowanie"
  ]
  node [
    id 40
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 41
    label "odparowywa&#263;"
  ]
  node [
    id 42
    label "nale&#380;e&#263;"
  ]
  node [
    id 43
    label "wyparowanie"
  ]
  node [
    id 44
    label "zesp&#243;&#322;"
  ]
  node [
    id 45
    label "parowanie"
  ]
  node [
    id 46
    label "damp"
  ]
  node [
    id 47
    label "odparowywanie"
  ]
  node [
    id 48
    label "poker"
  ]
  node [
    id 49
    label "moneta"
  ]
  node [
    id 50
    label "odparowa&#263;"
  ]
  node [
    id 51
    label "jednostka_monetarna"
  ]
  node [
    id 52
    label "uk&#322;ad"
  ]
  node [
    id 53
    label "gaz"
  ]
  node [
    id 54
    label "chodzi&#263;"
  ]
  node [
    id 55
    label "zbi&#243;r"
  ]
  node [
    id 56
    label "Imperium_Osma&#324;skie"
  ]
  node [
    id 57
    label "inflame"
  ]
  node [
    id 58
    label "zrobi&#263;"
  ]
  node [
    id 59
    label "zabrudzi&#263;"
  ]
  node [
    id 60
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 61
    label "journey"
  ]
  node [
    id 62
    label "podbieg"
  ]
  node [
    id 63
    label "bezsilnikowy"
  ]
  node [
    id 64
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 65
    label "wylot"
  ]
  node [
    id 66
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 67
    label "drogowskaz"
  ]
  node [
    id 68
    label "nawierzchnia"
  ]
  node [
    id 69
    label "turystyka"
  ]
  node [
    id 70
    label "budowla"
  ]
  node [
    id 71
    label "spos&#243;b"
  ]
  node [
    id 72
    label "passage"
  ]
  node [
    id 73
    label "marszrutyzacja"
  ]
  node [
    id 74
    label "zbior&#243;wka"
  ]
  node [
    id 75
    label "ekskursja"
  ]
  node [
    id 76
    label "rajza"
  ]
  node [
    id 77
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 78
    label "ruch"
  ]
  node [
    id 79
    label "trasa"
  ]
  node [
    id 80
    label "wyb&#243;j"
  ]
  node [
    id 81
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 82
    label "ekwipunek"
  ]
  node [
    id 83
    label "korona_drogi"
  ]
  node [
    id 84
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 85
    label "pobocze"
  ]
  node [
    id 86
    label "herbata_czarna"
  ]
  node [
    id 87
    label "Bryant"
  ]
  node [
    id 88
    label "Swenson"
  ]
  node [
    id 89
    label "Maja"
  ]
  node [
    id 90
    label "Mahiu"
  ]
  node [
    id 91
    label "Lauren"
  ]
  node [
    id 92
    label "z"
  ]
  node [
    id 93
    label "Utah"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 87
    target 88
  ]
  edge [
    source 89
    target 90
  ]
  edge [
    source 91
    target 92
  ]
  edge [
    source 91
    target 93
  ]
  edge [
    source 92
    target 93
  ]
]
