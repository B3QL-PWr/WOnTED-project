graph [
  maxDegree 46
  minDegree 1
  meanDegree 2.0597014925373136
  density 0.010298507462686568
  graphCliqueNumber 3
  node [
    id 0
    label "towarzystwo"
    origin "text"
  ]
  node [
    id 1
    label "przyjaciel"
    origin "text"
  ]
  node [
    id 2
    label "ozorkowy"
    origin "text"
  ]
  node [
    id 3
    label "zaprasza&#263;"
    origin "text"
  ]
  node [
    id 4
    label "historyczny"
    origin "text"
  ]
  node [
    id 5
    label "spacer"
    origin "text"
  ]
  node [
    id 6
    label "ozorkowie"
    origin "text"
  ]
  node [
    id 7
    label "tym"
    origin "text"
  ]
  node [
    id 8
    label "razem"
    origin "text"
  ]
  node [
    id 9
    label "pozna&#263;"
    origin "text"
  ]
  node [
    id 10
    label "historia"
    origin "text"
  ]
  node [
    id 11
    label "parafia"
    origin "text"
  ]
  node [
    id 12
    label "ko&#347;ci&#243;&#322;"
    origin "text"
  ]
  node [
    id 13
    label "ewangelicko"
    origin "text"
  ]
  node [
    id 14
    label "augsburski"
    origin "text"
  ]
  node [
    id 15
    label "pan"
    origin "text"
  ]
  node [
    id 16
    label "aposto&#322;"
    origin "text"
  ]
  node [
    id 17
    label "jezus"
    origin "text"
  ]
  node [
    id 18
    label "chrystus"
    origin "text"
  ]
  node [
    id 19
    label "zbi&#243;rka"
    origin "text"
  ]
  node [
    id 20
    label "przed"
    origin "text"
  ]
  node [
    id 21
    label "ula"
    origin "text"
  ]
  node [
    id 22
    label "&#347;rednia"
    origin "text"
  ]
  node [
    id 23
    label "maj"
    origin "text"
  ]
  node [
    id 24
    label "godz"
    origin "text"
  ]
  node [
    id 25
    label "Ku&#378;nica_Ko&#322;&#322;&#261;tajowska"
  ]
  node [
    id 26
    label "obecno&#347;&#263;"
  ]
  node [
    id 27
    label "Krakowska_Kongregacja_Kupiecka"
  ]
  node [
    id 28
    label "organizacja"
  ]
  node [
    id 29
    label "Eleusis"
  ]
  node [
    id 30
    label "asystencja"
  ]
  node [
    id 31
    label "Rzeczpospolita_babi&#324;ska"
  ]
  node [
    id 32
    label "Polskie_Towarzystwo_Turystyczno-Krajoznawcze"
  ]
  node [
    id 33
    label "grupa"
  ]
  node [
    id 34
    label "fabianie"
  ]
  node [
    id 35
    label "Chewra_Kadisza"
  ]
  node [
    id 36
    label "grono"
  ]
  node [
    id 37
    label "umowa_stowarzyszeniowa"
  ]
  node [
    id 38
    label "wi&#281;&#378;"
  ]
  node [
    id 39
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 40
    label "partnership"
  ]
  node [
    id 41
    label "Monar"
  ]
  node [
    id 42
    label "Rotary_International"
  ]
  node [
    id 43
    label "kochanek"
  ]
  node [
    id 44
    label "zaprzyja&#378;nianie_si&#281;"
  ]
  node [
    id 45
    label "przyja&#378;nienie_si&#281;"
  ]
  node [
    id 46
    label "kum"
  ]
  node [
    id 47
    label "sympatyk"
  ]
  node [
    id 48
    label "bratnia_dusza"
  ]
  node [
    id 49
    label "amikus"
  ]
  node [
    id 50
    label "zaprzyja&#378;nienie_si&#281;"
  ]
  node [
    id 51
    label "pobratymiec"
  ]
  node [
    id 52
    label "drogi"
  ]
  node [
    id 53
    label "mi&#281;sny"
  ]
  node [
    id 54
    label "invite"
  ]
  node [
    id 55
    label "ask"
  ]
  node [
    id 56
    label "oferowa&#263;"
  ]
  node [
    id 57
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 58
    label "dawny"
  ]
  node [
    id 59
    label "zgodny"
  ]
  node [
    id 60
    label "historycznie"
  ]
  node [
    id 61
    label "wiekopomny"
  ]
  node [
    id 62
    label "prawdziwy"
  ]
  node [
    id 63
    label "dziejowo"
  ]
  node [
    id 64
    label "prezentacja"
  ]
  node [
    id 65
    label "natural_process"
  ]
  node [
    id 66
    label "czynno&#347;&#263;"
  ]
  node [
    id 67
    label "ruch"
  ]
  node [
    id 68
    label "&#322;&#261;cznie"
  ]
  node [
    id 69
    label "przyswoi&#263;"
  ]
  node [
    id 70
    label "feel"
  ]
  node [
    id 71
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 72
    label "teach"
  ]
  node [
    id 73
    label "zrozumie&#263;"
  ]
  node [
    id 74
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 75
    label "rozr&#243;&#380;ni&#263;"
  ]
  node [
    id 76
    label "topographic_point"
  ]
  node [
    id 77
    label "experience"
  ]
  node [
    id 78
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 79
    label "domy&#347;li&#263;_si&#281;"
  ]
  node [
    id 80
    label "visualize"
  ]
  node [
    id 81
    label "report"
  ]
  node [
    id 82
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 83
    label "wypowied&#378;"
  ]
  node [
    id 84
    label "neografia"
  ]
  node [
    id 85
    label "przedmiot"
  ]
  node [
    id 86
    label "papirologia"
  ]
  node [
    id 87
    label "historia_gospodarcza"
  ]
  node [
    id 88
    label "przebiec"
  ]
  node [
    id 89
    label "hista"
  ]
  node [
    id 90
    label "nauka_humanistyczna"
  ]
  node [
    id 91
    label "filigranistyka"
  ]
  node [
    id 92
    label "dyplomatyka"
  ]
  node [
    id 93
    label "annalistyka"
  ]
  node [
    id 94
    label "historyka"
  ]
  node [
    id 95
    label "heraldyka"
  ]
  node [
    id 96
    label "fabu&#322;a"
  ]
  node [
    id 97
    label "muzealnictwo"
  ]
  node [
    id 98
    label "varsavianistyka"
  ]
  node [
    id 99
    label "prezentyzm"
  ]
  node [
    id 100
    label "mediewistyka"
  ]
  node [
    id 101
    label "przebiegni&#281;cie"
  ]
  node [
    id 102
    label "charakter"
  ]
  node [
    id 103
    label "paleografia"
  ]
  node [
    id 104
    label "genealogia"
  ]
  node [
    id 105
    label "prozopografia"
  ]
  node [
    id 106
    label "motyw"
  ]
  node [
    id 107
    label "nautologia"
  ]
  node [
    id 108
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 109
    label "epoka"
  ]
  node [
    id 110
    label "numizmatyka"
  ]
  node [
    id 111
    label "ruralistyka"
  ]
  node [
    id 112
    label "epigrafika"
  ]
  node [
    id 113
    label "&#378;r&#243;d&#322;oznawstwo"
  ]
  node [
    id 114
    label "historiografia"
  ]
  node [
    id 115
    label "bizantynistyka"
  ]
  node [
    id 116
    label "weksylologia"
  ]
  node [
    id 117
    label "kierunek"
  ]
  node [
    id 118
    label "ikonografia"
  ]
  node [
    id 119
    label "chronologia"
  ]
  node [
    id 120
    label "archiwistyka"
  ]
  node [
    id 121
    label "sfragistyka"
  ]
  node [
    id 122
    label "zabytkoznawstwo"
  ]
  node [
    id 123
    label "historia_sztuki"
  ]
  node [
    id 124
    label "rada_parafialna"
  ]
  node [
    id 125
    label "parochia"
  ]
  node [
    id 126
    label "zabudowania"
  ]
  node [
    id 127
    label "dekanat"
  ]
  node [
    id 128
    label "parafianin"
  ]
  node [
    id 129
    label "diecezja"
  ]
  node [
    id 130
    label "&#347;wi&#261;tynia"
  ]
  node [
    id 131
    label "zakrystia"
  ]
  node [
    id 132
    label "organizacja_religijna"
  ]
  node [
    id 133
    label "nawa"
  ]
  node [
    id 134
    label "nerwica_eklezjogenna"
  ]
  node [
    id 135
    label "prezbiterium"
  ]
  node [
    id 136
    label "kropielnica"
  ]
  node [
    id 137
    label "wsp&#243;lnota"
  ]
  node [
    id 138
    label "church"
  ]
  node [
    id 139
    label "kruchta"
  ]
  node [
    id 140
    label "Ska&#322;ka"
  ]
  node [
    id 141
    label "kult"
  ]
  node [
    id 142
    label "ub&#322;agalnia"
  ]
  node [
    id 143
    label "dom"
  ]
  node [
    id 144
    label "cz&#322;owiek"
  ]
  node [
    id 145
    label "profesor"
  ]
  node [
    id 146
    label "kszta&#322;ciciel"
  ]
  node [
    id 147
    label "jegomo&#347;&#263;"
  ]
  node [
    id 148
    label "zwrot"
  ]
  node [
    id 149
    label "pracodawca"
  ]
  node [
    id 150
    label "rz&#261;dzenie"
  ]
  node [
    id 151
    label "m&#261;&#380;"
  ]
  node [
    id 152
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 153
    label "ch&#322;opina"
  ]
  node [
    id 154
    label "bratek"
  ]
  node [
    id 155
    label "opiekun"
  ]
  node [
    id 156
    label "doros&#322;y"
  ]
  node [
    id 157
    label "preceptor"
  ]
  node [
    id 158
    label "Midas"
  ]
  node [
    id 159
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 160
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 161
    label "murza"
  ]
  node [
    id 162
    label "ojciec"
  ]
  node [
    id 163
    label "androlog"
  ]
  node [
    id 164
    label "pupil"
  ]
  node [
    id 165
    label "efendi"
  ]
  node [
    id 166
    label "nabab"
  ]
  node [
    id 167
    label "w&#322;odarz"
  ]
  node [
    id 168
    label "szkolnik"
  ]
  node [
    id 169
    label "pedagog"
  ]
  node [
    id 170
    label "popularyzator"
  ]
  node [
    id 171
    label "andropauza"
  ]
  node [
    id 172
    label "gra_w_karty"
  ]
  node [
    id 173
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 174
    label "Mieszko_I"
  ]
  node [
    id 175
    label "bogaty"
  ]
  node [
    id 176
    label "samiec"
  ]
  node [
    id 177
    label "przyw&#243;dca"
  ]
  node [
    id 178
    label "pa&#324;stwo"
  ]
  node [
    id 179
    label "belfer"
  ]
  node [
    id 180
    label "zwolennik"
  ]
  node [
    id 181
    label "chor&#261;&#380;y"
  ]
  node [
    id 182
    label "tuba"
  ]
  node [
    id 183
    label "ptak"
  ]
  node [
    id 184
    label "ucze&#324;"
  ]
  node [
    id 185
    label "&#347;w"
  ]
  node [
    id 186
    label "g&#322;osiciel"
  ]
  node [
    id 187
    label "Judasz"
  ]
  node [
    id 188
    label "ska&#322;owrony"
  ]
  node [
    id 189
    label "rozsiewca"
  ]
  node [
    id 190
    label "kwestowanie"
  ]
  node [
    id 191
    label "apel"
  ]
  node [
    id 192
    label "koszyk&#243;wka"
  ]
  node [
    id 193
    label "recoil"
  ]
  node [
    id 194
    label "spotkanie"
  ]
  node [
    id 195
    label "kwestarz"
  ]
  node [
    id 196
    label "collection"
  ]
  node [
    id 197
    label "chwyt"
  ]
  node [
    id 198
    label "miara_tendencji_centralnej"
  ]
  node [
    id 199
    label "&#346;wi&#281;to_Pracy"
  ]
  node [
    id 200
    label "miesi&#261;c"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 10
    target 105
  ]
  edge [
    source 10
    target 106
  ]
  edge [
    source 10
    target 107
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 10
    target 110
  ]
  edge [
    source 10
    target 111
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 10
    target 121
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 124
  ]
  edge [
    source 11
    target 125
  ]
  edge [
    source 11
    target 126
  ]
  edge [
    source 11
    target 127
  ]
  edge [
    source 11
    target 128
  ]
  edge [
    source 11
    target 129
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 20
  ]
  edge [
    source 12
    target 21
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 12
    target 131
  ]
  edge [
    source 12
    target 132
  ]
  edge [
    source 12
    target 133
  ]
  edge [
    source 12
    target 134
  ]
  edge [
    source 12
    target 135
  ]
  edge [
    source 12
    target 136
  ]
  edge [
    source 12
    target 137
  ]
  edge [
    source 12
    target 138
  ]
  edge [
    source 12
    target 139
  ]
  edge [
    source 12
    target 140
  ]
  edge [
    source 12
    target 141
  ]
  edge [
    source 12
    target 142
  ]
  edge [
    source 12
    target 143
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 144
  ]
  edge [
    source 15
    target 145
  ]
  edge [
    source 15
    target 146
  ]
  edge [
    source 15
    target 147
  ]
  edge [
    source 15
    target 148
  ]
  edge [
    source 15
    target 149
  ]
  edge [
    source 15
    target 150
  ]
  edge [
    source 15
    target 151
  ]
  edge [
    source 15
    target 152
  ]
  edge [
    source 15
    target 153
  ]
  edge [
    source 15
    target 154
  ]
  edge [
    source 15
    target 155
  ]
  edge [
    source 15
    target 156
  ]
  edge [
    source 15
    target 157
  ]
  edge [
    source 15
    target 158
  ]
  edge [
    source 15
    target 159
  ]
  edge [
    source 15
    target 160
  ]
  edge [
    source 15
    target 161
  ]
  edge [
    source 15
    target 162
  ]
  edge [
    source 15
    target 163
  ]
  edge [
    source 15
    target 164
  ]
  edge [
    source 15
    target 165
  ]
  edge [
    source 15
    target 166
  ]
  edge [
    source 15
    target 167
  ]
  edge [
    source 15
    target 168
  ]
  edge [
    source 15
    target 169
  ]
  edge [
    source 15
    target 170
  ]
  edge [
    source 15
    target 171
  ]
  edge [
    source 15
    target 172
  ]
  edge [
    source 15
    target 173
  ]
  edge [
    source 15
    target 174
  ]
  edge [
    source 15
    target 175
  ]
  edge [
    source 15
    target 176
  ]
  edge [
    source 15
    target 177
  ]
  edge [
    source 15
    target 178
  ]
  edge [
    source 15
    target 179
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 180
  ]
  edge [
    source 16
    target 181
  ]
  edge [
    source 16
    target 182
  ]
  edge [
    source 16
    target 183
  ]
  edge [
    source 16
    target 184
  ]
  edge [
    source 16
    target 185
  ]
  edge [
    source 16
    target 186
  ]
  edge [
    source 16
    target 187
  ]
  edge [
    source 16
    target 188
  ]
  edge [
    source 16
    target 189
  ]
  edge [
    source 16
    target 170
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 190
  ]
  edge [
    source 19
    target 191
  ]
  edge [
    source 19
    target 66
  ]
  edge [
    source 19
    target 192
  ]
  edge [
    source 19
    target 193
  ]
  edge [
    source 19
    target 194
  ]
  edge [
    source 19
    target 195
  ]
  edge [
    source 19
    target 196
  ]
  edge [
    source 19
    target 197
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 198
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 199
  ]
  edge [
    source 23
    target 200
  ]
]
