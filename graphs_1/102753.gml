graph [
  maxDegree 9
  minDegree 1
  meanDegree 1.9375
  density 0.0625
  graphCliqueNumber 2
  node [
    id 0
    label "dziennik"
    origin "text"
  ]
  node [
    id 1
    label "urz&#281;dowy"
    origin "text"
  ]
  node [
    id 2
    label "minister"
    origin "text"
  ]
  node [
    id 3
    label "sprawiedliwo&#347;&#263;"
    origin "text"
  ]
  node [
    id 4
    label "grudzie&#324;"
    origin "text"
  ]
  node [
    id 5
    label "poz"
    origin "text"
  ]
  node [
    id 6
    label "spis"
  ]
  node [
    id 7
    label "sheet"
  ]
  node [
    id 8
    label "gazeta"
  ]
  node [
    id 9
    label "diariusz"
  ]
  node [
    id 10
    label "pami&#281;tnik"
  ]
  node [
    id 11
    label "journal"
  ]
  node [
    id 12
    label "ksi&#281;ga"
  ]
  node [
    id 13
    label "program_informacyjny"
  ]
  node [
    id 14
    label "urz&#281;dowo"
  ]
  node [
    id 15
    label "oficjalny"
  ]
  node [
    id 16
    label "formalny"
  ]
  node [
    id 17
    label "Goebbels"
  ]
  node [
    id 18
    label "Sto&#322;ypin"
  ]
  node [
    id 19
    label "rz&#261;d"
  ]
  node [
    id 20
    label "dostojnik"
  ]
  node [
    id 21
    label "konsekwencja"
  ]
  node [
    id 22
    label "punishment"
  ]
  node [
    id 23
    label "cecha"
  ]
  node [
    id 24
    label "roboty_przymusowe"
  ]
  node [
    id 25
    label "nemezis"
  ]
  node [
    id 26
    label "righteousness"
  ]
  node [
    id 27
    label "Bo&#380;e_Narodzenie"
  ]
  node [
    id 28
    label "miesi&#261;c"
  ]
  node [
    id 29
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 30
    label "Barb&#243;rka"
  ]
  node [
    id 31
    label "Sylwester"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
]
