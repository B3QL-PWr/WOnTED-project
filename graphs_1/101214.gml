graph [
  maxDegree 5
  minDegree 1
  meanDegree 3.7777777777777777
  density 0.4722222222222222
  graphCliqueNumber 6
  node [
    id 0
    label "kondygnacja"
    origin "text"
  ]
  node [
    id 1
    label "budynek"
  ]
  node [
    id 2
    label "p&#322;aszczyzna"
  ]
  node [
    id 3
    label "polski"
  ]
  node [
    id 4
    label "norma"
  ]
  node [
    id 5
    label "p&#243;&#322;nocny"
  ]
  node [
    id 6
    label "by&#322;y"
  ]
  node [
    id 7
    label "01025"
  ]
  node [
    id 8
    label "2004"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 4
    target 8
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 7
    target 8
  ]
]
