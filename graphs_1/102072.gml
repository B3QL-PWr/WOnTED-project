graph [
  maxDegree 7
  minDegree 1
  meanDegree 1.875
  density 0.125
  graphCliqueNumber 2
  node [
    id 0
    label "konferencja"
    origin "text"
  ]
  node [
    id 1
    label "sejmowy"
    origin "text"
  ]
  node [
    id 2
    label "zasoby"
    origin "text"
  ]
  node [
    id 3
    label "edukacyjny"
    origin "text"
  ]
  node [
    id 4
    label "polska"
    origin "text"
  ]
  node [
    id 5
    label "konferencyjka"
  ]
  node [
    id 6
    label "Poczdam"
  ]
  node [
    id 7
    label "conference"
  ]
  node [
    id 8
    label "spotkanie"
  ]
  node [
    id 9
    label "grusza_pospolita"
  ]
  node [
    id 10
    label "Ja&#322;ta"
  ]
  node [
    id 11
    label "podmiot_gospodarczy"
  ]
  node [
    id 12
    label "z&#322;o&#380;e"
  ]
  node [
    id 13
    label "zasoby_kopalin"
  ]
  node [
    id 14
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 15
    label "edukacyjnie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 15
  ]
]
