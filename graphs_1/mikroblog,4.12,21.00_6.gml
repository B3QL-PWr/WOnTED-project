graph [
  maxDegree 7
  minDegree 1
  meanDegree 1.75
  density 0.25
  graphCliqueNumber 2
  node [
    id 0
    label "szanowa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "plusujesz"
    origin "text"
  ]
  node [
    id 2
    label "czu&#263;"
  ]
  node [
    id 3
    label "chowa&#263;"
  ]
  node [
    id 4
    label "treasure"
  ]
  node [
    id 5
    label "respektowa&#263;"
  ]
  node [
    id 6
    label "powa&#380;anie"
  ]
  node [
    id 7
    label "wyra&#380;a&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
]
