graph [
  maxDegree 35
  minDegree 1
  meanDegree 2
  density 0.022222222222222223
  graphCliqueNumber 3
  node [
    id 0
    label "wstrz&#261;s"
    origin "text"
  ]
  node [
    id 1
    label "m&#243;zg"
    origin "text"
  ]
  node [
    id 2
    label "bia&#322;y"
    origin "text"
  ]
  node [
    id 3
    label "dziecko"
    origin "text"
  ]
  node [
    id 4
    label "impact"
  ]
  node [
    id 5
    label "oznaka"
  ]
  node [
    id 6
    label "ruch"
  ]
  node [
    id 7
    label "zmiana"
  ]
  node [
    id 8
    label "szok"
  ]
  node [
    id 9
    label "zaburzenie"
  ]
  node [
    id 10
    label "g&#322;adkom&#243;zgowie"
  ]
  node [
    id 11
    label "elektroencefalogram"
  ]
  node [
    id 12
    label "substancja_szara"
  ]
  node [
    id 13
    label "przodom&#243;zgowie"
  ]
  node [
    id 14
    label "bruzda"
  ]
  node [
    id 15
    label "&#347;r&#243;dm&#243;zgowie"
  ]
  node [
    id 16
    label "wzg&#243;rze"
  ]
  node [
    id 17
    label "umys&#322;"
  ]
  node [
    id 18
    label "zw&#243;j"
  ]
  node [
    id 19
    label "kresom&#243;zgowie"
  ]
  node [
    id 20
    label "uk&#322;ad_limbiczny"
  ]
  node [
    id 21
    label "cia&#322;o_migda&#322;owate"
  ]
  node [
    id 22
    label "pie&#324;_m&#243;zgu"
  ]
  node [
    id 23
    label "przysadka"
  ]
  node [
    id 24
    label "wiedza"
  ]
  node [
    id 25
    label "bezzakr&#281;towo&#347;&#263;"
  ]
  node [
    id 26
    label "przedmurze"
  ]
  node [
    id 27
    label "ty&#322;om&#243;zgowie"
  ]
  node [
    id 28
    label "projektodawca"
  ]
  node [
    id 29
    label "noosfera"
  ]
  node [
    id 30
    label "cecha"
  ]
  node [
    id 31
    label "g&#322;owa"
  ]
  node [
    id 32
    label "organ"
  ]
  node [
    id 33
    label "uk&#322;ad_nerwowy"
  ]
  node [
    id 34
    label "most"
  ]
  node [
    id 35
    label "przepuklina_m&#243;zgowa"
  ]
  node [
    id 36
    label "splot_naczyni&#243;wkowy"
  ]
  node [
    id 37
    label "encefalografia"
  ]
  node [
    id 38
    label "mi&#281;dzym&#243;zgowie"
  ]
  node [
    id 39
    label "m&#243;&#380;d&#380;ek"
  ]
  node [
    id 40
    label "kora_m&#243;zgowa"
  ]
  node [
    id 41
    label "podwzg&#243;rze"
  ]
  node [
    id 42
    label "poduszka"
  ]
  node [
    id 43
    label "cz&#322;owiek"
  ]
  node [
    id 44
    label "jasny"
  ]
  node [
    id 45
    label "typ_orientalny"
  ]
  node [
    id 46
    label "bezbarwny"
  ]
  node [
    id 47
    label "blady"
  ]
  node [
    id 48
    label "bierka_szachowa"
  ]
  node [
    id 49
    label "bia&#322;osk&#243;ry"
  ]
  node [
    id 50
    label "czysty"
  ]
  node [
    id 51
    label "Rosjanin"
  ]
  node [
    id 52
    label "siwy"
  ]
  node [
    id 53
    label "jasnosk&#243;ry"
  ]
  node [
    id 54
    label "bia&#322;as"
  ]
  node [
    id 55
    label "&#347;nie&#380;nie"
  ]
  node [
    id 56
    label "bia&#322;e"
  ]
  node [
    id 57
    label "nacjonalista"
  ]
  node [
    id 58
    label "bia&#322;a_n&#281;dza"
  ]
  node [
    id 59
    label "bia&#322;y_taniec"
  ]
  node [
    id 60
    label "konserwatysta"
  ]
  node [
    id 61
    label "dzia&#322;acz"
  ]
  node [
    id 62
    label "medyczny"
  ]
  node [
    id 63
    label "s&#322;u&#380;ba_zdrowia"
  ]
  node [
    id 64
    label "&#347;nie&#380;no"
  ]
  node [
    id 65
    label "bia&#322;o"
  ]
  node [
    id 66
    label "carat"
  ]
  node [
    id 67
    label "Polak"
  ]
  node [
    id 68
    label "bia&#322;y_murzyn"
  ]
  node [
    id 69
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 70
    label "dobry"
  ]
  node [
    id 71
    label "libera&#322;"
  ]
  node [
    id 72
    label "potomstwo"
  ]
  node [
    id 73
    label "organizm"
  ]
  node [
    id 74
    label "sraluch"
  ]
  node [
    id 75
    label "utulanie"
  ]
  node [
    id 76
    label "pediatra"
  ]
  node [
    id 77
    label "dzieciarnia"
  ]
  node [
    id 78
    label "m&#322;odziak"
  ]
  node [
    id 79
    label "dzieciak"
  ]
  node [
    id 80
    label "utula&#263;"
  ]
  node [
    id 81
    label "potomek"
  ]
  node [
    id 82
    label "pedofil"
  ]
  node [
    id 83
    label "entliczek-pentliczek"
  ]
  node [
    id 84
    label "m&#322;odzik"
  ]
  node [
    id 85
    label "cz&#322;owieczek"
  ]
  node [
    id 86
    label "zwierz&#281;"
  ]
  node [
    id 87
    label "niepe&#322;noletni"
  ]
  node [
    id 88
    label "fledgling"
  ]
  node [
    id 89
    label "utuli&#263;"
  ]
  node [
    id 90
    label "utulenie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
]
