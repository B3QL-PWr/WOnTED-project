graph [
  maxDegree 21
  minDegree 1
  meanDegree 1.935483870967742
  density 0.06451612903225806
  graphCliqueNumber 2
  node [
    id 0
    label "nietypowy"
    origin "text"
  ]
  node [
    id 1
    label "zachowanie"
    origin "text"
  ]
  node [
    id 2
    label "kur"
    origin "text"
  ]
  node [
    id 3
    label "inny"
  ]
  node [
    id 4
    label "nietypowo"
  ]
  node [
    id 5
    label "zwierz&#281;"
  ]
  node [
    id 6
    label "zrobienie"
  ]
  node [
    id 7
    label "podporz&#261;dkowanie_si&#281;"
  ]
  node [
    id 8
    label "podtrzymanie"
  ]
  node [
    id 9
    label "reakcja"
  ]
  node [
    id 10
    label "tajemnica"
  ]
  node [
    id 11
    label "zdyscyplinowanie"
  ]
  node [
    id 12
    label "observation"
  ]
  node [
    id 13
    label "behawior"
  ]
  node [
    id 14
    label "dieta"
  ]
  node [
    id 15
    label "bearing"
  ]
  node [
    id 16
    label "pochowanie"
  ]
  node [
    id 17
    label "wydarzenie"
  ]
  node [
    id 18
    label "przechowanie"
  ]
  node [
    id 19
    label "post&#261;pienie"
  ]
  node [
    id 20
    label "post"
  ]
  node [
    id 21
    label "struktura"
  ]
  node [
    id 22
    label "spos&#243;b"
  ]
  node [
    id 23
    label "etolog"
  ]
  node [
    id 24
    label "zapia&#263;"
  ]
  node [
    id 25
    label "r&#243;&#380;yczka"
  ]
  node [
    id 26
    label "zapianie"
  ]
  node [
    id 27
    label "samiec"
  ]
  node [
    id 28
    label "kura"
  ]
  node [
    id 29
    label "pia&#263;"
  ]
  node [
    id 30
    label "pianie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
]
