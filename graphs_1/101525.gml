graph [
  maxDegree 38
  minDegree 1
  meanDegree 2
  density 0.012121212121212121
  graphCliqueNumber 6
  node [
    id 0
    label "bardzo"
    origin "text"
  ]
  node [
    id 1
    label "prosi&#263;"
    origin "text"
  ]
  node [
    id 2
    label "zabra&#263;"
    origin "text"
  ]
  node [
    id 3
    label "g&#322;os"
    origin "text"
  ]
  node [
    id 4
    label "pan"
    origin "text"
  ]
  node [
    id 5
    label "pose&#322;"
    origin "text"
  ]
  node [
    id 6
    label "wojciech"
    origin "text"
  ]
  node [
    id 7
    label "sa&#322;uga"
    origin "text"
  ]
  node [
    id 8
    label "platforma"
    origin "text"
  ]
  node [
    id 9
    label "obywatelski"
    origin "text"
  ]
  node [
    id 10
    label "w_chuj"
  ]
  node [
    id 11
    label "trwa&#263;"
  ]
  node [
    id 12
    label "zezwala&#263;"
  ]
  node [
    id 13
    label "ask"
  ]
  node [
    id 14
    label "invite"
  ]
  node [
    id 15
    label "zach&#281;ca&#263;"
  ]
  node [
    id 16
    label "preach"
  ]
  node [
    id 17
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 18
    label "pies"
  ]
  node [
    id 19
    label "ujmowa&#263;_si&#281;"
  ]
  node [
    id 20
    label "poleca&#263;"
  ]
  node [
    id 21
    label "zaprasza&#263;"
  ]
  node [
    id 22
    label "suffice"
  ]
  node [
    id 23
    label "dziwi&#263;_si&#281;"
  ]
  node [
    id 24
    label "consume"
  ]
  node [
    id 25
    label "zaj&#261;&#263;"
  ]
  node [
    id 26
    label "wzi&#261;&#263;"
  ]
  node [
    id 27
    label "przenie&#347;&#263;"
  ]
  node [
    id 28
    label "spowodowa&#263;"
  ]
  node [
    id 29
    label "z&#322;apa&#263;"
  ]
  node [
    id 30
    label "przesun&#261;&#263;"
  ]
  node [
    id 31
    label "deprive"
  ]
  node [
    id 32
    label "uda&#263;_si&#281;"
  ]
  node [
    id 33
    label "poci&#261;gn&#261;&#263;"
  ]
  node [
    id 34
    label "abstract"
  ]
  node [
    id 35
    label "withdraw"
  ]
  node [
    id 36
    label "doprowadzi&#263;"
  ]
  node [
    id 37
    label "opinion"
  ]
  node [
    id 38
    label "wypowied&#378;"
  ]
  node [
    id 39
    label "zmatowienie"
  ]
  node [
    id 40
    label "wpa&#347;&#263;"
  ]
  node [
    id 41
    label "grupa"
  ]
  node [
    id 42
    label "wokal"
  ]
  node [
    id 43
    label "note"
  ]
  node [
    id 44
    label "wydawa&#263;"
  ]
  node [
    id 45
    label "nakaz"
  ]
  node [
    id 46
    label "regestr"
  ]
  node [
    id 47
    label "&#347;piewak_operowy"
  ]
  node [
    id 48
    label "matowie&#263;"
  ]
  node [
    id 49
    label "wpada&#263;"
  ]
  node [
    id 50
    label "stanowisko"
  ]
  node [
    id 51
    label "zjawisko"
  ]
  node [
    id 52
    label "mutacja"
  ]
  node [
    id 53
    label "partia"
  ]
  node [
    id 54
    label "&#347;piewak"
  ]
  node [
    id 55
    label "emisja"
  ]
  node [
    id 56
    label "brzmienie"
  ]
  node [
    id 57
    label "zmatowie&#263;"
  ]
  node [
    id 58
    label "wydanie"
  ]
  node [
    id 59
    label "zesp&#243;&#322;"
  ]
  node [
    id 60
    label "wyda&#263;"
  ]
  node [
    id 61
    label "zdolno&#347;&#263;"
  ]
  node [
    id 62
    label "decyzja"
  ]
  node [
    id 63
    label "wpadni&#281;cie"
  ]
  node [
    id 64
    label "linia_melodyczna"
  ]
  node [
    id 65
    label "wpadanie"
  ]
  node [
    id 66
    label "onomatopeja"
  ]
  node [
    id 67
    label "sound"
  ]
  node [
    id 68
    label "matowienie"
  ]
  node [
    id 69
    label "ch&#243;rzysta"
  ]
  node [
    id 70
    label "d&#378;wi&#281;k"
  ]
  node [
    id 71
    label "foniatra"
  ]
  node [
    id 72
    label "&#347;piewaczka"
  ]
  node [
    id 73
    label "cz&#322;owiek"
  ]
  node [
    id 74
    label "profesor"
  ]
  node [
    id 75
    label "kszta&#322;ciciel"
  ]
  node [
    id 76
    label "jegomo&#347;&#263;"
  ]
  node [
    id 77
    label "zwrot"
  ]
  node [
    id 78
    label "pracodawca"
  ]
  node [
    id 79
    label "rz&#261;dzenie"
  ]
  node [
    id 80
    label "m&#261;&#380;"
  ]
  node [
    id 81
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 82
    label "ch&#322;opina"
  ]
  node [
    id 83
    label "bratek"
  ]
  node [
    id 84
    label "opiekun"
  ]
  node [
    id 85
    label "doros&#322;y"
  ]
  node [
    id 86
    label "preceptor"
  ]
  node [
    id 87
    label "Midas"
  ]
  node [
    id 88
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 89
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 90
    label "murza"
  ]
  node [
    id 91
    label "ojciec"
  ]
  node [
    id 92
    label "androlog"
  ]
  node [
    id 93
    label "pupil"
  ]
  node [
    id 94
    label "efendi"
  ]
  node [
    id 95
    label "nabab"
  ]
  node [
    id 96
    label "w&#322;odarz"
  ]
  node [
    id 97
    label "szkolnik"
  ]
  node [
    id 98
    label "pedagog"
  ]
  node [
    id 99
    label "popularyzator"
  ]
  node [
    id 100
    label "andropauza"
  ]
  node [
    id 101
    label "gra_w_karty"
  ]
  node [
    id 102
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 103
    label "Mieszko_I"
  ]
  node [
    id 104
    label "bogaty"
  ]
  node [
    id 105
    label "samiec"
  ]
  node [
    id 106
    label "przyw&#243;dca"
  ]
  node [
    id 107
    label "pa&#324;stwo"
  ]
  node [
    id 108
    label "belfer"
  ]
  node [
    id 109
    label "dyplomata"
  ]
  node [
    id 110
    label "wys&#322;annik"
  ]
  node [
    id 111
    label "przedstawiciel"
  ]
  node [
    id 112
    label "kurier_dyplomatyczny"
  ]
  node [
    id 113
    label "ablegat"
  ]
  node [
    id 114
    label "klubista"
  ]
  node [
    id 115
    label "Miko&#322;ajczyk"
  ]
  node [
    id 116
    label "Korwin"
  ]
  node [
    id 117
    label "parlamentarzysta"
  ]
  node [
    id 118
    label "dyscyplina_partyjna"
  ]
  node [
    id 119
    label "izba_ni&#380;sza"
  ]
  node [
    id 120
    label "poselstwo"
  ]
  node [
    id 121
    label "koturn"
  ]
  node [
    id 122
    label "samoch&#243;d_ci&#281;&#380;arowy"
  ]
  node [
    id 123
    label "skorupa_ziemska"
  ]
  node [
    id 124
    label "but"
  ]
  node [
    id 125
    label "p&#322;yta_kontynentalna"
  ]
  node [
    id 126
    label "podeszwa"
  ]
  node [
    id 127
    label "nadwozie"
  ]
  node [
    id 128
    label "sfera"
  ]
  node [
    id 129
    label "struktura"
  ]
  node [
    id 130
    label "p&#322;aszczyzna"
  ]
  node [
    id 131
    label "odpowiedzialny"
  ]
  node [
    id 132
    label "obywatelsko"
  ]
  node [
    id 133
    label "s&#322;uszny"
  ]
  node [
    id 134
    label "oddolny"
  ]
  node [
    id 135
    label "Wojciecha"
  ]
  node [
    id 136
    label "Sa&#322;uga"
  ]
  node [
    id 137
    label "Krzysztofa"
  ]
  node [
    id 138
    label "Gadowski"
  ]
  node [
    id 139
    label "Anna"
  ]
  node [
    id 140
    label "paluch"
  ]
  node [
    id 141
    label "Edwarda"
  ]
  node [
    id 142
    label "Wojtas"
  ]
  node [
    id 143
    label "polskie"
  ]
  node [
    id 144
    label "stronnictwo"
  ]
  node [
    id 145
    label "ludowy"
  ]
  node [
    id 146
    label "Artur"
  ]
  node [
    id 147
    label "ostrowski"
  ]
  node [
    id 148
    label "lewica"
  ]
  node [
    id 149
    label "i"
  ]
  node [
    id 150
    label "demokrata"
  ]
  node [
    id 151
    label "Piotrk&#243;w"
  ]
  node [
    id 152
    label "trybunalski"
  ]
  node [
    id 153
    label "fundusz"
  ]
  node [
    id 154
    label "sp&#243;jno&#347;&#263;"
  ]
  node [
    id 155
    label "Putra"
  ]
  node [
    id 156
    label "Tadeusz"
  ]
  node [
    id 157
    label "Arkita"
  ]
  node [
    id 158
    label "Arkit"
  ]
  node [
    id 159
    label "&#347;wi&#281;ty"
  ]
  node [
    id 160
    label "miko&#322;aj"
  ]
  node [
    id 161
    label "Mi&#281;dzygminnego"
  ]
  node [
    id 162
    label "zwi&#261;zek"
  ]
  node [
    id 163
    label "chrzanowy"
  ]
  node [
    id 164
    label "Lubi&#261;&#380;a"
  ]
  node [
    id 165
    label "Trzebinia"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 8
    target 122
  ]
  edge [
    source 8
    target 123
  ]
  edge [
    source 8
    target 124
  ]
  edge [
    source 8
    target 125
  ]
  edge [
    source 8
    target 126
  ]
  edge [
    source 8
    target 127
  ]
  edge [
    source 8
    target 128
  ]
  edge [
    source 8
    target 129
  ]
  edge [
    source 8
    target 130
  ]
  edge [
    source 9
    target 131
  ]
  edge [
    source 9
    target 132
  ]
  edge [
    source 9
    target 133
  ]
  edge [
    source 9
    target 134
  ]
  edge [
    source 135
    target 136
  ]
  edge [
    source 137
    target 138
  ]
  edge [
    source 137
    target 155
  ]
  edge [
    source 139
    target 140
  ]
  edge [
    source 141
    target 142
  ]
  edge [
    source 143
    target 144
  ]
  edge [
    source 143
    target 145
  ]
  edge [
    source 144
    target 145
  ]
  edge [
    source 146
    target 147
  ]
  edge [
    source 148
    target 149
  ]
  edge [
    source 148
    target 150
  ]
  edge [
    source 149
    target 150
  ]
  edge [
    source 149
    target 161
  ]
  edge [
    source 149
    target 162
  ]
  edge [
    source 149
    target 163
  ]
  edge [
    source 149
    target 164
  ]
  edge [
    source 149
    target 165
  ]
  edge [
    source 151
    target 152
  ]
  edge [
    source 153
    target 154
  ]
  edge [
    source 156
    target 157
  ]
  edge [
    source 156
    target 158
  ]
  edge [
    source 159
    target 160
  ]
  edge [
    source 161
    target 162
  ]
  edge [
    source 161
    target 163
  ]
  edge [
    source 161
    target 164
  ]
  edge [
    source 161
    target 165
  ]
  edge [
    source 162
    target 163
  ]
  edge [
    source 162
    target 164
  ]
  edge [
    source 162
    target 165
  ]
  edge [
    source 163
    target 164
  ]
  edge [
    source 163
    target 165
  ]
  edge [
    source 164
    target 165
  ]
]
