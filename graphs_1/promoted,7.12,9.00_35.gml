graph [
  maxDegree 15
  minDegree 1
  meanDegree 1.9333333333333333
  density 0.06666666666666667
  graphCliqueNumber 2
  node [
    id 0
    label "lewiatan"
    origin "text"
  ]
  node [
    id 1
    label "jeden"
    origin "text"
  ]
  node [
    id 2
    label "krok"
    origin "text"
  ]
  node [
    id 3
    label "biblizm"
  ]
  node [
    id 4
    label "potw&#243;r"
  ]
  node [
    id 5
    label "kieliszek"
  ]
  node [
    id 6
    label "shot"
  ]
  node [
    id 7
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 8
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 9
    label "jaki&#347;"
  ]
  node [
    id 10
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 11
    label "jednolicie"
  ]
  node [
    id 12
    label "w&#243;dka"
  ]
  node [
    id 13
    label "ten"
  ]
  node [
    id 14
    label "ujednolicenie"
  ]
  node [
    id 15
    label "jednakowy"
  ]
  node [
    id 16
    label "pace"
  ]
  node [
    id 17
    label "czyn"
  ]
  node [
    id 18
    label "passus"
  ]
  node [
    id 19
    label "measurement"
  ]
  node [
    id 20
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 21
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 22
    label "ruch"
  ]
  node [
    id 23
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 24
    label "action"
  ]
  node [
    id 25
    label "chodzi&#263;"
  ]
  node [
    id 26
    label "tu&#322;&#243;w"
  ]
  node [
    id 27
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 28
    label "skejt"
  ]
  node [
    id 29
    label "step"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
]
