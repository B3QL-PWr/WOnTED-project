graph [
  maxDegree 19
  minDegree 1
  meanDegree 1.9166666666666667
  density 0.08333333333333333
  graphCliqueNumber 2
  node [
    id 0
    label "koniec"
    origin "text"
  ]
  node [
    id 1
    label "napis"
    origin "text"
  ]
  node [
    id 2
    label "defenestracja"
  ]
  node [
    id 3
    label "szereg"
  ]
  node [
    id 4
    label "dzia&#322;anie"
  ]
  node [
    id 5
    label "miejsce"
  ]
  node [
    id 6
    label "ostatnie_podrygi"
  ]
  node [
    id 7
    label "kres"
  ]
  node [
    id 8
    label "agonia"
  ]
  node [
    id 9
    label "visitation"
  ]
  node [
    id 10
    label "szeol"
  ]
  node [
    id 11
    label "mogi&#322;a"
  ]
  node [
    id 12
    label "chwila"
  ]
  node [
    id 13
    label "wydarzenie"
  ]
  node [
    id 14
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 15
    label "pogrzebanie"
  ]
  node [
    id 16
    label "punkt"
  ]
  node [
    id 17
    label "&#380;a&#322;oba"
  ]
  node [
    id 18
    label "zabicie"
  ]
  node [
    id 19
    label "kres_&#380;ycia"
  ]
  node [
    id 20
    label "expressive_style"
  ]
  node [
    id 21
    label "autografia"
  ]
  node [
    id 22
    label "tekst"
  ]
  node [
    id 23
    label "informacja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
]
