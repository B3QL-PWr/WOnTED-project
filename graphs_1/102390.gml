graph [
  maxDegree 42
  minDegree 1
  meanDegree 2.125
  density 0.013364779874213837
  graphCliqueNumber 3
  node [
    id 0
    label "jeden"
    origin "text"
  ]
  node [
    id 1
    label "typowy"
    origin "text"
  ]
  node [
    id 2
    label "usterka"
    origin "text"
  ]
  node [
    id 3
    label "elektryczny"
    origin "text"
  ]
  node [
    id 4
    label "rustlera"
    origin "text"
  ]
  node [
    id 5
    label "by&#263;"
    origin "text"
  ]
  node [
    id 6
    label "wygi&#281;cie"
    origin "text"
  ]
  node [
    id 7
    label "sworze&#324;"
    origin "text"
  ]
  node [
    id 8
    label "zwrotnica"
    origin "text"
  ]
  node [
    id 9
    label "kingpinu"
    origin "text"
  ]
  node [
    id 10
    label "stan"
    origin "text"
  ]
  node [
    id 11
    label "pionowy"
    origin "text"
  ]
  node [
    id 12
    label "obr&#243;t"
    origin "text"
  ]
  node [
    id 13
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 14
    label "powodowa&#263;"
    origin "text"
  ]
  node [
    id 15
    label "problem"
    origin "text"
  ]
  node [
    id 16
    label "skr&#281;ca&#263;"
    origin "text"
  ]
  node [
    id 17
    label "lub"
    origin "text"
  ]
  node [
    id 18
    label "prostowa&#263;"
    origin "text"
  ]
  node [
    id 19
    label "ko&#322;a"
    origin "text"
  ]
  node [
    id 20
    label "skr&#281;t"
    origin "text"
  ]
  node [
    id 21
    label "kieliszek"
  ]
  node [
    id 22
    label "shot"
  ]
  node [
    id 23
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 24
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 25
    label "jaki&#347;"
  ]
  node [
    id 26
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 27
    label "jednolicie"
  ]
  node [
    id 28
    label "w&#243;dka"
  ]
  node [
    id 29
    label "ten"
  ]
  node [
    id 30
    label "ujednolicenie"
  ]
  node [
    id 31
    label "jednakowy"
  ]
  node [
    id 32
    label "typowo"
  ]
  node [
    id 33
    label "zwyczajny"
  ]
  node [
    id 34
    label "zwyk&#322;y"
  ]
  node [
    id 35
    label "cz&#281;sty"
  ]
  node [
    id 36
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 37
    label "pomy&#322;ka"
  ]
  node [
    id 38
    label "defekt"
  ]
  node [
    id 39
    label "elektrycznie"
  ]
  node [
    id 40
    label "si&#281;ga&#263;"
  ]
  node [
    id 41
    label "trwa&#263;"
  ]
  node [
    id 42
    label "obecno&#347;&#263;"
  ]
  node [
    id 43
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 44
    label "stand"
  ]
  node [
    id 45
    label "mie&#263;_miejsce"
  ]
  node [
    id 46
    label "uczestniczy&#263;"
  ]
  node [
    id 47
    label "chodzi&#263;"
  ]
  node [
    id 48
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 49
    label "equal"
  ]
  node [
    id 50
    label "curvature"
  ]
  node [
    id 51
    label "&#322;uk_kolankowy"
  ]
  node [
    id 52
    label "zdeformowanie"
  ]
  node [
    id 53
    label "nier&#243;wno&#347;&#263;"
  ]
  node [
    id 54
    label "bending"
  ]
  node [
    id 55
    label "ukszta&#322;towanie"
  ]
  node [
    id 56
    label "pochylenie"
  ]
  node [
    id 57
    label "powyginanie"
  ]
  node [
    id 58
    label "camber"
  ]
  node [
    id 59
    label "wygi&#281;cie_si&#281;"
  ]
  node [
    id 60
    label "ko&#322;ek"
  ]
  node [
    id 61
    label "cap"
  ]
  node [
    id 62
    label "blokada"
  ]
  node [
    id 63
    label "opornica"
  ]
  node [
    id 64
    label "switch"
  ]
  node [
    id 65
    label "urz&#261;dzenie"
  ]
  node [
    id 66
    label "Arizona"
  ]
  node [
    id 67
    label "Georgia"
  ]
  node [
    id 68
    label "warstwa"
  ]
  node [
    id 69
    label "jednostka_administracyjna"
  ]
  node [
    id 70
    label "Goa"
  ]
  node [
    id 71
    label "Hawaje"
  ]
  node [
    id 72
    label "Floryda"
  ]
  node [
    id 73
    label "Oklahoma"
  ]
  node [
    id 74
    label "punkt"
  ]
  node [
    id 75
    label "Alaska"
  ]
  node [
    id 76
    label "Alabama"
  ]
  node [
    id 77
    label "wci&#281;cie"
  ]
  node [
    id 78
    label "Oregon"
  ]
  node [
    id 79
    label "poziom"
  ]
  node [
    id 80
    label "Teksas"
  ]
  node [
    id 81
    label "Illinois"
  ]
  node [
    id 82
    label "Jukatan"
  ]
  node [
    id 83
    label "Waszyngton"
  ]
  node [
    id 84
    label "shape"
  ]
  node [
    id 85
    label "Nowy_Meksyk"
  ]
  node [
    id 86
    label "ilo&#347;&#263;"
  ]
  node [
    id 87
    label "state"
  ]
  node [
    id 88
    label "Nowy_York"
  ]
  node [
    id 89
    label "Arakan"
  ]
  node [
    id 90
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 91
    label "Kalifornia"
  ]
  node [
    id 92
    label "wektor"
  ]
  node [
    id 93
    label "Massachusetts"
  ]
  node [
    id 94
    label "miejsce"
  ]
  node [
    id 95
    label "Pensylwania"
  ]
  node [
    id 96
    label "Maryland"
  ]
  node [
    id 97
    label "Michigan"
  ]
  node [
    id 98
    label "Ohio"
  ]
  node [
    id 99
    label "Kansas"
  ]
  node [
    id 100
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 101
    label "Luizjana"
  ]
  node [
    id 102
    label "samopoczucie"
  ]
  node [
    id 103
    label "Wirginia"
  ]
  node [
    id 104
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 105
    label "hierarchiczny"
  ]
  node [
    id 106
    label "pionowo"
  ]
  node [
    id 107
    label "turn"
  ]
  node [
    id 108
    label "ruch"
  ]
  node [
    id 109
    label "sprzeda&#380;"
  ]
  node [
    id 110
    label "proces_ekonomiczny"
  ]
  node [
    id 111
    label "zmiana"
  ]
  node [
    id 112
    label "wp&#322;yw"
  ]
  node [
    id 113
    label "obieg"
  ]
  node [
    id 114
    label "round"
  ]
  node [
    id 115
    label "motywowa&#263;"
  ]
  node [
    id 116
    label "act"
  ]
  node [
    id 117
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 118
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 119
    label "trudno&#347;&#263;"
  ]
  node [
    id 120
    label "sprawa"
  ]
  node [
    id 121
    label "ambaras"
  ]
  node [
    id 122
    label "problemat"
  ]
  node [
    id 123
    label "pierepa&#322;ka"
  ]
  node [
    id 124
    label "obstruction"
  ]
  node [
    id 125
    label "problematyka"
  ]
  node [
    id 126
    label "jajko_Kolumba"
  ]
  node [
    id 127
    label "subiekcja"
  ]
  node [
    id 128
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 129
    label "os&#322;abia&#263;"
  ]
  node [
    id 130
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 131
    label "splata&#263;"
  ]
  node [
    id 132
    label "kierunek"
  ]
  node [
    id 133
    label "rozwija&#263;_si&#281;"
  ]
  node [
    id 134
    label "screw"
  ]
  node [
    id 135
    label "twist"
  ]
  node [
    id 136
    label "odchyla&#263;_si&#281;"
  ]
  node [
    id 137
    label "throw"
  ]
  node [
    id 138
    label "wrench"
  ]
  node [
    id 139
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 140
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 141
    label "scala&#263;"
  ]
  node [
    id 142
    label "korygowa&#263;"
  ]
  node [
    id 143
    label "amend"
  ]
  node [
    id 144
    label "rozk&#322;ada&#263;"
  ]
  node [
    id 145
    label "repair"
  ]
  node [
    id 146
    label "settle"
  ]
  node [
    id 147
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 148
    label "grupa"
  ]
  node [
    id 149
    label "huczek"
  ]
  node [
    id 150
    label "class"
  ]
  node [
    id 151
    label "podsterowno&#347;&#263;"
  ]
  node [
    id 152
    label "papieros"
  ]
  node [
    id 153
    label "odcinek"
  ]
  node [
    id 154
    label "whirl"
  ]
  node [
    id 155
    label "nadsterowno&#347;&#263;"
  ]
  node [
    id 156
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 157
    label "serpentyna"
  ]
  node [
    id 158
    label "trasa"
  ]
  node [
    id 159
    label "kszta&#322;t"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 10
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 6
    target 13
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 7
    target 10
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 15
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 12
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 11
  ]
  edge [
    source 9
    target 14
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 10
    target 69
  ]
  edge [
    source 10
    target 70
  ]
  edge [
    source 10
    target 71
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 12
    target 107
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 12
    target 109
  ]
  edge [
    source 12
    target 110
  ]
  edge [
    source 12
    target 111
  ]
  edge [
    source 12
    target 112
  ]
  edge [
    source 12
    target 113
  ]
  edge [
    source 12
    target 114
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 115
  ]
  edge [
    source 14
    target 45
  ]
  edge [
    source 14
    target 116
  ]
  edge [
    source 14
    target 117
  ]
  edge [
    source 14
    target 118
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 119
  ]
  edge [
    source 15
    target 120
  ]
  edge [
    source 15
    target 121
  ]
  edge [
    source 15
    target 122
  ]
  edge [
    source 15
    target 123
  ]
  edge [
    source 15
    target 124
  ]
  edge [
    source 15
    target 125
  ]
  edge [
    source 15
    target 126
  ]
  edge [
    source 15
    target 127
  ]
  edge [
    source 15
    target 128
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 129
  ]
  edge [
    source 16
    target 130
  ]
  edge [
    source 16
    target 131
  ]
  edge [
    source 16
    target 132
  ]
  edge [
    source 16
    target 133
  ]
  edge [
    source 16
    target 134
  ]
  edge [
    source 16
    target 135
  ]
  edge [
    source 16
    target 136
  ]
  edge [
    source 16
    target 137
  ]
  edge [
    source 16
    target 138
  ]
  edge [
    source 16
    target 139
  ]
  edge [
    source 16
    target 140
  ]
  edge [
    source 16
    target 141
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 142
  ]
  edge [
    source 18
    target 143
  ]
  edge [
    source 18
    target 144
  ]
  edge [
    source 18
    target 145
  ]
  edge [
    source 18
    target 140
  ]
  edge [
    source 18
    target 146
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 147
  ]
  edge [
    source 19
    target 148
  ]
  edge [
    source 19
    target 149
  ]
  edge [
    source 19
    target 150
  ]
  edge [
    source 20
    target 94
  ]
  edge [
    source 20
    target 151
  ]
  edge [
    source 20
    target 152
  ]
  edge [
    source 20
    target 108
  ]
  edge [
    source 20
    target 153
  ]
  edge [
    source 20
    target 154
  ]
  edge [
    source 20
    target 155
  ]
  edge [
    source 20
    target 156
  ]
  edge [
    source 20
    target 157
  ]
  edge [
    source 20
    target 158
  ]
  edge [
    source 20
    target 159
  ]
]
