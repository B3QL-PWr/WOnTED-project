graph [
  maxDegree 25
  minDegree 1
  meanDegree 2.0434782608695654
  density 0.022455805064500716
  graphCliqueNumber 3
  node [
    id 0
    label "tatra"
    origin "text"
  ]
  node [
    id 1
    label "trucks"
    origin "text"
  ]
  node [
    id 2
    label "dostarczy&#263;"
    origin "text"
  ]
  node [
    id 3
    label "samoch&#243;d"
    origin "text"
  ]
  node [
    id 4
    label "wojskowy"
    origin "text"
  ]
  node [
    id 5
    label "ci&#261;g"
    origin "text"
  ]
  node [
    id 6
    label "trzy"
    origin "text"
  ]
  node [
    id 7
    label "bliski"
    origin "text"
  ]
  node [
    id 8
    label "lato"
    origin "text"
  ]
  node [
    id 9
    label "wytworzy&#263;"
  ]
  node [
    id 10
    label "give"
  ]
  node [
    id 11
    label "picture"
  ]
  node [
    id 12
    label "spowodowa&#263;"
  ]
  node [
    id 13
    label "baga&#380;nik"
  ]
  node [
    id 14
    label "immobilizer"
  ]
  node [
    id 15
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 16
    label "ko&#322;o_samochodowe"
  ]
  node [
    id 17
    label "poduszka_powietrzna"
  ]
  node [
    id 18
    label "dachowanie"
  ]
  node [
    id 19
    label "dwu&#347;lad"
  ]
  node [
    id 20
    label "deska_rozdzielcza"
  ]
  node [
    id 21
    label "poci&#261;g_drogowy"
  ]
  node [
    id 22
    label "kierownica"
  ]
  node [
    id 23
    label "pojazd_drogowy"
  ]
  node [
    id 24
    label "uk&#322;ad_kierowniczy"
  ]
  node [
    id 25
    label "pompa_wodna"
  ]
  node [
    id 26
    label "silnik"
  ]
  node [
    id 27
    label "wycieraczka"
  ]
  node [
    id 28
    label "bak"
  ]
  node [
    id 29
    label "ABS"
  ]
  node [
    id 30
    label "most"
  ]
  node [
    id 31
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 32
    label "spryskiwacz"
  ]
  node [
    id 33
    label "t&#322;umik"
  ]
  node [
    id 34
    label "tempomat"
  ]
  node [
    id 35
    label "cz&#322;owiek"
  ]
  node [
    id 36
    label "rota"
  ]
  node [
    id 37
    label "zdemobilizowanie"
  ]
  node [
    id 38
    label "militarnie"
  ]
  node [
    id 39
    label "s&#322;u&#380;y&#263;_w_wojsku"
  ]
  node [
    id 40
    label "Gurkha"
  ]
  node [
    id 41
    label "demobilizowanie"
  ]
  node [
    id 42
    label "walcz&#261;cy"
  ]
  node [
    id 43
    label "harcap"
  ]
  node [
    id 44
    label "&#380;o&#322;dowy"
  ]
  node [
    id 45
    label "mundurowy"
  ]
  node [
    id 46
    label "s&#322;u&#380;enie_w_wojsku"
  ]
  node [
    id 47
    label "zdemobilizowa&#263;"
  ]
  node [
    id 48
    label "typowy"
  ]
  node [
    id 49
    label "antybalistyczny"
  ]
  node [
    id 50
    label "specjalny"
  ]
  node [
    id 51
    label "podleg&#322;y"
  ]
  node [
    id 52
    label "&#380;o&#322;nierstwo"
  ]
  node [
    id 53
    label "elew"
  ]
  node [
    id 54
    label "so&#322;dat"
  ]
  node [
    id 55
    label "wojsko"
  ]
  node [
    id 56
    label "wojskowo"
  ]
  node [
    id 57
    label "demobilizowa&#263;"
  ]
  node [
    id 58
    label "si&#322;a"
  ]
  node [
    id 59
    label "stan"
  ]
  node [
    id 60
    label "lina"
  ]
  node [
    id 61
    label "way"
  ]
  node [
    id 62
    label "cable"
  ]
  node [
    id 63
    label "przebieg"
  ]
  node [
    id 64
    label "zbi&#243;r"
  ]
  node [
    id 65
    label "ch&#243;d"
  ]
  node [
    id 66
    label "trasa"
  ]
  node [
    id 67
    label "rz&#261;d"
  ]
  node [
    id 68
    label "k&#322;us"
  ]
  node [
    id 69
    label "progression"
  ]
  node [
    id 70
    label "current"
  ]
  node [
    id 71
    label "pr&#261;d"
  ]
  node [
    id 72
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 73
    label "wydarzenie"
  ]
  node [
    id 74
    label "lot"
  ]
  node [
    id 75
    label "blisko"
  ]
  node [
    id 76
    label "przesz&#322;y"
  ]
  node [
    id 77
    label "gotowy"
  ]
  node [
    id 78
    label "dok&#322;adny"
  ]
  node [
    id 79
    label "kr&#243;tki"
  ]
  node [
    id 80
    label "znajomy"
  ]
  node [
    id 81
    label "przysz&#322;y"
  ]
  node [
    id 82
    label "oddalony"
  ]
  node [
    id 83
    label "silny"
  ]
  node [
    id 84
    label "zbli&#380;enie"
  ]
  node [
    id 85
    label "zwi&#261;zany"
  ]
  node [
    id 86
    label "nieodleg&#322;y"
  ]
  node [
    id 87
    label "ma&#322;y"
  ]
  node [
    id 88
    label "pora_roku"
  ]
  node [
    id 89
    label "TATRA"
  ]
  node [
    id 90
    label "T"
  ]
  node [
    id 91
    label "815"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 0
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 35
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 89
    target 90
  ]
  edge [
    source 89
    target 91
  ]
  edge [
    source 90
    target 91
  ]
]
