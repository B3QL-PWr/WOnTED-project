graph [
  maxDegree 47
  minDegree 1
  meanDegree 2.435233160621762
  density 0.004213206160245262
  graphCliqueNumber 8
  node [
    id 0
    label "wizualizowa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "otrzyma&#263;"
    origin "text"
  ]
  node [
    id 2
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 3
    label "nagroda"
    origin "text"
  ]
  node [
    id 4
    label "finansowy"
    origin "text"
  ]
  node [
    id 5
    label "dochodzi&#263;"
    origin "text"
  ]
  node [
    id 6
    label "wniosek"
    origin "text"
  ]
  node [
    id 7
    label "taki"
    origin "text"
  ]
  node [
    id 8
    label "wydarzenie"
    origin "text"
  ]
  node [
    id 9
    label "istotnie"
    origin "text"
  ]
  node [
    id 10
    label "zmieni&#263;by"
    origin "text"
  ]
  node [
    id 11
    label "nasze"
    origin "text"
  ]
  node [
    id 12
    label "&#380;ycie"
    origin "text"
  ]
  node [
    id 13
    label "lepsze"
    origin "text"
  ]
  node [
    id 14
    label "szcz&#281;&#347;liwy"
    origin "text"
  ]
  node [
    id 15
    label "koniec"
    origin "text"
  ]
  node [
    id 16
    label "oko"
    origin "text"
  ]
  node [
    id 17
    label "wyobra&#378;nia"
    origin "text"
  ]
  node [
    id 18
    label "odbiera&#263;"
    origin "text"
  ]
  node [
    id 19
    label "walizka"
    origin "text"
  ]
  node [
    id 20
    label "pe&#322;ny"
    origin "text"
  ]
  node [
    id 21
    label "got&#243;wka"
    origin "text"
  ]
  node [
    id 22
    label "by&#263;"
    origin "text"
  ]
  node [
    id 23
    label "pewien"
    origin "text"
  ]
  node [
    id 24
    label "rok"
    origin "text"
  ]
  node [
    id 25
    label "wygrana"
    origin "text"
  ]
  node [
    id 26
    label "ni&#380;"
    origin "text"
  ]
  node [
    id 27
    label "przed"
    origin "text"
  ]
  node [
    id 28
    label "losowanie"
    origin "text"
  ]
  node [
    id 29
    label "nic"
    origin "text"
  ]
  node [
    id 30
    label "bardzo"
    origin "text"
  ]
  node [
    id 31
    label "mylny"
    origin "text"
  ]
  node [
    id 32
    label "lato"
    origin "text"
  ]
  node [
    id 33
    label "tych"
    origin "text"
  ]
  node [
    id 34
    label "philip"
    origin "text"
  ]
  node [
    id 35
    label "brickman"
    origin "text"
  ]
  node [
    id 36
    label "wraz"
    origin "text"
  ]
  node [
    id 37
    label "zesp&#243;&#322;"
    origin "text"
  ]
  node [
    id 38
    label "northwestern"
    origin "text"
  ]
  node [
    id 39
    label "university"
    origin "text"
  ]
  node [
    id 40
    label "rozes&#322;a&#263;"
    origin "text"
  ]
  node [
    id 41
    label "kwestionariusz"
    origin "text"
  ]
  node [
    id 42
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 43
    label "miesi&#261;c"
    origin "text"
  ]
  node [
    id 44
    label "wczesno"
    origin "text"
  ]
  node [
    id 45
    label "wygra&#263;"
    origin "text"
  ]
  node [
    id 46
    label "spory"
    origin "text"
  ]
  node [
    id 47
    label "sum"
    origin "text"
  ]
  node [
    id 48
    label "loteria"
    origin "text"
  ]
  node [
    id 49
    label "tysi&#261;c"
    origin "text"
  ]
  node [
    id 50
    label "milion"
    origin "text"
  ]
  node [
    id 51
    label "oraz"
    origin "text"
  ]
  node [
    id 52
    label "dla"
    origin "text"
  ]
  node [
    id 53
    label "uleg&#322;y"
    origin "text"
  ]
  node [
    id 54
    label "wynik"
    origin "text"
  ]
  node [
    id 55
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 56
    label "dotyczy&#263;"
    origin "text"
  ]
  node [
    id 57
    label "poziom"
    origin "text"
  ]
  node [
    id 58
    label "szcz&#281;&#347;cie"
    origin "text"
  ]
  node [
    id 59
    label "chwila"
    origin "text"
  ]
  node [
    id 60
    label "tychy"
    origin "text"
  ]
  node [
    id 61
    label "wydarzenia"
    origin "text"
  ]
  node [
    id 62
    label "doprowadzi&#263;"
    origin "text"
  ]
  node [
    id 63
    label "sformu&#322;owanie"
    origin "text"
  ]
  node [
    id 64
    label "teza"
    origin "text"
  ]
  node [
    id 65
    label "niezale&#380;nie"
    origin "text"
  ]
  node [
    id 66
    label "losowy"
    origin "text"
  ]
  node [
    id 67
    label "podejmowa&#263;"
    origin "text"
  ]
  node [
    id 68
    label "wysi&#322;ek"
    origin "text"
  ]
  node [
    id 69
    label "tym"
    origin "text"
  ]
  node [
    id 70
    label "sam"
    origin "text"
  ]
  node [
    id 71
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 72
    label "przystosowywa&#263;"
    origin "text"
  ]
  node [
    id 73
    label "si&#281;"
    origin "text"
  ]
  node [
    id 74
    label "zar&#243;wno"
    origin "text"
  ]
  node [
    id 75
    label "ekscytowa&#263;"
    origin "text"
  ]
  node [
    id 76
    label "jak"
    origin "text"
  ]
  node [
    id 77
    label "traumatyczny"
    origin "text"
  ]
  node [
    id 78
    label "dwana&#347;cie"
    origin "text"
  ]
  node [
    id 79
    label "wyp&#322;at"
    origin "text"
  ]
  node [
    id 80
    label "lub"
    origin "text"
  ]
  node [
    id 81
    label "wypadek"
    origin "text"
  ]
  node [
    id 82
    label "przykuwa&#263;"
    origin "text"
  ]
  node [
    id 83
    label "reszta"
    origin "text"
  ]
  node [
    id 84
    label "w&#243;zek"
    origin "text"
  ]
  node [
    id 85
    label "cz&#322;onek"
    origin "text"
  ]
  node [
    id 86
    label "grupa"
    origin "text"
  ]
  node [
    id 87
    label "powr&#243;ci&#263;"
    origin "text"
  ]
  node [
    id 88
    label "sprzed"
    origin "text"
  ]
  node [
    id 89
    label "wytworzy&#263;"
  ]
  node [
    id 90
    label "return"
  ]
  node [
    id 91
    label "give_birth"
  ]
  node [
    id 92
    label "dosta&#263;"
  ]
  node [
    id 93
    label "doros&#322;y"
  ]
  node [
    id 94
    label "wiele"
  ]
  node [
    id 95
    label "dorodny"
  ]
  node [
    id 96
    label "znaczny"
  ]
  node [
    id 97
    label "du&#380;o"
  ]
  node [
    id 98
    label "prawdziwy"
  ]
  node [
    id 99
    label "niema&#322;o"
  ]
  node [
    id 100
    label "wa&#380;ny"
  ]
  node [
    id 101
    label "rozwini&#281;ty"
  ]
  node [
    id 102
    label "konsekwencja"
  ]
  node [
    id 103
    label "oskar"
  ]
  node [
    id 104
    label "wyr&#243;&#380;nienie"
  ]
  node [
    id 105
    label "mi&#281;dzybankowy"
  ]
  node [
    id 106
    label "finansowo"
  ]
  node [
    id 107
    label "fizyczny"
  ]
  node [
    id 108
    label "pozamaterialny"
  ]
  node [
    id 109
    label "materjalny"
  ]
  node [
    id 110
    label "uzyskiwa&#263;"
  ]
  node [
    id 111
    label "doczeka&#263;"
  ]
  node [
    id 112
    label "submit"
  ]
  node [
    id 113
    label "dop&#322;ywa&#263;"
  ]
  node [
    id 114
    label "dosi&#281;ga&#263;"
  ]
  node [
    id 115
    label "dociera&#263;"
  ]
  node [
    id 116
    label "supervene"
  ]
  node [
    id 117
    label "doznawa&#263;"
  ]
  node [
    id 118
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 119
    label "osi&#261;ga&#263;"
  ]
  node [
    id 120
    label "orgazm"
  ]
  node [
    id 121
    label "przy&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 122
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 123
    label "ripen"
  ]
  node [
    id 124
    label "zachodzi&#263;"
  ]
  node [
    id 125
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 126
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 127
    label "zdarza&#263;_si&#281;"
  ]
  node [
    id 128
    label "dokoptowywa&#263;"
  ]
  node [
    id 129
    label "robi&#263;"
  ]
  node [
    id 130
    label "reach"
  ]
  node [
    id 131
    label "claim"
  ]
  node [
    id 132
    label "dolatywa&#263;"
  ]
  node [
    id 133
    label "przesy&#322;ka"
  ]
  node [
    id 134
    label "postrzega&#263;"
  ]
  node [
    id 135
    label "powodowa&#263;"
  ]
  node [
    id 136
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 137
    label "twierdzenie"
  ]
  node [
    id 138
    label "my&#347;l"
  ]
  node [
    id 139
    label "wnioskowanie"
  ]
  node [
    id 140
    label "propozycja"
  ]
  node [
    id 141
    label "motion"
  ]
  node [
    id 142
    label "pismo"
  ]
  node [
    id 143
    label "prayer"
  ]
  node [
    id 144
    label "okre&#347;lony"
  ]
  node [
    id 145
    label "jaki&#347;"
  ]
  node [
    id 146
    label "czynno&#347;&#263;"
  ]
  node [
    id 147
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 148
    label "motyw"
  ]
  node [
    id 149
    label "fabu&#322;a"
  ]
  node [
    id 150
    label "przebiec"
  ]
  node [
    id 151
    label "przebiegni&#281;cie"
  ]
  node [
    id 152
    label "charakter"
  ]
  node [
    id 153
    label "realnie"
  ]
  node [
    id 154
    label "importantly"
  ]
  node [
    id 155
    label "istotny"
  ]
  node [
    id 156
    label "energy"
  ]
  node [
    id 157
    label "czas"
  ]
  node [
    id 158
    label "bycie"
  ]
  node [
    id 159
    label "zegar_biologiczny"
  ]
  node [
    id 160
    label "okres_noworodkowy"
  ]
  node [
    id 161
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 162
    label "entity"
  ]
  node [
    id 163
    label "prze&#380;ywanie"
  ]
  node [
    id 164
    label "prze&#380;ycie"
  ]
  node [
    id 165
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 166
    label "wiek_matuzalemowy"
  ]
  node [
    id 167
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 168
    label "dzieci&#324;stwo"
  ]
  node [
    id 169
    label "power"
  ]
  node [
    id 170
    label "szwung"
  ]
  node [
    id 171
    label "menopauza"
  ]
  node [
    id 172
    label "umarcie"
  ]
  node [
    id 173
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 174
    label "life"
  ]
  node [
    id 175
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 176
    label "&#380;ywy"
  ]
  node [
    id 177
    label "rozw&#243;j"
  ]
  node [
    id 178
    label "po&#322;&#243;g"
  ]
  node [
    id 179
    label "byt"
  ]
  node [
    id 180
    label "przebywanie"
  ]
  node [
    id 181
    label "subsistence"
  ]
  node [
    id 182
    label "koleje_losu"
  ]
  node [
    id 183
    label "raj_utracony"
  ]
  node [
    id 184
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 185
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 186
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 187
    label "andropauza"
  ]
  node [
    id 188
    label "warunki"
  ]
  node [
    id 189
    label "do&#380;ywanie"
  ]
  node [
    id 190
    label "niemowl&#281;ctwo"
  ]
  node [
    id 191
    label "umieranie"
  ]
  node [
    id 192
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 193
    label "staro&#347;&#263;"
  ]
  node [
    id 194
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 195
    label "&#347;mier&#263;"
  ]
  node [
    id 196
    label "pomy&#347;lny"
  ]
  node [
    id 197
    label "zadowolony"
  ]
  node [
    id 198
    label "udany"
  ]
  node [
    id 199
    label "szcz&#281;&#347;liwie"
  ]
  node [
    id 200
    label "pogodny"
  ]
  node [
    id 201
    label "dobry"
  ]
  node [
    id 202
    label "defenestracja"
  ]
  node [
    id 203
    label "szereg"
  ]
  node [
    id 204
    label "dzia&#322;anie"
  ]
  node [
    id 205
    label "miejsce"
  ]
  node [
    id 206
    label "ostatnie_podrygi"
  ]
  node [
    id 207
    label "kres"
  ]
  node [
    id 208
    label "agonia"
  ]
  node [
    id 209
    label "visitation"
  ]
  node [
    id 210
    label "szeol"
  ]
  node [
    id 211
    label "mogi&#322;a"
  ]
  node [
    id 212
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 213
    label "pogrzebanie"
  ]
  node [
    id 214
    label "punkt"
  ]
  node [
    id 215
    label "&#380;a&#322;oba"
  ]
  node [
    id 216
    label "zabicie"
  ]
  node [
    id 217
    label "kres_&#380;ycia"
  ]
  node [
    id 218
    label "wypowied&#378;"
  ]
  node [
    id 219
    label "siniec"
  ]
  node [
    id 220
    label "uwaga"
  ]
  node [
    id 221
    label "rzecz"
  ]
  node [
    id 222
    label "&#380;y&#322;a_&#347;rodkowa_siatk&#243;wki"
  ]
  node [
    id 223
    label "powieka"
  ]
  node [
    id 224
    label "oczy"
  ]
  node [
    id 225
    label "&#347;lepko"
  ]
  node [
    id 226
    label "ga&#322;ka_oczna"
  ]
  node [
    id 227
    label "ser_&#380;&#243;&#322;ty"
  ]
  node [
    id 228
    label "&#347;lepie"
  ]
  node [
    id 229
    label "twarz"
  ]
  node [
    id 230
    label "worek_spoj&#243;wkowy"
  ]
  node [
    id 231
    label "organ"
  ]
  node [
    id 232
    label "nerw_wzrokowy"
  ]
  node [
    id 233
    label "wzrok"
  ]
  node [
    id 234
    label "spoj&#243;wka"
  ]
  node [
    id 235
    label "&#378;renica"
  ]
  node [
    id 236
    label "narz&#261;d_&#322;zowy"
  ]
  node [
    id 237
    label "nab&#322;onek_barwnikowy"
  ]
  node [
    id 238
    label "kaprawie&#263;"
  ]
  node [
    id 239
    label "kaprawienie"
  ]
  node [
    id 240
    label "spojrzenie"
  ]
  node [
    id 241
    label "net"
  ]
  node [
    id 242
    label "r&#261;bek_rog&#243;wki"
  ]
  node [
    id 243
    label "coloboma"
  ]
  node [
    id 244
    label "ros&#243;&#322;"
  ]
  node [
    id 245
    label "kraina"
  ]
  node [
    id 246
    label "zdolno&#347;&#263;"
  ]
  node [
    id 247
    label "imagineskopia"
  ]
  node [
    id 248
    label "umys&#322;"
  ]
  node [
    id 249
    label "fondness"
  ]
  node [
    id 250
    label "radio"
  ]
  node [
    id 251
    label "liszy&#263;"
  ]
  node [
    id 252
    label "przyjmowa&#263;"
  ]
  node [
    id 253
    label "deprive"
  ]
  node [
    id 254
    label "zabiera&#263;"
  ]
  node [
    id 255
    label "odzyskiwa&#263;"
  ]
  node [
    id 256
    label "accept"
  ]
  node [
    id 257
    label "antena"
  ]
  node [
    id 258
    label "telewizor"
  ]
  node [
    id 259
    label "pozbawia&#263;"
  ]
  node [
    id 260
    label "fall"
  ]
  node [
    id 261
    label "bra&#263;"
  ]
  node [
    id 262
    label "konfiskowa&#263;"
  ]
  node [
    id 263
    label "zlecenie"
  ]
  node [
    id 264
    label "bag"
  ]
  node [
    id 265
    label "torba"
  ]
  node [
    id 266
    label "sakwoja&#380;"
  ]
  node [
    id 267
    label "nieograniczony"
  ]
  node [
    id 268
    label "nape&#322;nienie_si&#281;"
  ]
  node [
    id 269
    label "kompletny"
  ]
  node [
    id 270
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 271
    label "r&#243;wny"
  ]
  node [
    id 272
    label "wype&#322;nianie_si&#281;"
  ]
  node [
    id 273
    label "bezwzgl&#281;dny"
  ]
  node [
    id 274
    label "zupe&#322;ny"
  ]
  node [
    id 275
    label "ca&#322;y"
  ]
  node [
    id 276
    label "satysfakcja"
  ]
  node [
    id 277
    label "wszechogarniaj&#261;cy"
  ]
  node [
    id 278
    label "pe&#322;no"
  ]
  node [
    id 279
    label "wype&#322;nienie"
  ]
  node [
    id 280
    label "otwarty"
  ]
  node [
    id 281
    label "pieni&#261;dze"
  ]
  node [
    id 282
    label "brz&#281;cz&#261;ca_moneta"
  ]
  node [
    id 283
    label "money"
  ]
  node [
    id 284
    label "si&#281;ga&#263;"
  ]
  node [
    id 285
    label "trwa&#263;"
  ]
  node [
    id 286
    label "obecno&#347;&#263;"
  ]
  node [
    id 287
    label "stan"
  ]
  node [
    id 288
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 289
    label "stand"
  ]
  node [
    id 290
    label "mie&#263;_miejsce"
  ]
  node [
    id 291
    label "uczestniczy&#263;"
  ]
  node [
    id 292
    label "chodzi&#263;"
  ]
  node [
    id 293
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 294
    label "equal"
  ]
  node [
    id 295
    label "upewnienie_si&#281;"
  ]
  node [
    id 296
    label "wierzenie"
  ]
  node [
    id 297
    label "mo&#380;liwy"
  ]
  node [
    id 298
    label "ufanie"
  ]
  node [
    id 299
    label "spokojny"
  ]
  node [
    id 300
    label "upewnianie_si&#281;"
  ]
  node [
    id 301
    label "stulecie"
  ]
  node [
    id 302
    label "kalendarz"
  ]
  node [
    id 303
    label "pora_roku"
  ]
  node [
    id 304
    label "cykl_astronomiczny"
  ]
  node [
    id 305
    label "p&#243;&#322;rocze"
  ]
  node [
    id 306
    label "kwarta&#322;"
  ]
  node [
    id 307
    label "kurs"
  ]
  node [
    id 308
    label "jubileusz"
  ]
  node [
    id 309
    label "lata"
  ]
  node [
    id 310
    label "martwy_sezon"
  ]
  node [
    id 311
    label "puchar"
  ]
  node [
    id 312
    label "korzy&#347;&#263;"
  ]
  node [
    id 313
    label "sukces"
  ]
  node [
    id 314
    label "przedmiot"
  ]
  node [
    id 315
    label "conquest"
  ]
  node [
    id 316
    label "faza"
  ]
  node [
    id 317
    label "depression"
  ]
  node [
    id 318
    label "zjawisko"
  ]
  node [
    id 319
    label "nizina"
  ]
  node [
    id 320
    label "wygrywanie"
  ]
  node [
    id 321
    label "rozlosowanie"
  ]
  node [
    id 322
    label "draw"
  ]
  node [
    id 323
    label "wybieranie"
  ]
  node [
    id 324
    label "miernota"
  ]
  node [
    id 325
    label "g&#243;wno"
  ]
  node [
    id 326
    label "love"
  ]
  node [
    id 327
    label "ilo&#347;&#263;"
  ]
  node [
    id 328
    label "brak"
  ]
  node [
    id 329
    label "ciura"
  ]
  node [
    id 330
    label "w_chuj"
  ]
  node [
    id 331
    label "b&#322;&#281;dny"
  ]
  node [
    id 332
    label "mylnie"
  ]
  node [
    id 333
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 334
    label "whole"
  ]
  node [
    id 335
    label "odm&#322;adza&#263;"
  ]
  node [
    id 336
    label "zabudowania"
  ]
  node [
    id 337
    label "odm&#322;odzenie"
  ]
  node [
    id 338
    label "zespolik"
  ]
  node [
    id 339
    label "skupienie"
  ]
  node [
    id 340
    label "schorzenie"
  ]
  node [
    id 341
    label "Depeche_Mode"
  ]
  node [
    id 342
    label "Mazowsze"
  ]
  node [
    id 343
    label "ro&#347;lina"
  ]
  node [
    id 344
    label "zbi&#243;r"
  ]
  node [
    id 345
    label "The_Beatles"
  ]
  node [
    id 346
    label "group"
  ]
  node [
    id 347
    label "&#346;wietliki"
  ]
  node [
    id 348
    label "odm&#322;adzanie"
  ]
  node [
    id 349
    label "batch"
  ]
  node [
    id 350
    label "roz&#322;o&#380;y&#263;"
  ]
  node [
    id 351
    label "ship"
  ]
  node [
    id 352
    label "przes&#322;a&#263;"
  ]
  node [
    id 353
    label "&#322;&#243;&#380;ko"
  ]
  node [
    id 354
    label "kafeteria"
  ]
  node [
    id 355
    label "formularz"
  ]
  node [
    id 356
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 357
    label "miech"
  ]
  node [
    id 358
    label "kalendy"
  ]
  node [
    id 359
    label "tydzie&#324;"
  ]
  node [
    id 360
    label "wcze&#347;nie"
  ]
  node [
    id 361
    label "score"
  ]
  node [
    id 362
    label "okaza&#263;_si&#281;"
  ]
  node [
    id 363
    label "zwojowa&#263;"
  ]
  node [
    id 364
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 365
    label "leave"
  ]
  node [
    id 366
    label "znie&#347;&#263;"
  ]
  node [
    id 367
    label "zagwarantowa&#263;"
  ]
  node [
    id 368
    label "instrument_muzyczny"
  ]
  node [
    id 369
    label "zagra&#263;"
  ]
  node [
    id 370
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 371
    label "zrobi&#263;"
  ]
  node [
    id 372
    label "net_income"
  ]
  node [
    id 373
    label "sporo"
  ]
  node [
    id 374
    label "intensywny"
  ]
  node [
    id 375
    label "sumowate"
  ]
  node [
    id 376
    label "Uzbekistan"
  ]
  node [
    id 377
    label "catfish"
  ]
  node [
    id 378
    label "ryba"
  ]
  node [
    id 379
    label "jednostka_monetarna"
  ]
  node [
    id 380
    label "gra_losowa"
  ]
  node [
    id 381
    label "tauzen"
  ]
  node [
    id 382
    label "musik"
  ]
  node [
    id 383
    label "molarity"
  ]
  node [
    id 384
    label "licytacja"
  ]
  node [
    id 385
    label "patyk"
  ]
  node [
    id 386
    label "liczba"
  ]
  node [
    id 387
    label "gra_w_karty"
  ]
  node [
    id 388
    label "kwota"
  ]
  node [
    id 389
    label "miljon"
  ]
  node [
    id 390
    label "ba&#324;ka"
  ]
  node [
    id 391
    label "zmi&#281;kni&#281;cie"
  ]
  node [
    id 392
    label "mi&#281;kni&#281;cie"
  ]
  node [
    id 393
    label "zale&#380;ny"
  ]
  node [
    id 394
    label "ulegle"
  ]
  node [
    id 395
    label "typ"
  ]
  node [
    id 396
    label "przyczyna"
  ]
  node [
    id 397
    label "zaokr&#261;gli&#263;"
  ]
  node [
    id 398
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 399
    label "zaokr&#261;glenie"
  ]
  node [
    id 400
    label "event"
  ]
  node [
    id 401
    label "rezultat"
  ]
  node [
    id 402
    label "proceed"
  ]
  node [
    id 403
    label "catch"
  ]
  node [
    id 404
    label "pozosta&#263;"
  ]
  node [
    id 405
    label "osta&#263;_si&#281;"
  ]
  node [
    id 406
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 407
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 408
    label "change"
  ]
  node [
    id 409
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 410
    label "bargain"
  ]
  node [
    id 411
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 412
    label "tycze&#263;"
  ]
  node [
    id 413
    label "wysoko&#347;&#263;"
  ]
  node [
    id 414
    label "szczebel"
  ]
  node [
    id 415
    label "po&#322;o&#380;enie"
  ]
  node [
    id 416
    label "kierunek"
  ]
  node [
    id 417
    label "wyk&#322;adnik"
  ]
  node [
    id 418
    label "budynek"
  ]
  node [
    id 419
    label "punkt_widzenia"
  ]
  node [
    id 420
    label "jako&#347;&#263;"
  ]
  node [
    id 421
    label "ranga"
  ]
  node [
    id 422
    label "p&#322;aszczyzna"
  ]
  node [
    id 423
    label "wra&#380;enie"
  ]
  node [
    id 424
    label "przeznaczenie"
  ]
  node [
    id 425
    label "dobrodziejstwo"
  ]
  node [
    id 426
    label "dobro"
  ]
  node [
    id 427
    label "przypadek"
  ]
  node [
    id 428
    label "time"
  ]
  node [
    id 429
    label "poprowadzi&#263;"
  ]
  node [
    id 430
    label "spowodowa&#263;"
  ]
  node [
    id 431
    label "pos&#322;a&#263;"
  ]
  node [
    id 432
    label "have_a_bun_in_the_oven"
  ]
  node [
    id 433
    label "wykona&#263;"
  ]
  node [
    id 434
    label "wzbudzi&#263;"
  ]
  node [
    id 435
    label "wprowadzi&#263;"
  ]
  node [
    id 436
    label "set"
  ]
  node [
    id 437
    label "take"
  ]
  node [
    id 438
    label "carry"
  ]
  node [
    id 439
    label "poinformowanie"
  ]
  node [
    id 440
    label "zapisanie"
  ]
  node [
    id 441
    label "wording"
  ]
  node [
    id 442
    label "statement"
  ]
  node [
    id 443
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 444
    label "rzucenie"
  ]
  node [
    id 445
    label "s&#261;d"
  ]
  node [
    id 446
    label "znaczenie"
  ]
  node [
    id 447
    label "argument"
  ]
  node [
    id 448
    label "idea"
  ]
  node [
    id 449
    label "dialektyka"
  ]
  node [
    id 450
    label "poj&#281;cie"
  ]
  node [
    id 451
    label "niezale&#380;ny"
  ]
  node [
    id 452
    label "losowo"
  ]
  node [
    id 453
    label "przypadkowy"
  ]
  node [
    id 454
    label "zmienia&#263;"
  ]
  node [
    id 455
    label "reagowa&#263;"
  ]
  node [
    id 456
    label "rise"
  ]
  node [
    id 457
    label "admit"
  ]
  node [
    id 458
    label "drive"
  ]
  node [
    id 459
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 460
    label "podnosi&#263;"
  ]
  node [
    id 461
    label "zach&#243;d"
  ]
  node [
    id 462
    label "trudzenie"
  ]
  node [
    id 463
    label "przytoczenie_si&#281;"
  ]
  node [
    id 464
    label "trudzi&#263;"
  ]
  node [
    id 465
    label "cecha"
  ]
  node [
    id 466
    label "wleczenie_si&#281;"
  ]
  node [
    id 467
    label "przytaczanie_si&#281;"
  ]
  node [
    id 468
    label "wlec_si&#281;"
  ]
  node [
    id 469
    label "sklep"
  ]
  node [
    id 470
    label "asymilowa&#263;"
  ]
  node [
    id 471
    label "wapniak"
  ]
  node [
    id 472
    label "dwun&#243;g"
  ]
  node [
    id 473
    label "polifag"
  ]
  node [
    id 474
    label "wz&#243;r"
  ]
  node [
    id 475
    label "profanum"
  ]
  node [
    id 476
    label "hominid"
  ]
  node [
    id 477
    label "homo_sapiens"
  ]
  node [
    id 478
    label "nasada"
  ]
  node [
    id 479
    label "podw&#322;adny"
  ]
  node [
    id 480
    label "ludzko&#347;&#263;"
  ]
  node [
    id 481
    label "os&#322;abianie"
  ]
  node [
    id 482
    label "mikrokosmos"
  ]
  node [
    id 483
    label "portrecista"
  ]
  node [
    id 484
    label "duch"
  ]
  node [
    id 485
    label "g&#322;owa"
  ]
  node [
    id 486
    label "oddzia&#322;ywanie"
  ]
  node [
    id 487
    label "asymilowanie"
  ]
  node [
    id 488
    label "osoba"
  ]
  node [
    id 489
    label "os&#322;abia&#263;"
  ]
  node [
    id 490
    label "figura"
  ]
  node [
    id 491
    label "Adam"
  ]
  node [
    id 492
    label "senior"
  ]
  node [
    id 493
    label "antropochoria"
  ]
  node [
    id 494
    label "posta&#263;"
  ]
  node [
    id 495
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 496
    label "porusza&#263;"
  ]
  node [
    id 497
    label "revolutionize"
  ]
  node [
    id 498
    label "byd&#322;o"
  ]
  node [
    id 499
    label "zobo"
  ]
  node [
    id 500
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 501
    label "yakalo"
  ]
  node [
    id 502
    label "dzo"
  ]
  node [
    id 503
    label "bolesny"
  ]
  node [
    id 504
    label "dramatyczny"
  ]
  node [
    id 505
    label "traumatycznie"
  ]
  node [
    id 506
    label "pourazowy"
  ]
  node [
    id 507
    label "ubezpieczenie_wypadkowe"
  ]
  node [
    id 508
    label "happening"
  ]
  node [
    id 509
    label "bind"
  ]
  node [
    id 510
    label "przymocowywa&#263;"
  ]
  node [
    id 511
    label "obezw&#322;adnia&#263;"
  ]
  node [
    id 512
    label "unieruchamia&#263;"
  ]
  node [
    id 513
    label "uwi&#261;zywa&#263;"
  ]
  node [
    id 514
    label "remainder"
  ]
  node [
    id 515
    label "wydanie"
  ]
  node [
    id 516
    label "wyda&#263;"
  ]
  node [
    id 517
    label "wydawa&#263;"
  ]
  node [
    id 518
    label "pozosta&#322;y"
  ]
  node [
    id 519
    label "pojazd_niemechaniczny"
  ]
  node [
    id 520
    label "cia&#322;o"
  ]
  node [
    id 521
    label "organizacja"
  ]
  node [
    id 522
    label "przedstawiciel"
  ]
  node [
    id 523
    label "shaft"
  ]
  node [
    id 524
    label "podmiot"
  ]
  node [
    id 525
    label "fiut"
  ]
  node [
    id 526
    label "przyrodzenie"
  ]
  node [
    id 527
    label "wchodzenie"
  ]
  node [
    id 528
    label "ptaszek"
  ]
  node [
    id 529
    label "wej&#347;cie"
  ]
  node [
    id 530
    label "narz&#261;d_kopulacyjny"
  ]
  node [
    id 531
    label "element_anatomiczny"
  ]
  node [
    id 532
    label "cz&#261;steczka"
  ]
  node [
    id 533
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 534
    label "egzemplarz"
  ]
  node [
    id 535
    label "formacja_geologiczna"
  ]
  node [
    id 536
    label "harcerze_starsi"
  ]
  node [
    id 537
    label "liga"
  ]
  node [
    id 538
    label "Terranie"
  ]
  node [
    id 539
    label "pakiet_klimatyczny"
  ]
  node [
    id 540
    label "oddzia&#322;"
  ]
  node [
    id 541
    label "stage_set"
  ]
  node [
    id 542
    label "Entuzjastki"
  ]
  node [
    id 543
    label "type"
  ]
  node [
    id 544
    label "category"
  ]
  node [
    id 545
    label "specgrupa"
  ]
  node [
    id 546
    label "gromada"
  ]
  node [
    id 547
    label "Eurogrupa"
  ]
  node [
    id 548
    label "jednostka_systematyczna"
  ]
  node [
    id 549
    label "kompozycja"
  ]
  node [
    id 550
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 551
    label "przyj&#347;&#263;"
  ]
  node [
    id 552
    label "revive"
  ]
  node [
    id 553
    label "podj&#261;&#263;"
  ]
  node [
    id 554
    label "wr&#243;ci&#263;"
  ]
  node [
    id 555
    label "przyby&#263;"
  ]
  node [
    id 556
    label "recur"
  ]
  node [
    id 557
    label "Philip"
  ]
  node [
    id 558
    label "Brickman"
  ]
  node [
    id 559
    label "Northwestern"
  ]
  node [
    id 560
    label "University"
  ]
  node [
    id 561
    label "Janoff"
  ]
  node [
    id 562
    label "Bulman"
  ]
  node [
    id 563
    label "p&#243;&#378;no"
  ]
  node [
    id 564
    label "Coates"
  ]
  node [
    id 565
    label "dzie&#324;"
  ]
  node [
    id 566
    label "Lottery"
  ]
  node [
    id 567
    label "winners"
  ]
  node [
    id 568
    label "Anda"
  ]
  node [
    id 569
    label "accident"
  ]
  node [
    id 570
    label "victims"
  ]
  node [
    id 571
    label "IS"
  ]
  node [
    id 572
    label "happiness"
  ]
  node [
    id 573
    label "relative"
  ]
  node [
    id 574
    label "Journal"
  ]
  node [
    id 575
    label "of"
  ]
  node [
    id 576
    label "Personality"
  ]
  node [
    id 577
    label "Social"
  ]
  node [
    id 578
    label "Psychology"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 144
  ]
  edge [
    source 7
    target 145
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 146
  ]
  edge [
    source 8
    target 147
  ]
  edge [
    source 8
    target 148
  ]
  edge [
    source 8
    target 149
  ]
  edge [
    source 8
    target 150
  ]
  edge [
    source 8
    target 151
  ]
  edge [
    source 8
    target 152
  ]
  edge [
    source 8
    target 15
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 153
  ]
  edge [
    source 9
    target 154
  ]
  edge [
    source 9
    target 155
  ]
  edge [
    source 9
    target 100
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 68
  ]
  edge [
    source 11
    target 58
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 15
  ]
  edge [
    source 12
    target 16
  ]
  edge [
    source 12
    target 83
  ]
  edge [
    source 12
    target 84
  ]
  edge [
    source 12
    target 156
  ]
  edge [
    source 12
    target 157
  ]
  edge [
    source 12
    target 158
  ]
  edge [
    source 12
    target 159
  ]
  edge [
    source 12
    target 160
  ]
  edge [
    source 12
    target 161
  ]
  edge [
    source 12
    target 162
  ]
  edge [
    source 12
    target 163
  ]
  edge [
    source 12
    target 164
  ]
  edge [
    source 12
    target 165
  ]
  edge [
    source 12
    target 166
  ]
  edge [
    source 12
    target 167
  ]
  edge [
    source 12
    target 168
  ]
  edge [
    source 12
    target 169
  ]
  edge [
    source 12
    target 170
  ]
  edge [
    source 12
    target 171
  ]
  edge [
    source 12
    target 172
  ]
  edge [
    source 12
    target 173
  ]
  edge [
    source 12
    target 174
  ]
  edge [
    source 12
    target 175
  ]
  edge [
    source 12
    target 176
  ]
  edge [
    source 12
    target 177
  ]
  edge [
    source 12
    target 178
  ]
  edge [
    source 12
    target 179
  ]
  edge [
    source 12
    target 180
  ]
  edge [
    source 12
    target 181
  ]
  edge [
    source 12
    target 182
  ]
  edge [
    source 12
    target 183
  ]
  edge [
    source 12
    target 184
  ]
  edge [
    source 12
    target 185
  ]
  edge [
    source 12
    target 186
  ]
  edge [
    source 12
    target 187
  ]
  edge [
    source 12
    target 188
  ]
  edge [
    source 12
    target 189
  ]
  edge [
    source 12
    target 190
  ]
  edge [
    source 12
    target 191
  ]
  edge [
    source 12
    target 192
  ]
  edge [
    source 12
    target 193
  ]
  edge [
    source 12
    target 194
  ]
  edge [
    source 12
    target 195
  ]
  edge [
    source 12
    target 35
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 82
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 22
  ]
  edge [
    source 14
    target 26
  ]
  edge [
    source 14
    target 196
  ]
  edge [
    source 14
    target 197
  ]
  edge [
    source 14
    target 198
  ]
  edge [
    source 14
    target 199
  ]
  edge [
    source 14
    target 200
  ]
  edge [
    source 14
    target 201
  ]
  edge [
    source 14
    target 20
  ]
  edge [
    source 14
    target 18
  ]
  edge [
    source 15
    target 202
  ]
  edge [
    source 15
    target 203
  ]
  edge [
    source 15
    target 204
  ]
  edge [
    source 15
    target 205
  ]
  edge [
    source 15
    target 206
  ]
  edge [
    source 15
    target 207
  ]
  edge [
    source 15
    target 208
  ]
  edge [
    source 15
    target 209
  ]
  edge [
    source 15
    target 210
  ]
  edge [
    source 15
    target 211
  ]
  edge [
    source 15
    target 59
  ]
  edge [
    source 15
    target 212
  ]
  edge [
    source 15
    target 213
  ]
  edge [
    source 15
    target 214
  ]
  edge [
    source 15
    target 215
  ]
  edge [
    source 15
    target 216
  ]
  edge [
    source 15
    target 217
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 218
  ]
  edge [
    source 16
    target 219
  ]
  edge [
    source 16
    target 220
  ]
  edge [
    source 16
    target 221
  ]
  edge [
    source 16
    target 222
  ]
  edge [
    source 16
    target 223
  ]
  edge [
    source 16
    target 224
  ]
  edge [
    source 16
    target 225
  ]
  edge [
    source 16
    target 226
  ]
  edge [
    source 16
    target 227
  ]
  edge [
    source 16
    target 228
  ]
  edge [
    source 16
    target 229
  ]
  edge [
    source 16
    target 230
  ]
  edge [
    source 16
    target 231
  ]
  edge [
    source 16
    target 232
  ]
  edge [
    source 16
    target 233
  ]
  edge [
    source 16
    target 234
  ]
  edge [
    source 16
    target 235
  ]
  edge [
    source 16
    target 236
  ]
  edge [
    source 16
    target 237
  ]
  edge [
    source 16
    target 238
  ]
  edge [
    source 16
    target 239
  ]
  edge [
    source 16
    target 240
  ]
  edge [
    source 16
    target 241
  ]
  edge [
    source 16
    target 242
  ]
  edge [
    source 16
    target 243
  ]
  edge [
    source 16
    target 244
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 245
  ]
  edge [
    source 17
    target 246
  ]
  edge [
    source 17
    target 247
  ]
  edge [
    source 17
    target 248
  ]
  edge [
    source 17
    target 249
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 250
  ]
  edge [
    source 18
    target 251
  ]
  edge [
    source 18
    target 252
  ]
  edge [
    source 18
    target 253
  ]
  edge [
    source 18
    target 254
  ]
  edge [
    source 18
    target 255
  ]
  edge [
    source 18
    target 256
  ]
  edge [
    source 18
    target 257
  ]
  edge [
    source 18
    target 258
  ]
  edge [
    source 18
    target 259
  ]
  edge [
    source 18
    target 117
  ]
  edge [
    source 18
    target 260
  ]
  edge [
    source 18
    target 261
  ]
  edge [
    source 18
    target 262
  ]
  edge [
    source 18
    target 263
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 264
  ]
  edge [
    source 19
    target 265
  ]
  edge [
    source 19
    target 266
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 267
  ]
  edge [
    source 20
    target 268
  ]
  edge [
    source 20
    target 269
  ]
  edge [
    source 20
    target 270
  ]
  edge [
    source 20
    target 271
  ]
  edge [
    source 20
    target 272
  ]
  edge [
    source 20
    target 273
  ]
  edge [
    source 20
    target 274
  ]
  edge [
    source 20
    target 275
  ]
  edge [
    source 20
    target 276
  ]
  edge [
    source 20
    target 277
  ]
  edge [
    source 20
    target 278
  ]
  edge [
    source 20
    target 279
  ]
  edge [
    source 20
    target 280
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 281
  ]
  edge [
    source 21
    target 282
  ]
  edge [
    source 21
    target 283
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 25
  ]
  edge [
    source 22
    target 284
  ]
  edge [
    source 22
    target 285
  ]
  edge [
    source 22
    target 286
  ]
  edge [
    source 22
    target 287
  ]
  edge [
    source 22
    target 288
  ]
  edge [
    source 22
    target 289
  ]
  edge [
    source 22
    target 290
  ]
  edge [
    source 22
    target 291
  ]
  edge [
    source 22
    target 292
  ]
  edge [
    source 22
    target 293
  ]
  edge [
    source 22
    target 294
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 295
  ]
  edge [
    source 23
    target 296
  ]
  edge [
    source 23
    target 297
  ]
  edge [
    source 23
    target 298
  ]
  edge [
    source 23
    target 145
  ]
  edge [
    source 23
    target 299
  ]
  edge [
    source 23
    target 300
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 42
  ]
  edge [
    source 24
    target 44
  ]
  edge [
    source 24
    target 59
  ]
  edge [
    source 24
    target 60
  ]
  edge [
    source 24
    target 301
  ]
  edge [
    source 24
    target 302
  ]
  edge [
    source 24
    target 157
  ]
  edge [
    source 24
    target 303
  ]
  edge [
    source 24
    target 304
  ]
  edge [
    source 24
    target 305
  ]
  edge [
    source 24
    target 86
  ]
  edge [
    source 24
    target 306
  ]
  edge [
    source 24
    target 307
  ]
  edge [
    source 24
    target 308
  ]
  edge [
    source 24
    target 43
  ]
  edge [
    source 24
    target 309
  ]
  edge [
    source 24
    target 310
  ]
  edge [
    source 24
    target 561
  ]
  edge [
    source 24
    target 562
  ]
  edge [
    source 25
    target 79
  ]
  edge [
    source 25
    target 80
  ]
  edge [
    source 25
    target 311
  ]
  edge [
    source 25
    target 312
  ]
  edge [
    source 25
    target 313
  ]
  edge [
    source 25
    target 314
  ]
  edge [
    source 25
    target 315
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 57
  ]
  edge [
    source 26
    target 316
  ]
  edge [
    source 26
    target 317
  ]
  edge [
    source 26
    target 318
  ]
  edge [
    source 26
    target 319
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 58
  ]
  edge [
    source 27
    target 59
  ]
  edge [
    source 27
    target 34
  ]
  edge [
    source 27
    target 67
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 320
  ]
  edge [
    source 28
    target 321
  ]
  edge [
    source 28
    target 322
  ]
  edge [
    source 28
    target 323
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 324
  ]
  edge [
    source 29
    target 325
  ]
  edge [
    source 29
    target 326
  ]
  edge [
    source 29
    target 327
  ]
  edge [
    source 29
    target 328
  ]
  edge [
    source 29
    target 329
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 74
  ]
  edge [
    source 30
    target 75
  ]
  edge [
    source 30
    target 76
  ]
  edge [
    source 30
    target 77
  ]
  edge [
    source 30
    target 330
  ]
  edge [
    source 30
    target 62
  ]
  edge [
    source 30
    target 65
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 331
  ]
  edge [
    source 31
    target 332
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 303
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 67
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 333
  ]
  edge [
    source 37
    target 334
  ]
  edge [
    source 37
    target 335
  ]
  edge [
    source 37
    target 336
  ]
  edge [
    source 37
    target 337
  ]
  edge [
    source 37
    target 338
  ]
  edge [
    source 37
    target 339
  ]
  edge [
    source 37
    target 340
  ]
  edge [
    source 37
    target 86
  ]
  edge [
    source 37
    target 341
  ]
  edge [
    source 37
    target 342
  ]
  edge [
    source 37
    target 343
  ]
  edge [
    source 37
    target 344
  ]
  edge [
    source 37
    target 345
  ]
  edge [
    source 37
    target 346
  ]
  edge [
    source 37
    target 347
  ]
  edge [
    source 37
    target 348
  ]
  edge [
    source 37
    target 349
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 71
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 350
  ]
  edge [
    source 40
    target 351
  ]
  edge [
    source 40
    target 352
  ]
  edge [
    source 40
    target 353
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 55
  ]
  edge [
    source 41
    target 56
  ]
  edge [
    source 41
    target 354
  ]
  edge [
    source 41
    target 355
  ]
  edge [
    source 41
    target 57
  ]
  edge [
    source 41
    target 61
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 52
  ]
  edge [
    source 42
    target 54
  ]
  edge [
    source 42
    target 55
  ]
  edge [
    source 42
    target 81
  ]
  edge [
    source 42
    target 82
  ]
  edge [
    source 42
    target 50
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 78
  ]
  edge [
    source 43
    target 79
  ]
  edge [
    source 43
    target 157
  ]
  edge [
    source 43
    target 356
  ]
  edge [
    source 43
    target 357
  ]
  edge [
    source 43
    target 358
  ]
  edge [
    source 43
    target 359
  ]
  edge [
    source 43
    target 57
  ]
  edge [
    source 43
    target 83
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 53
  ]
  edge [
    source 44
    target 360
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 361
  ]
  edge [
    source 45
    target 362
  ]
  edge [
    source 45
    target 363
  ]
  edge [
    source 45
    target 364
  ]
  edge [
    source 45
    target 365
  ]
  edge [
    source 45
    target 366
  ]
  edge [
    source 45
    target 367
  ]
  edge [
    source 45
    target 368
  ]
  edge [
    source 45
    target 369
  ]
  edge [
    source 45
    target 370
  ]
  edge [
    source 45
    target 371
  ]
  edge [
    source 45
    target 372
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 373
  ]
  edge [
    source 46
    target 374
  ]
  edge [
    source 46
    target 100
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 375
  ]
  edge [
    source 47
    target 376
  ]
  edge [
    source 47
    target 377
  ]
  edge [
    source 47
    target 378
  ]
  edge [
    source 47
    target 379
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 380
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 381
  ]
  edge [
    source 49
    target 382
  ]
  edge [
    source 49
    target 383
  ]
  edge [
    source 49
    target 384
  ]
  edge [
    source 49
    target 385
  ]
  edge [
    source 49
    target 386
  ]
  edge [
    source 49
    target 387
  ]
  edge [
    source 49
    target 388
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 386
  ]
  edge [
    source 50
    target 389
  ]
  edge [
    source 50
    target 390
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 391
  ]
  edge [
    source 53
    target 392
  ]
  edge [
    source 53
    target 393
  ]
  edge [
    source 53
    target 394
  ]
  edge [
    source 53
    target 68
  ]
  edge [
    source 54
    target 61
  ]
  edge [
    source 54
    target 62
  ]
  edge [
    source 54
    target 395
  ]
  edge [
    source 54
    target 204
  ]
  edge [
    source 54
    target 396
  ]
  edge [
    source 54
    target 397
  ]
  edge [
    source 54
    target 398
  ]
  edge [
    source 54
    target 399
  ]
  edge [
    source 54
    target 400
  ]
  edge [
    source 54
    target 401
  ]
  edge [
    source 54
    target 54
  ]
  edge [
    source 54
    target 66
  ]
  edge [
    source 55
    target 402
  ]
  edge [
    source 55
    target 403
  ]
  edge [
    source 55
    target 404
  ]
  edge [
    source 55
    target 405
  ]
  edge [
    source 55
    target 406
  ]
  edge [
    source 55
    target 407
  ]
  edge [
    source 55
    target 370
  ]
  edge [
    source 55
    target 408
  ]
  edge [
    source 55
    target 409
  ]
  edge [
    source 55
    target 87
  ]
  edge [
    source 55
    target 63
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 410
  ]
  edge [
    source 56
    target 411
  ]
  edge [
    source 56
    target 412
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 70
  ]
  edge [
    source 57
    target 71
  ]
  edge [
    source 57
    target 84
  ]
  edge [
    source 57
    target 87
  ]
  edge [
    source 57
    target 88
  ]
  edge [
    source 57
    target 413
  ]
  edge [
    source 57
    target 316
  ]
  edge [
    source 57
    target 414
  ]
  edge [
    source 57
    target 415
  ]
  edge [
    source 57
    target 416
  ]
  edge [
    source 57
    target 417
  ]
  edge [
    source 57
    target 418
  ]
  edge [
    source 57
    target 419
  ]
  edge [
    source 57
    target 420
  ]
  edge [
    source 57
    target 212
  ]
  edge [
    source 57
    target 421
  ]
  edge [
    source 57
    target 422
  ]
  edge [
    source 57
    target 83
  ]
  edge [
    source 57
    target 61
  ]
  edge [
    source 58
    target 64
  ]
  edge [
    source 58
    target 65
  ]
  edge [
    source 58
    target 69
  ]
  edge [
    source 58
    target 85
  ]
  edge [
    source 58
    target 423
  ]
  edge [
    source 58
    target 424
  ]
  edge [
    source 58
    target 425
  ]
  edge [
    source 58
    target 426
  ]
  edge [
    source 58
    target 427
  ]
  edge [
    source 59
    target 157
  ]
  edge [
    source 59
    target 428
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 88
  ]
  edge [
    source 61
    target 77
  ]
  edge [
    source 61
    target 78
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 429
  ]
  edge [
    source 62
    target 430
  ]
  edge [
    source 62
    target 431
  ]
  edge [
    source 62
    target 432
  ]
  edge [
    source 62
    target 433
  ]
  edge [
    source 62
    target 434
  ]
  edge [
    source 62
    target 435
  ]
  edge [
    source 62
    target 436
  ]
  edge [
    source 62
    target 437
  ]
  edge [
    source 62
    target 438
  ]
  edge [
    source 62
    target 74
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 218
  ]
  edge [
    source 63
    target 439
  ]
  edge [
    source 63
    target 440
  ]
  edge [
    source 63
    target 441
  ]
  edge [
    source 63
    target 442
  ]
  edge [
    source 63
    target 443
  ]
  edge [
    source 63
    target 444
  ]
  edge [
    source 64
    target 137
  ]
  edge [
    source 64
    target 445
  ]
  edge [
    source 64
    target 446
  ]
  edge [
    source 64
    target 447
  ]
  edge [
    source 64
    target 448
  ]
  edge [
    source 64
    target 449
  ]
  edge [
    source 64
    target 450
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 451
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 66
    target 452
  ]
  edge [
    source 66
    target 453
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 67
    target 454
  ]
  edge [
    source 67
    target 455
  ]
  edge [
    source 67
    target 456
  ]
  edge [
    source 67
    target 457
  ]
  edge [
    source 67
    target 458
  ]
  edge [
    source 67
    target 129
  ]
  edge [
    source 67
    target 322
  ]
  edge [
    source 67
    target 459
  ]
  edge [
    source 67
    target 460
  ]
  edge [
    source 68
    target 461
  ]
  edge [
    source 68
    target 462
  ]
  edge [
    source 68
    target 463
  ]
  edge [
    source 68
    target 464
  ]
  edge [
    source 68
    target 465
  ]
  edge [
    source 68
    target 466
  ]
  edge [
    source 68
    target 467
  ]
  edge [
    source 68
    target 468
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 70
    target 469
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 71
    target 470
  ]
  edge [
    source 71
    target 471
  ]
  edge [
    source 71
    target 472
  ]
  edge [
    source 71
    target 473
  ]
  edge [
    source 71
    target 474
  ]
  edge [
    source 71
    target 475
  ]
  edge [
    source 71
    target 476
  ]
  edge [
    source 71
    target 477
  ]
  edge [
    source 71
    target 478
  ]
  edge [
    source 71
    target 479
  ]
  edge [
    source 71
    target 480
  ]
  edge [
    source 71
    target 481
  ]
  edge [
    source 71
    target 482
  ]
  edge [
    source 71
    target 483
  ]
  edge [
    source 71
    target 484
  ]
  edge [
    source 71
    target 485
  ]
  edge [
    source 71
    target 486
  ]
  edge [
    source 71
    target 487
  ]
  edge [
    source 71
    target 488
  ]
  edge [
    source 71
    target 489
  ]
  edge [
    source 71
    target 490
  ]
  edge [
    source 71
    target 491
  ]
  edge [
    source 71
    target 492
  ]
  edge [
    source 71
    target 493
  ]
  edge [
    source 71
    target 494
  ]
  edge [
    source 71
    target 85
  ]
  edge [
    source 72
    target 73
  ]
  edge [
    source 72
    target 454
  ]
  edge [
    source 72
    target 495
  ]
  edge [
    source 72
    target 294
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 75
    target 76
  ]
  edge [
    source 75
    target 496
  ]
  edge [
    source 75
    target 497
  ]
  edge [
    source 76
    target 498
  ]
  edge [
    source 76
    target 499
  ]
  edge [
    source 76
    target 500
  ]
  edge [
    source 76
    target 501
  ]
  edge [
    source 76
    target 502
  ]
  edge [
    source 77
    target 503
  ]
  edge [
    source 77
    target 504
  ]
  edge [
    source 77
    target 505
  ]
  edge [
    source 77
    target 506
  ]
  edge [
    source 80
    target 81
  ]
  edge [
    source 81
    target 146
  ]
  edge [
    source 81
    target 147
  ]
  edge [
    source 81
    target 148
  ]
  edge [
    source 81
    target 507
  ]
  edge [
    source 81
    target 149
  ]
  edge [
    source 81
    target 150
  ]
  edge [
    source 81
    target 508
  ]
  edge [
    source 81
    target 151
  ]
  edge [
    source 81
    target 400
  ]
  edge [
    source 81
    target 152
  ]
  edge [
    source 82
    target 83
  ]
  edge [
    source 82
    target 509
  ]
  edge [
    source 82
    target 510
  ]
  edge [
    source 82
    target 511
  ]
  edge [
    source 82
    target 512
  ]
  edge [
    source 82
    target 513
  ]
  edge [
    source 83
    target 514
  ]
  edge [
    source 83
    target 515
  ]
  edge [
    source 83
    target 516
  ]
  edge [
    source 83
    target 212
  ]
  edge [
    source 83
    target 517
  ]
  edge [
    source 83
    target 518
  ]
  edge [
    source 83
    target 388
  ]
  edge [
    source 84
    target 519
  ]
  edge [
    source 85
    target 86
  ]
  edge [
    source 85
    target 520
  ]
  edge [
    source 85
    target 521
  ]
  edge [
    source 85
    target 522
  ]
  edge [
    source 85
    target 523
  ]
  edge [
    source 85
    target 524
  ]
  edge [
    source 85
    target 525
  ]
  edge [
    source 85
    target 526
  ]
  edge [
    source 85
    target 527
  ]
  edge [
    source 85
    target 528
  ]
  edge [
    source 85
    target 231
  ]
  edge [
    source 85
    target 529
  ]
  edge [
    source 85
    target 530
  ]
  edge [
    source 85
    target 531
  ]
  edge [
    source 86
    target 87
  ]
  edge [
    source 86
    target 335
  ]
  edge [
    source 86
    target 470
  ]
  edge [
    source 86
    target 532
  ]
  edge [
    source 86
    target 533
  ]
  edge [
    source 86
    target 534
  ]
  edge [
    source 86
    target 535
  ]
  edge [
    source 86
    target 270
  ]
  edge [
    source 86
    target 536
  ]
  edge [
    source 86
    target 537
  ]
  edge [
    source 86
    target 538
  ]
  edge [
    source 86
    target 347
  ]
  edge [
    source 86
    target 539
  ]
  edge [
    source 86
    target 540
  ]
  edge [
    source 86
    target 541
  ]
  edge [
    source 86
    target 542
  ]
  edge [
    source 86
    target 333
  ]
  edge [
    source 86
    target 337
  ]
  edge [
    source 86
    target 543
  ]
  edge [
    source 86
    target 544
  ]
  edge [
    source 86
    target 487
  ]
  edge [
    source 86
    target 545
  ]
  edge [
    source 86
    target 348
  ]
  edge [
    source 86
    target 546
  ]
  edge [
    source 86
    target 547
  ]
  edge [
    source 86
    target 548
  ]
  edge [
    source 86
    target 549
  ]
  edge [
    source 86
    target 550
  ]
  edge [
    source 86
    target 344
  ]
  edge [
    source 87
    target 90
  ]
  edge [
    source 87
    target 430
  ]
  edge [
    source 87
    target 551
  ]
  edge [
    source 87
    target 552
  ]
  edge [
    source 87
    target 553
  ]
  edge [
    source 87
    target 554
  ]
  edge [
    source 87
    target 555
  ]
  edge [
    source 87
    target 556
  ]
  edge [
    source 557
    target 558
  ]
  edge [
    source 558
    target 563
  ]
  edge [
    source 559
    target 560
  ]
  edge [
    source 561
    target 562
  ]
  edge [
    source 564
    target 565
  ]
  edge [
    source 566
    target 567
  ]
  edge [
    source 566
    target 568
  ]
  edge [
    source 566
    target 569
  ]
  edge [
    source 566
    target 570
  ]
  edge [
    source 566
    target 571
  ]
  edge [
    source 566
    target 572
  ]
  edge [
    source 566
    target 573
  ]
  edge [
    source 567
    target 568
  ]
  edge [
    source 567
    target 569
  ]
  edge [
    source 567
    target 570
  ]
  edge [
    source 567
    target 571
  ]
  edge [
    source 567
    target 572
  ]
  edge [
    source 567
    target 573
  ]
  edge [
    source 568
    target 569
  ]
  edge [
    source 568
    target 570
  ]
  edge [
    source 568
    target 571
  ]
  edge [
    source 568
    target 572
  ]
  edge [
    source 568
    target 573
  ]
  edge [
    source 568
    target 574
  ]
  edge [
    source 568
    target 575
  ]
  edge [
    source 568
    target 576
  ]
  edge [
    source 568
    target 577
  ]
  edge [
    source 568
    target 578
  ]
  edge [
    source 569
    target 570
  ]
  edge [
    source 569
    target 571
  ]
  edge [
    source 569
    target 572
  ]
  edge [
    source 569
    target 573
  ]
  edge [
    source 570
    target 571
  ]
  edge [
    source 570
    target 572
  ]
  edge [
    source 570
    target 573
  ]
  edge [
    source 571
    target 572
  ]
  edge [
    source 571
    target 573
  ]
  edge [
    source 572
    target 573
  ]
  edge [
    source 574
    target 575
  ]
  edge [
    source 574
    target 576
  ]
  edge [
    source 574
    target 577
  ]
  edge [
    source 574
    target 578
  ]
  edge [
    source 575
    target 576
  ]
  edge [
    source 575
    target 577
  ]
  edge [
    source 575
    target 578
  ]
  edge [
    source 576
    target 577
  ]
  edge [
    source 576
    target 578
  ]
  edge [
    source 577
    target 578
  ]
]
