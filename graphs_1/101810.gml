graph [
  maxDegree 36
  minDegree 1
  meanDegree 1.7916666666666667
  density 0.038120567375886524
  graphCliqueNumber 2
  node [
    id 0
    label "edwin"
    origin "text"
  ]
  node [
    id 1
    label "bendyk"
    origin "text"
  ]
  node [
    id 2
    label "pisz"
    origin "text"
  ]
  node [
    id 3
    label "strona"
    origin "text"
  ]
  node [
    id 4
    label "rz&#261;dowy"
    origin "text"
  ]
  node [
    id 5
    label "skr&#281;canie"
  ]
  node [
    id 6
    label "voice"
  ]
  node [
    id 7
    label "forma"
  ]
  node [
    id 8
    label "internet"
  ]
  node [
    id 9
    label "skr&#281;ci&#263;"
  ]
  node [
    id 10
    label "kartka"
  ]
  node [
    id 11
    label "orientowa&#263;"
  ]
  node [
    id 12
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 13
    label "powierzchnia"
  ]
  node [
    id 14
    label "plik"
  ]
  node [
    id 15
    label "bok"
  ]
  node [
    id 16
    label "pagina"
  ]
  node [
    id 17
    label "orientowanie"
  ]
  node [
    id 18
    label "fragment"
  ]
  node [
    id 19
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 20
    label "s&#261;d"
  ]
  node [
    id 21
    label "skr&#281;ca&#263;"
  ]
  node [
    id 22
    label "g&#243;ra"
  ]
  node [
    id 23
    label "serwis_internetowy"
  ]
  node [
    id 24
    label "orientacja"
  ]
  node [
    id 25
    label "linia"
  ]
  node [
    id 26
    label "skr&#281;cenie"
  ]
  node [
    id 27
    label "layout"
  ]
  node [
    id 28
    label "zorientowa&#263;"
  ]
  node [
    id 29
    label "zorientowanie"
  ]
  node [
    id 30
    label "obiekt"
  ]
  node [
    id 31
    label "podmiot"
  ]
  node [
    id 32
    label "ty&#322;"
  ]
  node [
    id 33
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 34
    label "logowanie"
  ]
  node [
    id 35
    label "adres_internetowy"
  ]
  node [
    id 36
    label "uj&#281;cie"
  ]
  node [
    id 37
    label "prz&#243;d"
  ]
  node [
    id 38
    label "posta&#263;"
  ]
  node [
    id 39
    label "wsp&#243;lny"
  ]
  node [
    id 40
    label "Edwin"
  ]
  node [
    id 41
    label "Bendyk"
  ]
  node [
    id 42
    label "Ars"
  ]
  node [
    id 43
    label "Technica"
  ]
  node [
    id 44
    label "Alexander"
  ]
  node [
    id 45
    label "Galloway"
  ]
  node [
    id 46
    label "Washington"
  ]
  node [
    id 47
    label "posta"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 46
    target 47
  ]
]
