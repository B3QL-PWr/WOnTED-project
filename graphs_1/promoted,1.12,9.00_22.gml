graph [
  maxDegree 71
  minDegree 1
  meanDegree 2.010362694300518
  density 0.010470639032815199
  graphCliqueNumber 2
  node [
    id 0
    label "beyonce"
    origin "text"
  ]
  node [
    id 1
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 2
    label "lato"
    origin "text"
  ]
  node [
    id 3
    label "by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "idol"
    origin "text"
  ]
  node [
    id 5
    label "wz&#243;r"
    origin "text"
  ]
  node [
    id 6
    label "dla"
    origin "text"
  ]
  node [
    id 7
    label "feministka"
    origin "text"
  ]
  node [
    id 8
    label "ca&#322;y"
    origin "text"
  ]
  node [
    id 9
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 10
    label "wyzyskiwa&#263;"
    origin "text"
  ]
  node [
    id 11
    label "pracownica"
    origin "text"
  ]
  node [
    id 12
    label "sri"
    origin "text"
  ]
  node [
    id 13
    label "lanka"
    origin "text"
  ]
  node [
    id 14
    label "szyja"
    origin "text"
  ]
  node [
    id 15
    label "ubranie"
    origin "text"
  ]
  node [
    id 16
    label "ekskluzywny"
    origin "text"
  ]
  node [
    id 17
    label "marek"
    origin "text"
  ]
  node [
    id 18
    label "pora_roku"
  ]
  node [
    id 19
    label "si&#281;ga&#263;"
  ]
  node [
    id 20
    label "trwa&#263;"
  ]
  node [
    id 21
    label "obecno&#347;&#263;"
  ]
  node [
    id 22
    label "stan"
  ]
  node [
    id 23
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 24
    label "stand"
  ]
  node [
    id 25
    label "mie&#263;_miejsce"
  ]
  node [
    id 26
    label "uczestniczy&#263;"
  ]
  node [
    id 27
    label "chodzi&#263;"
  ]
  node [
    id 28
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 29
    label "equal"
  ]
  node [
    id 30
    label "figura"
  ]
  node [
    id 31
    label "Eastwood"
  ]
  node [
    id 32
    label "snowman"
  ]
  node [
    id 33
    label "gwiazda"
  ]
  node [
    id 34
    label "typ"
  ]
  node [
    id 35
    label "cz&#322;owiek"
  ]
  node [
    id 36
    label "rule"
  ]
  node [
    id 37
    label "projekt"
  ]
  node [
    id 38
    label "zapis"
  ]
  node [
    id 39
    label "motyw"
  ]
  node [
    id 40
    label "ruch"
  ]
  node [
    id 41
    label "figure"
  ]
  node [
    id 42
    label "dekal"
  ]
  node [
    id 43
    label "ideal"
  ]
  node [
    id 44
    label "mildew"
  ]
  node [
    id 45
    label "spos&#243;b"
  ]
  node [
    id 46
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 47
    label "dzia&#322;aczka"
  ]
  node [
    id 48
    label "zwolenniczka"
  ]
  node [
    id 49
    label "du&#380;y"
  ]
  node [
    id 50
    label "jedyny"
  ]
  node [
    id 51
    label "kompletny"
  ]
  node [
    id 52
    label "zdr&#243;w"
  ]
  node [
    id 53
    label "&#380;ywy"
  ]
  node [
    id 54
    label "ca&#322;o"
  ]
  node [
    id 55
    label "pe&#322;ny"
  ]
  node [
    id 56
    label "calu&#347;ko"
  ]
  node [
    id 57
    label "podobny"
  ]
  node [
    id 58
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 59
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 60
    label "obszar"
  ]
  node [
    id 61
    label "obiekt_naturalny"
  ]
  node [
    id 62
    label "przedmiot"
  ]
  node [
    id 63
    label "biosfera"
  ]
  node [
    id 64
    label "grupa"
  ]
  node [
    id 65
    label "stw&#243;r"
  ]
  node [
    id 66
    label "Stary_&#346;wiat"
  ]
  node [
    id 67
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 68
    label "rzecz"
  ]
  node [
    id 69
    label "magnetosfera"
  ]
  node [
    id 70
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 71
    label "environment"
  ]
  node [
    id 72
    label "Nowy_&#346;wiat"
  ]
  node [
    id 73
    label "geosfera"
  ]
  node [
    id 74
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 75
    label "planeta"
  ]
  node [
    id 76
    label "przejmowa&#263;"
  ]
  node [
    id 77
    label "litosfera"
  ]
  node [
    id 78
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 79
    label "makrokosmos"
  ]
  node [
    id 80
    label "barysfera"
  ]
  node [
    id 81
    label "biota"
  ]
  node [
    id 82
    label "p&#243;&#322;noc"
  ]
  node [
    id 83
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 84
    label "fauna"
  ]
  node [
    id 85
    label "wszechstworzenie"
  ]
  node [
    id 86
    label "geotermia"
  ]
  node [
    id 87
    label "biegun"
  ]
  node [
    id 88
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 89
    label "ekosystem"
  ]
  node [
    id 90
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 91
    label "teren"
  ]
  node [
    id 92
    label "zjawisko"
  ]
  node [
    id 93
    label "p&#243;&#322;kula"
  ]
  node [
    id 94
    label "atmosfera"
  ]
  node [
    id 95
    label "mikrokosmos"
  ]
  node [
    id 96
    label "class"
  ]
  node [
    id 97
    label "po&#322;udnie"
  ]
  node [
    id 98
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 99
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 100
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 101
    label "przejmowanie"
  ]
  node [
    id 102
    label "przestrze&#324;"
  ]
  node [
    id 103
    label "asymilowanie_si&#281;"
  ]
  node [
    id 104
    label "przej&#261;&#263;"
  ]
  node [
    id 105
    label "ekosfera"
  ]
  node [
    id 106
    label "przyroda"
  ]
  node [
    id 107
    label "ciemna_materia"
  ]
  node [
    id 108
    label "geoida"
  ]
  node [
    id 109
    label "Wsch&#243;d"
  ]
  node [
    id 110
    label "populace"
  ]
  node [
    id 111
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 112
    label "huczek"
  ]
  node [
    id 113
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 114
    label "Ziemia"
  ]
  node [
    id 115
    label "universe"
  ]
  node [
    id 116
    label "ozonosfera"
  ]
  node [
    id 117
    label "rze&#378;ba"
  ]
  node [
    id 118
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 119
    label "zagranica"
  ]
  node [
    id 120
    label "hydrosfera"
  ]
  node [
    id 121
    label "woda"
  ]
  node [
    id 122
    label "kuchnia"
  ]
  node [
    id 123
    label "przej&#281;cie"
  ]
  node [
    id 124
    label "czarna_dziura"
  ]
  node [
    id 125
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 126
    label "morze"
  ]
  node [
    id 127
    label "wykorzystywa&#263;"
  ]
  node [
    id 128
    label "feat"
  ]
  node [
    id 129
    label "kauczuk"
  ]
  node [
    id 130
    label "cz&#322;onek"
  ]
  node [
    id 131
    label "gruczo&#322;_tarczycowy"
  ]
  node [
    id 132
    label "grdyka"
  ]
  node [
    id 133
    label "kark"
  ]
  node [
    id 134
    label "&#380;y&#322;a_szyjna_przednia"
  ]
  node [
    id 135
    label "d&#243;&#322;_nadobojczykowy"
  ]
  node [
    id 136
    label "ci&#261;g_komunikacyjny"
  ]
  node [
    id 137
    label "&#380;y&#322;a_szyjna_zewn&#281;trzna"
  ]
  node [
    id 138
    label "&#380;y&#322;a_szyjna_wewn&#281;trzna"
  ]
  node [
    id 139
    label "gardziel"
  ]
  node [
    id 140
    label "neck"
  ]
  node [
    id 141
    label "mi&#281;sie&#324;_pochy&#322;y"
  ]
  node [
    id 142
    label "podgardle"
  ]
  node [
    id 143
    label "przedbramie"
  ]
  node [
    id 144
    label "mi&#281;sie&#324;_mostkowo-obojczykowo-sutkowy"
  ]
  node [
    id 145
    label "nerw_przeponowy"
  ]
  node [
    id 146
    label "przyodzianie"
  ]
  node [
    id 147
    label "za&#322;o&#380;y&#263;"
  ]
  node [
    id 148
    label "zrzucenie"
  ]
  node [
    id 149
    label "odziewek"
  ]
  node [
    id 150
    label "upi&#281;kszenie"
  ]
  node [
    id 151
    label "pi&#281;kniejszy"
  ]
  node [
    id 152
    label "garderoba"
  ]
  node [
    id 153
    label "adoption"
  ]
  node [
    id 154
    label "kr&#243;j"
  ]
  node [
    id 155
    label "pasmanteria"
  ]
  node [
    id 156
    label "pochodzi&#263;"
  ]
  node [
    id 157
    label "ubranie_si&#281;"
  ]
  node [
    id 158
    label "wyko&#324;czenie"
  ]
  node [
    id 159
    label "znosi&#263;"
  ]
  node [
    id 160
    label "infliction"
  ]
  node [
    id 161
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 162
    label "przymierzenie"
  ]
  node [
    id 163
    label "poubieranie"
  ]
  node [
    id 164
    label "poprzystrajanie"
  ]
  node [
    id 165
    label "znoszenie"
  ]
  node [
    id 166
    label "przebranie"
  ]
  node [
    id 167
    label "przywdzianie"
  ]
  node [
    id 168
    label "w&#322;o&#380;enie"
  ]
  node [
    id 169
    label "pochodzenie"
  ]
  node [
    id 170
    label "str&#243;j"
  ]
  node [
    id 171
    label "nosi&#263;"
  ]
  node [
    id 172
    label "odzie&#380;"
  ]
  node [
    id 173
    label "gorset"
  ]
  node [
    id 174
    label "zrzuci&#263;"
  ]
  node [
    id 175
    label "obleczenie"
  ]
  node [
    id 176
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 177
    label "umieszczenie"
  ]
  node [
    id 178
    label "wyszukany"
  ]
  node [
    id 179
    label "elitarny"
  ]
  node [
    id 180
    label "wymagaj&#261;cy"
  ]
  node [
    id 181
    label "galantyna"
  ]
  node [
    id 182
    label "luksusowo"
  ]
  node [
    id 183
    label "wyj&#261;tkowy"
  ]
  node [
    id 184
    label "zamkni&#281;ty"
  ]
  node [
    id 185
    label "wspania&#322;y"
  ]
  node [
    id 186
    label "ekskluzywnie"
  ]
  node [
    id 187
    label "drogi"
  ]
  node [
    id 188
    label "w&#261;ski"
  ]
  node [
    id 189
    label "selerowate"
  ]
  node [
    id 190
    label "bylina"
  ]
  node [
    id 191
    label "hygrofit"
  ]
  node [
    id 192
    label "Sri"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 15
  ]
  edge [
    source 6
    target 16
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 9
    target 100
  ]
  edge [
    source 9
    target 101
  ]
  edge [
    source 9
    target 102
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 104
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 9
    target 116
  ]
  edge [
    source 9
    target 117
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 9
    target 119
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 127
  ]
  edge [
    source 10
    target 128
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 129
  ]
  edge [
    source 13
    target 192
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 130
  ]
  edge [
    source 14
    target 131
  ]
  edge [
    source 14
    target 132
  ]
  edge [
    source 14
    target 133
  ]
  edge [
    source 14
    target 134
  ]
  edge [
    source 14
    target 135
  ]
  edge [
    source 14
    target 136
  ]
  edge [
    source 14
    target 137
  ]
  edge [
    source 14
    target 138
  ]
  edge [
    source 14
    target 139
  ]
  edge [
    source 14
    target 140
  ]
  edge [
    source 14
    target 141
  ]
  edge [
    source 14
    target 142
  ]
  edge [
    source 14
    target 143
  ]
  edge [
    source 14
    target 144
  ]
  edge [
    source 14
    target 145
  ]
  edge [
    source 15
    target 146
  ]
  edge [
    source 15
    target 147
  ]
  edge [
    source 15
    target 148
  ]
  edge [
    source 15
    target 149
  ]
  edge [
    source 15
    target 150
  ]
  edge [
    source 15
    target 151
  ]
  edge [
    source 15
    target 152
  ]
  edge [
    source 15
    target 153
  ]
  edge [
    source 15
    target 154
  ]
  edge [
    source 15
    target 155
  ]
  edge [
    source 15
    target 156
  ]
  edge [
    source 15
    target 157
  ]
  edge [
    source 15
    target 158
  ]
  edge [
    source 15
    target 159
  ]
  edge [
    source 15
    target 160
  ]
  edge [
    source 15
    target 161
  ]
  edge [
    source 15
    target 162
  ]
  edge [
    source 15
    target 163
  ]
  edge [
    source 15
    target 164
  ]
  edge [
    source 15
    target 165
  ]
  edge [
    source 15
    target 166
  ]
  edge [
    source 15
    target 167
  ]
  edge [
    source 15
    target 168
  ]
  edge [
    source 15
    target 169
  ]
  edge [
    source 15
    target 170
  ]
  edge [
    source 15
    target 171
  ]
  edge [
    source 15
    target 172
  ]
  edge [
    source 15
    target 173
  ]
  edge [
    source 15
    target 174
  ]
  edge [
    source 15
    target 175
  ]
  edge [
    source 15
    target 176
  ]
  edge [
    source 15
    target 177
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 178
  ]
  edge [
    source 16
    target 179
  ]
  edge [
    source 16
    target 180
  ]
  edge [
    source 16
    target 181
  ]
  edge [
    source 16
    target 182
  ]
  edge [
    source 16
    target 183
  ]
  edge [
    source 16
    target 184
  ]
  edge [
    source 16
    target 185
  ]
  edge [
    source 16
    target 186
  ]
  edge [
    source 16
    target 187
  ]
  edge [
    source 16
    target 188
  ]
  edge [
    source 17
    target 189
  ]
  edge [
    source 17
    target 190
  ]
  edge [
    source 17
    target 191
  ]
]
