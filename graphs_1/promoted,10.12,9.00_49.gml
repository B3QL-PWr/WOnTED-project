graph [
  maxDegree 23
  minDegree 1
  meanDegree 1.951219512195122
  density 0.04878048780487805
  graphCliqueNumber 2
  node [
    id 0
    label "prawdziwy"
    origin "text"
  ]
  node [
    id 1
    label "bieg"
    origin "text"
  ]
  node [
    id 2
    label "zdarzenie"
    origin "text"
  ]
  node [
    id 3
    label "szczery"
  ]
  node [
    id 4
    label "naprawd&#281;"
  ]
  node [
    id 5
    label "zgodny"
  ]
  node [
    id 6
    label "naturalny"
  ]
  node [
    id 7
    label "realnie"
  ]
  node [
    id 8
    label "prawdziwie"
  ]
  node [
    id 9
    label "m&#261;dry"
  ]
  node [
    id 10
    label "&#380;ywny"
  ]
  node [
    id 11
    label "podobny"
  ]
  node [
    id 12
    label "wy&#347;cig"
  ]
  node [
    id 13
    label "parametr"
  ]
  node [
    id 14
    label "ciek_wodny"
  ]
  node [
    id 15
    label "roll"
  ]
  node [
    id 16
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 17
    label "przedbieg"
  ]
  node [
    id 18
    label "konkurencja"
  ]
  node [
    id 19
    label "tryb"
  ]
  node [
    id 20
    label "bystrzyca"
  ]
  node [
    id 21
    label "pr&#261;d"
  ]
  node [
    id 22
    label "pozycja"
  ]
  node [
    id 23
    label "linia"
  ]
  node [
    id 24
    label "procedura"
  ]
  node [
    id 25
    label "ruch"
  ]
  node [
    id 26
    label "czo&#322;&#243;wka"
  ]
  node [
    id 27
    label "kurs"
  ]
  node [
    id 28
    label "syfon"
  ]
  node [
    id 29
    label "kierunek"
  ]
  node [
    id 30
    label "cycle"
  ]
  node [
    id 31
    label "proces"
  ]
  node [
    id 32
    label "d&#261;&#380;enie"
  ]
  node [
    id 33
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 34
    label "czynno&#347;&#263;"
  ]
  node [
    id 35
    label "motyw"
  ]
  node [
    id 36
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 37
    label "fabu&#322;a"
  ]
  node [
    id 38
    label "przebiec"
  ]
  node [
    id 39
    label "przebiegni&#281;cie"
  ]
  node [
    id 40
    label "charakter"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
]
