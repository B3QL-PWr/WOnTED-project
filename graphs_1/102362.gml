graph [
  maxDegree 35
  minDegree 1
  meanDegree 2.2270742358078603
  density 0.009767869455297633
  graphCliqueNumber 3
  node [
    id 0
    label "podczas"
    origin "text"
  ]
  node [
    id 1
    label "comiesi&#281;czny"
    origin "text"
  ]
  node [
    id 2
    label "spotkanie"
    origin "text"
  ]
  node [
    id 3
    label "przedstawiciel"
    origin "text"
  ]
  node [
    id 4
    label "fundacja"
    origin "text"
  ]
  node [
    id 5
    label "nowa"
    origin "text"
  ]
  node [
    id 6
    label "media"
    origin "text"
  ]
  node [
    id 7
    label "zaprzyja&#378;niony"
    origin "text"
  ]
  node [
    id 8
    label "dziennikarz"
    origin "text"
  ]
  node [
    id 9
    label "student"
    origin "text"
  ]
  node [
    id 10
    label "wyk&#322;adowca"
    origin "text"
  ]
  node [
    id 11
    label "ehu"
    origin "text"
  ]
  node [
    id 12
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 13
    label "om&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 14
    label "zagadnienie"
    origin "text"
  ]
  node [
    id 15
    label "zwi&#261;za&#263;"
    origin "text"
  ]
  node [
    id 16
    label "dziennikarstwo"
    origin "text"
  ]
  node [
    id 17
    label "polska"
    origin "text"
  ]
  node [
    id 18
    label "europa"
    origin "text"
  ]
  node [
    id 19
    label "trend"
    origin "text"
  ]
  node [
    id 20
    label "r&#243;&#380;ny"
    origin "text"
  ]
  node [
    id 21
    label "dziedzina"
    origin "text"
  ]
  node [
    id 22
    label "grafik"
    origin "text"
  ]
  node [
    id 23
    label "prasowy"
    origin "text"
  ]
  node [
    id 24
    label "internet"
    origin "text"
  ]
  node [
    id 25
    label "jak"
    origin "text"
  ]
  node [
    id 26
    label "narz&#281;dzie"
    origin "text"
  ]
  node [
    id 27
    label "edukacyjny"
    origin "text"
  ]
  node [
    id 28
    label "sie&#263;"
    origin "text"
  ]
  node [
    id 29
    label "spo&#322;eczny"
    origin "text"
  ]
  node [
    id 30
    label "wideoblogi"
    origin "text"
  ]
  node [
    id 31
    label "zajecie"
    origin "text"
  ]
  node [
    id 32
    label "warsztatowy"
    origin "text"
  ]
  node [
    id 33
    label "poprowadzi&#263;"
    origin "text"
  ]
  node [
    id 34
    label "micha&#322;"
    origin "text"
  ]
  node [
    id 35
    label "mas&#322;owski"
    origin "text"
  ]
  node [
    id 36
    label "pawe&#322;"
    origin "text"
  ]
  node [
    id 37
    label "sito"
    origin "text"
  ]
  node [
    id 38
    label "dorota"
    origin "text"
  ]
  node [
    id 39
    label "cybulska"
    origin "text"
  ]
  node [
    id 40
    label "ale&#347;"
    origin "text"
  ]
  node [
    id 41
    label "&#322;ukashevich"
    origin "text"
  ]
  node [
    id 42
    label "horbulewicz"
    origin "text"
  ]
  node [
    id 43
    label "cykliczny"
  ]
  node [
    id 44
    label "comiesi&#281;cznie"
  ]
  node [
    id 45
    label "miesi&#281;czny"
  ]
  node [
    id 46
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 47
    label "po&#380;egnanie"
  ]
  node [
    id 48
    label "spowodowanie"
  ]
  node [
    id 49
    label "znalezienie"
  ]
  node [
    id 50
    label "znajomy"
  ]
  node [
    id 51
    label "doznanie"
  ]
  node [
    id 52
    label "employment"
  ]
  node [
    id 53
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 54
    label "gather"
  ]
  node [
    id 55
    label "powitanie"
  ]
  node [
    id 56
    label "spotykanie"
  ]
  node [
    id 57
    label "wydarzenie"
  ]
  node [
    id 58
    label "gathering"
  ]
  node [
    id 59
    label "spotkanie_si&#281;"
  ]
  node [
    id 60
    label "zdarzenie_si&#281;"
  ]
  node [
    id 61
    label "match"
  ]
  node [
    id 62
    label "zawarcie"
  ]
  node [
    id 63
    label "cz&#322;owiek"
  ]
  node [
    id 64
    label "cz&#322;onek"
  ]
  node [
    id 65
    label "substytuowanie"
  ]
  node [
    id 66
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 67
    label "przyk&#322;ad"
  ]
  node [
    id 68
    label "zast&#281;pca"
  ]
  node [
    id 69
    label "substytuowa&#263;"
  ]
  node [
    id 70
    label "foundation"
  ]
  node [
    id 71
    label "instytucja"
  ]
  node [
    id 72
    label "dar"
  ]
  node [
    id 73
    label "darowizna"
  ]
  node [
    id 74
    label "pocz&#261;tek"
  ]
  node [
    id 75
    label "gwiazda"
  ]
  node [
    id 76
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 77
    label "przekazior"
  ]
  node [
    id 78
    label "mass-media"
  ]
  node [
    id 79
    label "uzbrajanie"
  ]
  node [
    id 80
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 81
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 82
    label "medium"
  ]
  node [
    id 83
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 84
    label "nowiniarz"
  ]
  node [
    id 85
    label "akredytowa&#263;"
  ]
  node [
    id 86
    label "akredytowanie"
  ]
  node [
    id 87
    label "bran&#380;owiec"
  ]
  node [
    id 88
    label "publicysta"
  ]
  node [
    id 89
    label "tutor"
  ]
  node [
    id 90
    label "akademik"
  ]
  node [
    id 91
    label "immatrykulowanie"
  ]
  node [
    id 92
    label "s&#322;uchacz"
  ]
  node [
    id 93
    label "immatrykulowa&#263;"
  ]
  node [
    id 94
    label "absolwent"
  ]
  node [
    id 95
    label "indeks"
  ]
  node [
    id 96
    label "nauczyciel_akademicki"
  ]
  node [
    id 97
    label "prowadz&#261;cy"
  ]
  node [
    id 98
    label "proceed"
  ]
  node [
    id 99
    label "catch"
  ]
  node [
    id 100
    label "pozosta&#263;"
  ]
  node [
    id 101
    label "osta&#263;_si&#281;"
  ]
  node [
    id 102
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 103
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 104
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 105
    label "change"
  ]
  node [
    id 106
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 107
    label "przedyskutowa&#263;"
  ]
  node [
    id 108
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 109
    label "publicize"
  ]
  node [
    id 110
    label "temat"
  ]
  node [
    id 111
    label "sprawa"
  ]
  node [
    id 112
    label "problemat"
  ]
  node [
    id 113
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 114
    label "problematyka"
  ]
  node [
    id 115
    label "st&#281;&#380;e&#263;"
  ]
  node [
    id 116
    label "tobo&#322;ek"
  ]
  node [
    id 117
    label "obezw&#322;adni&#263;"
  ]
  node [
    id 118
    label "scali&#263;"
  ]
  node [
    id 119
    label "zawi&#261;za&#263;"
  ]
  node [
    id 120
    label "zatrzyma&#263;"
  ]
  node [
    id 121
    label "form"
  ]
  node [
    id 122
    label "bind"
  ]
  node [
    id 123
    label "wi&#261;zanie_chemiczne"
  ]
  node [
    id 124
    label "unify"
  ]
  node [
    id 125
    label "consort"
  ]
  node [
    id 126
    label "incorporate"
  ]
  node [
    id 127
    label "wi&#281;&#378;"
  ]
  node [
    id 128
    label "zobowi&#261;za&#263;"
  ]
  node [
    id 129
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 130
    label "w&#281;ze&#322;"
  ]
  node [
    id 131
    label "cz&#261;steczka_chemiczna"
  ]
  node [
    id 132
    label "powi&#261;za&#263;"
  ]
  node [
    id 133
    label "opakowa&#263;"
  ]
  node [
    id 134
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 135
    label "cement"
  ]
  node [
    id 136
    label "zaprawa"
  ]
  node [
    id 137
    label "relate"
  ]
  node [
    id 138
    label "nauka_humanistyczna"
  ]
  node [
    id 139
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 140
    label "medioznawca"
  ]
  node [
    id 141
    label "system"
  ]
  node [
    id 142
    label "fashionistka"
  ]
  node [
    id 143
    label "odzie&#380;"
  ]
  node [
    id 144
    label "ideologia"
  ]
  node [
    id 145
    label "tendencja_rozwojowa"
  ]
  node [
    id 146
    label "praktyka"
  ]
  node [
    id 147
    label "metoda"
  ]
  node [
    id 148
    label "zwyczaj"
  ]
  node [
    id 149
    label "r&#243;&#380;nie"
  ]
  node [
    id 150
    label "inny"
  ]
  node [
    id 151
    label "jaki&#347;"
  ]
  node [
    id 152
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 153
    label "zakres"
  ]
  node [
    id 154
    label "bezdro&#380;e"
  ]
  node [
    id 155
    label "zbi&#243;r"
  ]
  node [
    id 156
    label "funkcja"
  ]
  node [
    id 157
    label "sfera"
  ]
  node [
    id 158
    label "poddzia&#322;"
  ]
  node [
    id 159
    label "plastyk"
  ]
  node [
    id 160
    label "informatyk"
  ]
  node [
    id 161
    label "rozk&#322;ad"
  ]
  node [
    id 162
    label "diagram"
  ]
  node [
    id 163
    label "medialny"
  ]
  node [
    id 164
    label "medialnie"
  ]
  node [
    id 165
    label "us&#322;uga_internetowa"
  ]
  node [
    id 166
    label "biznes_elektroniczny"
  ]
  node [
    id 167
    label "punkt_dost&#281;pu"
  ]
  node [
    id 168
    label "hipertekst"
  ]
  node [
    id 169
    label "gra_sieciowa"
  ]
  node [
    id 170
    label "mem"
  ]
  node [
    id 171
    label "e-hazard"
  ]
  node [
    id 172
    label "sie&#263;_komputerowa"
  ]
  node [
    id 173
    label "podcast"
  ]
  node [
    id 174
    label "netbook"
  ]
  node [
    id 175
    label "provider"
  ]
  node [
    id 176
    label "cyberprzestrze&#324;"
  ]
  node [
    id 177
    label "grooming"
  ]
  node [
    id 178
    label "strona"
  ]
  node [
    id 179
    label "byd&#322;o"
  ]
  node [
    id 180
    label "zobo"
  ]
  node [
    id 181
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 182
    label "yakalo"
  ]
  node [
    id 183
    label "dzo"
  ]
  node [
    id 184
    label "przedmiot"
  ]
  node [
    id 185
    label "&#347;rodek"
  ]
  node [
    id 186
    label "po&#380;yteczny_idiota"
  ]
  node [
    id 187
    label "urz&#261;dzenie"
  ]
  node [
    id 188
    label "tylec"
  ]
  node [
    id 189
    label "niezb&#281;dnik"
  ]
  node [
    id 190
    label "spos&#243;b"
  ]
  node [
    id 191
    label "edukacyjnie"
  ]
  node [
    id 192
    label "gauze"
  ]
  node [
    id 193
    label "nitka"
  ]
  node [
    id 194
    label "mesh"
  ]
  node [
    id 195
    label "snu&#263;"
  ]
  node [
    id 196
    label "organization"
  ]
  node [
    id 197
    label "zasadzka"
  ]
  node [
    id 198
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 199
    label "web"
  ]
  node [
    id 200
    label "struktura"
  ]
  node [
    id 201
    label "organizacja"
  ]
  node [
    id 202
    label "vane"
  ]
  node [
    id 203
    label "kszta&#322;t"
  ]
  node [
    id 204
    label "obiekt"
  ]
  node [
    id 205
    label "wysnu&#263;"
  ]
  node [
    id 206
    label "instalacja"
  ]
  node [
    id 207
    label "net"
  ]
  node [
    id 208
    label "plecionka"
  ]
  node [
    id 209
    label "rozmieszczenie"
  ]
  node [
    id 210
    label "niepubliczny"
  ]
  node [
    id 211
    label "spo&#322;ecznie"
  ]
  node [
    id 212
    label "publiczny"
  ]
  node [
    id 213
    label "technicznie"
  ]
  node [
    id 214
    label "control"
  ]
  node [
    id 215
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 216
    label "zbudowa&#263;"
  ]
  node [
    id 217
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 218
    label "moderate"
  ]
  node [
    id 219
    label "leave"
  ]
  node [
    id 220
    label "guidebook"
  ]
  node [
    id 221
    label "nakre&#347;li&#263;"
  ]
  node [
    id 222
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 223
    label "doprowadzi&#263;"
  ]
  node [
    id 224
    label "krzywa"
  ]
  node [
    id 225
    label "odsiewacz"
  ]
  node [
    id 226
    label "przesiewacz"
  ]
  node [
    id 227
    label "przyrz&#261;d"
  ]
  node [
    id 228
    label "ocena"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 16
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 17
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 24
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 18
  ]
  edge [
    source 6
    target 22
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 19
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 111
  ]
  edge [
    source 14
    target 112
  ]
  edge [
    source 14
    target 113
  ]
  edge [
    source 14
    target 114
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 115
  ]
  edge [
    source 15
    target 116
  ]
  edge [
    source 15
    target 117
  ]
  edge [
    source 15
    target 118
  ]
  edge [
    source 15
    target 119
  ]
  edge [
    source 15
    target 120
  ]
  edge [
    source 15
    target 121
  ]
  edge [
    source 15
    target 122
  ]
  edge [
    source 15
    target 123
  ]
  edge [
    source 15
    target 124
  ]
  edge [
    source 15
    target 125
  ]
  edge [
    source 15
    target 126
  ]
  edge [
    source 15
    target 127
  ]
  edge [
    source 15
    target 128
  ]
  edge [
    source 15
    target 129
  ]
  edge [
    source 15
    target 130
  ]
  edge [
    source 15
    target 131
  ]
  edge [
    source 15
    target 132
  ]
  edge [
    source 15
    target 133
  ]
  edge [
    source 15
    target 134
  ]
  edge [
    source 15
    target 135
  ]
  edge [
    source 15
    target 136
  ]
  edge [
    source 15
    target 137
  ]
  edge [
    source 16
    target 138
  ]
  edge [
    source 16
    target 139
  ]
  edge [
    source 16
    target 140
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 22
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 141
  ]
  edge [
    source 19
    target 142
  ]
  edge [
    source 19
    target 143
  ]
  edge [
    source 19
    target 144
  ]
  edge [
    source 19
    target 145
  ]
  edge [
    source 19
    target 146
  ]
  edge [
    source 19
    target 147
  ]
  edge [
    source 19
    target 148
  ]
  edge [
    source 19
    target 21
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 149
  ]
  edge [
    source 20
    target 150
  ]
  edge [
    source 20
    target 151
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 152
  ]
  edge [
    source 21
    target 153
  ]
  edge [
    source 21
    target 154
  ]
  edge [
    source 21
    target 155
  ]
  edge [
    source 21
    target 156
  ]
  edge [
    source 21
    target 157
  ]
  edge [
    source 21
    target 158
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 159
  ]
  edge [
    source 22
    target 160
  ]
  edge [
    source 22
    target 161
  ]
  edge [
    source 22
    target 162
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 163
  ]
  edge [
    source 23
    target 164
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 165
  ]
  edge [
    source 24
    target 166
  ]
  edge [
    source 24
    target 167
  ]
  edge [
    source 24
    target 168
  ]
  edge [
    source 24
    target 169
  ]
  edge [
    source 24
    target 170
  ]
  edge [
    source 24
    target 171
  ]
  edge [
    source 24
    target 172
  ]
  edge [
    source 24
    target 173
  ]
  edge [
    source 24
    target 174
  ]
  edge [
    source 24
    target 175
  ]
  edge [
    source 24
    target 176
  ]
  edge [
    source 24
    target 177
  ]
  edge [
    source 24
    target 178
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 179
  ]
  edge [
    source 25
    target 180
  ]
  edge [
    source 25
    target 181
  ]
  edge [
    source 25
    target 182
  ]
  edge [
    source 25
    target 183
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 63
  ]
  edge [
    source 26
    target 184
  ]
  edge [
    source 26
    target 185
  ]
  edge [
    source 26
    target 186
  ]
  edge [
    source 26
    target 187
  ]
  edge [
    source 26
    target 188
  ]
  edge [
    source 26
    target 189
  ]
  edge [
    source 26
    target 190
  ]
  edge [
    source 26
    target 37
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 191
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 168
  ]
  edge [
    source 28
    target 192
  ]
  edge [
    source 28
    target 193
  ]
  edge [
    source 28
    target 194
  ]
  edge [
    source 28
    target 171
  ]
  edge [
    source 28
    target 174
  ]
  edge [
    source 28
    target 176
  ]
  edge [
    source 28
    target 166
  ]
  edge [
    source 28
    target 195
  ]
  edge [
    source 28
    target 196
  ]
  edge [
    source 28
    target 197
  ]
  edge [
    source 28
    target 198
  ]
  edge [
    source 28
    target 199
  ]
  edge [
    source 28
    target 175
  ]
  edge [
    source 28
    target 200
  ]
  edge [
    source 28
    target 165
  ]
  edge [
    source 28
    target 167
  ]
  edge [
    source 28
    target 201
  ]
  edge [
    source 28
    target 170
  ]
  edge [
    source 28
    target 202
  ]
  edge [
    source 28
    target 173
  ]
  edge [
    source 28
    target 177
  ]
  edge [
    source 28
    target 203
  ]
  edge [
    source 28
    target 178
  ]
  edge [
    source 28
    target 204
  ]
  edge [
    source 28
    target 205
  ]
  edge [
    source 28
    target 169
  ]
  edge [
    source 28
    target 206
  ]
  edge [
    source 28
    target 172
  ]
  edge [
    source 28
    target 207
  ]
  edge [
    source 28
    target 208
  ]
  edge [
    source 28
    target 209
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 210
  ]
  edge [
    source 29
    target 211
  ]
  edge [
    source 29
    target 212
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 213
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 214
  ]
  edge [
    source 33
    target 215
  ]
  edge [
    source 33
    target 216
  ]
  edge [
    source 33
    target 217
  ]
  edge [
    source 33
    target 218
  ]
  edge [
    source 33
    target 219
  ]
  edge [
    source 33
    target 220
  ]
  edge [
    source 33
    target 221
  ]
  edge [
    source 33
    target 222
  ]
  edge [
    source 33
    target 223
  ]
  edge [
    source 33
    target 224
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 41
  ]
  edge [
    source 34
    target 42
  ]
  edge [
    source 34
    target 184
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 225
  ]
  edge [
    source 37
    target 226
  ]
  edge [
    source 37
    target 227
  ]
  edge [
    source 37
    target 228
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 40
    target 41
  ]
]
