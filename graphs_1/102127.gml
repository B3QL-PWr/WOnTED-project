graph [
  maxDegree 20
  minDegree 1
  meanDegree 2.1
  density 0.013207547169811321
  graphCliqueNumber 3
  node [
    id 0
    label "tym"
    origin "text"
  ]
  node [
    id 1
    label "razem"
    origin "text"
  ]
  node [
    id 2
    label "sobotni"
    origin "text"
  ]
  node [
    id 3
    label "poranek"
    origin "text"
  ]
  node [
    id 4
    label "przywita&#263;"
    origin "text"
  ]
  node [
    id 5
    label "nasa"
    origin "text"
  ]
  node [
    id 6
    label "ulewny"
    origin "text"
  ]
  node [
    id 7
    label "deszcz"
    origin "text"
  ]
  node [
    id 8
    label "kiepski"
    origin "text"
  ]
  node [
    id 9
    label "pogoda"
    origin "text"
  ]
  node [
    id 10
    label "utrzymywa&#263;"
    origin "text"
  ]
  node [
    id 11
    label "si&#281;"
    origin "text"
  ]
  node [
    id 12
    label "przez"
    origin "text"
  ]
  node [
    id 13
    label "wi&#281;kszo&#347;&#263;"
    origin "text"
  ]
  node [
    id 14
    label "weekend"
    origin "text"
  ]
  node [
    id 15
    label "przesta&#263;"
    origin "text"
  ]
  node [
    id 16
    label "pada&#263;"
    origin "text"
  ]
  node [
    id 17
    label "dopiero"
    origin "text"
  ]
  node [
    id 18
    label "niedziela"
    origin "text"
  ]
  node [
    id 19
    label "oko&#322;o"
    origin "text"
  ]
  node [
    id 20
    label "po&#322;udnie"
    origin "text"
  ]
  node [
    id 21
    label "inicjatywa"
    origin "text"
  ]
  node [
    id 22
    label "adam"
    origin "text"
  ]
  node [
    id 23
    label "postanowi&#263;by&#263;"
    origin "text"
  ]
  node [
    id 24
    label "wykorzysta&#263;"
    origin "text"
  ]
  node [
    id 25
    label "chocia&#380;"
    origin "text"
  ]
  node [
    id 26
    label "ten"
    origin "text"
  ]
  node [
    id 27
    label "ko&#324;c&#243;wka"
    origin "text"
  ]
  node [
    id 28
    label "b&#322;oto"
    origin "text"
  ]
  node [
    id 29
    label "sp&#281;dzi&#263;"
    origin "text"
  ]
  node [
    id 30
    label "niedzielny"
    origin "text"
  ]
  node [
    id 31
    label "popo&#322;udnie"
    origin "text"
  ]
  node [
    id 32
    label "tora"
    origin "text"
  ]
  node [
    id 33
    label "&#322;&#261;cznie"
  ]
  node [
    id 34
    label "podkurek"
  ]
  node [
    id 35
    label "blady_&#347;wit"
  ]
  node [
    id 36
    label "dzie&#324;"
  ]
  node [
    id 37
    label "uczci&#263;"
  ]
  node [
    id 38
    label "uzewn&#281;trzni&#263;_si&#281;"
  ]
  node [
    id 39
    label "greet"
  ]
  node [
    id 40
    label "welcome"
  ]
  node [
    id 41
    label "pozdrowi&#263;"
  ]
  node [
    id 42
    label "ulewnie"
  ]
  node [
    id 43
    label "deszczowy"
  ]
  node [
    id 44
    label "silny"
  ]
  node [
    id 45
    label "burza"
  ]
  node [
    id 46
    label "opad"
  ]
  node [
    id 47
    label "mn&#243;stwo"
  ]
  node [
    id 48
    label "rain"
  ]
  node [
    id 49
    label "nieumiej&#281;tny"
  ]
  node [
    id 50
    label "marnie"
  ]
  node [
    id 51
    label "z&#322;y"
  ]
  node [
    id 52
    label "niemocny"
  ]
  node [
    id 53
    label "kiepsko"
  ]
  node [
    id 54
    label "pok&#243;j"
  ]
  node [
    id 55
    label "atak"
  ]
  node [
    id 56
    label "czas"
  ]
  node [
    id 57
    label "program"
  ]
  node [
    id 58
    label "meteorology"
  ]
  node [
    id 59
    label "weather"
  ]
  node [
    id 60
    label "warunki"
  ]
  node [
    id 61
    label "zjawisko"
  ]
  node [
    id 62
    label "potrzyma&#263;"
  ]
  node [
    id 63
    label "prognoza_meteorologiczna"
  ]
  node [
    id 64
    label "zapewnia&#263;"
  ]
  node [
    id 65
    label "manewr"
  ]
  node [
    id 66
    label "byt"
  ]
  node [
    id 67
    label "cope"
  ]
  node [
    id 68
    label "&#322;o&#380;y&#263;"
  ]
  node [
    id 69
    label "zachowywa&#263;"
  ]
  node [
    id 70
    label "twierdzi&#263;"
  ]
  node [
    id 71
    label "trzyma&#263;"
  ]
  node [
    id 72
    label "corroborate"
  ]
  node [
    id 73
    label "sprawowa&#263;"
  ]
  node [
    id 74
    label "s&#261;dzi&#263;"
  ]
  node [
    id 75
    label "podtrzymywa&#263;"
  ]
  node [
    id 76
    label "defy"
  ]
  node [
    id 77
    label "panowa&#263;"
  ]
  node [
    id 78
    label "argue"
  ]
  node [
    id 79
    label "broni&#263;"
  ]
  node [
    id 80
    label "majority"
  ]
  node [
    id 81
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 82
    label "sobota"
  ]
  node [
    id 83
    label "tydzie&#324;"
  ]
  node [
    id 84
    label "fail"
  ]
  node [
    id 85
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 86
    label "leave_office"
  ]
  node [
    id 87
    label "sko&#324;czy&#263;"
  ]
  node [
    id 88
    label "coating"
  ]
  node [
    id 89
    label "drop"
  ]
  node [
    id 90
    label "zrobi&#263;"
  ]
  node [
    id 91
    label "zdycha&#263;"
  ]
  node [
    id 92
    label "by&#263;"
  ]
  node [
    id 93
    label "mie&#263;_miejsce"
  ]
  node [
    id 94
    label "die"
  ]
  node [
    id 95
    label "przestawa&#263;"
  ]
  node [
    id 96
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 97
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 98
    label "przelecie&#263;"
  ]
  node [
    id 99
    label "okazywa&#263;_si&#281;"
  ]
  node [
    id 100
    label "robi&#263;"
  ]
  node [
    id 101
    label "gin&#261;&#263;"
  ]
  node [
    id 102
    label "przypada&#263;"
  ]
  node [
    id 103
    label "poddawa&#263;_si&#281;"
  ]
  node [
    id 104
    label "czu&#263;_si&#281;"
  ]
  node [
    id 105
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 106
    label "fall"
  ]
  node [
    id 107
    label "spada&#263;"
  ]
  node [
    id 108
    label "s&#322;abn&#261;&#263;"
  ]
  node [
    id 109
    label "Dzie&#324;_Papieski"
  ]
  node [
    id 110
    label "Wielkanoc"
  ]
  node [
    id 111
    label "dzie&#324;_&#347;wi&#281;ty"
  ]
  node [
    id 112
    label "Niedziela_Palmowa"
  ]
  node [
    id 113
    label "Zes&#322;anie_Ducha_&#346;wi&#281;tego"
  ]
  node [
    id 114
    label "dzie&#324;_&#347;wi&#261;teczny"
  ]
  node [
    id 115
    label "bia&#322;a_niedziela"
  ]
  node [
    id 116
    label "niedziela_przewodnia"
  ]
  node [
    id 117
    label "dwunasta"
  ]
  node [
    id 118
    label "obszar"
  ]
  node [
    id 119
    label "Ziemia"
  ]
  node [
    id 120
    label "godzina"
  ]
  node [
    id 121
    label "strona_&#347;wiata"
  ]
  node [
    id 122
    label "&#347;rodek"
  ]
  node [
    id 123
    label "pora"
  ]
  node [
    id 124
    label "plan"
  ]
  node [
    id 125
    label "propozycja"
  ]
  node [
    id 126
    label "samodzielno&#347;&#263;"
  ]
  node [
    id 127
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 128
    label "u&#380;y&#263;"
  ]
  node [
    id 129
    label "seize"
  ]
  node [
    id 130
    label "skorzysta&#263;"
  ]
  node [
    id 131
    label "okre&#347;lony"
  ]
  node [
    id 132
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 133
    label "szereg"
  ]
  node [
    id 134
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 135
    label "koniec"
  ]
  node [
    id 136
    label "spout"
  ]
  node [
    id 137
    label "ending"
  ]
  node [
    id 138
    label "kawa&#322;ek"
  ]
  node [
    id 139
    label "finish"
  ]
  node [
    id 140
    label "zako&#324;czenie"
  ]
  node [
    id 141
    label "morfem"
  ]
  node [
    id 142
    label "end_point"
  ]
  node [
    id 143
    label "terminal"
  ]
  node [
    id 144
    label "ziemia"
  ]
  node [
    id 145
    label "gn&#243;j"
  ]
  node [
    id 146
    label "bajor"
  ]
  node [
    id 147
    label "szuwar"
  ]
  node [
    id 148
    label "teren"
  ]
  node [
    id 149
    label "rudawka"
  ]
  node [
    id 150
    label "moczar"
  ]
  node [
    id 151
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 152
    label "usun&#261;&#263;"
  ]
  node [
    id 153
    label "authorize"
  ]
  node [
    id 154
    label "base_on_balls"
  ]
  node [
    id 155
    label "skupi&#263;"
  ]
  node [
    id 156
    label "&#347;wi&#261;teczny"
  ]
  node [
    id 157
    label "odwieczerz"
  ]
  node [
    id 158
    label "zw&#243;j"
  ]
  node [
    id 159
    label "Tora"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 54
  ]
  edge [
    source 9
    target 55
  ]
  edge [
    source 9
    target 56
  ]
  edge [
    source 9
    target 57
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 64
  ]
  edge [
    source 10
    target 65
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 10
    target 69
  ]
  edge [
    source 10
    target 70
  ]
  edge [
    source 10
    target 71
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 14
  ]
  edge [
    source 11
    target 28
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 80
  ]
  edge [
    source 13
    target 81
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 27
  ]
  edge [
    source 14
    target 18
  ]
  edge [
    source 14
    target 82
  ]
  edge [
    source 14
    target 83
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 84
  ]
  edge [
    source 15
    target 85
  ]
  edge [
    source 15
    target 86
  ]
  edge [
    source 15
    target 87
  ]
  edge [
    source 15
    target 88
  ]
  edge [
    source 15
    target 89
  ]
  edge [
    source 15
    target 90
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 91
  ]
  edge [
    source 16
    target 92
  ]
  edge [
    source 16
    target 93
  ]
  edge [
    source 16
    target 94
  ]
  edge [
    source 16
    target 95
  ]
  edge [
    source 16
    target 96
  ]
  edge [
    source 16
    target 97
  ]
  edge [
    source 16
    target 98
  ]
  edge [
    source 16
    target 99
  ]
  edge [
    source 16
    target 100
  ]
  edge [
    source 16
    target 101
  ]
  edge [
    source 16
    target 102
  ]
  edge [
    source 16
    target 103
  ]
  edge [
    source 16
    target 104
  ]
  edge [
    source 16
    target 105
  ]
  edge [
    source 16
    target 106
  ]
  edge [
    source 16
    target 107
  ]
  edge [
    source 16
    target 108
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 109
  ]
  edge [
    source 18
    target 110
  ]
  edge [
    source 18
    target 111
  ]
  edge [
    source 18
    target 112
  ]
  edge [
    source 18
    target 113
  ]
  edge [
    source 18
    target 114
  ]
  edge [
    source 18
    target 115
  ]
  edge [
    source 18
    target 116
  ]
  edge [
    source 18
    target 83
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 117
  ]
  edge [
    source 20
    target 118
  ]
  edge [
    source 20
    target 119
  ]
  edge [
    source 20
    target 120
  ]
  edge [
    source 20
    target 121
  ]
  edge [
    source 20
    target 81
  ]
  edge [
    source 20
    target 122
  ]
  edge [
    source 20
    target 123
  ]
  edge [
    source 20
    target 36
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 124
  ]
  edge [
    source 21
    target 125
  ]
  edge [
    source 21
    target 126
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 127
  ]
  edge [
    source 24
    target 128
  ]
  edge [
    source 24
    target 129
  ]
  edge [
    source 24
    target 130
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 131
  ]
  edge [
    source 26
    target 132
  ]
  edge [
    source 27
    target 133
  ]
  edge [
    source 27
    target 134
  ]
  edge [
    source 27
    target 135
  ]
  edge [
    source 27
    target 136
  ]
  edge [
    source 27
    target 137
  ]
  edge [
    source 27
    target 138
  ]
  edge [
    source 27
    target 139
  ]
  edge [
    source 27
    target 81
  ]
  edge [
    source 27
    target 140
  ]
  edge [
    source 27
    target 141
  ]
  edge [
    source 27
    target 142
  ]
  edge [
    source 27
    target 143
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 144
  ]
  edge [
    source 28
    target 145
  ]
  edge [
    source 28
    target 146
  ]
  edge [
    source 28
    target 147
  ]
  edge [
    source 28
    target 148
  ]
  edge [
    source 28
    target 149
  ]
  edge [
    source 28
    target 150
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 151
  ]
  edge [
    source 29
    target 152
  ]
  edge [
    source 29
    target 153
  ]
  edge [
    source 29
    target 154
  ]
  edge [
    source 29
    target 155
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 156
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 36
  ]
  edge [
    source 31
    target 123
  ]
  edge [
    source 31
    target 157
  ]
  edge [
    source 32
    target 158
  ]
  edge [
    source 32
    target 159
  ]
]
