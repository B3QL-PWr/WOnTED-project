graph [
  maxDegree 9
  minDegree 1
  meanDegree 1.875
  density 0.125
  graphCliqueNumber 2
  node [
    id 0
    label "rosyjski"
    origin "text"
  ]
  node [
    id 1
    label "wszystko"
    origin "text"
  ]
  node [
    id 2
    label "wida&#263;"
    origin "text"
  ]
  node [
    id 3
    label "wiadomo"
    origin "text"
  ]
  node [
    id 4
    label "po_rosyjsku"
  ]
  node [
    id 5
    label "j&#281;zyk"
  ]
  node [
    id 6
    label "wielkoruski"
  ]
  node [
    id 7
    label "kacapski"
  ]
  node [
    id 8
    label "Russian"
  ]
  node [
    id 9
    label "rusek"
  ]
  node [
    id 10
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 11
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 12
    label "lock"
  ]
  node [
    id 13
    label "absolut"
  ]
  node [
    id 14
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 15
    label "by&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 15
  ]
]
