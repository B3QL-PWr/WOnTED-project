graph [
  maxDegree 324
  minDegree 1
  meanDegree 2.041198501872659
  density 0.003829640716459023
  graphCliqueNumber 5
  node [
    id 0
    label "czeczenia"
    origin "text"
  ]
  node [
    id 1
    label "nazwa"
    origin "text"
  ]
  node [
    id 2
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 3
    label "cz&#281;sto"
    origin "text"
  ]
  node [
    id 4
    label "przewija&#263;"
    origin "text"
  ]
  node [
    id 5
    label "si&#281;"
    origin "text"
  ]
  node [
    id 6
    label "gazeta"
    origin "text"
  ]
  node [
    id 7
    label "telewizja"
    origin "text"
  ]
  node [
    id 8
    label "szary"
    origin "text"
  ]
  node [
    id 9
    label "obywatel"
    origin "text"
  ]
  node [
    id 10
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 11
    label "niewiele"
    origin "text"
  ]
  node [
    id 12
    label "wi&#281;kszo&#347;&#263;"
    origin "text"
  ]
  node [
    id 13
    label "polak"
    origin "text"
  ]
  node [
    id 14
    label "nawet"
    origin "text"
  ]
  node [
    id 15
    label "wiedzie&#263;"
    origin "text"
  ]
  node [
    id 16
    label "gdzie"
    origin "text"
  ]
  node [
    id 17
    label "ten"
    origin "text"
  ]
  node [
    id 18
    label "kraj"
    origin "text"
  ]
  node [
    id 19
    label "le&#380;"
    origin "text"
  ]
  node [
    id 20
    label "dlaczego"
    origin "text"
  ]
  node [
    id 21
    label "walczy&#263;"
    origin "text"
  ]
  node [
    id 22
    label "rosja"
    origin "text"
  ]
  node [
    id 23
    label "chcie&#263;byby&#263;"
    origin "text"
  ]
  node [
    id 24
    label "cho&#263;"
    origin "text"
  ]
  node [
    id 25
    label "troch&#281;"
    origin "text"
  ]
  node [
    id 26
    label "przybli&#380;y&#263;"
    origin "text"
  ]
  node [
    id 27
    label "problematyka"
    origin "text"
  ]
  node [
    id 28
    label "kaukaz"
    origin "text"
  ]
  node [
    id 29
    label "napi&#281;cie"
    origin "text"
  ]
  node [
    id 30
    label "tamtejszy"
    origin "text"
  ]
  node [
    id 31
    label "republika"
    origin "text"
  ]
  node [
    id 32
    label "term"
  ]
  node [
    id 33
    label "wezwanie"
  ]
  node [
    id 34
    label "leksem"
  ]
  node [
    id 35
    label "patron"
  ]
  node [
    id 36
    label "cz&#281;stokro&#263;"
  ]
  node [
    id 37
    label "cz&#281;sty"
  ]
  node [
    id 38
    label "przebiera&#263;"
  ]
  node [
    id 39
    label "owija&#263;"
  ]
  node [
    id 40
    label "robi&#263;"
  ]
  node [
    id 41
    label "swathe"
  ]
  node [
    id 42
    label "kaseta"
  ]
  node [
    id 43
    label "powodowa&#263;"
  ]
  node [
    id 44
    label "pielucha"
  ]
  node [
    id 45
    label "prasa"
  ]
  node [
    id 46
    label "redakcja"
  ]
  node [
    id 47
    label "tytu&#322;"
  ]
  node [
    id 48
    label "rozk&#322;ad&#243;wka"
  ]
  node [
    id 49
    label "czasopismo"
  ]
  node [
    id 50
    label "Polsat"
  ]
  node [
    id 51
    label "paj&#281;czarz"
  ]
  node [
    id 52
    label "gadaj&#261;ca_g&#322;owa"
  ]
  node [
    id 53
    label "programowiec"
  ]
  node [
    id 54
    label "technologia"
  ]
  node [
    id 55
    label "urz&#261;dzenie_RTV"
  ]
  node [
    id 56
    label "Interwizja"
  ]
  node [
    id 57
    label "BBC"
  ]
  node [
    id 58
    label "ekran"
  ]
  node [
    id 59
    label "media"
  ]
  node [
    id 60
    label "odbieranie"
  ]
  node [
    id 61
    label "odbiera&#263;"
  ]
  node [
    id 62
    label "odbiornik"
  ]
  node [
    id 63
    label "instytucja"
  ]
  node [
    id 64
    label "du&#380;y_pok&#243;j"
  ]
  node [
    id 65
    label "studio"
  ]
  node [
    id 66
    label "telekomunikacja"
  ]
  node [
    id 67
    label "muza"
  ]
  node [
    id 68
    label "p&#322;owy"
  ]
  node [
    id 69
    label "zielono"
  ]
  node [
    id 70
    label "blady"
  ]
  node [
    id 71
    label "pochmurno"
  ]
  node [
    id 72
    label "szaro"
  ]
  node [
    id 73
    label "chmurnienie"
  ]
  node [
    id 74
    label "zwyczajny"
  ]
  node [
    id 75
    label "zwyk&#322;y"
  ]
  node [
    id 76
    label "szarzenie"
  ]
  node [
    id 77
    label "niezabawny"
  ]
  node [
    id 78
    label "brzydki"
  ]
  node [
    id 79
    label "nieciekawy"
  ]
  node [
    id 80
    label "oboj&#281;tny"
  ]
  node [
    id 81
    label "poszarzenie"
  ]
  node [
    id 82
    label "bezbarwnie"
  ]
  node [
    id 83
    label "ch&#322;odny"
  ]
  node [
    id 84
    label "srebrny"
  ]
  node [
    id 85
    label "spochmurnienie"
  ]
  node [
    id 86
    label "cz&#322;owiek"
  ]
  node [
    id 87
    label "przedstawiciel"
  ]
  node [
    id 88
    label "miastowy"
  ]
  node [
    id 89
    label "mieszkaniec"
  ]
  node [
    id 90
    label "pa&#324;stwo"
  ]
  node [
    id 91
    label "remark"
  ]
  node [
    id 92
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 93
    label "u&#380;ywa&#263;"
  ]
  node [
    id 94
    label "okre&#347;la&#263;"
  ]
  node [
    id 95
    label "j&#281;zyk"
  ]
  node [
    id 96
    label "say"
  ]
  node [
    id 97
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 98
    label "formu&#322;owa&#263;"
  ]
  node [
    id 99
    label "talk"
  ]
  node [
    id 100
    label "powiada&#263;"
  ]
  node [
    id 101
    label "informowa&#263;"
  ]
  node [
    id 102
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 103
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 104
    label "wydobywa&#263;"
  ]
  node [
    id 105
    label "express"
  ]
  node [
    id 106
    label "chew_the_fat"
  ]
  node [
    id 107
    label "dysfonia"
  ]
  node [
    id 108
    label "umie&#263;"
  ]
  node [
    id 109
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 110
    label "tell"
  ]
  node [
    id 111
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 112
    label "wyra&#380;a&#263;"
  ]
  node [
    id 113
    label "gaworzy&#263;"
  ]
  node [
    id 114
    label "rozmawia&#263;"
  ]
  node [
    id 115
    label "dziama&#263;"
  ]
  node [
    id 116
    label "prawi&#263;"
  ]
  node [
    id 117
    label "nieistotnie"
  ]
  node [
    id 118
    label "nieznaczny"
  ]
  node [
    id 119
    label "pomiernie"
  ]
  node [
    id 120
    label "ma&#322;y"
  ]
  node [
    id 121
    label "majority"
  ]
  node [
    id 122
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 123
    label "polski"
  ]
  node [
    id 124
    label "cognizance"
  ]
  node [
    id 125
    label "okre&#347;lony"
  ]
  node [
    id 126
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 127
    label "Skandynawia"
  ]
  node [
    id 128
    label "Rwanda"
  ]
  node [
    id 129
    label "Filipiny"
  ]
  node [
    id 130
    label "Yorkshire"
  ]
  node [
    id 131
    label "Kaukaz"
  ]
  node [
    id 132
    label "Podbeskidzie"
  ]
  node [
    id 133
    label "Toskania"
  ]
  node [
    id 134
    label "&#321;emkowszczyzna"
  ]
  node [
    id 135
    label "obszar"
  ]
  node [
    id 136
    label "Monako"
  ]
  node [
    id 137
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 138
    label "Amhara"
  ]
  node [
    id 139
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 140
    label "Lombardia"
  ]
  node [
    id 141
    label "Korea"
  ]
  node [
    id 142
    label "Kalabria"
  ]
  node [
    id 143
    label "Czarnog&#243;ra"
  ]
  node [
    id 144
    label "Ghana"
  ]
  node [
    id 145
    label "Tyrol"
  ]
  node [
    id 146
    label "Malawi"
  ]
  node [
    id 147
    label "Indonezja"
  ]
  node [
    id 148
    label "Bu&#322;garia"
  ]
  node [
    id 149
    label "Nauru"
  ]
  node [
    id 150
    label "Kenia"
  ]
  node [
    id 151
    label "Pamir"
  ]
  node [
    id 152
    label "Kambod&#380;a"
  ]
  node [
    id 153
    label "Lubelszczyzna"
  ]
  node [
    id 154
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 155
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 156
    label "Mali"
  ]
  node [
    id 157
    label "&#379;ywiecczyzna"
  ]
  node [
    id 158
    label "Austria"
  ]
  node [
    id 159
    label "interior"
  ]
  node [
    id 160
    label "Europa_Wschodnia"
  ]
  node [
    id 161
    label "Armenia"
  ]
  node [
    id 162
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 163
    label "Fid&#380;i"
  ]
  node [
    id 164
    label "Tuwalu"
  ]
  node [
    id 165
    label "Zabajkale"
  ]
  node [
    id 166
    label "Etiopia"
  ]
  node [
    id 167
    label "Malezja"
  ]
  node [
    id 168
    label "Malta"
  ]
  node [
    id 169
    label "Kaszuby"
  ]
  node [
    id 170
    label "Noworosja"
  ]
  node [
    id 171
    label "Bo&#347;nia"
  ]
  node [
    id 172
    label "Tad&#380;ykistan"
  ]
  node [
    id 173
    label "Grenada"
  ]
  node [
    id 174
    label "Ba&#322;kany"
  ]
  node [
    id 175
    label "Wehrlen"
  ]
  node [
    id 176
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 177
    label "Anglia"
  ]
  node [
    id 178
    label "Kielecczyzna"
  ]
  node [
    id 179
    label "Rumunia"
  ]
  node [
    id 180
    label "Pomorze_Zachodnie"
  ]
  node [
    id 181
    label "Maroko"
  ]
  node [
    id 182
    label "Bhutan"
  ]
  node [
    id 183
    label "Opolskie"
  ]
  node [
    id 184
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 185
    label "Ko&#322;yma"
  ]
  node [
    id 186
    label "Oksytania"
  ]
  node [
    id 187
    label "S&#322;owacja"
  ]
  node [
    id 188
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 189
    label "Seszele"
  ]
  node [
    id 190
    label "Syjon"
  ]
  node [
    id 191
    label "Kuwejt"
  ]
  node [
    id 192
    label "Arabia_Saudyjska"
  ]
  node [
    id 193
    label "Kociewie"
  ]
  node [
    id 194
    label "Kanada"
  ]
  node [
    id 195
    label "Ekwador"
  ]
  node [
    id 196
    label "ziemia"
  ]
  node [
    id 197
    label "Japonia"
  ]
  node [
    id 198
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 199
    label "Hiszpania"
  ]
  node [
    id 200
    label "Wyspy_Marshalla"
  ]
  node [
    id 201
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 202
    label "D&#380;ibuti"
  ]
  node [
    id 203
    label "Botswana"
  ]
  node [
    id 204
    label "Huculszczyzna"
  ]
  node [
    id 205
    label "Wietnam"
  ]
  node [
    id 206
    label "Egipt"
  ]
  node [
    id 207
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 208
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 209
    label "Burkina_Faso"
  ]
  node [
    id 210
    label "Bawaria"
  ]
  node [
    id 211
    label "Niemcy"
  ]
  node [
    id 212
    label "Khitai"
  ]
  node [
    id 213
    label "Macedonia"
  ]
  node [
    id 214
    label "Albania"
  ]
  node [
    id 215
    label "Madagaskar"
  ]
  node [
    id 216
    label "Bahrajn"
  ]
  node [
    id 217
    label "Jemen"
  ]
  node [
    id 218
    label "Lesoto"
  ]
  node [
    id 219
    label "Maghreb"
  ]
  node [
    id 220
    label "Samoa"
  ]
  node [
    id 221
    label "Andora"
  ]
  node [
    id 222
    label "Bory_Tucholskie"
  ]
  node [
    id 223
    label "Chiny"
  ]
  node [
    id 224
    label "Europa_Zachodnia"
  ]
  node [
    id 225
    label "Cypr"
  ]
  node [
    id 226
    label "Wielka_Brytania"
  ]
  node [
    id 227
    label "Kerala"
  ]
  node [
    id 228
    label "Podhale"
  ]
  node [
    id 229
    label "Kabylia"
  ]
  node [
    id 230
    label "Ukraina"
  ]
  node [
    id 231
    label "Paragwaj"
  ]
  node [
    id 232
    label "Trynidad_i_Tobago"
  ]
  node [
    id 233
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 234
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 235
    label "Ma&#322;opolska"
  ]
  node [
    id 236
    label "Polesie"
  ]
  node [
    id 237
    label "Liguria"
  ]
  node [
    id 238
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 239
    label "Libia"
  ]
  node [
    id 240
    label "&#321;&#243;dzkie"
  ]
  node [
    id 241
    label "Surinam"
  ]
  node [
    id 242
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 243
    label "Palestyna"
  ]
  node [
    id 244
    label "Nigeria"
  ]
  node [
    id 245
    label "Australia"
  ]
  node [
    id 246
    label "Honduras"
  ]
  node [
    id 247
    label "Bojkowszczyzna"
  ]
  node [
    id 248
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 249
    label "Karaiby"
  ]
  node [
    id 250
    label "Peru"
  ]
  node [
    id 251
    label "USA"
  ]
  node [
    id 252
    label "Bangladesz"
  ]
  node [
    id 253
    label "Kazachstan"
  ]
  node [
    id 254
    label "Nepal"
  ]
  node [
    id 255
    label "Irak"
  ]
  node [
    id 256
    label "Nadrenia"
  ]
  node [
    id 257
    label "Sudan"
  ]
  node [
    id 258
    label "S&#261;decczyzna"
  ]
  node [
    id 259
    label "Sand&#380;ak"
  ]
  node [
    id 260
    label "San_Marino"
  ]
  node [
    id 261
    label "Burundi"
  ]
  node [
    id 262
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 263
    label "Dominikana"
  ]
  node [
    id 264
    label "Komory"
  ]
  node [
    id 265
    label "Zakarpacie"
  ]
  node [
    id 266
    label "Gwatemala"
  ]
  node [
    id 267
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 268
    label "Zag&#243;rze"
  ]
  node [
    id 269
    label "Andaluzja"
  ]
  node [
    id 270
    label "granica_pa&#324;stwa"
  ]
  node [
    id 271
    label "Turkiestan"
  ]
  node [
    id 272
    label "Naddniestrze"
  ]
  node [
    id 273
    label "Hercegowina"
  ]
  node [
    id 274
    label "Brunei"
  ]
  node [
    id 275
    label "Iran"
  ]
  node [
    id 276
    label "jednostka_administracyjna"
  ]
  node [
    id 277
    label "Zimbabwe"
  ]
  node [
    id 278
    label "Namibia"
  ]
  node [
    id 279
    label "Meksyk"
  ]
  node [
    id 280
    label "Opolszczyzna"
  ]
  node [
    id 281
    label "Kamerun"
  ]
  node [
    id 282
    label "Afryka_Wschodnia"
  ]
  node [
    id 283
    label "Szlezwik"
  ]
  node [
    id 284
    label "Lotaryngia"
  ]
  node [
    id 285
    label "Somalia"
  ]
  node [
    id 286
    label "Angola"
  ]
  node [
    id 287
    label "Gabon"
  ]
  node [
    id 288
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 289
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 290
    label "Nowa_Zelandia"
  ]
  node [
    id 291
    label "Mozambik"
  ]
  node [
    id 292
    label "Tunezja"
  ]
  node [
    id 293
    label "Tajwan"
  ]
  node [
    id 294
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 295
    label "Liban"
  ]
  node [
    id 296
    label "Jordania"
  ]
  node [
    id 297
    label "Tonga"
  ]
  node [
    id 298
    label "Czad"
  ]
  node [
    id 299
    label "Gwinea"
  ]
  node [
    id 300
    label "Liberia"
  ]
  node [
    id 301
    label "Belize"
  ]
  node [
    id 302
    label "Mazowsze"
  ]
  node [
    id 303
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 304
    label "Benin"
  ]
  node [
    id 305
    label "&#321;otwa"
  ]
  node [
    id 306
    label "Syria"
  ]
  node [
    id 307
    label "Afryka_Zachodnia"
  ]
  node [
    id 308
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 309
    label "Dominika"
  ]
  node [
    id 310
    label "Antigua_i_Barbuda"
  ]
  node [
    id 311
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 312
    label "Hanower"
  ]
  node [
    id 313
    label "Galicja"
  ]
  node [
    id 314
    label "Szkocja"
  ]
  node [
    id 315
    label "Walia"
  ]
  node [
    id 316
    label "Afganistan"
  ]
  node [
    id 317
    label "W&#322;ochy"
  ]
  node [
    id 318
    label "Kiribati"
  ]
  node [
    id 319
    label "Szwajcaria"
  ]
  node [
    id 320
    label "Powi&#347;le"
  ]
  node [
    id 321
    label "Chorwacja"
  ]
  node [
    id 322
    label "Sahara_Zachodnia"
  ]
  node [
    id 323
    label "Tajlandia"
  ]
  node [
    id 324
    label "Salwador"
  ]
  node [
    id 325
    label "Bahamy"
  ]
  node [
    id 326
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 327
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 328
    label "Zamojszczyzna"
  ]
  node [
    id 329
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 330
    label "S&#322;owenia"
  ]
  node [
    id 331
    label "Gambia"
  ]
  node [
    id 332
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 333
    label "Urugwaj"
  ]
  node [
    id 334
    label "Podlasie"
  ]
  node [
    id 335
    label "Zair"
  ]
  node [
    id 336
    label "Erytrea"
  ]
  node [
    id 337
    label "Laponia"
  ]
  node [
    id 338
    label "Kujawy"
  ]
  node [
    id 339
    label "Umbria"
  ]
  node [
    id 340
    label "Rosja"
  ]
  node [
    id 341
    label "Mauritius"
  ]
  node [
    id 342
    label "Niger"
  ]
  node [
    id 343
    label "Uganda"
  ]
  node [
    id 344
    label "Turkmenistan"
  ]
  node [
    id 345
    label "Turcja"
  ]
  node [
    id 346
    label "Mezoameryka"
  ]
  node [
    id 347
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 348
    label "Irlandia"
  ]
  node [
    id 349
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 350
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 351
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 352
    label "Gwinea_Bissau"
  ]
  node [
    id 353
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 354
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 355
    label "Kurdystan"
  ]
  node [
    id 356
    label "Belgia"
  ]
  node [
    id 357
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 358
    label "Palau"
  ]
  node [
    id 359
    label "Barbados"
  ]
  node [
    id 360
    label "Wenezuela"
  ]
  node [
    id 361
    label "W&#281;gry"
  ]
  node [
    id 362
    label "Chile"
  ]
  node [
    id 363
    label "Argentyna"
  ]
  node [
    id 364
    label "Kolumbia"
  ]
  node [
    id 365
    label "Armagnac"
  ]
  node [
    id 366
    label "Kampania"
  ]
  node [
    id 367
    label "Sierra_Leone"
  ]
  node [
    id 368
    label "Azerbejd&#380;an"
  ]
  node [
    id 369
    label "Kongo"
  ]
  node [
    id 370
    label "Polinezja"
  ]
  node [
    id 371
    label "Warmia"
  ]
  node [
    id 372
    label "Pakistan"
  ]
  node [
    id 373
    label "Liechtenstein"
  ]
  node [
    id 374
    label "Wielkopolska"
  ]
  node [
    id 375
    label "Nikaragua"
  ]
  node [
    id 376
    label "Senegal"
  ]
  node [
    id 377
    label "brzeg"
  ]
  node [
    id 378
    label "Bordeaux"
  ]
  node [
    id 379
    label "Lauda"
  ]
  node [
    id 380
    label "Indie"
  ]
  node [
    id 381
    label "Mazury"
  ]
  node [
    id 382
    label "Suazi"
  ]
  node [
    id 383
    label "Polska"
  ]
  node [
    id 384
    label "Algieria"
  ]
  node [
    id 385
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 386
    label "Jamajka"
  ]
  node [
    id 387
    label "Timor_Wschodni"
  ]
  node [
    id 388
    label "Oceania"
  ]
  node [
    id 389
    label "Kostaryka"
  ]
  node [
    id 390
    label "Lasko"
  ]
  node [
    id 391
    label "Podkarpacie"
  ]
  node [
    id 392
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 393
    label "Kuba"
  ]
  node [
    id 394
    label "Mauretania"
  ]
  node [
    id 395
    label "Amazonia"
  ]
  node [
    id 396
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 397
    label "Portoryko"
  ]
  node [
    id 398
    label "Brazylia"
  ]
  node [
    id 399
    label "Mo&#322;dawia"
  ]
  node [
    id 400
    label "organizacja"
  ]
  node [
    id 401
    label "Litwa"
  ]
  node [
    id 402
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 403
    label "Kirgistan"
  ]
  node [
    id 404
    label "Izrael"
  ]
  node [
    id 405
    label "Grecja"
  ]
  node [
    id 406
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 407
    label "Kurpie"
  ]
  node [
    id 408
    label "Holandia"
  ]
  node [
    id 409
    label "Sri_Lanka"
  ]
  node [
    id 410
    label "Tonkin"
  ]
  node [
    id 411
    label "Katar"
  ]
  node [
    id 412
    label "Azja_Wschodnia"
  ]
  node [
    id 413
    label "Kaszmir"
  ]
  node [
    id 414
    label "Mikronezja"
  ]
  node [
    id 415
    label "Ukraina_Zachodnia"
  ]
  node [
    id 416
    label "Laos"
  ]
  node [
    id 417
    label "Mongolia"
  ]
  node [
    id 418
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 419
    label "Malediwy"
  ]
  node [
    id 420
    label "Zambia"
  ]
  node [
    id 421
    label "Turyngia"
  ]
  node [
    id 422
    label "Tanzania"
  ]
  node [
    id 423
    label "Gujana"
  ]
  node [
    id 424
    label "Apulia"
  ]
  node [
    id 425
    label "Uzbekistan"
  ]
  node [
    id 426
    label "Panama"
  ]
  node [
    id 427
    label "Czechy"
  ]
  node [
    id 428
    label "Gruzja"
  ]
  node [
    id 429
    label "Baszkiria"
  ]
  node [
    id 430
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 431
    label "Francja"
  ]
  node [
    id 432
    label "Serbia"
  ]
  node [
    id 433
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 434
    label "Togo"
  ]
  node [
    id 435
    label "Estonia"
  ]
  node [
    id 436
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 437
    label "Indochiny"
  ]
  node [
    id 438
    label "Boliwia"
  ]
  node [
    id 439
    label "Oman"
  ]
  node [
    id 440
    label "Portugalia"
  ]
  node [
    id 441
    label "Wyspy_Salomona"
  ]
  node [
    id 442
    label "Haiti"
  ]
  node [
    id 443
    label "Luksemburg"
  ]
  node [
    id 444
    label "Lubuskie"
  ]
  node [
    id 445
    label "Biskupizna"
  ]
  node [
    id 446
    label "Birma"
  ]
  node [
    id 447
    label "Rodezja"
  ]
  node [
    id 448
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 449
    label "zmaga&#263;_si&#281;"
  ]
  node [
    id 450
    label "cope"
  ]
  node [
    id 451
    label "contend"
  ]
  node [
    id 452
    label "zawody"
  ]
  node [
    id 453
    label "opowiada&#263;_si&#281;"
  ]
  node [
    id 454
    label "dzia&#322;a&#263;"
  ]
  node [
    id 455
    label "wrestle"
  ]
  node [
    id 456
    label "sprzeciwia&#263;_si&#281;"
  ]
  node [
    id 457
    label "my&#347;lenie"
  ]
  node [
    id 458
    label "argue"
  ]
  node [
    id 459
    label "stara&#263;_si&#281;"
  ]
  node [
    id 460
    label "fight"
  ]
  node [
    id 461
    label "opiera&#263;_si&#281;"
  ]
  node [
    id 462
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 463
    label "prze&#322;o&#380;y&#263;"
  ]
  node [
    id 464
    label "approach"
  ]
  node [
    id 465
    label "set_about"
  ]
  node [
    id 466
    label "zapozna&#263;"
  ]
  node [
    id 467
    label "heat"
  ]
  node [
    id 468
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 469
    label "zbi&#243;r"
  ]
  node [
    id 470
    label "problem"
  ]
  node [
    id 471
    label "molos_typu_g&#243;rskiego"
  ]
  node [
    id 472
    label "owczarek"
  ]
  node [
    id 473
    label "pies_policyjny"
  ]
  node [
    id 474
    label "pies_str&#243;&#380;uj&#261;cy"
  ]
  node [
    id 475
    label "usztywnienie"
  ]
  node [
    id 476
    label "napr&#281;&#380;enie"
  ]
  node [
    id 477
    label "striving"
  ]
  node [
    id 478
    label "nastr&#243;j"
  ]
  node [
    id 479
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 480
    label "tension"
  ]
  node [
    id 481
    label "nietutejszy"
  ]
  node [
    id 482
    label "Buriacja"
  ]
  node [
    id 483
    label "Abchazja"
  ]
  node [
    id 484
    label "Inguszetia"
  ]
  node [
    id 485
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 486
    label "Nachiczewan"
  ]
  node [
    id 487
    label "Karaka&#322;pacja"
  ]
  node [
    id 488
    label "Jakucja"
  ]
  node [
    id 489
    label "Singapur"
  ]
  node [
    id 490
    label "Komi"
  ]
  node [
    id 491
    label "Karelia"
  ]
  node [
    id 492
    label "Tatarstan"
  ]
  node [
    id 493
    label "Chakasja"
  ]
  node [
    id 494
    label "Dagestan"
  ]
  node [
    id 495
    label "Mordowia"
  ]
  node [
    id 496
    label "Ka&#322;mucja"
  ]
  node [
    id 497
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 498
    label "ustr&#243;j"
  ]
  node [
    id 499
    label "Republika_Cisalpi&#324;ska"
  ]
  node [
    id 500
    label "pa&#324;stwo_demokratyczne"
  ]
  node [
    id 501
    label "Mari_El"
  ]
  node [
    id 502
    label "Ad&#380;aria"
  ]
  node [
    id 503
    label "Czuwaszja"
  ]
  node [
    id 504
    label "Tuwa"
  ]
  node [
    id 505
    label "Czeczenia"
  ]
  node [
    id 506
    label "Udmurcja"
  ]
  node [
    id 507
    label "biuro"
  ]
  node [
    id 508
    label "polityczny"
  ]
  node [
    id 509
    label "sekretarz"
  ]
  node [
    id 510
    label "generalny"
  ]
  node [
    id 511
    label "KC"
  ]
  node [
    id 512
    label "PZPR"
  ]
  node [
    id 513
    label "Michai&#322;"
  ]
  node [
    id 514
    label "Gorbaczow"
  ]
  node [
    id 515
    label "stowarzyszy&#263;"
  ]
  node [
    id 516
    label "&#8222;"
  ]
  node [
    id 517
    label "&#8221;"
  ]
  node [
    id 518
    label "zwi&#261;zka"
  ]
  node [
    id 519
    label "poprze&#263;"
  ]
  node [
    id 520
    label "pierestrojka"
  ]
  node [
    id 521
    label "CzI"
  ]
  node [
    id 522
    label "ASRR"
  ]
  node [
    id 523
    label "front"
  ]
  node [
    id 524
    label "ludowy"
  ]
  node [
    id 525
    label "dok"
  ]
  node [
    id 526
    label "Zawgajew"
  ]
  node [
    id 527
    label "Czeczeno"
  ]
  node [
    id 528
    label "Inguski"
  ]
  node [
    id 529
    label "muzu&#322;ma&#324;ski"
  ]
  node [
    id 530
    label "zarz&#261;d"
  ]
  node [
    id 531
    label "duchowny"
  ]
  node [
    id 532
    label "inicjatywa"
  ]
  node [
    id 533
    label "demokratyczny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 10
    target 105
  ]
  edge [
    source 10
    target 106
  ]
  edge [
    source 10
    target 107
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 10
    target 110
  ]
  edge [
    source 10
    target 111
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 117
  ]
  edge [
    source 11
    target 118
  ]
  edge [
    source 11
    target 119
  ]
  edge [
    source 11
    target 120
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 121
  ]
  edge [
    source 12
    target 122
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 123
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 124
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 125
  ]
  edge [
    source 17
    target 126
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 127
  ]
  edge [
    source 18
    target 128
  ]
  edge [
    source 18
    target 129
  ]
  edge [
    source 18
    target 130
  ]
  edge [
    source 18
    target 131
  ]
  edge [
    source 18
    target 132
  ]
  edge [
    source 18
    target 133
  ]
  edge [
    source 18
    target 134
  ]
  edge [
    source 18
    target 135
  ]
  edge [
    source 18
    target 136
  ]
  edge [
    source 18
    target 137
  ]
  edge [
    source 18
    target 138
  ]
  edge [
    source 18
    target 139
  ]
  edge [
    source 18
    target 140
  ]
  edge [
    source 18
    target 141
  ]
  edge [
    source 18
    target 142
  ]
  edge [
    source 18
    target 143
  ]
  edge [
    source 18
    target 144
  ]
  edge [
    source 18
    target 145
  ]
  edge [
    source 18
    target 146
  ]
  edge [
    source 18
    target 147
  ]
  edge [
    source 18
    target 148
  ]
  edge [
    source 18
    target 149
  ]
  edge [
    source 18
    target 150
  ]
  edge [
    source 18
    target 151
  ]
  edge [
    source 18
    target 152
  ]
  edge [
    source 18
    target 153
  ]
  edge [
    source 18
    target 154
  ]
  edge [
    source 18
    target 155
  ]
  edge [
    source 18
    target 156
  ]
  edge [
    source 18
    target 157
  ]
  edge [
    source 18
    target 158
  ]
  edge [
    source 18
    target 159
  ]
  edge [
    source 18
    target 160
  ]
  edge [
    source 18
    target 161
  ]
  edge [
    source 18
    target 162
  ]
  edge [
    source 18
    target 163
  ]
  edge [
    source 18
    target 164
  ]
  edge [
    source 18
    target 165
  ]
  edge [
    source 18
    target 166
  ]
  edge [
    source 18
    target 167
  ]
  edge [
    source 18
    target 168
  ]
  edge [
    source 18
    target 169
  ]
  edge [
    source 18
    target 170
  ]
  edge [
    source 18
    target 171
  ]
  edge [
    source 18
    target 172
  ]
  edge [
    source 18
    target 173
  ]
  edge [
    source 18
    target 174
  ]
  edge [
    source 18
    target 175
  ]
  edge [
    source 18
    target 176
  ]
  edge [
    source 18
    target 177
  ]
  edge [
    source 18
    target 178
  ]
  edge [
    source 18
    target 179
  ]
  edge [
    source 18
    target 180
  ]
  edge [
    source 18
    target 181
  ]
  edge [
    source 18
    target 182
  ]
  edge [
    source 18
    target 183
  ]
  edge [
    source 18
    target 184
  ]
  edge [
    source 18
    target 185
  ]
  edge [
    source 18
    target 186
  ]
  edge [
    source 18
    target 187
  ]
  edge [
    source 18
    target 188
  ]
  edge [
    source 18
    target 189
  ]
  edge [
    source 18
    target 190
  ]
  edge [
    source 18
    target 191
  ]
  edge [
    source 18
    target 192
  ]
  edge [
    source 18
    target 193
  ]
  edge [
    source 18
    target 194
  ]
  edge [
    source 18
    target 195
  ]
  edge [
    source 18
    target 196
  ]
  edge [
    source 18
    target 197
  ]
  edge [
    source 18
    target 198
  ]
  edge [
    source 18
    target 199
  ]
  edge [
    source 18
    target 200
  ]
  edge [
    source 18
    target 201
  ]
  edge [
    source 18
    target 202
  ]
  edge [
    source 18
    target 203
  ]
  edge [
    source 18
    target 204
  ]
  edge [
    source 18
    target 205
  ]
  edge [
    source 18
    target 206
  ]
  edge [
    source 18
    target 207
  ]
  edge [
    source 18
    target 208
  ]
  edge [
    source 18
    target 209
  ]
  edge [
    source 18
    target 210
  ]
  edge [
    source 18
    target 211
  ]
  edge [
    source 18
    target 212
  ]
  edge [
    source 18
    target 213
  ]
  edge [
    source 18
    target 214
  ]
  edge [
    source 18
    target 215
  ]
  edge [
    source 18
    target 216
  ]
  edge [
    source 18
    target 217
  ]
  edge [
    source 18
    target 218
  ]
  edge [
    source 18
    target 219
  ]
  edge [
    source 18
    target 220
  ]
  edge [
    source 18
    target 221
  ]
  edge [
    source 18
    target 222
  ]
  edge [
    source 18
    target 223
  ]
  edge [
    source 18
    target 224
  ]
  edge [
    source 18
    target 225
  ]
  edge [
    source 18
    target 226
  ]
  edge [
    source 18
    target 227
  ]
  edge [
    source 18
    target 228
  ]
  edge [
    source 18
    target 229
  ]
  edge [
    source 18
    target 230
  ]
  edge [
    source 18
    target 231
  ]
  edge [
    source 18
    target 232
  ]
  edge [
    source 18
    target 233
  ]
  edge [
    source 18
    target 234
  ]
  edge [
    source 18
    target 235
  ]
  edge [
    source 18
    target 236
  ]
  edge [
    source 18
    target 237
  ]
  edge [
    source 18
    target 238
  ]
  edge [
    source 18
    target 239
  ]
  edge [
    source 18
    target 240
  ]
  edge [
    source 18
    target 241
  ]
  edge [
    source 18
    target 242
  ]
  edge [
    source 18
    target 243
  ]
  edge [
    source 18
    target 244
  ]
  edge [
    source 18
    target 245
  ]
  edge [
    source 18
    target 246
  ]
  edge [
    source 18
    target 247
  ]
  edge [
    source 18
    target 248
  ]
  edge [
    source 18
    target 249
  ]
  edge [
    source 18
    target 250
  ]
  edge [
    source 18
    target 251
  ]
  edge [
    source 18
    target 252
  ]
  edge [
    source 18
    target 253
  ]
  edge [
    source 18
    target 254
  ]
  edge [
    source 18
    target 255
  ]
  edge [
    source 18
    target 256
  ]
  edge [
    source 18
    target 257
  ]
  edge [
    source 18
    target 258
  ]
  edge [
    source 18
    target 259
  ]
  edge [
    source 18
    target 260
  ]
  edge [
    source 18
    target 261
  ]
  edge [
    source 18
    target 262
  ]
  edge [
    source 18
    target 263
  ]
  edge [
    source 18
    target 264
  ]
  edge [
    source 18
    target 265
  ]
  edge [
    source 18
    target 266
  ]
  edge [
    source 18
    target 267
  ]
  edge [
    source 18
    target 268
  ]
  edge [
    source 18
    target 269
  ]
  edge [
    source 18
    target 270
  ]
  edge [
    source 18
    target 271
  ]
  edge [
    source 18
    target 272
  ]
  edge [
    source 18
    target 273
  ]
  edge [
    source 18
    target 274
  ]
  edge [
    source 18
    target 275
  ]
  edge [
    source 18
    target 276
  ]
  edge [
    source 18
    target 277
  ]
  edge [
    source 18
    target 278
  ]
  edge [
    source 18
    target 279
  ]
  edge [
    source 18
    target 280
  ]
  edge [
    source 18
    target 281
  ]
  edge [
    source 18
    target 282
  ]
  edge [
    source 18
    target 283
  ]
  edge [
    source 18
    target 284
  ]
  edge [
    source 18
    target 285
  ]
  edge [
    source 18
    target 286
  ]
  edge [
    source 18
    target 287
  ]
  edge [
    source 18
    target 288
  ]
  edge [
    source 18
    target 289
  ]
  edge [
    source 18
    target 290
  ]
  edge [
    source 18
    target 291
  ]
  edge [
    source 18
    target 292
  ]
  edge [
    source 18
    target 293
  ]
  edge [
    source 18
    target 294
  ]
  edge [
    source 18
    target 295
  ]
  edge [
    source 18
    target 296
  ]
  edge [
    source 18
    target 297
  ]
  edge [
    source 18
    target 298
  ]
  edge [
    source 18
    target 299
  ]
  edge [
    source 18
    target 300
  ]
  edge [
    source 18
    target 301
  ]
  edge [
    source 18
    target 302
  ]
  edge [
    source 18
    target 303
  ]
  edge [
    source 18
    target 304
  ]
  edge [
    source 18
    target 305
  ]
  edge [
    source 18
    target 306
  ]
  edge [
    source 18
    target 307
  ]
  edge [
    source 18
    target 308
  ]
  edge [
    source 18
    target 309
  ]
  edge [
    source 18
    target 310
  ]
  edge [
    source 18
    target 311
  ]
  edge [
    source 18
    target 312
  ]
  edge [
    source 18
    target 313
  ]
  edge [
    source 18
    target 314
  ]
  edge [
    source 18
    target 315
  ]
  edge [
    source 18
    target 316
  ]
  edge [
    source 18
    target 317
  ]
  edge [
    source 18
    target 318
  ]
  edge [
    source 18
    target 319
  ]
  edge [
    source 18
    target 320
  ]
  edge [
    source 18
    target 321
  ]
  edge [
    source 18
    target 322
  ]
  edge [
    source 18
    target 323
  ]
  edge [
    source 18
    target 324
  ]
  edge [
    source 18
    target 325
  ]
  edge [
    source 18
    target 326
  ]
  edge [
    source 18
    target 327
  ]
  edge [
    source 18
    target 328
  ]
  edge [
    source 18
    target 329
  ]
  edge [
    source 18
    target 330
  ]
  edge [
    source 18
    target 331
  ]
  edge [
    source 18
    target 332
  ]
  edge [
    source 18
    target 333
  ]
  edge [
    source 18
    target 334
  ]
  edge [
    source 18
    target 335
  ]
  edge [
    source 18
    target 336
  ]
  edge [
    source 18
    target 337
  ]
  edge [
    source 18
    target 338
  ]
  edge [
    source 18
    target 339
  ]
  edge [
    source 18
    target 340
  ]
  edge [
    source 18
    target 341
  ]
  edge [
    source 18
    target 342
  ]
  edge [
    source 18
    target 343
  ]
  edge [
    source 18
    target 344
  ]
  edge [
    source 18
    target 345
  ]
  edge [
    source 18
    target 346
  ]
  edge [
    source 18
    target 347
  ]
  edge [
    source 18
    target 348
  ]
  edge [
    source 18
    target 349
  ]
  edge [
    source 18
    target 350
  ]
  edge [
    source 18
    target 351
  ]
  edge [
    source 18
    target 352
  ]
  edge [
    source 18
    target 353
  ]
  edge [
    source 18
    target 354
  ]
  edge [
    source 18
    target 355
  ]
  edge [
    source 18
    target 356
  ]
  edge [
    source 18
    target 357
  ]
  edge [
    source 18
    target 358
  ]
  edge [
    source 18
    target 359
  ]
  edge [
    source 18
    target 360
  ]
  edge [
    source 18
    target 361
  ]
  edge [
    source 18
    target 362
  ]
  edge [
    source 18
    target 363
  ]
  edge [
    source 18
    target 364
  ]
  edge [
    source 18
    target 365
  ]
  edge [
    source 18
    target 366
  ]
  edge [
    source 18
    target 367
  ]
  edge [
    source 18
    target 368
  ]
  edge [
    source 18
    target 369
  ]
  edge [
    source 18
    target 370
  ]
  edge [
    source 18
    target 371
  ]
  edge [
    source 18
    target 372
  ]
  edge [
    source 18
    target 373
  ]
  edge [
    source 18
    target 374
  ]
  edge [
    source 18
    target 375
  ]
  edge [
    source 18
    target 376
  ]
  edge [
    source 18
    target 377
  ]
  edge [
    source 18
    target 378
  ]
  edge [
    source 18
    target 379
  ]
  edge [
    source 18
    target 380
  ]
  edge [
    source 18
    target 381
  ]
  edge [
    source 18
    target 382
  ]
  edge [
    source 18
    target 383
  ]
  edge [
    source 18
    target 384
  ]
  edge [
    source 18
    target 385
  ]
  edge [
    source 18
    target 386
  ]
  edge [
    source 18
    target 387
  ]
  edge [
    source 18
    target 388
  ]
  edge [
    source 18
    target 389
  ]
  edge [
    source 18
    target 390
  ]
  edge [
    source 18
    target 391
  ]
  edge [
    source 18
    target 392
  ]
  edge [
    source 18
    target 393
  ]
  edge [
    source 18
    target 394
  ]
  edge [
    source 18
    target 395
  ]
  edge [
    source 18
    target 396
  ]
  edge [
    source 18
    target 397
  ]
  edge [
    source 18
    target 398
  ]
  edge [
    source 18
    target 399
  ]
  edge [
    source 18
    target 400
  ]
  edge [
    source 18
    target 401
  ]
  edge [
    source 18
    target 402
  ]
  edge [
    source 18
    target 403
  ]
  edge [
    source 18
    target 404
  ]
  edge [
    source 18
    target 405
  ]
  edge [
    source 18
    target 406
  ]
  edge [
    source 18
    target 407
  ]
  edge [
    source 18
    target 408
  ]
  edge [
    source 18
    target 409
  ]
  edge [
    source 18
    target 410
  ]
  edge [
    source 18
    target 411
  ]
  edge [
    source 18
    target 412
  ]
  edge [
    source 18
    target 413
  ]
  edge [
    source 18
    target 414
  ]
  edge [
    source 18
    target 415
  ]
  edge [
    source 18
    target 416
  ]
  edge [
    source 18
    target 417
  ]
  edge [
    source 18
    target 418
  ]
  edge [
    source 18
    target 419
  ]
  edge [
    source 18
    target 420
  ]
  edge [
    source 18
    target 421
  ]
  edge [
    source 18
    target 422
  ]
  edge [
    source 18
    target 423
  ]
  edge [
    source 18
    target 424
  ]
  edge [
    source 18
    target 425
  ]
  edge [
    source 18
    target 426
  ]
  edge [
    source 18
    target 427
  ]
  edge [
    source 18
    target 428
  ]
  edge [
    source 18
    target 429
  ]
  edge [
    source 18
    target 430
  ]
  edge [
    source 18
    target 431
  ]
  edge [
    source 18
    target 432
  ]
  edge [
    source 18
    target 433
  ]
  edge [
    source 18
    target 434
  ]
  edge [
    source 18
    target 435
  ]
  edge [
    source 18
    target 436
  ]
  edge [
    source 18
    target 437
  ]
  edge [
    source 18
    target 438
  ]
  edge [
    source 18
    target 439
  ]
  edge [
    source 18
    target 440
  ]
  edge [
    source 18
    target 441
  ]
  edge [
    source 18
    target 442
  ]
  edge [
    source 18
    target 443
  ]
  edge [
    source 18
    target 444
  ]
  edge [
    source 18
    target 445
  ]
  edge [
    source 18
    target 446
  ]
  edge [
    source 18
    target 447
  ]
  edge [
    source 18
    target 448
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 449
  ]
  edge [
    source 21
    target 450
  ]
  edge [
    source 21
    target 451
  ]
  edge [
    source 21
    target 452
  ]
  edge [
    source 21
    target 453
  ]
  edge [
    source 21
    target 454
  ]
  edge [
    source 21
    target 455
  ]
  edge [
    source 21
    target 456
  ]
  edge [
    source 21
    target 40
  ]
  edge [
    source 21
    target 457
  ]
  edge [
    source 21
    target 458
  ]
  edge [
    source 21
    target 459
  ]
  edge [
    source 21
    target 460
  ]
  edge [
    source 21
    target 461
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 462
  ]
  edge [
    source 26
    target 463
  ]
  edge [
    source 26
    target 464
  ]
  edge [
    source 26
    target 465
  ]
  edge [
    source 26
    target 466
  ]
  edge [
    source 26
    target 467
  ]
  edge [
    source 26
    target 468
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 469
  ]
  edge [
    source 27
    target 470
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 471
  ]
  edge [
    source 28
    target 472
  ]
  edge [
    source 28
    target 473
  ]
  edge [
    source 28
    target 474
  ]
  edge [
    source 28
    target 515
  ]
  edge [
    source 28
    target 516
  ]
  edge [
    source 28
    target 517
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 475
  ]
  edge [
    source 29
    target 476
  ]
  edge [
    source 29
    target 477
  ]
  edge [
    source 29
    target 478
  ]
  edge [
    source 29
    target 479
  ]
  edge [
    source 29
    target 480
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 481
  ]
  edge [
    source 31
    target 482
  ]
  edge [
    source 31
    target 483
  ]
  edge [
    source 31
    target 484
  ]
  edge [
    source 31
    target 485
  ]
  edge [
    source 31
    target 486
  ]
  edge [
    source 31
    target 487
  ]
  edge [
    source 31
    target 488
  ]
  edge [
    source 31
    target 489
  ]
  edge [
    source 31
    target 490
  ]
  edge [
    source 31
    target 491
  ]
  edge [
    source 31
    target 492
  ]
  edge [
    source 31
    target 493
  ]
  edge [
    source 31
    target 494
  ]
  edge [
    source 31
    target 495
  ]
  edge [
    source 31
    target 496
  ]
  edge [
    source 31
    target 497
  ]
  edge [
    source 31
    target 498
  ]
  edge [
    source 31
    target 499
  ]
  edge [
    source 31
    target 429
  ]
  edge [
    source 31
    target 500
  ]
  edge [
    source 31
    target 501
  ]
  edge [
    source 31
    target 502
  ]
  edge [
    source 31
    target 503
  ]
  edge [
    source 31
    target 504
  ]
  edge [
    source 31
    target 505
  ]
  edge [
    source 31
    target 506
  ]
  edge [
    source 31
    target 90
  ]
  edge [
    source 507
    target 508
  ]
  edge [
    source 509
    target 510
  ]
  edge [
    source 509
    target 511
  ]
  edge [
    source 509
    target 512
  ]
  edge [
    source 510
    target 511
  ]
  edge [
    source 510
    target 512
  ]
  edge [
    source 511
    target 512
  ]
  edge [
    source 513
    target 514
  ]
  edge [
    source 515
    target 516
  ]
  edge [
    source 515
    target 517
  ]
  edge [
    source 516
    target 517
  ]
  edge [
    source 518
    target 519
  ]
  edge [
    source 518
    target 520
  ]
  edge [
    source 519
    target 520
  ]
  edge [
    source 521
    target 522
  ]
  edge [
    source 521
    target 523
  ]
  edge [
    source 521
    target 524
  ]
  edge [
    source 522
    target 523
  ]
  edge [
    source 522
    target 524
  ]
  edge [
    source 523
    target 524
  ]
  edge [
    source 525
    target 526
  ]
  edge [
    source 527
    target 528
  ]
  edge [
    source 527
    target 529
  ]
  edge [
    source 527
    target 530
  ]
  edge [
    source 527
    target 531
  ]
  edge [
    source 528
    target 529
  ]
  edge [
    source 528
    target 530
  ]
  edge [
    source 528
    target 531
  ]
  edge [
    source 529
    target 530
  ]
  edge [
    source 529
    target 531
  ]
  edge [
    source 530
    target 531
  ]
  edge [
    source 532
    target 533
  ]
]
