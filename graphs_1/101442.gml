graph [
  maxDegree 1
  minDegree 1
  meanDegree 1
  density 0.1111111111111111
  graphCliqueNumber 2
  node [
    id 0
    label "predrag"
    origin "text"
  ]
  node [
    id 1
    label "pavlovi&#263;"
    origin "text"
  ]
  node [
    id 2
    label "Predrag"
  ]
  node [
    id 3
    label "Pavlovi&#263;"
  ]
  node [
    id 4
    label "&#1055;&#1088;&#1077;&#1076;&#1088;&#1072;&#1075;"
  ]
  node [
    id 5
    label "&#1055;&#1072;&#1074;&#1083;&#1086;&#1074;&#1080;&#1115;"
  ]
  node [
    id 6
    label "Napredaku"
  ]
  node [
    id 7
    label "Kru&#353;evac"
  ]
  node [
    id 8
    label "igrzysko"
  ]
  node [
    id 9
    label "olimpijski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 8
    target 9
  ]
]
