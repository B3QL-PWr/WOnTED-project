graph [
  maxDegree 15
  minDegree 1
  meanDegree 1.9166666666666667
  density 0.08333333333333333
  graphCliqueNumber 2
  node [
    id 0
    label "sos"
    origin "text"
  ]
  node [
    id 1
    label "spaghetti"
    origin "text"
  ]
  node [
    id 2
    label "r&#243;&#380;norodno&#347;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "spo&#322;ecze&#324;stwo"
    origin "text"
  ]
  node [
    id 4
    label "mieszanina"
  ]
  node [
    id 5
    label "jedzenie"
  ]
  node [
    id 6
    label "gulasz"
  ]
  node [
    id 7
    label "zaklepka"
  ]
  node [
    id 8
    label "pasta"
  ]
  node [
    id 9
    label "zr&#243;&#380;nicowanie"
  ]
  node [
    id 10
    label "pole"
  ]
  node [
    id 11
    label "kastowo&#347;&#263;"
  ]
  node [
    id 12
    label "ludzie_pracy"
  ]
  node [
    id 13
    label "community"
  ]
  node [
    id 14
    label "status"
  ]
  node [
    id 15
    label "cywilizacja"
  ]
  node [
    id 16
    label "pozaklasowy"
  ]
  node [
    id 17
    label "aspo&#322;eczny"
  ]
  node [
    id 18
    label "uwarstwienie"
  ]
  node [
    id 19
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 20
    label "elita"
  ]
  node [
    id 21
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 22
    label "wspo&#322;ecze&#324;stwo"
  ]
  node [
    id 23
    label "klasa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
]
