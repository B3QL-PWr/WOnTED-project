graph [
  maxDegree 42
  minDegree 1
  meanDegree 2
  density 0.02702702702702703
  graphCliqueNumber 2
  node [
    id 0
    label "wjazd"
    origin "text"
  ]
  node [
    id 1
    label "p&#243;&#378;ny"
    origin "text"
  ]
  node [
    id 2
    label "pomara&#324;czowy"
    origin "text"
  ]
  node [
    id 3
    label "zako&#324;czony"
    origin "text"
  ]
  node [
    id 4
    label "ucieczka"
    origin "text"
  ]
  node [
    id 5
    label "przej&#347;cie"
    origin "text"
  ]
  node [
    id 6
    label "dla"
    origin "text"
  ]
  node [
    id 7
    label "pieszy"
    origin "text"
  ]
  node [
    id 8
    label "dost&#281;p"
  ]
  node [
    id 9
    label "zamek"
  ]
  node [
    id 10
    label "budowa"
  ]
  node [
    id 11
    label "droga"
  ]
  node [
    id 12
    label "antaba"
  ]
  node [
    id 13
    label "wydarzenie"
  ]
  node [
    id 14
    label "wej&#347;cie"
  ]
  node [
    id 15
    label "zawiasy"
  ]
  node [
    id 16
    label "ogrodzenie"
  ]
  node [
    id 17
    label "wrzeci&#261;dz"
  ]
  node [
    id 18
    label "p&#243;&#378;no"
  ]
  node [
    id 19
    label "do_p&#243;&#378;na"
  ]
  node [
    id 20
    label "pomara&#324;czowo"
  ]
  node [
    id 21
    label "owocowy"
  ]
  node [
    id 22
    label "ciep&#322;y"
  ]
  node [
    id 23
    label "cytrusowy"
  ]
  node [
    id 24
    label "czyn"
  ]
  node [
    id 25
    label "czynno&#347;&#263;"
  ]
  node [
    id 26
    label "withdrawal"
  ]
  node [
    id 27
    label "apostasy"
  ]
  node [
    id 28
    label "postawa"
  ]
  node [
    id 29
    label "nale&#380;enie"
  ]
  node [
    id 30
    label "odmienienie"
  ]
  node [
    id 31
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 32
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 33
    label "mini&#281;cie"
  ]
  node [
    id 34
    label "prze&#380;ycie"
  ]
  node [
    id 35
    label "strain"
  ]
  node [
    id 36
    label "przerobienie"
  ]
  node [
    id 37
    label "stanie_si&#281;"
  ]
  node [
    id 38
    label "dostanie_si&#281;"
  ]
  node [
    id 39
    label "wydeptanie"
  ]
  node [
    id 40
    label "wydeptywanie"
  ]
  node [
    id 41
    label "offense"
  ]
  node [
    id 42
    label "wymienienie"
  ]
  node [
    id 43
    label "zacz&#281;cie"
  ]
  node [
    id 44
    label "trwanie"
  ]
  node [
    id 45
    label "przepojenie"
  ]
  node [
    id 46
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 47
    label "zaliczenie"
  ]
  node [
    id 48
    label "zdarzenie_si&#281;"
  ]
  node [
    id 49
    label "uznanie"
  ]
  node [
    id 50
    label "nasycenie_si&#281;"
  ]
  node [
    id 51
    label "przemokni&#281;cie"
  ]
  node [
    id 52
    label "nas&#261;czenie"
  ]
  node [
    id 53
    label "mienie"
  ]
  node [
    id 54
    label "ustawa"
  ]
  node [
    id 55
    label "experience"
  ]
  node [
    id 56
    label "przewy&#380;szenie"
  ]
  node [
    id 57
    label "miejsce"
  ]
  node [
    id 58
    label "faza"
  ]
  node [
    id 59
    label "doznanie"
  ]
  node [
    id 60
    label "przestanie"
  ]
  node [
    id 61
    label "traversal"
  ]
  node [
    id 62
    label "przebycie"
  ]
  node [
    id 63
    label "przedostanie_si&#281;"
  ]
  node [
    id 64
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 65
    label "wstawka"
  ]
  node [
    id 66
    label "przepuszczenie"
  ]
  node [
    id 67
    label "wytyczenie"
  ]
  node [
    id 68
    label "crack"
  ]
  node [
    id 69
    label "specjalny"
  ]
  node [
    id 70
    label "cz&#322;owiek"
  ]
  node [
    id 71
    label "pieszo"
  ]
  node [
    id 72
    label "piechotny"
  ]
  node [
    id 73
    label "w&#281;drowiec"
  ]
  node [
    id 74
    label "chodnik"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
]
