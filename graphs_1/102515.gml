graph [
  maxDegree 46
  minDegree 1
  meanDegree 2.1266666666666665
  density 0.007112597547380156
  graphCliqueNumber 3
  node [
    id 0
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 1
    label "marco"
    origin "text"
  ]
  node [
    id 2
    label "pi&#261;tek"
    origin "text"
  ]
  node [
    id 3
    label "sopocki"
    origin "text"
  ]
  node [
    id 4
    label "wydzia&#322;"
    origin "text"
  ]
  node [
    id 5
    label "szko&#322;a"
    origin "text"
  ]
  node [
    id 6
    label "wysoki"
    origin "text"
  ]
  node [
    id 7
    label "psychologia"
    origin "text"
  ]
  node [
    id 8
    label "spo&#322;eczny"
    origin "text"
  ]
  node [
    id 9
    label "by&#263;"
    origin "text"
  ]
  node [
    id 10
    label "mie&#263;"
    origin "text"
  ]
  node [
    id 11
    label "miejsce"
    origin "text"
  ]
  node [
    id 12
    label "wyj&#261;tkowy"
    origin "text"
  ]
  node [
    id 13
    label "wydarzenie"
    origin "text"
  ]
  node [
    id 14
    label "naukowy"
    origin "text"
  ]
  node [
    id 15
    label "m&#243;zg"
    origin "text"
  ]
  node [
    id 16
    label "pierwsza"
    origin "text"
  ]
  node [
    id 17
    label "typ"
    origin "text"
  ]
  node [
    id 18
    label "inicjatywa"
    origin "text"
  ]
  node [
    id 19
    label "tr&#243;jmiasto"
    origin "text"
  ]
  node [
    id 20
    label "rama"
    origin "text"
  ]
  node [
    id 21
    label "&#347;wiatowy"
    origin "text"
  ]
  node [
    id 22
    label "tydzie&#324;"
    origin "text"
  ]
  node [
    id 23
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 24
    label "corocznie"
    origin "text"
  ]
  node [
    id 25
    label "organizowa&#263;"
    origin "text"
  ]
  node [
    id 26
    label "przez"
    origin "text"
  ]
  node [
    id 27
    label "fundacja"
    origin "text"
  ]
  node [
    id 28
    label "dan"
    origin "text"
  ]
  node [
    id 29
    label "s&#322;o&#324;ce"
  ]
  node [
    id 30
    label "czynienie_si&#281;"
  ]
  node [
    id 31
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 32
    label "czas"
  ]
  node [
    id 33
    label "long_time"
  ]
  node [
    id 34
    label "przedpo&#322;udnie"
  ]
  node [
    id 35
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 36
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 37
    label "godzina"
  ]
  node [
    id 38
    label "t&#322;usty_czwartek"
  ]
  node [
    id 39
    label "wsta&#263;"
  ]
  node [
    id 40
    label "day"
  ]
  node [
    id 41
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 42
    label "przedwiecz&#243;r"
  ]
  node [
    id 43
    label "Sylwester"
  ]
  node [
    id 44
    label "po&#322;udnie"
  ]
  node [
    id 45
    label "wzej&#347;cie"
  ]
  node [
    id 46
    label "podwiecz&#243;r"
  ]
  node [
    id 47
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 48
    label "rano"
  ]
  node [
    id 49
    label "termin"
  ]
  node [
    id 50
    label "ranek"
  ]
  node [
    id 51
    label "doba"
  ]
  node [
    id 52
    label "wiecz&#243;r"
  ]
  node [
    id 53
    label "walentynki"
  ]
  node [
    id 54
    label "popo&#322;udnie"
  ]
  node [
    id 55
    label "noc"
  ]
  node [
    id 56
    label "wstanie"
  ]
  node [
    id 57
    label "dzie&#324;_powszedni"
  ]
  node [
    id 58
    label "Wielki_Pi&#261;tek"
  ]
  node [
    id 59
    label "podsekcja"
  ]
  node [
    id 60
    label "whole"
  ]
  node [
    id 61
    label "relation"
  ]
  node [
    id 62
    label "politechnika"
  ]
  node [
    id 63
    label "katedra"
  ]
  node [
    id 64
    label "urz&#261;d"
  ]
  node [
    id 65
    label "uniwersytet"
  ]
  node [
    id 66
    label "jednostka_organizacyjna"
  ]
  node [
    id 67
    label "insourcing"
  ]
  node [
    id 68
    label "ministerstwo"
  ]
  node [
    id 69
    label "miejsce_pracy"
  ]
  node [
    id 70
    label "dzia&#322;"
  ]
  node [
    id 71
    label "Mickiewicz"
  ]
  node [
    id 72
    label "szkolenie"
  ]
  node [
    id 73
    label "przepisa&#263;"
  ]
  node [
    id 74
    label "lesson"
  ]
  node [
    id 75
    label "grupa"
  ]
  node [
    id 76
    label "praktyka"
  ]
  node [
    id 77
    label "metoda"
  ]
  node [
    id 78
    label "niepokalanki"
  ]
  node [
    id 79
    label "kara"
  ]
  node [
    id 80
    label "zda&#263;"
  ]
  node [
    id 81
    label "form"
  ]
  node [
    id 82
    label "kwalifikacje"
  ]
  node [
    id 83
    label "system"
  ]
  node [
    id 84
    label "przepisanie"
  ]
  node [
    id 85
    label "sztuba"
  ]
  node [
    id 86
    label "wiedza"
  ]
  node [
    id 87
    label "stopek"
  ]
  node [
    id 88
    label "school"
  ]
  node [
    id 89
    label "absolwent"
  ]
  node [
    id 90
    label "urszulanki"
  ]
  node [
    id 91
    label "gabinet"
  ]
  node [
    id 92
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 93
    label "ideologia"
  ]
  node [
    id 94
    label "lekcja"
  ]
  node [
    id 95
    label "muzyka"
  ]
  node [
    id 96
    label "podr&#281;cznik"
  ]
  node [
    id 97
    label "zdanie"
  ]
  node [
    id 98
    label "siedziba"
  ]
  node [
    id 99
    label "sekretariat"
  ]
  node [
    id 100
    label "nauka"
  ]
  node [
    id 101
    label "do&#347;wiadczenie"
  ]
  node [
    id 102
    label "tablica"
  ]
  node [
    id 103
    label "teren_szko&#322;y"
  ]
  node [
    id 104
    label "instytucja"
  ]
  node [
    id 105
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 106
    label "skolaryzacja"
  ]
  node [
    id 107
    label "&#322;awa_szkolna"
  ]
  node [
    id 108
    label "klasa"
  ]
  node [
    id 109
    label "warto&#347;ciowy"
  ]
  node [
    id 110
    label "du&#380;y"
  ]
  node [
    id 111
    label "wysoce"
  ]
  node [
    id 112
    label "daleki"
  ]
  node [
    id 113
    label "znaczny"
  ]
  node [
    id 114
    label "wysoko"
  ]
  node [
    id 115
    label "szczytnie"
  ]
  node [
    id 116
    label "wznios&#322;y"
  ]
  node [
    id 117
    label "wyrafinowany"
  ]
  node [
    id 118
    label "z_wysoka"
  ]
  node [
    id 119
    label "chwalebny"
  ]
  node [
    id 120
    label "uprzywilejowany"
  ]
  node [
    id 121
    label "niepo&#347;ledni"
  ]
  node [
    id 122
    label "psychologia_analityczna"
  ]
  node [
    id 123
    label "psychobiologia"
  ]
  node [
    id 124
    label "gestaltyzm"
  ]
  node [
    id 125
    label "tyflopsychologia"
  ]
  node [
    id 126
    label "psychosocjologia"
  ]
  node [
    id 127
    label "psychometria"
  ]
  node [
    id 128
    label "hipnotyzm"
  ]
  node [
    id 129
    label "cyberpsychologia"
  ]
  node [
    id 130
    label "etnopsychologia"
  ]
  node [
    id 131
    label "psycholingwistyka"
  ]
  node [
    id 132
    label "aromachologia"
  ]
  node [
    id 133
    label "socjopsychologia"
  ]
  node [
    id 134
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 135
    label "psychologia_teoretyczna"
  ]
  node [
    id 136
    label "neuropsychologia"
  ]
  node [
    id 137
    label "biopsychologia"
  ]
  node [
    id 138
    label "psychologia_muzyki"
  ]
  node [
    id 139
    label "psychologia_systemowa"
  ]
  node [
    id 140
    label "psychofizyka"
  ]
  node [
    id 141
    label "wn&#281;trze"
  ]
  node [
    id 142
    label "psychologia_humanistyczna"
  ]
  node [
    id 143
    label "zoopsychologia"
  ]
  node [
    id 144
    label "cecha"
  ]
  node [
    id 145
    label "wizja-logika"
  ]
  node [
    id 146
    label "psychologia_s&#261;dowa"
  ]
  node [
    id 147
    label "psychologia_stosowana"
  ]
  node [
    id 148
    label "chronopsychologia"
  ]
  node [
    id 149
    label "psychology"
  ]
  node [
    id 150
    label "interakcjonizm"
  ]
  node [
    id 151
    label "psychologia_religii"
  ]
  node [
    id 152
    label "psychologia_pastoralna"
  ]
  node [
    id 153
    label "charakterologia"
  ]
  node [
    id 154
    label "psychologia_ewolucyjna"
  ]
  node [
    id 155
    label "psychologia_s&#322;uchu"
  ]
  node [
    id 156
    label "psychologia_&#347;rodowiskowa"
  ]
  node [
    id 157
    label "psychologia_pozytywna"
  ]
  node [
    id 158
    label "psychologia_zdrowia"
  ]
  node [
    id 159
    label "grafologia"
  ]
  node [
    id 160
    label "artefakt"
  ]
  node [
    id 161
    label "psychotanatologia"
  ]
  node [
    id 162
    label "psychotechnika"
  ]
  node [
    id 163
    label "asocjacjonizm"
  ]
  node [
    id 164
    label "niepubliczny"
  ]
  node [
    id 165
    label "spo&#322;ecznie"
  ]
  node [
    id 166
    label "publiczny"
  ]
  node [
    id 167
    label "si&#281;ga&#263;"
  ]
  node [
    id 168
    label "trwa&#263;"
  ]
  node [
    id 169
    label "obecno&#347;&#263;"
  ]
  node [
    id 170
    label "stan"
  ]
  node [
    id 171
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 172
    label "stand"
  ]
  node [
    id 173
    label "mie&#263;_miejsce"
  ]
  node [
    id 174
    label "uczestniczy&#263;"
  ]
  node [
    id 175
    label "chodzi&#263;"
  ]
  node [
    id 176
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 177
    label "equal"
  ]
  node [
    id 178
    label "czu&#263;"
  ]
  node [
    id 179
    label "need"
  ]
  node [
    id 180
    label "hide"
  ]
  node [
    id 181
    label "support"
  ]
  node [
    id 182
    label "cia&#322;o"
  ]
  node [
    id 183
    label "plac"
  ]
  node [
    id 184
    label "uwaga"
  ]
  node [
    id 185
    label "przestrze&#324;"
  ]
  node [
    id 186
    label "status"
  ]
  node [
    id 187
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 188
    label "chwila"
  ]
  node [
    id 189
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 190
    label "rz&#261;d"
  ]
  node [
    id 191
    label "praca"
  ]
  node [
    id 192
    label "location"
  ]
  node [
    id 193
    label "warunek_lokalowy"
  ]
  node [
    id 194
    label "wyj&#261;tkowo"
  ]
  node [
    id 195
    label "inny"
  ]
  node [
    id 196
    label "czynno&#347;&#263;"
  ]
  node [
    id 197
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 198
    label "motyw"
  ]
  node [
    id 199
    label "fabu&#322;a"
  ]
  node [
    id 200
    label "przebiec"
  ]
  node [
    id 201
    label "przebiegni&#281;cie"
  ]
  node [
    id 202
    label "charakter"
  ]
  node [
    id 203
    label "specjalny"
  ]
  node [
    id 204
    label "edukacyjnie"
  ]
  node [
    id 205
    label "intelektualny"
  ]
  node [
    id 206
    label "skomplikowany"
  ]
  node [
    id 207
    label "zgodny"
  ]
  node [
    id 208
    label "naukowo"
  ]
  node [
    id 209
    label "scjentyficzny"
  ]
  node [
    id 210
    label "teoretyczny"
  ]
  node [
    id 211
    label "specjalistyczny"
  ]
  node [
    id 212
    label "g&#322;adkom&#243;zgowie"
  ]
  node [
    id 213
    label "elektroencefalogram"
  ]
  node [
    id 214
    label "substancja_szara"
  ]
  node [
    id 215
    label "przodom&#243;zgowie"
  ]
  node [
    id 216
    label "bruzda"
  ]
  node [
    id 217
    label "&#347;r&#243;dm&#243;zgowie"
  ]
  node [
    id 218
    label "wzg&#243;rze"
  ]
  node [
    id 219
    label "umys&#322;"
  ]
  node [
    id 220
    label "zw&#243;j"
  ]
  node [
    id 221
    label "kresom&#243;zgowie"
  ]
  node [
    id 222
    label "uk&#322;ad_limbiczny"
  ]
  node [
    id 223
    label "cia&#322;o_migda&#322;owate"
  ]
  node [
    id 224
    label "pie&#324;_m&#243;zgu"
  ]
  node [
    id 225
    label "przysadka"
  ]
  node [
    id 226
    label "bezzakr&#281;towo&#347;&#263;"
  ]
  node [
    id 227
    label "przedmurze"
  ]
  node [
    id 228
    label "ty&#322;om&#243;zgowie"
  ]
  node [
    id 229
    label "projektodawca"
  ]
  node [
    id 230
    label "noosfera"
  ]
  node [
    id 231
    label "g&#322;owa"
  ]
  node [
    id 232
    label "organ"
  ]
  node [
    id 233
    label "uk&#322;ad_nerwowy"
  ]
  node [
    id 234
    label "most"
  ]
  node [
    id 235
    label "przepuklina_m&#243;zgowa"
  ]
  node [
    id 236
    label "splot_naczyni&#243;wkowy"
  ]
  node [
    id 237
    label "encefalografia"
  ]
  node [
    id 238
    label "mi&#281;dzym&#243;zgowie"
  ]
  node [
    id 239
    label "m&#243;&#380;d&#380;ek"
  ]
  node [
    id 240
    label "kora_m&#243;zgowa"
  ]
  node [
    id 241
    label "podwzg&#243;rze"
  ]
  node [
    id 242
    label "poduszka"
  ]
  node [
    id 243
    label "cz&#322;owiek"
  ]
  node [
    id 244
    label "gromada"
  ]
  node [
    id 245
    label "autorament"
  ]
  node [
    id 246
    label "przypuszczenie"
  ]
  node [
    id 247
    label "cynk"
  ]
  node [
    id 248
    label "rezultat"
  ]
  node [
    id 249
    label "jednostka_systematyczna"
  ]
  node [
    id 250
    label "kr&#243;lestwo"
  ]
  node [
    id 251
    label "obstawia&#263;"
  ]
  node [
    id 252
    label "design"
  ]
  node [
    id 253
    label "facet"
  ]
  node [
    id 254
    label "variety"
  ]
  node [
    id 255
    label "sztuka"
  ]
  node [
    id 256
    label "antycypacja"
  ]
  node [
    id 257
    label "plan"
  ]
  node [
    id 258
    label "propozycja"
  ]
  node [
    id 259
    label "samodzielno&#347;&#263;"
  ]
  node [
    id 260
    label "zakres"
  ]
  node [
    id 261
    label "dodatek"
  ]
  node [
    id 262
    label "struktura"
  ]
  node [
    id 263
    label "stela&#380;"
  ]
  node [
    id 264
    label "za&#322;o&#380;enie"
  ]
  node [
    id 265
    label "human_body"
  ]
  node [
    id 266
    label "szablon"
  ]
  node [
    id 267
    label "oprawa"
  ]
  node [
    id 268
    label "paczka"
  ]
  node [
    id 269
    label "obramowanie"
  ]
  node [
    id 270
    label "pojazd"
  ]
  node [
    id 271
    label "postawa"
  ]
  node [
    id 272
    label "element_konstrukcyjny"
  ]
  node [
    id 273
    label "kulturalny"
  ]
  node [
    id 274
    label "&#347;wiatowo"
  ]
  node [
    id 275
    label "generalny"
  ]
  node [
    id 276
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 277
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 278
    label "weekend"
  ]
  node [
    id 279
    label "miesi&#261;c"
  ]
  node [
    id 280
    label "coroczny"
  ]
  node [
    id 281
    label "cyklicznie"
  ]
  node [
    id 282
    label "rocznie"
  ]
  node [
    id 283
    label "planowa&#263;"
  ]
  node [
    id 284
    label "dostosowywa&#263;"
  ]
  node [
    id 285
    label "pozyskiwa&#263;"
  ]
  node [
    id 286
    label "wprowadza&#263;"
  ]
  node [
    id 287
    label "treat"
  ]
  node [
    id 288
    label "przygotowywa&#263;"
  ]
  node [
    id 289
    label "create"
  ]
  node [
    id 290
    label "ensnare"
  ]
  node [
    id 291
    label "tworzy&#263;"
  ]
  node [
    id 292
    label "standard"
  ]
  node [
    id 293
    label "skupia&#263;"
  ]
  node [
    id 294
    label "foundation"
  ]
  node [
    id 295
    label "dar"
  ]
  node [
    id 296
    label "darowizna"
  ]
  node [
    id 297
    label "pocz&#261;tek"
  ]
  node [
    id 298
    label "wiek"
  ]
  node [
    id 299
    label "paleocen"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 122
  ]
  edge [
    source 7
    target 123
  ]
  edge [
    source 7
    target 124
  ]
  edge [
    source 7
    target 125
  ]
  edge [
    source 7
    target 126
  ]
  edge [
    source 7
    target 127
  ]
  edge [
    source 7
    target 128
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 7
    target 130
  ]
  edge [
    source 7
    target 131
  ]
  edge [
    source 7
    target 132
  ]
  edge [
    source 7
    target 133
  ]
  edge [
    source 7
    target 134
  ]
  edge [
    source 7
    target 135
  ]
  edge [
    source 7
    target 136
  ]
  edge [
    source 7
    target 137
  ]
  edge [
    source 7
    target 138
  ]
  edge [
    source 7
    target 139
  ]
  edge [
    source 7
    target 140
  ]
  edge [
    source 7
    target 141
  ]
  edge [
    source 7
    target 142
  ]
  edge [
    source 7
    target 143
  ]
  edge [
    source 7
    target 144
  ]
  edge [
    source 7
    target 145
  ]
  edge [
    source 7
    target 146
  ]
  edge [
    source 7
    target 147
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 148
  ]
  edge [
    source 7
    target 149
  ]
  edge [
    source 7
    target 150
  ]
  edge [
    source 7
    target 151
  ]
  edge [
    source 7
    target 152
  ]
  edge [
    source 7
    target 153
  ]
  edge [
    source 7
    target 154
  ]
  edge [
    source 7
    target 155
  ]
  edge [
    source 7
    target 156
  ]
  edge [
    source 7
    target 157
  ]
  edge [
    source 7
    target 158
  ]
  edge [
    source 7
    target 159
  ]
  edge [
    source 7
    target 160
  ]
  edge [
    source 7
    target 161
  ]
  edge [
    source 7
    target 162
  ]
  edge [
    source 7
    target 163
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 164
  ]
  edge [
    source 8
    target 165
  ]
  edge [
    source 8
    target 166
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 15
  ]
  edge [
    source 9
    target 16
  ]
  edge [
    source 9
    target 24
  ]
  edge [
    source 9
    target 25
  ]
  edge [
    source 9
    target 167
  ]
  edge [
    source 9
    target 168
  ]
  edge [
    source 9
    target 169
  ]
  edge [
    source 9
    target 170
  ]
  edge [
    source 9
    target 171
  ]
  edge [
    source 9
    target 172
  ]
  edge [
    source 9
    target 173
  ]
  edge [
    source 9
    target 174
  ]
  edge [
    source 9
    target 175
  ]
  edge [
    source 9
    target 176
  ]
  edge [
    source 9
    target 177
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 171
  ]
  edge [
    source 10
    target 178
  ]
  edge [
    source 10
    target 179
  ]
  edge [
    source 10
    target 180
  ]
  edge [
    source 10
    target 181
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 182
  ]
  edge [
    source 11
    target 183
  ]
  edge [
    source 11
    target 144
  ]
  edge [
    source 11
    target 184
  ]
  edge [
    source 11
    target 185
  ]
  edge [
    source 11
    target 186
  ]
  edge [
    source 11
    target 187
  ]
  edge [
    source 11
    target 188
  ]
  edge [
    source 11
    target 189
  ]
  edge [
    source 11
    target 190
  ]
  edge [
    source 11
    target 191
  ]
  edge [
    source 11
    target 192
  ]
  edge [
    source 11
    target 193
  ]
  edge [
    source 11
    target 19
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 194
  ]
  edge [
    source 12
    target 195
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 196
  ]
  edge [
    source 13
    target 197
  ]
  edge [
    source 13
    target 198
  ]
  edge [
    source 13
    target 199
  ]
  edge [
    source 13
    target 200
  ]
  edge [
    source 13
    target 201
  ]
  edge [
    source 13
    target 202
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 18
  ]
  edge [
    source 14
    target 19
  ]
  edge [
    source 14
    target 203
  ]
  edge [
    source 14
    target 204
  ]
  edge [
    source 14
    target 205
  ]
  edge [
    source 14
    target 206
  ]
  edge [
    source 14
    target 207
  ]
  edge [
    source 14
    target 208
  ]
  edge [
    source 14
    target 209
  ]
  edge [
    source 14
    target 210
  ]
  edge [
    source 14
    target 211
  ]
  edge [
    source 15
    target 22
  ]
  edge [
    source 15
    target 23
  ]
  edge [
    source 15
    target 212
  ]
  edge [
    source 15
    target 213
  ]
  edge [
    source 15
    target 214
  ]
  edge [
    source 15
    target 215
  ]
  edge [
    source 15
    target 216
  ]
  edge [
    source 15
    target 217
  ]
  edge [
    source 15
    target 218
  ]
  edge [
    source 15
    target 219
  ]
  edge [
    source 15
    target 220
  ]
  edge [
    source 15
    target 221
  ]
  edge [
    source 15
    target 222
  ]
  edge [
    source 15
    target 223
  ]
  edge [
    source 15
    target 224
  ]
  edge [
    source 15
    target 225
  ]
  edge [
    source 15
    target 86
  ]
  edge [
    source 15
    target 226
  ]
  edge [
    source 15
    target 227
  ]
  edge [
    source 15
    target 228
  ]
  edge [
    source 15
    target 229
  ]
  edge [
    source 15
    target 230
  ]
  edge [
    source 15
    target 144
  ]
  edge [
    source 15
    target 231
  ]
  edge [
    source 15
    target 232
  ]
  edge [
    source 15
    target 233
  ]
  edge [
    source 15
    target 234
  ]
  edge [
    source 15
    target 235
  ]
  edge [
    source 15
    target 236
  ]
  edge [
    source 15
    target 237
  ]
  edge [
    source 15
    target 238
  ]
  edge [
    source 15
    target 239
  ]
  edge [
    source 15
    target 240
  ]
  edge [
    source 15
    target 241
  ]
  edge [
    source 15
    target 242
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 37
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 243
  ]
  edge [
    source 17
    target 244
  ]
  edge [
    source 17
    target 245
  ]
  edge [
    source 17
    target 246
  ]
  edge [
    source 17
    target 247
  ]
  edge [
    source 17
    target 248
  ]
  edge [
    source 17
    target 249
  ]
  edge [
    source 17
    target 250
  ]
  edge [
    source 17
    target 251
  ]
  edge [
    source 17
    target 252
  ]
  edge [
    source 17
    target 253
  ]
  edge [
    source 17
    target 254
  ]
  edge [
    source 17
    target 255
  ]
  edge [
    source 17
    target 256
  ]
  edge [
    source 18
    target 257
  ]
  edge [
    source 18
    target 258
  ]
  edge [
    source 18
    target 259
  ]
  edge [
    source 18
    target 24
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 260
  ]
  edge [
    source 20
    target 261
  ]
  edge [
    source 20
    target 262
  ]
  edge [
    source 20
    target 263
  ]
  edge [
    source 20
    target 264
  ]
  edge [
    source 20
    target 265
  ]
  edge [
    source 20
    target 266
  ]
  edge [
    source 20
    target 267
  ]
  edge [
    source 20
    target 268
  ]
  edge [
    source 20
    target 189
  ]
  edge [
    source 20
    target 269
  ]
  edge [
    source 20
    target 270
  ]
  edge [
    source 20
    target 271
  ]
  edge [
    source 20
    target 272
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 273
  ]
  edge [
    source 21
    target 274
  ]
  edge [
    source 21
    target 275
  ]
  edge [
    source 22
    target 276
  ]
  edge [
    source 22
    target 51
  ]
  edge [
    source 22
    target 32
  ]
  edge [
    source 22
    target 277
  ]
  edge [
    source 22
    target 278
  ]
  edge [
    source 22
    target 279
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 280
  ]
  edge [
    source 24
    target 281
  ]
  edge [
    source 24
    target 282
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 283
  ]
  edge [
    source 25
    target 284
  ]
  edge [
    source 25
    target 285
  ]
  edge [
    source 25
    target 286
  ]
  edge [
    source 25
    target 287
  ]
  edge [
    source 25
    target 288
  ]
  edge [
    source 25
    target 289
  ]
  edge [
    source 25
    target 290
  ]
  edge [
    source 25
    target 291
  ]
  edge [
    source 25
    target 292
  ]
  edge [
    source 25
    target 293
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 294
  ]
  edge [
    source 27
    target 104
  ]
  edge [
    source 27
    target 295
  ]
  edge [
    source 27
    target 296
  ]
  edge [
    source 27
    target 297
  ]
  edge [
    source 28
    target 298
  ]
  edge [
    source 28
    target 299
  ]
]
