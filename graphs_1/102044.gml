graph [
  maxDegree 49
  minDegree 1
  meanDegree 2.37394451145959
  density 0.0028670827433086832
  graphCliqueNumber 5
  node [
    id 0
    label "pracownica"
    origin "text"
  ]
  node [
    id 1
    label "absolwent"
    origin "text"
  ]
  node [
    id 2
    label "program"
    origin "text"
  ]
  node [
    id 3
    label "terapeutyczny"
    origin "text"
  ]
  node [
    id 4
    label "o&#347;rodek"
    origin "text"
  ]
  node [
    id 5
    label "monar"
    origin "text"
  ]
  node [
    id 6
    label "&#322;&#243;d&#378;"
    origin "text"
  ]
  node [
    id 7
    label "ozorkowie"
    origin "text"
  ]
  node [
    id 8
    label "za&#322;o&#380;y&#263;"
    origin "text"
  ]
  node [
    id 9
    label "stowarzyszenie"
    origin "text"
  ]
  node [
    id 10
    label "kultura"
    origin "text"
  ]
  node [
    id 11
    label "fizyczny"
    origin "text"
  ]
  node [
    id 12
    label "pod"
    origin "text"
  ]
  node [
    id 13
    label "nazwa"
    origin "text"
  ]
  node [
    id 14
    label "klub"
    origin "text"
  ]
  node [
    id 15
    label "pi&#322;karski"
    origin "text"
  ]
  node [
    id 16
    label "kotan"
    origin "text"
  ]
  node [
    id 17
    label "ozorek"
    origin "text"
  ]
  node [
    id 18
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 19
    label "zarejestrowa&#263;"
    origin "text"
  ]
  node [
    id 20
    label "starostwo"
    origin "text"
  ]
  node [
    id 21
    label "powiatowy"
    origin "text"
  ]
  node [
    id 22
    label "zgierz"
    origin "text"
  ]
  node [
    id 23
    label "nast&#281;pnie"
    origin "text"
  ]
  node [
    id 24
    label "zg&#322;osi&#263;"
    origin "text"
  ]
  node [
    id 25
    label "rozgrywka"
    origin "text"
  ]
  node [
    id 26
    label "puchar"
    origin "text"
  ]
  node [
    id 27
    label "polska"
    origin "text"
  ]
  node [
    id 28
    label "ligowy"
    origin "text"
  ]
  node [
    id 29
    label "klasa"
    origin "text"
  ]
  node [
    id 30
    label "bardzo"
    origin "text"
  ]
  node [
    id 31
    label "prowadzony"
    origin "text"
  ]
  node [
    id 32
    label "przez"
    origin "text"
  ]
  node [
    id 33
    label "&#322;zpn"
    origin "text"
  ]
  node [
    id 34
    label "pi&#322;karz"
    origin "text"
  ]
  node [
    id 35
    label "kotana"
    origin "text"
  ]
  node [
    id 36
    label "pacjent"
    origin "text"
  ]
  node [
    id 37
    label "tzw"
    origin "text"
  ]
  node [
    id 38
    label "neofita"
    origin "text"
  ]
  node [
    id 39
    label "osoba"
    origin "text"
  ]
  node [
    id 40
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 41
    label "uko&#324;czy&#263;"
    origin "text"
  ]
  node [
    id 42
    label "leczenie"
    origin "text"
  ]
  node [
    id 43
    label "pierwszy"
    origin "text"
  ]
  node [
    id 44
    label "mecz"
    origin "text"
  ]
  node [
    id 45
    label "runda"
    origin "text"
  ]
  node [
    id 46
    label "wst&#281;pna"
    origin "text"
  ]
  node [
    id 47
    label "szczebel"
    origin "text"
  ]
  node [
    id 48
    label "wojew&#243;dzki"
    origin "text"
  ]
  node [
    id 49
    label "zespoli&#263;"
    origin "text"
  ]
  node [
    id 50
    label "rozegra&#263;"
    origin "text"
  ]
  node [
    id 51
    label "niedziela"
    origin "text"
  ]
  node [
    id 52
    label "godz"
    origin "text"
  ]
  node [
    id 53
    label "sw&#281;dowie"
    origin "text"
  ]
  node [
    id 54
    label "tamtejszy"
    origin "text"
  ]
  node [
    id 55
    label "huragan"
    origin "text"
  ]
  node [
    id 56
    label "je&#380;eli"
    origin "text"
  ]
  node [
    id 57
    label "wygra&#263;"
    origin "text"
  ]
  node [
    id 58
    label "wtorek"
    origin "text"
  ]
  node [
    id 59
    label "by&#263;"
    origin "text"
  ]
  node [
    id 60
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 61
    label "wolny"
    origin "text"
  ]
  node [
    id 62
    label "praca"
    origin "text"
  ]
  node [
    id 63
    label "doj&#347;&#263;"
    origin "text"
  ]
  node [
    id 64
    label "derby"
    origin "text"
  ]
  node [
    id 65
    label "ozorkowy"
    origin "text"
  ]
  node [
    id 66
    label "ksi&#261;dz"
    origin "text"
  ]
  node [
    id 67
    label "tur"
    origin "text"
  ]
  node [
    id 68
    label "swoje"
    origin "text"
  ]
  node [
    id 69
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 70
    label "rozgrywa&#263;"
    origin "text"
  ]
  node [
    id 71
    label "boisko"
    origin "text"
  ]
  node [
    id 72
    label "przy"
    origin "text"
  ]
  node [
    id 73
    label "ula"
    origin "text"
  ]
  node [
    id 74
    label "le&#347;na"
    origin "text"
  ]
  node [
    id 75
    label "ale"
    origin "text"
  ]
  node [
    id 76
    label "si&#281;"
    origin "text"
  ]
  node [
    id 77
    label "stara&#263;"
    origin "text"
  ]
  node [
    id 78
    label "zgoda"
    origin "text"
  ]
  node [
    id 79
    label "mosiru"
    origin "text"
  ]
  node [
    id 80
    label "prezes"
    origin "text"
  ]
  node [
    id 81
    label "spotkanie"
    origin "text"
  ]
  node [
    id 82
    label "pucharowy"
    origin "text"
  ]
  node [
    id 83
    label "sezon"
    origin "text"
  ]
  node [
    id 84
    label "stadion"
    origin "text"
  ]
  node [
    id 85
    label "&#322;&#281;czycki"
    origin "text"
  ]
  node [
    id 86
    label "nasienie"
    origin "text"
  ]
  node [
    id 87
    label "trawa"
    origin "text"
  ]
  node [
    id 88
    label "ilo&#347;&#263;"
    origin "text"
  ]
  node [
    id 89
    label "pierwotnie"
    origin "text"
  ]
  node [
    id 90
    label "mia&#322;yby&#263;"
    origin "text"
  ]
  node [
    id 91
    label "wysia&#263;"
    origin "text"
  ]
  node [
    id 92
    label "p&#322;yt"
    origin "text"
  ]
  node [
    id 93
    label "laski"
    origin "text"
  ]
  node [
    id 94
    label "miejski"
    origin "text"
  ]
  node [
    id 95
    label "przekaza&#263;"
    origin "text"
  ]
  node [
    id 96
    label "nowopowsta&#322;ego"
    origin "text"
  ]
  node [
    id 97
    label "treningowy"
    origin "text"
  ]
  node [
    id 98
    label "g&#322;&#243;wny"
    origin "text"
  ]
  node [
    id 99
    label "pro&#347;ba"
    origin "text"
  ]
  node [
    id 100
    label "osi&#324;ski"
    origin "text"
  ]
  node [
    id 101
    label "dyrektor"
    origin "text"
  ]
  node [
    id 102
    label "mosir"
    origin "text"
  ]
  node [
    id 103
    label "sierpie&#324;"
    origin "text"
  ]
  node [
    id 104
    label "wszelki"
    origin "text"
  ]
  node [
    id 105
    label "szczeg&#243;&#322;"
    origin "text"
  ]
  node [
    id 106
    label "dotyczy&#263;"
    origin "text"
  ]
  node [
    id 107
    label "umieszcza&#263;"
    origin "text"
  ]
  node [
    id 108
    label "strona"
    origin "text"
  ]
  node [
    id 109
    label "internetowy"
    origin "text"
  ]
  node [
    id 110
    label "chwila"
    origin "text"
  ]
  node [
    id 111
    label "obecna"
    origin "text"
  ]
  node [
    id 112
    label "budowa"
    origin "text"
  ]
  node [
    id 113
    label "adres"
    origin "text"
  ]
  node [
    id 114
    label "pan"
    origin "text"
  ]
  node [
    id 115
    label "cz&#322;owiek"
  ]
  node [
    id 116
    label "student"
  ]
  node [
    id 117
    label "szko&#322;a"
  ]
  node [
    id 118
    label "ucze&#324;"
  ]
  node [
    id 119
    label "spis"
  ]
  node [
    id 120
    label "odinstalowanie"
  ]
  node [
    id 121
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 122
    label "za&#322;o&#380;enie"
  ]
  node [
    id 123
    label "podstawa"
  ]
  node [
    id 124
    label "emitowanie"
  ]
  node [
    id 125
    label "odinstalowywanie"
  ]
  node [
    id 126
    label "instrukcja"
  ]
  node [
    id 127
    label "punkt"
  ]
  node [
    id 128
    label "teleferie"
  ]
  node [
    id 129
    label "emitowa&#263;"
  ]
  node [
    id 130
    label "wytw&#243;r"
  ]
  node [
    id 131
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 132
    label "sekcja_krytyczna"
  ]
  node [
    id 133
    label "oferta"
  ]
  node [
    id 134
    label "prezentowa&#263;"
  ]
  node [
    id 135
    label "blok"
  ]
  node [
    id 136
    label "podprogram"
  ]
  node [
    id 137
    label "tryb"
  ]
  node [
    id 138
    label "dzia&#322;"
  ]
  node [
    id 139
    label "broszura"
  ]
  node [
    id 140
    label "deklaracja"
  ]
  node [
    id 141
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 142
    label "struktura_organizacyjna"
  ]
  node [
    id 143
    label "zaprezentowanie"
  ]
  node [
    id 144
    label "informatyka"
  ]
  node [
    id 145
    label "booklet"
  ]
  node [
    id 146
    label "menu"
  ]
  node [
    id 147
    label "oprogramowanie"
  ]
  node [
    id 148
    label "instalowanie"
  ]
  node [
    id 149
    label "furkacja"
  ]
  node [
    id 150
    label "odinstalowa&#263;"
  ]
  node [
    id 151
    label "instalowa&#263;"
  ]
  node [
    id 152
    label "pirat"
  ]
  node [
    id 153
    label "zainstalowanie"
  ]
  node [
    id 154
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 155
    label "ogranicznik_referencyjny"
  ]
  node [
    id 156
    label "zainstalowa&#263;"
  ]
  node [
    id 157
    label "kana&#322;"
  ]
  node [
    id 158
    label "zaprezentowa&#263;"
  ]
  node [
    id 159
    label "interfejs"
  ]
  node [
    id 160
    label "odinstalowywa&#263;"
  ]
  node [
    id 161
    label "folder"
  ]
  node [
    id 162
    label "course_of_study"
  ]
  node [
    id 163
    label "ram&#243;wka"
  ]
  node [
    id 164
    label "prezentowanie"
  ]
  node [
    id 165
    label "okno"
  ]
  node [
    id 166
    label "terapeutycznie"
  ]
  node [
    id 167
    label "leczniczy"
  ]
  node [
    id 168
    label "miejsce"
  ]
  node [
    id 169
    label "Hollywood"
  ]
  node [
    id 170
    label "zal&#261;&#380;ek"
  ]
  node [
    id 171
    label "otoczenie"
  ]
  node [
    id 172
    label "o&#347;rodkowy_uk&#322;ad_nerwowy"
  ]
  node [
    id 173
    label "&#347;rodek"
  ]
  node [
    id 174
    label "center"
  ]
  node [
    id 175
    label "instytucja"
  ]
  node [
    id 176
    label "skupisko"
  ]
  node [
    id 177
    label "warunki"
  ]
  node [
    id 178
    label "regaty"
  ]
  node [
    id 179
    label "statek"
  ]
  node [
    id 180
    label "spalin&#243;wka"
  ]
  node [
    id 181
    label "pok&#322;ad"
  ]
  node [
    id 182
    label "ster"
  ]
  node [
    id 183
    label "kratownica"
  ]
  node [
    id 184
    label "pojazd_niemechaniczny"
  ]
  node [
    id 185
    label "drzewce"
  ]
  node [
    id 186
    label "insert"
  ]
  node [
    id 187
    label "utworzy&#263;"
  ]
  node [
    id 188
    label "ubra&#263;"
  ]
  node [
    id 189
    label "set"
  ]
  node [
    id 190
    label "invest"
  ]
  node [
    id 191
    label "pokry&#263;"
  ]
  node [
    id 192
    label "przewidzie&#263;"
  ]
  node [
    id 193
    label "umie&#347;ci&#263;"
  ]
  node [
    id 194
    label "map"
  ]
  node [
    id 195
    label "load"
  ]
  node [
    id 196
    label "zap&#322;aci&#263;"
  ]
  node [
    id 197
    label "oblec_si&#281;"
  ]
  node [
    id 198
    label "podwin&#261;&#263;"
  ]
  node [
    id 199
    label "plant"
  ]
  node [
    id 200
    label "create"
  ]
  node [
    id 201
    label "zrobi&#263;"
  ]
  node [
    id 202
    label "str&#243;j"
  ]
  node [
    id 203
    label "jell"
  ]
  node [
    id 204
    label "spowodowa&#263;"
  ]
  node [
    id 205
    label "oblec"
  ]
  node [
    id 206
    label "przyodzia&#263;"
  ]
  node [
    id 207
    label "install"
  ]
  node [
    id 208
    label "Ku&#378;nica_Ko&#322;&#322;&#261;tajowska"
  ]
  node [
    id 209
    label "Krakowska_Kongregacja_Kupiecka"
  ]
  node [
    id 210
    label "organizacja"
  ]
  node [
    id 211
    label "Eleusis"
  ]
  node [
    id 212
    label "Rzeczpospolita_babi&#324;ska"
  ]
  node [
    id 213
    label "Polskie_Towarzystwo_Turystyczno-Krajoznawcze"
  ]
  node [
    id 214
    label "grupa"
  ]
  node [
    id 215
    label "fabianie"
  ]
  node [
    id 216
    label "Chewra_Kadisza"
  ]
  node [
    id 217
    label "umowa_stowarzyszeniowa"
  ]
  node [
    id 218
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 219
    label "Rotary_International"
  ]
  node [
    id 220
    label "Monar"
  ]
  node [
    id 221
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 222
    label "przedmiot"
  ]
  node [
    id 223
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 224
    label "Wsch&#243;d"
  ]
  node [
    id 225
    label "rzecz"
  ]
  node [
    id 226
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 227
    label "sztuka"
  ]
  node [
    id 228
    label "religia"
  ]
  node [
    id 229
    label "przejmowa&#263;"
  ]
  node [
    id 230
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 231
    label "makrokosmos"
  ]
  node [
    id 232
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 233
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 234
    label "zjawisko"
  ]
  node [
    id 235
    label "praca_rolnicza"
  ]
  node [
    id 236
    label "tradycja"
  ]
  node [
    id 237
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 238
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 239
    label "przejmowanie"
  ]
  node [
    id 240
    label "cecha"
  ]
  node [
    id 241
    label "asymilowanie_si&#281;"
  ]
  node [
    id 242
    label "przej&#261;&#263;"
  ]
  node [
    id 243
    label "hodowla"
  ]
  node [
    id 244
    label "brzoskwiniarnia"
  ]
  node [
    id 245
    label "populace"
  ]
  node [
    id 246
    label "konwencja"
  ]
  node [
    id 247
    label "propriety"
  ]
  node [
    id 248
    label "jako&#347;&#263;"
  ]
  node [
    id 249
    label "kuchnia"
  ]
  node [
    id 250
    label "zwyczaj"
  ]
  node [
    id 251
    label "przej&#281;cie"
  ]
  node [
    id 252
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 253
    label "fizykalnie"
  ]
  node [
    id 254
    label "fizycznie"
  ]
  node [
    id 255
    label "materializowanie"
  ]
  node [
    id 256
    label "pracownik"
  ]
  node [
    id 257
    label "gimnastyczny"
  ]
  node [
    id 258
    label "widoczny"
  ]
  node [
    id 259
    label "namacalny"
  ]
  node [
    id 260
    label "zmaterializowanie"
  ]
  node [
    id 261
    label "organiczny"
  ]
  node [
    id 262
    label "materjalny"
  ]
  node [
    id 263
    label "term"
  ]
  node [
    id 264
    label "wezwanie"
  ]
  node [
    id 265
    label "leksem"
  ]
  node [
    id 266
    label "patron"
  ]
  node [
    id 267
    label "society"
  ]
  node [
    id 268
    label "jakobini"
  ]
  node [
    id 269
    label "klubista"
  ]
  node [
    id 270
    label "lokal"
  ]
  node [
    id 271
    label "od&#322;am"
  ]
  node [
    id 272
    label "siedziba"
  ]
  node [
    id 273
    label "bar"
  ]
  node [
    id 274
    label "specjalny"
  ]
  node [
    id 275
    label "sportowy"
  ]
  node [
    id 276
    label "po_pi&#322;karsku"
  ]
  node [
    id 277
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 278
    label "typowy"
  ]
  node [
    id 279
    label "pi&#322;karsko"
  ]
  node [
    id 280
    label "jednostka_osadnicza"
  ]
  node [
    id 281
    label "pieczarkowiec"
  ]
  node [
    id 282
    label "ozorkowate"
  ]
  node [
    id 283
    label "saprotrof"
  ]
  node [
    id 284
    label "grzyb"
  ]
  node [
    id 285
    label "paso&#380;yt"
  ]
  node [
    id 286
    label "podroby"
  ]
  node [
    id 287
    label "proceed"
  ]
  node [
    id 288
    label "catch"
  ]
  node [
    id 289
    label "pozosta&#263;"
  ]
  node [
    id 290
    label "osta&#263;_si&#281;"
  ]
  node [
    id 291
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 292
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 293
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 294
    label "change"
  ]
  node [
    id 295
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 296
    label "notice"
  ]
  node [
    id 297
    label "logarithm"
  ]
  node [
    id 298
    label "wpisa&#263;"
  ]
  node [
    id 299
    label "urz&#261;d"
  ]
  node [
    id 300
    label "jednostka_administracyjna"
  ]
  node [
    id 301
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 302
    label "kolejny"
  ]
  node [
    id 303
    label "report"
  ]
  node [
    id 304
    label "announce"
  ]
  node [
    id 305
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 306
    label "write"
  ]
  node [
    id 307
    label "poinformowa&#263;"
  ]
  node [
    id 308
    label "euroliga"
  ]
  node [
    id 309
    label "zagrywka"
  ]
  node [
    id 310
    label "rewan&#380;owy"
  ]
  node [
    id 311
    label "faza"
  ]
  node [
    id 312
    label "interliga"
  ]
  node [
    id 313
    label "trafienie"
  ]
  node [
    id 314
    label "contest"
  ]
  node [
    id 315
    label "zawarto&#347;&#263;"
  ]
  node [
    id 316
    label "zawody"
  ]
  node [
    id 317
    label "zwyci&#281;stwo"
  ]
  node [
    id 318
    label "naczynie"
  ]
  node [
    id 319
    label "nagroda"
  ]
  node [
    id 320
    label "typ"
  ]
  node [
    id 321
    label "warstwa"
  ]
  node [
    id 322
    label "znak_jako&#347;ci"
  ]
  node [
    id 323
    label "przepisa&#263;"
  ]
  node [
    id 324
    label "pomoc"
  ]
  node [
    id 325
    label "arrangement"
  ]
  node [
    id 326
    label "wagon"
  ]
  node [
    id 327
    label "form"
  ]
  node [
    id 328
    label "zaleta"
  ]
  node [
    id 329
    label "poziom"
  ]
  node [
    id 330
    label "dziennik_lekcyjny"
  ]
  node [
    id 331
    label "&#347;rodowisko"
  ]
  node [
    id 332
    label "atak"
  ]
  node [
    id 333
    label "przepisanie"
  ]
  node [
    id 334
    label "class"
  ]
  node [
    id 335
    label "obrona"
  ]
  node [
    id 336
    label "type"
  ]
  node [
    id 337
    label "promocja"
  ]
  node [
    id 338
    label "&#322;awka"
  ]
  node [
    id 339
    label "kurs"
  ]
  node [
    id 340
    label "botanika"
  ]
  node [
    id 341
    label "sala"
  ]
  node [
    id 342
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 343
    label "gromada"
  ]
  node [
    id 344
    label "obiekt"
  ]
  node [
    id 345
    label "Ekwici"
  ]
  node [
    id 346
    label "fakcja"
  ]
  node [
    id 347
    label "tablica"
  ]
  node [
    id 348
    label "programowanie_obiektowe"
  ]
  node [
    id 349
    label "wykrzyknik"
  ]
  node [
    id 350
    label "jednostka_systematyczna"
  ]
  node [
    id 351
    label "mecz_mistrzowski"
  ]
  node [
    id 352
    label "zbi&#243;r"
  ]
  node [
    id 353
    label "rezerwa"
  ]
  node [
    id 354
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 355
    label "w_chuj"
  ]
  node [
    id 356
    label "gracz"
  ]
  node [
    id 357
    label "legionista"
  ]
  node [
    id 358
    label "sportowiec"
  ]
  node [
    id 359
    label "Daniel_Dubicki"
  ]
  node [
    id 360
    label "od&#322;&#261;czenie"
  ]
  node [
    id 361
    label "przymus_bezpo&#347;redni"
  ]
  node [
    id 362
    label "od&#322;&#261;czanie"
  ]
  node [
    id 363
    label "od&#322;&#261;czy&#263;"
  ]
  node [
    id 364
    label "klient"
  ]
  node [
    id 365
    label "od&#322;&#261;cza&#263;"
  ]
  node [
    id 366
    label "szpitalnik"
  ]
  node [
    id 367
    label "przypadek"
  ]
  node [
    id 368
    label "chory"
  ]
  node [
    id 369
    label "piel&#281;gniarz"
  ]
  node [
    id 370
    label "nowowierca"
  ]
  node [
    id 371
    label "katechumen"
  ]
  node [
    id 372
    label "nadgorliwiec"
  ]
  node [
    id 373
    label "nowicjusz"
  ]
  node [
    id 374
    label "wyznawca"
  ]
  node [
    id 375
    label "nowy"
  ]
  node [
    id 376
    label "Zgredek"
  ]
  node [
    id 377
    label "kategoria_gramatyczna"
  ]
  node [
    id 378
    label "Casanova"
  ]
  node [
    id 379
    label "Don_Juan"
  ]
  node [
    id 380
    label "Gargantua"
  ]
  node [
    id 381
    label "Faust"
  ]
  node [
    id 382
    label "profanum"
  ]
  node [
    id 383
    label "Chocho&#322;"
  ]
  node [
    id 384
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 385
    label "koniugacja"
  ]
  node [
    id 386
    label "Winnetou"
  ]
  node [
    id 387
    label "Dwukwiat"
  ]
  node [
    id 388
    label "homo_sapiens"
  ]
  node [
    id 389
    label "Edyp"
  ]
  node [
    id 390
    label "Herkules_Poirot"
  ]
  node [
    id 391
    label "ludzko&#347;&#263;"
  ]
  node [
    id 392
    label "mikrokosmos"
  ]
  node [
    id 393
    label "person"
  ]
  node [
    id 394
    label "Szwejk"
  ]
  node [
    id 395
    label "portrecista"
  ]
  node [
    id 396
    label "Sherlock_Holmes"
  ]
  node [
    id 397
    label "Hamlet"
  ]
  node [
    id 398
    label "duch"
  ]
  node [
    id 399
    label "oddzia&#322;ywanie"
  ]
  node [
    id 400
    label "g&#322;owa"
  ]
  node [
    id 401
    label "Quasimodo"
  ]
  node [
    id 402
    label "Dulcynea"
  ]
  node [
    id 403
    label "Wallenrod"
  ]
  node [
    id 404
    label "Don_Kiszot"
  ]
  node [
    id 405
    label "Plastu&#347;"
  ]
  node [
    id 406
    label "Harry_Potter"
  ]
  node [
    id 407
    label "figura"
  ]
  node [
    id 408
    label "parali&#380;owa&#263;"
  ]
  node [
    id 409
    label "istota"
  ]
  node [
    id 410
    label "Werter"
  ]
  node [
    id 411
    label "antropochoria"
  ]
  node [
    id 412
    label "posta&#263;"
  ]
  node [
    id 413
    label "communicate"
  ]
  node [
    id 414
    label "przesta&#263;"
  ]
  node [
    id 415
    label "end"
  ]
  node [
    id 416
    label "sko&#324;czy&#263;"
  ]
  node [
    id 417
    label "opatrywanie"
  ]
  node [
    id 418
    label "homeopata"
  ]
  node [
    id 419
    label "zabieg"
  ]
  node [
    id 420
    label "sanatoryjny"
  ]
  node [
    id 421
    label "&#322;agodzenie"
  ]
  node [
    id 422
    label "odwykowy"
  ]
  node [
    id 423
    label "alopata"
  ]
  node [
    id 424
    label "nauka_medyczna"
  ]
  node [
    id 425
    label "pomaganie"
  ]
  node [
    id 426
    label "plombowanie"
  ]
  node [
    id 427
    label "uzdrawianie"
  ]
  node [
    id 428
    label "wizyta"
  ]
  node [
    id 429
    label "zdiagnozowanie"
  ]
  node [
    id 430
    label "wizytowanie"
  ]
  node [
    id 431
    label "opatrzenie"
  ]
  node [
    id 432
    label "medication"
  ]
  node [
    id 433
    label "opieka_medyczna"
  ]
  node [
    id 434
    label "najwa&#380;niejszy"
  ]
  node [
    id 435
    label "pocz&#261;tkowy"
  ]
  node [
    id 436
    label "dobry"
  ]
  node [
    id 437
    label "ch&#281;tny"
  ]
  node [
    id 438
    label "pr&#281;dki"
  ]
  node [
    id 439
    label "gra"
  ]
  node [
    id 440
    label "dwumecz"
  ]
  node [
    id 441
    label "game"
  ]
  node [
    id 442
    label "serw"
  ]
  node [
    id 443
    label "seria"
  ]
  node [
    id 444
    label "czas"
  ]
  node [
    id 445
    label "okr&#261;&#380;enie"
  ]
  node [
    id 446
    label "rhythm"
  ]
  node [
    id 447
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 448
    label "turniej"
  ]
  node [
    id 449
    label "krewna"
  ]
  node [
    id 450
    label "stopie&#324;"
  ]
  node [
    id 451
    label "drabina"
  ]
  node [
    id 452
    label "gradation"
  ]
  node [
    id 453
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 454
    label "connect"
  ]
  node [
    id 455
    label "przeprowadzi&#263;"
  ]
  node [
    id 456
    label "play"
  ]
  node [
    id 457
    label "Dzie&#324;_Papieski"
  ]
  node [
    id 458
    label "Wielkanoc"
  ]
  node [
    id 459
    label "dzie&#324;_&#347;wi&#281;ty"
  ]
  node [
    id 460
    label "weekend"
  ]
  node [
    id 461
    label "Niedziela_Palmowa"
  ]
  node [
    id 462
    label "Zes&#322;anie_Ducha_&#346;wi&#281;tego"
  ]
  node [
    id 463
    label "dzie&#324;_&#347;wi&#261;teczny"
  ]
  node [
    id 464
    label "bia&#322;a_niedziela"
  ]
  node [
    id 465
    label "niedziela_przewodnia"
  ]
  node [
    id 466
    label "tydzie&#324;"
  ]
  node [
    id 467
    label "nietutejszy"
  ]
  node [
    id 468
    label "wiatr"
  ]
  node [
    id 469
    label "wojna"
  ]
  node [
    id 470
    label "rycz&#261;ce_czterdziestki"
  ]
  node [
    id 471
    label "nawa&#322;no&#347;&#263;"
  ]
  node [
    id 472
    label "fire"
  ]
  node [
    id 473
    label "wydarzenie"
  ]
  node [
    id 474
    label "wichrzyca"
  ]
  node [
    id 475
    label "cyclone"
  ]
  node [
    id 476
    label "score"
  ]
  node [
    id 477
    label "okaza&#263;_si&#281;"
  ]
  node [
    id 478
    label "zwojowa&#263;"
  ]
  node [
    id 479
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 480
    label "leave"
  ]
  node [
    id 481
    label "znie&#347;&#263;"
  ]
  node [
    id 482
    label "zagwarantowa&#263;"
  ]
  node [
    id 483
    label "instrument_muzyczny"
  ]
  node [
    id 484
    label "zagra&#263;"
  ]
  node [
    id 485
    label "net_income"
  ]
  node [
    id 486
    label "dzie&#324;_powszedni"
  ]
  node [
    id 487
    label "si&#281;ga&#263;"
  ]
  node [
    id 488
    label "trwa&#263;"
  ]
  node [
    id 489
    label "obecno&#347;&#263;"
  ]
  node [
    id 490
    label "stan"
  ]
  node [
    id 491
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 492
    label "stand"
  ]
  node [
    id 493
    label "mie&#263;_miejsce"
  ]
  node [
    id 494
    label "uczestniczy&#263;"
  ]
  node [
    id 495
    label "chodzi&#263;"
  ]
  node [
    id 496
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 497
    label "equal"
  ]
  node [
    id 498
    label "s&#322;o&#324;ce"
  ]
  node [
    id 499
    label "czynienie_si&#281;"
  ]
  node [
    id 500
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 501
    label "long_time"
  ]
  node [
    id 502
    label "przedpo&#322;udnie"
  ]
  node [
    id 503
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 504
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 505
    label "godzina"
  ]
  node [
    id 506
    label "t&#322;usty_czwartek"
  ]
  node [
    id 507
    label "wsta&#263;"
  ]
  node [
    id 508
    label "day"
  ]
  node [
    id 509
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 510
    label "przedwiecz&#243;r"
  ]
  node [
    id 511
    label "Sylwester"
  ]
  node [
    id 512
    label "po&#322;udnie"
  ]
  node [
    id 513
    label "wzej&#347;cie"
  ]
  node [
    id 514
    label "podwiecz&#243;r"
  ]
  node [
    id 515
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 516
    label "rano"
  ]
  node [
    id 517
    label "termin"
  ]
  node [
    id 518
    label "ranek"
  ]
  node [
    id 519
    label "doba"
  ]
  node [
    id 520
    label "wiecz&#243;r"
  ]
  node [
    id 521
    label "walentynki"
  ]
  node [
    id 522
    label "popo&#322;udnie"
  ]
  node [
    id 523
    label "noc"
  ]
  node [
    id 524
    label "wstanie"
  ]
  node [
    id 525
    label "niezale&#380;ny"
  ]
  node [
    id 526
    label "swobodnie"
  ]
  node [
    id 527
    label "niespieszny"
  ]
  node [
    id 528
    label "rozrzedzanie"
  ]
  node [
    id 529
    label "zwolnienie_si&#281;"
  ]
  node [
    id 530
    label "wolno"
  ]
  node [
    id 531
    label "rozrzedzenie"
  ]
  node [
    id 532
    label "lu&#378;no"
  ]
  node [
    id 533
    label "zwalnianie_si&#281;"
  ]
  node [
    id 534
    label "wolnie"
  ]
  node [
    id 535
    label "strza&#322;"
  ]
  node [
    id 536
    label "rozwodnienie"
  ]
  node [
    id 537
    label "wakowa&#263;"
  ]
  node [
    id 538
    label "rozwadnianie"
  ]
  node [
    id 539
    label "rzedni&#281;cie"
  ]
  node [
    id 540
    label "zrzedni&#281;cie"
  ]
  node [
    id 541
    label "stosunek_pracy"
  ]
  node [
    id 542
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 543
    label "benedykty&#324;ski"
  ]
  node [
    id 544
    label "pracowanie"
  ]
  node [
    id 545
    label "zaw&#243;d"
  ]
  node [
    id 546
    label "kierownictwo"
  ]
  node [
    id 547
    label "zmiana"
  ]
  node [
    id 548
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 549
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 550
    label "tynkarski"
  ]
  node [
    id 551
    label "czynnik_produkcji"
  ]
  node [
    id 552
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 553
    label "zobowi&#261;zanie"
  ]
  node [
    id 554
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 555
    label "czynno&#347;&#263;"
  ]
  node [
    id 556
    label "tyrka"
  ]
  node [
    id 557
    label "pracowa&#263;"
  ]
  node [
    id 558
    label "poda&#380;_pracy"
  ]
  node [
    id 559
    label "zak&#322;ad"
  ]
  node [
    id 560
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 561
    label "najem"
  ]
  node [
    id 562
    label "get"
  ]
  node [
    id 563
    label "zaj&#347;&#263;"
  ]
  node [
    id 564
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 565
    label "dop&#322;ata"
  ]
  node [
    id 566
    label "supervene"
  ]
  node [
    id 567
    label "heed"
  ]
  node [
    id 568
    label "dodatek"
  ]
  node [
    id 569
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 570
    label "uzyska&#263;"
  ]
  node [
    id 571
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 572
    label "orgazm"
  ]
  node [
    id 573
    label "dozna&#263;"
  ]
  node [
    id 574
    label "sta&#263;_si&#281;"
  ]
  node [
    id 575
    label "bodziec"
  ]
  node [
    id 576
    label "drive"
  ]
  node [
    id 577
    label "informacja"
  ]
  node [
    id 578
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 579
    label "dotrze&#263;"
  ]
  node [
    id 580
    label "postrzega&#263;"
  ]
  node [
    id 581
    label "become"
  ]
  node [
    id 582
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 583
    label "dop&#322;yn&#261;&#263;"
  ]
  node [
    id 584
    label "przesy&#322;ka"
  ]
  node [
    id 585
    label "dolecie&#263;"
  ]
  node [
    id 586
    label "przy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 587
    label "dokoptowa&#263;"
  ]
  node [
    id 588
    label "bowler_hat"
  ]
  node [
    id 589
    label "gonitwa"
  ]
  node [
    id 590
    label "obuwie"
  ]
  node [
    id 591
    label "mi&#281;sny"
  ]
  node [
    id 592
    label "klecha"
  ]
  node [
    id 593
    label "eklezjasta"
  ]
  node [
    id 594
    label "rozgrzeszanie"
  ]
  node [
    id 595
    label "duszpasterstwo"
  ]
  node [
    id 596
    label "rozgrzesza&#263;"
  ]
  node [
    id 597
    label "kap&#322;an"
  ]
  node [
    id 598
    label "ksi&#281;&#380;a"
  ]
  node [
    id 599
    label "duchowny"
  ]
  node [
    id 600
    label "kol&#281;da"
  ]
  node [
    id 601
    label "seminarzysta"
  ]
  node [
    id 602
    label "pasterz"
  ]
  node [
    id 603
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 604
    label "byd&#322;o"
  ]
  node [
    id 605
    label "ssak_wymar&#322;y"
  ]
  node [
    id 606
    label "czyj&#347;"
  ]
  node [
    id 607
    label "m&#261;&#380;"
  ]
  node [
    id 608
    label "robi&#263;"
  ]
  node [
    id 609
    label "przeprowadza&#263;"
  ]
  node [
    id 610
    label "gra&#263;"
  ]
  node [
    id 611
    label "ziemia"
  ]
  node [
    id 612
    label "pole"
  ]
  node [
    id 613
    label "linia"
  ]
  node [
    id 614
    label "skrzyd&#322;o"
  ]
  node [
    id 615
    label "aut"
  ]
  node [
    id 616
    label "bojo"
  ]
  node [
    id 617
    label "bojowisko"
  ]
  node [
    id 618
    label "budowla"
  ]
  node [
    id 619
    label "piwo"
  ]
  node [
    id 620
    label "spok&#243;j"
  ]
  node [
    id 621
    label "odpowied&#378;"
  ]
  node [
    id 622
    label "wiedza"
  ]
  node [
    id 623
    label "zwalnia&#263;_si&#281;"
  ]
  node [
    id 624
    label "zwolni&#263;_si&#281;"
  ]
  node [
    id 625
    label "entity"
  ]
  node [
    id 626
    label "agreement"
  ]
  node [
    id 627
    label "consensus"
  ]
  node [
    id 628
    label "decyzja"
  ]
  node [
    id 629
    label "jednomy&#347;lno&#347;&#263;"
  ]
  node [
    id 630
    label "license"
  ]
  node [
    id 631
    label "pozwole&#324;stwo"
  ]
  node [
    id 632
    label "gruba_ryba"
  ]
  node [
    id 633
    label "zwierzchnik"
  ]
  node [
    id 634
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 635
    label "po&#380;egnanie"
  ]
  node [
    id 636
    label "spowodowanie"
  ]
  node [
    id 637
    label "znalezienie"
  ]
  node [
    id 638
    label "znajomy"
  ]
  node [
    id 639
    label "doznanie"
  ]
  node [
    id 640
    label "employment"
  ]
  node [
    id 641
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 642
    label "gather"
  ]
  node [
    id 643
    label "powitanie"
  ]
  node [
    id 644
    label "spotykanie"
  ]
  node [
    id 645
    label "gathering"
  ]
  node [
    id 646
    label "spotkanie_si&#281;"
  ]
  node [
    id 647
    label "zdarzenie_si&#281;"
  ]
  node [
    id 648
    label "match"
  ]
  node [
    id 649
    label "zawarcie"
  ]
  node [
    id 650
    label "przypominaj&#261;cy"
  ]
  node [
    id 651
    label "season"
  ]
  node [
    id 652
    label "serial"
  ]
  node [
    id 653
    label "rok"
  ]
  node [
    id 654
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 655
    label "zgromadzenie"
  ]
  node [
    id 656
    label "korona"
  ]
  node [
    id 657
    label "court"
  ]
  node [
    id 658
    label "trybuna"
  ]
  node [
    id 659
    label "&#322;upina"
  ]
  node [
    id 660
    label "zarodek"
  ]
  node [
    id 661
    label "plemnik"
  ]
  node [
    id 662
    label "p&#322;yn_nasienny"
  ]
  node [
    id 663
    label "spierdolina"
  ]
  node [
    id 664
    label "elajosom"
  ]
  node [
    id 665
    label "organ"
  ]
  node [
    id 666
    label "seed"
  ]
  node [
    id 667
    label "przyczepka"
  ]
  node [
    id 668
    label "bielmo"
  ]
  node [
    id 669
    label "narkotyk_mi&#281;kki"
  ]
  node [
    id 670
    label "wiechlinowate"
  ]
  node [
    id 671
    label "ziarno"
  ]
  node [
    id 672
    label "rastaman"
  ]
  node [
    id 673
    label "przestrze&#324;"
  ]
  node [
    id 674
    label "ro&#347;lina"
  ]
  node [
    id 675
    label "teren"
  ]
  node [
    id 676
    label "zbiorowisko"
  ]
  node [
    id 677
    label "koleoryza"
  ]
  node [
    id 678
    label "rozmiar"
  ]
  node [
    id 679
    label "part"
  ]
  node [
    id 680
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 681
    label "instynktownie"
  ]
  node [
    id 682
    label "pocz&#261;tkowo"
  ]
  node [
    id 683
    label "niezale&#380;nie"
  ]
  node [
    id 684
    label "dziko"
  ]
  node [
    id 685
    label "prymitywny"
  ]
  node [
    id 686
    label "pierwotny"
  ]
  node [
    id 687
    label "miejsko"
  ]
  node [
    id 688
    label "miastowy"
  ]
  node [
    id 689
    label "publiczny"
  ]
  node [
    id 690
    label "impart"
  ]
  node [
    id 691
    label "sygna&#322;"
  ]
  node [
    id 692
    label "propagate"
  ]
  node [
    id 693
    label "transfer"
  ]
  node [
    id 694
    label "give"
  ]
  node [
    id 695
    label "wys&#322;a&#263;"
  ]
  node [
    id 696
    label "poda&#263;"
  ]
  node [
    id 697
    label "wp&#322;aci&#263;"
  ]
  node [
    id 698
    label "treningowo"
  ]
  node [
    id 699
    label "g&#322;&#243;wnie"
  ]
  node [
    id 700
    label "solicitation"
  ]
  node [
    id 701
    label "wypowied&#378;"
  ]
  node [
    id 702
    label "dyro"
  ]
  node [
    id 703
    label "dyrektoriat"
  ]
  node [
    id 704
    label "dyrygent"
  ]
  node [
    id 705
    label "Tadeusz_Rydzyk"
  ]
  node [
    id 706
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 707
    label "Wniebowzi&#281;cie_Naj&#347;wi&#281;tszej_Maryi_Panny"
  ]
  node [
    id 708
    label "miesi&#261;c"
  ]
  node [
    id 709
    label "Sierpie&#324;"
  ]
  node [
    id 710
    label "ka&#380;dy"
  ]
  node [
    id 711
    label "niuansowa&#263;_si&#281;"
  ]
  node [
    id 712
    label "niuansowa&#263;"
  ]
  node [
    id 713
    label "zniuansowa&#263;"
  ]
  node [
    id 714
    label "element"
  ]
  node [
    id 715
    label "sk&#322;adnik"
  ]
  node [
    id 716
    label "zniuansowa&#263;_si&#281;"
  ]
  node [
    id 717
    label "bargain"
  ]
  node [
    id 718
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 719
    label "tycze&#263;"
  ]
  node [
    id 720
    label "zmienia&#263;"
  ]
  node [
    id 721
    label "plasowa&#263;"
  ]
  node [
    id 722
    label "prze&#322;adowywa&#263;"
  ]
  node [
    id 723
    label "pomieszcza&#263;"
  ]
  node [
    id 724
    label "wpiernicza&#263;"
  ]
  node [
    id 725
    label "accommodate"
  ]
  node [
    id 726
    label "venture"
  ]
  node [
    id 727
    label "powodowa&#263;"
  ]
  node [
    id 728
    label "okre&#347;la&#263;"
  ]
  node [
    id 729
    label "skr&#281;canie"
  ]
  node [
    id 730
    label "voice"
  ]
  node [
    id 731
    label "forma"
  ]
  node [
    id 732
    label "internet"
  ]
  node [
    id 733
    label "skr&#281;ci&#263;"
  ]
  node [
    id 734
    label "kartka"
  ]
  node [
    id 735
    label "orientowa&#263;"
  ]
  node [
    id 736
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 737
    label "powierzchnia"
  ]
  node [
    id 738
    label "plik"
  ]
  node [
    id 739
    label "bok"
  ]
  node [
    id 740
    label "pagina"
  ]
  node [
    id 741
    label "orientowanie"
  ]
  node [
    id 742
    label "fragment"
  ]
  node [
    id 743
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 744
    label "s&#261;d"
  ]
  node [
    id 745
    label "skr&#281;ca&#263;"
  ]
  node [
    id 746
    label "g&#243;ra"
  ]
  node [
    id 747
    label "serwis_internetowy"
  ]
  node [
    id 748
    label "orientacja"
  ]
  node [
    id 749
    label "skr&#281;cenie"
  ]
  node [
    id 750
    label "layout"
  ]
  node [
    id 751
    label "zorientowa&#263;"
  ]
  node [
    id 752
    label "zorientowanie"
  ]
  node [
    id 753
    label "podmiot"
  ]
  node [
    id 754
    label "ty&#322;"
  ]
  node [
    id 755
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 756
    label "logowanie"
  ]
  node [
    id 757
    label "adres_internetowy"
  ]
  node [
    id 758
    label "uj&#281;cie"
  ]
  node [
    id 759
    label "prz&#243;d"
  ]
  node [
    id 760
    label "nowoczesny"
  ]
  node [
    id 761
    label "elektroniczny"
  ]
  node [
    id 762
    label "sieciowo"
  ]
  node [
    id 763
    label "netowy"
  ]
  node [
    id 764
    label "internetowo"
  ]
  node [
    id 765
    label "time"
  ]
  node [
    id 766
    label "wjazd"
  ]
  node [
    id 767
    label "struktura"
  ]
  node [
    id 768
    label "konstrukcja"
  ]
  node [
    id 769
    label "r&#243;w"
  ]
  node [
    id 770
    label "kreacja"
  ]
  node [
    id 771
    label "posesja"
  ]
  node [
    id 772
    label "dokumentacja_powykonawcza"
  ]
  node [
    id 773
    label "mechanika"
  ]
  node [
    id 774
    label "zwierz&#281;"
  ]
  node [
    id 775
    label "miejsce_pracy"
  ]
  node [
    id 776
    label "constitution"
  ]
  node [
    id 777
    label "adres_elektroniczny"
  ]
  node [
    id 778
    label "domena"
  ]
  node [
    id 779
    label "po&#322;o&#380;enie"
  ]
  node [
    id 780
    label "kod_pocztowy"
  ]
  node [
    id 781
    label "dane"
  ]
  node [
    id 782
    label "pismo"
  ]
  node [
    id 783
    label "personalia"
  ]
  node [
    id 784
    label "dziedzina"
  ]
  node [
    id 785
    label "profesor"
  ]
  node [
    id 786
    label "kszta&#322;ciciel"
  ]
  node [
    id 787
    label "jegomo&#347;&#263;"
  ]
  node [
    id 788
    label "zwrot"
  ]
  node [
    id 789
    label "pracodawca"
  ]
  node [
    id 790
    label "rz&#261;dzenie"
  ]
  node [
    id 791
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 792
    label "ch&#322;opina"
  ]
  node [
    id 793
    label "bratek"
  ]
  node [
    id 794
    label "opiekun"
  ]
  node [
    id 795
    label "doros&#322;y"
  ]
  node [
    id 796
    label "preceptor"
  ]
  node [
    id 797
    label "Midas"
  ]
  node [
    id 798
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 799
    label "murza"
  ]
  node [
    id 800
    label "ojciec"
  ]
  node [
    id 801
    label "androlog"
  ]
  node [
    id 802
    label "pupil"
  ]
  node [
    id 803
    label "efendi"
  ]
  node [
    id 804
    label "nabab"
  ]
  node [
    id 805
    label "w&#322;odarz"
  ]
  node [
    id 806
    label "szkolnik"
  ]
  node [
    id 807
    label "pedagog"
  ]
  node [
    id 808
    label "popularyzator"
  ]
  node [
    id 809
    label "andropauza"
  ]
  node [
    id 810
    label "gra_w_karty"
  ]
  node [
    id 811
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 812
    label "Mieszko_I"
  ]
  node [
    id 813
    label "bogaty"
  ]
  node [
    id 814
    label "samiec"
  ]
  node [
    id 815
    label "przyw&#243;dca"
  ]
  node [
    id 816
    label "pa&#324;stwo"
  ]
  node [
    id 817
    label "belfer"
  ]
  node [
    id 818
    label "kluba"
  ]
  node [
    id 819
    label "KOTAN"
  ]
  node [
    id 820
    label "wyspa"
  ]
  node [
    id 821
    label "Zgierz"
  ]
  node [
    id 822
    label "polski"
  ]
  node [
    id 823
    label "Kotana"
  ]
  node [
    id 824
    label "KP"
  ]
  node [
    id 825
    label "tura"
  ]
  node [
    id 826
    label "by&#322;y"
  ]
  node [
    id 827
    label "ko&#322;o"
  ]
  node [
    id 828
    label "p&#243;&#378;no"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 186
  ]
  edge [
    source 8
    target 187
  ]
  edge [
    source 8
    target 188
  ]
  edge [
    source 8
    target 189
  ]
  edge [
    source 8
    target 190
  ]
  edge [
    source 8
    target 191
  ]
  edge [
    source 8
    target 192
  ]
  edge [
    source 8
    target 193
  ]
  edge [
    source 8
    target 194
  ]
  edge [
    source 8
    target 195
  ]
  edge [
    source 8
    target 196
  ]
  edge [
    source 8
    target 197
  ]
  edge [
    source 8
    target 198
  ]
  edge [
    source 8
    target 199
  ]
  edge [
    source 8
    target 200
  ]
  edge [
    source 8
    target 201
  ]
  edge [
    source 8
    target 202
  ]
  edge [
    source 8
    target 203
  ]
  edge [
    source 8
    target 204
  ]
  edge [
    source 8
    target 205
  ]
  edge [
    source 8
    target 206
  ]
  edge [
    source 8
    target 207
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 208
  ]
  edge [
    source 9
    target 209
  ]
  edge [
    source 9
    target 210
  ]
  edge [
    source 9
    target 211
  ]
  edge [
    source 9
    target 212
  ]
  edge [
    source 9
    target 213
  ]
  edge [
    source 9
    target 214
  ]
  edge [
    source 9
    target 215
  ]
  edge [
    source 9
    target 216
  ]
  edge [
    source 9
    target 217
  ]
  edge [
    source 9
    target 218
  ]
  edge [
    source 9
    target 219
  ]
  edge [
    source 9
    target 220
  ]
  edge [
    source 9
    target 14
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 221
  ]
  edge [
    source 10
    target 222
  ]
  edge [
    source 10
    target 223
  ]
  edge [
    source 10
    target 224
  ]
  edge [
    source 10
    target 225
  ]
  edge [
    source 10
    target 226
  ]
  edge [
    source 10
    target 227
  ]
  edge [
    source 10
    target 228
  ]
  edge [
    source 10
    target 229
  ]
  edge [
    source 10
    target 230
  ]
  edge [
    source 10
    target 231
  ]
  edge [
    source 10
    target 232
  ]
  edge [
    source 10
    target 233
  ]
  edge [
    source 10
    target 234
  ]
  edge [
    source 10
    target 235
  ]
  edge [
    source 10
    target 236
  ]
  edge [
    source 10
    target 237
  ]
  edge [
    source 10
    target 238
  ]
  edge [
    source 10
    target 239
  ]
  edge [
    source 10
    target 240
  ]
  edge [
    source 10
    target 241
  ]
  edge [
    source 10
    target 242
  ]
  edge [
    source 10
    target 243
  ]
  edge [
    source 10
    target 244
  ]
  edge [
    source 10
    target 245
  ]
  edge [
    source 10
    target 246
  ]
  edge [
    source 10
    target 247
  ]
  edge [
    source 10
    target 248
  ]
  edge [
    source 10
    target 249
  ]
  edge [
    source 10
    target 250
  ]
  edge [
    source 10
    target 251
  ]
  edge [
    source 10
    target 252
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 253
  ]
  edge [
    source 11
    target 254
  ]
  edge [
    source 11
    target 255
  ]
  edge [
    source 11
    target 256
  ]
  edge [
    source 11
    target 257
  ]
  edge [
    source 11
    target 258
  ]
  edge [
    source 11
    target 259
  ]
  edge [
    source 11
    target 260
  ]
  edge [
    source 11
    target 261
  ]
  edge [
    source 11
    target 262
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 112
  ]
  edge [
    source 12
    target 113
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 263
  ]
  edge [
    source 13
    target 264
  ]
  edge [
    source 13
    target 265
  ]
  edge [
    source 13
    target 266
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 17
  ]
  edge [
    source 14
    target 18
  ]
  edge [
    source 14
    target 106
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 14
    target 267
  ]
  edge [
    source 14
    target 268
  ]
  edge [
    source 14
    target 269
  ]
  edge [
    source 14
    target 270
  ]
  edge [
    source 14
    target 271
  ]
  edge [
    source 14
    target 272
  ]
  edge [
    source 14
    target 273
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 63
  ]
  edge [
    source 15
    target 64
  ]
  edge [
    source 15
    target 274
  ]
  edge [
    source 15
    target 275
  ]
  edge [
    source 15
    target 276
  ]
  edge [
    source 15
    target 277
  ]
  edge [
    source 15
    target 278
  ]
  edge [
    source 15
    target 279
  ]
  edge [
    source 15
    target 818
  ]
  edge [
    source 15
    target 819
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 15
    target 65
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 65
  ]
  edge [
    source 16
    target 68
  ]
  edge [
    source 16
    target 59
  ]
  edge [
    source 16
    target 113
  ]
  edge [
    source 16
    target 80
  ]
  edge [
    source 16
    target 114
  ]
  edge [
    source 16
    target 280
  ]
  edge [
    source 16
    target 46
  ]
  edge [
    source 16
    target 102
  ]
  edge [
    source 16
    target 84
  ]
  edge [
    source 16
    target 85
  ]
  edge [
    source 16
    target 66
  ]
  edge [
    source 16
    target 106
  ]
  edge [
    source 16
    target 112
  ]
  edge [
    source 17
    target 35
  ]
  edge [
    source 17
    target 50
  ]
  edge [
    source 17
    target 66
  ]
  edge [
    source 17
    target 67
  ]
  edge [
    source 17
    target 281
  ]
  edge [
    source 17
    target 282
  ]
  edge [
    source 17
    target 283
  ]
  edge [
    source 17
    target 284
  ]
  edge [
    source 17
    target 285
  ]
  edge [
    source 17
    target 286
  ]
  edge [
    source 17
    target 21
  ]
  edge [
    source 17
    target 22
  ]
  edge [
    source 17
    target 40
  ]
  edge [
    source 17
    target 109
  ]
  edge [
    source 17
    target 79
  ]
  edge [
    source 17
    target 94
  ]
  edge [
    source 17
    target 818
  ]
  edge [
    source 17
    target 819
  ]
  edge [
    source 17
    target 823
  ]
  edge [
    source 17
    target 824
  ]
  edge [
    source 17
    target 825
  ]
  edge [
    source 17
    target 827
  ]
  edge [
    source 17
    target 828
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 94
  ]
  edge [
    source 18
    target 95
  ]
  edge [
    source 18
    target 287
  ]
  edge [
    source 18
    target 288
  ]
  edge [
    source 18
    target 289
  ]
  edge [
    source 18
    target 290
  ]
  edge [
    source 18
    target 291
  ]
  edge [
    source 18
    target 292
  ]
  edge [
    source 18
    target 293
  ]
  edge [
    source 18
    target 294
  ]
  edge [
    source 18
    target 295
  ]
  edge [
    source 18
    target 70
  ]
  edge [
    source 18
    target 80
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 293
  ]
  edge [
    source 19
    target 296
  ]
  edge [
    source 19
    target 297
  ]
  edge [
    source 19
    target 298
  ]
  edge [
    source 19
    target 77
  ]
  edge [
    source 19
    target 70
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 299
  ]
  edge [
    source 20
    target 300
  ]
  edge [
    source 20
    target 301
  ]
  edge [
    source 20
    target 57
  ]
  edge [
    source 20
    target 78
  ]
  edge [
    source 20
    target 66
  ]
  edge [
    source 20
    target 820
  ]
  edge [
    source 20
    target 821
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 820
  ]
  edge [
    source 21
    target 821
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 40
  ]
  edge [
    source 22
    target 109
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 302
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 303
  ]
  edge [
    source 24
    target 304
  ]
  edge [
    source 24
    target 305
  ]
  edge [
    source 24
    target 306
  ]
  edge [
    source 24
    target 307
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 308
  ]
  edge [
    source 25
    target 309
  ]
  edge [
    source 25
    target 310
  ]
  edge [
    source 25
    target 311
  ]
  edge [
    source 25
    target 312
  ]
  edge [
    source 25
    target 313
  ]
  edge [
    source 25
    target 45
  ]
  edge [
    source 25
    target 314
  ]
  edge [
    source 25
    target 54
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 46
  ]
  edge [
    source 26
    target 315
  ]
  edge [
    source 26
    target 316
  ]
  edge [
    source 26
    target 317
  ]
  edge [
    source 26
    target 318
  ]
  edge [
    source 26
    target 319
  ]
  edge [
    source 26
    target 822
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 47
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 81
  ]
  edge [
    source 28
    target 82
  ]
  edge [
    source 28
    target 32
  ]
  edge [
    source 28
    target 49
  ]
  edge [
    source 28
    target 96
  ]
  edge [
    source 28
    target 71
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 320
  ]
  edge [
    source 29
    target 321
  ]
  edge [
    source 29
    target 322
  ]
  edge [
    source 29
    target 222
  ]
  edge [
    source 29
    target 323
  ]
  edge [
    source 29
    target 214
  ]
  edge [
    source 29
    target 324
  ]
  edge [
    source 29
    target 325
  ]
  edge [
    source 29
    target 326
  ]
  edge [
    source 29
    target 327
  ]
  edge [
    source 29
    target 328
  ]
  edge [
    source 29
    target 329
  ]
  edge [
    source 29
    target 330
  ]
  edge [
    source 29
    target 331
  ]
  edge [
    source 29
    target 332
  ]
  edge [
    source 29
    target 333
  ]
  edge [
    source 29
    target 117
  ]
  edge [
    source 29
    target 334
  ]
  edge [
    source 29
    target 210
  ]
  edge [
    source 29
    target 335
  ]
  edge [
    source 29
    target 336
  ]
  edge [
    source 29
    target 337
  ]
  edge [
    source 29
    target 338
  ]
  edge [
    source 29
    target 339
  ]
  edge [
    source 29
    target 340
  ]
  edge [
    source 29
    target 341
  ]
  edge [
    source 29
    target 342
  ]
  edge [
    source 29
    target 343
  ]
  edge [
    source 29
    target 344
  ]
  edge [
    source 29
    target 345
  ]
  edge [
    source 29
    target 346
  ]
  edge [
    source 29
    target 347
  ]
  edge [
    source 29
    target 348
  ]
  edge [
    source 29
    target 349
  ]
  edge [
    source 29
    target 350
  ]
  edge [
    source 29
    target 351
  ]
  edge [
    source 29
    target 352
  ]
  edge [
    source 29
    target 248
  ]
  edge [
    source 29
    target 353
  ]
  edge [
    source 29
    target 354
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 99
  ]
  edge [
    source 30
    target 100
  ]
  edge [
    source 30
    target 355
  ]
  edge [
    source 30
    target 50
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 51
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 49
  ]
  edge [
    source 32
    target 96
  ]
  edge [
    source 32
    target 71
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 356
  ]
  edge [
    source 34
    target 357
  ]
  edge [
    source 34
    target 358
  ]
  edge [
    source 34
    target 359
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 49
  ]
  edge [
    source 36
    target 360
  ]
  edge [
    source 36
    target 115
  ]
  edge [
    source 36
    target 361
  ]
  edge [
    source 36
    target 362
  ]
  edge [
    source 36
    target 363
  ]
  edge [
    source 36
    target 364
  ]
  edge [
    source 36
    target 365
  ]
  edge [
    source 36
    target 366
  ]
  edge [
    source 36
    target 367
  ]
  edge [
    source 36
    target 368
  ]
  edge [
    source 36
    target 369
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 370
  ]
  edge [
    source 38
    target 371
  ]
  edge [
    source 38
    target 372
  ]
  edge [
    source 38
    target 373
  ]
  edge [
    source 38
    target 374
  ]
  edge [
    source 38
    target 375
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 376
  ]
  edge [
    source 39
    target 377
  ]
  edge [
    source 39
    target 378
  ]
  edge [
    source 39
    target 379
  ]
  edge [
    source 39
    target 380
  ]
  edge [
    source 39
    target 381
  ]
  edge [
    source 39
    target 382
  ]
  edge [
    source 39
    target 383
  ]
  edge [
    source 39
    target 384
  ]
  edge [
    source 39
    target 385
  ]
  edge [
    source 39
    target 386
  ]
  edge [
    source 39
    target 387
  ]
  edge [
    source 39
    target 388
  ]
  edge [
    source 39
    target 389
  ]
  edge [
    source 39
    target 390
  ]
  edge [
    source 39
    target 391
  ]
  edge [
    source 39
    target 392
  ]
  edge [
    source 39
    target 393
  ]
  edge [
    source 39
    target 394
  ]
  edge [
    source 39
    target 395
  ]
  edge [
    source 39
    target 396
  ]
  edge [
    source 39
    target 397
  ]
  edge [
    source 39
    target 398
  ]
  edge [
    source 39
    target 399
  ]
  edge [
    source 39
    target 400
  ]
  edge [
    source 39
    target 401
  ]
  edge [
    source 39
    target 402
  ]
  edge [
    source 39
    target 403
  ]
  edge [
    source 39
    target 404
  ]
  edge [
    source 39
    target 405
  ]
  edge [
    source 39
    target 406
  ]
  edge [
    source 39
    target 407
  ]
  edge [
    source 39
    target 408
  ]
  edge [
    source 39
    target 409
  ]
  edge [
    source 39
    target 410
  ]
  edge [
    source 39
    target 411
  ]
  edge [
    source 39
    target 412
  ]
  edge [
    source 39
    target 56
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 58
  ]
  edge [
    source 40
    target 59
  ]
  edge [
    source 40
    target 88
  ]
  edge [
    source 40
    target 89
  ]
  edge [
    source 40
    target 109
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 413
  ]
  edge [
    source 41
    target 414
  ]
  edge [
    source 41
    target 415
  ]
  edge [
    source 41
    target 416
  ]
  edge [
    source 41
    target 201
  ]
  edge [
    source 41
    target 47
  ]
  edge [
    source 42
    target 417
  ]
  edge [
    source 42
    target 418
  ]
  edge [
    source 42
    target 419
  ]
  edge [
    source 42
    target 420
  ]
  edge [
    source 42
    target 421
  ]
  edge [
    source 42
    target 422
  ]
  edge [
    source 42
    target 423
  ]
  edge [
    source 42
    target 424
  ]
  edge [
    source 42
    target 425
  ]
  edge [
    source 42
    target 426
  ]
  edge [
    source 42
    target 427
  ]
  edge [
    source 42
    target 428
  ]
  edge [
    source 42
    target 429
  ]
  edge [
    source 42
    target 430
  ]
  edge [
    source 42
    target 431
  ]
  edge [
    source 42
    target 432
  ]
  edge [
    source 42
    target 433
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 434
  ]
  edge [
    source 43
    target 435
  ]
  edge [
    source 43
    target 436
  ]
  edge [
    source 43
    target 437
  ]
  edge [
    source 43
    target 60
  ]
  edge [
    source 43
    target 438
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 68
  ]
  edge [
    source 44
    target 69
  ]
  edge [
    source 44
    target 335
  ]
  edge [
    source 44
    target 439
  ]
  edge [
    source 44
    target 440
  ]
  edge [
    source 44
    target 441
  ]
  edge [
    source 44
    target 442
  ]
  edge [
    source 44
    target 64
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 443
  ]
  edge [
    source 45
    target 311
  ]
  edge [
    source 45
    target 444
  ]
  edge [
    source 45
    target 445
  ]
  edge [
    source 45
    target 446
  ]
  edge [
    source 45
    target 447
  ]
  edge [
    source 45
    target 448
  ]
  edge [
    source 46
    target 449
  ]
  edge [
    source 46
    target 102
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 450
  ]
  edge [
    source 47
    target 451
  ]
  edge [
    source 47
    target 311
  ]
  edge [
    source 47
    target 452
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 49
    target 453
  ]
  edge [
    source 49
    target 454
  ]
  edge [
    source 49
    target 96
  ]
  edge [
    source 49
    target 71
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 293
  ]
  edge [
    source 50
    target 455
  ]
  edge [
    source 50
    target 456
  ]
  edge [
    source 50
    target 204
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 457
  ]
  edge [
    source 51
    target 458
  ]
  edge [
    source 51
    target 459
  ]
  edge [
    source 51
    target 460
  ]
  edge [
    source 51
    target 461
  ]
  edge [
    source 51
    target 462
  ]
  edge [
    source 51
    target 463
  ]
  edge [
    source 51
    target 464
  ]
  edge [
    source 51
    target 465
  ]
  edge [
    source 51
    target 466
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 467
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 468
  ]
  edge [
    source 55
    target 469
  ]
  edge [
    source 55
    target 470
  ]
  edge [
    source 55
    target 471
  ]
  edge [
    source 55
    target 472
  ]
  edge [
    source 55
    target 473
  ]
  edge [
    source 55
    target 474
  ]
  edge [
    source 55
    target 475
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 476
  ]
  edge [
    source 57
    target 477
  ]
  edge [
    source 57
    target 478
  ]
  edge [
    source 57
    target 479
  ]
  edge [
    source 57
    target 480
  ]
  edge [
    source 57
    target 481
  ]
  edge [
    source 57
    target 482
  ]
  edge [
    source 57
    target 483
  ]
  edge [
    source 57
    target 484
  ]
  edge [
    source 57
    target 293
  ]
  edge [
    source 57
    target 201
  ]
  edge [
    source 57
    target 485
  ]
  edge [
    source 57
    target 78
  ]
  edge [
    source 57
    target 66
  ]
  edge [
    source 58
    target 486
  ]
  edge [
    source 58
    target 466
  ]
  edge [
    source 58
    target 83
  ]
  edge [
    source 58
    target 95
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 75
  ]
  edge [
    source 59
    target 76
  ]
  edge [
    source 59
    target 107
  ]
  edge [
    source 59
    target 110
  ]
  edge [
    source 59
    target 487
  ]
  edge [
    source 59
    target 488
  ]
  edge [
    source 59
    target 489
  ]
  edge [
    source 59
    target 490
  ]
  edge [
    source 59
    target 491
  ]
  edge [
    source 59
    target 492
  ]
  edge [
    source 59
    target 493
  ]
  edge [
    source 59
    target 494
  ]
  edge [
    source 59
    target 495
  ]
  edge [
    source 59
    target 496
  ]
  edge [
    source 59
    target 497
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 498
  ]
  edge [
    source 60
    target 499
  ]
  edge [
    source 60
    target 500
  ]
  edge [
    source 60
    target 444
  ]
  edge [
    source 60
    target 501
  ]
  edge [
    source 60
    target 502
  ]
  edge [
    source 60
    target 503
  ]
  edge [
    source 60
    target 504
  ]
  edge [
    source 60
    target 466
  ]
  edge [
    source 60
    target 505
  ]
  edge [
    source 60
    target 506
  ]
  edge [
    source 60
    target 507
  ]
  edge [
    source 60
    target 508
  ]
  edge [
    source 60
    target 509
  ]
  edge [
    source 60
    target 510
  ]
  edge [
    source 60
    target 511
  ]
  edge [
    source 60
    target 512
  ]
  edge [
    source 60
    target 513
  ]
  edge [
    source 60
    target 514
  ]
  edge [
    source 60
    target 515
  ]
  edge [
    source 60
    target 516
  ]
  edge [
    source 60
    target 517
  ]
  edge [
    source 60
    target 518
  ]
  edge [
    source 60
    target 519
  ]
  edge [
    source 60
    target 520
  ]
  edge [
    source 60
    target 521
  ]
  edge [
    source 60
    target 522
  ]
  edge [
    source 60
    target 523
  ]
  edge [
    source 60
    target 524
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 525
  ]
  edge [
    source 61
    target 526
  ]
  edge [
    source 61
    target 527
  ]
  edge [
    source 61
    target 528
  ]
  edge [
    source 61
    target 529
  ]
  edge [
    source 61
    target 530
  ]
  edge [
    source 61
    target 531
  ]
  edge [
    source 61
    target 532
  ]
  edge [
    source 61
    target 533
  ]
  edge [
    source 61
    target 534
  ]
  edge [
    source 61
    target 535
  ]
  edge [
    source 61
    target 536
  ]
  edge [
    source 61
    target 537
  ]
  edge [
    source 61
    target 538
  ]
  edge [
    source 61
    target 539
  ]
  edge [
    source 61
    target 540
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 541
  ]
  edge [
    source 62
    target 542
  ]
  edge [
    source 62
    target 543
  ]
  edge [
    source 62
    target 544
  ]
  edge [
    source 62
    target 545
  ]
  edge [
    source 62
    target 546
  ]
  edge [
    source 62
    target 547
  ]
  edge [
    source 62
    target 548
  ]
  edge [
    source 62
    target 130
  ]
  edge [
    source 62
    target 549
  ]
  edge [
    source 62
    target 550
  ]
  edge [
    source 62
    target 551
  ]
  edge [
    source 62
    target 552
  ]
  edge [
    source 62
    target 553
  ]
  edge [
    source 62
    target 554
  ]
  edge [
    source 62
    target 555
  ]
  edge [
    source 62
    target 556
  ]
  edge [
    source 62
    target 557
  ]
  edge [
    source 62
    target 272
  ]
  edge [
    source 62
    target 558
  ]
  edge [
    source 62
    target 168
  ]
  edge [
    source 62
    target 559
  ]
  edge [
    source 62
    target 560
  ]
  edge [
    source 62
    target 561
  ]
  edge [
    source 62
    target 112
  ]
  edge [
    source 63
    target 562
  ]
  edge [
    source 63
    target 563
  ]
  edge [
    source 63
    target 479
  ]
  edge [
    source 63
    target 564
  ]
  edge [
    source 63
    target 565
  ]
  edge [
    source 63
    target 566
  ]
  edge [
    source 63
    target 567
  ]
  edge [
    source 63
    target 568
  ]
  edge [
    source 63
    target 288
  ]
  edge [
    source 63
    target 569
  ]
  edge [
    source 63
    target 570
  ]
  edge [
    source 63
    target 571
  ]
  edge [
    source 63
    target 572
  ]
  edge [
    source 63
    target 573
  ]
  edge [
    source 63
    target 574
  ]
  edge [
    source 63
    target 575
  ]
  edge [
    source 63
    target 576
  ]
  edge [
    source 63
    target 577
  ]
  edge [
    source 63
    target 293
  ]
  edge [
    source 63
    target 204
  ]
  edge [
    source 63
    target 578
  ]
  edge [
    source 63
    target 579
  ]
  edge [
    source 63
    target 580
  ]
  edge [
    source 63
    target 581
  ]
  edge [
    source 63
    target 582
  ]
  edge [
    source 63
    target 583
  ]
  edge [
    source 63
    target 584
  ]
  edge [
    source 63
    target 585
  ]
  edge [
    source 63
    target 586
  ]
  edge [
    source 63
    target 587
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 588
  ]
  edge [
    source 64
    target 589
  ]
  edge [
    source 64
    target 590
  ]
  edge [
    source 65
    target 591
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 66
    target 80
  ]
  edge [
    source 66
    target 92
  ]
  edge [
    source 66
    target 592
  ]
  edge [
    source 66
    target 593
  ]
  edge [
    source 66
    target 594
  ]
  edge [
    source 66
    target 595
  ]
  edge [
    source 66
    target 596
  ]
  edge [
    source 66
    target 597
  ]
  edge [
    source 66
    target 598
  ]
  edge [
    source 66
    target 599
  ]
  edge [
    source 66
    target 600
  ]
  edge [
    source 66
    target 601
  ]
  edge [
    source 66
    target 602
  ]
  edge [
    source 66
    target 78
  ]
  edge [
    source 66
    target 85
  ]
  edge [
    source 66
    target 825
  ]
  edge [
    source 67
    target 70
  ]
  edge [
    source 67
    target 99
  ]
  edge [
    source 67
    target 603
  ]
  edge [
    source 67
    target 604
  ]
  edge [
    source 67
    target 605
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 69
    target 606
  ]
  edge [
    source 69
    target 607
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 81
  ]
  edge [
    source 70
    target 608
  ]
  edge [
    source 70
    target 456
  ]
  edge [
    source 70
    target 609
  ]
  edge [
    source 70
    target 610
  ]
  edge [
    source 70
    target 77
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 71
    target 92
  ]
  edge [
    source 71
    target 93
  ]
  edge [
    source 71
    target 96
  ]
  edge [
    source 71
    target 97
  ]
  edge [
    source 71
    target 611
  ]
  edge [
    source 71
    target 612
  ]
  edge [
    source 71
    target 613
  ]
  edge [
    source 71
    target 344
  ]
  edge [
    source 71
    target 614
  ]
  edge [
    source 71
    target 615
  ]
  edge [
    source 71
    target 616
  ]
  edge [
    source 71
    target 617
  ]
  edge [
    source 71
    target 618
  ]
  edge [
    source 72
    target 73
  ]
  edge [
    source 72
    target 84
  ]
  edge [
    source 72
    target 85
  ]
  edge [
    source 72
    target 97
  ]
  edge [
    source 72
    target 98
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 74
    target 75
  ]
  edge [
    source 75
    target 619
  ]
  edge [
    source 76
    target 77
  ]
  edge [
    source 77
    target 78
  ]
  edge [
    source 78
    target 79
  ]
  edge [
    source 78
    target 529
  ]
  edge [
    source 78
    target 620
  ]
  edge [
    source 78
    target 533
  ]
  edge [
    source 78
    target 621
  ]
  edge [
    source 78
    target 622
  ]
  edge [
    source 78
    target 623
  ]
  edge [
    source 78
    target 624
  ]
  edge [
    source 78
    target 625
  ]
  edge [
    source 78
    target 626
  ]
  edge [
    source 78
    target 627
  ]
  edge [
    source 78
    target 628
  ]
  edge [
    source 78
    target 629
  ]
  edge [
    source 78
    target 630
  ]
  edge [
    source 78
    target 631
  ]
  edge [
    source 79
    target 80
  ]
  edge [
    source 79
    target 94
  ]
  edge [
    source 80
    target 114
  ]
  edge [
    source 80
    target 632
  ]
  edge [
    source 80
    target 633
  ]
  edge [
    source 81
    target 634
  ]
  edge [
    source 81
    target 635
  ]
  edge [
    source 81
    target 636
  ]
  edge [
    source 81
    target 637
  ]
  edge [
    source 81
    target 638
  ]
  edge [
    source 81
    target 639
  ]
  edge [
    source 81
    target 640
  ]
  edge [
    source 81
    target 641
  ]
  edge [
    source 81
    target 642
  ]
  edge [
    source 81
    target 643
  ]
  edge [
    source 81
    target 644
  ]
  edge [
    source 81
    target 473
  ]
  edge [
    source 81
    target 645
  ]
  edge [
    source 81
    target 646
  ]
  edge [
    source 81
    target 647
  ]
  edge [
    source 81
    target 648
  ]
  edge [
    source 81
    target 649
  ]
  edge [
    source 82
    target 83
  ]
  edge [
    source 82
    target 650
  ]
  edge [
    source 83
    target 84
  ]
  edge [
    source 83
    target 651
  ]
  edge [
    source 83
    target 652
  ]
  edge [
    source 83
    target 443
  ]
  edge [
    source 83
    target 444
  ]
  edge [
    source 83
    target 653
  ]
  edge [
    source 83
    target 95
  ]
  edge [
    source 84
    target 344
  ]
  edge [
    source 84
    target 654
  ]
  edge [
    source 84
    target 655
  ]
  edge [
    source 84
    target 656
  ]
  edge [
    source 84
    target 657
  ]
  edge [
    source 84
    target 658
  ]
  edge [
    source 84
    target 618
  ]
  edge [
    source 84
    target 112
  ]
  edge [
    source 85
    target 86
  ]
  edge [
    source 86
    target 87
  ]
  edge [
    source 86
    target 659
  ]
  edge [
    source 86
    target 660
  ]
  edge [
    source 86
    target 661
  ]
  edge [
    source 86
    target 662
  ]
  edge [
    source 86
    target 663
  ]
  edge [
    source 86
    target 664
  ]
  edge [
    source 86
    target 665
  ]
  edge [
    source 86
    target 666
  ]
  edge [
    source 86
    target 667
  ]
  edge [
    source 86
    target 668
  ]
  edge [
    source 87
    target 88
  ]
  edge [
    source 87
    target 669
  ]
  edge [
    source 87
    target 670
  ]
  edge [
    source 87
    target 671
  ]
  edge [
    source 87
    target 672
  ]
  edge [
    source 87
    target 673
  ]
  edge [
    source 87
    target 674
  ]
  edge [
    source 87
    target 675
  ]
  edge [
    source 87
    target 676
  ]
  edge [
    source 87
    target 677
  ]
  edge [
    source 88
    target 678
  ]
  edge [
    source 88
    target 679
  ]
  edge [
    source 88
    target 680
  ]
  edge [
    source 89
    target 90
  ]
  edge [
    source 89
    target 681
  ]
  edge [
    source 89
    target 682
  ]
  edge [
    source 89
    target 683
  ]
  edge [
    source 89
    target 684
  ]
  edge [
    source 89
    target 685
  ]
  edge [
    source 89
    target 686
  ]
  edge [
    source 90
    target 91
  ]
  edge [
    source 91
    target 92
  ]
  edge [
    source 91
    target 95
  ]
  edge [
    source 91
    target 96
  ]
  edge [
    source 92
    target 98
  ]
  edge [
    source 93
    target 94
  ]
  edge [
    source 94
    target 687
  ]
  edge [
    source 94
    target 688
  ]
  edge [
    source 94
    target 278
  ]
  edge [
    source 94
    target 689
  ]
  edge [
    source 95
    target 690
  ]
  edge [
    source 95
    target 691
  ]
  edge [
    source 95
    target 692
  ]
  edge [
    source 95
    target 693
  ]
  edge [
    source 95
    target 694
  ]
  edge [
    source 95
    target 695
  ]
  edge [
    source 95
    target 201
  ]
  edge [
    source 95
    target 696
  ]
  edge [
    source 95
    target 697
  ]
  edge [
    source 97
    target 698
  ]
  edge [
    source 98
    target 434
  ]
  edge [
    source 98
    target 699
  ]
  edge [
    source 99
    target 700
  ]
  edge [
    source 99
    target 701
  ]
  edge [
    source 100
    target 101
  ]
  edge [
    source 100
    target 826
  ]
  edge [
    source 101
    target 102
  ]
  edge [
    source 101
    target 702
  ]
  edge [
    source 101
    target 703
  ]
  edge [
    source 101
    target 704
  ]
  edge [
    source 101
    target 705
  ]
  edge [
    source 101
    target 706
  ]
  edge [
    source 101
    target 633
  ]
  edge [
    source 102
    target 103
  ]
  edge [
    source 103
    target 104
  ]
  edge [
    source 103
    target 707
  ]
  edge [
    source 103
    target 708
  ]
  edge [
    source 103
    target 709
  ]
  edge [
    source 104
    target 105
  ]
  edge [
    source 104
    target 710
  ]
  edge [
    source 105
    target 106
  ]
  edge [
    source 105
    target 711
  ]
  edge [
    source 105
    target 712
  ]
  edge [
    source 105
    target 713
  ]
  edge [
    source 105
    target 714
  ]
  edge [
    source 105
    target 715
  ]
  edge [
    source 105
    target 716
  ]
  edge [
    source 105
    target 110
  ]
  edge [
    source 106
    target 717
  ]
  edge [
    source 106
    target 718
  ]
  edge [
    source 106
    target 719
  ]
  edge [
    source 107
    target 108
  ]
  edge [
    source 107
    target 720
  ]
  edge [
    source 107
    target 721
  ]
  edge [
    source 107
    target 722
  ]
  edge [
    source 107
    target 723
  ]
  edge [
    source 107
    target 724
  ]
  edge [
    source 107
    target 608
  ]
  edge [
    source 107
    target 725
  ]
  edge [
    source 107
    target 193
  ]
  edge [
    source 107
    target 726
  ]
  edge [
    source 107
    target 727
  ]
  edge [
    source 107
    target 728
  ]
  edge [
    source 108
    target 109
  ]
  edge [
    source 108
    target 729
  ]
  edge [
    source 108
    target 730
  ]
  edge [
    source 108
    target 731
  ]
  edge [
    source 108
    target 732
  ]
  edge [
    source 108
    target 733
  ]
  edge [
    source 108
    target 734
  ]
  edge [
    source 108
    target 735
  ]
  edge [
    source 108
    target 736
  ]
  edge [
    source 108
    target 737
  ]
  edge [
    source 108
    target 738
  ]
  edge [
    source 108
    target 739
  ]
  edge [
    source 108
    target 740
  ]
  edge [
    source 108
    target 741
  ]
  edge [
    source 108
    target 742
  ]
  edge [
    source 108
    target 743
  ]
  edge [
    source 108
    target 744
  ]
  edge [
    source 108
    target 745
  ]
  edge [
    source 108
    target 746
  ]
  edge [
    source 108
    target 747
  ]
  edge [
    source 108
    target 748
  ]
  edge [
    source 108
    target 613
  ]
  edge [
    source 108
    target 749
  ]
  edge [
    source 108
    target 750
  ]
  edge [
    source 108
    target 751
  ]
  edge [
    source 108
    target 752
  ]
  edge [
    source 108
    target 344
  ]
  edge [
    source 108
    target 753
  ]
  edge [
    source 108
    target 754
  ]
  edge [
    source 108
    target 755
  ]
  edge [
    source 108
    target 756
  ]
  edge [
    source 108
    target 757
  ]
  edge [
    source 108
    target 758
  ]
  edge [
    source 108
    target 759
  ]
  edge [
    source 108
    target 412
  ]
  edge [
    source 108
    target 113
  ]
  edge [
    source 109
    target 760
  ]
  edge [
    source 109
    target 761
  ]
  edge [
    source 109
    target 762
  ]
  edge [
    source 109
    target 763
  ]
  edge [
    source 109
    target 764
  ]
  edge [
    source 110
    target 111
  ]
  edge [
    source 110
    target 444
  ]
  edge [
    source 110
    target 765
  ]
  edge [
    source 111
    target 112
  ]
  edge [
    source 112
    target 407
  ]
  edge [
    source 112
    target 766
  ]
  edge [
    source 112
    target 767
  ]
  edge [
    source 112
    target 768
  ]
  edge [
    source 112
    target 769
  ]
  edge [
    source 112
    target 770
  ]
  edge [
    source 112
    target 771
  ]
  edge [
    source 112
    target 240
  ]
  edge [
    source 112
    target 772
  ]
  edge [
    source 112
    target 665
  ]
  edge [
    source 112
    target 773
  ]
  edge [
    source 112
    target 774
  ]
  edge [
    source 112
    target 775
  ]
  edge [
    source 112
    target 776
  ]
  edge [
    source 113
    target 777
  ]
  edge [
    source 113
    target 778
  ]
  edge [
    source 113
    target 779
  ]
  edge [
    source 113
    target 780
  ]
  edge [
    source 113
    target 781
  ]
  edge [
    source 113
    target 782
  ]
  edge [
    source 113
    target 584
  ]
  edge [
    source 113
    target 783
  ]
  edge [
    source 113
    target 272
  ]
  edge [
    source 113
    target 784
  ]
  edge [
    source 114
    target 115
  ]
  edge [
    source 114
    target 785
  ]
  edge [
    source 114
    target 786
  ]
  edge [
    source 114
    target 787
  ]
  edge [
    source 114
    target 788
  ]
  edge [
    source 114
    target 789
  ]
  edge [
    source 114
    target 790
  ]
  edge [
    source 114
    target 607
  ]
  edge [
    source 114
    target 791
  ]
  edge [
    source 114
    target 792
  ]
  edge [
    source 114
    target 793
  ]
  edge [
    source 114
    target 794
  ]
  edge [
    source 114
    target 795
  ]
  edge [
    source 114
    target 796
  ]
  edge [
    source 114
    target 797
  ]
  edge [
    source 114
    target 798
  ]
  edge [
    source 114
    target 706
  ]
  edge [
    source 114
    target 799
  ]
  edge [
    source 114
    target 800
  ]
  edge [
    source 114
    target 801
  ]
  edge [
    source 114
    target 802
  ]
  edge [
    source 114
    target 803
  ]
  edge [
    source 114
    target 804
  ]
  edge [
    source 114
    target 805
  ]
  edge [
    source 114
    target 806
  ]
  edge [
    source 114
    target 807
  ]
  edge [
    source 114
    target 808
  ]
  edge [
    source 114
    target 809
  ]
  edge [
    source 114
    target 810
  ]
  edge [
    source 114
    target 811
  ]
  edge [
    source 114
    target 812
  ]
  edge [
    source 114
    target 813
  ]
  edge [
    source 114
    target 814
  ]
  edge [
    source 114
    target 815
  ]
  edge [
    source 114
    target 816
  ]
  edge [
    source 114
    target 817
  ]
  edge [
    source 818
    target 819
  ]
  edge [
    source 819
    target 824
  ]
  edge [
    source 819
    target 827
  ]
  edge [
    source 819
    target 828
  ]
  edge [
    source 820
    target 821
  ]
  edge [
    source 827
    target 828
  ]
]
