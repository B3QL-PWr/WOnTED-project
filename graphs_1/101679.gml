graph [
  maxDegree 28
  minDegree 1
  meanDegree 2.0707070707070705
  density 0.01051120340460442
  graphCliqueNumber 2
  node [
    id 0
    label "zasadniczy"
    origin "text"
  ]
  node [
    id 1
    label "pytanie"
    origin "text"
  ]
  node [
    id 2
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 3
    label "nale&#380;e&#263;"
    origin "text"
  ]
  node [
    id 4
    label "zada&#263;"
    origin "text"
  ]
  node [
    id 5
    label "dotyczy&#263;"
    origin "text"
  ]
  node [
    id 6
    label "podstawa"
    origin "text"
  ]
  node [
    id 7
    label "autor"
    origin "text"
  ]
  node [
    id 8
    label "sprawozdanie"
    origin "text"
  ]
  node [
    id 9
    label "spos&#243;b"
    origin "text"
  ]
  node [
    id 10
    label "niedopuszczalny"
    origin "text"
  ]
  node [
    id 11
    label "interpretowa&#263;"
    origin "text"
  ]
  node [
    id 12
    label "stanowisko"
    origin "text"
  ]
  node [
    id 13
    label "minister"
    origin "text"
  ]
  node [
    id 14
    label "gilowska"
    origin "text"
  ]
  node [
    id 15
    label "tre&#347;&#263;"
    origin "text"
  ]
  node [
    id 16
    label "poda&#263;"
    origin "text"
  ]
  node [
    id 17
    label "&#380;aden"
    origin "text"
  ]
  node [
    id 18
    label "dow&#243;d"
    origin "text"
  ]
  node [
    id 19
    label "taka"
    origin "text"
  ]
  node [
    id 20
    label "interpretacja"
    origin "text"
  ]
  node [
    id 21
    label "uprawdopodobni&#263;"
    origin "text"
  ]
  node [
    id 22
    label "miara"
    origin "text"
  ]
  node [
    id 23
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 24
    label "uzna&#263;"
    origin "text"
  ]
  node [
    id 25
    label "wiarygodny"
    origin "text"
  ]
  node [
    id 26
    label "og&#243;lny"
  ]
  node [
    id 27
    label "g&#322;&#243;wny"
  ]
  node [
    id 28
    label "zasadniczo"
  ]
  node [
    id 29
    label "surowy"
  ]
  node [
    id 30
    label "sprawa"
  ]
  node [
    id 31
    label "zadanie"
  ]
  node [
    id 32
    label "wypowied&#378;"
  ]
  node [
    id 33
    label "problemat"
  ]
  node [
    id 34
    label "rozpytywanie"
  ]
  node [
    id 35
    label "sprawdzian"
  ]
  node [
    id 36
    label "przes&#322;uchiwanie"
  ]
  node [
    id 37
    label "wypytanie"
  ]
  node [
    id 38
    label "zwracanie_si&#281;"
  ]
  node [
    id 39
    label "wypowiedzenie"
  ]
  node [
    id 40
    label "wywo&#322;ywanie"
  ]
  node [
    id 41
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 42
    label "problematyka"
  ]
  node [
    id 43
    label "question"
  ]
  node [
    id 44
    label "sprawdzanie"
  ]
  node [
    id 45
    label "odpowiadanie"
  ]
  node [
    id 46
    label "survey"
  ]
  node [
    id 47
    label "odpowiada&#263;"
  ]
  node [
    id 48
    label "egzaminowanie"
  ]
  node [
    id 49
    label "trza"
  ]
  node [
    id 50
    label "uczestniczy&#263;"
  ]
  node [
    id 51
    label "zalicza&#263;_si&#281;"
  ]
  node [
    id 52
    label "para"
  ]
  node [
    id 53
    label "necessity"
  ]
  node [
    id 54
    label "d&#378;wign&#261;&#263;"
  ]
  node [
    id 55
    label "zaj&#261;&#263;"
  ]
  node [
    id 56
    label "zaszkodzi&#263;"
  ]
  node [
    id 57
    label "distribute"
  ]
  node [
    id 58
    label "za&#322;o&#380;y&#263;"
  ]
  node [
    id 59
    label "nakarmi&#263;"
  ]
  node [
    id 60
    label "deal"
  ]
  node [
    id 61
    label "put"
  ]
  node [
    id 62
    label "set"
  ]
  node [
    id 63
    label "zobowi&#261;za&#263;"
  ]
  node [
    id 64
    label "bargain"
  ]
  node [
    id 65
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 66
    label "tycze&#263;"
  ]
  node [
    id 67
    label "podstawowy"
  ]
  node [
    id 68
    label "strategia"
  ]
  node [
    id 69
    label "pot&#281;ga"
  ]
  node [
    id 70
    label "zasadzenie"
  ]
  node [
    id 71
    label "przedmiot"
  ]
  node [
    id 72
    label "za&#322;o&#380;enie"
  ]
  node [
    id 73
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 74
    label "&#347;ciana"
  ]
  node [
    id 75
    label "documentation"
  ]
  node [
    id 76
    label "dzieci&#281;ctwo"
  ]
  node [
    id 77
    label "pomys&#322;"
  ]
  node [
    id 78
    label "bok"
  ]
  node [
    id 79
    label "d&#243;&#322;"
  ]
  node [
    id 80
    label "punkt_odniesienia"
  ]
  node [
    id 81
    label "column"
  ]
  node [
    id 82
    label "zasadzi&#263;"
  ]
  node [
    id 83
    label "background"
  ]
  node [
    id 84
    label "pomys&#322;odawca"
  ]
  node [
    id 85
    label "kszta&#322;ciciel"
  ]
  node [
    id 86
    label "tworzyciel"
  ]
  node [
    id 87
    label "&#347;w"
  ]
  node [
    id 88
    label "wykonawca"
  ]
  node [
    id 89
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 90
    label "message"
  ]
  node [
    id 91
    label "korespondent"
  ]
  node [
    id 92
    label "sprawko"
  ]
  node [
    id 93
    label "model"
  ]
  node [
    id 94
    label "zbi&#243;r"
  ]
  node [
    id 95
    label "tryb"
  ]
  node [
    id 96
    label "narz&#281;dzie"
  ]
  node [
    id 97
    label "nature"
  ]
  node [
    id 98
    label "niedopuszczalnie"
  ]
  node [
    id 99
    label "niezgodny"
  ]
  node [
    id 100
    label "wykonywa&#263;"
  ]
  node [
    id 101
    label "analizowa&#263;"
  ]
  node [
    id 102
    label "give"
  ]
  node [
    id 103
    label "rozumie&#263;"
  ]
  node [
    id 104
    label "odbiera&#263;"
  ]
  node [
    id 105
    label "gloss"
  ]
  node [
    id 106
    label "postawi&#263;"
  ]
  node [
    id 107
    label "miejsce"
  ]
  node [
    id 108
    label "awansowanie"
  ]
  node [
    id 109
    label "po&#322;o&#380;enie"
  ]
  node [
    id 110
    label "awansowa&#263;"
  ]
  node [
    id 111
    label "uprawianie"
  ]
  node [
    id 112
    label "powierzanie"
  ]
  node [
    id 113
    label "punkt"
  ]
  node [
    id 114
    label "pogl&#261;d"
  ]
  node [
    id 115
    label "wojsko"
  ]
  node [
    id 116
    label "praca"
  ]
  node [
    id 117
    label "wakowa&#263;"
  ]
  node [
    id 118
    label "stawia&#263;"
  ]
  node [
    id 119
    label "Goebbels"
  ]
  node [
    id 120
    label "Sto&#322;ypin"
  ]
  node [
    id 121
    label "rz&#261;d"
  ]
  node [
    id 122
    label "dostojnik"
  ]
  node [
    id 123
    label "zawarto&#347;&#263;"
  ]
  node [
    id 124
    label "temat"
  ]
  node [
    id 125
    label "istota"
  ]
  node [
    id 126
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 127
    label "informacja"
  ]
  node [
    id 128
    label "tenis"
  ]
  node [
    id 129
    label "da&#263;"
  ]
  node [
    id 130
    label "siatk&#243;wka"
  ]
  node [
    id 131
    label "introduce"
  ]
  node [
    id 132
    label "jedzenie"
  ]
  node [
    id 133
    label "zaserwowa&#263;"
  ]
  node [
    id 134
    label "ustawi&#263;"
  ]
  node [
    id 135
    label "zagra&#263;"
  ]
  node [
    id 136
    label "supply"
  ]
  node [
    id 137
    label "nafaszerowa&#263;"
  ]
  node [
    id 138
    label "poinformowa&#263;"
  ]
  node [
    id 139
    label "nijaki"
  ]
  node [
    id 140
    label "dokument"
  ]
  node [
    id 141
    label "forsing"
  ]
  node [
    id 142
    label "certificate"
  ]
  node [
    id 143
    label "rewizja"
  ]
  node [
    id 144
    label "argument"
  ]
  node [
    id 145
    label "act"
  ]
  node [
    id 146
    label "rzecz"
  ]
  node [
    id 147
    label "&#347;rodek"
  ]
  node [
    id 148
    label "uzasadnienie"
  ]
  node [
    id 149
    label "Bangladesz"
  ]
  node [
    id 150
    label "jednostka_monetarna"
  ]
  node [
    id 151
    label "wytw&#243;r"
  ]
  node [
    id 152
    label "kontekst"
  ]
  node [
    id 153
    label "obja&#347;nienie"
  ]
  node [
    id 154
    label "wypracowanie"
  ]
  node [
    id 155
    label "hermeneutyka"
  ]
  node [
    id 156
    label "interpretation"
  ]
  node [
    id 157
    label "explanation"
  ]
  node [
    id 158
    label "realizacja"
  ]
  node [
    id 159
    label "legalize"
  ]
  node [
    id 160
    label "spowodowa&#263;"
  ]
  node [
    id 161
    label "infimum"
  ]
  node [
    id 162
    label "dymensja"
  ]
  node [
    id 163
    label "proportion"
  ]
  node [
    id 164
    label "skala"
  ]
  node [
    id 165
    label "supremum"
  ]
  node [
    id 166
    label "granica"
  ]
  node [
    id 167
    label "ilo&#347;&#263;"
  ]
  node [
    id 168
    label "funkcja"
  ]
  node [
    id 169
    label "przeliczenie"
  ]
  node [
    id 170
    label "przeliczanie"
  ]
  node [
    id 171
    label "rzut"
  ]
  node [
    id 172
    label "przelicza&#263;"
  ]
  node [
    id 173
    label "wielko&#347;&#263;"
  ]
  node [
    id 174
    label "continence"
  ]
  node [
    id 175
    label "odwiedziny"
  ]
  node [
    id 176
    label "cecha"
  ]
  node [
    id 177
    label "liczba"
  ]
  node [
    id 178
    label "poj&#281;cie"
  ]
  node [
    id 179
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 180
    label "warunek_lokalowy"
  ]
  node [
    id 181
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 182
    label "zakres"
  ]
  node [
    id 183
    label "matematyka"
  ]
  node [
    id 184
    label "przeliczy&#263;"
  ]
  node [
    id 185
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 186
    label "jednostka"
  ]
  node [
    id 187
    label "free"
  ]
  node [
    id 188
    label "rede"
  ]
  node [
    id 189
    label "assent"
  ]
  node [
    id 190
    label "oceni&#263;"
  ]
  node [
    id 191
    label "przyzna&#263;"
  ]
  node [
    id 192
    label "stwierdzi&#263;"
  ]
  node [
    id 193
    label "see"
  ]
  node [
    id 194
    label "wiarygodnie"
  ]
  node [
    id 195
    label "uwiarygodnienie"
  ]
  node [
    id 196
    label "uwiarygodnianie"
  ]
  node [
    id 197
    label "pewny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 2
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 32
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 20
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 100
  ]
  edge [
    source 11
    target 101
  ]
  edge [
    source 11
    target 102
  ]
  edge [
    source 11
    target 103
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 12
    target 107
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 12
    target 109
  ]
  edge [
    source 12
    target 110
  ]
  edge [
    source 12
    target 111
  ]
  edge [
    source 12
    target 112
  ]
  edge [
    source 12
    target 113
  ]
  edge [
    source 12
    target 114
  ]
  edge [
    source 12
    target 115
  ]
  edge [
    source 12
    target 116
  ]
  edge [
    source 12
    target 117
  ]
  edge [
    source 12
    target 118
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 119
  ]
  edge [
    source 13
    target 120
  ]
  edge [
    source 13
    target 121
  ]
  edge [
    source 13
    target 122
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 123
  ]
  edge [
    source 15
    target 124
  ]
  edge [
    source 15
    target 125
  ]
  edge [
    source 15
    target 126
  ]
  edge [
    source 15
    target 127
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 128
  ]
  edge [
    source 16
    target 129
  ]
  edge [
    source 16
    target 130
  ]
  edge [
    source 16
    target 131
  ]
  edge [
    source 16
    target 132
  ]
  edge [
    source 16
    target 133
  ]
  edge [
    source 16
    target 102
  ]
  edge [
    source 16
    target 134
  ]
  edge [
    source 16
    target 135
  ]
  edge [
    source 16
    target 136
  ]
  edge [
    source 16
    target 137
  ]
  edge [
    source 16
    target 138
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 21
  ]
  edge [
    source 17
    target 22
  ]
  edge [
    source 17
    target 139
  ]
  edge [
    source 18
    target 140
  ]
  edge [
    source 18
    target 141
  ]
  edge [
    source 18
    target 142
  ]
  edge [
    source 18
    target 143
  ]
  edge [
    source 18
    target 144
  ]
  edge [
    source 18
    target 145
  ]
  edge [
    source 18
    target 146
  ]
  edge [
    source 18
    target 147
  ]
  edge [
    source 18
    target 148
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 149
  ]
  edge [
    source 19
    target 150
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 32
  ]
  edge [
    source 20
    target 151
  ]
  edge [
    source 20
    target 152
  ]
  edge [
    source 20
    target 153
  ]
  edge [
    source 20
    target 154
  ]
  edge [
    source 20
    target 155
  ]
  edge [
    source 20
    target 156
  ]
  edge [
    source 20
    target 157
  ]
  edge [
    source 20
    target 158
  ]
  edge [
    source 21
    target 159
  ]
  edge [
    source 21
    target 160
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 161
  ]
  edge [
    source 22
    target 162
  ]
  edge [
    source 22
    target 163
  ]
  edge [
    source 22
    target 164
  ]
  edge [
    source 22
    target 165
  ]
  edge [
    source 22
    target 166
  ]
  edge [
    source 22
    target 167
  ]
  edge [
    source 22
    target 168
  ]
  edge [
    source 22
    target 169
  ]
  edge [
    source 22
    target 170
  ]
  edge [
    source 22
    target 171
  ]
  edge [
    source 22
    target 172
  ]
  edge [
    source 22
    target 173
  ]
  edge [
    source 22
    target 174
  ]
  edge [
    source 22
    target 175
  ]
  edge [
    source 22
    target 176
  ]
  edge [
    source 22
    target 177
  ]
  edge [
    source 22
    target 178
  ]
  edge [
    source 22
    target 179
  ]
  edge [
    source 22
    target 180
  ]
  edge [
    source 22
    target 181
  ]
  edge [
    source 22
    target 182
  ]
  edge [
    source 22
    target 183
  ]
  edge [
    source 22
    target 184
  ]
  edge [
    source 22
    target 185
  ]
  edge [
    source 22
    target 186
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 187
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 188
  ]
  edge [
    source 24
    target 189
  ]
  edge [
    source 24
    target 190
  ]
  edge [
    source 24
    target 191
  ]
  edge [
    source 24
    target 192
  ]
  edge [
    source 24
    target 193
  ]
  edge [
    source 25
    target 194
  ]
  edge [
    source 25
    target 195
  ]
  edge [
    source 25
    target 196
  ]
  edge [
    source 25
    target 197
  ]
]
