graph [
  maxDegree 23
  minDegree 1
  meanDegree 2
  density 0.02857142857142857
  graphCliqueNumber 2
  node [
    id 0
    label "wczoraj"
    origin "text"
  ]
  node [
    id 1
    label "mia&#322;"
    origin "text"
  ]
  node [
    id 2
    label "rozmowa"
    origin "text"
  ]
  node [
    id 3
    label "szef"
    origin "text"
  ]
  node [
    id 4
    label "zwi&#261;zek"
    origin "text"
  ]
  node [
    id 5
    label "wypowiedzenie"
    origin "text"
  ]
  node [
    id 6
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 7
    label "z&#322;o&#380;y&#263;by&#263;"
    origin "text"
  ]
  node [
    id 8
    label "dobra"
    origin "text"
  ]
  node [
    id 9
    label "ile"
    origin "text"
  ]
  node [
    id 10
    label "doba"
  ]
  node [
    id 11
    label "dawno"
  ]
  node [
    id 12
    label "niedawno"
  ]
  node [
    id 13
    label "proszek"
  ]
  node [
    id 14
    label "discussion"
  ]
  node [
    id 15
    label "czynno&#347;&#263;"
  ]
  node [
    id 16
    label "odpowied&#378;"
  ]
  node [
    id 17
    label "rozhowor"
  ]
  node [
    id 18
    label "cisza"
  ]
  node [
    id 19
    label "cz&#322;owiek"
  ]
  node [
    id 20
    label "kierowa&#263;"
  ]
  node [
    id 21
    label "zwrot"
  ]
  node [
    id 22
    label "kierownictwo"
  ]
  node [
    id 23
    label "pryncypa&#322;"
  ]
  node [
    id 24
    label "odwodnienie"
  ]
  node [
    id 25
    label "konstytucja"
  ]
  node [
    id 26
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 27
    label "substancja_chemiczna"
  ]
  node [
    id 28
    label "bratnia_dusza"
  ]
  node [
    id 29
    label "zwi&#261;zanie"
  ]
  node [
    id 30
    label "lokant"
  ]
  node [
    id 31
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 32
    label "zwi&#261;za&#263;"
  ]
  node [
    id 33
    label "organizacja"
  ]
  node [
    id 34
    label "odwadnia&#263;"
  ]
  node [
    id 35
    label "marriage"
  ]
  node [
    id 36
    label "marketing_afiliacyjny"
  ]
  node [
    id 37
    label "bearing"
  ]
  node [
    id 38
    label "wi&#261;zanie"
  ]
  node [
    id 39
    label "odwadnianie"
  ]
  node [
    id 40
    label "koligacja"
  ]
  node [
    id 41
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 42
    label "odwodni&#263;"
  ]
  node [
    id 43
    label "azeotrop"
  ]
  node [
    id 44
    label "powi&#261;zanie"
  ]
  node [
    id 45
    label "&#347;cie&#347;nienie"
  ]
  node [
    id 46
    label "denunciation"
  ]
  node [
    id 47
    label "wydanie"
  ]
  node [
    id 48
    label "konwersja"
  ]
  node [
    id 49
    label "notification"
  ]
  node [
    id 50
    label "generowa&#263;"
  ]
  node [
    id 51
    label "wyra&#380;enie"
  ]
  node [
    id 52
    label "message"
  ]
  node [
    id 53
    label "powiedzenie"
  ]
  node [
    id 54
    label "szyk"
  ]
  node [
    id 55
    label "rozwi&#261;zanie"
  ]
  node [
    id 56
    label "notice"
  ]
  node [
    id 57
    label "wydobycie"
  ]
  node [
    id 58
    label "generowanie"
  ]
  node [
    id 59
    label "przepowiedzenie"
  ]
  node [
    id 60
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 61
    label "zwerbalizowanie"
  ]
  node [
    id 62
    label "frymark"
  ]
  node [
    id 63
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 64
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 65
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 66
    label "commodity"
  ]
  node [
    id 67
    label "mienie"
  ]
  node [
    id 68
    label "Wilko"
  ]
  node [
    id 69
    label "jednostka_monetarna"
  ]
  node [
    id 70
    label "centym"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
]
