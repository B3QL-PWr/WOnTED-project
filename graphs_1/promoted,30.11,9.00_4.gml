graph [
  maxDegree 71
  minDegree 1
  meanDegree 1.9857142857142858
  density 0.014285714285714285
  graphCliqueNumber 2
  node [
    id 0
    label "nowy"
    origin "text"
  ]
  node [
    id 1
    label "raport"
    origin "text"
  ]
  node [
    id 2
    label "ods&#322;ania&#263;"
    origin "text"
  ]
  node [
    id 3
    label "mroczny"
    origin "text"
  ]
  node [
    id 4
    label "kulisa"
    origin "text"
  ]
  node [
    id 5
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 6
    label "nauka"
    origin "text"
  ]
  node [
    id 7
    label "cz&#322;owiek"
  ]
  node [
    id 8
    label "nowotny"
  ]
  node [
    id 9
    label "drugi"
  ]
  node [
    id 10
    label "kolejny"
  ]
  node [
    id 11
    label "bie&#380;&#261;cy"
  ]
  node [
    id 12
    label "nowo"
  ]
  node [
    id 13
    label "narybek"
  ]
  node [
    id 14
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 15
    label "obcy"
  ]
  node [
    id 16
    label "raport_Beveridge'a"
  ]
  node [
    id 17
    label "relacja"
  ]
  node [
    id 18
    label "raport_Fischlera"
  ]
  node [
    id 19
    label "statement"
  ]
  node [
    id 20
    label "testify"
  ]
  node [
    id 21
    label "odsuwa&#263;"
  ]
  node [
    id 22
    label "zabiera&#263;"
  ]
  node [
    id 23
    label "ukazywa&#263;"
  ]
  node [
    id 24
    label "nieostry"
  ]
  node [
    id 25
    label "gro&#378;ny"
  ]
  node [
    id 26
    label "niepokoj&#261;cy"
  ]
  node [
    id 27
    label "s&#322;aby"
  ]
  node [
    id 28
    label "przy&#263;mienie"
  ]
  node [
    id 29
    label "pomroczny"
  ]
  node [
    id 30
    label "mroczno"
  ]
  node [
    id 31
    label "przy&#263;miewanie"
  ]
  node [
    id 32
    label "nostalgiczny"
  ]
  node [
    id 33
    label "ciemny"
  ]
  node [
    id 34
    label "straszny"
  ]
  node [
    id 35
    label "nastrojowy"
  ]
  node [
    id 36
    label "mrocznie"
  ]
  node [
    id 37
    label "tajemniczy"
  ]
  node [
    id 38
    label "ponury"
  ]
  node [
    id 39
    label "makabryczny"
  ]
  node [
    id 40
    label "ci&#281;&#380;ki"
  ]
  node [
    id 41
    label "bezradosny"
  ]
  node [
    id 42
    label "miejsce"
  ]
  node [
    id 43
    label "silnik_t&#322;okowy"
  ]
  node [
    id 44
    label "ogr&#243;d"
  ]
  node [
    id 45
    label "dekoracja"
  ]
  node [
    id 46
    label "kompozycja"
  ]
  node [
    id 47
    label "wing"
  ]
  node [
    id 48
    label "drzewostan"
  ]
  node [
    id 49
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 50
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 51
    label "obszar"
  ]
  node [
    id 52
    label "obiekt_naturalny"
  ]
  node [
    id 53
    label "przedmiot"
  ]
  node [
    id 54
    label "biosfera"
  ]
  node [
    id 55
    label "grupa"
  ]
  node [
    id 56
    label "stw&#243;r"
  ]
  node [
    id 57
    label "Stary_&#346;wiat"
  ]
  node [
    id 58
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 59
    label "rzecz"
  ]
  node [
    id 60
    label "magnetosfera"
  ]
  node [
    id 61
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 62
    label "environment"
  ]
  node [
    id 63
    label "Nowy_&#346;wiat"
  ]
  node [
    id 64
    label "geosfera"
  ]
  node [
    id 65
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 66
    label "planeta"
  ]
  node [
    id 67
    label "przejmowa&#263;"
  ]
  node [
    id 68
    label "litosfera"
  ]
  node [
    id 69
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 70
    label "makrokosmos"
  ]
  node [
    id 71
    label "barysfera"
  ]
  node [
    id 72
    label "biota"
  ]
  node [
    id 73
    label "p&#243;&#322;noc"
  ]
  node [
    id 74
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 75
    label "fauna"
  ]
  node [
    id 76
    label "wszechstworzenie"
  ]
  node [
    id 77
    label "geotermia"
  ]
  node [
    id 78
    label "biegun"
  ]
  node [
    id 79
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 80
    label "ekosystem"
  ]
  node [
    id 81
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 82
    label "teren"
  ]
  node [
    id 83
    label "zjawisko"
  ]
  node [
    id 84
    label "p&#243;&#322;kula"
  ]
  node [
    id 85
    label "atmosfera"
  ]
  node [
    id 86
    label "mikrokosmos"
  ]
  node [
    id 87
    label "class"
  ]
  node [
    id 88
    label "po&#322;udnie"
  ]
  node [
    id 89
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 90
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 91
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 92
    label "przejmowanie"
  ]
  node [
    id 93
    label "przestrze&#324;"
  ]
  node [
    id 94
    label "asymilowanie_si&#281;"
  ]
  node [
    id 95
    label "przej&#261;&#263;"
  ]
  node [
    id 96
    label "ekosfera"
  ]
  node [
    id 97
    label "przyroda"
  ]
  node [
    id 98
    label "ciemna_materia"
  ]
  node [
    id 99
    label "geoida"
  ]
  node [
    id 100
    label "Wsch&#243;d"
  ]
  node [
    id 101
    label "populace"
  ]
  node [
    id 102
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 103
    label "huczek"
  ]
  node [
    id 104
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 105
    label "Ziemia"
  ]
  node [
    id 106
    label "universe"
  ]
  node [
    id 107
    label "ozonosfera"
  ]
  node [
    id 108
    label "rze&#378;ba"
  ]
  node [
    id 109
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 110
    label "zagranica"
  ]
  node [
    id 111
    label "hydrosfera"
  ]
  node [
    id 112
    label "woda"
  ]
  node [
    id 113
    label "kuchnia"
  ]
  node [
    id 114
    label "przej&#281;cie"
  ]
  node [
    id 115
    label "czarna_dziura"
  ]
  node [
    id 116
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 117
    label "morze"
  ]
  node [
    id 118
    label "nauki_o_Ziemi"
  ]
  node [
    id 119
    label "teoria_naukowa"
  ]
  node [
    id 120
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 121
    label "nauki_o_poznaniu"
  ]
  node [
    id 122
    label "nomotetyczny"
  ]
  node [
    id 123
    label "metodologia"
  ]
  node [
    id 124
    label "przem&#243;wienie"
  ]
  node [
    id 125
    label "wiedza"
  ]
  node [
    id 126
    label "kultura_duchowa"
  ]
  node [
    id 127
    label "nauki_penalne"
  ]
  node [
    id 128
    label "systematyka"
  ]
  node [
    id 129
    label "inwentyka"
  ]
  node [
    id 130
    label "dziedzina"
  ]
  node [
    id 131
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 132
    label "miasteczko_rowerowe"
  ]
  node [
    id 133
    label "fotowoltaika"
  ]
  node [
    id 134
    label "porada"
  ]
  node [
    id 135
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 136
    label "proces"
  ]
  node [
    id 137
    label "imagineskopia"
  ]
  node [
    id 138
    label "typologia"
  ]
  node [
    id 139
    label "&#322;awa_szkolna"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
]
