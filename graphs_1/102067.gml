graph [
  maxDegree 15
  minDegree 1
  meanDegree 1.935483870967742
  density 0.06451612903225806
  graphCliqueNumber 2
  node [
    id 0
    label "youtube"
    origin "text"
  ]
  node [
    id 1
    label "nie"
    origin "text"
  ]
  node [
    id 2
    label "kwiecie&#324;"
    origin "text"
  ]
  node [
    id 3
    label "&#347;wiatowy"
    origin "text"
  ]
  node [
    id 4
    label "w&#322;asno&#347;&#263;"
    origin "text"
  ]
  node [
    id 5
    label "intelektualny"
    origin "text"
  ]
  node [
    id 6
    label "sprzeciw"
  ]
  node [
    id 7
    label "miesi&#261;c"
  ]
  node [
    id 8
    label "kulturalny"
  ]
  node [
    id 9
    label "&#347;wiatowo"
  ]
  node [
    id 10
    label "generalny"
  ]
  node [
    id 11
    label "rodowo&#347;&#263;"
  ]
  node [
    id 12
    label "prawo_rzeczowe"
  ]
  node [
    id 13
    label "possession"
  ]
  node [
    id 14
    label "stan"
  ]
  node [
    id 15
    label "dobra"
  ]
  node [
    id 16
    label "charakterystyka"
  ]
  node [
    id 17
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 18
    label "patent"
  ]
  node [
    id 19
    label "mienie"
  ]
  node [
    id 20
    label "przej&#347;&#263;"
  ]
  node [
    id 21
    label "attribute"
  ]
  node [
    id 22
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 23
    label "przej&#347;cie"
  ]
  node [
    id 24
    label "g&#322;&#281;boki"
  ]
  node [
    id 25
    label "naukowy"
  ]
  node [
    id 26
    label "inteligentny"
  ]
  node [
    id 27
    label "wznios&#322;y"
  ]
  node [
    id 28
    label "umys&#322;owy"
  ]
  node [
    id 29
    label "intelektualnie"
  ]
  node [
    id 30
    label "my&#347;l&#261;cy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 11
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
]
