graph [
  maxDegree 9
  minDegree 1
  meanDegree 1.8333333333333333
  density 0.16666666666666666
  graphCliqueNumber 2
  node [
    id 0
    label "zaczyna&#263;"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "marylarodowicz"
    origin "text"
  ]
  node [
    id 3
    label "humorobrazkowy"
    origin "text"
  ]
  node [
    id 4
    label "open"
  ]
  node [
    id 5
    label "odejmowa&#263;"
  ]
  node [
    id 6
    label "mie&#263;_miejsce"
  ]
  node [
    id 7
    label "robi&#263;_pierwszy_krok"
  ]
  node [
    id 8
    label "set_about"
  ]
  node [
    id 9
    label "begin"
  ]
  node [
    id 10
    label "post&#281;powa&#263;"
  ]
  node [
    id 11
    label "bankrupt"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
]
