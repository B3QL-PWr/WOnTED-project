graph [
  maxDegree 71
  minDegree 1
  meanDegree 2.07725321888412
  density 0.008953677667603967
  graphCliqueNumber 2
  node [
    id 0
    label "komentator"
    origin "text"
  ]
  node [
    id 1
    label "obserwowa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "polski"
    origin "text"
  ]
  node [
    id 3
    label "poletko"
    origin "text"
  ]
  node [
    id 4
    label "web"
    origin "text"
  ]
  node [
    id 5
    label "uwa&#380;a&#263;"
    origin "text"
  ]
  node [
    id 6
    label "by&#263;"
    origin "text"
  ]
  node [
    id 7
    label "ca&#322;kowicie"
    origin "text"
  ]
  node [
    id 8
    label "wt&#243;rny"
    origin "text"
  ]
  node [
    id 9
    label "oprze&#263;"
    origin "text"
  ]
  node [
    id 10
    label "kopiowa&#263;"
    origin "text"
  ]
  node [
    id 11
    label "istnie&#263;"
    origin "text"
  ]
  node [
    id 12
    label "serwis"
    origin "text"
  ]
  node [
    id 13
    label "ameryka&#324;ski"
    origin "text"
  ]
  node [
    id 14
    label "pogo&#324;"
    origin "text"
  ]
  node [
    id 15
    label "nowo&#347;&#263;"
    origin "text"
  ]
  node [
    id 16
    label "te&#380;"
    origin "text"
  ]
  node [
    id 17
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 18
    label "prowadzi&#263;"
    origin "text"
  ]
  node [
    id 19
    label "&#347;lepa"
    origin "text"
  ]
  node [
    id 20
    label "uliczka"
    origin "text"
  ]
  node [
    id 21
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 22
    label "donosi&#263;"
    origin "text"
  ]
  node [
    id 23
    label "nowa"
    origin "text"
  ]
  node [
    id 24
    label "autor"
  ]
  node [
    id 25
    label "uczony"
  ]
  node [
    id 26
    label "dziennikarz"
  ]
  node [
    id 27
    label "patrze&#263;"
  ]
  node [
    id 28
    label "dostrzega&#263;"
  ]
  node [
    id 29
    label "look"
  ]
  node [
    id 30
    label "lacki"
  ]
  node [
    id 31
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 32
    label "przedmiot"
  ]
  node [
    id 33
    label "sztajer"
  ]
  node [
    id 34
    label "drabant"
  ]
  node [
    id 35
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 36
    label "polak"
  ]
  node [
    id 37
    label "pierogi_ruskie"
  ]
  node [
    id 38
    label "krakowiak"
  ]
  node [
    id 39
    label "Polish"
  ]
  node [
    id 40
    label "j&#281;zyk"
  ]
  node [
    id 41
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 42
    label "oberek"
  ]
  node [
    id 43
    label "po_polsku"
  ]
  node [
    id 44
    label "mazur"
  ]
  node [
    id 45
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 46
    label "chodzony"
  ]
  node [
    id 47
    label "skoczny"
  ]
  node [
    id 48
    label "ryba_po_grecku"
  ]
  node [
    id 49
    label "goniony"
  ]
  node [
    id 50
    label "polsko"
  ]
  node [
    id 51
    label "grz&#261;dka"
  ]
  node [
    id 52
    label "posiad&#322;o&#347;&#263;"
  ]
  node [
    id 53
    label "continue"
  ]
  node [
    id 54
    label "str&#243;&#380;y&#263;"
  ]
  node [
    id 55
    label "consider"
  ]
  node [
    id 56
    label "my&#347;le&#263;"
  ]
  node [
    id 57
    label "pilnowa&#263;"
  ]
  node [
    id 58
    label "robi&#263;"
  ]
  node [
    id 59
    label "uznawa&#263;"
  ]
  node [
    id 60
    label "skupia&#263;_si&#281;"
  ]
  node [
    id 61
    label "mie&#263;_na_baczenie"
  ]
  node [
    id 62
    label "deliver"
  ]
  node [
    id 63
    label "si&#281;ga&#263;"
  ]
  node [
    id 64
    label "trwa&#263;"
  ]
  node [
    id 65
    label "obecno&#347;&#263;"
  ]
  node [
    id 66
    label "stan"
  ]
  node [
    id 67
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 68
    label "stand"
  ]
  node [
    id 69
    label "mie&#263;_miejsce"
  ]
  node [
    id 70
    label "uczestniczy&#263;"
  ]
  node [
    id 71
    label "chodzi&#263;"
  ]
  node [
    id 72
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 73
    label "equal"
  ]
  node [
    id 74
    label "kompletny"
  ]
  node [
    id 75
    label "wniwecz"
  ]
  node [
    id 76
    label "zupe&#322;ny"
  ]
  node [
    id 77
    label "wt&#243;rnie"
  ]
  node [
    id 78
    label "uboczny"
  ]
  node [
    id 79
    label "nieoryginalny"
  ]
  node [
    id 80
    label "establish"
  ]
  node [
    id 81
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 82
    label "recline"
  ]
  node [
    id 83
    label "podstawa"
  ]
  node [
    id 84
    label "ustawi&#263;"
  ]
  node [
    id 85
    label "osnowa&#263;"
  ]
  node [
    id 86
    label "pirat"
  ]
  node [
    id 87
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 88
    label "mock"
  ]
  node [
    id 89
    label "transcribe"
  ]
  node [
    id 90
    label "wytwarza&#263;"
  ]
  node [
    id 91
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 92
    label "mecz"
  ]
  node [
    id 93
    label "service"
  ]
  node [
    id 94
    label "wytw&#243;r"
  ]
  node [
    id 95
    label "zak&#322;ad"
  ]
  node [
    id 96
    label "us&#322;uga"
  ]
  node [
    id 97
    label "uderzenie"
  ]
  node [
    id 98
    label "doniesienie"
  ]
  node [
    id 99
    label "zastawa"
  ]
  node [
    id 100
    label "YouTube"
  ]
  node [
    id 101
    label "punkt"
  ]
  node [
    id 102
    label "porcja"
  ]
  node [
    id 103
    label "strona"
  ]
  node [
    id 104
    label "nowoczesny"
  ]
  node [
    id 105
    label "p&#243;&#322;nocnoameryka&#324;ski"
  ]
  node [
    id 106
    label "boston"
  ]
  node [
    id 107
    label "po_ameryka&#324;sku"
  ]
  node [
    id 108
    label "cake-walk"
  ]
  node [
    id 109
    label "charakterystyczny"
  ]
  node [
    id 110
    label "placek_ameryka&#324;ski"
  ]
  node [
    id 111
    label "fajny"
  ]
  node [
    id 112
    label "j&#281;zyk_angielski"
  ]
  node [
    id 113
    label "Princeton"
  ]
  node [
    id 114
    label "pepperoni"
  ]
  node [
    id 115
    label "sa&#322;atka_Snickers"
  ]
  node [
    id 116
    label "zachodni"
  ]
  node [
    id 117
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 118
    label "anglosaski"
  ]
  node [
    id 119
    label "typowy"
  ]
  node [
    id 120
    label "bieg"
  ]
  node [
    id 121
    label "grupa"
  ]
  node [
    id 122
    label "urozmaicenie"
  ]
  node [
    id 123
    label "newness"
  ]
  node [
    id 124
    label "wiek"
  ]
  node [
    id 125
    label "control"
  ]
  node [
    id 126
    label "eksponowa&#263;"
  ]
  node [
    id 127
    label "kre&#347;li&#263;"
  ]
  node [
    id 128
    label "g&#243;rowa&#263;"
  ]
  node [
    id 129
    label "message"
  ]
  node [
    id 130
    label "partner"
  ]
  node [
    id 131
    label "string"
  ]
  node [
    id 132
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 133
    label "przesuwa&#263;"
  ]
  node [
    id 134
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 135
    label "powodowa&#263;"
  ]
  node [
    id 136
    label "kierowa&#263;"
  ]
  node [
    id 137
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 138
    label "manipulate"
  ]
  node [
    id 139
    label "&#380;y&#263;"
  ]
  node [
    id 140
    label "navigate"
  ]
  node [
    id 141
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 142
    label "ukierunkowywa&#263;"
  ]
  node [
    id 143
    label "linia_melodyczna"
  ]
  node [
    id 144
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 145
    label "prowadzenie"
  ]
  node [
    id 146
    label "tworzy&#263;"
  ]
  node [
    id 147
    label "przewy&#380;sza&#263;"
  ]
  node [
    id 148
    label "sterowa&#263;"
  ]
  node [
    id 149
    label "krzywa"
  ]
  node [
    id 150
    label "ulica"
  ]
  node [
    id 151
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 152
    label "obszar"
  ]
  node [
    id 153
    label "obiekt_naturalny"
  ]
  node [
    id 154
    label "Stary_&#346;wiat"
  ]
  node [
    id 155
    label "stw&#243;r"
  ]
  node [
    id 156
    label "biosfera"
  ]
  node [
    id 157
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 158
    label "rzecz"
  ]
  node [
    id 159
    label "magnetosfera"
  ]
  node [
    id 160
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 161
    label "environment"
  ]
  node [
    id 162
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 163
    label "geosfera"
  ]
  node [
    id 164
    label "Nowy_&#346;wiat"
  ]
  node [
    id 165
    label "planeta"
  ]
  node [
    id 166
    label "przejmowa&#263;"
  ]
  node [
    id 167
    label "litosfera"
  ]
  node [
    id 168
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 169
    label "makrokosmos"
  ]
  node [
    id 170
    label "barysfera"
  ]
  node [
    id 171
    label "biota"
  ]
  node [
    id 172
    label "p&#243;&#322;noc"
  ]
  node [
    id 173
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 174
    label "fauna"
  ]
  node [
    id 175
    label "wszechstworzenie"
  ]
  node [
    id 176
    label "geotermia"
  ]
  node [
    id 177
    label "biegun"
  ]
  node [
    id 178
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 179
    label "ekosystem"
  ]
  node [
    id 180
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 181
    label "teren"
  ]
  node [
    id 182
    label "zjawisko"
  ]
  node [
    id 183
    label "p&#243;&#322;kula"
  ]
  node [
    id 184
    label "atmosfera"
  ]
  node [
    id 185
    label "mikrokosmos"
  ]
  node [
    id 186
    label "class"
  ]
  node [
    id 187
    label "po&#322;udnie"
  ]
  node [
    id 188
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 189
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 190
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 191
    label "przejmowanie"
  ]
  node [
    id 192
    label "przestrze&#324;"
  ]
  node [
    id 193
    label "asymilowanie_si&#281;"
  ]
  node [
    id 194
    label "przej&#261;&#263;"
  ]
  node [
    id 195
    label "ekosfera"
  ]
  node [
    id 196
    label "przyroda"
  ]
  node [
    id 197
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 198
    label "ciemna_materia"
  ]
  node [
    id 199
    label "geoida"
  ]
  node [
    id 200
    label "Wsch&#243;d"
  ]
  node [
    id 201
    label "populace"
  ]
  node [
    id 202
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 203
    label "huczek"
  ]
  node [
    id 204
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 205
    label "Ziemia"
  ]
  node [
    id 206
    label "universe"
  ]
  node [
    id 207
    label "ozonosfera"
  ]
  node [
    id 208
    label "rze&#378;ba"
  ]
  node [
    id 209
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 210
    label "zagranica"
  ]
  node [
    id 211
    label "hydrosfera"
  ]
  node [
    id 212
    label "woda"
  ]
  node [
    id 213
    label "kuchnia"
  ]
  node [
    id 214
    label "przej&#281;cie"
  ]
  node [
    id 215
    label "czarna_dziura"
  ]
  node [
    id 216
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 217
    label "morze"
  ]
  node [
    id 218
    label "zu&#380;y&#263;"
  ]
  node [
    id 219
    label "get"
  ]
  node [
    id 220
    label "render"
  ]
  node [
    id 221
    label "ci&#261;&#380;a"
  ]
  node [
    id 222
    label "informowa&#263;"
  ]
  node [
    id 223
    label "zanosi&#263;"
  ]
  node [
    id 224
    label "introduce"
  ]
  node [
    id 225
    label "do&#322;&#261;cza&#263;"
  ]
  node [
    id 226
    label "spill_the_beans"
  ]
  node [
    id 227
    label "give"
  ]
  node [
    id 228
    label "dosi&#281;ga&#263;"
  ]
  node [
    id 229
    label "inform"
  ]
  node [
    id 230
    label "przeby&#263;"
  ]
  node [
    id 231
    label "gwiazda"
  ]
  node [
    id 232
    label "gwiazda_podw&#243;jna"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 58
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 68
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 20
  ]
  edge [
    source 12
    target 21
  ]
  edge [
    source 12
    target 23
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 12
    target 94
  ]
  edge [
    source 12
    target 95
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 104
  ]
  edge [
    source 13
    target 105
  ]
  edge [
    source 13
    target 106
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 13
    target 113
  ]
  edge [
    source 13
    target 114
  ]
  edge [
    source 13
    target 115
  ]
  edge [
    source 13
    target 116
  ]
  edge [
    source 13
    target 117
  ]
  edge [
    source 13
    target 118
  ]
  edge [
    source 13
    target 119
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 120
  ]
  edge [
    source 14
    target 121
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 122
  ]
  edge [
    source 15
    target 123
  ]
  edge [
    source 15
    target 124
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 125
  ]
  edge [
    source 18
    target 126
  ]
  edge [
    source 18
    target 127
  ]
  edge [
    source 18
    target 128
  ]
  edge [
    source 18
    target 129
  ]
  edge [
    source 18
    target 130
  ]
  edge [
    source 18
    target 131
  ]
  edge [
    source 18
    target 132
  ]
  edge [
    source 18
    target 133
  ]
  edge [
    source 18
    target 134
  ]
  edge [
    source 18
    target 91
  ]
  edge [
    source 18
    target 135
  ]
  edge [
    source 18
    target 136
  ]
  edge [
    source 18
    target 137
  ]
  edge [
    source 18
    target 58
  ]
  edge [
    source 18
    target 138
  ]
  edge [
    source 18
    target 139
  ]
  edge [
    source 18
    target 140
  ]
  edge [
    source 18
    target 141
  ]
  edge [
    source 18
    target 142
  ]
  edge [
    source 18
    target 143
  ]
  edge [
    source 18
    target 144
  ]
  edge [
    source 18
    target 145
  ]
  edge [
    source 18
    target 146
  ]
  edge [
    source 18
    target 147
  ]
  edge [
    source 18
    target 148
  ]
  edge [
    source 18
    target 149
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 150
  ]
  edge [
    source 21
    target 151
  ]
  edge [
    source 21
    target 152
  ]
  edge [
    source 21
    target 153
  ]
  edge [
    source 21
    target 32
  ]
  edge [
    source 21
    target 154
  ]
  edge [
    source 21
    target 121
  ]
  edge [
    source 21
    target 155
  ]
  edge [
    source 21
    target 156
  ]
  edge [
    source 21
    target 157
  ]
  edge [
    source 21
    target 158
  ]
  edge [
    source 21
    target 159
  ]
  edge [
    source 21
    target 160
  ]
  edge [
    source 21
    target 161
  ]
  edge [
    source 21
    target 162
  ]
  edge [
    source 21
    target 163
  ]
  edge [
    source 21
    target 164
  ]
  edge [
    source 21
    target 165
  ]
  edge [
    source 21
    target 166
  ]
  edge [
    source 21
    target 167
  ]
  edge [
    source 21
    target 168
  ]
  edge [
    source 21
    target 169
  ]
  edge [
    source 21
    target 170
  ]
  edge [
    source 21
    target 171
  ]
  edge [
    source 21
    target 172
  ]
  edge [
    source 21
    target 173
  ]
  edge [
    source 21
    target 174
  ]
  edge [
    source 21
    target 175
  ]
  edge [
    source 21
    target 176
  ]
  edge [
    source 21
    target 177
  ]
  edge [
    source 21
    target 178
  ]
  edge [
    source 21
    target 179
  ]
  edge [
    source 21
    target 180
  ]
  edge [
    source 21
    target 181
  ]
  edge [
    source 21
    target 182
  ]
  edge [
    source 21
    target 183
  ]
  edge [
    source 21
    target 184
  ]
  edge [
    source 21
    target 185
  ]
  edge [
    source 21
    target 186
  ]
  edge [
    source 21
    target 187
  ]
  edge [
    source 21
    target 188
  ]
  edge [
    source 21
    target 189
  ]
  edge [
    source 21
    target 190
  ]
  edge [
    source 21
    target 191
  ]
  edge [
    source 21
    target 192
  ]
  edge [
    source 21
    target 193
  ]
  edge [
    source 21
    target 194
  ]
  edge [
    source 21
    target 195
  ]
  edge [
    source 21
    target 196
  ]
  edge [
    source 21
    target 197
  ]
  edge [
    source 21
    target 198
  ]
  edge [
    source 21
    target 199
  ]
  edge [
    source 21
    target 200
  ]
  edge [
    source 21
    target 201
  ]
  edge [
    source 21
    target 202
  ]
  edge [
    source 21
    target 203
  ]
  edge [
    source 21
    target 204
  ]
  edge [
    source 21
    target 205
  ]
  edge [
    source 21
    target 206
  ]
  edge [
    source 21
    target 207
  ]
  edge [
    source 21
    target 208
  ]
  edge [
    source 21
    target 209
  ]
  edge [
    source 21
    target 210
  ]
  edge [
    source 21
    target 211
  ]
  edge [
    source 21
    target 212
  ]
  edge [
    source 21
    target 213
  ]
  edge [
    source 21
    target 214
  ]
  edge [
    source 21
    target 215
  ]
  edge [
    source 21
    target 216
  ]
  edge [
    source 21
    target 217
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 218
  ]
  edge [
    source 22
    target 219
  ]
  edge [
    source 22
    target 220
  ]
  edge [
    source 22
    target 221
  ]
  edge [
    source 22
    target 222
  ]
  edge [
    source 22
    target 223
  ]
  edge [
    source 22
    target 224
  ]
  edge [
    source 22
    target 225
  ]
  edge [
    source 22
    target 226
  ]
  edge [
    source 22
    target 227
  ]
  edge [
    source 22
    target 228
  ]
  edge [
    source 22
    target 229
  ]
  edge [
    source 22
    target 230
  ]
  edge [
    source 23
    target 231
  ]
  edge [
    source 23
    target 232
  ]
]
