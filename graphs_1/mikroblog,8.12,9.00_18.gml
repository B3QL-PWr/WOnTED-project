graph [
  maxDegree 13
  minDegree 1
  meanDegree 1.875
  density 0.125
  graphCliqueNumber 2
  node [
    id 0
    label "pi&#263;"
    origin "text"
  ]
  node [
    id 1
    label "cokolwiek"
    origin "text"
  ]
  node [
    id 2
    label "goli&#263;"
  ]
  node [
    id 3
    label "ch&#322;on&#261;&#263;"
  ]
  node [
    id 4
    label "gulp"
  ]
  node [
    id 5
    label "nabiera&#263;"
  ]
  node [
    id 6
    label "naoliwia&#263;_si&#281;"
  ]
  node [
    id 7
    label "dostarcza&#263;"
  ]
  node [
    id 8
    label "odurza&#263;_si&#281;"
  ]
  node [
    id 9
    label "obci&#261;ga&#263;"
  ]
  node [
    id 10
    label "spo&#380;ywa&#263;"
  ]
  node [
    id 11
    label "nawi&#261;zywa&#263;"
  ]
  node [
    id 12
    label "wch&#322;ania&#263;"
  ]
  node [
    id 13
    label "uwiera&#263;"
  ]
  node [
    id 14
    label "ilo&#347;&#263;"
  ]
  node [
    id 15
    label "co&#347;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
]
