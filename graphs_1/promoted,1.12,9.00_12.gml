graph [
  maxDegree 33
  minDegree 1
  meanDegree 2.0575539568345325
  density 0.014909811281409655
  graphCliqueNumber 4
  node [
    id 0
    label "warszawski"
    origin "text"
  ]
  node [
    id 1
    label "s&#261;d"
    origin "text"
  ]
  node [
    id 2
    label "rejonowy"
    origin "text"
  ]
  node [
    id 3
    label "ponownie"
    origin "text"
  ]
  node [
    id 4
    label "rozpatrze&#263;"
    origin "text"
  ]
  node [
    id 5
    label "sprawa"
    origin "text"
  ]
  node [
    id 6
    label "prokurator"
    origin "text"
  ]
  node [
    id 7
    label "uniewinni&#263;"
    origin "text"
  ]
  node [
    id 8
    label "zarzut"
    origin "text"
  ]
  node [
    id 9
    label "przyj&#281;cie"
    origin "text"
  ]
  node [
    id 10
    label "tys"
    origin "text"
  ]
  node [
    id 11
    label "z&#322;oty"
    origin "text"
  ]
  node [
    id 12
    label "zamian"
    origin "text"
  ]
  node [
    id 13
    label "pomoc"
    origin "text"
  ]
  node [
    id 14
    label "uwolni&#263;"
    origin "text"
  ]
  node [
    id 15
    label "handlarz"
    origin "text"
  ]
  node [
    id 16
    label "narkotyk"
    origin "text"
  ]
  node [
    id 17
    label "pi&#261;tek"
    origin "text"
  ]
  node [
    id 18
    label "okr&#281;gowy"
    origin "text"
  ]
  node [
    id 19
    label "warszawa"
    origin "text"
  ]
  node [
    id 20
    label "po_warszawsku"
  ]
  node [
    id 21
    label "marmuzela"
  ]
  node [
    id 22
    label "mazowiecki"
  ]
  node [
    id 23
    label "procesowicz"
  ]
  node [
    id 24
    label "wypowied&#378;"
  ]
  node [
    id 25
    label "pods&#261;dny"
  ]
  node [
    id 26
    label "podejrzany"
  ]
  node [
    id 27
    label "broni&#263;"
  ]
  node [
    id 28
    label "bronienie"
  ]
  node [
    id 29
    label "system"
  ]
  node [
    id 30
    label "my&#347;l"
  ]
  node [
    id 31
    label "wytw&#243;r"
  ]
  node [
    id 32
    label "urz&#261;d"
  ]
  node [
    id 33
    label "konektyw"
  ]
  node [
    id 34
    label "court"
  ]
  node [
    id 35
    label "obrona"
  ]
  node [
    id 36
    label "s&#261;downictwo"
  ]
  node [
    id 37
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 38
    label "forum"
  ]
  node [
    id 39
    label "zesp&#243;&#322;"
  ]
  node [
    id 40
    label "post&#281;powanie"
  ]
  node [
    id 41
    label "skazany"
  ]
  node [
    id 42
    label "wydarzenie"
  ]
  node [
    id 43
    label "&#347;wiadek"
  ]
  node [
    id 44
    label "antylogizm"
  ]
  node [
    id 45
    label "strona"
  ]
  node [
    id 46
    label "oskar&#380;yciel"
  ]
  node [
    id 47
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 48
    label "biuro"
  ]
  node [
    id 49
    label "instytucja"
  ]
  node [
    id 50
    label "znowu"
  ]
  node [
    id 51
    label "ponowny"
  ]
  node [
    id 52
    label "przeprowadzi&#263;"
  ]
  node [
    id 53
    label "przygl&#261;dn&#261;&#263;_si&#281;"
  ]
  node [
    id 54
    label "reason"
  ]
  node [
    id 55
    label "temat"
  ]
  node [
    id 56
    label "kognicja"
  ]
  node [
    id 57
    label "idea"
  ]
  node [
    id 58
    label "szczeg&#243;&#322;"
  ]
  node [
    id 59
    label "rzecz"
  ]
  node [
    id 60
    label "przes&#322;anka"
  ]
  node [
    id 61
    label "rozprawa"
  ]
  node [
    id 62
    label "object"
  ]
  node [
    id 63
    label "proposition"
  ]
  node [
    id 64
    label "pracownik"
  ]
  node [
    id 65
    label "prawnik"
  ]
  node [
    id 66
    label "oskar&#380;yciel_publiczny"
  ]
  node [
    id 67
    label "potraktowa&#263;"
  ]
  node [
    id 68
    label "wyda&#263;_wyrok"
  ]
  node [
    id 69
    label "niewinny"
  ]
  node [
    id 70
    label "oskar&#380;enie"
  ]
  node [
    id 71
    label "pretensja"
  ]
  node [
    id 72
    label "oskar&#380;ycielstwo"
  ]
  node [
    id 73
    label "wzi&#281;cie"
  ]
  node [
    id 74
    label "reception"
  ]
  node [
    id 75
    label "zareagowanie"
  ]
  node [
    id 76
    label "poch&#322;oni&#281;cie"
  ]
  node [
    id 77
    label "entertainment"
  ]
  node [
    id 78
    label "spotkanie"
  ]
  node [
    id 79
    label "zobowi&#261;zanie_si&#281;"
  ]
  node [
    id 80
    label "stanie_si&#281;"
  ]
  node [
    id 81
    label "presumption"
  ]
  node [
    id 82
    label "dopuszczenie"
  ]
  node [
    id 83
    label "credence"
  ]
  node [
    id 84
    label "uznanie"
  ]
  node [
    id 85
    label "zgodzenie_si&#281;"
  ]
  node [
    id 86
    label "zrobienie"
  ]
  node [
    id 87
    label "party"
  ]
  node [
    id 88
    label "impreza"
  ]
  node [
    id 89
    label "wpuszczenie"
  ]
  node [
    id 90
    label "przyj&#261;&#263;"
  ]
  node [
    id 91
    label "nau&#380;ywanie_si&#281;"
  ]
  node [
    id 92
    label "w&#322;&#261;czenie"
  ]
  node [
    id 93
    label "umieszczenie"
  ]
  node [
    id 94
    label "szlachetny"
  ]
  node [
    id 95
    label "metaliczny"
  ]
  node [
    id 96
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 97
    label "z&#322;oci&#347;cie"
  ]
  node [
    id 98
    label "grosz"
  ]
  node [
    id 99
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 100
    label "utytu&#322;owany"
  ]
  node [
    id 101
    label "poz&#322;ocenie"
  ]
  node [
    id 102
    label "Polska"
  ]
  node [
    id 103
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 104
    label "wspania&#322;y"
  ]
  node [
    id 105
    label "doskona&#322;y"
  ]
  node [
    id 106
    label "kochany"
  ]
  node [
    id 107
    label "jednostka_monetarna"
  ]
  node [
    id 108
    label "z&#322;ocenie"
  ]
  node [
    id 109
    label "zgodzi&#263;"
  ]
  node [
    id 110
    label "pomocnik"
  ]
  node [
    id 111
    label "doch&#243;d"
  ]
  node [
    id 112
    label "property"
  ]
  node [
    id 113
    label "przedmiot"
  ]
  node [
    id 114
    label "grupa"
  ]
  node [
    id 115
    label "telefon_zaufania"
  ]
  node [
    id 116
    label "darowizna"
  ]
  node [
    id 117
    label "&#347;rodek"
  ]
  node [
    id 118
    label "liga"
  ]
  node [
    id 119
    label "pom&#243;c"
  ]
  node [
    id 120
    label "spowodowa&#263;"
  ]
  node [
    id 121
    label "release"
  ]
  node [
    id 122
    label "wytworzy&#263;"
  ]
  node [
    id 123
    label "wzbudzi&#263;"
  ]
  node [
    id 124
    label "deliver"
  ]
  node [
    id 125
    label "kupiec"
  ]
  node [
    id 126
    label "naszprycowa&#263;"
  ]
  node [
    id 127
    label "&#347;rodek_psychoaktywny"
  ]
  node [
    id 128
    label "szprycowa&#263;"
  ]
  node [
    id 129
    label "narkobiznes"
  ]
  node [
    id 130
    label "naszprycowanie"
  ]
  node [
    id 131
    label "szprycowanie"
  ]
  node [
    id 132
    label "dzie&#324;_powszedni"
  ]
  node [
    id 133
    label "Wielki_Pi&#261;tek"
  ]
  node [
    id 134
    label "tydzie&#324;"
  ]
  node [
    id 135
    label "Warszawa"
  ]
  node [
    id 136
    label "samoch&#243;d"
  ]
  node [
    id 137
    label "fastback"
  ]
  node [
    id 138
    label "w"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 11
    target 98
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 11
    target 100
  ]
  edge [
    source 11
    target 101
  ]
  edge [
    source 11
    target 102
  ]
  edge [
    source 11
    target 103
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 108
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 13
    target 113
  ]
  edge [
    source 13
    target 114
  ]
  edge [
    source 13
    target 115
  ]
  edge [
    source 13
    target 116
  ]
  edge [
    source 13
    target 117
  ]
  edge [
    source 13
    target 118
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 119
  ]
  edge [
    source 14
    target 120
  ]
  edge [
    source 14
    target 121
  ]
  edge [
    source 14
    target 122
  ]
  edge [
    source 14
    target 123
  ]
  edge [
    source 14
    target 124
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 125
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 126
  ]
  edge [
    source 16
    target 127
  ]
  edge [
    source 16
    target 128
  ]
  edge [
    source 16
    target 129
  ]
  edge [
    source 16
    target 130
  ]
  edge [
    source 16
    target 131
  ]
  edge [
    source 17
    target 132
  ]
  edge [
    source 17
    target 133
  ]
  edge [
    source 17
    target 134
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 138
  ]
  edge [
    source 19
    target 135
  ]
  edge [
    source 19
    target 136
  ]
  edge [
    source 19
    target 137
  ]
  edge [
    source 19
    target 138
  ]
]
