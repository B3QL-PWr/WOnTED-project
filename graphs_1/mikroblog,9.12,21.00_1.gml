graph [
  maxDegree 38
  minDegree 1
  meanDegree 1.9615384615384615
  density 0.038461538461538464
  graphCliqueNumber 2
  node [
    id 0
    label "dzi&#281;kowa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "serdecznie"
    origin "text"
  ]
  node [
    id 2
    label "zakazhandlu"
    origin "text"
  ]
  node [
    id 3
    label "pan"
    origin "text"
  ]
  node [
    id 4
    label "pis"
    origin "text"
  ]
  node [
    id 5
    label "odmawia&#263;"
  ]
  node [
    id 6
    label "wycofywa&#263;_si&#281;"
  ]
  node [
    id 7
    label "sk&#322;ada&#263;"
  ]
  node [
    id 8
    label "thank"
  ]
  node [
    id 9
    label "odwdzi&#281;cza&#263;_si&#281;"
  ]
  node [
    id 10
    label "wyra&#380;a&#263;"
  ]
  node [
    id 11
    label "etykieta"
  ]
  node [
    id 12
    label "siarczy&#347;cie"
  ]
  node [
    id 13
    label "serdeczny"
  ]
  node [
    id 14
    label "szczerze"
  ]
  node [
    id 15
    label "mi&#322;o"
  ]
  node [
    id 16
    label "cz&#322;owiek"
  ]
  node [
    id 17
    label "profesor"
  ]
  node [
    id 18
    label "kszta&#322;ciciel"
  ]
  node [
    id 19
    label "jegomo&#347;&#263;"
  ]
  node [
    id 20
    label "zwrot"
  ]
  node [
    id 21
    label "pracodawca"
  ]
  node [
    id 22
    label "rz&#261;dzenie"
  ]
  node [
    id 23
    label "m&#261;&#380;"
  ]
  node [
    id 24
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 25
    label "ch&#322;opina"
  ]
  node [
    id 26
    label "bratek"
  ]
  node [
    id 27
    label "opiekun"
  ]
  node [
    id 28
    label "doros&#322;y"
  ]
  node [
    id 29
    label "preceptor"
  ]
  node [
    id 30
    label "Midas"
  ]
  node [
    id 31
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 32
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 33
    label "murza"
  ]
  node [
    id 34
    label "ojciec"
  ]
  node [
    id 35
    label "androlog"
  ]
  node [
    id 36
    label "pupil"
  ]
  node [
    id 37
    label "efendi"
  ]
  node [
    id 38
    label "nabab"
  ]
  node [
    id 39
    label "w&#322;odarz"
  ]
  node [
    id 40
    label "szkolnik"
  ]
  node [
    id 41
    label "pedagog"
  ]
  node [
    id 42
    label "popularyzator"
  ]
  node [
    id 43
    label "andropauza"
  ]
  node [
    id 44
    label "gra_w_karty"
  ]
  node [
    id 45
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 46
    label "Mieszko_I"
  ]
  node [
    id 47
    label "bogaty"
  ]
  node [
    id 48
    label "samiec"
  ]
  node [
    id 49
    label "przyw&#243;dca"
  ]
  node [
    id 50
    label "pa&#324;stwo"
  ]
  node [
    id 51
    label "belfer"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
]
