graph [
  maxDegree 39
  minDegree 1
  meanDegree 2
  density 0.015873015873015872
  graphCliqueNumber 2
  node [
    id 0
    label "rok"
    origin "text"
  ]
  node [
    id 1
    label "johan"
    origin "text"
  ]
  node [
    id 2
    label "huibers"
    origin "text"
  ]
  node [
    id 3
    label "czyta&#263;"
    origin "text"
  ]
  node [
    id 4
    label "swoje"
    origin "text"
  ]
  node [
    id 5
    label "dziecko"
    origin "text"
  ]
  node [
    id 6
    label "opowie&#347;&#263;"
    origin "text"
  ]
  node [
    id 7
    label "arka"
    origin "text"
  ]
  node [
    id 8
    label "noe"
    origin "text"
  ]
  node [
    id 9
    label "wtedy"
    origin "text"
  ]
  node [
    id 10
    label "przyj&#347;&#263;"
    origin "text"
  ]
  node [
    id 11
    label "g&#322;owa"
    origin "text"
  ]
  node [
    id 12
    label "szalon"
    origin "text"
  ]
  node [
    id 13
    label "pomys&#322;"
    origin "text"
  ]
  node [
    id 14
    label "zbudowa&#263;"
    origin "text"
  ]
  node [
    id 15
    label "w&#322;asny"
    origin "text"
  ]
  node [
    id 16
    label "stulecie"
  ]
  node [
    id 17
    label "kalendarz"
  ]
  node [
    id 18
    label "czas"
  ]
  node [
    id 19
    label "pora_roku"
  ]
  node [
    id 20
    label "cykl_astronomiczny"
  ]
  node [
    id 21
    label "p&#243;&#322;rocze"
  ]
  node [
    id 22
    label "grupa"
  ]
  node [
    id 23
    label "kwarta&#322;"
  ]
  node [
    id 24
    label "kurs"
  ]
  node [
    id 25
    label "jubileusz"
  ]
  node [
    id 26
    label "miesi&#261;c"
  ]
  node [
    id 27
    label "lata"
  ]
  node [
    id 28
    label "martwy_sezon"
  ]
  node [
    id 29
    label "dysleksja"
  ]
  node [
    id 30
    label "umie&#263;"
  ]
  node [
    id 31
    label "wr&#243;&#380;y&#263;"
  ]
  node [
    id 32
    label "przetwarza&#263;"
  ]
  node [
    id 33
    label "read"
  ]
  node [
    id 34
    label "poznawa&#263;"
  ]
  node [
    id 35
    label "obserwowa&#263;"
  ]
  node [
    id 36
    label "odczytywa&#263;"
  ]
  node [
    id 37
    label "cz&#322;owiek"
  ]
  node [
    id 38
    label "potomstwo"
  ]
  node [
    id 39
    label "organizm"
  ]
  node [
    id 40
    label "sraluch"
  ]
  node [
    id 41
    label "utulanie"
  ]
  node [
    id 42
    label "pediatra"
  ]
  node [
    id 43
    label "dzieciarnia"
  ]
  node [
    id 44
    label "m&#322;odziak"
  ]
  node [
    id 45
    label "dzieciak"
  ]
  node [
    id 46
    label "utula&#263;"
  ]
  node [
    id 47
    label "potomek"
  ]
  node [
    id 48
    label "pedofil"
  ]
  node [
    id 49
    label "entliczek-pentliczek"
  ]
  node [
    id 50
    label "m&#322;odzik"
  ]
  node [
    id 51
    label "cz&#322;owieczek"
  ]
  node [
    id 52
    label "zwierz&#281;"
  ]
  node [
    id 53
    label "niepe&#322;noletni"
  ]
  node [
    id 54
    label "fledgling"
  ]
  node [
    id 55
    label "utuli&#263;"
  ]
  node [
    id 56
    label "utulenie"
  ]
  node [
    id 57
    label "report"
  ]
  node [
    id 58
    label "fabu&#322;a"
  ]
  node [
    id 59
    label "opowiadanie"
  ]
  node [
    id 60
    label "wypowied&#378;"
  ]
  node [
    id 61
    label "Arka_Przymierza"
  ]
  node [
    id 62
    label "skrzynia"
  ]
  node [
    id 63
    label "statek"
  ]
  node [
    id 64
    label "kiedy&#347;"
  ]
  node [
    id 65
    label "sta&#263;_si&#281;"
  ]
  node [
    id 66
    label "zaistnie&#263;"
  ]
  node [
    id 67
    label "doj&#347;&#263;"
  ]
  node [
    id 68
    label "become"
  ]
  node [
    id 69
    label "line_up"
  ]
  node [
    id 70
    label "przyby&#263;"
  ]
  node [
    id 71
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 72
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 73
    label "ucho"
  ]
  node [
    id 74
    label "makrocefalia"
  ]
  node [
    id 75
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 76
    label "m&#243;zg"
  ]
  node [
    id 77
    label "kierownictwo"
  ]
  node [
    id 78
    label "czaszka"
  ]
  node [
    id 79
    label "dekiel"
  ]
  node [
    id 80
    label "umys&#322;"
  ]
  node [
    id 81
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 82
    label "&#347;ci&#281;cie"
  ]
  node [
    id 83
    label "sztuka"
  ]
  node [
    id 84
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 85
    label "g&#243;ra"
  ]
  node [
    id 86
    label "byd&#322;o"
  ]
  node [
    id 87
    label "alkohol"
  ]
  node [
    id 88
    label "wiedza"
  ]
  node [
    id 89
    label "ro&#347;lina"
  ]
  node [
    id 90
    label "&#347;ci&#281;gno"
  ]
  node [
    id 91
    label "&#380;ycie"
  ]
  node [
    id 92
    label "pryncypa&#322;"
  ]
  node [
    id 93
    label "fryzura"
  ]
  node [
    id 94
    label "noosfera"
  ]
  node [
    id 95
    label "kierowa&#263;"
  ]
  node [
    id 96
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 97
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 98
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 99
    label "cecha"
  ]
  node [
    id 100
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 101
    label "zdolno&#347;&#263;"
  ]
  node [
    id 102
    label "kszta&#322;t"
  ]
  node [
    id 103
    label "cz&#322;onek"
  ]
  node [
    id 104
    label "cia&#322;o"
  ]
  node [
    id 105
    label "obiekt"
  ]
  node [
    id 106
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 107
    label "system"
  ]
  node [
    id 108
    label "wytw&#243;r"
  ]
  node [
    id 109
    label "idea"
  ]
  node [
    id 110
    label "ukra&#347;&#263;"
  ]
  node [
    id 111
    label "ukradzenie"
  ]
  node [
    id 112
    label "pocz&#261;tki"
  ]
  node [
    id 113
    label "zaplanowa&#263;"
  ]
  node [
    id 114
    label "establish"
  ]
  node [
    id 115
    label "stworzy&#263;"
  ]
  node [
    id 116
    label "wytworzy&#263;"
  ]
  node [
    id 117
    label "evolve"
  ]
  node [
    id 118
    label "budowla"
  ]
  node [
    id 119
    label "swoisty"
  ]
  node [
    id 120
    label "czyj&#347;"
  ]
  node [
    id 121
    label "osobny"
  ]
  node [
    id 122
    label "zwi&#261;zany"
  ]
  node [
    id 123
    label "samodzielny"
  ]
  node [
    id 124
    label "Johan"
  ]
  node [
    id 125
    label "Huibers"
  ]
  node [
    id 126
    label "Noe"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 126
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 65
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 10
    target 18
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 10
    target 69
  ]
  edge [
    source 10
    target 70
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 37
  ]
  edge [
    source 11
    target 71
  ]
  edge [
    source 11
    target 72
  ]
  edge [
    source 11
    target 73
  ]
  edge [
    source 11
    target 74
  ]
  edge [
    source 11
    target 75
  ]
  edge [
    source 11
    target 76
  ]
  edge [
    source 11
    target 77
  ]
  edge [
    source 11
    target 78
  ]
  edge [
    source 11
    target 79
  ]
  edge [
    source 11
    target 80
  ]
  edge [
    source 11
    target 81
  ]
  edge [
    source 11
    target 82
  ]
  edge [
    source 11
    target 83
  ]
  edge [
    source 11
    target 84
  ]
  edge [
    source 11
    target 85
  ]
  edge [
    source 11
    target 86
  ]
  edge [
    source 11
    target 87
  ]
  edge [
    source 11
    target 88
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 11
    target 92
  ]
  edge [
    source 11
    target 93
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 11
    target 98
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 11
    target 100
  ]
  edge [
    source 11
    target 101
  ]
  edge [
    source 11
    target 102
  ]
  edge [
    source 11
    target 103
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 113
  ]
  edge [
    source 14
    target 114
  ]
  edge [
    source 14
    target 115
  ]
  edge [
    source 14
    target 116
  ]
  edge [
    source 14
    target 117
  ]
  edge [
    source 14
    target 118
  ]
  edge [
    source 15
    target 119
  ]
  edge [
    source 15
    target 120
  ]
  edge [
    source 15
    target 121
  ]
  edge [
    source 15
    target 122
  ]
  edge [
    source 15
    target 123
  ]
  edge [
    source 124
    target 125
  ]
]
