graph [
  maxDegree 1
  minDegree 1
  meanDegree 1
  density 0.14285714285714285
  graphCliqueNumber 2
  node [
    id 0
    label "marcin"
    origin "text"
  ]
  node [
    id 1
    label "koniusz"
    origin "text"
  ]
  node [
    id 2
    label "Marcin"
  ]
  node [
    id 3
    label "Koniusz"
  ]
  node [
    id 4
    label "igrzysko"
  ]
  node [
    id 5
    label "olimpijski"
  ]
  node [
    id 6
    label "Nicolasem"
  ]
  node [
    id 7
    label "Limbachem"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 6
    target 7
  ]
]
