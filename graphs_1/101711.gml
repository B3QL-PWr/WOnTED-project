graph [
  maxDegree 324
  minDegree 1
  meanDegree 2.0219092331768387
  density 0.003169136729117302
  graphCliqueNumber 3
  node [
    id 0
    label "protest"
    origin "text"
  ]
  node [
    id 1
    label "reporter"
    origin "text"
  ]
  node [
    id 2
    label "bez"
    origin "text"
  ]
  node [
    id 3
    label "granica"
    origin "text"
  ]
  node [
    id 4
    label "przeciwko"
    origin "text"
  ]
  node [
    id 5
    label "cenzura"
    origin "text"
  ]
  node [
    id 6
    label "internet"
    origin "text"
  ]
  node [
    id 7
    label "nale&#380;e&#263;"
    origin "text"
  ]
  node [
    id 8
    label "zaprotestowa&#263;"
    origin "text"
  ]
  node [
    id 9
    label "w&#322;a&#347;nie"
    origin "text"
  ]
  node [
    id 10
    label "dlatego"
    origin "text"
  ]
  node [
    id 11
    label "ostatnie"
    origin "text"
  ]
  node [
    id 12
    label "polityczny"
    origin "text"
  ]
  node [
    id 13
    label "trend"
    origin "text"
  ]
  node [
    id 14
    label "wcale"
    origin "text"
  ]
  node [
    id 15
    label "nasz"
    origin "text"
  ]
  node [
    id 16
    label "obiektywnie"
    origin "text"
  ]
  node [
    id 17
    label "rzecz"
    origin "text"
  ]
  node [
    id 18
    label "bra&#263;"
    origin "text"
  ]
  node [
    id 19
    label "z&#322;y"
    origin "text"
  ]
  node [
    id 20
    label "pozycja"
    origin "text"
  ]
  node [
    id 21
    label "poprawia&#263;"
    origin "text"
  ]
  node [
    id 22
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 23
    label "daleko"
    origin "text"
  ]
  node [
    id 24
    label "kraj"
    origin "text"
  ]
  node [
    id 25
    label "znajdowa&#263;"
    origin "text"
  ]
  node [
    id 26
    label "si&#281;"
    origin "text"
  ]
  node [
    id 27
    label "czarna"
    origin "text"
  ]
  node [
    id 28
    label "list"
    origin "text"
  ]
  node [
    id 29
    label "arabia"
    origin "text"
  ]
  node [
    id 30
    label "saudyjski"
    origin "text"
  ]
  node [
    id 31
    label "bia&#322;oru&#347;"
    origin "text"
  ]
  node [
    id 32
    label "birma"
    origin "text"
  ]
  node [
    id 33
    label "chiny"
    origin "text"
  ]
  node [
    id 34
    label "egipy"
    origin "text"
  ]
  node [
    id 35
    label "iran"
    origin "text"
  ]
  node [
    id 36
    label "korea"
    origin "text"
  ]
  node [
    id 37
    label "p&#243;&#322;nocny"
    origin "text"
  ]
  node [
    id 38
    label "kuba"
    origin "text"
  ]
  node [
    id 39
    label "syria"
    origin "text"
  ]
  node [
    id 40
    label "tunezja"
    origin "text"
  ]
  node [
    id 41
    label "turkmenistan"
    origin "text"
  ]
  node [
    id 42
    label "uzbekistan"
    origin "text"
  ]
  node [
    id 43
    label "wietnam"
    origin "text"
  ]
  node [
    id 44
    label "ale"
    origin "text"
  ]
  node [
    id 45
    label "koniec"
    origin "text"
  ]
  node [
    id 46
    label "nic"
    origin "text"
  ]
  node [
    id 47
    label "by&#263;"
    origin "text"
  ]
  node [
    id 48
    label "dana"
    origin "text"
  ]
  node [
    id 49
    label "wiecznie"
    origin "text"
  ]
  node [
    id 50
    label "reakcja"
  ]
  node [
    id 51
    label "protestacja"
  ]
  node [
    id 52
    label "czerwona_kartka"
  ]
  node [
    id 53
    label "dziennikarz"
  ]
  node [
    id 54
    label "ki&#347;&#263;"
  ]
  node [
    id 55
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 56
    label "krzew"
  ]
  node [
    id 57
    label "pi&#380;maczkowate"
  ]
  node [
    id 58
    label "pestkowiec"
  ]
  node [
    id 59
    label "kwiat"
  ]
  node [
    id 60
    label "owoc"
  ]
  node [
    id 61
    label "oliwkowate"
  ]
  node [
    id 62
    label "ro&#347;lina"
  ]
  node [
    id 63
    label "hy&#263;ka"
  ]
  node [
    id 64
    label "lilac"
  ]
  node [
    id 65
    label "delfinidyna"
  ]
  node [
    id 66
    label "zakres"
  ]
  node [
    id 67
    label "Ural"
  ]
  node [
    id 68
    label "kres"
  ]
  node [
    id 69
    label "granice"
  ]
  node [
    id 70
    label "granica_pa&#324;stwa"
  ]
  node [
    id 71
    label "pu&#322;ap"
  ]
  node [
    id 72
    label "frontier"
  ]
  node [
    id 73
    label "end"
  ]
  node [
    id 74
    label "miara"
  ]
  node [
    id 75
    label "poj&#281;cie"
  ]
  node [
    id 76
    label "przej&#347;cie"
  ]
  node [
    id 77
    label "zdj&#281;cie"
  ]
  node [
    id 78
    label "zdj&#261;&#263;"
  ]
  node [
    id 79
    label "bell_ringer"
  ]
  node [
    id 80
    label "&#347;wiadectwo"
  ]
  node [
    id 81
    label "ekskomunikowa&#263;"
  ]
  node [
    id 82
    label "zdejmowanie"
  ]
  node [
    id 83
    label "crisscross"
  ]
  node [
    id 84
    label "urz&#261;d"
  ]
  node [
    id 85
    label "mark"
  ]
  node [
    id 86
    label "drugi_obieg"
  ]
  node [
    id 87
    label "krytyka"
  ]
  node [
    id 88
    label "proces"
  ]
  node [
    id 89
    label "ekskomunikowanie"
  ]
  node [
    id 90
    label "kontrola"
  ]
  node [
    id 91
    label "zjawisko"
  ]
  node [
    id 92
    label "kara"
  ]
  node [
    id 93
    label "p&#243;&#322;kownik"
  ]
  node [
    id 94
    label "zdejmowa&#263;"
  ]
  node [
    id 95
    label "us&#322;uga_internetowa"
  ]
  node [
    id 96
    label "biznes_elektroniczny"
  ]
  node [
    id 97
    label "punkt_dost&#281;pu"
  ]
  node [
    id 98
    label "hipertekst"
  ]
  node [
    id 99
    label "gra_sieciowa"
  ]
  node [
    id 100
    label "mem"
  ]
  node [
    id 101
    label "e-hazard"
  ]
  node [
    id 102
    label "sie&#263;_komputerowa"
  ]
  node [
    id 103
    label "media"
  ]
  node [
    id 104
    label "podcast"
  ]
  node [
    id 105
    label "netbook"
  ]
  node [
    id 106
    label "provider"
  ]
  node [
    id 107
    label "cyberprzestrze&#324;"
  ]
  node [
    id 108
    label "grooming"
  ]
  node [
    id 109
    label "strona"
  ]
  node [
    id 110
    label "trza"
  ]
  node [
    id 111
    label "uczestniczy&#263;"
  ]
  node [
    id 112
    label "zalicza&#263;_si&#281;"
  ]
  node [
    id 113
    label "para"
  ]
  node [
    id 114
    label "necessity"
  ]
  node [
    id 115
    label "post&#261;pi&#263;"
  ]
  node [
    id 116
    label "object"
  ]
  node [
    id 117
    label "dok&#322;adnie"
  ]
  node [
    id 118
    label "internowanie"
  ]
  node [
    id 119
    label "prorz&#261;dowy"
  ]
  node [
    id 120
    label "wi&#281;zie&#324;"
  ]
  node [
    id 121
    label "politycznie"
  ]
  node [
    id 122
    label "internowa&#263;"
  ]
  node [
    id 123
    label "ideologiczny"
  ]
  node [
    id 124
    label "system"
  ]
  node [
    id 125
    label "fashionistka"
  ]
  node [
    id 126
    label "odzie&#380;"
  ]
  node [
    id 127
    label "ideologia"
  ]
  node [
    id 128
    label "tendencja_rozwojowa"
  ]
  node [
    id 129
    label "praktyka"
  ]
  node [
    id 130
    label "metoda"
  ]
  node [
    id 131
    label "zwyczaj"
  ]
  node [
    id 132
    label "dziedzina"
  ]
  node [
    id 133
    label "ni_chuja"
  ]
  node [
    id 134
    label "ca&#322;kiem"
  ]
  node [
    id 135
    label "zupe&#322;nie"
  ]
  node [
    id 136
    label "czyj&#347;"
  ]
  node [
    id 137
    label "faktycznie"
  ]
  node [
    id 138
    label "obiektywny"
  ]
  node [
    id 139
    label "niezale&#380;nie"
  ]
  node [
    id 140
    label "obiekt"
  ]
  node [
    id 141
    label "temat"
  ]
  node [
    id 142
    label "istota"
  ]
  node [
    id 143
    label "wpada&#263;"
  ]
  node [
    id 144
    label "wpa&#347;&#263;"
  ]
  node [
    id 145
    label "przedmiot"
  ]
  node [
    id 146
    label "wpadanie"
  ]
  node [
    id 147
    label "kultura"
  ]
  node [
    id 148
    label "przyroda"
  ]
  node [
    id 149
    label "mienie"
  ]
  node [
    id 150
    label "wpadni&#281;cie"
  ]
  node [
    id 151
    label "get"
  ]
  node [
    id 152
    label "przewa&#380;a&#263;"
  ]
  node [
    id 153
    label "zobowi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 154
    label "poczytywa&#263;"
  ]
  node [
    id 155
    label "levy"
  ]
  node [
    id 156
    label "pokonywa&#263;"
  ]
  node [
    id 157
    label "u&#380;ywa&#263;"
  ]
  node [
    id 158
    label "rusza&#263;"
  ]
  node [
    id 159
    label "zalicza&#263;"
  ]
  node [
    id 160
    label "wygrywa&#263;"
  ]
  node [
    id 161
    label "open"
  ]
  node [
    id 162
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 163
    label "branie"
  ]
  node [
    id 164
    label "korzysta&#263;"
  ]
  node [
    id 165
    label "&#263;pa&#263;"
  ]
  node [
    id 166
    label "wch&#322;ania&#263;"
  ]
  node [
    id 167
    label "interpretowa&#263;"
  ]
  node [
    id 168
    label "atakowa&#263;"
  ]
  node [
    id 169
    label "prowadzi&#263;"
  ]
  node [
    id 170
    label "rucha&#263;"
  ]
  node [
    id 171
    label "take"
  ]
  node [
    id 172
    label "dostawa&#263;"
  ]
  node [
    id 173
    label "wzi&#261;&#263;"
  ]
  node [
    id 174
    label "wk&#322;ada&#263;"
  ]
  node [
    id 175
    label "chwyta&#263;"
  ]
  node [
    id 176
    label "arise"
  ]
  node [
    id 177
    label "za&#380;ywa&#263;"
  ]
  node [
    id 178
    label "uprawia&#263;_seks"
  ]
  node [
    id 179
    label "porywa&#263;"
  ]
  node [
    id 180
    label "robi&#263;"
  ]
  node [
    id 181
    label "grza&#263;"
  ]
  node [
    id 182
    label "abstract"
  ]
  node [
    id 183
    label "zaopatrywa&#263;_si&#281;"
  ]
  node [
    id 184
    label "wzmaga&#263;_si&#281;"
  ]
  node [
    id 185
    label "towarzystwo"
  ]
  node [
    id 186
    label "otrzymywa&#263;"
  ]
  node [
    id 187
    label "przyjmowa&#263;"
  ]
  node [
    id 188
    label "wchodzi&#263;"
  ]
  node [
    id 189
    label "ucieka&#263;"
  ]
  node [
    id 190
    label "odurza&#263;_si&#281;"
  ]
  node [
    id 191
    label "buzowa&#263;_si&#281;"
  ]
  node [
    id 192
    label "&#322;apa&#263;"
  ]
  node [
    id 193
    label "raise"
  ]
  node [
    id 194
    label "zdenerwowany"
  ]
  node [
    id 195
    label "zez&#322;oszczenie"
  ]
  node [
    id 196
    label "rozz&#322;oszczenie_si&#281;"
  ]
  node [
    id 197
    label "gniewanie"
  ]
  node [
    id 198
    label "niekorzystny"
  ]
  node [
    id 199
    label "niemoralny"
  ]
  node [
    id 200
    label "niegrzeczny"
  ]
  node [
    id 201
    label "pieski"
  ]
  node [
    id 202
    label "negatywny"
  ]
  node [
    id 203
    label "&#378;le"
  ]
  node [
    id 204
    label "syf"
  ]
  node [
    id 205
    label "niew&#322;a&#347;ciwy"
  ]
  node [
    id 206
    label "sierdzisty"
  ]
  node [
    id 207
    label "z&#322;oszczenie"
  ]
  node [
    id 208
    label "rozgniewanie"
  ]
  node [
    id 209
    label "niepomy&#347;lny"
  ]
  node [
    id 210
    label "spis"
  ]
  node [
    id 211
    label "znaczenie"
  ]
  node [
    id 212
    label "awansowanie"
  ]
  node [
    id 213
    label "po&#322;o&#380;enie"
  ]
  node [
    id 214
    label "rz&#261;d"
  ]
  node [
    id 215
    label "wydawa&#263;"
  ]
  node [
    id 216
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 217
    label "szermierka"
  ]
  node [
    id 218
    label "debit"
  ]
  node [
    id 219
    label "status"
  ]
  node [
    id 220
    label "adres"
  ]
  node [
    id 221
    label "redaktor"
  ]
  node [
    id 222
    label "poster"
  ]
  node [
    id 223
    label "le&#380;e&#263;"
  ]
  node [
    id 224
    label "wyda&#263;"
  ]
  node [
    id 225
    label "bearing"
  ]
  node [
    id 226
    label "wojsko"
  ]
  node [
    id 227
    label "druk"
  ]
  node [
    id 228
    label "awans"
  ]
  node [
    id 229
    label "ustawienie"
  ]
  node [
    id 230
    label "sytuacja"
  ]
  node [
    id 231
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 232
    label "miejsce"
  ]
  node [
    id 233
    label "decyzja_lokalizacyjna"
  ]
  node [
    id 234
    label "szata_graficzna"
  ]
  node [
    id 235
    label "awansowa&#263;"
  ]
  node [
    id 236
    label "rozmieszczenie"
  ]
  node [
    id 237
    label "publikacja"
  ]
  node [
    id 238
    label "check"
  ]
  node [
    id 239
    label "ulepsza&#263;"
  ]
  node [
    id 240
    label "wprowadza&#263;"
  ]
  node [
    id 241
    label "upomina&#263;"
  ]
  node [
    id 242
    label "right"
  ]
  node [
    id 243
    label "dawno"
  ]
  node [
    id 244
    label "nisko"
  ]
  node [
    id 245
    label "nieobecnie"
  ]
  node [
    id 246
    label "daleki"
  ]
  node [
    id 247
    label "het"
  ]
  node [
    id 248
    label "wysoko"
  ]
  node [
    id 249
    label "du&#380;o"
  ]
  node [
    id 250
    label "znacznie"
  ]
  node [
    id 251
    label "g&#322;&#281;boko"
  ]
  node [
    id 252
    label "Skandynawia"
  ]
  node [
    id 253
    label "Rwanda"
  ]
  node [
    id 254
    label "Filipiny"
  ]
  node [
    id 255
    label "Yorkshire"
  ]
  node [
    id 256
    label "Kaukaz"
  ]
  node [
    id 257
    label "Podbeskidzie"
  ]
  node [
    id 258
    label "Toskania"
  ]
  node [
    id 259
    label "&#321;emkowszczyzna"
  ]
  node [
    id 260
    label "obszar"
  ]
  node [
    id 261
    label "Monako"
  ]
  node [
    id 262
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 263
    label "Amhara"
  ]
  node [
    id 264
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 265
    label "Lombardia"
  ]
  node [
    id 266
    label "Korea"
  ]
  node [
    id 267
    label "Kalabria"
  ]
  node [
    id 268
    label "Czarnog&#243;ra"
  ]
  node [
    id 269
    label "Ghana"
  ]
  node [
    id 270
    label "Tyrol"
  ]
  node [
    id 271
    label "Malawi"
  ]
  node [
    id 272
    label "Indonezja"
  ]
  node [
    id 273
    label "Bu&#322;garia"
  ]
  node [
    id 274
    label "Nauru"
  ]
  node [
    id 275
    label "Kenia"
  ]
  node [
    id 276
    label "Pamir"
  ]
  node [
    id 277
    label "Kambod&#380;a"
  ]
  node [
    id 278
    label "Lubelszczyzna"
  ]
  node [
    id 279
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 280
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 281
    label "Mali"
  ]
  node [
    id 282
    label "&#379;ywiecczyzna"
  ]
  node [
    id 283
    label "Austria"
  ]
  node [
    id 284
    label "interior"
  ]
  node [
    id 285
    label "Europa_Wschodnia"
  ]
  node [
    id 286
    label "Armenia"
  ]
  node [
    id 287
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 288
    label "Fid&#380;i"
  ]
  node [
    id 289
    label "Tuwalu"
  ]
  node [
    id 290
    label "Zabajkale"
  ]
  node [
    id 291
    label "Etiopia"
  ]
  node [
    id 292
    label "Malezja"
  ]
  node [
    id 293
    label "Malta"
  ]
  node [
    id 294
    label "Kaszuby"
  ]
  node [
    id 295
    label "Noworosja"
  ]
  node [
    id 296
    label "Bo&#347;nia"
  ]
  node [
    id 297
    label "Tad&#380;ykistan"
  ]
  node [
    id 298
    label "Grenada"
  ]
  node [
    id 299
    label "Ba&#322;kany"
  ]
  node [
    id 300
    label "Wehrlen"
  ]
  node [
    id 301
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 302
    label "Anglia"
  ]
  node [
    id 303
    label "Kielecczyzna"
  ]
  node [
    id 304
    label "Rumunia"
  ]
  node [
    id 305
    label "Pomorze_Zachodnie"
  ]
  node [
    id 306
    label "Maroko"
  ]
  node [
    id 307
    label "Bhutan"
  ]
  node [
    id 308
    label "Opolskie"
  ]
  node [
    id 309
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 310
    label "Ko&#322;yma"
  ]
  node [
    id 311
    label "Oksytania"
  ]
  node [
    id 312
    label "S&#322;owacja"
  ]
  node [
    id 313
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 314
    label "Seszele"
  ]
  node [
    id 315
    label "Syjon"
  ]
  node [
    id 316
    label "Kuwejt"
  ]
  node [
    id 317
    label "Arabia_Saudyjska"
  ]
  node [
    id 318
    label "Kociewie"
  ]
  node [
    id 319
    label "Kanada"
  ]
  node [
    id 320
    label "Ekwador"
  ]
  node [
    id 321
    label "ziemia"
  ]
  node [
    id 322
    label "Japonia"
  ]
  node [
    id 323
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 324
    label "Hiszpania"
  ]
  node [
    id 325
    label "Wyspy_Marshalla"
  ]
  node [
    id 326
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 327
    label "D&#380;ibuti"
  ]
  node [
    id 328
    label "Botswana"
  ]
  node [
    id 329
    label "Huculszczyzna"
  ]
  node [
    id 330
    label "Wietnam"
  ]
  node [
    id 331
    label "Egipt"
  ]
  node [
    id 332
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 333
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 334
    label "Burkina_Faso"
  ]
  node [
    id 335
    label "Bawaria"
  ]
  node [
    id 336
    label "Niemcy"
  ]
  node [
    id 337
    label "Khitai"
  ]
  node [
    id 338
    label "Macedonia"
  ]
  node [
    id 339
    label "Albania"
  ]
  node [
    id 340
    label "Madagaskar"
  ]
  node [
    id 341
    label "Bahrajn"
  ]
  node [
    id 342
    label "Jemen"
  ]
  node [
    id 343
    label "Lesoto"
  ]
  node [
    id 344
    label "Maghreb"
  ]
  node [
    id 345
    label "Samoa"
  ]
  node [
    id 346
    label "Andora"
  ]
  node [
    id 347
    label "Bory_Tucholskie"
  ]
  node [
    id 348
    label "Chiny"
  ]
  node [
    id 349
    label "Europa_Zachodnia"
  ]
  node [
    id 350
    label "Cypr"
  ]
  node [
    id 351
    label "Wielka_Brytania"
  ]
  node [
    id 352
    label "Kerala"
  ]
  node [
    id 353
    label "Podhale"
  ]
  node [
    id 354
    label "Kabylia"
  ]
  node [
    id 355
    label "Ukraina"
  ]
  node [
    id 356
    label "Paragwaj"
  ]
  node [
    id 357
    label "Trynidad_i_Tobago"
  ]
  node [
    id 358
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 359
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 360
    label "Ma&#322;opolska"
  ]
  node [
    id 361
    label "Polesie"
  ]
  node [
    id 362
    label "Liguria"
  ]
  node [
    id 363
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 364
    label "Libia"
  ]
  node [
    id 365
    label "&#321;&#243;dzkie"
  ]
  node [
    id 366
    label "Surinam"
  ]
  node [
    id 367
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 368
    label "Palestyna"
  ]
  node [
    id 369
    label "Nigeria"
  ]
  node [
    id 370
    label "Australia"
  ]
  node [
    id 371
    label "Honduras"
  ]
  node [
    id 372
    label "Bojkowszczyzna"
  ]
  node [
    id 373
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 374
    label "Karaiby"
  ]
  node [
    id 375
    label "Peru"
  ]
  node [
    id 376
    label "USA"
  ]
  node [
    id 377
    label "Bangladesz"
  ]
  node [
    id 378
    label "Kazachstan"
  ]
  node [
    id 379
    label "Nepal"
  ]
  node [
    id 380
    label "Irak"
  ]
  node [
    id 381
    label "Nadrenia"
  ]
  node [
    id 382
    label "Sudan"
  ]
  node [
    id 383
    label "S&#261;decczyzna"
  ]
  node [
    id 384
    label "Sand&#380;ak"
  ]
  node [
    id 385
    label "San_Marino"
  ]
  node [
    id 386
    label "Burundi"
  ]
  node [
    id 387
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 388
    label "Dominikana"
  ]
  node [
    id 389
    label "Komory"
  ]
  node [
    id 390
    label "Zakarpacie"
  ]
  node [
    id 391
    label "Gwatemala"
  ]
  node [
    id 392
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 393
    label "Zag&#243;rze"
  ]
  node [
    id 394
    label "Andaluzja"
  ]
  node [
    id 395
    label "Turkiestan"
  ]
  node [
    id 396
    label "Naddniestrze"
  ]
  node [
    id 397
    label "Hercegowina"
  ]
  node [
    id 398
    label "Brunei"
  ]
  node [
    id 399
    label "Iran"
  ]
  node [
    id 400
    label "jednostka_administracyjna"
  ]
  node [
    id 401
    label "Zimbabwe"
  ]
  node [
    id 402
    label "Namibia"
  ]
  node [
    id 403
    label "Meksyk"
  ]
  node [
    id 404
    label "Opolszczyzna"
  ]
  node [
    id 405
    label "Kamerun"
  ]
  node [
    id 406
    label "Afryka_Wschodnia"
  ]
  node [
    id 407
    label "Szlezwik"
  ]
  node [
    id 408
    label "Lotaryngia"
  ]
  node [
    id 409
    label "Somalia"
  ]
  node [
    id 410
    label "Angola"
  ]
  node [
    id 411
    label "Gabon"
  ]
  node [
    id 412
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 413
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 414
    label "Nowa_Zelandia"
  ]
  node [
    id 415
    label "Mozambik"
  ]
  node [
    id 416
    label "Tunezja"
  ]
  node [
    id 417
    label "Tajwan"
  ]
  node [
    id 418
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 419
    label "Liban"
  ]
  node [
    id 420
    label "Jordania"
  ]
  node [
    id 421
    label "Tonga"
  ]
  node [
    id 422
    label "Czad"
  ]
  node [
    id 423
    label "Gwinea"
  ]
  node [
    id 424
    label "Liberia"
  ]
  node [
    id 425
    label "Belize"
  ]
  node [
    id 426
    label "Mazowsze"
  ]
  node [
    id 427
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 428
    label "Benin"
  ]
  node [
    id 429
    label "&#321;otwa"
  ]
  node [
    id 430
    label "Syria"
  ]
  node [
    id 431
    label "Afryka_Zachodnia"
  ]
  node [
    id 432
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 433
    label "Dominika"
  ]
  node [
    id 434
    label "Antigua_i_Barbuda"
  ]
  node [
    id 435
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 436
    label "Hanower"
  ]
  node [
    id 437
    label "Galicja"
  ]
  node [
    id 438
    label "Szkocja"
  ]
  node [
    id 439
    label "Walia"
  ]
  node [
    id 440
    label "Afganistan"
  ]
  node [
    id 441
    label "W&#322;ochy"
  ]
  node [
    id 442
    label "Kiribati"
  ]
  node [
    id 443
    label "Szwajcaria"
  ]
  node [
    id 444
    label "Powi&#347;le"
  ]
  node [
    id 445
    label "Chorwacja"
  ]
  node [
    id 446
    label "Sahara_Zachodnia"
  ]
  node [
    id 447
    label "Tajlandia"
  ]
  node [
    id 448
    label "Salwador"
  ]
  node [
    id 449
    label "Bahamy"
  ]
  node [
    id 450
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 451
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 452
    label "Zamojszczyzna"
  ]
  node [
    id 453
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 454
    label "S&#322;owenia"
  ]
  node [
    id 455
    label "Gambia"
  ]
  node [
    id 456
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 457
    label "Urugwaj"
  ]
  node [
    id 458
    label "Podlasie"
  ]
  node [
    id 459
    label "Zair"
  ]
  node [
    id 460
    label "Erytrea"
  ]
  node [
    id 461
    label "Laponia"
  ]
  node [
    id 462
    label "Kujawy"
  ]
  node [
    id 463
    label "Umbria"
  ]
  node [
    id 464
    label "Rosja"
  ]
  node [
    id 465
    label "Mauritius"
  ]
  node [
    id 466
    label "Niger"
  ]
  node [
    id 467
    label "Uganda"
  ]
  node [
    id 468
    label "Turkmenistan"
  ]
  node [
    id 469
    label "Turcja"
  ]
  node [
    id 470
    label "Mezoameryka"
  ]
  node [
    id 471
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 472
    label "Irlandia"
  ]
  node [
    id 473
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 474
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 475
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 476
    label "Gwinea_Bissau"
  ]
  node [
    id 477
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 478
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 479
    label "Kurdystan"
  ]
  node [
    id 480
    label "Belgia"
  ]
  node [
    id 481
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 482
    label "Palau"
  ]
  node [
    id 483
    label "Barbados"
  ]
  node [
    id 484
    label "Wenezuela"
  ]
  node [
    id 485
    label "W&#281;gry"
  ]
  node [
    id 486
    label "Chile"
  ]
  node [
    id 487
    label "Argentyna"
  ]
  node [
    id 488
    label "Kolumbia"
  ]
  node [
    id 489
    label "Armagnac"
  ]
  node [
    id 490
    label "Kampania"
  ]
  node [
    id 491
    label "Sierra_Leone"
  ]
  node [
    id 492
    label "Azerbejd&#380;an"
  ]
  node [
    id 493
    label "Kongo"
  ]
  node [
    id 494
    label "Polinezja"
  ]
  node [
    id 495
    label "Warmia"
  ]
  node [
    id 496
    label "Pakistan"
  ]
  node [
    id 497
    label "Liechtenstein"
  ]
  node [
    id 498
    label "Wielkopolska"
  ]
  node [
    id 499
    label "Nikaragua"
  ]
  node [
    id 500
    label "Senegal"
  ]
  node [
    id 501
    label "brzeg"
  ]
  node [
    id 502
    label "Bordeaux"
  ]
  node [
    id 503
    label "Lauda"
  ]
  node [
    id 504
    label "Indie"
  ]
  node [
    id 505
    label "Mazury"
  ]
  node [
    id 506
    label "Suazi"
  ]
  node [
    id 507
    label "Polska"
  ]
  node [
    id 508
    label "Algieria"
  ]
  node [
    id 509
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 510
    label "Jamajka"
  ]
  node [
    id 511
    label "Timor_Wschodni"
  ]
  node [
    id 512
    label "Oceania"
  ]
  node [
    id 513
    label "Kostaryka"
  ]
  node [
    id 514
    label "Lasko"
  ]
  node [
    id 515
    label "Podkarpacie"
  ]
  node [
    id 516
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 517
    label "Kuba"
  ]
  node [
    id 518
    label "Mauretania"
  ]
  node [
    id 519
    label "Amazonia"
  ]
  node [
    id 520
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 521
    label "Portoryko"
  ]
  node [
    id 522
    label "Brazylia"
  ]
  node [
    id 523
    label "Mo&#322;dawia"
  ]
  node [
    id 524
    label "organizacja"
  ]
  node [
    id 525
    label "Litwa"
  ]
  node [
    id 526
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 527
    label "Kirgistan"
  ]
  node [
    id 528
    label "Izrael"
  ]
  node [
    id 529
    label "Grecja"
  ]
  node [
    id 530
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 531
    label "Kurpie"
  ]
  node [
    id 532
    label "Holandia"
  ]
  node [
    id 533
    label "Sri_Lanka"
  ]
  node [
    id 534
    label "Tonkin"
  ]
  node [
    id 535
    label "Katar"
  ]
  node [
    id 536
    label "Azja_Wschodnia"
  ]
  node [
    id 537
    label "Kaszmir"
  ]
  node [
    id 538
    label "Mikronezja"
  ]
  node [
    id 539
    label "Ukraina_Zachodnia"
  ]
  node [
    id 540
    label "Laos"
  ]
  node [
    id 541
    label "Mongolia"
  ]
  node [
    id 542
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 543
    label "Malediwy"
  ]
  node [
    id 544
    label "Zambia"
  ]
  node [
    id 545
    label "Turyngia"
  ]
  node [
    id 546
    label "Tanzania"
  ]
  node [
    id 547
    label "Gujana"
  ]
  node [
    id 548
    label "Apulia"
  ]
  node [
    id 549
    label "Uzbekistan"
  ]
  node [
    id 550
    label "Panama"
  ]
  node [
    id 551
    label "Czechy"
  ]
  node [
    id 552
    label "Gruzja"
  ]
  node [
    id 553
    label "Baszkiria"
  ]
  node [
    id 554
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 555
    label "Francja"
  ]
  node [
    id 556
    label "Serbia"
  ]
  node [
    id 557
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 558
    label "Togo"
  ]
  node [
    id 559
    label "Estonia"
  ]
  node [
    id 560
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 561
    label "Indochiny"
  ]
  node [
    id 562
    label "Boliwia"
  ]
  node [
    id 563
    label "Oman"
  ]
  node [
    id 564
    label "Portugalia"
  ]
  node [
    id 565
    label "Wyspy_Salomona"
  ]
  node [
    id 566
    label "Haiti"
  ]
  node [
    id 567
    label "Luksemburg"
  ]
  node [
    id 568
    label "Lubuskie"
  ]
  node [
    id 569
    label "Biskupizna"
  ]
  node [
    id 570
    label "Birma"
  ]
  node [
    id 571
    label "Rodezja"
  ]
  node [
    id 572
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 573
    label "doznawa&#263;"
  ]
  node [
    id 574
    label "znachodzi&#263;"
  ]
  node [
    id 575
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 576
    label "pozyskiwa&#263;"
  ]
  node [
    id 577
    label "odzyskiwa&#263;"
  ]
  node [
    id 578
    label "os&#261;dza&#263;"
  ]
  node [
    id 579
    label "wykrywa&#263;"
  ]
  node [
    id 580
    label "unwrap"
  ]
  node [
    id 581
    label "detect"
  ]
  node [
    id 582
    label "wymy&#347;la&#263;"
  ]
  node [
    id 583
    label "powodowa&#263;"
  ]
  node [
    id 584
    label "kawa"
  ]
  node [
    id 585
    label "czarny"
  ]
  node [
    id 586
    label "murzynek"
  ]
  node [
    id 587
    label "li&#347;&#263;"
  ]
  node [
    id 588
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 589
    label "poczta"
  ]
  node [
    id 590
    label "epistolografia"
  ]
  node [
    id 591
    label "przesy&#322;ka"
  ]
  node [
    id 592
    label "poczta_elektroniczna"
  ]
  node [
    id 593
    label "znaczek_pocztowy"
  ]
  node [
    id 594
    label "arabski"
  ]
  node [
    id 595
    label "mro&#378;ny"
  ]
  node [
    id 596
    label "nocny"
  ]
  node [
    id 597
    label "&#347;nie&#380;ny"
  ]
  node [
    id 598
    label "zimowy"
  ]
  node [
    id 599
    label "piwo"
  ]
  node [
    id 600
    label "defenestracja"
  ]
  node [
    id 601
    label "szereg"
  ]
  node [
    id 602
    label "dzia&#322;anie"
  ]
  node [
    id 603
    label "ostatnie_podrygi"
  ]
  node [
    id 604
    label "agonia"
  ]
  node [
    id 605
    label "visitation"
  ]
  node [
    id 606
    label "szeol"
  ]
  node [
    id 607
    label "mogi&#322;a"
  ]
  node [
    id 608
    label "chwila"
  ]
  node [
    id 609
    label "wydarzenie"
  ]
  node [
    id 610
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 611
    label "pogrzebanie"
  ]
  node [
    id 612
    label "punkt"
  ]
  node [
    id 613
    label "&#380;a&#322;oba"
  ]
  node [
    id 614
    label "zabicie"
  ]
  node [
    id 615
    label "kres_&#380;ycia"
  ]
  node [
    id 616
    label "miernota"
  ]
  node [
    id 617
    label "g&#243;wno"
  ]
  node [
    id 618
    label "love"
  ]
  node [
    id 619
    label "ilo&#347;&#263;"
  ]
  node [
    id 620
    label "brak"
  ]
  node [
    id 621
    label "ciura"
  ]
  node [
    id 622
    label "si&#281;ga&#263;"
  ]
  node [
    id 623
    label "trwa&#263;"
  ]
  node [
    id 624
    label "obecno&#347;&#263;"
  ]
  node [
    id 625
    label "stan"
  ]
  node [
    id 626
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 627
    label "stand"
  ]
  node [
    id 628
    label "mie&#263;_miejsce"
  ]
  node [
    id 629
    label "chodzi&#263;"
  ]
  node [
    id 630
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 631
    label "equal"
  ]
  node [
    id 632
    label "dar"
  ]
  node [
    id 633
    label "cnota"
  ]
  node [
    id 634
    label "buddyzm"
  ]
  node [
    id 635
    label "ci&#261;gle"
  ]
  node [
    id 636
    label "stale"
  ]
  node [
    id 637
    label "wieczny"
  ]
  node [
    id 638
    label "trwale"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 7
    target 113
  ]
  edge [
    source 7
    target 114
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 117
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 118
  ]
  edge [
    source 12
    target 119
  ]
  edge [
    source 12
    target 120
  ]
  edge [
    source 12
    target 121
  ]
  edge [
    source 12
    target 122
  ]
  edge [
    source 12
    target 123
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 124
  ]
  edge [
    source 13
    target 125
  ]
  edge [
    source 13
    target 126
  ]
  edge [
    source 13
    target 127
  ]
  edge [
    source 13
    target 128
  ]
  edge [
    source 13
    target 129
  ]
  edge [
    source 13
    target 130
  ]
  edge [
    source 13
    target 131
  ]
  edge [
    source 13
    target 132
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 133
  ]
  edge [
    source 14
    target 134
  ]
  edge [
    source 14
    target 135
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 136
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 137
  ]
  edge [
    source 16
    target 138
  ]
  edge [
    source 16
    target 139
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 140
  ]
  edge [
    source 17
    target 141
  ]
  edge [
    source 17
    target 142
  ]
  edge [
    source 17
    target 143
  ]
  edge [
    source 17
    target 144
  ]
  edge [
    source 17
    target 145
  ]
  edge [
    source 17
    target 146
  ]
  edge [
    source 17
    target 147
  ]
  edge [
    source 17
    target 148
  ]
  edge [
    source 17
    target 149
  ]
  edge [
    source 17
    target 116
  ]
  edge [
    source 17
    target 150
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 151
  ]
  edge [
    source 18
    target 152
  ]
  edge [
    source 18
    target 153
  ]
  edge [
    source 18
    target 154
  ]
  edge [
    source 18
    target 155
  ]
  edge [
    source 18
    target 156
  ]
  edge [
    source 18
    target 157
  ]
  edge [
    source 18
    target 158
  ]
  edge [
    source 18
    target 159
  ]
  edge [
    source 18
    target 47
  ]
  edge [
    source 18
    target 160
  ]
  edge [
    source 18
    target 161
  ]
  edge [
    source 18
    target 162
  ]
  edge [
    source 18
    target 163
  ]
  edge [
    source 18
    target 164
  ]
  edge [
    source 18
    target 165
  ]
  edge [
    source 18
    target 166
  ]
  edge [
    source 18
    target 167
  ]
  edge [
    source 18
    target 168
  ]
  edge [
    source 18
    target 169
  ]
  edge [
    source 18
    target 170
  ]
  edge [
    source 18
    target 171
  ]
  edge [
    source 18
    target 172
  ]
  edge [
    source 18
    target 173
  ]
  edge [
    source 18
    target 174
  ]
  edge [
    source 18
    target 175
  ]
  edge [
    source 18
    target 176
  ]
  edge [
    source 18
    target 177
  ]
  edge [
    source 18
    target 178
  ]
  edge [
    source 18
    target 179
  ]
  edge [
    source 18
    target 180
  ]
  edge [
    source 18
    target 181
  ]
  edge [
    source 18
    target 182
  ]
  edge [
    source 18
    target 183
  ]
  edge [
    source 18
    target 184
  ]
  edge [
    source 18
    target 185
  ]
  edge [
    source 18
    target 186
  ]
  edge [
    source 18
    target 187
  ]
  edge [
    source 18
    target 188
  ]
  edge [
    source 18
    target 189
  ]
  edge [
    source 18
    target 190
  ]
  edge [
    source 18
    target 191
  ]
  edge [
    source 18
    target 192
  ]
  edge [
    source 18
    target 193
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 194
  ]
  edge [
    source 19
    target 195
  ]
  edge [
    source 19
    target 196
  ]
  edge [
    source 19
    target 197
  ]
  edge [
    source 19
    target 198
  ]
  edge [
    source 19
    target 199
  ]
  edge [
    source 19
    target 200
  ]
  edge [
    source 19
    target 201
  ]
  edge [
    source 19
    target 202
  ]
  edge [
    source 19
    target 203
  ]
  edge [
    source 19
    target 204
  ]
  edge [
    source 19
    target 205
  ]
  edge [
    source 19
    target 206
  ]
  edge [
    source 19
    target 207
  ]
  edge [
    source 19
    target 208
  ]
  edge [
    source 19
    target 209
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 210
  ]
  edge [
    source 20
    target 211
  ]
  edge [
    source 20
    target 212
  ]
  edge [
    source 20
    target 213
  ]
  edge [
    source 20
    target 214
  ]
  edge [
    source 20
    target 215
  ]
  edge [
    source 20
    target 216
  ]
  edge [
    source 20
    target 217
  ]
  edge [
    source 20
    target 218
  ]
  edge [
    source 20
    target 219
  ]
  edge [
    source 20
    target 220
  ]
  edge [
    source 20
    target 221
  ]
  edge [
    source 20
    target 222
  ]
  edge [
    source 20
    target 223
  ]
  edge [
    source 20
    target 224
  ]
  edge [
    source 20
    target 225
  ]
  edge [
    source 20
    target 226
  ]
  edge [
    source 20
    target 227
  ]
  edge [
    source 20
    target 228
  ]
  edge [
    source 20
    target 229
  ]
  edge [
    source 20
    target 230
  ]
  edge [
    source 20
    target 231
  ]
  edge [
    source 20
    target 232
  ]
  edge [
    source 20
    target 233
  ]
  edge [
    source 20
    target 234
  ]
  edge [
    source 20
    target 235
  ]
  edge [
    source 20
    target 236
  ]
  edge [
    source 20
    target 237
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 238
  ]
  edge [
    source 21
    target 239
  ]
  edge [
    source 21
    target 240
  ]
  edge [
    source 21
    target 241
  ]
  edge [
    source 21
    target 242
  ]
  edge [
    source 21
    target 158
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 243
  ]
  edge [
    source 23
    target 244
  ]
  edge [
    source 23
    target 245
  ]
  edge [
    source 23
    target 246
  ]
  edge [
    source 23
    target 247
  ]
  edge [
    source 23
    target 248
  ]
  edge [
    source 23
    target 249
  ]
  edge [
    source 23
    target 250
  ]
  edge [
    source 23
    target 251
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 252
  ]
  edge [
    source 24
    target 253
  ]
  edge [
    source 24
    target 254
  ]
  edge [
    source 24
    target 255
  ]
  edge [
    source 24
    target 256
  ]
  edge [
    source 24
    target 257
  ]
  edge [
    source 24
    target 258
  ]
  edge [
    source 24
    target 259
  ]
  edge [
    source 24
    target 260
  ]
  edge [
    source 24
    target 261
  ]
  edge [
    source 24
    target 262
  ]
  edge [
    source 24
    target 263
  ]
  edge [
    source 24
    target 264
  ]
  edge [
    source 24
    target 265
  ]
  edge [
    source 24
    target 266
  ]
  edge [
    source 24
    target 267
  ]
  edge [
    source 24
    target 268
  ]
  edge [
    source 24
    target 269
  ]
  edge [
    source 24
    target 270
  ]
  edge [
    source 24
    target 271
  ]
  edge [
    source 24
    target 272
  ]
  edge [
    source 24
    target 273
  ]
  edge [
    source 24
    target 274
  ]
  edge [
    source 24
    target 275
  ]
  edge [
    source 24
    target 276
  ]
  edge [
    source 24
    target 277
  ]
  edge [
    source 24
    target 278
  ]
  edge [
    source 24
    target 279
  ]
  edge [
    source 24
    target 280
  ]
  edge [
    source 24
    target 281
  ]
  edge [
    source 24
    target 282
  ]
  edge [
    source 24
    target 283
  ]
  edge [
    source 24
    target 284
  ]
  edge [
    source 24
    target 285
  ]
  edge [
    source 24
    target 286
  ]
  edge [
    source 24
    target 287
  ]
  edge [
    source 24
    target 288
  ]
  edge [
    source 24
    target 289
  ]
  edge [
    source 24
    target 290
  ]
  edge [
    source 24
    target 291
  ]
  edge [
    source 24
    target 292
  ]
  edge [
    source 24
    target 293
  ]
  edge [
    source 24
    target 294
  ]
  edge [
    source 24
    target 295
  ]
  edge [
    source 24
    target 296
  ]
  edge [
    source 24
    target 297
  ]
  edge [
    source 24
    target 298
  ]
  edge [
    source 24
    target 299
  ]
  edge [
    source 24
    target 300
  ]
  edge [
    source 24
    target 301
  ]
  edge [
    source 24
    target 302
  ]
  edge [
    source 24
    target 303
  ]
  edge [
    source 24
    target 304
  ]
  edge [
    source 24
    target 305
  ]
  edge [
    source 24
    target 306
  ]
  edge [
    source 24
    target 307
  ]
  edge [
    source 24
    target 308
  ]
  edge [
    source 24
    target 309
  ]
  edge [
    source 24
    target 310
  ]
  edge [
    source 24
    target 311
  ]
  edge [
    source 24
    target 312
  ]
  edge [
    source 24
    target 313
  ]
  edge [
    source 24
    target 314
  ]
  edge [
    source 24
    target 315
  ]
  edge [
    source 24
    target 316
  ]
  edge [
    source 24
    target 317
  ]
  edge [
    source 24
    target 318
  ]
  edge [
    source 24
    target 319
  ]
  edge [
    source 24
    target 320
  ]
  edge [
    source 24
    target 321
  ]
  edge [
    source 24
    target 322
  ]
  edge [
    source 24
    target 323
  ]
  edge [
    source 24
    target 324
  ]
  edge [
    source 24
    target 325
  ]
  edge [
    source 24
    target 326
  ]
  edge [
    source 24
    target 327
  ]
  edge [
    source 24
    target 328
  ]
  edge [
    source 24
    target 329
  ]
  edge [
    source 24
    target 330
  ]
  edge [
    source 24
    target 331
  ]
  edge [
    source 24
    target 332
  ]
  edge [
    source 24
    target 333
  ]
  edge [
    source 24
    target 334
  ]
  edge [
    source 24
    target 335
  ]
  edge [
    source 24
    target 336
  ]
  edge [
    source 24
    target 337
  ]
  edge [
    source 24
    target 338
  ]
  edge [
    source 24
    target 339
  ]
  edge [
    source 24
    target 340
  ]
  edge [
    source 24
    target 341
  ]
  edge [
    source 24
    target 342
  ]
  edge [
    source 24
    target 343
  ]
  edge [
    source 24
    target 344
  ]
  edge [
    source 24
    target 345
  ]
  edge [
    source 24
    target 346
  ]
  edge [
    source 24
    target 347
  ]
  edge [
    source 24
    target 348
  ]
  edge [
    source 24
    target 349
  ]
  edge [
    source 24
    target 350
  ]
  edge [
    source 24
    target 351
  ]
  edge [
    source 24
    target 352
  ]
  edge [
    source 24
    target 353
  ]
  edge [
    source 24
    target 354
  ]
  edge [
    source 24
    target 355
  ]
  edge [
    source 24
    target 356
  ]
  edge [
    source 24
    target 357
  ]
  edge [
    source 24
    target 358
  ]
  edge [
    source 24
    target 359
  ]
  edge [
    source 24
    target 360
  ]
  edge [
    source 24
    target 361
  ]
  edge [
    source 24
    target 362
  ]
  edge [
    source 24
    target 363
  ]
  edge [
    source 24
    target 364
  ]
  edge [
    source 24
    target 365
  ]
  edge [
    source 24
    target 366
  ]
  edge [
    source 24
    target 367
  ]
  edge [
    source 24
    target 368
  ]
  edge [
    source 24
    target 369
  ]
  edge [
    source 24
    target 370
  ]
  edge [
    source 24
    target 371
  ]
  edge [
    source 24
    target 372
  ]
  edge [
    source 24
    target 373
  ]
  edge [
    source 24
    target 374
  ]
  edge [
    source 24
    target 375
  ]
  edge [
    source 24
    target 376
  ]
  edge [
    source 24
    target 377
  ]
  edge [
    source 24
    target 378
  ]
  edge [
    source 24
    target 379
  ]
  edge [
    source 24
    target 380
  ]
  edge [
    source 24
    target 381
  ]
  edge [
    source 24
    target 382
  ]
  edge [
    source 24
    target 383
  ]
  edge [
    source 24
    target 384
  ]
  edge [
    source 24
    target 385
  ]
  edge [
    source 24
    target 386
  ]
  edge [
    source 24
    target 387
  ]
  edge [
    source 24
    target 388
  ]
  edge [
    source 24
    target 389
  ]
  edge [
    source 24
    target 390
  ]
  edge [
    source 24
    target 391
  ]
  edge [
    source 24
    target 392
  ]
  edge [
    source 24
    target 393
  ]
  edge [
    source 24
    target 394
  ]
  edge [
    source 24
    target 70
  ]
  edge [
    source 24
    target 395
  ]
  edge [
    source 24
    target 396
  ]
  edge [
    source 24
    target 397
  ]
  edge [
    source 24
    target 398
  ]
  edge [
    source 24
    target 399
  ]
  edge [
    source 24
    target 400
  ]
  edge [
    source 24
    target 401
  ]
  edge [
    source 24
    target 402
  ]
  edge [
    source 24
    target 403
  ]
  edge [
    source 24
    target 404
  ]
  edge [
    source 24
    target 405
  ]
  edge [
    source 24
    target 406
  ]
  edge [
    source 24
    target 407
  ]
  edge [
    source 24
    target 408
  ]
  edge [
    source 24
    target 409
  ]
  edge [
    source 24
    target 410
  ]
  edge [
    source 24
    target 411
  ]
  edge [
    source 24
    target 412
  ]
  edge [
    source 24
    target 413
  ]
  edge [
    source 24
    target 414
  ]
  edge [
    source 24
    target 415
  ]
  edge [
    source 24
    target 416
  ]
  edge [
    source 24
    target 417
  ]
  edge [
    source 24
    target 418
  ]
  edge [
    source 24
    target 419
  ]
  edge [
    source 24
    target 420
  ]
  edge [
    source 24
    target 421
  ]
  edge [
    source 24
    target 422
  ]
  edge [
    source 24
    target 423
  ]
  edge [
    source 24
    target 424
  ]
  edge [
    source 24
    target 425
  ]
  edge [
    source 24
    target 426
  ]
  edge [
    source 24
    target 427
  ]
  edge [
    source 24
    target 428
  ]
  edge [
    source 24
    target 429
  ]
  edge [
    source 24
    target 430
  ]
  edge [
    source 24
    target 431
  ]
  edge [
    source 24
    target 432
  ]
  edge [
    source 24
    target 433
  ]
  edge [
    source 24
    target 434
  ]
  edge [
    source 24
    target 435
  ]
  edge [
    source 24
    target 436
  ]
  edge [
    source 24
    target 437
  ]
  edge [
    source 24
    target 438
  ]
  edge [
    source 24
    target 439
  ]
  edge [
    source 24
    target 440
  ]
  edge [
    source 24
    target 441
  ]
  edge [
    source 24
    target 442
  ]
  edge [
    source 24
    target 443
  ]
  edge [
    source 24
    target 444
  ]
  edge [
    source 24
    target 445
  ]
  edge [
    source 24
    target 446
  ]
  edge [
    source 24
    target 447
  ]
  edge [
    source 24
    target 448
  ]
  edge [
    source 24
    target 449
  ]
  edge [
    source 24
    target 450
  ]
  edge [
    source 24
    target 451
  ]
  edge [
    source 24
    target 452
  ]
  edge [
    source 24
    target 453
  ]
  edge [
    source 24
    target 454
  ]
  edge [
    source 24
    target 455
  ]
  edge [
    source 24
    target 456
  ]
  edge [
    source 24
    target 457
  ]
  edge [
    source 24
    target 458
  ]
  edge [
    source 24
    target 459
  ]
  edge [
    source 24
    target 460
  ]
  edge [
    source 24
    target 461
  ]
  edge [
    source 24
    target 462
  ]
  edge [
    source 24
    target 463
  ]
  edge [
    source 24
    target 464
  ]
  edge [
    source 24
    target 465
  ]
  edge [
    source 24
    target 466
  ]
  edge [
    source 24
    target 467
  ]
  edge [
    source 24
    target 468
  ]
  edge [
    source 24
    target 469
  ]
  edge [
    source 24
    target 470
  ]
  edge [
    source 24
    target 471
  ]
  edge [
    source 24
    target 472
  ]
  edge [
    source 24
    target 473
  ]
  edge [
    source 24
    target 474
  ]
  edge [
    source 24
    target 475
  ]
  edge [
    source 24
    target 476
  ]
  edge [
    source 24
    target 477
  ]
  edge [
    source 24
    target 478
  ]
  edge [
    source 24
    target 479
  ]
  edge [
    source 24
    target 480
  ]
  edge [
    source 24
    target 481
  ]
  edge [
    source 24
    target 482
  ]
  edge [
    source 24
    target 483
  ]
  edge [
    source 24
    target 484
  ]
  edge [
    source 24
    target 485
  ]
  edge [
    source 24
    target 486
  ]
  edge [
    source 24
    target 487
  ]
  edge [
    source 24
    target 488
  ]
  edge [
    source 24
    target 489
  ]
  edge [
    source 24
    target 490
  ]
  edge [
    source 24
    target 491
  ]
  edge [
    source 24
    target 492
  ]
  edge [
    source 24
    target 493
  ]
  edge [
    source 24
    target 494
  ]
  edge [
    source 24
    target 495
  ]
  edge [
    source 24
    target 496
  ]
  edge [
    source 24
    target 497
  ]
  edge [
    source 24
    target 498
  ]
  edge [
    source 24
    target 499
  ]
  edge [
    source 24
    target 500
  ]
  edge [
    source 24
    target 501
  ]
  edge [
    source 24
    target 502
  ]
  edge [
    source 24
    target 503
  ]
  edge [
    source 24
    target 504
  ]
  edge [
    source 24
    target 505
  ]
  edge [
    source 24
    target 506
  ]
  edge [
    source 24
    target 507
  ]
  edge [
    source 24
    target 508
  ]
  edge [
    source 24
    target 509
  ]
  edge [
    source 24
    target 510
  ]
  edge [
    source 24
    target 511
  ]
  edge [
    source 24
    target 512
  ]
  edge [
    source 24
    target 513
  ]
  edge [
    source 24
    target 514
  ]
  edge [
    source 24
    target 515
  ]
  edge [
    source 24
    target 516
  ]
  edge [
    source 24
    target 517
  ]
  edge [
    source 24
    target 518
  ]
  edge [
    source 24
    target 519
  ]
  edge [
    source 24
    target 520
  ]
  edge [
    source 24
    target 521
  ]
  edge [
    source 24
    target 522
  ]
  edge [
    source 24
    target 523
  ]
  edge [
    source 24
    target 524
  ]
  edge [
    source 24
    target 525
  ]
  edge [
    source 24
    target 526
  ]
  edge [
    source 24
    target 527
  ]
  edge [
    source 24
    target 528
  ]
  edge [
    source 24
    target 529
  ]
  edge [
    source 24
    target 530
  ]
  edge [
    source 24
    target 531
  ]
  edge [
    source 24
    target 532
  ]
  edge [
    source 24
    target 533
  ]
  edge [
    source 24
    target 534
  ]
  edge [
    source 24
    target 535
  ]
  edge [
    source 24
    target 536
  ]
  edge [
    source 24
    target 537
  ]
  edge [
    source 24
    target 538
  ]
  edge [
    source 24
    target 539
  ]
  edge [
    source 24
    target 540
  ]
  edge [
    source 24
    target 541
  ]
  edge [
    source 24
    target 542
  ]
  edge [
    source 24
    target 543
  ]
  edge [
    source 24
    target 544
  ]
  edge [
    source 24
    target 545
  ]
  edge [
    source 24
    target 546
  ]
  edge [
    source 24
    target 547
  ]
  edge [
    source 24
    target 548
  ]
  edge [
    source 24
    target 549
  ]
  edge [
    source 24
    target 550
  ]
  edge [
    source 24
    target 551
  ]
  edge [
    source 24
    target 552
  ]
  edge [
    source 24
    target 553
  ]
  edge [
    source 24
    target 554
  ]
  edge [
    source 24
    target 555
  ]
  edge [
    source 24
    target 556
  ]
  edge [
    source 24
    target 557
  ]
  edge [
    source 24
    target 558
  ]
  edge [
    source 24
    target 559
  ]
  edge [
    source 24
    target 560
  ]
  edge [
    source 24
    target 561
  ]
  edge [
    source 24
    target 562
  ]
  edge [
    source 24
    target 563
  ]
  edge [
    source 24
    target 564
  ]
  edge [
    source 24
    target 565
  ]
  edge [
    source 24
    target 566
  ]
  edge [
    source 24
    target 567
  ]
  edge [
    source 24
    target 568
  ]
  edge [
    source 24
    target 569
  ]
  edge [
    source 24
    target 570
  ]
  edge [
    source 24
    target 571
  ]
  edge [
    source 24
    target 572
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 573
  ]
  edge [
    source 25
    target 574
  ]
  edge [
    source 25
    target 575
  ]
  edge [
    source 25
    target 576
  ]
  edge [
    source 25
    target 577
  ]
  edge [
    source 25
    target 578
  ]
  edge [
    source 25
    target 579
  ]
  edge [
    source 25
    target 580
  ]
  edge [
    source 25
    target 581
  ]
  edge [
    source 25
    target 582
  ]
  edge [
    source 25
    target 583
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 584
  ]
  edge [
    source 27
    target 585
  ]
  edge [
    source 27
    target 586
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 587
  ]
  edge [
    source 28
    target 588
  ]
  edge [
    source 28
    target 589
  ]
  edge [
    source 28
    target 590
  ]
  edge [
    source 28
    target 591
  ]
  edge [
    source 28
    target 592
  ]
  edge [
    source 28
    target 593
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 594
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 595
  ]
  edge [
    source 37
    target 596
  ]
  edge [
    source 37
    target 597
  ]
  edge [
    source 37
    target 598
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 599
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 600
  ]
  edge [
    source 45
    target 601
  ]
  edge [
    source 45
    target 602
  ]
  edge [
    source 45
    target 232
  ]
  edge [
    source 45
    target 603
  ]
  edge [
    source 45
    target 68
  ]
  edge [
    source 45
    target 604
  ]
  edge [
    source 45
    target 605
  ]
  edge [
    source 45
    target 606
  ]
  edge [
    source 45
    target 607
  ]
  edge [
    source 45
    target 608
  ]
  edge [
    source 45
    target 609
  ]
  edge [
    source 45
    target 610
  ]
  edge [
    source 45
    target 611
  ]
  edge [
    source 45
    target 612
  ]
  edge [
    source 45
    target 613
  ]
  edge [
    source 45
    target 614
  ]
  edge [
    source 45
    target 615
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 616
  ]
  edge [
    source 46
    target 617
  ]
  edge [
    source 46
    target 618
  ]
  edge [
    source 46
    target 619
  ]
  edge [
    source 46
    target 620
  ]
  edge [
    source 46
    target 621
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 622
  ]
  edge [
    source 47
    target 623
  ]
  edge [
    source 47
    target 624
  ]
  edge [
    source 47
    target 625
  ]
  edge [
    source 47
    target 626
  ]
  edge [
    source 47
    target 627
  ]
  edge [
    source 47
    target 628
  ]
  edge [
    source 47
    target 111
  ]
  edge [
    source 47
    target 629
  ]
  edge [
    source 47
    target 630
  ]
  edge [
    source 47
    target 631
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 632
  ]
  edge [
    source 48
    target 633
  ]
  edge [
    source 48
    target 634
  ]
  edge [
    source 49
    target 635
  ]
  edge [
    source 49
    target 636
  ]
  edge [
    source 49
    target 637
  ]
  edge [
    source 49
    target 638
  ]
]
