graph [
  maxDegree 12
  minDegree 1
  meanDegree 1.8888888888888888
  density 0.1111111111111111
  graphCliqueNumber 2
  node [
    id 0
    label "bombka"
    origin "text"
  ]
  node [
    id 1
    label "herb"
    origin "text"
  ]
  node [
    id 2
    label "szlachecki"
    origin "text"
  ]
  node [
    id 3
    label "bangle"
  ]
  node [
    id 4
    label "ozdoba"
  ]
  node [
    id 5
    label "trzymacz"
  ]
  node [
    id 6
    label "symbol"
  ]
  node [
    id 7
    label "tarcza_herbowa"
  ]
  node [
    id 8
    label "barwy"
  ]
  node [
    id 9
    label "heraldyka"
  ]
  node [
    id 10
    label "blazonowa&#263;"
  ]
  node [
    id 11
    label "blazonowanie"
  ]
  node [
    id 12
    label "klejnot_herbowy"
  ]
  node [
    id 13
    label "korona_rangowa"
  ]
  node [
    id 14
    label "znak"
  ]
  node [
    id 15
    label "szlachecko"
  ]
  node [
    id 16
    label "&#347;lachecki"
  ]
  node [
    id 17
    label "i"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
]
