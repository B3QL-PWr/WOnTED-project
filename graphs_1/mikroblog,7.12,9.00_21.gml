graph [
  maxDegree 12
  minDegree 1
  meanDegree 1.92
  density 0.08
  graphCliqueNumber 2
  node [
    id 0
    label "ogarn&#261;&#263;"
    origin "text"
  ]
  node [
    id 1
    label "wiek"
    origin "text"
  ]
  node [
    id 2
    label "ogl&#261;da&#263;"
    origin "text"
  ]
  node [
    id 3
    label "visit"
  ]
  node [
    id 4
    label "spowodowa&#263;"
  ]
  node [
    id 5
    label "spotka&#263;"
  ]
  node [
    id 6
    label "manipulate"
  ]
  node [
    id 7
    label "environment"
  ]
  node [
    id 8
    label "otoczy&#263;"
  ]
  node [
    id 9
    label "dotkn&#261;&#263;"
  ]
  node [
    id 10
    label "involve"
  ]
  node [
    id 11
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 12
    label "czas"
  ]
  node [
    id 13
    label "period"
  ]
  node [
    id 14
    label "rok"
  ]
  node [
    id 15
    label "cecha"
  ]
  node [
    id 16
    label "long_time"
  ]
  node [
    id 17
    label "choroba_wieku"
  ]
  node [
    id 18
    label "jednostka_geologiczna"
  ]
  node [
    id 19
    label "chron"
  ]
  node [
    id 20
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 21
    label "notice"
  ]
  node [
    id 22
    label "zapoznawa&#263;_si&#281;"
  ]
  node [
    id 23
    label "styka&#263;_si&#281;"
  ]
  node [
    id 24
    label "w&#322;&#261;cza&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
]
