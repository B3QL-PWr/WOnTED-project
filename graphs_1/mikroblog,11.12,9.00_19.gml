graph [
  maxDegree 15
  minDegree 1
  meanDegree 1.9555555555555555
  density 0.044444444444444446
  graphCliqueNumber 2
  node [
    id 0
    label "teza"
    origin "text"
  ]
  node [
    id 1
    label "czu&#263;"
    origin "text"
  ]
  node [
    id 2
    label "dawny"
    origin "text"
  ]
  node [
    id 3
    label "klimat"
    origin "text"
  ]
  node [
    id 4
    label "&#347;wi&#281;to"
    origin "text"
  ]
  node [
    id 5
    label "twierdzenie"
  ]
  node [
    id 6
    label "s&#261;d"
  ]
  node [
    id 7
    label "znaczenie"
  ]
  node [
    id 8
    label "argument"
  ]
  node [
    id 9
    label "idea"
  ]
  node [
    id 10
    label "dialektyka"
  ]
  node [
    id 11
    label "poj&#281;cie"
  ]
  node [
    id 12
    label "by&#263;"
  ]
  node [
    id 13
    label "uczuwa&#263;"
  ]
  node [
    id 14
    label "przewidywa&#263;"
  ]
  node [
    id 15
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 16
    label "smell"
  ]
  node [
    id 17
    label "postrzega&#263;"
  ]
  node [
    id 18
    label "doznawa&#263;"
  ]
  node [
    id 19
    label "spirit"
  ]
  node [
    id 20
    label "anticipate"
  ]
  node [
    id 21
    label "przesz&#322;y"
  ]
  node [
    id 22
    label "dawno"
  ]
  node [
    id 23
    label "dawniej"
  ]
  node [
    id 24
    label "kombatant"
  ]
  node [
    id 25
    label "stary"
  ]
  node [
    id 26
    label "odleg&#322;y"
  ]
  node [
    id 27
    label "anachroniczny"
  ]
  node [
    id 28
    label "przestarza&#322;y"
  ]
  node [
    id 29
    label "od_dawna"
  ]
  node [
    id 30
    label "poprzedni"
  ]
  node [
    id 31
    label "d&#322;ugoletni"
  ]
  node [
    id 32
    label "wcze&#347;niejszy"
  ]
  node [
    id 33
    label "niegdysiejszy"
  ]
  node [
    id 34
    label "zesp&#243;&#322;"
  ]
  node [
    id 35
    label "styl"
  ]
  node [
    id 36
    label "atmosfera"
  ]
  node [
    id 37
    label "&#346;wi&#281;to_Pracy"
  ]
  node [
    id 38
    label "czas"
  ]
  node [
    id 39
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 40
    label "Nowy_Rok"
  ]
  node [
    id 41
    label "Godowe_&#346;wi&#281;to"
  ]
  node [
    id 42
    label "dzie&#324;_&#347;wi&#261;teczny"
  ]
  node [
    id 43
    label "Barb&#243;rka"
  ]
  node [
    id 44
    label "ramadan"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
]
