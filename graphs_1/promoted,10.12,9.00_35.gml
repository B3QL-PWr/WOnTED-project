graph [
  maxDegree 15
  minDegree 1
  meanDegree 1.9534883720930232
  density 0.046511627906976744
  graphCliqueNumber 2
  node [
    id 0
    label "dzi&#347;"
    origin "text"
  ]
  node [
    id 1
    label "bezplanu"
    origin "text"
  ]
  node [
    id 2
    label "zabiera&#263;"
    origin "text"
  ]
  node [
    id 3
    label "petare"
    origin "text"
  ]
  node [
    id 4
    label "przedmie&#347;cie"
    origin "text"
  ]
  node [
    id 5
    label "caracas"
    origin "text"
  ]
  node [
    id 6
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 7
    label "slams"
    origin "text"
  ]
  node [
    id 8
    label "ameryka"
    origin "text"
  ]
  node [
    id 9
    label "po&#322;udniowy"
    origin "text"
  ]
  node [
    id 10
    label "doba"
  ]
  node [
    id 11
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 12
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 13
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 14
    label "zajmowa&#263;"
  ]
  node [
    id 15
    label "blurt_out"
  ]
  node [
    id 16
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 17
    label "liszy&#263;"
  ]
  node [
    id 18
    label "przenosi&#263;"
  ]
  node [
    id 19
    label "deprive"
  ]
  node [
    id 20
    label "poci&#261;ga&#263;"
  ]
  node [
    id 21
    label "abstract"
  ]
  node [
    id 22
    label "przesuwa&#263;"
  ]
  node [
    id 23
    label "&#322;apa&#263;"
  ]
  node [
    id 24
    label "fall"
  ]
  node [
    id 25
    label "konfiskowa&#263;"
  ]
  node [
    id 26
    label "prowadzi&#263;"
  ]
  node [
    id 27
    label "obrze&#380;e"
  ]
  node [
    id 28
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 29
    label "doros&#322;y"
  ]
  node [
    id 30
    label "wiele"
  ]
  node [
    id 31
    label "dorodny"
  ]
  node [
    id 32
    label "znaczny"
  ]
  node [
    id 33
    label "du&#380;o"
  ]
  node [
    id 34
    label "prawdziwy"
  ]
  node [
    id 35
    label "niema&#322;o"
  ]
  node [
    id 36
    label "wa&#380;ny"
  ]
  node [
    id 37
    label "rozwini&#281;ty"
  ]
  node [
    id 38
    label "dom"
  ]
  node [
    id 39
    label "gor&#261;cy"
  ]
  node [
    id 40
    label "s&#322;oneczny"
  ]
  node [
    id 41
    label "po&#322;udniowo"
  ]
  node [
    id 42
    label "Ameryka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 38
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 39
  ]
  edge [
    source 9
    target 40
  ]
  edge [
    source 9
    target 41
  ]
  edge [
    source 9
    target 42
  ]
]
