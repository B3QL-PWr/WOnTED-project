graph [
  maxDegree 3
  minDegree 1
  meanDegree 1.6
  density 0.4
  graphCliqueNumber 2
  node [
    id 0
    label "ewa"
    origin "text"
  ]
  node [
    id 1
    label "&#322;&#281;towska"
    origin "text"
  ]
  node [
    id 2
    label "przewodnicz&#261;cy"
    origin "text"
  ]
  node [
    id 3
    label "zwierzchnik"
  ]
  node [
    id 4
    label "prowadz&#261;cy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
]
