graph [
  maxDegree 11
  minDegree 1
  meanDegree 1.9
  density 0.1
  graphCliqueNumber 2
  node [
    id 0
    label "dzwonowo"
    origin "text"
  ]
  node [
    id 1
    label "wojew&#243;dztwo"
    origin "text"
  ]
  node [
    id 2
    label "wielkopolski"
    origin "text"
  ]
  node [
    id 3
    label "jednostka_administracyjna"
  ]
  node [
    id 4
    label "makroregion"
  ]
  node [
    id 5
    label "powiat"
  ]
  node [
    id 6
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 7
    label "mikroregion"
  ]
  node [
    id 8
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 9
    label "pa&#324;stwo"
  ]
  node [
    id 10
    label "po_wielkopolsku"
  ]
  node [
    id 11
    label "knypek"
  ]
  node [
    id 12
    label "kwyrla"
  ]
  node [
    id 13
    label "bimba"
  ]
  node [
    id 14
    label "hy&#263;ka"
  ]
  node [
    id 15
    label "plyndz"
  ]
  node [
    id 16
    label "regionalny"
  ]
  node [
    id 17
    label "gzik"
  ]
  node [
    id 18
    label "polski"
  ]
  node [
    id 19
    label "myrdyrda"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
]
