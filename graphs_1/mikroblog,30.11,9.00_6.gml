graph [
  maxDegree 30
  minDegree 1
  meanDegree 2
  density 0.016666666666666666
  graphCliqueNumber 2
  node [
    id 0
    label "p&#243;j&#347;&#263;"
    origin "text"
  ]
  node [
    id 1
    label "jutro"
    origin "text"
  ]
  node [
    id 2
    label "pod"
    origin "text"
  ]
  node [
    id 3
    label "buda"
    origin "text"
  ]
  node [
    id 4
    label "wstawi&#263;"
    origin "text"
  ]
  node [
    id 5
    label "zdj&#281;cie"
    origin "text"
  ]
  node [
    id 6
    label "wygl&#261;da&#263;"
    origin "text"
  ]
  node [
    id 7
    label "dzi&#347;"
    origin "text"
  ]
  node [
    id 8
    label "juz"
    origin "text"
  ]
  node [
    id 9
    label "by&#263;"
    origin "text"
  ]
  node [
    id 10
    label "zbyt"
    origin "text"
  ]
  node [
    id 11
    label "najeba&#263;"
    origin "text"
  ]
  node [
    id 12
    label "kuchennerewolucje"
    origin "text"
  ]
  node [
    id 13
    label "opu&#347;ci&#263;"
  ]
  node [
    id 14
    label "poruszy&#263;_si&#281;"
  ]
  node [
    id 15
    label "proceed"
  ]
  node [
    id 16
    label "powie&#347;&#263;_si&#281;"
  ]
  node [
    id 17
    label "popsu&#263;_si&#281;"
  ]
  node [
    id 18
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 19
    label "kopn&#261;&#263;_si&#281;"
  ]
  node [
    id 20
    label "zacz&#261;&#263;"
  ]
  node [
    id 21
    label "zmieni&#263;"
  ]
  node [
    id 22
    label "zosta&#263;"
  ]
  node [
    id 23
    label "sail"
  ]
  node [
    id 24
    label "leave"
  ]
  node [
    id 25
    label "uda&#263;_si&#281;"
  ]
  node [
    id 26
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 27
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 28
    label "zrobi&#263;"
  ]
  node [
    id 29
    label "wyp&#322;yn&#261;&#263;"
  ]
  node [
    id 30
    label "sprzeda&#263;_si&#281;"
  ]
  node [
    id 31
    label "przyj&#261;&#263;"
  ]
  node [
    id 32
    label "potoczy&#263;_si&#281;"
  ]
  node [
    id 33
    label "become"
  ]
  node [
    id 34
    label "play_along"
  ]
  node [
    id 35
    label "travel"
  ]
  node [
    id 36
    label "blisko"
  ]
  node [
    id 37
    label "przysz&#322;o&#347;&#263;"
  ]
  node [
    id 38
    label "dzie&#324;_jutrzejszy"
  ]
  node [
    id 39
    label "jutrzejszy"
  ]
  node [
    id 40
    label "dzie&#324;"
  ]
  node [
    id 41
    label "Mickiewicz"
  ]
  node [
    id 42
    label "przedmiot"
  ]
  node [
    id 43
    label "budynek"
  ]
  node [
    id 44
    label "obstawia&#263;"
  ]
  node [
    id 45
    label "barak"
  ]
  node [
    id 46
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 47
    label "obstawianie"
  ]
  node [
    id 48
    label "radiow&#243;z"
  ]
  node [
    id 49
    label "shed"
  ]
  node [
    id 50
    label "obstawienie"
  ]
  node [
    id 51
    label "stopek"
  ]
  node [
    id 52
    label "siatka"
  ]
  node [
    id 53
    label "poprzeczka"
  ]
  node [
    id 54
    label "nadwozie"
  ]
  node [
    id 55
    label "obstawi&#263;"
  ]
  node [
    id 56
    label "gabinet"
  ]
  node [
    id 57
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 58
    label "paka"
  ]
  node [
    id 59
    label "boisko"
  ]
  node [
    id 60
    label "siedziba"
  ]
  node [
    id 61
    label "s&#322;upek"
  ]
  node [
    id 62
    label "sekretariat"
  ]
  node [
    id 63
    label "tablica"
  ]
  node [
    id 64
    label "teren_szko&#322;y"
  ]
  node [
    id 65
    label "klasa"
  ]
  node [
    id 66
    label "insert"
  ]
  node [
    id 67
    label "plant"
  ]
  node [
    id 68
    label "umie&#347;ci&#263;"
  ]
  node [
    id 69
    label "wyretuszowanie"
  ]
  node [
    id 70
    label "podlew"
  ]
  node [
    id 71
    label "po&#347;ci&#261;ganie"
  ]
  node [
    id 72
    label "cenzura"
  ]
  node [
    id 73
    label "legitymacja"
  ]
  node [
    id 74
    label "uniewa&#380;nienie"
  ]
  node [
    id 75
    label "abolicjonista"
  ]
  node [
    id 76
    label "withdrawal"
  ]
  node [
    id 77
    label "uwolnienie"
  ]
  node [
    id 78
    label "picture"
  ]
  node [
    id 79
    label "retuszowa&#263;"
  ]
  node [
    id 80
    label "fota"
  ]
  node [
    id 81
    label "obraz"
  ]
  node [
    id 82
    label "fototeka"
  ]
  node [
    id 83
    label "zrobienie"
  ]
  node [
    id 84
    label "retuszowanie"
  ]
  node [
    id 85
    label "monid&#322;o"
  ]
  node [
    id 86
    label "talbotypia"
  ]
  node [
    id 87
    label "relief"
  ]
  node [
    id 88
    label "wyretuszowa&#263;"
  ]
  node [
    id 89
    label "photograph"
  ]
  node [
    id 90
    label "zabronienie"
  ]
  node [
    id 91
    label "ziarno"
  ]
  node [
    id 92
    label "przepa&#322;"
  ]
  node [
    id 93
    label "fotogaleria"
  ]
  node [
    id 94
    label "cinch"
  ]
  node [
    id 95
    label "odsuni&#281;cie"
  ]
  node [
    id 96
    label "rozpakowanie"
  ]
  node [
    id 97
    label "czeka&#263;"
  ]
  node [
    id 98
    label "lookout"
  ]
  node [
    id 99
    label "wyziera&#263;"
  ]
  node [
    id 100
    label "peep"
  ]
  node [
    id 101
    label "look"
  ]
  node [
    id 102
    label "patrze&#263;"
  ]
  node [
    id 103
    label "doba"
  ]
  node [
    id 104
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 105
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 106
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 107
    label "si&#281;ga&#263;"
  ]
  node [
    id 108
    label "trwa&#263;"
  ]
  node [
    id 109
    label "obecno&#347;&#263;"
  ]
  node [
    id 110
    label "stan"
  ]
  node [
    id 111
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 112
    label "stand"
  ]
  node [
    id 113
    label "mie&#263;_miejsce"
  ]
  node [
    id 114
    label "uczestniczy&#263;"
  ]
  node [
    id 115
    label "chodzi&#263;"
  ]
  node [
    id 116
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 117
    label "equal"
  ]
  node [
    id 118
    label "nadmiernie"
  ]
  node [
    id 119
    label "sprzedawanie"
  ]
  node [
    id 120
    label "sprzeda&#380;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 9
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 9
    target 116
  ]
  edge [
    source 9
    target 117
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 11
    target 12
  ]
]
