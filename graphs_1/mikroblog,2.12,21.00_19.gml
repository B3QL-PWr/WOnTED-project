graph [
  maxDegree 18
  minDegree 1
  meanDegree 1.8823529411764706
  density 0.0570409982174688
  graphCliqueNumber 2
  node [
    id 0
    label "my&#347;l"
    origin "text"
  ]
  node [
    id 1
    label "nar&#243;d"
    origin "text"
  ]
  node [
    id 2
    label "system"
  ]
  node [
    id 3
    label "s&#261;d"
  ]
  node [
    id 4
    label "wytw&#243;r"
  ]
  node [
    id 5
    label "istota"
  ]
  node [
    id 6
    label "thinking"
  ]
  node [
    id 7
    label "idea"
  ]
  node [
    id 8
    label "political_orientation"
  ]
  node [
    id 9
    label "pomys&#322;"
  ]
  node [
    id 10
    label "szko&#322;a"
  ]
  node [
    id 11
    label "umys&#322;"
  ]
  node [
    id 12
    label "fantomatyka"
  ]
  node [
    id 13
    label "t&#322;oczenie_si&#281;"
  ]
  node [
    id 14
    label "p&#322;&#243;d"
  ]
  node [
    id 15
    label "Samojedzi"
  ]
  node [
    id 16
    label "nacja"
  ]
  node [
    id 17
    label "Aztekowie"
  ]
  node [
    id 18
    label "Buriaci"
  ]
  node [
    id 19
    label "Irokezi"
  ]
  node [
    id 20
    label "Komancze"
  ]
  node [
    id 21
    label "t&#322;um"
  ]
  node [
    id 22
    label "ludno&#347;&#263;"
  ]
  node [
    id 23
    label "lud"
  ]
  node [
    id 24
    label "Czejenowie"
  ]
  node [
    id 25
    label "Siuksowie"
  ]
  node [
    id 26
    label "Wotiacy"
  ]
  node [
    id 27
    label "Baszkirzy"
  ]
  node [
    id 28
    label "Apacze"
  ]
  node [
    id 29
    label "mniejszo&#347;&#263;_narodowa"
  ]
  node [
    id 30
    label "Mohikanie"
  ]
  node [
    id 31
    label "Syngalezi"
  ]
  node [
    id 32
    label "XD"
  ]
  node [
    id 33
    label "ojeba&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 32
    target 33
  ]
]
