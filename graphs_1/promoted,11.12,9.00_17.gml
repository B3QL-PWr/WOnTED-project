graph [
  maxDegree 40
  minDegree 1
  meanDegree 2.1710526315789473
  density 0.014377831997211573
  graphCliqueNumber 4
  node [
    id 0
    label "prokuratura"
    origin "text"
  ]
  node [
    id 1
    label "okr&#281;gowy"
    origin "text"
  ]
  node [
    id 2
    label "gda&#324;ski"
    origin "text"
  ]
  node [
    id 3
    label "skierowa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "s&#261;d"
    origin "text"
  ]
  node [
    id 5
    label "akt"
    origin "text"
  ]
  node [
    id 6
    label "oskar&#380;enie"
    origin "text"
  ]
  node [
    id 7
    label "sprawa"
    origin "text"
  ]
  node [
    id 8
    label "brutalny"
    origin "text"
  ]
  node [
    id 9
    label "atak"
    origin "text"
  ]
  node [
    id 10
    label "obywatel"
    origin "text"
  ]
  node [
    id 11
    label "szwecja"
    origin "text"
  ]
  node [
    id 12
    label "maj"
    origin "text"
  ]
  node [
    id 13
    label "siedziba"
  ]
  node [
    id 14
    label "urz&#261;d"
  ]
  node [
    id 15
    label "organ"
  ]
  node [
    id 16
    label "zdobny"
  ]
  node [
    id 17
    label "barokowy"
  ]
  node [
    id 18
    label "pomorski"
  ]
  node [
    id 19
    label "renesansowy"
  ]
  node [
    id 20
    label "po_gda&#324;sku"
  ]
  node [
    id 21
    label "masywny"
  ]
  node [
    id 22
    label "return"
  ]
  node [
    id 23
    label "podpowiedzie&#263;"
  ]
  node [
    id 24
    label "direct"
  ]
  node [
    id 25
    label "dispatch"
  ]
  node [
    id 26
    label "przeznaczy&#263;"
  ]
  node [
    id 27
    label "ustawi&#263;"
  ]
  node [
    id 28
    label "wys&#322;a&#263;"
  ]
  node [
    id 29
    label "precede"
  ]
  node [
    id 30
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 31
    label "set"
  ]
  node [
    id 32
    label "procesowicz"
  ]
  node [
    id 33
    label "wypowied&#378;"
  ]
  node [
    id 34
    label "pods&#261;dny"
  ]
  node [
    id 35
    label "podejrzany"
  ]
  node [
    id 36
    label "broni&#263;"
  ]
  node [
    id 37
    label "bronienie"
  ]
  node [
    id 38
    label "system"
  ]
  node [
    id 39
    label "my&#347;l"
  ]
  node [
    id 40
    label "wytw&#243;r"
  ]
  node [
    id 41
    label "konektyw"
  ]
  node [
    id 42
    label "court"
  ]
  node [
    id 43
    label "obrona"
  ]
  node [
    id 44
    label "s&#261;downictwo"
  ]
  node [
    id 45
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 46
    label "forum"
  ]
  node [
    id 47
    label "zesp&#243;&#322;"
  ]
  node [
    id 48
    label "post&#281;powanie"
  ]
  node [
    id 49
    label "skazany"
  ]
  node [
    id 50
    label "wydarzenie"
  ]
  node [
    id 51
    label "&#347;wiadek"
  ]
  node [
    id 52
    label "antylogizm"
  ]
  node [
    id 53
    label "strona"
  ]
  node [
    id 54
    label "oskar&#380;yciel"
  ]
  node [
    id 55
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 56
    label "biuro"
  ]
  node [
    id 57
    label "instytucja"
  ]
  node [
    id 58
    label "certificate"
  ]
  node [
    id 59
    label "erotyka"
  ]
  node [
    id 60
    label "podniecanie"
  ]
  node [
    id 61
    label "wzw&#243;d"
  ]
  node [
    id 62
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 63
    label "rozmna&#380;anie"
  ]
  node [
    id 64
    label "ontologia"
  ]
  node [
    id 65
    label "fascyku&#322;"
  ]
  node [
    id 66
    label "fragment"
  ]
  node [
    id 67
    label "po&#380;&#261;danie"
  ]
  node [
    id 68
    label "imisja"
  ]
  node [
    id 69
    label "po&#380;ycie"
  ]
  node [
    id 70
    label "pozycja_misjonarska"
  ]
  node [
    id 71
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 72
    label "podnieci&#263;"
  ]
  node [
    id 73
    label "podnieca&#263;"
  ]
  node [
    id 74
    label "funkcja"
  ]
  node [
    id 75
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 76
    label "czynno&#347;&#263;"
  ]
  node [
    id 77
    label "urzeczywistnienie"
  ]
  node [
    id 78
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 79
    label "gra_wst&#281;pna"
  ]
  node [
    id 80
    label "scena"
  ]
  node [
    id 81
    label "nago&#347;&#263;"
  ]
  node [
    id 82
    label "poj&#281;cie"
  ]
  node [
    id 83
    label "numer"
  ]
  node [
    id 84
    label "ruch_frykcyjny"
  ]
  node [
    id 85
    label "baraszki"
  ]
  node [
    id 86
    label "dokument"
  ]
  node [
    id 87
    label "na_pieska"
  ]
  node [
    id 88
    label "arystotelizm"
  ]
  node [
    id 89
    label "z&#322;&#261;czenie"
  ]
  node [
    id 90
    label "act"
  ]
  node [
    id 91
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 92
    label "seks"
  ]
  node [
    id 93
    label "zwyczaj"
  ]
  node [
    id 94
    label "podniecenie"
  ]
  node [
    id 95
    label "ocena"
  ]
  node [
    id 96
    label "ocenienie"
  ]
  node [
    id 97
    label "skar&#380;yciel"
  ]
  node [
    id 98
    label "suspicja"
  ]
  node [
    id 99
    label "temat"
  ]
  node [
    id 100
    label "kognicja"
  ]
  node [
    id 101
    label "idea"
  ]
  node [
    id 102
    label "szczeg&#243;&#322;"
  ]
  node [
    id 103
    label "rzecz"
  ]
  node [
    id 104
    label "przes&#322;anka"
  ]
  node [
    id 105
    label "rozprawa"
  ]
  node [
    id 106
    label "object"
  ]
  node [
    id 107
    label "proposition"
  ]
  node [
    id 108
    label "szczery"
  ]
  node [
    id 109
    label "drastycznie"
  ]
  node [
    id 110
    label "silny"
  ]
  node [
    id 111
    label "bezlitosny"
  ]
  node [
    id 112
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 113
    label "okrutny"
  ]
  node [
    id 114
    label "brutalnie"
  ]
  node [
    id 115
    label "przemoc"
  ]
  node [
    id 116
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 117
    label "mocny"
  ]
  node [
    id 118
    label "niedelikatny"
  ]
  node [
    id 119
    label "manewr"
  ]
  node [
    id 120
    label "spasm"
  ]
  node [
    id 121
    label "&#380;&#261;danie"
  ]
  node [
    id 122
    label "rzucenie"
  ]
  node [
    id 123
    label "pogorszenie"
  ]
  node [
    id 124
    label "krytyka"
  ]
  node [
    id 125
    label "stroke"
  ]
  node [
    id 126
    label "liga"
  ]
  node [
    id 127
    label "pozycja"
  ]
  node [
    id 128
    label "zagrywka"
  ]
  node [
    id 129
    label "rzuci&#263;"
  ]
  node [
    id 130
    label "walka"
  ]
  node [
    id 131
    label "pogoda"
  ]
  node [
    id 132
    label "przyp&#322;yw"
  ]
  node [
    id 133
    label "fit"
  ]
  node [
    id 134
    label "ofensywa"
  ]
  node [
    id 135
    label "knock"
  ]
  node [
    id 136
    label "oznaka"
  ]
  node [
    id 137
    label "bat"
  ]
  node [
    id 138
    label "kaszel"
  ]
  node [
    id 139
    label "cz&#322;owiek"
  ]
  node [
    id 140
    label "przedstawiciel"
  ]
  node [
    id 141
    label "miastowy"
  ]
  node [
    id 142
    label "mieszkaniec"
  ]
  node [
    id 143
    label "pa&#324;stwo"
  ]
  node [
    id 144
    label "&#346;wi&#281;to_Pracy"
  ]
  node [
    id 145
    label "miesi&#261;c"
  ]
  node [
    id 146
    label "w"
  ]
  node [
    id 147
    label "RP"
  ]
  node [
    id 148
    label "Lech"
  ]
  node [
    id 149
    label "wa&#322;&#281;sa"
  ]
  node [
    id 150
    label "Bart&#322;omiej"
  ]
  node [
    id 151
    label "sprawca"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 119
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 33
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 129
  ]
  edge [
    source 9
    target 130
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 9
    target 131
  ]
  edge [
    source 9
    target 132
  ]
  edge [
    source 9
    target 133
  ]
  edge [
    source 9
    target 134
  ]
  edge [
    source 9
    target 135
  ]
  edge [
    source 9
    target 136
  ]
  edge [
    source 9
    target 137
  ]
  edge [
    source 9
    target 138
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 139
  ]
  edge [
    source 10
    target 140
  ]
  edge [
    source 10
    target 141
  ]
  edge [
    source 10
    target 142
  ]
  edge [
    source 10
    target 143
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 144
  ]
  edge [
    source 12
    target 145
  ]
  edge [
    source 146
    target 150
  ]
  edge [
    source 146
    target 151
  ]
  edge [
    source 147
    target 148
  ]
  edge [
    source 147
    target 149
  ]
  edge [
    source 148
    target 149
  ]
  edge [
    source 150
    target 151
  ]
]
