graph [
  maxDegree 11
  minDegree 1
  meanDegree 1.9259259259259258
  density 0.07407407407407407
  graphCliqueNumber 2
  node [
    id 0
    label "jedyny"
    origin "text"
  ]
  node [
    id 1
    label "argument"
    origin "text"
  ]
  node [
    id 2
    label "pseudo"
    origin "text"
  ]
  node [
    id 3
    label "katol"
    origin "text"
  ]
  node [
    id 4
    label "wykop"
    origin "text"
  ]
  node [
    id 5
    label "ukochany"
  ]
  node [
    id 6
    label "optymalnie"
  ]
  node [
    id 7
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 8
    label "najlepszy"
  ]
  node [
    id 9
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 10
    label "s&#261;d"
  ]
  node [
    id 11
    label "argumentacja"
  ]
  node [
    id 12
    label "parametr"
  ]
  node [
    id 13
    label "rzecz"
  ]
  node [
    id 14
    label "dow&#243;d"
  ]
  node [
    id 15
    label "operand"
  ]
  node [
    id 16
    label "zmienna"
  ]
  node [
    id 17
    label "odwa&#322;"
  ]
  node [
    id 18
    label "chody"
  ]
  node [
    id 19
    label "grodzisko"
  ]
  node [
    id 20
    label "budowa"
  ]
  node [
    id 21
    label "kopniak"
  ]
  node [
    id 22
    label "wyrobisko"
  ]
  node [
    id 23
    label "zrzutowy"
  ]
  node [
    id 24
    label "szaniec"
  ]
  node [
    id 25
    label "odk&#322;ad"
  ]
  node [
    id 26
    label "wg&#322;&#281;bienie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
]
