graph [
  maxDegree 22
  minDegree 1
  meanDegree 1.92
  density 0.08
  graphCliqueNumber 2
  node [
    id 0
    label "jesteeem"
    origin "text"
  ]
  node [
    id 1
    label "diaaabe&#322;"
    origin "text"
  ]
  node [
    id 2
    label "gra"
    origin "text"
  ]
  node [
    id 3
    label "heheszki"
    origin "text"
  ]
  node [
    id 4
    label "humorobrazkowy"
    origin "text"
  ]
  node [
    id 5
    label "zabawa"
  ]
  node [
    id 6
    label "rywalizacja"
  ]
  node [
    id 7
    label "czynno&#347;&#263;"
  ]
  node [
    id 8
    label "Pok&#233;mon"
  ]
  node [
    id 9
    label "synteza"
  ]
  node [
    id 10
    label "odtworzenie"
  ]
  node [
    id 11
    label "komplet"
  ]
  node [
    id 12
    label "rekwizyt_do_gry"
  ]
  node [
    id 13
    label "odg&#322;os"
  ]
  node [
    id 14
    label "rozgrywka"
  ]
  node [
    id 15
    label "post&#281;powanie"
  ]
  node [
    id 16
    label "wydarzenie"
  ]
  node [
    id 17
    label "apparent_motion"
  ]
  node [
    id 18
    label "game"
  ]
  node [
    id 19
    label "zmienno&#347;&#263;"
  ]
  node [
    id 20
    label "zasada"
  ]
  node [
    id 21
    label "akcja"
  ]
  node [
    id 22
    label "play"
  ]
  node [
    id 23
    label "contest"
  ]
  node [
    id 24
    label "zbijany"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 3
    target 4
  ]
]
