graph [
  maxDegree 15
  minDegree 1
  meanDegree 1.92
  density 0.08
  graphCliqueNumber 2
  node [
    id 0
    label "zrobi&#263;"
    origin "text"
  ]
  node [
    id 1
    label "reklama"
    origin "text"
  ]
  node [
    id 2
    label "zorganizowa&#263;"
  ]
  node [
    id 3
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 4
    label "przerobi&#263;"
  ]
  node [
    id 5
    label "wystylizowa&#263;"
  ]
  node [
    id 6
    label "cause"
  ]
  node [
    id 7
    label "wydali&#263;"
  ]
  node [
    id 8
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 9
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 10
    label "post&#261;pi&#263;"
  ]
  node [
    id 11
    label "appoint"
  ]
  node [
    id 12
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 13
    label "nabra&#263;"
  ]
  node [
    id 14
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 15
    label "make"
  ]
  node [
    id 16
    label "copywriting"
  ]
  node [
    id 17
    label "brief"
  ]
  node [
    id 18
    label "bran&#380;a"
  ]
  node [
    id 19
    label "informacja"
  ]
  node [
    id 20
    label "promowa&#263;"
  ]
  node [
    id 21
    label "akcja"
  ]
  node [
    id 22
    label "wypromowa&#263;"
  ]
  node [
    id 23
    label "samplowanie"
  ]
  node [
    id 24
    label "tekst"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
]
