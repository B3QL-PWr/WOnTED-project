graph [
  maxDegree 8
  minDegree 1
  meanDegree 1.7777777777777777
  density 0.2222222222222222
  graphCliqueNumber 2
  node [
    id 0
    label "podrywajzwykopem"
    origin "text"
  ]
  node [
    id 1
    label "policja"
    origin "text"
  ]
  node [
    id 2
    label "rozowepaski"
    origin "text"
  ]
  node [
    id 3
    label "komisariat"
  ]
  node [
    id 4
    label "psiarnia"
  ]
  node [
    id 5
    label "posterunek"
  ]
  node [
    id 6
    label "grupa"
  ]
  node [
    id 7
    label "organ"
  ]
  node [
    id 8
    label "s&#322;u&#380;ba"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
]
