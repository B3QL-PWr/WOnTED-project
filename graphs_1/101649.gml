graph [
  maxDegree 32
  minDegree 1
  meanDegree 2.0966183574879227
  density 0.010177759016931664
  graphCliqueNumber 3
  node [
    id 0
    label "czym"
    origin "text"
  ]
  node [
    id 1
    label "r&#243;&#380;ny"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "procedowany"
    origin "text"
  ]
  node [
    id 4
    label "dzi&#347;"
    origin "text"
  ]
  node [
    id 5
    label "projekt"
    origin "text"
  ]
  node [
    id 6
    label "ustawa"
    origin "text"
  ]
  node [
    id 7
    label "wczesny"
    origin "text"
  ]
  node [
    id 8
    label "opr&#243;cz"
    origin "text"
  ]
  node [
    id 9
    label "wskazanie"
    origin "text"
  ]
  node [
    id 10
    label "przepis"
    origin "text"
  ]
  node [
    id 11
    label "kolejny"
    origin "text"
  ]
  node [
    id 12
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 13
    label "wolny"
    origin "text"
  ]
  node [
    id 14
    label "przypada&#263;"
    origin "text"
  ]
  node [
    id 15
    label "stycze&#324;"
    origin "text"
  ]
  node [
    id 16
    label "likwidowa&#263;"
    origin "text"
  ]
  node [
    id 17
    label "obowi&#261;zek"
    origin "text"
  ]
  node [
    id 18
    label "zapewnienie"
    origin "text"
  ]
  node [
    id 19
    label "pracownik"
    origin "text"
  ]
  node [
    id 20
    label "dodatkowy"
    origin "text"
  ]
  node [
    id 21
    label "przypadek"
    origin "text"
  ]
  node [
    id 22
    label "kiedy"
    origin "text"
  ]
  node [
    id 23
    label "ustawowy"
    origin "text"
  ]
  node [
    id 24
    label "wolne"
    origin "text"
  ]
  node [
    id 25
    label "praca"
    origin "text"
  ]
  node [
    id 26
    label "&#347;wi&#281;to"
    origin "text"
  ]
  node [
    id 27
    label "sobota"
    origin "text"
  ]
  node [
    id 28
    label "r&#243;&#380;nie"
  ]
  node [
    id 29
    label "inny"
  ]
  node [
    id 30
    label "jaki&#347;"
  ]
  node [
    id 31
    label "doba"
  ]
  node [
    id 32
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 33
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 34
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 35
    label "dokument"
  ]
  node [
    id 36
    label "device"
  ]
  node [
    id 37
    label "program_u&#380;ytkowy"
  ]
  node [
    id 38
    label "intencja"
  ]
  node [
    id 39
    label "agreement"
  ]
  node [
    id 40
    label "pomys&#322;"
  ]
  node [
    id 41
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 42
    label "plan"
  ]
  node [
    id 43
    label "dokumentacja"
  ]
  node [
    id 44
    label "Karta_Nauczyciela"
  ]
  node [
    id 45
    label "marc&#243;wka"
  ]
  node [
    id 46
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 47
    label "akt"
  ]
  node [
    id 48
    label "przej&#347;&#263;"
  ]
  node [
    id 49
    label "charter"
  ]
  node [
    id 50
    label "przej&#347;cie"
  ]
  node [
    id 51
    label "pocz&#261;tkowy"
  ]
  node [
    id 52
    label "wcze&#347;nie"
  ]
  node [
    id 53
    label "wynik"
  ]
  node [
    id 54
    label "podkre&#347;lenie"
  ]
  node [
    id 55
    label "wybranie"
  ]
  node [
    id 56
    label "appointment"
  ]
  node [
    id 57
    label "podanie"
  ]
  node [
    id 58
    label "education"
  ]
  node [
    id 59
    label "przyczyna"
  ]
  node [
    id 60
    label "meaning"
  ]
  node [
    id 61
    label "wskaz&#243;wka"
  ]
  node [
    id 62
    label "pokazanie"
  ]
  node [
    id 63
    label "wyja&#347;nienie"
  ]
  node [
    id 64
    label "przedawnienie_si&#281;"
  ]
  node [
    id 65
    label "spos&#243;b"
  ]
  node [
    id 66
    label "norma_prawna"
  ]
  node [
    id 67
    label "kodeks"
  ]
  node [
    id 68
    label "prawo"
  ]
  node [
    id 69
    label "regulation"
  ]
  node [
    id 70
    label "przedawni&#263;_si&#281;"
  ]
  node [
    id 71
    label "porada"
  ]
  node [
    id 72
    label "przedawnianie_si&#281;"
  ]
  node [
    id 73
    label "recepta"
  ]
  node [
    id 74
    label "przedawnia&#263;_si&#281;"
  ]
  node [
    id 75
    label "nast&#281;pnie"
  ]
  node [
    id 76
    label "kt&#243;ry&#347;"
  ]
  node [
    id 77
    label "kolejno"
  ]
  node [
    id 78
    label "nastopny"
  ]
  node [
    id 79
    label "s&#322;o&#324;ce"
  ]
  node [
    id 80
    label "czynienie_si&#281;"
  ]
  node [
    id 81
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 82
    label "czas"
  ]
  node [
    id 83
    label "long_time"
  ]
  node [
    id 84
    label "przedpo&#322;udnie"
  ]
  node [
    id 85
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 86
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 87
    label "tydzie&#324;"
  ]
  node [
    id 88
    label "godzina"
  ]
  node [
    id 89
    label "t&#322;usty_czwartek"
  ]
  node [
    id 90
    label "wsta&#263;"
  ]
  node [
    id 91
    label "day"
  ]
  node [
    id 92
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 93
    label "przedwiecz&#243;r"
  ]
  node [
    id 94
    label "Sylwester"
  ]
  node [
    id 95
    label "po&#322;udnie"
  ]
  node [
    id 96
    label "wzej&#347;cie"
  ]
  node [
    id 97
    label "podwiecz&#243;r"
  ]
  node [
    id 98
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 99
    label "rano"
  ]
  node [
    id 100
    label "termin"
  ]
  node [
    id 101
    label "ranek"
  ]
  node [
    id 102
    label "wiecz&#243;r"
  ]
  node [
    id 103
    label "walentynki"
  ]
  node [
    id 104
    label "popo&#322;udnie"
  ]
  node [
    id 105
    label "noc"
  ]
  node [
    id 106
    label "wstanie"
  ]
  node [
    id 107
    label "niezale&#380;ny"
  ]
  node [
    id 108
    label "swobodnie"
  ]
  node [
    id 109
    label "niespieszny"
  ]
  node [
    id 110
    label "rozrzedzanie"
  ]
  node [
    id 111
    label "zwolnienie_si&#281;"
  ]
  node [
    id 112
    label "wolno"
  ]
  node [
    id 113
    label "rozrzedzenie"
  ]
  node [
    id 114
    label "lu&#378;no"
  ]
  node [
    id 115
    label "zwalnianie_si&#281;"
  ]
  node [
    id 116
    label "wolnie"
  ]
  node [
    id 117
    label "strza&#322;"
  ]
  node [
    id 118
    label "rozwodnienie"
  ]
  node [
    id 119
    label "wakowa&#263;"
  ]
  node [
    id 120
    label "rozwadnianie"
  ]
  node [
    id 121
    label "rzedni&#281;cie"
  ]
  node [
    id 122
    label "zrzedni&#281;cie"
  ]
  node [
    id 123
    label "by&#263;"
  ]
  node [
    id 124
    label "podoba&#263;_si&#281;"
  ]
  node [
    id 125
    label "wypada&#263;"
  ]
  node [
    id 126
    label "pada&#263;"
  ]
  node [
    id 127
    label "przywiera&#263;"
  ]
  node [
    id 128
    label "trafia&#263;_si&#281;"
  ]
  node [
    id 129
    label "dociera&#263;"
  ]
  node [
    id 130
    label "fall"
  ]
  node [
    id 131
    label "go"
  ]
  node [
    id 132
    label "rzuca&#263;_si&#281;"
  ]
  node [
    id 133
    label "Nowy_Rok"
  ]
  node [
    id 134
    label "miesi&#261;c"
  ]
  node [
    id 135
    label "za&#322;atwia&#263;"
  ]
  node [
    id 136
    label "usuwa&#263;"
  ]
  node [
    id 137
    label "powinno&#347;&#263;"
  ]
  node [
    id 138
    label "zadanie"
  ]
  node [
    id 139
    label "wym&#243;g"
  ]
  node [
    id 140
    label "duty"
  ]
  node [
    id 141
    label "obarczy&#263;"
  ]
  node [
    id 142
    label "zrobienie"
  ]
  node [
    id 143
    label "obietnica"
  ]
  node [
    id 144
    label "spowodowanie"
  ]
  node [
    id 145
    label "automatyczny"
  ]
  node [
    id 146
    label "poinformowanie"
  ]
  node [
    id 147
    label "zapowied&#378;"
  ]
  node [
    id 148
    label "statement"
  ]
  node [
    id 149
    label "security"
  ]
  node [
    id 150
    label "za&#347;wiadczenie"
  ]
  node [
    id 151
    label "proposition"
  ]
  node [
    id 152
    label "cz&#322;owiek_pracy"
  ]
  node [
    id 153
    label "cz&#322;owiek"
  ]
  node [
    id 154
    label "delegowa&#263;"
  ]
  node [
    id 155
    label "s&#322;u&#380;ba_cywilna"
  ]
  node [
    id 156
    label "pracu&#347;"
  ]
  node [
    id 157
    label "delegowanie"
  ]
  node [
    id 158
    label "r&#281;ka"
  ]
  node [
    id 159
    label "salariat"
  ]
  node [
    id 160
    label "poboczny"
  ]
  node [
    id 161
    label "dodatkowo"
  ]
  node [
    id 162
    label "uboczny"
  ]
  node [
    id 163
    label "pacjent"
  ]
  node [
    id 164
    label "kategoria_gramatyczna"
  ]
  node [
    id 165
    label "schorzenie"
  ]
  node [
    id 166
    label "przeznaczenie"
  ]
  node [
    id 167
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 168
    label "wydarzenie"
  ]
  node [
    id 169
    label "happening"
  ]
  node [
    id 170
    label "przyk&#322;ad"
  ]
  node [
    id 171
    label "ustawowo"
  ]
  node [
    id 172
    label "regulaminowy"
  ]
  node [
    id 173
    label "czas_wolny"
  ]
  node [
    id 174
    label "stosunek_pracy"
  ]
  node [
    id 175
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 176
    label "benedykty&#324;ski"
  ]
  node [
    id 177
    label "pracowanie"
  ]
  node [
    id 178
    label "zaw&#243;d"
  ]
  node [
    id 179
    label "kierownictwo"
  ]
  node [
    id 180
    label "zmiana"
  ]
  node [
    id 181
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 182
    label "wytw&#243;r"
  ]
  node [
    id 183
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 184
    label "tynkarski"
  ]
  node [
    id 185
    label "czynnik_produkcji"
  ]
  node [
    id 186
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 187
    label "zobowi&#261;zanie"
  ]
  node [
    id 188
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 189
    label "czynno&#347;&#263;"
  ]
  node [
    id 190
    label "tyrka"
  ]
  node [
    id 191
    label "pracowa&#263;"
  ]
  node [
    id 192
    label "siedziba"
  ]
  node [
    id 193
    label "poda&#380;_pracy"
  ]
  node [
    id 194
    label "miejsce"
  ]
  node [
    id 195
    label "zak&#322;ad"
  ]
  node [
    id 196
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 197
    label "najem"
  ]
  node [
    id 198
    label "&#346;wi&#281;to_Pracy"
  ]
  node [
    id 199
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 200
    label "Godowe_&#346;wi&#281;to"
  ]
  node [
    id 201
    label "dzie&#324;_&#347;wi&#261;teczny"
  ]
  node [
    id 202
    label "Barb&#243;rka"
  ]
  node [
    id 203
    label "ramadan"
  ]
  node [
    id 204
    label "weekend"
  ]
  node [
    id 205
    label "dzie&#324;_powszedni"
  ]
  node [
    id 206
    label "Wielka_Sobota"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 10
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 53
  ]
  edge [
    source 9
    target 54
  ]
  edge [
    source 9
    target 55
  ]
  edge [
    source 9
    target 56
  ]
  edge [
    source 9
    target 57
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 64
  ]
  edge [
    source 10
    target 65
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 10
    target 69
  ]
  edge [
    source 10
    target 70
  ]
  edge [
    source 10
    target 71
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 29
  ]
  edge [
    source 11
    target 75
  ]
  edge [
    source 11
    target 76
  ]
  edge [
    source 11
    target 77
  ]
  edge [
    source 11
    target 78
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 20
  ]
  edge [
    source 12
    target 79
  ]
  edge [
    source 12
    target 80
  ]
  edge [
    source 12
    target 81
  ]
  edge [
    source 12
    target 82
  ]
  edge [
    source 12
    target 83
  ]
  edge [
    source 12
    target 84
  ]
  edge [
    source 12
    target 85
  ]
  edge [
    source 12
    target 86
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 12
    target 89
  ]
  edge [
    source 12
    target 90
  ]
  edge [
    source 12
    target 91
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 12
    target 94
  ]
  edge [
    source 12
    target 95
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 31
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 21
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 13
    target 113
  ]
  edge [
    source 13
    target 114
  ]
  edge [
    source 13
    target 115
  ]
  edge [
    source 13
    target 116
  ]
  edge [
    source 13
    target 117
  ]
  edge [
    source 13
    target 118
  ]
  edge [
    source 13
    target 119
  ]
  edge [
    source 13
    target 120
  ]
  edge [
    source 13
    target 121
  ]
  edge [
    source 13
    target 122
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 26
  ]
  edge [
    source 14
    target 27
  ]
  edge [
    source 14
    target 123
  ]
  edge [
    source 14
    target 124
  ]
  edge [
    source 14
    target 125
  ]
  edge [
    source 14
    target 126
  ]
  edge [
    source 14
    target 127
  ]
  edge [
    source 14
    target 128
  ]
  edge [
    source 14
    target 129
  ]
  edge [
    source 14
    target 130
  ]
  edge [
    source 14
    target 131
  ]
  edge [
    source 14
    target 132
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 133
  ]
  edge [
    source 15
    target 134
  ]
  edge [
    source 15
    target 22
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 135
  ]
  edge [
    source 16
    target 136
  ]
  edge [
    source 16
    target 23
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 137
  ]
  edge [
    source 17
    target 138
  ]
  edge [
    source 17
    target 139
  ]
  edge [
    source 17
    target 140
  ]
  edge [
    source 17
    target 141
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 142
  ]
  edge [
    source 18
    target 143
  ]
  edge [
    source 18
    target 144
  ]
  edge [
    source 18
    target 145
  ]
  edge [
    source 18
    target 146
  ]
  edge [
    source 18
    target 147
  ]
  edge [
    source 18
    target 148
  ]
  edge [
    source 18
    target 149
  ]
  edge [
    source 18
    target 150
  ]
  edge [
    source 18
    target 151
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 152
  ]
  edge [
    source 19
    target 153
  ]
  edge [
    source 19
    target 154
  ]
  edge [
    source 19
    target 155
  ]
  edge [
    source 19
    target 156
  ]
  edge [
    source 19
    target 157
  ]
  edge [
    source 19
    target 158
  ]
  edge [
    source 19
    target 159
  ]
  edge [
    source 20
    target 160
  ]
  edge [
    source 20
    target 161
  ]
  edge [
    source 20
    target 162
  ]
  edge [
    source 20
    target 26
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 163
  ]
  edge [
    source 21
    target 164
  ]
  edge [
    source 21
    target 165
  ]
  edge [
    source 21
    target 166
  ]
  edge [
    source 21
    target 167
  ]
  edge [
    source 21
    target 168
  ]
  edge [
    source 21
    target 169
  ]
  edge [
    source 21
    target 170
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 171
  ]
  edge [
    source 23
    target 172
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 173
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 174
  ]
  edge [
    source 25
    target 175
  ]
  edge [
    source 25
    target 176
  ]
  edge [
    source 25
    target 177
  ]
  edge [
    source 25
    target 178
  ]
  edge [
    source 25
    target 179
  ]
  edge [
    source 25
    target 180
  ]
  edge [
    source 25
    target 181
  ]
  edge [
    source 25
    target 182
  ]
  edge [
    source 25
    target 183
  ]
  edge [
    source 25
    target 184
  ]
  edge [
    source 25
    target 185
  ]
  edge [
    source 25
    target 186
  ]
  edge [
    source 25
    target 187
  ]
  edge [
    source 25
    target 188
  ]
  edge [
    source 25
    target 189
  ]
  edge [
    source 25
    target 190
  ]
  edge [
    source 25
    target 191
  ]
  edge [
    source 25
    target 192
  ]
  edge [
    source 25
    target 193
  ]
  edge [
    source 25
    target 194
  ]
  edge [
    source 25
    target 195
  ]
  edge [
    source 25
    target 196
  ]
  edge [
    source 25
    target 197
  ]
  edge [
    source 26
    target 198
  ]
  edge [
    source 26
    target 82
  ]
  edge [
    source 26
    target 199
  ]
  edge [
    source 26
    target 133
  ]
  edge [
    source 26
    target 200
  ]
  edge [
    source 26
    target 201
  ]
  edge [
    source 26
    target 202
  ]
  edge [
    source 26
    target 203
  ]
  edge [
    source 27
    target 204
  ]
  edge [
    source 27
    target 205
  ]
  edge [
    source 27
    target 206
  ]
]
