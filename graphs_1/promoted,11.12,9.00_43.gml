graph [
  maxDegree 25
  minDegree 1
  meanDegree 2
  density 0.030303030303030304
  graphCliqueNumber 3
  node [
    id 0
    label "mama"
    origin "text"
  ]
  node [
    id 1
    label "golf"
    origin "text"
  ]
  node [
    id 2
    label "w&#322;&#261;czy&#263;"
    origin "text"
  ]
  node [
    id 3
    label "funkcja"
    origin "text"
  ]
  node [
    id 4
    label "pierwsze&#324;stwo"
    origin "text"
  ]
  node [
    id 5
    label "matczysko"
  ]
  node [
    id 6
    label "macierz"
  ]
  node [
    id 7
    label "przodkini"
  ]
  node [
    id 8
    label "Matka_Boska"
  ]
  node [
    id 9
    label "macocha"
  ]
  node [
    id 10
    label "matka_zast&#281;pcza"
  ]
  node [
    id 11
    label "stara"
  ]
  node [
    id 12
    label "rodzice"
  ]
  node [
    id 13
    label "rodzic"
  ]
  node [
    id 14
    label "samoch&#243;d_osobowy"
  ]
  node [
    id 15
    label "volkswagen"
  ]
  node [
    id 16
    label "sport"
  ]
  node [
    id 17
    label "w&#243;zek_golfowy"
  ]
  node [
    id 18
    label "r&#281;kawica_golfowa"
  ]
  node [
    id 19
    label "gra"
  ]
  node [
    id 20
    label "zatoka"
  ]
  node [
    id 21
    label "do&#322;ek"
  ]
  node [
    id 22
    label "dekolt"
  ]
  node [
    id 23
    label "ko&#322;eczek_golfowy"
  ]
  node [
    id 24
    label "sweter"
  ]
  node [
    id 25
    label "flaga_golfowa"
  ]
  node [
    id 26
    label "Golf"
  ]
  node [
    id 27
    label "bilard"
  ]
  node [
    id 28
    label "samoch&#243;d"
  ]
  node [
    id 29
    label "zacz&#261;&#263;"
  ]
  node [
    id 30
    label "nastawi&#263;"
  ]
  node [
    id 31
    label "prosecute"
  ]
  node [
    id 32
    label "odkr&#281;ci&#263;_kurek"
  ]
  node [
    id 33
    label "impersonate"
  ]
  node [
    id 34
    label "umie&#347;ci&#263;"
  ]
  node [
    id 35
    label "obejrze&#263;"
  ]
  node [
    id 36
    label "draw"
  ]
  node [
    id 37
    label "incorporate"
  ]
  node [
    id 38
    label "uruchomi&#263;"
  ]
  node [
    id 39
    label "dokoptowa&#263;"
  ]
  node [
    id 40
    label "infimum"
  ]
  node [
    id 41
    label "znaczenie"
  ]
  node [
    id 42
    label "awansowanie"
  ]
  node [
    id 43
    label "zastosowanie"
  ]
  node [
    id 44
    label "function"
  ]
  node [
    id 45
    label "funkcjonowanie"
  ]
  node [
    id 46
    label "cel"
  ]
  node [
    id 47
    label "supremum"
  ]
  node [
    id 48
    label "powierzanie"
  ]
  node [
    id 49
    label "rzut"
  ]
  node [
    id 50
    label "addytywno&#347;&#263;"
  ]
  node [
    id 51
    label "monotoniczno&#347;&#263;"
  ]
  node [
    id 52
    label "wakowa&#263;"
  ]
  node [
    id 53
    label "dziedzina"
  ]
  node [
    id 54
    label "postawi&#263;"
  ]
  node [
    id 55
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 56
    label "czyn"
  ]
  node [
    id 57
    label "przeciwdziedzina"
  ]
  node [
    id 58
    label "matematyka"
  ]
  node [
    id 59
    label "awansowa&#263;"
  ]
  node [
    id 60
    label "praca"
  ]
  node [
    id 61
    label "stawia&#263;"
  ]
  node [
    id 62
    label "jednostka"
  ]
  node [
    id 63
    label "przewaga"
  ]
  node [
    id 64
    label "laterality"
  ]
  node [
    id 65
    label "kolejno&#347;&#263;"
  ]
  node [
    id 66
    label "prym"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
]
