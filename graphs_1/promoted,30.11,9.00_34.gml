graph [
  maxDegree 49
  minDegree 1
  meanDegree 1.9649122807017543
  density 0.017388604253997825
  graphCliqueNumber 2
  node [
    id 0
    label "ostatni"
    origin "text"
  ]
  node [
    id 1
    label "program"
    origin "text"
  ]
  node [
    id 2
    label "info"
    origin "text"
  ]
  node [
    id 3
    label "wideo"
    origin "text"
  ]
  node [
    id 4
    label "pojawi&#263;"
    origin "text"
  ]
  node [
    id 5
    label "si&#281;"
    origin "text"
  ]
  node [
    id 6
    label "grafik"
    origin "text"
  ]
  node [
    id 7
    label "por&#243;wnywa&#263;"
    origin "text"
  ]
  node [
    id 8
    label "rosja"
    origin "text"
  ]
  node [
    id 9
    label "zbrodniczy"
    origin "text"
  ]
  node [
    id 10
    label "nazistowski"
    origin "text"
  ]
  node [
    id 11
    label "organizacja"
    origin "text"
  ]
  node [
    id 12
    label "cz&#322;owiek"
  ]
  node [
    id 13
    label "kolejny"
  ]
  node [
    id 14
    label "istota_&#380;ywa"
  ]
  node [
    id 15
    label "najgorszy"
  ]
  node [
    id 16
    label "aktualny"
  ]
  node [
    id 17
    label "ostatnio"
  ]
  node [
    id 18
    label "niedawno"
  ]
  node [
    id 19
    label "wi&#261;&#380;&#261;cy"
  ]
  node [
    id 20
    label "sko&#324;czony"
  ]
  node [
    id 21
    label "poprzedni"
  ]
  node [
    id 22
    label "pozosta&#322;y"
  ]
  node [
    id 23
    label "w&#261;tpliwy"
  ]
  node [
    id 24
    label "spis"
  ]
  node [
    id 25
    label "odinstalowanie"
  ]
  node [
    id 26
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 27
    label "za&#322;o&#380;enie"
  ]
  node [
    id 28
    label "podstawa"
  ]
  node [
    id 29
    label "emitowanie"
  ]
  node [
    id 30
    label "odinstalowywanie"
  ]
  node [
    id 31
    label "instrukcja"
  ]
  node [
    id 32
    label "punkt"
  ]
  node [
    id 33
    label "teleferie"
  ]
  node [
    id 34
    label "emitowa&#263;"
  ]
  node [
    id 35
    label "wytw&#243;r"
  ]
  node [
    id 36
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 37
    label "sekcja_krytyczna"
  ]
  node [
    id 38
    label "oferta"
  ]
  node [
    id 39
    label "prezentowa&#263;"
  ]
  node [
    id 40
    label "blok"
  ]
  node [
    id 41
    label "podprogram"
  ]
  node [
    id 42
    label "tryb"
  ]
  node [
    id 43
    label "dzia&#322;"
  ]
  node [
    id 44
    label "broszura"
  ]
  node [
    id 45
    label "deklaracja"
  ]
  node [
    id 46
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 47
    label "struktura_organizacyjna"
  ]
  node [
    id 48
    label "zaprezentowanie"
  ]
  node [
    id 49
    label "informatyka"
  ]
  node [
    id 50
    label "booklet"
  ]
  node [
    id 51
    label "menu"
  ]
  node [
    id 52
    label "oprogramowanie"
  ]
  node [
    id 53
    label "instalowanie"
  ]
  node [
    id 54
    label "furkacja"
  ]
  node [
    id 55
    label "odinstalowa&#263;"
  ]
  node [
    id 56
    label "instalowa&#263;"
  ]
  node [
    id 57
    label "pirat"
  ]
  node [
    id 58
    label "zainstalowanie"
  ]
  node [
    id 59
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 60
    label "ogranicznik_referencyjny"
  ]
  node [
    id 61
    label "zainstalowa&#263;"
  ]
  node [
    id 62
    label "kana&#322;"
  ]
  node [
    id 63
    label "zaprezentowa&#263;"
  ]
  node [
    id 64
    label "interfejs"
  ]
  node [
    id 65
    label "odinstalowywa&#263;"
  ]
  node [
    id 66
    label "folder"
  ]
  node [
    id 67
    label "course_of_study"
  ]
  node [
    id 68
    label "ram&#243;wka"
  ]
  node [
    id 69
    label "prezentowanie"
  ]
  node [
    id 70
    label "okno"
  ]
  node [
    id 71
    label "odtwarzacz"
  ]
  node [
    id 72
    label "g&#322;owica_elektromagnetyczna"
  ]
  node [
    id 73
    label "film"
  ]
  node [
    id 74
    label "technika"
  ]
  node [
    id 75
    label "wideokaseta"
  ]
  node [
    id 76
    label "plastyk"
  ]
  node [
    id 77
    label "informatyk"
  ]
  node [
    id 78
    label "rozk&#322;ad"
  ]
  node [
    id 79
    label "diagram"
  ]
  node [
    id 80
    label "analizowa&#263;"
  ]
  node [
    id 81
    label "szacowa&#263;"
  ]
  node [
    id 82
    label "barbarzy&#324;ski"
  ]
  node [
    id 83
    label "spaczony"
  ]
  node [
    id 84
    label "zbrodniczo"
  ]
  node [
    id 85
    label "anty&#380;ydowski"
  ]
  node [
    id 86
    label "faszystowski"
  ]
  node [
    id 87
    label "hitlerowsko"
  ]
  node [
    id 88
    label "rasistowski"
  ]
  node [
    id 89
    label "antykomunistyczny"
  ]
  node [
    id 90
    label "po_nazistowsku"
  ]
  node [
    id 91
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 92
    label "endecki"
  ]
  node [
    id 93
    label "komitet_koordynacyjny"
  ]
  node [
    id 94
    label "przybud&#243;wka"
  ]
  node [
    id 95
    label "ZOMO"
  ]
  node [
    id 96
    label "podmiot"
  ]
  node [
    id 97
    label "boj&#243;wka"
  ]
  node [
    id 98
    label "zesp&#243;&#322;"
  ]
  node [
    id 99
    label "organization"
  ]
  node [
    id 100
    label "TOPR"
  ]
  node [
    id 101
    label "jednostka_organizacyjna"
  ]
  node [
    id 102
    label "przedstawicielstwo"
  ]
  node [
    id 103
    label "Cepelia"
  ]
  node [
    id 104
    label "GOPR"
  ]
  node [
    id 105
    label "ZMP"
  ]
  node [
    id 106
    label "ZBoWiD"
  ]
  node [
    id 107
    label "struktura"
  ]
  node [
    id 108
    label "od&#322;am"
  ]
  node [
    id 109
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 110
    label "centrala"
  ]
  node [
    id 111
    label "min&#261;&#263;"
  ]
  node [
    id 112
    label "20"
  ]
  node [
    id 113
    label "TVP"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 11
    target 92
  ]
  edge [
    source 11
    target 93
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 11
    target 98
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 11
    target 100
  ]
  edge [
    source 11
    target 101
  ]
  edge [
    source 11
    target 102
  ]
  edge [
    source 11
    target 103
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 108
  ]
  edge [
    source 11
    target 109
  ]
  edge [
    source 11
    target 110
  ]
  edge [
    source 111
    target 112
  ]
]
