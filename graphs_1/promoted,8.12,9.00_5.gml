graph [
  maxDegree 12
  minDegree 1
  meanDegree 1.8888888888888888
  density 0.1111111111111111
  graphCliqueNumber 2
  node [
    id 0
    label "opis"
    origin "text"
  ]
  node [
    id 1
    label "muszy"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "exposition"
  ]
  node [
    id 4
    label "czynno&#347;&#263;"
  ]
  node [
    id 5
    label "wypowied&#378;"
  ]
  node [
    id 6
    label "obja&#347;nienie"
  ]
  node [
    id 7
    label "si&#281;ga&#263;"
  ]
  node [
    id 8
    label "trwa&#263;"
  ]
  node [
    id 9
    label "obecno&#347;&#263;"
  ]
  node [
    id 10
    label "stan"
  ]
  node [
    id 11
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 12
    label "stand"
  ]
  node [
    id 13
    label "mie&#263;_miejsce"
  ]
  node [
    id 14
    label "uczestniczy&#263;"
  ]
  node [
    id 15
    label "chodzi&#263;"
  ]
  node [
    id 16
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 17
    label "equal"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
]
