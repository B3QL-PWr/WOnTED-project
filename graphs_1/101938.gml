graph [
  maxDegree 47
  minDegree 1
  meanDegree 2.2334630350194553
  density 0.008724464980544747
  graphCliqueNumber 4
  node [
    id 0
    label "strona"
    origin "text"
  ]
  node [
    id 1
    label "otrzymywa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "przeprowadza&#263;"
    origin "text"
  ]
  node [
    id 3
    label "tryb"
    origin "text"
  ]
  node [
    id 4
    label "natychmiastowy"
    origin "text"
  ]
  node [
    id 5
    label "zgodnie"
    origin "text"
  ]
  node [
    id 6
    label "prawo"
    origin "text"
  ]
  node [
    id 7
    label "krajowy"
    origin "text"
  ]
  node [
    id 8
    label "post&#281;powanie"
    origin "text"
  ]
  node [
    id 9
    label "wyja&#347;nia&#263;"
    origin "text"
  ]
  node [
    id 10
    label "raz"
    origin "text"
  ]
  node [
    id 11
    label "potrzeba"
    origin "text"
  ]
  node [
    id 12
    label "bra&#263;"
    origin "text"
  ]
  node [
    id 13
    label "udzia&#322;"
    origin "text"
  ]
  node [
    id 14
    label "wytwarza&#263;"
    origin "text"
  ]
  node [
    id 15
    label "informowa&#263;"
    origin "text"
  ]
  node [
    id 16
    label "niezw&#322;ocznie"
    origin "text"
  ]
  node [
    id 17
    label "okoliczno&#347;&#263;"
    origin "text"
  ]
  node [
    id 18
    label "wynik"
    origin "text"
  ]
  node [
    id 19
    label "&#347;rodek"
    origin "text"
  ]
  node [
    id 20
    label "zastosowany"
    origin "text"
  ]
  node [
    id 21
    label "cela"
    origin "text"
  ]
  node [
    id 22
    label "unikn&#261;&#263;"
    origin "text"
  ]
  node [
    id 23
    label "powt&#243;rzenie"
    origin "text"
  ]
  node [
    id 24
    label "si&#281;"
    origin "text"
  ]
  node [
    id 25
    label "taki"
    origin "text"
  ]
  node [
    id 26
    label "sytuacja"
    origin "text"
  ]
  node [
    id 27
    label "skr&#281;canie"
  ]
  node [
    id 28
    label "voice"
  ]
  node [
    id 29
    label "forma"
  ]
  node [
    id 30
    label "internet"
  ]
  node [
    id 31
    label "skr&#281;ci&#263;"
  ]
  node [
    id 32
    label "kartka"
  ]
  node [
    id 33
    label "orientowa&#263;"
  ]
  node [
    id 34
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 35
    label "powierzchnia"
  ]
  node [
    id 36
    label "plik"
  ]
  node [
    id 37
    label "bok"
  ]
  node [
    id 38
    label "pagina"
  ]
  node [
    id 39
    label "orientowanie"
  ]
  node [
    id 40
    label "fragment"
  ]
  node [
    id 41
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 42
    label "s&#261;d"
  ]
  node [
    id 43
    label "skr&#281;ca&#263;"
  ]
  node [
    id 44
    label "g&#243;ra"
  ]
  node [
    id 45
    label "serwis_internetowy"
  ]
  node [
    id 46
    label "orientacja"
  ]
  node [
    id 47
    label "linia"
  ]
  node [
    id 48
    label "skr&#281;cenie"
  ]
  node [
    id 49
    label "layout"
  ]
  node [
    id 50
    label "zorientowa&#263;"
  ]
  node [
    id 51
    label "zorientowanie"
  ]
  node [
    id 52
    label "obiekt"
  ]
  node [
    id 53
    label "podmiot"
  ]
  node [
    id 54
    label "ty&#322;"
  ]
  node [
    id 55
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 56
    label "logowanie"
  ]
  node [
    id 57
    label "adres_internetowy"
  ]
  node [
    id 58
    label "uj&#281;cie"
  ]
  node [
    id 59
    label "prz&#243;d"
  ]
  node [
    id 60
    label "posta&#263;"
  ]
  node [
    id 61
    label "take"
  ]
  node [
    id 62
    label "dostawa&#263;"
  ]
  node [
    id 63
    label "return"
  ]
  node [
    id 64
    label "wykonywa&#263;"
  ]
  node [
    id 65
    label "osi&#261;ga&#263;"
  ]
  node [
    id 66
    label "robi&#263;"
  ]
  node [
    id 67
    label "pomaga&#263;"
  ]
  node [
    id 68
    label "transact"
  ]
  node [
    id 69
    label "tworzy&#263;"
  ]
  node [
    id 70
    label "powodowa&#263;"
  ]
  node [
    id 71
    label "string"
  ]
  node [
    id 72
    label "funkcjonowa&#263;"
  ]
  node [
    id 73
    label "kategoria_gramatyczna"
  ]
  node [
    id 74
    label "skala"
  ]
  node [
    id 75
    label "cecha"
  ]
  node [
    id 76
    label "ko&#322;o_z&#281;bate"
  ]
  node [
    id 77
    label "z&#261;b"
  ]
  node [
    id 78
    label "modalno&#347;&#263;"
  ]
  node [
    id 79
    label "koniugacja"
  ]
  node [
    id 80
    label "ko&#322;o"
  ]
  node [
    id 81
    label "spos&#243;b"
  ]
  node [
    id 82
    label "szybki"
  ]
  node [
    id 83
    label "natychmiast"
  ]
  node [
    id 84
    label "jednakowo"
  ]
  node [
    id 85
    label "spokojnie"
  ]
  node [
    id 86
    label "zgodny"
  ]
  node [
    id 87
    label "dobrze"
  ]
  node [
    id 88
    label "zbie&#380;nie"
  ]
  node [
    id 89
    label "obserwacja"
  ]
  node [
    id 90
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 91
    label "nauka_prawa"
  ]
  node [
    id 92
    label "dominion"
  ]
  node [
    id 93
    label "normatywizm"
  ]
  node [
    id 94
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 95
    label "qualification"
  ]
  node [
    id 96
    label "opis"
  ]
  node [
    id 97
    label "regu&#322;a_Allena"
  ]
  node [
    id 98
    label "normalizacja"
  ]
  node [
    id 99
    label "kazuistyka"
  ]
  node [
    id 100
    label "regu&#322;a_Glogera"
  ]
  node [
    id 101
    label "kultura_duchowa"
  ]
  node [
    id 102
    label "prawo_karne"
  ]
  node [
    id 103
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 104
    label "standard"
  ]
  node [
    id 105
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 106
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 107
    label "struktura"
  ]
  node [
    id 108
    label "szko&#322;a"
  ]
  node [
    id 109
    label "prawo_karne_procesowe"
  ]
  node [
    id 110
    label "prawo_Mendla"
  ]
  node [
    id 111
    label "przepis"
  ]
  node [
    id 112
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 113
    label "criterion"
  ]
  node [
    id 114
    label "kanonistyka"
  ]
  node [
    id 115
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 116
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 117
    label "wykonawczy"
  ]
  node [
    id 118
    label "twierdzenie"
  ]
  node [
    id 119
    label "judykatura"
  ]
  node [
    id 120
    label "legislacyjnie"
  ]
  node [
    id 121
    label "umocowa&#263;"
  ]
  node [
    id 122
    label "procesualistyka"
  ]
  node [
    id 123
    label "kierunek"
  ]
  node [
    id 124
    label "kryminologia"
  ]
  node [
    id 125
    label "kryminalistyka"
  ]
  node [
    id 126
    label "cywilistyka"
  ]
  node [
    id 127
    label "law"
  ]
  node [
    id 128
    label "zasada_d'Alemberta"
  ]
  node [
    id 129
    label "jurisprudence"
  ]
  node [
    id 130
    label "zasada"
  ]
  node [
    id 131
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 132
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 133
    label "rodzimy"
  ]
  node [
    id 134
    label "robienie"
  ]
  node [
    id 135
    label "czynno&#347;&#263;"
  ]
  node [
    id 136
    label "zachowanie"
  ]
  node [
    id 137
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 138
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 139
    label "kognicja"
  ]
  node [
    id 140
    label "rozprawa"
  ]
  node [
    id 141
    label "kazanie"
  ]
  node [
    id 142
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 143
    label "campaign"
  ]
  node [
    id 144
    label "fashion"
  ]
  node [
    id 145
    label "wydarzenie"
  ]
  node [
    id 146
    label "przes&#322;anka"
  ]
  node [
    id 147
    label "zmierzanie"
  ]
  node [
    id 148
    label "poja&#347;nia&#263;"
  ]
  node [
    id 149
    label "elaborate"
  ]
  node [
    id 150
    label "explain"
  ]
  node [
    id 151
    label "suplikowa&#263;"
  ]
  node [
    id 152
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 153
    label "przedstawia&#263;"
  ]
  node [
    id 154
    label "chwila"
  ]
  node [
    id 155
    label "uderzenie"
  ]
  node [
    id 156
    label "cios"
  ]
  node [
    id 157
    label "time"
  ]
  node [
    id 158
    label "need"
  ]
  node [
    id 159
    label "wym&#243;g"
  ]
  node [
    id 160
    label "necessity"
  ]
  node [
    id 161
    label "pragnienie"
  ]
  node [
    id 162
    label "get"
  ]
  node [
    id 163
    label "przewa&#380;a&#263;"
  ]
  node [
    id 164
    label "zobowi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 165
    label "poczytywa&#263;"
  ]
  node [
    id 166
    label "levy"
  ]
  node [
    id 167
    label "pokonywa&#263;"
  ]
  node [
    id 168
    label "u&#380;ywa&#263;"
  ]
  node [
    id 169
    label "rusza&#263;"
  ]
  node [
    id 170
    label "zalicza&#263;"
  ]
  node [
    id 171
    label "by&#263;"
  ]
  node [
    id 172
    label "wygrywa&#263;"
  ]
  node [
    id 173
    label "open"
  ]
  node [
    id 174
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 175
    label "branie"
  ]
  node [
    id 176
    label "korzysta&#263;"
  ]
  node [
    id 177
    label "&#263;pa&#263;"
  ]
  node [
    id 178
    label "wch&#322;ania&#263;"
  ]
  node [
    id 179
    label "interpretowa&#263;"
  ]
  node [
    id 180
    label "atakowa&#263;"
  ]
  node [
    id 181
    label "prowadzi&#263;"
  ]
  node [
    id 182
    label "rucha&#263;"
  ]
  node [
    id 183
    label "wzi&#261;&#263;"
  ]
  node [
    id 184
    label "wk&#322;ada&#263;"
  ]
  node [
    id 185
    label "chwyta&#263;"
  ]
  node [
    id 186
    label "arise"
  ]
  node [
    id 187
    label "za&#380;ywa&#263;"
  ]
  node [
    id 188
    label "uprawia&#263;_seks"
  ]
  node [
    id 189
    label "porywa&#263;"
  ]
  node [
    id 190
    label "grza&#263;"
  ]
  node [
    id 191
    label "abstract"
  ]
  node [
    id 192
    label "zaopatrywa&#263;_si&#281;"
  ]
  node [
    id 193
    label "wzmaga&#263;_si&#281;"
  ]
  node [
    id 194
    label "towarzystwo"
  ]
  node [
    id 195
    label "przyjmowa&#263;"
  ]
  node [
    id 196
    label "wchodzi&#263;"
  ]
  node [
    id 197
    label "ucieka&#263;"
  ]
  node [
    id 198
    label "odurza&#263;_si&#281;"
  ]
  node [
    id 199
    label "buzowa&#263;_si&#281;"
  ]
  node [
    id 200
    label "&#322;apa&#263;"
  ]
  node [
    id 201
    label "raise"
  ]
  node [
    id 202
    label "obecno&#347;&#263;"
  ]
  node [
    id 203
    label "kwota"
  ]
  node [
    id 204
    label "ilo&#347;&#263;"
  ]
  node [
    id 205
    label "create"
  ]
  node [
    id 206
    label "give"
  ]
  node [
    id 207
    label "komunikowa&#263;"
  ]
  node [
    id 208
    label "powiada&#263;"
  ]
  node [
    id 209
    label "inform"
  ]
  node [
    id 210
    label "niezw&#322;oczny"
  ]
  node [
    id 211
    label "sk&#322;adnik"
  ]
  node [
    id 212
    label "warunki"
  ]
  node [
    id 213
    label "typ"
  ]
  node [
    id 214
    label "dzia&#322;anie"
  ]
  node [
    id 215
    label "przyczyna"
  ]
  node [
    id 216
    label "zaokr&#261;gli&#263;"
  ]
  node [
    id 217
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 218
    label "zaokr&#261;glenie"
  ]
  node [
    id 219
    label "event"
  ]
  node [
    id 220
    label "rezultat"
  ]
  node [
    id 221
    label "miejsce"
  ]
  node [
    id 222
    label "czas"
  ]
  node [
    id 223
    label "abstrakcja"
  ]
  node [
    id 224
    label "punkt"
  ]
  node [
    id 225
    label "substancja"
  ]
  node [
    id 226
    label "chemikalia"
  ]
  node [
    id 227
    label "pomieszczenie"
  ]
  node [
    id 228
    label "klasztor"
  ]
  node [
    id 229
    label "zrobi&#263;"
  ]
  node [
    id 230
    label "fly"
  ]
  node [
    id 231
    label "umkn&#261;&#263;"
  ]
  node [
    id 232
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 233
    label "tent-fly"
  ]
  node [
    id 234
    label "zrobienie"
  ]
  node [
    id 235
    label "figura_stylistyczna"
  ]
  node [
    id 236
    label "podanie"
  ]
  node [
    id 237
    label "z&#322;o&#380;enie"
  ]
  node [
    id 238
    label "delivery"
  ]
  node [
    id 239
    label "figura_s&#322;&#243;w"
  ]
  node [
    id 240
    label "repeat"
  ]
  node [
    id 241
    label "nauczenie_si&#281;"
  ]
  node [
    id 242
    label "reduplication"
  ]
  node [
    id 243
    label "repetition"
  ]
  node [
    id 244
    label "przekopanie"
  ]
  node [
    id 245
    label "przetopienie"
  ]
  node [
    id 246
    label "okre&#347;lony"
  ]
  node [
    id 247
    label "jaki&#347;"
  ]
  node [
    id 248
    label "szczeg&#243;&#322;"
  ]
  node [
    id 249
    label "motyw"
  ]
  node [
    id 250
    label "state"
  ]
  node [
    id 251
    label "realia"
  ]
  node [
    id 252
    label "rzeczpospolita"
  ]
  node [
    id 253
    label "polski"
  ]
  node [
    id 254
    label "prezydent"
  ]
  node [
    id 255
    label "litr"
  ]
  node [
    id 256
    label "Kaczy&#324;ski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 133
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 11
  ]
  edge [
    source 8
    target 18
  ]
  edge [
    source 8
    target 134
  ]
  edge [
    source 8
    target 135
  ]
  edge [
    source 8
    target 136
  ]
  edge [
    source 8
    target 137
  ]
  edge [
    source 8
    target 138
  ]
  edge [
    source 8
    target 139
  ]
  edge [
    source 8
    target 140
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 141
  ]
  edge [
    source 8
    target 142
  ]
  edge [
    source 8
    target 143
  ]
  edge [
    source 8
    target 144
  ]
  edge [
    source 8
    target 145
  ]
  edge [
    source 8
    target 146
  ]
  edge [
    source 8
    target 147
  ]
  edge [
    source 8
    target 15
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 12
  ]
  edge [
    source 9
    target 19
  ]
  edge [
    source 9
    target 148
  ]
  edge [
    source 9
    target 149
  ]
  edge [
    source 9
    target 150
  ]
  edge [
    source 9
    target 151
  ]
  edge [
    source 9
    target 152
  ]
  edge [
    source 9
    target 153
  ]
  edge [
    source 9
    target 13
  ]
  edge [
    source 9
    target 20
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 154
  ]
  edge [
    source 10
    target 155
  ]
  edge [
    source 10
    target 156
  ]
  edge [
    source 10
    target 157
  ]
  edge [
    source 11
    target 158
  ]
  edge [
    source 11
    target 159
  ]
  edge [
    source 11
    target 160
  ]
  edge [
    source 11
    target 161
  ]
  edge [
    source 11
    target 26
  ]
  edge [
    source 11
    target 22
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 162
  ]
  edge [
    source 12
    target 163
  ]
  edge [
    source 12
    target 164
  ]
  edge [
    source 12
    target 165
  ]
  edge [
    source 12
    target 166
  ]
  edge [
    source 12
    target 167
  ]
  edge [
    source 12
    target 168
  ]
  edge [
    source 12
    target 169
  ]
  edge [
    source 12
    target 170
  ]
  edge [
    source 12
    target 171
  ]
  edge [
    source 12
    target 172
  ]
  edge [
    source 12
    target 173
  ]
  edge [
    source 12
    target 174
  ]
  edge [
    source 12
    target 175
  ]
  edge [
    source 12
    target 176
  ]
  edge [
    source 12
    target 177
  ]
  edge [
    source 12
    target 178
  ]
  edge [
    source 12
    target 179
  ]
  edge [
    source 12
    target 180
  ]
  edge [
    source 12
    target 181
  ]
  edge [
    source 12
    target 182
  ]
  edge [
    source 12
    target 61
  ]
  edge [
    source 12
    target 62
  ]
  edge [
    source 12
    target 183
  ]
  edge [
    source 12
    target 184
  ]
  edge [
    source 12
    target 185
  ]
  edge [
    source 12
    target 186
  ]
  edge [
    source 12
    target 187
  ]
  edge [
    source 12
    target 188
  ]
  edge [
    source 12
    target 189
  ]
  edge [
    source 12
    target 66
  ]
  edge [
    source 12
    target 190
  ]
  edge [
    source 12
    target 191
  ]
  edge [
    source 12
    target 192
  ]
  edge [
    source 12
    target 193
  ]
  edge [
    source 12
    target 194
  ]
  edge [
    source 12
    target 195
  ]
  edge [
    source 12
    target 196
  ]
  edge [
    source 12
    target 197
  ]
  edge [
    source 12
    target 198
  ]
  edge [
    source 12
    target 199
  ]
  edge [
    source 12
    target 200
  ]
  edge [
    source 12
    target 201
  ]
  edge [
    source 13
    target 202
  ]
  edge [
    source 13
    target 137
  ]
  edge [
    source 13
    target 203
  ]
  edge [
    source 13
    target 204
  ]
  edge [
    source 13
    target 20
  ]
  edge [
    source 14
    target 17
  ]
  edge [
    source 14
    target 66
  ]
  edge [
    source 14
    target 205
  ]
  edge [
    source 14
    target 206
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 207
  ]
  edge [
    source 15
    target 208
  ]
  edge [
    source 15
    target 209
  ]
  edge [
    source 16
    target 83
  ]
  edge [
    source 16
    target 210
  ]
  edge [
    source 16
    target 18
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 145
  ]
  edge [
    source 17
    target 211
  ]
  edge [
    source 17
    target 212
  ]
  edge [
    source 17
    target 26
  ]
  edge [
    source 18
    target 213
  ]
  edge [
    source 18
    target 214
  ]
  edge [
    source 18
    target 215
  ]
  edge [
    source 18
    target 216
  ]
  edge [
    source 18
    target 217
  ]
  edge [
    source 18
    target 218
  ]
  edge [
    source 18
    target 219
  ]
  edge [
    source 18
    target 220
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 221
  ]
  edge [
    source 19
    target 222
  ]
  edge [
    source 19
    target 223
  ]
  edge [
    source 19
    target 224
  ]
  edge [
    source 19
    target 225
  ]
  edge [
    source 19
    target 81
  ]
  edge [
    source 19
    target 226
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 227
  ]
  edge [
    source 21
    target 228
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 229
  ]
  edge [
    source 22
    target 230
  ]
  edge [
    source 22
    target 231
  ]
  edge [
    source 22
    target 232
  ]
  edge [
    source 22
    target 233
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 234
  ]
  edge [
    source 23
    target 135
  ]
  edge [
    source 23
    target 235
  ]
  edge [
    source 23
    target 236
  ]
  edge [
    source 23
    target 237
  ]
  edge [
    source 23
    target 238
  ]
  edge [
    source 23
    target 239
  ]
  edge [
    source 23
    target 240
  ]
  edge [
    source 23
    target 241
  ]
  edge [
    source 23
    target 242
  ]
  edge [
    source 23
    target 243
  ]
  edge [
    source 23
    target 244
  ]
  edge [
    source 23
    target 245
  ]
  edge [
    source 23
    target 40
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 246
  ]
  edge [
    source 25
    target 247
  ]
  edge [
    source 26
    target 248
  ]
  edge [
    source 26
    target 249
  ]
  edge [
    source 26
    target 250
  ]
  edge [
    source 26
    target 251
  ]
  edge [
    source 26
    target 212
  ]
  edge [
    source 252
    target 253
  ]
  edge [
    source 252
    target 254
  ]
  edge [
    source 253
    target 254
  ]
  edge [
    source 255
    target 256
  ]
]
