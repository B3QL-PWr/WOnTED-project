graph [
  maxDegree 71
  minDegree 1
  meanDegree 2.0817843866171004
  density 0.007767852188869778
  graphCliqueNumber 2
  node [
    id 0
    label "zastanawia&#263;"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 3
    label "p&#322;atny"
    origin "text"
  ]
  node [
    id 4
    label "news"
    origin "text"
  ]
  node [
    id 5
    label "by&#263;"
    origin "text"
  ]
  node [
    id 6
    label "natychmiast"
    origin "text"
  ]
  node [
    id 7
    label "piracone"
    origin "text"
  ]
  node [
    id 8
    label "dla"
    origin "text"
  ]
  node [
    id 9
    label "profesjonalny"
    origin "text"
  ]
  node [
    id 10
    label "pirat"
    origin "text"
  ]
  node [
    id 11
    label "dystrybuowa&#263;"
    origin "text"
  ]
  node [
    id 12
    label "sie&#263;"
    origin "text"
  ]
  node [
    id 13
    label "film"
    origin "text"
  ]
  node [
    id 14
    label "piosenka"
    origin "text"
  ]
  node [
    id 15
    label "pow&#243;d"
    origin "text"
  ]
  node [
    id 16
    label "duma"
    origin "text"
  ]
  node [
    id 17
    label "pr&#281;dko&#347;&#263;"
    origin "text"
  ]
  node [
    id 18
    label "wrzuci&#263;"
    origin "text"
  ]
  node [
    id 19
    label "tre&#347;&#263;"
    origin "text"
  ]
  node [
    id 20
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 21
    label "prasowy"
    origin "text"
  ]
  node [
    id 22
    label "potrafi&#263;by"
    origin "text"
  ]
  node [
    id 23
    label "udost&#281;pnia&#263;"
    origin "text"
  ]
  node [
    id 24
    label "bezp&#322;atny"
    origin "text"
  ]
  node [
    id 25
    label "wersja"
    origin "text"
  ]
  node [
    id 26
    label "sekunda"
    origin "text"
  ]
  node [
    id 27
    label "publikacja"
    origin "text"
  ]
  node [
    id 28
    label "strike"
  ]
  node [
    id 29
    label "interesowa&#263;"
  ]
  node [
    id 30
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 31
    label "obszar"
  ]
  node [
    id 32
    label "obiekt_naturalny"
  ]
  node [
    id 33
    label "przedmiot"
  ]
  node [
    id 34
    label "Stary_&#346;wiat"
  ]
  node [
    id 35
    label "grupa"
  ]
  node [
    id 36
    label "stw&#243;r"
  ]
  node [
    id 37
    label "biosfera"
  ]
  node [
    id 38
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 39
    label "rzecz"
  ]
  node [
    id 40
    label "magnetosfera"
  ]
  node [
    id 41
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 42
    label "environment"
  ]
  node [
    id 43
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 44
    label "geosfera"
  ]
  node [
    id 45
    label "Nowy_&#346;wiat"
  ]
  node [
    id 46
    label "planeta"
  ]
  node [
    id 47
    label "przejmowa&#263;"
  ]
  node [
    id 48
    label "litosfera"
  ]
  node [
    id 49
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 50
    label "makrokosmos"
  ]
  node [
    id 51
    label "barysfera"
  ]
  node [
    id 52
    label "biota"
  ]
  node [
    id 53
    label "p&#243;&#322;noc"
  ]
  node [
    id 54
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 55
    label "fauna"
  ]
  node [
    id 56
    label "wszechstworzenie"
  ]
  node [
    id 57
    label "geotermia"
  ]
  node [
    id 58
    label "biegun"
  ]
  node [
    id 59
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 60
    label "ekosystem"
  ]
  node [
    id 61
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 62
    label "teren"
  ]
  node [
    id 63
    label "zjawisko"
  ]
  node [
    id 64
    label "p&#243;&#322;kula"
  ]
  node [
    id 65
    label "atmosfera"
  ]
  node [
    id 66
    label "mikrokosmos"
  ]
  node [
    id 67
    label "class"
  ]
  node [
    id 68
    label "po&#322;udnie"
  ]
  node [
    id 69
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 70
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 71
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 72
    label "przejmowanie"
  ]
  node [
    id 73
    label "przestrze&#324;"
  ]
  node [
    id 74
    label "asymilowanie_si&#281;"
  ]
  node [
    id 75
    label "przej&#261;&#263;"
  ]
  node [
    id 76
    label "ekosfera"
  ]
  node [
    id 77
    label "przyroda"
  ]
  node [
    id 78
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 79
    label "ciemna_materia"
  ]
  node [
    id 80
    label "geoida"
  ]
  node [
    id 81
    label "Wsch&#243;d"
  ]
  node [
    id 82
    label "populace"
  ]
  node [
    id 83
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 84
    label "huczek"
  ]
  node [
    id 85
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 86
    label "Ziemia"
  ]
  node [
    id 87
    label "universe"
  ]
  node [
    id 88
    label "ozonosfera"
  ]
  node [
    id 89
    label "rze&#378;ba"
  ]
  node [
    id 90
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 91
    label "zagranica"
  ]
  node [
    id 92
    label "hydrosfera"
  ]
  node [
    id 93
    label "woda"
  ]
  node [
    id 94
    label "kuchnia"
  ]
  node [
    id 95
    label "przej&#281;cie"
  ]
  node [
    id 96
    label "czarna_dziura"
  ]
  node [
    id 97
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 98
    label "morze"
  ]
  node [
    id 99
    label "najemny"
  ]
  node [
    id 100
    label "p&#322;atnie"
  ]
  node [
    id 101
    label "system"
  ]
  node [
    id 102
    label "nowostka"
  ]
  node [
    id 103
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 104
    label "doniesienie"
  ]
  node [
    id 105
    label "informacja"
  ]
  node [
    id 106
    label "message"
  ]
  node [
    id 107
    label "nius"
  ]
  node [
    id 108
    label "si&#281;ga&#263;"
  ]
  node [
    id 109
    label "trwa&#263;"
  ]
  node [
    id 110
    label "obecno&#347;&#263;"
  ]
  node [
    id 111
    label "stan"
  ]
  node [
    id 112
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 113
    label "stand"
  ]
  node [
    id 114
    label "mie&#263;_miejsce"
  ]
  node [
    id 115
    label "uczestniczy&#263;"
  ]
  node [
    id 116
    label "chodzi&#263;"
  ]
  node [
    id 117
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 118
    label "equal"
  ]
  node [
    id 119
    label "szybko"
  ]
  node [
    id 120
    label "natychmiastowy"
  ]
  node [
    id 121
    label "specjalny"
  ]
  node [
    id 122
    label "kompetentny"
  ]
  node [
    id 123
    label "rzetelny"
  ]
  node [
    id 124
    label "trained"
  ]
  node [
    id 125
    label "porz&#261;dny"
  ]
  node [
    id 126
    label "fachowy"
  ]
  node [
    id 127
    label "ch&#322;odny"
  ]
  node [
    id 128
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 129
    label "profesjonalnie"
  ]
  node [
    id 130
    label "zawodowo"
  ]
  node [
    id 131
    label "&#380;agl&#243;wka"
  ]
  node [
    id 132
    label "rozb&#243;jnik"
  ]
  node [
    id 133
    label "kieruj&#261;cy"
  ]
  node [
    id 134
    label "podr&#243;bka"
  ]
  node [
    id 135
    label "rum"
  ]
  node [
    id 136
    label "program"
  ]
  node [
    id 137
    label "kopiowa&#263;"
  ]
  node [
    id 138
    label "przest&#281;pca"
  ]
  node [
    id 139
    label "postrzeleniec"
  ]
  node [
    id 140
    label "circulate"
  ]
  node [
    id 141
    label "rozprowadza&#263;"
  ]
  node [
    id 142
    label "hipertekst"
  ]
  node [
    id 143
    label "gauze"
  ]
  node [
    id 144
    label "nitka"
  ]
  node [
    id 145
    label "mesh"
  ]
  node [
    id 146
    label "e-hazard"
  ]
  node [
    id 147
    label "netbook"
  ]
  node [
    id 148
    label "cyberprzestrze&#324;"
  ]
  node [
    id 149
    label "biznes_elektroniczny"
  ]
  node [
    id 150
    label "snu&#263;"
  ]
  node [
    id 151
    label "organization"
  ]
  node [
    id 152
    label "zasadzka"
  ]
  node [
    id 153
    label "web"
  ]
  node [
    id 154
    label "provider"
  ]
  node [
    id 155
    label "struktura"
  ]
  node [
    id 156
    label "us&#322;uga_internetowa"
  ]
  node [
    id 157
    label "punkt_dost&#281;pu"
  ]
  node [
    id 158
    label "organizacja"
  ]
  node [
    id 159
    label "mem"
  ]
  node [
    id 160
    label "vane"
  ]
  node [
    id 161
    label "podcast"
  ]
  node [
    id 162
    label "grooming"
  ]
  node [
    id 163
    label "kszta&#322;t"
  ]
  node [
    id 164
    label "strona"
  ]
  node [
    id 165
    label "obiekt"
  ]
  node [
    id 166
    label "wysnu&#263;"
  ]
  node [
    id 167
    label "gra_sieciowa"
  ]
  node [
    id 168
    label "instalacja"
  ]
  node [
    id 169
    label "sie&#263;_komputerowa"
  ]
  node [
    id 170
    label "net"
  ]
  node [
    id 171
    label "plecionka"
  ]
  node [
    id 172
    label "media"
  ]
  node [
    id 173
    label "rozmieszczenie"
  ]
  node [
    id 174
    label "rozbieg&#243;wka"
  ]
  node [
    id 175
    label "block"
  ]
  node [
    id 176
    label "blik"
  ]
  node [
    id 177
    label "odczula&#263;"
  ]
  node [
    id 178
    label "rola"
  ]
  node [
    id 179
    label "trawiarnia"
  ]
  node [
    id 180
    label "b&#322;ona"
  ]
  node [
    id 181
    label "filmoteka"
  ]
  node [
    id 182
    label "sztuka"
  ]
  node [
    id 183
    label "muza"
  ]
  node [
    id 184
    label "odczuli&#263;"
  ]
  node [
    id 185
    label "klatka"
  ]
  node [
    id 186
    label "odczulenie"
  ]
  node [
    id 187
    label "emulsja_fotograficzna"
  ]
  node [
    id 188
    label "animatronika"
  ]
  node [
    id 189
    label "dorobek"
  ]
  node [
    id 190
    label "odczulanie"
  ]
  node [
    id 191
    label "scena"
  ]
  node [
    id 192
    label "czo&#322;&#243;wka"
  ]
  node [
    id 193
    label "ty&#322;&#243;wka"
  ]
  node [
    id 194
    label "napisy"
  ]
  node [
    id 195
    label "photograph"
  ]
  node [
    id 196
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 197
    label "postprodukcja"
  ]
  node [
    id 198
    label "sklejarka"
  ]
  node [
    id 199
    label "anamorfoza"
  ]
  node [
    id 200
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 201
    label "ta&#347;ma"
  ]
  node [
    id 202
    label "b&#322;ona_&#347;wiat&#322;oczu&#322;a"
  ]
  node [
    id 203
    label "uj&#281;cie"
  ]
  node [
    id 204
    label "zwrotka"
  ]
  node [
    id 205
    label "nuci&#263;"
  ]
  node [
    id 206
    label "zanuci&#263;"
  ]
  node [
    id 207
    label "utw&#243;r"
  ]
  node [
    id 208
    label "zanucenie"
  ]
  node [
    id 209
    label "nucenie"
  ]
  node [
    id 210
    label "tekst"
  ]
  node [
    id 211
    label "piosnka"
  ]
  node [
    id 212
    label "przyczyna"
  ]
  node [
    id 213
    label "matuszka"
  ]
  node [
    id 214
    label "geneza"
  ]
  node [
    id 215
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 216
    label "czynnik"
  ]
  node [
    id 217
    label "poci&#261;ganie"
  ]
  node [
    id 218
    label "rezultat"
  ]
  node [
    id 219
    label "uprz&#261;&#380;"
  ]
  node [
    id 220
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 221
    label "subject"
  ]
  node [
    id 222
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 223
    label "realizowa&#263;_si&#281;"
  ]
  node [
    id 224
    label "emocja"
  ]
  node [
    id 225
    label "pride"
  ]
  node [
    id 226
    label "gratyfikacja"
  ]
  node [
    id 227
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 228
    label "elegia"
  ]
  node [
    id 229
    label "wiersz"
  ]
  node [
    id 230
    label "dobro"
  ]
  node [
    id 231
    label "enjoyment"
  ]
  node [
    id 232
    label "pie&#347;&#324;"
  ]
  node [
    id 233
    label "realizowanie_si&#281;"
  ]
  node [
    id 234
    label "pe&#322;ny"
  ]
  node [
    id 235
    label "utw&#243;r_epicki"
  ]
  node [
    id 236
    label "postawa"
  ]
  node [
    id 237
    label "conceit"
  ]
  node [
    id 238
    label "energia"
  ]
  node [
    id 239
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 240
    label "celerity"
  ]
  node [
    id 241
    label "cecha"
  ]
  node [
    id 242
    label "tempo"
  ]
  node [
    id 243
    label "wektorowa_wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 244
    label "insert"
  ]
  node [
    id 245
    label "umie&#347;ci&#263;"
  ]
  node [
    id 246
    label "zawarto&#347;&#263;"
  ]
  node [
    id 247
    label "temat"
  ]
  node [
    id 248
    label "istota"
  ]
  node [
    id 249
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 250
    label "medialny"
  ]
  node [
    id 251
    label "medialnie"
  ]
  node [
    id 252
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 253
    label "bezp&#322;atnie"
  ]
  node [
    id 254
    label "darmowo"
  ]
  node [
    id 255
    label "typ"
  ]
  node [
    id 256
    label "posta&#263;"
  ]
  node [
    id 257
    label "minuta"
  ]
  node [
    id 258
    label "tercja"
  ]
  node [
    id 259
    label "milisekunda"
  ]
  node [
    id 260
    label "nanosekunda"
  ]
  node [
    id 261
    label "uk&#322;ad_SI"
  ]
  node [
    id 262
    label "mikrosekunda"
  ]
  node [
    id 263
    label "time"
  ]
  node [
    id 264
    label "jednostka_czasu"
  ]
  node [
    id 265
    label "jednostka"
  ]
  node [
    id 266
    label "produkcja"
  ]
  node [
    id 267
    label "druk"
  ]
  node [
    id 268
    label "notification"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 16
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 5
    target 12
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 129
  ]
  edge [
    source 9
    target 130
  ]
  edge [
    source 9
    target 23
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 20
  ]
  edge [
    source 10
    target 19
  ]
  edge [
    source 10
    target 131
  ]
  edge [
    source 10
    target 132
  ]
  edge [
    source 10
    target 133
  ]
  edge [
    source 10
    target 134
  ]
  edge [
    source 10
    target 135
  ]
  edge [
    source 10
    target 136
  ]
  edge [
    source 10
    target 137
  ]
  edge [
    source 10
    target 138
  ]
  edge [
    source 10
    target 139
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 140
  ]
  edge [
    source 11
    target 141
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 19
  ]
  edge [
    source 12
    target 142
  ]
  edge [
    source 12
    target 143
  ]
  edge [
    source 12
    target 144
  ]
  edge [
    source 12
    target 145
  ]
  edge [
    source 12
    target 146
  ]
  edge [
    source 12
    target 147
  ]
  edge [
    source 12
    target 148
  ]
  edge [
    source 12
    target 149
  ]
  edge [
    source 12
    target 150
  ]
  edge [
    source 12
    target 151
  ]
  edge [
    source 12
    target 152
  ]
  edge [
    source 12
    target 59
  ]
  edge [
    source 12
    target 153
  ]
  edge [
    source 12
    target 154
  ]
  edge [
    source 12
    target 155
  ]
  edge [
    source 12
    target 156
  ]
  edge [
    source 12
    target 157
  ]
  edge [
    source 12
    target 158
  ]
  edge [
    source 12
    target 159
  ]
  edge [
    source 12
    target 160
  ]
  edge [
    source 12
    target 161
  ]
  edge [
    source 12
    target 162
  ]
  edge [
    source 12
    target 163
  ]
  edge [
    source 12
    target 164
  ]
  edge [
    source 12
    target 165
  ]
  edge [
    source 12
    target 166
  ]
  edge [
    source 12
    target 167
  ]
  edge [
    source 12
    target 168
  ]
  edge [
    source 12
    target 169
  ]
  edge [
    source 12
    target 170
  ]
  edge [
    source 12
    target 171
  ]
  edge [
    source 12
    target 172
  ]
  edge [
    source 12
    target 173
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 174
  ]
  edge [
    source 13
    target 175
  ]
  edge [
    source 13
    target 176
  ]
  edge [
    source 13
    target 177
  ]
  edge [
    source 13
    target 178
  ]
  edge [
    source 13
    target 179
  ]
  edge [
    source 13
    target 180
  ]
  edge [
    source 13
    target 181
  ]
  edge [
    source 13
    target 182
  ]
  edge [
    source 13
    target 183
  ]
  edge [
    source 13
    target 184
  ]
  edge [
    source 13
    target 185
  ]
  edge [
    source 13
    target 186
  ]
  edge [
    source 13
    target 187
  ]
  edge [
    source 13
    target 188
  ]
  edge [
    source 13
    target 189
  ]
  edge [
    source 13
    target 190
  ]
  edge [
    source 13
    target 191
  ]
  edge [
    source 13
    target 192
  ]
  edge [
    source 13
    target 193
  ]
  edge [
    source 13
    target 194
  ]
  edge [
    source 13
    target 195
  ]
  edge [
    source 13
    target 196
  ]
  edge [
    source 13
    target 197
  ]
  edge [
    source 13
    target 198
  ]
  edge [
    source 13
    target 199
  ]
  edge [
    source 13
    target 200
  ]
  edge [
    source 13
    target 201
  ]
  edge [
    source 13
    target 202
  ]
  edge [
    source 13
    target 203
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 204
  ]
  edge [
    source 14
    target 205
  ]
  edge [
    source 14
    target 206
  ]
  edge [
    source 14
    target 207
  ]
  edge [
    source 14
    target 208
  ]
  edge [
    source 14
    target 209
  ]
  edge [
    source 14
    target 210
  ]
  edge [
    source 14
    target 211
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 164
  ]
  edge [
    source 15
    target 212
  ]
  edge [
    source 15
    target 213
  ]
  edge [
    source 15
    target 214
  ]
  edge [
    source 15
    target 215
  ]
  edge [
    source 15
    target 216
  ]
  edge [
    source 15
    target 217
  ]
  edge [
    source 15
    target 218
  ]
  edge [
    source 15
    target 219
  ]
  edge [
    source 15
    target 220
  ]
  edge [
    source 15
    target 221
  ]
  edge [
    source 16
    target 222
  ]
  edge [
    source 16
    target 223
  ]
  edge [
    source 16
    target 224
  ]
  edge [
    source 16
    target 225
  ]
  edge [
    source 16
    target 226
  ]
  edge [
    source 16
    target 227
  ]
  edge [
    source 16
    target 228
  ]
  edge [
    source 16
    target 39
  ]
  edge [
    source 16
    target 229
  ]
  edge [
    source 16
    target 230
  ]
  edge [
    source 16
    target 231
  ]
  edge [
    source 16
    target 232
  ]
  edge [
    source 16
    target 233
  ]
  edge [
    source 16
    target 234
  ]
  edge [
    source 16
    target 235
  ]
  edge [
    source 16
    target 236
  ]
  edge [
    source 16
    target 237
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 238
  ]
  edge [
    source 17
    target 239
  ]
  edge [
    source 17
    target 240
  ]
  edge [
    source 17
    target 241
  ]
  edge [
    source 17
    target 242
  ]
  edge [
    source 17
    target 243
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 244
  ]
  edge [
    source 18
    target 245
  ]
  edge [
    source 19
    target 21
  ]
  edge [
    source 19
    target 246
  ]
  edge [
    source 19
    target 247
  ]
  edge [
    source 19
    target 248
  ]
  edge [
    source 19
    target 249
  ]
  edge [
    source 19
    target 105
  ]
  edge [
    source 19
    target 27
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 250
  ]
  edge [
    source 21
    target 251
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 252
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 253
  ]
  edge [
    source 24
    target 254
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 255
  ]
  edge [
    source 25
    target 256
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 257
  ]
  edge [
    source 26
    target 258
  ]
  edge [
    source 26
    target 259
  ]
  edge [
    source 26
    target 260
  ]
  edge [
    source 26
    target 261
  ]
  edge [
    source 26
    target 262
  ]
  edge [
    source 26
    target 263
  ]
  edge [
    source 26
    target 264
  ]
  edge [
    source 26
    target 265
  ]
  edge [
    source 27
    target 266
  ]
  edge [
    source 27
    target 267
  ]
  edge [
    source 27
    target 268
  ]
  edge [
    source 27
    target 210
  ]
]
