graph [
  maxDegree 46
  minDegree 1
  meanDegree 2.0374531835205993
  density 0.007659598434287967
  graphCliqueNumber 2
  node [
    id 0
    label "film"
    origin "text"
  ]
  node [
    id 1
    label "dokumentalny"
    origin "text"
  ]
  node [
    id 2
    label "traktowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "historia"
    origin "text"
  ]
  node [
    id 4
    label "gie&#322;da"
    origin "text"
  ]
  node [
    id 5
    label "komputerowy"
    origin "text"
  ]
  node [
    id 6
    label "polska"
    origin "text"
  ]
  node [
    id 7
    label "moment"
    origin "text"
  ]
  node [
    id 8
    label "powstanie"
    origin "text"
  ]
  node [
    id 9
    label "po&#322;owa"
    origin "text"
  ]
  node [
    id 10
    label "lato"
    origin "text"
  ]
  node [
    id 11
    label "ten"
    origin "text"
  ]
  node [
    id 12
    label "przez"
    origin "text"
  ]
  node [
    id 13
    label "z&#322;oty"
    origin "text"
  ]
  node [
    id 14
    label "istnienie"
    origin "text"
  ]
  node [
    id 15
    label "si&#281;"
    origin "text"
  ]
  node [
    id 16
    label "sta&#263;"
    origin "text"
  ]
  node [
    id 17
    label "wej&#347;cie"
    origin "text"
  ]
  node [
    id 18
    label "ustawa"
    origin "text"
  ]
  node [
    id 19
    label "prawie"
    origin "text"
  ]
  node [
    id 20
    label "autorski"
    origin "text"
  ]
  node [
    id 21
    label "rok"
    origin "text"
  ]
  node [
    id 22
    label "wreszcie"
    origin "text"
  ]
  node [
    id 23
    label "upadek"
    origin "text"
  ]
  node [
    id 24
    label "rozbieg&#243;wka"
  ]
  node [
    id 25
    label "block"
  ]
  node [
    id 26
    label "odczula&#263;"
  ]
  node [
    id 27
    label "blik"
  ]
  node [
    id 28
    label "rola"
  ]
  node [
    id 29
    label "trawiarnia"
  ]
  node [
    id 30
    label "b&#322;ona"
  ]
  node [
    id 31
    label "filmoteka"
  ]
  node [
    id 32
    label "sztuka"
  ]
  node [
    id 33
    label "muza"
  ]
  node [
    id 34
    label "odczuli&#263;"
  ]
  node [
    id 35
    label "klatka"
  ]
  node [
    id 36
    label "odczulenie"
  ]
  node [
    id 37
    label "emulsja_fotograficzna"
  ]
  node [
    id 38
    label "animatronika"
  ]
  node [
    id 39
    label "dorobek"
  ]
  node [
    id 40
    label "odczulanie"
  ]
  node [
    id 41
    label "scena"
  ]
  node [
    id 42
    label "czo&#322;&#243;wka"
  ]
  node [
    id 43
    label "ty&#322;&#243;wka"
  ]
  node [
    id 44
    label "napisy"
  ]
  node [
    id 45
    label "photograph"
  ]
  node [
    id 46
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 47
    label "postprodukcja"
  ]
  node [
    id 48
    label "sklejarka"
  ]
  node [
    id 49
    label "anamorfoza"
  ]
  node [
    id 50
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 51
    label "ta&#347;ma"
  ]
  node [
    id 52
    label "b&#322;ona_&#347;wiat&#322;oczu&#322;a"
  ]
  node [
    id 53
    label "uj&#281;cie"
  ]
  node [
    id 54
    label "dokumentalnie"
  ]
  node [
    id 55
    label "use"
  ]
  node [
    id 56
    label "cz&#281;stowa&#263;"
  ]
  node [
    id 57
    label "robi&#263;"
  ]
  node [
    id 58
    label "dotyczy&#263;"
  ]
  node [
    id 59
    label "poddawa&#263;"
  ]
  node [
    id 60
    label "report"
  ]
  node [
    id 61
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 62
    label "wypowied&#378;"
  ]
  node [
    id 63
    label "neografia"
  ]
  node [
    id 64
    label "przedmiot"
  ]
  node [
    id 65
    label "papirologia"
  ]
  node [
    id 66
    label "historia_gospodarcza"
  ]
  node [
    id 67
    label "przebiec"
  ]
  node [
    id 68
    label "hista"
  ]
  node [
    id 69
    label "nauka_humanistyczna"
  ]
  node [
    id 70
    label "filigranistyka"
  ]
  node [
    id 71
    label "dyplomatyka"
  ]
  node [
    id 72
    label "annalistyka"
  ]
  node [
    id 73
    label "historyka"
  ]
  node [
    id 74
    label "heraldyka"
  ]
  node [
    id 75
    label "fabu&#322;a"
  ]
  node [
    id 76
    label "muzealnictwo"
  ]
  node [
    id 77
    label "varsavianistyka"
  ]
  node [
    id 78
    label "mediewistyka"
  ]
  node [
    id 79
    label "prezentyzm"
  ]
  node [
    id 80
    label "przebiegni&#281;cie"
  ]
  node [
    id 81
    label "charakter"
  ]
  node [
    id 82
    label "paleografia"
  ]
  node [
    id 83
    label "genealogia"
  ]
  node [
    id 84
    label "czynno&#347;&#263;"
  ]
  node [
    id 85
    label "prozopografia"
  ]
  node [
    id 86
    label "motyw"
  ]
  node [
    id 87
    label "nautologia"
  ]
  node [
    id 88
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 89
    label "epoka"
  ]
  node [
    id 90
    label "numizmatyka"
  ]
  node [
    id 91
    label "ruralistyka"
  ]
  node [
    id 92
    label "&#378;r&#243;d&#322;oznawstwo"
  ]
  node [
    id 93
    label "epigrafika"
  ]
  node [
    id 94
    label "historiografia"
  ]
  node [
    id 95
    label "bizantynistyka"
  ]
  node [
    id 96
    label "weksylologia"
  ]
  node [
    id 97
    label "kierunek"
  ]
  node [
    id 98
    label "ikonografia"
  ]
  node [
    id 99
    label "chronologia"
  ]
  node [
    id 100
    label "archiwistyka"
  ]
  node [
    id 101
    label "sfragistyka"
  ]
  node [
    id 102
    label "zabytkoznawstwo"
  ]
  node [
    id 103
    label "historia_sztuki"
  ]
  node [
    id 104
    label "odreagowywa&#263;"
  ]
  node [
    id 105
    label "dzie&#324;_trzech_wied&#378;m"
  ]
  node [
    id 106
    label "nied&#378;wied&#378;"
  ]
  node [
    id 107
    label "odreagowywanie"
  ]
  node [
    id 108
    label "odreagowanie"
  ]
  node [
    id 109
    label "odreagowa&#263;"
  ]
  node [
    id 110
    label "sesja"
  ]
  node [
    id 111
    label "byk"
  ]
  node [
    id 112
    label "instytucja"
  ]
  node [
    id 113
    label "targ"
  ]
  node [
    id 114
    label "arbitra&#380;"
  ]
  node [
    id 115
    label "komputerowo"
  ]
  node [
    id 116
    label "okres_czasu"
  ]
  node [
    id 117
    label "minute"
  ]
  node [
    id 118
    label "jednostka_geologiczna"
  ]
  node [
    id 119
    label "warto&#347;&#263;_oczekiwana"
  ]
  node [
    id 120
    label "time"
  ]
  node [
    id 121
    label "chron"
  ]
  node [
    id 122
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 123
    label "fragment"
  ]
  node [
    id 124
    label "uniesienie_si&#281;"
  ]
  node [
    id 125
    label "geneza"
  ]
  node [
    id 126
    label "chmielnicczyzna"
  ]
  node [
    id 127
    label "beginning"
  ]
  node [
    id 128
    label "orgy"
  ]
  node [
    id 129
    label "Ko&#347;ciuszko"
  ]
  node [
    id 130
    label "sprzeciwienie_si&#281;"
  ]
  node [
    id 131
    label "utworzenie"
  ]
  node [
    id 132
    label "powstanie_listopadowe"
  ]
  node [
    id 133
    label "potworzenie_si&#281;"
  ]
  node [
    id 134
    label "powstanie_warszawskie"
  ]
  node [
    id 135
    label "kl&#281;czenie"
  ]
  node [
    id 136
    label "zbuntowanie_si&#281;"
  ]
  node [
    id 137
    label "siedzenie"
  ]
  node [
    id 138
    label "stworzenie"
  ]
  node [
    id 139
    label "walka"
  ]
  node [
    id 140
    label "zaistnienie"
  ]
  node [
    id 141
    label "odbudowanie_si&#281;"
  ]
  node [
    id 142
    label "pierwocina"
  ]
  node [
    id 143
    label "origin"
  ]
  node [
    id 144
    label "koliszczyzna"
  ]
  node [
    id 145
    label "powstanie_tambowskie"
  ]
  node [
    id 146
    label "&#380;akieria"
  ]
  node [
    id 147
    label "le&#380;enie"
  ]
  node [
    id 148
    label "medium"
  ]
  node [
    id 149
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 150
    label "czas"
  ]
  node [
    id 151
    label "pora_roku"
  ]
  node [
    id 152
    label "okre&#347;lony"
  ]
  node [
    id 153
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 154
    label "szlachetny"
  ]
  node [
    id 155
    label "metaliczny"
  ]
  node [
    id 156
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 157
    label "z&#322;oci&#347;cie"
  ]
  node [
    id 158
    label "grosz"
  ]
  node [
    id 159
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 160
    label "utytu&#322;owany"
  ]
  node [
    id 161
    label "poz&#322;ocenie"
  ]
  node [
    id 162
    label "Polska"
  ]
  node [
    id 163
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 164
    label "wspania&#322;y"
  ]
  node [
    id 165
    label "doskona&#322;y"
  ]
  node [
    id 166
    label "kochany"
  ]
  node [
    id 167
    label "jednostka_monetarna"
  ]
  node [
    id 168
    label "z&#322;ocenie"
  ]
  node [
    id 169
    label "produkowanie"
  ]
  node [
    id 170
    label "robienie"
  ]
  node [
    id 171
    label "byt"
  ]
  node [
    id 172
    label "bycie"
  ]
  node [
    id 173
    label "utrzymywa&#263;"
  ]
  node [
    id 174
    label "znikni&#281;cie"
  ]
  node [
    id 175
    label "utrzyma&#263;"
  ]
  node [
    id 176
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 177
    label "urzeczywistnianie"
  ]
  node [
    id 178
    label "entity"
  ]
  node [
    id 179
    label "egzystencja"
  ]
  node [
    id 180
    label "wyprodukowanie"
  ]
  node [
    id 181
    label "dematerializowanie_si&#281;"
  ]
  node [
    id 182
    label "utrzymanie"
  ]
  node [
    id 183
    label "utrzymywanie"
  ]
  node [
    id 184
    label "being"
  ]
  node [
    id 185
    label "pozostawa&#263;"
  ]
  node [
    id 186
    label "trwa&#263;"
  ]
  node [
    id 187
    label "by&#263;"
  ]
  node [
    id 188
    label "wystarcza&#263;"
  ]
  node [
    id 189
    label "obowi&#261;zywa&#263;"
  ]
  node [
    id 190
    label "czeka&#263;"
  ]
  node [
    id 191
    label "stand"
  ]
  node [
    id 192
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 193
    label "mieszka&#263;"
  ]
  node [
    id 194
    label "wystarczy&#263;"
  ]
  node [
    id 195
    label "sprawowa&#263;"
  ]
  node [
    id 196
    label "przebywa&#263;"
  ]
  node [
    id 197
    label "kosztowa&#263;"
  ]
  node [
    id 198
    label "undertaking"
  ]
  node [
    id 199
    label "wystawa&#263;"
  ]
  node [
    id 200
    label "base"
  ]
  node [
    id 201
    label "digest"
  ]
  node [
    id 202
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 203
    label "wzi&#281;cie"
  ]
  node [
    id 204
    label "doj&#347;cie"
  ]
  node [
    id 205
    label "wnikni&#281;cie"
  ]
  node [
    id 206
    label "spotkanie"
  ]
  node [
    id 207
    label "przekroczenie"
  ]
  node [
    id 208
    label "bramka"
  ]
  node [
    id 209
    label "stanie_si&#281;"
  ]
  node [
    id 210
    label "podw&#243;rze"
  ]
  node [
    id 211
    label "dostanie_si&#281;"
  ]
  node [
    id 212
    label "zaatakowanie"
  ]
  node [
    id 213
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 214
    label "wzniesienie_si&#281;"
  ]
  node [
    id 215
    label "otw&#243;r"
  ]
  node [
    id 216
    label "pojawienie_si&#281;"
  ]
  node [
    id 217
    label "zacz&#281;cie"
  ]
  node [
    id 218
    label "obsi&#261;dni&#281;cie"
  ]
  node [
    id 219
    label "trespass"
  ]
  node [
    id 220
    label "poznanie"
  ]
  node [
    id 221
    label "wnij&#347;cie"
  ]
  node [
    id 222
    label "zdarzenie_si&#281;"
  ]
  node [
    id 223
    label "approach"
  ]
  node [
    id 224
    label "nast&#261;pienie"
  ]
  node [
    id 225
    label "pocz&#261;tek"
  ]
  node [
    id 226
    label "zag&#322;&#281;bienie_si&#281;"
  ]
  node [
    id 227
    label "wpuszczenie"
  ]
  node [
    id 228
    label "stimulation"
  ]
  node [
    id 229
    label "wch&#243;d"
  ]
  node [
    id 230
    label "dost&#281;p"
  ]
  node [
    id 231
    label "cz&#322;onek"
  ]
  node [
    id 232
    label "vent"
  ]
  node [
    id 233
    label "przenikni&#281;cie"
  ]
  node [
    id 234
    label "w&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 235
    label "przy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 236
    label "urz&#261;dzenie"
  ]
  node [
    id 237
    label "release"
  ]
  node [
    id 238
    label "dom"
  ]
  node [
    id 239
    label "Karta_Nauczyciela"
  ]
  node [
    id 240
    label "marc&#243;wka"
  ]
  node [
    id 241
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 242
    label "akt"
  ]
  node [
    id 243
    label "przej&#347;&#263;"
  ]
  node [
    id 244
    label "charter"
  ]
  node [
    id 245
    label "przej&#347;cie"
  ]
  node [
    id 246
    label "w&#322;asny"
  ]
  node [
    id 247
    label "oryginalny"
  ]
  node [
    id 248
    label "autorsko"
  ]
  node [
    id 249
    label "stulecie"
  ]
  node [
    id 250
    label "kalendarz"
  ]
  node [
    id 251
    label "cykl_astronomiczny"
  ]
  node [
    id 252
    label "p&#243;&#322;rocze"
  ]
  node [
    id 253
    label "grupa"
  ]
  node [
    id 254
    label "kwarta&#322;"
  ]
  node [
    id 255
    label "kurs"
  ]
  node [
    id 256
    label "jubileusz"
  ]
  node [
    id 257
    label "miesi&#261;c"
  ]
  node [
    id 258
    label "lata"
  ]
  node [
    id 259
    label "martwy_sezon"
  ]
  node [
    id 260
    label "w&#380;dy"
  ]
  node [
    id 261
    label "kres"
  ]
  node [
    id 262
    label "gleba"
  ]
  node [
    id 263
    label "kondycja"
  ]
  node [
    id 264
    label "ruch"
  ]
  node [
    id 265
    label "inclination"
  ]
  node [
    id 266
    label "pogorszenie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 116
  ]
  edge [
    source 7
    target 117
  ]
  edge [
    source 7
    target 118
  ]
  edge [
    source 7
    target 119
  ]
  edge [
    source 7
    target 120
  ]
  edge [
    source 7
    target 121
  ]
  edge [
    source 7
    target 122
  ]
  edge [
    source 7
    target 123
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 124
  ]
  edge [
    source 8
    target 125
  ]
  edge [
    source 8
    target 126
  ]
  edge [
    source 8
    target 127
  ]
  edge [
    source 8
    target 128
  ]
  edge [
    source 8
    target 129
  ]
  edge [
    source 8
    target 130
  ]
  edge [
    source 8
    target 131
  ]
  edge [
    source 8
    target 132
  ]
  edge [
    source 8
    target 133
  ]
  edge [
    source 8
    target 134
  ]
  edge [
    source 8
    target 135
  ]
  edge [
    source 8
    target 136
  ]
  edge [
    source 8
    target 137
  ]
  edge [
    source 8
    target 138
  ]
  edge [
    source 8
    target 139
  ]
  edge [
    source 8
    target 140
  ]
  edge [
    source 8
    target 141
  ]
  edge [
    source 8
    target 142
  ]
  edge [
    source 8
    target 143
  ]
  edge [
    source 8
    target 144
  ]
  edge [
    source 8
    target 145
  ]
  edge [
    source 8
    target 146
  ]
  edge [
    source 8
    target 147
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 148
  ]
  edge [
    source 9
    target 149
  ]
  edge [
    source 9
    target 150
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 13
  ]
  edge [
    source 10
    target 14
  ]
  edge [
    source 10
    target 151
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 152
  ]
  edge [
    source 11
    target 153
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 154
  ]
  edge [
    source 13
    target 155
  ]
  edge [
    source 13
    target 156
  ]
  edge [
    source 13
    target 157
  ]
  edge [
    source 13
    target 158
  ]
  edge [
    source 13
    target 159
  ]
  edge [
    source 13
    target 160
  ]
  edge [
    source 13
    target 161
  ]
  edge [
    source 13
    target 162
  ]
  edge [
    source 13
    target 163
  ]
  edge [
    source 13
    target 164
  ]
  edge [
    source 13
    target 165
  ]
  edge [
    source 13
    target 166
  ]
  edge [
    source 13
    target 167
  ]
  edge [
    source 13
    target 168
  ]
  edge [
    source 13
    target 19
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 169
  ]
  edge [
    source 14
    target 170
  ]
  edge [
    source 14
    target 171
  ]
  edge [
    source 14
    target 172
  ]
  edge [
    source 14
    target 173
  ]
  edge [
    source 14
    target 174
  ]
  edge [
    source 14
    target 175
  ]
  edge [
    source 14
    target 176
  ]
  edge [
    source 14
    target 177
  ]
  edge [
    source 14
    target 178
  ]
  edge [
    source 14
    target 179
  ]
  edge [
    source 14
    target 180
  ]
  edge [
    source 14
    target 181
  ]
  edge [
    source 14
    target 182
  ]
  edge [
    source 14
    target 183
  ]
  edge [
    source 14
    target 184
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 185
  ]
  edge [
    source 16
    target 186
  ]
  edge [
    source 16
    target 187
  ]
  edge [
    source 16
    target 188
  ]
  edge [
    source 16
    target 189
  ]
  edge [
    source 16
    target 190
  ]
  edge [
    source 16
    target 191
  ]
  edge [
    source 16
    target 192
  ]
  edge [
    source 16
    target 193
  ]
  edge [
    source 16
    target 194
  ]
  edge [
    source 16
    target 195
  ]
  edge [
    source 16
    target 196
  ]
  edge [
    source 16
    target 197
  ]
  edge [
    source 16
    target 198
  ]
  edge [
    source 16
    target 199
  ]
  edge [
    source 16
    target 200
  ]
  edge [
    source 16
    target 201
  ]
  edge [
    source 16
    target 202
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 203
  ]
  edge [
    source 17
    target 204
  ]
  edge [
    source 17
    target 205
  ]
  edge [
    source 17
    target 206
  ]
  edge [
    source 17
    target 207
  ]
  edge [
    source 17
    target 208
  ]
  edge [
    source 17
    target 209
  ]
  edge [
    source 17
    target 210
  ]
  edge [
    source 17
    target 211
  ]
  edge [
    source 17
    target 212
  ]
  edge [
    source 17
    target 213
  ]
  edge [
    source 17
    target 214
  ]
  edge [
    source 17
    target 215
  ]
  edge [
    source 17
    target 216
  ]
  edge [
    source 17
    target 217
  ]
  edge [
    source 17
    target 218
  ]
  edge [
    source 17
    target 219
  ]
  edge [
    source 17
    target 220
  ]
  edge [
    source 17
    target 221
  ]
  edge [
    source 17
    target 222
  ]
  edge [
    source 17
    target 223
  ]
  edge [
    source 17
    target 224
  ]
  edge [
    source 17
    target 225
  ]
  edge [
    source 17
    target 226
  ]
  edge [
    source 17
    target 227
  ]
  edge [
    source 17
    target 228
  ]
  edge [
    source 17
    target 229
  ]
  edge [
    source 17
    target 230
  ]
  edge [
    source 17
    target 231
  ]
  edge [
    source 17
    target 232
  ]
  edge [
    source 17
    target 233
  ]
  edge [
    source 17
    target 234
  ]
  edge [
    source 17
    target 235
  ]
  edge [
    source 17
    target 236
  ]
  edge [
    source 17
    target 237
  ]
  edge [
    source 17
    target 238
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 239
  ]
  edge [
    source 18
    target 240
  ]
  edge [
    source 18
    target 241
  ]
  edge [
    source 18
    target 242
  ]
  edge [
    source 18
    target 243
  ]
  edge [
    source 18
    target 244
  ]
  edge [
    source 18
    target 245
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 246
  ]
  edge [
    source 20
    target 247
  ]
  edge [
    source 20
    target 248
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 249
  ]
  edge [
    source 21
    target 250
  ]
  edge [
    source 21
    target 150
  ]
  edge [
    source 21
    target 151
  ]
  edge [
    source 21
    target 251
  ]
  edge [
    source 21
    target 252
  ]
  edge [
    source 21
    target 253
  ]
  edge [
    source 21
    target 254
  ]
  edge [
    source 21
    target 255
  ]
  edge [
    source 21
    target 256
  ]
  edge [
    source 21
    target 257
  ]
  edge [
    source 21
    target 258
  ]
  edge [
    source 21
    target 259
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 260
  ]
  edge [
    source 23
    target 261
  ]
  edge [
    source 23
    target 262
  ]
  edge [
    source 23
    target 263
  ]
  edge [
    source 23
    target 264
  ]
  edge [
    source 23
    target 265
  ]
  edge [
    source 23
    target 266
  ]
]
