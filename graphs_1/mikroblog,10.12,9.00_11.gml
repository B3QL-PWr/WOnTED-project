graph [
  maxDegree 13
  minDegree 1
  meanDegree 2.0625
  density 0.06653225806451613
  graphCliqueNumber 2
  node [
    id 0
    label "wczorajszy"
    origin "text"
  ]
  node [
    id 1
    label "tinder"
    origin "text"
  ]
  node [
    id 2
    label "randka"
    origin "text"
  ]
  node [
    id 3
    label "uwa&#380;a&#263;"
    origin "text"
  ]
  node [
    id 4
    label "udany"
    origin "text"
  ]
  node [
    id 5
    label "dawny"
  ]
  node [
    id 6
    label "stary"
  ]
  node [
    id 7
    label "archaicznie"
  ]
  node [
    id 8
    label "zgrzybienie"
  ]
  node [
    id 9
    label "przestarzale"
  ]
  node [
    id 10
    label "starzenie_si&#281;"
  ]
  node [
    id 11
    label "zestarzenie_si&#281;"
  ]
  node [
    id 12
    label "niedzisiejszy"
  ]
  node [
    id 13
    label "spotkanie"
  ]
  node [
    id 14
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 15
    label "appointment"
  ]
  node [
    id 16
    label "amory"
  ]
  node [
    id 17
    label "continue"
  ]
  node [
    id 18
    label "str&#243;&#380;y&#263;"
  ]
  node [
    id 19
    label "consider"
  ]
  node [
    id 20
    label "my&#347;le&#263;"
  ]
  node [
    id 21
    label "pilnowa&#263;"
  ]
  node [
    id 22
    label "robi&#263;"
  ]
  node [
    id 23
    label "uznawa&#263;"
  ]
  node [
    id 24
    label "obserwowa&#263;"
  ]
  node [
    id 25
    label "skupia&#263;_si&#281;"
  ]
  node [
    id 26
    label "mie&#263;_na_baczenie"
  ]
  node [
    id 27
    label "deliver"
  ]
  node [
    id 28
    label "fajny"
  ]
  node [
    id 29
    label "dobry"
  ]
  node [
    id 30
    label "przyjemny"
  ]
  node [
    id 31
    label "udanie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
]
