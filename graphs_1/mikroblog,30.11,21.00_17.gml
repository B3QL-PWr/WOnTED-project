graph [
  maxDegree 7
  minDegree 1
  meanDegree 1.8
  density 0.2
  graphCliqueNumber 2
  node [
    id 0
    label "rap"
    origin "text"
  ]
  node [
    id 1
    label "czarnuszyrap"
    origin "text"
  ]
  node [
    id 2
    label "heheszki"
    origin "text"
  ]
  node [
    id 3
    label "humorobrazkowy"
    origin "text"
  ]
  node [
    id 4
    label "muzyka_rozrywkowa"
  ]
  node [
    id 5
    label "asp"
  ]
  node [
    id 6
    label "karpiowate"
  ]
  node [
    id 7
    label "ryba"
  ]
  node [
    id 8
    label "czarna_muzyka"
  ]
  node [
    id 9
    label "drapie&#380;nik"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
]
