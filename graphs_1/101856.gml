graph [
  maxDegree 41
  minDegree 1
  meanDegree 2.0902255639097747
  density 0.005251823024898931
  graphCliqueNumber 4
  node [
    id 0
    label "copyright"
    origin "text"
  ]
  node [
    id 1
    label "alliance"
    origin "text"
  ]
  node [
    id 2
    label "ameryka&#324;ski"
    origin "text"
  ]
  node [
    id 3
    label "organizacja"
    origin "text"
  ]
  node [
    id 4
    label "skupia&#263;"
    origin "text"
  ]
  node [
    id 5
    label "taki"
    origin "text"
  ]
  node [
    id 6
    label "podmiot"
    origin "text"
  ]
  node [
    id 7
    label "jak"
    origin "text"
  ]
  node [
    id 8
    label "mpaa"
    origin "text"
  ]
  node [
    id 9
    label "riaa"
    origin "text"
  ]
  node [
    id 10
    label "przedstawiciel"
    origin "text"
  ]
  node [
    id 11
    label "przemys&#322;"
    origin "text"
  ]
  node [
    id 12
    label "kultura"
    origin "text"
  ]
  node [
    id 13
    label "usa"
    origin "text"
  ]
  node [
    id 14
    label "tzw"
    origin "text"
  ]
  node [
    id 15
    label "content"
    origin "text"
  ]
  node [
    id 16
    label "rozes&#322;a&#263;"
    origin "text"
  ]
  node [
    id 17
    label "osoba"
    origin "text"
  ]
  node [
    id 18
    label "ubiega&#263;"
    origin "text"
  ]
  node [
    id 19
    label "si&#281;"
    origin "text"
  ]
  node [
    id 20
    label "kandydowa&#263;"
    origin "text"
  ]
  node [
    id 21
    label "wybory"
    origin "text"
  ]
  node [
    id 22
    label "prezydencki"
    origin "text"
  ]
  node [
    id 23
    label "rocznik"
    origin "text"
  ]
  node [
    id 24
    label "list"
    origin "text"
  ]
  node [
    id 25
    label "po&#322;&#261;czy&#263;"
    origin "text"
  ]
  node [
    id 26
    label "kwestionariusz"
    origin "text"
  ]
  node [
    id 27
    label "informowa&#263;"
    origin "text"
  ]
  node [
    id 28
    label "sondowa&#263;"
    origin "text"
  ]
  node [
    id 29
    label "polityk"
    origin "text"
  ]
  node [
    id 30
    label "sprawa"
    origin "text"
  ]
  node [
    id 31
    label "circa"
    origin "text"
  ]
  node [
    id 32
    label "apelowa&#263;"
    origin "text"
  ]
  node [
    id 33
    label "zaostrzenie"
    origin "text"
  ]
  node [
    id 34
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 35
    label "skuteczno&#347;&#263;"
    origin "text"
  ]
  node [
    id 36
    label "walka"
    origin "text"
  ]
  node [
    id 37
    label "piractwo"
    origin "text"
  ]
  node [
    id 38
    label "dla"
    origin "text"
  ]
  node [
    id 39
    label "dobra"
    origin "text"
  ]
  node [
    id 40
    label "tylko"
    origin "text"
  ]
  node [
    id 41
    label "liczy&#263;"
    origin "text"
  ]
  node [
    id 42
    label "milion"
    origin "text"
  ]
  node [
    id 43
    label "&#380;yj&#261;cy"
    origin "text"
  ]
  node [
    id 44
    label "prawy"
    origin "text"
  ]
  node [
    id 45
    label "autorski"
    origin "text"
  ]
  node [
    id 46
    label "ale"
    origin "text"
  ]
  node [
    id 47
    label "tak&#380;e"
    origin "text"
  ]
  node [
    id 48
    label "ca&#322;y"
    origin "text"
  ]
  node [
    id 49
    label "spo&#322;ecze&#324;stwo"
    origin "text"
  ]
  node [
    id 50
    label "nowoczesny"
  ]
  node [
    id 51
    label "p&#243;&#322;nocnoameryka&#324;ski"
  ]
  node [
    id 52
    label "po_ameryka&#324;sku"
  ]
  node [
    id 53
    label "boston"
  ]
  node [
    id 54
    label "cake-walk"
  ]
  node [
    id 55
    label "charakterystyczny"
  ]
  node [
    id 56
    label "placek_ameryka&#324;ski"
  ]
  node [
    id 57
    label "fajny"
  ]
  node [
    id 58
    label "j&#281;zyk_angielski"
  ]
  node [
    id 59
    label "Princeton"
  ]
  node [
    id 60
    label "pepperoni"
  ]
  node [
    id 61
    label "sa&#322;atka_Snickers"
  ]
  node [
    id 62
    label "zachodni"
  ]
  node [
    id 63
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 64
    label "anglosaski"
  ]
  node [
    id 65
    label "typowy"
  ]
  node [
    id 66
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 67
    label "endecki"
  ]
  node [
    id 68
    label "komitet_koordynacyjny"
  ]
  node [
    id 69
    label "przybud&#243;wka"
  ]
  node [
    id 70
    label "ZOMO"
  ]
  node [
    id 71
    label "boj&#243;wka"
  ]
  node [
    id 72
    label "zesp&#243;&#322;"
  ]
  node [
    id 73
    label "organization"
  ]
  node [
    id 74
    label "TOPR"
  ]
  node [
    id 75
    label "jednostka_organizacyjna"
  ]
  node [
    id 76
    label "przedstawicielstwo"
  ]
  node [
    id 77
    label "Cepelia"
  ]
  node [
    id 78
    label "GOPR"
  ]
  node [
    id 79
    label "ZMP"
  ]
  node [
    id 80
    label "ZBoWiD"
  ]
  node [
    id 81
    label "struktura"
  ]
  node [
    id 82
    label "od&#322;am"
  ]
  node [
    id 83
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 84
    label "centrala"
  ]
  node [
    id 85
    label "zbiera&#263;"
  ]
  node [
    id 86
    label "masowa&#263;"
  ]
  node [
    id 87
    label "ognisko"
  ]
  node [
    id 88
    label "po&#347;wi&#281;ca&#263;"
  ]
  node [
    id 89
    label "huddle"
  ]
  node [
    id 90
    label "okre&#347;lony"
  ]
  node [
    id 91
    label "jaki&#347;"
  ]
  node [
    id 92
    label "cz&#322;owiek"
  ]
  node [
    id 93
    label "byt"
  ]
  node [
    id 94
    label "prawo"
  ]
  node [
    id 95
    label "nauka_prawa"
  ]
  node [
    id 96
    label "osobowo&#347;&#263;"
  ]
  node [
    id 97
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 98
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 99
    label "byd&#322;o"
  ]
  node [
    id 100
    label "zobo"
  ]
  node [
    id 101
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 102
    label "yakalo"
  ]
  node [
    id 103
    label "dzo"
  ]
  node [
    id 104
    label "cz&#322;onek"
  ]
  node [
    id 105
    label "substytuowanie"
  ]
  node [
    id 106
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 107
    label "przyk&#322;ad"
  ]
  node [
    id 108
    label "zast&#281;pca"
  ]
  node [
    id 109
    label "substytuowa&#263;"
  ]
  node [
    id 110
    label "gospodarka"
  ]
  node [
    id 111
    label "przechowalnictwo"
  ]
  node [
    id 112
    label "uprzemys&#322;owienie"
  ]
  node [
    id 113
    label "przemys&#322;&#243;wka"
  ]
  node [
    id 114
    label "uprzemys&#322;owienie_si&#281;"
  ]
  node [
    id 115
    label "uprzemys&#322;awianie_si&#281;"
  ]
  node [
    id 116
    label "uprzemys&#322;awianie"
  ]
  node [
    id 117
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 118
    label "przedmiot"
  ]
  node [
    id 119
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 120
    label "Wsch&#243;d"
  ]
  node [
    id 121
    label "rzecz"
  ]
  node [
    id 122
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 123
    label "sztuka"
  ]
  node [
    id 124
    label "religia"
  ]
  node [
    id 125
    label "przejmowa&#263;"
  ]
  node [
    id 126
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 127
    label "makrokosmos"
  ]
  node [
    id 128
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 129
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 130
    label "zjawisko"
  ]
  node [
    id 131
    label "praca_rolnicza"
  ]
  node [
    id 132
    label "tradycja"
  ]
  node [
    id 133
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 134
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 135
    label "przejmowanie"
  ]
  node [
    id 136
    label "cecha"
  ]
  node [
    id 137
    label "asymilowanie_si&#281;"
  ]
  node [
    id 138
    label "przej&#261;&#263;"
  ]
  node [
    id 139
    label "hodowla"
  ]
  node [
    id 140
    label "brzoskwiniarnia"
  ]
  node [
    id 141
    label "populace"
  ]
  node [
    id 142
    label "konwencja"
  ]
  node [
    id 143
    label "propriety"
  ]
  node [
    id 144
    label "jako&#347;&#263;"
  ]
  node [
    id 145
    label "kuchnia"
  ]
  node [
    id 146
    label "zwyczaj"
  ]
  node [
    id 147
    label "przej&#281;cie"
  ]
  node [
    id 148
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 149
    label "roz&#322;o&#380;y&#263;"
  ]
  node [
    id 150
    label "ship"
  ]
  node [
    id 151
    label "przes&#322;a&#263;"
  ]
  node [
    id 152
    label "&#322;&#243;&#380;ko"
  ]
  node [
    id 153
    label "Zgredek"
  ]
  node [
    id 154
    label "kategoria_gramatyczna"
  ]
  node [
    id 155
    label "Casanova"
  ]
  node [
    id 156
    label "Don_Juan"
  ]
  node [
    id 157
    label "Gargantua"
  ]
  node [
    id 158
    label "Faust"
  ]
  node [
    id 159
    label "profanum"
  ]
  node [
    id 160
    label "Chocho&#322;"
  ]
  node [
    id 161
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 162
    label "koniugacja"
  ]
  node [
    id 163
    label "Winnetou"
  ]
  node [
    id 164
    label "Dwukwiat"
  ]
  node [
    id 165
    label "homo_sapiens"
  ]
  node [
    id 166
    label "Edyp"
  ]
  node [
    id 167
    label "Herkules_Poirot"
  ]
  node [
    id 168
    label "ludzko&#347;&#263;"
  ]
  node [
    id 169
    label "mikrokosmos"
  ]
  node [
    id 170
    label "person"
  ]
  node [
    id 171
    label "Sherlock_Holmes"
  ]
  node [
    id 172
    label "portrecista"
  ]
  node [
    id 173
    label "Szwejk"
  ]
  node [
    id 174
    label "Hamlet"
  ]
  node [
    id 175
    label "duch"
  ]
  node [
    id 176
    label "g&#322;owa"
  ]
  node [
    id 177
    label "oddzia&#322;ywanie"
  ]
  node [
    id 178
    label "Quasimodo"
  ]
  node [
    id 179
    label "Dulcynea"
  ]
  node [
    id 180
    label "Don_Kiszot"
  ]
  node [
    id 181
    label "Wallenrod"
  ]
  node [
    id 182
    label "Plastu&#347;"
  ]
  node [
    id 183
    label "Harry_Potter"
  ]
  node [
    id 184
    label "figura"
  ]
  node [
    id 185
    label "parali&#380;owa&#263;"
  ]
  node [
    id 186
    label "istota"
  ]
  node [
    id 187
    label "Werter"
  ]
  node [
    id 188
    label "antropochoria"
  ]
  node [
    id 189
    label "posta&#263;"
  ]
  node [
    id 190
    label "robi&#263;"
  ]
  node [
    id 191
    label "anticipate"
  ]
  node [
    id 192
    label "draw_a_bead_on"
  ]
  node [
    id 193
    label "stara&#263;_si&#281;"
  ]
  node [
    id 194
    label "skrutator"
  ]
  node [
    id 195
    label "g&#322;osowanie"
  ]
  node [
    id 196
    label "formacja"
  ]
  node [
    id 197
    label "kronika"
  ]
  node [
    id 198
    label "czasopismo"
  ]
  node [
    id 199
    label "yearbook"
  ]
  node [
    id 200
    label "li&#347;&#263;"
  ]
  node [
    id 201
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 202
    label "poczta"
  ]
  node [
    id 203
    label "epistolografia"
  ]
  node [
    id 204
    label "przesy&#322;ka"
  ]
  node [
    id 205
    label "poczta_elektroniczna"
  ]
  node [
    id 206
    label "znaczek_pocztowy"
  ]
  node [
    id 207
    label "relate"
  ]
  node [
    id 208
    label "spowodowa&#263;"
  ]
  node [
    id 209
    label "stworzy&#263;"
  ]
  node [
    id 210
    label "po&#322;&#261;czenie"
  ]
  node [
    id 211
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 212
    label "connect"
  ]
  node [
    id 213
    label "zjednoczy&#263;"
  ]
  node [
    id 214
    label "incorporate"
  ]
  node [
    id 215
    label "zrobi&#263;"
  ]
  node [
    id 216
    label "kafeteria"
  ]
  node [
    id 217
    label "formularz"
  ]
  node [
    id 218
    label "komunikowa&#263;"
  ]
  node [
    id 219
    label "powiada&#263;"
  ]
  node [
    id 220
    label "inform"
  ]
  node [
    id 221
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 222
    label "analizowa&#263;"
  ]
  node [
    id 223
    label "sound"
  ]
  node [
    id 224
    label "maca&#263;"
  ]
  node [
    id 225
    label "bada&#263;"
  ]
  node [
    id 226
    label "question"
  ]
  node [
    id 227
    label "J&#281;drzejewicz"
  ]
  node [
    id 228
    label "Sto&#322;ypin"
  ]
  node [
    id 229
    label "Nixon"
  ]
  node [
    id 230
    label "Perykles"
  ]
  node [
    id 231
    label "bezpartyjny"
  ]
  node [
    id 232
    label "Gomu&#322;ka"
  ]
  node [
    id 233
    label "Gorbaczow"
  ]
  node [
    id 234
    label "Borel"
  ]
  node [
    id 235
    label "Katon"
  ]
  node [
    id 236
    label "McCarthy"
  ]
  node [
    id 237
    label "Naser"
  ]
  node [
    id 238
    label "Gierek"
  ]
  node [
    id 239
    label "Goebbels"
  ]
  node [
    id 240
    label "Ko&#322;&#322;&#261;taj"
  ]
  node [
    id 241
    label "Jaros&#322;aw_Kaczy&#324;ski"
  ]
  node [
    id 242
    label "de_Gaulle"
  ]
  node [
    id 243
    label "Vladimir_Me&#269;iar"
  ]
  node [
    id 244
    label "Bre&#380;niew"
  ]
  node [
    id 245
    label "Juliusz_Cezar"
  ]
  node [
    id 246
    label "Bierut"
  ]
  node [
    id 247
    label "Kuro&#324;"
  ]
  node [
    id 248
    label "Arafat"
  ]
  node [
    id 249
    label "Fidel_Castro"
  ]
  node [
    id 250
    label "Moczar"
  ]
  node [
    id 251
    label "Miko&#322;ajczyk"
  ]
  node [
    id 252
    label "Korwin"
  ]
  node [
    id 253
    label "dzia&#322;acz"
  ]
  node [
    id 254
    label "Winston_Churchill"
  ]
  node [
    id 255
    label "Leszek_Miller"
  ]
  node [
    id 256
    label "Ziobro"
  ]
  node [
    id 257
    label "Chruszczow"
  ]
  node [
    id 258
    label "Putin"
  ]
  node [
    id 259
    label "Falandysz"
  ]
  node [
    id 260
    label "Mao"
  ]
  node [
    id 261
    label "Metternich"
  ]
  node [
    id 262
    label "temat"
  ]
  node [
    id 263
    label "kognicja"
  ]
  node [
    id 264
    label "idea"
  ]
  node [
    id 265
    label "szczeg&#243;&#322;"
  ]
  node [
    id 266
    label "wydarzenie"
  ]
  node [
    id 267
    label "przes&#322;anka"
  ]
  node [
    id 268
    label "rozprawa"
  ]
  node [
    id 269
    label "object"
  ]
  node [
    id 270
    label "proposition"
  ]
  node [
    id 271
    label "call"
  ]
  node [
    id 272
    label "prosi&#263;"
  ]
  node [
    id 273
    label "odwo&#322;ywa&#263;_si&#281;"
  ]
  node [
    id 274
    label "przemawia&#263;"
  ]
  node [
    id 275
    label "address"
  ]
  node [
    id 276
    label "ostrzejszy"
  ]
  node [
    id 277
    label "wzmo&#380;enie"
  ]
  node [
    id 278
    label "czynno&#347;&#263;"
  ]
  node [
    id 279
    label "spowodowanie"
  ]
  node [
    id 280
    label "przyprawienie"
  ]
  node [
    id 281
    label "surowszy"
  ]
  node [
    id 282
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 283
    label "eskalowanie"
  ]
  node [
    id 284
    label "ostry"
  ]
  node [
    id 285
    label "uwydatnienie"
  ]
  node [
    id 286
    label "pogorszenie"
  ]
  node [
    id 287
    label "aggravation"
  ]
  node [
    id 288
    label "doros&#322;y"
  ]
  node [
    id 289
    label "wiele"
  ]
  node [
    id 290
    label "dorodny"
  ]
  node [
    id 291
    label "znaczny"
  ]
  node [
    id 292
    label "du&#380;o"
  ]
  node [
    id 293
    label "prawdziwy"
  ]
  node [
    id 294
    label "niema&#322;o"
  ]
  node [
    id 295
    label "wa&#380;ny"
  ]
  node [
    id 296
    label "rozwini&#281;ty"
  ]
  node [
    id 297
    label "effectiveness"
  ]
  node [
    id 298
    label "w&#322;a&#347;ciwo&#347;&#263;"
  ]
  node [
    id 299
    label "czyn"
  ]
  node [
    id 300
    label "trudno&#347;&#263;"
  ]
  node [
    id 301
    label "zaatakowanie"
  ]
  node [
    id 302
    label "obrona"
  ]
  node [
    id 303
    label "konfrontacyjny"
  ]
  node [
    id 304
    label "military_action"
  ]
  node [
    id 305
    label "wrestle"
  ]
  node [
    id 306
    label "action"
  ]
  node [
    id 307
    label "rywalizacja"
  ]
  node [
    id 308
    label "sambo"
  ]
  node [
    id 309
    label "contest"
  ]
  node [
    id 310
    label "sp&#243;r"
  ]
  node [
    id 311
    label "bootleg"
  ]
  node [
    id 312
    label "przest&#281;pstwo"
  ]
  node [
    id 313
    label "plagiarism"
  ]
  node [
    id 314
    label "nielegalno&#347;&#263;"
  ]
  node [
    id 315
    label "bandytyzm"
  ]
  node [
    id 316
    label "terroryzm"
  ]
  node [
    id 317
    label "transmisja"
  ]
  node [
    id 318
    label "reprodukcja"
  ]
  node [
    id 319
    label "frymark"
  ]
  node [
    id 320
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 321
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 322
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 323
    label "commodity"
  ]
  node [
    id 324
    label "mienie"
  ]
  node [
    id 325
    label "Wilko"
  ]
  node [
    id 326
    label "jednostka_monetarna"
  ]
  node [
    id 327
    label "centym"
  ]
  node [
    id 328
    label "report"
  ]
  node [
    id 329
    label "dodawa&#263;"
  ]
  node [
    id 330
    label "wymienia&#263;"
  ]
  node [
    id 331
    label "okre&#347;la&#263;"
  ]
  node [
    id 332
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 333
    label "dyskalkulia"
  ]
  node [
    id 334
    label "wynagrodzenie"
  ]
  node [
    id 335
    label "admit"
  ]
  node [
    id 336
    label "osi&#261;ga&#263;"
  ]
  node [
    id 337
    label "wyznacza&#263;"
  ]
  node [
    id 338
    label "posiada&#263;"
  ]
  node [
    id 339
    label "mierzy&#263;"
  ]
  node [
    id 340
    label "odlicza&#263;"
  ]
  node [
    id 341
    label "bra&#263;"
  ]
  node [
    id 342
    label "wycenia&#263;"
  ]
  node [
    id 343
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 344
    label "rachowa&#263;"
  ]
  node [
    id 345
    label "tell"
  ]
  node [
    id 346
    label "uwzgl&#281;dnia&#263;"
  ]
  node [
    id 347
    label "policza&#263;"
  ]
  node [
    id 348
    label "count"
  ]
  node [
    id 349
    label "liczba"
  ]
  node [
    id 350
    label "miljon"
  ]
  node [
    id 351
    label "ba&#324;ka"
  ]
  node [
    id 352
    label "z_prawa"
  ]
  node [
    id 353
    label "cnotliwy"
  ]
  node [
    id 354
    label "moralny"
  ]
  node [
    id 355
    label "naturalny"
  ]
  node [
    id 356
    label "zgodnie_z_prawem"
  ]
  node [
    id 357
    label "legalny"
  ]
  node [
    id 358
    label "na_prawo"
  ]
  node [
    id 359
    label "s&#322;uszny"
  ]
  node [
    id 360
    label "w_prawo"
  ]
  node [
    id 361
    label "w_spos&#243;b_prawy"
  ]
  node [
    id 362
    label "prawicowy"
  ]
  node [
    id 363
    label "chwalebny"
  ]
  node [
    id 364
    label "zacny"
  ]
  node [
    id 365
    label "w&#322;asny"
  ]
  node [
    id 366
    label "oryginalny"
  ]
  node [
    id 367
    label "autorsko"
  ]
  node [
    id 368
    label "piwo"
  ]
  node [
    id 369
    label "jedyny"
  ]
  node [
    id 370
    label "kompletny"
  ]
  node [
    id 371
    label "zdr&#243;w"
  ]
  node [
    id 372
    label "&#380;ywy"
  ]
  node [
    id 373
    label "ca&#322;o"
  ]
  node [
    id 374
    label "pe&#322;ny"
  ]
  node [
    id 375
    label "calu&#347;ko"
  ]
  node [
    id 376
    label "podobny"
  ]
  node [
    id 377
    label "pole"
  ]
  node [
    id 378
    label "kastowo&#347;&#263;"
  ]
  node [
    id 379
    label "ludzie_pracy"
  ]
  node [
    id 380
    label "community"
  ]
  node [
    id 381
    label "status"
  ]
  node [
    id 382
    label "cywilizacja"
  ]
  node [
    id 383
    label "pozaklasowy"
  ]
  node [
    id 384
    label "aspo&#322;eczny"
  ]
  node [
    id 385
    label "uwarstwienie"
  ]
  node [
    id 386
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 387
    label "elita"
  ]
  node [
    id 388
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 389
    label "wspo&#322;ecze&#324;stwo"
  ]
  node [
    id 390
    label "klasa"
  ]
  node [
    id 391
    label "Alliance"
  ]
  node [
    id 392
    label "Ryan"
  ]
  node [
    id 393
    label "Paula"
  ]
  node [
    id 394
    label "digital"
  ]
  node [
    id 395
    label "Milennium"
  ]
  node [
    id 396
    label "Act"
  ]
  node [
    id 397
    label "Sonny"
  ]
  node [
    id 398
    label "bono"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 391
  ]
  edge [
    source 0
    target 394
  ]
  edge [
    source 0
    target 395
  ]
  edge [
    source 0
    target 396
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 26
  ]
  edge [
    source 7
    target 27
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 10
    target 105
  ]
  edge [
    source 10
    target 106
  ]
  edge [
    source 10
    target 107
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 17
  ]
  edge [
    source 11
    target 43
  ]
  edge [
    source 11
    target 110
  ]
  edge [
    source 11
    target 111
  ]
  edge [
    source 11
    target 112
  ]
  edge [
    source 11
    target 113
  ]
  edge [
    source 11
    target 114
  ]
  edge [
    source 11
    target 115
  ]
  edge [
    source 11
    target 116
  ]
  edge [
    source 11
    target 36
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 117
  ]
  edge [
    source 12
    target 118
  ]
  edge [
    source 12
    target 119
  ]
  edge [
    source 12
    target 120
  ]
  edge [
    source 12
    target 121
  ]
  edge [
    source 12
    target 122
  ]
  edge [
    source 12
    target 123
  ]
  edge [
    source 12
    target 124
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 12
    target 131
  ]
  edge [
    source 12
    target 132
  ]
  edge [
    source 12
    target 133
  ]
  edge [
    source 12
    target 134
  ]
  edge [
    source 12
    target 135
  ]
  edge [
    source 12
    target 136
  ]
  edge [
    source 12
    target 137
  ]
  edge [
    source 12
    target 138
  ]
  edge [
    source 12
    target 139
  ]
  edge [
    source 12
    target 140
  ]
  edge [
    source 12
    target 141
  ]
  edge [
    source 12
    target 142
  ]
  edge [
    source 12
    target 143
  ]
  edge [
    source 12
    target 144
  ]
  edge [
    source 12
    target 145
  ]
  edge [
    source 12
    target 146
  ]
  edge [
    source 12
    target 147
  ]
  edge [
    source 12
    target 148
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 22
  ]
  edge [
    source 13
    target 23
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 149
  ]
  edge [
    source 16
    target 150
  ]
  edge [
    source 16
    target 151
  ]
  edge [
    source 16
    target 152
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 42
  ]
  edge [
    source 17
    target 153
  ]
  edge [
    source 17
    target 154
  ]
  edge [
    source 17
    target 155
  ]
  edge [
    source 17
    target 156
  ]
  edge [
    source 17
    target 157
  ]
  edge [
    source 17
    target 158
  ]
  edge [
    source 17
    target 159
  ]
  edge [
    source 17
    target 160
  ]
  edge [
    source 17
    target 161
  ]
  edge [
    source 17
    target 162
  ]
  edge [
    source 17
    target 163
  ]
  edge [
    source 17
    target 164
  ]
  edge [
    source 17
    target 165
  ]
  edge [
    source 17
    target 166
  ]
  edge [
    source 17
    target 167
  ]
  edge [
    source 17
    target 168
  ]
  edge [
    source 17
    target 169
  ]
  edge [
    source 17
    target 170
  ]
  edge [
    source 17
    target 171
  ]
  edge [
    source 17
    target 172
  ]
  edge [
    source 17
    target 173
  ]
  edge [
    source 17
    target 174
  ]
  edge [
    source 17
    target 175
  ]
  edge [
    source 17
    target 176
  ]
  edge [
    source 17
    target 177
  ]
  edge [
    source 17
    target 178
  ]
  edge [
    source 17
    target 179
  ]
  edge [
    source 17
    target 180
  ]
  edge [
    source 17
    target 181
  ]
  edge [
    source 17
    target 182
  ]
  edge [
    source 17
    target 183
  ]
  edge [
    source 17
    target 184
  ]
  edge [
    source 17
    target 185
  ]
  edge [
    source 17
    target 186
  ]
  edge [
    source 17
    target 187
  ]
  edge [
    source 17
    target 188
  ]
  edge [
    source 17
    target 189
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 190
  ]
  edge [
    source 18
    target 191
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 192
  ]
  edge [
    source 20
    target 193
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 194
  ]
  edge [
    source 21
    target 195
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 196
  ]
  edge [
    source 23
    target 197
  ]
  edge [
    source 23
    target 198
  ]
  edge [
    source 23
    target 199
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 200
  ]
  edge [
    source 24
    target 201
  ]
  edge [
    source 24
    target 202
  ]
  edge [
    source 24
    target 203
  ]
  edge [
    source 24
    target 204
  ]
  edge [
    source 24
    target 205
  ]
  edge [
    source 24
    target 206
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 207
  ]
  edge [
    source 25
    target 208
  ]
  edge [
    source 25
    target 209
  ]
  edge [
    source 25
    target 210
  ]
  edge [
    source 25
    target 211
  ]
  edge [
    source 25
    target 212
  ]
  edge [
    source 25
    target 213
  ]
  edge [
    source 25
    target 214
  ]
  edge [
    source 25
    target 215
  ]
  edge [
    source 26
    target 216
  ]
  edge [
    source 26
    target 217
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 218
  ]
  edge [
    source 27
    target 219
  ]
  edge [
    source 27
    target 220
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 221
  ]
  edge [
    source 28
    target 222
  ]
  edge [
    source 28
    target 223
  ]
  edge [
    source 28
    target 224
  ]
  edge [
    source 28
    target 225
  ]
  edge [
    source 28
    target 226
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 227
  ]
  edge [
    source 29
    target 228
  ]
  edge [
    source 29
    target 229
  ]
  edge [
    source 29
    target 230
  ]
  edge [
    source 29
    target 231
  ]
  edge [
    source 29
    target 232
  ]
  edge [
    source 29
    target 233
  ]
  edge [
    source 29
    target 234
  ]
  edge [
    source 29
    target 235
  ]
  edge [
    source 29
    target 236
  ]
  edge [
    source 29
    target 237
  ]
  edge [
    source 29
    target 238
  ]
  edge [
    source 29
    target 239
  ]
  edge [
    source 29
    target 240
  ]
  edge [
    source 29
    target 241
  ]
  edge [
    source 29
    target 242
  ]
  edge [
    source 29
    target 243
  ]
  edge [
    source 29
    target 244
  ]
  edge [
    source 29
    target 245
  ]
  edge [
    source 29
    target 246
  ]
  edge [
    source 29
    target 247
  ]
  edge [
    source 29
    target 248
  ]
  edge [
    source 29
    target 249
  ]
  edge [
    source 29
    target 250
  ]
  edge [
    source 29
    target 251
  ]
  edge [
    source 29
    target 252
  ]
  edge [
    source 29
    target 253
  ]
  edge [
    source 29
    target 254
  ]
  edge [
    source 29
    target 255
  ]
  edge [
    source 29
    target 256
  ]
  edge [
    source 29
    target 257
  ]
  edge [
    source 29
    target 258
  ]
  edge [
    source 29
    target 259
  ]
  edge [
    source 29
    target 260
  ]
  edge [
    source 29
    target 261
  ]
  edge [
    source 30
    target 262
  ]
  edge [
    source 30
    target 263
  ]
  edge [
    source 30
    target 264
  ]
  edge [
    source 30
    target 265
  ]
  edge [
    source 30
    target 121
  ]
  edge [
    source 30
    target 266
  ]
  edge [
    source 30
    target 267
  ]
  edge [
    source 30
    target 268
  ]
  edge [
    source 30
    target 269
  ]
  edge [
    source 30
    target 270
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 271
  ]
  edge [
    source 32
    target 272
  ]
  edge [
    source 32
    target 273
  ]
  edge [
    source 32
    target 274
  ]
  edge [
    source 32
    target 275
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 276
  ]
  edge [
    source 33
    target 277
  ]
  edge [
    source 33
    target 278
  ]
  edge [
    source 33
    target 279
  ]
  edge [
    source 33
    target 280
  ]
  edge [
    source 33
    target 281
  ]
  edge [
    source 33
    target 282
  ]
  edge [
    source 33
    target 283
  ]
  edge [
    source 33
    target 284
  ]
  edge [
    source 33
    target 285
  ]
  edge [
    source 33
    target 286
  ]
  edge [
    source 33
    target 287
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 288
  ]
  edge [
    source 34
    target 289
  ]
  edge [
    source 34
    target 290
  ]
  edge [
    source 34
    target 291
  ]
  edge [
    source 34
    target 292
  ]
  edge [
    source 34
    target 293
  ]
  edge [
    source 34
    target 294
  ]
  edge [
    source 34
    target 295
  ]
  edge [
    source 34
    target 296
  ]
  edge [
    source 34
    target 48
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 297
  ]
  edge [
    source 35
    target 136
  ]
  edge [
    source 35
    target 298
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 299
  ]
  edge [
    source 36
    target 300
  ]
  edge [
    source 36
    target 301
  ]
  edge [
    source 36
    target 302
  ]
  edge [
    source 36
    target 303
  ]
  edge [
    source 36
    target 304
  ]
  edge [
    source 36
    target 305
  ]
  edge [
    source 36
    target 306
  ]
  edge [
    source 36
    target 266
  ]
  edge [
    source 36
    target 307
  ]
  edge [
    source 36
    target 308
  ]
  edge [
    source 36
    target 309
  ]
  edge [
    source 36
    target 310
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 311
  ]
  edge [
    source 37
    target 312
  ]
  edge [
    source 37
    target 313
  ]
  edge [
    source 37
    target 314
  ]
  edge [
    source 37
    target 315
  ]
  edge [
    source 37
    target 316
  ]
  edge [
    source 37
    target 317
  ]
  edge [
    source 37
    target 318
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 47
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 48
  ]
  edge [
    source 39
    target 319
  ]
  edge [
    source 39
    target 320
  ]
  edge [
    source 39
    target 321
  ]
  edge [
    source 39
    target 322
  ]
  edge [
    source 39
    target 323
  ]
  edge [
    source 39
    target 324
  ]
  edge [
    source 39
    target 325
  ]
  edge [
    source 39
    target 326
  ]
  edge [
    source 39
    target 327
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 328
  ]
  edge [
    source 41
    target 329
  ]
  edge [
    source 41
    target 330
  ]
  edge [
    source 41
    target 331
  ]
  edge [
    source 41
    target 332
  ]
  edge [
    source 41
    target 333
  ]
  edge [
    source 41
    target 334
  ]
  edge [
    source 41
    target 335
  ]
  edge [
    source 41
    target 336
  ]
  edge [
    source 41
    target 337
  ]
  edge [
    source 41
    target 338
  ]
  edge [
    source 41
    target 339
  ]
  edge [
    source 41
    target 340
  ]
  edge [
    source 41
    target 341
  ]
  edge [
    source 41
    target 342
  ]
  edge [
    source 41
    target 343
  ]
  edge [
    source 41
    target 344
  ]
  edge [
    source 41
    target 345
  ]
  edge [
    source 41
    target 346
  ]
  edge [
    source 41
    target 347
  ]
  edge [
    source 41
    target 348
  ]
  edge [
    source 42
    target 349
  ]
  edge [
    source 42
    target 350
  ]
  edge [
    source 42
    target 351
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 352
  ]
  edge [
    source 44
    target 353
  ]
  edge [
    source 44
    target 354
  ]
  edge [
    source 44
    target 355
  ]
  edge [
    source 44
    target 356
  ]
  edge [
    source 44
    target 357
  ]
  edge [
    source 44
    target 358
  ]
  edge [
    source 44
    target 359
  ]
  edge [
    source 44
    target 360
  ]
  edge [
    source 44
    target 361
  ]
  edge [
    source 44
    target 362
  ]
  edge [
    source 44
    target 363
  ]
  edge [
    source 44
    target 364
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 365
  ]
  edge [
    source 45
    target 366
  ]
  edge [
    source 45
    target 367
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 368
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 369
  ]
  edge [
    source 48
    target 370
  ]
  edge [
    source 48
    target 371
  ]
  edge [
    source 48
    target 372
  ]
  edge [
    source 48
    target 373
  ]
  edge [
    source 48
    target 374
  ]
  edge [
    source 48
    target 375
  ]
  edge [
    source 48
    target 376
  ]
  edge [
    source 49
    target 377
  ]
  edge [
    source 49
    target 378
  ]
  edge [
    source 49
    target 379
  ]
  edge [
    source 49
    target 380
  ]
  edge [
    source 49
    target 381
  ]
  edge [
    source 49
    target 382
  ]
  edge [
    source 49
    target 383
  ]
  edge [
    source 49
    target 384
  ]
  edge [
    source 49
    target 385
  ]
  edge [
    source 49
    target 386
  ]
  edge [
    source 49
    target 387
  ]
  edge [
    source 49
    target 388
  ]
  edge [
    source 49
    target 389
  ]
  edge [
    source 49
    target 390
  ]
  edge [
    source 392
    target 393
  ]
  edge [
    source 394
    target 395
  ]
  edge [
    source 394
    target 396
  ]
  edge [
    source 395
    target 396
  ]
  edge [
    source 396
    target 397
  ]
  edge [
    source 396
    target 398
  ]
  edge [
    source 397
    target 398
  ]
]
