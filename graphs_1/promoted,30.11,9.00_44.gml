graph [
  maxDegree 10
  minDegree 1
  meanDegree 1.951219512195122
  density 0.04878048780487805
  graphCliqueNumber 3
  node [
    id 0
    label "ringier"
    origin "text"
  ]
  node [
    id 1
    label "axel"
    origin "text"
  ]
  node [
    id 2
    label "springer"
    origin "text"
  ]
  node [
    id 3
    label "polska"
    origin "text"
  ]
  node [
    id 4
    label "wys&#322;a&#263;"
    origin "text"
  ]
  node [
    id 5
    label "pozew"
    origin "text"
  ]
  node [
    id 6
    label "cywilny"
    origin "text"
  ]
  node [
    id 7
    label "samuel"
    origin "text"
  ]
  node [
    id 8
    label "pereiry"
    origin "text"
  ]
  node [
    id 9
    label "szef"
    origin "text"
  ]
  node [
    id 10
    label "portal"
    origin "text"
  ]
  node [
    id 11
    label "tvp"
    origin "text"
  ]
  node [
    id 12
    label "jazda_figurowa"
  ]
  node [
    id 13
    label "skok"
  ]
  node [
    id 14
    label "wytworzy&#263;"
  ]
  node [
    id 15
    label "line"
  ]
  node [
    id 16
    label "ship"
  ]
  node [
    id 17
    label "convey"
  ]
  node [
    id 18
    label "przekaza&#263;"
  ]
  node [
    id 19
    label "post"
  ]
  node [
    id 20
    label "wy&#322;o&#380;y&#263;"
  ]
  node [
    id 21
    label "nakaza&#263;"
  ]
  node [
    id 22
    label "wniosek"
  ]
  node [
    id 23
    label "pow&#243;dztwo"
  ]
  node [
    id 24
    label "cywilnie"
  ]
  node [
    id 25
    label "nieoficjalny"
  ]
  node [
    id 26
    label "cz&#322;owiek"
  ]
  node [
    id 27
    label "kierowa&#263;"
  ]
  node [
    id 28
    label "zwrot"
  ]
  node [
    id 29
    label "kierownictwo"
  ]
  node [
    id 30
    label "pryncypa&#322;"
  ]
  node [
    id 31
    label "obramienie"
  ]
  node [
    id 32
    label "forum"
  ]
  node [
    id 33
    label "serwis_internetowy"
  ]
  node [
    id 34
    label "wej&#347;cie"
  ]
  node [
    id 35
    label "Onet"
  ]
  node [
    id 36
    label "archiwolta"
  ]
  node [
    id 37
    label "Springer"
  ]
  node [
    id 38
    label "Polska"
  ]
  node [
    id 39
    label "Samuel"
  ]
  node [
    id 40
    label "Pereiry"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 24
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 26
  ]
  edge [
    source 9
    target 27
  ]
  edge [
    source 9
    target 28
  ]
  edge [
    source 9
    target 29
  ]
  edge [
    source 9
    target 30
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 31
  ]
  edge [
    source 10
    target 32
  ]
  edge [
    source 10
    target 33
  ]
  edge [
    source 10
    target 34
  ]
  edge [
    source 10
    target 35
  ]
  edge [
    source 10
    target 36
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 39
    target 40
  ]
]
