graph [
  maxDegree 77
  minDegree 1
  meanDegree 2.2465753424657535
  density 0.005140904673834676
  graphCliqueNumber 3
  node [
    id 0
    label "codziennie"
    origin "text"
  ]
  node [
    id 1
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 2
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 3
    label "dokonywa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "wybory"
    origin "text"
  ]
  node [
    id 5
    label "tychy"
    origin "text"
  ]
  node [
    id 6
    label "wp&#322;ywa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "&#380;ycie"
    origin "text"
  ]
  node [
    id 8
    label "cho&#263;by"
    origin "text"
  ]
  node [
    id 9
    label "wiedzie&#263;"
    origin "text"
  ]
  node [
    id 10
    label "jak"
    origin "text"
  ]
  node [
    id 11
    label "&#347;wiadomo&#347;&#263;"
    origin "text"
  ]
  node [
    id 12
    label "by&#322;a"
    origin "text"
  ]
  node [
    id 13
    label "niezno&#347;ny"
    origin "text"
  ]
  node [
    id 14
    label "wyb&#243;r"
    origin "text"
  ]
  node [
    id 15
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 16
    label "znaczenie"
    origin "text"
  ]
  node [
    id 17
    label "dla"
    origin "text"
  ]
  node [
    id 18
    label "los"
    origin "text"
  ]
  node [
    id 19
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 20
    label "inny"
    origin "text"
  ]
  node [
    id 21
    label "s&#322;owy"
    origin "text"
  ]
  node [
    id 22
    label "by&#263;"
    origin "text"
  ]
  node [
    id 23
    label "oboj&#281;tny"
    origin "text"
  ]
  node [
    id 24
    label "jaki"
    origin "text"
  ]
  node [
    id 25
    label "dokona&#263;"
    origin "text"
  ]
  node [
    id 26
    label "zdecydowa&#263;"
    origin "text"
  ]
  node [
    id 27
    label "si&#281;"
    origin "text"
  ]
  node [
    id 28
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 29
    label "wtedy"
    origin "text"
  ]
  node [
    id 30
    label "gdy"
    origin "text"
  ]
  node [
    id 31
    label "wszyscy"
    origin "text"
  ]
  node [
    id 32
    label "milcza"
    origin "text"
  ]
  node [
    id 33
    label "dzia&#322;a&#263;"
    origin "text"
  ]
  node [
    id 34
    label "opu&#347;ci&#263;"
    origin "text"
  ]
  node [
    id 35
    label "r&#281;ka"
    origin "text"
  ]
  node [
    id 36
    label "lub"
    origin "text"
  ]
  node [
    id 37
    label "pochowa&#263;"
    origin "text"
  ]
  node [
    id 38
    label "k&#261;t"
    origin "text"
  ]
  node [
    id 39
    label "pisa&#263;"
    origin "text"
  ]
  node [
    id 40
    label "prawda"
    origin "text"
  ]
  node [
    id 41
    label "robi&#263;"
    origin "text"
  ]
  node [
    id 42
    label "poczucie"
    origin "text"
  ]
  node [
    id 43
    label "odpowiedzialno&#347;&#263;"
    origin "text"
  ]
  node [
    id 44
    label "wasz"
    origin "text"
  ]
  node [
    id 45
    label "daily"
  ]
  node [
    id 46
    label "cz&#281;sto"
  ]
  node [
    id 47
    label "codzienny"
  ]
  node [
    id 48
    label "prozaicznie"
  ]
  node [
    id 49
    label "stale"
  ]
  node [
    id 50
    label "regularnie"
  ]
  node [
    id 51
    label "pospolicie"
  ]
  node [
    id 52
    label "jaki&#347;"
  ]
  node [
    id 53
    label "asymilowa&#263;"
  ]
  node [
    id 54
    label "wapniak"
  ]
  node [
    id 55
    label "dwun&#243;g"
  ]
  node [
    id 56
    label "polifag"
  ]
  node [
    id 57
    label "wz&#243;r"
  ]
  node [
    id 58
    label "profanum"
  ]
  node [
    id 59
    label "hominid"
  ]
  node [
    id 60
    label "homo_sapiens"
  ]
  node [
    id 61
    label "nasada"
  ]
  node [
    id 62
    label "podw&#322;adny"
  ]
  node [
    id 63
    label "ludzko&#347;&#263;"
  ]
  node [
    id 64
    label "os&#322;abianie"
  ]
  node [
    id 65
    label "mikrokosmos"
  ]
  node [
    id 66
    label "portrecista"
  ]
  node [
    id 67
    label "duch"
  ]
  node [
    id 68
    label "g&#322;owa"
  ]
  node [
    id 69
    label "oddzia&#322;ywanie"
  ]
  node [
    id 70
    label "asymilowanie"
  ]
  node [
    id 71
    label "osoba"
  ]
  node [
    id 72
    label "os&#322;abia&#263;"
  ]
  node [
    id 73
    label "figura"
  ]
  node [
    id 74
    label "Adam"
  ]
  node [
    id 75
    label "senior"
  ]
  node [
    id 76
    label "antropochoria"
  ]
  node [
    id 77
    label "posta&#263;"
  ]
  node [
    id 78
    label "determine"
  ]
  node [
    id 79
    label "przestawa&#263;"
  ]
  node [
    id 80
    label "make"
  ]
  node [
    id 81
    label "skrutator"
  ]
  node [
    id 82
    label "g&#322;osowanie"
  ]
  node [
    id 83
    label "zasila&#263;"
  ]
  node [
    id 84
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 85
    label "work"
  ]
  node [
    id 86
    label "przyp&#322;ywa&#263;"
  ]
  node [
    id 87
    label "kapita&#322;"
  ]
  node [
    id 88
    label "dochodzi&#263;"
  ]
  node [
    id 89
    label "pour"
  ]
  node [
    id 90
    label "ciek_wodny"
  ]
  node [
    id 91
    label "energy"
  ]
  node [
    id 92
    label "czas"
  ]
  node [
    id 93
    label "bycie"
  ]
  node [
    id 94
    label "zegar_biologiczny"
  ]
  node [
    id 95
    label "okres_noworodkowy"
  ]
  node [
    id 96
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 97
    label "entity"
  ]
  node [
    id 98
    label "prze&#380;ywanie"
  ]
  node [
    id 99
    label "prze&#380;ycie"
  ]
  node [
    id 100
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 101
    label "wiek_matuzalemowy"
  ]
  node [
    id 102
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 103
    label "dzieci&#324;stwo"
  ]
  node [
    id 104
    label "power"
  ]
  node [
    id 105
    label "szwung"
  ]
  node [
    id 106
    label "menopauza"
  ]
  node [
    id 107
    label "umarcie"
  ]
  node [
    id 108
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 109
    label "life"
  ]
  node [
    id 110
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 111
    label "&#380;ywy"
  ]
  node [
    id 112
    label "rozw&#243;j"
  ]
  node [
    id 113
    label "po&#322;&#243;g"
  ]
  node [
    id 114
    label "byt"
  ]
  node [
    id 115
    label "przebywanie"
  ]
  node [
    id 116
    label "subsistence"
  ]
  node [
    id 117
    label "koleje_losu"
  ]
  node [
    id 118
    label "raj_utracony"
  ]
  node [
    id 119
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 120
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 121
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 122
    label "andropauza"
  ]
  node [
    id 123
    label "warunki"
  ]
  node [
    id 124
    label "do&#380;ywanie"
  ]
  node [
    id 125
    label "niemowl&#281;ctwo"
  ]
  node [
    id 126
    label "umieranie"
  ]
  node [
    id 127
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 128
    label "staro&#347;&#263;"
  ]
  node [
    id 129
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 130
    label "&#347;mier&#263;"
  ]
  node [
    id 131
    label "cognizance"
  ]
  node [
    id 132
    label "byd&#322;o"
  ]
  node [
    id 133
    label "zobo"
  ]
  node [
    id 134
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 135
    label "yakalo"
  ]
  node [
    id 136
    label "dzo"
  ]
  node [
    id 137
    label "stan"
  ]
  node [
    id 138
    label "psychika"
  ]
  node [
    id 139
    label "psychoanaliza"
  ]
  node [
    id 140
    label "wiedza"
  ]
  node [
    id 141
    label "ekstraspekcja"
  ]
  node [
    id 142
    label "zemdle&#263;"
  ]
  node [
    id 143
    label "conscience"
  ]
  node [
    id 144
    label "Freud"
  ]
  node [
    id 145
    label "feeling"
  ]
  node [
    id 146
    label "partnerka"
  ]
  node [
    id 147
    label "dokuczliwy"
  ]
  node [
    id 148
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 149
    label "niegrzeczny"
  ]
  node [
    id 150
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 151
    label "niezno&#347;nie"
  ]
  node [
    id 152
    label "uci&#261;&#380;liwy"
  ]
  node [
    id 153
    label "czynno&#347;&#263;"
  ]
  node [
    id 154
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 155
    label "decyzja"
  ]
  node [
    id 156
    label "ewentualno&#347;&#263;"
  ]
  node [
    id 157
    label "pick"
  ]
  node [
    id 158
    label "czyj&#347;"
  ]
  node [
    id 159
    label "m&#261;&#380;"
  ]
  node [
    id 160
    label "gravity"
  ]
  node [
    id 161
    label "okre&#347;lanie"
  ]
  node [
    id 162
    label "liczenie"
  ]
  node [
    id 163
    label "odgrywanie_roli"
  ]
  node [
    id 164
    label "wskazywanie"
  ]
  node [
    id 165
    label "weight"
  ]
  node [
    id 166
    label "command"
  ]
  node [
    id 167
    label "istota"
  ]
  node [
    id 168
    label "cecha"
  ]
  node [
    id 169
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 170
    label "informacja"
  ]
  node [
    id 171
    label "odk&#322;adanie"
  ]
  node [
    id 172
    label "wyra&#380;enie"
  ]
  node [
    id 173
    label "wyraz"
  ]
  node [
    id 174
    label "assay"
  ]
  node [
    id 175
    label "condition"
  ]
  node [
    id 176
    label "kto&#347;"
  ]
  node [
    id 177
    label "stawianie"
  ]
  node [
    id 178
    label "si&#322;a"
  ]
  node [
    id 179
    label "przymus"
  ]
  node [
    id 180
    label "rzuci&#263;"
  ]
  node [
    id 181
    label "hazard"
  ]
  node [
    id 182
    label "destiny"
  ]
  node [
    id 183
    label "bilet"
  ]
  node [
    id 184
    label "przebieg_&#380;ycia"
  ]
  node [
    id 185
    label "rzucenie"
  ]
  node [
    id 186
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 187
    label "obszar"
  ]
  node [
    id 188
    label "obiekt_naturalny"
  ]
  node [
    id 189
    label "przedmiot"
  ]
  node [
    id 190
    label "Stary_&#346;wiat"
  ]
  node [
    id 191
    label "grupa"
  ]
  node [
    id 192
    label "stw&#243;r"
  ]
  node [
    id 193
    label "biosfera"
  ]
  node [
    id 194
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 195
    label "rzecz"
  ]
  node [
    id 196
    label "magnetosfera"
  ]
  node [
    id 197
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 198
    label "environment"
  ]
  node [
    id 199
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 200
    label "geosfera"
  ]
  node [
    id 201
    label "Nowy_&#346;wiat"
  ]
  node [
    id 202
    label "planeta"
  ]
  node [
    id 203
    label "przejmowa&#263;"
  ]
  node [
    id 204
    label "litosfera"
  ]
  node [
    id 205
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 206
    label "makrokosmos"
  ]
  node [
    id 207
    label "barysfera"
  ]
  node [
    id 208
    label "biota"
  ]
  node [
    id 209
    label "p&#243;&#322;noc"
  ]
  node [
    id 210
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 211
    label "fauna"
  ]
  node [
    id 212
    label "wszechstworzenie"
  ]
  node [
    id 213
    label "geotermia"
  ]
  node [
    id 214
    label "biegun"
  ]
  node [
    id 215
    label "ekosystem"
  ]
  node [
    id 216
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 217
    label "teren"
  ]
  node [
    id 218
    label "zjawisko"
  ]
  node [
    id 219
    label "p&#243;&#322;kula"
  ]
  node [
    id 220
    label "atmosfera"
  ]
  node [
    id 221
    label "class"
  ]
  node [
    id 222
    label "po&#322;udnie"
  ]
  node [
    id 223
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 224
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 225
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 226
    label "przejmowanie"
  ]
  node [
    id 227
    label "przestrze&#324;"
  ]
  node [
    id 228
    label "asymilowanie_si&#281;"
  ]
  node [
    id 229
    label "przej&#261;&#263;"
  ]
  node [
    id 230
    label "ekosfera"
  ]
  node [
    id 231
    label "przyroda"
  ]
  node [
    id 232
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 233
    label "ciemna_materia"
  ]
  node [
    id 234
    label "geoida"
  ]
  node [
    id 235
    label "Wsch&#243;d"
  ]
  node [
    id 236
    label "populace"
  ]
  node [
    id 237
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 238
    label "huczek"
  ]
  node [
    id 239
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 240
    label "Ziemia"
  ]
  node [
    id 241
    label "universe"
  ]
  node [
    id 242
    label "ozonosfera"
  ]
  node [
    id 243
    label "rze&#378;ba"
  ]
  node [
    id 244
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 245
    label "zagranica"
  ]
  node [
    id 246
    label "hydrosfera"
  ]
  node [
    id 247
    label "woda"
  ]
  node [
    id 248
    label "kuchnia"
  ]
  node [
    id 249
    label "przej&#281;cie"
  ]
  node [
    id 250
    label "czarna_dziura"
  ]
  node [
    id 251
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 252
    label "morze"
  ]
  node [
    id 253
    label "kolejny"
  ]
  node [
    id 254
    label "inaczej"
  ]
  node [
    id 255
    label "r&#243;&#380;ny"
  ]
  node [
    id 256
    label "inszy"
  ]
  node [
    id 257
    label "osobno"
  ]
  node [
    id 258
    label "si&#281;ga&#263;"
  ]
  node [
    id 259
    label "trwa&#263;"
  ]
  node [
    id 260
    label "obecno&#347;&#263;"
  ]
  node [
    id 261
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 262
    label "stand"
  ]
  node [
    id 263
    label "mie&#263;_miejsce"
  ]
  node [
    id 264
    label "uczestniczy&#263;"
  ]
  node [
    id 265
    label "chodzi&#263;"
  ]
  node [
    id 266
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 267
    label "equal"
  ]
  node [
    id 268
    label "zoboj&#281;tnienie"
  ]
  node [
    id 269
    label "oboj&#281;tnie"
  ]
  node [
    id 270
    label "nieszkodliwy"
  ]
  node [
    id 271
    label "zneutralizowanie_si&#281;"
  ]
  node [
    id 272
    label "neutralny"
  ]
  node [
    id 273
    label "&#347;ni&#281;ty"
  ]
  node [
    id 274
    label "niewa&#380;ny"
  ]
  node [
    id 275
    label "bierny"
  ]
  node [
    id 276
    label "neutralizowanie"
  ]
  node [
    id 277
    label "zneutralizowanie"
  ]
  node [
    id 278
    label "neutralizowanie_si&#281;"
  ]
  node [
    id 279
    label "przesta&#263;"
  ]
  node [
    id 280
    label "zrobi&#263;"
  ]
  node [
    id 281
    label "cause"
  ]
  node [
    id 282
    label "communicate"
  ]
  node [
    id 283
    label "sta&#263;_si&#281;"
  ]
  node [
    id 284
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 285
    label "podj&#261;&#263;"
  ]
  node [
    id 286
    label "decide"
  ]
  node [
    id 287
    label "remark"
  ]
  node [
    id 288
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 289
    label "u&#380;ywa&#263;"
  ]
  node [
    id 290
    label "okre&#347;la&#263;"
  ]
  node [
    id 291
    label "j&#281;zyk"
  ]
  node [
    id 292
    label "say"
  ]
  node [
    id 293
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 294
    label "formu&#322;owa&#263;"
  ]
  node [
    id 295
    label "talk"
  ]
  node [
    id 296
    label "powiada&#263;"
  ]
  node [
    id 297
    label "informowa&#263;"
  ]
  node [
    id 298
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 299
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 300
    label "wydobywa&#263;"
  ]
  node [
    id 301
    label "express"
  ]
  node [
    id 302
    label "chew_the_fat"
  ]
  node [
    id 303
    label "dysfonia"
  ]
  node [
    id 304
    label "umie&#263;"
  ]
  node [
    id 305
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 306
    label "tell"
  ]
  node [
    id 307
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 308
    label "wyra&#380;a&#263;"
  ]
  node [
    id 309
    label "gaworzy&#263;"
  ]
  node [
    id 310
    label "rozmawia&#263;"
  ]
  node [
    id 311
    label "dziama&#263;"
  ]
  node [
    id 312
    label "prawi&#263;"
  ]
  node [
    id 313
    label "kiedy&#347;"
  ]
  node [
    id 314
    label "reakcja_chemiczna"
  ]
  node [
    id 315
    label "function"
  ]
  node [
    id 316
    label "commit"
  ]
  node [
    id 317
    label "bangla&#263;"
  ]
  node [
    id 318
    label "tryb"
  ]
  node [
    id 319
    label "powodowa&#263;"
  ]
  node [
    id 320
    label "istnie&#263;"
  ]
  node [
    id 321
    label "wyforowa&#263;_si&#281;"
  ]
  node [
    id 322
    label "omin&#261;&#263;"
  ]
  node [
    id 323
    label "humiliate"
  ]
  node [
    id 324
    label "pozostawi&#263;"
  ]
  node [
    id 325
    label "potani&#263;"
  ]
  node [
    id 326
    label "obni&#380;y&#263;"
  ]
  node [
    id 327
    label "evacuate"
  ]
  node [
    id 328
    label "authorize"
  ]
  node [
    id 329
    label "leave"
  ]
  node [
    id 330
    label "straci&#263;"
  ]
  node [
    id 331
    label "zostawi&#263;"
  ]
  node [
    id 332
    label "drop"
  ]
  node [
    id 333
    label "tekst"
  ]
  node [
    id 334
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 335
    label "krzy&#380;"
  ]
  node [
    id 336
    label "paw"
  ]
  node [
    id 337
    label "rami&#281;"
  ]
  node [
    id 338
    label "gestykulowanie"
  ]
  node [
    id 339
    label "pi&#322;ka_no&#380;na"
  ]
  node [
    id 340
    label "pracownik"
  ]
  node [
    id 341
    label "ko&#324;czyna_g&#243;rna"
  ]
  node [
    id 342
    label "bramkarz"
  ]
  node [
    id 343
    label "&#347;r&#243;dr&#281;cze"
  ]
  node [
    id 344
    label "handwriting"
  ]
  node [
    id 345
    label "hasta"
  ]
  node [
    id 346
    label "pi&#322;ka"
  ]
  node [
    id 347
    label "&#322;okie&#263;"
  ]
  node [
    id 348
    label "spos&#243;b"
  ]
  node [
    id 349
    label "zagrywka"
  ]
  node [
    id 350
    label "obietnica"
  ]
  node [
    id 351
    label "przedrami&#281;"
  ]
  node [
    id 352
    label "chwyta&#263;"
  ]
  node [
    id 353
    label "r&#261;czyna"
  ]
  node [
    id 354
    label "wykroczenie"
  ]
  node [
    id 355
    label "kroki"
  ]
  node [
    id 356
    label "palec"
  ]
  node [
    id 357
    label "&#380;&#243;&#322;ta_kartka"
  ]
  node [
    id 358
    label "graba"
  ]
  node [
    id 359
    label "hand"
  ]
  node [
    id 360
    label "nadgarstek"
  ]
  node [
    id 361
    label "pomocnik"
  ]
  node [
    id 362
    label "k&#322;&#261;b"
  ]
  node [
    id 363
    label "hazena"
  ]
  node [
    id 364
    label "gestykulowa&#263;"
  ]
  node [
    id 365
    label "cmoknonsens"
  ]
  node [
    id 366
    label "d&#322;o&#324;"
  ]
  node [
    id 367
    label "chwytanie"
  ]
  node [
    id 368
    label "czerwona_kartka"
  ]
  node [
    id 369
    label "zw&#322;oki"
  ]
  node [
    id 370
    label "bury"
  ]
  node [
    id 371
    label "znie&#347;&#263;"
  ]
  node [
    id 372
    label "hide"
  ]
  node [
    id 373
    label "powk&#322;ada&#263;"
  ]
  node [
    id 374
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 375
    label "poumieszcza&#263;"
  ]
  node [
    id 376
    label "miejsce"
  ]
  node [
    id 377
    label "p&#322;aszczyzna"
  ]
  node [
    id 378
    label "garderoba"
  ]
  node [
    id 379
    label "rami&#281;_k&#261;ta"
  ]
  node [
    id 380
    label "siedziba"
  ]
  node [
    id 381
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 382
    label "ubocze"
  ]
  node [
    id 383
    label "obiekt_matematyczny"
  ]
  node [
    id 384
    label "dom"
  ]
  node [
    id 385
    label "ozdabia&#263;"
  ]
  node [
    id 386
    label "dysgrafia"
  ]
  node [
    id 387
    label "prasa"
  ]
  node [
    id 388
    label "spell"
  ]
  node [
    id 389
    label "skryba"
  ]
  node [
    id 390
    label "donosi&#263;"
  ]
  node [
    id 391
    label "code"
  ]
  node [
    id 392
    label "dysortografia"
  ]
  node [
    id 393
    label "read"
  ]
  node [
    id 394
    label "tworzy&#263;"
  ]
  node [
    id 395
    label "styl"
  ]
  node [
    id 396
    label "stawia&#263;"
  ]
  node [
    id 397
    label "s&#261;d"
  ]
  node [
    id 398
    label "truth"
  ]
  node [
    id 399
    label "nieprawdziwy"
  ]
  node [
    id 400
    label "za&#322;o&#380;enie"
  ]
  node [
    id 401
    label "prawdziwy"
  ]
  node [
    id 402
    label "realia"
  ]
  node [
    id 403
    label "tentegowa&#263;"
  ]
  node [
    id 404
    label "urz&#261;dza&#263;"
  ]
  node [
    id 405
    label "give"
  ]
  node [
    id 406
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 407
    label "czyni&#263;"
  ]
  node [
    id 408
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 409
    label "post&#281;powa&#263;"
  ]
  node [
    id 410
    label "wydala&#263;"
  ]
  node [
    id 411
    label "oszukiwa&#263;"
  ]
  node [
    id 412
    label "organizowa&#263;"
  ]
  node [
    id 413
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 414
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 415
    label "przerabia&#263;"
  ]
  node [
    id 416
    label "stylizowa&#263;"
  ]
  node [
    id 417
    label "falowa&#263;"
  ]
  node [
    id 418
    label "act"
  ]
  node [
    id 419
    label "peddle"
  ]
  node [
    id 420
    label "ukazywa&#263;"
  ]
  node [
    id 421
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 422
    label "praca"
  ]
  node [
    id 423
    label "zareagowanie"
  ]
  node [
    id 424
    label "opanowanie"
  ]
  node [
    id 425
    label "doznanie"
  ]
  node [
    id 426
    label "dowiedzenie_si&#281;"
  ]
  node [
    id 427
    label "intuition"
  ]
  node [
    id 428
    label "os&#322;upienie"
  ]
  node [
    id 429
    label "domy&#347;lenie_si&#281;"
  ]
  node [
    id 430
    label "smell"
  ]
  node [
    id 431
    label "zdarzenie_si&#281;"
  ]
  node [
    id 432
    label "wstyd"
  ]
  node [
    id 433
    label "konsekwencja"
  ]
  node [
    id 434
    label "guilt"
  ]
  node [
    id 435
    label "liability"
  ]
  node [
    id 436
    label "obarczy&#263;"
  ]
  node [
    id 437
    label "obowi&#261;zek"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 1
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 4
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 7
    target 113
  ]
  edge [
    source 7
    target 114
  ]
  edge [
    source 7
    target 115
  ]
  edge [
    source 7
    target 116
  ]
  edge [
    source 7
    target 117
  ]
  edge [
    source 7
    target 118
  ]
  edge [
    source 7
    target 119
  ]
  edge [
    source 7
    target 120
  ]
  edge [
    source 7
    target 121
  ]
  edge [
    source 7
    target 122
  ]
  edge [
    source 7
    target 123
  ]
  edge [
    source 7
    target 124
  ]
  edge [
    source 7
    target 125
  ]
  edge [
    source 7
    target 126
  ]
  edge [
    source 7
    target 127
  ]
  edge [
    source 7
    target 128
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 7
    target 130
  ]
  edge [
    source 7
    target 18
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 131
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 132
  ]
  edge [
    source 10
    target 133
  ]
  edge [
    source 10
    target 134
  ]
  edge [
    source 10
    target 135
  ]
  edge [
    source 10
    target 136
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 137
  ]
  edge [
    source 11
    target 138
  ]
  edge [
    source 11
    target 139
  ]
  edge [
    source 11
    target 140
  ]
  edge [
    source 11
    target 141
  ]
  edge [
    source 11
    target 142
  ]
  edge [
    source 11
    target 143
  ]
  edge [
    source 11
    target 144
  ]
  edge [
    source 11
    target 145
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 146
  ]
  edge [
    source 13
    target 147
  ]
  edge [
    source 13
    target 148
  ]
  edge [
    source 13
    target 149
  ]
  edge [
    source 13
    target 150
  ]
  edge [
    source 13
    target 151
  ]
  edge [
    source 13
    target 152
  ]
  edge [
    source 14
    target 24
  ]
  edge [
    source 14
    target 44
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 153
  ]
  edge [
    source 14
    target 154
  ]
  edge [
    source 14
    target 155
  ]
  edge [
    source 14
    target 156
  ]
  edge [
    source 14
    target 157
  ]
  edge [
    source 14
    target 31
  ]
  edge [
    source 14
    target 19
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 38
  ]
  edge [
    source 15
    target 158
  ]
  edge [
    source 15
    target 159
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 160
  ]
  edge [
    source 16
    target 161
  ]
  edge [
    source 16
    target 162
  ]
  edge [
    source 16
    target 163
  ]
  edge [
    source 16
    target 164
  ]
  edge [
    source 16
    target 93
  ]
  edge [
    source 16
    target 165
  ]
  edge [
    source 16
    target 166
  ]
  edge [
    source 16
    target 167
  ]
  edge [
    source 16
    target 168
  ]
  edge [
    source 16
    target 169
  ]
  edge [
    source 16
    target 170
  ]
  edge [
    source 16
    target 171
  ]
  edge [
    source 16
    target 172
  ]
  edge [
    source 16
    target 173
  ]
  edge [
    source 16
    target 174
  ]
  edge [
    source 16
    target 175
  ]
  edge [
    source 16
    target 176
  ]
  edge [
    source 16
    target 177
  ]
  edge [
    source 16
    target 32
  ]
  edge [
    source 16
    target 43
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 23
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 178
  ]
  edge [
    source 18
    target 179
  ]
  edge [
    source 18
    target 180
  ]
  edge [
    source 18
    target 181
  ]
  edge [
    source 18
    target 182
  ]
  edge [
    source 18
    target 119
  ]
  edge [
    source 18
    target 183
  ]
  edge [
    source 18
    target 184
  ]
  edge [
    source 18
    target 185
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 26
  ]
  edge [
    source 19
    target 28
  ]
  edge [
    source 19
    target 186
  ]
  edge [
    source 19
    target 187
  ]
  edge [
    source 19
    target 188
  ]
  edge [
    source 19
    target 189
  ]
  edge [
    source 19
    target 190
  ]
  edge [
    source 19
    target 191
  ]
  edge [
    source 19
    target 192
  ]
  edge [
    source 19
    target 193
  ]
  edge [
    source 19
    target 194
  ]
  edge [
    source 19
    target 195
  ]
  edge [
    source 19
    target 196
  ]
  edge [
    source 19
    target 197
  ]
  edge [
    source 19
    target 198
  ]
  edge [
    source 19
    target 199
  ]
  edge [
    source 19
    target 200
  ]
  edge [
    source 19
    target 201
  ]
  edge [
    source 19
    target 202
  ]
  edge [
    source 19
    target 203
  ]
  edge [
    source 19
    target 204
  ]
  edge [
    source 19
    target 205
  ]
  edge [
    source 19
    target 206
  ]
  edge [
    source 19
    target 207
  ]
  edge [
    source 19
    target 208
  ]
  edge [
    source 19
    target 209
  ]
  edge [
    source 19
    target 210
  ]
  edge [
    source 19
    target 211
  ]
  edge [
    source 19
    target 212
  ]
  edge [
    source 19
    target 213
  ]
  edge [
    source 19
    target 214
  ]
  edge [
    source 19
    target 154
  ]
  edge [
    source 19
    target 215
  ]
  edge [
    source 19
    target 216
  ]
  edge [
    source 19
    target 217
  ]
  edge [
    source 19
    target 218
  ]
  edge [
    source 19
    target 219
  ]
  edge [
    source 19
    target 220
  ]
  edge [
    source 19
    target 65
  ]
  edge [
    source 19
    target 221
  ]
  edge [
    source 19
    target 222
  ]
  edge [
    source 19
    target 223
  ]
  edge [
    source 19
    target 224
  ]
  edge [
    source 19
    target 225
  ]
  edge [
    source 19
    target 226
  ]
  edge [
    source 19
    target 227
  ]
  edge [
    source 19
    target 228
  ]
  edge [
    source 19
    target 229
  ]
  edge [
    source 19
    target 230
  ]
  edge [
    source 19
    target 231
  ]
  edge [
    source 19
    target 232
  ]
  edge [
    source 19
    target 233
  ]
  edge [
    source 19
    target 234
  ]
  edge [
    source 19
    target 235
  ]
  edge [
    source 19
    target 236
  ]
  edge [
    source 19
    target 237
  ]
  edge [
    source 19
    target 238
  ]
  edge [
    source 19
    target 239
  ]
  edge [
    source 19
    target 240
  ]
  edge [
    source 19
    target 241
  ]
  edge [
    source 19
    target 242
  ]
  edge [
    source 19
    target 243
  ]
  edge [
    source 19
    target 244
  ]
  edge [
    source 19
    target 245
  ]
  edge [
    source 19
    target 246
  ]
  edge [
    source 19
    target 247
  ]
  edge [
    source 19
    target 248
  ]
  edge [
    source 19
    target 249
  ]
  edge [
    source 19
    target 250
  ]
  edge [
    source 19
    target 251
  ]
  edge [
    source 19
    target 252
  ]
  edge [
    source 19
    target 39
  ]
  edge [
    source 19
    target 40
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 253
  ]
  edge [
    source 20
    target 254
  ]
  edge [
    source 20
    target 255
  ]
  edge [
    source 20
    target 256
  ]
  edge [
    source 20
    target 257
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 25
  ]
  edge [
    source 22
    target 258
  ]
  edge [
    source 22
    target 259
  ]
  edge [
    source 22
    target 260
  ]
  edge [
    source 22
    target 137
  ]
  edge [
    source 22
    target 261
  ]
  edge [
    source 22
    target 262
  ]
  edge [
    source 22
    target 263
  ]
  edge [
    source 22
    target 264
  ]
  edge [
    source 22
    target 265
  ]
  edge [
    source 22
    target 266
  ]
  edge [
    source 22
    target 267
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 268
  ]
  edge [
    source 23
    target 269
  ]
  edge [
    source 23
    target 270
  ]
  edge [
    source 23
    target 271
  ]
  edge [
    source 23
    target 272
  ]
  edge [
    source 23
    target 273
  ]
  edge [
    source 23
    target 274
  ]
  edge [
    source 23
    target 275
  ]
  edge [
    source 23
    target 276
  ]
  edge [
    source 23
    target 277
  ]
  edge [
    source 23
    target 278
  ]
  edge [
    source 25
    target 279
  ]
  edge [
    source 25
    target 280
  ]
  edge [
    source 25
    target 281
  ]
  edge [
    source 25
    target 282
  ]
  edge [
    source 25
    target 37
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 32
  ]
  edge [
    source 26
    target 283
  ]
  edge [
    source 26
    target 284
  ]
  edge [
    source 26
    target 285
  ]
  edge [
    source 26
    target 78
  ]
  edge [
    source 26
    target 286
  ]
  edge [
    source 26
    target 280
  ]
  edge [
    source 26
    target 33
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 33
  ]
  edge [
    source 27
    target 37
  ]
  edge [
    source 27
    target 38
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 39
  ]
  edge [
    source 28
    target 287
  ]
  edge [
    source 28
    target 288
  ]
  edge [
    source 28
    target 289
  ]
  edge [
    source 28
    target 290
  ]
  edge [
    source 28
    target 291
  ]
  edge [
    source 28
    target 292
  ]
  edge [
    source 28
    target 293
  ]
  edge [
    source 28
    target 294
  ]
  edge [
    source 28
    target 295
  ]
  edge [
    source 28
    target 296
  ]
  edge [
    source 28
    target 297
  ]
  edge [
    source 28
    target 298
  ]
  edge [
    source 28
    target 299
  ]
  edge [
    source 28
    target 300
  ]
  edge [
    source 28
    target 301
  ]
  edge [
    source 28
    target 302
  ]
  edge [
    source 28
    target 303
  ]
  edge [
    source 28
    target 304
  ]
  edge [
    source 28
    target 305
  ]
  edge [
    source 28
    target 306
  ]
  edge [
    source 28
    target 307
  ]
  edge [
    source 28
    target 308
  ]
  edge [
    source 28
    target 309
  ]
  edge [
    source 28
    target 310
  ]
  edge [
    source 28
    target 311
  ]
  edge [
    source 28
    target 312
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 33
  ]
  edge [
    source 29
    target 313
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 34
  ]
  edge [
    source 33
    target 263
  ]
  edge [
    source 33
    target 85
  ]
  edge [
    source 33
    target 314
  ]
  edge [
    source 33
    target 315
  ]
  edge [
    source 33
    target 316
  ]
  edge [
    source 33
    target 317
  ]
  edge [
    source 33
    target 41
  ]
  edge [
    source 33
    target 78
  ]
  edge [
    source 33
    target 318
  ]
  edge [
    source 33
    target 319
  ]
  edge [
    source 33
    target 311
  ]
  edge [
    source 33
    target 320
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 321
  ]
  edge [
    source 34
    target 322
  ]
  edge [
    source 34
    target 323
  ]
  edge [
    source 34
    target 324
  ]
  edge [
    source 34
    target 325
  ]
  edge [
    source 34
    target 326
  ]
  edge [
    source 34
    target 327
  ]
  edge [
    source 34
    target 328
  ]
  edge [
    source 34
    target 329
  ]
  edge [
    source 34
    target 279
  ]
  edge [
    source 34
    target 330
  ]
  edge [
    source 34
    target 331
  ]
  edge [
    source 34
    target 332
  ]
  edge [
    source 34
    target 333
  ]
  edge [
    source 34
    target 334
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 335
  ]
  edge [
    source 35
    target 336
  ]
  edge [
    source 35
    target 337
  ]
  edge [
    source 35
    target 338
  ]
  edge [
    source 35
    target 339
  ]
  edge [
    source 35
    target 340
  ]
  edge [
    source 35
    target 341
  ]
  edge [
    source 35
    target 342
  ]
  edge [
    source 35
    target 343
  ]
  edge [
    source 35
    target 344
  ]
  edge [
    source 35
    target 345
  ]
  edge [
    source 35
    target 346
  ]
  edge [
    source 35
    target 347
  ]
  edge [
    source 35
    target 348
  ]
  edge [
    source 35
    target 349
  ]
  edge [
    source 35
    target 350
  ]
  edge [
    source 35
    target 351
  ]
  edge [
    source 35
    target 352
  ]
  edge [
    source 35
    target 353
  ]
  edge [
    source 35
    target 168
  ]
  edge [
    source 35
    target 354
  ]
  edge [
    source 35
    target 355
  ]
  edge [
    source 35
    target 356
  ]
  edge [
    source 35
    target 357
  ]
  edge [
    source 35
    target 358
  ]
  edge [
    source 35
    target 359
  ]
  edge [
    source 35
    target 360
  ]
  edge [
    source 35
    target 361
  ]
  edge [
    source 35
    target 362
  ]
  edge [
    source 35
    target 363
  ]
  edge [
    source 35
    target 364
  ]
  edge [
    source 35
    target 365
  ]
  edge [
    source 35
    target 366
  ]
  edge [
    source 35
    target 367
  ]
  edge [
    source 35
    target 368
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 37
    target 369
  ]
  edge [
    source 37
    target 370
  ]
  edge [
    source 37
    target 371
  ]
  edge [
    source 37
    target 372
  ]
  edge [
    source 37
    target 330
  ]
  edge [
    source 37
    target 373
  ]
  edge [
    source 37
    target 374
  ]
  edge [
    source 37
    target 375
  ]
  edge [
    source 38
    target 376
  ]
  edge [
    source 38
    target 377
  ]
  edge [
    source 38
    target 378
  ]
  edge [
    source 38
    target 379
  ]
  edge [
    source 38
    target 380
  ]
  edge [
    source 38
    target 381
  ]
  edge [
    source 38
    target 382
  ]
  edge [
    source 38
    target 383
  ]
  edge [
    source 38
    target 384
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 385
  ]
  edge [
    source 39
    target 386
  ]
  edge [
    source 39
    target 387
  ]
  edge [
    source 39
    target 388
  ]
  edge [
    source 39
    target 389
  ]
  edge [
    source 39
    target 390
  ]
  edge [
    source 39
    target 391
  ]
  edge [
    source 39
    target 333
  ]
  edge [
    source 39
    target 392
  ]
  edge [
    source 39
    target 393
  ]
  edge [
    source 39
    target 394
  ]
  edge [
    source 39
    target 294
  ]
  edge [
    source 39
    target 395
  ]
  edge [
    source 39
    target 396
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 397
  ]
  edge [
    source 40
    target 398
  ]
  edge [
    source 40
    target 399
  ]
  edge [
    source 40
    target 400
  ]
  edge [
    source 40
    target 401
  ]
  edge [
    source 40
    target 402
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 403
  ]
  edge [
    source 41
    target 404
  ]
  edge [
    source 41
    target 405
  ]
  edge [
    source 41
    target 406
  ]
  edge [
    source 41
    target 407
  ]
  edge [
    source 41
    target 408
  ]
  edge [
    source 41
    target 409
  ]
  edge [
    source 41
    target 410
  ]
  edge [
    source 41
    target 411
  ]
  edge [
    source 41
    target 412
  ]
  edge [
    source 41
    target 413
  ]
  edge [
    source 41
    target 414
  ]
  edge [
    source 41
    target 85
  ]
  edge [
    source 41
    target 415
  ]
  edge [
    source 41
    target 416
  ]
  edge [
    source 41
    target 417
  ]
  edge [
    source 41
    target 418
  ]
  edge [
    source 41
    target 419
  ]
  edge [
    source 41
    target 420
  ]
  edge [
    source 41
    target 421
  ]
  edge [
    source 41
    target 422
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 423
  ]
  edge [
    source 42
    target 424
  ]
  edge [
    source 42
    target 425
  ]
  edge [
    source 42
    target 426
  ]
  edge [
    source 42
    target 427
  ]
  edge [
    source 42
    target 140
  ]
  edge [
    source 42
    target 141
  ]
  edge [
    source 42
    target 428
  ]
  edge [
    source 42
    target 429
  ]
  edge [
    source 42
    target 430
  ]
  edge [
    source 42
    target 431
  ]
  edge [
    source 42
    target 145
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 432
  ]
  edge [
    source 43
    target 433
  ]
  edge [
    source 43
    target 168
  ]
  edge [
    source 43
    target 434
  ]
  edge [
    source 43
    target 435
  ]
  edge [
    source 43
    target 436
  ]
  edge [
    source 43
    target 437
  ]
  edge [
    source 44
    target 158
  ]
]
