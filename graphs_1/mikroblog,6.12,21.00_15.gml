graph [
  maxDegree 13
  minDegree 1
  meanDegree 1.9259259259259258
  density 0.07407407407407407
  graphCliqueNumber 2
  node [
    id 0
    label "ma&#322;a"
    origin "text"
  ]
  node [
    id 1
    label "miejscowo&#347;&#263;"
    origin "text"
  ]
  node [
    id 2
    label "jeden"
    origin "text"
  ]
  node [
    id 3
    label "apteka"
    origin "text"
  ]
  node [
    id 4
    label "dziewczynka"
  ]
  node [
    id 5
    label "dziewczyna"
  ]
  node [
    id 6
    label "Aurignac"
  ]
  node [
    id 7
    label "osiedle"
  ]
  node [
    id 8
    label "Levallois-Perret"
  ]
  node [
    id 9
    label "Sabaudia"
  ]
  node [
    id 10
    label "Opat&#243;wek"
  ]
  node [
    id 11
    label "Saint-Acheul"
  ]
  node [
    id 12
    label "Boulogne"
  ]
  node [
    id 13
    label "Cecora"
  ]
  node [
    id 14
    label "kieliszek"
  ]
  node [
    id 15
    label "shot"
  ]
  node [
    id 16
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 17
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 18
    label "jaki&#347;"
  ]
  node [
    id 19
    label "jednolicie"
  ]
  node [
    id 20
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 21
    label "w&#243;dka"
  ]
  node [
    id 22
    label "ten"
  ]
  node [
    id 23
    label "ujednolicenie"
  ]
  node [
    id 24
    label "jednakowy"
  ]
  node [
    id 25
    label "punkt_apteczny"
  ]
  node [
    id 26
    label "sklep"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
]
