graph [
  maxDegree 1
  minDegree 1
  meanDegree 1
  density 0.3333333333333333
  graphCliqueNumber 2
  node [
    id 0
    label "gondreville"
    origin "text"
  ]
  node [
    id 1
    label "loiret"
    origin "text"
  ]
  node [
    id 2
    label "region"
  ]
  node [
    id 3
    label "centralny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
]
