graph [
  maxDegree 47
  minDegree 1
  meanDegree 2.0217391304347827
  density 0.011047754811119031
  graphCliqueNumber 2
  node [
    id 0
    label "pro&#347;ba"
    origin "text"
  ]
  node [
    id 1
    label "jaros&#322;aw"
    origin "text"
  ]
  node [
    id 2
    label "lipszyca"
    origin "text"
  ]
  node [
    id 3
    label "wkleja&#263;"
    origin "text"
  ]
  node [
    id 4
    label "zaproszenie"
    origin "text"
  ]
  node [
    id 5
    label "spotkanie"
    origin "text"
  ]
  node [
    id 6
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 7
    label "odby&#263;"
    origin "text"
  ]
  node [
    id 8
    label "si&#281;"
    origin "text"
  ]
  node [
    id 9
    label "&#347;roda"
    origin "text"
  ]
  node [
    id 10
    label "warszawa"
    origin "text"
  ]
  node [
    id 11
    label "zapowiada&#263;"
    origin "text"
  ]
  node [
    id 12
    label "sympatyczny"
    origin "text"
  ]
  node [
    id 13
    label "impreza"
    origin "text"
  ]
  node [
    id 14
    label "przy"
    origin "text"
  ]
  node [
    id 15
    label "tym"
    origin "text"
  ]
  node [
    id 16
    label "by&#263;"
    origin "text"
  ]
  node [
    id 17
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 18
    label "porozmawia&#263;"
    origin "text"
  ]
  node [
    id 19
    label "prawo"
    origin "text"
  ]
  node [
    id 20
    label "autorski"
    origin "text"
  ]
  node [
    id 21
    label "przedstawiciel"
    origin "text"
  ]
  node [
    id 22
    label "ministerstwo"
    origin "text"
  ]
  node [
    id 23
    label "kultura"
    origin "text"
  ]
  node [
    id 24
    label "solicitation"
  ]
  node [
    id 25
    label "wypowied&#378;"
  ]
  node [
    id 26
    label "przykleja&#263;"
  ]
  node [
    id 27
    label "wgrywa&#263;"
  ]
  node [
    id 28
    label "inset"
  ]
  node [
    id 29
    label "zaproponowanie"
  ]
  node [
    id 30
    label "propozycja"
  ]
  node [
    id 31
    label "invitation"
  ]
  node [
    id 32
    label "druk"
  ]
  node [
    id 33
    label "karteczka"
  ]
  node [
    id 34
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 35
    label "po&#380;egnanie"
  ]
  node [
    id 36
    label "spowodowanie"
  ]
  node [
    id 37
    label "znalezienie"
  ]
  node [
    id 38
    label "znajomy"
  ]
  node [
    id 39
    label "doznanie"
  ]
  node [
    id 40
    label "employment"
  ]
  node [
    id 41
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 42
    label "gather"
  ]
  node [
    id 43
    label "powitanie"
  ]
  node [
    id 44
    label "spotykanie"
  ]
  node [
    id 45
    label "wydarzenie"
  ]
  node [
    id 46
    label "gathering"
  ]
  node [
    id 47
    label "spotkanie_si&#281;"
  ]
  node [
    id 48
    label "zdarzenie_si&#281;"
  ]
  node [
    id 49
    label "match"
  ]
  node [
    id 50
    label "zawarcie"
  ]
  node [
    id 51
    label "reserve"
  ]
  node [
    id 52
    label "przej&#347;&#263;"
  ]
  node [
    id 53
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 54
    label "Popielec"
  ]
  node [
    id 55
    label "dzie&#324;_powszedni"
  ]
  node [
    id 56
    label "tydzie&#324;"
  ]
  node [
    id 57
    label "Warszawa"
  ]
  node [
    id 58
    label "samoch&#243;d"
  ]
  node [
    id 59
    label "fastback"
  ]
  node [
    id 60
    label "informowa&#263;"
  ]
  node [
    id 61
    label "og&#322;asza&#263;"
  ]
  node [
    id 62
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 63
    label "bode"
  ]
  node [
    id 64
    label "post"
  ]
  node [
    id 65
    label "ostrzega&#263;"
  ]
  node [
    id 66
    label "harbinger"
  ]
  node [
    id 67
    label "sympatycznie"
  ]
  node [
    id 68
    label "weso&#322;y"
  ]
  node [
    id 69
    label "mi&#322;y"
  ]
  node [
    id 70
    label "przyjemny"
  ]
  node [
    id 71
    label "party"
  ]
  node [
    id 72
    label "rozrywka"
  ]
  node [
    id 73
    label "przyj&#281;cie"
  ]
  node [
    id 74
    label "okazja"
  ]
  node [
    id 75
    label "impra"
  ]
  node [
    id 76
    label "si&#281;ga&#263;"
  ]
  node [
    id 77
    label "trwa&#263;"
  ]
  node [
    id 78
    label "obecno&#347;&#263;"
  ]
  node [
    id 79
    label "stan"
  ]
  node [
    id 80
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 81
    label "stand"
  ]
  node [
    id 82
    label "mie&#263;_miejsce"
  ]
  node [
    id 83
    label "uczestniczy&#263;"
  ]
  node [
    id 84
    label "chodzi&#263;"
  ]
  node [
    id 85
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 86
    label "equal"
  ]
  node [
    id 87
    label "free"
  ]
  node [
    id 88
    label "prate"
  ]
  node [
    id 89
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 90
    label "obserwacja"
  ]
  node [
    id 91
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 92
    label "nauka_prawa"
  ]
  node [
    id 93
    label "dominion"
  ]
  node [
    id 94
    label "normatywizm"
  ]
  node [
    id 95
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 96
    label "qualification"
  ]
  node [
    id 97
    label "opis"
  ]
  node [
    id 98
    label "regu&#322;a_Allena"
  ]
  node [
    id 99
    label "normalizacja"
  ]
  node [
    id 100
    label "kazuistyka"
  ]
  node [
    id 101
    label "regu&#322;a_Glogera"
  ]
  node [
    id 102
    label "kultura_duchowa"
  ]
  node [
    id 103
    label "prawo_karne"
  ]
  node [
    id 104
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 105
    label "standard"
  ]
  node [
    id 106
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 107
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 108
    label "struktura"
  ]
  node [
    id 109
    label "szko&#322;a"
  ]
  node [
    id 110
    label "prawo_karne_procesowe"
  ]
  node [
    id 111
    label "prawo_Mendla"
  ]
  node [
    id 112
    label "przepis"
  ]
  node [
    id 113
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 114
    label "criterion"
  ]
  node [
    id 115
    label "kanonistyka"
  ]
  node [
    id 116
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 117
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 118
    label "wykonawczy"
  ]
  node [
    id 119
    label "twierdzenie"
  ]
  node [
    id 120
    label "judykatura"
  ]
  node [
    id 121
    label "legislacyjnie"
  ]
  node [
    id 122
    label "umocowa&#263;"
  ]
  node [
    id 123
    label "podmiot"
  ]
  node [
    id 124
    label "procesualistyka"
  ]
  node [
    id 125
    label "kierunek"
  ]
  node [
    id 126
    label "kryminologia"
  ]
  node [
    id 127
    label "kryminalistyka"
  ]
  node [
    id 128
    label "cywilistyka"
  ]
  node [
    id 129
    label "law"
  ]
  node [
    id 130
    label "zasada_d'Alemberta"
  ]
  node [
    id 131
    label "jurisprudence"
  ]
  node [
    id 132
    label "zasada"
  ]
  node [
    id 133
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 134
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 135
    label "w&#322;asny"
  ]
  node [
    id 136
    label "oryginalny"
  ]
  node [
    id 137
    label "autorsko"
  ]
  node [
    id 138
    label "cz&#322;owiek"
  ]
  node [
    id 139
    label "cz&#322;onek"
  ]
  node [
    id 140
    label "substytuowanie"
  ]
  node [
    id 141
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 142
    label "przyk&#322;ad"
  ]
  node [
    id 143
    label "zast&#281;pca"
  ]
  node [
    id 144
    label "substytuowa&#263;"
  ]
  node [
    id 145
    label "Ministerstwo_Obrony_Narodowej"
  ]
  node [
    id 146
    label "ministerium"
  ]
  node [
    id 147
    label "resort"
  ]
  node [
    id 148
    label "urz&#261;d"
  ]
  node [
    id 149
    label "MSW"
  ]
  node [
    id 150
    label "departament"
  ]
  node [
    id 151
    label "NKWD"
  ]
  node [
    id 152
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 153
    label "przedmiot"
  ]
  node [
    id 154
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 155
    label "Wsch&#243;d"
  ]
  node [
    id 156
    label "rzecz"
  ]
  node [
    id 157
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 158
    label "sztuka"
  ]
  node [
    id 159
    label "religia"
  ]
  node [
    id 160
    label "przejmowa&#263;"
  ]
  node [
    id 161
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 162
    label "makrokosmos"
  ]
  node [
    id 163
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 164
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 165
    label "zjawisko"
  ]
  node [
    id 166
    label "praca_rolnicza"
  ]
  node [
    id 167
    label "tradycja"
  ]
  node [
    id 168
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 169
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 170
    label "przejmowanie"
  ]
  node [
    id 171
    label "cecha"
  ]
  node [
    id 172
    label "asymilowanie_si&#281;"
  ]
  node [
    id 173
    label "przej&#261;&#263;"
  ]
  node [
    id 174
    label "hodowla"
  ]
  node [
    id 175
    label "brzoskwiniarnia"
  ]
  node [
    id 176
    label "populace"
  ]
  node [
    id 177
    label "konwencja"
  ]
  node [
    id 178
    label "propriety"
  ]
  node [
    id 179
    label "jako&#347;&#263;"
  ]
  node [
    id 180
    label "kuchnia"
  ]
  node [
    id 181
    label "zwyczaj"
  ]
  node [
    id 182
    label "przej&#281;cie"
  ]
  node [
    id 183
    label "ro&#347;linno&#347;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 11
  ]
  edge [
    source 8
    target 12
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 54
  ]
  edge [
    source 9
    target 55
  ]
  edge [
    source 9
    target 56
  ]
  edge [
    source 9
    target 13
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 57
  ]
  edge [
    source 10
    target 58
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 11
    target 60
  ]
  edge [
    source 11
    target 61
  ]
  edge [
    source 11
    target 62
  ]
  edge [
    source 11
    target 63
  ]
  edge [
    source 11
    target 64
  ]
  edge [
    source 11
    target 65
  ]
  edge [
    source 11
    target 66
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 67
  ]
  edge [
    source 12
    target 68
  ]
  edge [
    source 12
    target 69
  ]
  edge [
    source 12
    target 70
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 71
  ]
  edge [
    source 13
    target 72
  ]
  edge [
    source 13
    target 73
  ]
  edge [
    source 13
    target 74
  ]
  edge [
    source 13
    target 75
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 76
  ]
  edge [
    source 16
    target 77
  ]
  edge [
    source 16
    target 78
  ]
  edge [
    source 16
    target 79
  ]
  edge [
    source 16
    target 80
  ]
  edge [
    source 16
    target 81
  ]
  edge [
    source 16
    target 82
  ]
  edge [
    source 16
    target 83
  ]
  edge [
    source 16
    target 84
  ]
  edge [
    source 16
    target 85
  ]
  edge [
    source 16
    target 86
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 87
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 88
  ]
  edge [
    source 18
    target 89
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 90
  ]
  edge [
    source 19
    target 91
  ]
  edge [
    source 19
    target 92
  ]
  edge [
    source 19
    target 93
  ]
  edge [
    source 19
    target 94
  ]
  edge [
    source 19
    target 95
  ]
  edge [
    source 19
    target 96
  ]
  edge [
    source 19
    target 97
  ]
  edge [
    source 19
    target 98
  ]
  edge [
    source 19
    target 99
  ]
  edge [
    source 19
    target 100
  ]
  edge [
    source 19
    target 101
  ]
  edge [
    source 19
    target 102
  ]
  edge [
    source 19
    target 103
  ]
  edge [
    source 19
    target 104
  ]
  edge [
    source 19
    target 105
  ]
  edge [
    source 19
    target 106
  ]
  edge [
    source 19
    target 107
  ]
  edge [
    source 19
    target 108
  ]
  edge [
    source 19
    target 109
  ]
  edge [
    source 19
    target 110
  ]
  edge [
    source 19
    target 111
  ]
  edge [
    source 19
    target 112
  ]
  edge [
    source 19
    target 113
  ]
  edge [
    source 19
    target 114
  ]
  edge [
    source 19
    target 115
  ]
  edge [
    source 19
    target 116
  ]
  edge [
    source 19
    target 117
  ]
  edge [
    source 19
    target 118
  ]
  edge [
    source 19
    target 119
  ]
  edge [
    source 19
    target 120
  ]
  edge [
    source 19
    target 121
  ]
  edge [
    source 19
    target 122
  ]
  edge [
    source 19
    target 123
  ]
  edge [
    source 19
    target 124
  ]
  edge [
    source 19
    target 125
  ]
  edge [
    source 19
    target 126
  ]
  edge [
    source 19
    target 127
  ]
  edge [
    source 19
    target 128
  ]
  edge [
    source 19
    target 129
  ]
  edge [
    source 19
    target 130
  ]
  edge [
    source 19
    target 131
  ]
  edge [
    source 19
    target 132
  ]
  edge [
    source 19
    target 133
  ]
  edge [
    source 19
    target 134
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 135
  ]
  edge [
    source 20
    target 136
  ]
  edge [
    source 20
    target 137
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 138
  ]
  edge [
    source 21
    target 139
  ]
  edge [
    source 21
    target 140
  ]
  edge [
    source 21
    target 141
  ]
  edge [
    source 21
    target 142
  ]
  edge [
    source 21
    target 143
  ]
  edge [
    source 21
    target 144
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 145
  ]
  edge [
    source 22
    target 146
  ]
  edge [
    source 22
    target 147
  ]
  edge [
    source 22
    target 148
  ]
  edge [
    source 22
    target 149
  ]
  edge [
    source 22
    target 150
  ]
  edge [
    source 22
    target 151
  ]
  edge [
    source 23
    target 152
  ]
  edge [
    source 23
    target 153
  ]
  edge [
    source 23
    target 154
  ]
  edge [
    source 23
    target 155
  ]
  edge [
    source 23
    target 156
  ]
  edge [
    source 23
    target 157
  ]
  edge [
    source 23
    target 158
  ]
  edge [
    source 23
    target 159
  ]
  edge [
    source 23
    target 160
  ]
  edge [
    source 23
    target 161
  ]
  edge [
    source 23
    target 162
  ]
  edge [
    source 23
    target 163
  ]
  edge [
    source 23
    target 164
  ]
  edge [
    source 23
    target 165
  ]
  edge [
    source 23
    target 166
  ]
  edge [
    source 23
    target 167
  ]
  edge [
    source 23
    target 168
  ]
  edge [
    source 23
    target 169
  ]
  edge [
    source 23
    target 170
  ]
  edge [
    source 23
    target 171
  ]
  edge [
    source 23
    target 172
  ]
  edge [
    source 23
    target 173
  ]
  edge [
    source 23
    target 174
  ]
  edge [
    source 23
    target 175
  ]
  edge [
    source 23
    target 176
  ]
  edge [
    source 23
    target 177
  ]
  edge [
    source 23
    target 178
  ]
  edge [
    source 23
    target 179
  ]
  edge [
    source 23
    target 180
  ]
  edge [
    source 23
    target 181
  ]
  edge [
    source 23
    target 182
  ]
  edge [
    source 23
    target 183
  ]
]
