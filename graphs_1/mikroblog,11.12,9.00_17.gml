graph [
  maxDegree 12
  minDegree 1
  meanDegree 1.8095238095238095
  density 0.09047619047619047
  graphCliqueNumber 2
  node [
    id 0
    label "joanna"
    origin "text"
  ]
  node [
    id 1
    label "j&#281;drzejczyk"
    origin "text"
  ]
  node [
    id 2
    label "rockowy"
    origin "text"
  ]
  node [
    id 3
    label "stylizacja"
    origin "text"
  ]
  node [
    id 4
    label "muzyczny"
  ]
  node [
    id 5
    label "nieklasyczny"
  ]
  node [
    id 6
    label "charakterystyczny"
  ]
  node [
    id 7
    label "rockowo"
  ]
  node [
    id 8
    label "wygl&#261;d"
  ]
  node [
    id 9
    label "szafiarka"
  ]
  node [
    id 10
    label "na&#347;ladownictwo"
  ]
  node [
    id 11
    label "wypowied&#378;"
  ]
  node [
    id 12
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 13
    label "imitacja"
  ]
  node [
    id 14
    label "zestawienie"
  ]
  node [
    id 15
    label "modyfikacja"
  ]
  node [
    id 16
    label "otoczka"
  ]
  node [
    id 17
    label "stylization"
  ]
  node [
    id 18
    label "styl"
  ]
  node [
    id 19
    label "Joanna"
  ]
  node [
    id 20
    label "J&#281;drzejczyk"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 19
    target 20
  ]
]
