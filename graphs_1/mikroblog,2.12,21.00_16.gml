graph [
  maxDegree 17
  minDegree 1
  meanDegree 2
  density 0.034482758620689655
  graphCliqueNumber 2
  node [
    id 0
    label "mama"
    origin "text"
  ]
  node [
    id 1
    label "zrobi&#263;"
    origin "text"
  ]
  node [
    id 2
    label "zadanie"
    origin "text"
  ]
  node [
    id 3
    label "domowy"
    origin "text"
  ]
  node [
    id 4
    label "nikt"
    origin "text"
  ]
  node [
    id 5
    label "chcie&#263;"
    origin "text"
  ]
  node [
    id 6
    label "pom&#243;c"
    origin "text"
  ]
  node [
    id 7
    label "matczysko"
  ]
  node [
    id 8
    label "macierz"
  ]
  node [
    id 9
    label "przodkini"
  ]
  node [
    id 10
    label "Matka_Boska"
  ]
  node [
    id 11
    label "macocha"
  ]
  node [
    id 12
    label "matka_zast&#281;pcza"
  ]
  node [
    id 13
    label "stara"
  ]
  node [
    id 14
    label "rodzice"
  ]
  node [
    id 15
    label "rodzic"
  ]
  node [
    id 16
    label "zorganizowa&#263;"
  ]
  node [
    id 17
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 18
    label "przerobi&#263;"
  ]
  node [
    id 19
    label "wystylizowa&#263;"
  ]
  node [
    id 20
    label "cause"
  ]
  node [
    id 21
    label "wydali&#263;"
  ]
  node [
    id 22
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 23
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 24
    label "post&#261;pi&#263;"
  ]
  node [
    id 25
    label "appoint"
  ]
  node [
    id 26
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 27
    label "nabra&#263;"
  ]
  node [
    id 28
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 29
    label "make"
  ]
  node [
    id 30
    label "yield"
  ]
  node [
    id 31
    label "czynno&#347;&#263;"
  ]
  node [
    id 32
    label "problem"
  ]
  node [
    id 33
    label "przepisanie"
  ]
  node [
    id 34
    label "przepisa&#263;"
  ]
  node [
    id 35
    label "za&#322;o&#380;enie"
  ]
  node [
    id 36
    label "work"
  ]
  node [
    id 37
    label "nakarmienie"
  ]
  node [
    id 38
    label "duty"
  ]
  node [
    id 39
    label "zbi&#243;r"
  ]
  node [
    id 40
    label "powierzanie"
  ]
  node [
    id 41
    label "zaszkodzenie"
  ]
  node [
    id 42
    label "d&#378;wigni&#281;cie"
  ]
  node [
    id 43
    label "zaj&#281;cie"
  ]
  node [
    id 44
    label "zobowi&#261;zanie"
  ]
  node [
    id 45
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 46
    label "domowo"
  ]
  node [
    id 47
    label "budynkowy"
  ]
  node [
    id 48
    label "miernota"
  ]
  node [
    id 49
    label "ciura"
  ]
  node [
    id 50
    label "czu&#263;"
  ]
  node [
    id 51
    label "desire"
  ]
  node [
    id 52
    label "kcie&#263;"
  ]
  node [
    id 53
    label "help"
  ]
  node [
    id 54
    label "aid"
  ]
  node [
    id 55
    label "u&#322;atwi&#263;"
  ]
  node [
    id 56
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 57
    label "concur"
  ]
  node [
    id 58
    label "zaskutkowa&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
]
