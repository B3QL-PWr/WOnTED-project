graph [
  maxDegree 17
  minDegree 1
  meanDegree 1.96
  density 0.04
  graphCliqueNumber 2
  node [
    id 0
    label "by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "jaki&#347;"
    origin "text"
  ]
  node [
    id 2
    label "koneser"
    origin "text"
  ]
  node [
    id 3
    label "ruda"
    origin "text"
  ]
  node [
    id 4
    label "kasztanow&#322;osy"
    origin "text"
  ]
  node [
    id 5
    label "czerwonow&#322;osych"
    origin "text"
  ]
  node [
    id 6
    label "kobieta"
    origin "text"
  ]
  node [
    id 7
    label "si&#281;ga&#263;"
  ]
  node [
    id 8
    label "trwa&#263;"
  ]
  node [
    id 9
    label "obecno&#347;&#263;"
  ]
  node [
    id 10
    label "stan"
  ]
  node [
    id 11
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 12
    label "stand"
  ]
  node [
    id 13
    label "mie&#263;_miejsce"
  ]
  node [
    id 14
    label "uczestniczy&#263;"
  ]
  node [
    id 15
    label "chodzi&#263;"
  ]
  node [
    id 16
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 17
    label "equal"
  ]
  node [
    id 18
    label "jako&#347;"
  ]
  node [
    id 19
    label "charakterystyczny"
  ]
  node [
    id 20
    label "ciekawy"
  ]
  node [
    id 21
    label "jako_tako"
  ]
  node [
    id 22
    label "dziwny"
  ]
  node [
    id 23
    label "niez&#322;y"
  ]
  node [
    id 24
    label "przyzwoity"
  ]
  node [
    id 25
    label "znawca"
  ]
  node [
    id 26
    label "aglomerownia"
  ]
  node [
    id 27
    label "kopalina_podstawowa"
  ]
  node [
    id 28
    label "wypa&#322;ek"
  ]
  node [
    id 29
    label "pra&#380;alnia"
  ]
  node [
    id 30
    label "ore"
  ]
  node [
    id 31
    label "atakamit"
  ]
  node [
    id 32
    label "br&#261;zowow&#322;osy"
  ]
  node [
    id 33
    label "rudow&#322;osy"
  ]
  node [
    id 34
    label "cz&#322;owiek"
  ]
  node [
    id 35
    label "przekwitanie"
  ]
  node [
    id 36
    label "m&#281;&#380;yna"
  ]
  node [
    id 37
    label "babka"
  ]
  node [
    id 38
    label "samica"
  ]
  node [
    id 39
    label "doros&#322;y"
  ]
  node [
    id 40
    label "ulec"
  ]
  node [
    id 41
    label "uleganie"
  ]
  node [
    id 42
    label "partnerka"
  ]
  node [
    id 43
    label "&#380;ona"
  ]
  node [
    id 44
    label "ulega&#263;"
  ]
  node [
    id 45
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 46
    label "pa&#324;stwo"
  ]
  node [
    id 47
    label "ulegni&#281;cie"
  ]
  node [
    id 48
    label "menopauza"
  ]
  node [
    id 49
    label "&#322;ono"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
]
