graph [
  maxDegree 19
  minDegree 1
  meanDegree 1.9090909090909092
  density 0.09090909090909091
  graphCliqueNumber 2
  node [
    id 0
    label "skok"
    origin "text"
  ]
  node [
    id 1
    label "polak"
    origin "text"
  ]
  node [
    id 2
    label "tworczoscwlasna"
    origin "text"
  ]
  node [
    id 3
    label "wybicie"
  ]
  node [
    id 4
    label "konkurencja"
  ]
  node [
    id 5
    label "derail"
  ]
  node [
    id 6
    label "ptak"
  ]
  node [
    id 7
    label "ruch"
  ]
  node [
    id 8
    label "l&#261;dowanie"
  ]
  node [
    id 9
    label "&#322;apa"
  ]
  node [
    id 10
    label "struktura_anatomiczna"
  ]
  node [
    id 11
    label "stroke"
  ]
  node [
    id 12
    label "gimnastyka_przyrz&#261;dowa"
  ]
  node [
    id 13
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 14
    label "zmiana"
  ]
  node [
    id 15
    label "caper"
  ]
  node [
    id 16
    label "zaj&#261;c"
  ]
  node [
    id 17
    label "naskok"
  ]
  node [
    id 18
    label "napad"
  ]
  node [
    id 19
    label "ko&#347;&#263;_skokowa"
  ]
  node [
    id 20
    label "noga"
  ]
  node [
    id 21
    label "polski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 21
  ]
]
