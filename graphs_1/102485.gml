graph [
  maxDegree 34
  minDegree 1
  meanDegree 2.1218274111675126
  density 0.010825650056977106
  graphCliqueNumber 3
  node [
    id 0
    label "luty"
    origin "text"
  ]
  node [
    id 1
    label "godz"
    origin "text"
  ]
  node [
    id 2
    label "miejski"
    origin "text"
  ]
  node [
    id 3
    label "biblioteka"
    origin "text"
  ]
  node [
    id 4
    label "publiczny"
    origin "text"
  ]
  node [
    id 5
    label "odby&#263;"
    origin "text"
  ]
  node [
    id 6
    label "si&#281;"
    origin "text"
  ]
  node [
    id 7
    label "otwarcie"
    origin "text"
  ]
  node [
    id 8
    label "wystawa"
    origin "text"
  ]
  node [
    id 9
    label "&#347;wi&#281;to"
    origin "text"
  ]
  node [
    id 10
    label "wachlarz"
    origin "text"
  ]
  node [
    id 11
    label "lalka"
    origin "text"
  ]
  node [
    id 12
    label "japo&#324;ski"
    origin "text"
  ]
  node [
    id 13
    label "po&#322;&#261;czy&#263;"
    origin "text"
  ]
  node [
    id 14
    label "fotograficzny"
    origin "text"
  ]
  node [
    id 15
    label "masakatsu"
    origin "text"
  ]
  node [
    id 16
    label "yoshida"
    origin "text"
  ]
  node [
    id 17
    label "podczas"
    origin "text"
  ]
  node [
    id 18
    label "autor"
    origin "text"
  ]
  node [
    id 19
    label "wyg&#322;osi&#263;"
    origin "text"
  ]
  node [
    id 20
    label "prelekcja"
    origin "text"
  ]
  node [
    id 21
    label "pi&#261;tek"
    origin "text"
  ]
  node [
    id 22
    label "kultura"
    origin "text"
  ]
  node [
    id 23
    label "japonia"
    origin "text"
  ]
  node [
    id 24
    label "wst&#281;p"
    origin "text"
  ]
  node [
    id 25
    label "wolny"
    origin "text"
  ]
  node [
    id 26
    label "miesi&#261;c"
  ]
  node [
    id 27
    label "Matka_Boska_Gromniczna"
  ]
  node [
    id 28
    label "walentynki"
  ]
  node [
    id 29
    label "miejsko"
  ]
  node [
    id 30
    label "miastowy"
  ]
  node [
    id 31
    label "typowy"
  ]
  node [
    id 32
    label "pok&#243;j"
  ]
  node [
    id 33
    label "rewers"
  ]
  node [
    id 34
    label "informatorium"
  ]
  node [
    id 35
    label "kolekcja"
  ]
  node [
    id 36
    label "czytelnik"
  ]
  node [
    id 37
    label "budynek"
  ]
  node [
    id 38
    label "zbi&#243;r"
  ]
  node [
    id 39
    label "programowanie"
  ]
  node [
    id 40
    label "ksi&#281;gozbi&#243;r_podr&#281;czny"
  ]
  node [
    id 41
    label "library"
  ]
  node [
    id 42
    label "instytucja"
  ]
  node [
    id 43
    label "czytelnia"
  ]
  node [
    id 44
    label "jawny"
  ]
  node [
    id 45
    label "upublicznienie"
  ]
  node [
    id 46
    label "upublicznianie"
  ]
  node [
    id 47
    label "publicznie"
  ]
  node [
    id 48
    label "reserve"
  ]
  node [
    id 49
    label "przej&#347;&#263;"
  ]
  node [
    id 50
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 51
    label "zdecydowanie"
  ]
  node [
    id 52
    label "czynno&#347;&#263;"
  ]
  node [
    id 53
    label "bezpo&#347;rednio"
  ]
  node [
    id 54
    label "udost&#281;pnienie"
  ]
  node [
    id 55
    label "granie"
  ]
  node [
    id 56
    label "gra&#263;"
  ]
  node [
    id 57
    label "ewidentnie"
  ]
  node [
    id 58
    label "jawnie"
  ]
  node [
    id 59
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 60
    label "rozpocz&#281;cie"
  ]
  node [
    id 61
    label "otwarty"
  ]
  node [
    id 62
    label "opening"
  ]
  node [
    id 63
    label "jawno"
  ]
  node [
    id 64
    label "galeria"
  ]
  node [
    id 65
    label "miejsce"
  ]
  node [
    id 66
    label "sklep"
  ]
  node [
    id 67
    label "muzeum"
  ]
  node [
    id 68
    label "Arsena&#322;"
  ]
  node [
    id 69
    label "szyba"
  ]
  node [
    id 70
    label "kurator"
  ]
  node [
    id 71
    label "Agropromocja"
  ]
  node [
    id 72
    label "wernisa&#380;"
  ]
  node [
    id 73
    label "impreza"
  ]
  node [
    id 74
    label "kustosz"
  ]
  node [
    id 75
    label "ekspozycja"
  ]
  node [
    id 76
    label "okno"
  ]
  node [
    id 77
    label "&#346;wi&#281;to_Pracy"
  ]
  node [
    id 78
    label "czas"
  ]
  node [
    id 79
    label "Nowy_Rok"
  ]
  node [
    id 80
    label "Godowe_&#346;wi&#281;to"
  ]
  node [
    id 81
    label "dzie&#324;_&#347;wi&#261;teczny"
  ]
  node [
    id 82
    label "Barb&#243;rka"
  ]
  node [
    id 83
    label "ramadan"
  ]
  node [
    id 84
    label "uk&#322;ad"
  ]
  node [
    id 85
    label "g&#322;uszec"
  ]
  node [
    id 86
    label "dodatek"
  ]
  node [
    id 87
    label "podzakres"
  ]
  node [
    id 88
    label "dziedzina"
  ]
  node [
    id 89
    label "ogon"
  ]
  node [
    id 90
    label "sfera"
  ]
  node [
    id 91
    label "wielko&#347;&#263;"
  ]
  node [
    id 92
    label "lalkarstwo"
  ]
  node [
    id 93
    label "statuetka"
  ]
  node [
    id 94
    label "zabawka"
  ]
  node [
    id 95
    label "matrioszka"
  ]
  node [
    id 96
    label "dummy"
  ]
  node [
    id 97
    label "Japanese"
  ]
  node [
    id 98
    label "po_japo&#324;sku"
  ]
  node [
    id 99
    label "katsudon"
  ]
  node [
    id 100
    label "j&#281;zyk"
  ]
  node [
    id 101
    label "futon"
  ]
  node [
    id 102
    label "japo&#324;sko"
  ]
  node [
    id 103
    label "hanafuda"
  ]
  node [
    id 104
    label "ky&#363;d&#333;"
  ]
  node [
    id 105
    label "j&#281;zyk_izolowany"
  ]
  node [
    id 106
    label "ju-jitsu"
  ]
  node [
    id 107
    label "karate"
  ]
  node [
    id 108
    label "sh&#333;gi"
  ]
  node [
    id 109
    label "azjatycki"
  ]
  node [
    id 110
    label "ikebana"
  ]
  node [
    id 111
    label "dalekowschodni"
  ]
  node [
    id 112
    label "relate"
  ]
  node [
    id 113
    label "spowodowa&#263;"
  ]
  node [
    id 114
    label "stworzy&#263;"
  ]
  node [
    id 115
    label "po&#322;&#261;czenie"
  ]
  node [
    id 116
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 117
    label "connect"
  ]
  node [
    id 118
    label "zjednoczy&#263;"
  ]
  node [
    id 119
    label "incorporate"
  ]
  node [
    id 120
    label "zrobi&#263;"
  ]
  node [
    id 121
    label "wizualny"
  ]
  node [
    id 122
    label "wierny"
  ]
  node [
    id 123
    label "fotograficznie"
  ]
  node [
    id 124
    label "pomys&#322;odawca"
  ]
  node [
    id 125
    label "kszta&#322;ciciel"
  ]
  node [
    id 126
    label "tworzyciel"
  ]
  node [
    id 127
    label "&#347;w"
  ]
  node [
    id 128
    label "wykonawca"
  ]
  node [
    id 129
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 130
    label "powiedzie&#263;"
  ]
  node [
    id 131
    label "say"
  ]
  node [
    id 132
    label "blok"
  ]
  node [
    id 133
    label "handout"
  ]
  node [
    id 134
    label "lecture"
  ]
  node [
    id 135
    label "wyk&#322;ad"
  ]
  node [
    id 136
    label "dzie&#324;_powszedni"
  ]
  node [
    id 137
    label "Wielki_Pi&#261;tek"
  ]
  node [
    id 138
    label "tydzie&#324;"
  ]
  node [
    id 139
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 140
    label "przedmiot"
  ]
  node [
    id 141
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 142
    label "Wsch&#243;d"
  ]
  node [
    id 143
    label "rzecz"
  ]
  node [
    id 144
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 145
    label "sztuka"
  ]
  node [
    id 146
    label "religia"
  ]
  node [
    id 147
    label "przejmowa&#263;"
  ]
  node [
    id 148
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 149
    label "makrokosmos"
  ]
  node [
    id 150
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 151
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 152
    label "zjawisko"
  ]
  node [
    id 153
    label "praca_rolnicza"
  ]
  node [
    id 154
    label "tradycja"
  ]
  node [
    id 155
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 156
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 157
    label "przejmowanie"
  ]
  node [
    id 158
    label "cecha"
  ]
  node [
    id 159
    label "asymilowanie_si&#281;"
  ]
  node [
    id 160
    label "przej&#261;&#263;"
  ]
  node [
    id 161
    label "hodowla"
  ]
  node [
    id 162
    label "brzoskwiniarnia"
  ]
  node [
    id 163
    label "populace"
  ]
  node [
    id 164
    label "konwencja"
  ]
  node [
    id 165
    label "propriety"
  ]
  node [
    id 166
    label "jako&#347;&#263;"
  ]
  node [
    id 167
    label "kuchnia"
  ]
  node [
    id 168
    label "zwyczaj"
  ]
  node [
    id 169
    label "przej&#281;cie"
  ]
  node [
    id 170
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 171
    label "doj&#347;cie"
  ]
  node [
    id 172
    label "zapowied&#378;"
  ]
  node [
    id 173
    label "evocation"
  ]
  node [
    id 174
    label "g&#322;oska"
  ]
  node [
    id 175
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 176
    label "wymowa"
  ]
  node [
    id 177
    label "pocz&#261;tek"
  ]
  node [
    id 178
    label "utw&#243;r"
  ]
  node [
    id 179
    label "podstawy"
  ]
  node [
    id 180
    label "tekst"
  ]
  node [
    id 181
    label "niezale&#380;ny"
  ]
  node [
    id 182
    label "swobodnie"
  ]
  node [
    id 183
    label "niespieszny"
  ]
  node [
    id 184
    label "rozrzedzanie"
  ]
  node [
    id 185
    label "zwolnienie_si&#281;"
  ]
  node [
    id 186
    label "wolno"
  ]
  node [
    id 187
    label "rozrzedzenie"
  ]
  node [
    id 188
    label "lu&#378;no"
  ]
  node [
    id 189
    label "zwalnianie_si&#281;"
  ]
  node [
    id 190
    label "wolnie"
  ]
  node [
    id 191
    label "strza&#322;"
  ]
  node [
    id 192
    label "rozwodnienie"
  ]
  node [
    id 193
    label "wakowa&#263;"
  ]
  node [
    id 194
    label "rozwadnianie"
  ]
  node [
    id 195
    label "rzedni&#281;cie"
  ]
  node [
    id 196
    label "zrzedni&#281;cie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 17
  ]
  edge [
    source 7
    target 18
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 13
  ]
  edge [
    source 8
    target 14
  ]
  edge [
    source 8
    target 18
  ]
  edge [
    source 8
    target 19
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 35
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 19
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 92
  ]
  edge [
    source 11
    target 93
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 11
    target 16
  ]
  edge [
    source 11
    target 21
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 12
    target 107
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 12
    target 109
  ]
  edge [
    source 12
    target 110
  ]
  edge [
    source 12
    target 111
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 13
    target 113
  ]
  edge [
    source 13
    target 114
  ]
  edge [
    source 13
    target 115
  ]
  edge [
    source 13
    target 116
  ]
  edge [
    source 13
    target 117
  ]
  edge [
    source 13
    target 118
  ]
  edge [
    source 13
    target 119
  ]
  edge [
    source 13
    target 120
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 121
  ]
  edge [
    source 14
    target 122
  ]
  edge [
    source 14
    target 123
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 21
  ]
  edge [
    source 18
    target 124
  ]
  edge [
    source 18
    target 125
  ]
  edge [
    source 18
    target 126
  ]
  edge [
    source 18
    target 127
  ]
  edge [
    source 18
    target 128
  ]
  edge [
    source 18
    target 129
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 130
  ]
  edge [
    source 19
    target 131
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 132
  ]
  edge [
    source 20
    target 133
  ]
  edge [
    source 20
    target 134
  ]
  edge [
    source 20
    target 135
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 136
  ]
  edge [
    source 21
    target 137
  ]
  edge [
    source 21
    target 138
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 139
  ]
  edge [
    source 22
    target 140
  ]
  edge [
    source 22
    target 141
  ]
  edge [
    source 22
    target 142
  ]
  edge [
    source 22
    target 143
  ]
  edge [
    source 22
    target 144
  ]
  edge [
    source 22
    target 145
  ]
  edge [
    source 22
    target 146
  ]
  edge [
    source 22
    target 147
  ]
  edge [
    source 22
    target 148
  ]
  edge [
    source 22
    target 149
  ]
  edge [
    source 22
    target 150
  ]
  edge [
    source 22
    target 151
  ]
  edge [
    source 22
    target 152
  ]
  edge [
    source 22
    target 153
  ]
  edge [
    source 22
    target 154
  ]
  edge [
    source 22
    target 155
  ]
  edge [
    source 22
    target 156
  ]
  edge [
    source 22
    target 157
  ]
  edge [
    source 22
    target 158
  ]
  edge [
    source 22
    target 159
  ]
  edge [
    source 22
    target 160
  ]
  edge [
    source 22
    target 161
  ]
  edge [
    source 22
    target 162
  ]
  edge [
    source 22
    target 163
  ]
  edge [
    source 22
    target 164
  ]
  edge [
    source 22
    target 165
  ]
  edge [
    source 22
    target 166
  ]
  edge [
    source 22
    target 167
  ]
  edge [
    source 22
    target 168
  ]
  edge [
    source 22
    target 169
  ]
  edge [
    source 22
    target 170
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 171
  ]
  edge [
    source 24
    target 172
  ]
  edge [
    source 24
    target 173
  ]
  edge [
    source 24
    target 174
  ]
  edge [
    source 24
    target 175
  ]
  edge [
    source 24
    target 176
  ]
  edge [
    source 24
    target 177
  ]
  edge [
    source 24
    target 178
  ]
  edge [
    source 24
    target 179
  ]
  edge [
    source 24
    target 180
  ]
  edge [
    source 25
    target 181
  ]
  edge [
    source 25
    target 182
  ]
  edge [
    source 25
    target 183
  ]
  edge [
    source 25
    target 184
  ]
  edge [
    source 25
    target 185
  ]
  edge [
    source 25
    target 186
  ]
  edge [
    source 25
    target 187
  ]
  edge [
    source 25
    target 188
  ]
  edge [
    source 25
    target 189
  ]
  edge [
    source 25
    target 190
  ]
  edge [
    source 25
    target 191
  ]
  edge [
    source 25
    target 192
  ]
  edge [
    source 25
    target 193
  ]
  edge [
    source 25
    target 194
  ]
  edge [
    source 25
    target 195
  ]
  edge [
    source 25
    target 196
  ]
]
