graph [
  maxDegree 45
  minDegree 1
  meanDegree 2.189247311827957
  density 0.004718205413422321
  graphCliqueNumber 3
  node [
    id 0
    label "pisz"
    origin "text"
  ]
  node [
    id 1
    label "gazeta"
    origin "text"
  ]
  node [
    id 2
    label "wyborczy"
    origin "text"
  ]
  node [
    id 3
    label "inicjatywa"
    origin "text"
  ]
  node [
    id 4
    label "konwent"
    origin "text"
  ]
  node [
    id 5
    label "senior"
    origin "text"
  ]
  node [
    id 6
    label "sejm"
    origin "text"
  ]
  node [
    id 7
    label "polecenie"
    origin "text"
  ]
  node [
    id 8
    label "prokuratura"
    origin "text"
  ]
  node [
    id 9
    label "krajowy"
    origin "text"
  ]
  node [
    id 10
    label "ca&#322;y"
    origin "text"
  ]
  node [
    id 11
    label "polska"
    origin "text"
  ]
  node [
    id 12
    label "prokurator"
    origin "text"
  ]
  node [
    id 13
    label "zbiera&#263;"
    origin "text"
  ]
  node [
    id 14
    label "dana"
    origin "text"
  ]
  node [
    id 15
    label "&#347;ledztwo"
    origin "text"
  ]
  node [
    id 16
    label "dochodzenie"
    origin "text"
  ]
  node [
    id 17
    label "dotyczy&#263;"
    origin "text"
  ]
  node [
    id 18
    label "media"
    origin "text"
  ]
  node [
    id 19
    label "lato"
    origin "text"
  ]
  node [
    id 20
    label "bliski"
    origin "text"
  ]
  node [
    id 21
    label "dni"
    origin "text"
  ]
  node [
    id 22
    label "sp&#322;yn&#261;&#263;"
    origin "text"
  ]
  node [
    id 23
    label "warszawa"
    origin "text"
  ]
  node [
    id 24
    label "list"
    origin "text"
  ]
  node [
    id 25
    label "wykaz"
    origin "text"
  ]
  node [
    id 26
    label "redakcja"
    origin "text"
  ]
  node [
    id 27
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 28
    label "odmawia&#263;"
    origin "text"
  ]
  node [
    id 29
    label "publikowa&#263;"
    origin "text"
  ]
  node [
    id 30
    label "sprostowanie"
    origin "text"
  ]
  node [
    id 31
    label "maja"
    origin "text"
  ]
  node [
    id 32
    label "proces"
    origin "text"
  ]
  node [
    id 33
    label "znies&#322;awienie"
    origin "text"
  ]
  node [
    id 34
    label "ujawnia&#263;"
    origin "text"
  ]
  node [
    id 35
    label "tajemnica"
    origin "text"
  ]
  node [
    id 36
    label "dziennikarski"
    origin "text"
  ]
  node [
    id 37
    label "by&#263;"
    origin "text"
  ]
  node [
    id 38
    label "te&#380;"
    origin "text"
  ]
  node [
    id 39
    label "sprawa"
    origin "text"
  ]
  node [
    id 40
    label "prywatny"
    origin "text"
  ]
  node [
    id 41
    label "zwi&#261;za&#263;"
    origin "text"
  ]
  node [
    id 42
    label "wykonywa&#263;"
    origin "text"
  ]
  node [
    id 43
    label "zaw&#243;d"
    origin "text"
  ]
  node [
    id 44
    label "apelacyjny"
    origin "text"
  ]
  node [
    id 45
    label "rzesz&#243;w"
    origin "text"
  ]
  node [
    id 46
    label "wys&#322;a&#263;"
    origin "text"
  ]
  node [
    id 47
    label "wszystek"
    origin "text"
  ]
  node [
    id 48
    label "dziennikarz"
    origin "text"
  ]
  node [
    id 49
    label "by&#322;y"
    origin "text"
  ]
  node [
    id 50
    label "dwa"
    origin "text"
  ]
  node [
    id 51
    label "pami&#281;ta&#263;"
    origin "text"
  ]
  node [
    id 52
    label "osoba"
    origin "text"
  ]
  node [
    id 53
    label "jecha&#263;"
    origin "text"
  ]
  node [
    id 54
    label "pijana"
    origin "text"
  ]
  node [
    id 55
    label "rower"
    origin "text"
  ]
  node [
    id 56
    label "prok"
    origin "text"
  ]
  node [
    id 57
    label "domaradzki"
    origin "text"
  ]
  node [
    id 58
    label "prasa"
  ]
  node [
    id 59
    label "tytu&#322;"
  ]
  node [
    id 60
    label "rozk&#322;ad&#243;wka"
  ]
  node [
    id 61
    label "czasopismo"
  ]
  node [
    id 62
    label "plan"
  ]
  node [
    id 63
    label "propozycja"
  ]
  node [
    id 64
    label "samodzielno&#347;&#263;"
  ]
  node [
    id 65
    label "zjazd"
  ]
  node [
    id 66
    label "oratorium"
  ]
  node [
    id 67
    label "wirydarz"
  ]
  node [
    id 68
    label "cela"
  ]
  node [
    id 69
    label "kustodia"
  ]
  node [
    id 70
    label "grupa"
  ]
  node [
    id 71
    label "&#321;agiewniki"
  ]
  node [
    id 72
    label "refektarz"
  ]
  node [
    id 73
    label "zgromadzenie"
  ]
  node [
    id 74
    label "Pyrkon"
  ]
  node [
    id 75
    label "zakon"
  ]
  node [
    id 76
    label "kapitularz"
  ]
  node [
    id 77
    label "Ko&#347;ci&#243;&#322;"
  ]
  node [
    id 78
    label "siedziba"
  ]
  node [
    id 79
    label "cz&#322;owiek"
  ]
  node [
    id 80
    label "komendancja"
  ]
  node [
    id 81
    label "feuda&#322;"
  ]
  node [
    id 82
    label "doros&#322;y"
  ]
  node [
    id 83
    label "ksi&#261;&#380;&#281;"
  ]
  node [
    id 84
    label "starzec"
  ]
  node [
    id 85
    label "dostojnik_ko&#347;cielny"
  ]
  node [
    id 86
    label "zwierzchnik"
  ]
  node [
    id 87
    label "zawodnik"
  ]
  node [
    id 88
    label "parlament"
  ]
  node [
    id 89
    label "obrady"
  ]
  node [
    id 90
    label "lewica"
  ]
  node [
    id 91
    label "prawica"
  ]
  node [
    id 92
    label "centrum"
  ]
  node [
    id 93
    label "izba_ni&#380;sza"
  ]
  node [
    id 94
    label "parliament"
  ]
  node [
    id 95
    label "zadanie"
  ]
  node [
    id 96
    label "wypowied&#378;"
  ]
  node [
    id 97
    label "education"
  ]
  node [
    id 98
    label "zaordynowanie"
  ]
  node [
    id 99
    label "rekomendacja"
  ]
  node [
    id 100
    label "ukaz"
  ]
  node [
    id 101
    label "przesadzenie"
  ]
  node [
    id 102
    label "statement"
  ]
  node [
    id 103
    label "recommendation"
  ]
  node [
    id 104
    label "powierzenie"
  ]
  node [
    id 105
    label "pobiegni&#281;cie"
  ]
  node [
    id 106
    label "consign"
  ]
  node [
    id 107
    label "pognanie"
  ]
  node [
    id 108
    label "doradzenie"
  ]
  node [
    id 109
    label "urz&#261;d"
  ]
  node [
    id 110
    label "organ"
  ]
  node [
    id 111
    label "rodzimy"
  ]
  node [
    id 112
    label "du&#380;y"
  ]
  node [
    id 113
    label "jedyny"
  ]
  node [
    id 114
    label "kompletny"
  ]
  node [
    id 115
    label "zdr&#243;w"
  ]
  node [
    id 116
    label "&#380;ywy"
  ]
  node [
    id 117
    label "ca&#322;o"
  ]
  node [
    id 118
    label "pe&#322;ny"
  ]
  node [
    id 119
    label "calu&#347;ko"
  ]
  node [
    id 120
    label "podobny"
  ]
  node [
    id 121
    label "pracownik"
  ]
  node [
    id 122
    label "prawnik"
  ]
  node [
    id 123
    label "oskar&#380;yciel_publiczny"
  ]
  node [
    id 124
    label "consolidate"
  ]
  node [
    id 125
    label "przejmowa&#263;"
  ]
  node [
    id 126
    label "dostawa&#263;"
  ]
  node [
    id 127
    label "mie&#263;_miejsce"
  ]
  node [
    id 128
    label "sprz&#261;ta&#263;"
  ]
  node [
    id 129
    label "oszcz&#281;dza&#263;"
  ]
  node [
    id 130
    label "pozyskiwa&#263;"
  ]
  node [
    id 131
    label "meet"
  ]
  node [
    id 132
    label "congregate"
  ]
  node [
    id 133
    label "poci&#261;ga&#263;"
  ]
  node [
    id 134
    label "robi&#263;"
  ]
  node [
    id 135
    label "uk&#322;ada&#263;"
  ]
  node [
    id 136
    label "umieszcza&#263;"
  ]
  node [
    id 137
    label "wzbiera&#263;"
  ]
  node [
    id 138
    label "gromadzi&#263;"
  ]
  node [
    id 139
    label "powodowa&#263;"
  ]
  node [
    id 140
    label "bra&#263;"
  ]
  node [
    id 141
    label "nat&#281;&#380;a&#263;"
  ]
  node [
    id 142
    label "dar"
  ]
  node [
    id 143
    label "cnota"
  ]
  node [
    id 144
    label "buddyzm"
  ]
  node [
    id 145
    label "detektyw"
  ]
  node [
    id 146
    label "wydarzenie"
  ]
  node [
    id 147
    label "examination"
  ]
  node [
    id 148
    label "dosi&#281;ganie"
  ]
  node [
    id 149
    label "robienie"
  ]
  node [
    id 150
    label "dor&#281;czanie"
  ]
  node [
    id 151
    label "&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 152
    label "stawanie_si&#281;"
  ]
  node [
    id 153
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 154
    label "trial"
  ]
  node [
    id 155
    label "dop&#322;ata"
  ]
  node [
    id 156
    label "dojrza&#322;y"
  ]
  node [
    id 157
    label "assay"
  ]
  node [
    id 158
    label "dochrapywanie_si&#281;"
  ]
  node [
    id 159
    label "inquest"
  ]
  node [
    id 160
    label "dodatek"
  ]
  node [
    id 161
    label "orgazm"
  ]
  node [
    id 162
    label "przy&#322;&#261;czanie_si&#281;"
  ]
  node [
    id 163
    label "osi&#261;ganie"
  ]
  node [
    id 164
    label "rozpowszechnianie"
  ]
  node [
    id 165
    label "rozwijanie_si&#281;"
  ]
  node [
    id 166
    label "strzelenie"
  ]
  node [
    id 167
    label "dolatywanie"
  ]
  node [
    id 168
    label "czynno&#347;&#263;"
  ]
  node [
    id 169
    label "roszczenie"
  ]
  node [
    id 170
    label "ro&#347;ni&#281;cie"
  ]
  node [
    id 171
    label "inquisition"
  ]
  node [
    id 172
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 173
    label "doczekanie"
  ]
  node [
    id 174
    label "docieranie"
  ]
  node [
    id 175
    label "dop&#322;ywanie"
  ]
  node [
    id 176
    label "zaznawanie"
  ]
  node [
    id 177
    label "maturation"
  ]
  node [
    id 178
    label "uzyskiwanie"
  ]
  node [
    id 179
    label "doje&#380;d&#380;anie"
  ]
  node [
    id 180
    label "przesy&#322;ka"
  ]
  node [
    id 181
    label "Inquisition"
  ]
  node [
    id 182
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 183
    label "postrzeganie"
  ]
  node [
    id 184
    label "powodowanie"
  ]
  node [
    id 185
    label "bargain"
  ]
  node [
    id 186
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 187
    label "tycze&#263;"
  ]
  node [
    id 188
    label "przekazior"
  ]
  node [
    id 189
    label "mass-media"
  ]
  node [
    id 190
    label "uzbrajanie"
  ]
  node [
    id 191
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 192
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 193
    label "medium"
  ]
  node [
    id 194
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 195
    label "pora_roku"
  ]
  node [
    id 196
    label "blisko"
  ]
  node [
    id 197
    label "przesz&#322;y"
  ]
  node [
    id 198
    label "gotowy"
  ]
  node [
    id 199
    label "dok&#322;adny"
  ]
  node [
    id 200
    label "kr&#243;tki"
  ]
  node [
    id 201
    label "znajomy"
  ]
  node [
    id 202
    label "przysz&#322;y"
  ]
  node [
    id 203
    label "oddalony"
  ]
  node [
    id 204
    label "silny"
  ]
  node [
    id 205
    label "zbli&#380;enie"
  ]
  node [
    id 206
    label "zwi&#261;zany"
  ]
  node [
    id 207
    label "nieodleg&#322;y"
  ]
  node [
    id 208
    label "ma&#322;y"
  ]
  node [
    id 209
    label "czas"
  ]
  node [
    id 210
    label "spowodowa&#263;"
  ]
  node [
    id 211
    label "dotrze&#263;"
  ]
  node [
    id 212
    label "flow"
  ]
  node [
    id 213
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 214
    label "rozprzestrzeni&#263;_si&#281;"
  ]
  node [
    id 215
    label "pokry&#263;_si&#281;"
  ]
  node [
    id 216
    label "zsun&#261;&#263;_si&#281;"
  ]
  node [
    id 217
    label "Warszawa"
  ]
  node [
    id 218
    label "samoch&#243;d"
  ]
  node [
    id 219
    label "fastback"
  ]
  node [
    id 220
    label "li&#347;&#263;"
  ]
  node [
    id 221
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 222
    label "poczta"
  ]
  node [
    id 223
    label "epistolografia"
  ]
  node [
    id 224
    label "poczta_elektroniczna"
  ]
  node [
    id 225
    label "znaczek_pocztowy"
  ]
  node [
    id 226
    label "wyliczanka"
  ]
  node [
    id 227
    label "catalog"
  ]
  node [
    id 228
    label "stock"
  ]
  node [
    id 229
    label "figurowa&#263;"
  ]
  node [
    id 230
    label "zbi&#243;r"
  ]
  node [
    id 231
    label "book"
  ]
  node [
    id 232
    label "pozycja"
  ]
  node [
    id 233
    label "tekst"
  ]
  node [
    id 234
    label "sumariusz"
  ]
  node [
    id 235
    label "telewizja"
  ]
  node [
    id 236
    label "redaction"
  ]
  node [
    id 237
    label "obr&#243;bka"
  ]
  node [
    id 238
    label "radio"
  ]
  node [
    id 239
    label "wydawnictwo"
  ]
  node [
    id 240
    label "zesp&#243;&#322;"
  ]
  node [
    id 241
    label "redaktor"
  ]
  node [
    id 242
    label "composition"
  ]
  node [
    id 243
    label "odrzuca&#263;"
  ]
  node [
    id 244
    label "wypowiada&#263;"
  ]
  node [
    id 245
    label "frame"
  ]
  node [
    id 246
    label "sprzeciwia&#263;_si&#281;"
  ]
  node [
    id 247
    label "os&#261;dza&#263;"
  ]
  node [
    id 248
    label "thank"
  ]
  node [
    id 249
    label "contest"
  ]
  node [
    id 250
    label "odpowiada&#263;"
  ]
  node [
    id 251
    label "wprowadza&#263;"
  ]
  node [
    id 252
    label "upublicznia&#263;"
  ]
  node [
    id 253
    label "give"
  ]
  node [
    id 254
    label "figura_my&#347;li"
  ]
  node [
    id 255
    label "obja&#347;nienie"
  ]
  node [
    id 256
    label "amendment"
  ]
  node [
    id 257
    label "tekst_prasowy"
  ]
  node [
    id 258
    label "skorygowanie"
  ]
  node [
    id 259
    label "correction"
  ]
  node [
    id 260
    label "wedyzm"
  ]
  node [
    id 261
    label "energia"
  ]
  node [
    id 262
    label "legislacyjnie"
  ]
  node [
    id 263
    label "kognicja"
  ]
  node [
    id 264
    label "przebieg"
  ]
  node [
    id 265
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 266
    label "przes&#322;anka"
  ]
  node [
    id 267
    label "rozprawa"
  ]
  node [
    id 268
    label "zjawisko"
  ]
  node [
    id 269
    label "nast&#281;pstwo"
  ]
  node [
    id 270
    label "zeszmacenie"
  ]
  node [
    id 271
    label "defamation"
  ]
  node [
    id 272
    label "profanation"
  ]
  node [
    id 273
    label "obelga"
  ]
  node [
    id 274
    label "oskar&#380;enie"
  ]
  node [
    id 275
    label "zszarganie"
  ]
  node [
    id 276
    label "zaszkodzenie"
  ]
  node [
    id 277
    label "informowa&#263;"
  ]
  node [
    id 278
    label "demaskator"
  ]
  node [
    id 279
    label "objawia&#263;"
  ]
  node [
    id 280
    label "dostrzega&#263;"
  ]
  node [
    id 281
    label "unwrap"
  ]
  node [
    id 282
    label "indicate"
  ]
  node [
    id 283
    label "dyskrecja"
  ]
  node [
    id 284
    label "secret"
  ]
  node [
    id 285
    label "zachowa&#263;"
  ]
  node [
    id 286
    label "zachowanie"
  ]
  node [
    id 287
    label "zachowywa&#263;"
  ]
  node [
    id 288
    label "wiedza"
  ]
  node [
    id 289
    label "wyda&#263;"
  ]
  node [
    id 290
    label "informacja"
  ]
  node [
    id 291
    label "rzecz"
  ]
  node [
    id 292
    label "enigmat"
  ]
  node [
    id 293
    label "zachowywanie"
  ]
  node [
    id 294
    label "wydawa&#263;"
  ]
  node [
    id 295
    label "wypaplanie"
  ]
  node [
    id 296
    label "taj&#324;"
  ]
  node [
    id 297
    label "spos&#243;b"
  ]
  node [
    id 298
    label "obowi&#261;zek"
  ]
  node [
    id 299
    label "zawodowy"
  ]
  node [
    id 300
    label "rzetelny"
  ]
  node [
    id 301
    label "tre&#347;ciwy"
  ]
  node [
    id 302
    label "po_dziennikarsku"
  ]
  node [
    id 303
    label "dziennikarsko"
  ]
  node [
    id 304
    label "obiektywny"
  ]
  node [
    id 305
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 306
    label "wzorowy"
  ]
  node [
    id 307
    label "typowy"
  ]
  node [
    id 308
    label "si&#281;ga&#263;"
  ]
  node [
    id 309
    label "trwa&#263;"
  ]
  node [
    id 310
    label "obecno&#347;&#263;"
  ]
  node [
    id 311
    label "stan"
  ]
  node [
    id 312
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 313
    label "stand"
  ]
  node [
    id 314
    label "uczestniczy&#263;"
  ]
  node [
    id 315
    label "chodzi&#263;"
  ]
  node [
    id 316
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 317
    label "equal"
  ]
  node [
    id 318
    label "temat"
  ]
  node [
    id 319
    label "idea"
  ]
  node [
    id 320
    label "szczeg&#243;&#322;"
  ]
  node [
    id 321
    label "object"
  ]
  node [
    id 322
    label "proposition"
  ]
  node [
    id 323
    label "czyj&#347;"
  ]
  node [
    id 324
    label "nieformalny"
  ]
  node [
    id 325
    label "personalny"
  ]
  node [
    id 326
    label "w&#322;asny"
  ]
  node [
    id 327
    label "prywatnie"
  ]
  node [
    id 328
    label "niepubliczny"
  ]
  node [
    id 329
    label "st&#281;&#380;e&#263;"
  ]
  node [
    id 330
    label "tobo&#322;ek"
  ]
  node [
    id 331
    label "obezw&#322;adni&#263;"
  ]
  node [
    id 332
    label "scali&#263;"
  ]
  node [
    id 333
    label "zawi&#261;za&#263;"
  ]
  node [
    id 334
    label "zatrzyma&#263;"
  ]
  node [
    id 335
    label "form"
  ]
  node [
    id 336
    label "bind"
  ]
  node [
    id 337
    label "wi&#261;zanie_chemiczne"
  ]
  node [
    id 338
    label "unify"
  ]
  node [
    id 339
    label "consort"
  ]
  node [
    id 340
    label "incorporate"
  ]
  node [
    id 341
    label "wi&#281;&#378;"
  ]
  node [
    id 342
    label "zobowi&#261;za&#263;"
  ]
  node [
    id 343
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 344
    label "w&#281;ze&#322;"
  ]
  node [
    id 345
    label "cz&#261;steczka_chemiczna"
  ]
  node [
    id 346
    label "powi&#261;za&#263;"
  ]
  node [
    id 347
    label "opakowa&#263;"
  ]
  node [
    id 348
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 349
    label "cement"
  ]
  node [
    id 350
    label "zaprawa"
  ]
  node [
    id 351
    label "relate"
  ]
  node [
    id 352
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 353
    label "work"
  ]
  node [
    id 354
    label "muzyka"
  ]
  node [
    id 355
    label "rola"
  ]
  node [
    id 356
    label "create"
  ]
  node [
    id 357
    label "wytwarza&#263;"
  ]
  node [
    id 358
    label "praca"
  ]
  node [
    id 359
    label "kwalifikacje"
  ]
  node [
    id 360
    label "emocja"
  ]
  node [
    id 361
    label "zawodoznawstwo"
  ]
  node [
    id 362
    label "office"
  ]
  node [
    id 363
    label "craft"
  ]
  node [
    id 364
    label "appellate"
  ]
  node [
    id 365
    label "wytworzy&#263;"
  ]
  node [
    id 366
    label "line"
  ]
  node [
    id 367
    label "ship"
  ]
  node [
    id 368
    label "convey"
  ]
  node [
    id 369
    label "przekaza&#263;"
  ]
  node [
    id 370
    label "post"
  ]
  node [
    id 371
    label "wy&#322;o&#380;y&#263;"
  ]
  node [
    id 372
    label "nakaza&#263;"
  ]
  node [
    id 373
    label "nowiniarz"
  ]
  node [
    id 374
    label "akredytowa&#263;"
  ]
  node [
    id 375
    label "akredytowanie"
  ]
  node [
    id 376
    label "bran&#380;owiec"
  ]
  node [
    id 377
    label "publicysta"
  ]
  node [
    id 378
    label "dawny"
  ]
  node [
    id 379
    label "rozw&#243;d"
  ]
  node [
    id 380
    label "rozwodzi&#263;_si&#281;"
  ]
  node [
    id 381
    label "eksprezydent"
  ]
  node [
    id 382
    label "rozwie&#347;&#263;_si&#281;"
  ]
  node [
    id 383
    label "partner"
  ]
  node [
    id 384
    label "rozwiedzenie_si&#281;"
  ]
  node [
    id 385
    label "rozwodzenie_si&#281;"
  ]
  node [
    id 386
    label "wcze&#347;niejszy"
  ]
  node [
    id 387
    label "zna&#263;"
  ]
  node [
    id 388
    label "troska&#263;_si&#281;"
  ]
  node [
    id 389
    label "chowa&#263;"
  ]
  node [
    id 390
    label "think"
  ]
  node [
    id 391
    label "pilnowa&#263;"
  ]
  node [
    id 392
    label "recall"
  ]
  node [
    id 393
    label "echo"
  ]
  node [
    id 394
    label "take_care"
  ]
  node [
    id 395
    label "Zgredek"
  ]
  node [
    id 396
    label "kategoria_gramatyczna"
  ]
  node [
    id 397
    label "Casanova"
  ]
  node [
    id 398
    label "Don_Juan"
  ]
  node [
    id 399
    label "Gargantua"
  ]
  node [
    id 400
    label "Faust"
  ]
  node [
    id 401
    label "profanum"
  ]
  node [
    id 402
    label "Chocho&#322;"
  ]
  node [
    id 403
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 404
    label "koniugacja"
  ]
  node [
    id 405
    label "Winnetou"
  ]
  node [
    id 406
    label "Dwukwiat"
  ]
  node [
    id 407
    label "homo_sapiens"
  ]
  node [
    id 408
    label "Edyp"
  ]
  node [
    id 409
    label "Herkules_Poirot"
  ]
  node [
    id 410
    label "ludzko&#347;&#263;"
  ]
  node [
    id 411
    label "mikrokosmos"
  ]
  node [
    id 412
    label "person"
  ]
  node [
    id 413
    label "Sherlock_Holmes"
  ]
  node [
    id 414
    label "portrecista"
  ]
  node [
    id 415
    label "Szwejk"
  ]
  node [
    id 416
    label "Hamlet"
  ]
  node [
    id 417
    label "duch"
  ]
  node [
    id 418
    label "g&#322;owa"
  ]
  node [
    id 419
    label "oddzia&#322;ywanie"
  ]
  node [
    id 420
    label "Quasimodo"
  ]
  node [
    id 421
    label "Dulcynea"
  ]
  node [
    id 422
    label "Don_Kiszot"
  ]
  node [
    id 423
    label "Wallenrod"
  ]
  node [
    id 424
    label "Plastu&#347;"
  ]
  node [
    id 425
    label "Harry_Potter"
  ]
  node [
    id 426
    label "figura"
  ]
  node [
    id 427
    label "parali&#380;owa&#263;"
  ]
  node [
    id 428
    label "istota"
  ]
  node [
    id 429
    label "Werter"
  ]
  node [
    id 430
    label "antropochoria"
  ]
  node [
    id 431
    label "posta&#263;"
  ]
  node [
    id 432
    label "proceed"
  ]
  node [
    id 433
    label "napada&#263;"
  ]
  node [
    id 434
    label "wyz&#322;o&#347;liwia&#263;_si&#281;"
  ]
  node [
    id 435
    label "wy&#347;miewa&#263;"
  ]
  node [
    id 436
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 437
    label "czu&#263;"
  ]
  node [
    id 438
    label "overdrive"
  ]
  node [
    id 439
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 440
    label "ride"
  ]
  node [
    id 441
    label "korzysta&#263;"
  ]
  node [
    id 442
    label "go"
  ]
  node [
    id 443
    label "prowadzi&#263;"
  ]
  node [
    id 444
    label "continue"
  ]
  node [
    id 445
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 446
    label "drive"
  ]
  node [
    id 447
    label "kontynuowa&#263;"
  ]
  node [
    id 448
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 449
    label "odbywa&#263;"
  ]
  node [
    id 450
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 451
    label "carry"
  ]
  node [
    id 452
    label "suport"
  ]
  node [
    id 453
    label "sztyca"
  ]
  node [
    id 454
    label "mostek"
  ]
  node [
    id 455
    label "cykloergometr"
  ]
  node [
    id 456
    label "&#322;a&#324;cuch"
  ]
  node [
    id 457
    label "przerzutka"
  ]
  node [
    id 458
    label "Romet"
  ]
  node [
    id 459
    label "torpedo"
  ]
  node [
    id 460
    label "dwuko&#322;owiec"
  ]
  node [
    id 461
    label "miska"
  ]
  node [
    id 462
    label "siode&#322;ko"
  ]
  node [
    id 463
    label "kierownica"
  ]
  node [
    id 464
    label "pojazd_niemechaniczny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 10
    target 47
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 16
  ]
  edge [
    source 11
    target 51
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 121
  ]
  edge [
    source 12
    target 122
  ]
  edge [
    source 12
    target 123
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 124
  ]
  edge [
    source 13
    target 125
  ]
  edge [
    source 13
    target 126
  ]
  edge [
    source 13
    target 127
  ]
  edge [
    source 13
    target 128
  ]
  edge [
    source 13
    target 129
  ]
  edge [
    source 13
    target 130
  ]
  edge [
    source 13
    target 131
  ]
  edge [
    source 13
    target 132
  ]
  edge [
    source 13
    target 133
  ]
  edge [
    source 13
    target 134
  ]
  edge [
    source 13
    target 135
  ]
  edge [
    source 13
    target 136
  ]
  edge [
    source 13
    target 137
  ]
  edge [
    source 13
    target 138
  ]
  edge [
    source 13
    target 139
  ]
  edge [
    source 13
    target 140
  ]
  edge [
    source 13
    target 141
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 142
  ]
  edge [
    source 14
    target 143
  ]
  edge [
    source 14
    target 144
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 36
  ]
  edge [
    source 15
    target 37
  ]
  edge [
    source 15
    target 145
  ]
  edge [
    source 15
    target 146
  ]
  edge [
    source 15
    target 147
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 148
  ]
  edge [
    source 16
    target 149
  ]
  edge [
    source 16
    target 150
  ]
  edge [
    source 16
    target 151
  ]
  edge [
    source 16
    target 147
  ]
  edge [
    source 16
    target 152
  ]
  edge [
    source 16
    target 153
  ]
  edge [
    source 16
    target 154
  ]
  edge [
    source 16
    target 155
  ]
  edge [
    source 16
    target 156
  ]
  edge [
    source 16
    target 157
  ]
  edge [
    source 16
    target 158
  ]
  edge [
    source 16
    target 159
  ]
  edge [
    source 16
    target 160
  ]
  edge [
    source 16
    target 161
  ]
  edge [
    source 16
    target 162
  ]
  edge [
    source 16
    target 163
  ]
  edge [
    source 16
    target 164
  ]
  edge [
    source 16
    target 165
  ]
  edge [
    source 16
    target 166
  ]
  edge [
    source 16
    target 167
  ]
  edge [
    source 16
    target 168
  ]
  edge [
    source 16
    target 169
  ]
  edge [
    source 16
    target 170
  ]
  edge [
    source 16
    target 171
  ]
  edge [
    source 16
    target 172
  ]
  edge [
    source 16
    target 173
  ]
  edge [
    source 16
    target 146
  ]
  edge [
    source 16
    target 174
  ]
  edge [
    source 16
    target 175
  ]
  edge [
    source 16
    target 176
  ]
  edge [
    source 16
    target 177
  ]
  edge [
    source 16
    target 178
  ]
  edge [
    source 16
    target 179
  ]
  edge [
    source 16
    target 180
  ]
  edge [
    source 16
    target 181
  ]
  edge [
    source 16
    target 182
  ]
  edge [
    source 16
    target 183
  ]
  edge [
    source 16
    target 145
  ]
  edge [
    source 16
    target 184
  ]
  edge [
    source 16
    target 51
  ]
  edge [
    source 16
    target 52
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 39
  ]
  edge [
    source 17
    target 48
  ]
  edge [
    source 17
    target 185
  ]
  edge [
    source 17
    target 186
  ]
  edge [
    source 17
    target 187
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 188
  ]
  edge [
    source 18
    target 189
  ]
  edge [
    source 18
    target 190
  ]
  edge [
    source 18
    target 191
  ]
  edge [
    source 18
    target 192
  ]
  edge [
    source 18
    target 193
  ]
  edge [
    source 18
    target 194
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 195
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 79
  ]
  edge [
    source 20
    target 196
  ]
  edge [
    source 20
    target 197
  ]
  edge [
    source 20
    target 198
  ]
  edge [
    source 20
    target 199
  ]
  edge [
    source 20
    target 200
  ]
  edge [
    source 20
    target 201
  ]
  edge [
    source 20
    target 202
  ]
  edge [
    source 20
    target 203
  ]
  edge [
    source 20
    target 204
  ]
  edge [
    source 20
    target 205
  ]
  edge [
    source 20
    target 206
  ]
  edge [
    source 20
    target 207
  ]
  edge [
    source 20
    target 208
  ]
  edge [
    source 20
    target 22
  ]
  edge [
    source 20
    target 45
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 209
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 210
  ]
  edge [
    source 22
    target 211
  ]
  edge [
    source 22
    target 212
  ]
  edge [
    source 22
    target 213
  ]
  edge [
    source 22
    target 214
  ]
  edge [
    source 22
    target 215
  ]
  edge [
    source 22
    target 216
  ]
  edge [
    source 22
    target 45
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 217
  ]
  edge [
    source 23
    target 218
  ]
  edge [
    source 23
    target 219
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 220
  ]
  edge [
    source 24
    target 221
  ]
  edge [
    source 24
    target 222
  ]
  edge [
    source 24
    target 223
  ]
  edge [
    source 24
    target 180
  ]
  edge [
    source 24
    target 224
  ]
  edge [
    source 24
    target 225
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 46
  ]
  edge [
    source 25
    target 39
  ]
  edge [
    source 25
    target 226
  ]
  edge [
    source 25
    target 227
  ]
  edge [
    source 25
    target 228
  ]
  edge [
    source 25
    target 229
  ]
  edge [
    source 25
    target 230
  ]
  edge [
    source 25
    target 231
  ]
  edge [
    source 25
    target 232
  ]
  edge [
    source 25
    target 233
  ]
  edge [
    source 25
    target 234
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 235
  ]
  edge [
    source 26
    target 236
  ]
  edge [
    source 26
    target 237
  ]
  edge [
    source 26
    target 238
  ]
  edge [
    source 26
    target 239
  ]
  edge [
    source 26
    target 240
  ]
  edge [
    source 26
    target 241
  ]
  edge [
    source 26
    target 78
  ]
  edge [
    source 26
    target 242
  ]
  edge [
    source 26
    target 233
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 243
  ]
  edge [
    source 28
    target 244
  ]
  edge [
    source 28
    target 245
  ]
  edge [
    source 28
    target 246
  ]
  edge [
    source 28
    target 247
  ]
  edge [
    source 28
    target 248
  ]
  edge [
    source 28
    target 249
  ]
  edge [
    source 28
    target 250
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 239
  ]
  edge [
    source 29
    target 251
  ]
  edge [
    source 29
    target 252
  ]
  edge [
    source 29
    target 253
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 254
  ]
  edge [
    source 30
    target 255
  ]
  edge [
    source 30
    target 256
  ]
  edge [
    source 30
    target 257
  ]
  edge [
    source 30
    target 258
  ]
  edge [
    source 30
    target 259
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 260
  ]
  edge [
    source 31
    target 261
  ]
  edge [
    source 31
    target 144
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 262
  ]
  edge [
    source 32
    target 263
  ]
  edge [
    source 32
    target 264
  ]
  edge [
    source 32
    target 265
  ]
  edge [
    source 32
    target 146
  ]
  edge [
    source 32
    target 266
  ]
  edge [
    source 32
    target 267
  ]
  edge [
    source 32
    target 268
  ]
  edge [
    source 32
    target 269
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 270
  ]
  edge [
    source 33
    target 271
  ]
  edge [
    source 33
    target 272
  ]
  edge [
    source 33
    target 273
  ]
  edge [
    source 33
    target 274
  ]
  edge [
    source 33
    target 275
  ]
  edge [
    source 33
    target 276
  ]
  edge [
    source 33
    target 55
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 277
  ]
  edge [
    source 34
    target 278
  ]
  edge [
    source 34
    target 279
  ]
  edge [
    source 34
    target 280
  ]
  edge [
    source 34
    target 281
  ]
  edge [
    source 34
    target 282
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 283
  ]
  edge [
    source 35
    target 284
  ]
  edge [
    source 35
    target 285
  ]
  edge [
    source 35
    target 286
  ]
  edge [
    source 35
    target 221
  ]
  edge [
    source 35
    target 287
  ]
  edge [
    source 35
    target 288
  ]
  edge [
    source 35
    target 289
  ]
  edge [
    source 35
    target 290
  ]
  edge [
    source 35
    target 291
  ]
  edge [
    source 35
    target 292
  ]
  edge [
    source 35
    target 293
  ]
  edge [
    source 35
    target 294
  ]
  edge [
    source 35
    target 295
  ]
  edge [
    source 35
    target 296
  ]
  edge [
    source 35
    target 297
  ]
  edge [
    source 35
    target 298
  ]
  edge [
    source 36
    target 299
  ]
  edge [
    source 36
    target 300
  ]
  edge [
    source 36
    target 301
  ]
  edge [
    source 36
    target 302
  ]
  edge [
    source 36
    target 303
  ]
  edge [
    source 36
    target 304
  ]
  edge [
    source 36
    target 305
  ]
  edge [
    source 36
    target 306
  ]
  edge [
    source 36
    target 307
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 39
  ]
  edge [
    source 37
    target 47
  ]
  edge [
    source 37
    target 308
  ]
  edge [
    source 37
    target 309
  ]
  edge [
    source 37
    target 310
  ]
  edge [
    source 37
    target 311
  ]
  edge [
    source 37
    target 312
  ]
  edge [
    source 37
    target 313
  ]
  edge [
    source 37
    target 127
  ]
  edge [
    source 37
    target 314
  ]
  edge [
    source 37
    target 315
  ]
  edge [
    source 37
    target 316
  ]
  edge [
    source 37
    target 317
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 47
  ]
  edge [
    source 39
    target 318
  ]
  edge [
    source 39
    target 263
  ]
  edge [
    source 39
    target 319
  ]
  edge [
    source 39
    target 320
  ]
  edge [
    source 39
    target 291
  ]
  edge [
    source 39
    target 146
  ]
  edge [
    source 39
    target 266
  ]
  edge [
    source 39
    target 267
  ]
  edge [
    source 39
    target 321
  ]
  edge [
    source 39
    target 322
  ]
  edge [
    source 39
    target 57
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 323
  ]
  edge [
    source 40
    target 324
  ]
  edge [
    source 40
    target 325
  ]
  edge [
    source 40
    target 326
  ]
  edge [
    source 40
    target 327
  ]
  edge [
    source 40
    target 328
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 329
  ]
  edge [
    source 41
    target 330
  ]
  edge [
    source 41
    target 331
  ]
  edge [
    source 41
    target 332
  ]
  edge [
    source 41
    target 333
  ]
  edge [
    source 41
    target 334
  ]
  edge [
    source 41
    target 335
  ]
  edge [
    source 41
    target 336
  ]
  edge [
    source 41
    target 337
  ]
  edge [
    source 41
    target 338
  ]
  edge [
    source 41
    target 339
  ]
  edge [
    source 41
    target 340
  ]
  edge [
    source 41
    target 341
  ]
  edge [
    source 41
    target 342
  ]
  edge [
    source 41
    target 343
  ]
  edge [
    source 41
    target 344
  ]
  edge [
    source 41
    target 345
  ]
  edge [
    source 41
    target 346
  ]
  edge [
    source 41
    target 347
  ]
  edge [
    source 41
    target 348
  ]
  edge [
    source 41
    target 349
  ]
  edge [
    source 41
    target 350
  ]
  edge [
    source 41
    target 351
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 352
  ]
  edge [
    source 42
    target 353
  ]
  edge [
    source 42
    target 134
  ]
  edge [
    source 42
    target 354
  ]
  edge [
    source 42
    target 355
  ]
  edge [
    source 42
    target 356
  ]
  edge [
    source 42
    target 357
  ]
  edge [
    source 42
    target 358
  ]
  edge [
    source 42
    target 53
  ]
  edge [
    source 43
    target 359
  ]
  edge [
    source 43
    target 360
  ]
  edge [
    source 43
    target 361
  ]
  edge [
    source 43
    target 362
  ]
  edge [
    source 43
    target 358
  ]
  edge [
    source 43
    target 363
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 364
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 46
    target 365
  ]
  edge [
    source 46
    target 366
  ]
  edge [
    source 46
    target 367
  ]
  edge [
    source 46
    target 368
  ]
  edge [
    source 46
    target 369
  ]
  edge [
    source 46
    target 370
  ]
  edge [
    source 46
    target 371
  ]
  edge [
    source 46
    target 372
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 373
  ]
  edge [
    source 48
    target 374
  ]
  edge [
    source 48
    target 375
  ]
  edge [
    source 48
    target 376
  ]
  edge [
    source 48
    target 377
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 378
  ]
  edge [
    source 49
    target 379
  ]
  edge [
    source 49
    target 380
  ]
  edge [
    source 49
    target 381
  ]
  edge [
    source 49
    target 382
  ]
  edge [
    source 49
    target 383
  ]
  edge [
    source 49
    target 384
  ]
  edge [
    source 49
    target 385
  ]
  edge [
    source 49
    target 386
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 308
  ]
  edge [
    source 51
    target 387
  ]
  edge [
    source 51
    target 388
  ]
  edge [
    source 51
    target 287
  ]
  edge [
    source 51
    target 389
  ]
  edge [
    source 51
    target 390
  ]
  edge [
    source 51
    target 391
  ]
  edge [
    source 51
    target 134
  ]
  edge [
    source 51
    target 392
  ]
  edge [
    source 51
    target 393
  ]
  edge [
    source 51
    target 394
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 395
  ]
  edge [
    source 52
    target 396
  ]
  edge [
    source 52
    target 397
  ]
  edge [
    source 52
    target 398
  ]
  edge [
    source 52
    target 399
  ]
  edge [
    source 52
    target 400
  ]
  edge [
    source 52
    target 401
  ]
  edge [
    source 52
    target 402
  ]
  edge [
    source 52
    target 403
  ]
  edge [
    source 52
    target 404
  ]
  edge [
    source 52
    target 405
  ]
  edge [
    source 52
    target 406
  ]
  edge [
    source 52
    target 407
  ]
  edge [
    source 52
    target 408
  ]
  edge [
    source 52
    target 409
  ]
  edge [
    source 52
    target 410
  ]
  edge [
    source 52
    target 411
  ]
  edge [
    source 52
    target 412
  ]
  edge [
    source 52
    target 413
  ]
  edge [
    source 52
    target 414
  ]
  edge [
    source 52
    target 415
  ]
  edge [
    source 52
    target 416
  ]
  edge [
    source 52
    target 417
  ]
  edge [
    source 52
    target 418
  ]
  edge [
    source 52
    target 419
  ]
  edge [
    source 52
    target 420
  ]
  edge [
    source 52
    target 421
  ]
  edge [
    source 52
    target 422
  ]
  edge [
    source 52
    target 423
  ]
  edge [
    source 52
    target 424
  ]
  edge [
    source 52
    target 425
  ]
  edge [
    source 52
    target 426
  ]
  edge [
    source 52
    target 427
  ]
  edge [
    source 52
    target 428
  ]
  edge [
    source 52
    target 429
  ]
  edge [
    source 52
    target 430
  ]
  edge [
    source 52
    target 431
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 432
  ]
  edge [
    source 53
    target 433
  ]
  edge [
    source 53
    target 434
  ]
  edge [
    source 53
    target 435
  ]
  edge [
    source 53
    target 436
  ]
  edge [
    source 53
    target 437
  ]
  edge [
    source 53
    target 438
  ]
  edge [
    source 53
    target 439
  ]
  edge [
    source 53
    target 440
  ]
  edge [
    source 53
    target 441
  ]
  edge [
    source 53
    target 442
  ]
  edge [
    source 53
    target 443
  ]
  edge [
    source 53
    target 444
  ]
  edge [
    source 53
    target 445
  ]
  edge [
    source 53
    target 446
  ]
  edge [
    source 53
    target 447
  ]
  edge [
    source 53
    target 448
  ]
  edge [
    source 53
    target 449
  ]
  edge [
    source 53
    target 450
  ]
  edge [
    source 53
    target 451
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 452
  ]
  edge [
    source 55
    target 453
  ]
  edge [
    source 55
    target 454
  ]
  edge [
    source 55
    target 455
  ]
  edge [
    source 55
    target 456
  ]
  edge [
    source 55
    target 457
  ]
  edge [
    source 55
    target 458
  ]
  edge [
    source 55
    target 459
  ]
  edge [
    source 55
    target 460
  ]
  edge [
    source 55
    target 461
  ]
  edge [
    source 55
    target 462
  ]
  edge [
    source 55
    target 463
  ]
  edge [
    source 55
    target 464
  ]
  edge [
    source 56
    target 57
  ]
]
