graph [
  maxDegree 17
  minDegree 1
  meanDegree 1.9298245614035088
  density 0.03446115288220551
  graphCliqueNumber 2
  node [
    id 0
    label "laserowy"
    origin "text"
  ]
  node [
    id 1
    label "korekcja"
    origin "text"
  ]
  node [
    id 2
    label "wzrok"
    origin "text"
  ]
  node [
    id 3
    label "opinia"
    origin "text"
  ]
  node [
    id 4
    label "rok"
    origin "text"
  ]
  node [
    id 5
    label "p&#243;&#378;no"
    origin "text"
  ]
  node [
    id 6
    label "laserowakorekcjawzroku"
    origin "text"
  ]
  node [
    id 7
    label "okokiedys"
    origin "text"
  ]
  node [
    id 8
    label "laserowo"
  ]
  node [
    id 9
    label "&#347;wietlny"
  ]
  node [
    id 10
    label "poprawa"
  ]
  node [
    id 11
    label "correction"
  ]
  node [
    id 12
    label "widzenie"
  ]
  node [
    id 13
    label "widzie&#263;"
  ]
  node [
    id 14
    label "oko"
  ]
  node [
    id 15
    label "expression"
  ]
  node [
    id 16
    label "czynno&#347;&#263;_wzrokowa"
  ]
  node [
    id 17
    label "zm&#261;cenie_si&#281;"
  ]
  node [
    id 18
    label "m&#281;tnienie"
  ]
  node [
    id 19
    label "zmys&#322;"
  ]
  node [
    id 20
    label "m&#281;tnie&#263;"
  ]
  node [
    id 21
    label "kontakt"
  ]
  node [
    id 22
    label "okulista"
  ]
  node [
    id 23
    label "dokument"
  ]
  node [
    id 24
    label "rehabilitowanie_si&#281;"
  ]
  node [
    id 25
    label "reputacja"
  ]
  node [
    id 26
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 27
    label "rehabilitowa&#263;_si&#281;"
  ]
  node [
    id 28
    label "cecha"
  ]
  node [
    id 29
    label "zrehabilitowanie_si&#281;"
  ]
  node [
    id 30
    label "informacja"
  ]
  node [
    id 31
    label "sofcik"
  ]
  node [
    id 32
    label "appraisal"
  ]
  node [
    id 33
    label "ekspertyza"
  ]
  node [
    id 34
    label "zrehabilitowa&#263;_si&#281;"
  ]
  node [
    id 35
    label "pogl&#261;d"
  ]
  node [
    id 36
    label "kryterium"
  ]
  node [
    id 37
    label "wielko&#347;&#263;"
  ]
  node [
    id 38
    label "stulecie"
  ]
  node [
    id 39
    label "kalendarz"
  ]
  node [
    id 40
    label "czas"
  ]
  node [
    id 41
    label "pora_roku"
  ]
  node [
    id 42
    label "cykl_astronomiczny"
  ]
  node [
    id 43
    label "p&#243;&#322;rocze"
  ]
  node [
    id 44
    label "grupa"
  ]
  node [
    id 45
    label "kwarta&#322;"
  ]
  node [
    id 46
    label "kurs"
  ]
  node [
    id 47
    label "jubileusz"
  ]
  node [
    id 48
    label "miesi&#261;c"
  ]
  node [
    id 49
    label "lata"
  ]
  node [
    id 50
    label "martwy_sezon"
  ]
  node [
    id 51
    label "p&#243;&#378;ny"
  ]
  node [
    id 52
    label "Ola"
  ]
  node [
    id 53
    label "7"
  ]
  node [
    id 54
    label "OP"
  ]
  node [
    id 55
    label "Pozdro"
  ]
  node [
    id 56
    label "Mirka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 55
    target 56
  ]
]
