graph [
  maxDegree 22
  minDegree 1
  meanDegree 2
  density 0.04878048780487805
  graphCliqueNumber 2
  node [
    id 0
    label "ca&#322;y"
    origin "text"
  ]
  node [
    id 1
    label "akcja"
    origin "text"
  ]
  node [
    id 2
    label "dewastacja"
    origin "text"
  ]
  node [
    id 3
    label "lamborghini"
    origin "text"
  ]
  node [
    id 4
    label "by&#322;a"
    origin "text"
  ]
  node [
    id 5
    label "marketingowy"
    origin "text"
  ]
  node [
    id 6
    label "du&#380;y"
  ]
  node [
    id 7
    label "jedyny"
  ]
  node [
    id 8
    label "kompletny"
  ]
  node [
    id 9
    label "zdr&#243;w"
  ]
  node [
    id 10
    label "&#380;ywy"
  ]
  node [
    id 11
    label "ca&#322;o"
  ]
  node [
    id 12
    label "pe&#322;ny"
  ]
  node [
    id 13
    label "calu&#347;ko"
  ]
  node [
    id 14
    label "podobny"
  ]
  node [
    id 15
    label "zagrywka"
  ]
  node [
    id 16
    label "czyn"
  ]
  node [
    id 17
    label "czynno&#347;&#263;"
  ]
  node [
    id 18
    label "wysoko&#347;&#263;"
  ]
  node [
    id 19
    label "stock"
  ]
  node [
    id 20
    label "gra"
  ]
  node [
    id 21
    label "w&#281;ze&#322;"
  ]
  node [
    id 22
    label "instrument_strunowy"
  ]
  node [
    id 23
    label "dywidenda"
  ]
  node [
    id 24
    label "przebieg"
  ]
  node [
    id 25
    label "udzia&#322;"
  ]
  node [
    id 26
    label "occupation"
  ]
  node [
    id 27
    label "jazda"
  ]
  node [
    id 28
    label "wydarzenie"
  ]
  node [
    id 29
    label "commotion"
  ]
  node [
    id 30
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 31
    label "sp&#243;&#322;ka_akcyjna"
  ]
  node [
    id 32
    label "operacja"
  ]
  node [
    id 33
    label "destruction"
  ]
  node [
    id 34
    label "zniszczenie"
  ]
  node [
    id 35
    label "devastation"
  ]
  node [
    id 36
    label "supersamoch&#243;d"
  ]
  node [
    id 37
    label "Lamborghini"
  ]
  node [
    id 38
    label "samoch&#243;d"
  ]
  node [
    id 39
    label "partnerka"
  ]
  node [
    id 40
    label "marketingowo"
  ]
  node [
    id 41
    label "handlowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
]
