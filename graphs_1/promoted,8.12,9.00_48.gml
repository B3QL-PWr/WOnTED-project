graph [
  maxDegree 21
  minDegree 1
  meanDegree 2
  density 0.014598540145985401
  graphCliqueNumber 3
  node [
    id 0
    label "koniec"
    origin "text"
  ]
  node [
    id 1
    label "wojna"
    origin "text"
  ]
  node [
    id 2
    label "przez"
    origin "text"
  ]
  node [
    id 3
    label "prawie"
    origin "text"
  ]
  node [
    id 4
    label "siedemdziesi&#261;t"
    origin "text"
  ]
  node [
    id 5
    label "lato"
    origin "text"
  ]
  node [
    id 6
    label "kolejny"
    origin "text"
  ]
  node [
    id 7
    label "lokator"
    origin "text"
  ]
  node [
    id 8
    label "prywatny"
    origin "text"
  ]
  node [
    id 9
    label "w&#322;a&#347;ciciel"
    origin "text"
  ]
  node [
    id 10
    label "niszczy&#263;"
    origin "text"
  ]
  node [
    id 11
    label "per&#322;a"
    origin "text"
  ]
  node [
    id 12
    label "&#347;l&#261;ski"
    origin "text"
  ]
  node [
    id 13
    label "renesans"
    origin "text"
  ]
  node [
    id 14
    label "zamienia&#263;"
    origin "text"
  ]
  node [
    id 15
    label "pa&#322;ac"
    origin "text"
  ]
  node [
    id 16
    label "gorzan&#243;w"
    origin "text"
  ]
  node [
    id 17
    label "tragiczny"
    origin "text"
  ]
  node [
    id 18
    label "ruina"
    origin "text"
  ]
  node [
    id 19
    label "defenestracja"
  ]
  node [
    id 20
    label "szereg"
  ]
  node [
    id 21
    label "dzia&#322;anie"
  ]
  node [
    id 22
    label "miejsce"
  ]
  node [
    id 23
    label "ostatnie_podrygi"
  ]
  node [
    id 24
    label "kres"
  ]
  node [
    id 25
    label "agonia"
  ]
  node [
    id 26
    label "visitation"
  ]
  node [
    id 27
    label "szeol"
  ]
  node [
    id 28
    label "mogi&#322;a"
  ]
  node [
    id 29
    label "chwila"
  ]
  node [
    id 30
    label "wydarzenie"
  ]
  node [
    id 31
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 32
    label "pogrzebanie"
  ]
  node [
    id 33
    label "punkt"
  ]
  node [
    id 34
    label "&#380;a&#322;oba"
  ]
  node [
    id 35
    label "zabicie"
  ]
  node [
    id 36
    label "kres_&#380;ycia"
  ]
  node [
    id 37
    label "zimna_wojna"
  ]
  node [
    id 38
    label "zbrodnia_przeciw_ludzko&#347;ci"
  ]
  node [
    id 39
    label "angaria"
  ]
  node [
    id 40
    label "wr&#243;g"
  ]
  node [
    id 41
    label "zbrodnia_przeciw_pokojowi"
  ]
  node [
    id 42
    label "walka"
  ]
  node [
    id 43
    label "war"
  ]
  node [
    id 44
    label "konflikt"
  ]
  node [
    id 45
    label "wojna_stuletnia"
  ]
  node [
    id 46
    label "burza"
  ]
  node [
    id 47
    label "zbrodnia_wojenna"
  ]
  node [
    id 48
    label "gra_w_karty"
  ]
  node [
    id 49
    label "sp&#243;r"
  ]
  node [
    id 50
    label "pora_roku"
  ]
  node [
    id 51
    label "inny"
  ]
  node [
    id 52
    label "nast&#281;pnie"
  ]
  node [
    id 53
    label "kt&#243;ry&#347;"
  ]
  node [
    id 54
    label "kolejno"
  ]
  node [
    id 55
    label "nastopny"
  ]
  node [
    id 56
    label "najemca"
  ]
  node [
    id 57
    label "za&#322;o&#380;yciel"
  ]
  node [
    id 58
    label "mieszkaniec"
  ]
  node [
    id 59
    label "urz&#261;dzenie"
  ]
  node [
    id 60
    label "urz&#261;dzenie_nawigacyjne"
  ]
  node [
    id 61
    label "czyj&#347;"
  ]
  node [
    id 62
    label "nieformalny"
  ]
  node [
    id 63
    label "personalny"
  ]
  node [
    id 64
    label "w&#322;asny"
  ]
  node [
    id 65
    label "prywatnie"
  ]
  node [
    id 66
    label "niepubliczny"
  ]
  node [
    id 67
    label "wykupienie"
  ]
  node [
    id 68
    label "bycie_w_posiadaniu"
  ]
  node [
    id 69
    label "wykupywanie"
  ]
  node [
    id 70
    label "podmiot"
  ]
  node [
    id 71
    label "os&#322;abia&#263;"
  ]
  node [
    id 72
    label "destroy"
  ]
  node [
    id 73
    label "wygrywa&#263;"
  ]
  node [
    id 74
    label "pamper"
  ]
  node [
    id 75
    label "zdrowie"
  ]
  node [
    id 76
    label "szkodzi&#263;"
  ]
  node [
    id 77
    label "uszkadza&#263;"
  ]
  node [
    id 78
    label "mar"
  ]
  node [
    id 79
    label "powodowa&#263;"
  ]
  node [
    id 80
    label "cz&#322;owiek"
  ]
  node [
    id 81
    label "tworzywo"
  ]
  node [
    id 82
    label "obiekt_naturalny"
  ]
  node [
    id 83
    label "mineraloid"
  ]
  node [
    id 84
    label "gem"
  ]
  node [
    id 85
    label "talent"
  ]
  node [
    id 86
    label "buchta"
  ]
  node [
    id 87
    label "szl&#261;ski"
  ]
  node [
    id 88
    label "waloszek"
  ]
  node [
    id 89
    label "szpajza"
  ]
  node [
    id 90
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 91
    label "ch&#322;opiec"
  ]
  node [
    id 92
    label "cug"
  ]
  node [
    id 93
    label "francuz"
  ]
  node [
    id 94
    label "regionalny"
  ]
  node [
    id 95
    label "&#347;lonski"
  ]
  node [
    id 96
    label "halba"
  ]
  node [
    id 97
    label "polski"
  ]
  node [
    id 98
    label "mietlorz"
  ]
  node [
    id 99
    label "sza&#322;ot"
  ]
  node [
    id 100
    label "czarne_kluski"
  ]
  node [
    id 101
    label "po_&#347;l&#261;sku"
  ]
  node [
    id 102
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 103
    label "krepel"
  ]
  node [
    id 104
    label "etnolekt"
  ]
  node [
    id 105
    label "Machiavelli"
  ]
  node [
    id 106
    label "Rafael"
  ]
  node [
    id 107
    label "nowo&#380;ytno&#347;&#263;"
  ]
  node [
    id 108
    label "quattrocento"
  ]
  node [
    id 109
    label "cinquecento"
  ]
  node [
    id 110
    label "manieryzm"
  ]
  node [
    id 111
    label "humanizm"
  ]
  node [
    id 112
    label "rozkwit"
  ]
  node [
    id 113
    label "mienia&#263;"
  ]
  node [
    id 114
    label "zakomunikowa&#263;"
  ]
  node [
    id 115
    label "zmienia&#263;"
  ]
  node [
    id 116
    label "zast&#281;powa&#263;"
  ]
  node [
    id 117
    label "Wersal"
  ]
  node [
    id 118
    label "Belweder"
  ]
  node [
    id 119
    label "budynek"
  ]
  node [
    id 120
    label "w&#322;adza"
  ]
  node [
    id 121
    label "rezydencja"
  ]
  node [
    id 122
    label "dramatyczny"
  ]
  node [
    id 123
    label "tragicznie"
  ]
  node [
    id 124
    label "wstrz&#261;saj&#261;cy"
  ]
  node [
    id 125
    label "&#347;miertelny"
  ]
  node [
    id 126
    label "nieszcz&#281;sny"
  ]
  node [
    id 127
    label "straszny"
  ]
  node [
    id 128
    label "koszmarny"
  ]
  node [
    id 129
    label "pechowy"
  ]
  node [
    id 130
    label "traiczny"
  ]
  node [
    id 131
    label "feralny"
  ]
  node [
    id 132
    label "nacechowany"
  ]
  node [
    id 133
    label "typowy"
  ]
  node [
    id 134
    label "descent"
  ]
  node [
    id 135
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 136
    label "stan"
  ]
  node [
    id 137
    label "Gorzan&#243;w"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 71
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 80
  ]
  edge [
    source 11
    target 81
  ]
  edge [
    source 11
    target 82
  ]
  edge [
    source 11
    target 83
  ]
  edge [
    source 11
    target 84
  ]
  edge [
    source 11
    target 85
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 86
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 12
    target 89
  ]
  edge [
    source 12
    target 90
  ]
  edge [
    source 12
    target 91
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 12
    target 94
  ]
  edge [
    source 12
    target 95
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 105
  ]
  edge [
    source 13
    target 106
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 113
  ]
  edge [
    source 14
    target 114
  ]
  edge [
    source 14
    target 115
  ]
  edge [
    source 14
    target 116
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 117
  ]
  edge [
    source 15
    target 118
  ]
  edge [
    source 15
    target 119
  ]
  edge [
    source 15
    target 120
  ]
  edge [
    source 15
    target 121
  ]
  edge [
    source 15
    target 137
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 122
  ]
  edge [
    source 17
    target 123
  ]
  edge [
    source 17
    target 124
  ]
  edge [
    source 17
    target 125
  ]
  edge [
    source 17
    target 126
  ]
  edge [
    source 17
    target 127
  ]
  edge [
    source 17
    target 128
  ]
  edge [
    source 17
    target 129
  ]
  edge [
    source 17
    target 130
  ]
  edge [
    source 17
    target 131
  ]
  edge [
    source 17
    target 132
  ]
  edge [
    source 17
    target 133
  ]
  edge [
    source 18
    target 134
  ]
  edge [
    source 18
    target 135
  ]
  edge [
    source 18
    target 136
  ]
]
