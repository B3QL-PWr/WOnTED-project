graph [
  maxDegree 5
  minDegree 1
  meanDegree 1.6842105263157894
  density 0.0935672514619883
  graphCliqueNumber 3
  node [
    id 0
    label "szaniec"
    origin "text"
  ]
  node [
    id 1
    label "kresowy"
    origin "text"
  ]
  node [
    id 2
    label "wa&#322;"
  ]
  node [
    id 3
    label "redoubt"
  ]
  node [
    id 4
    label "r&#243;w"
  ]
  node [
    id 5
    label "fortyfikacja"
  ]
  node [
    id 6
    label "wschodni"
  ]
  node [
    id 7
    label "blin"
  ]
  node [
    id 8
    label "warenik"
  ]
  node [
    id 9
    label "okr&#281;g"
  ]
  node [
    id 10
    label "XIV"
  ]
  node [
    id 11
    label "lwowski"
  ]
  node [
    id 12
    label "narodowy"
  ]
  node [
    id 13
    label "si&#322;y"
  ]
  node [
    id 14
    label "zbrojny"
  ]
  node [
    id 15
    label "grupa"
  ]
  node [
    id 16
    label "sza&#324;c"
  ]
  node [
    id 17
    label "propaganda"
  ]
  node [
    id 18
    label "czyn"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 17
    target 18
  ]
]
