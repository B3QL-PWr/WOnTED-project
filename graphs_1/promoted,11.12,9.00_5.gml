graph [
  maxDegree 28
  minDegree 1
  meanDegree 2
  density 0.019801980198019802
  graphCliqueNumber 2
  node [
    id 0
    label "masakrowa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "emeryt"
    origin "text"
  ]
  node [
    id 2
    label "pa&#322;ka"
    origin "text"
  ]
  node [
    id 3
    label "ci&#261;gn&#261;&#263;"
    origin "text"
  ]
  node [
    id 4
    label "kobieta"
    origin "text"
  ]
  node [
    id 5
    label "w&#322;os"
    origin "text"
  ]
  node [
    id 6
    label "ulica"
    origin "text"
  ]
  node [
    id 7
    label "kopa&#263;"
    origin "text"
  ]
  node [
    id 8
    label "niepe&#322;nosprawna"
    origin "text"
  ]
  node [
    id 9
    label "bi&#263;"
  ]
  node [
    id 10
    label "slaughter"
  ]
  node [
    id 11
    label "oldboj"
  ]
  node [
    id 12
    label "starszy_cz&#322;owiek"
  ]
  node [
    id 13
    label "niepracuj&#261;cy"
  ]
  node [
    id 14
    label "&#347;wiadczeniobiorca"
  ]
  node [
    id 15
    label "niedostateczny"
  ]
  node [
    id 16
    label "helofit"
  ]
  node [
    id 17
    label "ro&#347;lina_zielna"
  ]
  node [
    id 18
    label "n&#243;&#380;ka"
  ]
  node [
    id 19
    label "cane"
  ]
  node [
    id 20
    label "cios"
  ]
  node [
    id 21
    label "kij"
  ]
  node [
    id 22
    label "bro&#324;"
  ]
  node [
    id 23
    label "pa&#322;kowate"
  ]
  node [
    id 24
    label "bro&#324;_obuchowa"
  ]
  node [
    id 25
    label "bylina"
  ]
  node [
    id 26
    label "rogo&#380;yna"
  ]
  node [
    id 27
    label "proceed"
  ]
  node [
    id 28
    label "imperativeness"
  ]
  node [
    id 29
    label "przeci&#261;ga&#263;"
  ]
  node [
    id 30
    label "force"
  ]
  node [
    id 31
    label "wia&#263;"
  ]
  node [
    id 32
    label "rusza&#263;"
  ]
  node [
    id 33
    label "przewozi&#263;"
  ]
  node [
    id 34
    label "chcie&#263;"
  ]
  node [
    id 35
    label "adhere"
  ]
  node [
    id 36
    label "przesuwa&#263;"
  ]
  node [
    id 37
    label "wch&#322;ania&#263;"
  ]
  node [
    id 38
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 39
    label "radzi&#263;_sobie"
  ]
  node [
    id 40
    label "wyjmowa&#263;"
  ]
  node [
    id 41
    label "wa&#380;y&#263;"
  ]
  node [
    id 42
    label "przemieszcza&#263;"
  ]
  node [
    id 43
    label "set_about"
  ]
  node [
    id 44
    label "robi&#263;"
  ]
  node [
    id 45
    label "obrabia&#263;"
  ]
  node [
    id 46
    label "wyd&#322;u&#380;a&#263;"
  ]
  node [
    id 47
    label "zabiera&#263;"
  ]
  node [
    id 48
    label "wyt&#322;acza&#263;"
  ]
  node [
    id 49
    label "poci&#261;ga&#263;"
  ]
  node [
    id 50
    label "prosecute"
  ]
  node [
    id 51
    label "blow_up"
  ]
  node [
    id 52
    label "i&#347;&#263;"
  ]
  node [
    id 53
    label "cz&#322;owiek"
  ]
  node [
    id 54
    label "przekwitanie"
  ]
  node [
    id 55
    label "m&#281;&#380;yna"
  ]
  node [
    id 56
    label "babka"
  ]
  node [
    id 57
    label "samica"
  ]
  node [
    id 58
    label "doros&#322;y"
  ]
  node [
    id 59
    label "ulec"
  ]
  node [
    id 60
    label "uleganie"
  ]
  node [
    id 61
    label "partnerka"
  ]
  node [
    id 62
    label "&#380;ona"
  ]
  node [
    id 63
    label "ulega&#263;"
  ]
  node [
    id 64
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 65
    label "pa&#324;stwo"
  ]
  node [
    id 66
    label "ulegni&#281;cie"
  ]
  node [
    id 67
    label "menopauza"
  ]
  node [
    id 68
    label "&#322;ono"
  ]
  node [
    id 69
    label "cebulka"
  ]
  node [
    id 70
    label "wytw&#243;r"
  ]
  node [
    id 71
    label "powierzchnia"
  ]
  node [
    id 72
    label "mieszek_w&#322;osowy"
  ]
  node [
    id 73
    label "tkanina"
  ]
  node [
    id 74
    label "&#347;rodowisko"
  ]
  node [
    id 75
    label "miasteczko"
  ]
  node [
    id 76
    label "streetball"
  ]
  node [
    id 77
    label "pierzeja"
  ]
  node [
    id 78
    label "grupa"
  ]
  node [
    id 79
    label "pas_ruchu"
  ]
  node [
    id 80
    label "pas_rozdzielczy"
  ]
  node [
    id 81
    label "jezdnia"
  ]
  node [
    id 82
    label "droga"
  ]
  node [
    id 83
    label "korona_drogi"
  ]
  node [
    id 84
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 85
    label "chodnik"
  ]
  node [
    id 86
    label "arteria"
  ]
  node [
    id 87
    label "Broadway"
  ]
  node [
    id 88
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 89
    label "wysepka"
  ]
  node [
    id 90
    label "autostrada"
  ]
  node [
    id 91
    label "spulchnia&#263;"
  ]
  node [
    id 92
    label "wykopywa&#263;"
  ]
  node [
    id 93
    label "chow"
  ]
  node [
    id 94
    label "pora&#380;a&#263;"
  ]
  node [
    id 95
    label "hack"
  ]
  node [
    id 96
    label "pi&#322;ka_no&#380;na"
  ]
  node [
    id 97
    label "d&#243;&#322;"
  ]
  node [
    id 98
    label "krytykowa&#263;"
  ]
  node [
    id 99
    label "uderza&#263;"
  ]
  node [
    id 100
    label "dig"
  ]
  node [
    id 101
    label "przeszukiwa&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
]
