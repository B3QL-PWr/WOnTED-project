graph [
  maxDegree 23
  minDegree 1
  meanDegree 1.9672131147540983
  density 0.03278688524590164
  graphCliqueNumber 2
  node [
    id 0
    label "naukowiec"
    origin "text"
  ]
  node [
    id 1
    label "stworzy&#263;"
    origin "text"
  ]
  node [
    id 2
    label "test"
    origin "text"
  ]
  node [
    id 3
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 4
    label "pozwala&#263;"
    origin "text"
  ]
  node [
    id 5
    label "zdiagnozowa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "wszystek"
    origin "text"
  ]
  node [
    id 7
    label "rodzaj"
    origin "text"
  ]
  node [
    id 8
    label "rak"
    origin "text"
  ]
  node [
    id 9
    label "Miczurin"
  ]
  node [
    id 10
    label "&#347;ledziciel"
  ]
  node [
    id 11
    label "uczony"
  ]
  node [
    id 12
    label "przygotowa&#263;"
  ]
  node [
    id 13
    label "specjalista_od_public_relations"
  ]
  node [
    id 14
    label "create"
  ]
  node [
    id 15
    label "zrobi&#263;"
  ]
  node [
    id 16
    label "wizerunek"
  ]
  node [
    id 17
    label "do&#347;wiadczenie"
  ]
  node [
    id 18
    label "arkusz"
  ]
  node [
    id 19
    label "sprawdzian"
  ]
  node [
    id 20
    label "quiz"
  ]
  node [
    id 21
    label "przechodzi&#263;"
  ]
  node [
    id 22
    label "przechodzenie"
  ]
  node [
    id 23
    label "badanie"
  ]
  node [
    id 24
    label "narz&#281;dzie"
  ]
  node [
    id 25
    label "sytuacja"
  ]
  node [
    id 26
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 27
    label "authorize"
  ]
  node [
    id 28
    label "uznawa&#263;"
  ]
  node [
    id 29
    label "consent"
  ]
  node [
    id 30
    label "rozpozna&#263;"
  ]
  node [
    id 31
    label "oceni&#263;"
  ]
  node [
    id 32
    label "ca&#322;y"
  ]
  node [
    id 33
    label "kategoria_gramatyczna"
  ]
  node [
    id 34
    label "autorament"
  ]
  node [
    id 35
    label "jednostka_systematyczna"
  ]
  node [
    id 36
    label "fashion"
  ]
  node [
    id 37
    label "rodzina"
  ]
  node [
    id 38
    label "variety"
  ]
  node [
    id 39
    label "mozaika"
  ]
  node [
    id 40
    label "cz&#322;owiek"
  ]
  node [
    id 41
    label "przyrz&#261;d"
  ]
  node [
    id 42
    label "guz_z&#322;o&#347;liwy"
  ]
  node [
    id 43
    label "nacieka&#263;"
  ]
  node [
    id 44
    label "naciekanie"
  ]
  node [
    id 45
    label "erwinia"
  ]
  node [
    id 46
    label "rakowate"
  ]
  node [
    id 47
    label "mumia"
  ]
  node [
    id 48
    label "bakteria_brodawkowa"
  ]
  node [
    id 49
    label "dziesi&#281;cion&#243;g"
  ]
  node [
    id 50
    label "pieprzyk"
  ]
  node [
    id 51
    label "czarcia_miot&#322;a"
  ]
  node [
    id 52
    label "naciekn&#261;&#263;"
  ]
  node [
    id 53
    label "fitopatolog"
  ]
  node [
    id 54
    label "choroba_somatyczna"
  ]
  node [
    id 55
    label "schorzenie"
  ]
  node [
    id 56
    label "naciekni&#281;cie"
  ]
  node [
    id 57
    label "stres_oksydacyjny"
  ]
  node [
    id 58
    label "zbi&#243;r"
  ]
  node [
    id 59
    label "wada"
  ]
  node [
    id 60
    label "choroba"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 33
  ]
  edge [
    source 7
    target 34
  ]
  edge [
    source 7
    target 35
  ]
  edge [
    source 7
    target 36
  ]
  edge [
    source 7
    target 37
  ]
  edge [
    source 7
    target 38
  ]
  edge [
    source 8
    target 39
  ]
  edge [
    source 8
    target 40
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 8
    target 46
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
]
