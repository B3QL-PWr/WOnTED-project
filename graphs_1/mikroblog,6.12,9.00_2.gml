graph [
  maxDegree 29
  minDegree 1
  meanDegree 2.106060606060606
  density 0.01607679851954661
  graphCliqueNumber 3
  node [
    id 0
    label "linkedin"
    origin "text"
  ]
  node [
    id 1
    label "w&#322;a&#347;nie"
    origin "text"
  ]
  node [
    id 2
    label "napisa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "laseczek"
    origin "text"
  ]
  node [
    id 4
    label "propozycja"
    origin "text"
  ]
  node [
    id 5
    label "stanowisko"
    origin "text"
  ]
  node [
    id 6
    label "firma"
    origin "text"
  ]
  node [
    id 7
    label "oddzia&#322;"
    origin "text"
  ]
  node [
    id 8
    label "nawet"
    origin "text"
  ]
  node [
    id 9
    label "projekt"
    origin "text"
  ]
  node [
    id 10
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 11
    label "obecnie"
    origin "text"
  ]
  node [
    id 12
    label "by&#263;"
    origin "text"
  ]
  node [
    id 13
    label "oczywi&#347;cie"
    origin "text"
  ]
  node [
    id 14
    label "wszystko"
    origin "text"
  ]
  node [
    id 15
    label "pr&#243;cz"
    origin "text"
  ]
  node [
    id 16
    label "moi"
    origin "text"
  ]
  node [
    id 17
    label "profil"
    origin "text"
  ]
  node [
    id 18
    label "dok&#322;adnie"
  ]
  node [
    id 19
    label "postawi&#263;"
  ]
  node [
    id 20
    label "prasa"
  ]
  node [
    id 21
    label "stworzy&#263;"
  ]
  node [
    id 22
    label "donie&#347;&#263;"
  ]
  node [
    id 23
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 24
    label "write"
  ]
  node [
    id 25
    label "styl"
  ]
  node [
    id 26
    label "read"
  ]
  node [
    id 27
    label "pomys&#322;"
  ]
  node [
    id 28
    label "proposal"
  ]
  node [
    id 29
    label "miejsce"
  ]
  node [
    id 30
    label "awansowanie"
  ]
  node [
    id 31
    label "po&#322;o&#380;enie"
  ]
  node [
    id 32
    label "awansowa&#263;"
  ]
  node [
    id 33
    label "uprawianie"
  ]
  node [
    id 34
    label "powierzanie"
  ]
  node [
    id 35
    label "punkt"
  ]
  node [
    id 36
    label "pogl&#261;d"
  ]
  node [
    id 37
    label "wojsko"
  ]
  node [
    id 38
    label "praca"
  ]
  node [
    id 39
    label "wakowa&#263;"
  ]
  node [
    id 40
    label "stawia&#263;"
  ]
  node [
    id 41
    label "cz&#322;owiek"
  ]
  node [
    id 42
    label "MAC"
  ]
  node [
    id 43
    label "Hortex"
  ]
  node [
    id 44
    label "reengineering"
  ]
  node [
    id 45
    label "nazwa_w&#322;asna"
  ]
  node [
    id 46
    label "podmiot_gospodarczy"
  ]
  node [
    id 47
    label "Google"
  ]
  node [
    id 48
    label "zaufanie"
  ]
  node [
    id 49
    label "biurowiec"
  ]
  node [
    id 50
    label "interes"
  ]
  node [
    id 51
    label "zasoby_ludzkie"
  ]
  node [
    id 52
    label "networking"
  ]
  node [
    id 53
    label "paczkarnia"
  ]
  node [
    id 54
    label "Canon"
  ]
  node [
    id 55
    label "HP"
  ]
  node [
    id 56
    label "Baltona"
  ]
  node [
    id 57
    label "Pewex"
  ]
  node [
    id 58
    label "MAN_SE"
  ]
  node [
    id 59
    label "Apeks"
  ]
  node [
    id 60
    label "zasoby"
  ]
  node [
    id 61
    label "Orbis"
  ]
  node [
    id 62
    label "miejsce_pracy"
  ]
  node [
    id 63
    label "siedziba"
  ]
  node [
    id 64
    label "Spo&#322;em"
  ]
  node [
    id 65
    label "przedsi&#281;biorczo&#347;&#263;"
  ]
  node [
    id 66
    label "Orlen"
  ]
  node [
    id 67
    label "klasa"
  ]
  node [
    id 68
    label "whole"
  ]
  node [
    id 69
    label "jednostka"
  ]
  node [
    id 70
    label "jednostka_geologiczna"
  ]
  node [
    id 71
    label "system"
  ]
  node [
    id 72
    label "poziom"
  ]
  node [
    id 73
    label "agencja"
  ]
  node [
    id 74
    label "dogger"
  ]
  node [
    id 75
    label "formacja"
  ]
  node [
    id 76
    label "pi&#281;tro"
  ]
  node [
    id 77
    label "filia"
  ]
  node [
    id 78
    label "dzia&#322;"
  ]
  node [
    id 79
    label "promocja"
  ]
  node [
    id 80
    label "zesp&#243;&#322;"
  ]
  node [
    id 81
    label "kurs"
  ]
  node [
    id 82
    label "przegrupowywa&#263;_si&#281;"
  ]
  node [
    id 83
    label "bank"
  ]
  node [
    id 84
    label "lias"
  ]
  node [
    id 85
    label "szpital"
  ]
  node [
    id 86
    label "przegrupowywanie_si&#281;"
  ]
  node [
    id 87
    label "malm"
  ]
  node [
    id 88
    label "ajencja"
  ]
  node [
    id 89
    label "dokument"
  ]
  node [
    id 90
    label "device"
  ]
  node [
    id 91
    label "program_u&#380;ytkowy"
  ]
  node [
    id 92
    label "intencja"
  ]
  node [
    id 93
    label "agreement"
  ]
  node [
    id 94
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 95
    label "plan"
  ]
  node [
    id 96
    label "dokumentacja"
  ]
  node [
    id 97
    label "ninie"
  ]
  node [
    id 98
    label "aktualny"
  ]
  node [
    id 99
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 100
    label "si&#281;ga&#263;"
  ]
  node [
    id 101
    label "trwa&#263;"
  ]
  node [
    id 102
    label "obecno&#347;&#263;"
  ]
  node [
    id 103
    label "stan"
  ]
  node [
    id 104
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 105
    label "stand"
  ]
  node [
    id 106
    label "mie&#263;_miejsce"
  ]
  node [
    id 107
    label "uczestniczy&#263;"
  ]
  node [
    id 108
    label "chodzi&#263;"
  ]
  node [
    id 109
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 110
    label "equal"
  ]
  node [
    id 111
    label "lock"
  ]
  node [
    id 112
    label "absolut"
  ]
  node [
    id 113
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 114
    label "rzeczywisto&#347;&#263;_wirtualna"
  ]
  node [
    id 115
    label "seria"
  ]
  node [
    id 116
    label "twarz"
  ]
  node [
    id 117
    label "section"
  ]
  node [
    id 118
    label "listwa"
  ]
  node [
    id 119
    label "podgl&#261;d"
  ]
  node [
    id 120
    label "ozdoba"
  ]
  node [
    id 121
    label "dominanta"
  ]
  node [
    id 122
    label "obw&#243;dka"
  ]
  node [
    id 123
    label "faseta"
  ]
  node [
    id 124
    label "kontur"
  ]
  node [
    id 125
    label "profile"
  ]
  node [
    id 126
    label "konto"
  ]
  node [
    id 127
    label "przekr&#243;j"
  ]
  node [
    id 128
    label "awatar"
  ]
  node [
    id 129
    label "charakter"
  ]
  node [
    id 130
    label "element_konstrukcyjny"
  ]
  node [
    id 131
    label "sylwetka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 37
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 15
  ]
  edge [
    source 9
    target 12
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 27
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 17
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 11
    target 98
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 16
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 12
    target 107
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 12
    target 109
  ]
  edge [
    source 12
    target 110
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 111
  ]
  edge [
    source 14
    target 112
  ]
  edge [
    source 14
    target 113
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 114
  ]
  edge [
    source 17
    target 115
  ]
  edge [
    source 17
    target 116
  ]
  edge [
    source 17
    target 117
  ]
  edge [
    source 17
    target 118
  ]
  edge [
    source 17
    target 119
  ]
  edge [
    source 17
    target 120
  ]
  edge [
    source 17
    target 121
  ]
  edge [
    source 17
    target 122
  ]
  edge [
    source 17
    target 123
  ]
  edge [
    source 17
    target 124
  ]
  edge [
    source 17
    target 125
  ]
  edge [
    source 17
    target 126
  ]
  edge [
    source 17
    target 127
  ]
  edge [
    source 17
    target 128
  ]
  edge [
    source 17
    target 129
  ]
  edge [
    source 17
    target 130
  ]
  edge [
    source 17
    target 131
  ]
]
