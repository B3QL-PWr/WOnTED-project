graph [
  maxDegree 13
  minDegree 1
  meanDegree 1.8857142857142857
  density 0.05546218487394958
  graphCliqueNumber 4
  node [
    id 0
    label "minister"
    origin "text"
  ]
  node [
    id 1
    label "sport"
    origin "text"
  ]
  node [
    id 2
    label "turystyka"
    origin "text"
  ]
  node [
    id 3
    label "miros&#322;aw"
    origin "text"
  ]
  node [
    id 4
    label "drzewiecki"
    origin "text"
  ]
  node [
    id 5
    label "Goebbels"
  ]
  node [
    id 6
    label "Sto&#322;ypin"
  ]
  node [
    id 7
    label "rz&#261;d"
  ]
  node [
    id 8
    label "dostojnik"
  ]
  node [
    id 9
    label "zaatakowanie"
  ]
  node [
    id 10
    label "usportowi&#263;"
  ]
  node [
    id 11
    label "atakowanie"
  ]
  node [
    id 12
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 13
    label "zgrupowanie"
  ]
  node [
    id 14
    label "zaatakowa&#263;"
  ]
  node [
    id 15
    label "usportowienie"
  ]
  node [
    id 16
    label "sokolstwo"
  ]
  node [
    id 17
    label "kultura_fizyczna"
  ]
  node [
    id 18
    label "atakowa&#263;"
  ]
  node [
    id 19
    label "podr&#243;&#380;"
  ]
  node [
    id 20
    label "turyzm"
  ]
  node [
    id 21
    label "ruch"
  ]
  node [
    id 22
    label "i"
  ]
  node [
    id 23
    label "Miros&#322;awa"
  ]
  node [
    id 24
    label "Drzewiecki"
  ]
  node [
    id 25
    label "Krzysztofa"
  ]
  node [
    id 26
    label "Putra"
  ]
  node [
    id 27
    label "Wojciecha"
  ]
  node [
    id 28
    label "Olejniczak"
  ]
  node [
    id 29
    label "Zbigniew"
  ]
  node [
    id 30
    label "Matuszczaka"
  ]
  node [
    id 31
    label "Matuszczak"
  ]
  node [
    id 32
    label "Wojciech"
  ]
  node [
    id 33
    label "opole"
  ]
  node [
    id 34
    label "lubelski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 28
    target 32
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 31
  ]
  edge [
    source 33
    target 34
  ]
]
