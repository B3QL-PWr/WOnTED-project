graph [
  maxDegree 22
  minDegree 1
  meanDegree 2
  density 0.030303030303030304
  graphCliqueNumber 3
  node [
    id 0
    label "krzysztof"
    origin "text"
  ]
  node [
    id 1
    label "by&#322;y"
    origin "text"
  ]
  node [
    id 2
    label "kurator"
    origin "text"
  ]
  node [
    id 3
    label "s&#261;dowy"
    origin "text"
  ]
  node [
    id 4
    label "przez"
    origin "text"
  ]
  node [
    id 5
    label "lato"
    origin "text"
  ]
  node [
    id 6
    label "molestowa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "gwa&#322;ci&#263;"
    origin "text"
  ]
  node [
    id 8
    label "dziecko"
    origin "text"
  ]
  node [
    id 9
    label "krewna"
    origin "text"
  ]
  node [
    id 10
    label "dawny"
  ]
  node [
    id 11
    label "rozw&#243;d"
  ]
  node [
    id 12
    label "rozwodzi&#263;_si&#281;"
  ]
  node [
    id 13
    label "eksprezydent"
  ]
  node [
    id 14
    label "rozwie&#347;&#263;_si&#281;"
  ]
  node [
    id 15
    label "partner"
  ]
  node [
    id 16
    label "rozwiedzenie_si&#281;"
  ]
  node [
    id 17
    label "rozwodzenie_si&#281;"
  ]
  node [
    id 18
    label "wcze&#347;niejszy"
  ]
  node [
    id 19
    label "muzealnik"
  ]
  node [
    id 20
    label "cz&#322;owiek"
  ]
  node [
    id 21
    label "przedstawiciel"
  ]
  node [
    id 22
    label "opiekun"
  ]
  node [
    id 23
    label "funkcjonariusz"
  ]
  node [
    id 24
    label "wystawa"
  ]
  node [
    id 25
    label "urz&#281;dnik"
  ]
  node [
    id 26
    label "pe&#322;nomocnik"
  ]
  node [
    id 27
    label "wyznawca"
  ]
  node [
    id 28
    label "popularyzator"
  ]
  node [
    id 29
    label "kuratorium"
  ]
  node [
    id 30
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 31
    label "nadzorca"
  ]
  node [
    id 32
    label "s&#261;downie"
  ]
  node [
    id 33
    label "urz&#281;dowy"
  ]
  node [
    id 34
    label "pora_roku"
  ]
  node [
    id 35
    label "wykorzystywa&#263;"
  ]
  node [
    id 36
    label "zmusza&#263;"
  ]
  node [
    id 37
    label "nudzi&#263;"
  ]
  node [
    id 38
    label "trouble_oneself"
  ]
  node [
    id 39
    label "prosi&#263;"
  ]
  node [
    id 40
    label "conflict"
  ]
  node [
    id 41
    label "krzywdzi&#263;"
  ]
  node [
    id 42
    label "narusza&#263;"
  ]
  node [
    id 43
    label "potomstwo"
  ]
  node [
    id 44
    label "organizm"
  ]
  node [
    id 45
    label "sraluch"
  ]
  node [
    id 46
    label "utulanie"
  ]
  node [
    id 47
    label "pediatra"
  ]
  node [
    id 48
    label "dzieciarnia"
  ]
  node [
    id 49
    label "m&#322;odziak"
  ]
  node [
    id 50
    label "dzieciak"
  ]
  node [
    id 51
    label "utula&#263;"
  ]
  node [
    id 52
    label "potomek"
  ]
  node [
    id 53
    label "entliczek-pentliczek"
  ]
  node [
    id 54
    label "pedofil"
  ]
  node [
    id 55
    label "m&#322;odzik"
  ]
  node [
    id 56
    label "cz&#322;owieczek"
  ]
  node [
    id 57
    label "zwierz&#281;"
  ]
  node [
    id 58
    label "niepe&#322;noletni"
  ]
  node [
    id 59
    label "fledgling"
  ]
  node [
    id 60
    label "utuli&#263;"
  ]
  node [
    id 61
    label "utulenie"
  ]
  node [
    id 62
    label "kobieta"
  ]
  node [
    id 63
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 64
    label "krewni"
  ]
  node [
    id 65
    label "Krzysztof"
  ]
  node [
    id 66
    label "M"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 36
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 20
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 8
    target 46
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 65
    target 66
  ]
]
