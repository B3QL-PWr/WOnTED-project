graph [
  maxDegree 15
  minDegree 1
  meanDegree 2.046511627906977
  density 0.024076607387140903
  graphCliqueNumber 3
  node [
    id 0
    label "jedynie"
    origin "text"
  ]
  node [
    id 1
    label "kandydat"
    origin "text"
  ]
  node [
    id 2
    label "stanowisko"
    origin "text"
  ]
  node [
    id 3
    label "programista"
    origin "text"
  ]
  node [
    id 4
    label "by&#263;"
    origin "text"
  ]
  node [
    id 5
    label "wsta&#263;"
    origin "text"
  ]
  node [
    id 6
    label "napisa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "logicznie"
    origin "text"
  ]
  node [
    id 8
    label "dzia&#322;a&#263;"
    origin "text"
  ]
  node [
    id 9
    label "skrypt"
    origin "text"
  ]
  node [
    id 10
    label "nim"
    origin "text"
  ]
  node [
    id 11
    label "cz&#322;owiek"
  ]
  node [
    id 12
    label "materia&#322;"
  ]
  node [
    id 13
    label "lista_wyborcza"
  ]
  node [
    id 14
    label "bierne_prawo_wyborcze"
  ]
  node [
    id 15
    label "aspirowanie"
  ]
  node [
    id 16
    label "postawi&#263;"
  ]
  node [
    id 17
    label "miejsce"
  ]
  node [
    id 18
    label "awansowanie"
  ]
  node [
    id 19
    label "po&#322;o&#380;enie"
  ]
  node [
    id 20
    label "awansowa&#263;"
  ]
  node [
    id 21
    label "uprawianie"
  ]
  node [
    id 22
    label "powierzanie"
  ]
  node [
    id 23
    label "punkt"
  ]
  node [
    id 24
    label "pogl&#261;d"
  ]
  node [
    id 25
    label "wojsko"
  ]
  node [
    id 26
    label "praca"
  ]
  node [
    id 27
    label "wakowa&#263;"
  ]
  node [
    id 28
    label "stawia&#263;"
  ]
  node [
    id 29
    label "informatyk"
  ]
  node [
    id 30
    label "si&#281;ga&#263;"
  ]
  node [
    id 31
    label "trwa&#263;"
  ]
  node [
    id 32
    label "obecno&#347;&#263;"
  ]
  node [
    id 33
    label "stan"
  ]
  node [
    id 34
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 35
    label "stand"
  ]
  node [
    id 36
    label "mie&#263;_miejsce"
  ]
  node [
    id 37
    label "uczestniczy&#263;"
  ]
  node [
    id 38
    label "chodzi&#263;"
  ]
  node [
    id 39
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 40
    label "equal"
  ]
  node [
    id 41
    label "podnie&#347;&#263;_si&#281;"
  ]
  node [
    id 42
    label "opu&#347;ci&#263;"
  ]
  node [
    id 43
    label "rise"
  ]
  node [
    id 44
    label "arise"
  ]
  node [
    id 45
    label "mount"
  ]
  node [
    id 46
    label "stan&#261;&#263;"
  ]
  node [
    id 47
    label "przesta&#263;"
  ]
  node [
    id 48
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 49
    label "kuca&#263;"
  ]
  node [
    id 50
    label "ascend"
  ]
  node [
    id 51
    label "wyzdrowie&#263;"
  ]
  node [
    id 52
    label "dzie&#324;"
  ]
  node [
    id 53
    label "wzej&#347;&#263;"
  ]
  node [
    id 54
    label "prasa"
  ]
  node [
    id 55
    label "stworzy&#263;"
  ]
  node [
    id 56
    label "donie&#347;&#263;"
  ]
  node [
    id 57
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 58
    label "write"
  ]
  node [
    id 59
    label "styl"
  ]
  node [
    id 60
    label "read"
  ]
  node [
    id 61
    label "sensownie"
  ]
  node [
    id 62
    label "logically"
  ]
  node [
    id 63
    label "naukowo"
  ]
  node [
    id 64
    label "logiczny"
  ]
  node [
    id 65
    label "rozumowo"
  ]
  node [
    id 66
    label "work"
  ]
  node [
    id 67
    label "reakcja_chemiczna"
  ]
  node [
    id 68
    label "function"
  ]
  node [
    id 69
    label "commit"
  ]
  node [
    id 70
    label "bangla&#263;"
  ]
  node [
    id 71
    label "robi&#263;"
  ]
  node [
    id 72
    label "determine"
  ]
  node [
    id 73
    label "tryb"
  ]
  node [
    id 74
    label "powodowa&#263;"
  ]
  node [
    id 75
    label "dziama&#263;"
  ]
  node [
    id 76
    label "istnie&#263;"
  ]
  node [
    id 77
    label "drabina_analgetyczna"
  ]
  node [
    id 78
    label "model"
  ]
  node [
    id 79
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 80
    label "exemplar"
  ]
  node [
    id 81
    label "program"
  ]
  node [
    id 82
    label "wyprawka"
  ]
  node [
    id 83
    label "pomoc_naukowa"
  ]
  node [
    id 84
    label "mildew"
  ]
  node [
    id 85
    label "gra_planszowa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 16
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 8
    target 36
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 10
    target 85
  ]
]
