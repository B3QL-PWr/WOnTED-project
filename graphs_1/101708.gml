graph [
  maxDegree 38
  minDegree 1
  meanDegree 2.131233595800525
  density 0.00560850946263296
  graphCliqueNumber 3
  node [
    id 0
    label "czas"
    origin "text"
  ]
  node [
    id 1
    label "sesja"
    origin "text"
  ]
  node [
    id 2
    label "zada&#263;"
    origin "text"
  ]
  node [
    id 3
    label "pytanie"
    origin "text"
  ]
  node [
    id 4
    label "rzetelno&#347;&#263;"
    origin "text"
  ]
  node [
    id 5
    label "relacja"
    origin "text"
  ]
  node [
    id 6
    label "reporterski"
    origin "text"
  ]
  node [
    id 7
    label "sprawi&#263;"
    origin "text"
  ]
  node [
    id 8
    label "sam"
    origin "text"
  ]
  node [
    id 9
    label "zgromadzenie"
    origin "text"
  ]
  node [
    id 10
    label "jednia"
    origin "text"
  ]
  node [
    id 11
    label "obserwator"
    origin "text"
  ]
  node [
    id 12
    label "odebra&#263;"
    origin "text"
  ]
  node [
    id 13
    label "jako"
    origin "text"
  ]
  node [
    id 14
    label "happening"
    origin "text"
  ]
  node [
    id 15
    label "manifestacja"
    origin "text"
  ]
  node [
    id 16
    label "pogarda"
    origin "text"
  ]
  node [
    id 17
    label "zastanawia&#263;"
    origin "text"
  ]
  node [
    id 18
    label "si&#281;"
    origin "text"
  ]
  node [
    id 19
    label "tak&#380;e"
    origin "text"
  ]
  node [
    id 20
    label "nad"
    origin "text"
  ]
  node [
    id 21
    label "wzajemny"
    origin "text"
  ]
  node [
    id 22
    label "odniesienie"
    origin "text"
  ]
  node [
    id 23
    label "informacja"
    origin "text"
  ]
  node [
    id 24
    label "komentarz"
    origin "text"
  ]
  node [
    id 25
    label "redaktor"
    origin "text"
  ]
  node [
    id 26
    label "naczelne"
    origin "text"
  ]
  node [
    id 27
    label "zapyta&#263;"
    origin "text"
  ]
  node [
    id 28
    label "polityka"
    origin "text"
  ]
  node [
    id 29
    label "redakcyjny"
    origin "text"
  ]
  node [
    id 30
    label "jaka"
    origin "text"
  ]
  node [
    id 31
    label "przyj&#261;&#263;"
    origin "text"
  ]
  node [
    id 32
    label "wobec"
    origin "text"
  ]
  node [
    id 33
    label "krzy&#380;"
    origin "text"
  ]
  node [
    id 34
    label "uzna&#263;"
    origin "text"
  ]
  node [
    id 35
    label "ten"
    origin "text"
  ]
  node [
    id 36
    label "sprawa"
    origin "text"
  ]
  node [
    id 37
    label "istotny"
    origin "text"
  ]
  node [
    id 38
    label "dla"
    origin "text"
  ]
  node [
    id 39
    label "swoje"
    origin "text"
  ]
  node [
    id 40
    label "odbiorca"
    origin "text"
  ]
  node [
    id 41
    label "le&#380;e&#263;"
    origin "text"
  ]
  node [
    id 42
    label "podstawa"
    origin "text"
  ]
  node [
    id 43
    label "taki"
    origin "text"
  ]
  node [
    id 44
    label "decyzja"
    origin "text"
  ]
  node [
    id 45
    label "te&#380;"
    origin "text"
  ]
  node [
    id 46
    label "dlaczego"
    origin "text"
  ]
  node [
    id 47
    label "pewne"
    origin "text"
  ]
  node [
    id 48
    label "w&#261;tek"
    origin "text"
  ]
  node [
    id 49
    label "lub"
    origin "text"
  ]
  node [
    id 50
    label "uwypukli&#263;"
    origin "text"
  ]
  node [
    id 51
    label "inny"
    origin "text"
  ]
  node [
    id 52
    label "nieistotny"
    origin "text"
  ]
  node [
    id 53
    label "zdecydowa&#263;"
    origin "text"
  ]
  node [
    id 54
    label "przekaza&#263;"
    origin "text"
  ]
  node [
    id 55
    label "czasokres"
  ]
  node [
    id 56
    label "trawienie"
  ]
  node [
    id 57
    label "kategoria_gramatyczna"
  ]
  node [
    id 58
    label "period"
  ]
  node [
    id 59
    label "odczyt"
  ]
  node [
    id 60
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 61
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 62
    label "chwila"
  ]
  node [
    id 63
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 64
    label "poprzedzenie"
  ]
  node [
    id 65
    label "koniugacja"
  ]
  node [
    id 66
    label "dzieje"
  ]
  node [
    id 67
    label "poprzedzi&#263;"
  ]
  node [
    id 68
    label "przep&#322;ywanie"
  ]
  node [
    id 69
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 70
    label "odwlekanie_si&#281;"
  ]
  node [
    id 71
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 72
    label "Zeitgeist"
  ]
  node [
    id 73
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 74
    label "okres_czasu"
  ]
  node [
    id 75
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 76
    label "pochodzi&#263;"
  ]
  node [
    id 77
    label "schy&#322;ek"
  ]
  node [
    id 78
    label "czwarty_wymiar"
  ]
  node [
    id 79
    label "chronometria"
  ]
  node [
    id 80
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 81
    label "poprzedzanie"
  ]
  node [
    id 82
    label "pogoda"
  ]
  node [
    id 83
    label "zegar"
  ]
  node [
    id 84
    label "pochodzenie"
  ]
  node [
    id 85
    label "poprzedza&#263;"
  ]
  node [
    id 86
    label "trawi&#263;"
  ]
  node [
    id 87
    label "time_period"
  ]
  node [
    id 88
    label "rachuba_czasu"
  ]
  node [
    id 89
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 90
    label "czasoprzestrze&#324;"
  ]
  node [
    id 91
    label "laba"
  ]
  node [
    id 92
    label "seria"
  ]
  node [
    id 93
    label "obiekt"
  ]
  node [
    id 94
    label "dyskusja"
  ]
  node [
    id 95
    label "conference"
  ]
  node [
    id 96
    label "sesyjka"
  ]
  node [
    id 97
    label "spotkanie"
  ]
  node [
    id 98
    label "gie&#322;da_papier&#243;w_warto&#347;ciowych"
  ]
  node [
    id 99
    label "dzie&#324;_pracy"
  ]
  node [
    id 100
    label "konsylium"
  ]
  node [
    id 101
    label "egzamin"
  ]
  node [
    id 102
    label "dogrywka"
  ]
  node [
    id 103
    label "rok_akademicki"
  ]
  node [
    id 104
    label "psychoterapia"
  ]
  node [
    id 105
    label "d&#378;wign&#261;&#263;"
  ]
  node [
    id 106
    label "zaj&#261;&#263;"
  ]
  node [
    id 107
    label "zaszkodzi&#263;"
  ]
  node [
    id 108
    label "distribute"
  ]
  node [
    id 109
    label "za&#322;o&#380;y&#263;"
  ]
  node [
    id 110
    label "nakarmi&#263;"
  ]
  node [
    id 111
    label "deal"
  ]
  node [
    id 112
    label "put"
  ]
  node [
    id 113
    label "set"
  ]
  node [
    id 114
    label "zobowi&#261;za&#263;"
  ]
  node [
    id 115
    label "zadanie"
  ]
  node [
    id 116
    label "wypowied&#378;"
  ]
  node [
    id 117
    label "problemat"
  ]
  node [
    id 118
    label "rozpytywanie"
  ]
  node [
    id 119
    label "sprawdzian"
  ]
  node [
    id 120
    label "przes&#322;uchiwanie"
  ]
  node [
    id 121
    label "wypytanie"
  ]
  node [
    id 122
    label "zwracanie_si&#281;"
  ]
  node [
    id 123
    label "wypowiedzenie"
  ]
  node [
    id 124
    label "wywo&#322;ywanie"
  ]
  node [
    id 125
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 126
    label "problematyka"
  ]
  node [
    id 127
    label "question"
  ]
  node [
    id 128
    label "sprawdzanie"
  ]
  node [
    id 129
    label "odpowiadanie"
  ]
  node [
    id 130
    label "survey"
  ]
  node [
    id 131
    label "odpowiada&#263;"
  ]
  node [
    id 132
    label "egzaminowanie"
  ]
  node [
    id 133
    label "nastawienie"
  ]
  node [
    id 134
    label "message"
  ]
  node [
    id 135
    label "podzbi&#243;r"
  ]
  node [
    id 136
    label "iloczyn_kartezja&#324;ski"
  ]
  node [
    id 137
    label "ustosunkowywa&#263;"
  ]
  node [
    id 138
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 139
    label "bratnia_dusza"
  ]
  node [
    id 140
    label "zwi&#261;zanie"
  ]
  node [
    id 141
    label "ustosunkowanie"
  ]
  node [
    id 142
    label "ustosunkowywanie"
  ]
  node [
    id 143
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 144
    label "zwi&#261;za&#263;"
  ]
  node [
    id 145
    label "ustosunkowa&#263;"
  ]
  node [
    id 146
    label "niezgodno&#347;&#263;"
  ]
  node [
    id 147
    label "korespondent"
  ]
  node [
    id 148
    label "marriage"
  ]
  node [
    id 149
    label "wi&#261;zanie"
  ]
  node [
    id 150
    label "trasa"
  ]
  node [
    id 151
    label "zwi&#261;zek"
  ]
  node [
    id 152
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 153
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 154
    label "sprawko"
  ]
  node [
    id 155
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 156
    label "charakterystyczny"
  ]
  node [
    id 157
    label "wyrobi&#263;"
  ]
  node [
    id 158
    label "przygotowa&#263;"
  ]
  node [
    id 159
    label "wzi&#261;&#263;"
  ]
  node [
    id 160
    label "catch"
  ]
  node [
    id 161
    label "spowodowa&#263;"
  ]
  node [
    id 162
    label "frame"
  ]
  node [
    id 163
    label "sklep"
  ]
  node [
    id 164
    label "czynno&#347;&#263;"
  ]
  node [
    id 165
    label "spowodowanie"
  ]
  node [
    id 166
    label "klasztor"
  ]
  node [
    id 167
    label "kongregacja"
  ]
  node [
    id 168
    label "skupienie"
  ]
  node [
    id 169
    label "po&#347;ci&#261;ganie"
  ]
  node [
    id 170
    label "grupa"
  ]
  node [
    id 171
    label "wsp&#243;lnota"
  ]
  node [
    id 172
    label "organ"
  ]
  node [
    id 173
    label "templum"
  ]
  node [
    id 174
    label "gathering"
  ]
  node [
    id 175
    label "concourse"
  ]
  node [
    id 176
    label "caucus"
  ]
  node [
    id 177
    label "konwentykiel"
  ]
  node [
    id 178
    label "gromadzenie"
  ]
  node [
    id 179
    label "pozyskanie"
  ]
  node [
    id 180
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 181
    label "widownia"
  ]
  node [
    id 182
    label "wys&#322;annik"
  ]
  node [
    id 183
    label "ogl&#261;dacz"
  ]
  node [
    id 184
    label "odzyska&#263;"
  ]
  node [
    id 185
    label "zabra&#263;"
  ]
  node [
    id 186
    label "chwyci&#263;"
  ]
  node [
    id 187
    label "sketch"
  ]
  node [
    id 188
    label "zlecenie"
  ]
  node [
    id 189
    label "deprive"
  ]
  node [
    id 190
    label "give_birth"
  ]
  node [
    id 191
    label "dozna&#263;"
  ]
  node [
    id 192
    label "pozbawi&#263;"
  ]
  node [
    id 193
    label "deliver"
  ]
  node [
    id 194
    label "przedstawienie"
  ]
  node [
    id 195
    label "sztuka_wsp&#243;&#322;czesna"
  ]
  node [
    id 196
    label "exhibition"
  ]
  node [
    id 197
    label "show"
  ]
  node [
    id 198
    label "pokaz"
  ]
  node [
    id 199
    label "emocja"
  ]
  node [
    id 200
    label "strike"
  ]
  node [
    id 201
    label "interesowa&#263;"
  ]
  node [
    id 202
    label "zajemny"
  ]
  node [
    id 203
    label "wzajemnie"
  ]
  node [
    id 204
    label "zobop&#243;lny"
  ]
  node [
    id 205
    label "wsp&#243;lny"
  ]
  node [
    id 206
    label "od&#322;o&#380;enie"
  ]
  node [
    id 207
    label "skill"
  ]
  node [
    id 208
    label "doznanie"
  ]
  node [
    id 209
    label "gaze"
  ]
  node [
    id 210
    label "deference"
  ]
  node [
    id 211
    label "dochrapanie_si&#281;"
  ]
  node [
    id 212
    label "dostarczenie"
  ]
  node [
    id 213
    label "mention"
  ]
  node [
    id 214
    label "bearing"
  ]
  node [
    id 215
    label "po&#380;yczenie"
  ]
  node [
    id 216
    label "cel"
  ]
  node [
    id 217
    label "uzyskanie"
  ]
  node [
    id 218
    label "oddanie"
  ]
  node [
    id 219
    label "powi&#261;zanie"
  ]
  node [
    id 220
    label "doj&#347;cie"
  ]
  node [
    id 221
    label "doj&#347;&#263;"
  ]
  node [
    id 222
    label "powzi&#261;&#263;"
  ]
  node [
    id 223
    label "wiedza"
  ]
  node [
    id 224
    label "sygna&#322;"
  ]
  node [
    id 225
    label "obiegni&#281;cie"
  ]
  node [
    id 226
    label "obieganie"
  ]
  node [
    id 227
    label "obiec"
  ]
  node [
    id 228
    label "dane"
  ]
  node [
    id 229
    label "obiega&#263;"
  ]
  node [
    id 230
    label "punkt"
  ]
  node [
    id 231
    label "publikacja"
  ]
  node [
    id 232
    label "powzi&#281;cie"
  ]
  node [
    id 233
    label "comment"
  ]
  node [
    id 234
    label "artyku&#322;"
  ]
  node [
    id 235
    label "ocena"
  ]
  node [
    id 236
    label "gossip"
  ]
  node [
    id 237
    label "interpretacja"
  ]
  node [
    id 238
    label "audycja"
  ]
  node [
    id 239
    label "tekst"
  ]
  node [
    id 240
    label "cz&#322;owiek"
  ]
  node [
    id 241
    label "bran&#380;owiec"
  ]
  node [
    id 242
    label "wydawnictwo"
  ]
  node [
    id 243
    label "edytor"
  ]
  node [
    id 244
    label "redakcja"
  ]
  node [
    id 245
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 246
    label "kciuk"
  ]
  node [
    id 247
    label "ssaki_wy&#380;sze"
  ]
  node [
    id 248
    label "sprawdzi&#263;"
  ]
  node [
    id 249
    label "ask"
  ]
  node [
    id 250
    label "przes&#322;ucha&#263;"
  ]
  node [
    id 251
    label "quiz"
  ]
  node [
    id 252
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 253
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 254
    label "policy"
  ]
  node [
    id 255
    label "dyplomacja"
  ]
  node [
    id 256
    label "Europejska_Polityka_S&#261;siedztwa"
  ]
  node [
    id 257
    label "metoda"
  ]
  node [
    id 258
    label "kaftan"
  ]
  node [
    id 259
    label "dostarczy&#263;"
  ]
  node [
    id 260
    label "poch&#322;on&#261;&#263;"
  ]
  node [
    id 261
    label "przybra&#263;"
  ]
  node [
    id 262
    label "swallow"
  ]
  node [
    id 263
    label "zobowi&#261;za&#263;_si&#281;"
  ]
  node [
    id 264
    label "umie&#347;ci&#263;"
  ]
  node [
    id 265
    label "obra&#263;"
  ]
  node [
    id 266
    label "fall"
  ]
  node [
    id 267
    label "undertake"
  ]
  node [
    id 268
    label "absorb"
  ]
  node [
    id 269
    label "dopu&#347;ci&#263;"
  ]
  node [
    id 270
    label "receive"
  ]
  node [
    id 271
    label "draw"
  ]
  node [
    id 272
    label "zrobi&#263;"
  ]
  node [
    id 273
    label "przyj&#281;cie"
  ]
  node [
    id 274
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 275
    label "zgodzi&#263;_si&#281;"
  ]
  node [
    id 276
    label "wpu&#347;ci&#263;"
  ]
  node [
    id 277
    label "traverse"
  ]
  node [
    id 278
    label "gest"
  ]
  node [
    id 279
    label "cierpienie"
  ]
  node [
    id 280
    label "kr&#281;gos&#322;up"
  ]
  node [
    id 281
    label "symbol"
  ]
  node [
    id 282
    label "order"
  ]
  node [
    id 283
    label "przedmiot"
  ]
  node [
    id 284
    label "d&#322;o&#324;"
  ]
  node [
    id 285
    label "biblizm"
  ]
  node [
    id 286
    label "kszta&#322;t"
  ]
  node [
    id 287
    label "kara_&#347;mierci"
  ]
  node [
    id 288
    label "rede"
  ]
  node [
    id 289
    label "assent"
  ]
  node [
    id 290
    label "oceni&#263;"
  ]
  node [
    id 291
    label "przyzna&#263;"
  ]
  node [
    id 292
    label "stwierdzi&#263;"
  ]
  node [
    id 293
    label "see"
  ]
  node [
    id 294
    label "okre&#347;lony"
  ]
  node [
    id 295
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 296
    label "temat"
  ]
  node [
    id 297
    label "kognicja"
  ]
  node [
    id 298
    label "idea"
  ]
  node [
    id 299
    label "szczeg&#243;&#322;"
  ]
  node [
    id 300
    label "rzecz"
  ]
  node [
    id 301
    label "wydarzenie"
  ]
  node [
    id 302
    label "przes&#322;anka"
  ]
  node [
    id 303
    label "rozprawa"
  ]
  node [
    id 304
    label "object"
  ]
  node [
    id 305
    label "proposition"
  ]
  node [
    id 306
    label "du&#380;y"
  ]
  node [
    id 307
    label "silny"
  ]
  node [
    id 308
    label "realny"
  ]
  node [
    id 309
    label "eksponowany"
  ]
  node [
    id 310
    label "istotnie"
  ]
  node [
    id 311
    label "znaczny"
  ]
  node [
    id 312
    label "dono&#347;ny"
  ]
  node [
    id 313
    label "recipient_role"
  ]
  node [
    id 314
    label "otrzymywanie"
  ]
  node [
    id 315
    label "otrzymanie"
  ]
  node [
    id 316
    label "by&#263;"
  ]
  node [
    id 317
    label "trwa&#263;"
  ]
  node [
    id 318
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 319
    label "po&#322;o&#380;enie"
  ]
  node [
    id 320
    label "equate"
  ]
  node [
    id 321
    label "gr&#243;b"
  ]
  node [
    id 322
    label "pokrywa&#263;"
  ]
  node [
    id 323
    label "lie"
  ]
  node [
    id 324
    label "zwierz&#281;"
  ]
  node [
    id 325
    label "spoczywa&#263;"
  ]
  node [
    id 326
    label "podstawowy"
  ]
  node [
    id 327
    label "strategia"
  ]
  node [
    id 328
    label "pot&#281;ga"
  ]
  node [
    id 329
    label "zasadzenie"
  ]
  node [
    id 330
    label "za&#322;o&#380;enie"
  ]
  node [
    id 331
    label "&#347;ciana"
  ]
  node [
    id 332
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 333
    label "documentation"
  ]
  node [
    id 334
    label "dzieci&#281;ctwo"
  ]
  node [
    id 335
    label "pomys&#322;"
  ]
  node [
    id 336
    label "bok"
  ]
  node [
    id 337
    label "d&#243;&#322;"
  ]
  node [
    id 338
    label "punkt_odniesienia"
  ]
  node [
    id 339
    label "column"
  ]
  node [
    id 340
    label "zasadzi&#263;"
  ]
  node [
    id 341
    label "background"
  ]
  node [
    id 342
    label "jaki&#347;"
  ]
  node [
    id 343
    label "dokument"
  ]
  node [
    id 344
    label "resolution"
  ]
  node [
    id 345
    label "zdecydowanie"
  ]
  node [
    id 346
    label "wytw&#243;r"
  ]
  node [
    id 347
    label "podj&#281;cie_decyzji"
  ]
  node [
    id 348
    label "management"
  ]
  node [
    id 349
    label "splot"
  ]
  node [
    id 350
    label "matter"
  ]
  node [
    id 351
    label "forum"
  ]
  node [
    id 352
    label "fabu&#322;a"
  ]
  node [
    id 353
    label "rozmieszczenie"
  ]
  node [
    id 354
    label "ceg&#322;a"
  ]
  node [
    id 355
    label "socket"
  ]
  node [
    id 356
    label "topik"
  ]
  node [
    id 357
    label "podkre&#347;li&#263;"
  ]
  node [
    id 358
    label "zaokr&#261;gli&#263;"
  ]
  node [
    id 359
    label "try"
  ]
  node [
    id 360
    label "kolejny"
  ]
  node [
    id 361
    label "inaczej"
  ]
  node [
    id 362
    label "r&#243;&#380;ny"
  ]
  node [
    id 363
    label "inszy"
  ]
  node [
    id 364
    label "osobno"
  ]
  node [
    id 365
    label "nieistotnie"
  ]
  node [
    id 366
    label "nieznaczny"
  ]
  node [
    id 367
    label "ma&#322;owa&#380;ny"
  ]
  node [
    id 368
    label "s&#322;aby"
  ]
  node [
    id 369
    label "sta&#263;_si&#281;"
  ]
  node [
    id 370
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 371
    label "podj&#261;&#263;"
  ]
  node [
    id 372
    label "determine"
  ]
  node [
    id 373
    label "decide"
  ]
  node [
    id 374
    label "impart"
  ]
  node [
    id 375
    label "propagate"
  ]
  node [
    id 376
    label "transfer"
  ]
  node [
    id 377
    label "give"
  ]
  node [
    id 378
    label "wys&#322;a&#263;"
  ]
  node [
    id 379
    label "poda&#263;"
  ]
  node [
    id 380
    label "wp&#322;aci&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 157
  ]
  edge [
    source 7
    target 158
  ]
  edge [
    source 7
    target 159
  ]
  edge [
    source 7
    target 160
  ]
  edge [
    source 7
    target 161
  ]
  edge [
    source 7
    target 162
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 163
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 164
  ]
  edge [
    source 9
    target 165
  ]
  edge [
    source 9
    target 166
  ]
  edge [
    source 9
    target 167
  ]
  edge [
    source 9
    target 168
  ]
  edge [
    source 9
    target 169
  ]
  edge [
    source 9
    target 170
  ]
  edge [
    source 9
    target 171
  ]
  edge [
    source 9
    target 172
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 173
  ]
  edge [
    source 9
    target 174
  ]
  edge [
    source 9
    target 175
  ]
  edge [
    source 9
    target 176
  ]
  edge [
    source 9
    target 177
  ]
  edge [
    source 9
    target 178
  ]
  edge [
    source 9
    target 179
  ]
  edge [
    source 9
    target 15
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 180
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 181
  ]
  edge [
    source 11
    target 182
  ]
  edge [
    source 11
    target 183
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 184
  ]
  edge [
    source 12
    target 185
  ]
  edge [
    source 12
    target 186
  ]
  edge [
    source 12
    target 187
  ]
  edge [
    source 12
    target 188
  ]
  edge [
    source 12
    target 161
  ]
  edge [
    source 12
    target 189
  ]
  edge [
    source 12
    target 31
  ]
  edge [
    source 12
    target 190
  ]
  edge [
    source 12
    target 191
  ]
  edge [
    source 12
    target 192
  ]
  edge [
    source 12
    target 193
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 194
  ]
  edge [
    source 14
    target 195
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 196
  ]
  edge [
    source 15
    target 170
  ]
  edge [
    source 15
    target 197
  ]
  edge [
    source 15
    target 198
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 199
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 200
  ]
  edge [
    source 17
    target 201
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 53
  ]
  edge [
    source 18
    target 54
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 202
  ]
  edge [
    source 21
    target 203
  ]
  edge [
    source 21
    target 204
  ]
  edge [
    source 21
    target 205
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 116
  ]
  edge [
    source 22
    target 206
  ]
  edge [
    source 22
    target 207
  ]
  edge [
    source 22
    target 208
  ]
  edge [
    source 22
    target 209
  ]
  edge [
    source 22
    target 210
  ]
  edge [
    source 22
    target 211
  ]
  edge [
    source 22
    target 212
  ]
  edge [
    source 22
    target 213
  ]
  edge [
    source 22
    target 214
  ]
  edge [
    source 22
    target 215
  ]
  edge [
    source 22
    target 216
  ]
  edge [
    source 22
    target 217
  ]
  edge [
    source 22
    target 218
  ]
  edge [
    source 22
    target 219
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 49
  ]
  edge [
    source 23
    target 50
  ]
  edge [
    source 23
    target 220
  ]
  edge [
    source 23
    target 221
  ]
  edge [
    source 23
    target 222
  ]
  edge [
    source 23
    target 223
  ]
  edge [
    source 23
    target 224
  ]
  edge [
    source 23
    target 225
  ]
  edge [
    source 23
    target 226
  ]
  edge [
    source 23
    target 227
  ]
  edge [
    source 23
    target 228
  ]
  edge [
    source 23
    target 229
  ]
  edge [
    source 23
    target 230
  ]
  edge [
    source 23
    target 231
  ]
  edge [
    source 23
    target 232
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 233
  ]
  edge [
    source 24
    target 234
  ]
  edge [
    source 24
    target 235
  ]
  edge [
    source 24
    target 236
  ]
  edge [
    source 24
    target 237
  ]
  edge [
    source 24
    target 238
  ]
  edge [
    source 24
    target 239
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 240
  ]
  edge [
    source 25
    target 241
  ]
  edge [
    source 25
    target 242
  ]
  edge [
    source 25
    target 243
  ]
  edge [
    source 25
    target 244
  ]
  edge [
    source 25
    target 245
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 246
  ]
  edge [
    source 26
    target 247
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 44
  ]
  edge [
    source 27
    target 45
  ]
  edge [
    source 27
    target 248
  ]
  edge [
    source 27
    target 249
  ]
  edge [
    source 27
    target 250
  ]
  edge [
    source 27
    target 251
  ]
  edge [
    source 27
    target 252
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 253
  ]
  edge [
    source 28
    target 254
  ]
  edge [
    source 28
    target 255
  ]
  edge [
    source 28
    target 256
  ]
  edge [
    source 28
    target 257
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 258
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 259
  ]
  edge [
    source 31
    target 260
  ]
  edge [
    source 31
    target 200
  ]
  edge [
    source 31
    target 261
  ]
  edge [
    source 31
    target 262
  ]
  edge [
    source 31
    target 263
  ]
  edge [
    source 31
    target 264
  ]
  edge [
    source 31
    target 265
  ]
  edge [
    source 31
    target 266
  ]
  edge [
    source 31
    target 159
  ]
  edge [
    source 31
    target 267
  ]
  edge [
    source 31
    target 268
  ]
  edge [
    source 31
    target 269
  ]
  edge [
    source 31
    target 270
  ]
  edge [
    source 31
    target 271
  ]
  edge [
    source 31
    target 272
  ]
  edge [
    source 31
    target 273
  ]
  edge [
    source 31
    target 274
  ]
  edge [
    source 31
    target 275
  ]
  edge [
    source 31
    target 34
  ]
  edge [
    source 31
    target 276
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 277
  ]
  edge [
    source 33
    target 278
  ]
  edge [
    source 33
    target 279
  ]
  edge [
    source 33
    target 280
  ]
  edge [
    source 33
    target 281
  ]
  edge [
    source 33
    target 282
  ]
  edge [
    source 33
    target 283
  ]
  edge [
    source 33
    target 284
  ]
  edge [
    source 33
    target 285
  ]
  edge [
    source 33
    target 286
  ]
  edge [
    source 33
    target 287
  ]
  edge [
    source 33
    target 54
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 51
  ]
  edge [
    source 34
    target 52
  ]
  edge [
    source 34
    target 288
  ]
  edge [
    source 34
    target 289
  ]
  edge [
    source 34
    target 290
  ]
  edge [
    source 34
    target 291
  ]
  edge [
    source 34
    target 292
  ]
  edge [
    source 34
    target 293
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 294
  ]
  edge [
    source 35
    target 295
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 296
  ]
  edge [
    source 36
    target 297
  ]
  edge [
    source 36
    target 298
  ]
  edge [
    source 36
    target 299
  ]
  edge [
    source 36
    target 300
  ]
  edge [
    source 36
    target 301
  ]
  edge [
    source 36
    target 302
  ]
  edge [
    source 36
    target 303
  ]
  edge [
    source 36
    target 304
  ]
  edge [
    source 36
    target 305
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 306
  ]
  edge [
    source 37
    target 307
  ]
  edge [
    source 37
    target 308
  ]
  edge [
    source 37
    target 309
  ]
  edge [
    source 37
    target 310
  ]
  edge [
    source 37
    target 311
  ]
  edge [
    source 37
    target 312
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 240
  ]
  edge [
    source 40
    target 283
  ]
  edge [
    source 40
    target 313
  ]
  edge [
    source 40
    target 314
  ]
  edge [
    source 40
    target 315
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 316
  ]
  edge [
    source 41
    target 317
  ]
  edge [
    source 41
    target 318
  ]
  edge [
    source 41
    target 319
  ]
  edge [
    source 41
    target 320
  ]
  edge [
    source 41
    target 321
  ]
  edge [
    source 41
    target 322
  ]
  edge [
    source 41
    target 323
  ]
  edge [
    source 41
    target 324
  ]
  edge [
    source 41
    target 325
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 326
  ]
  edge [
    source 42
    target 327
  ]
  edge [
    source 42
    target 328
  ]
  edge [
    source 42
    target 329
  ]
  edge [
    source 42
    target 330
  ]
  edge [
    source 42
    target 331
  ]
  edge [
    source 42
    target 332
  ]
  edge [
    source 42
    target 283
  ]
  edge [
    source 42
    target 333
  ]
  edge [
    source 42
    target 334
  ]
  edge [
    source 42
    target 335
  ]
  edge [
    source 42
    target 336
  ]
  edge [
    source 42
    target 337
  ]
  edge [
    source 42
    target 338
  ]
  edge [
    source 42
    target 339
  ]
  edge [
    source 42
    target 340
  ]
  edge [
    source 42
    target 341
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 294
  ]
  edge [
    source 43
    target 342
  ]
  edge [
    source 44
    target 343
  ]
  edge [
    source 44
    target 344
  ]
  edge [
    source 44
    target 345
  ]
  edge [
    source 44
    target 346
  ]
  edge [
    source 44
    target 347
  ]
  edge [
    source 44
    target 348
  ]
  edge [
    source 44
    target 52
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 349
  ]
  edge [
    source 48
    target 346
  ]
  edge [
    source 48
    target 296
  ]
  edge [
    source 48
    target 350
  ]
  edge [
    source 48
    target 351
  ]
  edge [
    source 48
    target 352
  ]
  edge [
    source 48
    target 353
  ]
  edge [
    source 48
    target 354
  ]
  edge [
    source 48
    target 355
  ]
  edge [
    source 48
    target 356
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 357
  ]
  edge [
    source 50
    target 358
  ]
  edge [
    source 50
    target 359
  ]
  edge [
    source 51
    target 360
  ]
  edge [
    source 51
    target 361
  ]
  edge [
    source 51
    target 362
  ]
  edge [
    source 51
    target 363
  ]
  edge [
    source 51
    target 364
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 365
  ]
  edge [
    source 52
    target 366
  ]
  edge [
    source 52
    target 367
  ]
  edge [
    source 52
    target 368
  ]
  edge [
    source 53
    target 369
  ]
  edge [
    source 53
    target 370
  ]
  edge [
    source 53
    target 371
  ]
  edge [
    source 53
    target 372
  ]
  edge [
    source 53
    target 373
  ]
  edge [
    source 53
    target 272
  ]
  edge [
    source 54
    target 374
  ]
  edge [
    source 54
    target 224
  ]
  edge [
    source 54
    target 375
  ]
  edge [
    source 54
    target 376
  ]
  edge [
    source 54
    target 377
  ]
  edge [
    source 54
    target 378
  ]
  edge [
    source 54
    target 272
  ]
  edge [
    source 54
    target 379
  ]
  edge [
    source 54
    target 380
  ]
]
