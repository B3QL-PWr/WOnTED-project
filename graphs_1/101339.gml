graph [
  maxDegree 35
  minDegree 1
  meanDegree 1.9444444444444444
  density 0.05555555555555555
  graphCliqueNumber 2
  node [
    id 0
    label "kurs"
    origin "text"
  ]
  node [
    id 1
    label "walutowy"
    origin "text"
  ]
  node [
    id 2
    label "seria"
  ]
  node [
    id 3
    label "stawka"
  ]
  node [
    id 4
    label "course"
  ]
  node [
    id 5
    label "way"
  ]
  node [
    id 6
    label "zaj&#281;cia"
  ]
  node [
    id 7
    label "grupa"
  ]
  node [
    id 8
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 9
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 10
    label "rok"
  ]
  node [
    id 11
    label "Lira"
  ]
  node [
    id 12
    label "cedu&#322;a"
  ]
  node [
    id 13
    label "zwy&#380;kowanie"
  ]
  node [
    id 14
    label "spos&#243;b"
  ]
  node [
    id 15
    label "passage"
  ]
  node [
    id 16
    label "deprecjacja"
  ]
  node [
    id 17
    label "przejazd"
  ]
  node [
    id 18
    label "drive"
  ]
  node [
    id 19
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 20
    label "bearing"
  ]
  node [
    id 21
    label "przeorientowywa&#263;"
  ]
  node [
    id 22
    label "trasa"
  ]
  node [
    id 23
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 24
    label "manner"
  ]
  node [
    id 25
    label "nauka"
  ]
  node [
    id 26
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 27
    label "przeorientowa&#263;"
  ]
  node [
    id 28
    label "kierunek"
  ]
  node [
    id 29
    label "przeorientowanie"
  ]
  node [
    id 30
    label "zni&#380;kowanie"
  ]
  node [
    id 31
    label "przeorientowywanie"
  ]
  node [
    id 32
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 33
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 34
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 35
    label "klasa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
]
