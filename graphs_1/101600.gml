graph [
  maxDegree 14
  minDegree 1
  meanDegree 1.9615384615384615
  density 0.038461538461538464
  graphCliqueNumber 2
  node [
    id 0
    label "gdyby"
    origin "text"
  ]
  node [
    id 1
    label "m&#261;&#380;"
    origin "text"
  ]
  node [
    id 2
    label "tw&#243;j"
    origin "text"
  ]
  node [
    id 3
    label "by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "minister"
    origin "text"
  ]
  node [
    id 5
    label "wi&#281;zienie"
    origin "text"
  ]
  node [
    id 6
    label "wyrok"
    origin "text"
  ]
  node [
    id 7
    label "pan_domu"
  ]
  node [
    id 8
    label "cz&#322;owiek"
  ]
  node [
    id 9
    label "ch&#322;op"
  ]
  node [
    id 10
    label "ma&#322;&#380;onek"
  ]
  node [
    id 11
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 12
    label "stary"
  ]
  node [
    id 13
    label "&#347;lubny"
  ]
  node [
    id 14
    label "m&#243;j"
  ]
  node [
    id 15
    label "pan_i_w&#322;adca"
  ]
  node [
    id 16
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 17
    label "pan_m&#322;ody"
  ]
  node [
    id 18
    label "czyj&#347;"
  ]
  node [
    id 19
    label "si&#281;ga&#263;"
  ]
  node [
    id 20
    label "trwa&#263;"
  ]
  node [
    id 21
    label "obecno&#347;&#263;"
  ]
  node [
    id 22
    label "stan"
  ]
  node [
    id 23
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 24
    label "stand"
  ]
  node [
    id 25
    label "mie&#263;_miejsce"
  ]
  node [
    id 26
    label "uczestniczy&#263;"
  ]
  node [
    id 27
    label "chodzi&#263;"
  ]
  node [
    id 28
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 29
    label "equal"
  ]
  node [
    id 30
    label "Goebbels"
  ]
  node [
    id 31
    label "Sto&#322;ypin"
  ]
  node [
    id 32
    label "rz&#261;d"
  ]
  node [
    id 33
    label "dostojnik"
  ]
  node [
    id 34
    label "pozbawienie_wolno&#347;ci"
  ]
  node [
    id 35
    label "ogranicza&#263;"
  ]
  node [
    id 36
    label "uniemo&#380;liwianie"
  ]
  node [
    id 37
    label "Butyrki"
  ]
  node [
    id 38
    label "ciupa"
  ]
  node [
    id 39
    label "przerwa_w_&#380;yciorysie"
  ]
  node [
    id 40
    label "reedukator"
  ]
  node [
    id 41
    label "miejsce_odosobnienia"
  ]
  node [
    id 42
    label "&#321;ubianka"
  ]
  node [
    id 43
    label "pierdel"
  ]
  node [
    id 44
    label "imprisonment"
  ]
  node [
    id 45
    label "sytuacja"
  ]
  node [
    id 46
    label "orzeczenie"
  ]
  node [
    id 47
    label "order"
  ]
  node [
    id 48
    label "wydarzenie"
  ]
  node [
    id 49
    label "kara"
  ]
  node [
    id 50
    label "judgment"
  ]
  node [
    id 51
    label "sentencja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
]
