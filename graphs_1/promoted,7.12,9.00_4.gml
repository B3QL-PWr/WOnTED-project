graph [
  maxDegree 38
  minDegree 1
  meanDegree 2
  density 0.02666666666666667
  graphCliqueNumber 3
  node [
    id 0
    label "czas"
    origin "text"
  ]
  node [
    id 1
    label "pierwsza"
    origin "text"
  ]
  node [
    id 2
    label "wojna"
    origin "text"
  ]
  node [
    id 3
    label "&#347;wiatowy"
    origin "text"
  ]
  node [
    id 4
    label "imperium"
    origin "text"
  ]
  node [
    id 5
    label "osma&#324;ski"
    origin "text"
  ]
  node [
    id 6
    label "pope&#322;ni&#263;"
    origin "text"
  ]
  node [
    id 7
    label "straszliwy"
    origin "text"
  ]
  node [
    id 8
    label "zbrodnia"
    origin "text"
  ]
  node [
    id 9
    label "czasokres"
  ]
  node [
    id 10
    label "trawienie"
  ]
  node [
    id 11
    label "kategoria_gramatyczna"
  ]
  node [
    id 12
    label "period"
  ]
  node [
    id 13
    label "odczyt"
  ]
  node [
    id 14
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 15
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 16
    label "chwila"
  ]
  node [
    id 17
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 18
    label "poprzedzenie"
  ]
  node [
    id 19
    label "koniugacja"
  ]
  node [
    id 20
    label "dzieje"
  ]
  node [
    id 21
    label "poprzedzi&#263;"
  ]
  node [
    id 22
    label "przep&#322;ywanie"
  ]
  node [
    id 23
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 24
    label "odwlekanie_si&#281;"
  ]
  node [
    id 25
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 26
    label "Zeitgeist"
  ]
  node [
    id 27
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 28
    label "okres_czasu"
  ]
  node [
    id 29
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 30
    label "pochodzi&#263;"
  ]
  node [
    id 31
    label "schy&#322;ek"
  ]
  node [
    id 32
    label "czwarty_wymiar"
  ]
  node [
    id 33
    label "chronometria"
  ]
  node [
    id 34
    label "poprzedzanie"
  ]
  node [
    id 35
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 36
    label "pogoda"
  ]
  node [
    id 37
    label "zegar"
  ]
  node [
    id 38
    label "trawi&#263;"
  ]
  node [
    id 39
    label "pochodzenie"
  ]
  node [
    id 40
    label "poprzedza&#263;"
  ]
  node [
    id 41
    label "time_period"
  ]
  node [
    id 42
    label "rachuba_czasu"
  ]
  node [
    id 43
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 44
    label "czasoprzestrze&#324;"
  ]
  node [
    id 45
    label "laba"
  ]
  node [
    id 46
    label "godzina"
  ]
  node [
    id 47
    label "zimna_wojna"
  ]
  node [
    id 48
    label "zbrodnia_przeciw_ludzko&#347;ci"
  ]
  node [
    id 49
    label "angaria"
  ]
  node [
    id 50
    label "wr&#243;g"
  ]
  node [
    id 51
    label "zbrodnia_przeciw_pokojowi"
  ]
  node [
    id 52
    label "walka"
  ]
  node [
    id 53
    label "war"
  ]
  node [
    id 54
    label "konflikt"
  ]
  node [
    id 55
    label "wojna_stuletnia"
  ]
  node [
    id 56
    label "burza"
  ]
  node [
    id 57
    label "zbrodnia_wojenna"
  ]
  node [
    id 58
    label "gra_w_karty"
  ]
  node [
    id 59
    label "sp&#243;r"
  ]
  node [
    id 60
    label "kulturalny"
  ]
  node [
    id 61
    label "&#347;wiatowo"
  ]
  node [
    id 62
    label "generalny"
  ]
  node [
    id 63
    label "pa&#324;stwo"
  ]
  node [
    id 64
    label "Imperium_Osma&#324;skie"
  ]
  node [
    id 65
    label "pot&#281;ga"
  ]
  node [
    id 66
    label "zrobi&#263;"
  ]
  node [
    id 67
    label "stworzy&#263;"
  ]
  node [
    id 68
    label "strasznie"
  ]
  node [
    id 69
    label "straszny"
  ]
  node [
    id 70
    label "straszliwie"
  ]
  node [
    id 71
    label "kurewski"
  ]
  node [
    id 72
    label "olbrzymi"
  ]
  node [
    id 73
    label "crime"
  ]
  node [
    id 74
    label "przest&#281;pstwo"
  ]
  node [
    id 75
    label "post&#281;pek"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
]
