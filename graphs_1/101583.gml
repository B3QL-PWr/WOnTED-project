graph [
  maxDegree 324
  minDegree 1
  meanDegree 2.293304994686504
  density 0.0024396861645601103
  graphCliqueNumber 7
  node [
    id 0
    label "pan"
    origin "text"
  ]
  node [
    id 1
    label "marsza&#322;ek"
    origin "text"
  ]
  node [
    id 2
    label "premier"
    origin "text"
  ]
  node [
    id 3
    label "wysoki"
    origin "text"
  ]
  node [
    id 4
    label "izba"
    origin "text"
  ]
  node [
    id 5
    label "przez"
    origin "text"
  ]
  node [
    id 6
    label "polska"
    origin "text"
  ]
  node [
    id 7
    label "przetacza&#263;"
    origin "text"
  ]
  node [
    id 8
    label "si&#281;"
    origin "text"
  ]
  node [
    id 9
    label "dzisiaj"
    origin "text"
  ]
  node [
    id 10
    label "fala"
    origin "text"
  ]
  node [
    id 11
    label "pow&#243;d&#378;"
    origin "text"
  ]
  node [
    id 12
    label "wraz"
    origin "text"
  ]
  node [
    id 13
    label "rozpacz"
    origin "text"
  ]
  node [
    id 14
    label "ludzki"
    origin "text"
  ]
  node [
    id 15
    label "tragedia"
    origin "text"
  ]
  node [
    id 16
    label "nasz"
    origin "text"
  ]
  node [
    id 17
    label "rola"
    origin "text"
  ]
  node [
    id 18
    label "nasa"
    origin "text"
  ]
  node [
    id 19
    label "wszyscy"
    origin "text"
  ]
  node [
    id 20
    label "przed"
    origin "text"
  ]
  node [
    id 21
    label "polski"
    origin "text"
  ]
  node [
    id 22
    label "parlament"
    origin "text"
  ]
  node [
    id 23
    label "rz&#261;d"
    origin "text"
  ]
  node [
    id 24
    label "by&#263;"
    origin "text"
  ]
  node [
    id 25
    label "ten"
    origin "text"
  ]
  node [
    id 26
    label "powstrzyma&#263;"
    origin "text"
  ]
  node [
    id 27
    label "odbudowa&#263;"
    origin "text"
  ]
  node [
    id 28
    label "nadzieja"
    origin "text"
  ]
  node [
    id 29
    label "polak"
    origin "text"
  ]
  node [
    id 30
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 31
    label "swoje"
    origin "text"
  ]
  node [
    id 32
    label "kraj"
    origin "text"
  ]
  node [
    id 33
    label "bezpieczny"
    origin "text"
  ]
  node [
    id 34
    label "konieczna"
    origin "text"
  ]
  node [
    id 35
    label "bardzo"
    origin "text"
  ]
  node [
    id 36
    label "szybki"
    origin "text"
  ]
  node [
    id 37
    label "podj&#281;cie"
    origin "text"
  ]
  node [
    id 38
    label "taki"
    origin "text"
  ]
  node [
    id 39
    label "dzia&#322;ania"
    origin "text"
  ]
  node [
    id 40
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 41
    label "przysz&#322;o&#347;&#263;"
    origin "text"
  ]
  node [
    id 42
    label "pozwoli&#263;"
    origin "text"
  ]
  node [
    id 43
    label "rozmiar"
    origin "text"
  ]
  node [
    id 44
    label "unikn&#261;&#263;"
    origin "text"
  ]
  node [
    id 45
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 46
    label "laska"
    origin "text"
  ]
  node [
    id 47
    label "marsza&#322;kowski"
    origin "text"
  ]
  node [
    id 48
    label "z&#322;o&#380;one"
    origin "text"
  ]
  node [
    id 49
    label "projekt"
    origin "text"
  ]
  node [
    id 50
    label "ustawa"
    origin "text"
  ]
  node [
    id 51
    label "raz"
    origin "text"
  ]
  node [
    id 52
    label "procedowane"
    origin "text"
  ]
  node [
    id 53
    label "chocia&#380;by"
    origin "text"
  ]
  node [
    id 54
    label "fundusz"
    origin "text"
  ]
  node [
    id 55
    label "pomoc"
    origin "text"
  ]
  node [
    id 56
    label "ofiara"
    origin "text"
  ]
  node [
    id 57
    label "kl&#281;ska"
    origin "text"
  ]
  node [
    id 58
    label "&#380;ywio&#322;owy"
    origin "text"
  ]
  node [
    id 59
    label "z&#322;o&#380;ony"
    origin "text"
  ]
  node [
    id 60
    label "klub"
    origin "text"
  ]
  node [
    id 61
    label "prawa"
    origin "text"
  ]
  node [
    id 62
    label "pro&#347;ba"
    origin "text"
  ]
  node [
    id 63
    label "z&#322;o&#380;y&#263;"
    origin "text"
  ]
  node [
    id 64
    label "pose&#322;"
    origin "text"
  ]
  node [
    id 65
    label "platforma"
    origin "text"
  ]
  node [
    id 66
    label "obywatelski"
    origin "text"
  ]
  node [
    id 67
    label "lewica"
    origin "text"
  ]
  node [
    id 68
    label "demokratyczny"
    origin "text"
  ]
  node [
    id 69
    label "stronnictwo"
    origin "text"
  ]
  node [
    id 70
    label "ludowy"
    origin "text"
  ]
  node [
    id 71
    label "je&#380;eli"
    origin "text"
  ]
  node [
    id 72
    label "dobry"
    origin "text"
  ]
  node [
    id 73
    label "pom&#243;c"
    origin "text"
  ]
  node [
    id 74
    label "procedowa&#263;"
    origin "text"
  ]
  node [
    id 75
    label "parlamentarzysta"
    origin "text"
  ]
  node [
    id 76
    label "dla"
    origin "text"
  ]
  node [
    id 77
    label "oklask"
    origin "text"
  ]
  node [
    id 78
    label "przygotowa&#263;"
    origin "text"
  ]
  node [
    id 79
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 80
    label "usuwa&#263;"
    origin "text"
  ]
  node [
    id 81
    label "skutek"
    origin "text"
  ]
  node [
    id 82
    label "laski"
    origin "text"
  ]
  node [
    id 83
    label "kolejny"
    origin "text"
  ]
  node [
    id 84
    label "rozwi&#261;zanie"
    origin "text"
  ]
  node [
    id 85
    label "cz&#322;owiek"
  ]
  node [
    id 86
    label "profesor"
  ]
  node [
    id 87
    label "kszta&#322;ciciel"
  ]
  node [
    id 88
    label "jegomo&#347;&#263;"
  ]
  node [
    id 89
    label "zwrot"
  ]
  node [
    id 90
    label "pracodawca"
  ]
  node [
    id 91
    label "rz&#261;dzenie"
  ]
  node [
    id 92
    label "m&#261;&#380;"
  ]
  node [
    id 93
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 94
    label "ch&#322;opina"
  ]
  node [
    id 95
    label "bratek"
  ]
  node [
    id 96
    label "opiekun"
  ]
  node [
    id 97
    label "doros&#322;y"
  ]
  node [
    id 98
    label "preceptor"
  ]
  node [
    id 99
    label "Midas"
  ]
  node [
    id 100
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 101
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 102
    label "murza"
  ]
  node [
    id 103
    label "ojciec"
  ]
  node [
    id 104
    label "androlog"
  ]
  node [
    id 105
    label "pupil"
  ]
  node [
    id 106
    label "efendi"
  ]
  node [
    id 107
    label "nabab"
  ]
  node [
    id 108
    label "w&#322;odarz"
  ]
  node [
    id 109
    label "szkolnik"
  ]
  node [
    id 110
    label "pedagog"
  ]
  node [
    id 111
    label "popularyzator"
  ]
  node [
    id 112
    label "andropauza"
  ]
  node [
    id 113
    label "gra_w_karty"
  ]
  node [
    id 114
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 115
    label "Mieszko_I"
  ]
  node [
    id 116
    label "bogaty"
  ]
  node [
    id 117
    label "samiec"
  ]
  node [
    id 118
    label "przyw&#243;dca"
  ]
  node [
    id 119
    label "pa&#324;stwo"
  ]
  node [
    id 120
    label "belfer"
  ]
  node [
    id 121
    label "stopie&#324;_wojskowy"
  ]
  node [
    id 122
    label "dostojnik"
  ]
  node [
    id 123
    label "oficer"
  ]
  node [
    id 124
    label "Pi&#322;sudski"
  ]
  node [
    id 125
    label "Jelcyn"
  ]
  node [
    id 126
    label "Sto&#322;ypin"
  ]
  node [
    id 127
    label "Chruszczow"
  ]
  node [
    id 128
    label "Miko&#322;ajczyk"
  ]
  node [
    id 129
    label "Bismarck"
  ]
  node [
    id 130
    label "zwierzchnik"
  ]
  node [
    id 131
    label "warto&#347;ciowy"
  ]
  node [
    id 132
    label "du&#380;y"
  ]
  node [
    id 133
    label "wysoce"
  ]
  node [
    id 134
    label "daleki"
  ]
  node [
    id 135
    label "znaczny"
  ]
  node [
    id 136
    label "wysoko"
  ]
  node [
    id 137
    label "szczytnie"
  ]
  node [
    id 138
    label "wznios&#322;y"
  ]
  node [
    id 139
    label "wyrafinowany"
  ]
  node [
    id 140
    label "z_wysoka"
  ]
  node [
    id 141
    label "chwalebny"
  ]
  node [
    id 142
    label "uprzywilejowany"
  ]
  node [
    id 143
    label "niepo&#347;ledni"
  ]
  node [
    id 144
    label "pok&#243;j"
  ]
  node [
    id 145
    label "zwi&#261;zek"
  ]
  node [
    id 146
    label "NIK"
  ]
  node [
    id 147
    label "urz&#261;d"
  ]
  node [
    id 148
    label "organ"
  ]
  node [
    id 149
    label "pomieszczenie"
  ]
  node [
    id 150
    label "zmienia&#263;"
  ]
  node [
    id 151
    label "obrabia&#263;"
  ]
  node [
    id 152
    label "przemieszcza&#263;"
  ]
  node [
    id 153
    label "przelewa&#263;"
  ]
  node [
    id 154
    label "toczy&#263;"
  ]
  node [
    id 155
    label "grind"
  ]
  node [
    id 156
    label "przesuwa&#263;"
  ]
  node [
    id 157
    label "doba"
  ]
  node [
    id 158
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 159
    label "dzi&#347;"
  ]
  node [
    id 160
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 161
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 162
    label "reakcja"
  ]
  node [
    id 163
    label "zafalowanie"
  ]
  node [
    id 164
    label "czo&#322;o_fali"
  ]
  node [
    id 165
    label "rozbijanie_si&#281;"
  ]
  node [
    id 166
    label "zjawisko"
  ]
  node [
    id 167
    label "clutter"
  ]
  node [
    id 168
    label "pasemko"
  ]
  node [
    id 169
    label "grzywa_fali"
  ]
  node [
    id 170
    label "t&#322;um"
  ]
  node [
    id 171
    label "przemoc"
  ]
  node [
    id 172
    label "kot"
  ]
  node [
    id 173
    label "mn&#243;stwo"
  ]
  node [
    id 174
    label "wojsko"
  ]
  node [
    id 175
    label "efekt_Dopplera"
  ]
  node [
    id 176
    label "kszta&#322;t"
  ]
  node [
    id 177
    label "obcinka"
  ]
  node [
    id 178
    label "stream"
  ]
  node [
    id 179
    label "fit"
  ]
  node [
    id 180
    label "zafalowa&#263;"
  ]
  node [
    id 181
    label "karb"
  ]
  node [
    id 182
    label "rozbicie_si&#281;"
  ]
  node [
    id 183
    label "znak_diakrytyczny"
  ]
  node [
    id 184
    label "okres"
  ]
  node [
    id 185
    label "woda"
  ]
  node [
    id 186
    label "strumie&#324;"
  ]
  node [
    id 187
    label "wezbranie"
  ]
  node [
    id 188
    label "kl&#281;ska_&#380;ywio&#322;owa"
  ]
  node [
    id 189
    label "emocja"
  ]
  node [
    id 190
    label "dezolacja"
  ]
  node [
    id 191
    label "empatyczny"
  ]
  node [
    id 192
    label "naturalny"
  ]
  node [
    id 193
    label "prawdziwy"
  ]
  node [
    id 194
    label "ludzko"
  ]
  node [
    id 195
    label "po_ludzku"
  ]
  node [
    id 196
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 197
    label "normalny"
  ]
  node [
    id 198
    label "przyzwoity"
  ]
  node [
    id 199
    label "lipa"
  ]
  node [
    id 200
    label "cios"
  ]
  node [
    id 201
    label "dramat"
  ]
  node [
    id 202
    label "gatunek_literacki"
  ]
  node [
    id 203
    label "play"
  ]
  node [
    id 204
    label "czyj&#347;"
  ]
  node [
    id 205
    label "pole"
  ]
  node [
    id 206
    label "znaczenie"
  ]
  node [
    id 207
    label "ziemia"
  ]
  node [
    id 208
    label "sk&#322;ad"
  ]
  node [
    id 209
    label "zastosowanie"
  ]
  node [
    id 210
    label "zreinterpretowa&#263;"
  ]
  node [
    id 211
    label "zreinterpretowanie"
  ]
  node [
    id 212
    label "function"
  ]
  node [
    id 213
    label "zagranie"
  ]
  node [
    id 214
    label "p&#322;osa"
  ]
  node [
    id 215
    label "plik"
  ]
  node [
    id 216
    label "cel"
  ]
  node [
    id 217
    label "reinterpretowanie"
  ]
  node [
    id 218
    label "tekst"
  ]
  node [
    id 219
    label "wykonywa&#263;"
  ]
  node [
    id 220
    label "uprawi&#263;"
  ]
  node [
    id 221
    label "uprawienie"
  ]
  node [
    id 222
    label "gra&#263;"
  ]
  node [
    id 223
    label "radlina"
  ]
  node [
    id 224
    label "ustawi&#263;"
  ]
  node [
    id 225
    label "irygowa&#263;"
  ]
  node [
    id 226
    label "wrench"
  ]
  node [
    id 227
    label "irygowanie"
  ]
  node [
    id 228
    label "dialog"
  ]
  node [
    id 229
    label "zagon"
  ]
  node [
    id 230
    label "scenariusz"
  ]
  node [
    id 231
    label "zagra&#263;"
  ]
  node [
    id 232
    label "pierwszoplanowo&#347;&#263;"
  ]
  node [
    id 233
    label "ustawienie"
  ]
  node [
    id 234
    label "czyn"
  ]
  node [
    id 235
    label "gospodarstwo"
  ]
  node [
    id 236
    label "reinterpretowa&#263;"
  ]
  node [
    id 237
    label "granie"
  ]
  node [
    id 238
    label "wykonywanie"
  ]
  node [
    id 239
    label "kostium"
  ]
  node [
    id 240
    label "aktorstwo"
  ]
  node [
    id 241
    label "posta&#263;"
  ]
  node [
    id 242
    label "lacki"
  ]
  node [
    id 243
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 244
    label "przedmiot"
  ]
  node [
    id 245
    label "sztajer"
  ]
  node [
    id 246
    label "drabant"
  ]
  node [
    id 247
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 248
    label "pierogi_ruskie"
  ]
  node [
    id 249
    label "krakowiak"
  ]
  node [
    id 250
    label "Polish"
  ]
  node [
    id 251
    label "j&#281;zyk"
  ]
  node [
    id 252
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 253
    label "oberek"
  ]
  node [
    id 254
    label "po_polsku"
  ]
  node [
    id 255
    label "mazur"
  ]
  node [
    id 256
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 257
    label "chodzony"
  ]
  node [
    id 258
    label "skoczny"
  ]
  node [
    id 259
    label "ryba_po_grecku"
  ]
  node [
    id 260
    label "goniony"
  ]
  node [
    id 261
    label "polsko"
  ]
  node [
    id 262
    label "grupa"
  ]
  node [
    id 263
    label "plankton_polityczny"
  ]
  node [
    id 264
    label "ustawodawca"
  ]
  node [
    id 265
    label "europarlament"
  ]
  node [
    id 266
    label "grupa_bilateralna"
  ]
  node [
    id 267
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 268
    label "kategoria"
  ]
  node [
    id 269
    label "egzekutywa"
  ]
  node [
    id 270
    label "gabinet_cieni"
  ]
  node [
    id 271
    label "gromada"
  ]
  node [
    id 272
    label "Londyn"
  ]
  node [
    id 273
    label "Konsulat"
  ]
  node [
    id 274
    label "uporz&#261;dkowanie"
  ]
  node [
    id 275
    label "jednostka_systematyczna"
  ]
  node [
    id 276
    label "szpaler"
  ]
  node [
    id 277
    label "przybli&#380;enie"
  ]
  node [
    id 278
    label "tract"
  ]
  node [
    id 279
    label "number"
  ]
  node [
    id 280
    label "lon&#380;a"
  ]
  node [
    id 281
    label "w&#322;adza"
  ]
  node [
    id 282
    label "instytucja"
  ]
  node [
    id 283
    label "klasa"
  ]
  node [
    id 284
    label "si&#281;ga&#263;"
  ]
  node [
    id 285
    label "trwa&#263;"
  ]
  node [
    id 286
    label "obecno&#347;&#263;"
  ]
  node [
    id 287
    label "stan"
  ]
  node [
    id 288
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 289
    label "stand"
  ]
  node [
    id 290
    label "mie&#263;_miejsce"
  ]
  node [
    id 291
    label "uczestniczy&#263;"
  ]
  node [
    id 292
    label "chodzi&#263;"
  ]
  node [
    id 293
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 294
    label "equal"
  ]
  node [
    id 295
    label "okre&#347;lony"
  ]
  node [
    id 296
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 297
    label "continue"
  ]
  node [
    id 298
    label "zreflektowa&#263;"
  ]
  node [
    id 299
    label "spowodowa&#263;"
  ]
  node [
    id 300
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 301
    label "zaczepi&#263;"
  ]
  node [
    id 302
    label "anticipate"
  ]
  node [
    id 303
    label "przywr&#243;ci&#263;"
  ]
  node [
    id 304
    label "odtworzy&#263;"
  ]
  node [
    id 305
    label "rebuild"
  ]
  node [
    id 306
    label "&#347;wiate&#322;ko_w_tunelu"
  ]
  node [
    id 307
    label "wierzy&#263;"
  ]
  node [
    id 308
    label "szansa"
  ]
  node [
    id 309
    label "oczekiwanie"
  ]
  node [
    id 310
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 311
    label "spoczywa&#263;"
  ]
  node [
    id 312
    label "gotowy"
  ]
  node [
    id 313
    label "might"
  ]
  node [
    id 314
    label "Skandynawia"
  ]
  node [
    id 315
    label "Filipiny"
  ]
  node [
    id 316
    label "Rwanda"
  ]
  node [
    id 317
    label "Kaukaz"
  ]
  node [
    id 318
    label "Kaszmir"
  ]
  node [
    id 319
    label "Toskania"
  ]
  node [
    id 320
    label "Yorkshire"
  ]
  node [
    id 321
    label "&#321;emkowszczyzna"
  ]
  node [
    id 322
    label "obszar"
  ]
  node [
    id 323
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 324
    label "Monako"
  ]
  node [
    id 325
    label "Amhara"
  ]
  node [
    id 326
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 327
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 328
    label "Lombardia"
  ]
  node [
    id 329
    label "Korea"
  ]
  node [
    id 330
    label "Kalabria"
  ]
  node [
    id 331
    label "Ghana"
  ]
  node [
    id 332
    label "Czarnog&#243;ra"
  ]
  node [
    id 333
    label "Tyrol"
  ]
  node [
    id 334
    label "Malawi"
  ]
  node [
    id 335
    label "Indonezja"
  ]
  node [
    id 336
    label "Bu&#322;garia"
  ]
  node [
    id 337
    label "Nauru"
  ]
  node [
    id 338
    label "Kenia"
  ]
  node [
    id 339
    label "Pamir"
  ]
  node [
    id 340
    label "Kambod&#380;a"
  ]
  node [
    id 341
    label "Lubelszczyzna"
  ]
  node [
    id 342
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 343
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 344
    label "Mali"
  ]
  node [
    id 345
    label "&#379;ywiecczyzna"
  ]
  node [
    id 346
    label "Austria"
  ]
  node [
    id 347
    label "interior"
  ]
  node [
    id 348
    label "Europa_Wschodnia"
  ]
  node [
    id 349
    label "Armenia"
  ]
  node [
    id 350
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 351
    label "Fid&#380;i"
  ]
  node [
    id 352
    label "Tuwalu"
  ]
  node [
    id 353
    label "Zabajkale"
  ]
  node [
    id 354
    label "Etiopia"
  ]
  node [
    id 355
    label "Malta"
  ]
  node [
    id 356
    label "Malezja"
  ]
  node [
    id 357
    label "Kaszuby"
  ]
  node [
    id 358
    label "Bo&#347;nia"
  ]
  node [
    id 359
    label "Noworosja"
  ]
  node [
    id 360
    label "Grenada"
  ]
  node [
    id 361
    label "Tad&#380;ykistan"
  ]
  node [
    id 362
    label "Ba&#322;kany"
  ]
  node [
    id 363
    label "Wehrlen"
  ]
  node [
    id 364
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 365
    label "Anglia"
  ]
  node [
    id 366
    label "Kielecczyzna"
  ]
  node [
    id 367
    label "Rumunia"
  ]
  node [
    id 368
    label "Pomorze_Zachodnie"
  ]
  node [
    id 369
    label "Maroko"
  ]
  node [
    id 370
    label "Bhutan"
  ]
  node [
    id 371
    label "Opolskie"
  ]
  node [
    id 372
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 373
    label "Ko&#322;yma"
  ]
  node [
    id 374
    label "Oksytania"
  ]
  node [
    id 375
    label "S&#322;owacja"
  ]
  node [
    id 376
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 377
    label "Seszele"
  ]
  node [
    id 378
    label "Syjon"
  ]
  node [
    id 379
    label "Kuwejt"
  ]
  node [
    id 380
    label "Arabia_Saudyjska"
  ]
  node [
    id 381
    label "Kociewie"
  ]
  node [
    id 382
    label "Ekwador"
  ]
  node [
    id 383
    label "Kanada"
  ]
  node [
    id 384
    label "Japonia"
  ]
  node [
    id 385
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 386
    label "Hiszpania"
  ]
  node [
    id 387
    label "Wyspy_Marshalla"
  ]
  node [
    id 388
    label "Botswana"
  ]
  node [
    id 389
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 390
    label "D&#380;ibuti"
  ]
  node [
    id 391
    label "Huculszczyzna"
  ]
  node [
    id 392
    label "Wietnam"
  ]
  node [
    id 393
    label "Egipt"
  ]
  node [
    id 394
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 395
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 396
    label "Burkina_Faso"
  ]
  node [
    id 397
    label "Bawaria"
  ]
  node [
    id 398
    label "Niemcy"
  ]
  node [
    id 399
    label "Khitai"
  ]
  node [
    id 400
    label "Macedonia"
  ]
  node [
    id 401
    label "Albania"
  ]
  node [
    id 402
    label "Madagaskar"
  ]
  node [
    id 403
    label "Bahrajn"
  ]
  node [
    id 404
    label "Jemen"
  ]
  node [
    id 405
    label "Lesoto"
  ]
  node [
    id 406
    label "Maghreb"
  ]
  node [
    id 407
    label "Samoa"
  ]
  node [
    id 408
    label "Andora"
  ]
  node [
    id 409
    label "Bory_Tucholskie"
  ]
  node [
    id 410
    label "Chiny"
  ]
  node [
    id 411
    label "Europa_Zachodnia"
  ]
  node [
    id 412
    label "Cypr"
  ]
  node [
    id 413
    label "Wielka_Brytania"
  ]
  node [
    id 414
    label "Kerala"
  ]
  node [
    id 415
    label "Podhale"
  ]
  node [
    id 416
    label "Kabylia"
  ]
  node [
    id 417
    label "Ukraina"
  ]
  node [
    id 418
    label "Paragwaj"
  ]
  node [
    id 419
    label "Trynidad_i_Tobago"
  ]
  node [
    id 420
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 421
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 422
    label "Ma&#322;opolska"
  ]
  node [
    id 423
    label "Polesie"
  ]
  node [
    id 424
    label "Liguria"
  ]
  node [
    id 425
    label "Libia"
  ]
  node [
    id 426
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 427
    label "&#321;&#243;dzkie"
  ]
  node [
    id 428
    label "Surinam"
  ]
  node [
    id 429
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 430
    label "Palestyna"
  ]
  node [
    id 431
    label "Australia"
  ]
  node [
    id 432
    label "Nigeria"
  ]
  node [
    id 433
    label "Honduras"
  ]
  node [
    id 434
    label "Bojkowszczyzna"
  ]
  node [
    id 435
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 436
    label "Karaiby"
  ]
  node [
    id 437
    label "Bangladesz"
  ]
  node [
    id 438
    label "Peru"
  ]
  node [
    id 439
    label "Kazachstan"
  ]
  node [
    id 440
    label "USA"
  ]
  node [
    id 441
    label "Irak"
  ]
  node [
    id 442
    label "Nepal"
  ]
  node [
    id 443
    label "S&#261;decczyzna"
  ]
  node [
    id 444
    label "Sudan"
  ]
  node [
    id 445
    label "Sand&#380;ak"
  ]
  node [
    id 446
    label "Nadrenia"
  ]
  node [
    id 447
    label "San_Marino"
  ]
  node [
    id 448
    label "Burundi"
  ]
  node [
    id 449
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 450
    label "Dominikana"
  ]
  node [
    id 451
    label "Komory"
  ]
  node [
    id 452
    label "Zakarpacie"
  ]
  node [
    id 453
    label "Gwatemala"
  ]
  node [
    id 454
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 455
    label "Zag&#243;rze"
  ]
  node [
    id 456
    label "Andaluzja"
  ]
  node [
    id 457
    label "granica_pa&#324;stwa"
  ]
  node [
    id 458
    label "Turkiestan"
  ]
  node [
    id 459
    label "Naddniestrze"
  ]
  node [
    id 460
    label "Hercegowina"
  ]
  node [
    id 461
    label "Brunei"
  ]
  node [
    id 462
    label "Iran"
  ]
  node [
    id 463
    label "jednostka_administracyjna"
  ]
  node [
    id 464
    label "Zimbabwe"
  ]
  node [
    id 465
    label "Namibia"
  ]
  node [
    id 466
    label "Meksyk"
  ]
  node [
    id 467
    label "Lotaryngia"
  ]
  node [
    id 468
    label "Kamerun"
  ]
  node [
    id 469
    label "Opolszczyzna"
  ]
  node [
    id 470
    label "Afryka_Wschodnia"
  ]
  node [
    id 471
    label "Szlezwik"
  ]
  node [
    id 472
    label "Somalia"
  ]
  node [
    id 473
    label "Angola"
  ]
  node [
    id 474
    label "Gabon"
  ]
  node [
    id 475
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 476
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 477
    label "Mozambik"
  ]
  node [
    id 478
    label "Tajwan"
  ]
  node [
    id 479
    label "Tunezja"
  ]
  node [
    id 480
    label "Nowa_Zelandia"
  ]
  node [
    id 481
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 482
    label "Podbeskidzie"
  ]
  node [
    id 483
    label "Liban"
  ]
  node [
    id 484
    label "Jordania"
  ]
  node [
    id 485
    label "Tonga"
  ]
  node [
    id 486
    label "Czad"
  ]
  node [
    id 487
    label "Liberia"
  ]
  node [
    id 488
    label "Gwinea"
  ]
  node [
    id 489
    label "Belize"
  ]
  node [
    id 490
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 491
    label "Mazowsze"
  ]
  node [
    id 492
    label "&#321;otwa"
  ]
  node [
    id 493
    label "Syria"
  ]
  node [
    id 494
    label "Benin"
  ]
  node [
    id 495
    label "Afryka_Zachodnia"
  ]
  node [
    id 496
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 497
    label "Dominika"
  ]
  node [
    id 498
    label "Antigua_i_Barbuda"
  ]
  node [
    id 499
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 500
    label "Hanower"
  ]
  node [
    id 501
    label "Galicja"
  ]
  node [
    id 502
    label "Szkocja"
  ]
  node [
    id 503
    label "Walia"
  ]
  node [
    id 504
    label "Afganistan"
  ]
  node [
    id 505
    label "Kiribati"
  ]
  node [
    id 506
    label "W&#322;ochy"
  ]
  node [
    id 507
    label "Szwajcaria"
  ]
  node [
    id 508
    label "Powi&#347;le"
  ]
  node [
    id 509
    label "Sahara_Zachodnia"
  ]
  node [
    id 510
    label "Chorwacja"
  ]
  node [
    id 511
    label "Tajlandia"
  ]
  node [
    id 512
    label "Salwador"
  ]
  node [
    id 513
    label "Bahamy"
  ]
  node [
    id 514
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 515
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 516
    label "Zamojszczyzna"
  ]
  node [
    id 517
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 518
    label "S&#322;owenia"
  ]
  node [
    id 519
    label "Gambia"
  ]
  node [
    id 520
    label "Kujawy"
  ]
  node [
    id 521
    label "Urugwaj"
  ]
  node [
    id 522
    label "Podlasie"
  ]
  node [
    id 523
    label "Zair"
  ]
  node [
    id 524
    label "Erytrea"
  ]
  node [
    id 525
    label "Laponia"
  ]
  node [
    id 526
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 527
    label "Umbria"
  ]
  node [
    id 528
    label "Rosja"
  ]
  node [
    id 529
    label "Uganda"
  ]
  node [
    id 530
    label "Niger"
  ]
  node [
    id 531
    label "Mauritius"
  ]
  node [
    id 532
    label "Turkmenistan"
  ]
  node [
    id 533
    label "Turcja"
  ]
  node [
    id 534
    label "Mezoameryka"
  ]
  node [
    id 535
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 536
    label "Irlandia"
  ]
  node [
    id 537
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 538
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 539
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 540
    label "Gwinea_Bissau"
  ]
  node [
    id 541
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 542
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 543
    label "Kurdystan"
  ]
  node [
    id 544
    label "Belgia"
  ]
  node [
    id 545
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 546
    label "Palau"
  ]
  node [
    id 547
    label "Barbados"
  ]
  node [
    id 548
    label "Chile"
  ]
  node [
    id 549
    label "Wenezuela"
  ]
  node [
    id 550
    label "W&#281;gry"
  ]
  node [
    id 551
    label "Argentyna"
  ]
  node [
    id 552
    label "Kolumbia"
  ]
  node [
    id 553
    label "Kampania"
  ]
  node [
    id 554
    label "Armagnac"
  ]
  node [
    id 555
    label "Sierra_Leone"
  ]
  node [
    id 556
    label "Azerbejd&#380;an"
  ]
  node [
    id 557
    label "Kongo"
  ]
  node [
    id 558
    label "Polinezja"
  ]
  node [
    id 559
    label "Warmia"
  ]
  node [
    id 560
    label "Pakistan"
  ]
  node [
    id 561
    label "Liechtenstein"
  ]
  node [
    id 562
    label "Wielkopolska"
  ]
  node [
    id 563
    label "Nikaragua"
  ]
  node [
    id 564
    label "Senegal"
  ]
  node [
    id 565
    label "brzeg"
  ]
  node [
    id 566
    label "Bordeaux"
  ]
  node [
    id 567
    label "Lauda"
  ]
  node [
    id 568
    label "Indie"
  ]
  node [
    id 569
    label "Mazury"
  ]
  node [
    id 570
    label "Suazi"
  ]
  node [
    id 571
    label "Polska"
  ]
  node [
    id 572
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 573
    label "Algieria"
  ]
  node [
    id 574
    label "Jamajka"
  ]
  node [
    id 575
    label "Timor_Wschodni"
  ]
  node [
    id 576
    label "Oceania"
  ]
  node [
    id 577
    label "Kostaryka"
  ]
  node [
    id 578
    label "Podkarpacie"
  ]
  node [
    id 579
    label "Lasko"
  ]
  node [
    id 580
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 581
    label "Kuba"
  ]
  node [
    id 582
    label "Mauretania"
  ]
  node [
    id 583
    label "Amazonia"
  ]
  node [
    id 584
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 585
    label "Portoryko"
  ]
  node [
    id 586
    label "Brazylia"
  ]
  node [
    id 587
    label "Mo&#322;dawia"
  ]
  node [
    id 588
    label "organizacja"
  ]
  node [
    id 589
    label "Litwa"
  ]
  node [
    id 590
    label "Kirgistan"
  ]
  node [
    id 591
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 592
    label "Izrael"
  ]
  node [
    id 593
    label "Grecja"
  ]
  node [
    id 594
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 595
    label "Kurpie"
  ]
  node [
    id 596
    label "Holandia"
  ]
  node [
    id 597
    label "Sri_Lanka"
  ]
  node [
    id 598
    label "Tonkin"
  ]
  node [
    id 599
    label "Katar"
  ]
  node [
    id 600
    label "Azja_Wschodnia"
  ]
  node [
    id 601
    label "Mikronezja"
  ]
  node [
    id 602
    label "Ukraina_Zachodnia"
  ]
  node [
    id 603
    label "Laos"
  ]
  node [
    id 604
    label "Mongolia"
  ]
  node [
    id 605
    label "Turyngia"
  ]
  node [
    id 606
    label "Malediwy"
  ]
  node [
    id 607
    label "Zambia"
  ]
  node [
    id 608
    label "Baszkiria"
  ]
  node [
    id 609
    label "Tanzania"
  ]
  node [
    id 610
    label "Gujana"
  ]
  node [
    id 611
    label "Apulia"
  ]
  node [
    id 612
    label "Czechy"
  ]
  node [
    id 613
    label "Panama"
  ]
  node [
    id 614
    label "Uzbekistan"
  ]
  node [
    id 615
    label "Gruzja"
  ]
  node [
    id 616
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 617
    label "Serbia"
  ]
  node [
    id 618
    label "Francja"
  ]
  node [
    id 619
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 620
    label "Togo"
  ]
  node [
    id 621
    label "Estonia"
  ]
  node [
    id 622
    label "Indochiny"
  ]
  node [
    id 623
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 624
    label "Oman"
  ]
  node [
    id 625
    label "Boliwia"
  ]
  node [
    id 626
    label "Portugalia"
  ]
  node [
    id 627
    label "Wyspy_Salomona"
  ]
  node [
    id 628
    label "Luksemburg"
  ]
  node [
    id 629
    label "Haiti"
  ]
  node [
    id 630
    label "Biskupizna"
  ]
  node [
    id 631
    label "Lubuskie"
  ]
  node [
    id 632
    label "Birma"
  ]
  node [
    id 633
    label "Rodezja"
  ]
  node [
    id 634
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 635
    label "schronienie"
  ]
  node [
    id 636
    label "&#322;atwy"
  ]
  node [
    id 637
    label "bezpiecznie"
  ]
  node [
    id 638
    label "w_chuj"
  ]
  node [
    id 639
    label "bezpo&#347;redni"
  ]
  node [
    id 640
    label "kr&#243;tki"
  ]
  node [
    id 641
    label "temperamentny"
  ]
  node [
    id 642
    label "prosty"
  ]
  node [
    id 643
    label "intensywny"
  ]
  node [
    id 644
    label "dynamiczny"
  ]
  node [
    id 645
    label "szybko"
  ]
  node [
    id 646
    label "sprawny"
  ]
  node [
    id 647
    label "bystrolotny"
  ]
  node [
    id 648
    label "energiczny"
  ]
  node [
    id 649
    label "zrobienie"
  ]
  node [
    id 650
    label "consumption"
  ]
  node [
    id 651
    label "czynno&#347;&#263;"
  ]
  node [
    id 652
    label "spowodowanie"
  ]
  node [
    id 653
    label "zareagowanie"
  ]
  node [
    id 654
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 655
    label "erecting"
  ]
  node [
    id 656
    label "movement"
  ]
  node [
    id 657
    label "entertainment"
  ]
  node [
    id 658
    label "zacz&#281;cie"
  ]
  node [
    id 659
    label "jaki&#347;"
  ]
  node [
    id 660
    label "strategia"
  ]
  node [
    id 661
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 662
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 663
    label "ob&#322;awa_augustowska"
  ]
  node [
    id 664
    label "jutro"
  ]
  node [
    id 665
    label "czas"
  ]
  node [
    id 666
    label "pofolgowa&#263;"
  ]
  node [
    id 667
    label "assent"
  ]
  node [
    id 668
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 669
    label "leave"
  ]
  node [
    id 670
    label "uzna&#263;"
  ]
  node [
    id 671
    label "dymensja"
  ]
  node [
    id 672
    label "odzie&#380;"
  ]
  node [
    id 673
    label "cecha"
  ]
  node [
    id 674
    label "circumference"
  ]
  node [
    id 675
    label "liczba"
  ]
  node [
    id 676
    label "ilo&#347;&#263;"
  ]
  node [
    id 677
    label "warunek_lokalowy"
  ]
  node [
    id 678
    label "zrobi&#263;"
  ]
  node [
    id 679
    label "fly"
  ]
  node [
    id 680
    label "umkn&#261;&#263;"
  ]
  node [
    id 681
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 682
    label "tent-fly"
  ]
  node [
    id 683
    label "niedostateczny"
  ]
  node [
    id 684
    label "staff"
  ]
  node [
    id 685
    label "kobieta"
  ]
  node [
    id 686
    label "seks_oralny"
  ]
  node [
    id 687
    label "podpora"
  ]
  node [
    id 688
    label "tyrs"
  ]
  node [
    id 689
    label "insygnium"
  ]
  node [
    id 690
    label "towar"
  ]
  node [
    id 691
    label "plewinka"
  ]
  node [
    id 692
    label "astrowce"
  ]
  node [
    id 693
    label "dokument"
  ]
  node [
    id 694
    label "device"
  ]
  node [
    id 695
    label "program_u&#380;ytkowy"
  ]
  node [
    id 696
    label "intencja"
  ]
  node [
    id 697
    label "agreement"
  ]
  node [
    id 698
    label "pomys&#322;"
  ]
  node [
    id 699
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 700
    label "plan"
  ]
  node [
    id 701
    label "dokumentacja"
  ]
  node [
    id 702
    label "Karta_Nauczyciela"
  ]
  node [
    id 703
    label "marc&#243;wka"
  ]
  node [
    id 704
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 705
    label "akt"
  ]
  node [
    id 706
    label "przej&#347;&#263;"
  ]
  node [
    id 707
    label "charter"
  ]
  node [
    id 708
    label "przej&#347;cie"
  ]
  node [
    id 709
    label "chwila"
  ]
  node [
    id 710
    label "uderzenie"
  ]
  node [
    id 711
    label "time"
  ]
  node [
    id 712
    label "uruchomienie"
  ]
  node [
    id 713
    label "nap&#322;yn&#261;&#263;"
  ]
  node [
    id 714
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 715
    label "supernadz&#243;r"
  ]
  node [
    id 716
    label "absolutorium"
  ]
  node [
    id 717
    label "nap&#322;yni&#281;cie"
  ]
  node [
    id 718
    label "podupada&#263;"
  ]
  node [
    id 719
    label "nap&#322;ywanie"
  ]
  node [
    id 720
    label "podupadanie"
  ]
  node [
    id 721
    label "kwestor"
  ]
  node [
    id 722
    label "uruchamia&#263;"
  ]
  node [
    id 723
    label "mienie"
  ]
  node [
    id 724
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 725
    label "uruchamianie"
  ]
  node [
    id 726
    label "czynnik_produkcji"
  ]
  node [
    id 727
    label "zgodzi&#263;"
  ]
  node [
    id 728
    label "pomocnik"
  ]
  node [
    id 729
    label "doch&#243;d"
  ]
  node [
    id 730
    label "property"
  ]
  node [
    id 731
    label "telefon_zaufania"
  ]
  node [
    id 732
    label "darowizna"
  ]
  node [
    id 733
    label "&#347;rodek"
  ]
  node [
    id 734
    label "liga"
  ]
  node [
    id 735
    label "gamo&#324;"
  ]
  node [
    id 736
    label "istota_&#380;ywa"
  ]
  node [
    id 737
    label "dupa_wo&#322;owa"
  ]
  node [
    id 738
    label "pastwa"
  ]
  node [
    id 739
    label "rzecz"
  ]
  node [
    id 740
    label "zbi&#243;rka"
  ]
  node [
    id 741
    label "dar"
  ]
  node [
    id 742
    label "nieszcz&#281;&#347;nik"
  ]
  node [
    id 743
    label "nastawienie"
  ]
  node [
    id 744
    label "crack"
  ]
  node [
    id 745
    label "zwierz&#281;"
  ]
  node [
    id 746
    label "ko&#347;cielny"
  ]
  node [
    id 747
    label "po&#347;miewisko"
  ]
  node [
    id 748
    label "gorzka_pigu&#322;ka"
  ]
  node [
    id 749
    label "nieszcz&#281;&#347;cie"
  ]
  node [
    id 750
    label "przegra"
  ]
  node [
    id 751
    label "wysiadka"
  ]
  node [
    id 752
    label "passa"
  ]
  node [
    id 753
    label "niefortunno&#347;&#263;"
  ]
  node [
    id 754
    label "po&#322;o&#380;enie"
  ]
  node [
    id 755
    label "visitation"
  ]
  node [
    id 756
    label "niepowodzenie"
  ]
  node [
    id 757
    label "wydarzenie"
  ]
  node [
    id 758
    label "calamity"
  ]
  node [
    id 759
    label "reverse"
  ]
  node [
    id 760
    label "rezultat"
  ]
  node [
    id 761
    label "k&#322;adzenie"
  ]
  node [
    id 762
    label "spontaniczny"
  ]
  node [
    id 763
    label "&#380;ywy"
  ]
  node [
    id 764
    label "&#380;ywio&#322;owo"
  ]
  node [
    id 765
    label "skomplikowanie"
  ]
  node [
    id 766
    label "trudny"
  ]
  node [
    id 767
    label "society"
  ]
  node [
    id 768
    label "jakobini"
  ]
  node [
    id 769
    label "klubista"
  ]
  node [
    id 770
    label "stowarzyszenie"
  ]
  node [
    id 771
    label "lokal"
  ]
  node [
    id 772
    label "od&#322;am"
  ]
  node [
    id 773
    label "siedziba"
  ]
  node [
    id 774
    label "bar"
  ]
  node [
    id 775
    label "solicitation"
  ]
  node [
    id 776
    label "wypowied&#378;"
  ]
  node [
    id 777
    label "zestaw"
  ]
  node [
    id 778
    label "scali&#263;"
  ]
  node [
    id 779
    label "give"
  ]
  node [
    id 780
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 781
    label "note"
  ]
  node [
    id 782
    label "set"
  ]
  node [
    id 783
    label "da&#263;"
  ]
  node [
    id 784
    label "marshal"
  ]
  node [
    id 785
    label "opracowa&#263;"
  ]
  node [
    id 786
    label "przekaza&#263;"
  ]
  node [
    id 787
    label "zmieni&#263;"
  ]
  node [
    id 788
    label "pay"
  ]
  node [
    id 789
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 790
    label "odda&#263;"
  ]
  node [
    id 791
    label "przy&#322;o&#380;y&#263;"
  ]
  node [
    id 792
    label "jell"
  ]
  node [
    id 793
    label "frame"
  ]
  node [
    id 794
    label "u&#322;o&#380;y&#263;"
  ]
  node [
    id 795
    label "zebra&#263;"
  ]
  node [
    id 796
    label "fold"
  ]
  node [
    id 797
    label "dyplomata"
  ]
  node [
    id 798
    label "wys&#322;annik"
  ]
  node [
    id 799
    label "przedstawiciel"
  ]
  node [
    id 800
    label "kurier_dyplomatyczny"
  ]
  node [
    id 801
    label "ablegat"
  ]
  node [
    id 802
    label "Korwin"
  ]
  node [
    id 803
    label "dyscyplina_partyjna"
  ]
  node [
    id 804
    label "izba_ni&#380;sza"
  ]
  node [
    id 805
    label "poselstwo"
  ]
  node [
    id 806
    label "koturn"
  ]
  node [
    id 807
    label "samoch&#243;d_ci&#281;&#380;arowy"
  ]
  node [
    id 808
    label "skorupa_ziemska"
  ]
  node [
    id 809
    label "but"
  ]
  node [
    id 810
    label "p&#322;yta_kontynentalna"
  ]
  node [
    id 811
    label "podeszwa"
  ]
  node [
    id 812
    label "nadwozie"
  ]
  node [
    id 813
    label "sfera"
  ]
  node [
    id 814
    label "struktura"
  ]
  node [
    id 815
    label "p&#322;aszczyzna"
  ]
  node [
    id 816
    label "odpowiedzialny"
  ]
  node [
    id 817
    label "obywatelsko"
  ]
  node [
    id 818
    label "s&#322;uszny"
  ]
  node [
    id 819
    label "oddolny"
  ]
  node [
    id 820
    label "left"
  ]
  node [
    id 821
    label "sejm"
  ]
  node [
    id 822
    label "szko&#322;a"
  ]
  node [
    id 823
    label "blok"
  ]
  node [
    id 824
    label "zdemokratyzowanie"
  ]
  node [
    id 825
    label "uczciwy"
  ]
  node [
    id 826
    label "demokratyzowanie_si&#281;"
  ]
  node [
    id 827
    label "post&#281;powy"
  ]
  node [
    id 828
    label "demokratycznie"
  ]
  node [
    id 829
    label "demokratyzowanie"
  ]
  node [
    id 830
    label "zdemokratyzowanie_si&#281;"
  ]
  node [
    id 831
    label "SLD"
  ]
  node [
    id 832
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 833
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 834
    label "ZChN"
  ]
  node [
    id 835
    label "Wigowie"
  ]
  node [
    id 836
    label "unit"
  ]
  node [
    id 837
    label "Razem"
  ]
  node [
    id 838
    label "partia"
  ]
  node [
    id 839
    label "si&#322;a"
  ]
  node [
    id 840
    label "PiS"
  ]
  node [
    id 841
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 842
    label "Bund"
  ]
  node [
    id 843
    label "AWS"
  ]
  node [
    id 844
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 845
    label "Kuomintang"
  ]
  node [
    id 846
    label "Jakobici"
  ]
  node [
    id 847
    label "PSL"
  ]
  node [
    id 848
    label "Federali&#347;ci"
  ]
  node [
    id 849
    label "ZSL"
  ]
  node [
    id 850
    label "PPR"
  ]
  node [
    id 851
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 852
    label "PO"
  ]
  node [
    id 853
    label "wiejski"
  ]
  node [
    id 854
    label "folk"
  ]
  node [
    id 855
    label "publiczny"
  ]
  node [
    id 856
    label "etniczny"
  ]
  node [
    id 857
    label "ludowo"
  ]
  node [
    id 858
    label "pomy&#347;lny"
  ]
  node [
    id 859
    label "skuteczny"
  ]
  node [
    id 860
    label "moralny"
  ]
  node [
    id 861
    label "korzystny"
  ]
  node [
    id 862
    label "odpowiedni"
  ]
  node [
    id 863
    label "dobrze"
  ]
  node [
    id 864
    label "pozytywny"
  ]
  node [
    id 865
    label "grzeczny"
  ]
  node [
    id 866
    label "powitanie"
  ]
  node [
    id 867
    label "mi&#322;y"
  ]
  node [
    id 868
    label "dobroczynny"
  ]
  node [
    id 869
    label "pos&#322;uszny"
  ]
  node [
    id 870
    label "ca&#322;y"
  ]
  node [
    id 871
    label "czw&#243;rka"
  ]
  node [
    id 872
    label "spokojny"
  ]
  node [
    id 873
    label "&#347;mieszny"
  ]
  node [
    id 874
    label "drogi"
  ]
  node [
    id 875
    label "help"
  ]
  node [
    id 876
    label "aid"
  ]
  node [
    id 877
    label "u&#322;atwi&#263;"
  ]
  node [
    id 878
    label "concur"
  ]
  node [
    id 879
    label "zaskutkowa&#263;"
  ]
  node [
    id 880
    label "mandatariusz"
  ]
  node [
    id 881
    label "polityk"
  ]
  node [
    id 882
    label "oklaski"
  ]
  node [
    id 883
    label "kla&#347;ni&#281;cie"
  ]
  node [
    id 884
    label "arrange"
  ]
  node [
    id 885
    label "dress"
  ]
  node [
    id 886
    label "wyszkoli&#263;"
  ]
  node [
    id 887
    label "narz&#261;dzi&#263;"
  ]
  node [
    id 888
    label "wytworzy&#263;"
  ]
  node [
    id 889
    label "ukierunkowa&#263;"
  ]
  node [
    id 890
    label "train"
  ]
  node [
    id 891
    label "wykona&#263;"
  ]
  node [
    id 892
    label "cook"
  ]
  node [
    id 893
    label "blurt_out"
  ]
  node [
    id 894
    label "przenosi&#263;"
  ]
  node [
    id 895
    label "rugowa&#263;"
  ]
  node [
    id 896
    label "pozbywa&#263;_si&#281;"
  ]
  node [
    id 897
    label "robi&#263;"
  ]
  node [
    id 898
    label "zabija&#263;"
  ]
  node [
    id 899
    label "undo"
  ]
  node [
    id 900
    label "powodowa&#263;"
  ]
  node [
    id 901
    label "inny"
  ]
  node [
    id 902
    label "nast&#281;pnie"
  ]
  node [
    id 903
    label "kt&#243;ry&#347;"
  ]
  node [
    id 904
    label "kolejno"
  ]
  node [
    id 905
    label "nastopny"
  ]
  node [
    id 906
    label "wynik"
  ]
  node [
    id 907
    label "wyj&#347;cie"
  ]
  node [
    id 908
    label "spe&#322;nienie"
  ]
  node [
    id 909
    label "cesarskie_ci&#281;cie"
  ]
  node [
    id 910
    label "po&#322;o&#380;na"
  ]
  node [
    id 911
    label "proces_fizjologiczny"
  ]
  node [
    id 912
    label "przestanie"
  ]
  node [
    id 913
    label "usuni&#281;cie"
  ]
  node [
    id 914
    label "uniewa&#380;nienie"
  ]
  node [
    id 915
    label "birth"
  ]
  node [
    id 916
    label "wymy&#347;lenie"
  ]
  node [
    id 917
    label "po&#322;&#243;g"
  ]
  node [
    id 918
    label "szok_poporodowy"
  ]
  node [
    id 919
    label "event"
  ]
  node [
    id 920
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 921
    label "spos&#243;b"
  ]
  node [
    id 922
    label "rozwi&#261;zywanie_si&#281;"
  ]
  node [
    id 923
    label "dula"
  ]
  node [
    id 924
    label "prawy"
  ]
  node [
    id 925
    label "i"
  ]
  node [
    id 926
    label "sprawiedliwo&#347;&#263;"
  ]
  node [
    id 927
    label "sojusz"
  ]
  node [
    id 928
    label "polskie"
  ]
  node [
    id 929
    label "biuro"
  ]
  node [
    id 930
    label "analiza"
  ]
  node [
    id 931
    label "sejmowy"
  ]
  node [
    id 932
    label "Jaros&#322;awa"
  ]
  node [
    id 933
    label "Kaczy&#324;ski"
  ]
  node [
    id 934
    label "infrastruktura"
  ]
  node [
    id 935
    label "&#347;rodowisko"
  ]
  node [
    id 936
    label "ochrona"
  ]
  node [
    id 937
    label "wyspa"
  ]
  node [
    id 938
    label "dorzecze"
  ]
  node [
    id 939
    label "g&#243;rny"
  ]
  node [
    id 940
    label "Wis&#322;a"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 150
  ]
  edge [
    source 7
    target 151
  ]
  edge [
    source 7
    target 152
  ]
  edge [
    source 7
    target 153
  ]
  edge [
    source 7
    target 154
  ]
  edge [
    source 7
    target 155
  ]
  edge [
    source 7
    target 156
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 45
  ]
  edge [
    source 9
    target 46
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 9
    target 24
  ]
  edge [
    source 9
    target 157
  ]
  edge [
    source 9
    target 158
  ]
  edge [
    source 9
    target 159
  ]
  edge [
    source 9
    target 160
  ]
  edge [
    source 9
    target 161
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 10
    target 13
  ]
  edge [
    source 10
    target 25
  ]
  edge [
    source 10
    target 26
  ]
  edge [
    source 10
    target 162
  ]
  edge [
    source 10
    target 163
  ]
  edge [
    source 10
    target 164
  ]
  edge [
    source 10
    target 165
  ]
  edge [
    source 10
    target 166
  ]
  edge [
    source 10
    target 167
  ]
  edge [
    source 10
    target 168
  ]
  edge [
    source 10
    target 169
  ]
  edge [
    source 10
    target 170
  ]
  edge [
    source 10
    target 171
  ]
  edge [
    source 10
    target 172
  ]
  edge [
    source 10
    target 173
  ]
  edge [
    source 10
    target 174
  ]
  edge [
    source 10
    target 175
  ]
  edge [
    source 10
    target 176
  ]
  edge [
    source 10
    target 177
  ]
  edge [
    source 10
    target 178
  ]
  edge [
    source 10
    target 179
  ]
  edge [
    source 10
    target 180
  ]
  edge [
    source 10
    target 181
  ]
  edge [
    source 10
    target 182
  ]
  edge [
    source 10
    target 183
  ]
  edge [
    source 10
    target 184
  ]
  edge [
    source 10
    target 185
  ]
  edge [
    source 10
    target 186
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 81
  ]
  edge [
    source 11
    target 187
  ]
  edge [
    source 11
    target 188
  ]
  edge [
    source 11
    target 936
  ]
  edge [
    source 11
    target 20
  ]
  edge [
    source 11
    target 937
  ]
  edge [
    source 11
    target 938
  ]
  edge [
    source 11
    target 939
  ]
  edge [
    source 11
    target 940
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 189
  ]
  edge [
    source 13
    target 190
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 191
  ]
  edge [
    source 14
    target 192
  ]
  edge [
    source 14
    target 193
  ]
  edge [
    source 14
    target 194
  ]
  edge [
    source 14
    target 195
  ]
  edge [
    source 14
    target 196
  ]
  edge [
    source 14
    target 197
  ]
  edge [
    source 14
    target 198
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 38
  ]
  edge [
    source 15
    target 199
  ]
  edge [
    source 15
    target 200
  ]
  edge [
    source 15
    target 201
  ]
  edge [
    source 15
    target 202
  ]
  edge [
    source 15
    target 203
  ]
  edge [
    source 15
    target 66
  ]
  edge [
    source 15
    target 19
  ]
  edge [
    source 15
    target 21
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 204
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 19
  ]
  edge [
    source 17
    target 21
  ]
  edge [
    source 17
    target 205
  ]
  edge [
    source 17
    target 206
  ]
  edge [
    source 17
    target 207
  ]
  edge [
    source 17
    target 208
  ]
  edge [
    source 17
    target 209
  ]
  edge [
    source 17
    target 210
  ]
  edge [
    source 17
    target 211
  ]
  edge [
    source 17
    target 212
  ]
  edge [
    source 17
    target 213
  ]
  edge [
    source 17
    target 214
  ]
  edge [
    source 17
    target 215
  ]
  edge [
    source 17
    target 216
  ]
  edge [
    source 17
    target 217
  ]
  edge [
    source 17
    target 218
  ]
  edge [
    source 17
    target 219
  ]
  edge [
    source 17
    target 220
  ]
  edge [
    source 17
    target 221
  ]
  edge [
    source 17
    target 222
  ]
  edge [
    source 17
    target 223
  ]
  edge [
    source 17
    target 224
  ]
  edge [
    source 17
    target 225
  ]
  edge [
    source 17
    target 226
  ]
  edge [
    source 17
    target 227
  ]
  edge [
    source 17
    target 228
  ]
  edge [
    source 17
    target 229
  ]
  edge [
    source 17
    target 230
  ]
  edge [
    source 17
    target 231
  ]
  edge [
    source 17
    target 176
  ]
  edge [
    source 17
    target 232
  ]
  edge [
    source 17
    target 233
  ]
  edge [
    source 17
    target 234
  ]
  edge [
    source 17
    target 235
  ]
  edge [
    source 17
    target 236
  ]
  edge [
    source 17
    target 237
  ]
  edge [
    source 17
    target 238
  ]
  edge [
    source 17
    target 239
  ]
  edge [
    source 17
    target 240
  ]
  edge [
    source 17
    target 241
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 76
  ]
  edge [
    source 19
    target 29
  ]
  edge [
    source 19
    target 21
  ]
  edge [
    source 20
    target 936
  ]
  edge [
    source 20
    target 937
  ]
  edge [
    source 20
    target 938
  ]
  edge [
    source 20
    target 939
  ]
  edge [
    source 20
    target 940
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 75
  ]
  edge [
    source 21
    target 242
  ]
  edge [
    source 21
    target 243
  ]
  edge [
    source 21
    target 244
  ]
  edge [
    source 21
    target 245
  ]
  edge [
    source 21
    target 246
  ]
  edge [
    source 21
    target 247
  ]
  edge [
    source 21
    target 29
  ]
  edge [
    source 21
    target 248
  ]
  edge [
    source 21
    target 249
  ]
  edge [
    source 21
    target 250
  ]
  edge [
    source 21
    target 251
  ]
  edge [
    source 21
    target 252
  ]
  edge [
    source 21
    target 253
  ]
  edge [
    source 21
    target 254
  ]
  edge [
    source 21
    target 255
  ]
  edge [
    source 21
    target 256
  ]
  edge [
    source 21
    target 257
  ]
  edge [
    source 21
    target 258
  ]
  edge [
    source 21
    target 259
  ]
  edge [
    source 21
    target 260
  ]
  edge [
    source 21
    target 261
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 262
  ]
  edge [
    source 22
    target 263
  ]
  edge [
    source 22
    target 264
  ]
  edge [
    source 22
    target 147
  ]
  edge [
    source 22
    target 265
  ]
  edge [
    source 22
    target 266
  ]
  edge [
    source 22
    target 75
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 267
  ]
  edge [
    source 23
    target 268
  ]
  edge [
    source 23
    target 269
  ]
  edge [
    source 23
    target 270
  ]
  edge [
    source 23
    target 271
  ]
  edge [
    source 23
    target 272
  ]
  edge [
    source 23
    target 273
  ]
  edge [
    source 23
    target 274
  ]
  edge [
    source 23
    target 275
  ]
  edge [
    source 23
    target 276
  ]
  edge [
    source 23
    target 277
  ]
  edge [
    source 23
    target 278
  ]
  edge [
    source 23
    target 279
  ]
  edge [
    source 23
    target 280
  ]
  edge [
    source 23
    target 281
  ]
  edge [
    source 23
    target 282
  ]
  edge [
    source 23
    target 283
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 30
  ]
  edge [
    source 24
    target 31
  ]
  edge [
    source 24
    target 34
  ]
  edge [
    source 24
    target 35
  ]
  edge [
    source 24
    target 48
  ]
  edge [
    source 24
    target 49
  ]
  edge [
    source 24
    target 51
  ]
  edge [
    source 24
    target 71
  ]
  edge [
    source 24
    target 72
  ]
  edge [
    source 24
    target 74
  ]
  edge [
    source 24
    target 77
  ]
  edge [
    source 24
    target 45
  ]
  edge [
    source 24
    target 59
  ]
  edge [
    source 24
    target 47
  ]
  edge [
    source 24
    target 83
  ]
  edge [
    source 24
    target 284
  ]
  edge [
    source 24
    target 285
  ]
  edge [
    source 24
    target 286
  ]
  edge [
    source 24
    target 287
  ]
  edge [
    source 24
    target 288
  ]
  edge [
    source 24
    target 289
  ]
  edge [
    source 24
    target 290
  ]
  edge [
    source 24
    target 291
  ]
  edge [
    source 24
    target 292
  ]
  edge [
    source 24
    target 293
  ]
  edge [
    source 24
    target 294
  ]
  edge [
    source 25
    target 295
  ]
  edge [
    source 25
    target 296
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 297
  ]
  edge [
    source 26
    target 298
  ]
  edge [
    source 26
    target 299
  ]
  edge [
    source 26
    target 300
  ]
  edge [
    source 26
    target 301
  ]
  edge [
    source 26
    target 302
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 303
  ]
  edge [
    source 27
    target 304
  ]
  edge [
    source 27
    target 305
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 306
  ]
  edge [
    source 28
    target 307
  ]
  edge [
    source 28
    target 308
  ]
  edge [
    source 28
    target 309
  ]
  edge [
    source 28
    target 310
  ]
  edge [
    source 28
    target 311
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 77
  ]
  edge [
    source 29
    target 36
  ]
  edge [
    source 30
    target 40
  ]
  edge [
    source 30
    target 73
  ]
  edge [
    source 30
    target 220
  ]
  edge [
    source 30
    target 312
  ]
  edge [
    source 30
    target 313
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 314
  ]
  edge [
    source 32
    target 315
  ]
  edge [
    source 32
    target 316
  ]
  edge [
    source 32
    target 317
  ]
  edge [
    source 32
    target 318
  ]
  edge [
    source 32
    target 319
  ]
  edge [
    source 32
    target 320
  ]
  edge [
    source 32
    target 321
  ]
  edge [
    source 32
    target 322
  ]
  edge [
    source 32
    target 323
  ]
  edge [
    source 32
    target 324
  ]
  edge [
    source 32
    target 325
  ]
  edge [
    source 32
    target 326
  ]
  edge [
    source 32
    target 327
  ]
  edge [
    source 32
    target 328
  ]
  edge [
    source 32
    target 329
  ]
  edge [
    source 32
    target 330
  ]
  edge [
    source 32
    target 331
  ]
  edge [
    source 32
    target 332
  ]
  edge [
    source 32
    target 333
  ]
  edge [
    source 32
    target 334
  ]
  edge [
    source 32
    target 335
  ]
  edge [
    source 32
    target 336
  ]
  edge [
    source 32
    target 337
  ]
  edge [
    source 32
    target 338
  ]
  edge [
    source 32
    target 339
  ]
  edge [
    source 32
    target 340
  ]
  edge [
    source 32
    target 341
  ]
  edge [
    source 32
    target 342
  ]
  edge [
    source 32
    target 343
  ]
  edge [
    source 32
    target 344
  ]
  edge [
    source 32
    target 345
  ]
  edge [
    source 32
    target 346
  ]
  edge [
    source 32
    target 347
  ]
  edge [
    source 32
    target 348
  ]
  edge [
    source 32
    target 349
  ]
  edge [
    source 32
    target 350
  ]
  edge [
    source 32
    target 351
  ]
  edge [
    source 32
    target 352
  ]
  edge [
    source 32
    target 353
  ]
  edge [
    source 32
    target 354
  ]
  edge [
    source 32
    target 355
  ]
  edge [
    source 32
    target 356
  ]
  edge [
    source 32
    target 357
  ]
  edge [
    source 32
    target 358
  ]
  edge [
    source 32
    target 359
  ]
  edge [
    source 32
    target 360
  ]
  edge [
    source 32
    target 361
  ]
  edge [
    source 32
    target 362
  ]
  edge [
    source 32
    target 363
  ]
  edge [
    source 32
    target 364
  ]
  edge [
    source 32
    target 365
  ]
  edge [
    source 32
    target 366
  ]
  edge [
    source 32
    target 367
  ]
  edge [
    source 32
    target 368
  ]
  edge [
    source 32
    target 369
  ]
  edge [
    source 32
    target 370
  ]
  edge [
    source 32
    target 371
  ]
  edge [
    source 32
    target 372
  ]
  edge [
    source 32
    target 373
  ]
  edge [
    source 32
    target 374
  ]
  edge [
    source 32
    target 375
  ]
  edge [
    source 32
    target 376
  ]
  edge [
    source 32
    target 377
  ]
  edge [
    source 32
    target 378
  ]
  edge [
    source 32
    target 379
  ]
  edge [
    source 32
    target 380
  ]
  edge [
    source 32
    target 381
  ]
  edge [
    source 32
    target 382
  ]
  edge [
    source 32
    target 383
  ]
  edge [
    source 32
    target 207
  ]
  edge [
    source 32
    target 384
  ]
  edge [
    source 32
    target 385
  ]
  edge [
    source 32
    target 386
  ]
  edge [
    source 32
    target 387
  ]
  edge [
    source 32
    target 388
  ]
  edge [
    source 32
    target 389
  ]
  edge [
    source 32
    target 390
  ]
  edge [
    source 32
    target 391
  ]
  edge [
    source 32
    target 392
  ]
  edge [
    source 32
    target 393
  ]
  edge [
    source 32
    target 394
  ]
  edge [
    source 32
    target 395
  ]
  edge [
    source 32
    target 396
  ]
  edge [
    source 32
    target 397
  ]
  edge [
    source 32
    target 398
  ]
  edge [
    source 32
    target 399
  ]
  edge [
    source 32
    target 400
  ]
  edge [
    source 32
    target 401
  ]
  edge [
    source 32
    target 402
  ]
  edge [
    source 32
    target 403
  ]
  edge [
    source 32
    target 404
  ]
  edge [
    source 32
    target 405
  ]
  edge [
    source 32
    target 406
  ]
  edge [
    source 32
    target 407
  ]
  edge [
    source 32
    target 408
  ]
  edge [
    source 32
    target 409
  ]
  edge [
    source 32
    target 410
  ]
  edge [
    source 32
    target 411
  ]
  edge [
    source 32
    target 412
  ]
  edge [
    source 32
    target 413
  ]
  edge [
    source 32
    target 414
  ]
  edge [
    source 32
    target 415
  ]
  edge [
    source 32
    target 416
  ]
  edge [
    source 32
    target 417
  ]
  edge [
    source 32
    target 418
  ]
  edge [
    source 32
    target 419
  ]
  edge [
    source 32
    target 420
  ]
  edge [
    source 32
    target 421
  ]
  edge [
    source 32
    target 422
  ]
  edge [
    source 32
    target 423
  ]
  edge [
    source 32
    target 424
  ]
  edge [
    source 32
    target 425
  ]
  edge [
    source 32
    target 426
  ]
  edge [
    source 32
    target 427
  ]
  edge [
    source 32
    target 428
  ]
  edge [
    source 32
    target 429
  ]
  edge [
    source 32
    target 430
  ]
  edge [
    source 32
    target 431
  ]
  edge [
    source 32
    target 432
  ]
  edge [
    source 32
    target 433
  ]
  edge [
    source 32
    target 434
  ]
  edge [
    source 32
    target 435
  ]
  edge [
    source 32
    target 436
  ]
  edge [
    source 32
    target 437
  ]
  edge [
    source 32
    target 438
  ]
  edge [
    source 32
    target 439
  ]
  edge [
    source 32
    target 440
  ]
  edge [
    source 32
    target 441
  ]
  edge [
    source 32
    target 442
  ]
  edge [
    source 32
    target 443
  ]
  edge [
    source 32
    target 444
  ]
  edge [
    source 32
    target 445
  ]
  edge [
    source 32
    target 446
  ]
  edge [
    source 32
    target 447
  ]
  edge [
    source 32
    target 448
  ]
  edge [
    source 32
    target 449
  ]
  edge [
    source 32
    target 450
  ]
  edge [
    source 32
    target 451
  ]
  edge [
    source 32
    target 452
  ]
  edge [
    source 32
    target 453
  ]
  edge [
    source 32
    target 454
  ]
  edge [
    source 32
    target 455
  ]
  edge [
    source 32
    target 456
  ]
  edge [
    source 32
    target 457
  ]
  edge [
    source 32
    target 458
  ]
  edge [
    source 32
    target 459
  ]
  edge [
    source 32
    target 460
  ]
  edge [
    source 32
    target 461
  ]
  edge [
    source 32
    target 462
  ]
  edge [
    source 32
    target 463
  ]
  edge [
    source 32
    target 464
  ]
  edge [
    source 32
    target 465
  ]
  edge [
    source 32
    target 466
  ]
  edge [
    source 32
    target 467
  ]
  edge [
    source 32
    target 468
  ]
  edge [
    source 32
    target 469
  ]
  edge [
    source 32
    target 470
  ]
  edge [
    source 32
    target 471
  ]
  edge [
    source 32
    target 472
  ]
  edge [
    source 32
    target 473
  ]
  edge [
    source 32
    target 474
  ]
  edge [
    source 32
    target 475
  ]
  edge [
    source 32
    target 476
  ]
  edge [
    source 32
    target 477
  ]
  edge [
    source 32
    target 478
  ]
  edge [
    source 32
    target 479
  ]
  edge [
    source 32
    target 480
  ]
  edge [
    source 32
    target 481
  ]
  edge [
    source 32
    target 482
  ]
  edge [
    source 32
    target 483
  ]
  edge [
    source 32
    target 484
  ]
  edge [
    source 32
    target 485
  ]
  edge [
    source 32
    target 486
  ]
  edge [
    source 32
    target 487
  ]
  edge [
    source 32
    target 488
  ]
  edge [
    source 32
    target 489
  ]
  edge [
    source 32
    target 490
  ]
  edge [
    source 32
    target 491
  ]
  edge [
    source 32
    target 492
  ]
  edge [
    source 32
    target 493
  ]
  edge [
    source 32
    target 494
  ]
  edge [
    source 32
    target 495
  ]
  edge [
    source 32
    target 496
  ]
  edge [
    source 32
    target 497
  ]
  edge [
    source 32
    target 498
  ]
  edge [
    source 32
    target 499
  ]
  edge [
    source 32
    target 500
  ]
  edge [
    source 32
    target 501
  ]
  edge [
    source 32
    target 502
  ]
  edge [
    source 32
    target 503
  ]
  edge [
    source 32
    target 504
  ]
  edge [
    source 32
    target 505
  ]
  edge [
    source 32
    target 506
  ]
  edge [
    source 32
    target 507
  ]
  edge [
    source 32
    target 508
  ]
  edge [
    source 32
    target 509
  ]
  edge [
    source 32
    target 510
  ]
  edge [
    source 32
    target 511
  ]
  edge [
    source 32
    target 512
  ]
  edge [
    source 32
    target 513
  ]
  edge [
    source 32
    target 514
  ]
  edge [
    source 32
    target 515
  ]
  edge [
    source 32
    target 516
  ]
  edge [
    source 32
    target 517
  ]
  edge [
    source 32
    target 518
  ]
  edge [
    source 32
    target 519
  ]
  edge [
    source 32
    target 520
  ]
  edge [
    source 32
    target 521
  ]
  edge [
    source 32
    target 522
  ]
  edge [
    source 32
    target 523
  ]
  edge [
    source 32
    target 524
  ]
  edge [
    source 32
    target 525
  ]
  edge [
    source 32
    target 526
  ]
  edge [
    source 32
    target 527
  ]
  edge [
    source 32
    target 528
  ]
  edge [
    source 32
    target 529
  ]
  edge [
    source 32
    target 530
  ]
  edge [
    source 32
    target 531
  ]
  edge [
    source 32
    target 532
  ]
  edge [
    source 32
    target 533
  ]
  edge [
    source 32
    target 534
  ]
  edge [
    source 32
    target 535
  ]
  edge [
    source 32
    target 536
  ]
  edge [
    source 32
    target 537
  ]
  edge [
    source 32
    target 538
  ]
  edge [
    source 32
    target 539
  ]
  edge [
    source 32
    target 540
  ]
  edge [
    source 32
    target 541
  ]
  edge [
    source 32
    target 542
  ]
  edge [
    source 32
    target 543
  ]
  edge [
    source 32
    target 544
  ]
  edge [
    source 32
    target 545
  ]
  edge [
    source 32
    target 546
  ]
  edge [
    source 32
    target 547
  ]
  edge [
    source 32
    target 548
  ]
  edge [
    source 32
    target 549
  ]
  edge [
    source 32
    target 550
  ]
  edge [
    source 32
    target 551
  ]
  edge [
    source 32
    target 552
  ]
  edge [
    source 32
    target 553
  ]
  edge [
    source 32
    target 554
  ]
  edge [
    source 32
    target 555
  ]
  edge [
    source 32
    target 556
  ]
  edge [
    source 32
    target 557
  ]
  edge [
    source 32
    target 558
  ]
  edge [
    source 32
    target 559
  ]
  edge [
    source 32
    target 560
  ]
  edge [
    source 32
    target 561
  ]
  edge [
    source 32
    target 562
  ]
  edge [
    source 32
    target 563
  ]
  edge [
    source 32
    target 564
  ]
  edge [
    source 32
    target 565
  ]
  edge [
    source 32
    target 566
  ]
  edge [
    source 32
    target 567
  ]
  edge [
    source 32
    target 568
  ]
  edge [
    source 32
    target 569
  ]
  edge [
    source 32
    target 570
  ]
  edge [
    source 32
    target 571
  ]
  edge [
    source 32
    target 572
  ]
  edge [
    source 32
    target 573
  ]
  edge [
    source 32
    target 574
  ]
  edge [
    source 32
    target 575
  ]
  edge [
    source 32
    target 576
  ]
  edge [
    source 32
    target 577
  ]
  edge [
    source 32
    target 578
  ]
  edge [
    source 32
    target 579
  ]
  edge [
    source 32
    target 580
  ]
  edge [
    source 32
    target 581
  ]
  edge [
    source 32
    target 582
  ]
  edge [
    source 32
    target 583
  ]
  edge [
    source 32
    target 584
  ]
  edge [
    source 32
    target 585
  ]
  edge [
    source 32
    target 586
  ]
  edge [
    source 32
    target 587
  ]
  edge [
    source 32
    target 588
  ]
  edge [
    source 32
    target 589
  ]
  edge [
    source 32
    target 590
  ]
  edge [
    source 32
    target 591
  ]
  edge [
    source 32
    target 592
  ]
  edge [
    source 32
    target 593
  ]
  edge [
    source 32
    target 594
  ]
  edge [
    source 32
    target 595
  ]
  edge [
    source 32
    target 596
  ]
  edge [
    source 32
    target 597
  ]
  edge [
    source 32
    target 598
  ]
  edge [
    source 32
    target 599
  ]
  edge [
    source 32
    target 600
  ]
  edge [
    source 32
    target 601
  ]
  edge [
    source 32
    target 602
  ]
  edge [
    source 32
    target 603
  ]
  edge [
    source 32
    target 604
  ]
  edge [
    source 32
    target 605
  ]
  edge [
    source 32
    target 606
  ]
  edge [
    source 32
    target 607
  ]
  edge [
    source 32
    target 608
  ]
  edge [
    source 32
    target 609
  ]
  edge [
    source 32
    target 610
  ]
  edge [
    source 32
    target 611
  ]
  edge [
    source 32
    target 612
  ]
  edge [
    source 32
    target 613
  ]
  edge [
    source 32
    target 614
  ]
  edge [
    source 32
    target 615
  ]
  edge [
    source 32
    target 616
  ]
  edge [
    source 32
    target 617
  ]
  edge [
    source 32
    target 618
  ]
  edge [
    source 32
    target 619
  ]
  edge [
    source 32
    target 620
  ]
  edge [
    source 32
    target 621
  ]
  edge [
    source 32
    target 622
  ]
  edge [
    source 32
    target 623
  ]
  edge [
    source 32
    target 624
  ]
  edge [
    source 32
    target 625
  ]
  edge [
    source 32
    target 626
  ]
  edge [
    source 32
    target 627
  ]
  edge [
    source 32
    target 628
  ]
  edge [
    source 32
    target 629
  ]
  edge [
    source 32
    target 630
  ]
  edge [
    source 32
    target 631
  ]
  edge [
    source 32
    target 632
  ]
  edge [
    source 32
    target 633
  ]
  edge [
    source 32
    target 634
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 635
  ]
  edge [
    source 33
    target 636
  ]
  edge [
    source 33
    target 637
  ]
  edge [
    source 33
    target 35
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 638
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 639
  ]
  edge [
    source 36
    target 640
  ]
  edge [
    source 36
    target 641
  ]
  edge [
    source 36
    target 642
  ]
  edge [
    source 36
    target 643
  ]
  edge [
    source 36
    target 644
  ]
  edge [
    source 36
    target 645
  ]
  edge [
    source 36
    target 646
  ]
  edge [
    source 36
    target 647
  ]
  edge [
    source 36
    target 648
  ]
  edge [
    source 36
    target 58
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 649
  ]
  edge [
    source 37
    target 650
  ]
  edge [
    source 37
    target 651
  ]
  edge [
    source 37
    target 652
  ]
  edge [
    source 37
    target 653
  ]
  edge [
    source 37
    target 654
  ]
  edge [
    source 37
    target 655
  ]
  edge [
    source 37
    target 656
  ]
  edge [
    source 37
    target 657
  ]
  edge [
    source 37
    target 658
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 42
  ]
  edge [
    source 38
    target 43
  ]
  edge [
    source 38
    target 295
  ]
  edge [
    source 38
    target 659
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 660
  ]
  edge [
    source 39
    target 661
  ]
  edge [
    source 39
    target 662
  ]
  edge [
    source 39
    target 663
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 50
  ]
  edge [
    source 40
    target 84
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 664
  ]
  edge [
    source 41
    target 216
  ]
  edge [
    source 41
    target 665
  ]
  edge [
    source 41
    target 48
  ]
  edge [
    source 41
    target 53
  ]
  edge [
    source 41
    target 69
  ]
  edge [
    source 41
    target 74
  ]
  edge [
    source 42
    target 666
  ]
  edge [
    source 42
    target 667
  ]
  edge [
    source 42
    target 668
  ]
  edge [
    source 42
    target 669
  ]
  edge [
    source 42
    target 670
  ]
  edge [
    source 42
    target 57
  ]
  edge [
    source 42
    target 62
  ]
  edge [
    source 42
    target 49
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 671
  ]
  edge [
    source 43
    target 206
  ]
  edge [
    source 43
    target 672
  ]
  edge [
    source 43
    target 673
  ]
  edge [
    source 43
    target 674
  ]
  edge [
    source 43
    target 675
  ]
  edge [
    source 43
    target 676
  ]
  edge [
    source 43
    target 677
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 678
  ]
  edge [
    source 44
    target 679
  ]
  edge [
    source 44
    target 680
  ]
  edge [
    source 44
    target 681
  ]
  edge [
    source 44
    target 682
  ]
  edge [
    source 45
    target 78
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 683
  ]
  edge [
    source 46
    target 684
  ]
  edge [
    source 46
    target 685
  ]
  edge [
    source 46
    target 686
  ]
  edge [
    source 46
    target 687
  ]
  edge [
    source 46
    target 676
  ]
  edge [
    source 46
    target 688
  ]
  edge [
    source 46
    target 689
  ]
  edge [
    source 46
    target 690
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 82
  ]
  edge [
    source 47
    target 73
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 691
  ]
  edge [
    source 48
    target 692
  ]
  edge [
    source 48
    target 53
  ]
  edge [
    source 48
    target 69
  ]
  edge [
    source 48
    target 74
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 63
  ]
  edge [
    source 49
    target 60
  ]
  edge [
    source 49
    target 693
  ]
  edge [
    source 49
    target 694
  ]
  edge [
    source 49
    target 695
  ]
  edge [
    source 49
    target 696
  ]
  edge [
    source 49
    target 697
  ]
  edge [
    source 49
    target 698
  ]
  edge [
    source 49
    target 699
  ]
  edge [
    source 49
    target 700
  ]
  edge [
    source 49
    target 701
  ]
  edge [
    source 49
    target 57
  ]
  edge [
    source 49
    target 62
  ]
  edge [
    source 50
    target 80
  ]
  edge [
    source 50
    target 702
  ]
  edge [
    source 50
    target 703
  ]
  edge [
    source 50
    target 704
  ]
  edge [
    source 50
    target 705
  ]
  edge [
    source 50
    target 706
  ]
  edge [
    source 50
    target 707
  ]
  edge [
    source 50
    target 708
  ]
  edge [
    source 50
    target 84
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 709
  ]
  edge [
    source 51
    target 710
  ]
  edge [
    source 51
    target 200
  ]
  edge [
    source 51
    target 711
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 69
  ]
  edge [
    source 53
    target 74
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 712
  ]
  edge [
    source 54
    target 713
  ]
  edge [
    source 54
    target 714
  ]
  edge [
    source 54
    target 715
  ]
  edge [
    source 54
    target 716
  ]
  edge [
    source 54
    target 717
  ]
  edge [
    source 54
    target 718
  ]
  edge [
    source 54
    target 719
  ]
  edge [
    source 54
    target 720
  ]
  edge [
    source 54
    target 721
  ]
  edge [
    source 54
    target 722
  ]
  edge [
    source 54
    target 723
  ]
  edge [
    source 54
    target 724
  ]
  edge [
    source 54
    target 725
  ]
  edge [
    source 54
    target 282
  ]
  edge [
    source 54
    target 726
  ]
  edge [
    source 54
    target 56
  ]
  edge [
    source 54
    target 57
  ]
  edge [
    source 54
    target 58
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 727
  ]
  edge [
    source 55
    target 728
  ]
  edge [
    source 55
    target 729
  ]
  edge [
    source 55
    target 730
  ]
  edge [
    source 55
    target 244
  ]
  edge [
    source 55
    target 262
  ]
  edge [
    source 55
    target 731
  ]
  edge [
    source 55
    target 732
  ]
  edge [
    source 55
    target 733
  ]
  edge [
    source 55
    target 734
  ]
  edge [
    source 55
    target 57
  ]
  edge [
    source 55
    target 58
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 85
  ]
  edge [
    source 56
    target 735
  ]
  edge [
    source 56
    target 736
  ]
  edge [
    source 56
    target 737
  ]
  edge [
    source 56
    target 244
  ]
  edge [
    source 56
    target 738
  ]
  edge [
    source 56
    target 739
  ]
  edge [
    source 56
    target 740
  ]
  edge [
    source 56
    target 741
  ]
  edge [
    source 56
    target 742
  ]
  edge [
    source 56
    target 743
  ]
  edge [
    source 56
    target 744
  ]
  edge [
    source 56
    target 745
  ]
  edge [
    source 56
    target 746
  ]
  edge [
    source 56
    target 747
  ]
  edge [
    source 56
    target 58
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 748
  ]
  edge [
    source 57
    target 749
  ]
  edge [
    source 57
    target 750
  ]
  edge [
    source 57
    target 751
  ]
  edge [
    source 57
    target 752
  ]
  edge [
    source 57
    target 753
  ]
  edge [
    source 57
    target 754
  ]
  edge [
    source 57
    target 755
  ]
  edge [
    source 57
    target 756
  ]
  edge [
    source 57
    target 757
  ]
  edge [
    source 57
    target 758
  ]
  edge [
    source 57
    target 759
  ]
  edge [
    source 57
    target 760
  ]
  edge [
    source 57
    target 761
  ]
  edge [
    source 57
    target 62
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 762
  ]
  edge [
    source 58
    target 763
  ]
  edge [
    source 58
    target 764
  ]
  edge [
    source 58
    target 648
  ]
  edge [
    source 59
    target 82
  ]
  edge [
    source 59
    target 765
  ]
  edge [
    source 59
    target 766
  ]
  edge [
    source 59
    target 72
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 79
  ]
  edge [
    source 60
    target 767
  ]
  edge [
    source 60
    target 768
  ]
  edge [
    source 60
    target 769
  ]
  edge [
    source 60
    target 770
  ]
  edge [
    source 60
    target 771
  ]
  edge [
    source 60
    target 772
  ]
  edge [
    source 60
    target 773
  ]
  edge [
    source 60
    target 774
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 66
  ]
  edge [
    source 61
    target 67
  ]
  edge [
    source 62
    target 775
  ]
  edge [
    source 62
    target 776
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 777
  ]
  edge [
    source 63
    target 778
  ]
  edge [
    source 63
    target 303
  ]
  edge [
    source 63
    target 779
  ]
  edge [
    source 63
    target 780
  ]
  edge [
    source 63
    target 781
  ]
  edge [
    source 63
    target 782
  ]
  edge [
    source 63
    target 783
  ]
  edge [
    source 63
    target 784
  ]
  edge [
    source 63
    target 785
  ]
  edge [
    source 63
    target 786
  ]
  edge [
    source 63
    target 787
  ]
  edge [
    source 63
    target 788
  ]
  edge [
    source 63
    target 789
  ]
  edge [
    source 63
    target 790
  ]
  edge [
    source 63
    target 791
  ]
  edge [
    source 63
    target 792
  ]
  edge [
    source 63
    target 299
  ]
  edge [
    source 63
    target 793
  ]
  edge [
    source 63
    target 794
  ]
  edge [
    source 63
    target 795
  ]
  edge [
    source 63
    target 796
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 797
  ]
  edge [
    source 64
    target 798
  ]
  edge [
    source 64
    target 799
  ]
  edge [
    source 64
    target 800
  ]
  edge [
    source 64
    target 801
  ]
  edge [
    source 64
    target 769
  ]
  edge [
    source 64
    target 128
  ]
  edge [
    source 64
    target 802
  ]
  edge [
    source 64
    target 75
  ]
  edge [
    source 64
    target 803
  ]
  edge [
    source 64
    target 804
  ]
  edge [
    source 64
    target 805
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 806
  ]
  edge [
    source 65
    target 807
  ]
  edge [
    source 65
    target 808
  ]
  edge [
    source 65
    target 809
  ]
  edge [
    source 65
    target 810
  ]
  edge [
    source 65
    target 811
  ]
  edge [
    source 65
    target 812
  ]
  edge [
    source 65
    target 813
  ]
  edge [
    source 65
    target 814
  ]
  edge [
    source 65
    target 815
  ]
  edge [
    source 66
    target 816
  ]
  edge [
    source 66
    target 817
  ]
  edge [
    source 66
    target 818
  ]
  edge [
    source 66
    target 819
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 67
    target 820
  ]
  edge [
    source 67
    target 821
  ]
  edge [
    source 67
    target 822
  ]
  edge [
    source 67
    target 823
  ]
  edge [
    source 67
    target 927
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 68
    target 824
  ]
  edge [
    source 68
    target 825
  ]
  edge [
    source 68
    target 826
  ]
  edge [
    source 68
    target 827
  ]
  edge [
    source 68
    target 828
  ]
  edge [
    source 68
    target 829
  ]
  edge [
    source 68
    target 830
  ]
  edge [
    source 68
    target 927
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 69
    target 831
  ]
  edge [
    source 69
    target 832
  ]
  edge [
    source 69
    target 833
  ]
  edge [
    source 69
    target 834
  ]
  edge [
    source 69
    target 835
  ]
  edge [
    source 69
    target 269
  ]
  edge [
    source 69
    target 836
  ]
  edge [
    source 69
    target 823
  ]
  edge [
    source 69
    target 837
  ]
  edge [
    source 69
    target 838
  ]
  edge [
    source 69
    target 588
  ]
  edge [
    source 69
    target 839
  ]
  edge [
    source 69
    target 840
  ]
  edge [
    source 69
    target 841
  ]
  edge [
    source 69
    target 842
  ]
  edge [
    source 69
    target 843
  ]
  edge [
    source 69
    target 844
  ]
  edge [
    source 69
    target 845
  ]
  edge [
    source 69
    target 846
  ]
  edge [
    source 69
    target 847
  ]
  edge [
    source 69
    target 848
  ]
  edge [
    source 69
    target 849
  ]
  edge [
    source 69
    target 850
  ]
  edge [
    source 69
    target 851
  ]
  edge [
    source 69
    target 852
  ]
  edge [
    source 69
    target 74
  ]
  edge [
    source 69
    target 928
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 853
  ]
  edge [
    source 70
    target 854
  ]
  edge [
    source 70
    target 855
  ]
  edge [
    source 70
    target 856
  ]
  edge [
    source 70
    target 857
  ]
  edge [
    source 70
    target 928
  ]
  edge [
    source 72
    target 73
  ]
  edge [
    source 72
    target 858
  ]
  edge [
    source 72
    target 859
  ]
  edge [
    source 72
    target 860
  ]
  edge [
    source 72
    target 861
  ]
  edge [
    source 72
    target 862
  ]
  edge [
    source 72
    target 89
  ]
  edge [
    source 72
    target 863
  ]
  edge [
    source 72
    target 864
  ]
  edge [
    source 72
    target 865
  ]
  edge [
    source 72
    target 866
  ]
  edge [
    source 72
    target 867
  ]
  edge [
    source 72
    target 868
  ]
  edge [
    source 72
    target 869
  ]
  edge [
    source 72
    target 870
  ]
  edge [
    source 72
    target 196
  ]
  edge [
    source 72
    target 871
  ]
  edge [
    source 72
    target 872
  ]
  edge [
    source 72
    target 873
  ]
  edge [
    source 72
    target 874
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 73
    target 875
  ]
  edge [
    source 73
    target 876
  ]
  edge [
    source 73
    target 877
  ]
  edge [
    source 73
    target 681
  ]
  edge [
    source 73
    target 878
  ]
  edge [
    source 73
    target 678
  ]
  edge [
    source 73
    target 879
  ]
  edge [
    source 75
    target 76
  ]
  edge [
    source 75
    target 880
  ]
  edge [
    source 75
    target 881
  ]
  edge [
    source 75
    target 266
  ]
  edge [
    source 77
    target 882
  ]
  edge [
    source 77
    target 883
  ]
  edge [
    source 78
    target 884
  ]
  edge [
    source 78
    target 299
  ]
  edge [
    source 78
    target 885
  ]
  edge [
    source 78
    target 886
  ]
  edge [
    source 78
    target 887
  ]
  edge [
    source 78
    target 888
  ]
  edge [
    source 78
    target 889
  ]
  edge [
    source 78
    target 890
  ]
  edge [
    source 78
    target 891
  ]
  edge [
    source 78
    target 678
  ]
  edge [
    source 78
    target 892
  ]
  edge [
    source 78
    target 782
  ]
  edge [
    source 79
    target 204
  ]
  edge [
    source 79
    target 92
  ]
  edge [
    source 80
    target 81
  ]
  edge [
    source 80
    target 893
  ]
  edge [
    source 80
    target 894
  ]
  edge [
    source 80
    target 895
  ]
  edge [
    source 80
    target 896
  ]
  edge [
    source 80
    target 897
  ]
  edge [
    source 80
    target 898
  ]
  edge [
    source 80
    target 156
  ]
  edge [
    source 80
    target 899
  ]
  edge [
    source 80
    target 900
  ]
  edge [
    source 81
    target 760
  ]
  edge [
    source 81
    target 82
  ]
  edge [
    source 83
    target 84
  ]
  edge [
    source 83
    target 901
  ]
  edge [
    source 83
    target 902
  ]
  edge [
    source 83
    target 903
  ]
  edge [
    source 83
    target 904
  ]
  edge [
    source 83
    target 905
  ]
  edge [
    source 84
    target 906
  ]
  edge [
    source 84
    target 907
  ]
  edge [
    source 84
    target 908
  ]
  edge [
    source 84
    target 909
  ]
  edge [
    source 84
    target 910
  ]
  edge [
    source 84
    target 911
  ]
  edge [
    source 84
    target 912
  ]
  edge [
    source 84
    target 703
  ]
  edge [
    source 84
    target 913
  ]
  edge [
    source 84
    target 914
  ]
  edge [
    source 84
    target 698
  ]
  edge [
    source 84
    target 915
  ]
  edge [
    source 84
    target 916
  ]
  edge [
    source 84
    target 917
  ]
  edge [
    source 84
    target 918
  ]
  edge [
    source 84
    target 919
  ]
  edge [
    source 84
    target 920
  ]
  edge [
    source 84
    target 921
  ]
  edge [
    source 84
    target 922
  ]
  edge [
    source 84
    target 923
  ]
  edge [
    source 924
    target 925
  ]
  edge [
    source 924
    target 926
  ]
  edge [
    source 925
    target 926
  ]
  edge [
    source 925
    target 934
  ]
  edge [
    source 925
    target 935
  ]
  edge [
    source 929
    target 930
  ]
  edge [
    source 929
    target 931
  ]
  edge [
    source 930
    target 931
  ]
  edge [
    source 932
    target 933
  ]
  edge [
    source 934
    target 935
  ]
  edge [
    source 936
    target 937
  ]
  edge [
    source 936
    target 938
  ]
  edge [
    source 936
    target 939
  ]
  edge [
    source 936
    target 940
  ]
  edge [
    source 937
    target 938
  ]
  edge [
    source 937
    target 939
  ]
  edge [
    source 937
    target 940
  ]
  edge [
    source 938
    target 939
  ]
  edge [
    source 938
    target 940
  ]
  edge [
    source 939
    target 940
  ]
]
