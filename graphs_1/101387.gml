graph [
  maxDegree 2
  minDegree 1
  meanDegree 1.4285714285714286
  density 0.07142857142857142
  graphCliqueNumber 3
  node [
    id 0
    label "craig"
    origin "text"
  ]
  node [
    id 1
    label "wolanin"
    origin "text"
  ]
  node [
    id 2
    label "Craig"
  ]
  node [
    id 3
    label "Wolanin"
  ]
  node [
    id 4
    label "Grosse"
  ]
  node [
    id 5
    label "Pointe"
  ]
  node [
    id 6
    label "Newa"
  ]
  node [
    id 7
    label "jersey"
  ]
  node [
    id 8
    label "devils"
  ]
  node [
    id 9
    label "Quebec"
  ]
  node [
    id 10
    label "Nordiques"
  ]
  node [
    id 11
    label "Toronto"
  ]
  node [
    id 12
    label "Maple"
  ]
  node [
    id 13
    label "Leafs"
  ]
  node [
    id 14
    label "Tampa"
  ]
  node [
    id 15
    label "Bay"
  ]
  node [
    id 16
    label "Lightning"
  ]
  node [
    id 17
    label "Colorado"
  ]
  node [
    id 18
    label "Avalanche"
  ]
  node [
    id 19
    label "puchar"
  ]
  node [
    id 20
    label "Stanley"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 13
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 19
    target 20
  ]
]
