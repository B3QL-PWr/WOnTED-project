graph [
  maxDegree 41
  minDegree 1
  meanDegree 2.215686274509804
  density 0.010914710711870956
  graphCliqueNumber 2
  node [
    id 0
    label "po&#322;owa"
    origin "text"
  ]
  node [
    id 1
    label "firma"
    origin "text"
  ]
  node [
    id 2
    label "uwa&#380;a&#263;"
    origin "text"
  ]
  node [
    id 3
    label "warunek"
    origin "text"
  ]
  node [
    id 4
    label "prowadzenie"
    origin "text"
  ]
  node [
    id 5
    label "biznes"
    origin "text"
  ]
  node [
    id 6
    label "rok"
    origin "text"
  ]
  node [
    id 7
    label "pogorszy&#263;"
    origin "text"
  ]
  node [
    id 8
    label "si&#281;"
    origin "text"
  ]
  node [
    id 9
    label "por&#243;wnanie"
    origin "text"
  ]
  node [
    id 10
    label "lato"
    origin "text"
  ]
  node [
    id 11
    label "poprzedni"
    origin "text"
  ]
  node [
    id 12
    label "wynika&#263;"
    origin "text"
  ]
  node [
    id 13
    label "badanie"
    origin "text"
  ]
  node [
    id 14
    label "nastr&#243;j"
    origin "text"
  ]
  node [
    id 15
    label "przedsi&#281;biorca"
    origin "text"
  ]
  node [
    id 16
    label "zrealizowa&#263;"
    origin "text"
  ]
  node [
    id 17
    label "zlecenie"
    origin "text"
  ]
  node [
    id 18
    label "konfederacja"
    origin "text"
  ]
  node [
    id 19
    label "lewiatan"
    origin "text"
  ]
  node [
    id 20
    label "medium"
  ]
  node [
    id 21
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 22
    label "czas"
  ]
  node [
    id 23
    label "cz&#322;owiek"
  ]
  node [
    id 24
    label "Hortex"
  ]
  node [
    id 25
    label "MAC"
  ]
  node [
    id 26
    label "reengineering"
  ]
  node [
    id 27
    label "nazwa_w&#322;asna"
  ]
  node [
    id 28
    label "podmiot_gospodarczy"
  ]
  node [
    id 29
    label "Google"
  ]
  node [
    id 30
    label "zaufanie"
  ]
  node [
    id 31
    label "biurowiec"
  ]
  node [
    id 32
    label "networking"
  ]
  node [
    id 33
    label "zasoby_ludzkie"
  ]
  node [
    id 34
    label "interes"
  ]
  node [
    id 35
    label "paczkarnia"
  ]
  node [
    id 36
    label "Canon"
  ]
  node [
    id 37
    label "HP"
  ]
  node [
    id 38
    label "Baltona"
  ]
  node [
    id 39
    label "Pewex"
  ]
  node [
    id 40
    label "MAN_SE"
  ]
  node [
    id 41
    label "Apeks"
  ]
  node [
    id 42
    label "zasoby"
  ]
  node [
    id 43
    label "Orbis"
  ]
  node [
    id 44
    label "miejsce_pracy"
  ]
  node [
    id 45
    label "siedziba"
  ]
  node [
    id 46
    label "Spo&#322;em"
  ]
  node [
    id 47
    label "przedsi&#281;biorczo&#347;&#263;"
  ]
  node [
    id 48
    label "Orlen"
  ]
  node [
    id 49
    label "klasa"
  ]
  node [
    id 50
    label "continue"
  ]
  node [
    id 51
    label "str&#243;&#380;y&#263;"
  ]
  node [
    id 52
    label "consider"
  ]
  node [
    id 53
    label "my&#347;le&#263;"
  ]
  node [
    id 54
    label "pilnowa&#263;"
  ]
  node [
    id 55
    label "robi&#263;"
  ]
  node [
    id 56
    label "uznawa&#263;"
  ]
  node [
    id 57
    label "obserwowa&#263;"
  ]
  node [
    id 58
    label "skupia&#263;_si&#281;"
  ]
  node [
    id 59
    label "mie&#263;_na_baczenie"
  ]
  node [
    id 60
    label "deliver"
  ]
  node [
    id 61
    label "za&#322;o&#380;enie"
  ]
  node [
    id 62
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 63
    label "umowa"
  ]
  node [
    id 64
    label "agent"
  ]
  node [
    id 65
    label "condition"
  ]
  node [
    id 66
    label "ekspozycja"
  ]
  node [
    id 67
    label "faktor"
  ]
  node [
    id 68
    label "robienie"
  ]
  node [
    id 69
    label "przywodzenie"
  ]
  node [
    id 70
    label "prowadzanie"
  ]
  node [
    id 71
    label "ukierunkowywanie"
  ]
  node [
    id 72
    label "kszta&#322;towanie"
  ]
  node [
    id 73
    label "poprowadzenie"
  ]
  node [
    id 74
    label "wprowadzanie"
  ]
  node [
    id 75
    label "dysponowanie"
  ]
  node [
    id 76
    label "przeci&#261;ganie"
  ]
  node [
    id 77
    label "doprowadzanie"
  ]
  node [
    id 78
    label "wprowadzenie"
  ]
  node [
    id 79
    label "eksponowanie"
  ]
  node [
    id 80
    label "oprowadzenie"
  ]
  node [
    id 81
    label "trzymanie"
  ]
  node [
    id 82
    label "ta&#324;czenie"
  ]
  node [
    id 83
    label "przeci&#281;cie"
  ]
  node [
    id 84
    label "przewy&#380;szanie"
  ]
  node [
    id 85
    label "prowadzi&#263;"
  ]
  node [
    id 86
    label "aim"
  ]
  node [
    id 87
    label "czynno&#347;&#263;"
  ]
  node [
    id 88
    label "zwracanie"
  ]
  node [
    id 89
    label "ci&#261;gni&#281;cie_si&#281;"
  ]
  node [
    id 90
    label "przecinanie"
  ]
  node [
    id 91
    label "sterowanie"
  ]
  node [
    id 92
    label "drive"
  ]
  node [
    id 93
    label "kre&#347;lenie"
  ]
  node [
    id 94
    label "management"
  ]
  node [
    id 95
    label "dawanie"
  ]
  node [
    id 96
    label "oprowadzanie"
  ]
  node [
    id 97
    label "pozarz&#261;dzanie"
  ]
  node [
    id 98
    label "g&#243;rowanie"
  ]
  node [
    id 99
    label "linia_melodyczna"
  ]
  node [
    id 100
    label "granie"
  ]
  node [
    id 101
    label "doprowadzenie"
  ]
  node [
    id 102
    label "kierowanie"
  ]
  node [
    id 103
    label "zaprowadzanie"
  ]
  node [
    id 104
    label "lead"
  ]
  node [
    id 105
    label "powodowanie"
  ]
  node [
    id 106
    label "krzywa"
  ]
  node [
    id 107
    label "rynek"
  ]
  node [
    id 108
    label "korzy&#347;&#263;"
  ]
  node [
    id 109
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 110
    label "przedsi&#281;wzi&#281;cie"
  ]
  node [
    id 111
    label "object"
  ]
  node [
    id 112
    label "sprawa"
  ]
  node [
    id 113
    label "stulecie"
  ]
  node [
    id 114
    label "kalendarz"
  ]
  node [
    id 115
    label "pora_roku"
  ]
  node [
    id 116
    label "cykl_astronomiczny"
  ]
  node [
    id 117
    label "p&#243;&#322;rocze"
  ]
  node [
    id 118
    label "grupa"
  ]
  node [
    id 119
    label "kwarta&#322;"
  ]
  node [
    id 120
    label "kurs"
  ]
  node [
    id 121
    label "jubileusz"
  ]
  node [
    id 122
    label "miesi&#261;c"
  ]
  node [
    id 123
    label "lata"
  ]
  node [
    id 124
    label "martwy_sezon"
  ]
  node [
    id 125
    label "worsen"
  ]
  node [
    id 126
    label "zmieni&#263;"
  ]
  node [
    id 127
    label "simile"
  ]
  node [
    id 128
    label "figura_stylistyczna"
  ]
  node [
    id 129
    label "zestawienie"
  ]
  node [
    id 130
    label "zr&#243;&#380;nicowanie"
  ]
  node [
    id 131
    label "comparison"
  ]
  node [
    id 132
    label "zanalizowanie"
  ]
  node [
    id 133
    label "poprzednio"
  ]
  node [
    id 134
    label "przesz&#322;y"
  ]
  node [
    id 135
    label "wcze&#347;niejszy"
  ]
  node [
    id 136
    label "okazywa&#263;_si&#281;"
  ]
  node [
    id 137
    label "rise"
  ]
  node [
    id 138
    label "appear"
  ]
  node [
    id 139
    label "rodzi&#263;_si&#281;"
  ]
  node [
    id 140
    label "usi&#322;owanie"
  ]
  node [
    id 141
    label "examination"
  ]
  node [
    id 142
    label "investigation"
  ]
  node [
    id 143
    label "ustalenie"
  ]
  node [
    id 144
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 145
    label "ustalanie"
  ]
  node [
    id 146
    label "bia&#322;a_niedziela"
  ]
  node [
    id 147
    label "analysis"
  ]
  node [
    id 148
    label "rozpatrywanie"
  ]
  node [
    id 149
    label "wziernikowanie"
  ]
  node [
    id 150
    label "obserwowanie"
  ]
  node [
    id 151
    label "omawianie"
  ]
  node [
    id 152
    label "sprawdzanie"
  ]
  node [
    id 153
    label "udowadnianie"
  ]
  node [
    id 154
    label "diagnostyka"
  ]
  node [
    id 155
    label "macanie"
  ]
  node [
    id 156
    label "rektalny"
  ]
  node [
    id 157
    label "penetrowanie"
  ]
  node [
    id 158
    label "krytykowanie"
  ]
  node [
    id 159
    label "kontrola"
  ]
  node [
    id 160
    label "dociekanie"
  ]
  node [
    id 161
    label "zrecenzowanie"
  ]
  node [
    id 162
    label "praca"
  ]
  node [
    id 163
    label "rezultat"
  ]
  node [
    id 164
    label "kwas"
  ]
  node [
    id 165
    label "stan"
  ]
  node [
    id 166
    label "cecha"
  ]
  node [
    id 167
    label "klimat"
  ]
  node [
    id 168
    label "state"
  ]
  node [
    id 169
    label "samopoczucie"
  ]
  node [
    id 170
    label "charakter"
  ]
  node [
    id 171
    label "wydawca"
  ]
  node [
    id 172
    label "kapitalista"
  ]
  node [
    id 173
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 174
    label "osoba_fizyczna"
  ]
  node [
    id 175
    label "wsp&#243;lnik"
  ]
  node [
    id 176
    label "klasa_&#347;rednia"
  ]
  node [
    id 177
    label "spowodowa&#263;"
  ]
  node [
    id 178
    label "stworzy&#263;"
  ]
  node [
    id 179
    label "manufacture"
  ]
  node [
    id 180
    label "actualize"
  ]
  node [
    id 181
    label "realize"
  ]
  node [
    id 182
    label "wykorzysta&#263;"
  ]
  node [
    id 183
    label "spieni&#281;&#380;y&#263;"
  ]
  node [
    id 184
    label "spadni&#281;cie"
  ]
  node [
    id 185
    label "zbiegni&#281;cie"
  ]
  node [
    id 186
    label "odebranie"
  ]
  node [
    id 187
    label "odebra&#263;"
  ]
  node [
    id 188
    label "odbieranie"
  ]
  node [
    id 189
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 190
    label "decree"
  ]
  node [
    id 191
    label "undertaking"
  ]
  node [
    id 192
    label "odbiera&#263;"
  ]
  node [
    id 193
    label "polecenie"
  ]
  node [
    id 194
    label "zwi&#261;zek"
  ]
  node [
    id 195
    label "organizacja"
  ]
  node [
    id 196
    label "pa&#324;stwo_z&#322;o&#380;one"
  ]
  node [
    id 197
    label "konfederacja_barska"
  ]
  node [
    id 198
    label "Unia_Europejska"
  ]
  node [
    id 199
    label "alliance"
  ]
  node [
    id 200
    label "combination"
  ]
  node [
    id 201
    label "Konfederacja"
  ]
  node [
    id 202
    label "biblizm"
  ]
  node [
    id 203
    label "potw&#243;r"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 22
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 125
  ]
  edge [
    source 7
    target 126
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 129
  ]
  edge [
    source 9
    target 130
  ]
  edge [
    source 9
    target 131
  ]
  edge [
    source 9
    target 132
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 133
  ]
  edge [
    source 11
    target 134
  ]
  edge [
    source 11
    target 135
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 136
  ]
  edge [
    source 12
    target 137
  ]
  edge [
    source 12
    target 138
  ]
  edge [
    source 12
    target 139
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 140
  ]
  edge [
    source 13
    target 141
  ]
  edge [
    source 13
    target 142
  ]
  edge [
    source 13
    target 143
  ]
  edge [
    source 13
    target 144
  ]
  edge [
    source 13
    target 145
  ]
  edge [
    source 13
    target 146
  ]
  edge [
    source 13
    target 147
  ]
  edge [
    source 13
    target 148
  ]
  edge [
    source 13
    target 149
  ]
  edge [
    source 13
    target 150
  ]
  edge [
    source 13
    target 151
  ]
  edge [
    source 13
    target 152
  ]
  edge [
    source 13
    target 153
  ]
  edge [
    source 13
    target 154
  ]
  edge [
    source 13
    target 87
  ]
  edge [
    source 13
    target 155
  ]
  edge [
    source 13
    target 156
  ]
  edge [
    source 13
    target 157
  ]
  edge [
    source 13
    target 158
  ]
  edge [
    source 13
    target 159
  ]
  edge [
    source 13
    target 160
  ]
  edge [
    source 13
    target 161
  ]
  edge [
    source 13
    target 162
  ]
  edge [
    source 13
    target 163
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 164
  ]
  edge [
    source 14
    target 165
  ]
  edge [
    source 14
    target 166
  ]
  edge [
    source 14
    target 167
  ]
  edge [
    source 14
    target 168
  ]
  edge [
    source 14
    target 169
  ]
  edge [
    source 14
    target 170
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 171
  ]
  edge [
    source 15
    target 172
  ]
  edge [
    source 15
    target 173
  ]
  edge [
    source 15
    target 174
  ]
  edge [
    source 15
    target 175
  ]
  edge [
    source 15
    target 176
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 177
  ]
  edge [
    source 16
    target 178
  ]
  edge [
    source 16
    target 179
  ]
  edge [
    source 16
    target 180
  ]
  edge [
    source 16
    target 181
  ]
  edge [
    source 16
    target 182
  ]
  edge [
    source 16
    target 183
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 184
  ]
  edge [
    source 17
    target 185
  ]
  edge [
    source 17
    target 186
  ]
  edge [
    source 17
    target 187
  ]
  edge [
    source 17
    target 188
  ]
  edge [
    source 17
    target 189
  ]
  edge [
    source 17
    target 190
  ]
  edge [
    source 17
    target 191
  ]
  edge [
    source 17
    target 192
  ]
  edge [
    source 17
    target 193
  ]
  edge [
    source 17
    target 162
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 194
  ]
  edge [
    source 18
    target 195
  ]
  edge [
    source 18
    target 196
  ]
  edge [
    source 18
    target 197
  ]
  edge [
    source 18
    target 198
  ]
  edge [
    source 18
    target 199
  ]
  edge [
    source 18
    target 200
  ]
  edge [
    source 18
    target 201
  ]
  edge [
    source 19
    target 202
  ]
  edge [
    source 19
    target 203
  ]
]
