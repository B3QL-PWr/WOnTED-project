graph [
  maxDegree 21
  minDegree 1
  meanDegree 2.017094017094017
  density 0.017388741526672562
  graphCliqueNumber 3
  node [
    id 0
    label "pisa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "kilka"
    origin "text"
  ]
  node [
    id 2
    label "dni"
    origin "text"
  ]
  node [
    id 3
    label "temu"
    origin "text"
  ]
  node [
    id 4
    label "nowa"
    origin "text"
  ]
  node [
    id 5
    label "wersja"
    origin "text"
  ]
  node [
    id 6
    label "firefoksa"
    origin "text"
  ]
  node [
    id 7
    label "skupi&#263;by&#263;"
    origin "text"
  ]
  node [
    id 8
    label "si&#281;"
    origin "text"
  ]
  node [
    id 9
    label "kwestia"
    origin "text"
  ]
  node [
    id 10
    label "otwarto&#347;&#263;"
    origin "text"
  ]
  node [
    id 11
    label "funkcjonalno&#347;&#263;"
    origin "text"
  ]
  node [
    id 12
    label "atrakcja"
    origin "text"
  ]
  node [
    id 13
    label "przemawia&#263;"
    origin "text"
  ]
  node [
    id 14
    label "korzysta&#263;"
    origin "text"
  ]
  node [
    id 15
    label "ten"
    origin "text"
  ]
  node [
    id 16
    label "przegl&#261;darka"
    origin "text"
  ]
  node [
    id 17
    label "zmieni&#263;by&#263;"
    origin "text"
  ]
  node [
    id 18
    label "zdanie"
    origin "text"
  ]
  node [
    id 19
    label "zas&#322;ugiwa&#263;"
    origin "text"
  ]
  node [
    id 20
    label "uwaga"
    origin "text"
  ]
  node [
    id 21
    label "ozdabia&#263;"
  ]
  node [
    id 22
    label "dysgrafia"
  ]
  node [
    id 23
    label "prasa"
  ]
  node [
    id 24
    label "spell"
  ]
  node [
    id 25
    label "skryba"
  ]
  node [
    id 26
    label "donosi&#263;"
  ]
  node [
    id 27
    label "code"
  ]
  node [
    id 28
    label "tekst"
  ]
  node [
    id 29
    label "dysortografia"
  ]
  node [
    id 30
    label "read"
  ]
  node [
    id 31
    label "tworzy&#263;"
  ]
  node [
    id 32
    label "formu&#322;owa&#263;"
  ]
  node [
    id 33
    label "styl"
  ]
  node [
    id 34
    label "stawia&#263;"
  ]
  node [
    id 35
    label "&#347;ledziowate"
  ]
  node [
    id 36
    label "ryba"
  ]
  node [
    id 37
    label "czas"
  ]
  node [
    id 38
    label "gwiazda"
  ]
  node [
    id 39
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 40
    label "typ"
  ]
  node [
    id 41
    label "posta&#263;"
  ]
  node [
    id 42
    label "sprawa"
  ]
  node [
    id 43
    label "problemat"
  ]
  node [
    id 44
    label "wypowied&#378;"
  ]
  node [
    id 45
    label "dialog"
  ]
  node [
    id 46
    label "problematyka"
  ]
  node [
    id 47
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 48
    label "subject"
  ]
  node [
    id 49
    label "gotowo&#347;&#263;"
  ]
  node [
    id 50
    label "frankness"
  ]
  node [
    id 51
    label "cecha"
  ]
  node [
    id 52
    label "jawno&#347;&#263;"
  ]
  node [
    id 53
    label "rozmiar"
  ]
  node [
    id 54
    label "nieograniczono&#347;&#263;"
  ]
  node [
    id 55
    label "kontaktowo&#347;&#263;"
  ]
  node [
    id 56
    label "prostoduszno&#347;&#263;"
  ]
  node [
    id 57
    label "u&#380;ytkowo&#347;&#263;"
  ]
  node [
    id 58
    label "functionality"
  ]
  node [
    id 59
    label "praktyczno&#347;&#263;"
  ]
  node [
    id 60
    label "urozmaicenie"
  ]
  node [
    id 61
    label "sensacja"
  ]
  node [
    id 62
    label "ciekawostka"
  ]
  node [
    id 63
    label "punkt"
  ]
  node [
    id 64
    label "urok"
  ]
  node [
    id 65
    label "spoke"
  ]
  node [
    id 66
    label "wyg&#322;asza&#263;"
  ]
  node [
    id 67
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 68
    label "say"
  ]
  node [
    id 69
    label "wydobywa&#263;"
  ]
  node [
    id 70
    label "uzewn&#281;trznia&#263;_si&#281;"
  ]
  node [
    id 71
    label "zaczyna&#263;"
  ]
  node [
    id 72
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 73
    label "talk"
  ]
  node [
    id 74
    label "address"
  ]
  node [
    id 75
    label "use"
  ]
  node [
    id 76
    label "uzyskiwa&#263;"
  ]
  node [
    id 77
    label "u&#380;ywa&#263;"
  ]
  node [
    id 78
    label "okre&#347;lony"
  ]
  node [
    id 79
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 80
    label "viewer"
  ]
  node [
    id 81
    label "przyrz&#261;d"
  ]
  node [
    id 82
    label "program"
  ]
  node [
    id 83
    label "browser"
  ]
  node [
    id 84
    label "projektor"
  ]
  node [
    id 85
    label "attitude"
  ]
  node [
    id 86
    label "system"
  ]
  node [
    id 87
    label "przedstawienie"
  ]
  node [
    id 88
    label "fraza"
  ]
  node [
    id 89
    label "prison_term"
  ]
  node [
    id 90
    label "adjudication"
  ]
  node [
    id 91
    label "przekazanie"
  ]
  node [
    id 92
    label "pass"
  ]
  node [
    id 93
    label "wyra&#380;enie"
  ]
  node [
    id 94
    label "okres"
  ]
  node [
    id 95
    label "z&#322;o&#380;enie_egzaminu"
  ]
  node [
    id 96
    label "wypowiedzenie"
  ]
  node [
    id 97
    label "konektyw"
  ]
  node [
    id 98
    label "zaliczenie"
  ]
  node [
    id 99
    label "stanowisko"
  ]
  node [
    id 100
    label "powierzenie"
  ]
  node [
    id 101
    label "antylogizm"
  ]
  node [
    id 102
    label "zmuszenie"
  ]
  node [
    id 103
    label "szko&#322;a"
  ]
  node [
    id 104
    label "addition"
  ]
  node [
    id 105
    label "nagana"
  ]
  node [
    id 106
    label "stan"
  ]
  node [
    id 107
    label "dzienniczek"
  ]
  node [
    id 108
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 109
    label "wzgl&#261;d"
  ]
  node [
    id 110
    label "gossip"
  ]
  node [
    id 111
    label "upomnienie"
  ]
  node [
    id 112
    label "federacja"
  ]
  node [
    id 113
    label "biblioteka"
  ]
  node [
    id 114
    label "cyfrowy"
  ]
  node [
    id 115
    label "OAI"
  ]
  node [
    id 116
    label "PMH"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 42
  ]
  edge [
    source 9
    target 43
  ]
  edge [
    source 9
    target 44
  ]
  edge [
    source 9
    target 45
  ]
  edge [
    source 9
    target 46
  ]
  edge [
    source 9
    target 47
  ]
  edge [
    source 9
    target 48
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 49
  ]
  edge [
    source 10
    target 50
  ]
  edge [
    source 10
    target 51
  ]
  edge [
    source 10
    target 52
  ]
  edge [
    source 10
    target 53
  ]
  edge [
    source 10
    target 54
  ]
  edge [
    source 10
    target 55
  ]
  edge [
    source 10
    target 41
  ]
  edge [
    source 10
    target 56
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 57
  ]
  edge [
    source 11
    target 58
  ]
  edge [
    source 11
    target 59
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 60
  ]
  edge [
    source 12
    target 61
  ]
  edge [
    source 12
    target 62
  ]
  edge [
    source 12
    target 63
  ]
  edge [
    source 12
    target 64
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 65
  ]
  edge [
    source 13
    target 66
  ]
  edge [
    source 13
    target 67
  ]
  edge [
    source 13
    target 68
  ]
  edge [
    source 13
    target 69
  ]
  edge [
    source 13
    target 70
  ]
  edge [
    source 13
    target 71
  ]
  edge [
    source 13
    target 72
  ]
  edge [
    source 13
    target 73
  ]
  edge [
    source 13
    target 74
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 75
  ]
  edge [
    source 14
    target 76
  ]
  edge [
    source 14
    target 77
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 78
  ]
  edge [
    source 15
    target 79
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 80
  ]
  edge [
    source 16
    target 81
  ]
  edge [
    source 16
    target 82
  ]
  edge [
    source 16
    target 83
  ]
  edge [
    source 16
    target 84
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 85
  ]
  edge [
    source 18
    target 86
  ]
  edge [
    source 18
    target 87
  ]
  edge [
    source 18
    target 88
  ]
  edge [
    source 18
    target 89
  ]
  edge [
    source 18
    target 90
  ]
  edge [
    source 18
    target 91
  ]
  edge [
    source 18
    target 92
  ]
  edge [
    source 18
    target 93
  ]
  edge [
    source 18
    target 94
  ]
  edge [
    source 18
    target 95
  ]
  edge [
    source 18
    target 96
  ]
  edge [
    source 18
    target 97
  ]
  edge [
    source 18
    target 98
  ]
  edge [
    source 18
    target 99
  ]
  edge [
    source 18
    target 100
  ]
  edge [
    source 18
    target 101
  ]
  edge [
    source 18
    target 102
  ]
  edge [
    source 18
    target 103
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 104
  ]
  edge [
    source 20
    target 105
  ]
  edge [
    source 20
    target 44
  ]
  edge [
    source 20
    target 106
  ]
  edge [
    source 20
    target 107
  ]
  edge [
    source 20
    target 108
  ]
  edge [
    source 20
    target 109
  ]
  edge [
    source 20
    target 110
  ]
  edge [
    source 20
    target 111
  ]
  edge [
    source 20
    target 28
  ]
  edge [
    source 112
    target 113
  ]
  edge [
    source 112
    target 114
  ]
  edge [
    source 113
    target 114
  ]
  edge [
    source 115
    target 116
  ]
]
