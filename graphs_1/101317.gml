graph [
  maxDegree 36
  minDegree 1
  meanDegree 2
  density 0.03773584905660377
  graphCliqueNumber 3
  node [
    id 0
    label "miejski"
    origin "text"
  ]
  node [
    id 1
    label "strona"
    origin "text"
  ]
  node [
    id 2
    label "ksi&#281;&#380;yc"
    origin "text"
  ]
  node [
    id 3
    label "miejsko"
  ]
  node [
    id 4
    label "miastowy"
  ]
  node [
    id 5
    label "typowy"
  ]
  node [
    id 6
    label "publiczny"
  ]
  node [
    id 7
    label "skr&#281;canie"
  ]
  node [
    id 8
    label "voice"
  ]
  node [
    id 9
    label "forma"
  ]
  node [
    id 10
    label "internet"
  ]
  node [
    id 11
    label "skr&#281;ci&#263;"
  ]
  node [
    id 12
    label "kartka"
  ]
  node [
    id 13
    label "orientowa&#263;"
  ]
  node [
    id 14
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 15
    label "powierzchnia"
  ]
  node [
    id 16
    label "plik"
  ]
  node [
    id 17
    label "bok"
  ]
  node [
    id 18
    label "pagina"
  ]
  node [
    id 19
    label "orientowanie"
  ]
  node [
    id 20
    label "fragment"
  ]
  node [
    id 21
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 22
    label "s&#261;d"
  ]
  node [
    id 23
    label "skr&#281;ca&#263;"
  ]
  node [
    id 24
    label "g&#243;ra"
  ]
  node [
    id 25
    label "serwis_internetowy"
  ]
  node [
    id 26
    label "orientacja"
  ]
  node [
    id 27
    label "linia"
  ]
  node [
    id 28
    label "skr&#281;cenie"
  ]
  node [
    id 29
    label "layout"
  ]
  node [
    id 30
    label "zorientowa&#263;"
  ]
  node [
    id 31
    label "zorientowanie"
  ]
  node [
    id 32
    label "obiekt"
  ]
  node [
    id 33
    label "podmiot"
  ]
  node [
    id 34
    label "ty&#322;"
  ]
  node [
    id 35
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 36
    label "logowanie"
  ]
  node [
    id 37
    label "adres_internetowy"
  ]
  node [
    id 38
    label "uj&#281;cie"
  ]
  node [
    id 39
    label "prz&#243;d"
  ]
  node [
    id 40
    label "posta&#263;"
  ]
  node [
    id 41
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 42
    label "faza_Ksi&#281;&#380;yca"
  ]
  node [
    id 43
    label "moon"
  ]
  node [
    id 44
    label "&#347;wiat&#322;o"
  ]
  node [
    id 45
    label "aposelenium"
  ]
  node [
    id 46
    label "peryselenium"
  ]
  node [
    id 47
    label "Tytan"
  ]
  node [
    id 48
    label "kalendarz_ksi&#281;&#380;ycowy"
  ]
  node [
    id 49
    label "miesi&#261;c"
  ]
  node [
    id 50
    label "satelita"
  ]
  node [
    id 51
    label "stary"
  ]
  node [
    id 52
    label "dobry"
  ]
  node [
    id 53
    label "ma&#322;&#380;e&#324;stwo"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 53
  ]
  edge [
    source 52
    target 53
  ]
]
