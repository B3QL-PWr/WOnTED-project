graph [
  maxDegree 12
  minDegree 1
  meanDegree 1.8666666666666667
  density 0.13333333333333333
  graphCliqueNumber 2
  node [
    id 0
    label "trzeba"
    origin "text"
  ]
  node [
    id 1
    label "uwa&#380;a&#263;"
    origin "text"
  ]
  node [
    id 2
    label "trza"
  ]
  node [
    id 3
    label "necessity"
  ]
  node [
    id 4
    label "continue"
  ]
  node [
    id 5
    label "str&#243;&#380;y&#263;"
  ]
  node [
    id 6
    label "consider"
  ]
  node [
    id 7
    label "my&#347;le&#263;"
  ]
  node [
    id 8
    label "pilnowa&#263;"
  ]
  node [
    id 9
    label "robi&#263;"
  ]
  node [
    id 10
    label "uznawa&#263;"
  ]
  node [
    id 11
    label "obserwowa&#263;"
  ]
  node [
    id 12
    label "skupia&#263;_si&#281;"
  ]
  node [
    id 13
    label "mie&#263;_na_baczenie"
  ]
  node [
    id 14
    label "deliver"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
]
