graph [
  maxDegree 43
  minDegree 1
  meanDegree 2.1095890410958904
  density 0.0072494468766181805
  graphCliqueNumber 3
  node [
    id 0
    label "gdy"
    origin "text"
  ]
  node [
    id 1
    label "tylko"
    origin "text"
  ]
  node [
    id 2
    label "paul"
    origin "text"
  ]
  node [
    id 3
    label "odprowadzi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "delegacja"
    origin "text"
  ]
  node [
    id 5
    label "lukas"
    origin "text"
  ]
  node [
    id 6
    label "wyrzuci&#263;"
    origin "text"
  ]
  node [
    id 7
    label "lista"
    origin "text"
  ]
  node [
    id 8
    label "&#380;yczenia"
    origin "text"
  ]
  node [
    id 9
    label "wuj"
    origin "text"
  ]
  node [
    id 10
    label "karol"
    origin "text"
  ]
  node [
    id 11
    label "&#347;mietniczka"
    origin "text"
  ]
  node [
    id 12
    label "wr&#243;ci&#263;"
    origin "text"
  ]
  node [
    id 13
    label "brzeg"
    origin "text"
  ]
  node [
    id 14
    label "basen"
    origin "text"
  ]
  node [
    id 15
    label "zam&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 16
    label "kilka"
    origin "text"
  ]
  node [
    id 17
    label "dobrze"
    origin "text"
  ]
  node [
    id 18
    label "sch&#322;odzi&#263;"
    origin "text"
  ]
  node [
    id 19
    label "puszek"
    origin "text"
  ]
  node [
    id 20
    label "piwo"
    origin "text"
  ]
  node [
    id 21
    label "skoczy&#263;"
    origin "text"
  ]
  node [
    id 22
    label "woda"
    origin "text"
  ]
  node [
    id 23
    label "kiedy"
    origin "text"
  ]
  node [
    id 24
    label "wynurzy&#263;"
    origin "text"
  ]
  node [
    id 25
    label "si&#281;"
    origin "text"
  ]
  node [
    id 26
    label "sta&#263;"
    origin "text"
  ]
  node [
    id 27
    label "nieprzyjemny"
    origin "text"
  ]
  node [
    id 28
    label "informacja"
    origin "text"
  ]
  node [
    id 29
    label "zast&#281;pcja"
    origin "text"
  ]
  node [
    id 30
    label "wontaka"
    origin "text"
  ]
  node [
    id 31
    label "bugajew"
    origin "text"
  ]
  node [
    id 32
    label "czeka&#263;"
    origin "text"
  ]
  node [
    id 33
    label "przyj&#281;cie"
    origin "text"
  ]
  node [
    id 34
    label "rozpoczyna&#263;"
    origin "text"
  ]
  node [
    id 35
    label "nieformalny"
    origin "text"
  ]
  node [
    id 36
    label "cz&#281;&#347;&#263;"
    origin "text"
  ]
  node [
    id 37
    label "urz&#281;dowanie"
    origin "text"
  ]
  node [
    id 38
    label "jako"
    origin "text"
  ]
  node [
    id 39
    label "pierwsze"
    origin "text"
  ]
  node [
    id 40
    label "kaza&#263;"
    origin "text"
  ]
  node [
    id 41
    label "przyprowadzi&#263;"
    origin "text"
  ]
  node [
    id 42
    label "bugajewa"
    origin "text"
  ]
  node [
    id 43
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 44
    label "dostarczy&#263;"
  ]
  node [
    id 45
    label "company"
  ]
  node [
    id 46
    label "accompany"
  ]
  node [
    id 47
    label "mission"
  ]
  node [
    id 48
    label "deputacja"
  ]
  node [
    id 49
    label "upowa&#380;nienie"
  ]
  node [
    id 50
    label "wyjazd"
  ]
  node [
    id 51
    label "przeniesienie"
  ]
  node [
    id 52
    label "za&#347;wiadczenie"
  ]
  node [
    id 53
    label "reprezentacja"
  ]
  node [
    id 54
    label "roll"
  ]
  node [
    id 55
    label "wypierdoli&#263;"
  ]
  node [
    id 56
    label "frame"
  ]
  node [
    id 57
    label "powiedzie&#263;"
  ]
  node [
    id 58
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 59
    label "pozby&#263;_si&#281;"
  ]
  node [
    id 60
    label "usun&#261;&#263;"
  ]
  node [
    id 61
    label "arouse"
  ]
  node [
    id 62
    label "wychrzani&#263;"
  ]
  node [
    id 63
    label "wypierniczy&#263;"
  ]
  node [
    id 64
    label "wysadzi&#263;"
  ]
  node [
    id 65
    label "ruszy&#263;"
  ]
  node [
    id 66
    label "zrobi&#263;"
  ]
  node [
    id 67
    label "reproach"
  ]
  node [
    id 68
    label "wyliczanka"
  ]
  node [
    id 69
    label "catalog"
  ]
  node [
    id 70
    label "stock"
  ]
  node [
    id 71
    label "figurowa&#263;"
  ]
  node [
    id 72
    label "zbi&#243;r"
  ]
  node [
    id 73
    label "book"
  ]
  node [
    id 74
    label "pozycja"
  ]
  node [
    id 75
    label "tekst"
  ]
  node [
    id 76
    label "sumariusz"
  ]
  node [
    id 77
    label "serdeczno&#347;ci"
  ]
  node [
    id 78
    label "praise"
  ]
  node [
    id 79
    label "krewny"
  ]
  node [
    id 80
    label "wujo"
  ]
  node [
    id 81
    label "szufla"
  ]
  node [
    id 82
    label "&#347;mietnik"
  ]
  node [
    id 83
    label "dustpan"
  ]
  node [
    id 84
    label "render"
  ]
  node [
    id 85
    label "return"
  ]
  node [
    id 86
    label "zosta&#263;"
  ]
  node [
    id 87
    label "spowodowa&#263;"
  ]
  node [
    id 88
    label "przyj&#347;&#263;"
  ]
  node [
    id 89
    label "zjawi&#263;_si&#281;"
  ]
  node [
    id 90
    label "revive"
  ]
  node [
    id 91
    label "pojedna&#263;_si&#281;"
  ]
  node [
    id 92
    label "podj&#261;&#263;"
  ]
  node [
    id 93
    label "nawi&#261;za&#263;"
  ]
  node [
    id 94
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 95
    label "przyby&#263;"
  ]
  node [
    id 96
    label "recur"
  ]
  node [
    id 97
    label "linia"
  ]
  node [
    id 98
    label "koniec"
  ]
  node [
    id 99
    label "ekoton"
  ]
  node [
    id 100
    label "kraj"
  ]
  node [
    id 101
    label "str&#261;d"
  ]
  node [
    id 102
    label "zawarto&#347;&#263;"
  ]
  node [
    id 103
    label "obiekt"
  ]
  node [
    id 104
    label "k&#261;pielisko"
  ]
  node [
    id 105
    label "zaj&#281;cia"
  ]
  node [
    id 106
    label "zbiornik"
  ]
  node [
    id 107
    label "naczynie"
  ]
  node [
    id 108
    label "region"
  ]
  node [
    id 109
    label "port"
  ]
  node [
    id 110
    label "niecka_basenowa"
  ]
  node [
    id 111
    label "zbiornik_wodny"
  ]
  node [
    id 112
    label "budowla"
  ]
  node [
    id 113
    label "falownica"
  ]
  node [
    id 114
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 115
    label "poleci&#263;"
  ]
  node [
    id 116
    label "zarezerwowa&#263;"
  ]
  node [
    id 117
    label "zamawianie"
  ]
  node [
    id 118
    label "zaczarowa&#263;"
  ]
  node [
    id 119
    label "order"
  ]
  node [
    id 120
    label "zam&#243;wienie"
  ]
  node [
    id 121
    label "bespeak"
  ]
  node [
    id 122
    label "indent"
  ]
  node [
    id 123
    label "indenture"
  ]
  node [
    id 124
    label "zamawia&#263;"
  ]
  node [
    id 125
    label "zg&#322;osi&#263;"
  ]
  node [
    id 126
    label "appoint"
  ]
  node [
    id 127
    label "um&#243;wi&#263;_si&#281;"
  ]
  node [
    id 128
    label "zleci&#263;"
  ]
  node [
    id 129
    label "&#347;ledziowate"
  ]
  node [
    id 130
    label "ryba"
  ]
  node [
    id 131
    label "moralnie"
  ]
  node [
    id 132
    label "wiele"
  ]
  node [
    id 133
    label "lepiej"
  ]
  node [
    id 134
    label "korzystnie"
  ]
  node [
    id 135
    label "pomy&#347;lnie"
  ]
  node [
    id 136
    label "pozytywnie"
  ]
  node [
    id 137
    label "dobry"
  ]
  node [
    id 138
    label "dobroczynnie"
  ]
  node [
    id 139
    label "odpowiednio"
  ]
  node [
    id 140
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 141
    label "skutecznie"
  ]
  node [
    id 142
    label "cool"
  ]
  node [
    id 143
    label "obni&#380;y&#263;"
  ]
  node [
    id 144
    label "gospodarka"
  ]
  node [
    id 145
    label "wyhamowa&#263;"
  ]
  node [
    id 146
    label "aplomb"
  ]
  node [
    id 147
    label "akcesorium"
  ]
  node [
    id 148
    label "browarnia"
  ]
  node [
    id 149
    label "anta&#322;"
  ]
  node [
    id 150
    label "wyj&#347;cie"
  ]
  node [
    id 151
    label "warzy&#263;"
  ]
  node [
    id 152
    label "warzenie"
  ]
  node [
    id 153
    label "uwarzenie"
  ]
  node [
    id 154
    label "alkohol"
  ]
  node [
    id 155
    label "nap&#243;j"
  ]
  node [
    id 156
    label "nawarzy&#263;"
  ]
  node [
    id 157
    label "bacik"
  ]
  node [
    id 158
    label "uwarzy&#263;"
  ]
  node [
    id 159
    label "nawarzenie"
  ]
  node [
    id 160
    label "birofilia"
  ]
  node [
    id 161
    label "poruszy&#263;_si&#281;"
  ]
  node [
    id 162
    label "napa&#347;&#263;"
  ]
  node [
    id 163
    label "parachute"
  ]
  node [
    id 164
    label "uda&#263;_si&#281;"
  ]
  node [
    id 165
    label "wypowied&#378;"
  ]
  node [
    id 166
    label "obiekt_naturalny"
  ]
  node [
    id 167
    label "bicie"
  ]
  node [
    id 168
    label "wysi&#281;k"
  ]
  node [
    id 169
    label "pustka"
  ]
  node [
    id 170
    label "woda_s&#322;odka"
  ]
  node [
    id 171
    label "p&#322;ycizna"
  ]
  node [
    id 172
    label "ciecz"
  ]
  node [
    id 173
    label "spi&#281;trza&#263;"
  ]
  node [
    id 174
    label "uj&#281;cie_wody"
  ]
  node [
    id 175
    label "chlasta&#263;"
  ]
  node [
    id 176
    label "spi&#281;trzy&#263;"
  ]
  node [
    id 177
    label "bombast"
  ]
  node [
    id 178
    label "water"
  ]
  node [
    id 179
    label "kryptodepresja"
  ]
  node [
    id 180
    label "wodnik"
  ]
  node [
    id 181
    label "pojazd"
  ]
  node [
    id 182
    label "&#347;r&#243;dl&#261;dowy"
  ]
  node [
    id 183
    label "fala"
  ]
  node [
    id 184
    label "Waruna"
  ]
  node [
    id 185
    label "nap&#243;j_bezalkoholowy"
  ]
  node [
    id 186
    label "zrzut"
  ]
  node [
    id 187
    label "dotleni&#263;"
  ]
  node [
    id 188
    label "utylizator"
  ]
  node [
    id 189
    label "przyroda"
  ]
  node [
    id 190
    label "uci&#261;g"
  ]
  node [
    id 191
    label "wybrze&#380;e"
  ]
  node [
    id 192
    label "nabranie"
  ]
  node [
    id 193
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 194
    label "klarownik"
  ]
  node [
    id 195
    label "chlastanie"
  ]
  node [
    id 196
    label "przybrze&#380;e"
  ]
  node [
    id 197
    label "deklamacja"
  ]
  node [
    id 198
    label "spi&#281;trzenie"
  ]
  node [
    id 199
    label "przybieranie"
  ]
  node [
    id 200
    label "nabra&#263;"
  ]
  node [
    id 201
    label "tlenek"
  ]
  node [
    id 202
    label "spi&#281;trzanie"
  ]
  node [
    id 203
    label "l&#243;d"
  ]
  node [
    id 204
    label "pozostawa&#263;"
  ]
  node [
    id 205
    label "trwa&#263;"
  ]
  node [
    id 206
    label "by&#263;"
  ]
  node [
    id 207
    label "wystarcza&#263;"
  ]
  node [
    id 208
    label "obowi&#261;zywa&#263;"
  ]
  node [
    id 209
    label "stand"
  ]
  node [
    id 210
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 211
    label "mieszka&#263;"
  ]
  node [
    id 212
    label "wystarczy&#263;"
  ]
  node [
    id 213
    label "sprawowa&#263;"
  ]
  node [
    id 214
    label "przebywa&#263;"
  ]
  node [
    id 215
    label "kosztowa&#263;"
  ]
  node [
    id 216
    label "undertaking"
  ]
  node [
    id 217
    label "wystawa&#263;"
  ]
  node [
    id 218
    label "base"
  ]
  node [
    id 219
    label "digest"
  ]
  node [
    id 220
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 221
    label "niemile"
  ]
  node [
    id 222
    label "rozd&#281;cie_si&#281;"
  ]
  node [
    id 223
    label "niezgodny"
  ]
  node [
    id 224
    label "z&#322;y"
  ]
  node [
    id 225
    label "niew&#322;a&#347;ciwy"
  ]
  node [
    id 226
    label "nieprzyjemnie"
  ]
  node [
    id 227
    label "doj&#347;cie"
  ]
  node [
    id 228
    label "doj&#347;&#263;"
  ]
  node [
    id 229
    label "powzi&#261;&#263;"
  ]
  node [
    id 230
    label "wiedza"
  ]
  node [
    id 231
    label "sygna&#322;"
  ]
  node [
    id 232
    label "obiegni&#281;cie"
  ]
  node [
    id 233
    label "obieganie"
  ]
  node [
    id 234
    label "obiec"
  ]
  node [
    id 235
    label "dane"
  ]
  node [
    id 236
    label "obiega&#263;"
  ]
  node [
    id 237
    label "punkt"
  ]
  node [
    id 238
    label "publikacja"
  ]
  node [
    id 239
    label "powzi&#281;cie"
  ]
  node [
    id 240
    label "hold"
  ]
  node [
    id 241
    label "sp&#281;dza&#263;"
  ]
  node [
    id 242
    label "look"
  ]
  node [
    id 243
    label "decydowa&#263;"
  ]
  node [
    id 244
    label "oczekiwa&#263;"
  ]
  node [
    id 245
    label "pauzowa&#263;"
  ]
  node [
    id 246
    label "anticipate"
  ]
  node [
    id 247
    label "wzi&#281;cie"
  ]
  node [
    id 248
    label "reception"
  ]
  node [
    id 249
    label "zareagowanie"
  ]
  node [
    id 250
    label "poch&#322;oni&#281;cie"
  ]
  node [
    id 251
    label "entertainment"
  ]
  node [
    id 252
    label "spotkanie"
  ]
  node [
    id 253
    label "zobowi&#261;zanie_si&#281;"
  ]
  node [
    id 254
    label "stanie_si&#281;"
  ]
  node [
    id 255
    label "presumption"
  ]
  node [
    id 256
    label "dopuszczenie"
  ]
  node [
    id 257
    label "credence"
  ]
  node [
    id 258
    label "uznanie"
  ]
  node [
    id 259
    label "zgodzenie_si&#281;"
  ]
  node [
    id 260
    label "zrobienie"
  ]
  node [
    id 261
    label "party"
  ]
  node [
    id 262
    label "impreza"
  ]
  node [
    id 263
    label "wpuszczenie"
  ]
  node [
    id 264
    label "przyj&#261;&#263;"
  ]
  node [
    id 265
    label "nau&#380;ywanie_si&#281;"
  ]
  node [
    id 266
    label "w&#322;&#261;czenie"
  ]
  node [
    id 267
    label "umieszczenie"
  ]
  node [
    id 268
    label "start"
  ]
  node [
    id 269
    label "robi&#263;_pierwszy_krok"
  ]
  node [
    id 270
    label "begin"
  ]
  node [
    id 271
    label "nieformalnie"
  ]
  node [
    id 272
    label "nieoficjalny"
  ]
  node [
    id 273
    label "whole"
  ]
  node [
    id 274
    label "Rzym_Zachodni"
  ]
  node [
    id 275
    label "element"
  ]
  node [
    id 276
    label "ilo&#347;&#263;"
  ]
  node [
    id 277
    label "urz&#261;dzenie"
  ]
  node [
    id 278
    label "Rzym_Wschodni"
  ]
  node [
    id 279
    label "pracowanie"
  ]
  node [
    id 280
    label "zmusza&#263;"
  ]
  node [
    id 281
    label "nakazywa&#263;"
  ]
  node [
    id 282
    label "wymaga&#263;"
  ]
  node [
    id 283
    label "zmusi&#263;"
  ]
  node [
    id 284
    label "wyg&#322;asza&#263;"
  ]
  node [
    id 285
    label "command"
  ]
  node [
    id 286
    label "say"
  ]
  node [
    id 287
    label "wyg&#322;osi&#263;"
  ]
  node [
    id 288
    label "nakaza&#263;"
  ]
  node [
    id 289
    label "wprowadzi&#263;"
  ]
  node [
    id 290
    label "pos&#322;a&#263;"
  ]
  node [
    id 291
    label "carry"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 81
  ]
  edge [
    source 11
    target 82
  ]
  edge [
    source 11
    target 83
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 84
  ]
  edge [
    source 12
    target 85
  ]
  edge [
    source 12
    target 86
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 12
    target 89
  ]
  edge [
    source 12
    target 90
  ]
  edge [
    source 12
    target 91
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 12
    target 94
  ]
  edge [
    source 12
    target 95
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 25
  ]
  edge [
    source 13
    target 26
  ]
  edge [
    source 13
    target 97
  ]
  edge [
    source 13
    target 98
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 13
    target 101
  ]
  edge [
    source 13
    target 72
  ]
  edge [
    source 13
    target 22
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 102
  ]
  edge [
    source 14
    target 103
  ]
  edge [
    source 14
    target 104
  ]
  edge [
    source 14
    target 105
  ]
  edge [
    source 14
    target 106
  ]
  edge [
    source 14
    target 107
  ]
  edge [
    source 14
    target 108
  ]
  edge [
    source 14
    target 109
  ]
  edge [
    source 14
    target 110
  ]
  edge [
    source 14
    target 111
  ]
  edge [
    source 14
    target 112
  ]
  edge [
    source 14
    target 113
  ]
  edge [
    source 14
    target 114
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 115
  ]
  edge [
    source 15
    target 116
  ]
  edge [
    source 15
    target 117
  ]
  edge [
    source 15
    target 118
  ]
  edge [
    source 15
    target 119
  ]
  edge [
    source 15
    target 120
  ]
  edge [
    source 15
    target 121
  ]
  edge [
    source 15
    target 122
  ]
  edge [
    source 15
    target 123
  ]
  edge [
    source 15
    target 124
  ]
  edge [
    source 15
    target 125
  ]
  edge [
    source 15
    target 126
  ]
  edge [
    source 15
    target 127
  ]
  edge [
    source 15
    target 128
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 129
  ]
  edge [
    source 16
    target 130
  ]
  edge [
    source 16
    target 32
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 131
  ]
  edge [
    source 17
    target 132
  ]
  edge [
    source 17
    target 133
  ]
  edge [
    source 17
    target 134
  ]
  edge [
    source 17
    target 135
  ]
  edge [
    source 17
    target 136
  ]
  edge [
    source 17
    target 137
  ]
  edge [
    source 17
    target 138
  ]
  edge [
    source 17
    target 139
  ]
  edge [
    source 17
    target 140
  ]
  edge [
    source 17
    target 141
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 142
  ]
  edge [
    source 18
    target 143
  ]
  edge [
    source 18
    target 144
  ]
  edge [
    source 18
    target 145
  ]
  edge [
    source 18
    target 146
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 147
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 148
  ]
  edge [
    source 20
    target 149
  ]
  edge [
    source 20
    target 150
  ]
  edge [
    source 20
    target 151
  ]
  edge [
    source 20
    target 152
  ]
  edge [
    source 20
    target 153
  ]
  edge [
    source 20
    target 154
  ]
  edge [
    source 20
    target 155
  ]
  edge [
    source 20
    target 156
  ]
  edge [
    source 20
    target 157
  ]
  edge [
    source 20
    target 158
  ]
  edge [
    source 20
    target 159
  ]
  edge [
    source 20
    target 160
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 161
  ]
  edge [
    source 21
    target 162
  ]
  edge [
    source 21
    target 163
  ]
  edge [
    source 21
    target 164
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 165
  ]
  edge [
    source 22
    target 166
  ]
  edge [
    source 22
    target 167
  ]
  edge [
    source 22
    target 168
  ]
  edge [
    source 22
    target 169
  ]
  edge [
    source 22
    target 170
  ]
  edge [
    source 22
    target 171
  ]
  edge [
    source 22
    target 172
  ]
  edge [
    source 22
    target 173
  ]
  edge [
    source 22
    target 174
  ]
  edge [
    source 22
    target 175
  ]
  edge [
    source 22
    target 176
  ]
  edge [
    source 22
    target 155
  ]
  edge [
    source 22
    target 177
  ]
  edge [
    source 22
    target 178
  ]
  edge [
    source 22
    target 179
  ]
  edge [
    source 22
    target 180
  ]
  edge [
    source 22
    target 181
  ]
  edge [
    source 22
    target 182
  ]
  edge [
    source 22
    target 183
  ]
  edge [
    source 22
    target 184
  ]
  edge [
    source 22
    target 185
  ]
  edge [
    source 22
    target 186
  ]
  edge [
    source 22
    target 187
  ]
  edge [
    source 22
    target 188
  ]
  edge [
    source 22
    target 189
  ]
  edge [
    source 22
    target 190
  ]
  edge [
    source 22
    target 191
  ]
  edge [
    source 22
    target 192
  ]
  edge [
    source 22
    target 193
  ]
  edge [
    source 22
    target 194
  ]
  edge [
    source 22
    target 195
  ]
  edge [
    source 22
    target 196
  ]
  edge [
    source 22
    target 197
  ]
  edge [
    source 22
    target 198
  ]
  edge [
    source 22
    target 199
  ]
  edge [
    source 22
    target 200
  ]
  edge [
    source 22
    target 201
  ]
  edge [
    source 22
    target 202
  ]
  edge [
    source 22
    target 203
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 34
  ]
  edge [
    source 25
    target 35
  ]
  edge [
    source 26
    target 204
  ]
  edge [
    source 26
    target 205
  ]
  edge [
    source 26
    target 206
  ]
  edge [
    source 26
    target 207
  ]
  edge [
    source 26
    target 208
  ]
  edge [
    source 26
    target 32
  ]
  edge [
    source 26
    target 209
  ]
  edge [
    source 26
    target 210
  ]
  edge [
    source 26
    target 211
  ]
  edge [
    source 26
    target 212
  ]
  edge [
    source 26
    target 213
  ]
  edge [
    source 26
    target 214
  ]
  edge [
    source 26
    target 215
  ]
  edge [
    source 26
    target 216
  ]
  edge [
    source 26
    target 217
  ]
  edge [
    source 26
    target 218
  ]
  edge [
    source 26
    target 219
  ]
  edge [
    source 26
    target 220
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 221
  ]
  edge [
    source 27
    target 222
  ]
  edge [
    source 27
    target 223
  ]
  edge [
    source 27
    target 224
  ]
  edge [
    source 27
    target 225
  ]
  edge [
    source 27
    target 226
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 227
  ]
  edge [
    source 28
    target 228
  ]
  edge [
    source 28
    target 229
  ]
  edge [
    source 28
    target 230
  ]
  edge [
    source 28
    target 231
  ]
  edge [
    source 28
    target 232
  ]
  edge [
    source 28
    target 233
  ]
  edge [
    source 28
    target 234
  ]
  edge [
    source 28
    target 235
  ]
  edge [
    source 28
    target 236
  ]
  edge [
    source 28
    target 237
  ]
  edge [
    source 28
    target 238
  ]
  edge [
    source 28
    target 239
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 206
  ]
  edge [
    source 32
    target 240
  ]
  edge [
    source 32
    target 241
  ]
  edge [
    source 32
    target 242
  ]
  edge [
    source 32
    target 243
  ]
  edge [
    source 32
    target 244
  ]
  edge [
    source 32
    target 245
  ]
  edge [
    source 32
    target 246
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 247
  ]
  edge [
    source 33
    target 248
  ]
  edge [
    source 33
    target 249
  ]
  edge [
    source 33
    target 250
  ]
  edge [
    source 33
    target 251
  ]
  edge [
    source 33
    target 252
  ]
  edge [
    source 33
    target 253
  ]
  edge [
    source 33
    target 254
  ]
  edge [
    source 33
    target 255
  ]
  edge [
    source 33
    target 256
  ]
  edge [
    source 33
    target 257
  ]
  edge [
    source 33
    target 258
  ]
  edge [
    source 33
    target 259
  ]
  edge [
    source 33
    target 260
  ]
  edge [
    source 33
    target 261
  ]
  edge [
    source 33
    target 262
  ]
  edge [
    source 33
    target 263
  ]
  edge [
    source 33
    target 264
  ]
  edge [
    source 33
    target 265
  ]
  edge [
    source 33
    target 266
  ]
  edge [
    source 33
    target 267
  ]
  edge [
    source 34
    target 268
  ]
  edge [
    source 34
    target 269
  ]
  edge [
    source 34
    target 210
  ]
  edge [
    source 34
    target 270
  ]
  edge [
    source 34
    target 213
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 271
  ]
  edge [
    source 35
    target 272
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 273
  ]
  edge [
    source 36
    target 274
  ]
  edge [
    source 36
    target 275
  ]
  edge [
    source 36
    target 276
  ]
  edge [
    source 36
    target 277
  ]
  edge [
    source 36
    target 278
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 279
  ]
  edge [
    source 37
    target 251
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 40
    target 280
  ]
  edge [
    source 40
    target 281
  ]
  edge [
    source 40
    target 282
  ]
  edge [
    source 40
    target 283
  ]
  edge [
    source 40
    target 284
  ]
  edge [
    source 40
    target 285
  ]
  edge [
    source 40
    target 119
  ]
  edge [
    source 40
    target 286
  ]
  edge [
    source 40
    target 287
  ]
  edge [
    source 40
    target 288
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 289
  ]
  edge [
    source 41
    target 290
  ]
  edge [
    source 41
    target 291
  ]
]
