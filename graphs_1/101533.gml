graph [
  maxDegree 218
  minDegree 1
  meanDegree 2.0603621730382295
  density 0.004153955994028688
  graphCliqueNumber 4
  node [
    id 0
    label "okres"
    origin "text"
  ]
  node [
    id 1
    label "powstanie"
    origin "text"
  ]
  node [
    id 2
    label "cmentarz"
    origin "text"
  ]
  node [
    id 3
    label "koniec"
    origin "text"
  ]
  node [
    id 4
    label "wojna"
    origin "text"
  ]
  node [
    id 5
    label "&#347;wiatowy"
    origin "text"
  ]
  node [
    id 6
    label "pochowa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "nim"
    origin "text"
  ]
  node [
    id 8
    label "&#380;o&#322;nierz"
    origin "text"
  ]
  node [
    id 9
    label "polski"
    origin "text"
  ]
  node [
    id 10
    label "pa&#324;stwo"
    origin "text"
  ]
  node [
    id 11
    label "podziemny"
    origin "text"
  ]
  node [
    id 12
    label "niemcy"
    origin "text"
  ]
  node [
    id 13
    label "nigdy"
    origin "text"
  ]
  node [
    id 14
    label "sprofanowa&#263;"
    origin "text"
  ]
  node [
    id 15
    label "le&#347;ny"
    origin "text"
  ]
  node [
    id 16
    label "chocia&#380;"
    origin "text"
  ]
  node [
    id 17
    label "pewno"
    origin "text"
  ]
  node [
    id 18
    label "wiedzie&#263;"
    origin "text"
  ]
  node [
    id 19
    label "by&#263;"
    origin "text"
  ]
  node [
    id 20
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 21
    label "obawia&#263;"
    origin "text"
  ]
  node [
    id 22
    label "si&#281;"
    origin "text"
  ]
  node [
    id 23
    label "partyzant"
    origin "text"
  ]
  node [
    id 24
    label "uszanowa&#263;by"
    origin "text"
  ]
  node [
    id 25
    label "tak&#380;e"
    origin "text"
  ]
  node [
    id 26
    label "pobliski"
    origin "text"
  ]
  node [
    id 27
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 28
    label "wrzesie&#324;"
    origin "text"
  ]
  node [
    id 29
    label "rocznik"
    origin "text"
  ]
  node [
    id 30
    label "bitwa"
    origin "text"
  ]
  node [
    id 31
    label "pod"
    origin "text"
  ]
  node [
    id 32
    label "barak"
    origin "text"
  ]
  node [
    id 33
    label "swoje"
    origin "text"
  ]
  node [
    id 34
    label "paleogen"
  ]
  node [
    id 35
    label "spell"
  ]
  node [
    id 36
    label "czas"
  ]
  node [
    id 37
    label "period"
  ]
  node [
    id 38
    label "prekambr"
  ]
  node [
    id 39
    label "jura"
  ]
  node [
    id 40
    label "interstadia&#322;"
  ]
  node [
    id 41
    label "jednostka_geologiczna"
  ]
  node [
    id 42
    label "izochronizm"
  ]
  node [
    id 43
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 44
    label "okres_noachijski"
  ]
  node [
    id 45
    label "orosir"
  ]
  node [
    id 46
    label "sten"
  ]
  node [
    id 47
    label "kreda"
  ]
  node [
    id 48
    label "drugorz&#281;d"
  ]
  node [
    id 49
    label "semester"
  ]
  node [
    id 50
    label "trzeciorz&#281;d"
  ]
  node [
    id 51
    label "ton"
  ]
  node [
    id 52
    label "dzieje"
  ]
  node [
    id 53
    label "poprzednik"
  ]
  node [
    id 54
    label "kalim"
  ]
  node [
    id 55
    label "ordowik"
  ]
  node [
    id 56
    label "karbon"
  ]
  node [
    id 57
    label "trias"
  ]
  node [
    id 58
    label "stater"
  ]
  node [
    id 59
    label "era"
  ]
  node [
    id 60
    label "cykl"
  ]
  node [
    id 61
    label "p&#243;&#322;okres"
  ]
  node [
    id 62
    label "czwartorz&#281;d"
  ]
  node [
    id 63
    label "pulsacja"
  ]
  node [
    id 64
    label "okres_amazo&#324;ski"
  ]
  node [
    id 65
    label "kambr"
  ]
  node [
    id 66
    label "Zeitgeist"
  ]
  node [
    id 67
    label "nast&#281;pnik"
  ]
  node [
    id 68
    label "kriogen"
  ]
  node [
    id 69
    label "glacja&#322;"
  ]
  node [
    id 70
    label "fala"
  ]
  node [
    id 71
    label "okres_czasu"
  ]
  node [
    id 72
    label "riak"
  ]
  node [
    id 73
    label "schy&#322;ek"
  ]
  node [
    id 74
    label "okres_hesperyjski"
  ]
  node [
    id 75
    label "sylur"
  ]
  node [
    id 76
    label "dewon"
  ]
  node [
    id 77
    label "ciota"
  ]
  node [
    id 78
    label "epoka"
  ]
  node [
    id 79
    label "pierwszorz&#281;d"
  ]
  node [
    id 80
    label "okres_halsztacki"
  ]
  node [
    id 81
    label "ektas"
  ]
  node [
    id 82
    label "zdanie"
  ]
  node [
    id 83
    label "condition"
  ]
  node [
    id 84
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 85
    label "rok_akademicki"
  ]
  node [
    id 86
    label "wapie&#324;_muszlowy"
  ]
  node [
    id 87
    label "postglacja&#322;"
  ]
  node [
    id 88
    label "faza"
  ]
  node [
    id 89
    label "proces_fizjologiczny"
  ]
  node [
    id 90
    label "ediakar"
  ]
  node [
    id 91
    label "time_period"
  ]
  node [
    id 92
    label "ruch_drgaj&#261;cy"
  ]
  node [
    id 93
    label "perm"
  ]
  node [
    id 94
    label "rok_szkolny"
  ]
  node [
    id 95
    label "neogen"
  ]
  node [
    id 96
    label "sider"
  ]
  node [
    id 97
    label "flow"
  ]
  node [
    id 98
    label "podokres"
  ]
  node [
    id 99
    label "preglacja&#322;"
  ]
  node [
    id 100
    label "retoryka"
  ]
  node [
    id 101
    label "choroba_przyrodzona"
  ]
  node [
    id 102
    label "uniesienie_si&#281;"
  ]
  node [
    id 103
    label "geneza"
  ]
  node [
    id 104
    label "chmielnicczyzna"
  ]
  node [
    id 105
    label "beginning"
  ]
  node [
    id 106
    label "orgy"
  ]
  node [
    id 107
    label "Ko&#347;ciuszko"
  ]
  node [
    id 108
    label "sprzeciwienie_si&#281;"
  ]
  node [
    id 109
    label "utworzenie"
  ]
  node [
    id 110
    label "powstanie_listopadowe"
  ]
  node [
    id 111
    label "potworzenie_si&#281;"
  ]
  node [
    id 112
    label "powstanie_warszawskie"
  ]
  node [
    id 113
    label "kl&#281;czenie"
  ]
  node [
    id 114
    label "zbuntowanie_si&#281;"
  ]
  node [
    id 115
    label "siedzenie"
  ]
  node [
    id 116
    label "stworzenie"
  ]
  node [
    id 117
    label "walka"
  ]
  node [
    id 118
    label "zaistnienie"
  ]
  node [
    id 119
    label "odbudowanie_si&#281;"
  ]
  node [
    id 120
    label "pierwocina"
  ]
  node [
    id 121
    label "origin"
  ]
  node [
    id 122
    label "koliszczyzna"
  ]
  node [
    id 123
    label "powstanie_tambowskie"
  ]
  node [
    id 124
    label "&#380;akieria"
  ]
  node [
    id 125
    label "le&#380;enie"
  ]
  node [
    id 126
    label "miejsce_poch&#243;wku"
  ]
  node [
    id 127
    label "pogrobowisko"
  ]
  node [
    id 128
    label "&#380;alnik"
  ]
  node [
    id 129
    label "sm&#281;tarz"
  ]
  node [
    id 130
    label "park_sztywnych"
  ]
  node [
    id 131
    label "smentarz"
  ]
  node [
    id 132
    label "cinerarium"
  ]
  node [
    id 133
    label "defenestracja"
  ]
  node [
    id 134
    label "szereg"
  ]
  node [
    id 135
    label "dzia&#322;anie"
  ]
  node [
    id 136
    label "miejsce"
  ]
  node [
    id 137
    label "ostatnie_podrygi"
  ]
  node [
    id 138
    label "kres"
  ]
  node [
    id 139
    label "agonia"
  ]
  node [
    id 140
    label "visitation"
  ]
  node [
    id 141
    label "szeol"
  ]
  node [
    id 142
    label "mogi&#322;a"
  ]
  node [
    id 143
    label "chwila"
  ]
  node [
    id 144
    label "wydarzenie"
  ]
  node [
    id 145
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 146
    label "pogrzebanie"
  ]
  node [
    id 147
    label "punkt"
  ]
  node [
    id 148
    label "&#380;a&#322;oba"
  ]
  node [
    id 149
    label "zabicie"
  ]
  node [
    id 150
    label "kres_&#380;ycia"
  ]
  node [
    id 151
    label "zimna_wojna"
  ]
  node [
    id 152
    label "zbrodnia_przeciw_ludzko&#347;ci"
  ]
  node [
    id 153
    label "angaria"
  ]
  node [
    id 154
    label "wr&#243;g"
  ]
  node [
    id 155
    label "zbrodnia_przeciw_pokojowi"
  ]
  node [
    id 156
    label "war"
  ]
  node [
    id 157
    label "konflikt"
  ]
  node [
    id 158
    label "wojna_stuletnia"
  ]
  node [
    id 159
    label "burza"
  ]
  node [
    id 160
    label "zbrodnia_wojenna"
  ]
  node [
    id 161
    label "gra_w_karty"
  ]
  node [
    id 162
    label "sp&#243;r"
  ]
  node [
    id 163
    label "kulturalny"
  ]
  node [
    id 164
    label "&#347;wiatowo"
  ]
  node [
    id 165
    label "generalny"
  ]
  node [
    id 166
    label "zw&#322;oki"
  ]
  node [
    id 167
    label "bury"
  ]
  node [
    id 168
    label "znie&#347;&#263;"
  ]
  node [
    id 169
    label "hide"
  ]
  node [
    id 170
    label "straci&#263;"
  ]
  node [
    id 171
    label "powk&#322;ada&#263;"
  ]
  node [
    id 172
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 173
    label "poumieszcza&#263;"
  ]
  node [
    id 174
    label "gra_planszowa"
  ]
  node [
    id 175
    label "cz&#322;owiek"
  ]
  node [
    id 176
    label "demobilizowa&#263;"
  ]
  node [
    id 177
    label "rota"
  ]
  node [
    id 178
    label "walcz&#261;cy"
  ]
  node [
    id 179
    label "demobilizowanie"
  ]
  node [
    id 180
    label "harcap"
  ]
  node [
    id 181
    label "&#380;o&#322;nierstwo"
  ]
  node [
    id 182
    label "&#380;o&#322;dowy"
  ]
  node [
    id 183
    label "zdemobilizowanie"
  ]
  node [
    id 184
    label "elew"
  ]
  node [
    id 185
    label "mundurowy"
  ]
  node [
    id 186
    label "s&#322;u&#380;y&#263;_w_wojsku"
  ]
  node [
    id 187
    label "so&#322;dat"
  ]
  node [
    id 188
    label "wojsko"
  ]
  node [
    id 189
    label "s&#322;u&#380;enie_w_wojsku"
  ]
  node [
    id 190
    label "zdemobilizowa&#263;"
  ]
  node [
    id 191
    label "Gurkha"
  ]
  node [
    id 192
    label "lacki"
  ]
  node [
    id 193
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 194
    label "przedmiot"
  ]
  node [
    id 195
    label "sztajer"
  ]
  node [
    id 196
    label "drabant"
  ]
  node [
    id 197
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 198
    label "polak"
  ]
  node [
    id 199
    label "pierogi_ruskie"
  ]
  node [
    id 200
    label "krakowiak"
  ]
  node [
    id 201
    label "Polish"
  ]
  node [
    id 202
    label "j&#281;zyk"
  ]
  node [
    id 203
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 204
    label "oberek"
  ]
  node [
    id 205
    label "po_polsku"
  ]
  node [
    id 206
    label "mazur"
  ]
  node [
    id 207
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 208
    label "chodzony"
  ]
  node [
    id 209
    label "skoczny"
  ]
  node [
    id 210
    label "ryba_po_grecku"
  ]
  node [
    id 211
    label "goniony"
  ]
  node [
    id 212
    label "polsko"
  ]
  node [
    id 213
    label "Filipiny"
  ]
  node [
    id 214
    label "Rwanda"
  ]
  node [
    id 215
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 216
    label "Monako"
  ]
  node [
    id 217
    label "Korea"
  ]
  node [
    id 218
    label "Ghana"
  ]
  node [
    id 219
    label "Czarnog&#243;ra"
  ]
  node [
    id 220
    label "Malawi"
  ]
  node [
    id 221
    label "Indonezja"
  ]
  node [
    id 222
    label "Bu&#322;garia"
  ]
  node [
    id 223
    label "Nauru"
  ]
  node [
    id 224
    label "Kenia"
  ]
  node [
    id 225
    label "Kambod&#380;a"
  ]
  node [
    id 226
    label "Mali"
  ]
  node [
    id 227
    label "Austria"
  ]
  node [
    id 228
    label "interior"
  ]
  node [
    id 229
    label "Armenia"
  ]
  node [
    id 230
    label "Fid&#380;i"
  ]
  node [
    id 231
    label "Tuwalu"
  ]
  node [
    id 232
    label "Etiopia"
  ]
  node [
    id 233
    label "Malta"
  ]
  node [
    id 234
    label "Malezja"
  ]
  node [
    id 235
    label "Grenada"
  ]
  node [
    id 236
    label "Tad&#380;ykistan"
  ]
  node [
    id 237
    label "Wehrlen"
  ]
  node [
    id 238
    label "para"
  ]
  node [
    id 239
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 240
    label "Rumunia"
  ]
  node [
    id 241
    label "Maroko"
  ]
  node [
    id 242
    label "Bhutan"
  ]
  node [
    id 243
    label "S&#322;owacja"
  ]
  node [
    id 244
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 245
    label "Seszele"
  ]
  node [
    id 246
    label "Kuwejt"
  ]
  node [
    id 247
    label "Arabia_Saudyjska"
  ]
  node [
    id 248
    label "Ekwador"
  ]
  node [
    id 249
    label "Kanada"
  ]
  node [
    id 250
    label "Japonia"
  ]
  node [
    id 251
    label "ziemia"
  ]
  node [
    id 252
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 253
    label "Hiszpania"
  ]
  node [
    id 254
    label "Wyspy_Marshalla"
  ]
  node [
    id 255
    label "Botswana"
  ]
  node [
    id 256
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 257
    label "D&#380;ibuti"
  ]
  node [
    id 258
    label "grupa"
  ]
  node [
    id 259
    label "Wietnam"
  ]
  node [
    id 260
    label "Egipt"
  ]
  node [
    id 261
    label "Burkina_Faso"
  ]
  node [
    id 262
    label "Niemcy"
  ]
  node [
    id 263
    label "Khitai"
  ]
  node [
    id 264
    label "Macedonia"
  ]
  node [
    id 265
    label "Albania"
  ]
  node [
    id 266
    label "Madagaskar"
  ]
  node [
    id 267
    label "Bahrajn"
  ]
  node [
    id 268
    label "Jemen"
  ]
  node [
    id 269
    label "Lesoto"
  ]
  node [
    id 270
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 271
    label "Samoa"
  ]
  node [
    id 272
    label "Andora"
  ]
  node [
    id 273
    label "Chiny"
  ]
  node [
    id 274
    label "Cypr"
  ]
  node [
    id 275
    label "Wielka_Brytania"
  ]
  node [
    id 276
    label "Ukraina"
  ]
  node [
    id 277
    label "Paragwaj"
  ]
  node [
    id 278
    label "Trynidad_i_Tobago"
  ]
  node [
    id 279
    label "Libia"
  ]
  node [
    id 280
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 281
    label "Surinam"
  ]
  node [
    id 282
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 283
    label "Australia"
  ]
  node [
    id 284
    label "Nigeria"
  ]
  node [
    id 285
    label "Honduras"
  ]
  node [
    id 286
    label "Bangladesz"
  ]
  node [
    id 287
    label "Peru"
  ]
  node [
    id 288
    label "Kazachstan"
  ]
  node [
    id 289
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 290
    label "Irak"
  ]
  node [
    id 291
    label "holoarktyka"
  ]
  node [
    id 292
    label "USA"
  ]
  node [
    id 293
    label "Sudan"
  ]
  node [
    id 294
    label "Nepal"
  ]
  node [
    id 295
    label "San_Marino"
  ]
  node [
    id 296
    label "Burundi"
  ]
  node [
    id 297
    label "Dominikana"
  ]
  node [
    id 298
    label "Komory"
  ]
  node [
    id 299
    label "granica_pa&#324;stwa"
  ]
  node [
    id 300
    label "Gwatemala"
  ]
  node [
    id 301
    label "Antarktis"
  ]
  node [
    id 302
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 303
    label "Brunei"
  ]
  node [
    id 304
    label "Iran"
  ]
  node [
    id 305
    label "Zimbabwe"
  ]
  node [
    id 306
    label "Namibia"
  ]
  node [
    id 307
    label "Meksyk"
  ]
  node [
    id 308
    label "Kamerun"
  ]
  node [
    id 309
    label "zwrot"
  ]
  node [
    id 310
    label "Somalia"
  ]
  node [
    id 311
    label "Angola"
  ]
  node [
    id 312
    label "Gabon"
  ]
  node [
    id 313
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 314
    label "Mozambik"
  ]
  node [
    id 315
    label "Tajwan"
  ]
  node [
    id 316
    label "Tunezja"
  ]
  node [
    id 317
    label "Nowa_Zelandia"
  ]
  node [
    id 318
    label "Liban"
  ]
  node [
    id 319
    label "Jordania"
  ]
  node [
    id 320
    label "Tonga"
  ]
  node [
    id 321
    label "Czad"
  ]
  node [
    id 322
    label "Liberia"
  ]
  node [
    id 323
    label "Gwinea"
  ]
  node [
    id 324
    label "Belize"
  ]
  node [
    id 325
    label "&#321;otwa"
  ]
  node [
    id 326
    label "Syria"
  ]
  node [
    id 327
    label "Benin"
  ]
  node [
    id 328
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 329
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 330
    label "Dominika"
  ]
  node [
    id 331
    label "Antigua_i_Barbuda"
  ]
  node [
    id 332
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 333
    label "Hanower"
  ]
  node [
    id 334
    label "partia"
  ]
  node [
    id 335
    label "Afganistan"
  ]
  node [
    id 336
    label "Kiribati"
  ]
  node [
    id 337
    label "W&#322;ochy"
  ]
  node [
    id 338
    label "Szwajcaria"
  ]
  node [
    id 339
    label "Sahara_Zachodnia"
  ]
  node [
    id 340
    label "Chorwacja"
  ]
  node [
    id 341
    label "Tajlandia"
  ]
  node [
    id 342
    label "Salwador"
  ]
  node [
    id 343
    label "Bahamy"
  ]
  node [
    id 344
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 345
    label "S&#322;owenia"
  ]
  node [
    id 346
    label "Gambia"
  ]
  node [
    id 347
    label "Urugwaj"
  ]
  node [
    id 348
    label "Zair"
  ]
  node [
    id 349
    label "Erytrea"
  ]
  node [
    id 350
    label "Rosja"
  ]
  node [
    id 351
    label "Uganda"
  ]
  node [
    id 352
    label "Niger"
  ]
  node [
    id 353
    label "Mauritius"
  ]
  node [
    id 354
    label "Turkmenistan"
  ]
  node [
    id 355
    label "Turcja"
  ]
  node [
    id 356
    label "Irlandia"
  ]
  node [
    id 357
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 358
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 359
    label "Gwinea_Bissau"
  ]
  node [
    id 360
    label "Belgia"
  ]
  node [
    id 361
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 362
    label "Palau"
  ]
  node [
    id 363
    label "Barbados"
  ]
  node [
    id 364
    label "Chile"
  ]
  node [
    id 365
    label "Wenezuela"
  ]
  node [
    id 366
    label "W&#281;gry"
  ]
  node [
    id 367
    label "Argentyna"
  ]
  node [
    id 368
    label "Kolumbia"
  ]
  node [
    id 369
    label "Sierra_Leone"
  ]
  node [
    id 370
    label "Azerbejd&#380;an"
  ]
  node [
    id 371
    label "Kongo"
  ]
  node [
    id 372
    label "Pakistan"
  ]
  node [
    id 373
    label "Liechtenstein"
  ]
  node [
    id 374
    label "Nikaragua"
  ]
  node [
    id 375
    label "Senegal"
  ]
  node [
    id 376
    label "Indie"
  ]
  node [
    id 377
    label "Suazi"
  ]
  node [
    id 378
    label "Polska"
  ]
  node [
    id 379
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 380
    label "Algieria"
  ]
  node [
    id 381
    label "terytorium"
  ]
  node [
    id 382
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 383
    label "Jamajka"
  ]
  node [
    id 384
    label "Kostaryka"
  ]
  node [
    id 385
    label "Timor_Wschodni"
  ]
  node [
    id 386
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 387
    label "Kuba"
  ]
  node [
    id 388
    label "Mauretania"
  ]
  node [
    id 389
    label "Portoryko"
  ]
  node [
    id 390
    label "Brazylia"
  ]
  node [
    id 391
    label "Mo&#322;dawia"
  ]
  node [
    id 392
    label "organizacja"
  ]
  node [
    id 393
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 394
    label "Litwa"
  ]
  node [
    id 395
    label "Kirgistan"
  ]
  node [
    id 396
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 397
    label "Izrael"
  ]
  node [
    id 398
    label "Grecja"
  ]
  node [
    id 399
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 400
    label "Holandia"
  ]
  node [
    id 401
    label "Sri_Lanka"
  ]
  node [
    id 402
    label "Katar"
  ]
  node [
    id 403
    label "Mikronezja"
  ]
  node [
    id 404
    label "Mongolia"
  ]
  node [
    id 405
    label "Laos"
  ]
  node [
    id 406
    label "Malediwy"
  ]
  node [
    id 407
    label "Zambia"
  ]
  node [
    id 408
    label "Tanzania"
  ]
  node [
    id 409
    label "Gujana"
  ]
  node [
    id 410
    label "Czechy"
  ]
  node [
    id 411
    label "Panama"
  ]
  node [
    id 412
    label "Uzbekistan"
  ]
  node [
    id 413
    label "Gruzja"
  ]
  node [
    id 414
    label "Serbia"
  ]
  node [
    id 415
    label "Francja"
  ]
  node [
    id 416
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 417
    label "Togo"
  ]
  node [
    id 418
    label "Estonia"
  ]
  node [
    id 419
    label "Oman"
  ]
  node [
    id 420
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 421
    label "Portugalia"
  ]
  node [
    id 422
    label "Boliwia"
  ]
  node [
    id 423
    label "Luksemburg"
  ]
  node [
    id 424
    label "Haiti"
  ]
  node [
    id 425
    label "Wyspy_Salomona"
  ]
  node [
    id 426
    label "Birma"
  ]
  node [
    id 427
    label "Rodezja"
  ]
  node [
    id 428
    label "podziemnie"
  ]
  node [
    id 429
    label "tajny"
  ]
  node [
    id 430
    label "konspiracyjnie"
  ]
  node [
    id 431
    label "kompletnie"
  ]
  node [
    id 432
    label "skali&#263;"
  ]
  node [
    id 433
    label "zniewa&#380;y&#263;"
  ]
  node [
    id 434
    label "defile"
  ]
  node [
    id 435
    label "poziomka"
  ]
  node [
    id 436
    label "owocowy"
  ]
  node [
    id 437
    label "drzewny"
  ]
  node [
    id 438
    label "je&#380;yna"
  ]
  node [
    id 439
    label "zielony"
  ]
  node [
    id 440
    label "&#347;wie&#380;y"
  ]
  node [
    id 441
    label "bor&#243;wka"
  ]
  node [
    id 442
    label "cognizance"
  ]
  node [
    id 443
    label "si&#281;ga&#263;"
  ]
  node [
    id 444
    label "trwa&#263;"
  ]
  node [
    id 445
    label "obecno&#347;&#263;"
  ]
  node [
    id 446
    label "stan"
  ]
  node [
    id 447
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 448
    label "stand"
  ]
  node [
    id 449
    label "mie&#263;_miejsce"
  ]
  node [
    id 450
    label "uczestniczy&#263;"
  ]
  node [
    id 451
    label "chodzi&#263;"
  ]
  node [
    id 452
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 453
    label "equal"
  ]
  node [
    id 454
    label "partyzantka"
  ]
  node [
    id 455
    label "stronnik"
  ]
  node [
    id 456
    label "bojownik"
  ]
  node [
    id 457
    label "wojna_podjazdowa"
  ]
  node [
    id 458
    label "bliski"
  ]
  node [
    id 459
    label "poblisko"
  ]
  node [
    id 460
    label "miesi&#261;c"
  ]
  node [
    id 461
    label "formacja"
  ]
  node [
    id 462
    label "kronika"
  ]
  node [
    id 463
    label "czasopismo"
  ]
  node [
    id 464
    label "yearbook"
  ]
  node [
    id 465
    label "zaj&#347;cie"
  ]
  node [
    id 466
    label "batalista"
  ]
  node [
    id 467
    label "action"
  ]
  node [
    id 468
    label "bitwa_pod_Pi&#322;awcami"
  ]
  node [
    id 469
    label "say_farewell"
  ]
  node [
    id 470
    label "budynek"
  ]
  node [
    id 471
    label "ii"
  ]
  node [
    id 472
    label "polskie"
  ]
  node [
    id 473
    label "regionalny"
  ]
  node [
    id 474
    label "dyrekcja"
  ]
  node [
    id 475
    label "las"
  ]
  node [
    id 476
    label "pa&#324;stwowy"
  ]
  node [
    id 477
    label "drogi"
  ]
  node [
    id 478
    label "krajowy"
  ]
  node [
    id 479
    label "nr"
  ]
  node [
    id 480
    label "7"
  ]
  node [
    id 481
    label "Virtuti"
  ]
  node [
    id 482
    label "Militari"
  ]
  node [
    id 483
    label "urz&#261;d"
  ]
  node [
    id 484
    label "wojew&#243;dzki"
  ]
  node [
    id 485
    label "miasto"
  ]
  node [
    id 486
    label "skar&#380;yski"
  ]
  node [
    id 487
    label "kamienny"
  ]
  node [
    id 488
    label "Ewa"
  ]
  node [
    id 489
    label "Kierzkowska"
  ]
  node [
    id 490
    label "Krzysztofa"
  ]
  node [
    id 491
    label "Jurgiel"
  ]
  node [
    id 492
    label "prawo"
  ]
  node [
    id 493
    label "i"
  ]
  node [
    id 494
    label "sprawiedliwo&#347;&#263;"
  ]
  node [
    id 495
    label "marka"
  ]
  node [
    id 496
    label "Sawicki"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 471
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 471
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 17
  ]
  edge [
    source 7
    target 18
  ]
  edge [
    source 7
    target 174
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 33
  ]
  edge [
    source 8
    target 175
  ]
  edge [
    source 8
    target 176
  ]
  edge [
    source 8
    target 177
  ]
  edge [
    source 8
    target 178
  ]
  edge [
    source 8
    target 179
  ]
  edge [
    source 8
    target 180
  ]
  edge [
    source 8
    target 181
  ]
  edge [
    source 8
    target 182
  ]
  edge [
    source 8
    target 183
  ]
  edge [
    source 8
    target 184
  ]
  edge [
    source 8
    target 185
  ]
  edge [
    source 8
    target 186
  ]
  edge [
    source 8
    target 187
  ]
  edge [
    source 8
    target 188
  ]
  edge [
    source 8
    target 189
  ]
  edge [
    source 8
    target 190
  ]
  edge [
    source 8
    target 191
  ]
  edge [
    source 8
    target 23
  ]
  edge [
    source 8
    target 21
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 192
  ]
  edge [
    source 9
    target 193
  ]
  edge [
    source 9
    target 194
  ]
  edge [
    source 9
    target 195
  ]
  edge [
    source 9
    target 196
  ]
  edge [
    source 9
    target 197
  ]
  edge [
    source 9
    target 198
  ]
  edge [
    source 9
    target 199
  ]
  edge [
    source 9
    target 200
  ]
  edge [
    source 9
    target 201
  ]
  edge [
    source 9
    target 202
  ]
  edge [
    source 9
    target 203
  ]
  edge [
    source 9
    target 204
  ]
  edge [
    source 9
    target 205
  ]
  edge [
    source 9
    target 206
  ]
  edge [
    source 9
    target 207
  ]
  edge [
    source 9
    target 208
  ]
  edge [
    source 9
    target 209
  ]
  edge [
    source 9
    target 210
  ]
  edge [
    source 9
    target 211
  ]
  edge [
    source 9
    target 212
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 213
  ]
  edge [
    source 10
    target 214
  ]
  edge [
    source 10
    target 215
  ]
  edge [
    source 10
    target 216
  ]
  edge [
    source 10
    target 217
  ]
  edge [
    source 10
    target 218
  ]
  edge [
    source 10
    target 219
  ]
  edge [
    source 10
    target 220
  ]
  edge [
    source 10
    target 221
  ]
  edge [
    source 10
    target 222
  ]
  edge [
    source 10
    target 223
  ]
  edge [
    source 10
    target 224
  ]
  edge [
    source 10
    target 225
  ]
  edge [
    source 10
    target 226
  ]
  edge [
    source 10
    target 227
  ]
  edge [
    source 10
    target 228
  ]
  edge [
    source 10
    target 229
  ]
  edge [
    source 10
    target 230
  ]
  edge [
    source 10
    target 231
  ]
  edge [
    source 10
    target 232
  ]
  edge [
    source 10
    target 233
  ]
  edge [
    source 10
    target 234
  ]
  edge [
    source 10
    target 235
  ]
  edge [
    source 10
    target 236
  ]
  edge [
    source 10
    target 237
  ]
  edge [
    source 10
    target 238
  ]
  edge [
    source 10
    target 239
  ]
  edge [
    source 10
    target 240
  ]
  edge [
    source 10
    target 241
  ]
  edge [
    source 10
    target 242
  ]
  edge [
    source 10
    target 243
  ]
  edge [
    source 10
    target 244
  ]
  edge [
    source 10
    target 245
  ]
  edge [
    source 10
    target 246
  ]
  edge [
    source 10
    target 247
  ]
  edge [
    source 10
    target 248
  ]
  edge [
    source 10
    target 249
  ]
  edge [
    source 10
    target 250
  ]
  edge [
    source 10
    target 251
  ]
  edge [
    source 10
    target 252
  ]
  edge [
    source 10
    target 253
  ]
  edge [
    source 10
    target 254
  ]
  edge [
    source 10
    target 255
  ]
  edge [
    source 10
    target 256
  ]
  edge [
    source 10
    target 257
  ]
  edge [
    source 10
    target 258
  ]
  edge [
    source 10
    target 259
  ]
  edge [
    source 10
    target 260
  ]
  edge [
    source 10
    target 261
  ]
  edge [
    source 10
    target 262
  ]
  edge [
    source 10
    target 263
  ]
  edge [
    source 10
    target 264
  ]
  edge [
    source 10
    target 265
  ]
  edge [
    source 10
    target 266
  ]
  edge [
    source 10
    target 267
  ]
  edge [
    source 10
    target 268
  ]
  edge [
    source 10
    target 269
  ]
  edge [
    source 10
    target 270
  ]
  edge [
    source 10
    target 271
  ]
  edge [
    source 10
    target 272
  ]
  edge [
    source 10
    target 273
  ]
  edge [
    source 10
    target 274
  ]
  edge [
    source 10
    target 275
  ]
  edge [
    source 10
    target 276
  ]
  edge [
    source 10
    target 277
  ]
  edge [
    source 10
    target 278
  ]
  edge [
    source 10
    target 279
  ]
  edge [
    source 10
    target 280
  ]
  edge [
    source 10
    target 281
  ]
  edge [
    source 10
    target 282
  ]
  edge [
    source 10
    target 283
  ]
  edge [
    source 10
    target 284
  ]
  edge [
    source 10
    target 285
  ]
  edge [
    source 10
    target 286
  ]
  edge [
    source 10
    target 287
  ]
  edge [
    source 10
    target 288
  ]
  edge [
    source 10
    target 289
  ]
  edge [
    source 10
    target 290
  ]
  edge [
    source 10
    target 291
  ]
  edge [
    source 10
    target 292
  ]
  edge [
    source 10
    target 293
  ]
  edge [
    source 10
    target 294
  ]
  edge [
    source 10
    target 295
  ]
  edge [
    source 10
    target 296
  ]
  edge [
    source 10
    target 297
  ]
  edge [
    source 10
    target 298
  ]
  edge [
    source 10
    target 299
  ]
  edge [
    source 10
    target 300
  ]
  edge [
    source 10
    target 301
  ]
  edge [
    source 10
    target 302
  ]
  edge [
    source 10
    target 303
  ]
  edge [
    source 10
    target 304
  ]
  edge [
    source 10
    target 305
  ]
  edge [
    source 10
    target 306
  ]
  edge [
    source 10
    target 307
  ]
  edge [
    source 10
    target 308
  ]
  edge [
    source 10
    target 309
  ]
  edge [
    source 10
    target 310
  ]
  edge [
    source 10
    target 311
  ]
  edge [
    source 10
    target 312
  ]
  edge [
    source 10
    target 313
  ]
  edge [
    source 10
    target 314
  ]
  edge [
    source 10
    target 315
  ]
  edge [
    source 10
    target 316
  ]
  edge [
    source 10
    target 317
  ]
  edge [
    source 10
    target 318
  ]
  edge [
    source 10
    target 319
  ]
  edge [
    source 10
    target 320
  ]
  edge [
    source 10
    target 321
  ]
  edge [
    source 10
    target 322
  ]
  edge [
    source 10
    target 323
  ]
  edge [
    source 10
    target 324
  ]
  edge [
    source 10
    target 325
  ]
  edge [
    source 10
    target 326
  ]
  edge [
    source 10
    target 327
  ]
  edge [
    source 10
    target 328
  ]
  edge [
    source 10
    target 329
  ]
  edge [
    source 10
    target 330
  ]
  edge [
    source 10
    target 331
  ]
  edge [
    source 10
    target 332
  ]
  edge [
    source 10
    target 333
  ]
  edge [
    source 10
    target 334
  ]
  edge [
    source 10
    target 335
  ]
  edge [
    source 10
    target 336
  ]
  edge [
    source 10
    target 337
  ]
  edge [
    source 10
    target 338
  ]
  edge [
    source 10
    target 339
  ]
  edge [
    source 10
    target 340
  ]
  edge [
    source 10
    target 341
  ]
  edge [
    source 10
    target 342
  ]
  edge [
    source 10
    target 343
  ]
  edge [
    source 10
    target 344
  ]
  edge [
    source 10
    target 345
  ]
  edge [
    source 10
    target 346
  ]
  edge [
    source 10
    target 347
  ]
  edge [
    source 10
    target 348
  ]
  edge [
    source 10
    target 349
  ]
  edge [
    source 10
    target 350
  ]
  edge [
    source 10
    target 351
  ]
  edge [
    source 10
    target 352
  ]
  edge [
    source 10
    target 353
  ]
  edge [
    source 10
    target 354
  ]
  edge [
    source 10
    target 355
  ]
  edge [
    source 10
    target 356
  ]
  edge [
    source 10
    target 357
  ]
  edge [
    source 10
    target 358
  ]
  edge [
    source 10
    target 359
  ]
  edge [
    source 10
    target 360
  ]
  edge [
    source 10
    target 361
  ]
  edge [
    source 10
    target 362
  ]
  edge [
    source 10
    target 363
  ]
  edge [
    source 10
    target 364
  ]
  edge [
    source 10
    target 365
  ]
  edge [
    source 10
    target 366
  ]
  edge [
    source 10
    target 367
  ]
  edge [
    source 10
    target 368
  ]
  edge [
    source 10
    target 369
  ]
  edge [
    source 10
    target 370
  ]
  edge [
    source 10
    target 371
  ]
  edge [
    source 10
    target 372
  ]
  edge [
    source 10
    target 373
  ]
  edge [
    source 10
    target 374
  ]
  edge [
    source 10
    target 375
  ]
  edge [
    source 10
    target 376
  ]
  edge [
    source 10
    target 377
  ]
  edge [
    source 10
    target 378
  ]
  edge [
    source 10
    target 379
  ]
  edge [
    source 10
    target 380
  ]
  edge [
    source 10
    target 381
  ]
  edge [
    source 10
    target 382
  ]
  edge [
    source 10
    target 383
  ]
  edge [
    source 10
    target 384
  ]
  edge [
    source 10
    target 385
  ]
  edge [
    source 10
    target 386
  ]
  edge [
    source 10
    target 387
  ]
  edge [
    source 10
    target 388
  ]
  edge [
    source 10
    target 389
  ]
  edge [
    source 10
    target 390
  ]
  edge [
    source 10
    target 391
  ]
  edge [
    source 10
    target 392
  ]
  edge [
    source 10
    target 393
  ]
  edge [
    source 10
    target 394
  ]
  edge [
    source 10
    target 395
  ]
  edge [
    source 10
    target 396
  ]
  edge [
    source 10
    target 397
  ]
  edge [
    source 10
    target 398
  ]
  edge [
    source 10
    target 399
  ]
  edge [
    source 10
    target 400
  ]
  edge [
    source 10
    target 401
  ]
  edge [
    source 10
    target 402
  ]
  edge [
    source 10
    target 403
  ]
  edge [
    source 10
    target 404
  ]
  edge [
    source 10
    target 405
  ]
  edge [
    source 10
    target 406
  ]
  edge [
    source 10
    target 407
  ]
  edge [
    source 10
    target 408
  ]
  edge [
    source 10
    target 409
  ]
  edge [
    source 10
    target 410
  ]
  edge [
    source 10
    target 411
  ]
  edge [
    source 10
    target 412
  ]
  edge [
    source 10
    target 413
  ]
  edge [
    source 10
    target 414
  ]
  edge [
    source 10
    target 415
  ]
  edge [
    source 10
    target 416
  ]
  edge [
    source 10
    target 417
  ]
  edge [
    source 10
    target 418
  ]
  edge [
    source 10
    target 419
  ]
  edge [
    source 10
    target 420
  ]
  edge [
    source 10
    target 421
  ]
  edge [
    source 10
    target 422
  ]
  edge [
    source 10
    target 423
  ]
  edge [
    source 10
    target 424
  ]
  edge [
    source 10
    target 425
  ]
  edge [
    source 10
    target 426
  ]
  edge [
    source 10
    target 427
  ]
  edge [
    source 10
    target 472
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 428
  ]
  edge [
    source 11
    target 429
  ]
  edge [
    source 11
    target 430
  ]
  edge [
    source 11
    target 472
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 431
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 432
  ]
  edge [
    source 14
    target 433
  ]
  edge [
    source 14
    target 434
  ]
  edge [
    source 15
    target 435
  ]
  edge [
    source 15
    target 436
  ]
  edge [
    source 15
    target 437
  ]
  edge [
    source 15
    target 438
  ]
  edge [
    source 15
    target 439
  ]
  edge [
    source 15
    target 440
  ]
  edge [
    source 15
    target 441
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 32
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 442
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 443
  ]
  edge [
    source 19
    target 444
  ]
  edge [
    source 19
    target 445
  ]
  edge [
    source 19
    target 446
  ]
  edge [
    source 19
    target 447
  ]
  edge [
    source 19
    target 448
  ]
  edge [
    source 19
    target 449
  ]
  edge [
    source 19
    target 450
  ]
  edge [
    source 19
    target 451
  ]
  edge [
    source 19
    target 452
  ]
  edge [
    source 19
    target 453
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 454
  ]
  edge [
    source 23
    target 455
  ]
  edge [
    source 23
    target 456
  ]
  edge [
    source 23
    target 457
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 458
  ]
  edge [
    source 26
    target 459
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 460
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 461
  ]
  edge [
    source 29
    target 462
  ]
  edge [
    source 29
    target 463
  ]
  edge [
    source 29
    target 464
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 465
  ]
  edge [
    source 30
    target 466
  ]
  edge [
    source 30
    target 117
  ]
  edge [
    source 30
    target 467
  ]
  edge [
    source 30
    target 468
  ]
  edge [
    source 30
    target 32
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 32
    target 469
  ]
  edge [
    source 32
    target 470
  ]
  edge [
    source 473
    target 474
  ]
  edge [
    source 473
    target 475
  ]
  edge [
    source 473
    target 476
  ]
  edge [
    source 474
    target 475
  ]
  edge [
    source 474
    target 476
  ]
  edge [
    source 475
    target 476
  ]
  edge [
    source 477
    target 478
  ]
  edge [
    source 477
    target 479
  ]
  edge [
    source 477
    target 480
  ]
  edge [
    source 478
    target 479
  ]
  edge [
    source 478
    target 480
  ]
  edge [
    source 479
    target 480
  ]
  edge [
    source 481
    target 482
  ]
  edge [
    source 483
    target 484
  ]
  edge [
    source 483
    target 485
  ]
  edge [
    source 483
    target 486
  ]
  edge [
    source 483
    target 487
  ]
  edge [
    source 485
    target 486
  ]
  edge [
    source 485
    target 487
  ]
  edge [
    source 486
    target 487
  ]
  edge [
    source 488
    target 489
  ]
  edge [
    source 490
    target 491
  ]
  edge [
    source 492
    target 493
  ]
  edge [
    source 492
    target 494
  ]
  edge [
    source 493
    target 494
  ]
  edge [
    source 495
    target 496
  ]
]
