graph [
  maxDegree 10
  minDegree 1
  meanDegree 1.9047619047619047
  density 0.09523809523809523
  graphCliqueNumber 2
  node [
    id 0
    label "mama"
    origin "text"
  ]
  node [
    id 1
    label "dzisiaj"
    origin "text"
  ]
  node [
    id 2
    label "dla"
    origin "text"
  ]
  node [
    id 3
    label "nasze"
    origin "text"
  ]
  node [
    id 4
    label "pierwsza"
    origin "text"
  ]
  node [
    id 5
    label "rozdajo"
    origin "text"
  ]
  node [
    id 6
    label "matczysko"
  ]
  node [
    id 7
    label "macierz"
  ]
  node [
    id 8
    label "przodkini"
  ]
  node [
    id 9
    label "Matka_Boska"
  ]
  node [
    id 10
    label "macocha"
  ]
  node [
    id 11
    label "matka_zast&#281;pcza"
  ]
  node [
    id 12
    label "stara"
  ]
  node [
    id 13
    label "rodzice"
  ]
  node [
    id 14
    label "rodzic"
  ]
  node [
    id 15
    label "doba"
  ]
  node [
    id 16
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 17
    label "dzi&#347;"
  ]
  node [
    id 18
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 19
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 20
    label "godzina"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 20
  ]
]
