graph [
  maxDegree 21
  minDegree 1
  meanDegree 1.96
  density 0.04
  graphCliqueNumber 2
  node [
    id 0
    label "pose&#322;"
    origin "text"
  ]
  node [
    id 1
    label "anna"
    origin "text"
  ]
  node [
    id 2
    label "paluch"
    origin "text"
  ]
  node [
    id 3
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 4
    label "kompleksowy"
    origin "text"
  ]
  node [
    id 5
    label "rozwi&#261;zanie"
    origin "text"
  ]
  node [
    id 6
    label "dyplomata"
  ]
  node [
    id 7
    label "wys&#322;annik"
  ]
  node [
    id 8
    label "przedstawiciel"
  ]
  node [
    id 9
    label "kurier_dyplomatyczny"
  ]
  node [
    id 10
    label "ablegat"
  ]
  node [
    id 11
    label "klubista"
  ]
  node [
    id 12
    label "Miko&#322;ajczyk"
  ]
  node [
    id 13
    label "Korwin"
  ]
  node [
    id 14
    label "parlamentarzysta"
  ]
  node [
    id 15
    label "dyscyplina_partyjna"
  ]
  node [
    id 16
    label "izba_ni&#380;sza"
  ]
  node [
    id 17
    label "poselstwo"
  ]
  node [
    id 18
    label "bu&#322;ka"
  ]
  node [
    id 19
    label "r&#281;koje&#347;&#263;"
  ]
  node [
    id 20
    label "obr&#281;cz"
  ]
  node [
    id 21
    label "du&#380;y_palec"
  ]
  node [
    id 22
    label "stopa"
  ]
  node [
    id 23
    label "toe"
  ]
  node [
    id 24
    label "czyj&#347;"
  ]
  node [
    id 25
    label "m&#261;&#380;"
  ]
  node [
    id 26
    label "ca&#322;o&#347;ciowo"
  ]
  node [
    id 27
    label "og&#243;lny"
  ]
  node [
    id 28
    label "pe&#322;ny"
  ]
  node [
    id 29
    label "wielostronny"
  ]
  node [
    id 30
    label "wynik"
  ]
  node [
    id 31
    label "wyj&#347;cie"
  ]
  node [
    id 32
    label "spe&#322;nienie"
  ]
  node [
    id 33
    label "cesarskie_ci&#281;cie"
  ]
  node [
    id 34
    label "po&#322;o&#380;na"
  ]
  node [
    id 35
    label "proces_fizjologiczny"
  ]
  node [
    id 36
    label "przestanie"
  ]
  node [
    id 37
    label "marc&#243;wka"
  ]
  node [
    id 38
    label "usuni&#281;cie"
  ]
  node [
    id 39
    label "uniewa&#380;nienie"
  ]
  node [
    id 40
    label "pomys&#322;"
  ]
  node [
    id 41
    label "birth"
  ]
  node [
    id 42
    label "wymy&#347;lenie"
  ]
  node [
    id 43
    label "po&#322;&#243;g"
  ]
  node [
    id 44
    label "szok_poporodowy"
  ]
  node [
    id 45
    label "event"
  ]
  node [
    id 46
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 47
    label "spos&#243;b"
  ]
  node [
    id 48
    label "rozwi&#261;zywanie_si&#281;"
  ]
  node [
    id 49
    label "dula"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
]
