graph [
  maxDegree 17
  minDegree 1
  meanDegree 2.122448979591837
  density 0.04421768707482993
  graphCliqueNumber 3
  node [
    id 0
    label "mirabelka"
    origin "text"
  ]
  node [
    id 1
    label "mircy"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;by"
    origin "text"
  ]
  node [
    id 3
    label "zainteresowany"
    origin "text"
  ]
  node [
    id 4
    label "seria"
    origin "text"
  ]
  node [
    id 5
    label "ciekawostka"
    origin "text"
  ]
  node [
    id 6
    label "lek"
    origin "text"
  ]
  node [
    id 7
    label "narkotyk"
    origin "text"
  ]
  node [
    id 8
    label "kliniczny"
    origin "text"
  ]
  node [
    id 9
    label "&#347;liwka"
  ]
  node [
    id 10
    label "&#347;liwa_domowa"
  ]
  node [
    id 11
    label "cz&#322;owiek"
  ]
  node [
    id 12
    label "stage_set"
  ]
  node [
    id 13
    label "partia"
  ]
  node [
    id 14
    label "przekr&#243;j_geologiczny"
  ]
  node [
    id 15
    label "sekwencja"
  ]
  node [
    id 16
    label "komplet"
  ]
  node [
    id 17
    label "przebieg"
  ]
  node [
    id 18
    label "zestawienie"
  ]
  node [
    id 19
    label "jednostka_systematyczna"
  ]
  node [
    id 20
    label "d&#378;wi&#281;k"
  ]
  node [
    id 21
    label "line"
  ]
  node [
    id 22
    label "zbi&#243;r"
  ]
  node [
    id 23
    label "produkcja"
  ]
  node [
    id 24
    label "set"
  ]
  node [
    id 25
    label "jednostka"
  ]
  node [
    id 26
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 27
    label "informacja"
  ]
  node [
    id 28
    label "naszprycowa&#263;"
  ]
  node [
    id 29
    label "Albania"
  ]
  node [
    id 30
    label "tonizowa&#263;"
  ]
  node [
    id 31
    label "medicine"
  ]
  node [
    id 32
    label "szprycowa&#263;"
  ]
  node [
    id 33
    label "przepisanie"
  ]
  node [
    id 34
    label "przepisa&#263;"
  ]
  node [
    id 35
    label "tonizowanie"
  ]
  node [
    id 36
    label "szprycowanie"
  ]
  node [
    id 37
    label "naszprycowanie"
  ]
  node [
    id 38
    label "jednostka_monetarna"
  ]
  node [
    id 39
    label "apteczka"
  ]
  node [
    id 40
    label "substancja"
  ]
  node [
    id 41
    label "spos&#243;b"
  ]
  node [
    id 42
    label "quindarka"
  ]
  node [
    id 43
    label "&#347;rodek_psychoaktywny"
  ]
  node [
    id 44
    label "narkobiznes"
  ]
  node [
    id 45
    label "chorobowy"
  ]
  node [
    id 46
    label "typowy"
  ]
  node [
    id 47
    label "klinicznie"
  ]
  node [
    id 48
    label "sterylny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 28
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 32
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 37
  ]
  edge [
    source 7
    target 36
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 8
    target 46
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 8
    target 48
  ]
]
