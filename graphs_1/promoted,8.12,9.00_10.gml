graph [
  maxDegree 135
  minDegree 1
  meanDegree 1.9898989898989898
  density 0.010101010101010102
  graphCliqueNumber 2
  node [
    id 0
    label "po&#380;ar"
    origin "text"
  ]
  node [
    id 1
    label "pi&#281;trowy"
    origin "text"
  ]
  node [
    id 2
    label "blok"
    origin "text"
  ]
  node [
    id 3
    label "strunino"
    origin "text"
  ]
  node [
    id 4
    label "region"
    origin "text"
  ]
  node [
    id 5
    label "vladimirski"
    origin "text"
  ]
  node [
    id 6
    label "rosja"
    origin "text"
  ]
  node [
    id 7
    label "kryzys"
  ]
  node [
    id 8
    label "zap&#322;on"
  ]
  node [
    id 9
    label "miazmaty"
  ]
  node [
    id 10
    label "zalew"
  ]
  node [
    id 11
    label "podpalenie"
  ]
  node [
    id 12
    label "wojna"
  ]
  node [
    id 13
    label "p&#322;omie&#324;"
  ]
  node [
    id 14
    label "stra&#380;ak"
  ]
  node [
    id 15
    label "nawa&#322;no&#347;&#263;"
  ]
  node [
    id 16
    label "fire"
  ]
  node [
    id 17
    label "kl&#281;ska"
  ]
  node [
    id 18
    label "wydarzenie"
  ]
  node [
    id 19
    label "zapr&#243;szenie"
  ]
  node [
    id 20
    label "pogorzelec"
  ]
  node [
    id 21
    label "ogie&#324;"
  ]
  node [
    id 22
    label "przyp&#322;yw"
  ]
  node [
    id 23
    label "zagmatwany"
  ]
  node [
    id 24
    label "pi&#281;trowo"
  ]
  node [
    id 25
    label "du&#380;y"
  ]
  node [
    id 26
    label "rozbudowany"
  ]
  node [
    id 27
    label "g&#243;rny"
  ]
  node [
    id 28
    label "square"
  ]
  node [
    id 29
    label "whole"
  ]
  node [
    id 30
    label "zamek"
  ]
  node [
    id 31
    label "block"
  ]
  node [
    id 32
    label "grupa"
  ]
  node [
    id 33
    label "budynek"
  ]
  node [
    id 34
    label "blokowisko"
  ]
  node [
    id 35
    label "barak"
  ]
  node [
    id 36
    label "stok_kontynentalny"
  ]
  node [
    id 37
    label "blokada"
  ]
  node [
    id 38
    label "jednostka_pami&#281;ci_komputera"
  ]
  node [
    id 39
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 40
    label "siatk&#243;wka"
  ]
  node [
    id 41
    label "kr&#261;g"
  ]
  node [
    id 42
    label "bloking"
  ]
  node [
    id 43
    label "dom_wielorodzinny"
  ]
  node [
    id 44
    label "dzia&#322;"
  ]
  node [
    id 45
    label "przeszkoda"
  ]
  node [
    id 46
    label "bie&#380;nia"
  ]
  node [
    id 47
    label "organizacja"
  ]
  node [
    id 48
    label "start"
  ]
  node [
    id 49
    label "blockage"
  ]
  node [
    id 50
    label "obrona"
  ]
  node [
    id 51
    label "bajt"
  ]
  node [
    id 52
    label "zesp&#243;&#322;"
  ]
  node [
    id 53
    label "artyku&#322;"
  ]
  node [
    id 54
    label "bry&#322;a"
  ]
  node [
    id 55
    label "zeszyt"
  ]
  node [
    id 56
    label "ok&#322;adka"
  ]
  node [
    id 57
    label "kontynent"
  ]
  node [
    id 58
    label "referat"
  ]
  node [
    id 59
    label "nastawnia"
  ]
  node [
    id 60
    label "skorupa_ziemska"
  ]
  node [
    id 61
    label "program"
  ]
  node [
    id 62
    label "zbi&#243;r"
  ]
  node [
    id 63
    label "ram&#243;wka"
  ]
  node [
    id 64
    label "j&#261;kanie"
  ]
  node [
    id 65
    label "Skandynawia"
  ]
  node [
    id 66
    label "Yorkshire"
  ]
  node [
    id 67
    label "Kaukaz"
  ]
  node [
    id 68
    label "Kaszmir"
  ]
  node [
    id 69
    label "Toskania"
  ]
  node [
    id 70
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 71
    label "&#321;emkowszczyzna"
  ]
  node [
    id 72
    label "obszar"
  ]
  node [
    id 73
    label "Amhara"
  ]
  node [
    id 74
    label "Lombardia"
  ]
  node [
    id 75
    label "Podbeskidzie"
  ]
  node [
    id 76
    label "okr&#281;g"
  ]
  node [
    id 77
    label "Chiny_Wschodnie"
  ]
  node [
    id 78
    label "Kalabria"
  ]
  node [
    id 79
    label "Tyrol"
  ]
  node [
    id 80
    label "Pamir"
  ]
  node [
    id 81
    label "Lubelszczyzna"
  ]
  node [
    id 82
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 83
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 84
    label "&#379;ywiecczyzna"
  ]
  node [
    id 85
    label "Europa_Wschodnia"
  ]
  node [
    id 86
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 87
    label "Chiny_Zachodnie"
  ]
  node [
    id 88
    label "Zabajkale"
  ]
  node [
    id 89
    label "Kaszuby"
  ]
  node [
    id 90
    label "Bo&#347;nia"
  ]
  node [
    id 91
    label "Noworosja"
  ]
  node [
    id 92
    label "Ba&#322;kany"
  ]
  node [
    id 93
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 94
    label "Anglia"
  ]
  node [
    id 95
    label "Kielecczyzna"
  ]
  node [
    id 96
    label "Krajina"
  ]
  node [
    id 97
    label "Pomorze_Zachodnie"
  ]
  node [
    id 98
    label "Opolskie"
  ]
  node [
    id 99
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 100
    label "Ko&#322;yma"
  ]
  node [
    id 101
    label "Oksytania"
  ]
  node [
    id 102
    label "country"
  ]
  node [
    id 103
    label "Syjon"
  ]
  node [
    id 104
    label "Kociewie"
  ]
  node [
    id 105
    label "Huculszczyzna"
  ]
  node [
    id 106
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 107
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 108
    label "Bawaria"
  ]
  node [
    id 109
    label "Maghreb"
  ]
  node [
    id 110
    label "Bory_Tucholskie"
  ]
  node [
    id 111
    label "Europa_Zachodnia"
  ]
  node [
    id 112
    label "Kerala"
  ]
  node [
    id 113
    label "Burgundia"
  ]
  node [
    id 114
    label "Podhale"
  ]
  node [
    id 115
    label "Kabylia"
  ]
  node [
    id 116
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 117
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 118
    label "Ma&#322;opolska"
  ]
  node [
    id 119
    label "Polesie"
  ]
  node [
    id 120
    label "Liguria"
  ]
  node [
    id 121
    label "&#321;&#243;dzkie"
  ]
  node [
    id 122
    label "Palestyna"
  ]
  node [
    id 123
    label "Bojkowszczyzna"
  ]
  node [
    id 124
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 125
    label "Karaiby"
  ]
  node [
    id 126
    label "S&#261;decczyzna"
  ]
  node [
    id 127
    label "Sand&#380;ak"
  ]
  node [
    id 128
    label "Nadrenia"
  ]
  node [
    id 129
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 130
    label "Zakarpacie"
  ]
  node [
    id 131
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 132
    label "Zag&#243;rze"
  ]
  node [
    id 133
    label "Andaluzja"
  ]
  node [
    id 134
    label "&#379;mud&#378;"
  ]
  node [
    id 135
    label "Turkiestan"
  ]
  node [
    id 136
    label "Naddniestrze"
  ]
  node [
    id 137
    label "Hercegowina"
  ]
  node [
    id 138
    label "Opolszczyzna"
  ]
  node [
    id 139
    label "jednostka_administracyjna"
  ]
  node [
    id 140
    label "Lotaryngia"
  ]
  node [
    id 141
    label "Afryka_Wschodnia"
  ]
  node [
    id 142
    label "Szlezwik"
  ]
  node [
    id 143
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 144
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 145
    label "Mazowsze"
  ]
  node [
    id 146
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 147
    label "Afryka_Zachodnia"
  ]
  node [
    id 148
    label "Galicja"
  ]
  node [
    id 149
    label "Szkocja"
  ]
  node [
    id 150
    label "Walia"
  ]
  node [
    id 151
    label "Powi&#347;le"
  ]
  node [
    id 152
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 153
    label "Zamojszczyzna"
  ]
  node [
    id 154
    label "Kujawy"
  ]
  node [
    id 155
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 156
    label "Podlasie"
  ]
  node [
    id 157
    label "Laponia"
  ]
  node [
    id 158
    label "Umbria"
  ]
  node [
    id 159
    label "Mezoameryka"
  ]
  node [
    id 160
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 161
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 162
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 163
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 164
    label "Kraina"
  ]
  node [
    id 165
    label "Kurdystan"
  ]
  node [
    id 166
    label "Flandria"
  ]
  node [
    id 167
    label "Kampania"
  ]
  node [
    id 168
    label "Armagnac"
  ]
  node [
    id 169
    label "Polinezja"
  ]
  node [
    id 170
    label "Warmia"
  ]
  node [
    id 171
    label "Wielkopolska"
  ]
  node [
    id 172
    label "Bordeaux"
  ]
  node [
    id 173
    label "Lauda"
  ]
  node [
    id 174
    label "Mazury"
  ]
  node [
    id 175
    label "Podkarpacie"
  ]
  node [
    id 176
    label "Oceania"
  ]
  node [
    id 177
    label "Lasko"
  ]
  node [
    id 178
    label "Amazonia"
  ]
  node [
    id 179
    label "podregion"
  ]
  node [
    id 180
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 181
    label "Kurpie"
  ]
  node [
    id 182
    label "Tonkin"
  ]
  node [
    id 183
    label "Azja_Wschodnia"
  ]
  node [
    id 184
    label "Mikronezja"
  ]
  node [
    id 185
    label "Ukraina_Zachodnia"
  ]
  node [
    id 186
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 187
    label "subregion"
  ]
  node [
    id 188
    label "Turyngia"
  ]
  node [
    id 189
    label "Baszkiria"
  ]
  node [
    id 190
    label "Apulia"
  ]
  node [
    id 191
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 192
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 193
    label "Indochiny"
  ]
  node [
    id 194
    label "Biskupizna"
  ]
  node [
    id 195
    label "Lubuskie"
  ]
  node [
    id 196
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 197
    label "Ameryka_&#321;aci&#324;ska"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 5
    target 6
  ]
]
