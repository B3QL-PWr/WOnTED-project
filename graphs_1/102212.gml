graph [
  maxDegree 16
  minDegree 1
  meanDegree 5.72972972972973
  density 0.15915915915915915
  graphCliqueNumber 12
  node [
    id 0
    label "ambasador"
    origin "text"
  ]
  node [
    id 1
    label "nadzwyczajny"
    origin "text"
  ]
  node [
    id 2
    label "pe&#322;nomocny"
    origin "text"
  ]
  node [
    id 3
    label "dyplomata"
  ]
  node [
    id 4
    label "zwolennik"
  ]
  node [
    id 5
    label "chor&#261;&#380;y"
  ]
  node [
    id 6
    label "tuba"
  ]
  node [
    id 7
    label "przedstawiciel"
  ]
  node [
    id 8
    label "hay_fever"
  ]
  node [
    id 9
    label "rozsiewca"
  ]
  node [
    id 10
    label "popularyzator"
  ]
  node [
    id 11
    label "nadzwyczajnie"
  ]
  node [
    id 12
    label "wyj&#261;tkowy"
  ]
  node [
    id 13
    label "ekstraordynaryjny"
  ]
  node [
    id 14
    label "niezwyk&#322;y"
  ]
  node [
    id 15
    label "uprawniony"
  ]
  node [
    id 16
    label "sta&#322;y"
  ]
  node [
    id 17
    label "zjednoczy&#263;"
  ]
  node [
    id 18
    label "kr&#243;lestwo"
  ]
  node [
    id 19
    label "wielki"
  ]
  node [
    id 20
    label "brytania"
  ]
  node [
    id 21
    label "i"
  ]
  node [
    id 22
    label "Irlandia"
  ]
  node [
    id 23
    label "p&#243;&#322;nocny"
  ]
  node [
    id 24
    label "przy"
  ]
  node [
    id 25
    label "unia"
  ]
  node [
    id 26
    label "europejski"
  ]
  node [
    id 27
    label "republika"
  ]
  node [
    id 28
    label "Bu&#322;garia"
  ]
  node [
    id 29
    label "traktat"
  ]
  node [
    id 30
    label "ustanawia&#263;"
  ]
  node [
    id 31
    label "konstytucja"
  ]
  node [
    id 32
    label "dla"
  ]
  node [
    id 33
    label "europ"
  ]
  node [
    id 34
    label "wsp&#243;lnota"
  ]
  node [
    id 35
    label "energia"
  ]
  node [
    id 36
    label "atomowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 7
    target 16
  ]
  edge [
    source 7
    target 17
  ]
  edge [
    source 7
    target 18
  ]
  edge [
    source 7
    target 19
  ]
  edge [
    source 7
    target 20
  ]
  edge [
    source 7
    target 21
  ]
  edge [
    source 7
    target 22
  ]
  edge [
    source 7
    target 23
  ]
  edge [
    source 7
    target 24
  ]
  edge [
    source 7
    target 25
  ]
  edge [
    source 7
    target 26
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 18
  ]
  edge [
    source 16
    target 19
  ]
  edge [
    source 16
    target 20
  ]
  edge [
    source 16
    target 21
  ]
  edge [
    source 16
    target 22
  ]
  edge [
    source 16
    target 23
  ]
  edge [
    source 16
    target 24
  ]
  edge [
    source 16
    target 25
  ]
  edge [
    source 16
    target 26
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 19
  ]
  edge [
    source 17
    target 20
  ]
  edge [
    source 17
    target 21
  ]
  edge [
    source 17
    target 22
  ]
  edge [
    source 17
    target 23
  ]
  edge [
    source 17
    target 24
  ]
  edge [
    source 17
    target 25
  ]
  edge [
    source 17
    target 26
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 20
  ]
  edge [
    source 18
    target 21
  ]
  edge [
    source 18
    target 22
  ]
  edge [
    source 18
    target 23
  ]
  edge [
    source 18
    target 24
  ]
  edge [
    source 18
    target 25
  ]
  edge [
    source 18
    target 26
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 21
  ]
  edge [
    source 19
    target 22
  ]
  edge [
    source 19
    target 23
  ]
  edge [
    source 19
    target 24
  ]
  edge [
    source 19
    target 25
  ]
  edge [
    source 19
    target 26
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 22
  ]
  edge [
    source 20
    target 23
  ]
  edge [
    source 20
    target 24
  ]
  edge [
    source 20
    target 25
  ]
  edge [
    source 20
    target 26
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 23
  ]
  edge [
    source 21
    target 24
  ]
  edge [
    source 21
    target 25
  ]
  edge [
    source 21
    target 26
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 24
  ]
  edge [
    source 22
    target 25
  ]
  edge [
    source 22
    target 26
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 25
  ]
  edge [
    source 23
    target 26
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 26
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 29
  ]
  edge [
    source 26
    target 30
  ]
  edge [
    source 26
    target 34
  ]
  edge [
    source 26
    target 35
  ]
  edge [
    source 26
    target 36
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 31
  ]
  edge [
    source 29
    target 32
  ]
  edge [
    source 29
    target 33
  ]
  edge [
    source 29
    target 34
  ]
  edge [
    source 29
    target 35
  ]
  edge [
    source 29
    target 36
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 32
  ]
  edge [
    source 30
    target 33
  ]
  edge [
    source 30
    target 34
  ]
  edge [
    source 30
    target 35
  ]
  edge [
    source 30
    target 36
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 33
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 36
  ]
  edge [
    source 35
    target 36
  ]
]
