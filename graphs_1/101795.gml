graph [
  maxDegree 324
  minDegree 1
  meanDegree 2.071207430340557
  density 0.003211174310605515
  graphCliqueNumber 3
  node [
    id 0
    label "aktywista"
    origin "text"
  ]
  node [
    id 1
    label "kampania"
    origin "text"
  ]
  node [
    id 2
    label "opublikowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "niedawno"
    origin "text"
  ]
  node [
    id 4
    label "alternatywny"
    origin "text"
  ]
  node [
    id 5
    label "raport"
    origin "text"
  ]
  node [
    id 6
    label "roczny"
    origin "text"
  ]
  node [
    id 7
    label "dzia&#322;alno&#347;&#263;"
    origin "text"
  ]
  node [
    id 8
    label "koncern"
    origin "text"
  ]
  node [
    id 9
    label "problem"
    origin "text"
  ]
  node [
    id 10
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 11
    label "by&#263;"
    origin "text"
  ]
  node [
    id 12
    label "do&#347;&#263;"
    origin "text"
  ]
  node [
    id 13
    label "znana"
    origin "text"
  ]
  node [
    id 14
    label "szczeg&#243;lnie"
    origin "text"
  ]
  node [
    id 15
    label "kraj"
    origin "text"
  ]
  node [
    id 16
    label "tak"
    origin "text"
  ]
  node [
    id 17
    label "jak"
    origin "text"
  ]
  node [
    id 18
    label "nigeria"
    origin "text"
  ]
  node [
    id 19
    label "ekwador"
    origin "text"
  ]
  node [
    id 20
    label "gdzie"
    origin "text"
  ]
  node [
    id 21
    label "obecnie"
    origin "text"
  ]
  node [
    id 22
    label "toczy&#263;"
    origin "text"
  ]
  node [
    id 23
    label "si&#281;"
    origin "text"
  ]
  node [
    id 24
    label "proces"
    origin "text"
  ]
  node [
    id 25
    label "przeciw"
    origin "text"
  ]
  node [
    id 26
    label "firma"
    origin "text"
  ]
  node [
    id 27
    label "miliard"
    origin "text"
  ]
  node [
    id 28
    label "dolar"
    origin "text"
  ]
  node [
    id 29
    label "odszkodowanie"
    origin "text"
  ]
  node [
    id 30
    label "dla"
    origin "text"
  ]
  node [
    id 31
    label "ofiara"
    origin "text"
  ]
  node [
    id 32
    label "z&#322;e"
    origin "text"
  ]
  node [
    id 33
    label "ekologiczny"
    origin "text"
  ]
  node [
    id 34
    label "praktyk"
    origin "text"
  ]
  node [
    id 35
    label "lato"
    origin "text"
  ]
  node [
    id 36
    label "texaco"
    origin "text"
  ]
  node [
    id 37
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 38
    label "nale&#380;e&#263;"
    origin "text"
  ]
  node [
    id 39
    label "chevron"
    origin "text"
  ]
  node [
    id 40
    label "wyla&#263;"
    origin "text"
  ]
  node [
    id 41
    label "miejscowa"
    origin "text"
  ]
  node [
    id 42
    label "rzeka"
    origin "text"
  ]
  node [
    id 43
    label "ponad"
    origin "text"
  ]
  node [
    id 44
    label "litr"
    origin "text"
  ]
  node [
    id 45
    label "skazi&#263;"
    origin "text"
  ]
  node [
    id 46
    label "woda"
    origin "text"
  ]
  node [
    id 47
    label "zwolennik"
  ]
  node [
    id 48
    label "cz&#322;onek"
  ]
  node [
    id 49
    label "Owsiak"
  ]
  node [
    id 50
    label "Michnik"
  ]
  node [
    id 51
    label "Asnyk"
  ]
  node [
    id 52
    label "dzia&#322;anie"
  ]
  node [
    id 53
    label "campaign"
  ]
  node [
    id 54
    label "wydarzenie"
  ]
  node [
    id 55
    label "akcja"
  ]
  node [
    id 56
    label "Kampania_Wrze&#347;niowa"
  ]
  node [
    id 57
    label "upubliczni&#263;"
  ]
  node [
    id 58
    label "wydawnictwo"
  ]
  node [
    id 59
    label "wprowadzi&#263;"
  ]
  node [
    id 60
    label "picture"
  ]
  node [
    id 61
    label "ostatni"
  ]
  node [
    id 62
    label "aktualnie"
  ]
  node [
    id 63
    label "inny"
  ]
  node [
    id 64
    label "dwojaki"
  ]
  node [
    id 65
    label "alternatywnie"
  ]
  node [
    id 66
    label "niekonwencjonalny"
  ]
  node [
    id 67
    label "undergroundowy"
  ]
  node [
    id 68
    label "raport_Beveridge'a"
  ]
  node [
    id 69
    label "relacja"
  ]
  node [
    id 70
    label "raport_Fischlera"
  ]
  node [
    id 71
    label "statement"
  ]
  node [
    id 72
    label "kilkunastomiesi&#281;czny"
  ]
  node [
    id 73
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 74
    label "absolutorium"
  ]
  node [
    id 75
    label "activity"
  ]
  node [
    id 76
    label "consortium"
  ]
  node [
    id 77
    label "trudno&#347;&#263;"
  ]
  node [
    id 78
    label "sprawa"
  ]
  node [
    id 79
    label "ambaras"
  ]
  node [
    id 80
    label "problemat"
  ]
  node [
    id 81
    label "pierepa&#322;ka"
  ]
  node [
    id 82
    label "obstruction"
  ]
  node [
    id 83
    label "problematyka"
  ]
  node [
    id 84
    label "jajko_Kolumba"
  ]
  node [
    id 85
    label "subiekcja"
  ]
  node [
    id 86
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 87
    label "si&#281;ga&#263;"
  ]
  node [
    id 88
    label "trwa&#263;"
  ]
  node [
    id 89
    label "obecno&#347;&#263;"
  ]
  node [
    id 90
    label "stan"
  ]
  node [
    id 91
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 92
    label "stand"
  ]
  node [
    id 93
    label "mie&#263;_miejsce"
  ]
  node [
    id 94
    label "uczestniczy&#263;"
  ]
  node [
    id 95
    label "chodzi&#263;"
  ]
  node [
    id 96
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 97
    label "equal"
  ]
  node [
    id 98
    label "szczeg&#243;lny"
  ]
  node [
    id 99
    label "osobnie"
  ]
  node [
    id 100
    label "wyj&#261;tkowo"
  ]
  node [
    id 101
    label "specially"
  ]
  node [
    id 102
    label "Skandynawia"
  ]
  node [
    id 103
    label "Rwanda"
  ]
  node [
    id 104
    label "Filipiny"
  ]
  node [
    id 105
    label "Yorkshire"
  ]
  node [
    id 106
    label "Kaukaz"
  ]
  node [
    id 107
    label "Podbeskidzie"
  ]
  node [
    id 108
    label "Toskania"
  ]
  node [
    id 109
    label "&#321;emkowszczyzna"
  ]
  node [
    id 110
    label "obszar"
  ]
  node [
    id 111
    label "Monako"
  ]
  node [
    id 112
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 113
    label "Amhara"
  ]
  node [
    id 114
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 115
    label "Lombardia"
  ]
  node [
    id 116
    label "Korea"
  ]
  node [
    id 117
    label "Kalabria"
  ]
  node [
    id 118
    label "Czarnog&#243;ra"
  ]
  node [
    id 119
    label "Ghana"
  ]
  node [
    id 120
    label "Tyrol"
  ]
  node [
    id 121
    label "Malawi"
  ]
  node [
    id 122
    label "Indonezja"
  ]
  node [
    id 123
    label "Bu&#322;garia"
  ]
  node [
    id 124
    label "Nauru"
  ]
  node [
    id 125
    label "Kenia"
  ]
  node [
    id 126
    label "Pamir"
  ]
  node [
    id 127
    label "Kambod&#380;a"
  ]
  node [
    id 128
    label "Lubelszczyzna"
  ]
  node [
    id 129
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 130
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 131
    label "Mali"
  ]
  node [
    id 132
    label "&#379;ywiecczyzna"
  ]
  node [
    id 133
    label "Austria"
  ]
  node [
    id 134
    label "interior"
  ]
  node [
    id 135
    label "Europa_Wschodnia"
  ]
  node [
    id 136
    label "Armenia"
  ]
  node [
    id 137
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 138
    label "Fid&#380;i"
  ]
  node [
    id 139
    label "Tuwalu"
  ]
  node [
    id 140
    label "Zabajkale"
  ]
  node [
    id 141
    label "Etiopia"
  ]
  node [
    id 142
    label "Malezja"
  ]
  node [
    id 143
    label "Malta"
  ]
  node [
    id 144
    label "Kaszuby"
  ]
  node [
    id 145
    label "Noworosja"
  ]
  node [
    id 146
    label "Bo&#347;nia"
  ]
  node [
    id 147
    label "Tad&#380;ykistan"
  ]
  node [
    id 148
    label "Grenada"
  ]
  node [
    id 149
    label "Ba&#322;kany"
  ]
  node [
    id 150
    label "Wehrlen"
  ]
  node [
    id 151
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 152
    label "Anglia"
  ]
  node [
    id 153
    label "Kielecczyzna"
  ]
  node [
    id 154
    label "Rumunia"
  ]
  node [
    id 155
    label "Pomorze_Zachodnie"
  ]
  node [
    id 156
    label "Maroko"
  ]
  node [
    id 157
    label "Bhutan"
  ]
  node [
    id 158
    label "Opolskie"
  ]
  node [
    id 159
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 160
    label "Ko&#322;yma"
  ]
  node [
    id 161
    label "Oksytania"
  ]
  node [
    id 162
    label "S&#322;owacja"
  ]
  node [
    id 163
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 164
    label "Seszele"
  ]
  node [
    id 165
    label "Syjon"
  ]
  node [
    id 166
    label "Kuwejt"
  ]
  node [
    id 167
    label "Arabia_Saudyjska"
  ]
  node [
    id 168
    label "Kociewie"
  ]
  node [
    id 169
    label "Kanada"
  ]
  node [
    id 170
    label "Ekwador"
  ]
  node [
    id 171
    label "ziemia"
  ]
  node [
    id 172
    label "Japonia"
  ]
  node [
    id 173
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 174
    label "Hiszpania"
  ]
  node [
    id 175
    label "Wyspy_Marshalla"
  ]
  node [
    id 176
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 177
    label "D&#380;ibuti"
  ]
  node [
    id 178
    label "Botswana"
  ]
  node [
    id 179
    label "Huculszczyzna"
  ]
  node [
    id 180
    label "Wietnam"
  ]
  node [
    id 181
    label "Egipt"
  ]
  node [
    id 182
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 183
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 184
    label "Burkina_Faso"
  ]
  node [
    id 185
    label "Bawaria"
  ]
  node [
    id 186
    label "Niemcy"
  ]
  node [
    id 187
    label "Khitai"
  ]
  node [
    id 188
    label "Macedonia"
  ]
  node [
    id 189
    label "Albania"
  ]
  node [
    id 190
    label "Madagaskar"
  ]
  node [
    id 191
    label "Bahrajn"
  ]
  node [
    id 192
    label "Jemen"
  ]
  node [
    id 193
    label "Lesoto"
  ]
  node [
    id 194
    label "Maghreb"
  ]
  node [
    id 195
    label "Samoa"
  ]
  node [
    id 196
    label "Andora"
  ]
  node [
    id 197
    label "Bory_Tucholskie"
  ]
  node [
    id 198
    label "Chiny"
  ]
  node [
    id 199
    label "Europa_Zachodnia"
  ]
  node [
    id 200
    label "Cypr"
  ]
  node [
    id 201
    label "Wielka_Brytania"
  ]
  node [
    id 202
    label "Kerala"
  ]
  node [
    id 203
    label "Podhale"
  ]
  node [
    id 204
    label "Kabylia"
  ]
  node [
    id 205
    label "Ukraina"
  ]
  node [
    id 206
    label "Paragwaj"
  ]
  node [
    id 207
    label "Trynidad_i_Tobago"
  ]
  node [
    id 208
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 209
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 210
    label "Ma&#322;opolska"
  ]
  node [
    id 211
    label "Polesie"
  ]
  node [
    id 212
    label "Liguria"
  ]
  node [
    id 213
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 214
    label "Libia"
  ]
  node [
    id 215
    label "&#321;&#243;dzkie"
  ]
  node [
    id 216
    label "Surinam"
  ]
  node [
    id 217
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 218
    label "Palestyna"
  ]
  node [
    id 219
    label "Nigeria"
  ]
  node [
    id 220
    label "Australia"
  ]
  node [
    id 221
    label "Honduras"
  ]
  node [
    id 222
    label "Bojkowszczyzna"
  ]
  node [
    id 223
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 224
    label "Karaiby"
  ]
  node [
    id 225
    label "Peru"
  ]
  node [
    id 226
    label "USA"
  ]
  node [
    id 227
    label "Bangladesz"
  ]
  node [
    id 228
    label "Kazachstan"
  ]
  node [
    id 229
    label "Nepal"
  ]
  node [
    id 230
    label "Irak"
  ]
  node [
    id 231
    label "Nadrenia"
  ]
  node [
    id 232
    label "Sudan"
  ]
  node [
    id 233
    label "S&#261;decczyzna"
  ]
  node [
    id 234
    label "Sand&#380;ak"
  ]
  node [
    id 235
    label "San_Marino"
  ]
  node [
    id 236
    label "Burundi"
  ]
  node [
    id 237
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 238
    label "Dominikana"
  ]
  node [
    id 239
    label "Komory"
  ]
  node [
    id 240
    label "Zakarpacie"
  ]
  node [
    id 241
    label "Gwatemala"
  ]
  node [
    id 242
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 243
    label "Zag&#243;rze"
  ]
  node [
    id 244
    label "Andaluzja"
  ]
  node [
    id 245
    label "granica_pa&#324;stwa"
  ]
  node [
    id 246
    label "Turkiestan"
  ]
  node [
    id 247
    label "Naddniestrze"
  ]
  node [
    id 248
    label "Hercegowina"
  ]
  node [
    id 249
    label "Brunei"
  ]
  node [
    id 250
    label "Iran"
  ]
  node [
    id 251
    label "jednostka_administracyjna"
  ]
  node [
    id 252
    label "Zimbabwe"
  ]
  node [
    id 253
    label "Namibia"
  ]
  node [
    id 254
    label "Meksyk"
  ]
  node [
    id 255
    label "Opolszczyzna"
  ]
  node [
    id 256
    label "Kamerun"
  ]
  node [
    id 257
    label "Afryka_Wschodnia"
  ]
  node [
    id 258
    label "Szlezwik"
  ]
  node [
    id 259
    label "Lotaryngia"
  ]
  node [
    id 260
    label "Somalia"
  ]
  node [
    id 261
    label "Angola"
  ]
  node [
    id 262
    label "Gabon"
  ]
  node [
    id 263
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 264
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 265
    label "Nowa_Zelandia"
  ]
  node [
    id 266
    label "Mozambik"
  ]
  node [
    id 267
    label "Tunezja"
  ]
  node [
    id 268
    label "Tajwan"
  ]
  node [
    id 269
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 270
    label "Liban"
  ]
  node [
    id 271
    label "Jordania"
  ]
  node [
    id 272
    label "Tonga"
  ]
  node [
    id 273
    label "Czad"
  ]
  node [
    id 274
    label "Gwinea"
  ]
  node [
    id 275
    label "Liberia"
  ]
  node [
    id 276
    label "Belize"
  ]
  node [
    id 277
    label "Mazowsze"
  ]
  node [
    id 278
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 279
    label "Benin"
  ]
  node [
    id 280
    label "&#321;otwa"
  ]
  node [
    id 281
    label "Syria"
  ]
  node [
    id 282
    label "Afryka_Zachodnia"
  ]
  node [
    id 283
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 284
    label "Dominika"
  ]
  node [
    id 285
    label "Antigua_i_Barbuda"
  ]
  node [
    id 286
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 287
    label "Hanower"
  ]
  node [
    id 288
    label "Galicja"
  ]
  node [
    id 289
    label "Szkocja"
  ]
  node [
    id 290
    label "Walia"
  ]
  node [
    id 291
    label "Afganistan"
  ]
  node [
    id 292
    label "W&#322;ochy"
  ]
  node [
    id 293
    label "Kiribati"
  ]
  node [
    id 294
    label "Szwajcaria"
  ]
  node [
    id 295
    label "Powi&#347;le"
  ]
  node [
    id 296
    label "Chorwacja"
  ]
  node [
    id 297
    label "Sahara_Zachodnia"
  ]
  node [
    id 298
    label "Tajlandia"
  ]
  node [
    id 299
    label "Salwador"
  ]
  node [
    id 300
    label "Bahamy"
  ]
  node [
    id 301
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 302
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 303
    label "Zamojszczyzna"
  ]
  node [
    id 304
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 305
    label "S&#322;owenia"
  ]
  node [
    id 306
    label "Gambia"
  ]
  node [
    id 307
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 308
    label "Urugwaj"
  ]
  node [
    id 309
    label "Podlasie"
  ]
  node [
    id 310
    label "Zair"
  ]
  node [
    id 311
    label "Erytrea"
  ]
  node [
    id 312
    label "Laponia"
  ]
  node [
    id 313
    label "Kujawy"
  ]
  node [
    id 314
    label "Umbria"
  ]
  node [
    id 315
    label "Rosja"
  ]
  node [
    id 316
    label "Mauritius"
  ]
  node [
    id 317
    label "Niger"
  ]
  node [
    id 318
    label "Uganda"
  ]
  node [
    id 319
    label "Turkmenistan"
  ]
  node [
    id 320
    label "Turcja"
  ]
  node [
    id 321
    label "Mezoameryka"
  ]
  node [
    id 322
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 323
    label "Irlandia"
  ]
  node [
    id 324
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 325
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 326
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 327
    label "Gwinea_Bissau"
  ]
  node [
    id 328
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 329
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 330
    label "Kurdystan"
  ]
  node [
    id 331
    label "Belgia"
  ]
  node [
    id 332
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 333
    label "Palau"
  ]
  node [
    id 334
    label "Barbados"
  ]
  node [
    id 335
    label "Wenezuela"
  ]
  node [
    id 336
    label "W&#281;gry"
  ]
  node [
    id 337
    label "Chile"
  ]
  node [
    id 338
    label "Argentyna"
  ]
  node [
    id 339
    label "Kolumbia"
  ]
  node [
    id 340
    label "Armagnac"
  ]
  node [
    id 341
    label "Kampania"
  ]
  node [
    id 342
    label "Sierra_Leone"
  ]
  node [
    id 343
    label "Azerbejd&#380;an"
  ]
  node [
    id 344
    label "Kongo"
  ]
  node [
    id 345
    label "Polinezja"
  ]
  node [
    id 346
    label "Warmia"
  ]
  node [
    id 347
    label "Pakistan"
  ]
  node [
    id 348
    label "Liechtenstein"
  ]
  node [
    id 349
    label "Wielkopolska"
  ]
  node [
    id 350
    label "Nikaragua"
  ]
  node [
    id 351
    label "Senegal"
  ]
  node [
    id 352
    label "brzeg"
  ]
  node [
    id 353
    label "Bordeaux"
  ]
  node [
    id 354
    label "Lauda"
  ]
  node [
    id 355
    label "Indie"
  ]
  node [
    id 356
    label "Mazury"
  ]
  node [
    id 357
    label "Suazi"
  ]
  node [
    id 358
    label "Polska"
  ]
  node [
    id 359
    label "Algieria"
  ]
  node [
    id 360
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 361
    label "Jamajka"
  ]
  node [
    id 362
    label "Timor_Wschodni"
  ]
  node [
    id 363
    label "Oceania"
  ]
  node [
    id 364
    label "Kostaryka"
  ]
  node [
    id 365
    label "Lasko"
  ]
  node [
    id 366
    label "Podkarpacie"
  ]
  node [
    id 367
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 368
    label "Kuba"
  ]
  node [
    id 369
    label "Mauretania"
  ]
  node [
    id 370
    label "Amazonia"
  ]
  node [
    id 371
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 372
    label "Portoryko"
  ]
  node [
    id 373
    label "Brazylia"
  ]
  node [
    id 374
    label "Mo&#322;dawia"
  ]
  node [
    id 375
    label "organizacja"
  ]
  node [
    id 376
    label "Litwa"
  ]
  node [
    id 377
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 378
    label "Kirgistan"
  ]
  node [
    id 379
    label "Izrael"
  ]
  node [
    id 380
    label "Grecja"
  ]
  node [
    id 381
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 382
    label "Kurpie"
  ]
  node [
    id 383
    label "Holandia"
  ]
  node [
    id 384
    label "Sri_Lanka"
  ]
  node [
    id 385
    label "Tonkin"
  ]
  node [
    id 386
    label "Katar"
  ]
  node [
    id 387
    label "Azja_Wschodnia"
  ]
  node [
    id 388
    label "Kaszmir"
  ]
  node [
    id 389
    label "Mikronezja"
  ]
  node [
    id 390
    label "Ukraina_Zachodnia"
  ]
  node [
    id 391
    label "Laos"
  ]
  node [
    id 392
    label "Mongolia"
  ]
  node [
    id 393
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 394
    label "Malediwy"
  ]
  node [
    id 395
    label "Zambia"
  ]
  node [
    id 396
    label "Turyngia"
  ]
  node [
    id 397
    label "Tanzania"
  ]
  node [
    id 398
    label "Gujana"
  ]
  node [
    id 399
    label "Apulia"
  ]
  node [
    id 400
    label "Uzbekistan"
  ]
  node [
    id 401
    label "Panama"
  ]
  node [
    id 402
    label "Czechy"
  ]
  node [
    id 403
    label "Gruzja"
  ]
  node [
    id 404
    label "Baszkiria"
  ]
  node [
    id 405
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 406
    label "Francja"
  ]
  node [
    id 407
    label "Serbia"
  ]
  node [
    id 408
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 409
    label "Togo"
  ]
  node [
    id 410
    label "Estonia"
  ]
  node [
    id 411
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 412
    label "Indochiny"
  ]
  node [
    id 413
    label "Boliwia"
  ]
  node [
    id 414
    label "Oman"
  ]
  node [
    id 415
    label "Portugalia"
  ]
  node [
    id 416
    label "Wyspy_Salomona"
  ]
  node [
    id 417
    label "Haiti"
  ]
  node [
    id 418
    label "Luksemburg"
  ]
  node [
    id 419
    label "Lubuskie"
  ]
  node [
    id 420
    label "Biskupizna"
  ]
  node [
    id 421
    label "Birma"
  ]
  node [
    id 422
    label "Rodezja"
  ]
  node [
    id 423
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 424
    label "byd&#322;o"
  ]
  node [
    id 425
    label "zobo"
  ]
  node [
    id 426
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 427
    label "yakalo"
  ]
  node [
    id 428
    label "dzo"
  ]
  node [
    id 429
    label "ninie"
  ]
  node [
    id 430
    label "aktualny"
  ]
  node [
    id 431
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 432
    label "niszczy&#263;"
  ]
  node [
    id 433
    label "obrabia&#263;"
  ]
  node [
    id 434
    label "wypuszcza&#263;"
  ]
  node [
    id 435
    label "przemieszcza&#263;"
  ]
  node [
    id 436
    label "zabiera&#263;"
  ]
  node [
    id 437
    label "prowadzi&#263;"
  ]
  node [
    id 438
    label "p&#281;dzi&#263;"
  ]
  node [
    id 439
    label "grind"
  ]
  node [
    id 440
    label "force"
  ]
  node [
    id 441
    label "manipulate"
  ]
  node [
    id 442
    label "przesuwa&#263;"
  ]
  node [
    id 443
    label "tacza&#263;"
  ]
  node [
    id 444
    label "&#380;y&#263;"
  ]
  node [
    id 445
    label "carry"
  ]
  node [
    id 446
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 447
    label "&#347;ci&#261;ga&#263;"
  ]
  node [
    id 448
    label "rusza&#263;"
  ]
  node [
    id 449
    label "legislacyjnie"
  ]
  node [
    id 450
    label "kognicja"
  ]
  node [
    id 451
    label "przebieg"
  ]
  node [
    id 452
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 453
    label "przes&#322;anka"
  ]
  node [
    id 454
    label "rozprawa"
  ]
  node [
    id 455
    label "zjawisko"
  ]
  node [
    id 456
    label "nast&#281;pstwo"
  ]
  node [
    id 457
    label "cz&#322;owiek"
  ]
  node [
    id 458
    label "Hortex"
  ]
  node [
    id 459
    label "MAC"
  ]
  node [
    id 460
    label "reengineering"
  ]
  node [
    id 461
    label "nazwa_w&#322;asna"
  ]
  node [
    id 462
    label "podmiot_gospodarczy"
  ]
  node [
    id 463
    label "Google"
  ]
  node [
    id 464
    label "zaufanie"
  ]
  node [
    id 465
    label "biurowiec"
  ]
  node [
    id 466
    label "networking"
  ]
  node [
    id 467
    label "zasoby_ludzkie"
  ]
  node [
    id 468
    label "interes"
  ]
  node [
    id 469
    label "paczkarnia"
  ]
  node [
    id 470
    label "Canon"
  ]
  node [
    id 471
    label "HP"
  ]
  node [
    id 472
    label "Baltona"
  ]
  node [
    id 473
    label "Pewex"
  ]
  node [
    id 474
    label "MAN_SE"
  ]
  node [
    id 475
    label "Apeks"
  ]
  node [
    id 476
    label "zasoby"
  ]
  node [
    id 477
    label "Orbis"
  ]
  node [
    id 478
    label "miejsce_pracy"
  ]
  node [
    id 479
    label "siedziba"
  ]
  node [
    id 480
    label "Spo&#322;em"
  ]
  node [
    id 481
    label "przedsi&#281;biorczo&#347;&#263;"
  ]
  node [
    id 482
    label "Orlen"
  ]
  node [
    id 483
    label "klasa"
  ]
  node [
    id 484
    label "liczba"
  ]
  node [
    id 485
    label "Mariany_P&#243;&#322;nocne"
  ]
  node [
    id 486
    label "cent"
  ]
  node [
    id 487
    label "Sint_Eustatius"
  ]
  node [
    id 488
    label "Saba"
  ]
  node [
    id 489
    label "Bonaire"
  ]
  node [
    id 490
    label "jednostka_monetarna"
  ]
  node [
    id 491
    label "zap&#322;ata"
  ]
  node [
    id 492
    label "refund"
  ]
  node [
    id 493
    label "ryzyko_niewygas&#322;e"
  ]
  node [
    id 494
    label "zado&#347;&#263;uczynienie"
  ]
  node [
    id 495
    label "gamo&#324;"
  ]
  node [
    id 496
    label "istota_&#380;ywa"
  ]
  node [
    id 497
    label "dupa_wo&#322;owa"
  ]
  node [
    id 498
    label "przedmiot"
  ]
  node [
    id 499
    label "pastwa"
  ]
  node [
    id 500
    label "rzecz"
  ]
  node [
    id 501
    label "zbi&#243;rka"
  ]
  node [
    id 502
    label "dar"
  ]
  node [
    id 503
    label "nieszcz&#281;&#347;nik"
  ]
  node [
    id 504
    label "nastawienie"
  ]
  node [
    id 505
    label "crack"
  ]
  node [
    id 506
    label "zwierz&#281;"
  ]
  node [
    id 507
    label "ko&#347;cielny"
  ]
  node [
    id 508
    label "po&#347;miewisko"
  ]
  node [
    id 509
    label "cholerstwo"
  ]
  node [
    id 510
    label "negatywno&#347;&#263;"
  ]
  node [
    id 511
    label "ailment"
  ]
  node [
    id 512
    label "ekologicznie"
  ]
  node [
    id 513
    label "przyjazny"
  ]
  node [
    id 514
    label "znawca"
  ]
  node [
    id 515
    label "pora_roku"
  ]
  node [
    id 516
    label "trza"
  ]
  node [
    id 517
    label "zalicza&#263;_si&#281;"
  ]
  node [
    id 518
    label "para"
  ]
  node [
    id 519
    label "necessity"
  ]
  node [
    id 520
    label "odprawi&#263;"
  ]
  node [
    id 521
    label "flood"
  ]
  node [
    id 522
    label "wyrzuci&#263;"
  ]
  node [
    id 523
    label "pokry&#263;"
  ]
  node [
    id 524
    label "wydosta&#263;_si&#281;"
  ]
  node [
    id 525
    label "wyrazi&#263;"
  ]
  node [
    id 526
    label "spill"
  ]
  node [
    id 527
    label "flow_out"
  ]
  node [
    id 528
    label "opr&#243;&#380;ni&#263;"
  ]
  node [
    id 529
    label "Izera"
  ]
  node [
    id 530
    label "Orla"
  ]
  node [
    id 531
    label "Amazonka"
  ]
  node [
    id 532
    label "Kaczawa"
  ]
  node [
    id 533
    label "Hudson"
  ]
  node [
    id 534
    label "Drina"
  ]
  node [
    id 535
    label "Windawa"
  ]
  node [
    id 536
    label "Wereszyca"
  ]
  node [
    id 537
    label "Wis&#322;a"
  ]
  node [
    id 538
    label "Peczora"
  ]
  node [
    id 539
    label "Pad"
  ]
  node [
    id 540
    label "ciek_wodny"
  ]
  node [
    id 541
    label "Alabama"
  ]
  node [
    id 542
    label "Ren"
  ]
  node [
    id 543
    label "Dunaj"
  ]
  node [
    id 544
    label "S&#322;upia"
  ]
  node [
    id 545
    label "Orinoko"
  ]
  node [
    id 546
    label "Wia&#378;ma"
  ]
  node [
    id 547
    label "Zyrianka"
  ]
  node [
    id 548
    label "Pia&#347;nica"
  ]
  node [
    id 549
    label "Sekwana"
  ]
  node [
    id 550
    label "Dniestr"
  ]
  node [
    id 551
    label "Nil"
  ]
  node [
    id 552
    label "Turiec"
  ]
  node [
    id 553
    label "Dniepr"
  ]
  node [
    id 554
    label "Cisa"
  ]
  node [
    id 555
    label "D&#378;wina"
  ]
  node [
    id 556
    label "Odra"
  ]
  node [
    id 557
    label "Supra&#347;l"
  ]
  node [
    id 558
    label "Wieprza"
  ]
  node [
    id 559
    label "woda_powierzchniowa"
  ]
  node [
    id 560
    label "Amur"
  ]
  node [
    id 561
    label "Anadyr"
  ]
  node [
    id 562
    label "Podkamienna_Tunguzka"
  ]
  node [
    id 563
    label "ilo&#347;&#263;"
  ]
  node [
    id 564
    label "So&#322;a"
  ]
  node [
    id 565
    label "Zi&#281;bina"
  ]
  node [
    id 566
    label "Ropa"
  ]
  node [
    id 567
    label "Mozela"
  ]
  node [
    id 568
    label "Styks"
  ]
  node [
    id 569
    label "Witim"
  ]
  node [
    id 570
    label "Don"
  ]
  node [
    id 571
    label "Sanica"
  ]
  node [
    id 572
    label "potamoplankton"
  ]
  node [
    id 573
    label "Wo&#322;ga"
  ]
  node [
    id 574
    label "Moza"
  ]
  node [
    id 575
    label "Pas&#322;&#281;ka"
  ]
  node [
    id 576
    label "Niemen"
  ]
  node [
    id 577
    label "Lena"
  ]
  node [
    id 578
    label "Dwina"
  ]
  node [
    id 579
    label "Zarycz"
  ]
  node [
    id 580
    label "Brze&#378;niczanka"
  ]
  node [
    id 581
    label "odp&#322;ywanie"
  ]
  node [
    id 582
    label "Jenisej"
  ]
  node [
    id 583
    label "Ussuri"
  ]
  node [
    id 584
    label "wpadni&#281;cie"
  ]
  node [
    id 585
    label "Pr&#261;dnik"
  ]
  node [
    id 586
    label "Lete"
  ]
  node [
    id 587
    label "&#321;aba"
  ]
  node [
    id 588
    label "Ob"
  ]
  node [
    id 589
    label "Rega"
  ]
  node [
    id 590
    label "Widawa"
  ]
  node [
    id 591
    label "Newa"
  ]
  node [
    id 592
    label "Berezyna"
  ]
  node [
    id 593
    label "wpadanie"
  ]
  node [
    id 594
    label "&#321;upawa"
  ]
  node [
    id 595
    label "strumie&#324;"
  ]
  node [
    id 596
    label "Pars&#281;ta"
  ]
  node [
    id 597
    label "ghaty"
  ]
  node [
    id 598
    label "dekalitr"
  ]
  node [
    id 599
    label "metryczna_jednostka_obj&#281;to&#347;ci"
  ]
  node [
    id 600
    label "flacha"
  ]
  node [
    id 601
    label "hektolitr"
  ]
  node [
    id 602
    label "poison"
  ]
  node [
    id 603
    label "zanieczy&#347;ci&#263;"
  ]
  node [
    id 604
    label "corrupt"
  ]
  node [
    id 605
    label "zdeformowa&#263;"
  ]
  node [
    id 606
    label "wypowied&#378;"
  ]
  node [
    id 607
    label "obiekt_naturalny"
  ]
  node [
    id 608
    label "bicie"
  ]
  node [
    id 609
    label "wysi&#281;k"
  ]
  node [
    id 610
    label "pustka"
  ]
  node [
    id 611
    label "woda_s&#322;odka"
  ]
  node [
    id 612
    label "p&#322;ycizna"
  ]
  node [
    id 613
    label "ciecz"
  ]
  node [
    id 614
    label "spi&#281;trza&#263;"
  ]
  node [
    id 615
    label "uj&#281;cie_wody"
  ]
  node [
    id 616
    label "chlasta&#263;"
  ]
  node [
    id 617
    label "spi&#281;trzy&#263;"
  ]
  node [
    id 618
    label "nap&#243;j"
  ]
  node [
    id 619
    label "bombast"
  ]
  node [
    id 620
    label "water"
  ]
  node [
    id 621
    label "kryptodepresja"
  ]
  node [
    id 622
    label "wodnik"
  ]
  node [
    id 623
    label "pojazd"
  ]
  node [
    id 624
    label "&#347;r&#243;dl&#261;dowy"
  ]
  node [
    id 625
    label "fala"
  ]
  node [
    id 626
    label "Waruna"
  ]
  node [
    id 627
    label "nap&#243;j_bezalkoholowy"
  ]
  node [
    id 628
    label "zrzut"
  ]
  node [
    id 629
    label "dotleni&#263;"
  ]
  node [
    id 630
    label "utylizator"
  ]
  node [
    id 631
    label "przyroda"
  ]
  node [
    id 632
    label "uci&#261;g"
  ]
  node [
    id 633
    label "wybrze&#380;e"
  ]
  node [
    id 634
    label "nabranie"
  ]
  node [
    id 635
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 636
    label "klarownik"
  ]
  node [
    id 637
    label "chlastanie"
  ]
  node [
    id 638
    label "przybrze&#380;e"
  ]
  node [
    id 639
    label "deklamacja"
  ]
  node [
    id 640
    label "spi&#281;trzenie"
  ]
  node [
    id 641
    label "przybieranie"
  ]
  node [
    id 642
    label "nabra&#263;"
  ]
  node [
    id 643
    label "tlenek"
  ]
  node [
    id 644
    label "spi&#281;trzanie"
  ]
  node [
    id 645
    label "l&#243;d"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 8
    target 34
  ]
  edge [
    source 8
    target 35
  ]
  edge [
    source 8
    target 26
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 8
  ]
  edge [
    source 8
    target 27
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 87
  ]
  edge [
    source 11
    target 88
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 11
    target 92
  ]
  edge [
    source 11
    target 93
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 98
  ]
  edge [
    source 14
    target 99
  ]
  edge [
    source 14
    target 100
  ]
  edge [
    source 14
    target 101
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 102
  ]
  edge [
    source 15
    target 103
  ]
  edge [
    source 15
    target 104
  ]
  edge [
    source 15
    target 105
  ]
  edge [
    source 15
    target 106
  ]
  edge [
    source 15
    target 107
  ]
  edge [
    source 15
    target 108
  ]
  edge [
    source 15
    target 109
  ]
  edge [
    source 15
    target 110
  ]
  edge [
    source 15
    target 111
  ]
  edge [
    source 15
    target 112
  ]
  edge [
    source 15
    target 113
  ]
  edge [
    source 15
    target 114
  ]
  edge [
    source 15
    target 115
  ]
  edge [
    source 15
    target 116
  ]
  edge [
    source 15
    target 117
  ]
  edge [
    source 15
    target 118
  ]
  edge [
    source 15
    target 119
  ]
  edge [
    source 15
    target 120
  ]
  edge [
    source 15
    target 121
  ]
  edge [
    source 15
    target 122
  ]
  edge [
    source 15
    target 123
  ]
  edge [
    source 15
    target 124
  ]
  edge [
    source 15
    target 125
  ]
  edge [
    source 15
    target 126
  ]
  edge [
    source 15
    target 127
  ]
  edge [
    source 15
    target 128
  ]
  edge [
    source 15
    target 129
  ]
  edge [
    source 15
    target 130
  ]
  edge [
    source 15
    target 131
  ]
  edge [
    source 15
    target 132
  ]
  edge [
    source 15
    target 133
  ]
  edge [
    source 15
    target 134
  ]
  edge [
    source 15
    target 135
  ]
  edge [
    source 15
    target 136
  ]
  edge [
    source 15
    target 137
  ]
  edge [
    source 15
    target 138
  ]
  edge [
    source 15
    target 139
  ]
  edge [
    source 15
    target 140
  ]
  edge [
    source 15
    target 141
  ]
  edge [
    source 15
    target 142
  ]
  edge [
    source 15
    target 143
  ]
  edge [
    source 15
    target 144
  ]
  edge [
    source 15
    target 145
  ]
  edge [
    source 15
    target 146
  ]
  edge [
    source 15
    target 147
  ]
  edge [
    source 15
    target 148
  ]
  edge [
    source 15
    target 149
  ]
  edge [
    source 15
    target 150
  ]
  edge [
    source 15
    target 151
  ]
  edge [
    source 15
    target 152
  ]
  edge [
    source 15
    target 153
  ]
  edge [
    source 15
    target 154
  ]
  edge [
    source 15
    target 155
  ]
  edge [
    source 15
    target 156
  ]
  edge [
    source 15
    target 157
  ]
  edge [
    source 15
    target 158
  ]
  edge [
    source 15
    target 159
  ]
  edge [
    source 15
    target 160
  ]
  edge [
    source 15
    target 161
  ]
  edge [
    source 15
    target 162
  ]
  edge [
    source 15
    target 163
  ]
  edge [
    source 15
    target 164
  ]
  edge [
    source 15
    target 165
  ]
  edge [
    source 15
    target 166
  ]
  edge [
    source 15
    target 167
  ]
  edge [
    source 15
    target 168
  ]
  edge [
    source 15
    target 169
  ]
  edge [
    source 15
    target 170
  ]
  edge [
    source 15
    target 171
  ]
  edge [
    source 15
    target 172
  ]
  edge [
    source 15
    target 173
  ]
  edge [
    source 15
    target 174
  ]
  edge [
    source 15
    target 175
  ]
  edge [
    source 15
    target 176
  ]
  edge [
    source 15
    target 177
  ]
  edge [
    source 15
    target 178
  ]
  edge [
    source 15
    target 179
  ]
  edge [
    source 15
    target 180
  ]
  edge [
    source 15
    target 181
  ]
  edge [
    source 15
    target 182
  ]
  edge [
    source 15
    target 183
  ]
  edge [
    source 15
    target 184
  ]
  edge [
    source 15
    target 185
  ]
  edge [
    source 15
    target 186
  ]
  edge [
    source 15
    target 187
  ]
  edge [
    source 15
    target 188
  ]
  edge [
    source 15
    target 189
  ]
  edge [
    source 15
    target 190
  ]
  edge [
    source 15
    target 191
  ]
  edge [
    source 15
    target 192
  ]
  edge [
    source 15
    target 193
  ]
  edge [
    source 15
    target 194
  ]
  edge [
    source 15
    target 195
  ]
  edge [
    source 15
    target 196
  ]
  edge [
    source 15
    target 197
  ]
  edge [
    source 15
    target 198
  ]
  edge [
    source 15
    target 199
  ]
  edge [
    source 15
    target 200
  ]
  edge [
    source 15
    target 201
  ]
  edge [
    source 15
    target 202
  ]
  edge [
    source 15
    target 203
  ]
  edge [
    source 15
    target 204
  ]
  edge [
    source 15
    target 205
  ]
  edge [
    source 15
    target 206
  ]
  edge [
    source 15
    target 207
  ]
  edge [
    source 15
    target 208
  ]
  edge [
    source 15
    target 209
  ]
  edge [
    source 15
    target 210
  ]
  edge [
    source 15
    target 211
  ]
  edge [
    source 15
    target 212
  ]
  edge [
    source 15
    target 213
  ]
  edge [
    source 15
    target 214
  ]
  edge [
    source 15
    target 215
  ]
  edge [
    source 15
    target 216
  ]
  edge [
    source 15
    target 217
  ]
  edge [
    source 15
    target 218
  ]
  edge [
    source 15
    target 219
  ]
  edge [
    source 15
    target 220
  ]
  edge [
    source 15
    target 221
  ]
  edge [
    source 15
    target 222
  ]
  edge [
    source 15
    target 223
  ]
  edge [
    source 15
    target 224
  ]
  edge [
    source 15
    target 225
  ]
  edge [
    source 15
    target 226
  ]
  edge [
    source 15
    target 227
  ]
  edge [
    source 15
    target 228
  ]
  edge [
    source 15
    target 229
  ]
  edge [
    source 15
    target 230
  ]
  edge [
    source 15
    target 231
  ]
  edge [
    source 15
    target 232
  ]
  edge [
    source 15
    target 233
  ]
  edge [
    source 15
    target 234
  ]
  edge [
    source 15
    target 235
  ]
  edge [
    source 15
    target 236
  ]
  edge [
    source 15
    target 237
  ]
  edge [
    source 15
    target 238
  ]
  edge [
    source 15
    target 239
  ]
  edge [
    source 15
    target 240
  ]
  edge [
    source 15
    target 241
  ]
  edge [
    source 15
    target 242
  ]
  edge [
    source 15
    target 243
  ]
  edge [
    source 15
    target 244
  ]
  edge [
    source 15
    target 245
  ]
  edge [
    source 15
    target 246
  ]
  edge [
    source 15
    target 247
  ]
  edge [
    source 15
    target 248
  ]
  edge [
    source 15
    target 249
  ]
  edge [
    source 15
    target 250
  ]
  edge [
    source 15
    target 251
  ]
  edge [
    source 15
    target 252
  ]
  edge [
    source 15
    target 253
  ]
  edge [
    source 15
    target 254
  ]
  edge [
    source 15
    target 255
  ]
  edge [
    source 15
    target 256
  ]
  edge [
    source 15
    target 257
  ]
  edge [
    source 15
    target 258
  ]
  edge [
    source 15
    target 259
  ]
  edge [
    source 15
    target 260
  ]
  edge [
    source 15
    target 261
  ]
  edge [
    source 15
    target 262
  ]
  edge [
    source 15
    target 263
  ]
  edge [
    source 15
    target 264
  ]
  edge [
    source 15
    target 265
  ]
  edge [
    source 15
    target 266
  ]
  edge [
    source 15
    target 267
  ]
  edge [
    source 15
    target 268
  ]
  edge [
    source 15
    target 269
  ]
  edge [
    source 15
    target 270
  ]
  edge [
    source 15
    target 271
  ]
  edge [
    source 15
    target 272
  ]
  edge [
    source 15
    target 273
  ]
  edge [
    source 15
    target 274
  ]
  edge [
    source 15
    target 275
  ]
  edge [
    source 15
    target 276
  ]
  edge [
    source 15
    target 277
  ]
  edge [
    source 15
    target 278
  ]
  edge [
    source 15
    target 279
  ]
  edge [
    source 15
    target 280
  ]
  edge [
    source 15
    target 281
  ]
  edge [
    source 15
    target 282
  ]
  edge [
    source 15
    target 283
  ]
  edge [
    source 15
    target 284
  ]
  edge [
    source 15
    target 285
  ]
  edge [
    source 15
    target 286
  ]
  edge [
    source 15
    target 287
  ]
  edge [
    source 15
    target 288
  ]
  edge [
    source 15
    target 289
  ]
  edge [
    source 15
    target 290
  ]
  edge [
    source 15
    target 291
  ]
  edge [
    source 15
    target 292
  ]
  edge [
    source 15
    target 293
  ]
  edge [
    source 15
    target 294
  ]
  edge [
    source 15
    target 295
  ]
  edge [
    source 15
    target 296
  ]
  edge [
    source 15
    target 297
  ]
  edge [
    source 15
    target 298
  ]
  edge [
    source 15
    target 299
  ]
  edge [
    source 15
    target 300
  ]
  edge [
    source 15
    target 301
  ]
  edge [
    source 15
    target 302
  ]
  edge [
    source 15
    target 303
  ]
  edge [
    source 15
    target 304
  ]
  edge [
    source 15
    target 305
  ]
  edge [
    source 15
    target 306
  ]
  edge [
    source 15
    target 307
  ]
  edge [
    source 15
    target 308
  ]
  edge [
    source 15
    target 309
  ]
  edge [
    source 15
    target 310
  ]
  edge [
    source 15
    target 311
  ]
  edge [
    source 15
    target 312
  ]
  edge [
    source 15
    target 313
  ]
  edge [
    source 15
    target 314
  ]
  edge [
    source 15
    target 315
  ]
  edge [
    source 15
    target 316
  ]
  edge [
    source 15
    target 317
  ]
  edge [
    source 15
    target 318
  ]
  edge [
    source 15
    target 319
  ]
  edge [
    source 15
    target 320
  ]
  edge [
    source 15
    target 321
  ]
  edge [
    source 15
    target 322
  ]
  edge [
    source 15
    target 323
  ]
  edge [
    source 15
    target 324
  ]
  edge [
    source 15
    target 325
  ]
  edge [
    source 15
    target 326
  ]
  edge [
    source 15
    target 327
  ]
  edge [
    source 15
    target 328
  ]
  edge [
    source 15
    target 329
  ]
  edge [
    source 15
    target 330
  ]
  edge [
    source 15
    target 331
  ]
  edge [
    source 15
    target 332
  ]
  edge [
    source 15
    target 333
  ]
  edge [
    source 15
    target 334
  ]
  edge [
    source 15
    target 335
  ]
  edge [
    source 15
    target 336
  ]
  edge [
    source 15
    target 337
  ]
  edge [
    source 15
    target 338
  ]
  edge [
    source 15
    target 339
  ]
  edge [
    source 15
    target 340
  ]
  edge [
    source 15
    target 341
  ]
  edge [
    source 15
    target 342
  ]
  edge [
    source 15
    target 343
  ]
  edge [
    source 15
    target 344
  ]
  edge [
    source 15
    target 345
  ]
  edge [
    source 15
    target 346
  ]
  edge [
    source 15
    target 347
  ]
  edge [
    source 15
    target 348
  ]
  edge [
    source 15
    target 349
  ]
  edge [
    source 15
    target 350
  ]
  edge [
    source 15
    target 351
  ]
  edge [
    source 15
    target 352
  ]
  edge [
    source 15
    target 353
  ]
  edge [
    source 15
    target 354
  ]
  edge [
    source 15
    target 355
  ]
  edge [
    source 15
    target 356
  ]
  edge [
    source 15
    target 357
  ]
  edge [
    source 15
    target 358
  ]
  edge [
    source 15
    target 359
  ]
  edge [
    source 15
    target 360
  ]
  edge [
    source 15
    target 361
  ]
  edge [
    source 15
    target 362
  ]
  edge [
    source 15
    target 363
  ]
  edge [
    source 15
    target 364
  ]
  edge [
    source 15
    target 365
  ]
  edge [
    source 15
    target 366
  ]
  edge [
    source 15
    target 367
  ]
  edge [
    source 15
    target 368
  ]
  edge [
    source 15
    target 369
  ]
  edge [
    source 15
    target 370
  ]
  edge [
    source 15
    target 371
  ]
  edge [
    source 15
    target 372
  ]
  edge [
    source 15
    target 373
  ]
  edge [
    source 15
    target 374
  ]
  edge [
    source 15
    target 375
  ]
  edge [
    source 15
    target 376
  ]
  edge [
    source 15
    target 377
  ]
  edge [
    source 15
    target 378
  ]
  edge [
    source 15
    target 379
  ]
  edge [
    source 15
    target 380
  ]
  edge [
    source 15
    target 381
  ]
  edge [
    source 15
    target 382
  ]
  edge [
    source 15
    target 383
  ]
  edge [
    source 15
    target 384
  ]
  edge [
    source 15
    target 385
  ]
  edge [
    source 15
    target 386
  ]
  edge [
    source 15
    target 387
  ]
  edge [
    source 15
    target 388
  ]
  edge [
    source 15
    target 389
  ]
  edge [
    source 15
    target 390
  ]
  edge [
    source 15
    target 391
  ]
  edge [
    source 15
    target 392
  ]
  edge [
    source 15
    target 393
  ]
  edge [
    source 15
    target 394
  ]
  edge [
    source 15
    target 395
  ]
  edge [
    source 15
    target 396
  ]
  edge [
    source 15
    target 397
  ]
  edge [
    source 15
    target 398
  ]
  edge [
    source 15
    target 399
  ]
  edge [
    source 15
    target 400
  ]
  edge [
    source 15
    target 401
  ]
  edge [
    source 15
    target 402
  ]
  edge [
    source 15
    target 403
  ]
  edge [
    source 15
    target 404
  ]
  edge [
    source 15
    target 405
  ]
  edge [
    source 15
    target 406
  ]
  edge [
    source 15
    target 407
  ]
  edge [
    source 15
    target 408
  ]
  edge [
    source 15
    target 409
  ]
  edge [
    source 15
    target 410
  ]
  edge [
    source 15
    target 411
  ]
  edge [
    source 15
    target 412
  ]
  edge [
    source 15
    target 413
  ]
  edge [
    source 15
    target 414
  ]
  edge [
    source 15
    target 415
  ]
  edge [
    source 15
    target 416
  ]
  edge [
    source 15
    target 417
  ]
  edge [
    source 15
    target 418
  ]
  edge [
    source 15
    target 419
  ]
  edge [
    source 15
    target 420
  ]
  edge [
    source 15
    target 421
  ]
  edge [
    source 15
    target 422
  ]
  edge [
    source 15
    target 423
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 424
  ]
  edge [
    source 17
    target 425
  ]
  edge [
    source 17
    target 426
  ]
  edge [
    source 17
    target 427
  ]
  edge [
    source 17
    target 428
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 429
  ]
  edge [
    source 21
    target 430
  ]
  edge [
    source 21
    target 431
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 432
  ]
  edge [
    source 22
    target 433
  ]
  edge [
    source 22
    target 434
  ]
  edge [
    source 22
    target 435
  ]
  edge [
    source 22
    target 436
  ]
  edge [
    source 22
    target 437
  ]
  edge [
    source 22
    target 438
  ]
  edge [
    source 22
    target 439
  ]
  edge [
    source 22
    target 440
  ]
  edge [
    source 22
    target 441
  ]
  edge [
    source 22
    target 442
  ]
  edge [
    source 22
    target 443
  ]
  edge [
    source 22
    target 444
  ]
  edge [
    source 22
    target 445
  ]
  edge [
    source 22
    target 446
  ]
  edge [
    source 22
    target 447
  ]
  edge [
    source 22
    target 448
  ]
  edge [
    source 22
    target 24
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 449
  ]
  edge [
    source 24
    target 450
  ]
  edge [
    source 24
    target 451
  ]
  edge [
    source 24
    target 452
  ]
  edge [
    source 24
    target 54
  ]
  edge [
    source 24
    target 453
  ]
  edge [
    source 24
    target 454
  ]
  edge [
    source 24
    target 455
  ]
  edge [
    source 24
    target 456
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 457
  ]
  edge [
    source 26
    target 458
  ]
  edge [
    source 26
    target 459
  ]
  edge [
    source 26
    target 460
  ]
  edge [
    source 26
    target 461
  ]
  edge [
    source 26
    target 462
  ]
  edge [
    source 26
    target 463
  ]
  edge [
    source 26
    target 464
  ]
  edge [
    source 26
    target 465
  ]
  edge [
    source 26
    target 466
  ]
  edge [
    source 26
    target 467
  ]
  edge [
    source 26
    target 468
  ]
  edge [
    source 26
    target 469
  ]
  edge [
    source 26
    target 470
  ]
  edge [
    source 26
    target 471
  ]
  edge [
    source 26
    target 472
  ]
  edge [
    source 26
    target 473
  ]
  edge [
    source 26
    target 474
  ]
  edge [
    source 26
    target 475
  ]
  edge [
    source 26
    target 476
  ]
  edge [
    source 26
    target 477
  ]
  edge [
    source 26
    target 478
  ]
  edge [
    source 26
    target 479
  ]
  edge [
    source 26
    target 480
  ]
  edge [
    source 26
    target 481
  ]
  edge [
    source 26
    target 482
  ]
  edge [
    source 26
    target 483
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 43
  ]
  edge [
    source 27
    target 44
  ]
  edge [
    source 27
    target 484
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 333
  ]
  edge [
    source 28
    target 401
  ]
  edge [
    source 28
    target 252
  ]
  edge [
    source 28
    target 389
  ]
  edge [
    source 28
    target 175
  ]
  edge [
    source 28
    target 485
  ]
  edge [
    source 28
    target 486
  ]
  edge [
    source 28
    target 362
  ]
  edge [
    source 28
    target 487
  ]
  edge [
    source 28
    target 299
  ]
  edge [
    source 28
    target 488
  ]
  edge [
    source 28
    target 489
  ]
  edge [
    source 28
    target 372
  ]
  edge [
    source 28
    target 226
  ]
  edge [
    source 28
    target 490
  ]
  edge [
    source 28
    target 170
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 491
  ]
  edge [
    source 29
    target 492
  ]
  edge [
    source 29
    target 493
  ]
  edge [
    source 29
    target 494
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 457
  ]
  edge [
    source 31
    target 495
  ]
  edge [
    source 31
    target 496
  ]
  edge [
    source 31
    target 497
  ]
  edge [
    source 31
    target 498
  ]
  edge [
    source 31
    target 499
  ]
  edge [
    source 31
    target 500
  ]
  edge [
    source 31
    target 501
  ]
  edge [
    source 31
    target 502
  ]
  edge [
    source 31
    target 503
  ]
  edge [
    source 31
    target 504
  ]
  edge [
    source 31
    target 505
  ]
  edge [
    source 31
    target 506
  ]
  edge [
    source 31
    target 507
  ]
  edge [
    source 31
    target 508
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 500
  ]
  edge [
    source 32
    target 509
  ]
  edge [
    source 32
    target 510
  ]
  edge [
    source 32
    target 511
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 512
  ]
  edge [
    source 33
    target 513
  ]
  edge [
    source 34
    target 514
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 515
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 516
  ]
  edge [
    source 38
    target 94
  ]
  edge [
    source 38
    target 517
  ]
  edge [
    source 38
    target 518
  ]
  edge [
    source 38
    target 519
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 520
  ]
  edge [
    source 40
    target 521
  ]
  edge [
    source 40
    target 522
  ]
  edge [
    source 40
    target 523
  ]
  edge [
    source 40
    target 524
  ]
  edge [
    source 40
    target 525
  ]
  edge [
    source 40
    target 526
  ]
  edge [
    source 40
    target 527
  ]
  edge [
    source 40
    target 528
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 529
  ]
  edge [
    source 42
    target 530
  ]
  edge [
    source 42
    target 531
  ]
  edge [
    source 42
    target 344
  ]
  edge [
    source 42
    target 532
  ]
  edge [
    source 42
    target 533
  ]
  edge [
    source 42
    target 534
  ]
  edge [
    source 42
    target 535
  ]
  edge [
    source 42
    target 536
  ]
  edge [
    source 42
    target 537
  ]
  edge [
    source 42
    target 538
  ]
  edge [
    source 42
    target 539
  ]
  edge [
    source 42
    target 540
  ]
  edge [
    source 42
    target 541
  ]
  edge [
    source 42
    target 542
  ]
  edge [
    source 42
    target 543
  ]
  edge [
    source 42
    target 544
  ]
  edge [
    source 42
    target 545
  ]
  edge [
    source 42
    target 546
  ]
  edge [
    source 42
    target 547
  ]
  edge [
    source 42
    target 548
  ]
  edge [
    source 42
    target 549
  ]
  edge [
    source 42
    target 550
  ]
  edge [
    source 42
    target 551
  ]
  edge [
    source 42
    target 552
  ]
  edge [
    source 42
    target 553
  ]
  edge [
    source 42
    target 554
  ]
  edge [
    source 42
    target 555
  ]
  edge [
    source 42
    target 556
  ]
  edge [
    source 42
    target 557
  ]
  edge [
    source 42
    target 558
  ]
  edge [
    source 42
    target 559
  ]
  edge [
    source 42
    target 560
  ]
  edge [
    source 42
    target 561
  ]
  edge [
    source 42
    target 562
  ]
  edge [
    source 42
    target 563
  ]
  edge [
    source 42
    target 564
  ]
  edge [
    source 42
    target 565
  ]
  edge [
    source 42
    target 566
  ]
  edge [
    source 42
    target 567
  ]
  edge [
    source 42
    target 568
  ]
  edge [
    source 42
    target 569
  ]
  edge [
    source 42
    target 570
  ]
  edge [
    source 42
    target 571
  ]
  edge [
    source 42
    target 572
  ]
  edge [
    source 42
    target 573
  ]
  edge [
    source 42
    target 574
  ]
  edge [
    source 42
    target 575
  ]
  edge [
    source 42
    target 576
  ]
  edge [
    source 42
    target 577
  ]
  edge [
    source 42
    target 578
  ]
  edge [
    source 42
    target 579
  ]
  edge [
    source 42
    target 580
  ]
  edge [
    source 42
    target 581
  ]
  edge [
    source 42
    target 582
  ]
  edge [
    source 42
    target 583
  ]
  edge [
    source 42
    target 584
  ]
  edge [
    source 42
    target 585
  ]
  edge [
    source 42
    target 586
  ]
  edge [
    source 42
    target 587
  ]
  edge [
    source 42
    target 588
  ]
  edge [
    source 42
    target 160
  ]
  edge [
    source 42
    target 589
  ]
  edge [
    source 42
    target 590
  ]
  edge [
    source 42
    target 591
  ]
  edge [
    source 42
    target 592
  ]
  edge [
    source 42
    target 593
  ]
  edge [
    source 42
    target 594
  ]
  edge [
    source 42
    target 595
  ]
  edge [
    source 42
    target 596
  ]
  edge [
    source 42
    target 597
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 598
  ]
  edge [
    source 44
    target 599
  ]
  edge [
    source 44
    target 600
  ]
  edge [
    source 44
    target 601
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 602
  ]
  edge [
    source 45
    target 603
  ]
  edge [
    source 45
    target 604
  ]
  edge [
    source 45
    target 605
  ]
  edge [
    source 46
    target 606
  ]
  edge [
    source 46
    target 607
  ]
  edge [
    source 46
    target 608
  ]
  edge [
    source 46
    target 609
  ]
  edge [
    source 46
    target 610
  ]
  edge [
    source 46
    target 611
  ]
  edge [
    source 46
    target 612
  ]
  edge [
    source 46
    target 613
  ]
  edge [
    source 46
    target 614
  ]
  edge [
    source 46
    target 615
  ]
  edge [
    source 46
    target 616
  ]
  edge [
    source 46
    target 617
  ]
  edge [
    source 46
    target 618
  ]
  edge [
    source 46
    target 619
  ]
  edge [
    source 46
    target 620
  ]
  edge [
    source 46
    target 621
  ]
  edge [
    source 46
    target 622
  ]
  edge [
    source 46
    target 623
  ]
  edge [
    source 46
    target 624
  ]
  edge [
    source 46
    target 625
  ]
  edge [
    source 46
    target 626
  ]
  edge [
    source 46
    target 627
  ]
  edge [
    source 46
    target 628
  ]
  edge [
    source 46
    target 629
  ]
  edge [
    source 46
    target 630
  ]
  edge [
    source 46
    target 631
  ]
  edge [
    source 46
    target 632
  ]
  edge [
    source 46
    target 633
  ]
  edge [
    source 46
    target 634
  ]
  edge [
    source 46
    target 635
  ]
  edge [
    source 46
    target 636
  ]
  edge [
    source 46
    target 637
  ]
  edge [
    source 46
    target 638
  ]
  edge [
    source 46
    target 639
  ]
  edge [
    source 46
    target 640
  ]
  edge [
    source 46
    target 641
  ]
  edge [
    source 46
    target 642
  ]
  edge [
    source 46
    target 643
  ]
  edge [
    source 46
    target 644
  ]
  edge [
    source 46
    target 645
  ]
]
