graph [
  maxDegree 3
  minDegree 1
  meanDegree 1.4
  density 0.15555555555555556
  graphCliqueNumber 3
  node [
    id 0
    label "giuseppe"
    origin "text"
  ]
  node [
    id 1
    label "abbagnale"
    origin "text"
  ]
  node [
    id 2
    label "Giuseppe"
  ]
  node [
    id 3
    label "Abbagnale"
  ]
  node [
    id 4
    label "di"
  ]
  node [
    id 5
    label "Capua"
  ]
  node [
    id 6
    label "los"
  ]
  node [
    id 7
    label "Angeles"
  ]
  node [
    id 8
    label "mistrzostwo"
  ]
  node [
    id 9
    label "&#347;wiat"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 8
    target 9
  ]
]
