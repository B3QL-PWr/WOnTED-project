graph [
  maxDegree 15
  minDegree 1
  meanDegree 1.9259259259259258
  density 0.07407407407407407
  graphCliqueNumber 3
  node [
    id 0
    label "niedziela"
    origin "text"
  ]
  node [
    id 1
    label "wielkanocny"
    origin "text"
  ]
  node [
    id 2
    label "Dzie&#324;_Papieski"
  ]
  node [
    id 3
    label "Wielkanoc"
  ]
  node [
    id 4
    label "dzie&#324;_&#347;wi&#281;ty"
  ]
  node [
    id 5
    label "weekend"
  ]
  node [
    id 6
    label "Niedziela_Palmowa"
  ]
  node [
    id 7
    label "Zes&#322;anie_Ducha_&#346;wi&#281;tego"
  ]
  node [
    id 8
    label "dzie&#324;_&#347;wi&#261;teczny"
  ]
  node [
    id 9
    label "bia&#322;a_niedziela"
  ]
  node [
    id 10
    label "niedziela_przewodnia"
  ]
  node [
    id 11
    label "tydzie&#324;"
  ]
  node [
    id 12
    label "szpekucha"
  ]
  node [
    id 13
    label "&#347;wi&#261;teczny"
  ]
  node [
    id 14
    label "Turki"
  ]
  node [
    id 15
    label "mazurek"
  ]
  node [
    id 16
    label "wielki"
  ]
  node [
    id 17
    label "&#347;wi&#281;to"
  ]
  node [
    id 18
    label "i"
  ]
  node [
    id 19
    label "zmartwychwsta&#263;"
  ]
  node [
    id 20
    label "pa&#324;skie"
  ]
  node [
    id 21
    label "triduum"
  ]
  node [
    id 22
    label "paschalny"
  ]
  node [
    id 23
    label "czwartek"
  ]
  node [
    id 24
    label "gr&#243;b"
  ]
  node [
    id 25
    label "Chrystus"
  ]
  node [
    id 26
    label "zmartwychwsta&#322;y"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 16
    target 23
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 24
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 25
    target 26
  ]
]
