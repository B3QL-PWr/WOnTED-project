graph [
  maxDegree 32
  minDegree 1
  meanDegree 2.0952380952380953
  density 0.03379416282642089
  graphCliqueNumber 4
  node [
    id 0
    label "sanktuarium"
    origin "text"
  ]
  node [
    id 1
    label "wambierzyckiej"
    origin "text"
  ]
  node [
    id 2
    label "kr&#243;lowa"
    origin "text"
  ]
  node [
    id 3
    label "rodzina"
    origin "text"
  ]
  node [
    id 4
    label "miejsce"
  ]
  node [
    id 5
    label "Liche&#324;_Stary"
  ]
  node [
    id 6
    label "&#321;agiewniki"
  ]
  node [
    id 7
    label "Jasna_G&#243;ra"
  ]
  node [
    id 8
    label "Izabela_I_Kastylijska"
  ]
  node [
    id 9
    label "pszczo&#322;a"
  ]
  node [
    id 10
    label "Bona"
  ]
  node [
    id 11
    label "Wiktoria"
  ]
  node [
    id 12
    label "kand"
  ]
  node [
    id 13
    label "strzelec"
  ]
  node [
    id 14
    label "El&#380;bieta_I"
  ]
  node [
    id 15
    label "kr&#243;lowa_matka"
  ]
  node [
    id 16
    label "mr&#243;wka"
  ]
  node [
    id 17
    label "krewni"
  ]
  node [
    id 18
    label "Firlejowie"
  ]
  node [
    id 19
    label "Ossoli&#324;scy"
  ]
  node [
    id 20
    label "grupa"
  ]
  node [
    id 21
    label "rodze&#324;stwo"
  ]
  node [
    id 22
    label "&#346;wi&#281;ta_Rodzina"
  ]
  node [
    id 23
    label "rz&#261;d"
  ]
  node [
    id 24
    label "Radziwi&#322;&#322;owie"
  ]
  node [
    id 25
    label "przyjaciel_domu"
  ]
  node [
    id 26
    label "Ostrogscy"
  ]
  node [
    id 27
    label "theater"
  ]
  node [
    id 28
    label "dom_rodzinny"
  ]
  node [
    id 29
    label "potomstwo"
  ]
  node [
    id 30
    label "Soplicowie"
  ]
  node [
    id 31
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 32
    label "Czartoryscy"
  ]
  node [
    id 33
    label "family"
  ]
  node [
    id 34
    label "kin"
  ]
  node [
    id 35
    label "bliscy"
  ]
  node [
    id 36
    label "powinowaci"
  ]
  node [
    id 37
    label "Sapiehowie"
  ]
  node [
    id 38
    label "ordynacja"
  ]
  node [
    id 39
    label "jednostka_systematyczna"
  ]
  node [
    id 40
    label "zbi&#243;r"
  ]
  node [
    id 41
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 42
    label "Kossakowie"
  ]
  node [
    id 43
    label "rodzice"
  ]
  node [
    id 44
    label "dom"
  ]
  node [
    id 45
    label "Wambierzyckiej"
  ]
  node [
    id 46
    label "kr&#243;lowy"
  ]
  node [
    id 47
    label "Pius"
  ]
  node [
    id 48
    label "XI"
  ]
  node [
    id 49
    label "matka"
  ]
  node [
    id 50
    label "boski"
  ]
  node [
    id 51
    label "ludwik"
  ]
  node [
    id 52
    label "von"
  ]
  node [
    id 53
    label "Panwitz"
  ]
  node [
    id 54
    label "wojna"
  ]
  node [
    id 55
    label "trzydziestoletni"
  ]
  node [
    id 56
    label "Franciszka"
  ]
  node [
    id 57
    label "Antoni"
  ]
  node [
    id 58
    label "Goetzen"
  ]
  node [
    id 59
    label "bo&#380;y"
  ]
  node [
    id 60
    label "Jan"
  ]
  node [
    id 61
    label "pawe&#322;"
  ]
  node [
    id 62
    label "ii"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 59
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 53
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 56
  ]
  edge [
    source 52
    target 57
  ]
  edge [
    source 52
    target 58
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 58
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 62
  ]
  edge [
    source 61
    target 62
  ]
]
