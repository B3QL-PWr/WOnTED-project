graph [
  maxDegree 16
  minDegree 1
  meanDegree 2.7857142857142856
  density 0.10317460317460317
  graphCliqueNumber 5
  node [
    id 0
    label "batalion"
    origin "text"
  ]
  node [
    id 1
    label "brze&#380;any"
    origin "text"
  ]
  node [
    id 2
    label "ptak_wodno-b&#322;otny"
  ]
  node [
    id 3
    label "kompania"
  ]
  node [
    id 4
    label "formacja"
  ]
  node [
    id 5
    label "s&#322;onki"
  ]
  node [
    id 6
    label "pu&#322;k"
  ]
  node [
    id 7
    label "dywizjon"
  ]
  node [
    id 8
    label "ptak_w&#281;drowny"
  ]
  node [
    id 9
    label "pododdzia&#322;"
  ]
  node [
    id 10
    label "brygada"
  ]
  node [
    id 11
    label "on"
  ]
  node [
    id 12
    label "Brze&#380;any"
  ]
  node [
    id 13
    label "Brze&#380;a&#324;ski"
  ]
  node [
    id 14
    label "obrona"
  ]
  node [
    id 15
    label "narodowy"
  ]
  node [
    id 16
    label "wojsko"
  ]
  node [
    id 17
    label "polskie"
  ]
  node [
    id 18
    label "ii"
  ]
  node [
    id 19
    label "RP"
  ]
  node [
    id 20
    label "lwowski"
  ]
  node [
    id 21
    label "Brze&#380;a&#324;skiego"
  ]
  node [
    id 22
    label "51"
  ]
  node [
    id 23
    label "piechota"
  ]
  node [
    id 24
    label "strzelec"
  ]
  node [
    id 25
    label "kresowy"
  ]
  node [
    id 26
    label "tarnopolski"
  ]
  node [
    id 27
    label "p&#243;&#322;brygada"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 6
    target 22
  ]
  edge [
    source 6
    target 23
  ]
  edge [
    source 6
    target 24
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 10
    target 20
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 20
  ]
  edge [
    source 11
    target 21
  ]
  edge [
    source 11
    target 26
  ]
  edge [
    source 11
    target 27
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 24
  ]
  edge [
    source 22
    target 25
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 25
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 26
    target 27
  ]
]
