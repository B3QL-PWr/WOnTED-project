graph [
  maxDegree 38
  minDegree 1
  meanDegree 1.9375
  density 0.030753968253968252
  graphCliqueNumber 3
  node [
    id 0
    label "prosi&#263;"
    origin "text"
  ]
  node [
    id 1
    label "bardzo"
    origin "text"
  ]
  node [
    id 2
    label "pan"
    origin "text"
  ]
  node [
    id 3
    label "marsza&#322;ek"
    origin "text"
  ]
  node [
    id 4
    label "trwa&#263;"
  ]
  node [
    id 5
    label "zezwala&#263;"
  ]
  node [
    id 6
    label "ask"
  ]
  node [
    id 7
    label "invite"
  ]
  node [
    id 8
    label "zach&#281;ca&#263;"
  ]
  node [
    id 9
    label "preach"
  ]
  node [
    id 10
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 11
    label "pies"
  ]
  node [
    id 12
    label "ujmowa&#263;_si&#281;"
  ]
  node [
    id 13
    label "poleca&#263;"
  ]
  node [
    id 14
    label "zaprasza&#263;"
  ]
  node [
    id 15
    label "suffice"
  ]
  node [
    id 16
    label "dziwi&#263;_si&#281;"
  ]
  node [
    id 17
    label "w_chuj"
  ]
  node [
    id 18
    label "cz&#322;owiek"
  ]
  node [
    id 19
    label "profesor"
  ]
  node [
    id 20
    label "kszta&#322;ciciel"
  ]
  node [
    id 21
    label "jegomo&#347;&#263;"
  ]
  node [
    id 22
    label "zwrot"
  ]
  node [
    id 23
    label "pracodawca"
  ]
  node [
    id 24
    label "rz&#261;dzenie"
  ]
  node [
    id 25
    label "m&#261;&#380;"
  ]
  node [
    id 26
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 27
    label "ch&#322;opina"
  ]
  node [
    id 28
    label "bratek"
  ]
  node [
    id 29
    label "opiekun"
  ]
  node [
    id 30
    label "doros&#322;y"
  ]
  node [
    id 31
    label "preceptor"
  ]
  node [
    id 32
    label "Midas"
  ]
  node [
    id 33
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 34
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 35
    label "murza"
  ]
  node [
    id 36
    label "ojciec"
  ]
  node [
    id 37
    label "androlog"
  ]
  node [
    id 38
    label "pupil"
  ]
  node [
    id 39
    label "efendi"
  ]
  node [
    id 40
    label "nabab"
  ]
  node [
    id 41
    label "w&#322;odarz"
  ]
  node [
    id 42
    label "szkolnik"
  ]
  node [
    id 43
    label "pedagog"
  ]
  node [
    id 44
    label "popularyzator"
  ]
  node [
    id 45
    label "andropauza"
  ]
  node [
    id 46
    label "gra_w_karty"
  ]
  node [
    id 47
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 48
    label "Mieszko_I"
  ]
  node [
    id 49
    label "bogaty"
  ]
  node [
    id 50
    label "samiec"
  ]
  node [
    id 51
    label "przyw&#243;dca"
  ]
  node [
    id 52
    label "pa&#324;stwo"
  ]
  node [
    id 53
    label "belfer"
  ]
  node [
    id 54
    label "stopie&#324;_wojskowy"
  ]
  node [
    id 55
    label "dostojnik"
  ]
  node [
    id 56
    label "oficer"
  ]
  node [
    id 57
    label "parlamentarzysta"
  ]
  node [
    id 58
    label "Pi&#322;sudski"
  ]
  node [
    id 59
    label "ludwik"
  ]
  node [
    id 60
    label "Dorn"
  ]
  node [
    id 61
    label "Jerzy"
  ]
  node [
    id 62
    label "Feliksa"
  ]
  node [
    id 63
    label "Fedorowicz"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 63
  ]
  edge [
    source 62
    target 63
  ]
]
