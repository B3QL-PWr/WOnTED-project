graph [
  maxDegree 5
  minDegree 1
  meanDegree 1.7777777777777777
  density 0.2222222222222222
  graphCliqueNumber 3
  node [
    id 0
    label "morderstwo"
    origin "text"
  ]
  node [
    id 1
    label "arlis"
    origin "text"
  ]
  node [
    id 2
    label "perry"
    origin "text"
  ]
  node [
    id 3
    label "zabicie"
  ]
  node [
    id 4
    label "przest&#281;pstwo"
  ]
  node [
    id 5
    label "Arlis"
  ]
  node [
    id 6
    label "Perry"
  ]
  node [
    id 7
    label "hej"
  ]
  node [
    id 8
    label "Mirka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 7
    target 8
  ]
]
