graph [
  maxDegree 36
  minDegree 1
  meanDegree 2.3777777777777778
  density 0.006623336428350356
  graphCliqueNumber 6
  node [
    id 0
    label "odno&#347;nie"
    origin "text"
  ]
  node [
    id 1
    label "by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "mie&#263;"
    origin "text"
  ]
  node [
    id 3
    label "czynienie"
    origin "text"
  ]
  node [
    id 4
    label "rozbudowa"
    origin "text"
  ]
  node [
    id 5
    label "dzia&#322;alno&#347;&#263;"
    origin "text"
  ]
  node [
    id 6
    label "wiedzie&#263;"
    origin "text"
  ]
  node [
    id 7
    label "developersk&#261;"
    origin "text"
  ]
  node [
    id 8
    label "terenia"
    origin "text"
  ]
  node [
    id 9
    label "gmin"
    origin "text"
  ]
  node [
    id 10
    label "je&#347;li"
    origin "text"
  ]
  node [
    id 11
    label "chodzi&#263;"
    origin "text"
  ]
  node [
    id 12
    label "sp&#243;&#322;ka"
    origin "text"
  ]
  node [
    id 13
    label "uzdrowiskowy"
    origin "text"
  ]
  node [
    id 14
    label "sprywatyzowa&#263;"
    origin "text"
  ]
  node [
    id 15
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 16
    label "nic"
    origin "text"
  ]
  node [
    id 17
    label "wsp&#243;lny"
    origin "text"
  ]
  node [
    id 18
    label "kwestia"
    origin "text"
  ]
  node [
    id 19
    label "dotyczy&#263;"
    origin "text"
  ]
  node [
    id 20
    label "zmiana"
    origin "text"
  ]
  node [
    id 21
    label "funkcja"
    origin "text"
  ]
  node [
    id 22
    label "jaka"
    origin "text"
  ]
  node [
    id 23
    label "pe&#322;nia"
    origin "text"
  ]
  node [
    id 24
    label "je&#380;eli"
    origin "text"
  ]
  node [
    id 25
    label "si&#281;"
    origin "text"
  ]
  node [
    id 26
    label "dzia&#322;o"
    origin "text"
  ]
  node [
    id 27
    label "uzdrowisko"
    origin "text"
  ]
  node [
    id 28
    label "pierwsza"
    origin "text"
  ]
  node [
    id 29
    label "w&#322;a&#347;nie"
    origin "text"
  ]
  node [
    id 30
    label "ustalenie"
    origin "text"
  ]
  node [
    id 31
    label "strefa"
    origin "text"
  ]
  node [
    id 32
    label "druga"
    origin "text"
  ]
  node [
    id 33
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 34
    label "tychy"
    origin "text"
  ]
  node [
    id 35
    label "wykonywa&#263;"
    origin "text"
  ]
  node [
    id 36
    label "prowadzi&#263;"
    origin "text"
  ]
  node [
    id 37
    label "budowa&#263;"
    origin "text"
  ]
  node [
    id 38
    label "wszystko"
    origin "text"
  ]
  node [
    id 39
    label "zapisany"
    origin "text"
  ]
  node [
    id 40
    label "ustawa"
    origin "text"
  ]
  node [
    id 41
    label "konsekwencja"
    origin "text"
  ]
  node [
    id 42
    label "przepis"
    origin "text"
  ]
  node [
    id 43
    label "decyzja"
    origin "text"
  ]
  node [
    id 44
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 45
    label "podejmowa&#263;"
    origin "text"
  ]
  node [
    id 46
    label "rada"
    origin "text"
  ]
  node [
    id 47
    label "przygotowywa&#263;"
    origin "text"
  ]
  node [
    id 48
    label "operat"
    origin "text"
  ]
  node [
    id 49
    label "zgodnie"
    origin "text"
  ]
  node [
    id 50
    label "dzi&#281;kowa&#263;"
    origin "text"
  ]
  node [
    id 51
    label "bardzo"
    origin "text"
  ]
  node [
    id 52
    label "oklask"
    origin "text"
  ]
  node [
    id 53
    label "si&#281;ga&#263;"
  ]
  node [
    id 54
    label "trwa&#263;"
  ]
  node [
    id 55
    label "obecno&#347;&#263;"
  ]
  node [
    id 56
    label "stan"
  ]
  node [
    id 57
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 58
    label "stand"
  ]
  node [
    id 59
    label "mie&#263;_miejsce"
  ]
  node [
    id 60
    label "uczestniczy&#263;"
  ]
  node [
    id 61
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 62
    label "equal"
  ]
  node [
    id 63
    label "czu&#263;"
  ]
  node [
    id 64
    label "need"
  ]
  node [
    id 65
    label "hide"
  ]
  node [
    id 66
    label "support"
  ]
  node [
    id 67
    label "robienie"
  ]
  node [
    id 68
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 69
    label "porobienie"
  ]
  node [
    id 70
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 71
    label "act"
  ]
  node [
    id 72
    label "campaign"
  ]
  node [
    id 73
    label "m&#281;czenie_si&#281;"
  ]
  node [
    id 74
    label "budowa"
  ]
  node [
    id 75
    label "rozw&#243;j"
  ]
  node [
    id 76
    label "czynno&#347;&#263;"
  ]
  node [
    id 77
    label "development"
  ]
  node [
    id 78
    label "dzia&#322;anie"
  ]
  node [
    id 79
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 80
    label "absolutorium"
  ]
  node [
    id 81
    label "activity"
  ]
  node [
    id 82
    label "cognizance"
  ]
  node [
    id 83
    label "stan_trzeci"
  ]
  node [
    id 84
    label "gminno&#347;&#263;"
  ]
  node [
    id 85
    label "proceed"
  ]
  node [
    id 86
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 87
    label "bangla&#263;"
  ]
  node [
    id 88
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 89
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 90
    label "run"
  ]
  node [
    id 91
    label "tryb"
  ]
  node [
    id 92
    label "p&#322;ywa&#263;"
  ]
  node [
    id 93
    label "continue"
  ]
  node [
    id 94
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 95
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 96
    label "przebiega&#263;"
  ]
  node [
    id 97
    label "wk&#322;ada&#263;"
  ]
  node [
    id 98
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 99
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 100
    label "para"
  ]
  node [
    id 101
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 102
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 103
    label "krok"
  ]
  node [
    id 104
    label "str&#243;j"
  ]
  node [
    id 105
    label "bywa&#263;"
  ]
  node [
    id 106
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 107
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 108
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 109
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 110
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 111
    label "dziama&#263;"
  ]
  node [
    id 112
    label "stara&#263;_si&#281;"
  ]
  node [
    id 113
    label "carry"
  ]
  node [
    id 114
    label "podmiot_gospodarczy"
  ]
  node [
    id 115
    label "organizacja"
  ]
  node [
    id 116
    label "zesp&#243;&#322;"
  ]
  node [
    id 117
    label "wsp&#243;lnictwo"
  ]
  node [
    id 118
    label "privatize"
  ]
  node [
    id 119
    label "zreorganizowa&#263;"
  ]
  node [
    id 120
    label "czyj&#347;"
  ]
  node [
    id 121
    label "m&#261;&#380;"
  ]
  node [
    id 122
    label "miernota"
  ]
  node [
    id 123
    label "g&#243;wno"
  ]
  node [
    id 124
    label "love"
  ]
  node [
    id 125
    label "ilo&#347;&#263;"
  ]
  node [
    id 126
    label "brak"
  ]
  node [
    id 127
    label "ciura"
  ]
  node [
    id 128
    label "spolny"
  ]
  node [
    id 129
    label "jeden"
  ]
  node [
    id 130
    label "sp&#243;lny"
  ]
  node [
    id 131
    label "wsp&#243;lnie"
  ]
  node [
    id 132
    label "uwsp&#243;lnianie"
  ]
  node [
    id 133
    label "uwsp&#243;lnienie"
  ]
  node [
    id 134
    label "sprawa"
  ]
  node [
    id 135
    label "problemat"
  ]
  node [
    id 136
    label "wypowied&#378;"
  ]
  node [
    id 137
    label "dialog"
  ]
  node [
    id 138
    label "problematyka"
  ]
  node [
    id 139
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 140
    label "subject"
  ]
  node [
    id 141
    label "bargain"
  ]
  node [
    id 142
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 143
    label "tycze&#263;"
  ]
  node [
    id 144
    label "anatomopatolog"
  ]
  node [
    id 145
    label "rewizja"
  ]
  node [
    id 146
    label "oznaka"
  ]
  node [
    id 147
    label "czas"
  ]
  node [
    id 148
    label "ferment"
  ]
  node [
    id 149
    label "komplet"
  ]
  node [
    id 150
    label "tura"
  ]
  node [
    id 151
    label "amendment"
  ]
  node [
    id 152
    label "zmianka"
  ]
  node [
    id 153
    label "odmienianie"
  ]
  node [
    id 154
    label "passage"
  ]
  node [
    id 155
    label "zjawisko"
  ]
  node [
    id 156
    label "change"
  ]
  node [
    id 157
    label "praca"
  ]
  node [
    id 158
    label "infimum"
  ]
  node [
    id 159
    label "znaczenie"
  ]
  node [
    id 160
    label "awansowanie"
  ]
  node [
    id 161
    label "zastosowanie"
  ]
  node [
    id 162
    label "function"
  ]
  node [
    id 163
    label "funkcjonowanie"
  ]
  node [
    id 164
    label "cel"
  ]
  node [
    id 165
    label "supremum"
  ]
  node [
    id 166
    label "powierzanie"
  ]
  node [
    id 167
    label "rzut"
  ]
  node [
    id 168
    label "addytywno&#347;&#263;"
  ]
  node [
    id 169
    label "monotoniczno&#347;&#263;"
  ]
  node [
    id 170
    label "wakowa&#263;"
  ]
  node [
    id 171
    label "dziedzina"
  ]
  node [
    id 172
    label "postawi&#263;"
  ]
  node [
    id 173
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 174
    label "czyn"
  ]
  node [
    id 175
    label "przeciwdziedzina"
  ]
  node [
    id 176
    label "matematyka"
  ]
  node [
    id 177
    label "awansowa&#263;"
  ]
  node [
    id 178
    label "stawia&#263;"
  ]
  node [
    id 179
    label "jednostka"
  ]
  node [
    id 180
    label "kaftan"
  ]
  node [
    id 181
    label "zakres"
  ]
  node [
    id 182
    label "faza_Ksi&#281;&#380;yca"
  ]
  node [
    id 183
    label "szczyt"
  ]
  node [
    id 184
    label "bateria"
  ]
  node [
    id 185
    label "laweta"
  ]
  node [
    id 186
    label "bro&#324;_artyleryjska"
  ]
  node [
    id 187
    label "bro&#324;"
  ]
  node [
    id 188
    label "oporopowrotnik"
  ]
  node [
    id 189
    label "przedmuchiwacz"
  ]
  node [
    id 190
    label "artyleria"
  ]
  node [
    id 191
    label "waln&#261;&#263;"
  ]
  node [
    id 192
    label "bateria_artylerii"
  ]
  node [
    id 193
    label "cannon"
  ]
  node [
    id 194
    label "Busko-Zdr&#243;j"
  ]
  node [
    id 195
    label "miejscowo&#347;&#263;"
  ]
  node [
    id 196
    label "Rabka-Zdr&#243;j"
  ]
  node [
    id 197
    label "Sopot"
  ]
  node [
    id 198
    label "&#379;egiest&#243;w-Zdr&#243;j"
  ]
  node [
    id 199
    label "D&#261;bki"
  ]
  node [
    id 200
    label "Inowroc&#322;aw"
  ]
  node [
    id 201
    label "Solec-Zdr&#243;j"
  ]
  node [
    id 202
    label "Go&#322;dap"
  ]
  node [
    id 203
    label "Ciechocinek"
  ]
  node [
    id 204
    label "Aleksandr&#243;w_Kujawski"
  ]
  node [
    id 205
    label "Ko&#322;obrzeg"
  ]
  node [
    id 206
    label "Kamie&#324;_Pomorski"
  ]
  node [
    id 207
    label "Szczawnica"
  ]
  node [
    id 208
    label "kuracjusz"
  ]
  node [
    id 209
    label "Gocza&#322;kowice-Zdr&#243;j"
  ]
  node [
    id 210
    label "Ustro&#324;"
  ]
  node [
    id 211
    label "G&#322;ucho&#322;azy"
  ]
  node [
    id 212
    label "Ustka"
  ]
  node [
    id 213
    label "Wieniec-Zdr&#243;j"
  ]
  node [
    id 214
    label "Swoszowice"
  ]
  node [
    id 215
    label "August&#243;w"
  ]
  node [
    id 216
    label "&#346;winouj&#347;cie"
  ]
  node [
    id 217
    label "Trzci&#324;sko-Zdr&#243;j"
  ]
  node [
    id 218
    label "Konstancin-Jeziorna"
  ]
  node [
    id 219
    label "sanatorium"
  ]
  node [
    id 220
    label "godzina"
  ]
  node [
    id 221
    label "dok&#322;adnie"
  ]
  node [
    id 222
    label "zrobienie"
  ]
  node [
    id 223
    label "zdecydowanie"
  ]
  node [
    id 224
    label "spowodowanie"
  ]
  node [
    id 225
    label "appointment"
  ]
  node [
    id 226
    label "localization"
  ]
  node [
    id 227
    label "informacja"
  ]
  node [
    id 228
    label "umocnienie"
  ]
  node [
    id 229
    label "obszar"
  ]
  node [
    id 230
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 231
    label "obrona_strefowa"
  ]
  node [
    id 232
    label "free"
  ]
  node [
    id 233
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 234
    label "work"
  ]
  node [
    id 235
    label "robi&#263;"
  ]
  node [
    id 236
    label "muzyka"
  ]
  node [
    id 237
    label "rola"
  ]
  node [
    id 238
    label "create"
  ]
  node [
    id 239
    label "wytwarza&#263;"
  ]
  node [
    id 240
    label "control"
  ]
  node [
    id 241
    label "eksponowa&#263;"
  ]
  node [
    id 242
    label "kre&#347;li&#263;"
  ]
  node [
    id 243
    label "g&#243;rowa&#263;"
  ]
  node [
    id 244
    label "message"
  ]
  node [
    id 245
    label "partner"
  ]
  node [
    id 246
    label "string"
  ]
  node [
    id 247
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 248
    label "przesuwa&#263;"
  ]
  node [
    id 249
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 250
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 251
    label "powodowa&#263;"
  ]
  node [
    id 252
    label "kierowa&#263;"
  ]
  node [
    id 253
    label "manipulate"
  ]
  node [
    id 254
    label "&#380;y&#263;"
  ]
  node [
    id 255
    label "navigate"
  ]
  node [
    id 256
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 257
    label "ukierunkowywa&#263;"
  ]
  node [
    id 258
    label "linia_melodyczna"
  ]
  node [
    id 259
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 260
    label "prowadzenie"
  ]
  node [
    id 261
    label "tworzy&#263;"
  ]
  node [
    id 262
    label "przewy&#380;sza&#263;"
  ]
  node [
    id 263
    label "sterowa&#263;"
  ]
  node [
    id 264
    label "krzywa"
  ]
  node [
    id 265
    label "planowa&#263;"
  ]
  node [
    id 266
    label "dodawa&#263;_otuchy"
  ]
  node [
    id 267
    label "consist"
  ]
  node [
    id 268
    label "train"
  ]
  node [
    id 269
    label "raise"
  ]
  node [
    id 270
    label "stanowi&#263;"
  ]
  node [
    id 271
    label "lock"
  ]
  node [
    id 272
    label "absolut"
  ]
  node [
    id 273
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 274
    label "Karta_Nauczyciela"
  ]
  node [
    id 275
    label "marc&#243;wka"
  ]
  node [
    id 276
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 277
    label "akt"
  ]
  node [
    id 278
    label "przej&#347;&#263;"
  ]
  node [
    id 279
    label "charter"
  ]
  node [
    id 280
    label "przej&#347;cie"
  ]
  node [
    id 281
    label "zwi&#261;zek_przyczynowy"
  ]
  node [
    id 282
    label "skrupia&#263;_si&#281;"
  ]
  node [
    id 283
    label "skrupianie_si&#281;"
  ]
  node [
    id 284
    label "odczuwanie"
  ]
  node [
    id 285
    label "skrupienie_si&#281;"
  ]
  node [
    id 286
    label "skrupi&#263;_si&#281;"
  ]
  node [
    id 287
    label "odczucie"
  ]
  node [
    id 288
    label "odczuwa&#263;"
  ]
  node [
    id 289
    label "odczu&#263;"
  ]
  node [
    id 290
    label "event"
  ]
  node [
    id 291
    label "rezultat"
  ]
  node [
    id 292
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 293
    label "koszula_Dejaniry"
  ]
  node [
    id 294
    label "przedawnienie_si&#281;"
  ]
  node [
    id 295
    label "recepta"
  ]
  node [
    id 296
    label "norma_prawna"
  ]
  node [
    id 297
    label "kodeks"
  ]
  node [
    id 298
    label "prawo"
  ]
  node [
    id 299
    label "regulation"
  ]
  node [
    id 300
    label "przedawni&#263;_si&#281;"
  ]
  node [
    id 301
    label "porada"
  ]
  node [
    id 302
    label "przedawnianie_si&#281;"
  ]
  node [
    id 303
    label "spos&#243;b"
  ]
  node [
    id 304
    label "przedawnia&#263;_si&#281;"
  ]
  node [
    id 305
    label "dokument"
  ]
  node [
    id 306
    label "resolution"
  ]
  node [
    id 307
    label "wytw&#243;r"
  ]
  node [
    id 308
    label "podj&#281;cie_decyzji"
  ]
  node [
    id 309
    label "management"
  ]
  node [
    id 310
    label "zmienia&#263;"
  ]
  node [
    id 311
    label "reagowa&#263;"
  ]
  node [
    id 312
    label "rise"
  ]
  node [
    id 313
    label "admit"
  ]
  node [
    id 314
    label "drive"
  ]
  node [
    id 315
    label "draw"
  ]
  node [
    id 316
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 317
    label "podnosi&#263;"
  ]
  node [
    id 318
    label "dyskusja"
  ]
  node [
    id 319
    label "grupa"
  ]
  node [
    id 320
    label "Rada_Unii_Europejskiej"
  ]
  node [
    id 321
    label "conference"
  ]
  node [
    id 322
    label "organ"
  ]
  node [
    id 323
    label "zgromadzenie"
  ]
  node [
    id 324
    label "wskaz&#243;wka"
  ]
  node [
    id 325
    label "konsylium"
  ]
  node [
    id 326
    label "Rada_Europy"
  ]
  node [
    id 327
    label "Rada_Europejska"
  ]
  node [
    id 328
    label "posiedzenie"
  ]
  node [
    id 329
    label "sposobi&#263;"
  ]
  node [
    id 330
    label "arrange"
  ]
  node [
    id 331
    label "pryczy&#263;"
  ]
  node [
    id 332
    label "szkoli&#263;"
  ]
  node [
    id 333
    label "usposabia&#263;"
  ]
  node [
    id 334
    label "opracowanie"
  ]
  node [
    id 335
    label "dokumentacja"
  ]
  node [
    id 336
    label "jednakowo"
  ]
  node [
    id 337
    label "spokojnie"
  ]
  node [
    id 338
    label "zgodny"
  ]
  node [
    id 339
    label "dobrze"
  ]
  node [
    id 340
    label "zbie&#380;nie"
  ]
  node [
    id 341
    label "odmawia&#263;"
  ]
  node [
    id 342
    label "wycofywa&#263;_si&#281;"
  ]
  node [
    id 343
    label "sk&#322;ada&#263;"
  ]
  node [
    id 344
    label "thank"
  ]
  node [
    id 345
    label "odwdzi&#281;cza&#263;_si&#281;"
  ]
  node [
    id 346
    label "wyra&#380;a&#263;"
  ]
  node [
    id 347
    label "etykieta"
  ]
  node [
    id 348
    label "w_chuj"
  ]
  node [
    id 349
    label "oklaski"
  ]
  node [
    id 350
    label "kla&#347;ni&#281;cie"
  ]
  node [
    id 351
    label "Jerzy"
  ]
  node [
    id 352
    label "Szmajdzi&#324;ski"
  ]
  node [
    id 353
    label "J&#243;zefa"
  ]
  node [
    id 354
    label "Piotr"
  ]
  node [
    id 355
    label "klima"
  ]
  node [
    id 356
    label "Leszek"
  ]
  node [
    id 357
    label "Cie&#347;lik"
  ]
  node [
    id 358
    label "platforma"
  ]
  node [
    id 359
    label "obywatelski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 35
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 26
  ]
  edge [
    source 8
    target 27
  ]
  edge [
    source 8
    target 33
  ]
  edge [
    source 8
    target 13
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 27
  ]
  edge [
    source 9
    target 46
  ]
  edge [
    source 9
    target 47
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 56
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 24
  ]
  edge [
    source 11
    target 85
  ]
  edge [
    source 11
    target 86
  ]
  edge [
    source 11
    target 87
  ]
  edge [
    source 11
    target 88
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 11
    target 92
  ]
  edge [
    source 11
    target 93
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 11
    target 59
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 11
    target 98
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 11
    target 100
  ]
  edge [
    source 11
    target 101
  ]
  edge [
    source 11
    target 102
  ]
  edge [
    source 11
    target 103
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 108
  ]
  edge [
    source 11
    target 109
  ]
  edge [
    source 11
    target 110
  ]
  edge [
    source 11
    target 111
  ]
  edge [
    source 11
    target 112
  ]
  edge [
    source 11
    target 113
  ]
  edge [
    source 11
    target 33
  ]
  edge [
    source 11
    target 49
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 114
  ]
  edge [
    source 12
    target 115
  ]
  edge [
    source 12
    target 116
  ]
  edge [
    source 12
    target 117
  ]
  edge [
    source 12
    target 42
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 48
  ]
  edge [
    source 13
    target 49
  ]
  edge [
    source 13
    target 40
  ]
  edge [
    source 13
    target 50
  ]
  edge [
    source 13
    target 18
  ]
  edge [
    source 13
    target 39
  ]
  edge [
    source 13
    target 34
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 118
  ]
  edge [
    source 14
    target 119
  ]
  edge [
    source 14
    target 29
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 120
  ]
  edge [
    source 15
    target 121
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 122
  ]
  edge [
    source 16
    target 123
  ]
  edge [
    source 16
    target 124
  ]
  edge [
    source 16
    target 125
  ]
  edge [
    source 16
    target 126
  ]
  edge [
    source 16
    target 127
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 128
  ]
  edge [
    source 17
    target 129
  ]
  edge [
    source 17
    target 130
  ]
  edge [
    source 17
    target 131
  ]
  edge [
    source 17
    target 132
  ]
  edge [
    source 17
    target 133
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 28
  ]
  edge [
    source 18
    target 29
  ]
  edge [
    source 18
    target 134
  ]
  edge [
    source 18
    target 135
  ]
  edge [
    source 18
    target 136
  ]
  edge [
    source 18
    target 137
  ]
  edge [
    source 18
    target 138
  ]
  edge [
    source 18
    target 139
  ]
  edge [
    source 18
    target 140
  ]
  edge [
    source 18
    target 34
  ]
  edge [
    source 18
    target 40
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 141
  ]
  edge [
    source 19
    target 142
  ]
  edge [
    source 19
    target 143
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 144
  ]
  edge [
    source 20
    target 145
  ]
  edge [
    source 20
    target 146
  ]
  edge [
    source 20
    target 147
  ]
  edge [
    source 20
    target 148
  ]
  edge [
    source 20
    target 149
  ]
  edge [
    source 20
    target 150
  ]
  edge [
    source 20
    target 151
  ]
  edge [
    source 20
    target 152
  ]
  edge [
    source 20
    target 153
  ]
  edge [
    source 20
    target 154
  ]
  edge [
    source 20
    target 155
  ]
  edge [
    source 20
    target 156
  ]
  edge [
    source 20
    target 157
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 158
  ]
  edge [
    source 21
    target 159
  ]
  edge [
    source 21
    target 160
  ]
  edge [
    source 21
    target 161
  ]
  edge [
    source 21
    target 162
  ]
  edge [
    source 21
    target 163
  ]
  edge [
    source 21
    target 164
  ]
  edge [
    source 21
    target 165
  ]
  edge [
    source 21
    target 166
  ]
  edge [
    source 21
    target 167
  ]
  edge [
    source 21
    target 168
  ]
  edge [
    source 21
    target 169
  ]
  edge [
    source 21
    target 170
  ]
  edge [
    source 21
    target 171
  ]
  edge [
    source 21
    target 172
  ]
  edge [
    source 21
    target 173
  ]
  edge [
    source 21
    target 174
  ]
  edge [
    source 21
    target 175
  ]
  edge [
    source 21
    target 176
  ]
  edge [
    source 21
    target 177
  ]
  edge [
    source 21
    target 157
  ]
  edge [
    source 21
    target 178
  ]
  edge [
    source 21
    target 179
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 35
  ]
  edge [
    source 22
    target 180
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 181
  ]
  edge [
    source 23
    target 182
  ]
  edge [
    source 23
    target 183
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 184
  ]
  edge [
    source 26
    target 185
  ]
  edge [
    source 26
    target 186
  ]
  edge [
    source 26
    target 187
  ]
  edge [
    source 26
    target 188
  ]
  edge [
    source 26
    target 189
  ]
  edge [
    source 26
    target 190
  ]
  edge [
    source 26
    target 191
  ]
  edge [
    source 26
    target 192
  ]
  edge [
    source 26
    target 193
  ]
  edge [
    source 27
    target 35
  ]
  edge [
    source 27
    target 194
  ]
  edge [
    source 27
    target 195
  ]
  edge [
    source 27
    target 196
  ]
  edge [
    source 27
    target 197
  ]
  edge [
    source 27
    target 198
  ]
  edge [
    source 27
    target 199
  ]
  edge [
    source 27
    target 200
  ]
  edge [
    source 27
    target 201
  ]
  edge [
    source 27
    target 202
  ]
  edge [
    source 27
    target 203
  ]
  edge [
    source 27
    target 204
  ]
  edge [
    source 27
    target 205
  ]
  edge [
    source 27
    target 206
  ]
  edge [
    source 27
    target 207
  ]
  edge [
    source 27
    target 208
  ]
  edge [
    source 27
    target 209
  ]
  edge [
    source 27
    target 210
  ]
  edge [
    source 27
    target 211
  ]
  edge [
    source 27
    target 212
  ]
  edge [
    source 27
    target 213
  ]
  edge [
    source 27
    target 214
  ]
  edge [
    source 27
    target 215
  ]
  edge [
    source 27
    target 216
  ]
  edge [
    source 27
    target 217
  ]
  edge [
    source 27
    target 218
  ]
  edge [
    source 27
    target 219
  ]
  edge [
    source 27
    target 27
  ]
  edge [
    source 28
    target 220
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 221
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 222
  ]
  edge [
    source 30
    target 223
  ]
  edge [
    source 30
    target 76
  ]
  edge [
    source 30
    target 224
  ]
  edge [
    source 30
    target 225
  ]
  edge [
    source 30
    target 226
  ]
  edge [
    source 30
    target 227
  ]
  edge [
    source 30
    target 43
  ]
  edge [
    source 30
    target 228
  ]
  edge [
    source 30
    target 44
  ]
  edge [
    source 30
    target 48
  ]
  edge [
    source 30
    target 50
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 34
  ]
  edge [
    source 31
    target 35
  ]
  edge [
    source 31
    target 229
  ]
  edge [
    source 31
    target 230
  ]
  edge [
    source 31
    target 231
  ]
  edge [
    source 31
    target 44
  ]
  edge [
    source 31
    target 48
  ]
  edge [
    source 31
    target 50
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 220
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 36
  ]
  edge [
    source 33
    target 37
  ]
  edge [
    source 33
    target 232
  ]
  edge [
    source 33
    target 49
  ]
  edge [
    source 34
    target 41
  ]
  edge [
    source 34
    target 42
  ]
  edge [
    source 34
    target 40
  ]
  edge [
    source 34
    target 39
  ]
  edge [
    source 35
    target 233
  ]
  edge [
    source 35
    target 234
  ]
  edge [
    source 35
    target 235
  ]
  edge [
    source 35
    target 236
  ]
  edge [
    source 35
    target 237
  ]
  edge [
    source 35
    target 238
  ]
  edge [
    source 35
    target 239
  ]
  edge [
    source 35
    target 157
  ]
  edge [
    source 35
    target 47
  ]
  edge [
    source 35
    target 38
  ]
  edge [
    source 36
    target 240
  ]
  edge [
    source 36
    target 241
  ]
  edge [
    source 36
    target 242
  ]
  edge [
    source 36
    target 243
  ]
  edge [
    source 36
    target 244
  ]
  edge [
    source 36
    target 245
  ]
  edge [
    source 36
    target 246
  ]
  edge [
    source 36
    target 247
  ]
  edge [
    source 36
    target 248
  ]
  edge [
    source 36
    target 249
  ]
  edge [
    source 36
    target 250
  ]
  edge [
    source 36
    target 251
  ]
  edge [
    source 36
    target 252
  ]
  edge [
    source 36
    target 233
  ]
  edge [
    source 36
    target 235
  ]
  edge [
    source 36
    target 253
  ]
  edge [
    source 36
    target 254
  ]
  edge [
    source 36
    target 255
  ]
  edge [
    source 36
    target 256
  ]
  edge [
    source 36
    target 257
  ]
  edge [
    source 36
    target 258
  ]
  edge [
    source 36
    target 259
  ]
  edge [
    source 36
    target 260
  ]
  edge [
    source 36
    target 261
  ]
  edge [
    source 36
    target 262
  ]
  edge [
    source 36
    target 263
  ]
  edge [
    source 36
    target 264
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 265
  ]
  edge [
    source 37
    target 266
  ]
  edge [
    source 37
    target 267
  ]
  edge [
    source 37
    target 268
  ]
  edge [
    source 37
    target 261
  ]
  edge [
    source 37
    target 239
  ]
  edge [
    source 37
    target 269
  ]
  edge [
    source 37
    target 270
  ]
  edge [
    source 38
    target 271
  ]
  edge [
    source 38
    target 272
  ]
  edge [
    source 38
    target 273
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 40
    target 42
  ]
  edge [
    source 40
    target 274
  ]
  edge [
    source 40
    target 275
  ]
  edge [
    source 40
    target 276
  ]
  edge [
    source 40
    target 277
  ]
  edge [
    source 40
    target 278
  ]
  edge [
    source 40
    target 279
  ]
  edge [
    source 40
    target 280
  ]
  edge [
    source 40
    target 52
  ]
  edge [
    source 41
    target 281
  ]
  edge [
    source 41
    target 282
  ]
  edge [
    source 41
    target 283
  ]
  edge [
    source 41
    target 284
  ]
  edge [
    source 41
    target 285
  ]
  edge [
    source 41
    target 286
  ]
  edge [
    source 41
    target 287
  ]
  edge [
    source 41
    target 288
  ]
  edge [
    source 41
    target 289
  ]
  edge [
    source 41
    target 290
  ]
  edge [
    source 41
    target 291
  ]
  edge [
    source 41
    target 292
  ]
  edge [
    source 41
    target 293
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 49
  ]
  edge [
    source 42
    target 294
  ]
  edge [
    source 42
    target 295
  ]
  edge [
    source 42
    target 296
  ]
  edge [
    source 42
    target 297
  ]
  edge [
    source 42
    target 298
  ]
  edge [
    source 42
    target 299
  ]
  edge [
    source 42
    target 300
  ]
  edge [
    source 42
    target 301
  ]
  edge [
    source 42
    target 302
  ]
  edge [
    source 42
    target 303
  ]
  edge [
    source 42
    target 304
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 305
  ]
  edge [
    source 43
    target 306
  ]
  edge [
    source 43
    target 223
  ]
  edge [
    source 43
    target 307
  ]
  edge [
    source 43
    target 308
  ]
  edge [
    source 43
    target 309
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 48
  ]
  edge [
    source 44
    target 50
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 310
  ]
  edge [
    source 45
    target 311
  ]
  edge [
    source 45
    target 312
  ]
  edge [
    source 45
    target 313
  ]
  edge [
    source 45
    target 314
  ]
  edge [
    source 45
    target 235
  ]
  edge [
    source 45
    target 315
  ]
  edge [
    source 45
    target 316
  ]
  edge [
    source 45
    target 317
  ]
  edge [
    source 46
    target 318
  ]
  edge [
    source 46
    target 319
  ]
  edge [
    source 46
    target 320
  ]
  edge [
    source 46
    target 321
  ]
  edge [
    source 46
    target 322
  ]
  edge [
    source 46
    target 323
  ]
  edge [
    source 46
    target 324
  ]
  edge [
    source 46
    target 325
  ]
  edge [
    source 46
    target 326
  ]
  edge [
    source 46
    target 327
  ]
  edge [
    source 46
    target 328
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 329
  ]
  edge [
    source 47
    target 330
  ]
  edge [
    source 47
    target 331
  ]
  edge [
    source 47
    target 268
  ]
  edge [
    source 47
    target 235
  ]
  edge [
    source 47
    target 239
  ]
  edge [
    source 47
    target 332
  ]
  edge [
    source 47
    target 333
  ]
  edge [
    source 48
    target 334
  ]
  edge [
    source 48
    target 335
  ]
  edge [
    source 48
    target 50
  ]
  edge [
    source 49
    target 336
  ]
  edge [
    source 49
    target 337
  ]
  edge [
    source 49
    target 338
  ]
  edge [
    source 49
    target 339
  ]
  edge [
    source 49
    target 340
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 341
  ]
  edge [
    source 50
    target 342
  ]
  edge [
    source 50
    target 343
  ]
  edge [
    source 50
    target 344
  ]
  edge [
    source 50
    target 345
  ]
  edge [
    source 50
    target 346
  ]
  edge [
    source 50
    target 347
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 348
  ]
  edge [
    source 52
    target 349
  ]
  edge [
    source 52
    target 350
  ]
  edge [
    source 351
    target 352
  ]
  edge [
    source 353
    target 354
  ]
  edge [
    source 353
    target 355
  ]
  edge [
    source 354
    target 355
  ]
  edge [
    source 356
    target 357
  ]
  edge [
    source 358
    target 359
  ]
]
