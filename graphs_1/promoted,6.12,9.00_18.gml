graph [
  maxDegree 71
  minDegree 1
  meanDegree 2.2857142857142856
  density 0.0121580547112462
  graphCliqueNumber 6
  node [
    id 0
    label "regionalny"
    origin "text"
  ]
  node [
    id 1
    label "dyrektor"
    origin "text"
  ]
  node [
    id 2
    label "ochrona"
    origin "text"
  ]
  node [
    id 3
    label "&#347;rodowisko"
    origin "text"
  ]
  node [
    id 4
    label "olsztyn"
    origin "text"
  ]
  node [
    id 5
    label "podpisa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "decyzja"
    origin "text"
  ]
  node [
    id 7
    label "&#347;rodowiskowy"
    origin "text"
  ]
  node [
    id 8
    label "uwarunkowanie"
    origin "text"
  ]
  node [
    id 9
    label "dla"
    origin "text"
  ]
  node [
    id 10
    label "inwestycja"
    origin "text"
  ]
  node [
    id 11
    label "wodny"
    origin "text"
  ]
  node [
    id 12
    label "&#322;&#261;czy&#263;"
    origin "text"
  ]
  node [
    id 13
    label "zalewo"
    origin "text"
  ]
  node [
    id 14
    label "wi&#347;lanin"
    origin "text"
  ]
  node [
    id 15
    label "zatoka"
    origin "text"
  ]
  node [
    id 16
    label "gda&#324;ska"
    origin "text"
  ]
  node [
    id 17
    label "lokalizacja"
    origin "text"
  ]
  node [
    id 18
    label "nowa"
    origin "text"
  ]
  node [
    id 19
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 20
    label "poda&#263;"
    origin "text"
  ]
  node [
    id 21
    label "rzeczniczka"
    origin "text"
  ]
  node [
    id 22
    label "rdo&#347;"
    origin "text"
  ]
  node [
    id 23
    label "justyn"
    origin "text"
  ]
  node [
    id 24
    label "januszewicz"
    origin "text"
  ]
  node [
    id 25
    label "lokalny"
  ]
  node [
    id 26
    label "typowy"
  ]
  node [
    id 27
    label "tradycyjny"
  ]
  node [
    id 28
    label "regionalnie"
  ]
  node [
    id 29
    label "dyro"
  ]
  node [
    id 30
    label "dyrektoriat"
  ]
  node [
    id 31
    label "dyrygent"
  ]
  node [
    id 32
    label "Tadeusz_Rydzyk"
  ]
  node [
    id 33
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 34
    label "zwierzchnik"
  ]
  node [
    id 35
    label "chemical_bond"
  ]
  node [
    id 36
    label "tarcza"
  ]
  node [
    id 37
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 38
    label "obiekt"
  ]
  node [
    id 39
    label "Stra&#380;_Ochrony_Kolei"
  ]
  node [
    id 40
    label "borowiec"
  ]
  node [
    id 41
    label "obstawienie"
  ]
  node [
    id 42
    label "formacja"
  ]
  node [
    id 43
    label "ubezpieczenie"
  ]
  node [
    id 44
    label "obstawia&#263;"
  ]
  node [
    id 45
    label "obstawianie"
  ]
  node [
    id 46
    label "transportacja"
  ]
  node [
    id 47
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 48
    label "obiekt_naturalny"
  ]
  node [
    id 49
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 50
    label "grupa"
  ]
  node [
    id 51
    label "stw&#243;r"
  ]
  node [
    id 52
    label "rzecz"
  ]
  node [
    id 53
    label "environment"
  ]
  node [
    id 54
    label "biota"
  ]
  node [
    id 55
    label "wszechstworzenie"
  ]
  node [
    id 56
    label "otoczenie"
  ]
  node [
    id 57
    label "fauna"
  ]
  node [
    id 58
    label "ekosystem"
  ]
  node [
    id 59
    label "teren"
  ]
  node [
    id 60
    label "mikrokosmos"
  ]
  node [
    id 61
    label "class"
  ]
  node [
    id 62
    label "zesp&#243;&#322;"
  ]
  node [
    id 63
    label "warunki"
  ]
  node [
    id 64
    label "huczek"
  ]
  node [
    id 65
    label "Ziemia"
  ]
  node [
    id 66
    label "woda"
  ]
  node [
    id 67
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 68
    label "za&#347;wiadczy&#263;"
  ]
  node [
    id 69
    label "postawi&#263;"
  ]
  node [
    id 70
    label "sign"
  ]
  node [
    id 71
    label "opatrzy&#263;"
  ]
  node [
    id 72
    label "dokument"
  ]
  node [
    id 73
    label "resolution"
  ]
  node [
    id 74
    label "zdecydowanie"
  ]
  node [
    id 75
    label "wytw&#243;r"
  ]
  node [
    id 76
    label "podj&#281;cie_decyzji"
  ]
  node [
    id 77
    label "management"
  ]
  node [
    id 78
    label "okre&#347;lenie"
  ]
  node [
    id 79
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 80
    label "agent"
  ]
  node [
    id 81
    label "ekspozycja"
  ]
  node [
    id 82
    label "faktor"
  ]
  node [
    id 83
    label "inwestycje"
  ]
  node [
    id 84
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 85
    label "wydatki_maj&#261;tkowe"
  ]
  node [
    id 86
    label "wk&#322;ad"
  ]
  node [
    id 87
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 88
    label "kapita&#322;"
  ]
  node [
    id 89
    label "przedsi&#281;wzi&#281;cie"
  ]
  node [
    id 90
    label "inwestowanie"
  ]
  node [
    id 91
    label "bud&#380;et_domowy"
  ]
  node [
    id 92
    label "sentyment_inwestycyjny"
  ]
  node [
    id 93
    label "rezultat"
  ]
  node [
    id 94
    label "specjalny"
  ]
  node [
    id 95
    label "relate"
  ]
  node [
    id 96
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 97
    label "wsp&#243;&#322;&#380;y&#263;"
  ]
  node [
    id 98
    label "robi&#263;"
  ]
  node [
    id 99
    label "po&#347;redniczy&#263;"
  ]
  node [
    id 100
    label "powodowa&#263;"
  ]
  node [
    id 101
    label "za&#322;&#261;cza&#263;"
  ]
  node [
    id 102
    label "mieszkaniec"
  ]
  node [
    id 103
    label "jama"
  ]
  node [
    id 104
    label "jezdnia"
  ]
  node [
    id 105
    label "&#322;ysina"
  ]
  node [
    id 106
    label "korona_drogi"
  ]
  node [
    id 107
    label "przystanek"
  ]
  node [
    id 108
    label "zbiornik_wodny"
  ]
  node [
    id 109
    label "golf"
  ]
  node [
    id 110
    label "Zatoka_Botnicka"
  ]
  node [
    id 111
    label "zaburzenie"
  ]
  node [
    id 112
    label "czynno&#347;&#263;"
  ]
  node [
    id 113
    label "miejsce"
  ]
  node [
    id 114
    label "decyzja_lokalizacyjna"
  ]
  node [
    id 115
    label "localization"
  ]
  node [
    id 116
    label "le&#380;e&#263;"
  ]
  node [
    id 117
    label "adres"
  ]
  node [
    id 118
    label "gwiazda"
  ]
  node [
    id 119
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 120
    label "obszar"
  ]
  node [
    id 121
    label "przedmiot"
  ]
  node [
    id 122
    label "Stary_&#346;wiat"
  ]
  node [
    id 123
    label "biosfera"
  ]
  node [
    id 124
    label "magnetosfera"
  ]
  node [
    id 125
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 126
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 127
    label "geosfera"
  ]
  node [
    id 128
    label "Nowy_&#346;wiat"
  ]
  node [
    id 129
    label "planeta"
  ]
  node [
    id 130
    label "przejmowa&#263;"
  ]
  node [
    id 131
    label "litosfera"
  ]
  node [
    id 132
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 133
    label "makrokosmos"
  ]
  node [
    id 134
    label "barysfera"
  ]
  node [
    id 135
    label "p&#243;&#322;noc"
  ]
  node [
    id 136
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 137
    label "geotermia"
  ]
  node [
    id 138
    label "biegun"
  ]
  node [
    id 139
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 140
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 141
    label "zjawisko"
  ]
  node [
    id 142
    label "p&#243;&#322;kula"
  ]
  node [
    id 143
    label "atmosfera"
  ]
  node [
    id 144
    label "po&#322;udnie"
  ]
  node [
    id 145
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 146
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 147
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 148
    label "przejmowanie"
  ]
  node [
    id 149
    label "przestrze&#324;"
  ]
  node [
    id 150
    label "asymilowanie_si&#281;"
  ]
  node [
    id 151
    label "przej&#261;&#263;"
  ]
  node [
    id 152
    label "ekosfera"
  ]
  node [
    id 153
    label "przyroda"
  ]
  node [
    id 154
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 155
    label "ciemna_materia"
  ]
  node [
    id 156
    label "geoida"
  ]
  node [
    id 157
    label "Wsch&#243;d"
  ]
  node [
    id 158
    label "populace"
  ]
  node [
    id 159
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 160
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 161
    label "universe"
  ]
  node [
    id 162
    label "ozonosfera"
  ]
  node [
    id 163
    label "rze&#378;ba"
  ]
  node [
    id 164
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 165
    label "zagranica"
  ]
  node [
    id 166
    label "hydrosfera"
  ]
  node [
    id 167
    label "kuchnia"
  ]
  node [
    id 168
    label "przej&#281;cie"
  ]
  node [
    id 169
    label "czarna_dziura"
  ]
  node [
    id 170
    label "morze"
  ]
  node [
    id 171
    label "tenis"
  ]
  node [
    id 172
    label "da&#263;"
  ]
  node [
    id 173
    label "siatk&#243;wka"
  ]
  node [
    id 174
    label "introduce"
  ]
  node [
    id 175
    label "jedzenie"
  ]
  node [
    id 176
    label "zaserwowa&#263;"
  ]
  node [
    id 177
    label "give"
  ]
  node [
    id 178
    label "ustawi&#263;"
  ]
  node [
    id 179
    label "zagra&#263;"
  ]
  node [
    id 180
    label "supply"
  ]
  node [
    id 181
    label "nafaszerowa&#263;"
  ]
  node [
    id 182
    label "poinformowa&#263;"
  ]
  node [
    id 183
    label "Justyn"
  ]
  node [
    id 184
    label "Januszewicz"
  ]
  node [
    id 185
    label "Zalewo"
  ]
  node [
    id 186
    label "Wi&#347;lanin"
  ]
  node [
    id 187
    label "w"
  ]
  node [
    id 188
    label "Olsztyn"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 95
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 102
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 103
  ]
  edge [
    source 15
    target 104
  ]
  edge [
    source 15
    target 105
  ]
  edge [
    source 15
    target 106
  ]
  edge [
    source 15
    target 107
  ]
  edge [
    source 15
    target 66
  ]
  edge [
    source 15
    target 108
  ]
  edge [
    source 15
    target 109
  ]
  edge [
    source 15
    target 110
  ]
  edge [
    source 15
    target 111
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 112
  ]
  edge [
    source 17
    target 113
  ]
  edge [
    source 17
    target 114
  ]
  edge [
    source 17
    target 115
  ]
  edge [
    source 17
    target 116
  ]
  edge [
    source 17
    target 117
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 118
  ]
  edge [
    source 18
    target 119
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 47
  ]
  edge [
    source 19
    target 120
  ]
  edge [
    source 19
    target 48
  ]
  edge [
    source 19
    target 121
  ]
  edge [
    source 19
    target 122
  ]
  edge [
    source 19
    target 50
  ]
  edge [
    source 19
    target 51
  ]
  edge [
    source 19
    target 123
  ]
  edge [
    source 19
    target 49
  ]
  edge [
    source 19
    target 52
  ]
  edge [
    source 19
    target 124
  ]
  edge [
    source 19
    target 125
  ]
  edge [
    source 19
    target 53
  ]
  edge [
    source 19
    target 126
  ]
  edge [
    source 19
    target 127
  ]
  edge [
    source 19
    target 128
  ]
  edge [
    source 19
    target 129
  ]
  edge [
    source 19
    target 130
  ]
  edge [
    source 19
    target 131
  ]
  edge [
    source 19
    target 132
  ]
  edge [
    source 19
    target 133
  ]
  edge [
    source 19
    target 134
  ]
  edge [
    source 19
    target 54
  ]
  edge [
    source 19
    target 135
  ]
  edge [
    source 19
    target 136
  ]
  edge [
    source 19
    target 57
  ]
  edge [
    source 19
    target 55
  ]
  edge [
    source 19
    target 137
  ]
  edge [
    source 19
    target 138
  ]
  edge [
    source 19
    target 139
  ]
  edge [
    source 19
    target 58
  ]
  edge [
    source 19
    target 140
  ]
  edge [
    source 19
    target 59
  ]
  edge [
    source 19
    target 141
  ]
  edge [
    source 19
    target 142
  ]
  edge [
    source 19
    target 143
  ]
  edge [
    source 19
    target 60
  ]
  edge [
    source 19
    target 61
  ]
  edge [
    source 19
    target 144
  ]
  edge [
    source 19
    target 145
  ]
  edge [
    source 19
    target 146
  ]
  edge [
    source 19
    target 147
  ]
  edge [
    source 19
    target 148
  ]
  edge [
    source 19
    target 149
  ]
  edge [
    source 19
    target 150
  ]
  edge [
    source 19
    target 151
  ]
  edge [
    source 19
    target 152
  ]
  edge [
    source 19
    target 153
  ]
  edge [
    source 19
    target 154
  ]
  edge [
    source 19
    target 155
  ]
  edge [
    source 19
    target 156
  ]
  edge [
    source 19
    target 157
  ]
  edge [
    source 19
    target 158
  ]
  edge [
    source 19
    target 159
  ]
  edge [
    source 19
    target 64
  ]
  edge [
    source 19
    target 160
  ]
  edge [
    source 19
    target 65
  ]
  edge [
    source 19
    target 161
  ]
  edge [
    source 19
    target 162
  ]
  edge [
    source 19
    target 163
  ]
  edge [
    source 19
    target 164
  ]
  edge [
    source 19
    target 165
  ]
  edge [
    source 19
    target 166
  ]
  edge [
    source 19
    target 66
  ]
  edge [
    source 19
    target 167
  ]
  edge [
    source 19
    target 168
  ]
  edge [
    source 19
    target 169
  ]
  edge [
    source 19
    target 67
  ]
  edge [
    source 19
    target 170
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 171
  ]
  edge [
    source 20
    target 172
  ]
  edge [
    source 20
    target 173
  ]
  edge [
    source 20
    target 174
  ]
  edge [
    source 20
    target 175
  ]
  edge [
    source 20
    target 176
  ]
  edge [
    source 20
    target 177
  ]
  edge [
    source 20
    target 178
  ]
  edge [
    source 20
    target 179
  ]
  edge [
    source 20
    target 180
  ]
  edge [
    source 20
    target 181
  ]
  edge [
    source 20
    target 182
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 183
    target 184
  ]
  edge [
    source 185
    target 186
  ]
  edge [
    source 187
    target 188
  ]
]
