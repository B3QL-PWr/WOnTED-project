graph [
  maxDegree 27
  minDegree 1
  meanDegree 2.0631578947368423
  density 0.0219484882418813
  graphCliqueNumber 3
  node [
    id 0
    label "olbrzym"
    origin "text"
  ]
  node [
    id 1
    label "hydrauliczny"
    origin "text"
  ]
  node [
    id 2
    label "maszyna"
    origin "text"
  ]
  node [
    id 3
    label "ku&#263;"
    origin "text"
  ]
  node [
    id 4
    label "stal"
    origin "text"
  ]
  node [
    id 5
    label "gor&#261;co"
    origin "text"
  ]
  node [
    id 6
    label "proces"
    origin "text"
  ]
  node [
    id 7
    label "obr&#243;bka"
    origin "text"
  ]
  node [
    id 8
    label "metal"
    origin "text"
  ]
  node [
    id 9
    label "cz&#322;owiek"
  ]
  node [
    id 10
    label "gwiazda"
  ]
  node [
    id 11
    label "przedmiot"
  ]
  node [
    id 12
    label "Gargantua"
  ]
  node [
    id 13
    label "nieu&#322;omek"
  ]
  node [
    id 14
    label "istota_fantastyczna"
  ]
  node [
    id 15
    label "zwierz&#281;"
  ]
  node [
    id 16
    label "ogromny"
  ]
  node [
    id 17
    label "hydraulicznie"
  ]
  node [
    id 18
    label "rami&#281;"
  ]
  node [
    id 19
    label "maszyneria"
  ]
  node [
    id 20
    label "pracowanie"
  ]
  node [
    id 21
    label "przeci&#261;&#380;alno&#347;&#263;"
  ]
  node [
    id 22
    label "tuleja"
  ]
  node [
    id 23
    label "b&#281;benek"
  ]
  node [
    id 24
    label "dehumanizacja"
  ]
  node [
    id 25
    label "b&#281;ben"
  ]
  node [
    id 26
    label "wa&#322;"
  ]
  node [
    id 27
    label "kolumna"
  ]
  node [
    id 28
    label "kad&#322;ub"
  ]
  node [
    id 29
    label "mimo&#347;r&#243;d"
  ]
  node [
    id 30
    label "rz&#281;&#380;enie"
  ]
  node [
    id 31
    label "mechanizm"
  ]
  node [
    id 32
    label "t&#322;ok"
  ]
  node [
    id 33
    label "pracowa&#263;"
  ]
  node [
    id 34
    label "przyk&#322;adka"
  ]
  node [
    id 35
    label "rz&#281;zi&#263;"
  ]
  node [
    id 36
    label "trawers"
  ]
  node [
    id 37
    label "wa&#322;ek"
  ]
  node [
    id 38
    label "n&#243;&#380;"
  ]
  node [
    id 39
    label "prototypownia"
  ]
  node [
    id 40
    label "urz&#261;dzenie"
  ]
  node [
    id 41
    label "deflektor"
  ]
  node [
    id 42
    label "kowal"
  ]
  node [
    id 43
    label "obrabia&#263;"
  ]
  node [
    id 44
    label "uczy&#263;_si&#281;"
  ]
  node [
    id 45
    label "kruszy&#263;"
  ]
  node [
    id 46
    label "forge"
  ]
  node [
    id 47
    label "wbija&#263;"
  ]
  node [
    id 48
    label "chase"
  ]
  node [
    id 49
    label "strike"
  ]
  node [
    id 50
    label "kuwa&#263;"
  ]
  node [
    id 51
    label "stop"
  ]
  node [
    id 52
    label "steel"
  ]
  node [
    id 53
    label "austenityzowanie"
  ]
  node [
    id 54
    label "ardor"
  ]
  node [
    id 55
    label "szkodliwie"
  ]
  node [
    id 56
    label "gor&#261;cy"
  ]
  node [
    id 57
    label "seksownie"
  ]
  node [
    id 58
    label "serdecznie"
  ]
  node [
    id 59
    label "g&#322;&#281;boko"
  ]
  node [
    id 60
    label "ciep&#322;o"
  ]
  node [
    id 61
    label "war"
  ]
  node [
    id 62
    label "legislacyjnie"
  ]
  node [
    id 63
    label "kognicja"
  ]
  node [
    id 64
    label "przebieg"
  ]
  node [
    id 65
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 66
    label "wydarzenie"
  ]
  node [
    id 67
    label "przes&#322;anka"
  ]
  node [
    id 68
    label "rozprawa"
  ]
  node [
    id 69
    label "zjawisko"
  ]
  node [
    id 70
    label "nast&#281;pstwo"
  ]
  node [
    id 71
    label "proces_technologiczny"
  ]
  node [
    id 72
    label "czynno&#347;&#263;"
  ]
  node [
    id 73
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 74
    label "odlewalnia"
  ]
  node [
    id 75
    label "rock"
  ]
  node [
    id 76
    label "pieszczocha"
  ]
  node [
    id 77
    label "orygina&#322;"
  ]
  node [
    id 78
    label "metallic_element"
  ]
  node [
    id 79
    label "przeci&#261;gn&#261;&#263;"
  ]
  node [
    id 80
    label "pogo"
  ]
  node [
    id 81
    label "pierwiastek"
  ]
  node [
    id 82
    label "przebijarka"
  ]
  node [
    id 83
    label "sk&#243;ra"
  ]
  node [
    id 84
    label "przeci&#261;gni&#281;cie"
  ]
  node [
    id 85
    label "pi&#243;ra"
  ]
  node [
    id 86
    label "wytrawialnia"
  ]
  node [
    id 87
    label "wytrawia&#263;"
  ]
  node [
    id 88
    label "kuc"
  ]
  node [
    id 89
    label "fan"
  ]
  node [
    id 90
    label "kucie"
  ]
  node [
    id 91
    label "przedstawiciel"
  ]
  node [
    id 92
    label "pogowa&#263;"
  ]
  node [
    id 93
    label "topialnia"
  ]
  node [
    id 94
    label "naszywka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 8
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
]
