graph [
  maxDegree 11
  minDegree 1
  meanDegree 1.8571428571428572
  density 0.14285714285714285
  graphCliqueNumber 2
  node [
    id 0
    label "lotnictwo"
    origin "text"
  ]
  node [
    id 1
    label "bombowy"
    origin "text"
  ]
  node [
    id 2
    label "konwojer"
  ]
  node [
    id 3
    label "skrzyd&#322;o"
  ]
  node [
    id 4
    label "awiacja"
  ]
  node [
    id 5
    label "skr&#281;tomierz"
  ]
  node [
    id 6
    label "nauka"
  ]
  node [
    id 7
    label "transport"
  ]
  node [
    id 8
    label "wojsko"
  ]
  node [
    id 9
    label "nawigacja"
  ]
  node [
    id 10
    label "balenit"
  ]
  node [
    id 11
    label "Si&#322;y_Powietrzne"
  ]
  node [
    id 12
    label "kapitalny"
  ]
  node [
    id 13
    label "bombowo"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
]
