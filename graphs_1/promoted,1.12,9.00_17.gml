graph [
  maxDegree 19
  minDegree 1
  meanDegree 2.033333333333333
  density 0.03446327683615819
  graphCliqueNumber 2
  node [
    id 0
    label "cena"
    origin "text"
  ]
  node [
    id 1
    label "krzem"
    origin "text"
  ]
  node [
    id 2
    label "panel"
    origin "text"
  ]
  node [
    id 3
    label "spad&#322;y"
    origin "text"
  ]
  node [
    id 4
    label "sam"
    origin "text"
  ]
  node [
    id 5
    label "ci&#261;g"
    origin "text"
  ]
  node [
    id 6
    label "rok"
    origin "text"
  ]
  node [
    id 7
    label "cenowa_zmiana_poda&#380;y"
  ]
  node [
    id 8
    label "warto&#347;&#263;"
  ]
  node [
    id 9
    label "cenowa_elastyczno&#347;&#263;_popytu"
  ]
  node [
    id 10
    label "dyskryminacja_cenowa"
  ]
  node [
    id 11
    label "inflacja"
  ]
  node [
    id 12
    label "kupowanie"
  ]
  node [
    id 13
    label "kosztowa&#263;"
  ]
  node [
    id 14
    label "kosztowanie"
  ]
  node [
    id 15
    label "worth"
  ]
  node [
    id 16
    label "wyceni&#263;"
  ]
  node [
    id 17
    label "wycenienie"
  ]
  node [
    id 18
    label "w&#281;glowiec"
  ]
  node [
    id 19
    label "silicon"
  ]
  node [
    id 20
    label "p&#243;&#322;metal"
  ]
  node [
    id 21
    label "p&#243;&#322;przewodnik"
  ]
  node [
    id 22
    label "sonda&#380;"
  ]
  node [
    id 23
    label "dyskusja"
  ]
  node [
    id 24
    label "coffer"
  ]
  node [
    id 25
    label "p&#322;yta"
  ]
  node [
    id 26
    label "konsola"
  ]
  node [
    id 27
    label "ok&#322;adzina"
  ]
  node [
    id 28
    label "opakowanie"
  ]
  node [
    id 29
    label "sklep"
  ]
  node [
    id 30
    label "si&#322;a"
  ]
  node [
    id 31
    label "stan"
  ]
  node [
    id 32
    label "lina"
  ]
  node [
    id 33
    label "way"
  ]
  node [
    id 34
    label "cable"
  ]
  node [
    id 35
    label "przebieg"
  ]
  node [
    id 36
    label "zbi&#243;r"
  ]
  node [
    id 37
    label "ch&#243;d"
  ]
  node [
    id 38
    label "trasa"
  ]
  node [
    id 39
    label "rz&#261;d"
  ]
  node [
    id 40
    label "k&#322;us"
  ]
  node [
    id 41
    label "progression"
  ]
  node [
    id 42
    label "current"
  ]
  node [
    id 43
    label "pr&#261;d"
  ]
  node [
    id 44
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 45
    label "wydarzenie"
  ]
  node [
    id 46
    label "lot"
  ]
  node [
    id 47
    label "stulecie"
  ]
  node [
    id 48
    label "kalendarz"
  ]
  node [
    id 49
    label "czas"
  ]
  node [
    id 50
    label "pora_roku"
  ]
  node [
    id 51
    label "cykl_astronomiczny"
  ]
  node [
    id 52
    label "p&#243;&#322;rocze"
  ]
  node [
    id 53
    label "grupa"
  ]
  node [
    id 54
    label "kwarta&#322;"
  ]
  node [
    id 55
    label "kurs"
  ]
  node [
    id 56
    label "jubileusz"
  ]
  node [
    id 57
    label "miesi&#261;c"
  ]
  node [
    id 58
    label "lata"
  ]
  node [
    id 59
    label "martwy_sezon"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
]
