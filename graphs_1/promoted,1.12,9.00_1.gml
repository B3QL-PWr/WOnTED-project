graph [
  maxDegree 24
  minDegree 1
  meanDegree 2
  density 0.0196078431372549
  graphCliqueNumber 3
  node [
    id 0
    label "forum"
    origin "text"
  ]
  node [
    id 1
    label "&#380;yd"
    origin "text"
  ]
  node [
    id 2
    label "polski"
    origin "text"
  ]
  node [
    id 3
    label "swoje"
    origin "text"
  ]
  node [
    id 4
    label "profil"
    origin "text"
  ]
  node [
    id 5
    label "facebookowy"
    origin "text"
  ]
  node [
    id 6
    label "zarzuca&#263;"
    origin "text"
  ]
  node [
    id 7
    label "tygodnik"
    origin "text"
  ]
  node [
    id 8
    label "newsweek"
    origin "text"
  ]
  node [
    id 9
    label "manipulacja"
    origin "text"
  ]
  node [
    id 10
    label "artyku&#322;"
    origin "text"
  ]
  node [
    id 11
    label "europa"
    origin "text"
  ]
  node [
    id 12
    label "wci&#261;&#380;"
    origin "text"
  ]
  node [
    id 13
    label "antysemicki"
    origin "text"
  ]
  node [
    id 14
    label "s&#261;d"
  ]
  node [
    id 15
    label "plac"
  ]
  node [
    id 16
    label "miejsce"
  ]
  node [
    id 17
    label "grupa_dyskusyjna"
  ]
  node [
    id 18
    label "grupa"
  ]
  node [
    id 19
    label "przestrze&#324;"
  ]
  node [
    id 20
    label "agora"
  ]
  node [
    id 21
    label "bazylika"
  ]
  node [
    id 22
    label "konferencja"
  ]
  node [
    id 23
    label "strona"
  ]
  node [
    id 24
    label "portal"
  ]
  node [
    id 25
    label "chciwiec"
  ]
  node [
    id 26
    label "judenrat"
  ]
  node [
    id 27
    label "materialista"
  ]
  node [
    id 28
    label "sk&#261;piarz"
  ]
  node [
    id 29
    label "wyznawca"
  ]
  node [
    id 30
    label "&#379;ydziak"
  ]
  node [
    id 31
    label "sk&#261;py"
  ]
  node [
    id 32
    label "szmonces"
  ]
  node [
    id 33
    label "monoteista"
  ]
  node [
    id 34
    label "mosiek"
  ]
  node [
    id 35
    label "lacki"
  ]
  node [
    id 36
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 37
    label "przedmiot"
  ]
  node [
    id 38
    label "sztajer"
  ]
  node [
    id 39
    label "drabant"
  ]
  node [
    id 40
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 41
    label "polak"
  ]
  node [
    id 42
    label "pierogi_ruskie"
  ]
  node [
    id 43
    label "krakowiak"
  ]
  node [
    id 44
    label "Polish"
  ]
  node [
    id 45
    label "j&#281;zyk"
  ]
  node [
    id 46
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 47
    label "oberek"
  ]
  node [
    id 48
    label "po_polsku"
  ]
  node [
    id 49
    label "mazur"
  ]
  node [
    id 50
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 51
    label "chodzony"
  ]
  node [
    id 52
    label "skoczny"
  ]
  node [
    id 53
    label "ryba_po_grecku"
  ]
  node [
    id 54
    label "goniony"
  ]
  node [
    id 55
    label "polsko"
  ]
  node [
    id 56
    label "rzeczywisto&#347;&#263;_wirtualna"
  ]
  node [
    id 57
    label "seria"
  ]
  node [
    id 58
    label "twarz"
  ]
  node [
    id 59
    label "section"
  ]
  node [
    id 60
    label "listwa"
  ]
  node [
    id 61
    label "podgl&#261;d"
  ]
  node [
    id 62
    label "ozdoba"
  ]
  node [
    id 63
    label "dominanta"
  ]
  node [
    id 64
    label "obw&#243;dka"
  ]
  node [
    id 65
    label "faseta"
  ]
  node [
    id 66
    label "kontur"
  ]
  node [
    id 67
    label "profile"
  ]
  node [
    id 68
    label "konto"
  ]
  node [
    id 69
    label "przekr&#243;j"
  ]
  node [
    id 70
    label "awatar"
  ]
  node [
    id 71
    label "charakter"
  ]
  node [
    id 72
    label "element_konstrukcyjny"
  ]
  node [
    id 73
    label "sylwetka"
  ]
  node [
    id 74
    label "twierdzi&#263;"
  ]
  node [
    id 75
    label "intrude"
  ]
  node [
    id 76
    label "przestawa&#263;"
  ]
  node [
    id 77
    label "prosecute"
  ]
  node [
    id 78
    label "umieszcza&#263;"
  ]
  node [
    id 79
    label "przeznacza&#263;"
  ]
  node [
    id 80
    label "bequeath"
  ]
  node [
    id 81
    label "inflict"
  ]
  node [
    id 82
    label "odchodzi&#263;"
  ]
  node [
    id 83
    label "czasopismo"
  ]
  node [
    id 84
    label "czynno&#347;&#263;"
  ]
  node [
    id 85
    label "podst&#281;p"
  ]
  node [
    id 86
    label "dokument"
  ]
  node [
    id 87
    label "towar"
  ]
  node [
    id 88
    label "nag&#322;&#243;wek"
  ]
  node [
    id 89
    label "znak_j&#281;zykowy"
  ]
  node [
    id 90
    label "wyr&#243;b"
  ]
  node [
    id 91
    label "blok"
  ]
  node [
    id 92
    label "line"
  ]
  node [
    id 93
    label "paragraf"
  ]
  node [
    id 94
    label "rodzajnik"
  ]
  node [
    id 95
    label "prawda"
  ]
  node [
    id 96
    label "szkic"
  ]
  node [
    id 97
    label "tekst"
  ]
  node [
    id 98
    label "fragment"
  ]
  node [
    id 99
    label "ci&#261;g&#322;y"
  ]
  node [
    id 100
    label "stale"
  ]
  node [
    id 101
    label "przeciwny"
  ]
  node [
    id 102
    label "anty&#380;ydowsko"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 13
    target 101
  ]
  edge [
    source 13
    target 102
  ]
]
