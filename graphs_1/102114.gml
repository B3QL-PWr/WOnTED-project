graph [
  maxDegree 29
  minDegree 1
  meanDegree 2.108108108108108
  density 0.011457109283196239
  graphCliqueNumber 3
  node [
    id 0
    label "zanim"
    origin "text"
  ]
  node [
    id 1
    label "przyst&#261;pi&#263;"
    origin "text"
  ]
  node [
    id 2
    label "rozkr&#281;ci&#263;"
    origin "text"
  ]
  node [
    id 3
    label "zasilacz"
    origin "text"
  ]
  node [
    id 4
    label "przyjrze&#263;"
    origin "text"
  ]
  node [
    id 5
    label "si&#281;"
    origin "text"
  ]
  node [
    id 6
    label "wi&#261;zka"
    origin "text"
  ]
  node [
    id 7
    label "przew&#243;d"
    origin "text"
  ]
  node [
    id 8
    label "wychodz&#261;ca"
    origin "text"
  ]
  node [
    id 9
    label "obudowa"
    origin "text"
  ]
  node [
    id 10
    label "atx"
    origin "text"
  ]
  node [
    id 11
    label "maja"
    origin "text"
  ]
  node [
    id 12
    label "ten"
    origin "text"
  ]
  node [
    id 13
    label "mi&#322;a"
    origin "text"
  ]
  node [
    id 14
    label "cecha"
    origin "text"
  ]
  node [
    id 15
    label "kolor"
    origin "text"
  ]
  node [
    id 16
    label "dany"
    origin "text"
  ]
  node [
    id 17
    label "jednoznacznie"
    origin "text"
  ]
  node [
    id 18
    label "identyfikowa&#263;"
    origin "text"
  ]
  node [
    id 19
    label "funkcja"
    origin "text"
  ]
  node [
    id 20
    label "bez"
    origin "text"
  ]
  node [
    id 21
    label "wzgl&#261;d"
    origin "text"
  ]
  node [
    id 22
    label "jaki"
    origin "text"
  ]
  node [
    id 23
    label "wtyczka"
    origin "text"
  ]
  node [
    id 24
    label "prowadzi&#263;"
    origin "text"
  ]
  node [
    id 25
    label "przyk&#322;adowo"
    origin "text"
  ]
  node [
    id 26
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 27
    label "czerwone"
    origin "text"
  ]
  node [
    id 28
    label "zawsze"
    origin "text"
  ]
  node [
    id 29
    label "wej&#347;&#263;"
  ]
  node [
    id 30
    label "cause"
  ]
  node [
    id 31
    label "zacz&#261;&#263;"
  ]
  node [
    id 32
    label "mount"
  ]
  node [
    id 33
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 34
    label "rozebra&#263;"
  ]
  node [
    id 35
    label "rozprostowa&#263;"
  ]
  node [
    id 36
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 37
    label "powi&#281;kszy&#263;"
  ]
  node [
    id 38
    label "p&#281;tla_ociekowa"
  ]
  node [
    id 39
    label "urz&#261;dzenie"
  ]
  node [
    id 40
    label "p&#281;k"
  ]
  node [
    id 41
    label "beam"
  ]
  node [
    id 42
    label "&#347;wiat&#322;o"
  ]
  node [
    id 43
    label "&#380;y&#322;a"
  ]
  node [
    id 44
    label "linia"
  ]
  node [
    id 45
    label "przewodnictwo"
  ]
  node [
    id 46
    label "kognicja"
  ]
  node [
    id 47
    label "obw&#243;d_elektryczny"
  ]
  node [
    id 48
    label "tr&#243;jnik"
  ]
  node [
    id 49
    label "organ"
  ]
  node [
    id 50
    label "przy&#322;&#261;cze"
  ]
  node [
    id 51
    label "duct"
  ]
  node [
    id 52
    label "post&#281;powanie"
  ]
  node [
    id 53
    label "wydarzenie"
  ]
  node [
    id 54
    label "rozprawa"
  ]
  node [
    id 55
    label "przes&#322;anka"
  ]
  node [
    id 56
    label "ochrona"
  ]
  node [
    id 57
    label "enclosure"
  ]
  node [
    id 58
    label "konstrukcja"
  ]
  node [
    id 59
    label "wyposa&#380;enie_wn&#281;trz"
  ]
  node [
    id 60
    label "os&#322;ona"
  ]
  node [
    id 61
    label "zabudowa"
  ]
  node [
    id 62
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 63
    label "box"
  ]
  node [
    id 64
    label "wyrobisko"
  ]
  node [
    id 65
    label "wedyzm"
  ]
  node [
    id 66
    label "energia"
  ]
  node [
    id 67
    label "buddyzm"
  ]
  node [
    id 68
    label "okre&#347;lony"
  ]
  node [
    id 69
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 70
    label "ptaszyna"
  ]
  node [
    id 71
    label "umi&#322;owana"
  ]
  node [
    id 72
    label "kochanka"
  ]
  node [
    id 73
    label "kochanie"
  ]
  node [
    id 74
    label "Dulcynea"
  ]
  node [
    id 75
    label "wybranka"
  ]
  node [
    id 76
    label "charakterystyka"
  ]
  node [
    id 77
    label "m&#322;ot"
  ]
  node [
    id 78
    label "pr&#243;ba"
  ]
  node [
    id 79
    label "marka"
  ]
  node [
    id 80
    label "attribute"
  ]
  node [
    id 81
    label "znak"
  ]
  node [
    id 82
    label "drzewo"
  ]
  node [
    id 83
    label "&#347;wieci&#263;"
  ]
  node [
    id 84
    label "prze&#322;ama&#263;_si&#281;"
  ]
  node [
    id 85
    label "prze&#322;amanie"
  ]
  node [
    id 86
    label "prze&#322;amywa&#263;"
  ]
  node [
    id 87
    label "prze&#322;amywa&#263;_si&#281;"
  ]
  node [
    id 88
    label "prze&#322;amywanie_si&#281;"
  ]
  node [
    id 89
    label "ubarwienie"
  ]
  node [
    id 90
    label "symbol"
  ]
  node [
    id 91
    label "prze&#322;amywanie"
  ]
  node [
    id 92
    label "struktura"
  ]
  node [
    id 93
    label "prze&#322;ama&#263;"
  ]
  node [
    id 94
    label "zblakn&#261;&#263;"
  ]
  node [
    id 95
    label "liczba_kwantowa"
  ]
  node [
    id 96
    label "blakn&#261;&#263;"
  ]
  node [
    id 97
    label "zblakni&#281;cie"
  ]
  node [
    id 98
    label "poker"
  ]
  node [
    id 99
    label "&#347;wiecenie"
  ]
  node [
    id 100
    label "blakni&#281;cie"
  ]
  node [
    id 101
    label "prze&#322;amanie_si&#281;"
  ]
  node [
    id 102
    label "jednoznaczny"
  ]
  node [
    id 103
    label "equate"
  ]
  node [
    id 104
    label "rozpoznawa&#263;"
  ]
  node [
    id 105
    label "ujednolica&#263;"
  ]
  node [
    id 106
    label "equal"
  ]
  node [
    id 107
    label "infimum"
  ]
  node [
    id 108
    label "znaczenie"
  ]
  node [
    id 109
    label "awansowanie"
  ]
  node [
    id 110
    label "zastosowanie"
  ]
  node [
    id 111
    label "function"
  ]
  node [
    id 112
    label "funkcjonowanie"
  ]
  node [
    id 113
    label "cel"
  ]
  node [
    id 114
    label "supremum"
  ]
  node [
    id 115
    label "powierzanie"
  ]
  node [
    id 116
    label "rzut"
  ]
  node [
    id 117
    label "addytywno&#347;&#263;"
  ]
  node [
    id 118
    label "monotoniczno&#347;&#263;"
  ]
  node [
    id 119
    label "wakowa&#263;"
  ]
  node [
    id 120
    label "dziedzina"
  ]
  node [
    id 121
    label "postawi&#263;"
  ]
  node [
    id 122
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 123
    label "czyn"
  ]
  node [
    id 124
    label "przeciwdziedzina"
  ]
  node [
    id 125
    label "matematyka"
  ]
  node [
    id 126
    label "awansowa&#263;"
  ]
  node [
    id 127
    label "praca"
  ]
  node [
    id 128
    label "stawia&#263;"
  ]
  node [
    id 129
    label "jednostka"
  ]
  node [
    id 130
    label "ki&#347;&#263;"
  ]
  node [
    id 131
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 132
    label "krzew"
  ]
  node [
    id 133
    label "pi&#380;maczkowate"
  ]
  node [
    id 134
    label "pestkowiec"
  ]
  node [
    id 135
    label "kwiat"
  ]
  node [
    id 136
    label "owoc"
  ]
  node [
    id 137
    label "oliwkowate"
  ]
  node [
    id 138
    label "ro&#347;lina"
  ]
  node [
    id 139
    label "hy&#263;ka"
  ]
  node [
    id 140
    label "lilac"
  ]
  node [
    id 141
    label "delfinidyna"
  ]
  node [
    id 142
    label "przyczyna"
  ]
  node [
    id 143
    label "uwaga"
  ]
  node [
    id 144
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 145
    label "punkt_widzenia"
  ]
  node [
    id 146
    label "szpieg"
  ]
  node [
    id 147
    label "dodatek"
  ]
  node [
    id 148
    label "ko&#324;c&#243;wka"
  ]
  node [
    id 149
    label "connection"
  ]
  node [
    id 150
    label "kontakt"
  ]
  node [
    id 151
    label "control"
  ]
  node [
    id 152
    label "eksponowa&#263;"
  ]
  node [
    id 153
    label "kre&#347;li&#263;"
  ]
  node [
    id 154
    label "g&#243;rowa&#263;"
  ]
  node [
    id 155
    label "message"
  ]
  node [
    id 156
    label "partner"
  ]
  node [
    id 157
    label "string"
  ]
  node [
    id 158
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 159
    label "przesuwa&#263;"
  ]
  node [
    id 160
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 161
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 162
    label "powodowa&#263;"
  ]
  node [
    id 163
    label "kierowa&#263;"
  ]
  node [
    id 164
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 165
    label "robi&#263;"
  ]
  node [
    id 166
    label "manipulate"
  ]
  node [
    id 167
    label "&#380;y&#263;"
  ]
  node [
    id 168
    label "navigate"
  ]
  node [
    id 169
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 170
    label "ukierunkowywa&#263;"
  ]
  node [
    id 171
    label "linia_melodyczna"
  ]
  node [
    id 172
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 173
    label "prowadzenie"
  ]
  node [
    id 174
    label "tworzy&#263;"
  ]
  node [
    id 175
    label "przewy&#380;sza&#263;"
  ]
  node [
    id 176
    label "sterowa&#263;"
  ]
  node [
    id 177
    label "krzywa"
  ]
  node [
    id 178
    label "przyk&#322;adowy"
  ]
  node [
    id 179
    label "jaki&#347;"
  ]
  node [
    id 180
    label "sygnalizacja_&#347;wietlna"
  ]
  node [
    id 181
    label "zaw&#380;dy"
  ]
  node [
    id 182
    label "ci&#261;gle"
  ]
  node [
    id 183
    label "na_zawsze"
  ]
  node [
    id 184
    label "cz&#281;sto"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 16
  ]
  edge [
    source 7
    target 17
  ]
  edge [
    source 7
    target 23
  ]
  edge [
    source 7
    target 12
  ]
  edge [
    source 7
    target 26
  ]
  edge [
    source 7
    target 15
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 39
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 56
  ]
  edge [
    source 9
    target 57
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 65
  ]
  edge [
    source 11
    target 66
  ]
  edge [
    source 11
    target 67
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 24
  ]
  edge [
    source 12
    target 68
  ]
  edge [
    source 12
    target 69
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 70
  ]
  edge [
    source 13
    target 71
  ]
  edge [
    source 13
    target 72
  ]
  edge [
    source 13
    target 73
  ]
  edge [
    source 13
    target 74
  ]
  edge [
    source 13
    target 75
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 76
  ]
  edge [
    source 14
    target 77
  ]
  edge [
    source 14
    target 78
  ]
  edge [
    source 14
    target 79
  ]
  edge [
    source 14
    target 80
  ]
  edge [
    source 14
    target 81
  ]
  edge [
    source 14
    target 82
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 27
  ]
  edge [
    source 15
    target 83
  ]
  edge [
    source 15
    target 84
  ]
  edge [
    source 15
    target 85
  ]
  edge [
    source 15
    target 86
  ]
  edge [
    source 15
    target 87
  ]
  edge [
    source 15
    target 88
  ]
  edge [
    source 15
    target 89
  ]
  edge [
    source 15
    target 90
  ]
  edge [
    source 15
    target 91
  ]
  edge [
    source 15
    target 92
  ]
  edge [
    source 15
    target 93
  ]
  edge [
    source 15
    target 94
  ]
  edge [
    source 15
    target 95
  ]
  edge [
    source 15
    target 96
  ]
  edge [
    source 15
    target 97
  ]
  edge [
    source 15
    target 98
  ]
  edge [
    source 15
    target 99
  ]
  edge [
    source 15
    target 100
  ]
  edge [
    source 15
    target 101
  ]
  edge [
    source 16
    target 68
  ]
  edge [
    source 16
    target 69
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 102
  ]
  edge [
    source 17
    target 27
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 103
  ]
  edge [
    source 18
    target 104
  ]
  edge [
    source 18
    target 105
  ]
  edge [
    source 18
    target 106
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 107
  ]
  edge [
    source 19
    target 108
  ]
  edge [
    source 19
    target 109
  ]
  edge [
    source 19
    target 110
  ]
  edge [
    source 19
    target 111
  ]
  edge [
    source 19
    target 112
  ]
  edge [
    source 19
    target 113
  ]
  edge [
    source 19
    target 114
  ]
  edge [
    source 19
    target 115
  ]
  edge [
    source 19
    target 116
  ]
  edge [
    source 19
    target 117
  ]
  edge [
    source 19
    target 118
  ]
  edge [
    source 19
    target 119
  ]
  edge [
    source 19
    target 120
  ]
  edge [
    source 19
    target 121
  ]
  edge [
    source 19
    target 122
  ]
  edge [
    source 19
    target 123
  ]
  edge [
    source 19
    target 124
  ]
  edge [
    source 19
    target 125
  ]
  edge [
    source 19
    target 126
  ]
  edge [
    source 19
    target 127
  ]
  edge [
    source 19
    target 128
  ]
  edge [
    source 19
    target 129
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 130
  ]
  edge [
    source 20
    target 131
  ]
  edge [
    source 20
    target 132
  ]
  edge [
    source 20
    target 133
  ]
  edge [
    source 20
    target 134
  ]
  edge [
    source 20
    target 135
  ]
  edge [
    source 20
    target 136
  ]
  edge [
    source 20
    target 137
  ]
  edge [
    source 20
    target 138
  ]
  edge [
    source 20
    target 139
  ]
  edge [
    source 20
    target 140
  ]
  edge [
    source 20
    target 141
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 142
  ]
  edge [
    source 21
    target 143
  ]
  edge [
    source 21
    target 144
  ]
  edge [
    source 21
    target 145
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 146
  ]
  edge [
    source 23
    target 147
  ]
  edge [
    source 23
    target 148
  ]
  edge [
    source 23
    target 149
  ]
  edge [
    source 23
    target 150
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 151
  ]
  edge [
    source 24
    target 152
  ]
  edge [
    source 24
    target 153
  ]
  edge [
    source 24
    target 154
  ]
  edge [
    source 24
    target 155
  ]
  edge [
    source 24
    target 156
  ]
  edge [
    source 24
    target 157
  ]
  edge [
    source 24
    target 158
  ]
  edge [
    source 24
    target 159
  ]
  edge [
    source 24
    target 160
  ]
  edge [
    source 24
    target 161
  ]
  edge [
    source 24
    target 162
  ]
  edge [
    source 24
    target 163
  ]
  edge [
    source 24
    target 164
  ]
  edge [
    source 24
    target 165
  ]
  edge [
    source 24
    target 166
  ]
  edge [
    source 24
    target 167
  ]
  edge [
    source 24
    target 168
  ]
  edge [
    source 24
    target 169
  ]
  edge [
    source 24
    target 170
  ]
  edge [
    source 24
    target 171
  ]
  edge [
    source 24
    target 172
  ]
  edge [
    source 24
    target 173
  ]
  edge [
    source 24
    target 174
  ]
  edge [
    source 24
    target 175
  ]
  edge [
    source 24
    target 176
  ]
  edge [
    source 24
    target 177
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 178
  ]
  edge [
    source 26
    target 179
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 180
  ]
  edge [
    source 28
    target 181
  ]
  edge [
    source 28
    target 182
  ]
  edge [
    source 28
    target 183
  ]
  edge [
    source 28
    target 184
  ]
]
