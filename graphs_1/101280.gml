graph [
  maxDegree 5
  minDegree 1
  meanDegree 1.5384615384615385
  density 0.1282051282051282
  graphCliqueNumber 2
  node [
    id 0
    label "naw&#322;o&#263;"
    origin "text"
  ]
  node [
    id 1
    label "kanadyjski"
    origin "text"
  ]
  node [
    id 2
    label "astrowate"
  ]
  node [
    id 3
    label "bylina"
  ]
  node [
    id 4
    label "goldenrod"
  ]
  node [
    id 5
    label "po_kanadyjsku"
  ]
  node [
    id 6
    label "anglosaski"
  ]
  node [
    id 7
    label "p&#243;&#322;nocnoameryka&#324;ski"
  ]
  node [
    id 8
    label "ameryka&#324;ski"
  ]
  node [
    id 9
    label "matka"
  ]
  node [
    id 10
    label "boski"
  ]
  node [
    id 11
    label "ameryk"
  ]
  node [
    id 12
    label "p&#243;&#322;nocny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 11
    target 12
  ]
]
