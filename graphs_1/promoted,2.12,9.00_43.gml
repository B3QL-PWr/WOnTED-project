graph [
  maxDegree 25
  minDegree 1
  meanDegree 1.975609756097561
  density 0.024390243902439025
  graphCliqueNumber 2
  node [
    id 0
    label "oddzia&#322;"
    origin "text"
  ]
  node [
    id 1
    label "przedszkolny"
    origin "text"
  ]
  node [
    id 2
    label "ucz&#281;szcza&#263;"
    origin "text"
  ]
  node [
    id 3
    label "letni"
    origin "text"
  ]
  node [
    id 4
    label "ch&#322;opiec"
    origin "text"
  ]
  node [
    id 5
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 6
    label "prze&#380;y&#263;"
    origin "text"
  ]
  node [
    id 7
    label "ogromny"
    origin "text"
  ]
  node [
    id 8
    label "tragedia"
    origin "text"
  ]
  node [
    id 9
    label "whole"
  ]
  node [
    id 10
    label "jednostka"
  ]
  node [
    id 11
    label "jednostka_geologiczna"
  ]
  node [
    id 12
    label "system"
  ]
  node [
    id 13
    label "poziom"
  ]
  node [
    id 14
    label "agencja"
  ]
  node [
    id 15
    label "dogger"
  ]
  node [
    id 16
    label "formacja"
  ]
  node [
    id 17
    label "pi&#281;tro"
  ]
  node [
    id 18
    label "filia"
  ]
  node [
    id 19
    label "dzia&#322;"
  ]
  node [
    id 20
    label "promocja"
  ]
  node [
    id 21
    label "zesp&#243;&#322;"
  ]
  node [
    id 22
    label "kurs"
  ]
  node [
    id 23
    label "przegrupowywa&#263;_si&#281;"
  ]
  node [
    id 24
    label "wojsko"
  ]
  node [
    id 25
    label "siedziba"
  ]
  node [
    id 26
    label "bank"
  ]
  node [
    id 27
    label "lias"
  ]
  node [
    id 28
    label "szpital"
  ]
  node [
    id 29
    label "przegrupowywanie_si&#281;"
  ]
  node [
    id 30
    label "malm"
  ]
  node [
    id 31
    label "ajencja"
  ]
  node [
    id 32
    label "klasa"
  ]
  node [
    id 33
    label "szczeg&#243;lny"
  ]
  node [
    id 34
    label "odpowiedni"
  ]
  node [
    id 35
    label "instytucjonalny"
  ]
  node [
    id 36
    label "robi&#263;"
  ]
  node [
    id 37
    label "inflict"
  ]
  node [
    id 38
    label "nijaki"
  ]
  node [
    id 39
    label "sezonowy"
  ]
  node [
    id 40
    label "letnio"
  ]
  node [
    id 41
    label "s&#322;oneczny"
  ]
  node [
    id 42
    label "weso&#322;y"
  ]
  node [
    id 43
    label "oboj&#281;tny"
  ]
  node [
    id 44
    label "latowy"
  ]
  node [
    id 45
    label "ciep&#322;y"
  ]
  node [
    id 46
    label "typowy"
  ]
  node [
    id 47
    label "cz&#322;owiek"
  ]
  node [
    id 48
    label "pomocnik"
  ]
  node [
    id 49
    label "g&#243;wniarz"
  ]
  node [
    id 50
    label "&#347;l&#261;ski"
  ]
  node [
    id 51
    label "m&#322;odzieniec"
  ]
  node [
    id 52
    label "kajtek"
  ]
  node [
    id 53
    label "kawaler"
  ]
  node [
    id 54
    label "usynawianie"
  ]
  node [
    id 55
    label "dziecko"
  ]
  node [
    id 56
    label "okrzos"
  ]
  node [
    id 57
    label "usynowienie"
  ]
  node [
    id 58
    label "sympatia"
  ]
  node [
    id 59
    label "pederasta"
  ]
  node [
    id 60
    label "synek"
  ]
  node [
    id 61
    label "boyfriend"
  ]
  node [
    id 62
    label "wytrzyma&#263;"
  ]
  node [
    id 63
    label "przej&#347;&#263;"
  ]
  node [
    id 64
    label "poradzi&#263;_sobie"
  ]
  node [
    id 65
    label "see"
  ]
  node [
    id 66
    label "visualize"
  ]
  node [
    id 67
    label "dozna&#263;"
  ]
  node [
    id 68
    label "olbrzymio"
  ]
  node [
    id 69
    label "wyj&#261;tkowy"
  ]
  node [
    id 70
    label "ogromnie"
  ]
  node [
    id 71
    label "znaczny"
  ]
  node [
    id 72
    label "jebitny"
  ]
  node [
    id 73
    label "prawdziwy"
  ]
  node [
    id 74
    label "wa&#380;ny"
  ]
  node [
    id 75
    label "liczny"
  ]
  node [
    id 76
    label "dono&#347;ny"
  ]
  node [
    id 77
    label "lipa"
  ]
  node [
    id 78
    label "cios"
  ]
  node [
    id 79
    label "dramat"
  ]
  node [
    id 80
    label "gatunek_literacki"
  ]
  node [
    id 81
    label "play"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
]
