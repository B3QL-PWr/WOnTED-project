graph [
  maxDegree 14
  minDegree 1
  meanDegree 2
  density 0.03333333333333333
  graphCliqueNumber 2
  node [
    id 0
    label "kotek"
    origin "text"
  ]
  node [
    id 1
    label "heheszki"
    origin "text"
  ]
  node [
    id 2
    label "gifnadzis"
    origin "text"
  ]
  node [
    id 3
    label "gif"
    origin "text"
  ]
  node [
    id 4
    label "by&#263;"
    origin "text"
  ]
  node [
    id 5
    label "taki"
    origin "text"
  ]
  node [
    id 6
    label "okre&#347;lenie"
    origin "text"
  ]
  node [
    id 7
    label "prawniczy"
    origin "text"
  ]
  node [
    id 8
    label "dzia&#322;a&#263;"
    origin "text"
  ]
  node [
    id 9
    label "wsp&#243;lnie"
    origin "text"
  ]
  node [
    id 10
    label "porozumienie"
    origin "text"
  ]
  node [
    id 11
    label "kochanie"
  ]
  node [
    id 12
    label "filmik"
  ]
  node [
    id 13
    label "si&#281;ga&#263;"
  ]
  node [
    id 14
    label "trwa&#263;"
  ]
  node [
    id 15
    label "obecno&#347;&#263;"
  ]
  node [
    id 16
    label "stan"
  ]
  node [
    id 17
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 18
    label "stand"
  ]
  node [
    id 19
    label "mie&#263;_miejsce"
  ]
  node [
    id 20
    label "uczestniczy&#263;"
  ]
  node [
    id 21
    label "chodzi&#263;"
  ]
  node [
    id 22
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 23
    label "equal"
  ]
  node [
    id 24
    label "okre&#347;lony"
  ]
  node [
    id 25
    label "jaki&#347;"
  ]
  node [
    id 26
    label "zdecydowanie"
  ]
  node [
    id 27
    label "follow-up"
  ]
  node [
    id 28
    label "appointment"
  ]
  node [
    id 29
    label "ustalenie"
  ]
  node [
    id 30
    label "localization"
  ]
  node [
    id 31
    label "denomination"
  ]
  node [
    id 32
    label "wyra&#380;enie"
  ]
  node [
    id 33
    label "ozdobnik"
  ]
  node [
    id 34
    label "przewidzenie"
  ]
  node [
    id 35
    label "term"
  ]
  node [
    id 36
    label "prawniczo"
  ]
  node [
    id 37
    label "konstytucyjnoprawny"
  ]
  node [
    id 38
    label "jurydyczny"
  ]
  node [
    id 39
    label "urz&#281;dowy"
  ]
  node [
    id 40
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 41
    label "work"
  ]
  node [
    id 42
    label "reakcja_chemiczna"
  ]
  node [
    id 43
    label "function"
  ]
  node [
    id 44
    label "commit"
  ]
  node [
    id 45
    label "bangla&#263;"
  ]
  node [
    id 46
    label "robi&#263;"
  ]
  node [
    id 47
    label "determine"
  ]
  node [
    id 48
    label "tryb"
  ]
  node [
    id 49
    label "powodowa&#263;"
  ]
  node [
    id 50
    label "dziama&#263;"
  ]
  node [
    id 51
    label "istnie&#263;"
  ]
  node [
    id 52
    label "wsp&#243;lny"
  ]
  node [
    id 53
    label "sp&#243;lnie"
  ]
  node [
    id 54
    label "zgoda"
  ]
  node [
    id 55
    label "uk&#322;ad_dayto&#324;ski"
  ]
  node [
    id 56
    label "rozstrzygni&#281;cie"
  ]
  node [
    id 57
    label "umowa"
  ]
  node [
    id 58
    label "agent"
  ]
  node [
    id 59
    label "communication"
  ]
  node [
    id 60
    label "z&#322;oty_blok"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 36
  ]
  edge [
    source 7
    target 37
  ]
  edge [
    source 7
    target 38
  ]
  edge [
    source 7
    target 39
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 19
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 8
    target 46
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 52
  ]
  edge [
    source 9
    target 53
  ]
  edge [
    source 10
    target 54
  ]
  edge [
    source 10
    target 55
  ]
  edge [
    source 10
    target 56
  ]
  edge [
    source 10
    target 57
  ]
  edge [
    source 10
    target 58
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 10
    target 60
  ]
]
