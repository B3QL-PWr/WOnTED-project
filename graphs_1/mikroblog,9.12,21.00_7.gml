graph [
  maxDegree 2
  minDegree 1
  meanDegree 1.6
  density 0.4
  graphCliqueNumber 2
  node [
    id 0
    label "bekazlewactwa"
    origin "text"
  ]
  node [
    id 1
    label "lewackalogika"
    origin "text"
  ]
  node [
    id 2
    label "zakazhandlu"
    origin "text"
  ]
  node [
    id 3
    label "brainlet"
    origin "text"
  ]
  node [
    id 4
    label "heheszki"
    origin "text"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
]
