graph [
  maxDegree 19
  minDegree 1
  meanDegree 1.9130434782608696
  density 0.08695652173913043
  graphCliqueNumber 2
  node [
    id 0
    label "dla"
    origin "text"
  ]
  node [
    id 1
    label "niecierpliwy"
    origin "text"
  ]
  node [
    id 2
    label "akcja"
    origin "text"
  ]
  node [
    id 3
    label "niecierpliwie"
  ]
  node [
    id 4
    label "niespokojny"
  ]
  node [
    id 5
    label "zagrywka"
  ]
  node [
    id 6
    label "czyn"
  ]
  node [
    id 7
    label "czynno&#347;&#263;"
  ]
  node [
    id 8
    label "wysoko&#347;&#263;"
  ]
  node [
    id 9
    label "stock"
  ]
  node [
    id 10
    label "gra"
  ]
  node [
    id 11
    label "w&#281;ze&#322;"
  ]
  node [
    id 12
    label "instrument_strunowy"
  ]
  node [
    id 13
    label "dywidenda"
  ]
  node [
    id 14
    label "przebieg"
  ]
  node [
    id 15
    label "udzia&#322;"
  ]
  node [
    id 16
    label "occupation"
  ]
  node [
    id 17
    label "jazda"
  ]
  node [
    id 18
    label "wydarzenie"
  ]
  node [
    id 19
    label "commotion"
  ]
  node [
    id 20
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 21
    label "sp&#243;&#322;ka_akcyjna"
  ]
  node [
    id 22
    label "operacja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
]
