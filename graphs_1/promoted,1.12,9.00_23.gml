graph [
  maxDegree 8
  minDegree 1
  meanDegree 1.8823529411764706
  density 0.11764705882352941
  graphCliqueNumber 2
  node [
    id 0
    label "brajanek"
    origin "text"
  ]
  node [
    id 1
    label "chcie&#263;"
    origin "text"
  ]
  node [
    id 2
    label "siusiu"
    origin "text"
  ]
  node [
    id 3
    label "karynka"
    origin "text"
  ]
  node [
    id 4
    label "potrafi&#263;"
    origin "text"
  ]
  node [
    id 5
    label "upilnowa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "czu&#263;"
  ]
  node [
    id 7
    label "desire"
  ]
  node [
    id 8
    label "kcie&#263;"
  ]
  node [
    id 9
    label "umie&#263;"
  ]
  node [
    id 10
    label "cope"
  ]
  node [
    id 11
    label "potrafia&#263;"
  ]
  node [
    id 12
    label "posuwa&#263;_si&#281;"
  ]
  node [
    id 13
    label "posun&#261;&#263;_si&#281;"
  ]
  node [
    id 14
    label "can"
  ]
  node [
    id 15
    label "visualize"
  ]
  node [
    id 16
    label "zrobi&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 4
    target 10
  ]
  edge [
    source 4
    target 11
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 5
    target 15
  ]
  edge [
    source 5
    target 16
  ]
]
