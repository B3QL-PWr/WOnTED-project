graph [
  maxDegree 8
  minDegree 1
  meanDegree 1.7777777777777777
  density 0.2222222222222222
  graphCliqueNumber 2
  node [
    id 0
    label "pom&#243;c"
    origin "text"
  ]
  node [
    id 1
    label "&#322;ukasz"
    origin "text"
  ]
  node [
    id 2
    label "help"
  ]
  node [
    id 3
    label "aid"
  ]
  node [
    id 4
    label "u&#322;atwi&#263;"
  ]
  node [
    id 5
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 6
    label "concur"
  ]
  node [
    id 7
    label "zrobi&#263;"
  ]
  node [
    id 8
    label "zaskutkowa&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
]
