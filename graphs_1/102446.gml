graph [
  maxDegree 37
  minDegree 1
  meanDegree 2.0555555555555554
  density 0.011483550589695841
  graphCliqueNumber 2
  node [
    id 0
    label "kilka"
    origin "text"
  ]
  node [
    id 1
    label "tydzie&#324;"
    origin "text"
  ]
  node [
    id 2
    label "temu"
    origin "text"
  ]
  node [
    id 3
    label "prowadzi&#263;by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "mirek"
    origin "text"
  ]
  node [
    id 5
    label "warsztat"
    origin "text"
  ]
  node [
    id 6
    label "kultura"
    origin "text"
  ]
  node [
    id 7
    label "mi&#281;dzyczas"
    origin "text"
  ]
  node [
    id 8
    label "sie&#263;"
    origin "text"
  ]
  node [
    id 9
    label "swoje"
    origin "text"
  ]
  node [
    id 10
    label "relacja"
    origin "text"
  ]
  node [
    id 11
    label "materia&#322;"
    origin "text"
  ]
  node [
    id 12
    label "prezentacja"
    origin "text"
  ]
  node [
    id 13
    label "wrzuci&#263;"
    origin "text"
  ]
  node [
    id 14
    label "dwa"
    origin "text"
  ]
  node [
    id 15
    label "uczestnica"
    origin "text"
  ]
  node [
    id 16
    label "grzegorz"
    origin "text"
  ]
  node [
    id 17
    label "stun&#380;a"
    origin "text"
  ]
  node [
    id 18
    label "pedagog"
    origin "text"
  ]
  node [
    id 19
    label "gda&#324;sk"
    origin "text"
  ]
  node [
    id 20
    label "pracuj&#261;cy"
    origin "text"
  ]
  node [
    id 21
    label "nowa"
    origin "text"
  ]
  node [
    id 22
    label "media"
    origin "text"
  ]
  node [
    id 23
    label "pisz"
    origin "text"
  ]
  node [
    id 24
    label "blog"
    origin "text"
  ]
  node [
    id 25
    label "mi&#281;dzy"
    origin "text"
  ]
  node [
    id 26
    label "inny"
    origin "text"
  ]
  node [
    id 27
    label "&#347;ledziowate"
  ]
  node [
    id 28
    label "ryba"
  ]
  node [
    id 29
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 30
    label "doba"
  ]
  node [
    id 31
    label "czas"
  ]
  node [
    id 32
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 33
    label "weekend"
  ]
  node [
    id 34
    label "miesi&#261;c"
  ]
  node [
    id 35
    label "miejsce"
  ]
  node [
    id 36
    label "sprawno&#347;&#263;"
  ]
  node [
    id 37
    label "spotkanie"
  ]
  node [
    id 38
    label "wyposa&#380;enie"
  ]
  node [
    id 39
    label "st&#243;&#322;_roboczy"
  ]
  node [
    id 40
    label "pracownia"
  ]
  node [
    id 41
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 42
    label "przedmiot"
  ]
  node [
    id 43
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 44
    label "Wsch&#243;d"
  ]
  node [
    id 45
    label "rzecz"
  ]
  node [
    id 46
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 47
    label "sztuka"
  ]
  node [
    id 48
    label "religia"
  ]
  node [
    id 49
    label "przejmowa&#263;"
  ]
  node [
    id 50
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 51
    label "makrokosmos"
  ]
  node [
    id 52
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 53
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 54
    label "zjawisko"
  ]
  node [
    id 55
    label "praca_rolnicza"
  ]
  node [
    id 56
    label "tradycja"
  ]
  node [
    id 57
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 58
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 59
    label "przejmowanie"
  ]
  node [
    id 60
    label "cecha"
  ]
  node [
    id 61
    label "asymilowanie_si&#281;"
  ]
  node [
    id 62
    label "przej&#261;&#263;"
  ]
  node [
    id 63
    label "hodowla"
  ]
  node [
    id 64
    label "brzoskwiniarnia"
  ]
  node [
    id 65
    label "populace"
  ]
  node [
    id 66
    label "konwencja"
  ]
  node [
    id 67
    label "propriety"
  ]
  node [
    id 68
    label "jako&#347;&#263;"
  ]
  node [
    id 69
    label "kuchnia"
  ]
  node [
    id 70
    label "zwyczaj"
  ]
  node [
    id 71
    label "przej&#281;cie"
  ]
  node [
    id 72
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 73
    label "hipertekst"
  ]
  node [
    id 74
    label "gauze"
  ]
  node [
    id 75
    label "nitka"
  ]
  node [
    id 76
    label "mesh"
  ]
  node [
    id 77
    label "e-hazard"
  ]
  node [
    id 78
    label "netbook"
  ]
  node [
    id 79
    label "cyberprzestrze&#324;"
  ]
  node [
    id 80
    label "biznes_elektroniczny"
  ]
  node [
    id 81
    label "snu&#263;"
  ]
  node [
    id 82
    label "organization"
  ]
  node [
    id 83
    label "zasadzka"
  ]
  node [
    id 84
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 85
    label "web"
  ]
  node [
    id 86
    label "provider"
  ]
  node [
    id 87
    label "struktura"
  ]
  node [
    id 88
    label "us&#322;uga_internetowa"
  ]
  node [
    id 89
    label "punkt_dost&#281;pu"
  ]
  node [
    id 90
    label "organizacja"
  ]
  node [
    id 91
    label "mem"
  ]
  node [
    id 92
    label "vane"
  ]
  node [
    id 93
    label "podcast"
  ]
  node [
    id 94
    label "grooming"
  ]
  node [
    id 95
    label "kszta&#322;t"
  ]
  node [
    id 96
    label "strona"
  ]
  node [
    id 97
    label "obiekt"
  ]
  node [
    id 98
    label "wysnu&#263;"
  ]
  node [
    id 99
    label "gra_sieciowa"
  ]
  node [
    id 100
    label "instalacja"
  ]
  node [
    id 101
    label "sie&#263;_komputerowa"
  ]
  node [
    id 102
    label "net"
  ]
  node [
    id 103
    label "plecionka"
  ]
  node [
    id 104
    label "rozmieszczenie"
  ]
  node [
    id 105
    label "wypowied&#378;"
  ]
  node [
    id 106
    label "message"
  ]
  node [
    id 107
    label "podzbi&#243;r"
  ]
  node [
    id 108
    label "iloczyn_kartezja&#324;ski"
  ]
  node [
    id 109
    label "ustosunkowywa&#263;"
  ]
  node [
    id 110
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 111
    label "bratnia_dusza"
  ]
  node [
    id 112
    label "zwi&#261;zanie"
  ]
  node [
    id 113
    label "ustosunkowanie"
  ]
  node [
    id 114
    label "ustosunkowywanie"
  ]
  node [
    id 115
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 116
    label "zwi&#261;za&#263;"
  ]
  node [
    id 117
    label "ustosunkowa&#263;"
  ]
  node [
    id 118
    label "niezgodno&#347;&#263;"
  ]
  node [
    id 119
    label "korespondent"
  ]
  node [
    id 120
    label "marriage"
  ]
  node [
    id 121
    label "wi&#261;zanie"
  ]
  node [
    id 122
    label "trasa"
  ]
  node [
    id 123
    label "zwi&#261;zek"
  ]
  node [
    id 124
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 125
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 126
    label "sprawko"
  ]
  node [
    id 127
    label "krajka"
  ]
  node [
    id 128
    label "cz&#322;owiek"
  ]
  node [
    id 129
    label "tworzywo"
  ]
  node [
    id 130
    label "krajalno&#347;&#263;"
  ]
  node [
    id 131
    label "archiwum"
  ]
  node [
    id 132
    label "kandydat"
  ]
  node [
    id 133
    label "bielarnia"
  ]
  node [
    id 134
    label "rozrywa&#263;_si&#281;"
  ]
  node [
    id 135
    label "dane"
  ]
  node [
    id 136
    label "materia"
  ]
  node [
    id 137
    label "wyr&#243;b_w&#322;&#243;kienniczy"
  ]
  node [
    id 138
    label "substancja"
  ]
  node [
    id 139
    label "nawil&#380;arka"
  ]
  node [
    id 140
    label "dyspozycja"
  ]
  node [
    id 141
    label "pokaz&#243;wka"
  ]
  node [
    id 142
    label "szkolenie"
  ]
  node [
    id 143
    label "informacja"
  ]
  node [
    id 144
    label "impreza"
  ]
  node [
    id 145
    label "komunikat"
  ]
  node [
    id 146
    label "grafika_u&#380;ytkowa"
  ]
  node [
    id 147
    label "prezenter"
  ]
  node [
    id 148
    label "show"
  ]
  node [
    id 149
    label "insert"
  ]
  node [
    id 150
    label "umie&#347;ci&#263;"
  ]
  node [
    id 151
    label "profesor"
  ]
  node [
    id 152
    label "J&#281;drzejewicz"
  ]
  node [
    id 153
    label "kszta&#322;ciciel"
  ]
  node [
    id 154
    label "szkolnik"
  ]
  node [
    id 155
    label "Friedrich_Fr&#246;bel"
  ]
  node [
    id 156
    label "preceptor"
  ]
  node [
    id 157
    label "John_Dewey"
  ]
  node [
    id 158
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 159
    label "specjalista_od_nauk_spo&#322;ecznych"
  ]
  node [
    id 160
    label "nauczyciel"
  ]
  node [
    id 161
    label "popularyzator"
  ]
  node [
    id 162
    label "belfer"
  ]
  node [
    id 163
    label "gwiazda"
  ]
  node [
    id 164
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 165
    label "przekazior"
  ]
  node [
    id 166
    label "mass-media"
  ]
  node [
    id 167
    label "uzbrajanie"
  ]
  node [
    id 168
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 169
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 170
    label "medium"
  ]
  node [
    id 171
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 172
    label "komcio"
  ]
  node [
    id 173
    label "blogosfera"
  ]
  node [
    id 174
    label "pami&#281;tnik"
  ]
  node [
    id 175
    label "kolejny"
  ]
  node [
    id 176
    label "inaczej"
  ]
  node [
    id 177
    label "r&#243;&#380;ny"
  ]
  node [
    id 178
    label "inszy"
  ]
  node [
    id 179
    label "osobno"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 31
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 13
  ]
  edge [
    source 8
    target 14
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 22
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 105
  ]
  edge [
    source 10
    target 106
  ]
  edge [
    source 10
    target 107
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 10
    target 110
  ]
  edge [
    source 10
    target 111
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 10
    target 121
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 10
    target 125
  ]
  edge [
    source 10
    target 126
  ]
  edge [
    source 10
    target 17
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 127
  ]
  edge [
    source 11
    target 128
  ]
  edge [
    source 11
    target 129
  ]
  edge [
    source 11
    target 130
  ]
  edge [
    source 11
    target 131
  ]
  edge [
    source 11
    target 132
  ]
  edge [
    source 11
    target 133
  ]
  edge [
    source 11
    target 134
  ]
  edge [
    source 11
    target 135
  ]
  edge [
    source 11
    target 136
  ]
  edge [
    source 11
    target 137
  ]
  edge [
    source 11
    target 138
  ]
  edge [
    source 11
    target 139
  ]
  edge [
    source 11
    target 140
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 12
    target 141
  ]
  edge [
    source 12
    target 142
  ]
  edge [
    source 12
    target 143
  ]
  edge [
    source 12
    target 144
  ]
  edge [
    source 12
    target 145
  ]
  edge [
    source 12
    target 146
  ]
  edge [
    source 12
    target 147
  ]
  edge [
    source 12
    target 148
  ]
  edge [
    source 13
    target 149
  ]
  edge [
    source 13
    target 150
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 151
  ]
  edge [
    source 18
    target 152
  ]
  edge [
    source 18
    target 153
  ]
  edge [
    source 18
    target 154
  ]
  edge [
    source 18
    target 155
  ]
  edge [
    source 18
    target 156
  ]
  edge [
    source 18
    target 157
  ]
  edge [
    source 18
    target 158
  ]
  edge [
    source 18
    target 159
  ]
  edge [
    source 18
    target 160
  ]
  edge [
    source 18
    target 161
  ]
  edge [
    source 18
    target 162
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 163
  ]
  edge [
    source 21
    target 164
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 165
  ]
  edge [
    source 22
    target 166
  ]
  edge [
    source 22
    target 167
  ]
  edge [
    source 22
    target 168
  ]
  edge [
    source 22
    target 169
  ]
  edge [
    source 22
    target 170
  ]
  edge [
    source 22
    target 171
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 172
  ]
  edge [
    source 24
    target 96
  ]
  edge [
    source 24
    target 173
  ]
  edge [
    source 24
    target 174
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 175
  ]
  edge [
    source 26
    target 176
  ]
  edge [
    source 26
    target 177
  ]
  edge [
    source 26
    target 178
  ]
  edge [
    source 26
    target 179
  ]
]
