graph [
  maxDegree 32
  minDegree 1
  meanDegree 1.9833333333333334
  density 0.016666666666666666
  graphCliqueNumber 2
  node [
    id 0
    label "normalna"
    origin "text"
  ]
  node [
    id 1
    label "mama"
    origin "text"
  ]
  node [
    id 2
    label "zabra&#263;"
    origin "text"
  ]
  node [
    id 3
    label "picie"
    origin "text"
  ]
  node [
    id 4
    label "alkohol"
    origin "text"
  ]
  node [
    id 5
    label "mimo"
    origin "text"
  ]
  node [
    id 6
    label "sko&#324;czy&#263;by&#263;"
    origin "text"
  ]
  node [
    id 7
    label "lato"
    origin "text"
  ]
  node [
    id 8
    label "by&#263;"
    origin "text"
  ]
  node [
    id 9
    label "wed&#322;ug"
    origin "text"
  ]
  node [
    id 10
    label "chcie&#263;byby&#263;"
    origin "text"
  ]
  node [
    id 11
    label "zaprosi&#263;"
    origin "text"
  ]
  node [
    id 12
    label "kole&#380;anka"
    origin "text"
  ]
  node [
    id 13
    label "wypi&#263;"
    origin "text"
  ]
  node [
    id 14
    label "zwyk&#322;y"
    origin "text"
  ]
  node [
    id 15
    label "prosta"
  ]
  node [
    id 16
    label "matczysko"
  ]
  node [
    id 17
    label "macierz"
  ]
  node [
    id 18
    label "przodkini"
  ]
  node [
    id 19
    label "Matka_Boska"
  ]
  node [
    id 20
    label "macocha"
  ]
  node [
    id 21
    label "matka_zast&#281;pcza"
  ]
  node [
    id 22
    label "stara"
  ]
  node [
    id 23
    label "rodzice"
  ]
  node [
    id 24
    label "rodzic"
  ]
  node [
    id 25
    label "consume"
  ]
  node [
    id 26
    label "zaj&#261;&#263;"
  ]
  node [
    id 27
    label "wzi&#261;&#263;"
  ]
  node [
    id 28
    label "przenie&#347;&#263;"
  ]
  node [
    id 29
    label "spowodowa&#263;"
  ]
  node [
    id 30
    label "z&#322;apa&#263;"
  ]
  node [
    id 31
    label "przesun&#261;&#263;"
  ]
  node [
    id 32
    label "deprive"
  ]
  node [
    id 33
    label "uda&#263;_si&#281;"
  ]
  node [
    id 34
    label "poci&#261;gn&#261;&#263;"
  ]
  node [
    id 35
    label "abstract"
  ]
  node [
    id 36
    label "withdraw"
  ]
  node [
    id 37
    label "doprowadzi&#263;"
  ]
  node [
    id 38
    label "obci&#261;ganie"
  ]
  node [
    id 39
    label "od&#380;ywianie_si&#281;"
  ]
  node [
    id 40
    label "wmuszanie"
  ]
  node [
    id 41
    label "psychoza_alkoholowa"
  ]
  node [
    id 42
    label "naoliwianie_si&#281;"
  ]
  node [
    id 43
    label "rozpijanie"
  ]
  node [
    id 44
    label "pijanie"
  ]
  node [
    id 45
    label "na&#322;&#243;g"
  ]
  node [
    id 46
    label "zapicie"
  ]
  node [
    id 47
    label "disulfiram"
  ]
  node [
    id 48
    label "ciecz"
  ]
  node [
    id 49
    label "pija&#324;stwo"
  ]
  node [
    id 50
    label "wys&#261;czanie"
  ]
  node [
    id 51
    label "zatruwanie_si&#281;"
  ]
  node [
    id 52
    label "upicie_si&#281;"
  ]
  node [
    id 53
    label "upijanie_si&#281;"
  ]
  node [
    id 54
    label "substancja"
  ]
  node [
    id 55
    label "ufetowanie_si&#281;"
  ]
  node [
    id 56
    label "przep&#322;ukiwanie_gard&#322;a"
  ]
  node [
    id 57
    label "opijanie"
  ]
  node [
    id 58
    label "pojenie"
  ]
  node [
    id 59
    label "przepicie"
  ]
  node [
    id 60
    label "smakowanie"
  ]
  node [
    id 61
    label "zapijanie"
  ]
  node [
    id 62
    label "rozpicie"
  ]
  node [
    id 63
    label "schorzenie"
  ]
  node [
    id 64
    label "przepicie_si&#281;"
  ]
  node [
    id 65
    label "wypitek"
  ]
  node [
    id 66
    label "golenie"
  ]
  node [
    id 67
    label "drink"
  ]
  node [
    id 68
    label "gorzelnia_rolnicza"
  ]
  node [
    id 69
    label "upija&#263;"
  ]
  node [
    id 70
    label "szk&#322;o"
  ]
  node [
    id 71
    label "spirytualia"
  ]
  node [
    id 72
    label "nap&#243;j"
  ]
  node [
    id 73
    label "wypicie"
  ]
  node [
    id 74
    label "poniewierca"
  ]
  node [
    id 75
    label "rozgrzewacz"
  ]
  node [
    id 76
    label "upajanie"
  ]
  node [
    id 77
    label "piwniczka"
  ]
  node [
    id 78
    label "najebka"
  ]
  node [
    id 79
    label "grupa_hydroksylowa"
  ]
  node [
    id 80
    label "le&#380;akownia"
  ]
  node [
    id 81
    label "g&#322;owa"
  ]
  node [
    id 82
    label "upi&#263;"
  ]
  node [
    id 83
    label "upojenie"
  ]
  node [
    id 84
    label "likwor"
  ]
  node [
    id 85
    label "u&#380;ywka"
  ]
  node [
    id 86
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 87
    label "alko"
  ]
  node [
    id 88
    label "pora_roku"
  ]
  node [
    id 89
    label "si&#281;ga&#263;"
  ]
  node [
    id 90
    label "trwa&#263;"
  ]
  node [
    id 91
    label "obecno&#347;&#263;"
  ]
  node [
    id 92
    label "stan"
  ]
  node [
    id 93
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 94
    label "stand"
  ]
  node [
    id 95
    label "mie&#263;_miejsce"
  ]
  node [
    id 96
    label "uczestniczy&#263;"
  ]
  node [
    id 97
    label "chodzi&#263;"
  ]
  node [
    id 98
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 99
    label "equal"
  ]
  node [
    id 100
    label "zaproponowa&#263;"
  ]
  node [
    id 101
    label "invite"
  ]
  node [
    id 102
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 103
    label "ask"
  ]
  node [
    id 104
    label "kuma"
  ]
  node [
    id 105
    label "spo&#380;y&#263;"
  ]
  node [
    id 106
    label "dostarczy&#263;"
  ]
  node [
    id 107
    label "wychla&#263;"
  ]
  node [
    id 108
    label "wy&#322;oi&#263;"
  ]
  node [
    id 109
    label "obci&#261;gn&#261;&#263;"
  ]
  node [
    id 110
    label "give_birth"
  ]
  node [
    id 111
    label "naoliwi&#263;_si&#281;"
  ]
  node [
    id 112
    label "zrobi&#263;"
  ]
  node [
    id 113
    label "wch&#322;on&#261;&#263;"
  ]
  node [
    id 114
    label "napi&#263;_si&#281;"
  ]
  node [
    id 115
    label "zwyczajnie"
  ]
  node [
    id 116
    label "okre&#347;lony"
  ]
  node [
    id 117
    label "zwykle"
  ]
  node [
    id 118
    label "przeci&#281;tny"
  ]
  node [
    id 119
    label "cz&#281;sty"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 100
  ]
  edge [
    source 11
    target 101
  ]
  edge [
    source 11
    target 102
  ]
  edge [
    source 11
    target 103
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 105
  ]
  edge [
    source 13
    target 106
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 13
    target 113
  ]
  edge [
    source 13
    target 114
  ]
  edge [
    source 14
    target 115
  ]
  edge [
    source 14
    target 116
  ]
  edge [
    source 14
    target 117
  ]
  edge [
    source 14
    target 118
  ]
  edge [
    source 14
    target 119
  ]
]
