graph [
  maxDegree 593
  minDegree 1
  meanDegree 2.013953488372093
  density 0.0023445325825053468
  graphCliqueNumber 2
  node [
    id 0
    label "gospodarz"
    origin "text"
  ]
  node [
    id 1
    label "szczyt"
    origin "text"
  ]
  node [
    id 2
    label "klimatyczny"
    origin "text"
  ]
  node [
    id 3
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 4
    label "obradowa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "temat"
    origin "text"
  ]
  node [
    id 6
    label "zmiana"
    origin "text"
  ]
  node [
    id 7
    label "klimat"
    origin "text"
  ]
  node [
    id 8
    label "dzia&#322;anie"
    origin "text"
  ]
  node [
    id 9
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 10
    label "wdro&#380;y&#263;"
    origin "text"
  ]
  node [
    id 11
    label "aby"
    origin "text"
  ]
  node [
    id 12
    label "zapobiec"
    origin "text"
  ]
  node [
    id 13
    label "daleki"
    origin "text"
  ]
  node [
    id 14
    label "degradacja"
    origin "text"
  ]
  node [
    id 15
    label "&#347;rodowisko"
    origin "text"
  ]
  node [
    id 16
    label "naturalny"
    origin "text"
  ]
  node [
    id 17
    label "pawilon"
    origin "text"
  ]
  node [
    id 18
    label "miasto"
    origin "text"
  ]
  node [
    id 19
    label "ustawa"
    origin "text"
  ]
  node [
    id 20
    label "kapliczka"
    origin "text"
  ]
  node [
    id 21
    label "w&#281;giel"
    origin "text"
  ]
  node [
    id 22
    label "organizm"
  ]
  node [
    id 23
    label "opiekun"
  ]
  node [
    id 24
    label "g&#322;owa_domu"
  ]
  node [
    id 25
    label "rolnik"
  ]
  node [
    id 26
    label "wie&#347;niak"
  ]
  node [
    id 27
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 28
    label "zarz&#261;dca"
  ]
  node [
    id 29
    label "m&#261;&#380;"
  ]
  node [
    id 30
    label "organizator"
  ]
  node [
    id 31
    label "Lubogoszcz"
  ]
  node [
    id 32
    label "koniec"
  ]
  node [
    id 33
    label "wierch"
  ]
  node [
    id 34
    label "czas"
  ]
  node [
    id 35
    label "&#321;omnica"
  ]
  node [
    id 36
    label "Magura"
  ]
  node [
    id 37
    label "Wo&#322;ek"
  ]
  node [
    id 38
    label "Wielki_Chocz"
  ]
  node [
    id 39
    label "Turbacz"
  ]
  node [
    id 40
    label "Walig&#243;ra"
  ]
  node [
    id 41
    label "Orlica"
  ]
  node [
    id 42
    label "korona"
  ]
  node [
    id 43
    label "bok"
  ]
  node [
    id 44
    label "Jaworzyna"
  ]
  node [
    id 45
    label "Groniczki"
  ]
  node [
    id 46
    label "Radunia"
  ]
  node [
    id 47
    label "&#346;winica"
  ]
  node [
    id 48
    label "Okr&#261;glica"
  ]
  node [
    id 49
    label "Beskid"
  ]
  node [
    id 50
    label "poziom"
  ]
  node [
    id 51
    label "wzmo&#380;enie"
  ]
  node [
    id 52
    label "Czupel"
  ]
  node [
    id 53
    label "fasada"
  ]
  node [
    id 54
    label "Rysianka"
  ]
  node [
    id 55
    label "g&#243;ra"
  ]
  node [
    id 56
    label "Jaworz"
  ]
  node [
    id 57
    label "Rudawiec"
  ]
  node [
    id 58
    label "Che&#322;miec"
  ]
  node [
    id 59
    label "zwie&#324;czenie"
  ]
  node [
    id 60
    label "Wielki_Bukowiec"
  ]
  node [
    id 61
    label "wzniesienie"
  ]
  node [
    id 62
    label "godzina_szczytu"
  ]
  node [
    id 63
    label "summit"
  ]
  node [
    id 64
    label "Wielka_Racza"
  ]
  node [
    id 65
    label "wierzcho&#322;"
  ]
  node [
    id 66
    label "&#346;nie&#380;nik"
  ]
  node [
    id 67
    label "&#347;ciana"
  ]
  node [
    id 68
    label "Barania_G&#243;ra"
  ]
  node [
    id 69
    label "Ko&#322;owr&#243;t"
  ]
  node [
    id 70
    label "Ja&#322;owiec"
  ]
  node [
    id 71
    label "Wielka_Sowa"
  ]
  node [
    id 72
    label "wierzcho&#322;ek"
  ]
  node [
    id 73
    label "Obidowa"
  ]
  node [
    id 74
    label "konferencja"
  ]
  node [
    id 75
    label "Cubryna"
  ]
  node [
    id 76
    label "Szrenica"
  ]
  node [
    id 77
    label "Czarna_G&#243;ra"
  ]
  node [
    id 78
    label "Mody&#324;"
  ]
  node [
    id 79
    label "&#346;l&#281;&#380;a"
  ]
  node [
    id 80
    label "nastrojowo"
  ]
  node [
    id 81
    label "atmospheric"
  ]
  node [
    id 82
    label "klimatycznie"
  ]
  node [
    id 83
    label "ucieszy&#263;"
  ]
  node [
    id 84
    label "dyskutowa&#263;"
  ]
  node [
    id 85
    label "sit"
  ]
  node [
    id 86
    label "fraza"
  ]
  node [
    id 87
    label "forma"
  ]
  node [
    id 88
    label "melodia"
  ]
  node [
    id 89
    label "rzecz"
  ]
  node [
    id 90
    label "zbacza&#263;"
  ]
  node [
    id 91
    label "entity"
  ]
  node [
    id 92
    label "omawia&#263;"
  ]
  node [
    id 93
    label "topik"
  ]
  node [
    id 94
    label "wyraz_pochodny"
  ]
  node [
    id 95
    label "om&#243;wi&#263;"
  ]
  node [
    id 96
    label "omawianie"
  ]
  node [
    id 97
    label "w&#261;tek"
  ]
  node [
    id 98
    label "forum"
  ]
  node [
    id 99
    label "cecha"
  ]
  node [
    id 100
    label "zboczenie"
  ]
  node [
    id 101
    label "zbaczanie"
  ]
  node [
    id 102
    label "tre&#347;&#263;"
  ]
  node [
    id 103
    label "tematyka"
  ]
  node [
    id 104
    label "sprawa"
  ]
  node [
    id 105
    label "istota"
  ]
  node [
    id 106
    label "otoczka"
  ]
  node [
    id 107
    label "zboczy&#263;"
  ]
  node [
    id 108
    label "om&#243;wienie"
  ]
  node [
    id 109
    label "anatomopatolog"
  ]
  node [
    id 110
    label "rewizja"
  ]
  node [
    id 111
    label "oznaka"
  ]
  node [
    id 112
    label "ferment"
  ]
  node [
    id 113
    label "komplet"
  ]
  node [
    id 114
    label "tura"
  ]
  node [
    id 115
    label "amendment"
  ]
  node [
    id 116
    label "zmianka"
  ]
  node [
    id 117
    label "odmienianie"
  ]
  node [
    id 118
    label "passage"
  ]
  node [
    id 119
    label "zjawisko"
  ]
  node [
    id 120
    label "change"
  ]
  node [
    id 121
    label "praca"
  ]
  node [
    id 122
    label "zesp&#243;&#322;"
  ]
  node [
    id 123
    label "styl"
  ]
  node [
    id 124
    label "atmosfera"
  ]
  node [
    id 125
    label "nakr&#281;cenie"
  ]
  node [
    id 126
    label "cz&#322;owiek"
  ]
  node [
    id 127
    label "robienie"
  ]
  node [
    id 128
    label "infimum"
  ]
  node [
    id 129
    label "hipnotyzowanie"
  ]
  node [
    id 130
    label "bycie"
  ]
  node [
    id 131
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 132
    label "jednostka"
  ]
  node [
    id 133
    label "uruchamianie"
  ]
  node [
    id 134
    label "kampania"
  ]
  node [
    id 135
    label "w&#322;&#261;czanie"
  ]
  node [
    id 136
    label "operacja"
  ]
  node [
    id 137
    label "operation"
  ]
  node [
    id 138
    label "supremum"
  ]
  node [
    id 139
    label "kres"
  ]
  node [
    id 140
    label "zako&#324;czenie"
  ]
  node [
    id 141
    label "funkcja"
  ]
  node [
    id 142
    label "skutek"
  ]
  node [
    id 143
    label "dzianie_si&#281;"
  ]
  node [
    id 144
    label "liczy&#263;"
  ]
  node [
    id 145
    label "zadzia&#322;anie"
  ]
  node [
    id 146
    label "podzia&#322;anie"
  ]
  node [
    id 147
    label "rzut"
  ]
  node [
    id 148
    label "czynny"
  ]
  node [
    id 149
    label "liczenie"
  ]
  node [
    id 150
    label "czynno&#347;&#263;"
  ]
  node [
    id 151
    label "wdzieranie_si&#281;"
  ]
  node [
    id 152
    label "rozpocz&#281;cie"
  ]
  node [
    id 153
    label "docieranie"
  ]
  node [
    id 154
    label "wp&#322;yw"
  ]
  node [
    id 155
    label "podtrzymywanie"
  ]
  node [
    id 156
    label "nakr&#281;canie"
  ]
  node [
    id 157
    label "uruchomienie"
  ]
  node [
    id 158
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 159
    label "impact"
  ]
  node [
    id 160
    label "tr&#243;jstronny"
  ]
  node [
    id 161
    label "matematyka"
  ]
  node [
    id 162
    label "reakcja_chemiczna"
  ]
  node [
    id 163
    label "act"
  ]
  node [
    id 164
    label "priorytet"
  ]
  node [
    id 165
    label "w&#322;&#261;czenie"
  ]
  node [
    id 166
    label "natural_process"
  ]
  node [
    id 167
    label "zatrzymanie"
  ]
  node [
    id 168
    label "rezultat"
  ]
  node [
    id 169
    label "powodowanie"
  ]
  node [
    id 170
    label "oferta"
  ]
  node [
    id 171
    label "free"
  ]
  node [
    id 172
    label "establish"
  ]
  node [
    id 173
    label "zaszczepiciel"
  ]
  node [
    id 174
    label "zacz&#261;&#263;"
  ]
  node [
    id 175
    label "begin"
  ]
  node [
    id 176
    label "nak&#322;oni&#263;"
  ]
  node [
    id 177
    label "wrazi&#263;"
  ]
  node [
    id 178
    label "nauczy&#263;"
  ]
  node [
    id 179
    label "przyzwyczai&#263;"
  ]
  node [
    id 180
    label "wprowadzi&#263;"
  ]
  node [
    id 181
    label "initiate"
  ]
  node [
    id 182
    label "troch&#281;"
  ]
  node [
    id 183
    label "zapobie&#380;e&#263;"
  ]
  node [
    id 184
    label "zrobi&#263;"
  ]
  node [
    id 185
    label "cook"
  ]
  node [
    id 186
    label "dawny"
  ]
  node [
    id 187
    label "du&#380;y"
  ]
  node [
    id 188
    label "s&#322;aby"
  ]
  node [
    id 189
    label "oddalony"
  ]
  node [
    id 190
    label "daleko"
  ]
  node [
    id 191
    label "przysz&#322;y"
  ]
  node [
    id 192
    label "ogl&#281;dny"
  ]
  node [
    id 193
    label "r&#243;&#380;ny"
  ]
  node [
    id 194
    label "g&#322;&#281;boki"
  ]
  node [
    id 195
    label "odlegle"
  ]
  node [
    id 196
    label "nieobecny"
  ]
  node [
    id 197
    label "odleg&#322;y"
  ]
  node [
    id 198
    label "d&#322;ugi"
  ]
  node [
    id 199
    label "zwi&#261;zany"
  ]
  node [
    id 200
    label "obcy"
  ]
  node [
    id 201
    label "reduction"
  ]
  node [
    id 202
    label "kariera"
  ]
  node [
    id 203
    label "zdegradowanie"
  ]
  node [
    id 204
    label "downfall"
  ]
  node [
    id 205
    label "proces_chemiczny"
  ]
  node [
    id 206
    label "spadek"
  ]
  node [
    id 207
    label "pogorszenie"
  ]
  node [
    id 208
    label "degradowanie"
  ]
  node [
    id 209
    label "analiza"
  ]
  node [
    id 210
    label "kara"
  ]
  node [
    id 211
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 212
    label "obiekt_naturalny"
  ]
  node [
    id 213
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 214
    label "grupa"
  ]
  node [
    id 215
    label "stw&#243;r"
  ]
  node [
    id 216
    label "environment"
  ]
  node [
    id 217
    label "biota"
  ]
  node [
    id 218
    label "wszechstworzenie"
  ]
  node [
    id 219
    label "otoczenie"
  ]
  node [
    id 220
    label "fauna"
  ]
  node [
    id 221
    label "ekosystem"
  ]
  node [
    id 222
    label "teren"
  ]
  node [
    id 223
    label "mikrokosmos"
  ]
  node [
    id 224
    label "class"
  ]
  node [
    id 225
    label "warunki"
  ]
  node [
    id 226
    label "huczek"
  ]
  node [
    id 227
    label "Ziemia"
  ]
  node [
    id 228
    label "woda"
  ]
  node [
    id 229
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 230
    label "szczery"
  ]
  node [
    id 231
    label "prawy"
  ]
  node [
    id 232
    label "bezsporny"
  ]
  node [
    id 233
    label "organicznie"
  ]
  node [
    id 234
    label "immanentny"
  ]
  node [
    id 235
    label "naturalnie"
  ]
  node [
    id 236
    label "zwyczajny"
  ]
  node [
    id 237
    label "neutralny"
  ]
  node [
    id 238
    label "zrozumia&#322;y"
  ]
  node [
    id 239
    label "rzeczywisty"
  ]
  node [
    id 240
    label "normalny"
  ]
  node [
    id 241
    label "pierwotny"
  ]
  node [
    id 242
    label "budynek"
  ]
  node [
    id 243
    label "skrzyd&#322;o"
  ]
  node [
    id 244
    label "Brac&#322;aw"
  ]
  node [
    id 245
    label "Zb&#261;szy&#324;"
  ]
  node [
    id 246
    label "G&#322;uch&#243;w"
  ]
  node [
    id 247
    label "Hallstatt"
  ]
  node [
    id 248
    label "Zbara&#380;"
  ]
  node [
    id 249
    label "Wielkie_Tyrnowo"
  ]
  node [
    id 250
    label "Nachiczewan"
  ]
  node [
    id 251
    label "Suworow"
  ]
  node [
    id 252
    label "Halicz"
  ]
  node [
    id 253
    label "Gandawa"
  ]
  node [
    id 254
    label "Kamionka_Strumi&#322;owa"
  ]
  node [
    id 255
    label "Wismar"
  ]
  node [
    id 256
    label "Norymberga"
  ]
  node [
    id 257
    label "Ruciane-Nida"
  ]
  node [
    id 258
    label "Wia&#378;ma"
  ]
  node [
    id 259
    label "Sewilla"
  ]
  node [
    id 260
    label "Pietropaw&#322;owsk"
  ]
  node [
    id 261
    label "Kobry&#324;"
  ]
  node [
    id 262
    label "Brno"
  ]
  node [
    id 263
    label "Tomsk"
  ]
  node [
    id 264
    label "Poniatowa"
  ]
  node [
    id 265
    label "Hadziacz"
  ]
  node [
    id 266
    label "Tiume&#324;"
  ]
  node [
    id 267
    label "Karlsbad"
  ]
  node [
    id 268
    label "Drohobycz"
  ]
  node [
    id 269
    label "Lyon"
  ]
  node [
    id 270
    label "Dzier&#380;y&#324;sk"
  ]
  node [
    id 271
    label "K&#322;odawa"
  ]
  node [
    id 272
    label "Solikamsk"
  ]
  node [
    id 273
    label "Wolgast"
  ]
  node [
    id 274
    label "Saloniki"
  ]
  node [
    id 275
    label "Lw&#243;w"
  ]
  node [
    id 276
    label "Al-Kufa"
  ]
  node [
    id 277
    label "Hamburg"
  ]
  node [
    id 278
    label "Bogusz&#243;w-Gorce"
  ]
  node [
    id 279
    label "Nampula"
  ]
  node [
    id 280
    label "burmistrz"
  ]
  node [
    id 281
    label "D&#252;sseldorf"
  ]
  node [
    id 282
    label "Nowy_Orlean"
  ]
  node [
    id 283
    label "Bamberg"
  ]
  node [
    id 284
    label "Osaka"
  ]
  node [
    id 285
    label "Michalovce"
  ]
  node [
    id 286
    label "Niko&#322;ajewsk_nad_Amurem"
  ]
  node [
    id 287
    label "Fryburg"
  ]
  node [
    id 288
    label "Trabzon"
  ]
  node [
    id 289
    label "Wersal"
  ]
  node [
    id 290
    label "Swatowe"
  ]
  node [
    id 291
    label "Ka&#322;uga"
  ]
  node [
    id 292
    label "Dijon"
  ]
  node [
    id 293
    label "Cannes"
  ]
  node [
    id 294
    label "Borowsk"
  ]
  node [
    id 295
    label "Kursk"
  ]
  node [
    id 296
    label "Tyberiada"
  ]
  node [
    id 297
    label "Boden"
  ]
  node [
    id 298
    label "Dodona"
  ]
  node [
    id 299
    label "Vukovar"
  ]
  node [
    id 300
    label "Soleczniki"
  ]
  node [
    id 301
    label "Barcelona"
  ]
  node [
    id 302
    label "Oszmiana"
  ]
  node [
    id 303
    label "Stuttgart"
  ]
  node [
    id 304
    label "Nerczy&#324;sk"
  ]
  node [
    id 305
    label "Essen"
  ]
  node [
    id 306
    label "Bijsk"
  ]
  node [
    id 307
    label "Luboml"
  ]
  node [
    id 308
    label "Gr&#243;dek"
  ]
  node [
    id 309
    label "Orany"
  ]
  node [
    id 310
    label "Siedliszcze"
  ]
  node [
    id 311
    label "P&#322;owdiw"
  ]
  node [
    id 312
    label "A&#322;apajewsk"
  ]
  node [
    id 313
    label "Liverpool"
  ]
  node [
    id 314
    label "Ostrawa"
  ]
  node [
    id 315
    label "Penza"
  ]
  node [
    id 316
    label "Rudki"
  ]
  node [
    id 317
    label "Aktobe"
  ]
  node [
    id 318
    label "I&#322;awka"
  ]
  node [
    id 319
    label "Tolkmicko"
  ]
  node [
    id 320
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 321
    label "Sajgon"
  ]
  node [
    id 322
    label "Windawa"
  ]
  node [
    id 323
    label "Weimar"
  ]
  node [
    id 324
    label "Jekaterynburg"
  ]
  node [
    id 325
    label "Lejda"
  ]
  node [
    id 326
    label "Cremona"
  ]
  node [
    id 327
    label "Wo&#322;godo&#324;sk"
  ]
  node [
    id 328
    label "Kordoba"
  ]
  node [
    id 329
    label "urz&#261;d"
  ]
  node [
    id 330
    label "&#321;ohojsk"
  ]
  node [
    id 331
    label "Kalmar"
  ]
  node [
    id 332
    label "Akerman"
  ]
  node [
    id 333
    label "Locarno"
  ]
  node [
    id 334
    label "Bych&#243;w"
  ]
  node [
    id 335
    label "Toledo"
  ]
  node [
    id 336
    label "Minusi&#324;sk"
  ]
  node [
    id 337
    label "Szk&#322;&#243;w"
  ]
  node [
    id 338
    label "Wenecja"
  ]
  node [
    id 339
    label "Bazylea"
  ]
  node [
    id 340
    label "Peszt"
  ]
  node [
    id 341
    label "Piza"
  ]
  node [
    id 342
    label "Tanger"
  ]
  node [
    id 343
    label "Krzywi&#324;"
  ]
  node [
    id 344
    label "Eger"
  ]
  node [
    id 345
    label "Bogus&#322;aw"
  ]
  node [
    id 346
    label "Taganrog"
  ]
  node [
    id 347
    label "Oksford"
  ]
  node [
    id 348
    label "Gwardiejsk"
  ]
  node [
    id 349
    label "Tyraspol"
  ]
  node [
    id 350
    label "Kleczew"
  ]
  node [
    id 351
    label "Nowa_D&#281;ba"
  ]
  node [
    id 352
    label "Wilejka"
  ]
  node [
    id 353
    label "Modena"
  ]
  node [
    id 354
    label "Demmin"
  ]
  node [
    id 355
    label "Houston"
  ]
  node [
    id 356
    label "Rydu&#322;towy"
  ]
  node [
    id 357
    label "Bordeaux"
  ]
  node [
    id 358
    label "Schmalkalden"
  ]
  node [
    id 359
    label "O&#322;omuniec"
  ]
  node [
    id 360
    label "Tuluza"
  ]
  node [
    id 361
    label "tramwaj"
  ]
  node [
    id 362
    label "Nantes"
  ]
  node [
    id 363
    label "Debreczyn"
  ]
  node [
    id 364
    label "Kowel"
  ]
  node [
    id 365
    label "Witnica"
  ]
  node [
    id 366
    label "Stalingrad"
  ]
  node [
    id 367
    label "Drezno"
  ]
  node [
    id 368
    label "Perejas&#322;aw"
  ]
  node [
    id 369
    label "Luksor"
  ]
  node [
    id 370
    label "Ostaszk&#243;w"
  ]
  node [
    id 371
    label "Gettysburg"
  ]
  node [
    id 372
    label "Trydent"
  ]
  node [
    id 373
    label "Poczdam"
  ]
  node [
    id 374
    label "Mesyna"
  ]
  node [
    id 375
    label "Krasnogorsk"
  ]
  node [
    id 376
    label "Kars"
  ]
  node [
    id 377
    label "Darmstadt"
  ]
  node [
    id 378
    label "Rzg&#243;w"
  ]
  node [
    id 379
    label "Kar&#322;owice"
  ]
  node [
    id 380
    label "Czeskie_Budziejowice"
  ]
  node [
    id 381
    label "Buda"
  ]
  node [
    id 382
    label "Monako"
  ]
  node [
    id 383
    label "Pardubice"
  ]
  node [
    id 384
    label "Pas&#322;&#281;k"
  ]
  node [
    id 385
    label "Fatima"
  ]
  node [
    id 386
    label "Wo&#322;oko&#322;amsk"
  ]
  node [
    id 387
    label "Bir&#380;e"
  ]
  node [
    id 388
    label "Wi&#322;komierz"
  ]
  node [
    id 389
    label "Opawa"
  ]
  node [
    id 390
    label "Mantua"
  ]
  node [
    id 391
    label "ulica"
  ]
  node [
    id 392
    label "Tarragona"
  ]
  node [
    id 393
    label "Antwerpia"
  ]
  node [
    id 394
    label "Asuan"
  ]
  node [
    id 395
    label "Korynt"
  ]
  node [
    id 396
    label "Armenia"
  ]
  node [
    id 397
    label "Budionnowsk"
  ]
  node [
    id 398
    label "Lengyel"
  ]
  node [
    id 399
    label "Betlejem"
  ]
  node [
    id 400
    label "Asy&#380;"
  ]
  node [
    id 401
    label "Batumi"
  ]
  node [
    id 402
    label "Paczk&#243;w"
  ]
  node [
    id 403
    label "Grenada"
  ]
  node [
    id 404
    label "Suczawa"
  ]
  node [
    id 405
    label "Nowogard"
  ]
  node [
    id 406
    label "Tyr"
  ]
  node [
    id 407
    label "Bria&#324;sk"
  ]
  node [
    id 408
    label "Bar"
  ]
  node [
    id 409
    label "Czerkiesk"
  ]
  node [
    id 410
    label "Ja&#322;ta"
  ]
  node [
    id 411
    label "Mo&#347;ciska"
  ]
  node [
    id 412
    label "Medyna"
  ]
  node [
    id 413
    label "Tartu"
  ]
  node [
    id 414
    label "Pemba"
  ]
  node [
    id 415
    label "Lipawa"
  ]
  node [
    id 416
    label "Tyl&#380;a"
  ]
  node [
    id 417
    label "Dayton"
  ]
  node [
    id 418
    label "Lipsk"
  ]
  node [
    id 419
    label "Rohatyn"
  ]
  node [
    id 420
    label "Peszawar"
  ]
  node [
    id 421
    label "Adrianopol"
  ]
  node [
    id 422
    label "Azow"
  ]
  node [
    id 423
    label "Iwano-Frankowsk"
  ]
  node [
    id 424
    label "Czarnobyl"
  ]
  node [
    id 425
    label "Rakoniewice"
  ]
  node [
    id 426
    label "Obuch&#243;w"
  ]
  node [
    id 427
    label "Orneta"
  ]
  node [
    id 428
    label "Koszyce"
  ]
  node [
    id 429
    label "Czeski_Cieszyn"
  ]
  node [
    id 430
    label "Zagorsk"
  ]
  node [
    id 431
    label "Nieder_Selters"
  ]
  node [
    id 432
    label "Ko&#322;omna"
  ]
  node [
    id 433
    label "Rost&#243;w"
  ]
  node [
    id 434
    label "Bolonia"
  ]
  node [
    id 435
    label "Rajgr&#243;d"
  ]
  node [
    id 436
    label "L&#252;neburg"
  ]
  node [
    id 437
    label "Brack"
  ]
  node [
    id 438
    label "Konstancja"
  ]
  node [
    id 439
    label "Koluszki"
  ]
  node [
    id 440
    label "Semipa&#322;aty&#324;sk"
  ]
  node [
    id 441
    label "Suez"
  ]
  node [
    id 442
    label "Mrocza"
  ]
  node [
    id 443
    label "Triest"
  ]
  node [
    id 444
    label "Murma&#324;sk"
  ]
  node [
    id 445
    label "Tu&#322;a"
  ]
  node [
    id 446
    label "Tarnogr&#243;d"
  ]
  node [
    id 447
    label "Radziech&#243;w"
  ]
  node [
    id 448
    label "Kokand"
  ]
  node [
    id 449
    label "Kircholm"
  ]
  node [
    id 450
    label "Nowa_Ruda"
  ]
  node [
    id 451
    label "Huma&#324;"
  ]
  node [
    id 452
    label "Turkiestan"
  ]
  node [
    id 453
    label "Kani&#243;w"
  ]
  node [
    id 454
    label "Pilzno"
  ]
  node [
    id 455
    label "Korfant&#243;w"
  ]
  node [
    id 456
    label "Dubno"
  ]
  node [
    id 457
    label "Bras&#322;aw"
  ]
  node [
    id 458
    label "Choroszcz"
  ]
  node [
    id 459
    label "Nowogr&#243;d"
  ]
  node [
    id 460
    label "Konotop"
  ]
  node [
    id 461
    label "M&#347;cis&#322;aw"
  ]
  node [
    id 462
    label "Jastarnia"
  ]
  node [
    id 463
    label "Ba&#324;ska_Bystrzyca"
  ]
  node [
    id 464
    label "Omsk"
  ]
  node [
    id 465
    label "Troick"
  ]
  node [
    id 466
    label "Koper"
  ]
  node [
    id 467
    label "Jenisejsk"
  ]
  node [
    id 468
    label "Frankfurt_nad_Odr&#261;"
  ]
  node [
    id 469
    label "Oldenburg_in_Holstein"
  ]
  node [
    id 470
    label "Trenczyn"
  ]
  node [
    id 471
    label "Wormacja"
  ]
  node [
    id 472
    label "Wagram"
  ]
  node [
    id 473
    label "Lubeka"
  ]
  node [
    id 474
    label "Genewa"
  ]
  node [
    id 475
    label "&#379;ydacz&#243;w"
  ]
  node [
    id 476
    label "Kleck"
  ]
  node [
    id 477
    label "G&#243;ra_Kalwaria"
  ]
  node [
    id 478
    label "Struga"
  ]
  node [
    id 479
    label "Izbica_Kujawska"
  ]
  node [
    id 480
    label "Stalinogorsk"
  ]
  node [
    id 481
    label "Izmir"
  ]
  node [
    id 482
    label "Dortmund"
  ]
  node [
    id 483
    label "Workuta"
  ]
  node [
    id 484
    label "Jerycho"
  ]
  node [
    id 485
    label "Brunszwik"
  ]
  node [
    id 486
    label "Aleksandria"
  ]
  node [
    id 487
    label "miejscowo&#347;&#263;"
  ]
  node [
    id 488
    label "Borys&#322;aw"
  ]
  node [
    id 489
    label "Zaleszczyki"
  ]
  node [
    id 490
    label "Z&#322;oczew"
  ]
  node [
    id 491
    label "Piast&#243;w"
  ]
  node [
    id 492
    label "Frankfurt_nad_Menem"
  ]
  node [
    id 493
    label "Bor"
  ]
  node [
    id 494
    label "Nazaret"
  ]
  node [
    id 495
    label "Sarat&#243;w"
  ]
  node [
    id 496
    label "Brasz&#243;w"
  ]
  node [
    id 497
    label "Malin"
  ]
  node [
    id 498
    label "Parma"
  ]
  node [
    id 499
    label "Wierchoja&#324;sk"
  ]
  node [
    id 500
    label "Tarent"
  ]
  node [
    id 501
    label "Mariampol"
  ]
  node [
    id 502
    label "Wuhan"
  ]
  node [
    id 503
    label "Split"
  ]
  node [
    id 504
    label "Baranowicze"
  ]
  node [
    id 505
    label "Marki"
  ]
  node [
    id 506
    label "Adana"
  ]
  node [
    id 507
    label "B&#322;aszki"
  ]
  node [
    id 508
    label "Lubecz"
  ]
  node [
    id 509
    label "Sulech&#243;w"
  ]
  node [
    id 510
    label "Borys&#243;w"
  ]
  node [
    id 511
    label "Homel"
  ]
  node [
    id 512
    label "Tours"
  ]
  node [
    id 513
    label "Zaporo&#380;e"
  ]
  node [
    id 514
    label "Edam"
  ]
  node [
    id 515
    label "Kamieniec_Podolski"
  ]
  node [
    id 516
    label "Miedwie&#380;jegorsk"
  ]
  node [
    id 517
    label "Konstantynopol"
  ]
  node [
    id 518
    label "Chocim"
  ]
  node [
    id 519
    label "Mohylew"
  ]
  node [
    id 520
    label "Merseburg"
  ]
  node [
    id 521
    label "Kapsztad"
  ]
  node [
    id 522
    label "Sambor"
  ]
  node [
    id 523
    label "Manchester"
  ]
  node [
    id 524
    label "Pi&#324;sk"
  ]
  node [
    id 525
    label "Ochryda"
  ]
  node [
    id 526
    label "Rybi&#324;sk"
  ]
  node [
    id 527
    label "Czadca"
  ]
  node [
    id 528
    label "Orenburg"
  ]
  node [
    id 529
    label "Krajowa"
  ]
  node [
    id 530
    label "Eleusis"
  ]
  node [
    id 531
    label "Awinion"
  ]
  node [
    id 532
    label "Rzeczyca"
  ]
  node [
    id 533
    label "Lozanna"
  ]
  node [
    id 534
    label "Barczewo"
  ]
  node [
    id 535
    label "&#379;migr&#243;d"
  ]
  node [
    id 536
    label "Chabarowsk"
  ]
  node [
    id 537
    label "Jena"
  ]
  node [
    id 538
    label "Xai-Xai"
  ]
  node [
    id 539
    label "Radk&#243;w"
  ]
  node [
    id 540
    label "Syrakuzy"
  ]
  node [
    id 541
    label "Zas&#322;aw"
  ]
  node [
    id 542
    label "Windsor"
  ]
  node [
    id 543
    label "Getynga"
  ]
  node [
    id 544
    label "Carrara"
  ]
  node [
    id 545
    label "Madras"
  ]
  node [
    id 546
    label "Nitra"
  ]
  node [
    id 547
    label "Kilonia"
  ]
  node [
    id 548
    label "Rawenna"
  ]
  node [
    id 549
    label "Stawropol"
  ]
  node [
    id 550
    label "Warna"
  ]
  node [
    id 551
    label "Ba&#322;tijsk"
  ]
  node [
    id 552
    label "Cumana"
  ]
  node [
    id 553
    label "Kostroma"
  ]
  node [
    id 554
    label "Bajonna"
  ]
  node [
    id 555
    label "Magadan"
  ]
  node [
    id 556
    label "Kercz"
  ]
  node [
    id 557
    label "Harbin"
  ]
  node [
    id 558
    label "Sankt_Florian"
  ]
  node [
    id 559
    label "Bia&#322;ogr&#243;d_nad_Dniestrem"
  ]
  node [
    id 560
    label "Wo&#322;kowysk"
  ]
  node [
    id 561
    label "Norak"
  ]
  node [
    id 562
    label "S&#232;vres"
  ]
  node [
    id 563
    label "Barwice"
  ]
  node [
    id 564
    label "Sumy"
  ]
  node [
    id 565
    label "Jutrosin"
  ]
  node [
    id 566
    label "Canterbury"
  ]
  node [
    id 567
    label "Czerkasy"
  ]
  node [
    id 568
    label "Troki"
  ]
  node [
    id 569
    label "Dnieprodzier&#380;y&#324;sk"
  ]
  node [
    id 570
    label "Turka"
  ]
  node [
    id 571
    label "Budziszyn"
  ]
  node [
    id 572
    label "A&#322;czewsk"
  ]
  node [
    id 573
    label "Chark&#243;w"
  ]
  node [
    id 574
    label "Go&#347;cino"
  ]
  node [
    id 575
    label "Ku&#378;nieck"
  ]
  node [
    id 576
    label "Wotki&#324;sk"
  ]
  node [
    id 577
    label "Symferopol"
  ]
  node [
    id 578
    label "Dmitrow"
  ]
  node [
    id 579
    label "Cherso&#324;"
  ]
  node [
    id 580
    label "zabudowa"
  ]
  node [
    id 581
    label "Orlean"
  ]
  node [
    id 582
    label "Nowogr&#243;dek"
  ]
  node [
    id 583
    label "Ku&#378;nia_Raciborska"
  ]
  node [
    id 584
    label "Berdia&#324;sk"
  ]
  node [
    id 585
    label "Szumsk"
  ]
  node [
    id 586
    label "Z&#322;ocz&#243;w"
  ]
  node [
    id 587
    label "Orsza"
  ]
  node [
    id 588
    label "Cluny"
  ]
  node [
    id 589
    label "Aralsk"
  ]
  node [
    id 590
    label "Stoczek_&#321;ukowski"
  ]
  node [
    id 591
    label "Bogumin"
  ]
  node [
    id 592
    label "Antiochia"
  ]
  node [
    id 593
    label "Inhambane"
  ]
  node [
    id 594
    label "S&#322;awia&#324;sk_nad_Kubaniem"
  ]
  node [
    id 595
    label "Wielki_Pres&#322;aw"
  ]
  node [
    id 596
    label "Trewir"
  ]
  node [
    id 597
    label "Nowogr&#243;d_Wielki"
  ]
  node [
    id 598
    label "Siewieromorsk"
  ]
  node [
    id 599
    label "Calais"
  ]
  node [
    id 600
    label "Twer"
  ]
  node [
    id 601
    label "&#379;ytawa"
  ]
  node [
    id 602
    label "Eupatoria"
  ]
  node [
    id 603
    label "Stara_Zagora"
  ]
  node [
    id 604
    label "Jastrowie"
  ]
  node [
    id 605
    label "Piatigorsk"
  ]
  node [
    id 606
    label "&#379;&#243;&#322;kiew"
  ]
  node [
    id 607
    label "Le&#324;sk"
  ]
  node [
    id 608
    label "Johannesburg"
  ]
  node [
    id 609
    label "Kaszyn"
  ]
  node [
    id 610
    label "&#346;wi&#281;ciany"
  ]
  node [
    id 611
    label "&#379;ylina"
  ]
  node [
    id 612
    label "Sewastopol"
  ]
  node [
    id 613
    label "Pietrozawodsk"
  ]
  node [
    id 614
    label "Bobolice"
  ]
  node [
    id 615
    label "Mosty"
  ]
  node [
    id 616
    label "Korsu&#324;_Szewczenkowski"
  ]
  node [
    id 617
    label "Karaganda"
  ]
  node [
    id 618
    label "Marsylia"
  ]
  node [
    id 619
    label "Buchara"
  ]
  node [
    id 620
    label "Dubrownik"
  ]
  node [
    id 621
    label "Be&#322;z"
  ]
  node [
    id 622
    label "Oran"
  ]
  node [
    id 623
    label "Regensburg"
  ]
  node [
    id 624
    label "Rotterdam"
  ]
  node [
    id 625
    label "Trembowla"
  ]
  node [
    id 626
    label "Woskriesiensk"
  ]
  node [
    id 627
    label "Po&#322;ock"
  ]
  node [
    id 628
    label "Poprad"
  ]
  node [
    id 629
    label "Kronsztad"
  ]
  node [
    id 630
    label "Los_Angeles"
  ]
  node [
    id 631
    label "U&#322;an_Ude"
  ]
  node [
    id 632
    label "U&#347;cie_nad_&#321;ab&#261;"
  ]
  node [
    id 633
    label "W&#322;adywostok"
  ]
  node [
    id 634
    label "Kandahar"
  ]
  node [
    id 635
    label "Tobolsk"
  ]
  node [
    id 636
    label "Boston"
  ]
  node [
    id 637
    label "Hawana"
  ]
  node [
    id 638
    label "Kis&#322;owodzk"
  ]
  node [
    id 639
    label "Tulon"
  ]
  node [
    id 640
    label "Utrecht"
  ]
  node [
    id 641
    label "Oleszyce"
  ]
  node [
    id 642
    label "Hradec_Kr&#225;lov&#233;"
  ]
  node [
    id 643
    label "Katania"
  ]
  node [
    id 644
    label "Teby"
  ]
  node [
    id 645
    label "Paw&#322;owo"
  ]
  node [
    id 646
    label "W&#252;rzburg"
  ]
  node [
    id 647
    label "Podiebrady"
  ]
  node [
    id 648
    label "Uppsala"
  ]
  node [
    id 649
    label "Poniewie&#380;"
  ]
  node [
    id 650
    label "Niko&#322;ajewsk"
  ]
  node [
    id 651
    label "Aczy&#324;sk"
  ]
  node [
    id 652
    label "Berezyna"
  ]
  node [
    id 653
    label "Ostr&#243;g"
  ]
  node [
    id 654
    label "Brze&#347;&#263;"
  ]
  node [
    id 655
    label "Lancaster"
  ]
  node [
    id 656
    label "Stryj"
  ]
  node [
    id 657
    label "Kozielsk"
  ]
  node [
    id 658
    label "Loreto"
  ]
  node [
    id 659
    label "B&#322;agowieszcze&#324;sk"
  ]
  node [
    id 660
    label "Hebron"
  ]
  node [
    id 661
    label "Kaspijsk"
  ]
  node [
    id 662
    label "Peczora"
  ]
  node [
    id 663
    label "Isfahan"
  ]
  node [
    id 664
    label "Chimoio"
  ]
  node [
    id 665
    label "Mory&#324;"
  ]
  node [
    id 666
    label "Kowno"
  ]
  node [
    id 667
    label "Nie&#347;wie&#380;"
  ]
  node [
    id 668
    label "Opalenica"
  ]
  node [
    id 669
    label "Kolonia"
  ]
  node [
    id 670
    label "Stary_Sambor"
  ]
  node [
    id 671
    label "Kolkata"
  ]
  node [
    id 672
    label "Turkmenbaszy"
  ]
  node [
    id 673
    label "Mohyl&#243;w_Podolski"
  ]
  node [
    id 674
    label "Nankin"
  ]
  node [
    id 675
    label "Krzanowice"
  ]
  node [
    id 676
    label "Efez"
  ]
  node [
    id 677
    label "Dobrodzie&#324;"
  ]
  node [
    id 678
    label "Neapol"
  ]
  node [
    id 679
    label "S&#322;uck"
  ]
  node [
    id 680
    label "Fryburg_Bryzgowijski"
  ]
  node [
    id 681
    label "W&#322;odzimierz_Wo&#322;y&#324;ski"
  ]
  node [
    id 682
    label "Frydek-Mistek"
  ]
  node [
    id 683
    label "Korsze"
  ]
  node [
    id 684
    label "T&#322;uszcz"
  ]
  node [
    id 685
    label "Soligorsk"
  ]
  node [
    id 686
    label "Kie&#380;mark"
  ]
  node [
    id 687
    label "Mannheim"
  ]
  node [
    id 688
    label "Ulm"
  ]
  node [
    id 689
    label "Podhajce"
  ]
  node [
    id 690
    label "Dniepropetrowsk"
  ]
  node [
    id 691
    label "Szamocin"
  ]
  node [
    id 692
    label "Ko&#322;omyja"
  ]
  node [
    id 693
    label "Buczacz"
  ]
  node [
    id 694
    label "M&#252;nster"
  ]
  node [
    id 695
    label "Brema"
  ]
  node [
    id 696
    label "Delhi"
  ]
  node [
    id 697
    label "&#346;niatyn"
  ]
  node [
    id 698
    label "Nicea"
  ]
  node [
    id 699
    label "Szawle"
  ]
  node [
    id 700
    label "Czerniowce"
  ]
  node [
    id 701
    label "Mi&#347;nia"
  ]
  node [
    id 702
    label "Sydney"
  ]
  node [
    id 703
    label "Moguncja"
  ]
  node [
    id 704
    label "Narbona"
  ]
  node [
    id 705
    label "Swiet&#322;ogorsk"
  ]
  node [
    id 706
    label "Wittenberga"
  ]
  node [
    id 707
    label "Uljanowsk"
  ]
  node [
    id 708
    label "&#321;uga&#324;sk"
  ]
  node [
    id 709
    label "Wyborg"
  ]
  node [
    id 710
    label "Trojan"
  ]
  node [
    id 711
    label "Kalwaria_Zebrzydowska"
  ]
  node [
    id 712
    label "Brandenburg"
  ]
  node [
    id 713
    label "Kemerowo"
  ]
  node [
    id 714
    label "Kaszgar"
  ]
  node [
    id 715
    label "Lenzen"
  ]
  node [
    id 716
    label "Nanning"
  ]
  node [
    id 717
    label "Gotha"
  ]
  node [
    id 718
    label "Zurych"
  ]
  node [
    id 719
    label "Baltimore"
  ]
  node [
    id 720
    label "&#321;uck"
  ]
  node [
    id 721
    label "Bristol"
  ]
  node [
    id 722
    label "Ferrara"
  ]
  node [
    id 723
    label "Mariupol"
  ]
  node [
    id 724
    label "przestrze&#324;_publiczna"
  ]
  node [
    id 725
    label "Lhasa"
  ]
  node [
    id 726
    label "Czerniejewo"
  ]
  node [
    id 727
    label "Filadelfia"
  ]
  node [
    id 728
    label "Kanton"
  ]
  node [
    id 729
    label "Milan&#243;wek"
  ]
  node [
    id 730
    label "Perwomajsk"
  ]
  node [
    id 731
    label "Nieftiegorsk"
  ]
  node [
    id 732
    label "Pittsburgh"
  ]
  node [
    id 733
    label "Greifswald"
  ]
  node [
    id 734
    label "Akwileja"
  ]
  node [
    id 735
    label "Norfolk"
  ]
  node [
    id 736
    label "Perm"
  ]
  node [
    id 737
    label "Detroit"
  ]
  node [
    id 738
    label "Fergana"
  ]
  node [
    id 739
    label "Starobielsk"
  ]
  node [
    id 740
    label "Wielsk"
  ]
  node [
    id 741
    label "Zaklik&#243;w"
  ]
  node [
    id 742
    label "Majsur"
  ]
  node [
    id 743
    label "Narwa"
  ]
  node [
    id 744
    label "Chicago"
  ]
  node [
    id 745
    label "Byczyna"
  ]
  node [
    id 746
    label "Mozyrz"
  ]
  node [
    id 747
    label "Konstantyn&#243;wka"
  ]
  node [
    id 748
    label "Miko&#322;aj&#243;w"
  ]
  node [
    id 749
    label "Megara"
  ]
  node [
    id 750
    label "Stralsund"
  ]
  node [
    id 751
    label "Wo&#322;gograd"
  ]
  node [
    id 752
    label "Lichinga"
  ]
  node [
    id 753
    label "Haga"
  ]
  node [
    id 754
    label "Tarnopol"
  ]
  node [
    id 755
    label "K&#322;ajpeda"
  ]
  node [
    id 756
    label "Nowomoskowsk"
  ]
  node [
    id 757
    label "Ussuryjsk"
  ]
  node [
    id 758
    label "Brugia"
  ]
  node [
    id 759
    label "Natal"
  ]
  node [
    id 760
    label "Kro&#347;niewice"
  ]
  node [
    id 761
    label "Edynburg"
  ]
  node [
    id 762
    label "Marburg"
  ]
  node [
    id 763
    label "&#346;wiebodzice"
  ]
  node [
    id 764
    label "S&#322;onim"
  ]
  node [
    id 765
    label "Dalton"
  ]
  node [
    id 766
    label "Smorgonie"
  ]
  node [
    id 767
    label "Orze&#322;"
  ]
  node [
    id 768
    label "Nowoku&#378;nieck"
  ]
  node [
    id 769
    label "Zadar"
  ]
  node [
    id 770
    label "Tel_Awiw-Jafa"
  ]
  node [
    id 771
    label "Koprzywnica"
  ]
  node [
    id 772
    label "Angarsk"
  ]
  node [
    id 773
    label "Mo&#380;ajsk"
  ]
  node [
    id 774
    label "Akwizgran"
  ]
  node [
    id 775
    label "Norylsk"
  ]
  node [
    id 776
    label "Jawor&#243;w"
  ]
  node [
    id 777
    label "weduta"
  ]
  node [
    id 778
    label "Jab&#322;onk&#243;w"
  ]
  node [
    id 779
    label "Suzdal"
  ]
  node [
    id 780
    label "W&#322;odzimierz"
  ]
  node [
    id 781
    label "Bujnaksk"
  ]
  node [
    id 782
    label "Beresteczko"
  ]
  node [
    id 783
    label "Strzelno"
  ]
  node [
    id 784
    label "Siewsk"
  ]
  node [
    id 785
    label "Cymlansk"
  ]
  node [
    id 786
    label "Trzyniec"
  ]
  node [
    id 787
    label "Hanower"
  ]
  node [
    id 788
    label "Wuppertal"
  ]
  node [
    id 789
    label "Sura&#380;"
  ]
  node [
    id 790
    label "Winchester"
  ]
  node [
    id 791
    label "Samara"
  ]
  node [
    id 792
    label "Sydon"
  ]
  node [
    id 793
    label "Krasnodar"
  ]
  node [
    id 794
    label "Worone&#380;"
  ]
  node [
    id 795
    label "Paw&#322;odar"
  ]
  node [
    id 796
    label "Czelabi&#324;sk"
  ]
  node [
    id 797
    label "Reda"
  ]
  node [
    id 798
    label "Karwina"
  ]
  node [
    id 799
    label "Wyszehrad"
  ]
  node [
    id 800
    label "Sara&#324;sk"
  ]
  node [
    id 801
    label "Koby&#322;ka"
  ]
  node [
    id 802
    label "Winnica"
  ]
  node [
    id 803
    label "Tambow"
  ]
  node [
    id 804
    label "Pyskowice"
  ]
  node [
    id 805
    label "Heidelberg"
  ]
  node [
    id 806
    label "Maribor"
  ]
  node [
    id 807
    label "Werona"
  ]
  node [
    id 808
    label "G&#322;uszyca"
  ]
  node [
    id 809
    label "Rostock"
  ]
  node [
    id 810
    label "Mekka"
  ]
  node [
    id 811
    label "Liberec"
  ]
  node [
    id 812
    label "Bie&#322;gorod"
  ]
  node [
    id 813
    label "Berdycz&#243;w"
  ]
  node [
    id 814
    label "Sierdobsk"
  ]
  node [
    id 815
    label "Bobrujsk"
  ]
  node [
    id 816
    label "Padwa"
  ]
  node [
    id 817
    label "Pasawa"
  ]
  node [
    id 818
    label "Chanty-Mansyjsk"
  ]
  node [
    id 819
    label "&#379;ar&#243;w"
  ]
  node [
    id 820
    label "Poczaj&#243;w"
  ]
  node [
    id 821
    label "Barabi&#324;sk"
  ]
  node [
    id 822
    label "Gorycja"
  ]
  node [
    id 823
    label "Haarlem"
  ]
  node [
    id 824
    label "Kiejdany"
  ]
  node [
    id 825
    label "Chmielnicki"
  ]
  node [
    id 826
    label "Magnitogorsk"
  ]
  node [
    id 827
    label "Burgas"
  ]
  node [
    id 828
    label "Siena"
  ]
  node [
    id 829
    label "Korzec"
  ]
  node [
    id 830
    label "Bonn"
  ]
  node [
    id 831
    label "Pr&#243;szk&#243;w"
  ]
  node [
    id 832
    label "Walencja"
  ]
  node [
    id 833
    label "Mosina"
  ]
  node [
    id 834
    label "Karta_Nauczyciela"
  ]
  node [
    id 835
    label "marc&#243;wka"
  ]
  node [
    id 836
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 837
    label "akt"
  ]
  node [
    id 838
    label "przej&#347;&#263;"
  ]
  node [
    id 839
    label "charter"
  ]
  node [
    id 840
    label "przej&#347;cie"
  ]
  node [
    id 841
    label "figura"
  ]
  node [
    id 842
    label "surowiec_energetyczny"
  ]
  node [
    id 843
    label "w&#281;glowiec"
  ]
  node [
    id 844
    label "w&#281;glowodan"
  ]
  node [
    id 845
    label "kopalina_podstawowa"
  ]
  node [
    id 846
    label "coal"
  ]
  node [
    id 847
    label "przybory_do_pisania"
  ]
  node [
    id 848
    label "makroelement"
  ]
  node [
    id 849
    label "niemetal"
  ]
  node [
    id 850
    label "zsypnik"
  ]
  node [
    id 851
    label "w&#281;glarka"
  ]
  node [
    id 852
    label "zw&#281;glenie_si&#281;"
  ]
  node [
    id 853
    label "zw&#281;glanie_si&#281;"
  ]
  node [
    id 854
    label "coil"
  ]
  node [
    id 855
    label "bry&#322;a"
  ]
  node [
    id 856
    label "ska&#322;a"
  ]
  node [
    id 857
    label "carbon"
  ]
  node [
    id 858
    label "fulleren"
  ]
  node [
    id 859
    label "rysunek"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 122
  ]
  edge [
    source 7
    target 123
  ]
  edge [
    source 7
    target 124
  ]
  edge [
    source 8
    target 125
  ]
  edge [
    source 8
    target 126
  ]
  edge [
    source 8
    target 127
  ]
  edge [
    source 8
    target 128
  ]
  edge [
    source 8
    target 129
  ]
  edge [
    source 8
    target 130
  ]
  edge [
    source 8
    target 131
  ]
  edge [
    source 8
    target 132
  ]
  edge [
    source 8
    target 133
  ]
  edge [
    source 8
    target 134
  ]
  edge [
    source 8
    target 135
  ]
  edge [
    source 8
    target 136
  ]
  edge [
    source 8
    target 137
  ]
  edge [
    source 8
    target 138
  ]
  edge [
    source 8
    target 139
  ]
  edge [
    source 8
    target 140
  ]
  edge [
    source 8
    target 141
  ]
  edge [
    source 8
    target 142
  ]
  edge [
    source 8
    target 143
  ]
  edge [
    source 8
    target 144
  ]
  edge [
    source 8
    target 145
  ]
  edge [
    source 8
    target 146
  ]
  edge [
    source 8
    target 147
  ]
  edge [
    source 8
    target 148
  ]
  edge [
    source 8
    target 149
  ]
  edge [
    source 8
    target 150
  ]
  edge [
    source 8
    target 151
  ]
  edge [
    source 8
    target 152
  ]
  edge [
    source 8
    target 153
  ]
  edge [
    source 8
    target 154
  ]
  edge [
    source 8
    target 155
  ]
  edge [
    source 8
    target 156
  ]
  edge [
    source 8
    target 157
  ]
  edge [
    source 8
    target 158
  ]
  edge [
    source 8
    target 159
  ]
  edge [
    source 8
    target 160
  ]
  edge [
    source 8
    target 161
  ]
  edge [
    source 8
    target 162
  ]
  edge [
    source 8
    target 163
  ]
  edge [
    source 8
    target 164
  ]
  edge [
    source 8
    target 165
  ]
  edge [
    source 8
    target 166
  ]
  edge [
    source 8
    target 167
  ]
  edge [
    source 8
    target 168
  ]
  edge [
    source 8
    target 169
  ]
  edge [
    source 8
    target 170
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 171
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 172
  ]
  edge [
    source 10
    target 173
  ]
  edge [
    source 10
    target 174
  ]
  edge [
    source 10
    target 175
  ]
  edge [
    source 10
    target 176
  ]
  edge [
    source 10
    target 177
  ]
  edge [
    source 10
    target 178
  ]
  edge [
    source 10
    target 179
  ]
  edge [
    source 10
    target 180
  ]
  edge [
    source 10
    target 181
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 182
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 183
  ]
  edge [
    source 12
    target 184
  ]
  edge [
    source 12
    target 185
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 186
  ]
  edge [
    source 13
    target 187
  ]
  edge [
    source 13
    target 188
  ]
  edge [
    source 13
    target 189
  ]
  edge [
    source 13
    target 190
  ]
  edge [
    source 13
    target 191
  ]
  edge [
    source 13
    target 192
  ]
  edge [
    source 13
    target 193
  ]
  edge [
    source 13
    target 194
  ]
  edge [
    source 13
    target 195
  ]
  edge [
    source 13
    target 196
  ]
  edge [
    source 13
    target 197
  ]
  edge [
    source 13
    target 198
  ]
  edge [
    source 13
    target 199
  ]
  edge [
    source 13
    target 200
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 201
  ]
  edge [
    source 14
    target 202
  ]
  edge [
    source 14
    target 203
  ]
  edge [
    source 14
    target 204
  ]
  edge [
    source 14
    target 205
  ]
  edge [
    source 14
    target 206
  ]
  edge [
    source 14
    target 207
  ]
  edge [
    source 14
    target 208
  ]
  edge [
    source 14
    target 209
  ]
  edge [
    source 14
    target 210
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 211
  ]
  edge [
    source 15
    target 212
  ]
  edge [
    source 15
    target 213
  ]
  edge [
    source 15
    target 214
  ]
  edge [
    source 15
    target 215
  ]
  edge [
    source 15
    target 89
  ]
  edge [
    source 15
    target 216
  ]
  edge [
    source 15
    target 217
  ]
  edge [
    source 15
    target 218
  ]
  edge [
    source 15
    target 219
  ]
  edge [
    source 15
    target 220
  ]
  edge [
    source 15
    target 221
  ]
  edge [
    source 15
    target 222
  ]
  edge [
    source 15
    target 223
  ]
  edge [
    source 15
    target 224
  ]
  edge [
    source 15
    target 122
  ]
  edge [
    source 15
    target 225
  ]
  edge [
    source 15
    target 226
  ]
  edge [
    source 15
    target 227
  ]
  edge [
    source 15
    target 228
  ]
  edge [
    source 15
    target 229
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 230
  ]
  edge [
    source 16
    target 231
  ]
  edge [
    source 16
    target 232
  ]
  edge [
    source 16
    target 233
  ]
  edge [
    source 16
    target 234
  ]
  edge [
    source 16
    target 235
  ]
  edge [
    source 16
    target 236
  ]
  edge [
    source 16
    target 237
  ]
  edge [
    source 16
    target 238
  ]
  edge [
    source 16
    target 239
  ]
  edge [
    source 16
    target 240
  ]
  edge [
    source 16
    target 241
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 242
  ]
  edge [
    source 17
    target 243
  ]
  edge [
    source 18
    target 244
  ]
  edge [
    source 18
    target 245
  ]
  edge [
    source 18
    target 246
  ]
  edge [
    source 18
    target 247
  ]
  edge [
    source 18
    target 248
  ]
  edge [
    source 18
    target 249
  ]
  edge [
    source 18
    target 250
  ]
  edge [
    source 18
    target 251
  ]
  edge [
    source 18
    target 252
  ]
  edge [
    source 18
    target 253
  ]
  edge [
    source 18
    target 254
  ]
  edge [
    source 18
    target 255
  ]
  edge [
    source 18
    target 256
  ]
  edge [
    source 18
    target 257
  ]
  edge [
    source 18
    target 258
  ]
  edge [
    source 18
    target 259
  ]
  edge [
    source 18
    target 260
  ]
  edge [
    source 18
    target 261
  ]
  edge [
    source 18
    target 262
  ]
  edge [
    source 18
    target 263
  ]
  edge [
    source 18
    target 264
  ]
  edge [
    source 18
    target 265
  ]
  edge [
    source 18
    target 266
  ]
  edge [
    source 18
    target 267
  ]
  edge [
    source 18
    target 268
  ]
  edge [
    source 18
    target 269
  ]
  edge [
    source 18
    target 270
  ]
  edge [
    source 18
    target 271
  ]
  edge [
    source 18
    target 272
  ]
  edge [
    source 18
    target 273
  ]
  edge [
    source 18
    target 274
  ]
  edge [
    source 18
    target 275
  ]
  edge [
    source 18
    target 276
  ]
  edge [
    source 18
    target 277
  ]
  edge [
    source 18
    target 278
  ]
  edge [
    source 18
    target 279
  ]
  edge [
    source 18
    target 280
  ]
  edge [
    source 18
    target 281
  ]
  edge [
    source 18
    target 282
  ]
  edge [
    source 18
    target 283
  ]
  edge [
    source 18
    target 284
  ]
  edge [
    source 18
    target 285
  ]
  edge [
    source 18
    target 286
  ]
  edge [
    source 18
    target 287
  ]
  edge [
    source 18
    target 288
  ]
  edge [
    source 18
    target 289
  ]
  edge [
    source 18
    target 290
  ]
  edge [
    source 18
    target 291
  ]
  edge [
    source 18
    target 292
  ]
  edge [
    source 18
    target 293
  ]
  edge [
    source 18
    target 294
  ]
  edge [
    source 18
    target 295
  ]
  edge [
    source 18
    target 296
  ]
  edge [
    source 18
    target 297
  ]
  edge [
    source 18
    target 298
  ]
  edge [
    source 18
    target 299
  ]
  edge [
    source 18
    target 300
  ]
  edge [
    source 18
    target 301
  ]
  edge [
    source 18
    target 302
  ]
  edge [
    source 18
    target 303
  ]
  edge [
    source 18
    target 304
  ]
  edge [
    source 18
    target 305
  ]
  edge [
    source 18
    target 306
  ]
  edge [
    source 18
    target 307
  ]
  edge [
    source 18
    target 308
  ]
  edge [
    source 18
    target 309
  ]
  edge [
    source 18
    target 310
  ]
  edge [
    source 18
    target 311
  ]
  edge [
    source 18
    target 312
  ]
  edge [
    source 18
    target 313
  ]
  edge [
    source 18
    target 314
  ]
  edge [
    source 18
    target 315
  ]
  edge [
    source 18
    target 316
  ]
  edge [
    source 18
    target 317
  ]
  edge [
    source 18
    target 318
  ]
  edge [
    source 18
    target 319
  ]
  edge [
    source 18
    target 320
  ]
  edge [
    source 18
    target 321
  ]
  edge [
    source 18
    target 322
  ]
  edge [
    source 18
    target 323
  ]
  edge [
    source 18
    target 324
  ]
  edge [
    source 18
    target 325
  ]
  edge [
    source 18
    target 326
  ]
  edge [
    source 18
    target 327
  ]
  edge [
    source 18
    target 328
  ]
  edge [
    source 18
    target 329
  ]
  edge [
    source 18
    target 330
  ]
  edge [
    source 18
    target 331
  ]
  edge [
    source 18
    target 332
  ]
  edge [
    source 18
    target 333
  ]
  edge [
    source 18
    target 334
  ]
  edge [
    source 18
    target 335
  ]
  edge [
    source 18
    target 336
  ]
  edge [
    source 18
    target 337
  ]
  edge [
    source 18
    target 338
  ]
  edge [
    source 18
    target 339
  ]
  edge [
    source 18
    target 340
  ]
  edge [
    source 18
    target 341
  ]
  edge [
    source 18
    target 342
  ]
  edge [
    source 18
    target 343
  ]
  edge [
    source 18
    target 344
  ]
  edge [
    source 18
    target 345
  ]
  edge [
    source 18
    target 346
  ]
  edge [
    source 18
    target 347
  ]
  edge [
    source 18
    target 348
  ]
  edge [
    source 18
    target 349
  ]
  edge [
    source 18
    target 350
  ]
  edge [
    source 18
    target 351
  ]
  edge [
    source 18
    target 352
  ]
  edge [
    source 18
    target 353
  ]
  edge [
    source 18
    target 354
  ]
  edge [
    source 18
    target 355
  ]
  edge [
    source 18
    target 356
  ]
  edge [
    source 18
    target 357
  ]
  edge [
    source 18
    target 358
  ]
  edge [
    source 18
    target 359
  ]
  edge [
    source 18
    target 360
  ]
  edge [
    source 18
    target 361
  ]
  edge [
    source 18
    target 362
  ]
  edge [
    source 18
    target 363
  ]
  edge [
    source 18
    target 364
  ]
  edge [
    source 18
    target 365
  ]
  edge [
    source 18
    target 366
  ]
  edge [
    source 18
    target 367
  ]
  edge [
    source 18
    target 368
  ]
  edge [
    source 18
    target 369
  ]
  edge [
    source 18
    target 370
  ]
  edge [
    source 18
    target 371
  ]
  edge [
    source 18
    target 372
  ]
  edge [
    source 18
    target 373
  ]
  edge [
    source 18
    target 374
  ]
  edge [
    source 18
    target 375
  ]
  edge [
    source 18
    target 376
  ]
  edge [
    source 18
    target 377
  ]
  edge [
    source 18
    target 378
  ]
  edge [
    source 18
    target 379
  ]
  edge [
    source 18
    target 380
  ]
  edge [
    source 18
    target 381
  ]
  edge [
    source 18
    target 382
  ]
  edge [
    source 18
    target 383
  ]
  edge [
    source 18
    target 384
  ]
  edge [
    source 18
    target 385
  ]
  edge [
    source 18
    target 386
  ]
  edge [
    source 18
    target 387
  ]
  edge [
    source 18
    target 388
  ]
  edge [
    source 18
    target 389
  ]
  edge [
    source 18
    target 390
  ]
  edge [
    source 18
    target 391
  ]
  edge [
    source 18
    target 392
  ]
  edge [
    source 18
    target 393
  ]
  edge [
    source 18
    target 394
  ]
  edge [
    source 18
    target 395
  ]
  edge [
    source 18
    target 396
  ]
  edge [
    source 18
    target 397
  ]
  edge [
    source 18
    target 398
  ]
  edge [
    source 18
    target 399
  ]
  edge [
    source 18
    target 400
  ]
  edge [
    source 18
    target 401
  ]
  edge [
    source 18
    target 402
  ]
  edge [
    source 18
    target 403
  ]
  edge [
    source 18
    target 404
  ]
  edge [
    source 18
    target 405
  ]
  edge [
    source 18
    target 406
  ]
  edge [
    source 18
    target 407
  ]
  edge [
    source 18
    target 408
  ]
  edge [
    source 18
    target 409
  ]
  edge [
    source 18
    target 410
  ]
  edge [
    source 18
    target 411
  ]
  edge [
    source 18
    target 412
  ]
  edge [
    source 18
    target 413
  ]
  edge [
    source 18
    target 414
  ]
  edge [
    source 18
    target 415
  ]
  edge [
    source 18
    target 416
  ]
  edge [
    source 18
    target 417
  ]
  edge [
    source 18
    target 418
  ]
  edge [
    source 18
    target 419
  ]
  edge [
    source 18
    target 420
  ]
  edge [
    source 18
    target 421
  ]
  edge [
    source 18
    target 422
  ]
  edge [
    source 18
    target 423
  ]
  edge [
    source 18
    target 424
  ]
  edge [
    source 18
    target 425
  ]
  edge [
    source 18
    target 426
  ]
  edge [
    source 18
    target 427
  ]
  edge [
    source 18
    target 428
  ]
  edge [
    source 18
    target 429
  ]
  edge [
    source 18
    target 430
  ]
  edge [
    source 18
    target 431
  ]
  edge [
    source 18
    target 432
  ]
  edge [
    source 18
    target 433
  ]
  edge [
    source 18
    target 434
  ]
  edge [
    source 18
    target 435
  ]
  edge [
    source 18
    target 436
  ]
  edge [
    source 18
    target 437
  ]
  edge [
    source 18
    target 438
  ]
  edge [
    source 18
    target 439
  ]
  edge [
    source 18
    target 440
  ]
  edge [
    source 18
    target 441
  ]
  edge [
    source 18
    target 442
  ]
  edge [
    source 18
    target 443
  ]
  edge [
    source 18
    target 444
  ]
  edge [
    source 18
    target 445
  ]
  edge [
    source 18
    target 446
  ]
  edge [
    source 18
    target 447
  ]
  edge [
    source 18
    target 448
  ]
  edge [
    source 18
    target 449
  ]
  edge [
    source 18
    target 450
  ]
  edge [
    source 18
    target 451
  ]
  edge [
    source 18
    target 452
  ]
  edge [
    source 18
    target 453
  ]
  edge [
    source 18
    target 454
  ]
  edge [
    source 18
    target 455
  ]
  edge [
    source 18
    target 456
  ]
  edge [
    source 18
    target 457
  ]
  edge [
    source 18
    target 458
  ]
  edge [
    source 18
    target 459
  ]
  edge [
    source 18
    target 460
  ]
  edge [
    source 18
    target 461
  ]
  edge [
    source 18
    target 462
  ]
  edge [
    source 18
    target 463
  ]
  edge [
    source 18
    target 464
  ]
  edge [
    source 18
    target 465
  ]
  edge [
    source 18
    target 466
  ]
  edge [
    source 18
    target 467
  ]
  edge [
    source 18
    target 468
  ]
  edge [
    source 18
    target 469
  ]
  edge [
    source 18
    target 470
  ]
  edge [
    source 18
    target 471
  ]
  edge [
    source 18
    target 472
  ]
  edge [
    source 18
    target 473
  ]
  edge [
    source 18
    target 474
  ]
  edge [
    source 18
    target 475
  ]
  edge [
    source 18
    target 476
  ]
  edge [
    source 18
    target 477
  ]
  edge [
    source 18
    target 478
  ]
  edge [
    source 18
    target 479
  ]
  edge [
    source 18
    target 480
  ]
  edge [
    source 18
    target 481
  ]
  edge [
    source 18
    target 482
  ]
  edge [
    source 18
    target 483
  ]
  edge [
    source 18
    target 484
  ]
  edge [
    source 18
    target 485
  ]
  edge [
    source 18
    target 486
  ]
  edge [
    source 18
    target 487
  ]
  edge [
    source 18
    target 488
  ]
  edge [
    source 18
    target 489
  ]
  edge [
    source 18
    target 490
  ]
  edge [
    source 18
    target 491
  ]
  edge [
    source 18
    target 492
  ]
  edge [
    source 18
    target 493
  ]
  edge [
    source 18
    target 494
  ]
  edge [
    source 18
    target 495
  ]
  edge [
    source 18
    target 496
  ]
  edge [
    source 18
    target 497
  ]
  edge [
    source 18
    target 498
  ]
  edge [
    source 18
    target 499
  ]
  edge [
    source 18
    target 500
  ]
  edge [
    source 18
    target 501
  ]
  edge [
    source 18
    target 502
  ]
  edge [
    source 18
    target 503
  ]
  edge [
    source 18
    target 504
  ]
  edge [
    source 18
    target 505
  ]
  edge [
    source 18
    target 506
  ]
  edge [
    source 18
    target 507
  ]
  edge [
    source 18
    target 508
  ]
  edge [
    source 18
    target 509
  ]
  edge [
    source 18
    target 510
  ]
  edge [
    source 18
    target 511
  ]
  edge [
    source 18
    target 512
  ]
  edge [
    source 18
    target 513
  ]
  edge [
    source 18
    target 514
  ]
  edge [
    source 18
    target 515
  ]
  edge [
    source 18
    target 516
  ]
  edge [
    source 18
    target 517
  ]
  edge [
    source 18
    target 518
  ]
  edge [
    source 18
    target 519
  ]
  edge [
    source 18
    target 520
  ]
  edge [
    source 18
    target 521
  ]
  edge [
    source 18
    target 522
  ]
  edge [
    source 18
    target 523
  ]
  edge [
    source 18
    target 524
  ]
  edge [
    source 18
    target 525
  ]
  edge [
    source 18
    target 526
  ]
  edge [
    source 18
    target 527
  ]
  edge [
    source 18
    target 528
  ]
  edge [
    source 18
    target 529
  ]
  edge [
    source 18
    target 530
  ]
  edge [
    source 18
    target 531
  ]
  edge [
    source 18
    target 532
  ]
  edge [
    source 18
    target 533
  ]
  edge [
    source 18
    target 534
  ]
  edge [
    source 18
    target 535
  ]
  edge [
    source 18
    target 536
  ]
  edge [
    source 18
    target 537
  ]
  edge [
    source 18
    target 538
  ]
  edge [
    source 18
    target 539
  ]
  edge [
    source 18
    target 540
  ]
  edge [
    source 18
    target 541
  ]
  edge [
    source 18
    target 542
  ]
  edge [
    source 18
    target 543
  ]
  edge [
    source 18
    target 544
  ]
  edge [
    source 18
    target 545
  ]
  edge [
    source 18
    target 546
  ]
  edge [
    source 18
    target 547
  ]
  edge [
    source 18
    target 548
  ]
  edge [
    source 18
    target 549
  ]
  edge [
    source 18
    target 550
  ]
  edge [
    source 18
    target 551
  ]
  edge [
    source 18
    target 552
  ]
  edge [
    source 18
    target 553
  ]
  edge [
    source 18
    target 554
  ]
  edge [
    source 18
    target 555
  ]
  edge [
    source 18
    target 556
  ]
  edge [
    source 18
    target 557
  ]
  edge [
    source 18
    target 558
  ]
  edge [
    source 18
    target 559
  ]
  edge [
    source 18
    target 560
  ]
  edge [
    source 18
    target 561
  ]
  edge [
    source 18
    target 562
  ]
  edge [
    source 18
    target 563
  ]
  edge [
    source 18
    target 564
  ]
  edge [
    source 18
    target 565
  ]
  edge [
    source 18
    target 566
  ]
  edge [
    source 18
    target 567
  ]
  edge [
    source 18
    target 568
  ]
  edge [
    source 18
    target 569
  ]
  edge [
    source 18
    target 570
  ]
  edge [
    source 18
    target 571
  ]
  edge [
    source 18
    target 572
  ]
  edge [
    source 18
    target 573
  ]
  edge [
    source 18
    target 574
  ]
  edge [
    source 18
    target 575
  ]
  edge [
    source 18
    target 576
  ]
  edge [
    source 18
    target 577
  ]
  edge [
    source 18
    target 578
  ]
  edge [
    source 18
    target 579
  ]
  edge [
    source 18
    target 580
  ]
  edge [
    source 18
    target 581
  ]
  edge [
    source 18
    target 582
  ]
  edge [
    source 18
    target 583
  ]
  edge [
    source 18
    target 584
  ]
  edge [
    source 18
    target 585
  ]
  edge [
    source 18
    target 586
  ]
  edge [
    source 18
    target 587
  ]
  edge [
    source 18
    target 588
  ]
  edge [
    source 18
    target 589
  ]
  edge [
    source 18
    target 590
  ]
  edge [
    source 18
    target 591
  ]
  edge [
    source 18
    target 592
  ]
  edge [
    source 18
    target 214
  ]
  edge [
    source 18
    target 593
  ]
  edge [
    source 18
    target 594
  ]
  edge [
    source 18
    target 595
  ]
  edge [
    source 18
    target 596
  ]
  edge [
    source 18
    target 597
  ]
  edge [
    source 18
    target 598
  ]
  edge [
    source 18
    target 599
  ]
  edge [
    source 18
    target 600
  ]
  edge [
    source 18
    target 601
  ]
  edge [
    source 18
    target 602
  ]
  edge [
    source 18
    target 603
  ]
  edge [
    source 18
    target 604
  ]
  edge [
    source 18
    target 605
  ]
  edge [
    source 18
    target 606
  ]
  edge [
    source 18
    target 607
  ]
  edge [
    source 18
    target 608
  ]
  edge [
    source 18
    target 609
  ]
  edge [
    source 18
    target 610
  ]
  edge [
    source 18
    target 611
  ]
  edge [
    source 18
    target 612
  ]
  edge [
    source 18
    target 613
  ]
  edge [
    source 18
    target 614
  ]
  edge [
    source 18
    target 615
  ]
  edge [
    source 18
    target 616
  ]
  edge [
    source 18
    target 617
  ]
  edge [
    source 18
    target 618
  ]
  edge [
    source 18
    target 619
  ]
  edge [
    source 18
    target 620
  ]
  edge [
    source 18
    target 621
  ]
  edge [
    source 18
    target 622
  ]
  edge [
    source 18
    target 623
  ]
  edge [
    source 18
    target 624
  ]
  edge [
    source 18
    target 625
  ]
  edge [
    source 18
    target 626
  ]
  edge [
    source 18
    target 627
  ]
  edge [
    source 18
    target 628
  ]
  edge [
    source 18
    target 629
  ]
  edge [
    source 18
    target 630
  ]
  edge [
    source 18
    target 631
  ]
  edge [
    source 18
    target 632
  ]
  edge [
    source 18
    target 633
  ]
  edge [
    source 18
    target 634
  ]
  edge [
    source 18
    target 635
  ]
  edge [
    source 18
    target 636
  ]
  edge [
    source 18
    target 637
  ]
  edge [
    source 18
    target 638
  ]
  edge [
    source 18
    target 639
  ]
  edge [
    source 18
    target 640
  ]
  edge [
    source 18
    target 641
  ]
  edge [
    source 18
    target 642
  ]
  edge [
    source 18
    target 643
  ]
  edge [
    source 18
    target 644
  ]
  edge [
    source 18
    target 645
  ]
  edge [
    source 18
    target 646
  ]
  edge [
    source 18
    target 647
  ]
  edge [
    source 18
    target 648
  ]
  edge [
    source 18
    target 649
  ]
  edge [
    source 18
    target 650
  ]
  edge [
    source 18
    target 651
  ]
  edge [
    source 18
    target 652
  ]
  edge [
    source 18
    target 653
  ]
  edge [
    source 18
    target 654
  ]
  edge [
    source 18
    target 655
  ]
  edge [
    source 18
    target 656
  ]
  edge [
    source 18
    target 657
  ]
  edge [
    source 18
    target 658
  ]
  edge [
    source 18
    target 659
  ]
  edge [
    source 18
    target 660
  ]
  edge [
    source 18
    target 661
  ]
  edge [
    source 18
    target 662
  ]
  edge [
    source 18
    target 663
  ]
  edge [
    source 18
    target 664
  ]
  edge [
    source 18
    target 665
  ]
  edge [
    source 18
    target 666
  ]
  edge [
    source 18
    target 667
  ]
  edge [
    source 18
    target 668
  ]
  edge [
    source 18
    target 669
  ]
  edge [
    source 18
    target 670
  ]
  edge [
    source 18
    target 671
  ]
  edge [
    source 18
    target 672
  ]
  edge [
    source 18
    target 673
  ]
  edge [
    source 18
    target 674
  ]
  edge [
    source 18
    target 675
  ]
  edge [
    source 18
    target 676
  ]
  edge [
    source 18
    target 677
  ]
  edge [
    source 18
    target 678
  ]
  edge [
    source 18
    target 679
  ]
  edge [
    source 18
    target 680
  ]
  edge [
    source 18
    target 681
  ]
  edge [
    source 18
    target 682
  ]
  edge [
    source 18
    target 683
  ]
  edge [
    source 18
    target 684
  ]
  edge [
    source 18
    target 685
  ]
  edge [
    source 18
    target 686
  ]
  edge [
    source 18
    target 687
  ]
  edge [
    source 18
    target 688
  ]
  edge [
    source 18
    target 689
  ]
  edge [
    source 18
    target 690
  ]
  edge [
    source 18
    target 691
  ]
  edge [
    source 18
    target 692
  ]
  edge [
    source 18
    target 693
  ]
  edge [
    source 18
    target 694
  ]
  edge [
    source 18
    target 695
  ]
  edge [
    source 18
    target 696
  ]
  edge [
    source 18
    target 697
  ]
  edge [
    source 18
    target 698
  ]
  edge [
    source 18
    target 699
  ]
  edge [
    source 18
    target 700
  ]
  edge [
    source 18
    target 701
  ]
  edge [
    source 18
    target 702
  ]
  edge [
    source 18
    target 703
  ]
  edge [
    source 18
    target 704
  ]
  edge [
    source 18
    target 705
  ]
  edge [
    source 18
    target 706
  ]
  edge [
    source 18
    target 707
  ]
  edge [
    source 18
    target 708
  ]
  edge [
    source 18
    target 709
  ]
  edge [
    source 18
    target 710
  ]
  edge [
    source 18
    target 711
  ]
  edge [
    source 18
    target 712
  ]
  edge [
    source 18
    target 713
  ]
  edge [
    source 18
    target 714
  ]
  edge [
    source 18
    target 715
  ]
  edge [
    source 18
    target 716
  ]
  edge [
    source 18
    target 717
  ]
  edge [
    source 18
    target 718
  ]
  edge [
    source 18
    target 719
  ]
  edge [
    source 18
    target 720
  ]
  edge [
    source 18
    target 721
  ]
  edge [
    source 18
    target 722
  ]
  edge [
    source 18
    target 723
  ]
  edge [
    source 18
    target 724
  ]
  edge [
    source 18
    target 725
  ]
  edge [
    source 18
    target 726
  ]
  edge [
    source 18
    target 727
  ]
  edge [
    source 18
    target 728
  ]
  edge [
    source 18
    target 729
  ]
  edge [
    source 18
    target 730
  ]
  edge [
    source 18
    target 731
  ]
  edge [
    source 18
    target 732
  ]
  edge [
    source 18
    target 733
  ]
  edge [
    source 18
    target 734
  ]
  edge [
    source 18
    target 735
  ]
  edge [
    source 18
    target 736
  ]
  edge [
    source 18
    target 737
  ]
  edge [
    source 18
    target 738
  ]
  edge [
    source 18
    target 739
  ]
  edge [
    source 18
    target 740
  ]
  edge [
    source 18
    target 741
  ]
  edge [
    source 18
    target 742
  ]
  edge [
    source 18
    target 743
  ]
  edge [
    source 18
    target 744
  ]
  edge [
    source 18
    target 745
  ]
  edge [
    source 18
    target 746
  ]
  edge [
    source 18
    target 747
  ]
  edge [
    source 18
    target 748
  ]
  edge [
    source 18
    target 749
  ]
  edge [
    source 18
    target 750
  ]
  edge [
    source 18
    target 751
  ]
  edge [
    source 18
    target 752
  ]
  edge [
    source 18
    target 753
  ]
  edge [
    source 18
    target 754
  ]
  edge [
    source 18
    target 755
  ]
  edge [
    source 18
    target 756
  ]
  edge [
    source 18
    target 757
  ]
  edge [
    source 18
    target 758
  ]
  edge [
    source 18
    target 759
  ]
  edge [
    source 18
    target 760
  ]
  edge [
    source 18
    target 761
  ]
  edge [
    source 18
    target 762
  ]
  edge [
    source 18
    target 763
  ]
  edge [
    source 18
    target 764
  ]
  edge [
    source 18
    target 765
  ]
  edge [
    source 18
    target 766
  ]
  edge [
    source 18
    target 767
  ]
  edge [
    source 18
    target 768
  ]
  edge [
    source 18
    target 769
  ]
  edge [
    source 18
    target 770
  ]
  edge [
    source 18
    target 771
  ]
  edge [
    source 18
    target 772
  ]
  edge [
    source 18
    target 773
  ]
  edge [
    source 18
    target 774
  ]
  edge [
    source 18
    target 775
  ]
  edge [
    source 18
    target 776
  ]
  edge [
    source 18
    target 777
  ]
  edge [
    source 18
    target 778
  ]
  edge [
    source 18
    target 779
  ]
  edge [
    source 18
    target 780
  ]
  edge [
    source 18
    target 781
  ]
  edge [
    source 18
    target 782
  ]
  edge [
    source 18
    target 783
  ]
  edge [
    source 18
    target 784
  ]
  edge [
    source 18
    target 785
  ]
  edge [
    source 18
    target 786
  ]
  edge [
    source 18
    target 787
  ]
  edge [
    source 18
    target 788
  ]
  edge [
    source 18
    target 789
  ]
  edge [
    source 18
    target 790
  ]
  edge [
    source 18
    target 791
  ]
  edge [
    source 18
    target 792
  ]
  edge [
    source 18
    target 793
  ]
  edge [
    source 18
    target 794
  ]
  edge [
    source 18
    target 795
  ]
  edge [
    source 18
    target 796
  ]
  edge [
    source 18
    target 797
  ]
  edge [
    source 18
    target 798
  ]
  edge [
    source 18
    target 799
  ]
  edge [
    source 18
    target 800
  ]
  edge [
    source 18
    target 801
  ]
  edge [
    source 18
    target 802
  ]
  edge [
    source 18
    target 803
  ]
  edge [
    source 18
    target 804
  ]
  edge [
    source 18
    target 805
  ]
  edge [
    source 18
    target 806
  ]
  edge [
    source 18
    target 807
  ]
  edge [
    source 18
    target 808
  ]
  edge [
    source 18
    target 809
  ]
  edge [
    source 18
    target 810
  ]
  edge [
    source 18
    target 811
  ]
  edge [
    source 18
    target 812
  ]
  edge [
    source 18
    target 813
  ]
  edge [
    source 18
    target 814
  ]
  edge [
    source 18
    target 815
  ]
  edge [
    source 18
    target 816
  ]
  edge [
    source 18
    target 817
  ]
  edge [
    source 18
    target 818
  ]
  edge [
    source 18
    target 819
  ]
  edge [
    source 18
    target 820
  ]
  edge [
    source 18
    target 821
  ]
  edge [
    source 18
    target 822
  ]
  edge [
    source 18
    target 823
  ]
  edge [
    source 18
    target 824
  ]
  edge [
    source 18
    target 825
  ]
  edge [
    source 18
    target 826
  ]
  edge [
    source 18
    target 827
  ]
  edge [
    source 18
    target 828
  ]
  edge [
    source 18
    target 829
  ]
  edge [
    source 18
    target 830
  ]
  edge [
    source 18
    target 831
  ]
  edge [
    source 18
    target 832
  ]
  edge [
    source 18
    target 833
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 834
  ]
  edge [
    source 19
    target 835
  ]
  edge [
    source 19
    target 836
  ]
  edge [
    source 19
    target 837
  ]
  edge [
    source 19
    target 838
  ]
  edge [
    source 19
    target 839
  ]
  edge [
    source 19
    target 840
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 841
  ]
  edge [
    source 21
    target 842
  ]
  edge [
    source 21
    target 843
  ]
  edge [
    source 21
    target 844
  ]
  edge [
    source 21
    target 845
  ]
  edge [
    source 21
    target 846
  ]
  edge [
    source 21
    target 847
  ]
  edge [
    source 21
    target 848
  ]
  edge [
    source 21
    target 849
  ]
  edge [
    source 21
    target 850
  ]
  edge [
    source 21
    target 851
  ]
  edge [
    source 21
    target 852
  ]
  edge [
    source 21
    target 853
  ]
  edge [
    source 21
    target 854
  ]
  edge [
    source 21
    target 855
  ]
  edge [
    source 21
    target 856
  ]
  edge [
    source 21
    target 857
  ]
  edge [
    source 21
    target 858
  ]
  edge [
    source 21
    target 859
  ]
]
