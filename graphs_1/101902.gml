graph [
  maxDegree 24
  minDegree 1
  meanDegree 1.9130434782608696
  density 0.04251207729468599
  graphCliqueNumber 2
  node [
    id 0
    label "przenosi&#263;"
    origin "text"
  ]
  node [
    id 1
    label "baza"
    origin "text"
  ]
  node [
    id 2
    label "access"
    origin "text"
  ]
  node [
    id 3
    label "mysql"
    origin "text"
  ]
  node [
    id 4
    label "infest"
  ]
  node [
    id 5
    label "zmienia&#263;"
  ]
  node [
    id 6
    label "wytrzyma&#263;"
  ]
  node [
    id 7
    label "dostosowywa&#263;"
  ]
  node [
    id 8
    label "rozpowszechnia&#263;"
  ]
  node [
    id 9
    label "ponosi&#263;"
  ]
  node [
    id 10
    label "przemieszcza&#263;"
  ]
  node [
    id 11
    label "move"
  ]
  node [
    id 12
    label "przelatywa&#263;"
  ]
  node [
    id 13
    label "strzela&#263;"
  ]
  node [
    id 14
    label "kopiowa&#263;"
  ]
  node [
    id 15
    label "transfer"
  ]
  node [
    id 16
    label "umieszcza&#263;"
  ]
  node [
    id 17
    label "pocisk"
  ]
  node [
    id 18
    label "circulate"
  ]
  node [
    id 19
    label "estrange"
  ]
  node [
    id 20
    label "go"
  ]
  node [
    id 21
    label "rozprzestrzenia&#263;"
  ]
  node [
    id 22
    label "pole"
  ]
  node [
    id 23
    label "s&#322;owo_kluczowe"
  ]
  node [
    id 24
    label "za&#322;o&#380;enie"
  ]
  node [
    id 25
    label "podstawa"
  ]
  node [
    id 26
    label "rekord"
  ]
  node [
    id 27
    label "system_bazy_danych"
  ]
  node [
    id 28
    label "kosmetyk"
  ]
  node [
    id 29
    label "zasadzenie"
  ]
  node [
    id 30
    label "kolumna"
  ]
  node [
    id 31
    label "documentation"
  ]
  node [
    id 32
    label "base"
  ]
  node [
    id 33
    label "zasadzi&#263;"
  ]
  node [
    id 34
    label "baseball"
  ]
  node [
    id 35
    label "stacjonowanie"
  ]
  node [
    id 36
    label "informatyka"
  ]
  node [
    id 37
    label "punkt_odniesienia"
  ]
  node [
    id 38
    label "poj&#281;cie"
  ]
  node [
    id 39
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 40
    label "boisko"
  ]
  node [
    id 41
    label "miejsce"
  ]
  node [
    id 42
    label "podstawowy"
  ]
  node [
    id 43
    label "zbi&#243;r"
  ]
  node [
    id 44
    label "MS"
  ]
  node [
    id 45
    label "Access"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 44
    target 45
  ]
]
