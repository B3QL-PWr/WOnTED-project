graph [
  maxDegree 11
  minDegree 1
  meanDegree 1.9375
  density 0.0625
  graphCliqueNumber 2
  node [
    id 0
    label "fajny"
    origin "text"
  ]
  node [
    id 1
    label "obrazek"
    origin "text"
  ]
  node [
    id 2
    label "imgur"
    origin "text"
  ]
  node [
    id 3
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 4
    label "poprawi&#263;"
    origin "text"
  ]
  node [
    id 5
    label "humor"
    origin "text"
  ]
  node [
    id 6
    label "fajnie"
  ]
  node [
    id 7
    label "dobry"
  ]
  node [
    id 8
    label "byczy"
  ]
  node [
    id 9
    label "klawy"
  ]
  node [
    id 10
    label "druk_ulotny"
  ]
  node [
    id 11
    label "opowiadanie"
  ]
  node [
    id 12
    label "rysunek"
  ]
  node [
    id 13
    label "picture"
  ]
  node [
    id 14
    label "sprawdzi&#263;"
  ]
  node [
    id 15
    label "correct"
  ]
  node [
    id 16
    label "ulepszy&#263;"
  ]
  node [
    id 17
    label "amend"
  ]
  node [
    id 18
    label "level"
  ]
  node [
    id 19
    label "pouk&#322;ada&#263;"
  ]
  node [
    id 20
    label "upomnie&#263;"
  ]
  node [
    id 21
    label "rectify"
  ]
  node [
    id 22
    label "stan"
  ]
  node [
    id 23
    label "temper"
  ]
  node [
    id 24
    label "&#347;mieszno&#347;&#263;"
  ]
  node [
    id 25
    label "mechanizm_obronny"
  ]
  node [
    id 26
    label "nastr&#243;j"
  ]
  node [
    id 27
    label "state"
  ]
  node [
    id 28
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 29
    label "samopoczucie"
  ]
  node [
    id 30
    label "fondness"
  ]
  node [
    id 31
    label "p&#322;yn_ustrojowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
]
