graph [
  maxDegree 2
  minDegree 1
  meanDegree 1.7142857142857142
  density 0.2857142857142857
  graphCliqueNumber 2
  node [
    id 0
    label "roba"
    origin "text"
  ]
  node [
    id 1
    label "czajepki"
    origin "text"
  ]
  node [
    id 2
    label "miko&#322;aj"
    origin "text"
  ]
  node [
    id 3
    label "avatarach"
    origin "text"
  ]
  node [
    id 4
    label "pisacz"
    origin "text"
  ]
  node [
    id 5
    label "komentacz"
    origin "text"
  ]
  node [
    id 6
    label "suknia"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
]
