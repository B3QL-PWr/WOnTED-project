graph [
  maxDegree 8
  minDegree 1
  meanDegree 2
  density 0.09090909090909091
  graphCliqueNumber 2
  node [
    id 0
    label "przeczyta&#263;"
    origin "text"
  ]
  node [
    id 1
    label "kruk"
    origin "text"
  ]
  node [
    id 2
    label "traktowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "sroka"
    origin "text"
  ]
  node [
    id 4
    label "pozna&#263;"
  ]
  node [
    id 5
    label "wywr&#243;&#380;y&#263;"
  ]
  node [
    id 6
    label "przetworzy&#263;"
  ]
  node [
    id 7
    label "read"
  ]
  node [
    id 8
    label "zaobserwowa&#263;"
  ]
  node [
    id 9
    label "odczyta&#263;"
  ]
  node [
    id 10
    label "krakanie"
  ]
  node [
    id 11
    label "zakraka&#263;"
  ]
  node [
    id 12
    label "ptak"
  ]
  node [
    id 13
    label "kraka&#263;"
  ]
  node [
    id 14
    label "wszystko&#380;erca"
  ]
  node [
    id 15
    label "krukowate"
  ]
  node [
    id 16
    label "use"
  ]
  node [
    id 17
    label "cz&#281;stowa&#263;"
  ]
  node [
    id 18
    label "robi&#263;"
  ]
  node [
    id 19
    label "dotyczy&#263;"
  ]
  node [
    id 20
    label "poddawa&#263;"
  ]
  node [
    id 21
    label "European_magpie"
  ]
  node [
    id 22
    label "ptak_pospolity"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 15
  ]
]
