graph [
  maxDegree 11
  minDegree 1
  meanDegree 1.8461538461538463
  density 0.15384615384615385
  graphCliqueNumber 2
  node [
    id 0
    label "prezentowa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "destin"
    origin "text"
  ]
  node [
    id 2
    label "sandlin"
    origin "text"
  ]
  node [
    id 3
    label "zapoznawa&#263;"
  ]
  node [
    id 4
    label "przedstawia&#263;"
  ]
  node [
    id 5
    label "present"
  ]
  node [
    id 6
    label "gra&#263;"
  ]
  node [
    id 7
    label "uprzedza&#263;"
  ]
  node [
    id 8
    label "represent"
  ]
  node [
    id 9
    label "program"
  ]
  node [
    id 10
    label "wyra&#380;a&#263;"
  ]
  node [
    id 11
    label "attest"
  ]
  node [
    id 12
    label "display"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 1
    target 2
  ]
]
