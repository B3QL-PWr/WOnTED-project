graph [
  maxDegree 4
  minDegree 1
  meanDegree 1.7142857142857142
  density 0.2857142857142857
  graphCliqueNumber 2
  node [
    id 0
    label "mireczka"
    origin "text"
  ]
  node [
    id 1
    label "tak"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "zarywa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "transgress"
  ]
  node [
    id 5
    label "zaleca&#263;_si&#281;"
  ]
  node [
    id 6
    label "niszczy&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 6
  ]
]
