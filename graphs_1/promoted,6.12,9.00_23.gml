graph [
  maxDegree 28
  minDegree 1
  meanDegree 2.1587301587301586
  density 0.03481822836661547
  graphCliqueNumber 5
  node [
    id 0
    label "czesc"
    origin "text"
  ]
  node [
    id 1
    label "wykopki"
    origin "text"
  ]
  node [
    id 2
    label "opowiesc"
    origin "text"
  ]
  node [
    id 3
    label "cykl"
    origin "text"
  ]
  node [
    id 4
    label "klient"
    origin "text"
  ]
  node [
    id 5
    label "versus"
    origin "text"
  ]
  node [
    id 6
    label "duza"
    origin "text"
  ]
  node [
    id 7
    label "zla"
    origin "text"
  ]
  node [
    id 8
    label "firma"
    origin "text"
  ]
  node [
    id 9
    label "zbi&#243;r"
  ]
  node [
    id 10
    label "sadzeniak"
  ]
  node [
    id 11
    label "sekwencja"
  ]
  node [
    id 12
    label "czas"
  ]
  node [
    id 13
    label "edycja"
  ]
  node [
    id 14
    label "przebieg"
  ]
  node [
    id 15
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 16
    label "okres"
  ]
  node [
    id 17
    label "cykl_p&#322;ciowy"
  ]
  node [
    id 18
    label "cycle"
  ]
  node [
    id 19
    label "owulacja"
  ]
  node [
    id 20
    label "miesi&#261;czka"
  ]
  node [
    id 21
    label "set"
  ]
  node [
    id 22
    label "cz&#322;owiek"
  ]
  node [
    id 23
    label "bratek"
  ]
  node [
    id 24
    label "klientela"
  ]
  node [
    id 25
    label "szlachcic"
  ]
  node [
    id 26
    label "agent_rozliczeniowy"
  ]
  node [
    id 27
    label "komputer_cyfrowy"
  ]
  node [
    id 28
    label "program"
  ]
  node [
    id 29
    label "us&#322;ugobiorca"
  ]
  node [
    id 30
    label "Rzymianin"
  ]
  node [
    id 31
    label "obywatel"
  ]
  node [
    id 32
    label "MAC"
  ]
  node [
    id 33
    label "Hortex"
  ]
  node [
    id 34
    label "reengineering"
  ]
  node [
    id 35
    label "nazwa_w&#322;asna"
  ]
  node [
    id 36
    label "podmiot_gospodarczy"
  ]
  node [
    id 37
    label "Google"
  ]
  node [
    id 38
    label "zaufanie"
  ]
  node [
    id 39
    label "biurowiec"
  ]
  node [
    id 40
    label "interes"
  ]
  node [
    id 41
    label "zasoby_ludzkie"
  ]
  node [
    id 42
    label "networking"
  ]
  node [
    id 43
    label "paczkarnia"
  ]
  node [
    id 44
    label "Canon"
  ]
  node [
    id 45
    label "HP"
  ]
  node [
    id 46
    label "Baltona"
  ]
  node [
    id 47
    label "Pewex"
  ]
  node [
    id 48
    label "MAN_SE"
  ]
  node [
    id 49
    label "Apeks"
  ]
  node [
    id 50
    label "zasoby"
  ]
  node [
    id 51
    label "Orbis"
  ]
  node [
    id 52
    label "miejsce_pracy"
  ]
  node [
    id 53
    label "siedziba"
  ]
  node [
    id 54
    label "Spo&#322;em"
  ]
  node [
    id 55
    label "przedsi&#281;biorczo&#347;&#263;"
  ]
  node [
    id 56
    label "Orlen"
  ]
  node [
    id 57
    label "klasa"
  ]
  node [
    id 58
    label "Agata"
  ]
  node [
    id 59
    label "mebel"
  ]
  node [
    id 60
    label "Rumia"
  ]
  node [
    id 61
    label "st&#243;&#322;"
  ]
  node [
    id 62
    label "Shetland"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 22
  ]
  edge [
    source 8
    target 32
  ]
  edge [
    source 8
    target 33
  ]
  edge [
    source 8
    target 34
  ]
  edge [
    source 8
    target 35
  ]
  edge [
    source 8
    target 36
  ]
  edge [
    source 8
    target 37
  ]
  edge [
    source 8
    target 38
  ]
  edge [
    source 8
    target 39
  ]
  edge [
    source 8
    target 40
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 8
    target 46
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 60
  ]
  edge [
    source 58
    target 61
  ]
  edge [
    source 58
    target 62
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 61
  ]
  edge [
    source 59
    target 62
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 62
  ]
  edge [
    source 61
    target 62
  ]
]
