graph [
  maxDegree 28
  minDegree 1
  meanDegree 2.109289617486339
  density 0.011589503392782082
  graphCliqueNumber 3
  node [
    id 0
    label "siemka"
    origin "text"
  ]
  node [
    id 1
    label "kolejny"
    origin "text"
  ]
  node [
    id 2
    label "dzienia"
    origin "text"
  ]
  node [
    id 3
    label "seria"
    origin "text"
  ]
  node [
    id 4
    label "nasze"
    origin "text"
  ]
  node [
    id 5
    label "pixelowego"
    origin "text"
  ]
  node [
    id 6
    label "rozdajo"
    origin "text"
  ]
  node [
    id 7
    label "pixelxmas"
    origin "text"
  ]
  node [
    id 8
    label "dzisiaj"
    origin "text"
  ]
  node [
    id 9
    label "mama"
    origin "text"
  ]
  node [
    id 10
    label "dla"
    origin "text"
  ]
  node [
    id 11
    label "co&#347;"
    origin "text"
  ]
  node [
    id 12
    label "zwi&#261;za&#263;"
    origin "text"
  ]
  node [
    id 13
    label "pubg"
    origin "text"
  ]
  node [
    id 14
    label "by&#263;"
    origin "text"
  ]
  node [
    id 15
    label "patch"
    origin "text"
  ]
  node [
    id 16
    label "crate"
    origin "text"
  ]
  node [
    id 17
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 18
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 19
    label "wylosowa&#263;"
    origin "text"
  ]
  node [
    id 20
    label "naszywka"
    origin "text"
  ]
  node [
    id 21
    label "rzep"
    origin "text"
  ]
  node [
    id 22
    label "przyczepi&#263;"
    origin "text"
  ]
  node [
    id 23
    label "siebie"
    origin "text"
  ]
  node [
    id 24
    label "plecak"
    origin "text"
  ]
  node [
    id 25
    label "koszulka"
    origin "text"
  ]
  node [
    id 26
    label "oboj&#281;tnie"
    origin "text"
  ]
  node [
    id 27
    label "standardowo"
    origin "text"
  ]
  node [
    id 28
    label "wygrywa&#263;"
    origin "text"
  ]
  node [
    id 29
    label "spo&#347;r&#243;d"
    origin "text"
  ]
  node [
    id 30
    label "wszyscy"
    origin "text"
  ]
  node [
    id 31
    label "pulsuj&#261;cy"
    origin "text"
  ]
  node [
    id 32
    label "ale"
    origin "text"
  ]
  node [
    id 33
    label "zwi&#281;kszy&#263;"
    origin "text"
  ]
  node [
    id 34
    label "sw&#243;j"
    origin "text"
  ]
  node [
    id 35
    label "wrzuca&#263;"
    origin "text"
  ]
  node [
    id 36
    label "humorobrazkowy"
    origin "text"
  ]
  node [
    id 37
    label "tylko"
    origin "text"
  ]
  node [
    id 38
    label "obserwowa&#263;"
    origin "text"
  ]
  node [
    id 39
    label "pixelday"
    origin "text"
  ]
  node [
    id 40
    label "ma&#322;o"
    origin "text"
  ]
  node [
    id 41
    label "bardziej"
    origin "text"
  ]
  node [
    id 42
    label "popkulturowy"
    origin "text"
  ]
  node [
    id 43
    label "gamingow&#261;"
    origin "text"
  ]
  node [
    id 44
    label "inny"
  ]
  node [
    id 45
    label "nast&#281;pnie"
  ]
  node [
    id 46
    label "kt&#243;ry&#347;"
  ]
  node [
    id 47
    label "kolejno"
  ]
  node [
    id 48
    label "nastopny"
  ]
  node [
    id 49
    label "stage_set"
  ]
  node [
    id 50
    label "partia"
  ]
  node [
    id 51
    label "przekr&#243;j_geologiczny"
  ]
  node [
    id 52
    label "sekwencja"
  ]
  node [
    id 53
    label "komplet"
  ]
  node [
    id 54
    label "przebieg"
  ]
  node [
    id 55
    label "zestawienie"
  ]
  node [
    id 56
    label "jednostka_systematyczna"
  ]
  node [
    id 57
    label "d&#378;wi&#281;k"
  ]
  node [
    id 58
    label "line"
  ]
  node [
    id 59
    label "zbi&#243;r"
  ]
  node [
    id 60
    label "produkcja"
  ]
  node [
    id 61
    label "set"
  ]
  node [
    id 62
    label "jednostka"
  ]
  node [
    id 63
    label "doba"
  ]
  node [
    id 64
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 65
    label "dzi&#347;"
  ]
  node [
    id 66
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 67
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 68
    label "matczysko"
  ]
  node [
    id 69
    label "macierz"
  ]
  node [
    id 70
    label "przodkini"
  ]
  node [
    id 71
    label "Matka_Boska"
  ]
  node [
    id 72
    label "macocha"
  ]
  node [
    id 73
    label "matka_zast&#281;pcza"
  ]
  node [
    id 74
    label "stara"
  ]
  node [
    id 75
    label "rodzice"
  ]
  node [
    id 76
    label "rodzic"
  ]
  node [
    id 77
    label "thing"
  ]
  node [
    id 78
    label "cosik"
  ]
  node [
    id 79
    label "st&#281;&#380;e&#263;"
  ]
  node [
    id 80
    label "tobo&#322;ek"
  ]
  node [
    id 81
    label "obezw&#322;adni&#263;"
  ]
  node [
    id 82
    label "scali&#263;"
  ]
  node [
    id 83
    label "zawi&#261;za&#263;"
  ]
  node [
    id 84
    label "zatrzyma&#263;"
  ]
  node [
    id 85
    label "form"
  ]
  node [
    id 86
    label "bind"
  ]
  node [
    id 87
    label "wi&#261;zanie_chemiczne"
  ]
  node [
    id 88
    label "unify"
  ]
  node [
    id 89
    label "consort"
  ]
  node [
    id 90
    label "incorporate"
  ]
  node [
    id 91
    label "wi&#281;&#378;"
  ]
  node [
    id 92
    label "zobowi&#261;za&#263;"
  ]
  node [
    id 93
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 94
    label "w&#281;ze&#322;"
  ]
  node [
    id 95
    label "cz&#261;steczka_chemiczna"
  ]
  node [
    id 96
    label "powi&#261;za&#263;"
  ]
  node [
    id 97
    label "opakowa&#263;"
  ]
  node [
    id 98
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 99
    label "cement"
  ]
  node [
    id 100
    label "zaprawa"
  ]
  node [
    id 101
    label "relate"
  ]
  node [
    id 102
    label "si&#281;ga&#263;"
  ]
  node [
    id 103
    label "trwa&#263;"
  ]
  node [
    id 104
    label "obecno&#347;&#263;"
  ]
  node [
    id 105
    label "stan"
  ]
  node [
    id 106
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 107
    label "stand"
  ]
  node [
    id 108
    label "mie&#263;_miejsce"
  ]
  node [
    id 109
    label "uczestniczy&#263;"
  ]
  node [
    id 110
    label "chodzi&#263;"
  ]
  node [
    id 111
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 112
    label "equal"
  ]
  node [
    id 113
    label "uprawi&#263;"
  ]
  node [
    id 114
    label "gotowy"
  ]
  node [
    id 115
    label "might"
  ]
  node [
    id 116
    label "wybra&#263;"
  ]
  node [
    id 117
    label "wygra&#263;"
  ]
  node [
    id 118
    label "band"
  ]
  node [
    id 119
    label "rockers"
  ]
  node [
    id 120
    label "harleyowiec"
  ]
  node [
    id 121
    label "mundur"
  ]
  node [
    id 122
    label "metal"
  ]
  node [
    id 123
    label "naszycie"
  ]
  node [
    id 124
    label "hardrockowiec"
  ]
  node [
    id 125
    label "szamerunek"
  ]
  node [
    id 126
    label "logo"
  ]
  node [
    id 127
    label "nie&#322;upka"
  ]
  node [
    id 128
    label "bur"
  ]
  node [
    id 129
    label "zapi&#281;cie"
  ]
  node [
    id 130
    label "cook"
  ]
  node [
    id 131
    label "przymocowa&#263;"
  ]
  node [
    id 132
    label "poczepia&#263;"
  ]
  node [
    id 133
    label "backpacking"
  ]
  node [
    id 134
    label "torba"
  ]
  node [
    id 135
    label "pack"
  ]
  node [
    id 136
    label "ubranko"
  ]
  node [
    id 137
    label "os&#322;ona"
  ]
  node [
    id 138
    label "g&#243;ra"
  ]
  node [
    id 139
    label "artyku&#322;"
  ]
  node [
    id 140
    label "niemowl&#281;"
  ]
  node [
    id 141
    label "zwyczajnie"
  ]
  node [
    id 142
    label "nieuwa&#380;nie"
  ]
  node [
    id 143
    label "spokojnie"
  ]
  node [
    id 144
    label "nieszkodliwie"
  ]
  node [
    id 145
    label "nieistotnie"
  ]
  node [
    id 146
    label "oboj&#281;tny"
  ]
  node [
    id 147
    label "neutralnie"
  ]
  node [
    id 148
    label "schematyczny"
  ]
  node [
    id 149
    label "typowo"
  ]
  node [
    id 150
    label "standardowy"
  ]
  node [
    id 151
    label "zagwarantowywa&#263;"
  ]
  node [
    id 152
    label "znosi&#263;"
  ]
  node [
    id 153
    label "gra&#263;"
  ]
  node [
    id 154
    label "osi&#261;ga&#263;"
  ]
  node [
    id 155
    label "okazywa&#263;_si&#281;"
  ]
  node [
    id 156
    label "robi&#263;"
  ]
  node [
    id 157
    label "strike"
  ]
  node [
    id 158
    label "instrument_muzyczny"
  ]
  node [
    id 159
    label "muzykowa&#263;"
  ]
  node [
    id 160
    label "play"
  ]
  node [
    id 161
    label "net_income"
  ]
  node [
    id 162
    label "piwo"
  ]
  node [
    id 163
    label "zmieni&#263;"
  ]
  node [
    id 164
    label "ascend"
  ]
  node [
    id 165
    label "cz&#322;owiek"
  ]
  node [
    id 166
    label "bli&#378;ni"
  ]
  node [
    id 167
    label "odpowiedni"
  ]
  node [
    id 168
    label "swojak"
  ]
  node [
    id 169
    label "samodzielny"
  ]
  node [
    id 170
    label "give"
  ]
  node [
    id 171
    label "umieszcza&#263;"
  ]
  node [
    id 172
    label "patrze&#263;"
  ]
  node [
    id 173
    label "dostrzega&#263;"
  ]
  node [
    id 174
    label "look"
  ]
  node [
    id 175
    label "mo&#380;liwie"
  ]
  node [
    id 176
    label "nieznaczny"
  ]
  node [
    id 177
    label "kr&#243;tko"
  ]
  node [
    id 178
    label "nieliczny"
  ]
  node [
    id 179
    label "mikroskopijnie"
  ]
  node [
    id 180
    label "pomiernie"
  ]
  node [
    id 181
    label "ma&#322;y"
  ]
  node [
    id 182
    label "popularny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 77
  ]
  edge [
    source 11
    target 78
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 41
  ]
  edge [
    source 12
    target 42
  ]
  edge [
    source 12
    target 79
  ]
  edge [
    source 12
    target 80
  ]
  edge [
    source 12
    target 81
  ]
  edge [
    source 12
    target 82
  ]
  edge [
    source 12
    target 83
  ]
  edge [
    source 12
    target 84
  ]
  edge [
    source 12
    target 85
  ]
  edge [
    source 12
    target 86
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 12
    target 89
  ]
  edge [
    source 12
    target 90
  ]
  edge [
    source 12
    target 91
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 12
    target 94
  ]
  edge [
    source 12
    target 95
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 24
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 102
  ]
  edge [
    source 14
    target 103
  ]
  edge [
    source 14
    target 104
  ]
  edge [
    source 14
    target 105
  ]
  edge [
    source 14
    target 106
  ]
  edge [
    source 14
    target 107
  ]
  edge [
    source 14
    target 108
  ]
  edge [
    source 14
    target 109
  ]
  edge [
    source 14
    target 110
  ]
  edge [
    source 14
    target 111
  ]
  edge [
    source 14
    target 112
  ]
  edge [
    source 14
    target 18
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 21
  ]
  edge [
    source 17
    target 22
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 32
  ]
  edge [
    source 18
    target 33
  ]
  edge [
    source 18
    target 113
  ]
  edge [
    source 18
    target 114
  ]
  edge [
    source 18
    target 115
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 116
  ]
  edge [
    source 19
    target 117
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 118
  ]
  edge [
    source 20
    target 119
  ]
  edge [
    source 20
    target 120
  ]
  edge [
    source 20
    target 121
  ]
  edge [
    source 20
    target 122
  ]
  edge [
    source 20
    target 123
  ]
  edge [
    source 20
    target 124
  ]
  edge [
    source 20
    target 125
  ]
  edge [
    source 20
    target 126
  ]
  edge [
    source 21
    target 127
  ]
  edge [
    source 21
    target 128
  ]
  edge [
    source 21
    target 129
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 130
  ]
  edge [
    source 22
    target 131
  ]
  edge [
    source 22
    target 132
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 28
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 133
  ]
  edge [
    source 24
    target 134
  ]
  edge [
    source 24
    target 135
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 136
  ]
  edge [
    source 25
    target 137
  ]
  edge [
    source 25
    target 138
  ]
  edge [
    source 25
    target 139
  ]
  edge [
    source 25
    target 140
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 141
  ]
  edge [
    source 26
    target 142
  ]
  edge [
    source 26
    target 143
  ]
  edge [
    source 26
    target 144
  ]
  edge [
    source 26
    target 145
  ]
  edge [
    source 26
    target 146
  ]
  edge [
    source 26
    target 147
  ]
  edge [
    source 26
    target 33
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 141
  ]
  edge [
    source 27
    target 148
  ]
  edge [
    source 27
    target 149
  ]
  edge [
    source 27
    target 150
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 151
  ]
  edge [
    source 28
    target 108
  ]
  edge [
    source 28
    target 152
  ]
  edge [
    source 28
    target 153
  ]
  edge [
    source 28
    target 154
  ]
  edge [
    source 28
    target 155
  ]
  edge [
    source 28
    target 156
  ]
  edge [
    source 28
    target 157
  ]
  edge [
    source 28
    target 158
  ]
  edge [
    source 28
    target 159
  ]
  edge [
    source 28
    target 160
  ]
  edge [
    source 28
    target 161
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 32
    target 162
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 163
  ]
  edge [
    source 33
    target 164
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 165
  ]
  edge [
    source 34
    target 166
  ]
  edge [
    source 34
    target 167
  ]
  edge [
    source 34
    target 168
  ]
  edge [
    source 34
    target 169
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 170
  ]
  edge [
    source 35
    target 171
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 38
    target 172
  ]
  edge [
    source 38
    target 173
  ]
  edge [
    source 38
    target 174
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 175
  ]
  edge [
    source 40
    target 176
  ]
  edge [
    source 40
    target 177
  ]
  edge [
    source 40
    target 145
  ]
  edge [
    source 40
    target 178
  ]
  edge [
    source 40
    target 179
  ]
  edge [
    source 40
    target 180
  ]
  edge [
    source 40
    target 181
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 182
  ]
]
