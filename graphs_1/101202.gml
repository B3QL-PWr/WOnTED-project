graph [
  maxDegree 13
  minDegree 1
  meanDegree 2
  density 0.14285714285714285
  graphCliqueNumber 3
  node [
    id 0
    label "standard"
    origin "text"
  ]
  node [
    id 1
    label "reference"
    origin "text"
  ]
  node [
    id 2
    label "method"
    origin "text"
  ]
  node [
    id 3
    label "zorganizowa&#263;"
  ]
  node [
    id 4
    label "model"
  ]
  node [
    id 5
    label "zwyk&#322;o&#347;&#263;"
  ]
  node [
    id 6
    label "taniec_towarzyski"
  ]
  node [
    id 7
    label "ordinariness"
  ]
  node [
    id 8
    label "organizowanie"
  ]
  node [
    id 9
    label "criterion"
  ]
  node [
    id 10
    label "zorganizowanie"
  ]
  node [
    id 11
    label "instytucja"
  ]
  node [
    id 12
    label "organizowa&#263;"
  ]
  node [
    id 13
    label "Reference"
  ]
  node [
    id 14
    label "Method"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 13
    target 14
  ]
]
