graph [
  maxDegree 16
  minDegree 1
  meanDegree 1.9583333333333333
  density 0.041666666666666664
  graphCliqueNumber 2
  node [
    id 0
    label "krakowskie"
    origin "text"
  ]
  node [
    id 1
    label "szopkarstwo"
    origin "text"
  ]
  node [
    id 2
    label "trafi&#263;"
    origin "text"
  ]
  node [
    id 3
    label "lista"
    origin "text"
  ]
  node [
    id 4
    label "reprezentatywny"
    origin "text"
  ]
  node [
    id 5
    label "niematerialny"
    origin "text"
  ]
  node [
    id 6
    label "dziedzictwo"
    origin "text"
  ]
  node [
    id 7
    label "kulturowy"
    origin "text"
  ]
  node [
    id 8
    label "ludzko&#347;&#263;"
    origin "text"
  ]
  node [
    id 9
    label "sztuka_ludowa"
  ]
  node [
    id 10
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 11
    label "przypasowa&#263;"
  ]
  node [
    id 12
    label "wpa&#347;&#263;"
  ]
  node [
    id 13
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 14
    label "spotka&#263;"
  ]
  node [
    id 15
    label "dotrze&#263;"
  ]
  node [
    id 16
    label "zjawi&#263;_si&#281;"
  ]
  node [
    id 17
    label "happen"
  ]
  node [
    id 18
    label "znale&#378;&#263;"
  ]
  node [
    id 19
    label "hit"
  ]
  node [
    id 20
    label "pocisk"
  ]
  node [
    id 21
    label "stumble"
  ]
  node [
    id 22
    label "dolecie&#263;"
  ]
  node [
    id 23
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 24
    label "wyliczanka"
  ]
  node [
    id 25
    label "catalog"
  ]
  node [
    id 26
    label "stock"
  ]
  node [
    id 27
    label "figurowa&#263;"
  ]
  node [
    id 28
    label "zbi&#243;r"
  ]
  node [
    id 29
    label "book"
  ]
  node [
    id 30
    label "pozycja"
  ]
  node [
    id 31
    label "tekst"
  ]
  node [
    id 32
    label "sumariusz"
  ]
  node [
    id 33
    label "dobry"
  ]
  node [
    id 34
    label "typowy"
  ]
  node [
    id 35
    label "dematerializowanie"
  ]
  node [
    id 36
    label "niematerialnie"
  ]
  node [
    id 37
    label "zdematerializowanie"
  ]
  node [
    id 38
    label "wydziedziczy&#263;"
  ]
  node [
    id 39
    label "zachowek"
  ]
  node [
    id 40
    label "wydziedziczenie"
  ]
  node [
    id 41
    label "prawo"
  ]
  node [
    id 42
    label "mienie"
  ]
  node [
    id 43
    label "scheda_spadkowa"
  ]
  node [
    id 44
    label "sukcesja"
  ]
  node [
    id 45
    label "kulturowo"
  ]
  node [
    id 46
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 47
    label "cz&#322;owiek"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 8
    target 46
  ]
  edge [
    source 8
    target 47
  ]
]
