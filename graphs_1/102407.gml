graph [
  maxDegree 24
  minDegree 1
  meanDegree 1.9682539682539681
  density 0.031746031746031744
  graphCliqueNumber 2
  node [
    id 0
    label "okazja"
    origin "text"
  ]
  node [
    id 1
    label "&#347;wi&#281;to"
    origin "text"
  ]
  node [
    id 2
    label "bo&#380;y"
    origin "text"
  ]
  node [
    id 3
    label "narodzenie"
    origin "text"
  ]
  node [
    id 4
    label "pragn&#261;&#263;"
    origin "text"
  ]
  node [
    id 5
    label "z&#322;o&#380;y&#263;"
    origin "text"
  ]
  node [
    id 6
    label "serdeczny"
    origin "text"
  ]
  node [
    id 7
    label "&#380;yczenia"
    origin "text"
  ]
  node [
    id 8
    label "atrakcyjny"
  ]
  node [
    id 9
    label "oferta"
  ]
  node [
    id 10
    label "adeptness"
  ]
  node [
    id 11
    label "okazka"
  ]
  node [
    id 12
    label "wydarzenie"
  ]
  node [
    id 13
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 14
    label "podw&#243;zka"
  ]
  node [
    id 15
    label "autostop"
  ]
  node [
    id 16
    label "sytuacja"
  ]
  node [
    id 17
    label "&#346;wi&#281;to_Pracy"
  ]
  node [
    id 18
    label "czas"
  ]
  node [
    id 19
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 20
    label "Nowy_Rok"
  ]
  node [
    id 21
    label "Godowe_&#346;wi&#281;to"
  ]
  node [
    id 22
    label "dzie&#324;_&#347;wi&#261;teczny"
  ]
  node [
    id 23
    label "Barb&#243;rka"
  ]
  node [
    id 24
    label "ramadan"
  ]
  node [
    id 25
    label "urodzenie"
  ]
  node [
    id 26
    label "desire"
  ]
  node [
    id 27
    label "&#322;akn&#261;&#263;"
  ]
  node [
    id 28
    label "chcie&#263;"
  ]
  node [
    id 29
    label "czu&#263;"
  ]
  node [
    id 30
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 31
    label "t&#281;skni&#263;"
  ]
  node [
    id 32
    label "zestaw"
  ]
  node [
    id 33
    label "scali&#263;"
  ]
  node [
    id 34
    label "przywr&#243;ci&#263;"
  ]
  node [
    id 35
    label "give"
  ]
  node [
    id 36
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 37
    label "note"
  ]
  node [
    id 38
    label "set"
  ]
  node [
    id 39
    label "da&#263;"
  ]
  node [
    id 40
    label "marshal"
  ]
  node [
    id 41
    label "opracowa&#263;"
  ]
  node [
    id 42
    label "przekaza&#263;"
  ]
  node [
    id 43
    label "zmieni&#263;"
  ]
  node [
    id 44
    label "pay"
  ]
  node [
    id 45
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 46
    label "odda&#263;"
  ]
  node [
    id 47
    label "przy&#322;o&#380;y&#263;"
  ]
  node [
    id 48
    label "jell"
  ]
  node [
    id 49
    label "spowodowa&#263;"
  ]
  node [
    id 50
    label "frame"
  ]
  node [
    id 51
    label "u&#322;o&#380;y&#263;"
  ]
  node [
    id 52
    label "zebra&#263;"
  ]
  node [
    id 53
    label "fold"
  ]
  node [
    id 54
    label "szczery"
  ]
  node [
    id 55
    label "&#380;yczliwy"
  ]
  node [
    id 56
    label "serdecznie"
  ]
  node [
    id 57
    label "siarczysty"
  ]
  node [
    id 58
    label "mi&#322;y"
  ]
  node [
    id 59
    label "ciep&#322;y"
  ]
  node [
    id 60
    label "drogi"
  ]
  node [
    id 61
    label "serdeczno&#347;ci"
  ]
  node [
    id 62
    label "praise"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
]
