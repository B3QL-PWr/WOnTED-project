graph [
  maxDegree 47
  minDegree 1
  meanDegree 2.0568181818181817
  density 0.011753246753246753
  graphCliqueNumber 2
  node [
    id 0
    label "nieokre&#347;lony"
    origin "text"
  ]
  node [
    id 1
    label "zapach"
    origin "text"
  ]
  node [
    id 2
    label "dawno"
    origin "text"
  ]
  node [
    id 3
    label "wietrzy&#263;"
    origin "text"
  ]
  node [
    id 4
    label "mieszkanie"
    origin "text"
  ]
  node [
    id 5
    label "dobre"
    origin "text"
  ]
  node [
    id 6
    label "&#347;wiat&#322;o"
    origin "text"
  ]
  node [
    id 7
    label "oceni&#263;"
    origin "text"
  ]
  node [
    id 8
    label "my&#347;l"
    origin "text"
  ]
  node [
    id 9
    label "dwa"
    origin "text"
  ]
  node [
    id 10
    label "duha"
    origin "text"
  ]
  node [
    id 11
    label "okno"
    origin "text"
  ]
  node [
    id 12
    label "ponad"
    origin "text"
  ]
  node [
    id 13
    label "dach"
    origin "text"
  ]
  node [
    id 14
    label "kamienica"
    origin "text"
  ]
  node [
    id 15
    label "lato"
    origin "text"
  ]
  node [
    id 16
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 17
    label "by&#263;"
    origin "text"
  ]
  node [
    id 18
    label "gor&#261;co"
    origin "text"
  ]
  node [
    id 19
    label "zamazywanie"
  ]
  node [
    id 20
    label "zamazanie"
  ]
  node [
    id 21
    label "niewyrazisto"
  ]
  node [
    id 22
    label "przyprawa"
  ]
  node [
    id 23
    label "kosmetyk"
  ]
  node [
    id 24
    label "wpa&#347;&#263;"
  ]
  node [
    id 25
    label "wpada&#263;"
  ]
  node [
    id 26
    label "wpadanie"
  ]
  node [
    id 27
    label "wydanie"
  ]
  node [
    id 28
    label "wyda&#263;"
  ]
  node [
    id 29
    label "liczba_kwantowa"
  ]
  node [
    id 30
    label "puff"
  ]
  node [
    id 31
    label "aromat"
  ]
  node [
    id 32
    label "upojno&#347;&#263;"
  ]
  node [
    id 33
    label "fizyka_cz&#261;stek_elementarnych"
  ]
  node [
    id 34
    label "owiewanie"
  ]
  node [
    id 35
    label "ciasto"
  ]
  node [
    id 36
    label "wydawa&#263;"
  ]
  node [
    id 37
    label "smak"
  ]
  node [
    id 38
    label "zjawisko"
  ]
  node [
    id 39
    label "wpadni&#281;cie"
  ]
  node [
    id 40
    label "zapachowo&#347;&#263;"
  ]
  node [
    id 41
    label "dawny"
  ]
  node [
    id 42
    label "ongi&#347;"
  ]
  node [
    id 43
    label "dawnie"
  ]
  node [
    id 44
    label "wcze&#347;niej"
  ]
  node [
    id 45
    label "d&#322;ugotrwale"
  ]
  node [
    id 46
    label "czy&#347;ci&#263;"
  ]
  node [
    id 47
    label "vent"
  ]
  node [
    id 48
    label "poddawa&#263;"
  ]
  node [
    id 49
    label "powietrze"
  ]
  node [
    id 50
    label "air"
  ]
  node [
    id 51
    label "tropi&#263;"
  ]
  node [
    id 52
    label "domy&#347;la&#263;_si&#281;"
  ]
  node [
    id 53
    label "stanie"
  ]
  node [
    id 54
    label "przebywanie"
  ]
  node [
    id 55
    label "panowanie"
  ]
  node [
    id 56
    label "zajmowanie"
  ]
  node [
    id 57
    label "pomieszkanie"
  ]
  node [
    id 58
    label "adjustment"
  ]
  node [
    id 59
    label "modu&#322;_mieszkalny"
  ]
  node [
    id 60
    label "lokal"
  ]
  node [
    id 61
    label "kwadrat"
  ]
  node [
    id 62
    label "animation"
  ]
  node [
    id 63
    label "dom"
  ]
  node [
    id 64
    label "&#347;wieci&#263;"
  ]
  node [
    id 65
    label "o&#347;wietlenie"
  ]
  node [
    id 66
    label "&#347;wiat&#322;ocie&#324;"
  ]
  node [
    id 67
    label "sk&#322;ad_drukarski"
  ]
  node [
    id 68
    label "rozja&#347;nia&#263;"
  ]
  node [
    id 69
    label "punkt_widzenia"
  ]
  node [
    id 70
    label "obsadnik"
  ]
  node [
    id 71
    label "rzuca&#263;"
  ]
  node [
    id 72
    label "rzucenie"
  ]
  node [
    id 73
    label "promieniowanie_optyczne"
  ]
  node [
    id 74
    label "promieniowanie_elektromagnetyczne"
  ]
  node [
    id 75
    label "light"
  ]
  node [
    id 76
    label "przy&#263;mienie"
  ]
  node [
    id 77
    label "odst&#281;p"
  ]
  node [
    id 78
    label "interpretacja"
  ]
  node [
    id 79
    label "przeb&#322;yskiwanie"
  ]
  node [
    id 80
    label "przy&#263;miewa&#263;"
  ]
  node [
    id 81
    label "licznik_energii_elektrycznej"
  ]
  node [
    id 82
    label "lighting"
  ]
  node [
    id 83
    label "rzuci&#263;"
  ]
  node [
    id 84
    label "cecha"
  ]
  node [
    id 85
    label "plama"
  ]
  node [
    id 86
    label "radiance"
  ]
  node [
    id 87
    label "przy&#263;miewanie"
  ]
  node [
    id 88
    label "fotokataliza"
  ]
  node [
    id 89
    label "ja&#347;nia"
  ]
  node [
    id 90
    label "m&#261;drze"
  ]
  node [
    id 91
    label "rzucanie"
  ]
  node [
    id 92
    label "&#347;wiecenie"
  ]
  node [
    id 93
    label "&#347;rednica"
  ]
  node [
    id 94
    label "energia"
  ]
  node [
    id 95
    label "b&#322;ysk"
  ]
  node [
    id 96
    label "promie&#324;"
  ]
  node [
    id 97
    label "instalacja"
  ]
  node [
    id 98
    label "przy&#263;mi&#263;"
  ]
  node [
    id 99
    label "lighter"
  ]
  node [
    id 100
    label "&#347;wiat&#322;y"
  ]
  node [
    id 101
    label "przeb&#322;yskiwa&#263;"
  ]
  node [
    id 102
    label "lampa"
  ]
  node [
    id 103
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 104
    label "okre&#347;li&#263;"
  ]
  node [
    id 105
    label "evaluate"
  ]
  node [
    id 106
    label "znale&#378;&#263;"
  ]
  node [
    id 107
    label "wystawi&#263;"
  ]
  node [
    id 108
    label "pomy&#347;le&#263;"
  ]
  node [
    id 109
    label "visualize"
  ]
  node [
    id 110
    label "zrobi&#263;"
  ]
  node [
    id 111
    label "system"
  ]
  node [
    id 112
    label "s&#261;d"
  ]
  node [
    id 113
    label "wytw&#243;r"
  ]
  node [
    id 114
    label "istota"
  ]
  node [
    id 115
    label "thinking"
  ]
  node [
    id 116
    label "idea"
  ]
  node [
    id 117
    label "political_orientation"
  ]
  node [
    id 118
    label "pomys&#322;"
  ]
  node [
    id 119
    label "szko&#322;a"
  ]
  node [
    id 120
    label "umys&#322;"
  ]
  node [
    id 121
    label "fantomatyka"
  ]
  node [
    id 122
    label "t&#322;oczenie_si&#281;"
  ]
  node [
    id 123
    label "p&#322;&#243;d"
  ]
  node [
    id 124
    label "lufcik"
  ]
  node [
    id 125
    label "pasek_narz&#281;dzi"
  ]
  node [
    id 126
    label "parapet"
  ]
  node [
    id 127
    label "otw&#243;r"
  ]
  node [
    id 128
    label "skrzyd&#322;o"
  ]
  node [
    id 129
    label "kwatera_okienna"
  ]
  node [
    id 130
    label "szyba"
  ]
  node [
    id 131
    label "futryna"
  ]
  node [
    id 132
    label "nadokiennik"
  ]
  node [
    id 133
    label "interfejs"
  ]
  node [
    id 134
    label "inspekt"
  ]
  node [
    id 135
    label "program"
  ]
  node [
    id 136
    label "casement"
  ]
  node [
    id 137
    label "prze&#347;wit"
  ]
  node [
    id 138
    label "menad&#380;er_okien"
  ]
  node [
    id 139
    label "pulpit"
  ]
  node [
    id 140
    label "transenna"
  ]
  node [
    id 141
    label "nora"
  ]
  node [
    id 142
    label "okiennica"
  ]
  node [
    id 143
    label "okap"
  ]
  node [
    id 144
    label "konstrukcja"
  ]
  node [
    id 145
    label "garderoba"
  ]
  node [
    id 146
    label "obr&#243;bka_blacharska"
  ]
  node [
    id 147
    label "budynek"
  ]
  node [
    id 148
    label "&#347;lemi&#281;"
  ]
  node [
    id 149
    label "po&#322;a&#263;_dachowa"
  ]
  node [
    id 150
    label "wi&#281;&#378;ba"
  ]
  node [
    id 151
    label "podsufitka"
  ]
  node [
    id 152
    label "nadwozie"
  ]
  node [
    id 153
    label "pokrycie_dachowe"
  ]
  node [
    id 154
    label "siedziba"
  ]
  node [
    id 155
    label "dom_wielorodzinny"
  ]
  node [
    id 156
    label "pora_roku"
  ]
  node [
    id 157
    label "si&#281;ga&#263;"
  ]
  node [
    id 158
    label "trwa&#263;"
  ]
  node [
    id 159
    label "obecno&#347;&#263;"
  ]
  node [
    id 160
    label "stan"
  ]
  node [
    id 161
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 162
    label "stand"
  ]
  node [
    id 163
    label "mie&#263;_miejsce"
  ]
  node [
    id 164
    label "uczestniczy&#263;"
  ]
  node [
    id 165
    label "chodzi&#263;"
  ]
  node [
    id 166
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 167
    label "equal"
  ]
  node [
    id 168
    label "ardor"
  ]
  node [
    id 169
    label "szkodliwie"
  ]
  node [
    id 170
    label "gor&#261;cy"
  ]
  node [
    id 171
    label "seksownie"
  ]
  node [
    id 172
    label "serdecznie"
  ]
  node [
    id 173
    label "g&#322;&#281;boko"
  ]
  node [
    id 174
    label "ciep&#322;o"
  ]
  node [
    id 175
    label "war"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 24
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 8
    target 122
  ]
  edge [
    source 8
    target 123
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 124
  ]
  edge [
    source 11
    target 125
  ]
  edge [
    source 11
    target 126
  ]
  edge [
    source 11
    target 127
  ]
  edge [
    source 11
    target 128
  ]
  edge [
    source 11
    target 129
  ]
  edge [
    source 11
    target 130
  ]
  edge [
    source 11
    target 131
  ]
  edge [
    source 11
    target 132
  ]
  edge [
    source 11
    target 133
  ]
  edge [
    source 11
    target 134
  ]
  edge [
    source 11
    target 135
  ]
  edge [
    source 11
    target 136
  ]
  edge [
    source 11
    target 137
  ]
  edge [
    source 11
    target 138
  ]
  edge [
    source 11
    target 139
  ]
  edge [
    source 11
    target 140
  ]
  edge [
    source 11
    target 141
  ]
  edge [
    source 11
    target 142
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 143
  ]
  edge [
    source 13
    target 144
  ]
  edge [
    source 13
    target 145
  ]
  edge [
    source 13
    target 146
  ]
  edge [
    source 13
    target 147
  ]
  edge [
    source 13
    target 148
  ]
  edge [
    source 13
    target 149
  ]
  edge [
    source 13
    target 150
  ]
  edge [
    source 13
    target 151
  ]
  edge [
    source 13
    target 152
  ]
  edge [
    source 13
    target 153
  ]
  edge [
    source 13
    target 154
  ]
  edge [
    source 13
    target 63
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 155
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 156
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 157
  ]
  edge [
    source 17
    target 158
  ]
  edge [
    source 17
    target 159
  ]
  edge [
    source 17
    target 160
  ]
  edge [
    source 17
    target 161
  ]
  edge [
    source 17
    target 162
  ]
  edge [
    source 17
    target 163
  ]
  edge [
    source 17
    target 164
  ]
  edge [
    source 17
    target 165
  ]
  edge [
    source 17
    target 166
  ]
  edge [
    source 17
    target 167
  ]
  edge [
    source 18
    target 168
  ]
  edge [
    source 18
    target 169
  ]
  edge [
    source 18
    target 170
  ]
  edge [
    source 18
    target 171
  ]
  edge [
    source 18
    target 172
  ]
  edge [
    source 18
    target 173
  ]
  edge [
    source 18
    target 174
  ]
  edge [
    source 18
    target 175
  ]
]
