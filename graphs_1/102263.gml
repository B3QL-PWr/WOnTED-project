graph [
  maxDegree 30
  minDegree 1
  meanDegree 1.972972972972973
  density 0.02702702702702703
  graphCliqueNumber 2
  node [
    id 0
    label "bohater"
    origin "text"
  ]
  node [
    id 1
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 2
    label "imi&#281;"
    origin "text"
  ]
  node [
    id 3
    label "oskar"
    origin "text"
  ]
  node [
    id 4
    label "gra"
    origin "text"
  ]
  node [
    id 5
    label "tamburyn"
    origin "text"
  ]
  node [
    id 6
    label "bohaterski"
  ]
  node [
    id 7
    label "cz&#322;owiek"
  ]
  node [
    id 8
    label "Zgredek"
  ]
  node [
    id 9
    label "Herkules"
  ]
  node [
    id 10
    label "Casanova"
  ]
  node [
    id 11
    label "Borewicz"
  ]
  node [
    id 12
    label "Don_Juan"
  ]
  node [
    id 13
    label "posta&#263;_mitologiczna"
  ]
  node [
    id 14
    label "Winnetou"
  ]
  node [
    id 15
    label "niezwyci&#281;&#380;ony"
  ]
  node [
    id 16
    label "Messi"
  ]
  node [
    id 17
    label "Herkules_Poirot"
  ]
  node [
    id 18
    label "cz&#322;owiek_wielkiego_ducha"
  ]
  node [
    id 19
    label "Szwejk"
  ]
  node [
    id 20
    label "Sherlock_Holmes"
  ]
  node [
    id 21
    label "Fantastyczna_Czw&#243;rka"
  ]
  node [
    id 22
    label "Hamlet"
  ]
  node [
    id 23
    label "Asterix"
  ]
  node [
    id 24
    label "Quasimodo"
  ]
  node [
    id 25
    label "Don_Kiszot"
  ]
  node [
    id 26
    label "Wallenrod"
  ]
  node [
    id 27
    label "uczestnik"
  ]
  node [
    id 28
    label "&#347;mia&#322;ek"
  ]
  node [
    id 29
    label "Harry_Potter"
  ]
  node [
    id 30
    label "podmiot"
  ]
  node [
    id 31
    label "Achilles"
  ]
  node [
    id 32
    label "Werter"
  ]
  node [
    id 33
    label "Mario"
  ]
  node [
    id 34
    label "posta&#263;"
  ]
  node [
    id 35
    label "czyj&#347;"
  ]
  node [
    id 36
    label "m&#261;&#380;"
  ]
  node [
    id 37
    label "rehabilitowanie_si&#281;"
  ]
  node [
    id 38
    label "reputacja"
  ]
  node [
    id 39
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 40
    label "rehabilitowa&#263;_si&#281;"
  ]
  node [
    id 41
    label "patron"
  ]
  node [
    id 42
    label "nazwa_w&#322;asna"
  ]
  node [
    id 43
    label "deklinacja"
  ]
  node [
    id 44
    label "zrehabilitowanie_si&#281;"
  ]
  node [
    id 45
    label "imiennictwo"
  ]
  node [
    id 46
    label "wezwanie"
  ]
  node [
    id 47
    label "zrehabilitowa&#263;_si&#281;"
  ]
  node [
    id 48
    label "personalia"
  ]
  node [
    id 49
    label "term"
  ]
  node [
    id 50
    label "leksem"
  ]
  node [
    id 51
    label "wielko&#347;&#263;"
  ]
  node [
    id 52
    label "zabawa"
  ]
  node [
    id 53
    label "rywalizacja"
  ]
  node [
    id 54
    label "czynno&#347;&#263;"
  ]
  node [
    id 55
    label "Pok&#233;mon"
  ]
  node [
    id 56
    label "synteza"
  ]
  node [
    id 57
    label "odtworzenie"
  ]
  node [
    id 58
    label "komplet"
  ]
  node [
    id 59
    label "rekwizyt_do_gry"
  ]
  node [
    id 60
    label "odg&#322;os"
  ]
  node [
    id 61
    label "rozgrywka"
  ]
  node [
    id 62
    label "post&#281;powanie"
  ]
  node [
    id 63
    label "wydarzenie"
  ]
  node [
    id 64
    label "apparent_motion"
  ]
  node [
    id 65
    label "game"
  ]
  node [
    id 66
    label "zmienno&#347;&#263;"
  ]
  node [
    id 67
    label "zasada"
  ]
  node [
    id 68
    label "akcja"
  ]
  node [
    id 69
    label "play"
  ]
  node [
    id 70
    label "contest"
  ]
  node [
    id 71
    label "zbijany"
  ]
  node [
    id 72
    label "instrument_perkusyjny"
  ]
  node [
    id 73
    label "b&#281;benek"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
]
