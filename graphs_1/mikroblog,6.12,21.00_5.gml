graph [
  maxDegree 7
  minDegree 1
  meanDegree 1.8518518518518519
  density 0.07122507122507123
  graphCliqueNumber 2
  node [
    id 0
    label "ida"
    origin "text"
  ]
  node [
    id 1
    label "opowiada&#263;"
    origin "text"
  ]
  node [
    id 2
    label "dzieciak"
    origin "text"
  ]
  node [
    id 3
    label "mineralach"
    origin "text"
  ]
  node [
    id 4
    label "potem"
    origin "text"
  ]
  node [
    id 5
    label "pensja"
    origin "text"
  ]
  node [
    id 6
    label "wasi"
    origin "text"
  ]
  node [
    id 7
    label "podatek"
    origin "text"
  ]
  node [
    id 8
    label "relate"
  ]
  node [
    id 9
    label "przedstawia&#263;"
  ]
  node [
    id 10
    label "prawi&#263;"
  ]
  node [
    id 11
    label "naiwniak"
  ]
  node [
    id 12
    label "dziecinny"
  ]
  node [
    id 13
    label "dziecko"
  ]
  node [
    id 14
    label "bech"
  ]
  node [
    id 15
    label "salarium"
  ]
  node [
    id 16
    label "wynagrodzenie"
  ]
  node [
    id 17
    label "kategoria_urz&#281;dnicza"
  ]
  node [
    id 18
    label "szko&#322;a"
  ]
  node [
    id 19
    label "salariat"
  ]
  node [
    id 20
    label "bilans_handlowy"
  ]
  node [
    id 21
    label "op&#322;ata"
  ]
  node [
    id 22
    label "danina"
  ]
  node [
    id 23
    label "trybut"
  ]
  node [
    id 24
    label "tr&#243;jk&#261;t_Harbergera"
  ]
  node [
    id 25
    label "weso&#322;y"
  ]
  node [
    id 26
    label "Miko&#322;ajek"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 15
  ]
  edge [
    source 5
    target 16
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 20
  ]
  edge [
    source 7
    target 21
  ]
  edge [
    source 7
    target 22
  ]
  edge [
    source 7
    target 23
  ]
  edge [
    source 7
    target 24
  ]
  edge [
    source 25
    target 26
  ]
]
