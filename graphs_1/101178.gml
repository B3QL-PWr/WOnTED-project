graph [
  maxDegree 13
  minDegree 1
  meanDegree 1.894736842105263
  density 0.10526315789473684
  graphCliqueNumber 3
  node [
    id 0
    label "bania"
    origin "text"
  ]
  node [
    id 1
    label "ali"
    origin "text"
  ]
  node [
    id 2
    label "niedostateczny"
  ]
  node [
    id 3
    label "mak&#243;wka"
  ]
  node [
    id 4
    label "warzywo"
  ]
  node [
    id 5
    label "przedmiot"
  ]
  node [
    id 6
    label "owoc"
  ]
  node [
    id 7
    label "&#322;eb"
  ]
  node [
    id 8
    label "nibyjagoda"
  ]
  node [
    id 9
    label "dynia"
  ]
  node [
    id 10
    label "popijawa"
  ]
  node [
    id 11
    label "czaszka"
  ]
  node [
    id 12
    label "p&#281;katy"
  ]
  node [
    id 13
    label "bu"
  ]
  node [
    id 14
    label "Asz"
  ]
  node [
    id 15
    label "Szarkijja"
  ]
  node [
    id 16
    label "&#1576;&#1606;&#1609;"
  ]
  node [
    id 17
    label "&#1576;&#1608;"
  ]
  node [
    id 18
    label "&#1593;&#1604;&#1610;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 18
  ]
  edge [
    source 17
    target 18
  ]
]
