graph [
  maxDegree 26
  minDegree 1
  meanDegree 1.9555555555555555
  density 0.044444444444444446
  graphCliqueNumber 2
  node [
    id 0
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 1
    label "olx"
    origin "text"
  ]
  node [
    id 2
    label "wyrabia&#263;"
    origin "text"
  ]
  node [
    id 3
    label "nawet"
    origin "text"
  ]
  node [
    id 4
    label "asymilowa&#263;"
  ]
  node [
    id 5
    label "wapniak"
  ]
  node [
    id 6
    label "dwun&#243;g"
  ]
  node [
    id 7
    label "polifag"
  ]
  node [
    id 8
    label "wz&#243;r"
  ]
  node [
    id 9
    label "profanum"
  ]
  node [
    id 10
    label "hominid"
  ]
  node [
    id 11
    label "homo_sapiens"
  ]
  node [
    id 12
    label "nasada"
  ]
  node [
    id 13
    label "podw&#322;adny"
  ]
  node [
    id 14
    label "ludzko&#347;&#263;"
  ]
  node [
    id 15
    label "os&#322;abianie"
  ]
  node [
    id 16
    label "mikrokosmos"
  ]
  node [
    id 17
    label "portrecista"
  ]
  node [
    id 18
    label "duch"
  ]
  node [
    id 19
    label "g&#322;owa"
  ]
  node [
    id 20
    label "oddzia&#322;ywanie"
  ]
  node [
    id 21
    label "asymilowanie"
  ]
  node [
    id 22
    label "osoba"
  ]
  node [
    id 23
    label "os&#322;abia&#263;"
  ]
  node [
    id 24
    label "figura"
  ]
  node [
    id 25
    label "Adam"
  ]
  node [
    id 26
    label "senior"
  ]
  node [
    id 27
    label "antropochoria"
  ]
  node [
    id 28
    label "posta&#263;"
  ]
  node [
    id 29
    label "get"
  ]
  node [
    id 30
    label "produkowa&#263;"
  ]
  node [
    id 31
    label "wykonywa&#263;"
  ]
  node [
    id 32
    label "zdobywa&#263;"
  ]
  node [
    id 33
    label "spe&#322;nia&#263;"
  ]
  node [
    id 34
    label "wytrzymywa&#263;"
  ]
  node [
    id 35
    label "wypracowywa&#263;"
  ]
  node [
    id 36
    label "osi&#261;ga&#263;"
  ]
  node [
    id 37
    label "train"
  ]
  node [
    id 38
    label "robi&#263;"
  ]
  node [
    id 39
    label "zu&#380;ywa&#263;"
  ]
  node [
    id 40
    label "rozwija&#263;"
  ]
  node [
    id 41
    label "wywi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 42
    label "ugniata&#263;"
  ]
  node [
    id 43
    label "przepracowywa&#263;"
  ]
  node [
    id 44
    label "doskonali&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
]
