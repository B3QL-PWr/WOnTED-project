graph [
  maxDegree 57
  minDegree 1
  meanDegree 2.359567901234568
  density 0.0018220601553934888
  graphCliqueNumber 7
  node [
    id 0
    label "letni"
    origin "text"
  ]
  node [
    id 1
    label "rafa&#322;"
    origin "text"
  ]
  node [
    id 2
    label "ozorkowy"
    origin "text"
  ]
  node [
    id 3
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 4
    label "dotkliwie"
    origin "text"
  ]
  node [
    id 5
    label "okaleczy&#263;"
    origin "text"
  ]
  node [
    id 6
    label "przez"
    origin "text"
  ]
  node [
    id 7
    label "pies"
    origin "text"
  ]
  node [
    id 8
    label "owczarek"
    origin "text"
  ]
  node [
    id 9
    label "podhala&#324;ski"
    origin "text"
  ]
  node [
    id 10
    label "rozerwa&#263;"
    origin "text"
  ]
  node [
    id 11
    label "nos"
    origin "text"
  ]
  node [
    id 12
    label "odgry&#378;&#263;"
    origin "text"
  ]
  node [
    id 13
    label "kawa&#322;ek"
    origin "text"
  ]
  node [
    id 14
    label "nozdrza"
    origin "text"
  ]
  node [
    id 15
    label "dawny"
    origin "text"
  ]
  node [
    id 16
    label "wygl&#261;d"
    origin "text"
  ]
  node [
    id 17
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 18
    label "przywr&#243;ci&#263;"
    origin "text"
  ]
  node [
    id 19
    label "ch&#322;opiec"
    origin "text"
  ]
  node [
    id 20
    label "tylko"
    origin "text"
  ]
  node [
    id 21
    label "operacja"
    origin "text"
  ]
  node [
    id 22
    label "plastyczny"
    origin "text"
  ]
  node [
    id 23
    label "zdarzenie"
    origin "text"
  ]
  node [
    id 24
    label "doj&#347;&#263;"
    origin "text"
  ]
  node [
    id 25
    label "przed"
    origin "text"
  ]
  node [
    id 26
    label "jeden"
    origin "text"
  ]
  node [
    id 27
    label "bar"
    origin "text"
  ]
  node [
    id 28
    label "piwny"
    origin "text"
  ]
  node [
    id 29
    label "miejscowo&#347;&#263;"
    origin "text"
  ]
  node [
    id 30
    label "cedrowice"
    origin "text"
  ]
  node [
    id 31
    label "nast&#281;pny"
    origin "text"
  ]
  node [
    id 32
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 33
    label "rano"
    origin "text"
  ]
  node [
    id 34
    label "mia&#322;"
    origin "text"
  ]
  node [
    id 35
    label "wyjecha&#263;"
    origin "text"
  ]
  node [
    id 36
    label "wraz"
    origin "text"
  ]
  node [
    id 37
    label "rodzic"
    origin "text"
  ]
  node [
    id 38
    label "wakacje"
    origin "text"
  ]
  node [
    id 39
    label "nad"
    origin "text"
  ]
  node [
    id 40
    label "jezioro"
    origin "text"
  ]
  node [
    id 41
    label "znajdowa&#263;"
    origin "text"
  ]
  node [
    id 42
    label "si&#281;"
    origin "text"
  ]
  node [
    id 43
    label "s&#261;siedzki"
    origin "text"
  ]
  node [
    id 44
    label "nasi"
    origin "text"
  ]
  node [
    id 45
    label "dom"
    origin "text"
  ]
  node [
    id 46
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 47
    label "ojciec"
    origin "text"
  ]
  node [
    id 48
    label "prowadzi&#263;"
    origin "text"
  ]
  node [
    id 49
    label "moi"
    origin "text"
  ]
  node [
    id 50
    label "wieczor"
    origin "text"
  ]
  node [
    id 51
    label "postanowi&#263;"
    origin "text"
  ]
  node [
    id 52
    label "odwiedzi&#263;"
    origin "text"
  ]
  node [
    id 53
    label "babcia"
    origin "text"
  ]
  node [
    id 54
    label "przy"
    origin "text"
  ]
  node [
    id 55
    label "wej&#347;cie"
    origin "text"
  ]
  node [
    id 56
    label "lokal"
    origin "text"
  ]
  node [
    id 57
    label "spotka&#263;"
    origin "text"
  ]
  node [
    id 58
    label "wuj"
    origin "text"
  ]
  node [
    id 59
    label "smycz"
    origin "text"
  ]
  node [
    id 60
    label "by&#263;"
    origin "text"
  ]
  node [
    id 61
    label "bez"
    origin "text"
  ]
  node [
    id 62
    label "kaganiec"
    origin "text"
  ]
  node [
    id 63
    label "wiedzie&#263;"
    origin "text"
  ]
  node [
    id 64
    label "jaki"
    origin "text"
  ]
  node [
    id 65
    label "pow&#243;d"
    origin "text"
  ]
  node [
    id 66
    label "bestia"
    origin "text"
  ]
  node [
    id 67
    label "rzuci&#263;"
    origin "text"
  ]
  node [
    id 68
    label "syn"
    origin "text"
  ]
  node [
    id 69
    label "pami&#281;ta&#263;"
    origin "text"
  ]
  node [
    id 70
    label "dziecko"
    origin "text"
  ]
  node [
    id 71
    label "wpa&#347;&#263;"
    origin "text"
  ]
  node [
    id 72
    label "przera&#378;liwy"
    origin "text"
  ]
  node [
    id 73
    label "p&#322;acz"
    origin "text"
  ]
  node [
    id 74
    label "la&#263;"
    origin "text"
  ]
  node [
    id 75
    label "krew"
    origin "text"
  ]
  node [
    id 76
    label "szcz&#281;&#347;cie"
    origin "text"
  ]
  node [
    id 77
    label "lekarz"
    origin "text"
  ]
  node [
    id 78
    label "pora"
    origin "text"
  ]
  node [
    id 79
    label "uda&#263;"
    origin "text"
  ]
  node [
    id 80
    label "przeprowadzi&#263;"
    origin "text"
  ]
  node [
    id 81
    label "&#347;wiadek"
    origin "text"
  ]
  node [
    id 82
    label "podawa&#263;"
    origin "text"
  ]
  node [
    id 83
    label "r&#243;&#380;ny"
    origin "text"
  ]
  node [
    id 84
    label "wersja"
    origin "text"
  ]
  node [
    id 85
    label "wed&#322;ug"
    origin "text"
  ]
  node [
    id 86
    label "zaatakowa&#263;"
    origin "text"
  ]
  node [
    id 87
    label "kiedy"
    origin "text"
  ]
  node [
    id 88
    label "w&#322;a&#347;ciciel"
    origin "text"
  ]
  node [
    id 89
    label "czas"
    origin "text"
  ]
  node [
    id 90
    label "opr&#243;&#380;nia&#263;"
    origin "text"
  ]
  node [
    id 91
    label "kufel"
    origin "text"
  ]
  node [
    id 92
    label "piwo"
    origin "text"
  ]
  node [
    id 93
    label "przekaza&#263;"
    origin "text"
  ]
  node [
    id 94
    label "potrzyma&#263;"
    origin "text"
  ]
  node [
    id 95
    label "koral"
    origin "text"
  ]
  node [
    id 96
    label "tak"
    origin "text"
  ]
  node [
    id 97
    label "wabi&#263;"
    origin "text"
  ]
  node [
    id 98
    label "twierdzi&#263;"
    origin "text"
  ]
  node [
    id 99
    label "dra&#380;ni&#263;"
    origin "text"
  ]
  node [
    id 100
    label "czworon&#243;g"
    origin "text"
  ]
  node [
    id 101
    label "dlatego"
    origin "text"
  ]
  node [
    id 102
    label "wywo&#322;a&#263;"
    origin "text"
  ]
  node [
    id 103
    label "agresja"
    origin "text"
  ]
  node [
    id 104
    label "bardzo"
    origin "text"
  ]
  node [
    id 105
    label "spokojny"
    origin "text"
  ]
  node [
    id 106
    label "zawsze"
    origin "text"
  ]
  node [
    id 107
    label "wiernie"
    origin "text"
  ]
  node [
    id 108
    label "chodzi&#263;"
    origin "text"
  ]
  node [
    id 109
    label "noga"
    origin "text"
  ]
  node [
    id 110
    label "s&#322;ucha&#263;"
    origin "text"
  ]
  node [
    id 111
    label "polecenie"
    origin "text"
  ]
  node [
    id 112
    label "zwi&#261;zek"
    origin "text"
  ]
  node [
    id 113
    label "tym"
    origin "text"
  ]
  node [
    id 114
    label "zdarza&#263;"
    origin "text"
  ]
  node [
    id 115
    label "wychodzi&#263;by&#263;"
    origin "text"
  ]
  node [
    id 116
    label "nim"
    origin "text"
  ]
  node [
    id 117
    label "spacer"
    origin "text"
  ]
  node [
    id 118
    label "nigdy"
    origin "text"
  ]
  node [
    id 119
    label "wczesno"
    origin "text"
  ]
  node [
    id 120
    label "zrobi&#263;"
    origin "text"
  ]
  node [
    id 121
    label "nikt"
    origin "text"
  ]
  node [
    id 122
    label "nic"
    origin "text"
  ]
  node [
    id 123
    label "z&#322;e"
    origin "text"
  ]
  node [
    id 124
    label "pogry&#378;&#263;"
    origin "text"
  ]
  node [
    id 125
    label "szok"
    origin "text"
  ]
  node [
    id 126
    label "wakacyjny"
    origin "text"
  ]
  node [
    id 127
    label "szale&#324;stwo"
    origin "text"
  ]
  node [
    id 128
    label "woda"
    origin "text"
  ]
  node [
    id 129
    label "le&#380;"
    origin "text"
  ]
  node [
    id 130
    label "obola&#322;y"
    origin "text"
  ]
  node [
    id 131
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 132
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 133
    label "rana"
    origin "text"
  ]
  node [
    id 134
    label "konieczny"
    origin "text"
  ]
  node [
    id 135
    label "przeszczep"
    origin "text"
  ]
  node [
    id 136
    label "okolica"
    origin "text"
  ]
  node [
    id 137
    label "pachwina"
    origin "text"
  ]
  node [
    id 138
    label "martwi&#263;"
    origin "text"
  ]
  node [
    id 139
    label "czeka&#263;"
    origin "text"
  ]
  node [
    id 140
    label "seria"
    origin "text"
  ]
  node [
    id 141
    label "bolesny"
    origin "text"
  ]
  node [
    id 142
    label "zastrzyk"
    origin "text"
  ]
  node [
    id 143
    label "ten"
    origin "text"
  ]
  node [
    id 144
    label "por"
    origin "text"
  ]
  node [
    id 145
    label "bowiem"
    origin "text"
  ]
  node [
    id 146
    label "ustali&#263;"
    origin "text"
  ]
  node [
    id 147
    label "szczepi&#263;"
    origin "text"
  ]
  node [
    id 148
    label "przeciw"
    origin "text"
  ]
  node [
    id 149
    label "w&#347;cieklizna"
    origin "text"
  ]
  node [
    id 150
    label "wprawdzie"
    origin "text"
  ]
  node [
    id 151
    label "ale"
    origin "text"
  ]
  node [
    id 152
    label "dokument"
    origin "text"
  ]
  node [
    id 153
    label "obecnie"
    origin "text"
  ]
  node [
    id 154
    label "pod"
    origin "text"
  ]
  node [
    id 155
    label "obserwacja"
    origin "text"
  ]
  node [
    id 156
    label "weterynaria"
    origin "text"
  ]
  node [
    id 157
    label "nijaki"
  ]
  node [
    id 158
    label "sezonowy"
  ]
  node [
    id 159
    label "letnio"
  ]
  node [
    id 160
    label "s&#322;oneczny"
  ]
  node [
    id 161
    label "weso&#322;y"
  ]
  node [
    id 162
    label "oboj&#281;tny"
  ]
  node [
    id 163
    label "latowy"
  ]
  node [
    id 164
    label "ciep&#322;y"
  ]
  node [
    id 165
    label "typowy"
  ]
  node [
    id 166
    label "mi&#281;sny"
  ]
  node [
    id 167
    label "proceed"
  ]
  node [
    id 168
    label "catch"
  ]
  node [
    id 169
    label "pozosta&#263;"
  ]
  node [
    id 170
    label "osta&#263;_si&#281;"
  ]
  node [
    id 171
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 172
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 173
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 174
    label "change"
  ]
  node [
    id 175
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 176
    label "przykro"
  ]
  node [
    id 177
    label "dotkliwy"
  ]
  node [
    id 178
    label "skrzywdzi&#263;"
  ]
  node [
    id 179
    label "uszkodzi&#263;"
  ]
  node [
    id 180
    label "zrani&#263;"
  ]
  node [
    id 181
    label "lame"
  ]
  node [
    id 182
    label "hurt"
  ]
  node [
    id 183
    label "cz&#322;owiek"
  ]
  node [
    id 184
    label "wy&#263;"
  ]
  node [
    id 185
    label "ssak_drapie&#380;ny"
  ]
  node [
    id 186
    label "spragniony"
  ]
  node [
    id 187
    label "rakarz"
  ]
  node [
    id 188
    label "psowate"
  ]
  node [
    id 189
    label "istota_&#380;ywa"
  ]
  node [
    id 190
    label "kabanos"
  ]
  node [
    id 191
    label "mi&#322;o&#347;nik"
  ]
  node [
    id 192
    label "&#322;ajdak"
  ]
  node [
    id 193
    label "policjant"
  ]
  node [
    id 194
    label "szczucie"
  ]
  node [
    id 195
    label "s&#322;u&#380;enie"
  ]
  node [
    id 196
    label "sobaka"
  ]
  node [
    id 197
    label "dogoterapia"
  ]
  node [
    id 198
    label "Cerber"
  ]
  node [
    id 199
    label "wyzwisko"
  ]
  node [
    id 200
    label "szczu&#263;"
  ]
  node [
    id 201
    label "wycie"
  ]
  node [
    id 202
    label "szczeka&#263;"
  ]
  node [
    id 203
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 204
    label "trufla"
  ]
  node [
    id 205
    label "samiec"
  ]
  node [
    id 206
    label "piese&#322;"
  ]
  node [
    id 207
    label "zawy&#263;"
  ]
  node [
    id 208
    label "pies_pasterski"
  ]
  node [
    id 209
    label "owczarz"
  ]
  node [
    id 210
    label "pastuszek"
  ]
  node [
    id 211
    label "drobiony"
  ]
  node [
    id 212
    label "krzesany"
  ]
  node [
    id 213
    label "gwara"
  ]
  node [
    id 214
    label "po_podhala&#324;sku"
  ]
  node [
    id 215
    label "polski"
  ]
  node [
    id 216
    label "g&#243;ralski"
  ]
  node [
    id 217
    label "trombita"
  ]
  node [
    id 218
    label "moskol"
  ]
  node [
    id 219
    label "tatrza&#324;ski"
  ]
  node [
    id 220
    label "rozweseli&#263;"
  ]
  node [
    id 221
    label "zniszczy&#263;"
  ]
  node [
    id 222
    label "przerwa&#263;"
  ]
  node [
    id 223
    label "posi&#261;&#347;&#263;"
  ]
  node [
    id 224
    label "distract"
  ]
  node [
    id 225
    label "explode"
  ]
  node [
    id 226
    label "burst"
  ]
  node [
    id 227
    label "podzieli&#263;"
  ]
  node [
    id 228
    label "si&#261;kanie"
  ]
  node [
    id 229
    label "si&#261;ka&#263;"
  ]
  node [
    id 230
    label "cz&#322;onek"
  ]
  node [
    id 231
    label "nozdrze"
  ]
  node [
    id 232
    label "twarz"
  ]
  node [
    id 233
    label "rezonator"
  ]
  node [
    id 234
    label "katar"
  ]
  node [
    id 235
    label "eskimoski"
  ]
  node [
    id 236
    label "otw&#243;r_nosowy"
  ]
  node [
    id 237
    label "ozena"
  ]
  node [
    id 238
    label "jama_nosowa"
  ]
  node [
    id 239
    label "eskimosek"
  ]
  node [
    id 240
    label "si&#261;kn&#261;&#263;"
  ]
  node [
    id 241
    label "arhinia"
  ]
  node [
    id 242
    label "zako&#324;czenie"
  ]
  node [
    id 243
    label "ma&#322;&#380;owina_nosowa"
  ]
  node [
    id 244
    label "przegroda_nosowa"
  ]
  node [
    id 245
    label "si&#261;kni&#281;cie"
  ]
  node [
    id 246
    label "oddzieli&#263;"
  ]
  node [
    id 247
    label "podp&#322;ywa&#263;"
  ]
  node [
    id 248
    label "podp&#322;ywanie"
  ]
  node [
    id 249
    label "plot"
  ]
  node [
    id 250
    label "podp&#322;yn&#261;&#263;"
  ]
  node [
    id 251
    label "piece"
  ]
  node [
    id 252
    label "kawa&#322;"
  ]
  node [
    id 253
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 254
    label "utw&#243;r"
  ]
  node [
    id 255
    label "podp&#322;yni&#281;cie"
  ]
  node [
    id 256
    label "przesz&#322;y"
  ]
  node [
    id 257
    label "dawno"
  ]
  node [
    id 258
    label "dawniej"
  ]
  node [
    id 259
    label "kombatant"
  ]
  node [
    id 260
    label "stary"
  ]
  node [
    id 261
    label "odleg&#322;y"
  ]
  node [
    id 262
    label "anachroniczny"
  ]
  node [
    id 263
    label "przestarza&#322;y"
  ]
  node [
    id 264
    label "od_dawna"
  ]
  node [
    id 265
    label "poprzedni"
  ]
  node [
    id 266
    label "d&#322;ugoletni"
  ]
  node [
    id 267
    label "wcze&#347;niejszy"
  ]
  node [
    id 268
    label "niegdysiejszy"
  ]
  node [
    id 269
    label "nadawanie"
  ]
  node [
    id 270
    label "ubarwienie"
  ]
  node [
    id 271
    label "brzydota"
  ]
  node [
    id 272
    label "widok"
  ]
  node [
    id 273
    label "cecha"
  ]
  node [
    id 274
    label "postarzanie"
  ]
  node [
    id 275
    label "prostota"
  ]
  node [
    id 276
    label "shape"
  ]
  node [
    id 277
    label "postarzenie"
  ]
  node [
    id 278
    label "kszta&#322;t"
  ]
  node [
    id 279
    label "postarzy&#263;"
  ]
  node [
    id 280
    label "postarza&#263;"
  ]
  node [
    id 281
    label "portrecista"
  ]
  node [
    id 282
    label "doprowadzi&#263;"
  ]
  node [
    id 283
    label "pomocnik"
  ]
  node [
    id 284
    label "g&#243;wniarz"
  ]
  node [
    id 285
    label "&#347;l&#261;ski"
  ]
  node [
    id 286
    label "m&#322;odzieniec"
  ]
  node [
    id 287
    label "kajtek"
  ]
  node [
    id 288
    label "kawaler"
  ]
  node [
    id 289
    label "usynawianie"
  ]
  node [
    id 290
    label "okrzos"
  ]
  node [
    id 291
    label "usynowienie"
  ]
  node [
    id 292
    label "sympatia"
  ]
  node [
    id 293
    label "pederasta"
  ]
  node [
    id 294
    label "synek"
  ]
  node [
    id 295
    label "boyfriend"
  ]
  node [
    id 296
    label "infimum"
  ]
  node [
    id 297
    label "laparotomia"
  ]
  node [
    id 298
    label "chirurg"
  ]
  node [
    id 299
    label "manewr_Pringle&#8217;a"
  ]
  node [
    id 300
    label "supremum"
  ]
  node [
    id 301
    label "torakotomia"
  ]
  node [
    id 302
    label "funkcja"
  ]
  node [
    id 303
    label "strategia"
  ]
  node [
    id 304
    label "szew"
  ]
  node [
    id 305
    label "rzut"
  ]
  node [
    id 306
    label "liczenie"
  ]
  node [
    id 307
    label "ob&#322;awa_augustowska"
  ]
  node [
    id 308
    label "proces_my&#347;lowy"
  ]
  node [
    id 309
    label "czyn"
  ]
  node [
    id 310
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 311
    label "matematyka"
  ]
  node [
    id 312
    label "zabieg"
  ]
  node [
    id 313
    label "mathematical_process"
  ]
  node [
    id 314
    label "liczy&#263;"
  ]
  node [
    id 315
    label "jednostka"
  ]
  node [
    id 316
    label "sugestywny"
  ]
  node [
    id 317
    label "tr&#243;jwymiarowy"
  ]
  node [
    id 318
    label "realistyczny"
  ]
  node [
    id 319
    label "obrazowo"
  ]
  node [
    id 320
    label "mi&#281;kki"
  ]
  node [
    id 321
    label "&#380;ywy"
  ]
  node [
    id 322
    label "zdolny"
  ]
  node [
    id 323
    label "plastycznie"
  ]
  node [
    id 324
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 325
    label "czynno&#347;&#263;"
  ]
  node [
    id 326
    label "motyw"
  ]
  node [
    id 327
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 328
    label "fabu&#322;a"
  ]
  node [
    id 329
    label "przebiec"
  ]
  node [
    id 330
    label "przebiegni&#281;cie"
  ]
  node [
    id 331
    label "charakter"
  ]
  node [
    id 332
    label "get"
  ]
  node [
    id 333
    label "zaj&#347;&#263;"
  ]
  node [
    id 334
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 335
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 336
    label "dop&#322;ata"
  ]
  node [
    id 337
    label "supervene"
  ]
  node [
    id 338
    label "heed"
  ]
  node [
    id 339
    label "dodatek"
  ]
  node [
    id 340
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 341
    label "uzyska&#263;"
  ]
  node [
    id 342
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 343
    label "orgazm"
  ]
  node [
    id 344
    label "dozna&#263;"
  ]
  node [
    id 345
    label "sta&#263;_si&#281;"
  ]
  node [
    id 346
    label "bodziec"
  ]
  node [
    id 347
    label "drive"
  ]
  node [
    id 348
    label "informacja"
  ]
  node [
    id 349
    label "spowodowa&#263;"
  ]
  node [
    id 350
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 351
    label "dotrze&#263;"
  ]
  node [
    id 352
    label "postrzega&#263;"
  ]
  node [
    id 353
    label "become"
  ]
  node [
    id 354
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 355
    label "dop&#322;yn&#261;&#263;"
  ]
  node [
    id 356
    label "przesy&#322;ka"
  ]
  node [
    id 357
    label "dolecie&#263;"
  ]
  node [
    id 358
    label "przy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 359
    label "dokoptowa&#263;"
  ]
  node [
    id 360
    label "kieliszek"
  ]
  node [
    id 361
    label "shot"
  ]
  node [
    id 362
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 363
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 364
    label "jaki&#347;"
  ]
  node [
    id 365
    label "jednolicie"
  ]
  node [
    id 366
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 367
    label "w&#243;dka"
  ]
  node [
    id 368
    label "ujednolicenie"
  ]
  node [
    id 369
    label "jednakowy"
  ]
  node [
    id 370
    label "berylowiec"
  ]
  node [
    id 371
    label "kawiarnia"
  ]
  node [
    id 372
    label "lada"
  ]
  node [
    id 373
    label "milibar"
  ]
  node [
    id 374
    label "zak&#322;ad"
  ]
  node [
    id 375
    label "buffet"
  ]
  node [
    id 376
    label "grupa"
  ]
  node [
    id 377
    label "mikrobar"
  ]
  node [
    id 378
    label "gastronomia"
  ]
  node [
    id 379
    label "jednostka_ci&#347;nienia"
  ]
  node [
    id 380
    label "blat"
  ]
  node [
    id 381
    label "specjalny"
  ]
  node [
    id 382
    label "jasnobr&#261;zowy"
  ]
  node [
    id 383
    label "alkoholowy"
  ]
  node [
    id 384
    label "z&#322;oto&#380;&#243;&#322;ty"
  ]
  node [
    id 385
    label "Aurignac"
  ]
  node [
    id 386
    label "osiedle"
  ]
  node [
    id 387
    label "Levallois-Perret"
  ]
  node [
    id 388
    label "Sabaudia"
  ]
  node [
    id 389
    label "Saint-Acheul"
  ]
  node [
    id 390
    label "Opat&#243;wek"
  ]
  node [
    id 391
    label "Boulogne"
  ]
  node [
    id 392
    label "Cecora"
  ]
  node [
    id 393
    label "nast&#281;pnie"
  ]
  node [
    id 394
    label "kolejno"
  ]
  node [
    id 395
    label "nastopny"
  ]
  node [
    id 396
    label "s&#322;o&#324;ce"
  ]
  node [
    id 397
    label "czynienie_si&#281;"
  ]
  node [
    id 398
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 399
    label "long_time"
  ]
  node [
    id 400
    label "przedpo&#322;udnie"
  ]
  node [
    id 401
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 402
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 403
    label "tydzie&#324;"
  ]
  node [
    id 404
    label "godzina"
  ]
  node [
    id 405
    label "t&#322;usty_czwartek"
  ]
  node [
    id 406
    label "wsta&#263;"
  ]
  node [
    id 407
    label "day"
  ]
  node [
    id 408
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 409
    label "przedwiecz&#243;r"
  ]
  node [
    id 410
    label "Sylwester"
  ]
  node [
    id 411
    label "po&#322;udnie"
  ]
  node [
    id 412
    label "wzej&#347;cie"
  ]
  node [
    id 413
    label "podwiecz&#243;r"
  ]
  node [
    id 414
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 415
    label "termin"
  ]
  node [
    id 416
    label "ranek"
  ]
  node [
    id 417
    label "doba"
  ]
  node [
    id 418
    label "wiecz&#243;r"
  ]
  node [
    id 419
    label "walentynki"
  ]
  node [
    id 420
    label "popo&#322;udnie"
  ]
  node [
    id 421
    label "noc"
  ]
  node [
    id 422
    label "wstanie"
  ]
  node [
    id 423
    label "wsch&#243;d"
  ]
  node [
    id 424
    label "aurora"
  ]
  node [
    id 425
    label "zjawisko"
  ]
  node [
    id 426
    label "proszek"
  ]
  node [
    id 427
    label "leave"
  ]
  node [
    id 428
    label "uda&#263;_si&#281;"
  ]
  node [
    id 429
    label "opiekun"
  ]
  node [
    id 430
    label "wapniak"
  ]
  node [
    id 431
    label "rodzic_chrzestny"
  ]
  node [
    id 432
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 433
    label "rodzice"
  ]
  node [
    id 434
    label "urlop"
  ]
  node [
    id 435
    label "czas_wolny"
  ]
  node [
    id 436
    label "rok_akademicki"
  ]
  node [
    id 437
    label "rok_szkolny"
  ]
  node [
    id 438
    label "dystrofizm"
  ]
  node [
    id 439
    label "Gop&#322;o"
  ]
  node [
    id 440
    label "termoklina"
  ]
  node [
    id 441
    label "Wigry"
  ]
  node [
    id 442
    label "epilimnion"
  ]
  node [
    id 443
    label "hypolimnion"
  ]
  node [
    id 444
    label "szuwar"
  ]
  node [
    id 445
    label "&#346;wite&#378;"
  ]
  node [
    id 446
    label "Nikaragua"
  ]
  node [
    id 447
    label "Huron"
  ]
  node [
    id 448
    label "woda_powierzchniowa"
  ]
  node [
    id 449
    label "Jezioro_Ancylusowe"
  ]
  node [
    id 450
    label "zbiornik_wodny"
  ]
  node [
    id 451
    label "Jasie&#324;"
  ]
  node [
    id 452
    label "&#321;adoga"
  ]
  node [
    id 453
    label "Wicko"
  ]
  node [
    id 454
    label "doznawa&#263;"
  ]
  node [
    id 455
    label "znachodzi&#263;"
  ]
  node [
    id 456
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 457
    label "pozyskiwa&#263;"
  ]
  node [
    id 458
    label "odzyskiwa&#263;"
  ]
  node [
    id 459
    label "os&#261;dza&#263;"
  ]
  node [
    id 460
    label "wykrywa&#263;"
  ]
  node [
    id 461
    label "unwrap"
  ]
  node [
    id 462
    label "detect"
  ]
  node [
    id 463
    label "wymy&#347;la&#263;"
  ]
  node [
    id 464
    label "powodowa&#263;"
  ]
  node [
    id 465
    label "somsiedzki"
  ]
  node [
    id 466
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 467
    label "s&#261;siedzko"
  ]
  node [
    id 468
    label "wzajemny"
  ]
  node [
    id 469
    label "garderoba"
  ]
  node [
    id 470
    label "wiecha"
  ]
  node [
    id 471
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 472
    label "budynek"
  ]
  node [
    id 473
    label "fratria"
  ]
  node [
    id 474
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 475
    label "poj&#281;cie"
  ]
  node [
    id 476
    label "rodzina"
  ]
  node [
    id 477
    label "substancja_mieszkaniowa"
  ]
  node [
    id 478
    label "instytucja"
  ]
  node [
    id 479
    label "dom_rodzinny"
  ]
  node [
    id 480
    label "stead"
  ]
  node [
    id 481
    label "siedziba"
  ]
  node [
    id 482
    label "remark"
  ]
  node [
    id 483
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 484
    label "u&#380;ywa&#263;"
  ]
  node [
    id 485
    label "okre&#347;la&#263;"
  ]
  node [
    id 486
    label "j&#281;zyk"
  ]
  node [
    id 487
    label "say"
  ]
  node [
    id 488
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 489
    label "formu&#322;owa&#263;"
  ]
  node [
    id 490
    label "talk"
  ]
  node [
    id 491
    label "powiada&#263;"
  ]
  node [
    id 492
    label "informowa&#263;"
  ]
  node [
    id 493
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 494
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 495
    label "wydobywa&#263;"
  ]
  node [
    id 496
    label "express"
  ]
  node [
    id 497
    label "chew_the_fat"
  ]
  node [
    id 498
    label "dysfonia"
  ]
  node [
    id 499
    label "umie&#263;"
  ]
  node [
    id 500
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 501
    label "tell"
  ]
  node [
    id 502
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 503
    label "wyra&#380;a&#263;"
  ]
  node [
    id 504
    label "gaworzy&#263;"
  ]
  node [
    id 505
    label "rozmawia&#263;"
  ]
  node [
    id 506
    label "dziama&#263;"
  ]
  node [
    id 507
    label "prawi&#263;"
  ]
  node [
    id 508
    label "pomys&#322;odawca"
  ]
  node [
    id 509
    label "kszta&#322;ciciel"
  ]
  node [
    id 510
    label "tworzyciel"
  ]
  node [
    id 511
    label "ojczym"
  ]
  node [
    id 512
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 513
    label "papa"
  ]
  node [
    id 514
    label "&#347;w"
  ]
  node [
    id 515
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 516
    label "zakonnik"
  ]
  node [
    id 517
    label "kuwada"
  ]
  node [
    id 518
    label "przodek"
  ]
  node [
    id 519
    label "wykonawca"
  ]
  node [
    id 520
    label "control"
  ]
  node [
    id 521
    label "eksponowa&#263;"
  ]
  node [
    id 522
    label "kre&#347;li&#263;"
  ]
  node [
    id 523
    label "g&#243;rowa&#263;"
  ]
  node [
    id 524
    label "message"
  ]
  node [
    id 525
    label "partner"
  ]
  node [
    id 526
    label "string"
  ]
  node [
    id 527
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 528
    label "przesuwa&#263;"
  ]
  node [
    id 529
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 530
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 531
    label "kierowa&#263;"
  ]
  node [
    id 532
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 533
    label "robi&#263;"
  ]
  node [
    id 534
    label "manipulate"
  ]
  node [
    id 535
    label "&#380;y&#263;"
  ]
  node [
    id 536
    label "navigate"
  ]
  node [
    id 537
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 538
    label "ukierunkowywa&#263;"
  ]
  node [
    id 539
    label "linia_melodyczna"
  ]
  node [
    id 540
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 541
    label "prowadzenie"
  ]
  node [
    id 542
    label "tworzy&#263;"
  ]
  node [
    id 543
    label "przewy&#380;sza&#263;"
  ]
  node [
    id 544
    label "sterowa&#263;"
  ]
  node [
    id 545
    label "krzywa"
  ]
  node [
    id 546
    label "podj&#261;&#263;"
  ]
  node [
    id 547
    label "determine"
  ]
  node [
    id 548
    label "visualize"
  ]
  node [
    id 549
    label "zawita&#263;"
  ]
  node [
    id 550
    label "babulinka"
  ]
  node [
    id 551
    label "przodkini"
  ]
  node [
    id 552
    label "kobieta"
  ]
  node [
    id 553
    label "baba"
  ]
  node [
    id 554
    label "starszy_cz&#322;owiek"
  ]
  node [
    id 555
    label "dziadkowie"
  ]
  node [
    id 556
    label "wzi&#281;cie"
  ]
  node [
    id 557
    label "doj&#347;cie"
  ]
  node [
    id 558
    label "wnikni&#281;cie"
  ]
  node [
    id 559
    label "spotkanie"
  ]
  node [
    id 560
    label "przekroczenie"
  ]
  node [
    id 561
    label "bramka"
  ]
  node [
    id 562
    label "stanie_si&#281;"
  ]
  node [
    id 563
    label "podw&#243;rze"
  ]
  node [
    id 564
    label "dostanie_si&#281;"
  ]
  node [
    id 565
    label "zaatakowanie"
  ]
  node [
    id 566
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 567
    label "wzniesienie_si&#281;"
  ]
  node [
    id 568
    label "otw&#243;r"
  ]
  node [
    id 569
    label "pojawienie_si&#281;"
  ]
  node [
    id 570
    label "zacz&#281;cie"
  ]
  node [
    id 571
    label "obsi&#261;dni&#281;cie"
  ]
  node [
    id 572
    label "trespass"
  ]
  node [
    id 573
    label "poznanie"
  ]
  node [
    id 574
    label "wnij&#347;cie"
  ]
  node [
    id 575
    label "zdarzenie_si&#281;"
  ]
  node [
    id 576
    label "approach"
  ]
  node [
    id 577
    label "nast&#261;pienie"
  ]
  node [
    id 578
    label "pocz&#261;tek"
  ]
  node [
    id 579
    label "zag&#322;&#281;bienie_si&#281;"
  ]
  node [
    id 580
    label "wpuszczenie"
  ]
  node [
    id 581
    label "stimulation"
  ]
  node [
    id 582
    label "wch&#243;d"
  ]
  node [
    id 583
    label "dost&#281;p"
  ]
  node [
    id 584
    label "vent"
  ]
  node [
    id 585
    label "przenikni&#281;cie"
  ]
  node [
    id 586
    label "w&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 587
    label "przy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 588
    label "urz&#261;dzenie"
  ]
  node [
    id 589
    label "release"
  ]
  node [
    id 590
    label "miejsce"
  ]
  node [
    id 591
    label "befall"
  ]
  node [
    id 592
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 593
    label "pozna&#263;"
  ]
  node [
    id 594
    label "go_steady"
  ]
  node [
    id 595
    label "insert"
  ]
  node [
    id 596
    label "znale&#378;&#263;"
  ]
  node [
    id 597
    label "krewny"
  ]
  node [
    id 598
    label "wujo"
  ]
  node [
    id 599
    label "uwi&#281;&#378;"
  ]
  node [
    id 600
    label "linka"
  ]
  node [
    id 601
    label "presja"
  ]
  node [
    id 602
    label "si&#281;ga&#263;"
  ]
  node [
    id 603
    label "trwa&#263;"
  ]
  node [
    id 604
    label "obecno&#347;&#263;"
  ]
  node [
    id 605
    label "stan"
  ]
  node [
    id 606
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 607
    label "stand"
  ]
  node [
    id 608
    label "mie&#263;_miejsce"
  ]
  node [
    id 609
    label "uczestniczy&#263;"
  ]
  node [
    id 610
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 611
    label "equal"
  ]
  node [
    id 612
    label "ki&#347;&#263;"
  ]
  node [
    id 613
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 614
    label "krzew"
  ]
  node [
    id 615
    label "pi&#380;maczkowate"
  ]
  node [
    id 616
    label "pestkowiec"
  ]
  node [
    id 617
    label "kwiat"
  ]
  node [
    id 618
    label "owoc"
  ]
  node [
    id 619
    label "oliwkowate"
  ]
  node [
    id 620
    label "ro&#347;lina"
  ]
  node [
    id 621
    label "hy&#263;ka"
  ]
  node [
    id 622
    label "lilac"
  ]
  node [
    id 623
    label "delfinidyna"
  ]
  node [
    id 624
    label "ochrona"
  ]
  node [
    id 625
    label "&#347;wiat&#322;o"
  ]
  node [
    id 626
    label "k&#261;sa&#263;"
  ]
  node [
    id 627
    label "plecionka"
  ]
  node [
    id 628
    label "k&#261;sanie"
  ]
  node [
    id 629
    label "gun_muzzle"
  ]
  node [
    id 630
    label "cognizance"
  ]
  node [
    id 631
    label "strona"
  ]
  node [
    id 632
    label "przyczyna"
  ]
  node [
    id 633
    label "matuszka"
  ]
  node [
    id 634
    label "geneza"
  ]
  node [
    id 635
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 636
    label "czynnik"
  ]
  node [
    id 637
    label "poci&#261;ganie"
  ]
  node [
    id 638
    label "rezultat"
  ]
  node [
    id 639
    label "uprz&#261;&#380;"
  ]
  node [
    id 640
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 641
    label "subject"
  ]
  node [
    id 642
    label "zwierz&#281;"
  ]
  node [
    id 643
    label "popapraniec"
  ]
  node [
    id 644
    label "okrutnik"
  ]
  node [
    id 645
    label "zwyrol"
  ]
  node [
    id 646
    label "sztuka"
  ]
  node [
    id 647
    label "degenerat"
  ]
  node [
    id 648
    label "wyzwanie"
  ]
  node [
    id 649
    label "majdn&#261;&#263;"
  ]
  node [
    id 650
    label "opu&#347;ci&#263;"
  ]
  node [
    id 651
    label "cie&#324;"
  ]
  node [
    id 652
    label "konwulsja"
  ]
  node [
    id 653
    label "podejrzenie"
  ]
  node [
    id 654
    label "ruszy&#263;"
  ]
  node [
    id 655
    label "odej&#347;&#263;"
  ]
  node [
    id 656
    label "project"
  ]
  node [
    id 657
    label "da&#263;"
  ]
  node [
    id 658
    label "czar"
  ]
  node [
    id 659
    label "atak"
  ]
  node [
    id 660
    label "przewr&#243;ci&#263;"
  ]
  node [
    id 661
    label "zdecydowa&#263;"
  ]
  node [
    id 662
    label "rush"
  ]
  node [
    id 663
    label "bewilder"
  ]
  node [
    id 664
    label "zmieni&#263;_w&#322;a&#347;ciwo&#347;ci_fizyczne_lub_chemiczne_czego&#347;"
  ]
  node [
    id 665
    label "sygn&#261;&#263;"
  ]
  node [
    id 666
    label "zmieni&#263;"
  ]
  node [
    id 667
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 668
    label "poruszy&#263;"
  ]
  node [
    id 669
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 670
    label "most"
  ]
  node [
    id 671
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 672
    label "frame"
  ]
  node [
    id 673
    label "powiedzie&#263;"
  ]
  node [
    id 674
    label "przeznaczenie"
  ]
  node [
    id 675
    label "pierdoln&#261;&#263;"
  ]
  node [
    id 676
    label "peddle"
  ]
  node [
    id 677
    label "skonstruowa&#263;"
  ]
  node [
    id 678
    label "towar"
  ]
  node [
    id 679
    label "zna&#263;"
  ]
  node [
    id 680
    label "troska&#263;_si&#281;"
  ]
  node [
    id 681
    label "zachowywa&#263;"
  ]
  node [
    id 682
    label "chowa&#263;"
  ]
  node [
    id 683
    label "think"
  ]
  node [
    id 684
    label "pilnowa&#263;"
  ]
  node [
    id 685
    label "recall"
  ]
  node [
    id 686
    label "echo"
  ]
  node [
    id 687
    label "take_care"
  ]
  node [
    id 688
    label "potomstwo"
  ]
  node [
    id 689
    label "organizm"
  ]
  node [
    id 690
    label "sraluch"
  ]
  node [
    id 691
    label "utulanie"
  ]
  node [
    id 692
    label "pediatra"
  ]
  node [
    id 693
    label "dzieciarnia"
  ]
  node [
    id 694
    label "m&#322;odziak"
  ]
  node [
    id 695
    label "dzieciak"
  ]
  node [
    id 696
    label "utula&#263;"
  ]
  node [
    id 697
    label "potomek"
  ]
  node [
    id 698
    label "pedofil"
  ]
  node [
    id 699
    label "entliczek-pentliczek"
  ]
  node [
    id 700
    label "m&#322;odzik"
  ]
  node [
    id 701
    label "cz&#322;owieczek"
  ]
  node [
    id 702
    label "niepe&#322;noletni"
  ]
  node [
    id 703
    label "fledgling"
  ]
  node [
    id 704
    label "utuli&#263;"
  ]
  node [
    id 705
    label "utulenie"
  ]
  node [
    id 706
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 707
    label "strike"
  ]
  node [
    id 708
    label "rzecz"
  ]
  node [
    id 709
    label "d&#378;wi&#281;k"
  ]
  node [
    id 710
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 711
    label "zapach"
  ]
  node [
    id 712
    label "wp&#322;yn&#261;&#263;"
  ]
  node [
    id 713
    label "wkl&#281;sn&#261;&#263;"
  ]
  node [
    id 714
    label "wpada&#263;"
  ]
  node [
    id 715
    label "wymy&#347;li&#263;"
  ]
  node [
    id 716
    label "wpieprzy&#263;_si&#281;"
  ]
  node [
    id 717
    label "fall"
  ]
  node [
    id 718
    label "ulec"
  ]
  node [
    id 719
    label "zakocha&#263;_si&#281;"
  ]
  node [
    id 720
    label "uderzy&#263;"
  ]
  node [
    id 721
    label "fall_upon"
  ]
  node [
    id 722
    label "ponie&#347;&#263;"
  ]
  node [
    id 723
    label "emocja"
  ]
  node [
    id 724
    label "ogrom"
  ]
  node [
    id 725
    label "collapse"
  ]
  node [
    id 726
    label "decline"
  ]
  node [
    id 727
    label "wpierdoli&#263;_si&#281;"
  ]
  node [
    id 728
    label "zaj&#347;&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 729
    label "przejmuj&#261;cy"
  ]
  node [
    id 730
    label "straszny"
  ]
  node [
    id 731
    label "straszliwy"
  ]
  node [
    id 732
    label "przera&#378;liwie"
  ]
  node [
    id 733
    label "reakcja"
  ]
  node [
    id 734
    label "koncert"
  ]
  node [
    id 735
    label "&#380;al"
  ]
  node [
    id 736
    label "marginalizowa&#263;"
  ]
  node [
    id 737
    label "pada&#263;"
  ]
  node [
    id 738
    label "odlewa&#263;"
  ]
  node [
    id 739
    label "getaway"
  ]
  node [
    id 740
    label "przemieszcza&#263;"
  ]
  node [
    id 741
    label "inculcate"
  ]
  node [
    id 742
    label "na&#347;miewa&#263;_si&#281;"
  ]
  node [
    id 743
    label "bi&#263;"
  ]
  node [
    id 744
    label "wype&#322;nia&#263;"
  ]
  node [
    id 745
    label "pour"
  ]
  node [
    id 746
    label "sika&#263;"
  ]
  node [
    id 747
    label "pokrewie&#324;stwo"
  ]
  node [
    id 748
    label "wykrwawi&#263;"
  ]
  node [
    id 749
    label "kr&#261;&#380;enie"
  ]
  node [
    id 750
    label "wykrwawia&#263;"
  ]
  node [
    id 751
    label "wykrwawianie"
  ]
  node [
    id 752
    label "hematokryt"
  ]
  node [
    id 753
    label "farba"
  ]
  node [
    id 754
    label "wykrwawianie_si&#281;"
  ]
  node [
    id 755
    label "dializowa&#263;"
  ]
  node [
    id 756
    label "marker_nowotworowy"
  ]
  node [
    id 757
    label "krwioplucie"
  ]
  node [
    id 758
    label "wykrwawia&#263;_si&#281;"
  ]
  node [
    id 759
    label "tkanka_&#322;&#261;czna"
  ]
  node [
    id 760
    label "dializowanie"
  ]
  node [
    id 761
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 762
    label "wykrwawienie_si&#281;"
  ]
  node [
    id 763
    label "krwinka"
  ]
  node [
    id 764
    label "lifeblood"
  ]
  node [
    id 765
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 766
    label "wykrwawi&#263;_si&#281;"
  ]
  node [
    id 767
    label "osocze_krwi"
  ]
  node [
    id 768
    label "uk&#322;ad_krwiono&#347;ny"
  ]
  node [
    id 769
    label "&#347;mier&#263;"
  ]
  node [
    id 770
    label "wykrwawienie"
  ]
  node [
    id 771
    label "wra&#380;enie"
  ]
  node [
    id 772
    label "dobrodziejstwo"
  ]
  node [
    id 773
    label "dobro"
  ]
  node [
    id 774
    label "przypadek"
  ]
  node [
    id 775
    label "zbada&#263;"
  ]
  node [
    id 776
    label "medyk"
  ]
  node [
    id 777
    label "lekarze"
  ]
  node [
    id 778
    label "pracownik"
  ]
  node [
    id 779
    label "eskulap"
  ]
  node [
    id 780
    label "Hipokrates"
  ]
  node [
    id 781
    label "Mesmer"
  ]
  node [
    id 782
    label "Galen"
  ]
  node [
    id 783
    label "dokt&#243;r"
  ]
  node [
    id 784
    label "okres_czasu"
  ]
  node [
    id 785
    label "run"
  ]
  node [
    id 786
    label "represent"
  ]
  node [
    id 787
    label "pom&#243;c"
  ]
  node [
    id 788
    label "zbudowa&#263;"
  ]
  node [
    id 789
    label "przewie&#347;&#263;"
  ]
  node [
    id 790
    label "wykona&#263;"
  ]
  node [
    id 791
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 792
    label "draw"
  ]
  node [
    id 793
    label "carry"
  ]
  node [
    id 794
    label "s&#261;d"
  ]
  node [
    id 795
    label "osoba_fizyczna"
  ]
  node [
    id 796
    label "uczestnik"
  ]
  node [
    id 797
    label "obserwator"
  ]
  node [
    id 798
    label "dru&#380;ba"
  ]
  node [
    id 799
    label "tenis"
  ]
  node [
    id 800
    label "cover"
  ]
  node [
    id 801
    label "siatk&#243;wka"
  ]
  node [
    id 802
    label "dawa&#263;"
  ]
  node [
    id 803
    label "faszerowa&#263;"
  ]
  node [
    id 804
    label "introduce"
  ]
  node [
    id 805
    label "jedzenie"
  ]
  node [
    id 806
    label "tender"
  ]
  node [
    id 807
    label "deal"
  ]
  node [
    id 808
    label "kelner"
  ]
  node [
    id 809
    label "serwowa&#263;"
  ]
  node [
    id 810
    label "rozgrywa&#263;"
  ]
  node [
    id 811
    label "stawia&#263;"
  ]
  node [
    id 812
    label "r&#243;&#380;nie"
  ]
  node [
    id 813
    label "inny"
  ]
  node [
    id 814
    label "typ"
  ]
  node [
    id 815
    label "posta&#263;"
  ]
  node [
    id 816
    label "sport"
  ]
  node [
    id 817
    label "skrytykowa&#263;"
  ]
  node [
    id 818
    label "spell"
  ]
  node [
    id 819
    label "spr&#243;bowa&#263;"
  ]
  node [
    id 820
    label "attack"
  ]
  node [
    id 821
    label "nast&#261;pi&#263;"
  ]
  node [
    id 822
    label "postara&#263;_si&#281;"
  ]
  node [
    id 823
    label "przeby&#263;"
  ]
  node [
    id 824
    label "anoint"
  ]
  node [
    id 825
    label "rozegra&#263;"
  ]
  node [
    id 826
    label "wykupienie"
  ]
  node [
    id 827
    label "bycie_w_posiadaniu"
  ]
  node [
    id 828
    label "wykupywanie"
  ]
  node [
    id 829
    label "podmiot"
  ]
  node [
    id 830
    label "czasokres"
  ]
  node [
    id 831
    label "trawienie"
  ]
  node [
    id 832
    label "kategoria_gramatyczna"
  ]
  node [
    id 833
    label "period"
  ]
  node [
    id 834
    label "odczyt"
  ]
  node [
    id 835
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 836
    label "chwila"
  ]
  node [
    id 837
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 838
    label "poprzedzenie"
  ]
  node [
    id 839
    label "koniugacja"
  ]
  node [
    id 840
    label "dzieje"
  ]
  node [
    id 841
    label "poprzedzi&#263;"
  ]
  node [
    id 842
    label "przep&#322;ywanie"
  ]
  node [
    id 843
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 844
    label "odwlekanie_si&#281;"
  ]
  node [
    id 845
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 846
    label "Zeitgeist"
  ]
  node [
    id 847
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 848
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 849
    label "pochodzi&#263;"
  ]
  node [
    id 850
    label "schy&#322;ek"
  ]
  node [
    id 851
    label "czwarty_wymiar"
  ]
  node [
    id 852
    label "chronometria"
  ]
  node [
    id 853
    label "poprzedzanie"
  ]
  node [
    id 854
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 855
    label "pogoda"
  ]
  node [
    id 856
    label "zegar"
  ]
  node [
    id 857
    label "trawi&#263;"
  ]
  node [
    id 858
    label "pochodzenie"
  ]
  node [
    id 859
    label "poprzedza&#263;"
  ]
  node [
    id 860
    label "time_period"
  ]
  node [
    id 861
    label "rachuba_czasu"
  ]
  node [
    id 862
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 863
    label "czasoprzestrze&#324;"
  ]
  node [
    id 864
    label "laba"
  ]
  node [
    id 865
    label "zu&#380;ywa&#263;"
  ]
  node [
    id 866
    label "wyjmowa&#263;"
  ]
  node [
    id 867
    label "authorize"
  ]
  node [
    id 868
    label "zawarto&#347;&#263;"
  ]
  node [
    id 869
    label "birofilista"
  ]
  node [
    id 870
    label "birofilistyka"
  ]
  node [
    id 871
    label "ucho"
  ]
  node [
    id 872
    label "naczynie"
  ]
  node [
    id 873
    label "porcja"
  ]
  node [
    id 874
    label "jar"
  ]
  node [
    id 875
    label "browarnia"
  ]
  node [
    id 876
    label "anta&#322;"
  ]
  node [
    id 877
    label "wyj&#347;cie"
  ]
  node [
    id 878
    label "warzy&#263;"
  ]
  node [
    id 879
    label "warzenie"
  ]
  node [
    id 880
    label "uwarzenie"
  ]
  node [
    id 881
    label "alkohol"
  ]
  node [
    id 882
    label "nap&#243;j"
  ]
  node [
    id 883
    label "nawarzy&#263;"
  ]
  node [
    id 884
    label "bacik"
  ]
  node [
    id 885
    label "uwarzy&#263;"
  ]
  node [
    id 886
    label "nawarzenie"
  ]
  node [
    id 887
    label "birofilia"
  ]
  node [
    id 888
    label "impart"
  ]
  node [
    id 889
    label "sygna&#322;"
  ]
  node [
    id 890
    label "propagate"
  ]
  node [
    id 891
    label "transfer"
  ]
  node [
    id 892
    label "give"
  ]
  node [
    id 893
    label "wys&#322;a&#263;"
  ]
  node [
    id 894
    label "poda&#263;"
  ]
  node [
    id 895
    label "wp&#322;aci&#263;"
  ]
  node [
    id 896
    label "utrzyma&#263;_si&#281;"
  ]
  node [
    id 897
    label "zmusi&#263;"
  ]
  node [
    id 898
    label "hold"
  ]
  node [
    id 899
    label "zatrzyma&#263;"
  ]
  node [
    id 900
    label "clasp"
  ]
  node [
    id 901
    label "hodowla"
  ]
  node [
    id 902
    label "utrzyma&#263;"
  ]
  node [
    id 903
    label "antozoopolip"
  ]
  node [
    id 904
    label "tworzywo"
  ]
  node [
    id 905
    label "szkielet"
  ]
  node [
    id 906
    label "korale"
  ]
  node [
    id 907
    label "rafa"
  ]
  node [
    id 908
    label "coral"
  ]
  node [
    id 909
    label "czerwie&#324;"
  ]
  node [
    id 910
    label "koralik"
  ]
  node [
    id 911
    label "parzyde&#322;kowiec"
  ]
  node [
    id 912
    label "organizm_rafotw&#243;rczy"
  ]
  node [
    id 913
    label "koralowce"
  ]
  node [
    id 914
    label "sprowadza&#263;"
  ]
  node [
    id 915
    label "pull"
  ]
  node [
    id 916
    label "mani&#263;"
  ]
  node [
    id 917
    label "zapewnia&#263;"
  ]
  node [
    id 918
    label "oznajmia&#263;"
  ]
  node [
    id 919
    label "komunikowa&#263;"
  ]
  node [
    id 920
    label "attest"
  ]
  node [
    id 921
    label "argue"
  ]
  node [
    id 922
    label "wywo&#322;ywa&#263;"
  ]
  node [
    id 923
    label "tease"
  ]
  node [
    id 924
    label "pobudza&#263;"
  ]
  node [
    id 925
    label "denerwowa&#263;"
  ]
  node [
    id 926
    label "wkurwia&#263;"
  ]
  node [
    id 927
    label "chafe"
  ]
  node [
    id 928
    label "displease"
  ]
  node [
    id 929
    label "kr&#281;gowiec"
  ]
  node [
    id 930
    label "zwierz&#281;_domowe"
  ]
  node [
    id 931
    label "tetrapody"
  ]
  node [
    id 932
    label "critter"
  ]
  node [
    id 933
    label "trip"
  ]
  node [
    id 934
    label "przetworzy&#263;"
  ]
  node [
    id 935
    label "wydali&#263;"
  ]
  node [
    id 936
    label "wezwa&#263;"
  ]
  node [
    id 937
    label "revolutionize"
  ]
  node [
    id 938
    label "arouse"
  ]
  node [
    id 939
    label "train"
  ]
  node [
    id 940
    label "oznajmi&#263;"
  ]
  node [
    id 941
    label "poleci&#263;"
  ]
  node [
    id 942
    label "zachowanie"
  ]
  node [
    id 943
    label "potop_szwedzki"
  ]
  node [
    id 944
    label "rapt"
  ]
  node [
    id 945
    label "okres_godowy"
  ]
  node [
    id 946
    label "aggression"
  ]
  node [
    id 947
    label "napad"
  ]
  node [
    id 948
    label "w_chuj"
  ]
  node [
    id 949
    label "uspokojenie"
  ]
  node [
    id 950
    label "uspokojenie_si&#281;"
  ]
  node [
    id 951
    label "niezak&#322;&#243;cony"
  ]
  node [
    id 952
    label "przyjemny"
  ]
  node [
    id 953
    label "wolny"
  ]
  node [
    id 954
    label "spokojnie"
  ]
  node [
    id 955
    label "cicho"
  ]
  node [
    id 956
    label "nietrudny"
  ]
  node [
    id 957
    label "bezproblemowy"
  ]
  node [
    id 958
    label "uspokajanie_si&#281;"
  ]
  node [
    id 959
    label "uspokajanie"
  ]
  node [
    id 960
    label "zaw&#380;dy"
  ]
  node [
    id 961
    label "ci&#261;gle"
  ]
  node [
    id 962
    label "na_zawsze"
  ]
  node [
    id 963
    label "cz&#281;sto"
  ]
  node [
    id 964
    label "precisely"
  ]
  node [
    id 965
    label "dok&#322;adnie"
  ]
  node [
    id 966
    label "wierny"
  ]
  node [
    id 967
    label "accurately"
  ]
  node [
    id 968
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 969
    label "bangla&#263;"
  ]
  node [
    id 970
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 971
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 972
    label "tryb"
  ]
  node [
    id 973
    label "p&#322;ywa&#263;"
  ]
  node [
    id 974
    label "continue"
  ]
  node [
    id 975
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 976
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 977
    label "przebiega&#263;"
  ]
  node [
    id 978
    label "wk&#322;ada&#263;"
  ]
  node [
    id 979
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 980
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 981
    label "para"
  ]
  node [
    id 982
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 983
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 984
    label "krok"
  ]
  node [
    id 985
    label "str&#243;j"
  ]
  node [
    id 986
    label "bywa&#263;"
  ]
  node [
    id 987
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 988
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 989
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 990
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 991
    label "stara&#263;_si&#281;"
  ]
  node [
    id 992
    label "&#322;amaga"
  ]
  node [
    id 993
    label "kopa&#263;"
  ]
  node [
    id 994
    label "jedenastka"
  ]
  node [
    id 995
    label "mi&#281;sie&#324;_krawiecki"
  ]
  node [
    id 996
    label "d&#322;uga_pi&#322;ka"
  ]
  node [
    id 997
    label "sfaulowanie"
  ]
  node [
    id 998
    label "kopn&#261;&#263;"
  ]
  node [
    id 999
    label "czpas"
  ]
  node [
    id 1000
    label "Wis&#322;a"
  ]
  node [
    id 1001
    label "depta&#263;"
  ]
  node [
    id 1002
    label "ekstraklasa"
  ]
  node [
    id 1003
    label "bramkarz"
  ]
  node [
    id 1004
    label "r&#281;ka"
  ]
  node [
    id 1005
    label "mato&#322;"
  ]
  node [
    id 1006
    label "zamurowywa&#263;"
  ]
  node [
    id 1007
    label "dogranie"
  ]
  node [
    id 1008
    label "kopni&#281;cie"
  ]
  node [
    id 1009
    label "catenaccio"
  ]
  node [
    id 1010
    label "lobowanie"
  ]
  node [
    id 1011
    label "mi&#281;sie&#324;_po&#347;ladkowy_ma&#322;y"
  ]
  node [
    id 1012
    label "tackle"
  ]
  node [
    id 1013
    label "ko&#324;czyna_dolna"
  ]
  node [
    id 1014
    label "interliga"
  ]
  node [
    id 1015
    label "lobowa&#263;"
  ]
  node [
    id 1016
    label "mi&#281;czak"
  ]
  node [
    id 1017
    label "podpora"
  ]
  node [
    id 1018
    label "stopa"
  ]
  node [
    id 1019
    label "pi&#322;ka"
  ]
  node [
    id 1020
    label "nerw_udowo-goleniowy"
  ]
  node [
    id 1021
    label "bezbramkowy"
  ]
  node [
    id 1022
    label "zamurowywanie"
  ]
  node [
    id 1023
    label "przelobowa&#263;"
  ]
  node [
    id 1024
    label "&#380;&#243;&#322;ta_kartka"
  ]
  node [
    id 1025
    label "dogrywanie"
  ]
  node [
    id 1026
    label "zamurowanie"
  ]
  node [
    id 1027
    label "ko&#324;czyna"
  ]
  node [
    id 1028
    label "faulowa&#263;"
  ]
  node [
    id 1029
    label "narz&#261;d_ruchu"
  ]
  node [
    id 1030
    label "mi&#281;sie&#324;_po&#347;ladkowy_&#347;redni"
  ]
  node [
    id 1031
    label "napinacz"
  ]
  node [
    id 1032
    label "dublet"
  ]
  node [
    id 1033
    label "gira"
  ]
  node [
    id 1034
    label "przelobowanie"
  ]
  node [
    id 1035
    label "dogra&#263;"
  ]
  node [
    id 1036
    label "zamurowa&#263;"
  ]
  node [
    id 1037
    label "kopanie"
  ]
  node [
    id 1038
    label "mundial"
  ]
  node [
    id 1039
    label "mi&#281;sie&#324;_po&#347;ladkowy_wielki"
  ]
  node [
    id 1040
    label "&#322;&#261;czyna"
  ]
  node [
    id 1041
    label "dogrywa&#263;"
  ]
  node [
    id 1042
    label "s&#322;abeusz"
  ]
  node [
    id 1043
    label "faulowanie"
  ]
  node [
    id 1044
    label "sfaulowa&#263;"
  ]
  node [
    id 1045
    label "nerw_udowy"
  ]
  node [
    id 1046
    label "czerwona_kartka"
  ]
  node [
    id 1047
    label "podporz&#261;dkowywa&#263;_si&#281;"
  ]
  node [
    id 1048
    label "lubi&#263;"
  ]
  node [
    id 1049
    label "odbiera&#263;"
  ]
  node [
    id 1050
    label "wybiera&#263;"
  ]
  node [
    id 1051
    label "odtwarza&#263;"
  ]
  node [
    id 1052
    label "nat&#281;&#380;a&#263;"
  ]
  node [
    id 1053
    label "zadanie"
  ]
  node [
    id 1054
    label "wypowied&#378;"
  ]
  node [
    id 1055
    label "education"
  ]
  node [
    id 1056
    label "zaordynowanie"
  ]
  node [
    id 1057
    label "rekomendacja"
  ]
  node [
    id 1058
    label "ukaz"
  ]
  node [
    id 1059
    label "przesadzenie"
  ]
  node [
    id 1060
    label "statement"
  ]
  node [
    id 1061
    label "recommendation"
  ]
  node [
    id 1062
    label "powierzenie"
  ]
  node [
    id 1063
    label "pobiegni&#281;cie"
  ]
  node [
    id 1064
    label "consign"
  ]
  node [
    id 1065
    label "pognanie"
  ]
  node [
    id 1066
    label "doradzenie"
  ]
  node [
    id 1067
    label "odwodnienie"
  ]
  node [
    id 1068
    label "konstytucja"
  ]
  node [
    id 1069
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 1070
    label "substancja_chemiczna"
  ]
  node [
    id 1071
    label "bratnia_dusza"
  ]
  node [
    id 1072
    label "zwi&#261;zanie"
  ]
  node [
    id 1073
    label "lokant"
  ]
  node [
    id 1074
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 1075
    label "zwi&#261;za&#263;"
  ]
  node [
    id 1076
    label "organizacja"
  ]
  node [
    id 1077
    label "odwadnia&#263;"
  ]
  node [
    id 1078
    label "marriage"
  ]
  node [
    id 1079
    label "marketing_afiliacyjny"
  ]
  node [
    id 1080
    label "bearing"
  ]
  node [
    id 1081
    label "wi&#261;zanie"
  ]
  node [
    id 1082
    label "odwadnianie"
  ]
  node [
    id 1083
    label "koligacja"
  ]
  node [
    id 1084
    label "odwodni&#263;"
  ]
  node [
    id 1085
    label "azeotrop"
  ]
  node [
    id 1086
    label "powi&#261;zanie"
  ]
  node [
    id 1087
    label "gra_planszowa"
  ]
  node [
    id 1088
    label "prezentacja"
  ]
  node [
    id 1089
    label "natural_process"
  ]
  node [
    id 1090
    label "ruch"
  ]
  node [
    id 1091
    label "kompletnie"
  ]
  node [
    id 1092
    label "wcze&#347;nie"
  ]
  node [
    id 1093
    label "zorganizowa&#263;"
  ]
  node [
    id 1094
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 1095
    label "przerobi&#263;"
  ]
  node [
    id 1096
    label "wystylizowa&#263;"
  ]
  node [
    id 1097
    label "cause"
  ]
  node [
    id 1098
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 1099
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 1100
    label "post&#261;pi&#263;"
  ]
  node [
    id 1101
    label "appoint"
  ]
  node [
    id 1102
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 1103
    label "nabra&#263;"
  ]
  node [
    id 1104
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 1105
    label "make"
  ]
  node [
    id 1106
    label "miernota"
  ]
  node [
    id 1107
    label "ciura"
  ]
  node [
    id 1108
    label "g&#243;wno"
  ]
  node [
    id 1109
    label "love"
  ]
  node [
    id 1110
    label "ilo&#347;&#263;"
  ]
  node [
    id 1111
    label "brak"
  ]
  node [
    id 1112
    label "cholerstwo"
  ]
  node [
    id 1113
    label "negatywno&#347;&#263;"
  ]
  node [
    id 1114
    label "ailment"
  ]
  node [
    id 1115
    label "chew"
  ]
  node [
    id 1116
    label "rozdrobni&#263;"
  ]
  node [
    id 1117
    label "porani&#263;"
  ]
  node [
    id 1118
    label "shock_absorber"
  ]
  node [
    id 1119
    label "wstrz&#261;s"
  ]
  node [
    id 1120
    label "zaskoczenie"
  ]
  node [
    id 1121
    label "prze&#380;ycie"
  ]
  node [
    id 1122
    label "wakacyjnie"
  ]
  node [
    id 1123
    label "folly"
  ]
  node [
    id 1124
    label "pomieszanie_zmys&#322;&#243;w"
  ]
  node [
    id 1125
    label "choroba_psychiczna"
  ]
  node [
    id 1126
    label "gor&#261;cy_okres"
  ]
  node [
    id 1127
    label "temper"
  ]
  node [
    id 1128
    label "stupidity"
  ]
  node [
    id 1129
    label "ob&#322;&#281;d"
  ]
  node [
    id 1130
    label "g&#322;upstwo"
  ]
  node [
    id 1131
    label "mn&#243;stwo"
  ]
  node [
    id 1132
    label "zabawa"
  ]
  node [
    id 1133
    label "zamieszanie"
  ]
  node [
    id 1134
    label "oszo&#322;omstwo"
  ]
  node [
    id 1135
    label "poryw"
  ]
  node [
    id 1136
    label "sytuacja"
  ]
  node [
    id 1137
    label "obiekt_naturalny"
  ]
  node [
    id 1138
    label "bicie"
  ]
  node [
    id 1139
    label "wysi&#281;k"
  ]
  node [
    id 1140
    label "pustka"
  ]
  node [
    id 1141
    label "woda_s&#322;odka"
  ]
  node [
    id 1142
    label "p&#322;ycizna"
  ]
  node [
    id 1143
    label "ciecz"
  ]
  node [
    id 1144
    label "spi&#281;trza&#263;"
  ]
  node [
    id 1145
    label "uj&#281;cie_wody"
  ]
  node [
    id 1146
    label "chlasta&#263;"
  ]
  node [
    id 1147
    label "spi&#281;trzy&#263;"
  ]
  node [
    id 1148
    label "bombast"
  ]
  node [
    id 1149
    label "water"
  ]
  node [
    id 1150
    label "kryptodepresja"
  ]
  node [
    id 1151
    label "wodnik"
  ]
  node [
    id 1152
    label "pojazd"
  ]
  node [
    id 1153
    label "&#347;r&#243;dl&#261;dowy"
  ]
  node [
    id 1154
    label "fala"
  ]
  node [
    id 1155
    label "Waruna"
  ]
  node [
    id 1156
    label "nap&#243;j_bezalkoholowy"
  ]
  node [
    id 1157
    label "zrzut"
  ]
  node [
    id 1158
    label "dotleni&#263;"
  ]
  node [
    id 1159
    label "utylizator"
  ]
  node [
    id 1160
    label "przyroda"
  ]
  node [
    id 1161
    label "uci&#261;g"
  ]
  node [
    id 1162
    label "wybrze&#380;e"
  ]
  node [
    id 1163
    label "nabranie"
  ]
  node [
    id 1164
    label "klarownik"
  ]
  node [
    id 1165
    label "chlastanie"
  ]
  node [
    id 1166
    label "przybrze&#380;e"
  ]
  node [
    id 1167
    label "deklamacja"
  ]
  node [
    id 1168
    label "spi&#281;trzenie"
  ]
  node [
    id 1169
    label "przybieranie"
  ]
  node [
    id 1170
    label "tlenek"
  ]
  node [
    id 1171
    label "spi&#281;trzanie"
  ]
  node [
    id 1172
    label "l&#243;d"
  ]
  node [
    id 1173
    label "chory"
  ]
  node [
    id 1174
    label "cierpi&#261;cy"
  ]
  node [
    id 1175
    label "czyj&#347;"
  ]
  node [
    id 1176
    label "m&#261;&#380;"
  ]
  node [
    id 1177
    label "doros&#322;y"
  ]
  node [
    id 1178
    label "wiele"
  ]
  node [
    id 1179
    label "dorodny"
  ]
  node [
    id 1180
    label "znaczny"
  ]
  node [
    id 1181
    label "du&#380;o"
  ]
  node [
    id 1182
    label "prawdziwy"
  ]
  node [
    id 1183
    label "niema&#322;o"
  ]
  node [
    id 1184
    label "wa&#380;ny"
  ]
  node [
    id 1185
    label "rozwini&#281;ty"
  ]
  node [
    id 1186
    label "uraz"
  ]
  node [
    id 1187
    label "rych&#322;ozrost"
  ]
  node [
    id 1188
    label "zszywa&#263;"
  ]
  node [
    id 1189
    label "j&#261;trzy&#263;"
  ]
  node [
    id 1190
    label "zszycie"
  ]
  node [
    id 1191
    label "zszy&#263;"
  ]
  node [
    id 1192
    label "zaklejenie"
  ]
  node [
    id 1193
    label "wound"
  ]
  node [
    id 1194
    label "zszywanie"
  ]
  node [
    id 1195
    label "zaklejanie"
  ]
  node [
    id 1196
    label "ropienie"
  ]
  node [
    id 1197
    label "ropie&#263;"
  ]
  node [
    id 1198
    label "niezb&#281;dnie"
  ]
  node [
    id 1199
    label "przyj&#261;&#263;_si&#281;"
  ]
  node [
    id 1200
    label "dawca"
  ]
  node [
    id 1201
    label "biorca"
  ]
  node [
    id 1202
    label "organ"
  ]
  node [
    id 1203
    label "tkanka"
  ]
  node [
    id 1204
    label "przyjmowa&#263;_si&#281;"
  ]
  node [
    id 1205
    label "przyjmowanie_si&#281;"
  ]
  node [
    id 1206
    label "graft"
  ]
  node [
    id 1207
    label "obszar"
  ]
  node [
    id 1208
    label "krajobraz"
  ]
  node [
    id 1209
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 1210
    label "po_s&#261;siedzku"
  ]
  node [
    id 1211
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 1212
    label "areola"
  ]
  node [
    id 1213
    label "kaktus"
  ]
  node [
    id 1214
    label "wi&#281;zad&#322;o_pachwinowe"
  ]
  node [
    id 1215
    label "tu&#322;&#243;w"
  ]
  node [
    id 1216
    label "kana&#322;_pachwinowy"
  ]
  node [
    id 1217
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 1218
    label "niepokoi&#263;"
  ]
  node [
    id 1219
    label "sp&#281;dza&#263;"
  ]
  node [
    id 1220
    label "look"
  ]
  node [
    id 1221
    label "decydowa&#263;"
  ]
  node [
    id 1222
    label "oczekiwa&#263;"
  ]
  node [
    id 1223
    label "pauzowa&#263;"
  ]
  node [
    id 1224
    label "anticipate"
  ]
  node [
    id 1225
    label "stage_set"
  ]
  node [
    id 1226
    label "partia"
  ]
  node [
    id 1227
    label "przekr&#243;j_geologiczny"
  ]
  node [
    id 1228
    label "sekwencja"
  ]
  node [
    id 1229
    label "komplet"
  ]
  node [
    id 1230
    label "przebieg"
  ]
  node [
    id 1231
    label "zestawienie"
  ]
  node [
    id 1232
    label "jednostka_systematyczna"
  ]
  node [
    id 1233
    label "line"
  ]
  node [
    id 1234
    label "zbi&#243;r"
  ]
  node [
    id 1235
    label "produkcja"
  ]
  node [
    id 1236
    label "set"
  ]
  node [
    id 1237
    label "bolesno"
  ]
  node [
    id 1238
    label "tkliwy"
  ]
  node [
    id 1239
    label "bole&#347;nie"
  ]
  node [
    id 1240
    label "niezadowolony"
  ]
  node [
    id 1241
    label "przykry"
  ]
  node [
    id 1242
    label "bole&#347;ny"
  ]
  node [
    id 1243
    label "&#380;a&#322;osny"
  ]
  node [
    id 1244
    label "dojmuj&#261;cy"
  ]
  node [
    id 1245
    label "wstrzykni&#281;cie"
  ]
  node [
    id 1246
    label "szpryca"
  ]
  node [
    id 1247
    label "nap&#322;yw"
  ]
  node [
    id 1248
    label "stroke"
  ]
  node [
    id 1249
    label "pomoc"
  ]
  node [
    id 1250
    label "strzykawka"
  ]
  node [
    id 1251
    label "injection"
  ]
  node [
    id 1252
    label "okre&#347;lony"
  ]
  node [
    id 1253
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 1254
    label "w&#322;oszczyzna"
  ]
  node [
    id 1255
    label "warzywo"
  ]
  node [
    id 1256
    label "czosnek"
  ]
  node [
    id 1257
    label "kapelusz"
  ]
  node [
    id 1258
    label "uj&#347;cie"
  ]
  node [
    id 1259
    label "umocni&#263;"
  ]
  node [
    id 1260
    label "bind"
  ]
  node [
    id 1261
    label "put"
  ]
  node [
    id 1262
    label "inoculate"
  ]
  node [
    id 1263
    label "uszlachetnia&#263;"
  ]
  node [
    id 1264
    label "uodpornia&#263;"
  ]
  node [
    id 1265
    label "schorzenie"
  ]
  node [
    id 1266
    label "choroba_wirusowa"
  ]
  node [
    id 1267
    label "wirus_w&#347;cieklizny"
  ]
  node [
    id 1268
    label "choroba_odzwierz&#281;ca"
  ]
  node [
    id 1269
    label "record"
  ]
  node [
    id 1270
    label "wytw&#243;r"
  ]
  node [
    id 1271
    label "&#347;wiadectwo"
  ]
  node [
    id 1272
    label "zapis"
  ]
  node [
    id 1273
    label "fascyku&#322;"
  ]
  node [
    id 1274
    label "raport&#243;wka"
  ]
  node [
    id 1275
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 1276
    label "artyku&#322;"
  ]
  node [
    id 1277
    label "plik"
  ]
  node [
    id 1278
    label "writing"
  ]
  node [
    id 1279
    label "dokumentacja"
  ]
  node [
    id 1280
    label "parafa"
  ]
  node [
    id 1281
    label "sygnatariusz"
  ]
  node [
    id 1282
    label "registratura"
  ]
  node [
    id 1283
    label "ninie"
  ]
  node [
    id 1284
    label "aktualny"
  ]
  node [
    id 1285
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 1286
    label "observation"
  ]
  node [
    id 1287
    label "stwierdzenie"
  ]
  node [
    id 1288
    label "badanie"
  ]
  node [
    id 1289
    label "metoda"
  ]
  node [
    id 1290
    label "zasada"
  ]
  node [
    id 1291
    label "higiena_zwierz&#261;t"
  ]
  node [
    id 1292
    label "sztuka_leczenia"
  ]
  node [
    id 1293
    label "kierunek"
  ]
  node [
    id 1294
    label "serologia"
  ]
  node [
    id 1295
    label "deontologia_weterynaryjna"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 19
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 146
  ]
  edge [
    source 7
    target 183
  ]
  edge [
    source 7
    target 184
  ]
  edge [
    source 7
    target 185
  ]
  edge [
    source 7
    target 186
  ]
  edge [
    source 7
    target 187
  ]
  edge [
    source 7
    target 188
  ]
  edge [
    source 7
    target 189
  ]
  edge [
    source 7
    target 190
  ]
  edge [
    source 7
    target 191
  ]
  edge [
    source 7
    target 192
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 193
  ]
  edge [
    source 7
    target 194
  ]
  edge [
    source 7
    target 195
  ]
  edge [
    source 7
    target 196
  ]
  edge [
    source 7
    target 197
  ]
  edge [
    source 7
    target 198
  ]
  edge [
    source 7
    target 199
  ]
  edge [
    source 7
    target 200
  ]
  edge [
    source 7
    target 201
  ]
  edge [
    source 7
    target 202
  ]
  edge [
    source 7
    target 203
  ]
  edge [
    source 7
    target 204
  ]
  edge [
    source 7
    target 205
  ]
  edge [
    source 7
    target 206
  ]
  edge [
    source 7
    target 207
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 208
  ]
  edge [
    source 8
    target 209
  ]
  edge [
    source 8
    target 210
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 37
  ]
  edge [
    source 8
    target 141
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 9
    target 211
  ]
  edge [
    source 9
    target 212
  ]
  edge [
    source 9
    target 213
  ]
  edge [
    source 9
    target 214
  ]
  edge [
    source 9
    target 215
  ]
  edge [
    source 9
    target 216
  ]
  edge [
    source 9
    target 217
  ]
  edge [
    source 9
    target 218
  ]
  edge [
    source 9
    target 219
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 141
  ]
  edge [
    source 9
    target 41
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 135
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 220
  ]
  edge [
    source 10
    target 221
  ]
  edge [
    source 10
    target 222
  ]
  edge [
    source 10
    target 223
  ]
  edge [
    source 10
    target 224
  ]
  edge [
    source 10
    target 225
  ]
  edge [
    source 10
    target 226
  ]
  edge [
    source 10
    target 227
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 73
  ]
  edge [
    source 11
    target 74
  ]
  edge [
    source 11
    target 133
  ]
  edge [
    source 11
    target 228
  ]
  edge [
    source 11
    target 229
  ]
  edge [
    source 11
    target 230
  ]
  edge [
    source 11
    target 231
  ]
  edge [
    source 11
    target 232
  ]
  edge [
    source 11
    target 233
  ]
  edge [
    source 11
    target 234
  ]
  edge [
    source 11
    target 235
  ]
  edge [
    source 11
    target 236
  ]
  edge [
    source 11
    target 237
  ]
  edge [
    source 11
    target 238
  ]
  edge [
    source 11
    target 239
  ]
  edge [
    source 11
    target 240
  ]
  edge [
    source 11
    target 241
  ]
  edge [
    source 11
    target 242
  ]
  edge [
    source 11
    target 243
  ]
  edge [
    source 11
    target 244
  ]
  edge [
    source 11
    target 245
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 246
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 247
  ]
  edge [
    source 13
    target 248
  ]
  edge [
    source 13
    target 249
  ]
  edge [
    source 13
    target 250
  ]
  edge [
    source 13
    target 251
  ]
  edge [
    source 13
    target 252
  ]
  edge [
    source 13
    target 253
  ]
  edge [
    source 13
    target 254
  ]
  edge [
    source 13
    target 255
  ]
  edge [
    source 13
    target 88
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 77
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 256
  ]
  edge [
    source 15
    target 257
  ]
  edge [
    source 15
    target 258
  ]
  edge [
    source 15
    target 259
  ]
  edge [
    source 15
    target 260
  ]
  edge [
    source 15
    target 261
  ]
  edge [
    source 15
    target 262
  ]
  edge [
    source 15
    target 263
  ]
  edge [
    source 15
    target 264
  ]
  edge [
    source 15
    target 265
  ]
  edge [
    source 15
    target 266
  ]
  edge [
    source 15
    target 267
  ]
  edge [
    source 15
    target 268
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 269
  ]
  edge [
    source 16
    target 270
  ]
  edge [
    source 16
    target 271
  ]
  edge [
    source 16
    target 272
  ]
  edge [
    source 16
    target 273
  ]
  edge [
    source 16
    target 274
  ]
  edge [
    source 16
    target 275
  ]
  edge [
    source 16
    target 276
  ]
  edge [
    source 16
    target 277
  ]
  edge [
    source 16
    target 278
  ]
  edge [
    source 16
    target 279
  ]
  edge [
    source 16
    target 280
  ]
  edge [
    source 16
    target 281
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 19
  ]
  edge [
    source 17
    target 139
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 282
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 53
  ]
  edge [
    source 19
    target 93
  ]
  edge [
    source 19
    target 59
  ]
  edge [
    source 19
    target 124
  ]
  edge [
    source 19
    target 60
  ]
  edge [
    source 19
    target 42
  ]
  edge [
    source 19
    target 183
  ]
  edge [
    source 19
    target 283
  ]
  edge [
    source 19
    target 284
  ]
  edge [
    source 19
    target 285
  ]
  edge [
    source 19
    target 286
  ]
  edge [
    source 19
    target 287
  ]
  edge [
    source 19
    target 288
  ]
  edge [
    source 19
    target 289
  ]
  edge [
    source 19
    target 70
  ]
  edge [
    source 19
    target 290
  ]
  edge [
    source 19
    target 291
  ]
  edge [
    source 19
    target 292
  ]
  edge [
    source 19
    target 293
  ]
  edge [
    source 19
    target 294
  ]
  edge [
    source 19
    target 295
  ]
  edge [
    source 19
    target 22
  ]
  edge [
    source 19
    target 56
  ]
  edge [
    source 19
    target 46
  ]
  edge [
    source 19
    target 110
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 69
  ]
  edge [
    source 20
    target 70
  ]
  edge [
    source 20
    target 108
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 80
  ]
  edge [
    source 21
    target 81
  ]
  edge [
    source 21
    target 296
  ]
  edge [
    source 21
    target 297
  ]
  edge [
    source 21
    target 298
  ]
  edge [
    source 21
    target 299
  ]
  edge [
    source 21
    target 300
  ]
  edge [
    source 21
    target 301
  ]
  edge [
    source 21
    target 302
  ]
  edge [
    source 21
    target 303
  ]
  edge [
    source 21
    target 304
  ]
  edge [
    source 21
    target 305
  ]
  edge [
    source 21
    target 306
  ]
  edge [
    source 21
    target 307
  ]
  edge [
    source 21
    target 308
  ]
  edge [
    source 21
    target 309
  ]
  edge [
    source 21
    target 310
  ]
  edge [
    source 21
    target 311
  ]
  edge [
    source 21
    target 312
  ]
  edge [
    source 21
    target 313
  ]
  edge [
    source 21
    target 314
  ]
  edge [
    source 21
    target 315
  ]
  edge [
    source 21
    target 135
  ]
  edge [
    source 21
    target 126
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 316
  ]
  edge [
    source 22
    target 317
  ]
  edge [
    source 22
    target 318
  ]
  edge [
    source 22
    target 319
  ]
  edge [
    source 22
    target 320
  ]
  edge [
    source 22
    target 321
  ]
  edge [
    source 22
    target 322
  ]
  edge [
    source 22
    target 323
  ]
  edge [
    source 22
    target 110
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 84
  ]
  edge [
    source 23
    target 85
  ]
  edge [
    source 23
    target 324
  ]
  edge [
    source 23
    target 325
  ]
  edge [
    source 23
    target 326
  ]
  edge [
    source 23
    target 327
  ]
  edge [
    source 23
    target 328
  ]
  edge [
    source 23
    target 329
  ]
  edge [
    source 23
    target 330
  ]
  edge [
    source 23
    target 331
  ]
  edge [
    source 23
    target 90
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 332
  ]
  edge [
    source 24
    target 333
  ]
  edge [
    source 24
    target 334
  ]
  edge [
    source 24
    target 335
  ]
  edge [
    source 24
    target 336
  ]
  edge [
    source 24
    target 337
  ]
  edge [
    source 24
    target 338
  ]
  edge [
    source 24
    target 339
  ]
  edge [
    source 24
    target 168
  ]
  edge [
    source 24
    target 340
  ]
  edge [
    source 24
    target 341
  ]
  edge [
    source 24
    target 342
  ]
  edge [
    source 24
    target 343
  ]
  edge [
    source 24
    target 344
  ]
  edge [
    source 24
    target 345
  ]
  edge [
    source 24
    target 346
  ]
  edge [
    source 24
    target 347
  ]
  edge [
    source 24
    target 348
  ]
  edge [
    source 24
    target 173
  ]
  edge [
    source 24
    target 349
  ]
  edge [
    source 24
    target 350
  ]
  edge [
    source 24
    target 351
  ]
  edge [
    source 24
    target 352
  ]
  edge [
    source 24
    target 353
  ]
  edge [
    source 24
    target 354
  ]
  edge [
    source 24
    target 355
  ]
  edge [
    source 24
    target 356
  ]
  edge [
    source 24
    target 357
  ]
  edge [
    source 24
    target 358
  ]
  edge [
    source 24
    target 359
  ]
  edge [
    source 24
    target 103
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 360
  ]
  edge [
    source 26
    target 361
  ]
  edge [
    source 26
    target 362
  ]
  edge [
    source 26
    target 363
  ]
  edge [
    source 26
    target 364
  ]
  edge [
    source 26
    target 365
  ]
  edge [
    source 26
    target 366
  ]
  edge [
    source 26
    target 367
  ]
  edge [
    source 26
    target 143
  ]
  edge [
    source 26
    target 368
  ]
  edge [
    source 26
    target 369
  ]
  edge [
    source 26
    target 106
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 40
  ]
  edge [
    source 27
    target 41
  ]
  edge [
    source 27
    target 370
  ]
  edge [
    source 27
    target 371
  ]
  edge [
    source 27
    target 372
  ]
  edge [
    source 27
    target 373
  ]
  edge [
    source 27
    target 374
  ]
  edge [
    source 27
    target 375
  ]
  edge [
    source 27
    target 376
  ]
  edge [
    source 27
    target 377
  ]
  edge [
    source 27
    target 378
  ]
  edge [
    source 27
    target 379
  ]
  edge [
    source 27
    target 380
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 381
  ]
  edge [
    source 28
    target 382
  ]
  edge [
    source 28
    target 383
  ]
  edge [
    source 28
    target 384
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 385
  ]
  edge [
    source 29
    target 386
  ]
  edge [
    source 29
    target 387
  ]
  edge [
    source 29
    target 388
  ]
  edge [
    source 29
    target 389
  ]
  edge [
    source 29
    target 390
  ]
  edge [
    source 29
    target 391
  ]
  edge [
    source 29
    target 392
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 393
  ]
  edge [
    source 31
    target 394
  ]
  edge [
    source 31
    target 395
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 396
  ]
  edge [
    source 32
    target 397
  ]
  edge [
    source 32
    target 398
  ]
  edge [
    source 32
    target 89
  ]
  edge [
    source 32
    target 399
  ]
  edge [
    source 32
    target 400
  ]
  edge [
    source 32
    target 401
  ]
  edge [
    source 32
    target 402
  ]
  edge [
    source 32
    target 403
  ]
  edge [
    source 32
    target 404
  ]
  edge [
    source 32
    target 405
  ]
  edge [
    source 32
    target 406
  ]
  edge [
    source 32
    target 407
  ]
  edge [
    source 32
    target 408
  ]
  edge [
    source 32
    target 409
  ]
  edge [
    source 32
    target 410
  ]
  edge [
    source 32
    target 411
  ]
  edge [
    source 32
    target 412
  ]
  edge [
    source 32
    target 413
  ]
  edge [
    source 32
    target 414
  ]
  edge [
    source 32
    target 415
  ]
  edge [
    source 32
    target 416
  ]
  edge [
    source 32
    target 417
  ]
  edge [
    source 32
    target 418
  ]
  edge [
    source 32
    target 419
  ]
  edge [
    source 32
    target 420
  ]
  edge [
    source 32
    target 421
  ]
  edge [
    source 32
    target 422
  ]
  edge [
    source 33
    target 423
  ]
  edge [
    source 33
    target 424
  ]
  edge [
    source 33
    target 78
  ]
  edge [
    source 33
    target 425
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 426
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 427
  ]
  edge [
    source 35
    target 428
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 49
  ]
  edge [
    source 37
    target 50
  ]
  edge [
    source 37
    target 137
  ]
  edge [
    source 37
    target 429
  ]
  edge [
    source 37
    target 430
  ]
  edge [
    source 37
    target 431
  ]
  edge [
    source 37
    target 432
  ]
  edge [
    source 37
    target 433
  ]
  edge [
    source 37
    target 47
  ]
  edge [
    source 37
    target 81
  ]
  edge [
    source 37
    target 111
  ]
  edge [
    source 37
    target 141
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 434
  ]
  edge [
    source 38
    target 435
  ]
  edge [
    source 38
    target 436
  ]
  edge [
    source 38
    target 437
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 127
  ]
  edge [
    source 39
    target 128
  ]
  edge [
    source 40
    target 438
  ]
  edge [
    source 40
    target 439
  ]
  edge [
    source 40
    target 440
  ]
  edge [
    source 40
    target 441
  ]
  edge [
    source 40
    target 442
  ]
  edge [
    source 40
    target 443
  ]
  edge [
    source 40
    target 444
  ]
  edge [
    source 40
    target 445
  ]
  edge [
    source 40
    target 446
  ]
  edge [
    source 40
    target 447
  ]
  edge [
    source 40
    target 448
  ]
  edge [
    source 40
    target 449
  ]
  edge [
    source 40
    target 450
  ]
  edge [
    source 40
    target 451
  ]
  edge [
    source 40
    target 452
  ]
  edge [
    source 40
    target 453
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 454
  ]
  edge [
    source 41
    target 455
  ]
  edge [
    source 41
    target 456
  ]
  edge [
    source 41
    target 457
  ]
  edge [
    source 41
    target 458
  ]
  edge [
    source 41
    target 459
  ]
  edge [
    source 41
    target 460
  ]
  edge [
    source 41
    target 461
  ]
  edge [
    source 41
    target 462
  ]
  edge [
    source 41
    target 463
  ]
  edge [
    source 41
    target 464
  ]
  edge [
    source 41
    target 66
  ]
  edge [
    source 41
    target 114
  ]
  edge [
    source 41
    target 135
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 67
  ]
  edge [
    source 42
    target 68
  ]
  edge [
    source 42
    target 74
  ]
  edge [
    source 42
    target 75
  ]
  edge [
    source 42
    target 79
  ]
  edge [
    source 42
    target 80
  ]
  edge [
    source 42
    target 97
  ]
  edge [
    source 42
    target 66
  ]
  edge [
    source 42
    target 99
  ]
  edge [
    source 42
    target 100
  ]
  edge [
    source 42
    target 114
  ]
  edge [
    source 42
    target 115
  ]
  edge [
    source 42
    target 138
  ]
  edge [
    source 42
    target 145
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 465
  ]
  edge [
    source 43
    target 466
  ]
  edge [
    source 43
    target 467
  ]
  edge [
    source 43
    target 468
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 71
  ]
  edge [
    source 45
    target 72
  ]
  edge [
    source 45
    target 130
  ]
  edge [
    source 45
    target 131
  ]
  edge [
    source 45
    target 469
  ]
  edge [
    source 45
    target 470
  ]
  edge [
    source 45
    target 471
  ]
  edge [
    source 45
    target 376
  ]
  edge [
    source 45
    target 472
  ]
  edge [
    source 45
    target 473
  ]
  edge [
    source 45
    target 474
  ]
  edge [
    source 45
    target 475
  ]
  edge [
    source 45
    target 476
  ]
  edge [
    source 45
    target 477
  ]
  edge [
    source 45
    target 478
  ]
  edge [
    source 45
    target 479
  ]
  edge [
    source 45
    target 480
  ]
  edge [
    source 45
    target 481
  ]
  edge [
    source 45
    target 55
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 88
  ]
  edge [
    source 46
    target 77
  ]
  edge [
    source 46
    target 134
  ]
  edge [
    source 46
    target 482
  ]
  edge [
    source 46
    target 483
  ]
  edge [
    source 46
    target 484
  ]
  edge [
    source 46
    target 485
  ]
  edge [
    source 46
    target 486
  ]
  edge [
    source 46
    target 487
  ]
  edge [
    source 46
    target 488
  ]
  edge [
    source 46
    target 489
  ]
  edge [
    source 46
    target 490
  ]
  edge [
    source 46
    target 491
  ]
  edge [
    source 46
    target 492
  ]
  edge [
    source 46
    target 493
  ]
  edge [
    source 46
    target 494
  ]
  edge [
    source 46
    target 495
  ]
  edge [
    source 46
    target 496
  ]
  edge [
    source 46
    target 497
  ]
  edge [
    source 46
    target 498
  ]
  edge [
    source 46
    target 499
  ]
  edge [
    source 46
    target 500
  ]
  edge [
    source 46
    target 501
  ]
  edge [
    source 46
    target 502
  ]
  edge [
    source 46
    target 503
  ]
  edge [
    source 46
    target 504
  ]
  edge [
    source 46
    target 505
  ]
  edge [
    source 46
    target 506
  ]
  edge [
    source 46
    target 507
  ]
  edge [
    source 47
    target 508
  ]
  edge [
    source 47
    target 509
  ]
  edge [
    source 47
    target 510
  ]
  edge [
    source 47
    target 511
  ]
  edge [
    source 47
    target 512
  ]
  edge [
    source 47
    target 260
  ]
  edge [
    source 47
    target 205
  ]
  edge [
    source 47
    target 513
  ]
  edge [
    source 47
    target 514
  ]
  edge [
    source 47
    target 515
  ]
  edge [
    source 47
    target 516
  ]
  edge [
    source 47
    target 517
  ]
  edge [
    source 47
    target 518
  ]
  edge [
    source 47
    target 519
  ]
  edge [
    source 47
    target 433
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 520
  ]
  edge [
    source 48
    target 521
  ]
  edge [
    source 48
    target 522
  ]
  edge [
    source 48
    target 523
  ]
  edge [
    source 48
    target 524
  ]
  edge [
    source 48
    target 525
  ]
  edge [
    source 48
    target 526
  ]
  edge [
    source 48
    target 527
  ]
  edge [
    source 48
    target 528
  ]
  edge [
    source 48
    target 529
  ]
  edge [
    source 48
    target 530
  ]
  edge [
    source 48
    target 464
  ]
  edge [
    source 48
    target 531
  ]
  edge [
    source 48
    target 532
  ]
  edge [
    source 48
    target 533
  ]
  edge [
    source 48
    target 534
  ]
  edge [
    source 48
    target 535
  ]
  edge [
    source 48
    target 536
  ]
  edge [
    source 48
    target 537
  ]
  edge [
    source 48
    target 538
  ]
  edge [
    source 48
    target 539
  ]
  edge [
    source 48
    target 540
  ]
  edge [
    source 48
    target 541
  ]
  edge [
    source 48
    target 542
  ]
  edge [
    source 48
    target 543
  ]
  edge [
    source 48
    target 544
  ]
  edge [
    source 48
    target 545
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 345
  ]
  edge [
    source 51
    target 120
  ]
  edge [
    source 51
    target 546
  ]
  edge [
    source 51
    target 547
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 548
  ]
  edge [
    source 52
    target 549
  ]
  edge [
    source 52
    target 71
  ]
  edge [
    source 52
    target 97
  ]
  edge [
    source 52
    target 101
  ]
  edge [
    source 52
    target 77
  ]
  edge [
    source 52
    target 137
  ]
  edge [
    source 52
    target 155
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 85
  ]
  edge [
    source 53
    target 550
  ]
  edge [
    source 53
    target 551
  ]
  edge [
    source 53
    target 552
  ]
  edge [
    source 53
    target 553
  ]
  edge [
    source 53
    target 554
  ]
  edge [
    source 53
    target 555
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 108
  ]
  edge [
    source 54
    target 109
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 556
  ]
  edge [
    source 55
    target 557
  ]
  edge [
    source 55
    target 558
  ]
  edge [
    source 55
    target 559
  ]
  edge [
    source 55
    target 560
  ]
  edge [
    source 55
    target 561
  ]
  edge [
    source 55
    target 562
  ]
  edge [
    source 55
    target 563
  ]
  edge [
    source 55
    target 564
  ]
  edge [
    source 55
    target 565
  ]
  edge [
    source 55
    target 566
  ]
  edge [
    source 55
    target 567
  ]
  edge [
    source 55
    target 568
  ]
  edge [
    source 55
    target 569
  ]
  edge [
    source 55
    target 570
  ]
  edge [
    source 55
    target 571
  ]
  edge [
    source 55
    target 572
  ]
  edge [
    source 55
    target 573
  ]
  edge [
    source 55
    target 574
  ]
  edge [
    source 55
    target 575
  ]
  edge [
    source 55
    target 576
  ]
  edge [
    source 55
    target 577
  ]
  edge [
    source 55
    target 578
  ]
  edge [
    source 55
    target 579
  ]
  edge [
    source 55
    target 580
  ]
  edge [
    source 55
    target 581
  ]
  edge [
    source 55
    target 582
  ]
  edge [
    source 55
    target 583
  ]
  edge [
    source 55
    target 230
  ]
  edge [
    source 55
    target 584
  ]
  edge [
    source 55
    target 585
  ]
  edge [
    source 55
    target 586
  ]
  edge [
    source 55
    target 587
  ]
  edge [
    source 55
    target 588
  ]
  edge [
    source 55
    target 589
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 378
  ]
  edge [
    source 56
    target 590
  ]
  edge [
    source 56
    target 374
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 591
  ]
  edge [
    source 57
    target 592
  ]
  edge [
    source 57
    target 593
  ]
  edge [
    source 57
    target 349
  ]
  edge [
    source 57
    target 594
  ]
  edge [
    source 57
    target 595
  ]
  edge [
    source 57
    target 596
  ]
  edge [
    source 57
    target 173
  ]
  edge [
    source 57
    target 548
  ]
  edge [
    source 57
    target 71
  ]
  edge [
    source 57
    target 142
  ]
  edge [
    source 58
    target 597
  ]
  edge [
    source 58
    target 598
  ]
  edge [
    source 59
    target 94
  ]
  edge [
    source 59
    target 599
  ]
  edge [
    source 59
    target 600
  ]
  edge [
    source 59
    target 601
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 125
  ]
  edge [
    source 60
    target 134
  ]
  edge [
    source 60
    target 135
  ]
  edge [
    source 60
    target 147
  ]
  edge [
    source 60
    target 95
  ]
  edge [
    source 60
    target 154
  ]
  edge [
    source 60
    target 602
  ]
  edge [
    source 60
    target 603
  ]
  edge [
    source 60
    target 604
  ]
  edge [
    source 60
    target 605
  ]
  edge [
    source 60
    target 606
  ]
  edge [
    source 60
    target 607
  ]
  edge [
    source 60
    target 608
  ]
  edge [
    source 60
    target 609
  ]
  edge [
    source 60
    target 108
  ]
  edge [
    source 60
    target 610
  ]
  edge [
    source 60
    target 611
  ]
  edge [
    source 60
    target 139
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 117
  ]
  edge [
    source 61
    target 612
  ]
  edge [
    source 61
    target 613
  ]
  edge [
    source 61
    target 614
  ]
  edge [
    source 61
    target 615
  ]
  edge [
    source 61
    target 616
  ]
  edge [
    source 61
    target 617
  ]
  edge [
    source 61
    target 618
  ]
  edge [
    source 61
    target 619
  ]
  edge [
    source 61
    target 620
  ]
  edge [
    source 61
    target 621
  ]
  edge [
    source 61
    target 622
  ]
  edge [
    source 61
    target 623
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 118
  ]
  edge [
    source 62
    target 624
  ]
  edge [
    source 62
    target 625
  ]
  edge [
    source 62
    target 626
  ]
  edge [
    source 62
    target 627
  ]
  edge [
    source 62
    target 628
  ]
  edge [
    source 62
    target 629
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 630
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 631
  ]
  edge [
    source 65
    target 632
  ]
  edge [
    source 65
    target 633
  ]
  edge [
    source 65
    target 634
  ]
  edge [
    source 65
    target 635
  ]
  edge [
    source 65
    target 636
  ]
  edge [
    source 65
    target 637
  ]
  edge [
    source 65
    target 638
  ]
  edge [
    source 65
    target 639
  ]
  edge [
    source 65
    target 640
  ]
  edge [
    source 65
    target 641
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 66
    target 98
  ]
  edge [
    source 66
    target 642
  ]
  edge [
    source 66
    target 643
  ]
  edge [
    source 66
    target 644
  ]
  edge [
    source 66
    target 645
  ]
  edge [
    source 66
    target 646
  ]
  edge [
    source 66
    target 647
  ]
  edge [
    source 66
    target 114
  ]
  edge [
    source 66
    target 135
  ]
  edge [
    source 67
    target 648
  ]
  edge [
    source 67
    target 649
  ]
  edge [
    source 67
    target 650
  ]
  edge [
    source 67
    target 651
  ]
  edge [
    source 67
    target 652
  ]
  edge [
    source 67
    target 653
  ]
  edge [
    source 67
    target 102
  ]
  edge [
    source 67
    target 654
  ]
  edge [
    source 67
    target 655
  ]
  edge [
    source 67
    target 656
  ]
  edge [
    source 67
    target 657
  ]
  edge [
    source 67
    target 658
  ]
  edge [
    source 67
    target 659
  ]
  edge [
    source 67
    target 660
  ]
  edge [
    source 67
    target 661
  ]
  edge [
    source 67
    target 662
  ]
  edge [
    source 67
    target 663
  ]
  edge [
    source 67
    target 664
  ]
  edge [
    source 67
    target 665
  ]
  edge [
    source 67
    target 666
  ]
  edge [
    source 67
    target 667
  ]
  edge [
    source 67
    target 668
  ]
  edge [
    source 67
    target 625
  ]
  edge [
    source 67
    target 669
  ]
  edge [
    source 67
    target 670
  ]
  edge [
    source 67
    target 671
  ]
  edge [
    source 67
    target 349
  ]
  edge [
    source 67
    target 672
  ]
  edge [
    source 67
    target 673
  ]
  edge [
    source 67
    target 674
  ]
  edge [
    source 67
    target 675
  ]
  edge [
    source 67
    target 676
  ]
  edge [
    source 67
    target 677
  ]
  edge [
    source 67
    target 678
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 68
    target 291
  ]
  edge [
    source 68
    target 289
  ]
  edge [
    source 68
    target 70
  ]
  edge [
    source 69
    target 602
  ]
  edge [
    source 69
    target 679
  ]
  edge [
    source 69
    target 680
  ]
  edge [
    source 69
    target 681
  ]
  edge [
    source 69
    target 682
  ]
  edge [
    source 69
    target 683
  ]
  edge [
    source 69
    target 684
  ]
  edge [
    source 69
    target 533
  ]
  edge [
    source 69
    target 685
  ]
  edge [
    source 69
    target 686
  ]
  edge [
    source 69
    target 687
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 183
  ]
  edge [
    source 70
    target 688
  ]
  edge [
    source 70
    target 689
  ]
  edge [
    source 70
    target 690
  ]
  edge [
    source 70
    target 691
  ]
  edge [
    source 70
    target 692
  ]
  edge [
    source 70
    target 693
  ]
  edge [
    source 70
    target 694
  ]
  edge [
    source 70
    target 695
  ]
  edge [
    source 70
    target 696
  ]
  edge [
    source 70
    target 697
  ]
  edge [
    source 70
    target 698
  ]
  edge [
    source 70
    target 699
  ]
  edge [
    source 70
    target 700
  ]
  edge [
    source 70
    target 701
  ]
  edge [
    source 70
    target 642
  ]
  edge [
    source 70
    target 702
  ]
  edge [
    source 70
    target 703
  ]
  edge [
    source 70
    target 704
  ]
  edge [
    source 70
    target 705
  ]
  edge [
    source 70
    target 149
  ]
  edge [
    source 71
    target 706
  ]
  edge [
    source 71
    target 707
  ]
  edge [
    source 71
    target 708
  ]
  edge [
    source 71
    target 709
  ]
  edge [
    source 71
    target 710
  ]
  edge [
    source 71
    target 711
  ]
  edge [
    source 71
    target 712
  ]
  edge [
    source 71
    target 713
  ]
  edge [
    source 71
    target 714
  ]
  edge [
    source 71
    target 715
  ]
  edge [
    source 71
    target 716
  ]
  edge [
    source 71
    target 717
  ]
  edge [
    source 71
    target 718
  ]
  edge [
    source 71
    target 719
  ]
  edge [
    source 71
    target 625
  ]
  edge [
    source 71
    target 720
  ]
  edge [
    source 71
    target 721
  ]
  edge [
    source 71
    target 722
  ]
  edge [
    source 71
    target 723
  ]
  edge [
    source 71
    target 724
  ]
  edge [
    source 71
    target 725
  ]
  edge [
    source 71
    target 726
  ]
  edge [
    source 71
    target 727
  ]
  edge [
    source 71
    target 728
  ]
  edge [
    source 72
    target 73
  ]
  edge [
    source 72
    target 729
  ]
  edge [
    source 72
    target 730
  ]
  edge [
    source 72
    target 731
  ]
  edge [
    source 72
    target 732
  ]
  edge [
    source 73
    target 733
  ]
  edge [
    source 73
    target 325
  ]
  edge [
    source 73
    target 734
  ]
  edge [
    source 73
    target 735
  ]
  edge [
    source 74
    target 736
  ]
  edge [
    source 74
    target 737
  ]
  edge [
    source 74
    target 738
  ]
  edge [
    source 74
    target 739
  ]
  edge [
    source 74
    target 740
  ]
  edge [
    source 74
    target 741
  ]
  edge [
    source 74
    target 742
  ]
  edge [
    source 74
    target 743
  ]
  edge [
    source 74
    target 744
  ]
  edge [
    source 74
    target 676
  ]
  edge [
    source 74
    target 745
  ]
  edge [
    source 74
    target 746
  ]
  edge [
    source 75
    target 76
  ]
  edge [
    source 75
    target 747
  ]
  edge [
    source 75
    target 748
  ]
  edge [
    source 75
    target 749
  ]
  edge [
    source 75
    target 750
  ]
  edge [
    source 75
    target 751
  ]
  edge [
    source 75
    target 752
  ]
  edge [
    source 75
    target 753
  ]
  edge [
    source 75
    target 754
  ]
  edge [
    source 75
    target 755
  ]
  edge [
    source 75
    target 756
  ]
  edge [
    source 75
    target 331
  ]
  edge [
    source 75
    target 757
  ]
  edge [
    source 75
    target 273
  ]
  edge [
    source 75
    target 758
  ]
  edge [
    source 75
    target 759
  ]
  edge [
    source 75
    target 760
  ]
  edge [
    source 75
    target 761
  ]
  edge [
    source 75
    target 762
  ]
  edge [
    source 75
    target 763
  ]
  edge [
    source 75
    target 764
  ]
  edge [
    source 75
    target 765
  ]
  edge [
    source 75
    target 766
  ]
  edge [
    source 75
    target 767
  ]
  edge [
    source 75
    target 768
  ]
  edge [
    source 75
    target 769
  ]
  edge [
    source 75
    target 770
  ]
  edge [
    source 76
    target 77
  ]
  edge [
    source 76
    target 771
  ]
  edge [
    source 76
    target 674
  ]
  edge [
    source 76
    target 772
  ]
  edge [
    source 76
    target 773
  ]
  edge [
    source 76
    target 774
  ]
  edge [
    source 77
    target 78
  ]
  edge [
    source 77
    target 155
  ]
  edge [
    source 77
    target 156
  ]
  edge [
    source 77
    target 775
  ]
  edge [
    source 77
    target 776
  ]
  edge [
    source 77
    target 777
  ]
  edge [
    source 77
    target 778
  ]
  edge [
    source 77
    target 779
  ]
  edge [
    source 77
    target 780
  ]
  edge [
    source 77
    target 781
  ]
  edge [
    source 77
    target 782
  ]
  edge [
    source 77
    target 783
  ]
  edge [
    source 77
    target 81
  ]
  edge [
    source 77
    target 111
  ]
  edge [
    source 77
    target 141
  ]
  edge [
    source 77
    target 97
  ]
  edge [
    source 77
    target 101
  ]
  edge [
    source 77
    target 137
  ]
  edge [
    source 78
    target 79
  ]
  edge [
    source 78
    target 784
  ]
  edge [
    source 78
    target 785
  ]
  edge [
    source 78
    target 89
  ]
  edge [
    source 79
    target 144
  ]
  edge [
    source 79
    target 172
  ]
  edge [
    source 79
    target 786
  ]
  edge [
    source 80
    target 787
  ]
  edge [
    source 80
    target 788
  ]
  edge [
    source 80
    target 334
  ]
  edge [
    source 80
    target 427
  ]
  edge [
    source 80
    target 789
  ]
  edge [
    source 80
    target 790
  ]
  edge [
    source 80
    target 791
  ]
  edge [
    source 80
    target 792
  ]
  edge [
    source 80
    target 671
  ]
  edge [
    source 80
    target 793
  ]
  edge [
    source 81
    target 82
  ]
  edge [
    source 81
    target 183
  ]
  edge [
    source 81
    target 794
  ]
  edge [
    source 81
    target 795
  ]
  edge [
    source 81
    target 796
  ]
  edge [
    source 81
    target 797
  ]
  edge [
    source 81
    target 798
  ]
  edge [
    source 81
    target 111
  ]
  edge [
    source 81
    target 141
  ]
  edge [
    source 82
    target 83
  ]
  edge [
    source 82
    target 799
  ]
  edge [
    source 82
    target 800
  ]
  edge [
    source 82
    target 801
  ]
  edge [
    source 82
    target 802
  ]
  edge [
    source 82
    target 803
  ]
  edge [
    source 82
    target 492
  ]
  edge [
    source 82
    target 804
  ]
  edge [
    source 82
    target 805
  ]
  edge [
    source 82
    target 806
  ]
  edge [
    source 82
    target 807
  ]
  edge [
    source 82
    target 808
  ]
  edge [
    source 82
    target 809
  ]
  edge [
    source 82
    target 810
  ]
  edge [
    source 82
    target 811
  ]
  edge [
    source 83
    target 84
  ]
  edge [
    source 83
    target 812
  ]
  edge [
    source 83
    target 813
  ]
  edge [
    source 83
    target 364
  ]
  edge [
    source 84
    target 814
  ]
  edge [
    source 84
    target 815
  ]
  edge [
    source 86
    target 816
  ]
  edge [
    source 86
    target 667
  ]
  edge [
    source 86
    target 817
  ]
  edge [
    source 86
    target 818
  ]
  edge [
    source 86
    target 819
  ]
  edge [
    source 86
    target 673
  ]
  edge [
    source 86
    target 820
  ]
  edge [
    source 86
    target 821
  ]
  edge [
    source 86
    target 822
  ]
  edge [
    source 86
    target 120
  ]
  edge [
    source 86
    target 823
  ]
  edge [
    source 86
    target 824
  ]
  edge [
    source 86
    target 825
  ]
  edge [
    source 87
    target 88
  ]
  edge [
    source 88
    target 89
  ]
  edge [
    source 88
    target 94
  ]
  edge [
    source 88
    target 95
  ]
  edge [
    source 88
    target 106
  ]
  edge [
    source 88
    target 150
  ]
  edge [
    source 88
    target 98
  ]
  edge [
    source 88
    target 826
  ]
  edge [
    source 88
    target 827
  ]
  edge [
    source 88
    target 828
  ]
  edge [
    source 88
    target 829
  ]
  edge [
    source 89
    target 90
  ]
  edge [
    source 89
    target 830
  ]
  edge [
    source 89
    target 831
  ]
  edge [
    source 89
    target 832
  ]
  edge [
    source 89
    target 833
  ]
  edge [
    source 89
    target 834
  ]
  edge [
    source 89
    target 835
  ]
  edge [
    source 89
    target 335
  ]
  edge [
    source 89
    target 836
  ]
  edge [
    source 89
    target 837
  ]
  edge [
    source 89
    target 838
  ]
  edge [
    source 89
    target 839
  ]
  edge [
    source 89
    target 840
  ]
  edge [
    source 89
    target 841
  ]
  edge [
    source 89
    target 842
  ]
  edge [
    source 89
    target 843
  ]
  edge [
    source 89
    target 844
  ]
  edge [
    source 89
    target 845
  ]
  edge [
    source 89
    target 846
  ]
  edge [
    source 89
    target 847
  ]
  edge [
    source 89
    target 784
  ]
  edge [
    source 89
    target 848
  ]
  edge [
    source 89
    target 849
  ]
  edge [
    source 89
    target 850
  ]
  edge [
    source 89
    target 851
  ]
  edge [
    source 89
    target 852
  ]
  edge [
    source 89
    target 853
  ]
  edge [
    source 89
    target 854
  ]
  edge [
    source 89
    target 855
  ]
  edge [
    source 89
    target 856
  ]
  edge [
    source 89
    target 857
  ]
  edge [
    source 89
    target 858
  ]
  edge [
    source 89
    target 859
  ]
  edge [
    source 89
    target 860
  ]
  edge [
    source 89
    target 861
  ]
  edge [
    source 89
    target 862
  ]
  edge [
    source 89
    target 863
  ]
  edge [
    source 89
    target 864
  ]
  edge [
    source 90
    target 91
  ]
  edge [
    source 90
    target 865
  ]
  edge [
    source 90
    target 866
  ]
  edge [
    source 90
    target 867
  ]
  edge [
    source 91
    target 92
  ]
  edge [
    source 91
    target 868
  ]
  edge [
    source 91
    target 869
  ]
  edge [
    source 91
    target 870
  ]
  edge [
    source 91
    target 871
  ]
  edge [
    source 91
    target 872
  ]
  edge [
    source 91
    target 873
  ]
  edge [
    source 91
    target 874
  ]
  edge [
    source 92
    target 93
  ]
  edge [
    source 92
    target 875
  ]
  edge [
    source 92
    target 876
  ]
  edge [
    source 92
    target 877
  ]
  edge [
    source 92
    target 878
  ]
  edge [
    source 92
    target 879
  ]
  edge [
    source 92
    target 880
  ]
  edge [
    source 92
    target 881
  ]
  edge [
    source 92
    target 882
  ]
  edge [
    source 92
    target 883
  ]
  edge [
    source 92
    target 884
  ]
  edge [
    source 92
    target 885
  ]
  edge [
    source 92
    target 886
  ]
  edge [
    source 92
    target 887
  ]
  edge [
    source 92
    target 151
  ]
  edge [
    source 93
    target 888
  ]
  edge [
    source 93
    target 889
  ]
  edge [
    source 93
    target 890
  ]
  edge [
    source 93
    target 891
  ]
  edge [
    source 93
    target 892
  ]
  edge [
    source 93
    target 893
  ]
  edge [
    source 93
    target 120
  ]
  edge [
    source 93
    target 894
  ]
  edge [
    source 93
    target 895
  ]
  edge [
    source 94
    target 896
  ]
  edge [
    source 94
    target 897
  ]
  edge [
    source 94
    target 898
  ]
  edge [
    source 94
    target 855
  ]
  edge [
    source 94
    target 899
  ]
  edge [
    source 94
    target 900
  ]
  edge [
    source 94
    target 901
  ]
  edge [
    source 94
    target 902
  ]
  edge [
    source 95
    target 96
  ]
  edge [
    source 95
    target 153
  ]
  edge [
    source 95
    target 903
  ]
  edge [
    source 95
    target 904
  ]
  edge [
    source 95
    target 905
  ]
  edge [
    source 95
    target 906
  ]
  edge [
    source 95
    target 907
  ]
  edge [
    source 95
    target 908
  ]
  edge [
    source 95
    target 909
  ]
  edge [
    source 95
    target 910
  ]
  edge [
    source 95
    target 911
  ]
  edge [
    source 95
    target 912
  ]
  edge [
    source 95
    target 913
  ]
  edge [
    source 96
    target 97
  ]
  edge [
    source 96
    target 98
  ]
  edge [
    source 96
    target 151
  ]
  edge [
    source 97
    target 914
  ]
  edge [
    source 97
    target 915
  ]
  edge [
    source 97
    target 792
  ]
  edge [
    source 97
    target 494
  ]
  edge [
    source 97
    target 916
  ]
  edge [
    source 97
    target 101
  ]
  edge [
    source 97
    target 137
  ]
  edge [
    source 97
    target 155
  ]
  edge [
    source 98
    target 917
  ]
  edge [
    source 98
    target 918
  ]
  edge [
    source 98
    target 919
  ]
  edge [
    source 98
    target 920
  ]
  edge [
    source 98
    target 921
  ]
  edge [
    source 99
    target 922
  ]
  edge [
    source 99
    target 923
  ]
  edge [
    source 99
    target 924
  ]
  edge [
    source 99
    target 925
  ]
  edge [
    source 99
    target 926
  ]
  edge [
    source 99
    target 533
  ]
  edge [
    source 99
    target 927
  ]
  edge [
    source 99
    target 464
  ]
  edge [
    source 99
    target 928
  ]
  edge [
    source 100
    target 101
  ]
  edge [
    source 100
    target 929
  ]
  edge [
    source 100
    target 930
  ]
  edge [
    source 100
    target 931
  ]
  edge [
    source 100
    target 932
  ]
  edge [
    source 101
    target 102
  ]
  edge [
    source 101
    target 137
  ]
  edge [
    source 101
    target 155
  ]
  edge [
    source 102
    target 103
  ]
  edge [
    source 102
    target 933
  ]
  edge [
    source 102
    target 349
  ]
  edge [
    source 102
    target 934
  ]
  edge [
    source 102
    target 935
  ]
  edge [
    source 102
    target 936
  ]
  edge [
    source 102
    target 937
  ]
  edge [
    source 102
    target 938
  ]
  edge [
    source 102
    target 939
  ]
  edge [
    source 102
    target 940
  ]
  edge [
    source 102
    target 941
  ]
  edge [
    source 103
    target 104
  ]
  edge [
    source 103
    target 942
  ]
  edge [
    source 103
    target 943
  ]
  edge [
    source 103
    target 944
  ]
  edge [
    source 103
    target 945
  ]
  edge [
    source 103
    target 946
  ]
  edge [
    source 103
    target 642
  ]
  edge [
    source 103
    target 947
  ]
  edge [
    source 104
    target 105
  ]
  edge [
    source 104
    target 948
  ]
  edge [
    source 105
    target 949
  ]
  edge [
    source 105
    target 950
  ]
  edge [
    source 105
    target 951
  ]
  edge [
    source 105
    target 952
  ]
  edge [
    source 105
    target 953
  ]
  edge [
    source 105
    target 954
  ]
  edge [
    source 105
    target 955
  ]
  edge [
    source 105
    target 956
  ]
  edge [
    source 105
    target 957
  ]
  edge [
    source 105
    target 958
  ]
  edge [
    source 105
    target 959
  ]
  edge [
    source 106
    target 107
  ]
  edge [
    source 106
    target 960
  ]
  edge [
    source 106
    target 961
  ]
  edge [
    source 106
    target 962
  ]
  edge [
    source 106
    target 963
  ]
  edge [
    source 107
    target 108
  ]
  edge [
    source 107
    target 964
  ]
  edge [
    source 107
    target 965
  ]
  edge [
    source 107
    target 966
  ]
  edge [
    source 107
    target 967
  ]
  edge [
    source 108
    target 167
  ]
  edge [
    source 108
    target 968
  ]
  edge [
    source 108
    target 969
  ]
  edge [
    source 108
    target 970
  ]
  edge [
    source 108
    target 971
  ]
  edge [
    source 108
    target 785
  ]
  edge [
    source 108
    target 972
  ]
  edge [
    source 108
    target 973
  ]
  edge [
    source 108
    target 974
  ]
  edge [
    source 108
    target 975
  ]
  edge [
    source 108
    target 976
  ]
  edge [
    source 108
    target 977
  ]
  edge [
    source 108
    target 608
  ]
  edge [
    source 108
    target 978
  ]
  edge [
    source 108
    target 979
  ]
  edge [
    source 108
    target 980
  ]
  edge [
    source 108
    target 981
  ]
  edge [
    source 108
    target 982
  ]
  edge [
    source 108
    target 983
  ]
  edge [
    source 108
    target 984
  ]
  edge [
    source 108
    target 985
  ]
  edge [
    source 108
    target 986
  ]
  edge [
    source 108
    target 765
  ]
  edge [
    source 108
    target 987
  ]
  edge [
    source 108
    target 988
  ]
  edge [
    source 108
    target 989
  ]
  edge [
    source 108
    target 990
  ]
  edge [
    source 108
    target 506
  ]
  edge [
    source 108
    target 991
  ]
  edge [
    source 108
    target 793
  ]
  edge [
    source 109
    target 110
  ]
  edge [
    source 109
    target 992
  ]
  edge [
    source 109
    target 993
  ]
  edge [
    source 109
    target 994
  ]
  edge [
    source 109
    target 995
  ]
  edge [
    source 109
    target 996
  ]
  edge [
    source 109
    target 997
  ]
  edge [
    source 109
    target 998
  ]
  edge [
    source 109
    target 999
  ]
  edge [
    source 109
    target 1000
  ]
  edge [
    source 109
    target 1001
  ]
  edge [
    source 109
    target 1002
  ]
  edge [
    source 109
    target 1003
  ]
  edge [
    source 109
    target 1004
  ]
  edge [
    source 109
    target 1005
  ]
  edge [
    source 109
    target 1006
  ]
  edge [
    source 109
    target 1007
  ]
  edge [
    source 109
    target 1008
  ]
  edge [
    source 109
    target 1009
  ]
  edge [
    source 109
    target 1010
  ]
  edge [
    source 109
    target 1011
  ]
  edge [
    source 109
    target 1012
  ]
  edge [
    source 109
    target 1013
  ]
  edge [
    source 109
    target 1014
  ]
  edge [
    source 109
    target 1015
  ]
  edge [
    source 109
    target 1016
  ]
  edge [
    source 109
    target 1017
  ]
  edge [
    source 109
    target 1018
  ]
  edge [
    source 109
    target 1019
  ]
  edge [
    source 109
    target 1020
  ]
  edge [
    source 109
    target 1021
  ]
  edge [
    source 109
    target 1022
  ]
  edge [
    source 109
    target 1023
  ]
  edge [
    source 109
    target 1024
  ]
  edge [
    source 109
    target 1025
  ]
  edge [
    source 109
    target 1026
  ]
  edge [
    source 109
    target 1027
  ]
  edge [
    source 109
    target 1028
  ]
  edge [
    source 109
    target 1029
  ]
  edge [
    source 109
    target 1030
  ]
  edge [
    source 109
    target 1031
  ]
  edge [
    source 109
    target 1032
  ]
  edge [
    source 109
    target 1033
  ]
  edge [
    source 109
    target 1034
  ]
  edge [
    source 109
    target 1035
  ]
  edge [
    source 109
    target 1036
  ]
  edge [
    source 109
    target 1037
  ]
  edge [
    source 109
    target 1038
  ]
  edge [
    source 109
    target 1039
  ]
  edge [
    source 109
    target 1040
  ]
  edge [
    source 109
    target 1041
  ]
  edge [
    source 109
    target 1042
  ]
  edge [
    source 109
    target 1043
  ]
  edge [
    source 109
    target 1044
  ]
  edge [
    source 109
    target 1045
  ]
  edge [
    source 109
    target 1046
  ]
  edge [
    source 110
    target 111
  ]
  edge [
    source 110
    target 974
  ]
  edge [
    source 110
    target 1047
  ]
  edge [
    source 110
    target 1048
  ]
  edge [
    source 110
    target 1049
  ]
  edge [
    source 110
    target 1050
  ]
  edge [
    source 110
    target 1051
  ]
  edge [
    source 110
    target 1052
  ]
  edge [
    source 111
    target 112
  ]
  edge [
    source 111
    target 1053
  ]
  edge [
    source 111
    target 1054
  ]
  edge [
    source 111
    target 1055
  ]
  edge [
    source 111
    target 1056
  ]
  edge [
    source 111
    target 1057
  ]
  edge [
    source 111
    target 1058
  ]
  edge [
    source 111
    target 1059
  ]
  edge [
    source 111
    target 1060
  ]
  edge [
    source 111
    target 1061
  ]
  edge [
    source 111
    target 1062
  ]
  edge [
    source 111
    target 1063
  ]
  edge [
    source 111
    target 1064
  ]
  edge [
    source 111
    target 1065
  ]
  edge [
    source 111
    target 1066
  ]
  edge [
    source 111
    target 141
  ]
  edge [
    source 112
    target 113
  ]
  edge [
    source 112
    target 1067
  ]
  edge [
    source 112
    target 1068
  ]
  edge [
    source 112
    target 1069
  ]
  edge [
    source 112
    target 1070
  ]
  edge [
    source 112
    target 1071
  ]
  edge [
    source 112
    target 1072
  ]
  edge [
    source 112
    target 1073
  ]
  edge [
    source 112
    target 1074
  ]
  edge [
    source 112
    target 1075
  ]
  edge [
    source 112
    target 1076
  ]
  edge [
    source 112
    target 1077
  ]
  edge [
    source 112
    target 1078
  ]
  edge [
    source 112
    target 1079
  ]
  edge [
    source 112
    target 1080
  ]
  edge [
    source 112
    target 1081
  ]
  edge [
    source 112
    target 1082
  ]
  edge [
    source 112
    target 1083
  ]
  edge [
    source 112
    target 350
  ]
  edge [
    source 112
    target 1084
  ]
  edge [
    source 112
    target 1085
  ]
  edge [
    source 112
    target 1086
  ]
  edge [
    source 113
    target 114
  ]
  edge [
    source 114
    target 135
  ]
  edge [
    source 115
    target 116
  ]
  edge [
    source 116
    target 117
  ]
  edge [
    source 116
    target 1087
  ]
  edge [
    source 117
    target 1088
  ]
  edge [
    source 117
    target 1089
  ]
  edge [
    source 117
    target 325
  ]
  edge [
    source 117
    target 1090
  ]
  edge [
    source 117
    target 130
  ]
  edge [
    source 117
    target 145
  ]
  edge [
    source 118
    target 119
  ]
  edge [
    source 118
    target 1091
  ]
  edge [
    source 119
    target 120
  ]
  edge [
    source 119
    target 1092
  ]
  edge [
    source 120
    target 121
  ]
  edge [
    source 120
    target 1093
  ]
  edge [
    source 120
    target 1094
  ]
  edge [
    source 120
    target 1095
  ]
  edge [
    source 120
    target 1096
  ]
  edge [
    source 120
    target 1097
  ]
  edge [
    source 120
    target 935
  ]
  edge [
    source 120
    target 1098
  ]
  edge [
    source 120
    target 1099
  ]
  edge [
    source 120
    target 1100
  ]
  edge [
    source 120
    target 1101
  ]
  edge [
    source 120
    target 1102
  ]
  edge [
    source 120
    target 1103
  ]
  edge [
    source 120
    target 1104
  ]
  edge [
    source 120
    target 1105
  ]
  edge [
    source 120
    target 146
  ]
  edge [
    source 121
    target 122
  ]
  edge [
    source 121
    target 1106
  ]
  edge [
    source 121
    target 1107
  ]
  edge [
    source 122
    target 123
  ]
  edge [
    source 122
    target 1106
  ]
  edge [
    source 122
    target 1108
  ]
  edge [
    source 122
    target 1109
  ]
  edge [
    source 122
    target 1110
  ]
  edge [
    source 122
    target 1111
  ]
  edge [
    source 122
    target 1107
  ]
  edge [
    source 123
    target 124
  ]
  edge [
    source 123
    target 708
  ]
  edge [
    source 123
    target 1112
  ]
  edge [
    source 123
    target 1113
  ]
  edge [
    source 123
    target 1114
  ]
  edge [
    source 124
    target 1115
  ]
  edge [
    source 124
    target 1116
  ]
  edge [
    source 124
    target 1117
  ]
  edge [
    source 125
    target 126
  ]
  edge [
    source 125
    target 1118
  ]
  edge [
    source 125
    target 1119
  ]
  edge [
    source 125
    target 1120
  ]
  edge [
    source 125
    target 1121
  ]
  edge [
    source 126
    target 127
  ]
  edge [
    source 126
    target 161
  ]
  edge [
    source 126
    target 165
  ]
  edge [
    source 126
    target 1122
  ]
  edge [
    source 127
    target 1123
  ]
  edge [
    source 127
    target 1124
  ]
  edge [
    source 127
    target 1125
  ]
  edge [
    source 127
    target 1126
  ]
  edge [
    source 127
    target 1127
  ]
  edge [
    source 127
    target 1128
  ]
  edge [
    source 127
    target 1129
  ]
  edge [
    source 127
    target 1130
  ]
  edge [
    source 127
    target 1131
  ]
  edge [
    source 127
    target 1132
  ]
  edge [
    source 127
    target 1133
  ]
  edge [
    source 127
    target 1134
  ]
  edge [
    source 127
    target 1135
  ]
  edge [
    source 127
    target 1136
  ]
  edge [
    source 128
    target 129
  ]
  edge [
    source 128
    target 1054
  ]
  edge [
    source 128
    target 1137
  ]
  edge [
    source 128
    target 1138
  ]
  edge [
    source 128
    target 1139
  ]
  edge [
    source 128
    target 1140
  ]
  edge [
    source 128
    target 1141
  ]
  edge [
    source 128
    target 1142
  ]
  edge [
    source 128
    target 1143
  ]
  edge [
    source 128
    target 1144
  ]
  edge [
    source 128
    target 1145
  ]
  edge [
    source 128
    target 1146
  ]
  edge [
    source 128
    target 1147
  ]
  edge [
    source 128
    target 882
  ]
  edge [
    source 128
    target 1148
  ]
  edge [
    source 128
    target 1149
  ]
  edge [
    source 128
    target 1150
  ]
  edge [
    source 128
    target 1151
  ]
  edge [
    source 128
    target 1152
  ]
  edge [
    source 128
    target 1153
  ]
  edge [
    source 128
    target 1154
  ]
  edge [
    source 128
    target 1155
  ]
  edge [
    source 128
    target 1156
  ]
  edge [
    source 128
    target 1157
  ]
  edge [
    source 128
    target 1158
  ]
  edge [
    source 128
    target 1159
  ]
  edge [
    source 128
    target 1160
  ]
  edge [
    source 128
    target 1161
  ]
  edge [
    source 128
    target 1162
  ]
  edge [
    source 128
    target 1163
  ]
  edge [
    source 128
    target 761
  ]
  edge [
    source 128
    target 1164
  ]
  edge [
    source 128
    target 1165
  ]
  edge [
    source 128
    target 1166
  ]
  edge [
    source 128
    target 1167
  ]
  edge [
    source 128
    target 1168
  ]
  edge [
    source 128
    target 1169
  ]
  edge [
    source 128
    target 1103
  ]
  edge [
    source 128
    target 1170
  ]
  edge [
    source 128
    target 1171
  ]
  edge [
    source 128
    target 1172
  ]
  edge [
    source 129
    target 130
  ]
  edge [
    source 130
    target 1173
  ]
  edge [
    source 130
    target 1174
  ]
  edge [
    source 130
    target 145
  ]
  edge [
    source 131
    target 132
  ]
  edge [
    source 131
    target 151
  ]
  edge [
    source 131
    target 152
  ]
  edge [
    source 131
    target 1175
  ]
  edge [
    source 131
    target 1176
  ]
  edge [
    source 132
    target 133
  ]
  edge [
    source 132
    target 1177
  ]
  edge [
    source 132
    target 1178
  ]
  edge [
    source 132
    target 1179
  ]
  edge [
    source 132
    target 1180
  ]
  edge [
    source 132
    target 1181
  ]
  edge [
    source 132
    target 1182
  ]
  edge [
    source 132
    target 1183
  ]
  edge [
    source 132
    target 1184
  ]
  edge [
    source 132
    target 1185
  ]
  edge [
    source 133
    target 1186
  ]
  edge [
    source 133
    target 1187
  ]
  edge [
    source 133
    target 1188
  ]
  edge [
    source 133
    target 1189
  ]
  edge [
    source 133
    target 1190
  ]
  edge [
    source 133
    target 1191
  ]
  edge [
    source 133
    target 1192
  ]
  edge [
    source 133
    target 1193
  ]
  edge [
    source 133
    target 1194
  ]
  edge [
    source 133
    target 1195
  ]
  edge [
    source 133
    target 1196
  ]
  edge [
    source 133
    target 1197
  ]
  edge [
    source 134
    target 1198
  ]
  edge [
    source 135
    target 136
  ]
  edge [
    source 135
    target 1199
  ]
  edge [
    source 135
    target 1200
  ]
  edge [
    source 135
    target 1201
  ]
  edge [
    source 135
    target 1202
  ]
  edge [
    source 135
    target 1203
  ]
  edge [
    source 135
    target 1204
  ]
  edge [
    source 135
    target 1205
  ]
  edge [
    source 135
    target 1206
  ]
  edge [
    source 136
    target 137
  ]
  edge [
    source 136
    target 590
  ]
  edge [
    source 136
    target 1207
  ]
  edge [
    source 136
    target 1208
  ]
  edge [
    source 136
    target 376
  ]
  edge [
    source 136
    target 1202
  ]
  edge [
    source 136
    target 1160
  ]
  edge [
    source 136
    target 1209
  ]
  edge [
    source 136
    target 1210
  ]
  edge [
    source 136
    target 1211
  ]
  edge [
    source 137
    target 1212
  ]
  edge [
    source 137
    target 1213
  ]
  edge [
    source 137
    target 1214
  ]
  edge [
    source 137
    target 1215
  ]
  edge [
    source 137
    target 1216
  ]
  edge [
    source 137
    target 1217
  ]
  edge [
    source 137
    target 155
  ]
  edge [
    source 138
    target 1218
  ]
  edge [
    source 139
    target 140
  ]
  edge [
    source 139
    target 898
  ]
  edge [
    source 139
    target 1219
  ]
  edge [
    source 139
    target 1220
  ]
  edge [
    source 139
    target 1221
  ]
  edge [
    source 139
    target 1222
  ]
  edge [
    source 139
    target 1223
  ]
  edge [
    source 139
    target 1224
  ]
  edge [
    source 140
    target 141
  ]
  edge [
    source 140
    target 1225
  ]
  edge [
    source 140
    target 1226
  ]
  edge [
    source 140
    target 1227
  ]
  edge [
    source 140
    target 1228
  ]
  edge [
    source 140
    target 1229
  ]
  edge [
    source 140
    target 1230
  ]
  edge [
    source 140
    target 1231
  ]
  edge [
    source 140
    target 1232
  ]
  edge [
    source 140
    target 709
  ]
  edge [
    source 140
    target 1233
  ]
  edge [
    source 140
    target 1234
  ]
  edge [
    source 140
    target 1235
  ]
  edge [
    source 140
    target 1236
  ]
  edge [
    source 140
    target 315
  ]
  edge [
    source 141
    target 142
  ]
  edge [
    source 141
    target 1237
  ]
  edge [
    source 141
    target 1238
  ]
  edge [
    source 141
    target 1239
  ]
  edge [
    source 141
    target 1240
  ]
  edge [
    source 141
    target 1241
  ]
  edge [
    source 141
    target 177
  ]
  edge [
    source 141
    target 1242
  ]
  edge [
    source 141
    target 1174
  ]
  edge [
    source 141
    target 1243
  ]
  edge [
    source 141
    target 1244
  ]
  edge [
    source 142
    target 143
  ]
  edge [
    source 142
    target 1245
  ]
  edge [
    source 142
    target 1246
  ]
  edge [
    source 142
    target 1247
  ]
  edge [
    source 142
    target 1248
  ]
  edge [
    source 142
    target 1249
  ]
  edge [
    source 142
    target 1250
  ]
  edge [
    source 142
    target 1251
  ]
  edge [
    source 143
    target 144
  ]
  edge [
    source 143
    target 1252
  ]
  edge [
    source 143
    target 1253
  ]
  edge [
    source 144
    target 568
  ]
  edge [
    source 144
    target 1254
  ]
  edge [
    source 144
    target 1255
  ]
  edge [
    source 144
    target 1256
  ]
  edge [
    source 144
    target 1257
  ]
  edge [
    source 144
    target 1258
  ]
  edge [
    source 145
    target 146
  ]
  edge [
    source 146
    target 1259
  ]
  edge [
    source 146
    target 1260
  ]
  edge [
    source 146
    target 661
  ]
  edge [
    source 146
    target 349
  ]
  edge [
    source 146
    target 461
  ]
  edge [
    source 146
    target 1261
  ]
  edge [
    source 147
    target 148
  ]
  edge [
    source 147
    target 1262
  ]
  edge [
    source 147
    target 1263
  ]
  edge [
    source 147
    target 1204
  ]
  edge [
    source 147
    target 1264
  ]
  edge [
    source 148
    target 149
  ]
  edge [
    source 149
    target 150
  ]
  edge [
    source 149
    target 1265
  ]
  edge [
    source 149
    target 1266
  ]
  edge [
    source 149
    target 1267
  ]
  edge [
    source 149
    target 1268
  ]
  edge [
    source 152
    target 153
  ]
  edge [
    source 152
    target 1269
  ]
  edge [
    source 152
    target 1270
  ]
  edge [
    source 152
    target 1271
  ]
  edge [
    source 152
    target 1272
  ]
  edge [
    source 152
    target 1273
  ]
  edge [
    source 152
    target 1274
  ]
  edge [
    source 152
    target 1275
  ]
  edge [
    source 152
    target 1276
  ]
  edge [
    source 152
    target 1277
  ]
  edge [
    source 152
    target 1278
  ]
  edge [
    source 152
    target 254
  ]
  edge [
    source 152
    target 1279
  ]
  edge [
    source 152
    target 1280
  ]
  edge [
    source 152
    target 1281
  ]
  edge [
    source 152
    target 1282
  ]
  edge [
    source 153
    target 1283
  ]
  edge [
    source 153
    target 1284
  ]
  edge [
    source 153
    target 1285
  ]
  edge [
    source 154
    target 155
  ]
  edge [
    source 155
    target 482
  ]
  edge [
    source 155
    target 325
  ]
  edge [
    source 155
    target 1286
  ]
  edge [
    source 155
    target 1287
  ]
  edge [
    source 155
    target 1288
  ]
  edge [
    source 155
    target 1289
  ]
  edge [
    source 155
    target 1290
  ]
  edge [
    source 155
    target 308
  ]
  edge [
    source 156
    target 1291
  ]
  edge [
    source 156
    target 1292
  ]
  edge [
    source 156
    target 1293
  ]
  edge [
    source 156
    target 1294
  ]
  edge [
    source 156
    target 1295
  ]
]
