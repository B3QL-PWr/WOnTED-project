graph [
  maxDegree 18
  minDegree 1
  meanDegree 2.3333333333333335
  density 0.03954802259887006
  graphCliqueNumber 5
  node [
    id 0
    label "ulica"
    origin "text"
  ]
  node [
    id 1
    label "tadeusz"
    origin "text"
  ]
  node [
    id 2
    label "ko&#347;ciuszko"
    origin "text"
  ]
  node [
    id 3
    label "mi&#324;ski"
    origin "text"
  ]
  node [
    id 4
    label "mazowiecki"
    origin "text"
  ]
  node [
    id 5
    label "&#347;rodowisko"
  ]
  node [
    id 6
    label "miasteczko"
  ]
  node [
    id 7
    label "streetball"
  ]
  node [
    id 8
    label "pierzeja"
  ]
  node [
    id 9
    label "grupa"
  ]
  node [
    id 10
    label "pas_ruchu"
  ]
  node [
    id 11
    label "pas_rozdzielczy"
  ]
  node [
    id 12
    label "jezdnia"
  ]
  node [
    id 13
    label "droga"
  ]
  node [
    id 14
    label "korona_drogi"
  ]
  node [
    id 15
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 16
    label "chodnik"
  ]
  node [
    id 17
    label "arteria"
  ]
  node [
    id 18
    label "Broadway"
  ]
  node [
    id 19
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 20
    label "wysepka"
  ]
  node [
    id 21
    label "autostrada"
  ]
  node [
    id 22
    label "po_mi&#324;sku"
  ]
  node [
    id 23
    label "bia&#322;oruski"
  ]
  node [
    id 24
    label "polski"
  ]
  node [
    id 25
    label "po_mazowiecku"
  ]
  node [
    id 26
    label "regionalny"
  ]
  node [
    id 27
    label "Tadeusz"
  ]
  node [
    id 28
    label "Ko&#347;ciuszko"
  ]
  node [
    id 29
    label "i"
  ]
  node [
    id 30
    label "wojna"
  ]
  node [
    id 31
    label "&#347;wiatowy"
  ]
  node [
    id 32
    label "starostwo"
  ]
  node [
    id 33
    label "powiatowy"
  ]
  node [
    id 34
    label "urz&#261;d"
  ]
  node [
    id 35
    label "miasto"
  ]
  node [
    id 36
    label "PKO"
  ]
  node [
    id 37
    label "b&#322;ogos&#322;awionej&#160;pami&#281;ci"
  ]
  node [
    id 38
    label "co"
  ]
  node [
    id 39
    label "s&#322;ycha&#263;"
  ]
  node [
    id 40
    label "przychodzie&#324;"
  ]
  node [
    id 41
    label "rejonowy"
  ]
  node [
    id 42
    label "zak&#322;ad"
  ]
  node [
    id 43
    label "opieka"
  ]
  node [
    id 44
    label "zdrowotny"
  ]
  node [
    id 45
    label "przedszkole"
  ]
  node [
    id 46
    label "miejski"
  ]
  node [
    id 47
    label "nr"
  ]
  node [
    id 48
    label "1"
  ]
  node [
    id 49
    label "wojew&#243;dzki"
  ]
  node [
    id 50
    label "inspektorat"
  ]
  node [
    id 51
    label "ochrona"
  ]
  node [
    id 52
    label "agencja"
  ]
  node [
    id 53
    label "restrukturyzacja"
  ]
  node [
    id 54
    label "modernizacja"
  ]
  node [
    id 55
    label "rolnictwo"
  ]
  node [
    id 56
    label "salezja&#324;ski"
  ]
  node [
    id 57
    label "liceum"
  ]
  node [
    id 58
    label "og&#243;lnokszta&#322;c&#261;cy"
  ]
  node [
    id 59
    label "gimnazjum"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 31
  ]
  edge [
    source 29
    target 52
  ]
  edge [
    source 29
    target 53
  ]
  edge [
    source 29
    target 54
  ]
  edge [
    source 29
    target 55
  ]
  edge [
    source 29
    target 56
  ]
  edge [
    source 29
    target 57
  ]
  edge [
    source 29
    target 58
  ]
  edge [
    source 29
    target 59
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 44
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 47
  ]
  edge [
    source 45
    target 48
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 48
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 51
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 54
  ]
  edge [
    source 52
    target 55
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 55
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 58
  ]
  edge [
    source 56
    target 59
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 59
  ]
  edge [
    source 58
    target 59
  ]
]
