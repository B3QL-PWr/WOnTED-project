graph [
  maxDegree 14
  minDegree 1
  meanDegree 2.0294117647058822
  density 0.03028972783143108
  graphCliqueNumber 3
  node [
    id 0
    label "cho&#263;"
    origin "text"
  ]
  node [
    id 1
    label "niemcy"
    origin "text"
  ]
  node [
    id 2
    label "zarabia&#263;"
    origin "text"
  ]
  node [
    id 3
    label "znacznie"
    origin "text"
  ]
  node [
    id 4
    label "du&#380;o"
    origin "text"
  ]
  node [
    id 5
    label "wiele"
    origin "text"
  ]
  node [
    id 6
    label "produkt"
    origin "text"
  ]
  node [
    id 7
    label "swoje"
    origin "text"
  ]
  node [
    id 8
    label "sklep"
    origin "text"
  ]
  node [
    id 9
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 10
    label "kupi&#263;"
    origin "text"
  ]
  node [
    id 11
    label "tanio"
    origin "text"
  ]
  node [
    id 12
    label "polska"
    origin "text"
  ]
  node [
    id 13
    label "get"
  ]
  node [
    id 14
    label "niszczy&#263;"
  ]
  node [
    id 15
    label "dostawa&#263;"
  ]
  node [
    id 16
    label "pozyskiwa&#263;"
  ]
  node [
    id 17
    label "zas&#322;ugiwa&#263;"
  ]
  node [
    id 18
    label "ugniata&#263;"
  ]
  node [
    id 19
    label "zaczyna&#263;"
  ]
  node [
    id 20
    label "m&#281;czy&#263;"
  ]
  node [
    id 21
    label "wype&#322;nia&#263;"
  ]
  node [
    id 22
    label "miesza&#263;"
  ]
  node [
    id 23
    label "pracowa&#263;"
  ]
  node [
    id 24
    label "net_income"
  ]
  node [
    id 25
    label "znaczny"
  ]
  node [
    id 26
    label "zauwa&#380;alnie"
  ]
  node [
    id 27
    label "du&#380;y"
  ]
  node [
    id 28
    label "cz&#281;sto"
  ]
  node [
    id 29
    label "bardzo"
  ]
  node [
    id 30
    label "mocno"
  ]
  node [
    id 31
    label "wiela"
  ]
  node [
    id 32
    label "production"
  ]
  node [
    id 33
    label "substancja"
  ]
  node [
    id 34
    label "rezultat"
  ]
  node [
    id 35
    label "wytw&#243;r"
  ]
  node [
    id 36
    label "stoisko"
  ]
  node [
    id 37
    label "sk&#322;ad"
  ]
  node [
    id 38
    label "firma"
  ]
  node [
    id 39
    label "lokal_u&#380;ytkowy"
  ]
  node [
    id 40
    label "witryna"
  ]
  node [
    id 41
    label "obiekt_handlowy"
  ]
  node [
    id 42
    label "zaplecze"
  ]
  node [
    id 43
    label "p&#243;&#322;ka"
  ]
  node [
    id 44
    label "by&#263;"
  ]
  node [
    id 45
    label "uprawi&#263;"
  ]
  node [
    id 46
    label "gotowy"
  ]
  node [
    id 47
    label "might"
  ]
  node [
    id 48
    label "wzi&#261;&#263;"
  ]
  node [
    id 49
    label "catch"
  ]
  node [
    id 50
    label "przyj&#261;&#263;"
  ]
  node [
    id 51
    label "beget"
  ]
  node [
    id 52
    label "pozyska&#263;"
  ]
  node [
    id 53
    label "ustawi&#263;"
  ]
  node [
    id 54
    label "uzna&#263;"
  ]
  node [
    id 55
    label "zagra&#263;"
  ]
  node [
    id 56
    label "uwierzy&#263;"
  ]
  node [
    id 57
    label "p&#322;atnie"
  ]
  node [
    id 58
    label "tandetnie"
  ]
  node [
    id 59
    label "taniej"
  ]
  node [
    id 60
    label "najtaniej"
  ]
  node [
    id 61
    label "tani"
  ]
  node [
    id 62
    label "niedrogi"
  ]
  node [
    id 63
    label "g&#322;&#243;wny"
  ]
  node [
    id 64
    label "urz&#261;d"
  ]
  node [
    id 65
    label "statystyczny"
  ]
  node [
    id 66
    label "dolny"
  ]
  node [
    id 67
    label "&#347;l&#261;ski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 36
  ]
  edge [
    source 8
    target 37
  ]
  edge [
    source 8
    target 38
  ]
  edge [
    source 8
    target 39
  ]
  edge [
    source 8
    target 40
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 44
  ]
  edge [
    source 9
    target 45
  ]
  edge [
    source 9
    target 46
  ]
  edge [
    source 9
    target 47
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 13
  ]
  edge [
    source 10
    target 48
  ]
  edge [
    source 10
    target 49
  ]
  edge [
    source 10
    target 50
  ]
  edge [
    source 10
    target 51
  ]
  edge [
    source 10
    target 52
  ]
  edge [
    source 10
    target 53
  ]
  edge [
    source 10
    target 54
  ]
  edge [
    source 10
    target 55
  ]
  edge [
    source 10
    target 56
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 57
  ]
  edge [
    source 11
    target 58
  ]
  edge [
    source 11
    target 59
  ]
  edge [
    source 11
    target 60
  ]
  edge [
    source 11
    target 61
  ]
  edge [
    source 11
    target 62
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 65
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 66
    target 67
  ]
]
