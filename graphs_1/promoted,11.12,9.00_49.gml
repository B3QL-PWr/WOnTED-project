graph [
  maxDegree 14
  minDegree 1
  meanDegree 1.945945945945946
  density 0.05405405405405406
  graphCliqueNumber 2
  node [
    id 0
    label "klinika"
    origin "text"
  ]
  node [
    id 1
    label "budzik"
    origin "text"
  ]
  node [
    id 2
    label "dla"
    origin "text"
  ]
  node [
    id 3
    label "doros&#322;a"
    origin "text"
  ]
  node [
    id 4
    label "olsztyn"
    origin "text"
  ]
  node [
    id 5
    label "wybudzi&#263;"
    origin "text"
  ]
  node [
    id 6
    label "si&#281;"
    origin "text"
  ]
  node [
    id 7
    label "dziesi&#261;ty"
    origin "text"
  ]
  node [
    id 8
    label "pacjent"
    origin "text"
  ]
  node [
    id 9
    label "szpitalnictwo"
  ]
  node [
    id 10
    label "oddzia&#322;_septyczny"
  ]
  node [
    id 11
    label "klinicysta"
  ]
  node [
    id 12
    label "zabieg&#243;wka"
  ]
  node [
    id 13
    label "izba_chorych"
  ]
  node [
    id 14
    label "&#322;&#243;&#380;eczko_nadziei"
  ]
  node [
    id 15
    label "blok_operacyjny"
  ]
  node [
    id 16
    label "instytucja"
  ]
  node [
    id 17
    label "centrum_urazowe"
  ]
  node [
    id 18
    label "plac&#243;wka_s&#322;u&#380;by_zdrowia"
  ]
  node [
    id 19
    label "kostnica"
  ]
  node [
    id 20
    label "oddzia&#322;"
  ]
  node [
    id 21
    label "sala_chorych"
  ]
  node [
    id 22
    label "ekscytarz"
  ]
  node [
    id 23
    label "zegar"
  ]
  node [
    id 24
    label "obudzi&#263;"
  ]
  node [
    id 25
    label "dzie&#324;"
  ]
  node [
    id 26
    label "od&#322;&#261;czenie"
  ]
  node [
    id 27
    label "cz&#322;owiek"
  ]
  node [
    id 28
    label "przymus_bezpo&#347;redni"
  ]
  node [
    id 29
    label "od&#322;&#261;czy&#263;"
  ]
  node [
    id 30
    label "od&#322;&#261;czanie"
  ]
  node [
    id 31
    label "klient"
  ]
  node [
    id 32
    label "chory"
  ]
  node [
    id 33
    label "szpitalnik"
  ]
  node [
    id 34
    label "od&#322;&#261;cza&#263;"
  ]
  node [
    id 35
    label "przypadek"
  ]
  node [
    id 36
    label "piel&#281;gniarz"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 25
  ]
  edge [
    source 8
    target 26
  ]
  edge [
    source 8
    target 27
  ]
  edge [
    source 8
    target 28
  ]
  edge [
    source 8
    target 29
  ]
  edge [
    source 8
    target 30
  ]
  edge [
    source 8
    target 31
  ]
  edge [
    source 8
    target 32
  ]
  edge [
    source 8
    target 33
  ]
  edge [
    source 8
    target 34
  ]
  edge [
    source 8
    target 35
  ]
  edge [
    source 8
    target 36
  ]
]
