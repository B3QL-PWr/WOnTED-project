graph [
  maxDegree 23
  minDegree 1
  meanDegree 2
  density 0.016129032258064516
  graphCliqueNumber 3
  node [
    id 0
    label "wyrok"
    origin "text"
  ]
  node [
    id 1
    label "zawieszenie"
    origin "text"
  ]
  node [
    id 2
    label "us&#322;ysze&#263;"
    origin "text"
  ]
  node [
    id 3
    label "by&#322;y"
    origin "text"
  ]
  node [
    id 4
    label "policjant"
    origin "text"
  ]
  node [
    id 5
    label "wroc&#322;aw"
    origin "text"
  ]
  node [
    id 6
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 7
    label "zaplecze"
    origin "text"
  ]
  node [
    id 8
    label "jeden"
    origin "text"
  ]
  node [
    id 9
    label "supermarket"
    origin "text"
  ]
  node [
    id 10
    label "przeprowadzi&#263;"
    origin "text"
  ]
  node [
    id 11
    label "brutalny"
    origin "text"
  ]
  node [
    id 12
    label "interwencja"
    origin "text"
  ]
  node [
    id 13
    label "wobec"
    origin "text"
  ]
  node [
    id 14
    label "letni"
    origin "text"
  ]
  node [
    id 15
    label "kobieta"
    origin "text"
  ]
  node [
    id 16
    label "orzeczenie"
  ]
  node [
    id 17
    label "order"
  ]
  node [
    id 18
    label "wydarzenie"
  ]
  node [
    id 19
    label "kara"
  ]
  node [
    id 20
    label "judgment"
  ]
  node [
    id 21
    label "sentencja"
  ]
  node [
    id 22
    label "hang"
  ]
  node [
    id 23
    label "przymocowanie"
  ]
  node [
    id 24
    label "stop"
  ]
  node [
    id 25
    label "odwieszanie"
  ]
  node [
    id 26
    label "reprieve"
  ]
  node [
    id 27
    label "pozawieszanie"
  ]
  node [
    id 28
    label "suspension"
  ]
  node [
    id 29
    label "skomunikowanie"
  ]
  node [
    id 30
    label "obwieszenie"
  ]
  node [
    id 31
    label "powieszenie"
  ]
  node [
    id 32
    label "odwieszenie"
  ]
  node [
    id 33
    label "zako&#324;czenie"
  ]
  node [
    id 34
    label "wstrzymanie"
  ]
  node [
    id 35
    label "oduczenie"
  ]
  node [
    id 36
    label "disavowal"
  ]
  node [
    id 37
    label "resor"
  ]
  node [
    id 38
    label "zabranie"
  ]
  node [
    id 39
    label "podwozie"
  ]
  node [
    id 40
    label "amortyzator"
  ]
  node [
    id 41
    label "umieszczenie"
  ]
  node [
    id 42
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 43
    label "postrzec"
  ]
  node [
    id 44
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 45
    label "dawny"
  ]
  node [
    id 46
    label "rozw&#243;d"
  ]
  node [
    id 47
    label "rozwodzi&#263;_si&#281;"
  ]
  node [
    id 48
    label "eksprezydent"
  ]
  node [
    id 49
    label "rozwie&#347;&#263;_si&#281;"
  ]
  node [
    id 50
    label "partner"
  ]
  node [
    id 51
    label "rozwiedzenie_si&#281;"
  ]
  node [
    id 52
    label "rozwodzenie_si&#281;"
  ]
  node [
    id 53
    label "wcze&#347;niejszy"
  ]
  node [
    id 54
    label "policja"
  ]
  node [
    id 55
    label "blacharz"
  ]
  node [
    id 56
    label "pa&#322;a"
  ]
  node [
    id 57
    label "mundurowy"
  ]
  node [
    id 58
    label "str&#243;&#380;"
  ]
  node [
    id 59
    label "glina"
  ]
  node [
    id 60
    label "wyposa&#380;enie"
  ]
  node [
    id 61
    label "infrastruktura"
  ]
  node [
    id 62
    label "pomieszczenie"
  ]
  node [
    id 63
    label "sklep"
  ]
  node [
    id 64
    label "kieliszek"
  ]
  node [
    id 65
    label "shot"
  ]
  node [
    id 66
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 67
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 68
    label "jaki&#347;"
  ]
  node [
    id 69
    label "jednolicie"
  ]
  node [
    id 70
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 71
    label "w&#243;dka"
  ]
  node [
    id 72
    label "ten"
  ]
  node [
    id 73
    label "ujednolicenie"
  ]
  node [
    id 74
    label "jednakowy"
  ]
  node [
    id 75
    label "market"
  ]
  node [
    id 76
    label "pom&#243;c"
  ]
  node [
    id 77
    label "zbudowa&#263;"
  ]
  node [
    id 78
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 79
    label "leave"
  ]
  node [
    id 80
    label "przewie&#347;&#263;"
  ]
  node [
    id 81
    label "wykona&#263;"
  ]
  node [
    id 82
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 83
    label "draw"
  ]
  node [
    id 84
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 85
    label "carry"
  ]
  node [
    id 86
    label "szczery"
  ]
  node [
    id 87
    label "drastycznie"
  ]
  node [
    id 88
    label "silny"
  ]
  node [
    id 89
    label "bezlitosny"
  ]
  node [
    id 90
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 91
    label "okrutny"
  ]
  node [
    id 92
    label "brutalnie"
  ]
  node [
    id 93
    label "przemoc"
  ]
  node [
    id 94
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 95
    label "mocny"
  ]
  node [
    id 96
    label "niedelikatny"
  ]
  node [
    id 97
    label "interposition"
  ]
  node [
    id 98
    label "akcja"
  ]
  node [
    id 99
    label "ingerencja"
  ]
  node [
    id 100
    label "nijaki"
  ]
  node [
    id 101
    label "sezonowy"
  ]
  node [
    id 102
    label "letnio"
  ]
  node [
    id 103
    label "s&#322;oneczny"
  ]
  node [
    id 104
    label "weso&#322;y"
  ]
  node [
    id 105
    label "oboj&#281;tny"
  ]
  node [
    id 106
    label "latowy"
  ]
  node [
    id 107
    label "ciep&#322;y"
  ]
  node [
    id 108
    label "typowy"
  ]
  node [
    id 109
    label "cz&#322;owiek"
  ]
  node [
    id 110
    label "przekwitanie"
  ]
  node [
    id 111
    label "m&#281;&#380;yna"
  ]
  node [
    id 112
    label "babka"
  ]
  node [
    id 113
    label "samica"
  ]
  node [
    id 114
    label "doros&#322;y"
  ]
  node [
    id 115
    label "ulec"
  ]
  node [
    id 116
    label "uleganie"
  ]
  node [
    id 117
    label "partnerka"
  ]
  node [
    id 118
    label "&#380;ona"
  ]
  node [
    id 119
    label "ulega&#263;"
  ]
  node [
    id 120
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 121
    label "pa&#324;stwo"
  ]
  node [
    id 122
    label "ulegni&#281;cie"
  ]
  node [
    id 123
    label "menopauza"
  ]
  node [
    id 124
    label "&#322;ono"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 86
  ]
  edge [
    source 11
    target 87
  ]
  edge [
    source 11
    target 88
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 11
    target 92
  ]
  edge [
    source 11
    target 93
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 100
  ]
  edge [
    source 14
    target 101
  ]
  edge [
    source 14
    target 102
  ]
  edge [
    source 14
    target 103
  ]
  edge [
    source 14
    target 104
  ]
  edge [
    source 14
    target 105
  ]
  edge [
    source 14
    target 106
  ]
  edge [
    source 14
    target 107
  ]
  edge [
    source 14
    target 108
  ]
  edge [
    source 15
    target 109
  ]
  edge [
    source 15
    target 110
  ]
  edge [
    source 15
    target 111
  ]
  edge [
    source 15
    target 112
  ]
  edge [
    source 15
    target 113
  ]
  edge [
    source 15
    target 114
  ]
  edge [
    source 15
    target 115
  ]
  edge [
    source 15
    target 116
  ]
  edge [
    source 15
    target 117
  ]
  edge [
    source 15
    target 118
  ]
  edge [
    source 15
    target 119
  ]
  edge [
    source 15
    target 120
  ]
  edge [
    source 15
    target 121
  ]
  edge [
    source 15
    target 122
  ]
  edge [
    source 15
    target 123
  ]
  edge [
    source 15
    target 124
  ]
]
