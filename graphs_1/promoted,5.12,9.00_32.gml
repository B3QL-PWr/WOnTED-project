graph [
  maxDegree 32
  minDegree 1
  meanDegree 2.036697247706422
  density 0.018858307849133536
  graphCliqueNumber 4
  node [
    id 0
    label "wyja&#347;ni&#263;"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "dlaczego"
    origin "text"
  ]
  node [
    id 3
    label "tadeusz"
    origin "text"
  ]
  node [
    id 4
    label "rydzyk"
    origin "text"
  ]
  node [
    id 5
    label "tak"
    origin "text"
  ]
  node [
    id 6
    label "ostro"
    origin "text"
  ]
  node [
    id 7
    label "skrytykowa&#263;"
    origin "text"
  ]
  node [
    id 8
    label "dotacja"
    origin "text"
  ]
  node [
    id 9
    label "dla"
    origin "text"
  ]
  node [
    id 10
    label "film"
    origin "text"
  ]
  node [
    id 11
    label "kler"
    origin "text"
  ]
  node [
    id 12
    label "dzia&#322;alno&#347;&#263;"
    origin "text"
  ]
  node [
    id 13
    label "polski"
    origin "text"
  ]
  node [
    id 14
    label "instytut"
    origin "text"
  ]
  node [
    id 15
    label "sztuk"
    origin "text"
  ]
  node [
    id 16
    label "filmowy"
    origin "text"
  ]
  node [
    id 17
    label "explain"
  ]
  node [
    id 18
    label "przedstawi&#263;"
  ]
  node [
    id 19
    label "poja&#347;ni&#263;"
  ]
  node [
    id 20
    label "clear"
  ]
  node [
    id 21
    label "rydz"
  ]
  node [
    id 22
    label "niemile"
  ]
  node [
    id 23
    label "zdecydowanie"
  ]
  node [
    id 24
    label "jednoznacznie"
  ]
  node [
    id 25
    label "raptownie"
  ]
  node [
    id 26
    label "widocznie"
  ]
  node [
    id 27
    label "wyra&#378;nie"
  ]
  node [
    id 28
    label "gryz&#261;co"
  ]
  node [
    id 29
    label "energicznie"
  ]
  node [
    id 30
    label "szybko"
  ]
  node [
    id 31
    label "podniecaj&#261;co"
  ]
  node [
    id 32
    label "intensywnie"
  ]
  node [
    id 33
    label "dziko"
  ]
  node [
    id 34
    label "ostry"
  ]
  node [
    id 35
    label "ci&#281;&#380;ko"
  ]
  node [
    id 36
    label "nieneutralnie"
  ]
  node [
    id 37
    label "oceni&#263;"
  ]
  node [
    id 38
    label "review"
  ]
  node [
    id 39
    label "zaopiniowa&#263;"
  ]
  node [
    id 40
    label "dop&#322;ata"
  ]
  node [
    id 41
    label "rozbieg&#243;wka"
  ]
  node [
    id 42
    label "block"
  ]
  node [
    id 43
    label "blik"
  ]
  node [
    id 44
    label "odczula&#263;"
  ]
  node [
    id 45
    label "rola"
  ]
  node [
    id 46
    label "trawiarnia"
  ]
  node [
    id 47
    label "b&#322;ona"
  ]
  node [
    id 48
    label "filmoteka"
  ]
  node [
    id 49
    label "sztuka"
  ]
  node [
    id 50
    label "muza"
  ]
  node [
    id 51
    label "odczuli&#263;"
  ]
  node [
    id 52
    label "klatka"
  ]
  node [
    id 53
    label "odczulenie"
  ]
  node [
    id 54
    label "emulsja_fotograficzna"
  ]
  node [
    id 55
    label "animatronika"
  ]
  node [
    id 56
    label "dorobek"
  ]
  node [
    id 57
    label "odczulanie"
  ]
  node [
    id 58
    label "scena"
  ]
  node [
    id 59
    label "czo&#322;&#243;wka"
  ]
  node [
    id 60
    label "ty&#322;&#243;wka"
  ]
  node [
    id 61
    label "napisy"
  ]
  node [
    id 62
    label "photograph"
  ]
  node [
    id 63
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 64
    label "postprodukcja"
  ]
  node [
    id 65
    label "sklejarka"
  ]
  node [
    id 66
    label "anamorfoza"
  ]
  node [
    id 67
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 68
    label "ta&#347;ma"
  ]
  node [
    id 69
    label "b&#322;ona_&#347;wiat&#322;oczu&#322;a"
  ]
  node [
    id 70
    label "uj&#281;cie"
  ]
  node [
    id 71
    label "duchowny"
  ]
  node [
    id 72
    label "Ko&#347;ci&#243;&#322;"
  ]
  node [
    id 73
    label "stan"
  ]
  node [
    id 74
    label "dzia&#322;anie"
  ]
  node [
    id 75
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 76
    label "absolutorium"
  ]
  node [
    id 77
    label "activity"
  ]
  node [
    id 78
    label "lacki"
  ]
  node [
    id 79
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 80
    label "przedmiot"
  ]
  node [
    id 81
    label "sztajer"
  ]
  node [
    id 82
    label "drabant"
  ]
  node [
    id 83
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 84
    label "polak"
  ]
  node [
    id 85
    label "pierogi_ruskie"
  ]
  node [
    id 86
    label "krakowiak"
  ]
  node [
    id 87
    label "Polish"
  ]
  node [
    id 88
    label "j&#281;zyk"
  ]
  node [
    id 89
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 90
    label "oberek"
  ]
  node [
    id 91
    label "po_polsku"
  ]
  node [
    id 92
    label "mazur"
  ]
  node [
    id 93
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 94
    label "chodzony"
  ]
  node [
    id 95
    label "skoczny"
  ]
  node [
    id 96
    label "ryba_po_grecku"
  ]
  node [
    id 97
    label "goniony"
  ]
  node [
    id 98
    label "polsko"
  ]
  node [
    id 99
    label "plac&#243;wka"
  ]
  node [
    id 100
    label "institute"
  ]
  node [
    id 101
    label "&#379;ydowski_Instytut_Historyczny"
  ]
  node [
    id 102
    label "Ossolineum"
  ]
  node [
    id 103
    label "instytucja"
  ]
  node [
    id 104
    label "plac&#243;wka_naukowa"
  ]
  node [
    id 105
    label "cinematic"
  ]
  node [
    id 106
    label "filmowo"
  ]
  node [
    id 107
    label "Tadeusz"
  ]
  node [
    id 108
    label "z&#322;oty"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 22
  ]
  edge [
    source 6
    target 23
  ]
  edge [
    source 6
    target 24
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 37
  ]
  edge [
    source 7
    target 38
  ]
  edge [
    source 7
    target 39
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 40
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 41
  ]
  edge [
    source 10
    target 42
  ]
  edge [
    source 10
    target 43
  ]
  edge [
    source 10
    target 44
  ]
  edge [
    source 10
    target 45
  ]
  edge [
    source 10
    target 46
  ]
  edge [
    source 10
    target 47
  ]
  edge [
    source 10
    target 48
  ]
  edge [
    source 10
    target 49
  ]
  edge [
    source 10
    target 50
  ]
  edge [
    source 10
    target 51
  ]
  edge [
    source 10
    target 52
  ]
  edge [
    source 10
    target 53
  ]
  edge [
    source 10
    target 54
  ]
  edge [
    source 10
    target 55
  ]
  edge [
    source 10
    target 56
  ]
  edge [
    source 10
    target 57
  ]
  edge [
    source 10
    target 58
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 10
    target 60
  ]
  edge [
    source 10
    target 61
  ]
  edge [
    source 10
    target 62
  ]
  edge [
    source 10
    target 63
  ]
  edge [
    source 10
    target 64
  ]
  edge [
    source 10
    target 65
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 10
    target 69
  ]
  edge [
    source 10
    target 70
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 71
  ]
  edge [
    source 11
    target 72
  ]
  edge [
    source 11
    target 73
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 74
  ]
  edge [
    source 12
    target 75
  ]
  edge [
    source 12
    target 76
  ]
  edge [
    source 12
    target 77
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 78
  ]
  edge [
    source 13
    target 79
  ]
  edge [
    source 13
    target 80
  ]
  edge [
    source 13
    target 81
  ]
  edge [
    source 13
    target 82
  ]
  edge [
    source 13
    target 83
  ]
  edge [
    source 13
    target 84
  ]
  edge [
    source 13
    target 85
  ]
  edge [
    source 13
    target 86
  ]
  edge [
    source 13
    target 87
  ]
  edge [
    source 13
    target 88
  ]
  edge [
    source 13
    target 89
  ]
  edge [
    source 13
    target 90
  ]
  edge [
    source 13
    target 91
  ]
  edge [
    source 13
    target 92
  ]
  edge [
    source 13
    target 93
  ]
  edge [
    source 13
    target 94
  ]
  edge [
    source 13
    target 95
  ]
  edge [
    source 13
    target 96
  ]
  edge [
    source 13
    target 97
  ]
  edge [
    source 13
    target 98
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 13
    target 16
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 99
  ]
  edge [
    source 14
    target 100
  ]
  edge [
    source 14
    target 101
  ]
  edge [
    source 14
    target 102
  ]
  edge [
    source 14
    target 103
  ]
  edge [
    source 14
    target 104
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 105
  ]
  edge [
    source 16
    target 106
  ]
]
