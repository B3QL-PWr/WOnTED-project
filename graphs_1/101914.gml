graph [
  maxDegree 593
  minDegree 1
  meanDegree 1.9969088098918084
  density 0.0030911901081916537
  graphCliqueNumber 2
  node [
    id 0
    label "inwestycja"
    origin "text"
  ]
  node [
    id 1
    label "relizowane"
    origin "text"
  ]
  node [
    id 2
    label "nadzorowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "przez"
    origin "text"
  ]
  node [
    id 4
    label "wim"
    origin "text"
  ]
  node [
    id 5
    label "uj&#261;&#263;"
    origin "text"
  ]
  node [
    id 6
    label "plan"
    origin "text"
  ]
  node [
    id 7
    label "bud&#380;et"
    origin "text"
  ]
  node [
    id 8
    label "miasto"
    origin "text"
  ]
  node [
    id 9
    label "rocznik"
    origin "text"
  ]
  node [
    id 10
    label "inwestycje"
  ]
  node [
    id 11
    label "wydatki_maj&#261;tkowe"
  ]
  node [
    id 12
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 13
    label "wk&#322;ad"
  ]
  node [
    id 14
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 15
    label "kapita&#322;"
  ]
  node [
    id 16
    label "przedsi&#281;wzi&#281;cie"
  ]
  node [
    id 17
    label "inwestowanie"
  ]
  node [
    id 18
    label "bud&#380;et_domowy"
  ]
  node [
    id 19
    label "sentyment_inwestycyjny"
  ]
  node [
    id 20
    label "rezultat"
  ]
  node [
    id 21
    label "manipulate"
  ]
  node [
    id 22
    label "pracowa&#263;"
  ]
  node [
    id 23
    label "zabra&#263;"
  ]
  node [
    id 24
    label "zakomunikowa&#263;"
  ]
  node [
    id 25
    label "zaaresztowa&#263;"
  ]
  node [
    id 26
    label "wzi&#261;&#263;"
  ]
  node [
    id 27
    label "fascinate"
  ]
  node [
    id 28
    label "reduce"
  ]
  node [
    id 29
    label "suspend"
  ]
  node [
    id 30
    label "testify"
  ]
  node [
    id 31
    label "wzbudzi&#263;"
  ]
  node [
    id 32
    label "zamkn&#261;&#263;"
  ]
  node [
    id 33
    label "device"
  ]
  node [
    id 34
    label "model"
  ]
  node [
    id 35
    label "wytw&#243;r"
  ]
  node [
    id 36
    label "obraz"
  ]
  node [
    id 37
    label "przestrze&#324;"
  ]
  node [
    id 38
    label "dekoracja"
  ]
  node [
    id 39
    label "intencja"
  ]
  node [
    id 40
    label "agreement"
  ]
  node [
    id 41
    label "pomys&#322;"
  ]
  node [
    id 42
    label "punkt"
  ]
  node [
    id 43
    label "miejsce_pracy"
  ]
  node [
    id 44
    label "perspektywa"
  ]
  node [
    id 45
    label "rysunek"
  ]
  node [
    id 46
    label "reprezentacja"
  ]
  node [
    id 47
    label "wydatki_bud&#380;etowe"
  ]
  node [
    id 48
    label "etat"
  ]
  node [
    id 49
    label "portfel"
  ]
  node [
    id 50
    label "rezerwa_rewaluacyjna"
  ]
  node [
    id 51
    label "kwota"
  ]
  node [
    id 52
    label "Brac&#322;aw"
  ]
  node [
    id 53
    label "Zb&#261;szy&#324;"
  ]
  node [
    id 54
    label "G&#322;uch&#243;w"
  ]
  node [
    id 55
    label "Hallstatt"
  ]
  node [
    id 56
    label "Zbara&#380;"
  ]
  node [
    id 57
    label "Wielkie_Tyrnowo"
  ]
  node [
    id 58
    label "Nachiczewan"
  ]
  node [
    id 59
    label "Suworow"
  ]
  node [
    id 60
    label "Halicz"
  ]
  node [
    id 61
    label "Gandawa"
  ]
  node [
    id 62
    label "Kamionka_Strumi&#322;owa"
  ]
  node [
    id 63
    label "Wismar"
  ]
  node [
    id 64
    label "Norymberga"
  ]
  node [
    id 65
    label "Ruciane-Nida"
  ]
  node [
    id 66
    label "Wia&#378;ma"
  ]
  node [
    id 67
    label "Sewilla"
  ]
  node [
    id 68
    label "Pietropaw&#322;owsk"
  ]
  node [
    id 69
    label "Kobry&#324;"
  ]
  node [
    id 70
    label "Brno"
  ]
  node [
    id 71
    label "Tomsk"
  ]
  node [
    id 72
    label "Poniatowa"
  ]
  node [
    id 73
    label "Hadziacz"
  ]
  node [
    id 74
    label "Tiume&#324;"
  ]
  node [
    id 75
    label "Karlsbad"
  ]
  node [
    id 76
    label "Drohobycz"
  ]
  node [
    id 77
    label "Lyon"
  ]
  node [
    id 78
    label "Dzier&#380;y&#324;sk"
  ]
  node [
    id 79
    label "K&#322;odawa"
  ]
  node [
    id 80
    label "Solikamsk"
  ]
  node [
    id 81
    label "Wolgast"
  ]
  node [
    id 82
    label "Saloniki"
  ]
  node [
    id 83
    label "Lw&#243;w"
  ]
  node [
    id 84
    label "Al-Kufa"
  ]
  node [
    id 85
    label "Hamburg"
  ]
  node [
    id 86
    label "Bogusz&#243;w-Gorce"
  ]
  node [
    id 87
    label "Nampula"
  ]
  node [
    id 88
    label "burmistrz"
  ]
  node [
    id 89
    label "D&#252;sseldorf"
  ]
  node [
    id 90
    label "Nowy_Orlean"
  ]
  node [
    id 91
    label "Bamberg"
  ]
  node [
    id 92
    label "Osaka"
  ]
  node [
    id 93
    label "Niko&#322;ajewsk_nad_Amurem"
  ]
  node [
    id 94
    label "Michalovce"
  ]
  node [
    id 95
    label "Fryburg"
  ]
  node [
    id 96
    label "Trabzon"
  ]
  node [
    id 97
    label "Wersal"
  ]
  node [
    id 98
    label "Swatowe"
  ]
  node [
    id 99
    label "Ka&#322;uga"
  ]
  node [
    id 100
    label "Dijon"
  ]
  node [
    id 101
    label "Cannes"
  ]
  node [
    id 102
    label "Borowsk"
  ]
  node [
    id 103
    label "Kursk"
  ]
  node [
    id 104
    label "Tyberiada"
  ]
  node [
    id 105
    label "Boden"
  ]
  node [
    id 106
    label "Dodona"
  ]
  node [
    id 107
    label "Vukovar"
  ]
  node [
    id 108
    label "Soleczniki"
  ]
  node [
    id 109
    label "Barcelona"
  ]
  node [
    id 110
    label "Oszmiana"
  ]
  node [
    id 111
    label "Stuttgart"
  ]
  node [
    id 112
    label "Nerczy&#324;sk"
  ]
  node [
    id 113
    label "Bijsk"
  ]
  node [
    id 114
    label "Essen"
  ]
  node [
    id 115
    label "Luboml"
  ]
  node [
    id 116
    label "Gr&#243;dek"
  ]
  node [
    id 117
    label "Orany"
  ]
  node [
    id 118
    label "Siedliszcze"
  ]
  node [
    id 119
    label "P&#322;owdiw"
  ]
  node [
    id 120
    label "A&#322;apajewsk"
  ]
  node [
    id 121
    label "Liverpool"
  ]
  node [
    id 122
    label "Ostrawa"
  ]
  node [
    id 123
    label "Penza"
  ]
  node [
    id 124
    label "Rudki"
  ]
  node [
    id 125
    label "Aktobe"
  ]
  node [
    id 126
    label "I&#322;awka"
  ]
  node [
    id 127
    label "Tolkmicko"
  ]
  node [
    id 128
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 129
    label "Sajgon"
  ]
  node [
    id 130
    label "Windawa"
  ]
  node [
    id 131
    label "Weimar"
  ]
  node [
    id 132
    label "Jekaterynburg"
  ]
  node [
    id 133
    label "Lejda"
  ]
  node [
    id 134
    label "Cremona"
  ]
  node [
    id 135
    label "Wo&#322;godo&#324;sk"
  ]
  node [
    id 136
    label "Kordoba"
  ]
  node [
    id 137
    label "urz&#261;d"
  ]
  node [
    id 138
    label "&#321;ohojsk"
  ]
  node [
    id 139
    label "Kalmar"
  ]
  node [
    id 140
    label "Akerman"
  ]
  node [
    id 141
    label "Locarno"
  ]
  node [
    id 142
    label "Bych&#243;w"
  ]
  node [
    id 143
    label "Toledo"
  ]
  node [
    id 144
    label "Minusi&#324;sk"
  ]
  node [
    id 145
    label "Szk&#322;&#243;w"
  ]
  node [
    id 146
    label "Wenecja"
  ]
  node [
    id 147
    label "Bazylea"
  ]
  node [
    id 148
    label "Peszt"
  ]
  node [
    id 149
    label "Piza"
  ]
  node [
    id 150
    label "Tanger"
  ]
  node [
    id 151
    label "Krzywi&#324;"
  ]
  node [
    id 152
    label "Eger"
  ]
  node [
    id 153
    label "Bogus&#322;aw"
  ]
  node [
    id 154
    label "Taganrog"
  ]
  node [
    id 155
    label "Oksford"
  ]
  node [
    id 156
    label "Gwardiejsk"
  ]
  node [
    id 157
    label "Tyraspol"
  ]
  node [
    id 158
    label "Kleczew"
  ]
  node [
    id 159
    label "Nowa_D&#281;ba"
  ]
  node [
    id 160
    label "Wilejka"
  ]
  node [
    id 161
    label "Modena"
  ]
  node [
    id 162
    label "Demmin"
  ]
  node [
    id 163
    label "Houston"
  ]
  node [
    id 164
    label "Rydu&#322;towy"
  ]
  node [
    id 165
    label "Bordeaux"
  ]
  node [
    id 166
    label "Schmalkalden"
  ]
  node [
    id 167
    label "O&#322;omuniec"
  ]
  node [
    id 168
    label "Tuluza"
  ]
  node [
    id 169
    label "tramwaj"
  ]
  node [
    id 170
    label "Nantes"
  ]
  node [
    id 171
    label "Debreczyn"
  ]
  node [
    id 172
    label "Kowel"
  ]
  node [
    id 173
    label "Witnica"
  ]
  node [
    id 174
    label "Stalingrad"
  ]
  node [
    id 175
    label "Drezno"
  ]
  node [
    id 176
    label "Perejas&#322;aw"
  ]
  node [
    id 177
    label "Luksor"
  ]
  node [
    id 178
    label "Ostaszk&#243;w"
  ]
  node [
    id 179
    label "Gettysburg"
  ]
  node [
    id 180
    label "Trydent"
  ]
  node [
    id 181
    label "Poczdam"
  ]
  node [
    id 182
    label "Mesyna"
  ]
  node [
    id 183
    label "Krasnogorsk"
  ]
  node [
    id 184
    label "Kars"
  ]
  node [
    id 185
    label "Darmstadt"
  ]
  node [
    id 186
    label "Rzg&#243;w"
  ]
  node [
    id 187
    label "Kar&#322;owice"
  ]
  node [
    id 188
    label "Czeskie_Budziejowice"
  ]
  node [
    id 189
    label "Buda"
  ]
  node [
    id 190
    label "Monako"
  ]
  node [
    id 191
    label "Pardubice"
  ]
  node [
    id 192
    label "Pas&#322;&#281;k"
  ]
  node [
    id 193
    label "Fatima"
  ]
  node [
    id 194
    label "Bir&#380;e"
  ]
  node [
    id 195
    label "Wo&#322;oko&#322;amsk"
  ]
  node [
    id 196
    label "Wi&#322;komierz"
  ]
  node [
    id 197
    label "Opawa"
  ]
  node [
    id 198
    label "Mantua"
  ]
  node [
    id 199
    label "ulica"
  ]
  node [
    id 200
    label "Tarragona"
  ]
  node [
    id 201
    label "Antwerpia"
  ]
  node [
    id 202
    label "Asuan"
  ]
  node [
    id 203
    label "Korynt"
  ]
  node [
    id 204
    label "Armenia"
  ]
  node [
    id 205
    label "Budionnowsk"
  ]
  node [
    id 206
    label "Lengyel"
  ]
  node [
    id 207
    label "Betlejem"
  ]
  node [
    id 208
    label "Asy&#380;"
  ]
  node [
    id 209
    label "Batumi"
  ]
  node [
    id 210
    label "Paczk&#243;w"
  ]
  node [
    id 211
    label "Grenada"
  ]
  node [
    id 212
    label "Suczawa"
  ]
  node [
    id 213
    label "Nowogard"
  ]
  node [
    id 214
    label "Tyr"
  ]
  node [
    id 215
    label "Bria&#324;sk"
  ]
  node [
    id 216
    label "Bar"
  ]
  node [
    id 217
    label "Czerkiesk"
  ]
  node [
    id 218
    label "Ja&#322;ta"
  ]
  node [
    id 219
    label "Mo&#347;ciska"
  ]
  node [
    id 220
    label "Medyna"
  ]
  node [
    id 221
    label "Tartu"
  ]
  node [
    id 222
    label "Pemba"
  ]
  node [
    id 223
    label "Lipawa"
  ]
  node [
    id 224
    label "Tyl&#380;a"
  ]
  node [
    id 225
    label "Lipsk"
  ]
  node [
    id 226
    label "Dayton"
  ]
  node [
    id 227
    label "Rohatyn"
  ]
  node [
    id 228
    label "Peszawar"
  ]
  node [
    id 229
    label "Azow"
  ]
  node [
    id 230
    label "Adrianopol"
  ]
  node [
    id 231
    label "Iwano-Frankowsk"
  ]
  node [
    id 232
    label "Czarnobyl"
  ]
  node [
    id 233
    label "Rakoniewice"
  ]
  node [
    id 234
    label "Obuch&#243;w"
  ]
  node [
    id 235
    label "Orneta"
  ]
  node [
    id 236
    label "Koszyce"
  ]
  node [
    id 237
    label "Czeski_Cieszyn"
  ]
  node [
    id 238
    label "Zagorsk"
  ]
  node [
    id 239
    label "Nieder_Selters"
  ]
  node [
    id 240
    label "Ko&#322;omna"
  ]
  node [
    id 241
    label "Rost&#243;w"
  ]
  node [
    id 242
    label "Bolonia"
  ]
  node [
    id 243
    label "Rajgr&#243;d"
  ]
  node [
    id 244
    label "L&#252;neburg"
  ]
  node [
    id 245
    label "Brack"
  ]
  node [
    id 246
    label "Konstancja"
  ]
  node [
    id 247
    label "Koluszki"
  ]
  node [
    id 248
    label "Semipa&#322;aty&#324;sk"
  ]
  node [
    id 249
    label "Suez"
  ]
  node [
    id 250
    label "Mrocza"
  ]
  node [
    id 251
    label "Triest"
  ]
  node [
    id 252
    label "Murma&#324;sk"
  ]
  node [
    id 253
    label "Tu&#322;a"
  ]
  node [
    id 254
    label "Tarnogr&#243;d"
  ]
  node [
    id 255
    label "Radziech&#243;w"
  ]
  node [
    id 256
    label "Kokand"
  ]
  node [
    id 257
    label "Kircholm"
  ]
  node [
    id 258
    label "Nowa_Ruda"
  ]
  node [
    id 259
    label "Huma&#324;"
  ]
  node [
    id 260
    label "Turkiestan"
  ]
  node [
    id 261
    label "Kani&#243;w"
  ]
  node [
    id 262
    label "Pilzno"
  ]
  node [
    id 263
    label "Dubno"
  ]
  node [
    id 264
    label "Bras&#322;aw"
  ]
  node [
    id 265
    label "Korfant&#243;w"
  ]
  node [
    id 266
    label "Choroszcz"
  ]
  node [
    id 267
    label "Nowogr&#243;d"
  ]
  node [
    id 268
    label "Konotop"
  ]
  node [
    id 269
    label "M&#347;cis&#322;aw"
  ]
  node [
    id 270
    label "Jastarnia"
  ]
  node [
    id 271
    label "Ba&#324;ska_Bystrzyca"
  ]
  node [
    id 272
    label "Omsk"
  ]
  node [
    id 273
    label "Troick"
  ]
  node [
    id 274
    label "Koper"
  ]
  node [
    id 275
    label "Jenisejsk"
  ]
  node [
    id 276
    label "Frankfurt_nad_Odr&#261;"
  ]
  node [
    id 277
    label "Oldenburg_in_Holstein"
  ]
  node [
    id 278
    label "Trenczyn"
  ]
  node [
    id 279
    label "Wormacja"
  ]
  node [
    id 280
    label "Wagram"
  ]
  node [
    id 281
    label "Lubeka"
  ]
  node [
    id 282
    label "Genewa"
  ]
  node [
    id 283
    label "&#379;ydacz&#243;w"
  ]
  node [
    id 284
    label "Kleck"
  ]
  node [
    id 285
    label "G&#243;ra_Kalwaria"
  ]
  node [
    id 286
    label "Struga"
  ]
  node [
    id 287
    label "Izmir"
  ]
  node [
    id 288
    label "Dortmund"
  ]
  node [
    id 289
    label "Izbica_Kujawska"
  ]
  node [
    id 290
    label "Stalinogorsk"
  ]
  node [
    id 291
    label "Workuta"
  ]
  node [
    id 292
    label "Jerycho"
  ]
  node [
    id 293
    label "Brunszwik"
  ]
  node [
    id 294
    label "Aleksandria"
  ]
  node [
    id 295
    label "miejscowo&#347;&#263;"
  ]
  node [
    id 296
    label "Borys&#322;aw"
  ]
  node [
    id 297
    label "Zaleszczyki"
  ]
  node [
    id 298
    label "Z&#322;oczew"
  ]
  node [
    id 299
    label "Piast&#243;w"
  ]
  node [
    id 300
    label "Frankfurt_nad_Menem"
  ]
  node [
    id 301
    label "Bor"
  ]
  node [
    id 302
    label "Nazaret"
  ]
  node [
    id 303
    label "Sarat&#243;w"
  ]
  node [
    id 304
    label "Brasz&#243;w"
  ]
  node [
    id 305
    label "Malin"
  ]
  node [
    id 306
    label "Parma"
  ]
  node [
    id 307
    label "Wierchoja&#324;sk"
  ]
  node [
    id 308
    label "Tarent"
  ]
  node [
    id 309
    label "Mariampol"
  ]
  node [
    id 310
    label "Wuhan"
  ]
  node [
    id 311
    label "Split"
  ]
  node [
    id 312
    label "Baranowicze"
  ]
  node [
    id 313
    label "Marki"
  ]
  node [
    id 314
    label "Adana"
  ]
  node [
    id 315
    label "B&#322;aszki"
  ]
  node [
    id 316
    label "Lubecz"
  ]
  node [
    id 317
    label "Sulech&#243;w"
  ]
  node [
    id 318
    label "Borys&#243;w"
  ]
  node [
    id 319
    label "Homel"
  ]
  node [
    id 320
    label "Tours"
  ]
  node [
    id 321
    label "Kapsztad"
  ]
  node [
    id 322
    label "Edam"
  ]
  node [
    id 323
    label "Zaporo&#380;e"
  ]
  node [
    id 324
    label "Miedwie&#380;jegorsk"
  ]
  node [
    id 325
    label "Kamieniec_Podolski"
  ]
  node [
    id 326
    label "Chocim"
  ]
  node [
    id 327
    label "Mohylew"
  ]
  node [
    id 328
    label "Merseburg"
  ]
  node [
    id 329
    label "Konstantynopol"
  ]
  node [
    id 330
    label "Sambor"
  ]
  node [
    id 331
    label "Manchester"
  ]
  node [
    id 332
    label "Pi&#324;sk"
  ]
  node [
    id 333
    label "Ochryda"
  ]
  node [
    id 334
    label "Rybi&#324;sk"
  ]
  node [
    id 335
    label "Czadca"
  ]
  node [
    id 336
    label "Orenburg"
  ]
  node [
    id 337
    label "Krajowa"
  ]
  node [
    id 338
    label "Eleusis"
  ]
  node [
    id 339
    label "Awinion"
  ]
  node [
    id 340
    label "Rzeczyca"
  ]
  node [
    id 341
    label "Barczewo"
  ]
  node [
    id 342
    label "Lozanna"
  ]
  node [
    id 343
    label "&#379;migr&#243;d"
  ]
  node [
    id 344
    label "Chabarowsk"
  ]
  node [
    id 345
    label "Jena"
  ]
  node [
    id 346
    label "Xai-Xai"
  ]
  node [
    id 347
    label "Radk&#243;w"
  ]
  node [
    id 348
    label "Syrakuzy"
  ]
  node [
    id 349
    label "Zas&#322;aw"
  ]
  node [
    id 350
    label "Getynga"
  ]
  node [
    id 351
    label "Windsor"
  ]
  node [
    id 352
    label "Carrara"
  ]
  node [
    id 353
    label "Madras"
  ]
  node [
    id 354
    label "Nitra"
  ]
  node [
    id 355
    label "Kilonia"
  ]
  node [
    id 356
    label "Rawenna"
  ]
  node [
    id 357
    label "Stawropol"
  ]
  node [
    id 358
    label "Warna"
  ]
  node [
    id 359
    label "Ba&#322;tijsk"
  ]
  node [
    id 360
    label "Cumana"
  ]
  node [
    id 361
    label "Kostroma"
  ]
  node [
    id 362
    label "Bajonna"
  ]
  node [
    id 363
    label "Magadan"
  ]
  node [
    id 364
    label "Kercz"
  ]
  node [
    id 365
    label "Harbin"
  ]
  node [
    id 366
    label "Sankt_Florian"
  ]
  node [
    id 367
    label "Norak"
  ]
  node [
    id 368
    label "Wo&#322;kowysk"
  ]
  node [
    id 369
    label "Bia&#322;ogr&#243;d_nad_Dniestrem"
  ]
  node [
    id 370
    label "S&#232;vres"
  ]
  node [
    id 371
    label "Barwice"
  ]
  node [
    id 372
    label "Jutrosin"
  ]
  node [
    id 373
    label "Sumy"
  ]
  node [
    id 374
    label "Canterbury"
  ]
  node [
    id 375
    label "Czerkasy"
  ]
  node [
    id 376
    label "Troki"
  ]
  node [
    id 377
    label "Dnieprodzier&#380;y&#324;sk"
  ]
  node [
    id 378
    label "Turka"
  ]
  node [
    id 379
    label "Budziszyn"
  ]
  node [
    id 380
    label "A&#322;czewsk"
  ]
  node [
    id 381
    label "Chark&#243;w"
  ]
  node [
    id 382
    label "Go&#347;cino"
  ]
  node [
    id 383
    label "Ku&#378;nieck"
  ]
  node [
    id 384
    label "Wotki&#324;sk"
  ]
  node [
    id 385
    label "Symferopol"
  ]
  node [
    id 386
    label "Dmitrow"
  ]
  node [
    id 387
    label "Cherso&#324;"
  ]
  node [
    id 388
    label "zabudowa"
  ]
  node [
    id 389
    label "Nowogr&#243;dek"
  ]
  node [
    id 390
    label "Orlean"
  ]
  node [
    id 391
    label "Ku&#378;nia_Raciborska"
  ]
  node [
    id 392
    label "Berdia&#324;sk"
  ]
  node [
    id 393
    label "Szumsk"
  ]
  node [
    id 394
    label "Z&#322;ocz&#243;w"
  ]
  node [
    id 395
    label "Orsza"
  ]
  node [
    id 396
    label "Cluny"
  ]
  node [
    id 397
    label "Aralsk"
  ]
  node [
    id 398
    label "Stoczek_&#321;ukowski"
  ]
  node [
    id 399
    label "Bogumin"
  ]
  node [
    id 400
    label "Antiochia"
  ]
  node [
    id 401
    label "grupa"
  ]
  node [
    id 402
    label "Inhambane"
  ]
  node [
    id 403
    label "S&#322;awia&#324;sk_nad_Kubaniem"
  ]
  node [
    id 404
    label "Wielki_Pres&#322;aw"
  ]
  node [
    id 405
    label "Trewir"
  ]
  node [
    id 406
    label "Nowogr&#243;d_Wielki"
  ]
  node [
    id 407
    label "Siewieromorsk"
  ]
  node [
    id 408
    label "Calais"
  ]
  node [
    id 409
    label "&#379;ytawa"
  ]
  node [
    id 410
    label "Eupatoria"
  ]
  node [
    id 411
    label "Twer"
  ]
  node [
    id 412
    label "Stara_Zagora"
  ]
  node [
    id 413
    label "Jastrowie"
  ]
  node [
    id 414
    label "Piatigorsk"
  ]
  node [
    id 415
    label "&#379;&#243;&#322;kiew"
  ]
  node [
    id 416
    label "Le&#324;sk"
  ]
  node [
    id 417
    label "Johannesburg"
  ]
  node [
    id 418
    label "Kaszyn"
  ]
  node [
    id 419
    label "&#346;wi&#281;ciany"
  ]
  node [
    id 420
    label "&#379;ylina"
  ]
  node [
    id 421
    label "Sewastopol"
  ]
  node [
    id 422
    label "Pietrozawodsk"
  ]
  node [
    id 423
    label "Bobolice"
  ]
  node [
    id 424
    label "Mosty"
  ]
  node [
    id 425
    label "Korsu&#324;_Szewczenkowski"
  ]
  node [
    id 426
    label "Karaganda"
  ]
  node [
    id 427
    label "Marsylia"
  ]
  node [
    id 428
    label "Buchara"
  ]
  node [
    id 429
    label "Dubrownik"
  ]
  node [
    id 430
    label "Be&#322;z"
  ]
  node [
    id 431
    label "Oran"
  ]
  node [
    id 432
    label "Regensburg"
  ]
  node [
    id 433
    label "Rotterdam"
  ]
  node [
    id 434
    label "Trembowla"
  ]
  node [
    id 435
    label "Woskriesiensk"
  ]
  node [
    id 436
    label "Po&#322;ock"
  ]
  node [
    id 437
    label "Poprad"
  ]
  node [
    id 438
    label "Los_Angeles"
  ]
  node [
    id 439
    label "Kronsztad"
  ]
  node [
    id 440
    label "U&#322;an_Ude"
  ]
  node [
    id 441
    label "U&#347;cie_nad_&#321;ab&#261;"
  ]
  node [
    id 442
    label "W&#322;adywostok"
  ]
  node [
    id 443
    label "Kandahar"
  ]
  node [
    id 444
    label "Tobolsk"
  ]
  node [
    id 445
    label "Boston"
  ]
  node [
    id 446
    label "Hawana"
  ]
  node [
    id 447
    label "Kis&#322;owodzk"
  ]
  node [
    id 448
    label "Tulon"
  ]
  node [
    id 449
    label "Utrecht"
  ]
  node [
    id 450
    label "Oleszyce"
  ]
  node [
    id 451
    label "Hradec_Kr&#225;lov&#233;"
  ]
  node [
    id 452
    label "Katania"
  ]
  node [
    id 453
    label "Teby"
  ]
  node [
    id 454
    label "Paw&#322;owo"
  ]
  node [
    id 455
    label "W&#252;rzburg"
  ]
  node [
    id 456
    label "Podiebrady"
  ]
  node [
    id 457
    label "Uppsala"
  ]
  node [
    id 458
    label "Poniewie&#380;"
  ]
  node [
    id 459
    label "Berezyna"
  ]
  node [
    id 460
    label "Aczy&#324;sk"
  ]
  node [
    id 461
    label "Niko&#322;ajewsk"
  ]
  node [
    id 462
    label "Ostr&#243;g"
  ]
  node [
    id 463
    label "Brze&#347;&#263;"
  ]
  node [
    id 464
    label "Stryj"
  ]
  node [
    id 465
    label "Lancaster"
  ]
  node [
    id 466
    label "Kozielsk"
  ]
  node [
    id 467
    label "Loreto"
  ]
  node [
    id 468
    label "B&#322;agowieszcze&#324;sk"
  ]
  node [
    id 469
    label "Hebron"
  ]
  node [
    id 470
    label "Kaspijsk"
  ]
  node [
    id 471
    label "Peczora"
  ]
  node [
    id 472
    label "Isfahan"
  ]
  node [
    id 473
    label "Chimoio"
  ]
  node [
    id 474
    label "Mory&#324;"
  ]
  node [
    id 475
    label "Kowno"
  ]
  node [
    id 476
    label "Nie&#347;wie&#380;"
  ]
  node [
    id 477
    label "Opalenica"
  ]
  node [
    id 478
    label "Kolonia"
  ]
  node [
    id 479
    label "Stary_Sambor"
  ]
  node [
    id 480
    label "Kolkata"
  ]
  node [
    id 481
    label "Turkmenbaszy"
  ]
  node [
    id 482
    label "Mohyl&#243;w_Podolski"
  ]
  node [
    id 483
    label "Nankin"
  ]
  node [
    id 484
    label "Krzanowice"
  ]
  node [
    id 485
    label "Efez"
  ]
  node [
    id 486
    label "Dobrodzie&#324;"
  ]
  node [
    id 487
    label "Neapol"
  ]
  node [
    id 488
    label "S&#322;uck"
  ]
  node [
    id 489
    label "Fryburg_Bryzgowijski"
  ]
  node [
    id 490
    label "W&#322;odzimierz_Wo&#322;y&#324;ski"
  ]
  node [
    id 491
    label "Frydek-Mistek"
  ]
  node [
    id 492
    label "Korsze"
  ]
  node [
    id 493
    label "T&#322;uszcz"
  ]
  node [
    id 494
    label "Soligorsk"
  ]
  node [
    id 495
    label "Kie&#380;mark"
  ]
  node [
    id 496
    label "Mannheim"
  ]
  node [
    id 497
    label "Ulm"
  ]
  node [
    id 498
    label "Podhajce"
  ]
  node [
    id 499
    label "Dniepropetrowsk"
  ]
  node [
    id 500
    label "Szamocin"
  ]
  node [
    id 501
    label "Ko&#322;omyja"
  ]
  node [
    id 502
    label "Buczacz"
  ]
  node [
    id 503
    label "M&#252;nster"
  ]
  node [
    id 504
    label "Brema"
  ]
  node [
    id 505
    label "Delhi"
  ]
  node [
    id 506
    label "Nicea"
  ]
  node [
    id 507
    label "&#346;niatyn"
  ]
  node [
    id 508
    label "Szawle"
  ]
  node [
    id 509
    label "Czerniowce"
  ]
  node [
    id 510
    label "Mi&#347;nia"
  ]
  node [
    id 511
    label "Sydney"
  ]
  node [
    id 512
    label "Moguncja"
  ]
  node [
    id 513
    label "Narbona"
  ]
  node [
    id 514
    label "Swiet&#322;ogorsk"
  ]
  node [
    id 515
    label "Wittenberga"
  ]
  node [
    id 516
    label "Uljanowsk"
  ]
  node [
    id 517
    label "Wyborg"
  ]
  node [
    id 518
    label "&#321;uga&#324;sk"
  ]
  node [
    id 519
    label "Trojan"
  ]
  node [
    id 520
    label "Kalwaria_Zebrzydowska"
  ]
  node [
    id 521
    label "Brandenburg"
  ]
  node [
    id 522
    label "Kemerowo"
  ]
  node [
    id 523
    label "Kaszgar"
  ]
  node [
    id 524
    label "Lenzen"
  ]
  node [
    id 525
    label "Nanning"
  ]
  node [
    id 526
    label "Gotha"
  ]
  node [
    id 527
    label "Zurych"
  ]
  node [
    id 528
    label "Baltimore"
  ]
  node [
    id 529
    label "&#321;uck"
  ]
  node [
    id 530
    label "Bristol"
  ]
  node [
    id 531
    label "Ferrara"
  ]
  node [
    id 532
    label "Mariupol"
  ]
  node [
    id 533
    label "przestrze&#324;_publiczna"
  ]
  node [
    id 534
    label "Filadelfia"
  ]
  node [
    id 535
    label "Czerniejewo"
  ]
  node [
    id 536
    label "Milan&#243;wek"
  ]
  node [
    id 537
    label "Lhasa"
  ]
  node [
    id 538
    label "Kanton"
  ]
  node [
    id 539
    label "Perwomajsk"
  ]
  node [
    id 540
    label "Nieftiegorsk"
  ]
  node [
    id 541
    label "Greifswald"
  ]
  node [
    id 542
    label "Pittsburgh"
  ]
  node [
    id 543
    label "Akwileja"
  ]
  node [
    id 544
    label "Norfolk"
  ]
  node [
    id 545
    label "Perm"
  ]
  node [
    id 546
    label "Fergana"
  ]
  node [
    id 547
    label "Detroit"
  ]
  node [
    id 548
    label "Starobielsk"
  ]
  node [
    id 549
    label "Wielsk"
  ]
  node [
    id 550
    label "Zaklik&#243;w"
  ]
  node [
    id 551
    label "Majsur"
  ]
  node [
    id 552
    label "Narwa"
  ]
  node [
    id 553
    label "Chicago"
  ]
  node [
    id 554
    label "Byczyna"
  ]
  node [
    id 555
    label "Mozyrz"
  ]
  node [
    id 556
    label "Konstantyn&#243;wka"
  ]
  node [
    id 557
    label "Miko&#322;aj&#243;w"
  ]
  node [
    id 558
    label "Megara"
  ]
  node [
    id 559
    label "Stralsund"
  ]
  node [
    id 560
    label "Wo&#322;gograd"
  ]
  node [
    id 561
    label "Lichinga"
  ]
  node [
    id 562
    label "Haga"
  ]
  node [
    id 563
    label "Tarnopol"
  ]
  node [
    id 564
    label "Nowomoskowsk"
  ]
  node [
    id 565
    label "K&#322;ajpeda"
  ]
  node [
    id 566
    label "Ussuryjsk"
  ]
  node [
    id 567
    label "Brugia"
  ]
  node [
    id 568
    label "Natal"
  ]
  node [
    id 569
    label "Kro&#347;niewice"
  ]
  node [
    id 570
    label "Edynburg"
  ]
  node [
    id 571
    label "Marburg"
  ]
  node [
    id 572
    label "Dalton"
  ]
  node [
    id 573
    label "S&#322;onim"
  ]
  node [
    id 574
    label "&#346;wiebodzice"
  ]
  node [
    id 575
    label "Smorgonie"
  ]
  node [
    id 576
    label "Orze&#322;"
  ]
  node [
    id 577
    label "Nowoku&#378;nieck"
  ]
  node [
    id 578
    label "Zadar"
  ]
  node [
    id 579
    label "Koprzywnica"
  ]
  node [
    id 580
    label "Angarsk"
  ]
  node [
    id 581
    label "Tel_Awiw-Jafa"
  ]
  node [
    id 582
    label "Mo&#380;ajsk"
  ]
  node [
    id 583
    label "Norylsk"
  ]
  node [
    id 584
    label "Akwizgran"
  ]
  node [
    id 585
    label "Jawor&#243;w"
  ]
  node [
    id 586
    label "weduta"
  ]
  node [
    id 587
    label "Jab&#322;onk&#243;w"
  ]
  node [
    id 588
    label "Suzdal"
  ]
  node [
    id 589
    label "W&#322;odzimierz"
  ]
  node [
    id 590
    label "Bujnaksk"
  ]
  node [
    id 591
    label "Beresteczko"
  ]
  node [
    id 592
    label "Strzelno"
  ]
  node [
    id 593
    label "Siewsk"
  ]
  node [
    id 594
    label "Cymlansk"
  ]
  node [
    id 595
    label "Trzyniec"
  ]
  node [
    id 596
    label "Hanower"
  ]
  node [
    id 597
    label "Wuppertal"
  ]
  node [
    id 598
    label "Sura&#380;"
  ]
  node [
    id 599
    label "Samara"
  ]
  node [
    id 600
    label "Winchester"
  ]
  node [
    id 601
    label "Krasnodar"
  ]
  node [
    id 602
    label "Sydon"
  ]
  node [
    id 603
    label "Worone&#380;"
  ]
  node [
    id 604
    label "Paw&#322;odar"
  ]
  node [
    id 605
    label "Czelabi&#324;sk"
  ]
  node [
    id 606
    label "Reda"
  ]
  node [
    id 607
    label "Karwina"
  ]
  node [
    id 608
    label "Wyszehrad"
  ]
  node [
    id 609
    label "Sara&#324;sk"
  ]
  node [
    id 610
    label "Koby&#322;ka"
  ]
  node [
    id 611
    label "Tambow"
  ]
  node [
    id 612
    label "Pyskowice"
  ]
  node [
    id 613
    label "Winnica"
  ]
  node [
    id 614
    label "Heidelberg"
  ]
  node [
    id 615
    label "Maribor"
  ]
  node [
    id 616
    label "Werona"
  ]
  node [
    id 617
    label "G&#322;uszyca"
  ]
  node [
    id 618
    label "Rostock"
  ]
  node [
    id 619
    label "Mekka"
  ]
  node [
    id 620
    label "Liberec"
  ]
  node [
    id 621
    label "Bie&#322;gorod"
  ]
  node [
    id 622
    label "Berdycz&#243;w"
  ]
  node [
    id 623
    label "Sierdobsk"
  ]
  node [
    id 624
    label "Bobrujsk"
  ]
  node [
    id 625
    label "Padwa"
  ]
  node [
    id 626
    label "Chanty-Mansyjsk"
  ]
  node [
    id 627
    label "Pasawa"
  ]
  node [
    id 628
    label "Poczaj&#243;w"
  ]
  node [
    id 629
    label "&#379;ar&#243;w"
  ]
  node [
    id 630
    label "Barabi&#324;sk"
  ]
  node [
    id 631
    label "Gorycja"
  ]
  node [
    id 632
    label "Haarlem"
  ]
  node [
    id 633
    label "Kiejdany"
  ]
  node [
    id 634
    label "Chmielnicki"
  ]
  node [
    id 635
    label "Siena"
  ]
  node [
    id 636
    label "Burgas"
  ]
  node [
    id 637
    label "Magnitogorsk"
  ]
  node [
    id 638
    label "Korzec"
  ]
  node [
    id 639
    label "Bonn"
  ]
  node [
    id 640
    label "Pr&#243;szk&#243;w"
  ]
  node [
    id 641
    label "Walencja"
  ]
  node [
    id 642
    label "Mosina"
  ]
  node [
    id 643
    label "formacja"
  ]
  node [
    id 644
    label "kronika"
  ]
  node [
    id 645
    label "czasopismo"
  ]
  node [
    id 646
    label "yearbook"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 8
    target 122
  ]
  edge [
    source 8
    target 123
  ]
  edge [
    source 8
    target 124
  ]
  edge [
    source 8
    target 125
  ]
  edge [
    source 8
    target 126
  ]
  edge [
    source 8
    target 127
  ]
  edge [
    source 8
    target 128
  ]
  edge [
    source 8
    target 129
  ]
  edge [
    source 8
    target 130
  ]
  edge [
    source 8
    target 131
  ]
  edge [
    source 8
    target 132
  ]
  edge [
    source 8
    target 133
  ]
  edge [
    source 8
    target 134
  ]
  edge [
    source 8
    target 135
  ]
  edge [
    source 8
    target 136
  ]
  edge [
    source 8
    target 137
  ]
  edge [
    source 8
    target 138
  ]
  edge [
    source 8
    target 139
  ]
  edge [
    source 8
    target 140
  ]
  edge [
    source 8
    target 141
  ]
  edge [
    source 8
    target 142
  ]
  edge [
    source 8
    target 143
  ]
  edge [
    source 8
    target 144
  ]
  edge [
    source 8
    target 145
  ]
  edge [
    source 8
    target 146
  ]
  edge [
    source 8
    target 147
  ]
  edge [
    source 8
    target 148
  ]
  edge [
    source 8
    target 149
  ]
  edge [
    source 8
    target 150
  ]
  edge [
    source 8
    target 151
  ]
  edge [
    source 8
    target 152
  ]
  edge [
    source 8
    target 153
  ]
  edge [
    source 8
    target 154
  ]
  edge [
    source 8
    target 155
  ]
  edge [
    source 8
    target 156
  ]
  edge [
    source 8
    target 157
  ]
  edge [
    source 8
    target 158
  ]
  edge [
    source 8
    target 159
  ]
  edge [
    source 8
    target 160
  ]
  edge [
    source 8
    target 161
  ]
  edge [
    source 8
    target 162
  ]
  edge [
    source 8
    target 163
  ]
  edge [
    source 8
    target 164
  ]
  edge [
    source 8
    target 165
  ]
  edge [
    source 8
    target 166
  ]
  edge [
    source 8
    target 167
  ]
  edge [
    source 8
    target 168
  ]
  edge [
    source 8
    target 169
  ]
  edge [
    source 8
    target 170
  ]
  edge [
    source 8
    target 171
  ]
  edge [
    source 8
    target 172
  ]
  edge [
    source 8
    target 173
  ]
  edge [
    source 8
    target 174
  ]
  edge [
    source 8
    target 175
  ]
  edge [
    source 8
    target 176
  ]
  edge [
    source 8
    target 177
  ]
  edge [
    source 8
    target 178
  ]
  edge [
    source 8
    target 179
  ]
  edge [
    source 8
    target 180
  ]
  edge [
    source 8
    target 181
  ]
  edge [
    source 8
    target 182
  ]
  edge [
    source 8
    target 183
  ]
  edge [
    source 8
    target 184
  ]
  edge [
    source 8
    target 185
  ]
  edge [
    source 8
    target 186
  ]
  edge [
    source 8
    target 187
  ]
  edge [
    source 8
    target 188
  ]
  edge [
    source 8
    target 189
  ]
  edge [
    source 8
    target 190
  ]
  edge [
    source 8
    target 191
  ]
  edge [
    source 8
    target 192
  ]
  edge [
    source 8
    target 193
  ]
  edge [
    source 8
    target 194
  ]
  edge [
    source 8
    target 195
  ]
  edge [
    source 8
    target 196
  ]
  edge [
    source 8
    target 197
  ]
  edge [
    source 8
    target 198
  ]
  edge [
    source 8
    target 199
  ]
  edge [
    source 8
    target 200
  ]
  edge [
    source 8
    target 201
  ]
  edge [
    source 8
    target 202
  ]
  edge [
    source 8
    target 203
  ]
  edge [
    source 8
    target 204
  ]
  edge [
    source 8
    target 205
  ]
  edge [
    source 8
    target 206
  ]
  edge [
    source 8
    target 207
  ]
  edge [
    source 8
    target 208
  ]
  edge [
    source 8
    target 209
  ]
  edge [
    source 8
    target 210
  ]
  edge [
    source 8
    target 211
  ]
  edge [
    source 8
    target 212
  ]
  edge [
    source 8
    target 213
  ]
  edge [
    source 8
    target 214
  ]
  edge [
    source 8
    target 215
  ]
  edge [
    source 8
    target 216
  ]
  edge [
    source 8
    target 217
  ]
  edge [
    source 8
    target 218
  ]
  edge [
    source 8
    target 219
  ]
  edge [
    source 8
    target 220
  ]
  edge [
    source 8
    target 221
  ]
  edge [
    source 8
    target 222
  ]
  edge [
    source 8
    target 223
  ]
  edge [
    source 8
    target 224
  ]
  edge [
    source 8
    target 225
  ]
  edge [
    source 8
    target 226
  ]
  edge [
    source 8
    target 227
  ]
  edge [
    source 8
    target 228
  ]
  edge [
    source 8
    target 229
  ]
  edge [
    source 8
    target 230
  ]
  edge [
    source 8
    target 231
  ]
  edge [
    source 8
    target 232
  ]
  edge [
    source 8
    target 233
  ]
  edge [
    source 8
    target 234
  ]
  edge [
    source 8
    target 235
  ]
  edge [
    source 8
    target 236
  ]
  edge [
    source 8
    target 237
  ]
  edge [
    source 8
    target 238
  ]
  edge [
    source 8
    target 239
  ]
  edge [
    source 8
    target 240
  ]
  edge [
    source 8
    target 241
  ]
  edge [
    source 8
    target 242
  ]
  edge [
    source 8
    target 243
  ]
  edge [
    source 8
    target 244
  ]
  edge [
    source 8
    target 245
  ]
  edge [
    source 8
    target 246
  ]
  edge [
    source 8
    target 247
  ]
  edge [
    source 8
    target 248
  ]
  edge [
    source 8
    target 249
  ]
  edge [
    source 8
    target 250
  ]
  edge [
    source 8
    target 251
  ]
  edge [
    source 8
    target 252
  ]
  edge [
    source 8
    target 253
  ]
  edge [
    source 8
    target 254
  ]
  edge [
    source 8
    target 255
  ]
  edge [
    source 8
    target 256
  ]
  edge [
    source 8
    target 257
  ]
  edge [
    source 8
    target 258
  ]
  edge [
    source 8
    target 259
  ]
  edge [
    source 8
    target 260
  ]
  edge [
    source 8
    target 261
  ]
  edge [
    source 8
    target 262
  ]
  edge [
    source 8
    target 263
  ]
  edge [
    source 8
    target 264
  ]
  edge [
    source 8
    target 265
  ]
  edge [
    source 8
    target 266
  ]
  edge [
    source 8
    target 267
  ]
  edge [
    source 8
    target 268
  ]
  edge [
    source 8
    target 269
  ]
  edge [
    source 8
    target 270
  ]
  edge [
    source 8
    target 271
  ]
  edge [
    source 8
    target 272
  ]
  edge [
    source 8
    target 273
  ]
  edge [
    source 8
    target 274
  ]
  edge [
    source 8
    target 275
  ]
  edge [
    source 8
    target 276
  ]
  edge [
    source 8
    target 277
  ]
  edge [
    source 8
    target 278
  ]
  edge [
    source 8
    target 279
  ]
  edge [
    source 8
    target 280
  ]
  edge [
    source 8
    target 281
  ]
  edge [
    source 8
    target 282
  ]
  edge [
    source 8
    target 283
  ]
  edge [
    source 8
    target 284
  ]
  edge [
    source 8
    target 285
  ]
  edge [
    source 8
    target 286
  ]
  edge [
    source 8
    target 287
  ]
  edge [
    source 8
    target 288
  ]
  edge [
    source 8
    target 289
  ]
  edge [
    source 8
    target 290
  ]
  edge [
    source 8
    target 291
  ]
  edge [
    source 8
    target 292
  ]
  edge [
    source 8
    target 293
  ]
  edge [
    source 8
    target 294
  ]
  edge [
    source 8
    target 295
  ]
  edge [
    source 8
    target 296
  ]
  edge [
    source 8
    target 297
  ]
  edge [
    source 8
    target 298
  ]
  edge [
    source 8
    target 299
  ]
  edge [
    source 8
    target 300
  ]
  edge [
    source 8
    target 301
  ]
  edge [
    source 8
    target 302
  ]
  edge [
    source 8
    target 303
  ]
  edge [
    source 8
    target 304
  ]
  edge [
    source 8
    target 305
  ]
  edge [
    source 8
    target 306
  ]
  edge [
    source 8
    target 307
  ]
  edge [
    source 8
    target 308
  ]
  edge [
    source 8
    target 309
  ]
  edge [
    source 8
    target 310
  ]
  edge [
    source 8
    target 311
  ]
  edge [
    source 8
    target 312
  ]
  edge [
    source 8
    target 313
  ]
  edge [
    source 8
    target 314
  ]
  edge [
    source 8
    target 315
  ]
  edge [
    source 8
    target 316
  ]
  edge [
    source 8
    target 317
  ]
  edge [
    source 8
    target 318
  ]
  edge [
    source 8
    target 319
  ]
  edge [
    source 8
    target 320
  ]
  edge [
    source 8
    target 321
  ]
  edge [
    source 8
    target 322
  ]
  edge [
    source 8
    target 323
  ]
  edge [
    source 8
    target 324
  ]
  edge [
    source 8
    target 325
  ]
  edge [
    source 8
    target 326
  ]
  edge [
    source 8
    target 327
  ]
  edge [
    source 8
    target 328
  ]
  edge [
    source 8
    target 329
  ]
  edge [
    source 8
    target 330
  ]
  edge [
    source 8
    target 331
  ]
  edge [
    source 8
    target 332
  ]
  edge [
    source 8
    target 333
  ]
  edge [
    source 8
    target 334
  ]
  edge [
    source 8
    target 335
  ]
  edge [
    source 8
    target 336
  ]
  edge [
    source 8
    target 337
  ]
  edge [
    source 8
    target 338
  ]
  edge [
    source 8
    target 339
  ]
  edge [
    source 8
    target 340
  ]
  edge [
    source 8
    target 341
  ]
  edge [
    source 8
    target 342
  ]
  edge [
    source 8
    target 343
  ]
  edge [
    source 8
    target 344
  ]
  edge [
    source 8
    target 345
  ]
  edge [
    source 8
    target 346
  ]
  edge [
    source 8
    target 347
  ]
  edge [
    source 8
    target 348
  ]
  edge [
    source 8
    target 349
  ]
  edge [
    source 8
    target 350
  ]
  edge [
    source 8
    target 351
  ]
  edge [
    source 8
    target 352
  ]
  edge [
    source 8
    target 353
  ]
  edge [
    source 8
    target 354
  ]
  edge [
    source 8
    target 355
  ]
  edge [
    source 8
    target 356
  ]
  edge [
    source 8
    target 357
  ]
  edge [
    source 8
    target 358
  ]
  edge [
    source 8
    target 359
  ]
  edge [
    source 8
    target 360
  ]
  edge [
    source 8
    target 361
  ]
  edge [
    source 8
    target 362
  ]
  edge [
    source 8
    target 363
  ]
  edge [
    source 8
    target 364
  ]
  edge [
    source 8
    target 365
  ]
  edge [
    source 8
    target 366
  ]
  edge [
    source 8
    target 367
  ]
  edge [
    source 8
    target 368
  ]
  edge [
    source 8
    target 369
  ]
  edge [
    source 8
    target 370
  ]
  edge [
    source 8
    target 371
  ]
  edge [
    source 8
    target 372
  ]
  edge [
    source 8
    target 373
  ]
  edge [
    source 8
    target 374
  ]
  edge [
    source 8
    target 375
  ]
  edge [
    source 8
    target 376
  ]
  edge [
    source 8
    target 377
  ]
  edge [
    source 8
    target 378
  ]
  edge [
    source 8
    target 379
  ]
  edge [
    source 8
    target 380
  ]
  edge [
    source 8
    target 381
  ]
  edge [
    source 8
    target 382
  ]
  edge [
    source 8
    target 383
  ]
  edge [
    source 8
    target 384
  ]
  edge [
    source 8
    target 385
  ]
  edge [
    source 8
    target 386
  ]
  edge [
    source 8
    target 387
  ]
  edge [
    source 8
    target 388
  ]
  edge [
    source 8
    target 389
  ]
  edge [
    source 8
    target 390
  ]
  edge [
    source 8
    target 391
  ]
  edge [
    source 8
    target 392
  ]
  edge [
    source 8
    target 393
  ]
  edge [
    source 8
    target 394
  ]
  edge [
    source 8
    target 395
  ]
  edge [
    source 8
    target 396
  ]
  edge [
    source 8
    target 397
  ]
  edge [
    source 8
    target 398
  ]
  edge [
    source 8
    target 399
  ]
  edge [
    source 8
    target 400
  ]
  edge [
    source 8
    target 401
  ]
  edge [
    source 8
    target 402
  ]
  edge [
    source 8
    target 403
  ]
  edge [
    source 8
    target 404
  ]
  edge [
    source 8
    target 405
  ]
  edge [
    source 8
    target 406
  ]
  edge [
    source 8
    target 407
  ]
  edge [
    source 8
    target 408
  ]
  edge [
    source 8
    target 409
  ]
  edge [
    source 8
    target 410
  ]
  edge [
    source 8
    target 411
  ]
  edge [
    source 8
    target 412
  ]
  edge [
    source 8
    target 413
  ]
  edge [
    source 8
    target 414
  ]
  edge [
    source 8
    target 415
  ]
  edge [
    source 8
    target 416
  ]
  edge [
    source 8
    target 417
  ]
  edge [
    source 8
    target 418
  ]
  edge [
    source 8
    target 419
  ]
  edge [
    source 8
    target 420
  ]
  edge [
    source 8
    target 421
  ]
  edge [
    source 8
    target 422
  ]
  edge [
    source 8
    target 423
  ]
  edge [
    source 8
    target 424
  ]
  edge [
    source 8
    target 425
  ]
  edge [
    source 8
    target 426
  ]
  edge [
    source 8
    target 427
  ]
  edge [
    source 8
    target 428
  ]
  edge [
    source 8
    target 429
  ]
  edge [
    source 8
    target 430
  ]
  edge [
    source 8
    target 431
  ]
  edge [
    source 8
    target 432
  ]
  edge [
    source 8
    target 433
  ]
  edge [
    source 8
    target 434
  ]
  edge [
    source 8
    target 435
  ]
  edge [
    source 8
    target 436
  ]
  edge [
    source 8
    target 437
  ]
  edge [
    source 8
    target 438
  ]
  edge [
    source 8
    target 439
  ]
  edge [
    source 8
    target 440
  ]
  edge [
    source 8
    target 441
  ]
  edge [
    source 8
    target 442
  ]
  edge [
    source 8
    target 443
  ]
  edge [
    source 8
    target 444
  ]
  edge [
    source 8
    target 445
  ]
  edge [
    source 8
    target 446
  ]
  edge [
    source 8
    target 447
  ]
  edge [
    source 8
    target 448
  ]
  edge [
    source 8
    target 449
  ]
  edge [
    source 8
    target 450
  ]
  edge [
    source 8
    target 451
  ]
  edge [
    source 8
    target 452
  ]
  edge [
    source 8
    target 453
  ]
  edge [
    source 8
    target 454
  ]
  edge [
    source 8
    target 455
  ]
  edge [
    source 8
    target 456
  ]
  edge [
    source 8
    target 457
  ]
  edge [
    source 8
    target 458
  ]
  edge [
    source 8
    target 459
  ]
  edge [
    source 8
    target 460
  ]
  edge [
    source 8
    target 461
  ]
  edge [
    source 8
    target 462
  ]
  edge [
    source 8
    target 463
  ]
  edge [
    source 8
    target 464
  ]
  edge [
    source 8
    target 465
  ]
  edge [
    source 8
    target 466
  ]
  edge [
    source 8
    target 467
  ]
  edge [
    source 8
    target 468
  ]
  edge [
    source 8
    target 469
  ]
  edge [
    source 8
    target 470
  ]
  edge [
    source 8
    target 471
  ]
  edge [
    source 8
    target 472
  ]
  edge [
    source 8
    target 473
  ]
  edge [
    source 8
    target 474
  ]
  edge [
    source 8
    target 475
  ]
  edge [
    source 8
    target 476
  ]
  edge [
    source 8
    target 477
  ]
  edge [
    source 8
    target 478
  ]
  edge [
    source 8
    target 479
  ]
  edge [
    source 8
    target 480
  ]
  edge [
    source 8
    target 481
  ]
  edge [
    source 8
    target 482
  ]
  edge [
    source 8
    target 483
  ]
  edge [
    source 8
    target 484
  ]
  edge [
    source 8
    target 485
  ]
  edge [
    source 8
    target 486
  ]
  edge [
    source 8
    target 487
  ]
  edge [
    source 8
    target 488
  ]
  edge [
    source 8
    target 489
  ]
  edge [
    source 8
    target 490
  ]
  edge [
    source 8
    target 491
  ]
  edge [
    source 8
    target 492
  ]
  edge [
    source 8
    target 493
  ]
  edge [
    source 8
    target 494
  ]
  edge [
    source 8
    target 495
  ]
  edge [
    source 8
    target 496
  ]
  edge [
    source 8
    target 497
  ]
  edge [
    source 8
    target 498
  ]
  edge [
    source 8
    target 499
  ]
  edge [
    source 8
    target 500
  ]
  edge [
    source 8
    target 501
  ]
  edge [
    source 8
    target 502
  ]
  edge [
    source 8
    target 503
  ]
  edge [
    source 8
    target 504
  ]
  edge [
    source 8
    target 505
  ]
  edge [
    source 8
    target 506
  ]
  edge [
    source 8
    target 507
  ]
  edge [
    source 8
    target 508
  ]
  edge [
    source 8
    target 509
  ]
  edge [
    source 8
    target 510
  ]
  edge [
    source 8
    target 511
  ]
  edge [
    source 8
    target 512
  ]
  edge [
    source 8
    target 513
  ]
  edge [
    source 8
    target 514
  ]
  edge [
    source 8
    target 515
  ]
  edge [
    source 8
    target 516
  ]
  edge [
    source 8
    target 517
  ]
  edge [
    source 8
    target 518
  ]
  edge [
    source 8
    target 519
  ]
  edge [
    source 8
    target 520
  ]
  edge [
    source 8
    target 521
  ]
  edge [
    source 8
    target 522
  ]
  edge [
    source 8
    target 523
  ]
  edge [
    source 8
    target 524
  ]
  edge [
    source 8
    target 525
  ]
  edge [
    source 8
    target 526
  ]
  edge [
    source 8
    target 527
  ]
  edge [
    source 8
    target 528
  ]
  edge [
    source 8
    target 529
  ]
  edge [
    source 8
    target 530
  ]
  edge [
    source 8
    target 531
  ]
  edge [
    source 8
    target 532
  ]
  edge [
    source 8
    target 533
  ]
  edge [
    source 8
    target 534
  ]
  edge [
    source 8
    target 535
  ]
  edge [
    source 8
    target 536
  ]
  edge [
    source 8
    target 537
  ]
  edge [
    source 8
    target 538
  ]
  edge [
    source 8
    target 539
  ]
  edge [
    source 8
    target 540
  ]
  edge [
    source 8
    target 541
  ]
  edge [
    source 8
    target 542
  ]
  edge [
    source 8
    target 543
  ]
  edge [
    source 8
    target 544
  ]
  edge [
    source 8
    target 545
  ]
  edge [
    source 8
    target 546
  ]
  edge [
    source 8
    target 547
  ]
  edge [
    source 8
    target 548
  ]
  edge [
    source 8
    target 549
  ]
  edge [
    source 8
    target 550
  ]
  edge [
    source 8
    target 551
  ]
  edge [
    source 8
    target 552
  ]
  edge [
    source 8
    target 553
  ]
  edge [
    source 8
    target 554
  ]
  edge [
    source 8
    target 555
  ]
  edge [
    source 8
    target 556
  ]
  edge [
    source 8
    target 557
  ]
  edge [
    source 8
    target 558
  ]
  edge [
    source 8
    target 559
  ]
  edge [
    source 8
    target 560
  ]
  edge [
    source 8
    target 561
  ]
  edge [
    source 8
    target 562
  ]
  edge [
    source 8
    target 563
  ]
  edge [
    source 8
    target 564
  ]
  edge [
    source 8
    target 565
  ]
  edge [
    source 8
    target 566
  ]
  edge [
    source 8
    target 567
  ]
  edge [
    source 8
    target 568
  ]
  edge [
    source 8
    target 569
  ]
  edge [
    source 8
    target 570
  ]
  edge [
    source 8
    target 571
  ]
  edge [
    source 8
    target 572
  ]
  edge [
    source 8
    target 573
  ]
  edge [
    source 8
    target 574
  ]
  edge [
    source 8
    target 575
  ]
  edge [
    source 8
    target 576
  ]
  edge [
    source 8
    target 577
  ]
  edge [
    source 8
    target 578
  ]
  edge [
    source 8
    target 579
  ]
  edge [
    source 8
    target 580
  ]
  edge [
    source 8
    target 581
  ]
  edge [
    source 8
    target 582
  ]
  edge [
    source 8
    target 583
  ]
  edge [
    source 8
    target 584
  ]
  edge [
    source 8
    target 585
  ]
  edge [
    source 8
    target 586
  ]
  edge [
    source 8
    target 587
  ]
  edge [
    source 8
    target 588
  ]
  edge [
    source 8
    target 589
  ]
  edge [
    source 8
    target 590
  ]
  edge [
    source 8
    target 591
  ]
  edge [
    source 8
    target 592
  ]
  edge [
    source 8
    target 593
  ]
  edge [
    source 8
    target 594
  ]
  edge [
    source 8
    target 595
  ]
  edge [
    source 8
    target 596
  ]
  edge [
    source 8
    target 597
  ]
  edge [
    source 8
    target 598
  ]
  edge [
    source 8
    target 599
  ]
  edge [
    source 8
    target 600
  ]
  edge [
    source 8
    target 601
  ]
  edge [
    source 8
    target 602
  ]
  edge [
    source 8
    target 603
  ]
  edge [
    source 8
    target 604
  ]
  edge [
    source 8
    target 605
  ]
  edge [
    source 8
    target 606
  ]
  edge [
    source 8
    target 607
  ]
  edge [
    source 8
    target 608
  ]
  edge [
    source 8
    target 609
  ]
  edge [
    source 8
    target 610
  ]
  edge [
    source 8
    target 611
  ]
  edge [
    source 8
    target 612
  ]
  edge [
    source 8
    target 613
  ]
  edge [
    source 8
    target 614
  ]
  edge [
    source 8
    target 615
  ]
  edge [
    source 8
    target 616
  ]
  edge [
    source 8
    target 617
  ]
  edge [
    source 8
    target 618
  ]
  edge [
    source 8
    target 619
  ]
  edge [
    source 8
    target 620
  ]
  edge [
    source 8
    target 621
  ]
  edge [
    source 8
    target 622
  ]
  edge [
    source 8
    target 623
  ]
  edge [
    source 8
    target 624
  ]
  edge [
    source 8
    target 625
  ]
  edge [
    source 8
    target 626
  ]
  edge [
    source 8
    target 627
  ]
  edge [
    source 8
    target 628
  ]
  edge [
    source 8
    target 629
  ]
  edge [
    source 8
    target 630
  ]
  edge [
    source 8
    target 631
  ]
  edge [
    source 8
    target 632
  ]
  edge [
    source 8
    target 633
  ]
  edge [
    source 8
    target 634
  ]
  edge [
    source 8
    target 635
  ]
  edge [
    source 8
    target 636
  ]
  edge [
    source 8
    target 637
  ]
  edge [
    source 8
    target 638
  ]
  edge [
    source 8
    target 639
  ]
  edge [
    source 8
    target 640
  ]
  edge [
    source 8
    target 641
  ]
  edge [
    source 8
    target 642
  ]
  edge [
    source 9
    target 643
  ]
  edge [
    source 9
    target 644
  ]
  edge [
    source 9
    target 645
  ]
  edge [
    source 9
    target 646
  ]
]
