graph [
  maxDegree 13
  minDegree 1
  meanDegree 1.9230769230769231
  density 0.07692307692307693
  graphCliqueNumber 2
  node [
    id 0
    label "filler"
    origin "text"
  ]
  node [
    id 1
    label "aleja"
    origin "text"
  ]
  node [
    id 2
    label "jerozolimski"
    origin "text"
  ]
  node [
    id 3
    label "by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "ten"
    origin "text"
  ]
  node [
    id 5
    label "autobus"
    origin "text"
  ]
  node [
    id 6
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 7
    label "drzewo_owocowe"
  ]
  node [
    id 8
    label "chodnik"
  ]
  node [
    id 9
    label "ulica"
  ]
  node [
    id 10
    label "deptak"
  ]
  node [
    id 11
    label "si&#281;ga&#263;"
  ]
  node [
    id 12
    label "trwa&#263;"
  ]
  node [
    id 13
    label "obecno&#347;&#263;"
  ]
  node [
    id 14
    label "stan"
  ]
  node [
    id 15
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 16
    label "stand"
  ]
  node [
    id 17
    label "mie&#263;_miejsce"
  ]
  node [
    id 18
    label "uczestniczy&#263;"
  ]
  node [
    id 19
    label "chodzi&#263;"
  ]
  node [
    id 20
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 21
    label "equal"
  ]
  node [
    id 22
    label "okre&#347;lony"
  ]
  node [
    id 23
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 24
    label "samoch&#243;d"
  ]
  node [
    id 25
    label "miejsce_siedz&#261;ce"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 25
  ]
]
