graph [
  maxDegree 35
  minDegree 1
  meanDegree 2.051546391752577
  density 0.010629774050531489
  graphCliqueNumber 3
  node [
    id 0
    label "szcz&#281;&#347;cie"
    origin "text"
  ]
  node [
    id 1
    label "chodzi&#263;"
    origin "text"
  ]
  node [
    id 2
    label "widma"
    origin "text"
  ]
  node [
    id 3
    label "elektromagnetyczny"
    origin "text"
  ]
  node [
    id 4
    label "niedawno"
    origin "text"
  ]
  node [
    id 5
    label "komisja"
    origin "text"
  ]
  node [
    id 6
    label "europejski"
    origin "text"
  ]
  node [
    id 7
    label "og&#322;osi&#263;"
    origin "text"
  ]
  node [
    id 8
    label "zamierza&#263;"
    origin "text"
  ]
  node [
    id 9
    label "doprowadzi&#263;"
    origin "text"
  ]
  node [
    id 10
    label "uchyli&#263;"
    origin "text"
  ]
  node [
    id 11
    label "stara"
    origin "text"
  ]
  node [
    id 12
    label "dyrektywa"
    origin "text"
  ]
  node [
    id 13
    label "gsm"
    origin "text"
  ]
  node [
    id 14
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 15
    label "sprawi&#263;"
    origin "text"
  ]
  node [
    id 16
    label "sie&#263;"
    origin "text"
  ]
  node [
    id 17
    label "kom&#243;rkowy"
    origin "text"
  ]
  node [
    id 18
    label "europa"
    origin "text"
  ]
  node [
    id 19
    label "rozwin&#261;&#263;"
    origin "text"
  ]
  node [
    id 20
    label "si&#281;"
    origin "text"
  ]
  node [
    id 21
    label "szybko"
    origin "text"
  ]
  node [
    id 22
    label "sprawnie"
    origin "text"
  ]
  node [
    id 23
    label "podczas"
    origin "text"
  ]
  node [
    id 24
    label "gdy"
    origin "text"
  ]
  node [
    id 25
    label "usa"
    origin "text"
  ]
  node [
    id 26
    label "trzeba"
    origin "text"
  ]
  node [
    id 27
    label "by&#263;"
    origin "text"
  ]
  node [
    id 28
    label "czeka&#263;"
    origin "text"
  ]
  node [
    id 29
    label "iphone"
    origin "text"
  ]
  node [
    id 30
    label "wra&#380;enie"
  ]
  node [
    id 31
    label "przeznaczenie"
  ]
  node [
    id 32
    label "dobrodziejstwo"
  ]
  node [
    id 33
    label "dobro"
  ]
  node [
    id 34
    label "przypadek"
  ]
  node [
    id 35
    label "proceed"
  ]
  node [
    id 36
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 37
    label "bangla&#263;"
  ]
  node [
    id 38
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 39
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 40
    label "run"
  ]
  node [
    id 41
    label "tryb"
  ]
  node [
    id 42
    label "p&#322;ywa&#263;"
  ]
  node [
    id 43
    label "continue"
  ]
  node [
    id 44
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 45
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 46
    label "przebiega&#263;"
  ]
  node [
    id 47
    label "mie&#263;_miejsce"
  ]
  node [
    id 48
    label "wk&#322;ada&#263;"
  ]
  node [
    id 49
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 50
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 51
    label "para"
  ]
  node [
    id 52
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 53
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 54
    label "krok"
  ]
  node [
    id 55
    label "str&#243;j"
  ]
  node [
    id 56
    label "bywa&#263;"
  ]
  node [
    id 57
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 58
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 59
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 60
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 61
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 62
    label "dziama&#263;"
  ]
  node [
    id 63
    label "stara&#263;_si&#281;"
  ]
  node [
    id 64
    label "carry"
  ]
  node [
    id 65
    label "elektromagnetycznie"
  ]
  node [
    id 66
    label "ostatni"
  ]
  node [
    id 67
    label "aktualnie"
  ]
  node [
    id 68
    label "obrady"
  ]
  node [
    id 69
    label "zesp&#243;&#322;"
  ]
  node [
    id 70
    label "organ"
  ]
  node [
    id 71
    label "Komisja_Europejska"
  ]
  node [
    id 72
    label "podkomisja"
  ]
  node [
    id 73
    label "European"
  ]
  node [
    id 74
    label "po_europejsku"
  ]
  node [
    id 75
    label "charakterystyczny"
  ]
  node [
    id 76
    label "europejsko"
  ]
  node [
    id 77
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 78
    label "typowy"
  ]
  node [
    id 79
    label "communicate"
  ]
  node [
    id 80
    label "opublikowa&#263;"
  ]
  node [
    id 81
    label "obwo&#322;a&#263;"
  ]
  node [
    id 82
    label "poda&#263;"
  ]
  node [
    id 83
    label "publish"
  ]
  node [
    id 84
    label "declare"
  ]
  node [
    id 85
    label "volunteer"
  ]
  node [
    id 86
    label "poprowadzi&#263;"
  ]
  node [
    id 87
    label "spowodowa&#263;"
  ]
  node [
    id 88
    label "pos&#322;a&#263;"
  ]
  node [
    id 89
    label "have_a_bun_in_the_oven"
  ]
  node [
    id 90
    label "wykona&#263;"
  ]
  node [
    id 91
    label "wzbudzi&#263;"
  ]
  node [
    id 92
    label "wprowadzi&#263;"
  ]
  node [
    id 93
    label "set"
  ]
  node [
    id 94
    label "take"
  ]
  node [
    id 95
    label "rise"
  ]
  node [
    id 96
    label "zniwelowa&#263;"
  ]
  node [
    id 97
    label "przesun&#261;&#263;"
  ]
  node [
    id 98
    label "retract"
  ]
  node [
    id 99
    label "uniewa&#380;ni&#263;"
  ]
  node [
    id 100
    label "matka"
  ]
  node [
    id 101
    label "kobieta"
  ]
  node [
    id 102
    label "partnerka"
  ]
  node [
    id 103
    label "&#380;ona"
  ]
  node [
    id 104
    label "starzy"
  ]
  node [
    id 105
    label "polecenie"
  ]
  node [
    id 106
    label "guidance"
  ]
  node [
    id 107
    label "wyrobi&#263;"
  ]
  node [
    id 108
    label "przygotowa&#263;"
  ]
  node [
    id 109
    label "wzi&#261;&#263;"
  ]
  node [
    id 110
    label "catch"
  ]
  node [
    id 111
    label "frame"
  ]
  node [
    id 112
    label "hipertekst"
  ]
  node [
    id 113
    label "gauze"
  ]
  node [
    id 114
    label "nitka"
  ]
  node [
    id 115
    label "mesh"
  ]
  node [
    id 116
    label "e-hazard"
  ]
  node [
    id 117
    label "netbook"
  ]
  node [
    id 118
    label "cyberprzestrze&#324;"
  ]
  node [
    id 119
    label "biznes_elektroniczny"
  ]
  node [
    id 120
    label "snu&#263;"
  ]
  node [
    id 121
    label "organization"
  ]
  node [
    id 122
    label "zasadzka"
  ]
  node [
    id 123
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 124
    label "web"
  ]
  node [
    id 125
    label "provider"
  ]
  node [
    id 126
    label "struktura"
  ]
  node [
    id 127
    label "us&#322;uga_internetowa"
  ]
  node [
    id 128
    label "punkt_dost&#281;pu"
  ]
  node [
    id 129
    label "organizacja"
  ]
  node [
    id 130
    label "mem"
  ]
  node [
    id 131
    label "vane"
  ]
  node [
    id 132
    label "podcast"
  ]
  node [
    id 133
    label "grooming"
  ]
  node [
    id 134
    label "kszta&#322;t"
  ]
  node [
    id 135
    label "strona"
  ]
  node [
    id 136
    label "obiekt"
  ]
  node [
    id 137
    label "wysnu&#263;"
  ]
  node [
    id 138
    label "gra_sieciowa"
  ]
  node [
    id 139
    label "instalacja"
  ]
  node [
    id 140
    label "sie&#263;_komputerowa"
  ]
  node [
    id 141
    label "net"
  ]
  node [
    id 142
    label "plecionka"
  ]
  node [
    id 143
    label "media"
  ]
  node [
    id 144
    label "rozmieszczenie"
  ]
  node [
    id 145
    label "postawi&#263;"
  ]
  node [
    id 146
    label "rozpakowa&#263;"
  ]
  node [
    id 147
    label "pu&#347;ci&#263;"
  ]
  node [
    id 148
    label "zwi&#281;kszy&#263;"
  ]
  node [
    id 149
    label "gallop"
  ]
  node [
    id 150
    label "om&#243;wi&#263;"
  ]
  node [
    id 151
    label "roz&#322;o&#380;y&#263;"
  ]
  node [
    id 152
    label "rozstawi&#263;"
  ]
  node [
    id 153
    label "develop"
  ]
  node [
    id 154
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 155
    label "evolve"
  ]
  node [
    id 156
    label "exsert"
  ]
  node [
    id 157
    label "dopowiedzie&#263;"
  ]
  node [
    id 158
    label "quicker"
  ]
  node [
    id 159
    label "promptly"
  ]
  node [
    id 160
    label "bezpo&#347;rednio"
  ]
  node [
    id 161
    label "quickest"
  ]
  node [
    id 162
    label "dynamicznie"
  ]
  node [
    id 163
    label "szybciej"
  ]
  node [
    id 164
    label "prosto"
  ]
  node [
    id 165
    label "szybciochem"
  ]
  node [
    id 166
    label "szybki"
  ]
  node [
    id 167
    label "zdrowo"
  ]
  node [
    id 168
    label "kompetentnie"
  ]
  node [
    id 169
    label "udanie"
  ]
  node [
    id 170
    label "dobrze"
  ]
  node [
    id 171
    label "funkcjonalnie"
  ]
  node [
    id 172
    label "sprawny"
  ]
  node [
    id 173
    label "umiej&#281;tnie"
  ]
  node [
    id 174
    label "skutecznie"
  ]
  node [
    id 175
    label "trza"
  ]
  node [
    id 176
    label "necessity"
  ]
  node [
    id 177
    label "si&#281;ga&#263;"
  ]
  node [
    id 178
    label "trwa&#263;"
  ]
  node [
    id 179
    label "obecno&#347;&#263;"
  ]
  node [
    id 180
    label "stan"
  ]
  node [
    id 181
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 182
    label "stand"
  ]
  node [
    id 183
    label "uczestniczy&#263;"
  ]
  node [
    id 184
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 185
    label "equal"
  ]
  node [
    id 186
    label "hold"
  ]
  node [
    id 187
    label "sp&#281;dza&#263;"
  ]
  node [
    id 188
    label "look"
  ]
  node [
    id 189
    label "decydowa&#263;"
  ]
  node [
    id 190
    label "oczekiwa&#263;"
  ]
  node [
    id 191
    label "pauzowa&#263;"
  ]
  node [
    id 192
    label "anticipate"
  ]
  node [
    id 193
    label "iPhone"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 100
  ]
  edge [
    source 11
    target 101
  ]
  edge [
    source 11
    target 102
  ]
  edge [
    source 11
    target 103
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 107
  ]
  edge [
    source 15
    target 108
  ]
  edge [
    source 15
    target 109
  ]
  edge [
    source 15
    target 110
  ]
  edge [
    source 15
    target 87
  ]
  edge [
    source 15
    target 111
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 112
  ]
  edge [
    source 16
    target 113
  ]
  edge [
    source 16
    target 114
  ]
  edge [
    source 16
    target 115
  ]
  edge [
    source 16
    target 116
  ]
  edge [
    source 16
    target 117
  ]
  edge [
    source 16
    target 118
  ]
  edge [
    source 16
    target 119
  ]
  edge [
    source 16
    target 120
  ]
  edge [
    source 16
    target 121
  ]
  edge [
    source 16
    target 122
  ]
  edge [
    source 16
    target 123
  ]
  edge [
    source 16
    target 124
  ]
  edge [
    source 16
    target 125
  ]
  edge [
    source 16
    target 126
  ]
  edge [
    source 16
    target 127
  ]
  edge [
    source 16
    target 128
  ]
  edge [
    source 16
    target 129
  ]
  edge [
    source 16
    target 130
  ]
  edge [
    source 16
    target 131
  ]
  edge [
    source 16
    target 132
  ]
  edge [
    source 16
    target 133
  ]
  edge [
    source 16
    target 134
  ]
  edge [
    source 16
    target 135
  ]
  edge [
    source 16
    target 136
  ]
  edge [
    source 16
    target 137
  ]
  edge [
    source 16
    target 138
  ]
  edge [
    source 16
    target 139
  ]
  edge [
    source 16
    target 140
  ]
  edge [
    source 16
    target 141
  ]
  edge [
    source 16
    target 142
  ]
  edge [
    source 16
    target 143
  ]
  edge [
    source 16
    target 144
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 145
  ]
  edge [
    source 19
    target 146
  ]
  edge [
    source 19
    target 147
  ]
  edge [
    source 19
    target 148
  ]
  edge [
    source 19
    target 149
  ]
  edge [
    source 19
    target 150
  ]
  edge [
    source 19
    target 151
  ]
  edge [
    source 19
    target 152
  ]
  edge [
    source 19
    target 153
  ]
  edge [
    source 19
    target 154
  ]
  edge [
    source 19
    target 155
  ]
  edge [
    source 19
    target 156
  ]
  edge [
    source 19
    target 157
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 158
  ]
  edge [
    source 21
    target 159
  ]
  edge [
    source 21
    target 160
  ]
  edge [
    source 21
    target 161
  ]
  edge [
    source 21
    target 162
  ]
  edge [
    source 21
    target 163
  ]
  edge [
    source 21
    target 164
  ]
  edge [
    source 21
    target 165
  ]
  edge [
    source 21
    target 166
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 167
  ]
  edge [
    source 22
    target 168
  ]
  edge [
    source 22
    target 169
  ]
  edge [
    source 22
    target 170
  ]
  edge [
    source 22
    target 171
  ]
  edge [
    source 22
    target 172
  ]
  edge [
    source 22
    target 173
  ]
  edge [
    source 22
    target 174
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 175
  ]
  edge [
    source 26
    target 176
  ]
  edge [
    source 26
    target 28
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 177
  ]
  edge [
    source 27
    target 178
  ]
  edge [
    source 27
    target 179
  ]
  edge [
    source 27
    target 180
  ]
  edge [
    source 27
    target 181
  ]
  edge [
    source 27
    target 182
  ]
  edge [
    source 27
    target 47
  ]
  edge [
    source 27
    target 183
  ]
  edge [
    source 27
    target 184
  ]
  edge [
    source 27
    target 185
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 186
  ]
  edge [
    source 28
    target 187
  ]
  edge [
    source 28
    target 188
  ]
  edge [
    source 28
    target 189
  ]
  edge [
    source 28
    target 190
  ]
  edge [
    source 28
    target 191
  ]
  edge [
    source 28
    target 192
  ]
  edge [
    source 29
    target 193
  ]
]
