graph [
  maxDegree 19
  minDegree 1
  meanDegree 1.9565217391304348
  density 0.043478260869565216
  graphCliqueNumber 2
  node [
    id 0
    label "dwa"
    origin "text"
  ]
  node [
    id 1
    label "tydzie&#324;"
    origin "text"
  ]
  node [
    id 2
    label "zdrowie"
    origin "text"
  ]
  node [
    id 3
    label "celina"
    origin "text"
  ]
  node [
    id 4
    label "pozwoli&#263;"
    origin "text"
  ]
  node [
    id 5
    label "siebie"
    origin "text"
  ]
  node [
    id 6
    label "gdzie&#347;"
    origin "text"
  ]
  node [
    id 7
    label "odessa"
    origin "text"
  ]
  node [
    id 8
    label "daleko"
    origin "text"
  ]
  node [
    id 9
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 10
    label "doba"
  ]
  node [
    id 11
    label "czas"
  ]
  node [
    id 12
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 13
    label "weekend"
  ]
  node [
    id 14
    label "miesi&#261;c"
  ]
  node [
    id 15
    label "os&#322;abia&#263;"
  ]
  node [
    id 16
    label "niszczy&#263;"
  ]
  node [
    id 17
    label "zniszczy&#263;"
  ]
  node [
    id 18
    label "stan"
  ]
  node [
    id 19
    label "firmness"
  ]
  node [
    id 20
    label "kondycja"
  ]
  node [
    id 21
    label "zniszczenie"
  ]
  node [
    id 22
    label "rozsypanie_si&#281;"
  ]
  node [
    id 23
    label "os&#322;abi&#263;"
  ]
  node [
    id 24
    label "cecha"
  ]
  node [
    id 25
    label "zdarcie"
  ]
  node [
    id 26
    label "zdrowotno&#347;&#263;"
  ]
  node [
    id 27
    label "zedrze&#263;"
  ]
  node [
    id 28
    label "niszczenie"
  ]
  node [
    id 29
    label "os&#322;abienie"
  ]
  node [
    id 30
    label "soundness"
  ]
  node [
    id 31
    label "os&#322;abianie"
  ]
  node [
    id 32
    label "pofolgowa&#263;"
  ]
  node [
    id 33
    label "assent"
  ]
  node [
    id 34
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 35
    label "leave"
  ]
  node [
    id 36
    label "uzna&#263;"
  ]
  node [
    id 37
    label "dawno"
  ]
  node [
    id 38
    label "nisko"
  ]
  node [
    id 39
    label "nieobecnie"
  ]
  node [
    id 40
    label "daleki"
  ]
  node [
    id 41
    label "het"
  ]
  node [
    id 42
    label "wysoko"
  ]
  node [
    id 43
    label "du&#380;o"
  ]
  node [
    id 44
    label "znacznie"
  ]
  node [
    id 45
    label "g&#322;&#281;boko"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 37
  ]
  edge [
    source 8
    target 38
  ]
  edge [
    source 8
    target 39
  ]
  edge [
    source 8
    target 40
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 8
    target 45
  ]
]
