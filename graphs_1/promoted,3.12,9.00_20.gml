graph [
  maxDegree 9
  minDegree 1
  meanDegree 1.8571428571428572
  density 0.14285714285714285
  graphCliqueNumber 2
  node [
    id 0
    label "wielki"
    origin "text"
  ]
  node [
    id 1
    label "wojownik"
    origin "text"
  ]
  node [
    id 2
    label "dupny"
  ]
  node [
    id 3
    label "wysoce"
  ]
  node [
    id 4
    label "wyj&#261;tkowy"
  ]
  node [
    id 5
    label "wybitny"
  ]
  node [
    id 6
    label "znaczny"
  ]
  node [
    id 7
    label "prawdziwy"
  ]
  node [
    id 8
    label "wa&#380;ny"
  ]
  node [
    id 9
    label "nieprzeci&#281;tny"
  ]
  node [
    id 10
    label "&#380;o&#322;nierz"
  ]
  node [
    id 11
    label "cz&#322;owiek"
  ]
  node [
    id 12
    label "walcz&#261;cy"
  ]
  node [
    id 13
    label "osada"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
]
