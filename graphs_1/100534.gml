graph [
  maxDegree 6
  minDegree 1
  meanDegree 1.6923076923076923
  density 0.14102564102564102
  graphCliqueNumber 3
  node [
    id 0
    label "pal"
    origin "text"
  ]
  node [
    id 1
    label "pra&#269;a"
    origin "text"
  ]
  node [
    id 2
    label "dalba"
  ]
  node [
    id 3
    label "picket"
  ]
  node [
    id 4
    label "paliszcze"
  ]
  node [
    id 5
    label "s&#322;up"
  ]
  node [
    id 6
    label "palisada"
  ]
  node [
    id 7
    label "pa&#322;a"
  ]
  node [
    id 8
    label "Pra&#269;a"
  ]
  node [
    id 9
    label "Bo&#347;nia"
  ]
  node [
    id 10
    label "Podrinje"
  ]
  node [
    id 11
    label "i"
  ]
  node [
    id 12
    label "Hercegowina"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 9
    target 12
  ]
  edge [
    source 11
    target 12
  ]
]
