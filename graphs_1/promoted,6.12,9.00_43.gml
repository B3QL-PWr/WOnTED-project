graph [
  maxDegree 35
  minDegree 1
  meanDegree 1.96875
  density 0.03125
  graphCliqueNumber 2
  node [
    id 0
    label "rom"
    origin "text"
  ]
  node [
    id 1
    label "podkrakowski"
    origin "text"
  ]
  node [
    id 2
    label "maszkowic"
    origin "text"
  ]
  node [
    id 3
    label "mieszka&#263;"
    origin "text"
  ]
  node [
    id 4
    label "skandaliczny"
    origin "text"
  ]
  node [
    id 5
    label "warunek"
    origin "text"
  ]
  node [
    id 6
    label "osada"
    origin "text"
  ]
  node [
    id 7
    label "przypomina&#263;"
    origin "text"
  ]
  node [
    id 8
    label "slums"
    origin "text"
  ]
  node [
    id 9
    label "zajmowa&#263;"
  ]
  node [
    id 10
    label "sta&#263;"
  ]
  node [
    id 11
    label "przebywa&#263;"
  ]
  node [
    id 12
    label "room"
  ]
  node [
    id 13
    label "panowa&#263;"
  ]
  node [
    id 14
    label "fall"
  ]
  node [
    id 15
    label "skandalicznie"
  ]
  node [
    id 16
    label "straszny"
  ]
  node [
    id 17
    label "gorsz&#261;cy"
  ]
  node [
    id 18
    label "sensacyjny"
  ]
  node [
    id 19
    label "za&#322;o&#380;enie"
  ]
  node [
    id 20
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 21
    label "umowa"
  ]
  node [
    id 22
    label "agent"
  ]
  node [
    id 23
    label "condition"
  ]
  node [
    id 24
    label "ekspozycja"
  ]
  node [
    id 25
    label "faktor"
  ]
  node [
    id 26
    label "&#346;mie&#322;&#243;w"
  ]
  node [
    id 27
    label "strza&#322;a"
  ]
  node [
    id 28
    label "Go&#322;&#261;bek"
  ]
  node [
    id 29
    label "grupa"
  ]
  node [
    id 30
    label "Antoniewo"
  ]
  node [
    id 31
    label "G&#243;rczyn"
  ]
  node [
    id 32
    label "Gwiazdowo"
  ]
  node [
    id 33
    label "ochrona"
  ]
  node [
    id 34
    label "&#379;ebrowo"
  ]
  node [
    id 35
    label "Paszk&#243;w"
  ]
  node [
    id 36
    label "nasada"
  ]
  node [
    id 37
    label "skupienie"
  ]
  node [
    id 38
    label "Rog&#243;w"
  ]
  node [
    id 39
    label "kwiatostan"
  ]
  node [
    id 40
    label "Nowy_Korczyn"
  ]
  node [
    id 41
    label "Pokrowskoje"
  ]
  node [
    id 42
    label "Falenty"
  ]
  node [
    id 43
    label "wojownik"
  ]
  node [
    id 44
    label "W&#243;lka"
  ]
  node [
    id 45
    label "Grabowiec"
  ]
  node [
    id 46
    label "Izbica"
  ]
  node [
    id 47
    label "Wieniec-Zdr&#243;j"
  ]
  node [
    id 48
    label "Gr&#243;dek"
  ]
  node [
    id 49
    label "Sobk&#243;w"
  ]
  node [
    id 50
    label "Babin"
  ]
  node [
    id 51
    label "crew"
  ]
  node [
    id 52
    label "Grzybowo"
  ]
  node [
    id 53
    label "Turlej"
  ]
  node [
    id 54
    label "dru&#380;yna"
  ]
  node [
    id 55
    label "zbi&#243;r"
  ]
  node [
    id 56
    label "Rejowiec"
  ]
  node [
    id 57
    label "Skotniki"
  ]
  node [
    id 58
    label "miejscowo&#347;&#263;"
  ]
  node [
    id 59
    label "recall"
  ]
  node [
    id 60
    label "u&#347;wiadamia&#263;"
  ]
  node [
    id 61
    label "informowa&#263;"
  ]
  node [
    id 62
    label "prompt"
  ]
  node [
    id 63
    label "dom"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 8
    target 63
  ]
]
