graph [
  maxDegree 14
  minDegree 1
  meanDegree 1.9230769230769231
  density 0.03770739064856712
  graphCliqueNumber 2
  node [
    id 0
    label "robot"
    origin "text"
  ]
  node [
    id 1
    label "amazona"
    origin "text"
  ]
  node [
    id 2
    label "spryska&#263;"
    origin "text"
  ]
  node [
    id 3
    label "pracownik"
    origin "text"
  ]
  node [
    id 4
    label "&#347;rodek"
    origin "text"
  ]
  node [
    id 5
    label "odstraszaj&#261;cy"
    origin "text"
  ]
  node [
    id 6
    label "nied&#378;wied&#378;"
    origin "text"
  ]
  node [
    id 7
    label "przypadkowo"
    origin "text"
  ]
  node [
    id 8
    label "urz&#261;dzenie_kuchenne"
  ]
  node [
    id 9
    label "maszyna"
  ]
  node [
    id 10
    label "sprz&#281;t_AGD"
  ]
  node [
    id 11
    label "automat"
  ]
  node [
    id 12
    label "zwil&#380;y&#263;"
  ]
  node [
    id 13
    label "spray"
  ]
  node [
    id 14
    label "zmoczy&#263;"
  ]
  node [
    id 15
    label "zabezpieczy&#263;"
  ]
  node [
    id 16
    label "nawie&#378;&#263;"
  ]
  node [
    id 17
    label "spatter"
  ]
  node [
    id 18
    label "cz&#322;owiek_pracy"
  ]
  node [
    id 19
    label "cz&#322;owiek"
  ]
  node [
    id 20
    label "delegowa&#263;"
  ]
  node [
    id 21
    label "s&#322;u&#380;ba_cywilna"
  ]
  node [
    id 22
    label "pracu&#347;"
  ]
  node [
    id 23
    label "delegowanie"
  ]
  node [
    id 24
    label "r&#281;ka"
  ]
  node [
    id 25
    label "salariat"
  ]
  node [
    id 26
    label "miejsce"
  ]
  node [
    id 27
    label "czas"
  ]
  node [
    id 28
    label "abstrakcja"
  ]
  node [
    id 29
    label "punkt"
  ]
  node [
    id 30
    label "substancja"
  ]
  node [
    id 31
    label "spos&#243;b"
  ]
  node [
    id 32
    label "chemikalia"
  ]
  node [
    id 33
    label "odpychaj&#261;cy"
  ]
  node [
    id 34
    label "odstraszaj&#261;co"
  ]
  node [
    id 35
    label "mi&#347;"
  ]
  node [
    id 36
    label "strategia_nied&#378;wiedzia"
  ]
  node [
    id 37
    label "symbol"
  ]
  node [
    id 38
    label "ssak_drapie&#380;ny"
  ]
  node [
    id 39
    label "gie&#322;da_papier&#243;w_warto&#347;ciowych"
  ]
  node [
    id 40
    label "gawrowanie"
  ]
  node [
    id 41
    label "nied&#378;wiedziowate"
  ]
  node [
    id 42
    label "wszystko&#380;erca"
  ]
  node [
    id 43
    label "ch&#322;op_jak_d&#261;b"
  ]
  node [
    id 44
    label "marucha"
  ]
  node [
    id 45
    label "gawra"
  ]
  node [
    id 46
    label "gawrowa&#263;"
  ]
  node [
    id 47
    label "rzutem_na_ta&#347;m&#281;"
  ]
  node [
    id 48
    label "przypadkowy"
  ]
  node [
    id 49
    label "Amazona"
  ]
  node [
    id 50
    label "Newa"
  ]
  node [
    id 51
    label "jersey"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 50
    target 51
  ]
]
