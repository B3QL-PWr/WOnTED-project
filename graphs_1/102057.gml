graph [
  maxDegree 10
  minDegree 1
  meanDegree 1.875
  density 0.125
  graphCliqueNumber 2
  node [
    id 0
    label "qmam"
    origin "text"
  ]
  node [
    id 1
    label "the"
    origin "text"
  ]
  node [
    id 2
    label "best"
    origin "text"
  ]
  node [
    id 3
    label "internauta"
    origin "text"
  ]
  node [
    id 4
    label "wybra&#263;"
    origin "text"
  ]
  node [
    id 5
    label "laureat"
    origin "text"
  ]
  node [
    id 6
    label "u&#380;ytkownik"
  ]
  node [
    id 7
    label "zu&#380;y&#263;"
  ]
  node [
    id 8
    label "distill"
  ]
  node [
    id 9
    label "wyj&#261;&#263;"
  ]
  node [
    id 10
    label "sie&#263;_rybacka"
  ]
  node [
    id 11
    label "powo&#322;a&#263;"
  ]
  node [
    id 12
    label "kotwica"
  ]
  node [
    id 13
    label "ustali&#263;"
  ]
  node [
    id 14
    label "pick"
  ]
  node [
    id 15
    label "zdobywca"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 4
    target 8
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 4
    target 10
  ]
  edge [
    source 4
    target 11
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 5
    target 15
  ]
]
