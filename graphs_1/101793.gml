graph [
  maxDegree 20
  minDegree 1
  meanDegree 2.1196581196581197
  density 0.009097245148747294
  graphCliqueNumber 3
  node [
    id 0
    label "jak"
    origin "text"
  ]
  node [
    id 1
    label "pisa&#263;by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "cia"
    origin "text"
  ]
  node [
    id 3
    label "rok"
    origin "text"
  ]
  node [
    id 4
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 5
    label "zmniejszy&#263;"
    origin "text"
  ]
  node [
    id 6
    label "si&#281;"
    origin "text"
  ]
  node [
    id 7
    label "wynagrodzenie"
    origin "text"
  ]
  node [
    id 8
    label "nauczyciel"
    origin "text"
  ]
  node [
    id 9
    label "rz&#261;d"
    origin "text"
  ]
  node [
    id 10
    label "twierdzi&#263;"
    origin "text"
  ]
  node [
    id 11
    label "taki"
    origin "text"
  ]
  node [
    id 12
    label "niebezpiecze&#324;stwo"
    origin "text"
  ]
  node [
    id 13
    label "by&#263;"
    origin "text"
  ]
  node [
    id 14
    label "niewielki"
    origin "text"
  ]
  node [
    id 15
    label "samorz&#261;d"
    origin "text"
  ]
  node [
    id 16
    label "dop&#322;aci&#263;"
    origin "text"
  ]
  node [
    id 17
    label "pensja"
    origin "text"
  ]
  node [
    id 18
    label "aby"
    origin "text"
  ]
  node [
    id 19
    label "by&#322;y"
    origin "text"
  ]
  node [
    id 20
    label "niski"
    origin "text"
  ]
  node [
    id 21
    label "je&#347;li"
    origin "text"
  ]
  node [
    id 22
    label "mie&#263;by"
    origin "text"
  ]
  node [
    id 23
    label "podwy&#380;szy&#263;"
    origin "text"
  ]
  node [
    id 24
    label "proca"
    origin "text"
  ]
  node [
    id 25
    label "w&#322;asne"
    origin "text"
  ]
  node [
    id 26
    label "&#347;rodek"
    origin "text"
  ]
  node [
    id 27
    label "przeznaczy&#263;"
    origin "text"
  ]
  node [
    id 28
    label "ponad"
    origin "text"
  ]
  node [
    id 29
    label "milion"
    origin "text"
  ]
  node [
    id 30
    label "z&#322;oty"
    origin "text"
  ]
  node [
    id 31
    label "wiele"
    origin "text"
  ]
  node [
    id 32
    label "deklarowa&#263;"
    origin "text"
  ]
  node [
    id 33
    label "utrzyma&#263;"
    origin "text"
  ]
  node [
    id 34
    label "p&#322;aca"
    origin "text"
  ]
  node [
    id 35
    label "obecna"
    origin "text"
  ]
  node [
    id 36
    label "poziom"
    origin "text"
  ]
  node [
    id 37
    label "byd&#322;o"
  ]
  node [
    id 38
    label "zobo"
  ]
  node [
    id 39
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 40
    label "yakalo"
  ]
  node [
    id 41
    label "dzo"
  ]
  node [
    id 42
    label "stulecie"
  ]
  node [
    id 43
    label "kalendarz"
  ]
  node [
    id 44
    label "czas"
  ]
  node [
    id 45
    label "pora_roku"
  ]
  node [
    id 46
    label "cykl_astronomiczny"
  ]
  node [
    id 47
    label "p&#243;&#322;rocze"
  ]
  node [
    id 48
    label "grupa"
  ]
  node [
    id 49
    label "kwarta&#322;"
  ]
  node [
    id 50
    label "kurs"
  ]
  node [
    id 51
    label "jubileusz"
  ]
  node [
    id 52
    label "miesi&#261;c"
  ]
  node [
    id 53
    label "lata"
  ]
  node [
    id 54
    label "martwy_sezon"
  ]
  node [
    id 55
    label "zmieni&#263;"
  ]
  node [
    id 56
    label "soften"
  ]
  node [
    id 57
    label "liczenie"
  ]
  node [
    id 58
    label "return"
  ]
  node [
    id 59
    label "doch&#243;d"
  ]
  node [
    id 60
    label "zap&#322;ata"
  ]
  node [
    id 61
    label "wynagrodzenie_brutto"
  ]
  node [
    id 62
    label "ryzyko_niewygas&#322;e"
  ]
  node [
    id 63
    label "koszt_rodzajowy"
  ]
  node [
    id 64
    label "zado&#347;&#263;uczynienie"
  ]
  node [
    id 65
    label "danie"
  ]
  node [
    id 66
    label "policzenie"
  ]
  node [
    id 67
    label "policzy&#263;"
  ]
  node [
    id 68
    label "liczy&#263;"
  ]
  node [
    id 69
    label "refund"
  ]
  node [
    id 70
    label "bud&#380;et_domowy"
  ]
  node [
    id 71
    label "pay"
  ]
  node [
    id 72
    label "ordynaria"
  ]
  node [
    id 73
    label "profesor"
  ]
  node [
    id 74
    label "kszta&#322;ciciel"
  ]
  node [
    id 75
    label "szkolnik"
  ]
  node [
    id 76
    label "preceptor"
  ]
  node [
    id 77
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 78
    label "pedagog"
  ]
  node [
    id 79
    label "popularyzator"
  ]
  node [
    id 80
    label "belfer"
  ]
  node [
    id 81
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 82
    label "kategoria"
  ]
  node [
    id 83
    label "egzekutywa"
  ]
  node [
    id 84
    label "gabinet_cieni"
  ]
  node [
    id 85
    label "gromada"
  ]
  node [
    id 86
    label "premier"
  ]
  node [
    id 87
    label "Londyn"
  ]
  node [
    id 88
    label "Konsulat"
  ]
  node [
    id 89
    label "uporz&#261;dkowanie"
  ]
  node [
    id 90
    label "jednostka_systematyczna"
  ]
  node [
    id 91
    label "szpaler"
  ]
  node [
    id 92
    label "przybli&#380;enie"
  ]
  node [
    id 93
    label "tract"
  ]
  node [
    id 94
    label "number"
  ]
  node [
    id 95
    label "lon&#380;a"
  ]
  node [
    id 96
    label "w&#322;adza"
  ]
  node [
    id 97
    label "instytucja"
  ]
  node [
    id 98
    label "klasa"
  ]
  node [
    id 99
    label "zapewnia&#263;"
  ]
  node [
    id 100
    label "oznajmia&#263;"
  ]
  node [
    id 101
    label "komunikowa&#263;"
  ]
  node [
    id 102
    label "attest"
  ]
  node [
    id 103
    label "argue"
  ]
  node [
    id 104
    label "okre&#347;lony"
  ]
  node [
    id 105
    label "jaki&#347;"
  ]
  node [
    id 106
    label "zawisa&#263;"
  ]
  node [
    id 107
    label "zawisanie"
  ]
  node [
    id 108
    label "cecha"
  ]
  node [
    id 109
    label "czarny_punkt"
  ]
  node [
    id 110
    label "zagrozi&#263;"
  ]
  node [
    id 111
    label "si&#281;ga&#263;"
  ]
  node [
    id 112
    label "trwa&#263;"
  ]
  node [
    id 113
    label "obecno&#347;&#263;"
  ]
  node [
    id 114
    label "stan"
  ]
  node [
    id 115
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 116
    label "stand"
  ]
  node [
    id 117
    label "mie&#263;_miejsce"
  ]
  node [
    id 118
    label "uczestniczy&#263;"
  ]
  node [
    id 119
    label "chodzi&#263;"
  ]
  node [
    id 120
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 121
    label "equal"
  ]
  node [
    id 122
    label "ma&#322;o"
  ]
  node [
    id 123
    label "nielicznie"
  ]
  node [
    id 124
    label "ma&#322;y"
  ]
  node [
    id 125
    label "niewa&#380;ny"
  ]
  node [
    id 126
    label "autonomy"
  ]
  node [
    id 127
    label "organ"
  ]
  node [
    id 128
    label "zap&#322;aci&#263;"
  ]
  node [
    id 129
    label "salarium"
  ]
  node [
    id 130
    label "kategoria_urz&#281;dnicza"
  ]
  node [
    id 131
    label "szko&#322;a"
  ]
  node [
    id 132
    label "salariat"
  ]
  node [
    id 133
    label "troch&#281;"
  ]
  node [
    id 134
    label "dawny"
  ]
  node [
    id 135
    label "rozw&#243;d"
  ]
  node [
    id 136
    label "rozwodzi&#263;_si&#281;"
  ]
  node [
    id 137
    label "eksprezydent"
  ]
  node [
    id 138
    label "rozwie&#347;&#263;_si&#281;"
  ]
  node [
    id 139
    label "partner"
  ]
  node [
    id 140
    label "rozwiedzenie_si&#281;"
  ]
  node [
    id 141
    label "rozwodzenie_si&#281;"
  ]
  node [
    id 142
    label "wcze&#347;niejszy"
  ]
  node [
    id 143
    label "marny"
  ]
  node [
    id 144
    label "wstydliwy"
  ]
  node [
    id 145
    label "nieznaczny"
  ]
  node [
    id 146
    label "gorszy"
  ]
  node [
    id 147
    label "bliski"
  ]
  node [
    id 148
    label "s&#322;aby"
  ]
  node [
    id 149
    label "obni&#380;enie"
  ]
  node [
    id 150
    label "nisko"
  ]
  node [
    id 151
    label "n&#281;dznie"
  ]
  node [
    id 152
    label "po&#347;ledni"
  ]
  node [
    id 153
    label "uni&#380;ony"
  ]
  node [
    id 154
    label "pospolity"
  ]
  node [
    id 155
    label "obni&#380;anie"
  ]
  node [
    id 156
    label "pomierny"
  ]
  node [
    id 157
    label "zwi&#281;kszy&#263;"
  ]
  node [
    id 158
    label "better"
  ]
  node [
    id 159
    label "bro&#324;_miotaj&#261;ca"
  ]
  node [
    id 160
    label "zabawka"
  ]
  node [
    id 161
    label "bro&#324;_my&#347;liwska"
  ]
  node [
    id 162
    label "bro&#324;"
  ]
  node [
    id 163
    label "catapult"
  ]
  node [
    id 164
    label "urwis"
  ]
  node [
    id 165
    label "miejsce"
  ]
  node [
    id 166
    label "abstrakcja"
  ]
  node [
    id 167
    label "punkt"
  ]
  node [
    id 168
    label "substancja"
  ]
  node [
    id 169
    label "spos&#243;b"
  ]
  node [
    id 170
    label "chemikalia"
  ]
  node [
    id 171
    label "sta&#263;_si&#281;"
  ]
  node [
    id 172
    label "appoint"
  ]
  node [
    id 173
    label "zrobi&#263;"
  ]
  node [
    id 174
    label "ustali&#263;"
  ]
  node [
    id 175
    label "oblat"
  ]
  node [
    id 176
    label "liczba"
  ]
  node [
    id 177
    label "miljon"
  ]
  node [
    id 178
    label "ba&#324;ka"
  ]
  node [
    id 179
    label "szlachetny"
  ]
  node [
    id 180
    label "metaliczny"
  ]
  node [
    id 181
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 182
    label "z&#322;ocenie"
  ]
  node [
    id 183
    label "grosz"
  ]
  node [
    id 184
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 185
    label "utytu&#322;owany"
  ]
  node [
    id 186
    label "poz&#322;ocenie"
  ]
  node [
    id 187
    label "Polska"
  ]
  node [
    id 188
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 189
    label "wspania&#322;y"
  ]
  node [
    id 190
    label "doskona&#322;y"
  ]
  node [
    id 191
    label "kochany"
  ]
  node [
    id 192
    label "jednostka_monetarna"
  ]
  node [
    id 193
    label "z&#322;oci&#347;cie"
  ]
  node [
    id 194
    label "wiela"
  ]
  node [
    id 195
    label "du&#380;y"
  ]
  node [
    id 196
    label "poda&#263;"
  ]
  node [
    id 197
    label "obiecywa&#263;"
  ]
  node [
    id 198
    label "obieca&#263;"
  ]
  node [
    id 199
    label "bespeak"
  ]
  node [
    id 200
    label "sign"
  ]
  node [
    id 201
    label "podawa&#263;"
  ]
  node [
    id 202
    label "zapewni&#263;"
  ]
  node [
    id 203
    label "manewr"
  ]
  node [
    id 204
    label "byt"
  ]
  node [
    id 205
    label "zachowa&#263;"
  ]
  node [
    id 206
    label "zdo&#322;a&#263;"
  ]
  node [
    id 207
    label "op&#322;aci&#263;"
  ]
  node [
    id 208
    label "foster"
  ]
  node [
    id 209
    label "feed"
  ]
  node [
    id 210
    label "podtrzyma&#263;"
  ]
  node [
    id 211
    label "preserve"
  ]
  node [
    id 212
    label "obroni&#263;"
  ]
  node [
    id 213
    label "przetrzyma&#263;"
  ]
  node [
    id 214
    label "potrzyma&#263;"
  ]
  node [
    id 215
    label "unie&#347;&#263;"
  ]
  node [
    id 216
    label "wysoko&#347;&#263;"
  ]
  node [
    id 217
    label "faza"
  ]
  node [
    id 218
    label "szczebel"
  ]
  node [
    id 219
    label "po&#322;o&#380;enie"
  ]
  node [
    id 220
    label "kierunek"
  ]
  node [
    id 221
    label "wyk&#322;adnik"
  ]
  node [
    id 222
    label "budynek"
  ]
  node [
    id 223
    label "punkt_widzenia"
  ]
  node [
    id 224
    label "jako&#347;&#263;"
  ]
  node [
    id 225
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 226
    label "ranga"
  ]
  node [
    id 227
    label "p&#322;aszczyzna"
  ]
  node [
    id 228
    label "Leszek"
  ]
  node [
    id 229
    label "&#346;witalski"
  ]
  node [
    id 230
    label "stary"
  ]
  node [
    id 231
    label "Bogaczowice"
  ]
  node [
    id 232
    label "Ryszarda"
  ]
  node [
    id 233
    label "Legutko"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 17
  ]
  edge [
    source 7
    target 34
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 17
  ]
  edge [
    source 8
    target 18
  ]
  edge [
    source 8
    target 24
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 32
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 12
    target 107
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 12
    target 109
  ]
  edge [
    source 12
    target 110
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 13
    target 113
  ]
  edge [
    source 13
    target 114
  ]
  edge [
    source 13
    target 115
  ]
  edge [
    source 13
    target 116
  ]
  edge [
    source 13
    target 117
  ]
  edge [
    source 13
    target 118
  ]
  edge [
    source 13
    target 119
  ]
  edge [
    source 13
    target 120
  ]
  edge [
    source 13
    target 121
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 122
  ]
  edge [
    source 14
    target 123
  ]
  edge [
    source 14
    target 124
  ]
  edge [
    source 14
    target 125
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 21
  ]
  edge [
    source 15
    target 22
  ]
  edge [
    source 15
    target 126
  ]
  edge [
    source 15
    target 127
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 128
  ]
  edge [
    source 16
    target 24
  ]
  edge [
    source 16
    target 33
  ]
  edge [
    source 17
    target 23
  ]
  edge [
    source 17
    target 129
  ]
  edge [
    source 17
    target 130
  ]
  edge [
    source 17
    target 131
  ]
  edge [
    source 17
    target 132
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 133
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 134
  ]
  edge [
    source 19
    target 135
  ]
  edge [
    source 19
    target 136
  ]
  edge [
    source 19
    target 137
  ]
  edge [
    source 19
    target 138
  ]
  edge [
    source 19
    target 139
  ]
  edge [
    source 19
    target 140
  ]
  edge [
    source 19
    target 141
  ]
  edge [
    source 19
    target 142
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 143
  ]
  edge [
    source 20
    target 144
  ]
  edge [
    source 20
    target 145
  ]
  edge [
    source 20
    target 146
  ]
  edge [
    source 20
    target 147
  ]
  edge [
    source 20
    target 148
  ]
  edge [
    source 20
    target 149
  ]
  edge [
    source 20
    target 150
  ]
  edge [
    source 20
    target 151
  ]
  edge [
    source 20
    target 152
  ]
  edge [
    source 20
    target 153
  ]
  edge [
    source 20
    target 154
  ]
  edge [
    source 20
    target 155
  ]
  edge [
    source 20
    target 156
  ]
  edge [
    source 20
    target 124
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 157
  ]
  edge [
    source 23
    target 158
  ]
  edge [
    source 23
    target 29
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 159
  ]
  edge [
    source 24
    target 160
  ]
  edge [
    source 24
    target 161
  ]
  edge [
    source 24
    target 162
  ]
  edge [
    source 24
    target 163
  ]
  edge [
    source 24
    target 164
  ]
  edge [
    source 24
    target 33
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 165
  ]
  edge [
    source 26
    target 44
  ]
  edge [
    source 26
    target 166
  ]
  edge [
    source 26
    target 167
  ]
  edge [
    source 26
    target 168
  ]
  edge [
    source 26
    target 169
  ]
  edge [
    source 26
    target 170
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 171
  ]
  edge [
    source 27
    target 172
  ]
  edge [
    source 27
    target 173
  ]
  edge [
    source 27
    target 174
  ]
  edge [
    source 27
    target 175
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 176
  ]
  edge [
    source 29
    target 177
  ]
  edge [
    source 29
    target 178
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 179
  ]
  edge [
    source 30
    target 180
  ]
  edge [
    source 30
    target 181
  ]
  edge [
    source 30
    target 182
  ]
  edge [
    source 30
    target 183
  ]
  edge [
    source 30
    target 184
  ]
  edge [
    source 30
    target 185
  ]
  edge [
    source 30
    target 186
  ]
  edge [
    source 30
    target 187
  ]
  edge [
    source 30
    target 188
  ]
  edge [
    source 30
    target 189
  ]
  edge [
    source 30
    target 190
  ]
  edge [
    source 30
    target 191
  ]
  edge [
    source 30
    target 192
  ]
  edge [
    source 30
    target 193
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 194
  ]
  edge [
    source 31
    target 195
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 99
  ]
  edge [
    source 32
    target 196
  ]
  edge [
    source 32
    target 197
  ]
  edge [
    source 32
    target 198
  ]
  edge [
    source 32
    target 199
  ]
  edge [
    source 32
    target 200
  ]
  edge [
    source 32
    target 201
  ]
  edge [
    source 32
    target 202
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 203
  ]
  edge [
    source 33
    target 204
  ]
  edge [
    source 33
    target 205
  ]
  edge [
    source 33
    target 206
  ]
  edge [
    source 33
    target 207
  ]
  edge [
    source 33
    target 208
  ]
  edge [
    source 33
    target 209
  ]
  edge [
    source 33
    target 210
  ]
  edge [
    source 33
    target 211
  ]
  edge [
    source 33
    target 212
  ]
  edge [
    source 33
    target 213
  ]
  edge [
    source 33
    target 173
  ]
  edge [
    source 33
    target 214
  ]
  edge [
    source 33
    target 215
  ]
  edge [
    source 33
    target 202
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 129
  ]
  edge [
    source 34
    target 130
  ]
  edge [
    source 34
    target 132
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 36
    target 216
  ]
  edge [
    source 36
    target 217
  ]
  edge [
    source 36
    target 218
  ]
  edge [
    source 36
    target 219
  ]
  edge [
    source 36
    target 220
  ]
  edge [
    source 36
    target 221
  ]
  edge [
    source 36
    target 222
  ]
  edge [
    source 36
    target 223
  ]
  edge [
    source 36
    target 224
  ]
  edge [
    source 36
    target 225
  ]
  edge [
    source 36
    target 226
  ]
  edge [
    source 36
    target 227
  ]
  edge [
    source 228
    target 229
  ]
  edge [
    source 230
    target 231
  ]
  edge [
    source 232
    target 233
  ]
]
