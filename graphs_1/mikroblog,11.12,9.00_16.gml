graph [
  maxDegree 23
  minDegree 1
  meanDegree 2.1839080459770117
  density 0.025394279604383854
  graphCliqueNumber 6
  node [
    id 0
    label "stycze&#324;"
    origin "text"
  ]
  node [
    id 1
    label "dzia&#322;o"
    origin "text"
  ]
  node [
    id 2
    label "polska"
    origin "text"
  ]
  node [
    id 3
    label "publiczny"
    origin "text"
  ]
  node [
    id 4
    label "rejestr"
    origin "text"
  ]
  node [
    id 5
    label "sprawca"
    origin "text"
  ]
  node [
    id 6
    label "przest&#281;pstwo"
    origin "text"
  ]
  node [
    id 7
    label "t&#322;o"
    origin "text"
  ]
  node [
    id 8
    label "seksualny"
    origin "text"
  ]
  node [
    id 9
    label "Nowy_Rok"
  ]
  node [
    id 10
    label "miesi&#261;c"
  ]
  node [
    id 11
    label "bateria"
  ]
  node [
    id 12
    label "laweta"
  ]
  node [
    id 13
    label "bro&#324;_artyleryjska"
  ]
  node [
    id 14
    label "bro&#324;"
  ]
  node [
    id 15
    label "oporopowrotnik"
  ]
  node [
    id 16
    label "przedmuchiwacz"
  ]
  node [
    id 17
    label "artyleria"
  ]
  node [
    id 18
    label "waln&#261;&#263;"
  ]
  node [
    id 19
    label "bateria_artylerii"
  ]
  node [
    id 20
    label "cannon"
  ]
  node [
    id 21
    label "jawny"
  ]
  node [
    id 22
    label "upublicznienie"
  ]
  node [
    id 23
    label "upublicznianie"
  ]
  node [
    id 24
    label "publicznie"
  ]
  node [
    id 25
    label "wyliczanka"
  ]
  node [
    id 26
    label "skala"
  ]
  node [
    id 27
    label "catalog"
  ]
  node [
    id 28
    label "organy"
  ]
  node [
    id 29
    label "brzmienie"
  ]
  node [
    id 30
    label "stock"
  ]
  node [
    id 31
    label "procesor"
  ]
  node [
    id 32
    label "figurowa&#263;"
  ]
  node [
    id 33
    label "przycisk"
  ]
  node [
    id 34
    label "zbi&#243;r"
  ]
  node [
    id 35
    label "book"
  ]
  node [
    id 36
    label "urz&#261;dzenie"
  ]
  node [
    id 37
    label "uk&#322;ad_elektroniczny"
  ]
  node [
    id 38
    label "regestr"
  ]
  node [
    id 39
    label "pozycja"
  ]
  node [
    id 40
    label "tekst"
  ]
  node [
    id 41
    label "sumariusz"
  ]
  node [
    id 42
    label "cz&#322;owiek"
  ]
  node [
    id 43
    label "sprawiciel"
  ]
  node [
    id 44
    label "przest&#281;pczo&#347;&#263;"
  ]
  node [
    id 45
    label "brudny"
  ]
  node [
    id 46
    label "niesprawiedliwo&#347;&#263;"
  ]
  node [
    id 47
    label "sprawstwo"
  ]
  node [
    id 48
    label "crime"
  ]
  node [
    id 49
    label "melodia"
  ]
  node [
    id 50
    label "partia"
  ]
  node [
    id 51
    label "&#347;rodowisko"
  ]
  node [
    id 52
    label "obiekt"
  ]
  node [
    id 53
    label "ubarwienie"
  ]
  node [
    id 54
    label "lingwistyka_kognitywna"
  ]
  node [
    id 55
    label "gestaltyzm"
  ]
  node [
    id 56
    label "obraz"
  ]
  node [
    id 57
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 58
    label "pod&#322;o&#380;e"
  ]
  node [
    id 59
    label "informacja"
  ]
  node [
    id 60
    label "causal_agent"
  ]
  node [
    id 61
    label "dalszoplanowy"
  ]
  node [
    id 62
    label "layer"
  ]
  node [
    id 63
    label "plan"
  ]
  node [
    id 64
    label "warunki"
  ]
  node [
    id 65
    label "background"
  ]
  node [
    id 66
    label "p&#322;aszczyzna"
  ]
  node [
    id 67
    label "p&#322;ciowo"
  ]
  node [
    id 68
    label "seksualnie"
  ]
  node [
    id 69
    label "seksowny"
  ]
  node [
    id 70
    label "erotyczny"
  ]
  node [
    id 71
    label "na"
  ]
  node [
    id 72
    label "Dariusz"
  ]
  node [
    id 73
    label "buda"
  ]
  node [
    id 74
    label "nowa"
  ]
  node [
    id 75
    label "dw&#243;r"
  ]
  node [
    id 76
    label "Gda&#324;skie"
  ]
  node [
    id 77
    label "Laskowice"
  ]
  node [
    id 78
    label "wielki"
  ]
  node [
    id 79
    label "s&#261;d"
  ]
  node [
    id 80
    label "wojew&#243;dzki"
  ]
  node [
    id 81
    label "apelacyjny"
  ]
  node [
    id 82
    label "w"
  ]
  node [
    id 83
    label "gda&#324;ski"
  ]
  node [
    id 84
    label "zak&#322;ad"
  ]
  node [
    id 85
    label "karny"
  ]
  node [
    id 86
    label "okr&#281;gowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 4
    target 8
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 72
    target 73
  ]
  edge [
    source 74
    target 75
  ]
  edge [
    source 74
    target 76
  ]
  edge [
    source 75
    target 76
  ]
  edge [
    source 77
    target 78
  ]
  edge [
    source 79
    target 80
  ]
  edge [
    source 79
    target 81
  ]
  edge [
    source 79
    target 82
  ]
  edge [
    source 79
    target 83
  ]
  edge [
    source 79
    target 86
  ]
  edge [
    source 81
    target 82
  ]
  edge [
    source 81
    target 83
  ]
  edge [
    source 82
    target 83
  ]
  edge [
    source 84
    target 85
  ]
]
