graph [
  maxDegree 13
  minDegree 1
  meanDegree 1.7419354838709677
  density 0.05806451612903226
  graphCliqueNumber 3
  node [
    id 0
    label "powinienby&#263;"
    origin "text"
  ]
  node [
    id 1
    label "by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "tch&#243;rz"
    origin "text"
  ]
  node [
    id 3
    label "myersie"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;ga&#263;"
  ]
  node [
    id 5
    label "trwa&#263;"
  ]
  node [
    id 6
    label "obecno&#347;&#263;"
  ]
  node [
    id 7
    label "stan"
  ]
  node [
    id 8
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 9
    label "stand"
  ]
  node [
    id 10
    label "mie&#263;_miejsce"
  ]
  node [
    id 11
    label "uczestniczy&#263;"
  ]
  node [
    id 12
    label "chodzi&#263;"
  ]
  node [
    id 13
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 14
    label "equal"
  ]
  node [
    id 15
    label "&#322;asice_w&#322;a&#347;ciwe"
  ]
  node [
    id 16
    label "zwierz&#281;_&#322;owne"
  ]
  node [
    id 17
    label "zwierz&#281;_futerkowe"
  ]
  node [
    id 18
    label "ssak_drapie&#380;ny"
  ]
  node [
    id 19
    label "zwierzyna_drobna"
  ]
  node [
    id 20
    label "strachoput"
  ]
  node [
    id 21
    label "strachaj&#322;o"
  ]
  node [
    id 22
    label "Micha&#322;"
  ]
  node [
    id 23
    label "anio&#322;"
  ]
  node [
    id 24
    label "Benvenuto"
  ]
  node [
    id 25
    label "Cellini"
  ]
  node [
    id 26
    label "pika"
  ]
  node [
    id 27
    label "&#8217;"
  ]
  node [
    id 28
    label "em"
  ]
  node [
    id 29
    label "ludwik"
  ]
  node [
    id 30
    label "XIV"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 28
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 29
    target 30
  ]
]
