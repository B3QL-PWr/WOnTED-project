graph [
  maxDegree 29
  minDegree 1
  meanDegree 2
  density 0.019230769230769232
  graphCliqueNumber 2
  node [
    id 0
    label "jak"
    origin "text"
  ]
  node [
    id 1
    label "donosi&#263;"
    origin "text"
  ]
  node [
    id 2
    label "times"
    origin "text"
  ]
  node [
    id 3
    label "u&#380;ytkownik"
    origin "text"
  ]
  node [
    id 4
    label "umieszcza&#263;"
    origin "text"
  ]
  node [
    id 5
    label "swoje"
    origin "text"
  ]
  node [
    id 6
    label "profil"
    origin "text"
  ]
  node [
    id 7
    label "reklama"
    origin "text"
  ]
  node [
    id 8
    label "p&#322;aci&#263;"
    origin "text"
  ]
  node [
    id 9
    label "kanadyjski"
    origin "text"
  ]
  node [
    id 10
    label "firma"
    origin "text"
  ]
  node [
    id 11
    label "weblo"
    origin "text"
  ]
  node [
    id 12
    label "byd&#322;o"
  ]
  node [
    id 13
    label "zobo"
  ]
  node [
    id 14
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 15
    label "yakalo"
  ]
  node [
    id 16
    label "dzo"
  ]
  node [
    id 17
    label "zu&#380;y&#263;"
  ]
  node [
    id 18
    label "get"
  ]
  node [
    id 19
    label "render"
  ]
  node [
    id 20
    label "ci&#261;&#380;a"
  ]
  node [
    id 21
    label "informowa&#263;"
  ]
  node [
    id 22
    label "zanosi&#263;"
  ]
  node [
    id 23
    label "introduce"
  ]
  node [
    id 24
    label "do&#322;&#261;cza&#263;"
  ]
  node [
    id 25
    label "spill_the_beans"
  ]
  node [
    id 26
    label "give"
  ]
  node [
    id 27
    label "dosi&#281;ga&#263;"
  ]
  node [
    id 28
    label "inform"
  ]
  node [
    id 29
    label "przeby&#263;"
  ]
  node [
    id 30
    label "j&#281;zykowo"
  ]
  node [
    id 31
    label "podmiot"
  ]
  node [
    id 32
    label "zmienia&#263;"
  ]
  node [
    id 33
    label "plasowa&#263;"
  ]
  node [
    id 34
    label "prze&#322;adowywa&#263;"
  ]
  node [
    id 35
    label "pomieszcza&#263;"
  ]
  node [
    id 36
    label "wpiernicza&#263;"
  ]
  node [
    id 37
    label "robi&#263;"
  ]
  node [
    id 38
    label "accommodate"
  ]
  node [
    id 39
    label "umie&#347;ci&#263;"
  ]
  node [
    id 40
    label "venture"
  ]
  node [
    id 41
    label "powodowa&#263;"
  ]
  node [
    id 42
    label "okre&#347;la&#263;"
  ]
  node [
    id 43
    label "rzeczywisto&#347;&#263;_wirtualna"
  ]
  node [
    id 44
    label "seria"
  ]
  node [
    id 45
    label "twarz"
  ]
  node [
    id 46
    label "section"
  ]
  node [
    id 47
    label "listwa"
  ]
  node [
    id 48
    label "podgl&#261;d"
  ]
  node [
    id 49
    label "ozdoba"
  ]
  node [
    id 50
    label "dominanta"
  ]
  node [
    id 51
    label "obw&#243;dka"
  ]
  node [
    id 52
    label "faseta"
  ]
  node [
    id 53
    label "kontur"
  ]
  node [
    id 54
    label "profile"
  ]
  node [
    id 55
    label "konto"
  ]
  node [
    id 56
    label "przekr&#243;j"
  ]
  node [
    id 57
    label "awatar"
  ]
  node [
    id 58
    label "charakter"
  ]
  node [
    id 59
    label "element_konstrukcyjny"
  ]
  node [
    id 60
    label "sylwetka"
  ]
  node [
    id 61
    label "copywriting"
  ]
  node [
    id 62
    label "brief"
  ]
  node [
    id 63
    label "bran&#380;a"
  ]
  node [
    id 64
    label "informacja"
  ]
  node [
    id 65
    label "promowa&#263;"
  ]
  node [
    id 66
    label "akcja"
  ]
  node [
    id 67
    label "wypromowa&#263;"
  ]
  node [
    id 68
    label "samplowanie"
  ]
  node [
    id 69
    label "tekst"
  ]
  node [
    id 70
    label "buli&#263;"
  ]
  node [
    id 71
    label "osi&#261;ga&#263;"
  ]
  node [
    id 72
    label "wydawa&#263;"
  ]
  node [
    id 73
    label "pay"
  ]
  node [
    id 74
    label "po_kanadyjsku"
  ]
  node [
    id 75
    label "anglosaski"
  ]
  node [
    id 76
    label "p&#243;&#322;nocnoameryka&#324;ski"
  ]
  node [
    id 77
    label "ameryka&#324;ski"
  ]
  node [
    id 78
    label "cz&#322;owiek"
  ]
  node [
    id 79
    label "Hortex"
  ]
  node [
    id 80
    label "MAC"
  ]
  node [
    id 81
    label "reengineering"
  ]
  node [
    id 82
    label "nazwa_w&#322;asna"
  ]
  node [
    id 83
    label "podmiot_gospodarczy"
  ]
  node [
    id 84
    label "Google"
  ]
  node [
    id 85
    label "zaufanie"
  ]
  node [
    id 86
    label "biurowiec"
  ]
  node [
    id 87
    label "networking"
  ]
  node [
    id 88
    label "zasoby_ludzkie"
  ]
  node [
    id 89
    label "interes"
  ]
  node [
    id 90
    label "paczkarnia"
  ]
  node [
    id 91
    label "Canon"
  ]
  node [
    id 92
    label "HP"
  ]
  node [
    id 93
    label "Baltona"
  ]
  node [
    id 94
    label "Pewex"
  ]
  node [
    id 95
    label "MAN_SE"
  ]
  node [
    id 96
    label "Apeks"
  ]
  node [
    id 97
    label "zasoby"
  ]
  node [
    id 98
    label "Orbis"
  ]
  node [
    id 99
    label "miejsce_pracy"
  ]
  node [
    id 100
    label "siedziba"
  ]
  node [
    id 101
    label "Spo&#322;em"
  ]
  node [
    id 102
    label "przedsi&#281;biorczo&#347;&#263;"
  ]
  node [
    id 103
    label "Orlen"
  ]
  node [
    id 104
    label "klasa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 26
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 104
  ]
]
