graph [
  maxDegree 328
  minDegree 1
  meanDegree 2.6296296296296298
  density 0.003047079524483928
  graphCliqueNumber 3
  node [
    id 0
    label "szanowny"
    origin "text"
  ]
  node [
    id 1
    label "pa&#324;stwo"
    origin "text"
  ]
  node [
    id 2
    label "bardzo"
    origin "text"
  ]
  node [
    id 3
    label "cz&#281;sto"
    origin "text"
  ]
  node [
    id 4
    label "powtarza&#263;"
    origin "text"
  ]
  node [
    id 5
    label "si&#281;"
    origin "text"
  ]
  node [
    id 6
    label "pytanie"
    origin "text"
  ]
  node [
    id 7
    label "sk&#261;d"
    origin "text"
  ]
  node [
    id 8
    label "wzi&#261;&#263;"
    origin "text"
  ]
  node [
    id 9
    label "pieni&#261;dz"
    origin "text"
  ]
  node [
    id 10
    label "chcie&#263;"
    origin "text"
  ]
  node [
    id 11
    label "tylko"
    origin "text"
  ]
  node [
    id 12
    label "przybli&#380;y&#263;"
    origin "text"
  ]
  node [
    id 13
    label "wy&#347;cie"
    origin "text"
  ]
  node [
    id 14
    label "uchwala&#263;"
    origin "text"
  ]
  node [
    id 15
    label "bud&#380;et"
    origin "text"
  ]
  node [
    id 16
    label "ten"
    origin "text"
  ]
  node [
    id 17
    label "sala"
    origin "text"
  ]
  node [
    id 18
    label "ile"
    origin "text"
  ]
  node [
    id 19
    label "by&#263;"
    origin "text"
  ]
  node [
    id 20
    label "misja"
    origin "text"
  ]
  node [
    id 21
    label "pokojowa"
    origin "text"
  ]
  node [
    id 22
    label "wojna"
    origin "text"
  ]
  node [
    id 23
    label "irak"
    origin "text"
  ]
  node [
    id 24
    label "afganistan"
    origin "text"
  ]
  node [
    id 25
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 26
    label "dzisiaj"
    origin "text"
  ]
  node [
    id 27
    label "trzeba"
    origin "text"
  ]
  node [
    id 28
    label "zwraca&#263;"
    origin "text"
  ]
  node [
    id 29
    label "maj&#261;tek"
    origin "text"
  ]
  node [
    id 30
    label "tym"
    origin "text"
  ]
  node [
    id 31
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 32
    label "kraj"
    origin "text"
  ]
  node [
    id 33
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 34
    label "da&#263;"
    origin "text"
  ]
  node [
    id 35
    label "kilka"
    origin "text"
  ]
  node [
    id 36
    label "z&#322;oty"
    origin "text"
  ]
  node [
    id 37
    label "straci&#263;"
    origin "text"
  ]
  node [
    id 38
    label "zdrowie"
    origin "text"
  ]
  node [
    id 39
    label "zostawa&#263;"
    origin "text"
  ]
  node [
    id 40
    label "pracowa&#263;"
    origin "text"
  ]
  node [
    id 41
    label "dla"
    origin "text"
  ]
  node [
    id 42
    label "dobra"
    origin "text"
  ]
  node [
    id 43
    label "oklask"
    origin "text"
  ]
  node [
    id 44
    label "akceptowa&#263;"
    origin "text"
  ]
  node [
    id 45
    label "filozofia"
    origin "text"
  ]
  node [
    id 46
    label "by&#322;a"
    origin "text"
  ]
  node [
    id 47
    label "prezentowa&#263;"
    origin "text"
  ]
  node [
    id 48
    label "aby"
    origin "text"
  ]
  node [
    id 49
    label "emerytura"
    origin "text"
  ]
  node [
    id 50
    label "pomostowy"
    origin "text"
  ]
  node [
    id 51
    label "zabra&#263;"
    origin "text"
  ]
  node [
    id 52
    label "emeryt"
    origin "text"
  ]
  node [
    id 53
    label "zasi&#322;ek"
    origin "text"
  ]
  node [
    id 54
    label "pomoc"
    origin "text"
  ]
  node [
    id 55
    label "spo&#322;eczny"
    origin "text"
  ]
  node [
    id 56
    label "uwaga"
    origin "text"
  ]
  node [
    id 57
    label "nasi"
    origin "text"
  ]
  node [
    id 58
    label "projekt"
    origin "text"
  ]
  node [
    id 59
    label "uprawnienie"
    origin "text"
  ]
  node [
    id 60
    label "chodzi&#263;"
    origin "text"
  ]
  node [
    id 61
    label "nic"
    origin "text"
  ]
  node [
    id 62
    label "inny"
    origin "text"
  ]
  node [
    id 63
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 64
    label "potrzebowa&#263;"
    origin "text"
  ]
  node [
    id 65
    label "jak"
    origin "text"
  ]
  node [
    id 66
    label "bezpiecze&#324;stwo"
    origin "text"
  ]
  node [
    id 67
    label "m&#243;wi&#263;by&#263;"
    origin "text"
  ]
  node [
    id 68
    label "maja"
    origin "text"
  ]
  node [
    id 69
    label "tychy"
    origin "text"
  ]
  node [
    id 70
    label "szacownie"
  ]
  node [
    id 71
    label "Filipiny"
  ]
  node [
    id 72
    label "Rwanda"
  ]
  node [
    id 73
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 74
    label "Monako"
  ]
  node [
    id 75
    label "Korea"
  ]
  node [
    id 76
    label "Ghana"
  ]
  node [
    id 77
    label "Czarnog&#243;ra"
  ]
  node [
    id 78
    label "Malawi"
  ]
  node [
    id 79
    label "Indonezja"
  ]
  node [
    id 80
    label "Bu&#322;garia"
  ]
  node [
    id 81
    label "Nauru"
  ]
  node [
    id 82
    label "Kenia"
  ]
  node [
    id 83
    label "Kambod&#380;a"
  ]
  node [
    id 84
    label "Mali"
  ]
  node [
    id 85
    label "Austria"
  ]
  node [
    id 86
    label "interior"
  ]
  node [
    id 87
    label "Armenia"
  ]
  node [
    id 88
    label "Fid&#380;i"
  ]
  node [
    id 89
    label "Tuwalu"
  ]
  node [
    id 90
    label "Etiopia"
  ]
  node [
    id 91
    label "Malta"
  ]
  node [
    id 92
    label "Malezja"
  ]
  node [
    id 93
    label "Grenada"
  ]
  node [
    id 94
    label "Tad&#380;ykistan"
  ]
  node [
    id 95
    label "Wehrlen"
  ]
  node [
    id 96
    label "para"
  ]
  node [
    id 97
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 98
    label "Rumunia"
  ]
  node [
    id 99
    label "Maroko"
  ]
  node [
    id 100
    label "Bhutan"
  ]
  node [
    id 101
    label "S&#322;owacja"
  ]
  node [
    id 102
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 103
    label "Seszele"
  ]
  node [
    id 104
    label "Kuwejt"
  ]
  node [
    id 105
    label "Arabia_Saudyjska"
  ]
  node [
    id 106
    label "Ekwador"
  ]
  node [
    id 107
    label "Kanada"
  ]
  node [
    id 108
    label "Japonia"
  ]
  node [
    id 109
    label "ziemia"
  ]
  node [
    id 110
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 111
    label "Hiszpania"
  ]
  node [
    id 112
    label "Wyspy_Marshalla"
  ]
  node [
    id 113
    label "Botswana"
  ]
  node [
    id 114
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 115
    label "D&#380;ibuti"
  ]
  node [
    id 116
    label "grupa"
  ]
  node [
    id 117
    label "Wietnam"
  ]
  node [
    id 118
    label "Egipt"
  ]
  node [
    id 119
    label "Burkina_Faso"
  ]
  node [
    id 120
    label "Niemcy"
  ]
  node [
    id 121
    label "Khitai"
  ]
  node [
    id 122
    label "Macedonia"
  ]
  node [
    id 123
    label "Albania"
  ]
  node [
    id 124
    label "Madagaskar"
  ]
  node [
    id 125
    label "Bahrajn"
  ]
  node [
    id 126
    label "Jemen"
  ]
  node [
    id 127
    label "Lesoto"
  ]
  node [
    id 128
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 129
    label "Samoa"
  ]
  node [
    id 130
    label "Andora"
  ]
  node [
    id 131
    label "Chiny"
  ]
  node [
    id 132
    label "Cypr"
  ]
  node [
    id 133
    label "Wielka_Brytania"
  ]
  node [
    id 134
    label "Ukraina"
  ]
  node [
    id 135
    label "Paragwaj"
  ]
  node [
    id 136
    label "Trynidad_i_Tobago"
  ]
  node [
    id 137
    label "Libia"
  ]
  node [
    id 138
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 139
    label "Surinam"
  ]
  node [
    id 140
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 141
    label "Australia"
  ]
  node [
    id 142
    label "Nigeria"
  ]
  node [
    id 143
    label "Honduras"
  ]
  node [
    id 144
    label "Bangladesz"
  ]
  node [
    id 145
    label "Peru"
  ]
  node [
    id 146
    label "Kazachstan"
  ]
  node [
    id 147
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 148
    label "Irak"
  ]
  node [
    id 149
    label "holoarktyka"
  ]
  node [
    id 150
    label "USA"
  ]
  node [
    id 151
    label "Sudan"
  ]
  node [
    id 152
    label "Nepal"
  ]
  node [
    id 153
    label "San_Marino"
  ]
  node [
    id 154
    label "Burundi"
  ]
  node [
    id 155
    label "Dominikana"
  ]
  node [
    id 156
    label "Komory"
  ]
  node [
    id 157
    label "granica_pa&#324;stwa"
  ]
  node [
    id 158
    label "Gwatemala"
  ]
  node [
    id 159
    label "Antarktis"
  ]
  node [
    id 160
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 161
    label "Brunei"
  ]
  node [
    id 162
    label "Iran"
  ]
  node [
    id 163
    label "Zimbabwe"
  ]
  node [
    id 164
    label "Namibia"
  ]
  node [
    id 165
    label "Meksyk"
  ]
  node [
    id 166
    label "Kamerun"
  ]
  node [
    id 167
    label "zwrot"
  ]
  node [
    id 168
    label "Somalia"
  ]
  node [
    id 169
    label "Angola"
  ]
  node [
    id 170
    label "Gabon"
  ]
  node [
    id 171
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 172
    label "Mozambik"
  ]
  node [
    id 173
    label "Tajwan"
  ]
  node [
    id 174
    label "Tunezja"
  ]
  node [
    id 175
    label "Nowa_Zelandia"
  ]
  node [
    id 176
    label "Liban"
  ]
  node [
    id 177
    label "Jordania"
  ]
  node [
    id 178
    label "Tonga"
  ]
  node [
    id 179
    label "Czad"
  ]
  node [
    id 180
    label "Liberia"
  ]
  node [
    id 181
    label "Gwinea"
  ]
  node [
    id 182
    label "Belize"
  ]
  node [
    id 183
    label "&#321;otwa"
  ]
  node [
    id 184
    label "Syria"
  ]
  node [
    id 185
    label "Benin"
  ]
  node [
    id 186
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 187
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 188
    label "Dominika"
  ]
  node [
    id 189
    label "Antigua_i_Barbuda"
  ]
  node [
    id 190
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 191
    label "Hanower"
  ]
  node [
    id 192
    label "partia"
  ]
  node [
    id 193
    label "Afganistan"
  ]
  node [
    id 194
    label "Kiribati"
  ]
  node [
    id 195
    label "W&#322;ochy"
  ]
  node [
    id 196
    label "Szwajcaria"
  ]
  node [
    id 197
    label "Sahara_Zachodnia"
  ]
  node [
    id 198
    label "Chorwacja"
  ]
  node [
    id 199
    label "Tajlandia"
  ]
  node [
    id 200
    label "Salwador"
  ]
  node [
    id 201
    label "Bahamy"
  ]
  node [
    id 202
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 203
    label "S&#322;owenia"
  ]
  node [
    id 204
    label "Gambia"
  ]
  node [
    id 205
    label "Urugwaj"
  ]
  node [
    id 206
    label "Zair"
  ]
  node [
    id 207
    label "Erytrea"
  ]
  node [
    id 208
    label "Rosja"
  ]
  node [
    id 209
    label "Uganda"
  ]
  node [
    id 210
    label "Niger"
  ]
  node [
    id 211
    label "Mauritius"
  ]
  node [
    id 212
    label "Turkmenistan"
  ]
  node [
    id 213
    label "Turcja"
  ]
  node [
    id 214
    label "Irlandia"
  ]
  node [
    id 215
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 216
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 217
    label "Gwinea_Bissau"
  ]
  node [
    id 218
    label "Belgia"
  ]
  node [
    id 219
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 220
    label "Palau"
  ]
  node [
    id 221
    label "Barbados"
  ]
  node [
    id 222
    label "Chile"
  ]
  node [
    id 223
    label "Wenezuela"
  ]
  node [
    id 224
    label "W&#281;gry"
  ]
  node [
    id 225
    label "Argentyna"
  ]
  node [
    id 226
    label "Kolumbia"
  ]
  node [
    id 227
    label "Sierra_Leone"
  ]
  node [
    id 228
    label "Azerbejd&#380;an"
  ]
  node [
    id 229
    label "Kongo"
  ]
  node [
    id 230
    label "Pakistan"
  ]
  node [
    id 231
    label "Liechtenstein"
  ]
  node [
    id 232
    label "Nikaragua"
  ]
  node [
    id 233
    label "Senegal"
  ]
  node [
    id 234
    label "Indie"
  ]
  node [
    id 235
    label "Suazi"
  ]
  node [
    id 236
    label "Polska"
  ]
  node [
    id 237
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 238
    label "Algieria"
  ]
  node [
    id 239
    label "terytorium"
  ]
  node [
    id 240
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 241
    label "Jamajka"
  ]
  node [
    id 242
    label "Kostaryka"
  ]
  node [
    id 243
    label "Timor_Wschodni"
  ]
  node [
    id 244
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 245
    label "Kuba"
  ]
  node [
    id 246
    label "Mauretania"
  ]
  node [
    id 247
    label "Portoryko"
  ]
  node [
    id 248
    label "Brazylia"
  ]
  node [
    id 249
    label "Mo&#322;dawia"
  ]
  node [
    id 250
    label "organizacja"
  ]
  node [
    id 251
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 252
    label "Litwa"
  ]
  node [
    id 253
    label "Kirgistan"
  ]
  node [
    id 254
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 255
    label "Izrael"
  ]
  node [
    id 256
    label "Grecja"
  ]
  node [
    id 257
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 258
    label "Holandia"
  ]
  node [
    id 259
    label "Sri_Lanka"
  ]
  node [
    id 260
    label "Katar"
  ]
  node [
    id 261
    label "Mikronezja"
  ]
  node [
    id 262
    label "Mongolia"
  ]
  node [
    id 263
    label "Laos"
  ]
  node [
    id 264
    label "Malediwy"
  ]
  node [
    id 265
    label "Zambia"
  ]
  node [
    id 266
    label "Tanzania"
  ]
  node [
    id 267
    label "Gujana"
  ]
  node [
    id 268
    label "Czechy"
  ]
  node [
    id 269
    label "Panama"
  ]
  node [
    id 270
    label "Uzbekistan"
  ]
  node [
    id 271
    label "Gruzja"
  ]
  node [
    id 272
    label "Serbia"
  ]
  node [
    id 273
    label "Francja"
  ]
  node [
    id 274
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 275
    label "Togo"
  ]
  node [
    id 276
    label "Estonia"
  ]
  node [
    id 277
    label "Oman"
  ]
  node [
    id 278
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 279
    label "Portugalia"
  ]
  node [
    id 280
    label "Boliwia"
  ]
  node [
    id 281
    label "Luksemburg"
  ]
  node [
    id 282
    label "Haiti"
  ]
  node [
    id 283
    label "Wyspy_Salomona"
  ]
  node [
    id 284
    label "Birma"
  ]
  node [
    id 285
    label "Rodezja"
  ]
  node [
    id 286
    label "w_chuj"
  ]
  node [
    id 287
    label "cz&#281;stokro&#263;"
  ]
  node [
    id 288
    label "cz&#281;sty"
  ]
  node [
    id 289
    label "impart"
  ]
  node [
    id 290
    label "repeat"
  ]
  node [
    id 291
    label "robi&#263;"
  ]
  node [
    id 292
    label "podszkala&#263;_si&#281;"
  ]
  node [
    id 293
    label "podawa&#263;"
  ]
  node [
    id 294
    label "sprawa"
  ]
  node [
    id 295
    label "zadanie"
  ]
  node [
    id 296
    label "wypowied&#378;"
  ]
  node [
    id 297
    label "problemat"
  ]
  node [
    id 298
    label "rozpytywanie"
  ]
  node [
    id 299
    label "sprawdzian"
  ]
  node [
    id 300
    label "przes&#322;uchiwanie"
  ]
  node [
    id 301
    label "wypytanie"
  ]
  node [
    id 302
    label "zwracanie_si&#281;"
  ]
  node [
    id 303
    label "wypowiedzenie"
  ]
  node [
    id 304
    label "wywo&#322;ywanie"
  ]
  node [
    id 305
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 306
    label "problematyka"
  ]
  node [
    id 307
    label "question"
  ]
  node [
    id 308
    label "sprawdzanie"
  ]
  node [
    id 309
    label "odpowiadanie"
  ]
  node [
    id 310
    label "survey"
  ]
  node [
    id 311
    label "odpowiada&#263;"
  ]
  node [
    id 312
    label "egzaminowanie"
  ]
  node [
    id 313
    label "wej&#347;&#263;"
  ]
  node [
    id 314
    label "get"
  ]
  node [
    id 315
    label "wzi&#281;cie"
  ]
  node [
    id 316
    label "wyrucha&#263;"
  ]
  node [
    id 317
    label "uciec"
  ]
  node [
    id 318
    label "ruszy&#263;"
  ]
  node [
    id 319
    label "wygra&#263;"
  ]
  node [
    id 320
    label "obj&#261;&#263;"
  ]
  node [
    id 321
    label "zacz&#261;&#263;"
  ]
  node [
    id 322
    label "wyciupcia&#263;"
  ]
  node [
    id 323
    label "World_Health_Organization"
  ]
  node [
    id 324
    label "skorzysta&#263;"
  ]
  node [
    id 325
    label "pokona&#263;"
  ]
  node [
    id 326
    label "poczyta&#263;"
  ]
  node [
    id 327
    label "poruszy&#263;"
  ]
  node [
    id 328
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 329
    label "take"
  ]
  node [
    id 330
    label "aim"
  ]
  node [
    id 331
    label "arise"
  ]
  node [
    id 332
    label "u&#380;y&#263;"
  ]
  node [
    id 333
    label "zaatakowa&#263;"
  ]
  node [
    id 334
    label "receive"
  ]
  node [
    id 335
    label "uda&#263;_si&#281;"
  ]
  node [
    id 336
    label "dosta&#263;"
  ]
  node [
    id 337
    label "otrzyma&#263;"
  ]
  node [
    id 338
    label "obskoczy&#263;"
  ]
  node [
    id 339
    label "zaopatrzy&#263;_si&#281;"
  ]
  node [
    id 340
    label "zrobi&#263;"
  ]
  node [
    id 341
    label "bra&#263;"
  ]
  node [
    id 342
    label "nakaza&#263;"
  ]
  node [
    id 343
    label "chwyci&#263;"
  ]
  node [
    id 344
    label "przyj&#261;&#263;"
  ]
  node [
    id 345
    label "seize"
  ]
  node [
    id 346
    label "odziedziczy&#263;"
  ]
  node [
    id 347
    label "withdraw"
  ]
  node [
    id 348
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 349
    label "wch&#322;on&#261;&#263;"
  ]
  node [
    id 350
    label "poda&#380;_pieni&#261;dza"
  ]
  node [
    id 351
    label "zdewaluowa&#263;"
  ]
  node [
    id 352
    label "moniak"
  ]
  node [
    id 353
    label "wytw&#243;r"
  ]
  node [
    id 354
    label "zdewaluowanie"
  ]
  node [
    id 355
    label "jednostka_monetarna"
  ]
  node [
    id 356
    label "&#347;rodek_p&#322;atniczy"
  ]
  node [
    id 357
    label "numizmat"
  ]
  node [
    id 358
    label "rozmieni&#263;"
  ]
  node [
    id 359
    label "rozmienienie"
  ]
  node [
    id 360
    label "rozmienianie"
  ]
  node [
    id 361
    label "dewaluowanie"
  ]
  node [
    id 362
    label "nomina&#322;"
  ]
  node [
    id 363
    label "coin"
  ]
  node [
    id 364
    label "dewaluowa&#263;"
  ]
  node [
    id 365
    label "pieni&#261;dze"
  ]
  node [
    id 366
    label "znak_pieni&#281;&#380;ny"
  ]
  node [
    id 367
    label "rozmienia&#263;"
  ]
  node [
    id 368
    label "czu&#263;"
  ]
  node [
    id 369
    label "desire"
  ]
  node [
    id 370
    label "kcie&#263;"
  ]
  node [
    id 371
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 372
    label "prze&#322;o&#380;y&#263;"
  ]
  node [
    id 373
    label "approach"
  ]
  node [
    id 374
    label "set_about"
  ]
  node [
    id 375
    label "zapozna&#263;"
  ]
  node [
    id 376
    label "heat"
  ]
  node [
    id 377
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 378
    label "ustanawia&#263;"
  ]
  node [
    id 379
    label "carry"
  ]
  node [
    id 380
    label "wydatki_bud&#380;etowe"
  ]
  node [
    id 381
    label "etat"
  ]
  node [
    id 382
    label "portfel"
  ]
  node [
    id 383
    label "rezerwa_rewaluacyjna"
  ]
  node [
    id 384
    label "kwota"
  ]
  node [
    id 385
    label "okre&#347;lony"
  ]
  node [
    id 386
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 387
    label "zgromadzenie"
  ]
  node [
    id 388
    label "publiczno&#347;&#263;"
  ]
  node [
    id 389
    label "audience"
  ]
  node [
    id 390
    label "pomieszczenie"
  ]
  node [
    id 391
    label "si&#281;ga&#263;"
  ]
  node [
    id 392
    label "trwa&#263;"
  ]
  node [
    id 393
    label "obecno&#347;&#263;"
  ]
  node [
    id 394
    label "stan"
  ]
  node [
    id 395
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 396
    label "stand"
  ]
  node [
    id 397
    label "mie&#263;_miejsce"
  ]
  node [
    id 398
    label "uczestniczy&#263;"
  ]
  node [
    id 399
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 400
    label "equal"
  ]
  node [
    id 401
    label "plac&#243;wka"
  ]
  node [
    id 402
    label "misje"
  ]
  node [
    id 403
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 404
    label "obowi&#261;zek"
  ]
  node [
    id 405
    label "reprezentacja"
  ]
  node [
    id 406
    label "pracownik"
  ]
  node [
    id 407
    label "s&#322;u&#380;&#261;ca"
  ]
  node [
    id 408
    label "subretka"
  ]
  node [
    id 409
    label "zimna_wojna"
  ]
  node [
    id 410
    label "zbrodnia_przeciw_ludzko&#347;ci"
  ]
  node [
    id 411
    label "angaria"
  ]
  node [
    id 412
    label "wr&#243;g"
  ]
  node [
    id 413
    label "zbrodnia_przeciw_pokojowi"
  ]
  node [
    id 414
    label "walka"
  ]
  node [
    id 415
    label "war"
  ]
  node [
    id 416
    label "konflikt"
  ]
  node [
    id 417
    label "wojna_stuletnia"
  ]
  node [
    id 418
    label "burza"
  ]
  node [
    id 419
    label "zbrodnia_wojenna"
  ]
  node [
    id 420
    label "gra_w_karty"
  ]
  node [
    id 421
    label "sp&#243;r"
  ]
  node [
    id 422
    label "remark"
  ]
  node [
    id 423
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 424
    label "u&#380;ywa&#263;"
  ]
  node [
    id 425
    label "okre&#347;la&#263;"
  ]
  node [
    id 426
    label "j&#281;zyk"
  ]
  node [
    id 427
    label "say"
  ]
  node [
    id 428
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 429
    label "formu&#322;owa&#263;"
  ]
  node [
    id 430
    label "talk"
  ]
  node [
    id 431
    label "powiada&#263;"
  ]
  node [
    id 432
    label "informowa&#263;"
  ]
  node [
    id 433
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 434
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 435
    label "wydobywa&#263;"
  ]
  node [
    id 436
    label "express"
  ]
  node [
    id 437
    label "chew_the_fat"
  ]
  node [
    id 438
    label "dysfonia"
  ]
  node [
    id 439
    label "umie&#263;"
  ]
  node [
    id 440
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 441
    label "tell"
  ]
  node [
    id 442
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 443
    label "wyra&#380;a&#263;"
  ]
  node [
    id 444
    label "gaworzy&#263;"
  ]
  node [
    id 445
    label "rozmawia&#263;"
  ]
  node [
    id 446
    label "dziama&#263;"
  ]
  node [
    id 447
    label "prawi&#263;"
  ]
  node [
    id 448
    label "doba"
  ]
  node [
    id 449
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 450
    label "dzi&#347;"
  ]
  node [
    id 451
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 452
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 453
    label "trza"
  ]
  node [
    id 454
    label "necessity"
  ]
  node [
    id 455
    label "ustawia&#263;"
  ]
  node [
    id 456
    label "wydala&#263;"
  ]
  node [
    id 457
    label "give"
  ]
  node [
    id 458
    label "manipulate"
  ]
  node [
    id 459
    label "indicate"
  ]
  node [
    id 460
    label "przeznacza&#263;"
  ]
  node [
    id 461
    label "haftowa&#263;"
  ]
  node [
    id 462
    label "przekazywa&#263;"
  ]
  node [
    id 463
    label "rodowo&#347;&#263;"
  ]
  node [
    id 464
    label "frymark"
  ]
  node [
    id 465
    label "possession"
  ]
  node [
    id 466
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 467
    label "patent"
  ]
  node [
    id 468
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 469
    label "mienie"
  ]
  node [
    id 470
    label "przej&#347;&#263;"
  ]
  node [
    id 471
    label "w&#322;asno&#347;&#263;"
  ]
  node [
    id 472
    label "Wilko"
  ]
  node [
    id 473
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 474
    label "przej&#347;cie"
  ]
  node [
    id 475
    label "rezultat"
  ]
  node [
    id 476
    label "Skandynawia"
  ]
  node [
    id 477
    label "Kaukaz"
  ]
  node [
    id 478
    label "Kaszmir"
  ]
  node [
    id 479
    label "Toskania"
  ]
  node [
    id 480
    label "Yorkshire"
  ]
  node [
    id 481
    label "&#321;emkowszczyzna"
  ]
  node [
    id 482
    label "obszar"
  ]
  node [
    id 483
    label "Amhara"
  ]
  node [
    id 484
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 485
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 486
    label "Lombardia"
  ]
  node [
    id 487
    label "Kalabria"
  ]
  node [
    id 488
    label "Tyrol"
  ]
  node [
    id 489
    label "Pamir"
  ]
  node [
    id 490
    label "Lubelszczyzna"
  ]
  node [
    id 491
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 492
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 493
    label "&#379;ywiecczyzna"
  ]
  node [
    id 494
    label "Europa_Wschodnia"
  ]
  node [
    id 495
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 496
    label "Zabajkale"
  ]
  node [
    id 497
    label "Kaszuby"
  ]
  node [
    id 498
    label "Bo&#347;nia"
  ]
  node [
    id 499
    label "Noworosja"
  ]
  node [
    id 500
    label "Ba&#322;kany"
  ]
  node [
    id 501
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 502
    label "Anglia"
  ]
  node [
    id 503
    label "Kielecczyzna"
  ]
  node [
    id 504
    label "Pomorze_Zachodnie"
  ]
  node [
    id 505
    label "Opolskie"
  ]
  node [
    id 506
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 507
    label "Ko&#322;yma"
  ]
  node [
    id 508
    label "Oksytania"
  ]
  node [
    id 509
    label "Syjon"
  ]
  node [
    id 510
    label "Kociewie"
  ]
  node [
    id 511
    label "Huculszczyzna"
  ]
  node [
    id 512
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 513
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 514
    label "Bawaria"
  ]
  node [
    id 515
    label "Maghreb"
  ]
  node [
    id 516
    label "Bory_Tucholskie"
  ]
  node [
    id 517
    label "Europa_Zachodnia"
  ]
  node [
    id 518
    label "Kerala"
  ]
  node [
    id 519
    label "Podhale"
  ]
  node [
    id 520
    label "Kabylia"
  ]
  node [
    id 521
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 522
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 523
    label "Ma&#322;opolska"
  ]
  node [
    id 524
    label "Polesie"
  ]
  node [
    id 525
    label "Liguria"
  ]
  node [
    id 526
    label "&#321;&#243;dzkie"
  ]
  node [
    id 527
    label "Palestyna"
  ]
  node [
    id 528
    label "Bojkowszczyzna"
  ]
  node [
    id 529
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 530
    label "Karaiby"
  ]
  node [
    id 531
    label "S&#261;decczyzna"
  ]
  node [
    id 532
    label "Sand&#380;ak"
  ]
  node [
    id 533
    label "Nadrenia"
  ]
  node [
    id 534
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 535
    label "Zakarpacie"
  ]
  node [
    id 536
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 537
    label "Zag&#243;rze"
  ]
  node [
    id 538
    label "Andaluzja"
  ]
  node [
    id 539
    label "Turkiestan"
  ]
  node [
    id 540
    label "Naddniestrze"
  ]
  node [
    id 541
    label "Hercegowina"
  ]
  node [
    id 542
    label "jednostka_administracyjna"
  ]
  node [
    id 543
    label "Lotaryngia"
  ]
  node [
    id 544
    label "Opolszczyzna"
  ]
  node [
    id 545
    label "Afryka_Wschodnia"
  ]
  node [
    id 546
    label "Szlezwik"
  ]
  node [
    id 547
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 548
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 549
    label "Podbeskidzie"
  ]
  node [
    id 550
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 551
    label "Mazowsze"
  ]
  node [
    id 552
    label "Afryka_Zachodnia"
  ]
  node [
    id 553
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 554
    label "Galicja"
  ]
  node [
    id 555
    label "Szkocja"
  ]
  node [
    id 556
    label "Walia"
  ]
  node [
    id 557
    label "Powi&#347;le"
  ]
  node [
    id 558
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 559
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 560
    label "Zamojszczyzna"
  ]
  node [
    id 561
    label "Kujawy"
  ]
  node [
    id 562
    label "Podlasie"
  ]
  node [
    id 563
    label "Laponia"
  ]
  node [
    id 564
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 565
    label "Umbria"
  ]
  node [
    id 566
    label "Mezoameryka"
  ]
  node [
    id 567
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 568
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 569
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 570
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 571
    label "Kurdystan"
  ]
  node [
    id 572
    label "Kampania"
  ]
  node [
    id 573
    label "Armagnac"
  ]
  node [
    id 574
    label "Polinezja"
  ]
  node [
    id 575
    label "Warmia"
  ]
  node [
    id 576
    label "Wielkopolska"
  ]
  node [
    id 577
    label "brzeg"
  ]
  node [
    id 578
    label "Bordeaux"
  ]
  node [
    id 579
    label "Lauda"
  ]
  node [
    id 580
    label "Mazury"
  ]
  node [
    id 581
    label "Oceania"
  ]
  node [
    id 582
    label "Podkarpacie"
  ]
  node [
    id 583
    label "Lasko"
  ]
  node [
    id 584
    label "Amazonia"
  ]
  node [
    id 585
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 586
    label "Kurpie"
  ]
  node [
    id 587
    label "Tonkin"
  ]
  node [
    id 588
    label "Azja_Wschodnia"
  ]
  node [
    id 589
    label "Ukraina_Zachodnia"
  ]
  node [
    id 590
    label "Turyngia"
  ]
  node [
    id 591
    label "Baszkiria"
  ]
  node [
    id 592
    label "Apulia"
  ]
  node [
    id 593
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 594
    label "Indochiny"
  ]
  node [
    id 595
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 596
    label "Biskupizna"
  ]
  node [
    id 597
    label "Lubuskie"
  ]
  node [
    id 598
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 599
    label "czyj&#347;"
  ]
  node [
    id 600
    label "m&#261;&#380;"
  ]
  node [
    id 601
    label "dostarczy&#263;"
  ]
  node [
    id 602
    label "obieca&#263;"
  ]
  node [
    id 603
    label "pozwoli&#263;"
  ]
  node [
    id 604
    label "przeznaczy&#263;"
  ]
  node [
    id 605
    label "doda&#263;"
  ]
  node [
    id 606
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 607
    label "wyrzec_si&#281;"
  ]
  node [
    id 608
    label "supply"
  ]
  node [
    id 609
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 610
    label "zada&#263;"
  ]
  node [
    id 611
    label "odst&#261;pi&#263;"
  ]
  node [
    id 612
    label "feed"
  ]
  node [
    id 613
    label "testify"
  ]
  node [
    id 614
    label "powierzy&#263;"
  ]
  node [
    id 615
    label "convey"
  ]
  node [
    id 616
    label "przekaza&#263;"
  ]
  node [
    id 617
    label "zap&#322;aci&#263;"
  ]
  node [
    id 618
    label "dress"
  ]
  node [
    id 619
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 620
    label "udost&#281;pni&#263;"
  ]
  node [
    id 621
    label "sztachn&#261;&#263;"
  ]
  node [
    id 622
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 623
    label "przywali&#263;"
  ]
  node [
    id 624
    label "rap"
  ]
  node [
    id 625
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 626
    label "picture"
  ]
  node [
    id 627
    label "&#347;ledziowate"
  ]
  node [
    id 628
    label "ryba"
  ]
  node [
    id 629
    label "szlachetny"
  ]
  node [
    id 630
    label "metaliczny"
  ]
  node [
    id 631
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 632
    label "z&#322;ocenie"
  ]
  node [
    id 633
    label "grosz"
  ]
  node [
    id 634
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 635
    label "utytu&#322;owany"
  ]
  node [
    id 636
    label "poz&#322;ocenie"
  ]
  node [
    id 637
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 638
    label "wspania&#322;y"
  ]
  node [
    id 639
    label "doskona&#322;y"
  ]
  node [
    id 640
    label "kochany"
  ]
  node [
    id 641
    label "z&#322;oci&#347;cie"
  ]
  node [
    id 642
    label "omin&#261;&#263;"
  ]
  node [
    id 643
    label "spowodowa&#263;"
  ]
  node [
    id 644
    label "leave_office"
  ]
  node [
    id 645
    label "forfeit"
  ]
  node [
    id 646
    label "stracenie"
  ]
  node [
    id 647
    label "execute"
  ]
  node [
    id 648
    label "pogorszy&#263;_si&#281;"
  ]
  node [
    id 649
    label "zabi&#263;"
  ]
  node [
    id 650
    label "wytraci&#263;"
  ]
  node [
    id 651
    label "liszy&#263;_si&#281;"
  ]
  node [
    id 652
    label "przegra&#263;"
  ]
  node [
    id 653
    label "waste"
  ]
  node [
    id 654
    label "os&#322;abia&#263;"
  ]
  node [
    id 655
    label "niszczy&#263;"
  ]
  node [
    id 656
    label "zniszczy&#263;"
  ]
  node [
    id 657
    label "firmness"
  ]
  node [
    id 658
    label "kondycja"
  ]
  node [
    id 659
    label "os&#322;abi&#263;"
  ]
  node [
    id 660
    label "rozsypanie_si&#281;"
  ]
  node [
    id 661
    label "zdarcie"
  ]
  node [
    id 662
    label "cecha"
  ]
  node [
    id 663
    label "zniszczenie"
  ]
  node [
    id 664
    label "zedrze&#263;"
  ]
  node [
    id 665
    label "zdrowotno&#347;&#263;"
  ]
  node [
    id 666
    label "niszczenie"
  ]
  node [
    id 667
    label "os&#322;abienie"
  ]
  node [
    id 668
    label "soundness"
  ]
  node [
    id 669
    label "os&#322;abianie"
  ]
  node [
    id 670
    label "pozostawa&#263;"
  ]
  node [
    id 671
    label "zmienia&#263;_si&#281;"
  ]
  node [
    id 672
    label "ostawa&#263;_si&#281;"
  ]
  node [
    id 673
    label "stop"
  ]
  node [
    id 674
    label "przebywa&#263;"
  ]
  node [
    id 675
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 676
    label "blend"
  ]
  node [
    id 677
    label "zachowywa&#263;_si&#281;"
  ]
  node [
    id 678
    label "change"
  ]
  node [
    id 679
    label "endeavor"
  ]
  node [
    id 680
    label "funkcjonowa&#263;"
  ]
  node [
    id 681
    label "dobiega&#263;_si&#281;"
  ]
  node [
    id 682
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 683
    label "dzia&#322;a&#263;"
  ]
  node [
    id 684
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 685
    label "work"
  ]
  node [
    id 686
    label "bangla&#263;"
  ]
  node [
    id 687
    label "do"
  ]
  node [
    id 688
    label "maszyna"
  ]
  node [
    id 689
    label "tryb"
  ]
  node [
    id 690
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 691
    label "praca"
  ]
  node [
    id 692
    label "podejmowa&#263;"
  ]
  node [
    id 693
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 694
    label "commodity"
  ]
  node [
    id 695
    label "centym"
  ]
  node [
    id 696
    label "oklaski"
  ]
  node [
    id 697
    label "kla&#347;ni&#281;cie"
  ]
  node [
    id 698
    label "zezwala&#263;"
  ]
  node [
    id 699
    label "godzi&#263;_si&#281;"
  ]
  node [
    id 700
    label "assent"
  ]
  node [
    id 701
    label "authorize"
  ]
  node [
    id 702
    label "uznawa&#263;"
  ]
  node [
    id 703
    label "trudno&#347;&#263;"
  ]
  node [
    id 704
    label "spos&#243;b"
  ]
  node [
    id 705
    label "semiotyka"
  ]
  node [
    id 706
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 707
    label "logika"
  ]
  node [
    id 708
    label "przedmiot"
  ]
  node [
    id 709
    label "kierunek"
  ]
  node [
    id 710
    label "podstawa"
  ]
  node [
    id 711
    label "konwersja"
  ]
  node [
    id 712
    label "political_orientation"
  ]
  node [
    id 713
    label "aintelektualizm"
  ]
  node [
    id 714
    label "aksjologia"
  ]
  node [
    id 715
    label "ekofilozofia"
  ]
  node [
    id 716
    label "kreatyzm"
  ]
  node [
    id 717
    label "my&#347;licielstwo"
  ]
  node [
    id 718
    label "prajednia"
  ]
  node [
    id 719
    label "nauka_humanistyczna"
  ]
  node [
    id 720
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 721
    label "eudajmonia"
  ]
  node [
    id 722
    label "partnerka"
  ]
  node [
    id 723
    label "zapoznawa&#263;"
  ]
  node [
    id 724
    label "przedstawia&#263;"
  ]
  node [
    id 725
    label "present"
  ]
  node [
    id 726
    label "gra&#263;"
  ]
  node [
    id 727
    label "uprzedza&#263;"
  ]
  node [
    id 728
    label "represent"
  ]
  node [
    id 729
    label "program"
  ]
  node [
    id 730
    label "attest"
  ]
  node [
    id 731
    label "display"
  ]
  node [
    id 732
    label "troch&#281;"
  ]
  node [
    id 733
    label "czas"
  ]
  node [
    id 734
    label "ubezpieczenie_emerytalne"
  ]
  node [
    id 735
    label "&#347;wiadczenie_spo&#322;eczne"
  ]
  node [
    id 736
    label "egzystencja"
  ]
  node [
    id 737
    label "retirement"
  ]
  node [
    id 738
    label "przej&#347;ciowy"
  ]
  node [
    id 739
    label "consume"
  ]
  node [
    id 740
    label "zaj&#261;&#263;"
  ]
  node [
    id 741
    label "przenie&#347;&#263;"
  ]
  node [
    id 742
    label "z&#322;apa&#263;"
  ]
  node [
    id 743
    label "przesun&#261;&#263;"
  ]
  node [
    id 744
    label "deprive"
  ]
  node [
    id 745
    label "poci&#261;gn&#261;&#263;"
  ]
  node [
    id 746
    label "abstract"
  ]
  node [
    id 747
    label "doprowadzi&#263;"
  ]
  node [
    id 748
    label "oldboj"
  ]
  node [
    id 749
    label "starszy_cz&#322;owiek"
  ]
  node [
    id 750
    label "niepracuj&#261;cy"
  ]
  node [
    id 751
    label "&#347;wiadczeniobiorca"
  ]
  node [
    id 752
    label "zapomoga"
  ]
  node [
    id 753
    label "zgodzi&#263;"
  ]
  node [
    id 754
    label "pomocnik"
  ]
  node [
    id 755
    label "doch&#243;d"
  ]
  node [
    id 756
    label "property"
  ]
  node [
    id 757
    label "telefon_zaufania"
  ]
  node [
    id 758
    label "darowizna"
  ]
  node [
    id 759
    label "&#347;rodek"
  ]
  node [
    id 760
    label "liga"
  ]
  node [
    id 761
    label "niepubliczny"
  ]
  node [
    id 762
    label "spo&#322;ecznie"
  ]
  node [
    id 763
    label "publiczny"
  ]
  node [
    id 764
    label "nagana"
  ]
  node [
    id 765
    label "dzienniczek"
  ]
  node [
    id 766
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 767
    label "wzgl&#261;d"
  ]
  node [
    id 768
    label "gossip"
  ]
  node [
    id 769
    label "upomnienie"
  ]
  node [
    id 770
    label "tekst"
  ]
  node [
    id 771
    label "dokument"
  ]
  node [
    id 772
    label "device"
  ]
  node [
    id 773
    label "program_u&#380;ytkowy"
  ]
  node [
    id 774
    label "intencja"
  ]
  node [
    id 775
    label "agreement"
  ]
  node [
    id 776
    label "pomys&#322;"
  ]
  node [
    id 777
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 778
    label "plan"
  ]
  node [
    id 779
    label "dokumentacja"
  ]
  node [
    id 780
    label "czynno&#347;&#263;"
  ]
  node [
    id 781
    label "spowodowanie"
  ]
  node [
    id 782
    label "title"
  ]
  node [
    id 783
    label "law"
  ]
  node [
    id 784
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 785
    label "authorization"
  ]
  node [
    id 786
    label "proceed"
  ]
  node [
    id 787
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 788
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 789
    label "run"
  ]
  node [
    id 790
    label "p&#322;ywa&#263;"
  ]
  node [
    id 791
    label "continue"
  ]
  node [
    id 792
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 793
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 794
    label "przebiega&#263;"
  ]
  node [
    id 795
    label "wk&#322;ada&#263;"
  ]
  node [
    id 796
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 797
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 798
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 799
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 800
    label "krok"
  ]
  node [
    id 801
    label "str&#243;j"
  ]
  node [
    id 802
    label "bywa&#263;"
  ]
  node [
    id 803
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 804
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 805
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 806
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 807
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 808
    label "stara&#263;_si&#281;"
  ]
  node [
    id 809
    label "miernota"
  ]
  node [
    id 810
    label "g&#243;wno"
  ]
  node [
    id 811
    label "love"
  ]
  node [
    id 812
    label "ilo&#347;&#263;"
  ]
  node [
    id 813
    label "brak"
  ]
  node [
    id 814
    label "ciura"
  ]
  node [
    id 815
    label "kolejny"
  ]
  node [
    id 816
    label "inaczej"
  ]
  node [
    id 817
    label "r&#243;&#380;ny"
  ]
  node [
    id 818
    label "inszy"
  ]
  node [
    id 819
    label "osobno"
  ]
  node [
    id 820
    label "asymilowa&#263;"
  ]
  node [
    id 821
    label "wapniak"
  ]
  node [
    id 822
    label "dwun&#243;g"
  ]
  node [
    id 823
    label "polifag"
  ]
  node [
    id 824
    label "wz&#243;r"
  ]
  node [
    id 825
    label "profanum"
  ]
  node [
    id 826
    label "hominid"
  ]
  node [
    id 827
    label "homo_sapiens"
  ]
  node [
    id 828
    label "nasada"
  ]
  node [
    id 829
    label "podw&#322;adny"
  ]
  node [
    id 830
    label "ludzko&#347;&#263;"
  ]
  node [
    id 831
    label "mikrokosmos"
  ]
  node [
    id 832
    label "portrecista"
  ]
  node [
    id 833
    label "duch"
  ]
  node [
    id 834
    label "g&#322;owa"
  ]
  node [
    id 835
    label "oddzia&#322;ywanie"
  ]
  node [
    id 836
    label "asymilowanie"
  ]
  node [
    id 837
    label "osoba"
  ]
  node [
    id 838
    label "figura"
  ]
  node [
    id 839
    label "Adam"
  ]
  node [
    id 840
    label "senior"
  ]
  node [
    id 841
    label "antropochoria"
  ]
  node [
    id 842
    label "posta&#263;"
  ]
  node [
    id 843
    label "zapotrzebowa&#263;"
  ]
  node [
    id 844
    label "need"
  ]
  node [
    id 845
    label "claim"
  ]
  node [
    id 846
    label "byd&#322;o"
  ]
  node [
    id 847
    label "zobo"
  ]
  node [
    id 848
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 849
    label "yakalo"
  ]
  node [
    id 850
    label "dzo"
  ]
  node [
    id 851
    label "BHP"
  ]
  node [
    id 852
    label "katapultowa&#263;"
  ]
  node [
    id 853
    label "ubezpiecza&#263;"
  ]
  node [
    id 854
    label "porz&#261;dek"
  ]
  node [
    id 855
    label "ubezpieczenie"
  ]
  node [
    id 856
    label "ubezpieczy&#263;"
  ]
  node [
    id 857
    label "safety"
  ]
  node [
    id 858
    label "katapultowanie"
  ]
  node [
    id 859
    label "ubezpieczanie"
  ]
  node [
    id 860
    label "test_zderzeniowy"
  ]
  node [
    id 861
    label "wedyzm"
  ]
  node [
    id 862
    label "energia"
  ]
  node [
    id 863
    label "buddyzm"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 238
  ]
  edge [
    source 1
    target 239
  ]
  edge [
    source 1
    target 240
  ]
  edge [
    source 1
    target 241
  ]
  edge [
    source 1
    target 242
  ]
  edge [
    source 1
    target 243
  ]
  edge [
    source 1
    target 244
  ]
  edge [
    source 1
    target 245
  ]
  edge [
    source 1
    target 246
  ]
  edge [
    source 1
    target 247
  ]
  edge [
    source 1
    target 248
  ]
  edge [
    source 1
    target 249
  ]
  edge [
    source 1
    target 250
  ]
  edge [
    source 1
    target 251
  ]
  edge [
    source 1
    target 252
  ]
  edge [
    source 1
    target 253
  ]
  edge [
    source 1
    target 254
  ]
  edge [
    source 1
    target 255
  ]
  edge [
    source 1
    target 256
  ]
  edge [
    source 1
    target 257
  ]
  edge [
    source 1
    target 258
  ]
  edge [
    source 1
    target 259
  ]
  edge [
    source 1
    target 260
  ]
  edge [
    source 1
    target 261
  ]
  edge [
    source 1
    target 262
  ]
  edge [
    source 1
    target 263
  ]
  edge [
    source 1
    target 264
  ]
  edge [
    source 1
    target 265
  ]
  edge [
    source 1
    target 266
  ]
  edge [
    source 1
    target 267
  ]
  edge [
    source 1
    target 268
  ]
  edge [
    source 1
    target 269
  ]
  edge [
    source 1
    target 270
  ]
  edge [
    source 1
    target 271
  ]
  edge [
    source 1
    target 272
  ]
  edge [
    source 1
    target 273
  ]
  edge [
    source 1
    target 274
  ]
  edge [
    source 1
    target 275
  ]
  edge [
    source 1
    target 276
  ]
  edge [
    source 1
    target 277
  ]
  edge [
    source 1
    target 278
  ]
  edge [
    source 1
    target 279
  ]
  edge [
    source 1
    target 280
  ]
  edge [
    source 1
    target 281
  ]
  edge [
    source 1
    target 282
  ]
  edge [
    source 1
    target 283
  ]
  edge [
    source 1
    target 284
  ]
  edge [
    source 1
    target 285
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 286
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 8
    target 313
  ]
  edge [
    source 8
    target 314
  ]
  edge [
    source 8
    target 315
  ]
  edge [
    source 8
    target 316
  ]
  edge [
    source 8
    target 317
  ]
  edge [
    source 8
    target 318
  ]
  edge [
    source 8
    target 319
  ]
  edge [
    source 8
    target 320
  ]
  edge [
    source 8
    target 321
  ]
  edge [
    source 8
    target 322
  ]
  edge [
    source 8
    target 323
  ]
  edge [
    source 8
    target 324
  ]
  edge [
    source 8
    target 325
  ]
  edge [
    source 8
    target 326
  ]
  edge [
    source 8
    target 327
  ]
  edge [
    source 8
    target 328
  ]
  edge [
    source 8
    target 329
  ]
  edge [
    source 8
    target 330
  ]
  edge [
    source 8
    target 331
  ]
  edge [
    source 8
    target 332
  ]
  edge [
    source 8
    target 333
  ]
  edge [
    source 8
    target 334
  ]
  edge [
    source 8
    target 335
  ]
  edge [
    source 8
    target 336
  ]
  edge [
    source 8
    target 337
  ]
  edge [
    source 8
    target 338
  ]
  edge [
    source 8
    target 339
  ]
  edge [
    source 8
    target 340
  ]
  edge [
    source 8
    target 341
  ]
  edge [
    source 8
    target 342
  ]
  edge [
    source 8
    target 343
  ]
  edge [
    source 8
    target 344
  ]
  edge [
    source 8
    target 345
  ]
  edge [
    source 8
    target 346
  ]
  edge [
    source 8
    target 347
  ]
  edge [
    source 8
    target 348
  ]
  edge [
    source 8
    target 349
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 40
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 49
  ]
  edge [
    source 9
    target 350
  ]
  edge [
    source 9
    target 351
  ]
  edge [
    source 9
    target 352
  ]
  edge [
    source 9
    target 353
  ]
  edge [
    source 9
    target 354
  ]
  edge [
    source 9
    target 355
  ]
  edge [
    source 9
    target 356
  ]
  edge [
    source 9
    target 357
  ]
  edge [
    source 9
    target 358
  ]
  edge [
    source 9
    target 359
  ]
  edge [
    source 9
    target 360
  ]
  edge [
    source 9
    target 361
  ]
  edge [
    source 9
    target 362
  ]
  edge [
    source 9
    target 363
  ]
  edge [
    source 9
    target 364
  ]
  edge [
    source 9
    target 365
  ]
  edge [
    source 9
    target 366
  ]
  edge [
    source 9
    target 367
  ]
  edge [
    source 9
    target 42
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 368
  ]
  edge [
    source 10
    target 369
  ]
  edge [
    source 10
    target 370
  ]
  edge [
    source 10
    target 64
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 30
  ]
  edge [
    source 11
    target 68
  ]
  edge [
    source 11
    target 63
  ]
  edge [
    source 11
    target 59
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 371
  ]
  edge [
    source 12
    target 372
  ]
  edge [
    source 12
    target 373
  ]
  edge [
    source 12
    target 374
  ]
  edge [
    source 12
    target 375
  ]
  edge [
    source 12
    target 376
  ]
  edge [
    source 12
    target 377
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 378
  ]
  edge [
    source 14
    target 379
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 380
  ]
  edge [
    source 15
    target 381
  ]
  edge [
    source 15
    target 382
  ]
  edge [
    source 15
    target 383
  ]
  edge [
    source 15
    target 384
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 385
  ]
  edge [
    source 16
    target 386
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 387
  ]
  edge [
    source 17
    target 388
  ]
  edge [
    source 17
    target 389
  ]
  edge [
    source 17
    target 390
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 391
  ]
  edge [
    source 19
    target 392
  ]
  edge [
    source 19
    target 393
  ]
  edge [
    source 19
    target 394
  ]
  edge [
    source 19
    target 395
  ]
  edge [
    source 19
    target 396
  ]
  edge [
    source 19
    target 397
  ]
  edge [
    source 19
    target 398
  ]
  edge [
    source 19
    target 60
  ]
  edge [
    source 19
    target 399
  ]
  edge [
    source 19
    target 400
  ]
  edge [
    source 19
    target 39
  ]
  edge [
    source 19
    target 64
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 401
  ]
  edge [
    source 20
    target 402
  ]
  edge [
    source 20
    target 295
  ]
  edge [
    source 20
    target 403
  ]
  edge [
    source 20
    target 404
  ]
  edge [
    source 20
    target 405
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 406
  ]
  edge [
    source 21
    target 407
  ]
  edge [
    source 21
    target 408
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 409
  ]
  edge [
    source 22
    target 410
  ]
  edge [
    source 22
    target 411
  ]
  edge [
    source 22
    target 412
  ]
  edge [
    source 22
    target 413
  ]
  edge [
    source 22
    target 414
  ]
  edge [
    source 22
    target 415
  ]
  edge [
    source 22
    target 416
  ]
  edge [
    source 22
    target 417
  ]
  edge [
    source 22
    target 418
  ]
  edge [
    source 22
    target 419
  ]
  edge [
    source 22
    target 420
  ]
  edge [
    source 22
    target 421
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 58
  ]
  edge [
    source 25
    target 59
  ]
  edge [
    source 25
    target 422
  ]
  edge [
    source 25
    target 423
  ]
  edge [
    source 25
    target 424
  ]
  edge [
    source 25
    target 425
  ]
  edge [
    source 25
    target 426
  ]
  edge [
    source 25
    target 427
  ]
  edge [
    source 25
    target 428
  ]
  edge [
    source 25
    target 429
  ]
  edge [
    source 25
    target 430
  ]
  edge [
    source 25
    target 431
  ]
  edge [
    source 25
    target 432
  ]
  edge [
    source 25
    target 433
  ]
  edge [
    source 25
    target 434
  ]
  edge [
    source 25
    target 435
  ]
  edge [
    source 25
    target 436
  ]
  edge [
    source 25
    target 437
  ]
  edge [
    source 25
    target 438
  ]
  edge [
    source 25
    target 439
  ]
  edge [
    source 25
    target 440
  ]
  edge [
    source 25
    target 441
  ]
  edge [
    source 25
    target 442
  ]
  edge [
    source 25
    target 443
  ]
  edge [
    source 25
    target 444
  ]
  edge [
    source 25
    target 445
  ]
  edge [
    source 25
    target 446
  ]
  edge [
    source 25
    target 447
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 448
  ]
  edge [
    source 26
    target 449
  ]
  edge [
    source 26
    target 450
  ]
  edge [
    source 26
    target 451
  ]
  edge [
    source 26
    target 452
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 50
  ]
  edge [
    source 27
    target 51
  ]
  edge [
    source 27
    target 52
  ]
  edge [
    source 27
    target 453
  ]
  edge [
    source 27
    target 454
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 55
  ]
  edge [
    source 28
    target 56
  ]
  edge [
    source 28
    target 455
  ]
  edge [
    source 28
    target 456
  ]
  edge [
    source 28
    target 457
  ]
  edge [
    source 28
    target 458
  ]
  edge [
    source 28
    target 459
  ]
  edge [
    source 28
    target 460
  ]
  edge [
    source 28
    target 461
  ]
  edge [
    source 28
    target 462
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 463
  ]
  edge [
    source 29
    target 464
  ]
  edge [
    source 29
    target 394
  ]
  edge [
    source 29
    target 42
  ]
  edge [
    source 29
    target 465
  ]
  edge [
    source 29
    target 466
  ]
  edge [
    source 29
    target 467
  ]
  edge [
    source 29
    target 468
  ]
  edge [
    source 29
    target 469
  ]
  edge [
    source 29
    target 470
  ]
  edge [
    source 29
    target 471
  ]
  edge [
    source 29
    target 472
  ]
  edge [
    source 29
    target 473
  ]
  edge [
    source 29
    target 474
  ]
  edge [
    source 29
    target 475
  ]
  edge [
    source 29
    target 384
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 36
  ]
  edge [
    source 30
    target 39
  ]
  edge [
    source 30
    target 32
  ]
  edge [
    source 30
    target 67
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 37
  ]
  edge [
    source 31
    target 45
  ]
  edge [
    source 31
    target 46
  ]
  edge [
    source 31
    target 35
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 40
  ]
  edge [
    source 32
    target 42
  ]
  edge [
    source 32
    target 43
  ]
  edge [
    source 32
    target 476
  ]
  edge [
    source 32
    target 71
  ]
  edge [
    source 32
    target 72
  ]
  edge [
    source 32
    target 477
  ]
  edge [
    source 32
    target 478
  ]
  edge [
    source 32
    target 479
  ]
  edge [
    source 32
    target 480
  ]
  edge [
    source 32
    target 481
  ]
  edge [
    source 32
    target 482
  ]
  edge [
    source 32
    target 73
  ]
  edge [
    source 32
    target 74
  ]
  edge [
    source 32
    target 483
  ]
  edge [
    source 32
    target 484
  ]
  edge [
    source 32
    target 485
  ]
  edge [
    source 32
    target 486
  ]
  edge [
    source 32
    target 75
  ]
  edge [
    source 32
    target 487
  ]
  edge [
    source 32
    target 76
  ]
  edge [
    source 32
    target 77
  ]
  edge [
    source 32
    target 488
  ]
  edge [
    source 32
    target 78
  ]
  edge [
    source 32
    target 79
  ]
  edge [
    source 32
    target 80
  ]
  edge [
    source 32
    target 81
  ]
  edge [
    source 32
    target 82
  ]
  edge [
    source 32
    target 489
  ]
  edge [
    source 32
    target 83
  ]
  edge [
    source 32
    target 490
  ]
  edge [
    source 32
    target 491
  ]
  edge [
    source 32
    target 492
  ]
  edge [
    source 32
    target 84
  ]
  edge [
    source 32
    target 493
  ]
  edge [
    source 32
    target 85
  ]
  edge [
    source 32
    target 86
  ]
  edge [
    source 32
    target 494
  ]
  edge [
    source 32
    target 87
  ]
  edge [
    source 32
    target 495
  ]
  edge [
    source 32
    target 88
  ]
  edge [
    source 32
    target 89
  ]
  edge [
    source 32
    target 496
  ]
  edge [
    source 32
    target 90
  ]
  edge [
    source 32
    target 91
  ]
  edge [
    source 32
    target 92
  ]
  edge [
    source 32
    target 497
  ]
  edge [
    source 32
    target 498
  ]
  edge [
    source 32
    target 499
  ]
  edge [
    source 32
    target 93
  ]
  edge [
    source 32
    target 94
  ]
  edge [
    source 32
    target 500
  ]
  edge [
    source 32
    target 95
  ]
  edge [
    source 32
    target 501
  ]
  edge [
    source 32
    target 502
  ]
  edge [
    source 32
    target 503
  ]
  edge [
    source 32
    target 98
  ]
  edge [
    source 32
    target 504
  ]
  edge [
    source 32
    target 99
  ]
  edge [
    source 32
    target 100
  ]
  edge [
    source 32
    target 505
  ]
  edge [
    source 32
    target 506
  ]
  edge [
    source 32
    target 507
  ]
  edge [
    source 32
    target 508
  ]
  edge [
    source 32
    target 101
  ]
  edge [
    source 32
    target 102
  ]
  edge [
    source 32
    target 103
  ]
  edge [
    source 32
    target 509
  ]
  edge [
    source 32
    target 104
  ]
  edge [
    source 32
    target 105
  ]
  edge [
    source 32
    target 510
  ]
  edge [
    source 32
    target 106
  ]
  edge [
    source 32
    target 107
  ]
  edge [
    source 32
    target 109
  ]
  edge [
    source 32
    target 108
  ]
  edge [
    source 32
    target 110
  ]
  edge [
    source 32
    target 111
  ]
  edge [
    source 32
    target 112
  ]
  edge [
    source 32
    target 113
  ]
  edge [
    source 32
    target 114
  ]
  edge [
    source 32
    target 115
  ]
  edge [
    source 32
    target 511
  ]
  edge [
    source 32
    target 117
  ]
  edge [
    source 32
    target 118
  ]
  edge [
    source 32
    target 512
  ]
  edge [
    source 32
    target 513
  ]
  edge [
    source 32
    target 119
  ]
  edge [
    source 32
    target 514
  ]
  edge [
    source 32
    target 120
  ]
  edge [
    source 32
    target 121
  ]
  edge [
    source 32
    target 122
  ]
  edge [
    source 32
    target 123
  ]
  edge [
    source 32
    target 124
  ]
  edge [
    source 32
    target 125
  ]
  edge [
    source 32
    target 126
  ]
  edge [
    source 32
    target 127
  ]
  edge [
    source 32
    target 515
  ]
  edge [
    source 32
    target 129
  ]
  edge [
    source 32
    target 130
  ]
  edge [
    source 32
    target 516
  ]
  edge [
    source 32
    target 131
  ]
  edge [
    source 32
    target 517
  ]
  edge [
    source 32
    target 132
  ]
  edge [
    source 32
    target 133
  ]
  edge [
    source 32
    target 518
  ]
  edge [
    source 32
    target 519
  ]
  edge [
    source 32
    target 520
  ]
  edge [
    source 32
    target 134
  ]
  edge [
    source 32
    target 135
  ]
  edge [
    source 32
    target 136
  ]
  edge [
    source 32
    target 521
  ]
  edge [
    source 32
    target 522
  ]
  edge [
    source 32
    target 523
  ]
  edge [
    source 32
    target 524
  ]
  edge [
    source 32
    target 525
  ]
  edge [
    source 32
    target 137
  ]
  edge [
    source 32
    target 138
  ]
  edge [
    source 32
    target 526
  ]
  edge [
    source 32
    target 139
  ]
  edge [
    source 32
    target 140
  ]
  edge [
    source 32
    target 527
  ]
  edge [
    source 32
    target 141
  ]
  edge [
    source 32
    target 142
  ]
  edge [
    source 32
    target 143
  ]
  edge [
    source 32
    target 528
  ]
  edge [
    source 32
    target 529
  ]
  edge [
    source 32
    target 530
  ]
  edge [
    source 32
    target 144
  ]
  edge [
    source 32
    target 145
  ]
  edge [
    source 32
    target 146
  ]
  edge [
    source 32
    target 150
  ]
  edge [
    source 32
    target 148
  ]
  edge [
    source 32
    target 152
  ]
  edge [
    source 32
    target 531
  ]
  edge [
    source 32
    target 151
  ]
  edge [
    source 32
    target 532
  ]
  edge [
    source 32
    target 533
  ]
  edge [
    source 32
    target 153
  ]
  edge [
    source 32
    target 154
  ]
  edge [
    source 32
    target 534
  ]
  edge [
    source 32
    target 155
  ]
  edge [
    source 32
    target 156
  ]
  edge [
    source 32
    target 535
  ]
  edge [
    source 32
    target 158
  ]
  edge [
    source 32
    target 536
  ]
  edge [
    source 32
    target 537
  ]
  edge [
    source 32
    target 538
  ]
  edge [
    source 32
    target 157
  ]
  edge [
    source 32
    target 539
  ]
  edge [
    source 32
    target 540
  ]
  edge [
    source 32
    target 541
  ]
  edge [
    source 32
    target 161
  ]
  edge [
    source 32
    target 162
  ]
  edge [
    source 32
    target 542
  ]
  edge [
    source 32
    target 163
  ]
  edge [
    source 32
    target 164
  ]
  edge [
    source 32
    target 165
  ]
  edge [
    source 32
    target 543
  ]
  edge [
    source 32
    target 166
  ]
  edge [
    source 32
    target 544
  ]
  edge [
    source 32
    target 545
  ]
  edge [
    source 32
    target 546
  ]
  edge [
    source 32
    target 168
  ]
  edge [
    source 32
    target 169
  ]
  edge [
    source 32
    target 170
  ]
  edge [
    source 32
    target 171
  ]
  edge [
    source 32
    target 547
  ]
  edge [
    source 32
    target 172
  ]
  edge [
    source 32
    target 173
  ]
  edge [
    source 32
    target 174
  ]
  edge [
    source 32
    target 175
  ]
  edge [
    source 32
    target 548
  ]
  edge [
    source 32
    target 549
  ]
  edge [
    source 32
    target 176
  ]
  edge [
    source 32
    target 177
  ]
  edge [
    source 32
    target 178
  ]
  edge [
    source 32
    target 179
  ]
  edge [
    source 32
    target 180
  ]
  edge [
    source 32
    target 181
  ]
  edge [
    source 32
    target 182
  ]
  edge [
    source 32
    target 550
  ]
  edge [
    source 32
    target 551
  ]
  edge [
    source 32
    target 183
  ]
  edge [
    source 32
    target 184
  ]
  edge [
    source 32
    target 185
  ]
  edge [
    source 32
    target 552
  ]
  edge [
    source 32
    target 553
  ]
  edge [
    source 32
    target 188
  ]
  edge [
    source 32
    target 189
  ]
  edge [
    source 32
    target 190
  ]
  edge [
    source 32
    target 191
  ]
  edge [
    source 32
    target 554
  ]
  edge [
    source 32
    target 555
  ]
  edge [
    source 32
    target 556
  ]
  edge [
    source 32
    target 193
  ]
  edge [
    source 32
    target 194
  ]
  edge [
    source 32
    target 195
  ]
  edge [
    source 32
    target 196
  ]
  edge [
    source 32
    target 557
  ]
  edge [
    source 32
    target 197
  ]
  edge [
    source 32
    target 198
  ]
  edge [
    source 32
    target 199
  ]
  edge [
    source 32
    target 200
  ]
  edge [
    source 32
    target 201
  ]
  edge [
    source 32
    target 558
  ]
  edge [
    source 32
    target 559
  ]
  edge [
    source 32
    target 560
  ]
  edge [
    source 32
    target 202
  ]
  edge [
    source 32
    target 203
  ]
  edge [
    source 32
    target 204
  ]
  edge [
    source 32
    target 561
  ]
  edge [
    source 32
    target 205
  ]
  edge [
    source 32
    target 562
  ]
  edge [
    source 32
    target 206
  ]
  edge [
    source 32
    target 207
  ]
  edge [
    source 32
    target 563
  ]
  edge [
    source 32
    target 564
  ]
  edge [
    source 32
    target 565
  ]
  edge [
    source 32
    target 208
  ]
  edge [
    source 32
    target 209
  ]
  edge [
    source 32
    target 210
  ]
  edge [
    source 32
    target 211
  ]
  edge [
    source 32
    target 212
  ]
  edge [
    source 32
    target 213
  ]
  edge [
    source 32
    target 566
  ]
  edge [
    source 32
    target 567
  ]
  edge [
    source 32
    target 214
  ]
  edge [
    source 32
    target 215
  ]
  edge [
    source 32
    target 216
  ]
  edge [
    source 32
    target 568
  ]
  edge [
    source 32
    target 217
  ]
  edge [
    source 32
    target 569
  ]
  edge [
    source 32
    target 570
  ]
  edge [
    source 32
    target 571
  ]
  edge [
    source 32
    target 218
  ]
  edge [
    source 32
    target 219
  ]
  edge [
    source 32
    target 220
  ]
  edge [
    source 32
    target 221
  ]
  edge [
    source 32
    target 222
  ]
  edge [
    source 32
    target 223
  ]
  edge [
    source 32
    target 224
  ]
  edge [
    source 32
    target 225
  ]
  edge [
    source 32
    target 226
  ]
  edge [
    source 32
    target 572
  ]
  edge [
    source 32
    target 573
  ]
  edge [
    source 32
    target 227
  ]
  edge [
    source 32
    target 228
  ]
  edge [
    source 32
    target 229
  ]
  edge [
    source 32
    target 574
  ]
  edge [
    source 32
    target 575
  ]
  edge [
    source 32
    target 230
  ]
  edge [
    source 32
    target 231
  ]
  edge [
    source 32
    target 576
  ]
  edge [
    source 32
    target 232
  ]
  edge [
    source 32
    target 233
  ]
  edge [
    source 32
    target 577
  ]
  edge [
    source 32
    target 578
  ]
  edge [
    source 32
    target 579
  ]
  edge [
    source 32
    target 234
  ]
  edge [
    source 32
    target 580
  ]
  edge [
    source 32
    target 235
  ]
  edge [
    source 32
    target 236
  ]
  edge [
    source 32
    target 237
  ]
  edge [
    source 32
    target 238
  ]
  edge [
    source 32
    target 241
  ]
  edge [
    source 32
    target 243
  ]
  edge [
    source 32
    target 581
  ]
  edge [
    source 32
    target 242
  ]
  edge [
    source 32
    target 582
  ]
  edge [
    source 32
    target 583
  ]
  edge [
    source 32
    target 244
  ]
  edge [
    source 32
    target 245
  ]
  edge [
    source 32
    target 246
  ]
  edge [
    source 32
    target 584
  ]
  edge [
    source 32
    target 240
  ]
  edge [
    source 32
    target 247
  ]
  edge [
    source 32
    target 248
  ]
  edge [
    source 32
    target 249
  ]
  edge [
    source 32
    target 250
  ]
  edge [
    source 32
    target 252
  ]
  edge [
    source 32
    target 253
  ]
  edge [
    source 32
    target 257
  ]
  edge [
    source 32
    target 255
  ]
  edge [
    source 32
    target 256
  ]
  edge [
    source 32
    target 585
  ]
  edge [
    source 32
    target 586
  ]
  edge [
    source 32
    target 258
  ]
  edge [
    source 32
    target 259
  ]
  edge [
    source 32
    target 587
  ]
  edge [
    source 32
    target 260
  ]
  edge [
    source 32
    target 588
  ]
  edge [
    source 32
    target 261
  ]
  edge [
    source 32
    target 589
  ]
  edge [
    source 32
    target 263
  ]
  edge [
    source 32
    target 262
  ]
  edge [
    source 32
    target 590
  ]
  edge [
    source 32
    target 264
  ]
  edge [
    source 32
    target 265
  ]
  edge [
    source 32
    target 591
  ]
  edge [
    source 32
    target 266
  ]
  edge [
    source 32
    target 267
  ]
  edge [
    source 32
    target 592
  ]
  edge [
    source 32
    target 268
  ]
  edge [
    source 32
    target 269
  ]
  edge [
    source 32
    target 270
  ]
  edge [
    source 32
    target 271
  ]
  edge [
    source 32
    target 274
  ]
  edge [
    source 32
    target 272
  ]
  edge [
    source 32
    target 273
  ]
  edge [
    source 32
    target 593
  ]
  edge [
    source 32
    target 275
  ]
  edge [
    source 32
    target 276
  ]
  edge [
    source 32
    target 594
  ]
  edge [
    source 32
    target 595
  ]
  edge [
    source 32
    target 277
  ]
  edge [
    source 32
    target 280
  ]
  edge [
    source 32
    target 279
  ]
  edge [
    source 32
    target 283
  ]
  edge [
    source 32
    target 281
  ]
  edge [
    source 32
    target 282
  ]
  edge [
    source 32
    target 596
  ]
  edge [
    source 32
    target 597
  ]
  edge [
    source 32
    target 284
  ]
  edge [
    source 32
    target 285
  ]
  edge [
    source 32
    target 598
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 599
  ]
  edge [
    source 33
    target 600
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 60
  ]
  edge [
    source 34
    target 59
  ]
  edge [
    source 34
    target 601
  ]
  edge [
    source 34
    target 602
  ]
  edge [
    source 34
    target 603
  ]
  edge [
    source 34
    target 604
  ]
  edge [
    source 34
    target 605
  ]
  edge [
    source 34
    target 457
  ]
  edge [
    source 34
    target 606
  ]
  edge [
    source 34
    target 607
  ]
  edge [
    source 34
    target 608
  ]
  edge [
    source 34
    target 609
  ]
  edge [
    source 34
    target 610
  ]
  edge [
    source 34
    target 611
  ]
  edge [
    source 34
    target 612
  ]
  edge [
    source 34
    target 613
  ]
  edge [
    source 34
    target 614
  ]
  edge [
    source 34
    target 615
  ]
  edge [
    source 34
    target 616
  ]
  edge [
    source 34
    target 328
  ]
  edge [
    source 34
    target 617
  ]
  edge [
    source 34
    target 618
  ]
  edge [
    source 34
    target 619
  ]
  edge [
    source 34
    target 620
  ]
  edge [
    source 34
    target 621
  ]
  edge [
    source 34
    target 622
  ]
  edge [
    source 34
    target 340
  ]
  edge [
    source 34
    target 623
  ]
  edge [
    source 34
    target 624
  ]
  edge [
    source 34
    target 625
  ]
  edge [
    source 34
    target 626
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 627
  ]
  edge [
    source 35
    target 628
  ]
  edge [
    source 36
    target 629
  ]
  edge [
    source 36
    target 630
  ]
  edge [
    source 36
    target 631
  ]
  edge [
    source 36
    target 632
  ]
  edge [
    source 36
    target 633
  ]
  edge [
    source 36
    target 634
  ]
  edge [
    source 36
    target 635
  ]
  edge [
    source 36
    target 636
  ]
  edge [
    source 36
    target 236
  ]
  edge [
    source 36
    target 637
  ]
  edge [
    source 36
    target 638
  ]
  edge [
    source 36
    target 639
  ]
  edge [
    source 36
    target 640
  ]
  edge [
    source 36
    target 355
  ]
  edge [
    source 36
    target 641
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 642
  ]
  edge [
    source 37
    target 643
  ]
  edge [
    source 37
    target 644
  ]
  edge [
    source 37
    target 645
  ]
  edge [
    source 37
    target 646
  ]
  edge [
    source 37
    target 647
  ]
  edge [
    source 37
    target 648
  ]
  edge [
    source 37
    target 649
  ]
  edge [
    source 37
    target 650
  ]
  edge [
    source 37
    target 622
  ]
  edge [
    source 37
    target 651
  ]
  edge [
    source 37
    target 652
  ]
  edge [
    source 37
    target 653
  ]
  edge [
    source 37
    target 67
  ]
  edge [
    source 37
    target 69
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 654
  ]
  edge [
    source 38
    target 655
  ]
  edge [
    source 38
    target 656
  ]
  edge [
    source 38
    target 394
  ]
  edge [
    source 38
    target 657
  ]
  edge [
    source 38
    target 658
  ]
  edge [
    source 38
    target 659
  ]
  edge [
    source 38
    target 660
  ]
  edge [
    source 38
    target 661
  ]
  edge [
    source 38
    target 662
  ]
  edge [
    source 38
    target 663
  ]
  edge [
    source 38
    target 664
  ]
  edge [
    source 38
    target 665
  ]
  edge [
    source 38
    target 666
  ]
  edge [
    source 38
    target 667
  ]
  edge [
    source 38
    target 668
  ]
  edge [
    source 38
    target 669
  ]
  edge [
    source 39
    target 670
  ]
  edge [
    source 39
    target 671
  ]
  edge [
    source 39
    target 672
  ]
  edge [
    source 39
    target 673
  ]
  edge [
    source 39
    target 674
  ]
  edge [
    source 39
    target 675
  ]
  edge [
    source 39
    target 676
  ]
  edge [
    source 39
    target 677
  ]
  edge [
    source 39
    target 678
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 679
  ]
  edge [
    source 40
    target 680
  ]
  edge [
    source 40
    target 681
  ]
  edge [
    source 40
    target 397
  ]
  edge [
    source 40
    target 682
  ]
  edge [
    source 40
    target 683
  ]
  edge [
    source 40
    target 684
  ]
  edge [
    source 40
    target 685
  ]
  edge [
    source 40
    target 686
  ]
  edge [
    source 40
    target 687
  ]
  edge [
    source 40
    target 688
  ]
  edge [
    source 40
    target 689
  ]
  edge [
    source 40
    target 446
  ]
  edge [
    source 40
    target 690
  ]
  edge [
    source 40
    target 691
  ]
  edge [
    source 40
    target 692
  ]
  edge [
    source 40
    target 51
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 42
    target 464
  ]
  edge [
    source 42
    target 138
  ]
  edge [
    source 42
    target 693
  ]
  edge [
    source 42
    target 468
  ]
  edge [
    source 42
    target 694
  ]
  edge [
    source 42
    target 469
  ]
  edge [
    source 42
    target 472
  ]
  edge [
    source 42
    target 355
  ]
  edge [
    source 42
    target 695
  ]
  edge [
    source 42
    target 58
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 696
  ]
  edge [
    source 43
    target 697
  ]
  edge [
    source 43
    target 52
  ]
  edge [
    source 43
    target 54
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 698
  ]
  edge [
    source 44
    target 699
  ]
  edge [
    source 44
    target 700
  ]
  edge [
    source 44
    target 701
  ]
  edge [
    source 44
    target 702
  ]
  edge [
    source 45
    target 703
  ]
  edge [
    source 45
    target 704
  ]
  edge [
    source 45
    target 705
  ]
  edge [
    source 45
    target 706
  ]
  edge [
    source 45
    target 707
  ]
  edge [
    source 45
    target 708
  ]
  edge [
    source 45
    target 709
  ]
  edge [
    source 45
    target 710
  ]
  edge [
    source 45
    target 711
  ]
  edge [
    source 45
    target 712
  ]
  edge [
    source 45
    target 713
  ]
  edge [
    source 45
    target 714
  ]
  edge [
    source 45
    target 715
  ]
  edge [
    source 45
    target 716
  ]
  edge [
    source 45
    target 717
  ]
  edge [
    source 45
    target 718
  ]
  edge [
    source 45
    target 719
  ]
  edge [
    source 45
    target 720
  ]
  edge [
    source 45
    target 721
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 722
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 723
  ]
  edge [
    source 47
    target 724
  ]
  edge [
    source 47
    target 725
  ]
  edge [
    source 47
    target 726
  ]
  edge [
    source 47
    target 727
  ]
  edge [
    source 47
    target 728
  ]
  edge [
    source 47
    target 729
  ]
  edge [
    source 47
    target 443
  ]
  edge [
    source 47
    target 730
  ]
  edge [
    source 47
    target 731
  ]
  edge [
    source 48
    target 732
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 733
  ]
  edge [
    source 49
    target 734
  ]
  edge [
    source 49
    target 735
  ]
  edge [
    source 49
    target 736
  ]
  edge [
    source 49
    target 737
  ]
  edge [
    source 50
    target 738
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 53
  ]
  edge [
    source 51
    target 739
  ]
  edge [
    source 51
    target 740
  ]
  edge [
    source 51
    target 741
  ]
  edge [
    source 51
    target 643
  ]
  edge [
    source 51
    target 742
  ]
  edge [
    source 51
    target 743
  ]
  edge [
    source 51
    target 744
  ]
  edge [
    source 51
    target 335
  ]
  edge [
    source 51
    target 745
  ]
  edge [
    source 51
    target 746
  ]
  edge [
    source 51
    target 347
  ]
  edge [
    source 51
    target 747
  ]
  edge [
    source 52
    target 748
  ]
  edge [
    source 52
    target 749
  ]
  edge [
    source 52
    target 750
  ]
  edge [
    source 52
    target 751
  ]
  edge [
    source 52
    target 54
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 752
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 753
  ]
  edge [
    source 54
    target 754
  ]
  edge [
    source 54
    target 755
  ]
  edge [
    source 54
    target 756
  ]
  edge [
    source 54
    target 708
  ]
  edge [
    source 54
    target 116
  ]
  edge [
    source 54
    target 757
  ]
  edge [
    source 54
    target 758
  ]
  edge [
    source 54
    target 759
  ]
  edge [
    source 54
    target 760
  ]
  edge [
    source 55
    target 761
  ]
  edge [
    source 55
    target 762
  ]
  edge [
    source 55
    target 763
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 764
  ]
  edge [
    source 56
    target 296
  ]
  edge [
    source 56
    target 394
  ]
  edge [
    source 56
    target 765
  ]
  edge [
    source 56
    target 766
  ]
  edge [
    source 56
    target 767
  ]
  edge [
    source 56
    target 768
  ]
  edge [
    source 56
    target 769
  ]
  edge [
    source 56
    target 770
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 58
    target 771
  ]
  edge [
    source 58
    target 772
  ]
  edge [
    source 58
    target 773
  ]
  edge [
    source 58
    target 774
  ]
  edge [
    source 58
    target 775
  ]
  edge [
    source 58
    target 776
  ]
  edge [
    source 58
    target 777
  ]
  edge [
    source 58
    target 778
  ]
  edge [
    source 58
    target 779
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 61
  ]
  edge [
    source 59
    target 68
  ]
  edge [
    source 59
    target 69
  ]
  edge [
    source 59
    target 771
  ]
  edge [
    source 59
    target 780
  ]
  edge [
    source 59
    target 781
  ]
  edge [
    source 59
    target 782
  ]
  edge [
    source 59
    target 783
  ]
  edge [
    source 59
    target 784
  ]
  edge [
    source 59
    target 785
  ]
  edge [
    source 60
    target 786
  ]
  edge [
    source 60
    target 682
  ]
  edge [
    source 60
    target 686
  ]
  edge [
    source 60
    target 787
  ]
  edge [
    source 60
    target 788
  ]
  edge [
    source 60
    target 789
  ]
  edge [
    source 60
    target 689
  ]
  edge [
    source 60
    target 790
  ]
  edge [
    source 60
    target 791
  ]
  edge [
    source 60
    target 792
  ]
  edge [
    source 60
    target 793
  ]
  edge [
    source 60
    target 794
  ]
  edge [
    source 60
    target 397
  ]
  edge [
    source 60
    target 795
  ]
  edge [
    source 60
    target 796
  ]
  edge [
    source 60
    target 797
  ]
  edge [
    source 60
    target 96
  ]
  edge [
    source 60
    target 798
  ]
  edge [
    source 60
    target 799
  ]
  edge [
    source 60
    target 800
  ]
  edge [
    source 60
    target 801
  ]
  edge [
    source 60
    target 802
  ]
  edge [
    source 60
    target 803
  ]
  edge [
    source 60
    target 804
  ]
  edge [
    source 60
    target 805
  ]
  edge [
    source 60
    target 806
  ]
  edge [
    source 60
    target 807
  ]
  edge [
    source 60
    target 446
  ]
  edge [
    source 60
    target 808
  ]
  edge [
    source 60
    target 379
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 809
  ]
  edge [
    source 61
    target 810
  ]
  edge [
    source 61
    target 811
  ]
  edge [
    source 61
    target 812
  ]
  edge [
    source 61
    target 813
  ]
  edge [
    source 61
    target 814
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 815
  ]
  edge [
    source 62
    target 816
  ]
  edge [
    source 62
    target 817
  ]
  edge [
    source 62
    target 818
  ]
  edge [
    source 62
    target 819
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 820
  ]
  edge [
    source 63
    target 821
  ]
  edge [
    source 63
    target 822
  ]
  edge [
    source 63
    target 823
  ]
  edge [
    source 63
    target 824
  ]
  edge [
    source 63
    target 825
  ]
  edge [
    source 63
    target 826
  ]
  edge [
    source 63
    target 827
  ]
  edge [
    source 63
    target 828
  ]
  edge [
    source 63
    target 829
  ]
  edge [
    source 63
    target 830
  ]
  edge [
    source 63
    target 669
  ]
  edge [
    source 63
    target 831
  ]
  edge [
    source 63
    target 832
  ]
  edge [
    source 63
    target 833
  ]
  edge [
    source 63
    target 834
  ]
  edge [
    source 63
    target 835
  ]
  edge [
    source 63
    target 836
  ]
  edge [
    source 63
    target 837
  ]
  edge [
    source 63
    target 654
  ]
  edge [
    source 63
    target 838
  ]
  edge [
    source 63
    target 839
  ]
  edge [
    source 63
    target 840
  ]
  edge [
    source 63
    target 841
  ]
  edge [
    source 63
    target 842
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 843
  ]
  edge [
    source 64
    target 844
  ]
  edge [
    source 64
    target 845
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 846
  ]
  edge [
    source 65
    target 847
  ]
  edge [
    source 65
    target 848
  ]
  edge [
    source 65
    target 849
  ]
  edge [
    source 65
    target 850
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 66
    target 851
  ]
  edge [
    source 66
    target 852
  ]
  edge [
    source 66
    target 853
  ]
  edge [
    source 66
    target 394
  ]
  edge [
    source 66
    target 662
  ]
  edge [
    source 66
    target 854
  ]
  edge [
    source 66
    target 855
  ]
  edge [
    source 66
    target 856
  ]
  edge [
    source 66
    target 857
  ]
  edge [
    source 66
    target 858
  ]
  edge [
    source 66
    target 859
  ]
  edge [
    source 66
    target 860
  ]
  edge [
    source 67
    target 69
  ]
  edge [
    source 68
    target 861
  ]
  edge [
    source 68
    target 862
  ]
  edge [
    source 68
    target 863
  ]
]
