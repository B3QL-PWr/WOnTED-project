graph [
  maxDegree 28
  minDegree 1
  meanDegree 2.0425531914893615
  density 0.02196293754289636
  graphCliqueNumber 3
  node [
    id 0
    label "pijany"
    origin "text"
  ]
  node [
    id 1
    label "patostreamer"
    origin "text"
  ]
  node [
    id 2
    label "dawid"
    origin "text"
  ]
  node [
    id 3
    label "zn&#281;ca&#263;"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "nad"
    origin "text"
  ]
  node [
    id 6
    label "bezbronny"
    origin "text"
  ]
  node [
    id 7
    label "pies"
    origin "text"
  ]
  node [
    id 8
    label "chwali&#263;"
    origin "text"
  ]
  node [
    id 9
    label "wszystek"
    origin "text"
  ]
  node [
    id 10
    label "swoje"
    origin "text"
  ]
  node [
    id 11
    label "owczarek"
    origin "text"
  ]
  node [
    id 12
    label "niemiecki"
    origin "text"
  ]
  node [
    id 13
    label "tresowa&#263;"
    origin "text"
  ]
  node [
    id 14
    label "kij"
    origin "text"
  ]
  node [
    id 15
    label "cz&#322;owiek"
  ]
  node [
    id 16
    label "upicie_si&#281;"
  ]
  node [
    id 17
    label "szalony"
  ]
  node [
    id 18
    label "d&#281;tka"
  ]
  node [
    id 19
    label "pij&#261;cy"
  ]
  node [
    id 20
    label "upijanie_si&#281;"
  ]
  node [
    id 21
    label "napi&#322;y"
  ]
  node [
    id 22
    label "nieprzytomny"
  ]
  node [
    id 23
    label "pull"
  ]
  node [
    id 24
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 25
    label "mani&#263;"
  ]
  node [
    id 26
    label "bezradny"
  ]
  node [
    id 27
    label "s&#322;aby"
  ]
  node [
    id 28
    label "bezbronnie"
  ]
  node [
    id 29
    label "niezdolny"
  ]
  node [
    id 30
    label "wy&#263;"
  ]
  node [
    id 31
    label "ssak_drapie&#380;ny"
  ]
  node [
    id 32
    label "spragniony"
  ]
  node [
    id 33
    label "rakarz"
  ]
  node [
    id 34
    label "psowate"
  ]
  node [
    id 35
    label "istota_&#380;ywa"
  ]
  node [
    id 36
    label "kabanos"
  ]
  node [
    id 37
    label "mi&#322;o&#347;nik"
  ]
  node [
    id 38
    label "&#322;ajdak"
  ]
  node [
    id 39
    label "czworon&#243;g"
  ]
  node [
    id 40
    label "policjant"
  ]
  node [
    id 41
    label "szczucie"
  ]
  node [
    id 42
    label "s&#322;u&#380;enie"
  ]
  node [
    id 43
    label "sobaka"
  ]
  node [
    id 44
    label "dogoterapia"
  ]
  node [
    id 45
    label "Cerber"
  ]
  node [
    id 46
    label "wyzwisko"
  ]
  node [
    id 47
    label "szczu&#263;"
  ]
  node [
    id 48
    label "wycie"
  ]
  node [
    id 49
    label "szczeka&#263;"
  ]
  node [
    id 50
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 51
    label "trufla"
  ]
  node [
    id 52
    label "samiec"
  ]
  node [
    id 53
    label "piese&#322;"
  ]
  node [
    id 54
    label "zawy&#263;"
  ]
  node [
    id 55
    label "wys&#322;awia&#263;"
  ]
  node [
    id 56
    label "wywy&#380;sza&#263;"
  ]
  node [
    id 57
    label "bless"
  ]
  node [
    id 58
    label "patrze&#263;_jak_w_&#347;wi&#281;ty_obraz"
  ]
  node [
    id 59
    label "nagradza&#263;"
  ]
  node [
    id 60
    label "wielmo&#380;y&#263;"
  ]
  node [
    id 61
    label "glorify"
  ]
  node [
    id 62
    label "ca&#322;y"
  ]
  node [
    id 63
    label "pies_pasterski"
  ]
  node [
    id 64
    label "owczarz"
  ]
  node [
    id 65
    label "pastuszek"
  ]
  node [
    id 66
    label "szwabski"
  ]
  node [
    id 67
    label "po_niemiecku"
  ]
  node [
    id 68
    label "niemiec"
  ]
  node [
    id 69
    label "cenar"
  ]
  node [
    id 70
    label "j&#281;zyk"
  ]
  node [
    id 71
    label "europejski"
  ]
  node [
    id 72
    label "German"
  ]
  node [
    id 73
    label "pionier"
  ]
  node [
    id 74
    label "niemiecko"
  ]
  node [
    id 75
    label "j&#281;zyk_zachodniogerma&#324;ski"
  ]
  node [
    id 76
    label "zachodnioeuropejski"
  ]
  node [
    id 77
    label "strudel"
  ]
  node [
    id 78
    label "junkers"
  ]
  node [
    id 79
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 80
    label "podporz&#261;dkowywa&#263;"
  ]
  node [
    id 81
    label "educate"
  ]
  node [
    id 82
    label "uczy&#263;"
  ]
  node [
    id 83
    label "zwierz&#281;"
  ]
  node [
    id 84
    label "doskonali&#263;"
  ]
  node [
    id 85
    label "obiekt_naturalny"
  ]
  node [
    id 86
    label "cake"
  ]
  node [
    id 87
    label "przedmiot"
  ]
  node [
    id 88
    label "patyk"
  ]
  node [
    id 89
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 90
    label "piwo"
  ]
  node [
    id 91
    label "nalewak"
  ]
  node [
    id 92
    label "Dawid"
  ]
  node [
    id 93
    label "R"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 8
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 12
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 15
  ]
  edge [
    source 7
    target 30
  ]
  edge [
    source 7
    target 31
  ]
  edge [
    source 7
    target 32
  ]
  edge [
    source 7
    target 33
  ]
  edge [
    source 7
    target 34
  ]
  edge [
    source 7
    target 35
  ]
  edge [
    source 7
    target 36
  ]
  edge [
    source 7
    target 37
  ]
  edge [
    source 7
    target 38
  ]
  edge [
    source 7
    target 39
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 63
  ]
  edge [
    source 11
    target 64
  ]
  edge [
    source 11
    target 65
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 66
  ]
  edge [
    source 12
    target 67
  ]
  edge [
    source 12
    target 68
  ]
  edge [
    source 12
    target 69
  ]
  edge [
    source 12
    target 70
  ]
  edge [
    source 12
    target 71
  ]
  edge [
    source 12
    target 72
  ]
  edge [
    source 12
    target 73
  ]
  edge [
    source 12
    target 74
  ]
  edge [
    source 12
    target 75
  ]
  edge [
    source 12
    target 76
  ]
  edge [
    source 12
    target 77
  ]
  edge [
    source 12
    target 78
  ]
  edge [
    source 12
    target 79
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 80
  ]
  edge [
    source 13
    target 81
  ]
  edge [
    source 13
    target 82
  ]
  edge [
    source 13
    target 83
  ]
  edge [
    source 13
    target 84
  ]
  edge [
    source 14
    target 85
  ]
  edge [
    source 14
    target 86
  ]
  edge [
    source 14
    target 87
  ]
  edge [
    source 14
    target 88
  ]
  edge [
    source 14
    target 89
  ]
  edge [
    source 14
    target 90
  ]
  edge [
    source 14
    target 91
  ]
  edge [
    source 92
    target 93
  ]
]
