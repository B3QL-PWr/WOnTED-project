graph [
  maxDegree 14
  minDegree 1
  meanDegree 1.9574468085106382
  density 0.0425531914893617
  graphCliqueNumber 2
  node [
    id 0
    label "go&#347;&#263;"
    origin "text"
  ]
  node [
    id 1
    label "honorowy"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "ambasador"
    origin "text"
  ]
  node [
    id 4
    label "izrael"
    origin "text"
  ]
  node [
    id 5
    label "usa"
    origin "text"
  ]
  node [
    id 6
    label "cz&#322;owiek"
  ]
  node [
    id 7
    label "bratek"
  ]
  node [
    id 8
    label "przybysz"
  ]
  node [
    id 9
    label "facet"
  ]
  node [
    id 10
    label "hotel"
  ]
  node [
    id 11
    label "restauracja"
  ]
  node [
    id 12
    label "klient"
  ]
  node [
    id 13
    label "uczestnik"
  ]
  node [
    id 14
    label "sztuka"
  ]
  node [
    id 15
    label "odwiedziny"
  ]
  node [
    id 16
    label "szlachetny"
  ]
  node [
    id 17
    label "godny"
  ]
  node [
    id 18
    label "ambitny"
  ]
  node [
    id 19
    label "honorowo"
  ]
  node [
    id 20
    label "honorny"
  ]
  node [
    id 21
    label "symboliczny"
  ]
  node [
    id 22
    label "wyj&#261;tkowy"
  ]
  node [
    id 23
    label "bezinteresowny"
  ]
  node [
    id 24
    label "reprezentacyjny"
  ]
  node [
    id 25
    label "zaszczytny"
  ]
  node [
    id 26
    label "stosowny"
  ]
  node [
    id 27
    label "dumny"
  ]
  node [
    id 28
    label "si&#281;ga&#263;"
  ]
  node [
    id 29
    label "trwa&#263;"
  ]
  node [
    id 30
    label "obecno&#347;&#263;"
  ]
  node [
    id 31
    label "stan"
  ]
  node [
    id 32
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 33
    label "stand"
  ]
  node [
    id 34
    label "mie&#263;_miejsce"
  ]
  node [
    id 35
    label "uczestniczy&#263;"
  ]
  node [
    id 36
    label "chodzi&#263;"
  ]
  node [
    id 37
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 38
    label "equal"
  ]
  node [
    id 39
    label "dyplomata"
  ]
  node [
    id 40
    label "zwolennik"
  ]
  node [
    id 41
    label "chor&#261;&#380;y"
  ]
  node [
    id 42
    label "tuba"
  ]
  node [
    id 43
    label "przedstawiciel"
  ]
  node [
    id 44
    label "hay_fever"
  ]
  node [
    id 45
    label "rozsiewca"
  ]
  node [
    id 46
    label "popularyzator"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 4
    target 5
  ]
]
