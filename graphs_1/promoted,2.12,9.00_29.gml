graph [
  maxDegree 20
  minDegree 1
  meanDegree 1.9411764705882353
  density 0.058823529411764705
  graphCliqueNumber 2
  node [
    id 0
    label "&#380;aden"
    origin "text"
  ]
  node [
    id 1
    label "biega&#263;"
    origin "text"
  ]
  node [
    id 2
    label "guz"
    origin "text"
  ]
  node [
    id 3
    label "dziwa"
    origin "text"
  ]
  node [
    id 4
    label "nijaki"
  ]
  node [
    id 5
    label "cieka&#263;"
  ]
  node [
    id 6
    label "dash"
  ]
  node [
    id 7
    label "up&#281;dza&#263;_si&#281;"
  ]
  node [
    id 8
    label "rush"
  ]
  node [
    id 9
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 10
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 11
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 12
    label "zaleca&#263;_si&#281;"
  ]
  node [
    id 13
    label "ucieka&#263;"
  ]
  node [
    id 14
    label "uprawia&#263;"
  ]
  node [
    id 15
    label "rozwija&#263;_si&#281;"
  ]
  node [
    id 16
    label "hula&#263;"
  ]
  node [
    id 17
    label "chodzi&#263;"
  ]
  node [
    id 18
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 19
    label "rozwolnienie"
  ]
  node [
    id 20
    label "biec"
  ]
  node [
    id 21
    label "chorowa&#263;"
  ]
  node [
    id 22
    label "&#347;ciga&#263;_si&#281;"
  ]
  node [
    id 23
    label "s&#281;k_zaro&#347;ni&#281;ty"
  ]
  node [
    id 24
    label "zgrubienie"
  ]
  node [
    id 25
    label "zmiana"
  ]
  node [
    id 26
    label "ma&#322;pa"
  ]
  node [
    id 27
    label "jawnogrzesznica"
  ]
  node [
    id 28
    label "rozpustnica"
  ]
  node [
    id 29
    label "pigalak"
  ]
  node [
    id 30
    label "dziewczyna_lekkich_obyczaj&#243;w"
  ]
  node [
    id 31
    label "kurwa"
  ]
  node [
    id 32
    label "dziewczyna"
  ]
  node [
    id 33
    label "diva"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
]
