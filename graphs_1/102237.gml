graph [
  maxDegree 31
  minDegree 1
  meanDegree 2.2475247524752477
  density 0.011181715186444018
  graphCliqueNumber 3
  node [
    id 0
    label "organ"
    origin "text"
  ]
  node [
    id 1
    label "odwo&#322;awczy"
    origin "text"
  ]
  node [
    id 2
    label "odmawia&#263;"
    origin "text"
  ]
  node [
    id 3
    label "uwzgl&#281;dnienie"
    origin "text"
  ]
  node [
    id 4
    label "cofni&#281;cie"
    origin "text"
  ]
  node [
    id 5
    label "odwo&#322;anie"
    origin "text"
  ]
  node [
    id 6
    label "je&#380;eli"
    origin "text"
  ]
  node [
    id 7
    label "zachodzi&#263;"
    origin "text"
  ]
  node [
    id 8
    label "prawdopodobie&#324;stwo"
    origin "text"
  ]
  node [
    id 9
    label "pozostawi&#263;"
    origin "text"
  ]
  node [
    id 10
    label "moc"
    origin "text"
  ]
  node [
    id 11
    label "decyzja"
    origin "text"
  ]
  node [
    id 12
    label "wyda&#263;"
    origin "text"
  ]
  node [
    id 13
    label "naruszenie"
    origin "text"
  ]
  node [
    id 14
    label "przepis"
    origin "text"
  ]
  node [
    id 15
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 16
    label "uzasadnia&#263;"
    origin "text"
  ]
  node [
    id 17
    label "uchyli&#263;"
    origin "text"
  ]
  node [
    id 18
    label "lub"
    origin "text"
  ]
  node [
    id 19
    label "zmiana"
    origin "text"
  ]
  node [
    id 20
    label "art"
    origin "text"
  ]
  node [
    id 21
    label "wydawa&#263;"
    origin "text"
  ]
  node [
    id 22
    label "uk&#322;ad"
  ]
  node [
    id 23
    label "organogeneza"
  ]
  node [
    id 24
    label "Komitet_Region&#243;w"
  ]
  node [
    id 25
    label "Izba_Konsyliarska"
  ]
  node [
    id 26
    label "budowa"
  ]
  node [
    id 27
    label "okolica"
  ]
  node [
    id 28
    label "zesp&#243;&#322;"
  ]
  node [
    id 29
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 30
    label "jednostka_organizacyjna"
  ]
  node [
    id 31
    label "dekortykacja"
  ]
  node [
    id 32
    label "struktura_anatomiczna"
  ]
  node [
    id 33
    label "tkanka"
  ]
  node [
    id 34
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 35
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 36
    label "stomia"
  ]
  node [
    id 37
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 38
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 39
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 40
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 41
    label "tw&#243;r"
  ]
  node [
    id 42
    label "appellate"
  ]
  node [
    id 43
    label "odrzuca&#263;"
  ]
  node [
    id 44
    label "wypowiada&#263;"
  ]
  node [
    id 45
    label "frame"
  ]
  node [
    id 46
    label "sprzeciwia&#263;_si&#281;"
  ]
  node [
    id 47
    label "os&#261;dza&#263;"
  ]
  node [
    id 48
    label "thank"
  ]
  node [
    id 49
    label "contest"
  ]
  node [
    id 50
    label "odpowiada&#263;"
  ]
  node [
    id 51
    label "wzi&#281;cie"
  ]
  node [
    id 52
    label "acknowledgment"
  ]
  node [
    id 53
    label "retraction"
  ]
  node [
    id 54
    label "spowodowanie"
  ]
  node [
    id 55
    label "przemieszczenie"
  ]
  node [
    id 56
    label "uniewa&#380;nienie"
  ]
  node [
    id 57
    label "revocation"
  ]
  node [
    id 58
    label "coitus_interruptus"
  ]
  node [
    id 59
    label "wojsko"
  ]
  node [
    id 60
    label "cofni&#281;cie_si&#281;"
  ]
  node [
    id 61
    label "wniosek"
  ]
  node [
    id 62
    label "pismo_urz&#281;dowe"
  ]
  node [
    id 63
    label "mention"
  ]
  node [
    id 64
    label "reference"
  ]
  node [
    id 65
    label "odprawienie"
  ]
  node [
    id 66
    label "ukrywa&#263;_si&#281;"
  ]
  node [
    id 67
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 68
    label "foray"
  ]
  node [
    id 69
    label "reach"
  ]
  node [
    id 70
    label "mie&#263;_miejsce"
  ]
  node [
    id 71
    label "przebiega&#263;"
  ]
  node [
    id 72
    label "wpada&#263;"
  ]
  node [
    id 73
    label "nak&#322;ada&#263;_si&#281;"
  ]
  node [
    id 74
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 75
    label "intervene"
  ]
  node [
    id 76
    label "pokrywa&#263;"
  ]
  node [
    id 77
    label "dochodzi&#263;"
  ]
  node [
    id 78
    label "zachodzi&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 79
    label "przys&#322;ania&#263;"
  ]
  node [
    id 80
    label "pokrywa&#263;_si&#281;"
  ]
  node [
    id 81
    label "wsp&#243;&#322;wyst&#281;powa&#263;"
  ]
  node [
    id 82
    label "istnie&#263;"
  ]
  node [
    id 83
    label "podchodzi&#263;"
  ]
  node [
    id 84
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 85
    label "poj&#281;cie"
  ]
  node [
    id 86
    label "skrzywdzi&#263;"
  ]
  node [
    id 87
    label "impart"
  ]
  node [
    id 88
    label "liszy&#263;"
  ]
  node [
    id 89
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 90
    label "doprowadzi&#263;"
  ]
  node [
    id 91
    label "da&#263;"
  ]
  node [
    id 92
    label "zachowa&#263;"
  ]
  node [
    id 93
    label "stworzy&#263;"
  ]
  node [
    id 94
    label "permit"
  ]
  node [
    id 95
    label "przekaza&#263;"
  ]
  node [
    id 96
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 97
    label "zrobi&#263;"
  ]
  node [
    id 98
    label "zerwa&#263;"
  ]
  node [
    id 99
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 100
    label "zaplanowa&#263;"
  ]
  node [
    id 101
    label "zabra&#263;"
  ]
  node [
    id 102
    label "shove"
  ]
  node [
    id 103
    label "spowodowa&#263;"
  ]
  node [
    id 104
    label "zrezygnowa&#263;"
  ]
  node [
    id 105
    label "wyznaczy&#263;"
  ]
  node [
    id 106
    label "drop"
  ]
  node [
    id 107
    label "release"
  ]
  node [
    id 108
    label "shelve"
  ]
  node [
    id 109
    label "wa&#380;no&#347;&#263;"
  ]
  node [
    id 110
    label "nasycenie"
  ]
  node [
    id 111
    label "wuchta"
  ]
  node [
    id 112
    label "parametr"
  ]
  node [
    id 113
    label "immunity"
  ]
  node [
    id 114
    label "zdolno&#347;&#263;"
  ]
  node [
    id 115
    label "wytrzyma&#322;o&#347;&#263;"
  ]
  node [
    id 116
    label "mn&#243;stwo"
  ]
  node [
    id 117
    label "potencja"
  ]
  node [
    id 118
    label "izotonia"
  ]
  node [
    id 119
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 120
    label "nefelometria"
  ]
  node [
    id 121
    label "dokument"
  ]
  node [
    id 122
    label "resolution"
  ]
  node [
    id 123
    label "zdecydowanie"
  ]
  node [
    id 124
    label "wytw&#243;r"
  ]
  node [
    id 125
    label "podj&#281;cie_decyzji"
  ]
  node [
    id 126
    label "management"
  ]
  node [
    id 127
    label "panna_na_wydaniu"
  ]
  node [
    id 128
    label "translate"
  ]
  node [
    id 129
    label "give"
  ]
  node [
    id 130
    label "pieni&#261;dze"
  ]
  node [
    id 131
    label "supply"
  ]
  node [
    id 132
    label "wprowadzi&#263;"
  ]
  node [
    id 133
    label "zapach"
  ]
  node [
    id 134
    label "wydawnictwo"
  ]
  node [
    id 135
    label "powierzy&#263;"
  ]
  node [
    id 136
    label "produkcja"
  ]
  node [
    id 137
    label "poda&#263;"
  ]
  node [
    id 138
    label "skojarzy&#263;"
  ]
  node [
    id 139
    label "dress"
  ]
  node [
    id 140
    label "plon"
  ]
  node [
    id 141
    label "ujawni&#263;"
  ]
  node [
    id 142
    label "reszta"
  ]
  node [
    id 143
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 144
    label "zadenuncjowa&#263;"
  ]
  node [
    id 145
    label "tajemnica"
  ]
  node [
    id 146
    label "wiano"
  ]
  node [
    id 147
    label "gor&#261;cy_pieni&#261;dz"
  ]
  node [
    id 148
    label "wytworzy&#263;"
  ]
  node [
    id 149
    label "d&#378;wi&#281;k"
  ]
  node [
    id 150
    label "picture"
  ]
  node [
    id 151
    label "zrobienie"
  ]
  node [
    id 152
    label "transgresja"
  ]
  node [
    id 153
    label "zacz&#281;cie"
  ]
  node [
    id 154
    label "zepsucie"
  ]
  node [
    id 155
    label "discourtesy"
  ]
  node [
    id 156
    label "odj&#281;cie"
  ]
  node [
    id 157
    label "przedawnienie_si&#281;"
  ]
  node [
    id 158
    label "recepta"
  ]
  node [
    id 159
    label "norma_prawna"
  ]
  node [
    id 160
    label "kodeks"
  ]
  node [
    id 161
    label "prawo"
  ]
  node [
    id 162
    label "regulation"
  ]
  node [
    id 163
    label "przedawni&#263;_si&#281;"
  ]
  node [
    id 164
    label "porada"
  ]
  node [
    id 165
    label "przedawnianie_si&#281;"
  ]
  node [
    id 166
    label "spos&#243;b"
  ]
  node [
    id 167
    label "przedawnia&#263;_si&#281;"
  ]
  node [
    id 168
    label "explain"
  ]
  node [
    id 169
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 170
    label "rise"
  ]
  node [
    id 171
    label "zniwelowa&#263;"
  ]
  node [
    id 172
    label "przesun&#261;&#263;"
  ]
  node [
    id 173
    label "retract"
  ]
  node [
    id 174
    label "uniewa&#380;ni&#263;"
  ]
  node [
    id 175
    label "anatomopatolog"
  ]
  node [
    id 176
    label "rewizja"
  ]
  node [
    id 177
    label "oznaka"
  ]
  node [
    id 178
    label "czas"
  ]
  node [
    id 179
    label "ferment"
  ]
  node [
    id 180
    label "komplet"
  ]
  node [
    id 181
    label "tura"
  ]
  node [
    id 182
    label "amendment"
  ]
  node [
    id 183
    label "zmianka"
  ]
  node [
    id 184
    label "odmienianie"
  ]
  node [
    id 185
    label "passage"
  ]
  node [
    id 186
    label "zjawisko"
  ]
  node [
    id 187
    label "change"
  ]
  node [
    id 188
    label "praca"
  ]
  node [
    id 189
    label "surrender"
  ]
  node [
    id 190
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 191
    label "train"
  ]
  node [
    id 192
    label "wytwarza&#263;"
  ]
  node [
    id 193
    label "dawa&#263;"
  ]
  node [
    id 194
    label "wprowadza&#263;"
  ]
  node [
    id 195
    label "ujawnia&#263;"
  ]
  node [
    id 196
    label "powierza&#263;"
  ]
  node [
    id 197
    label "denuncjowa&#263;"
  ]
  node [
    id 198
    label "robi&#263;"
  ]
  node [
    id 199
    label "placard"
  ]
  node [
    id 200
    label "kojarzy&#263;"
  ]
  node [
    id 201
    label "podawa&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 12
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 9
    target 100
  ]
  edge [
    source 9
    target 101
  ]
  edge [
    source 9
    target 102
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 104
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 10
    target 110
  ]
  edge [
    source 10
    target 111
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 21
  ]
  edge [
    source 11
    target 15
  ]
  edge [
    source 11
    target 121
  ]
  edge [
    source 11
    target 122
  ]
  edge [
    source 11
    target 123
  ]
  edge [
    source 11
    target 124
  ]
  edge [
    source 11
    target 125
  ]
  edge [
    source 11
    target 126
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 12
    target 131
  ]
  edge [
    source 12
    target 132
  ]
  edge [
    source 12
    target 91
  ]
  edge [
    source 12
    target 133
  ]
  edge [
    source 12
    target 134
  ]
  edge [
    source 12
    target 135
  ]
  edge [
    source 12
    target 136
  ]
  edge [
    source 12
    target 137
  ]
  edge [
    source 12
    target 138
  ]
  edge [
    source 12
    target 139
  ]
  edge [
    source 12
    target 140
  ]
  edge [
    source 12
    target 141
  ]
  edge [
    source 12
    target 142
  ]
  edge [
    source 12
    target 143
  ]
  edge [
    source 12
    target 144
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 12
    target 145
  ]
  edge [
    source 12
    target 146
  ]
  edge [
    source 12
    target 147
  ]
  edge [
    source 12
    target 148
  ]
  edge [
    source 12
    target 149
  ]
  edge [
    source 12
    target 150
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 151
  ]
  edge [
    source 13
    target 152
  ]
  edge [
    source 13
    target 153
  ]
  edge [
    source 13
    target 154
  ]
  edge [
    source 13
    target 155
  ]
  edge [
    source 13
    target 156
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 157
  ]
  edge [
    source 14
    target 158
  ]
  edge [
    source 14
    target 159
  ]
  edge [
    source 14
    target 160
  ]
  edge [
    source 14
    target 161
  ]
  edge [
    source 14
    target 162
  ]
  edge [
    source 14
    target 163
  ]
  edge [
    source 14
    target 164
  ]
  edge [
    source 14
    target 165
  ]
  edge [
    source 14
    target 166
  ]
  edge [
    source 14
    target 167
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 168
  ]
  edge [
    source 16
    target 169
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 170
  ]
  edge [
    source 17
    target 103
  ]
  edge [
    source 17
    target 171
  ]
  edge [
    source 17
    target 172
  ]
  edge [
    source 17
    target 173
  ]
  edge [
    source 17
    target 174
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 175
  ]
  edge [
    source 19
    target 176
  ]
  edge [
    source 19
    target 177
  ]
  edge [
    source 19
    target 178
  ]
  edge [
    source 19
    target 179
  ]
  edge [
    source 19
    target 180
  ]
  edge [
    source 19
    target 181
  ]
  edge [
    source 19
    target 182
  ]
  edge [
    source 19
    target 183
  ]
  edge [
    source 19
    target 184
  ]
  edge [
    source 19
    target 185
  ]
  edge [
    source 19
    target 186
  ]
  edge [
    source 19
    target 187
  ]
  edge [
    source 19
    target 188
  ]
  edge [
    source 21
    target 87
  ]
  edge [
    source 21
    target 127
  ]
  edge [
    source 21
    target 189
  ]
  edge [
    source 21
    target 190
  ]
  edge [
    source 21
    target 191
  ]
  edge [
    source 21
    target 129
  ]
  edge [
    source 21
    target 192
  ]
  edge [
    source 21
    target 193
  ]
  edge [
    source 21
    target 133
  ]
  edge [
    source 21
    target 194
  ]
  edge [
    source 21
    target 195
  ]
  edge [
    source 21
    target 134
  ]
  edge [
    source 21
    target 196
  ]
  edge [
    source 21
    target 136
  ]
  edge [
    source 21
    target 197
  ]
  edge [
    source 21
    target 70
  ]
  edge [
    source 21
    target 140
  ]
  edge [
    source 21
    target 142
  ]
  edge [
    source 21
    target 198
  ]
  edge [
    source 21
    target 199
  ]
  edge [
    source 21
    target 145
  ]
  edge [
    source 21
    target 146
  ]
  edge [
    source 21
    target 200
  ]
  edge [
    source 21
    target 149
  ]
  edge [
    source 21
    target 201
  ]
]
