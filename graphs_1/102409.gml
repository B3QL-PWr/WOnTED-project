graph [
  maxDegree 46
  minDegree 1
  meanDegree 2.074074074074074
  density 0.009646856158484065
  graphCliqueNumber 3
  node [
    id 0
    label "wrzesie&#324;"
    origin "text"
  ]
  node [
    id 1
    label "rok"
    origin "text"
  ]
  node [
    id 2
    label "realizowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "projekt"
    origin "text"
  ]
  node [
    id 5
    label "edycja"
    origin "text"
  ]
  node [
    id 6
    label "szko&#322;a"
    origin "text"
  ]
  node [
    id 7
    label "m&#322;odzie&#380;owy"
    origin "text"
  ]
  node [
    id 8
    label "akcja"
    origin "text"
  ]
  node [
    id 9
    label "multimedialny"
    origin "text"
  ]
  node [
    id 10
    label "bra&#263;"
    origin "text"
  ]
  node [
    id 11
    label "nim"
    origin "text"
  ]
  node [
    id 12
    label "udzia&#322;"
    origin "text"
  ]
  node [
    id 13
    label "redakcja"
    origin "text"
  ]
  node [
    id 14
    label "qmam&#243;w"
    origin "text"
  ]
  node [
    id 15
    label "wybrana"
    origin "text"
  ]
  node [
    id 16
    label "czas"
    origin "text"
  ]
  node [
    id 17
    label "rekrutacja"
    origin "text"
  ]
  node [
    id 18
    label "miesi&#261;c"
  ]
  node [
    id 19
    label "stulecie"
  ]
  node [
    id 20
    label "kalendarz"
  ]
  node [
    id 21
    label "pora_roku"
  ]
  node [
    id 22
    label "cykl_astronomiczny"
  ]
  node [
    id 23
    label "p&#243;&#322;rocze"
  ]
  node [
    id 24
    label "grupa"
  ]
  node [
    id 25
    label "kwarta&#322;"
  ]
  node [
    id 26
    label "kurs"
  ]
  node [
    id 27
    label "jubileusz"
  ]
  node [
    id 28
    label "lata"
  ]
  node [
    id 29
    label "martwy_sezon"
  ]
  node [
    id 30
    label "wykorzystywa&#263;"
  ]
  node [
    id 31
    label "przeprowadza&#263;"
  ]
  node [
    id 32
    label "spieni&#281;&#380;a&#263;"
  ]
  node [
    id 33
    label "prosecute"
  ]
  node [
    id 34
    label "create"
  ]
  node [
    id 35
    label "prawdzi&#263;"
  ]
  node [
    id 36
    label "tworzy&#263;"
  ]
  node [
    id 37
    label "si&#281;ga&#263;"
  ]
  node [
    id 38
    label "trwa&#263;"
  ]
  node [
    id 39
    label "obecno&#347;&#263;"
  ]
  node [
    id 40
    label "stan"
  ]
  node [
    id 41
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 42
    label "stand"
  ]
  node [
    id 43
    label "mie&#263;_miejsce"
  ]
  node [
    id 44
    label "uczestniczy&#263;"
  ]
  node [
    id 45
    label "chodzi&#263;"
  ]
  node [
    id 46
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 47
    label "equal"
  ]
  node [
    id 48
    label "dokument"
  ]
  node [
    id 49
    label "device"
  ]
  node [
    id 50
    label "program_u&#380;ytkowy"
  ]
  node [
    id 51
    label "intencja"
  ]
  node [
    id 52
    label "agreement"
  ]
  node [
    id 53
    label "pomys&#322;"
  ]
  node [
    id 54
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 55
    label "plan"
  ]
  node [
    id 56
    label "dokumentacja"
  ]
  node [
    id 57
    label "impression"
  ]
  node [
    id 58
    label "odmiana"
  ]
  node [
    id 59
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 60
    label "notification"
  ]
  node [
    id 61
    label "cykl"
  ]
  node [
    id 62
    label "zmiana"
  ]
  node [
    id 63
    label "produkcja"
  ]
  node [
    id 64
    label "egzemplarz"
  ]
  node [
    id 65
    label "Mickiewicz"
  ]
  node [
    id 66
    label "szkolenie"
  ]
  node [
    id 67
    label "przepisa&#263;"
  ]
  node [
    id 68
    label "lesson"
  ]
  node [
    id 69
    label "praktyka"
  ]
  node [
    id 70
    label "metoda"
  ]
  node [
    id 71
    label "niepokalanki"
  ]
  node [
    id 72
    label "kara"
  ]
  node [
    id 73
    label "zda&#263;"
  ]
  node [
    id 74
    label "form"
  ]
  node [
    id 75
    label "kwalifikacje"
  ]
  node [
    id 76
    label "system"
  ]
  node [
    id 77
    label "przepisanie"
  ]
  node [
    id 78
    label "sztuba"
  ]
  node [
    id 79
    label "wiedza"
  ]
  node [
    id 80
    label "stopek"
  ]
  node [
    id 81
    label "school"
  ]
  node [
    id 82
    label "absolwent"
  ]
  node [
    id 83
    label "urszulanki"
  ]
  node [
    id 84
    label "gabinet"
  ]
  node [
    id 85
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 86
    label "ideologia"
  ]
  node [
    id 87
    label "lekcja"
  ]
  node [
    id 88
    label "muzyka"
  ]
  node [
    id 89
    label "podr&#281;cznik"
  ]
  node [
    id 90
    label "zdanie"
  ]
  node [
    id 91
    label "siedziba"
  ]
  node [
    id 92
    label "sekretariat"
  ]
  node [
    id 93
    label "nauka"
  ]
  node [
    id 94
    label "do&#347;wiadczenie"
  ]
  node [
    id 95
    label "tablica"
  ]
  node [
    id 96
    label "teren_szko&#322;y"
  ]
  node [
    id 97
    label "instytucja"
  ]
  node [
    id 98
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 99
    label "skolaryzacja"
  ]
  node [
    id 100
    label "&#322;awa_szkolna"
  ]
  node [
    id 101
    label "klasa"
  ]
  node [
    id 102
    label "m&#322;odzie&#380;owo"
  ]
  node [
    id 103
    label "zagrywka"
  ]
  node [
    id 104
    label "czyn"
  ]
  node [
    id 105
    label "czynno&#347;&#263;"
  ]
  node [
    id 106
    label "wysoko&#347;&#263;"
  ]
  node [
    id 107
    label "stock"
  ]
  node [
    id 108
    label "gra"
  ]
  node [
    id 109
    label "w&#281;ze&#322;"
  ]
  node [
    id 110
    label "instrument_strunowy"
  ]
  node [
    id 111
    label "dywidenda"
  ]
  node [
    id 112
    label "przebieg"
  ]
  node [
    id 113
    label "occupation"
  ]
  node [
    id 114
    label "jazda"
  ]
  node [
    id 115
    label "wydarzenie"
  ]
  node [
    id 116
    label "commotion"
  ]
  node [
    id 117
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 118
    label "sp&#243;&#322;ka_akcyjna"
  ]
  node [
    id 119
    label "operacja"
  ]
  node [
    id 120
    label "multimedialnie"
  ]
  node [
    id 121
    label "get"
  ]
  node [
    id 122
    label "przewa&#380;a&#263;"
  ]
  node [
    id 123
    label "zobowi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 124
    label "poczytywa&#263;"
  ]
  node [
    id 125
    label "levy"
  ]
  node [
    id 126
    label "pokonywa&#263;"
  ]
  node [
    id 127
    label "u&#380;ywa&#263;"
  ]
  node [
    id 128
    label "rusza&#263;"
  ]
  node [
    id 129
    label "zalicza&#263;"
  ]
  node [
    id 130
    label "wygrywa&#263;"
  ]
  node [
    id 131
    label "open"
  ]
  node [
    id 132
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 133
    label "branie"
  ]
  node [
    id 134
    label "korzysta&#263;"
  ]
  node [
    id 135
    label "&#263;pa&#263;"
  ]
  node [
    id 136
    label "wch&#322;ania&#263;"
  ]
  node [
    id 137
    label "interpretowa&#263;"
  ]
  node [
    id 138
    label "atakowa&#263;"
  ]
  node [
    id 139
    label "prowadzi&#263;"
  ]
  node [
    id 140
    label "rucha&#263;"
  ]
  node [
    id 141
    label "take"
  ]
  node [
    id 142
    label "dostawa&#263;"
  ]
  node [
    id 143
    label "wzi&#261;&#263;"
  ]
  node [
    id 144
    label "wk&#322;ada&#263;"
  ]
  node [
    id 145
    label "chwyta&#263;"
  ]
  node [
    id 146
    label "arise"
  ]
  node [
    id 147
    label "za&#380;ywa&#263;"
  ]
  node [
    id 148
    label "uprawia&#263;_seks"
  ]
  node [
    id 149
    label "porywa&#263;"
  ]
  node [
    id 150
    label "robi&#263;"
  ]
  node [
    id 151
    label "grza&#263;"
  ]
  node [
    id 152
    label "abstract"
  ]
  node [
    id 153
    label "zaopatrywa&#263;_si&#281;"
  ]
  node [
    id 154
    label "wzmaga&#263;_si&#281;"
  ]
  node [
    id 155
    label "towarzystwo"
  ]
  node [
    id 156
    label "otrzymywa&#263;"
  ]
  node [
    id 157
    label "przyjmowa&#263;"
  ]
  node [
    id 158
    label "wchodzi&#263;"
  ]
  node [
    id 159
    label "ucieka&#263;"
  ]
  node [
    id 160
    label "odurza&#263;_si&#281;"
  ]
  node [
    id 161
    label "buzowa&#263;_si&#281;"
  ]
  node [
    id 162
    label "&#322;apa&#263;"
  ]
  node [
    id 163
    label "raise"
  ]
  node [
    id 164
    label "gra_planszowa"
  ]
  node [
    id 165
    label "kwota"
  ]
  node [
    id 166
    label "ilo&#347;&#263;"
  ]
  node [
    id 167
    label "telewizja"
  ]
  node [
    id 168
    label "redaction"
  ]
  node [
    id 169
    label "obr&#243;bka"
  ]
  node [
    id 170
    label "radio"
  ]
  node [
    id 171
    label "wydawnictwo"
  ]
  node [
    id 172
    label "zesp&#243;&#322;"
  ]
  node [
    id 173
    label "redaktor"
  ]
  node [
    id 174
    label "composition"
  ]
  node [
    id 175
    label "tekst"
  ]
  node [
    id 176
    label "czasokres"
  ]
  node [
    id 177
    label "trawienie"
  ]
  node [
    id 178
    label "kategoria_gramatyczna"
  ]
  node [
    id 179
    label "period"
  ]
  node [
    id 180
    label "odczyt"
  ]
  node [
    id 181
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 182
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 183
    label "chwila"
  ]
  node [
    id 184
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 185
    label "poprzedzenie"
  ]
  node [
    id 186
    label "koniugacja"
  ]
  node [
    id 187
    label "dzieje"
  ]
  node [
    id 188
    label "poprzedzi&#263;"
  ]
  node [
    id 189
    label "przep&#322;ywanie"
  ]
  node [
    id 190
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 191
    label "odwlekanie_si&#281;"
  ]
  node [
    id 192
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 193
    label "Zeitgeist"
  ]
  node [
    id 194
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 195
    label "okres_czasu"
  ]
  node [
    id 196
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 197
    label "pochodzi&#263;"
  ]
  node [
    id 198
    label "schy&#322;ek"
  ]
  node [
    id 199
    label "czwarty_wymiar"
  ]
  node [
    id 200
    label "chronometria"
  ]
  node [
    id 201
    label "poprzedzanie"
  ]
  node [
    id 202
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 203
    label "pogoda"
  ]
  node [
    id 204
    label "zegar"
  ]
  node [
    id 205
    label "trawi&#263;"
  ]
  node [
    id 206
    label "pochodzenie"
  ]
  node [
    id 207
    label "poprzedza&#263;"
  ]
  node [
    id 208
    label "time_period"
  ]
  node [
    id 209
    label "rachuba_czasu"
  ]
  node [
    id 210
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 211
    label "czasoprzestrze&#324;"
  ]
  node [
    id 212
    label "laba"
  ]
  node [
    id 213
    label "wyb&#243;r"
  ]
  node [
    id 214
    label "recruitment"
  ]
  node [
    id 215
    label "nab&#243;r"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 16
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 24
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 12
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 121
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 10
    target 125
  ]
  edge [
    source 10
    target 126
  ]
  edge [
    source 10
    target 127
  ]
  edge [
    source 10
    target 128
  ]
  edge [
    source 10
    target 129
  ]
  edge [
    source 10
    target 130
  ]
  edge [
    source 10
    target 131
  ]
  edge [
    source 10
    target 132
  ]
  edge [
    source 10
    target 133
  ]
  edge [
    source 10
    target 134
  ]
  edge [
    source 10
    target 135
  ]
  edge [
    source 10
    target 136
  ]
  edge [
    source 10
    target 137
  ]
  edge [
    source 10
    target 138
  ]
  edge [
    source 10
    target 139
  ]
  edge [
    source 10
    target 140
  ]
  edge [
    source 10
    target 141
  ]
  edge [
    source 10
    target 142
  ]
  edge [
    source 10
    target 143
  ]
  edge [
    source 10
    target 144
  ]
  edge [
    source 10
    target 145
  ]
  edge [
    source 10
    target 146
  ]
  edge [
    source 10
    target 147
  ]
  edge [
    source 10
    target 148
  ]
  edge [
    source 10
    target 149
  ]
  edge [
    source 10
    target 150
  ]
  edge [
    source 10
    target 151
  ]
  edge [
    source 10
    target 152
  ]
  edge [
    source 10
    target 153
  ]
  edge [
    source 10
    target 154
  ]
  edge [
    source 10
    target 155
  ]
  edge [
    source 10
    target 156
  ]
  edge [
    source 10
    target 157
  ]
  edge [
    source 10
    target 158
  ]
  edge [
    source 10
    target 159
  ]
  edge [
    source 10
    target 160
  ]
  edge [
    source 10
    target 161
  ]
  edge [
    source 10
    target 162
  ]
  edge [
    source 10
    target 163
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 164
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 39
  ]
  edge [
    source 12
    target 59
  ]
  edge [
    source 12
    target 165
  ]
  edge [
    source 12
    target 166
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 167
  ]
  edge [
    source 13
    target 168
  ]
  edge [
    source 13
    target 169
  ]
  edge [
    source 13
    target 170
  ]
  edge [
    source 13
    target 171
  ]
  edge [
    source 13
    target 172
  ]
  edge [
    source 13
    target 173
  ]
  edge [
    source 13
    target 91
  ]
  edge [
    source 13
    target 174
  ]
  edge [
    source 13
    target 175
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 176
  ]
  edge [
    source 16
    target 177
  ]
  edge [
    source 16
    target 178
  ]
  edge [
    source 16
    target 179
  ]
  edge [
    source 16
    target 180
  ]
  edge [
    source 16
    target 181
  ]
  edge [
    source 16
    target 182
  ]
  edge [
    source 16
    target 183
  ]
  edge [
    source 16
    target 184
  ]
  edge [
    source 16
    target 185
  ]
  edge [
    source 16
    target 186
  ]
  edge [
    source 16
    target 187
  ]
  edge [
    source 16
    target 188
  ]
  edge [
    source 16
    target 189
  ]
  edge [
    source 16
    target 190
  ]
  edge [
    source 16
    target 191
  ]
  edge [
    source 16
    target 192
  ]
  edge [
    source 16
    target 193
  ]
  edge [
    source 16
    target 194
  ]
  edge [
    source 16
    target 195
  ]
  edge [
    source 16
    target 196
  ]
  edge [
    source 16
    target 197
  ]
  edge [
    source 16
    target 198
  ]
  edge [
    source 16
    target 199
  ]
  edge [
    source 16
    target 200
  ]
  edge [
    source 16
    target 201
  ]
  edge [
    source 16
    target 202
  ]
  edge [
    source 16
    target 203
  ]
  edge [
    source 16
    target 204
  ]
  edge [
    source 16
    target 205
  ]
  edge [
    source 16
    target 206
  ]
  edge [
    source 16
    target 207
  ]
  edge [
    source 16
    target 208
  ]
  edge [
    source 16
    target 209
  ]
  edge [
    source 16
    target 210
  ]
  edge [
    source 16
    target 211
  ]
  edge [
    source 16
    target 212
  ]
  edge [
    source 17
    target 213
  ]
  edge [
    source 17
    target 214
  ]
  edge [
    source 17
    target 215
  ]
]
