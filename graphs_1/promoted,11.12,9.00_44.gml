graph [
  maxDegree 36
  minDegree 1
  meanDegree 1.9811320754716981
  density 0.018867924528301886
  graphCliqueNumber 2
  node [
    id 0
    label "pierwsza"
    origin "text"
  ]
  node [
    id 1
    label "pom&#347;ci&#263;"
    origin "text"
  ]
  node [
    id 2
    label "&#347;mier&#263;"
    origin "text"
  ]
  node [
    id 3
    label "po&#347;lubi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "przez"
    origin "text"
  ]
  node [
    id 5
    label "m&#281;&#380;czyzna"
    origin "text"
  ]
  node [
    id 6
    label "nakazywa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "wymordowa&#263;"
    origin "text"
  ]
  node [
    id 8
    label "pose&#322;"
    origin "text"
  ]
  node [
    id 9
    label "wrogi"
    origin "text"
  ]
  node [
    id 10
    label "plemi&#281;"
    origin "text"
  ]
  node [
    id 11
    label "godzina"
  ]
  node [
    id 12
    label "cook"
  ]
  node [
    id 13
    label "odbi&#263;_si&#281;"
  ]
  node [
    id 14
    label "odwzajemni&#263;_si&#281;"
  ]
  node [
    id 15
    label "defenestracja"
  ]
  node [
    id 16
    label "kres"
  ]
  node [
    id 17
    label "agonia"
  ]
  node [
    id 18
    label "&#380;ycie"
  ]
  node [
    id 19
    label "szeol"
  ]
  node [
    id 20
    label "mogi&#322;a"
  ]
  node [
    id 21
    label "pogrzeb"
  ]
  node [
    id 22
    label "istota_nadprzyrodzona"
  ]
  node [
    id 23
    label "&#380;a&#322;oba"
  ]
  node [
    id 24
    label "pogrzebanie"
  ]
  node [
    id 25
    label "upadek"
  ]
  node [
    id 26
    label "zabicie"
  ]
  node [
    id 27
    label "kres_&#380;ycia"
  ]
  node [
    id 28
    label "chajtn&#261;&#263;_si&#281;"
  ]
  node [
    id 29
    label "marry"
  ]
  node [
    id 30
    label "spowodowa&#263;"
  ]
  node [
    id 31
    label "ch&#322;opina"
  ]
  node [
    id 32
    label "cz&#322;owiek"
  ]
  node [
    id 33
    label "jegomo&#347;&#263;"
  ]
  node [
    id 34
    label "bratek"
  ]
  node [
    id 35
    label "doros&#322;y"
  ]
  node [
    id 36
    label "samiec"
  ]
  node [
    id 37
    label "ojciec"
  ]
  node [
    id 38
    label "twardziel"
  ]
  node [
    id 39
    label "androlog"
  ]
  node [
    id 40
    label "pa&#324;stwo"
  ]
  node [
    id 41
    label "m&#261;&#380;"
  ]
  node [
    id 42
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 43
    label "andropauza"
  ]
  node [
    id 44
    label "pakowa&#263;"
  ]
  node [
    id 45
    label "wymaga&#263;"
  ]
  node [
    id 46
    label "command"
  ]
  node [
    id 47
    label "poleca&#263;"
  ]
  node [
    id 48
    label "inflict"
  ]
  node [
    id 49
    label "pomordowa&#263;"
  ]
  node [
    id 50
    label "wybi&#263;"
  ]
  node [
    id 51
    label "killing"
  ]
  node [
    id 52
    label "dyplomata"
  ]
  node [
    id 53
    label "wys&#322;annik"
  ]
  node [
    id 54
    label "przedstawiciel"
  ]
  node [
    id 55
    label "kurier_dyplomatyczny"
  ]
  node [
    id 56
    label "ablegat"
  ]
  node [
    id 57
    label "klubista"
  ]
  node [
    id 58
    label "Miko&#322;ajczyk"
  ]
  node [
    id 59
    label "Korwin"
  ]
  node [
    id 60
    label "parlamentarzysta"
  ]
  node [
    id 61
    label "dyscyplina_partyjna"
  ]
  node [
    id 62
    label "izba_ni&#380;sza"
  ]
  node [
    id 63
    label "poselstwo"
  ]
  node [
    id 64
    label "antagonizowanie"
  ]
  node [
    id 65
    label "negatywny"
  ]
  node [
    id 66
    label "obcy"
  ]
  node [
    id 67
    label "wrogo"
  ]
  node [
    id 68
    label "nieprzyjacielsko"
  ]
  node [
    id 69
    label "zantagonizowanie"
  ]
  node [
    id 70
    label "nieprzyjemny"
  ]
  node [
    id 71
    label "Kipczacy"
  ]
  node [
    id 72
    label "Do&#322;ganie"
  ]
  node [
    id 73
    label "Nawahowie"
  ]
  node [
    id 74
    label "Wizygoci"
  ]
  node [
    id 75
    label "Paleoazjaci"
  ]
  node [
    id 76
    label "Indoariowie"
  ]
  node [
    id 77
    label "Macziguengowie"
  ]
  node [
    id 78
    label "Kumbrowie"
  ]
  node [
    id 79
    label "Antowie"
  ]
  node [
    id 80
    label "Polanie"
  ]
  node [
    id 81
    label "Indoira&#324;czycy"
  ]
  node [
    id 82
    label "Ugrowie"
  ]
  node [
    id 83
    label "moiety"
  ]
  node [
    id 84
    label "Drzewianie"
  ]
  node [
    id 85
    label "Kozacy"
  ]
  node [
    id 86
    label "rodzina"
  ]
  node [
    id 87
    label "Negryci"
  ]
  node [
    id 88
    label "Obodryci"
  ]
  node [
    id 89
    label "Nogajowie"
  ]
  node [
    id 90
    label "Wenedowie"
  ]
  node [
    id 91
    label "Dogonowie"
  ]
  node [
    id 92
    label "Retowie"
  ]
  node [
    id 93
    label "Po&#322;owcy"
  ]
  node [
    id 94
    label "fratria"
  ]
  node [
    id 95
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 96
    label "Tagalowie"
  ]
  node [
    id 97
    label "szczep"
  ]
  node [
    id 98
    label "Majowie"
  ]
  node [
    id 99
    label "Frygijczycy"
  ]
  node [
    id 100
    label "Maroni"
  ]
  node [
    id 101
    label "jednostka_systematyczna"
  ]
  node [
    id 102
    label "lud"
  ]
  node [
    id 103
    label "Ladynowie"
  ]
  node [
    id 104
    label "Tocharowie"
  ]
  node [
    id 105
    label "Achajowie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 10
    target 71
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 10
    target 105
  ]
]
