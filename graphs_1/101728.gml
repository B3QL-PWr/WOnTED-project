graph [
  maxDegree 16
  minDegree 1
  meanDegree 1.9473684210526316
  density 0.05263157894736842
  graphCliqueNumber 2
  node [
    id 0
    label "rozumienie"
    origin "text"
  ]
  node [
    id 1
    label "niniejszy"
    origin "text"
  ]
  node [
    id 2
    label "umowa"
    origin "text"
  ]
  node [
    id 3
    label "okre&#347;lenie"
    origin "text"
  ]
  node [
    id 4
    label "robienie"
  ]
  node [
    id 5
    label "czynno&#347;&#263;"
  ]
  node [
    id 6
    label "czucie"
  ]
  node [
    id 7
    label "wytw&#243;r"
  ]
  node [
    id 8
    label "wnioskowanie"
  ]
  node [
    id 9
    label "bycie"
  ]
  node [
    id 10
    label "bycie_&#347;wiadomym"
  ]
  node [
    id 11
    label "kontekst"
  ]
  node [
    id 12
    label "apprehension"
  ]
  node [
    id 13
    label "j&#281;zyk"
  ]
  node [
    id 14
    label "kumanie"
  ]
  node [
    id 15
    label "obja&#347;nienie"
  ]
  node [
    id 16
    label "hermeneutyka"
  ]
  node [
    id 17
    label "interpretation"
  ]
  node [
    id 18
    label "realization"
  ]
  node [
    id 19
    label "ten"
  ]
  node [
    id 20
    label "czyn"
  ]
  node [
    id 21
    label "contract"
  ]
  node [
    id 22
    label "gestia_transportowa"
  ]
  node [
    id 23
    label "zawrze&#263;"
  ]
  node [
    id 24
    label "klauzula"
  ]
  node [
    id 25
    label "porozumienie"
  ]
  node [
    id 26
    label "warunek"
  ]
  node [
    id 27
    label "zawarcie"
  ]
  node [
    id 28
    label "zdecydowanie"
  ]
  node [
    id 29
    label "follow-up"
  ]
  node [
    id 30
    label "appointment"
  ]
  node [
    id 31
    label "ustalenie"
  ]
  node [
    id 32
    label "localization"
  ]
  node [
    id 33
    label "denomination"
  ]
  node [
    id 34
    label "wyra&#380;enie"
  ]
  node [
    id 35
    label "ozdobnik"
  ]
  node [
    id 36
    label "przewidzenie"
  ]
  node [
    id 37
    label "term"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
]
