graph [
  maxDegree 27
  minDegree 1
  meanDegree 2.0208333333333335
  density 0.021271929824561404
  graphCliqueNumber 3
  node [
    id 0
    label "kolejny"
    origin "text"
  ]
  node [
    id 1
    label "dawka"
    origin "text"
  ]
  node [
    id 2
    label "brak"
    origin "text"
  ]
  node [
    id 3
    label "rozwaga"
    origin "text"
  ]
  node [
    id 4
    label "nieostro&#380;no&#347;&#263;"
    origin "text"
  ]
  node [
    id 5
    label "g&#322;upota"
    origin "text"
  ]
  node [
    id 6
    label "prosto"
    origin "text"
  ]
  node [
    id 7
    label "polski"
    origin "text"
  ]
  node [
    id 8
    label "droga"
    origin "text"
  ]
  node [
    id 9
    label "inny"
  ]
  node [
    id 10
    label "nast&#281;pnie"
  ]
  node [
    id 11
    label "kt&#243;ry&#347;"
  ]
  node [
    id 12
    label "kolejno"
  ]
  node [
    id 13
    label "nastopny"
  ]
  node [
    id 14
    label "porcja"
  ]
  node [
    id 15
    label "zas&#243;b"
  ]
  node [
    id 16
    label "prywatywny"
  ]
  node [
    id 17
    label "defect"
  ]
  node [
    id 18
    label "odej&#347;cie"
  ]
  node [
    id 19
    label "gap"
  ]
  node [
    id 20
    label "kr&#243;tki"
  ]
  node [
    id 21
    label "wyr&#243;b"
  ]
  node [
    id 22
    label "nieistnienie"
  ]
  node [
    id 23
    label "wada"
  ]
  node [
    id 24
    label "odej&#347;&#263;"
  ]
  node [
    id 25
    label "odchodzenie"
  ]
  node [
    id 26
    label "odchodzi&#263;"
  ]
  node [
    id 27
    label "circumspection"
  ]
  node [
    id 28
    label "ostro&#380;no&#347;&#263;"
  ]
  node [
    id 29
    label "rozs&#261;dek"
  ]
  node [
    id 30
    label "czyn"
  ]
  node [
    id 31
    label "cecha"
  ]
  node [
    id 32
    label "banalny"
  ]
  node [
    id 33
    label "pomroczno&#347;&#263;_jasna"
  ]
  node [
    id 34
    label "szczeg&#243;&#322;"
  ]
  node [
    id 35
    label "furda"
  ]
  node [
    id 36
    label "g&#243;wno"
  ]
  node [
    id 37
    label "stupidity"
  ]
  node [
    id 38
    label "sofcik"
  ]
  node [
    id 39
    label "g&#322;upstwo"
  ]
  node [
    id 40
    label "osielstwo"
  ]
  node [
    id 41
    label "intelekt"
  ]
  node [
    id 42
    label "elementarily"
  ]
  node [
    id 43
    label "bezpo&#347;rednio"
  ]
  node [
    id 44
    label "naturalnie"
  ]
  node [
    id 45
    label "skromnie"
  ]
  node [
    id 46
    label "prosty"
  ]
  node [
    id 47
    label "&#322;atwo"
  ]
  node [
    id 48
    label "niepozornie"
  ]
  node [
    id 49
    label "lacki"
  ]
  node [
    id 50
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 51
    label "przedmiot"
  ]
  node [
    id 52
    label "sztajer"
  ]
  node [
    id 53
    label "drabant"
  ]
  node [
    id 54
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 55
    label "polak"
  ]
  node [
    id 56
    label "pierogi_ruskie"
  ]
  node [
    id 57
    label "krakowiak"
  ]
  node [
    id 58
    label "Polish"
  ]
  node [
    id 59
    label "j&#281;zyk"
  ]
  node [
    id 60
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 61
    label "oberek"
  ]
  node [
    id 62
    label "po_polsku"
  ]
  node [
    id 63
    label "mazur"
  ]
  node [
    id 64
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 65
    label "chodzony"
  ]
  node [
    id 66
    label "skoczny"
  ]
  node [
    id 67
    label "ryba_po_grecku"
  ]
  node [
    id 68
    label "goniony"
  ]
  node [
    id 69
    label "polsko"
  ]
  node [
    id 70
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 71
    label "journey"
  ]
  node [
    id 72
    label "podbieg"
  ]
  node [
    id 73
    label "bezsilnikowy"
  ]
  node [
    id 74
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 75
    label "wylot"
  ]
  node [
    id 76
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 77
    label "drogowskaz"
  ]
  node [
    id 78
    label "nawierzchnia"
  ]
  node [
    id 79
    label "turystyka"
  ]
  node [
    id 80
    label "budowla"
  ]
  node [
    id 81
    label "spos&#243;b"
  ]
  node [
    id 82
    label "passage"
  ]
  node [
    id 83
    label "marszrutyzacja"
  ]
  node [
    id 84
    label "zbior&#243;wka"
  ]
  node [
    id 85
    label "rajza"
  ]
  node [
    id 86
    label "ekskursja"
  ]
  node [
    id 87
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 88
    label "ruch"
  ]
  node [
    id 89
    label "trasa"
  ]
  node [
    id 90
    label "wyb&#243;j"
  ]
  node [
    id 91
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 92
    label "ekwipunek"
  ]
  node [
    id 93
    label "korona_drogi"
  ]
  node [
    id 94
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 95
    label "pobocze"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
]
