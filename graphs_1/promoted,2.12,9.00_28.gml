graph [
  maxDegree 25
  minDegree 1
  meanDegree 2
  density 0.021052631578947368
  graphCliqueNumber 3
  node [
    id 0
    label "dokona&#263;"
    origin "text"
  ]
  node [
    id 1
    label "odkrycie"
    origin "text"
  ]
  node [
    id 2
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 3
    label "podwa&#380;a&#263;"
    origin "text"
  ]
  node [
    id 4
    label "wa&#380;ny"
    origin "text"
  ]
  node [
    id 5
    label "informacja"
    origin "text"
  ]
  node [
    id 6
    label "przekazywa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "dawny"
    origin "text"
  ]
  node [
    id 8
    label "temat"
    origin "text"
  ]
  node [
    id 9
    label "elektrownia"
    origin "text"
  ]
  node [
    id 10
    label "przesta&#263;"
  ]
  node [
    id 11
    label "zrobi&#263;"
  ]
  node [
    id 12
    label "cause"
  ]
  node [
    id 13
    label "communicate"
  ]
  node [
    id 14
    label "objawienie"
  ]
  node [
    id 15
    label "jawny"
  ]
  node [
    id 16
    label "zsuni&#281;cie"
  ]
  node [
    id 17
    label "znalezienie"
  ]
  node [
    id 18
    label "poinformowanie"
  ]
  node [
    id 19
    label "novum"
  ]
  node [
    id 20
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 21
    label "zwr&#243;cenie_uwagi"
  ]
  node [
    id 22
    label "ukazanie"
  ]
  node [
    id 23
    label "disclosure"
  ]
  node [
    id 24
    label "ods&#322;oni&#281;cie_si&#281;"
  ]
  node [
    id 25
    label "poznanie"
  ]
  node [
    id 26
    label "podniesienie"
  ]
  node [
    id 27
    label "detection"
  ]
  node [
    id 28
    label "discovery"
  ]
  node [
    id 29
    label "niespodzianka"
  ]
  node [
    id 30
    label "podnosi&#263;"
  ]
  node [
    id 31
    label "sabotage"
  ]
  node [
    id 32
    label "silny"
  ]
  node [
    id 33
    label "wa&#380;nie"
  ]
  node [
    id 34
    label "eksponowany"
  ]
  node [
    id 35
    label "istotnie"
  ]
  node [
    id 36
    label "znaczny"
  ]
  node [
    id 37
    label "dobry"
  ]
  node [
    id 38
    label "wynios&#322;y"
  ]
  node [
    id 39
    label "dono&#347;ny"
  ]
  node [
    id 40
    label "doj&#347;cie"
  ]
  node [
    id 41
    label "doj&#347;&#263;"
  ]
  node [
    id 42
    label "powzi&#261;&#263;"
  ]
  node [
    id 43
    label "wiedza"
  ]
  node [
    id 44
    label "sygna&#322;"
  ]
  node [
    id 45
    label "obiegni&#281;cie"
  ]
  node [
    id 46
    label "obieganie"
  ]
  node [
    id 47
    label "obiec"
  ]
  node [
    id 48
    label "dane"
  ]
  node [
    id 49
    label "obiega&#263;"
  ]
  node [
    id 50
    label "punkt"
  ]
  node [
    id 51
    label "publikacja"
  ]
  node [
    id 52
    label "powzi&#281;cie"
  ]
  node [
    id 53
    label "impart"
  ]
  node [
    id 54
    label "podawa&#263;"
  ]
  node [
    id 55
    label "wp&#322;aca&#263;"
  ]
  node [
    id 56
    label "give"
  ]
  node [
    id 57
    label "powodowa&#263;"
  ]
  node [
    id 58
    label "wysy&#322;a&#263;"
  ]
  node [
    id 59
    label "przesz&#322;y"
  ]
  node [
    id 60
    label "dawno"
  ]
  node [
    id 61
    label "dawniej"
  ]
  node [
    id 62
    label "kombatant"
  ]
  node [
    id 63
    label "stary"
  ]
  node [
    id 64
    label "odleg&#322;y"
  ]
  node [
    id 65
    label "anachroniczny"
  ]
  node [
    id 66
    label "przestarza&#322;y"
  ]
  node [
    id 67
    label "od_dawna"
  ]
  node [
    id 68
    label "poprzedni"
  ]
  node [
    id 69
    label "d&#322;ugoletni"
  ]
  node [
    id 70
    label "wcze&#347;niejszy"
  ]
  node [
    id 71
    label "niegdysiejszy"
  ]
  node [
    id 72
    label "fraza"
  ]
  node [
    id 73
    label "forma"
  ]
  node [
    id 74
    label "melodia"
  ]
  node [
    id 75
    label "rzecz"
  ]
  node [
    id 76
    label "zbacza&#263;"
  ]
  node [
    id 77
    label "entity"
  ]
  node [
    id 78
    label "omawia&#263;"
  ]
  node [
    id 79
    label "topik"
  ]
  node [
    id 80
    label "wyraz_pochodny"
  ]
  node [
    id 81
    label "om&#243;wi&#263;"
  ]
  node [
    id 82
    label "omawianie"
  ]
  node [
    id 83
    label "w&#261;tek"
  ]
  node [
    id 84
    label "forum"
  ]
  node [
    id 85
    label "cecha"
  ]
  node [
    id 86
    label "zboczenie"
  ]
  node [
    id 87
    label "zbaczanie"
  ]
  node [
    id 88
    label "tre&#347;&#263;"
  ]
  node [
    id 89
    label "tematyka"
  ]
  node [
    id 90
    label "sprawa"
  ]
  node [
    id 91
    label "istota"
  ]
  node [
    id 92
    label "otoczka"
  ]
  node [
    id 93
    label "zboczy&#263;"
  ]
  node [
    id 94
    label "om&#243;wienie"
  ]
  node [
    id 95
    label "zak&#322;ad_komunalny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 9
    target 95
  ]
]
