graph [
  maxDegree 44
  minDegree 1
  meanDegree 2.871794871794872
  density 0.012325299878947947
  graphCliqueNumber 8
  node [
    id 0
    label "konferencja"
    origin "text"
  ]
  node [
    id 1
    label "by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "organizowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "pod"
    origin "text"
  ]
  node [
    id 4
    label "patronat"
    origin "text"
  ]
  node [
    id 5
    label "minister"
    origin "text"
  ]
  node [
    id 6
    label "nauka"
    origin "text"
  ]
  node [
    id 7
    label "szkolnictwo"
    origin "text"
  ]
  node [
    id 8
    label "wysoki"
    origin "text"
  ]
  node [
    id 9
    label "profesor"
    origin "text"
  ]
  node [
    id 10
    label "barbara"
    origin "text"
  ]
  node [
    id 11
    label "kudryckiej"
    origin "text"
  ]
  node [
    id 12
    label "przewodnicz&#261;ca"
    origin "text"
  ]
  node [
    id 13
    label "krasp"
    origin "text"
  ]
  node [
    id 14
    label "katarzyna"
    origin "text"
  ]
  node [
    id 15
    label "cha&#322;asi&#324;skiej"
    origin "text"
  ]
  node [
    id 16
    label "macukow"
    origin "text"
  ]
  node [
    id 17
    label "prezes"
    origin "text"
  ]
  node [
    id 18
    label "pan"
    origin "text"
  ]
  node [
    id 19
    label "micha&#322;"
    origin "text"
  ]
  node [
    id 20
    label "kleibera"
    origin "text"
  ]
  node [
    id 21
    label "konferencyjka"
  ]
  node [
    id 22
    label "Poczdam"
  ]
  node [
    id 23
    label "conference"
  ]
  node [
    id 24
    label "spotkanie"
  ]
  node [
    id 25
    label "grusza_pospolita"
  ]
  node [
    id 26
    label "Ja&#322;ta"
  ]
  node [
    id 27
    label "si&#281;ga&#263;"
  ]
  node [
    id 28
    label "trwa&#263;"
  ]
  node [
    id 29
    label "obecno&#347;&#263;"
  ]
  node [
    id 30
    label "stan"
  ]
  node [
    id 31
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 32
    label "stand"
  ]
  node [
    id 33
    label "mie&#263;_miejsce"
  ]
  node [
    id 34
    label "uczestniczy&#263;"
  ]
  node [
    id 35
    label "chodzi&#263;"
  ]
  node [
    id 36
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 37
    label "equal"
  ]
  node [
    id 38
    label "planowa&#263;"
  ]
  node [
    id 39
    label "dostosowywa&#263;"
  ]
  node [
    id 40
    label "pozyskiwa&#263;"
  ]
  node [
    id 41
    label "wprowadza&#263;"
  ]
  node [
    id 42
    label "treat"
  ]
  node [
    id 43
    label "przygotowywa&#263;"
  ]
  node [
    id 44
    label "create"
  ]
  node [
    id 45
    label "ensnare"
  ]
  node [
    id 46
    label "tworzy&#263;"
  ]
  node [
    id 47
    label "standard"
  ]
  node [
    id 48
    label "skupia&#263;"
  ]
  node [
    id 49
    label "licencja"
  ]
  node [
    id 50
    label "opieka"
  ]
  node [
    id 51
    label "nadz&#243;r"
  ]
  node [
    id 52
    label "sponsorship"
  ]
  node [
    id 53
    label "Goebbels"
  ]
  node [
    id 54
    label "Sto&#322;ypin"
  ]
  node [
    id 55
    label "rz&#261;d"
  ]
  node [
    id 56
    label "dostojnik"
  ]
  node [
    id 57
    label "nauki_o_Ziemi"
  ]
  node [
    id 58
    label "teoria_naukowa"
  ]
  node [
    id 59
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 60
    label "nauki_o_poznaniu"
  ]
  node [
    id 61
    label "nomotetyczny"
  ]
  node [
    id 62
    label "metodologia"
  ]
  node [
    id 63
    label "przem&#243;wienie"
  ]
  node [
    id 64
    label "wiedza"
  ]
  node [
    id 65
    label "kultura_duchowa"
  ]
  node [
    id 66
    label "nauki_penalne"
  ]
  node [
    id 67
    label "systematyka"
  ]
  node [
    id 68
    label "inwentyka"
  ]
  node [
    id 69
    label "dziedzina"
  ]
  node [
    id 70
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 71
    label "miasteczko_rowerowe"
  ]
  node [
    id 72
    label "fotowoltaika"
  ]
  node [
    id 73
    label "porada"
  ]
  node [
    id 74
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 75
    label "proces"
  ]
  node [
    id 76
    label "imagineskopia"
  ]
  node [
    id 77
    label "typologia"
  ]
  node [
    id 78
    label "&#322;awa_szkolna"
  ]
  node [
    id 79
    label "gospodarka"
  ]
  node [
    id 80
    label "program_nauczania"
  ]
  node [
    id 81
    label "Karta_Nauczyciela"
  ]
  node [
    id 82
    label "warto&#347;ciowy"
  ]
  node [
    id 83
    label "du&#380;y"
  ]
  node [
    id 84
    label "wysoce"
  ]
  node [
    id 85
    label "daleki"
  ]
  node [
    id 86
    label "znaczny"
  ]
  node [
    id 87
    label "wysoko"
  ]
  node [
    id 88
    label "szczytnie"
  ]
  node [
    id 89
    label "wznios&#322;y"
  ]
  node [
    id 90
    label "wyrafinowany"
  ]
  node [
    id 91
    label "z_wysoka"
  ]
  node [
    id 92
    label "chwalebny"
  ]
  node [
    id 93
    label "uprzywilejowany"
  ]
  node [
    id 94
    label "niepo&#347;ledni"
  ]
  node [
    id 95
    label "wirtuoz"
  ]
  node [
    id 96
    label "nauczyciel_akademicki"
  ]
  node [
    id 97
    label "tytu&#322;"
  ]
  node [
    id 98
    label "konsulent"
  ]
  node [
    id 99
    label "nauczyciel"
  ]
  node [
    id 100
    label "stopie&#324;_naukowy"
  ]
  node [
    id 101
    label "profesura"
  ]
  node [
    id 102
    label "gruba_ryba"
  ]
  node [
    id 103
    label "zwierzchnik"
  ]
  node [
    id 104
    label "cz&#322;owiek"
  ]
  node [
    id 105
    label "kszta&#322;ciciel"
  ]
  node [
    id 106
    label "jegomo&#347;&#263;"
  ]
  node [
    id 107
    label "zwrot"
  ]
  node [
    id 108
    label "pracodawca"
  ]
  node [
    id 109
    label "rz&#261;dzenie"
  ]
  node [
    id 110
    label "m&#261;&#380;"
  ]
  node [
    id 111
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 112
    label "ch&#322;opina"
  ]
  node [
    id 113
    label "bratek"
  ]
  node [
    id 114
    label "opiekun"
  ]
  node [
    id 115
    label "doros&#322;y"
  ]
  node [
    id 116
    label "preceptor"
  ]
  node [
    id 117
    label "Midas"
  ]
  node [
    id 118
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 119
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 120
    label "murza"
  ]
  node [
    id 121
    label "ojciec"
  ]
  node [
    id 122
    label "androlog"
  ]
  node [
    id 123
    label "pupil"
  ]
  node [
    id 124
    label "efendi"
  ]
  node [
    id 125
    label "nabab"
  ]
  node [
    id 126
    label "w&#322;odarz"
  ]
  node [
    id 127
    label "szkolnik"
  ]
  node [
    id 128
    label "pedagog"
  ]
  node [
    id 129
    label "popularyzator"
  ]
  node [
    id 130
    label "andropauza"
  ]
  node [
    id 131
    label "gra_w_karty"
  ]
  node [
    id 132
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 133
    label "Mieszko_I"
  ]
  node [
    id 134
    label "bogaty"
  ]
  node [
    id 135
    label "samiec"
  ]
  node [
    id 136
    label "przyw&#243;dca"
  ]
  node [
    id 137
    label "pa&#324;stwo"
  ]
  node [
    id 138
    label "belfer"
  ]
  node [
    id 139
    label "przedmiot"
  ]
  node [
    id 140
    label "i"
  ]
  node [
    id 141
    label "Barbara"
  ]
  node [
    id 142
    label "Kudryckiej"
  ]
  node [
    id 143
    label "Micha&#322;"
  ]
  node [
    id 144
    label "Kleibera"
  ]
  node [
    id 145
    label "Katarzyna"
  ]
  node [
    id 146
    label "Cha&#322;asi&#324;skiej"
  ]
  node [
    id 147
    label "Macukow"
  ]
  node [
    id 148
    label "sala"
  ]
  node [
    id 149
    label "senat"
  ]
  node [
    id 150
    label "pa&#322;ac"
  ]
  node [
    id 151
    label "kazimierzowski"
  ]
  node [
    id 152
    label "uniwersytet"
  ]
  node [
    id 153
    label "warszawski"
  ]
  node [
    id 154
    label "Creative"
  ]
  node [
    id 155
    label "Commons"
  ]
  node [
    id 156
    label "polski"
  ]
  node [
    id 157
    label "Alka"
  ]
  node [
    id 158
    label "Tarkowski"
  ]
  node [
    id 159
    label "przegl&#261;d"
  ]
  node [
    id 160
    label "zagadnienie"
  ]
  node [
    id 161
    label "zwi&#261;za&#263;"
  ]
  node [
    id 162
    label "zeszyt"
  ]
  node [
    id 163
    label "otworzy&#263;"
  ]
  node [
    id 164
    label "barcelo&#324;ski"
  ]
  node [
    id 165
    label "Catalonia"
  ]
  node [
    id 166
    label "Ignasi"
  ]
  node [
    id 167
    label "Labastida"
  ]
  node [
    id 168
    label "juan"
  ]
  node [
    id 169
    label "wyspa"
  ]
  node [
    id 170
    label "strona"
  ]
  node [
    id 171
    label "otwarto&#347;&#263;"
  ]
  node [
    id 172
    label "do&#347;wiadczy&#263;"
  ]
  node [
    id 173
    label "biblioteka"
  ]
  node [
    id 174
    label "uniwersytecki"
  ]
  node [
    id 175
    label "Learn"
  ]
  node [
    id 176
    label "Ahrash"
  ]
  node [
    id 177
    label "Bissell"
  ]
  node [
    id 178
    label "edukacja"
  ]
  node [
    id 179
    label "dla"
  ]
  node [
    id 180
    label "innowacja"
  ]
  node [
    id 181
    label "&#8211;"
  ]
  node [
    id 182
    label "pomoc"
  ]
  node [
    id 183
    label "zak&#322;ad"
  ]
  node [
    id 184
    label "Bioinformatyki"
  ]
  node [
    id 185
    label "wydzia&#322;"
  ]
  node [
    id 186
    label "biologia"
  ]
  node [
    id 187
    label "instytut"
  ]
  node [
    id 188
    label "biochemia"
  ]
  node [
    id 189
    label "biofizyka"
  ]
  node [
    id 190
    label "pawe&#322;"
  ]
  node [
    id 191
    label "szcz&#281;sny"
  ]
  node [
    id 192
    label "2"
  ]
  node [
    id 193
    label "0"
  ]
  node [
    id 194
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 195
    label "Polska"
  ]
  node [
    id 196
    label "interdyscyplinarny"
  ]
  node [
    id 197
    label "centrum"
  ]
  node [
    id 198
    label "modelowa&#263;"
  ]
  node [
    id 199
    label "matematyczny"
  ]
  node [
    id 200
    label "komputerowy"
  ]
  node [
    id 201
    label "marka"
  ]
  node [
    id 202
    label "Niezg&#243;dka"
  ]
  node [
    id 203
    label "kancelaria"
  ]
  node [
    id 204
    label "prawny"
  ]
  node [
    id 205
    label "Grynhoff"
  ]
  node [
    id 206
    label "wo&#378;ny"
  ]
  node [
    id 207
    label "Mali&#324;ski"
  ]
  node [
    id 208
    label "Krzysztofa"
  ]
  node [
    id 209
    label "Siewicz"
  ]
  node [
    id 210
    label "aspekt"
  ]
  node [
    id 211
    label "badanie"
  ]
  node [
    id 212
    label "polityka"
  ]
  node [
    id 213
    label "naukowy"
  ]
  node [
    id 214
    label "Jan"
  ]
  node [
    id 215
    label "koz&#322;owski"
  ]
  node [
    id 216
    label "alternatywny"
  ]
  node [
    id 217
    label "forma"
  ]
  node [
    id 218
    label "peer"
  ]
  node [
    id 219
    label "review"
  ]
  node [
    id 220
    label "jaka"
  ]
  node [
    id 221
    label "wprowadzi&#263;"
  ]
  node [
    id 222
    label "do"
  ]
  node [
    id 223
    label "collegium"
  ]
  node [
    id 224
    label "Civitas"
  ]
  node [
    id 225
    label "Edwin"
  ]
  node [
    id 226
    label "Bendyk"
  ]
  node [
    id 227
    label "departament"
  ]
  node [
    id 228
    label "strategia"
  ]
  node [
    id 229
    label "rozw&#243;j"
  ]
  node [
    id 230
    label "analiza"
  ]
  node [
    id 231
    label "Gulda"
  ]
  node [
    id 232
    label "Juliusz"
  ]
  node [
    id 233
    label "Braun"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 20
  ]
  edge [
    source 7
    target 140
  ]
  edge [
    source 7
    target 197
  ]
  edge [
    source 7
    target 211
  ]
  edge [
    source 7
    target 212
  ]
  edge [
    source 7
    target 213
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 140
  ]
  edge [
    source 8
    target 197
  ]
  edge [
    source 8
    target 211
  ]
  edge [
    source 8
    target 212
  ]
  edge [
    source 8
    target 213
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 13
  ]
  edge [
    source 9
    target 14
  ]
  edge [
    source 9
    target 18
  ]
  edge [
    source 9
    target 19
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 9
    target 100
  ]
  edge [
    source 9
    target 101
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 15
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 16
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 102
  ]
  edge [
    source 17
    target 103
  ]
  edge [
    source 18
    target 104
  ]
  edge [
    source 18
    target 105
  ]
  edge [
    source 18
    target 106
  ]
  edge [
    source 18
    target 107
  ]
  edge [
    source 18
    target 108
  ]
  edge [
    source 18
    target 109
  ]
  edge [
    source 18
    target 110
  ]
  edge [
    source 18
    target 111
  ]
  edge [
    source 18
    target 112
  ]
  edge [
    source 18
    target 113
  ]
  edge [
    source 18
    target 114
  ]
  edge [
    source 18
    target 115
  ]
  edge [
    source 18
    target 116
  ]
  edge [
    source 18
    target 117
  ]
  edge [
    source 18
    target 118
  ]
  edge [
    source 18
    target 119
  ]
  edge [
    source 18
    target 120
  ]
  edge [
    source 18
    target 121
  ]
  edge [
    source 18
    target 122
  ]
  edge [
    source 18
    target 123
  ]
  edge [
    source 18
    target 124
  ]
  edge [
    source 18
    target 125
  ]
  edge [
    source 18
    target 126
  ]
  edge [
    source 18
    target 127
  ]
  edge [
    source 18
    target 128
  ]
  edge [
    source 18
    target 129
  ]
  edge [
    source 18
    target 130
  ]
  edge [
    source 18
    target 131
  ]
  edge [
    source 18
    target 132
  ]
  edge [
    source 18
    target 133
  ]
  edge [
    source 18
    target 134
  ]
  edge [
    source 18
    target 135
  ]
  edge [
    source 18
    target 136
  ]
  edge [
    source 18
    target 137
  ]
  edge [
    source 18
    target 138
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 139
  ]
  edge [
    source 140
    target 166
  ]
  edge [
    source 140
    target 167
  ]
  edge [
    source 140
    target 168
  ]
  edge [
    source 140
    target 187
  ]
  edge [
    source 140
    target 188
  ]
  edge [
    source 140
    target 189
  ]
  edge [
    source 140
    target 196
  ]
  edge [
    source 140
    target 197
  ]
  edge [
    source 140
    target 198
  ]
  edge [
    source 140
    target 199
  ]
  edge [
    source 140
    target 200
  ]
  edge [
    source 140
    target 211
  ]
  edge [
    source 140
    target 212
  ]
  edge [
    source 140
    target 213
  ]
  edge [
    source 140
    target 227
  ]
  edge [
    source 140
    target 228
  ]
  edge [
    source 140
    target 229
  ]
  edge [
    source 140
    target 230
  ]
  edge [
    source 141
    target 142
  ]
  edge [
    source 143
    target 144
  ]
  edge [
    source 145
    target 146
  ]
  edge [
    source 145
    target 147
  ]
  edge [
    source 146
    target 147
  ]
  edge [
    source 148
    target 149
  ]
  edge [
    source 150
    target 151
  ]
  edge [
    source 152
    target 153
  ]
  edge [
    source 152
    target 164
  ]
  edge [
    source 154
    target 155
  ]
  edge [
    source 154
    target 156
  ]
  edge [
    source 154
    target 165
  ]
  edge [
    source 154
    target 175
  ]
  edge [
    source 154
    target 178
  ]
  edge [
    source 154
    target 179
  ]
  edge [
    source 154
    target 180
  ]
  edge [
    source 154
    target 181
  ]
  edge [
    source 154
    target 162
  ]
  edge [
    source 154
    target 182
  ]
  edge [
    source 155
    target 156
  ]
  edge [
    source 155
    target 165
  ]
  edge [
    source 155
    target 175
  ]
  edge [
    source 155
    target 178
  ]
  edge [
    source 155
    target 179
  ]
  edge [
    source 155
    target 180
  ]
  edge [
    source 155
    target 181
  ]
  edge [
    source 155
    target 162
  ]
  edge [
    source 155
    target 182
  ]
  edge [
    source 156
    target 220
  ]
  edge [
    source 156
    target 221
  ]
  edge [
    source 156
    target 163
  ]
  edge [
    source 156
    target 222
  ]
  edge [
    source 157
    target 158
  ]
  edge [
    source 159
    target 160
  ]
  edge [
    source 159
    target 161
  ]
  edge [
    source 159
    target 162
  ]
  edge [
    source 159
    target 163
  ]
  edge [
    source 160
    target 161
  ]
  edge [
    source 160
    target 162
  ]
  edge [
    source 160
    target 163
  ]
  edge [
    source 161
    target 162
  ]
  edge [
    source 161
    target 163
  ]
  edge [
    source 162
    target 163
  ]
  edge [
    source 162
    target 178
  ]
  edge [
    source 162
    target 179
  ]
  edge [
    source 162
    target 180
  ]
  edge [
    source 162
    target 181
  ]
  edge [
    source 162
    target 182
  ]
  edge [
    source 163
    target 194
  ]
  edge [
    source 163
    target 169
  ]
  edge [
    source 163
    target 195
  ]
  edge [
    source 163
    target 204
  ]
  edge [
    source 163
    target 210
  ]
  edge [
    source 163
    target 220
  ]
  edge [
    source 163
    target 221
  ]
  edge [
    source 163
    target 222
  ]
  edge [
    source 166
    target 167
  ]
  edge [
    source 166
    target 168
  ]
  edge [
    source 167
    target 168
  ]
  edge [
    source 169
    target 170
  ]
  edge [
    source 169
    target 171
  ]
  edge [
    source 169
    target 172
  ]
  edge [
    source 169
    target 173
  ]
  edge [
    source 169
    target 174
  ]
  edge [
    source 169
    target 195
  ]
  edge [
    source 170
    target 171
  ]
  edge [
    source 170
    target 172
  ]
  edge [
    source 170
    target 173
  ]
  edge [
    source 170
    target 174
  ]
  edge [
    source 171
    target 172
  ]
  edge [
    source 171
    target 173
  ]
  edge [
    source 171
    target 174
  ]
  edge [
    source 172
    target 173
  ]
  edge [
    source 172
    target 174
  ]
  edge [
    source 173
    target 174
  ]
  edge [
    source 176
    target 177
  ]
  edge [
    source 178
    target 179
  ]
  edge [
    source 178
    target 180
  ]
  edge [
    source 178
    target 181
  ]
  edge [
    source 178
    target 182
  ]
  edge [
    source 179
    target 180
  ]
  edge [
    source 179
    target 181
  ]
  edge [
    source 179
    target 182
  ]
  edge [
    source 180
    target 181
  ]
  edge [
    source 180
    target 182
  ]
  edge [
    source 181
    target 182
  ]
  edge [
    source 183
    target 184
  ]
  edge [
    source 185
    target 186
  ]
  edge [
    source 187
    target 188
  ]
  edge [
    source 187
    target 189
  ]
  edge [
    source 188
    target 189
  ]
  edge [
    source 190
    target 191
  ]
  edge [
    source 192
    target 193
  ]
  edge [
    source 196
    target 197
  ]
  edge [
    source 196
    target 198
  ]
  edge [
    source 196
    target 199
  ]
  edge [
    source 196
    target 200
  ]
  edge [
    source 197
    target 198
  ]
  edge [
    source 197
    target 199
  ]
  edge [
    source 197
    target 200
  ]
  edge [
    source 197
    target 211
  ]
  edge [
    source 197
    target 212
  ]
  edge [
    source 197
    target 213
  ]
  edge [
    source 198
    target 199
  ]
  edge [
    source 198
    target 200
  ]
  edge [
    source 199
    target 200
  ]
  edge [
    source 201
    target 202
  ]
  edge [
    source 203
    target 204
  ]
  edge [
    source 203
    target 205
  ]
  edge [
    source 203
    target 206
  ]
  edge [
    source 203
    target 207
  ]
  edge [
    source 204
    target 205
  ]
  edge [
    source 204
    target 206
  ]
  edge [
    source 204
    target 207
  ]
  edge [
    source 204
    target 210
  ]
  edge [
    source 205
    target 206
  ]
  edge [
    source 205
    target 207
  ]
  edge [
    source 206
    target 207
  ]
  edge [
    source 208
    target 209
  ]
  edge [
    source 208
    target 231
  ]
  edge [
    source 211
    target 212
  ]
  edge [
    source 211
    target 213
  ]
  edge [
    source 212
    target 213
  ]
  edge [
    source 214
    target 215
  ]
  edge [
    source 216
    target 217
  ]
  edge [
    source 216
    target 218
  ]
  edge [
    source 216
    target 219
  ]
  edge [
    source 217
    target 218
  ]
  edge [
    source 217
    target 219
  ]
  edge [
    source 218
    target 219
  ]
  edge [
    source 220
    target 221
  ]
  edge [
    source 220
    target 222
  ]
  edge [
    source 221
    target 222
  ]
  edge [
    source 223
    target 224
  ]
  edge [
    source 225
    target 226
  ]
  edge [
    source 227
    target 228
  ]
  edge [
    source 227
    target 229
  ]
  edge [
    source 227
    target 230
  ]
  edge [
    source 228
    target 229
  ]
  edge [
    source 228
    target 230
  ]
  edge [
    source 232
    target 233
  ]
]
