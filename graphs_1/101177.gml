graph [
  maxDegree 6
  minDegree 1
  meanDegree 3.466666666666667
  density 0.24761904761904763
  graphCliqueNumber 7
  node [
    id 0
    label "god&#322;o"
    origin "text"
  ]
  node [
    id 1
    label "hongkong"
    origin "text"
  ]
  node [
    id 2
    label "kogut_galijski"
  ]
  node [
    id 3
    label "symbol"
  ]
  node [
    id 4
    label "wielki"
  ]
  node [
    id 5
    label "brytania"
  ]
  node [
    id 6
    label "chi&#324;ski"
  ]
  node [
    id 7
    label "republika"
  ]
  node [
    id 8
    label "ludowy"
  ]
  node [
    id 9
    label "Hongkong"
  ]
  node [
    id 10
    label "specjalny"
  ]
  node [
    id 11
    label "region"
  ]
  node [
    id 12
    label "administracyjny"
  ]
  node [
    id 13
    label "Hong"
  ]
  node [
    id 14
    label "Kongo"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 6
    target 9
  ]
  edge [
    source 6
    target 10
  ]
  edge [
    source 6
    target 11
  ]
  edge [
    source 6
    target 12
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 7
    target 10
  ]
  edge [
    source 7
    target 11
  ]
  edge [
    source 7
    target 12
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 8
    target 11
  ]
  edge [
    source 8
    target 12
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 9
    target 12
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 13
    target 14
  ]
]
