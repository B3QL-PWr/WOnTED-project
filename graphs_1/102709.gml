graph [
  maxDegree 65
  minDegree 1
  meanDegree 2.0428571428571427
  density 0.014696813977389518
  graphCliqueNumber 3
  node [
    id 0
    label "nast&#281;pny"
    origin "text"
  ]
  node [
    id 1
    label "dla"
    origin "text"
  ]
  node [
    id 2
    label "linia"
    origin "text"
  ]
  node [
    id 3
    label "sto"
    origin "text"
  ]
  node [
    id 4
    label "trzydzie&#347;ci"
    origin "text"
  ]
  node [
    id 5
    label "prosi&#263;"
    origin "text"
  ]
  node [
    id 6
    label "pan"
    origin "text"
  ]
  node [
    id 7
    label "by&#263;by"
    origin "text"
  ]
  node [
    id 8
    label "przystanek"
    origin "text"
  ]
  node [
    id 9
    label "kostrzewski"
    origin "text"
  ]
  node [
    id 10
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 11
    label "by&#263;"
    origin "text"
  ]
  node [
    id 12
    label "che&#322;mski"
    origin "text"
  ]
  node [
    id 13
    label "dopiero"
    origin "text"
  ]
  node [
    id 14
    label "nast&#281;pnie"
  ]
  node [
    id 15
    label "kolejno"
  ]
  node [
    id 16
    label "nastopny"
  ]
  node [
    id 17
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 18
    label "cz&#322;owiek"
  ]
  node [
    id 19
    label "koniec"
  ]
  node [
    id 20
    label "uporz&#261;dkowanie"
  ]
  node [
    id 21
    label "coalescence"
  ]
  node [
    id 22
    label "rz&#261;d"
  ]
  node [
    id 23
    label "grupa_organizm&#243;w"
  ]
  node [
    id 24
    label "curve"
  ]
  node [
    id 25
    label "kompleksja"
  ]
  node [
    id 26
    label "access"
  ]
  node [
    id 27
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 28
    label "tekst"
  ]
  node [
    id 29
    label "fragment"
  ]
  node [
    id 30
    label "cord"
  ]
  node [
    id 31
    label "przewo&#378;nik"
  ]
  node [
    id 32
    label "budowa"
  ]
  node [
    id 33
    label "granica"
  ]
  node [
    id 34
    label "roz&#322;&#261;cza&#263;"
  ]
  node [
    id 35
    label "szpaler"
  ]
  node [
    id 36
    label "phreaker"
  ]
  node [
    id 37
    label "tract"
  ]
  node [
    id 38
    label "sztrych"
  ]
  node [
    id 39
    label "kontakt"
  ]
  node [
    id 40
    label "spos&#243;b"
  ]
  node [
    id 41
    label "prowadzi&#263;"
  ]
  node [
    id 42
    label "point"
  ]
  node [
    id 43
    label "materia&#322;_zecerski"
  ]
  node [
    id 44
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 45
    label "linijka"
  ]
  node [
    id 46
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 47
    label "po&#322;&#261;czenie"
  ]
  node [
    id 48
    label "cecha"
  ]
  node [
    id 49
    label "transporter"
  ]
  node [
    id 50
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 51
    label "przeorientowywa&#263;"
  ]
  node [
    id 52
    label "bearing"
  ]
  node [
    id 53
    label "line"
  ]
  node [
    id 54
    label "trasa"
  ]
  node [
    id 55
    label "przew&#243;d"
  ]
  node [
    id 56
    label "figura_geometryczna"
  ]
  node [
    id 57
    label "kszta&#322;t"
  ]
  node [
    id 58
    label "drzewo_genealogiczne"
  ]
  node [
    id 59
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 60
    label "roz&#322;&#261;czanie"
  ]
  node [
    id 61
    label "wygl&#261;d"
  ]
  node [
    id 62
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 63
    label "poprowadzi&#263;"
  ]
  node [
    id 64
    label "armia"
  ]
  node [
    id 65
    label "szczep"
  ]
  node [
    id 66
    label "Ural"
  ]
  node [
    id 67
    label "przeorientowa&#263;"
  ]
  node [
    id 68
    label "prostoliniowo&#347;&#263;"
  ]
  node [
    id 69
    label "granice"
  ]
  node [
    id 70
    label "przeorientowanie"
  ]
  node [
    id 71
    label "billing"
  ]
  node [
    id 72
    label "prowadzenie"
  ]
  node [
    id 73
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 74
    label "zbi&#243;r"
  ]
  node [
    id 75
    label "przeorientowywanie"
  ]
  node [
    id 76
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 77
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 78
    label "jard"
  ]
  node [
    id 79
    label "trwa&#263;"
  ]
  node [
    id 80
    label "zezwala&#263;"
  ]
  node [
    id 81
    label "ask"
  ]
  node [
    id 82
    label "invite"
  ]
  node [
    id 83
    label "zach&#281;ca&#263;"
  ]
  node [
    id 84
    label "preach"
  ]
  node [
    id 85
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 86
    label "pies"
  ]
  node [
    id 87
    label "ujmowa&#263;_si&#281;"
  ]
  node [
    id 88
    label "poleca&#263;"
  ]
  node [
    id 89
    label "zaprasza&#263;"
  ]
  node [
    id 90
    label "suffice"
  ]
  node [
    id 91
    label "dziwi&#263;_si&#281;"
  ]
  node [
    id 92
    label "profesor"
  ]
  node [
    id 93
    label "kszta&#322;ciciel"
  ]
  node [
    id 94
    label "jegomo&#347;&#263;"
  ]
  node [
    id 95
    label "zwrot"
  ]
  node [
    id 96
    label "pracodawca"
  ]
  node [
    id 97
    label "rz&#261;dzenie"
  ]
  node [
    id 98
    label "m&#261;&#380;"
  ]
  node [
    id 99
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 100
    label "ch&#322;opina"
  ]
  node [
    id 101
    label "bratek"
  ]
  node [
    id 102
    label "opiekun"
  ]
  node [
    id 103
    label "doros&#322;y"
  ]
  node [
    id 104
    label "preceptor"
  ]
  node [
    id 105
    label "Midas"
  ]
  node [
    id 106
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 107
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 108
    label "murza"
  ]
  node [
    id 109
    label "ojciec"
  ]
  node [
    id 110
    label "androlog"
  ]
  node [
    id 111
    label "pupil"
  ]
  node [
    id 112
    label "efendi"
  ]
  node [
    id 113
    label "nabab"
  ]
  node [
    id 114
    label "w&#322;odarz"
  ]
  node [
    id 115
    label "szkolnik"
  ]
  node [
    id 116
    label "pedagog"
  ]
  node [
    id 117
    label "popularyzator"
  ]
  node [
    id 118
    label "andropauza"
  ]
  node [
    id 119
    label "gra_w_karty"
  ]
  node [
    id 120
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 121
    label "Mieszko_I"
  ]
  node [
    id 122
    label "bogaty"
  ]
  node [
    id 123
    label "samiec"
  ]
  node [
    id 124
    label "przyw&#243;dca"
  ]
  node [
    id 125
    label "pa&#324;stwo"
  ]
  node [
    id 126
    label "belfer"
  ]
  node [
    id 127
    label "stanowisko"
  ]
  node [
    id 128
    label "si&#281;ga&#263;"
  ]
  node [
    id 129
    label "obecno&#347;&#263;"
  ]
  node [
    id 130
    label "stan"
  ]
  node [
    id 131
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 132
    label "stand"
  ]
  node [
    id 133
    label "mie&#263;_miejsce"
  ]
  node [
    id 134
    label "uczestniczy&#263;"
  ]
  node [
    id 135
    label "chodzi&#263;"
  ]
  node [
    id 136
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 137
    label "equal"
  ]
  node [
    id 138
    label "po_che&#322;msku"
  ]
  node [
    id 139
    label "lubelski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 18
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 127
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 128
  ]
  edge [
    source 11
    target 79
  ]
  edge [
    source 11
    target 129
  ]
  edge [
    source 11
    target 130
  ]
  edge [
    source 11
    target 131
  ]
  edge [
    source 11
    target 132
  ]
  edge [
    source 11
    target 133
  ]
  edge [
    source 11
    target 134
  ]
  edge [
    source 11
    target 135
  ]
  edge [
    source 11
    target 136
  ]
  edge [
    source 11
    target 137
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 138
  ]
  edge [
    source 12
    target 139
  ]
]
