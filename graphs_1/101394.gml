graph [
  maxDegree 2
  minDegree 1
  meanDegree 1.36
  density 0.056666666666666664
  graphCliqueNumber 3
  node [
    id 0
    label "maciej"
    origin "text"
  ]
  node [
    id 1
    label "bartodziejski"
    origin "text"
  ]
  node [
    id 2
    label "Maciej"
  ]
  node [
    id 3
    label "Bartodziejski"
  ]
  node [
    id 4
    label "resursa"
  ]
  node [
    id 5
    label "&#322;&#243;d&#378;"
  ]
  node [
    id 6
    label "Bzura"
  ]
  node [
    id 7
    label "ozorek"
  ]
  node [
    id 8
    label "SPS"
  ]
  node [
    id 9
    label "zdu&#324;ski"
  ]
  node [
    id 10
    label "woli"
  ]
  node [
    id 11
    label "skra"
  ]
  node [
    id 12
    label "Be&#322;chat&#243;w"
  ]
  node [
    id 13
    label "mistrzostwo"
  ]
  node [
    id 14
    label "europ"
  ]
  node [
    id 15
    label "junior"
  ]
  node [
    id 16
    label "seria"
  ]
  node [
    id 17
    label "albo"
  ]
  node [
    id 18
    label "polski"
  ]
  node [
    id 19
    label "liga"
  ]
  node [
    id 20
    label "siatk&#243;wka"
  ]
  node [
    id 21
    label "siatkarz"
  ]
  node [
    id 22
    label "Wielu&#324;"
  ]
  node [
    id 23
    label "Jack"
  ]
  node [
    id 24
    label "Nawrocki"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 20
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 23
    target 24
  ]
]
