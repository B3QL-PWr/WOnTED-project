graph [
  maxDegree 41
  minDegree 1
  meanDegree 2
  density 0.013333333333333334
  graphCliqueNumber 2
  node [
    id 0
    label "niebezpieczny"
    origin "text"
  ]
  node [
    id 1
    label "tendencja"
    origin "text"
  ]
  node [
    id 2
    label "polski"
    origin "text"
  ]
  node [
    id 3
    label "droga"
    origin "text"
  ]
  node [
    id 4
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 5
    label "drugi"
    origin "text"
  ]
  node [
    id 6
    label "potr&#261;ci&#263;"
    origin "text"
  ]
  node [
    id 7
    label "pieszy"
    origin "text"
  ]
  node [
    id 8
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 9
    label "przej&#347;cie"
    origin "text"
  ]
  node [
    id 10
    label "gro&#378;ny"
  ]
  node [
    id 11
    label "k&#322;opotliwy"
  ]
  node [
    id 12
    label "niebezpiecznie"
  ]
  node [
    id 13
    label "system"
  ]
  node [
    id 14
    label "podatno&#347;&#263;"
  ]
  node [
    id 15
    label "idea"
  ]
  node [
    id 16
    label "ideologia"
  ]
  node [
    id 17
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 18
    label "praktyka"
  ]
  node [
    id 19
    label "metoda"
  ]
  node [
    id 20
    label "lacki"
  ]
  node [
    id 21
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 22
    label "przedmiot"
  ]
  node [
    id 23
    label "sztajer"
  ]
  node [
    id 24
    label "drabant"
  ]
  node [
    id 25
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 26
    label "polak"
  ]
  node [
    id 27
    label "pierogi_ruskie"
  ]
  node [
    id 28
    label "krakowiak"
  ]
  node [
    id 29
    label "Polish"
  ]
  node [
    id 30
    label "j&#281;zyk"
  ]
  node [
    id 31
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 32
    label "oberek"
  ]
  node [
    id 33
    label "po_polsku"
  ]
  node [
    id 34
    label "mazur"
  ]
  node [
    id 35
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 36
    label "chodzony"
  ]
  node [
    id 37
    label "skoczny"
  ]
  node [
    id 38
    label "ryba_po_grecku"
  ]
  node [
    id 39
    label "goniony"
  ]
  node [
    id 40
    label "polsko"
  ]
  node [
    id 41
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 42
    label "journey"
  ]
  node [
    id 43
    label "podbieg"
  ]
  node [
    id 44
    label "bezsilnikowy"
  ]
  node [
    id 45
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 46
    label "wylot"
  ]
  node [
    id 47
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 48
    label "drogowskaz"
  ]
  node [
    id 49
    label "nawierzchnia"
  ]
  node [
    id 50
    label "turystyka"
  ]
  node [
    id 51
    label "budowla"
  ]
  node [
    id 52
    label "spos&#243;b"
  ]
  node [
    id 53
    label "passage"
  ]
  node [
    id 54
    label "marszrutyzacja"
  ]
  node [
    id 55
    label "zbior&#243;wka"
  ]
  node [
    id 56
    label "ekskursja"
  ]
  node [
    id 57
    label "rajza"
  ]
  node [
    id 58
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 59
    label "ruch"
  ]
  node [
    id 60
    label "trasa"
  ]
  node [
    id 61
    label "wyb&#243;j"
  ]
  node [
    id 62
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 63
    label "ekwipunek"
  ]
  node [
    id 64
    label "korona_drogi"
  ]
  node [
    id 65
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 66
    label "pobocze"
  ]
  node [
    id 67
    label "inny"
  ]
  node [
    id 68
    label "kolejny"
  ]
  node [
    id 69
    label "przeciwny"
  ]
  node [
    id 70
    label "sw&#243;j"
  ]
  node [
    id 71
    label "odwrotnie"
  ]
  node [
    id 72
    label "dzie&#324;"
  ]
  node [
    id 73
    label "podobny"
  ]
  node [
    id 74
    label "wt&#243;ry"
  ]
  node [
    id 75
    label "precipitate"
  ]
  node [
    id 76
    label "allude"
  ]
  node [
    id 77
    label "odliczy&#263;"
  ]
  node [
    id 78
    label "uderzy&#263;"
  ]
  node [
    id 79
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 80
    label "smell"
  ]
  node [
    id 81
    label "specjalny"
  ]
  node [
    id 82
    label "w&#281;drowiec"
  ]
  node [
    id 83
    label "pieszo"
  ]
  node [
    id 84
    label "piechotny"
  ]
  node [
    id 85
    label "chodnik"
  ]
  node [
    id 86
    label "asymilowa&#263;"
  ]
  node [
    id 87
    label "wapniak"
  ]
  node [
    id 88
    label "dwun&#243;g"
  ]
  node [
    id 89
    label "polifag"
  ]
  node [
    id 90
    label "wz&#243;r"
  ]
  node [
    id 91
    label "profanum"
  ]
  node [
    id 92
    label "hominid"
  ]
  node [
    id 93
    label "homo_sapiens"
  ]
  node [
    id 94
    label "nasada"
  ]
  node [
    id 95
    label "podw&#322;adny"
  ]
  node [
    id 96
    label "ludzko&#347;&#263;"
  ]
  node [
    id 97
    label "os&#322;abianie"
  ]
  node [
    id 98
    label "mikrokosmos"
  ]
  node [
    id 99
    label "portrecista"
  ]
  node [
    id 100
    label "duch"
  ]
  node [
    id 101
    label "oddzia&#322;ywanie"
  ]
  node [
    id 102
    label "g&#322;owa"
  ]
  node [
    id 103
    label "asymilowanie"
  ]
  node [
    id 104
    label "osoba"
  ]
  node [
    id 105
    label "os&#322;abia&#263;"
  ]
  node [
    id 106
    label "figura"
  ]
  node [
    id 107
    label "Adam"
  ]
  node [
    id 108
    label "senior"
  ]
  node [
    id 109
    label "antropochoria"
  ]
  node [
    id 110
    label "posta&#263;"
  ]
  node [
    id 111
    label "nale&#380;enie"
  ]
  node [
    id 112
    label "odmienienie"
  ]
  node [
    id 113
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 114
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 115
    label "mini&#281;cie"
  ]
  node [
    id 116
    label "prze&#380;ycie"
  ]
  node [
    id 117
    label "strain"
  ]
  node [
    id 118
    label "przerobienie"
  ]
  node [
    id 119
    label "stanie_si&#281;"
  ]
  node [
    id 120
    label "dostanie_si&#281;"
  ]
  node [
    id 121
    label "wydeptanie"
  ]
  node [
    id 122
    label "wydeptywanie"
  ]
  node [
    id 123
    label "offense"
  ]
  node [
    id 124
    label "wymienienie"
  ]
  node [
    id 125
    label "zacz&#281;cie"
  ]
  node [
    id 126
    label "trwanie"
  ]
  node [
    id 127
    label "przepojenie"
  ]
  node [
    id 128
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 129
    label "zaliczenie"
  ]
  node [
    id 130
    label "zdarzenie_si&#281;"
  ]
  node [
    id 131
    label "uznanie"
  ]
  node [
    id 132
    label "nasycenie_si&#281;"
  ]
  node [
    id 133
    label "przemokni&#281;cie"
  ]
  node [
    id 134
    label "nas&#261;czenie"
  ]
  node [
    id 135
    label "mienie"
  ]
  node [
    id 136
    label "ustawa"
  ]
  node [
    id 137
    label "experience"
  ]
  node [
    id 138
    label "przewy&#380;szenie"
  ]
  node [
    id 139
    label "miejsce"
  ]
  node [
    id 140
    label "faza"
  ]
  node [
    id 141
    label "doznanie"
  ]
  node [
    id 142
    label "przestanie"
  ]
  node [
    id 143
    label "traversal"
  ]
  node [
    id 144
    label "przebycie"
  ]
  node [
    id 145
    label "przedostanie_si&#281;"
  ]
  node [
    id 146
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 147
    label "wstawka"
  ]
  node [
    id 148
    label "przepuszczenie"
  ]
  node [
    id 149
    label "wytyczenie"
  ]
  node [
    id 150
    label "crack"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 9
    target 116
  ]
  edge [
    source 9
    target 117
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 9
    target 119
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 129
  ]
  edge [
    source 9
    target 130
  ]
  edge [
    source 9
    target 131
  ]
  edge [
    source 9
    target 132
  ]
  edge [
    source 9
    target 133
  ]
  edge [
    source 9
    target 134
  ]
  edge [
    source 9
    target 135
  ]
  edge [
    source 9
    target 136
  ]
  edge [
    source 9
    target 137
  ]
  edge [
    source 9
    target 138
  ]
  edge [
    source 9
    target 139
  ]
  edge [
    source 9
    target 140
  ]
  edge [
    source 9
    target 141
  ]
  edge [
    source 9
    target 142
  ]
  edge [
    source 9
    target 143
  ]
  edge [
    source 9
    target 144
  ]
  edge [
    source 9
    target 145
  ]
  edge [
    source 9
    target 146
  ]
  edge [
    source 9
    target 147
  ]
  edge [
    source 9
    target 148
  ]
  edge [
    source 9
    target 149
  ]
  edge [
    source 9
    target 150
  ]
]
