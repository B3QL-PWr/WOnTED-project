graph [
  maxDegree 33
  minDegree 1
  meanDegree 1.951219512195122
  density 0.04878048780487805
  graphCliqueNumber 2
  node [
    id 0
    label "ile"
    origin "text"
  ]
  node [
    id 1
    label "mireczkow"
    origin "text"
  ]
  node [
    id 2
    label "wpada&#263;"
    origin "text"
  ]
  node [
    id 3
    label "kolacja"
    origin "text"
  ]
  node [
    id 4
    label "gda&#324;sk"
    origin "text"
  ]
  node [
    id 5
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 6
    label "wpa&#347;&#263;"
  ]
  node [
    id 7
    label "chowa&#263;"
  ]
  node [
    id 8
    label "wpierdala&#263;_si&#281;"
  ]
  node [
    id 9
    label "strike"
  ]
  node [
    id 10
    label "rzecz"
  ]
  node [
    id 11
    label "ulega&#263;"
  ]
  node [
    id 12
    label "przypomina&#263;"
  ]
  node [
    id 13
    label "wp&#322;ywa&#263;"
  ]
  node [
    id 14
    label "popada&#263;"
  ]
  node [
    id 15
    label "zapach"
  ]
  node [
    id 16
    label "czu&#263;"
  ]
  node [
    id 17
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 18
    label "pogo"
  ]
  node [
    id 19
    label "flatten"
  ]
  node [
    id 20
    label "zachodzi&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 21
    label "odwiedza&#263;"
  ]
  node [
    id 22
    label "fall"
  ]
  node [
    id 23
    label "ujmowa&#263;"
  ]
  node [
    id 24
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 25
    label "wpieprza&#263;_si&#281;"
  ]
  node [
    id 26
    label "&#347;wiat&#322;o"
  ]
  node [
    id 27
    label "wymy&#347;la&#263;"
  ]
  node [
    id 28
    label "wkl&#281;sa&#263;"
  ]
  node [
    id 29
    label "emocja"
  ]
  node [
    id 30
    label "demaskowa&#263;"
  ]
  node [
    id 31
    label "zaziera&#263;"
  ]
  node [
    id 32
    label "ogrom"
  ]
  node [
    id 33
    label "spotyka&#263;"
  ]
  node [
    id 34
    label "d&#378;wi&#281;k"
  ]
  node [
    id 35
    label "drop"
  ]
  node [
    id 36
    label "przywilej"
  ]
  node [
    id 37
    label "odwieczerz"
  ]
  node [
    id 38
    label "spotkanie"
  ]
  node [
    id 39
    label "posi&#322;ek"
  ]
  node [
    id 40
    label "filiacja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
]
