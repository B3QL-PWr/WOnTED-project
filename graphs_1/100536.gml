graph [
  maxDegree 6
  minDegree 1
  meanDegree 2.6666666666666665
  density 0.09195402298850575
  graphCliqueNumber 7
  node [
    id 0
    label "szczepionka"
    origin "text"
  ]
  node [
    id 1
    label "pre"
    origin "text"
  ]
  node [
    id 2
    label "pandemiczny"
    origin "text"
  ]
  node [
    id 3
    label "przeciw"
    origin "text"
  ]
  node [
    id 4
    label "grypa"
    origin "text"
  ]
  node [
    id 5
    label "przyj&#261;&#263;_si&#281;"
  ]
  node [
    id 6
    label "tiomersal"
  ]
  node [
    id 7
    label "przyjmowa&#263;_si&#281;"
  ]
  node [
    id 8
    label "vaccine"
  ]
  node [
    id 9
    label "biopreparat"
  ]
  node [
    id 10
    label "epidemiczny"
  ]
  node [
    id 11
    label "wirus_grypy"
  ]
  node [
    id 12
    label "influenca"
  ]
  node [
    id 13
    label "choroba_zara&#378;liwa"
  ]
  node [
    id 14
    label "kaszel"
  ]
  node [
    id 15
    label "choroba_wirusowa"
  ]
  node [
    id 16
    label "albo"
  ]
  node [
    id 17
    label "H5N1"
  ]
  node [
    id 18
    label "komitet"
  ]
  node [
    id 19
    label "do&#160;spraw"
  ]
  node [
    id 20
    label "produkt"
  ]
  node [
    id 21
    label "leczniczy"
  ]
  node [
    id 22
    label "stosowa&#263;"
  ]
  node [
    id 23
    label "u"
  ]
  node [
    id 24
    label "ludzie"
  ]
  node [
    id 25
    label "Prepandrix"
  ]
  node [
    id 26
    label "&#174;"
  ]
  node [
    id 27
    label "Pandemrix"
  ]
  node [
    id 28
    label "GSK"
  ]
  node [
    id 29
    label "Biologicals"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 11
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 20
  ]
  edge [
    source 18
    target 21
  ]
  edge [
    source 18
    target 22
  ]
  edge [
    source 18
    target 23
  ]
  edge [
    source 18
    target 24
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 21
  ]
  edge [
    source 19
    target 22
  ]
  edge [
    source 19
    target 23
  ]
  edge [
    source 19
    target 24
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 22
  ]
  edge [
    source 20
    target 23
  ]
  edge [
    source 20
    target 24
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 23
  ]
  edge [
    source 21
    target 24
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 24
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 28
    target 29
  ]
]
