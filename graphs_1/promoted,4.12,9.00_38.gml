graph [
  maxDegree 29
  minDegree 1
  meanDegree 2.857142857142857
  density 0.05952380952380952
  graphCliqueNumber 8
  node [
    id 0
    label "adam"
    origin "text"
  ]
  node [
    id 1
    label "s&#322;odowy"
    origin "text"
  ]
  node [
    id 2
    label "grudzie&#324;"
    origin "text"
  ]
  node [
    id 3
    label "czarnk&#243;w"
    origin "text"
  ]
  node [
    id 4
    label "majsterkowa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "popularny"
    origin "text"
  ]
  node [
    id 6
    label "polski"
    origin "text"
  ]
  node [
    id 7
    label "ksi&#281;ga_przys&#322;&#243;w_"
    origin "text"
  ]
  node [
    id 8
    label "dla"
    origin "text"
  ]
  node [
    id 9
    label "zr&#243;b"
    origin "text"
  ]
  node [
    id 10
    label "s&#322;odkawy"
  ]
  node [
    id 11
    label "zbo&#380;owy"
  ]
  node [
    id 12
    label "Bo&#380;e_Narodzenie"
  ]
  node [
    id 13
    label "miesi&#261;c"
  ]
  node [
    id 14
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 15
    label "Barb&#243;rka"
  ]
  node [
    id 16
    label "Sylwester"
  ]
  node [
    id 17
    label "extort"
  ]
  node [
    id 18
    label "pracowa&#263;"
  ]
  node [
    id 19
    label "przyst&#281;pny"
  ]
  node [
    id 20
    label "&#322;atwy"
  ]
  node [
    id 21
    label "popularnie"
  ]
  node [
    id 22
    label "znany"
  ]
  node [
    id 23
    label "lacki"
  ]
  node [
    id 24
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 25
    label "przedmiot"
  ]
  node [
    id 26
    label "sztajer"
  ]
  node [
    id 27
    label "drabant"
  ]
  node [
    id 28
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 29
    label "polak"
  ]
  node [
    id 30
    label "pierogi_ruskie"
  ]
  node [
    id 31
    label "krakowiak"
  ]
  node [
    id 32
    label "Polish"
  ]
  node [
    id 33
    label "j&#281;zyk"
  ]
  node [
    id 34
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 35
    label "oberek"
  ]
  node [
    id 36
    label "po_polsku"
  ]
  node [
    id 37
    label "mazur"
  ]
  node [
    id 38
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 39
    label "chodzony"
  ]
  node [
    id 40
    label "skoczny"
  ]
  node [
    id 41
    label "ryba_po_grecku"
  ]
  node [
    id 42
    label "goniony"
  ]
  node [
    id 43
    label "polsko"
  ]
  node [
    id 44
    label "Adam"
  ]
  node [
    id 45
    label "prowadz&#261;cy"
  ]
  node [
    id 46
    label "Ksi&#281;gaPrzys&#322;&#243;wPrzypowie&#347;ciSalomona"
  ]
  node [
    id 47
    label "tv"
  ]
  node [
    id 48
    label "m&#322;odzie&#380;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 23
  ]
  edge [
    source 6
    target 24
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 9
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 8
    target 46
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 9
    target 45
  ]
  edge [
    source 9
    target 46
  ]
  edge [
    source 9
    target 47
  ]
  edge [
    source 9
    target 48
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 47
  ]
  edge [
    source 45
    target 48
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 48
  ]
  edge [
    source 47
    target 48
  ]
]
