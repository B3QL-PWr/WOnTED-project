graph [
  maxDegree 593
  minDegree 1
  meanDegree 2.0919540229885056
  density 0.0017189433220940885
  graphCliqueNumber 3
  node [
    id 0
    label "dzia&#322;ania"
    origin "text"
  ]
  node [
    id 1
    label "naprawczy"
    origin "text"
  ]
  node [
    id 2
    label "wykonanie"
    origin "text"
  ]
  node [
    id 3
    label "okres"
    origin "text"
  ]
  node [
    id 4
    label "d&#322;ugoterminowy"
    origin "text"
  ]
  node [
    id 5
    label "lato"
    origin "text"
  ]
  node [
    id 6
    label "powinny"
    origin "text"
  ]
  node [
    id 7
    label "by&#263;"
    origin "text"
  ]
  node [
    id 8
    label "przedmiot"
    origin "text"
  ]
  node [
    id 9
    label "szczeg&#243;&#322;owy"
    origin "text"
  ]
  node [
    id 10
    label "analiza"
    origin "text"
  ]
  node [
    id 11
    label "aktualizacja"
    origin "text"
  ]
  node [
    id 12
    label "mapa"
    origin "text"
  ]
  node [
    id 13
    label "akustyczny"
    origin "text"
  ]
  node [
    id 14
    label "tym"
    origin "text"
  ]
  node [
    id 15
    label "ida"
    origin "text"
  ]
  node [
    id 16
    label "program"
    origin "text"
  ]
  node [
    id 17
    label "wzgl&#261;d"
    origin "text"
  ]
  node [
    id 18
    label "planowa&#263;"
    origin "text"
  ]
  node [
    id 19
    label "zmiana"
    origin "text"
  ]
  node [
    id 20
    label "uk&#322;ad"
    origin "text"
  ]
  node [
    id 21
    label "transportowy"
    origin "text"
  ]
  node [
    id 22
    label "miasto"
    origin "text"
  ]
  node [
    id 23
    label "budowa"
    origin "text"
  ]
  node [
    id 24
    label "nowa"
    origin "text"
  ]
  node [
    id 25
    label "osiedle"
    origin "text"
  ]
  node [
    id 26
    label "mieszkaniowy"
    origin "text"
  ]
  node [
    id 27
    label "sfera"
    origin "text"
  ]
  node [
    id 28
    label "przemys&#322;owy"
    origin "text"
  ]
  node [
    id 29
    label "stworzenie"
    origin "text"
  ]
  node [
    id 30
    label "dok&#322;adny"
    origin "text"
  ]
  node [
    id 31
    label "koncepcja"
    origin "text"
  ]
  node [
    id 32
    label "antyha&#322;asowy"
    origin "text"
  ]
  node [
    id 33
    label "perspektywa"
    origin "text"
  ]
  node [
    id 34
    label "lata"
    origin "text"
  ]
  node [
    id 35
    label "obarczy&#263;"
    origin "text"
  ]
  node [
    id 36
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 37
    label "b&#322;&#261;d"
    origin "text"
  ]
  node [
    id 38
    label "zar&#243;wno"
    origin "text"
  ]
  node [
    id 39
    label "je&#347;li"
    origin "text"
  ]
  node [
    id 40
    label "chodzi&#263;"
    origin "text"
  ]
  node [
    id 41
    label "rozwi&#261;zanie"
    origin "text"
  ]
  node [
    id 42
    label "techniczny"
    origin "text"
  ]
  node [
    id 43
    label "jak"
    origin "text"
  ]
  node [
    id 44
    label "finansowy"
    origin "text"
  ]
  node [
    id 45
    label "st&#261;d"
    origin "text"
  ]
  node [
    id 46
    label "niniejszy"
    origin "text"
  ]
  node [
    id 47
    label "opracowanie"
    origin "text"
  ]
  node [
    id 48
    label "okre&#347;li&#263;"
    origin "text"
  ]
  node [
    id 49
    label "tylko"
    origin "text"
  ]
  node [
    id 50
    label "dla"
    origin "text"
  ]
  node [
    id 51
    label "cel"
    origin "text"
  ]
  node [
    id 52
    label "kr&#243;tko"
    origin "text"
  ]
  node [
    id 53
    label "&#347;rednioterminowy"
    origin "text"
  ]
  node [
    id 54
    label "dokona&#263;"
    origin "text"
  ]
  node [
    id 55
    label "wskazanie"
    origin "text"
  ]
  node [
    id 56
    label "obszar"
    origin "text"
  ]
  node [
    id 57
    label "kwalifikowa&#263;"
    origin "text"
  ]
  node [
    id 58
    label "si&#281;"
    origin "text"
  ]
  node [
    id 59
    label "podj&#281;cie"
    origin "text"
  ]
  node [
    id 60
    label "strategia"
  ]
  node [
    id 61
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 62
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 63
    label "ob&#322;awa_augustowska"
  ]
  node [
    id 64
    label "zrobienie"
  ]
  node [
    id 65
    label "czynno&#347;&#263;"
  ]
  node [
    id 66
    label "fabrication"
  ]
  node [
    id 67
    label "ziszczenie_si&#281;"
  ]
  node [
    id 68
    label "pojawienie_si&#281;"
  ]
  node [
    id 69
    label "dzie&#322;o"
  ]
  node [
    id 70
    label "production"
  ]
  node [
    id 71
    label "completion"
  ]
  node [
    id 72
    label "realizacja"
  ]
  node [
    id 73
    label "paleogen"
  ]
  node [
    id 74
    label "spell"
  ]
  node [
    id 75
    label "czas"
  ]
  node [
    id 76
    label "period"
  ]
  node [
    id 77
    label "prekambr"
  ]
  node [
    id 78
    label "jura"
  ]
  node [
    id 79
    label "interstadia&#322;"
  ]
  node [
    id 80
    label "jednostka_geologiczna"
  ]
  node [
    id 81
    label "izochronizm"
  ]
  node [
    id 82
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 83
    label "okres_noachijski"
  ]
  node [
    id 84
    label "orosir"
  ]
  node [
    id 85
    label "sten"
  ]
  node [
    id 86
    label "kreda"
  ]
  node [
    id 87
    label "drugorz&#281;d"
  ]
  node [
    id 88
    label "semester"
  ]
  node [
    id 89
    label "trzeciorz&#281;d"
  ]
  node [
    id 90
    label "ton"
  ]
  node [
    id 91
    label "dzieje"
  ]
  node [
    id 92
    label "poprzednik"
  ]
  node [
    id 93
    label "kalim"
  ]
  node [
    id 94
    label "ordowik"
  ]
  node [
    id 95
    label "karbon"
  ]
  node [
    id 96
    label "trias"
  ]
  node [
    id 97
    label "stater"
  ]
  node [
    id 98
    label "era"
  ]
  node [
    id 99
    label "cykl"
  ]
  node [
    id 100
    label "p&#243;&#322;okres"
  ]
  node [
    id 101
    label "czwartorz&#281;d"
  ]
  node [
    id 102
    label "pulsacja"
  ]
  node [
    id 103
    label "okres_amazo&#324;ski"
  ]
  node [
    id 104
    label "kambr"
  ]
  node [
    id 105
    label "Zeitgeist"
  ]
  node [
    id 106
    label "nast&#281;pnik"
  ]
  node [
    id 107
    label "kriogen"
  ]
  node [
    id 108
    label "glacja&#322;"
  ]
  node [
    id 109
    label "fala"
  ]
  node [
    id 110
    label "okres_czasu"
  ]
  node [
    id 111
    label "riak"
  ]
  node [
    id 112
    label "schy&#322;ek"
  ]
  node [
    id 113
    label "okres_hesperyjski"
  ]
  node [
    id 114
    label "sylur"
  ]
  node [
    id 115
    label "dewon"
  ]
  node [
    id 116
    label "ciota"
  ]
  node [
    id 117
    label "epoka"
  ]
  node [
    id 118
    label "pierwszorz&#281;d"
  ]
  node [
    id 119
    label "okres_halsztacki"
  ]
  node [
    id 120
    label "ektas"
  ]
  node [
    id 121
    label "zdanie"
  ]
  node [
    id 122
    label "condition"
  ]
  node [
    id 123
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 124
    label "rok_akademicki"
  ]
  node [
    id 125
    label "wapie&#324;_muszlowy"
  ]
  node [
    id 126
    label "postglacja&#322;"
  ]
  node [
    id 127
    label "faza"
  ]
  node [
    id 128
    label "proces_fizjologiczny"
  ]
  node [
    id 129
    label "ediakar"
  ]
  node [
    id 130
    label "time_period"
  ]
  node [
    id 131
    label "ruch_drgaj&#261;cy"
  ]
  node [
    id 132
    label "perm"
  ]
  node [
    id 133
    label "rok_szkolny"
  ]
  node [
    id 134
    label "neogen"
  ]
  node [
    id 135
    label "sider"
  ]
  node [
    id 136
    label "flow"
  ]
  node [
    id 137
    label "podokres"
  ]
  node [
    id 138
    label "preglacja&#322;"
  ]
  node [
    id 139
    label "retoryka"
  ]
  node [
    id 140
    label "choroba_przyrodzona"
  ]
  node [
    id 141
    label "d&#322;ugoterminowo"
  ]
  node [
    id 142
    label "przysz&#322;o&#347;ciowy"
  ]
  node [
    id 143
    label "d&#322;ugi"
  ]
  node [
    id 144
    label "czasowy"
  ]
  node [
    id 145
    label "pora_roku"
  ]
  node [
    id 146
    label "nale&#380;ny"
  ]
  node [
    id 147
    label "si&#281;ga&#263;"
  ]
  node [
    id 148
    label "trwa&#263;"
  ]
  node [
    id 149
    label "obecno&#347;&#263;"
  ]
  node [
    id 150
    label "stan"
  ]
  node [
    id 151
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 152
    label "stand"
  ]
  node [
    id 153
    label "mie&#263;_miejsce"
  ]
  node [
    id 154
    label "uczestniczy&#263;"
  ]
  node [
    id 155
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 156
    label "equal"
  ]
  node [
    id 157
    label "robienie"
  ]
  node [
    id 158
    label "kr&#261;&#380;enie"
  ]
  node [
    id 159
    label "rzecz"
  ]
  node [
    id 160
    label "zbacza&#263;"
  ]
  node [
    id 161
    label "entity"
  ]
  node [
    id 162
    label "element"
  ]
  node [
    id 163
    label "omawia&#263;"
  ]
  node [
    id 164
    label "om&#243;wi&#263;"
  ]
  node [
    id 165
    label "sponiewiera&#263;"
  ]
  node [
    id 166
    label "sponiewieranie"
  ]
  node [
    id 167
    label "omawianie"
  ]
  node [
    id 168
    label "charakter"
  ]
  node [
    id 169
    label "program_nauczania"
  ]
  node [
    id 170
    label "w&#261;tek"
  ]
  node [
    id 171
    label "thing"
  ]
  node [
    id 172
    label "zboczenie"
  ]
  node [
    id 173
    label "zbaczanie"
  ]
  node [
    id 174
    label "tre&#347;&#263;"
  ]
  node [
    id 175
    label "tematyka"
  ]
  node [
    id 176
    label "istota"
  ]
  node [
    id 177
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 178
    label "kultura"
  ]
  node [
    id 179
    label "zboczy&#263;"
  ]
  node [
    id 180
    label "discipline"
  ]
  node [
    id 181
    label "om&#243;wienie"
  ]
  node [
    id 182
    label "szczeg&#243;&#322;owo"
  ]
  node [
    id 183
    label "skrupulatny"
  ]
  node [
    id 184
    label "w&#261;ski"
  ]
  node [
    id 185
    label "opis"
  ]
  node [
    id 186
    label "analysis"
  ]
  node [
    id 187
    label "reakcja_chemiczna"
  ]
  node [
    id 188
    label "dissection"
  ]
  node [
    id 189
    label "badanie"
  ]
  node [
    id 190
    label "metoda"
  ]
  node [
    id 191
    label "dodatek"
  ]
  node [
    id 192
    label "modernizacja"
  ]
  node [
    id 193
    label "poprawka"
  ]
  node [
    id 194
    label "modernization"
  ]
  node [
    id 195
    label "masztab"
  ]
  node [
    id 196
    label "izarytma"
  ]
  node [
    id 197
    label "plot"
  ]
  node [
    id 198
    label "legenda"
  ]
  node [
    id 199
    label "fotoszkic"
  ]
  node [
    id 200
    label "atlas"
  ]
  node [
    id 201
    label "wododzia&#322;"
  ]
  node [
    id 202
    label "rysunek"
  ]
  node [
    id 203
    label "god&#322;o_mapy"
  ]
  node [
    id 204
    label "instrumentalny"
  ]
  node [
    id 205
    label "akustycznie"
  ]
  node [
    id 206
    label "spis"
  ]
  node [
    id 207
    label "odinstalowanie"
  ]
  node [
    id 208
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 209
    label "za&#322;o&#380;enie"
  ]
  node [
    id 210
    label "podstawa"
  ]
  node [
    id 211
    label "emitowanie"
  ]
  node [
    id 212
    label "odinstalowywanie"
  ]
  node [
    id 213
    label "instrukcja"
  ]
  node [
    id 214
    label "punkt"
  ]
  node [
    id 215
    label "teleferie"
  ]
  node [
    id 216
    label "emitowa&#263;"
  ]
  node [
    id 217
    label "wytw&#243;r"
  ]
  node [
    id 218
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 219
    label "sekcja_krytyczna"
  ]
  node [
    id 220
    label "prezentowa&#263;"
  ]
  node [
    id 221
    label "blok"
  ]
  node [
    id 222
    label "podprogram"
  ]
  node [
    id 223
    label "tryb"
  ]
  node [
    id 224
    label "dzia&#322;"
  ]
  node [
    id 225
    label "broszura"
  ]
  node [
    id 226
    label "deklaracja"
  ]
  node [
    id 227
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 228
    label "struktura_organizacyjna"
  ]
  node [
    id 229
    label "zaprezentowanie"
  ]
  node [
    id 230
    label "informatyka"
  ]
  node [
    id 231
    label "booklet"
  ]
  node [
    id 232
    label "menu"
  ]
  node [
    id 233
    label "oprogramowanie"
  ]
  node [
    id 234
    label "instalowanie"
  ]
  node [
    id 235
    label "furkacja"
  ]
  node [
    id 236
    label "odinstalowa&#263;"
  ]
  node [
    id 237
    label "instalowa&#263;"
  ]
  node [
    id 238
    label "okno"
  ]
  node [
    id 239
    label "pirat"
  ]
  node [
    id 240
    label "zainstalowanie"
  ]
  node [
    id 241
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 242
    label "ogranicznik_referencyjny"
  ]
  node [
    id 243
    label "zainstalowa&#263;"
  ]
  node [
    id 244
    label "kana&#322;"
  ]
  node [
    id 245
    label "zaprezentowa&#263;"
  ]
  node [
    id 246
    label "interfejs"
  ]
  node [
    id 247
    label "odinstalowywa&#263;"
  ]
  node [
    id 248
    label "folder"
  ]
  node [
    id 249
    label "course_of_study"
  ]
  node [
    id 250
    label "ram&#243;wka"
  ]
  node [
    id 251
    label "prezentowanie"
  ]
  node [
    id 252
    label "oferta"
  ]
  node [
    id 253
    label "przyczyna"
  ]
  node [
    id 254
    label "uwaga"
  ]
  node [
    id 255
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 256
    label "punkt_widzenia"
  ]
  node [
    id 257
    label "volunteer"
  ]
  node [
    id 258
    label "opuszcza&#263;_si&#281;"
  ]
  node [
    id 259
    label "lot_&#347;lizgowy"
  ]
  node [
    id 260
    label "organize"
  ]
  node [
    id 261
    label "my&#347;le&#263;"
  ]
  node [
    id 262
    label "opracowywa&#263;"
  ]
  node [
    id 263
    label "project"
  ]
  node [
    id 264
    label "mean"
  ]
  node [
    id 265
    label "anatomopatolog"
  ]
  node [
    id 266
    label "rewizja"
  ]
  node [
    id 267
    label "oznaka"
  ]
  node [
    id 268
    label "ferment"
  ]
  node [
    id 269
    label "komplet"
  ]
  node [
    id 270
    label "tura"
  ]
  node [
    id 271
    label "amendment"
  ]
  node [
    id 272
    label "zmianka"
  ]
  node [
    id 273
    label "odmienianie"
  ]
  node [
    id 274
    label "passage"
  ]
  node [
    id 275
    label "zjawisko"
  ]
  node [
    id 276
    label "change"
  ]
  node [
    id 277
    label "praca"
  ]
  node [
    id 278
    label "sk&#322;ad"
  ]
  node [
    id 279
    label "zachowanie"
  ]
  node [
    id 280
    label "umowa"
  ]
  node [
    id 281
    label "podsystem"
  ]
  node [
    id 282
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 283
    label "przeciwsobno&#347;&#263;"
  ]
  node [
    id 284
    label "system"
  ]
  node [
    id 285
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 286
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 287
    label "struktura"
  ]
  node [
    id 288
    label "wi&#281;&#378;"
  ]
  node [
    id 289
    label "zawarcie"
  ]
  node [
    id 290
    label "systemat"
  ]
  node [
    id 291
    label "usenet"
  ]
  node [
    id 292
    label "ONZ"
  ]
  node [
    id 293
    label "o&#347;"
  ]
  node [
    id 294
    label "organ"
  ]
  node [
    id 295
    label "przestawi&#263;"
  ]
  node [
    id 296
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 297
    label "traktat_wersalski"
  ]
  node [
    id 298
    label "rozprz&#261;c"
  ]
  node [
    id 299
    label "cybernetyk"
  ]
  node [
    id 300
    label "cia&#322;o"
  ]
  node [
    id 301
    label "zawrze&#263;"
  ]
  node [
    id 302
    label "konstelacja"
  ]
  node [
    id 303
    label "alliance"
  ]
  node [
    id 304
    label "zbi&#243;r"
  ]
  node [
    id 305
    label "NATO"
  ]
  node [
    id 306
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 307
    label "treaty"
  ]
  node [
    id 308
    label "wy&#322;adowczy"
  ]
  node [
    id 309
    label "Brac&#322;aw"
  ]
  node [
    id 310
    label "Zb&#261;szy&#324;"
  ]
  node [
    id 311
    label "G&#322;uch&#243;w"
  ]
  node [
    id 312
    label "Hallstatt"
  ]
  node [
    id 313
    label "Zbara&#380;"
  ]
  node [
    id 314
    label "Wielkie_Tyrnowo"
  ]
  node [
    id 315
    label "Nachiczewan"
  ]
  node [
    id 316
    label "Suworow"
  ]
  node [
    id 317
    label "Halicz"
  ]
  node [
    id 318
    label "Gandawa"
  ]
  node [
    id 319
    label "Kamionka_Strumi&#322;owa"
  ]
  node [
    id 320
    label "Wismar"
  ]
  node [
    id 321
    label "Norymberga"
  ]
  node [
    id 322
    label "Ruciane-Nida"
  ]
  node [
    id 323
    label "Wia&#378;ma"
  ]
  node [
    id 324
    label "Sewilla"
  ]
  node [
    id 325
    label "Pietropaw&#322;owsk"
  ]
  node [
    id 326
    label "Kobry&#324;"
  ]
  node [
    id 327
    label "Brno"
  ]
  node [
    id 328
    label "Tomsk"
  ]
  node [
    id 329
    label "Poniatowa"
  ]
  node [
    id 330
    label "Hadziacz"
  ]
  node [
    id 331
    label "Tiume&#324;"
  ]
  node [
    id 332
    label "Karlsbad"
  ]
  node [
    id 333
    label "Drohobycz"
  ]
  node [
    id 334
    label "Lyon"
  ]
  node [
    id 335
    label "Dzier&#380;y&#324;sk"
  ]
  node [
    id 336
    label "K&#322;odawa"
  ]
  node [
    id 337
    label "Solikamsk"
  ]
  node [
    id 338
    label "Wolgast"
  ]
  node [
    id 339
    label "Saloniki"
  ]
  node [
    id 340
    label "Lw&#243;w"
  ]
  node [
    id 341
    label "Al-Kufa"
  ]
  node [
    id 342
    label "Hamburg"
  ]
  node [
    id 343
    label "Bogusz&#243;w-Gorce"
  ]
  node [
    id 344
    label "Nampula"
  ]
  node [
    id 345
    label "burmistrz"
  ]
  node [
    id 346
    label "D&#252;sseldorf"
  ]
  node [
    id 347
    label "Nowy_Orlean"
  ]
  node [
    id 348
    label "Bamberg"
  ]
  node [
    id 349
    label "Osaka"
  ]
  node [
    id 350
    label "Niko&#322;ajewsk_nad_Amurem"
  ]
  node [
    id 351
    label "Michalovce"
  ]
  node [
    id 352
    label "Fryburg"
  ]
  node [
    id 353
    label "Trabzon"
  ]
  node [
    id 354
    label "Wersal"
  ]
  node [
    id 355
    label "Swatowe"
  ]
  node [
    id 356
    label "Ka&#322;uga"
  ]
  node [
    id 357
    label "Dijon"
  ]
  node [
    id 358
    label "Cannes"
  ]
  node [
    id 359
    label "Borowsk"
  ]
  node [
    id 360
    label "Kursk"
  ]
  node [
    id 361
    label "Tyberiada"
  ]
  node [
    id 362
    label "Boden"
  ]
  node [
    id 363
    label "Dodona"
  ]
  node [
    id 364
    label "Vukovar"
  ]
  node [
    id 365
    label "Soleczniki"
  ]
  node [
    id 366
    label "Barcelona"
  ]
  node [
    id 367
    label "Oszmiana"
  ]
  node [
    id 368
    label "Stuttgart"
  ]
  node [
    id 369
    label "Nerczy&#324;sk"
  ]
  node [
    id 370
    label "Bijsk"
  ]
  node [
    id 371
    label "Essen"
  ]
  node [
    id 372
    label "Luboml"
  ]
  node [
    id 373
    label "Gr&#243;dek"
  ]
  node [
    id 374
    label "Orany"
  ]
  node [
    id 375
    label "Siedliszcze"
  ]
  node [
    id 376
    label "P&#322;owdiw"
  ]
  node [
    id 377
    label "A&#322;apajewsk"
  ]
  node [
    id 378
    label "Liverpool"
  ]
  node [
    id 379
    label "Ostrawa"
  ]
  node [
    id 380
    label "Penza"
  ]
  node [
    id 381
    label "Rudki"
  ]
  node [
    id 382
    label "Aktobe"
  ]
  node [
    id 383
    label "I&#322;awka"
  ]
  node [
    id 384
    label "Tolkmicko"
  ]
  node [
    id 385
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 386
    label "Sajgon"
  ]
  node [
    id 387
    label "Windawa"
  ]
  node [
    id 388
    label "Weimar"
  ]
  node [
    id 389
    label "Jekaterynburg"
  ]
  node [
    id 390
    label "Lejda"
  ]
  node [
    id 391
    label "Cremona"
  ]
  node [
    id 392
    label "Wo&#322;godo&#324;sk"
  ]
  node [
    id 393
    label "Kordoba"
  ]
  node [
    id 394
    label "urz&#261;d"
  ]
  node [
    id 395
    label "&#321;ohojsk"
  ]
  node [
    id 396
    label "Kalmar"
  ]
  node [
    id 397
    label "Akerman"
  ]
  node [
    id 398
    label "Locarno"
  ]
  node [
    id 399
    label "Bych&#243;w"
  ]
  node [
    id 400
    label "Toledo"
  ]
  node [
    id 401
    label "Minusi&#324;sk"
  ]
  node [
    id 402
    label "Szk&#322;&#243;w"
  ]
  node [
    id 403
    label "Wenecja"
  ]
  node [
    id 404
    label "Bazylea"
  ]
  node [
    id 405
    label "Peszt"
  ]
  node [
    id 406
    label "Piza"
  ]
  node [
    id 407
    label "Tanger"
  ]
  node [
    id 408
    label "Krzywi&#324;"
  ]
  node [
    id 409
    label "Eger"
  ]
  node [
    id 410
    label "Bogus&#322;aw"
  ]
  node [
    id 411
    label "Taganrog"
  ]
  node [
    id 412
    label "Oksford"
  ]
  node [
    id 413
    label "Gwardiejsk"
  ]
  node [
    id 414
    label "Tyraspol"
  ]
  node [
    id 415
    label "Kleczew"
  ]
  node [
    id 416
    label "Nowa_D&#281;ba"
  ]
  node [
    id 417
    label "Wilejka"
  ]
  node [
    id 418
    label "Modena"
  ]
  node [
    id 419
    label "Demmin"
  ]
  node [
    id 420
    label "Houston"
  ]
  node [
    id 421
    label "Rydu&#322;towy"
  ]
  node [
    id 422
    label "Bordeaux"
  ]
  node [
    id 423
    label "Schmalkalden"
  ]
  node [
    id 424
    label "O&#322;omuniec"
  ]
  node [
    id 425
    label "Tuluza"
  ]
  node [
    id 426
    label "tramwaj"
  ]
  node [
    id 427
    label "Nantes"
  ]
  node [
    id 428
    label "Debreczyn"
  ]
  node [
    id 429
    label "Kowel"
  ]
  node [
    id 430
    label "Witnica"
  ]
  node [
    id 431
    label "Stalingrad"
  ]
  node [
    id 432
    label "Drezno"
  ]
  node [
    id 433
    label "Perejas&#322;aw"
  ]
  node [
    id 434
    label "Luksor"
  ]
  node [
    id 435
    label "Ostaszk&#243;w"
  ]
  node [
    id 436
    label "Gettysburg"
  ]
  node [
    id 437
    label "Trydent"
  ]
  node [
    id 438
    label "Poczdam"
  ]
  node [
    id 439
    label "Mesyna"
  ]
  node [
    id 440
    label "Krasnogorsk"
  ]
  node [
    id 441
    label "Kars"
  ]
  node [
    id 442
    label "Darmstadt"
  ]
  node [
    id 443
    label "Rzg&#243;w"
  ]
  node [
    id 444
    label "Kar&#322;owice"
  ]
  node [
    id 445
    label "Czeskie_Budziejowice"
  ]
  node [
    id 446
    label "Buda"
  ]
  node [
    id 447
    label "Monako"
  ]
  node [
    id 448
    label "Pardubice"
  ]
  node [
    id 449
    label "Pas&#322;&#281;k"
  ]
  node [
    id 450
    label "Fatima"
  ]
  node [
    id 451
    label "Bir&#380;e"
  ]
  node [
    id 452
    label "Wo&#322;oko&#322;amsk"
  ]
  node [
    id 453
    label "Wi&#322;komierz"
  ]
  node [
    id 454
    label "Opawa"
  ]
  node [
    id 455
    label "Mantua"
  ]
  node [
    id 456
    label "ulica"
  ]
  node [
    id 457
    label "Tarragona"
  ]
  node [
    id 458
    label "Antwerpia"
  ]
  node [
    id 459
    label "Asuan"
  ]
  node [
    id 460
    label "Korynt"
  ]
  node [
    id 461
    label "Armenia"
  ]
  node [
    id 462
    label "Budionnowsk"
  ]
  node [
    id 463
    label "Lengyel"
  ]
  node [
    id 464
    label "Betlejem"
  ]
  node [
    id 465
    label "Asy&#380;"
  ]
  node [
    id 466
    label "Batumi"
  ]
  node [
    id 467
    label "Paczk&#243;w"
  ]
  node [
    id 468
    label "Grenada"
  ]
  node [
    id 469
    label "Suczawa"
  ]
  node [
    id 470
    label "Nowogard"
  ]
  node [
    id 471
    label "Tyr"
  ]
  node [
    id 472
    label "Bria&#324;sk"
  ]
  node [
    id 473
    label "Bar"
  ]
  node [
    id 474
    label "Czerkiesk"
  ]
  node [
    id 475
    label "Ja&#322;ta"
  ]
  node [
    id 476
    label "Mo&#347;ciska"
  ]
  node [
    id 477
    label "Medyna"
  ]
  node [
    id 478
    label "Tartu"
  ]
  node [
    id 479
    label "Pemba"
  ]
  node [
    id 480
    label "Lipawa"
  ]
  node [
    id 481
    label "Tyl&#380;a"
  ]
  node [
    id 482
    label "Lipsk"
  ]
  node [
    id 483
    label "Dayton"
  ]
  node [
    id 484
    label "Rohatyn"
  ]
  node [
    id 485
    label "Peszawar"
  ]
  node [
    id 486
    label "Azow"
  ]
  node [
    id 487
    label "Adrianopol"
  ]
  node [
    id 488
    label "Iwano-Frankowsk"
  ]
  node [
    id 489
    label "Czarnobyl"
  ]
  node [
    id 490
    label "Rakoniewice"
  ]
  node [
    id 491
    label "Obuch&#243;w"
  ]
  node [
    id 492
    label "Orneta"
  ]
  node [
    id 493
    label "Koszyce"
  ]
  node [
    id 494
    label "Czeski_Cieszyn"
  ]
  node [
    id 495
    label "Zagorsk"
  ]
  node [
    id 496
    label "Nieder_Selters"
  ]
  node [
    id 497
    label "Ko&#322;omna"
  ]
  node [
    id 498
    label "Rost&#243;w"
  ]
  node [
    id 499
    label "Bolonia"
  ]
  node [
    id 500
    label "Rajgr&#243;d"
  ]
  node [
    id 501
    label "L&#252;neburg"
  ]
  node [
    id 502
    label "Brack"
  ]
  node [
    id 503
    label "Konstancja"
  ]
  node [
    id 504
    label "Koluszki"
  ]
  node [
    id 505
    label "Semipa&#322;aty&#324;sk"
  ]
  node [
    id 506
    label "Suez"
  ]
  node [
    id 507
    label "Mrocza"
  ]
  node [
    id 508
    label "Triest"
  ]
  node [
    id 509
    label "Murma&#324;sk"
  ]
  node [
    id 510
    label "Tu&#322;a"
  ]
  node [
    id 511
    label "Tarnogr&#243;d"
  ]
  node [
    id 512
    label "Radziech&#243;w"
  ]
  node [
    id 513
    label "Kokand"
  ]
  node [
    id 514
    label "Kircholm"
  ]
  node [
    id 515
    label "Nowa_Ruda"
  ]
  node [
    id 516
    label "Huma&#324;"
  ]
  node [
    id 517
    label "Turkiestan"
  ]
  node [
    id 518
    label "Kani&#243;w"
  ]
  node [
    id 519
    label "Pilzno"
  ]
  node [
    id 520
    label "Dubno"
  ]
  node [
    id 521
    label "Bras&#322;aw"
  ]
  node [
    id 522
    label "Korfant&#243;w"
  ]
  node [
    id 523
    label "Choroszcz"
  ]
  node [
    id 524
    label "Nowogr&#243;d"
  ]
  node [
    id 525
    label "Konotop"
  ]
  node [
    id 526
    label "M&#347;cis&#322;aw"
  ]
  node [
    id 527
    label "Jastarnia"
  ]
  node [
    id 528
    label "Ba&#324;ska_Bystrzyca"
  ]
  node [
    id 529
    label "Omsk"
  ]
  node [
    id 530
    label "Troick"
  ]
  node [
    id 531
    label "Koper"
  ]
  node [
    id 532
    label "Jenisejsk"
  ]
  node [
    id 533
    label "Frankfurt_nad_Odr&#261;"
  ]
  node [
    id 534
    label "Oldenburg_in_Holstein"
  ]
  node [
    id 535
    label "Trenczyn"
  ]
  node [
    id 536
    label "Wormacja"
  ]
  node [
    id 537
    label "Wagram"
  ]
  node [
    id 538
    label "Lubeka"
  ]
  node [
    id 539
    label "Genewa"
  ]
  node [
    id 540
    label "&#379;ydacz&#243;w"
  ]
  node [
    id 541
    label "Kleck"
  ]
  node [
    id 542
    label "G&#243;ra_Kalwaria"
  ]
  node [
    id 543
    label "Struga"
  ]
  node [
    id 544
    label "Izmir"
  ]
  node [
    id 545
    label "Dortmund"
  ]
  node [
    id 546
    label "Izbica_Kujawska"
  ]
  node [
    id 547
    label "Stalinogorsk"
  ]
  node [
    id 548
    label "Workuta"
  ]
  node [
    id 549
    label "Jerycho"
  ]
  node [
    id 550
    label "Brunszwik"
  ]
  node [
    id 551
    label "Aleksandria"
  ]
  node [
    id 552
    label "miejscowo&#347;&#263;"
  ]
  node [
    id 553
    label "Borys&#322;aw"
  ]
  node [
    id 554
    label "Zaleszczyki"
  ]
  node [
    id 555
    label "Z&#322;oczew"
  ]
  node [
    id 556
    label "Piast&#243;w"
  ]
  node [
    id 557
    label "Frankfurt_nad_Menem"
  ]
  node [
    id 558
    label "Bor"
  ]
  node [
    id 559
    label "Nazaret"
  ]
  node [
    id 560
    label "Sarat&#243;w"
  ]
  node [
    id 561
    label "Brasz&#243;w"
  ]
  node [
    id 562
    label "Malin"
  ]
  node [
    id 563
    label "Parma"
  ]
  node [
    id 564
    label "Wierchoja&#324;sk"
  ]
  node [
    id 565
    label "Tarent"
  ]
  node [
    id 566
    label "Mariampol"
  ]
  node [
    id 567
    label "Wuhan"
  ]
  node [
    id 568
    label "Split"
  ]
  node [
    id 569
    label "Baranowicze"
  ]
  node [
    id 570
    label "Marki"
  ]
  node [
    id 571
    label "Adana"
  ]
  node [
    id 572
    label "B&#322;aszki"
  ]
  node [
    id 573
    label "Lubecz"
  ]
  node [
    id 574
    label "Sulech&#243;w"
  ]
  node [
    id 575
    label "Borys&#243;w"
  ]
  node [
    id 576
    label "Homel"
  ]
  node [
    id 577
    label "Tours"
  ]
  node [
    id 578
    label "Kapsztad"
  ]
  node [
    id 579
    label "Edam"
  ]
  node [
    id 580
    label "Zaporo&#380;e"
  ]
  node [
    id 581
    label "Miedwie&#380;jegorsk"
  ]
  node [
    id 582
    label "Kamieniec_Podolski"
  ]
  node [
    id 583
    label "Chocim"
  ]
  node [
    id 584
    label "Mohylew"
  ]
  node [
    id 585
    label "Merseburg"
  ]
  node [
    id 586
    label "Konstantynopol"
  ]
  node [
    id 587
    label "Sambor"
  ]
  node [
    id 588
    label "Manchester"
  ]
  node [
    id 589
    label "Pi&#324;sk"
  ]
  node [
    id 590
    label "Ochryda"
  ]
  node [
    id 591
    label "Rybi&#324;sk"
  ]
  node [
    id 592
    label "Czadca"
  ]
  node [
    id 593
    label "Orenburg"
  ]
  node [
    id 594
    label "Krajowa"
  ]
  node [
    id 595
    label "Eleusis"
  ]
  node [
    id 596
    label "Awinion"
  ]
  node [
    id 597
    label "Rzeczyca"
  ]
  node [
    id 598
    label "Barczewo"
  ]
  node [
    id 599
    label "Lozanna"
  ]
  node [
    id 600
    label "&#379;migr&#243;d"
  ]
  node [
    id 601
    label "Chabarowsk"
  ]
  node [
    id 602
    label "Jena"
  ]
  node [
    id 603
    label "Xai-Xai"
  ]
  node [
    id 604
    label "Radk&#243;w"
  ]
  node [
    id 605
    label "Syrakuzy"
  ]
  node [
    id 606
    label "Zas&#322;aw"
  ]
  node [
    id 607
    label "Getynga"
  ]
  node [
    id 608
    label "Windsor"
  ]
  node [
    id 609
    label "Carrara"
  ]
  node [
    id 610
    label "Madras"
  ]
  node [
    id 611
    label "Nitra"
  ]
  node [
    id 612
    label "Kilonia"
  ]
  node [
    id 613
    label "Rawenna"
  ]
  node [
    id 614
    label "Stawropol"
  ]
  node [
    id 615
    label "Warna"
  ]
  node [
    id 616
    label "Ba&#322;tijsk"
  ]
  node [
    id 617
    label "Cumana"
  ]
  node [
    id 618
    label "Kostroma"
  ]
  node [
    id 619
    label "Bajonna"
  ]
  node [
    id 620
    label "Magadan"
  ]
  node [
    id 621
    label "Kercz"
  ]
  node [
    id 622
    label "Harbin"
  ]
  node [
    id 623
    label "Sankt_Florian"
  ]
  node [
    id 624
    label "Norak"
  ]
  node [
    id 625
    label "Wo&#322;kowysk"
  ]
  node [
    id 626
    label "Bia&#322;ogr&#243;d_nad_Dniestrem"
  ]
  node [
    id 627
    label "S&#232;vres"
  ]
  node [
    id 628
    label "Barwice"
  ]
  node [
    id 629
    label "Jutrosin"
  ]
  node [
    id 630
    label "Sumy"
  ]
  node [
    id 631
    label "Canterbury"
  ]
  node [
    id 632
    label "Czerkasy"
  ]
  node [
    id 633
    label "Troki"
  ]
  node [
    id 634
    label "Dnieprodzier&#380;y&#324;sk"
  ]
  node [
    id 635
    label "Turka"
  ]
  node [
    id 636
    label "Budziszyn"
  ]
  node [
    id 637
    label "A&#322;czewsk"
  ]
  node [
    id 638
    label "Chark&#243;w"
  ]
  node [
    id 639
    label "Go&#347;cino"
  ]
  node [
    id 640
    label "Ku&#378;nieck"
  ]
  node [
    id 641
    label "Wotki&#324;sk"
  ]
  node [
    id 642
    label "Symferopol"
  ]
  node [
    id 643
    label "Dmitrow"
  ]
  node [
    id 644
    label "Cherso&#324;"
  ]
  node [
    id 645
    label "zabudowa"
  ]
  node [
    id 646
    label "Nowogr&#243;dek"
  ]
  node [
    id 647
    label "Orlean"
  ]
  node [
    id 648
    label "Ku&#378;nia_Raciborska"
  ]
  node [
    id 649
    label "Berdia&#324;sk"
  ]
  node [
    id 650
    label "Szumsk"
  ]
  node [
    id 651
    label "Z&#322;ocz&#243;w"
  ]
  node [
    id 652
    label "Orsza"
  ]
  node [
    id 653
    label "Cluny"
  ]
  node [
    id 654
    label "Aralsk"
  ]
  node [
    id 655
    label "Stoczek_&#321;ukowski"
  ]
  node [
    id 656
    label "Bogumin"
  ]
  node [
    id 657
    label "Antiochia"
  ]
  node [
    id 658
    label "grupa"
  ]
  node [
    id 659
    label "Inhambane"
  ]
  node [
    id 660
    label "S&#322;awia&#324;sk_nad_Kubaniem"
  ]
  node [
    id 661
    label "Wielki_Pres&#322;aw"
  ]
  node [
    id 662
    label "Trewir"
  ]
  node [
    id 663
    label "Nowogr&#243;d_Wielki"
  ]
  node [
    id 664
    label "Siewieromorsk"
  ]
  node [
    id 665
    label "Calais"
  ]
  node [
    id 666
    label "&#379;ytawa"
  ]
  node [
    id 667
    label "Eupatoria"
  ]
  node [
    id 668
    label "Twer"
  ]
  node [
    id 669
    label "Stara_Zagora"
  ]
  node [
    id 670
    label "Jastrowie"
  ]
  node [
    id 671
    label "Piatigorsk"
  ]
  node [
    id 672
    label "&#379;&#243;&#322;kiew"
  ]
  node [
    id 673
    label "Le&#324;sk"
  ]
  node [
    id 674
    label "Johannesburg"
  ]
  node [
    id 675
    label "Kaszyn"
  ]
  node [
    id 676
    label "&#346;wi&#281;ciany"
  ]
  node [
    id 677
    label "&#379;ylina"
  ]
  node [
    id 678
    label "Sewastopol"
  ]
  node [
    id 679
    label "Pietrozawodsk"
  ]
  node [
    id 680
    label "Bobolice"
  ]
  node [
    id 681
    label "Mosty"
  ]
  node [
    id 682
    label "Korsu&#324;_Szewczenkowski"
  ]
  node [
    id 683
    label "Karaganda"
  ]
  node [
    id 684
    label "Marsylia"
  ]
  node [
    id 685
    label "Buchara"
  ]
  node [
    id 686
    label "Dubrownik"
  ]
  node [
    id 687
    label "Be&#322;z"
  ]
  node [
    id 688
    label "Oran"
  ]
  node [
    id 689
    label "Regensburg"
  ]
  node [
    id 690
    label "Rotterdam"
  ]
  node [
    id 691
    label "Trembowla"
  ]
  node [
    id 692
    label "Woskriesiensk"
  ]
  node [
    id 693
    label "Po&#322;ock"
  ]
  node [
    id 694
    label "Poprad"
  ]
  node [
    id 695
    label "Los_Angeles"
  ]
  node [
    id 696
    label "Kronsztad"
  ]
  node [
    id 697
    label "U&#322;an_Ude"
  ]
  node [
    id 698
    label "U&#347;cie_nad_&#321;ab&#261;"
  ]
  node [
    id 699
    label "W&#322;adywostok"
  ]
  node [
    id 700
    label "Kandahar"
  ]
  node [
    id 701
    label "Tobolsk"
  ]
  node [
    id 702
    label "Boston"
  ]
  node [
    id 703
    label "Hawana"
  ]
  node [
    id 704
    label "Kis&#322;owodzk"
  ]
  node [
    id 705
    label "Tulon"
  ]
  node [
    id 706
    label "Utrecht"
  ]
  node [
    id 707
    label "Oleszyce"
  ]
  node [
    id 708
    label "Hradec_Kr&#225;lov&#233;"
  ]
  node [
    id 709
    label "Katania"
  ]
  node [
    id 710
    label "Teby"
  ]
  node [
    id 711
    label "Paw&#322;owo"
  ]
  node [
    id 712
    label "W&#252;rzburg"
  ]
  node [
    id 713
    label "Podiebrady"
  ]
  node [
    id 714
    label "Uppsala"
  ]
  node [
    id 715
    label "Poniewie&#380;"
  ]
  node [
    id 716
    label "Berezyna"
  ]
  node [
    id 717
    label "Aczy&#324;sk"
  ]
  node [
    id 718
    label "Niko&#322;ajewsk"
  ]
  node [
    id 719
    label "Ostr&#243;g"
  ]
  node [
    id 720
    label "Brze&#347;&#263;"
  ]
  node [
    id 721
    label "Stryj"
  ]
  node [
    id 722
    label "Lancaster"
  ]
  node [
    id 723
    label "Kozielsk"
  ]
  node [
    id 724
    label "Loreto"
  ]
  node [
    id 725
    label "B&#322;agowieszcze&#324;sk"
  ]
  node [
    id 726
    label "Hebron"
  ]
  node [
    id 727
    label "Kaspijsk"
  ]
  node [
    id 728
    label "Peczora"
  ]
  node [
    id 729
    label "Isfahan"
  ]
  node [
    id 730
    label "Chimoio"
  ]
  node [
    id 731
    label "Mory&#324;"
  ]
  node [
    id 732
    label "Kowno"
  ]
  node [
    id 733
    label "Nie&#347;wie&#380;"
  ]
  node [
    id 734
    label "Opalenica"
  ]
  node [
    id 735
    label "Kolonia"
  ]
  node [
    id 736
    label "Stary_Sambor"
  ]
  node [
    id 737
    label "Kolkata"
  ]
  node [
    id 738
    label "Turkmenbaszy"
  ]
  node [
    id 739
    label "Mohyl&#243;w_Podolski"
  ]
  node [
    id 740
    label "Nankin"
  ]
  node [
    id 741
    label "Krzanowice"
  ]
  node [
    id 742
    label "Efez"
  ]
  node [
    id 743
    label "Dobrodzie&#324;"
  ]
  node [
    id 744
    label "Neapol"
  ]
  node [
    id 745
    label "S&#322;uck"
  ]
  node [
    id 746
    label "Fryburg_Bryzgowijski"
  ]
  node [
    id 747
    label "W&#322;odzimierz_Wo&#322;y&#324;ski"
  ]
  node [
    id 748
    label "Frydek-Mistek"
  ]
  node [
    id 749
    label "Korsze"
  ]
  node [
    id 750
    label "T&#322;uszcz"
  ]
  node [
    id 751
    label "Soligorsk"
  ]
  node [
    id 752
    label "Kie&#380;mark"
  ]
  node [
    id 753
    label "Mannheim"
  ]
  node [
    id 754
    label "Ulm"
  ]
  node [
    id 755
    label "Podhajce"
  ]
  node [
    id 756
    label "Dniepropetrowsk"
  ]
  node [
    id 757
    label "Szamocin"
  ]
  node [
    id 758
    label "Ko&#322;omyja"
  ]
  node [
    id 759
    label "Buczacz"
  ]
  node [
    id 760
    label "M&#252;nster"
  ]
  node [
    id 761
    label "Brema"
  ]
  node [
    id 762
    label "Delhi"
  ]
  node [
    id 763
    label "Nicea"
  ]
  node [
    id 764
    label "&#346;niatyn"
  ]
  node [
    id 765
    label "Szawle"
  ]
  node [
    id 766
    label "Czerniowce"
  ]
  node [
    id 767
    label "Mi&#347;nia"
  ]
  node [
    id 768
    label "Sydney"
  ]
  node [
    id 769
    label "Moguncja"
  ]
  node [
    id 770
    label "Narbona"
  ]
  node [
    id 771
    label "Swiet&#322;ogorsk"
  ]
  node [
    id 772
    label "Wittenberga"
  ]
  node [
    id 773
    label "Uljanowsk"
  ]
  node [
    id 774
    label "Wyborg"
  ]
  node [
    id 775
    label "&#321;uga&#324;sk"
  ]
  node [
    id 776
    label "Trojan"
  ]
  node [
    id 777
    label "Kalwaria_Zebrzydowska"
  ]
  node [
    id 778
    label "Brandenburg"
  ]
  node [
    id 779
    label "Kemerowo"
  ]
  node [
    id 780
    label "Kaszgar"
  ]
  node [
    id 781
    label "Lenzen"
  ]
  node [
    id 782
    label "Nanning"
  ]
  node [
    id 783
    label "Gotha"
  ]
  node [
    id 784
    label "Zurych"
  ]
  node [
    id 785
    label "Baltimore"
  ]
  node [
    id 786
    label "&#321;uck"
  ]
  node [
    id 787
    label "Bristol"
  ]
  node [
    id 788
    label "Ferrara"
  ]
  node [
    id 789
    label "Mariupol"
  ]
  node [
    id 790
    label "przestrze&#324;_publiczna"
  ]
  node [
    id 791
    label "Filadelfia"
  ]
  node [
    id 792
    label "Czerniejewo"
  ]
  node [
    id 793
    label "Milan&#243;wek"
  ]
  node [
    id 794
    label "Lhasa"
  ]
  node [
    id 795
    label "Kanton"
  ]
  node [
    id 796
    label "Perwomajsk"
  ]
  node [
    id 797
    label "Nieftiegorsk"
  ]
  node [
    id 798
    label "Greifswald"
  ]
  node [
    id 799
    label "Pittsburgh"
  ]
  node [
    id 800
    label "Akwileja"
  ]
  node [
    id 801
    label "Norfolk"
  ]
  node [
    id 802
    label "Perm"
  ]
  node [
    id 803
    label "Fergana"
  ]
  node [
    id 804
    label "Detroit"
  ]
  node [
    id 805
    label "Starobielsk"
  ]
  node [
    id 806
    label "Wielsk"
  ]
  node [
    id 807
    label "Zaklik&#243;w"
  ]
  node [
    id 808
    label "Majsur"
  ]
  node [
    id 809
    label "Narwa"
  ]
  node [
    id 810
    label "Chicago"
  ]
  node [
    id 811
    label "Byczyna"
  ]
  node [
    id 812
    label "Mozyrz"
  ]
  node [
    id 813
    label "Konstantyn&#243;wka"
  ]
  node [
    id 814
    label "Miko&#322;aj&#243;w"
  ]
  node [
    id 815
    label "Megara"
  ]
  node [
    id 816
    label "Stralsund"
  ]
  node [
    id 817
    label "Wo&#322;gograd"
  ]
  node [
    id 818
    label "Lichinga"
  ]
  node [
    id 819
    label "Haga"
  ]
  node [
    id 820
    label "Tarnopol"
  ]
  node [
    id 821
    label "Nowomoskowsk"
  ]
  node [
    id 822
    label "K&#322;ajpeda"
  ]
  node [
    id 823
    label "Ussuryjsk"
  ]
  node [
    id 824
    label "Brugia"
  ]
  node [
    id 825
    label "Natal"
  ]
  node [
    id 826
    label "Kro&#347;niewice"
  ]
  node [
    id 827
    label "Edynburg"
  ]
  node [
    id 828
    label "Marburg"
  ]
  node [
    id 829
    label "Dalton"
  ]
  node [
    id 830
    label "S&#322;onim"
  ]
  node [
    id 831
    label "&#346;wiebodzice"
  ]
  node [
    id 832
    label "Smorgonie"
  ]
  node [
    id 833
    label "Orze&#322;"
  ]
  node [
    id 834
    label "Nowoku&#378;nieck"
  ]
  node [
    id 835
    label "Zadar"
  ]
  node [
    id 836
    label "Koprzywnica"
  ]
  node [
    id 837
    label "Angarsk"
  ]
  node [
    id 838
    label "Tel_Awiw-Jafa"
  ]
  node [
    id 839
    label "Mo&#380;ajsk"
  ]
  node [
    id 840
    label "Norylsk"
  ]
  node [
    id 841
    label "Akwizgran"
  ]
  node [
    id 842
    label "Jawor&#243;w"
  ]
  node [
    id 843
    label "weduta"
  ]
  node [
    id 844
    label "Jab&#322;onk&#243;w"
  ]
  node [
    id 845
    label "Suzdal"
  ]
  node [
    id 846
    label "W&#322;odzimierz"
  ]
  node [
    id 847
    label "Bujnaksk"
  ]
  node [
    id 848
    label "Beresteczko"
  ]
  node [
    id 849
    label "Strzelno"
  ]
  node [
    id 850
    label "Siewsk"
  ]
  node [
    id 851
    label "Cymlansk"
  ]
  node [
    id 852
    label "Trzyniec"
  ]
  node [
    id 853
    label "Hanower"
  ]
  node [
    id 854
    label "Wuppertal"
  ]
  node [
    id 855
    label "Sura&#380;"
  ]
  node [
    id 856
    label "Samara"
  ]
  node [
    id 857
    label "Winchester"
  ]
  node [
    id 858
    label "Krasnodar"
  ]
  node [
    id 859
    label "Sydon"
  ]
  node [
    id 860
    label "Worone&#380;"
  ]
  node [
    id 861
    label "Paw&#322;odar"
  ]
  node [
    id 862
    label "Czelabi&#324;sk"
  ]
  node [
    id 863
    label "Reda"
  ]
  node [
    id 864
    label "Karwina"
  ]
  node [
    id 865
    label "Wyszehrad"
  ]
  node [
    id 866
    label "Sara&#324;sk"
  ]
  node [
    id 867
    label "Koby&#322;ka"
  ]
  node [
    id 868
    label "Tambow"
  ]
  node [
    id 869
    label "Pyskowice"
  ]
  node [
    id 870
    label "Winnica"
  ]
  node [
    id 871
    label "Heidelberg"
  ]
  node [
    id 872
    label "Maribor"
  ]
  node [
    id 873
    label "Werona"
  ]
  node [
    id 874
    label "G&#322;uszyca"
  ]
  node [
    id 875
    label "Rostock"
  ]
  node [
    id 876
    label "Mekka"
  ]
  node [
    id 877
    label "Liberec"
  ]
  node [
    id 878
    label "Bie&#322;gorod"
  ]
  node [
    id 879
    label "Berdycz&#243;w"
  ]
  node [
    id 880
    label "Sierdobsk"
  ]
  node [
    id 881
    label "Bobrujsk"
  ]
  node [
    id 882
    label "Padwa"
  ]
  node [
    id 883
    label "Chanty-Mansyjsk"
  ]
  node [
    id 884
    label "Pasawa"
  ]
  node [
    id 885
    label "Poczaj&#243;w"
  ]
  node [
    id 886
    label "&#379;ar&#243;w"
  ]
  node [
    id 887
    label "Barabi&#324;sk"
  ]
  node [
    id 888
    label "Gorycja"
  ]
  node [
    id 889
    label "Haarlem"
  ]
  node [
    id 890
    label "Kiejdany"
  ]
  node [
    id 891
    label "Chmielnicki"
  ]
  node [
    id 892
    label "Siena"
  ]
  node [
    id 893
    label "Burgas"
  ]
  node [
    id 894
    label "Magnitogorsk"
  ]
  node [
    id 895
    label "Korzec"
  ]
  node [
    id 896
    label "Bonn"
  ]
  node [
    id 897
    label "Pr&#243;szk&#243;w"
  ]
  node [
    id 898
    label "Walencja"
  ]
  node [
    id 899
    label "Mosina"
  ]
  node [
    id 900
    label "figura"
  ]
  node [
    id 901
    label "wjazd"
  ]
  node [
    id 902
    label "konstrukcja"
  ]
  node [
    id 903
    label "r&#243;w"
  ]
  node [
    id 904
    label "kreacja"
  ]
  node [
    id 905
    label "posesja"
  ]
  node [
    id 906
    label "cecha"
  ]
  node [
    id 907
    label "dokumentacja_powykonawcza"
  ]
  node [
    id 908
    label "mechanika"
  ]
  node [
    id 909
    label "zwierz&#281;"
  ]
  node [
    id 910
    label "miejsce_pracy"
  ]
  node [
    id 911
    label "constitution"
  ]
  node [
    id 912
    label "gwiazda"
  ]
  node [
    id 913
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 914
    label "Powsin"
  ]
  node [
    id 915
    label "G&#243;rce"
  ]
  node [
    id 916
    label "Rakowiec"
  ]
  node [
    id 917
    label "Dojlidy"
  ]
  node [
    id 918
    label "Horodyszcze"
  ]
  node [
    id 919
    label "Kujbyszewe"
  ]
  node [
    id 920
    label "Kabaty"
  ]
  node [
    id 921
    label "jednostka_osadnicza"
  ]
  node [
    id 922
    label "Ujazd&#243;w"
  ]
  node [
    id 923
    label "Kaw&#281;czyn"
  ]
  node [
    id 924
    label "Siersza"
  ]
  node [
    id 925
    label "Groch&#243;w"
  ]
  node [
    id 926
    label "Paw&#322;owice"
  ]
  node [
    id 927
    label "Bielice"
  ]
  node [
    id 928
    label "siedziba"
  ]
  node [
    id 929
    label "Tarchomin"
  ]
  node [
    id 930
    label "Br&#243;dno"
  ]
  node [
    id 931
    label "Jelcz"
  ]
  node [
    id 932
    label "Mariensztat"
  ]
  node [
    id 933
    label "Falenica"
  ]
  node [
    id 934
    label "Izborsk"
  ]
  node [
    id 935
    label "Wi&#347;niewo"
  ]
  node [
    id 936
    label "Marymont"
  ]
  node [
    id 937
    label "Solec"
  ]
  node [
    id 938
    label "Zakrz&#243;w"
  ]
  node [
    id 939
    label "Wi&#347;niowiec"
  ]
  node [
    id 940
    label "Natolin"
  ]
  node [
    id 941
    label "Anin"
  ]
  node [
    id 942
    label "Grabiszyn"
  ]
  node [
    id 943
    label "Orunia"
  ]
  node [
    id 944
    label "Gronik"
  ]
  node [
    id 945
    label "Boryszew"
  ]
  node [
    id 946
    label "Bogucice"
  ]
  node [
    id 947
    label "&#379;era&#324;"
  ]
  node [
    id 948
    label "zesp&#243;&#322;"
  ]
  node [
    id 949
    label "Jasienica"
  ]
  node [
    id 950
    label "Laskowice_O&#322;awskie"
  ]
  node [
    id 951
    label "Salwator"
  ]
  node [
    id 952
    label "Zerze&#324;"
  ]
  node [
    id 953
    label "M&#322;ociny"
  ]
  node [
    id 954
    label "Branice"
  ]
  node [
    id 955
    label "Chojny"
  ]
  node [
    id 956
    label "Wad&#243;w"
  ]
  node [
    id 957
    label "jednostka_administracyjna"
  ]
  node [
    id 958
    label "Miedzeszyn"
  ]
  node [
    id 959
    label "Ok&#281;cie"
  ]
  node [
    id 960
    label "Lewin&#243;w"
  ]
  node [
    id 961
    label "Broch&#243;w"
  ]
  node [
    id 962
    label "Marysin"
  ]
  node [
    id 963
    label "Szack"
  ]
  node [
    id 964
    label "Wielopole"
  ]
  node [
    id 965
    label "Opor&#243;w"
  ]
  node [
    id 966
    label "Osobowice"
  ]
  node [
    id 967
    label "Lubiesz&#243;w"
  ]
  node [
    id 968
    label "&#379;erniki"
  ]
  node [
    id 969
    label "Powi&#347;le"
  ]
  node [
    id 970
    label "osadnictwo"
  ]
  node [
    id 971
    label "Wojn&#243;w"
  ]
  node [
    id 972
    label "Latycz&#243;w"
  ]
  node [
    id 973
    label "Kortowo"
  ]
  node [
    id 974
    label "Rej&#243;w"
  ]
  node [
    id 975
    label "Arsk"
  ]
  node [
    id 976
    label "&#321;agiewniki"
  ]
  node [
    id 977
    label "Azory"
  ]
  node [
    id 978
    label "Imielin"
  ]
  node [
    id 979
    label "Rataje"
  ]
  node [
    id 980
    label "Nadodrze"
  ]
  node [
    id 981
    label "Szczytniki"
  ]
  node [
    id 982
    label "Gr&#281;ba&#322;&#243;w"
  ]
  node [
    id 983
    label "dzielnica"
  ]
  node [
    id 984
    label "S&#281;polno"
  ]
  node [
    id 985
    label "G&#243;rczyn"
  ]
  node [
    id 986
    label "Szcz&#281;&#347;liwice"
  ]
  node [
    id 987
    label "Zalesie"
  ]
  node [
    id 988
    label "Ochock"
  ]
  node [
    id 989
    label "Gutkowo"
  ]
  node [
    id 990
    label "G&#322;uszyna"
  ]
  node [
    id 991
    label "Le&#347;nica"
  ]
  node [
    id 992
    label "Micha&#322;owo"
  ]
  node [
    id 993
    label "Jelonki"
  ]
  node [
    id 994
    label "Marysin_Wawerski"
  ]
  node [
    id 995
    label "Biskupin"
  ]
  node [
    id 996
    label "Goc&#322;aw"
  ]
  node [
    id 997
    label "Wawrzyszew"
  ]
  node [
    id 998
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 999
    label "zakres"
  ]
  node [
    id 1000
    label "huczek"
  ]
  node [
    id 1001
    label "wymiar"
  ]
  node [
    id 1002
    label "strefa"
  ]
  node [
    id 1003
    label "przestrze&#324;"
  ]
  node [
    id 1004
    label "powierzchnia"
  ]
  node [
    id 1005
    label "kolur"
  ]
  node [
    id 1006
    label "kula"
  ]
  node [
    id 1007
    label "sector"
  ]
  node [
    id 1008
    label "p&#243;&#322;sfera"
  ]
  node [
    id 1009
    label "p&#243;&#322;kula"
  ]
  node [
    id 1010
    label "class"
  ]
  node [
    id 1011
    label "przemys&#322;owo"
  ]
  node [
    id 1012
    label "masowy"
  ]
  node [
    id 1013
    label "obiekt_naturalny"
  ]
  node [
    id 1014
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 1015
    label "stw&#243;r"
  ]
  node [
    id 1016
    label "environment"
  ]
  node [
    id 1017
    label "erecting"
  ]
  node [
    id 1018
    label "biota"
  ]
  node [
    id 1019
    label "wszechstworzenie"
  ]
  node [
    id 1020
    label "fauna"
  ]
  node [
    id 1021
    label "ekosystem"
  ]
  node [
    id 1022
    label "teren"
  ]
  node [
    id 1023
    label "powstanie"
  ]
  node [
    id 1024
    label "mikrokosmos"
  ]
  node [
    id 1025
    label "pope&#322;nienie"
  ]
  node [
    id 1026
    label "work"
  ]
  node [
    id 1027
    label "wizerunek"
  ]
  node [
    id 1028
    label "organizm"
  ]
  node [
    id 1029
    label "Ziemia"
  ]
  node [
    id 1030
    label "potworzenie"
  ]
  node [
    id 1031
    label "woda"
  ]
  node [
    id 1032
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 1033
    label "dok&#322;adnie"
  ]
  node [
    id 1034
    label "precyzowanie"
  ]
  node [
    id 1035
    label "rzetelny"
  ]
  node [
    id 1036
    label "sprecyzowanie"
  ]
  node [
    id 1037
    label "miliamperomierz"
  ]
  node [
    id 1038
    label "precyzyjny"
  ]
  node [
    id 1039
    label "problem"
  ]
  node [
    id 1040
    label "idea"
  ]
  node [
    id 1041
    label "pomys&#322;"
  ]
  node [
    id 1042
    label "poj&#281;cie"
  ]
  node [
    id 1043
    label "uj&#281;cie"
  ]
  node [
    id 1044
    label "zamys&#322;"
  ]
  node [
    id 1045
    label "widzie&#263;"
  ]
  node [
    id 1046
    label "pojmowanie"
  ]
  node [
    id 1047
    label "figura_geometryczna"
  ]
  node [
    id 1048
    label "decentracja"
  ]
  node [
    id 1049
    label "anticipation"
  ]
  node [
    id 1050
    label "krajobraz"
  ]
  node [
    id 1051
    label "scene"
  ]
  node [
    id 1052
    label "patrze&#263;"
  ]
  node [
    id 1053
    label "obraz"
  ]
  node [
    id 1054
    label "prognoza"
  ]
  node [
    id 1055
    label "dystans"
  ]
  node [
    id 1056
    label "plan"
  ]
  node [
    id 1057
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 1058
    label "patrzenie"
  ]
  node [
    id 1059
    label "posta&#263;"
  ]
  node [
    id 1060
    label "expectation"
  ]
  node [
    id 1061
    label "przestrzenno&#347;&#263;"
  ]
  node [
    id 1062
    label "summer"
  ]
  node [
    id 1063
    label "blame"
  ]
  node [
    id 1064
    label "obci&#261;&#380;y&#263;"
  ]
  node [
    id 1065
    label "charge"
  ]
  node [
    id 1066
    label "oskar&#380;y&#263;"
  ]
  node [
    id 1067
    label "odpowiedzialno&#347;&#263;"
  ]
  node [
    id 1068
    label "obowi&#261;zek"
  ]
  node [
    id 1069
    label "zobowi&#261;za&#263;"
  ]
  node [
    id 1070
    label "load"
  ]
  node [
    id 1071
    label "doros&#322;y"
  ]
  node [
    id 1072
    label "wiele"
  ]
  node [
    id 1073
    label "dorodny"
  ]
  node [
    id 1074
    label "znaczny"
  ]
  node [
    id 1075
    label "du&#380;o"
  ]
  node [
    id 1076
    label "prawdziwy"
  ]
  node [
    id 1077
    label "niema&#322;o"
  ]
  node [
    id 1078
    label "wa&#380;ny"
  ]
  node [
    id 1079
    label "rozwini&#281;ty"
  ]
  node [
    id 1080
    label "baseball"
  ]
  node [
    id 1081
    label "czyn"
  ]
  node [
    id 1082
    label "error"
  ]
  node [
    id 1083
    label "pomyli&#263;_si&#281;"
  ]
  node [
    id 1084
    label "pomylenie_si&#281;"
  ]
  node [
    id 1085
    label "mniemanie"
  ]
  node [
    id 1086
    label "byk"
  ]
  node [
    id 1087
    label "nieprawid&#322;owo&#347;&#263;"
  ]
  node [
    id 1088
    label "rezultat"
  ]
  node [
    id 1089
    label "proceed"
  ]
  node [
    id 1090
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 1091
    label "bangla&#263;"
  ]
  node [
    id 1092
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 1093
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 1094
    label "run"
  ]
  node [
    id 1095
    label "p&#322;ywa&#263;"
  ]
  node [
    id 1096
    label "continue"
  ]
  node [
    id 1097
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 1098
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 1099
    label "przebiega&#263;"
  ]
  node [
    id 1100
    label "wk&#322;ada&#263;"
  ]
  node [
    id 1101
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 1102
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 1103
    label "para"
  ]
  node [
    id 1104
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 1105
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 1106
    label "krok"
  ]
  node [
    id 1107
    label "str&#243;j"
  ]
  node [
    id 1108
    label "bywa&#263;"
  ]
  node [
    id 1109
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 1110
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 1111
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 1112
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 1113
    label "dziama&#263;"
  ]
  node [
    id 1114
    label "stara&#263;_si&#281;"
  ]
  node [
    id 1115
    label "carry"
  ]
  node [
    id 1116
    label "wynik"
  ]
  node [
    id 1117
    label "wyj&#347;cie"
  ]
  node [
    id 1118
    label "spe&#322;nienie"
  ]
  node [
    id 1119
    label "cesarskie_ci&#281;cie"
  ]
  node [
    id 1120
    label "po&#322;o&#380;na"
  ]
  node [
    id 1121
    label "przestanie"
  ]
  node [
    id 1122
    label "marc&#243;wka"
  ]
  node [
    id 1123
    label "usuni&#281;cie"
  ]
  node [
    id 1124
    label "uniewa&#380;nienie"
  ]
  node [
    id 1125
    label "birth"
  ]
  node [
    id 1126
    label "wymy&#347;lenie"
  ]
  node [
    id 1127
    label "po&#322;&#243;g"
  ]
  node [
    id 1128
    label "szok_poporodowy"
  ]
  node [
    id 1129
    label "event"
  ]
  node [
    id 1130
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 1131
    label "spos&#243;b"
  ]
  node [
    id 1132
    label "rozwi&#261;zywanie_si&#281;"
  ]
  node [
    id 1133
    label "dula"
  ]
  node [
    id 1134
    label "specjalny"
  ]
  node [
    id 1135
    label "nieznaczny"
  ]
  node [
    id 1136
    label "suchy"
  ]
  node [
    id 1137
    label "pozamerytoryczny"
  ]
  node [
    id 1138
    label "ambitny"
  ]
  node [
    id 1139
    label "technicznie"
  ]
  node [
    id 1140
    label "byd&#322;o"
  ]
  node [
    id 1141
    label "zobo"
  ]
  node [
    id 1142
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 1143
    label "yakalo"
  ]
  node [
    id 1144
    label "dzo"
  ]
  node [
    id 1145
    label "mi&#281;dzybankowy"
  ]
  node [
    id 1146
    label "finansowo"
  ]
  node [
    id 1147
    label "fizyczny"
  ]
  node [
    id 1148
    label "pozamaterialny"
  ]
  node [
    id 1149
    label "materjalny"
  ]
  node [
    id 1150
    label "ten"
  ]
  node [
    id 1151
    label "rozprawa"
  ]
  node [
    id 1152
    label "paper"
  ]
  node [
    id 1153
    label "przygotowanie"
  ]
  node [
    id 1154
    label "tekst"
  ]
  node [
    id 1155
    label "nominate"
  ]
  node [
    id 1156
    label "zdecydowa&#263;"
  ]
  node [
    id 1157
    label "spowodowa&#263;"
  ]
  node [
    id 1158
    label "zrobi&#263;"
  ]
  node [
    id 1159
    label "situate"
  ]
  node [
    id 1160
    label "miejsce"
  ]
  node [
    id 1161
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 1162
    label "przysz&#322;o&#347;&#263;"
  ]
  node [
    id 1163
    label "kr&#243;tki"
  ]
  node [
    id 1164
    label "przysz&#322;y"
  ]
  node [
    id 1165
    label "&#347;rednioterminowo"
  ]
  node [
    id 1166
    label "przesta&#263;"
  ]
  node [
    id 1167
    label "cause"
  ]
  node [
    id 1168
    label "communicate"
  ]
  node [
    id 1169
    label "podkre&#347;lenie"
  ]
  node [
    id 1170
    label "wybranie"
  ]
  node [
    id 1171
    label "appointment"
  ]
  node [
    id 1172
    label "podanie"
  ]
  node [
    id 1173
    label "education"
  ]
  node [
    id 1174
    label "meaning"
  ]
  node [
    id 1175
    label "wskaz&#243;wka"
  ]
  node [
    id 1176
    label "pokazanie"
  ]
  node [
    id 1177
    label "wyja&#347;nienie"
  ]
  node [
    id 1178
    label "pas_planetoid"
  ]
  node [
    id 1179
    label "wsch&#243;d"
  ]
  node [
    id 1180
    label "Neogea"
  ]
  node [
    id 1181
    label "holarktyka"
  ]
  node [
    id 1182
    label "Rakowice"
  ]
  node [
    id 1183
    label "Kosowo"
  ]
  node [
    id 1184
    label "Syberia_Wschodnia"
  ]
  node [
    id 1185
    label "p&#243;&#322;noc"
  ]
  node [
    id 1186
    label "akrecja"
  ]
  node [
    id 1187
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 1188
    label "terytorium"
  ]
  node [
    id 1189
    label "antroposfera"
  ]
  node [
    id 1190
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 1191
    label "po&#322;udnie"
  ]
  node [
    id 1192
    label "zach&#243;d"
  ]
  node [
    id 1193
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 1194
    label "Olszanica"
  ]
  node [
    id 1195
    label "Syberia_Zachodnia"
  ]
  node [
    id 1196
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 1197
    label "Ruda_Pabianicka"
  ]
  node [
    id 1198
    label "Notogea"
  ]
  node [
    id 1199
    label "&#321;&#281;g"
  ]
  node [
    id 1200
    label "Antarktyka"
  ]
  node [
    id 1201
    label "Piotrowo"
  ]
  node [
    id 1202
    label "Zab&#322;ocie"
  ]
  node [
    id 1203
    label "Pow&#261;zki"
  ]
  node [
    id 1204
    label "Arktyka"
  ]
  node [
    id 1205
    label "Ludwin&#243;w"
  ]
  node [
    id 1206
    label "Zabu&#380;e"
  ]
  node [
    id 1207
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 1208
    label "Kresy_Zachodnie"
  ]
  node [
    id 1209
    label "wyodr&#281;bnia&#263;"
  ]
  node [
    id 1210
    label "digest"
  ]
  node [
    id 1211
    label "consumption"
  ]
  node [
    id 1212
    label "spowodowanie"
  ]
  node [
    id 1213
    label "zareagowanie"
  ]
  node [
    id 1214
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 1215
    label "movement"
  ]
  node [
    id 1216
    label "entertainment"
  ]
  node [
    id 1217
    label "zacz&#281;cie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 34
  ]
  edge [
    source 7
    target 35
  ]
  edge [
    source 7
    target 147
  ]
  edge [
    source 7
    target 148
  ]
  edge [
    source 7
    target 149
  ]
  edge [
    source 7
    target 150
  ]
  edge [
    source 7
    target 151
  ]
  edge [
    source 7
    target 152
  ]
  edge [
    source 7
    target 153
  ]
  edge [
    source 7
    target 154
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 7
    target 155
  ]
  edge [
    source 7
    target 156
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 157
  ]
  edge [
    source 8
    target 158
  ]
  edge [
    source 8
    target 159
  ]
  edge [
    source 8
    target 160
  ]
  edge [
    source 8
    target 161
  ]
  edge [
    source 8
    target 162
  ]
  edge [
    source 8
    target 163
  ]
  edge [
    source 8
    target 164
  ]
  edge [
    source 8
    target 165
  ]
  edge [
    source 8
    target 166
  ]
  edge [
    source 8
    target 167
  ]
  edge [
    source 8
    target 168
  ]
  edge [
    source 8
    target 169
  ]
  edge [
    source 8
    target 170
  ]
  edge [
    source 8
    target 171
  ]
  edge [
    source 8
    target 172
  ]
  edge [
    source 8
    target 173
  ]
  edge [
    source 8
    target 174
  ]
  edge [
    source 8
    target 175
  ]
  edge [
    source 8
    target 176
  ]
  edge [
    source 8
    target 177
  ]
  edge [
    source 8
    target 178
  ]
  edge [
    source 8
    target 179
  ]
  edge [
    source 8
    target 180
  ]
  edge [
    source 8
    target 181
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 182
  ]
  edge [
    source 9
    target 30
  ]
  edge [
    source 9
    target 183
  ]
  edge [
    source 9
    target 184
  ]
  edge [
    source 9
    target 44
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 43
  ]
  edge [
    source 10
    target 44
  ]
  edge [
    source 10
    target 185
  ]
  edge [
    source 10
    target 186
  ]
  edge [
    source 10
    target 187
  ]
  edge [
    source 10
    target 188
  ]
  edge [
    source 10
    target 189
  ]
  edge [
    source 10
    target 190
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 191
  ]
  edge [
    source 11
    target 192
  ]
  edge [
    source 11
    target 193
  ]
  edge [
    source 11
    target 16
  ]
  edge [
    source 11
    target 194
  ]
  edge [
    source 11
    target 38
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 20
  ]
  edge [
    source 12
    target 195
  ]
  edge [
    source 12
    target 196
  ]
  edge [
    source 12
    target 197
  ]
  edge [
    source 12
    target 198
  ]
  edge [
    source 12
    target 199
  ]
  edge [
    source 12
    target 200
  ]
  edge [
    source 12
    target 201
  ]
  edge [
    source 12
    target 202
  ]
  edge [
    source 12
    target 203
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 204
  ]
  edge [
    source 13
    target 205
  ]
  edge [
    source 13
    target 55
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 48
  ]
  edge [
    source 16
    target 206
  ]
  edge [
    source 16
    target 207
  ]
  edge [
    source 16
    target 208
  ]
  edge [
    source 16
    target 209
  ]
  edge [
    source 16
    target 210
  ]
  edge [
    source 16
    target 211
  ]
  edge [
    source 16
    target 212
  ]
  edge [
    source 16
    target 213
  ]
  edge [
    source 16
    target 214
  ]
  edge [
    source 16
    target 215
  ]
  edge [
    source 16
    target 216
  ]
  edge [
    source 16
    target 217
  ]
  edge [
    source 16
    target 218
  ]
  edge [
    source 16
    target 219
  ]
  edge [
    source 16
    target 220
  ]
  edge [
    source 16
    target 221
  ]
  edge [
    source 16
    target 222
  ]
  edge [
    source 16
    target 223
  ]
  edge [
    source 16
    target 224
  ]
  edge [
    source 16
    target 225
  ]
  edge [
    source 16
    target 226
  ]
  edge [
    source 16
    target 227
  ]
  edge [
    source 16
    target 228
  ]
  edge [
    source 16
    target 229
  ]
  edge [
    source 16
    target 230
  ]
  edge [
    source 16
    target 231
  ]
  edge [
    source 16
    target 232
  ]
  edge [
    source 16
    target 233
  ]
  edge [
    source 16
    target 234
  ]
  edge [
    source 16
    target 235
  ]
  edge [
    source 16
    target 236
  ]
  edge [
    source 16
    target 237
  ]
  edge [
    source 16
    target 238
  ]
  edge [
    source 16
    target 239
  ]
  edge [
    source 16
    target 240
  ]
  edge [
    source 16
    target 241
  ]
  edge [
    source 16
    target 242
  ]
  edge [
    source 16
    target 243
  ]
  edge [
    source 16
    target 244
  ]
  edge [
    source 16
    target 245
  ]
  edge [
    source 16
    target 246
  ]
  edge [
    source 16
    target 247
  ]
  edge [
    source 16
    target 248
  ]
  edge [
    source 16
    target 249
  ]
  edge [
    source 16
    target 250
  ]
  edge [
    source 16
    target 251
  ]
  edge [
    source 16
    target 252
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 253
  ]
  edge [
    source 17
    target 254
  ]
  edge [
    source 17
    target 255
  ]
  edge [
    source 17
    target 256
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 257
  ]
  edge [
    source 18
    target 258
  ]
  edge [
    source 18
    target 259
  ]
  edge [
    source 18
    target 260
  ]
  edge [
    source 18
    target 261
  ]
  edge [
    source 18
    target 262
  ]
  edge [
    source 18
    target 263
  ]
  edge [
    source 18
    target 264
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 26
  ]
  edge [
    source 19
    target 27
  ]
  edge [
    source 19
    target 265
  ]
  edge [
    source 19
    target 266
  ]
  edge [
    source 19
    target 267
  ]
  edge [
    source 19
    target 75
  ]
  edge [
    source 19
    target 268
  ]
  edge [
    source 19
    target 269
  ]
  edge [
    source 19
    target 270
  ]
  edge [
    source 19
    target 271
  ]
  edge [
    source 19
    target 272
  ]
  edge [
    source 19
    target 273
  ]
  edge [
    source 19
    target 274
  ]
  edge [
    source 19
    target 275
  ]
  edge [
    source 19
    target 276
  ]
  edge [
    source 19
    target 277
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 278
  ]
  edge [
    source 20
    target 279
  ]
  edge [
    source 20
    target 280
  ]
  edge [
    source 20
    target 281
  ]
  edge [
    source 20
    target 282
  ]
  edge [
    source 20
    target 283
  ]
  edge [
    source 20
    target 284
  ]
  edge [
    source 20
    target 285
  ]
  edge [
    source 20
    target 286
  ]
  edge [
    source 20
    target 287
  ]
  edge [
    source 20
    target 288
  ]
  edge [
    source 20
    target 289
  ]
  edge [
    source 20
    target 290
  ]
  edge [
    source 20
    target 291
  ]
  edge [
    source 20
    target 292
  ]
  edge [
    source 20
    target 293
  ]
  edge [
    source 20
    target 294
  ]
  edge [
    source 20
    target 295
  ]
  edge [
    source 20
    target 296
  ]
  edge [
    source 20
    target 297
  ]
  edge [
    source 20
    target 298
  ]
  edge [
    source 20
    target 299
  ]
  edge [
    source 20
    target 300
  ]
  edge [
    source 20
    target 301
  ]
  edge [
    source 20
    target 302
  ]
  edge [
    source 20
    target 303
  ]
  edge [
    source 20
    target 304
  ]
  edge [
    source 20
    target 305
  ]
  edge [
    source 20
    target 306
  ]
  edge [
    source 20
    target 307
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 308
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 309
  ]
  edge [
    source 22
    target 310
  ]
  edge [
    source 22
    target 311
  ]
  edge [
    source 22
    target 312
  ]
  edge [
    source 22
    target 313
  ]
  edge [
    source 22
    target 314
  ]
  edge [
    source 22
    target 315
  ]
  edge [
    source 22
    target 316
  ]
  edge [
    source 22
    target 317
  ]
  edge [
    source 22
    target 318
  ]
  edge [
    source 22
    target 319
  ]
  edge [
    source 22
    target 320
  ]
  edge [
    source 22
    target 321
  ]
  edge [
    source 22
    target 322
  ]
  edge [
    source 22
    target 323
  ]
  edge [
    source 22
    target 324
  ]
  edge [
    source 22
    target 325
  ]
  edge [
    source 22
    target 326
  ]
  edge [
    source 22
    target 327
  ]
  edge [
    source 22
    target 328
  ]
  edge [
    source 22
    target 329
  ]
  edge [
    source 22
    target 330
  ]
  edge [
    source 22
    target 331
  ]
  edge [
    source 22
    target 332
  ]
  edge [
    source 22
    target 333
  ]
  edge [
    source 22
    target 334
  ]
  edge [
    source 22
    target 335
  ]
  edge [
    source 22
    target 336
  ]
  edge [
    source 22
    target 337
  ]
  edge [
    source 22
    target 338
  ]
  edge [
    source 22
    target 339
  ]
  edge [
    source 22
    target 340
  ]
  edge [
    source 22
    target 341
  ]
  edge [
    source 22
    target 342
  ]
  edge [
    source 22
    target 343
  ]
  edge [
    source 22
    target 344
  ]
  edge [
    source 22
    target 345
  ]
  edge [
    source 22
    target 346
  ]
  edge [
    source 22
    target 347
  ]
  edge [
    source 22
    target 348
  ]
  edge [
    source 22
    target 349
  ]
  edge [
    source 22
    target 350
  ]
  edge [
    source 22
    target 351
  ]
  edge [
    source 22
    target 352
  ]
  edge [
    source 22
    target 353
  ]
  edge [
    source 22
    target 354
  ]
  edge [
    source 22
    target 355
  ]
  edge [
    source 22
    target 356
  ]
  edge [
    source 22
    target 357
  ]
  edge [
    source 22
    target 358
  ]
  edge [
    source 22
    target 359
  ]
  edge [
    source 22
    target 360
  ]
  edge [
    source 22
    target 361
  ]
  edge [
    source 22
    target 362
  ]
  edge [
    source 22
    target 363
  ]
  edge [
    source 22
    target 364
  ]
  edge [
    source 22
    target 365
  ]
  edge [
    source 22
    target 366
  ]
  edge [
    source 22
    target 367
  ]
  edge [
    source 22
    target 368
  ]
  edge [
    source 22
    target 369
  ]
  edge [
    source 22
    target 370
  ]
  edge [
    source 22
    target 371
  ]
  edge [
    source 22
    target 372
  ]
  edge [
    source 22
    target 373
  ]
  edge [
    source 22
    target 374
  ]
  edge [
    source 22
    target 375
  ]
  edge [
    source 22
    target 376
  ]
  edge [
    source 22
    target 377
  ]
  edge [
    source 22
    target 378
  ]
  edge [
    source 22
    target 379
  ]
  edge [
    source 22
    target 380
  ]
  edge [
    source 22
    target 381
  ]
  edge [
    source 22
    target 382
  ]
  edge [
    source 22
    target 383
  ]
  edge [
    source 22
    target 384
  ]
  edge [
    source 22
    target 385
  ]
  edge [
    source 22
    target 386
  ]
  edge [
    source 22
    target 387
  ]
  edge [
    source 22
    target 388
  ]
  edge [
    source 22
    target 389
  ]
  edge [
    source 22
    target 390
  ]
  edge [
    source 22
    target 391
  ]
  edge [
    source 22
    target 392
  ]
  edge [
    source 22
    target 393
  ]
  edge [
    source 22
    target 394
  ]
  edge [
    source 22
    target 395
  ]
  edge [
    source 22
    target 396
  ]
  edge [
    source 22
    target 397
  ]
  edge [
    source 22
    target 398
  ]
  edge [
    source 22
    target 399
  ]
  edge [
    source 22
    target 400
  ]
  edge [
    source 22
    target 401
  ]
  edge [
    source 22
    target 402
  ]
  edge [
    source 22
    target 403
  ]
  edge [
    source 22
    target 404
  ]
  edge [
    source 22
    target 405
  ]
  edge [
    source 22
    target 406
  ]
  edge [
    source 22
    target 407
  ]
  edge [
    source 22
    target 408
  ]
  edge [
    source 22
    target 409
  ]
  edge [
    source 22
    target 410
  ]
  edge [
    source 22
    target 411
  ]
  edge [
    source 22
    target 412
  ]
  edge [
    source 22
    target 413
  ]
  edge [
    source 22
    target 414
  ]
  edge [
    source 22
    target 415
  ]
  edge [
    source 22
    target 416
  ]
  edge [
    source 22
    target 417
  ]
  edge [
    source 22
    target 418
  ]
  edge [
    source 22
    target 419
  ]
  edge [
    source 22
    target 420
  ]
  edge [
    source 22
    target 421
  ]
  edge [
    source 22
    target 422
  ]
  edge [
    source 22
    target 423
  ]
  edge [
    source 22
    target 424
  ]
  edge [
    source 22
    target 425
  ]
  edge [
    source 22
    target 426
  ]
  edge [
    source 22
    target 427
  ]
  edge [
    source 22
    target 428
  ]
  edge [
    source 22
    target 429
  ]
  edge [
    source 22
    target 430
  ]
  edge [
    source 22
    target 431
  ]
  edge [
    source 22
    target 432
  ]
  edge [
    source 22
    target 433
  ]
  edge [
    source 22
    target 434
  ]
  edge [
    source 22
    target 435
  ]
  edge [
    source 22
    target 436
  ]
  edge [
    source 22
    target 437
  ]
  edge [
    source 22
    target 438
  ]
  edge [
    source 22
    target 439
  ]
  edge [
    source 22
    target 440
  ]
  edge [
    source 22
    target 441
  ]
  edge [
    source 22
    target 442
  ]
  edge [
    source 22
    target 443
  ]
  edge [
    source 22
    target 444
  ]
  edge [
    source 22
    target 445
  ]
  edge [
    source 22
    target 446
  ]
  edge [
    source 22
    target 447
  ]
  edge [
    source 22
    target 448
  ]
  edge [
    source 22
    target 449
  ]
  edge [
    source 22
    target 450
  ]
  edge [
    source 22
    target 451
  ]
  edge [
    source 22
    target 452
  ]
  edge [
    source 22
    target 453
  ]
  edge [
    source 22
    target 454
  ]
  edge [
    source 22
    target 455
  ]
  edge [
    source 22
    target 456
  ]
  edge [
    source 22
    target 457
  ]
  edge [
    source 22
    target 458
  ]
  edge [
    source 22
    target 459
  ]
  edge [
    source 22
    target 460
  ]
  edge [
    source 22
    target 461
  ]
  edge [
    source 22
    target 462
  ]
  edge [
    source 22
    target 463
  ]
  edge [
    source 22
    target 464
  ]
  edge [
    source 22
    target 465
  ]
  edge [
    source 22
    target 466
  ]
  edge [
    source 22
    target 467
  ]
  edge [
    source 22
    target 468
  ]
  edge [
    source 22
    target 469
  ]
  edge [
    source 22
    target 470
  ]
  edge [
    source 22
    target 471
  ]
  edge [
    source 22
    target 472
  ]
  edge [
    source 22
    target 473
  ]
  edge [
    source 22
    target 474
  ]
  edge [
    source 22
    target 475
  ]
  edge [
    source 22
    target 476
  ]
  edge [
    source 22
    target 477
  ]
  edge [
    source 22
    target 478
  ]
  edge [
    source 22
    target 479
  ]
  edge [
    source 22
    target 480
  ]
  edge [
    source 22
    target 481
  ]
  edge [
    source 22
    target 482
  ]
  edge [
    source 22
    target 483
  ]
  edge [
    source 22
    target 484
  ]
  edge [
    source 22
    target 485
  ]
  edge [
    source 22
    target 486
  ]
  edge [
    source 22
    target 487
  ]
  edge [
    source 22
    target 488
  ]
  edge [
    source 22
    target 489
  ]
  edge [
    source 22
    target 490
  ]
  edge [
    source 22
    target 491
  ]
  edge [
    source 22
    target 492
  ]
  edge [
    source 22
    target 493
  ]
  edge [
    source 22
    target 494
  ]
  edge [
    source 22
    target 495
  ]
  edge [
    source 22
    target 496
  ]
  edge [
    source 22
    target 497
  ]
  edge [
    source 22
    target 498
  ]
  edge [
    source 22
    target 499
  ]
  edge [
    source 22
    target 500
  ]
  edge [
    source 22
    target 501
  ]
  edge [
    source 22
    target 502
  ]
  edge [
    source 22
    target 503
  ]
  edge [
    source 22
    target 504
  ]
  edge [
    source 22
    target 505
  ]
  edge [
    source 22
    target 506
  ]
  edge [
    source 22
    target 507
  ]
  edge [
    source 22
    target 508
  ]
  edge [
    source 22
    target 509
  ]
  edge [
    source 22
    target 510
  ]
  edge [
    source 22
    target 511
  ]
  edge [
    source 22
    target 512
  ]
  edge [
    source 22
    target 513
  ]
  edge [
    source 22
    target 514
  ]
  edge [
    source 22
    target 515
  ]
  edge [
    source 22
    target 516
  ]
  edge [
    source 22
    target 517
  ]
  edge [
    source 22
    target 518
  ]
  edge [
    source 22
    target 519
  ]
  edge [
    source 22
    target 520
  ]
  edge [
    source 22
    target 521
  ]
  edge [
    source 22
    target 522
  ]
  edge [
    source 22
    target 523
  ]
  edge [
    source 22
    target 524
  ]
  edge [
    source 22
    target 525
  ]
  edge [
    source 22
    target 526
  ]
  edge [
    source 22
    target 527
  ]
  edge [
    source 22
    target 528
  ]
  edge [
    source 22
    target 529
  ]
  edge [
    source 22
    target 530
  ]
  edge [
    source 22
    target 531
  ]
  edge [
    source 22
    target 532
  ]
  edge [
    source 22
    target 533
  ]
  edge [
    source 22
    target 534
  ]
  edge [
    source 22
    target 535
  ]
  edge [
    source 22
    target 536
  ]
  edge [
    source 22
    target 537
  ]
  edge [
    source 22
    target 538
  ]
  edge [
    source 22
    target 539
  ]
  edge [
    source 22
    target 540
  ]
  edge [
    source 22
    target 541
  ]
  edge [
    source 22
    target 542
  ]
  edge [
    source 22
    target 543
  ]
  edge [
    source 22
    target 544
  ]
  edge [
    source 22
    target 545
  ]
  edge [
    source 22
    target 546
  ]
  edge [
    source 22
    target 547
  ]
  edge [
    source 22
    target 548
  ]
  edge [
    source 22
    target 549
  ]
  edge [
    source 22
    target 550
  ]
  edge [
    source 22
    target 551
  ]
  edge [
    source 22
    target 552
  ]
  edge [
    source 22
    target 553
  ]
  edge [
    source 22
    target 554
  ]
  edge [
    source 22
    target 555
  ]
  edge [
    source 22
    target 556
  ]
  edge [
    source 22
    target 557
  ]
  edge [
    source 22
    target 558
  ]
  edge [
    source 22
    target 559
  ]
  edge [
    source 22
    target 560
  ]
  edge [
    source 22
    target 561
  ]
  edge [
    source 22
    target 562
  ]
  edge [
    source 22
    target 563
  ]
  edge [
    source 22
    target 564
  ]
  edge [
    source 22
    target 565
  ]
  edge [
    source 22
    target 566
  ]
  edge [
    source 22
    target 567
  ]
  edge [
    source 22
    target 568
  ]
  edge [
    source 22
    target 569
  ]
  edge [
    source 22
    target 570
  ]
  edge [
    source 22
    target 571
  ]
  edge [
    source 22
    target 572
  ]
  edge [
    source 22
    target 573
  ]
  edge [
    source 22
    target 574
  ]
  edge [
    source 22
    target 575
  ]
  edge [
    source 22
    target 576
  ]
  edge [
    source 22
    target 577
  ]
  edge [
    source 22
    target 578
  ]
  edge [
    source 22
    target 579
  ]
  edge [
    source 22
    target 580
  ]
  edge [
    source 22
    target 581
  ]
  edge [
    source 22
    target 582
  ]
  edge [
    source 22
    target 583
  ]
  edge [
    source 22
    target 584
  ]
  edge [
    source 22
    target 585
  ]
  edge [
    source 22
    target 586
  ]
  edge [
    source 22
    target 587
  ]
  edge [
    source 22
    target 588
  ]
  edge [
    source 22
    target 589
  ]
  edge [
    source 22
    target 590
  ]
  edge [
    source 22
    target 591
  ]
  edge [
    source 22
    target 592
  ]
  edge [
    source 22
    target 593
  ]
  edge [
    source 22
    target 594
  ]
  edge [
    source 22
    target 595
  ]
  edge [
    source 22
    target 596
  ]
  edge [
    source 22
    target 597
  ]
  edge [
    source 22
    target 598
  ]
  edge [
    source 22
    target 599
  ]
  edge [
    source 22
    target 600
  ]
  edge [
    source 22
    target 601
  ]
  edge [
    source 22
    target 602
  ]
  edge [
    source 22
    target 603
  ]
  edge [
    source 22
    target 604
  ]
  edge [
    source 22
    target 605
  ]
  edge [
    source 22
    target 606
  ]
  edge [
    source 22
    target 607
  ]
  edge [
    source 22
    target 608
  ]
  edge [
    source 22
    target 609
  ]
  edge [
    source 22
    target 610
  ]
  edge [
    source 22
    target 611
  ]
  edge [
    source 22
    target 612
  ]
  edge [
    source 22
    target 613
  ]
  edge [
    source 22
    target 614
  ]
  edge [
    source 22
    target 615
  ]
  edge [
    source 22
    target 616
  ]
  edge [
    source 22
    target 617
  ]
  edge [
    source 22
    target 618
  ]
  edge [
    source 22
    target 619
  ]
  edge [
    source 22
    target 620
  ]
  edge [
    source 22
    target 621
  ]
  edge [
    source 22
    target 622
  ]
  edge [
    source 22
    target 623
  ]
  edge [
    source 22
    target 624
  ]
  edge [
    source 22
    target 625
  ]
  edge [
    source 22
    target 626
  ]
  edge [
    source 22
    target 627
  ]
  edge [
    source 22
    target 628
  ]
  edge [
    source 22
    target 629
  ]
  edge [
    source 22
    target 630
  ]
  edge [
    source 22
    target 631
  ]
  edge [
    source 22
    target 632
  ]
  edge [
    source 22
    target 633
  ]
  edge [
    source 22
    target 634
  ]
  edge [
    source 22
    target 635
  ]
  edge [
    source 22
    target 636
  ]
  edge [
    source 22
    target 637
  ]
  edge [
    source 22
    target 638
  ]
  edge [
    source 22
    target 639
  ]
  edge [
    source 22
    target 640
  ]
  edge [
    source 22
    target 641
  ]
  edge [
    source 22
    target 642
  ]
  edge [
    source 22
    target 643
  ]
  edge [
    source 22
    target 644
  ]
  edge [
    source 22
    target 645
  ]
  edge [
    source 22
    target 646
  ]
  edge [
    source 22
    target 647
  ]
  edge [
    source 22
    target 648
  ]
  edge [
    source 22
    target 649
  ]
  edge [
    source 22
    target 650
  ]
  edge [
    source 22
    target 651
  ]
  edge [
    source 22
    target 652
  ]
  edge [
    source 22
    target 653
  ]
  edge [
    source 22
    target 654
  ]
  edge [
    source 22
    target 655
  ]
  edge [
    source 22
    target 656
  ]
  edge [
    source 22
    target 657
  ]
  edge [
    source 22
    target 658
  ]
  edge [
    source 22
    target 659
  ]
  edge [
    source 22
    target 660
  ]
  edge [
    source 22
    target 661
  ]
  edge [
    source 22
    target 662
  ]
  edge [
    source 22
    target 663
  ]
  edge [
    source 22
    target 664
  ]
  edge [
    source 22
    target 665
  ]
  edge [
    source 22
    target 666
  ]
  edge [
    source 22
    target 667
  ]
  edge [
    source 22
    target 668
  ]
  edge [
    source 22
    target 669
  ]
  edge [
    source 22
    target 670
  ]
  edge [
    source 22
    target 671
  ]
  edge [
    source 22
    target 672
  ]
  edge [
    source 22
    target 673
  ]
  edge [
    source 22
    target 674
  ]
  edge [
    source 22
    target 675
  ]
  edge [
    source 22
    target 676
  ]
  edge [
    source 22
    target 677
  ]
  edge [
    source 22
    target 678
  ]
  edge [
    source 22
    target 679
  ]
  edge [
    source 22
    target 680
  ]
  edge [
    source 22
    target 681
  ]
  edge [
    source 22
    target 682
  ]
  edge [
    source 22
    target 683
  ]
  edge [
    source 22
    target 684
  ]
  edge [
    source 22
    target 685
  ]
  edge [
    source 22
    target 686
  ]
  edge [
    source 22
    target 687
  ]
  edge [
    source 22
    target 688
  ]
  edge [
    source 22
    target 689
  ]
  edge [
    source 22
    target 690
  ]
  edge [
    source 22
    target 691
  ]
  edge [
    source 22
    target 692
  ]
  edge [
    source 22
    target 693
  ]
  edge [
    source 22
    target 694
  ]
  edge [
    source 22
    target 695
  ]
  edge [
    source 22
    target 696
  ]
  edge [
    source 22
    target 697
  ]
  edge [
    source 22
    target 698
  ]
  edge [
    source 22
    target 699
  ]
  edge [
    source 22
    target 700
  ]
  edge [
    source 22
    target 701
  ]
  edge [
    source 22
    target 702
  ]
  edge [
    source 22
    target 703
  ]
  edge [
    source 22
    target 704
  ]
  edge [
    source 22
    target 705
  ]
  edge [
    source 22
    target 706
  ]
  edge [
    source 22
    target 707
  ]
  edge [
    source 22
    target 708
  ]
  edge [
    source 22
    target 709
  ]
  edge [
    source 22
    target 710
  ]
  edge [
    source 22
    target 711
  ]
  edge [
    source 22
    target 712
  ]
  edge [
    source 22
    target 713
  ]
  edge [
    source 22
    target 714
  ]
  edge [
    source 22
    target 715
  ]
  edge [
    source 22
    target 716
  ]
  edge [
    source 22
    target 717
  ]
  edge [
    source 22
    target 718
  ]
  edge [
    source 22
    target 719
  ]
  edge [
    source 22
    target 720
  ]
  edge [
    source 22
    target 721
  ]
  edge [
    source 22
    target 722
  ]
  edge [
    source 22
    target 723
  ]
  edge [
    source 22
    target 724
  ]
  edge [
    source 22
    target 725
  ]
  edge [
    source 22
    target 726
  ]
  edge [
    source 22
    target 727
  ]
  edge [
    source 22
    target 728
  ]
  edge [
    source 22
    target 729
  ]
  edge [
    source 22
    target 730
  ]
  edge [
    source 22
    target 731
  ]
  edge [
    source 22
    target 732
  ]
  edge [
    source 22
    target 733
  ]
  edge [
    source 22
    target 734
  ]
  edge [
    source 22
    target 735
  ]
  edge [
    source 22
    target 736
  ]
  edge [
    source 22
    target 737
  ]
  edge [
    source 22
    target 738
  ]
  edge [
    source 22
    target 739
  ]
  edge [
    source 22
    target 740
  ]
  edge [
    source 22
    target 741
  ]
  edge [
    source 22
    target 742
  ]
  edge [
    source 22
    target 743
  ]
  edge [
    source 22
    target 744
  ]
  edge [
    source 22
    target 745
  ]
  edge [
    source 22
    target 746
  ]
  edge [
    source 22
    target 747
  ]
  edge [
    source 22
    target 748
  ]
  edge [
    source 22
    target 749
  ]
  edge [
    source 22
    target 750
  ]
  edge [
    source 22
    target 751
  ]
  edge [
    source 22
    target 752
  ]
  edge [
    source 22
    target 753
  ]
  edge [
    source 22
    target 754
  ]
  edge [
    source 22
    target 755
  ]
  edge [
    source 22
    target 756
  ]
  edge [
    source 22
    target 757
  ]
  edge [
    source 22
    target 758
  ]
  edge [
    source 22
    target 759
  ]
  edge [
    source 22
    target 760
  ]
  edge [
    source 22
    target 761
  ]
  edge [
    source 22
    target 762
  ]
  edge [
    source 22
    target 763
  ]
  edge [
    source 22
    target 764
  ]
  edge [
    source 22
    target 765
  ]
  edge [
    source 22
    target 766
  ]
  edge [
    source 22
    target 767
  ]
  edge [
    source 22
    target 768
  ]
  edge [
    source 22
    target 769
  ]
  edge [
    source 22
    target 770
  ]
  edge [
    source 22
    target 771
  ]
  edge [
    source 22
    target 772
  ]
  edge [
    source 22
    target 773
  ]
  edge [
    source 22
    target 774
  ]
  edge [
    source 22
    target 775
  ]
  edge [
    source 22
    target 776
  ]
  edge [
    source 22
    target 777
  ]
  edge [
    source 22
    target 778
  ]
  edge [
    source 22
    target 779
  ]
  edge [
    source 22
    target 780
  ]
  edge [
    source 22
    target 781
  ]
  edge [
    source 22
    target 782
  ]
  edge [
    source 22
    target 783
  ]
  edge [
    source 22
    target 784
  ]
  edge [
    source 22
    target 785
  ]
  edge [
    source 22
    target 786
  ]
  edge [
    source 22
    target 787
  ]
  edge [
    source 22
    target 788
  ]
  edge [
    source 22
    target 789
  ]
  edge [
    source 22
    target 790
  ]
  edge [
    source 22
    target 791
  ]
  edge [
    source 22
    target 792
  ]
  edge [
    source 22
    target 793
  ]
  edge [
    source 22
    target 794
  ]
  edge [
    source 22
    target 795
  ]
  edge [
    source 22
    target 796
  ]
  edge [
    source 22
    target 797
  ]
  edge [
    source 22
    target 798
  ]
  edge [
    source 22
    target 799
  ]
  edge [
    source 22
    target 800
  ]
  edge [
    source 22
    target 801
  ]
  edge [
    source 22
    target 802
  ]
  edge [
    source 22
    target 803
  ]
  edge [
    source 22
    target 804
  ]
  edge [
    source 22
    target 805
  ]
  edge [
    source 22
    target 806
  ]
  edge [
    source 22
    target 807
  ]
  edge [
    source 22
    target 808
  ]
  edge [
    source 22
    target 809
  ]
  edge [
    source 22
    target 810
  ]
  edge [
    source 22
    target 811
  ]
  edge [
    source 22
    target 812
  ]
  edge [
    source 22
    target 813
  ]
  edge [
    source 22
    target 814
  ]
  edge [
    source 22
    target 815
  ]
  edge [
    source 22
    target 816
  ]
  edge [
    source 22
    target 817
  ]
  edge [
    source 22
    target 818
  ]
  edge [
    source 22
    target 819
  ]
  edge [
    source 22
    target 820
  ]
  edge [
    source 22
    target 821
  ]
  edge [
    source 22
    target 822
  ]
  edge [
    source 22
    target 823
  ]
  edge [
    source 22
    target 824
  ]
  edge [
    source 22
    target 825
  ]
  edge [
    source 22
    target 826
  ]
  edge [
    source 22
    target 827
  ]
  edge [
    source 22
    target 828
  ]
  edge [
    source 22
    target 829
  ]
  edge [
    source 22
    target 830
  ]
  edge [
    source 22
    target 831
  ]
  edge [
    source 22
    target 832
  ]
  edge [
    source 22
    target 833
  ]
  edge [
    source 22
    target 834
  ]
  edge [
    source 22
    target 835
  ]
  edge [
    source 22
    target 836
  ]
  edge [
    source 22
    target 837
  ]
  edge [
    source 22
    target 838
  ]
  edge [
    source 22
    target 839
  ]
  edge [
    source 22
    target 840
  ]
  edge [
    source 22
    target 841
  ]
  edge [
    source 22
    target 842
  ]
  edge [
    source 22
    target 843
  ]
  edge [
    source 22
    target 844
  ]
  edge [
    source 22
    target 845
  ]
  edge [
    source 22
    target 846
  ]
  edge [
    source 22
    target 847
  ]
  edge [
    source 22
    target 848
  ]
  edge [
    source 22
    target 849
  ]
  edge [
    source 22
    target 850
  ]
  edge [
    source 22
    target 851
  ]
  edge [
    source 22
    target 852
  ]
  edge [
    source 22
    target 853
  ]
  edge [
    source 22
    target 854
  ]
  edge [
    source 22
    target 855
  ]
  edge [
    source 22
    target 856
  ]
  edge [
    source 22
    target 857
  ]
  edge [
    source 22
    target 858
  ]
  edge [
    source 22
    target 859
  ]
  edge [
    source 22
    target 860
  ]
  edge [
    source 22
    target 861
  ]
  edge [
    source 22
    target 862
  ]
  edge [
    source 22
    target 863
  ]
  edge [
    source 22
    target 864
  ]
  edge [
    source 22
    target 865
  ]
  edge [
    source 22
    target 866
  ]
  edge [
    source 22
    target 867
  ]
  edge [
    source 22
    target 868
  ]
  edge [
    source 22
    target 869
  ]
  edge [
    source 22
    target 870
  ]
  edge [
    source 22
    target 871
  ]
  edge [
    source 22
    target 872
  ]
  edge [
    source 22
    target 873
  ]
  edge [
    source 22
    target 874
  ]
  edge [
    source 22
    target 875
  ]
  edge [
    source 22
    target 876
  ]
  edge [
    source 22
    target 877
  ]
  edge [
    source 22
    target 878
  ]
  edge [
    source 22
    target 879
  ]
  edge [
    source 22
    target 880
  ]
  edge [
    source 22
    target 881
  ]
  edge [
    source 22
    target 882
  ]
  edge [
    source 22
    target 883
  ]
  edge [
    source 22
    target 884
  ]
  edge [
    source 22
    target 885
  ]
  edge [
    source 22
    target 886
  ]
  edge [
    source 22
    target 887
  ]
  edge [
    source 22
    target 888
  ]
  edge [
    source 22
    target 889
  ]
  edge [
    source 22
    target 890
  ]
  edge [
    source 22
    target 891
  ]
  edge [
    source 22
    target 892
  ]
  edge [
    source 22
    target 893
  ]
  edge [
    source 22
    target 894
  ]
  edge [
    source 22
    target 895
  ]
  edge [
    source 22
    target 896
  ]
  edge [
    source 22
    target 897
  ]
  edge [
    source 22
    target 898
  ]
  edge [
    source 22
    target 899
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 900
  ]
  edge [
    source 23
    target 901
  ]
  edge [
    source 23
    target 287
  ]
  edge [
    source 23
    target 902
  ]
  edge [
    source 23
    target 903
  ]
  edge [
    source 23
    target 904
  ]
  edge [
    source 23
    target 905
  ]
  edge [
    source 23
    target 906
  ]
  edge [
    source 23
    target 907
  ]
  edge [
    source 23
    target 294
  ]
  edge [
    source 23
    target 908
  ]
  edge [
    source 23
    target 909
  ]
  edge [
    source 23
    target 910
  ]
  edge [
    source 23
    target 277
  ]
  edge [
    source 23
    target 911
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 912
  ]
  edge [
    source 24
    target 913
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 914
  ]
  edge [
    source 25
    target 915
  ]
  edge [
    source 25
    target 444
  ]
  edge [
    source 25
    target 916
  ]
  edge [
    source 25
    target 917
  ]
  edge [
    source 25
    target 918
  ]
  edge [
    source 25
    target 919
  ]
  edge [
    source 25
    target 920
  ]
  edge [
    source 25
    target 921
  ]
  edge [
    source 25
    target 922
  ]
  edge [
    source 25
    target 923
  ]
  edge [
    source 25
    target 924
  ]
  edge [
    source 25
    target 925
  ]
  edge [
    source 25
    target 926
  ]
  edge [
    source 25
    target 927
  ]
  edge [
    source 25
    target 928
  ]
  edge [
    source 25
    target 929
  ]
  edge [
    source 25
    target 930
  ]
  edge [
    source 25
    target 931
  ]
  edge [
    source 25
    target 932
  ]
  edge [
    source 25
    target 933
  ]
  edge [
    source 25
    target 934
  ]
  edge [
    source 25
    target 935
  ]
  edge [
    source 25
    target 936
  ]
  edge [
    source 25
    target 937
  ]
  edge [
    source 25
    target 938
  ]
  edge [
    source 25
    target 939
  ]
  edge [
    source 25
    target 940
  ]
  edge [
    source 25
    target 658
  ]
  edge [
    source 25
    target 941
  ]
  edge [
    source 25
    target 942
  ]
  edge [
    source 25
    target 943
  ]
  edge [
    source 25
    target 944
  ]
  edge [
    source 25
    target 945
  ]
  edge [
    source 25
    target 946
  ]
  edge [
    source 25
    target 947
  ]
  edge [
    source 25
    target 948
  ]
  edge [
    source 25
    target 949
  ]
  edge [
    source 25
    target 950
  ]
  edge [
    source 25
    target 951
  ]
  edge [
    source 25
    target 952
  ]
  edge [
    source 25
    target 953
  ]
  edge [
    source 25
    target 954
  ]
  edge [
    source 25
    target 955
  ]
  edge [
    source 25
    target 956
  ]
  edge [
    source 25
    target 957
  ]
  edge [
    source 25
    target 958
  ]
  edge [
    source 25
    target 959
  ]
  edge [
    source 25
    target 960
  ]
  edge [
    source 25
    target 961
  ]
  edge [
    source 25
    target 962
  ]
  edge [
    source 25
    target 963
  ]
  edge [
    source 25
    target 964
  ]
  edge [
    source 25
    target 965
  ]
  edge [
    source 25
    target 966
  ]
  edge [
    source 25
    target 967
  ]
  edge [
    source 25
    target 968
  ]
  edge [
    source 25
    target 969
  ]
  edge [
    source 25
    target 970
  ]
  edge [
    source 25
    target 971
  ]
  edge [
    source 25
    target 972
  ]
  edge [
    source 25
    target 973
  ]
  edge [
    source 25
    target 974
  ]
  edge [
    source 25
    target 975
  ]
  edge [
    source 25
    target 976
  ]
  edge [
    source 25
    target 977
  ]
  edge [
    source 25
    target 978
  ]
  edge [
    source 25
    target 979
  ]
  edge [
    source 25
    target 980
  ]
  edge [
    source 25
    target 981
  ]
  edge [
    source 25
    target 982
  ]
  edge [
    source 25
    target 983
  ]
  edge [
    source 25
    target 984
  ]
  edge [
    source 25
    target 985
  ]
  edge [
    source 25
    target 986
  ]
  edge [
    source 25
    target 987
  ]
  edge [
    source 25
    target 988
  ]
  edge [
    source 25
    target 989
  ]
  edge [
    source 25
    target 990
  ]
  edge [
    source 25
    target 991
  ]
  edge [
    source 25
    target 992
  ]
  edge [
    source 25
    target 993
  ]
  edge [
    source 25
    target 994
  ]
  edge [
    source 25
    target 995
  ]
  edge [
    source 25
    target 996
  ]
  edge [
    source 25
    target 997
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 998
  ]
  edge [
    source 27
    target 999
  ]
  edge [
    source 27
    target 1000
  ]
  edge [
    source 27
    target 1001
  ]
  edge [
    source 27
    target 1002
  ]
  edge [
    source 27
    target 658
  ]
  edge [
    source 27
    target 1003
  ]
  edge [
    source 27
    target 286
  ]
  edge [
    source 27
    target 1004
  ]
  edge [
    source 27
    target 1005
  ]
  edge [
    source 27
    target 1006
  ]
  edge [
    source 27
    target 1007
  ]
  edge [
    source 27
    target 1008
  ]
  edge [
    source 27
    target 1009
  ]
  edge [
    source 27
    target 1010
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 1011
  ]
  edge [
    source 28
    target 1012
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 1013
  ]
  edge [
    source 29
    target 1014
  ]
  edge [
    source 29
    target 1015
  ]
  edge [
    source 29
    target 159
  ]
  edge [
    source 29
    target 1016
  ]
  edge [
    source 29
    target 1017
  ]
  edge [
    source 29
    target 1018
  ]
  edge [
    source 29
    target 1019
  ]
  edge [
    source 29
    target 1020
  ]
  edge [
    source 29
    target 1021
  ]
  edge [
    source 29
    target 1022
  ]
  edge [
    source 29
    target 1023
  ]
  edge [
    source 29
    target 1024
  ]
  edge [
    source 29
    target 64
  ]
  edge [
    source 29
    target 1025
  ]
  edge [
    source 29
    target 65
  ]
  edge [
    source 29
    target 1026
  ]
  edge [
    source 29
    target 1027
  ]
  edge [
    source 29
    target 1028
  ]
  edge [
    source 29
    target 300
  ]
  edge [
    source 29
    target 176
  ]
  edge [
    source 29
    target 1029
  ]
  edge [
    source 29
    target 1030
  ]
  edge [
    source 29
    target 1031
  ]
  edge [
    source 29
    target 1032
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 1033
  ]
  edge [
    source 30
    target 1034
  ]
  edge [
    source 30
    target 1035
  ]
  edge [
    source 30
    target 1036
  ]
  edge [
    source 30
    target 1037
  ]
  edge [
    source 30
    target 1038
  ]
  edge [
    source 31
    target 1039
  ]
  edge [
    source 31
    target 1040
  ]
  edge [
    source 31
    target 209
  ]
  edge [
    source 31
    target 1041
  ]
  edge [
    source 31
    target 1042
  ]
  edge [
    source 31
    target 1043
  ]
  edge [
    source 31
    target 1044
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 1045
  ]
  edge [
    source 33
    target 217
  ]
  edge [
    source 33
    target 1046
  ]
  edge [
    source 33
    target 1047
  ]
  edge [
    source 33
    target 1048
  ]
  edge [
    source 33
    target 1049
  ]
  edge [
    source 33
    target 1050
  ]
  edge [
    source 33
    target 1051
  ]
  edge [
    source 33
    target 1052
  ]
  edge [
    source 33
    target 1053
  ]
  edge [
    source 33
    target 1054
  ]
  edge [
    source 33
    target 1055
  ]
  edge [
    source 33
    target 1056
  ]
  edge [
    source 33
    target 1057
  ]
  edge [
    source 33
    target 190
  ]
  edge [
    source 33
    target 223
  ]
  edge [
    source 33
    target 1058
  ]
  edge [
    source 33
    target 1059
  ]
  edge [
    source 33
    target 1060
  ]
  edge [
    source 33
    target 1061
  ]
  edge [
    source 34
    target 1062
  ]
  edge [
    source 34
    target 75
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 1063
  ]
  edge [
    source 35
    target 1064
  ]
  edge [
    source 35
    target 1065
  ]
  edge [
    source 35
    target 1066
  ]
  edge [
    source 35
    target 1067
  ]
  edge [
    source 35
    target 1068
  ]
  edge [
    source 35
    target 1069
  ]
  edge [
    source 35
    target 1070
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 1071
  ]
  edge [
    source 36
    target 1072
  ]
  edge [
    source 36
    target 1073
  ]
  edge [
    source 36
    target 1074
  ]
  edge [
    source 36
    target 1075
  ]
  edge [
    source 36
    target 1076
  ]
  edge [
    source 36
    target 1077
  ]
  edge [
    source 36
    target 1078
  ]
  edge [
    source 36
    target 1079
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 1080
  ]
  edge [
    source 37
    target 1081
  ]
  edge [
    source 37
    target 1082
  ]
  edge [
    source 37
    target 1083
  ]
  edge [
    source 37
    target 1084
  ]
  edge [
    source 37
    target 1085
  ]
  edge [
    source 37
    target 1086
  ]
  edge [
    source 37
    target 1087
  ]
  edge [
    source 37
    target 1088
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 1089
  ]
  edge [
    source 40
    target 1090
  ]
  edge [
    source 40
    target 1091
  ]
  edge [
    source 40
    target 1092
  ]
  edge [
    source 40
    target 1093
  ]
  edge [
    source 40
    target 1094
  ]
  edge [
    source 40
    target 223
  ]
  edge [
    source 40
    target 1095
  ]
  edge [
    source 40
    target 1096
  ]
  edge [
    source 40
    target 1097
  ]
  edge [
    source 40
    target 1098
  ]
  edge [
    source 40
    target 1099
  ]
  edge [
    source 40
    target 153
  ]
  edge [
    source 40
    target 1100
  ]
  edge [
    source 40
    target 1101
  ]
  edge [
    source 40
    target 1102
  ]
  edge [
    source 40
    target 1103
  ]
  edge [
    source 40
    target 1104
  ]
  edge [
    source 40
    target 1105
  ]
  edge [
    source 40
    target 1106
  ]
  edge [
    source 40
    target 1107
  ]
  edge [
    source 40
    target 1108
  ]
  edge [
    source 40
    target 177
  ]
  edge [
    source 40
    target 1109
  ]
  edge [
    source 40
    target 1110
  ]
  edge [
    source 40
    target 1111
  ]
  edge [
    source 40
    target 1112
  ]
  edge [
    source 40
    target 1113
  ]
  edge [
    source 40
    target 1114
  ]
  edge [
    source 40
    target 1115
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 1116
  ]
  edge [
    source 41
    target 1117
  ]
  edge [
    source 41
    target 1118
  ]
  edge [
    source 41
    target 1119
  ]
  edge [
    source 41
    target 1120
  ]
  edge [
    source 41
    target 128
  ]
  edge [
    source 41
    target 1121
  ]
  edge [
    source 41
    target 1122
  ]
  edge [
    source 41
    target 1123
  ]
  edge [
    source 41
    target 1124
  ]
  edge [
    source 41
    target 1041
  ]
  edge [
    source 41
    target 1125
  ]
  edge [
    source 41
    target 1126
  ]
  edge [
    source 41
    target 1127
  ]
  edge [
    source 41
    target 1128
  ]
  edge [
    source 41
    target 1129
  ]
  edge [
    source 41
    target 1130
  ]
  edge [
    source 41
    target 1131
  ]
  edge [
    source 41
    target 1132
  ]
  edge [
    source 41
    target 1133
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 1134
  ]
  edge [
    source 42
    target 1135
  ]
  edge [
    source 42
    target 1136
  ]
  edge [
    source 42
    target 1137
  ]
  edge [
    source 42
    target 1138
  ]
  edge [
    source 42
    target 1139
  ]
  edge [
    source 43
    target 1140
  ]
  edge [
    source 43
    target 1141
  ]
  edge [
    source 43
    target 1142
  ]
  edge [
    source 43
    target 1143
  ]
  edge [
    source 43
    target 1144
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 1145
  ]
  edge [
    source 44
    target 1146
  ]
  edge [
    source 44
    target 1147
  ]
  edge [
    source 44
    target 1148
  ]
  edge [
    source 44
    target 1149
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 1150
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 1151
  ]
  edge [
    source 47
    target 1152
  ]
  edge [
    source 47
    target 1153
  ]
  edge [
    source 47
    target 1154
  ]
  edge [
    source 48
    target 1155
  ]
  edge [
    source 48
    target 1156
  ]
  edge [
    source 48
    target 1157
  ]
  edge [
    source 48
    target 1158
  ]
  edge [
    source 48
    target 1159
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 1160
  ]
  edge [
    source 51
    target 1161
  ]
  edge [
    source 51
    target 1162
  ]
  edge [
    source 51
    target 159
  ]
  edge [
    source 51
    target 214
  ]
  edge [
    source 51
    target 171
  ]
  edge [
    source 51
    target 1088
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 1163
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 1164
  ]
  edge [
    source 53
    target 1165
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 1166
  ]
  edge [
    source 54
    target 1158
  ]
  edge [
    source 54
    target 1167
  ]
  edge [
    source 54
    target 1168
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 1116
  ]
  edge [
    source 55
    target 1169
  ]
  edge [
    source 55
    target 1170
  ]
  edge [
    source 55
    target 1171
  ]
  edge [
    source 55
    target 1172
  ]
  edge [
    source 55
    target 1173
  ]
  edge [
    source 55
    target 253
  ]
  edge [
    source 55
    target 1174
  ]
  edge [
    source 55
    target 1175
  ]
  edge [
    source 55
    target 1176
  ]
  edge [
    source 55
    target 1177
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 1178
  ]
  edge [
    source 56
    target 1179
  ]
  edge [
    source 56
    target 1180
  ]
  edge [
    source 56
    target 1181
  ]
  edge [
    source 56
    target 1182
  ]
  edge [
    source 56
    target 1183
  ]
  edge [
    source 56
    target 1184
  ]
  edge [
    source 56
    target 1001
  ]
  edge [
    source 56
    target 1185
  ]
  edge [
    source 56
    target 1186
  ]
  edge [
    source 56
    target 1187
  ]
  edge [
    source 56
    target 286
  ]
  edge [
    source 56
    target 1188
  ]
  edge [
    source 56
    target 1189
  ]
  edge [
    source 56
    target 1190
  ]
  edge [
    source 56
    target 1191
  ]
  edge [
    source 56
    target 1192
  ]
  edge [
    source 56
    target 1193
  ]
  edge [
    source 56
    target 1194
  ]
  edge [
    source 56
    target 1195
  ]
  edge [
    source 56
    target 1003
  ]
  edge [
    source 56
    target 1196
  ]
  edge [
    source 56
    target 1197
  ]
  edge [
    source 56
    target 1198
  ]
  edge [
    source 56
    target 1199
  ]
  edge [
    source 56
    target 1200
  ]
  edge [
    source 56
    target 1201
  ]
  edge [
    source 56
    target 1202
  ]
  edge [
    source 56
    target 999
  ]
  edge [
    source 56
    target 1160
  ]
  edge [
    source 56
    target 1203
  ]
  edge [
    source 56
    target 1204
  ]
  edge [
    source 56
    target 304
  ]
  edge [
    source 56
    target 1205
  ]
  edge [
    source 56
    target 1206
  ]
  edge [
    source 56
    target 1207
  ]
  edge [
    source 56
    target 1208
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 1209
  ]
  edge [
    source 57
    target 1210
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 59
    target 64
  ]
  edge [
    source 59
    target 1211
  ]
  edge [
    source 59
    target 65
  ]
  edge [
    source 59
    target 1212
  ]
  edge [
    source 59
    target 1213
  ]
  edge [
    source 59
    target 1214
  ]
  edge [
    source 59
    target 1017
  ]
  edge [
    source 59
    target 1215
  ]
  edge [
    source 59
    target 1216
  ]
  edge [
    source 59
    target 1217
  ]
]
