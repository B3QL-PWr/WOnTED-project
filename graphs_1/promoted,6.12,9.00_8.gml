graph [
  maxDegree 43
  minDegree 1
  meanDegree 1.9655172413793103
  density 0.017091454272863568
  graphCliqueNumber 2
  node [
    id 0
    label "problem"
    origin "text"
  ]
  node [
    id 1
    label "system"
    origin "text"
  ]
  node [
    id 2
    label "hydrauliczny"
    origin "text"
  ]
  node [
    id 3
    label "falcona"
    origin "text"
  ]
  node [
    id 4
    label "spowodowa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "l&#261;dowanie"
    origin "text"
  ]
  node [
    id 6
    label "woda"
    origin "text"
  ]
  node [
    id 7
    label "trudno&#347;&#263;"
  ]
  node [
    id 8
    label "sprawa"
  ]
  node [
    id 9
    label "ambaras"
  ]
  node [
    id 10
    label "problemat"
  ]
  node [
    id 11
    label "pierepa&#322;ka"
  ]
  node [
    id 12
    label "obstruction"
  ]
  node [
    id 13
    label "problematyka"
  ]
  node [
    id 14
    label "jajko_Kolumba"
  ]
  node [
    id 15
    label "subiekcja"
  ]
  node [
    id 16
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 17
    label "model"
  ]
  node [
    id 18
    label "sk&#322;ad"
  ]
  node [
    id 19
    label "zachowanie"
  ]
  node [
    id 20
    label "podstawa"
  ]
  node [
    id 21
    label "porz&#261;dek"
  ]
  node [
    id 22
    label "Android"
  ]
  node [
    id 23
    label "przyn&#281;ta"
  ]
  node [
    id 24
    label "jednostka_geologiczna"
  ]
  node [
    id 25
    label "metoda"
  ]
  node [
    id 26
    label "podsystem"
  ]
  node [
    id 27
    label "p&#322;&#243;d"
  ]
  node [
    id 28
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 29
    label "s&#261;d"
  ]
  node [
    id 30
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 31
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 32
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 33
    label "j&#261;dro"
  ]
  node [
    id 34
    label "eratem"
  ]
  node [
    id 35
    label "ryba"
  ]
  node [
    id 36
    label "pulpit"
  ]
  node [
    id 37
    label "struktura"
  ]
  node [
    id 38
    label "spos&#243;b"
  ]
  node [
    id 39
    label "oddzia&#322;"
  ]
  node [
    id 40
    label "usenet"
  ]
  node [
    id 41
    label "o&#347;"
  ]
  node [
    id 42
    label "oprogramowanie"
  ]
  node [
    id 43
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 44
    label "poj&#281;cie"
  ]
  node [
    id 45
    label "w&#281;dkarstwo"
  ]
  node [
    id 46
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 47
    label "Leopard"
  ]
  node [
    id 48
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 49
    label "systemik"
  ]
  node [
    id 50
    label "rozprz&#261;c"
  ]
  node [
    id 51
    label "cybernetyk"
  ]
  node [
    id 52
    label "konstelacja"
  ]
  node [
    id 53
    label "doktryna"
  ]
  node [
    id 54
    label "net"
  ]
  node [
    id 55
    label "zbi&#243;r"
  ]
  node [
    id 56
    label "method"
  ]
  node [
    id 57
    label "systemat"
  ]
  node [
    id 58
    label "hydraulicznie"
  ]
  node [
    id 59
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 60
    label "act"
  ]
  node [
    id 61
    label "descent"
  ]
  node [
    id 62
    label "radzenie_sobie"
  ]
  node [
    id 63
    label "trafianie"
  ]
  node [
    id 64
    label "przybycie"
  ]
  node [
    id 65
    label "trafienie"
  ]
  node [
    id 66
    label "dobijanie"
  ]
  node [
    id 67
    label "podr&#243;&#380;owanie"
  ]
  node [
    id 68
    label "poradzenie_sobie"
  ]
  node [
    id 69
    label "skok"
  ]
  node [
    id 70
    label "przybywanie"
  ]
  node [
    id 71
    label "dobicie"
  ]
  node [
    id 72
    label "lot"
  ]
  node [
    id 73
    label "lecenie"
  ]
  node [
    id 74
    label "wypowied&#378;"
  ]
  node [
    id 75
    label "obiekt_naturalny"
  ]
  node [
    id 76
    label "bicie"
  ]
  node [
    id 77
    label "wysi&#281;k"
  ]
  node [
    id 78
    label "pustka"
  ]
  node [
    id 79
    label "woda_s&#322;odka"
  ]
  node [
    id 80
    label "p&#322;ycizna"
  ]
  node [
    id 81
    label "ciecz"
  ]
  node [
    id 82
    label "spi&#281;trza&#263;"
  ]
  node [
    id 83
    label "uj&#281;cie_wody"
  ]
  node [
    id 84
    label "chlasta&#263;"
  ]
  node [
    id 85
    label "spi&#281;trzy&#263;"
  ]
  node [
    id 86
    label "nap&#243;j"
  ]
  node [
    id 87
    label "bombast"
  ]
  node [
    id 88
    label "water"
  ]
  node [
    id 89
    label "kryptodepresja"
  ]
  node [
    id 90
    label "wodnik"
  ]
  node [
    id 91
    label "pojazd"
  ]
  node [
    id 92
    label "&#347;r&#243;dl&#261;dowy"
  ]
  node [
    id 93
    label "fala"
  ]
  node [
    id 94
    label "Waruna"
  ]
  node [
    id 95
    label "nap&#243;j_bezalkoholowy"
  ]
  node [
    id 96
    label "zrzut"
  ]
  node [
    id 97
    label "dotleni&#263;"
  ]
  node [
    id 98
    label "utylizator"
  ]
  node [
    id 99
    label "przyroda"
  ]
  node [
    id 100
    label "uci&#261;g"
  ]
  node [
    id 101
    label "wybrze&#380;e"
  ]
  node [
    id 102
    label "nabranie"
  ]
  node [
    id 103
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 104
    label "chlastanie"
  ]
  node [
    id 105
    label "klarownik"
  ]
  node [
    id 106
    label "przybrze&#380;e"
  ]
  node [
    id 107
    label "deklamacja"
  ]
  node [
    id 108
    label "spi&#281;trzenie"
  ]
  node [
    id 109
    label "przybieranie"
  ]
  node [
    id 110
    label "nabra&#263;"
  ]
  node [
    id 111
    label "tlenek"
  ]
  node [
    id 112
    label "spi&#281;trzanie"
  ]
  node [
    id 113
    label "l&#243;d"
  ]
  node [
    id 114
    label "Falcona"
  ]
  node [
    id 115
    label "9"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 114
    target 115
  ]
]
