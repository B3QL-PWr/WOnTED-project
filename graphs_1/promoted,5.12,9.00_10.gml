graph [
  maxDegree 24
  minDegree 1
  meanDegree 1.872340425531915
  density 0.04070305272895467
  graphCliqueNumber 2
  node [
    id 0
    label "razem"
    origin "text"
  ]
  node [
    id 1
    label "aut"
    origin "text"
  ]
  node [
    id 2
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 3
    label "silnik"
    origin "text"
  ]
  node [
    id 4
    label "&#322;&#261;cznie"
  ]
  node [
    id 5
    label "wrzut"
  ]
  node [
    id 6
    label "przedostanie_si&#281;"
  ]
  node [
    id 7
    label "przestrze&#324;"
  ]
  node [
    id 8
    label "out"
  ]
  node [
    id 9
    label "pi&#322;ka"
  ]
  node [
    id 10
    label "boisko"
  ]
  node [
    id 11
    label "doros&#322;y"
  ]
  node [
    id 12
    label "wiele"
  ]
  node [
    id 13
    label "dorodny"
  ]
  node [
    id 14
    label "znaczny"
  ]
  node [
    id 15
    label "du&#380;o"
  ]
  node [
    id 16
    label "prawdziwy"
  ]
  node [
    id 17
    label "niema&#322;o"
  ]
  node [
    id 18
    label "wa&#380;ny"
  ]
  node [
    id 19
    label "rozwini&#281;ty"
  ]
  node [
    id 20
    label "dotarcie"
  ]
  node [
    id 21
    label "wyci&#261;garka"
  ]
  node [
    id 22
    label "biblioteka"
  ]
  node [
    id 23
    label "aerosanie"
  ]
  node [
    id 24
    label "podgrzewacz"
  ]
  node [
    id 25
    label "bombowiec"
  ]
  node [
    id 26
    label "dociera&#263;"
  ]
  node [
    id 27
    label "gniazdo_zaworowe"
  ]
  node [
    id 28
    label "motor&#243;wka"
  ]
  node [
    id 29
    label "nap&#281;d"
  ]
  node [
    id 30
    label "perpetuum_mobile"
  ]
  node [
    id 31
    label "rz&#281;&#380;enie"
  ]
  node [
    id 32
    label "mechanizm"
  ]
  node [
    id 33
    label "gondola_silnikowa"
  ]
  node [
    id 34
    label "docieranie"
  ]
  node [
    id 35
    label "uk&#322;ad_zasilania"
  ]
  node [
    id 36
    label "rz&#281;zi&#263;"
  ]
  node [
    id 37
    label "motoszybowiec"
  ]
  node [
    id 38
    label "motogodzina"
  ]
  node [
    id 39
    label "samoch&#243;d"
  ]
  node [
    id 40
    label "dotrze&#263;"
  ]
  node [
    id 41
    label "radiator"
  ]
  node [
    id 42
    label "program"
  ]
  node [
    id 43
    label "Mateusz"
  ]
  node [
    id 44
    label "Morawiecki"
  ]
  node [
    id 45
    label "ministerstwo"
  ]
  node [
    id 46
    label "finanse"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 45
    target 46
  ]
]
