graph [
  maxDegree 72
  minDegree 1
  meanDegree 2.1422018348623855
  density 0.004924601919223874
  graphCliqueNumber 3
  node [
    id 0
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 1
    label "wkr&#243;tce"
    origin "text"
  ]
  node [
    id 2
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 3
    label "zaprezentowa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "polska"
    origin "text"
  ]
  node [
    id 5
    label "wersja"
    origin "text"
  ]
  node [
    id 6
    label "wyszukiwarka"
    origin "text"
  ]
  node [
    id 7
    label "book"
    origin "text"
  ]
  node [
    id 8
    label "search"
    origin "text"
  ]
  node [
    id 9
    label "zapowiada&#263;"
    origin "text"
  ]
  node [
    id 10
    label "jens"
    origin "text"
  ]
  node [
    id 11
    label "redmer"
    origin "text"
  ]
  node [
    id 12
    label "przedstawiciel"
    origin "text"
  ]
  node [
    id 13
    label "google"
    origin "text"
  ]
  node [
    id 14
    label "nasz"
    origin "text"
  ]
  node [
    id 15
    label "strefa"
    origin "text"
  ]
  node [
    id 16
    label "czasowy"
    origin "text"
  ]
  node [
    id 17
    label "doskona&#322;y"
    origin "text"
  ]
  node [
    id 18
    label "wiadomo&#347;&#263;"
    origin "text"
  ]
  node [
    id 19
    label "niestety"
    origin "text"
  ]
  node [
    id 20
    label "nasze"
    origin "text"
  ]
  node [
    id 21
    label "rodzimy"
    origin "text"
  ]
  node [
    id 22
    label "rozwi&#261;zanie"
    origin "text"
  ]
  node [
    id 23
    label "taki"
    origin "text"
  ]
  node [
    id 24
    label "jak"
    origin "text"
  ]
  node [
    id 25
    label "biblioteka"
    origin "text"
  ]
  node [
    id 26
    label "internetowy"
    origin "text"
  ]
  node [
    id 27
    label "by&#263;"
    origin "text"
  ]
  node [
    id 28
    label "wiele"
    origin "text"
  ]
  node [
    id 29
    label "pow&#243;d"
    origin "text"
  ]
  node [
    id 30
    label "kompletny"
    origin "text"
  ]
  node [
    id 31
    label "pomy&#322;ka"
    origin "text"
  ]
  node [
    id 32
    label "druga"
    origin "text"
  ]
  node [
    id 33
    label "strona"
    origin "text"
  ]
  node [
    id 34
    label "dzia&#322;ania"
    origin "text"
  ]
  node [
    id 35
    label "wydawa&#263;"
    origin "text"
  ]
  node [
    id 36
    label "si&#281;"
    origin "text"
  ]
  node [
    id 37
    label "tyle&#380;"
    origin "text"
  ]
  node [
    id 38
    label "fascynuj&#261;cy"
    origin "text"
  ]
  node [
    id 39
    label "niepokoj&#261;cy"
    origin "text"
  ]
  node [
    id 40
    label "w&#322;adza"
    origin "text"
  ]
  node [
    id 41
    label "nad"
    origin "text"
  ]
  node [
    id 42
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 43
    label "informacja"
    origin "text"
  ]
  node [
    id 44
    label "ale"
    origin "text"
  ]
  node [
    id 45
    label "jedno"
    origin "text"
  ]
  node [
    id 46
    label "wychodzi&#263;"
    origin "text"
  ]
  node [
    id 47
    label "oddawa&#263;"
    origin "text"
  ]
  node [
    id 48
    label "prywatny"
    origin "text"
  ]
  node [
    id 49
    label "nastawi&#263;"
    origin "text"
  ]
  node [
    id 50
    label "tylko"
    origin "text"
  ]
  node [
    id 51
    label "zysk"
    origin "text"
  ]
  node [
    id 52
    label "firma"
    origin "text"
  ]
  node [
    id 53
    label "wpr&#281;dce"
  ]
  node [
    id 54
    label "blisko"
  ]
  node [
    id 55
    label "nied&#322;ugi"
  ]
  node [
    id 56
    label "proceed"
  ]
  node [
    id 57
    label "catch"
  ]
  node [
    id 58
    label "pozosta&#263;"
  ]
  node [
    id 59
    label "osta&#263;_si&#281;"
  ]
  node [
    id 60
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 61
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 62
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 63
    label "change"
  ]
  node [
    id 64
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 65
    label "typify"
  ]
  node [
    id 66
    label "pokaza&#263;"
  ]
  node [
    id 67
    label "uprzedzi&#263;"
  ]
  node [
    id 68
    label "przedstawi&#263;"
  ]
  node [
    id 69
    label "represent"
  ]
  node [
    id 70
    label "program"
  ]
  node [
    id 71
    label "testify"
  ]
  node [
    id 72
    label "zapozna&#263;"
  ]
  node [
    id 73
    label "attest"
  ]
  node [
    id 74
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 75
    label "typ"
  ]
  node [
    id 76
    label "posta&#263;"
  ]
  node [
    id 77
    label "search_engine"
  ]
  node [
    id 78
    label "Google"
  ]
  node [
    id 79
    label "plik"
  ]
  node [
    id 80
    label "informowa&#263;"
  ]
  node [
    id 81
    label "og&#322;asza&#263;"
  ]
  node [
    id 82
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 83
    label "bode"
  ]
  node [
    id 84
    label "post"
  ]
  node [
    id 85
    label "ostrzega&#263;"
  ]
  node [
    id 86
    label "harbinger"
  ]
  node [
    id 87
    label "cz&#322;owiek"
  ]
  node [
    id 88
    label "cz&#322;onek"
  ]
  node [
    id 89
    label "substytuowanie"
  ]
  node [
    id 90
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 91
    label "przyk&#322;ad"
  ]
  node [
    id 92
    label "zast&#281;pca"
  ]
  node [
    id 93
    label "substytuowa&#263;"
  ]
  node [
    id 94
    label "czyj&#347;"
  ]
  node [
    id 95
    label "obszar"
  ]
  node [
    id 96
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 97
    label "obrona_strefowa"
  ]
  node [
    id 98
    label "przepustka"
  ]
  node [
    id 99
    label "czasowo"
  ]
  node [
    id 100
    label "wspania&#322;y"
  ]
  node [
    id 101
    label "pe&#322;ny"
  ]
  node [
    id 102
    label "naj"
  ]
  node [
    id 103
    label "doskonale"
  ]
  node [
    id 104
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 105
    label "&#347;wietny"
  ]
  node [
    id 106
    label "zarys"
  ]
  node [
    id 107
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 108
    label "zniesienie"
  ]
  node [
    id 109
    label "nap&#322;ywanie"
  ]
  node [
    id 110
    label "znosi&#263;"
  ]
  node [
    id 111
    label "znie&#347;&#263;"
  ]
  node [
    id 112
    label "komunikat"
  ]
  node [
    id 113
    label "depesza_emska"
  ]
  node [
    id 114
    label "communication"
  ]
  node [
    id 115
    label "znoszenie"
  ]
  node [
    id 116
    label "signal"
  ]
  node [
    id 117
    label "w&#322;asny"
  ]
  node [
    id 118
    label "tutejszy"
  ]
  node [
    id 119
    label "wynik"
  ]
  node [
    id 120
    label "wyj&#347;cie"
  ]
  node [
    id 121
    label "spe&#322;nienie"
  ]
  node [
    id 122
    label "cesarskie_ci&#281;cie"
  ]
  node [
    id 123
    label "po&#322;o&#380;na"
  ]
  node [
    id 124
    label "proces_fizjologiczny"
  ]
  node [
    id 125
    label "przestanie"
  ]
  node [
    id 126
    label "marc&#243;wka"
  ]
  node [
    id 127
    label "usuni&#281;cie"
  ]
  node [
    id 128
    label "uniewa&#380;nienie"
  ]
  node [
    id 129
    label "pomys&#322;"
  ]
  node [
    id 130
    label "birth"
  ]
  node [
    id 131
    label "wymy&#347;lenie"
  ]
  node [
    id 132
    label "po&#322;&#243;g"
  ]
  node [
    id 133
    label "szok_poporodowy"
  ]
  node [
    id 134
    label "event"
  ]
  node [
    id 135
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 136
    label "spos&#243;b"
  ]
  node [
    id 137
    label "rozwi&#261;zywanie_si&#281;"
  ]
  node [
    id 138
    label "dula"
  ]
  node [
    id 139
    label "okre&#347;lony"
  ]
  node [
    id 140
    label "jaki&#347;"
  ]
  node [
    id 141
    label "byd&#322;o"
  ]
  node [
    id 142
    label "zobo"
  ]
  node [
    id 143
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 144
    label "yakalo"
  ]
  node [
    id 145
    label "dzo"
  ]
  node [
    id 146
    label "pok&#243;j"
  ]
  node [
    id 147
    label "rewers"
  ]
  node [
    id 148
    label "informatorium"
  ]
  node [
    id 149
    label "kolekcja"
  ]
  node [
    id 150
    label "czytelnik"
  ]
  node [
    id 151
    label "budynek"
  ]
  node [
    id 152
    label "zbi&#243;r"
  ]
  node [
    id 153
    label "programowanie"
  ]
  node [
    id 154
    label "ksi&#281;gozbi&#243;r_podr&#281;czny"
  ]
  node [
    id 155
    label "library"
  ]
  node [
    id 156
    label "instytucja"
  ]
  node [
    id 157
    label "czytelnia"
  ]
  node [
    id 158
    label "nowoczesny"
  ]
  node [
    id 159
    label "elektroniczny"
  ]
  node [
    id 160
    label "sieciowo"
  ]
  node [
    id 161
    label "netowy"
  ]
  node [
    id 162
    label "internetowo"
  ]
  node [
    id 163
    label "si&#281;ga&#263;"
  ]
  node [
    id 164
    label "trwa&#263;"
  ]
  node [
    id 165
    label "obecno&#347;&#263;"
  ]
  node [
    id 166
    label "stan"
  ]
  node [
    id 167
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 168
    label "stand"
  ]
  node [
    id 169
    label "mie&#263;_miejsce"
  ]
  node [
    id 170
    label "uczestniczy&#263;"
  ]
  node [
    id 171
    label "chodzi&#263;"
  ]
  node [
    id 172
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 173
    label "equal"
  ]
  node [
    id 174
    label "wiela"
  ]
  node [
    id 175
    label "du&#380;y"
  ]
  node [
    id 176
    label "przyczyna"
  ]
  node [
    id 177
    label "matuszka"
  ]
  node [
    id 178
    label "geneza"
  ]
  node [
    id 179
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 180
    label "czynnik"
  ]
  node [
    id 181
    label "poci&#261;ganie"
  ]
  node [
    id 182
    label "rezultat"
  ]
  node [
    id 183
    label "uprz&#261;&#380;"
  ]
  node [
    id 184
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 185
    label "subject"
  ]
  node [
    id 186
    label "kompletnie"
  ]
  node [
    id 187
    label "w_pizdu"
  ]
  node [
    id 188
    label "zupe&#322;ny"
  ]
  node [
    id 189
    label "czyn"
  ]
  node [
    id 190
    label "error"
  ]
  node [
    id 191
    label "po&#322;&#261;czenie"
  ]
  node [
    id 192
    label "faux_pas"
  ]
  node [
    id 193
    label "godzina"
  ]
  node [
    id 194
    label "skr&#281;canie"
  ]
  node [
    id 195
    label "voice"
  ]
  node [
    id 196
    label "forma"
  ]
  node [
    id 197
    label "internet"
  ]
  node [
    id 198
    label "skr&#281;ci&#263;"
  ]
  node [
    id 199
    label "kartka"
  ]
  node [
    id 200
    label "orientowa&#263;"
  ]
  node [
    id 201
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 202
    label "powierzchnia"
  ]
  node [
    id 203
    label "bok"
  ]
  node [
    id 204
    label "pagina"
  ]
  node [
    id 205
    label "orientowanie"
  ]
  node [
    id 206
    label "fragment"
  ]
  node [
    id 207
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 208
    label "s&#261;d"
  ]
  node [
    id 209
    label "skr&#281;ca&#263;"
  ]
  node [
    id 210
    label "g&#243;ra"
  ]
  node [
    id 211
    label "serwis_internetowy"
  ]
  node [
    id 212
    label "orientacja"
  ]
  node [
    id 213
    label "linia"
  ]
  node [
    id 214
    label "skr&#281;cenie"
  ]
  node [
    id 215
    label "layout"
  ]
  node [
    id 216
    label "zorientowa&#263;"
  ]
  node [
    id 217
    label "zorientowanie"
  ]
  node [
    id 218
    label "obiekt"
  ]
  node [
    id 219
    label "podmiot"
  ]
  node [
    id 220
    label "ty&#322;"
  ]
  node [
    id 221
    label "logowanie"
  ]
  node [
    id 222
    label "adres_internetowy"
  ]
  node [
    id 223
    label "uj&#281;cie"
  ]
  node [
    id 224
    label "prz&#243;d"
  ]
  node [
    id 225
    label "strategia"
  ]
  node [
    id 226
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 227
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 228
    label "ob&#322;awa_augustowska"
  ]
  node [
    id 229
    label "impart"
  ]
  node [
    id 230
    label "panna_na_wydaniu"
  ]
  node [
    id 231
    label "surrender"
  ]
  node [
    id 232
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 233
    label "train"
  ]
  node [
    id 234
    label "give"
  ]
  node [
    id 235
    label "wytwarza&#263;"
  ]
  node [
    id 236
    label "dawa&#263;"
  ]
  node [
    id 237
    label "zapach"
  ]
  node [
    id 238
    label "wprowadza&#263;"
  ]
  node [
    id 239
    label "ujawnia&#263;"
  ]
  node [
    id 240
    label "wydawnictwo"
  ]
  node [
    id 241
    label "powierza&#263;"
  ]
  node [
    id 242
    label "produkcja"
  ]
  node [
    id 243
    label "denuncjowa&#263;"
  ]
  node [
    id 244
    label "plon"
  ]
  node [
    id 245
    label "reszta"
  ]
  node [
    id 246
    label "robi&#263;"
  ]
  node [
    id 247
    label "placard"
  ]
  node [
    id 248
    label "tajemnica"
  ]
  node [
    id 249
    label "wiano"
  ]
  node [
    id 250
    label "kojarzy&#263;"
  ]
  node [
    id 251
    label "d&#378;wi&#281;k"
  ]
  node [
    id 252
    label "podawa&#263;"
  ]
  node [
    id 253
    label "pasjonuj&#261;co"
  ]
  node [
    id 254
    label "interesuj&#261;cy"
  ]
  node [
    id 255
    label "niepokoj&#261;co"
  ]
  node [
    id 256
    label "panowanie"
  ]
  node [
    id 257
    label "Kreml"
  ]
  node [
    id 258
    label "wydolno&#347;&#263;"
  ]
  node [
    id 259
    label "grupa"
  ]
  node [
    id 260
    label "prawo"
  ]
  node [
    id 261
    label "rz&#261;dzenie"
  ]
  node [
    id 262
    label "rz&#261;d"
  ]
  node [
    id 263
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 264
    label "struktura"
  ]
  node [
    id 265
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 266
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 267
    label "obiekt_naturalny"
  ]
  node [
    id 268
    label "przedmiot"
  ]
  node [
    id 269
    label "biosfera"
  ]
  node [
    id 270
    label "stw&#243;r"
  ]
  node [
    id 271
    label "Stary_&#346;wiat"
  ]
  node [
    id 272
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 273
    label "rzecz"
  ]
  node [
    id 274
    label "magnetosfera"
  ]
  node [
    id 275
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 276
    label "environment"
  ]
  node [
    id 277
    label "Nowy_&#346;wiat"
  ]
  node [
    id 278
    label "geosfera"
  ]
  node [
    id 279
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 280
    label "planeta"
  ]
  node [
    id 281
    label "przejmowa&#263;"
  ]
  node [
    id 282
    label "litosfera"
  ]
  node [
    id 283
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 284
    label "makrokosmos"
  ]
  node [
    id 285
    label "barysfera"
  ]
  node [
    id 286
    label "biota"
  ]
  node [
    id 287
    label "p&#243;&#322;noc"
  ]
  node [
    id 288
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 289
    label "fauna"
  ]
  node [
    id 290
    label "wszechstworzenie"
  ]
  node [
    id 291
    label "geotermia"
  ]
  node [
    id 292
    label "biegun"
  ]
  node [
    id 293
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 294
    label "ekosystem"
  ]
  node [
    id 295
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 296
    label "teren"
  ]
  node [
    id 297
    label "zjawisko"
  ]
  node [
    id 298
    label "p&#243;&#322;kula"
  ]
  node [
    id 299
    label "atmosfera"
  ]
  node [
    id 300
    label "mikrokosmos"
  ]
  node [
    id 301
    label "class"
  ]
  node [
    id 302
    label "po&#322;udnie"
  ]
  node [
    id 303
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 304
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 305
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 306
    label "przejmowanie"
  ]
  node [
    id 307
    label "przestrze&#324;"
  ]
  node [
    id 308
    label "asymilowanie_si&#281;"
  ]
  node [
    id 309
    label "przej&#261;&#263;"
  ]
  node [
    id 310
    label "ekosfera"
  ]
  node [
    id 311
    label "przyroda"
  ]
  node [
    id 312
    label "ciemna_materia"
  ]
  node [
    id 313
    label "geoida"
  ]
  node [
    id 314
    label "Wsch&#243;d"
  ]
  node [
    id 315
    label "populace"
  ]
  node [
    id 316
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 317
    label "huczek"
  ]
  node [
    id 318
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 319
    label "Ziemia"
  ]
  node [
    id 320
    label "universe"
  ]
  node [
    id 321
    label "ozonosfera"
  ]
  node [
    id 322
    label "rze&#378;ba"
  ]
  node [
    id 323
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 324
    label "zagranica"
  ]
  node [
    id 325
    label "hydrosfera"
  ]
  node [
    id 326
    label "woda"
  ]
  node [
    id 327
    label "kuchnia"
  ]
  node [
    id 328
    label "przej&#281;cie"
  ]
  node [
    id 329
    label "czarna_dziura"
  ]
  node [
    id 330
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 331
    label "morze"
  ]
  node [
    id 332
    label "doj&#347;cie"
  ]
  node [
    id 333
    label "doj&#347;&#263;"
  ]
  node [
    id 334
    label "powzi&#261;&#263;"
  ]
  node [
    id 335
    label "wiedza"
  ]
  node [
    id 336
    label "sygna&#322;"
  ]
  node [
    id 337
    label "obiegni&#281;cie"
  ]
  node [
    id 338
    label "obieganie"
  ]
  node [
    id 339
    label "obiec"
  ]
  node [
    id 340
    label "dane"
  ]
  node [
    id 341
    label "obiega&#263;"
  ]
  node [
    id 342
    label "punkt"
  ]
  node [
    id 343
    label "publikacja"
  ]
  node [
    id 344
    label "powzi&#281;cie"
  ]
  node [
    id 345
    label "piwo"
  ]
  node [
    id 346
    label "uzyskiwa&#263;"
  ]
  node [
    id 347
    label "blend"
  ]
  node [
    id 348
    label "ograniczenie"
  ]
  node [
    id 349
    label "wydostawa&#263;_si&#281;"
  ]
  node [
    id 350
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 351
    label "za&#322;atwi&#263;"
  ]
  node [
    id 352
    label "schodzi&#263;"
  ]
  node [
    id 353
    label "gra&#263;"
  ]
  node [
    id 354
    label "osi&#261;ga&#263;"
  ]
  node [
    id 355
    label "okazywa&#263;_si&#281;"
  ]
  node [
    id 356
    label "seclude"
  ]
  node [
    id 357
    label "strona_&#347;wiata"
  ]
  node [
    id 358
    label "przedk&#322;ada&#263;"
  ]
  node [
    id 359
    label "przedstawia&#263;"
  ]
  node [
    id 360
    label "appear"
  ]
  node [
    id 361
    label "publish"
  ]
  node [
    id 362
    label "ko&#324;czy&#263;"
  ]
  node [
    id 363
    label "wypada&#263;"
  ]
  node [
    id 364
    label "pochodzi&#263;"
  ]
  node [
    id 365
    label "ukazywa&#263;_si&#281;_drukiem"
  ]
  node [
    id 366
    label "uwalnia&#263;_si&#281;"
  ]
  node [
    id 367
    label "wygl&#261;da&#263;"
  ]
  node [
    id 368
    label "opuszcza&#263;"
  ]
  node [
    id 369
    label "wystarcza&#263;"
  ]
  node [
    id 370
    label "wyrusza&#263;"
  ]
  node [
    id 371
    label "perform"
  ]
  node [
    id 372
    label "heighten"
  ]
  node [
    id 373
    label "za&#322;atwia&#263;_si&#281;"
  ]
  node [
    id 374
    label "render"
  ]
  node [
    id 375
    label "blurt_out"
  ]
  node [
    id 376
    label "sacrifice"
  ]
  node [
    id 377
    label "reflect"
  ]
  node [
    id 378
    label "dostarcza&#263;"
  ]
  node [
    id 379
    label "umieszcza&#263;"
  ]
  node [
    id 380
    label "deliver"
  ]
  node [
    id 381
    label "odst&#281;powa&#263;"
  ]
  node [
    id 382
    label "sprzedawa&#263;"
  ]
  node [
    id 383
    label "odpowiada&#263;"
  ]
  node [
    id 384
    label "przekazywa&#263;"
  ]
  node [
    id 385
    label "nieformalny"
  ]
  node [
    id 386
    label "personalny"
  ]
  node [
    id 387
    label "prywatnie"
  ]
  node [
    id 388
    label "niepubliczny"
  ]
  node [
    id 389
    label "aim"
  ]
  node [
    id 390
    label "z&#322;amanie"
  ]
  node [
    id 391
    label "poprawi&#263;"
  ]
  node [
    id 392
    label "zwr&#243;ci&#263;"
  ]
  node [
    id 393
    label "direct"
  ]
  node [
    id 394
    label "ustawi&#263;"
  ]
  node [
    id 395
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 396
    label "przyrz&#261;dzi&#263;"
  ]
  node [
    id 397
    label "umie&#347;ci&#263;"
  ]
  node [
    id 398
    label "plant"
  ]
  node [
    id 399
    label "wyznaczy&#263;"
  ]
  node [
    id 400
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 401
    label "set"
  ]
  node [
    id 402
    label "z&#322;o&#380;y&#263;"
  ]
  node [
    id 403
    label "dobro"
  ]
  node [
    id 404
    label "zaleta"
  ]
  node [
    id 405
    label "doch&#243;d"
  ]
  node [
    id 406
    label "MAC"
  ]
  node [
    id 407
    label "Hortex"
  ]
  node [
    id 408
    label "reengineering"
  ]
  node [
    id 409
    label "nazwa_w&#322;asna"
  ]
  node [
    id 410
    label "podmiot_gospodarczy"
  ]
  node [
    id 411
    label "zaufanie"
  ]
  node [
    id 412
    label "biurowiec"
  ]
  node [
    id 413
    label "interes"
  ]
  node [
    id 414
    label "zasoby_ludzkie"
  ]
  node [
    id 415
    label "networking"
  ]
  node [
    id 416
    label "paczkarnia"
  ]
  node [
    id 417
    label "Canon"
  ]
  node [
    id 418
    label "HP"
  ]
  node [
    id 419
    label "Baltona"
  ]
  node [
    id 420
    label "Pewex"
  ]
  node [
    id 421
    label "MAN_SE"
  ]
  node [
    id 422
    label "Apeks"
  ]
  node [
    id 423
    label "zasoby"
  ]
  node [
    id 424
    label "Orbis"
  ]
  node [
    id 425
    label "miejsce_pracy"
  ]
  node [
    id 426
    label "siedziba"
  ]
  node [
    id 427
    label "Spo&#322;em"
  ]
  node [
    id 428
    label "przedsi&#281;biorczo&#347;&#263;"
  ]
  node [
    id 429
    label "Orlen"
  ]
  node [
    id 430
    label "klasa"
  ]
  node [
    id 431
    label "Book"
  ]
  node [
    id 432
    label "Search"
  ]
  node [
    id 433
    label "Jens"
  ]
  node [
    id 434
    label "Redmer"
  ]
  node [
    id 435
    label "polski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 12
    target 89
  ]
  edge [
    source 12
    target 90
  ]
  edge [
    source 12
    target 91
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 34
  ]
  edge [
    source 13
    target 35
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 94
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 95
  ]
  edge [
    source 15
    target 96
  ]
  edge [
    source 15
    target 97
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 98
  ]
  edge [
    source 16
    target 99
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 100
  ]
  edge [
    source 17
    target 101
  ]
  edge [
    source 17
    target 102
  ]
  edge [
    source 17
    target 103
  ]
  edge [
    source 17
    target 104
  ]
  edge [
    source 17
    target 105
  ]
  edge [
    source 17
    target 42
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 106
  ]
  edge [
    source 18
    target 107
  ]
  edge [
    source 18
    target 108
  ]
  edge [
    source 18
    target 109
  ]
  edge [
    source 18
    target 110
  ]
  edge [
    source 18
    target 43
  ]
  edge [
    source 18
    target 111
  ]
  edge [
    source 18
    target 112
  ]
  edge [
    source 18
    target 113
  ]
  edge [
    source 18
    target 114
  ]
  edge [
    source 18
    target 115
  ]
  edge [
    source 18
    target 116
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 117
  ]
  edge [
    source 21
    target 118
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 119
  ]
  edge [
    source 22
    target 120
  ]
  edge [
    source 22
    target 121
  ]
  edge [
    source 22
    target 122
  ]
  edge [
    source 22
    target 123
  ]
  edge [
    source 22
    target 124
  ]
  edge [
    source 22
    target 125
  ]
  edge [
    source 22
    target 126
  ]
  edge [
    source 22
    target 127
  ]
  edge [
    source 22
    target 128
  ]
  edge [
    source 22
    target 129
  ]
  edge [
    source 22
    target 130
  ]
  edge [
    source 22
    target 131
  ]
  edge [
    source 22
    target 132
  ]
  edge [
    source 22
    target 133
  ]
  edge [
    source 22
    target 134
  ]
  edge [
    source 22
    target 135
  ]
  edge [
    source 22
    target 136
  ]
  edge [
    source 22
    target 137
  ]
  edge [
    source 22
    target 138
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 139
  ]
  edge [
    source 23
    target 140
  ]
  edge [
    source 24
    target 141
  ]
  edge [
    source 24
    target 142
  ]
  edge [
    source 24
    target 143
  ]
  edge [
    source 24
    target 144
  ]
  edge [
    source 24
    target 145
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 146
  ]
  edge [
    source 25
    target 147
  ]
  edge [
    source 25
    target 148
  ]
  edge [
    source 25
    target 149
  ]
  edge [
    source 25
    target 150
  ]
  edge [
    source 25
    target 151
  ]
  edge [
    source 25
    target 152
  ]
  edge [
    source 25
    target 153
  ]
  edge [
    source 25
    target 154
  ]
  edge [
    source 25
    target 155
  ]
  edge [
    source 25
    target 156
  ]
  edge [
    source 25
    target 157
  ]
  edge [
    source 25
    target 435
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 158
  ]
  edge [
    source 26
    target 159
  ]
  edge [
    source 26
    target 160
  ]
  edge [
    source 26
    target 161
  ]
  edge [
    source 26
    target 162
  ]
  edge [
    source 26
    target 435
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 163
  ]
  edge [
    source 27
    target 164
  ]
  edge [
    source 27
    target 165
  ]
  edge [
    source 27
    target 166
  ]
  edge [
    source 27
    target 167
  ]
  edge [
    source 27
    target 168
  ]
  edge [
    source 27
    target 169
  ]
  edge [
    source 27
    target 170
  ]
  edge [
    source 27
    target 171
  ]
  edge [
    source 27
    target 172
  ]
  edge [
    source 27
    target 173
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 39
  ]
  edge [
    source 28
    target 40
  ]
  edge [
    source 28
    target 174
  ]
  edge [
    source 28
    target 175
  ]
  edge [
    source 28
    target 38
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 33
  ]
  edge [
    source 29
    target 176
  ]
  edge [
    source 29
    target 177
  ]
  edge [
    source 29
    target 178
  ]
  edge [
    source 29
    target 179
  ]
  edge [
    source 29
    target 180
  ]
  edge [
    source 29
    target 181
  ]
  edge [
    source 29
    target 182
  ]
  edge [
    source 29
    target 183
  ]
  edge [
    source 29
    target 184
  ]
  edge [
    source 29
    target 185
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 186
  ]
  edge [
    source 30
    target 187
  ]
  edge [
    source 30
    target 188
  ]
  edge [
    source 30
    target 101
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 189
  ]
  edge [
    source 31
    target 190
  ]
  edge [
    source 31
    target 191
  ]
  edge [
    source 31
    target 192
  ]
  edge [
    source 31
    target 182
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 193
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 194
  ]
  edge [
    source 33
    target 195
  ]
  edge [
    source 33
    target 196
  ]
  edge [
    source 33
    target 197
  ]
  edge [
    source 33
    target 198
  ]
  edge [
    source 33
    target 199
  ]
  edge [
    source 33
    target 200
  ]
  edge [
    source 33
    target 201
  ]
  edge [
    source 33
    target 202
  ]
  edge [
    source 33
    target 79
  ]
  edge [
    source 33
    target 203
  ]
  edge [
    source 33
    target 204
  ]
  edge [
    source 33
    target 205
  ]
  edge [
    source 33
    target 206
  ]
  edge [
    source 33
    target 207
  ]
  edge [
    source 33
    target 208
  ]
  edge [
    source 33
    target 209
  ]
  edge [
    source 33
    target 210
  ]
  edge [
    source 33
    target 211
  ]
  edge [
    source 33
    target 212
  ]
  edge [
    source 33
    target 213
  ]
  edge [
    source 33
    target 214
  ]
  edge [
    source 33
    target 215
  ]
  edge [
    source 33
    target 216
  ]
  edge [
    source 33
    target 217
  ]
  edge [
    source 33
    target 218
  ]
  edge [
    source 33
    target 219
  ]
  edge [
    source 33
    target 220
  ]
  edge [
    source 33
    target 96
  ]
  edge [
    source 33
    target 221
  ]
  edge [
    source 33
    target 222
  ]
  edge [
    source 33
    target 223
  ]
  edge [
    source 33
    target 224
  ]
  edge [
    source 33
    target 76
  ]
  edge [
    source 33
    target 49
  ]
  edge [
    source 34
    target 225
  ]
  edge [
    source 34
    target 226
  ]
  edge [
    source 34
    target 227
  ]
  edge [
    source 34
    target 228
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 229
  ]
  edge [
    source 35
    target 230
  ]
  edge [
    source 35
    target 231
  ]
  edge [
    source 35
    target 232
  ]
  edge [
    source 35
    target 233
  ]
  edge [
    source 35
    target 234
  ]
  edge [
    source 35
    target 235
  ]
  edge [
    source 35
    target 236
  ]
  edge [
    source 35
    target 237
  ]
  edge [
    source 35
    target 238
  ]
  edge [
    source 35
    target 239
  ]
  edge [
    source 35
    target 240
  ]
  edge [
    source 35
    target 241
  ]
  edge [
    source 35
    target 242
  ]
  edge [
    source 35
    target 243
  ]
  edge [
    source 35
    target 169
  ]
  edge [
    source 35
    target 244
  ]
  edge [
    source 35
    target 245
  ]
  edge [
    source 35
    target 246
  ]
  edge [
    source 35
    target 247
  ]
  edge [
    source 35
    target 248
  ]
  edge [
    source 35
    target 249
  ]
  edge [
    source 35
    target 250
  ]
  edge [
    source 35
    target 251
  ]
  edge [
    source 35
    target 252
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 39
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 253
  ]
  edge [
    source 38
    target 254
  ]
  edge [
    source 39
    target 255
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 87
  ]
  edge [
    source 40
    target 256
  ]
  edge [
    source 40
    target 257
  ]
  edge [
    source 40
    target 258
  ]
  edge [
    source 40
    target 259
  ]
  edge [
    source 40
    target 260
  ]
  edge [
    source 40
    target 261
  ]
  edge [
    source 40
    target 262
  ]
  edge [
    source 40
    target 263
  ]
  edge [
    source 40
    target 264
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 265
  ]
  edge [
    source 42
    target 266
  ]
  edge [
    source 42
    target 95
  ]
  edge [
    source 42
    target 267
  ]
  edge [
    source 42
    target 268
  ]
  edge [
    source 42
    target 269
  ]
  edge [
    source 42
    target 259
  ]
  edge [
    source 42
    target 270
  ]
  edge [
    source 42
    target 271
  ]
  edge [
    source 42
    target 272
  ]
  edge [
    source 42
    target 273
  ]
  edge [
    source 42
    target 274
  ]
  edge [
    source 42
    target 275
  ]
  edge [
    source 42
    target 276
  ]
  edge [
    source 42
    target 277
  ]
  edge [
    source 42
    target 278
  ]
  edge [
    source 42
    target 279
  ]
  edge [
    source 42
    target 280
  ]
  edge [
    source 42
    target 281
  ]
  edge [
    source 42
    target 282
  ]
  edge [
    source 42
    target 283
  ]
  edge [
    source 42
    target 284
  ]
  edge [
    source 42
    target 285
  ]
  edge [
    source 42
    target 286
  ]
  edge [
    source 42
    target 287
  ]
  edge [
    source 42
    target 288
  ]
  edge [
    source 42
    target 289
  ]
  edge [
    source 42
    target 290
  ]
  edge [
    source 42
    target 291
  ]
  edge [
    source 42
    target 292
  ]
  edge [
    source 42
    target 293
  ]
  edge [
    source 42
    target 294
  ]
  edge [
    source 42
    target 295
  ]
  edge [
    source 42
    target 296
  ]
  edge [
    source 42
    target 297
  ]
  edge [
    source 42
    target 298
  ]
  edge [
    source 42
    target 299
  ]
  edge [
    source 42
    target 300
  ]
  edge [
    source 42
    target 301
  ]
  edge [
    source 42
    target 302
  ]
  edge [
    source 42
    target 303
  ]
  edge [
    source 42
    target 304
  ]
  edge [
    source 42
    target 305
  ]
  edge [
    source 42
    target 306
  ]
  edge [
    source 42
    target 307
  ]
  edge [
    source 42
    target 308
  ]
  edge [
    source 42
    target 309
  ]
  edge [
    source 42
    target 310
  ]
  edge [
    source 42
    target 311
  ]
  edge [
    source 42
    target 312
  ]
  edge [
    source 42
    target 313
  ]
  edge [
    source 42
    target 314
  ]
  edge [
    source 42
    target 315
  ]
  edge [
    source 42
    target 316
  ]
  edge [
    source 42
    target 317
  ]
  edge [
    source 42
    target 318
  ]
  edge [
    source 42
    target 319
  ]
  edge [
    source 42
    target 320
  ]
  edge [
    source 42
    target 321
  ]
  edge [
    source 42
    target 322
  ]
  edge [
    source 42
    target 323
  ]
  edge [
    source 42
    target 324
  ]
  edge [
    source 42
    target 325
  ]
  edge [
    source 42
    target 326
  ]
  edge [
    source 42
    target 327
  ]
  edge [
    source 42
    target 328
  ]
  edge [
    source 42
    target 329
  ]
  edge [
    source 42
    target 330
  ]
  edge [
    source 42
    target 331
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 332
  ]
  edge [
    source 43
    target 333
  ]
  edge [
    source 43
    target 334
  ]
  edge [
    source 43
    target 335
  ]
  edge [
    source 43
    target 336
  ]
  edge [
    source 43
    target 337
  ]
  edge [
    source 43
    target 338
  ]
  edge [
    source 43
    target 339
  ]
  edge [
    source 43
    target 340
  ]
  edge [
    source 43
    target 341
  ]
  edge [
    source 43
    target 342
  ]
  edge [
    source 43
    target 343
  ]
  edge [
    source 43
    target 344
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 345
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 346
  ]
  edge [
    source 46
    target 229
  ]
  edge [
    source 46
    target 56
  ]
  edge [
    source 46
    target 347
  ]
  edge [
    source 46
    target 234
  ]
  edge [
    source 46
    target 348
  ]
  edge [
    source 46
    target 349
  ]
  edge [
    source 46
    target 350
  ]
  edge [
    source 46
    target 351
  ]
  edge [
    source 46
    target 352
  ]
  edge [
    source 46
    target 353
  ]
  edge [
    source 46
    target 354
  ]
  edge [
    source 46
    target 355
  ]
  edge [
    source 46
    target 356
  ]
  edge [
    source 46
    target 357
  ]
  edge [
    source 46
    target 358
  ]
  edge [
    source 46
    target 359
  ]
  edge [
    source 46
    target 360
  ]
  edge [
    source 46
    target 361
  ]
  edge [
    source 46
    target 362
  ]
  edge [
    source 46
    target 363
  ]
  edge [
    source 46
    target 364
  ]
  edge [
    source 46
    target 365
  ]
  edge [
    source 46
    target 366
  ]
  edge [
    source 46
    target 367
  ]
  edge [
    source 46
    target 368
  ]
  edge [
    source 46
    target 369
  ]
  edge [
    source 46
    target 370
  ]
  edge [
    source 46
    target 371
  ]
  edge [
    source 46
    target 372
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 373
  ]
  edge [
    source 47
    target 374
  ]
  edge [
    source 47
    target 236
  ]
  edge [
    source 47
    target 359
  ]
  edge [
    source 47
    target 375
  ]
  edge [
    source 47
    target 229
  ]
  edge [
    source 47
    target 376
  ]
  edge [
    source 47
    target 231
  ]
  edge [
    source 47
    target 377
  ]
  edge [
    source 47
    target 378
  ]
  edge [
    source 47
    target 234
  ]
  edge [
    source 47
    target 379
  ]
  edge [
    source 47
    target 380
  ]
  edge [
    source 47
    target 381
  ]
  edge [
    source 47
    target 382
  ]
  edge [
    source 47
    target 383
  ]
  edge [
    source 47
    target 384
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 94
  ]
  edge [
    source 48
    target 385
  ]
  edge [
    source 48
    target 386
  ]
  edge [
    source 48
    target 117
  ]
  edge [
    source 48
    target 387
  ]
  edge [
    source 48
    target 388
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 389
  ]
  edge [
    source 49
    target 390
  ]
  edge [
    source 49
    target 391
  ]
  edge [
    source 49
    target 392
  ]
  edge [
    source 49
    target 393
  ]
  edge [
    source 49
    target 394
  ]
  edge [
    source 49
    target 395
  ]
  edge [
    source 49
    target 396
  ]
  edge [
    source 49
    target 397
  ]
  edge [
    source 49
    target 398
  ]
  edge [
    source 49
    target 399
  ]
  edge [
    source 49
    target 400
  ]
  edge [
    source 49
    target 401
  ]
  edge [
    source 49
    target 402
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 403
  ]
  edge [
    source 51
    target 404
  ]
  edge [
    source 51
    target 405
  ]
  edge [
    source 52
    target 87
  ]
  edge [
    source 52
    target 406
  ]
  edge [
    source 52
    target 407
  ]
  edge [
    source 52
    target 408
  ]
  edge [
    source 52
    target 409
  ]
  edge [
    source 52
    target 410
  ]
  edge [
    source 52
    target 78
  ]
  edge [
    source 52
    target 411
  ]
  edge [
    source 52
    target 412
  ]
  edge [
    source 52
    target 413
  ]
  edge [
    source 52
    target 414
  ]
  edge [
    source 52
    target 415
  ]
  edge [
    source 52
    target 416
  ]
  edge [
    source 52
    target 417
  ]
  edge [
    source 52
    target 418
  ]
  edge [
    source 52
    target 419
  ]
  edge [
    source 52
    target 420
  ]
  edge [
    source 52
    target 421
  ]
  edge [
    source 52
    target 422
  ]
  edge [
    source 52
    target 423
  ]
  edge [
    source 52
    target 424
  ]
  edge [
    source 52
    target 425
  ]
  edge [
    source 52
    target 426
  ]
  edge [
    source 52
    target 427
  ]
  edge [
    source 52
    target 428
  ]
  edge [
    source 52
    target 429
  ]
  edge [
    source 52
    target 430
  ]
  edge [
    source 431
    target 432
  ]
  edge [
    source 433
    target 434
  ]
]
