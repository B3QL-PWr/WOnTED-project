graph [
  maxDegree 49
  minDegree 1
  meanDegree 1.989247311827957
  density 0.010752688172043012
  graphCliqueNumber 2
  node [
    id 0
    label "przecie&#380;"
    origin "text"
  ]
  node [
    id 1
    label "marta"
    origin "text"
  ]
  node [
    id 2
    label "manowski"
    origin "text"
  ]
  node [
    id 3
    label "odkry&#263;"
    origin "text"
  ]
  node [
    id 4
    label "wykop"
    origin "text"
  ]
  node [
    id 5
    label "program"
    origin "text"
  ]
  node [
    id 6
    label "przegryw"
    origin "text"
  ]
  node [
    id 7
    label "szuka&#263;"
    origin "text"
  ]
  node [
    id 8
    label "&#380;ona"
    origin "text"
  ]
  node [
    id 9
    label "by&#263;"
    origin "text"
  ]
  node [
    id 10
    label "taki"
    origin "text"
  ]
  node [
    id 11
    label "hit"
    origin "text"
  ]
  node [
    id 12
    label "licencja"
    origin "text"
  ]
  node [
    id 13
    label "zagraniczny"
    origin "text"
  ]
  node [
    id 14
    label "edycja"
    origin "text"
  ]
  node [
    id 15
    label "pokry&#263;"
    origin "text"
  ]
  node [
    id 16
    label "zapotrzebowanie"
    origin "text"
  ]
  node [
    id 17
    label "finansowy"
    origin "text"
  ]
  node [
    id 18
    label "tytu&#322;"
    origin "text"
  ]
  node [
    id 19
    label "abonament"
    origin "text"
  ]
  node [
    id 20
    label "xddd"
    origin "text"
  ]
  node [
    id 21
    label "rolnikszukazony"
    origin "text"
  ]
  node [
    id 22
    label "pozna&#263;"
  ]
  node [
    id 23
    label "ukaza&#263;"
  ]
  node [
    id 24
    label "denounce"
  ]
  node [
    id 25
    label "podnie&#347;&#263;"
  ]
  node [
    id 26
    label "znale&#378;&#263;"
  ]
  node [
    id 27
    label "unwrap"
  ]
  node [
    id 28
    label "&#322;owca_talent&#243;w"
  ]
  node [
    id 29
    label "expose"
  ]
  node [
    id 30
    label "discover"
  ]
  node [
    id 31
    label "zsun&#261;&#263;"
  ]
  node [
    id 32
    label "zwr&#243;ci&#263;_uwag&#281;"
  ]
  node [
    id 33
    label "objawi&#263;"
  ]
  node [
    id 34
    label "poinformowa&#263;"
  ]
  node [
    id 35
    label "odwa&#322;"
  ]
  node [
    id 36
    label "chody"
  ]
  node [
    id 37
    label "grodzisko"
  ]
  node [
    id 38
    label "budowa"
  ]
  node [
    id 39
    label "kopniak"
  ]
  node [
    id 40
    label "wyrobisko"
  ]
  node [
    id 41
    label "zrzutowy"
  ]
  node [
    id 42
    label "szaniec"
  ]
  node [
    id 43
    label "odk&#322;ad"
  ]
  node [
    id 44
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 45
    label "spis"
  ]
  node [
    id 46
    label "odinstalowanie"
  ]
  node [
    id 47
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 48
    label "za&#322;o&#380;enie"
  ]
  node [
    id 49
    label "podstawa"
  ]
  node [
    id 50
    label "emitowanie"
  ]
  node [
    id 51
    label "odinstalowywanie"
  ]
  node [
    id 52
    label "instrukcja"
  ]
  node [
    id 53
    label "punkt"
  ]
  node [
    id 54
    label "teleferie"
  ]
  node [
    id 55
    label "emitowa&#263;"
  ]
  node [
    id 56
    label "wytw&#243;r"
  ]
  node [
    id 57
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 58
    label "sekcja_krytyczna"
  ]
  node [
    id 59
    label "oferta"
  ]
  node [
    id 60
    label "prezentowa&#263;"
  ]
  node [
    id 61
    label "blok"
  ]
  node [
    id 62
    label "podprogram"
  ]
  node [
    id 63
    label "tryb"
  ]
  node [
    id 64
    label "dzia&#322;"
  ]
  node [
    id 65
    label "broszura"
  ]
  node [
    id 66
    label "deklaracja"
  ]
  node [
    id 67
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 68
    label "struktura_organizacyjna"
  ]
  node [
    id 69
    label "zaprezentowanie"
  ]
  node [
    id 70
    label "informatyka"
  ]
  node [
    id 71
    label "booklet"
  ]
  node [
    id 72
    label "menu"
  ]
  node [
    id 73
    label "oprogramowanie"
  ]
  node [
    id 74
    label "instalowanie"
  ]
  node [
    id 75
    label "furkacja"
  ]
  node [
    id 76
    label "odinstalowa&#263;"
  ]
  node [
    id 77
    label "instalowa&#263;"
  ]
  node [
    id 78
    label "pirat"
  ]
  node [
    id 79
    label "zainstalowanie"
  ]
  node [
    id 80
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 81
    label "ogranicznik_referencyjny"
  ]
  node [
    id 82
    label "zainstalowa&#263;"
  ]
  node [
    id 83
    label "kana&#322;"
  ]
  node [
    id 84
    label "zaprezentowa&#263;"
  ]
  node [
    id 85
    label "interfejs"
  ]
  node [
    id 86
    label "odinstalowywa&#263;"
  ]
  node [
    id 87
    label "folder"
  ]
  node [
    id 88
    label "course_of_study"
  ]
  node [
    id 89
    label "ram&#243;wka"
  ]
  node [
    id 90
    label "prezentowanie"
  ]
  node [
    id 91
    label "okno"
  ]
  node [
    id 92
    label "nieudacznik"
  ]
  node [
    id 93
    label "przegraniec"
  ]
  node [
    id 94
    label "ask"
  ]
  node [
    id 95
    label "try"
  ]
  node [
    id 96
    label "&#322;azi&#263;"
  ]
  node [
    id 97
    label "sprawdza&#263;"
  ]
  node [
    id 98
    label "stara&#263;_si&#281;"
  ]
  node [
    id 99
    label "ma&#322;&#380;onek"
  ]
  node [
    id 100
    label "panna_m&#322;oda"
  ]
  node [
    id 101
    label "partnerka"
  ]
  node [
    id 102
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 103
    label "&#347;lubna"
  ]
  node [
    id 104
    label "kobita"
  ]
  node [
    id 105
    label "si&#281;ga&#263;"
  ]
  node [
    id 106
    label "trwa&#263;"
  ]
  node [
    id 107
    label "obecno&#347;&#263;"
  ]
  node [
    id 108
    label "stan"
  ]
  node [
    id 109
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 110
    label "stand"
  ]
  node [
    id 111
    label "mie&#263;_miejsce"
  ]
  node [
    id 112
    label "uczestniczy&#263;"
  ]
  node [
    id 113
    label "chodzi&#263;"
  ]
  node [
    id 114
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 115
    label "equal"
  ]
  node [
    id 116
    label "okre&#347;lony"
  ]
  node [
    id 117
    label "jaki&#347;"
  ]
  node [
    id 118
    label "sensacja"
  ]
  node [
    id 119
    label "popularny"
  ]
  node [
    id 120
    label "odkrycie"
  ]
  node [
    id 121
    label "nowina"
  ]
  node [
    id 122
    label "moda"
  ]
  node [
    id 123
    label "utw&#243;r"
  ]
  node [
    id 124
    label "prawo"
  ]
  node [
    id 125
    label "licencjonowa&#263;"
  ]
  node [
    id 126
    label "pozwolenie"
  ]
  node [
    id 127
    label "hodowla"
  ]
  node [
    id 128
    label "rasowy"
  ]
  node [
    id 129
    label "license"
  ]
  node [
    id 130
    label "zezwolenie"
  ]
  node [
    id 131
    label "za&#347;wiadczenie"
  ]
  node [
    id 132
    label "zagranicznie"
  ]
  node [
    id 133
    label "obcy"
  ]
  node [
    id 134
    label "impression"
  ]
  node [
    id 135
    label "odmiana"
  ]
  node [
    id 136
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 137
    label "notification"
  ]
  node [
    id 138
    label "cykl"
  ]
  node [
    id 139
    label "zmiana"
  ]
  node [
    id 140
    label "produkcja"
  ]
  node [
    id 141
    label "egzemplarz"
  ]
  node [
    id 142
    label "cover"
  ]
  node [
    id 143
    label "przykry&#263;"
  ]
  node [
    id 144
    label "zap&#322;aci&#263;"
  ]
  node [
    id 145
    label "zaj&#261;&#263;"
  ]
  node [
    id 146
    label "defray"
  ]
  node [
    id 147
    label "ustawi&#263;_si&#281;"
  ]
  node [
    id 148
    label "sheathing"
  ]
  node [
    id 149
    label "zap&#322;odni&#263;"
  ]
  node [
    id 150
    label "brood"
  ]
  node [
    id 151
    label "zaspokoi&#263;"
  ]
  node [
    id 152
    label "zamaskowa&#263;"
  ]
  node [
    id 153
    label "elastyczno&#347;&#263;_popytu"
  ]
  node [
    id 154
    label "krzywa_popytu"
  ]
  node [
    id 155
    label "popyt_zagregowany"
  ]
  node [
    id 156
    label "mechanizm_mno&#380;nikowy"
  ]
  node [
    id 157
    label "proces_ekonomiczny"
  ]
  node [
    id 158
    label "handel"
  ]
  node [
    id 159
    label "nawis_inflacyjny"
  ]
  node [
    id 160
    label "lepko&#347;&#263;_cen"
  ]
  node [
    id 161
    label "cena_r&#243;wnowagi_rynkowej"
  ]
  node [
    id 162
    label "czynnik_niecenowy"
  ]
  node [
    id 163
    label "mi&#281;dzybankowy"
  ]
  node [
    id 164
    label "finansowo"
  ]
  node [
    id 165
    label "fizyczny"
  ]
  node [
    id 166
    label "pozamaterialny"
  ]
  node [
    id 167
    label "materjalny"
  ]
  node [
    id 168
    label "podtytu&#322;"
  ]
  node [
    id 169
    label "debit"
  ]
  node [
    id 170
    label "szata_graficzna"
  ]
  node [
    id 171
    label "elevation"
  ]
  node [
    id 172
    label "wyda&#263;"
  ]
  node [
    id 173
    label "nadtytu&#322;"
  ]
  node [
    id 174
    label "tytulatura"
  ]
  node [
    id 175
    label "nazwa"
  ]
  node [
    id 176
    label "redaktor"
  ]
  node [
    id 177
    label "wydawa&#263;"
  ]
  node [
    id 178
    label "druk"
  ]
  node [
    id 179
    label "mianowaniec"
  ]
  node [
    id 180
    label "poster"
  ]
  node [
    id 181
    label "publikacja"
  ]
  node [
    id 182
    label "season"
  ]
  node [
    id 183
    label "przedp&#322;ata"
  ]
  node [
    id 184
    label "bloczek"
  ]
  node [
    id 185
    label "Marta"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 118
  ]
  edge [
    source 11
    target 119
  ]
  edge [
    source 11
    target 120
  ]
  edge [
    source 11
    target 121
  ]
  edge [
    source 11
    target 122
  ]
  edge [
    source 11
    target 123
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 124
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 12
    target 131
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 132
  ]
  edge [
    source 13
    target 133
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 134
  ]
  edge [
    source 14
    target 135
  ]
  edge [
    source 14
    target 136
  ]
  edge [
    source 14
    target 137
  ]
  edge [
    source 14
    target 138
  ]
  edge [
    source 14
    target 139
  ]
  edge [
    source 14
    target 140
  ]
  edge [
    source 14
    target 141
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 142
  ]
  edge [
    source 15
    target 143
  ]
  edge [
    source 15
    target 144
  ]
  edge [
    source 15
    target 145
  ]
  edge [
    source 15
    target 146
  ]
  edge [
    source 15
    target 147
  ]
  edge [
    source 15
    target 148
  ]
  edge [
    source 15
    target 149
  ]
  edge [
    source 15
    target 150
  ]
  edge [
    source 15
    target 151
  ]
  edge [
    source 15
    target 152
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 153
  ]
  edge [
    source 16
    target 154
  ]
  edge [
    source 16
    target 155
  ]
  edge [
    source 16
    target 156
  ]
  edge [
    source 16
    target 157
  ]
  edge [
    source 16
    target 158
  ]
  edge [
    source 16
    target 159
  ]
  edge [
    source 16
    target 160
  ]
  edge [
    source 16
    target 161
  ]
  edge [
    source 16
    target 162
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 163
  ]
  edge [
    source 17
    target 164
  ]
  edge [
    source 17
    target 165
  ]
  edge [
    source 17
    target 166
  ]
  edge [
    source 17
    target 167
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 168
  ]
  edge [
    source 18
    target 169
  ]
  edge [
    source 18
    target 170
  ]
  edge [
    source 18
    target 171
  ]
  edge [
    source 18
    target 172
  ]
  edge [
    source 18
    target 173
  ]
  edge [
    source 18
    target 174
  ]
  edge [
    source 18
    target 175
  ]
  edge [
    source 18
    target 176
  ]
  edge [
    source 18
    target 177
  ]
  edge [
    source 18
    target 178
  ]
  edge [
    source 18
    target 179
  ]
  edge [
    source 18
    target 180
  ]
  edge [
    source 18
    target 181
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 182
  ]
  edge [
    source 19
    target 183
  ]
  edge [
    source 19
    target 184
  ]
  edge [
    source 20
    target 21
  ]
]
