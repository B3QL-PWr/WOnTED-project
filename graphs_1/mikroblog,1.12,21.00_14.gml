graph [
  maxDegree 12
  minDegree 1
  meanDegree 1.8
  density 0.09473684210526316
  graphCliqueNumber 2
  node [
    id 0
    label "pami&#281;ta&#263;"
    origin "text"
  ]
  node [
    id 1
    label "afere"
    origin "text"
  ]
  node [
    id 2
    label "przeprosiny"
    origin "text"
  ]
  node [
    id 3
    label "ampm"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;ga&#263;"
  ]
  node [
    id 5
    label "zna&#263;"
  ]
  node [
    id 6
    label "troska&#263;_si&#281;"
  ]
  node [
    id 7
    label "zachowywa&#263;"
  ]
  node [
    id 8
    label "chowa&#263;"
  ]
  node [
    id 9
    label "think"
  ]
  node [
    id 10
    label "pilnowa&#263;"
  ]
  node [
    id 11
    label "robi&#263;"
  ]
  node [
    id 12
    label "recall"
  ]
  node [
    id 13
    label "echo"
  ]
  node [
    id 14
    label "take_care"
  ]
  node [
    id 15
    label "apology"
  ]
  node [
    id 16
    label "wypowied&#378;"
  ]
  node [
    id 17
    label "deprekacja"
  ]
  node [
    id 18
    label "XDDD"
  ]
  node [
    id 19
    label "lewactwo"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 18
    target 19
  ]
]
