graph [
  maxDegree 43
  minDegree 1
  meanDegree 2.045045045045045
  density 0.009253597488891607
  graphCliqueNumber 2
  node [
    id 0
    label "wszystko"
    origin "text"
  ]
  node [
    id 1
    label "jasny"
    origin "text"
  ]
  node [
    id 2
    label "jura"
    origin "text"
  ]
  node [
    id 3
    label "konkurs"
    origin "text"
  ]
  node [
    id 4
    label "globalnie"
    origin "text"
  ]
  node [
    id 5
    label "multimedialnie"
    origin "text"
  ]
  node [
    id 6
    label "og&#322;asza&#263;"
    origin "text"
  ]
  node [
    id 7
    label "lista"
    origin "text"
  ]
  node [
    id 8
    label "laureat"
    origin "text"
  ]
  node [
    id 9
    label "zdecydowa&#263;"
    origin "text"
  ]
  node [
    id 10
    label "przyznanie"
    origin "text"
  ]
  node [
    id 11
    label "trzy"
    origin "text"
  ]
  node [
    id 12
    label "r&#243;wnorz&#281;dny"
    origin "text"
  ]
  node [
    id 13
    label "nagroda"
    origin "text"
  ]
  node [
    id 14
    label "redakcja"
    origin "text"
  ]
  node [
    id 15
    label "qmam&#243;w"
    origin "text"
  ]
  node [
    id 16
    label "szko&#322;a"
    origin "text"
  ]
  node [
    id 17
    label "bez"
    origin "text"
  ]
  node [
    id 18
    label "granica"
    origin "text"
  ]
  node [
    id 19
    label "facto"
    origin "text"
  ]
  node [
    id 20
    label "czysty"
    origin "text"
  ]
  node [
    id 21
    label "las"
    origin "text"
  ]
  node [
    id 22
    label "lock"
  ]
  node [
    id 23
    label "absolut"
  ]
  node [
    id 24
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 25
    label "szczery"
  ]
  node [
    id 26
    label "o&#347;wietlenie"
  ]
  node [
    id 27
    label "klarowny"
  ]
  node [
    id 28
    label "przytomny"
  ]
  node [
    id 29
    label "jednoznaczny"
  ]
  node [
    id 30
    label "pogodny"
  ]
  node [
    id 31
    label "o&#347;wietlanie"
  ]
  node [
    id 32
    label "bia&#322;y"
  ]
  node [
    id 33
    label "niezm&#261;cony"
  ]
  node [
    id 34
    label "zrozumia&#322;y"
  ]
  node [
    id 35
    label "dobry"
  ]
  node [
    id 36
    label "jasno"
  ]
  node [
    id 37
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 38
    label "formacja_geologiczna"
  ]
  node [
    id 39
    label "jura_&#347;rodkowa"
  ]
  node [
    id 40
    label "dogger"
  ]
  node [
    id 41
    label "jura_p&#243;&#378;na"
  ]
  node [
    id 42
    label "era_mezozoiczna"
  ]
  node [
    id 43
    label "plezjozaur"
  ]
  node [
    id 44
    label "euoplocefal"
  ]
  node [
    id 45
    label "jura_wczesna"
  ]
  node [
    id 46
    label "eliminacje"
  ]
  node [
    id 47
    label "Interwizja"
  ]
  node [
    id 48
    label "emulation"
  ]
  node [
    id 49
    label "impreza"
  ]
  node [
    id 50
    label "casting"
  ]
  node [
    id 51
    label "Eurowizja"
  ]
  node [
    id 52
    label "nab&#243;r"
  ]
  node [
    id 53
    label "ca&#322;o&#347;ciowo"
  ]
  node [
    id 54
    label "&#347;wiatowy"
  ]
  node [
    id 55
    label "internationally"
  ]
  node [
    id 56
    label "og&#243;lnie"
  ]
  node [
    id 57
    label "globalny"
  ]
  node [
    id 58
    label "multimedialny"
  ]
  node [
    id 59
    label "announce"
  ]
  node [
    id 60
    label "publikowa&#263;"
  ]
  node [
    id 61
    label "obwo&#322;ywa&#263;"
  ]
  node [
    id 62
    label "post"
  ]
  node [
    id 63
    label "podawa&#263;"
  ]
  node [
    id 64
    label "wyliczanka"
  ]
  node [
    id 65
    label "catalog"
  ]
  node [
    id 66
    label "stock"
  ]
  node [
    id 67
    label "figurowa&#263;"
  ]
  node [
    id 68
    label "zbi&#243;r"
  ]
  node [
    id 69
    label "book"
  ]
  node [
    id 70
    label "pozycja"
  ]
  node [
    id 71
    label "tekst"
  ]
  node [
    id 72
    label "sumariusz"
  ]
  node [
    id 73
    label "zdobywca"
  ]
  node [
    id 74
    label "sta&#263;_si&#281;"
  ]
  node [
    id 75
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 76
    label "podj&#261;&#263;"
  ]
  node [
    id 77
    label "determine"
  ]
  node [
    id 78
    label "decide"
  ]
  node [
    id 79
    label "zrobi&#263;"
  ]
  node [
    id 80
    label "recognition"
  ]
  node [
    id 81
    label "danie"
  ]
  node [
    id 82
    label "stwierdzenie"
  ]
  node [
    id 83
    label "confession"
  ]
  node [
    id 84
    label "oznajmienie"
  ]
  node [
    id 85
    label "jednakowy"
  ]
  node [
    id 86
    label "r&#243;wnorz&#281;dnie"
  ]
  node [
    id 87
    label "return"
  ]
  node [
    id 88
    label "konsekwencja"
  ]
  node [
    id 89
    label "oskar"
  ]
  node [
    id 90
    label "wyr&#243;&#380;nienie"
  ]
  node [
    id 91
    label "telewizja"
  ]
  node [
    id 92
    label "redaction"
  ]
  node [
    id 93
    label "obr&#243;bka"
  ]
  node [
    id 94
    label "radio"
  ]
  node [
    id 95
    label "wydawnictwo"
  ]
  node [
    id 96
    label "zesp&#243;&#322;"
  ]
  node [
    id 97
    label "redaktor"
  ]
  node [
    id 98
    label "siedziba"
  ]
  node [
    id 99
    label "composition"
  ]
  node [
    id 100
    label "Mickiewicz"
  ]
  node [
    id 101
    label "czas"
  ]
  node [
    id 102
    label "szkolenie"
  ]
  node [
    id 103
    label "przepisa&#263;"
  ]
  node [
    id 104
    label "lesson"
  ]
  node [
    id 105
    label "grupa"
  ]
  node [
    id 106
    label "praktyka"
  ]
  node [
    id 107
    label "metoda"
  ]
  node [
    id 108
    label "niepokalanki"
  ]
  node [
    id 109
    label "kara"
  ]
  node [
    id 110
    label "zda&#263;"
  ]
  node [
    id 111
    label "form"
  ]
  node [
    id 112
    label "kwalifikacje"
  ]
  node [
    id 113
    label "system"
  ]
  node [
    id 114
    label "przepisanie"
  ]
  node [
    id 115
    label "sztuba"
  ]
  node [
    id 116
    label "wiedza"
  ]
  node [
    id 117
    label "stopek"
  ]
  node [
    id 118
    label "school"
  ]
  node [
    id 119
    label "absolwent"
  ]
  node [
    id 120
    label "urszulanki"
  ]
  node [
    id 121
    label "gabinet"
  ]
  node [
    id 122
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 123
    label "ideologia"
  ]
  node [
    id 124
    label "lekcja"
  ]
  node [
    id 125
    label "muzyka"
  ]
  node [
    id 126
    label "podr&#281;cznik"
  ]
  node [
    id 127
    label "zdanie"
  ]
  node [
    id 128
    label "sekretariat"
  ]
  node [
    id 129
    label "nauka"
  ]
  node [
    id 130
    label "do&#347;wiadczenie"
  ]
  node [
    id 131
    label "tablica"
  ]
  node [
    id 132
    label "teren_szko&#322;y"
  ]
  node [
    id 133
    label "instytucja"
  ]
  node [
    id 134
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 135
    label "skolaryzacja"
  ]
  node [
    id 136
    label "&#322;awa_szkolna"
  ]
  node [
    id 137
    label "klasa"
  ]
  node [
    id 138
    label "ki&#347;&#263;"
  ]
  node [
    id 139
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 140
    label "krzew"
  ]
  node [
    id 141
    label "pi&#380;maczkowate"
  ]
  node [
    id 142
    label "pestkowiec"
  ]
  node [
    id 143
    label "kwiat"
  ]
  node [
    id 144
    label "owoc"
  ]
  node [
    id 145
    label "oliwkowate"
  ]
  node [
    id 146
    label "ro&#347;lina"
  ]
  node [
    id 147
    label "hy&#263;ka"
  ]
  node [
    id 148
    label "lilac"
  ]
  node [
    id 149
    label "delfinidyna"
  ]
  node [
    id 150
    label "zakres"
  ]
  node [
    id 151
    label "Ural"
  ]
  node [
    id 152
    label "koniec"
  ]
  node [
    id 153
    label "kres"
  ]
  node [
    id 154
    label "granice"
  ]
  node [
    id 155
    label "granica_pa&#324;stwa"
  ]
  node [
    id 156
    label "pu&#322;ap"
  ]
  node [
    id 157
    label "frontier"
  ]
  node [
    id 158
    label "end"
  ]
  node [
    id 159
    label "miara"
  ]
  node [
    id 160
    label "poj&#281;cie"
  ]
  node [
    id 161
    label "przej&#347;cie"
  ]
  node [
    id 162
    label "sklarowanie"
  ]
  node [
    id 163
    label "cnotliwy"
  ]
  node [
    id 164
    label "umycie"
  ]
  node [
    id 165
    label "wolny"
  ]
  node [
    id 166
    label "czysto"
  ]
  node [
    id 167
    label "prawdziwy"
  ]
  node [
    id 168
    label "ca&#322;y"
  ]
  node [
    id 169
    label "bezpieczny"
  ]
  node [
    id 170
    label "klarowanie"
  ]
  node [
    id 171
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 172
    label "do_czysta"
  ]
  node [
    id 173
    label "bezchmurny"
  ]
  node [
    id 174
    label "wspinaczka"
  ]
  node [
    id 175
    label "przyjemny"
  ]
  node [
    id 176
    label "udany"
  ]
  node [
    id 177
    label "zdrowy"
  ]
  node [
    id 178
    label "mycie"
  ]
  node [
    id 179
    label "klarowanie_si&#281;"
  ]
  node [
    id 180
    label "schludny"
  ]
  node [
    id 181
    label "orze&#378;wiaj&#261;cy"
  ]
  node [
    id 182
    label "ostry"
  ]
  node [
    id 183
    label "nieemisyjny"
  ]
  node [
    id 184
    label "ewidentny"
  ]
  node [
    id 185
    label "dopuszczalny"
  ]
  node [
    id 186
    label "jednolity"
  ]
  node [
    id 187
    label "wyczyszczenie_si&#281;"
  ]
  node [
    id 188
    label "legalny"
  ]
  node [
    id 189
    label "doskona&#322;y"
  ]
  node [
    id 190
    label "wyklarowanie_si&#281;"
  ]
  node [
    id 191
    label "prze&#378;roczy"
  ]
  node [
    id 192
    label "przezroczy&#347;cie"
  ]
  node [
    id 193
    label "bezb&#322;&#281;dny"
  ]
  node [
    id 194
    label "kompletny"
  ]
  node [
    id 195
    label "uczciwy"
  ]
  node [
    id 196
    label "czyszczenie_si&#281;"
  ]
  node [
    id 197
    label "pewny"
  ]
  node [
    id 198
    label "porz&#261;dny"
  ]
  node [
    id 199
    label "nieodrodny"
  ]
  node [
    id 200
    label "ekologiczny"
  ]
  node [
    id 201
    label "chody"
  ]
  node [
    id 202
    label "dno_lasu"
  ]
  node [
    id 203
    label "obr&#281;b"
  ]
  node [
    id 204
    label "&#347;ci&#243;&#322;ka"
  ]
  node [
    id 205
    label "podszyt"
  ]
  node [
    id 206
    label "rewir"
  ]
  node [
    id 207
    label "podrost"
  ]
  node [
    id 208
    label "teren"
  ]
  node [
    id 209
    label "le&#347;nictwo"
  ]
  node [
    id 210
    label "wykarczowanie"
  ]
  node [
    id 211
    label "runo"
  ]
  node [
    id 212
    label "teren_le&#347;ny"
  ]
  node [
    id 213
    label "wykarczowa&#263;"
  ]
  node [
    id 214
    label "mn&#243;stwo"
  ]
  node [
    id 215
    label "nadle&#347;nictwo"
  ]
  node [
    id 216
    label "formacja_ro&#347;linna"
  ]
  node [
    id 217
    label "zalesienie"
  ]
  node [
    id 218
    label "karczowa&#263;"
  ]
  node [
    id 219
    label "wiatro&#322;om"
  ]
  node [
    id 220
    label "karczowanie"
  ]
  node [
    id 221
    label "driada"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 14
  ]
  edge [
    source 11
    target 15
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 85
  ]
  edge [
    source 12
    target 86
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 87
  ]
  edge [
    source 13
    target 88
  ]
  edge [
    source 13
    target 89
  ]
  edge [
    source 13
    target 90
  ]
  edge [
    source 13
    target 18
  ]
  edge [
    source 14
    target 91
  ]
  edge [
    source 14
    target 92
  ]
  edge [
    source 14
    target 93
  ]
  edge [
    source 14
    target 94
  ]
  edge [
    source 14
    target 95
  ]
  edge [
    source 14
    target 96
  ]
  edge [
    source 14
    target 97
  ]
  edge [
    source 14
    target 98
  ]
  edge [
    source 14
    target 99
  ]
  edge [
    source 14
    target 71
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 100
  ]
  edge [
    source 16
    target 101
  ]
  edge [
    source 16
    target 102
  ]
  edge [
    source 16
    target 103
  ]
  edge [
    source 16
    target 104
  ]
  edge [
    source 16
    target 105
  ]
  edge [
    source 16
    target 106
  ]
  edge [
    source 16
    target 107
  ]
  edge [
    source 16
    target 108
  ]
  edge [
    source 16
    target 109
  ]
  edge [
    source 16
    target 110
  ]
  edge [
    source 16
    target 111
  ]
  edge [
    source 16
    target 112
  ]
  edge [
    source 16
    target 113
  ]
  edge [
    source 16
    target 114
  ]
  edge [
    source 16
    target 115
  ]
  edge [
    source 16
    target 116
  ]
  edge [
    source 16
    target 117
  ]
  edge [
    source 16
    target 118
  ]
  edge [
    source 16
    target 119
  ]
  edge [
    source 16
    target 120
  ]
  edge [
    source 16
    target 121
  ]
  edge [
    source 16
    target 122
  ]
  edge [
    source 16
    target 123
  ]
  edge [
    source 16
    target 124
  ]
  edge [
    source 16
    target 125
  ]
  edge [
    source 16
    target 126
  ]
  edge [
    source 16
    target 127
  ]
  edge [
    source 16
    target 98
  ]
  edge [
    source 16
    target 128
  ]
  edge [
    source 16
    target 129
  ]
  edge [
    source 16
    target 130
  ]
  edge [
    source 16
    target 131
  ]
  edge [
    source 16
    target 132
  ]
  edge [
    source 16
    target 133
  ]
  edge [
    source 16
    target 134
  ]
  edge [
    source 16
    target 135
  ]
  edge [
    source 16
    target 136
  ]
  edge [
    source 16
    target 137
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 138
  ]
  edge [
    source 17
    target 139
  ]
  edge [
    source 17
    target 140
  ]
  edge [
    source 17
    target 141
  ]
  edge [
    source 17
    target 142
  ]
  edge [
    source 17
    target 143
  ]
  edge [
    source 17
    target 144
  ]
  edge [
    source 17
    target 145
  ]
  edge [
    source 17
    target 146
  ]
  edge [
    source 17
    target 147
  ]
  edge [
    source 17
    target 148
  ]
  edge [
    source 17
    target 149
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 150
  ]
  edge [
    source 18
    target 151
  ]
  edge [
    source 18
    target 152
  ]
  edge [
    source 18
    target 153
  ]
  edge [
    source 18
    target 154
  ]
  edge [
    source 18
    target 155
  ]
  edge [
    source 18
    target 156
  ]
  edge [
    source 18
    target 157
  ]
  edge [
    source 18
    target 158
  ]
  edge [
    source 18
    target 159
  ]
  edge [
    source 18
    target 160
  ]
  edge [
    source 18
    target 161
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 162
  ]
  edge [
    source 20
    target 163
  ]
  edge [
    source 20
    target 164
  ]
  edge [
    source 20
    target 165
  ]
  edge [
    source 20
    target 27
  ]
  edge [
    source 20
    target 166
  ]
  edge [
    source 20
    target 167
  ]
  edge [
    source 20
    target 168
  ]
  edge [
    source 20
    target 169
  ]
  edge [
    source 20
    target 170
  ]
  edge [
    source 20
    target 171
  ]
  edge [
    source 20
    target 172
  ]
  edge [
    source 20
    target 173
  ]
  edge [
    source 20
    target 174
  ]
  edge [
    source 20
    target 175
  ]
  edge [
    source 20
    target 176
  ]
  edge [
    source 20
    target 177
  ]
  edge [
    source 20
    target 178
  ]
  edge [
    source 20
    target 179
  ]
  edge [
    source 20
    target 180
  ]
  edge [
    source 20
    target 181
  ]
  edge [
    source 20
    target 182
  ]
  edge [
    source 20
    target 183
  ]
  edge [
    source 20
    target 184
  ]
  edge [
    source 20
    target 185
  ]
  edge [
    source 20
    target 186
  ]
  edge [
    source 20
    target 187
  ]
  edge [
    source 20
    target 188
  ]
  edge [
    source 20
    target 189
  ]
  edge [
    source 20
    target 190
  ]
  edge [
    source 20
    target 191
  ]
  edge [
    source 20
    target 192
  ]
  edge [
    source 20
    target 193
  ]
  edge [
    source 20
    target 194
  ]
  edge [
    source 20
    target 195
  ]
  edge [
    source 20
    target 196
  ]
  edge [
    source 20
    target 197
  ]
  edge [
    source 20
    target 198
  ]
  edge [
    source 20
    target 35
  ]
  edge [
    source 20
    target 199
  ]
  edge [
    source 20
    target 200
  ]
  edge [
    source 21
    target 201
  ]
  edge [
    source 21
    target 202
  ]
  edge [
    source 21
    target 203
  ]
  edge [
    source 21
    target 204
  ]
  edge [
    source 21
    target 205
  ]
  edge [
    source 21
    target 206
  ]
  edge [
    source 21
    target 207
  ]
  edge [
    source 21
    target 208
  ]
  edge [
    source 21
    target 209
  ]
  edge [
    source 21
    target 210
  ]
  edge [
    source 21
    target 211
  ]
  edge [
    source 21
    target 212
  ]
  edge [
    source 21
    target 213
  ]
  edge [
    source 21
    target 214
  ]
  edge [
    source 21
    target 215
  ]
  edge [
    source 21
    target 216
  ]
  edge [
    source 21
    target 217
  ]
  edge [
    source 21
    target 218
  ]
  edge [
    source 21
    target 219
  ]
  edge [
    source 21
    target 220
  ]
  edge [
    source 21
    target 221
  ]
]
