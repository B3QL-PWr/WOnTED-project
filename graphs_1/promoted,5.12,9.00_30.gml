graph [
  maxDegree 25
  minDegree 1
  meanDegree 2.062111801242236
  density 0.012888198757763975
  graphCliqueNumber 4
  node [
    id 0
    label "rodzic"
    origin "text"
  ]
  node [
    id 1
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 2
    label "sprzeciwia&#263;"
    origin "text"
  ]
  node [
    id 3
    label "si&#281;"
    origin "text"
  ]
  node [
    id 4
    label "obowi&#261;zkowy"
    origin "text"
  ]
  node [
    id 5
    label "szczepienie"
    origin "text"
  ]
  node [
    id 6
    label "ochronny"
    origin "text"
  ]
  node [
    id 7
    label "dziecko"
    origin "text"
  ]
  node [
    id 8
    label "powinny"
    origin "text"
  ]
  node [
    id 9
    label "p&#322;aci&#263;"
    origin "text"
  ]
  node [
    id 10
    label "wysoki"
    origin "text"
  ]
  node [
    id 11
    label "sk&#322;adka"
    origin "text"
  ]
  node [
    id 12
    label "zdrowotny"
    origin "text"
  ]
  node [
    id 13
    label "postulowa&#263;"
    origin "text"
  ]
  node [
    id 14
    label "lekarz"
    origin "text"
  ]
  node [
    id 15
    label "prawnik"
    origin "text"
  ]
  node [
    id 16
    label "zrzeszony"
    origin "text"
  ]
  node [
    id 17
    label "polski"
    origin "text"
  ]
  node [
    id 18
    label "towarzystwo"
    origin "text"
  ]
  node [
    id 19
    label "prawa"
    origin "text"
  ]
  node [
    id 20
    label "medyczny"
    origin "text"
  ]
  node [
    id 21
    label "opiekun"
  ]
  node [
    id 22
    label "wapniak"
  ]
  node [
    id 23
    label "rodzic_chrzestny"
  ]
  node [
    id 24
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 25
    label "rodzice"
  ]
  node [
    id 26
    label "konieczny"
  ]
  node [
    id 27
    label "porz&#261;dny"
  ]
  node [
    id 28
    label "obligatoryjnie"
  ]
  node [
    id 29
    label "obbligato"
  ]
  node [
    id 30
    label "kursowy"
  ]
  node [
    id 31
    label "wariolacja"
  ]
  node [
    id 32
    label "immunizacja"
  ]
  node [
    id 33
    label "zabieg"
  ]
  node [
    id 34
    label "uodpornianie"
  ]
  node [
    id 35
    label "inoculation"
  ]
  node [
    id 36
    label "uszlachetnianie"
  ]
  node [
    id 37
    label "uprawianie"
  ]
  node [
    id 38
    label "metoda"
  ]
  node [
    id 39
    label "immunizacja_czynna"
  ]
  node [
    id 40
    label "ochronnie"
  ]
  node [
    id 41
    label "cz&#322;owiek"
  ]
  node [
    id 42
    label "potomstwo"
  ]
  node [
    id 43
    label "organizm"
  ]
  node [
    id 44
    label "sraluch"
  ]
  node [
    id 45
    label "utulanie"
  ]
  node [
    id 46
    label "pediatra"
  ]
  node [
    id 47
    label "dzieciarnia"
  ]
  node [
    id 48
    label "m&#322;odziak"
  ]
  node [
    id 49
    label "dzieciak"
  ]
  node [
    id 50
    label "utula&#263;"
  ]
  node [
    id 51
    label "potomek"
  ]
  node [
    id 52
    label "pedofil"
  ]
  node [
    id 53
    label "entliczek-pentliczek"
  ]
  node [
    id 54
    label "m&#322;odzik"
  ]
  node [
    id 55
    label "cz&#322;owieczek"
  ]
  node [
    id 56
    label "zwierz&#281;"
  ]
  node [
    id 57
    label "niepe&#322;noletni"
  ]
  node [
    id 58
    label "fledgling"
  ]
  node [
    id 59
    label "utuli&#263;"
  ]
  node [
    id 60
    label "utulenie"
  ]
  node [
    id 61
    label "nale&#380;ny"
  ]
  node [
    id 62
    label "buli&#263;"
  ]
  node [
    id 63
    label "osi&#261;ga&#263;"
  ]
  node [
    id 64
    label "give"
  ]
  node [
    id 65
    label "wydawa&#263;"
  ]
  node [
    id 66
    label "pay"
  ]
  node [
    id 67
    label "warto&#347;ciowy"
  ]
  node [
    id 68
    label "du&#380;y"
  ]
  node [
    id 69
    label "wysoce"
  ]
  node [
    id 70
    label "daleki"
  ]
  node [
    id 71
    label "znaczny"
  ]
  node [
    id 72
    label "wysoko"
  ]
  node [
    id 73
    label "szczytnie"
  ]
  node [
    id 74
    label "wznios&#322;y"
  ]
  node [
    id 75
    label "wyrafinowany"
  ]
  node [
    id 76
    label "z_wysoka"
  ]
  node [
    id 77
    label "chwalebny"
  ]
  node [
    id 78
    label "uprzywilejowany"
  ]
  node [
    id 79
    label "niepo&#347;ledni"
  ]
  node [
    id 80
    label "arkusz"
  ]
  node [
    id 81
    label "z&#322;o&#380;y&#263;_si&#281;"
  ]
  node [
    id 82
    label "zbi&#243;rka"
  ]
  node [
    id 83
    label "&#243;semka"
  ]
  node [
    id 84
    label "sk&#322;ada&#263;_si&#281;"
  ]
  node [
    id 85
    label "czw&#243;rka"
  ]
  node [
    id 86
    label "nale&#380;no&#347;&#263;"
  ]
  node [
    id 87
    label "zdrowy"
  ]
  node [
    id 88
    label "dobry"
  ]
  node [
    id 89
    label "prozdrowotny"
  ]
  node [
    id 90
    label "zdrowotnie"
  ]
  node [
    id 91
    label "prosi&#263;"
  ]
  node [
    id 92
    label "postulate"
  ]
  node [
    id 93
    label "zbada&#263;"
  ]
  node [
    id 94
    label "medyk"
  ]
  node [
    id 95
    label "lekarze"
  ]
  node [
    id 96
    label "pracownik"
  ]
  node [
    id 97
    label "eskulap"
  ]
  node [
    id 98
    label "Hipokrates"
  ]
  node [
    id 99
    label "Mesmer"
  ]
  node [
    id 100
    label "Galen"
  ]
  node [
    id 101
    label "dokt&#243;r"
  ]
  node [
    id 102
    label "prawnicy"
  ]
  node [
    id 103
    label "Machiavelli"
  ]
  node [
    id 104
    label "specjalista"
  ]
  node [
    id 105
    label "aplikant"
  ]
  node [
    id 106
    label "student"
  ]
  node [
    id 107
    label "jurysta"
  ]
  node [
    id 108
    label "cz&#322;onek"
  ]
  node [
    id 109
    label "stowarzyszenie"
  ]
  node [
    id 110
    label "lacki"
  ]
  node [
    id 111
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 112
    label "przedmiot"
  ]
  node [
    id 113
    label "sztajer"
  ]
  node [
    id 114
    label "drabant"
  ]
  node [
    id 115
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 116
    label "polak"
  ]
  node [
    id 117
    label "pierogi_ruskie"
  ]
  node [
    id 118
    label "krakowiak"
  ]
  node [
    id 119
    label "Polish"
  ]
  node [
    id 120
    label "j&#281;zyk"
  ]
  node [
    id 121
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 122
    label "oberek"
  ]
  node [
    id 123
    label "po_polsku"
  ]
  node [
    id 124
    label "mazur"
  ]
  node [
    id 125
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 126
    label "chodzony"
  ]
  node [
    id 127
    label "skoczny"
  ]
  node [
    id 128
    label "ryba_po_grecku"
  ]
  node [
    id 129
    label "goniony"
  ]
  node [
    id 130
    label "polsko"
  ]
  node [
    id 131
    label "Ku&#378;nica_Ko&#322;&#322;&#261;tajowska"
  ]
  node [
    id 132
    label "obecno&#347;&#263;"
  ]
  node [
    id 133
    label "Krakowska_Kongregacja_Kupiecka"
  ]
  node [
    id 134
    label "organizacja"
  ]
  node [
    id 135
    label "Eleusis"
  ]
  node [
    id 136
    label "asystencja"
  ]
  node [
    id 137
    label "Rzeczpospolita_babi&#324;ska"
  ]
  node [
    id 138
    label "Polskie_Towarzystwo_Turystyczno-Krajoznawcze"
  ]
  node [
    id 139
    label "grupa"
  ]
  node [
    id 140
    label "fabianie"
  ]
  node [
    id 141
    label "Chewra_Kadisza"
  ]
  node [
    id 142
    label "grono"
  ]
  node [
    id 143
    label "umowa_stowarzyszeniowa"
  ]
  node [
    id 144
    label "wi&#281;&#378;"
  ]
  node [
    id 145
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 146
    label "partnership"
  ]
  node [
    id 147
    label "Monar"
  ]
  node [
    id 148
    label "Rotary_International"
  ]
  node [
    id 149
    label "specjalny"
  ]
  node [
    id 150
    label "praktyczny"
  ]
  node [
    id 151
    label "paramedyczny"
  ]
  node [
    id 152
    label "zgodny"
  ]
  node [
    id 153
    label "profilowy"
  ]
  node [
    id 154
    label "leczniczy"
  ]
  node [
    id 155
    label "lekarsko"
  ]
  node [
    id 156
    label "bia&#322;y"
  ]
  node [
    id 157
    label "medycznie"
  ]
  node [
    id 158
    label "specjalistyczny"
  ]
  node [
    id 159
    label "og&#243;lnopolski"
  ]
  node [
    id 160
    label "kongres"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 10
    target 69
  ]
  edge [
    source 10
    target 70
  ]
  edge [
    source 10
    target 71
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 80
  ]
  edge [
    source 11
    target 81
  ]
  edge [
    source 11
    target 82
  ]
  edge [
    source 11
    target 83
  ]
  edge [
    source 11
    target 84
  ]
  edge [
    source 11
    target 85
  ]
  edge [
    source 11
    target 86
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 12
    target 89
  ]
  edge [
    source 12
    target 90
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 91
  ]
  edge [
    source 13
    target 92
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 93
  ]
  edge [
    source 14
    target 94
  ]
  edge [
    source 14
    target 95
  ]
  edge [
    source 14
    target 96
  ]
  edge [
    source 14
    target 97
  ]
  edge [
    source 14
    target 98
  ]
  edge [
    source 14
    target 99
  ]
  edge [
    source 14
    target 100
  ]
  edge [
    source 14
    target 101
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 102
  ]
  edge [
    source 15
    target 103
  ]
  edge [
    source 15
    target 104
  ]
  edge [
    source 15
    target 105
  ]
  edge [
    source 15
    target 106
  ]
  edge [
    source 15
    target 107
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 108
  ]
  edge [
    source 16
    target 109
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 110
  ]
  edge [
    source 17
    target 111
  ]
  edge [
    source 17
    target 112
  ]
  edge [
    source 17
    target 113
  ]
  edge [
    source 17
    target 114
  ]
  edge [
    source 17
    target 115
  ]
  edge [
    source 17
    target 116
  ]
  edge [
    source 17
    target 117
  ]
  edge [
    source 17
    target 118
  ]
  edge [
    source 17
    target 119
  ]
  edge [
    source 17
    target 120
  ]
  edge [
    source 17
    target 121
  ]
  edge [
    source 17
    target 122
  ]
  edge [
    source 17
    target 123
  ]
  edge [
    source 17
    target 124
  ]
  edge [
    source 17
    target 125
  ]
  edge [
    source 17
    target 126
  ]
  edge [
    source 17
    target 127
  ]
  edge [
    source 17
    target 128
  ]
  edge [
    source 17
    target 129
  ]
  edge [
    source 17
    target 130
  ]
  edge [
    source 17
    target 19
  ]
  edge [
    source 17
    target 20
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 131
  ]
  edge [
    source 18
    target 132
  ]
  edge [
    source 18
    target 133
  ]
  edge [
    source 18
    target 134
  ]
  edge [
    source 18
    target 135
  ]
  edge [
    source 18
    target 136
  ]
  edge [
    source 18
    target 137
  ]
  edge [
    source 18
    target 138
  ]
  edge [
    source 18
    target 139
  ]
  edge [
    source 18
    target 140
  ]
  edge [
    source 18
    target 141
  ]
  edge [
    source 18
    target 142
  ]
  edge [
    source 18
    target 143
  ]
  edge [
    source 18
    target 144
  ]
  edge [
    source 18
    target 145
  ]
  edge [
    source 18
    target 146
  ]
  edge [
    source 18
    target 147
  ]
  edge [
    source 18
    target 148
  ]
  edge [
    source 18
    target 20
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 159
  ]
  edge [
    source 19
    target 160
  ]
  edge [
    source 20
    target 149
  ]
  edge [
    source 20
    target 150
  ]
  edge [
    source 20
    target 151
  ]
  edge [
    source 20
    target 152
  ]
  edge [
    source 20
    target 153
  ]
  edge [
    source 20
    target 154
  ]
  edge [
    source 20
    target 155
  ]
  edge [
    source 20
    target 156
  ]
  edge [
    source 20
    target 157
  ]
  edge [
    source 20
    target 158
  ]
  edge [
    source 20
    target 159
  ]
  edge [
    source 20
    target 160
  ]
  edge [
    source 159
    target 160
  ]
]
