graph [
  maxDegree 4
  minDegree 0
  meanDegree 1.7777777777777777
  density 0.10457516339869281
  graphCliqueNumber 5
  node [
    id 0
    label "tyzenhauz"
    origin "text"
  ]
  node [
    id 1
    label "mazepa"
  ]
  node [
    id 2
    label "tragedia"
  ]
  node [
    id 3
    label "wyspa"
  ]
  node [
    id 4
    label "pi&#281;&#263;"
  ]
  node [
    id 5
    label "akta"
  ]
  node [
    id 6
    label "Juliusz"
  ]
  node [
    id 7
    label "s&#322;owacki"
  ]
  node [
    id 8
    label "kawaler"
  ]
  node [
    id 9
    label "mieczowy"
  ]
  node [
    id 10
    label "Adam"
  ]
  node [
    id 11
    label "Mickiewicz"
  ]
  node [
    id 12
    label "pan"
  ]
  node [
    id 13
    label "Tadeusz"
  ]
  node [
    id 14
    label "Kasper"
  ]
  node [
    id 15
    label "Tyzenhaus"
  ]
  node [
    id 16
    label "Antoni"
  ]
  node [
    id 17
    label "Tyzenhauz"
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 16
    target 17
  ]
]
