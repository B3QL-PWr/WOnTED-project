graph [
  maxDegree 12
  minDegree 1
  meanDegree 2
  density 0.037037037037037035
  graphCliqueNumber 2
  node [
    id 0
    label "nowy"
    origin "text"
  ]
  node [
    id 1
    label "bardzo"
    origin "text"
  ]
  node [
    id 2
    label "ciekawy"
    origin "text"
  ]
  node [
    id 3
    label "spos&#243;b"
    origin "text"
  ]
  node [
    id 4
    label "unieruchomi&#263;"
    origin "text"
  ]
  node [
    id 5
    label "pirat"
    origin "text"
  ]
  node [
    id 6
    label "morski"
    origin "text"
  ]
  node [
    id 7
    label "cz&#322;owiek"
  ]
  node [
    id 8
    label "nowotny"
  ]
  node [
    id 9
    label "drugi"
  ]
  node [
    id 10
    label "kolejny"
  ]
  node [
    id 11
    label "bie&#380;&#261;cy"
  ]
  node [
    id 12
    label "nowo"
  ]
  node [
    id 13
    label "narybek"
  ]
  node [
    id 14
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 15
    label "obcy"
  ]
  node [
    id 16
    label "w_chuj"
  ]
  node [
    id 17
    label "swoisty"
  ]
  node [
    id 18
    label "interesowanie"
  ]
  node [
    id 19
    label "nietuzinkowy"
  ]
  node [
    id 20
    label "ciekawie"
  ]
  node [
    id 21
    label "indagator"
  ]
  node [
    id 22
    label "interesuj&#261;cy"
  ]
  node [
    id 23
    label "dziwny"
  ]
  node [
    id 24
    label "intryguj&#261;cy"
  ]
  node [
    id 25
    label "ch&#281;tny"
  ]
  node [
    id 26
    label "model"
  ]
  node [
    id 27
    label "zbi&#243;r"
  ]
  node [
    id 28
    label "tryb"
  ]
  node [
    id 29
    label "narz&#281;dzie"
  ]
  node [
    id 30
    label "nature"
  ]
  node [
    id 31
    label "throng"
  ]
  node [
    id 32
    label "zatrzyma&#263;"
  ]
  node [
    id 33
    label "wstrzyma&#263;"
  ]
  node [
    id 34
    label "give"
  ]
  node [
    id 35
    label "zrobi&#263;"
  ]
  node [
    id 36
    label "lock"
  ]
  node [
    id 37
    label "&#380;agl&#243;wka"
  ]
  node [
    id 38
    label "rozb&#243;jnik"
  ]
  node [
    id 39
    label "kieruj&#261;cy"
  ]
  node [
    id 40
    label "podr&#243;bka"
  ]
  node [
    id 41
    label "rum"
  ]
  node [
    id 42
    label "kopiowa&#263;"
  ]
  node [
    id 43
    label "program"
  ]
  node [
    id 44
    label "przest&#281;pca"
  ]
  node [
    id 45
    label "postrzeleniec"
  ]
  node [
    id 46
    label "specjalny"
  ]
  node [
    id 47
    label "niebieski"
  ]
  node [
    id 48
    label "nadmorski"
  ]
  node [
    id 49
    label "morsko"
  ]
  node [
    id 50
    label "wodny"
  ]
  node [
    id 51
    label "s&#322;ony"
  ]
  node [
    id 52
    label "zielony"
  ]
  node [
    id 53
    label "przypominaj&#261;cy"
  ]
  node [
    id 54
    label "typowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
]
