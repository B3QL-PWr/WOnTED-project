graph [
  maxDegree 11
  minDegree 1
  meanDegree 1.8461538461538463
  density 0.15384615384615385
  graphCliqueNumber 2
  node [
    id 0
    label "angielskizwykopem"
    origin "text"
  ]
  node [
    id 1
    label "angielski"
    origin "text"
  ]
  node [
    id 2
    label "heheszki"
    origin "text"
  ]
  node [
    id 3
    label "humorobrazkowy"
    origin "text"
  ]
  node [
    id 4
    label "angielsko"
  ]
  node [
    id 5
    label "English"
  ]
  node [
    id 6
    label "anglicki"
  ]
  node [
    id 7
    label "j&#281;zyk"
  ]
  node [
    id 8
    label "angol"
  ]
  node [
    id 9
    label "j&#281;zyki_angielskie"
  ]
  node [
    id 10
    label "j&#281;zyk_zachodniogerma&#324;ski"
  ]
  node [
    id 11
    label "brytyjski"
  ]
  node [
    id 12
    label "po_angielsku"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 2
    target 3
  ]
]
