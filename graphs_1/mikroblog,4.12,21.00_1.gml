graph [
  maxDegree 11
  minDegree 1
  meanDegree 1.8571428571428572
  density 0.14285714285714285
  graphCliqueNumber 2
  node [
    id 0
    label "wstawa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "morawiecki"
    origin "text"
  ]
  node [
    id 2
    label "zesrales"
    origin "text"
  ]
  node [
    id 3
    label "si&#281;"
    origin "text"
  ]
  node [
    id 4
    label "opuszcza&#263;"
  ]
  node [
    id 5
    label "rise"
  ]
  node [
    id 6
    label "stawa&#263;"
  ]
  node [
    id 7
    label "arise"
  ]
  node [
    id 8
    label "przestawa&#263;"
  ]
  node [
    id 9
    label "podnosi&#263;_si&#281;"
  ]
  node [
    id 10
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 11
    label "heighten"
  ]
  node [
    id 12
    label "wschodzi&#263;"
  ]
  node [
    id 13
    label "zdrowie&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
]
