graph [
  maxDegree 4
  minDegree 1
  meanDegree 2.1538461538461537
  density 0.1794871794871795
  graphCliqueNumber 3
  node [
    id 0
    label "barnet"
    origin "text"
  ]
  node [
    id 1
    label "femininum"
    origin "text"
  ]
  node [
    id 2
    label "cent"
    origin "text"
  ]
  node [
    id 3
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 4
    label "moneta"
  ]
  node [
    id 5
    label "Barnet"
  ]
  node [
    id 6
    label "c&#243;rka"
  ]
  node [
    id 7
    label "football"
  ]
  node [
    id 8
    label "Club"
  ]
  node [
    id 9
    label "League"
  ]
  node [
    id 10
    label "Two"
  ]
  node [
    id 11
    label "Arsenalu"
  ]
  node [
    id 12
    label "Londyn"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 7
    target 10
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 11
    target 12
  ]
]
