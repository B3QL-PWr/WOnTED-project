graph [
  maxDegree 54
  minDegree 1
  meanDegree 2.5444319460067493
  density 0.002865351290548141
  graphCliqueNumber 7
  node [
    id 0
    label "wyda&#263;"
    origin "text"
  ]
  node [
    id 1
    label "kolejny"
    origin "text"
  ]
  node [
    id 2
    label "wersja"
    origin "text"
  ]
  node [
    id 3
    label "wielo"
    origin "text"
  ]
  node [
    id 4
    label "platformowego"
    origin "text"
  ]
  node [
    id 5
    label "klient"
    origin "text"
  ]
  node [
    id 6
    label "sie&#263;"
    origin "text"
  ]
  node [
    id 7
    label "bittorrent"
    origin "text"
  ]
  node [
    id 8
    label "napisa&#263;"
    origin "text"
  ]
  node [
    id 9
    label "j&#281;zyk"
    origin "text"
  ]
  node [
    id 10
    label "cent"
    origin "text"
  ]
  node [
    id 11
    label "by&#263;"
    origin "text"
  ]
  node [
    id 12
    label "wolny"
    origin "text"
  ]
  node [
    id 13
    label "oprogramowanie"
    origin "text"
  ]
  node [
    id 14
    label "dwa"
    origin "text"
  ]
  node [
    id 15
    label "licencja"
    origin "text"
  ]
  node [
    id 16
    label "mit"
    origin "text"
  ]
  node [
    id 17
    label "gnu"
    origin "text"
  ]
  node [
    id 18
    label "general"
    origin "text"
  ]
  node [
    id 19
    label "public"
    origin "text"
  ]
  node [
    id 20
    label "license"
    origin "text"
  ]
  node [
    id 21
    label "posiada&#263;"
    origin "text"
  ]
  node [
    id 22
    label "taki"
    origin "text"
  ]
  node [
    id 23
    label "mo&#380;liwo&#347;ci"
    origin "text"
  ]
  node [
    id 24
    label "jak"
    origin "text"
  ]
  node [
    id 25
    label "szyfrowa&#263;"
    origin "text"
  ]
  node [
    id 26
    label "protok&#243;&#322;"
    origin "text"
  ]
  node [
    id 27
    label "obs&#322;uga"
    origin "text"
  ]
  node [
    id 28
    label "upnp"
    origin "text"
  ]
  node [
    id 29
    label "nat"
    origin "text"
  ]
  node [
    id 30
    label "pmp"
    origin "text"
  ]
  node [
    id 31
    label "wymiana"
    origin "text"
  ]
  node [
    id 32
    label "list"
    origin "text"
  ]
  node [
    id 33
    label "peer&#243;w"
    origin "text"
  ]
  node [
    id 34
    label "mo&#380;liwo&#347;&#263;"
    origin "text"
  ]
  node [
    id 35
    label "zdalny"
    origin "text"
  ]
  node [
    id 36
    label "pomoc"
    origin "text"
  ]
  node [
    id 37
    label "przegl&#261;darka"
    origin "text"
  ]
  node [
    id 38
    label "webgui"
    origin "text"
  ]
  node [
    id 39
    label "harmonogram"
    origin "text"
  ]
  node [
    id 40
    label "pobiera&#263;"
    origin "text"
  ]
  node [
    id 41
    label "ogranicza&#263;"
    origin "text"
  ]
  node [
    id 42
    label "pr&#281;dko&#347;&#263;"
    origin "text"
  ]
  node [
    id 43
    label "wysy&#322;a&#263;"
    origin "text"
  ]
  node [
    id 44
    label "dana"
    origin "text"
  ]
  node [
    id 45
    label "swobodny"
    origin "text"
  ]
  node [
    id 46
    label "wyb&#243;r"
    origin "text"
  ]
  node [
    id 47
    label "plik"
    origin "text"
  ]
  node [
    id 48
    label "pomin&#261;&#263;"
    origin "text"
  ]
  node [
    id 49
    label "ustawia&#263;"
    origin "text"
  ]
  node [
    id 50
    label "priorytet"
    origin "text"
  ]
  node [
    id 51
    label "dla"
    origin "text"
  ]
  node [
    id 52
    label "&#347;ci&#261;ga&#263;"
    origin "text"
  ]
  node [
    id 53
    label "torrent&#243;w"
    origin "text"
  ]
  node [
    id 54
    label "tak&#380;e"
    origin "text"
  ]
  node [
    id 55
    label "poszczeg&#243;lny"
    origin "text"
  ]
  node [
    id 56
    label "wewn&#261;trz"
    origin "text"
  ]
  node [
    id 57
    label "torrenta"
    origin "text"
  ]
  node [
    id 58
    label "ukaza&#263;"
    origin "text"
  ]
  node [
    id 59
    label "si&#281;"
    origin "text"
  ]
  node [
    id 60
    label "archiwalny"
    origin "text"
  ]
  node [
    id 61
    label "numer"
    origin "text"
  ]
  node [
    id 62
    label "magazyn"
    origin "text"
  ]
  node [
    id 63
    label "software"
    origin "text"
  ]
  node [
    id 64
    label "&#322;am"
    origin "text"
  ]
  node [
    id 65
    label "znale&#378;&#263;"
    origin "text"
  ]
  node [
    id 66
    label "nowo&#347;&#263;"
    origin "text"
  ]
  node [
    id 67
    label "bran&#380;a"
    origin "text"
  ]
  node [
    id 68
    label "ita"
    origin "text"
  ]
  node [
    id 69
    label "praktyczny"
    origin "text"
  ]
  node [
    id 70
    label "artyku&#322;"
    origin "text"
  ]
  node [
    id 71
    label "prezentowa&#263;"
    origin "text"
  ]
  node [
    id 72
    label "ciekawy"
    origin "text"
  ]
  node [
    id 73
    label "rozwi&#261;zanie"
    origin "text"
  ]
  node [
    id 74
    label "programistyczny"
    origin "text"
  ]
  node [
    id 75
    label "ydano"
    origin "text"
  ]
  node [
    id 76
    label "blokowa&#263;"
    origin "text"
  ]
  node [
    id 77
    label "adres"
    origin "text"
  ]
  node [
    id 78
    label "&#322;&#261;czy&#263;"
    origin "text"
  ]
  node [
    id 79
    label "trackerami"
    origin "text"
  ]
  node [
    id 80
    label "poprzez"
    origin "text"
  ]
  node [
    id 81
    label "https"
    origin "text"
  ]
  node [
    id 82
    label "tworzenie"
    origin "text"
  ]
  node [
    id 83
    label "torrent"
    origin "text"
  ]
  node [
    id 84
    label "opcja"
    origin "text"
  ]
  node [
    id 85
    label "sortowanie"
    origin "text"
  ]
  node [
    id 86
    label "filtrowa&#263;"
    origin "text"
  ]
  node [
    id 87
    label "pojedynczy"
    origin "text"
  ]
  node [
    id 88
    label "nas&#322;uchiwa&#263;"
    origin "text"
  ]
  node [
    id 89
    label "port"
    origin "text"
  ]
  node [
    id 90
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 91
    label "dht"
    origin "text"
  ]
  node [
    id 92
    label "uniwersalny"
    origin "text"
  ]
  node [
    id 93
    label "binarka"
    origin "text"
  ]
  node [
    id 94
    label "dotyczy&#263;"
    origin "text"
  ]
  node [
    id 95
    label "system"
    origin "text"
  ]
  node [
    id 96
    label "maca"
    origin "text"
  ]
  node [
    id 97
    label "osa"
    origin "text"
  ]
  node [
    id 98
    label "automatyczny"
    origin "text"
  ]
  node [
    id 99
    label "aktualizacja"
    origin "text"
  ]
  node [
    id 100
    label "u&#380;ycie"
    origin "text"
  ]
  node [
    id 101
    label "sparkle"
    origin "text"
  ]
  node [
    id 102
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 103
    label "przy"
    origin "text"
  ]
  node [
    id 104
    label "biblioteka"
    origin "text"
  ]
  node [
    id 105
    label "program"
    origin "text"
  ]
  node [
    id 106
    label "rozpowszechnia&#263;"
    origin "text"
  ]
  node [
    id 107
    label "gpl"
    origin "text"
  ]
  node [
    id 108
    label "tw&#243;rca"
    origin "text"
  ]
  node [
    id 109
    label "projekt"
    origin "text"
  ]
  node [
    id 110
    label "francuski"
    origin "text"
  ]
  node [
    id 111
    label "student"
    origin "text"
  ]
  node [
    id 112
    label "christophe"
    origin "text"
  ]
  node [
    id 113
    label "dumez"
    origin "text"
  ]
  node [
    id 114
    label "oprze&#263;"
    origin "text"
  ]
  node [
    id 115
    label "sw&#243;j"
    origin "text"
  ]
  node [
    id 116
    label "libtorrent"
    origin "text"
  ]
  node [
    id 117
    label "autorstwo"
    origin "text"
  ]
  node [
    id 118
    label "arvida"
    origin "text"
  ]
  node [
    id 119
    label "norberga"
    origin "text"
  ]
  node [
    id 120
    label "przez"
    origin "text"
  ]
  node [
    id 121
    label "ten"
    origin "text"
  ]
  node [
    id 122
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 123
    label "&#322;atwo"
    origin "text"
  ]
  node [
    id 124
    label "przenosi&#263;"
    origin "text"
  ]
  node [
    id 125
    label "pomi&#281;dzy"
    origin "text"
  ]
  node [
    id 126
    label "r&#243;&#380;ny"
    origin "text"
  ]
  node [
    id 127
    label "platforma"
    origin "text"
  ]
  node [
    id 128
    label "dost&#281;pny"
    origin "text"
  ]
  node [
    id 129
    label "aplikacja"
    origin "text"
  ]
  node [
    id 130
    label "firefox"
    origin "text"
  ]
  node [
    id 131
    label "opera"
    origin "text"
  ]
  node [
    id 132
    label "gftp"
    origin "text"
  ]
  node [
    id 133
    label "usun&#261;&#263;"
    origin "text"
  ]
  node [
    id 134
    label "gslapt"
    origin "text"
  ]
  node [
    id 135
    label "slapt"
    origin "text"
  ]
  node [
    id 136
    label "get"
    origin "text"
  ]
  node [
    id 137
    label "impart"
  ]
  node [
    id 138
    label "panna_na_wydaniu"
  ]
  node [
    id 139
    label "translate"
  ]
  node [
    id 140
    label "give"
  ]
  node [
    id 141
    label "pieni&#261;dze"
  ]
  node [
    id 142
    label "supply"
  ]
  node [
    id 143
    label "wprowadzi&#263;"
  ]
  node [
    id 144
    label "da&#263;"
  ]
  node [
    id 145
    label "zapach"
  ]
  node [
    id 146
    label "wydawnictwo"
  ]
  node [
    id 147
    label "powierzy&#263;"
  ]
  node [
    id 148
    label "produkcja"
  ]
  node [
    id 149
    label "poda&#263;"
  ]
  node [
    id 150
    label "skojarzy&#263;"
  ]
  node [
    id 151
    label "dress"
  ]
  node [
    id 152
    label "plon"
  ]
  node [
    id 153
    label "ujawni&#263;"
  ]
  node [
    id 154
    label "reszta"
  ]
  node [
    id 155
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 156
    label "zadenuncjowa&#263;"
  ]
  node [
    id 157
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 158
    label "zrobi&#263;"
  ]
  node [
    id 159
    label "tajemnica"
  ]
  node [
    id 160
    label "wiano"
  ]
  node [
    id 161
    label "gor&#261;cy_pieni&#261;dz"
  ]
  node [
    id 162
    label "wytworzy&#263;"
  ]
  node [
    id 163
    label "d&#378;wi&#281;k"
  ]
  node [
    id 164
    label "picture"
  ]
  node [
    id 165
    label "inny"
  ]
  node [
    id 166
    label "nast&#281;pnie"
  ]
  node [
    id 167
    label "kt&#243;ry&#347;"
  ]
  node [
    id 168
    label "kolejno"
  ]
  node [
    id 169
    label "nastopny"
  ]
  node [
    id 170
    label "typ"
  ]
  node [
    id 171
    label "posta&#263;"
  ]
  node [
    id 172
    label "cz&#322;owiek"
  ]
  node [
    id 173
    label "bratek"
  ]
  node [
    id 174
    label "klientela"
  ]
  node [
    id 175
    label "szlachcic"
  ]
  node [
    id 176
    label "agent_rozliczeniowy"
  ]
  node [
    id 177
    label "komputer_cyfrowy"
  ]
  node [
    id 178
    label "us&#322;ugobiorca"
  ]
  node [
    id 179
    label "Rzymianin"
  ]
  node [
    id 180
    label "obywatel"
  ]
  node [
    id 181
    label "hipertekst"
  ]
  node [
    id 182
    label "gauze"
  ]
  node [
    id 183
    label "nitka"
  ]
  node [
    id 184
    label "mesh"
  ]
  node [
    id 185
    label "e-hazard"
  ]
  node [
    id 186
    label "netbook"
  ]
  node [
    id 187
    label "cyberprzestrze&#324;"
  ]
  node [
    id 188
    label "biznes_elektroniczny"
  ]
  node [
    id 189
    label "snu&#263;"
  ]
  node [
    id 190
    label "organization"
  ]
  node [
    id 191
    label "zasadzka"
  ]
  node [
    id 192
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 193
    label "web"
  ]
  node [
    id 194
    label "provider"
  ]
  node [
    id 195
    label "struktura"
  ]
  node [
    id 196
    label "us&#322;uga_internetowa"
  ]
  node [
    id 197
    label "punkt_dost&#281;pu"
  ]
  node [
    id 198
    label "organizacja"
  ]
  node [
    id 199
    label "mem"
  ]
  node [
    id 200
    label "vane"
  ]
  node [
    id 201
    label "podcast"
  ]
  node [
    id 202
    label "grooming"
  ]
  node [
    id 203
    label "kszta&#322;t"
  ]
  node [
    id 204
    label "strona"
  ]
  node [
    id 205
    label "obiekt"
  ]
  node [
    id 206
    label "wysnu&#263;"
  ]
  node [
    id 207
    label "gra_sieciowa"
  ]
  node [
    id 208
    label "instalacja"
  ]
  node [
    id 209
    label "sie&#263;_komputerowa"
  ]
  node [
    id 210
    label "net"
  ]
  node [
    id 211
    label "plecionka"
  ]
  node [
    id 212
    label "media"
  ]
  node [
    id 213
    label "rozmieszczenie"
  ]
  node [
    id 214
    label "postawi&#263;"
  ]
  node [
    id 215
    label "prasa"
  ]
  node [
    id 216
    label "stworzy&#263;"
  ]
  node [
    id 217
    label "donie&#347;&#263;"
  ]
  node [
    id 218
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 219
    label "write"
  ]
  node [
    id 220
    label "styl"
  ]
  node [
    id 221
    label "read"
  ]
  node [
    id 222
    label "pisa&#263;"
  ]
  node [
    id 223
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 224
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 225
    label "ssanie"
  ]
  node [
    id 226
    label "po_koroniarsku"
  ]
  node [
    id 227
    label "przedmiot"
  ]
  node [
    id 228
    label "but"
  ]
  node [
    id 229
    label "m&#243;wienie"
  ]
  node [
    id 230
    label "rozumie&#263;"
  ]
  node [
    id 231
    label "formacja_geologiczna"
  ]
  node [
    id 232
    label "rozumienie"
  ]
  node [
    id 233
    label "m&#243;wi&#263;"
  ]
  node [
    id 234
    label "gramatyka"
  ]
  node [
    id 235
    label "pype&#263;"
  ]
  node [
    id 236
    label "makroglosja"
  ]
  node [
    id 237
    label "kawa&#322;ek"
  ]
  node [
    id 238
    label "artykulator"
  ]
  node [
    id 239
    label "kultura_duchowa"
  ]
  node [
    id 240
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 241
    label "jama_ustna"
  ]
  node [
    id 242
    label "spos&#243;b"
  ]
  node [
    id 243
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 244
    label "przet&#322;umaczenie"
  ]
  node [
    id 245
    label "t&#322;umaczenie"
  ]
  node [
    id 246
    label "language"
  ]
  node [
    id 247
    label "jeniec"
  ]
  node [
    id 248
    label "organ"
  ]
  node [
    id 249
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 250
    label "pismo"
  ]
  node [
    id 251
    label "formalizowanie"
  ]
  node [
    id 252
    label "fonetyka"
  ]
  node [
    id 253
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 254
    label "wokalizm"
  ]
  node [
    id 255
    label "liza&#263;"
  ]
  node [
    id 256
    label "s&#322;ownictwo"
  ]
  node [
    id 257
    label "formalizowa&#263;"
  ]
  node [
    id 258
    label "natural_language"
  ]
  node [
    id 259
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 260
    label "stylik"
  ]
  node [
    id 261
    label "konsonantyzm"
  ]
  node [
    id 262
    label "urz&#261;dzenie"
  ]
  node [
    id 263
    label "ssa&#263;"
  ]
  node [
    id 264
    label "kod"
  ]
  node [
    id 265
    label "lizanie"
  ]
  node [
    id 266
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 267
    label "moneta"
  ]
  node [
    id 268
    label "si&#281;ga&#263;"
  ]
  node [
    id 269
    label "trwa&#263;"
  ]
  node [
    id 270
    label "obecno&#347;&#263;"
  ]
  node [
    id 271
    label "stan"
  ]
  node [
    id 272
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 273
    label "stand"
  ]
  node [
    id 274
    label "mie&#263;_miejsce"
  ]
  node [
    id 275
    label "uczestniczy&#263;"
  ]
  node [
    id 276
    label "chodzi&#263;"
  ]
  node [
    id 277
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 278
    label "equal"
  ]
  node [
    id 279
    label "niezale&#380;ny"
  ]
  node [
    id 280
    label "swobodnie"
  ]
  node [
    id 281
    label "niespieszny"
  ]
  node [
    id 282
    label "rozrzedzanie"
  ]
  node [
    id 283
    label "zwolnienie_si&#281;"
  ]
  node [
    id 284
    label "wolno"
  ]
  node [
    id 285
    label "rozrzedzenie"
  ]
  node [
    id 286
    label "lu&#378;no"
  ]
  node [
    id 287
    label "zwalnianie_si&#281;"
  ]
  node [
    id 288
    label "wolnie"
  ]
  node [
    id 289
    label "strza&#322;"
  ]
  node [
    id 290
    label "rozwodnienie"
  ]
  node [
    id 291
    label "wakowa&#263;"
  ]
  node [
    id 292
    label "rozwadnianie"
  ]
  node [
    id 293
    label "rzedni&#281;cie"
  ]
  node [
    id 294
    label "zrzedni&#281;cie"
  ]
  node [
    id 295
    label "zbi&#243;r"
  ]
  node [
    id 296
    label "reengineering"
  ]
  node [
    id 297
    label "prawo"
  ]
  node [
    id 298
    label "licencjonowa&#263;"
  ]
  node [
    id 299
    label "pozwolenie"
  ]
  node [
    id 300
    label "hodowla"
  ]
  node [
    id 301
    label "rasowy"
  ]
  node [
    id 302
    label "zezwolenie"
  ]
  node [
    id 303
    label "za&#347;wiadczenie"
  ]
  node [
    id 304
    label "znaczenie"
  ]
  node [
    id 305
    label "fable"
  ]
  node [
    id 306
    label "mitologia"
  ]
  node [
    id 307
    label "ajtiologia"
  ]
  node [
    id 308
    label "opowie&#347;&#263;"
  ]
  node [
    id 309
    label "pogl&#261;d"
  ]
  node [
    id 310
    label "wiedzie&#263;"
  ]
  node [
    id 311
    label "mie&#263;"
  ]
  node [
    id 312
    label "keep_open"
  ]
  node [
    id 313
    label "zawiera&#263;"
  ]
  node [
    id 314
    label "support"
  ]
  node [
    id 315
    label "zdolno&#347;&#263;"
  ]
  node [
    id 316
    label "okre&#347;lony"
  ]
  node [
    id 317
    label "jaki&#347;"
  ]
  node [
    id 318
    label "capability"
  ]
  node [
    id 319
    label "potencja&#322;"
  ]
  node [
    id 320
    label "byd&#322;o"
  ]
  node [
    id 321
    label "zobo"
  ]
  node [
    id 322
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 323
    label "yakalo"
  ]
  node [
    id 324
    label "dzo"
  ]
  node [
    id 325
    label "przetwarza&#263;"
  ]
  node [
    id 326
    label "zapisywa&#263;"
  ]
  node [
    id 327
    label "code"
  ]
  node [
    id 328
    label "komunikacja_zintegrowana"
  ]
  node [
    id 329
    label "akt"
  ]
  node [
    id 330
    label "relacja"
  ]
  node [
    id 331
    label "etykieta"
  ]
  node [
    id 332
    label "zasada"
  ]
  node [
    id 333
    label "pracowanie"
  ]
  node [
    id 334
    label "robienie"
  ]
  node [
    id 335
    label "personel"
  ]
  node [
    id 336
    label "service"
  ]
  node [
    id 337
    label "zjawisko_fonetyczne"
  ]
  node [
    id 338
    label "zamiana"
  ]
  node [
    id 339
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 340
    label "implicite"
  ]
  node [
    id 341
    label "ruch"
  ]
  node [
    id 342
    label "handel"
  ]
  node [
    id 343
    label "deal"
  ]
  node [
    id 344
    label "exchange"
  ]
  node [
    id 345
    label "wydarzenie"
  ]
  node [
    id 346
    label "explicite"
  ]
  node [
    id 347
    label "szachy"
  ]
  node [
    id 348
    label "li&#347;&#263;"
  ]
  node [
    id 349
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 350
    label "poczta"
  ]
  node [
    id 351
    label "epistolografia"
  ]
  node [
    id 352
    label "przesy&#322;ka"
  ]
  node [
    id 353
    label "poczta_elektroniczna"
  ]
  node [
    id 354
    label "znaczek_pocztowy"
  ]
  node [
    id 355
    label "ability"
  ]
  node [
    id 356
    label "prospect"
  ]
  node [
    id 357
    label "egzekutywa"
  ]
  node [
    id 358
    label "alternatywa"
  ]
  node [
    id 359
    label "cecha"
  ]
  node [
    id 360
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 361
    label "obliczeniowo"
  ]
  node [
    id 362
    label "operator_modalny"
  ]
  node [
    id 363
    label "zdalnie"
  ]
  node [
    id 364
    label "zgodzi&#263;"
  ]
  node [
    id 365
    label "pomocnik"
  ]
  node [
    id 366
    label "doch&#243;d"
  ]
  node [
    id 367
    label "property"
  ]
  node [
    id 368
    label "grupa"
  ]
  node [
    id 369
    label "telefon_zaufania"
  ]
  node [
    id 370
    label "darowizna"
  ]
  node [
    id 371
    label "&#347;rodek"
  ]
  node [
    id 372
    label "liga"
  ]
  node [
    id 373
    label "viewer"
  ]
  node [
    id 374
    label "przyrz&#261;d"
  ]
  node [
    id 375
    label "browser"
  ]
  node [
    id 376
    label "projektor"
  ]
  node [
    id 377
    label "rozk&#322;ad"
  ]
  node [
    id 378
    label "diagram"
  ]
  node [
    id 379
    label "wycina&#263;"
  ]
  node [
    id 380
    label "open"
  ]
  node [
    id 381
    label "otrzymywa&#263;"
  ]
  node [
    id 382
    label "arise"
  ]
  node [
    id 383
    label "kopiowa&#263;"
  ]
  node [
    id 384
    label "raise"
  ]
  node [
    id 385
    label "wch&#322;ania&#263;"
  ]
  node [
    id 386
    label "bra&#263;"
  ]
  node [
    id 387
    label "pr&#243;bka"
  ]
  node [
    id 388
    label "wytycza&#263;"
  ]
  node [
    id 389
    label "bound"
  ]
  node [
    id 390
    label "wi&#281;zienie"
  ]
  node [
    id 391
    label "suppress"
  ]
  node [
    id 392
    label "environment"
  ]
  node [
    id 393
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 394
    label "zmniejsza&#263;"
  ]
  node [
    id 395
    label "stanowi&#263;"
  ]
  node [
    id 396
    label "energia"
  ]
  node [
    id 397
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 398
    label "celerity"
  ]
  node [
    id 399
    label "tempo"
  ]
  node [
    id 400
    label "wektorowa_wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 401
    label "nakazywa&#263;"
  ]
  node [
    id 402
    label "order"
  ]
  node [
    id 403
    label "dispatch"
  ]
  node [
    id 404
    label "grant"
  ]
  node [
    id 405
    label "wytwarza&#263;"
  ]
  node [
    id 406
    label "przekazywa&#263;"
  ]
  node [
    id 407
    label "dar"
  ]
  node [
    id 408
    label "cnota"
  ]
  node [
    id 409
    label "buddyzm"
  ]
  node [
    id 410
    label "naturalny"
  ]
  node [
    id 411
    label "bezpruderyjny"
  ]
  node [
    id 412
    label "wygodny"
  ]
  node [
    id 413
    label "dowolny"
  ]
  node [
    id 414
    label "czynno&#347;&#263;"
  ]
  node [
    id 415
    label "decyzja"
  ]
  node [
    id 416
    label "ewentualno&#347;&#263;"
  ]
  node [
    id 417
    label "pick"
  ]
  node [
    id 418
    label "dokument"
  ]
  node [
    id 419
    label "menad&#380;er_plik&#243;w"
  ]
  node [
    id 420
    label "nadpisywanie"
  ]
  node [
    id 421
    label "nadpisanie"
  ]
  node [
    id 422
    label "nadpisa&#263;"
  ]
  node [
    id 423
    label "paczka"
  ]
  node [
    id 424
    label "podkatalog"
  ]
  node [
    id 425
    label "bundle"
  ]
  node [
    id 426
    label "folder"
  ]
  node [
    id 427
    label "nadpisywa&#263;"
  ]
  node [
    id 428
    label "spowodowa&#263;"
  ]
  node [
    id 429
    label "omin&#261;&#263;"
  ]
  node [
    id 430
    label "zby&#263;"
  ]
  node [
    id 431
    label "train"
  ]
  node [
    id 432
    label "umieszcza&#263;"
  ]
  node [
    id 433
    label "wyznacza&#263;"
  ]
  node [
    id 434
    label "porz&#261;dkowa&#263;"
  ]
  node [
    id 435
    label "stanowisko"
  ]
  node [
    id 436
    label "go"
  ]
  node [
    id 437
    label "nadawa&#263;"
  ]
  node [
    id 438
    label "ustala&#263;"
  ]
  node [
    id 439
    label "poprawia&#263;"
  ]
  node [
    id 440
    label "podporz&#261;dkowywa&#263;"
  ]
  node [
    id 441
    label "kierowa&#263;"
  ]
  node [
    id 442
    label "zabezpiecza&#263;"
  ]
  node [
    id 443
    label "robi&#263;"
  ]
  node [
    id 444
    label "rozk&#322;ada&#263;"
  ]
  node [
    id 445
    label "decydowa&#263;"
  ]
  node [
    id 446
    label "nak&#322;ania&#263;"
  ]
  node [
    id 447
    label "range"
  ]
  node [
    id 448
    label "peddle"
  ]
  node [
    id 449
    label "wskazywa&#263;"
  ]
  node [
    id 450
    label "powodowa&#263;"
  ]
  node [
    id 451
    label "przyznawa&#263;"
  ]
  node [
    id 452
    label "dzia&#322;anie"
  ]
  node [
    id 453
    label "sprawa"
  ]
  node [
    id 454
    label "us&#322;uga"
  ]
  node [
    id 455
    label "cel"
  ]
  node [
    id 456
    label "pierwszy_plan"
  ]
  node [
    id 457
    label "zmusza&#263;"
  ]
  node [
    id 458
    label "stiffen"
  ]
  node [
    id 459
    label "bind"
  ]
  node [
    id 460
    label "przepisywa&#263;"
  ]
  node [
    id 461
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 462
    label "nale&#380;no&#347;&#263;"
  ]
  node [
    id 463
    label "sprawdzian"
  ]
  node [
    id 464
    label "pozyskiwa&#263;"
  ]
  node [
    id 465
    label "znosi&#263;"
  ]
  node [
    id 466
    label "kurczy&#263;"
  ]
  node [
    id 467
    label "odprowadza&#263;"
  ]
  node [
    id 468
    label "przewi&#261;zywa&#263;"
  ]
  node [
    id 469
    label "kra&#347;&#263;"
  ]
  node [
    id 470
    label "clamp"
  ]
  node [
    id 471
    label "gromadzi&#263;_si&#281;"
  ]
  node [
    id 472
    label "zdejmowa&#263;"
  ]
  node [
    id 473
    label "ciecz"
  ]
  node [
    id 474
    label "poszczeg&#243;lnie"
  ]
  node [
    id 475
    label "psychicznie"
  ]
  node [
    id 476
    label "pokaza&#263;"
  ]
  node [
    id 477
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 478
    label "unwrap"
  ]
  node [
    id 479
    label "cenny"
  ]
  node [
    id 480
    label "archival"
  ]
  node [
    id 481
    label "manewr"
  ]
  node [
    id 482
    label "sztos"
  ]
  node [
    id 483
    label "pok&#243;j"
  ]
  node [
    id 484
    label "facet"
  ]
  node [
    id 485
    label "wyst&#281;p"
  ]
  node [
    id 486
    label "turn"
  ]
  node [
    id 487
    label "impression"
  ]
  node [
    id 488
    label "hotel"
  ]
  node [
    id 489
    label "liczba"
  ]
  node [
    id 490
    label "punkt"
  ]
  node [
    id 491
    label "czasopismo"
  ]
  node [
    id 492
    label "&#380;art"
  ]
  node [
    id 493
    label "orygina&#322;"
  ]
  node [
    id 494
    label "oznaczenie"
  ]
  node [
    id 495
    label "zi&#243;&#322;ko"
  ]
  node [
    id 496
    label "akt_p&#322;ciowy"
  ]
  node [
    id 497
    label "publikacja"
  ]
  node [
    id 498
    label "hurtownia"
  ]
  node [
    id 499
    label "fabryka"
  ]
  node [
    id 500
    label "zesp&#243;&#322;"
  ]
  node [
    id 501
    label "tytu&#322;"
  ]
  node [
    id 502
    label "pomieszczenie"
  ]
  node [
    id 503
    label "kolumna"
  ]
  node [
    id 504
    label "odzyska&#263;"
  ]
  node [
    id 505
    label "devise"
  ]
  node [
    id 506
    label "oceni&#263;"
  ]
  node [
    id 507
    label "znaj&#347;&#263;"
  ]
  node [
    id 508
    label "wymy&#347;li&#263;"
  ]
  node [
    id 509
    label "invent"
  ]
  node [
    id 510
    label "pozyska&#263;"
  ]
  node [
    id 511
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 512
    label "wykry&#263;"
  ]
  node [
    id 513
    label "dozna&#263;"
  ]
  node [
    id 514
    label "urozmaicenie"
  ]
  node [
    id 515
    label "newness"
  ]
  node [
    id 516
    label "wiek"
  ]
  node [
    id 517
    label "dziedzina"
  ]
  node [
    id 518
    label "u&#380;yteczny"
  ]
  node [
    id 519
    label "praktycznie"
  ]
  node [
    id 520
    label "racjonalny"
  ]
  node [
    id 521
    label "towar"
  ]
  node [
    id 522
    label "nag&#322;&#243;wek"
  ]
  node [
    id 523
    label "znak_j&#281;zykowy"
  ]
  node [
    id 524
    label "wyr&#243;b"
  ]
  node [
    id 525
    label "blok"
  ]
  node [
    id 526
    label "line"
  ]
  node [
    id 527
    label "paragraf"
  ]
  node [
    id 528
    label "rodzajnik"
  ]
  node [
    id 529
    label "prawda"
  ]
  node [
    id 530
    label "szkic"
  ]
  node [
    id 531
    label "tekst"
  ]
  node [
    id 532
    label "fragment"
  ]
  node [
    id 533
    label "zapoznawa&#263;"
  ]
  node [
    id 534
    label "przedstawia&#263;"
  ]
  node [
    id 535
    label "present"
  ]
  node [
    id 536
    label "gra&#263;"
  ]
  node [
    id 537
    label "uprzedza&#263;"
  ]
  node [
    id 538
    label "represent"
  ]
  node [
    id 539
    label "wyra&#380;a&#263;"
  ]
  node [
    id 540
    label "attest"
  ]
  node [
    id 541
    label "display"
  ]
  node [
    id 542
    label "swoisty"
  ]
  node [
    id 543
    label "interesowanie"
  ]
  node [
    id 544
    label "nietuzinkowy"
  ]
  node [
    id 545
    label "ciekawie"
  ]
  node [
    id 546
    label "indagator"
  ]
  node [
    id 547
    label "interesuj&#261;cy"
  ]
  node [
    id 548
    label "dziwny"
  ]
  node [
    id 549
    label "intryguj&#261;cy"
  ]
  node [
    id 550
    label "ch&#281;tny"
  ]
  node [
    id 551
    label "wynik"
  ]
  node [
    id 552
    label "wyj&#347;cie"
  ]
  node [
    id 553
    label "spe&#322;nienie"
  ]
  node [
    id 554
    label "cesarskie_ci&#281;cie"
  ]
  node [
    id 555
    label "po&#322;o&#380;na"
  ]
  node [
    id 556
    label "proces_fizjologiczny"
  ]
  node [
    id 557
    label "przestanie"
  ]
  node [
    id 558
    label "marc&#243;wka"
  ]
  node [
    id 559
    label "usuni&#281;cie"
  ]
  node [
    id 560
    label "uniewa&#380;nienie"
  ]
  node [
    id 561
    label "pomys&#322;"
  ]
  node [
    id 562
    label "birth"
  ]
  node [
    id 563
    label "wymy&#347;lenie"
  ]
  node [
    id 564
    label "po&#322;&#243;g"
  ]
  node [
    id 565
    label "szok_poporodowy"
  ]
  node [
    id 566
    label "event"
  ]
  node [
    id 567
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 568
    label "rozwi&#261;zywanie_si&#281;"
  ]
  node [
    id 569
    label "dula"
  ]
  node [
    id 570
    label "throng"
  ]
  node [
    id 571
    label "zajmowa&#263;"
  ]
  node [
    id 572
    label "przeszkadza&#263;"
  ]
  node [
    id 573
    label "zablokowywa&#263;"
  ]
  node [
    id 574
    label "sk&#322;ada&#263;"
  ]
  node [
    id 575
    label "walczy&#263;"
  ]
  node [
    id 576
    label "interlock"
  ]
  node [
    id 577
    label "wstrzymywa&#263;"
  ]
  node [
    id 578
    label "unieruchamia&#263;"
  ]
  node [
    id 579
    label "kiblowa&#263;"
  ]
  node [
    id 580
    label "zatrzymywa&#263;"
  ]
  node [
    id 581
    label "parry"
  ]
  node [
    id 582
    label "adres_elektroniczny"
  ]
  node [
    id 583
    label "domena"
  ]
  node [
    id 584
    label "po&#322;o&#380;enie"
  ]
  node [
    id 585
    label "kod_pocztowy"
  ]
  node [
    id 586
    label "dane"
  ]
  node [
    id 587
    label "personalia"
  ]
  node [
    id 588
    label "siedziba"
  ]
  node [
    id 589
    label "relate"
  ]
  node [
    id 590
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 591
    label "wsp&#243;&#322;&#380;y&#263;"
  ]
  node [
    id 592
    label "po&#347;redniczy&#263;"
  ]
  node [
    id 593
    label "za&#322;&#261;cza&#263;"
  ]
  node [
    id 594
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 595
    label "pope&#322;nianie"
  ]
  node [
    id 596
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 597
    label "development"
  ]
  node [
    id 598
    label "stanowienie"
  ]
  node [
    id 599
    label "exploitation"
  ]
  node [
    id 600
    label "structure"
  ]
  node [
    id 601
    label "peer-to-peer"
  ]
  node [
    id 602
    label "metaplik"
  ]
  node [
    id 603
    label "choice"
  ]
  node [
    id 604
    label "instrument_obrotu_gie&#322;dowego"
  ]
  node [
    id 605
    label "kontrakt_opcyjny"
  ]
  node [
    id 606
    label "instrument_pochodny_o_niesymetrycznym_podziale_ryzyka"
  ]
  node [
    id 607
    label "categorization"
  ]
  node [
    id 608
    label "dzielenie"
  ]
  node [
    id 609
    label "strive"
  ]
  node [
    id 610
    label "nerka"
  ]
  node [
    id 611
    label "przepuszcza&#263;"
  ]
  node [
    id 612
    label "oczyszcza&#263;"
  ]
  node [
    id 613
    label "jednodzielny"
  ]
  node [
    id 614
    label "pojedynczo"
  ]
  node [
    id 615
    label "rzadki"
  ]
  node [
    id 616
    label "s&#322;ucha&#263;"
  ]
  node [
    id 617
    label "nat&#281;&#380;a&#263;"
  ]
  node [
    id 618
    label "Bordeaux"
  ]
  node [
    id 619
    label "nabrze&#380;e"
  ]
  node [
    id 620
    label "baza"
  ]
  node [
    id 621
    label "Kajenna"
  ]
  node [
    id 622
    label "Samara"
  ]
  node [
    id 623
    label "basen"
  ]
  node [
    id 624
    label "Berdia&#324;sk"
  ]
  node [
    id 625
    label "sztauer"
  ]
  node [
    id 626
    label "Koper"
  ]
  node [
    id 627
    label "Baku"
  ]
  node [
    id 628
    label "Korynt"
  ]
  node [
    id 629
    label "za&#322;adownia"
  ]
  node [
    id 630
    label "terminal"
  ]
  node [
    id 631
    label "uniwersalnie"
  ]
  node [
    id 632
    label "generalny"
  ]
  node [
    id 633
    label "wszechstronnie"
  ]
  node [
    id 634
    label "bargain"
  ]
  node [
    id 635
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 636
    label "tycze&#263;"
  ]
  node [
    id 637
    label "model"
  ]
  node [
    id 638
    label "sk&#322;ad"
  ]
  node [
    id 639
    label "zachowanie"
  ]
  node [
    id 640
    label "podstawa"
  ]
  node [
    id 641
    label "porz&#261;dek"
  ]
  node [
    id 642
    label "Android"
  ]
  node [
    id 643
    label "przyn&#281;ta"
  ]
  node [
    id 644
    label "jednostka_geologiczna"
  ]
  node [
    id 645
    label "metoda"
  ]
  node [
    id 646
    label "podsystem"
  ]
  node [
    id 647
    label "p&#322;&#243;d"
  ]
  node [
    id 648
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 649
    label "s&#261;d"
  ]
  node [
    id 650
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 651
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 652
    label "j&#261;dro"
  ]
  node [
    id 653
    label "eratem"
  ]
  node [
    id 654
    label "ryba"
  ]
  node [
    id 655
    label "pulpit"
  ]
  node [
    id 656
    label "oddzia&#322;"
  ]
  node [
    id 657
    label "usenet"
  ]
  node [
    id 658
    label "o&#347;"
  ]
  node [
    id 659
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 660
    label "poj&#281;cie"
  ]
  node [
    id 661
    label "w&#281;dkarstwo"
  ]
  node [
    id 662
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 663
    label "Leopard"
  ]
  node [
    id 664
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 665
    label "systemik"
  ]
  node [
    id 666
    label "rozprz&#261;c"
  ]
  node [
    id 667
    label "cybernetyk"
  ]
  node [
    id 668
    label "konstelacja"
  ]
  node [
    id 669
    label "doktryna"
  ]
  node [
    id 670
    label "method"
  ]
  node [
    id 671
    label "systemat"
  ]
  node [
    id 672
    label "m&#261;czny"
  ]
  node [
    id 673
    label "pieprzyca"
  ]
  node [
    id 674
    label "korzec"
  ]
  node [
    id 675
    label "surowiec"
  ]
  node [
    id 676
    label "jednostka_obj&#281;to&#347;ci_cia&#322;_sypkich"
  ]
  node [
    id 677
    label "maka"
  ]
  node [
    id 678
    label "pieczywo_chrupkie"
  ]
  node [
    id 679
    label "korze&#324;"
  ]
  node [
    id 680
    label "placek"
  ]
  node [
    id 681
    label "osowate"
  ]
  node [
    id 682
    label "osy_w&#322;a&#347;ciwe"
  ]
  node [
    id 683
    label "&#380;&#261;d&#322;&#243;wka"
  ]
  node [
    id 684
    label "automatycznie"
  ]
  node [
    id 685
    label "zapewnianie"
  ]
  node [
    id 686
    label "bezwiednie"
  ]
  node [
    id 687
    label "pewny"
  ]
  node [
    id 688
    label "zapewnienie"
  ]
  node [
    id 689
    label "nie&#347;wiadomy"
  ]
  node [
    id 690
    label "samoistny"
  ]
  node [
    id 691
    label "poniewolny"
  ]
  node [
    id 692
    label "dodatek"
  ]
  node [
    id 693
    label "modernizacja"
  ]
  node [
    id 694
    label "poprawka"
  ]
  node [
    id 695
    label "modernization"
  ]
  node [
    id 696
    label "use"
  ]
  node [
    id 697
    label "zrobienie"
  ]
  node [
    id 698
    label "doznanie"
  ]
  node [
    id 699
    label "stosowanie"
  ]
  node [
    id 700
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 701
    label "nau&#380;ywanie_si&#281;"
  ]
  node [
    id 702
    label "enjoyment"
  ]
  node [
    id 703
    label "zabawa"
  ]
  node [
    id 704
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 705
    label "proceed"
  ]
  node [
    id 706
    label "catch"
  ]
  node [
    id 707
    label "pozosta&#263;"
  ]
  node [
    id 708
    label "osta&#263;_si&#281;"
  ]
  node [
    id 709
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 710
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 711
    label "change"
  ]
  node [
    id 712
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 713
    label "rewers"
  ]
  node [
    id 714
    label "informatorium"
  ]
  node [
    id 715
    label "kolekcja"
  ]
  node [
    id 716
    label "czytelnik"
  ]
  node [
    id 717
    label "budynek"
  ]
  node [
    id 718
    label "programowanie"
  ]
  node [
    id 719
    label "ksi&#281;gozbi&#243;r_podr&#281;czny"
  ]
  node [
    id 720
    label "library"
  ]
  node [
    id 721
    label "instytucja"
  ]
  node [
    id 722
    label "czytelnia"
  ]
  node [
    id 723
    label "spis"
  ]
  node [
    id 724
    label "odinstalowanie"
  ]
  node [
    id 725
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 726
    label "za&#322;o&#380;enie"
  ]
  node [
    id 727
    label "emitowanie"
  ]
  node [
    id 728
    label "odinstalowywanie"
  ]
  node [
    id 729
    label "instrukcja"
  ]
  node [
    id 730
    label "teleferie"
  ]
  node [
    id 731
    label "emitowa&#263;"
  ]
  node [
    id 732
    label "wytw&#243;r"
  ]
  node [
    id 733
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 734
    label "sekcja_krytyczna"
  ]
  node [
    id 735
    label "podprogram"
  ]
  node [
    id 736
    label "tryb"
  ]
  node [
    id 737
    label "dzia&#322;"
  ]
  node [
    id 738
    label "broszura"
  ]
  node [
    id 739
    label "deklaracja"
  ]
  node [
    id 740
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 741
    label "struktura_organizacyjna"
  ]
  node [
    id 742
    label "zaprezentowanie"
  ]
  node [
    id 743
    label "informatyka"
  ]
  node [
    id 744
    label "booklet"
  ]
  node [
    id 745
    label "menu"
  ]
  node [
    id 746
    label "instalowanie"
  ]
  node [
    id 747
    label "furkacja"
  ]
  node [
    id 748
    label "odinstalowa&#263;"
  ]
  node [
    id 749
    label "instalowa&#263;"
  ]
  node [
    id 750
    label "okno"
  ]
  node [
    id 751
    label "pirat"
  ]
  node [
    id 752
    label "zainstalowanie"
  ]
  node [
    id 753
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 754
    label "ogranicznik_referencyjny"
  ]
  node [
    id 755
    label "zainstalowa&#263;"
  ]
  node [
    id 756
    label "kana&#322;"
  ]
  node [
    id 757
    label "zaprezentowa&#263;"
  ]
  node [
    id 758
    label "interfejs"
  ]
  node [
    id 759
    label "odinstalowywa&#263;"
  ]
  node [
    id 760
    label "course_of_study"
  ]
  node [
    id 761
    label "ram&#243;wka"
  ]
  node [
    id 762
    label "prezentowanie"
  ]
  node [
    id 763
    label "oferta"
  ]
  node [
    id 764
    label "generalize"
  ]
  node [
    id 765
    label "sprawia&#263;"
  ]
  node [
    id 766
    label "pomys&#322;odawca"
  ]
  node [
    id 767
    label "kszta&#322;ciciel"
  ]
  node [
    id 768
    label "tworzyciel"
  ]
  node [
    id 769
    label "&#347;w"
  ]
  node [
    id 770
    label "wykonawca"
  ]
  node [
    id 771
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 772
    label "device"
  ]
  node [
    id 773
    label "program_u&#380;ytkowy"
  ]
  node [
    id 774
    label "intencja"
  ]
  node [
    id 775
    label "agreement"
  ]
  node [
    id 776
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 777
    label "plan"
  ]
  node [
    id 778
    label "dokumentacja"
  ]
  node [
    id 779
    label "kurant"
  ]
  node [
    id 780
    label "menuet"
  ]
  node [
    id 781
    label "nami&#281;tny"
  ]
  node [
    id 782
    label "j&#281;zyk_kreolski_seszelski"
  ]
  node [
    id 783
    label "europejski"
  ]
  node [
    id 784
    label "po_francusku"
  ]
  node [
    id 785
    label "chrancuski"
  ]
  node [
    id 786
    label "francuz"
  ]
  node [
    id 787
    label "verlan"
  ]
  node [
    id 788
    label "zachodnioeuropejski"
  ]
  node [
    id 789
    label "bourr&#233;e"
  ]
  node [
    id 790
    label "French"
  ]
  node [
    id 791
    label "j&#281;zyk_roma&#324;ski"
  ]
  node [
    id 792
    label "frankofonia"
  ]
  node [
    id 793
    label "farandola"
  ]
  node [
    id 794
    label "tutor"
  ]
  node [
    id 795
    label "akademik"
  ]
  node [
    id 796
    label "immatrykulowanie"
  ]
  node [
    id 797
    label "s&#322;uchacz"
  ]
  node [
    id 798
    label "immatrykulowa&#263;"
  ]
  node [
    id 799
    label "absolwent"
  ]
  node [
    id 800
    label "indeks"
  ]
  node [
    id 801
    label "establish"
  ]
  node [
    id 802
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 803
    label "recline"
  ]
  node [
    id 804
    label "ustawi&#263;"
  ]
  node [
    id 805
    label "osnowa&#263;"
  ]
  node [
    id 806
    label "bli&#378;ni"
  ]
  node [
    id 807
    label "odpowiedni"
  ]
  node [
    id 808
    label "swojak"
  ]
  node [
    id 809
    label "samodzielny"
  ]
  node [
    id 810
    label "pochodzenie"
  ]
  node [
    id 811
    label "patent"
  ]
  node [
    id 812
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 813
    label "free"
  ]
  node [
    id 814
    label "&#322;atwy"
  ]
  node [
    id 815
    label "szybko"
  ]
  node [
    id 816
    label "&#322;atwie"
  ]
  node [
    id 817
    label "prosto"
  ]
  node [
    id 818
    label "snadnie"
  ]
  node [
    id 819
    label "przyjemnie"
  ]
  node [
    id 820
    label "&#322;acno"
  ]
  node [
    id 821
    label "infest"
  ]
  node [
    id 822
    label "zmienia&#263;"
  ]
  node [
    id 823
    label "wytrzyma&#263;"
  ]
  node [
    id 824
    label "dostosowywa&#263;"
  ]
  node [
    id 825
    label "ponosi&#263;"
  ]
  node [
    id 826
    label "przemieszcza&#263;"
  ]
  node [
    id 827
    label "move"
  ]
  node [
    id 828
    label "przelatywa&#263;"
  ]
  node [
    id 829
    label "strzela&#263;"
  ]
  node [
    id 830
    label "transfer"
  ]
  node [
    id 831
    label "pocisk"
  ]
  node [
    id 832
    label "circulate"
  ]
  node [
    id 833
    label "estrange"
  ]
  node [
    id 834
    label "rozprzestrzenia&#263;"
  ]
  node [
    id 835
    label "r&#243;&#380;nie"
  ]
  node [
    id 836
    label "koturn"
  ]
  node [
    id 837
    label "samoch&#243;d_ci&#281;&#380;arowy"
  ]
  node [
    id 838
    label "skorupa_ziemska"
  ]
  node [
    id 839
    label "p&#322;yta_kontynentalna"
  ]
  node [
    id 840
    label "podeszwa"
  ]
  node [
    id 841
    label "nadwozie"
  ]
  node [
    id 842
    label "sfera"
  ]
  node [
    id 843
    label "p&#322;aszczyzna"
  ]
  node [
    id 844
    label "mo&#380;liwy"
  ]
  node [
    id 845
    label "dost&#281;pnie"
  ]
  node [
    id 846
    label "ods&#322;oni&#281;cie_si&#281;"
  ]
  node [
    id 847
    label "przyst&#281;pnie"
  ]
  node [
    id 848
    label "zrozumia&#322;y"
  ]
  node [
    id 849
    label "odblokowywanie_si&#281;"
  ]
  node [
    id 850
    label "odblokowanie_si&#281;"
  ]
  node [
    id 851
    label "ods&#322;anianie_si&#281;"
  ]
  node [
    id 852
    label "application"
  ]
  node [
    id 853
    label "ozdoba"
  ]
  node [
    id 854
    label "naszycie"
  ]
  node [
    id 855
    label "wz&#243;r"
  ]
  node [
    id 856
    label "praktyka"
  ]
  node [
    id 857
    label "applique"
  ]
  node [
    id 858
    label "przedstawienie"
  ]
  node [
    id 859
    label "uwertura"
  ]
  node [
    id 860
    label "cyrk"
  ]
  node [
    id 861
    label "libretto"
  ]
  node [
    id 862
    label "teatr"
  ]
  node [
    id 863
    label "scena"
  ]
  node [
    id 864
    label "aria"
  ]
  node [
    id 865
    label "utw&#243;r"
  ]
  node [
    id 866
    label "sztuka"
  ]
  node [
    id 867
    label "przenie&#347;&#263;"
  ]
  node [
    id 868
    label "przesun&#261;&#263;"
  ]
  node [
    id 869
    label "pozby&#263;_si&#281;"
  ]
  node [
    id 870
    label "undo"
  ]
  node [
    id 871
    label "withdraw"
  ]
  node [
    id 872
    label "zabi&#263;"
  ]
  node [
    id 873
    label "wyrugowa&#263;"
  ]
  node [
    id 874
    label "motivate"
  ]
  node [
    id 875
    label "Public"
  ]
  node [
    id 876
    label "License"
  ]
  node [
    id 877
    label "NAT"
  ]
  node [
    id 878
    label "PMP"
  ]
  node [
    id 879
    label "2"
  ]
  node [
    id 880
    label "0"
  ]
  node [
    id 881
    label "4"
  ]
  node [
    id 882
    label "1"
  ]
  node [
    id 883
    label "Christophe"
  ]
  node [
    id 884
    label "Dumez"
  ]
  node [
    id 885
    label "Arvida"
  ]
  node [
    id 886
    label "Norberga"
  ]
  node [
    id 887
    label "Mark"
  ]
  node [
    id 888
    label "Shuttleworth"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 1
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 36
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 214
  ]
  edge [
    source 8
    target 215
  ]
  edge [
    source 8
    target 216
  ]
  edge [
    source 8
    target 217
  ]
  edge [
    source 8
    target 218
  ]
  edge [
    source 8
    target 219
  ]
  edge [
    source 8
    target 220
  ]
  edge [
    source 8
    target 221
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 37
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 100
  ]
  edge [
    source 9
    target 222
  ]
  edge [
    source 9
    target 223
  ]
  edge [
    source 9
    target 224
  ]
  edge [
    source 9
    target 225
  ]
  edge [
    source 9
    target 226
  ]
  edge [
    source 9
    target 227
  ]
  edge [
    source 9
    target 228
  ]
  edge [
    source 9
    target 229
  ]
  edge [
    source 9
    target 230
  ]
  edge [
    source 9
    target 231
  ]
  edge [
    source 9
    target 232
  ]
  edge [
    source 9
    target 233
  ]
  edge [
    source 9
    target 234
  ]
  edge [
    source 9
    target 235
  ]
  edge [
    source 9
    target 236
  ]
  edge [
    source 9
    target 237
  ]
  edge [
    source 9
    target 238
  ]
  edge [
    source 9
    target 239
  ]
  edge [
    source 9
    target 240
  ]
  edge [
    source 9
    target 241
  ]
  edge [
    source 9
    target 242
  ]
  edge [
    source 9
    target 243
  ]
  edge [
    source 9
    target 244
  ]
  edge [
    source 9
    target 245
  ]
  edge [
    source 9
    target 246
  ]
  edge [
    source 9
    target 247
  ]
  edge [
    source 9
    target 248
  ]
  edge [
    source 9
    target 249
  ]
  edge [
    source 9
    target 250
  ]
  edge [
    source 9
    target 251
  ]
  edge [
    source 9
    target 252
  ]
  edge [
    source 9
    target 253
  ]
  edge [
    source 9
    target 254
  ]
  edge [
    source 9
    target 255
  ]
  edge [
    source 9
    target 256
  ]
  edge [
    source 9
    target 257
  ]
  edge [
    source 9
    target 258
  ]
  edge [
    source 9
    target 259
  ]
  edge [
    source 9
    target 260
  ]
  edge [
    source 9
    target 261
  ]
  edge [
    source 9
    target 262
  ]
  edge [
    source 9
    target 263
  ]
  edge [
    source 9
    target 264
  ]
  edge [
    source 9
    target 265
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 38
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 37
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 10
    target 266
  ]
  edge [
    source 10
    target 267
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 11
    target 109
  ]
  edge [
    source 11
    target 110
  ]
  edge [
    source 11
    target 128
  ]
  edge [
    source 11
    target 22
  ]
  edge [
    source 11
    target 268
  ]
  edge [
    source 11
    target 269
  ]
  edge [
    source 11
    target 270
  ]
  edge [
    source 11
    target 271
  ]
  edge [
    source 11
    target 272
  ]
  edge [
    source 11
    target 273
  ]
  edge [
    source 11
    target 274
  ]
  edge [
    source 11
    target 275
  ]
  edge [
    source 11
    target 276
  ]
  edge [
    source 11
    target 277
  ]
  edge [
    source 11
    target 278
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 279
  ]
  edge [
    source 12
    target 280
  ]
  edge [
    source 12
    target 281
  ]
  edge [
    source 12
    target 282
  ]
  edge [
    source 12
    target 283
  ]
  edge [
    source 12
    target 284
  ]
  edge [
    source 12
    target 285
  ]
  edge [
    source 12
    target 286
  ]
  edge [
    source 12
    target 287
  ]
  edge [
    source 12
    target 288
  ]
  edge [
    source 12
    target 289
  ]
  edge [
    source 12
    target 290
  ]
  edge [
    source 12
    target 291
  ]
  edge [
    source 12
    target 292
  ]
  edge [
    source 12
    target 293
  ]
  edge [
    source 12
    target 294
  ]
  edge [
    source 12
    target 39
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 105
  ]
  edge [
    source 13
    target 295
  ]
  edge [
    source 13
    target 296
  ]
  edge [
    source 13
    target 95
  ]
  edge [
    source 13
    target 40
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 106
  ]
  edge [
    source 15
    target 107
  ]
  edge [
    source 15
    target 297
  ]
  edge [
    source 15
    target 298
  ]
  edge [
    source 15
    target 299
  ]
  edge [
    source 15
    target 300
  ]
  edge [
    source 15
    target 301
  ]
  edge [
    source 15
    target 20
  ]
  edge [
    source 15
    target 302
  ]
  edge [
    source 15
    target 303
  ]
  edge [
    source 15
    target 41
  ]
  edge [
    source 15
    target 42
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 304
  ]
  edge [
    source 16
    target 305
  ]
  edge [
    source 16
    target 306
  ]
  edge [
    source 16
    target 307
  ]
  edge [
    source 16
    target 308
  ]
  edge [
    source 16
    target 309
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 875
  ]
  edge [
    source 17
    target 876
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 43
  ]
  edge [
    source 18
    target 875
  ]
  edge [
    source 18
    target 876
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 40
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 45
  ]
  edge [
    source 20
    target 46
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 310
  ]
  edge [
    source 21
    target 311
  ]
  edge [
    source 21
    target 272
  ]
  edge [
    source 21
    target 312
  ]
  edge [
    source 21
    target 313
  ]
  edge [
    source 21
    target 314
  ]
  edge [
    source 21
    target 315
  ]
  edge [
    source 21
    target 34
  ]
  edge [
    source 21
    target 45
  ]
  edge [
    source 21
    target 46
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 129
  ]
  edge [
    source 22
    target 316
  ]
  edge [
    source 22
    target 317
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 318
  ]
  edge [
    source 23
    target 315
  ]
  edge [
    source 23
    target 319
  ]
  edge [
    source 23
    target 48
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 129
  ]
  edge [
    source 24
    target 130
  ]
  edge [
    source 24
    target 320
  ]
  edge [
    source 24
    target 321
  ]
  edge [
    source 24
    target 322
  ]
  edge [
    source 24
    target 323
  ]
  edge [
    source 24
    target 324
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 325
  ]
  edge [
    source 25
    target 326
  ]
  edge [
    source 25
    target 327
  ]
  edge [
    source 25
    target 34
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 80
  ]
  edge [
    source 26
    target 81
  ]
  edge [
    source 26
    target 328
  ]
  edge [
    source 26
    target 329
  ]
  edge [
    source 26
    target 330
  ]
  edge [
    source 26
    target 331
  ]
  edge [
    source 26
    target 332
  ]
  edge [
    source 26
    target 49
  ]
  edge [
    source 26
    target 42
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 35
  ]
  edge [
    source 27
    target 36
  ]
  edge [
    source 27
    target 81
  ]
  edge [
    source 27
    target 82
  ]
  edge [
    source 27
    target 57
  ]
  edge [
    source 27
    target 91
  ]
  edge [
    source 27
    target 333
  ]
  edge [
    source 27
    target 334
  ]
  edge [
    source 27
    target 335
  ]
  edge [
    source 27
    target 336
  ]
  edge [
    source 27
    target 50
  ]
  edge [
    source 27
    target 56
  ]
  edge [
    source 27
    target 40
  ]
  edge [
    source 27
    target 55
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 337
  ]
  edge [
    source 31
    target 338
  ]
  edge [
    source 31
    target 339
  ]
  edge [
    source 31
    target 340
  ]
  edge [
    source 31
    target 341
  ]
  edge [
    source 31
    target 342
  ]
  edge [
    source 31
    target 343
  ]
  edge [
    source 31
    target 344
  ]
  edge [
    source 31
    target 345
  ]
  edge [
    source 31
    target 346
  ]
  edge [
    source 31
    target 347
  ]
  edge [
    source 31
    target 52
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 348
  ]
  edge [
    source 32
    target 349
  ]
  edge [
    source 32
    target 350
  ]
  edge [
    source 32
    target 351
  ]
  edge [
    source 32
    target 352
  ]
  edge [
    source 32
    target 353
  ]
  edge [
    source 32
    target 354
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 53
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 48
  ]
  edge [
    source 34
    target 49
  ]
  edge [
    source 34
    target 355
  ]
  edge [
    source 34
    target 46
  ]
  edge [
    source 34
    target 356
  ]
  edge [
    source 34
    target 357
  ]
  edge [
    source 34
    target 358
  ]
  edge [
    source 34
    target 319
  ]
  edge [
    source 34
    target 359
  ]
  edge [
    source 34
    target 360
  ]
  edge [
    source 34
    target 361
  ]
  edge [
    source 34
    target 345
  ]
  edge [
    source 34
    target 362
  ]
  edge [
    source 34
    target 54
  ]
  edge [
    source 34
    target 85
  ]
  edge [
    source 35
    target 363
  ]
  edge [
    source 35
    target 45
  ]
  edge [
    source 35
    target 63
  ]
  edge [
    source 35
    target 55
  ]
  edge [
    source 35
    target 82
  ]
  edge [
    source 35
    target 101
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 364
  ]
  edge [
    source 36
    target 365
  ]
  edge [
    source 36
    target 366
  ]
  edge [
    source 36
    target 367
  ]
  edge [
    source 36
    target 227
  ]
  edge [
    source 36
    target 368
  ]
  edge [
    source 36
    target 369
  ]
  edge [
    source 36
    target 370
  ]
  edge [
    source 36
    target 371
  ]
  edge [
    source 36
    target 372
  ]
  edge [
    source 36
    target 57
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 373
  ]
  edge [
    source 37
    target 374
  ]
  edge [
    source 37
    target 105
  ]
  edge [
    source 37
    target 375
  ]
  edge [
    source 37
    target 376
  ]
  edge [
    source 37
    target 76
  ]
  edge [
    source 37
    target 108
  ]
  edge [
    source 37
    target 70
  ]
  edge [
    source 37
    target 113
  ]
  edge [
    source 37
    target 109
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 48
  ]
  edge [
    source 38
    target 77
  ]
  edge [
    source 38
    target 84
  ]
  edge [
    source 38
    target 110
  ]
  edge [
    source 38
    target 122
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 377
  ]
  edge [
    source 39
    target 378
  ]
  edge [
    source 39
    target 78
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 43
  ]
  edge [
    source 40
    target 44
  ]
  edge [
    source 40
    target 379
  ]
  edge [
    source 40
    target 380
  ]
  edge [
    source 40
    target 381
  ]
  edge [
    source 40
    target 382
  ]
  edge [
    source 40
    target 383
  ]
  edge [
    source 40
    target 384
  ]
  edge [
    source 40
    target 385
  ]
  edge [
    source 40
    target 386
  ]
  edge [
    source 40
    target 387
  ]
  edge [
    source 40
    target 79
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 388
  ]
  edge [
    source 41
    target 389
  ]
  edge [
    source 41
    target 390
  ]
  edge [
    source 41
    target 391
  ]
  edge [
    source 41
    target 392
  ]
  edge [
    source 41
    target 393
  ]
  edge [
    source 41
    target 394
  ]
  edge [
    source 41
    target 395
  ]
  edge [
    source 41
    target 80
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 396
  ]
  edge [
    source 42
    target 397
  ]
  edge [
    source 42
    target 398
  ]
  edge [
    source 42
    target 359
  ]
  edge [
    source 42
    target 399
  ]
  edge [
    source 42
    target 400
  ]
  edge [
    source 43
    target 401
  ]
  edge [
    source 43
    target 402
  ]
  edge [
    source 43
    target 403
  ]
  edge [
    source 43
    target 404
  ]
  edge [
    source 43
    target 405
  ]
  edge [
    source 43
    target 406
  ]
  edge [
    source 43
    target 81
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 407
  ]
  edge [
    source 44
    target 408
  ]
  edge [
    source 44
    target 409
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 279
  ]
  edge [
    source 45
    target 280
  ]
  edge [
    source 45
    target 410
  ]
  edge [
    source 45
    target 411
  ]
  edge [
    source 45
    target 288
  ]
  edge [
    source 45
    target 412
  ]
  edge [
    source 45
    target 413
  ]
  edge [
    source 45
    target 63
  ]
  edge [
    source 45
    target 55
  ]
  edge [
    source 45
    target 82
  ]
  edge [
    source 45
    target 101
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 414
  ]
  edge [
    source 46
    target 192
  ]
  edge [
    source 46
    target 415
  ]
  edge [
    source 46
    target 416
  ]
  edge [
    source 46
    target 417
  ]
  edge [
    source 46
    target 83
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 55
  ]
  edge [
    source 47
    target 56
  ]
  edge [
    source 47
    target 82
  ]
  edge [
    source 47
    target 83
  ]
  edge [
    source 47
    target 418
  ]
  edge [
    source 47
    target 419
  ]
  edge [
    source 47
    target 420
  ]
  edge [
    source 47
    target 421
  ]
  edge [
    source 47
    target 192
  ]
  edge [
    source 47
    target 422
  ]
  edge [
    source 47
    target 423
  ]
  edge [
    source 47
    target 424
  ]
  edge [
    source 47
    target 425
  ]
  edge [
    source 47
    target 426
  ]
  edge [
    source 47
    target 427
  ]
  edge [
    source 47
    target 93
  ]
  edge [
    source 48
    target 428
  ]
  edge [
    source 48
    target 429
  ]
  edge [
    source 48
    target 430
  ]
  edge [
    source 48
    target 77
  ]
  edge [
    source 48
    target 84
  ]
  edge [
    source 48
    target 110
  ]
  edge [
    source 48
    target 122
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 431
  ]
  edge [
    source 49
    target 432
  ]
  edge [
    source 49
    target 433
  ]
  edge [
    source 49
    target 434
  ]
  edge [
    source 49
    target 435
  ]
  edge [
    source 49
    target 436
  ]
  edge [
    source 49
    target 437
  ]
  edge [
    source 49
    target 438
  ]
  edge [
    source 49
    target 439
  ]
  edge [
    source 49
    target 440
  ]
  edge [
    source 49
    target 441
  ]
  edge [
    source 49
    target 442
  ]
  edge [
    source 49
    target 443
  ]
  edge [
    source 49
    target 444
  ]
  edge [
    source 49
    target 445
  ]
  edge [
    source 49
    target 446
  ]
  edge [
    source 49
    target 447
  ]
  edge [
    source 49
    target 448
  ]
  edge [
    source 49
    target 449
  ]
  edge [
    source 49
    target 450
  ]
  edge [
    source 49
    target 451
  ]
  edge [
    source 49
    target 86
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 452
  ]
  edge [
    source 50
    target 304
  ]
  edge [
    source 50
    target 453
  ]
  edge [
    source 50
    target 352
  ]
  edge [
    source 50
    target 454
  ]
  edge [
    source 50
    target 455
  ]
  edge [
    source 50
    target 456
  ]
  edge [
    source 50
    target 87
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 54
  ]
  edge [
    source 51
    target 55
  ]
  edge [
    source 51
    target 89
  ]
  edge [
    source 51
    target 90
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 457
  ]
  edge [
    source 52
    target 458
  ]
  edge [
    source 52
    target 459
  ]
  edge [
    source 52
    target 460
  ]
  edge [
    source 52
    target 461
  ]
  edge [
    source 52
    target 462
  ]
  edge [
    source 52
    target 463
  ]
  edge [
    source 52
    target 464
  ]
  edge [
    source 52
    target 465
  ]
  edge [
    source 52
    target 466
  ]
  edge [
    source 52
    target 467
  ]
  edge [
    source 52
    target 383
  ]
  edge [
    source 52
    target 443
  ]
  edge [
    source 52
    target 468
  ]
  edge [
    source 52
    target 469
  ]
  edge [
    source 52
    target 470
  ]
  edge [
    source 52
    target 471
  ]
  edge [
    source 52
    target 450
  ]
  edge [
    source 52
    target 472
  ]
  edge [
    source 52
    target 473
  ]
  edge [
    source 52
    target 88
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 57
  ]
  edge [
    source 53
    target 90
  ]
  edge [
    source 53
    target 93
  ]
  edge [
    source 54
    target 57
  ]
  edge [
    source 55
    target 474
  ]
  edge [
    source 55
    target 87
  ]
  edge [
    source 55
    target 63
  ]
  edge [
    source 55
    target 82
  ]
  edge [
    source 55
    target 101
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 475
  ]
  edge [
    source 56
    target 92
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 76
  ]
  edge [
    source 57
    target 90
  ]
  edge [
    source 57
    target 93
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 476
  ]
  edge [
    source 58
    target 477
  ]
  edge [
    source 58
    target 478
  ]
  edge [
    source 58
    target 94
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 479
  ]
  edge [
    source 60
    target 480
  ]
  edge [
    source 60
    target 98
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 481
  ]
  edge [
    source 61
    target 482
  ]
  edge [
    source 61
    target 483
  ]
  edge [
    source 61
    target 484
  ]
  edge [
    source 61
    target 485
  ]
  edge [
    source 61
    target 486
  ]
  edge [
    source 61
    target 487
  ]
  edge [
    source 61
    target 488
  ]
  edge [
    source 61
    target 489
  ]
  edge [
    source 61
    target 490
  ]
  edge [
    source 61
    target 491
  ]
  edge [
    source 61
    target 492
  ]
  edge [
    source 61
    target 493
  ]
  edge [
    source 61
    target 494
  ]
  edge [
    source 61
    target 495
  ]
  edge [
    source 61
    target 496
  ]
  edge [
    source 61
    target 497
  ]
  edge [
    source 61
    target 74
  ]
  edge [
    source 61
    target 99
  ]
  edge [
    source 61
    target 112
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 498
  ]
  edge [
    source 62
    target 499
  ]
  edge [
    source 62
    target 500
  ]
  edge [
    source 62
    target 501
  ]
  edge [
    source 62
    target 491
  ]
  edge [
    source 62
    target 502
  ]
  edge [
    source 62
    target 100
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 105
  ]
  edge [
    source 63
    target 295
  ]
  edge [
    source 63
    target 296
  ]
  edge [
    source 63
    target 82
  ]
  edge [
    source 63
    target 101
  ]
  edge [
    source 63
    target 879
  ]
  edge [
    source 63
    target 880
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 503
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 504
  ]
  edge [
    source 65
    target 505
  ]
  edge [
    source 65
    target 506
  ]
  edge [
    source 65
    target 507
  ]
  edge [
    source 65
    target 508
  ]
  edge [
    source 65
    target 509
  ]
  edge [
    source 65
    target 510
  ]
  edge [
    source 65
    target 511
  ]
  edge [
    source 65
    target 512
  ]
  edge [
    source 65
    target 157
  ]
  edge [
    source 65
    target 513
  ]
  edge [
    source 65
    target 94
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 66
    target 514
  ]
  edge [
    source 66
    target 515
  ]
  edge [
    source 66
    target 516
  ]
  edge [
    source 66
    target 95
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 67
    target 517
  ]
  edge [
    source 67
    target 72
  ]
  edge [
    source 67
    target 119
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 69
    target 518
  ]
  edge [
    source 69
    target 519
  ]
  edge [
    source 69
    target 520
  ]
  edge [
    source 69
    target 102
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 418
  ]
  edge [
    source 70
    target 521
  ]
  edge [
    source 70
    target 522
  ]
  edge [
    source 70
    target 523
  ]
  edge [
    source 70
    target 524
  ]
  edge [
    source 70
    target 525
  ]
  edge [
    source 70
    target 526
  ]
  edge [
    source 70
    target 527
  ]
  edge [
    source 70
    target 528
  ]
  edge [
    source 70
    target 529
  ]
  edge [
    source 70
    target 530
  ]
  edge [
    source 70
    target 531
  ]
  edge [
    source 70
    target 532
  ]
  edge [
    source 70
    target 113
  ]
  edge [
    source 70
    target 109
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 71
    target 533
  ]
  edge [
    source 71
    target 534
  ]
  edge [
    source 71
    target 535
  ]
  edge [
    source 71
    target 536
  ]
  edge [
    source 71
    target 537
  ]
  edge [
    source 71
    target 538
  ]
  edge [
    source 71
    target 105
  ]
  edge [
    source 71
    target 539
  ]
  edge [
    source 71
    target 540
  ]
  edge [
    source 71
    target 541
  ]
  edge [
    source 72
    target 73
  ]
  edge [
    source 72
    target 542
  ]
  edge [
    source 72
    target 172
  ]
  edge [
    source 72
    target 543
  ]
  edge [
    source 72
    target 544
  ]
  edge [
    source 72
    target 545
  ]
  edge [
    source 72
    target 546
  ]
  edge [
    source 72
    target 547
  ]
  edge [
    source 72
    target 548
  ]
  edge [
    source 72
    target 549
  ]
  edge [
    source 72
    target 550
  ]
  edge [
    source 72
    target 119
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 73
    target 551
  ]
  edge [
    source 73
    target 552
  ]
  edge [
    source 73
    target 553
  ]
  edge [
    source 73
    target 554
  ]
  edge [
    source 73
    target 555
  ]
  edge [
    source 73
    target 556
  ]
  edge [
    source 73
    target 557
  ]
  edge [
    source 73
    target 558
  ]
  edge [
    source 73
    target 559
  ]
  edge [
    source 73
    target 560
  ]
  edge [
    source 73
    target 561
  ]
  edge [
    source 73
    target 562
  ]
  edge [
    source 73
    target 563
  ]
  edge [
    source 73
    target 564
  ]
  edge [
    source 73
    target 565
  ]
  edge [
    source 73
    target 566
  ]
  edge [
    source 73
    target 567
  ]
  edge [
    source 73
    target 242
  ]
  edge [
    source 73
    target 568
  ]
  edge [
    source 73
    target 569
  ]
  edge [
    source 74
    target 75
  ]
  edge [
    source 74
    target 99
  ]
  edge [
    source 74
    target 112
  ]
  edge [
    source 75
    target 100
  ]
  edge [
    source 76
    target 77
  ]
  edge [
    source 76
    target 570
  ]
  edge [
    source 76
    target 571
  ]
  edge [
    source 76
    target 572
  ]
  edge [
    source 76
    target 573
  ]
  edge [
    source 76
    target 574
  ]
  edge [
    source 76
    target 575
  ]
  edge [
    source 76
    target 576
  ]
  edge [
    source 76
    target 577
  ]
  edge [
    source 76
    target 578
  ]
  edge [
    source 76
    target 579
  ]
  edge [
    source 76
    target 580
  ]
  edge [
    source 76
    target 581
  ]
  edge [
    source 76
    target 108
  ]
  edge [
    source 77
    target 78
  ]
  edge [
    source 77
    target 204
  ]
  edge [
    source 77
    target 582
  ]
  edge [
    source 77
    target 583
  ]
  edge [
    source 77
    target 584
  ]
  edge [
    source 77
    target 585
  ]
  edge [
    source 77
    target 586
  ]
  edge [
    source 77
    target 250
  ]
  edge [
    source 77
    target 352
  ]
  edge [
    source 77
    target 587
  ]
  edge [
    source 77
    target 588
  ]
  edge [
    source 77
    target 517
  ]
  edge [
    source 77
    target 84
  ]
  edge [
    source 77
    target 110
  ]
  edge [
    source 77
    target 122
  ]
  edge [
    source 78
    target 79
  ]
  edge [
    source 78
    target 589
  ]
  edge [
    source 78
    target 590
  ]
  edge [
    source 78
    target 591
  ]
  edge [
    source 78
    target 443
  ]
  edge [
    source 78
    target 592
  ]
  edge [
    source 78
    target 450
  ]
  edge [
    source 78
    target 593
  ]
  edge [
    source 79
    target 80
  ]
  edge [
    source 82
    target 334
  ]
  edge [
    source 82
    target 594
  ]
  edge [
    source 82
    target 595
  ]
  edge [
    source 82
    target 596
  ]
  edge [
    source 82
    target 597
  ]
  edge [
    source 82
    target 598
  ]
  edge [
    source 82
    target 599
  ]
  edge [
    source 82
    target 600
  ]
  edge [
    source 82
    target 101
  ]
  edge [
    source 83
    target 84
  ]
  edge [
    source 83
    target 601
  ]
  edge [
    source 83
    target 602
  ]
  edge [
    source 84
    target 85
  ]
  edge [
    source 84
    target 500
  ]
  edge [
    source 84
    target 603
  ]
  edge [
    source 84
    target 604
  ]
  edge [
    source 84
    target 605
  ]
  edge [
    source 84
    target 416
  ]
  edge [
    source 84
    target 606
  ]
  edge [
    source 84
    target 93
  ]
  edge [
    source 84
    target 110
  ]
  edge [
    source 84
    target 122
  ]
  edge [
    source 85
    target 86
  ]
  edge [
    source 85
    target 607
  ]
  edge [
    source 85
    target 608
  ]
  edge [
    source 86
    target 87
  ]
  edge [
    source 86
    target 609
  ]
  edge [
    source 86
    target 610
  ]
  edge [
    source 86
    target 442
  ]
  edge [
    source 86
    target 611
  ]
  edge [
    source 86
    target 612
  ]
  edge [
    source 87
    target 88
  ]
  edge [
    source 87
    target 613
  ]
  edge [
    source 87
    target 614
  ]
  edge [
    source 87
    target 615
  ]
  edge [
    source 88
    target 89
  ]
  edge [
    source 88
    target 616
  ]
  edge [
    source 88
    target 617
  ]
  edge [
    source 89
    target 618
  ]
  edge [
    source 89
    target 619
  ]
  edge [
    source 89
    target 620
  ]
  edge [
    source 89
    target 621
  ]
  edge [
    source 89
    target 622
  ]
  edge [
    source 89
    target 623
  ]
  edge [
    source 89
    target 624
  ]
  edge [
    source 89
    target 625
  ]
  edge [
    source 89
    target 626
  ]
  edge [
    source 89
    target 627
  ]
  edge [
    source 89
    target 628
  ]
  edge [
    source 89
    target 629
  ]
  edge [
    source 89
    target 630
  ]
  edge [
    source 90
    target 317
  ]
  edge [
    source 90
    target 93
  ]
  edge [
    source 91
    target 92
  ]
  edge [
    source 92
    target 93
  ]
  edge [
    source 92
    target 631
  ]
  edge [
    source 92
    target 632
  ]
  edge [
    source 92
    target 633
  ]
  edge [
    source 93
    target 94
  ]
  edge [
    source 94
    target 95
  ]
  edge [
    source 94
    target 101
  ]
  edge [
    source 94
    target 634
  ]
  edge [
    source 94
    target 635
  ]
  edge [
    source 94
    target 636
  ]
  edge [
    source 95
    target 96
  ]
  edge [
    source 95
    target 132
  ]
  edge [
    source 95
    target 133
  ]
  edge [
    source 95
    target 637
  ]
  edge [
    source 95
    target 638
  ]
  edge [
    source 95
    target 639
  ]
  edge [
    source 95
    target 640
  ]
  edge [
    source 95
    target 641
  ]
  edge [
    source 95
    target 642
  ]
  edge [
    source 95
    target 643
  ]
  edge [
    source 95
    target 644
  ]
  edge [
    source 95
    target 645
  ]
  edge [
    source 95
    target 646
  ]
  edge [
    source 95
    target 647
  ]
  edge [
    source 95
    target 648
  ]
  edge [
    source 95
    target 649
  ]
  edge [
    source 95
    target 650
  ]
  edge [
    source 95
    target 651
  ]
  edge [
    source 95
    target 192
  ]
  edge [
    source 95
    target 652
  ]
  edge [
    source 95
    target 653
  ]
  edge [
    source 95
    target 654
  ]
  edge [
    source 95
    target 655
  ]
  edge [
    source 95
    target 195
  ]
  edge [
    source 95
    target 242
  ]
  edge [
    source 95
    target 656
  ]
  edge [
    source 95
    target 657
  ]
  edge [
    source 95
    target 658
  ]
  edge [
    source 95
    target 659
  ]
  edge [
    source 95
    target 660
  ]
  edge [
    source 95
    target 661
  ]
  edge [
    source 95
    target 662
  ]
  edge [
    source 95
    target 663
  ]
  edge [
    source 95
    target 664
  ]
  edge [
    source 95
    target 665
  ]
  edge [
    source 95
    target 666
  ]
  edge [
    source 95
    target 667
  ]
  edge [
    source 95
    target 668
  ]
  edge [
    source 95
    target 669
  ]
  edge [
    source 95
    target 210
  ]
  edge [
    source 95
    target 295
  ]
  edge [
    source 95
    target 670
  ]
  edge [
    source 95
    target 671
  ]
  edge [
    source 96
    target 97
  ]
  edge [
    source 96
    target 672
  ]
  edge [
    source 96
    target 673
  ]
  edge [
    source 96
    target 674
  ]
  edge [
    source 96
    target 675
  ]
  edge [
    source 96
    target 676
  ]
  edge [
    source 96
    target 677
  ]
  edge [
    source 96
    target 678
  ]
  edge [
    source 96
    target 679
  ]
  edge [
    source 96
    target 680
  ]
  edge [
    source 97
    target 98
  ]
  edge [
    source 97
    target 681
  ]
  edge [
    source 97
    target 682
  ]
  edge [
    source 97
    target 683
  ]
  edge [
    source 98
    target 99
  ]
  edge [
    source 98
    target 684
  ]
  edge [
    source 98
    target 685
  ]
  edge [
    source 98
    target 686
  ]
  edge [
    source 98
    target 687
  ]
  edge [
    source 98
    target 688
  ]
  edge [
    source 98
    target 689
  ]
  edge [
    source 98
    target 690
  ]
  edge [
    source 98
    target 691
  ]
  edge [
    source 99
    target 100
  ]
  edge [
    source 99
    target 692
  ]
  edge [
    source 99
    target 693
  ]
  edge [
    source 99
    target 694
  ]
  edge [
    source 99
    target 105
  ]
  edge [
    source 99
    target 695
  ]
  edge [
    source 99
    target 112
  ]
  edge [
    source 100
    target 101
  ]
  edge [
    source 100
    target 103
  ]
  edge [
    source 100
    target 696
  ]
  edge [
    source 100
    target 697
  ]
  edge [
    source 100
    target 698
  ]
  edge [
    source 100
    target 699
  ]
  edge [
    source 100
    target 700
  ]
  edge [
    source 100
    target 701
  ]
  edge [
    source 100
    target 702
  ]
  edge [
    source 100
    target 518
  ]
  edge [
    source 100
    target 703
  ]
  edge [
    source 100
    target 704
  ]
  edge [
    source 102
    target 705
  ]
  edge [
    source 102
    target 706
  ]
  edge [
    source 102
    target 707
  ]
  edge [
    source 102
    target 708
  ]
  edge [
    source 102
    target 709
  ]
  edge [
    source 102
    target 710
  ]
  edge [
    source 102
    target 157
  ]
  edge [
    source 102
    target 711
  ]
  edge [
    source 102
    target 712
  ]
  edge [
    source 104
    target 109
  ]
  edge [
    source 104
    target 116
  ]
  edge [
    source 104
    target 483
  ]
  edge [
    source 104
    target 713
  ]
  edge [
    source 104
    target 714
  ]
  edge [
    source 104
    target 715
  ]
  edge [
    source 104
    target 716
  ]
  edge [
    source 104
    target 717
  ]
  edge [
    source 104
    target 295
  ]
  edge [
    source 104
    target 718
  ]
  edge [
    source 104
    target 719
  ]
  edge [
    source 104
    target 720
  ]
  edge [
    source 104
    target 721
  ]
  edge [
    source 104
    target 722
  ]
  edge [
    source 105
    target 723
  ]
  edge [
    source 105
    target 724
  ]
  edge [
    source 105
    target 725
  ]
  edge [
    source 105
    target 726
  ]
  edge [
    source 105
    target 640
  ]
  edge [
    source 105
    target 727
  ]
  edge [
    source 105
    target 728
  ]
  edge [
    source 105
    target 729
  ]
  edge [
    source 105
    target 490
  ]
  edge [
    source 105
    target 730
  ]
  edge [
    source 105
    target 731
  ]
  edge [
    source 105
    target 732
  ]
  edge [
    source 105
    target 733
  ]
  edge [
    source 105
    target 734
  ]
  edge [
    source 105
    target 525
  ]
  edge [
    source 105
    target 735
  ]
  edge [
    source 105
    target 736
  ]
  edge [
    source 105
    target 737
  ]
  edge [
    source 105
    target 738
  ]
  edge [
    source 105
    target 739
  ]
  edge [
    source 105
    target 740
  ]
  edge [
    source 105
    target 741
  ]
  edge [
    source 105
    target 742
  ]
  edge [
    source 105
    target 743
  ]
  edge [
    source 105
    target 744
  ]
  edge [
    source 105
    target 745
  ]
  edge [
    source 105
    target 746
  ]
  edge [
    source 105
    target 747
  ]
  edge [
    source 105
    target 748
  ]
  edge [
    source 105
    target 749
  ]
  edge [
    source 105
    target 750
  ]
  edge [
    source 105
    target 751
  ]
  edge [
    source 105
    target 752
  ]
  edge [
    source 105
    target 753
  ]
  edge [
    source 105
    target 754
  ]
  edge [
    source 105
    target 755
  ]
  edge [
    source 105
    target 756
  ]
  edge [
    source 105
    target 757
  ]
  edge [
    source 105
    target 758
  ]
  edge [
    source 105
    target 759
  ]
  edge [
    source 105
    target 426
  ]
  edge [
    source 105
    target 760
  ]
  edge [
    source 105
    target 761
  ]
  edge [
    source 105
    target 762
  ]
  edge [
    source 105
    target 763
  ]
  edge [
    source 105
    target 129
  ]
  edge [
    source 106
    target 764
  ]
  edge [
    source 106
    target 765
  ]
  edge [
    source 106
    target 124
  ]
  edge [
    source 107
    target 108
  ]
  edge [
    source 108
    target 109
  ]
  edge [
    source 108
    target 766
  ]
  edge [
    source 108
    target 767
  ]
  edge [
    source 108
    target 768
  ]
  edge [
    source 108
    target 769
  ]
  edge [
    source 108
    target 770
  ]
  edge [
    source 108
    target 771
  ]
  edge [
    source 109
    target 115
  ]
  edge [
    source 109
    target 121
  ]
  edge [
    source 109
    target 122
  ]
  edge [
    source 109
    target 418
  ]
  edge [
    source 109
    target 772
  ]
  edge [
    source 109
    target 773
  ]
  edge [
    source 109
    target 774
  ]
  edge [
    source 109
    target 775
  ]
  edge [
    source 109
    target 561
  ]
  edge [
    source 109
    target 776
  ]
  edge [
    source 109
    target 777
  ]
  edge [
    source 109
    target 778
  ]
  edge [
    source 109
    target 113
  ]
  edge [
    source 110
    target 111
  ]
  edge [
    source 110
    target 779
  ]
  edge [
    source 110
    target 780
  ]
  edge [
    source 110
    target 781
  ]
  edge [
    source 110
    target 782
  ]
  edge [
    source 110
    target 783
  ]
  edge [
    source 110
    target 784
  ]
  edge [
    source 110
    target 785
  ]
  edge [
    source 110
    target 786
  ]
  edge [
    source 110
    target 787
  ]
  edge [
    source 110
    target 788
  ]
  edge [
    source 110
    target 789
  ]
  edge [
    source 110
    target 790
  ]
  edge [
    source 110
    target 791
  ]
  edge [
    source 110
    target 792
  ]
  edge [
    source 110
    target 793
  ]
  edge [
    source 110
    target 122
  ]
  edge [
    source 111
    target 112
  ]
  edge [
    source 111
    target 794
  ]
  edge [
    source 111
    target 795
  ]
  edge [
    source 111
    target 796
  ]
  edge [
    source 111
    target 797
  ]
  edge [
    source 111
    target 798
  ]
  edge [
    source 111
    target 799
  ]
  edge [
    source 111
    target 800
  ]
  edge [
    source 112
    target 113
  ]
  edge [
    source 113
    target 114
  ]
  edge [
    source 114
    target 115
  ]
  edge [
    source 114
    target 801
  ]
  edge [
    source 114
    target 802
  ]
  edge [
    source 114
    target 803
  ]
  edge [
    source 114
    target 640
  ]
  edge [
    source 114
    target 804
  ]
  edge [
    source 114
    target 805
  ]
  edge [
    source 115
    target 172
  ]
  edge [
    source 115
    target 806
  ]
  edge [
    source 115
    target 807
  ]
  edge [
    source 115
    target 808
  ]
  edge [
    source 115
    target 809
  ]
  edge [
    source 116
    target 117
  ]
  edge [
    source 117
    target 118
  ]
  edge [
    source 117
    target 810
  ]
  edge [
    source 117
    target 811
  ]
  edge [
    source 118
    target 119
  ]
  edge [
    source 119
    target 120
  ]
  edge [
    source 120
    target 121
  ]
  edge [
    source 121
    target 316
  ]
  edge [
    source 121
    target 812
  ]
  edge [
    source 122
    target 123
  ]
  edge [
    source 122
    target 813
  ]
  edge [
    source 123
    target 124
  ]
  edge [
    source 123
    target 814
  ]
  edge [
    source 123
    target 815
  ]
  edge [
    source 123
    target 816
  ]
  edge [
    source 123
    target 817
  ]
  edge [
    source 123
    target 818
  ]
  edge [
    source 123
    target 819
  ]
  edge [
    source 123
    target 820
  ]
  edge [
    source 124
    target 125
  ]
  edge [
    source 124
    target 821
  ]
  edge [
    source 124
    target 822
  ]
  edge [
    source 124
    target 823
  ]
  edge [
    source 124
    target 824
  ]
  edge [
    source 124
    target 825
  ]
  edge [
    source 124
    target 826
  ]
  edge [
    source 124
    target 827
  ]
  edge [
    source 124
    target 828
  ]
  edge [
    source 124
    target 829
  ]
  edge [
    source 124
    target 383
  ]
  edge [
    source 124
    target 830
  ]
  edge [
    source 124
    target 432
  ]
  edge [
    source 124
    target 831
  ]
  edge [
    source 124
    target 832
  ]
  edge [
    source 124
    target 833
  ]
  edge [
    source 124
    target 436
  ]
  edge [
    source 124
    target 834
  ]
  edge [
    source 125
    target 126
  ]
  edge [
    source 126
    target 127
  ]
  edge [
    source 126
    target 835
  ]
  edge [
    source 126
    target 165
  ]
  edge [
    source 126
    target 317
  ]
  edge [
    source 126
    target 134
  ]
  edge [
    source 127
    target 128
  ]
  edge [
    source 127
    target 836
  ]
  edge [
    source 127
    target 837
  ]
  edge [
    source 127
    target 838
  ]
  edge [
    source 127
    target 228
  ]
  edge [
    source 127
    target 839
  ]
  edge [
    source 127
    target 840
  ]
  edge [
    source 127
    target 841
  ]
  edge [
    source 127
    target 842
  ]
  edge [
    source 127
    target 195
  ]
  edge [
    source 127
    target 843
  ]
  edge [
    source 127
    target 135
  ]
  edge [
    source 128
    target 814
  ]
  edge [
    source 128
    target 844
  ]
  edge [
    source 128
    target 845
  ]
  edge [
    source 128
    target 846
  ]
  edge [
    source 128
    target 847
  ]
  edge [
    source 128
    target 848
  ]
  edge [
    source 128
    target 849
  ]
  edge [
    source 128
    target 850
  ]
  edge [
    source 128
    target 851
  ]
  edge [
    source 129
    target 852
  ]
  edge [
    source 129
    target 853
  ]
  edge [
    source 129
    target 854
  ]
  edge [
    source 129
    target 250
  ]
  edge [
    source 129
    target 855
  ]
  edge [
    source 129
    target 856
  ]
  edge [
    source 129
    target 857
  ]
  edge [
    source 130
    target 131
  ]
  edge [
    source 131
    target 132
  ]
  edge [
    source 131
    target 858
  ]
  edge [
    source 131
    target 859
  ]
  edge [
    source 131
    target 860
  ]
  edge [
    source 131
    target 861
  ]
  edge [
    source 131
    target 862
  ]
  edge [
    source 131
    target 863
  ]
  edge [
    source 131
    target 864
  ]
  edge [
    source 131
    target 721
  ]
  edge [
    source 131
    target 865
  ]
  edge [
    source 131
    target 866
  ]
  edge [
    source 133
    target 134
  ]
  edge [
    source 133
    target 867
  ]
  edge [
    source 133
    target 428
  ]
  edge [
    source 133
    target 868
  ]
  edge [
    source 133
    target 869
  ]
  edge [
    source 133
    target 870
  ]
  edge [
    source 133
    target 871
  ]
  edge [
    source 133
    target 872
  ]
  edge [
    source 133
    target 873
  ]
  edge [
    source 133
    target 874
  ]
  edge [
    source 133
    target 436
  ]
  edge [
    source 134
    target 135
  ]
  edge [
    source 135
    target 136
  ]
  edge [
    source 875
    target 876
  ]
  edge [
    source 877
    target 878
  ]
  edge [
    source 879
    target 880
  ]
  edge [
    source 881
    target 882
  ]
  edge [
    source 883
    target 884
  ]
  edge [
    source 885
    target 886
  ]
  edge [
    source 887
    target 888
  ]
]
