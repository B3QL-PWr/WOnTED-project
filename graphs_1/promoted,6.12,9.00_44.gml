graph [
  maxDegree 33
  minDegree 1
  meanDegree 2.0377358490566038
  density 0.01940700808625337
  graphCliqueNumber 3
  node [
    id 0
    label "serwis"
    origin "text"
  ]
  node [
    id 1
    label "onet"
    origin "text"
  ]
  node [
    id 2
    label "zlikwidowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "mo&#380;liwo&#347;&#263;"
    origin "text"
  ]
  node [
    id 4
    label "zamieszcza&#263;"
    origin "text"
  ]
  node [
    id 5
    label "komentarz"
    origin "text"
  ]
  node [
    id 6
    label "pod"
    origin "text"
  ]
  node [
    id 7
    label "artyku&#322;"
    origin "text"
  ]
  node [
    id 8
    label "ukazowa&#263;"
    origin "text"
  ]
  node [
    id 9
    label "si&#281;"
    origin "text"
  ]
  node [
    id 10
    label "sekcja"
    origin "text"
  ]
  node [
    id 11
    label "wiadomo&#347;ci"
    origin "text"
  ]
  node [
    id 12
    label "kultura"
    origin "text"
  ]
  node [
    id 13
    label "mecz"
  ]
  node [
    id 14
    label "service"
  ]
  node [
    id 15
    label "wytw&#243;r"
  ]
  node [
    id 16
    label "zak&#322;ad"
  ]
  node [
    id 17
    label "us&#322;uga"
  ]
  node [
    id 18
    label "uderzenie"
  ]
  node [
    id 19
    label "doniesienie"
  ]
  node [
    id 20
    label "zastawa"
  ]
  node [
    id 21
    label "YouTube"
  ]
  node [
    id 22
    label "punkt"
  ]
  node [
    id 23
    label "porcja"
  ]
  node [
    id 24
    label "strona"
  ]
  node [
    id 25
    label "za&#322;atwi&#263;"
  ]
  node [
    id 26
    label "usun&#261;&#263;"
  ]
  node [
    id 27
    label "ability"
  ]
  node [
    id 28
    label "wyb&#243;r"
  ]
  node [
    id 29
    label "prospect"
  ]
  node [
    id 30
    label "egzekutywa"
  ]
  node [
    id 31
    label "alternatywa"
  ]
  node [
    id 32
    label "potencja&#322;"
  ]
  node [
    id 33
    label "cecha"
  ]
  node [
    id 34
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 35
    label "obliczeniowo"
  ]
  node [
    id 36
    label "wydarzenie"
  ]
  node [
    id 37
    label "operator_modalny"
  ]
  node [
    id 38
    label "posiada&#263;"
  ]
  node [
    id 39
    label "umieszcza&#263;"
  ]
  node [
    id 40
    label "publikowa&#263;"
  ]
  node [
    id 41
    label "comment"
  ]
  node [
    id 42
    label "ocena"
  ]
  node [
    id 43
    label "gossip"
  ]
  node [
    id 44
    label "interpretacja"
  ]
  node [
    id 45
    label "audycja"
  ]
  node [
    id 46
    label "tekst"
  ]
  node [
    id 47
    label "dokument"
  ]
  node [
    id 48
    label "towar"
  ]
  node [
    id 49
    label "nag&#322;&#243;wek"
  ]
  node [
    id 50
    label "znak_j&#281;zykowy"
  ]
  node [
    id 51
    label "wyr&#243;b"
  ]
  node [
    id 52
    label "blok"
  ]
  node [
    id 53
    label "line"
  ]
  node [
    id 54
    label "paragraf"
  ]
  node [
    id 55
    label "rodzajnik"
  ]
  node [
    id 56
    label "prawda"
  ]
  node [
    id 57
    label "szkic"
  ]
  node [
    id 58
    label "fragment"
  ]
  node [
    id 59
    label "podsekcja"
  ]
  node [
    id 60
    label "whole"
  ]
  node [
    id 61
    label "relation"
  ]
  node [
    id 62
    label "zw&#322;oki"
  ]
  node [
    id 63
    label "grupa"
  ]
  node [
    id 64
    label "orkiestra"
  ]
  node [
    id 65
    label "zesp&#243;&#322;"
  ]
  node [
    id 66
    label "urz&#261;d"
  ]
  node [
    id 67
    label "jednostka_organizacyjna"
  ]
  node [
    id 68
    label "autopsy"
  ]
  node [
    id 69
    label "insourcing"
  ]
  node [
    id 70
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 71
    label "badanie"
  ]
  node [
    id 72
    label "ministerstwo"
  ]
  node [
    id 73
    label "miejsce_pracy"
  ]
  node [
    id 74
    label "dzia&#322;"
  ]
  node [
    id 75
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 76
    label "przedmiot"
  ]
  node [
    id 77
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 78
    label "Wsch&#243;d"
  ]
  node [
    id 79
    label "rzecz"
  ]
  node [
    id 80
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 81
    label "sztuka"
  ]
  node [
    id 82
    label "religia"
  ]
  node [
    id 83
    label "przejmowa&#263;"
  ]
  node [
    id 84
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 85
    label "makrokosmos"
  ]
  node [
    id 86
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 87
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 88
    label "zjawisko"
  ]
  node [
    id 89
    label "praca_rolnicza"
  ]
  node [
    id 90
    label "tradycja"
  ]
  node [
    id 91
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 92
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 93
    label "przejmowanie"
  ]
  node [
    id 94
    label "asymilowanie_si&#281;"
  ]
  node [
    id 95
    label "przej&#261;&#263;"
  ]
  node [
    id 96
    label "hodowla"
  ]
  node [
    id 97
    label "brzoskwiniarnia"
  ]
  node [
    id 98
    label "populace"
  ]
  node [
    id 99
    label "konwencja"
  ]
  node [
    id 100
    label "propriety"
  ]
  node [
    id 101
    label "jako&#347;&#263;"
  ]
  node [
    id 102
    label "kuchnia"
  ]
  node [
    id 103
    label "zwyczaj"
  ]
  node [
    id 104
    label "przej&#281;cie"
  ]
  node [
    id 105
    label "ro&#347;linno&#347;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 10
    target 60
  ]
  edge [
    source 10
    target 61
  ]
  edge [
    source 10
    target 62
  ]
  edge [
    source 10
    target 63
  ]
  edge [
    source 10
    target 64
  ]
  edge [
    source 10
    target 65
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 10
    target 69
  ]
  edge [
    source 10
    target 70
  ]
  edge [
    source 10
    target 71
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 75
  ]
  edge [
    source 12
    target 76
  ]
  edge [
    source 12
    target 77
  ]
  edge [
    source 12
    target 78
  ]
  edge [
    source 12
    target 79
  ]
  edge [
    source 12
    target 80
  ]
  edge [
    source 12
    target 81
  ]
  edge [
    source 12
    target 82
  ]
  edge [
    source 12
    target 83
  ]
  edge [
    source 12
    target 84
  ]
  edge [
    source 12
    target 85
  ]
  edge [
    source 12
    target 86
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 12
    target 89
  ]
  edge [
    source 12
    target 90
  ]
  edge [
    source 12
    target 91
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 12
    target 33
  ]
  edge [
    source 12
    target 94
  ]
  edge [
    source 12
    target 95
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 12
    target 105
  ]
]
