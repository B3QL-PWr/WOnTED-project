graph [
  maxDegree 23
  minDegree 1
  meanDegree 1.9746835443037976
  density 0.02531645569620253
  graphCliqueNumber 3
  node [
    id 0
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 1
    label "jutro"
    origin "text"
  ]
  node [
    id 2
    label "pi&#261;teczek"
    origin "text"
  ]
  node [
    id 3
    label "ten"
    origin "text"
  ]
  node [
    id 4
    label "okazja"
    origin "text"
  ]
  node [
    id 5
    label "mama"
    origin "text"
  ]
  node [
    id 6
    label "gra"
    origin "text"
  ]
  node [
    id 7
    label "oddanie"
    origin "text"
  ]
  node [
    id 8
    label "blisko"
  ]
  node [
    id 9
    label "przysz&#322;o&#347;&#263;"
  ]
  node [
    id 10
    label "dzie&#324;_jutrzejszy"
  ]
  node [
    id 11
    label "jutrzejszy"
  ]
  node [
    id 12
    label "dzie&#324;"
  ]
  node [
    id 13
    label "okre&#347;lony"
  ]
  node [
    id 14
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 15
    label "atrakcyjny"
  ]
  node [
    id 16
    label "oferta"
  ]
  node [
    id 17
    label "adeptness"
  ]
  node [
    id 18
    label "okazka"
  ]
  node [
    id 19
    label "wydarzenie"
  ]
  node [
    id 20
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 21
    label "podw&#243;zka"
  ]
  node [
    id 22
    label "autostop"
  ]
  node [
    id 23
    label "sytuacja"
  ]
  node [
    id 24
    label "matczysko"
  ]
  node [
    id 25
    label "macierz"
  ]
  node [
    id 26
    label "przodkini"
  ]
  node [
    id 27
    label "Matka_Boska"
  ]
  node [
    id 28
    label "macocha"
  ]
  node [
    id 29
    label "matka_zast&#281;pcza"
  ]
  node [
    id 30
    label "stara"
  ]
  node [
    id 31
    label "rodzice"
  ]
  node [
    id 32
    label "rodzic"
  ]
  node [
    id 33
    label "zabawa"
  ]
  node [
    id 34
    label "rywalizacja"
  ]
  node [
    id 35
    label "czynno&#347;&#263;"
  ]
  node [
    id 36
    label "Pok&#233;mon"
  ]
  node [
    id 37
    label "synteza"
  ]
  node [
    id 38
    label "odtworzenie"
  ]
  node [
    id 39
    label "komplet"
  ]
  node [
    id 40
    label "rekwizyt_do_gry"
  ]
  node [
    id 41
    label "odg&#322;os"
  ]
  node [
    id 42
    label "rozgrywka"
  ]
  node [
    id 43
    label "post&#281;powanie"
  ]
  node [
    id 44
    label "apparent_motion"
  ]
  node [
    id 45
    label "game"
  ]
  node [
    id 46
    label "zmienno&#347;&#263;"
  ]
  node [
    id 47
    label "zasada"
  ]
  node [
    id 48
    label "akcja"
  ]
  node [
    id 49
    label "play"
  ]
  node [
    id 50
    label "contest"
  ]
  node [
    id 51
    label "zbijany"
  ]
  node [
    id 52
    label "render"
  ]
  node [
    id 53
    label "prohibition"
  ]
  node [
    id 54
    label "doj&#347;cie"
  ]
  node [
    id 55
    label "dostarczenie"
  ]
  node [
    id 56
    label "danie"
  ]
  node [
    id 57
    label "commitment"
  ]
  node [
    id 58
    label "pass"
  ]
  node [
    id 59
    label "po&#347;wi&#281;cenie"
  ]
  node [
    id 60
    label "odpowiedzenie"
  ]
  node [
    id 61
    label "sprzedanie"
  ]
  node [
    id 62
    label "prototype"
  ]
  node [
    id 63
    label "ofiarno&#347;&#263;"
  ]
  node [
    id 64
    label "zrobienie"
  ]
  node [
    id 65
    label "reciprocation"
  ]
  node [
    id 66
    label "odst&#261;pienie"
  ]
  node [
    id 67
    label "przedstawienie"
  ]
  node [
    id 68
    label "odej&#347;cie"
  ]
  node [
    id 69
    label "za&#322;atwienie_si&#281;"
  ]
  node [
    id 70
    label "przekazanie"
  ]
  node [
    id 71
    label "wierno&#347;&#263;"
  ]
  node [
    id 72
    label "powr&#243;cenie"
  ]
  node [
    id 73
    label "umieszczenie"
  ]
  node [
    id 74
    label "Alien"
  ]
  node [
    id 75
    label "Rage"
  ]
  node [
    id 76
    label "Unlimited"
  ]
  node [
    id 77
    label "zielonka"
  ]
  node [
    id 78
    label "2"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 19
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 74
    target 75
  ]
  edge [
    source 74
    target 76
  ]
  edge [
    source 75
    target 76
  ]
  edge [
    source 77
    target 78
  ]
]
