graph [
  maxDegree 19
  minDegree 1
  meanDegree 2
  density 0.05
  graphCliqueNumber 2
  node [
    id 0
    label "uwaga"
    origin "text"
  ]
  node [
    id 1
    label "mirka"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "akcja"
    origin "text"
  ]
  node [
    id 4
    label "nagana"
  ]
  node [
    id 5
    label "wypowied&#378;"
  ]
  node [
    id 6
    label "stan"
  ]
  node [
    id 7
    label "dzienniczek"
  ]
  node [
    id 8
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 9
    label "wzgl&#261;d"
  ]
  node [
    id 10
    label "gossip"
  ]
  node [
    id 11
    label "upomnienie"
  ]
  node [
    id 12
    label "tekst"
  ]
  node [
    id 13
    label "si&#281;ga&#263;"
  ]
  node [
    id 14
    label "trwa&#263;"
  ]
  node [
    id 15
    label "obecno&#347;&#263;"
  ]
  node [
    id 16
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 17
    label "stand"
  ]
  node [
    id 18
    label "mie&#263;_miejsce"
  ]
  node [
    id 19
    label "uczestniczy&#263;"
  ]
  node [
    id 20
    label "chodzi&#263;"
  ]
  node [
    id 21
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 22
    label "equal"
  ]
  node [
    id 23
    label "zagrywka"
  ]
  node [
    id 24
    label "czyn"
  ]
  node [
    id 25
    label "czynno&#347;&#263;"
  ]
  node [
    id 26
    label "wysoko&#347;&#263;"
  ]
  node [
    id 27
    label "stock"
  ]
  node [
    id 28
    label "gra"
  ]
  node [
    id 29
    label "w&#281;ze&#322;"
  ]
  node [
    id 30
    label "instrument_strunowy"
  ]
  node [
    id 31
    label "dywidenda"
  ]
  node [
    id 32
    label "przebieg"
  ]
  node [
    id 33
    label "udzia&#322;"
  ]
  node [
    id 34
    label "occupation"
  ]
  node [
    id 35
    label "jazda"
  ]
  node [
    id 36
    label "wydarzenie"
  ]
  node [
    id 37
    label "commotion"
  ]
  node [
    id 38
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 39
    label "sp&#243;&#322;ka_akcyjna"
  ]
  node [
    id 40
    label "operacja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
]
