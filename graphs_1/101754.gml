graph [
  maxDegree 39
  minDegree 1
  meanDegree 2.0469483568075115
  density 0.009655416777393924
  graphCliqueNumber 3
  node [
    id 0
    label "punkt"
    origin "text"
  ]
  node [
    id 1
    label "wyj&#347;cie"
    origin "text"
  ]
  node [
    id 2
    label "dla"
    origin "text"
  ]
  node [
    id 3
    label "benklera"
    origin "text"
  ]
  node [
    id 4
    label "by&#263;"
    origin "text"
  ]
  node [
    id 5
    label "analiza"
    origin "text"
  ]
  node [
    id 6
    label "ekonomiczny"
    origin "text"
  ]
  node [
    id 7
    label "ale"
    origin "text"
  ]
  node [
    id 8
    label "wykszta&#322;cenie"
    origin "text"
  ]
  node [
    id 9
    label "prawnik"
    origin "text"
  ]
  node [
    id 10
    label "nic"
    origin "text"
  ]
  node [
    id 11
    label "dziwny"
    origin "text"
  ]
  node [
    id 12
    label "szuka&#263;"
    origin "text"
  ]
  node [
    id 13
    label "nast&#281;pnie"
    origin "text"
  ]
  node [
    id 14
    label "drugi"
    origin "text"
  ]
  node [
    id 15
    label "dna"
    origin "text"
  ]
  node [
    id 16
    label "potem"
    origin "text"
  ]
  node [
    id 17
    label "trzeci"
    origin "text"
  ]
  node [
    id 18
    label "czwarty"
    origin "text"
  ]
  node [
    id 19
    label "bogactwo"
    origin "text"
  ]
  node [
    id 20
    label "sie&#263;"
    origin "text"
  ]
  node [
    id 21
    label "instrukcja"
    origin "text"
  ]
  node [
    id 22
    label "rozdzia&#322;"
    origin "text"
  ]
  node [
    id 23
    label "prosta"
  ]
  node [
    id 24
    label "po&#322;o&#380;enie"
  ]
  node [
    id 25
    label "chwila"
  ]
  node [
    id 26
    label "ust&#281;p"
  ]
  node [
    id 27
    label "problemat"
  ]
  node [
    id 28
    label "kres"
  ]
  node [
    id 29
    label "mark"
  ]
  node [
    id 30
    label "pozycja"
  ]
  node [
    id 31
    label "point"
  ]
  node [
    id 32
    label "stopie&#324;_pisma"
  ]
  node [
    id 33
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 34
    label "przestrze&#324;"
  ]
  node [
    id 35
    label "wojsko"
  ]
  node [
    id 36
    label "problematyka"
  ]
  node [
    id 37
    label "zapunktowa&#263;"
  ]
  node [
    id 38
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 39
    label "obiekt_matematyczny"
  ]
  node [
    id 40
    label "sprawa"
  ]
  node [
    id 41
    label "plamka"
  ]
  node [
    id 42
    label "miejsce"
  ]
  node [
    id 43
    label "obiekt"
  ]
  node [
    id 44
    label "plan"
  ]
  node [
    id 45
    label "podpunkt"
  ]
  node [
    id 46
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 47
    label "jednostka"
  ]
  node [
    id 48
    label "transgression"
  ]
  node [
    id 49
    label "postrze&#380;enie"
  ]
  node [
    id 50
    label "withdrawal"
  ]
  node [
    id 51
    label "zagranie"
  ]
  node [
    id 52
    label "policzenie"
  ]
  node [
    id 53
    label "spotkanie"
  ]
  node [
    id 54
    label "odch&#243;d"
  ]
  node [
    id 55
    label "podziewanie_si&#281;"
  ]
  node [
    id 56
    label "uwolnienie_si&#281;"
  ]
  node [
    id 57
    label "ograniczenie"
  ]
  node [
    id 58
    label "exit"
  ]
  node [
    id 59
    label "powiedzenie_si&#281;"
  ]
  node [
    id 60
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 61
    label "wywiedzenie_si&#281;"
  ]
  node [
    id 62
    label "wypadni&#281;cie"
  ]
  node [
    id 63
    label "zako&#324;czenie"
  ]
  node [
    id 64
    label "ewentualno&#347;&#263;"
  ]
  node [
    id 65
    label "ruszenie"
  ]
  node [
    id 66
    label "emergence"
  ]
  node [
    id 67
    label "opuszczenie"
  ]
  node [
    id 68
    label "przebywanie"
  ]
  node [
    id 69
    label "deviation"
  ]
  node [
    id 70
    label "podzianie_si&#281;"
  ]
  node [
    id 71
    label "wychodzenie"
  ]
  node [
    id 72
    label "wyczerpanie_si&#281;"
  ]
  node [
    id 73
    label "ukazanie_si&#281;_drukiem"
  ]
  node [
    id 74
    label "uzyskanie"
  ]
  node [
    id 75
    label "przedstawienie"
  ]
  node [
    id 76
    label "vent"
  ]
  node [
    id 77
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 78
    label "powychodzenie"
  ]
  node [
    id 79
    label "wych&#243;d"
  ]
  node [
    id 80
    label "uko&#324;czenie"
  ]
  node [
    id 81
    label "okazanie_si&#281;"
  ]
  node [
    id 82
    label "release"
  ]
  node [
    id 83
    label "si&#281;ga&#263;"
  ]
  node [
    id 84
    label "trwa&#263;"
  ]
  node [
    id 85
    label "obecno&#347;&#263;"
  ]
  node [
    id 86
    label "stan"
  ]
  node [
    id 87
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 88
    label "stand"
  ]
  node [
    id 89
    label "mie&#263;_miejsce"
  ]
  node [
    id 90
    label "uczestniczy&#263;"
  ]
  node [
    id 91
    label "chodzi&#263;"
  ]
  node [
    id 92
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 93
    label "equal"
  ]
  node [
    id 94
    label "opis"
  ]
  node [
    id 95
    label "analysis"
  ]
  node [
    id 96
    label "reakcja_chemiczna"
  ]
  node [
    id 97
    label "dissection"
  ]
  node [
    id 98
    label "badanie"
  ]
  node [
    id 99
    label "metoda"
  ]
  node [
    id 100
    label "ekonomicznie"
  ]
  node [
    id 101
    label "korzystny"
  ]
  node [
    id 102
    label "oszcz&#281;dny"
  ]
  node [
    id 103
    label "piwo"
  ]
  node [
    id 104
    label "kwalifikacje"
  ]
  node [
    id 105
    label "niepokalanki"
  ]
  node [
    id 106
    label "sophistication"
  ]
  node [
    id 107
    label "form"
  ]
  node [
    id 108
    label "training"
  ]
  node [
    id 109
    label "pomo&#380;enie"
  ]
  node [
    id 110
    label "wiedza"
  ]
  node [
    id 111
    label "zapoznanie"
  ]
  node [
    id 112
    label "rozwini&#281;cie"
  ]
  node [
    id 113
    label "wys&#322;anie"
  ]
  node [
    id 114
    label "o&#347;wiecenie"
  ]
  node [
    id 115
    label "skolaryzacja"
  ]
  node [
    id 116
    label "udoskonalenie"
  ]
  node [
    id 117
    label "urszulanki"
  ]
  node [
    id 118
    label "prawnicy"
  ]
  node [
    id 119
    label "Machiavelli"
  ]
  node [
    id 120
    label "specjalista"
  ]
  node [
    id 121
    label "aplikant"
  ]
  node [
    id 122
    label "student"
  ]
  node [
    id 123
    label "jurysta"
  ]
  node [
    id 124
    label "miernota"
  ]
  node [
    id 125
    label "g&#243;wno"
  ]
  node [
    id 126
    label "love"
  ]
  node [
    id 127
    label "ilo&#347;&#263;"
  ]
  node [
    id 128
    label "brak"
  ]
  node [
    id 129
    label "ciura"
  ]
  node [
    id 130
    label "dziwy"
  ]
  node [
    id 131
    label "dziwnie"
  ]
  node [
    id 132
    label "inny"
  ]
  node [
    id 133
    label "ask"
  ]
  node [
    id 134
    label "try"
  ]
  node [
    id 135
    label "&#322;azi&#263;"
  ]
  node [
    id 136
    label "sprawdza&#263;"
  ]
  node [
    id 137
    label "stara&#263;_si&#281;"
  ]
  node [
    id 138
    label "kolejny"
  ]
  node [
    id 139
    label "cz&#322;owiek"
  ]
  node [
    id 140
    label "przeciwny"
  ]
  node [
    id 141
    label "sw&#243;j"
  ]
  node [
    id 142
    label "odwrotnie"
  ]
  node [
    id 143
    label "dzie&#324;"
  ]
  node [
    id 144
    label "podobny"
  ]
  node [
    id 145
    label "wt&#243;ry"
  ]
  node [
    id 146
    label "podagra"
  ]
  node [
    id 147
    label "probenecyd"
  ]
  node [
    id 148
    label "schorzenie"
  ]
  node [
    id 149
    label "neutralny"
  ]
  node [
    id 150
    label "przypadkowy"
  ]
  node [
    id 151
    label "postronnie"
  ]
  node [
    id 152
    label "partner"
  ]
  node [
    id 153
    label "dziadek"
  ]
  node [
    id 154
    label "podostatek"
  ]
  node [
    id 155
    label "fortune"
  ]
  node [
    id 156
    label "cecha"
  ]
  node [
    id 157
    label "wysyp"
  ]
  node [
    id 158
    label "zr&#243;&#380;nicowanie"
  ]
  node [
    id 159
    label "mienie"
  ]
  node [
    id 160
    label "fullness"
  ]
  node [
    id 161
    label "sytuacja"
  ]
  node [
    id 162
    label "z&#322;ote_czasy"
  ]
  node [
    id 163
    label "hipertekst"
  ]
  node [
    id 164
    label "gauze"
  ]
  node [
    id 165
    label "nitka"
  ]
  node [
    id 166
    label "mesh"
  ]
  node [
    id 167
    label "e-hazard"
  ]
  node [
    id 168
    label "netbook"
  ]
  node [
    id 169
    label "cyberprzestrze&#324;"
  ]
  node [
    id 170
    label "biznes_elektroniczny"
  ]
  node [
    id 171
    label "snu&#263;"
  ]
  node [
    id 172
    label "organization"
  ]
  node [
    id 173
    label "zasadzka"
  ]
  node [
    id 174
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 175
    label "web"
  ]
  node [
    id 176
    label "provider"
  ]
  node [
    id 177
    label "struktura"
  ]
  node [
    id 178
    label "us&#322;uga_internetowa"
  ]
  node [
    id 179
    label "punkt_dost&#281;pu"
  ]
  node [
    id 180
    label "organizacja"
  ]
  node [
    id 181
    label "mem"
  ]
  node [
    id 182
    label "vane"
  ]
  node [
    id 183
    label "podcast"
  ]
  node [
    id 184
    label "grooming"
  ]
  node [
    id 185
    label "kszta&#322;t"
  ]
  node [
    id 186
    label "strona"
  ]
  node [
    id 187
    label "wysnu&#263;"
  ]
  node [
    id 188
    label "gra_sieciowa"
  ]
  node [
    id 189
    label "instalacja"
  ]
  node [
    id 190
    label "sie&#263;_komputerowa"
  ]
  node [
    id 191
    label "net"
  ]
  node [
    id 192
    label "plecionka"
  ]
  node [
    id 193
    label "media"
  ]
  node [
    id 194
    label "rozmieszczenie"
  ]
  node [
    id 195
    label "ulotka"
  ]
  node [
    id 196
    label "program"
  ]
  node [
    id 197
    label "instruktarz"
  ]
  node [
    id 198
    label "zbi&#243;r"
  ]
  node [
    id 199
    label "wskaz&#243;wka"
  ]
  node [
    id 200
    label "faza"
  ]
  node [
    id 201
    label "interruption"
  ]
  node [
    id 202
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 203
    label "podzia&#322;"
  ]
  node [
    id 204
    label "podrozdzia&#322;"
  ]
  node [
    id 205
    label "wydarzenie"
  ]
  node [
    id 206
    label "fragment"
  ]
  node [
    id 207
    label "myszka"
  ]
  node [
    id 208
    label "mika"
  ]
  node [
    id 209
    label "Adam"
  ]
  node [
    id 210
    label "Smith"
  ]
  node [
    id 211
    label "Wall"
  ]
  node [
    id 212
    label "street"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 9
    target 119
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 10
    target 125
  ]
  edge [
    source 10
    target 126
  ]
  edge [
    source 10
    target 127
  ]
  edge [
    source 10
    target 128
  ]
  edge [
    source 10
    target 129
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 130
  ]
  edge [
    source 11
    target 131
  ]
  edge [
    source 11
    target 132
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 133
  ]
  edge [
    source 12
    target 134
  ]
  edge [
    source 12
    target 135
  ]
  edge [
    source 12
    target 136
  ]
  edge [
    source 12
    target 137
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 138
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 139
  ]
  edge [
    source 14
    target 132
  ]
  edge [
    source 14
    target 138
  ]
  edge [
    source 14
    target 140
  ]
  edge [
    source 14
    target 141
  ]
  edge [
    source 14
    target 142
  ]
  edge [
    source 14
    target 143
  ]
  edge [
    source 14
    target 144
  ]
  edge [
    source 14
    target 145
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 146
  ]
  edge [
    source 15
    target 147
  ]
  edge [
    source 15
    target 148
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 139
  ]
  edge [
    source 17
    target 149
  ]
  edge [
    source 17
    target 150
  ]
  edge [
    source 17
    target 143
  ]
  edge [
    source 17
    target 151
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 152
  ]
  edge [
    source 18
    target 143
  ]
  edge [
    source 18
    target 153
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 154
  ]
  edge [
    source 19
    target 155
  ]
  edge [
    source 19
    target 156
  ]
  edge [
    source 19
    target 157
  ]
  edge [
    source 19
    target 158
  ]
  edge [
    source 19
    target 159
  ]
  edge [
    source 19
    target 160
  ]
  edge [
    source 19
    target 127
  ]
  edge [
    source 19
    target 161
  ]
  edge [
    source 19
    target 162
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 163
  ]
  edge [
    source 20
    target 164
  ]
  edge [
    source 20
    target 165
  ]
  edge [
    source 20
    target 166
  ]
  edge [
    source 20
    target 167
  ]
  edge [
    source 20
    target 168
  ]
  edge [
    source 20
    target 169
  ]
  edge [
    source 20
    target 170
  ]
  edge [
    source 20
    target 171
  ]
  edge [
    source 20
    target 172
  ]
  edge [
    source 20
    target 173
  ]
  edge [
    source 20
    target 174
  ]
  edge [
    source 20
    target 175
  ]
  edge [
    source 20
    target 176
  ]
  edge [
    source 20
    target 177
  ]
  edge [
    source 20
    target 178
  ]
  edge [
    source 20
    target 179
  ]
  edge [
    source 20
    target 180
  ]
  edge [
    source 20
    target 181
  ]
  edge [
    source 20
    target 182
  ]
  edge [
    source 20
    target 183
  ]
  edge [
    source 20
    target 184
  ]
  edge [
    source 20
    target 185
  ]
  edge [
    source 20
    target 186
  ]
  edge [
    source 20
    target 43
  ]
  edge [
    source 20
    target 187
  ]
  edge [
    source 20
    target 188
  ]
  edge [
    source 20
    target 189
  ]
  edge [
    source 20
    target 190
  ]
  edge [
    source 20
    target 191
  ]
  edge [
    source 20
    target 192
  ]
  edge [
    source 20
    target 193
  ]
  edge [
    source 20
    target 194
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 195
  ]
  edge [
    source 21
    target 196
  ]
  edge [
    source 21
    target 197
  ]
  edge [
    source 21
    target 198
  ]
  edge [
    source 21
    target 199
  ]
  edge [
    source 22
    target 200
  ]
  edge [
    source 22
    target 201
  ]
  edge [
    source 22
    target 202
  ]
  edge [
    source 22
    target 203
  ]
  edge [
    source 22
    target 204
  ]
  edge [
    source 22
    target 205
  ]
  edge [
    source 22
    target 206
  ]
  edge [
    source 207
    target 208
  ]
  edge [
    source 209
    target 210
  ]
  edge [
    source 211
    target 212
  ]
]
