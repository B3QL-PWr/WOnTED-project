graph [
  maxDegree 15
  minDegree 1
  meanDegree 2.4126984126984126
  density 0.03891449052739376
  graphCliqueNumber 6
  node [
    id 0
    label "ostatni"
    origin "text"
  ]
  node [
    id 1
    label "cyfra"
    origin "text"
  ]
  node [
    id 2
    label "plus"
    origin "text"
  ]
  node [
    id 3
    label "oznacza&#263;"
    origin "text"
  ]
  node [
    id 4
    label "jaki"
    origin "text"
  ]
  node [
    id 5
    label "by&#263;"
    origin "text"
  ]
  node [
    id 6
    label "xxd"
    origin "text"
  ]
  node [
    id 7
    label "cz&#322;owiek"
  ]
  node [
    id 8
    label "kolejny"
  ]
  node [
    id 9
    label "istota_&#380;ywa"
  ]
  node [
    id 10
    label "najgorszy"
  ]
  node [
    id 11
    label "aktualny"
  ]
  node [
    id 12
    label "ostatnio"
  ]
  node [
    id 13
    label "niedawno"
  ]
  node [
    id 14
    label "wi&#261;&#380;&#261;cy"
  ]
  node [
    id 15
    label "sko&#324;czony"
  ]
  node [
    id 16
    label "poprzedni"
  ]
  node [
    id 17
    label "pozosta&#322;y"
  ]
  node [
    id 18
    label "w&#261;tpliwy"
  ]
  node [
    id 19
    label "inicja&#322;"
  ]
  node [
    id 20
    label "wz&#243;r"
  ]
  node [
    id 21
    label "znak_pisarski"
  ]
  node [
    id 22
    label "warto&#347;&#263;"
  ]
  node [
    id 23
    label "wabik"
  ]
  node [
    id 24
    label "rewaluowa&#263;"
  ]
  node [
    id 25
    label "korzy&#347;&#263;"
  ]
  node [
    id 26
    label "dodawanie"
  ]
  node [
    id 27
    label "rewaluowanie"
  ]
  node [
    id 28
    label "stopie&#324;"
  ]
  node [
    id 29
    label "ocena"
  ]
  node [
    id 30
    label "zrewaluowa&#263;"
  ]
  node [
    id 31
    label "liczba"
  ]
  node [
    id 32
    label "znak_matematyczny"
  ]
  node [
    id 33
    label "strona"
  ]
  node [
    id 34
    label "zrewaluowanie"
  ]
  node [
    id 35
    label "okre&#347;la&#263;"
  ]
  node [
    id 36
    label "represent"
  ]
  node [
    id 37
    label "wyraz"
  ]
  node [
    id 38
    label "wskazywa&#263;"
  ]
  node [
    id 39
    label "stanowi&#263;"
  ]
  node [
    id 40
    label "signify"
  ]
  node [
    id 41
    label "set"
  ]
  node [
    id 42
    label "ustala&#263;"
  ]
  node [
    id 43
    label "si&#281;ga&#263;"
  ]
  node [
    id 44
    label "trwa&#263;"
  ]
  node [
    id 45
    label "obecno&#347;&#263;"
  ]
  node [
    id 46
    label "stan"
  ]
  node [
    id 47
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 48
    label "stand"
  ]
  node [
    id 49
    label "mie&#263;_miejsce"
  ]
  node [
    id 50
    label "uczestniczy&#263;"
  ]
  node [
    id 51
    label "chodzi&#263;"
  ]
  node [
    id 52
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 53
    label "equal"
  ]
  node [
    id 54
    label "XD"
  ]
  node [
    id 55
    label "1"
  ]
  node [
    id 56
    label "xd"
  ]
  node [
    id 57
    label "2"
  ]
  node [
    id 58
    label "3"
  ]
  node [
    id 59
    label "xDd"
  ]
  node [
    id 60
    label "7"
  ]
  node [
    id 61
    label "xDDDDddd"
  ]
  node [
    id 62
    label "8"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 56
  ]
  edge [
    source 54
    target 57
  ]
  edge [
    source 54
    target 58
  ]
  edge [
    source 54
    target 54
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 57
  ]
  edge [
    source 55
    target 58
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 58
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 61
  ]
  edge [
    source 59
    target 62
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 62
  ]
  edge [
    source 61
    target 62
  ]
]
