graph [
  maxDegree 12
  minDegree 1
  meanDegree 2.1052631578947367
  density 0.11695906432748537
  graphCliqueNumber 4
  node [
    id 0
    label "pluton"
    origin "text"
  ]
  node [
    id 1
    label "&#322;&#261;cznikowy"
    origin "text"
  ]
  node [
    id 2
    label "bateria"
  ]
  node [
    id 3
    label "dzia&#322;on"
  ]
  node [
    id 4
    label "kompania"
  ]
  node [
    id 5
    label "transuranowiec"
  ]
  node [
    id 6
    label "jednostka_organizacyjna"
  ]
  node [
    id 7
    label "dru&#380;yna"
  ]
  node [
    id 8
    label "aktynowiec"
  ]
  node [
    id 9
    label "pododdzia&#322;"
  ]
  node [
    id 10
    label "nr"
  ]
  node [
    id 11
    label "3"
  ]
  node [
    id 12
    label "Nr"
  ]
  node [
    id 13
    label "armia"
  ]
  node [
    id 14
    label "Krak&#243;w"
  ]
  node [
    id 15
    label "Piotr"
  ]
  node [
    id 16
    label "Dunin"
  ]
  node [
    id 17
    label "RWD"
  ]
  node [
    id 18
    label "8"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 17
    target 18
  ]
]
