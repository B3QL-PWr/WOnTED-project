graph [
  maxDegree 324
  minDegree 1
  meanDegree 2.2261208576998053
  density 0.0021718252270242003
  graphCliqueNumber 3
  node [
    id 0
    label "nieudany"
    origin "text"
  ]
  node [
    id 1
    label "nale&#380;e&#263;"
    origin "text"
  ]
  node [
    id 2
    label "uzna&#263;"
    origin "text"
  ]
  node [
    id 3
    label "pr&#243;ba"
    origin "text"
  ]
  node [
    id 4
    label "dywersyfikacja"
    origin "text"
  ]
  node [
    id 5
    label "dostawa"
    origin "text"
  ]
  node [
    id 6
    label "gaz"
    origin "text"
  ]
  node [
    id 7
    label "polska"
    origin "text"
  ]
  node [
    id 8
    label "poprzez"
    origin "text"
  ]
  node [
    id 9
    label "zawarcie"
    origin "text"
  ]
  node [
    id 10
    label "letni"
    origin "text"
  ]
  node [
    id 11
    label "umowa"
    origin "text"
  ]
  node [
    id 12
    label "katar"
    origin "text"
  ]
  node [
    id 13
    label "skropli&#263;"
    origin "text"
  ]
  node [
    id 14
    label "dostarczy&#263;"
    origin "text"
  ]
  node [
    id 15
    label "milion"
    origin "text"
  ]
  node [
    id 16
    label "ton"
    origin "text"
  ]
  node [
    id 17
    label "rok"
    origin "text"
  ]
  node [
    id 18
    label "stan"
    origin "text"
  ]
  node [
    id 19
    label "tylko"
    origin "text"
  ]
  node [
    id 20
    label "kilka"
    origin "text"
  ]
  node [
    id 21
    label "procent"
    origin "text"
  ]
  node [
    id 22
    label "roczny"
    origin "text"
  ]
  node [
    id 23
    label "zu&#380;ycie"
    origin "text"
  ]
  node [
    id 24
    label "przy"
    origin "text"
  ]
  node [
    id 25
    label "rosn&#261;&#263;"
    origin "text"
  ]
  node [
    id 26
    label "konsumpcja"
    origin "text"
  ]
  node [
    id 27
    label "udzia&#322;"
    origin "text"
  ]
  node [
    id 28
    label "ten"
    origin "text"
  ]
  node [
    id 29
    label "by&#263;"
    origin "text"
  ]
  node [
    id 30
    label "male&#263;"
    origin "text"
  ]
  node [
    id 31
    label "powa&#380;ny"
    origin "text"
  ]
  node [
    id 32
    label "w&#261;tpliwo&#347;&#263;"
    origin "text"
  ]
  node [
    id 33
    label "budzi&#263;"
    origin "text"
  ]
  node [
    id 34
    label "te&#380;"
    origin "text"
  ]
  node [
    id 35
    label "cena"
    origin "text"
  ]
  node [
    id 36
    label "dolar"
    origin "text"
  ]
  node [
    id 37
    label "tona"
    origin "text"
  ]
  node [
    id 38
    label "przeliczenie"
    origin "text"
  ]
  node [
    id 39
    label "oczywi&#347;cie"
    origin "text"
  ]
  node [
    id 40
    label "albo"
    origin "text"
  ]
  node [
    id 41
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 42
    label "ni&#380;"
    origin "text"
  ]
  node [
    id 43
    label "uzyskiwa&#263;"
    origin "text"
  ]
  node [
    id 44
    label "kontrakt"
    origin "text"
  ]
  node [
    id 45
    label "rosyjski"
    origin "text"
  ]
  node [
    id 46
    label "dodatkowo"
    origin "text"
  ]
  node [
    id 47
    label "konieczna"
    origin "text"
  ]
  node [
    id 48
    label "ponie&#347;&#263;"
    origin "text"
  ]
  node [
    id 49
    label "koszt"
    origin "text"
  ]
  node [
    id 50
    label "rozbudowa"
    origin "text"
  ]
  node [
    id 51
    label "infrastruktura"
    origin "text"
  ]
  node [
    id 52
    label "gazowniczy"
    origin "text"
  ]
  node [
    id 53
    label "l&#261;d"
    origin "text"
  ]
  node [
    id 54
    label "budowa"
    origin "text"
  ]
  node [
    id 55
    label "terminal"
    origin "text"
  ]
  node [
    id 56
    label "ba&#322;tyk"
    origin "text"
  ]
  node [
    id 57
    label "transport"
    origin "text"
  ]
  node [
    id 58
    label "tym"
    origin "text"
  ]
  node [
    id 59
    label "bardzo"
    origin "text"
  ]
  node [
    id 60
    label "posiada&#263;"
    origin "text"
  ]
  node [
    id 61
    label "flota"
    origin "text"
  ]
  node [
    id 62
    label "metanowc&#243;w"
    origin "text"
  ]
  node [
    id 63
    label "przepustowo&#347;&#263;"
    origin "text"
  ]
  node [
    id 64
    label "miliard"
    origin "text"
  ]
  node [
    id 65
    label "zakup"
    origin "text"
  ]
  node [
    id 66
    label "z&#322;oty"
    origin "text"
  ]
  node [
    id 67
    label "czy&#380;by"
    origin "text"
  ]
  node [
    id 68
    label "dywersyfikacyjny"
    origin "text"
  ]
  node [
    id 69
    label "pomys&#322;"
    origin "text"
  ]
  node [
    id 70
    label "mie&#263;"
    origin "text"
  ]
  node [
    id 71
    label "znowu"
    origin "text"
  ]
  node [
    id 72
    label "zap&#322;aci&#263;"
    origin "text"
  ]
  node [
    id 73
    label "polski"
    origin "text"
  ]
  node [
    id 74
    label "konsument"
    origin "text"
  ]
  node [
    id 75
    label "czas"
    origin "text"
  ]
  node [
    id 76
    label "tak"
    origin "text"
  ]
  node [
    id 77
    label "rz&#261;d"
    origin "text"
  ]
  node [
    id 78
    label "donald"
    origin "text"
  ]
  node [
    id 79
    label "tusk"
    origin "text"
  ]
  node [
    id 80
    label "potrafi&#263;"
    origin "text"
  ]
  node [
    id 81
    label "zabezpieczy&#263;"
    origin "text"
  ]
  node [
    id 82
    label "interes"
    origin "text"
  ]
  node [
    id 83
    label "rozmowa"
    origin "text"
  ]
  node [
    id 84
    label "strona"
    origin "text"
  ]
  node [
    id 85
    label "kraj"
    origin "text"
  ]
  node [
    id 86
    label "unia"
    origin "text"
  ]
  node [
    id 87
    label "europejski"
    origin "text"
  ]
  node [
    id 88
    label "my&#347;l"
    origin "text"
  ]
  node [
    id 89
    label "gazoci&#261;g"
    origin "text"
  ]
  node [
    id 90
    label "ba&#322;tycki"
    origin "text"
  ]
  node [
    id 91
    label "doprowadzi&#263;"
    origin "text"
  ]
  node [
    id 92
    label "najdro&#380;szy"
    origin "text"
  ]
  node [
    id 93
    label "nie"
    origin "text"
  ]
  node [
    id 94
    label "je&#347;li"
    origin "text"
  ]
  node [
    id 95
    label "sensowny"
    origin "text"
  ]
  node [
    id 96
    label "funkcjonowanie"
    origin "text"
  ]
  node [
    id 97
    label "lng"
    origin "text"
  ]
  node [
    id 98
    label "z&#322;y"
  ]
  node [
    id 99
    label "nieudanie"
  ]
  node [
    id 100
    label "nieciekawy"
  ]
  node [
    id 101
    label "trza"
  ]
  node [
    id 102
    label "uczestniczy&#263;"
  ]
  node [
    id 103
    label "zalicza&#263;_si&#281;"
  ]
  node [
    id 104
    label "para"
  ]
  node [
    id 105
    label "necessity"
  ]
  node [
    id 106
    label "rede"
  ]
  node [
    id 107
    label "assent"
  ]
  node [
    id 108
    label "oceni&#263;"
  ]
  node [
    id 109
    label "przyzna&#263;"
  ]
  node [
    id 110
    label "stwierdzi&#263;"
  ]
  node [
    id 111
    label "see"
  ]
  node [
    id 112
    label "usi&#322;owanie"
  ]
  node [
    id 113
    label "pobiera&#263;"
  ]
  node [
    id 114
    label "spotkanie"
  ]
  node [
    id 115
    label "analiza_chemiczna"
  ]
  node [
    id 116
    label "test"
  ]
  node [
    id 117
    label "znak"
  ]
  node [
    id 118
    label "item"
  ]
  node [
    id 119
    label "ilo&#347;&#263;"
  ]
  node [
    id 120
    label "effort"
  ]
  node [
    id 121
    label "czynno&#347;&#263;"
  ]
  node [
    id 122
    label "metal_szlachetny"
  ]
  node [
    id 123
    label "pobranie"
  ]
  node [
    id 124
    label "pobieranie"
  ]
  node [
    id 125
    label "sytuacja"
  ]
  node [
    id 126
    label "do&#347;wiadczenie"
  ]
  node [
    id 127
    label "probiernictwo"
  ]
  node [
    id 128
    label "zbi&#243;r"
  ]
  node [
    id 129
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 130
    label "pobra&#263;"
  ]
  node [
    id 131
    label "rezultat"
  ]
  node [
    id 132
    label "podzia&#322;"
  ]
  node [
    id 133
    label "poj&#281;cie"
  ]
  node [
    id 134
    label "towar"
  ]
  node [
    id 135
    label "dostawianie"
  ]
  node [
    id 136
    label "cosinus"
  ]
  node [
    id 137
    label "dostawia&#263;"
  ]
  node [
    id 138
    label "dobro"
  ]
  node [
    id 139
    label "import"
  ]
  node [
    id 140
    label "dostawienie"
  ]
  node [
    id 141
    label "dostawi&#263;"
  ]
  node [
    id 142
    label "cia&#322;o"
  ]
  node [
    id 143
    label "peda&#322;"
  ]
  node [
    id 144
    label "przy&#347;piesznik"
  ]
  node [
    id 145
    label "p&#322;omie&#324;"
  ]
  node [
    id 146
    label "paliwo"
  ]
  node [
    id 147
    label "instalacja"
  ]
  node [
    id 148
    label "gas"
  ]
  node [
    id 149
    label "bro&#324;"
  ]
  node [
    id 150
    label "accelerator"
  ]
  node [
    id 151
    label "rozpr&#281;&#380;liwo&#347;&#263;"
  ]
  node [
    id 152
    label "substancja"
  ]
  node [
    id 153
    label "stan_skupienia"
  ]
  node [
    id 154
    label "termojonizacja"
  ]
  node [
    id 155
    label "uk&#322;ad"
  ]
  node [
    id 156
    label "zrobienie"
  ]
  node [
    id 157
    label "dissolution"
  ]
  node [
    id 158
    label "spowodowanie"
  ]
  node [
    id 159
    label "zawieranie"
  ]
  node [
    id 160
    label "znajomy"
  ]
  node [
    id 161
    label "inclusion"
  ]
  node [
    id 162
    label "ustalenie"
  ]
  node [
    id 163
    label "zapoznanie_si&#281;"
  ]
  node [
    id 164
    label "pozamykanie"
  ]
  node [
    id 165
    label "zapoznanie"
  ]
  node [
    id 166
    label "uchwalenie"
  ]
  node [
    id 167
    label "umawianie_si&#281;"
  ]
  node [
    id 168
    label "zmieszczenie"
  ]
  node [
    id 169
    label "zamkni&#281;cie_si&#281;"
  ]
  node [
    id 170
    label "um&#243;wienie_si&#281;"
  ]
  node [
    id 171
    label "przyskrzynienie"
  ]
  node [
    id 172
    label "nijaki"
  ]
  node [
    id 173
    label "sezonowy"
  ]
  node [
    id 174
    label "letnio"
  ]
  node [
    id 175
    label "s&#322;oneczny"
  ]
  node [
    id 176
    label "weso&#322;y"
  ]
  node [
    id 177
    label "oboj&#281;tny"
  ]
  node [
    id 178
    label "latowy"
  ]
  node [
    id 179
    label "ciep&#322;y"
  ]
  node [
    id 180
    label "typowy"
  ]
  node [
    id 181
    label "czyn"
  ]
  node [
    id 182
    label "contract"
  ]
  node [
    id 183
    label "gestia_transportowa"
  ]
  node [
    id 184
    label "zawrze&#263;"
  ]
  node [
    id 185
    label "klauzula"
  ]
  node [
    id 186
    label "porozumienie"
  ]
  node [
    id 187
    label "warunek"
  ]
  node [
    id 188
    label "si&#261;ka&#263;"
  ]
  node [
    id 189
    label "nos"
  ]
  node [
    id 190
    label "inflammation"
  ]
  node [
    id 191
    label "albigens"
  ]
  node [
    id 192
    label "oznaka"
  ]
  node [
    id 193
    label "Katarzy"
  ]
  node [
    id 194
    label "smarka&#263;"
  ]
  node [
    id 195
    label "heretyk"
  ]
  node [
    id 196
    label "nie&#380;yt"
  ]
  node [
    id 197
    label "smark"
  ]
  node [
    id 198
    label "scattering"
  ]
  node [
    id 199
    label "zwil&#380;y&#263;"
  ]
  node [
    id 200
    label "condensation"
  ]
  node [
    id 201
    label "spowodowa&#263;"
  ]
  node [
    id 202
    label "zmieni&#263;_w&#322;a&#347;ciwo&#347;ci_fizyczne_lub_chemiczne_czego&#347;"
  ]
  node [
    id 203
    label "wytworzy&#263;"
  ]
  node [
    id 204
    label "give"
  ]
  node [
    id 205
    label "picture"
  ]
  node [
    id 206
    label "liczba"
  ]
  node [
    id 207
    label "miljon"
  ]
  node [
    id 208
    label "ba&#324;ka"
  ]
  node [
    id 209
    label "seria"
  ]
  node [
    id 210
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 211
    label "d&#378;wi&#281;k"
  ]
  node [
    id 212
    label "formality"
  ]
  node [
    id 213
    label "heksachord"
  ]
  node [
    id 214
    label "note"
  ]
  node [
    id 215
    label "akcent"
  ]
  node [
    id 216
    label "chromatyczno&#347;&#263;"
  ]
  node [
    id 217
    label "ubarwienie"
  ]
  node [
    id 218
    label "tone"
  ]
  node [
    id 219
    label "rejestr"
  ]
  node [
    id 220
    label "neoproterozoik"
  ]
  node [
    id 221
    label "interwa&#322;"
  ]
  node [
    id 222
    label "wieloton"
  ]
  node [
    id 223
    label "cecha"
  ]
  node [
    id 224
    label "r&#243;&#380;nica"
  ]
  node [
    id 225
    label "kolorystyka"
  ]
  node [
    id 226
    label "modalizm"
  ]
  node [
    id 227
    label "glinka"
  ]
  node [
    id 228
    label "zabarwienie"
  ]
  node [
    id 229
    label "sound"
  ]
  node [
    id 230
    label "repetycja"
  ]
  node [
    id 231
    label "zwyczaj"
  ]
  node [
    id 232
    label "tu&#324;czyk"
  ]
  node [
    id 233
    label "solmizacja"
  ]
  node [
    id 234
    label "jednostka"
  ]
  node [
    id 235
    label "stulecie"
  ]
  node [
    id 236
    label "kalendarz"
  ]
  node [
    id 237
    label "pora_roku"
  ]
  node [
    id 238
    label "cykl_astronomiczny"
  ]
  node [
    id 239
    label "p&#243;&#322;rocze"
  ]
  node [
    id 240
    label "grupa"
  ]
  node [
    id 241
    label "kwarta&#322;"
  ]
  node [
    id 242
    label "kurs"
  ]
  node [
    id 243
    label "jubileusz"
  ]
  node [
    id 244
    label "miesi&#261;c"
  ]
  node [
    id 245
    label "lata"
  ]
  node [
    id 246
    label "martwy_sezon"
  ]
  node [
    id 247
    label "Arizona"
  ]
  node [
    id 248
    label "Georgia"
  ]
  node [
    id 249
    label "warstwa"
  ]
  node [
    id 250
    label "jednostka_administracyjna"
  ]
  node [
    id 251
    label "Goa"
  ]
  node [
    id 252
    label "Hawaje"
  ]
  node [
    id 253
    label "Floryda"
  ]
  node [
    id 254
    label "Oklahoma"
  ]
  node [
    id 255
    label "punkt"
  ]
  node [
    id 256
    label "Alaska"
  ]
  node [
    id 257
    label "Alabama"
  ]
  node [
    id 258
    label "wci&#281;cie"
  ]
  node [
    id 259
    label "Oregon"
  ]
  node [
    id 260
    label "poziom"
  ]
  node [
    id 261
    label "Teksas"
  ]
  node [
    id 262
    label "Illinois"
  ]
  node [
    id 263
    label "Jukatan"
  ]
  node [
    id 264
    label "Waszyngton"
  ]
  node [
    id 265
    label "shape"
  ]
  node [
    id 266
    label "Nowy_Meksyk"
  ]
  node [
    id 267
    label "state"
  ]
  node [
    id 268
    label "Nowy_York"
  ]
  node [
    id 269
    label "Arakan"
  ]
  node [
    id 270
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 271
    label "Kalifornia"
  ]
  node [
    id 272
    label "wektor"
  ]
  node [
    id 273
    label "Massachusetts"
  ]
  node [
    id 274
    label "miejsce"
  ]
  node [
    id 275
    label "Pensylwania"
  ]
  node [
    id 276
    label "Maryland"
  ]
  node [
    id 277
    label "Michigan"
  ]
  node [
    id 278
    label "Ohio"
  ]
  node [
    id 279
    label "Kansas"
  ]
  node [
    id 280
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 281
    label "Luizjana"
  ]
  node [
    id 282
    label "samopoczucie"
  ]
  node [
    id 283
    label "Wirginia"
  ]
  node [
    id 284
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 285
    label "&#347;ledziowate"
  ]
  node [
    id 286
    label "ryba"
  ]
  node [
    id 287
    label "promil"
  ]
  node [
    id 288
    label "doch&#243;d"
  ]
  node [
    id 289
    label "baza_odsetkowa"
  ]
  node [
    id 290
    label "kredyt"
  ]
  node [
    id 291
    label "u&#322;amek"
  ]
  node [
    id 292
    label "symmetry"
  ]
  node [
    id 293
    label "kilkunastomiesi&#281;czny"
  ]
  node [
    id 294
    label "use"
  ]
  node [
    id 295
    label "wydanie"
  ]
  node [
    id 296
    label "exhaustion"
  ]
  node [
    id 297
    label "robi&#263;_si&#281;"
  ]
  node [
    id 298
    label "pulchnie&#263;"
  ]
  node [
    id 299
    label "proceed"
  ]
  node [
    id 300
    label "rodzi&#263;_si&#281;"
  ]
  node [
    id 301
    label "powi&#281;ksza&#263;"
  ]
  node [
    id 302
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 303
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 304
    label "augment"
  ]
  node [
    id 305
    label "wzbija&#263;_si&#281;"
  ]
  node [
    id 306
    label "rozwija&#263;_si&#281;"
  ]
  node [
    id 307
    label "podnosi&#263;_si&#281;"
  ]
  node [
    id 308
    label "narasta&#263;"
  ]
  node [
    id 309
    label "heighten"
  ]
  node [
    id 310
    label "raise"
  ]
  node [
    id 311
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 312
    label "sprout"
  ]
  node [
    id 313
    label "produkt_krajowy_brutto"
  ]
  node [
    id 314
    label "produkt_narodowy_brutto"
  ]
  node [
    id 315
    label "pulmonary_tuberculosis"
  ]
  node [
    id 316
    label "anoreksja_gospodarcza"
  ]
  node [
    id 317
    label "obecno&#347;&#263;"
  ]
  node [
    id 318
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 319
    label "kwota"
  ]
  node [
    id 320
    label "okre&#347;lony"
  ]
  node [
    id 321
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 322
    label "si&#281;ga&#263;"
  ]
  node [
    id 323
    label "trwa&#263;"
  ]
  node [
    id 324
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 325
    label "stand"
  ]
  node [
    id 326
    label "mie&#263;_miejsce"
  ]
  node [
    id 327
    label "chodzi&#263;"
  ]
  node [
    id 328
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 329
    label "equal"
  ]
  node [
    id 330
    label "widzie&#263;"
  ]
  node [
    id 331
    label "relax"
  ]
  node [
    id 332
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 333
    label "slack"
  ]
  node [
    id 334
    label "zmniejsza&#263;_si&#281;"
  ]
  node [
    id 335
    label "gro&#378;ny"
  ]
  node [
    id 336
    label "trudny"
  ]
  node [
    id 337
    label "du&#380;y"
  ]
  node [
    id 338
    label "spowa&#380;nienie"
  ]
  node [
    id 339
    label "prawdziwy"
  ]
  node [
    id 340
    label "powa&#380;nienie"
  ]
  node [
    id 341
    label "powa&#380;nie"
  ]
  node [
    id 342
    label "ci&#281;&#380;ko"
  ]
  node [
    id 343
    label "ci&#281;&#380;ki"
  ]
  node [
    id 344
    label "w&#261;tpienie"
  ]
  node [
    id 345
    label "wypowied&#378;"
  ]
  node [
    id 346
    label "wytw&#243;r"
  ]
  node [
    id 347
    label "question"
  ]
  node [
    id 348
    label "wywo&#322;ywa&#263;"
  ]
  node [
    id 349
    label "wyrywa&#263;"
  ]
  node [
    id 350
    label "prompt"
  ]
  node [
    id 351
    label "o&#380;ywia&#263;"
  ]
  node [
    id 352
    label "go"
  ]
  node [
    id 353
    label "cenowa_zmiana_poda&#380;y"
  ]
  node [
    id 354
    label "warto&#347;&#263;"
  ]
  node [
    id 355
    label "cenowa_elastyczno&#347;&#263;_popytu"
  ]
  node [
    id 356
    label "dyskryminacja_cenowa"
  ]
  node [
    id 357
    label "inflacja"
  ]
  node [
    id 358
    label "kupowanie"
  ]
  node [
    id 359
    label "kosztowa&#263;"
  ]
  node [
    id 360
    label "kosztowanie"
  ]
  node [
    id 361
    label "worth"
  ]
  node [
    id 362
    label "wyceni&#263;"
  ]
  node [
    id 363
    label "wycenienie"
  ]
  node [
    id 364
    label "Palau"
  ]
  node [
    id 365
    label "Zimbabwe"
  ]
  node [
    id 366
    label "Panama"
  ]
  node [
    id 367
    label "Mikronezja"
  ]
  node [
    id 368
    label "Wyspy_Marshalla"
  ]
  node [
    id 369
    label "Mariany_P&#243;&#322;nocne"
  ]
  node [
    id 370
    label "cent"
  ]
  node [
    id 371
    label "Timor_Wschodni"
  ]
  node [
    id 372
    label "Salwador"
  ]
  node [
    id 373
    label "Sint_Eustatius"
  ]
  node [
    id 374
    label "Saba"
  ]
  node [
    id 375
    label "Bonaire"
  ]
  node [
    id 376
    label "Portoryko"
  ]
  node [
    id 377
    label "USA"
  ]
  node [
    id 378
    label "jednostka_monetarna"
  ]
  node [
    id 379
    label "Ekwador"
  ]
  node [
    id 380
    label "kilogram"
  ]
  node [
    id 381
    label "kilotona"
  ]
  node [
    id 382
    label "metryczna_jednostka_masy"
  ]
  node [
    id 383
    label "wymienienie"
  ]
  node [
    id 384
    label "przerachowanie"
  ]
  node [
    id 385
    label "count"
  ]
  node [
    id 386
    label "skontrolowanie"
  ]
  node [
    id 387
    label "faza"
  ]
  node [
    id 388
    label "depression"
  ]
  node [
    id 389
    label "zjawisko"
  ]
  node [
    id 390
    label "nizina"
  ]
  node [
    id 391
    label "get"
  ]
  node [
    id 392
    label "mark"
  ]
  node [
    id 393
    label "wytwarza&#263;"
  ]
  node [
    id 394
    label "powodowa&#263;"
  ]
  node [
    id 395
    label "take"
  ]
  node [
    id 396
    label "zjazd"
  ]
  node [
    id 397
    label "bryd&#380;"
  ]
  node [
    id 398
    label "akt"
  ]
  node [
    id 399
    label "agent"
  ]
  node [
    id 400
    label "attachment"
  ]
  node [
    id 401
    label "praca"
  ]
  node [
    id 402
    label "po_rosyjsku"
  ]
  node [
    id 403
    label "j&#281;zyk"
  ]
  node [
    id 404
    label "wielkoruski"
  ]
  node [
    id 405
    label "kacapski"
  ]
  node [
    id 406
    label "Russian"
  ]
  node [
    id 407
    label "rusek"
  ]
  node [
    id 408
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 409
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 410
    label "dodatkowy"
  ]
  node [
    id 411
    label "wst&#261;pi&#263;"
  ]
  node [
    id 412
    label "riot"
  ]
  node [
    id 413
    label "uda&#263;_si&#281;"
  ]
  node [
    id 414
    label "porwa&#263;"
  ]
  node [
    id 415
    label "nak&#322;oni&#263;"
  ]
  node [
    id 416
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 417
    label "dozna&#263;"
  ]
  node [
    id 418
    label "carry"
  ]
  node [
    id 419
    label "nak&#322;ad"
  ]
  node [
    id 420
    label "rachunek_koszt&#243;w"
  ]
  node [
    id 421
    label "sumpt"
  ]
  node [
    id 422
    label "wydatek"
  ]
  node [
    id 423
    label "rozw&#243;j"
  ]
  node [
    id 424
    label "development"
  ]
  node [
    id 425
    label "telefonia"
  ]
  node [
    id 426
    label "trasa"
  ]
  node [
    id 427
    label "zaplecze"
  ]
  node [
    id 428
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 429
    label "radiofonia"
  ]
  node [
    id 430
    label "obszar"
  ]
  node [
    id 431
    label "skorupa_ziemska"
  ]
  node [
    id 432
    label "pojazd"
  ]
  node [
    id 433
    label "figura"
  ]
  node [
    id 434
    label "wjazd"
  ]
  node [
    id 435
    label "struktura"
  ]
  node [
    id 436
    label "konstrukcja"
  ]
  node [
    id 437
    label "r&#243;w"
  ]
  node [
    id 438
    label "kreacja"
  ]
  node [
    id 439
    label "posesja"
  ]
  node [
    id 440
    label "dokumentacja_powykonawcza"
  ]
  node [
    id 441
    label "organ"
  ]
  node [
    id 442
    label "mechanika"
  ]
  node [
    id 443
    label "zwierz&#281;"
  ]
  node [
    id 444
    label "miejsce_pracy"
  ]
  node [
    id 445
    label "constitution"
  ]
  node [
    id 446
    label "lotnisko"
  ]
  node [
    id 447
    label "port"
  ]
  node [
    id 448
    label "urz&#261;dzenie"
  ]
  node [
    id 449
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 450
    label "zawarto&#347;&#263;"
  ]
  node [
    id 451
    label "unos"
  ]
  node [
    id 452
    label "traffic"
  ]
  node [
    id 453
    label "za&#322;adunek"
  ]
  node [
    id 454
    label "gospodarka"
  ]
  node [
    id 455
    label "roz&#322;adunek"
  ]
  node [
    id 456
    label "sprz&#281;t"
  ]
  node [
    id 457
    label "jednoszynowy"
  ]
  node [
    id 458
    label "cedu&#322;a"
  ]
  node [
    id 459
    label "tyfon"
  ]
  node [
    id 460
    label "us&#322;uga"
  ]
  node [
    id 461
    label "prze&#322;adunek"
  ]
  node [
    id 462
    label "komunikacja"
  ]
  node [
    id 463
    label "w_chuj"
  ]
  node [
    id 464
    label "wiedzie&#263;"
  ]
  node [
    id 465
    label "keep_open"
  ]
  node [
    id 466
    label "zawiera&#263;"
  ]
  node [
    id 467
    label "support"
  ]
  node [
    id 468
    label "zdolno&#347;&#263;"
  ]
  node [
    id 469
    label "flotylla"
  ]
  node [
    id 470
    label "marynarka_wojenna"
  ]
  node [
    id 471
    label "formacja"
  ]
  node [
    id 472
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 473
    label "pieni&#261;dze"
  ]
  node [
    id 474
    label "eskadra"
  ]
  node [
    id 475
    label "capacity"
  ]
  node [
    id 476
    label "sprzedaj&#261;cy"
  ]
  node [
    id 477
    label "transakcja"
  ]
  node [
    id 478
    label "szlachetny"
  ]
  node [
    id 479
    label "metaliczny"
  ]
  node [
    id 480
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 481
    label "z&#322;ocenie"
  ]
  node [
    id 482
    label "grosz"
  ]
  node [
    id 483
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 484
    label "utytu&#322;owany"
  ]
  node [
    id 485
    label "poz&#322;ocenie"
  ]
  node [
    id 486
    label "Polska"
  ]
  node [
    id 487
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 488
    label "wspania&#322;y"
  ]
  node [
    id 489
    label "doskona&#322;y"
  ]
  node [
    id 490
    label "kochany"
  ]
  node [
    id 491
    label "z&#322;oci&#347;cie"
  ]
  node [
    id 492
    label "system"
  ]
  node [
    id 493
    label "idea"
  ]
  node [
    id 494
    label "ukra&#347;&#263;"
  ]
  node [
    id 495
    label "ukradzenie"
  ]
  node [
    id 496
    label "pocz&#261;tki"
  ]
  node [
    id 497
    label "czu&#263;"
  ]
  node [
    id 498
    label "need"
  ]
  node [
    id 499
    label "hide"
  ]
  node [
    id 500
    label "wy&#322;oi&#263;"
  ]
  node [
    id 501
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 502
    label "wyda&#263;"
  ]
  node [
    id 503
    label "pay"
  ]
  node [
    id 504
    label "zabuli&#263;"
  ]
  node [
    id 505
    label "lacki"
  ]
  node [
    id 506
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 507
    label "przedmiot"
  ]
  node [
    id 508
    label "sztajer"
  ]
  node [
    id 509
    label "drabant"
  ]
  node [
    id 510
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 511
    label "polak"
  ]
  node [
    id 512
    label "pierogi_ruskie"
  ]
  node [
    id 513
    label "krakowiak"
  ]
  node [
    id 514
    label "Polish"
  ]
  node [
    id 515
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 516
    label "oberek"
  ]
  node [
    id 517
    label "po_polsku"
  ]
  node [
    id 518
    label "mazur"
  ]
  node [
    id 519
    label "chodzony"
  ]
  node [
    id 520
    label "skoczny"
  ]
  node [
    id 521
    label "ryba_po_grecku"
  ]
  node [
    id 522
    label "goniony"
  ]
  node [
    id 523
    label "polsko"
  ]
  node [
    id 524
    label "rynek"
  ]
  node [
    id 525
    label "odbiorca"
  ]
  node [
    id 526
    label "go&#347;&#263;"
  ]
  node [
    id 527
    label "zjadacz"
  ]
  node [
    id 528
    label "&#322;a&#324;cuch_pokarmowy"
  ]
  node [
    id 529
    label "restauracja"
  ]
  node [
    id 530
    label "heterotrof"
  ]
  node [
    id 531
    label "klient"
  ]
  node [
    id 532
    label "u&#380;ytkownik"
  ]
  node [
    id 533
    label "czasokres"
  ]
  node [
    id 534
    label "trawienie"
  ]
  node [
    id 535
    label "kategoria_gramatyczna"
  ]
  node [
    id 536
    label "period"
  ]
  node [
    id 537
    label "odczyt"
  ]
  node [
    id 538
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 539
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 540
    label "chwila"
  ]
  node [
    id 541
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 542
    label "poprzedzenie"
  ]
  node [
    id 543
    label "koniugacja"
  ]
  node [
    id 544
    label "dzieje"
  ]
  node [
    id 545
    label "poprzedzi&#263;"
  ]
  node [
    id 546
    label "przep&#322;ywanie"
  ]
  node [
    id 547
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 548
    label "odwlekanie_si&#281;"
  ]
  node [
    id 549
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 550
    label "Zeitgeist"
  ]
  node [
    id 551
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 552
    label "okres_czasu"
  ]
  node [
    id 553
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 554
    label "pochodzi&#263;"
  ]
  node [
    id 555
    label "schy&#322;ek"
  ]
  node [
    id 556
    label "czwarty_wymiar"
  ]
  node [
    id 557
    label "chronometria"
  ]
  node [
    id 558
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 559
    label "poprzedzanie"
  ]
  node [
    id 560
    label "pogoda"
  ]
  node [
    id 561
    label "zegar"
  ]
  node [
    id 562
    label "pochodzenie"
  ]
  node [
    id 563
    label "poprzedza&#263;"
  ]
  node [
    id 564
    label "trawi&#263;"
  ]
  node [
    id 565
    label "time_period"
  ]
  node [
    id 566
    label "rachuba_czasu"
  ]
  node [
    id 567
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 568
    label "czasoprzestrze&#324;"
  ]
  node [
    id 569
    label "laba"
  ]
  node [
    id 570
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 571
    label "kategoria"
  ]
  node [
    id 572
    label "egzekutywa"
  ]
  node [
    id 573
    label "gabinet_cieni"
  ]
  node [
    id 574
    label "gromada"
  ]
  node [
    id 575
    label "premier"
  ]
  node [
    id 576
    label "Londyn"
  ]
  node [
    id 577
    label "Konsulat"
  ]
  node [
    id 578
    label "uporz&#261;dkowanie"
  ]
  node [
    id 579
    label "jednostka_systematyczna"
  ]
  node [
    id 580
    label "szpaler"
  ]
  node [
    id 581
    label "przybli&#380;enie"
  ]
  node [
    id 582
    label "tract"
  ]
  node [
    id 583
    label "number"
  ]
  node [
    id 584
    label "lon&#380;a"
  ]
  node [
    id 585
    label "w&#322;adza"
  ]
  node [
    id 586
    label "instytucja"
  ]
  node [
    id 587
    label "klasa"
  ]
  node [
    id 588
    label "umie&#263;"
  ]
  node [
    id 589
    label "cope"
  ]
  node [
    id 590
    label "potrafia&#263;"
  ]
  node [
    id 591
    label "posuwa&#263;_si&#281;"
  ]
  node [
    id 592
    label "posun&#261;&#263;_si&#281;"
  ]
  node [
    id 593
    label "can"
  ]
  node [
    id 594
    label "cover"
  ]
  node [
    id 595
    label "continue"
  ]
  node [
    id 596
    label "report"
  ]
  node [
    id 597
    label "bro&#324;_palna"
  ]
  node [
    id 598
    label "pistolet"
  ]
  node [
    id 599
    label "zainstalowa&#263;"
  ]
  node [
    id 600
    label "zapewni&#263;"
  ]
  node [
    id 601
    label "MAC"
  ]
  node [
    id 602
    label "Hortex"
  ]
  node [
    id 603
    label "reengineering"
  ]
  node [
    id 604
    label "podmiot_gospodarczy"
  ]
  node [
    id 605
    label "Google"
  ]
  node [
    id 606
    label "zaleta"
  ]
  node [
    id 607
    label "networking"
  ]
  node [
    id 608
    label "zasoby_ludzkie"
  ]
  node [
    id 609
    label "przedsi&#281;wzi&#281;cie"
  ]
  node [
    id 610
    label "Canon"
  ]
  node [
    id 611
    label "object"
  ]
  node [
    id 612
    label "HP"
  ]
  node [
    id 613
    label "Baltona"
  ]
  node [
    id 614
    label "firma"
  ]
  node [
    id 615
    label "Pewex"
  ]
  node [
    id 616
    label "MAN_SE"
  ]
  node [
    id 617
    label "Apeks"
  ]
  node [
    id 618
    label "zasoby"
  ]
  node [
    id 619
    label "Orbis"
  ]
  node [
    id 620
    label "Spo&#322;em"
  ]
  node [
    id 621
    label "sprawa"
  ]
  node [
    id 622
    label "przedsi&#281;biorczo&#347;&#263;"
  ]
  node [
    id 623
    label "Orlen"
  ]
  node [
    id 624
    label "penis"
  ]
  node [
    id 625
    label "discussion"
  ]
  node [
    id 626
    label "odpowied&#378;"
  ]
  node [
    id 627
    label "rozhowor"
  ]
  node [
    id 628
    label "cisza"
  ]
  node [
    id 629
    label "skr&#281;canie"
  ]
  node [
    id 630
    label "voice"
  ]
  node [
    id 631
    label "forma"
  ]
  node [
    id 632
    label "internet"
  ]
  node [
    id 633
    label "skr&#281;ci&#263;"
  ]
  node [
    id 634
    label "kartka"
  ]
  node [
    id 635
    label "orientowa&#263;"
  ]
  node [
    id 636
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 637
    label "powierzchnia"
  ]
  node [
    id 638
    label "plik"
  ]
  node [
    id 639
    label "bok"
  ]
  node [
    id 640
    label "pagina"
  ]
  node [
    id 641
    label "orientowanie"
  ]
  node [
    id 642
    label "fragment"
  ]
  node [
    id 643
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 644
    label "s&#261;d"
  ]
  node [
    id 645
    label "skr&#281;ca&#263;"
  ]
  node [
    id 646
    label "g&#243;ra"
  ]
  node [
    id 647
    label "serwis_internetowy"
  ]
  node [
    id 648
    label "orientacja"
  ]
  node [
    id 649
    label "linia"
  ]
  node [
    id 650
    label "skr&#281;cenie"
  ]
  node [
    id 651
    label "layout"
  ]
  node [
    id 652
    label "zorientowa&#263;"
  ]
  node [
    id 653
    label "zorientowanie"
  ]
  node [
    id 654
    label "obiekt"
  ]
  node [
    id 655
    label "podmiot"
  ]
  node [
    id 656
    label "ty&#322;"
  ]
  node [
    id 657
    label "logowanie"
  ]
  node [
    id 658
    label "adres_internetowy"
  ]
  node [
    id 659
    label "uj&#281;cie"
  ]
  node [
    id 660
    label "prz&#243;d"
  ]
  node [
    id 661
    label "posta&#263;"
  ]
  node [
    id 662
    label "Skandynawia"
  ]
  node [
    id 663
    label "Filipiny"
  ]
  node [
    id 664
    label "Rwanda"
  ]
  node [
    id 665
    label "Kaukaz"
  ]
  node [
    id 666
    label "Kaszmir"
  ]
  node [
    id 667
    label "Toskania"
  ]
  node [
    id 668
    label "Yorkshire"
  ]
  node [
    id 669
    label "&#321;emkowszczyzna"
  ]
  node [
    id 670
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 671
    label "Monako"
  ]
  node [
    id 672
    label "Amhara"
  ]
  node [
    id 673
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 674
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 675
    label "Lombardia"
  ]
  node [
    id 676
    label "Korea"
  ]
  node [
    id 677
    label "Kalabria"
  ]
  node [
    id 678
    label "Ghana"
  ]
  node [
    id 679
    label "Czarnog&#243;ra"
  ]
  node [
    id 680
    label "Tyrol"
  ]
  node [
    id 681
    label "Malawi"
  ]
  node [
    id 682
    label "Indonezja"
  ]
  node [
    id 683
    label "Bu&#322;garia"
  ]
  node [
    id 684
    label "Nauru"
  ]
  node [
    id 685
    label "Kenia"
  ]
  node [
    id 686
    label "Pamir"
  ]
  node [
    id 687
    label "Kambod&#380;a"
  ]
  node [
    id 688
    label "Lubelszczyzna"
  ]
  node [
    id 689
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 690
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 691
    label "Mali"
  ]
  node [
    id 692
    label "&#379;ywiecczyzna"
  ]
  node [
    id 693
    label "Austria"
  ]
  node [
    id 694
    label "interior"
  ]
  node [
    id 695
    label "Europa_Wschodnia"
  ]
  node [
    id 696
    label "Armenia"
  ]
  node [
    id 697
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 698
    label "Fid&#380;i"
  ]
  node [
    id 699
    label "Tuwalu"
  ]
  node [
    id 700
    label "Zabajkale"
  ]
  node [
    id 701
    label "Etiopia"
  ]
  node [
    id 702
    label "Malta"
  ]
  node [
    id 703
    label "Malezja"
  ]
  node [
    id 704
    label "Kaszuby"
  ]
  node [
    id 705
    label "Bo&#347;nia"
  ]
  node [
    id 706
    label "Noworosja"
  ]
  node [
    id 707
    label "Grenada"
  ]
  node [
    id 708
    label "Tad&#380;ykistan"
  ]
  node [
    id 709
    label "Ba&#322;kany"
  ]
  node [
    id 710
    label "Wehrlen"
  ]
  node [
    id 711
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 712
    label "Anglia"
  ]
  node [
    id 713
    label "Kielecczyzna"
  ]
  node [
    id 714
    label "Rumunia"
  ]
  node [
    id 715
    label "Pomorze_Zachodnie"
  ]
  node [
    id 716
    label "Maroko"
  ]
  node [
    id 717
    label "Bhutan"
  ]
  node [
    id 718
    label "Opolskie"
  ]
  node [
    id 719
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 720
    label "Ko&#322;yma"
  ]
  node [
    id 721
    label "Oksytania"
  ]
  node [
    id 722
    label "S&#322;owacja"
  ]
  node [
    id 723
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 724
    label "Seszele"
  ]
  node [
    id 725
    label "Syjon"
  ]
  node [
    id 726
    label "Kuwejt"
  ]
  node [
    id 727
    label "Arabia_Saudyjska"
  ]
  node [
    id 728
    label "Kociewie"
  ]
  node [
    id 729
    label "Kanada"
  ]
  node [
    id 730
    label "ziemia"
  ]
  node [
    id 731
    label "Japonia"
  ]
  node [
    id 732
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 733
    label "Hiszpania"
  ]
  node [
    id 734
    label "Botswana"
  ]
  node [
    id 735
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 736
    label "D&#380;ibuti"
  ]
  node [
    id 737
    label "Huculszczyzna"
  ]
  node [
    id 738
    label "Wietnam"
  ]
  node [
    id 739
    label "Egipt"
  ]
  node [
    id 740
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 741
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 742
    label "Burkina_Faso"
  ]
  node [
    id 743
    label "Bawaria"
  ]
  node [
    id 744
    label "Niemcy"
  ]
  node [
    id 745
    label "Khitai"
  ]
  node [
    id 746
    label "Macedonia"
  ]
  node [
    id 747
    label "Albania"
  ]
  node [
    id 748
    label "Madagaskar"
  ]
  node [
    id 749
    label "Bahrajn"
  ]
  node [
    id 750
    label "Jemen"
  ]
  node [
    id 751
    label "Lesoto"
  ]
  node [
    id 752
    label "Maghreb"
  ]
  node [
    id 753
    label "Samoa"
  ]
  node [
    id 754
    label "Andora"
  ]
  node [
    id 755
    label "Bory_Tucholskie"
  ]
  node [
    id 756
    label "Chiny"
  ]
  node [
    id 757
    label "Europa_Zachodnia"
  ]
  node [
    id 758
    label "Cypr"
  ]
  node [
    id 759
    label "Wielka_Brytania"
  ]
  node [
    id 760
    label "Kerala"
  ]
  node [
    id 761
    label "Podhale"
  ]
  node [
    id 762
    label "Kabylia"
  ]
  node [
    id 763
    label "Ukraina"
  ]
  node [
    id 764
    label "Paragwaj"
  ]
  node [
    id 765
    label "Trynidad_i_Tobago"
  ]
  node [
    id 766
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 767
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 768
    label "Ma&#322;opolska"
  ]
  node [
    id 769
    label "Polesie"
  ]
  node [
    id 770
    label "Liguria"
  ]
  node [
    id 771
    label "Libia"
  ]
  node [
    id 772
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 773
    label "&#321;&#243;dzkie"
  ]
  node [
    id 774
    label "Surinam"
  ]
  node [
    id 775
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 776
    label "Palestyna"
  ]
  node [
    id 777
    label "Australia"
  ]
  node [
    id 778
    label "Nigeria"
  ]
  node [
    id 779
    label "Honduras"
  ]
  node [
    id 780
    label "Bojkowszczyzna"
  ]
  node [
    id 781
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 782
    label "Karaiby"
  ]
  node [
    id 783
    label "Bangladesz"
  ]
  node [
    id 784
    label "Peru"
  ]
  node [
    id 785
    label "Kazachstan"
  ]
  node [
    id 786
    label "Irak"
  ]
  node [
    id 787
    label "Nepal"
  ]
  node [
    id 788
    label "S&#261;decczyzna"
  ]
  node [
    id 789
    label "Sudan"
  ]
  node [
    id 790
    label "Sand&#380;ak"
  ]
  node [
    id 791
    label "Nadrenia"
  ]
  node [
    id 792
    label "San_Marino"
  ]
  node [
    id 793
    label "Burundi"
  ]
  node [
    id 794
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 795
    label "Dominikana"
  ]
  node [
    id 796
    label "Komory"
  ]
  node [
    id 797
    label "Zakarpacie"
  ]
  node [
    id 798
    label "Gwatemala"
  ]
  node [
    id 799
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 800
    label "Zag&#243;rze"
  ]
  node [
    id 801
    label "Andaluzja"
  ]
  node [
    id 802
    label "granica_pa&#324;stwa"
  ]
  node [
    id 803
    label "Turkiestan"
  ]
  node [
    id 804
    label "Naddniestrze"
  ]
  node [
    id 805
    label "Hercegowina"
  ]
  node [
    id 806
    label "Brunei"
  ]
  node [
    id 807
    label "Iran"
  ]
  node [
    id 808
    label "Namibia"
  ]
  node [
    id 809
    label "Meksyk"
  ]
  node [
    id 810
    label "Lotaryngia"
  ]
  node [
    id 811
    label "Kamerun"
  ]
  node [
    id 812
    label "Opolszczyzna"
  ]
  node [
    id 813
    label "Afryka_Wschodnia"
  ]
  node [
    id 814
    label "Szlezwik"
  ]
  node [
    id 815
    label "Somalia"
  ]
  node [
    id 816
    label "Angola"
  ]
  node [
    id 817
    label "Gabon"
  ]
  node [
    id 818
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 819
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 820
    label "Mozambik"
  ]
  node [
    id 821
    label "Tajwan"
  ]
  node [
    id 822
    label "Tunezja"
  ]
  node [
    id 823
    label "Nowa_Zelandia"
  ]
  node [
    id 824
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 825
    label "Podbeskidzie"
  ]
  node [
    id 826
    label "Liban"
  ]
  node [
    id 827
    label "Jordania"
  ]
  node [
    id 828
    label "Tonga"
  ]
  node [
    id 829
    label "Czad"
  ]
  node [
    id 830
    label "Liberia"
  ]
  node [
    id 831
    label "Gwinea"
  ]
  node [
    id 832
    label "Belize"
  ]
  node [
    id 833
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 834
    label "Mazowsze"
  ]
  node [
    id 835
    label "&#321;otwa"
  ]
  node [
    id 836
    label "Syria"
  ]
  node [
    id 837
    label "Benin"
  ]
  node [
    id 838
    label "Afryka_Zachodnia"
  ]
  node [
    id 839
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 840
    label "Dominika"
  ]
  node [
    id 841
    label "Antigua_i_Barbuda"
  ]
  node [
    id 842
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 843
    label "Hanower"
  ]
  node [
    id 844
    label "Galicja"
  ]
  node [
    id 845
    label "Szkocja"
  ]
  node [
    id 846
    label "Walia"
  ]
  node [
    id 847
    label "Afganistan"
  ]
  node [
    id 848
    label "Kiribati"
  ]
  node [
    id 849
    label "W&#322;ochy"
  ]
  node [
    id 850
    label "Szwajcaria"
  ]
  node [
    id 851
    label "Powi&#347;le"
  ]
  node [
    id 852
    label "Sahara_Zachodnia"
  ]
  node [
    id 853
    label "Chorwacja"
  ]
  node [
    id 854
    label "Tajlandia"
  ]
  node [
    id 855
    label "Bahamy"
  ]
  node [
    id 856
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 857
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 858
    label "Zamojszczyzna"
  ]
  node [
    id 859
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 860
    label "S&#322;owenia"
  ]
  node [
    id 861
    label "Gambia"
  ]
  node [
    id 862
    label "Kujawy"
  ]
  node [
    id 863
    label "Urugwaj"
  ]
  node [
    id 864
    label "Podlasie"
  ]
  node [
    id 865
    label "Zair"
  ]
  node [
    id 866
    label "Erytrea"
  ]
  node [
    id 867
    label "Laponia"
  ]
  node [
    id 868
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 869
    label "Umbria"
  ]
  node [
    id 870
    label "Rosja"
  ]
  node [
    id 871
    label "Uganda"
  ]
  node [
    id 872
    label "Niger"
  ]
  node [
    id 873
    label "Mauritius"
  ]
  node [
    id 874
    label "Turkmenistan"
  ]
  node [
    id 875
    label "Turcja"
  ]
  node [
    id 876
    label "Mezoameryka"
  ]
  node [
    id 877
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 878
    label "Irlandia"
  ]
  node [
    id 879
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 880
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 881
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 882
    label "Gwinea_Bissau"
  ]
  node [
    id 883
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 884
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 885
    label "Kurdystan"
  ]
  node [
    id 886
    label "Belgia"
  ]
  node [
    id 887
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 888
    label "Barbados"
  ]
  node [
    id 889
    label "Chile"
  ]
  node [
    id 890
    label "Wenezuela"
  ]
  node [
    id 891
    label "W&#281;gry"
  ]
  node [
    id 892
    label "Argentyna"
  ]
  node [
    id 893
    label "Kolumbia"
  ]
  node [
    id 894
    label "Kampania"
  ]
  node [
    id 895
    label "Armagnac"
  ]
  node [
    id 896
    label "Sierra_Leone"
  ]
  node [
    id 897
    label "Azerbejd&#380;an"
  ]
  node [
    id 898
    label "Kongo"
  ]
  node [
    id 899
    label "Polinezja"
  ]
  node [
    id 900
    label "Warmia"
  ]
  node [
    id 901
    label "Pakistan"
  ]
  node [
    id 902
    label "Liechtenstein"
  ]
  node [
    id 903
    label "Wielkopolska"
  ]
  node [
    id 904
    label "Nikaragua"
  ]
  node [
    id 905
    label "Senegal"
  ]
  node [
    id 906
    label "brzeg"
  ]
  node [
    id 907
    label "Bordeaux"
  ]
  node [
    id 908
    label "Lauda"
  ]
  node [
    id 909
    label "Indie"
  ]
  node [
    id 910
    label "Mazury"
  ]
  node [
    id 911
    label "Suazi"
  ]
  node [
    id 912
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 913
    label "Algieria"
  ]
  node [
    id 914
    label "Jamajka"
  ]
  node [
    id 915
    label "Oceania"
  ]
  node [
    id 916
    label "Kostaryka"
  ]
  node [
    id 917
    label "Podkarpacie"
  ]
  node [
    id 918
    label "Lasko"
  ]
  node [
    id 919
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 920
    label "Kuba"
  ]
  node [
    id 921
    label "Mauretania"
  ]
  node [
    id 922
    label "Amazonia"
  ]
  node [
    id 923
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 924
    label "Brazylia"
  ]
  node [
    id 925
    label "Mo&#322;dawia"
  ]
  node [
    id 926
    label "organizacja"
  ]
  node [
    id 927
    label "Litwa"
  ]
  node [
    id 928
    label "Kirgistan"
  ]
  node [
    id 929
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 930
    label "Izrael"
  ]
  node [
    id 931
    label "Grecja"
  ]
  node [
    id 932
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 933
    label "Kurpie"
  ]
  node [
    id 934
    label "Holandia"
  ]
  node [
    id 935
    label "Sri_Lanka"
  ]
  node [
    id 936
    label "Tonkin"
  ]
  node [
    id 937
    label "Katar"
  ]
  node [
    id 938
    label "Azja_Wschodnia"
  ]
  node [
    id 939
    label "Ukraina_Zachodnia"
  ]
  node [
    id 940
    label "Laos"
  ]
  node [
    id 941
    label "Mongolia"
  ]
  node [
    id 942
    label "Turyngia"
  ]
  node [
    id 943
    label "Malediwy"
  ]
  node [
    id 944
    label "Zambia"
  ]
  node [
    id 945
    label "Baszkiria"
  ]
  node [
    id 946
    label "Tanzania"
  ]
  node [
    id 947
    label "Gujana"
  ]
  node [
    id 948
    label "Apulia"
  ]
  node [
    id 949
    label "Czechy"
  ]
  node [
    id 950
    label "Uzbekistan"
  ]
  node [
    id 951
    label "Gruzja"
  ]
  node [
    id 952
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 953
    label "Serbia"
  ]
  node [
    id 954
    label "Francja"
  ]
  node [
    id 955
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 956
    label "Togo"
  ]
  node [
    id 957
    label "Estonia"
  ]
  node [
    id 958
    label "Indochiny"
  ]
  node [
    id 959
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 960
    label "Oman"
  ]
  node [
    id 961
    label "Boliwia"
  ]
  node [
    id 962
    label "Portugalia"
  ]
  node [
    id 963
    label "Wyspy_Salomona"
  ]
  node [
    id 964
    label "Luksemburg"
  ]
  node [
    id 965
    label "Haiti"
  ]
  node [
    id 966
    label "Biskupizna"
  ]
  node [
    id 967
    label "Lubuskie"
  ]
  node [
    id 968
    label "Birma"
  ]
  node [
    id 969
    label "Rodezja"
  ]
  node [
    id 970
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 971
    label "partia"
  ]
  node [
    id 972
    label "Unia_Wolno&#347;ci"
  ]
  node [
    id 973
    label "Unia_Europejska"
  ]
  node [
    id 974
    label "combination"
  ]
  node [
    id 975
    label "union"
  ]
  node [
    id 976
    label "Unia"
  ]
  node [
    id 977
    label "European"
  ]
  node [
    id 978
    label "po_europejsku"
  ]
  node [
    id 979
    label "charakterystyczny"
  ]
  node [
    id 980
    label "europejsko"
  ]
  node [
    id 981
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 982
    label "istota"
  ]
  node [
    id 983
    label "thinking"
  ]
  node [
    id 984
    label "political_orientation"
  ]
  node [
    id 985
    label "szko&#322;a"
  ]
  node [
    id 986
    label "umys&#322;"
  ]
  node [
    id 987
    label "fantomatyka"
  ]
  node [
    id 988
    label "t&#322;oczenie_si&#281;"
  ]
  node [
    id 989
    label "p&#322;&#243;d"
  ]
  node [
    id 990
    label "ruroci&#261;g"
  ]
  node [
    id 991
    label "poprowadzi&#263;"
  ]
  node [
    id 992
    label "pos&#322;a&#263;"
  ]
  node [
    id 993
    label "have_a_bun_in_the_oven"
  ]
  node [
    id 994
    label "wykona&#263;"
  ]
  node [
    id 995
    label "wzbudzi&#263;"
  ]
  node [
    id 996
    label "wprowadzi&#263;"
  ]
  node [
    id 997
    label "set"
  ]
  node [
    id 998
    label "kochanek"
  ]
  node [
    id 999
    label "wybranek"
  ]
  node [
    id 1000
    label "kochanie"
  ]
  node [
    id 1001
    label "umi&#322;owany"
  ]
  node [
    id 1002
    label "drogi"
  ]
  node [
    id 1003
    label "sprzeciw"
  ]
  node [
    id 1004
    label "przyjemny"
  ]
  node [
    id 1005
    label "sensownie"
  ]
  node [
    id 1006
    label "uzasadniony"
  ]
  node [
    id 1007
    label "rozs&#261;dny"
  ]
  node [
    id 1008
    label "zrozumia&#322;y"
  ]
  node [
    id 1009
    label "skuteczny"
  ]
  node [
    id 1010
    label "nakr&#281;cenie"
  ]
  node [
    id 1011
    label "uruchomienie"
  ]
  node [
    id 1012
    label "impact"
  ]
  node [
    id 1013
    label "tr&#243;jstronny"
  ]
  node [
    id 1014
    label "w&#322;&#261;czenie"
  ]
  node [
    id 1015
    label "uruchamianie"
  ]
  node [
    id 1016
    label "podtrzymywanie"
  ]
  node [
    id 1017
    label "funkcja"
  ]
  node [
    id 1018
    label "zatrzymanie"
  ]
  node [
    id 1019
    label "dzianie_si&#281;"
  ]
  node [
    id 1020
    label "w&#322;&#261;czanie"
  ]
  node [
    id 1021
    label "nakr&#281;canie"
  ]
  node [
    id 1022
    label "Donald"
  ]
  node [
    id 1023
    label "Tusk"
  ]
  node [
    id 1024
    label "Waldemar"
  ]
  node [
    id 1025
    label "Pawlak"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 12
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 13
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 13
  ]
  edge [
    source 6
    target 16
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 151
  ]
  edge [
    source 6
    target 152
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 14
  ]
  edge [
    source 7
    target 21
  ]
  edge [
    source 7
    target 7
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 155
  ]
  edge [
    source 9
    target 156
  ]
  edge [
    source 9
    target 157
  ]
  edge [
    source 9
    target 158
  ]
  edge [
    source 9
    target 159
  ]
  edge [
    source 9
    target 160
  ]
  edge [
    source 9
    target 161
  ]
  edge [
    source 9
    target 162
  ]
  edge [
    source 9
    target 163
  ]
  edge [
    source 9
    target 164
  ]
  edge [
    source 9
    target 165
  ]
  edge [
    source 9
    target 166
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 9
    target 167
  ]
  edge [
    source 9
    target 168
  ]
  edge [
    source 9
    target 169
  ]
  edge [
    source 9
    target 170
  ]
  edge [
    source 9
    target 171
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 172
  ]
  edge [
    source 10
    target 173
  ]
  edge [
    source 10
    target 174
  ]
  edge [
    source 10
    target 175
  ]
  edge [
    source 10
    target 176
  ]
  edge [
    source 10
    target 177
  ]
  edge [
    source 10
    target 178
  ]
  edge [
    source 10
    target 179
  ]
  edge [
    source 10
    target 180
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 181
  ]
  edge [
    source 11
    target 182
  ]
  edge [
    source 11
    target 183
  ]
  edge [
    source 11
    target 184
  ]
  edge [
    source 11
    target 185
  ]
  edge [
    source 11
    target 186
  ]
  edge [
    source 11
    target 187
  ]
  edge [
    source 11
    target 44
  ]
  edge [
    source 12
    target 188
  ]
  edge [
    source 12
    target 189
  ]
  edge [
    source 12
    target 190
  ]
  edge [
    source 12
    target 191
  ]
  edge [
    source 12
    target 192
  ]
  edge [
    source 12
    target 193
  ]
  edge [
    source 12
    target 194
  ]
  edge [
    source 12
    target 195
  ]
  edge [
    source 12
    target 196
  ]
  edge [
    source 12
    target 197
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 17
  ]
  edge [
    source 13
    target 198
  ]
  edge [
    source 13
    target 199
  ]
  edge [
    source 13
    target 200
  ]
  edge [
    source 13
    target 201
  ]
  edge [
    source 13
    target 202
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 203
  ]
  edge [
    source 14
    target 204
  ]
  edge [
    source 14
    target 205
  ]
  edge [
    source 14
    target 201
  ]
  edge [
    source 14
    target 21
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 206
  ]
  edge [
    source 15
    target 207
  ]
  edge [
    source 15
    target 208
  ]
  edge [
    source 15
    target 22
  ]
  edge [
    source 16
    target 209
  ]
  edge [
    source 16
    target 210
  ]
  edge [
    source 16
    target 211
  ]
  edge [
    source 16
    target 212
  ]
  edge [
    source 16
    target 213
  ]
  edge [
    source 16
    target 214
  ]
  edge [
    source 16
    target 215
  ]
  edge [
    source 16
    target 216
  ]
  edge [
    source 16
    target 217
  ]
  edge [
    source 16
    target 218
  ]
  edge [
    source 16
    target 219
  ]
  edge [
    source 16
    target 220
  ]
  edge [
    source 16
    target 221
  ]
  edge [
    source 16
    target 222
  ]
  edge [
    source 16
    target 223
  ]
  edge [
    source 16
    target 224
  ]
  edge [
    source 16
    target 225
  ]
  edge [
    source 16
    target 226
  ]
  edge [
    source 16
    target 227
  ]
  edge [
    source 16
    target 228
  ]
  edge [
    source 16
    target 229
  ]
  edge [
    source 16
    target 230
  ]
  edge [
    source 16
    target 231
  ]
  edge [
    source 16
    target 232
  ]
  edge [
    source 16
    target 233
  ]
  edge [
    source 16
    target 234
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 235
  ]
  edge [
    source 17
    target 236
  ]
  edge [
    source 17
    target 75
  ]
  edge [
    source 17
    target 237
  ]
  edge [
    source 17
    target 238
  ]
  edge [
    source 17
    target 239
  ]
  edge [
    source 17
    target 240
  ]
  edge [
    source 17
    target 241
  ]
  edge [
    source 17
    target 242
  ]
  edge [
    source 17
    target 243
  ]
  edge [
    source 17
    target 244
  ]
  edge [
    source 17
    target 245
  ]
  edge [
    source 17
    target 246
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 247
  ]
  edge [
    source 18
    target 248
  ]
  edge [
    source 18
    target 249
  ]
  edge [
    source 18
    target 250
  ]
  edge [
    source 18
    target 251
  ]
  edge [
    source 18
    target 252
  ]
  edge [
    source 18
    target 253
  ]
  edge [
    source 18
    target 254
  ]
  edge [
    source 18
    target 255
  ]
  edge [
    source 18
    target 256
  ]
  edge [
    source 18
    target 257
  ]
  edge [
    source 18
    target 258
  ]
  edge [
    source 18
    target 259
  ]
  edge [
    source 18
    target 260
  ]
  edge [
    source 18
    target 29
  ]
  edge [
    source 18
    target 261
  ]
  edge [
    source 18
    target 262
  ]
  edge [
    source 18
    target 263
  ]
  edge [
    source 18
    target 264
  ]
  edge [
    source 18
    target 265
  ]
  edge [
    source 18
    target 266
  ]
  edge [
    source 18
    target 119
  ]
  edge [
    source 18
    target 267
  ]
  edge [
    source 18
    target 268
  ]
  edge [
    source 18
    target 269
  ]
  edge [
    source 18
    target 270
  ]
  edge [
    source 18
    target 271
  ]
  edge [
    source 18
    target 272
  ]
  edge [
    source 18
    target 273
  ]
  edge [
    source 18
    target 274
  ]
  edge [
    source 18
    target 275
  ]
  edge [
    source 18
    target 276
  ]
  edge [
    source 18
    target 277
  ]
  edge [
    source 18
    target 278
  ]
  edge [
    source 18
    target 279
  ]
  edge [
    source 18
    target 280
  ]
  edge [
    source 18
    target 281
  ]
  edge [
    source 18
    target 282
  ]
  edge [
    source 18
    target 283
  ]
  edge [
    source 18
    target 284
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 45
  ]
  edge [
    source 19
    target 90
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 285
  ]
  edge [
    source 20
    target 286
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 287
  ]
  edge [
    source 21
    target 288
  ]
  edge [
    source 21
    target 289
  ]
  edge [
    source 21
    target 290
  ]
  edge [
    source 21
    target 291
  ]
  edge [
    source 21
    target 119
  ]
  edge [
    source 21
    target 129
  ]
  edge [
    source 21
    target 292
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 293
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 294
  ]
  edge [
    source 23
    target 156
  ]
  edge [
    source 23
    target 121
  ]
  edge [
    source 23
    target 295
  ]
  edge [
    source 23
    target 119
  ]
  edge [
    source 23
    target 296
  ]
  edge [
    source 23
    target 26
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 297
  ]
  edge [
    source 25
    target 298
  ]
  edge [
    source 25
    target 299
  ]
  edge [
    source 25
    target 300
  ]
  edge [
    source 25
    target 301
  ]
  edge [
    source 25
    target 302
  ]
  edge [
    source 25
    target 303
  ]
  edge [
    source 25
    target 304
  ]
  edge [
    source 25
    target 305
  ]
  edge [
    source 25
    target 306
  ]
  edge [
    source 25
    target 307
  ]
  edge [
    source 25
    target 308
  ]
  edge [
    source 25
    target 309
  ]
  edge [
    source 25
    target 310
  ]
  edge [
    source 25
    target 311
  ]
  edge [
    source 25
    target 312
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 121
  ]
  edge [
    source 26
    target 313
  ]
  edge [
    source 26
    target 314
  ]
  edge [
    source 26
    target 315
  ]
  edge [
    source 26
    target 316
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 317
  ]
  edge [
    source 27
    target 318
  ]
  edge [
    source 27
    target 319
  ]
  edge [
    source 27
    target 119
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 67
  ]
  edge [
    source 28
    target 68
  ]
  edge [
    source 28
    target 320
  ]
  edge [
    source 28
    target 321
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 39
  ]
  edge [
    source 29
    target 40
  ]
  edge [
    source 29
    target 47
  ]
  edge [
    source 29
    target 48
  ]
  edge [
    source 29
    target 75
  ]
  edge [
    source 29
    target 76
  ]
  edge [
    source 29
    target 93
  ]
  edge [
    source 29
    target 81
  ]
  edge [
    source 29
    target 322
  ]
  edge [
    source 29
    target 323
  ]
  edge [
    source 29
    target 317
  ]
  edge [
    source 29
    target 324
  ]
  edge [
    source 29
    target 325
  ]
  edge [
    source 29
    target 326
  ]
  edge [
    source 29
    target 102
  ]
  edge [
    source 29
    target 327
  ]
  edge [
    source 29
    target 328
  ]
  edge [
    source 29
    target 329
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 330
  ]
  edge [
    source 30
    target 331
  ]
  edge [
    source 30
    target 332
  ]
  edge [
    source 30
    target 311
  ]
  edge [
    source 30
    target 333
  ]
  edge [
    source 30
    target 334
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 335
  ]
  edge [
    source 31
    target 336
  ]
  edge [
    source 31
    target 337
  ]
  edge [
    source 31
    target 338
  ]
  edge [
    source 31
    target 339
  ]
  edge [
    source 31
    target 340
  ]
  edge [
    source 31
    target 341
  ]
  edge [
    source 31
    target 342
  ]
  edge [
    source 31
    target 343
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 344
  ]
  edge [
    source 32
    target 345
  ]
  edge [
    source 32
    target 346
  ]
  edge [
    source 32
    target 347
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 348
  ]
  edge [
    source 33
    target 349
  ]
  edge [
    source 33
    target 350
  ]
  edge [
    source 33
    target 351
  ]
  edge [
    source 33
    target 352
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 353
  ]
  edge [
    source 35
    target 354
  ]
  edge [
    source 35
    target 355
  ]
  edge [
    source 35
    target 356
  ]
  edge [
    source 35
    target 357
  ]
  edge [
    source 35
    target 358
  ]
  edge [
    source 35
    target 359
  ]
  edge [
    source 35
    target 360
  ]
  edge [
    source 35
    target 361
  ]
  edge [
    source 35
    target 362
  ]
  edge [
    source 35
    target 363
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 364
  ]
  edge [
    source 36
    target 365
  ]
  edge [
    source 36
    target 366
  ]
  edge [
    source 36
    target 367
  ]
  edge [
    source 36
    target 368
  ]
  edge [
    source 36
    target 369
  ]
  edge [
    source 36
    target 370
  ]
  edge [
    source 36
    target 371
  ]
  edge [
    source 36
    target 372
  ]
  edge [
    source 36
    target 373
  ]
  edge [
    source 36
    target 374
  ]
  edge [
    source 36
    target 375
  ]
  edge [
    source 36
    target 376
  ]
  edge [
    source 36
    target 377
  ]
  edge [
    source 36
    target 378
  ]
  edge [
    source 36
    target 379
  ]
  edge [
    source 36
    target 45
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 380
  ]
  edge [
    source 37
    target 381
  ]
  edge [
    source 37
    target 382
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 383
  ]
  edge [
    source 38
    target 384
  ]
  edge [
    source 38
    target 385
  ]
  edge [
    source 38
    target 386
  ]
  edge [
    source 38
    target 234
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 87
  ]
  edge [
    source 40
    target 93
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 42
    target 260
  ]
  edge [
    source 42
    target 387
  ]
  edge [
    source 42
    target 388
  ]
  edge [
    source 42
    target 389
  ]
  edge [
    source 42
    target 390
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 391
  ]
  edge [
    source 43
    target 392
  ]
  edge [
    source 43
    target 393
  ]
  edge [
    source 43
    target 394
  ]
  edge [
    source 43
    target 395
  ]
  edge [
    source 43
    target 87
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 396
  ]
  edge [
    source 44
    target 397
  ]
  edge [
    source 44
    target 398
  ]
  edge [
    source 44
    target 399
  ]
  edge [
    source 44
    target 400
  ]
  edge [
    source 44
    target 401
  ]
  edge [
    source 44
    target 56
  ]
  edge [
    source 44
    target 55
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 84
  ]
  edge [
    source 45
    target 85
  ]
  edge [
    source 45
    target 402
  ]
  edge [
    source 45
    target 403
  ]
  edge [
    source 45
    target 404
  ]
  edge [
    source 45
    target 405
  ]
  edge [
    source 45
    target 406
  ]
  edge [
    source 45
    target 407
  ]
  edge [
    source 45
    target 408
  ]
  edge [
    source 45
    target 409
  ]
  edge [
    source 45
    target 90
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 410
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 411
  ]
  edge [
    source 48
    target 412
  ]
  edge [
    source 48
    target 413
  ]
  edge [
    source 48
    target 414
  ]
  edge [
    source 48
    target 415
  ]
  edge [
    source 48
    target 416
  ]
  edge [
    source 48
    target 417
  ]
  edge [
    source 48
    target 418
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 56
  ]
  edge [
    source 49
    target 57
  ]
  edge [
    source 49
    target 62
  ]
  edge [
    source 49
    target 54
  ]
  edge [
    source 49
    target 419
  ]
  edge [
    source 49
    target 420
  ]
  edge [
    source 49
    target 421
  ]
  edge [
    source 49
    target 422
  ]
  edge [
    source 49
    target 55
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 54
  ]
  edge [
    source 50
    target 423
  ]
  edge [
    source 50
    target 121
  ]
  edge [
    source 50
    target 424
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 425
  ]
  edge [
    source 51
    target 426
  ]
  edge [
    source 51
    target 427
  ]
  edge [
    source 51
    target 428
  ]
  edge [
    source 51
    target 429
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 430
  ]
  edge [
    source 53
    target 431
  ]
  edge [
    source 53
    target 432
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 88
  ]
  edge [
    source 54
    target 89
  ]
  edge [
    source 54
    target 433
  ]
  edge [
    source 54
    target 434
  ]
  edge [
    source 54
    target 435
  ]
  edge [
    source 54
    target 436
  ]
  edge [
    source 54
    target 437
  ]
  edge [
    source 54
    target 438
  ]
  edge [
    source 54
    target 439
  ]
  edge [
    source 54
    target 223
  ]
  edge [
    source 54
    target 440
  ]
  edge [
    source 54
    target 441
  ]
  edge [
    source 54
    target 442
  ]
  edge [
    source 54
    target 443
  ]
  edge [
    source 54
    target 444
  ]
  edge [
    source 54
    target 401
  ]
  edge [
    source 54
    target 445
  ]
  edge [
    source 54
    target 80
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 63
  ]
  edge [
    source 55
    target 96
  ]
  edge [
    source 55
    target 97
  ]
  edge [
    source 55
    target 446
  ]
  edge [
    source 55
    target 447
  ]
  edge [
    source 55
    target 274
  ]
  edge [
    source 55
    target 448
  ]
  edge [
    source 55
    target 62
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 449
  ]
  edge [
    source 57
    target 450
  ]
  edge [
    source 57
    target 451
  ]
  edge [
    source 57
    target 452
  ]
  edge [
    source 57
    target 453
  ]
  edge [
    source 57
    target 454
  ]
  edge [
    source 57
    target 455
  ]
  edge [
    source 57
    target 240
  ]
  edge [
    source 57
    target 456
  ]
  edge [
    source 57
    target 457
  ]
  edge [
    source 57
    target 458
  ]
  edge [
    source 57
    target 459
  ]
  edge [
    source 57
    target 460
  ]
  edge [
    source 57
    target 461
  ]
  edge [
    source 57
    target 462
  ]
  edge [
    source 57
    target 134
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 59
    target 463
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 464
  ]
  edge [
    source 60
    target 70
  ]
  edge [
    source 60
    target 324
  ]
  edge [
    source 60
    target 465
  ]
  edge [
    source 60
    target 466
  ]
  edge [
    source 60
    target 467
  ]
  edge [
    source 60
    target 468
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 65
  ]
  edge [
    source 61
    target 469
  ]
  edge [
    source 61
    target 470
  ]
  edge [
    source 61
    target 471
  ]
  edge [
    source 61
    target 472
  ]
  edge [
    source 61
    target 128
  ]
  edge [
    source 61
    target 473
  ]
  edge [
    source 61
    target 474
  ]
  edge [
    source 62
    target 64
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 468
  ]
  edge [
    source 63
    target 475
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 66
  ]
  edge [
    source 64
    target 206
  ]
  edge [
    source 64
    target 69
  ]
  edge [
    source 64
    target 73
  ]
  edge [
    source 65
    target 476
  ]
  edge [
    source 65
    target 138
  ]
  edge [
    source 65
    target 477
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 82
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 66
    target 478
  ]
  edge [
    source 66
    target 479
  ]
  edge [
    source 66
    target 480
  ]
  edge [
    source 66
    target 481
  ]
  edge [
    source 66
    target 482
  ]
  edge [
    source 66
    target 483
  ]
  edge [
    source 66
    target 484
  ]
  edge [
    source 66
    target 485
  ]
  edge [
    source 66
    target 486
  ]
  edge [
    source 66
    target 487
  ]
  edge [
    source 66
    target 488
  ]
  edge [
    source 66
    target 489
  ]
  edge [
    source 66
    target 490
  ]
  edge [
    source 66
    target 378
  ]
  edge [
    source 66
    target 491
  ]
  edge [
    source 66
    target 82
  ]
  edge [
    source 67
    target 83
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 69
    target 492
  ]
  edge [
    source 69
    target 346
  ]
  edge [
    source 69
    target 493
  ]
  edge [
    source 69
    target 494
  ]
  edge [
    source 69
    target 495
  ]
  edge [
    source 69
    target 496
  ]
  edge [
    source 69
    target 88
  ]
  edge [
    source 69
    target 73
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 91
  ]
  edge [
    source 70
    target 92
  ]
  edge [
    source 70
    target 324
  ]
  edge [
    source 70
    target 497
  ]
  edge [
    source 70
    target 498
  ]
  edge [
    source 70
    target 499
  ]
  edge [
    source 70
    target 467
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 72
    target 73
  ]
  edge [
    source 72
    target 500
  ]
  edge [
    source 72
    target 501
  ]
  edge [
    source 72
    target 502
  ]
  edge [
    source 72
    target 205
  ]
  edge [
    source 72
    target 503
  ]
  edge [
    source 72
    target 504
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 73
    target 81
  ]
  edge [
    source 73
    target 82
  ]
  edge [
    source 73
    target 505
  ]
  edge [
    source 73
    target 506
  ]
  edge [
    source 73
    target 507
  ]
  edge [
    source 73
    target 508
  ]
  edge [
    source 73
    target 509
  ]
  edge [
    source 73
    target 510
  ]
  edge [
    source 73
    target 511
  ]
  edge [
    source 73
    target 512
  ]
  edge [
    source 73
    target 513
  ]
  edge [
    source 73
    target 514
  ]
  edge [
    source 73
    target 403
  ]
  edge [
    source 73
    target 515
  ]
  edge [
    source 73
    target 516
  ]
  edge [
    source 73
    target 517
  ]
  edge [
    source 73
    target 518
  ]
  edge [
    source 73
    target 409
  ]
  edge [
    source 73
    target 519
  ]
  edge [
    source 73
    target 520
  ]
  edge [
    source 73
    target 521
  ]
  edge [
    source 73
    target 522
  ]
  edge [
    source 73
    target 523
  ]
  edge [
    source 74
    target 75
  ]
  edge [
    source 74
    target 524
  ]
  edge [
    source 74
    target 525
  ]
  edge [
    source 74
    target 526
  ]
  edge [
    source 74
    target 527
  ]
  edge [
    source 74
    target 528
  ]
  edge [
    source 74
    target 529
  ]
  edge [
    source 74
    target 530
  ]
  edge [
    source 74
    target 531
  ]
  edge [
    source 74
    target 532
  ]
  edge [
    source 75
    target 533
  ]
  edge [
    source 75
    target 534
  ]
  edge [
    source 75
    target 535
  ]
  edge [
    source 75
    target 536
  ]
  edge [
    source 75
    target 537
  ]
  edge [
    source 75
    target 538
  ]
  edge [
    source 75
    target 539
  ]
  edge [
    source 75
    target 540
  ]
  edge [
    source 75
    target 541
  ]
  edge [
    source 75
    target 542
  ]
  edge [
    source 75
    target 543
  ]
  edge [
    source 75
    target 544
  ]
  edge [
    source 75
    target 545
  ]
  edge [
    source 75
    target 546
  ]
  edge [
    source 75
    target 547
  ]
  edge [
    source 75
    target 548
  ]
  edge [
    source 75
    target 549
  ]
  edge [
    source 75
    target 550
  ]
  edge [
    source 75
    target 551
  ]
  edge [
    source 75
    target 552
  ]
  edge [
    source 75
    target 553
  ]
  edge [
    source 75
    target 554
  ]
  edge [
    source 75
    target 555
  ]
  edge [
    source 75
    target 556
  ]
  edge [
    source 75
    target 557
  ]
  edge [
    source 75
    target 558
  ]
  edge [
    source 75
    target 559
  ]
  edge [
    source 75
    target 560
  ]
  edge [
    source 75
    target 561
  ]
  edge [
    source 75
    target 562
  ]
  edge [
    source 75
    target 563
  ]
  edge [
    source 75
    target 564
  ]
  edge [
    source 75
    target 565
  ]
  edge [
    source 75
    target 566
  ]
  edge [
    source 75
    target 567
  ]
  edge [
    source 75
    target 568
  ]
  edge [
    source 75
    target 569
  ]
  edge [
    source 76
    target 77
  ]
  edge [
    source 77
    target 78
  ]
  edge [
    source 77
    target 570
  ]
  edge [
    source 77
    target 571
  ]
  edge [
    source 77
    target 572
  ]
  edge [
    source 77
    target 573
  ]
  edge [
    source 77
    target 574
  ]
  edge [
    source 77
    target 575
  ]
  edge [
    source 77
    target 576
  ]
  edge [
    source 77
    target 577
  ]
  edge [
    source 77
    target 578
  ]
  edge [
    source 77
    target 579
  ]
  edge [
    source 77
    target 580
  ]
  edge [
    source 77
    target 581
  ]
  edge [
    source 77
    target 582
  ]
  edge [
    source 77
    target 583
  ]
  edge [
    source 77
    target 584
  ]
  edge [
    source 77
    target 585
  ]
  edge [
    source 77
    target 586
  ]
  edge [
    source 77
    target 587
  ]
  edge [
    source 78
    target 79
  ]
  edge [
    source 78
    target 94
  ]
  edge [
    source 79
    target 80
  ]
  edge [
    source 80
    target 81
  ]
  edge [
    source 80
    target 588
  ]
  edge [
    source 80
    target 589
  ]
  edge [
    source 80
    target 590
  ]
  edge [
    source 80
    target 591
  ]
  edge [
    source 80
    target 592
  ]
  edge [
    source 80
    target 593
  ]
  edge [
    source 81
    target 594
  ]
  edge [
    source 81
    target 595
  ]
  edge [
    source 81
    target 596
  ]
  edge [
    source 81
    target 597
  ]
  edge [
    source 81
    target 201
  ]
  edge [
    source 81
    target 598
  ]
  edge [
    source 81
    target 599
  ]
  edge [
    source 81
    target 416
  ]
  edge [
    source 81
    target 600
  ]
  edge [
    source 81
    target 89
  ]
  edge [
    source 82
    target 83
  ]
  edge [
    source 82
    target 601
  ]
  edge [
    source 82
    target 602
  ]
  edge [
    source 82
    target 603
  ]
  edge [
    source 82
    target 604
  ]
  edge [
    source 82
    target 138
  ]
  edge [
    source 82
    target 605
  ]
  edge [
    source 82
    target 606
  ]
  edge [
    source 82
    target 607
  ]
  edge [
    source 82
    target 608
  ]
  edge [
    source 82
    target 609
  ]
  edge [
    source 82
    target 610
  ]
  edge [
    source 82
    target 611
  ]
  edge [
    source 82
    target 612
  ]
  edge [
    source 82
    target 613
  ]
  edge [
    source 82
    target 614
  ]
  edge [
    source 82
    target 615
  ]
  edge [
    source 82
    target 616
  ]
  edge [
    source 82
    target 617
  ]
  edge [
    source 82
    target 618
  ]
  edge [
    source 82
    target 619
  ]
  edge [
    source 82
    target 620
  ]
  edge [
    source 82
    target 621
  ]
  edge [
    source 82
    target 622
  ]
  edge [
    source 82
    target 623
  ]
  edge [
    source 82
    target 624
  ]
  edge [
    source 83
    target 84
  ]
  edge [
    source 83
    target 625
  ]
  edge [
    source 83
    target 121
  ]
  edge [
    source 83
    target 626
  ]
  edge [
    source 83
    target 627
  ]
  edge [
    source 83
    target 628
  ]
  edge [
    source 84
    target 629
  ]
  edge [
    source 84
    target 630
  ]
  edge [
    source 84
    target 631
  ]
  edge [
    source 84
    target 632
  ]
  edge [
    source 84
    target 633
  ]
  edge [
    source 84
    target 634
  ]
  edge [
    source 84
    target 635
  ]
  edge [
    source 84
    target 636
  ]
  edge [
    source 84
    target 637
  ]
  edge [
    source 84
    target 638
  ]
  edge [
    source 84
    target 639
  ]
  edge [
    source 84
    target 640
  ]
  edge [
    source 84
    target 641
  ]
  edge [
    source 84
    target 642
  ]
  edge [
    source 84
    target 643
  ]
  edge [
    source 84
    target 644
  ]
  edge [
    source 84
    target 645
  ]
  edge [
    source 84
    target 646
  ]
  edge [
    source 84
    target 647
  ]
  edge [
    source 84
    target 648
  ]
  edge [
    source 84
    target 649
  ]
  edge [
    source 84
    target 650
  ]
  edge [
    source 84
    target 651
  ]
  edge [
    source 84
    target 652
  ]
  edge [
    source 84
    target 653
  ]
  edge [
    source 84
    target 654
  ]
  edge [
    source 84
    target 655
  ]
  edge [
    source 84
    target 656
  ]
  edge [
    source 84
    target 129
  ]
  edge [
    source 84
    target 657
  ]
  edge [
    source 84
    target 658
  ]
  edge [
    source 84
    target 659
  ]
  edge [
    source 84
    target 660
  ]
  edge [
    source 84
    target 661
  ]
  edge [
    source 85
    target 86
  ]
  edge [
    source 85
    target 662
  ]
  edge [
    source 85
    target 663
  ]
  edge [
    source 85
    target 664
  ]
  edge [
    source 85
    target 665
  ]
  edge [
    source 85
    target 666
  ]
  edge [
    source 85
    target 667
  ]
  edge [
    source 85
    target 668
  ]
  edge [
    source 85
    target 669
  ]
  edge [
    source 85
    target 430
  ]
  edge [
    source 85
    target 670
  ]
  edge [
    source 85
    target 671
  ]
  edge [
    source 85
    target 672
  ]
  edge [
    source 85
    target 673
  ]
  edge [
    source 85
    target 674
  ]
  edge [
    source 85
    target 675
  ]
  edge [
    source 85
    target 676
  ]
  edge [
    source 85
    target 677
  ]
  edge [
    source 85
    target 678
  ]
  edge [
    source 85
    target 679
  ]
  edge [
    source 85
    target 680
  ]
  edge [
    source 85
    target 681
  ]
  edge [
    source 85
    target 682
  ]
  edge [
    source 85
    target 683
  ]
  edge [
    source 85
    target 684
  ]
  edge [
    source 85
    target 685
  ]
  edge [
    source 85
    target 686
  ]
  edge [
    source 85
    target 687
  ]
  edge [
    source 85
    target 688
  ]
  edge [
    source 85
    target 689
  ]
  edge [
    source 85
    target 690
  ]
  edge [
    source 85
    target 691
  ]
  edge [
    source 85
    target 692
  ]
  edge [
    source 85
    target 693
  ]
  edge [
    source 85
    target 694
  ]
  edge [
    source 85
    target 695
  ]
  edge [
    source 85
    target 696
  ]
  edge [
    source 85
    target 697
  ]
  edge [
    source 85
    target 698
  ]
  edge [
    source 85
    target 699
  ]
  edge [
    source 85
    target 700
  ]
  edge [
    source 85
    target 701
  ]
  edge [
    source 85
    target 702
  ]
  edge [
    source 85
    target 703
  ]
  edge [
    source 85
    target 704
  ]
  edge [
    source 85
    target 705
  ]
  edge [
    source 85
    target 706
  ]
  edge [
    source 85
    target 707
  ]
  edge [
    source 85
    target 708
  ]
  edge [
    source 85
    target 709
  ]
  edge [
    source 85
    target 710
  ]
  edge [
    source 85
    target 711
  ]
  edge [
    source 85
    target 712
  ]
  edge [
    source 85
    target 713
  ]
  edge [
    source 85
    target 714
  ]
  edge [
    source 85
    target 715
  ]
  edge [
    source 85
    target 716
  ]
  edge [
    source 85
    target 717
  ]
  edge [
    source 85
    target 718
  ]
  edge [
    source 85
    target 719
  ]
  edge [
    source 85
    target 720
  ]
  edge [
    source 85
    target 721
  ]
  edge [
    source 85
    target 722
  ]
  edge [
    source 85
    target 723
  ]
  edge [
    source 85
    target 724
  ]
  edge [
    source 85
    target 725
  ]
  edge [
    source 85
    target 726
  ]
  edge [
    source 85
    target 727
  ]
  edge [
    source 85
    target 728
  ]
  edge [
    source 85
    target 379
  ]
  edge [
    source 85
    target 729
  ]
  edge [
    source 85
    target 730
  ]
  edge [
    source 85
    target 731
  ]
  edge [
    source 85
    target 732
  ]
  edge [
    source 85
    target 733
  ]
  edge [
    source 85
    target 368
  ]
  edge [
    source 85
    target 734
  ]
  edge [
    source 85
    target 735
  ]
  edge [
    source 85
    target 736
  ]
  edge [
    source 85
    target 737
  ]
  edge [
    source 85
    target 738
  ]
  edge [
    source 85
    target 739
  ]
  edge [
    source 85
    target 740
  ]
  edge [
    source 85
    target 741
  ]
  edge [
    source 85
    target 742
  ]
  edge [
    source 85
    target 743
  ]
  edge [
    source 85
    target 744
  ]
  edge [
    source 85
    target 745
  ]
  edge [
    source 85
    target 746
  ]
  edge [
    source 85
    target 747
  ]
  edge [
    source 85
    target 748
  ]
  edge [
    source 85
    target 749
  ]
  edge [
    source 85
    target 750
  ]
  edge [
    source 85
    target 751
  ]
  edge [
    source 85
    target 752
  ]
  edge [
    source 85
    target 753
  ]
  edge [
    source 85
    target 754
  ]
  edge [
    source 85
    target 755
  ]
  edge [
    source 85
    target 756
  ]
  edge [
    source 85
    target 757
  ]
  edge [
    source 85
    target 758
  ]
  edge [
    source 85
    target 759
  ]
  edge [
    source 85
    target 760
  ]
  edge [
    source 85
    target 761
  ]
  edge [
    source 85
    target 762
  ]
  edge [
    source 85
    target 763
  ]
  edge [
    source 85
    target 764
  ]
  edge [
    source 85
    target 765
  ]
  edge [
    source 85
    target 766
  ]
  edge [
    source 85
    target 767
  ]
  edge [
    source 85
    target 768
  ]
  edge [
    source 85
    target 769
  ]
  edge [
    source 85
    target 770
  ]
  edge [
    source 85
    target 771
  ]
  edge [
    source 85
    target 772
  ]
  edge [
    source 85
    target 773
  ]
  edge [
    source 85
    target 774
  ]
  edge [
    source 85
    target 775
  ]
  edge [
    source 85
    target 776
  ]
  edge [
    source 85
    target 777
  ]
  edge [
    source 85
    target 778
  ]
  edge [
    source 85
    target 779
  ]
  edge [
    source 85
    target 780
  ]
  edge [
    source 85
    target 781
  ]
  edge [
    source 85
    target 782
  ]
  edge [
    source 85
    target 783
  ]
  edge [
    source 85
    target 784
  ]
  edge [
    source 85
    target 785
  ]
  edge [
    source 85
    target 377
  ]
  edge [
    source 85
    target 786
  ]
  edge [
    source 85
    target 787
  ]
  edge [
    source 85
    target 788
  ]
  edge [
    source 85
    target 789
  ]
  edge [
    source 85
    target 790
  ]
  edge [
    source 85
    target 791
  ]
  edge [
    source 85
    target 792
  ]
  edge [
    source 85
    target 793
  ]
  edge [
    source 85
    target 794
  ]
  edge [
    source 85
    target 795
  ]
  edge [
    source 85
    target 796
  ]
  edge [
    source 85
    target 797
  ]
  edge [
    source 85
    target 798
  ]
  edge [
    source 85
    target 799
  ]
  edge [
    source 85
    target 800
  ]
  edge [
    source 85
    target 801
  ]
  edge [
    source 85
    target 802
  ]
  edge [
    source 85
    target 803
  ]
  edge [
    source 85
    target 804
  ]
  edge [
    source 85
    target 805
  ]
  edge [
    source 85
    target 806
  ]
  edge [
    source 85
    target 807
  ]
  edge [
    source 85
    target 250
  ]
  edge [
    source 85
    target 365
  ]
  edge [
    source 85
    target 808
  ]
  edge [
    source 85
    target 809
  ]
  edge [
    source 85
    target 810
  ]
  edge [
    source 85
    target 811
  ]
  edge [
    source 85
    target 812
  ]
  edge [
    source 85
    target 813
  ]
  edge [
    source 85
    target 814
  ]
  edge [
    source 85
    target 815
  ]
  edge [
    source 85
    target 816
  ]
  edge [
    source 85
    target 817
  ]
  edge [
    source 85
    target 818
  ]
  edge [
    source 85
    target 819
  ]
  edge [
    source 85
    target 820
  ]
  edge [
    source 85
    target 821
  ]
  edge [
    source 85
    target 822
  ]
  edge [
    source 85
    target 823
  ]
  edge [
    source 85
    target 824
  ]
  edge [
    source 85
    target 825
  ]
  edge [
    source 85
    target 826
  ]
  edge [
    source 85
    target 827
  ]
  edge [
    source 85
    target 828
  ]
  edge [
    source 85
    target 829
  ]
  edge [
    source 85
    target 830
  ]
  edge [
    source 85
    target 831
  ]
  edge [
    source 85
    target 832
  ]
  edge [
    source 85
    target 833
  ]
  edge [
    source 85
    target 834
  ]
  edge [
    source 85
    target 835
  ]
  edge [
    source 85
    target 836
  ]
  edge [
    source 85
    target 837
  ]
  edge [
    source 85
    target 838
  ]
  edge [
    source 85
    target 839
  ]
  edge [
    source 85
    target 840
  ]
  edge [
    source 85
    target 841
  ]
  edge [
    source 85
    target 842
  ]
  edge [
    source 85
    target 843
  ]
  edge [
    source 85
    target 844
  ]
  edge [
    source 85
    target 845
  ]
  edge [
    source 85
    target 846
  ]
  edge [
    source 85
    target 847
  ]
  edge [
    source 85
    target 848
  ]
  edge [
    source 85
    target 849
  ]
  edge [
    source 85
    target 850
  ]
  edge [
    source 85
    target 851
  ]
  edge [
    source 85
    target 852
  ]
  edge [
    source 85
    target 853
  ]
  edge [
    source 85
    target 854
  ]
  edge [
    source 85
    target 372
  ]
  edge [
    source 85
    target 855
  ]
  edge [
    source 85
    target 856
  ]
  edge [
    source 85
    target 857
  ]
  edge [
    source 85
    target 858
  ]
  edge [
    source 85
    target 859
  ]
  edge [
    source 85
    target 860
  ]
  edge [
    source 85
    target 861
  ]
  edge [
    source 85
    target 862
  ]
  edge [
    source 85
    target 863
  ]
  edge [
    source 85
    target 864
  ]
  edge [
    source 85
    target 865
  ]
  edge [
    source 85
    target 866
  ]
  edge [
    source 85
    target 867
  ]
  edge [
    source 85
    target 868
  ]
  edge [
    source 85
    target 869
  ]
  edge [
    source 85
    target 870
  ]
  edge [
    source 85
    target 871
  ]
  edge [
    source 85
    target 872
  ]
  edge [
    source 85
    target 873
  ]
  edge [
    source 85
    target 874
  ]
  edge [
    source 85
    target 875
  ]
  edge [
    source 85
    target 876
  ]
  edge [
    source 85
    target 877
  ]
  edge [
    source 85
    target 878
  ]
  edge [
    source 85
    target 879
  ]
  edge [
    source 85
    target 880
  ]
  edge [
    source 85
    target 881
  ]
  edge [
    source 85
    target 882
  ]
  edge [
    source 85
    target 883
  ]
  edge [
    source 85
    target 884
  ]
  edge [
    source 85
    target 885
  ]
  edge [
    source 85
    target 886
  ]
  edge [
    source 85
    target 887
  ]
  edge [
    source 85
    target 364
  ]
  edge [
    source 85
    target 888
  ]
  edge [
    source 85
    target 889
  ]
  edge [
    source 85
    target 890
  ]
  edge [
    source 85
    target 891
  ]
  edge [
    source 85
    target 892
  ]
  edge [
    source 85
    target 893
  ]
  edge [
    source 85
    target 894
  ]
  edge [
    source 85
    target 895
  ]
  edge [
    source 85
    target 896
  ]
  edge [
    source 85
    target 897
  ]
  edge [
    source 85
    target 898
  ]
  edge [
    source 85
    target 899
  ]
  edge [
    source 85
    target 900
  ]
  edge [
    source 85
    target 901
  ]
  edge [
    source 85
    target 902
  ]
  edge [
    source 85
    target 903
  ]
  edge [
    source 85
    target 904
  ]
  edge [
    source 85
    target 905
  ]
  edge [
    source 85
    target 906
  ]
  edge [
    source 85
    target 907
  ]
  edge [
    source 85
    target 908
  ]
  edge [
    source 85
    target 909
  ]
  edge [
    source 85
    target 910
  ]
  edge [
    source 85
    target 911
  ]
  edge [
    source 85
    target 486
  ]
  edge [
    source 85
    target 912
  ]
  edge [
    source 85
    target 913
  ]
  edge [
    source 85
    target 914
  ]
  edge [
    source 85
    target 371
  ]
  edge [
    source 85
    target 915
  ]
  edge [
    source 85
    target 916
  ]
  edge [
    source 85
    target 917
  ]
  edge [
    source 85
    target 918
  ]
  edge [
    source 85
    target 919
  ]
  edge [
    source 85
    target 920
  ]
  edge [
    source 85
    target 921
  ]
  edge [
    source 85
    target 922
  ]
  edge [
    source 85
    target 923
  ]
  edge [
    source 85
    target 376
  ]
  edge [
    source 85
    target 924
  ]
  edge [
    source 85
    target 925
  ]
  edge [
    source 85
    target 926
  ]
  edge [
    source 85
    target 927
  ]
  edge [
    source 85
    target 928
  ]
  edge [
    source 85
    target 929
  ]
  edge [
    source 85
    target 930
  ]
  edge [
    source 85
    target 931
  ]
  edge [
    source 85
    target 932
  ]
  edge [
    source 85
    target 933
  ]
  edge [
    source 85
    target 934
  ]
  edge [
    source 85
    target 935
  ]
  edge [
    source 85
    target 936
  ]
  edge [
    source 85
    target 937
  ]
  edge [
    source 85
    target 938
  ]
  edge [
    source 85
    target 367
  ]
  edge [
    source 85
    target 939
  ]
  edge [
    source 85
    target 940
  ]
  edge [
    source 85
    target 941
  ]
  edge [
    source 85
    target 942
  ]
  edge [
    source 85
    target 943
  ]
  edge [
    source 85
    target 944
  ]
  edge [
    source 85
    target 945
  ]
  edge [
    source 85
    target 946
  ]
  edge [
    source 85
    target 947
  ]
  edge [
    source 85
    target 948
  ]
  edge [
    source 85
    target 949
  ]
  edge [
    source 85
    target 366
  ]
  edge [
    source 85
    target 950
  ]
  edge [
    source 85
    target 951
  ]
  edge [
    source 85
    target 952
  ]
  edge [
    source 85
    target 953
  ]
  edge [
    source 85
    target 954
  ]
  edge [
    source 85
    target 955
  ]
  edge [
    source 85
    target 956
  ]
  edge [
    source 85
    target 957
  ]
  edge [
    source 85
    target 958
  ]
  edge [
    source 85
    target 959
  ]
  edge [
    source 85
    target 960
  ]
  edge [
    source 85
    target 961
  ]
  edge [
    source 85
    target 962
  ]
  edge [
    source 85
    target 963
  ]
  edge [
    source 85
    target 964
  ]
  edge [
    source 85
    target 965
  ]
  edge [
    source 85
    target 966
  ]
  edge [
    source 85
    target 967
  ]
  edge [
    source 85
    target 968
  ]
  edge [
    source 85
    target 969
  ]
  edge [
    source 85
    target 970
  ]
  edge [
    source 86
    target 87
  ]
  edge [
    source 86
    target 155
  ]
  edge [
    source 86
    target 971
  ]
  edge [
    source 86
    target 926
  ]
  edge [
    source 86
    target 972
  ]
  edge [
    source 86
    target 973
  ]
  edge [
    source 86
    target 974
  ]
  edge [
    source 86
    target 975
  ]
  edge [
    source 86
    target 976
  ]
  edge [
    source 87
    target 88
  ]
  edge [
    source 87
    target 977
  ]
  edge [
    source 87
    target 978
  ]
  edge [
    source 87
    target 979
  ]
  edge [
    source 87
    target 980
  ]
  edge [
    source 87
    target 981
  ]
  edge [
    source 87
    target 180
  ]
  edge [
    source 88
    target 492
  ]
  edge [
    source 88
    target 644
  ]
  edge [
    source 88
    target 346
  ]
  edge [
    source 88
    target 982
  ]
  edge [
    source 88
    target 983
  ]
  edge [
    source 88
    target 493
  ]
  edge [
    source 88
    target 984
  ]
  edge [
    source 88
    target 985
  ]
  edge [
    source 88
    target 986
  ]
  edge [
    source 88
    target 987
  ]
  edge [
    source 88
    target 988
  ]
  edge [
    source 88
    target 989
  ]
  edge [
    source 89
    target 90
  ]
  edge [
    source 89
    target 990
  ]
  edge [
    source 90
    target 91
  ]
  edge [
    source 91
    target 991
  ]
  edge [
    source 91
    target 201
  ]
  edge [
    source 91
    target 992
  ]
  edge [
    source 91
    target 993
  ]
  edge [
    source 91
    target 994
  ]
  edge [
    source 91
    target 995
  ]
  edge [
    source 91
    target 996
  ]
  edge [
    source 91
    target 997
  ]
  edge [
    source 91
    target 395
  ]
  edge [
    source 91
    target 418
  ]
  edge [
    source 92
    target 998
  ]
  edge [
    source 92
    target 999
  ]
  edge [
    source 92
    target 1000
  ]
  edge [
    source 92
    target 1001
  ]
  edge [
    source 92
    target 1002
  ]
  edge [
    source 93
    target 1003
  ]
  edge [
    source 94
    target 95
  ]
  edge [
    source 95
    target 96
  ]
  edge [
    source 95
    target 1004
  ]
  edge [
    source 95
    target 1005
  ]
  edge [
    source 95
    target 1006
  ]
  edge [
    source 95
    target 1007
  ]
  edge [
    source 95
    target 1008
  ]
  edge [
    source 95
    target 1009
  ]
  edge [
    source 96
    target 1010
  ]
  edge [
    source 96
    target 1011
  ]
  edge [
    source 96
    target 1012
  ]
  edge [
    source 96
    target 1013
  ]
  edge [
    source 96
    target 1014
  ]
  edge [
    source 96
    target 1015
  ]
  edge [
    source 96
    target 1016
  ]
  edge [
    source 96
    target 1017
  ]
  edge [
    source 96
    target 1018
  ]
  edge [
    source 96
    target 1019
  ]
  edge [
    source 96
    target 1020
  ]
  edge [
    source 96
    target 1021
  ]
  edge [
    source 1022
    target 1023
  ]
  edge [
    source 1024
    target 1025
  ]
]
