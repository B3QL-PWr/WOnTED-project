graph [
  maxDegree 27
  minDegree 1
  meanDegree 2.029850746268657
  density 0.03075531433740389
  graphCliqueNumber 2
  node [
    id 0
    label "dziennik"
    origin "text"
  ]
  node [
    id 1
    label "urz&#281;dowy"
    origin "text"
  ]
  node [
    id 2
    label "minister"
    origin "text"
  ]
  node [
    id 3
    label "&#347;rodowisko"
    origin "text"
  ]
  node [
    id 4
    label "g&#322;&#243;wny"
    origin "text"
  ]
  node [
    id 5
    label "inspektor"
    origin "text"
  ]
  node [
    id 6
    label "ochrona"
    origin "text"
  ]
  node [
    id 7
    label "grudzie&#324;"
    origin "text"
  ]
  node [
    id 8
    label "poz"
    origin "text"
  ]
  node [
    id 9
    label "spis"
  ]
  node [
    id 10
    label "sheet"
  ]
  node [
    id 11
    label "gazeta"
  ]
  node [
    id 12
    label "diariusz"
  ]
  node [
    id 13
    label "pami&#281;tnik"
  ]
  node [
    id 14
    label "journal"
  ]
  node [
    id 15
    label "ksi&#281;ga"
  ]
  node [
    id 16
    label "program_informacyjny"
  ]
  node [
    id 17
    label "urz&#281;dowo"
  ]
  node [
    id 18
    label "oficjalny"
  ]
  node [
    id 19
    label "formalny"
  ]
  node [
    id 20
    label "Goebbels"
  ]
  node [
    id 21
    label "Sto&#322;ypin"
  ]
  node [
    id 22
    label "rz&#261;d"
  ]
  node [
    id 23
    label "dostojnik"
  ]
  node [
    id 24
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 25
    label "obiekt_naturalny"
  ]
  node [
    id 26
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 27
    label "grupa"
  ]
  node [
    id 28
    label "stw&#243;r"
  ]
  node [
    id 29
    label "rzecz"
  ]
  node [
    id 30
    label "environment"
  ]
  node [
    id 31
    label "biota"
  ]
  node [
    id 32
    label "wszechstworzenie"
  ]
  node [
    id 33
    label "otoczenie"
  ]
  node [
    id 34
    label "fauna"
  ]
  node [
    id 35
    label "ekosystem"
  ]
  node [
    id 36
    label "teren"
  ]
  node [
    id 37
    label "mikrokosmos"
  ]
  node [
    id 38
    label "class"
  ]
  node [
    id 39
    label "zesp&#243;&#322;"
  ]
  node [
    id 40
    label "warunki"
  ]
  node [
    id 41
    label "huczek"
  ]
  node [
    id 42
    label "Ziemia"
  ]
  node [
    id 43
    label "woda"
  ]
  node [
    id 44
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 45
    label "najwa&#380;niejszy"
  ]
  node [
    id 46
    label "g&#322;&#243;wnie"
  ]
  node [
    id 47
    label "kontroler"
  ]
  node [
    id 48
    label "urz&#281;dnik"
  ]
  node [
    id 49
    label "oficer_policji"
  ]
  node [
    id 50
    label "chemical_bond"
  ]
  node [
    id 51
    label "tarcza"
  ]
  node [
    id 52
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 53
    label "obiekt"
  ]
  node [
    id 54
    label "Stra&#380;_Ochrony_Kolei"
  ]
  node [
    id 55
    label "borowiec"
  ]
  node [
    id 56
    label "obstawienie"
  ]
  node [
    id 57
    label "formacja"
  ]
  node [
    id 58
    label "ubezpieczenie"
  ]
  node [
    id 59
    label "obstawia&#263;"
  ]
  node [
    id 60
    label "obstawianie"
  ]
  node [
    id 61
    label "transportacja"
  ]
  node [
    id 62
    label "Bo&#380;e_Narodzenie"
  ]
  node [
    id 63
    label "miesi&#261;c"
  ]
  node [
    id 64
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 65
    label "Barb&#243;rka"
  ]
  node [
    id 66
    label "Sylwester"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
]
