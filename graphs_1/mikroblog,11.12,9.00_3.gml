graph [
  maxDegree 7
  minDegree 1
  meanDegree 1.875
  density 0.125
  graphCliqueNumber 2
  node [
    id 0
    label "szanowa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "plusujesz"
    origin "text"
  ]
  node [
    id 2
    label "nostalgia"
    origin "text"
  ]
  node [
    id 3
    label "staregry"
    origin "text"
  ]
  node [
    id 4
    label "gta"
    origin "text"
  ]
  node [
    id 5
    label "gimbynieznajo"
    origin "text"
  ]
  node [
    id 6
    label "czu&#263;"
  ]
  node [
    id 7
    label "chowa&#263;"
  ]
  node [
    id 8
    label "treasure"
  ]
  node [
    id 9
    label "respektowa&#263;"
  ]
  node [
    id 10
    label "powa&#380;anie"
  ]
  node [
    id 11
    label "wyra&#380;a&#263;"
  ]
  node [
    id 12
    label "homesickness"
  ]
  node [
    id 13
    label "t&#281;sknota"
  ]
  node [
    id 14
    label "smutek"
  ]
  node [
    id 15
    label "wspominki"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
]
