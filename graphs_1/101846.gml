graph [
  maxDegree 35
  minDegree 1
  meanDegree 2.230326295585413
  density 0.004289089029971947
  graphCliqueNumber 3
  node [
    id 0
    label "internet"
    origin "text"
  ]
  node [
    id 1
    label "dok&#322;adnie"
    origin "text"
  ]
  node [
    id 2
    label "taki"
    origin "text"
  ]
  node [
    id 3
    label "serwis"
    origin "text"
  ]
  node [
    id 4
    label "jak"
    origin "text"
  ]
  node [
    id 5
    label "myspace"
    origin "text"
  ]
  node [
    id 6
    label "by&#263;"
    origin "text"
  ]
  node [
    id 7
    label "odpowiedzialny"
    origin "text"
  ]
  node [
    id 8
    label "wychowanie"
    origin "text"
  ]
  node [
    id 9
    label "dziecko"
    origin "text"
  ]
  node [
    id 10
    label "orzec"
    origin "text"
  ]
  node [
    id 11
    label "s&#261;d"
    origin "text"
  ]
  node [
    id 12
    label "austin"
    origin "text"
  ]
  node [
    id 13
    label "teksas"
    origin "text"
  ]
  node [
    id 14
    label "sprawa"
    origin "text"
  ]
  node [
    id 15
    label "dotyczy&#263;"
    origin "text"
  ]
  node [
    id 16
    label "trzynastoletni"
    origin "text"
  ]
  node [
    id 17
    label "dziewczynka"
    origin "text"
  ]
  node [
    id 18
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 19
    label "zapisywa&#263;"
    origin "text"
  ]
  node [
    id 20
    label "si&#281;"
    origin "text"
  ]
  node [
    id 21
    label "poda&#263;"
    origin "text"
  ]
  node [
    id 22
    label "nieprawdziwy"
    origin "text"
  ]
  node [
    id 23
    label "wiek"
    origin "text"
  ]
  node [
    id 24
    label "twierdzi&#263;"
    origin "text"
  ]
  node [
    id 25
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 26
    label "lata"
    origin "text"
  ]
  node [
    id 27
    label "efekt"
    origin "text"
  ]
  node [
    id 28
    label "korespondencja"
    origin "text"
  ]
  node [
    id 29
    label "potem"
    origin "text"
  ]
  node [
    id 30
    label "rozmowa"
    origin "text"
  ]
  node [
    id 31
    label "inny"
    origin "text"
  ]
  node [
    id 32
    label "uczestnik"
    origin "text"
  ]
  node [
    id 33
    label "letni"
    origin "text"
  ]
  node [
    id 34
    label "ch&#322;opak"
    origin "text"
  ]
  node [
    id 35
    label "doj&#347;&#263;"
    origin "text"
  ]
  node [
    id 36
    label "spotkanie"
    origin "text"
  ]
  node [
    id 37
    label "kino"
    origin "text"
  ]
  node [
    id 38
    label "kolacja"
    origin "text"
  ]
  node [
    id 39
    label "ponie&#347;&#263;"
    origin "text"
  ]
  node [
    id 40
    label "parking"
    origin "text"
  ]
  node [
    id 41
    label "hormon"
    origin "text"
  ]
  node [
    id 42
    label "s&#281;dzia"
    origin "text"
  ]
  node [
    id 43
    label "przytomnie"
    origin "text"
  ]
  node [
    id 44
    label "stwierdzi&#263;"
    origin "text"
  ]
  node [
    id 45
    label "je&#347;li"
    origin "text"
  ]
  node [
    id 46
    label "ktokolwiek"
    origin "text"
  ]
  node [
    id 47
    label "przygoda"
    origin "text"
  ]
  node [
    id 48
    label "nastolatka"
    origin "text"
  ]
  node [
    id 49
    label "sie&#263;"
    origin "text"
  ]
  node [
    id 50
    label "w&#322;a&#347;nie"
    origin "text"
  ]
  node [
    id 51
    label "rodzic"
    origin "text"
  ]
  node [
    id 52
    label "werdykt"
    origin "text"
  ]
  node [
    id 53
    label "ameryka&#324;ski"
    origin "text"
  ]
  node [
    id 54
    label "powinny"
    origin "text"
  ]
  node [
    id 55
    label "przemy&#347;le&#263;"
    origin "text"
  ]
  node [
    id 56
    label "tak&#380;e"
    origin "text"
  ]
  node [
    id 57
    label "nasi"
    origin "text"
  ]
  node [
    id 58
    label "rodzimy"
    origin "text"
  ]
  node [
    id 59
    label "krytyk"
    origin "text"
  ]
  node [
    id 60
    label "obci&#261;&#380;a&#263;"
    origin "text"
  ]
  node [
    id 61
    label "wina"
    origin "text"
  ]
  node [
    id 62
    label "wszystek"
    origin "text"
  ]
  node [
    id 63
    label "mo&#380;liwy"
    origin "text"
  ]
  node [
    id 64
    label "zbrodnia"
    origin "text"
  ]
  node [
    id 65
    label "wsp&#243;&#322;czesny"
    origin "text"
  ]
  node [
    id 66
    label "spo&#322;ecze&#324;stwo"
    origin "text"
  ]
  node [
    id 67
    label "demoralizacja"
    origin "text"
  ]
  node [
    id 68
    label "maluczki"
    origin "text"
  ]
  node [
    id 69
    label "c&#243;&#380;"
    origin "text"
  ]
  node [
    id 70
    label "warto"
    origin "text"
  ]
  node [
    id 71
    label "pami&#281;ta&#263;"
    origin "text"
  ]
  node [
    id 72
    label "maja"
    origin "text"
  ]
  node [
    id 73
    label "przynajmniej"
    origin "text"
  ]
  node [
    id 74
    label "wi&#281;kszo&#347;&#263;"
    origin "text"
  ]
  node [
    id 75
    label "us&#322;uga_internetowa"
  ]
  node [
    id 76
    label "biznes_elektroniczny"
  ]
  node [
    id 77
    label "punkt_dost&#281;pu"
  ]
  node [
    id 78
    label "hipertekst"
  ]
  node [
    id 79
    label "gra_sieciowa"
  ]
  node [
    id 80
    label "mem"
  ]
  node [
    id 81
    label "e-hazard"
  ]
  node [
    id 82
    label "sie&#263;_komputerowa"
  ]
  node [
    id 83
    label "media"
  ]
  node [
    id 84
    label "podcast"
  ]
  node [
    id 85
    label "netbook"
  ]
  node [
    id 86
    label "provider"
  ]
  node [
    id 87
    label "cyberprzestrze&#324;"
  ]
  node [
    id 88
    label "grooming"
  ]
  node [
    id 89
    label "strona"
  ]
  node [
    id 90
    label "punctiliously"
  ]
  node [
    id 91
    label "dok&#322;adny"
  ]
  node [
    id 92
    label "meticulously"
  ]
  node [
    id 93
    label "precyzyjnie"
  ]
  node [
    id 94
    label "rzetelnie"
  ]
  node [
    id 95
    label "okre&#347;lony"
  ]
  node [
    id 96
    label "jaki&#347;"
  ]
  node [
    id 97
    label "mecz"
  ]
  node [
    id 98
    label "service"
  ]
  node [
    id 99
    label "wytw&#243;r"
  ]
  node [
    id 100
    label "zak&#322;ad"
  ]
  node [
    id 101
    label "us&#322;uga"
  ]
  node [
    id 102
    label "uderzenie"
  ]
  node [
    id 103
    label "doniesienie"
  ]
  node [
    id 104
    label "zastawa"
  ]
  node [
    id 105
    label "YouTube"
  ]
  node [
    id 106
    label "punkt"
  ]
  node [
    id 107
    label "porcja"
  ]
  node [
    id 108
    label "byd&#322;o"
  ]
  node [
    id 109
    label "zobo"
  ]
  node [
    id 110
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 111
    label "yakalo"
  ]
  node [
    id 112
    label "dzo"
  ]
  node [
    id 113
    label "si&#281;ga&#263;"
  ]
  node [
    id 114
    label "trwa&#263;"
  ]
  node [
    id 115
    label "obecno&#347;&#263;"
  ]
  node [
    id 116
    label "stan"
  ]
  node [
    id 117
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 118
    label "stand"
  ]
  node [
    id 119
    label "mie&#263;_miejsce"
  ]
  node [
    id 120
    label "uczestniczy&#263;"
  ]
  node [
    id 121
    label "chodzi&#263;"
  ]
  node [
    id 122
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 123
    label "equal"
  ]
  node [
    id 124
    label "cz&#322;owiek"
  ]
  node [
    id 125
    label "powa&#380;ny"
  ]
  node [
    id 126
    label "odpowiedzialnie"
  ]
  node [
    id 127
    label "przewinienie"
  ]
  node [
    id 128
    label "sprawca"
  ]
  node [
    id 129
    label "odpowiadanie"
  ]
  node [
    id 130
    label "&#347;wiadomy"
  ]
  node [
    id 131
    label "tyflopedagogika"
  ]
  node [
    id 132
    label "courtesy"
  ]
  node [
    id 133
    label "surdotyflopedagogika"
  ]
  node [
    id 134
    label "education"
  ]
  node [
    id 135
    label "metodyka"
  ]
  node [
    id 136
    label "psychopedagogika"
  ]
  node [
    id 137
    label "wychowanie_si&#281;"
  ]
  node [
    id 138
    label "pedagogia"
  ]
  node [
    id 139
    label "zapewnienie"
  ]
  node [
    id 140
    label "surdopedagogika"
  ]
  node [
    id 141
    label "pedeutologia"
  ]
  node [
    id 142
    label "pedagogika_specjalna"
  ]
  node [
    id 143
    label "nauczenie"
  ]
  node [
    id 144
    label "antypedagogika"
  ]
  node [
    id 145
    label "wp&#322;yw"
  ]
  node [
    id 146
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 147
    label "andragogika"
  ]
  node [
    id 148
    label "dydaktyka"
  ]
  node [
    id 149
    label "defektologia"
  ]
  node [
    id 150
    label "oligofrenopedagogika"
  ]
  node [
    id 151
    label "potomstwo"
  ]
  node [
    id 152
    label "organizm"
  ]
  node [
    id 153
    label "sraluch"
  ]
  node [
    id 154
    label "utulanie"
  ]
  node [
    id 155
    label "pediatra"
  ]
  node [
    id 156
    label "dzieciarnia"
  ]
  node [
    id 157
    label "m&#322;odziak"
  ]
  node [
    id 158
    label "dzieciak"
  ]
  node [
    id 159
    label "utula&#263;"
  ]
  node [
    id 160
    label "potomek"
  ]
  node [
    id 161
    label "pedofil"
  ]
  node [
    id 162
    label "entliczek-pentliczek"
  ]
  node [
    id 163
    label "m&#322;odzik"
  ]
  node [
    id 164
    label "cz&#322;owieczek"
  ]
  node [
    id 165
    label "zwierz&#281;"
  ]
  node [
    id 166
    label "niepe&#322;noletni"
  ]
  node [
    id 167
    label "fledgling"
  ]
  node [
    id 168
    label "utuli&#263;"
  ]
  node [
    id 169
    label "utulenie"
  ]
  node [
    id 170
    label "connote"
  ]
  node [
    id 171
    label "decide"
  ]
  node [
    id 172
    label "zdecydowa&#263;"
  ]
  node [
    id 173
    label "procesowicz"
  ]
  node [
    id 174
    label "wypowied&#378;"
  ]
  node [
    id 175
    label "pods&#261;dny"
  ]
  node [
    id 176
    label "podejrzany"
  ]
  node [
    id 177
    label "broni&#263;"
  ]
  node [
    id 178
    label "bronienie"
  ]
  node [
    id 179
    label "system"
  ]
  node [
    id 180
    label "my&#347;l"
  ]
  node [
    id 181
    label "urz&#261;d"
  ]
  node [
    id 182
    label "konektyw"
  ]
  node [
    id 183
    label "court"
  ]
  node [
    id 184
    label "obrona"
  ]
  node [
    id 185
    label "s&#261;downictwo"
  ]
  node [
    id 186
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 187
    label "forum"
  ]
  node [
    id 188
    label "zesp&#243;&#322;"
  ]
  node [
    id 189
    label "post&#281;powanie"
  ]
  node [
    id 190
    label "skazany"
  ]
  node [
    id 191
    label "wydarzenie"
  ]
  node [
    id 192
    label "&#347;wiadek"
  ]
  node [
    id 193
    label "antylogizm"
  ]
  node [
    id 194
    label "oskar&#380;yciel"
  ]
  node [
    id 195
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 196
    label "biuro"
  ]
  node [
    id 197
    label "instytucja"
  ]
  node [
    id 198
    label "d&#380;ins"
  ]
  node [
    id 199
    label "drelich"
  ]
  node [
    id 200
    label "temat"
  ]
  node [
    id 201
    label "kognicja"
  ]
  node [
    id 202
    label "idea"
  ]
  node [
    id 203
    label "szczeg&#243;&#322;"
  ]
  node [
    id 204
    label "rzecz"
  ]
  node [
    id 205
    label "przes&#322;anka"
  ]
  node [
    id 206
    label "rozprawa"
  ]
  node [
    id 207
    label "object"
  ]
  node [
    id 208
    label "proposition"
  ]
  node [
    id 209
    label "bargain"
  ]
  node [
    id 210
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 211
    label "tycze&#263;"
  ]
  node [
    id 212
    label "kilkunastoletni"
  ]
  node [
    id 213
    label "potomkini"
  ]
  node [
    id 214
    label "prostytutka"
  ]
  node [
    id 215
    label "pisa&#263;"
  ]
  node [
    id 216
    label "zaleca&#263;"
  ]
  node [
    id 217
    label "devise"
  ]
  node [
    id 218
    label "spell"
  ]
  node [
    id 219
    label "save"
  ]
  node [
    id 220
    label "utrwala&#263;"
  ]
  node [
    id 221
    label "robi&#263;"
  ]
  node [
    id 222
    label "wype&#322;nia&#263;"
  ]
  node [
    id 223
    label "write"
  ]
  node [
    id 224
    label "powodowa&#263;"
  ]
  node [
    id 225
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 226
    label "przekazywa&#263;"
  ]
  node [
    id 227
    label "tenis"
  ]
  node [
    id 228
    label "da&#263;"
  ]
  node [
    id 229
    label "siatk&#243;wka"
  ]
  node [
    id 230
    label "introduce"
  ]
  node [
    id 231
    label "jedzenie"
  ]
  node [
    id 232
    label "zaserwowa&#263;"
  ]
  node [
    id 233
    label "give"
  ]
  node [
    id 234
    label "ustawi&#263;"
  ]
  node [
    id 235
    label "zagra&#263;"
  ]
  node [
    id 236
    label "supply"
  ]
  node [
    id 237
    label "nafaszerowa&#263;"
  ]
  node [
    id 238
    label "poinformowa&#263;"
  ]
  node [
    id 239
    label "udawany"
  ]
  node [
    id 240
    label "niezgodny"
  ]
  node [
    id 241
    label "nieprawdziwie"
  ]
  node [
    id 242
    label "nierzeczywi&#347;cie"
  ]
  node [
    id 243
    label "nieszczery"
  ]
  node [
    id 244
    label "niehistoryczny"
  ]
  node [
    id 245
    label "prawda"
  ]
  node [
    id 246
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 247
    label "czas"
  ]
  node [
    id 248
    label "period"
  ]
  node [
    id 249
    label "rok"
  ]
  node [
    id 250
    label "cecha"
  ]
  node [
    id 251
    label "long_time"
  ]
  node [
    id 252
    label "choroba_wieku"
  ]
  node [
    id 253
    label "jednostka_geologiczna"
  ]
  node [
    id 254
    label "chron"
  ]
  node [
    id 255
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 256
    label "zapewnia&#263;"
  ]
  node [
    id 257
    label "oznajmia&#263;"
  ]
  node [
    id 258
    label "komunikowa&#263;"
  ]
  node [
    id 259
    label "attest"
  ]
  node [
    id 260
    label "argue"
  ]
  node [
    id 261
    label "czyj&#347;"
  ]
  node [
    id 262
    label "m&#261;&#380;"
  ]
  node [
    id 263
    label "summer"
  ]
  node [
    id 264
    label "dzia&#322;anie"
  ]
  node [
    id 265
    label "typ"
  ]
  node [
    id 266
    label "impression"
  ]
  node [
    id 267
    label "robienie_wra&#380;enia"
  ]
  node [
    id 268
    label "wra&#380;enie"
  ]
  node [
    id 269
    label "przyczyna"
  ]
  node [
    id 270
    label "zrobienie_wra&#380;enia"
  ]
  node [
    id 271
    label "robi&#263;_wra&#380;enie"
  ]
  node [
    id 272
    label "&#347;rodek"
  ]
  node [
    id 273
    label "event"
  ]
  node [
    id 274
    label "zrobi&#263;_wra&#380;enie"
  ]
  node [
    id 275
    label "rezultat"
  ]
  node [
    id 276
    label "zbi&#243;r"
  ]
  node [
    id 277
    label "poczta_elektroniczna"
  ]
  node [
    id 278
    label "list"
  ]
  node [
    id 279
    label "mail"
  ]
  node [
    id 280
    label "position"
  ]
  node [
    id 281
    label "komunikacja"
  ]
  node [
    id 282
    label "discussion"
  ]
  node [
    id 283
    label "czynno&#347;&#263;"
  ]
  node [
    id 284
    label "odpowied&#378;"
  ]
  node [
    id 285
    label "rozhowor"
  ]
  node [
    id 286
    label "cisza"
  ]
  node [
    id 287
    label "kolejny"
  ]
  node [
    id 288
    label "inaczej"
  ]
  node [
    id 289
    label "r&#243;&#380;ny"
  ]
  node [
    id 290
    label "inszy"
  ]
  node [
    id 291
    label "osobno"
  ]
  node [
    id 292
    label "nijaki"
  ]
  node [
    id 293
    label "sezonowy"
  ]
  node [
    id 294
    label "letnio"
  ]
  node [
    id 295
    label "s&#322;oneczny"
  ]
  node [
    id 296
    label "weso&#322;y"
  ]
  node [
    id 297
    label "oboj&#281;tny"
  ]
  node [
    id 298
    label "latowy"
  ]
  node [
    id 299
    label "ciep&#322;y"
  ]
  node [
    id 300
    label "typowy"
  ]
  node [
    id 301
    label "pomocnik"
  ]
  node [
    id 302
    label "g&#243;wniarz"
  ]
  node [
    id 303
    label "&#347;l&#261;ski"
  ]
  node [
    id 304
    label "m&#322;odzieniec"
  ]
  node [
    id 305
    label "kajtek"
  ]
  node [
    id 306
    label "kawaler"
  ]
  node [
    id 307
    label "usynawianie"
  ]
  node [
    id 308
    label "okrzos"
  ]
  node [
    id 309
    label "usynowienie"
  ]
  node [
    id 310
    label "sympatia"
  ]
  node [
    id 311
    label "pederasta"
  ]
  node [
    id 312
    label "synek"
  ]
  node [
    id 313
    label "boyfriend"
  ]
  node [
    id 314
    label "get"
  ]
  node [
    id 315
    label "zaj&#347;&#263;"
  ]
  node [
    id 316
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 317
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 318
    label "dop&#322;ata"
  ]
  node [
    id 319
    label "supervene"
  ]
  node [
    id 320
    label "heed"
  ]
  node [
    id 321
    label "dodatek"
  ]
  node [
    id 322
    label "catch"
  ]
  node [
    id 323
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 324
    label "uzyska&#263;"
  ]
  node [
    id 325
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 326
    label "orgazm"
  ]
  node [
    id 327
    label "dozna&#263;"
  ]
  node [
    id 328
    label "sta&#263;_si&#281;"
  ]
  node [
    id 329
    label "bodziec"
  ]
  node [
    id 330
    label "drive"
  ]
  node [
    id 331
    label "informacja"
  ]
  node [
    id 332
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 333
    label "spowodowa&#263;"
  ]
  node [
    id 334
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 335
    label "dotrze&#263;"
  ]
  node [
    id 336
    label "postrzega&#263;"
  ]
  node [
    id 337
    label "become"
  ]
  node [
    id 338
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 339
    label "dop&#322;yn&#261;&#263;"
  ]
  node [
    id 340
    label "przesy&#322;ka"
  ]
  node [
    id 341
    label "dolecie&#263;"
  ]
  node [
    id 342
    label "przy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 343
    label "dokoptowa&#263;"
  ]
  node [
    id 344
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 345
    label "po&#380;egnanie"
  ]
  node [
    id 346
    label "spowodowanie"
  ]
  node [
    id 347
    label "znalezienie"
  ]
  node [
    id 348
    label "znajomy"
  ]
  node [
    id 349
    label "doznanie"
  ]
  node [
    id 350
    label "employment"
  ]
  node [
    id 351
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 352
    label "gather"
  ]
  node [
    id 353
    label "powitanie"
  ]
  node [
    id 354
    label "spotykanie"
  ]
  node [
    id 355
    label "gathering"
  ]
  node [
    id 356
    label "spotkanie_si&#281;"
  ]
  node [
    id 357
    label "zdarzenie_si&#281;"
  ]
  node [
    id 358
    label "match"
  ]
  node [
    id 359
    label "zawarcie"
  ]
  node [
    id 360
    label "animatronika"
  ]
  node [
    id 361
    label "cyrk"
  ]
  node [
    id 362
    label "seans"
  ]
  node [
    id 363
    label "dorobek"
  ]
  node [
    id 364
    label "ekran"
  ]
  node [
    id 365
    label "budynek"
  ]
  node [
    id 366
    label "kinoteatr"
  ]
  node [
    id 367
    label "picture"
  ]
  node [
    id 368
    label "bioskop"
  ]
  node [
    id 369
    label "sztuka"
  ]
  node [
    id 370
    label "muza"
  ]
  node [
    id 371
    label "przywilej"
  ]
  node [
    id 372
    label "odwieczerz"
  ]
  node [
    id 373
    label "posi&#322;ek"
  ]
  node [
    id 374
    label "filiacja"
  ]
  node [
    id 375
    label "wst&#261;pi&#263;"
  ]
  node [
    id 376
    label "riot"
  ]
  node [
    id 377
    label "uda&#263;_si&#281;"
  ]
  node [
    id 378
    label "porwa&#263;"
  ]
  node [
    id 379
    label "nak&#322;oni&#263;"
  ]
  node [
    id 380
    label "carry"
  ]
  node [
    id 381
    label "plac"
  ]
  node [
    id 382
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 383
    label "hormone"
  ]
  node [
    id 384
    label "sport"
  ]
  node [
    id 385
    label "prawnik"
  ]
  node [
    id 386
    label "os&#261;dziciel"
  ]
  node [
    id 387
    label "kartka"
  ]
  node [
    id 388
    label "opiniodawca"
  ]
  node [
    id 389
    label "pracownik"
  ]
  node [
    id 390
    label "orzeka&#263;"
  ]
  node [
    id 391
    label "orzekanie"
  ]
  node [
    id 392
    label "przytomny"
  ]
  node [
    id 393
    label "czujnie"
  ]
  node [
    id 394
    label "powiedzie&#263;"
  ]
  node [
    id 395
    label "testify"
  ]
  node [
    id 396
    label "uzna&#263;"
  ]
  node [
    id 397
    label "oznajmi&#263;"
  ]
  node [
    id 398
    label "declare"
  ]
  node [
    id 399
    label "kto&#347;"
  ]
  node [
    id 400
    label "awanturnik"
  ]
  node [
    id 401
    label "fascynacja"
  ]
  node [
    id 402
    label "romans"
  ]
  node [
    id 403
    label "awantura"
  ]
  node [
    id 404
    label "affair"
  ]
  node [
    id 405
    label "dziewczyna"
  ]
  node [
    id 406
    label "ma&#322;olata"
  ]
  node [
    id 407
    label "gauze"
  ]
  node [
    id 408
    label "nitka"
  ]
  node [
    id 409
    label "mesh"
  ]
  node [
    id 410
    label "snu&#263;"
  ]
  node [
    id 411
    label "organization"
  ]
  node [
    id 412
    label "zasadzka"
  ]
  node [
    id 413
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 414
    label "web"
  ]
  node [
    id 415
    label "struktura"
  ]
  node [
    id 416
    label "organizacja"
  ]
  node [
    id 417
    label "vane"
  ]
  node [
    id 418
    label "kszta&#322;t"
  ]
  node [
    id 419
    label "obiekt"
  ]
  node [
    id 420
    label "wysnu&#263;"
  ]
  node [
    id 421
    label "instalacja"
  ]
  node [
    id 422
    label "net"
  ]
  node [
    id 423
    label "plecionka"
  ]
  node [
    id 424
    label "rozmieszczenie"
  ]
  node [
    id 425
    label "opiekun"
  ]
  node [
    id 426
    label "wapniak"
  ]
  node [
    id 427
    label "rodzic_chrzestny"
  ]
  node [
    id 428
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 429
    label "rodzice"
  ]
  node [
    id 430
    label "opinion"
  ]
  node [
    id 431
    label "decyzja"
  ]
  node [
    id 432
    label "nowoczesny"
  ]
  node [
    id 433
    label "p&#243;&#322;nocnoameryka&#324;ski"
  ]
  node [
    id 434
    label "po_ameryka&#324;sku"
  ]
  node [
    id 435
    label "boston"
  ]
  node [
    id 436
    label "cake-walk"
  ]
  node [
    id 437
    label "charakterystyczny"
  ]
  node [
    id 438
    label "placek_ameryka&#324;ski"
  ]
  node [
    id 439
    label "fajny"
  ]
  node [
    id 440
    label "j&#281;zyk_angielski"
  ]
  node [
    id 441
    label "Princeton"
  ]
  node [
    id 442
    label "pepperoni"
  ]
  node [
    id 443
    label "sa&#322;atka_Snickers"
  ]
  node [
    id 444
    label "zachodni"
  ]
  node [
    id 445
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 446
    label "anglosaski"
  ]
  node [
    id 447
    label "nale&#380;ny"
  ]
  node [
    id 448
    label "pomy&#347;le&#263;"
  ]
  node [
    id 449
    label "reconsideration"
  ]
  node [
    id 450
    label "w&#322;asny"
  ]
  node [
    id 451
    label "tutejszy"
  ]
  node [
    id 452
    label "przeciwnik"
  ]
  node [
    id 453
    label "publicysta"
  ]
  node [
    id 454
    label "krytyka"
  ]
  node [
    id 455
    label "zobowi&#261;zywa&#263;"
  ]
  node [
    id 456
    label "charge"
  ]
  node [
    id 457
    label "szkodzi&#263;"
  ]
  node [
    id 458
    label "oskar&#380;a&#263;"
  ]
  node [
    id 459
    label "nak&#322;ada&#263;"
  ]
  node [
    id 460
    label "wstyd"
  ]
  node [
    id 461
    label "konsekwencja"
  ]
  node [
    id 462
    label "guilt"
  ]
  node [
    id 463
    label "lutnia"
  ]
  node [
    id 464
    label "ca&#322;y"
  ]
  node [
    id 465
    label "zno&#347;ny"
  ]
  node [
    id 466
    label "mo&#380;liwie"
  ]
  node [
    id 467
    label "urealnianie"
  ]
  node [
    id 468
    label "umo&#380;liwienie"
  ]
  node [
    id 469
    label "mo&#380;ebny"
  ]
  node [
    id 470
    label "umo&#380;liwianie"
  ]
  node [
    id 471
    label "dost&#281;pny"
  ]
  node [
    id 472
    label "urealnienie"
  ]
  node [
    id 473
    label "crime"
  ]
  node [
    id 474
    label "przest&#281;pstwo"
  ]
  node [
    id 475
    label "post&#281;pek"
  ]
  node [
    id 476
    label "uwsp&#243;&#322;cze&#347;nienie"
  ]
  node [
    id 477
    label "tera&#378;niejszy"
  ]
  node [
    id 478
    label "uwsp&#243;&#322;cze&#347;nianie"
  ]
  node [
    id 479
    label "sp&#243;&#322;czesny"
  ]
  node [
    id 480
    label "unowocze&#347;nianie"
  ]
  node [
    id 481
    label "jednoczesny"
  ]
  node [
    id 482
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 483
    label "pole"
  ]
  node [
    id 484
    label "kastowo&#347;&#263;"
  ]
  node [
    id 485
    label "ludzie_pracy"
  ]
  node [
    id 486
    label "community"
  ]
  node [
    id 487
    label "status"
  ]
  node [
    id 488
    label "cywilizacja"
  ]
  node [
    id 489
    label "pozaklasowy"
  ]
  node [
    id 490
    label "aspo&#322;eczny"
  ]
  node [
    id 491
    label "uwarstwienie"
  ]
  node [
    id 492
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 493
    label "elita"
  ]
  node [
    id 494
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 495
    label "wspo&#322;ecze&#324;stwo"
  ]
  node [
    id 496
    label "klasa"
  ]
  node [
    id 497
    label "zdemoralizowanie"
  ]
  node [
    id 498
    label "demoralization"
  ]
  node [
    id 499
    label "patologia"
  ]
  node [
    id 500
    label "malutki"
  ]
  node [
    id 501
    label "maluczko"
  ]
  node [
    id 502
    label "prosty"
  ]
  node [
    id 503
    label "nikczemny"
  ]
  node [
    id 504
    label "przysparza&#263;"
  ]
  node [
    id 505
    label "kali&#263;_si&#281;"
  ]
  node [
    id 506
    label "bonanza"
  ]
  node [
    id 507
    label "zna&#263;"
  ]
  node [
    id 508
    label "troska&#263;_si&#281;"
  ]
  node [
    id 509
    label "zachowywa&#263;"
  ]
  node [
    id 510
    label "chowa&#263;"
  ]
  node [
    id 511
    label "think"
  ]
  node [
    id 512
    label "pilnowa&#263;"
  ]
  node [
    id 513
    label "recall"
  ]
  node [
    id 514
    label "echo"
  ]
  node [
    id 515
    label "take_care"
  ]
  node [
    id 516
    label "wedyzm"
  ]
  node [
    id 517
    label "energia"
  ]
  node [
    id 518
    label "buddyzm"
  ]
  node [
    id 519
    label "majority"
  ]
  node [
    id 520
    label "cz&#281;&#347;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 124
  ]
  edge [
    source 7
    target 125
  ]
  edge [
    source 7
    target 126
  ]
  edge [
    source 7
    target 127
  ]
  edge [
    source 7
    target 128
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 7
    target 130
  ]
  edge [
    source 7
    target 38
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 131
  ]
  edge [
    source 8
    target 132
  ]
  edge [
    source 8
    target 133
  ]
  edge [
    source 8
    target 134
  ]
  edge [
    source 8
    target 135
  ]
  edge [
    source 8
    target 136
  ]
  edge [
    source 8
    target 137
  ]
  edge [
    source 8
    target 138
  ]
  edge [
    source 8
    target 139
  ]
  edge [
    source 8
    target 140
  ]
  edge [
    source 8
    target 141
  ]
  edge [
    source 8
    target 142
  ]
  edge [
    source 8
    target 143
  ]
  edge [
    source 8
    target 144
  ]
  edge [
    source 8
    target 145
  ]
  edge [
    source 8
    target 146
  ]
  edge [
    source 8
    target 147
  ]
  edge [
    source 8
    target 148
  ]
  edge [
    source 8
    target 149
  ]
  edge [
    source 8
    target 150
  ]
  edge [
    source 8
    target 28
  ]
  edge [
    source 8
    target 40
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 151
  ]
  edge [
    source 9
    target 152
  ]
  edge [
    source 9
    target 153
  ]
  edge [
    source 9
    target 154
  ]
  edge [
    source 9
    target 155
  ]
  edge [
    source 9
    target 156
  ]
  edge [
    source 9
    target 157
  ]
  edge [
    source 9
    target 158
  ]
  edge [
    source 9
    target 159
  ]
  edge [
    source 9
    target 160
  ]
  edge [
    source 9
    target 161
  ]
  edge [
    source 9
    target 162
  ]
  edge [
    source 9
    target 163
  ]
  edge [
    source 9
    target 164
  ]
  edge [
    source 9
    target 165
  ]
  edge [
    source 9
    target 166
  ]
  edge [
    source 9
    target 167
  ]
  edge [
    source 9
    target 168
  ]
  edge [
    source 9
    target 169
  ]
  edge [
    source 9
    target 17
  ]
  edge [
    source 9
    target 34
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 170
  ]
  edge [
    source 10
    target 171
  ]
  edge [
    source 10
    target 172
  ]
  edge [
    source 10
    target 44
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 53
  ]
  edge [
    source 11
    target 54
  ]
  edge [
    source 11
    target 173
  ]
  edge [
    source 11
    target 174
  ]
  edge [
    source 11
    target 175
  ]
  edge [
    source 11
    target 176
  ]
  edge [
    source 11
    target 177
  ]
  edge [
    source 11
    target 178
  ]
  edge [
    source 11
    target 179
  ]
  edge [
    source 11
    target 180
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 11
    target 181
  ]
  edge [
    source 11
    target 182
  ]
  edge [
    source 11
    target 183
  ]
  edge [
    source 11
    target 184
  ]
  edge [
    source 11
    target 185
  ]
  edge [
    source 11
    target 186
  ]
  edge [
    source 11
    target 187
  ]
  edge [
    source 11
    target 188
  ]
  edge [
    source 11
    target 189
  ]
  edge [
    source 11
    target 190
  ]
  edge [
    source 11
    target 191
  ]
  edge [
    source 11
    target 192
  ]
  edge [
    source 11
    target 193
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 11
    target 194
  ]
  edge [
    source 11
    target 195
  ]
  edge [
    source 11
    target 196
  ]
  edge [
    source 11
    target 197
  ]
  edge [
    source 11
    target 42
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 198
  ]
  edge [
    source 13
    target 199
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 200
  ]
  edge [
    source 14
    target 201
  ]
  edge [
    source 14
    target 202
  ]
  edge [
    source 14
    target 203
  ]
  edge [
    source 14
    target 204
  ]
  edge [
    source 14
    target 191
  ]
  edge [
    source 14
    target 205
  ]
  edge [
    source 14
    target 206
  ]
  edge [
    source 14
    target 207
  ]
  edge [
    source 14
    target 208
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 209
  ]
  edge [
    source 15
    target 210
  ]
  edge [
    source 15
    target 211
  ]
  edge [
    source 15
    target 67
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 212
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 213
  ]
  edge [
    source 17
    target 124
  ]
  edge [
    source 17
    target 214
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 215
  ]
  edge [
    source 19
    target 216
  ]
  edge [
    source 19
    target 217
  ]
  edge [
    source 19
    target 218
  ]
  edge [
    source 19
    target 219
  ]
  edge [
    source 19
    target 220
  ]
  edge [
    source 19
    target 221
  ]
  edge [
    source 19
    target 222
  ]
  edge [
    source 19
    target 223
  ]
  edge [
    source 19
    target 224
  ]
  edge [
    source 19
    target 225
  ]
  edge [
    source 19
    target 226
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 227
  ]
  edge [
    source 21
    target 228
  ]
  edge [
    source 21
    target 229
  ]
  edge [
    source 21
    target 230
  ]
  edge [
    source 21
    target 231
  ]
  edge [
    source 21
    target 232
  ]
  edge [
    source 21
    target 233
  ]
  edge [
    source 21
    target 234
  ]
  edge [
    source 21
    target 235
  ]
  edge [
    source 21
    target 236
  ]
  edge [
    source 21
    target 237
  ]
  edge [
    source 21
    target 238
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 239
  ]
  edge [
    source 22
    target 240
  ]
  edge [
    source 22
    target 241
  ]
  edge [
    source 22
    target 242
  ]
  edge [
    source 22
    target 243
  ]
  edge [
    source 22
    target 244
  ]
  edge [
    source 22
    target 245
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 246
  ]
  edge [
    source 23
    target 247
  ]
  edge [
    source 23
    target 248
  ]
  edge [
    source 23
    target 249
  ]
  edge [
    source 23
    target 250
  ]
  edge [
    source 23
    target 251
  ]
  edge [
    source 23
    target 252
  ]
  edge [
    source 23
    target 253
  ]
  edge [
    source 23
    target 254
  ]
  edge [
    source 23
    target 255
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 256
  ]
  edge [
    source 24
    target 257
  ]
  edge [
    source 24
    target 258
  ]
  edge [
    source 24
    target 259
  ]
  edge [
    source 24
    target 260
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 261
  ]
  edge [
    source 25
    target 262
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 263
  ]
  edge [
    source 26
    target 247
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 264
  ]
  edge [
    source 27
    target 265
  ]
  edge [
    source 27
    target 266
  ]
  edge [
    source 27
    target 267
  ]
  edge [
    source 27
    target 268
  ]
  edge [
    source 27
    target 269
  ]
  edge [
    source 27
    target 270
  ]
  edge [
    source 27
    target 271
  ]
  edge [
    source 27
    target 272
  ]
  edge [
    source 27
    target 273
  ]
  edge [
    source 27
    target 274
  ]
  edge [
    source 27
    target 275
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 276
  ]
  edge [
    source 28
    target 277
  ]
  edge [
    source 28
    target 278
  ]
  edge [
    source 28
    target 279
  ]
  edge [
    source 28
    target 280
  ]
  edge [
    source 28
    target 281
  ]
  edge [
    source 28
    target 40
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 38
  ]
  edge [
    source 29
    target 34
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 282
  ]
  edge [
    source 30
    target 283
  ]
  edge [
    source 30
    target 284
  ]
  edge [
    source 30
    target 285
  ]
  edge [
    source 30
    target 286
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 287
  ]
  edge [
    source 31
    target 288
  ]
  edge [
    source 31
    target 289
  ]
  edge [
    source 31
    target 290
  ]
  edge [
    source 31
    target 291
  ]
  edge [
    source 32
    target 124
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 292
  ]
  edge [
    source 33
    target 293
  ]
  edge [
    source 33
    target 294
  ]
  edge [
    source 33
    target 295
  ]
  edge [
    source 33
    target 296
  ]
  edge [
    source 33
    target 297
  ]
  edge [
    source 33
    target 298
  ]
  edge [
    source 33
    target 299
  ]
  edge [
    source 33
    target 300
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 39
  ]
  edge [
    source 34
    target 124
  ]
  edge [
    source 34
    target 301
  ]
  edge [
    source 34
    target 302
  ]
  edge [
    source 34
    target 303
  ]
  edge [
    source 34
    target 304
  ]
  edge [
    source 34
    target 305
  ]
  edge [
    source 34
    target 306
  ]
  edge [
    source 34
    target 307
  ]
  edge [
    source 34
    target 308
  ]
  edge [
    source 34
    target 309
  ]
  edge [
    source 34
    target 310
  ]
  edge [
    source 34
    target 311
  ]
  edge [
    source 34
    target 312
  ]
  edge [
    source 34
    target 313
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 314
  ]
  edge [
    source 35
    target 315
  ]
  edge [
    source 35
    target 316
  ]
  edge [
    source 35
    target 317
  ]
  edge [
    source 35
    target 318
  ]
  edge [
    source 35
    target 319
  ]
  edge [
    source 35
    target 320
  ]
  edge [
    source 35
    target 321
  ]
  edge [
    source 35
    target 322
  ]
  edge [
    source 35
    target 323
  ]
  edge [
    source 35
    target 324
  ]
  edge [
    source 35
    target 325
  ]
  edge [
    source 35
    target 326
  ]
  edge [
    source 35
    target 327
  ]
  edge [
    source 35
    target 328
  ]
  edge [
    source 35
    target 329
  ]
  edge [
    source 35
    target 330
  ]
  edge [
    source 35
    target 331
  ]
  edge [
    source 35
    target 332
  ]
  edge [
    source 35
    target 333
  ]
  edge [
    source 35
    target 334
  ]
  edge [
    source 35
    target 335
  ]
  edge [
    source 35
    target 336
  ]
  edge [
    source 35
    target 337
  ]
  edge [
    source 35
    target 338
  ]
  edge [
    source 35
    target 339
  ]
  edge [
    source 35
    target 340
  ]
  edge [
    source 35
    target 341
  ]
  edge [
    source 35
    target 342
  ]
  edge [
    source 35
    target 343
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 344
  ]
  edge [
    source 36
    target 345
  ]
  edge [
    source 36
    target 346
  ]
  edge [
    source 36
    target 347
  ]
  edge [
    source 36
    target 348
  ]
  edge [
    source 36
    target 349
  ]
  edge [
    source 36
    target 350
  ]
  edge [
    source 36
    target 351
  ]
  edge [
    source 36
    target 352
  ]
  edge [
    source 36
    target 353
  ]
  edge [
    source 36
    target 354
  ]
  edge [
    source 36
    target 191
  ]
  edge [
    source 36
    target 355
  ]
  edge [
    source 36
    target 356
  ]
  edge [
    source 36
    target 357
  ]
  edge [
    source 36
    target 358
  ]
  edge [
    source 36
    target 359
  ]
  edge [
    source 36
    target 38
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 360
  ]
  edge [
    source 37
    target 361
  ]
  edge [
    source 37
    target 362
  ]
  edge [
    source 37
    target 363
  ]
  edge [
    source 37
    target 364
  ]
  edge [
    source 37
    target 365
  ]
  edge [
    source 37
    target 366
  ]
  edge [
    source 37
    target 367
  ]
  edge [
    source 37
    target 368
  ]
  edge [
    source 37
    target 369
  ]
  edge [
    source 37
    target 370
  ]
  edge [
    source 38
    target 371
  ]
  edge [
    source 38
    target 372
  ]
  edge [
    source 38
    target 373
  ]
  edge [
    source 38
    target 374
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 375
  ]
  edge [
    source 39
    target 376
  ]
  edge [
    source 39
    target 377
  ]
  edge [
    source 39
    target 378
  ]
  edge [
    source 39
    target 379
  ]
  edge [
    source 39
    target 332
  ]
  edge [
    source 39
    target 327
  ]
  edge [
    source 39
    target 380
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 381
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 382
  ]
  edge [
    source 41
    target 383
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 124
  ]
  edge [
    source 42
    target 384
  ]
  edge [
    source 42
    target 385
  ]
  edge [
    source 42
    target 386
  ]
  edge [
    source 42
    target 387
  ]
  edge [
    source 42
    target 388
  ]
  edge [
    source 42
    target 389
  ]
  edge [
    source 42
    target 390
  ]
  edge [
    source 42
    target 391
  ]
  edge [
    source 42
    target 47
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 392
  ]
  edge [
    source 43
    target 393
  ]
  edge [
    source 43
    target 130
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 394
  ]
  edge [
    source 44
    target 395
  ]
  edge [
    source 44
    target 396
  ]
  edge [
    source 44
    target 397
  ]
  edge [
    source 44
    target 398
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 46
    target 399
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 191
  ]
  edge [
    source 47
    target 400
  ]
  edge [
    source 47
    target 401
  ]
  edge [
    source 47
    target 402
  ]
  edge [
    source 47
    target 403
  ]
  edge [
    source 47
    target 404
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 405
  ]
  edge [
    source 48
    target 406
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 78
  ]
  edge [
    source 49
    target 407
  ]
  edge [
    source 49
    target 408
  ]
  edge [
    source 49
    target 409
  ]
  edge [
    source 49
    target 81
  ]
  edge [
    source 49
    target 85
  ]
  edge [
    source 49
    target 87
  ]
  edge [
    source 49
    target 76
  ]
  edge [
    source 49
    target 410
  ]
  edge [
    source 49
    target 411
  ]
  edge [
    source 49
    target 412
  ]
  edge [
    source 49
    target 413
  ]
  edge [
    source 49
    target 414
  ]
  edge [
    source 49
    target 86
  ]
  edge [
    source 49
    target 415
  ]
  edge [
    source 49
    target 75
  ]
  edge [
    source 49
    target 77
  ]
  edge [
    source 49
    target 416
  ]
  edge [
    source 49
    target 80
  ]
  edge [
    source 49
    target 417
  ]
  edge [
    source 49
    target 84
  ]
  edge [
    source 49
    target 88
  ]
  edge [
    source 49
    target 418
  ]
  edge [
    source 49
    target 89
  ]
  edge [
    source 49
    target 419
  ]
  edge [
    source 49
    target 420
  ]
  edge [
    source 49
    target 79
  ]
  edge [
    source 49
    target 421
  ]
  edge [
    source 49
    target 82
  ]
  edge [
    source 49
    target 422
  ]
  edge [
    source 49
    target 423
  ]
  edge [
    source 49
    target 83
  ]
  edge [
    source 49
    target 424
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 74
  ]
  edge [
    source 51
    target 425
  ]
  edge [
    source 51
    target 426
  ]
  edge [
    source 51
    target 427
  ]
  edge [
    source 51
    target 428
  ]
  edge [
    source 51
    target 429
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 430
  ]
  edge [
    source 52
    target 431
  ]
  edge [
    source 53
    target 432
  ]
  edge [
    source 53
    target 433
  ]
  edge [
    source 53
    target 434
  ]
  edge [
    source 53
    target 435
  ]
  edge [
    source 53
    target 436
  ]
  edge [
    source 53
    target 437
  ]
  edge [
    source 53
    target 438
  ]
  edge [
    source 53
    target 439
  ]
  edge [
    source 53
    target 440
  ]
  edge [
    source 53
    target 441
  ]
  edge [
    source 53
    target 442
  ]
  edge [
    source 53
    target 443
  ]
  edge [
    source 53
    target 444
  ]
  edge [
    source 53
    target 445
  ]
  edge [
    source 53
    target 446
  ]
  edge [
    source 53
    target 300
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 447
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 448
  ]
  edge [
    source 55
    target 449
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 450
  ]
  edge [
    source 58
    target 451
  ]
  edge [
    source 59
    target 452
  ]
  edge [
    source 59
    target 453
  ]
  edge [
    source 59
    target 454
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 455
  ]
  edge [
    source 60
    target 456
  ]
  edge [
    source 60
    target 457
  ]
  edge [
    source 60
    target 458
  ]
  edge [
    source 60
    target 459
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 460
  ]
  edge [
    source 61
    target 461
  ]
  edge [
    source 61
    target 462
  ]
  edge [
    source 61
    target 463
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 464
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 465
  ]
  edge [
    source 63
    target 466
  ]
  edge [
    source 63
    target 467
  ]
  edge [
    source 63
    target 468
  ]
  edge [
    source 63
    target 469
  ]
  edge [
    source 63
    target 470
  ]
  edge [
    source 63
    target 471
  ]
  edge [
    source 63
    target 472
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 473
  ]
  edge [
    source 64
    target 474
  ]
  edge [
    source 64
    target 475
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 476
  ]
  edge [
    source 65
    target 477
  ]
  edge [
    source 65
    target 478
  ]
  edge [
    source 65
    target 479
  ]
  edge [
    source 65
    target 480
  ]
  edge [
    source 65
    target 481
  ]
  edge [
    source 65
    target 482
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 66
    target 483
  ]
  edge [
    source 66
    target 484
  ]
  edge [
    source 66
    target 485
  ]
  edge [
    source 66
    target 486
  ]
  edge [
    source 66
    target 487
  ]
  edge [
    source 66
    target 488
  ]
  edge [
    source 66
    target 489
  ]
  edge [
    source 66
    target 490
  ]
  edge [
    source 66
    target 491
  ]
  edge [
    source 66
    target 492
  ]
  edge [
    source 66
    target 493
  ]
  edge [
    source 66
    target 494
  ]
  edge [
    source 66
    target 495
  ]
  edge [
    source 66
    target 496
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 67
    target 497
  ]
  edge [
    source 67
    target 498
  ]
  edge [
    source 67
    target 499
  ]
  edge [
    source 67
    target 145
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 68
    target 500
  ]
  edge [
    source 68
    target 501
  ]
  edge [
    source 68
    target 502
  ]
  edge [
    source 68
    target 503
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 504
  ]
  edge [
    source 70
    target 233
  ]
  edge [
    source 70
    target 505
  ]
  edge [
    source 70
    target 506
  ]
  edge [
    source 71
    target 113
  ]
  edge [
    source 71
    target 507
  ]
  edge [
    source 71
    target 508
  ]
  edge [
    source 71
    target 509
  ]
  edge [
    source 71
    target 510
  ]
  edge [
    source 71
    target 511
  ]
  edge [
    source 71
    target 512
  ]
  edge [
    source 71
    target 221
  ]
  edge [
    source 71
    target 513
  ]
  edge [
    source 71
    target 514
  ]
  edge [
    source 71
    target 515
  ]
  edge [
    source 72
    target 73
  ]
  edge [
    source 72
    target 516
  ]
  edge [
    source 72
    target 517
  ]
  edge [
    source 72
    target 518
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 74
    target 519
  ]
  edge [
    source 74
    target 520
  ]
]
