graph [
  maxDegree 11
  minDegree 1
  meanDegree 1.8823529411764706
  density 0.11764705882352941
  graphCliqueNumber 2
  node [
    id 0
    label "wrzuca&#263;"
    origin "text"
  ]
  node [
    id 1
    label "najbardziej"
    origin "text"
  ]
  node [
    id 2
    label "rakowy"
    origin "text"
  ]
  node [
    id 3
    label "okre&#347;lenie"
    origin "text"
  ]
  node [
    id 4
    label "give"
  ]
  node [
    id 5
    label "umieszcza&#263;"
  ]
  node [
    id 6
    label "mi&#281;sny"
  ]
  node [
    id 7
    label "zdecydowanie"
  ]
  node [
    id 8
    label "follow-up"
  ]
  node [
    id 9
    label "appointment"
  ]
  node [
    id 10
    label "ustalenie"
  ]
  node [
    id 11
    label "localization"
  ]
  node [
    id 12
    label "denomination"
  ]
  node [
    id 13
    label "wyra&#380;enie"
  ]
  node [
    id 14
    label "ozdobnik"
  ]
  node [
    id 15
    label "przewidzenie"
  ]
  node [
    id 16
    label "term"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
]
