graph [
  maxDegree 39
  minDegree 1
  meanDegree 7.346153846153846
  density 0.14404223227752638
  graphCliqueNumber 10
  node [
    id 0
    label "contentnadzis"
    origin "text"
  ]
  node [
    id 1
    label "codziennaafrica"
    origin "text"
  ]
  node [
    id 2
    label "totoafrica"
    origin "text"
  ]
  node [
    id 3
    label "dziendobry"
    origin "text"
  ]
  node [
    id 4
    label "cytatnadzis"
    origin "text"
  ]
  node [
    id 5
    label "ciekawostkanadzis"
    origin "text"
  ]
  node [
    id 6
    label "pomyslnaobiad"
    origin "text"
  ]
  node [
    id 7
    label "pogodanadzis"
    origin "text"
  ]
  node [
    id 8
    label "wiatr"
  ]
  node [
    id 9
    label "Zach"
  ]
  node [
    id 10
    label "P&#322;n"
  ]
  node [
    id 11
    label "13"
  ]
  node [
    id 12
    label "23"
  ]
  node [
    id 13
    label "km"
  ]
  node [
    id 14
    label "17"
  ]
  node [
    id 15
    label "21"
  ]
  node [
    id 16
    label "P&#322;d"
  ]
  node [
    id 17
    label "18"
  ]
  node [
    id 18
    label "14"
  ]
  node [
    id 19
    label "h"
  ]
  node [
    id 20
    label "&#2039;"
  ]
  node [
    id 21
    label "Bydgoszcz"
  ]
  node [
    id 22
    label "28"
  ]
  node [
    id 23
    label "35"
  ]
  node [
    id 24
    label "do"
  ]
  node [
    id 25
    label "po&#322;udnie"
  ]
  node [
    id 26
    label "09"
  ]
  node [
    id 27
    label "12"
  ]
  node [
    id 28
    label "33"
  ]
  node [
    id 29
    label "25"
  ]
  node [
    id 30
    label "Gorz&#243;w"
  ]
  node [
    id 31
    label "Wlkp"
  ]
  node [
    id 32
    label "11"
  ]
  node [
    id 33
    label "9"
  ]
  node [
    id 34
    label "8"
  ]
  node [
    id 35
    label "4"
  ]
  node [
    id 36
    label "15"
  ]
  node [
    id 37
    label "7"
  ]
  node [
    id 38
    label "5"
  ]
  node [
    id 39
    label "10"
  ]
  node [
    id 40
    label "19"
  ]
  node [
    id 41
    label "6"
  ]
  node [
    id 42
    label "opole"
  ]
  node [
    id 43
    label "Rzesz&#243;w"
  ]
  node [
    id 44
    label "warszawa"
  ]
  node [
    id 45
    label "16"
  ]
  node [
    id 46
    label "Wroc&#322;aw"
  ]
  node [
    id 47
    label "zielony"
  ]
  node [
    id 48
    label "G&#243;ra"
  ]
  node [
    id 49
    label "Wsch"
  ]
  node [
    id 50
    label "rozr&#243;ba"
  ]
  node [
    id 51
    label "Rastakana"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 8
    target 11
  ]
  edge [
    source 8
    target 12
  ]
  edge [
    source 8
    target 13
  ]
  edge [
    source 8
    target 14
  ]
  edge [
    source 8
    target 15
  ]
  edge [
    source 8
    target 16
  ]
  edge [
    source 8
    target 17
  ]
  edge [
    source 8
    target 18
  ]
  edge [
    source 8
    target 19
  ]
  edge [
    source 8
    target 20
  ]
  edge [
    source 8
    target 21
  ]
  edge [
    source 8
    target 22
  ]
  edge [
    source 8
    target 23
  ]
  edge [
    source 8
    target 24
  ]
  edge [
    source 8
    target 25
  ]
  edge [
    source 8
    target 26
  ]
  edge [
    source 8
    target 27
  ]
  edge [
    source 8
    target 28
  ]
  edge [
    source 8
    target 29
  ]
  edge [
    source 8
    target 30
  ]
  edge [
    source 8
    target 31
  ]
  edge [
    source 8
    target 32
  ]
  edge [
    source 8
    target 33
  ]
  edge [
    source 8
    target 34
  ]
  edge [
    source 8
    target 35
  ]
  edge [
    source 8
    target 36
  ]
  edge [
    source 8
    target 37
  ]
  edge [
    source 8
    target 38
  ]
  edge [
    source 8
    target 39
  ]
  edge [
    source 8
    target 40
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 8
    target 46
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 9
    target 12
  ]
  edge [
    source 9
    target 13
  ]
  edge [
    source 9
    target 14
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 15
  ]
  edge [
    source 9
    target 17
  ]
  edge [
    source 9
    target 18
  ]
  edge [
    source 9
    target 19
  ]
  edge [
    source 9
    target 20
  ]
  edge [
    source 9
    target 21
  ]
  edge [
    source 9
    target 22
  ]
  edge [
    source 9
    target 23
  ]
  edge [
    source 9
    target 24
  ]
  edge [
    source 9
    target 25
  ]
  edge [
    source 9
    target 26
  ]
  edge [
    source 9
    target 27
  ]
  edge [
    source 9
    target 28
  ]
  edge [
    source 9
    target 29
  ]
  edge [
    source 9
    target 30
  ]
  edge [
    source 9
    target 31
  ]
  edge [
    source 9
    target 16
  ]
  edge [
    source 9
    target 33
  ]
  edge [
    source 9
    target 32
  ]
  edge [
    source 9
    target 34
  ]
  edge [
    source 9
    target 36
  ]
  edge [
    source 9
    target 37
  ]
  edge [
    source 9
    target 38
  ]
  edge [
    source 9
    target 39
  ]
  edge [
    source 9
    target 40
  ]
  edge [
    source 9
    target 42
  ]
  edge [
    source 9
    target 35
  ]
  edge [
    source 9
    target 45
  ]
  edge [
    source 9
    target 46
  ]
  edge [
    source 10
    target 15
  ]
  edge [
    source 10
    target 23
  ]
  edge [
    source 10
    target 13
  ]
  edge [
    source 10
    target 19
  ]
  edge [
    source 10
    target 24
  ]
  edge [
    source 10
    target 25
  ]
  edge [
    source 10
    target 26
  ]
  edge [
    source 10
    target 27
  ]
  edge [
    source 10
    target 28
  ]
  edge [
    source 10
    target 14
  ]
  edge [
    source 10
    target 36
  ]
  edge [
    source 10
    target 32
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 10
    target 17
  ]
  edge [
    source 10
    target 18
  ]
  edge [
    source 10
    target 40
  ]
  edge [
    source 11
    target 16
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 17
  ]
  edge [
    source 13
    target 18
  ]
  edge [
    source 13
    target 19
  ]
  edge [
    source 13
    target 20
  ]
  edge [
    source 13
    target 21
  ]
  edge [
    source 13
    target 23
  ]
  edge [
    source 13
    target 24
  ]
  edge [
    source 13
    target 25
  ]
  edge [
    source 13
    target 26
  ]
  edge [
    source 13
    target 27
  ]
  edge [
    source 13
    target 22
  ]
  edge [
    source 13
    target 29
  ]
  edge [
    source 13
    target 30
  ]
  edge [
    source 13
    target 31
  ]
  edge [
    source 13
    target 16
  ]
  edge [
    source 13
    target 33
  ]
  edge [
    source 13
    target 32
  ]
  edge [
    source 13
    target 36
  ]
  edge [
    source 13
    target 42
  ]
  edge [
    source 13
    target 35
  ]
  edge [
    source 13
    target 43
  ]
  edge [
    source 13
    target 34
  ]
  edge [
    source 13
    target 44
  ]
  edge [
    source 13
    target 45
  ]
  edge [
    source 13
    target 46
  ]
  edge [
    source 16
    target 32
  ]
  edge [
    source 16
    target 33
  ]
  edge [
    source 16
    target 34
  ]
  edge [
    source 16
    target 35
  ]
  edge [
    source 16
    target 38
  ]
  edge [
    source 16
    target 41
  ]
  edge [
    source 16
    target 27
  ]
  edge [
    source 16
    target 19
  ]
  edge [
    source 16
    target 20
  ]
  edge [
    source 16
    target 43
  ]
  edge [
    source 16
    target 39
  ]
  edge [
    source 16
    target 44
  ]
  edge [
    source 16
    target 49
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 20
  ]
  edge [
    source 18
    target 21
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 21
  ]
  edge [
    source 19
    target 23
  ]
  edge [
    source 19
    target 24
  ]
  edge [
    source 19
    target 25
  ]
  edge [
    source 19
    target 26
  ]
  edge [
    source 19
    target 27
  ]
  edge [
    source 19
    target 29
  ]
  edge [
    source 19
    target 30
  ]
  edge [
    source 19
    target 31
  ]
  edge [
    source 19
    target 36
  ]
  edge [
    source 19
    target 42
  ]
  edge [
    source 19
    target 35
  ]
  edge [
    source 19
    target 43
  ]
  edge [
    source 19
    target 34
  ]
  edge [
    source 19
    target 44
  ]
  edge [
    source 19
    target 46
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 29
  ]
  edge [
    source 20
    target 30
  ]
  edge [
    source 20
    target 31
  ]
  edge [
    source 20
    target 36
  ]
  edge [
    source 20
    target 42
  ]
  edge [
    source 20
    target 35
  ]
  edge [
    source 20
    target 43
  ]
  edge [
    source 20
    target 34
  ]
  edge [
    source 20
    target 44
  ]
  edge [
    source 20
    target 27
  ]
  edge [
    source 20
    target 46
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 25
  ]
  edge [
    source 23
    target 26
  ]
  edge [
    source 23
    target 27
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 26
  ]
  edge [
    source 24
    target 27
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 27
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 46
  ]
  edge [
    source 27
    target 35
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 31
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 34
    target 44
  ]
  edge [
    source 35
    target 43
  ]
  edge [
    source 36
    target 42
  ]
  edge [
    source 38
    target 49
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 50
    target 51
  ]
]
