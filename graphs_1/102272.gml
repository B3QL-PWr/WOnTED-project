graph [
  maxDegree 47
  minDegree 1
  meanDegree 1.9701492537313432
  density 0.029850746268656716
  graphCliqueNumber 2
  node [
    id 0
    label "czego"
    origin "text"
  ]
  node [
    id 1
    label "s&#322;u&#380;y&#263;"
    origin "text"
  ]
  node [
    id 2
    label "prawo"
    origin "text"
  ]
  node [
    id 3
    label "autorski"
    origin "text"
  ]
  node [
    id 4
    label "use"
  ]
  node [
    id 5
    label "trwa&#263;"
  ]
  node [
    id 6
    label "by&#263;"
  ]
  node [
    id 7
    label "&#380;o&#322;nierz"
  ]
  node [
    id 8
    label "pies"
  ]
  node [
    id 9
    label "robi&#263;"
  ]
  node [
    id 10
    label "wait"
  ]
  node [
    id 11
    label "pomaga&#263;"
  ]
  node [
    id 12
    label "cel"
  ]
  node [
    id 13
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 14
    label "przydawa&#263;_si&#281;"
  ]
  node [
    id 15
    label "pracowa&#263;"
  ]
  node [
    id 16
    label "suffice"
  ]
  node [
    id 17
    label "match"
  ]
  node [
    id 18
    label "s&#322;ugiwa&#263;"
  ]
  node [
    id 19
    label "obserwacja"
  ]
  node [
    id 20
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 21
    label "nauka_prawa"
  ]
  node [
    id 22
    label "dominion"
  ]
  node [
    id 23
    label "normatywizm"
  ]
  node [
    id 24
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 25
    label "qualification"
  ]
  node [
    id 26
    label "opis"
  ]
  node [
    id 27
    label "regu&#322;a_Allena"
  ]
  node [
    id 28
    label "normalizacja"
  ]
  node [
    id 29
    label "kazuistyka"
  ]
  node [
    id 30
    label "regu&#322;a_Glogera"
  ]
  node [
    id 31
    label "kultura_duchowa"
  ]
  node [
    id 32
    label "prawo_karne"
  ]
  node [
    id 33
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 34
    label "standard"
  ]
  node [
    id 35
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 36
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 37
    label "struktura"
  ]
  node [
    id 38
    label "szko&#322;a"
  ]
  node [
    id 39
    label "prawo_karne_procesowe"
  ]
  node [
    id 40
    label "prawo_Mendla"
  ]
  node [
    id 41
    label "przepis"
  ]
  node [
    id 42
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 43
    label "criterion"
  ]
  node [
    id 44
    label "kanonistyka"
  ]
  node [
    id 45
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 46
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 47
    label "wykonawczy"
  ]
  node [
    id 48
    label "twierdzenie"
  ]
  node [
    id 49
    label "judykatura"
  ]
  node [
    id 50
    label "legislacyjnie"
  ]
  node [
    id 51
    label "umocowa&#263;"
  ]
  node [
    id 52
    label "podmiot"
  ]
  node [
    id 53
    label "procesualistyka"
  ]
  node [
    id 54
    label "kierunek"
  ]
  node [
    id 55
    label "kryminologia"
  ]
  node [
    id 56
    label "kryminalistyka"
  ]
  node [
    id 57
    label "cywilistyka"
  ]
  node [
    id 58
    label "law"
  ]
  node [
    id 59
    label "zasada_d'Alemberta"
  ]
  node [
    id 60
    label "jurisprudence"
  ]
  node [
    id 61
    label "zasada"
  ]
  node [
    id 62
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 63
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 64
    label "w&#322;asny"
  ]
  node [
    id 65
    label "oryginalny"
  ]
  node [
    id 66
    label "autorsko"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
]
