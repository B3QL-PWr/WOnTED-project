graph [
  maxDegree 3
  minDegree 1
  meanDegree 1.6
  density 0.4
  graphCliqueNumber 2
  node [
    id 0
    label "gramatyk"
    origin "text"
  ]
  node [
    id 1
    label "kombinatoryczny"
    origin "text"
  ]
  node [
    id 2
    label "j&#281;zykoznawca"
  ]
  node [
    id 3
    label "kombinatorycznie"
  ]
  node [
    id 4
    label "niejednakowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
]
