graph [
  maxDegree 13
  minDegree 1
  meanDegree 1.875
  density 0.125
  graphCliqueNumber 2
  node [
    id 0
    label "efekt"
    origin "text"
  ]
  node [
    id 1
    label "dochodowy"
    origin "text"
  ]
  node [
    id 2
    label "dzia&#322;anie"
  ]
  node [
    id 3
    label "typ"
  ]
  node [
    id 4
    label "impression"
  ]
  node [
    id 5
    label "robienie_wra&#380;enia"
  ]
  node [
    id 6
    label "wra&#380;enie"
  ]
  node [
    id 7
    label "przyczyna"
  ]
  node [
    id 8
    label "zrobienie_wra&#380;enia"
  ]
  node [
    id 9
    label "robi&#263;_wra&#380;enie"
  ]
  node [
    id 10
    label "&#347;rodek"
  ]
  node [
    id 11
    label "event"
  ]
  node [
    id 12
    label "zrobi&#263;_wra&#380;enie"
  ]
  node [
    id 13
    label "rezultat"
  ]
  node [
    id 14
    label "korzystny"
  ]
  node [
    id 15
    label "dochodowo"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
]
