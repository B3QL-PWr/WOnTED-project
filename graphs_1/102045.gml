graph [
  maxDegree 23
  minDegree 1
  meanDegree 2.0853658536585367
  density 0.012793655543917402
  graphCliqueNumber 3
  node [
    id 0
    label "pi&#322;karz"
    origin "text"
  ]
  node [
    id 1
    label "turu"
    origin "text"
  ]
  node [
    id 2
    label "ozorek"
    origin "text"
  ]
  node [
    id 3
    label "juz"
    origin "text"
  ]
  node [
    id 4
    label "wznowi&#263;"
    origin "text"
  ]
  node [
    id 5
    label "trening"
    origin "text"
  ]
  node [
    id 6
    label "przerwa"
    origin "text"
  ]
  node [
    id 7
    label "zimowy"
    origin "text"
  ]
  node [
    id 8
    label "nasa"
    origin "text"
  ]
  node [
    id 9
    label "zesp&#243;&#322;"
    origin "text"
  ]
  node [
    id 10
    label "wzmocni&#263;"
    origin "text"
  ]
  node [
    id 11
    label "jak"
    origin "text"
  ]
  node [
    id 12
    label "narazie"
    origin "text"
  ]
  node [
    id 13
    label "dwa"
    origin "text"
  ]
  node [
    id 14
    label "pi&#261;toligowego"
    origin "text"
  ]
  node [
    id 15
    label "lks"
    origin "text"
  ]
  node [
    id 16
    label "rosan&#243;w"
    origin "text"
  ]
  node [
    id 17
    label "dawid"
    origin "text"
  ]
  node [
    id 18
    label "boraty&#324;ski"
    origin "text"
  ]
  node [
    id 19
    label "jarek"
    origin "text"
  ]
  node [
    id 20
    label "pietrzak"
    origin "text"
  ]
  node [
    id 21
    label "tak&#380;e"
    origin "text"
  ]
  node [
    id 22
    label "orze&#322;"
    origin "text"
  ]
  node [
    id 23
    label "parz&#281;czew"
    origin "text"
  ]
  node [
    id 24
    label "krzysztof"
    origin "text"
  ]
  node [
    id 25
    label "szabela"
    origin "text"
  ]
  node [
    id 26
    label "tomasz"
    origin "text"
  ]
  node [
    id 27
    label "jagielski"
    origin "text"
  ]
  node [
    id 28
    label "kr&#281;t"
    origin "text"
  ]
  node [
    id 29
    label "marcin"
    origin "text"
  ]
  node [
    id 30
    label "gaw&#281;da"
    origin "text"
  ]
  node [
    id 31
    label "maciek"
    origin "text"
  ]
  node [
    id 32
    label "sobote"
    origin "text"
  ]
  node [
    id 33
    label "luty"
    origin "text"
  ]
  node [
    id 34
    label "godz"
    origin "text"
  ]
  node [
    id 35
    label "nasi"
    origin "text"
  ]
  node [
    id 36
    label "rozegra&#263;"
    origin "text"
  ]
  node [
    id 37
    label "pierwszy"
    origin "text"
  ]
  node [
    id 38
    label "mecz"
    origin "text"
  ]
  node [
    id 39
    label "kontrolny"
    origin "text"
  ]
  node [
    id 40
    label "klasowy"
    origin "text"
  ]
  node [
    id 41
    label "polonia"
    origin "text"
  ]
  node [
    id 42
    label "andrzej"
    origin "text"
  ]
  node [
    id 43
    label "gracz"
  ]
  node [
    id 44
    label "legionista"
  ]
  node [
    id 45
    label "sportowiec"
  ]
  node [
    id 46
    label "Daniel_Dubicki"
  ]
  node [
    id 47
    label "pieczarkowiec"
  ]
  node [
    id 48
    label "ozorkowate"
  ]
  node [
    id 49
    label "saprotrof"
  ]
  node [
    id 50
    label "grzyb"
  ]
  node [
    id 51
    label "paso&#380;yt"
  ]
  node [
    id 52
    label "podroby"
  ]
  node [
    id 53
    label "sum_up"
  ]
  node [
    id 54
    label "przywr&#243;ci&#263;"
  ]
  node [
    id 55
    label "odblokowa&#263;"
  ]
  node [
    id 56
    label "doskonalenie"
  ]
  node [
    id 57
    label "training"
  ]
  node [
    id 58
    label "zgrupowanie"
  ]
  node [
    id 59
    label "ruch"
  ]
  node [
    id 60
    label "&#263;wiczenie"
  ]
  node [
    id 61
    label "obw&#243;d"
  ]
  node [
    id 62
    label "warsztat"
  ]
  node [
    id 63
    label "pauza"
  ]
  node [
    id 64
    label "miejsce"
  ]
  node [
    id 65
    label "przedzia&#322;"
  ]
  node [
    id 66
    label "czas"
  ]
  node [
    id 67
    label "sezonowy"
  ]
  node [
    id 68
    label "zimowo"
  ]
  node [
    id 69
    label "ch&#322;odny"
  ]
  node [
    id 70
    label "typowy"
  ]
  node [
    id 71
    label "hibernowy"
  ]
  node [
    id 72
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 73
    label "whole"
  ]
  node [
    id 74
    label "odm&#322;adza&#263;"
  ]
  node [
    id 75
    label "zabudowania"
  ]
  node [
    id 76
    label "odm&#322;odzenie"
  ]
  node [
    id 77
    label "zespolik"
  ]
  node [
    id 78
    label "skupienie"
  ]
  node [
    id 79
    label "schorzenie"
  ]
  node [
    id 80
    label "grupa"
  ]
  node [
    id 81
    label "Depeche_Mode"
  ]
  node [
    id 82
    label "Mazowsze"
  ]
  node [
    id 83
    label "ro&#347;lina"
  ]
  node [
    id 84
    label "zbi&#243;r"
  ]
  node [
    id 85
    label "The_Beatles"
  ]
  node [
    id 86
    label "group"
  ]
  node [
    id 87
    label "&#346;wietliki"
  ]
  node [
    id 88
    label "odm&#322;adzanie"
  ]
  node [
    id 89
    label "batch"
  ]
  node [
    id 90
    label "consolidate"
  ]
  node [
    id 91
    label "zwi&#281;kszy&#263;"
  ]
  node [
    id 92
    label "fixate"
  ]
  node [
    id 93
    label "spowodowa&#263;"
  ]
  node [
    id 94
    label "wzm&#243;c"
  ]
  node [
    id 95
    label "uskuteczni&#263;"
  ]
  node [
    id 96
    label "podnie&#347;&#263;"
  ]
  node [
    id 97
    label "utrwali&#263;"
  ]
  node [
    id 98
    label "wyregulowa&#263;"
  ]
  node [
    id 99
    label "umocnienie"
  ]
  node [
    id 100
    label "reinforce"
  ]
  node [
    id 101
    label "zabezpieczy&#263;"
  ]
  node [
    id 102
    label "zmieni&#263;"
  ]
  node [
    id 103
    label "mocny"
  ]
  node [
    id 104
    label "byd&#322;o"
  ]
  node [
    id 105
    label "zobo"
  ]
  node [
    id 106
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 107
    label "yakalo"
  ]
  node [
    id 108
    label "dzo"
  ]
  node [
    id 109
    label "ptak_drapie&#380;ny"
  ]
  node [
    id 110
    label "gapa"
  ]
  node [
    id 111
    label "or&#322;y"
  ]
  node [
    id 112
    label "eagle"
  ]
  node [
    id 113
    label "talent"
  ]
  node [
    id 114
    label "awers"
  ]
  node [
    id 115
    label "bystrzak"
  ]
  node [
    id 116
    label "angular_momentum"
  ]
  node [
    id 117
    label "moment"
  ]
  node [
    id 118
    label "gossip"
  ]
  node [
    id 119
    label "rozmowa"
  ]
  node [
    id 120
    label "opowiadanie"
  ]
  node [
    id 121
    label "gadu_gadu"
  ]
  node [
    id 122
    label "narrative"
  ]
  node [
    id 123
    label "talk"
  ]
  node [
    id 124
    label "brzuch"
  ]
  node [
    id 125
    label "miesi&#261;c"
  ]
  node [
    id 126
    label "Matka_Boska_Gromniczna"
  ]
  node [
    id 127
    label "walentynki"
  ]
  node [
    id 128
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 129
    label "przeprowadzi&#263;"
  ]
  node [
    id 130
    label "play"
  ]
  node [
    id 131
    label "najwa&#380;niejszy"
  ]
  node [
    id 132
    label "pocz&#261;tkowy"
  ]
  node [
    id 133
    label "dobry"
  ]
  node [
    id 134
    label "ch&#281;tny"
  ]
  node [
    id 135
    label "dzie&#324;"
  ]
  node [
    id 136
    label "pr&#281;dki"
  ]
  node [
    id 137
    label "obrona"
  ]
  node [
    id 138
    label "gra"
  ]
  node [
    id 139
    label "dwumecz"
  ]
  node [
    id 140
    label "game"
  ]
  node [
    id 141
    label "serw"
  ]
  node [
    id 142
    label "supervision"
  ]
  node [
    id 143
    label "kontrolnie"
  ]
  node [
    id 144
    label "pr&#243;bny"
  ]
  node [
    id 145
    label "niejednolity"
  ]
  node [
    id 146
    label "klasowo"
  ]
  node [
    id 147
    label "przychylny"
  ]
  node [
    id 148
    label "wspania&#322;y"
  ]
  node [
    id 149
    label "Turu"
  ]
  node [
    id 150
    label "LKS"
  ]
  node [
    id 151
    label "Rosan&#243;w"
  ]
  node [
    id 152
    label "Parz&#281;czew"
  ]
  node [
    id 153
    label "jarka"
  ]
  node [
    id 154
    label "Pietrzak"
  ]
  node [
    id 155
    label "Marcin"
  ]
  node [
    id 156
    label "Dawida"
  ]
  node [
    id 157
    label "Boraty&#324;ski"
  ]
  node [
    id 158
    label "Tomasz"
  ]
  node [
    id 159
    label "Jagielski"
  ]
  node [
    id 160
    label "Krzysztofa"
  ]
  node [
    id 161
    label "Szabela"
  ]
  node [
    id 162
    label "Polonia"
  ]
  node [
    id 163
    label "Andrzej"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 40
  ]
  edge [
    source 9
    target 41
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 39
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 108
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 27
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 109
  ]
  edge [
    source 22
    target 110
  ]
  edge [
    source 22
    target 111
  ]
  edge [
    source 22
    target 112
  ]
  edge [
    source 22
    target 113
  ]
  edge [
    source 22
    target 114
  ]
  edge [
    source 22
    target 115
  ]
  edge [
    source 22
    target 152
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 40
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 116
  ]
  edge [
    source 28
    target 117
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 32
  ]
  edge [
    source 30
    target 118
  ]
  edge [
    source 30
    target 119
  ]
  edge [
    source 30
    target 120
  ]
  edge [
    source 30
    target 121
  ]
  edge [
    source 30
    target 122
  ]
  edge [
    source 30
    target 123
  ]
  edge [
    source 30
    target 155
  ]
  edge [
    source 31
    target 124
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 37
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 37
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 125
  ]
  edge [
    source 33
    target 126
  ]
  edge [
    source 33
    target 127
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 128
  ]
  edge [
    source 36
    target 129
  ]
  edge [
    source 36
    target 130
  ]
  edge [
    source 36
    target 93
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 131
  ]
  edge [
    source 37
    target 132
  ]
  edge [
    source 37
    target 133
  ]
  edge [
    source 37
    target 134
  ]
  edge [
    source 37
    target 135
  ]
  edge [
    source 37
    target 136
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 137
  ]
  edge [
    source 38
    target 138
  ]
  edge [
    source 38
    target 139
  ]
  edge [
    source 38
    target 140
  ]
  edge [
    source 38
    target 141
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 142
  ]
  edge [
    source 39
    target 143
  ]
  edge [
    source 39
    target 144
  ]
  edge [
    source 40
    target 145
  ]
  edge [
    source 40
    target 146
  ]
  edge [
    source 40
    target 147
  ]
  edge [
    source 40
    target 148
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 150
    target 151
  ]
  edge [
    source 153
    target 154
  ]
  edge [
    source 156
    target 157
  ]
  edge [
    source 158
    target 159
  ]
  edge [
    source 160
    target 161
  ]
  edge [
    source 162
    target 163
  ]
]
