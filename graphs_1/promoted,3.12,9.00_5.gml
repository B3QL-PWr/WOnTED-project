graph [
  maxDegree 15
  minDegree 1
  meanDegree 2
  density 0.03773584905660377
  graphCliqueNumber 2
  node [
    id 0
    label "imam"
    origin "text"
  ]
  node [
    id 1
    label "zwr&#243;ci&#263;"
    origin "text"
  ]
  node [
    id 2
    label "uwaga"
    origin "text"
  ]
  node [
    id 3
    label "polska"
    origin "text"
  ]
  node [
    id 4
    label "francja"
    origin "text"
  ]
  node [
    id 5
    label "por&#243;wnanie"
    origin "text"
  ]
  node [
    id 6
    label "dwa"
    origin "text"
  ]
  node [
    id 7
    label "&#347;wi&#261;teczny"
    origin "text"
  ]
  node [
    id 8
    label "iluminacja"
    origin "text"
  ]
  node [
    id 9
    label "twitter"
    origin "text"
  ]
  node [
    id 10
    label "ulem"
  ]
  node [
    id 11
    label "hod&#380;a"
  ]
  node [
    id 12
    label "return"
  ]
  node [
    id 13
    label "rzygn&#261;&#263;"
  ]
  node [
    id 14
    label "shaftowa&#263;_si&#281;"
  ]
  node [
    id 15
    label "wydali&#263;"
  ]
  node [
    id 16
    label "direct"
  ]
  node [
    id 17
    label "zespawa&#263;_si&#281;"
  ]
  node [
    id 18
    label "przeznaczy&#263;"
  ]
  node [
    id 19
    label "give"
  ]
  node [
    id 20
    label "ustawi&#263;"
  ]
  node [
    id 21
    label "przekaza&#263;"
  ]
  node [
    id 22
    label "regenerate"
  ]
  node [
    id 23
    label "z_powrotem"
  ]
  node [
    id 24
    label "set"
  ]
  node [
    id 25
    label "nagana"
  ]
  node [
    id 26
    label "wypowied&#378;"
  ]
  node [
    id 27
    label "stan"
  ]
  node [
    id 28
    label "dzienniczek"
  ]
  node [
    id 29
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 30
    label "wzgl&#261;d"
  ]
  node [
    id 31
    label "gossip"
  ]
  node [
    id 32
    label "upomnienie"
  ]
  node [
    id 33
    label "tekst"
  ]
  node [
    id 34
    label "simile"
  ]
  node [
    id 35
    label "figura_stylistyczna"
  ]
  node [
    id 36
    label "zestawienie"
  ]
  node [
    id 37
    label "zr&#243;&#380;nicowanie"
  ]
  node [
    id 38
    label "comparison"
  ]
  node [
    id 39
    label "zanalizowanie"
  ]
  node [
    id 40
    label "dzie&#324;_wolny"
  ]
  node [
    id 41
    label "&#347;wi&#281;tny"
  ]
  node [
    id 42
    label "&#347;wi&#261;tecznie"
  ]
  node [
    id 43
    label "wyj&#261;tkowy"
  ]
  node [
    id 44
    label "od&#347;wi&#281;tny"
  ]
  node [
    id 45
    label "od&#347;wi&#281;tnie"
  ]
  node [
    id 46
    label "obrz&#281;dowy"
  ]
  node [
    id 47
    label "uroczysty"
  ]
  node [
    id 48
    label "o&#347;wietlenie"
  ]
  node [
    id 49
    label "light"
  ]
  node [
    id 50
    label "ilustracja"
  ]
  node [
    id 51
    label "u&#347;wiadomienie"
  ]
  node [
    id 52
    label "poznanie"
  ]
  node [
    id 53
    label "konto"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 27
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 9
    target 53
  ]
]
