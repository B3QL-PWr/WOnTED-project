graph [
  maxDegree 23
  minDegree 1
  meanDegree 1.9259259259259258
  density 0.07407407407407407
  graphCliqueNumber 2
  node [
    id 0
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 1
    label "kto"
    origin "text"
  ]
  node [
    id 2
    label "zaplusuje"
    origin "text"
  ]
  node [
    id 3
    label "dosta&#263;"
    origin "text"
  ]
  node [
    id 4
    label "mema"
    origin "text"
  ]
  node [
    id 5
    label "jaki&#347;"
  ]
  node [
    id 6
    label "get"
  ]
  node [
    id 7
    label "doczeka&#263;"
  ]
  node [
    id 8
    label "zwiastun"
  ]
  node [
    id 9
    label "si&#281;gn&#261;&#263;"
  ]
  node [
    id 10
    label "develop"
  ]
  node [
    id 11
    label "catch"
  ]
  node [
    id 12
    label "uzyska&#263;"
  ]
  node [
    id 13
    label "kupi&#263;"
  ]
  node [
    id 14
    label "wzi&#261;&#263;"
  ]
  node [
    id 15
    label "naby&#263;"
  ]
  node [
    id 16
    label "nabawienie_si&#281;"
  ]
  node [
    id 17
    label "obskoczy&#263;"
  ]
  node [
    id 18
    label "zapanowa&#263;"
  ]
  node [
    id 19
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 20
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 21
    label "zrobi&#263;"
  ]
  node [
    id 22
    label "nabawianie_si&#281;"
  ]
  node [
    id 23
    label "range"
  ]
  node [
    id 24
    label "schorzenie"
  ]
  node [
    id 25
    label "wystarczy&#263;"
  ]
  node [
    id 26
    label "wysta&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
]
