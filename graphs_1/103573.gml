graph [
  maxDegree 39
  minDegree 1
  meanDegree 2.1354838709677417
  density 0.006910951038730556
  graphCliqueNumber 3
  node [
    id 0
    label "nasz"
    origin "text"
  ]
  node [
    id 1
    label "&#322;awka"
    origin "text"
  ]
  node [
    id 2
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 3
    label "tak"
    origin "text"
  ]
  node [
    id 4
    label "powiedzie&#263;"
    origin "text"
  ]
  node [
    id 5
    label "pierwsza"
    origin "text"
  ]
  node [
    id 6
    label "msza"
    origin "text"
  ]
  node [
    id 7
    label "siedzie&#263;"
    origin "text"
  ]
  node [
    id 8
    label "pocz&#261;tkowo"
    origin "text"
  ]
  node [
    id 9
    label "ondricek"
    origin "text"
  ]
  node [
    id 10
    label "wierci&#263;"
    origin "text"
  ]
  node [
    id 11
    label "si&#281;"
    origin "text"
  ]
  node [
    id 12
    label "namawia&#263;"
    origin "text"
  ]
  node [
    id 13
    label "&#380;ebyby&#263;"
    origin "text"
  ]
  node [
    id 14
    label "p&#243;j&#347;&#263;"
    origin "text"
  ]
  node [
    id 15
    label "bardzo"
    origin "text"
  ]
  node [
    id 16
    label "prz&#243;d"
    origin "text"
  ]
  node [
    id 17
    label "albo"
    origin "text"
  ]
  node [
    id 18
    label "usi&#261;&#347;&#263;"
    origin "text"
  ]
  node [
    id 19
    label "bok"
    origin "text"
  ]
  node [
    id 20
    label "kr&#281;ci&#263;"
    origin "text"
  ]
  node [
    id 21
    label "stawa&#263;"
    origin "text"
  ]
  node [
    id 22
    label "mocno"
    origin "text"
  ]
  node [
    id 23
    label "wysuwa&#263;"
    origin "text"
  ]
  node [
    id 24
    label "g&#322;owa"
    origin "text"
  ]
  node [
    id 25
    label "&#380;eby"
    origin "text"
  ]
  node [
    id 26
    label "udowodni&#263;"
    origin "text"
  ]
  node [
    id 27
    label "jak"
    origin "text"
  ]
  node [
    id 28
    label "niewygodnie"
    origin "text"
  ]
  node [
    id 29
    label "patrze&#263;"
    origin "text"
  ]
  node [
    id 30
    label "ksi&#261;dz"
    origin "text"
  ]
  node [
    id 31
    label "przez"
    origin "text"
  ]
  node [
    id 32
    label "krata"
    origin "text"
  ]
  node [
    id 33
    label "ko&#347;ci&#243;&#322;"
    origin "text"
  ]
  node [
    id 34
    label "duszno"
    origin "text"
  ]
  node [
    id 35
    label "przedsionek"
    origin "text"
  ]
  node [
    id 36
    label "by&#263;"
    origin "text"
  ]
  node [
    id 37
    label "troch&#281;"
    origin "text"
  ]
  node [
    id 38
    label "ch&#322;odno"
    origin "text"
  ]
  node [
    id 39
    label "wieja"
    origin "text"
  ]
  node [
    id 40
    label "czyj&#347;"
  ]
  node [
    id 41
    label "krzes&#322;o"
  ]
  node [
    id 42
    label "siedzenie"
  ]
  node [
    id 43
    label "grupa"
  ]
  node [
    id 44
    label "blat"
  ]
  node [
    id 45
    label "mebel"
  ]
  node [
    id 46
    label "klasa"
  ]
  node [
    id 47
    label "free"
  ]
  node [
    id 48
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 49
    label "express"
  ]
  node [
    id 50
    label "rzekn&#261;&#263;"
  ]
  node [
    id 51
    label "okre&#347;li&#263;"
  ]
  node [
    id 52
    label "wyrazi&#263;"
  ]
  node [
    id 53
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 54
    label "unwrap"
  ]
  node [
    id 55
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 56
    label "convey"
  ]
  node [
    id 57
    label "discover"
  ]
  node [
    id 58
    label "wydoby&#263;"
  ]
  node [
    id 59
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 60
    label "poda&#263;"
  ]
  node [
    id 61
    label "godzina"
  ]
  node [
    id 62
    label "credo"
  ]
  node [
    id 63
    label "episto&#322;a"
  ]
  node [
    id 64
    label "prezbiter"
  ]
  node [
    id 65
    label "nabo&#380;e&#324;stwo"
  ]
  node [
    id 66
    label "kazanie"
  ]
  node [
    id 67
    label "kanon"
  ]
  node [
    id 68
    label "utw&#243;r"
  ]
  node [
    id 69
    label "sekreta"
  ]
  node [
    id 70
    label "przeistoczenie"
  ]
  node [
    id 71
    label "prefacja"
  ]
  node [
    id 72
    label "ofiarowanie"
  ]
  node [
    id 73
    label "katolicyzm"
  ]
  node [
    id 74
    label "prawos&#322;awie"
  ]
  node [
    id 75
    label "podniesienie"
  ]
  node [
    id 76
    label "confiteor"
  ]
  node [
    id 77
    label "ofertorium"
  ]
  node [
    id 78
    label "czytanie"
  ]
  node [
    id 79
    label "dzie&#322;o"
  ]
  node [
    id 80
    label "kolekta"
  ]
  node [
    id 81
    label "komunia"
  ]
  node [
    id 82
    label "gloria"
  ]
  node [
    id 83
    label "ewangelia"
  ]
  node [
    id 84
    label "Mass"
  ]
  node [
    id 85
    label "trwa&#263;"
  ]
  node [
    id 86
    label "sit"
  ]
  node [
    id 87
    label "pause"
  ]
  node [
    id 88
    label "ptak"
  ]
  node [
    id 89
    label "garowa&#263;"
  ]
  node [
    id 90
    label "mieszka&#263;"
  ]
  node [
    id 91
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 92
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 93
    label "przebywa&#263;"
  ]
  node [
    id 94
    label "brood"
  ]
  node [
    id 95
    label "zwierz&#281;"
  ]
  node [
    id 96
    label "doprowadza&#263;"
  ]
  node [
    id 97
    label "spoczywa&#263;"
  ]
  node [
    id 98
    label "tkwi&#263;"
  ]
  node [
    id 99
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 100
    label "pocz&#261;tkowy"
  ]
  node [
    id 101
    label "dzieci&#281;co"
  ]
  node [
    id 102
    label "zrazu"
  ]
  node [
    id 103
    label "przenika&#263;"
  ]
  node [
    id 104
    label "dra&#380;ni&#263;"
  ]
  node [
    id 105
    label "robi&#263;"
  ]
  node [
    id 106
    label "tug"
  ]
  node [
    id 107
    label "zam&#281;cza&#263;"
  ]
  node [
    id 108
    label "&#380;&#322;obi&#263;"
  ]
  node [
    id 109
    label "rusza&#263;"
  ]
  node [
    id 110
    label "przekonywa&#263;"
  ]
  node [
    id 111
    label "prompt"
  ]
  node [
    id 112
    label "opu&#347;ci&#263;"
  ]
  node [
    id 113
    label "poruszy&#263;_si&#281;"
  ]
  node [
    id 114
    label "proceed"
  ]
  node [
    id 115
    label "powie&#347;&#263;_si&#281;"
  ]
  node [
    id 116
    label "popsu&#263;_si&#281;"
  ]
  node [
    id 117
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 118
    label "kopn&#261;&#263;_si&#281;"
  ]
  node [
    id 119
    label "zacz&#261;&#263;"
  ]
  node [
    id 120
    label "zmieni&#263;"
  ]
  node [
    id 121
    label "zosta&#263;"
  ]
  node [
    id 122
    label "sail"
  ]
  node [
    id 123
    label "leave"
  ]
  node [
    id 124
    label "uda&#263;_si&#281;"
  ]
  node [
    id 125
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 126
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 127
    label "zrobi&#263;"
  ]
  node [
    id 128
    label "wyp&#322;yn&#261;&#263;"
  ]
  node [
    id 129
    label "sprzeda&#263;_si&#281;"
  ]
  node [
    id 130
    label "przyj&#261;&#263;"
  ]
  node [
    id 131
    label "potoczy&#263;_si&#281;"
  ]
  node [
    id 132
    label "become"
  ]
  node [
    id 133
    label "play_along"
  ]
  node [
    id 134
    label "travel"
  ]
  node [
    id 135
    label "w_chuj"
  ]
  node [
    id 136
    label "cia&#322;o"
  ]
  node [
    id 137
    label "kierunek"
  ]
  node [
    id 138
    label "przestrze&#324;"
  ]
  node [
    id 139
    label "strona"
  ]
  node [
    id 140
    label "zaj&#261;&#263;"
  ]
  node [
    id 141
    label "uplasowa&#263;_si&#281;"
  ]
  node [
    id 142
    label "mount"
  ]
  node [
    id 143
    label "spocz&#261;&#263;"
  ]
  node [
    id 144
    label "wyl&#261;dowa&#263;"
  ]
  node [
    id 145
    label "przyst&#261;pi&#263;"
  ]
  node [
    id 146
    label "sie&#347;&#263;"
  ]
  node [
    id 147
    label "strzelba"
  ]
  node [
    id 148
    label "wielok&#261;t"
  ]
  node [
    id 149
    label "&#347;ciana"
  ]
  node [
    id 150
    label "odcinek"
  ]
  node [
    id 151
    label "tu&#322;&#243;w"
  ]
  node [
    id 152
    label "bro&#324;_my&#347;liwska"
  ]
  node [
    id 153
    label "lufa"
  ]
  node [
    id 154
    label "beat_around_the_bush"
  ]
  node [
    id 155
    label "mata&#263;"
  ]
  node [
    id 156
    label "podporz&#261;dkowywa&#263;"
  ]
  node [
    id 157
    label "order"
  ]
  node [
    id 158
    label "k&#322;ama&#263;"
  ]
  node [
    id 159
    label "cheat"
  ]
  node [
    id 160
    label "nagrywa&#263;"
  ]
  node [
    id 161
    label "tworzy&#263;"
  ]
  node [
    id 162
    label "wrench"
  ]
  node [
    id 163
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 164
    label "trze&#263;"
  ]
  node [
    id 165
    label "wytwarza&#263;"
  ]
  node [
    id 166
    label "zostawa&#263;"
  ]
  node [
    id 167
    label "wystarcza&#263;"
  ]
  node [
    id 168
    label "stop"
  ]
  node [
    id 169
    label "zatrzymywa&#263;_si&#281;"
  ]
  node [
    id 170
    label "przybywa&#263;"
  ]
  node [
    id 171
    label "zmienia&#263;_pozycj&#281;"
  ]
  node [
    id 172
    label "przestawa&#263;"
  ]
  node [
    id 173
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 174
    label "pull"
  ]
  node [
    id 175
    label "unosi&#263;_si&#281;"
  ]
  node [
    id 176
    label "zdecydowanie"
  ]
  node [
    id 177
    label "stabilnie"
  ]
  node [
    id 178
    label "widocznie"
  ]
  node [
    id 179
    label "silny"
  ]
  node [
    id 180
    label "silnie"
  ]
  node [
    id 181
    label "niepodwa&#380;alnie"
  ]
  node [
    id 182
    label "konkretnie"
  ]
  node [
    id 183
    label "intensywny"
  ]
  node [
    id 184
    label "przekonuj&#261;co"
  ]
  node [
    id 185
    label "strongly"
  ]
  node [
    id 186
    label "niema&#322;o"
  ]
  node [
    id 187
    label "szczerze"
  ]
  node [
    id 188
    label "mocny"
  ]
  node [
    id 189
    label "powerfully"
  ]
  node [
    id 190
    label "wyjmowa&#263;"
  ]
  node [
    id 191
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 192
    label "przesuwa&#263;"
  ]
  node [
    id 193
    label "raise"
  ]
  node [
    id 194
    label "exsert"
  ]
  node [
    id 195
    label "cz&#322;owiek"
  ]
  node [
    id 196
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 197
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 198
    label "ucho"
  ]
  node [
    id 199
    label "makrocefalia"
  ]
  node [
    id 200
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 201
    label "m&#243;zg"
  ]
  node [
    id 202
    label "kierownictwo"
  ]
  node [
    id 203
    label "czaszka"
  ]
  node [
    id 204
    label "dekiel"
  ]
  node [
    id 205
    label "umys&#322;"
  ]
  node [
    id 206
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 207
    label "&#347;ci&#281;cie"
  ]
  node [
    id 208
    label "sztuka"
  ]
  node [
    id 209
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 210
    label "g&#243;ra"
  ]
  node [
    id 211
    label "byd&#322;o"
  ]
  node [
    id 212
    label "alkohol"
  ]
  node [
    id 213
    label "wiedza"
  ]
  node [
    id 214
    label "ro&#347;lina"
  ]
  node [
    id 215
    label "&#347;ci&#281;gno"
  ]
  node [
    id 216
    label "&#380;ycie"
  ]
  node [
    id 217
    label "pryncypa&#322;"
  ]
  node [
    id 218
    label "fryzura"
  ]
  node [
    id 219
    label "noosfera"
  ]
  node [
    id 220
    label "kierowa&#263;"
  ]
  node [
    id 221
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 222
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 223
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 224
    label "cecha"
  ]
  node [
    id 225
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 226
    label "zdolno&#347;&#263;"
  ]
  node [
    id 227
    label "kszta&#322;t"
  ]
  node [
    id 228
    label "cz&#322;onek"
  ]
  node [
    id 229
    label "obiekt"
  ]
  node [
    id 230
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 231
    label "testify"
  ]
  node [
    id 232
    label "uzasadni&#263;"
  ]
  node [
    id 233
    label "zobo"
  ]
  node [
    id 234
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 235
    label "yakalo"
  ]
  node [
    id 236
    label "dzo"
  ]
  node [
    id 237
    label "niewygodny"
  ]
  node [
    id 238
    label "k&#322;opotliwie"
  ]
  node [
    id 239
    label "koso"
  ]
  node [
    id 240
    label "szuka&#263;"
  ]
  node [
    id 241
    label "go_steady"
  ]
  node [
    id 242
    label "dba&#263;"
  ]
  node [
    id 243
    label "traktowa&#263;"
  ]
  node [
    id 244
    label "os&#261;dza&#263;"
  ]
  node [
    id 245
    label "punkt_widzenia"
  ]
  node [
    id 246
    label "uwa&#380;a&#263;"
  ]
  node [
    id 247
    label "look"
  ]
  node [
    id 248
    label "pogl&#261;da&#263;"
  ]
  node [
    id 249
    label "przygl&#261;da&#263;_si&#281;"
  ]
  node [
    id 250
    label "klecha"
  ]
  node [
    id 251
    label "eklezjasta"
  ]
  node [
    id 252
    label "rozgrzeszanie"
  ]
  node [
    id 253
    label "duszpasterstwo"
  ]
  node [
    id 254
    label "rozgrzesza&#263;"
  ]
  node [
    id 255
    label "kap&#322;an"
  ]
  node [
    id 256
    label "ksi&#281;&#380;a"
  ]
  node [
    id 257
    label "duchowny"
  ]
  node [
    id 258
    label "kol&#281;da"
  ]
  node [
    id 259
    label "seminarzysta"
  ]
  node [
    id 260
    label "pasterz"
  ]
  node [
    id 261
    label "ochrona"
  ]
  node [
    id 262
    label "konstrukcja"
  ]
  node [
    id 263
    label "skrzynka"
  ]
  node [
    id 264
    label "check"
  ]
  node [
    id 265
    label "wz&#243;r"
  ]
  node [
    id 266
    label "barroom"
  ]
  node [
    id 267
    label "ilo&#347;&#263;"
  ]
  node [
    id 268
    label "poj&#281;cie"
  ]
  node [
    id 269
    label "papier_milimetrowy"
  ]
  node [
    id 270
    label "tkanina"
  ]
  node [
    id 271
    label "okratowanie"
  ]
  node [
    id 272
    label "bar"
  ]
  node [
    id 273
    label "&#347;wi&#261;tynia"
  ]
  node [
    id 274
    label "zakrystia"
  ]
  node [
    id 275
    label "organizacja_religijna"
  ]
  node [
    id 276
    label "nawa"
  ]
  node [
    id 277
    label "nerwica_eklezjogenna"
  ]
  node [
    id 278
    label "kropielnica"
  ]
  node [
    id 279
    label "prezbiterium"
  ]
  node [
    id 280
    label "wsp&#243;lnota"
  ]
  node [
    id 281
    label "church"
  ]
  node [
    id 282
    label "kruchta"
  ]
  node [
    id 283
    label "Ska&#322;ka"
  ]
  node [
    id 284
    label "kult"
  ]
  node [
    id 285
    label "ub&#322;agalnia"
  ]
  node [
    id 286
    label "dom"
  ]
  node [
    id 287
    label "przyt&#322;aczaj&#261;co"
  ]
  node [
    id 288
    label "&#378;le"
  ]
  node [
    id 289
    label "ci&#281;&#380;ko"
  ]
  node [
    id 290
    label "duszny"
  ]
  node [
    id 291
    label "serce"
  ]
  node [
    id 292
    label "zapowied&#378;"
  ]
  node [
    id 293
    label "preview"
  ]
  node [
    id 294
    label "b&#322;&#281;dnik_kostny"
  ]
  node [
    id 295
    label "pomieszczenie"
  ]
  node [
    id 296
    label "si&#281;ga&#263;"
  ]
  node [
    id 297
    label "obecno&#347;&#263;"
  ]
  node [
    id 298
    label "stan"
  ]
  node [
    id 299
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 300
    label "stand"
  ]
  node [
    id 301
    label "mie&#263;_miejsce"
  ]
  node [
    id 302
    label "uczestniczy&#263;"
  ]
  node [
    id 303
    label "chodzi&#263;"
  ]
  node [
    id 304
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 305
    label "equal"
  ]
  node [
    id 306
    label "spokojnie"
  ]
  node [
    id 307
    label "niesympatycznie"
  ]
  node [
    id 308
    label "ch&#322;odnie"
  ]
  node [
    id 309
    label "ch&#322;odny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 10
    target 105
  ]
  edge [
    source 10
    target 106
  ]
  edge [
    source 10
    target 107
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 20
  ]
  edge [
    source 11
    target 21
  ]
  edge [
    source 11
    target 29
  ]
  edge [
    source 11
    target 30
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 110
  ]
  edge [
    source 12
    target 111
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 112
  ]
  edge [
    source 14
    target 113
  ]
  edge [
    source 14
    target 114
  ]
  edge [
    source 14
    target 115
  ]
  edge [
    source 14
    target 116
  ]
  edge [
    source 14
    target 117
  ]
  edge [
    source 14
    target 118
  ]
  edge [
    source 14
    target 119
  ]
  edge [
    source 14
    target 120
  ]
  edge [
    source 14
    target 121
  ]
  edge [
    source 14
    target 122
  ]
  edge [
    source 14
    target 123
  ]
  edge [
    source 14
    target 124
  ]
  edge [
    source 14
    target 125
  ]
  edge [
    source 14
    target 126
  ]
  edge [
    source 14
    target 127
  ]
  edge [
    source 14
    target 128
  ]
  edge [
    source 14
    target 129
  ]
  edge [
    source 14
    target 130
  ]
  edge [
    source 14
    target 131
  ]
  edge [
    source 14
    target 132
  ]
  edge [
    source 14
    target 133
  ]
  edge [
    source 14
    target 134
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 27
  ]
  edge [
    source 15
    target 28
  ]
  edge [
    source 15
    target 135
  ]
  edge [
    source 15
    target 15
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 24
  ]
  edge [
    source 16
    target 25
  ]
  edge [
    source 16
    target 136
  ]
  edge [
    source 16
    target 137
  ]
  edge [
    source 16
    target 138
  ]
  edge [
    source 16
    target 139
  ]
  edge [
    source 16
    target 29
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 117
  ]
  edge [
    source 18
    target 140
  ]
  edge [
    source 18
    target 113
  ]
  edge [
    source 18
    target 141
  ]
  edge [
    source 18
    target 130
  ]
  edge [
    source 18
    target 142
  ]
  edge [
    source 18
    target 143
  ]
  edge [
    source 18
    target 144
  ]
  edge [
    source 18
    target 120
  ]
  edge [
    source 18
    target 145
  ]
  edge [
    source 18
    target 146
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 21
  ]
  edge [
    source 19
    target 22
  ]
  edge [
    source 19
    target 147
  ]
  edge [
    source 19
    target 148
  ]
  edge [
    source 19
    target 149
  ]
  edge [
    source 19
    target 137
  ]
  edge [
    source 19
    target 150
  ]
  edge [
    source 19
    target 151
  ]
  edge [
    source 19
    target 152
  ]
  edge [
    source 19
    target 153
  ]
  edge [
    source 19
    target 139
  ]
  edge [
    source 20
    target 154
  ]
  edge [
    source 20
    target 155
  ]
  edge [
    source 20
    target 156
  ]
  edge [
    source 20
    target 157
  ]
  edge [
    source 20
    target 158
  ]
  edge [
    source 20
    target 159
  ]
  edge [
    source 20
    target 160
  ]
  edge [
    source 20
    target 161
  ]
  edge [
    source 20
    target 162
  ]
  edge [
    source 20
    target 163
  ]
  edge [
    source 20
    target 164
  ]
  edge [
    source 20
    target 165
  ]
  edge [
    source 20
    target 109
  ]
  edge [
    source 21
    target 166
  ]
  edge [
    source 21
    target 36
  ]
  edge [
    source 21
    target 167
  ]
  edge [
    source 21
    target 168
  ]
  edge [
    source 21
    target 169
  ]
  edge [
    source 21
    target 170
  ]
  edge [
    source 21
    target 171
  ]
  edge [
    source 21
    target 172
  ]
  edge [
    source 21
    target 173
  ]
  edge [
    source 21
    target 174
  ]
  edge [
    source 21
    target 175
  ]
  edge [
    source 21
    target 38
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 176
  ]
  edge [
    source 22
    target 177
  ]
  edge [
    source 22
    target 178
  ]
  edge [
    source 22
    target 179
  ]
  edge [
    source 22
    target 180
  ]
  edge [
    source 22
    target 181
  ]
  edge [
    source 22
    target 182
  ]
  edge [
    source 22
    target 183
  ]
  edge [
    source 22
    target 184
  ]
  edge [
    source 22
    target 185
  ]
  edge [
    source 22
    target 186
  ]
  edge [
    source 22
    target 187
  ]
  edge [
    source 22
    target 188
  ]
  edge [
    source 22
    target 189
  ]
  edge [
    source 22
    target 33
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 190
  ]
  edge [
    source 23
    target 191
  ]
  edge [
    source 23
    target 192
  ]
  edge [
    source 23
    target 193
  ]
  edge [
    source 23
    target 194
  ]
  edge [
    source 24
    target 195
  ]
  edge [
    source 24
    target 196
  ]
  edge [
    source 24
    target 197
  ]
  edge [
    source 24
    target 198
  ]
  edge [
    source 24
    target 199
  ]
  edge [
    source 24
    target 200
  ]
  edge [
    source 24
    target 201
  ]
  edge [
    source 24
    target 202
  ]
  edge [
    source 24
    target 203
  ]
  edge [
    source 24
    target 204
  ]
  edge [
    source 24
    target 205
  ]
  edge [
    source 24
    target 206
  ]
  edge [
    source 24
    target 207
  ]
  edge [
    source 24
    target 208
  ]
  edge [
    source 24
    target 209
  ]
  edge [
    source 24
    target 210
  ]
  edge [
    source 24
    target 211
  ]
  edge [
    source 24
    target 212
  ]
  edge [
    source 24
    target 213
  ]
  edge [
    source 24
    target 214
  ]
  edge [
    source 24
    target 215
  ]
  edge [
    source 24
    target 216
  ]
  edge [
    source 24
    target 217
  ]
  edge [
    source 24
    target 218
  ]
  edge [
    source 24
    target 219
  ]
  edge [
    source 24
    target 220
  ]
  edge [
    source 24
    target 221
  ]
  edge [
    source 24
    target 222
  ]
  edge [
    source 24
    target 223
  ]
  edge [
    source 24
    target 224
  ]
  edge [
    source 24
    target 225
  ]
  edge [
    source 24
    target 226
  ]
  edge [
    source 24
    target 227
  ]
  edge [
    source 24
    target 228
  ]
  edge [
    source 24
    target 136
  ]
  edge [
    source 24
    target 229
  ]
  edge [
    source 24
    target 230
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 231
  ]
  edge [
    source 26
    target 232
  ]
  edge [
    source 27
    target 211
  ]
  edge [
    source 27
    target 233
  ]
  edge [
    source 27
    target 234
  ]
  edge [
    source 27
    target 235
  ]
  edge [
    source 27
    target 236
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 237
  ]
  edge [
    source 28
    target 238
  ]
  edge [
    source 29
    target 239
  ]
  edge [
    source 29
    target 240
  ]
  edge [
    source 29
    target 241
  ]
  edge [
    source 29
    target 242
  ]
  edge [
    source 29
    target 243
  ]
  edge [
    source 29
    target 244
  ]
  edge [
    source 29
    target 245
  ]
  edge [
    source 29
    target 105
  ]
  edge [
    source 29
    target 246
  ]
  edge [
    source 29
    target 247
  ]
  edge [
    source 29
    target 248
  ]
  edge [
    source 29
    target 249
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 250
  ]
  edge [
    source 30
    target 251
  ]
  edge [
    source 30
    target 252
  ]
  edge [
    source 30
    target 253
  ]
  edge [
    source 30
    target 254
  ]
  edge [
    source 30
    target 255
  ]
  edge [
    source 30
    target 256
  ]
  edge [
    source 30
    target 257
  ]
  edge [
    source 30
    target 258
  ]
  edge [
    source 30
    target 259
  ]
  edge [
    source 30
    target 260
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 261
  ]
  edge [
    source 32
    target 262
  ]
  edge [
    source 32
    target 263
  ]
  edge [
    source 32
    target 264
  ]
  edge [
    source 32
    target 265
  ]
  edge [
    source 32
    target 266
  ]
  edge [
    source 32
    target 267
  ]
  edge [
    source 32
    target 268
  ]
  edge [
    source 32
    target 269
  ]
  edge [
    source 32
    target 270
  ]
  edge [
    source 32
    target 271
  ]
  edge [
    source 32
    target 272
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 273
  ]
  edge [
    source 33
    target 274
  ]
  edge [
    source 33
    target 275
  ]
  edge [
    source 33
    target 276
  ]
  edge [
    source 33
    target 277
  ]
  edge [
    source 33
    target 278
  ]
  edge [
    source 33
    target 279
  ]
  edge [
    source 33
    target 280
  ]
  edge [
    source 33
    target 281
  ]
  edge [
    source 33
    target 282
  ]
  edge [
    source 33
    target 283
  ]
  edge [
    source 33
    target 284
  ]
  edge [
    source 33
    target 285
  ]
  edge [
    source 33
    target 286
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 287
  ]
  edge [
    source 34
    target 288
  ]
  edge [
    source 34
    target 289
  ]
  edge [
    source 34
    target 290
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 291
  ]
  edge [
    source 35
    target 292
  ]
  edge [
    source 35
    target 293
  ]
  edge [
    source 35
    target 294
  ]
  edge [
    source 35
    target 295
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 296
  ]
  edge [
    source 36
    target 85
  ]
  edge [
    source 36
    target 297
  ]
  edge [
    source 36
    target 298
  ]
  edge [
    source 36
    target 299
  ]
  edge [
    source 36
    target 300
  ]
  edge [
    source 36
    target 301
  ]
  edge [
    source 36
    target 302
  ]
  edge [
    source 36
    target 303
  ]
  edge [
    source 36
    target 304
  ]
  edge [
    source 36
    target 305
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 39
  ]
  edge [
    source 38
    target 306
  ]
  edge [
    source 38
    target 307
  ]
  edge [
    source 38
    target 308
  ]
  edge [
    source 38
    target 309
  ]
]
