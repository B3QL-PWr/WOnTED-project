graph [
  maxDegree 56
  minDegree 1
  meanDegree 2.628205128205128
  density 0.01695616211745244
  graphCliqueNumber 11
  node [
    id 0
    label "znany"
    origin "text"
  ]
  node [
    id 1
    label "brytyjski"
    origin "text"
  ]
  node [
    id 2
    label "serial"
    origin "text"
  ]
  node [
    id 3
    label "komediowy"
    origin "text"
  ]
  node [
    id 4
    label "allo"
    origin "text"
  ]
  node [
    id 5
    label "obraz"
    origin "text"
  ]
  node [
    id 6
    label "upad&#322;y"
    origin "text"
  ]
  node [
    id 7
    label "madonna"
    origin "text"
  ]
  node [
    id 8
    label "wielki"
    origin "text"
  ]
  node [
    id 9
    label "cyc"
    origin "text"
  ]
  node [
    id 10
    label "p&#281;dzel"
    origin "text"
  ]
  node [
    id 11
    label "fikcyjny"
    origin "text"
  ]
  node [
    id 12
    label "malarz"
    origin "text"
  ]
  node [
    id 13
    label "van"
    origin "text"
  ]
  node [
    id 14
    label "clompa"
    origin "text"
  ]
  node [
    id 15
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 16
    label "sprzeda&#263;"
    origin "text"
  ]
  node [
    id 17
    label "aukcja"
    origin "text"
  ]
  node [
    id 18
    label "tys"
    origin "text"
  ]
  node [
    id 19
    label "funt"
    origin "text"
  ]
  node [
    id 20
    label "informowa&#263;"
    origin "text"
  ]
  node [
    id 21
    label "bbc"
    origin "text"
  ]
  node [
    id 22
    label "rozpowszechnianie"
  ]
  node [
    id 23
    label "wyp&#322;yni&#281;cie"
  ]
  node [
    id 24
    label "angielsko"
  ]
  node [
    id 25
    label "po_brytyjsku"
  ]
  node [
    id 26
    label "europejski"
  ]
  node [
    id 27
    label "j&#281;zyk_celtycki"
  ]
  node [
    id 28
    label "j&#281;zyk_martwy"
  ]
  node [
    id 29
    label "j&#281;zyk_angielski"
  ]
  node [
    id 30
    label "zachodnioeuropejski"
  ]
  node [
    id 31
    label "morris"
  ]
  node [
    id 32
    label "angielski"
  ]
  node [
    id 33
    label "anglosaski"
  ]
  node [
    id 34
    label "brytyjsko"
  ]
  node [
    id 35
    label "seria"
  ]
  node [
    id 36
    label "Klan"
  ]
  node [
    id 37
    label "film"
  ]
  node [
    id 38
    label "Ranczo"
  ]
  node [
    id 39
    label "program_telewizyjny"
  ]
  node [
    id 40
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 41
    label "komediowo"
  ]
  node [
    id 42
    label "opinion"
  ]
  node [
    id 43
    label "effigy"
  ]
  node [
    id 44
    label "sztafa&#380;"
  ]
  node [
    id 45
    label "przeplot"
  ]
  node [
    id 46
    label "pulment"
  ]
  node [
    id 47
    label "rola"
  ]
  node [
    id 48
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 49
    label "representation"
  ]
  node [
    id 50
    label "pod&#322;o&#380;e_malarskie"
  ]
  node [
    id 51
    label "projekcja"
  ]
  node [
    id 52
    label "wytw&#243;r"
  ]
  node [
    id 53
    label "inning"
  ]
  node [
    id 54
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 55
    label "oprawianie"
  ]
  node [
    id 56
    label "oprawia&#263;"
  ]
  node [
    id 57
    label "zjawisko"
  ]
  node [
    id 58
    label "wypunktowa&#263;"
  ]
  node [
    id 59
    label "zaj&#347;cie"
  ]
  node [
    id 60
    label "t&#322;o"
  ]
  node [
    id 61
    label "punktowa&#263;"
  ]
  node [
    id 62
    label "widok"
  ]
  node [
    id 63
    label "plama_barwna"
  ]
  node [
    id 64
    label "scena"
  ]
  node [
    id 65
    label "czo&#322;&#243;wka"
  ]
  node [
    id 66
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 67
    label "human_body"
  ]
  node [
    id 68
    label "ty&#322;&#243;wka"
  ]
  node [
    id 69
    label "napisy"
  ]
  node [
    id 70
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 71
    label "postprodukcja"
  ]
  node [
    id 72
    label "przedstawienie"
  ]
  node [
    id 73
    label "podobrazie"
  ]
  node [
    id 74
    label "anamorfoza"
  ]
  node [
    id 75
    label "parkiet"
  ]
  node [
    id 76
    label "ziarno"
  ]
  node [
    id 77
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 78
    label "ostro&#347;&#263;"
  ]
  node [
    id 79
    label "persona"
  ]
  node [
    id 80
    label "zbi&#243;r"
  ]
  node [
    id 81
    label "pogl&#261;d"
  ]
  node [
    id 82
    label "filmoteka"
  ]
  node [
    id 83
    label "uj&#281;cie"
  ]
  node [
    id 84
    label "perspektywa"
  ]
  node [
    id 85
    label "picture"
  ]
  node [
    id 86
    label "dupny"
  ]
  node [
    id 87
    label "wysoce"
  ]
  node [
    id 88
    label "wyj&#261;tkowy"
  ]
  node [
    id 89
    label "wybitny"
  ]
  node [
    id 90
    label "znaczny"
  ]
  node [
    id 91
    label "prawdziwy"
  ]
  node [
    id 92
    label "wa&#380;ny"
  ]
  node [
    id 93
    label "nieprzeci&#281;tny"
  ]
  node [
    id 94
    label "biust"
  ]
  node [
    id 95
    label "tkanina"
  ]
  node [
    id 96
    label "pier&#347;"
  ]
  node [
    id 97
    label "bawe&#322;na"
  ]
  node [
    id 98
    label "skuwka"
  ]
  node [
    id 99
    label "narz&#281;dzie"
  ]
  node [
    id 100
    label "w&#322;osie"
  ]
  node [
    id 101
    label "fikcyjnie"
  ]
  node [
    id 102
    label "nieprawdziwy"
  ]
  node [
    id 103
    label "plastyk"
  ]
  node [
    id 104
    label "Rafael"
  ]
  node [
    id 105
    label "Witkiewicz"
  ]
  node [
    id 106
    label "Rembrandt"
  ]
  node [
    id 107
    label "Matejko"
  ]
  node [
    id 108
    label "Witkacy"
  ]
  node [
    id 109
    label "fachowiec"
  ]
  node [
    id 110
    label "Rubens"
  ]
  node [
    id 111
    label "rzemie&#347;lnik"
  ]
  node [
    id 112
    label "Grottger"
  ]
  node [
    id 113
    label "Schulz"
  ]
  node [
    id 114
    label "Caravaggio"
  ]
  node [
    id 115
    label "br"
  ]
  node [
    id 116
    label "czarodziej"
  ]
  node [
    id 117
    label "artysta"
  ]
  node [
    id 118
    label "Pablo_Ruiz_Picasso"
  ]
  node [
    id 119
    label "samoch&#243;d"
  ]
  node [
    id 120
    label "nadwozie"
  ]
  node [
    id 121
    label "proceed"
  ]
  node [
    id 122
    label "catch"
  ]
  node [
    id 123
    label "pozosta&#263;"
  ]
  node [
    id 124
    label "osta&#263;_si&#281;"
  ]
  node [
    id 125
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 126
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 127
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 128
    label "change"
  ]
  node [
    id 129
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 130
    label "zhandlowa&#263;"
  ]
  node [
    id 131
    label "odda&#263;"
  ]
  node [
    id 132
    label "zach&#281;ci&#263;"
  ]
  node [
    id 133
    label "give_birth"
  ]
  node [
    id 134
    label "zdradzi&#263;"
  ]
  node [
    id 135
    label "op&#281;dzi&#263;"
  ]
  node [
    id 136
    label "sell"
  ]
  node [
    id 137
    label "przetarg"
  ]
  node [
    id 138
    label "jednostka_avoirdupois"
  ]
  node [
    id 139
    label "pens_brytyjski"
  ]
  node [
    id 140
    label "Falklandy"
  ]
  node [
    id 141
    label "Wyspa_Man"
  ]
  node [
    id 142
    label "kamie&#324;"
  ]
  node [
    id 143
    label "Wyspa_&#346;wi&#281;tej_Heleny"
  ]
  node [
    id 144
    label "uncja"
  ]
  node [
    id 145
    label "Wielka_Brytania"
  ]
  node [
    id 146
    label "marka"
  ]
  node [
    id 147
    label "jednostka_monetarna"
  ]
  node [
    id 148
    label "cetnar"
  ]
  node [
    id 149
    label "Guernsey"
  ]
  node [
    id 150
    label "komunikowa&#263;"
  ]
  node [
    id 151
    label "powiada&#263;"
  ]
  node [
    id 152
    label "inform"
  ]
  node [
    id 153
    label "z"
  ]
  node [
    id 154
    label "Allo"
  ]
  node [
    id 155
    label "Clompa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 4
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 12
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 6
    target 9
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 153
  ]
  edge [
    source 7
    target 154
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 153
  ]
  edge [
    source 8
    target 154
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 153
  ]
  edge [
    source 9
    target 154
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 101
  ]
  edge [
    source 11
    target 102
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 12
    target 107
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 12
    target 109
  ]
  edge [
    source 12
    target 110
  ]
  edge [
    source 12
    target 111
  ]
  edge [
    source 12
    target 112
  ]
  edge [
    source 12
    target 113
  ]
  edge [
    source 12
    target 114
  ]
  edge [
    source 12
    target 115
  ]
  edge [
    source 12
    target 116
  ]
  edge [
    source 12
    target 117
  ]
  edge [
    source 12
    target 118
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 119
  ]
  edge [
    source 13
    target 120
  ]
  edge [
    source 13
    target 155
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 121
  ]
  edge [
    source 15
    target 122
  ]
  edge [
    source 15
    target 123
  ]
  edge [
    source 15
    target 124
  ]
  edge [
    source 15
    target 125
  ]
  edge [
    source 15
    target 126
  ]
  edge [
    source 15
    target 127
  ]
  edge [
    source 15
    target 128
  ]
  edge [
    source 15
    target 129
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 130
  ]
  edge [
    source 16
    target 131
  ]
  edge [
    source 16
    target 132
  ]
  edge [
    source 16
    target 133
  ]
  edge [
    source 16
    target 134
  ]
  edge [
    source 16
    target 135
  ]
  edge [
    source 16
    target 136
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 137
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 138
  ]
  edge [
    source 19
    target 139
  ]
  edge [
    source 19
    target 140
  ]
  edge [
    source 19
    target 141
  ]
  edge [
    source 19
    target 142
  ]
  edge [
    source 19
    target 143
  ]
  edge [
    source 19
    target 144
  ]
  edge [
    source 19
    target 145
  ]
  edge [
    source 19
    target 146
  ]
  edge [
    source 19
    target 147
  ]
  edge [
    source 19
    target 148
  ]
  edge [
    source 19
    target 149
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 150
  ]
  edge [
    source 20
    target 151
  ]
  edge [
    source 20
    target 152
  ]
  edge [
    source 153
    target 154
  ]
  edge [
    source 153
    target 153
  ]
  edge [
    source 154
    target 154
  ]
]
