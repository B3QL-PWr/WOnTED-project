graph [
  maxDegree 34
  minDegree 1
  meanDegree 2.079822616407982
  density 0.004621828036462183
  graphCliqueNumber 3
  node [
    id 0
    label "wroc&#322;aw"
    origin "text"
  ]
  node [
    id 1
    label "kwiecie&#324;"
    origin "text"
  ]
  node [
    id 2
    label "rocznik"
    origin "text"
  ]
  node [
    id 3
    label "si&#243;dma"
    origin "text"
  ]
  node [
    id 4
    label "rocznica"
    origin "text"
  ]
  node [
    id 5
    label "przyznanie"
    origin "text"
  ]
  node [
    id 6
    label "polska"
    origin "text"
  ]
  node [
    id 7
    label "ukraina"
    origin "text"
  ]
  node [
    id 8
    label "organizacja"
    origin "text"
  ]
  node [
    id 9
    label "euro"
    origin "text"
  ]
  node [
    id 10
    label "prezydent"
    origin "text"
  ]
  node [
    id 11
    label "rafa&#322;"
    origin "text"
  ]
  node [
    id 12
    label "dutkiewicz"
    origin "text"
  ]
  node [
    id 13
    label "wmurowa&#263;"
    origin "text"
  ]
  node [
    id 14
    label "kamie&#324;"
    origin "text"
  ]
  node [
    id 15
    label "w&#281;gielny"
    origin "text"
  ]
  node [
    id 16
    label "pod"
    origin "text"
  ]
  node [
    id 17
    label "budowa"
    origin "text"
  ]
  node [
    id 18
    label "nowy"
    origin "text"
  ]
  node [
    id 19
    label "lotnisko"
    origin "text"
  ]
  node [
    id 20
    label "mi&#281;dzynarodowy"
    origin "text"
  ]
  node [
    id 21
    label "uroczysto&#347;&#263;"
    origin "text"
  ]
  node [
    id 22
    label "czysto"
    origin "text"
  ]
  node [
    id 23
    label "symboliczny"
    origin "text"
  ]
  node [
    id 24
    label "chi&#324;ski"
    origin "text"
  ]
  node [
    id 25
    label "robotnica"
    origin "text"
  ]
  node [
    id 26
    label "dawny"
    origin "text"
  ]
  node [
    id 27
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 28
    label "pracowa&#263;"
    origin "text"
  ]
  node [
    id 29
    label "betonowy"
    origin "text"
  ]
  node [
    id 30
    label "s&#322;up"
    origin "text"
  ]
  node [
    id 31
    label "tora"
    origin "text"
  ]
  node [
    id 32
    label "kolej"
    origin "text"
  ]
  node [
    id 33
    label "magnetyczny"
    origin "text"
  ]
  node [
    id 34
    label "sta&#263;"
    origin "text"
  ]
  node [
    id 35
    label "metro"
    origin "text"
  ]
  node [
    id 36
    label "te&#380;"
    origin "text"
  ]
  node [
    id 37
    label "doprowadzi&#263;"
    origin "text"
  ]
  node [
    id 38
    label "nowa"
    origin "text"
  ]
  node [
    id 39
    label "pewno"
    origin "text"
  ]
  node [
    id 40
    label "by&#263;"
    origin "text"
  ]
  node [
    id 41
    label "argument"
    origin "text"
  ]
  node [
    id 42
    label "walc"
    origin "text"
  ]
  node [
    id 43
    label "lekki"
    origin "text"
  ]
  node [
    id 44
    label "atletyka"
    origin "text"
  ]
  node [
    id 45
    label "kongres"
    origin "text"
  ]
  node [
    id 46
    label "futurologiczny"
    origin "text"
  ]
  node [
    id 47
    label "my&#347;l"
    origin "text"
  ]
  node [
    id 48
    label "tym"
    origin "text"
  ]
  node [
    id 49
    label "ostatni"
    origin "text"
  ]
  node [
    id 50
    label "miejsce"
    origin "text"
  ]
  node [
    id 51
    label "galeriowiec"
    origin "text"
  ]
  node [
    id 52
    label "powstawa&#263;"
    origin "text"
  ]
  node [
    id 53
    label "najwy&#380;szy"
    origin "text"
  ]
  node [
    id 54
    label "budynek"
    origin "text"
  ]
  node [
    id 55
    label "europa"
    origin "text"
  ]
  node [
    id 56
    label "water"
    origin "text"
  ]
  node [
    id 57
    label "tower"
    origin "text"
  ]
  node [
    id 58
    label "miesi&#261;c"
  ]
  node [
    id 59
    label "formacja"
  ]
  node [
    id 60
    label "kronika"
  ]
  node [
    id 61
    label "czasopismo"
  ]
  node [
    id 62
    label "yearbook"
  ]
  node [
    id 63
    label "godzina"
  ]
  node [
    id 64
    label "termin"
  ]
  node [
    id 65
    label "obchody"
  ]
  node [
    id 66
    label "recognition"
  ]
  node [
    id 67
    label "danie"
  ]
  node [
    id 68
    label "stwierdzenie"
  ]
  node [
    id 69
    label "confession"
  ]
  node [
    id 70
    label "oznajmienie"
  ]
  node [
    id 71
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 72
    label "endecki"
  ]
  node [
    id 73
    label "komitet_koordynacyjny"
  ]
  node [
    id 74
    label "przybud&#243;wka"
  ]
  node [
    id 75
    label "ZOMO"
  ]
  node [
    id 76
    label "podmiot"
  ]
  node [
    id 77
    label "boj&#243;wka"
  ]
  node [
    id 78
    label "zesp&#243;&#322;"
  ]
  node [
    id 79
    label "organization"
  ]
  node [
    id 80
    label "TOPR"
  ]
  node [
    id 81
    label "jednostka_organizacyjna"
  ]
  node [
    id 82
    label "przedstawicielstwo"
  ]
  node [
    id 83
    label "Cepelia"
  ]
  node [
    id 84
    label "GOPR"
  ]
  node [
    id 85
    label "ZMP"
  ]
  node [
    id 86
    label "ZBoWiD"
  ]
  node [
    id 87
    label "struktura"
  ]
  node [
    id 88
    label "od&#322;am"
  ]
  node [
    id 89
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 90
    label "centrala"
  ]
  node [
    id 91
    label "Watykan"
  ]
  node [
    id 92
    label "Kosowo"
  ]
  node [
    id 93
    label "San_Marino"
  ]
  node [
    id 94
    label "cent"
  ]
  node [
    id 95
    label "Monako"
  ]
  node [
    id 96
    label "Andora"
  ]
  node [
    id 97
    label "Czarnog&#243;ra"
  ]
  node [
    id 98
    label "moneta"
  ]
  node [
    id 99
    label "jednostka_monetarna"
  ]
  node [
    id 100
    label "Jelcyn"
  ]
  node [
    id 101
    label "Roosevelt"
  ]
  node [
    id 102
    label "Clinton"
  ]
  node [
    id 103
    label "dostojnik"
  ]
  node [
    id 104
    label "Tito"
  ]
  node [
    id 105
    label "de_Gaulle"
  ]
  node [
    id 106
    label "Nixon"
  ]
  node [
    id 107
    label "gruba_ryba"
  ]
  node [
    id 108
    label "Wa&#322;&#281;sa"
  ]
  node [
    id 109
    label "Putin"
  ]
  node [
    id 110
    label "Gorbaczow"
  ]
  node [
    id 111
    label "Naser"
  ]
  node [
    id 112
    label "samorz&#261;dowiec"
  ]
  node [
    id 113
    label "Kemal"
  ]
  node [
    id 114
    label "g&#322;owa_pa&#324;stwa"
  ]
  node [
    id 115
    label "zwierzchnik"
  ]
  node [
    id 116
    label "Bierut"
  ]
  node [
    id 117
    label "wprawi&#263;"
  ]
  node [
    id 118
    label "skamienienie"
  ]
  node [
    id 119
    label "z&#322;&#243;g"
  ]
  node [
    id 120
    label "jednostka_avoirdupois"
  ]
  node [
    id 121
    label "tworzywo"
  ]
  node [
    id 122
    label "rock"
  ]
  node [
    id 123
    label "lapidarium"
  ]
  node [
    id 124
    label "minera&#322;_barwny"
  ]
  node [
    id 125
    label "Had&#380;ar"
  ]
  node [
    id 126
    label "autografia"
  ]
  node [
    id 127
    label "rekwizyt_do_gry"
  ]
  node [
    id 128
    label "osad"
  ]
  node [
    id 129
    label "funt"
  ]
  node [
    id 130
    label "mad&#380;ong"
  ]
  node [
    id 131
    label "oczko"
  ]
  node [
    id 132
    label "cube"
  ]
  node [
    id 133
    label "p&#322;ytka"
  ]
  node [
    id 134
    label "domino"
  ]
  node [
    id 135
    label "ci&#281;&#380;ar"
  ]
  node [
    id 136
    label "ska&#322;a"
  ]
  node [
    id 137
    label "kamienienie"
  ]
  node [
    id 138
    label "w&#281;glowy"
  ]
  node [
    id 139
    label "figura"
  ]
  node [
    id 140
    label "wjazd"
  ]
  node [
    id 141
    label "konstrukcja"
  ]
  node [
    id 142
    label "r&#243;w"
  ]
  node [
    id 143
    label "kreacja"
  ]
  node [
    id 144
    label "posesja"
  ]
  node [
    id 145
    label "cecha"
  ]
  node [
    id 146
    label "dokumentacja_powykonawcza"
  ]
  node [
    id 147
    label "organ"
  ]
  node [
    id 148
    label "mechanika"
  ]
  node [
    id 149
    label "zwierz&#281;"
  ]
  node [
    id 150
    label "miejsce_pracy"
  ]
  node [
    id 151
    label "praca"
  ]
  node [
    id 152
    label "constitution"
  ]
  node [
    id 153
    label "cz&#322;owiek"
  ]
  node [
    id 154
    label "nowotny"
  ]
  node [
    id 155
    label "drugi"
  ]
  node [
    id 156
    label "kolejny"
  ]
  node [
    id 157
    label "bie&#380;&#261;cy"
  ]
  node [
    id 158
    label "nowo"
  ]
  node [
    id 159
    label "narybek"
  ]
  node [
    id 160
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 161
    label "obcy"
  ]
  node [
    id 162
    label "hala"
  ]
  node [
    id 163
    label "droga_ko&#322;owania"
  ]
  node [
    id 164
    label "baza"
  ]
  node [
    id 165
    label "p&#322;yta_postojowa"
  ]
  node [
    id 166
    label "wie&#380;a_kontroli_lot&#243;w"
  ]
  node [
    id 167
    label "betonka"
  ]
  node [
    id 168
    label "budowla"
  ]
  node [
    id 169
    label "aerodrom"
  ]
  node [
    id 170
    label "pas_startowy"
  ]
  node [
    id 171
    label "terminal"
  ]
  node [
    id 172
    label "mi&#281;dzynarodowo"
  ]
  node [
    id 173
    label "umi&#281;dzynarodowienie"
  ]
  node [
    id 174
    label "umi&#281;dzynaradawianie"
  ]
  node [
    id 175
    label "przysi&#281;ga_wojskowa"
  ]
  node [
    id 176
    label "egzaltacja"
  ]
  node [
    id 177
    label "wydarzenie"
  ]
  node [
    id 178
    label "przyrzeczenie_harcerskie"
  ]
  node [
    id 179
    label "atmosfera"
  ]
  node [
    id 180
    label "patos"
  ]
  node [
    id 181
    label "porz&#261;dnie"
  ]
  node [
    id 182
    label "cleanly"
  ]
  node [
    id 183
    label "uczciwie"
  ]
  node [
    id 184
    label "prawdziwie"
  ]
  node [
    id 185
    label "ostro"
  ]
  node [
    id 186
    label "zdrowo"
  ]
  node [
    id 187
    label "bezpiecznie"
  ]
  node [
    id 188
    label "cnotliwie"
  ]
  node [
    id 189
    label "przezroczo"
  ]
  node [
    id 190
    label "ewidentnie"
  ]
  node [
    id 191
    label "transparently"
  ]
  node [
    id 192
    label "klarownie"
  ]
  node [
    id 193
    label "przezroczysty"
  ]
  node [
    id 194
    label "czysty"
  ]
  node [
    id 195
    label "bezchmurnie"
  ]
  node [
    id 196
    label "udanie"
  ]
  node [
    id 197
    label "ekologicznie"
  ]
  node [
    id 198
    label "kompletnie"
  ]
  node [
    id 199
    label "bezb&#322;&#281;dnie"
  ]
  node [
    id 200
    label "przyjemnie"
  ]
  node [
    id 201
    label "doskonale"
  ]
  node [
    id 202
    label "nieprawdziwy"
  ]
  node [
    id 203
    label "ma&#322;y"
  ]
  node [
    id 204
    label "symbolicznie"
  ]
  node [
    id 205
    label "kitajski"
  ]
  node [
    id 206
    label "j&#281;zyk"
  ]
  node [
    id 207
    label "kaczka_po_peki&#324;sku"
  ]
  node [
    id 208
    label "dziwaczny"
  ]
  node [
    id 209
    label "tandetny"
  ]
  node [
    id 210
    label "makroj&#281;zyk"
  ]
  node [
    id 211
    label "chi&#324;sko"
  ]
  node [
    id 212
    label "po_chi&#324;sku"
  ]
  node [
    id 213
    label "j&#281;zyk_chi&#324;sko-tybeta&#324;ski"
  ]
  node [
    id 214
    label "azjatycki"
  ]
  node [
    id 215
    label "lipny"
  ]
  node [
    id 216
    label "go"
  ]
  node [
    id 217
    label "niedrogi"
  ]
  node [
    id 218
    label "dalekowschodni"
  ]
  node [
    id 219
    label "owad"
  ]
  node [
    id 220
    label "samica"
  ]
  node [
    id 221
    label "przesz&#322;y"
  ]
  node [
    id 222
    label "dawno"
  ]
  node [
    id 223
    label "dawniej"
  ]
  node [
    id 224
    label "kombatant"
  ]
  node [
    id 225
    label "stary"
  ]
  node [
    id 226
    label "odleg&#322;y"
  ]
  node [
    id 227
    label "anachroniczny"
  ]
  node [
    id 228
    label "przestarza&#322;y"
  ]
  node [
    id 229
    label "od_dawna"
  ]
  node [
    id 230
    label "poprzedni"
  ]
  node [
    id 231
    label "d&#322;ugoletni"
  ]
  node [
    id 232
    label "wcze&#347;niejszy"
  ]
  node [
    id 233
    label "niegdysiejszy"
  ]
  node [
    id 234
    label "endeavor"
  ]
  node [
    id 235
    label "funkcjonowa&#263;"
  ]
  node [
    id 236
    label "dobiega&#263;_si&#281;"
  ]
  node [
    id 237
    label "mie&#263;_miejsce"
  ]
  node [
    id 238
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 239
    label "dzia&#322;a&#263;"
  ]
  node [
    id 240
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 241
    label "work"
  ]
  node [
    id 242
    label "bangla&#263;"
  ]
  node [
    id 243
    label "do"
  ]
  node [
    id 244
    label "maszyna"
  ]
  node [
    id 245
    label "tryb"
  ]
  node [
    id 246
    label "dziama&#263;"
  ]
  node [
    id 247
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 248
    label "podejmowa&#263;"
  ]
  node [
    id 249
    label "t&#281;py"
  ]
  node [
    id 250
    label "uparty"
  ]
  node [
    id 251
    label "oddany"
  ]
  node [
    id 252
    label "formacja_geologiczna"
  ]
  node [
    id 253
    label "oszustwo"
  ]
  node [
    id 254
    label "picket"
  ]
  node [
    id 255
    label "tarcza_herbowa"
  ]
  node [
    id 256
    label "przedmiot"
  ]
  node [
    id 257
    label "chmura"
  ]
  node [
    id 258
    label "wska&#378;nik"
  ]
  node [
    id 259
    label "heraldyka"
  ]
  node [
    id 260
    label "upright"
  ]
  node [
    id 261
    label "figura_heraldyczna"
  ]
  node [
    id 262
    label "plume"
  ]
  node [
    id 263
    label "osoba_fizyczna"
  ]
  node [
    id 264
    label "pas"
  ]
  node [
    id 265
    label "zw&#243;j"
  ]
  node [
    id 266
    label "Tora"
  ]
  node [
    id 267
    label "pojazd_kolejowy"
  ]
  node [
    id 268
    label "czas"
  ]
  node [
    id 269
    label "tor"
  ]
  node [
    id 270
    label "tender"
  ]
  node [
    id 271
    label "droga"
  ]
  node [
    id 272
    label "blokada"
  ]
  node [
    id 273
    label "wagon"
  ]
  node [
    id 274
    label "run"
  ]
  node [
    id 275
    label "cedu&#322;a"
  ]
  node [
    id 276
    label "kolejno&#347;&#263;"
  ]
  node [
    id 277
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 278
    label "skomunikowanie_si&#281;"
  ]
  node [
    id 279
    label "trakcja"
  ]
  node [
    id 280
    label "linia"
  ]
  node [
    id 281
    label "cug"
  ]
  node [
    id 282
    label "poci&#261;g"
  ]
  node [
    id 283
    label "pocz&#261;tek"
  ]
  node [
    id 284
    label "nast&#281;pstwo"
  ]
  node [
    id 285
    label "lokomotywa"
  ]
  node [
    id 286
    label "skomunikowa&#263;_si&#281;"
  ]
  node [
    id 287
    label "proces"
  ]
  node [
    id 288
    label "magnetycznie"
  ]
  node [
    id 289
    label "zniewalaj&#261;cy"
  ]
  node [
    id 290
    label "pozostawa&#263;"
  ]
  node [
    id 291
    label "trwa&#263;"
  ]
  node [
    id 292
    label "wystarcza&#263;"
  ]
  node [
    id 293
    label "obowi&#261;zywa&#263;"
  ]
  node [
    id 294
    label "czeka&#263;"
  ]
  node [
    id 295
    label "stand"
  ]
  node [
    id 296
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 297
    label "mieszka&#263;"
  ]
  node [
    id 298
    label "wystarczy&#263;"
  ]
  node [
    id 299
    label "sprawowa&#263;"
  ]
  node [
    id 300
    label "przebywa&#263;"
  ]
  node [
    id 301
    label "kosztowa&#263;"
  ]
  node [
    id 302
    label "undertaking"
  ]
  node [
    id 303
    label "wystawa&#263;"
  ]
  node [
    id 304
    label "base"
  ]
  node [
    id 305
    label "digest"
  ]
  node [
    id 306
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 307
    label "poprowadzi&#263;"
  ]
  node [
    id 308
    label "spowodowa&#263;"
  ]
  node [
    id 309
    label "pos&#322;a&#263;"
  ]
  node [
    id 310
    label "have_a_bun_in_the_oven"
  ]
  node [
    id 311
    label "wykona&#263;"
  ]
  node [
    id 312
    label "wzbudzi&#263;"
  ]
  node [
    id 313
    label "wprowadzi&#263;"
  ]
  node [
    id 314
    label "set"
  ]
  node [
    id 315
    label "take"
  ]
  node [
    id 316
    label "carry"
  ]
  node [
    id 317
    label "gwiazda"
  ]
  node [
    id 318
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 319
    label "si&#281;ga&#263;"
  ]
  node [
    id 320
    label "obecno&#347;&#263;"
  ]
  node [
    id 321
    label "stan"
  ]
  node [
    id 322
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 323
    label "uczestniczy&#263;"
  ]
  node [
    id 324
    label "chodzi&#263;"
  ]
  node [
    id 325
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 326
    label "equal"
  ]
  node [
    id 327
    label "s&#261;d"
  ]
  node [
    id 328
    label "argumentacja"
  ]
  node [
    id 329
    label "parametr"
  ]
  node [
    id 330
    label "rzecz"
  ]
  node [
    id 331
    label "dow&#243;d"
  ]
  node [
    id 332
    label "operand"
  ]
  node [
    id 333
    label "zmienna"
  ]
  node [
    id 334
    label "taniec_towarzyski"
  ]
  node [
    id 335
    label "melodia"
  ]
  node [
    id 336
    label "taniec"
  ]
  node [
    id 337
    label "utw&#243;r"
  ]
  node [
    id 338
    label "letki"
  ]
  node [
    id 339
    label "nieznacznie"
  ]
  node [
    id 340
    label "subtelny"
  ]
  node [
    id 341
    label "prosty"
  ]
  node [
    id 342
    label "piaszczysty"
  ]
  node [
    id 343
    label "przyswajalny"
  ]
  node [
    id 344
    label "dietetyczny"
  ]
  node [
    id 345
    label "g&#322;adko"
  ]
  node [
    id 346
    label "bezpieczny"
  ]
  node [
    id 347
    label "delikatny"
  ]
  node [
    id 348
    label "p&#322;ynny"
  ]
  node [
    id 349
    label "s&#322;aby"
  ]
  node [
    id 350
    label "przyjemny"
  ]
  node [
    id 351
    label "zr&#281;czny"
  ]
  node [
    id 352
    label "nierozwa&#380;ny"
  ]
  node [
    id 353
    label "snadny"
  ]
  node [
    id 354
    label "nieg&#322;&#281;boko"
  ]
  node [
    id 355
    label "&#322;atwo"
  ]
  node [
    id 356
    label "&#322;atwy"
  ]
  node [
    id 357
    label "polotny"
  ]
  node [
    id 358
    label "cienki"
  ]
  node [
    id 359
    label "beztroskliwy"
  ]
  node [
    id 360
    label "beztrosko"
  ]
  node [
    id 361
    label "lekko"
  ]
  node [
    id 362
    label "ubogi"
  ]
  node [
    id 363
    label "zgrabny"
  ]
  node [
    id 364
    label "przewiewny"
  ]
  node [
    id 365
    label "suchy"
  ]
  node [
    id 366
    label "lekkozbrojny"
  ]
  node [
    id 367
    label "delikatnie"
  ]
  node [
    id 368
    label "&#322;acny"
  ]
  node [
    id 369
    label "zwinnie"
  ]
  node [
    id 370
    label "sport"
  ]
  node [
    id 371
    label "kongres_wiede&#324;ski"
  ]
  node [
    id 372
    label "convention"
  ]
  node [
    id 373
    label "konferencja"
  ]
  node [
    id 374
    label "system"
  ]
  node [
    id 375
    label "wytw&#243;r"
  ]
  node [
    id 376
    label "istota"
  ]
  node [
    id 377
    label "thinking"
  ]
  node [
    id 378
    label "idea"
  ]
  node [
    id 379
    label "political_orientation"
  ]
  node [
    id 380
    label "pomys&#322;"
  ]
  node [
    id 381
    label "szko&#322;a"
  ]
  node [
    id 382
    label "umys&#322;"
  ]
  node [
    id 383
    label "fantomatyka"
  ]
  node [
    id 384
    label "t&#322;oczenie_si&#281;"
  ]
  node [
    id 385
    label "p&#322;&#243;d"
  ]
  node [
    id 386
    label "istota_&#380;ywa"
  ]
  node [
    id 387
    label "najgorszy"
  ]
  node [
    id 388
    label "aktualny"
  ]
  node [
    id 389
    label "ostatnio"
  ]
  node [
    id 390
    label "niedawno"
  ]
  node [
    id 391
    label "wi&#261;&#380;&#261;cy"
  ]
  node [
    id 392
    label "sko&#324;czony"
  ]
  node [
    id 393
    label "pozosta&#322;y"
  ]
  node [
    id 394
    label "w&#261;tpliwy"
  ]
  node [
    id 395
    label "cia&#322;o"
  ]
  node [
    id 396
    label "plac"
  ]
  node [
    id 397
    label "uwaga"
  ]
  node [
    id 398
    label "przestrze&#324;"
  ]
  node [
    id 399
    label "status"
  ]
  node [
    id 400
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 401
    label "chwila"
  ]
  node [
    id 402
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 403
    label "rz&#261;d"
  ]
  node [
    id 404
    label "location"
  ]
  node [
    id 405
    label "warunek_lokalowy"
  ]
  node [
    id 406
    label "blok"
  ]
  node [
    id 407
    label "buntowa&#263;_si&#281;"
  ]
  node [
    id 408
    label "rise"
  ]
  node [
    id 409
    label "spring"
  ]
  node [
    id 410
    label "stawa&#263;"
  ]
  node [
    id 411
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 412
    label "sprzeciwia&#263;_si&#281;"
  ]
  node [
    id 413
    label "plot"
  ]
  node [
    id 414
    label "podnosi&#263;_si&#281;"
  ]
  node [
    id 415
    label "uwalnia&#263;_si&#281;"
  ]
  node [
    id 416
    label "publish"
  ]
  node [
    id 417
    label "kondygnacja"
  ]
  node [
    id 418
    label "skrzyd&#322;o"
  ]
  node [
    id 419
    label "dach"
  ]
  node [
    id 420
    label "balkon"
  ]
  node [
    id 421
    label "klatka_schodowa"
  ]
  node [
    id 422
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 423
    label "pod&#322;oga"
  ]
  node [
    id 424
    label "front"
  ]
  node [
    id 425
    label "strop"
  ]
  node [
    id 426
    label "alkierz"
  ]
  node [
    id 427
    label "Pentagon"
  ]
  node [
    id 428
    label "przedpro&#380;e"
  ]
  node [
    id 429
    label "2012"
  ]
  node [
    id 430
    label "Rafa&#322;"
  ]
  node [
    id 431
    label "Dutkiewicz"
  ]
  node [
    id 432
    label "Water"
  ]
  node [
    id 433
    label "Tower"
  ]
  node [
    id 434
    label "biuro"
  ]
  node [
    id 435
    label "promocja"
  ]
  node [
    id 436
    label "Wroc&#322;aw"
  ]
  node [
    id 437
    label "pawe&#322;"
  ]
  node [
    id 438
    label "Romaszkan"
  ]
  node [
    id 439
    label "liga"
  ]
  node [
    id 440
    label "mistrz"
  ]
  node [
    id 441
    label "Final"
  ]
  node [
    id 442
    label "Four"
  ]
  node [
    id 443
    label "euroliga"
  ]
  node [
    id 444
    label "europejski"
  ]
  node [
    id 445
    label "instytut"
  ]
  node [
    id 446
    label "technologiczny"
  ]
  node [
    id 447
    label "stadion"
  ]
  node [
    id 448
    label "olimpijski"
  ]
  node [
    id 449
    label "Toyotaka"
  ]
  node [
    id 450
    label "Ota"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 9
    target 429
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 10
    target 105
  ]
  edge [
    source 10
    target 106
  ]
  edge [
    source 10
    target 107
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 10
    target 110
  ]
  edge [
    source 10
    target 111
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 117
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 118
  ]
  edge [
    source 14
    target 119
  ]
  edge [
    source 14
    target 120
  ]
  edge [
    source 14
    target 121
  ]
  edge [
    source 14
    target 122
  ]
  edge [
    source 14
    target 123
  ]
  edge [
    source 14
    target 124
  ]
  edge [
    source 14
    target 125
  ]
  edge [
    source 14
    target 126
  ]
  edge [
    source 14
    target 127
  ]
  edge [
    source 14
    target 128
  ]
  edge [
    source 14
    target 129
  ]
  edge [
    source 14
    target 130
  ]
  edge [
    source 14
    target 131
  ]
  edge [
    source 14
    target 132
  ]
  edge [
    source 14
    target 133
  ]
  edge [
    source 14
    target 134
  ]
  edge [
    source 14
    target 135
  ]
  edge [
    source 14
    target 136
  ]
  edge [
    source 14
    target 137
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 138
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 30
  ]
  edge [
    source 16
    target 31
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 139
  ]
  edge [
    source 17
    target 140
  ]
  edge [
    source 17
    target 87
  ]
  edge [
    source 17
    target 141
  ]
  edge [
    source 17
    target 142
  ]
  edge [
    source 17
    target 143
  ]
  edge [
    source 17
    target 144
  ]
  edge [
    source 17
    target 145
  ]
  edge [
    source 17
    target 146
  ]
  edge [
    source 17
    target 147
  ]
  edge [
    source 17
    target 148
  ]
  edge [
    source 17
    target 149
  ]
  edge [
    source 17
    target 150
  ]
  edge [
    source 17
    target 151
  ]
  edge [
    source 17
    target 152
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 153
  ]
  edge [
    source 18
    target 154
  ]
  edge [
    source 18
    target 155
  ]
  edge [
    source 18
    target 156
  ]
  edge [
    source 18
    target 157
  ]
  edge [
    source 18
    target 158
  ]
  edge [
    source 18
    target 159
  ]
  edge [
    source 18
    target 160
  ]
  edge [
    source 18
    target 161
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 38
  ]
  edge [
    source 19
    target 39
  ]
  edge [
    source 19
    target 162
  ]
  edge [
    source 19
    target 163
  ]
  edge [
    source 19
    target 164
  ]
  edge [
    source 19
    target 165
  ]
  edge [
    source 19
    target 166
  ]
  edge [
    source 19
    target 167
  ]
  edge [
    source 19
    target 168
  ]
  edge [
    source 19
    target 169
  ]
  edge [
    source 19
    target 170
  ]
  edge [
    source 19
    target 171
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 172
  ]
  edge [
    source 20
    target 173
  ]
  edge [
    source 20
    target 174
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 145
  ]
  edge [
    source 21
    target 175
  ]
  edge [
    source 21
    target 176
  ]
  edge [
    source 21
    target 177
  ]
  edge [
    source 21
    target 178
  ]
  edge [
    source 21
    target 179
  ]
  edge [
    source 21
    target 180
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 181
  ]
  edge [
    source 22
    target 182
  ]
  edge [
    source 22
    target 183
  ]
  edge [
    source 22
    target 184
  ]
  edge [
    source 22
    target 185
  ]
  edge [
    source 22
    target 186
  ]
  edge [
    source 22
    target 187
  ]
  edge [
    source 22
    target 188
  ]
  edge [
    source 22
    target 189
  ]
  edge [
    source 22
    target 190
  ]
  edge [
    source 22
    target 191
  ]
  edge [
    source 22
    target 192
  ]
  edge [
    source 22
    target 193
  ]
  edge [
    source 22
    target 194
  ]
  edge [
    source 22
    target 195
  ]
  edge [
    source 22
    target 196
  ]
  edge [
    source 22
    target 197
  ]
  edge [
    source 22
    target 198
  ]
  edge [
    source 22
    target 199
  ]
  edge [
    source 22
    target 200
  ]
  edge [
    source 22
    target 201
  ]
  edge [
    source 22
    target 53
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 202
  ]
  edge [
    source 23
    target 203
  ]
  edge [
    source 23
    target 204
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 205
  ]
  edge [
    source 24
    target 206
  ]
  edge [
    source 24
    target 207
  ]
  edge [
    source 24
    target 208
  ]
  edge [
    source 24
    target 209
  ]
  edge [
    source 24
    target 210
  ]
  edge [
    source 24
    target 211
  ]
  edge [
    source 24
    target 212
  ]
  edge [
    source 24
    target 213
  ]
  edge [
    source 24
    target 214
  ]
  edge [
    source 24
    target 215
  ]
  edge [
    source 24
    target 216
  ]
  edge [
    source 24
    target 217
  ]
  edge [
    source 24
    target 218
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 219
  ]
  edge [
    source 25
    target 220
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 221
  ]
  edge [
    source 26
    target 222
  ]
  edge [
    source 26
    target 223
  ]
  edge [
    source 26
    target 224
  ]
  edge [
    source 26
    target 225
  ]
  edge [
    source 26
    target 226
  ]
  edge [
    source 26
    target 227
  ]
  edge [
    source 26
    target 228
  ]
  edge [
    source 26
    target 229
  ]
  edge [
    source 26
    target 230
  ]
  edge [
    source 26
    target 231
  ]
  edge [
    source 26
    target 232
  ]
  edge [
    source 26
    target 233
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 33
  ]
  edge [
    source 27
    target 34
  ]
  edge [
    source 27
    target 36
  ]
  edge [
    source 27
    target 37
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 234
  ]
  edge [
    source 28
    target 235
  ]
  edge [
    source 28
    target 236
  ]
  edge [
    source 28
    target 237
  ]
  edge [
    source 28
    target 238
  ]
  edge [
    source 28
    target 239
  ]
  edge [
    source 28
    target 240
  ]
  edge [
    source 28
    target 241
  ]
  edge [
    source 28
    target 242
  ]
  edge [
    source 28
    target 243
  ]
  edge [
    source 28
    target 244
  ]
  edge [
    source 28
    target 245
  ]
  edge [
    source 28
    target 246
  ]
  edge [
    source 28
    target 247
  ]
  edge [
    source 28
    target 151
  ]
  edge [
    source 28
    target 248
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 249
  ]
  edge [
    source 29
    target 250
  ]
  edge [
    source 29
    target 251
  ]
  edge [
    source 30
    target 252
  ]
  edge [
    source 30
    target 253
  ]
  edge [
    source 30
    target 254
  ]
  edge [
    source 30
    target 255
  ]
  edge [
    source 30
    target 256
  ]
  edge [
    source 30
    target 257
  ]
  edge [
    source 30
    target 258
  ]
  edge [
    source 30
    target 259
  ]
  edge [
    source 30
    target 260
  ]
  edge [
    source 30
    target 261
  ]
  edge [
    source 30
    target 262
  ]
  edge [
    source 30
    target 263
  ]
  edge [
    source 30
    target 264
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 265
  ]
  edge [
    source 31
    target 266
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 267
  ]
  edge [
    source 32
    target 268
  ]
  edge [
    source 32
    target 269
  ]
  edge [
    source 32
    target 270
  ]
  edge [
    source 32
    target 271
  ]
  edge [
    source 32
    target 272
  ]
  edge [
    source 32
    target 273
  ]
  edge [
    source 32
    target 274
  ]
  edge [
    source 32
    target 275
  ]
  edge [
    source 32
    target 276
  ]
  edge [
    source 32
    target 277
  ]
  edge [
    source 32
    target 278
  ]
  edge [
    source 32
    target 279
  ]
  edge [
    source 32
    target 280
  ]
  edge [
    source 32
    target 281
  ]
  edge [
    source 32
    target 282
  ]
  edge [
    source 32
    target 283
  ]
  edge [
    source 32
    target 284
  ]
  edge [
    source 32
    target 285
  ]
  edge [
    source 32
    target 286
  ]
  edge [
    source 32
    target 287
  ]
  edge [
    source 33
    target 288
  ]
  edge [
    source 33
    target 289
  ]
  edge [
    source 33
    target 44
  ]
  edge [
    source 33
    target 50
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 290
  ]
  edge [
    source 34
    target 291
  ]
  edge [
    source 34
    target 40
  ]
  edge [
    source 34
    target 292
  ]
  edge [
    source 34
    target 293
  ]
  edge [
    source 34
    target 294
  ]
  edge [
    source 34
    target 295
  ]
  edge [
    source 34
    target 296
  ]
  edge [
    source 34
    target 297
  ]
  edge [
    source 34
    target 298
  ]
  edge [
    source 34
    target 299
  ]
  edge [
    source 34
    target 300
  ]
  edge [
    source 34
    target 301
  ]
  edge [
    source 34
    target 302
  ]
  edge [
    source 34
    target 303
  ]
  edge [
    source 34
    target 304
  ]
  edge [
    source 34
    target 305
  ]
  edge [
    source 34
    target 306
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 282
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 307
  ]
  edge [
    source 37
    target 308
  ]
  edge [
    source 37
    target 309
  ]
  edge [
    source 37
    target 310
  ]
  edge [
    source 37
    target 311
  ]
  edge [
    source 37
    target 312
  ]
  edge [
    source 37
    target 313
  ]
  edge [
    source 37
    target 314
  ]
  edge [
    source 37
    target 315
  ]
  edge [
    source 37
    target 316
  ]
  edge [
    source 38
    target 317
  ]
  edge [
    source 38
    target 318
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 319
  ]
  edge [
    source 40
    target 291
  ]
  edge [
    source 40
    target 320
  ]
  edge [
    source 40
    target 321
  ]
  edge [
    source 40
    target 322
  ]
  edge [
    source 40
    target 295
  ]
  edge [
    source 40
    target 237
  ]
  edge [
    source 40
    target 323
  ]
  edge [
    source 40
    target 324
  ]
  edge [
    source 40
    target 325
  ]
  edge [
    source 40
    target 326
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 327
  ]
  edge [
    source 41
    target 328
  ]
  edge [
    source 41
    target 329
  ]
  edge [
    source 41
    target 330
  ]
  edge [
    source 41
    target 331
  ]
  edge [
    source 41
    target 332
  ]
  edge [
    source 41
    target 333
  ]
  edge [
    source 42
    target 334
  ]
  edge [
    source 42
    target 335
  ]
  edge [
    source 42
    target 336
  ]
  edge [
    source 42
    target 337
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 338
  ]
  edge [
    source 43
    target 339
  ]
  edge [
    source 43
    target 340
  ]
  edge [
    source 43
    target 341
  ]
  edge [
    source 43
    target 342
  ]
  edge [
    source 43
    target 343
  ]
  edge [
    source 43
    target 344
  ]
  edge [
    source 43
    target 345
  ]
  edge [
    source 43
    target 346
  ]
  edge [
    source 43
    target 347
  ]
  edge [
    source 43
    target 348
  ]
  edge [
    source 43
    target 349
  ]
  edge [
    source 43
    target 350
  ]
  edge [
    source 43
    target 351
  ]
  edge [
    source 43
    target 352
  ]
  edge [
    source 43
    target 353
  ]
  edge [
    source 43
    target 354
  ]
  edge [
    source 43
    target 355
  ]
  edge [
    source 43
    target 356
  ]
  edge [
    source 43
    target 357
  ]
  edge [
    source 43
    target 358
  ]
  edge [
    source 43
    target 359
  ]
  edge [
    source 43
    target 360
  ]
  edge [
    source 43
    target 361
  ]
  edge [
    source 43
    target 362
  ]
  edge [
    source 43
    target 363
  ]
  edge [
    source 43
    target 364
  ]
  edge [
    source 43
    target 365
  ]
  edge [
    source 43
    target 366
  ]
  edge [
    source 43
    target 367
  ]
  edge [
    source 43
    target 368
  ]
  edge [
    source 43
    target 369
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 370
  ]
  edge [
    source 44
    target 50
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 371
  ]
  edge [
    source 45
    target 372
  ]
  edge [
    source 45
    target 373
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 374
  ]
  edge [
    source 47
    target 327
  ]
  edge [
    source 47
    target 375
  ]
  edge [
    source 47
    target 376
  ]
  edge [
    source 47
    target 377
  ]
  edge [
    source 47
    target 378
  ]
  edge [
    source 47
    target 379
  ]
  edge [
    source 47
    target 380
  ]
  edge [
    source 47
    target 381
  ]
  edge [
    source 47
    target 382
  ]
  edge [
    source 47
    target 383
  ]
  edge [
    source 47
    target 384
  ]
  edge [
    source 47
    target 385
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 153
  ]
  edge [
    source 49
    target 156
  ]
  edge [
    source 49
    target 386
  ]
  edge [
    source 49
    target 387
  ]
  edge [
    source 49
    target 388
  ]
  edge [
    source 49
    target 389
  ]
  edge [
    source 49
    target 390
  ]
  edge [
    source 49
    target 391
  ]
  edge [
    source 49
    target 392
  ]
  edge [
    source 49
    target 230
  ]
  edge [
    source 49
    target 393
  ]
  edge [
    source 49
    target 394
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 395
  ]
  edge [
    source 50
    target 396
  ]
  edge [
    source 50
    target 145
  ]
  edge [
    source 50
    target 397
  ]
  edge [
    source 50
    target 398
  ]
  edge [
    source 50
    target 399
  ]
  edge [
    source 50
    target 400
  ]
  edge [
    source 50
    target 401
  ]
  edge [
    source 50
    target 402
  ]
  edge [
    source 50
    target 403
  ]
  edge [
    source 50
    target 151
  ]
  edge [
    source 50
    target 404
  ]
  edge [
    source 50
    target 405
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 406
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 407
  ]
  edge [
    source 52
    target 408
  ]
  edge [
    source 52
    target 409
  ]
  edge [
    source 52
    target 410
  ]
  edge [
    source 52
    target 411
  ]
  edge [
    source 52
    target 412
  ]
  edge [
    source 52
    target 413
  ]
  edge [
    source 52
    target 414
  ]
  edge [
    source 52
    target 415
  ]
  edge [
    source 52
    target 416
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 417
  ]
  edge [
    source 54
    target 418
  ]
  edge [
    source 54
    target 419
  ]
  edge [
    source 54
    target 420
  ]
  edge [
    source 54
    target 421
  ]
  edge [
    source 54
    target 422
  ]
  edge [
    source 54
    target 423
  ]
  edge [
    source 54
    target 424
  ]
  edge [
    source 54
    target 425
  ]
  edge [
    source 54
    target 426
  ]
  edge [
    source 54
    target 168
  ]
  edge [
    source 54
    target 427
  ]
  edge [
    source 54
    target 428
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 430
    target 431
  ]
  edge [
    source 432
    target 433
  ]
  edge [
    source 434
    target 435
  ]
  edge [
    source 434
    target 436
  ]
  edge [
    source 435
    target 436
  ]
  edge [
    source 437
    target 438
  ]
  edge [
    source 439
    target 440
  ]
  edge [
    source 441
    target 442
  ]
  edge [
    source 441
    target 443
  ]
  edge [
    source 442
    target 443
  ]
  edge [
    source 444
    target 445
  ]
  edge [
    source 444
    target 446
  ]
  edge [
    source 445
    target 446
  ]
  edge [
    source 447
    target 448
  ]
  edge [
    source 449
    target 450
  ]
]
