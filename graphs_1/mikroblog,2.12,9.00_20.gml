graph [
  maxDegree 15
  minDegree 1
  meanDegree 2.0294117647058822
  density 0.03028972783143108
  graphCliqueNumber 2
  node [
    id 0
    label "jaki&#347;"
    origin "text"
  ]
  node [
    id 1
    label "najeba&#263;"
    origin "text"
  ]
  node [
    id 2
    label "janusz"
    origin "text"
  ]
  node [
    id 3
    label "obrzyga&#263;"
    origin "text"
  ]
  node [
    id 4
    label "materle"
    origin "text"
  ]
  node [
    id 5
    label "trybun"
    origin "text"
  ]
  node [
    id 6
    label "niez&#322;y"
    origin "text"
  ]
  node [
    id 7
    label "dym"
    origin "text"
  ]
  node [
    id 8
    label "si&#281;"
    origin "text"
  ]
  node [
    id 9
    label "zrobi&#263;"
    origin "text"
  ]
  node [
    id 10
    label "jako&#347;"
  ]
  node [
    id 11
    label "charakterystyczny"
  ]
  node [
    id 12
    label "ciekawy"
  ]
  node [
    id 13
    label "jako_tako"
  ]
  node [
    id 14
    label "dziwny"
  ]
  node [
    id 15
    label "przyzwoity"
  ]
  node [
    id 16
    label "Polak"
  ]
  node [
    id 17
    label "gra&#380;yna"
  ]
  node [
    id 18
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 19
    label "pata&#322;ach"
  ]
  node [
    id 20
    label "cwaniaczek"
  ]
  node [
    id 21
    label "kibic"
  ]
  node [
    id 22
    label "przeci&#281;tniak"
  ]
  node [
    id 23
    label "tatusiek"
  ]
  node [
    id 24
    label "facet"
  ]
  node [
    id 25
    label "wydali&#263;"
  ]
  node [
    id 26
    label "zabrudzi&#263;"
  ]
  node [
    id 27
    label "zwolennik"
  ]
  node [
    id 28
    label "chor&#261;&#380;y"
  ]
  node [
    id 29
    label "tuba"
  ]
  node [
    id 30
    label "przedstawiciel"
  ]
  node [
    id 31
    label "oficer"
  ]
  node [
    id 32
    label "legion"
  ]
  node [
    id 33
    label "urz&#281;dnik"
  ]
  node [
    id 34
    label "przyw&#243;dca"
  ]
  node [
    id 35
    label "dow&#243;dca"
  ]
  node [
    id 36
    label "rozsiewca"
  ]
  node [
    id 37
    label "popularyzator"
  ]
  node [
    id 38
    label "polityk"
  ]
  node [
    id 39
    label "korzystny"
  ]
  node [
    id 40
    label "dobrze"
  ]
  node [
    id 41
    label "nie&#378;le"
  ]
  node [
    id 42
    label "pozytywny"
  ]
  node [
    id 43
    label "intensywny"
  ]
  node [
    id 44
    label "spory"
  ]
  node [
    id 45
    label "udolny"
  ]
  node [
    id 46
    label "niczegowaty"
  ]
  node [
    id 47
    label "skuteczny"
  ]
  node [
    id 48
    label "&#347;mieszny"
  ]
  node [
    id 49
    label "nieszpetny"
  ]
  node [
    id 50
    label "mieszanina"
  ]
  node [
    id 51
    label "zamieszki"
  ]
  node [
    id 52
    label "gry&#378;&#263;"
  ]
  node [
    id 53
    label "gaz"
  ]
  node [
    id 54
    label "aggro"
  ]
  node [
    id 55
    label "zorganizowa&#263;"
  ]
  node [
    id 56
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 57
    label "przerobi&#263;"
  ]
  node [
    id 58
    label "wystylizowa&#263;"
  ]
  node [
    id 59
    label "cause"
  ]
  node [
    id 60
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 61
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 62
    label "post&#261;pi&#263;"
  ]
  node [
    id 63
    label "appoint"
  ]
  node [
    id 64
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 65
    label "nabra&#263;"
  ]
  node [
    id 66
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 67
    label "make"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 55
  ]
  edge [
    source 9
    target 56
  ]
  edge [
    source 9
    target 57
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 9
    target 25
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 9
    target 67
  ]
]
