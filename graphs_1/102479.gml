graph [
  maxDegree 45
  minDegree 1
  meanDegree 2.188436830835118
  density 0.004696216375182656
  graphCliqueNumber 3
  node [
    id 0
    label "uda&#263;"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "rozstrzygn&#261;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "przetarg"
    origin "text"
  ]
  node [
    id 4
    label "budowa"
    origin "text"
  ]
  node [
    id 5
    label "kilometrowy"
    origin "text"
  ]
  node [
    id 6
    label "droga"
    origin "text"
  ]
  node [
    id 7
    label "solcy"
    origin "text"
  ]
  node [
    id 8
    label "ma&#322;a"
    origin "text"
  ]
  node [
    id 9
    label "gmin"
    origin "text"
  ]
  node [
    id 10
    label "ozorek"
    origin "text"
  ]
  node [
    id 11
    label "walka"
    origin "text"
  ]
  node [
    id 12
    label "wykonawstwo"
    origin "text"
  ]
  node [
    id 13
    label "ten"
    origin "text"
  ]
  node [
    id 14
    label "inwestycja"
    origin "text"
  ]
  node [
    id 15
    label "stan&#261;&#263;"
    origin "text"
  ]
  node [
    id 16
    label "trzy"
    origin "text"
  ]
  node [
    id 17
    label "firma"
    origin "text"
  ]
  node [
    id 18
    label "niestety"
    origin "text"
  ]
  node [
    id 19
    label "jeden"
    origin "text"
  ]
  node [
    id 20
    label "z&#322;o&#380;y&#263;"
    origin "text"
  ]
  node [
    id 21
    label "niekompletny"
    origin "text"
  ]
  node [
    id 22
    label "oferta"
    origin "text"
  ]
  node [
    id 23
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 24
    label "by&#263;"
    origin "text"
  ]
  node [
    id 25
    label "bra&#263;"
    origin "text"
  ]
  node [
    id 26
    label "pod"
    origin "text"
  ]
  node [
    id 27
    label "uwaga"
    origin "text"
  ]
  node [
    id 28
    label "przy"
    origin "text"
  ]
  node [
    id 29
    label "rozstrzygni&#281;cie"
    origin "text"
  ]
  node [
    id 30
    label "plac"
    origin "text"
  ]
  node [
    id 31
    label "boj"
    origin "text"
  ]
  node [
    id 32
    label "pozosta&#263;"
    origin "text"
  ]
  node [
    id 33
    label "dwa"
    origin "text"
  ]
  node [
    id 34
    label "oferent"
    origin "text"
  ]
  node [
    id 35
    label "ma&#322;o"
    origin "text"
  ]
  node [
    id 36
    label "odby&#263;"
    origin "text"
  ]
  node [
    id 37
    label "dlatego"
    origin "text"
  ]
  node [
    id 38
    label "dofinansowa&#263;"
    origin "text"
  ]
  node [
    id 39
    label "&#347;rodek"
    origin "text"
  ]
  node [
    id 40
    label "fundusz"
    origin "text"
  ]
  node [
    id 41
    label "sapard"
    origin "text"
  ]
  node [
    id 42
    label "wyja&#347;nia&#263;"
    origin "text"
  ]
  node [
    id 43
    label "w&#243;jt"
    origin "text"
  ]
  node [
    id 44
    label "ozorkowy"
    origin "text"
  ]
  node [
    id 45
    label "w&#322;adys&#322;aw"
    origin "text"
  ]
  node [
    id 46
    label "sobolewski"
    origin "text"
  ]
  node [
    id 47
    label "procent"
    origin "text"
  ]
  node [
    id 48
    label "otrzyma&#263;"
    origin "text"
  ]
  node [
    id 49
    label "unia"
    origin "text"
  ]
  node [
    id 50
    label "europejski"
    origin "text"
  ]
  node [
    id 51
    label "reszta"
    origin "text"
  ]
  node [
    id 52
    label "sfinansowa&#263;"
    origin "text"
  ]
  node [
    id 53
    label "bud&#380;et"
    origin "text"
  ]
  node [
    id 54
    label "udzia&#322;"
    origin "text"
  ]
  node [
    id 55
    label "powodowa&#263;"
    origin "text"
  ]
  node [
    id 56
    label "procedura"
    origin "text"
  ]
  node [
    id 57
    label "przetargowy"
    origin "text"
  ]
  node [
    id 58
    label "skomplikowany"
    origin "text"
  ]
  node [
    id 59
    label "chc&#261;cy"
    origin "text"
  ]
  node [
    id 60
    label "wzi&#261;&#263;"
    origin "text"
  ]
  node [
    id 61
    label "musza"
    origin "text"
  ]
  node [
    id 62
    label "niezliczony"
    origin "text"
  ]
  node [
    id 63
    label "dokument"
    origin "text"
  ]
  node [
    id 64
    label "za&#347;wiadczenie"
    origin "text"
  ]
  node [
    id 65
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 66
    label "represent"
  ]
  node [
    id 67
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 68
    label "decide"
  ]
  node [
    id 69
    label "determine"
  ]
  node [
    id 70
    label "zdecydowa&#263;"
  ]
  node [
    id 71
    label "konkurs"
  ]
  node [
    id 72
    label "licytacja"
  ]
  node [
    id 73
    label "przybitka"
  ]
  node [
    id 74
    label "auction"
  ]
  node [
    id 75
    label "sprzeda&#380;"
  ]
  node [
    id 76
    label "sale"
  ]
  node [
    id 77
    label "sp&#243;r"
  ]
  node [
    id 78
    label "figura"
  ]
  node [
    id 79
    label "wjazd"
  ]
  node [
    id 80
    label "struktura"
  ]
  node [
    id 81
    label "konstrukcja"
  ]
  node [
    id 82
    label "r&#243;w"
  ]
  node [
    id 83
    label "kreacja"
  ]
  node [
    id 84
    label "posesja"
  ]
  node [
    id 85
    label "cecha"
  ]
  node [
    id 86
    label "dokumentacja_powykonawcza"
  ]
  node [
    id 87
    label "organ"
  ]
  node [
    id 88
    label "mechanika"
  ]
  node [
    id 89
    label "zwierz&#281;"
  ]
  node [
    id 90
    label "miejsce_pracy"
  ]
  node [
    id 91
    label "praca"
  ]
  node [
    id 92
    label "constitution"
  ]
  node [
    id 93
    label "d&#322;uga&#347;ny"
  ]
  node [
    id 94
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 95
    label "journey"
  ]
  node [
    id 96
    label "podbieg"
  ]
  node [
    id 97
    label "bezsilnikowy"
  ]
  node [
    id 98
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 99
    label "wylot"
  ]
  node [
    id 100
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 101
    label "drogowskaz"
  ]
  node [
    id 102
    label "nawierzchnia"
  ]
  node [
    id 103
    label "turystyka"
  ]
  node [
    id 104
    label "budowla"
  ]
  node [
    id 105
    label "spos&#243;b"
  ]
  node [
    id 106
    label "passage"
  ]
  node [
    id 107
    label "marszrutyzacja"
  ]
  node [
    id 108
    label "zbior&#243;wka"
  ]
  node [
    id 109
    label "ekskursja"
  ]
  node [
    id 110
    label "rajza"
  ]
  node [
    id 111
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 112
    label "ruch"
  ]
  node [
    id 113
    label "trasa"
  ]
  node [
    id 114
    label "wyb&#243;j"
  ]
  node [
    id 115
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 116
    label "ekwipunek"
  ]
  node [
    id 117
    label "korona_drogi"
  ]
  node [
    id 118
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 119
    label "pobocze"
  ]
  node [
    id 120
    label "dziewczynka"
  ]
  node [
    id 121
    label "dziewczyna"
  ]
  node [
    id 122
    label "stan_trzeci"
  ]
  node [
    id 123
    label "stan"
  ]
  node [
    id 124
    label "gminno&#347;&#263;"
  ]
  node [
    id 125
    label "pieczarkowiec"
  ]
  node [
    id 126
    label "ozorkowate"
  ]
  node [
    id 127
    label "saprotrof"
  ]
  node [
    id 128
    label "grzyb"
  ]
  node [
    id 129
    label "paso&#380;yt"
  ]
  node [
    id 130
    label "podroby"
  ]
  node [
    id 131
    label "czyn"
  ]
  node [
    id 132
    label "trudno&#347;&#263;"
  ]
  node [
    id 133
    label "obrona"
  ]
  node [
    id 134
    label "zaatakowanie"
  ]
  node [
    id 135
    label "konfrontacyjny"
  ]
  node [
    id 136
    label "military_action"
  ]
  node [
    id 137
    label "wrestle"
  ]
  node [
    id 138
    label "action"
  ]
  node [
    id 139
    label "wydarzenie"
  ]
  node [
    id 140
    label "rywalizacja"
  ]
  node [
    id 141
    label "sambo"
  ]
  node [
    id 142
    label "contest"
  ]
  node [
    id 143
    label "wyst&#281;p"
  ]
  node [
    id 144
    label "performance"
  ]
  node [
    id 145
    label "realizacja"
  ]
  node [
    id 146
    label "okre&#347;lony"
  ]
  node [
    id 147
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 148
    label "inwestycje"
  ]
  node [
    id 149
    label "wydatki_maj&#261;tkowe"
  ]
  node [
    id 150
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 151
    label "wk&#322;ad"
  ]
  node [
    id 152
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 153
    label "kapita&#322;"
  ]
  node [
    id 154
    label "przedsi&#281;wzi&#281;cie"
  ]
  node [
    id 155
    label "inwestowanie"
  ]
  node [
    id 156
    label "bud&#380;et_domowy"
  ]
  node [
    id 157
    label "sentyment_inwestycyjny"
  ]
  node [
    id 158
    label "rezultat"
  ]
  node [
    id 159
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 160
    label "obj&#261;&#263;"
  ]
  node [
    id 161
    label "reserve"
  ]
  node [
    id 162
    label "poruszy&#263;_si&#281;"
  ]
  node [
    id 163
    label "zosta&#263;"
  ]
  node [
    id 164
    label "originate"
  ]
  node [
    id 165
    label "zatrzyma&#263;_si&#281;"
  ]
  node [
    id 166
    label "przyj&#261;&#263;"
  ]
  node [
    id 167
    label "wystarczy&#263;"
  ]
  node [
    id 168
    label "przesta&#263;"
  ]
  node [
    id 169
    label "unie&#347;&#263;_si&#281;"
  ]
  node [
    id 170
    label "zmieni&#263;"
  ]
  node [
    id 171
    label "przyby&#263;"
  ]
  node [
    id 172
    label "cz&#322;owiek"
  ]
  node [
    id 173
    label "Hortex"
  ]
  node [
    id 174
    label "MAC"
  ]
  node [
    id 175
    label "reengineering"
  ]
  node [
    id 176
    label "nazwa_w&#322;asna"
  ]
  node [
    id 177
    label "podmiot_gospodarczy"
  ]
  node [
    id 178
    label "Google"
  ]
  node [
    id 179
    label "zaufanie"
  ]
  node [
    id 180
    label "biurowiec"
  ]
  node [
    id 181
    label "networking"
  ]
  node [
    id 182
    label "zasoby_ludzkie"
  ]
  node [
    id 183
    label "interes"
  ]
  node [
    id 184
    label "paczkarnia"
  ]
  node [
    id 185
    label "Canon"
  ]
  node [
    id 186
    label "HP"
  ]
  node [
    id 187
    label "Baltona"
  ]
  node [
    id 188
    label "Pewex"
  ]
  node [
    id 189
    label "MAN_SE"
  ]
  node [
    id 190
    label "Apeks"
  ]
  node [
    id 191
    label "zasoby"
  ]
  node [
    id 192
    label "Orbis"
  ]
  node [
    id 193
    label "siedziba"
  ]
  node [
    id 194
    label "Spo&#322;em"
  ]
  node [
    id 195
    label "przedsi&#281;biorczo&#347;&#263;"
  ]
  node [
    id 196
    label "Orlen"
  ]
  node [
    id 197
    label "klasa"
  ]
  node [
    id 198
    label "kieliszek"
  ]
  node [
    id 199
    label "shot"
  ]
  node [
    id 200
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 201
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 202
    label "jaki&#347;"
  ]
  node [
    id 203
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 204
    label "jednolicie"
  ]
  node [
    id 205
    label "w&#243;dka"
  ]
  node [
    id 206
    label "ujednolicenie"
  ]
  node [
    id 207
    label "jednakowy"
  ]
  node [
    id 208
    label "zestaw"
  ]
  node [
    id 209
    label "scali&#263;"
  ]
  node [
    id 210
    label "przywr&#243;ci&#263;"
  ]
  node [
    id 211
    label "give"
  ]
  node [
    id 212
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 213
    label "note"
  ]
  node [
    id 214
    label "set"
  ]
  node [
    id 215
    label "da&#263;"
  ]
  node [
    id 216
    label "marshal"
  ]
  node [
    id 217
    label "opracowa&#263;"
  ]
  node [
    id 218
    label "przekaza&#263;"
  ]
  node [
    id 219
    label "pay"
  ]
  node [
    id 220
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 221
    label "odda&#263;"
  ]
  node [
    id 222
    label "przy&#322;o&#380;y&#263;"
  ]
  node [
    id 223
    label "jell"
  ]
  node [
    id 224
    label "spowodowa&#263;"
  ]
  node [
    id 225
    label "frame"
  ]
  node [
    id 226
    label "u&#322;o&#380;y&#263;"
  ]
  node [
    id 227
    label "zebra&#263;"
  ]
  node [
    id 228
    label "fold"
  ]
  node [
    id 229
    label "nieca&#322;y"
  ]
  node [
    id 230
    label "propozycja"
  ]
  node [
    id 231
    label "offer"
  ]
  node [
    id 232
    label "uprawi&#263;"
  ]
  node [
    id 233
    label "gotowy"
  ]
  node [
    id 234
    label "might"
  ]
  node [
    id 235
    label "si&#281;ga&#263;"
  ]
  node [
    id 236
    label "trwa&#263;"
  ]
  node [
    id 237
    label "obecno&#347;&#263;"
  ]
  node [
    id 238
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 239
    label "stand"
  ]
  node [
    id 240
    label "mie&#263;_miejsce"
  ]
  node [
    id 241
    label "uczestniczy&#263;"
  ]
  node [
    id 242
    label "chodzi&#263;"
  ]
  node [
    id 243
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 244
    label "equal"
  ]
  node [
    id 245
    label "get"
  ]
  node [
    id 246
    label "przewa&#380;a&#263;"
  ]
  node [
    id 247
    label "zobowi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 248
    label "poczytywa&#263;"
  ]
  node [
    id 249
    label "levy"
  ]
  node [
    id 250
    label "pokonywa&#263;"
  ]
  node [
    id 251
    label "u&#380;ywa&#263;"
  ]
  node [
    id 252
    label "rusza&#263;"
  ]
  node [
    id 253
    label "zalicza&#263;"
  ]
  node [
    id 254
    label "wygrywa&#263;"
  ]
  node [
    id 255
    label "open"
  ]
  node [
    id 256
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 257
    label "branie"
  ]
  node [
    id 258
    label "korzysta&#263;"
  ]
  node [
    id 259
    label "&#263;pa&#263;"
  ]
  node [
    id 260
    label "wch&#322;ania&#263;"
  ]
  node [
    id 261
    label "interpretowa&#263;"
  ]
  node [
    id 262
    label "atakowa&#263;"
  ]
  node [
    id 263
    label "prowadzi&#263;"
  ]
  node [
    id 264
    label "rucha&#263;"
  ]
  node [
    id 265
    label "take"
  ]
  node [
    id 266
    label "dostawa&#263;"
  ]
  node [
    id 267
    label "wk&#322;ada&#263;"
  ]
  node [
    id 268
    label "chwyta&#263;"
  ]
  node [
    id 269
    label "arise"
  ]
  node [
    id 270
    label "za&#380;ywa&#263;"
  ]
  node [
    id 271
    label "uprawia&#263;_seks"
  ]
  node [
    id 272
    label "porywa&#263;"
  ]
  node [
    id 273
    label "robi&#263;"
  ]
  node [
    id 274
    label "grza&#263;"
  ]
  node [
    id 275
    label "abstract"
  ]
  node [
    id 276
    label "zaopatrywa&#263;_si&#281;"
  ]
  node [
    id 277
    label "wzmaga&#263;_si&#281;"
  ]
  node [
    id 278
    label "towarzystwo"
  ]
  node [
    id 279
    label "otrzymywa&#263;"
  ]
  node [
    id 280
    label "przyjmowa&#263;"
  ]
  node [
    id 281
    label "wchodzi&#263;"
  ]
  node [
    id 282
    label "ucieka&#263;"
  ]
  node [
    id 283
    label "odurza&#263;_si&#281;"
  ]
  node [
    id 284
    label "buzowa&#263;_si&#281;"
  ]
  node [
    id 285
    label "&#322;apa&#263;"
  ]
  node [
    id 286
    label "raise"
  ]
  node [
    id 287
    label "nagana"
  ]
  node [
    id 288
    label "wypowied&#378;"
  ]
  node [
    id 289
    label "dzienniczek"
  ]
  node [
    id 290
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 291
    label "wzgl&#261;d"
  ]
  node [
    id 292
    label "gossip"
  ]
  node [
    id 293
    label "upomnienie"
  ]
  node [
    id 294
    label "tekst"
  ]
  node [
    id 295
    label "zdecydowanie"
  ]
  node [
    id 296
    label "adjudication"
  ]
  node [
    id 297
    label "oddzia&#322;anie"
  ]
  node [
    id 298
    label "resoluteness"
  ]
  node [
    id 299
    label "decyzja"
  ]
  node [
    id 300
    label "rozstrzygni&#281;cie_si&#281;"
  ]
  node [
    id 301
    label "stoisko"
  ]
  node [
    id 302
    label "Majdan"
  ]
  node [
    id 303
    label "miejsce"
  ]
  node [
    id 304
    label "obszar"
  ]
  node [
    id 305
    label "kram"
  ]
  node [
    id 306
    label "pierzeja"
  ]
  node [
    id 307
    label "przestrze&#324;"
  ]
  node [
    id 308
    label "obiekt_handlowy"
  ]
  node [
    id 309
    label "targowica"
  ]
  node [
    id 310
    label "zgromadzenie"
  ]
  node [
    id 311
    label "miasto"
  ]
  node [
    id 312
    label "pole_bitwy"
  ]
  node [
    id 313
    label "&#321;ubianka"
  ]
  node [
    id 314
    label "area"
  ]
  node [
    id 315
    label "l&#281;k"
  ]
  node [
    id 316
    label "utrzyma&#263;_si&#281;"
  ]
  node [
    id 317
    label "proceed"
  ]
  node [
    id 318
    label "catch"
  ]
  node [
    id 319
    label "osta&#263;_si&#281;"
  ]
  node [
    id 320
    label "support"
  ]
  node [
    id 321
    label "prze&#380;y&#263;"
  ]
  node [
    id 322
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 323
    label "kandydat"
  ]
  node [
    id 324
    label "podmiot"
  ]
  node [
    id 325
    label "mo&#380;liwie"
  ]
  node [
    id 326
    label "nieznaczny"
  ]
  node [
    id 327
    label "kr&#243;tko"
  ]
  node [
    id 328
    label "nieistotnie"
  ]
  node [
    id 329
    label "nieliczny"
  ]
  node [
    id 330
    label "mikroskopijnie"
  ]
  node [
    id 331
    label "pomiernie"
  ]
  node [
    id 332
    label "ma&#322;y"
  ]
  node [
    id 333
    label "przej&#347;&#263;"
  ]
  node [
    id 334
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 335
    label "dop&#322;aci&#263;"
  ]
  node [
    id 336
    label "czas"
  ]
  node [
    id 337
    label "abstrakcja"
  ]
  node [
    id 338
    label "punkt"
  ]
  node [
    id 339
    label "substancja"
  ]
  node [
    id 340
    label "chemikalia"
  ]
  node [
    id 341
    label "uruchomienie"
  ]
  node [
    id 342
    label "nap&#322;yn&#261;&#263;"
  ]
  node [
    id 343
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 344
    label "supernadz&#243;r"
  ]
  node [
    id 345
    label "absolutorium"
  ]
  node [
    id 346
    label "nap&#322;yni&#281;cie"
  ]
  node [
    id 347
    label "podupada&#263;"
  ]
  node [
    id 348
    label "nap&#322;ywanie"
  ]
  node [
    id 349
    label "podupadanie"
  ]
  node [
    id 350
    label "kwestor"
  ]
  node [
    id 351
    label "uruchamia&#263;"
  ]
  node [
    id 352
    label "mienie"
  ]
  node [
    id 353
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 354
    label "uruchamianie"
  ]
  node [
    id 355
    label "instytucja"
  ]
  node [
    id 356
    label "czynnik_produkcji"
  ]
  node [
    id 357
    label "poja&#347;nia&#263;"
  ]
  node [
    id 358
    label "elaborate"
  ]
  node [
    id 359
    label "explain"
  ]
  node [
    id 360
    label "suplikowa&#263;"
  ]
  node [
    id 361
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 362
    label "przedstawia&#263;"
  ]
  node [
    id 363
    label "gmina"
  ]
  node [
    id 364
    label "samorz&#261;dowiec"
  ]
  node [
    id 365
    label "mi&#281;sny"
  ]
  node [
    id 366
    label "promil"
  ]
  node [
    id 367
    label "doch&#243;d"
  ]
  node [
    id 368
    label "baza_odsetkowa"
  ]
  node [
    id 369
    label "kredyt"
  ]
  node [
    id 370
    label "u&#322;amek"
  ]
  node [
    id 371
    label "ilo&#347;&#263;"
  ]
  node [
    id 372
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 373
    label "symmetry"
  ]
  node [
    id 374
    label "wytworzy&#263;"
  ]
  node [
    id 375
    label "return"
  ]
  node [
    id 376
    label "give_birth"
  ]
  node [
    id 377
    label "dosta&#263;"
  ]
  node [
    id 378
    label "uk&#322;ad"
  ]
  node [
    id 379
    label "partia"
  ]
  node [
    id 380
    label "organizacja"
  ]
  node [
    id 381
    label "Unia_Wolno&#347;ci"
  ]
  node [
    id 382
    label "Unia_Europejska"
  ]
  node [
    id 383
    label "combination"
  ]
  node [
    id 384
    label "union"
  ]
  node [
    id 385
    label "Unia"
  ]
  node [
    id 386
    label "European"
  ]
  node [
    id 387
    label "po_europejsku"
  ]
  node [
    id 388
    label "charakterystyczny"
  ]
  node [
    id 389
    label "europejsko"
  ]
  node [
    id 390
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 391
    label "typowy"
  ]
  node [
    id 392
    label "remainder"
  ]
  node [
    id 393
    label "wydanie"
  ]
  node [
    id 394
    label "wyda&#263;"
  ]
  node [
    id 395
    label "wydawa&#263;"
  ]
  node [
    id 396
    label "pozosta&#322;y"
  ]
  node [
    id 397
    label "kwota"
  ]
  node [
    id 398
    label "zap&#322;aci&#263;"
  ]
  node [
    id 399
    label "wydatki_bud&#380;etowe"
  ]
  node [
    id 400
    label "etat"
  ]
  node [
    id 401
    label "portfel"
  ]
  node [
    id 402
    label "rezerwa_rewaluacyjna"
  ]
  node [
    id 403
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 404
    label "motywowa&#263;"
  ]
  node [
    id 405
    label "act"
  ]
  node [
    id 406
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 407
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 408
    label "s&#261;d"
  ]
  node [
    id 409
    label "legislacyjnie"
  ]
  node [
    id 410
    label "metodyka"
  ]
  node [
    id 411
    label "przebieg"
  ]
  node [
    id 412
    label "facylitator"
  ]
  node [
    id 413
    label "tryb"
  ]
  node [
    id 414
    label "skomplikowanie"
  ]
  node [
    id 415
    label "trudny"
  ]
  node [
    id 416
    label "wej&#347;&#263;"
  ]
  node [
    id 417
    label "wzi&#281;cie"
  ]
  node [
    id 418
    label "wyrucha&#263;"
  ]
  node [
    id 419
    label "uciec"
  ]
  node [
    id 420
    label "ruszy&#263;"
  ]
  node [
    id 421
    label "wygra&#263;"
  ]
  node [
    id 422
    label "zacz&#261;&#263;"
  ]
  node [
    id 423
    label "wyciupcia&#263;"
  ]
  node [
    id 424
    label "World_Health_Organization"
  ]
  node [
    id 425
    label "skorzysta&#263;"
  ]
  node [
    id 426
    label "pokona&#263;"
  ]
  node [
    id 427
    label "poczyta&#263;"
  ]
  node [
    id 428
    label "poruszy&#263;"
  ]
  node [
    id 429
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 430
    label "aim"
  ]
  node [
    id 431
    label "u&#380;y&#263;"
  ]
  node [
    id 432
    label "zaatakowa&#263;"
  ]
  node [
    id 433
    label "receive"
  ]
  node [
    id 434
    label "uda&#263;_si&#281;"
  ]
  node [
    id 435
    label "obskoczy&#263;"
  ]
  node [
    id 436
    label "zaopatrzy&#263;_si&#281;"
  ]
  node [
    id 437
    label "zrobi&#263;"
  ]
  node [
    id 438
    label "nakaza&#263;"
  ]
  node [
    id 439
    label "chwyci&#263;"
  ]
  node [
    id 440
    label "seize"
  ]
  node [
    id 441
    label "odziedziczy&#263;"
  ]
  node [
    id 442
    label "withdraw"
  ]
  node [
    id 443
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 444
    label "wch&#322;on&#261;&#263;"
  ]
  node [
    id 445
    label "liczny"
  ]
  node [
    id 446
    label "nieprzebranie"
  ]
  node [
    id 447
    label "du&#380;y"
  ]
  node [
    id 448
    label "przeliczny"
  ]
  node [
    id 449
    label "record"
  ]
  node [
    id 450
    label "wytw&#243;r"
  ]
  node [
    id 451
    label "&#347;wiadectwo"
  ]
  node [
    id 452
    label "zapis"
  ]
  node [
    id 453
    label "raport&#243;wka"
  ]
  node [
    id 454
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 455
    label "artyku&#322;"
  ]
  node [
    id 456
    label "plik"
  ]
  node [
    id 457
    label "writing"
  ]
  node [
    id 458
    label "utw&#243;r"
  ]
  node [
    id 459
    label "dokumentacja"
  ]
  node [
    id 460
    label "registratura"
  ]
  node [
    id 461
    label "parafa"
  ]
  node [
    id 462
    label "sygnatariusz"
  ]
  node [
    id 463
    label "fascyku&#322;"
  ]
  node [
    id 464
    label "potwierdzenie"
  ]
  node [
    id 465
    label "certificate"
  ]
  node [
    id 466
    label "zrobienie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 20
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 53
  ]
  edge [
    source 9
    target 18
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 125
  ]
  edge [
    source 10
    target 126
  ]
  edge [
    source 10
    target 127
  ]
  edge [
    source 10
    target 128
  ]
  edge [
    source 10
    target 129
  ]
  edge [
    source 10
    target 130
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 131
  ]
  edge [
    source 11
    target 132
  ]
  edge [
    source 11
    target 133
  ]
  edge [
    source 11
    target 134
  ]
  edge [
    source 11
    target 135
  ]
  edge [
    source 11
    target 136
  ]
  edge [
    source 11
    target 137
  ]
  edge [
    source 11
    target 138
  ]
  edge [
    source 11
    target 139
  ]
  edge [
    source 11
    target 140
  ]
  edge [
    source 11
    target 141
  ]
  edge [
    source 11
    target 142
  ]
  edge [
    source 11
    target 77
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 143
  ]
  edge [
    source 12
    target 144
  ]
  edge [
    source 12
    target 145
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 146
  ]
  edge [
    source 13
    target 147
  ]
  edge [
    source 13
    target 19
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 37
  ]
  edge [
    source 14
    target 24
  ]
  edge [
    source 14
    target 148
  ]
  edge [
    source 14
    target 149
  ]
  edge [
    source 14
    target 150
  ]
  edge [
    source 14
    target 151
  ]
  edge [
    source 14
    target 152
  ]
  edge [
    source 14
    target 153
  ]
  edge [
    source 14
    target 154
  ]
  edge [
    source 14
    target 155
  ]
  edge [
    source 14
    target 156
  ]
  edge [
    source 14
    target 157
  ]
  edge [
    source 14
    target 158
  ]
  edge [
    source 14
    target 41
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 159
  ]
  edge [
    source 15
    target 160
  ]
  edge [
    source 15
    target 161
  ]
  edge [
    source 15
    target 162
  ]
  edge [
    source 15
    target 163
  ]
  edge [
    source 15
    target 164
  ]
  edge [
    source 15
    target 165
  ]
  edge [
    source 15
    target 166
  ]
  edge [
    source 15
    target 167
  ]
  edge [
    source 15
    target 168
  ]
  edge [
    source 15
    target 169
  ]
  edge [
    source 15
    target 170
  ]
  edge [
    source 15
    target 171
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 58
  ]
  edge [
    source 17
    target 59
  ]
  edge [
    source 17
    target 172
  ]
  edge [
    source 17
    target 173
  ]
  edge [
    source 17
    target 174
  ]
  edge [
    source 17
    target 175
  ]
  edge [
    source 17
    target 176
  ]
  edge [
    source 17
    target 177
  ]
  edge [
    source 17
    target 178
  ]
  edge [
    source 17
    target 179
  ]
  edge [
    source 17
    target 180
  ]
  edge [
    source 17
    target 181
  ]
  edge [
    source 17
    target 182
  ]
  edge [
    source 17
    target 183
  ]
  edge [
    source 17
    target 184
  ]
  edge [
    source 17
    target 185
  ]
  edge [
    source 17
    target 186
  ]
  edge [
    source 17
    target 187
  ]
  edge [
    source 17
    target 188
  ]
  edge [
    source 17
    target 189
  ]
  edge [
    source 17
    target 190
  ]
  edge [
    source 17
    target 191
  ]
  edge [
    source 17
    target 192
  ]
  edge [
    source 17
    target 90
  ]
  edge [
    source 17
    target 193
  ]
  edge [
    source 17
    target 194
  ]
  edge [
    source 17
    target 195
  ]
  edge [
    source 17
    target 196
  ]
  edge [
    source 17
    target 197
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 54
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 198
  ]
  edge [
    source 19
    target 199
  ]
  edge [
    source 19
    target 200
  ]
  edge [
    source 19
    target 201
  ]
  edge [
    source 19
    target 202
  ]
  edge [
    source 19
    target 203
  ]
  edge [
    source 19
    target 204
  ]
  edge [
    source 19
    target 205
  ]
  edge [
    source 19
    target 206
  ]
  edge [
    source 19
    target 207
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 61
  ]
  edge [
    source 20
    target 62
  ]
  edge [
    source 20
    target 208
  ]
  edge [
    source 20
    target 209
  ]
  edge [
    source 20
    target 210
  ]
  edge [
    source 20
    target 211
  ]
  edge [
    source 20
    target 212
  ]
  edge [
    source 20
    target 213
  ]
  edge [
    source 20
    target 214
  ]
  edge [
    source 20
    target 215
  ]
  edge [
    source 20
    target 216
  ]
  edge [
    source 20
    target 217
  ]
  edge [
    source 20
    target 218
  ]
  edge [
    source 20
    target 170
  ]
  edge [
    source 20
    target 219
  ]
  edge [
    source 20
    target 220
  ]
  edge [
    source 20
    target 221
  ]
  edge [
    source 20
    target 222
  ]
  edge [
    source 20
    target 223
  ]
  edge [
    source 20
    target 224
  ]
  edge [
    source 20
    target 225
  ]
  edge [
    source 20
    target 226
  ]
  edge [
    source 20
    target 227
  ]
  edge [
    source 20
    target 228
  ]
  edge [
    source 20
    target 54
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 229
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 230
  ]
  edge [
    source 22
    target 231
  ]
  edge [
    source 22
    target 47
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 232
  ]
  edge [
    source 23
    target 233
  ]
  edge [
    source 23
    target 234
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 38
  ]
  edge [
    source 24
    target 57
  ]
  edge [
    source 24
    target 58
  ]
  edge [
    source 24
    target 235
  ]
  edge [
    source 24
    target 236
  ]
  edge [
    source 24
    target 237
  ]
  edge [
    source 24
    target 123
  ]
  edge [
    source 24
    target 238
  ]
  edge [
    source 24
    target 239
  ]
  edge [
    source 24
    target 240
  ]
  edge [
    source 24
    target 241
  ]
  edge [
    source 24
    target 242
  ]
  edge [
    source 24
    target 243
  ]
  edge [
    source 24
    target 244
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 245
  ]
  edge [
    source 25
    target 246
  ]
  edge [
    source 25
    target 247
  ]
  edge [
    source 25
    target 248
  ]
  edge [
    source 25
    target 249
  ]
  edge [
    source 25
    target 250
  ]
  edge [
    source 25
    target 251
  ]
  edge [
    source 25
    target 252
  ]
  edge [
    source 25
    target 253
  ]
  edge [
    source 25
    target 254
  ]
  edge [
    source 25
    target 255
  ]
  edge [
    source 25
    target 256
  ]
  edge [
    source 25
    target 257
  ]
  edge [
    source 25
    target 258
  ]
  edge [
    source 25
    target 259
  ]
  edge [
    source 25
    target 260
  ]
  edge [
    source 25
    target 261
  ]
  edge [
    source 25
    target 262
  ]
  edge [
    source 25
    target 263
  ]
  edge [
    source 25
    target 264
  ]
  edge [
    source 25
    target 265
  ]
  edge [
    source 25
    target 266
  ]
  edge [
    source 25
    target 60
  ]
  edge [
    source 25
    target 267
  ]
  edge [
    source 25
    target 268
  ]
  edge [
    source 25
    target 269
  ]
  edge [
    source 25
    target 270
  ]
  edge [
    source 25
    target 271
  ]
  edge [
    source 25
    target 272
  ]
  edge [
    source 25
    target 273
  ]
  edge [
    source 25
    target 274
  ]
  edge [
    source 25
    target 275
  ]
  edge [
    source 25
    target 276
  ]
  edge [
    source 25
    target 277
  ]
  edge [
    source 25
    target 278
  ]
  edge [
    source 25
    target 279
  ]
  edge [
    source 25
    target 280
  ]
  edge [
    source 25
    target 281
  ]
  edge [
    source 25
    target 282
  ]
  edge [
    source 25
    target 283
  ]
  edge [
    source 25
    target 284
  ]
  edge [
    source 25
    target 285
  ]
  edge [
    source 25
    target 286
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 287
  ]
  edge [
    source 27
    target 288
  ]
  edge [
    source 27
    target 123
  ]
  edge [
    source 27
    target 289
  ]
  edge [
    source 27
    target 290
  ]
  edge [
    source 27
    target 291
  ]
  edge [
    source 27
    target 292
  ]
  edge [
    source 27
    target 293
  ]
  edge [
    source 27
    target 294
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 295
  ]
  edge [
    source 29
    target 296
  ]
  edge [
    source 29
    target 297
  ]
  edge [
    source 29
    target 298
  ]
  edge [
    source 29
    target 299
  ]
  edge [
    source 29
    target 300
  ]
  edge [
    source 29
    target 158
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 301
  ]
  edge [
    source 30
    target 302
  ]
  edge [
    source 30
    target 303
  ]
  edge [
    source 30
    target 304
  ]
  edge [
    source 30
    target 305
  ]
  edge [
    source 30
    target 306
  ]
  edge [
    source 30
    target 307
  ]
  edge [
    source 30
    target 308
  ]
  edge [
    source 30
    target 309
  ]
  edge [
    source 30
    target 310
  ]
  edge [
    source 30
    target 311
  ]
  edge [
    source 30
    target 312
  ]
  edge [
    source 30
    target 313
  ]
  edge [
    source 30
    target 314
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 315
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 316
  ]
  edge [
    source 32
    target 317
  ]
  edge [
    source 32
    target 318
  ]
  edge [
    source 32
    target 319
  ]
  edge [
    source 32
    target 320
  ]
  edge [
    source 32
    target 65
  ]
  edge [
    source 32
    target 321
  ]
  edge [
    source 32
    target 322
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 323
  ]
  edge [
    source 34
    target 324
  ]
  edge [
    source 35
    target 325
  ]
  edge [
    source 35
    target 326
  ]
  edge [
    source 35
    target 327
  ]
  edge [
    source 35
    target 328
  ]
  edge [
    source 35
    target 329
  ]
  edge [
    source 35
    target 330
  ]
  edge [
    source 35
    target 331
  ]
  edge [
    source 35
    target 332
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 161
  ]
  edge [
    source 36
    target 333
  ]
  edge [
    source 36
    target 334
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 335
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 303
  ]
  edge [
    source 39
    target 336
  ]
  edge [
    source 39
    target 337
  ]
  edge [
    source 39
    target 338
  ]
  edge [
    source 39
    target 339
  ]
  edge [
    source 39
    target 105
  ]
  edge [
    source 39
    target 340
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 341
  ]
  edge [
    source 40
    target 342
  ]
  edge [
    source 40
    target 343
  ]
  edge [
    source 40
    target 344
  ]
  edge [
    source 40
    target 345
  ]
  edge [
    source 40
    target 346
  ]
  edge [
    source 40
    target 347
  ]
  edge [
    source 40
    target 348
  ]
  edge [
    source 40
    target 349
  ]
  edge [
    source 40
    target 350
  ]
  edge [
    source 40
    target 351
  ]
  edge [
    source 40
    target 352
  ]
  edge [
    source 40
    target 353
  ]
  edge [
    source 40
    target 354
  ]
  edge [
    source 40
    target 355
  ]
  edge [
    source 40
    target 356
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 54
  ]
  edge [
    source 41
    target 55
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 357
  ]
  edge [
    source 42
    target 358
  ]
  edge [
    source 42
    target 359
  ]
  edge [
    source 42
    target 360
  ]
  edge [
    source 42
    target 361
  ]
  edge [
    source 42
    target 362
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 363
  ]
  edge [
    source 43
    target 364
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 365
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 366
  ]
  edge [
    source 47
    target 367
  ]
  edge [
    source 47
    target 368
  ]
  edge [
    source 47
    target 369
  ]
  edge [
    source 47
    target 370
  ]
  edge [
    source 47
    target 371
  ]
  edge [
    source 47
    target 372
  ]
  edge [
    source 47
    target 373
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 374
  ]
  edge [
    source 48
    target 375
  ]
  edge [
    source 48
    target 376
  ]
  edge [
    source 48
    target 377
  ]
  edge [
    source 48
    target 60
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 378
  ]
  edge [
    source 49
    target 379
  ]
  edge [
    source 49
    target 380
  ]
  edge [
    source 49
    target 381
  ]
  edge [
    source 49
    target 382
  ]
  edge [
    source 49
    target 383
  ]
  edge [
    source 49
    target 384
  ]
  edge [
    source 49
    target 385
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 386
  ]
  edge [
    source 50
    target 387
  ]
  edge [
    source 50
    target 388
  ]
  edge [
    source 50
    target 389
  ]
  edge [
    source 50
    target 390
  ]
  edge [
    source 50
    target 391
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 392
  ]
  edge [
    source 51
    target 393
  ]
  edge [
    source 51
    target 394
  ]
  edge [
    source 51
    target 372
  ]
  edge [
    source 51
    target 395
  ]
  edge [
    source 51
    target 396
  ]
  edge [
    source 51
    target 397
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 219
  ]
  edge [
    source 52
    target 398
  ]
  edge [
    source 53
    target 399
  ]
  edge [
    source 53
    target 400
  ]
  edge [
    source 53
    target 401
  ]
  edge [
    source 53
    target 402
  ]
  edge [
    source 53
    target 397
  ]
  edge [
    source 54
    target 60
  ]
  edge [
    source 54
    target 61
  ]
  edge [
    source 54
    target 237
  ]
  edge [
    source 54
    target 403
  ]
  edge [
    source 54
    target 397
  ]
  edge [
    source 54
    target 371
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 404
  ]
  edge [
    source 55
    target 240
  ]
  edge [
    source 55
    target 405
  ]
  edge [
    source 55
    target 406
  ]
  edge [
    source 55
    target 407
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 408
  ]
  edge [
    source 56
    target 409
  ]
  edge [
    source 56
    target 403
  ]
  edge [
    source 56
    target 410
  ]
  edge [
    source 56
    target 411
  ]
  edge [
    source 56
    target 412
  ]
  edge [
    source 56
    target 413
  ]
  edge [
    source 58
    target 414
  ]
  edge [
    source 58
    target 415
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 60
    target 416
  ]
  edge [
    source 60
    target 245
  ]
  edge [
    source 60
    target 417
  ]
  edge [
    source 60
    target 418
  ]
  edge [
    source 60
    target 419
  ]
  edge [
    source 60
    target 420
  ]
  edge [
    source 60
    target 421
  ]
  edge [
    source 60
    target 160
  ]
  edge [
    source 60
    target 422
  ]
  edge [
    source 60
    target 423
  ]
  edge [
    source 60
    target 424
  ]
  edge [
    source 60
    target 425
  ]
  edge [
    source 60
    target 426
  ]
  edge [
    source 60
    target 427
  ]
  edge [
    source 60
    target 428
  ]
  edge [
    source 60
    target 429
  ]
  edge [
    source 60
    target 265
  ]
  edge [
    source 60
    target 430
  ]
  edge [
    source 60
    target 269
  ]
  edge [
    source 60
    target 431
  ]
  edge [
    source 60
    target 432
  ]
  edge [
    source 60
    target 433
  ]
  edge [
    source 60
    target 434
  ]
  edge [
    source 60
    target 377
  ]
  edge [
    source 60
    target 435
  ]
  edge [
    source 60
    target 436
  ]
  edge [
    source 60
    target 437
  ]
  edge [
    source 60
    target 438
  ]
  edge [
    source 60
    target 439
  ]
  edge [
    source 60
    target 166
  ]
  edge [
    source 60
    target 440
  ]
  edge [
    source 60
    target 441
  ]
  edge [
    source 60
    target 442
  ]
  edge [
    source 60
    target 443
  ]
  edge [
    source 60
    target 444
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 445
  ]
  edge [
    source 62
    target 446
  ]
  edge [
    source 62
    target 447
  ]
  edge [
    source 62
    target 448
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 449
  ]
  edge [
    source 63
    target 450
  ]
  edge [
    source 63
    target 451
  ]
  edge [
    source 63
    target 452
  ]
  edge [
    source 63
    target 453
  ]
  edge [
    source 63
    target 454
  ]
  edge [
    source 63
    target 455
  ]
  edge [
    source 63
    target 456
  ]
  edge [
    source 63
    target 457
  ]
  edge [
    source 63
    target 458
  ]
  edge [
    source 63
    target 459
  ]
  edge [
    source 63
    target 460
  ]
  edge [
    source 63
    target 461
  ]
  edge [
    source 63
    target 462
  ]
  edge [
    source 63
    target 463
  ]
  edge [
    source 64
    target 464
  ]
  edge [
    source 64
    target 465
  ]
  edge [
    source 64
    target 466
  ]
]
