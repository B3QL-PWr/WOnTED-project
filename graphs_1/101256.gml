graph [
  maxDegree 2
  minDegree 1
  meanDegree 1.6470588235294117
  density 0.10294117647058823
  graphCliqueNumber 3
  node [
    id 0
    label "promet"
    origin "text"
  ]
  node [
    id 1
    label "pierwiastek_promieniotw&#243;rczy"
  ]
  node [
    id 2
    label "lantanowiec"
  ]
  node [
    id 3
    label "J"
  ]
  node [
    id 4
    label "albo"
  ]
  node [
    id 5
    label "Marinskyego"
  ]
  node [
    id 6
    label "litr"
  ]
  node [
    id 7
    label "e"
  ]
  node [
    id 8
    label "Glendenina"
  ]
  node [
    id 9
    label "c&#243;rka"
  ]
  node [
    id 10
    label "dzie&#324;"
  ]
  node [
    id 11
    label "Coryella"
  ]
  node [
    id 12
    label "HR"
  ]
  node [
    id 13
    label "465"
  ]
  node [
    id 14
    label "HD"
  ]
  node [
    id 15
    label "101065"
  ]
  node [
    id 16
    label "965"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 16
  ]
]
