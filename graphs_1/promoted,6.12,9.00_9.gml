graph [
  maxDegree 17
  minDegree 1
  meanDegree 2
  density 0.08333333333333333
  graphCliqueNumber 3
  node [
    id 0
    label "mecz"
    origin "text"
  ]
  node [
    id 1
    label "liga"
    origin "text"
  ]
  node [
    id 2
    label "argenty&#324;ski"
    origin "text"
  ]
  node [
    id 3
    label "obrona"
  ]
  node [
    id 4
    label "gra"
  ]
  node [
    id 5
    label "dwumecz"
  ]
  node [
    id 6
    label "game"
  ]
  node [
    id 7
    label "serw"
  ]
  node [
    id 8
    label "poziom"
  ]
  node [
    id 9
    label "organizacja"
  ]
  node [
    id 10
    label "&#347;rodowisko"
  ]
  node [
    id 11
    label "atak"
  ]
  node [
    id 12
    label "grupa"
  ]
  node [
    id 13
    label "mecz_mistrzowski"
  ]
  node [
    id 14
    label "pr&#243;ba"
  ]
  node [
    id 15
    label "zbi&#243;r"
  ]
  node [
    id 16
    label "arrangement"
  ]
  node [
    id 17
    label "pomoc"
  ]
  node [
    id 18
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 19
    label "union"
  ]
  node [
    id 20
    label "rezerwa"
  ]
  node [
    id 21
    label "moneta"
  ]
  node [
    id 22
    label "po&#322;udniowoameryka&#324;ski"
  ]
  node [
    id 23
    label "po_argenty&#324;sku"
  ]
  node [
    id 24
    label "tango"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
]
