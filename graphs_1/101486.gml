graph [
  maxDegree 7
  minDegree 1
  meanDegree 1.6666666666666667
  density 0.09803921568627451
  graphCliqueNumber 3
  node [
    id 0
    label "synagoga"
    origin "text"
  ]
  node [
    id 1
    label "skwierzynie"
    origin "text"
  ]
  node [
    id 2
    label "aron_ha-kodesz"
  ]
  node [
    id 3
    label "babiniec"
  ]
  node [
    id 4
    label "budynek"
  ]
  node [
    id 5
    label "judaizm"
  ]
  node [
    id 6
    label "synagogue"
  ]
  node [
    id 7
    label "siedziba"
  ]
  node [
    id 8
    label "on"
  ]
  node [
    id 9
    label "W&#322;adys&#322;awa"
  ]
  node [
    id 10
    label "Jagie&#322;&#322;o"
  ]
  node [
    id 11
    label "ii"
  ]
  node [
    id 12
    label "wojna"
  ]
  node [
    id 13
    label "&#347;wiatowy"
  ]
  node [
    id 14
    label "noc"
  ]
  node [
    id 15
    label "kryszta&#322;owy"
  ]
  node [
    id 16
    label "powstaniec"
  ]
  node [
    id 17
    label "wielkopolski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 13
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 16
    target 17
  ]
]
