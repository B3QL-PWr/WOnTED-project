graph [
  maxDegree 52
  minDegree 1
  meanDegree 2.161290322580645
  density 0.004991432615659689
  graphCliqueNumber 3
  node [
    id 0
    label "koniec"
    origin "text"
  ]
  node [
    id 1
    label "wyj&#347;&#263;"
    origin "text"
  ]
  node [
    id 2
    label "czego"
    origin "text"
  ]
  node [
    id 3
    label "potrzebny"
    origin "text"
  ]
  node [
    id 4
    label "by&#263;"
    origin "text"
  ]
  node [
    id 5
    label "google"
    origin "text"
  ]
  node [
    id 6
    label "gears"
    origin "text"
  ]
  node [
    id 7
    label "dorzuci&#263;"
    origin "text"
  ]
  node [
    id 8
    label "kilka"
    origin "text"
  ]
  node [
    id 9
    label "bardzo"
    origin "text"
  ]
  node [
    id 10
    label "smakowity"
    origin "text"
  ]
  node [
    id 11
    label "k&#261;sek"
    origin "text"
  ]
  node [
    id 12
    label "mama"
    origin "text"
  ]
  node [
    id 13
    label "wreszcie"
    origin "text"
  ]
  node [
    id 14
    label "system"
    origin "text"
  ]
  node [
    id 15
    label "operacyjny"
    origin "text"
  ]
  node [
    id 16
    label "przegl&#261;darka"
    origin "text"
  ]
  node [
    id 17
    label "chroma"
    origin "text"
  ]
  node [
    id 18
    label "niezale&#380;nie"
    origin "text"
  ]
  node [
    id 19
    label "&#347;miertelny"
    origin "text"
  ]
  node [
    id 20
    label "cios"
    origin "text"
  ]
  node [
    id 21
    label "dla"
    origin "text"
  ]
  node [
    id 22
    label "windows"
    origin "text"
  ]
  node [
    id 23
    label "firefoksa"
    origin "text"
  ]
  node [
    id 24
    label "program"
    origin "text"
  ]
  node [
    id 25
    label "kolejny"
    origin "text"
  ]
  node [
    id 26
    label "zwiastun"
    origin "text"
  ]
  node [
    id 27
    label "zmiana"
    origin "text"
  ]
  node [
    id 28
    label "interfejs"
    origin "text"
  ]
  node [
    id 29
    label "graficzny"
    origin "text"
  ]
  node [
    id 30
    label "minuta"
    origin "text"
  ]
  node [
    id 31
    label "rzeczywi&#347;cie"
    origin "text"
  ]
  node [
    id 32
    label "da&#263;"
    origin "text"
  ]
  node [
    id 33
    label "si&#281;"
    origin "text"
  ]
  node [
    id 34
    label "odczu&#263;"
    origin "text"
  ]
  node [
    id 35
    label "szybko&#347;&#263;"
    origin "text"
  ]
  node [
    id 36
    label "wczytywa&#263;"
    origin "text"
  ]
  node [
    id 37
    label "strona"
    origin "text"
  ]
  node [
    id 38
    label "javascript"
    origin "text"
  ]
  node [
    id 39
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 40
    label "zak&#322;adka"
    origin "text"
  ]
  node [
    id 41
    label "oddzielny"
    origin "text"
  ]
  node [
    id 42
    label "proces"
    origin "text"
  ]
  node [
    id 43
    label "te&#380;"
    origin "text"
  ]
  node [
    id 44
    label "brzmie&#263;"
    origin "text"
  ]
  node [
    id 45
    label "nie&#378;le"
    origin "text"
  ]
  node [
    id 46
    label "szczeg&#243;lnie"
    origin "text"
  ]
  node [
    id 47
    label "pod"
    origin "text"
  ]
  node [
    id 48
    label "linuksem"
    origin "text"
  ]
  node [
    id 49
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 50
    label "adobe"
    origin "text"
  ]
  node [
    id 51
    label "flash"
    origin "text"
  ]
  node [
    id 52
    label "regularnie"
    origin "text"
  ]
  node [
    id 53
    label "wywala&#263;"
    origin "text"
  ]
  node [
    id 54
    label "niestety"
    origin "text"
  ]
  node [
    id 55
    label "wersja"
    origin "text"
  ]
  node [
    id 56
    label "linuksa"
    origin "text"
  ]
  node [
    id 57
    label "raz"
    origin "text"
  ]
  node [
    id 58
    label "brak"
    origin "text"
  ]
  node [
    id 59
    label "defenestracja"
  ]
  node [
    id 60
    label "szereg"
  ]
  node [
    id 61
    label "dzia&#322;anie"
  ]
  node [
    id 62
    label "miejsce"
  ]
  node [
    id 63
    label "ostatnie_podrygi"
  ]
  node [
    id 64
    label "kres"
  ]
  node [
    id 65
    label "agonia"
  ]
  node [
    id 66
    label "visitation"
  ]
  node [
    id 67
    label "szeol"
  ]
  node [
    id 68
    label "mogi&#322;a"
  ]
  node [
    id 69
    label "chwila"
  ]
  node [
    id 70
    label "wydarzenie"
  ]
  node [
    id 71
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 72
    label "pogrzebanie"
  ]
  node [
    id 73
    label "punkt"
  ]
  node [
    id 74
    label "&#380;a&#322;oba"
  ]
  node [
    id 75
    label "zabicie"
  ]
  node [
    id 76
    label "kres_&#380;ycia"
  ]
  node [
    id 77
    label "get"
  ]
  node [
    id 78
    label "opu&#347;ci&#263;"
  ]
  node [
    id 79
    label "ukaza&#263;_si&#281;_drukiem"
  ]
  node [
    id 80
    label "zej&#347;&#263;"
  ]
  node [
    id 81
    label "okaza&#263;_si&#281;"
  ]
  node [
    id 82
    label "powie&#347;&#263;_si&#281;"
  ]
  node [
    id 83
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 84
    label "sko&#324;czy&#263;"
  ]
  node [
    id 85
    label "ograniczenie"
  ]
  node [
    id 86
    label "ruszy&#263;"
  ]
  node [
    id 87
    label "wypa&#347;&#263;"
  ]
  node [
    id 88
    label "uko&#324;czy&#263;"
  ]
  node [
    id 89
    label "open"
  ]
  node [
    id 90
    label "moderate"
  ]
  node [
    id 91
    label "uzyska&#263;"
  ]
  node [
    id 92
    label "wydosta&#263;_si&#281;"
  ]
  node [
    id 93
    label "wywie&#347;&#263;_si&#281;"
  ]
  node [
    id 94
    label "mount"
  ]
  node [
    id 95
    label "leave"
  ]
  node [
    id 96
    label "drive"
  ]
  node [
    id 97
    label "zagra&#263;"
  ]
  node [
    id 98
    label "zademonstrowa&#263;"
  ]
  node [
    id 99
    label "wystarczy&#263;"
  ]
  node [
    id 100
    label "perform"
  ]
  node [
    id 101
    label "uwolni&#263;_si&#281;"
  ]
  node [
    id 102
    label "drop"
  ]
  node [
    id 103
    label "potrzebnie"
  ]
  node [
    id 104
    label "przydatny"
  ]
  node [
    id 105
    label "si&#281;ga&#263;"
  ]
  node [
    id 106
    label "trwa&#263;"
  ]
  node [
    id 107
    label "obecno&#347;&#263;"
  ]
  node [
    id 108
    label "stan"
  ]
  node [
    id 109
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 110
    label "stand"
  ]
  node [
    id 111
    label "mie&#263;_miejsce"
  ]
  node [
    id 112
    label "uczestniczy&#263;"
  ]
  node [
    id 113
    label "chodzi&#263;"
  ]
  node [
    id 114
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 115
    label "equal"
  ]
  node [
    id 116
    label "uzupe&#322;ni&#263;"
  ]
  node [
    id 117
    label "bewilder"
  ]
  node [
    id 118
    label "do&#322;o&#380;y&#263;"
  ]
  node [
    id 119
    label "&#347;ledziowate"
  ]
  node [
    id 120
    label "ryba"
  ]
  node [
    id 121
    label "w_chuj"
  ]
  node [
    id 122
    label "po&#380;&#261;dany"
  ]
  node [
    id 123
    label "ciekawy"
  ]
  node [
    id 124
    label "seksowny"
  ]
  node [
    id 125
    label "kusz&#261;cy"
  ]
  node [
    id 126
    label "dobry"
  ]
  node [
    id 127
    label "apetyczny"
  ]
  node [
    id 128
    label "smakowicie"
  ]
  node [
    id 129
    label "piece"
  ]
  node [
    id 130
    label "porcja"
  ]
  node [
    id 131
    label "matczysko"
  ]
  node [
    id 132
    label "macierz"
  ]
  node [
    id 133
    label "przodkini"
  ]
  node [
    id 134
    label "Matka_Boska"
  ]
  node [
    id 135
    label "macocha"
  ]
  node [
    id 136
    label "matka_zast&#281;pcza"
  ]
  node [
    id 137
    label "stara"
  ]
  node [
    id 138
    label "rodzice"
  ]
  node [
    id 139
    label "rodzic"
  ]
  node [
    id 140
    label "w&#380;dy"
  ]
  node [
    id 141
    label "model"
  ]
  node [
    id 142
    label "sk&#322;ad"
  ]
  node [
    id 143
    label "zachowanie"
  ]
  node [
    id 144
    label "podstawa"
  ]
  node [
    id 145
    label "porz&#261;dek"
  ]
  node [
    id 146
    label "Android"
  ]
  node [
    id 147
    label "przyn&#281;ta"
  ]
  node [
    id 148
    label "jednostka_geologiczna"
  ]
  node [
    id 149
    label "metoda"
  ]
  node [
    id 150
    label "podsystem"
  ]
  node [
    id 151
    label "p&#322;&#243;d"
  ]
  node [
    id 152
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 153
    label "s&#261;d"
  ]
  node [
    id 154
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 155
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 156
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 157
    label "j&#261;dro"
  ]
  node [
    id 158
    label "eratem"
  ]
  node [
    id 159
    label "pulpit"
  ]
  node [
    id 160
    label "struktura"
  ]
  node [
    id 161
    label "spos&#243;b"
  ]
  node [
    id 162
    label "oddzia&#322;"
  ]
  node [
    id 163
    label "usenet"
  ]
  node [
    id 164
    label "o&#347;"
  ]
  node [
    id 165
    label "oprogramowanie"
  ]
  node [
    id 166
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 167
    label "poj&#281;cie"
  ]
  node [
    id 168
    label "w&#281;dkarstwo"
  ]
  node [
    id 169
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 170
    label "Leopard"
  ]
  node [
    id 171
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 172
    label "systemik"
  ]
  node [
    id 173
    label "rozprz&#261;c"
  ]
  node [
    id 174
    label "cybernetyk"
  ]
  node [
    id 175
    label "konstelacja"
  ]
  node [
    id 176
    label "doktryna"
  ]
  node [
    id 177
    label "net"
  ]
  node [
    id 178
    label "zbi&#243;r"
  ]
  node [
    id 179
    label "method"
  ]
  node [
    id 180
    label "systemat"
  ]
  node [
    id 181
    label "mo&#380;liwy"
  ]
  node [
    id 182
    label "medyczny"
  ]
  node [
    id 183
    label "operacyjnie"
  ]
  node [
    id 184
    label "viewer"
  ]
  node [
    id 185
    label "przyrz&#261;d"
  ]
  node [
    id 186
    label "browser"
  ]
  node [
    id 187
    label "projektor"
  ]
  node [
    id 188
    label "niezale&#380;ny"
  ]
  node [
    id 189
    label "przed&#347;miertelny"
  ]
  node [
    id 190
    label "&#347;miertelnie"
  ]
  node [
    id 191
    label "zaciek&#322;y"
  ]
  node [
    id 192
    label "przyt&#322;aczaj&#261;cy"
  ]
  node [
    id 193
    label "wielki"
  ]
  node [
    id 194
    label "niebezpieczny"
  ]
  node [
    id 195
    label "tragiczny"
  ]
  node [
    id 196
    label "za&#380;arty"
  ]
  node [
    id 197
    label "ko&#324;cowy"
  ]
  node [
    id 198
    label "&#322;upliwo&#347;&#263;"
  ]
  node [
    id 199
    label "nieszcz&#281;&#347;cie"
  ]
  node [
    id 200
    label "shot"
  ]
  node [
    id 201
    label "struktura_geologiczna"
  ]
  node [
    id 202
    label "uderzenie"
  ]
  node [
    id 203
    label "siekacz"
  ]
  node [
    id 204
    label "blok"
  ]
  node [
    id 205
    label "pr&#243;ba"
  ]
  node [
    id 206
    label "coup"
  ]
  node [
    id 207
    label "time"
  ]
  node [
    id 208
    label "r&#243;&#380;a_skalna"
  ]
  node [
    id 209
    label "spis"
  ]
  node [
    id 210
    label "odinstalowanie"
  ]
  node [
    id 211
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 212
    label "za&#322;o&#380;enie"
  ]
  node [
    id 213
    label "emitowanie"
  ]
  node [
    id 214
    label "odinstalowywanie"
  ]
  node [
    id 215
    label "instrukcja"
  ]
  node [
    id 216
    label "teleferie"
  ]
  node [
    id 217
    label "emitowa&#263;"
  ]
  node [
    id 218
    label "wytw&#243;r"
  ]
  node [
    id 219
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 220
    label "sekcja_krytyczna"
  ]
  node [
    id 221
    label "oferta"
  ]
  node [
    id 222
    label "prezentowa&#263;"
  ]
  node [
    id 223
    label "podprogram"
  ]
  node [
    id 224
    label "tryb"
  ]
  node [
    id 225
    label "dzia&#322;"
  ]
  node [
    id 226
    label "broszura"
  ]
  node [
    id 227
    label "deklaracja"
  ]
  node [
    id 228
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 229
    label "struktura_organizacyjna"
  ]
  node [
    id 230
    label "zaprezentowanie"
  ]
  node [
    id 231
    label "informatyka"
  ]
  node [
    id 232
    label "booklet"
  ]
  node [
    id 233
    label "menu"
  ]
  node [
    id 234
    label "instalowanie"
  ]
  node [
    id 235
    label "furkacja"
  ]
  node [
    id 236
    label "odinstalowa&#263;"
  ]
  node [
    id 237
    label "instalowa&#263;"
  ]
  node [
    id 238
    label "pirat"
  ]
  node [
    id 239
    label "zainstalowanie"
  ]
  node [
    id 240
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 241
    label "ogranicznik_referencyjny"
  ]
  node [
    id 242
    label "zainstalowa&#263;"
  ]
  node [
    id 243
    label "kana&#322;"
  ]
  node [
    id 244
    label "zaprezentowa&#263;"
  ]
  node [
    id 245
    label "odinstalowywa&#263;"
  ]
  node [
    id 246
    label "folder"
  ]
  node [
    id 247
    label "course_of_study"
  ]
  node [
    id 248
    label "ram&#243;wka"
  ]
  node [
    id 249
    label "prezentowanie"
  ]
  node [
    id 250
    label "okno"
  ]
  node [
    id 251
    label "inny"
  ]
  node [
    id 252
    label "nast&#281;pnie"
  ]
  node [
    id 253
    label "kt&#243;ry&#347;"
  ]
  node [
    id 254
    label "kolejno"
  ]
  node [
    id 255
    label "nastopny"
  ]
  node [
    id 256
    label "nabawianie_si&#281;"
  ]
  node [
    id 257
    label "harbinger"
  ]
  node [
    id 258
    label "oznaka"
  ]
  node [
    id 259
    label "zapowied&#378;"
  ]
  node [
    id 260
    label "nabawienie_si&#281;"
  ]
  node [
    id 261
    label "obwie&#347;ciciel"
  ]
  node [
    id 262
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 263
    label "przewidywanie"
  ]
  node [
    id 264
    label "declaration"
  ]
  node [
    id 265
    label "reklama"
  ]
  node [
    id 266
    label "film_kr&#243;tkometra&#380;owy"
  ]
  node [
    id 267
    label "anatomopatolog"
  ]
  node [
    id 268
    label "rewizja"
  ]
  node [
    id 269
    label "czas"
  ]
  node [
    id 270
    label "ferment"
  ]
  node [
    id 271
    label "komplet"
  ]
  node [
    id 272
    label "tura"
  ]
  node [
    id 273
    label "amendment"
  ]
  node [
    id 274
    label "zmianka"
  ]
  node [
    id 275
    label "odmienianie"
  ]
  node [
    id 276
    label "passage"
  ]
  node [
    id 277
    label "zjawisko"
  ]
  node [
    id 278
    label "change"
  ]
  node [
    id 279
    label "praca"
  ]
  node [
    id 280
    label "urz&#261;dzenie"
  ]
  node [
    id 281
    label "elektroniczny"
  ]
  node [
    id 282
    label "graficznie"
  ]
  node [
    id 283
    label "zapis"
  ]
  node [
    id 284
    label "godzina"
  ]
  node [
    id 285
    label "sekunda"
  ]
  node [
    id 286
    label "kwadrans"
  ]
  node [
    id 287
    label "stopie&#324;"
  ]
  node [
    id 288
    label "design"
  ]
  node [
    id 289
    label "jednostka"
  ]
  node [
    id 290
    label "realny"
  ]
  node [
    id 291
    label "naprawd&#281;"
  ]
  node [
    id 292
    label "dostarczy&#263;"
  ]
  node [
    id 293
    label "obieca&#263;"
  ]
  node [
    id 294
    label "pozwoli&#263;"
  ]
  node [
    id 295
    label "przeznaczy&#263;"
  ]
  node [
    id 296
    label "doda&#263;"
  ]
  node [
    id 297
    label "give"
  ]
  node [
    id 298
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 299
    label "wyrzec_si&#281;"
  ]
  node [
    id 300
    label "supply"
  ]
  node [
    id 301
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 302
    label "zada&#263;"
  ]
  node [
    id 303
    label "odst&#261;pi&#263;"
  ]
  node [
    id 304
    label "feed"
  ]
  node [
    id 305
    label "testify"
  ]
  node [
    id 306
    label "powierzy&#263;"
  ]
  node [
    id 307
    label "convey"
  ]
  node [
    id 308
    label "przekaza&#263;"
  ]
  node [
    id 309
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 310
    label "zap&#322;aci&#263;"
  ]
  node [
    id 311
    label "dress"
  ]
  node [
    id 312
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 313
    label "udost&#281;pni&#263;"
  ]
  node [
    id 314
    label "sztachn&#261;&#263;"
  ]
  node [
    id 315
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 316
    label "zrobi&#263;"
  ]
  node [
    id 317
    label "przywali&#263;"
  ]
  node [
    id 318
    label "rap"
  ]
  node [
    id 319
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 320
    label "picture"
  ]
  node [
    id 321
    label "feel"
  ]
  node [
    id 322
    label "zagorze&#263;"
  ]
  node [
    id 323
    label "konsekwencja"
  ]
  node [
    id 324
    label "dozna&#263;"
  ]
  node [
    id 325
    label "postrzec"
  ]
  node [
    id 326
    label "sprawno&#347;&#263;"
  ]
  node [
    id 327
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 328
    label "celerity"
  ]
  node [
    id 329
    label "kr&#243;tko&#347;&#263;"
  ]
  node [
    id 330
    label "tempo"
  ]
  node [
    id 331
    label "wektorowa_wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 332
    label "pr&#281;dko&#347;&#263;"
  ]
  node [
    id 333
    label "przywo&#322;ywa&#263;"
  ]
  node [
    id 334
    label "dok&#322;ada&#263;"
  ]
  node [
    id 335
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 336
    label "odczytywa&#263;"
  ]
  node [
    id 337
    label "load"
  ]
  node [
    id 338
    label "skr&#281;canie"
  ]
  node [
    id 339
    label "voice"
  ]
  node [
    id 340
    label "forma"
  ]
  node [
    id 341
    label "internet"
  ]
  node [
    id 342
    label "skr&#281;ci&#263;"
  ]
  node [
    id 343
    label "kartka"
  ]
  node [
    id 344
    label "orientowa&#263;"
  ]
  node [
    id 345
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 346
    label "powierzchnia"
  ]
  node [
    id 347
    label "plik"
  ]
  node [
    id 348
    label "bok"
  ]
  node [
    id 349
    label "pagina"
  ]
  node [
    id 350
    label "orientowanie"
  ]
  node [
    id 351
    label "fragment"
  ]
  node [
    id 352
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 353
    label "skr&#281;ca&#263;"
  ]
  node [
    id 354
    label "g&#243;ra"
  ]
  node [
    id 355
    label "serwis_internetowy"
  ]
  node [
    id 356
    label "orientacja"
  ]
  node [
    id 357
    label "linia"
  ]
  node [
    id 358
    label "skr&#281;cenie"
  ]
  node [
    id 359
    label "layout"
  ]
  node [
    id 360
    label "zorientowa&#263;"
  ]
  node [
    id 361
    label "zorientowanie"
  ]
  node [
    id 362
    label "obiekt"
  ]
  node [
    id 363
    label "podmiot"
  ]
  node [
    id 364
    label "ty&#322;"
  ]
  node [
    id 365
    label "logowanie"
  ]
  node [
    id 366
    label "adres_internetowy"
  ]
  node [
    id 367
    label "uj&#281;cie"
  ]
  node [
    id 368
    label "prz&#243;d"
  ]
  node [
    id 369
    label "posta&#263;"
  ]
  node [
    id 370
    label "jaki&#347;"
  ]
  node [
    id 371
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 372
    label "widok"
  ]
  node [
    id 373
    label "z&#322;&#261;czenie"
  ]
  node [
    id 374
    label "bookmark"
  ]
  node [
    id 375
    label "znacznik"
  ]
  node [
    id 376
    label "sp&#243;&#322;g&#322;oska"
  ]
  node [
    id 377
    label "fa&#322;da"
  ]
  node [
    id 378
    label "osobno"
  ]
  node [
    id 379
    label "wyodr&#281;bnianie_si&#281;"
  ]
  node [
    id 380
    label "wydzielenie"
  ]
  node [
    id 381
    label "wyodr&#281;bnienie_si&#281;"
  ]
  node [
    id 382
    label "odr&#281;bny"
  ]
  node [
    id 383
    label "wyodr&#281;bnianie"
  ]
  node [
    id 384
    label "legislacyjnie"
  ]
  node [
    id 385
    label "kognicja"
  ]
  node [
    id 386
    label "przebieg"
  ]
  node [
    id 387
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 388
    label "przes&#322;anka"
  ]
  node [
    id 389
    label "rozprawa"
  ]
  node [
    id 390
    label "nast&#281;pstwo"
  ]
  node [
    id 391
    label "sound"
  ]
  node [
    id 392
    label "wyra&#380;a&#263;"
  ]
  node [
    id 393
    label "wydawa&#263;"
  ]
  node [
    id 394
    label "s&#322;ycha&#263;"
  ]
  node [
    id 395
    label "echo"
  ]
  node [
    id 396
    label "sporo"
  ]
  node [
    id 397
    label "pozytywnie"
  ]
  node [
    id 398
    label "intensywnie"
  ]
  node [
    id 399
    label "nieszpetnie"
  ]
  node [
    id 400
    label "niez&#322;y"
  ]
  node [
    id 401
    label "skutecznie"
  ]
  node [
    id 402
    label "szczeg&#243;lny"
  ]
  node [
    id 403
    label "osobnie"
  ]
  node [
    id 404
    label "wyj&#261;tkowo"
  ]
  node [
    id 405
    label "specially"
  ]
  node [
    id 406
    label "pami&#281;&#263;"
  ]
  node [
    id 407
    label "Adobe_Flash"
  ]
  node [
    id 408
    label "harmonijnie"
  ]
  node [
    id 409
    label "poprostu"
  ]
  node [
    id 410
    label "cz&#281;sto"
  ]
  node [
    id 411
    label "zwyczajny"
  ]
  node [
    id 412
    label "stale"
  ]
  node [
    id 413
    label "regularny"
  ]
  node [
    id 414
    label "wychrzania&#263;"
  ]
  node [
    id 415
    label "usuwa&#263;"
  ]
  node [
    id 416
    label "wypierdala&#263;"
  ]
  node [
    id 417
    label "pozbywa&#263;_si&#281;"
  ]
  node [
    id 418
    label "wypiernicza&#263;"
  ]
  node [
    id 419
    label "raise"
  ]
  node [
    id 420
    label "typ"
  ]
  node [
    id 421
    label "prywatywny"
  ]
  node [
    id 422
    label "defect"
  ]
  node [
    id 423
    label "odej&#347;cie"
  ]
  node [
    id 424
    label "gap"
  ]
  node [
    id 425
    label "kr&#243;tki"
  ]
  node [
    id 426
    label "wyr&#243;b"
  ]
  node [
    id 427
    label "nieistnienie"
  ]
  node [
    id 428
    label "wada"
  ]
  node [
    id 429
    label "odej&#347;&#263;"
  ]
  node [
    id 430
    label "odchodzenie"
  ]
  node [
    id 431
    label "odchodzi&#263;"
  ]
  node [
    id 432
    label "Adobe"
  ]
  node [
    id 433
    label "Flash"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 16
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 18
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 116
  ]
  edge [
    source 7
    target 117
  ]
  edge [
    source 7
    target 118
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 29
  ]
  edge [
    source 8
    target 30
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 38
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 10
    target 125
  ]
  edge [
    source 10
    target 126
  ]
  edge [
    source 10
    target 127
  ]
  edge [
    source 10
    target 128
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 129
  ]
  edge [
    source 11
    target 130
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 131
  ]
  edge [
    source 12
    target 132
  ]
  edge [
    source 12
    target 133
  ]
  edge [
    source 12
    target 134
  ]
  edge [
    source 12
    target 135
  ]
  edge [
    source 12
    target 136
  ]
  edge [
    source 12
    target 137
  ]
  edge [
    source 12
    target 138
  ]
  edge [
    source 12
    target 139
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 140
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 141
  ]
  edge [
    source 14
    target 142
  ]
  edge [
    source 14
    target 143
  ]
  edge [
    source 14
    target 144
  ]
  edge [
    source 14
    target 145
  ]
  edge [
    source 14
    target 146
  ]
  edge [
    source 14
    target 147
  ]
  edge [
    source 14
    target 148
  ]
  edge [
    source 14
    target 149
  ]
  edge [
    source 14
    target 150
  ]
  edge [
    source 14
    target 151
  ]
  edge [
    source 14
    target 152
  ]
  edge [
    source 14
    target 153
  ]
  edge [
    source 14
    target 154
  ]
  edge [
    source 14
    target 155
  ]
  edge [
    source 14
    target 156
  ]
  edge [
    source 14
    target 157
  ]
  edge [
    source 14
    target 158
  ]
  edge [
    source 14
    target 120
  ]
  edge [
    source 14
    target 159
  ]
  edge [
    source 14
    target 160
  ]
  edge [
    source 14
    target 161
  ]
  edge [
    source 14
    target 162
  ]
  edge [
    source 14
    target 163
  ]
  edge [
    source 14
    target 164
  ]
  edge [
    source 14
    target 165
  ]
  edge [
    source 14
    target 166
  ]
  edge [
    source 14
    target 167
  ]
  edge [
    source 14
    target 168
  ]
  edge [
    source 14
    target 169
  ]
  edge [
    source 14
    target 170
  ]
  edge [
    source 14
    target 171
  ]
  edge [
    source 14
    target 172
  ]
  edge [
    source 14
    target 173
  ]
  edge [
    source 14
    target 174
  ]
  edge [
    source 14
    target 175
  ]
  edge [
    source 14
    target 176
  ]
  edge [
    source 14
    target 177
  ]
  edge [
    source 14
    target 178
  ]
  edge [
    source 14
    target 179
  ]
  edge [
    source 14
    target 180
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 181
  ]
  edge [
    source 15
    target 182
  ]
  edge [
    source 15
    target 183
  ]
  edge [
    source 16
    target 53
  ]
  edge [
    source 16
    target 54
  ]
  edge [
    source 16
    target 184
  ]
  edge [
    source 16
    target 185
  ]
  edge [
    source 16
    target 24
  ]
  edge [
    source 16
    target 186
  ]
  edge [
    source 16
    target 187
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 188
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 189
  ]
  edge [
    source 19
    target 190
  ]
  edge [
    source 19
    target 191
  ]
  edge [
    source 19
    target 192
  ]
  edge [
    source 19
    target 193
  ]
  edge [
    source 19
    target 194
  ]
  edge [
    source 19
    target 195
  ]
  edge [
    source 19
    target 196
  ]
  edge [
    source 19
    target 197
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 198
  ]
  edge [
    source 20
    target 199
  ]
  edge [
    source 20
    target 200
  ]
  edge [
    source 20
    target 201
  ]
  edge [
    source 20
    target 202
  ]
  edge [
    source 20
    target 203
  ]
  edge [
    source 20
    target 204
  ]
  edge [
    source 20
    target 205
  ]
  edge [
    source 20
    target 206
  ]
  edge [
    source 20
    target 207
  ]
  edge [
    source 20
    target 208
  ]
  edge [
    source 20
    target 57
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 23
  ]
  edge [
    source 21
    target 55
  ]
  edge [
    source 21
    target 56
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 30
  ]
  edge [
    source 24
    target 209
  ]
  edge [
    source 24
    target 210
  ]
  edge [
    source 24
    target 211
  ]
  edge [
    source 24
    target 212
  ]
  edge [
    source 24
    target 144
  ]
  edge [
    source 24
    target 213
  ]
  edge [
    source 24
    target 214
  ]
  edge [
    source 24
    target 215
  ]
  edge [
    source 24
    target 73
  ]
  edge [
    source 24
    target 216
  ]
  edge [
    source 24
    target 217
  ]
  edge [
    source 24
    target 218
  ]
  edge [
    source 24
    target 219
  ]
  edge [
    source 24
    target 220
  ]
  edge [
    source 24
    target 221
  ]
  edge [
    source 24
    target 222
  ]
  edge [
    source 24
    target 204
  ]
  edge [
    source 24
    target 223
  ]
  edge [
    source 24
    target 224
  ]
  edge [
    source 24
    target 225
  ]
  edge [
    source 24
    target 226
  ]
  edge [
    source 24
    target 227
  ]
  edge [
    source 24
    target 228
  ]
  edge [
    source 24
    target 229
  ]
  edge [
    source 24
    target 230
  ]
  edge [
    source 24
    target 231
  ]
  edge [
    source 24
    target 232
  ]
  edge [
    source 24
    target 233
  ]
  edge [
    source 24
    target 165
  ]
  edge [
    source 24
    target 234
  ]
  edge [
    source 24
    target 235
  ]
  edge [
    source 24
    target 236
  ]
  edge [
    source 24
    target 237
  ]
  edge [
    source 24
    target 238
  ]
  edge [
    source 24
    target 239
  ]
  edge [
    source 24
    target 240
  ]
  edge [
    source 24
    target 241
  ]
  edge [
    source 24
    target 242
  ]
  edge [
    source 24
    target 243
  ]
  edge [
    source 24
    target 244
  ]
  edge [
    source 24
    target 28
  ]
  edge [
    source 24
    target 245
  ]
  edge [
    source 24
    target 246
  ]
  edge [
    source 24
    target 247
  ]
  edge [
    source 24
    target 248
  ]
  edge [
    source 24
    target 249
  ]
  edge [
    source 24
    target 250
  ]
  edge [
    source 24
    target 40
  ]
  edge [
    source 24
    target 51
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 251
  ]
  edge [
    source 25
    target 252
  ]
  edge [
    source 25
    target 253
  ]
  edge [
    source 25
    target 254
  ]
  edge [
    source 25
    target 255
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 256
  ]
  edge [
    source 26
    target 257
  ]
  edge [
    source 26
    target 258
  ]
  edge [
    source 26
    target 259
  ]
  edge [
    source 26
    target 260
  ]
  edge [
    source 26
    target 261
  ]
  edge [
    source 26
    target 262
  ]
  edge [
    source 26
    target 263
  ]
  edge [
    source 26
    target 264
  ]
  edge [
    source 26
    target 265
  ]
  edge [
    source 26
    target 266
  ]
  edge [
    source 26
    target 28
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 267
  ]
  edge [
    source 27
    target 268
  ]
  edge [
    source 27
    target 258
  ]
  edge [
    source 27
    target 269
  ]
  edge [
    source 27
    target 270
  ]
  edge [
    source 27
    target 271
  ]
  edge [
    source 27
    target 272
  ]
  edge [
    source 27
    target 273
  ]
  edge [
    source 27
    target 274
  ]
  edge [
    source 27
    target 275
  ]
  edge [
    source 27
    target 276
  ]
  edge [
    source 27
    target 277
  ]
  edge [
    source 27
    target 278
  ]
  edge [
    source 27
    target 279
  ]
  edge [
    source 27
    target 29
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 280
  ]
  edge [
    source 29
    target 281
  ]
  edge [
    source 29
    target 282
  ]
  edge [
    source 29
    target 49
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 283
  ]
  edge [
    source 30
    target 284
  ]
  edge [
    source 30
    target 285
  ]
  edge [
    source 30
    target 286
  ]
  edge [
    source 30
    target 287
  ]
  edge [
    source 30
    target 288
  ]
  edge [
    source 30
    target 207
  ]
  edge [
    source 30
    target 289
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 290
  ]
  edge [
    source 31
    target 291
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 292
  ]
  edge [
    source 32
    target 293
  ]
  edge [
    source 32
    target 294
  ]
  edge [
    source 32
    target 295
  ]
  edge [
    source 32
    target 296
  ]
  edge [
    source 32
    target 297
  ]
  edge [
    source 32
    target 298
  ]
  edge [
    source 32
    target 299
  ]
  edge [
    source 32
    target 300
  ]
  edge [
    source 32
    target 301
  ]
  edge [
    source 32
    target 302
  ]
  edge [
    source 32
    target 303
  ]
  edge [
    source 32
    target 304
  ]
  edge [
    source 32
    target 305
  ]
  edge [
    source 32
    target 306
  ]
  edge [
    source 32
    target 307
  ]
  edge [
    source 32
    target 308
  ]
  edge [
    source 32
    target 309
  ]
  edge [
    source 32
    target 310
  ]
  edge [
    source 32
    target 311
  ]
  edge [
    source 32
    target 312
  ]
  edge [
    source 32
    target 313
  ]
  edge [
    source 32
    target 314
  ]
  edge [
    source 32
    target 315
  ]
  edge [
    source 32
    target 316
  ]
  edge [
    source 32
    target 317
  ]
  edge [
    source 32
    target 318
  ]
  edge [
    source 32
    target 319
  ]
  edge [
    source 32
    target 320
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 321
  ]
  edge [
    source 34
    target 322
  ]
  edge [
    source 34
    target 323
  ]
  edge [
    source 34
    target 324
  ]
  edge [
    source 34
    target 315
  ]
  edge [
    source 34
    target 325
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 326
  ]
  edge [
    source 35
    target 327
  ]
  edge [
    source 35
    target 328
  ]
  edge [
    source 35
    target 329
  ]
  edge [
    source 35
    target 330
  ]
  edge [
    source 35
    target 331
  ]
  edge [
    source 35
    target 332
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 333
  ]
  edge [
    source 36
    target 334
  ]
  edge [
    source 36
    target 335
  ]
  edge [
    source 36
    target 336
  ]
  edge [
    source 36
    target 337
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 338
  ]
  edge [
    source 37
    target 339
  ]
  edge [
    source 37
    target 340
  ]
  edge [
    source 37
    target 341
  ]
  edge [
    source 37
    target 342
  ]
  edge [
    source 37
    target 343
  ]
  edge [
    source 37
    target 344
  ]
  edge [
    source 37
    target 345
  ]
  edge [
    source 37
    target 346
  ]
  edge [
    source 37
    target 347
  ]
  edge [
    source 37
    target 348
  ]
  edge [
    source 37
    target 349
  ]
  edge [
    source 37
    target 350
  ]
  edge [
    source 37
    target 351
  ]
  edge [
    source 37
    target 352
  ]
  edge [
    source 37
    target 153
  ]
  edge [
    source 37
    target 353
  ]
  edge [
    source 37
    target 354
  ]
  edge [
    source 37
    target 355
  ]
  edge [
    source 37
    target 356
  ]
  edge [
    source 37
    target 357
  ]
  edge [
    source 37
    target 358
  ]
  edge [
    source 37
    target 359
  ]
  edge [
    source 37
    target 360
  ]
  edge [
    source 37
    target 361
  ]
  edge [
    source 37
    target 362
  ]
  edge [
    source 37
    target 363
  ]
  edge [
    source 37
    target 364
  ]
  edge [
    source 37
    target 71
  ]
  edge [
    source 37
    target 365
  ]
  edge [
    source 37
    target 366
  ]
  edge [
    source 37
    target 367
  ]
  edge [
    source 37
    target 368
  ]
  edge [
    source 37
    target 369
  ]
  edge [
    source 37
    target 40
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 370
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 371
  ]
  edge [
    source 40
    target 372
  ]
  edge [
    source 40
    target 373
  ]
  edge [
    source 40
    target 374
  ]
  edge [
    source 40
    target 375
  ]
  edge [
    source 40
    target 376
  ]
  edge [
    source 40
    target 71
  ]
  edge [
    source 40
    target 377
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 378
  ]
  edge [
    source 41
    target 379
  ]
  edge [
    source 41
    target 380
  ]
  edge [
    source 41
    target 381
  ]
  edge [
    source 41
    target 382
  ]
  edge [
    source 41
    target 383
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 384
  ]
  edge [
    source 42
    target 385
  ]
  edge [
    source 42
    target 386
  ]
  edge [
    source 42
    target 387
  ]
  edge [
    source 42
    target 70
  ]
  edge [
    source 42
    target 388
  ]
  edge [
    source 42
    target 389
  ]
  edge [
    source 42
    target 277
  ]
  edge [
    source 42
    target 390
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 391
  ]
  edge [
    source 44
    target 392
  ]
  edge [
    source 44
    target 393
  ]
  edge [
    source 44
    target 394
  ]
  edge [
    source 44
    target 395
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 396
  ]
  edge [
    source 45
    target 397
  ]
  edge [
    source 45
    target 398
  ]
  edge [
    source 45
    target 399
  ]
  edge [
    source 45
    target 400
  ]
  edge [
    source 45
    target 401
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 402
  ]
  edge [
    source 46
    target 403
  ]
  edge [
    source 46
    target 404
  ]
  edge [
    source 46
    target 405
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 406
  ]
  edge [
    source 51
    target 407
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 408
  ]
  edge [
    source 52
    target 409
  ]
  edge [
    source 52
    target 410
  ]
  edge [
    source 52
    target 411
  ]
  edge [
    source 52
    target 412
  ]
  edge [
    source 52
    target 413
  ]
  edge [
    source 53
    target 414
  ]
  edge [
    source 53
    target 415
  ]
  edge [
    source 53
    target 416
  ]
  edge [
    source 53
    target 417
  ]
  edge [
    source 53
    target 418
  ]
  edge [
    source 53
    target 419
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 55
    target 420
  ]
  edge [
    source 55
    target 369
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 69
  ]
  edge [
    source 57
    target 202
  ]
  edge [
    source 57
    target 207
  ]
  edge [
    source 58
    target 421
  ]
  edge [
    source 58
    target 422
  ]
  edge [
    source 58
    target 423
  ]
  edge [
    source 58
    target 424
  ]
  edge [
    source 58
    target 425
  ]
  edge [
    source 58
    target 426
  ]
  edge [
    source 58
    target 427
  ]
  edge [
    source 58
    target 428
  ]
  edge [
    source 58
    target 429
  ]
  edge [
    source 58
    target 430
  ]
  edge [
    source 58
    target 431
  ]
  edge [
    source 432
    target 433
  ]
]
