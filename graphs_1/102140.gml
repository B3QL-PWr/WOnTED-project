graph [
  maxDegree 153
  minDegree 1
  meanDegree 2.313120176405733
  density 0.0025531127774897715
  graphCliqueNumber 4
  node [
    id 0
    label "religia"
    origin "text"
  ]
  node [
    id 1
    label "nasi"
    origin "text"
  ]
  node [
    id 2
    label "przodek"
    origin "text"
  ]
  node [
    id 3
    label "wywodzi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "indoeuropejski"
    origin "text"
  ]
  node [
    id 6
    label "politeistyczny"
    origin "text"
  ]
  node [
    id 7
    label "jak"
    origin "text"
  ]
  node [
    id 8
    label "sam"
    origin "text"
  ]
  node [
    id 9
    label "nazwa"
    origin "text"
  ]
  node [
    id 10
    label "wskazywa&#263;"
    origin "text"
  ]
  node [
    id 11
    label "wierzy&#263;"
    origin "text"
  ]
  node [
    id 12
    label "wiele"
    origin "text"
  ]
  node [
    id 13
    label "r&#243;wnorz&#281;dny"
    origin "text"
  ]
  node [
    id 14
    label "b&#243;g"
    origin "text"
  ]
  node [
    id 15
    label "lub"
    origin "text"
  ]
  node [
    id 16
    label "te&#380;"
    origin "text"
  ]
  node [
    id 17
    label "jeden"
    origin "text"
  ]
  node [
    id 18
    label "g&#322;&#243;wny"
    origin "text"
  ]
  node [
    id 19
    label "reszta"
    origin "text"
  ]
  node [
    id 20
    label "jako"
    origin "text"
  ]
  node [
    id 21
    label "uosobienie"
    origin "text"
  ]
  node [
    id 22
    label "henoteizm"
    origin "text"
  ]
  node [
    id 23
    label "niekt&#243;ry"
    origin "text"
  ]
  node [
    id 24
    label "dla"
    origin "text"
  ]
  node [
    id 25
    label "dany"
    origin "text"
  ]
  node [
    id 26
    label "plemi&#281;"
    origin "text"
  ]
  node [
    id 27
    label "stal"
    origin "text"
  ]
  node [
    id 28
    label "wysoki"
    origin "text"
  ]
  node [
    id 29
    label "ranga"
    origin "text"
  ]
  node [
    id 30
    label "wzgl&#261;d"
    origin "text"
  ]
  node [
    id 31
    label "rodzaj"
    origin "text"
  ]
  node [
    id 32
    label "wykonywa&#263;"
    origin "text"
  ]
  node [
    id 33
    label "praca"
    origin "text"
  ]
  node [
    id 34
    label "rolnik"
    origin "text"
  ]
  node [
    id 35
    label "znaczenie"
    origin "text"
  ]
  node [
    id 36
    label "mie&#263;"
    origin "text"
  ]
  node [
    id 37
    label "rybak"
    origin "text"
  ]
  node [
    id 38
    label "woda"
    origin "text"
  ]
  node [
    id 39
    label "wynika&#263;"
    origin "text"
  ]
  node [
    id 40
    label "tak&#380;e"
    origin "text"
  ]
  node [
    id 41
    label "st&#261;d"
    origin "text"
  ]
  node [
    id 42
    label "fakt"
    origin "text"
  ]
  node [
    id 43
    label "boski"
    origin "text"
  ]
  node [
    id 44
    label "si&#322;a"
    origin "text"
  ]
  node [
    id 45
    label "upatrywa&#263;"
    origin "text"
  ]
  node [
    id 46
    label "przyroda"
    origin "text"
  ]
  node [
    id 47
    label "dlatego"
    origin "text"
  ]
  node [
    id 48
    label "by&#322;a"
    origin "text"
  ]
  node [
    id 49
    label "tzw"
    origin "text"
  ]
  node [
    id 50
    label "naturalny"
    origin "text"
  ]
  node [
    id 51
    label "oprze&#263;"
    origin "text"
  ]
  node [
    id 52
    label "prawo"
    origin "text"
  ]
  node [
    id 53
    label "s&#322;owianin"
    origin "text"
  ]
  node [
    id 54
    label "tyka"
    origin "text"
  ]
  node [
    id 55
    label "polak"
    origin "text"
  ]
  node [
    id 56
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 57
    label "by&#263;"
    origin "text"
  ]
  node [
    id 58
    label "blisko"
    origin "text"
  ]
  node [
    id 59
    label "po&#322;a"
    origin "text"
  ]
  node [
    id 60
    label "miliard"
    origin "text"
  ]
  node [
    id 61
    label "stan"
    origin "text"
  ]
  node [
    id 62
    label "ponad"
    origin "text"
  ]
  node [
    id 63
    label "populacja"
    origin "text"
  ]
  node [
    id 64
    label "ziemia"
    origin "text"
  ]
  node [
    id 65
    label "ci&#261;g"
    origin "text"
  ]
  node [
    id 66
    label "ostatni"
    origin "text"
  ]
  node [
    id 67
    label "tysi&#261;clecie"
    origin "text"
  ]
  node [
    id 68
    label "przesta&#322;y"
    origin "text"
  ]
  node [
    id 69
    label "nar&#243;d"
    origin "text"
  ]
  node [
    id 70
    label "podzieli&#263;"
    origin "text"
  ]
  node [
    id 71
    label "szereg"
    origin "text"
  ]
  node [
    id 72
    label "mniejszy"
    origin "text"
  ]
  node [
    id 73
    label "pocz&#261;tek"
    origin "text"
  ]
  node [
    id 74
    label "trzy"
    origin "text"
  ]
  node [
    id 75
    label "zach&#243;d"
    origin "text"
  ]
  node [
    id 76
    label "wsch&#243;d"
    origin "text"
  ]
  node [
    id 77
    label "po&#322;udnie"
    origin "text"
  ]
  node [
    id 78
    label "dzisiaj"
    origin "text"
  ]
  node [
    id 79
    label "dzia&#263;"
    origin "text"
  ]
  node [
    id 80
    label "w&#322;a&#347;nie"
    origin "text"
  ]
  node [
    id 81
    label "grupa"
    origin "text"
  ]
  node [
    id 82
    label "zachodni"
    origin "text"
  ]
  node [
    id 83
    label "nale&#380;e&#263;"
    origin "text"
  ]
  node [
    id 84
    label "czech"
    origin "text"
  ]
  node [
    id 85
    label "s&#322;owak"
    origin "text"
  ]
  node [
    id 86
    label "&#322;u&#380;yczanin"
    origin "text"
  ]
  node [
    id 87
    label "ma&#322;y"
    origin "text"
  ]
  node [
    id 88
    label "s&#322;owia&#324;ski"
    origin "text"
  ]
  node [
    id 89
    label "zamieszkiwa&#263;"
    origin "text"
  ]
  node [
    id 90
    label "&#322;u&#380;yce"
    origin "text"
  ]
  node [
    id 91
    label "niemcy"
    origin "text"
  ]
  node [
    id 92
    label "wschodni"
    origin "text"
  ]
  node [
    id 93
    label "rosjanin"
    origin "text"
  ]
  node [
    id 94
    label "ukrainiec"
    origin "text"
  ]
  node [
    id 95
    label "bia&#322;orusin"
    origin "text"
  ]
  node [
    id 96
    label "&#322;emek"
    origin "text"
  ]
  node [
    id 97
    label "po&#322;udniowy"
    origin "text"
  ]
  node [
    id 98
    label "s&#322;oweniec"
    origin "text"
  ]
  node [
    id 99
    label "chorwat"
    origin "text"
  ]
  node [
    id 100
    label "bo&#347;niak"
    origin "text"
  ]
  node [
    id 101
    label "serb"
    origin "text"
  ]
  node [
    id 102
    label "czarnog&#243;rzec"
    origin "text"
  ]
  node [
    id 103
    label "macedo&#324;czyk"
    origin "text"
  ]
  node [
    id 104
    label "bu&#322;gar"
    origin "text"
  ]
  node [
    id 105
    label "wyznanie"
  ]
  node [
    id 106
    label "mitologia"
  ]
  node [
    id 107
    label "przedmiot"
  ]
  node [
    id 108
    label "ideologia"
  ]
  node [
    id 109
    label "kosmogonia"
  ]
  node [
    id 110
    label "mistyka"
  ]
  node [
    id 111
    label "nawraca&#263;"
  ]
  node [
    id 112
    label "nawracanie_si&#281;"
  ]
  node [
    id 113
    label "duchowny"
  ]
  node [
    id 114
    label "kultura"
  ]
  node [
    id 115
    label "kultura_duchowa"
  ]
  node [
    id 116
    label "kosmologia"
  ]
  node [
    id 117
    label "zwi&#261;zek_wyznaniowy"
  ]
  node [
    id 118
    label "nawraca&#263;_si&#281;"
  ]
  node [
    id 119
    label "kult"
  ]
  node [
    id 120
    label "rela"
  ]
  node [
    id 121
    label "p&#322;ug"
  ]
  node [
    id 122
    label "linea&#380;"
  ]
  node [
    id 123
    label "ojcowie"
  ]
  node [
    id 124
    label "antecesor"
  ]
  node [
    id 125
    label "krewny"
  ]
  node [
    id 126
    label "w&#243;z"
  ]
  node [
    id 127
    label "chodnik"
  ]
  node [
    id 128
    label "dziad"
  ]
  node [
    id 129
    label "post&#281;p"
  ]
  node [
    id 130
    label "wyrobisko"
  ]
  node [
    id 131
    label "uzasadnia&#263;"
  ]
  node [
    id 132
    label "wodzi&#263;"
  ]
  node [
    id 133
    label "wnioskowa&#263;"
  ]
  node [
    id 134
    label "zabiera&#263;"
  ]
  node [
    id 135
    label "stwierdza&#263;"
  ]
  node [
    id 136
    label "gaworzy&#263;"
  ]
  node [
    id 137
    label "chant"
  ]
  node [
    id 138
    label "powodowa&#263;"
  ]
  node [
    id 139
    label "condescend"
  ]
  node [
    id 140
    label "indogerma&#324;ski"
  ]
  node [
    id 141
    label "byd&#322;o"
  ]
  node [
    id 142
    label "zobo"
  ]
  node [
    id 143
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 144
    label "yakalo"
  ]
  node [
    id 145
    label "dzo"
  ]
  node [
    id 146
    label "sklep"
  ]
  node [
    id 147
    label "term"
  ]
  node [
    id 148
    label "wezwanie"
  ]
  node [
    id 149
    label "leksem"
  ]
  node [
    id 150
    label "patron"
  ]
  node [
    id 151
    label "pokazywa&#263;"
  ]
  node [
    id 152
    label "warto&#347;&#263;"
  ]
  node [
    id 153
    label "set"
  ]
  node [
    id 154
    label "represent"
  ]
  node [
    id 155
    label "indicate"
  ]
  node [
    id 156
    label "wyraz"
  ]
  node [
    id 157
    label "wybiera&#263;"
  ]
  node [
    id 158
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 159
    label "podawa&#263;"
  ]
  node [
    id 160
    label "signify"
  ]
  node [
    id 161
    label "podkre&#347;la&#263;"
  ]
  node [
    id 162
    label "czu&#263;"
  ]
  node [
    id 163
    label "chowa&#263;"
  ]
  node [
    id 164
    label "wierza&#263;"
  ]
  node [
    id 165
    label "powierzy&#263;"
  ]
  node [
    id 166
    label "powierza&#263;"
  ]
  node [
    id 167
    label "faith"
  ]
  node [
    id 168
    label "uznawa&#263;"
  ]
  node [
    id 169
    label "trust"
  ]
  node [
    id 170
    label "wyznawa&#263;"
  ]
  node [
    id 171
    label "nadzieja"
  ]
  node [
    id 172
    label "wiela"
  ]
  node [
    id 173
    label "du&#380;y"
  ]
  node [
    id 174
    label "jednakowy"
  ]
  node [
    id 175
    label "r&#243;wnorz&#281;dnie"
  ]
  node [
    id 176
    label "Dionizos"
  ]
  node [
    id 177
    label "Neptun"
  ]
  node [
    id 178
    label "Hesperos"
  ]
  node [
    id 179
    label "ba&#322;wan"
  ]
  node [
    id 180
    label "niebiosa"
  ]
  node [
    id 181
    label "Ereb"
  ]
  node [
    id 182
    label "Sylen"
  ]
  node [
    id 183
    label "uwielbienie"
  ]
  node [
    id 184
    label "s&#261;d_ostateczny"
  ]
  node [
    id 185
    label "idol"
  ]
  node [
    id 186
    label "Bachus"
  ]
  node [
    id 187
    label "ofiarowa&#263;"
  ]
  node [
    id 188
    label "tr&#243;jca"
  ]
  node [
    id 189
    label "Waruna"
  ]
  node [
    id 190
    label "ofiarowanie"
  ]
  node [
    id 191
    label "igrzyska_greckie"
  ]
  node [
    id 192
    label "Janus"
  ]
  node [
    id 193
    label "Kupidyn"
  ]
  node [
    id 194
    label "ofiarowywanie"
  ]
  node [
    id 195
    label "osoba"
  ]
  node [
    id 196
    label "gigant"
  ]
  node [
    id 197
    label "Boreasz"
  ]
  node [
    id 198
    label "politeizm"
  ]
  node [
    id 199
    label "istota_nadprzyrodzona"
  ]
  node [
    id 200
    label "ofiarowywa&#263;"
  ]
  node [
    id 201
    label "Posejdon"
  ]
  node [
    id 202
    label "kieliszek"
  ]
  node [
    id 203
    label "shot"
  ]
  node [
    id 204
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 205
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 206
    label "jaki&#347;"
  ]
  node [
    id 207
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 208
    label "jednolicie"
  ]
  node [
    id 209
    label "w&#243;dka"
  ]
  node [
    id 210
    label "ten"
  ]
  node [
    id 211
    label "ujednolicenie"
  ]
  node [
    id 212
    label "najwa&#380;niejszy"
  ]
  node [
    id 213
    label "g&#322;&#243;wnie"
  ]
  node [
    id 214
    label "remainder"
  ]
  node [
    id 215
    label "wydanie"
  ]
  node [
    id 216
    label "wyda&#263;"
  ]
  node [
    id 217
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 218
    label "wydawa&#263;"
  ]
  node [
    id 219
    label "pozosta&#322;y"
  ]
  node [
    id 220
    label "kwota"
  ]
  node [
    id 221
    label "model"
  ]
  node [
    id 222
    label "wierzenie"
  ]
  node [
    id 223
    label "okre&#347;lony"
  ]
  node [
    id 224
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 225
    label "Kipczacy"
  ]
  node [
    id 226
    label "Do&#322;ganie"
  ]
  node [
    id 227
    label "Nawahowie"
  ]
  node [
    id 228
    label "Wizygoci"
  ]
  node [
    id 229
    label "Paleoazjaci"
  ]
  node [
    id 230
    label "Indoariowie"
  ]
  node [
    id 231
    label "Macziguengowie"
  ]
  node [
    id 232
    label "Tocharowie"
  ]
  node [
    id 233
    label "Antowie"
  ]
  node [
    id 234
    label "Kumbrowie"
  ]
  node [
    id 235
    label "Polanie"
  ]
  node [
    id 236
    label "Indoira&#324;czycy"
  ]
  node [
    id 237
    label "moiety"
  ]
  node [
    id 238
    label "Drzewianie"
  ]
  node [
    id 239
    label "Kozacy"
  ]
  node [
    id 240
    label "rodzina"
  ]
  node [
    id 241
    label "Negryci"
  ]
  node [
    id 242
    label "Nogajowie"
  ]
  node [
    id 243
    label "Obodryci"
  ]
  node [
    id 244
    label "Wenedowie"
  ]
  node [
    id 245
    label "Dogonowie"
  ]
  node [
    id 246
    label "Retowie"
  ]
  node [
    id 247
    label "Po&#322;owcy"
  ]
  node [
    id 248
    label "fratria"
  ]
  node [
    id 249
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 250
    label "Tagalowie"
  ]
  node [
    id 251
    label "szczep"
  ]
  node [
    id 252
    label "Majowie"
  ]
  node [
    id 253
    label "Frygijczycy"
  ]
  node [
    id 254
    label "Maroni"
  ]
  node [
    id 255
    label "jednostka_systematyczna"
  ]
  node [
    id 256
    label "lud"
  ]
  node [
    id 257
    label "Ladynowie"
  ]
  node [
    id 258
    label "Ugrowie"
  ]
  node [
    id 259
    label "Achajowie"
  ]
  node [
    id 260
    label "stop"
  ]
  node [
    id 261
    label "metal"
  ]
  node [
    id 262
    label "steel"
  ]
  node [
    id 263
    label "austenityzowanie"
  ]
  node [
    id 264
    label "warto&#347;ciowy"
  ]
  node [
    id 265
    label "wysoce"
  ]
  node [
    id 266
    label "daleki"
  ]
  node [
    id 267
    label "znaczny"
  ]
  node [
    id 268
    label "wysoko"
  ]
  node [
    id 269
    label "szczytnie"
  ]
  node [
    id 270
    label "wznios&#322;y"
  ]
  node [
    id 271
    label "wyrafinowany"
  ]
  node [
    id 272
    label "z_wysoka"
  ]
  node [
    id 273
    label "chwalebny"
  ]
  node [
    id 274
    label "uprzywilejowany"
  ]
  node [
    id 275
    label "niepo&#347;ledni"
  ]
  node [
    id 276
    label "stopie&#324;_wojskowy"
  ]
  node [
    id 277
    label "degree"
  ]
  node [
    id 278
    label "stopie&#324;_s&#322;u&#380;bowy"
  ]
  node [
    id 279
    label "status"
  ]
  node [
    id 280
    label "numer"
  ]
  node [
    id 281
    label "przyczyna"
  ]
  node [
    id 282
    label "uwaga"
  ]
  node [
    id 283
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 284
    label "punkt_widzenia"
  ]
  node [
    id 285
    label "kategoria_gramatyczna"
  ]
  node [
    id 286
    label "autorament"
  ]
  node [
    id 287
    label "fashion"
  ]
  node [
    id 288
    label "variety"
  ]
  node [
    id 289
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 290
    label "work"
  ]
  node [
    id 291
    label "robi&#263;"
  ]
  node [
    id 292
    label "muzyka"
  ]
  node [
    id 293
    label "rola"
  ]
  node [
    id 294
    label "create"
  ]
  node [
    id 295
    label "wytwarza&#263;"
  ]
  node [
    id 296
    label "stosunek_pracy"
  ]
  node [
    id 297
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 298
    label "benedykty&#324;ski"
  ]
  node [
    id 299
    label "pracowanie"
  ]
  node [
    id 300
    label "zaw&#243;d"
  ]
  node [
    id 301
    label "kierownictwo"
  ]
  node [
    id 302
    label "zmiana"
  ]
  node [
    id 303
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 304
    label "wytw&#243;r"
  ]
  node [
    id 305
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 306
    label "tynkarski"
  ]
  node [
    id 307
    label "czynnik_produkcji"
  ]
  node [
    id 308
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 309
    label "zobowi&#261;zanie"
  ]
  node [
    id 310
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 311
    label "czynno&#347;&#263;"
  ]
  node [
    id 312
    label "tyrka"
  ]
  node [
    id 313
    label "pracowa&#263;"
  ]
  node [
    id 314
    label "siedziba"
  ]
  node [
    id 315
    label "poda&#380;_pracy"
  ]
  node [
    id 316
    label "miejsce"
  ]
  node [
    id 317
    label "zak&#322;ad"
  ]
  node [
    id 318
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 319
    label "najem"
  ]
  node [
    id 320
    label "wie&#347;niak"
  ]
  node [
    id 321
    label "specjalista"
  ]
  node [
    id 322
    label "gravity"
  ]
  node [
    id 323
    label "okre&#347;lanie"
  ]
  node [
    id 324
    label "liczenie"
  ]
  node [
    id 325
    label "odgrywanie_roli"
  ]
  node [
    id 326
    label "wskazywanie"
  ]
  node [
    id 327
    label "bycie"
  ]
  node [
    id 328
    label "weight"
  ]
  node [
    id 329
    label "command"
  ]
  node [
    id 330
    label "istota"
  ]
  node [
    id 331
    label "cecha"
  ]
  node [
    id 332
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 333
    label "informacja"
  ]
  node [
    id 334
    label "odk&#322;adanie"
  ]
  node [
    id 335
    label "wyra&#380;enie"
  ]
  node [
    id 336
    label "assay"
  ]
  node [
    id 337
    label "condition"
  ]
  node [
    id 338
    label "kto&#347;"
  ]
  node [
    id 339
    label "stawianie"
  ]
  node [
    id 340
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 341
    label "need"
  ]
  node [
    id 342
    label "hide"
  ]
  node [
    id 343
    label "support"
  ]
  node [
    id 344
    label "cz&#322;owiek"
  ]
  node [
    id 345
    label "rybiarz"
  ]
  node [
    id 346
    label "&#322;owi&#263;"
  ]
  node [
    id 347
    label "wypowied&#378;"
  ]
  node [
    id 348
    label "obiekt_naturalny"
  ]
  node [
    id 349
    label "bicie"
  ]
  node [
    id 350
    label "wysi&#281;k"
  ]
  node [
    id 351
    label "pustka"
  ]
  node [
    id 352
    label "woda_s&#322;odka"
  ]
  node [
    id 353
    label "p&#322;ycizna"
  ]
  node [
    id 354
    label "ciecz"
  ]
  node [
    id 355
    label "spi&#281;trza&#263;"
  ]
  node [
    id 356
    label "uj&#281;cie_wody"
  ]
  node [
    id 357
    label "chlasta&#263;"
  ]
  node [
    id 358
    label "spi&#281;trzy&#263;"
  ]
  node [
    id 359
    label "nap&#243;j"
  ]
  node [
    id 360
    label "bombast"
  ]
  node [
    id 361
    label "water"
  ]
  node [
    id 362
    label "kryptodepresja"
  ]
  node [
    id 363
    label "wodnik"
  ]
  node [
    id 364
    label "pojazd"
  ]
  node [
    id 365
    label "&#347;r&#243;dl&#261;dowy"
  ]
  node [
    id 366
    label "fala"
  ]
  node [
    id 367
    label "nap&#243;j_bezalkoholowy"
  ]
  node [
    id 368
    label "zrzut"
  ]
  node [
    id 369
    label "dotleni&#263;"
  ]
  node [
    id 370
    label "utylizator"
  ]
  node [
    id 371
    label "uci&#261;g"
  ]
  node [
    id 372
    label "wybrze&#380;e"
  ]
  node [
    id 373
    label "nabranie"
  ]
  node [
    id 374
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 375
    label "klarownik"
  ]
  node [
    id 376
    label "chlastanie"
  ]
  node [
    id 377
    label "przybrze&#380;e"
  ]
  node [
    id 378
    label "deklamacja"
  ]
  node [
    id 379
    label "spi&#281;trzenie"
  ]
  node [
    id 380
    label "przybieranie"
  ]
  node [
    id 381
    label "nabra&#263;"
  ]
  node [
    id 382
    label "tlenek"
  ]
  node [
    id 383
    label "spi&#281;trzanie"
  ]
  node [
    id 384
    label "l&#243;d"
  ]
  node [
    id 385
    label "okazywa&#263;_si&#281;"
  ]
  node [
    id 386
    label "rise"
  ]
  node [
    id 387
    label "appear"
  ]
  node [
    id 388
    label "rodzi&#263;_si&#281;"
  ]
  node [
    id 389
    label "wydarzenie"
  ]
  node [
    id 390
    label "bia&#322;e_plamy"
  ]
  node [
    id 391
    label "udany"
  ]
  node [
    id 392
    label "arcypi&#281;kny"
  ]
  node [
    id 393
    label "bezpretensjonalny"
  ]
  node [
    id 394
    label "fantastyczny"
  ]
  node [
    id 395
    label "wyj&#261;tkowy"
  ]
  node [
    id 396
    label "cudnie"
  ]
  node [
    id 397
    label "cudowny"
  ]
  node [
    id 398
    label "wielki"
  ]
  node [
    id 399
    label "wspania&#322;y"
  ]
  node [
    id 400
    label "bosko"
  ]
  node [
    id 401
    label "nadprzyrodzony"
  ]
  node [
    id 402
    label "&#347;mieszny"
  ]
  node [
    id 403
    label "nale&#380;ny"
  ]
  node [
    id 404
    label "wojsko"
  ]
  node [
    id 405
    label "magnitude"
  ]
  node [
    id 406
    label "energia"
  ]
  node [
    id 407
    label "capacity"
  ]
  node [
    id 408
    label "wuchta"
  ]
  node [
    id 409
    label "parametr"
  ]
  node [
    id 410
    label "moment_si&#322;y"
  ]
  node [
    id 411
    label "przemoc"
  ]
  node [
    id 412
    label "zdolno&#347;&#263;"
  ]
  node [
    id 413
    label "mn&#243;stwo"
  ]
  node [
    id 414
    label "wektorowa_wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 415
    label "rozwi&#261;zanie"
  ]
  node [
    id 416
    label "potencja"
  ]
  node [
    id 417
    label "zjawisko"
  ]
  node [
    id 418
    label "zaleta"
  ]
  node [
    id 419
    label "zauwa&#380;a&#263;"
  ]
  node [
    id 420
    label "biota"
  ]
  node [
    id 421
    label "wszechstworzenie"
  ]
  node [
    id 422
    label "Ziemia"
  ]
  node [
    id 423
    label "fauna"
  ]
  node [
    id 424
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 425
    label "stw&#243;r"
  ]
  node [
    id 426
    label "ekosystem"
  ]
  node [
    id 427
    label "rzecz"
  ]
  node [
    id 428
    label "teren"
  ]
  node [
    id 429
    label "environment"
  ]
  node [
    id 430
    label "przyra"
  ]
  node [
    id 431
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 432
    label "mikrokosmos"
  ]
  node [
    id 433
    label "partnerka"
  ]
  node [
    id 434
    label "szczery"
  ]
  node [
    id 435
    label "prawy"
  ]
  node [
    id 436
    label "bezsporny"
  ]
  node [
    id 437
    label "organicznie"
  ]
  node [
    id 438
    label "immanentny"
  ]
  node [
    id 439
    label "naturalnie"
  ]
  node [
    id 440
    label "zwyczajny"
  ]
  node [
    id 441
    label "neutralny"
  ]
  node [
    id 442
    label "zrozumia&#322;y"
  ]
  node [
    id 443
    label "rzeczywisty"
  ]
  node [
    id 444
    label "normalny"
  ]
  node [
    id 445
    label "pierwotny"
  ]
  node [
    id 446
    label "establish"
  ]
  node [
    id 447
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 448
    label "recline"
  ]
  node [
    id 449
    label "podstawa"
  ]
  node [
    id 450
    label "ustawi&#263;"
  ]
  node [
    id 451
    label "osnowa&#263;"
  ]
  node [
    id 452
    label "obserwacja"
  ]
  node [
    id 453
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 454
    label "nauka_prawa"
  ]
  node [
    id 455
    label "dominion"
  ]
  node [
    id 456
    label "normatywizm"
  ]
  node [
    id 457
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 458
    label "qualification"
  ]
  node [
    id 459
    label "opis"
  ]
  node [
    id 460
    label "regu&#322;a_Allena"
  ]
  node [
    id 461
    label "normalizacja"
  ]
  node [
    id 462
    label "kazuistyka"
  ]
  node [
    id 463
    label "regu&#322;a_Glogera"
  ]
  node [
    id 464
    label "prawo_karne"
  ]
  node [
    id 465
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 466
    label "standard"
  ]
  node [
    id 467
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 468
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 469
    label "struktura"
  ]
  node [
    id 470
    label "szko&#322;a"
  ]
  node [
    id 471
    label "prawo_karne_procesowe"
  ]
  node [
    id 472
    label "prawo_Mendla"
  ]
  node [
    id 473
    label "przepis"
  ]
  node [
    id 474
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 475
    label "criterion"
  ]
  node [
    id 476
    label "kanonistyka"
  ]
  node [
    id 477
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 478
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 479
    label "wykonawczy"
  ]
  node [
    id 480
    label "twierdzenie"
  ]
  node [
    id 481
    label "judykatura"
  ]
  node [
    id 482
    label "legislacyjnie"
  ]
  node [
    id 483
    label "umocowa&#263;"
  ]
  node [
    id 484
    label "podmiot"
  ]
  node [
    id 485
    label "procesualistyka"
  ]
  node [
    id 486
    label "kierunek"
  ]
  node [
    id 487
    label "kryminologia"
  ]
  node [
    id 488
    label "kryminalistyka"
  ]
  node [
    id 489
    label "cywilistyka"
  ]
  node [
    id 490
    label "law"
  ]
  node [
    id 491
    label "zasada_d'Alemberta"
  ]
  node [
    id 492
    label "jurisprudence"
  ]
  node [
    id 493
    label "zasada"
  ]
  node [
    id 494
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 495
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 496
    label "patyk"
  ]
  node [
    id 497
    label "znak_nawigacyjny"
  ]
  node [
    id 498
    label "poro&#380;e"
  ]
  node [
    id 499
    label "polski"
  ]
  node [
    id 500
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 501
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 502
    label "obszar"
  ]
  node [
    id 503
    label "biosfera"
  ]
  node [
    id 504
    label "Stary_&#346;wiat"
  ]
  node [
    id 505
    label "magnetosfera"
  ]
  node [
    id 506
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 507
    label "Nowy_&#346;wiat"
  ]
  node [
    id 508
    label "geosfera"
  ]
  node [
    id 509
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 510
    label "planeta"
  ]
  node [
    id 511
    label "przejmowa&#263;"
  ]
  node [
    id 512
    label "litosfera"
  ]
  node [
    id 513
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 514
    label "makrokosmos"
  ]
  node [
    id 515
    label "barysfera"
  ]
  node [
    id 516
    label "p&#243;&#322;noc"
  ]
  node [
    id 517
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 518
    label "geotermia"
  ]
  node [
    id 519
    label "biegun"
  ]
  node [
    id 520
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 521
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 522
    label "p&#243;&#322;kula"
  ]
  node [
    id 523
    label "atmosfera"
  ]
  node [
    id 524
    label "class"
  ]
  node [
    id 525
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 526
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 527
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 528
    label "przejmowanie"
  ]
  node [
    id 529
    label "przestrze&#324;"
  ]
  node [
    id 530
    label "asymilowanie_si&#281;"
  ]
  node [
    id 531
    label "przej&#261;&#263;"
  ]
  node [
    id 532
    label "ekosfera"
  ]
  node [
    id 533
    label "ciemna_materia"
  ]
  node [
    id 534
    label "geoida"
  ]
  node [
    id 535
    label "Wsch&#243;d"
  ]
  node [
    id 536
    label "populace"
  ]
  node [
    id 537
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 538
    label "huczek"
  ]
  node [
    id 539
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 540
    label "universe"
  ]
  node [
    id 541
    label "ozonosfera"
  ]
  node [
    id 542
    label "rze&#378;ba"
  ]
  node [
    id 543
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 544
    label "zagranica"
  ]
  node [
    id 545
    label "hydrosfera"
  ]
  node [
    id 546
    label "kuchnia"
  ]
  node [
    id 547
    label "przej&#281;cie"
  ]
  node [
    id 548
    label "czarna_dziura"
  ]
  node [
    id 549
    label "morze"
  ]
  node [
    id 550
    label "si&#281;ga&#263;"
  ]
  node [
    id 551
    label "trwa&#263;"
  ]
  node [
    id 552
    label "obecno&#347;&#263;"
  ]
  node [
    id 553
    label "stand"
  ]
  node [
    id 554
    label "mie&#263;_miejsce"
  ]
  node [
    id 555
    label "uczestniczy&#263;"
  ]
  node [
    id 556
    label "chodzi&#263;"
  ]
  node [
    id 557
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 558
    label "equal"
  ]
  node [
    id 559
    label "dok&#322;adnie"
  ]
  node [
    id 560
    label "bliski"
  ]
  node [
    id 561
    label "silnie"
  ]
  node [
    id 562
    label "tu&#380;_tu&#380;"
  ]
  node [
    id 563
    label "str&#243;j"
  ]
  node [
    id 564
    label "fragment"
  ]
  node [
    id 565
    label "liczba"
  ]
  node [
    id 566
    label "Arizona"
  ]
  node [
    id 567
    label "Georgia"
  ]
  node [
    id 568
    label "warstwa"
  ]
  node [
    id 569
    label "jednostka_administracyjna"
  ]
  node [
    id 570
    label "Goa"
  ]
  node [
    id 571
    label "Hawaje"
  ]
  node [
    id 572
    label "Floryda"
  ]
  node [
    id 573
    label "Oklahoma"
  ]
  node [
    id 574
    label "punkt"
  ]
  node [
    id 575
    label "Alaska"
  ]
  node [
    id 576
    label "Alabama"
  ]
  node [
    id 577
    label "wci&#281;cie"
  ]
  node [
    id 578
    label "Oregon"
  ]
  node [
    id 579
    label "poziom"
  ]
  node [
    id 580
    label "Teksas"
  ]
  node [
    id 581
    label "Illinois"
  ]
  node [
    id 582
    label "Jukatan"
  ]
  node [
    id 583
    label "Waszyngton"
  ]
  node [
    id 584
    label "shape"
  ]
  node [
    id 585
    label "Nowy_Meksyk"
  ]
  node [
    id 586
    label "ilo&#347;&#263;"
  ]
  node [
    id 587
    label "state"
  ]
  node [
    id 588
    label "Nowy_York"
  ]
  node [
    id 589
    label "Arakan"
  ]
  node [
    id 590
    label "Kalifornia"
  ]
  node [
    id 591
    label "wektor"
  ]
  node [
    id 592
    label "Massachusetts"
  ]
  node [
    id 593
    label "Pensylwania"
  ]
  node [
    id 594
    label "Maryland"
  ]
  node [
    id 595
    label "Michigan"
  ]
  node [
    id 596
    label "Ohio"
  ]
  node [
    id 597
    label "Kansas"
  ]
  node [
    id 598
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 599
    label "Luizjana"
  ]
  node [
    id 600
    label "samopoczucie"
  ]
  node [
    id 601
    label "Wirginia"
  ]
  node [
    id 602
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 603
    label "ch&#322;opstwo"
  ]
  node [
    id 604
    label "innowierstwo"
  ]
  node [
    id 605
    label "zbi&#243;r"
  ]
  node [
    id 606
    label "grupa_organizm&#243;w"
  ]
  node [
    id 607
    label "Skandynawia"
  ]
  node [
    id 608
    label "Yorkshire"
  ]
  node [
    id 609
    label "Kaukaz"
  ]
  node [
    id 610
    label "Kaszmir"
  ]
  node [
    id 611
    label "Toskania"
  ]
  node [
    id 612
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 613
    label "&#321;emkowszczyzna"
  ]
  node [
    id 614
    label "Amhara"
  ]
  node [
    id 615
    label "Lombardia"
  ]
  node [
    id 616
    label "Podbeskidzie"
  ]
  node [
    id 617
    label "Kalabria"
  ]
  node [
    id 618
    label "kort"
  ]
  node [
    id 619
    label "Tyrol"
  ]
  node [
    id 620
    label "Pamir"
  ]
  node [
    id 621
    label "Lubelszczyzna"
  ]
  node [
    id 622
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 623
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 624
    label "&#379;ywiecczyzna"
  ]
  node [
    id 625
    label "ryzosfera"
  ]
  node [
    id 626
    label "Europa_Wschodnia"
  ]
  node [
    id 627
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 628
    label "Zabajkale"
  ]
  node [
    id 629
    label "Kaszuby"
  ]
  node [
    id 630
    label "Bo&#347;nia"
  ]
  node [
    id 631
    label "Noworosja"
  ]
  node [
    id 632
    label "Ba&#322;kany"
  ]
  node [
    id 633
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 634
    label "Anglia"
  ]
  node [
    id 635
    label "Kielecczyzna"
  ]
  node [
    id 636
    label "Pomorze_Zachodnie"
  ]
  node [
    id 637
    label "Opolskie"
  ]
  node [
    id 638
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 639
    label "skorupa_ziemska"
  ]
  node [
    id 640
    label "Ko&#322;yma"
  ]
  node [
    id 641
    label "Oksytania"
  ]
  node [
    id 642
    label "Syjon"
  ]
  node [
    id 643
    label "posadzka"
  ]
  node [
    id 644
    label "pa&#324;stwo"
  ]
  node [
    id 645
    label "Kociewie"
  ]
  node [
    id 646
    label "Huculszczyzna"
  ]
  node [
    id 647
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 648
    label "budynek"
  ]
  node [
    id 649
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 650
    label "Bawaria"
  ]
  node [
    id 651
    label "pomieszczenie"
  ]
  node [
    id 652
    label "pr&#243;chnica"
  ]
  node [
    id 653
    label "glinowanie"
  ]
  node [
    id 654
    label "Maghreb"
  ]
  node [
    id 655
    label "Bory_Tucholskie"
  ]
  node [
    id 656
    label "Europa_Zachodnia"
  ]
  node [
    id 657
    label "Kerala"
  ]
  node [
    id 658
    label "Podhale"
  ]
  node [
    id 659
    label "Kabylia"
  ]
  node [
    id 660
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 661
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 662
    label "Ma&#322;opolska"
  ]
  node [
    id 663
    label "Polesie"
  ]
  node [
    id 664
    label "Liguria"
  ]
  node [
    id 665
    label "&#321;&#243;dzkie"
  ]
  node [
    id 666
    label "geosystem"
  ]
  node [
    id 667
    label "Palestyna"
  ]
  node [
    id 668
    label "Bojkowszczyzna"
  ]
  node [
    id 669
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 670
    label "Karaiby"
  ]
  node [
    id 671
    label "S&#261;decczyzna"
  ]
  node [
    id 672
    label "Sand&#380;ak"
  ]
  node [
    id 673
    label "Nadrenia"
  ]
  node [
    id 674
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 675
    label "Zakarpacie"
  ]
  node [
    id 676
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 677
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 678
    label "Zag&#243;rze"
  ]
  node [
    id 679
    label "Andaluzja"
  ]
  node [
    id 680
    label "Turkiestan"
  ]
  node [
    id 681
    label "Naddniestrze"
  ]
  node [
    id 682
    label "Hercegowina"
  ]
  node [
    id 683
    label "p&#322;aszczyzna"
  ]
  node [
    id 684
    label "Opolszczyzna"
  ]
  node [
    id 685
    label "Lotaryngia"
  ]
  node [
    id 686
    label "Afryka_Wschodnia"
  ]
  node [
    id 687
    label "Szlezwik"
  ]
  node [
    id 688
    label "powierzchnia"
  ]
  node [
    id 689
    label "glinowa&#263;"
  ]
  node [
    id 690
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 691
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 692
    label "podglebie"
  ]
  node [
    id 693
    label "Mazowsze"
  ]
  node [
    id 694
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 695
    label "Afryka_Zachodnia"
  ]
  node [
    id 696
    label "Galicja"
  ]
  node [
    id 697
    label "Szkocja"
  ]
  node [
    id 698
    label "Walia"
  ]
  node [
    id 699
    label "Powi&#347;le"
  ]
  node [
    id 700
    label "penetrator"
  ]
  node [
    id 701
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 702
    label "kompleks_sorpcyjny"
  ]
  node [
    id 703
    label "Zamojszczyzna"
  ]
  node [
    id 704
    label "Kujawy"
  ]
  node [
    id 705
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 706
    label "Podlasie"
  ]
  node [
    id 707
    label "Laponia"
  ]
  node [
    id 708
    label "Umbria"
  ]
  node [
    id 709
    label "plantowa&#263;"
  ]
  node [
    id 710
    label "Mezoameryka"
  ]
  node [
    id 711
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 712
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 713
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 714
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 715
    label "Kurdystan"
  ]
  node [
    id 716
    label "Kampania"
  ]
  node [
    id 717
    label "Armagnac"
  ]
  node [
    id 718
    label "Polinezja"
  ]
  node [
    id 719
    label "Warmia"
  ]
  node [
    id 720
    label "Wielkopolska"
  ]
  node [
    id 721
    label "Bordeaux"
  ]
  node [
    id 722
    label "Lauda"
  ]
  node [
    id 723
    label "Mazury"
  ]
  node [
    id 724
    label "Podkarpacie"
  ]
  node [
    id 725
    label "Oceania"
  ]
  node [
    id 726
    label "Lasko"
  ]
  node [
    id 727
    label "Amazonia"
  ]
  node [
    id 728
    label "glej"
  ]
  node [
    id 729
    label "martwica"
  ]
  node [
    id 730
    label "zapadnia"
  ]
  node [
    id 731
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 732
    label "Kurpie"
  ]
  node [
    id 733
    label "Tonkin"
  ]
  node [
    id 734
    label "Azja_Wschodnia"
  ]
  node [
    id 735
    label "Mikronezja"
  ]
  node [
    id 736
    label "Ukraina_Zachodnia"
  ]
  node [
    id 737
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 738
    label "Turyngia"
  ]
  node [
    id 739
    label "Baszkiria"
  ]
  node [
    id 740
    label "Apulia"
  ]
  node [
    id 741
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 742
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 743
    label "Indochiny"
  ]
  node [
    id 744
    label "Biskupizna"
  ]
  node [
    id 745
    label "Lubuskie"
  ]
  node [
    id 746
    label "domain"
  ]
  node [
    id 747
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 748
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 749
    label "lina"
  ]
  node [
    id 750
    label "way"
  ]
  node [
    id 751
    label "cable"
  ]
  node [
    id 752
    label "przebieg"
  ]
  node [
    id 753
    label "ch&#243;d"
  ]
  node [
    id 754
    label "trasa"
  ]
  node [
    id 755
    label "rz&#261;d"
  ]
  node [
    id 756
    label "k&#322;us"
  ]
  node [
    id 757
    label "progression"
  ]
  node [
    id 758
    label "current"
  ]
  node [
    id 759
    label "pr&#261;d"
  ]
  node [
    id 760
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 761
    label "lot"
  ]
  node [
    id 762
    label "kolejny"
  ]
  node [
    id 763
    label "istota_&#380;ywa"
  ]
  node [
    id 764
    label "najgorszy"
  ]
  node [
    id 765
    label "aktualny"
  ]
  node [
    id 766
    label "ostatnio"
  ]
  node [
    id 767
    label "niedawno"
  ]
  node [
    id 768
    label "wi&#261;&#380;&#261;cy"
  ]
  node [
    id 769
    label "sko&#324;czony"
  ]
  node [
    id 770
    label "poprzedni"
  ]
  node [
    id 771
    label "w&#261;tpliwy"
  ]
  node [
    id 772
    label "jubileusz"
  ]
  node [
    id 773
    label "czas"
  ]
  node [
    id 774
    label "przejrza&#322;y"
  ]
  node [
    id 775
    label "nie&#347;wie&#380;y"
  ]
  node [
    id 776
    label "przeterminowany"
  ]
  node [
    id 777
    label "Samojedzi"
  ]
  node [
    id 778
    label "nacja"
  ]
  node [
    id 779
    label "Aztekowie"
  ]
  node [
    id 780
    label "Irokezi"
  ]
  node [
    id 781
    label "Buriaci"
  ]
  node [
    id 782
    label "Komancze"
  ]
  node [
    id 783
    label "t&#322;um"
  ]
  node [
    id 784
    label "ludno&#347;&#263;"
  ]
  node [
    id 785
    label "Siuksowie"
  ]
  node [
    id 786
    label "Czejenowie"
  ]
  node [
    id 787
    label "Wotiacy"
  ]
  node [
    id 788
    label "Baszkirzy"
  ]
  node [
    id 789
    label "mniejszo&#347;&#263;_narodowa"
  ]
  node [
    id 790
    label "Mohikanie"
  ]
  node [
    id 791
    label "Apacze"
  ]
  node [
    id 792
    label "Syngalezi"
  ]
  node [
    id 793
    label "transgress"
  ]
  node [
    id 794
    label "impart"
  ]
  node [
    id 795
    label "spowodowa&#263;"
  ]
  node [
    id 796
    label "distribute"
  ]
  node [
    id 797
    label "wydzieli&#263;"
  ]
  node [
    id 798
    label "divide"
  ]
  node [
    id 799
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 800
    label "przyzna&#263;"
  ]
  node [
    id 801
    label "policzy&#263;"
  ]
  node [
    id 802
    label "pigeonhole"
  ]
  node [
    id 803
    label "exchange"
  ]
  node [
    id 804
    label "rozda&#263;"
  ]
  node [
    id 805
    label "sk&#322;&#243;ci&#263;"
  ]
  node [
    id 806
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 807
    label "zrobi&#263;"
  ]
  node [
    id 808
    label "change"
  ]
  node [
    id 809
    label "koniec"
  ]
  node [
    id 810
    label "unit"
  ]
  node [
    id 811
    label "uporz&#261;dkowanie"
  ]
  node [
    id 812
    label "szpaler"
  ]
  node [
    id 813
    label "tract"
  ]
  node [
    id 814
    label "rozmieszczenie"
  ]
  node [
    id 815
    label "column"
  ]
  node [
    id 816
    label "inny"
  ]
  node [
    id 817
    label "zmniejszenie_si&#281;"
  ]
  node [
    id 818
    label "zmniejszanie_si&#281;"
  ]
  node [
    id 819
    label "chudni&#281;cie"
  ]
  node [
    id 820
    label "zmniejszanie"
  ]
  node [
    id 821
    label "faza"
  ]
  node [
    id 822
    label "upgrade"
  ]
  node [
    id 823
    label "pierworodztwo"
  ]
  node [
    id 824
    label "nast&#281;pstwo"
  ]
  node [
    id 825
    label "s&#322;o&#324;ce"
  ]
  node [
    id 826
    label "usi&#322;owanie"
  ]
  node [
    id 827
    label "trud"
  ]
  node [
    id 828
    label "sunset"
  ]
  node [
    id 829
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 830
    label "wiecz&#243;r"
  ]
  node [
    id 831
    label "strona_&#347;wiata"
  ]
  node [
    id 832
    label "pora"
  ]
  node [
    id 833
    label "szar&#243;wka"
  ]
  node [
    id 834
    label "szabas"
  ]
  node [
    id 835
    label "brzask"
  ]
  node [
    id 836
    label "rano"
  ]
  node [
    id 837
    label "dwunasta"
  ]
  node [
    id 838
    label "godzina"
  ]
  node [
    id 839
    label "&#347;rodek"
  ]
  node [
    id 840
    label "dzie&#324;"
  ]
  node [
    id 841
    label "doba"
  ]
  node [
    id 842
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 843
    label "dzi&#347;"
  ]
  node [
    id 844
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 845
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 846
    label "produkowa&#263;"
  ]
  node [
    id 847
    label "knit"
  ]
  node [
    id 848
    label "odm&#322;adza&#263;"
  ]
  node [
    id 849
    label "asymilowa&#263;"
  ]
  node [
    id 850
    label "cz&#261;steczka"
  ]
  node [
    id 851
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 852
    label "egzemplarz"
  ]
  node [
    id 853
    label "formacja_geologiczna"
  ]
  node [
    id 854
    label "harcerze_starsi"
  ]
  node [
    id 855
    label "liga"
  ]
  node [
    id 856
    label "Terranie"
  ]
  node [
    id 857
    label "&#346;wietliki"
  ]
  node [
    id 858
    label "pakiet_klimatyczny"
  ]
  node [
    id 859
    label "oddzia&#322;"
  ]
  node [
    id 860
    label "stage_set"
  ]
  node [
    id 861
    label "Entuzjastki"
  ]
  node [
    id 862
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 863
    label "odm&#322;odzenie"
  ]
  node [
    id 864
    label "type"
  ]
  node [
    id 865
    label "category"
  ]
  node [
    id 866
    label "asymilowanie"
  ]
  node [
    id 867
    label "specgrupa"
  ]
  node [
    id 868
    label "odm&#322;adzanie"
  ]
  node [
    id 869
    label "gromada"
  ]
  node [
    id 870
    label "Eurogrupa"
  ]
  node [
    id 871
    label "kompozycja"
  ]
  node [
    id 872
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 873
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 874
    label "zachodny"
  ]
  node [
    id 875
    label "trza"
  ]
  node [
    id 876
    label "zalicza&#263;_si&#281;"
  ]
  node [
    id 877
    label "para"
  ]
  node [
    id 878
    label "necessity"
  ]
  node [
    id 879
    label "marny"
  ]
  node [
    id 880
    label "nieznaczny"
  ]
  node [
    id 881
    label "s&#322;aby"
  ]
  node [
    id 882
    label "ch&#322;opiec"
  ]
  node [
    id 883
    label "ma&#322;o"
  ]
  node [
    id 884
    label "n&#281;dznie"
  ]
  node [
    id 885
    label "niewa&#380;ny"
  ]
  node [
    id 886
    label "przeci&#281;tny"
  ]
  node [
    id 887
    label "nieliczny"
  ]
  node [
    id 888
    label "wstydliwy"
  ]
  node [
    id 889
    label "szybki"
  ]
  node [
    id 890
    label "m&#322;ody"
  ]
  node [
    id 891
    label "poga&#324;ski"
  ]
  node [
    id 892
    label "wschodnioeuropejski"
  ]
  node [
    id 893
    label "europejski"
  ]
  node [
    id 894
    label "topielec"
  ]
  node [
    id 895
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 896
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 897
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 898
    label "fall"
  ]
  node [
    id 899
    label "zajmowa&#263;"
  ]
  node [
    id 900
    label "sta&#263;"
  ]
  node [
    id 901
    label "osiedla&#263;_si&#281;"
  ]
  node [
    id 902
    label "syrniki"
  ]
  node [
    id 903
    label "gor&#261;cy"
  ]
  node [
    id 904
    label "s&#322;oneczny"
  ]
  node [
    id 905
    label "po&#322;udniowo"
  ]
  node [
    id 906
    label "S&#322;owianin"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 141
  ]
  edge [
    source 7
    target 142
  ]
  edge [
    source 7
    target 143
  ]
  edge [
    source 7
    target 144
  ]
  edge [
    source 7
    target 145
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 146
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 147
  ]
  edge [
    source 9
    target 148
  ]
  edge [
    source 9
    target 149
  ]
  edge [
    source 9
    target 150
  ]
  edge [
    source 9
    target 17
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 57
  ]
  edge [
    source 10
    target 151
  ]
  edge [
    source 10
    target 152
  ]
  edge [
    source 10
    target 153
  ]
  edge [
    source 10
    target 154
  ]
  edge [
    source 10
    target 155
  ]
  edge [
    source 10
    target 156
  ]
  edge [
    source 10
    target 157
  ]
  edge [
    source 10
    target 158
  ]
  edge [
    source 10
    target 159
  ]
  edge [
    source 10
    target 160
  ]
  edge [
    source 10
    target 161
  ]
  edge [
    source 10
    target 33
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 162
  ]
  edge [
    source 11
    target 163
  ]
  edge [
    source 11
    target 164
  ]
  edge [
    source 11
    target 165
  ]
  edge [
    source 11
    target 166
  ]
  edge [
    source 11
    target 167
  ]
  edge [
    source 11
    target 168
  ]
  edge [
    source 11
    target 169
  ]
  edge [
    source 11
    target 170
  ]
  edge [
    source 11
    target 171
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 172
  ]
  edge [
    source 12
    target 173
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 174
  ]
  edge [
    source 13
    target 175
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 176
  ]
  edge [
    source 14
    target 177
  ]
  edge [
    source 14
    target 178
  ]
  edge [
    source 14
    target 179
  ]
  edge [
    source 14
    target 180
  ]
  edge [
    source 14
    target 181
  ]
  edge [
    source 14
    target 182
  ]
  edge [
    source 14
    target 183
  ]
  edge [
    source 14
    target 184
  ]
  edge [
    source 14
    target 185
  ]
  edge [
    source 14
    target 186
  ]
  edge [
    source 14
    target 187
  ]
  edge [
    source 14
    target 188
  ]
  edge [
    source 14
    target 189
  ]
  edge [
    source 14
    target 190
  ]
  edge [
    source 14
    target 191
  ]
  edge [
    source 14
    target 192
  ]
  edge [
    source 14
    target 193
  ]
  edge [
    source 14
    target 194
  ]
  edge [
    source 14
    target 195
  ]
  edge [
    source 14
    target 196
  ]
  edge [
    source 14
    target 197
  ]
  edge [
    source 14
    target 198
  ]
  edge [
    source 14
    target 199
  ]
  edge [
    source 14
    target 200
  ]
  edge [
    source 14
    target 201
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 47
  ]
  edge [
    source 16
    target 78
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 57
  ]
  edge [
    source 17
    target 69
  ]
  edge [
    source 17
    target 202
  ]
  edge [
    source 17
    target 203
  ]
  edge [
    source 17
    target 204
  ]
  edge [
    source 17
    target 205
  ]
  edge [
    source 17
    target 206
  ]
  edge [
    source 17
    target 207
  ]
  edge [
    source 17
    target 208
  ]
  edge [
    source 17
    target 209
  ]
  edge [
    source 17
    target 210
  ]
  edge [
    source 17
    target 211
  ]
  edge [
    source 17
    target 174
  ]
  edge [
    source 17
    target 73
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 212
  ]
  edge [
    source 18
    target 213
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 214
  ]
  edge [
    source 19
    target 215
  ]
  edge [
    source 19
    target 216
  ]
  edge [
    source 19
    target 217
  ]
  edge [
    source 19
    target 218
  ]
  edge [
    source 19
    target 219
  ]
  edge [
    source 19
    target 220
  ]
  edge [
    source 19
    target 95
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 195
  ]
  edge [
    source 21
    target 221
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 222
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 206
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 33
  ]
  edge [
    source 24
    target 34
  ]
  edge [
    source 24
    target 36
  ]
  edge [
    source 24
    target 37
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 223
  ]
  edge [
    source 25
    target 224
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 225
  ]
  edge [
    source 26
    target 226
  ]
  edge [
    source 26
    target 227
  ]
  edge [
    source 26
    target 228
  ]
  edge [
    source 26
    target 229
  ]
  edge [
    source 26
    target 230
  ]
  edge [
    source 26
    target 231
  ]
  edge [
    source 26
    target 232
  ]
  edge [
    source 26
    target 233
  ]
  edge [
    source 26
    target 234
  ]
  edge [
    source 26
    target 235
  ]
  edge [
    source 26
    target 236
  ]
  edge [
    source 26
    target 237
  ]
  edge [
    source 26
    target 238
  ]
  edge [
    source 26
    target 239
  ]
  edge [
    source 26
    target 240
  ]
  edge [
    source 26
    target 241
  ]
  edge [
    source 26
    target 242
  ]
  edge [
    source 26
    target 243
  ]
  edge [
    source 26
    target 244
  ]
  edge [
    source 26
    target 245
  ]
  edge [
    source 26
    target 246
  ]
  edge [
    source 26
    target 247
  ]
  edge [
    source 26
    target 248
  ]
  edge [
    source 26
    target 249
  ]
  edge [
    source 26
    target 250
  ]
  edge [
    source 26
    target 251
  ]
  edge [
    source 26
    target 252
  ]
  edge [
    source 26
    target 253
  ]
  edge [
    source 26
    target 254
  ]
  edge [
    source 26
    target 255
  ]
  edge [
    source 26
    target 256
  ]
  edge [
    source 26
    target 257
  ]
  edge [
    source 26
    target 258
  ]
  edge [
    source 26
    target 259
  ]
  edge [
    source 26
    target 53
  ]
  edge [
    source 26
    target 68
  ]
  edge [
    source 26
    target 103
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 260
  ]
  edge [
    source 27
    target 261
  ]
  edge [
    source 27
    target 262
  ]
  edge [
    source 27
    target 263
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 264
  ]
  edge [
    source 28
    target 173
  ]
  edge [
    source 28
    target 265
  ]
  edge [
    source 28
    target 266
  ]
  edge [
    source 28
    target 267
  ]
  edge [
    source 28
    target 268
  ]
  edge [
    source 28
    target 269
  ]
  edge [
    source 28
    target 270
  ]
  edge [
    source 28
    target 271
  ]
  edge [
    source 28
    target 272
  ]
  edge [
    source 28
    target 273
  ]
  edge [
    source 28
    target 274
  ]
  edge [
    source 28
    target 275
  ]
  edge [
    source 28
    target 92
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 276
  ]
  edge [
    source 29
    target 277
  ]
  edge [
    source 29
    target 278
  ]
  edge [
    source 29
    target 279
  ]
  edge [
    source 29
    target 280
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 281
  ]
  edge [
    source 30
    target 282
  ]
  edge [
    source 30
    target 283
  ]
  edge [
    source 30
    target 284
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 285
  ]
  edge [
    source 31
    target 286
  ]
  edge [
    source 31
    target 255
  ]
  edge [
    source 31
    target 287
  ]
  edge [
    source 31
    target 240
  ]
  edge [
    source 31
    target 288
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 289
  ]
  edge [
    source 32
    target 290
  ]
  edge [
    source 32
    target 291
  ]
  edge [
    source 32
    target 292
  ]
  edge [
    source 32
    target 293
  ]
  edge [
    source 32
    target 294
  ]
  edge [
    source 32
    target 295
  ]
  edge [
    source 33
    target 296
  ]
  edge [
    source 33
    target 297
  ]
  edge [
    source 33
    target 298
  ]
  edge [
    source 33
    target 299
  ]
  edge [
    source 33
    target 300
  ]
  edge [
    source 33
    target 301
  ]
  edge [
    source 33
    target 302
  ]
  edge [
    source 33
    target 303
  ]
  edge [
    source 33
    target 304
  ]
  edge [
    source 33
    target 305
  ]
  edge [
    source 33
    target 306
  ]
  edge [
    source 33
    target 307
  ]
  edge [
    source 33
    target 308
  ]
  edge [
    source 33
    target 309
  ]
  edge [
    source 33
    target 310
  ]
  edge [
    source 33
    target 311
  ]
  edge [
    source 33
    target 312
  ]
  edge [
    source 33
    target 313
  ]
  edge [
    source 33
    target 314
  ]
  edge [
    source 33
    target 315
  ]
  edge [
    source 33
    target 316
  ]
  edge [
    source 33
    target 317
  ]
  edge [
    source 33
    target 318
  ]
  edge [
    source 33
    target 319
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 320
  ]
  edge [
    source 34
    target 321
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 322
  ]
  edge [
    source 35
    target 323
  ]
  edge [
    source 35
    target 324
  ]
  edge [
    source 35
    target 325
  ]
  edge [
    source 35
    target 326
  ]
  edge [
    source 35
    target 327
  ]
  edge [
    source 35
    target 328
  ]
  edge [
    source 35
    target 329
  ]
  edge [
    source 35
    target 330
  ]
  edge [
    source 35
    target 331
  ]
  edge [
    source 35
    target 332
  ]
  edge [
    source 35
    target 333
  ]
  edge [
    source 35
    target 334
  ]
  edge [
    source 35
    target 335
  ]
  edge [
    source 35
    target 156
  ]
  edge [
    source 35
    target 336
  ]
  edge [
    source 35
    target 337
  ]
  edge [
    source 35
    target 338
  ]
  edge [
    source 35
    target 339
  ]
  edge [
    source 35
    target 52
  ]
  edge [
    source 36
    target 340
  ]
  edge [
    source 36
    target 162
  ]
  edge [
    source 36
    target 341
  ]
  edge [
    source 36
    target 342
  ]
  edge [
    source 36
    target 343
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 344
  ]
  edge [
    source 37
    target 345
  ]
  edge [
    source 37
    target 346
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 347
  ]
  edge [
    source 38
    target 348
  ]
  edge [
    source 38
    target 349
  ]
  edge [
    source 38
    target 350
  ]
  edge [
    source 38
    target 351
  ]
  edge [
    source 38
    target 352
  ]
  edge [
    source 38
    target 353
  ]
  edge [
    source 38
    target 354
  ]
  edge [
    source 38
    target 355
  ]
  edge [
    source 38
    target 356
  ]
  edge [
    source 38
    target 357
  ]
  edge [
    source 38
    target 358
  ]
  edge [
    source 38
    target 359
  ]
  edge [
    source 38
    target 360
  ]
  edge [
    source 38
    target 361
  ]
  edge [
    source 38
    target 362
  ]
  edge [
    source 38
    target 363
  ]
  edge [
    source 38
    target 364
  ]
  edge [
    source 38
    target 365
  ]
  edge [
    source 38
    target 366
  ]
  edge [
    source 38
    target 189
  ]
  edge [
    source 38
    target 367
  ]
  edge [
    source 38
    target 368
  ]
  edge [
    source 38
    target 369
  ]
  edge [
    source 38
    target 370
  ]
  edge [
    source 38
    target 46
  ]
  edge [
    source 38
    target 371
  ]
  edge [
    source 38
    target 372
  ]
  edge [
    source 38
    target 373
  ]
  edge [
    source 38
    target 374
  ]
  edge [
    source 38
    target 375
  ]
  edge [
    source 38
    target 376
  ]
  edge [
    source 38
    target 377
  ]
  edge [
    source 38
    target 378
  ]
  edge [
    source 38
    target 379
  ]
  edge [
    source 38
    target 380
  ]
  edge [
    source 38
    target 381
  ]
  edge [
    source 38
    target 382
  ]
  edge [
    source 38
    target 383
  ]
  edge [
    source 38
    target 384
  ]
  edge [
    source 38
    target 56
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 385
  ]
  edge [
    source 39
    target 386
  ]
  edge [
    source 39
    target 387
  ]
  edge [
    source 39
    target 388
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 42
    target 389
  ]
  edge [
    source 42
    target 390
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 391
  ]
  edge [
    source 43
    target 392
  ]
  edge [
    source 43
    target 393
  ]
  edge [
    source 43
    target 394
  ]
  edge [
    source 43
    target 395
  ]
  edge [
    source 43
    target 396
  ]
  edge [
    source 43
    target 397
  ]
  edge [
    source 43
    target 398
  ]
  edge [
    source 43
    target 399
  ]
  edge [
    source 43
    target 400
  ]
  edge [
    source 43
    target 401
  ]
  edge [
    source 43
    target 402
  ]
  edge [
    source 43
    target 403
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 404
  ]
  edge [
    source 44
    target 405
  ]
  edge [
    source 44
    target 406
  ]
  edge [
    source 44
    target 407
  ]
  edge [
    source 44
    target 408
  ]
  edge [
    source 44
    target 331
  ]
  edge [
    source 44
    target 409
  ]
  edge [
    source 44
    target 410
  ]
  edge [
    source 44
    target 411
  ]
  edge [
    source 44
    target 412
  ]
  edge [
    source 44
    target 413
  ]
  edge [
    source 44
    target 414
  ]
  edge [
    source 44
    target 415
  ]
  edge [
    source 44
    target 249
  ]
  edge [
    source 44
    target 416
  ]
  edge [
    source 44
    target 417
  ]
  edge [
    source 44
    target 418
  ]
  edge [
    source 44
    target 65
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 419
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 52
  ]
  edge [
    source 46
    target 53
  ]
  edge [
    source 46
    target 420
  ]
  edge [
    source 46
    target 421
  ]
  edge [
    source 46
    target 348
  ]
  edge [
    source 46
    target 422
  ]
  edge [
    source 46
    target 423
  ]
  edge [
    source 46
    target 424
  ]
  edge [
    source 46
    target 107
  ]
  edge [
    source 46
    target 425
  ]
  edge [
    source 46
    target 426
  ]
  edge [
    source 46
    target 427
  ]
  edge [
    source 46
    target 428
  ]
  edge [
    source 46
    target 429
  ]
  edge [
    source 46
    target 430
  ]
  edge [
    source 46
    target 431
  ]
  edge [
    source 46
    target 432
  ]
  edge [
    source 46
    target 56
  ]
  edge [
    source 47
    target 77
  ]
  edge [
    source 47
    target 67
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 433
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 434
  ]
  edge [
    source 50
    target 435
  ]
  edge [
    source 50
    target 436
  ]
  edge [
    source 50
    target 437
  ]
  edge [
    source 50
    target 438
  ]
  edge [
    source 50
    target 439
  ]
  edge [
    source 50
    target 440
  ]
  edge [
    source 50
    target 441
  ]
  edge [
    source 50
    target 442
  ]
  edge [
    source 50
    target 443
  ]
  edge [
    source 50
    target 444
  ]
  edge [
    source 50
    target 445
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 446
  ]
  edge [
    source 51
    target 447
  ]
  edge [
    source 51
    target 448
  ]
  edge [
    source 51
    target 449
  ]
  edge [
    source 51
    target 450
  ]
  edge [
    source 51
    target 451
  ]
  edge [
    source 52
    target 452
  ]
  edge [
    source 52
    target 453
  ]
  edge [
    source 52
    target 454
  ]
  edge [
    source 52
    target 455
  ]
  edge [
    source 52
    target 456
  ]
  edge [
    source 52
    target 457
  ]
  edge [
    source 52
    target 458
  ]
  edge [
    source 52
    target 459
  ]
  edge [
    source 52
    target 460
  ]
  edge [
    source 52
    target 461
  ]
  edge [
    source 52
    target 462
  ]
  edge [
    source 52
    target 463
  ]
  edge [
    source 52
    target 115
  ]
  edge [
    source 52
    target 464
  ]
  edge [
    source 52
    target 465
  ]
  edge [
    source 52
    target 466
  ]
  edge [
    source 52
    target 467
  ]
  edge [
    source 52
    target 468
  ]
  edge [
    source 52
    target 469
  ]
  edge [
    source 52
    target 470
  ]
  edge [
    source 52
    target 471
  ]
  edge [
    source 52
    target 472
  ]
  edge [
    source 52
    target 473
  ]
  edge [
    source 52
    target 474
  ]
  edge [
    source 52
    target 475
  ]
  edge [
    source 52
    target 476
  ]
  edge [
    source 52
    target 477
  ]
  edge [
    source 52
    target 478
  ]
  edge [
    source 52
    target 479
  ]
  edge [
    source 52
    target 480
  ]
  edge [
    source 52
    target 481
  ]
  edge [
    source 52
    target 482
  ]
  edge [
    source 52
    target 483
  ]
  edge [
    source 52
    target 484
  ]
  edge [
    source 52
    target 485
  ]
  edge [
    source 52
    target 486
  ]
  edge [
    source 52
    target 487
  ]
  edge [
    source 52
    target 488
  ]
  edge [
    source 52
    target 489
  ]
  edge [
    source 52
    target 490
  ]
  edge [
    source 52
    target 491
  ]
  edge [
    source 52
    target 492
  ]
  edge [
    source 52
    target 493
  ]
  edge [
    source 52
    target 494
  ]
  edge [
    source 52
    target 495
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 55
  ]
  edge [
    source 53
    target 56
  ]
  edge [
    source 53
    target 67
  ]
  edge [
    source 53
    target 68
  ]
  edge [
    source 53
    target 80
  ]
  edge [
    source 53
    target 69
  ]
  edge [
    source 53
    target 82
  ]
  edge [
    source 53
    target 91
  ]
  edge [
    source 53
    target 92
  ]
  edge [
    source 53
    target 96
  ]
  edge [
    source 53
    target 97
  ]
  edge [
    source 53
    target 103
  ]
  edge [
    source 53
    target 102
  ]
  edge [
    source 53
    target 84
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 344
  ]
  edge [
    source 54
    target 496
  ]
  edge [
    source 54
    target 497
  ]
  edge [
    source 54
    target 498
  ]
  edge [
    source 55
    target 83
  ]
  edge [
    source 55
    target 84
  ]
  edge [
    source 55
    target 499
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 500
  ]
  edge [
    source 56
    target 501
  ]
  edge [
    source 56
    target 502
  ]
  edge [
    source 56
    target 348
  ]
  edge [
    source 56
    target 107
  ]
  edge [
    source 56
    target 503
  ]
  edge [
    source 56
    target 81
  ]
  edge [
    source 56
    target 425
  ]
  edge [
    source 56
    target 504
  ]
  edge [
    source 56
    target 424
  ]
  edge [
    source 56
    target 427
  ]
  edge [
    source 56
    target 505
  ]
  edge [
    source 56
    target 506
  ]
  edge [
    source 56
    target 429
  ]
  edge [
    source 56
    target 507
  ]
  edge [
    source 56
    target 508
  ]
  edge [
    source 56
    target 509
  ]
  edge [
    source 56
    target 510
  ]
  edge [
    source 56
    target 511
  ]
  edge [
    source 56
    target 512
  ]
  edge [
    source 56
    target 513
  ]
  edge [
    source 56
    target 514
  ]
  edge [
    source 56
    target 515
  ]
  edge [
    source 56
    target 420
  ]
  edge [
    source 56
    target 516
  ]
  edge [
    source 56
    target 517
  ]
  edge [
    source 56
    target 423
  ]
  edge [
    source 56
    target 421
  ]
  edge [
    source 56
    target 518
  ]
  edge [
    source 56
    target 519
  ]
  edge [
    source 56
    target 520
  ]
  edge [
    source 56
    target 426
  ]
  edge [
    source 56
    target 521
  ]
  edge [
    source 56
    target 428
  ]
  edge [
    source 56
    target 417
  ]
  edge [
    source 56
    target 522
  ]
  edge [
    source 56
    target 523
  ]
  edge [
    source 56
    target 432
  ]
  edge [
    source 56
    target 524
  ]
  edge [
    source 56
    target 77
  ]
  edge [
    source 56
    target 525
  ]
  edge [
    source 56
    target 526
  ]
  edge [
    source 56
    target 527
  ]
  edge [
    source 56
    target 528
  ]
  edge [
    source 56
    target 529
  ]
  edge [
    source 56
    target 530
  ]
  edge [
    source 56
    target 531
  ]
  edge [
    source 56
    target 532
  ]
  edge [
    source 56
    target 533
  ]
  edge [
    source 56
    target 534
  ]
  edge [
    source 56
    target 535
  ]
  edge [
    source 56
    target 536
  ]
  edge [
    source 56
    target 537
  ]
  edge [
    source 56
    target 538
  ]
  edge [
    source 56
    target 539
  ]
  edge [
    source 56
    target 422
  ]
  edge [
    source 56
    target 540
  ]
  edge [
    source 56
    target 541
  ]
  edge [
    source 56
    target 542
  ]
  edge [
    source 56
    target 543
  ]
  edge [
    source 56
    target 544
  ]
  edge [
    source 56
    target 545
  ]
  edge [
    source 56
    target 546
  ]
  edge [
    source 56
    target 547
  ]
  edge [
    source 56
    target 548
  ]
  edge [
    source 56
    target 431
  ]
  edge [
    source 56
    target 549
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 68
  ]
  edge [
    source 57
    target 73
  ]
  edge [
    source 57
    target 74
  ]
  edge [
    source 57
    target 86
  ]
  edge [
    source 57
    target 87
  ]
  edge [
    source 57
    target 550
  ]
  edge [
    source 57
    target 551
  ]
  edge [
    source 57
    target 552
  ]
  edge [
    source 57
    target 61
  ]
  edge [
    source 57
    target 340
  ]
  edge [
    source 57
    target 553
  ]
  edge [
    source 57
    target 554
  ]
  edge [
    source 57
    target 555
  ]
  edge [
    source 57
    target 556
  ]
  edge [
    source 57
    target 557
  ]
  edge [
    source 57
    target 558
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 559
  ]
  edge [
    source 58
    target 560
  ]
  edge [
    source 58
    target 561
  ]
  edge [
    source 58
    target 562
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 563
  ]
  edge [
    source 59
    target 564
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 565
  ]
  edge [
    source 60
    target 69
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 566
  ]
  edge [
    source 61
    target 567
  ]
  edge [
    source 61
    target 568
  ]
  edge [
    source 61
    target 569
  ]
  edge [
    source 61
    target 570
  ]
  edge [
    source 61
    target 571
  ]
  edge [
    source 61
    target 572
  ]
  edge [
    source 61
    target 573
  ]
  edge [
    source 61
    target 574
  ]
  edge [
    source 61
    target 575
  ]
  edge [
    source 61
    target 576
  ]
  edge [
    source 61
    target 577
  ]
  edge [
    source 61
    target 578
  ]
  edge [
    source 61
    target 579
  ]
  edge [
    source 61
    target 580
  ]
  edge [
    source 61
    target 581
  ]
  edge [
    source 61
    target 582
  ]
  edge [
    source 61
    target 583
  ]
  edge [
    source 61
    target 584
  ]
  edge [
    source 61
    target 585
  ]
  edge [
    source 61
    target 586
  ]
  edge [
    source 61
    target 587
  ]
  edge [
    source 61
    target 588
  ]
  edge [
    source 61
    target 589
  ]
  edge [
    source 61
    target 283
  ]
  edge [
    source 61
    target 590
  ]
  edge [
    source 61
    target 591
  ]
  edge [
    source 61
    target 592
  ]
  edge [
    source 61
    target 316
  ]
  edge [
    source 61
    target 593
  ]
  edge [
    source 61
    target 594
  ]
  edge [
    source 61
    target 595
  ]
  edge [
    source 61
    target 596
  ]
  edge [
    source 61
    target 597
  ]
  edge [
    source 61
    target 598
  ]
  edge [
    source 61
    target 599
  ]
  edge [
    source 61
    target 600
  ]
  edge [
    source 61
    target 601
  ]
  edge [
    source 61
    target 602
  ]
  edge [
    source 61
    target 65
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 70
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 603
  ]
  edge [
    source 63
    target 604
  ]
  edge [
    source 63
    target 605
  ]
  edge [
    source 63
    target 606
  ]
  edge [
    source 63
    target 249
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 607
  ]
  edge [
    source 64
    target 608
  ]
  edge [
    source 64
    target 609
  ]
  edge [
    source 64
    target 610
  ]
  edge [
    source 64
    target 611
  ]
  edge [
    source 64
    target 612
  ]
  edge [
    source 64
    target 613
  ]
  edge [
    source 64
    target 502
  ]
  edge [
    source 64
    target 614
  ]
  edge [
    source 64
    target 615
  ]
  edge [
    source 64
    target 616
  ]
  edge [
    source 64
    target 617
  ]
  edge [
    source 64
    target 618
  ]
  edge [
    source 64
    target 619
  ]
  edge [
    source 64
    target 620
  ]
  edge [
    source 64
    target 621
  ]
  edge [
    source 64
    target 622
  ]
  edge [
    source 64
    target 623
  ]
  edge [
    source 64
    target 624
  ]
  edge [
    source 64
    target 625
  ]
  edge [
    source 64
    target 626
  ]
  edge [
    source 64
    target 627
  ]
  edge [
    source 64
    target 628
  ]
  edge [
    source 64
    target 629
  ]
  edge [
    source 64
    target 630
  ]
  edge [
    source 64
    target 631
  ]
  edge [
    source 64
    target 632
  ]
  edge [
    source 64
    target 633
  ]
  edge [
    source 64
    target 634
  ]
  edge [
    source 64
    target 635
  ]
  edge [
    source 64
    target 636
  ]
  edge [
    source 64
    target 637
  ]
  edge [
    source 64
    target 638
  ]
  edge [
    source 64
    target 639
  ]
  edge [
    source 64
    target 640
  ]
  edge [
    source 64
    target 641
  ]
  edge [
    source 64
    target 642
  ]
  edge [
    source 64
    target 643
  ]
  edge [
    source 64
    target 644
  ]
  edge [
    source 64
    target 645
  ]
  edge [
    source 64
    target 646
  ]
  edge [
    source 64
    target 647
  ]
  edge [
    source 64
    target 648
  ]
  edge [
    source 64
    target 649
  ]
  edge [
    source 64
    target 650
  ]
  edge [
    source 64
    target 651
  ]
  edge [
    source 64
    target 652
  ]
  edge [
    source 64
    target 653
  ]
  edge [
    source 64
    target 654
  ]
  edge [
    source 64
    target 655
  ]
  edge [
    source 64
    target 656
  ]
  edge [
    source 64
    target 657
  ]
  edge [
    source 64
    target 658
  ]
  edge [
    source 64
    target 659
  ]
  edge [
    source 64
    target 660
  ]
  edge [
    source 64
    target 661
  ]
  edge [
    source 64
    target 662
  ]
  edge [
    source 64
    target 663
  ]
  edge [
    source 64
    target 664
  ]
  edge [
    source 64
    target 665
  ]
  edge [
    source 64
    target 666
  ]
  edge [
    source 64
    target 667
  ]
  edge [
    source 64
    target 668
  ]
  edge [
    source 64
    target 669
  ]
  edge [
    source 64
    target 670
  ]
  edge [
    source 64
    target 671
  ]
  edge [
    source 64
    target 672
  ]
  edge [
    source 64
    target 673
  ]
  edge [
    source 64
    target 674
  ]
  edge [
    source 64
    target 675
  ]
  edge [
    source 64
    target 676
  ]
  edge [
    source 64
    target 677
  ]
  edge [
    source 64
    target 678
  ]
  edge [
    source 64
    target 679
  ]
  edge [
    source 64
    target 680
  ]
  edge [
    source 64
    target 681
  ]
  edge [
    source 64
    target 682
  ]
  edge [
    source 64
    target 683
  ]
  edge [
    source 64
    target 684
  ]
  edge [
    source 64
    target 569
  ]
  edge [
    source 64
    target 685
  ]
  edge [
    source 64
    target 686
  ]
  edge [
    source 64
    target 687
  ]
  edge [
    source 64
    target 688
  ]
  edge [
    source 64
    target 689
  ]
  edge [
    source 64
    target 690
  ]
  edge [
    source 64
    target 691
  ]
  edge [
    source 64
    target 692
  ]
  edge [
    source 64
    target 693
  ]
  edge [
    source 64
    target 694
  ]
  edge [
    source 64
    target 428
  ]
  edge [
    source 64
    target 695
  ]
  edge [
    source 64
    target 307
  ]
  edge [
    source 64
    target 696
  ]
  edge [
    source 64
    target 697
  ]
  edge [
    source 64
    target 698
  ]
  edge [
    source 64
    target 699
  ]
  edge [
    source 64
    target 700
  ]
  edge [
    source 64
    target 701
  ]
  edge [
    source 64
    target 702
  ]
  edge [
    source 64
    target 703
  ]
  edge [
    source 64
    target 704
  ]
  edge [
    source 64
    target 705
  ]
  edge [
    source 64
    target 706
  ]
  edge [
    source 64
    target 707
  ]
  edge [
    source 64
    target 708
  ]
  edge [
    source 64
    target 709
  ]
  edge [
    source 64
    target 710
  ]
  edge [
    source 64
    target 711
  ]
  edge [
    source 64
    target 712
  ]
  edge [
    source 64
    target 713
  ]
  edge [
    source 64
    target 714
  ]
  edge [
    source 64
    target 715
  ]
  edge [
    source 64
    target 716
  ]
  edge [
    source 64
    target 717
  ]
  edge [
    source 64
    target 718
  ]
  edge [
    source 64
    target 719
  ]
  edge [
    source 64
    target 720
  ]
  edge [
    source 64
    target 512
  ]
  edge [
    source 64
    target 721
  ]
  edge [
    source 64
    target 722
  ]
  edge [
    source 64
    target 723
  ]
  edge [
    source 64
    target 724
  ]
  edge [
    source 64
    target 725
  ]
  edge [
    source 64
    target 726
  ]
  edge [
    source 64
    target 727
  ]
  edge [
    source 64
    target 364
  ]
  edge [
    source 64
    target 728
  ]
  edge [
    source 64
    target 729
  ]
  edge [
    source 64
    target 730
  ]
  edge [
    source 64
    target 529
  ]
  edge [
    source 64
    target 731
  ]
  edge [
    source 64
    target 369
  ]
  edge [
    source 64
    target 732
  ]
  edge [
    source 64
    target 733
  ]
  edge [
    source 64
    target 734
  ]
  edge [
    source 64
    target 735
  ]
  edge [
    source 64
    target 736
  ]
  edge [
    source 64
    target 737
  ]
  edge [
    source 64
    target 738
  ]
  edge [
    source 64
    target 739
  ]
  edge [
    source 64
    target 740
  ]
  edge [
    source 64
    target 316
  ]
  edge [
    source 64
    target 741
  ]
  edge [
    source 64
    target 742
  ]
  edge [
    source 64
    target 743
  ]
  edge [
    source 64
    target 744
  ]
  edge [
    source 64
    target 745
  ]
  edge [
    source 64
    target 746
  ]
  edge [
    source 64
    target 747
  ]
  edge [
    source 64
    target 748
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 749
  ]
  edge [
    source 65
    target 750
  ]
  edge [
    source 65
    target 751
  ]
  edge [
    source 65
    target 752
  ]
  edge [
    source 65
    target 605
  ]
  edge [
    source 65
    target 753
  ]
  edge [
    source 65
    target 754
  ]
  edge [
    source 65
    target 755
  ]
  edge [
    source 65
    target 756
  ]
  edge [
    source 65
    target 757
  ]
  edge [
    source 65
    target 758
  ]
  edge [
    source 65
    target 759
  ]
  edge [
    source 65
    target 760
  ]
  edge [
    source 65
    target 389
  ]
  edge [
    source 65
    target 761
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 66
    target 344
  ]
  edge [
    source 66
    target 762
  ]
  edge [
    source 66
    target 763
  ]
  edge [
    source 66
    target 764
  ]
  edge [
    source 66
    target 765
  ]
  edge [
    source 66
    target 766
  ]
  edge [
    source 66
    target 767
  ]
  edge [
    source 66
    target 768
  ]
  edge [
    source 66
    target 769
  ]
  edge [
    source 66
    target 770
  ]
  edge [
    source 66
    target 219
  ]
  edge [
    source 66
    target 771
  ]
  edge [
    source 67
    target 772
  ]
  edge [
    source 67
    target 773
  ]
  edge [
    source 68
    target 774
  ]
  edge [
    source 68
    target 775
  ]
  edge [
    source 68
    target 776
  ]
  edge [
    source 68
    target 103
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 69
    target 81
  ]
  edge [
    source 69
    target 87
  ]
  edge [
    source 69
    target 88
  ]
  edge [
    source 69
    target 777
  ]
  edge [
    source 69
    target 778
  ]
  edge [
    source 69
    target 779
  ]
  edge [
    source 69
    target 780
  ]
  edge [
    source 69
    target 781
  ]
  edge [
    source 69
    target 782
  ]
  edge [
    source 69
    target 783
  ]
  edge [
    source 69
    target 784
  ]
  edge [
    source 69
    target 256
  ]
  edge [
    source 69
    target 785
  ]
  edge [
    source 69
    target 786
  ]
  edge [
    source 69
    target 787
  ]
  edge [
    source 69
    target 788
  ]
  edge [
    source 69
    target 789
  ]
  edge [
    source 69
    target 790
  ]
  edge [
    source 69
    target 791
  ]
  edge [
    source 69
    target 792
  ]
  edge [
    source 69
    target 97
  ]
  edge [
    source 70
    target 793
  ]
  edge [
    source 70
    target 794
  ]
  edge [
    source 70
    target 795
  ]
  edge [
    source 70
    target 796
  ]
  edge [
    source 70
    target 797
  ]
  edge [
    source 70
    target 798
  ]
  edge [
    source 70
    target 799
  ]
  edge [
    source 70
    target 800
  ]
  edge [
    source 70
    target 801
  ]
  edge [
    source 70
    target 802
  ]
  edge [
    source 70
    target 803
  ]
  edge [
    source 70
    target 804
  ]
  edge [
    source 70
    target 805
  ]
  edge [
    source 70
    target 806
  ]
  edge [
    source 70
    target 807
  ]
  edge [
    source 70
    target 808
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 71
    target 809
  ]
  edge [
    source 71
    target 810
  ]
  edge [
    source 71
    target 811
  ]
  edge [
    source 71
    target 812
  ]
  edge [
    source 71
    target 813
  ]
  edge [
    source 71
    target 335
  ]
  edge [
    source 71
    target 605
  ]
  edge [
    source 71
    target 413
  ]
  edge [
    source 71
    target 814
  ]
  edge [
    source 71
    target 815
  ]
  edge [
    source 72
    target 73
  ]
  edge [
    source 72
    target 816
  ]
  edge [
    source 72
    target 817
  ]
  edge [
    source 72
    target 818
  ]
  edge [
    source 72
    target 819
  ]
  edge [
    source 72
    target 820
  ]
  edge [
    source 73
    target 316
  ]
  edge [
    source 73
    target 821
  ]
  edge [
    source 73
    target 822
  ]
  edge [
    source 73
    target 217
  ]
  edge [
    source 73
    target 823
  ]
  edge [
    source 73
    target 824
  ]
  edge [
    source 73
    target 76
  ]
  edge [
    source 74
    target 75
  ]
  edge [
    source 74
    target 80
  ]
  edge [
    source 74
    target 81
  ]
  edge [
    source 75
    target 76
  ]
  edge [
    source 75
    target 825
  ]
  edge [
    source 75
    target 826
  ]
  edge [
    source 75
    target 827
  ]
  edge [
    source 75
    target 502
  ]
  edge [
    source 75
    target 828
  ]
  edge [
    source 75
    target 829
  ]
  edge [
    source 75
    target 830
  ]
  edge [
    source 75
    target 831
  ]
  edge [
    source 75
    target 217
  ]
  edge [
    source 75
    target 832
  ]
  edge [
    source 75
    target 417
  ]
  edge [
    source 75
    target 833
  ]
  edge [
    source 76
    target 77
  ]
  edge [
    source 76
    target 825
  ]
  edge [
    source 76
    target 834
  ]
  edge [
    source 76
    target 502
  ]
  edge [
    source 76
    target 831
  ]
  edge [
    source 76
    target 217
  ]
  edge [
    source 76
    target 832
  ]
  edge [
    source 76
    target 835
  ]
  edge [
    source 76
    target 836
  ]
  edge [
    source 76
    target 417
  ]
  edge [
    source 77
    target 837
  ]
  edge [
    source 77
    target 502
  ]
  edge [
    source 77
    target 422
  ]
  edge [
    source 77
    target 838
  ]
  edge [
    source 77
    target 831
  ]
  edge [
    source 77
    target 217
  ]
  edge [
    source 77
    target 839
  ]
  edge [
    source 77
    target 832
  ]
  edge [
    source 77
    target 840
  ]
  edge [
    source 78
    target 79
  ]
  edge [
    source 78
    target 841
  ]
  edge [
    source 78
    target 842
  ]
  edge [
    source 78
    target 843
  ]
  edge [
    source 78
    target 844
  ]
  edge [
    source 78
    target 845
  ]
  edge [
    source 78
    target 80
  ]
  edge [
    source 78
    target 90
  ]
  edge [
    source 79
    target 846
  ]
  edge [
    source 79
    target 847
  ]
  edge [
    source 80
    target 559
  ]
  edge [
    source 80
    target 90
  ]
  edge [
    source 81
    target 848
  ]
  edge [
    source 81
    target 849
  ]
  edge [
    source 81
    target 850
  ]
  edge [
    source 81
    target 851
  ]
  edge [
    source 81
    target 852
  ]
  edge [
    source 81
    target 853
  ]
  edge [
    source 81
    target 520
  ]
  edge [
    source 81
    target 854
  ]
  edge [
    source 81
    target 855
  ]
  edge [
    source 81
    target 856
  ]
  edge [
    source 81
    target 857
  ]
  edge [
    source 81
    target 858
  ]
  edge [
    source 81
    target 859
  ]
  edge [
    source 81
    target 860
  ]
  edge [
    source 81
    target 861
  ]
  edge [
    source 81
    target 862
  ]
  edge [
    source 81
    target 863
  ]
  edge [
    source 81
    target 864
  ]
  edge [
    source 81
    target 865
  ]
  edge [
    source 81
    target 866
  ]
  edge [
    source 81
    target 867
  ]
  edge [
    source 81
    target 868
  ]
  edge [
    source 81
    target 869
  ]
  edge [
    source 81
    target 870
  ]
  edge [
    source 81
    target 255
  ]
  edge [
    source 81
    target 871
  ]
  edge [
    source 81
    target 872
  ]
  edge [
    source 81
    target 605
  ]
  edge [
    source 82
    target 83
  ]
  edge [
    source 82
    target 873
  ]
  edge [
    source 82
    target 874
  ]
  edge [
    source 82
    target 906
  ]
  edge [
    source 83
    target 92
  ]
  edge [
    source 83
    target 93
  ]
  edge [
    source 83
    target 97
  ]
  edge [
    source 83
    target 98
  ]
  edge [
    source 83
    target 875
  ]
  edge [
    source 83
    target 555
  ]
  edge [
    source 83
    target 876
  ]
  edge [
    source 83
    target 877
  ]
  edge [
    source 83
    target 878
  ]
  edge [
    source 84
    target 85
  ]
  edge [
    source 85
    target 86
  ]
  edge [
    source 87
    target 879
  ]
  edge [
    source 87
    target 880
  ]
  edge [
    source 87
    target 881
  ]
  edge [
    source 87
    target 882
  ]
  edge [
    source 87
    target 883
  ]
  edge [
    source 87
    target 884
  ]
  edge [
    source 87
    target 885
  ]
  edge [
    source 87
    target 886
  ]
  edge [
    source 87
    target 887
  ]
  edge [
    source 87
    target 888
  ]
  edge [
    source 87
    target 889
  ]
  edge [
    source 87
    target 890
  ]
  edge [
    source 88
    target 89
  ]
  edge [
    source 88
    target 891
  ]
  edge [
    source 88
    target 892
  ]
  edge [
    source 88
    target 893
  ]
  edge [
    source 88
    target 894
  ]
  edge [
    source 88
    target 895
  ]
  edge [
    source 88
    target 896
  ]
  edge [
    source 88
    target 897
  ]
  edge [
    source 89
    target 90
  ]
  edge [
    source 89
    target 898
  ]
  edge [
    source 89
    target 899
  ]
  edge [
    source 89
    target 900
  ]
  edge [
    source 89
    target 901
  ]
  edge [
    source 90
    target 91
  ]
  edge [
    source 92
    target 902
  ]
  edge [
    source 92
    target 906
  ]
  edge [
    source 93
    target 94
  ]
  edge [
    source 94
    target 95
  ]
  edge [
    source 95
    target 96
  ]
  edge [
    source 97
    target 903
  ]
  edge [
    source 97
    target 904
  ]
  edge [
    source 97
    target 905
  ]
  edge [
    source 97
    target 906
  ]
  edge [
    source 98
    target 99
  ]
  edge [
    source 98
    target 100
  ]
  edge [
    source 99
    target 100
  ]
  edge [
    source 100
    target 101
  ]
  edge [
    source 101
    target 102
  ]
  edge [
    source 102
    target 103
  ]
  edge [
    source 103
    target 104
  ]
]
