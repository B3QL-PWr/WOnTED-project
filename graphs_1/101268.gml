graph [
  maxDegree 2
  minDegree 0
  meanDegree 1.5
  density 0.5
  graphCliqueNumber 3
  node [
    id 0
    label "broncin"
    origin "text"
  ]
  node [
    id 1
    label "grab"
  ]
  node [
    id 2
    label "nad"
  ]
  node [
    id 3
    label "Pilica"
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 2
    target 3
  ]
]
