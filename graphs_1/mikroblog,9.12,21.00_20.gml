graph [
  maxDegree 16
  minDegree 1
  meanDegree 2.1866666666666665
  density 0.02954954954954955
  graphCliqueNumber 6
  node [
    id 0
    label "ma&#322;o"
    origin "text"
  ]
  node [
    id 1
    label "kto"
    origin "text"
  ]
  node [
    id 2
    label "wykop"
    origin "text"
  ]
  node [
    id 3
    label "kojarzy&#263;"
    origin "text"
  ]
  node [
    id 4
    label "paulina"
    origin "text"
  ]
  node [
    id 5
    label "pomagamypaulinie"
    origin "text"
  ]
  node [
    id 6
    label "skr&#243;t"
    origin "text"
  ]
  node [
    id 7
    label "dziewczyna"
    origin "text"
  ]
  node [
    id 8
    label "lato"
    origin "text"
  ]
  node [
    id 9
    label "chorowa&#263;"
    origin "text"
  ]
  node [
    id 10
    label "mo&#380;liwie"
  ]
  node [
    id 11
    label "nieznaczny"
  ]
  node [
    id 12
    label "kr&#243;tko"
  ]
  node [
    id 13
    label "nieistotnie"
  ]
  node [
    id 14
    label "nieliczny"
  ]
  node [
    id 15
    label "mikroskopijnie"
  ]
  node [
    id 16
    label "pomiernie"
  ]
  node [
    id 17
    label "ma&#322;y"
  ]
  node [
    id 18
    label "odwa&#322;"
  ]
  node [
    id 19
    label "chody"
  ]
  node [
    id 20
    label "grodzisko"
  ]
  node [
    id 21
    label "budowa"
  ]
  node [
    id 22
    label "kopniak"
  ]
  node [
    id 23
    label "wyrobisko"
  ]
  node [
    id 24
    label "zrzutowy"
  ]
  node [
    id 25
    label "szaniec"
  ]
  node [
    id 26
    label "odk&#322;ad"
  ]
  node [
    id 27
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 28
    label "cover"
  ]
  node [
    id 29
    label "rozumie&#263;"
  ]
  node [
    id 30
    label "zaskakiwa&#263;"
  ]
  node [
    id 31
    label "swat"
  ]
  node [
    id 32
    label "relate"
  ]
  node [
    id 33
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 34
    label "retrenchment"
  ]
  node [
    id 35
    label "shortening"
  ]
  node [
    id 36
    label "redukcja"
  ]
  node [
    id 37
    label "contraction"
  ]
  node [
    id 38
    label "przej&#347;cie"
  ]
  node [
    id 39
    label "leksem"
  ]
  node [
    id 40
    label "tekst"
  ]
  node [
    id 41
    label "cz&#322;owiek"
  ]
  node [
    id 42
    label "dziewka"
  ]
  node [
    id 43
    label "dziewoja"
  ]
  node [
    id 44
    label "siksa"
  ]
  node [
    id 45
    label "partnerka"
  ]
  node [
    id 46
    label "dziewczynina"
  ]
  node [
    id 47
    label "dziunia"
  ]
  node [
    id 48
    label "sympatia"
  ]
  node [
    id 49
    label "dziewcz&#281;"
  ]
  node [
    id 50
    label "s&#322;u&#380;&#261;ca"
  ]
  node [
    id 51
    label "kora"
  ]
  node [
    id 52
    label "m&#322;&#243;dka"
  ]
  node [
    id 53
    label "dziecina"
  ]
  node [
    id 54
    label "sikorka"
  ]
  node [
    id 55
    label "pora_roku"
  ]
  node [
    id 56
    label "cierpie&#263;"
  ]
  node [
    id 57
    label "pain"
  ]
  node [
    id 58
    label "garlic"
  ]
  node [
    id 59
    label "pragn&#261;&#263;"
  ]
  node [
    id 60
    label "Paulina"
  ]
  node [
    id 61
    label "robertt1969"
  ]
  node [
    id 62
    label "Robert"
  ]
  node [
    id 63
    label "komenda"
  ]
  node [
    id 64
    label "powiatowy"
  ]
  node [
    id 65
    label "pa&#324;stwowy"
  ]
  node [
    id 66
    label "stra&#380;a"
  ]
  node [
    id 67
    label "po&#380;arny"
  ]
  node [
    id 68
    label "pi&#322;a"
  ]
  node [
    id 69
    label "kiedy"
  ]
  node [
    id 70
    label "920"
  ]
  node [
    id 71
    label "poczta"
  ]
  node [
    id 72
    label "Polska"
  ]
  node [
    id 73
    label "bo&#380;y"
  ]
  node [
    id 74
    label "narodzenie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 8
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 9
    target 56
  ]
  edge [
    source 9
    target 57
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 64
  ]
  edge [
    source 62
    target 65
  ]
  edge [
    source 62
    target 66
  ]
  edge [
    source 62
    target 67
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 65
  ]
  edge [
    source 63
    target 66
  ]
  edge [
    source 63
    target 67
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 66
  ]
  edge [
    source 64
    target 67
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 67
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 68
    target 70
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 73
    target 74
  ]
]
