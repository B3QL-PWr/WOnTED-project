graph [
  maxDegree 31
  minDegree 1
  meanDegree 1.9811320754716981
  density 0.018867924528301886
  graphCliqueNumber 2
  node [
    id 0
    label "film"
    origin "text"
  ]
  node [
    id 1
    label "wojna"
    origin "text"
  ]
  node [
    id 2
    label "polsko"
    origin "text"
  ]
  node [
    id 3
    label "bolszewicki"
    origin "text"
  ]
  node [
    id 4
    label "umie&#347;ci&#263;"
    origin "text"
  ]
  node [
    id 5
    label "zagraniczny"
    origin "text"
  ]
  node [
    id 6
    label "do&#347;&#263;"
    origin "text"
  ]
  node [
    id 7
    label "popularny"
    origin "text"
  ]
  node [
    id 8
    label "kana&#322;"
    origin "text"
  ]
  node [
    id 9
    label "you"
    origin "text"
  ]
  node [
    id 10
    label "tube"
    origin "text"
  ]
  node [
    id 11
    label "rozbieg&#243;wka"
  ]
  node [
    id 12
    label "block"
  ]
  node [
    id 13
    label "odczula&#263;"
  ]
  node [
    id 14
    label "blik"
  ]
  node [
    id 15
    label "rola"
  ]
  node [
    id 16
    label "trawiarnia"
  ]
  node [
    id 17
    label "b&#322;ona"
  ]
  node [
    id 18
    label "filmoteka"
  ]
  node [
    id 19
    label "sztuka"
  ]
  node [
    id 20
    label "muza"
  ]
  node [
    id 21
    label "odczuli&#263;"
  ]
  node [
    id 22
    label "klatka"
  ]
  node [
    id 23
    label "odczulenie"
  ]
  node [
    id 24
    label "emulsja_fotograficzna"
  ]
  node [
    id 25
    label "animatronika"
  ]
  node [
    id 26
    label "dorobek"
  ]
  node [
    id 27
    label "odczulanie"
  ]
  node [
    id 28
    label "scena"
  ]
  node [
    id 29
    label "czo&#322;&#243;wka"
  ]
  node [
    id 30
    label "ty&#322;&#243;wka"
  ]
  node [
    id 31
    label "napisy"
  ]
  node [
    id 32
    label "photograph"
  ]
  node [
    id 33
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 34
    label "postprodukcja"
  ]
  node [
    id 35
    label "sklejarka"
  ]
  node [
    id 36
    label "anamorfoza"
  ]
  node [
    id 37
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 38
    label "ta&#347;ma"
  ]
  node [
    id 39
    label "b&#322;ona_&#347;wiat&#322;oczu&#322;a"
  ]
  node [
    id 40
    label "uj&#281;cie"
  ]
  node [
    id 41
    label "zimna_wojna"
  ]
  node [
    id 42
    label "zbrodnia_przeciw_ludzko&#347;ci"
  ]
  node [
    id 43
    label "angaria"
  ]
  node [
    id 44
    label "wr&#243;g"
  ]
  node [
    id 45
    label "zbrodnia_przeciw_pokojowi"
  ]
  node [
    id 46
    label "walka"
  ]
  node [
    id 47
    label "war"
  ]
  node [
    id 48
    label "konflikt"
  ]
  node [
    id 49
    label "wojna_stuletnia"
  ]
  node [
    id 50
    label "burza"
  ]
  node [
    id 51
    label "zbrodnia_wojenna"
  ]
  node [
    id 52
    label "gra_w_karty"
  ]
  node [
    id 53
    label "sp&#243;r"
  ]
  node [
    id 54
    label "po_polsku"
  ]
  node [
    id 55
    label "polski"
  ]
  node [
    id 56
    label "europejsko"
  ]
  node [
    id 57
    label "radykalny"
  ]
  node [
    id 58
    label "po_bolszewicku"
  ]
  node [
    id 59
    label "lewicowy"
  ]
  node [
    id 60
    label "skomunizowanie"
  ]
  node [
    id 61
    label "czerwono"
  ]
  node [
    id 62
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 63
    label "komunistyczny"
  ]
  node [
    id 64
    label "komunizowanie"
  ]
  node [
    id 65
    label "zrobi&#263;"
  ]
  node [
    id 66
    label "okre&#347;li&#263;"
  ]
  node [
    id 67
    label "uplasowa&#263;"
  ]
  node [
    id 68
    label "umieszcza&#263;"
  ]
  node [
    id 69
    label "wpierniczy&#263;"
  ]
  node [
    id 70
    label "pomie&#347;ci&#263;"
  ]
  node [
    id 71
    label "zmieni&#263;"
  ]
  node [
    id 72
    label "put"
  ]
  node [
    id 73
    label "set"
  ]
  node [
    id 74
    label "zagranicznie"
  ]
  node [
    id 75
    label "obcy"
  ]
  node [
    id 76
    label "przyst&#281;pny"
  ]
  node [
    id 77
    label "&#322;atwy"
  ]
  node [
    id 78
    label "popularnie"
  ]
  node [
    id 79
    label "znany"
  ]
  node [
    id 80
    label "chody"
  ]
  node [
    id 81
    label "szaniec"
  ]
  node [
    id 82
    label "gara&#380;"
  ]
  node [
    id 83
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 84
    label "tarapaty"
  ]
  node [
    id 85
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 86
    label "budowa"
  ]
  node [
    id 87
    label "pit"
  ]
  node [
    id 88
    label "ciek"
  ]
  node [
    id 89
    label "spos&#243;b"
  ]
  node [
    id 90
    label "odwa&#322;"
  ]
  node [
    id 91
    label "bystrza"
  ]
  node [
    id 92
    label "teatr"
  ]
  node [
    id 93
    label "piaskownik"
  ]
  node [
    id 94
    label "zrzutowy"
  ]
  node [
    id 95
    label "przew&#243;d"
  ]
  node [
    id 96
    label "syfon"
  ]
  node [
    id 97
    label "klarownia"
  ]
  node [
    id 98
    label "miejsce"
  ]
  node [
    id 99
    label "topologia_magistrali"
  ]
  node [
    id 100
    label "struktura_anatomiczna"
  ]
  node [
    id 101
    label "odk&#322;ad"
  ]
  node [
    id 102
    label "kanalizacja"
  ]
  node [
    id 103
    label "urz&#261;dzenie"
  ]
  node [
    id 104
    label "warsztat"
  ]
  node [
    id 105
    label "grodzisko"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 9
    target 10
  ]
]
