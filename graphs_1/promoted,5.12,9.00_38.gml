graph [
  maxDegree 23
  minDegree 1
  meanDegree 1.9827586206896552
  density 0.017241379310344827
  graphCliqueNumber 2
  node [
    id 0
    label "premier"
    origin "text"
  ]
  node [
    id 1
    label "classic"
    origin "text"
  ]
  node [
    id 2
    label "mie&#263;"
    origin "text"
  ]
  node [
    id 3
    label "miejsce"
    origin "text"
  ]
  node [
    id 4
    label "dopiero"
    origin "text"
  ]
  node [
    id 5
    label "wczoraj"
    origin "text"
  ]
  node [
    id 6
    label "u&#380;ytkownik"
    origin "text"
  ]
  node [
    id 7
    label "ten"
    origin "text"
  ]
  node [
    id 8
    label "konsola"
    origin "text"
  ]
  node [
    id 9
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 10
    label "uda&#263;"
    origin "text"
  ]
  node [
    id 11
    label "si&#281;"
    origin "text"
  ]
  node [
    id 12
    label "dosta&#263;"
    origin "text"
  ]
  node [
    id 13
    label "ustawienie"
    origin "text"
  ]
  node [
    id 14
    label "emulator"
    origin "text"
  ]
  node [
    id 15
    label "zmienia&#263;"
    origin "text"
  ]
  node [
    id 16
    label "opcja"
    origin "text"
  ]
  node [
    id 17
    label "systemowy"
    origin "text"
  ]
  node [
    id 18
    label "Jelcyn"
  ]
  node [
    id 19
    label "Sto&#322;ypin"
  ]
  node [
    id 20
    label "dostojnik"
  ]
  node [
    id 21
    label "Chruszczow"
  ]
  node [
    id 22
    label "Miko&#322;ajczyk"
  ]
  node [
    id 23
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 24
    label "Bismarck"
  ]
  node [
    id 25
    label "rz&#261;d"
  ]
  node [
    id 26
    label "zwierzchnik"
  ]
  node [
    id 27
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 28
    label "czu&#263;"
  ]
  node [
    id 29
    label "need"
  ]
  node [
    id 30
    label "hide"
  ]
  node [
    id 31
    label "support"
  ]
  node [
    id 32
    label "cia&#322;o"
  ]
  node [
    id 33
    label "plac"
  ]
  node [
    id 34
    label "cecha"
  ]
  node [
    id 35
    label "uwaga"
  ]
  node [
    id 36
    label "przestrze&#324;"
  ]
  node [
    id 37
    label "status"
  ]
  node [
    id 38
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 39
    label "chwila"
  ]
  node [
    id 40
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 41
    label "praca"
  ]
  node [
    id 42
    label "location"
  ]
  node [
    id 43
    label "warunek_lokalowy"
  ]
  node [
    id 44
    label "doba"
  ]
  node [
    id 45
    label "dawno"
  ]
  node [
    id 46
    label "niedawno"
  ]
  node [
    id 47
    label "j&#281;zykowo"
  ]
  node [
    id 48
    label "podmiot"
  ]
  node [
    id 49
    label "okre&#347;lony"
  ]
  node [
    id 50
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 51
    label "ozdobny"
  ]
  node [
    id 52
    label "stolik"
  ]
  node [
    id 53
    label "urz&#261;dzenie"
  ]
  node [
    id 54
    label "pulpit"
  ]
  node [
    id 55
    label "wspornik"
  ]
  node [
    id 56
    label "tremo"
  ]
  node [
    id 57
    label "pad"
  ]
  node [
    id 58
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 59
    label "represent"
  ]
  node [
    id 60
    label "get"
  ]
  node [
    id 61
    label "doczeka&#263;"
  ]
  node [
    id 62
    label "zwiastun"
  ]
  node [
    id 63
    label "si&#281;gn&#261;&#263;"
  ]
  node [
    id 64
    label "develop"
  ]
  node [
    id 65
    label "catch"
  ]
  node [
    id 66
    label "uzyska&#263;"
  ]
  node [
    id 67
    label "kupi&#263;"
  ]
  node [
    id 68
    label "wzi&#261;&#263;"
  ]
  node [
    id 69
    label "naby&#263;"
  ]
  node [
    id 70
    label "nabawienie_si&#281;"
  ]
  node [
    id 71
    label "obskoczy&#263;"
  ]
  node [
    id 72
    label "zapanowa&#263;"
  ]
  node [
    id 73
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 74
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 75
    label "zrobi&#263;"
  ]
  node [
    id 76
    label "nabawianie_si&#281;"
  ]
  node [
    id 77
    label "range"
  ]
  node [
    id 78
    label "schorzenie"
  ]
  node [
    id 79
    label "wystarczy&#263;"
  ]
  node [
    id 80
    label "wysta&#263;"
  ]
  node [
    id 81
    label "zinterpretowanie"
  ]
  node [
    id 82
    label "roz&#322;o&#380;enie"
  ]
  node [
    id 83
    label "erection"
  ]
  node [
    id 84
    label "czynno&#347;&#263;"
  ]
  node [
    id 85
    label "spowodowanie"
  ]
  node [
    id 86
    label "erecting"
  ]
  node [
    id 87
    label "setup"
  ]
  node [
    id 88
    label "ustalenie"
  ]
  node [
    id 89
    label "u&#322;o&#380;enie"
  ]
  node [
    id 90
    label "porozstawianie"
  ]
  node [
    id 91
    label "rola"
  ]
  node [
    id 92
    label "rozmieszczenie"
  ]
  node [
    id 93
    label "poustawianie"
  ]
  node [
    id 94
    label "program"
  ]
  node [
    id 95
    label "reengineering"
  ]
  node [
    id 96
    label "alternate"
  ]
  node [
    id 97
    label "przechodzi&#263;"
  ]
  node [
    id 98
    label "zast&#281;powa&#263;"
  ]
  node [
    id 99
    label "sprawia&#263;"
  ]
  node [
    id 100
    label "traci&#263;"
  ]
  node [
    id 101
    label "zyskiwa&#263;"
  ]
  node [
    id 102
    label "change"
  ]
  node [
    id 103
    label "zesp&#243;&#322;"
  ]
  node [
    id 104
    label "choice"
  ]
  node [
    id 105
    label "instrument_obrotu_gie&#322;dowego"
  ]
  node [
    id 106
    label "kontrakt_opcyjny"
  ]
  node [
    id 107
    label "ewentualno&#347;&#263;"
  ]
  node [
    id 108
    label "instrument_pochodny_o_niesymetrycznym_podziale_ryzyka"
  ]
  node [
    id 109
    label "zgodny"
  ]
  node [
    id 110
    label "globalny"
  ]
  node [
    id 111
    label "systemowo"
  ]
  node [
    id 112
    label "zale&#380;ny"
  ]
  node [
    id 113
    label "metodyczny"
  ]
  node [
    id 114
    label "PS"
  ]
  node [
    id 115
    label "Classic"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 58
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 60
  ]
  edge [
    source 12
    target 61
  ]
  edge [
    source 12
    target 62
  ]
  edge [
    source 12
    target 63
  ]
  edge [
    source 12
    target 64
  ]
  edge [
    source 12
    target 65
  ]
  edge [
    source 12
    target 66
  ]
  edge [
    source 12
    target 67
  ]
  edge [
    source 12
    target 68
  ]
  edge [
    source 12
    target 69
  ]
  edge [
    source 12
    target 70
  ]
  edge [
    source 12
    target 71
  ]
  edge [
    source 12
    target 72
  ]
  edge [
    source 12
    target 73
  ]
  edge [
    source 12
    target 74
  ]
  edge [
    source 12
    target 75
  ]
  edge [
    source 12
    target 76
  ]
  edge [
    source 12
    target 77
  ]
  edge [
    source 12
    target 78
  ]
  edge [
    source 12
    target 79
  ]
  edge [
    source 12
    target 80
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 81
  ]
  edge [
    source 13
    target 82
  ]
  edge [
    source 13
    target 83
  ]
  edge [
    source 13
    target 84
  ]
  edge [
    source 13
    target 85
  ]
  edge [
    source 13
    target 86
  ]
  edge [
    source 13
    target 87
  ]
  edge [
    source 13
    target 88
  ]
  edge [
    source 13
    target 89
  ]
  edge [
    source 13
    target 90
  ]
  edge [
    source 13
    target 91
  ]
  edge [
    source 13
    target 92
  ]
  edge [
    source 13
    target 93
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 94
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 95
  ]
  edge [
    source 15
    target 96
  ]
  edge [
    source 15
    target 97
  ]
  edge [
    source 15
    target 98
  ]
  edge [
    source 15
    target 99
  ]
  edge [
    source 15
    target 100
  ]
  edge [
    source 15
    target 101
  ]
  edge [
    source 15
    target 102
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 103
  ]
  edge [
    source 16
    target 104
  ]
  edge [
    source 16
    target 105
  ]
  edge [
    source 16
    target 106
  ]
  edge [
    source 16
    target 107
  ]
  edge [
    source 16
    target 108
  ]
  edge [
    source 17
    target 109
  ]
  edge [
    source 17
    target 110
  ]
  edge [
    source 17
    target 111
  ]
  edge [
    source 17
    target 112
  ]
  edge [
    source 17
    target 113
  ]
  edge [
    source 114
    target 115
  ]
]
