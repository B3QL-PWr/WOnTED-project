graph [
  maxDegree 31
  minDegree 1
  meanDegree 2.0806451612903225
  density 0.016915814319433516
  graphCliqueNumber 5
  node [
    id 0
    label "droga"
    origin "text"
  ]
  node [
    id 1
    label "klient"
    origin "text"
  ]
  node [
    id 2
    label "chcie&#263;by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "poinformowa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "rebtel"
    origin "text"
  ]
  node [
    id 5
    label "zdecydowa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "si&#281;"
    origin "text"
  ]
  node [
    id 7
    label "zako&#324;czenie"
    origin "text"
  ]
  node [
    id 8
    label "&#347;wiadczenie"
    origin "text"
  ]
  node [
    id 9
    label "us&#322;uga"
    origin "text"
  ]
  node [
    id 10
    label "polska"
    origin "text"
  ]
  node [
    id 11
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 12
    label "stycze&#324;"
    origin "text"
  ]
  node [
    id 13
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 14
    label "journey"
  ]
  node [
    id 15
    label "podbieg"
  ]
  node [
    id 16
    label "bezsilnikowy"
  ]
  node [
    id 17
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 18
    label "wylot"
  ]
  node [
    id 19
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 20
    label "drogowskaz"
  ]
  node [
    id 21
    label "nawierzchnia"
  ]
  node [
    id 22
    label "turystyka"
  ]
  node [
    id 23
    label "budowla"
  ]
  node [
    id 24
    label "spos&#243;b"
  ]
  node [
    id 25
    label "passage"
  ]
  node [
    id 26
    label "marszrutyzacja"
  ]
  node [
    id 27
    label "zbior&#243;wka"
  ]
  node [
    id 28
    label "rajza"
  ]
  node [
    id 29
    label "ekskursja"
  ]
  node [
    id 30
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 31
    label "ruch"
  ]
  node [
    id 32
    label "trasa"
  ]
  node [
    id 33
    label "wyb&#243;j"
  ]
  node [
    id 34
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 35
    label "ekwipunek"
  ]
  node [
    id 36
    label "korona_drogi"
  ]
  node [
    id 37
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 38
    label "pobocze"
  ]
  node [
    id 39
    label "cz&#322;owiek"
  ]
  node [
    id 40
    label "bratek"
  ]
  node [
    id 41
    label "klientela"
  ]
  node [
    id 42
    label "szlachcic"
  ]
  node [
    id 43
    label "agent_rozliczeniowy"
  ]
  node [
    id 44
    label "komputer_cyfrowy"
  ]
  node [
    id 45
    label "program"
  ]
  node [
    id 46
    label "us&#322;ugobiorca"
  ]
  node [
    id 47
    label "Rzymianin"
  ]
  node [
    id 48
    label "obywatel"
  ]
  node [
    id 49
    label "zakomunikowa&#263;"
  ]
  node [
    id 50
    label "inform"
  ]
  node [
    id 51
    label "sta&#263;_si&#281;"
  ]
  node [
    id 52
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 53
    label "podj&#261;&#263;"
  ]
  node [
    id 54
    label "determine"
  ]
  node [
    id 55
    label "decide"
  ]
  node [
    id 56
    label "zrobi&#263;"
  ]
  node [
    id 57
    label "dzia&#322;anie"
  ]
  node [
    id 58
    label "zrobienie"
  ]
  node [
    id 59
    label "koniec"
  ]
  node [
    id 60
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 61
    label "ko&#324;c&#243;wka"
  ]
  node [
    id 62
    label "zrezygnowanie"
  ]
  node [
    id 63
    label "closing"
  ]
  node [
    id 64
    label "adjustment"
  ]
  node [
    id 65
    label "ukszta&#322;towanie"
  ]
  node [
    id 66
    label "conclusion"
  ]
  node [
    id 67
    label "termination"
  ]
  node [
    id 68
    label "closure"
  ]
  node [
    id 69
    label "p&#322;acenie"
  ]
  node [
    id 70
    label "znaczenie"
  ]
  node [
    id 71
    label "sk&#322;adanie"
  ]
  node [
    id 72
    label "service"
  ]
  node [
    id 73
    label "czynienie_dobra"
  ]
  node [
    id 74
    label "informowanie"
  ]
  node [
    id 75
    label "command"
  ]
  node [
    id 76
    label "opowiadanie"
  ]
  node [
    id 77
    label "koszt_rodzajowy"
  ]
  node [
    id 78
    label "pracowanie"
  ]
  node [
    id 79
    label "przekonywanie"
  ]
  node [
    id 80
    label "wyraz"
  ]
  node [
    id 81
    label "performance"
  ]
  node [
    id 82
    label "zobowi&#261;zanie"
  ]
  node [
    id 83
    label "produkt_gotowy"
  ]
  node [
    id 84
    label "czynno&#347;&#263;"
  ]
  node [
    id 85
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 86
    label "asortyment"
  ]
  node [
    id 87
    label "s&#322;o&#324;ce"
  ]
  node [
    id 88
    label "czynienie_si&#281;"
  ]
  node [
    id 89
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 90
    label "czas"
  ]
  node [
    id 91
    label "long_time"
  ]
  node [
    id 92
    label "przedpo&#322;udnie"
  ]
  node [
    id 93
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 94
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 95
    label "tydzie&#324;"
  ]
  node [
    id 96
    label "godzina"
  ]
  node [
    id 97
    label "t&#322;usty_czwartek"
  ]
  node [
    id 98
    label "wsta&#263;"
  ]
  node [
    id 99
    label "day"
  ]
  node [
    id 100
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 101
    label "przedwiecz&#243;r"
  ]
  node [
    id 102
    label "Sylwester"
  ]
  node [
    id 103
    label "po&#322;udnie"
  ]
  node [
    id 104
    label "wzej&#347;cie"
  ]
  node [
    id 105
    label "podwiecz&#243;r"
  ]
  node [
    id 106
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 107
    label "rano"
  ]
  node [
    id 108
    label "termin"
  ]
  node [
    id 109
    label "ranek"
  ]
  node [
    id 110
    label "doba"
  ]
  node [
    id 111
    label "wiecz&#243;r"
  ]
  node [
    id 112
    label "walentynki"
  ]
  node [
    id 113
    label "popo&#322;udnie"
  ]
  node [
    id 114
    label "noc"
  ]
  node [
    id 115
    label "wstanie"
  ]
  node [
    id 116
    label "Nowy_Rok"
  ]
  node [
    id 117
    label "miesi&#261;c"
  ]
  node [
    id 118
    label "Rebtel"
  ]
  node [
    id 119
    label "Services"
  ]
  node [
    id 120
    label "sekunda"
  ]
  node [
    id 121
    label "&#224;"
  ]
  node [
    id 122
    label "rl"
  ]
  node [
    id 123
    label "Poland"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 87
  ]
  edge [
    source 11
    target 88
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 11
    target 92
  ]
  edge [
    source 11
    target 93
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 11
    target 98
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 11
    target 100
  ]
  edge [
    source 11
    target 101
  ]
  edge [
    source 11
    target 102
  ]
  edge [
    source 11
    target 103
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 108
  ]
  edge [
    source 11
    target 109
  ]
  edge [
    source 11
    target 110
  ]
  edge [
    source 11
    target 111
  ]
  edge [
    source 11
    target 112
  ]
  edge [
    source 11
    target 113
  ]
  edge [
    source 11
    target 114
  ]
  edge [
    source 11
    target 115
  ]
  edge [
    source 12
    target 116
  ]
  edge [
    source 12
    target 117
  ]
  edge [
    source 118
    target 119
  ]
  edge [
    source 118
    target 120
  ]
  edge [
    source 118
    target 121
  ]
  edge [
    source 118
    target 122
  ]
  edge [
    source 118
    target 123
  ]
  edge [
    source 119
    target 120
  ]
  edge [
    source 119
    target 121
  ]
  edge [
    source 119
    target 122
  ]
  edge [
    source 120
    target 121
  ]
  edge [
    source 120
    target 122
  ]
  edge [
    source 121
    target 122
  ]
]
