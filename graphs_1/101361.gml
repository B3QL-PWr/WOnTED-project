graph [
  maxDegree 16
  minDegree 1
  meanDegree 3.43859649122807
  density 0.06140350877192982
  graphCliqueNumber 7
  node [
    id 0
    label "centrum"
    origin "text"
  ]
  node [
    id 1
    label "kszta&#322;cenie"
    origin "text"
  ]
  node [
    id 2
    label "in&#380;ynier"
    origin "text"
  ]
  node [
    id 3
    label "miejsce"
  ]
  node [
    id 4
    label "centroprawica"
  ]
  node [
    id 5
    label "core"
  ]
  node [
    id 6
    label "Hollywood"
  ]
  node [
    id 7
    label "centrolew"
  ]
  node [
    id 8
    label "blok"
  ]
  node [
    id 9
    label "sejm"
  ]
  node [
    id 10
    label "punkt"
  ]
  node [
    id 11
    label "o&#347;rodek"
  ]
  node [
    id 12
    label "zapoznawanie"
  ]
  node [
    id 13
    label "wysy&#322;anie"
  ]
  node [
    id 14
    label "formation"
  ]
  node [
    id 15
    label "rozwijanie"
  ]
  node [
    id 16
    label "training"
  ]
  node [
    id 17
    label "pomaganie"
  ]
  node [
    id 18
    label "o&#347;wiecanie"
  ]
  node [
    id 19
    label "kliker"
  ]
  node [
    id 20
    label "heureza"
  ]
  node [
    id 21
    label "pouczenie"
  ]
  node [
    id 22
    label "nauka"
  ]
  node [
    id 23
    label "inteligent"
  ]
  node [
    id 24
    label "Pierre-&#201;mile_Martin"
  ]
  node [
    id 25
    label "fachowiec"
  ]
  node [
    id 26
    label "Tesla"
  ]
  node [
    id 27
    label "tytu&#322;"
  ]
  node [
    id 28
    label "kszta&#322;ci&#263;"
  ]
  node [
    id 29
    label "politechnika"
  ]
  node [
    id 30
    label "&#347;l&#261;ski"
  ]
  node [
    id 31
    label "budynek"
  ]
  node [
    id 32
    label "g&#322;&#243;wny"
  ]
  node [
    id 33
    label "tom"
  ]
  node [
    id 34
    label "Ko&#347;ciuszko"
  ]
  node [
    id 35
    label "laboratorium"
  ]
  node [
    id 36
    label "nowocze&#347;ni"
  ]
  node [
    id 37
    label "technologia"
  ]
  node [
    id 38
    label "przemys&#322;owy"
  ]
  node [
    id 39
    label "informatyczny"
  ]
  node [
    id 40
    label "studencki"
  ]
  node [
    id 41
    label "ko&#322;o"
  ]
  node [
    id 42
    label "naukowy"
  ]
  node [
    id 43
    label "energetyka"
  ]
  node [
    id 44
    label "komunalny"
  ]
  node [
    id 45
    label "Linuksa"
  ]
  node [
    id 46
    label "i"
  ]
  node [
    id 47
    label "wolny"
  ]
  node [
    id 48
    label "oprogramowa&#263;"
  ]
  node [
    id 49
    label "trwa&#322;o&#347;&#263;"
  ]
  node [
    id 50
    label "audytor"
  ]
  node [
    id 51
    label "energetyczny"
  ]
  node [
    id 52
    label "wyspa"
  ]
  node [
    id 53
    label "rybnik"
  ]
  node [
    id 54
    label "skaner"
  ]
  node [
    id 55
    label "mi&#322;o&#347;nik"
  ]
  node [
    id 56
    label "algorytm"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 37
  ]
  edge [
    source 35
    target 38
  ]
  edge [
    source 35
    target 39
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 38
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 42
  ]
  edge [
    source 40
    target 43
  ]
  edge [
    source 40
    target 44
  ]
  edge [
    source 40
    target 45
  ]
  edge [
    source 40
    target 46
  ]
  edge [
    source 40
    target 47
  ]
  edge [
    source 40
    target 48
  ]
  edge [
    source 40
    target 49
  ]
  edge [
    source 40
    target 50
  ]
  edge [
    source 40
    target 51
  ]
  edge [
    source 40
    target 52
  ]
  edge [
    source 40
    target 53
  ]
  edge [
    source 40
    target 54
  ]
  edge [
    source 40
    target 55
  ]
  edge [
    source 40
    target 56
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 43
  ]
  edge [
    source 41
    target 44
  ]
  edge [
    source 41
    target 45
  ]
  edge [
    source 41
    target 46
  ]
  edge [
    source 41
    target 47
  ]
  edge [
    source 41
    target 48
  ]
  edge [
    source 41
    target 49
  ]
  edge [
    source 41
    target 50
  ]
  edge [
    source 41
    target 51
  ]
  edge [
    source 41
    target 52
  ]
  edge [
    source 41
    target 53
  ]
  edge [
    source 41
    target 54
  ]
  edge [
    source 41
    target 55
  ]
  edge [
    source 41
    target 56
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 44
  ]
  edge [
    source 42
    target 45
  ]
  edge [
    source 42
    target 46
  ]
  edge [
    source 42
    target 47
  ]
  edge [
    source 42
    target 48
  ]
  edge [
    source 42
    target 49
  ]
  edge [
    source 42
    target 55
  ]
  edge [
    source 42
    target 56
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 47
  ]
  edge [
    source 45
    target 48
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 48
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 52
  ]
  edge [
    source 50
    target 53
  ]
  edge [
    source 50
    target 54
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 53
  ]
  edge [
    source 51
    target 54
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 54
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 55
    target 56
  ]
]
