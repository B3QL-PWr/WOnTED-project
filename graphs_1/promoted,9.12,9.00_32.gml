graph [
  maxDegree 28
  minDegree 1
  meanDegree 2.021978021978022
  density 0.022466422466422466
  graphCliqueNumber 4
  node [
    id 0
    label "chi&#324;ski"
    origin "text"
  ]
  node [
    id 1
    label "sonda"
    origin "text"
  ]
  node [
    id 2
    label "kosmiczny"
    origin "text"
  ]
  node [
    id 3
    label "chang"
    origin "text"
  ]
  node [
    id 4
    label "by&#263;"
    origin "text"
  ]
  node [
    id 5
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 6
    label "droga"
    origin "text"
  ]
  node [
    id 7
    label "ksi&#281;&#380;yc"
    origin "text"
  ]
  node [
    id 8
    label "kitajski"
  ]
  node [
    id 9
    label "j&#281;zyk"
  ]
  node [
    id 10
    label "kaczka_po_peki&#324;sku"
  ]
  node [
    id 11
    label "dziwaczny"
  ]
  node [
    id 12
    label "tandetny"
  ]
  node [
    id 13
    label "makroj&#281;zyk"
  ]
  node [
    id 14
    label "chi&#324;sko"
  ]
  node [
    id 15
    label "po_chi&#324;sku"
  ]
  node [
    id 16
    label "j&#281;zyk_chi&#324;sko-tybeta&#324;ski"
  ]
  node [
    id 17
    label "azjatycki"
  ]
  node [
    id 18
    label "lipny"
  ]
  node [
    id 19
    label "go"
  ]
  node [
    id 20
    label "niedrogi"
  ]
  node [
    id 21
    label "dalekowschodni"
  ]
  node [
    id 22
    label "sonda&#380;"
  ]
  node [
    id 23
    label "przyrz&#261;d"
  ]
  node [
    id 24
    label "leash"
  ]
  node [
    id 25
    label "badanie"
  ]
  node [
    id 26
    label "narz&#281;dzie"
  ]
  node [
    id 27
    label "lead"
  ]
  node [
    id 28
    label "statek_kosmiczny"
  ]
  node [
    id 29
    label "specjalny"
  ]
  node [
    id 30
    label "kosmicznie"
  ]
  node [
    id 31
    label "nieziemski"
  ]
  node [
    id 32
    label "futurystyczny"
  ]
  node [
    id 33
    label "pot&#281;&#380;ny"
  ]
  node [
    id 34
    label "astralny"
  ]
  node [
    id 35
    label "ciekawy"
  ]
  node [
    id 36
    label "pozaziemski"
  ]
  node [
    id 37
    label "tajemniczy"
  ]
  node [
    id 38
    label "niestworzony"
  ]
  node [
    id 39
    label "olbrzymi"
  ]
  node [
    id 40
    label "si&#281;ga&#263;"
  ]
  node [
    id 41
    label "trwa&#263;"
  ]
  node [
    id 42
    label "obecno&#347;&#263;"
  ]
  node [
    id 43
    label "stan"
  ]
  node [
    id 44
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 45
    label "stand"
  ]
  node [
    id 46
    label "mie&#263;_miejsce"
  ]
  node [
    id 47
    label "uczestniczy&#263;"
  ]
  node [
    id 48
    label "chodzi&#263;"
  ]
  node [
    id 49
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 50
    label "equal"
  ]
  node [
    id 51
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 52
    label "journey"
  ]
  node [
    id 53
    label "podbieg"
  ]
  node [
    id 54
    label "bezsilnikowy"
  ]
  node [
    id 55
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 56
    label "wylot"
  ]
  node [
    id 57
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 58
    label "drogowskaz"
  ]
  node [
    id 59
    label "nawierzchnia"
  ]
  node [
    id 60
    label "turystyka"
  ]
  node [
    id 61
    label "budowla"
  ]
  node [
    id 62
    label "spos&#243;b"
  ]
  node [
    id 63
    label "passage"
  ]
  node [
    id 64
    label "marszrutyzacja"
  ]
  node [
    id 65
    label "zbior&#243;wka"
  ]
  node [
    id 66
    label "ekskursja"
  ]
  node [
    id 67
    label "rajza"
  ]
  node [
    id 68
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 69
    label "ruch"
  ]
  node [
    id 70
    label "trasa"
  ]
  node [
    id 71
    label "wyb&#243;j"
  ]
  node [
    id 72
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 73
    label "ekwipunek"
  ]
  node [
    id 74
    label "korona_drogi"
  ]
  node [
    id 75
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 76
    label "pobocze"
  ]
  node [
    id 77
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 78
    label "faza_Ksi&#281;&#380;yca"
  ]
  node [
    id 79
    label "moon"
  ]
  node [
    id 80
    label "&#347;wiat&#322;o"
  ]
  node [
    id 81
    label "aposelenium"
  ]
  node [
    id 82
    label "peryselenium"
  ]
  node [
    id 83
    label "Tytan"
  ]
  node [
    id 84
    label "kalendarz_ksi&#281;&#380;ycowy"
  ]
  node [
    id 85
    label "miesi&#261;c"
  ]
  node [
    id 86
    label "satelita"
  ]
  node [
    id 87
    label "Chang"
  ]
  node [
    id 88
    label "&#8217;"
  ]
  node [
    id 89
    label "e"
  ]
  node [
    id 90
    label "4"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 87
    target 88
  ]
  edge [
    source 87
    target 89
  ]
  edge [
    source 87
    target 90
  ]
  edge [
    source 88
    target 89
  ]
  edge [
    source 88
    target 90
  ]
  edge [
    source 89
    target 90
  ]
]
