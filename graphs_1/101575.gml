graph [
  maxDegree 25
  minDegree 1
  meanDegree 2.0294117647058822
  density 0.03028972783143108
  graphCliqueNumber 4
  node [
    id 0
    label "naczelnik"
    origin "text"
  ]
  node [
    id 1
    label "og&#322;osi&#263;"
    origin "text"
  ]
  node [
    id 2
    label "rozkaz"
    origin "text"
  ]
  node [
    id 3
    label "chcie&#263;"
    origin "text"
  ]
  node [
    id 4
    label "p&#243;j&#347;&#263;"
    origin "text"
  ]
  node [
    id 5
    label "daleko"
    origin "text"
  ]
  node [
    id 6
    label "gdy"
    origin "text"
  ]
  node [
    id 7
    label "tema"
    origin "text"
  ]
  node [
    id 8
    label "keraban"
    origin "text"
  ]
  node [
    id 9
    label "zbli&#380;y&#263;"
    origin "text"
  ]
  node [
    id 10
    label "si&#281;"
    origin "text"
  ]
  node [
    id 11
    label "naczelnikostwo"
  ]
  node [
    id 12
    label "zwierzchnik"
  ]
  node [
    id 13
    label "dow&#243;dca"
  ]
  node [
    id 14
    label "communicate"
  ]
  node [
    id 15
    label "opublikowa&#263;"
  ]
  node [
    id 16
    label "obwo&#322;a&#263;"
  ]
  node [
    id 17
    label "poda&#263;"
  ]
  node [
    id 18
    label "publish"
  ]
  node [
    id 19
    label "declare"
  ]
  node [
    id 20
    label "komender&#243;wka"
  ]
  node [
    id 21
    label "polecenie"
  ]
  node [
    id 22
    label "direction"
  ]
  node [
    id 23
    label "czu&#263;"
  ]
  node [
    id 24
    label "desire"
  ]
  node [
    id 25
    label "kcie&#263;"
  ]
  node [
    id 26
    label "opu&#347;ci&#263;"
  ]
  node [
    id 27
    label "poruszy&#263;_si&#281;"
  ]
  node [
    id 28
    label "proceed"
  ]
  node [
    id 29
    label "powie&#347;&#263;_si&#281;"
  ]
  node [
    id 30
    label "popsu&#263;_si&#281;"
  ]
  node [
    id 31
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 32
    label "kopn&#261;&#263;_si&#281;"
  ]
  node [
    id 33
    label "zacz&#261;&#263;"
  ]
  node [
    id 34
    label "zmieni&#263;"
  ]
  node [
    id 35
    label "zosta&#263;"
  ]
  node [
    id 36
    label "sail"
  ]
  node [
    id 37
    label "leave"
  ]
  node [
    id 38
    label "uda&#263;_si&#281;"
  ]
  node [
    id 39
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 40
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 41
    label "zrobi&#263;"
  ]
  node [
    id 42
    label "wyp&#322;yn&#261;&#263;"
  ]
  node [
    id 43
    label "sprzeda&#263;_si&#281;"
  ]
  node [
    id 44
    label "przyj&#261;&#263;"
  ]
  node [
    id 45
    label "potoczy&#263;_si&#281;"
  ]
  node [
    id 46
    label "become"
  ]
  node [
    id 47
    label "play_along"
  ]
  node [
    id 48
    label "travel"
  ]
  node [
    id 49
    label "dawno"
  ]
  node [
    id 50
    label "nisko"
  ]
  node [
    id 51
    label "nieobecnie"
  ]
  node [
    id 52
    label "daleki"
  ]
  node [
    id 53
    label "het"
  ]
  node [
    id 54
    label "wysoko"
  ]
  node [
    id 55
    label "du&#380;o"
  ]
  node [
    id 56
    label "znacznie"
  ]
  node [
    id 57
    label "g&#322;&#281;boko"
  ]
  node [
    id 58
    label "Cesarstwo_Bizanty&#324;skie"
  ]
  node [
    id 59
    label "jednostka_administracyjna"
  ]
  node [
    id 60
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 61
    label "approach"
  ]
  node [
    id 62
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 63
    label "set_about"
  ]
  node [
    id 64
    label "van"
  ]
  node [
    id 65
    label "Mitten"
  ]
  node [
    id 66
    label "&#8217;"
  ]
  node [
    id 67
    label "albo"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 66
  ]
  edge [
    source 64
    target 67
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 67
  ]
  edge [
    source 66
    target 67
  ]
]
