graph [
  maxDegree 10
  minDegree 1
  meanDegree 1.8823529411764706
  density 0.11764705882352941
  graphCliqueNumber 2
  node [
    id 0
    label "kurwa"
    origin "text"
  ]
  node [
    id 1
    label "znowu"
    origin "text"
  ]
  node [
    id 2
    label "ten"
    origin "text"
  ]
  node [
    id 3
    label "paprykarz"
    origin "text"
  ]
  node [
    id 4
    label "szmaciarz"
  ]
  node [
    id 5
    label "zo&#322;za"
  ]
  node [
    id 6
    label "skurwienie_si&#281;"
  ]
  node [
    id 7
    label "kurwienie_si&#281;"
  ]
  node [
    id 8
    label "karze&#322;"
  ]
  node [
    id 9
    label "kurcz&#281;"
  ]
  node [
    id 10
    label "wyzwisko"
  ]
  node [
    id 11
    label "przekle&#324;stwo"
  ]
  node [
    id 12
    label "prostytutka"
  ]
  node [
    id 13
    label "okre&#347;lony"
  ]
  node [
    id 14
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 15
    label "potrawa"
  ]
  node [
    id 16
    label "pasztet"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
]
