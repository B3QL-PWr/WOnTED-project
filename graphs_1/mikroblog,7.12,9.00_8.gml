graph [
  maxDegree 46
  minDegree 1
  meanDegree 1.96
  density 0.04
  graphCliqueNumber 2
  node [
    id 0
    label "granie"
    origin "text"
  ]
  node [
    id 1
    label "trzeba"
    origin "text"
  ]
  node [
    id 2
    label "dwa"
    origin "text"
  ]
  node [
    id 3
    label "wybicie"
  ]
  node [
    id 4
    label "robienie"
  ]
  node [
    id 5
    label "nagranie_si&#281;"
  ]
  node [
    id 6
    label "uzewn&#281;trznianie_si&#281;"
  ]
  node [
    id 7
    label "pogranie"
  ]
  node [
    id 8
    label "rozgrywanie"
  ]
  node [
    id 9
    label "rola"
  ]
  node [
    id 10
    label "ust&#281;powanie"
  ]
  node [
    id 11
    label "wyst&#281;powanie"
  ]
  node [
    id 12
    label "dogranie"
  ]
  node [
    id 13
    label "playing"
  ]
  node [
    id 14
    label "przygrywanie"
  ]
  node [
    id 15
    label "odegranie_si&#281;"
  ]
  node [
    id 16
    label "migotanie"
  ]
  node [
    id 17
    label "dzianie_si&#281;"
  ]
  node [
    id 18
    label "&#347;ciganie"
  ]
  node [
    id 19
    label "czynno&#347;&#263;"
  ]
  node [
    id 20
    label "wyr&#243;wnanie"
  ]
  node [
    id 21
    label "na&#347;ladowanie"
  ]
  node [
    id 22
    label "igranie"
  ]
  node [
    id 23
    label "pr&#243;bowanie"
  ]
  node [
    id 24
    label "szczekanie"
  ]
  node [
    id 25
    label "brzmienie"
  ]
  node [
    id 26
    label "odgrywanie_si&#281;"
  ]
  node [
    id 27
    label "wybijanie"
  ]
  node [
    id 28
    label "przedstawianie"
  ]
  node [
    id 29
    label "instrument_muzyczny"
  ]
  node [
    id 30
    label "pretense"
  ]
  node [
    id 31
    label "lewa"
  ]
  node [
    id 32
    label "staranie_si&#281;"
  ]
  node [
    id 33
    label "dogrywanie"
  ]
  node [
    id 34
    label "glitter"
  ]
  node [
    id 35
    label "gra_w_karty"
  ]
  node [
    id 36
    label "otwarcie"
  ]
  node [
    id 37
    label "rozegranie_si&#281;"
  ]
  node [
    id 38
    label "pasowanie"
  ]
  node [
    id 39
    label "wykonywanie"
  ]
  node [
    id 40
    label "uderzenie"
  ]
  node [
    id 41
    label "wyr&#243;wnywanie"
  ]
  node [
    id 42
    label "grywanie"
  ]
  node [
    id 43
    label "zwalczenie"
  ]
  node [
    id 44
    label "mienienie_si&#281;"
  ]
  node [
    id 45
    label "instrumentalizacja"
  ]
  node [
    id 46
    label "prezentowanie"
  ]
  node [
    id 47
    label "wydawanie"
  ]
  node [
    id 48
    label "trza"
  ]
  node [
    id 49
    label "necessity"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
]
