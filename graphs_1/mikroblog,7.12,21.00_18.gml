graph [
  maxDegree 21
  minDegree 1
  meanDegree 2.176470588235294
  density 0.0324846356453029
  graphCliqueNumber 6
  node [
    id 0
    label "kot"
    origin "text"
  ]
  node [
    id 1
    label "maja"
    origin "text"
  ]
  node [
    id 2
    label "dobry"
    origin "text"
  ]
  node [
    id 3
    label "pami&#281;&#263;"
    origin "text"
  ]
  node [
    id 4
    label "zamiaucze&#263;"
  ]
  node [
    id 5
    label "pierwszoklasista"
  ]
  node [
    id 6
    label "fala"
  ]
  node [
    id 7
    label "miaucze&#263;"
  ]
  node [
    id 8
    label "kabanos"
  ]
  node [
    id 9
    label "kotowate"
  ]
  node [
    id 10
    label "miaukni&#281;cie"
  ]
  node [
    id 11
    label "samiec"
  ]
  node [
    id 12
    label "otrz&#281;siny"
  ]
  node [
    id 13
    label "miauczenie"
  ]
  node [
    id 14
    label "czworon&#243;g"
  ]
  node [
    id 15
    label "ssak_drapie&#380;ny"
  ]
  node [
    id 16
    label "rekrut"
  ]
  node [
    id 17
    label "zaj&#261;c"
  ]
  node [
    id 18
    label "kotwica"
  ]
  node [
    id 19
    label "trackball"
  ]
  node [
    id 20
    label "felinoterapia"
  ]
  node [
    id 21
    label "odk&#322;aczacz"
  ]
  node [
    id 22
    label "zamiauczenie"
  ]
  node [
    id 23
    label "wedyzm"
  ]
  node [
    id 24
    label "energia"
  ]
  node [
    id 25
    label "buddyzm"
  ]
  node [
    id 26
    label "pomy&#347;lny"
  ]
  node [
    id 27
    label "skuteczny"
  ]
  node [
    id 28
    label "moralny"
  ]
  node [
    id 29
    label "korzystny"
  ]
  node [
    id 30
    label "odpowiedni"
  ]
  node [
    id 31
    label "zwrot"
  ]
  node [
    id 32
    label "dobrze"
  ]
  node [
    id 33
    label "pozytywny"
  ]
  node [
    id 34
    label "grzeczny"
  ]
  node [
    id 35
    label "powitanie"
  ]
  node [
    id 36
    label "mi&#322;y"
  ]
  node [
    id 37
    label "dobroczynny"
  ]
  node [
    id 38
    label "pos&#322;uszny"
  ]
  node [
    id 39
    label "ca&#322;y"
  ]
  node [
    id 40
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 41
    label "czw&#243;rka"
  ]
  node [
    id 42
    label "spokojny"
  ]
  node [
    id 43
    label "&#347;mieszny"
  ]
  node [
    id 44
    label "drogi"
  ]
  node [
    id 45
    label "hipokamp"
  ]
  node [
    id 46
    label "wymazanie"
  ]
  node [
    id 47
    label "wytw&#243;r"
  ]
  node [
    id 48
    label "zachowa&#263;"
  ]
  node [
    id 49
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 50
    label "memory"
  ]
  node [
    id 51
    label "pami&#281;tno&#347;&#263;"
  ]
  node [
    id 52
    label "umys&#322;"
  ]
  node [
    id 53
    label "sprz&#281;t_komputerowy"
  ]
  node [
    id 54
    label "komputer"
  ]
  node [
    id 55
    label "Behavioural"
  ]
  node [
    id 56
    label "Processes"
  ]
  node [
    id 57
    label "Kyoto"
  ]
  node [
    id 58
    label "University"
  ]
  node [
    id 59
    label "John"
  ]
  node [
    id 60
    label "Paul"
  ]
  node [
    id 61
    label "Scott"
  ]
  node [
    id 62
    label "laboratorium"
  ]
  node [
    id 63
    label "zachowanie"
  ]
  node [
    id 64
    label "zwierz&#281;"
  ]
  node [
    id 65
    label "w"
  ]
  node [
    id 66
    label "bar"
  ]
  node [
    id 67
    label "Harbor"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 61
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 64
  ]
  edge [
    source 62
    target 65
  ]
  edge [
    source 62
    target 66
  ]
  edge [
    source 62
    target 67
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 65
  ]
  edge [
    source 63
    target 66
  ]
  edge [
    source 63
    target 67
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 66
  ]
  edge [
    source 64
    target 67
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 67
  ]
  edge [
    source 66
    target 67
  ]
]
