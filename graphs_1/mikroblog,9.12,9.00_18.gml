graph [
  maxDegree 13
  minDegree 1
  meanDegree 1.9285714285714286
  density 0.07142857142857142
  graphCliqueNumber 2
  node [
    id 0
    label "dobra"
    origin "text"
  ]
  node [
    id 1
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "cham"
    origin "text"
  ]
  node [
    id 4
    label "frymark"
  ]
  node [
    id 5
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 6
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 7
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 8
    label "commodity"
  ]
  node [
    id 9
    label "mienie"
  ]
  node [
    id 10
    label "Wilko"
  ]
  node [
    id 11
    label "jednostka_monetarna"
  ]
  node [
    id 12
    label "centym"
  ]
  node [
    id 13
    label "si&#281;ga&#263;"
  ]
  node [
    id 14
    label "trwa&#263;"
  ]
  node [
    id 15
    label "obecno&#347;&#263;"
  ]
  node [
    id 16
    label "stan"
  ]
  node [
    id 17
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 18
    label "stand"
  ]
  node [
    id 19
    label "mie&#263;_miejsce"
  ]
  node [
    id 20
    label "uczestniczy&#263;"
  ]
  node [
    id 21
    label "chodzi&#263;"
  ]
  node [
    id 22
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 23
    label "equal"
  ]
  node [
    id 24
    label "chamstwo"
  ]
  node [
    id 25
    label "prostak"
  ]
  node [
    id 26
    label "ch&#322;op"
  ]
  node [
    id 27
    label "chamski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
]
