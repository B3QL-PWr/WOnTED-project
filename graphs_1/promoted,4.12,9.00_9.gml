graph [
  maxDegree 28
  minDegree 1
  meanDegree 2.02
  density 0.020404040404040404
  graphCliqueNumber 2
  node [
    id 0
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 1
    label "kto&#347;"
    origin "text"
  ]
  node [
    id 2
    label "wyja&#347;ni&#263;"
    origin "text"
  ]
  node [
    id 3
    label "jaki"
    origin "text"
  ]
  node [
    id 4
    label "cel"
    origin "text"
  ]
  node [
    id 5
    label "by&#263;"
    origin "text"
  ]
  node [
    id 6
    label "ci&#261;gn&#261;&#263;"
    origin "text"
  ]
  node [
    id 7
    label "si&#281;"
    origin "text"
  ]
  node [
    id 8
    label "setka"
    origin "text"
  ]
  node [
    id 9
    label "kilometr"
    origin "text"
  ]
  node [
    id 10
    label "pas"
    origin "text"
  ]
  node [
    id 11
    label "ziele&#324;"
    origin "text"
  ]
  node [
    id 12
    label "cz&#322;owiek"
  ]
  node [
    id 13
    label "znaczenie"
  ]
  node [
    id 14
    label "go&#347;&#263;"
  ]
  node [
    id 15
    label "osoba"
  ]
  node [
    id 16
    label "posta&#263;"
  ]
  node [
    id 17
    label "explain"
  ]
  node [
    id 18
    label "przedstawi&#263;"
  ]
  node [
    id 19
    label "poja&#347;ni&#263;"
  ]
  node [
    id 20
    label "clear"
  ]
  node [
    id 21
    label "miejsce"
  ]
  node [
    id 22
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 23
    label "przysz&#322;o&#347;&#263;"
  ]
  node [
    id 24
    label "rzecz"
  ]
  node [
    id 25
    label "punkt"
  ]
  node [
    id 26
    label "thing"
  ]
  node [
    id 27
    label "rezultat"
  ]
  node [
    id 28
    label "si&#281;ga&#263;"
  ]
  node [
    id 29
    label "trwa&#263;"
  ]
  node [
    id 30
    label "obecno&#347;&#263;"
  ]
  node [
    id 31
    label "stan"
  ]
  node [
    id 32
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 33
    label "stand"
  ]
  node [
    id 34
    label "mie&#263;_miejsce"
  ]
  node [
    id 35
    label "uczestniczy&#263;"
  ]
  node [
    id 36
    label "chodzi&#263;"
  ]
  node [
    id 37
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 38
    label "equal"
  ]
  node [
    id 39
    label "proceed"
  ]
  node [
    id 40
    label "imperativeness"
  ]
  node [
    id 41
    label "przeci&#261;ga&#263;"
  ]
  node [
    id 42
    label "force"
  ]
  node [
    id 43
    label "wia&#263;"
  ]
  node [
    id 44
    label "rusza&#263;"
  ]
  node [
    id 45
    label "przewozi&#263;"
  ]
  node [
    id 46
    label "chcie&#263;"
  ]
  node [
    id 47
    label "adhere"
  ]
  node [
    id 48
    label "przesuwa&#263;"
  ]
  node [
    id 49
    label "wch&#322;ania&#263;"
  ]
  node [
    id 50
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 51
    label "radzi&#263;_sobie"
  ]
  node [
    id 52
    label "wyjmowa&#263;"
  ]
  node [
    id 53
    label "wa&#380;y&#263;"
  ]
  node [
    id 54
    label "przemieszcza&#263;"
  ]
  node [
    id 55
    label "set_about"
  ]
  node [
    id 56
    label "robi&#263;"
  ]
  node [
    id 57
    label "obrabia&#263;"
  ]
  node [
    id 58
    label "wyd&#322;u&#380;a&#263;"
  ]
  node [
    id 59
    label "zabiera&#263;"
  ]
  node [
    id 60
    label "wyt&#322;acza&#263;"
  ]
  node [
    id 61
    label "poci&#261;ga&#263;"
  ]
  node [
    id 62
    label "prosecute"
  ]
  node [
    id 63
    label "blow_up"
  ]
  node [
    id 64
    label "i&#347;&#263;"
  ]
  node [
    id 65
    label "stulecie"
  ]
  node [
    id 66
    label "obiekt"
  ]
  node [
    id 67
    label "kr&#243;tki_dystans"
  ]
  node [
    id 68
    label "pr&#281;dko&#347;&#263;"
  ]
  node [
    id 69
    label "przedmiot"
  ]
  node [
    id 70
    label "wiek"
  ]
  node [
    id 71
    label "nagranie"
  ]
  node [
    id 72
    label "mapa"
  ]
  node [
    id 73
    label "bieg"
  ]
  node [
    id 74
    label "zbi&#243;r"
  ]
  node [
    id 75
    label "liczba"
  ]
  node [
    id 76
    label "pieni&#261;dz"
  ]
  node [
    id 77
    label "w&#243;dka"
  ]
  node [
    id 78
    label "tkanina_we&#322;niana"
  ]
  node [
    id 79
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 80
    label "hektometr"
  ]
  node [
    id 81
    label "jednostka_metryczna"
  ]
  node [
    id 82
    label "dodatek"
  ]
  node [
    id 83
    label "linia"
  ]
  node [
    id 84
    label "sk&#322;ad"
  ]
  node [
    id 85
    label "obszar"
  ]
  node [
    id 86
    label "tarcza_herbowa"
  ]
  node [
    id 87
    label "licytacja"
  ]
  node [
    id 88
    label "zagranie"
  ]
  node [
    id 89
    label "heraldyka"
  ]
  node [
    id 90
    label "kawa&#322;ek"
  ]
  node [
    id 91
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 92
    label "figura_heraldyczna"
  ]
  node [
    id 93
    label "odznaka"
  ]
  node [
    id 94
    label "nap&#281;d"
  ]
  node [
    id 95
    label "wci&#281;cie"
  ]
  node [
    id 96
    label "bielizna"
  ]
  node [
    id 97
    label "greenness"
  ]
  node [
    id 98
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 99
    label "kolor"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 21
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 11
    target 98
  ]
  edge [
    source 11
    target 99
  ]
]
