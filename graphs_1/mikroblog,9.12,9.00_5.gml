graph [
  maxDegree 27
  minDegree 1
  meanDegree 2
  density 0.03225806451612903
  graphCliqueNumber 2
  node [
    id 0
    label "kto"
    origin "text"
  ]
  node [
    id 1
    label "zaplusuje"
    origin "text"
  ]
  node [
    id 2
    label "u&#347;miechni&#281;ty"
    origin "text"
  ]
  node [
    id 3
    label "chlebek"
    origin "text"
  ]
  node [
    id 4
    label "bo&#380;y"
    origin "text"
  ]
  node [
    id 5
    label "ten"
    origin "text"
  ]
  node [
    id 6
    label "nigdy"
    origin "text"
  ]
  node [
    id 7
    label "zazna&#263;"
    origin "text"
  ]
  node [
    id 8
    label "uczucie"
    origin "text"
  ]
  node [
    id 9
    label "g&#322;&#243;d"
    origin "text"
  ]
  node [
    id 10
    label "chleb"
    origin "text"
  ]
  node [
    id 11
    label "glupiewykopowezabawy"
    origin "text"
  ]
  node [
    id 12
    label "heheszki"
    origin "text"
  ]
  node [
    id 13
    label "chleboweheheszki"
    origin "text"
  ]
  node [
    id 14
    label "chlebekbozy"
    origin "text"
  ]
  node [
    id 15
    label "weso&#322;y"
  ]
  node [
    id 16
    label "zadowolony"
  ]
  node [
    id 17
    label "okre&#347;lony"
  ]
  node [
    id 18
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 19
    label "kompletnie"
  ]
  node [
    id 20
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 21
    label "feel"
  ]
  node [
    id 22
    label "zareagowanie"
  ]
  node [
    id 23
    label "wpa&#347;&#263;"
  ]
  node [
    id 24
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 25
    label "opanowanie"
  ]
  node [
    id 26
    label "d&#322;awi&#263;"
  ]
  node [
    id 27
    label "wpada&#263;"
  ]
  node [
    id 28
    label "os&#322;upienie"
  ]
  node [
    id 29
    label "zmys&#322;"
  ]
  node [
    id 30
    label "zaanga&#380;owanie"
  ]
  node [
    id 31
    label "smell"
  ]
  node [
    id 32
    label "zdarzenie_si&#281;"
  ]
  node [
    id 33
    label "ostygn&#261;&#263;"
  ]
  node [
    id 34
    label "afekt"
  ]
  node [
    id 35
    label "stan"
  ]
  node [
    id 36
    label "iskrzy&#263;"
  ]
  node [
    id 37
    label "afekcja"
  ]
  node [
    id 38
    label "przeczulica"
  ]
  node [
    id 39
    label "ow&#322;adni&#281;ty"
  ]
  node [
    id 40
    label "czucie"
  ]
  node [
    id 41
    label "doznanie"
  ]
  node [
    id 42
    label "emocja"
  ]
  node [
    id 43
    label "ogrom"
  ]
  node [
    id 44
    label "stygn&#261;&#263;"
  ]
  node [
    id 45
    label "poczucie"
  ]
  node [
    id 46
    label "temperatura"
  ]
  node [
    id 47
    label "potrzeba_fizjologiczna"
  ]
  node [
    id 48
    label "eagerness"
  ]
  node [
    id 49
    label "potrzeba"
  ]
  node [
    id 50
    label "famine"
  ]
  node [
    id 51
    label "czczo&#347;&#263;"
  ]
  node [
    id 52
    label "kl&#281;ska"
  ]
  node [
    id 53
    label "upragnienie"
  ]
  node [
    id 54
    label "smak"
  ]
  node [
    id 55
    label "na&#322;&#243;g"
  ]
  node [
    id 56
    label "ch&#281;&#263;"
  ]
  node [
    id 57
    label "dar_bo&#380;y"
  ]
  node [
    id 58
    label "bochenek"
  ]
  node [
    id 59
    label "pieczywo"
  ]
  node [
    id 60
    label "wypiek"
  ]
  node [
    id 61
    label "konsubstancjacja"
  ]
  node [
    id 62
    label "utrzymanie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 19
  ]
  edge [
    source 6
    target 14
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 20
  ]
  edge [
    source 7
    target 21
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 22
  ]
  edge [
    source 8
    target 23
  ]
  edge [
    source 8
    target 24
  ]
  edge [
    source 8
    target 25
  ]
  edge [
    source 8
    target 26
  ]
  edge [
    source 8
    target 27
  ]
  edge [
    source 8
    target 28
  ]
  edge [
    source 8
    target 29
  ]
  edge [
    source 8
    target 30
  ]
  edge [
    source 8
    target 31
  ]
  edge [
    source 8
    target 32
  ]
  edge [
    source 8
    target 33
  ]
  edge [
    source 8
    target 34
  ]
  edge [
    source 8
    target 35
  ]
  edge [
    source 8
    target 36
  ]
  edge [
    source 8
    target 37
  ]
  edge [
    source 8
    target 38
  ]
  edge [
    source 8
    target 39
  ]
  edge [
    source 8
    target 40
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 8
    target 46
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 47
  ]
  edge [
    source 9
    target 48
  ]
  edge [
    source 9
    target 49
  ]
  edge [
    source 9
    target 50
  ]
  edge [
    source 9
    target 51
  ]
  edge [
    source 9
    target 52
  ]
  edge [
    source 9
    target 53
  ]
  edge [
    source 9
    target 54
  ]
  edge [
    source 9
    target 55
  ]
  edge [
    source 9
    target 56
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 57
  ]
  edge [
    source 10
    target 58
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 10
    target 60
  ]
  edge [
    source 10
    target 61
  ]
  edge [
    source 10
    target 62
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
]
