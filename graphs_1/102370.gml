graph [
  maxDegree 9
  minDegree 1
  meanDegree 1.8
  density 0.2
  graphCliqueNumber 2
  node [
    id 0
    label "ch&#322;odny"
    origin "text"
  ]
  node [
    id 1
    label "och&#322;odzenie"
  ]
  node [
    id 2
    label "sch&#322;adzanie"
  ]
  node [
    id 3
    label "rozs&#261;dny"
  ]
  node [
    id 4
    label "opanowany"
  ]
  node [
    id 5
    label "ch&#322;odzenie_si&#281;"
  ]
  node [
    id 6
    label "och&#322;odzenie_si&#281;"
  ]
  node [
    id 7
    label "ch&#322;odno"
  ]
  node [
    id 8
    label "zi&#281;bienie"
  ]
  node [
    id 9
    label "niesympatyczny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
]
