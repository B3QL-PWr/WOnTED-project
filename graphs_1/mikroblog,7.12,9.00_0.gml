graph [
  maxDegree 9
  minDegree 1
  meanDegree 1.8
  density 0.2
  graphCliqueNumber 2
  node [
    id 0
    label "uwaga"
    origin "text"
  ]
  node [
    id 1
    label "nagana"
  ]
  node [
    id 2
    label "wypowied&#378;"
  ]
  node [
    id 3
    label "stan"
  ]
  node [
    id 4
    label "dzienniczek"
  ]
  node [
    id 5
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 6
    label "wzgl&#261;d"
  ]
  node [
    id 7
    label "gossip"
  ]
  node [
    id 8
    label "upomnienie"
  ]
  node [
    id 9
    label "tekst"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
]
