graph [
  maxDegree 54
  minDegree 1
  meanDegree 2.488888888888889
  density 0.006160616061606161
  graphCliqueNumber 7
  node [
    id 0
    label "podstawa"
    origin "text"
  ]
  node [
    id 1
    label "art"
    origin "text"
  ]
  node [
    id 2
    label "usta"
    origin "text"
  ]
  node [
    id 3
    label "ustawa"
    origin "text"
  ]
  node [
    id 4
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 5
    label "maj"
    origin "text"
  ]
  node [
    id 6
    label "rocznik"
    origin "text"
  ]
  node [
    id 7
    label "&#347;wiadczenie"
    origin "text"
  ]
  node [
    id 8
    label "pieni&#281;&#380;ny"
    origin "text"
  ]
  node [
    id 9
    label "przys&#322;ugiwa&#263;"
    origin "text"
  ]
  node [
    id 10
    label "osoba"
    origin "text"
  ]
  node [
    id 11
    label "deportowana"
    origin "text"
  ]
  node [
    id 12
    label "praca"
    origin "text"
  ]
  node [
    id 13
    label "przymusowy"
    origin "text"
  ]
  node [
    id 14
    label "osadzona"
    origin "text"
  ]
  node [
    id 15
    label "ob&#243;z"
    origin "text"
  ]
  node [
    id 16
    label "przez"
    origin "text"
  ]
  node [
    id 17
    label "iii"
    origin "text"
  ]
  node [
    id 18
    label "rzesza"
    origin "text"
  ]
  node [
    id 19
    label "zwi&#261;zek"
    origin "text"
  ]
  node [
    id 20
    label "socjalistyczny"
    origin "text"
  ]
  node [
    id 21
    label "republika"
    origin "text"
  ]
  node [
    id 22
    label "radziecki"
    origin "text"
  ]
  node [
    id 23
    label "dziennik"
    origin "text"
  ]
  node [
    id 24
    label "poz"
    origin "text"
  ]
  node [
    id 25
    label "zarz&#261;dza&#263;"
    origin "text"
  ]
  node [
    id 26
    label "si&#281;"
    origin "text"
  ]
  node [
    id 27
    label "nast&#281;powa&#263;"
    origin "text"
  ]
  node [
    id 28
    label "post&#281;powanie"
    origin "text"
  ]
  node [
    id 29
    label "przyznanie"
    origin "text"
  ]
  node [
    id 30
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 31
    label "mowa"
    origin "text"
  ]
  node [
    id 32
    label "zwa&#263;"
    origin "text"
  ]
  node [
    id 33
    label "daleko"
    origin "text"
  ]
  node [
    id 34
    label "wszczyna&#263;"
    origin "text"
  ]
  node [
    id 35
    label "pisemny"
    origin "text"
  ]
  node [
    id 36
    label "wniosek"
    origin "text"
  ]
  node [
    id 37
    label "zainteresowana"
    origin "text"
  ]
  node [
    id 38
    label "zaopiniowa&#263;"
    origin "text"
  ]
  node [
    id 39
    label "w&#322;a&#347;ciwy"
    origin "text"
  ]
  node [
    id 40
    label "stowarzyszenie"
    origin "text"
  ]
  node [
    id 41
    label "poszkodowana"
    origin "text"
  ]
  node [
    id 42
    label "podstawowy"
  ]
  node [
    id 43
    label "strategia"
  ]
  node [
    id 44
    label "pot&#281;ga"
  ]
  node [
    id 45
    label "zasadzenie"
  ]
  node [
    id 46
    label "przedmiot"
  ]
  node [
    id 47
    label "za&#322;o&#380;enie"
  ]
  node [
    id 48
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 49
    label "&#347;ciana"
  ]
  node [
    id 50
    label "documentation"
  ]
  node [
    id 51
    label "dzieci&#281;ctwo"
  ]
  node [
    id 52
    label "pomys&#322;"
  ]
  node [
    id 53
    label "bok"
  ]
  node [
    id 54
    label "d&#243;&#322;"
  ]
  node [
    id 55
    label "punkt_odniesienia"
  ]
  node [
    id 56
    label "column"
  ]
  node [
    id 57
    label "zasadzi&#263;"
  ]
  node [
    id 58
    label "background"
  ]
  node [
    id 59
    label "warga_dolna"
  ]
  node [
    id 60
    label "ryjek"
  ]
  node [
    id 61
    label "zaci&#261;&#263;"
  ]
  node [
    id 62
    label "ssa&#263;"
  ]
  node [
    id 63
    label "twarz"
  ]
  node [
    id 64
    label "dzi&#243;b"
  ]
  node [
    id 65
    label "&#322;uk_Kupidyna"
  ]
  node [
    id 66
    label "ssanie"
  ]
  node [
    id 67
    label "zaci&#281;cie"
  ]
  node [
    id 68
    label "jadaczka"
  ]
  node [
    id 69
    label "zacinanie"
  ]
  node [
    id 70
    label "organ"
  ]
  node [
    id 71
    label "jama_ustna"
  ]
  node [
    id 72
    label "otw&#243;r_g&#281;bowy"
  ]
  node [
    id 73
    label "warga_g&#243;rna"
  ]
  node [
    id 74
    label "zacina&#263;"
  ]
  node [
    id 75
    label "Karta_Nauczyciela"
  ]
  node [
    id 76
    label "marc&#243;wka"
  ]
  node [
    id 77
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 78
    label "akt"
  ]
  node [
    id 79
    label "przej&#347;&#263;"
  ]
  node [
    id 80
    label "charter"
  ]
  node [
    id 81
    label "przej&#347;cie"
  ]
  node [
    id 82
    label "s&#322;o&#324;ce"
  ]
  node [
    id 83
    label "czynienie_si&#281;"
  ]
  node [
    id 84
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 85
    label "czas"
  ]
  node [
    id 86
    label "long_time"
  ]
  node [
    id 87
    label "przedpo&#322;udnie"
  ]
  node [
    id 88
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 89
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 90
    label "tydzie&#324;"
  ]
  node [
    id 91
    label "godzina"
  ]
  node [
    id 92
    label "t&#322;usty_czwartek"
  ]
  node [
    id 93
    label "wsta&#263;"
  ]
  node [
    id 94
    label "day"
  ]
  node [
    id 95
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 96
    label "przedwiecz&#243;r"
  ]
  node [
    id 97
    label "Sylwester"
  ]
  node [
    id 98
    label "po&#322;udnie"
  ]
  node [
    id 99
    label "wzej&#347;cie"
  ]
  node [
    id 100
    label "podwiecz&#243;r"
  ]
  node [
    id 101
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 102
    label "rano"
  ]
  node [
    id 103
    label "termin"
  ]
  node [
    id 104
    label "ranek"
  ]
  node [
    id 105
    label "doba"
  ]
  node [
    id 106
    label "wiecz&#243;r"
  ]
  node [
    id 107
    label "walentynki"
  ]
  node [
    id 108
    label "popo&#322;udnie"
  ]
  node [
    id 109
    label "noc"
  ]
  node [
    id 110
    label "wstanie"
  ]
  node [
    id 111
    label "&#346;wi&#281;to_Pracy"
  ]
  node [
    id 112
    label "miesi&#261;c"
  ]
  node [
    id 113
    label "formacja"
  ]
  node [
    id 114
    label "kronika"
  ]
  node [
    id 115
    label "czasopismo"
  ]
  node [
    id 116
    label "yearbook"
  ]
  node [
    id 117
    label "p&#322;acenie"
  ]
  node [
    id 118
    label "znaczenie"
  ]
  node [
    id 119
    label "sk&#322;adanie"
  ]
  node [
    id 120
    label "service"
  ]
  node [
    id 121
    label "czynienie_dobra"
  ]
  node [
    id 122
    label "informowanie"
  ]
  node [
    id 123
    label "command"
  ]
  node [
    id 124
    label "opowiadanie"
  ]
  node [
    id 125
    label "koszt_rodzajowy"
  ]
  node [
    id 126
    label "pracowanie"
  ]
  node [
    id 127
    label "przekonywanie"
  ]
  node [
    id 128
    label "wyraz"
  ]
  node [
    id 129
    label "us&#322;uga"
  ]
  node [
    id 130
    label "performance"
  ]
  node [
    id 131
    label "zobowi&#261;zanie"
  ]
  node [
    id 132
    label "monetarny"
  ]
  node [
    id 133
    label "przypada&#263;"
  ]
  node [
    id 134
    label "Zgredek"
  ]
  node [
    id 135
    label "kategoria_gramatyczna"
  ]
  node [
    id 136
    label "Casanova"
  ]
  node [
    id 137
    label "Don_Juan"
  ]
  node [
    id 138
    label "Gargantua"
  ]
  node [
    id 139
    label "Faust"
  ]
  node [
    id 140
    label "profanum"
  ]
  node [
    id 141
    label "Chocho&#322;"
  ]
  node [
    id 142
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 143
    label "koniugacja"
  ]
  node [
    id 144
    label "Winnetou"
  ]
  node [
    id 145
    label "Dwukwiat"
  ]
  node [
    id 146
    label "homo_sapiens"
  ]
  node [
    id 147
    label "Edyp"
  ]
  node [
    id 148
    label "Herkules_Poirot"
  ]
  node [
    id 149
    label "ludzko&#347;&#263;"
  ]
  node [
    id 150
    label "mikrokosmos"
  ]
  node [
    id 151
    label "person"
  ]
  node [
    id 152
    label "Sherlock_Holmes"
  ]
  node [
    id 153
    label "portrecista"
  ]
  node [
    id 154
    label "Szwejk"
  ]
  node [
    id 155
    label "Hamlet"
  ]
  node [
    id 156
    label "duch"
  ]
  node [
    id 157
    label "g&#322;owa"
  ]
  node [
    id 158
    label "oddzia&#322;ywanie"
  ]
  node [
    id 159
    label "Quasimodo"
  ]
  node [
    id 160
    label "Dulcynea"
  ]
  node [
    id 161
    label "Don_Kiszot"
  ]
  node [
    id 162
    label "Wallenrod"
  ]
  node [
    id 163
    label "Plastu&#347;"
  ]
  node [
    id 164
    label "Harry_Potter"
  ]
  node [
    id 165
    label "figura"
  ]
  node [
    id 166
    label "parali&#380;owa&#263;"
  ]
  node [
    id 167
    label "istota"
  ]
  node [
    id 168
    label "Werter"
  ]
  node [
    id 169
    label "antropochoria"
  ]
  node [
    id 170
    label "posta&#263;"
  ]
  node [
    id 171
    label "stosunek_pracy"
  ]
  node [
    id 172
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 173
    label "benedykty&#324;ski"
  ]
  node [
    id 174
    label "zaw&#243;d"
  ]
  node [
    id 175
    label "kierownictwo"
  ]
  node [
    id 176
    label "zmiana"
  ]
  node [
    id 177
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 178
    label "wytw&#243;r"
  ]
  node [
    id 179
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 180
    label "tynkarski"
  ]
  node [
    id 181
    label "czynnik_produkcji"
  ]
  node [
    id 182
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 183
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 184
    label "czynno&#347;&#263;"
  ]
  node [
    id 185
    label "tyrka"
  ]
  node [
    id 186
    label "pracowa&#263;"
  ]
  node [
    id 187
    label "siedziba"
  ]
  node [
    id 188
    label "poda&#380;_pracy"
  ]
  node [
    id 189
    label "miejsce"
  ]
  node [
    id 190
    label "zak&#322;ad"
  ]
  node [
    id 191
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 192
    label "najem"
  ]
  node [
    id 193
    label "konieczny"
  ]
  node [
    id 194
    label "przymusowo"
  ]
  node [
    id 195
    label "poniewolny"
  ]
  node [
    id 196
    label "namiot"
  ]
  node [
    id 197
    label "ONZ"
  ]
  node [
    id 198
    label "grupa"
  ]
  node [
    id 199
    label "podob&#243;z"
  ]
  node [
    id 200
    label "blok"
  ]
  node [
    id 201
    label "odpoczynek"
  ]
  node [
    id 202
    label "alianci"
  ]
  node [
    id 203
    label "Paneuropa"
  ]
  node [
    id 204
    label "NATO"
  ]
  node [
    id 205
    label "miejsce_odosobnienia"
  ]
  node [
    id 206
    label "schronienie"
  ]
  node [
    id 207
    label "confederation"
  ]
  node [
    id 208
    label "obozowisko"
  ]
  node [
    id 209
    label "demofobia"
  ]
  node [
    id 210
    label "najazd"
  ]
  node [
    id 211
    label "odwodnienie"
  ]
  node [
    id 212
    label "konstytucja"
  ]
  node [
    id 213
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 214
    label "substancja_chemiczna"
  ]
  node [
    id 215
    label "bratnia_dusza"
  ]
  node [
    id 216
    label "zwi&#261;zanie"
  ]
  node [
    id 217
    label "lokant"
  ]
  node [
    id 218
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 219
    label "zwi&#261;za&#263;"
  ]
  node [
    id 220
    label "organizacja"
  ]
  node [
    id 221
    label "odwadnia&#263;"
  ]
  node [
    id 222
    label "marriage"
  ]
  node [
    id 223
    label "marketing_afiliacyjny"
  ]
  node [
    id 224
    label "bearing"
  ]
  node [
    id 225
    label "wi&#261;zanie"
  ]
  node [
    id 226
    label "odwadnianie"
  ]
  node [
    id 227
    label "koligacja"
  ]
  node [
    id 228
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 229
    label "odwodni&#263;"
  ]
  node [
    id 230
    label "azeotrop"
  ]
  node [
    id 231
    label "powi&#261;zanie"
  ]
  node [
    id 232
    label "lewicowy"
  ]
  node [
    id 233
    label "Buriacja"
  ]
  node [
    id 234
    label "Abchazja"
  ]
  node [
    id 235
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 236
    label "Inguszetia"
  ]
  node [
    id 237
    label "Nachiczewan"
  ]
  node [
    id 238
    label "Karaka&#322;pacja"
  ]
  node [
    id 239
    label "Jakucja"
  ]
  node [
    id 240
    label "Singapur"
  ]
  node [
    id 241
    label "Karelia"
  ]
  node [
    id 242
    label "Komi"
  ]
  node [
    id 243
    label "Tatarstan"
  ]
  node [
    id 244
    label "Chakasja"
  ]
  node [
    id 245
    label "Dagestan"
  ]
  node [
    id 246
    label "Mordowia"
  ]
  node [
    id 247
    label "Ka&#322;mucja"
  ]
  node [
    id 248
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 249
    label "ustr&#243;j"
  ]
  node [
    id 250
    label "Republika_Cisalpi&#324;ska"
  ]
  node [
    id 251
    label "Baszkiria"
  ]
  node [
    id 252
    label "pa&#324;stwo_demokratyczne"
  ]
  node [
    id 253
    label "Mari_El"
  ]
  node [
    id 254
    label "Ad&#380;aria"
  ]
  node [
    id 255
    label "Czuwaszja"
  ]
  node [
    id 256
    label "Tuwa"
  ]
  node [
    id 257
    label "Czeczenia"
  ]
  node [
    id 258
    label "Udmurcja"
  ]
  node [
    id 259
    label "pa&#324;stwo"
  ]
  node [
    id 260
    label "rosyjski"
  ]
  node [
    id 261
    label "po_sowiecku"
  ]
  node [
    id 262
    label "pozosta&#322;y"
  ]
  node [
    id 263
    label "spis"
  ]
  node [
    id 264
    label "sheet"
  ]
  node [
    id 265
    label "gazeta"
  ]
  node [
    id 266
    label "diariusz"
  ]
  node [
    id 267
    label "pami&#281;tnik"
  ]
  node [
    id 268
    label "journal"
  ]
  node [
    id 269
    label "ksi&#281;ga"
  ]
  node [
    id 270
    label "program_informacyjny"
  ]
  node [
    id 271
    label "control"
  ]
  node [
    id 272
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 273
    label "sprawowa&#263;"
  ]
  node [
    id 274
    label "poleca&#263;"
  ]
  node [
    id 275
    label "decydowa&#263;"
  ]
  node [
    id 276
    label "mie&#263;_miejsce"
  ]
  node [
    id 277
    label "chance"
  ]
  node [
    id 278
    label "zdarza&#263;_si&#281;"
  ]
  node [
    id 279
    label "alternate"
  ]
  node [
    id 280
    label "naciska&#263;"
  ]
  node [
    id 281
    label "atakowa&#263;"
  ]
  node [
    id 282
    label "robienie"
  ]
  node [
    id 283
    label "zachowanie"
  ]
  node [
    id 284
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 285
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 286
    label "kognicja"
  ]
  node [
    id 287
    label "rozprawa"
  ]
  node [
    id 288
    label "s&#261;d"
  ]
  node [
    id 289
    label "kazanie"
  ]
  node [
    id 290
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 291
    label "campaign"
  ]
  node [
    id 292
    label "fashion"
  ]
  node [
    id 293
    label "wydarzenie"
  ]
  node [
    id 294
    label "przes&#322;anka"
  ]
  node [
    id 295
    label "zmierzanie"
  ]
  node [
    id 296
    label "recognition"
  ]
  node [
    id 297
    label "danie"
  ]
  node [
    id 298
    label "stwierdzenie"
  ]
  node [
    id 299
    label "confession"
  ]
  node [
    id 300
    label "oznajmienie"
  ]
  node [
    id 301
    label "wypowied&#378;"
  ]
  node [
    id 302
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 303
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 304
    label "po_koroniarsku"
  ]
  node [
    id 305
    label "m&#243;wienie"
  ]
  node [
    id 306
    label "rozumie&#263;"
  ]
  node [
    id 307
    label "komunikacja"
  ]
  node [
    id 308
    label "rozumienie"
  ]
  node [
    id 309
    label "m&#243;wi&#263;"
  ]
  node [
    id 310
    label "gramatyka"
  ]
  node [
    id 311
    label "address"
  ]
  node [
    id 312
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 313
    label "przet&#322;umaczenie"
  ]
  node [
    id 314
    label "tongue"
  ]
  node [
    id 315
    label "t&#322;umaczenie"
  ]
  node [
    id 316
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 317
    label "pismo"
  ]
  node [
    id 318
    label "zdolno&#347;&#263;"
  ]
  node [
    id 319
    label "fonetyka"
  ]
  node [
    id 320
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 321
    label "wokalizm"
  ]
  node [
    id 322
    label "s&#322;ownictwo"
  ]
  node [
    id 323
    label "konsonantyzm"
  ]
  node [
    id 324
    label "kod"
  ]
  node [
    id 325
    label "nazywa&#263;"
  ]
  node [
    id 326
    label "dawno"
  ]
  node [
    id 327
    label "nisko"
  ]
  node [
    id 328
    label "nieobecnie"
  ]
  node [
    id 329
    label "daleki"
  ]
  node [
    id 330
    label "het"
  ]
  node [
    id 331
    label "wysoko"
  ]
  node [
    id 332
    label "du&#380;o"
  ]
  node [
    id 333
    label "znacznie"
  ]
  node [
    id 334
    label "g&#322;&#281;boko"
  ]
  node [
    id 335
    label "robi&#263;_pierwszy_krok"
  ]
  node [
    id 336
    label "begin"
  ]
  node [
    id 337
    label "pi&#347;mienny"
  ]
  node [
    id 338
    label "pisemnie"
  ]
  node [
    id 339
    label "twierdzenie"
  ]
  node [
    id 340
    label "my&#347;l"
  ]
  node [
    id 341
    label "wnioskowanie"
  ]
  node [
    id 342
    label "propozycja"
  ]
  node [
    id 343
    label "motion"
  ]
  node [
    id 344
    label "prayer"
  ]
  node [
    id 345
    label "stwierdzi&#263;"
  ]
  node [
    id 346
    label "taki"
  ]
  node [
    id 347
    label "nale&#380;yty"
  ]
  node [
    id 348
    label "charakterystyczny"
  ]
  node [
    id 349
    label "stosownie"
  ]
  node [
    id 350
    label "dobry"
  ]
  node [
    id 351
    label "prawdziwy"
  ]
  node [
    id 352
    label "ten"
  ]
  node [
    id 353
    label "uprawniony"
  ]
  node [
    id 354
    label "zasadniczy"
  ]
  node [
    id 355
    label "typowy"
  ]
  node [
    id 356
    label "nale&#380;ny"
  ]
  node [
    id 357
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 358
    label "Ku&#378;nica_Ko&#322;&#322;&#261;tajowska"
  ]
  node [
    id 359
    label "Krakowska_Kongregacja_Kupiecka"
  ]
  node [
    id 360
    label "Eleusis"
  ]
  node [
    id 361
    label "Rzeczpospolita_babi&#324;ska"
  ]
  node [
    id 362
    label "Polskie_Towarzystwo_Turystyczno-Krajoznawcze"
  ]
  node [
    id 363
    label "fabianie"
  ]
  node [
    id 364
    label "Chewra_Kadisza"
  ]
  node [
    id 365
    label "umowa_stowarzyszeniowa"
  ]
  node [
    id 366
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 367
    label "Rotary_International"
  ]
  node [
    id 368
    label "Monar"
  ]
  node [
    id 369
    label "zwi&#261;zka"
  ]
  node [
    id 370
    label "urz&#261;d"
  ]
  node [
    id 371
    label "do"
  ]
  node [
    id 372
    label "sprawi&#263;"
  ]
  node [
    id 373
    label "kombatant"
  ]
  node [
    id 374
    label "i"
  ]
  node [
    id 375
    label "represjonowa&#263;"
  ]
  node [
    id 376
    label "mi&#281;dzynarodowy"
  ]
  node [
    id 377
    label "biuro"
  ]
  node [
    id 378
    label "poszukiwanie"
  ]
  node [
    id 379
    label "bad"
  ]
  node [
    id 380
    label "Arolsen"
  ]
  node [
    id 381
    label "czerwony"
  ]
  node [
    id 382
    label "krzy&#380;"
  ]
  node [
    id 383
    label "instytut"
  ]
  node [
    id 384
    label "pami&#281;&#263;"
  ]
  node [
    id 385
    label "narodowy"
  ]
  node [
    id 386
    label "komisja"
  ]
  node [
    id 387
    label "&#347;ciga&#263;"
  ]
  node [
    id 388
    label "zbrodzie&#324;"
  ]
  node [
    id 389
    label "przeciwko"
  ]
  node [
    id 390
    label "nar&#243;d"
  ]
  node [
    id 391
    label "polskie"
  ]
  node [
    id 392
    label "centralny"
  ]
  node [
    id 393
    label "archiwum"
  ]
  node [
    id 394
    label "ministerstwo"
  ]
  node [
    id 395
    label "wewn&#281;trzny"
  ]
  node [
    id 396
    label "administracja"
  ]
  node [
    id 397
    label "wschodni"
  ]
  node [
    id 398
    label "fundacja"
  ]
  node [
    id 399
    label "o&#347;rodek"
  ]
  node [
    id 400
    label "karta"
  ]
  node [
    id 401
    label "Hoover"
  ]
  node [
    id 402
    label "uniwersytet"
  ]
  node [
    id 403
    label "wyspa"
  ]
  node [
    id 404
    label "Stanford"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 24
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 12
  ]
  edge [
    source 6
    target 14
  ]
  edge [
    source 6
    target 20
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 16
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 29
  ]
  edge [
    source 7
    target 117
  ]
  edge [
    source 7
    target 118
  ]
  edge [
    source 7
    target 119
  ]
  edge [
    source 7
    target 120
  ]
  edge [
    source 7
    target 121
  ]
  edge [
    source 7
    target 122
  ]
  edge [
    source 7
    target 123
  ]
  edge [
    source 7
    target 124
  ]
  edge [
    source 7
    target 125
  ]
  edge [
    source 7
    target 126
  ]
  edge [
    source 7
    target 127
  ]
  edge [
    source 7
    target 128
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 7
    target 130
  ]
  edge [
    source 7
    target 131
  ]
  edge [
    source 7
    target 16
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 7
    target 30
  ]
  edge [
    source 7
    target 19
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 30
  ]
  edge [
    source 8
    target 132
  ]
  edge [
    source 8
    target 18
  ]
  edge [
    source 8
    target 29
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 133
  ]
  edge [
    source 9
    target 30
  ]
  edge [
    source 9
    target 19
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 36
  ]
  edge [
    source 10
    target 37
  ]
  edge [
    source 10
    target 40
  ]
  edge [
    source 10
    target 41
  ]
  edge [
    source 10
    target 134
  ]
  edge [
    source 10
    target 135
  ]
  edge [
    source 10
    target 136
  ]
  edge [
    source 10
    target 137
  ]
  edge [
    source 10
    target 138
  ]
  edge [
    source 10
    target 139
  ]
  edge [
    source 10
    target 140
  ]
  edge [
    source 10
    target 141
  ]
  edge [
    source 10
    target 142
  ]
  edge [
    source 10
    target 143
  ]
  edge [
    source 10
    target 144
  ]
  edge [
    source 10
    target 145
  ]
  edge [
    source 10
    target 146
  ]
  edge [
    source 10
    target 147
  ]
  edge [
    source 10
    target 148
  ]
  edge [
    source 10
    target 149
  ]
  edge [
    source 10
    target 150
  ]
  edge [
    source 10
    target 151
  ]
  edge [
    source 10
    target 152
  ]
  edge [
    source 10
    target 153
  ]
  edge [
    source 10
    target 154
  ]
  edge [
    source 10
    target 155
  ]
  edge [
    source 10
    target 156
  ]
  edge [
    source 10
    target 157
  ]
  edge [
    source 10
    target 158
  ]
  edge [
    source 10
    target 159
  ]
  edge [
    source 10
    target 160
  ]
  edge [
    source 10
    target 161
  ]
  edge [
    source 10
    target 162
  ]
  edge [
    source 10
    target 163
  ]
  edge [
    source 10
    target 164
  ]
  edge [
    source 10
    target 165
  ]
  edge [
    source 10
    target 166
  ]
  edge [
    source 10
    target 167
  ]
  edge [
    source 10
    target 168
  ]
  edge [
    source 10
    target 169
  ]
  edge [
    source 10
    target 170
  ]
  edge [
    source 10
    target 10
  ]
  edge [
    source 10
    target 20
  ]
  edge [
    source 10
    target 22
  ]
  edge [
    source 10
    target 28
  ]
  edge [
    source 10
    target 370
  ]
  edge [
    source 10
    target 371
  ]
  edge [
    source 10
    target 372
  ]
  edge [
    source 10
    target 373
  ]
  edge [
    source 10
    target 374
  ]
  edge [
    source 10
    target 375
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 11
  ]
  edge [
    source 11
    target 21
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 15
  ]
  edge [
    source 12
    target 16
  ]
  edge [
    source 12
    target 171
  ]
  edge [
    source 12
    target 172
  ]
  edge [
    source 12
    target 173
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 12
    target 174
  ]
  edge [
    source 12
    target 175
  ]
  edge [
    source 12
    target 176
  ]
  edge [
    source 12
    target 177
  ]
  edge [
    source 12
    target 178
  ]
  edge [
    source 12
    target 179
  ]
  edge [
    source 12
    target 180
  ]
  edge [
    source 12
    target 181
  ]
  edge [
    source 12
    target 182
  ]
  edge [
    source 12
    target 131
  ]
  edge [
    source 12
    target 183
  ]
  edge [
    source 12
    target 184
  ]
  edge [
    source 12
    target 185
  ]
  edge [
    source 12
    target 186
  ]
  edge [
    source 12
    target 187
  ]
  edge [
    source 12
    target 188
  ]
  edge [
    source 12
    target 189
  ]
  edge [
    source 12
    target 190
  ]
  edge [
    source 12
    target 191
  ]
  edge [
    source 12
    target 192
  ]
  edge [
    source 12
    target 22
  ]
  edge [
    source 12
    target 20
  ]
  edge [
    source 12
    target 35
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 193
  ]
  edge [
    source 13
    target 194
  ]
  edge [
    source 13
    target 195
  ]
  edge [
    source 13
    target 23
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 19
  ]
  edge [
    source 15
    target 196
  ]
  edge [
    source 15
    target 197
  ]
  edge [
    source 15
    target 198
  ]
  edge [
    source 15
    target 199
  ]
  edge [
    source 15
    target 200
  ]
  edge [
    source 15
    target 201
  ]
  edge [
    source 15
    target 202
  ]
  edge [
    source 15
    target 203
  ]
  edge [
    source 15
    target 204
  ]
  edge [
    source 15
    target 205
  ]
  edge [
    source 15
    target 206
  ]
  edge [
    source 15
    target 207
  ]
  edge [
    source 15
    target 208
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 38
  ]
  edge [
    source 16
    target 39
  ]
  edge [
    source 16
    target 33
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 209
  ]
  edge [
    source 18
    target 210
  ]
  edge [
    source 18
    target 198
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 211
  ]
  edge [
    source 19
    target 212
  ]
  edge [
    source 19
    target 213
  ]
  edge [
    source 19
    target 214
  ]
  edge [
    source 19
    target 215
  ]
  edge [
    source 19
    target 216
  ]
  edge [
    source 19
    target 217
  ]
  edge [
    source 19
    target 218
  ]
  edge [
    source 19
    target 219
  ]
  edge [
    source 19
    target 220
  ]
  edge [
    source 19
    target 221
  ]
  edge [
    source 19
    target 222
  ]
  edge [
    source 19
    target 223
  ]
  edge [
    source 19
    target 224
  ]
  edge [
    source 19
    target 225
  ]
  edge [
    source 19
    target 226
  ]
  edge [
    source 19
    target 227
  ]
  edge [
    source 19
    target 228
  ]
  edge [
    source 19
    target 229
  ]
  edge [
    source 19
    target 230
  ]
  edge [
    source 19
    target 231
  ]
  edge [
    source 19
    target 34
  ]
  edge [
    source 19
    target 30
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 232
  ]
  edge [
    source 20
    target 22
  ]
  edge [
    source 20
    target 35
  ]
  edge [
    source 20
    target 369
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 233
  ]
  edge [
    source 21
    target 234
  ]
  edge [
    source 21
    target 235
  ]
  edge [
    source 21
    target 236
  ]
  edge [
    source 21
    target 237
  ]
  edge [
    source 21
    target 238
  ]
  edge [
    source 21
    target 239
  ]
  edge [
    source 21
    target 240
  ]
  edge [
    source 21
    target 241
  ]
  edge [
    source 21
    target 242
  ]
  edge [
    source 21
    target 243
  ]
  edge [
    source 21
    target 244
  ]
  edge [
    source 21
    target 245
  ]
  edge [
    source 21
    target 246
  ]
  edge [
    source 21
    target 247
  ]
  edge [
    source 21
    target 248
  ]
  edge [
    source 21
    target 249
  ]
  edge [
    source 21
    target 250
  ]
  edge [
    source 21
    target 251
  ]
  edge [
    source 21
    target 252
  ]
  edge [
    source 21
    target 253
  ]
  edge [
    source 21
    target 254
  ]
  edge [
    source 21
    target 255
  ]
  edge [
    source 21
    target 256
  ]
  edge [
    source 21
    target 257
  ]
  edge [
    source 21
    target 258
  ]
  edge [
    source 21
    target 259
  ]
  edge [
    source 21
    target 36
  ]
  edge [
    source 21
    target 369
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 260
  ]
  edge [
    source 22
    target 261
  ]
  edge [
    source 22
    target 262
  ]
  edge [
    source 22
    target 369
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 263
  ]
  edge [
    source 23
    target 264
  ]
  edge [
    source 23
    target 265
  ]
  edge [
    source 23
    target 266
  ]
  edge [
    source 23
    target 267
  ]
  edge [
    source 23
    target 268
  ]
  edge [
    source 23
    target 269
  ]
  edge [
    source 23
    target 270
  ]
  edge [
    source 23
    target 37
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 32
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 271
  ]
  edge [
    source 25
    target 272
  ]
  edge [
    source 25
    target 273
  ]
  edge [
    source 25
    target 274
  ]
  edge [
    source 25
    target 275
  ]
  edge [
    source 25
    target 39
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 34
  ]
  edge [
    source 26
    target 35
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 276
  ]
  edge [
    source 27
    target 277
  ]
  edge [
    source 27
    target 278
  ]
  edge [
    source 27
    target 279
  ]
  edge [
    source 27
    target 280
  ]
  edge [
    source 27
    target 281
  ]
  edge [
    source 27
    target 40
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 282
  ]
  edge [
    source 28
    target 184
  ]
  edge [
    source 28
    target 283
  ]
  edge [
    source 28
    target 284
  ]
  edge [
    source 28
    target 285
  ]
  edge [
    source 28
    target 286
  ]
  edge [
    source 28
    target 287
  ]
  edge [
    source 28
    target 288
  ]
  edge [
    source 28
    target 289
  ]
  edge [
    source 28
    target 290
  ]
  edge [
    source 28
    target 291
  ]
  edge [
    source 28
    target 292
  ]
  edge [
    source 28
    target 293
  ]
  edge [
    source 28
    target 294
  ]
  edge [
    source 28
    target 295
  ]
  edge [
    source 29
    target 296
  ]
  edge [
    source 29
    target 297
  ]
  edge [
    source 29
    target 298
  ]
  edge [
    source 29
    target 299
  ]
  edge [
    source 29
    target 300
  ]
  edge [
    source 29
    target 41
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 31
    target 301
  ]
  edge [
    source 31
    target 302
  ]
  edge [
    source 31
    target 303
  ]
  edge [
    source 31
    target 304
  ]
  edge [
    source 31
    target 305
  ]
  edge [
    source 31
    target 306
  ]
  edge [
    source 31
    target 307
  ]
  edge [
    source 31
    target 308
  ]
  edge [
    source 31
    target 309
  ]
  edge [
    source 31
    target 310
  ]
  edge [
    source 31
    target 311
  ]
  edge [
    source 31
    target 312
  ]
  edge [
    source 31
    target 313
  ]
  edge [
    source 31
    target 184
  ]
  edge [
    source 31
    target 314
  ]
  edge [
    source 31
    target 315
  ]
  edge [
    source 31
    target 316
  ]
  edge [
    source 31
    target 317
  ]
  edge [
    source 31
    target 318
  ]
  edge [
    source 31
    target 319
  ]
  edge [
    source 31
    target 320
  ]
  edge [
    source 31
    target 321
  ]
  edge [
    source 31
    target 322
  ]
  edge [
    source 31
    target 323
  ]
  edge [
    source 31
    target 324
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 325
  ]
  edge [
    source 33
    target 326
  ]
  edge [
    source 33
    target 327
  ]
  edge [
    source 33
    target 328
  ]
  edge [
    source 33
    target 329
  ]
  edge [
    source 33
    target 330
  ]
  edge [
    source 33
    target 331
  ]
  edge [
    source 33
    target 332
  ]
  edge [
    source 33
    target 333
  ]
  edge [
    source 33
    target 334
  ]
  edge [
    source 34
    target 335
  ]
  edge [
    source 34
    target 336
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 337
  ]
  edge [
    source 35
    target 338
  ]
  edge [
    source 36
    target 339
  ]
  edge [
    source 36
    target 340
  ]
  edge [
    source 36
    target 341
  ]
  edge [
    source 36
    target 342
  ]
  edge [
    source 36
    target 343
  ]
  edge [
    source 36
    target 317
  ]
  edge [
    source 36
    target 344
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 38
    target 345
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 346
  ]
  edge [
    source 39
    target 347
  ]
  edge [
    source 39
    target 348
  ]
  edge [
    source 39
    target 349
  ]
  edge [
    source 39
    target 350
  ]
  edge [
    source 39
    target 351
  ]
  edge [
    source 39
    target 352
  ]
  edge [
    source 39
    target 353
  ]
  edge [
    source 39
    target 354
  ]
  edge [
    source 39
    target 355
  ]
  edge [
    source 39
    target 356
  ]
  edge [
    source 39
    target 357
  ]
  edge [
    source 40
    target 358
  ]
  edge [
    source 40
    target 359
  ]
  edge [
    source 40
    target 220
  ]
  edge [
    source 40
    target 360
  ]
  edge [
    source 40
    target 361
  ]
  edge [
    source 40
    target 362
  ]
  edge [
    source 40
    target 198
  ]
  edge [
    source 40
    target 363
  ]
  edge [
    source 40
    target 364
  ]
  edge [
    source 40
    target 365
  ]
  edge [
    source 40
    target 366
  ]
  edge [
    source 40
    target 367
  ]
  edge [
    source 40
    target 368
  ]
  edge [
    source 370
    target 371
  ]
  edge [
    source 370
    target 372
  ]
  edge [
    source 370
    target 373
  ]
  edge [
    source 370
    target 374
  ]
  edge [
    source 370
    target 375
  ]
  edge [
    source 371
    target 372
  ]
  edge [
    source 371
    target 373
  ]
  edge [
    source 371
    target 374
  ]
  edge [
    source 371
    target 375
  ]
  edge [
    source 372
    target 373
  ]
  edge [
    source 372
    target 374
  ]
  edge [
    source 372
    target 375
  ]
  edge [
    source 372
    target 392
  ]
  edge [
    source 372
    target 393
  ]
  edge [
    source 372
    target 394
  ]
  edge [
    source 372
    target 395
  ]
  edge [
    source 372
    target 396
  ]
  edge [
    source 373
    target 374
  ]
  edge [
    source 373
    target 375
  ]
  edge [
    source 374
    target 375
  ]
  edge [
    source 374
    target 392
  ]
  edge [
    source 374
    target 393
  ]
  edge [
    source 374
    target 394
  ]
  edge [
    source 374
    target 395
  ]
  edge [
    source 374
    target 396
  ]
  edge [
    source 376
    target 377
  ]
  edge [
    source 376
    target 378
  ]
  edge [
    source 376
    target 381
  ]
  edge [
    source 376
    target 382
  ]
  edge [
    source 377
    target 378
  ]
  edge [
    source 379
    target 380
  ]
  edge [
    source 381
    target 382
  ]
  edge [
    source 383
    target 384
  ]
  edge [
    source 383
    target 385
  ]
  edge [
    source 383
    target 401
  ]
  edge [
    source 384
    target 385
  ]
  edge [
    source 386
    target 387
  ]
  edge [
    source 386
    target 388
  ]
  edge [
    source 386
    target 389
  ]
  edge [
    source 386
    target 390
  ]
  edge [
    source 386
    target 391
  ]
  edge [
    source 387
    target 388
  ]
  edge [
    source 387
    target 389
  ]
  edge [
    source 387
    target 390
  ]
  edge [
    source 387
    target 391
  ]
  edge [
    source 388
    target 389
  ]
  edge [
    source 388
    target 390
  ]
  edge [
    source 388
    target 391
  ]
  edge [
    source 389
    target 390
  ]
  edge [
    source 389
    target 391
  ]
  edge [
    source 390
    target 391
  ]
  edge [
    source 392
    target 393
  ]
  edge [
    source 392
    target 394
  ]
  edge [
    source 392
    target 395
  ]
  edge [
    source 392
    target 396
  ]
  edge [
    source 393
    target 394
  ]
  edge [
    source 393
    target 395
  ]
  edge [
    source 393
    target 396
  ]
  edge [
    source 393
    target 397
  ]
  edge [
    source 394
    target 395
  ]
  edge [
    source 394
    target 396
  ]
  edge [
    source 395
    target 396
  ]
  edge [
    source 398
    target 399
  ]
  edge [
    source 398
    target 400
  ]
  edge [
    source 399
    target 400
  ]
  edge [
    source 402
    target 403
  ]
  edge [
    source 402
    target 404
  ]
  edge [
    source 403
    target 404
  ]
]
