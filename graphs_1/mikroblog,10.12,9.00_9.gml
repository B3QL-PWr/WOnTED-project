graph [
  maxDegree 4
  minDegree 1
  meanDegree 1.75
  density 0.25
  graphCliqueNumber 2
  node [
    id 0
    label "powt&#243;rka"
    origin "text"
  ]
  node [
    id 1
    label "rozrywka"
    origin "text"
  ]
  node [
    id 2
    label "program"
  ]
  node [
    id 3
    label "czynno&#347;&#263;"
  ]
  node [
    id 4
    label "nauka"
  ]
  node [
    id 5
    label "czasoumilacz"
  ]
  node [
    id 6
    label "game"
  ]
  node [
    id 7
    label "odpoczynek"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
]
