graph [
  maxDegree 5
  minDegree 1
  meanDegree 1.875
  density 0.125
  graphCliqueNumber 3
  node [
    id 0
    label "audio"
    origin "text"
  ]
  node [
    id 1
    label "physic"
    origin "text"
  ]
  node [
    id 2
    label "Physic"
  ]
  node [
    id 3
    label "Yara"
  ]
  node [
    id 4
    label "monitor"
  ]
  node [
    id 5
    label "ii"
  ]
  node [
    id 6
    label "Classic"
  ]
  node [
    id 7
    label "tempo"
  ]
  node [
    id 8
    label "VI"
  ]
  node [
    id 9
    label "25"
  ]
  node [
    id 10
    label "Evolution"
  ]
  node [
    id 11
    label "Scorpio"
  ]
  node [
    id 12
    label "Virgo"
  ]
  node [
    id 13
    label "verte"
  ]
  node [
    id 14
    label "Avanti"
  ]
  node [
    id 15
    label "superior"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 10
  ]
  edge [
    source 5
    target 11
  ]
  edge [
    source 5
    target 15
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
]
