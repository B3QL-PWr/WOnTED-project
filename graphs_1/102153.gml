graph [
  maxDegree 50
  minDegree 1
  meanDegree 2.3956043956043955
  density 0.005276661664326863
  graphCliqueNumber 6
  node [
    id 0
    label "typowy"
    origin "text"
  ]
  node [
    id 1
    label "cykl"
    origin "text"
  ]
  node [
    id 2
    label "rozkazowy"
    origin "text"
  ]
  node [
    id 3
    label "system"
    origin "text"
  ]
  node [
    id 4
    label "architektura"
    origin "text"
  ]
  node [
    id 5
    label "von"
    origin "text"
  ]
  node [
    id 6
    label "neumanna"
    origin "text"
  ]
  node [
    id 7
    label "zaczyna&#263;"
    origin "text"
  ]
  node [
    id 8
    label "si&#281;"
    origin "text"
  ]
  node [
    id 9
    label "pobranie"
    origin "text"
  ]
  node [
    id 10
    label "rozkaz"
    origin "text"
  ]
  node [
    id 11
    label "pami&#281;&#263;"
    origin "text"
  ]
  node [
    id 12
    label "przes&#322;anie"
    origin "text"
  ]
  node [
    id 13
    label "rejestr"
    origin "text"
  ]
  node [
    id 14
    label "ang"
    origin "text"
  ]
  node [
    id 15
    label "instruetion"
    origin "text"
  ]
  node [
    id 16
    label "register"
    origin "text"
  ]
  node [
    id 17
    label "by&#263;"
    origin "text"
  ]
  node [
    id 18
    label "nast&#281;pnie"
    origin "text"
  ]
  node [
    id 19
    label "dekodowa&#263;"
    origin "text"
  ]
  node [
    id 20
    label "realizowa&#263;"
    origin "text"
  ]
  node [
    id 21
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 22
    label "spowodowa&#263;"
    origin "text"
  ]
  node [
    id 23
    label "argument"
    origin "text"
  ]
  node [
    id 24
    label "umie&#347;ci&#263;"
    origin "text"
  ]
  node [
    id 25
    label "inny"
    origin "text"
  ]
  node [
    id 26
    label "wewn&#281;trzny"
    origin "text"
  ]
  node [
    id 27
    label "wykonanie"
    origin "text"
  ]
  node [
    id 28
    label "argumentachjego"
    origin "text"
  ]
  node [
    id 29
    label "wynik"
    origin "text"
  ]
  node [
    id 30
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 31
    label "powr&#243;t"
    origin "text"
  ]
  node [
    id 32
    label "przechowa&#263;"
    origin "text"
  ]
  node [
    id 33
    label "zauwa&#380;y&#263;"
    origin "text"
  ]
  node [
    id 34
    label "jednostka"
    origin "text"
  ]
  node [
    id 35
    label "widzie&#263;"
    origin "text"
  ]
  node [
    id 36
    label "tylko"
    origin "text"
  ]
  node [
    id 37
    label "strumie&#324;"
    origin "text"
  ]
  node [
    id 38
    label "adres"
    origin "text"
  ]
  node [
    id 39
    label "znany"
    origin "text"
  ]
  node [
    id 40
    label "spos&#243;b"
    origin "text"
  ]
  node [
    id 41
    label "jak"
    origin "text"
  ]
  node [
    id 42
    label "powsta&#322;y"
    origin "text"
  ]
  node [
    id 43
    label "licznik"
    origin "text"
  ]
  node [
    id 44
    label "indeksowa&#263;"
    origin "text"
  ]
  node [
    id 45
    label "modyfikacja"
    origin "text"
  ]
  node [
    id 46
    label "po&#347;redni"
    origin "text"
  ]
  node [
    id 47
    label "literalny"
    origin "text"
  ]
  node [
    id 48
    label "itp"
    origin "text"
  ]
  node [
    id 49
    label "ani"
    origin "text"
  ]
  node [
    id 50
    label "czemu"
    origin "text"
  ]
  node [
    id 51
    label "s&#322;u&#380;y&#263;"
    origin "text"
  ]
  node [
    id 52
    label "lub"
    origin "text"
  ]
  node [
    id 53
    label "dana"
    origin "text"
  ]
  node [
    id 54
    label "uwaga"
    origin "text"
  ]
  node [
    id 55
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 56
    label "zaniedba&#263;"
    origin "text"
  ]
  node [
    id 57
    label "generowa&#263;"
    origin "text"
  ]
  node [
    id 58
    label "przez"
    origin "text"
  ]
  node [
    id 59
    label "program"
    origin "text"
  ]
  node [
    id 60
    label "interesowa&#263;"
    origin "text"
  ]
  node [
    id 61
    label "ci&#261;g"
    origin "text"
  ]
  node [
    id 62
    label "wytwarza&#263;"
    origin "text"
  ]
  node [
    id 63
    label "wykonywa&#263;"
    origin "text"
  ]
  node [
    id 64
    label "typowo"
  ]
  node [
    id 65
    label "zwyczajny"
  ]
  node [
    id 66
    label "zwyk&#322;y"
  ]
  node [
    id 67
    label "cz&#281;sty"
  ]
  node [
    id 68
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 69
    label "sekwencja"
  ]
  node [
    id 70
    label "czas"
  ]
  node [
    id 71
    label "edycja"
  ]
  node [
    id 72
    label "przebieg"
  ]
  node [
    id 73
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 74
    label "okres"
  ]
  node [
    id 75
    label "cykl_p&#322;ciowy"
  ]
  node [
    id 76
    label "cycle"
  ]
  node [
    id 77
    label "owulacja"
  ]
  node [
    id 78
    label "miesi&#261;czka"
  ]
  node [
    id 79
    label "set"
  ]
  node [
    id 80
    label "model"
  ]
  node [
    id 81
    label "sk&#322;ad"
  ]
  node [
    id 82
    label "zachowanie"
  ]
  node [
    id 83
    label "podstawa"
  ]
  node [
    id 84
    label "porz&#261;dek"
  ]
  node [
    id 85
    label "Android"
  ]
  node [
    id 86
    label "przyn&#281;ta"
  ]
  node [
    id 87
    label "jednostka_geologiczna"
  ]
  node [
    id 88
    label "metoda"
  ]
  node [
    id 89
    label "podsystem"
  ]
  node [
    id 90
    label "p&#322;&#243;d"
  ]
  node [
    id 91
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 92
    label "s&#261;d"
  ]
  node [
    id 93
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 94
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 95
    label "j&#261;dro"
  ]
  node [
    id 96
    label "eratem"
  ]
  node [
    id 97
    label "ryba"
  ]
  node [
    id 98
    label "pulpit"
  ]
  node [
    id 99
    label "struktura"
  ]
  node [
    id 100
    label "oddzia&#322;"
  ]
  node [
    id 101
    label "usenet"
  ]
  node [
    id 102
    label "o&#347;"
  ]
  node [
    id 103
    label "oprogramowanie"
  ]
  node [
    id 104
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 105
    label "poj&#281;cie"
  ]
  node [
    id 106
    label "w&#281;dkarstwo"
  ]
  node [
    id 107
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 108
    label "Leopard"
  ]
  node [
    id 109
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 110
    label "systemik"
  ]
  node [
    id 111
    label "rozprz&#261;c"
  ]
  node [
    id 112
    label "cybernetyk"
  ]
  node [
    id 113
    label "konstelacja"
  ]
  node [
    id 114
    label "doktryna"
  ]
  node [
    id 115
    label "net"
  ]
  node [
    id 116
    label "zbi&#243;r"
  ]
  node [
    id 117
    label "method"
  ]
  node [
    id 118
    label "systemat"
  ]
  node [
    id 119
    label "styl_architektoniczny"
  ]
  node [
    id 120
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 121
    label "labirynt"
  ]
  node [
    id 122
    label "architektonika"
  ]
  node [
    id 123
    label "architektura_rezydencjonalna"
  ]
  node [
    id 124
    label "computer_architecture"
  ]
  node [
    id 125
    label "open"
  ]
  node [
    id 126
    label "odejmowa&#263;"
  ]
  node [
    id 127
    label "mie&#263;_miejsce"
  ]
  node [
    id 128
    label "robi&#263;_pierwszy_krok"
  ]
  node [
    id 129
    label "set_about"
  ]
  node [
    id 130
    label "begin"
  ]
  node [
    id 131
    label "post&#281;powa&#263;"
  ]
  node [
    id 132
    label "bankrupt"
  ]
  node [
    id 133
    label "capture"
  ]
  node [
    id 134
    label "wzi&#281;cie"
  ]
  node [
    id 135
    label "wyci&#281;cie"
  ]
  node [
    id 136
    label "przeszczepienie"
  ]
  node [
    id 137
    label "wymienienie_si&#281;"
  ]
  node [
    id 138
    label "ci&#261;gni&#281;cie"
  ]
  node [
    id 139
    label "wch&#322;oni&#281;cie"
  ]
  node [
    id 140
    label "uzyskanie"
  ]
  node [
    id 141
    label "otrzymanie"
  ]
  node [
    id 142
    label "pr&#243;bka"
  ]
  node [
    id 143
    label "komender&#243;wka"
  ]
  node [
    id 144
    label "polecenie"
  ]
  node [
    id 145
    label "direction"
  ]
  node [
    id 146
    label "hipokamp"
  ]
  node [
    id 147
    label "wymazanie"
  ]
  node [
    id 148
    label "wytw&#243;r"
  ]
  node [
    id 149
    label "zachowa&#263;"
  ]
  node [
    id 150
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 151
    label "memory"
  ]
  node [
    id 152
    label "pami&#281;tno&#347;&#263;"
  ]
  node [
    id 153
    label "umys&#322;"
  ]
  node [
    id 154
    label "sprz&#281;t_komputerowy"
  ]
  node [
    id 155
    label "komputer"
  ]
  node [
    id 156
    label "znaczenie"
  ]
  node [
    id 157
    label "bed"
  ]
  node [
    id 158
    label "idea"
  ]
  node [
    id 159
    label "p&#243;j&#347;cie"
  ]
  node [
    id 160
    label "forward"
  ]
  node [
    id 161
    label "przekazanie"
  ]
  node [
    id 162
    label "pismo"
  ]
  node [
    id 163
    label "message"
  ]
  node [
    id 164
    label "wyliczanka"
  ]
  node [
    id 165
    label "skala"
  ]
  node [
    id 166
    label "catalog"
  ]
  node [
    id 167
    label "organy"
  ]
  node [
    id 168
    label "brzmienie"
  ]
  node [
    id 169
    label "stock"
  ]
  node [
    id 170
    label "procesor"
  ]
  node [
    id 171
    label "figurowa&#263;"
  ]
  node [
    id 172
    label "przycisk"
  ]
  node [
    id 173
    label "book"
  ]
  node [
    id 174
    label "urz&#261;dzenie"
  ]
  node [
    id 175
    label "uk&#322;ad_elektroniczny"
  ]
  node [
    id 176
    label "regestr"
  ]
  node [
    id 177
    label "pozycja"
  ]
  node [
    id 178
    label "tekst"
  ]
  node [
    id 179
    label "sumariusz"
  ]
  node [
    id 180
    label "zgodno&#347;&#263;"
  ]
  node [
    id 181
    label "wa&#322;ek"
  ]
  node [
    id 182
    label "maszyna_drukarska"
  ]
  node [
    id 183
    label "si&#281;ga&#263;"
  ]
  node [
    id 184
    label "trwa&#263;"
  ]
  node [
    id 185
    label "obecno&#347;&#263;"
  ]
  node [
    id 186
    label "stan"
  ]
  node [
    id 187
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 188
    label "stand"
  ]
  node [
    id 189
    label "uczestniczy&#263;"
  ]
  node [
    id 190
    label "chodzi&#263;"
  ]
  node [
    id 191
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 192
    label "equal"
  ]
  node [
    id 193
    label "kolejny"
  ]
  node [
    id 194
    label "przetwarza&#263;"
  ]
  node [
    id 195
    label "decode"
  ]
  node [
    id 196
    label "wykorzystywa&#263;"
  ]
  node [
    id 197
    label "przeprowadza&#263;"
  ]
  node [
    id 198
    label "spieni&#281;&#380;a&#263;"
  ]
  node [
    id 199
    label "prosecute"
  ]
  node [
    id 200
    label "create"
  ]
  node [
    id 201
    label "prawdzi&#263;"
  ]
  node [
    id 202
    label "tworzy&#263;"
  ]
  node [
    id 203
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 204
    label "act"
  ]
  node [
    id 205
    label "argumentacja"
  ]
  node [
    id 206
    label "parametr"
  ]
  node [
    id 207
    label "rzecz"
  ]
  node [
    id 208
    label "dow&#243;d"
  ]
  node [
    id 209
    label "operand"
  ]
  node [
    id 210
    label "zmienna"
  ]
  node [
    id 211
    label "zrobi&#263;"
  ]
  node [
    id 212
    label "okre&#347;li&#263;"
  ]
  node [
    id 213
    label "uplasowa&#263;"
  ]
  node [
    id 214
    label "umieszcza&#263;"
  ]
  node [
    id 215
    label "wpierniczy&#263;"
  ]
  node [
    id 216
    label "pomie&#347;ci&#263;"
  ]
  node [
    id 217
    label "zmieni&#263;"
  ]
  node [
    id 218
    label "put"
  ]
  node [
    id 219
    label "inaczej"
  ]
  node [
    id 220
    label "r&#243;&#380;ny"
  ]
  node [
    id 221
    label "inszy"
  ]
  node [
    id 222
    label "osobno"
  ]
  node [
    id 223
    label "wewn&#281;trznie"
  ]
  node [
    id 224
    label "wn&#281;trzny"
  ]
  node [
    id 225
    label "psychiczny"
  ]
  node [
    id 226
    label "numer"
  ]
  node [
    id 227
    label "zrobienie"
  ]
  node [
    id 228
    label "czynno&#347;&#263;"
  ]
  node [
    id 229
    label "fabrication"
  ]
  node [
    id 230
    label "ziszczenie_si&#281;"
  ]
  node [
    id 231
    label "pojawienie_si&#281;"
  ]
  node [
    id 232
    label "dzie&#322;o"
  ]
  node [
    id 233
    label "production"
  ]
  node [
    id 234
    label "completion"
  ]
  node [
    id 235
    label "realizacja"
  ]
  node [
    id 236
    label "typ"
  ]
  node [
    id 237
    label "dzia&#322;anie"
  ]
  node [
    id 238
    label "przyczyna"
  ]
  node [
    id 239
    label "zaokr&#261;gli&#263;"
  ]
  node [
    id 240
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 241
    label "zaokr&#261;glenie"
  ]
  node [
    id 242
    label "event"
  ]
  node [
    id 243
    label "rezultat"
  ]
  node [
    id 244
    label "free"
  ]
  node [
    id 245
    label "return"
  ]
  node [
    id 246
    label "odyseja"
  ]
  node [
    id 247
    label "para"
  ]
  node [
    id 248
    label "wydarzenie"
  ]
  node [
    id 249
    label "rektyfikacja"
  ]
  node [
    id 250
    label "continue"
  ]
  node [
    id 251
    label "podtrzyma&#263;"
  ]
  node [
    id 252
    label "uchroni&#263;"
  ]
  node [
    id 253
    label "preserve"
  ]
  node [
    id 254
    label "ukry&#263;"
  ]
  node [
    id 255
    label "zobaczy&#263;"
  ]
  node [
    id 256
    label "zwr&#243;ci&#263;_uwag&#281;"
  ]
  node [
    id 257
    label "notice"
  ]
  node [
    id 258
    label "cognizance"
  ]
  node [
    id 259
    label "infimum"
  ]
  node [
    id 260
    label "ewoluowanie"
  ]
  node [
    id 261
    label "przyswoi&#263;"
  ]
  node [
    id 262
    label "reakcja"
  ]
  node [
    id 263
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 264
    label "wyewoluowanie"
  ]
  node [
    id 265
    label "individual"
  ]
  node [
    id 266
    label "profanum"
  ]
  node [
    id 267
    label "starzenie_si&#281;"
  ]
  node [
    id 268
    label "homo_sapiens"
  ]
  node [
    id 269
    label "supremum"
  ]
  node [
    id 270
    label "przyswaja&#263;"
  ]
  node [
    id 271
    label "ludzko&#347;&#263;"
  ]
  node [
    id 272
    label "one"
  ]
  node [
    id 273
    label "funkcja"
  ]
  node [
    id 274
    label "przeliczenie"
  ]
  node [
    id 275
    label "przeliczanie"
  ]
  node [
    id 276
    label "mikrokosmos"
  ]
  node [
    id 277
    label "rzut"
  ]
  node [
    id 278
    label "portrecista"
  ]
  node [
    id 279
    label "przelicza&#263;"
  ]
  node [
    id 280
    label "przyswajanie"
  ]
  node [
    id 281
    label "duch"
  ]
  node [
    id 282
    label "wyewoluowa&#263;"
  ]
  node [
    id 283
    label "ewoluowa&#263;"
  ]
  node [
    id 284
    label "oddzia&#322;ywanie"
  ]
  node [
    id 285
    label "g&#322;owa"
  ]
  node [
    id 286
    label "liczba_naturalna"
  ]
  node [
    id 287
    label "osoba"
  ]
  node [
    id 288
    label "figura"
  ]
  node [
    id 289
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 290
    label "obiekt"
  ]
  node [
    id 291
    label "matematyka"
  ]
  node [
    id 292
    label "przyswojenie"
  ]
  node [
    id 293
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 294
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 295
    label "czynnik_biotyczny"
  ]
  node [
    id 296
    label "przeliczy&#263;"
  ]
  node [
    id 297
    label "antropochoria"
  ]
  node [
    id 298
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 299
    label "perceive"
  ]
  node [
    id 300
    label "reagowa&#263;"
  ]
  node [
    id 301
    label "male&#263;"
  ]
  node [
    id 302
    label "zmale&#263;"
  ]
  node [
    id 303
    label "spotka&#263;"
  ]
  node [
    id 304
    label "go_steady"
  ]
  node [
    id 305
    label "dostrzega&#263;"
  ]
  node [
    id 306
    label "zwi&#281;ksza&#263;_si&#281;"
  ]
  node [
    id 307
    label "zdawa&#263;_sobie_spraw&#281;"
  ]
  node [
    id 308
    label "ogl&#261;da&#263;"
  ]
  node [
    id 309
    label "os&#261;dza&#263;"
  ]
  node [
    id 310
    label "aprobowa&#263;"
  ]
  node [
    id 311
    label "punkt_widzenia"
  ]
  node [
    id 312
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 313
    label "wzrok"
  ]
  node [
    id 314
    label "postrzega&#263;"
  ]
  node [
    id 315
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 316
    label "fala"
  ]
  node [
    id 317
    label "ruch"
  ]
  node [
    id 318
    label "woda_powierzchniowa"
  ]
  node [
    id 319
    label "mn&#243;stwo"
  ]
  node [
    id 320
    label "Ajgospotamoj"
  ]
  node [
    id 321
    label "zjawisko"
  ]
  node [
    id 322
    label "ciek_wodny"
  ]
  node [
    id 323
    label "strona"
  ]
  node [
    id 324
    label "adres_elektroniczny"
  ]
  node [
    id 325
    label "domena"
  ]
  node [
    id 326
    label "po&#322;o&#380;enie"
  ]
  node [
    id 327
    label "kod_pocztowy"
  ]
  node [
    id 328
    label "dane"
  ]
  node [
    id 329
    label "przesy&#322;ka"
  ]
  node [
    id 330
    label "personalia"
  ]
  node [
    id 331
    label "siedziba"
  ]
  node [
    id 332
    label "dziedzina"
  ]
  node [
    id 333
    label "wielki"
  ]
  node [
    id 334
    label "rozpowszechnianie"
  ]
  node [
    id 335
    label "wyp&#322;yni&#281;cie"
  ]
  node [
    id 336
    label "tryb"
  ]
  node [
    id 337
    label "narz&#281;dzie"
  ]
  node [
    id 338
    label "nature"
  ]
  node [
    id 339
    label "byd&#322;o"
  ]
  node [
    id 340
    label "zobo"
  ]
  node [
    id 341
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 342
    label "yakalo"
  ]
  node [
    id 343
    label "dzo"
  ]
  node [
    id 344
    label "dzielna"
  ]
  node [
    id 345
    label "bicie"
  ]
  node [
    id 346
    label "mianownik"
  ]
  node [
    id 347
    label "u&#322;amek"
  ]
  node [
    id 348
    label "nabicie"
  ]
  node [
    id 349
    label "notowa&#263;"
  ]
  node [
    id 350
    label "wprowadza&#263;"
  ]
  node [
    id 351
    label "index"
  ]
  node [
    id 352
    label "wyraz_pochodny"
  ]
  node [
    id 353
    label "przystosowanie"
  ]
  node [
    id 354
    label "zmiana"
  ]
  node [
    id 355
    label "modification"
  ]
  node [
    id 356
    label "przer&#243;bka"
  ]
  node [
    id 357
    label "po&#347;rednio"
  ]
  node [
    id 358
    label "dos&#322;ownie"
  ]
  node [
    id 359
    label "wierny"
  ]
  node [
    id 360
    label "nieprzeno&#347;ny"
  ]
  node [
    id 361
    label "tekstualny"
  ]
  node [
    id 362
    label "use"
  ]
  node [
    id 363
    label "&#380;o&#322;nierz"
  ]
  node [
    id 364
    label "pies"
  ]
  node [
    id 365
    label "robi&#263;"
  ]
  node [
    id 366
    label "wait"
  ]
  node [
    id 367
    label "pomaga&#263;"
  ]
  node [
    id 368
    label "cel"
  ]
  node [
    id 369
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 370
    label "przydawa&#263;_si&#281;"
  ]
  node [
    id 371
    label "pracowa&#263;"
  ]
  node [
    id 372
    label "suffice"
  ]
  node [
    id 373
    label "match"
  ]
  node [
    id 374
    label "s&#322;ugiwa&#263;"
  ]
  node [
    id 375
    label "dar"
  ]
  node [
    id 376
    label "cnota"
  ]
  node [
    id 377
    label "buddyzm"
  ]
  node [
    id 378
    label "nagana"
  ]
  node [
    id 379
    label "wypowied&#378;"
  ]
  node [
    id 380
    label "dzienniczek"
  ]
  node [
    id 381
    label "wzgl&#261;d"
  ]
  node [
    id 382
    label "gossip"
  ]
  node [
    id 383
    label "upomnienie"
  ]
  node [
    id 384
    label "uprawi&#263;"
  ]
  node [
    id 385
    label "gotowy"
  ]
  node [
    id 386
    label "might"
  ]
  node [
    id 387
    label "oversight"
  ]
  node [
    id 388
    label "zlekcewa&#380;y&#263;"
  ]
  node [
    id 389
    label "wypowiedzenie"
  ]
  node [
    id 390
    label "spis"
  ]
  node [
    id 391
    label "odinstalowanie"
  ]
  node [
    id 392
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 393
    label "za&#322;o&#380;enie"
  ]
  node [
    id 394
    label "emitowanie"
  ]
  node [
    id 395
    label "odinstalowywanie"
  ]
  node [
    id 396
    label "instrukcja"
  ]
  node [
    id 397
    label "punkt"
  ]
  node [
    id 398
    label "teleferie"
  ]
  node [
    id 399
    label "emitowa&#263;"
  ]
  node [
    id 400
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 401
    label "sekcja_krytyczna"
  ]
  node [
    id 402
    label "prezentowa&#263;"
  ]
  node [
    id 403
    label "blok"
  ]
  node [
    id 404
    label "podprogram"
  ]
  node [
    id 405
    label "dzia&#322;"
  ]
  node [
    id 406
    label "broszura"
  ]
  node [
    id 407
    label "deklaracja"
  ]
  node [
    id 408
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 409
    label "struktura_organizacyjna"
  ]
  node [
    id 410
    label "zaprezentowanie"
  ]
  node [
    id 411
    label "informatyka"
  ]
  node [
    id 412
    label "booklet"
  ]
  node [
    id 413
    label "menu"
  ]
  node [
    id 414
    label "instalowanie"
  ]
  node [
    id 415
    label "furkacja"
  ]
  node [
    id 416
    label "odinstalowa&#263;"
  ]
  node [
    id 417
    label "instalowa&#263;"
  ]
  node [
    id 418
    label "okno"
  ]
  node [
    id 419
    label "pirat"
  ]
  node [
    id 420
    label "zainstalowanie"
  ]
  node [
    id 421
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 422
    label "ogranicznik_referencyjny"
  ]
  node [
    id 423
    label "zainstalowa&#263;"
  ]
  node [
    id 424
    label "kana&#322;"
  ]
  node [
    id 425
    label "zaprezentowa&#263;"
  ]
  node [
    id 426
    label "interfejs"
  ]
  node [
    id 427
    label "odinstalowywa&#263;"
  ]
  node [
    id 428
    label "folder"
  ]
  node [
    id 429
    label "course_of_study"
  ]
  node [
    id 430
    label "ram&#243;wka"
  ]
  node [
    id 431
    label "prezentowanie"
  ]
  node [
    id 432
    label "oferta"
  ]
  node [
    id 433
    label "rozciekawia&#263;"
  ]
  node [
    id 434
    label "sake"
  ]
  node [
    id 435
    label "si&#322;a"
  ]
  node [
    id 436
    label "lina"
  ]
  node [
    id 437
    label "way"
  ]
  node [
    id 438
    label "cable"
  ]
  node [
    id 439
    label "ch&#243;d"
  ]
  node [
    id 440
    label "trasa"
  ]
  node [
    id 441
    label "rz&#261;d"
  ]
  node [
    id 442
    label "k&#322;us"
  ]
  node [
    id 443
    label "progression"
  ]
  node [
    id 444
    label "current"
  ]
  node [
    id 445
    label "pr&#261;d"
  ]
  node [
    id 446
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 447
    label "lot"
  ]
  node [
    id 448
    label "give"
  ]
  node [
    id 449
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 450
    label "work"
  ]
  node [
    id 451
    label "muzyka"
  ]
  node [
    id 452
    label "rola"
  ]
  node [
    id 453
    label "praca"
  ]
  node [
    id 454
    label "Neumanna"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 454
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 125
  ]
  edge [
    source 7
    target 126
  ]
  edge [
    source 7
    target 127
  ]
  edge [
    source 7
    target 128
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 7
    target 130
  ]
  edge [
    source 7
    target 131
  ]
  edge [
    source 7
    target 132
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 36
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 22
  ]
  edge [
    source 9
    target 23
  ]
  edge [
    source 9
    target 133
  ]
  edge [
    source 9
    target 134
  ]
  edge [
    source 9
    target 135
  ]
  edge [
    source 9
    target 136
  ]
  edge [
    source 9
    target 137
  ]
  edge [
    source 9
    target 138
  ]
  edge [
    source 9
    target 139
  ]
  edge [
    source 9
    target 140
  ]
  edge [
    source 9
    target 141
  ]
  edge [
    source 9
    target 142
  ]
  edge [
    source 9
    target 46
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 13
  ]
  edge [
    source 10
    target 14
  ]
  edge [
    source 10
    target 16
  ]
  edge [
    source 10
    target 17
  ]
  edge [
    source 10
    target 27
  ]
  edge [
    source 10
    target 28
  ]
  edge [
    source 10
    target 43
  ]
  edge [
    source 10
    target 44
  ]
  edge [
    source 10
    target 51
  ]
  edge [
    source 10
    target 52
  ]
  edge [
    source 10
    target 143
  ]
  edge [
    source 10
    target 144
  ]
  edge [
    source 10
    target 145
  ]
  edge [
    source 10
    target 38
  ]
  edge [
    source 10
    target 40
  ]
  edge [
    source 10
    target 45
  ]
  edge [
    source 10
    target 22
  ]
  edge [
    source 10
    target 33
  ]
  edge [
    source 10
    target 54
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 23
  ]
  edge [
    source 11
    target 24
  ]
  edge [
    source 11
    target 32
  ]
  edge [
    source 11
    target 33
  ]
  edge [
    source 11
    target 34
  ]
  edge [
    source 11
    target 35
  ]
  edge [
    source 11
    target 38
  ]
  edge [
    source 11
    target 17
  ]
  edge [
    source 11
    target 58
  ]
  edge [
    source 11
    target 62
  ]
  edge [
    source 11
    target 146
  ]
  edge [
    source 11
    target 147
  ]
  edge [
    source 11
    target 148
  ]
  edge [
    source 11
    target 149
  ]
  edge [
    source 11
    target 150
  ]
  edge [
    source 11
    target 151
  ]
  edge [
    source 11
    target 152
  ]
  edge [
    source 11
    target 153
  ]
  edge [
    source 11
    target 154
  ]
  edge [
    source 11
    target 155
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 156
  ]
  edge [
    source 12
    target 157
  ]
  edge [
    source 12
    target 158
  ]
  edge [
    source 12
    target 159
  ]
  edge [
    source 12
    target 160
  ]
  edge [
    source 12
    target 161
  ]
  edge [
    source 12
    target 162
  ]
  edge [
    source 12
    target 163
  ]
  edge [
    source 12
    target 27
  ]
  edge [
    source 13
    target 25
  ]
  edge [
    source 13
    target 26
  ]
  edge [
    source 13
    target 164
  ]
  edge [
    source 13
    target 165
  ]
  edge [
    source 13
    target 166
  ]
  edge [
    source 13
    target 167
  ]
  edge [
    source 13
    target 168
  ]
  edge [
    source 13
    target 169
  ]
  edge [
    source 13
    target 170
  ]
  edge [
    source 13
    target 171
  ]
  edge [
    source 13
    target 172
  ]
  edge [
    source 13
    target 116
  ]
  edge [
    source 13
    target 173
  ]
  edge [
    source 13
    target 174
  ]
  edge [
    source 13
    target 175
  ]
  edge [
    source 13
    target 176
  ]
  edge [
    source 13
    target 177
  ]
  edge [
    source 13
    target 178
  ]
  edge [
    source 13
    target 179
  ]
  edge [
    source 13
    target 22
  ]
  edge [
    source 13
    target 33
  ]
  edge [
    source 13
    target 54
  ]
  edge [
    source 13
    target 38
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 180
  ]
  edge [
    source 16
    target 181
  ]
  edge [
    source 16
    target 167
  ]
  edge [
    source 16
    target 182
  ]
  edge [
    source 16
    target 172
  ]
  edge [
    source 16
    target 174
  ]
  edge [
    source 16
    target 176
  ]
  edge [
    source 16
    target 31
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 39
  ]
  edge [
    source 17
    target 183
  ]
  edge [
    source 17
    target 184
  ]
  edge [
    source 17
    target 185
  ]
  edge [
    source 17
    target 186
  ]
  edge [
    source 17
    target 187
  ]
  edge [
    source 17
    target 188
  ]
  edge [
    source 17
    target 127
  ]
  edge [
    source 17
    target 189
  ]
  edge [
    source 17
    target 190
  ]
  edge [
    source 17
    target 191
  ]
  edge [
    source 17
    target 192
  ]
  edge [
    source 17
    target 51
  ]
  edge [
    source 17
    target 55
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 193
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 194
  ]
  edge [
    source 19
    target 195
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 196
  ]
  edge [
    source 20
    target 197
  ]
  edge [
    source 20
    target 198
  ]
  edge [
    source 20
    target 199
  ]
  edge [
    source 20
    target 200
  ]
  edge [
    source 20
    target 201
  ]
  edge [
    source 20
    target 202
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 203
  ]
  edge [
    source 22
    target 204
  ]
  edge [
    source 22
    target 35
  ]
  edge [
    source 22
    target 33
  ]
  edge [
    source 22
    target 54
  ]
  edge [
    source 22
    target 38
  ]
  edge [
    source 23
    target 92
  ]
  edge [
    source 23
    target 205
  ]
  edge [
    source 23
    target 206
  ]
  edge [
    source 23
    target 207
  ]
  edge [
    source 23
    target 208
  ]
  edge [
    source 23
    target 209
  ]
  edge [
    source 23
    target 210
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 211
  ]
  edge [
    source 24
    target 212
  ]
  edge [
    source 24
    target 213
  ]
  edge [
    source 24
    target 214
  ]
  edge [
    source 24
    target 215
  ]
  edge [
    source 24
    target 216
  ]
  edge [
    source 24
    target 217
  ]
  edge [
    source 24
    target 218
  ]
  edge [
    source 24
    target 79
  ]
  edge [
    source 25
    target 193
  ]
  edge [
    source 25
    target 219
  ]
  edge [
    source 25
    target 220
  ]
  edge [
    source 25
    target 221
  ]
  edge [
    source 25
    target 222
  ]
  edge [
    source 25
    target 46
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 223
  ]
  edge [
    source 26
    target 224
  ]
  edge [
    source 26
    target 225
  ]
  edge [
    source 26
    target 226
  ]
  edge [
    source 26
    target 100
  ]
  edge [
    source 27
    target 227
  ]
  edge [
    source 27
    target 228
  ]
  edge [
    source 27
    target 229
  ]
  edge [
    source 27
    target 230
  ]
  edge [
    source 27
    target 231
  ]
  edge [
    source 27
    target 232
  ]
  edge [
    source 27
    target 233
  ]
  edge [
    source 27
    target 234
  ]
  edge [
    source 27
    target 235
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 38
  ]
  edge [
    source 28
    target 40
  ]
  edge [
    source 28
    target 45
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 236
  ]
  edge [
    source 29
    target 237
  ]
  edge [
    source 29
    target 238
  ]
  edge [
    source 29
    target 239
  ]
  edge [
    source 29
    target 240
  ]
  edge [
    source 29
    target 241
  ]
  edge [
    source 29
    target 242
  ]
  edge [
    source 29
    target 243
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 244
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 245
  ]
  edge [
    source 31
    target 246
  ]
  edge [
    source 31
    target 247
  ]
  edge [
    source 31
    target 248
  ]
  edge [
    source 31
    target 249
  ]
  edge [
    source 32
    target 250
  ]
  edge [
    source 32
    target 149
  ]
  edge [
    source 32
    target 251
  ]
  edge [
    source 32
    target 252
  ]
  edge [
    source 32
    target 253
  ]
  edge [
    source 32
    target 254
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 255
  ]
  edge [
    source 33
    target 256
  ]
  edge [
    source 33
    target 257
  ]
  edge [
    source 33
    target 258
  ]
  edge [
    source 33
    target 54
  ]
  edge [
    source 33
    target 38
  ]
  edge [
    source 34
    target 259
  ]
  edge [
    source 34
    target 260
  ]
  edge [
    source 34
    target 261
  ]
  edge [
    source 34
    target 262
  ]
  edge [
    source 34
    target 263
  ]
  edge [
    source 34
    target 264
  ]
  edge [
    source 34
    target 265
  ]
  edge [
    source 34
    target 266
  ]
  edge [
    source 34
    target 267
  ]
  edge [
    source 34
    target 268
  ]
  edge [
    source 34
    target 165
  ]
  edge [
    source 34
    target 269
  ]
  edge [
    source 34
    target 270
  ]
  edge [
    source 34
    target 271
  ]
  edge [
    source 34
    target 73
  ]
  edge [
    source 34
    target 272
  ]
  edge [
    source 34
    target 273
  ]
  edge [
    source 34
    target 274
  ]
  edge [
    source 34
    target 275
  ]
  edge [
    source 34
    target 276
  ]
  edge [
    source 34
    target 277
  ]
  edge [
    source 34
    target 278
  ]
  edge [
    source 34
    target 279
  ]
  edge [
    source 34
    target 280
  ]
  edge [
    source 34
    target 281
  ]
  edge [
    source 34
    target 282
  ]
  edge [
    source 34
    target 283
  ]
  edge [
    source 34
    target 284
  ]
  edge [
    source 34
    target 285
  ]
  edge [
    source 34
    target 286
  ]
  edge [
    source 34
    target 105
  ]
  edge [
    source 34
    target 287
  ]
  edge [
    source 34
    target 288
  ]
  edge [
    source 34
    target 289
  ]
  edge [
    source 34
    target 290
  ]
  edge [
    source 34
    target 291
  ]
  edge [
    source 34
    target 292
  ]
  edge [
    source 34
    target 293
  ]
  edge [
    source 34
    target 294
  ]
  edge [
    source 34
    target 295
  ]
  edge [
    source 34
    target 296
  ]
  edge [
    source 34
    target 297
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 298
  ]
  edge [
    source 35
    target 299
  ]
  edge [
    source 35
    target 300
  ]
  edge [
    source 35
    target 301
  ]
  edge [
    source 35
    target 302
  ]
  edge [
    source 35
    target 303
  ]
  edge [
    source 35
    target 304
  ]
  edge [
    source 35
    target 305
  ]
  edge [
    source 35
    target 306
  ]
  edge [
    source 35
    target 307
  ]
  edge [
    source 35
    target 308
  ]
  edge [
    source 35
    target 309
  ]
  edge [
    source 35
    target 310
  ]
  edge [
    source 35
    target 311
  ]
  edge [
    source 35
    target 312
  ]
  edge [
    source 35
    target 313
  ]
  edge [
    source 35
    target 314
  ]
  edge [
    source 35
    target 257
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 61
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 315
  ]
  edge [
    source 37
    target 316
  ]
  edge [
    source 37
    target 317
  ]
  edge [
    source 37
    target 318
  ]
  edge [
    source 37
    target 319
  ]
  edge [
    source 37
    target 320
  ]
  edge [
    source 37
    target 321
  ]
  edge [
    source 37
    target 322
  ]
  edge [
    source 38
    target 46
  ]
  edge [
    source 38
    target 47
  ]
  edge [
    source 38
    target 57
  ]
  edge [
    source 38
    target 61
  ]
  edge [
    source 38
    target 323
  ]
  edge [
    source 38
    target 324
  ]
  edge [
    source 38
    target 325
  ]
  edge [
    source 38
    target 326
  ]
  edge [
    source 38
    target 327
  ]
  edge [
    source 38
    target 328
  ]
  edge [
    source 38
    target 162
  ]
  edge [
    source 38
    target 329
  ]
  edge [
    source 38
    target 330
  ]
  edge [
    source 38
    target 331
  ]
  edge [
    source 38
    target 332
  ]
  edge [
    source 38
    target 40
  ]
  edge [
    source 38
    target 45
  ]
  edge [
    source 38
    target 44
  ]
  edge [
    source 38
    target 54
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 333
  ]
  edge [
    source 39
    target 334
  ]
  edge [
    source 39
    target 335
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 56
  ]
  edge [
    source 40
    target 57
  ]
  edge [
    source 40
    target 80
  ]
  edge [
    source 40
    target 116
  ]
  edge [
    source 40
    target 336
  ]
  edge [
    source 40
    target 337
  ]
  edge [
    source 40
    target 338
  ]
  edge [
    source 40
    target 45
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 339
  ]
  edge [
    source 41
    target 340
  ]
  edge [
    source 41
    target 341
  ]
  edge [
    source 41
    target 342
  ]
  edge [
    source 41
    target 343
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 43
    target 344
  ]
  edge [
    source 43
    target 345
  ]
  edge [
    source 43
    target 346
  ]
  edge [
    source 43
    target 347
  ]
  edge [
    source 43
    target 174
  ]
  edge [
    source 43
    target 348
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 349
  ]
  edge [
    source 44
    target 350
  ]
  edge [
    source 44
    target 351
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 352
  ]
  edge [
    source 45
    target 353
  ]
  edge [
    source 45
    target 354
  ]
  edge [
    source 45
    target 355
  ]
  edge [
    source 45
    target 356
  ]
  edge [
    source 46
    target 357
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 358
  ]
  edge [
    source 47
    target 359
  ]
  edge [
    source 47
    target 360
  ]
  edge [
    source 47
    target 361
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 51
    target 362
  ]
  edge [
    source 51
    target 184
  ]
  edge [
    source 51
    target 363
  ]
  edge [
    source 51
    target 364
  ]
  edge [
    source 51
    target 365
  ]
  edge [
    source 51
    target 366
  ]
  edge [
    source 51
    target 367
  ]
  edge [
    source 51
    target 368
  ]
  edge [
    source 51
    target 369
  ]
  edge [
    source 51
    target 370
  ]
  edge [
    source 51
    target 371
  ]
  edge [
    source 51
    target 372
  ]
  edge [
    source 51
    target 373
  ]
  edge [
    source 51
    target 374
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 375
  ]
  edge [
    source 53
    target 376
  ]
  edge [
    source 53
    target 377
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 378
  ]
  edge [
    source 54
    target 379
  ]
  edge [
    source 54
    target 186
  ]
  edge [
    source 54
    target 380
  ]
  edge [
    source 54
    target 150
  ]
  edge [
    source 54
    target 381
  ]
  edge [
    source 54
    target 382
  ]
  edge [
    source 54
    target 383
  ]
  edge [
    source 54
    target 178
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 384
  ]
  edge [
    source 55
    target 385
  ]
  edge [
    source 55
    target 386
  ]
  edge [
    source 56
    target 387
  ]
  edge [
    source 56
    target 388
  ]
  edge [
    source 57
    target 62
  ]
  edge [
    source 57
    target 389
  ]
  edge [
    source 57
    target 202
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 62
  ]
  edge [
    source 58
    target 63
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 63
  ]
  edge [
    source 59
    target 390
  ]
  edge [
    source 59
    target 391
  ]
  edge [
    source 59
    target 392
  ]
  edge [
    source 59
    target 393
  ]
  edge [
    source 59
    target 83
  ]
  edge [
    source 59
    target 394
  ]
  edge [
    source 59
    target 395
  ]
  edge [
    source 59
    target 396
  ]
  edge [
    source 59
    target 397
  ]
  edge [
    source 59
    target 398
  ]
  edge [
    source 59
    target 399
  ]
  edge [
    source 59
    target 148
  ]
  edge [
    source 59
    target 400
  ]
  edge [
    source 59
    target 401
  ]
  edge [
    source 59
    target 402
  ]
  edge [
    source 59
    target 403
  ]
  edge [
    source 59
    target 404
  ]
  edge [
    source 59
    target 336
  ]
  edge [
    source 59
    target 405
  ]
  edge [
    source 59
    target 406
  ]
  edge [
    source 59
    target 407
  ]
  edge [
    source 59
    target 408
  ]
  edge [
    source 59
    target 409
  ]
  edge [
    source 59
    target 410
  ]
  edge [
    source 59
    target 411
  ]
  edge [
    source 59
    target 412
  ]
  edge [
    source 59
    target 413
  ]
  edge [
    source 59
    target 103
  ]
  edge [
    source 59
    target 414
  ]
  edge [
    source 59
    target 415
  ]
  edge [
    source 59
    target 416
  ]
  edge [
    source 59
    target 417
  ]
  edge [
    source 59
    target 418
  ]
  edge [
    source 59
    target 419
  ]
  edge [
    source 59
    target 420
  ]
  edge [
    source 59
    target 421
  ]
  edge [
    source 59
    target 422
  ]
  edge [
    source 59
    target 423
  ]
  edge [
    source 59
    target 424
  ]
  edge [
    source 59
    target 425
  ]
  edge [
    source 59
    target 426
  ]
  edge [
    source 59
    target 427
  ]
  edge [
    source 59
    target 428
  ]
  edge [
    source 59
    target 429
  ]
  edge [
    source 59
    target 430
  ]
  edge [
    source 59
    target 431
  ]
  edge [
    source 59
    target 432
  ]
  edge [
    source 60
    target 433
  ]
  edge [
    source 60
    target 434
  ]
  edge [
    source 61
    target 435
  ]
  edge [
    source 61
    target 186
  ]
  edge [
    source 61
    target 436
  ]
  edge [
    source 61
    target 437
  ]
  edge [
    source 61
    target 438
  ]
  edge [
    source 61
    target 72
  ]
  edge [
    source 61
    target 116
  ]
  edge [
    source 61
    target 439
  ]
  edge [
    source 61
    target 440
  ]
  edge [
    source 61
    target 441
  ]
  edge [
    source 61
    target 442
  ]
  edge [
    source 61
    target 443
  ]
  edge [
    source 61
    target 444
  ]
  edge [
    source 61
    target 445
  ]
  edge [
    source 61
    target 446
  ]
  edge [
    source 61
    target 248
  ]
  edge [
    source 61
    target 447
  ]
  edge [
    source 62
    target 365
  ]
  edge [
    source 62
    target 200
  ]
  edge [
    source 62
    target 448
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 63
    target 449
  ]
  edge [
    source 63
    target 450
  ]
  edge [
    source 63
    target 365
  ]
  edge [
    source 63
    target 451
  ]
  edge [
    source 63
    target 452
  ]
  edge [
    source 63
    target 200
  ]
  edge [
    source 63
    target 453
  ]
]
