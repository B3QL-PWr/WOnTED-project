graph [
  maxDegree 3
  minDegree 0
  meanDegree 1.6666666666666667
  density 0.15151515151515152
  graphCliqueNumber 3
  node [
    id 0
    label "vestmanna"
    origin "text"
  ]
  node [
    id 1
    label "wyspa"
  ]
  node [
    id 2
    label "owczy"
  ]
  node [
    id 3
    label "Vestmanna"
  ]
  node [
    id 4
    label "kommuna"
  ]
  node [
    id 5
    label "ludzie"
  ]
  node [
    id 6
    label "zach&#243;d"
  ]
  node [
    id 7
    label "porto"
  ]
  node [
    id 8
    label "sp&#243;&#322;ka"
  ]
  node [
    id 9
    label "femininum"
  ]
  node [
    id 10
    label "Sk&#250;vadal"
  ]
  node [
    id 11
    label "Vestmannabj&#248;rgini"
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 8
    target 11
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 11
  ]
]
