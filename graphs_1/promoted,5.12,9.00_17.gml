graph [
  maxDegree 28
  minDegree 1
  meanDegree 1.9689922480620154
  density 0.015382751937984496
  graphCliqueNumber 3
  node [
    id 0
    label "jeden"
    origin "text"
  ]
  node [
    id 1
    label "popularny"
    origin "text"
  ]
  node [
    id 2
    label "ameryka&#324;ski"
    origin "text"
  ]
  node [
    id 3
    label "youtuber&#243;w"
    origin "text"
  ]
  node [
    id 4
    label "technologiczny"
    origin "text"
  ]
  node [
    id 5
    label "mkbhd"
    origin "text"
  ]
  node [
    id 6
    label "zrobi&#263;"
    origin "text"
  ]
  node [
    id 7
    label "&#347;lepy"
    origin "text"
  ]
  node [
    id 8
    label "test"
    origin "text"
  ]
  node [
    id 9
    label "aparat"
    origin "text"
  ]
  node [
    id 10
    label "smartfon"
    origin "text"
  ]
  node [
    id 11
    label "rok"
    origin "text"
  ]
  node [
    id 12
    label "kieliszek"
  ]
  node [
    id 13
    label "shot"
  ]
  node [
    id 14
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 15
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 16
    label "jaki&#347;"
  ]
  node [
    id 17
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 18
    label "jednolicie"
  ]
  node [
    id 19
    label "w&#243;dka"
  ]
  node [
    id 20
    label "ten"
  ]
  node [
    id 21
    label "ujednolicenie"
  ]
  node [
    id 22
    label "jednakowy"
  ]
  node [
    id 23
    label "przyst&#281;pny"
  ]
  node [
    id 24
    label "&#322;atwy"
  ]
  node [
    id 25
    label "popularnie"
  ]
  node [
    id 26
    label "znany"
  ]
  node [
    id 27
    label "nowoczesny"
  ]
  node [
    id 28
    label "p&#243;&#322;nocnoameryka&#324;ski"
  ]
  node [
    id 29
    label "boston"
  ]
  node [
    id 30
    label "po_ameryka&#324;sku"
  ]
  node [
    id 31
    label "cake-walk"
  ]
  node [
    id 32
    label "charakterystyczny"
  ]
  node [
    id 33
    label "placek_ameryka&#324;ski"
  ]
  node [
    id 34
    label "fajny"
  ]
  node [
    id 35
    label "j&#281;zyk_angielski"
  ]
  node [
    id 36
    label "Princeton"
  ]
  node [
    id 37
    label "pepperoni"
  ]
  node [
    id 38
    label "sa&#322;atka_Snickers"
  ]
  node [
    id 39
    label "zachodni"
  ]
  node [
    id 40
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 41
    label "anglosaski"
  ]
  node [
    id 42
    label "typowy"
  ]
  node [
    id 43
    label "zorganizowa&#263;"
  ]
  node [
    id 44
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 45
    label "przerobi&#263;"
  ]
  node [
    id 46
    label "wystylizowa&#263;"
  ]
  node [
    id 47
    label "cause"
  ]
  node [
    id 48
    label "wydali&#263;"
  ]
  node [
    id 49
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 50
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 51
    label "post&#261;pi&#263;"
  ]
  node [
    id 52
    label "appoint"
  ]
  node [
    id 53
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 54
    label "nabra&#263;"
  ]
  node [
    id 55
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 56
    label "make"
  ]
  node [
    id 57
    label "na_o&#347;lep"
  ]
  node [
    id 58
    label "&#347;lepak"
  ]
  node [
    id 59
    label "bezoki"
  ]
  node [
    id 60
    label "o&#347;lepni&#281;cie"
  ]
  node [
    id 61
    label "za&#347;lepianie"
  ]
  node [
    id 62
    label "o&#347;lepianie"
  ]
  node [
    id 63
    label "&#347;lepo"
  ]
  node [
    id 64
    label "za&#347;lepienie"
  ]
  node [
    id 65
    label "kaleki"
  ]
  node [
    id 66
    label "osoba_niepe&#322;nosprawna"
  ]
  node [
    id 67
    label "bezkrytyczny"
  ]
  node [
    id 68
    label "ciemny"
  ]
  node [
    id 69
    label "o&#347;lepienie"
  ]
  node [
    id 70
    label "&#347;lepni&#281;cie"
  ]
  node [
    id 71
    label "przypadkowy"
  ]
  node [
    id 72
    label "nieprzytomny"
  ]
  node [
    id 73
    label "do&#347;wiadczenie"
  ]
  node [
    id 74
    label "arkusz"
  ]
  node [
    id 75
    label "sprawdzian"
  ]
  node [
    id 76
    label "quiz"
  ]
  node [
    id 77
    label "przechodzi&#263;"
  ]
  node [
    id 78
    label "przechodzenie"
  ]
  node [
    id 79
    label "badanie"
  ]
  node [
    id 80
    label "narz&#281;dzie"
  ]
  node [
    id 81
    label "sytuacja"
  ]
  node [
    id 82
    label "ciemnia_optyczna"
  ]
  node [
    id 83
    label "przyrz&#261;d"
  ]
  node [
    id 84
    label "s&#322;uchawka_telefoniczna"
  ]
  node [
    id 85
    label "aparatownia"
  ]
  node [
    id 86
    label "orygina&#322;"
  ]
  node [
    id 87
    label "zi&#243;&#322;ko"
  ]
  node [
    id 88
    label "miech"
  ]
  node [
    id 89
    label "device"
  ]
  node [
    id 90
    label "w&#322;adza"
  ]
  node [
    id 91
    label "wy&#347;wietlacz"
  ]
  node [
    id 92
    label "dzwonienie"
  ]
  node [
    id 93
    label "zadzwoni&#263;"
  ]
  node [
    id 94
    label "dzwoni&#263;"
  ]
  node [
    id 95
    label "mikrotelefon"
  ]
  node [
    id 96
    label "wyzwalacz"
  ]
  node [
    id 97
    label "zesp&#243;&#322;"
  ]
  node [
    id 98
    label "celownik"
  ]
  node [
    id 99
    label "dekielek"
  ]
  node [
    id 100
    label "spust"
  ]
  node [
    id 101
    label "odleg&#322;o&#347;&#263;_hiperfokalna"
  ]
  node [
    id 102
    label "facet"
  ]
  node [
    id 103
    label "lampa_b&#322;yskowa"
  ]
  node [
    id 104
    label "mat&#243;wka"
  ]
  node [
    id 105
    label "urz&#261;dzenie"
  ]
  node [
    id 106
    label "migawka"
  ]
  node [
    id 107
    label "obiektyw"
  ]
  node [
    id 108
    label "kom&#243;rka"
  ]
  node [
    id 109
    label "ekran_dotykowy"
  ]
  node [
    id 110
    label "komputer_przeno&#347;ny"
  ]
  node [
    id 111
    label "stulecie"
  ]
  node [
    id 112
    label "kalendarz"
  ]
  node [
    id 113
    label "czas"
  ]
  node [
    id 114
    label "pora_roku"
  ]
  node [
    id 115
    label "cykl_astronomiczny"
  ]
  node [
    id 116
    label "p&#243;&#322;rocze"
  ]
  node [
    id 117
    label "grupa"
  ]
  node [
    id 118
    label "kwarta&#322;"
  ]
  node [
    id 119
    label "kurs"
  ]
  node [
    id 120
    label "jubileusz"
  ]
  node [
    id 121
    label "miesi&#261;c"
  ]
  node [
    id 122
    label "lata"
  ]
  node [
    id 123
    label "martwy_sezon"
  ]
  node [
    id 124
    label "Note"
  ]
  node [
    id 125
    label "9"
  ]
  node [
    id 126
    label "Samsunga"
  ]
  node [
    id 127
    label "Xiaomi"
  ]
  node [
    id 128
    label "Pocophone"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 9
    target 100
  ]
  edge [
    source 9
    target 101
  ]
  edge [
    source 9
    target 102
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 104
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 10
    target 110
  ]
  edge [
    source 11
    target 111
  ]
  edge [
    source 11
    target 112
  ]
  edge [
    source 11
    target 113
  ]
  edge [
    source 11
    target 114
  ]
  edge [
    source 11
    target 115
  ]
  edge [
    source 11
    target 116
  ]
  edge [
    source 11
    target 117
  ]
  edge [
    source 11
    target 118
  ]
  edge [
    source 11
    target 119
  ]
  edge [
    source 11
    target 120
  ]
  edge [
    source 11
    target 121
  ]
  edge [
    source 11
    target 122
  ]
  edge [
    source 11
    target 123
  ]
  edge [
    source 124
    target 125
  ]
  edge [
    source 124
    target 126
  ]
  edge [
    source 125
    target 126
  ]
  edge [
    source 127
    target 128
  ]
]
