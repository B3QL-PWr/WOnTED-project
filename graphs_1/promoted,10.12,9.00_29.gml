graph [
  maxDegree 18
  minDegree 1
  meanDegree 2
  density 0.022222222222222223
  graphCliqueNumber 2
  node [
    id 0
    label "jan"
    origin "text"
  ]
  node [
    id 1
    label "chlasta&#263;"
    origin "text"
  ]
  node [
    id 2
    label "nowa"
    origin "text"
  ]
  node [
    id 3
    label "skalmierzyce"
    origin "text"
  ]
  node [
    id 4
    label "pod"
    origin "text"
  ]
  node [
    id 5
    label "kalisz"
    origin "text"
  ]
  node [
    id 6
    label "by&#263;"
    origin "text"
  ]
  node [
    id 7
    label "rolnik"
    origin "text"
  ]
  node [
    id 8
    label "ogromny"
    origin "text"
  ]
  node [
    id 9
    label "wiedza"
    origin "text"
  ]
  node [
    id 10
    label "do&#347;wiadczenie"
    origin "text"
  ]
  node [
    id 11
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 12
    label "potwierdzenie"
    origin "text"
  ]
  node [
    id 13
    label "mi&#281;dzy_innymi"
    origin "text"
  ]
  node [
    id 14
    label "osi&#261;ga&#263;"
    origin "text"
  ]
  node [
    id 15
    label "plon"
    origin "text"
  ]
  node [
    id 16
    label "chlusta&#263;"
  ]
  node [
    id 17
    label "splash"
  ]
  node [
    id 18
    label "woda"
  ]
  node [
    id 19
    label "uderza&#263;"
  ]
  node [
    id 20
    label "rozcina&#263;"
  ]
  node [
    id 21
    label "gwiazda"
  ]
  node [
    id 22
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 23
    label "si&#281;ga&#263;"
  ]
  node [
    id 24
    label "trwa&#263;"
  ]
  node [
    id 25
    label "obecno&#347;&#263;"
  ]
  node [
    id 26
    label "stan"
  ]
  node [
    id 27
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 28
    label "stand"
  ]
  node [
    id 29
    label "mie&#263;_miejsce"
  ]
  node [
    id 30
    label "uczestniczy&#263;"
  ]
  node [
    id 31
    label "chodzi&#263;"
  ]
  node [
    id 32
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 33
    label "equal"
  ]
  node [
    id 34
    label "wie&#347;niak"
  ]
  node [
    id 35
    label "specjalista"
  ]
  node [
    id 36
    label "olbrzymio"
  ]
  node [
    id 37
    label "wyj&#261;tkowy"
  ]
  node [
    id 38
    label "ogromnie"
  ]
  node [
    id 39
    label "znaczny"
  ]
  node [
    id 40
    label "jebitny"
  ]
  node [
    id 41
    label "prawdziwy"
  ]
  node [
    id 42
    label "wa&#380;ny"
  ]
  node [
    id 43
    label "liczny"
  ]
  node [
    id 44
    label "dono&#347;ny"
  ]
  node [
    id 45
    label "pozwolenie"
  ]
  node [
    id 46
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 47
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 48
    label "wykszta&#322;cenie"
  ]
  node [
    id 49
    label "zaawansowanie"
  ]
  node [
    id 50
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 51
    label "intelekt"
  ]
  node [
    id 52
    label "cognition"
  ]
  node [
    id 53
    label "zbadanie"
  ]
  node [
    id 54
    label "skill"
  ]
  node [
    id 55
    label "wy&#347;wiadczenie"
  ]
  node [
    id 56
    label "znawstwo"
  ]
  node [
    id 57
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 58
    label "poczucie"
  ]
  node [
    id 59
    label "spotkanie"
  ]
  node [
    id 60
    label "do&#347;wiadczanie"
  ]
  node [
    id 61
    label "wydarzenie"
  ]
  node [
    id 62
    label "badanie"
  ]
  node [
    id 63
    label "assay"
  ]
  node [
    id 64
    label "obserwowanie"
  ]
  node [
    id 65
    label "checkup"
  ]
  node [
    id 66
    label "potraktowanie"
  ]
  node [
    id 67
    label "szko&#322;a"
  ]
  node [
    id 68
    label "eksperiencja"
  ]
  node [
    id 69
    label "dokument"
  ]
  node [
    id 70
    label "sanction"
  ]
  node [
    id 71
    label "certificate"
  ]
  node [
    id 72
    label "przy&#347;wiadczenie"
  ]
  node [
    id 73
    label "o&#347;wiadczenie"
  ]
  node [
    id 74
    label "stwierdzenie"
  ]
  node [
    id 75
    label "kontrasygnowanie"
  ]
  node [
    id 76
    label "zgodzenie_si&#281;"
  ]
  node [
    id 77
    label "get"
  ]
  node [
    id 78
    label "uzyskiwa&#263;"
  ]
  node [
    id 79
    label "mark"
  ]
  node [
    id 80
    label "dochrapywa&#263;_si&#281;"
  ]
  node [
    id 81
    label "dociera&#263;"
  ]
  node [
    id 82
    label "return"
  ]
  node [
    id 83
    label "naturalia"
  ]
  node [
    id 84
    label "wyda&#263;"
  ]
  node [
    id 85
    label "metr"
  ]
  node [
    id 86
    label "wydawa&#263;"
  ]
  node [
    id 87
    label "produkcja"
  ]
  node [
    id 88
    label "rezultat"
  ]
  node [
    id 89
    label "Jan"
  ]
  node [
    id 90
    label "Skalmierzyce"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 12
  ]
  edge [
    source 6
    target 13
  ]
  edge [
    source 6
    target 23
  ]
  edge [
    source 6
    target 24
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 34
  ]
  edge [
    source 7
    target 35
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 36
  ]
  edge [
    source 8
    target 37
  ]
  edge [
    source 8
    target 38
  ]
  edge [
    source 8
    target 39
  ]
  edge [
    source 8
    target 40
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 45
  ]
  edge [
    source 9
    target 46
  ]
  edge [
    source 9
    target 47
  ]
  edge [
    source 9
    target 48
  ]
  edge [
    source 9
    target 49
  ]
  edge [
    source 9
    target 50
  ]
  edge [
    source 9
    target 51
  ]
  edge [
    source 9
    target 52
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 53
  ]
  edge [
    source 10
    target 54
  ]
  edge [
    source 10
    target 55
  ]
  edge [
    source 10
    target 56
  ]
  edge [
    source 10
    target 57
  ]
  edge [
    source 10
    target 58
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 10
    target 60
  ]
  edge [
    source 10
    target 61
  ]
  edge [
    source 10
    target 62
  ]
  edge [
    source 10
    target 63
  ]
  edge [
    source 10
    target 64
  ]
  edge [
    source 10
    target 65
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 69
  ]
  edge [
    source 12
    target 70
  ]
  edge [
    source 12
    target 71
  ]
  edge [
    source 12
    target 72
  ]
  edge [
    source 12
    target 73
  ]
  edge [
    source 12
    target 74
  ]
  edge [
    source 12
    target 75
  ]
  edge [
    source 12
    target 76
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 77
  ]
  edge [
    source 14
    target 78
  ]
  edge [
    source 14
    target 79
  ]
  edge [
    source 14
    target 80
  ]
  edge [
    source 14
    target 81
  ]
  edge [
    source 15
    target 82
  ]
  edge [
    source 15
    target 83
  ]
  edge [
    source 15
    target 84
  ]
  edge [
    source 15
    target 85
  ]
  edge [
    source 15
    target 86
  ]
  edge [
    source 15
    target 87
  ]
  edge [
    source 15
    target 88
  ]
]
