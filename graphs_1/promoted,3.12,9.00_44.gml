graph [
  maxDegree 34
  minDegree 1
  meanDegree 1.9743589743589745
  density 0.02564102564102564
  graphCliqueNumber 2
  node [
    id 0
    label "zaczyna&#263;"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "realizowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "czarny"
    origin "text"
  ]
  node [
    id 4
    label "scenariusz"
    origin "text"
  ]
  node [
    id 5
    label "dla"
    origin "text"
  ]
  node [
    id 6
    label "rz&#261;d"
    origin "text"
  ]
  node [
    id 7
    label "open"
  ]
  node [
    id 8
    label "odejmowa&#263;"
  ]
  node [
    id 9
    label "mie&#263;_miejsce"
  ]
  node [
    id 10
    label "robi&#263;_pierwszy_krok"
  ]
  node [
    id 11
    label "set_about"
  ]
  node [
    id 12
    label "begin"
  ]
  node [
    id 13
    label "post&#281;powa&#263;"
  ]
  node [
    id 14
    label "bankrupt"
  ]
  node [
    id 15
    label "wykorzystywa&#263;"
  ]
  node [
    id 16
    label "przeprowadza&#263;"
  ]
  node [
    id 17
    label "spieni&#281;&#380;a&#263;"
  ]
  node [
    id 18
    label "prosecute"
  ]
  node [
    id 19
    label "create"
  ]
  node [
    id 20
    label "prawdzi&#263;"
  ]
  node [
    id 21
    label "tworzy&#263;"
  ]
  node [
    id 22
    label "wedel"
  ]
  node [
    id 23
    label "pessimistic"
  ]
  node [
    id 24
    label "ciemnienie"
  ]
  node [
    id 25
    label "brudny"
  ]
  node [
    id 26
    label "abolicjonista"
  ]
  node [
    id 27
    label "gorzki"
  ]
  node [
    id 28
    label "czarnuchowaty"
  ]
  node [
    id 29
    label "ciemnosk&#243;ry"
  ]
  node [
    id 30
    label "zaczernienie"
  ]
  node [
    id 31
    label "bierka_szachowa"
  ]
  node [
    id 32
    label "czarnuch"
  ]
  node [
    id 33
    label "okrutny"
  ]
  node [
    id 34
    label "negatywny"
  ]
  node [
    id 35
    label "zaczernianie_si&#281;"
  ]
  node [
    id 36
    label "czarne"
  ]
  node [
    id 37
    label "niepomy&#347;lny"
  ]
  node [
    id 38
    label "kafar"
  ]
  node [
    id 39
    label "czarno"
  ]
  node [
    id 40
    label "z&#322;y"
  ]
  node [
    id 41
    label "ciemny"
  ]
  node [
    id 42
    label "czernienie"
  ]
  node [
    id 43
    label "beznadziejny"
  ]
  node [
    id 44
    label "ksi&#261;dz"
  ]
  node [
    id 45
    label "pesymistycznie"
  ]
  node [
    id 46
    label "kompletny"
  ]
  node [
    id 47
    label "granatowo"
  ]
  node [
    id 48
    label "czarniawy"
  ]
  node [
    id 49
    label "przewrotny"
  ]
  node [
    id 50
    label "murzy&#324;ski"
  ]
  node [
    id 51
    label "kolorowy"
  ]
  node [
    id 52
    label "zaczernienie_si&#281;"
  ]
  node [
    id 53
    label "ponury"
  ]
  node [
    id 54
    label "scenario"
  ]
  node [
    id 55
    label "prognoza"
  ]
  node [
    id 56
    label "dramat"
  ]
  node [
    id 57
    label "rola"
  ]
  node [
    id 58
    label "plan"
  ]
  node [
    id 59
    label "tekst"
  ]
  node [
    id 60
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 61
    label "kategoria"
  ]
  node [
    id 62
    label "egzekutywa"
  ]
  node [
    id 63
    label "gabinet_cieni"
  ]
  node [
    id 64
    label "gromada"
  ]
  node [
    id 65
    label "premier"
  ]
  node [
    id 66
    label "Londyn"
  ]
  node [
    id 67
    label "Konsulat"
  ]
  node [
    id 68
    label "uporz&#261;dkowanie"
  ]
  node [
    id 69
    label "jednostka_systematyczna"
  ]
  node [
    id 70
    label "szpaler"
  ]
  node [
    id 71
    label "przybli&#380;enie"
  ]
  node [
    id 72
    label "tract"
  ]
  node [
    id 73
    label "number"
  ]
  node [
    id 74
    label "lon&#380;a"
  ]
  node [
    id 75
    label "w&#322;adza"
  ]
  node [
    id 76
    label "instytucja"
  ]
  node [
    id 77
    label "klasa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
]
