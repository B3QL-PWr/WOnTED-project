graph [
  maxDegree 71
  minDegree 1
  meanDegree 2
  density 0.015267175572519083
  graphCliqueNumber 2
  node [
    id 0
    label "kilka"
    origin "text"
  ]
  node [
    id 1
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 2
    label "temu"
    origin "text"
  ]
  node [
    id 3
    label "chi&#324;ski"
    origin "text"
  ]
  node [
    id 4
    label "naukowiec"
    origin "text"
  ]
  node [
    id 5
    label "poinformowa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 7
    label "poroda"
    origin "text"
  ]
  node [
    id 8
    label "genetycznie"
    origin "text"
  ]
  node [
    id 9
    label "zmodyfikowa&#263;"
    origin "text"
  ]
  node [
    id 10
    label "bli&#378;niaczek"
    origin "text"
  ]
  node [
    id 11
    label "&#347;ledziowate"
  ]
  node [
    id 12
    label "ryba"
  ]
  node [
    id 13
    label "s&#322;o&#324;ce"
  ]
  node [
    id 14
    label "czynienie_si&#281;"
  ]
  node [
    id 15
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 16
    label "czas"
  ]
  node [
    id 17
    label "long_time"
  ]
  node [
    id 18
    label "przedpo&#322;udnie"
  ]
  node [
    id 19
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 20
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 21
    label "tydzie&#324;"
  ]
  node [
    id 22
    label "godzina"
  ]
  node [
    id 23
    label "t&#322;usty_czwartek"
  ]
  node [
    id 24
    label "wsta&#263;"
  ]
  node [
    id 25
    label "day"
  ]
  node [
    id 26
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 27
    label "przedwiecz&#243;r"
  ]
  node [
    id 28
    label "Sylwester"
  ]
  node [
    id 29
    label "po&#322;udnie"
  ]
  node [
    id 30
    label "wzej&#347;cie"
  ]
  node [
    id 31
    label "podwiecz&#243;r"
  ]
  node [
    id 32
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 33
    label "rano"
  ]
  node [
    id 34
    label "termin"
  ]
  node [
    id 35
    label "ranek"
  ]
  node [
    id 36
    label "doba"
  ]
  node [
    id 37
    label "wiecz&#243;r"
  ]
  node [
    id 38
    label "walentynki"
  ]
  node [
    id 39
    label "popo&#322;udnie"
  ]
  node [
    id 40
    label "noc"
  ]
  node [
    id 41
    label "wstanie"
  ]
  node [
    id 42
    label "kitajski"
  ]
  node [
    id 43
    label "j&#281;zyk"
  ]
  node [
    id 44
    label "kaczka_po_peki&#324;sku"
  ]
  node [
    id 45
    label "dziwaczny"
  ]
  node [
    id 46
    label "tandetny"
  ]
  node [
    id 47
    label "makroj&#281;zyk"
  ]
  node [
    id 48
    label "chi&#324;sko"
  ]
  node [
    id 49
    label "po_chi&#324;sku"
  ]
  node [
    id 50
    label "j&#281;zyk_chi&#324;sko-tybeta&#324;ski"
  ]
  node [
    id 51
    label "azjatycki"
  ]
  node [
    id 52
    label "lipny"
  ]
  node [
    id 53
    label "go"
  ]
  node [
    id 54
    label "niedrogi"
  ]
  node [
    id 55
    label "dalekowschodni"
  ]
  node [
    id 56
    label "Miczurin"
  ]
  node [
    id 57
    label "&#347;ledziciel"
  ]
  node [
    id 58
    label "uczony"
  ]
  node [
    id 59
    label "zakomunikowa&#263;"
  ]
  node [
    id 60
    label "inform"
  ]
  node [
    id 61
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 62
    label "obszar"
  ]
  node [
    id 63
    label "obiekt_naturalny"
  ]
  node [
    id 64
    label "przedmiot"
  ]
  node [
    id 65
    label "Stary_&#346;wiat"
  ]
  node [
    id 66
    label "grupa"
  ]
  node [
    id 67
    label "stw&#243;r"
  ]
  node [
    id 68
    label "biosfera"
  ]
  node [
    id 69
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 70
    label "rzecz"
  ]
  node [
    id 71
    label "magnetosfera"
  ]
  node [
    id 72
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 73
    label "environment"
  ]
  node [
    id 74
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 75
    label "geosfera"
  ]
  node [
    id 76
    label "Nowy_&#346;wiat"
  ]
  node [
    id 77
    label "planeta"
  ]
  node [
    id 78
    label "przejmowa&#263;"
  ]
  node [
    id 79
    label "litosfera"
  ]
  node [
    id 80
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 81
    label "makrokosmos"
  ]
  node [
    id 82
    label "barysfera"
  ]
  node [
    id 83
    label "biota"
  ]
  node [
    id 84
    label "p&#243;&#322;noc"
  ]
  node [
    id 85
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 86
    label "fauna"
  ]
  node [
    id 87
    label "wszechstworzenie"
  ]
  node [
    id 88
    label "geotermia"
  ]
  node [
    id 89
    label "biegun"
  ]
  node [
    id 90
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 91
    label "ekosystem"
  ]
  node [
    id 92
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 93
    label "teren"
  ]
  node [
    id 94
    label "zjawisko"
  ]
  node [
    id 95
    label "p&#243;&#322;kula"
  ]
  node [
    id 96
    label "atmosfera"
  ]
  node [
    id 97
    label "mikrokosmos"
  ]
  node [
    id 98
    label "class"
  ]
  node [
    id 99
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 100
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 101
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 102
    label "przejmowanie"
  ]
  node [
    id 103
    label "przestrze&#324;"
  ]
  node [
    id 104
    label "asymilowanie_si&#281;"
  ]
  node [
    id 105
    label "przej&#261;&#263;"
  ]
  node [
    id 106
    label "ekosfera"
  ]
  node [
    id 107
    label "przyroda"
  ]
  node [
    id 108
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 109
    label "ciemna_materia"
  ]
  node [
    id 110
    label "geoida"
  ]
  node [
    id 111
    label "Wsch&#243;d"
  ]
  node [
    id 112
    label "populace"
  ]
  node [
    id 113
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 114
    label "huczek"
  ]
  node [
    id 115
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 116
    label "Ziemia"
  ]
  node [
    id 117
    label "universe"
  ]
  node [
    id 118
    label "ozonosfera"
  ]
  node [
    id 119
    label "rze&#378;ba"
  ]
  node [
    id 120
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 121
    label "zagranica"
  ]
  node [
    id 122
    label "hydrosfera"
  ]
  node [
    id 123
    label "woda"
  ]
  node [
    id 124
    label "kuchnia"
  ]
  node [
    id 125
    label "przej&#281;cie"
  ]
  node [
    id 126
    label "czarna_dziura"
  ]
  node [
    id 127
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 128
    label "morze"
  ]
  node [
    id 129
    label "dziedzicznie"
  ]
  node [
    id 130
    label "genetyczny"
  ]
  node [
    id 131
    label "zmieni&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 129
  ]
  edge [
    source 8
    target 130
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 131
  ]
]
