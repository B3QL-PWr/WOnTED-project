graph [
  maxDegree 12
  minDegree 1
  meanDegree 1.8461538461538463
  density 0.15384615384615385
  graphCliqueNumber 2
  node [
    id 0
    label "iskra"
    origin "text"
  ]
  node [
    id 1
    label "cz&#322;owiek"
  ]
  node [
    id 2
    label "odrobina"
  ]
  node [
    id 3
    label "b&#322;ysk"
  ]
  node [
    id 4
    label "freshness"
  ]
  node [
    id 5
    label "przyczyna"
  ]
  node [
    id 6
    label "flicker"
  ]
  node [
    id 7
    label "glint"
  ]
  node [
    id 8
    label "&#380;agiew"
  ]
  node [
    id 9
    label "discharge"
  ]
  node [
    id 10
    label "odblask"
  ]
  node [
    id 11
    label "pocz&#261;tek"
  ]
  node [
    id 12
    label "blask"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
]
