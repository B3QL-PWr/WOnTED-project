graph [
  maxDegree 15
  minDegree 1
  meanDegree 2.020408163265306
  density 0.020828950136755734
  graphCliqueNumber 2
  node [
    id 0
    label "jaki"
    origin "text"
  ]
  node [
    id 1
    label "kosmita"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;byby&#263;"
    origin "text"
  ]
  node [
    id 3
    label "uniwersum"
    origin "text"
  ]
  node [
    id 4
    label "kapitan"
    origin "text"
  ]
  node [
    id 5
    label "bomba"
    origin "text"
  ]
  node [
    id 6
    label "ostatni"
    origin "text"
  ]
  node [
    id 7
    label "cyfra"
    origin "text"
  ]
  node [
    id 8
    label "liczba"
    origin "text"
  ]
  node [
    id 9
    label "plus"
    origin "text"
  ]
  node [
    id 10
    label "prawda"
    origin "text"
  ]
  node [
    id 11
    label "powiedzie&#263;"
    origin "text"
  ]
  node [
    id 12
    label "osoba"
  ]
  node [
    id 13
    label "istota_&#380;ywa"
  ]
  node [
    id 14
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 15
    label "makrokosmos"
  ]
  node [
    id 16
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 17
    label "czarna_dziura"
  ]
  node [
    id 18
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 19
    label "przestrze&#324;"
  ]
  node [
    id 20
    label "planeta"
  ]
  node [
    id 21
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 22
    label "ekosfera"
  ]
  node [
    id 23
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 24
    label "poj&#281;cie"
  ]
  node [
    id 25
    label "ciemna_materia"
  ]
  node [
    id 26
    label "mikrokosmos"
  ]
  node [
    id 27
    label "pilot"
  ]
  node [
    id 28
    label "stopie&#324;_wojskowy"
  ]
  node [
    id 29
    label "oficer"
  ]
  node [
    id 30
    label "oficer_&#380;eglugi"
  ]
  node [
    id 31
    label "dow&#243;dca"
  ]
  node [
    id 32
    label "gracz"
  ]
  node [
    id 33
    label "zawodnik"
  ]
  node [
    id 34
    label "niedostateczny"
  ]
  node [
    id 35
    label "zapalnik"
  ]
  node [
    id 36
    label "novum"
  ]
  node [
    id 37
    label "czerep"
  ]
  node [
    id 38
    label "strza&#322;"
  ]
  node [
    id 39
    label "pocisk"
  ]
  node [
    id 40
    label "materia&#322;_piroklastyczny"
  ]
  node [
    id 41
    label "bombowiec"
  ]
  node [
    id 42
    label "nab&#243;j"
  ]
  node [
    id 43
    label "cz&#322;owiek"
  ]
  node [
    id 44
    label "kolejny"
  ]
  node [
    id 45
    label "najgorszy"
  ]
  node [
    id 46
    label "aktualny"
  ]
  node [
    id 47
    label "ostatnio"
  ]
  node [
    id 48
    label "niedawno"
  ]
  node [
    id 49
    label "wi&#261;&#380;&#261;cy"
  ]
  node [
    id 50
    label "sko&#324;czony"
  ]
  node [
    id 51
    label "poprzedni"
  ]
  node [
    id 52
    label "pozosta&#322;y"
  ]
  node [
    id 53
    label "w&#261;tpliwy"
  ]
  node [
    id 54
    label "inicja&#322;"
  ]
  node [
    id 55
    label "wz&#243;r"
  ]
  node [
    id 56
    label "znak_pisarski"
  ]
  node [
    id 57
    label "kategoria"
  ]
  node [
    id 58
    label "kategoria_gramatyczna"
  ]
  node [
    id 59
    label "kwadrat_magiczny"
  ]
  node [
    id 60
    label "cecha"
  ]
  node [
    id 61
    label "grupa"
  ]
  node [
    id 62
    label "wyra&#380;enie"
  ]
  node [
    id 63
    label "pierwiastek"
  ]
  node [
    id 64
    label "rozmiar"
  ]
  node [
    id 65
    label "number"
  ]
  node [
    id 66
    label "koniugacja"
  ]
  node [
    id 67
    label "warto&#347;&#263;"
  ]
  node [
    id 68
    label "rewaluowa&#263;"
  ]
  node [
    id 69
    label "wabik"
  ]
  node [
    id 70
    label "korzy&#347;&#263;"
  ]
  node [
    id 71
    label "dodawanie"
  ]
  node [
    id 72
    label "rewaluowanie"
  ]
  node [
    id 73
    label "stopie&#324;"
  ]
  node [
    id 74
    label "ocena"
  ]
  node [
    id 75
    label "zrewaluowa&#263;"
  ]
  node [
    id 76
    label "znak_matematyczny"
  ]
  node [
    id 77
    label "strona"
  ]
  node [
    id 78
    label "zrewaluowanie"
  ]
  node [
    id 79
    label "s&#261;d"
  ]
  node [
    id 80
    label "truth"
  ]
  node [
    id 81
    label "nieprawdziwy"
  ]
  node [
    id 82
    label "za&#322;o&#380;enie"
  ]
  node [
    id 83
    label "prawdziwy"
  ]
  node [
    id 84
    label "realia"
  ]
  node [
    id 85
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 86
    label "express"
  ]
  node [
    id 87
    label "rzekn&#261;&#263;"
  ]
  node [
    id 88
    label "okre&#347;li&#263;"
  ]
  node [
    id 89
    label "wyrazi&#263;"
  ]
  node [
    id 90
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 91
    label "unwrap"
  ]
  node [
    id 92
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 93
    label "convey"
  ]
  node [
    id 94
    label "discover"
  ]
  node [
    id 95
    label "wydoby&#263;"
  ]
  node [
    id 96
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 97
    label "poda&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 13
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 24
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 11
    target 85
  ]
  edge [
    source 11
    target 86
  ]
  edge [
    source 11
    target 87
  ]
  edge [
    source 11
    target 88
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 11
    target 92
  ]
  edge [
    source 11
    target 93
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 11
    target 97
  ]
]
