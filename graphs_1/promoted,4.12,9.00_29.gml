graph [
  maxDegree 28
  minDegree 1
  meanDegree 1.978494623655914
  density 0.021505376344086023
  graphCliqueNumber 3
  node [
    id 0
    label "zdj&#281;cia"
    origin "text"
  ]
  node [
    id 1
    label "guang"
    origin "text"
  ]
  node [
    id 2
    label "ujawni&#263;"
    origin "text"
  ]
  node [
    id 3
    label "chiny"
    origin "text"
  ]
  node [
    id 4
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 5
    label "rz&#261;d"
    origin "text"
  ]
  node [
    id 6
    label "chcie&#263;"
    origin "text"
  ]
  node [
    id 7
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 8
    label "narkomania"
    origin "text"
  ]
  node [
    id 9
    label "pacjent"
    origin "text"
  ]
  node [
    id 10
    label "hiv"
    origin "text"
  ]
  node [
    id 11
    label "problem"
    origin "text"
  ]
  node [
    id 12
    label "&#347;rodowiskowy"
    origin "text"
  ]
  node [
    id 13
    label "dostrzec"
  ]
  node [
    id 14
    label "denounce"
  ]
  node [
    id 15
    label "discover"
  ]
  node [
    id 16
    label "objawi&#263;"
  ]
  node [
    id 17
    label "poinformowa&#263;"
  ]
  node [
    id 18
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 19
    label "kategoria"
  ]
  node [
    id 20
    label "egzekutywa"
  ]
  node [
    id 21
    label "gabinet_cieni"
  ]
  node [
    id 22
    label "gromada"
  ]
  node [
    id 23
    label "premier"
  ]
  node [
    id 24
    label "Londyn"
  ]
  node [
    id 25
    label "Konsulat"
  ]
  node [
    id 26
    label "uporz&#261;dkowanie"
  ]
  node [
    id 27
    label "jednostka_systematyczna"
  ]
  node [
    id 28
    label "szpaler"
  ]
  node [
    id 29
    label "przybli&#380;enie"
  ]
  node [
    id 30
    label "tract"
  ]
  node [
    id 31
    label "number"
  ]
  node [
    id 32
    label "lon&#380;a"
  ]
  node [
    id 33
    label "w&#322;adza"
  ]
  node [
    id 34
    label "instytucja"
  ]
  node [
    id 35
    label "klasa"
  ]
  node [
    id 36
    label "czu&#263;"
  ]
  node [
    id 37
    label "desire"
  ]
  node [
    id 38
    label "kcie&#263;"
  ]
  node [
    id 39
    label "remark"
  ]
  node [
    id 40
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 41
    label "u&#380;ywa&#263;"
  ]
  node [
    id 42
    label "okre&#347;la&#263;"
  ]
  node [
    id 43
    label "j&#281;zyk"
  ]
  node [
    id 44
    label "say"
  ]
  node [
    id 45
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 46
    label "formu&#322;owa&#263;"
  ]
  node [
    id 47
    label "talk"
  ]
  node [
    id 48
    label "powiada&#263;"
  ]
  node [
    id 49
    label "informowa&#263;"
  ]
  node [
    id 50
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 51
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 52
    label "wydobywa&#263;"
  ]
  node [
    id 53
    label "express"
  ]
  node [
    id 54
    label "chew_the_fat"
  ]
  node [
    id 55
    label "dysfonia"
  ]
  node [
    id 56
    label "umie&#263;"
  ]
  node [
    id 57
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 58
    label "tell"
  ]
  node [
    id 59
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 60
    label "wyra&#380;a&#263;"
  ]
  node [
    id 61
    label "gaworzy&#263;"
  ]
  node [
    id 62
    label "rozmawia&#263;"
  ]
  node [
    id 63
    label "dziama&#263;"
  ]
  node [
    id 64
    label "prawi&#263;"
  ]
  node [
    id 65
    label "na&#322;&#243;g"
  ]
  node [
    id 66
    label "drug_addiction"
  ]
  node [
    id 67
    label "flashback"
  ]
  node [
    id 68
    label "patologia"
  ]
  node [
    id 69
    label "od&#322;&#261;czenie"
  ]
  node [
    id 70
    label "cz&#322;owiek"
  ]
  node [
    id 71
    label "przymus_bezpo&#347;redni"
  ]
  node [
    id 72
    label "od&#322;&#261;czanie"
  ]
  node [
    id 73
    label "od&#322;&#261;czy&#263;"
  ]
  node [
    id 74
    label "klient"
  ]
  node [
    id 75
    label "od&#322;&#261;cza&#263;"
  ]
  node [
    id 76
    label "szpitalnik"
  ]
  node [
    id 77
    label "przypadek"
  ]
  node [
    id 78
    label "chory"
  ]
  node [
    id 79
    label "piel&#281;gniarz"
  ]
  node [
    id 80
    label "trudno&#347;&#263;"
  ]
  node [
    id 81
    label "sprawa"
  ]
  node [
    id 82
    label "ambaras"
  ]
  node [
    id 83
    label "problemat"
  ]
  node [
    id 84
    label "pierepa&#322;ka"
  ]
  node [
    id 85
    label "obstruction"
  ]
  node [
    id 86
    label "problematyka"
  ]
  node [
    id 87
    label "jajko_Kolumba"
  ]
  node [
    id 88
    label "subiekcja"
  ]
  node [
    id 89
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 90
    label "zdj&#281;cie"
  ]
  node [
    id 91
    label "lu"
  ]
  node [
    id 92
    label "Guang"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 39
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 80
  ]
  edge [
    source 11
    target 81
  ]
  edge [
    source 11
    target 82
  ]
  edge [
    source 11
    target 83
  ]
  edge [
    source 11
    target 84
  ]
  edge [
    source 11
    target 85
  ]
  edge [
    source 11
    target 86
  ]
  edge [
    source 11
    target 87
  ]
  edge [
    source 11
    target 88
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 90
    target 91
  ]
  edge [
    source 90
    target 92
  ]
  edge [
    source 91
    target 92
  ]
]
