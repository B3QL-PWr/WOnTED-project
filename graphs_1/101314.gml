graph [
  maxDegree 1
  minDegree 1
  meanDegree 1
  density 0.2
  graphCliqueNumber 2
  node [
    id 0
    label "steeple"
    origin "text"
  ]
  node [
    id 1
    label "morden"
    origin "text"
  ]
  node [
    id 2
    label "Steeple"
  ]
  node [
    id 3
    label "Morden"
  ]
  node [
    id 4
    label "South"
  ]
  node [
    id 5
    label "Cambridgeshire"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
]
