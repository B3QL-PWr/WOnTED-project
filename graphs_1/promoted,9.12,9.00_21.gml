graph [
  maxDegree 45
  minDegree 1
  meanDegree 1.9540229885057472
  density 0.0227211975407645
  graphCliqueNumber 3
  node [
    id 0
    label "korona"
    origin "text"
  ]
  node [
    id 1
    label "kr&#243;l"
    origin "text"
  ]
  node [
    id 2
    label "p&#322;atek"
  ]
  node [
    id 3
    label "ro&#347;lina_wieloletnia"
  ]
  node [
    id 4
    label "nakrycie_g&#322;owy"
  ]
  node [
    id 5
    label "proteza_dentystyczna"
  ]
  node [
    id 6
    label "czub"
  ]
  node [
    id 7
    label "z&#261;b"
  ]
  node [
    id 8
    label "Kr&#243;lestwo_Prus"
  ]
  node [
    id 9
    label "element"
  ]
  node [
    id 10
    label "wieniec"
  ]
  node [
    id 11
    label "diadem"
  ]
  node [
    id 12
    label "liliowate"
  ]
  node [
    id 13
    label "znak_muzyczny"
  ]
  node [
    id 14
    label "g&#243;ra"
  ]
  node [
    id 15
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 16
    label "r&#243;g"
  ]
  node [
    id 17
    label "kres"
  ]
  node [
    id 18
    label "urz&#261;d"
  ]
  node [
    id 19
    label "maksimum"
  ]
  node [
    id 20
    label "genitalia"
  ]
  node [
    id 21
    label "warkocz"
  ]
  node [
    id 22
    label "motyl"
  ]
  node [
    id 23
    label "zwie&#324;czenie"
  ]
  node [
    id 24
    label "drzewo"
  ]
  node [
    id 25
    label "bryd&#380;"
  ]
  node [
    id 26
    label "zesp&#243;&#322;"
  ]
  node [
    id 27
    label "przepaska"
  ]
  node [
    id 28
    label "moneta"
  ]
  node [
    id 29
    label "jednostka_monetarna"
  ]
  node [
    id 30
    label "uk&#322;ad"
  ]
  node [
    id 31
    label "corona"
  ]
  node [
    id 32
    label "kwiat"
  ]
  node [
    id 33
    label "regalia"
  ]
  node [
    id 34
    label "kok"
  ]
  node [
    id 35
    label "Crown"
  ]
  node [
    id 36
    label "pa&#324;stwo"
  ]
  node [
    id 37
    label "geofit"
  ]
  node [
    id 38
    label "ro&#347;lino&#380;erca"
  ]
  node [
    id 39
    label "Otton_III"
  ]
  node [
    id 40
    label "&#322;o&#380;yskowiec"
  ]
  node [
    id 41
    label "monarchista"
  ]
  node [
    id 42
    label "Syzyf"
  ]
  node [
    id 43
    label "turzyca"
  ]
  node [
    id 44
    label "Zygmunt_III_Waza"
  ]
  node [
    id 45
    label "Fryderyk_II_Wielki"
  ]
  node [
    id 46
    label "Aleksander_Wielki"
  ]
  node [
    id 47
    label "monarcha"
  ]
  node [
    id 48
    label "gruba_ryba"
  ]
  node [
    id 49
    label "August_III_Sas"
  ]
  node [
    id 50
    label "koronowa&#263;"
  ]
  node [
    id 51
    label "tytu&#322;"
  ]
  node [
    id 52
    label "Boles&#322;aw_Chrobry"
  ]
  node [
    id 53
    label "zwierz&#281;_futerkowe"
  ]
  node [
    id 54
    label "basileus"
  ]
  node [
    id 55
    label "Zygmunt_II_August"
  ]
  node [
    id 56
    label "Jan_Kazimierz"
  ]
  node [
    id 57
    label "Manuel_I_Szcz&#281;&#347;liwy"
  ]
  node [
    id 58
    label "Ludwik_XVI"
  ]
  node [
    id 59
    label "trusia"
  ]
  node [
    id 60
    label "Tantal"
  ]
  node [
    id 61
    label "HP"
  ]
  node [
    id 62
    label "Edward_VII"
  ]
  node [
    id 63
    label "Jugurta"
  ]
  node [
    id 64
    label "Herod"
  ]
  node [
    id 65
    label "koronowanie"
  ]
  node [
    id 66
    label "omyk"
  ]
  node [
    id 67
    label "Augiasz"
  ]
  node [
    id 68
    label "Stanis&#322;aw_August_Poniatowski"
  ]
  node [
    id 69
    label "zaj&#261;cowate"
  ]
  node [
    id 70
    label "Salomon"
  ]
  node [
    id 71
    label "Karol_Albert"
  ]
  node [
    id 72
    label "figura_karciana"
  ]
  node [
    id 73
    label "baron"
  ]
  node [
    id 74
    label "figura"
  ]
  node [
    id 75
    label "Artur"
  ]
  node [
    id 76
    label "kicaj"
  ]
  node [
    id 77
    label "Dawid"
  ]
  node [
    id 78
    label "kr&#243;lowa_matka"
  ]
  node [
    id 79
    label "Zygmunt_I_Stary"
  ]
  node [
    id 80
    label "Henryk_IV"
  ]
  node [
    id 81
    label "Kazimierz_Wielki"
  ]
  node [
    id 82
    label "Boles&#322;aw"
  ]
  node [
    id 83
    label "chrobry"
  ]
  node [
    id 84
    label "Zygmunt"
  ]
  node [
    id 85
    label "III"
  ]
  node [
    id 86
    label "Waza"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 82
    target 83
  ]
  edge [
    source 84
    target 85
  ]
  edge [
    source 84
    target 86
  ]
  edge [
    source 85
    target 86
  ]
]
