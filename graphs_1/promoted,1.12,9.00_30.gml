graph [
  maxDegree 46
  minDegree 1
  meanDegree 2
  density 0.00966183574879227
  graphCliqueNumber 2
  node [
    id 0
    label "odk&#261;d"
    origin "text"
  ]
  node [
    id 1
    label "og&#322;osi&#263;"
    origin "text"
  ]
  node [
    id 2
    label "robert"
    origin "text"
  ]
  node [
    id 3
    label "kubica"
    origin "text"
  ]
  node [
    id 4
    label "wraca&#263;"
    origin "text"
  ]
  node [
    id 5
    label "formu&#322;a"
    origin "text"
  ]
  node [
    id 6
    label "wszyscy"
    origin "text"
  ]
  node [
    id 7
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 8
    label "historia"
    origin "text"
  ]
  node [
    id 9
    label "sportowiec"
    origin "text"
  ]
  node [
    id 10
    label "by&#263;"
    origin "text"
  ]
  node [
    id 11
    label "&#347;wietny"
    origin "text"
  ]
  node [
    id 12
    label "materia&#322;"
    origin "text"
  ]
  node [
    id 13
    label "stworzenie"
    origin "text"
  ]
  node [
    id 14
    label "film"
    origin "text"
  ]
  node [
    id 15
    label "communicate"
  ]
  node [
    id 16
    label "opublikowa&#263;"
  ]
  node [
    id 17
    label "obwo&#322;a&#263;"
  ]
  node [
    id 18
    label "poda&#263;"
  ]
  node [
    id 19
    label "publish"
  ]
  node [
    id 20
    label "declare"
  ]
  node [
    id 21
    label "zostawa&#263;"
  ]
  node [
    id 22
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 23
    label "return"
  ]
  node [
    id 24
    label "godzi&#263;_si&#281;"
  ]
  node [
    id 25
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 26
    label "przybywa&#263;"
  ]
  node [
    id 27
    label "zjawia&#263;_si&#281;"
  ]
  node [
    id 28
    label "przychodzi&#263;"
  ]
  node [
    id 29
    label "zaczyna&#263;"
  ]
  node [
    id 30
    label "tax_return"
  ]
  node [
    id 31
    label "nawi&#261;zywa&#263;"
  ]
  node [
    id 32
    label "recur"
  ]
  node [
    id 33
    label "powodowa&#263;"
  ]
  node [
    id 34
    label "rule"
  ]
  node [
    id 35
    label "obyczajowo&#347;&#263;"
  ]
  node [
    id 36
    label "zapis"
  ]
  node [
    id 37
    label "formularz"
  ]
  node [
    id 38
    label "sformu&#322;owanie"
  ]
  node [
    id 39
    label "kultura"
  ]
  node [
    id 40
    label "kultura_duchowa"
  ]
  node [
    id 41
    label "ceremony"
  ]
  node [
    id 42
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 43
    label "remark"
  ]
  node [
    id 44
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 45
    label "u&#380;ywa&#263;"
  ]
  node [
    id 46
    label "okre&#347;la&#263;"
  ]
  node [
    id 47
    label "j&#281;zyk"
  ]
  node [
    id 48
    label "say"
  ]
  node [
    id 49
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 50
    label "formu&#322;owa&#263;"
  ]
  node [
    id 51
    label "talk"
  ]
  node [
    id 52
    label "powiada&#263;"
  ]
  node [
    id 53
    label "informowa&#263;"
  ]
  node [
    id 54
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 55
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 56
    label "wydobywa&#263;"
  ]
  node [
    id 57
    label "express"
  ]
  node [
    id 58
    label "chew_the_fat"
  ]
  node [
    id 59
    label "dysfonia"
  ]
  node [
    id 60
    label "umie&#263;"
  ]
  node [
    id 61
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 62
    label "tell"
  ]
  node [
    id 63
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 64
    label "wyra&#380;a&#263;"
  ]
  node [
    id 65
    label "gaworzy&#263;"
  ]
  node [
    id 66
    label "rozmawia&#263;"
  ]
  node [
    id 67
    label "dziama&#263;"
  ]
  node [
    id 68
    label "prawi&#263;"
  ]
  node [
    id 69
    label "report"
  ]
  node [
    id 70
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 71
    label "wypowied&#378;"
  ]
  node [
    id 72
    label "neografia"
  ]
  node [
    id 73
    label "przedmiot"
  ]
  node [
    id 74
    label "papirologia"
  ]
  node [
    id 75
    label "historia_gospodarcza"
  ]
  node [
    id 76
    label "przebiec"
  ]
  node [
    id 77
    label "hista"
  ]
  node [
    id 78
    label "nauka_humanistyczna"
  ]
  node [
    id 79
    label "filigranistyka"
  ]
  node [
    id 80
    label "dyplomatyka"
  ]
  node [
    id 81
    label "annalistyka"
  ]
  node [
    id 82
    label "historyka"
  ]
  node [
    id 83
    label "heraldyka"
  ]
  node [
    id 84
    label "fabu&#322;a"
  ]
  node [
    id 85
    label "muzealnictwo"
  ]
  node [
    id 86
    label "varsavianistyka"
  ]
  node [
    id 87
    label "mediewistyka"
  ]
  node [
    id 88
    label "prezentyzm"
  ]
  node [
    id 89
    label "przebiegni&#281;cie"
  ]
  node [
    id 90
    label "charakter"
  ]
  node [
    id 91
    label "paleografia"
  ]
  node [
    id 92
    label "genealogia"
  ]
  node [
    id 93
    label "czynno&#347;&#263;"
  ]
  node [
    id 94
    label "prozopografia"
  ]
  node [
    id 95
    label "motyw"
  ]
  node [
    id 96
    label "nautologia"
  ]
  node [
    id 97
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 98
    label "epoka"
  ]
  node [
    id 99
    label "numizmatyka"
  ]
  node [
    id 100
    label "ruralistyka"
  ]
  node [
    id 101
    label "&#378;r&#243;d&#322;oznawstwo"
  ]
  node [
    id 102
    label "epigrafika"
  ]
  node [
    id 103
    label "historiografia"
  ]
  node [
    id 104
    label "bizantynistyka"
  ]
  node [
    id 105
    label "weksylologia"
  ]
  node [
    id 106
    label "kierunek"
  ]
  node [
    id 107
    label "ikonografia"
  ]
  node [
    id 108
    label "chronologia"
  ]
  node [
    id 109
    label "archiwistyka"
  ]
  node [
    id 110
    label "sfragistyka"
  ]
  node [
    id 111
    label "zabytkoznawstwo"
  ]
  node [
    id 112
    label "historia_sztuki"
  ]
  node [
    id 113
    label "cz&#322;owiek"
  ]
  node [
    id 114
    label "zgrupowanie"
  ]
  node [
    id 115
    label "si&#281;ga&#263;"
  ]
  node [
    id 116
    label "trwa&#263;"
  ]
  node [
    id 117
    label "obecno&#347;&#263;"
  ]
  node [
    id 118
    label "stan"
  ]
  node [
    id 119
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 120
    label "stand"
  ]
  node [
    id 121
    label "mie&#263;_miejsce"
  ]
  node [
    id 122
    label "uczestniczy&#263;"
  ]
  node [
    id 123
    label "chodzi&#263;"
  ]
  node [
    id 124
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 125
    label "equal"
  ]
  node [
    id 126
    label "pomy&#347;lny"
  ]
  node [
    id 127
    label "pozytywny"
  ]
  node [
    id 128
    label "wspaniale"
  ]
  node [
    id 129
    label "dobry"
  ]
  node [
    id 130
    label "superancki"
  ]
  node [
    id 131
    label "arcydzielny"
  ]
  node [
    id 132
    label "zajebisty"
  ]
  node [
    id 133
    label "wa&#380;ny"
  ]
  node [
    id 134
    label "&#347;wietnie"
  ]
  node [
    id 135
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 136
    label "skuteczny"
  ]
  node [
    id 137
    label "spania&#322;y"
  ]
  node [
    id 138
    label "krajka"
  ]
  node [
    id 139
    label "tworzywo"
  ]
  node [
    id 140
    label "krajalno&#347;&#263;"
  ]
  node [
    id 141
    label "archiwum"
  ]
  node [
    id 142
    label "kandydat"
  ]
  node [
    id 143
    label "bielarnia"
  ]
  node [
    id 144
    label "rozrywa&#263;_si&#281;"
  ]
  node [
    id 145
    label "dane"
  ]
  node [
    id 146
    label "materia"
  ]
  node [
    id 147
    label "wyr&#243;b_w&#322;&#243;kienniczy"
  ]
  node [
    id 148
    label "substancja"
  ]
  node [
    id 149
    label "nawil&#380;arka"
  ]
  node [
    id 150
    label "dyspozycja"
  ]
  node [
    id 151
    label "obiekt_naturalny"
  ]
  node [
    id 152
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 153
    label "stw&#243;r"
  ]
  node [
    id 154
    label "rzecz"
  ]
  node [
    id 155
    label "environment"
  ]
  node [
    id 156
    label "erecting"
  ]
  node [
    id 157
    label "biota"
  ]
  node [
    id 158
    label "wszechstworzenie"
  ]
  node [
    id 159
    label "fauna"
  ]
  node [
    id 160
    label "ekosystem"
  ]
  node [
    id 161
    label "teren"
  ]
  node [
    id 162
    label "powstanie"
  ]
  node [
    id 163
    label "mikrokosmos"
  ]
  node [
    id 164
    label "zrobienie"
  ]
  node [
    id 165
    label "pope&#322;nienie"
  ]
  node [
    id 166
    label "work"
  ]
  node [
    id 167
    label "wizerunek"
  ]
  node [
    id 168
    label "organizm"
  ]
  node [
    id 169
    label "cia&#322;o"
  ]
  node [
    id 170
    label "istota"
  ]
  node [
    id 171
    label "Ziemia"
  ]
  node [
    id 172
    label "potworzenie"
  ]
  node [
    id 173
    label "woda"
  ]
  node [
    id 174
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 175
    label "rozbieg&#243;wka"
  ]
  node [
    id 176
    label "block"
  ]
  node [
    id 177
    label "blik"
  ]
  node [
    id 178
    label "odczula&#263;"
  ]
  node [
    id 179
    label "rola"
  ]
  node [
    id 180
    label "trawiarnia"
  ]
  node [
    id 181
    label "b&#322;ona"
  ]
  node [
    id 182
    label "filmoteka"
  ]
  node [
    id 183
    label "sztuka"
  ]
  node [
    id 184
    label "muza"
  ]
  node [
    id 185
    label "odczuli&#263;"
  ]
  node [
    id 186
    label "klatka"
  ]
  node [
    id 187
    label "odczulenie"
  ]
  node [
    id 188
    label "emulsja_fotograficzna"
  ]
  node [
    id 189
    label "animatronika"
  ]
  node [
    id 190
    label "dorobek"
  ]
  node [
    id 191
    label "odczulanie"
  ]
  node [
    id 192
    label "scena"
  ]
  node [
    id 193
    label "czo&#322;&#243;wka"
  ]
  node [
    id 194
    label "ty&#322;&#243;wka"
  ]
  node [
    id 195
    label "napisy"
  ]
  node [
    id 196
    label "photograph"
  ]
  node [
    id 197
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 198
    label "postprodukcja"
  ]
  node [
    id 199
    label "sklejarka"
  ]
  node [
    id 200
    label "anamorfoza"
  ]
  node [
    id 201
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 202
    label "ta&#347;ma"
  ]
  node [
    id 203
    label "b&#322;ona_&#347;wiat&#322;oczu&#322;a"
  ]
  node [
    id 204
    label "uj&#281;cie"
  ]
  node [
    id 205
    label "Robert"
  ]
  node [
    id 206
    label "Kubica"
  ]
  node [
    id 207
    label "1"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 10
    target 121
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 10
    target 125
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 126
  ]
  edge [
    source 11
    target 127
  ]
  edge [
    source 11
    target 128
  ]
  edge [
    source 11
    target 129
  ]
  edge [
    source 11
    target 130
  ]
  edge [
    source 11
    target 131
  ]
  edge [
    source 11
    target 132
  ]
  edge [
    source 11
    target 133
  ]
  edge [
    source 11
    target 134
  ]
  edge [
    source 11
    target 135
  ]
  edge [
    source 11
    target 136
  ]
  edge [
    source 11
    target 137
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 138
  ]
  edge [
    source 12
    target 113
  ]
  edge [
    source 12
    target 139
  ]
  edge [
    source 12
    target 140
  ]
  edge [
    source 12
    target 141
  ]
  edge [
    source 12
    target 142
  ]
  edge [
    source 12
    target 143
  ]
  edge [
    source 12
    target 144
  ]
  edge [
    source 12
    target 145
  ]
  edge [
    source 12
    target 146
  ]
  edge [
    source 12
    target 147
  ]
  edge [
    source 12
    target 148
  ]
  edge [
    source 12
    target 149
  ]
  edge [
    source 12
    target 150
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 151
  ]
  edge [
    source 13
    target 152
  ]
  edge [
    source 13
    target 153
  ]
  edge [
    source 13
    target 154
  ]
  edge [
    source 13
    target 155
  ]
  edge [
    source 13
    target 156
  ]
  edge [
    source 13
    target 157
  ]
  edge [
    source 13
    target 158
  ]
  edge [
    source 13
    target 159
  ]
  edge [
    source 13
    target 160
  ]
  edge [
    source 13
    target 161
  ]
  edge [
    source 13
    target 162
  ]
  edge [
    source 13
    target 163
  ]
  edge [
    source 13
    target 164
  ]
  edge [
    source 13
    target 165
  ]
  edge [
    source 13
    target 93
  ]
  edge [
    source 13
    target 166
  ]
  edge [
    source 13
    target 167
  ]
  edge [
    source 13
    target 168
  ]
  edge [
    source 13
    target 169
  ]
  edge [
    source 13
    target 170
  ]
  edge [
    source 13
    target 171
  ]
  edge [
    source 13
    target 172
  ]
  edge [
    source 13
    target 173
  ]
  edge [
    source 13
    target 174
  ]
  edge [
    source 14
    target 175
  ]
  edge [
    source 14
    target 176
  ]
  edge [
    source 14
    target 177
  ]
  edge [
    source 14
    target 178
  ]
  edge [
    source 14
    target 179
  ]
  edge [
    source 14
    target 180
  ]
  edge [
    source 14
    target 181
  ]
  edge [
    source 14
    target 182
  ]
  edge [
    source 14
    target 183
  ]
  edge [
    source 14
    target 184
  ]
  edge [
    source 14
    target 185
  ]
  edge [
    source 14
    target 186
  ]
  edge [
    source 14
    target 187
  ]
  edge [
    source 14
    target 188
  ]
  edge [
    source 14
    target 189
  ]
  edge [
    source 14
    target 190
  ]
  edge [
    source 14
    target 191
  ]
  edge [
    source 14
    target 192
  ]
  edge [
    source 14
    target 193
  ]
  edge [
    source 14
    target 194
  ]
  edge [
    source 14
    target 195
  ]
  edge [
    source 14
    target 196
  ]
  edge [
    source 14
    target 197
  ]
  edge [
    source 14
    target 198
  ]
  edge [
    source 14
    target 199
  ]
  edge [
    source 14
    target 200
  ]
  edge [
    source 14
    target 201
  ]
  edge [
    source 14
    target 202
  ]
  edge [
    source 14
    target 203
  ]
  edge [
    source 14
    target 204
  ]
  edge [
    source 205
    target 206
  ]
]
