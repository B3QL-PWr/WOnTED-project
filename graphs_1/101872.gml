graph [
  maxDegree 17
  minDegree 1
  meanDegree 2.0675675675675675
  density 0.014065085493656922
  graphCliqueNumber 3
  node [
    id 0
    label "negocjacja"
    origin "text"
  ]
  node [
    id 1
    label "burmistrz"
    origin "text"
  ]
  node [
    id 2
    label "ozorkowy"
    origin "text"
  ]
  node [
    id 3
    label "&#322;&#243;dzkie"
    origin "text"
  ]
  node [
    id 4
    label "zak&#322;ad"
    origin "text"
  ]
  node [
    id 5
    label "energetyczny"
    origin "text"
  ]
  node [
    id 6
    label "uda&#263;"
    origin "text"
  ]
  node [
    id 7
    label "si&#281;"
    origin "text"
  ]
  node [
    id 8
    label "osi&#261;gn&#261;&#263;"
    origin "text"
  ]
  node [
    id 9
    label "porozumienie"
    origin "text"
  ]
  node [
    id 10
    label "sprawa"
    origin "text"
  ]
  node [
    id 11
    label "uruchomi&#263;"
    origin "text"
  ]
  node [
    id 12
    label "winda"
    origin "text"
  ]
  node [
    id 13
    label "osobowy"
    origin "text"
  ]
  node [
    id 14
    label "budowa&#263;"
    origin "text"
  ]
  node [
    id 15
    label "ozorkowski"
    origin "text"
  ]
  node [
    id 16
    label "magistrat"
    origin "text"
  ]
  node [
    id 17
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 18
    label "brakowa&#263;"
    origin "text"
  ]
  node [
    id 19
    label "&#378;r&#243;d&#322;o"
    origin "text"
  ]
  node [
    id 20
    label "zasilanie"
    origin "text"
  ]
  node [
    id 21
    label "&#322;ze"
    origin "text"
  ]
  node [
    id 22
    label "zgodzi&#263;"
    origin "text"
  ]
  node [
    id 23
    label "wykona&#263;"
    origin "text"
  ]
  node [
    id 24
    label "tymczasowy"
    origin "text"
  ]
  node [
    id 25
    label "przy&#322;&#261;cze"
    origin "text"
  ]
  node [
    id 26
    label "by&#263;"
    origin "text"
  ]
  node [
    id 27
    label "tylko"
    origin "text"
  ]
  node [
    id 28
    label "prowizorka"
    origin "text"
  ]
  node [
    id 29
    label "spos&#243;b"
  ]
  node [
    id 30
    label "ceklarz"
  ]
  node [
    id 31
    label "miasto"
  ]
  node [
    id 32
    label "burmistrzyna"
  ]
  node [
    id 33
    label "samorz&#261;dowiec"
  ]
  node [
    id 34
    label "mi&#281;sny"
  ]
  node [
    id 35
    label "czyn"
  ]
  node [
    id 36
    label "company"
  ]
  node [
    id 37
    label "zak&#322;adka"
  ]
  node [
    id 38
    label "firma"
  ]
  node [
    id 39
    label "instytut"
  ]
  node [
    id 40
    label "wyko&#324;czenie"
  ]
  node [
    id 41
    label "jednostka_organizacyjna"
  ]
  node [
    id 42
    label "umowa"
  ]
  node [
    id 43
    label "instytucja"
  ]
  node [
    id 44
    label "miejsce_pracy"
  ]
  node [
    id 45
    label "od&#380;ywczy"
  ]
  node [
    id 46
    label "energetycznie"
  ]
  node [
    id 47
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 48
    label "represent"
  ]
  node [
    id 49
    label "profit"
  ]
  node [
    id 50
    label "score"
  ]
  node [
    id 51
    label "dotrze&#263;"
  ]
  node [
    id 52
    label "uzyska&#263;"
  ]
  node [
    id 53
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 54
    label "make"
  ]
  node [
    id 55
    label "zgoda"
  ]
  node [
    id 56
    label "uk&#322;ad_dayto&#324;ski"
  ]
  node [
    id 57
    label "rozstrzygni&#281;cie"
  ]
  node [
    id 58
    label "agent"
  ]
  node [
    id 59
    label "communication"
  ]
  node [
    id 60
    label "z&#322;oty_blok"
  ]
  node [
    id 61
    label "temat"
  ]
  node [
    id 62
    label "kognicja"
  ]
  node [
    id 63
    label "idea"
  ]
  node [
    id 64
    label "szczeg&#243;&#322;"
  ]
  node [
    id 65
    label "rzecz"
  ]
  node [
    id 66
    label "wydarzenie"
  ]
  node [
    id 67
    label "przes&#322;anka"
  ]
  node [
    id 68
    label "rozprawa"
  ]
  node [
    id 69
    label "object"
  ]
  node [
    id 70
    label "proposition"
  ]
  node [
    id 71
    label "trip"
  ]
  node [
    id 72
    label "zacz&#261;&#263;"
  ]
  node [
    id 73
    label "spowodowa&#263;"
  ]
  node [
    id 74
    label "begin"
  ]
  node [
    id 75
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 76
    label "d&#378;wig"
  ]
  node [
    id 77
    label "szyb_windowy"
  ]
  node [
    id 78
    label "kabina"
  ]
  node [
    id 79
    label "lift"
  ]
  node [
    id 80
    label "osobowo"
  ]
  node [
    id 81
    label "planowa&#263;"
  ]
  node [
    id 82
    label "dodawa&#263;_otuchy"
  ]
  node [
    id 83
    label "consist"
  ]
  node [
    id 84
    label "train"
  ]
  node [
    id 85
    label "tworzy&#263;"
  ]
  node [
    id 86
    label "wytwarza&#263;"
  ]
  node [
    id 87
    label "raise"
  ]
  node [
    id 88
    label "stanowi&#263;"
  ]
  node [
    id 89
    label "rynek"
  ]
  node [
    id 90
    label "plac_ratuszowy"
  ]
  node [
    id 91
    label "sekretariat"
  ]
  node [
    id 92
    label "urz&#281;dnik"
  ]
  node [
    id 93
    label "urz&#261;d"
  ]
  node [
    id 94
    label "urz&#281;dnik_miejski"
  ]
  node [
    id 95
    label "siedziba"
  ]
  node [
    id 96
    label "zwierzchnik"
  ]
  node [
    id 97
    label "przebiera&#263;"
  ]
  node [
    id 98
    label "lack"
  ]
  node [
    id 99
    label "przegl&#261;da&#263;"
  ]
  node [
    id 100
    label "sprawdza&#263;"
  ]
  node [
    id 101
    label "utylizowa&#263;"
  ]
  node [
    id 102
    label "bra&#263;_si&#281;"
  ]
  node [
    id 103
    label "&#347;wiadectwo"
  ]
  node [
    id 104
    label "przyczyna"
  ]
  node [
    id 105
    label "matuszka"
  ]
  node [
    id 106
    label "geneza"
  ]
  node [
    id 107
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 108
    label "kamena"
  ]
  node [
    id 109
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 110
    label "czynnik"
  ]
  node [
    id 111
    label "pocz&#261;tek"
  ]
  node [
    id 112
    label "poci&#261;ganie"
  ]
  node [
    id 113
    label "rezultat"
  ]
  node [
    id 114
    label "ciek_wodny"
  ]
  node [
    id 115
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 116
    label "subject"
  ]
  node [
    id 117
    label "uk&#322;ad"
  ]
  node [
    id 118
    label "dostarczanie"
  ]
  node [
    id 119
    label "feeding"
  ]
  node [
    id 120
    label "elektryczno&#347;&#263;"
  ]
  node [
    id 121
    label "zatrudni&#263;"
  ]
  node [
    id 122
    label "zgodzenie"
  ]
  node [
    id 123
    label "pomoc"
  ]
  node [
    id 124
    label "zgadza&#263;"
  ]
  node [
    id 125
    label "wytworzy&#263;"
  ]
  node [
    id 126
    label "manufacture"
  ]
  node [
    id 127
    label "zrobi&#263;"
  ]
  node [
    id 128
    label "picture"
  ]
  node [
    id 129
    label "przepustka"
  ]
  node [
    id 130
    label "czasowo"
  ]
  node [
    id 131
    label "z&#322;&#261;czenie"
  ]
  node [
    id 132
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 133
    label "przew&#243;d"
  ]
  node [
    id 134
    label "si&#281;ga&#263;"
  ]
  node [
    id 135
    label "trwa&#263;"
  ]
  node [
    id 136
    label "obecno&#347;&#263;"
  ]
  node [
    id 137
    label "stan"
  ]
  node [
    id 138
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 139
    label "stand"
  ]
  node [
    id 140
    label "mie&#263;_miejsce"
  ]
  node [
    id 141
    label "uczestniczy&#263;"
  ]
  node [
    id 142
    label "chodzi&#263;"
  ]
  node [
    id 143
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 144
    label "equal"
  ]
  node [
    id 145
    label "tymczasowo&#347;&#263;"
  ]
  node [
    id 146
    label "p&#243;&#322;&#347;rodek"
  ]
  node [
    id 147
    label "&#322;&#243;dzki"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 22
  ]
  edge [
    source 7
    target 23
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 55
  ]
  edge [
    source 9
    target 56
  ]
  edge [
    source 9
    target 57
  ]
  edge [
    source 9
    target 42
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 61
  ]
  edge [
    source 10
    target 62
  ]
  edge [
    source 10
    target 63
  ]
  edge [
    source 10
    target 64
  ]
  edge [
    source 10
    target 65
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 10
    target 69
  ]
  edge [
    source 10
    target 70
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 71
  ]
  edge [
    source 11
    target 72
  ]
  edge [
    source 11
    target 73
  ]
  edge [
    source 11
    target 74
  ]
  edge [
    source 11
    target 75
  ]
  edge [
    source 11
    target 53
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 76
  ]
  edge [
    source 12
    target 77
  ]
  edge [
    source 12
    target 78
  ]
  edge [
    source 12
    target 79
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 80
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 81
  ]
  edge [
    source 14
    target 82
  ]
  edge [
    source 14
    target 83
  ]
  edge [
    source 14
    target 84
  ]
  edge [
    source 14
    target 85
  ]
  edge [
    source 14
    target 86
  ]
  edge [
    source 14
    target 87
  ]
  edge [
    source 14
    target 88
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 89
  ]
  edge [
    source 16
    target 90
  ]
  edge [
    source 16
    target 91
  ]
  edge [
    source 16
    target 92
  ]
  edge [
    source 16
    target 93
  ]
  edge [
    source 16
    target 94
  ]
  edge [
    source 16
    target 95
  ]
  edge [
    source 16
    target 96
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 97
  ]
  edge [
    source 18
    target 98
  ]
  edge [
    source 18
    target 99
  ]
  edge [
    source 18
    target 100
  ]
  edge [
    source 18
    target 101
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 102
  ]
  edge [
    source 19
    target 103
  ]
  edge [
    source 19
    target 104
  ]
  edge [
    source 19
    target 105
  ]
  edge [
    source 19
    target 106
  ]
  edge [
    source 19
    target 107
  ]
  edge [
    source 19
    target 108
  ]
  edge [
    source 19
    target 109
  ]
  edge [
    source 19
    target 110
  ]
  edge [
    source 19
    target 111
  ]
  edge [
    source 19
    target 112
  ]
  edge [
    source 19
    target 113
  ]
  edge [
    source 19
    target 114
  ]
  edge [
    source 19
    target 115
  ]
  edge [
    source 19
    target 116
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 117
  ]
  edge [
    source 20
    target 118
  ]
  edge [
    source 20
    target 119
  ]
  edge [
    source 20
    target 120
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 121
  ]
  edge [
    source 22
    target 122
  ]
  edge [
    source 22
    target 123
  ]
  edge [
    source 22
    target 124
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 125
  ]
  edge [
    source 23
    target 126
  ]
  edge [
    source 23
    target 127
  ]
  edge [
    source 23
    target 128
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 129
  ]
  edge [
    source 24
    target 130
  ]
  edge [
    source 25
    target 131
  ]
  edge [
    source 25
    target 132
  ]
  edge [
    source 25
    target 133
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 134
  ]
  edge [
    source 26
    target 135
  ]
  edge [
    source 26
    target 136
  ]
  edge [
    source 26
    target 137
  ]
  edge [
    source 26
    target 138
  ]
  edge [
    source 26
    target 139
  ]
  edge [
    source 26
    target 140
  ]
  edge [
    source 26
    target 141
  ]
  edge [
    source 26
    target 142
  ]
  edge [
    source 26
    target 143
  ]
  edge [
    source 26
    target 144
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 28
    target 145
  ]
  edge [
    source 28
    target 146
  ]
]
