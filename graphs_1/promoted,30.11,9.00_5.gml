graph [
  maxDegree 22
  minDegree 1
  meanDegree 2
  density 0.03125
  graphCliqueNumber 2
  node [
    id 0
    label "drogi"
    origin "text"
  ]
  node [
    id 1
    label "uk&#322;ada&#263;"
    origin "text"
  ]
  node [
    id 2
    label "wafelek"
    origin "text"
  ]
  node [
    id 3
    label "musza"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "chowa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "las"
    origin "text"
  ]
  node [
    id 7
    label "cz&#322;owiek"
  ]
  node [
    id 8
    label "warto&#347;ciowy"
  ]
  node [
    id 9
    label "bliski"
  ]
  node [
    id 10
    label "drogo"
  ]
  node [
    id 11
    label "przyjaciel"
  ]
  node [
    id 12
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 13
    label "mi&#322;y"
  ]
  node [
    id 14
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 15
    label "zbiera&#263;"
  ]
  node [
    id 16
    label "kszta&#322;ci&#263;"
  ]
  node [
    id 17
    label "zmienia&#263;_pozycj&#281;"
  ]
  node [
    id 18
    label "przygotowywa&#263;"
  ]
  node [
    id 19
    label "umieszcza&#263;"
  ]
  node [
    id 20
    label "digest"
  ]
  node [
    id 21
    label "treser"
  ]
  node [
    id 22
    label "uczy&#263;"
  ]
  node [
    id 23
    label "tworzy&#263;"
  ]
  node [
    id 24
    label "porz&#261;dkowa&#263;"
  ]
  node [
    id 25
    label "dispose"
  ]
  node [
    id 26
    label "raise"
  ]
  node [
    id 27
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 28
    label "ciastko"
  ]
  node [
    id 29
    label "wypiek"
  ]
  node [
    id 30
    label "baton"
  ]
  node [
    id 31
    label "wafel"
  ]
  node [
    id 32
    label "continue"
  ]
  node [
    id 33
    label "report"
  ]
  node [
    id 34
    label "ukrywa&#263;"
  ]
  node [
    id 35
    label "wk&#322;ada&#263;"
  ]
  node [
    id 36
    label "czu&#263;"
  ]
  node [
    id 37
    label "meliniarz"
  ]
  node [
    id 38
    label "znosi&#263;"
  ]
  node [
    id 39
    label "przetrzymywa&#263;"
  ]
  node [
    id 40
    label "opiekowa&#263;_si&#281;"
  ]
  node [
    id 41
    label "train"
  ]
  node [
    id 42
    label "hide"
  ]
  node [
    id 43
    label "hodowa&#263;"
  ]
  node [
    id 44
    label "chody"
  ]
  node [
    id 45
    label "dno_lasu"
  ]
  node [
    id 46
    label "obr&#281;b"
  ]
  node [
    id 47
    label "&#347;ci&#243;&#322;ka"
  ]
  node [
    id 48
    label "podszyt"
  ]
  node [
    id 49
    label "rewir"
  ]
  node [
    id 50
    label "podrost"
  ]
  node [
    id 51
    label "teren"
  ]
  node [
    id 52
    label "le&#347;nictwo"
  ]
  node [
    id 53
    label "wykarczowanie"
  ]
  node [
    id 54
    label "runo"
  ]
  node [
    id 55
    label "teren_le&#347;ny"
  ]
  node [
    id 56
    label "wykarczowa&#263;"
  ]
  node [
    id 57
    label "mn&#243;stwo"
  ]
  node [
    id 58
    label "nadle&#347;nictwo"
  ]
  node [
    id 59
    label "formacja_ro&#347;linna"
  ]
  node [
    id 60
    label "zalesienie"
  ]
  node [
    id 61
    label "karczowa&#263;"
  ]
  node [
    id 62
    label "wiatro&#322;om"
  ]
  node [
    id 63
    label "karczowanie"
  ]
  node [
    id 64
    label "driada"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
]
