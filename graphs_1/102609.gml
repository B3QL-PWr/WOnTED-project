graph [
  maxDegree 7
  minDegree 1
  meanDegree 1.6
  density 0.06666666666666667
  graphCliqueNumber 3
  node [
    id 0
    label "wyrok"
    origin "text"
  ]
  node [
    id 1
    label "trybuna&#322;"
    origin "text"
  ]
  node [
    id 2
    label "konstytucyjny"
    origin "text"
  ]
  node [
    id 3
    label "orzeczenie"
  ]
  node [
    id 4
    label "order"
  ]
  node [
    id 5
    label "wydarzenie"
  ]
  node [
    id 6
    label "kara"
  ]
  node [
    id 7
    label "judgment"
  ]
  node [
    id 8
    label "sentencja"
  ]
  node [
    id 9
    label "Europejski_Trybuna&#322;_Praw_Cz&#322;owieka"
  ]
  node [
    id 10
    label "s&#261;d"
  ]
  node [
    id 11
    label "ustawowy"
  ]
  node [
    id 12
    label "konstytucyjnie"
  ]
  node [
    id 13
    label "Krzysztofa"
  ]
  node [
    id 14
    label "Zalecki"
  ]
  node [
    id 15
    label "pierwszy"
  ]
  node [
    id 16
    label "prezes"
  ]
  node [
    id 17
    label "wysoki"
  ]
  node [
    id 18
    label "prokurator"
  ]
  node [
    id 19
    label "generalny"
  ]
  node [
    id 20
    label "prawo"
  ]
  node [
    id 21
    label "lotniczy"
  ]
  node [
    id 22
    label "konstytucja"
  ]
  node [
    id 23
    label "rzeczpospolita"
  ]
  node [
    id 24
    label "polski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 10
    target 17
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 24
  ]
  edge [
    source 23
    target 24
  ]
]
