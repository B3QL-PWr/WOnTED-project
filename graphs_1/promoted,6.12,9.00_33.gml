graph [
  maxDegree 17
  minDegree 1
  meanDegree 1.9384615384615385
  density 0.03028846153846154
  graphCliqueNumber 2
  node [
    id 0
    label "rok"
    origin "text"
  ]
  node [
    id 1
    label "alan"
    origin "text"
  ]
  node [
    id 2
    label "dundes"
    origin "text"
  ]
  node [
    id 3
    label "wyja&#347;ni&#263;"
    origin "text"
  ]
  node [
    id 4
    label "dlaczego"
    origin "text"
  ]
  node [
    id 5
    label "ameryka"
    origin "text"
  ]
  node [
    id 6
    label "zalewa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "by&#263;"
    origin "text"
  ]
  node [
    id 8
    label "dowcip"
    origin "text"
  ]
  node [
    id 9
    label "polak"
    origin "text"
  ]
  node [
    id 10
    label "stulecie"
  ]
  node [
    id 11
    label "kalendarz"
  ]
  node [
    id 12
    label "czas"
  ]
  node [
    id 13
    label "pora_roku"
  ]
  node [
    id 14
    label "cykl_astronomiczny"
  ]
  node [
    id 15
    label "p&#243;&#322;rocze"
  ]
  node [
    id 16
    label "grupa"
  ]
  node [
    id 17
    label "kwarta&#322;"
  ]
  node [
    id 18
    label "kurs"
  ]
  node [
    id 19
    label "jubileusz"
  ]
  node [
    id 20
    label "miesi&#261;c"
  ]
  node [
    id 21
    label "lata"
  ]
  node [
    id 22
    label "martwy_sezon"
  ]
  node [
    id 23
    label "explain"
  ]
  node [
    id 24
    label "przedstawi&#263;"
  ]
  node [
    id 25
    label "poja&#347;ni&#263;"
  ]
  node [
    id 26
    label "clear"
  ]
  node [
    id 27
    label "dawa&#263;"
  ]
  node [
    id 28
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 29
    label "flood"
  ]
  node [
    id 30
    label "wlewa&#263;"
  ]
  node [
    id 31
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 32
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 33
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 34
    label "moczy&#263;"
  ]
  node [
    id 35
    label "k&#322;ama&#263;"
  ]
  node [
    id 36
    label "oblewa&#263;"
  ]
  node [
    id 37
    label "pokrywa&#263;"
  ]
  node [
    id 38
    label "ton&#261;&#263;"
  ]
  node [
    id 39
    label "pour"
  ]
  node [
    id 40
    label "spuszcza&#263;_si&#281;"
  ]
  node [
    id 41
    label "plami&#263;"
  ]
  node [
    id 42
    label "si&#281;ga&#263;"
  ]
  node [
    id 43
    label "trwa&#263;"
  ]
  node [
    id 44
    label "obecno&#347;&#263;"
  ]
  node [
    id 45
    label "stan"
  ]
  node [
    id 46
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 47
    label "stand"
  ]
  node [
    id 48
    label "mie&#263;_miejsce"
  ]
  node [
    id 49
    label "uczestniczy&#263;"
  ]
  node [
    id 50
    label "chodzi&#263;"
  ]
  node [
    id 51
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 52
    label "equal"
  ]
  node [
    id 53
    label "koncept"
  ]
  node [
    id 54
    label "spalenie"
  ]
  node [
    id 55
    label "turn"
  ]
  node [
    id 56
    label "palenie"
  ]
  node [
    id 57
    label "opowiadanie"
  ]
  node [
    id 58
    label "gryps"
  ]
  node [
    id 59
    label "&#380;art"
  ]
  node [
    id 60
    label "anecdote"
  ]
  node [
    id 61
    label "raptularz"
  ]
  node [
    id 62
    label "polski"
  ]
  node [
    id 63
    label "Alan"
  ]
  node [
    id 64
    label "Dundes"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 63
    target 64
  ]
]
