graph [
  maxDegree 22
  minDegree 1
  meanDegree 1.9333333333333333
  density 0.0327683615819209
  graphCliqueNumber 2
  node [
    id 0
    label "kolejny"
    origin "text"
  ]
  node [
    id 1
    label "pr&#243;ba"
    origin "text"
  ]
  node [
    id 2
    label "zamyka&#263;"
    origin "text"
  ]
  node [
    id 3
    label "usta"
    origin "text"
  ]
  node [
    id 4
    label "inny"
  ]
  node [
    id 5
    label "nast&#281;pnie"
  ]
  node [
    id 6
    label "kt&#243;ry&#347;"
  ]
  node [
    id 7
    label "kolejno"
  ]
  node [
    id 8
    label "nastopny"
  ]
  node [
    id 9
    label "usi&#322;owanie"
  ]
  node [
    id 10
    label "pobiera&#263;"
  ]
  node [
    id 11
    label "spotkanie"
  ]
  node [
    id 12
    label "analiza_chemiczna"
  ]
  node [
    id 13
    label "test"
  ]
  node [
    id 14
    label "znak"
  ]
  node [
    id 15
    label "item"
  ]
  node [
    id 16
    label "ilo&#347;&#263;"
  ]
  node [
    id 17
    label "effort"
  ]
  node [
    id 18
    label "czynno&#347;&#263;"
  ]
  node [
    id 19
    label "metal_szlachetny"
  ]
  node [
    id 20
    label "pobranie"
  ]
  node [
    id 21
    label "pobieranie"
  ]
  node [
    id 22
    label "sytuacja"
  ]
  node [
    id 23
    label "do&#347;wiadczenie"
  ]
  node [
    id 24
    label "probiernictwo"
  ]
  node [
    id 25
    label "zbi&#243;r"
  ]
  node [
    id 26
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 27
    label "pobra&#263;"
  ]
  node [
    id 28
    label "rezultat"
  ]
  node [
    id 29
    label "ko&#324;czy&#263;"
  ]
  node [
    id 30
    label "ukrywa&#263;"
  ]
  node [
    id 31
    label "zawiera&#263;"
  ]
  node [
    id 32
    label "sk&#322;ada&#263;"
  ]
  node [
    id 33
    label "suspend"
  ]
  node [
    id 34
    label "unieruchamia&#263;"
  ]
  node [
    id 35
    label "umieszcza&#263;"
  ]
  node [
    id 36
    label "rozwi&#261;zywa&#263;"
  ]
  node [
    id 37
    label "instytucja"
  ]
  node [
    id 38
    label "blokowa&#263;"
  ]
  node [
    id 39
    label "ujmowa&#263;"
  ]
  node [
    id 40
    label "exsert"
  ]
  node [
    id 41
    label "lock"
  ]
  node [
    id 42
    label "warga_dolna"
  ]
  node [
    id 43
    label "ryjek"
  ]
  node [
    id 44
    label "zaci&#261;&#263;"
  ]
  node [
    id 45
    label "ssa&#263;"
  ]
  node [
    id 46
    label "twarz"
  ]
  node [
    id 47
    label "dzi&#243;b"
  ]
  node [
    id 48
    label "&#322;uk_Kupidyna"
  ]
  node [
    id 49
    label "ssanie"
  ]
  node [
    id 50
    label "zaci&#281;cie"
  ]
  node [
    id 51
    label "jadaczka"
  ]
  node [
    id 52
    label "zacinanie"
  ]
  node [
    id 53
    label "organ"
  ]
  node [
    id 54
    label "jama_ustna"
  ]
  node [
    id 55
    label "otw&#243;r_g&#281;bowy"
  ]
  node [
    id 56
    label "warga_g&#243;rna"
  ]
  node [
    id 57
    label "zacina&#263;"
  ]
  node [
    id 58
    label "unia"
  ]
  node [
    id 59
    label "europejski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 58
    target 59
  ]
]
