graph [
  maxDegree 12
  minDegree 1
  meanDegree 3.0625
  density 0.03223684210526316
  graphCliqueNumber 8
  node [
    id 0
    label "dzi&#347;"
    origin "text"
  ]
  node [
    id 1
    label "nowy"
    origin "text"
  ]
  node [
    id 2
    label "wpis"
    origin "text"
  ]
  node [
    id 3
    label "polskiepato"
    origin "text"
  ]
  node [
    id 4
    label "doba"
  ]
  node [
    id 5
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 6
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 7
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 8
    label "cz&#322;owiek"
  ]
  node [
    id 9
    label "nowotny"
  ]
  node [
    id 10
    label "drugi"
  ]
  node [
    id 11
    label "kolejny"
  ]
  node [
    id 12
    label "bie&#380;&#261;cy"
  ]
  node [
    id 13
    label "nowo"
  ]
  node [
    id 14
    label "narybek"
  ]
  node [
    id 15
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 16
    label "obcy"
  ]
  node [
    id 17
    label "czynno&#347;&#263;"
  ]
  node [
    id 18
    label "entrance"
  ]
  node [
    id 19
    label "inscription"
  ]
  node [
    id 20
    label "akt"
  ]
  node [
    id 21
    label "op&#322;ata"
  ]
  node [
    id 22
    label "tekst"
  ]
  node [
    id 23
    label "Iwon"
  ]
  node [
    id 24
    label "cygan"
  ]
  node [
    id 25
    label "d&#261;browa"
  ]
  node [
    id 26
    label "tarnowski"
  ]
  node [
    id 27
    label "Renat"
  ]
  node [
    id 28
    label "gram"
  ]
  node [
    id 29
    label "Robert"
  ]
  node [
    id 30
    label "k"
  ]
  node [
    id 31
    label "postscriptum"
  ]
  node [
    id 32
    label "zajazd"
  ]
  node [
    id 33
    label "le&#347;ny"
  ]
  node [
    id 34
    label "u"
  ]
  node [
    id 35
    label "trabant"
  ]
  node [
    id 36
    label "Pawe&#322;"
  ]
  node [
    id 37
    label "m&#322;ody"
  ]
  node [
    id 38
    label "klapa"
  ]
  node [
    id 39
    label "&#321;&#281;ka"
  ]
  node [
    id 40
    label "szczuci&#324;ski"
  ]
  node [
    id 41
    label "Tadeusz"
  ]
  node [
    id 42
    label "drab"
  ]
  node [
    id 43
    label "Tadek"
  ]
  node [
    id 44
    label "Iwona"
  ]
  node [
    id 45
    label "darek"
  ]
  node [
    id 46
    label "marek"
  ]
  node [
    id 47
    label "kapela"
  ]
  node [
    id 48
    label "prokuratura"
  ]
  node [
    id 49
    label "rejonowy"
  ]
  node [
    id 50
    label "w"
  ]
  node [
    id 51
    label "archiwum"
  ]
  node [
    id 52
    label "X"
  ]
  node [
    id 53
    label "m&#322;oda"
  ]
  node [
    id 54
    label "Szczucinie"
  ]
  node [
    id 55
    label "dwa"
  ]
  node [
    id 56
    label "dzie&#324;"
  ]
  node [
    id 57
    label "po"
  ]
  node [
    id 58
    label "&#347;mier&#263;"
  ]
  node [
    id 59
    label "&#380;andarmeria"
  ]
  node [
    id 60
    label "wojskowy"
  ]
  node [
    id 61
    label "krakowski"
  ]
  node [
    id 62
    label "Wojciech"
  ]
  node [
    id 63
    label "so&#322;tys"
  ]
  node [
    id 64
    label "Stany"
  ]
  node [
    id 65
    label "zjednoczy&#263;"
  ]
  node [
    id 66
    label "ma&#322;opolski"
  ]
  node [
    id 67
    label "wydzia&#322;"
  ]
  node [
    id 68
    label "zamiejscowy"
  ]
  node [
    id 69
    label "departament"
  ]
  node [
    id 70
    label "do"
  ]
  node [
    id 71
    label "sprawa"
  ]
  node [
    id 72
    label "przest&#281;pczo&#347;&#263;"
  ]
  node [
    id 73
    label "zorganizowa&#263;"
  ]
  node [
    id 74
    label "korupcja"
  ]
  node [
    id 75
    label "krajowy"
  ]
  node [
    id 76
    label "Ma&#322;opolska"
  ]
  node [
    id 77
    label "komenda"
  ]
  node [
    id 78
    label "wojew&#243;dzki"
  ]
  node [
    id 79
    label "policja"
  ]
  node [
    id 80
    label "europejski"
  ]
  node [
    id 81
    label "nakaz"
  ]
  node [
    id 82
    label "aresztowanie"
  ]
  node [
    id 83
    label "zak&#322;ad"
  ]
  node [
    id 84
    label "medycyna"
  ]
  node [
    id 85
    label "s&#261;dowy"
  ]
  node [
    id 86
    label "s&#261;d"
  ]
  node [
    id 87
    label "okr&#281;gowy"
  ]
  node [
    id 88
    label "Tarnowo"
  ]
  node [
    id 89
    label "J&#243;zef"
  ]
  node [
    id 90
    label "Star"
  ]
  node [
    id 91
    label "areszt"
  ]
  node [
    id 92
    label "&#347;ledczy"
  ]
  node [
    id 93
    label "Ewa"
  ]
  node [
    id 94
    label "kopacz"
  ]
  node [
    id 95
    label "m&#322;ode"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 44
  ]
  edge [
    source 24
    target 45
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 48
  ]
  edge [
    source 25
    target 49
  ]
  edge [
    source 25
    target 50
  ]
  edge [
    source 26
    target 48
  ]
  edge [
    source 26
    target 49
  ]
  edge [
    source 26
    target 50
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 31
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 36
  ]
  edge [
    source 30
    target 37
  ]
  edge [
    source 30
    target 38
  ]
  edge [
    source 30
    target 89
  ]
  edge [
    source 30
    target 90
  ]
  edge [
    source 31
    target 36
  ]
  edge [
    source 31
    target 37
  ]
  edge [
    source 31
    target 38
  ]
  edge [
    source 31
    target 89
  ]
  edge [
    source 31
    target 90
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 38
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 38
    target 53
  ]
  edge [
    source 38
    target 89
  ]
  edge [
    source 38
    target 90
  ]
  edge [
    source 38
    target 95
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 44
    target 54
  ]
  edge [
    source 44
    target 55
  ]
  edge [
    source 44
    target 56
  ]
  edge [
    source 44
    target 57
  ]
  edge [
    source 44
    target 58
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 50
  ]
  edge [
    source 48
    target 74
  ]
  edge [
    source 48
    target 75
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 50
    target 86
  ]
  edge [
    source 50
    target 87
  ]
  edge [
    source 50
    target 88
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 61
  ]
  edge [
    source 52
    target 61
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 56
  ]
  edge [
    source 54
    target 57
  ]
  edge [
    source 54
    target 58
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 57
  ]
  edge [
    source 55
    target 58
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 58
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 70
  ]
  edge [
    source 59
    target 91
  ]
  edge [
    source 59
    target 92
  ]
  edge [
    source 60
    target 70
  ]
  edge [
    source 60
    target 91
  ]
  edge [
    source 60
    target 92
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 66
    target 68
  ]
  edge [
    source 66
    target 69
  ]
  edge [
    source 66
    target 70
  ]
  edge [
    source 66
    target 71
  ]
  edge [
    source 66
    target 72
  ]
  edge [
    source 66
    target 73
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 67
    target 69
  ]
  edge [
    source 67
    target 70
  ]
  edge [
    source 67
    target 71
  ]
  edge [
    source 67
    target 72
  ]
  edge [
    source 67
    target 73
  ]
  edge [
    source 67
    target 76
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 68
    target 70
  ]
  edge [
    source 68
    target 71
  ]
  edge [
    source 68
    target 72
  ]
  edge [
    source 68
    target 73
  ]
  edge [
    source 68
    target 76
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 69
    target 71
  ]
  edge [
    source 69
    target 72
  ]
  edge [
    source 69
    target 73
  ]
  edge [
    source 69
    target 76
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 72
  ]
  edge [
    source 70
    target 73
  ]
  edge [
    source 70
    target 76
  ]
  edge [
    source 70
    target 91
  ]
  edge [
    source 70
    target 92
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 71
    target 73
  ]
  edge [
    source 71
    target 76
  ]
  edge [
    source 72
    target 73
  ]
  edge [
    source 72
    target 76
  ]
  edge [
    source 73
    target 76
  ]
  edge [
    source 74
    target 75
  ]
  edge [
    source 77
    target 78
  ]
  edge [
    source 77
    target 79
  ]
  edge [
    source 78
    target 79
  ]
  edge [
    source 80
    target 81
  ]
  edge [
    source 80
    target 82
  ]
  edge [
    source 81
    target 82
  ]
  edge [
    source 83
    target 84
  ]
  edge [
    source 83
    target 85
  ]
  edge [
    source 84
    target 85
  ]
  edge [
    source 86
    target 87
  ]
  edge [
    source 86
    target 88
  ]
  edge [
    source 87
    target 88
  ]
  edge [
    source 89
    target 90
  ]
  edge [
    source 91
    target 92
  ]
  edge [
    source 93
    target 94
  ]
]
