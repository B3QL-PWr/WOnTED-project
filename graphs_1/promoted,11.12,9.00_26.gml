graph [
  maxDegree 26
  minDegree 1
  meanDegree 1.9298245614035088
  density 0.03446115288220551
  graphCliqueNumber 2
  node [
    id 0
    label "nasa"
    origin "text"
  ]
  node [
    id 1
    label "przedstawia&#263;"
    origin "text"
  ]
  node [
    id 2
    label "pierwsza"
    origin "text"
  ]
  node [
    id 3
    label "nagranie"
    origin "text"
  ]
  node [
    id 4
    label "d&#378;wi&#281;k"
    origin "text"
  ]
  node [
    id 5
    label "mars"
    origin "text"
  ]
  node [
    id 6
    label "przedstawienie"
  ]
  node [
    id 7
    label "pokazywa&#263;"
  ]
  node [
    id 8
    label "zapoznawa&#263;"
  ]
  node [
    id 9
    label "typify"
  ]
  node [
    id 10
    label "opisywa&#263;"
  ]
  node [
    id 11
    label "teatr"
  ]
  node [
    id 12
    label "podawa&#263;"
  ]
  node [
    id 13
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 14
    label "demonstrowa&#263;"
  ]
  node [
    id 15
    label "represent"
  ]
  node [
    id 16
    label "ukazywa&#263;"
  ]
  node [
    id 17
    label "attest"
  ]
  node [
    id 18
    label "exhibit"
  ]
  node [
    id 19
    label "stanowi&#263;"
  ]
  node [
    id 20
    label "zg&#322;asza&#263;"
  ]
  node [
    id 21
    label "display"
  ]
  node [
    id 22
    label "godzina"
  ]
  node [
    id 23
    label "wys&#322;uchanie"
  ]
  node [
    id 24
    label "wytw&#243;r"
  ]
  node [
    id 25
    label "wys&#322;ucha&#263;"
  ]
  node [
    id 26
    label "recording"
  ]
  node [
    id 27
    label "utrwalenie"
  ]
  node [
    id 28
    label "seria"
  ]
  node [
    id 29
    label "dobiec"
  ]
  node [
    id 30
    label "wpa&#347;&#263;"
  ]
  node [
    id 31
    label "heksachord"
  ]
  node [
    id 32
    label "note"
  ]
  node [
    id 33
    label "wydawa&#263;"
  ]
  node [
    id 34
    label "intonacja"
  ]
  node [
    id 35
    label "akcent"
  ]
  node [
    id 36
    label "chromatyczno&#347;&#263;"
  ]
  node [
    id 37
    label "nadlecenie"
  ]
  node [
    id 38
    label "wpada&#263;"
  ]
  node [
    id 39
    label "zjawisko"
  ]
  node [
    id 40
    label "brzmienie"
  ]
  node [
    id 41
    label "wydanie"
  ]
  node [
    id 42
    label "wyda&#263;"
  ]
  node [
    id 43
    label "transmiter"
  ]
  node [
    id 44
    label "modalizm"
  ]
  node [
    id 45
    label "phone"
  ]
  node [
    id 46
    label "wpadni&#281;cie"
  ]
  node [
    id 47
    label "wpadanie"
  ]
  node [
    id 48
    label "onomatopeja"
  ]
  node [
    id 49
    label "sound"
  ]
  node [
    id 50
    label "repetycja"
  ]
  node [
    id 51
    label "solmizacja"
  ]
  node [
    id 52
    label "maszt"
  ]
  node [
    id 53
    label "mina"
  ]
  node [
    id 54
    label "platforma"
  ]
  node [
    id 55
    label "czerwona"
  ]
  node [
    id 56
    label "planet"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 55
    target 56
  ]
]
