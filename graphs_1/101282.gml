graph [
  maxDegree 20
  minDegree 1
  meanDegree 2
  density 0.037037037037037035
  graphCliqueNumber 3
  node [
    id 0
    label "zapomnie&#263;"
    origin "text"
  ]
  node [
    id 1
    label "nigdy"
    origin "text"
  ]
  node [
    id 2
    label "album"
    origin "text"
  ]
  node [
    id 3
    label "zesp&#243;&#322;"
    origin "text"
  ]
  node [
    id 4
    label "big"
    origin "text"
  ]
  node [
    id 5
    label "cyc"
    origin "text"
  ]
  node [
    id 6
    label "porzuci&#263;"
  ]
  node [
    id 7
    label "opu&#347;ci&#263;"
  ]
  node [
    id 8
    label "pozostawi&#263;"
  ]
  node [
    id 9
    label "screw"
  ]
  node [
    id 10
    label "przesta&#263;"
  ]
  node [
    id 11
    label "fuck"
  ]
  node [
    id 12
    label "straci&#263;"
  ]
  node [
    id 13
    label "zdolno&#347;&#263;"
  ]
  node [
    id 14
    label "wybaczy&#263;"
  ]
  node [
    id 15
    label "zrobi&#263;"
  ]
  node [
    id 16
    label "zabaczy&#263;"
  ]
  node [
    id 17
    label "kompletnie"
  ]
  node [
    id 18
    label "sketchbook"
  ]
  node [
    id 19
    label "studiowa&#263;"
  ]
  node [
    id 20
    label "pami&#281;tnik"
  ]
  node [
    id 21
    label "stamp_album"
  ]
  node [
    id 22
    label "p&#322;yta"
  ]
  node [
    id 23
    label "wydawnictwo"
  ]
  node [
    id 24
    label "kolekcja"
  ]
  node [
    id 25
    label "blok"
  ]
  node [
    id 26
    label "etui"
  ]
  node [
    id 27
    label "ksi&#281;ga"
  ]
  node [
    id 28
    label "indeks"
  ]
  node [
    id 29
    label "egzemplarz"
  ]
  node [
    id 30
    label "szkic"
  ]
  node [
    id 31
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 32
    label "whole"
  ]
  node [
    id 33
    label "odm&#322;adza&#263;"
  ]
  node [
    id 34
    label "zabudowania"
  ]
  node [
    id 35
    label "odm&#322;odzenie"
  ]
  node [
    id 36
    label "zespolik"
  ]
  node [
    id 37
    label "skupienie"
  ]
  node [
    id 38
    label "schorzenie"
  ]
  node [
    id 39
    label "grupa"
  ]
  node [
    id 40
    label "Depeche_Mode"
  ]
  node [
    id 41
    label "Mazowsze"
  ]
  node [
    id 42
    label "ro&#347;lina"
  ]
  node [
    id 43
    label "zbi&#243;r"
  ]
  node [
    id 44
    label "The_Beatles"
  ]
  node [
    id 45
    label "group"
  ]
  node [
    id 46
    label "&#346;wietliki"
  ]
  node [
    id 47
    label "odm&#322;adzanie"
  ]
  node [
    id 48
    label "batch"
  ]
  node [
    id 49
    label "biust"
  ]
  node [
    id 50
    label "bawe&#322;na"
  ]
  node [
    id 51
    label "pier&#347;"
  ]
  node [
    id 52
    label "tkanina"
  ]
  node [
    id 53
    label "on"
  ]
  node [
    id 54
    label "biga"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 54
  ]
]
