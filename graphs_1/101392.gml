graph [
  maxDegree 25
  minDegree 1
  meanDegree 1.935483870967742
  density 0.06451612903225806
  graphCliqueNumber 2
  node [
    id 0
    label "potok"
    origin "text"
  ]
  node [
    id 1
    label "funkcja"
    origin "text"
  ]
  node [
    id 2
    label "pow&#322;ok"
    origin "text"
  ]
  node [
    id 3
    label "fala"
  ]
  node [
    id 4
    label "flood"
  ]
  node [
    id 5
    label "ruch"
  ]
  node [
    id 6
    label "mn&#243;stwo"
  ]
  node [
    id 7
    label "ciek_wodny"
  ]
  node [
    id 8
    label "infimum"
  ]
  node [
    id 9
    label "znaczenie"
  ]
  node [
    id 10
    label "awansowanie"
  ]
  node [
    id 11
    label "zastosowanie"
  ]
  node [
    id 12
    label "function"
  ]
  node [
    id 13
    label "funkcjonowanie"
  ]
  node [
    id 14
    label "cel"
  ]
  node [
    id 15
    label "supremum"
  ]
  node [
    id 16
    label "powierzanie"
  ]
  node [
    id 17
    label "rzut"
  ]
  node [
    id 18
    label "addytywno&#347;&#263;"
  ]
  node [
    id 19
    label "monotoniczno&#347;&#263;"
  ]
  node [
    id 20
    label "wakowa&#263;"
  ]
  node [
    id 21
    label "dziedzina"
  ]
  node [
    id 22
    label "postawi&#263;"
  ]
  node [
    id 23
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 24
    label "czyn"
  ]
  node [
    id 25
    label "przeciwdziedzina"
  ]
  node [
    id 26
    label "matematyka"
  ]
  node [
    id 27
    label "awansowa&#263;"
  ]
  node [
    id 28
    label "praca"
  ]
  node [
    id 29
    label "stawia&#263;"
  ]
  node [
    id 30
    label "jednostka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
]
