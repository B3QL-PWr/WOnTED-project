graph [
  maxDegree 22
  minDegree 1
  meanDegree 1.9411764705882353
  density 0.058823529411764705
  graphCliqueNumber 2
  node [
    id 0
    label "madka"
    origin "text"
  ]
  node [
    id 1
    label "dziecko"
    origin "text"
  ]
  node [
    id 2
    label "muszy"
    origin "text"
  ]
  node [
    id 3
    label "czekac"
    origin "text"
  ]
  node [
    id 4
    label "kazdy"
    origin "text"
  ]
  node [
    id 5
    label "kolejka"
    origin "text"
  ]
  node [
    id 6
    label "cz&#322;owiek"
  ]
  node [
    id 7
    label "potomstwo"
  ]
  node [
    id 8
    label "organizm"
  ]
  node [
    id 9
    label "sraluch"
  ]
  node [
    id 10
    label "utulanie"
  ]
  node [
    id 11
    label "pediatra"
  ]
  node [
    id 12
    label "dzieciarnia"
  ]
  node [
    id 13
    label "m&#322;odziak"
  ]
  node [
    id 14
    label "dzieciak"
  ]
  node [
    id 15
    label "utula&#263;"
  ]
  node [
    id 16
    label "potomek"
  ]
  node [
    id 17
    label "pedofil"
  ]
  node [
    id 18
    label "entliczek-pentliczek"
  ]
  node [
    id 19
    label "m&#322;odzik"
  ]
  node [
    id 20
    label "cz&#322;owieczek"
  ]
  node [
    id 21
    label "zwierz&#281;"
  ]
  node [
    id 22
    label "niepe&#322;noletni"
  ]
  node [
    id 23
    label "fledgling"
  ]
  node [
    id 24
    label "utuli&#263;"
  ]
  node [
    id 25
    label "utulenie"
  ]
  node [
    id 26
    label "seria"
  ]
  node [
    id 27
    label "kolej"
  ]
  node [
    id 28
    label "nagromadzenie"
  ]
  node [
    id 29
    label "zabawka"
  ]
  node [
    id 30
    label "line"
  ]
  node [
    id 31
    label "rz&#261;d"
  ]
  node [
    id 32
    label "porcja"
  ]
  node [
    id 33
    label "struktura"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
]
