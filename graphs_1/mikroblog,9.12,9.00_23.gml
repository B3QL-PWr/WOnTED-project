graph [
  maxDegree 8
  minDegree 1
  meanDegree 1.8181818181818181
  density 0.18181818181818182
  graphCliqueNumber 2
  node [
    id 0
    label "saponara"
    origin "text"
  ]
  node [
    id 1
    label "lazio"
    origin "text"
  ]
  node [
    id 2
    label "sampdoria"
    origin "text"
  ]
  node [
    id 3
    label "asysta"
    origin "text"
  ]
  node [
    id 4
    label "kownackiego"
    origin "text"
  ]
  node [
    id 5
    label "zagrywka"
  ]
  node [
    id 6
    label "obecno&#347;&#263;"
  ]
  node [
    id 7
    label "company"
  ]
  node [
    id 8
    label "asystencja"
  ]
  node [
    id 9
    label "zesp&#243;&#322;"
  ]
  node [
    id 10
    label "pomoc"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
]
