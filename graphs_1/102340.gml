graph [
  maxDegree 50
  minDegree 1
  meanDegree 2.3113964686998396
  density 0.0037160714930865586
  graphCliqueNumber 4
  node [
    id 0
    label "niestety"
    origin "text"
  ]
  node [
    id 1
    label "by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "w&#322;a&#347;nie"
    origin "text"
  ]
  node [
    id 3
    label "jedno"
    origin "text"
  ]
  node [
    id 4
    label "ale"
    origin "text"
  ]
  node [
    id 5
    label "tyle"
    origin "text"
  ]
  node [
    id 6
    label "powa&#380;ny"
    origin "text"
  ]
  node [
    id 7
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 8
    label "kilkugodzinny"
    origin "text"
  ]
  node [
    id 9
    label "u&#380;ywanie"
    origin "text"
  ]
  node [
    id 10
    label "firefoxa"
    origin "text"
  ]
  node [
    id 11
    label "opera"
    origin "text"
  ]
  node [
    id 12
    label "potem"
    origin "text"
  ]
  node [
    id 13
    label "przez"
    origin "text"
  ]
  node [
    id 14
    label "kilka"
    origin "text"
  ]
  node [
    id 15
    label "dni"
    origin "text"
  ]
  node [
    id 16
    label "odpoczywa&#263;"
    origin "text"
  ]
  node [
    id 17
    label "coraz"
    origin "text"
  ]
  node [
    id 18
    label "bardzo"
    origin "text"
  ]
  node [
    id 19
    label "doskwiera&#263;"
    origin "text"
  ]
  node [
    id 20
    label "brak"
    origin "text"
  ]
  node [
    id 21
    label "firefoxowych"
    origin "text"
  ]
  node [
    id 22
    label "gad&#380;et"
    origin "text"
  ]
  node [
    id 23
    label "ponownie"
    origin "text"
  ]
  node [
    id 24
    label "oswaja&#263;"
    origin "text"
  ]
  node [
    id 25
    label "lis"
    origin "text"
  ]
  node [
    id 26
    label "pr&#243;bowa&#263;"
    origin "text"
  ]
  node [
    id 27
    label "wiedzie&#263;"
    origin "text"
  ]
  node [
    id 28
    label "nie"
    origin "text"
  ]
  node [
    id 29
    label "szansa"
    origin "text"
  ]
  node [
    id 30
    label "wygra&#263;"
    origin "text"
  ]
  node [
    id 31
    label "koszmarny"
    origin "text"
  ]
  node [
    id 32
    label "apetyt"
    origin "text"
  ]
  node [
    id 33
    label "ram"
    origin "text"
  ]
  node [
    id 34
    label "moje"
    origin "text"
  ]
  node [
    id 35
    label "blaszak"
    origin "text"
  ]
  node [
    id 36
    label "ten"
    origin "text"
  ]
  node [
    id 37
    label "zwierzak"
    origin "text"
  ]
  node [
    id 38
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 39
    label "starcie"
    origin "text"
  ]
  node [
    id 40
    label "po&#380;er"
    origin "text"
  ]
  node [
    id 41
    label "pami&#281;&#263;"
    origin "text"
  ]
  node [
    id 42
    label "ile"
    origin "text"
  ]
  node [
    id 43
    label "kilkana&#347;cie"
    origin "text"
  ]
  node [
    id 44
    label "godzina"
    origin "text"
  ]
  node [
    id 45
    label "zjada&#263;"
    origin "text"
  ]
  node [
    id 46
    label "po&#322;owa"
    origin "text"
  ]
  node [
    id 47
    label "dost&#281;pny"
    origin "text"
  ]
  node [
    id 48
    label "tym"
    origin "text"
  ]
  node [
    id 49
    label "tak"
    origin "text"
  ]
  node [
    id 50
    label "jak"
    origin "text"
  ]
  node [
    id 51
    label "teraz"
    origin "text"
  ]
  node [
    id 52
    label "gdy"
    origin "text"
  ]
  node [
    id 53
    label "pisa&#263;"
    origin "text"
  ]
  node [
    id 54
    label "notatka"
    origin "text"
  ]
  node [
    id 55
    label "dzia&#322;o"
    origin "text"
  ]
  node [
    id 56
    label "zaledwie"
    origin "text"
  ]
  node [
    id 57
    label "dwa"
    origin "text"
  ]
  node [
    id 58
    label "prawie"
    origin "text"
  ]
  node [
    id 59
    label "oczywi&#347;cie"
    origin "text"
  ]
  node [
    id 60
    label "pr&#243;bowa&#263;by&#263;"
    origin "text"
  ]
  node [
    id 61
    label "ograniczy&#263;"
    origin "text"
  ]
  node [
    id 62
    label "ilo&#347;&#263;"
    origin "text"
  ]
  node [
    id 63
    label "w&#322;&#261;czy&#263;"
    origin "text"
  ]
  node [
    id 64
    label "wtyczka"
    origin "text"
  ]
  node [
    id 65
    label "raczej"
    origin "text"
  ]
  node [
    id 66
    label "bez"
    origin "text"
  ]
  node [
    id 67
    label "firefox"
    origin "text"
  ]
  node [
    id 68
    label "przegrywa&#263;"
    origin "text"
  ]
  node [
    id 69
    label "nawet"
    origin "text"
  ]
  node [
    id 70
    label "wy&#322;&#261;czenie"
    origin "text"
  ]
  node [
    id 71
    label "wszyscy"
    origin "text"
  ]
  node [
    id 72
    label "lichy"
    origin "text"
  ]
  node [
    id 73
    label "zmniejszy&#263;"
    origin "text"
  ]
  node [
    id 74
    label "si&#281;"
    origin "text"
  ]
  node [
    id 75
    label "niemal"
    origin "text"
  ]
  node [
    id 76
    label "niezauwa&#380;alnie"
    origin "text"
  ]
  node [
    id 77
    label "przeczyta&#263;by&#263;"
    origin "text"
  ]
  node [
    id 78
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 79
    label "tworzy&#263;"
    origin "text"
  ]
  node [
    id 80
    label "twierdza"
    origin "text"
  ]
  node [
    id 81
    label "prosty"
    origin "text"
  ]
  node [
    id 82
    label "u&#380;ywa&#263;"
    origin "text"
  ]
  node [
    id 83
    label "du&#380;o"
    origin "text"
  ]
  node [
    id 84
    label "dzi&#281;ki"
    origin "text"
  ]
  node [
    id 85
    label "temu"
    origin "text"
  ]
  node [
    id 86
    label "szybko"
    origin "text"
  ]
  node [
    id 87
    label "przeczy&#263;"
    origin "text"
  ]
  node [
    id 88
    label "tak&#380;e"
    origin "text"
  ]
  node [
    id 89
    label "zadyszka"
    origin "text"
  ]
  node [
    id 90
    label "dostawa&#263;"
    origin "text"
  ]
  node [
    id 91
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 92
    label "system"
    origin "text"
  ]
  node [
    id 93
    label "przyk&#322;ad"
    origin "text"
  ]
  node [
    id 94
    label "jednocze&#347;nie"
    origin "text"
  ]
  node [
    id 95
    label "photoshopa"
    origin "text"
  ]
  node [
    id 96
    label "przepada&#263;"
    origin "text"
  ]
  node [
    id 97
    label "regularny"
    origin "text"
  ]
  node [
    id 98
    label "program"
    origin "text"
  ]
  node [
    id 99
    label "optymalizacja"
    origin "text"
  ]
  node [
    id 100
    label "skaka&#263;"
    origin "text"
  ]
  node [
    id 101
    label "rado&#347;nie"
    origin "text"
  ]
  node [
    id 102
    label "pomi&#281;dzy"
    origin "text"
  ]
  node [
    id 103
    label "firefoxem"
    origin "text"
  ]
  node [
    id 104
    label "idealny"
    origin "text"
  ]
  node [
    id 105
    label "przegl&#261;darka"
    origin "text"
  ]
  node [
    id 106
    label "taki"
    origin "text"
  ]
  node [
    id 107
    label "mo&#380;liwo&#347;ci"
    origin "text"
  ]
  node [
    id 108
    label "konfiguracyjny"
    origin "text"
  ]
  node [
    id 109
    label "wykorzystywa&#263;"
    origin "text"
  ]
  node [
    id 110
    label "poziom"
    origin "text"
  ]
  node [
    id 111
    label "si&#281;ga&#263;"
  ]
  node [
    id 112
    label "trwa&#263;"
  ]
  node [
    id 113
    label "obecno&#347;&#263;"
  ]
  node [
    id 114
    label "stan"
  ]
  node [
    id 115
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 116
    label "stand"
  ]
  node [
    id 117
    label "mie&#263;_miejsce"
  ]
  node [
    id 118
    label "uczestniczy&#263;"
  ]
  node [
    id 119
    label "chodzi&#263;"
  ]
  node [
    id 120
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 121
    label "equal"
  ]
  node [
    id 122
    label "dok&#322;adnie"
  ]
  node [
    id 123
    label "piwo"
  ]
  node [
    id 124
    label "wiele"
  ]
  node [
    id 125
    label "konkretnie"
  ]
  node [
    id 126
    label "nieznacznie"
  ]
  node [
    id 127
    label "gro&#378;ny"
  ]
  node [
    id 128
    label "trudny"
  ]
  node [
    id 129
    label "du&#380;y"
  ]
  node [
    id 130
    label "spowa&#380;nienie"
  ]
  node [
    id 131
    label "prawdziwy"
  ]
  node [
    id 132
    label "powa&#380;nienie"
  ]
  node [
    id 133
    label "powa&#380;nie"
  ]
  node [
    id 134
    label "ci&#281;&#380;ko"
  ]
  node [
    id 135
    label "ci&#281;&#380;ki"
  ]
  node [
    id 136
    label "jaki&#347;"
  ]
  node [
    id 137
    label "kilkogodzinny"
  ]
  node [
    id 138
    label "use"
  ]
  node [
    id 139
    label "robienie"
  ]
  node [
    id 140
    label "czynno&#347;&#263;"
  ]
  node [
    id 141
    label "exercise"
  ]
  node [
    id 142
    label "zniszczenie"
  ]
  node [
    id 143
    label "stosowanie"
  ]
  node [
    id 144
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 145
    label "przejaskrawianie"
  ]
  node [
    id 146
    label "nau&#380;ywanie_si&#281;"
  ]
  node [
    id 147
    label "zu&#380;ywanie"
  ]
  node [
    id 148
    label "u&#380;yteczny"
  ]
  node [
    id 149
    label "relish"
  ]
  node [
    id 150
    label "zaznawanie"
  ]
  node [
    id 151
    label "przedstawienie"
  ]
  node [
    id 152
    label "uwertura"
  ]
  node [
    id 153
    label "cyrk"
  ]
  node [
    id 154
    label "libretto"
  ]
  node [
    id 155
    label "teatr"
  ]
  node [
    id 156
    label "scena"
  ]
  node [
    id 157
    label "aria"
  ]
  node [
    id 158
    label "instytucja"
  ]
  node [
    id 159
    label "utw&#243;r"
  ]
  node [
    id 160
    label "sztuka"
  ]
  node [
    id 161
    label "&#347;ledziowate"
  ]
  node [
    id 162
    label "ryba"
  ]
  node [
    id 163
    label "czas"
  ]
  node [
    id 164
    label "pozostawa&#263;"
  ]
  node [
    id 165
    label "hesitate"
  ]
  node [
    id 166
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 167
    label "w_chuj"
  ]
  node [
    id 168
    label "przykrzy&#263;_si&#281;"
  ]
  node [
    id 169
    label "trouble_oneself"
  ]
  node [
    id 170
    label "dojmowa&#263;"
  ]
  node [
    id 171
    label "prywatywny"
  ]
  node [
    id 172
    label "defect"
  ]
  node [
    id 173
    label "odej&#347;cie"
  ]
  node [
    id 174
    label "gap"
  ]
  node [
    id 175
    label "kr&#243;tki"
  ]
  node [
    id 176
    label "wyr&#243;b"
  ]
  node [
    id 177
    label "nieistnienie"
  ]
  node [
    id 178
    label "wada"
  ]
  node [
    id 179
    label "odej&#347;&#263;"
  ]
  node [
    id 180
    label "odchodzenie"
  ]
  node [
    id 181
    label "odchodzi&#263;"
  ]
  node [
    id 182
    label "przedmiot"
  ]
  node [
    id 183
    label "znowu"
  ]
  node [
    id 184
    label "ponowny"
  ]
  node [
    id 185
    label "przywi&#261;zywa&#263;"
  ]
  node [
    id 186
    label "familiarize"
  ]
  node [
    id 187
    label "przyzwyczaja&#263;"
  ]
  node [
    id 188
    label "zwierz&#281;"
  ]
  node [
    id 189
    label "wyjadacz"
  ]
  node [
    id 190
    label "fio&#322;ek"
  ]
  node [
    id 191
    label "ro&#347;lino&#380;erca"
  ]
  node [
    id 192
    label "szal"
  ]
  node [
    id 193
    label "futro"
  ]
  node [
    id 194
    label "kita"
  ]
  node [
    id 195
    label "pies"
  ]
  node [
    id 196
    label "fox"
  ]
  node [
    id 197
    label "zwierz&#281;_&#322;owne"
  ]
  node [
    id 198
    label "liszka"
  ]
  node [
    id 199
    label "spryciarz"
  ]
  node [
    id 200
    label "zwierzyna_drobna"
  ]
  node [
    id 201
    label "przygotowywa&#263;_si&#281;"
  ]
  node [
    id 202
    label "feel"
  ]
  node [
    id 203
    label "try"
  ]
  node [
    id 204
    label "pos&#322;ugiwa&#263;_si&#281;"
  ]
  node [
    id 205
    label "spo&#380;ywa&#263;"
  ]
  node [
    id 206
    label "sprawdza&#263;"
  ]
  node [
    id 207
    label "stara&#263;_si&#281;"
  ]
  node [
    id 208
    label "kosztowa&#263;"
  ]
  node [
    id 209
    label "cognizance"
  ]
  node [
    id 210
    label "sprzeciw"
  ]
  node [
    id 211
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 212
    label "score"
  ]
  node [
    id 213
    label "okaza&#263;_si&#281;"
  ]
  node [
    id 214
    label "zwojowa&#263;"
  ]
  node [
    id 215
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 216
    label "leave"
  ]
  node [
    id 217
    label "znie&#347;&#263;"
  ]
  node [
    id 218
    label "zagwarantowa&#263;"
  ]
  node [
    id 219
    label "instrument_muzyczny"
  ]
  node [
    id 220
    label "zagra&#263;"
  ]
  node [
    id 221
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 222
    label "zrobi&#263;"
  ]
  node [
    id 223
    label "net_income"
  ]
  node [
    id 224
    label "okropny"
  ]
  node [
    id 225
    label "masakryczny"
  ]
  node [
    id 226
    label "straszny"
  ]
  node [
    id 227
    label "senny"
  ]
  node [
    id 228
    label "koszmarnie"
  ]
  node [
    id 229
    label "straszliwy"
  ]
  node [
    id 230
    label "makabrycznie"
  ]
  node [
    id 231
    label "g&#322;&#243;d"
  ]
  node [
    id 232
    label "oskoma"
  ]
  node [
    id 233
    label "po&#380;&#261;danie"
  ]
  node [
    id 234
    label "pa&#322;aszowanie"
  ]
  node [
    id 235
    label "emocja"
  ]
  node [
    id 236
    label "hunger"
  ]
  node [
    id 237
    label "inclination"
  ]
  node [
    id 238
    label "pa&#322;aszowa&#263;"
  ]
  node [
    id 239
    label "ch&#281;&#263;"
  ]
  node [
    id 240
    label "zajawka"
  ]
  node [
    id 241
    label "budynek"
  ]
  node [
    id 242
    label "okre&#347;lony"
  ]
  node [
    id 243
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 244
    label "zranienie"
  ]
  node [
    id 245
    label "konfrontacyjny"
  ]
  node [
    id 246
    label "usuni&#281;cie"
  ]
  node [
    id 247
    label "military_action"
  ]
  node [
    id 248
    label "trafienie"
  ]
  node [
    id 249
    label "oczyszczenie"
  ]
  node [
    id 250
    label "sambo"
  ]
  node [
    id 251
    label "euroliga"
  ]
  node [
    id 252
    label "zaatakowanie"
  ]
  node [
    id 253
    label "interliga"
  ]
  node [
    id 254
    label "runda"
  ]
  node [
    id 255
    label "contest"
  ]
  node [
    id 256
    label "discord"
  ]
  node [
    id 257
    label "zagrywka"
  ]
  node [
    id 258
    label "obrona"
  ]
  node [
    id 259
    label "expunction"
  ]
  node [
    id 260
    label "rub"
  ]
  node [
    id 261
    label "wydarzenie"
  ]
  node [
    id 262
    label "rewan&#380;owy"
  ]
  node [
    id 263
    label "otarcie"
  ]
  node [
    id 264
    label "faza"
  ]
  node [
    id 265
    label "rozdrobnienie"
  ]
  node [
    id 266
    label "k&#322;&#243;tnia"
  ]
  node [
    id 267
    label "scramble"
  ]
  node [
    id 268
    label "hipokamp"
  ]
  node [
    id 269
    label "wymazanie"
  ]
  node [
    id 270
    label "wytw&#243;r"
  ]
  node [
    id 271
    label "zachowa&#263;"
  ]
  node [
    id 272
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 273
    label "memory"
  ]
  node [
    id 274
    label "pami&#281;tno&#347;&#263;"
  ]
  node [
    id 275
    label "umys&#322;"
  ]
  node [
    id 276
    label "sprz&#281;t_komputerowy"
  ]
  node [
    id 277
    label "komputer"
  ]
  node [
    id 278
    label "minuta"
  ]
  node [
    id 279
    label "doba"
  ]
  node [
    id 280
    label "p&#243;&#322;godzina"
  ]
  node [
    id 281
    label "kwadrans"
  ]
  node [
    id 282
    label "time"
  ]
  node [
    id 283
    label "jednostka_czasu"
  ]
  node [
    id 284
    label "papusia&#263;"
  ]
  node [
    id 285
    label "wpieprza&#263;"
  ]
  node [
    id 286
    label "take"
  ]
  node [
    id 287
    label "&#380;re&#263;"
  ]
  node [
    id 288
    label "medium"
  ]
  node [
    id 289
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 290
    label "&#322;atwy"
  ]
  node [
    id 291
    label "mo&#380;liwy"
  ]
  node [
    id 292
    label "dost&#281;pnie"
  ]
  node [
    id 293
    label "ods&#322;oni&#281;cie_si&#281;"
  ]
  node [
    id 294
    label "przyst&#281;pnie"
  ]
  node [
    id 295
    label "zrozumia&#322;y"
  ]
  node [
    id 296
    label "odblokowywanie_si&#281;"
  ]
  node [
    id 297
    label "odblokowanie_si&#281;"
  ]
  node [
    id 298
    label "ods&#322;anianie_si&#281;"
  ]
  node [
    id 299
    label "byd&#322;o"
  ]
  node [
    id 300
    label "zobo"
  ]
  node [
    id 301
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 302
    label "yakalo"
  ]
  node [
    id 303
    label "dzo"
  ]
  node [
    id 304
    label "chwila"
  ]
  node [
    id 305
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 306
    label "ozdabia&#263;"
  ]
  node [
    id 307
    label "dysgrafia"
  ]
  node [
    id 308
    label "prasa"
  ]
  node [
    id 309
    label "spell"
  ]
  node [
    id 310
    label "skryba"
  ]
  node [
    id 311
    label "donosi&#263;"
  ]
  node [
    id 312
    label "code"
  ]
  node [
    id 313
    label "tekst"
  ]
  node [
    id 314
    label "dysortografia"
  ]
  node [
    id 315
    label "read"
  ]
  node [
    id 316
    label "formu&#322;owa&#263;"
  ]
  node [
    id 317
    label "styl"
  ]
  node [
    id 318
    label "stawia&#263;"
  ]
  node [
    id 319
    label "konotatka"
  ]
  node [
    id 320
    label "tre&#347;ciwy"
  ]
  node [
    id 321
    label "zapis"
  ]
  node [
    id 322
    label "note"
  ]
  node [
    id 323
    label "zwi&#281;z&#322;y"
  ]
  node [
    id 324
    label "bateria"
  ]
  node [
    id 325
    label "laweta"
  ]
  node [
    id 326
    label "bro&#324;_artyleryjska"
  ]
  node [
    id 327
    label "bro&#324;"
  ]
  node [
    id 328
    label "oporopowrotnik"
  ]
  node [
    id 329
    label "przedmuchiwacz"
  ]
  node [
    id 330
    label "artyleria"
  ]
  node [
    id 331
    label "waln&#261;&#263;"
  ]
  node [
    id 332
    label "bateria_artylerii"
  ]
  node [
    id 333
    label "cannon"
  ]
  node [
    id 334
    label "pomiarkowa&#263;"
  ]
  node [
    id 335
    label "boundary_line"
  ]
  node [
    id 336
    label "ustanowi&#263;"
  ]
  node [
    id 337
    label "reduce"
  ]
  node [
    id 338
    label "deoxidize"
  ]
  node [
    id 339
    label "otoczy&#263;"
  ]
  node [
    id 340
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 341
    label "rozmiar"
  ]
  node [
    id 342
    label "part"
  ]
  node [
    id 343
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 344
    label "zacz&#261;&#263;"
  ]
  node [
    id 345
    label "nastawi&#263;"
  ]
  node [
    id 346
    label "prosecute"
  ]
  node [
    id 347
    label "odkr&#281;ci&#263;_kurek"
  ]
  node [
    id 348
    label "impersonate"
  ]
  node [
    id 349
    label "umie&#347;ci&#263;"
  ]
  node [
    id 350
    label "obejrze&#263;"
  ]
  node [
    id 351
    label "draw"
  ]
  node [
    id 352
    label "incorporate"
  ]
  node [
    id 353
    label "uruchomi&#263;"
  ]
  node [
    id 354
    label "dokoptowa&#263;"
  ]
  node [
    id 355
    label "szpieg"
  ]
  node [
    id 356
    label "dodatek"
  ]
  node [
    id 357
    label "ko&#324;c&#243;wka"
  ]
  node [
    id 358
    label "connection"
  ]
  node [
    id 359
    label "przew&#243;d"
  ]
  node [
    id 360
    label "kontakt"
  ]
  node [
    id 361
    label "ki&#347;&#263;"
  ]
  node [
    id 362
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 363
    label "krzew"
  ]
  node [
    id 364
    label "pi&#380;maczkowate"
  ]
  node [
    id 365
    label "pestkowiec"
  ]
  node [
    id 366
    label "kwiat"
  ]
  node [
    id 367
    label "owoc"
  ]
  node [
    id 368
    label "oliwkowate"
  ]
  node [
    id 369
    label "ro&#347;lina"
  ]
  node [
    id 370
    label "hy&#263;ka"
  ]
  node [
    id 371
    label "lilac"
  ]
  node [
    id 372
    label "delfinidyna"
  ]
  node [
    id 373
    label "play"
  ]
  node [
    id 374
    label "ponosi&#263;"
  ]
  node [
    id 375
    label "od&#322;&#261;czenie"
  ]
  node [
    id 376
    label "cutoff"
  ]
  node [
    id 377
    label "przestanie"
  ]
  node [
    id 378
    label "wy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 379
    label "w&#322;&#261;czenie"
  ]
  node [
    id 380
    label "nieprawdopodobie&#324;stwo"
  ]
  node [
    id 381
    label "wydzielenie"
  ]
  node [
    id 382
    label "przygaszenie"
  ]
  node [
    id 383
    label "wykluczenie"
  ]
  node [
    id 384
    label "debarment"
  ]
  node [
    id 385
    label "gotowanie"
  ]
  node [
    id 386
    label "przerwanie"
  ]
  node [
    id 387
    label "zatrzymanie"
  ]
  node [
    id 388
    label "release"
  ]
  node [
    id 389
    label "closure"
  ]
  node [
    id 390
    label "odrzucenie"
  ]
  node [
    id 391
    label "odci&#281;cie"
  ]
  node [
    id 392
    label "licho"
  ]
  node [
    id 393
    label "z&#322;y"
  ]
  node [
    id 394
    label "n&#281;dzny"
  ]
  node [
    id 395
    label "kiepsko"
  ]
  node [
    id 396
    label "pomierny"
  ]
  node [
    id 397
    label "mizerny"
  ]
  node [
    id 398
    label "zmieni&#263;"
  ]
  node [
    id 399
    label "soften"
  ]
  node [
    id 400
    label "niezauwa&#380;alny"
  ]
  node [
    id 401
    label "niepostrzegalnie"
  ]
  node [
    id 402
    label "get"
  ]
  node [
    id 403
    label "consist"
  ]
  node [
    id 404
    label "raise"
  ]
  node [
    id 405
    label "robi&#263;"
  ]
  node [
    id 406
    label "pope&#322;nia&#263;"
  ]
  node [
    id 407
    label "wytwarza&#263;"
  ]
  node [
    id 408
    label "stanowi&#263;"
  ]
  node [
    id 409
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 410
    label "Brenna"
  ]
  node [
    id 411
    label "Szlisselburg"
  ]
  node [
    id 412
    label "Dyjament"
  ]
  node [
    id 413
    label "flanka"
  ]
  node [
    id 414
    label "schronienie"
  ]
  node [
    id 415
    label "budowla"
  ]
  node [
    id 416
    label "bastion"
  ]
  node [
    id 417
    label "prostowanie_si&#281;"
  ]
  node [
    id 418
    label "rozprostowanie_si&#281;"
  ]
  node [
    id 419
    label "rozprostowanie"
  ]
  node [
    id 420
    label "niez&#322;o&#380;ony"
  ]
  node [
    id 421
    label "prostoduszny"
  ]
  node [
    id 422
    label "naturalny"
  ]
  node [
    id 423
    label "naiwny"
  ]
  node [
    id 424
    label "cios"
  ]
  node [
    id 425
    label "prostowanie"
  ]
  node [
    id 426
    label "niepozorny"
  ]
  node [
    id 427
    label "zwyk&#322;y"
  ]
  node [
    id 428
    label "prosto"
  ]
  node [
    id 429
    label "po_prostu"
  ]
  node [
    id 430
    label "skromny"
  ]
  node [
    id 431
    label "wyprostowanie_si&#281;"
  ]
  node [
    id 432
    label "bash"
  ]
  node [
    id 433
    label "distribute"
  ]
  node [
    id 434
    label "give"
  ]
  node [
    id 435
    label "korzysta&#263;"
  ]
  node [
    id 436
    label "doznawa&#263;"
  ]
  node [
    id 437
    label "cz&#281;sto"
  ]
  node [
    id 438
    label "mocno"
  ]
  node [
    id 439
    label "wiela"
  ]
  node [
    id 440
    label "quicker"
  ]
  node [
    id 441
    label "promptly"
  ]
  node [
    id 442
    label "bezpo&#347;rednio"
  ]
  node [
    id 443
    label "quickest"
  ]
  node [
    id 444
    label "sprawnie"
  ]
  node [
    id 445
    label "dynamicznie"
  ]
  node [
    id 446
    label "szybciej"
  ]
  node [
    id 447
    label "szybciochem"
  ]
  node [
    id 448
    label "szybki"
  ]
  node [
    id 449
    label "kwestionowa&#263;"
  ]
  node [
    id 450
    label "k&#322;&#243;ci&#263;_si&#281;"
  ]
  node [
    id 451
    label "zapiera&#263;"
  ]
  node [
    id 452
    label "repudiate"
  ]
  node [
    id 453
    label "oznaka"
  ]
  node [
    id 454
    label "schorzenie"
  ]
  node [
    id 455
    label "astma_sercowa"
  ]
  node [
    id 456
    label "oddech"
  ]
  node [
    id 457
    label "dychawka"
  ]
  node [
    id 458
    label "salbutamol"
  ]
  node [
    id 459
    label "dusznica"
  ]
  node [
    id 460
    label "aminofilina"
  ]
  node [
    id 461
    label "uzyskiwa&#263;"
  ]
  node [
    id 462
    label "wystarcza&#263;"
  ]
  node [
    id 463
    label "range"
  ]
  node [
    id 464
    label "winnings"
  ]
  node [
    id 465
    label "otrzymywa&#263;"
  ]
  node [
    id 466
    label "opanowywa&#263;"
  ]
  node [
    id 467
    label "kupowa&#263;"
  ]
  node [
    id 468
    label "nabywa&#263;"
  ]
  node [
    id 469
    label "bra&#263;"
  ]
  node [
    id 470
    label "obskakiwa&#263;"
  ]
  node [
    id 471
    label "czyj&#347;"
  ]
  node [
    id 472
    label "m&#261;&#380;"
  ]
  node [
    id 473
    label "model"
  ]
  node [
    id 474
    label "sk&#322;ad"
  ]
  node [
    id 475
    label "zachowanie"
  ]
  node [
    id 476
    label "podstawa"
  ]
  node [
    id 477
    label "porz&#261;dek"
  ]
  node [
    id 478
    label "Android"
  ]
  node [
    id 479
    label "przyn&#281;ta"
  ]
  node [
    id 480
    label "jednostka_geologiczna"
  ]
  node [
    id 481
    label "metoda"
  ]
  node [
    id 482
    label "podsystem"
  ]
  node [
    id 483
    label "p&#322;&#243;d"
  ]
  node [
    id 484
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 485
    label "s&#261;d"
  ]
  node [
    id 486
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 487
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 488
    label "j&#261;dro"
  ]
  node [
    id 489
    label "eratem"
  ]
  node [
    id 490
    label "pulpit"
  ]
  node [
    id 491
    label "struktura"
  ]
  node [
    id 492
    label "spos&#243;b"
  ]
  node [
    id 493
    label "oddzia&#322;"
  ]
  node [
    id 494
    label "usenet"
  ]
  node [
    id 495
    label "o&#347;"
  ]
  node [
    id 496
    label "oprogramowanie"
  ]
  node [
    id 497
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 498
    label "poj&#281;cie"
  ]
  node [
    id 499
    label "w&#281;dkarstwo"
  ]
  node [
    id 500
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 501
    label "Leopard"
  ]
  node [
    id 502
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 503
    label "systemik"
  ]
  node [
    id 504
    label "rozprz&#261;c"
  ]
  node [
    id 505
    label "cybernetyk"
  ]
  node [
    id 506
    label "konstelacja"
  ]
  node [
    id 507
    label "doktryna"
  ]
  node [
    id 508
    label "net"
  ]
  node [
    id 509
    label "zbi&#243;r"
  ]
  node [
    id 510
    label "method"
  ]
  node [
    id 511
    label "systemat"
  ]
  node [
    id 512
    label "cz&#322;owiek"
  ]
  node [
    id 513
    label "czyn"
  ]
  node [
    id 514
    label "przedstawiciel"
  ]
  node [
    id 515
    label "ilustracja"
  ]
  node [
    id 516
    label "fakt"
  ]
  node [
    id 517
    label "simultaneously"
  ]
  node [
    id 518
    label "coincidentally"
  ]
  node [
    id 519
    label "synchronously"
  ]
  node [
    id 520
    label "concurrently"
  ]
  node [
    id 521
    label "jednoczesny"
  ]
  node [
    id 522
    label "pada&#263;"
  ]
  node [
    id 523
    label "marnowa&#263;_si&#281;"
  ]
  node [
    id 524
    label "gin&#261;&#263;"
  ]
  node [
    id 525
    label "love"
  ]
  node [
    id 526
    label "lubi&#263;"
  ]
  node [
    id 527
    label "w&#347;cieka&#263;_si&#281;"
  ]
  node [
    id 528
    label "drench"
  ]
  node [
    id 529
    label "precipice"
  ]
  node [
    id 530
    label "ko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 531
    label "die"
  ]
  node [
    id 532
    label "zwyczajnie"
  ]
  node [
    id 533
    label "sta&#322;y"
  ]
  node [
    id 534
    label "harmonijny"
  ]
  node [
    id 535
    label "powtarzalny"
  ]
  node [
    id 536
    label "zorganizowany"
  ]
  node [
    id 537
    label "regularnie"
  ]
  node [
    id 538
    label "spis"
  ]
  node [
    id 539
    label "odinstalowanie"
  ]
  node [
    id 540
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 541
    label "za&#322;o&#380;enie"
  ]
  node [
    id 542
    label "emitowanie"
  ]
  node [
    id 543
    label "odinstalowywanie"
  ]
  node [
    id 544
    label "instrukcja"
  ]
  node [
    id 545
    label "punkt"
  ]
  node [
    id 546
    label "teleferie"
  ]
  node [
    id 547
    label "emitowa&#263;"
  ]
  node [
    id 548
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 549
    label "sekcja_krytyczna"
  ]
  node [
    id 550
    label "prezentowa&#263;"
  ]
  node [
    id 551
    label "blok"
  ]
  node [
    id 552
    label "podprogram"
  ]
  node [
    id 553
    label "tryb"
  ]
  node [
    id 554
    label "dzia&#322;"
  ]
  node [
    id 555
    label "broszura"
  ]
  node [
    id 556
    label "deklaracja"
  ]
  node [
    id 557
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 558
    label "struktura_organizacyjna"
  ]
  node [
    id 559
    label "zaprezentowanie"
  ]
  node [
    id 560
    label "informatyka"
  ]
  node [
    id 561
    label "booklet"
  ]
  node [
    id 562
    label "menu"
  ]
  node [
    id 563
    label "instalowanie"
  ]
  node [
    id 564
    label "furkacja"
  ]
  node [
    id 565
    label "odinstalowa&#263;"
  ]
  node [
    id 566
    label "instalowa&#263;"
  ]
  node [
    id 567
    label "okno"
  ]
  node [
    id 568
    label "pirat"
  ]
  node [
    id 569
    label "zainstalowanie"
  ]
  node [
    id 570
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 571
    label "ogranicznik_referencyjny"
  ]
  node [
    id 572
    label "zainstalowa&#263;"
  ]
  node [
    id 573
    label "kana&#322;"
  ]
  node [
    id 574
    label "zaprezentowa&#263;"
  ]
  node [
    id 575
    label "interfejs"
  ]
  node [
    id 576
    label "odinstalowywa&#263;"
  ]
  node [
    id 577
    label "folder"
  ]
  node [
    id 578
    label "course_of_study"
  ]
  node [
    id 579
    label "ram&#243;wka"
  ]
  node [
    id 580
    label "prezentowanie"
  ]
  node [
    id 581
    label "oferta"
  ]
  node [
    id 582
    label "poprawa"
  ]
  node [
    id 583
    label "proces_ekonomiczny"
  ]
  node [
    id 584
    label "optimization"
  ]
  node [
    id 585
    label "lata&#263;"
  ]
  node [
    id 586
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 587
    label "rise"
  ]
  node [
    id 588
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 589
    label "begin"
  ]
  node [
    id 590
    label "chyba&#263;"
  ]
  node [
    id 591
    label "radosny"
  ]
  node [
    id 592
    label "weso&#322;o"
  ]
  node [
    id 593
    label "wznios&#322;y"
  ]
  node [
    id 594
    label "czysty"
  ]
  node [
    id 595
    label "idealistyczny"
  ]
  node [
    id 596
    label "wspania&#322;y"
  ]
  node [
    id 597
    label "pe&#322;ny"
  ]
  node [
    id 598
    label "abstrakcyjny"
  ]
  node [
    id 599
    label "doskonale"
  ]
  node [
    id 600
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 601
    label "skuteczny"
  ]
  node [
    id 602
    label "idealnie"
  ]
  node [
    id 603
    label "viewer"
  ]
  node [
    id 604
    label "przyrz&#261;d"
  ]
  node [
    id 605
    label "browser"
  ]
  node [
    id 606
    label "projektor"
  ]
  node [
    id 607
    label "capability"
  ]
  node [
    id 608
    label "zdolno&#347;&#263;"
  ]
  node [
    id 609
    label "potencja&#322;"
  ]
  node [
    id 610
    label "krzywdzi&#263;"
  ]
  node [
    id 611
    label "wyr&#281;cza&#263;_si&#281;"
  ]
  node [
    id 612
    label "wsp&#243;&#322;&#380;y&#263;"
  ]
  node [
    id 613
    label "liga&#263;"
  ]
  node [
    id 614
    label "wysoko&#347;&#263;"
  ]
  node [
    id 615
    label "szczebel"
  ]
  node [
    id 616
    label "po&#322;o&#380;enie"
  ]
  node [
    id 617
    label "kierunek"
  ]
  node [
    id 618
    label "wyk&#322;adnik"
  ]
  node [
    id 619
    label "punkt_widzenia"
  ]
  node [
    id 620
    label "jako&#347;&#263;"
  ]
  node [
    id 621
    label "ranga"
  ]
  node [
    id 622
    label "p&#322;aszczyzna"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 136
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 137
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 44
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 9
    target 138
  ]
  edge [
    source 9
    target 139
  ]
  edge [
    source 9
    target 140
  ]
  edge [
    source 9
    target 141
  ]
  edge [
    source 9
    target 142
  ]
  edge [
    source 9
    target 143
  ]
  edge [
    source 9
    target 144
  ]
  edge [
    source 9
    target 145
  ]
  edge [
    source 9
    target 146
  ]
  edge [
    source 9
    target 147
  ]
  edge [
    source 9
    target 148
  ]
  edge [
    source 9
    target 149
  ]
  edge [
    source 9
    target 150
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 16
  ]
  edge [
    source 10
    target 17
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 42
  ]
  edge [
    source 11
    target 43
  ]
  edge [
    source 11
    target 68
  ]
  edge [
    source 11
    target 69
  ]
  edge [
    source 11
    target 103
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 11
    target 110
  ]
  edge [
    source 11
    target 151
  ]
  edge [
    source 11
    target 152
  ]
  edge [
    source 11
    target 153
  ]
  edge [
    source 11
    target 154
  ]
  edge [
    source 11
    target 155
  ]
  edge [
    source 11
    target 156
  ]
  edge [
    source 11
    target 157
  ]
  edge [
    source 11
    target 158
  ]
  edge [
    source 11
    target 159
  ]
  edge [
    source 11
    target 160
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 59
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 27
  ]
  edge [
    source 13
    target 55
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 20
  ]
  edge [
    source 14
    target 21
  ]
  edge [
    source 14
    target 161
  ]
  edge [
    source 14
    target 162
  ]
  edge [
    source 14
    target 56
  ]
  edge [
    source 14
    target 88
  ]
  edge [
    source 14
    target 27
  ]
  edge [
    source 14
    target 55
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 163
  ]
  edge [
    source 16
    target 164
  ]
  edge [
    source 16
    target 165
  ]
  edge [
    source 16
    target 166
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 26
  ]
  edge [
    source 17
    target 44
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 167
  ]
  edge [
    source 18
    target 83
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 168
  ]
  edge [
    source 19
    target 169
  ]
  edge [
    source 19
    target 170
  ]
  edge [
    source 20
    target 171
  ]
  edge [
    source 20
    target 172
  ]
  edge [
    source 20
    target 173
  ]
  edge [
    source 20
    target 174
  ]
  edge [
    source 20
    target 175
  ]
  edge [
    source 20
    target 176
  ]
  edge [
    source 20
    target 177
  ]
  edge [
    source 20
    target 178
  ]
  edge [
    source 20
    target 179
  ]
  edge [
    source 20
    target 180
  ]
  edge [
    source 20
    target 181
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 182
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 183
  ]
  edge [
    source 23
    target 184
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 185
  ]
  edge [
    source 24
    target 186
  ]
  edge [
    source 24
    target 187
  ]
  edge [
    source 24
    target 188
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 189
  ]
  edge [
    source 25
    target 190
  ]
  edge [
    source 25
    target 191
  ]
  edge [
    source 25
    target 192
  ]
  edge [
    source 25
    target 193
  ]
  edge [
    source 25
    target 194
  ]
  edge [
    source 25
    target 195
  ]
  edge [
    source 25
    target 196
  ]
  edge [
    source 25
    target 197
  ]
  edge [
    source 25
    target 198
  ]
  edge [
    source 25
    target 199
  ]
  edge [
    source 25
    target 200
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 93
  ]
  edge [
    source 26
    target 94
  ]
  edge [
    source 26
    target 201
  ]
  edge [
    source 26
    target 202
  ]
  edge [
    source 26
    target 151
  ]
  edge [
    source 26
    target 203
  ]
  edge [
    source 26
    target 166
  ]
  edge [
    source 26
    target 204
  ]
  edge [
    source 26
    target 205
  ]
  edge [
    source 26
    target 206
  ]
  edge [
    source 26
    target 207
  ]
  edge [
    source 26
    target 208
  ]
  edge [
    source 26
    target 44
  ]
  edge [
    source 26
    target 43
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 209
  ]
  edge [
    source 27
    target 55
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 48
  ]
  edge [
    source 28
    target 49
  ]
  edge [
    source 28
    target 210
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 211
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 212
  ]
  edge [
    source 30
    target 213
  ]
  edge [
    source 30
    target 214
  ]
  edge [
    source 30
    target 215
  ]
  edge [
    source 30
    target 216
  ]
  edge [
    source 30
    target 217
  ]
  edge [
    source 30
    target 218
  ]
  edge [
    source 30
    target 219
  ]
  edge [
    source 30
    target 220
  ]
  edge [
    source 30
    target 221
  ]
  edge [
    source 30
    target 222
  ]
  edge [
    source 30
    target 223
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 224
  ]
  edge [
    source 31
    target 225
  ]
  edge [
    source 31
    target 226
  ]
  edge [
    source 31
    target 227
  ]
  edge [
    source 31
    target 228
  ]
  edge [
    source 31
    target 229
  ]
  edge [
    source 31
    target 230
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 72
  ]
  edge [
    source 32
    target 73
  ]
  edge [
    source 32
    target 231
  ]
  edge [
    source 32
    target 232
  ]
  edge [
    source 32
    target 233
  ]
  edge [
    source 32
    target 234
  ]
  edge [
    source 32
    target 235
  ]
  edge [
    source 32
    target 236
  ]
  edge [
    source 32
    target 237
  ]
  edge [
    source 32
    target 238
  ]
  edge [
    source 32
    target 239
  ]
  edge [
    source 32
    target 240
  ]
  edge [
    source 32
    target 109
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 99
  ]
  edge [
    source 33
    target 100
  ]
  edge [
    source 33
    target 109
  ]
  edge [
    source 33
    target 110
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 182
  ]
  edge [
    source 35
    target 241
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 53
  ]
  edge [
    source 36
    target 54
  ]
  edge [
    source 36
    target 242
  ]
  edge [
    source 36
    target 243
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 188
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 244
  ]
  edge [
    source 39
    target 245
  ]
  edge [
    source 39
    target 246
  ]
  edge [
    source 39
    target 247
  ]
  edge [
    source 39
    target 248
  ]
  edge [
    source 39
    target 249
  ]
  edge [
    source 39
    target 250
  ]
  edge [
    source 39
    target 251
  ]
  edge [
    source 39
    target 252
  ]
  edge [
    source 39
    target 253
  ]
  edge [
    source 39
    target 254
  ]
  edge [
    source 39
    target 255
  ]
  edge [
    source 39
    target 256
  ]
  edge [
    source 39
    target 257
  ]
  edge [
    source 39
    target 258
  ]
  edge [
    source 39
    target 259
  ]
  edge [
    source 39
    target 260
  ]
  edge [
    source 39
    target 261
  ]
  edge [
    source 39
    target 262
  ]
  edge [
    source 39
    target 263
  ]
  edge [
    source 39
    target 264
  ]
  edge [
    source 39
    target 265
  ]
  edge [
    source 39
    target 266
  ]
  edge [
    source 39
    target 267
  ]
  edge [
    source 39
    target 67
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 47
  ]
  edge [
    source 41
    target 48
  ]
  edge [
    source 41
    target 83
  ]
  edge [
    source 41
    target 84
  ]
  edge [
    source 41
    target 268
  ]
  edge [
    source 41
    target 269
  ]
  edge [
    source 41
    target 270
  ]
  edge [
    source 41
    target 271
  ]
  edge [
    source 41
    target 272
  ]
  edge [
    source 41
    target 273
  ]
  edge [
    source 41
    target 274
  ]
  edge [
    source 41
    target 275
  ]
  edge [
    source 41
    target 276
  ]
  edge [
    source 41
    target 277
  ]
  edge [
    source 41
    target 92
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 57
  ]
  edge [
    source 44
    target 58
  ]
  edge [
    source 44
    target 278
  ]
  edge [
    source 44
    target 279
  ]
  edge [
    source 44
    target 163
  ]
  edge [
    source 44
    target 280
  ]
  edge [
    source 44
    target 281
  ]
  edge [
    source 44
    target 282
  ]
  edge [
    source 44
    target 283
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 284
  ]
  edge [
    source 45
    target 285
  ]
  edge [
    source 45
    target 205
  ]
  edge [
    source 45
    target 286
  ]
  edge [
    source 45
    target 287
  ]
  edge [
    source 45
    target 64
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 288
  ]
  edge [
    source 46
    target 289
  ]
  edge [
    source 46
    target 163
  ]
  edge [
    source 47
    target 290
  ]
  edge [
    source 47
    target 291
  ]
  edge [
    source 47
    target 292
  ]
  edge [
    source 47
    target 293
  ]
  edge [
    source 47
    target 294
  ]
  edge [
    source 47
    target 295
  ]
  edge [
    source 47
    target 296
  ]
  edge [
    source 47
    target 297
  ]
  edge [
    source 47
    target 298
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 82
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 76
  ]
  edge [
    source 50
    target 77
  ]
  edge [
    source 50
    target 299
  ]
  edge [
    source 50
    target 300
  ]
  edge [
    source 50
    target 301
  ]
  edge [
    source 50
    target 302
  ]
  edge [
    source 50
    target 303
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 304
  ]
  edge [
    source 51
    target 305
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 53
    target 306
  ]
  edge [
    source 53
    target 307
  ]
  edge [
    source 53
    target 308
  ]
  edge [
    source 53
    target 309
  ]
  edge [
    source 53
    target 310
  ]
  edge [
    source 53
    target 311
  ]
  edge [
    source 53
    target 312
  ]
  edge [
    source 53
    target 313
  ]
  edge [
    source 53
    target 314
  ]
  edge [
    source 53
    target 315
  ]
  edge [
    source 53
    target 79
  ]
  edge [
    source 53
    target 316
  ]
  edge [
    source 53
    target 317
  ]
  edge [
    source 53
    target 318
  ]
  edge [
    source 53
    target 55
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 319
  ]
  edge [
    source 54
    target 320
  ]
  edge [
    source 54
    target 321
  ]
  edge [
    source 54
    target 322
  ]
  edge [
    source 54
    target 323
  ]
  edge [
    source 54
    target 64
  ]
  edge [
    source 54
    target 105
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 86
  ]
  edge [
    source 55
    target 87
  ]
  edge [
    source 55
    target 324
  ]
  edge [
    source 55
    target 325
  ]
  edge [
    source 55
    target 326
  ]
  edge [
    source 55
    target 327
  ]
  edge [
    source 55
    target 328
  ]
  edge [
    source 55
    target 329
  ]
  edge [
    source 55
    target 330
  ]
  edge [
    source 55
    target 331
  ]
  edge [
    source 55
    target 332
  ]
  edge [
    source 55
    target 333
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 88
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 60
  ]
  edge [
    source 58
    target 76
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 76
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 334
  ]
  edge [
    source 61
    target 335
  ]
  edge [
    source 61
    target 336
  ]
  edge [
    source 61
    target 73
  ]
  edge [
    source 61
    target 337
  ]
  edge [
    source 61
    target 338
  ]
  edge [
    source 61
    target 339
  ]
  edge [
    source 61
    target 340
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 341
  ]
  edge [
    source 62
    target 342
  ]
  edge [
    source 62
    target 343
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 344
  ]
  edge [
    source 63
    target 345
  ]
  edge [
    source 63
    target 346
  ]
  edge [
    source 63
    target 347
  ]
  edge [
    source 63
    target 348
  ]
  edge [
    source 63
    target 349
  ]
  edge [
    source 63
    target 350
  ]
  edge [
    source 63
    target 351
  ]
  edge [
    source 63
    target 352
  ]
  edge [
    source 63
    target 353
  ]
  edge [
    source 63
    target 354
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 71
  ]
  edge [
    source 64
    target 72
  ]
  edge [
    source 64
    target 355
  ]
  edge [
    source 64
    target 356
  ]
  edge [
    source 64
    target 357
  ]
  edge [
    source 64
    target 358
  ]
  edge [
    source 64
    target 359
  ]
  edge [
    source 64
    target 360
  ]
  edge [
    source 64
    target 105
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 66
    target 361
  ]
  edge [
    source 66
    target 362
  ]
  edge [
    source 66
    target 363
  ]
  edge [
    source 66
    target 364
  ]
  edge [
    source 66
    target 365
  ]
  edge [
    source 66
    target 366
  ]
  edge [
    source 66
    target 367
  ]
  edge [
    source 66
    target 368
  ]
  edge [
    source 66
    target 369
  ]
  edge [
    source 66
    target 370
  ]
  edge [
    source 66
    target 371
  ]
  edge [
    source 66
    target 372
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 68
    target 373
  ]
  edge [
    source 68
    target 374
  ]
  edge [
    source 68
    target 96
  ]
  edge [
    source 68
    target 97
  ]
  edge [
    source 68
    target 103
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 375
  ]
  edge [
    source 70
    target 376
  ]
  edge [
    source 70
    target 377
  ]
  edge [
    source 70
    target 378
  ]
  edge [
    source 70
    target 379
  ]
  edge [
    source 70
    target 380
  ]
  edge [
    source 70
    target 381
  ]
  edge [
    source 70
    target 382
  ]
  edge [
    source 70
    target 383
  ]
  edge [
    source 70
    target 384
  ]
  edge [
    source 70
    target 385
  ]
  edge [
    source 70
    target 386
  ]
  edge [
    source 70
    target 387
  ]
  edge [
    source 70
    target 388
  ]
  edge [
    source 70
    target 389
  ]
  edge [
    source 70
    target 390
  ]
  edge [
    source 70
    target 391
  ]
  edge [
    source 72
    target 392
  ]
  edge [
    source 72
    target 393
  ]
  edge [
    source 72
    target 394
  ]
  edge [
    source 72
    target 395
  ]
  edge [
    source 72
    target 396
  ]
  edge [
    source 72
    target 397
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 73
    target 398
  ]
  edge [
    source 73
    target 399
  ]
  edge [
    source 74
    target 75
  ]
  edge [
    source 75
    target 76
  ]
  edge [
    source 76
    target 400
  ]
  edge [
    source 76
    target 401
  ]
  edge [
    source 77
    target 78
  ]
  edge [
    source 77
    target 95
  ]
  edge [
    source 78
    target 79
  ]
  edge [
    source 79
    target 80
  ]
  edge [
    source 79
    target 402
  ]
  edge [
    source 79
    target 403
  ]
  edge [
    source 79
    target 404
  ]
  edge [
    source 79
    target 405
  ]
  edge [
    source 79
    target 406
  ]
  edge [
    source 79
    target 407
  ]
  edge [
    source 79
    target 408
  ]
  edge [
    source 79
    target 409
  ]
  edge [
    source 80
    target 81
  ]
  edge [
    source 80
    target 410
  ]
  edge [
    source 80
    target 411
  ]
  edge [
    source 80
    target 412
  ]
  edge [
    source 80
    target 413
  ]
  edge [
    source 80
    target 414
  ]
  edge [
    source 80
    target 415
  ]
  edge [
    source 80
    target 416
  ]
  edge [
    source 80
    target 94
  ]
  edge [
    source 81
    target 290
  ]
  edge [
    source 81
    target 417
  ]
  edge [
    source 81
    target 418
  ]
  edge [
    source 81
    target 419
  ]
  edge [
    source 81
    target 420
  ]
  edge [
    source 81
    target 421
  ]
  edge [
    source 81
    target 422
  ]
  edge [
    source 81
    target 423
  ]
  edge [
    source 81
    target 424
  ]
  edge [
    source 81
    target 425
  ]
  edge [
    source 81
    target 426
  ]
  edge [
    source 81
    target 427
  ]
  edge [
    source 81
    target 428
  ]
  edge [
    source 81
    target 429
  ]
  edge [
    source 81
    target 430
  ]
  edge [
    source 81
    target 431
  ]
  edge [
    source 82
    target 83
  ]
  edge [
    source 82
    target 94
  ]
  edge [
    source 82
    target 95
  ]
  edge [
    source 82
    target 432
  ]
  edge [
    source 82
    target 433
  ]
  edge [
    source 82
    target 144
  ]
  edge [
    source 82
    target 434
  ]
  edge [
    source 82
    target 435
  ]
  edge [
    source 82
    target 436
  ]
  edge [
    source 82
    target 109
  ]
  edge [
    source 83
    target 129
  ]
  edge [
    source 83
    target 437
  ]
  edge [
    source 83
    target 438
  ]
  edge [
    source 83
    target 439
  ]
  edge [
    source 84
    target 85
  ]
  edge [
    source 84
    target 88
  ]
  edge [
    source 85
    target 86
  ]
  edge [
    source 85
    target 89
  ]
  edge [
    source 86
    target 440
  ]
  edge [
    source 86
    target 441
  ]
  edge [
    source 86
    target 442
  ]
  edge [
    source 86
    target 443
  ]
  edge [
    source 86
    target 444
  ]
  edge [
    source 86
    target 445
  ]
  edge [
    source 86
    target 446
  ]
  edge [
    source 86
    target 428
  ]
  edge [
    source 86
    target 447
  ]
  edge [
    source 86
    target 448
  ]
  edge [
    source 87
    target 88
  ]
  edge [
    source 87
    target 449
  ]
  edge [
    source 87
    target 450
  ]
  edge [
    source 87
    target 451
  ]
  edge [
    source 87
    target 255
  ]
  edge [
    source 87
    target 452
  ]
  edge [
    source 89
    target 90
  ]
  edge [
    source 89
    target 453
  ]
  edge [
    source 89
    target 454
  ]
  edge [
    source 89
    target 455
  ]
  edge [
    source 89
    target 456
  ]
  edge [
    source 89
    target 457
  ]
  edge [
    source 89
    target 458
  ]
  edge [
    source 89
    target 459
  ]
  edge [
    source 89
    target 460
  ]
  edge [
    source 90
    target 91
  ]
  edge [
    source 90
    target 111
  ]
  edge [
    source 90
    target 461
  ]
  edge [
    source 90
    target 462
  ]
  edge [
    source 90
    target 463
  ]
  edge [
    source 90
    target 464
  ]
  edge [
    source 90
    target 465
  ]
  edge [
    source 90
    target 117
  ]
  edge [
    source 90
    target 466
  ]
  edge [
    source 90
    target 467
  ]
  edge [
    source 90
    target 468
  ]
  edge [
    source 90
    target 469
  ]
  edge [
    source 90
    target 470
  ]
  edge [
    source 91
    target 92
  ]
  edge [
    source 91
    target 471
  ]
  edge [
    source 91
    target 472
  ]
  edge [
    source 92
    target 93
  ]
  edge [
    source 92
    target 473
  ]
  edge [
    source 92
    target 474
  ]
  edge [
    source 92
    target 475
  ]
  edge [
    source 92
    target 476
  ]
  edge [
    source 92
    target 477
  ]
  edge [
    source 92
    target 478
  ]
  edge [
    source 92
    target 479
  ]
  edge [
    source 92
    target 480
  ]
  edge [
    source 92
    target 481
  ]
  edge [
    source 92
    target 482
  ]
  edge [
    source 92
    target 483
  ]
  edge [
    source 92
    target 484
  ]
  edge [
    source 92
    target 485
  ]
  edge [
    source 92
    target 486
  ]
  edge [
    source 92
    target 487
  ]
  edge [
    source 92
    target 343
  ]
  edge [
    source 92
    target 488
  ]
  edge [
    source 92
    target 489
  ]
  edge [
    source 92
    target 162
  ]
  edge [
    source 92
    target 490
  ]
  edge [
    source 92
    target 491
  ]
  edge [
    source 92
    target 492
  ]
  edge [
    source 92
    target 493
  ]
  edge [
    source 92
    target 494
  ]
  edge [
    source 92
    target 495
  ]
  edge [
    source 92
    target 496
  ]
  edge [
    source 92
    target 497
  ]
  edge [
    source 92
    target 498
  ]
  edge [
    source 92
    target 499
  ]
  edge [
    source 92
    target 500
  ]
  edge [
    source 92
    target 501
  ]
  edge [
    source 92
    target 502
  ]
  edge [
    source 92
    target 503
  ]
  edge [
    source 92
    target 504
  ]
  edge [
    source 92
    target 505
  ]
  edge [
    source 92
    target 506
  ]
  edge [
    source 92
    target 507
  ]
  edge [
    source 92
    target 508
  ]
  edge [
    source 92
    target 509
  ]
  edge [
    source 92
    target 510
  ]
  edge [
    source 92
    target 511
  ]
  edge [
    source 93
    target 512
  ]
  edge [
    source 93
    target 513
  ]
  edge [
    source 93
    target 514
  ]
  edge [
    source 93
    target 515
  ]
  edge [
    source 93
    target 516
  ]
  edge [
    source 94
    target 517
  ]
  edge [
    source 94
    target 518
  ]
  edge [
    source 94
    target 519
  ]
  edge [
    source 94
    target 520
  ]
  edge [
    source 94
    target 521
  ]
  edge [
    source 95
    target 96
  ]
  edge [
    source 96
    target 97
  ]
  edge [
    source 96
    target 522
  ]
  edge [
    source 96
    target 523
  ]
  edge [
    source 96
    target 524
  ]
  edge [
    source 96
    target 525
  ]
  edge [
    source 96
    target 526
  ]
  edge [
    source 96
    target 527
  ]
  edge [
    source 96
    target 528
  ]
  edge [
    source 96
    target 529
  ]
  edge [
    source 96
    target 530
  ]
  edge [
    source 96
    target 531
  ]
  edge [
    source 97
    target 532
  ]
  edge [
    source 97
    target 427
  ]
  edge [
    source 97
    target 533
  ]
  edge [
    source 97
    target 534
  ]
  edge [
    source 97
    target 535
  ]
  edge [
    source 97
    target 536
  ]
  edge [
    source 97
    target 537
  ]
  edge [
    source 97
    target 103
  ]
  edge [
    source 98
    target 99
  ]
  edge [
    source 98
    target 538
  ]
  edge [
    source 98
    target 539
  ]
  edge [
    source 98
    target 540
  ]
  edge [
    source 98
    target 541
  ]
  edge [
    source 98
    target 476
  ]
  edge [
    source 98
    target 542
  ]
  edge [
    source 98
    target 543
  ]
  edge [
    source 98
    target 544
  ]
  edge [
    source 98
    target 545
  ]
  edge [
    source 98
    target 546
  ]
  edge [
    source 98
    target 547
  ]
  edge [
    source 98
    target 270
  ]
  edge [
    source 98
    target 548
  ]
  edge [
    source 98
    target 549
  ]
  edge [
    source 98
    target 550
  ]
  edge [
    source 98
    target 551
  ]
  edge [
    source 98
    target 552
  ]
  edge [
    source 98
    target 553
  ]
  edge [
    source 98
    target 554
  ]
  edge [
    source 98
    target 555
  ]
  edge [
    source 98
    target 556
  ]
  edge [
    source 98
    target 557
  ]
  edge [
    source 98
    target 558
  ]
  edge [
    source 98
    target 559
  ]
  edge [
    source 98
    target 560
  ]
  edge [
    source 98
    target 561
  ]
  edge [
    source 98
    target 562
  ]
  edge [
    source 98
    target 496
  ]
  edge [
    source 98
    target 563
  ]
  edge [
    source 98
    target 564
  ]
  edge [
    source 98
    target 565
  ]
  edge [
    source 98
    target 566
  ]
  edge [
    source 98
    target 567
  ]
  edge [
    source 98
    target 568
  ]
  edge [
    source 98
    target 569
  ]
  edge [
    source 98
    target 570
  ]
  edge [
    source 98
    target 571
  ]
  edge [
    source 98
    target 572
  ]
  edge [
    source 98
    target 573
  ]
  edge [
    source 98
    target 574
  ]
  edge [
    source 98
    target 575
  ]
  edge [
    source 98
    target 576
  ]
  edge [
    source 98
    target 577
  ]
  edge [
    source 98
    target 578
  ]
  edge [
    source 98
    target 579
  ]
  edge [
    source 98
    target 580
  ]
  edge [
    source 98
    target 581
  ]
  edge [
    source 98
    target 105
  ]
  edge [
    source 99
    target 582
  ]
  edge [
    source 99
    target 583
  ]
  edge [
    source 99
    target 584
  ]
  edge [
    source 100
    target 101
  ]
  edge [
    source 100
    target 585
  ]
  edge [
    source 100
    target 586
  ]
  edge [
    source 100
    target 587
  ]
  edge [
    source 100
    target 588
  ]
  edge [
    source 100
    target 589
  ]
  edge [
    source 100
    target 590
  ]
  edge [
    source 100
    target 207
  ]
  edge [
    source 101
    target 102
  ]
  edge [
    source 101
    target 591
  ]
  edge [
    source 101
    target 592
  ]
  edge [
    source 102
    target 103
  ]
  edge [
    source 104
    target 105
  ]
  edge [
    source 104
    target 593
  ]
  edge [
    source 104
    target 594
  ]
  edge [
    source 104
    target 595
  ]
  edge [
    source 104
    target 596
  ]
  edge [
    source 104
    target 597
  ]
  edge [
    source 104
    target 598
  ]
  edge [
    source 104
    target 599
  ]
  edge [
    source 104
    target 600
  ]
  edge [
    source 104
    target 601
  ]
  edge [
    source 104
    target 602
  ]
  edge [
    source 105
    target 106
  ]
  edge [
    source 105
    target 603
  ]
  edge [
    source 105
    target 604
  ]
  edge [
    source 105
    target 605
  ]
  edge [
    source 105
    target 606
  ]
  edge [
    source 106
    target 107
  ]
  edge [
    source 106
    target 242
  ]
  edge [
    source 106
    target 136
  ]
  edge [
    source 107
    target 108
  ]
  edge [
    source 107
    target 607
  ]
  edge [
    source 107
    target 608
  ]
  edge [
    source 107
    target 609
  ]
  edge [
    source 109
    target 138
  ]
  edge [
    source 109
    target 610
  ]
  edge [
    source 109
    target 611
  ]
  edge [
    source 109
    target 433
  ]
  edge [
    source 109
    target 612
  ]
  edge [
    source 109
    target 434
  ]
  edge [
    source 109
    target 613
  ]
  edge [
    source 109
    target 435
  ]
  edge [
    source 110
    target 614
  ]
  edge [
    source 110
    target 264
  ]
  edge [
    source 110
    target 615
  ]
  edge [
    source 110
    target 616
  ]
  edge [
    source 110
    target 617
  ]
  edge [
    source 110
    target 618
  ]
  edge [
    source 110
    target 241
  ]
  edge [
    source 110
    target 619
  ]
  edge [
    source 110
    target 620
  ]
  edge [
    source 110
    target 289
  ]
  edge [
    source 110
    target 621
  ]
  edge [
    source 110
    target 622
  ]
]
