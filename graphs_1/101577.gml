graph [
  maxDegree 38
  minDegree 1
  meanDegree 2.2560553633217992
  density 0.007833525567089581
  graphCliqueNumber 3
  node [
    id 0
    label "zacz&#261;&#263;"
    origin "text"
  ]
  node [
    id 1
    label "wszystko"
    origin "text"
  ]
  node [
    id 2
    label "dra&#380;ni&#263;"
    origin "text"
  ]
  node [
    id 3
    label "niewypowiedziany"
    origin "text"
  ]
  node [
    id 4
    label "spos&#243;b"
    origin "text"
  ]
  node [
    id 5
    label "zmieni&#263;"
    origin "text"
  ]
  node [
    id 6
    label "si&#281;"
    origin "text"
  ]
  node [
    id 7
    label "zblad&#322;y"
    origin "text"
  ]
  node [
    id 8
    label "czu&#322;y"
    origin "text"
  ]
  node [
    id 9
    label "chora"
    origin "text"
  ]
  node [
    id 10
    label "przebiega&#263;"
    origin "text"
  ]
  node [
    id 11
    label "nerwowy"
    origin "text"
  ]
  node [
    id 12
    label "dreszcz"
    origin "text"
  ]
  node [
    id 13
    label "s&#322;ysze&#263;"
    origin "text"
  ]
  node [
    id 14
    label "turkot"
    origin "text"
  ]
  node [
    id 15
    label "odje&#380;d&#380;a&#263;"
    origin "text"
  ]
  node [
    id 16
    label "doro&#380;ka"
    origin "text"
  ]
  node [
    id 17
    label "widzie&#263;"
    origin "text"
  ]
  node [
    id 18
    label "jak"
    origin "text"
  ]
  node [
    id 19
    label "opr&#243;&#380;nia&#263;"
    origin "text"
  ]
  node [
    id 20
    label "powoli"
    origin "text"
  ]
  node [
    id 21
    label "stolik"
    origin "text"
  ]
  node [
    id 22
    label "g&#322;os"
    origin "text"
  ]
  node [
    id 23
    label "porzyckiego"
    origin "text"
  ]
  node [
    id 24
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 25
    label "opowiada&#263;"
    origin "text"
  ]
  node [
    id 26
    label "seria"
    origin "text"
  ]
  node [
    id 27
    label "zakulisowy"
    origin "text"
  ]
  node [
    id 28
    label "anegdotka"
    origin "text"
  ]
  node [
    id 29
    label "ponad"
    origin "text"
  ]
  node [
    id 30
    label "g&#243;rowa&#263;"
    origin "text"
  ]
  node [
    id 31
    label "uczucie"
    origin "text"
  ]
  node [
    id 32
    label "odczucie"
    origin "text"
  ]
  node [
    id 33
    label "smutek"
    origin "text"
  ]
  node [
    id 34
    label "pewien"
    origin "text"
  ]
  node [
    id 35
    label "lito&#347;&#263;"
    origin "text"
  ]
  node [
    id 36
    label "jaki"
    origin "text"
  ]
  node [
    id 37
    label "promienie&#263;"
    origin "text"
  ]
  node [
    id 38
    label "wzrok"
    origin "text"
  ]
  node [
    id 39
    label "siedz&#261;cy"
    origin "text"
  ]
  node [
    id 40
    label "naprzeciw"
    origin "text"
  ]
  node [
    id 41
    label "aktorka"
    origin "text"
  ]
  node [
    id 42
    label "cause"
  ]
  node [
    id 43
    label "introduce"
  ]
  node [
    id 44
    label "begin"
  ]
  node [
    id 45
    label "odj&#261;&#263;"
  ]
  node [
    id 46
    label "post&#261;pi&#263;"
  ]
  node [
    id 47
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 48
    label "do"
  ]
  node [
    id 49
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 50
    label "zrobi&#263;"
  ]
  node [
    id 51
    label "lock"
  ]
  node [
    id 52
    label "absolut"
  ]
  node [
    id 53
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 54
    label "wywo&#322;ywa&#263;"
  ]
  node [
    id 55
    label "tease"
  ]
  node [
    id 56
    label "pobudza&#263;"
  ]
  node [
    id 57
    label "denerwowa&#263;"
  ]
  node [
    id 58
    label "wkurwia&#263;"
  ]
  node [
    id 59
    label "robi&#263;"
  ]
  node [
    id 60
    label "chafe"
  ]
  node [
    id 61
    label "powodowa&#263;"
  ]
  node [
    id 62
    label "displease"
  ]
  node [
    id 63
    label "wielki"
  ]
  node [
    id 64
    label "model"
  ]
  node [
    id 65
    label "zbi&#243;r"
  ]
  node [
    id 66
    label "tryb"
  ]
  node [
    id 67
    label "narz&#281;dzie"
  ]
  node [
    id 68
    label "nature"
  ]
  node [
    id 69
    label "come_up"
  ]
  node [
    id 70
    label "straci&#263;"
  ]
  node [
    id 71
    label "przej&#347;&#263;"
  ]
  node [
    id 72
    label "zast&#261;pi&#263;"
  ]
  node [
    id 73
    label "sprawi&#263;"
  ]
  node [
    id 74
    label "zyska&#263;"
  ]
  node [
    id 75
    label "change"
  ]
  node [
    id 76
    label "wyblak&#322;y"
  ]
  node [
    id 77
    label "przyblad&#322;y"
  ]
  node [
    id 78
    label "poblad&#322;y"
  ]
  node [
    id 79
    label "zmi&#281;kczenie"
  ]
  node [
    id 80
    label "zmi&#281;kczanie"
  ]
  node [
    id 81
    label "uczulenie"
  ]
  node [
    id 82
    label "wra&#380;liwy"
  ]
  node [
    id 83
    label "precyzyjny"
  ]
  node [
    id 84
    label "mi&#322;y"
  ]
  node [
    id 85
    label "uczulanie"
  ]
  node [
    id 86
    label "czule"
  ]
  node [
    id 87
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 88
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 89
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 90
    label "przebywa&#263;"
  ]
  node [
    id 91
    label "draw"
  ]
  node [
    id 92
    label "biec"
  ]
  node [
    id 93
    label "carry"
  ]
  node [
    id 94
    label "raptowny"
  ]
  node [
    id 95
    label "s&#322;aby"
  ]
  node [
    id 96
    label "niecierpliwy"
  ]
  node [
    id 97
    label "niespokojny"
  ]
  node [
    id 98
    label "fizjologiczny"
  ]
  node [
    id 99
    label "ruchliwy"
  ]
  node [
    id 100
    label "nerwowo"
  ]
  node [
    id 101
    label "dra&#380;liwy"
  ]
  node [
    id 102
    label "throb"
  ]
  node [
    id 103
    label "ci&#261;goty"
  ]
  node [
    id 104
    label "oznaka"
  ]
  node [
    id 105
    label "doznanie"
  ]
  node [
    id 106
    label "listen"
  ]
  node [
    id 107
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 108
    label "postrzega&#263;"
  ]
  node [
    id 109
    label "s&#322;ycha&#263;"
  ]
  node [
    id 110
    label "read"
  ]
  node [
    id 111
    label "d&#378;wi&#281;k"
  ]
  node [
    id 112
    label "bluster"
  ]
  node [
    id 113
    label "dystansowa&#263;"
  ]
  node [
    id 114
    label "pada&#263;"
  ]
  node [
    id 115
    label "skr&#281;ca&#263;"
  ]
  node [
    id 116
    label "reagowa&#263;"
  ]
  node [
    id 117
    label "jecha&#263;"
  ]
  node [
    id 118
    label "traci&#263;_panowanie_nad_sob&#261;"
  ]
  node [
    id 119
    label "odchodzi&#263;"
  ]
  node [
    id 120
    label "impart"
  ]
  node [
    id 121
    label "post&#281;powa&#263;"
  ]
  node [
    id 122
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 123
    label "odbija&#263;"
  ]
  node [
    id 124
    label "kursowa&#263;"
  ]
  node [
    id 125
    label "odsuwa&#263;_si&#281;"
  ]
  node [
    id 126
    label "s&#322;abn&#261;&#263;"
  ]
  node [
    id 127
    label "rusza&#263;"
  ]
  node [
    id 128
    label "dor&#243;&#380;ka"
  ]
  node [
    id 129
    label "pojazd_niemechaniczny"
  ]
  node [
    id 130
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 131
    label "perceive"
  ]
  node [
    id 132
    label "spowodowa&#263;"
  ]
  node [
    id 133
    label "male&#263;"
  ]
  node [
    id 134
    label "zmale&#263;"
  ]
  node [
    id 135
    label "spotka&#263;"
  ]
  node [
    id 136
    label "go_steady"
  ]
  node [
    id 137
    label "dostrzega&#263;"
  ]
  node [
    id 138
    label "zwi&#281;ksza&#263;_si&#281;"
  ]
  node [
    id 139
    label "zdawa&#263;_sobie_spraw&#281;"
  ]
  node [
    id 140
    label "ogl&#261;da&#263;"
  ]
  node [
    id 141
    label "os&#261;dza&#263;"
  ]
  node [
    id 142
    label "aprobowa&#263;"
  ]
  node [
    id 143
    label "punkt_widzenia"
  ]
  node [
    id 144
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 145
    label "notice"
  ]
  node [
    id 146
    label "byd&#322;o"
  ]
  node [
    id 147
    label "zobo"
  ]
  node [
    id 148
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 149
    label "yakalo"
  ]
  node [
    id 150
    label "dzo"
  ]
  node [
    id 151
    label "zu&#380;ywa&#263;"
  ]
  node [
    id 152
    label "wyjmowa&#263;"
  ]
  node [
    id 153
    label "authorize"
  ]
  node [
    id 154
    label "wolny"
  ]
  node [
    id 155
    label "stopniowo"
  ]
  node [
    id 156
    label "wolniej"
  ]
  node [
    id 157
    label "niespiesznie"
  ]
  node [
    id 158
    label "bezproblemowo"
  ]
  node [
    id 159
    label "spokojny"
  ]
  node [
    id 160
    label "bryd&#380;ysta"
  ]
  node [
    id 161
    label "grupa"
  ]
  node [
    id 162
    label "st&#243;&#322;"
  ]
  node [
    id 163
    label "blat"
  ]
  node [
    id 164
    label "partner"
  ]
  node [
    id 165
    label "posta&#263;"
  ]
  node [
    id 166
    label "czwarty"
  ]
  node [
    id 167
    label "opinion"
  ]
  node [
    id 168
    label "wypowied&#378;"
  ]
  node [
    id 169
    label "zmatowienie"
  ]
  node [
    id 170
    label "wpa&#347;&#263;"
  ]
  node [
    id 171
    label "wokal"
  ]
  node [
    id 172
    label "note"
  ]
  node [
    id 173
    label "wydawa&#263;"
  ]
  node [
    id 174
    label "nakaz"
  ]
  node [
    id 175
    label "regestr"
  ]
  node [
    id 176
    label "&#347;piewak_operowy"
  ]
  node [
    id 177
    label "matowie&#263;"
  ]
  node [
    id 178
    label "wpada&#263;"
  ]
  node [
    id 179
    label "stanowisko"
  ]
  node [
    id 180
    label "zjawisko"
  ]
  node [
    id 181
    label "mutacja"
  ]
  node [
    id 182
    label "partia"
  ]
  node [
    id 183
    label "&#347;piewak"
  ]
  node [
    id 184
    label "emisja"
  ]
  node [
    id 185
    label "brzmienie"
  ]
  node [
    id 186
    label "zmatowie&#263;"
  ]
  node [
    id 187
    label "wydanie"
  ]
  node [
    id 188
    label "zesp&#243;&#322;"
  ]
  node [
    id 189
    label "wyda&#263;"
  ]
  node [
    id 190
    label "zdolno&#347;&#263;"
  ]
  node [
    id 191
    label "decyzja"
  ]
  node [
    id 192
    label "wpadni&#281;cie"
  ]
  node [
    id 193
    label "linia_melodyczna"
  ]
  node [
    id 194
    label "wpadanie"
  ]
  node [
    id 195
    label "onomatopeja"
  ]
  node [
    id 196
    label "sound"
  ]
  node [
    id 197
    label "matowienie"
  ]
  node [
    id 198
    label "ch&#243;rzysta"
  ]
  node [
    id 199
    label "foniatra"
  ]
  node [
    id 200
    label "&#347;piewaczka"
  ]
  node [
    id 201
    label "relate"
  ]
  node [
    id 202
    label "przedstawia&#263;"
  ]
  node [
    id 203
    label "prawi&#263;"
  ]
  node [
    id 204
    label "stage_set"
  ]
  node [
    id 205
    label "przekr&#243;j_geologiczny"
  ]
  node [
    id 206
    label "sekwencja"
  ]
  node [
    id 207
    label "komplet"
  ]
  node [
    id 208
    label "przebieg"
  ]
  node [
    id 209
    label "zestawienie"
  ]
  node [
    id 210
    label "jednostka_systematyczna"
  ]
  node [
    id 211
    label "line"
  ]
  node [
    id 212
    label "produkcja"
  ]
  node [
    id 213
    label "set"
  ]
  node [
    id 214
    label "jednostka"
  ]
  node [
    id 215
    label "zakulisowo"
  ]
  node [
    id 216
    label "przedpokojowy"
  ]
  node [
    id 217
    label "nieoficjalny"
  ]
  node [
    id 218
    label "opowiadanie"
  ]
  node [
    id 219
    label "anecdote"
  ]
  node [
    id 220
    label "control"
  ]
  node [
    id 221
    label "undertaking"
  ]
  node [
    id 222
    label "wystawa&#263;"
  ]
  node [
    id 223
    label "sprout"
  ]
  node [
    id 224
    label "prowadzi&#263;"
  ]
  node [
    id 225
    label "zareagowanie"
  ]
  node [
    id 226
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 227
    label "opanowanie"
  ]
  node [
    id 228
    label "d&#322;awi&#263;"
  ]
  node [
    id 229
    label "os&#322;upienie"
  ]
  node [
    id 230
    label "zmys&#322;"
  ]
  node [
    id 231
    label "zaanga&#380;owanie"
  ]
  node [
    id 232
    label "smell"
  ]
  node [
    id 233
    label "zdarzenie_si&#281;"
  ]
  node [
    id 234
    label "ostygn&#261;&#263;"
  ]
  node [
    id 235
    label "afekt"
  ]
  node [
    id 236
    label "stan"
  ]
  node [
    id 237
    label "iskrzy&#263;"
  ]
  node [
    id 238
    label "afekcja"
  ]
  node [
    id 239
    label "przeczulica"
  ]
  node [
    id 240
    label "ow&#322;adni&#281;ty"
  ]
  node [
    id 241
    label "czucie"
  ]
  node [
    id 242
    label "emocja"
  ]
  node [
    id 243
    label "ogrom"
  ]
  node [
    id 244
    label "stygn&#261;&#263;"
  ]
  node [
    id 245
    label "poczucie"
  ]
  node [
    id 246
    label "temperatura"
  ]
  node [
    id 247
    label "feel"
  ]
  node [
    id 248
    label "reakcja"
  ]
  node [
    id 249
    label "odczucia"
  ]
  node [
    id 250
    label "postrze&#380;enie"
  ]
  node [
    id 251
    label "konsekwencja"
  ]
  node [
    id 252
    label "proces"
  ]
  node [
    id 253
    label "sm&#281;tek"
  ]
  node [
    id 254
    label "przep&#322;akanie"
  ]
  node [
    id 255
    label "przep&#322;akiwanie"
  ]
  node [
    id 256
    label "nastr&#243;j"
  ]
  node [
    id 257
    label "przep&#322;akiwa&#263;"
  ]
  node [
    id 258
    label "przep&#322;aka&#263;"
  ]
  node [
    id 259
    label "upewnienie_si&#281;"
  ]
  node [
    id 260
    label "wierzenie"
  ]
  node [
    id 261
    label "mo&#380;liwy"
  ]
  node [
    id 262
    label "ufanie"
  ]
  node [
    id 263
    label "jaki&#347;"
  ]
  node [
    id 264
    label "upewnianie_si&#281;"
  ]
  node [
    id 265
    label "po&#380;a&#322;owa&#263;"
  ]
  node [
    id 266
    label "po&#380;a&#322;owanie"
  ]
  node [
    id 267
    label "mi&#322;o&#347;&#263;_bli&#378;niego"
  ]
  node [
    id 268
    label "sympathy"
  ]
  node [
    id 269
    label "&#347;wieci&#263;"
  ]
  node [
    id 270
    label "tryska&#263;"
  ]
  node [
    id 271
    label "emanowa&#263;"
  ]
  node [
    id 272
    label "promieniowa&#263;"
  ]
  node [
    id 273
    label "gra&#263;"
  ]
  node [
    id 274
    label "widzenie"
  ]
  node [
    id 275
    label "oko"
  ]
  node [
    id 276
    label "expression"
  ]
  node [
    id 277
    label "czynno&#347;&#263;_wzrokowa"
  ]
  node [
    id 278
    label "zm&#261;cenie_si&#281;"
  ]
  node [
    id 279
    label "m&#281;tnienie"
  ]
  node [
    id 280
    label "m&#281;tnie&#263;"
  ]
  node [
    id 281
    label "kontakt"
  ]
  node [
    id 282
    label "okulista"
  ]
  node [
    id 283
    label "na_siedz&#261;co"
  ]
  node [
    id 284
    label "z_naprzeciwka"
  ]
  node [
    id 285
    label "wprost"
  ]
  node [
    id 286
    label "po_przeciwnej_stronie"
  ]
  node [
    id 287
    label "pracownik"
  ]
  node [
    id 288
    label "wykonawczyni"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 6
    target 9
  ]
  edge [
    source 6
    target 19
  ]
  edge [
    source 6
    target 20
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 10
  ]
  edge [
    source 7
    target 23
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 23
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 11
    target 98
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 11
    target 100
  ]
  edge [
    source 11
    target 101
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 21
  ]
  edge [
    source 13
    target 22
  ]
  edge [
    source 13
    target 106
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 111
  ]
  edge [
    source 14
    target 112
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 113
  ]
  edge [
    source 15
    target 114
  ]
  edge [
    source 15
    target 115
  ]
  edge [
    source 15
    target 116
  ]
  edge [
    source 15
    target 117
  ]
  edge [
    source 15
    target 118
  ]
  edge [
    source 15
    target 119
  ]
  edge [
    source 15
    target 120
  ]
  edge [
    source 15
    target 121
  ]
  edge [
    source 15
    target 122
  ]
  edge [
    source 15
    target 123
  ]
  edge [
    source 15
    target 124
  ]
  edge [
    source 15
    target 125
  ]
  edge [
    source 15
    target 126
  ]
  edge [
    source 15
    target 127
  ]
  edge [
    source 15
    target 25
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 128
  ]
  edge [
    source 16
    target 129
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 130
  ]
  edge [
    source 17
    target 131
  ]
  edge [
    source 17
    target 116
  ]
  edge [
    source 17
    target 132
  ]
  edge [
    source 17
    target 133
  ]
  edge [
    source 17
    target 134
  ]
  edge [
    source 17
    target 135
  ]
  edge [
    source 17
    target 136
  ]
  edge [
    source 17
    target 137
  ]
  edge [
    source 17
    target 138
  ]
  edge [
    source 17
    target 139
  ]
  edge [
    source 17
    target 140
  ]
  edge [
    source 17
    target 141
  ]
  edge [
    source 17
    target 142
  ]
  edge [
    source 17
    target 143
  ]
  edge [
    source 17
    target 144
  ]
  edge [
    source 17
    target 38
  ]
  edge [
    source 17
    target 108
  ]
  edge [
    source 17
    target 145
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 146
  ]
  edge [
    source 18
    target 147
  ]
  edge [
    source 18
    target 148
  ]
  edge [
    source 18
    target 149
  ]
  edge [
    source 18
    target 150
  ]
  edge [
    source 19
    target 151
  ]
  edge [
    source 19
    target 152
  ]
  edge [
    source 19
    target 153
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 154
  ]
  edge [
    source 20
    target 155
  ]
  edge [
    source 20
    target 156
  ]
  edge [
    source 20
    target 157
  ]
  edge [
    source 20
    target 158
  ]
  edge [
    source 20
    target 159
  ]
  edge [
    source 21
    target 160
  ]
  edge [
    source 21
    target 161
  ]
  edge [
    source 21
    target 162
  ]
  edge [
    source 21
    target 163
  ]
  edge [
    source 21
    target 164
  ]
  edge [
    source 21
    target 165
  ]
  edge [
    source 21
    target 166
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 167
  ]
  edge [
    source 22
    target 168
  ]
  edge [
    source 22
    target 169
  ]
  edge [
    source 22
    target 170
  ]
  edge [
    source 22
    target 161
  ]
  edge [
    source 22
    target 171
  ]
  edge [
    source 22
    target 172
  ]
  edge [
    source 22
    target 173
  ]
  edge [
    source 22
    target 174
  ]
  edge [
    source 22
    target 175
  ]
  edge [
    source 22
    target 176
  ]
  edge [
    source 22
    target 177
  ]
  edge [
    source 22
    target 178
  ]
  edge [
    source 22
    target 179
  ]
  edge [
    source 22
    target 180
  ]
  edge [
    source 22
    target 181
  ]
  edge [
    source 22
    target 182
  ]
  edge [
    source 22
    target 183
  ]
  edge [
    source 22
    target 184
  ]
  edge [
    source 22
    target 185
  ]
  edge [
    source 22
    target 186
  ]
  edge [
    source 22
    target 187
  ]
  edge [
    source 22
    target 188
  ]
  edge [
    source 22
    target 189
  ]
  edge [
    source 22
    target 190
  ]
  edge [
    source 22
    target 191
  ]
  edge [
    source 22
    target 192
  ]
  edge [
    source 22
    target 193
  ]
  edge [
    source 22
    target 194
  ]
  edge [
    source 22
    target 195
  ]
  edge [
    source 22
    target 196
  ]
  edge [
    source 22
    target 197
  ]
  edge [
    source 22
    target 198
  ]
  edge [
    source 22
    target 111
  ]
  edge [
    source 22
    target 199
  ]
  edge [
    source 22
    target 200
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 201
  ]
  edge [
    source 25
    target 202
  ]
  edge [
    source 25
    target 203
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 204
  ]
  edge [
    source 26
    target 182
  ]
  edge [
    source 26
    target 205
  ]
  edge [
    source 26
    target 206
  ]
  edge [
    source 26
    target 207
  ]
  edge [
    source 26
    target 208
  ]
  edge [
    source 26
    target 209
  ]
  edge [
    source 26
    target 210
  ]
  edge [
    source 26
    target 111
  ]
  edge [
    source 26
    target 211
  ]
  edge [
    source 26
    target 65
  ]
  edge [
    source 26
    target 212
  ]
  edge [
    source 26
    target 213
  ]
  edge [
    source 26
    target 214
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 215
  ]
  edge [
    source 27
    target 216
  ]
  edge [
    source 27
    target 217
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 218
  ]
  edge [
    source 28
    target 219
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 220
  ]
  edge [
    source 30
    target 221
  ]
  edge [
    source 30
    target 222
  ]
  edge [
    source 30
    target 223
  ]
  edge [
    source 30
    target 224
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 225
  ]
  edge [
    source 31
    target 170
  ]
  edge [
    source 31
    target 226
  ]
  edge [
    source 31
    target 227
  ]
  edge [
    source 31
    target 228
  ]
  edge [
    source 31
    target 178
  ]
  edge [
    source 31
    target 229
  ]
  edge [
    source 31
    target 230
  ]
  edge [
    source 31
    target 231
  ]
  edge [
    source 31
    target 232
  ]
  edge [
    source 31
    target 233
  ]
  edge [
    source 31
    target 234
  ]
  edge [
    source 31
    target 235
  ]
  edge [
    source 31
    target 236
  ]
  edge [
    source 31
    target 237
  ]
  edge [
    source 31
    target 238
  ]
  edge [
    source 31
    target 239
  ]
  edge [
    source 31
    target 240
  ]
  edge [
    source 31
    target 241
  ]
  edge [
    source 31
    target 105
  ]
  edge [
    source 31
    target 242
  ]
  edge [
    source 31
    target 243
  ]
  edge [
    source 31
    target 244
  ]
  edge [
    source 31
    target 245
  ]
  edge [
    source 31
    target 246
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 247
  ]
  edge [
    source 32
    target 248
  ]
  edge [
    source 32
    target 249
  ]
  edge [
    source 32
    target 225
  ]
  edge [
    source 32
    target 250
  ]
  edge [
    source 32
    target 227
  ]
  edge [
    source 32
    target 105
  ]
  edge [
    source 32
    target 241
  ]
  edge [
    source 32
    target 251
  ]
  edge [
    source 32
    target 245
  ]
  edge [
    source 32
    target 229
  ]
  edge [
    source 32
    target 239
  ]
  edge [
    source 32
    target 230
  ]
  edge [
    source 32
    target 252
  ]
  edge [
    source 32
    target 233
  ]
  edge [
    source 32
    target 232
  ]
  edge [
    source 32
    target 180
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 253
  ]
  edge [
    source 33
    target 242
  ]
  edge [
    source 33
    target 254
  ]
  edge [
    source 33
    target 255
  ]
  edge [
    source 33
    target 256
  ]
  edge [
    source 33
    target 257
  ]
  edge [
    source 33
    target 258
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 259
  ]
  edge [
    source 34
    target 260
  ]
  edge [
    source 34
    target 261
  ]
  edge [
    source 34
    target 262
  ]
  edge [
    source 34
    target 263
  ]
  edge [
    source 34
    target 159
  ]
  edge [
    source 34
    target 264
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 242
  ]
  edge [
    source 35
    target 265
  ]
  edge [
    source 35
    target 266
  ]
  edge [
    source 35
    target 267
  ]
  edge [
    source 35
    target 268
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 269
  ]
  edge [
    source 37
    target 270
  ]
  edge [
    source 37
    target 271
  ]
  edge [
    source 37
    target 272
  ]
  edge [
    source 37
    target 273
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 274
  ]
  edge [
    source 38
    target 275
  ]
  edge [
    source 38
    target 276
  ]
  edge [
    source 38
    target 277
  ]
  edge [
    source 38
    target 278
  ]
  edge [
    source 38
    target 279
  ]
  edge [
    source 38
    target 230
  ]
  edge [
    source 38
    target 280
  ]
  edge [
    source 38
    target 281
  ]
  edge [
    source 38
    target 282
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 283
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 284
  ]
  edge [
    source 40
    target 285
  ]
  edge [
    source 40
    target 286
  ]
  edge [
    source 41
    target 287
  ]
  edge [
    source 41
    target 288
  ]
]
