graph [
  maxDegree 10
  minDegree 1
  meanDegree 1.8823529411764706
  density 0.0570409982174688
  graphCliqueNumber 2
  node [
    id 0
    label "plakat"
    origin "text"
  ]
  node [
    id 1
    label "podobizna"
    origin "text"
  ]
  node [
    id 2
    label "wisie&#263;"
    origin "text"
  ]
  node [
    id 3
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 4
    label "osiedlowy"
    origin "text"
  ]
  node [
    id 5
    label "si&#322;ownia"
    origin "text"
  ]
  node [
    id 6
    label "bill"
  ]
  node [
    id 7
    label "reklama"
  ]
  node [
    id 8
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 9
    label "przedstawienie"
  ]
  node [
    id 10
    label "wytw&#243;r"
  ]
  node [
    id 11
    label "obraz"
  ]
  node [
    id 12
    label "likeness"
  ]
  node [
    id 13
    label "dzie&#322;o"
  ]
  node [
    id 14
    label "konterfekt"
  ]
  node [
    id 15
    label "posta&#263;"
  ]
  node [
    id 16
    label "trwa&#263;"
  ]
  node [
    id 17
    label "by&#263;"
  ]
  node [
    id 18
    label "bent"
  ]
  node [
    id 19
    label "dynda&#263;"
  ]
  node [
    id 20
    label "zagra&#380;a&#263;"
  ]
  node [
    id 21
    label "m&#281;czy&#263;"
  ]
  node [
    id 22
    label "ci&#261;&#380;y&#263;"
  ]
  node [
    id 23
    label "unosi&#263;_si&#281;"
  ]
  node [
    id 24
    label "jaki&#347;"
  ]
  node [
    id 25
    label "lokalny"
  ]
  node [
    id 26
    label "pakowa&#263;"
  ]
  node [
    id 27
    label "kulturysta"
  ]
  node [
    id 28
    label "obiekt"
  ]
  node [
    id 29
    label "budowla"
  ]
  node [
    id 30
    label "zak&#322;ad_komunalny"
  ]
  node [
    id 31
    label "pakernia"
  ]
  node [
    id 32
    label "Ronnie"
  ]
  node [
    id 33
    label "Coleman"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 32
    target 33
  ]
]
