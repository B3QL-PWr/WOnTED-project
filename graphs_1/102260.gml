graph [
  maxDegree 18
  minDegree 1
  meanDegree 1.974025974025974
  density 0.025974025974025976
  graphCliqueNumber 2
  node [
    id 0
    label "henr"
    origin "text"
  ]
  node [
    id 1
    label "chesbrough"
    origin "text"
  ]
  node [
    id 2
    label "autor"
    origin "text"
  ]
  node [
    id 3
    label "popularny"
    origin "text"
  ]
  node [
    id 4
    label "ksi&#261;&#380;ka"
    origin "text"
  ]
  node [
    id 5
    label "otwarty"
    origin "text"
  ]
  node [
    id 6
    label "innowacyjno&#347;&#263;"
    origin "text"
  ]
  node [
    id 7
    label "pisz"
    origin "text"
  ]
  node [
    id 8
    label "businessweek"
    origin "text"
  ]
  node [
    id 9
    label "microsoft"
    origin "text"
  ]
  node [
    id 10
    label "powinien"
    origin "text"
  ]
  node [
    id 11
    label "cieszy&#263;"
    origin "text"
  ]
  node [
    id 12
    label "si&#281;"
    origin "text"
  ]
  node [
    id 13
    label "piractwo"
    origin "text"
  ]
  node [
    id 14
    label "chiny"
    origin "text"
  ]
  node [
    id 15
    label "indie"
    origin "text"
  ]
  node [
    id 16
    label "jednostka_indukcyjno&#347;ci"
  ]
  node [
    id 17
    label "milihenr"
  ]
  node [
    id 18
    label "pomys&#322;odawca"
  ]
  node [
    id 19
    label "kszta&#322;ciciel"
  ]
  node [
    id 20
    label "tworzyciel"
  ]
  node [
    id 21
    label "&#347;w"
  ]
  node [
    id 22
    label "wykonawca"
  ]
  node [
    id 23
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 24
    label "przyst&#281;pny"
  ]
  node [
    id 25
    label "&#322;atwy"
  ]
  node [
    id 26
    label "popularnie"
  ]
  node [
    id 27
    label "znany"
  ]
  node [
    id 28
    label "ok&#322;adka"
  ]
  node [
    id 29
    label "zak&#322;adka"
  ]
  node [
    id 30
    label "ekslibris"
  ]
  node [
    id 31
    label "wk&#322;ad"
  ]
  node [
    id 32
    label "przek&#322;adacz"
  ]
  node [
    id 33
    label "wydawnictwo"
  ]
  node [
    id 34
    label "grzbiet_ksi&#261;&#380;ki"
  ]
  node [
    id 35
    label "tytu&#322;"
  ]
  node [
    id 36
    label "bibliofilstwo"
  ]
  node [
    id 37
    label "falc"
  ]
  node [
    id 38
    label "nomina&#322;"
  ]
  node [
    id 39
    label "pagina"
  ]
  node [
    id 40
    label "rozdzia&#322;"
  ]
  node [
    id 41
    label "egzemplarz"
  ]
  node [
    id 42
    label "zw&#243;j"
  ]
  node [
    id 43
    label "tekst"
  ]
  node [
    id 44
    label "ewidentny"
  ]
  node [
    id 45
    label "bezpo&#347;redni"
  ]
  node [
    id 46
    label "otwarcie"
  ]
  node [
    id 47
    label "nieograniczony"
  ]
  node [
    id 48
    label "zdecydowany"
  ]
  node [
    id 49
    label "gotowy"
  ]
  node [
    id 50
    label "aktualny"
  ]
  node [
    id 51
    label "prostoduszny"
  ]
  node [
    id 52
    label "jawnie"
  ]
  node [
    id 53
    label "otworzysty"
  ]
  node [
    id 54
    label "dost&#281;pny"
  ]
  node [
    id 55
    label "publiczny"
  ]
  node [
    id 56
    label "aktywny"
  ]
  node [
    id 57
    label "kontaktowy"
  ]
  node [
    id 58
    label "oryginalno&#347;&#263;"
  ]
  node [
    id 59
    label "musie&#263;"
  ]
  node [
    id 60
    label "due"
  ]
  node [
    id 61
    label "amuse"
  ]
  node [
    id 62
    label "pie&#347;ci&#263;"
  ]
  node [
    id 63
    label "nape&#322;nia&#263;"
  ]
  node [
    id 64
    label "wzbudza&#263;"
  ]
  node [
    id 65
    label "raczy&#263;"
  ]
  node [
    id 66
    label "porusza&#263;"
  ]
  node [
    id 67
    label "szczerzy&#263;"
  ]
  node [
    id 68
    label "zaspokaja&#263;"
  ]
  node [
    id 69
    label "bootleg"
  ]
  node [
    id 70
    label "przest&#281;pstwo"
  ]
  node [
    id 71
    label "plagiarism"
  ]
  node [
    id 72
    label "nielegalno&#347;&#263;"
  ]
  node [
    id 73
    label "bandytyzm"
  ]
  node [
    id 74
    label "terroryzm"
  ]
  node [
    id 75
    label "transmisja"
  ]
  node [
    id 76
    label "reprodukcja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 10
    target 60
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 61
  ]
  edge [
    source 11
    target 62
  ]
  edge [
    source 11
    target 63
  ]
  edge [
    source 11
    target 64
  ]
  edge [
    source 11
    target 65
  ]
  edge [
    source 11
    target 66
  ]
  edge [
    source 11
    target 67
  ]
  edge [
    source 11
    target 68
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 69
  ]
  edge [
    source 13
    target 70
  ]
  edge [
    source 13
    target 71
  ]
  edge [
    source 13
    target 72
  ]
  edge [
    source 13
    target 73
  ]
  edge [
    source 13
    target 74
  ]
  edge [
    source 13
    target 75
  ]
  edge [
    source 13
    target 76
  ]
  edge [
    source 14
    target 15
  ]
]
