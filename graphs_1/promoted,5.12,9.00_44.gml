graph [
  maxDegree 15
  minDegree 1
  meanDegree 1.9583333333333333
  density 0.041666666666666664
  graphCliqueNumber 2
  node [
    id 0
    label "choinkowy"
    origin "text"
  ]
  node [
    id 1
    label "rozdajo"
    origin "text"
  ]
  node [
    id 2
    label "staj"
    origin "text"
  ]
  node [
    id 3
    label "si&#281;"
    origin "text"
  ]
  node [
    id 4
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 5
    label "tradycja"
    origin "text"
  ]
  node [
    id 6
    label "rok"
    origin "text"
  ]
  node [
    id 7
    label "mama"
    origin "text"
  ]
  node [
    id 8
    label "dwa"
    origin "text"
  ]
  node [
    id 9
    label "okazja"
    origin "text"
  ]
  node [
    id 10
    label "choinkowo"
  ]
  node [
    id 11
    label "objawienie"
  ]
  node [
    id 12
    label "obyczajowo&#347;&#263;"
  ]
  node [
    id 13
    label "staro&#347;cina_weselna"
  ]
  node [
    id 14
    label "kultura"
  ]
  node [
    id 15
    label "zwyczaj"
  ]
  node [
    id 16
    label "folklor"
  ]
  node [
    id 17
    label "stulecie"
  ]
  node [
    id 18
    label "kalendarz"
  ]
  node [
    id 19
    label "czas"
  ]
  node [
    id 20
    label "pora_roku"
  ]
  node [
    id 21
    label "cykl_astronomiczny"
  ]
  node [
    id 22
    label "p&#243;&#322;rocze"
  ]
  node [
    id 23
    label "grupa"
  ]
  node [
    id 24
    label "kwarta&#322;"
  ]
  node [
    id 25
    label "kurs"
  ]
  node [
    id 26
    label "jubileusz"
  ]
  node [
    id 27
    label "miesi&#261;c"
  ]
  node [
    id 28
    label "lata"
  ]
  node [
    id 29
    label "martwy_sezon"
  ]
  node [
    id 30
    label "matczysko"
  ]
  node [
    id 31
    label "macierz"
  ]
  node [
    id 32
    label "przodkini"
  ]
  node [
    id 33
    label "Matka_Boska"
  ]
  node [
    id 34
    label "macocha"
  ]
  node [
    id 35
    label "matka_zast&#281;pcza"
  ]
  node [
    id 36
    label "stara"
  ]
  node [
    id 37
    label "rodzice"
  ]
  node [
    id 38
    label "rodzic"
  ]
  node [
    id 39
    label "atrakcyjny"
  ]
  node [
    id 40
    label "oferta"
  ]
  node [
    id 41
    label "adeptness"
  ]
  node [
    id 42
    label "okazka"
  ]
  node [
    id 43
    label "wydarzenie"
  ]
  node [
    id 44
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 45
    label "podw&#243;zka"
  ]
  node [
    id 46
    label "autostop"
  ]
  node [
    id 47
    label "sytuacja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 11
  ]
  edge [
    source 5
    target 12
  ]
  edge [
    source 5
    target 13
  ]
  edge [
    source 5
    target 14
  ]
  edge [
    source 5
    target 15
  ]
  edge [
    source 5
    target 16
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 17
  ]
  edge [
    source 6
    target 18
  ]
  edge [
    source 6
    target 19
  ]
  edge [
    source 6
    target 20
  ]
  edge [
    source 6
    target 21
  ]
  edge [
    source 6
    target 22
  ]
  edge [
    source 6
    target 23
  ]
  edge [
    source 6
    target 24
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 30
  ]
  edge [
    source 7
    target 31
  ]
  edge [
    source 7
    target 32
  ]
  edge [
    source 7
    target 33
  ]
  edge [
    source 7
    target 34
  ]
  edge [
    source 7
    target 35
  ]
  edge [
    source 7
    target 36
  ]
  edge [
    source 7
    target 37
  ]
  edge [
    source 7
    target 38
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 39
  ]
  edge [
    source 9
    target 40
  ]
  edge [
    source 9
    target 41
  ]
  edge [
    source 9
    target 42
  ]
  edge [
    source 9
    target 43
  ]
  edge [
    source 9
    target 44
  ]
  edge [
    source 9
    target 45
  ]
  edge [
    source 9
    target 46
  ]
  edge [
    source 9
    target 47
  ]
]
