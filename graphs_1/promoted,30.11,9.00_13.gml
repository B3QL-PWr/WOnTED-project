graph [
  maxDegree 28
  minDegree 1
  meanDegree 1.9615384615384615
  density 0.038461538461538464
  graphCliqueNumber 2
  node [
    id 0
    label "poleca&#263;"
    origin "text"
  ]
  node [
    id 1
    label "ca&#322;y"
    origin "text"
  ]
  node [
    id 2
    label "kana&#322;"
    origin "text"
  ]
  node [
    id 3
    label "bardzo"
    origin "text"
  ]
  node [
    id 4
    label "relaksuj&#261;cy"
    origin "text"
  ]
  node [
    id 5
    label "control"
  ]
  node [
    id 6
    label "placard"
  ]
  node [
    id 7
    label "m&#243;wi&#263;"
  ]
  node [
    id 8
    label "charge"
  ]
  node [
    id 9
    label "zadawa&#263;"
  ]
  node [
    id 10
    label "ordynowa&#263;"
  ]
  node [
    id 11
    label "powierza&#263;"
  ]
  node [
    id 12
    label "wydawa&#263;"
  ]
  node [
    id 13
    label "doradza&#263;"
  ]
  node [
    id 14
    label "du&#380;y"
  ]
  node [
    id 15
    label "jedyny"
  ]
  node [
    id 16
    label "kompletny"
  ]
  node [
    id 17
    label "zdr&#243;w"
  ]
  node [
    id 18
    label "&#380;ywy"
  ]
  node [
    id 19
    label "ca&#322;o"
  ]
  node [
    id 20
    label "pe&#322;ny"
  ]
  node [
    id 21
    label "calu&#347;ko"
  ]
  node [
    id 22
    label "podobny"
  ]
  node [
    id 23
    label "chody"
  ]
  node [
    id 24
    label "szaniec"
  ]
  node [
    id 25
    label "gara&#380;"
  ]
  node [
    id 26
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 27
    label "tarapaty"
  ]
  node [
    id 28
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 29
    label "budowa"
  ]
  node [
    id 30
    label "pit"
  ]
  node [
    id 31
    label "ciek"
  ]
  node [
    id 32
    label "spos&#243;b"
  ]
  node [
    id 33
    label "odwa&#322;"
  ]
  node [
    id 34
    label "bystrza"
  ]
  node [
    id 35
    label "teatr"
  ]
  node [
    id 36
    label "piaskownik"
  ]
  node [
    id 37
    label "zrzutowy"
  ]
  node [
    id 38
    label "przew&#243;d"
  ]
  node [
    id 39
    label "syfon"
  ]
  node [
    id 40
    label "klarownia"
  ]
  node [
    id 41
    label "miejsce"
  ]
  node [
    id 42
    label "topologia_magistrali"
  ]
  node [
    id 43
    label "struktura_anatomiczna"
  ]
  node [
    id 44
    label "odk&#322;ad"
  ]
  node [
    id 45
    label "kanalizacja"
  ]
  node [
    id 46
    label "urz&#261;dzenie"
  ]
  node [
    id 47
    label "warsztat"
  ]
  node [
    id 48
    label "grodzisko"
  ]
  node [
    id 49
    label "w_chuj"
  ]
  node [
    id 50
    label "relaksowo"
  ]
  node [
    id 51
    label "uspokajaj&#261;cy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
]
