graph [
  maxDegree 7
  minDegree 1
  meanDegree 1.8666666666666667
  density 0.13333333333333333
  graphCliqueNumber 2
  node [
    id 0
    label "ocena"
    origin "text"
  ]
  node [
    id 1
    label "ratingowy"
    origin "text"
  ]
  node [
    id 2
    label "pozna&#324;"
    origin "text"
  ]
  node [
    id 3
    label "potwierdzi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "informacja"
  ]
  node [
    id 5
    label "sofcik"
  ]
  node [
    id 6
    label "appraisal"
  ]
  node [
    id 7
    label "decyzja"
  ]
  node [
    id 8
    label "pogl&#261;d"
  ]
  node [
    id 9
    label "kryterium"
  ]
  node [
    id 10
    label "stwierdzi&#263;"
  ]
  node [
    id 11
    label "acknowledge"
  ]
  node [
    id 12
    label "przy&#347;wiadczy&#263;"
  ]
  node [
    id 13
    label "zgodzi&#263;_si&#281;"
  ]
  node [
    id 14
    label "attest"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
]
