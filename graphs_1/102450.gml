graph [
  maxDegree 29
  minDegree 1
  meanDegree 2.046153846153846
  density 0.01586165772212284
  graphCliqueNumber 3
  node [
    id 0
    label "nowa"
    origin "text"
  ]
  node [
    id 1
    label "wydanie"
    origin "text"
  ]
  node [
    id 2
    label "internetowy"
    origin "text"
  ]
  node [
    id 3
    label "pismo"
    origin "text"
  ]
  node [
    id 4
    label "naukowy"
    origin "text"
  ]
  node [
    id 5
    label "po&#347;wi&#281;cony"
    origin "text"
  ]
  node [
    id 6
    label "by&#263;"
    origin "text"
  ]
  node [
    id 7
    label "ludologii"
    origin "text"
  ]
  node [
    id 8
    label "czyli"
    origin "text"
  ]
  node [
    id 9
    label "badanie"
    origin "text"
  ]
  node [
    id 10
    label "gra"
    origin "text"
  ]
  node [
    id 11
    label "gwiazda"
  ]
  node [
    id 12
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 13
    label "delivery"
  ]
  node [
    id 14
    label "podanie"
  ]
  node [
    id 15
    label "issue"
  ]
  node [
    id 16
    label "danie"
  ]
  node [
    id 17
    label "rendition"
  ]
  node [
    id 18
    label "egzemplarz"
  ]
  node [
    id 19
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 20
    label "impression"
  ]
  node [
    id 21
    label "odmiana"
  ]
  node [
    id 22
    label "zapach"
  ]
  node [
    id 23
    label "wytworzenie"
  ]
  node [
    id 24
    label "wprowadzenie"
  ]
  node [
    id 25
    label "zdarzenie_si&#281;"
  ]
  node [
    id 26
    label "zrobienie"
  ]
  node [
    id 27
    label "ujawnienie"
  ]
  node [
    id 28
    label "reszta"
  ]
  node [
    id 29
    label "ukazanie_si&#281;_drukiem"
  ]
  node [
    id 30
    label "zadenuncjowanie"
  ]
  node [
    id 31
    label "czasopismo"
  ]
  node [
    id 32
    label "urz&#261;dzenie"
  ]
  node [
    id 33
    label "d&#378;wi&#281;k"
  ]
  node [
    id 34
    label "publikacja"
  ]
  node [
    id 35
    label "nowoczesny"
  ]
  node [
    id 36
    label "elektroniczny"
  ]
  node [
    id 37
    label "sieciowo"
  ]
  node [
    id 38
    label "netowy"
  ]
  node [
    id 39
    label "internetowo"
  ]
  node [
    id 40
    label "paleograf"
  ]
  node [
    id 41
    label "list"
  ]
  node [
    id 42
    label "komunikacja"
  ]
  node [
    id 43
    label "psychotest"
  ]
  node [
    id 44
    label "ortografia"
  ]
  node [
    id 45
    label "handwriting"
  ]
  node [
    id 46
    label "grafia"
  ]
  node [
    id 47
    label "prasa"
  ]
  node [
    id 48
    label "j&#281;zyk"
  ]
  node [
    id 49
    label "adres"
  ]
  node [
    id 50
    label "script"
  ]
  node [
    id 51
    label "dzia&#322;"
  ]
  node [
    id 52
    label "paleografia"
  ]
  node [
    id 53
    label "Zwrotnica"
  ]
  node [
    id 54
    label "wk&#322;ad"
  ]
  node [
    id 55
    label "cecha"
  ]
  node [
    id 56
    label "przekaz"
  ]
  node [
    id 57
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 58
    label "interpunkcja"
  ]
  node [
    id 59
    label "communication"
  ]
  node [
    id 60
    label "dokument"
  ]
  node [
    id 61
    label "ok&#322;adka"
  ]
  node [
    id 62
    label "dzie&#322;o"
  ]
  node [
    id 63
    label "letter"
  ]
  node [
    id 64
    label "zajawka"
  ]
  node [
    id 65
    label "specjalny"
  ]
  node [
    id 66
    label "edukacyjnie"
  ]
  node [
    id 67
    label "intelektualny"
  ]
  node [
    id 68
    label "skomplikowany"
  ]
  node [
    id 69
    label "zgodny"
  ]
  node [
    id 70
    label "naukowo"
  ]
  node [
    id 71
    label "scjentyficzny"
  ]
  node [
    id 72
    label "teoretyczny"
  ]
  node [
    id 73
    label "specjalistyczny"
  ]
  node [
    id 74
    label "oddany"
  ]
  node [
    id 75
    label "si&#281;ga&#263;"
  ]
  node [
    id 76
    label "trwa&#263;"
  ]
  node [
    id 77
    label "obecno&#347;&#263;"
  ]
  node [
    id 78
    label "stan"
  ]
  node [
    id 79
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 80
    label "stand"
  ]
  node [
    id 81
    label "mie&#263;_miejsce"
  ]
  node [
    id 82
    label "uczestniczy&#263;"
  ]
  node [
    id 83
    label "chodzi&#263;"
  ]
  node [
    id 84
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 85
    label "equal"
  ]
  node [
    id 86
    label "usi&#322;owanie"
  ]
  node [
    id 87
    label "examination"
  ]
  node [
    id 88
    label "investigation"
  ]
  node [
    id 89
    label "ustalenie"
  ]
  node [
    id 90
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 91
    label "ustalanie"
  ]
  node [
    id 92
    label "bia&#322;a_niedziela"
  ]
  node [
    id 93
    label "analysis"
  ]
  node [
    id 94
    label "rozpatrywanie"
  ]
  node [
    id 95
    label "wziernikowanie"
  ]
  node [
    id 96
    label "obserwowanie"
  ]
  node [
    id 97
    label "omawianie"
  ]
  node [
    id 98
    label "sprawdzanie"
  ]
  node [
    id 99
    label "udowadnianie"
  ]
  node [
    id 100
    label "diagnostyka"
  ]
  node [
    id 101
    label "czynno&#347;&#263;"
  ]
  node [
    id 102
    label "macanie"
  ]
  node [
    id 103
    label "rektalny"
  ]
  node [
    id 104
    label "penetrowanie"
  ]
  node [
    id 105
    label "krytykowanie"
  ]
  node [
    id 106
    label "kontrola"
  ]
  node [
    id 107
    label "dociekanie"
  ]
  node [
    id 108
    label "zrecenzowanie"
  ]
  node [
    id 109
    label "praca"
  ]
  node [
    id 110
    label "rezultat"
  ]
  node [
    id 111
    label "zabawa"
  ]
  node [
    id 112
    label "rywalizacja"
  ]
  node [
    id 113
    label "Pok&#233;mon"
  ]
  node [
    id 114
    label "synteza"
  ]
  node [
    id 115
    label "odtworzenie"
  ]
  node [
    id 116
    label "komplet"
  ]
  node [
    id 117
    label "rekwizyt_do_gry"
  ]
  node [
    id 118
    label "odg&#322;os"
  ]
  node [
    id 119
    label "rozgrywka"
  ]
  node [
    id 120
    label "post&#281;powanie"
  ]
  node [
    id 121
    label "wydarzenie"
  ]
  node [
    id 122
    label "apparent_motion"
  ]
  node [
    id 123
    label "game"
  ]
  node [
    id 124
    label "zmienno&#347;&#263;"
  ]
  node [
    id 125
    label "zasada"
  ]
  node [
    id 126
    label "akcja"
  ]
  node [
    id 127
    label "play"
  ]
  node [
    id 128
    label "contest"
  ]
  node [
    id 129
    label "zbijany"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 9
    target 100
  ]
  edge [
    source 9
    target 101
  ]
  edge [
    source 9
    target 102
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 104
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 10
    target 111
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 10
    target 121
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 10
    target 125
  ]
  edge [
    source 10
    target 126
  ]
  edge [
    source 10
    target 127
  ]
  edge [
    source 10
    target 128
  ]
  edge [
    source 10
    target 129
  ]
]
