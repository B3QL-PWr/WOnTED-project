graph [
  maxDegree 15
  minDegree 1
  meanDegree 1.9692307692307693
  density 0.03076923076923077
  graphCliqueNumber 2
  node [
    id 0
    label "lekarka"
    origin "text"
  ]
  node [
    id 1
    label "linares"
    origin "text"
  ]
  node [
    id 2
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 3
    label "zaatakowa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "przez"
    origin "text"
  ]
  node [
    id 5
    label "nielegalny"
    origin "text"
  ]
  node [
    id 6
    label "migrant"
    origin "text"
  ]
  node [
    id 7
    label "pakistan"
    origin "text"
  ]
  node [
    id 8
    label "pr&#243;bowa&#263;"
    origin "text"
  ]
  node [
    id 9
    label "udzieli&#263;"
    origin "text"
  ]
  node [
    id 10
    label "pomoc"
    origin "text"
  ]
  node [
    id 11
    label "proceed"
  ]
  node [
    id 12
    label "catch"
  ]
  node [
    id 13
    label "pozosta&#263;"
  ]
  node [
    id 14
    label "osta&#263;_si&#281;"
  ]
  node [
    id 15
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 16
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 17
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 18
    label "change"
  ]
  node [
    id 19
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 20
    label "sport"
  ]
  node [
    id 21
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 22
    label "skrytykowa&#263;"
  ]
  node [
    id 23
    label "spell"
  ]
  node [
    id 24
    label "spr&#243;bowa&#263;"
  ]
  node [
    id 25
    label "powiedzie&#263;"
  ]
  node [
    id 26
    label "attack"
  ]
  node [
    id 27
    label "nast&#261;pi&#263;"
  ]
  node [
    id 28
    label "postara&#263;_si&#281;"
  ]
  node [
    id 29
    label "zrobi&#263;"
  ]
  node [
    id 30
    label "przeby&#263;"
  ]
  node [
    id 31
    label "anoint"
  ]
  node [
    id 32
    label "rozegra&#263;"
  ]
  node [
    id 33
    label "zdelegalizowanie"
  ]
  node [
    id 34
    label "nieoficjalny"
  ]
  node [
    id 35
    label "delegalizowanie"
  ]
  node [
    id 36
    label "nielegalnie"
  ]
  node [
    id 37
    label "cz&#322;owiek"
  ]
  node [
    id 38
    label "przygotowywa&#263;_si&#281;"
  ]
  node [
    id 39
    label "feel"
  ]
  node [
    id 40
    label "przedstawienie"
  ]
  node [
    id 41
    label "try"
  ]
  node [
    id 42
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 43
    label "pos&#322;ugiwa&#263;_si&#281;"
  ]
  node [
    id 44
    label "spo&#380;ywa&#263;"
  ]
  node [
    id 45
    label "sprawdza&#263;"
  ]
  node [
    id 46
    label "stara&#263;_si&#281;"
  ]
  node [
    id 47
    label "kosztowa&#263;"
  ]
  node [
    id 48
    label "da&#263;"
  ]
  node [
    id 49
    label "odst&#261;pi&#263;"
  ]
  node [
    id 50
    label "promocja"
  ]
  node [
    id 51
    label "udost&#281;pni&#263;"
  ]
  node [
    id 52
    label "przyzna&#263;"
  ]
  node [
    id 53
    label "give"
  ]
  node [
    id 54
    label "picture"
  ]
  node [
    id 55
    label "zgodzi&#263;"
  ]
  node [
    id 56
    label "pomocnik"
  ]
  node [
    id 57
    label "doch&#243;d"
  ]
  node [
    id 58
    label "property"
  ]
  node [
    id 59
    label "przedmiot"
  ]
  node [
    id 60
    label "grupa"
  ]
  node [
    id 61
    label "telefon_zaufania"
  ]
  node [
    id 62
    label "darowizna"
  ]
  node [
    id 63
    label "&#347;rodek"
  ]
  node [
    id 64
    label "liga"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 38
  ]
  edge [
    source 8
    target 39
  ]
  edge [
    source 8
    target 40
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 8
    target 46
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 48
  ]
  edge [
    source 9
    target 49
  ]
  edge [
    source 9
    target 50
  ]
  edge [
    source 9
    target 51
  ]
  edge [
    source 9
    target 52
  ]
  edge [
    source 9
    target 53
  ]
  edge [
    source 9
    target 54
  ]
  edge [
    source 10
    target 55
  ]
  edge [
    source 10
    target 56
  ]
  edge [
    source 10
    target 57
  ]
  edge [
    source 10
    target 58
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 10
    target 60
  ]
  edge [
    source 10
    target 61
  ]
  edge [
    source 10
    target 62
  ]
  edge [
    source 10
    target 63
  ]
  edge [
    source 10
    target 64
  ]
]
