graph [
  maxDegree 29
  minDegree 1
  meanDegree 1.988235294117647
  density 0.011764705882352941
  graphCliqueNumber 2
  node [
    id 0
    label "awaria"
    origin "text"
  ]
  node [
    id 1
    label "pompa"
    origin "text"
  ]
  node [
    id 2
    label "hydrauliczny"
    origin "text"
  ]
  node [
    id 3
    label "gridfin&#243;w"
    origin "text"
  ]
  node [
    id 4
    label "lotka"
    origin "text"
  ]
  node [
    id 5
    label "ostatni"
    origin "text"
  ]
  node [
    id 6
    label "faza"
    origin "text"
  ]
  node [
    id 7
    label "l&#261;dowanie"
    origin "text"
  ]
  node [
    id 8
    label "silnik"
    origin "text"
  ]
  node [
    id 9
    label "pozwoli&#263;"
    origin "text"
  ]
  node [
    id 10
    label "ustabilizowa&#263;"
    origin "text"
  ]
  node [
    id 11
    label "rakieta"
    origin "text"
  ]
  node [
    id 12
    label "ca&#322;kowity"
    origin "text"
  ]
  node [
    id 13
    label "zatrzymanie"
    origin "text"
  ]
  node [
    id 14
    label "obr&#243;t"
    origin "text"
  ]
  node [
    id 15
    label "katapultowa&#263;"
  ]
  node [
    id 16
    label "katapultowanie"
  ]
  node [
    id 17
    label "wydarzenie"
  ]
  node [
    id 18
    label "failure"
  ]
  node [
    id 19
    label "ulewa"
  ]
  node [
    id 20
    label "maszyna_hydrauliczna"
  ]
  node [
    id 21
    label "podnios&#322;o&#347;&#263;"
  ]
  node [
    id 22
    label "przepompownia"
  ]
  node [
    id 23
    label "smok"
  ]
  node [
    id 24
    label "nurnik"
  ]
  node [
    id 25
    label "rozmach"
  ]
  node [
    id 26
    label "hydraulicznie"
  ]
  node [
    id 27
    label "badminton"
  ]
  node [
    id 28
    label "skrzyd&#322;o"
  ]
  node [
    id 29
    label "rekwizyt_do_gry"
  ]
  node [
    id 30
    label "pi&#243;ro"
  ]
  node [
    id 31
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 32
    label "sterolotka"
  ]
  node [
    id 33
    label "cz&#322;owiek"
  ]
  node [
    id 34
    label "kolejny"
  ]
  node [
    id 35
    label "istota_&#380;ywa"
  ]
  node [
    id 36
    label "najgorszy"
  ]
  node [
    id 37
    label "aktualny"
  ]
  node [
    id 38
    label "ostatnio"
  ]
  node [
    id 39
    label "niedawno"
  ]
  node [
    id 40
    label "wi&#261;&#380;&#261;cy"
  ]
  node [
    id 41
    label "sko&#324;czony"
  ]
  node [
    id 42
    label "poprzedni"
  ]
  node [
    id 43
    label "pozosta&#322;y"
  ]
  node [
    id 44
    label "w&#261;tpliwy"
  ]
  node [
    id 45
    label "czas"
  ]
  node [
    id 46
    label "dw&#243;jnik"
  ]
  node [
    id 47
    label "fotoelement"
  ]
  node [
    id 48
    label "uk&#322;ad_fizyczny"
  ]
  node [
    id 49
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 50
    label "obsesja"
  ]
  node [
    id 51
    label "komutowa&#263;"
  ]
  node [
    id 52
    label "degree"
  ]
  node [
    id 53
    label "cykl_astronomiczny"
  ]
  node [
    id 54
    label "komutowanie"
  ]
  node [
    id 55
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 56
    label "coil"
  ]
  node [
    id 57
    label "przew&#243;d_elektryczny"
  ]
  node [
    id 58
    label "obw&#243;d"
  ]
  node [
    id 59
    label "zjawisko"
  ]
  node [
    id 60
    label "mieszalno&#347;&#263;"
  ]
  node [
    id 61
    label "stan_skupienia"
  ]
  node [
    id 62
    label "przyrz&#261;d_elektryczny"
  ]
  node [
    id 63
    label "nastr&#243;j"
  ]
  node [
    id 64
    label "przew&#243;d"
  ]
  node [
    id 65
    label "&#322;&#261;cznik_elektryczny"
  ]
  node [
    id 66
    label "kraw&#281;d&#378;"
  ]
  node [
    id 67
    label "okres"
  ]
  node [
    id 68
    label "po&#322;&#261;czenie_elektryczne"
  ]
  node [
    id 69
    label "przerywacz"
  ]
  node [
    id 70
    label "descent"
  ]
  node [
    id 71
    label "radzenie_sobie"
  ]
  node [
    id 72
    label "trafianie"
  ]
  node [
    id 73
    label "przybycie"
  ]
  node [
    id 74
    label "trafienie"
  ]
  node [
    id 75
    label "dobijanie"
  ]
  node [
    id 76
    label "podr&#243;&#380;owanie"
  ]
  node [
    id 77
    label "poradzenie_sobie"
  ]
  node [
    id 78
    label "skok"
  ]
  node [
    id 79
    label "przybywanie"
  ]
  node [
    id 80
    label "dobicie"
  ]
  node [
    id 81
    label "lot"
  ]
  node [
    id 82
    label "lecenie"
  ]
  node [
    id 83
    label "dotarcie"
  ]
  node [
    id 84
    label "wyci&#261;garka"
  ]
  node [
    id 85
    label "biblioteka"
  ]
  node [
    id 86
    label "aerosanie"
  ]
  node [
    id 87
    label "podgrzewacz"
  ]
  node [
    id 88
    label "bombowiec"
  ]
  node [
    id 89
    label "dociera&#263;"
  ]
  node [
    id 90
    label "gniazdo_zaworowe"
  ]
  node [
    id 91
    label "motor&#243;wka"
  ]
  node [
    id 92
    label "nap&#281;d"
  ]
  node [
    id 93
    label "perpetuum_mobile"
  ]
  node [
    id 94
    label "rz&#281;&#380;enie"
  ]
  node [
    id 95
    label "mechanizm"
  ]
  node [
    id 96
    label "gondola_silnikowa"
  ]
  node [
    id 97
    label "docieranie"
  ]
  node [
    id 98
    label "uk&#322;ad_zasilania"
  ]
  node [
    id 99
    label "rz&#281;zi&#263;"
  ]
  node [
    id 100
    label "motoszybowiec"
  ]
  node [
    id 101
    label "motogodzina"
  ]
  node [
    id 102
    label "samoch&#243;d"
  ]
  node [
    id 103
    label "dotrze&#263;"
  ]
  node [
    id 104
    label "radiator"
  ]
  node [
    id 105
    label "program"
  ]
  node [
    id 106
    label "pofolgowa&#263;"
  ]
  node [
    id 107
    label "assent"
  ]
  node [
    id 108
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 109
    label "leave"
  ]
  node [
    id 110
    label "uzna&#263;"
  ]
  node [
    id 111
    label "zagrza&#263;"
  ]
  node [
    id 112
    label "stabilize"
  ]
  node [
    id 113
    label "uregulowa&#263;"
  ]
  node [
    id 114
    label "silnik_rakietowy"
  ]
  node [
    id 115
    label "szybki"
  ]
  node [
    id 116
    label "pocisk_odrzutowy"
  ]
  node [
    id 117
    label "przyrz&#261;d"
  ]
  node [
    id 118
    label "but"
  ]
  node [
    id 119
    label "naci&#261;g"
  ]
  node [
    id 120
    label "tenisista"
  ]
  node [
    id 121
    label "pojazd_lataj&#261;cy"
  ]
  node [
    id 122
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 123
    label "&#347;nieg"
  ]
  node [
    id 124
    label "pojazd"
  ]
  node [
    id 125
    label "&#322;&#261;cznik"
  ]
  node [
    id 126
    label "g&#322;&#243;wka"
  ]
  node [
    id 127
    label "statek_kosmiczny"
  ]
  node [
    id 128
    label "w_pizdu"
  ]
  node [
    id 129
    label "&#322;&#261;czny"
  ]
  node [
    id 130
    label "og&#243;lnie"
  ]
  node [
    id 131
    label "ca&#322;y"
  ]
  node [
    id 132
    label "pe&#322;ny"
  ]
  node [
    id 133
    label "zupe&#322;nie"
  ]
  node [
    id 134
    label "kompletnie"
  ]
  node [
    id 135
    label "funkcjonowanie"
  ]
  node [
    id 136
    label "przefiltrowanie"
  ]
  node [
    id 137
    label "zaczepienie"
  ]
  node [
    id 138
    label "discontinuance"
  ]
  node [
    id 139
    label "observation"
  ]
  node [
    id 140
    label "zaaresztowanie"
  ]
  node [
    id 141
    label "przerwanie"
  ]
  node [
    id 142
    label "career"
  ]
  node [
    id 143
    label "&#322;apanie"
  ]
  node [
    id 144
    label "pozajmowanie"
  ]
  node [
    id 145
    label "przetrzymanie"
  ]
  node [
    id 146
    label "capture"
  ]
  node [
    id 147
    label "unieruchomienie"
  ]
  node [
    id 148
    label "czynno&#347;&#263;"
  ]
  node [
    id 149
    label "z&#322;apanie"
  ]
  node [
    id 150
    label "check"
  ]
  node [
    id 151
    label "hipostaza"
  ]
  node [
    id 152
    label "pochowanie"
  ]
  node [
    id 153
    label "spowodowanie"
  ]
  node [
    id 154
    label "uniemo&#380;liwienie"
  ]
  node [
    id 155
    label "przestanie"
  ]
  node [
    id 156
    label "oddzia&#322;anie"
  ]
  node [
    id 157
    label "zabranie"
  ]
  node [
    id 158
    label "zamkni&#281;cie"
  ]
  node [
    id 159
    label "przechowanie"
  ]
  node [
    id 160
    label "closure"
  ]
  node [
    id 161
    label "powodowanie"
  ]
  node [
    id 162
    label "turn"
  ]
  node [
    id 163
    label "ruch"
  ]
  node [
    id 164
    label "sprzeda&#380;"
  ]
  node [
    id 165
    label "proces_ekonomiczny"
  ]
  node [
    id 166
    label "zmiana"
  ]
  node [
    id 167
    label "wp&#322;yw"
  ]
  node [
    id 168
    label "obieg"
  ]
  node [
    id 169
    label "round"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 111
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 114
  ]
  edge [
    source 11
    target 115
  ]
  edge [
    source 11
    target 116
  ]
  edge [
    source 11
    target 117
  ]
  edge [
    source 11
    target 118
  ]
  edge [
    source 11
    target 119
  ]
  edge [
    source 11
    target 120
  ]
  edge [
    source 11
    target 121
  ]
  edge [
    source 11
    target 122
  ]
  edge [
    source 11
    target 123
  ]
  edge [
    source 11
    target 124
  ]
  edge [
    source 11
    target 125
  ]
  edge [
    source 11
    target 126
  ]
  edge [
    source 11
    target 127
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 12
    target 131
  ]
  edge [
    source 12
    target 132
  ]
  edge [
    source 12
    target 133
  ]
  edge [
    source 12
    target 134
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 135
  ]
  edge [
    source 13
    target 136
  ]
  edge [
    source 13
    target 137
  ]
  edge [
    source 13
    target 138
  ]
  edge [
    source 13
    target 139
  ]
  edge [
    source 13
    target 140
  ]
  edge [
    source 13
    target 141
  ]
  edge [
    source 13
    target 142
  ]
  edge [
    source 13
    target 143
  ]
  edge [
    source 13
    target 144
  ]
  edge [
    source 13
    target 145
  ]
  edge [
    source 13
    target 146
  ]
  edge [
    source 13
    target 147
  ]
  edge [
    source 13
    target 148
  ]
  edge [
    source 13
    target 149
  ]
  edge [
    source 13
    target 150
  ]
  edge [
    source 13
    target 151
  ]
  edge [
    source 13
    target 152
  ]
  edge [
    source 13
    target 153
  ]
  edge [
    source 13
    target 154
  ]
  edge [
    source 13
    target 155
  ]
  edge [
    source 13
    target 156
  ]
  edge [
    source 13
    target 157
  ]
  edge [
    source 13
    target 158
  ]
  edge [
    source 13
    target 159
  ]
  edge [
    source 13
    target 160
  ]
  edge [
    source 13
    target 161
  ]
  edge [
    source 14
    target 162
  ]
  edge [
    source 14
    target 163
  ]
  edge [
    source 14
    target 164
  ]
  edge [
    source 14
    target 165
  ]
  edge [
    source 14
    target 166
  ]
  edge [
    source 14
    target 167
  ]
  edge [
    source 14
    target 168
  ]
  edge [
    source 14
    target 169
  ]
]
