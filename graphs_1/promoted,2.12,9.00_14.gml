graph [
  maxDegree 31
  minDegree 1
  meanDegree 2.173228346456693
  density 0.01724784401949756
  graphCliqueNumber 6
  node [
    id 0
    label "uj&#281;cie"
    origin "text"
  ]
  node [
    id 1
    label "monitoring"
    origin "text"
  ]
  node [
    id 2
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 3
    label "gdzie"
    origin "text"
  ]
  node [
    id 4
    label "nieznany"
    origin "text"
  ]
  node [
    id 5
    label "sprawca"
    origin "text"
  ]
  node [
    id 6
    label "rysowa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "klucz"
    origin "text"
  ]
  node [
    id 8
    label "nowa"
    origin "text"
  ]
  node [
    id 9
    label "lamborghini"
    origin "text"
  ]
  node [
    id 10
    label "aventador"
    origin "text"
  ]
  node [
    id 11
    label "sekunda"
    origin "text"
  ]
  node [
    id 12
    label "zaparkowa&#263;"
    origin "text"
  ]
  node [
    id 13
    label "pod"
    origin "text"
  ]
  node [
    id 14
    label "dom"
    origin "text"
  ]
  node [
    id 15
    label "prezentacja"
  ]
  node [
    id 16
    label "capture"
  ]
  node [
    id 17
    label "wzi&#281;cie"
  ]
  node [
    id 18
    label "wytw&#243;r"
  ]
  node [
    id 19
    label "strona"
  ]
  node [
    id 20
    label "poinformowanie"
  ]
  node [
    id 21
    label "zapisanie"
  ]
  node [
    id 22
    label "withdrawal"
  ]
  node [
    id 23
    label "scena"
  ]
  node [
    id 24
    label "wording"
  ]
  node [
    id 25
    label "film"
  ]
  node [
    id 26
    label "zabranie"
  ]
  node [
    id 27
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 28
    label "zamkni&#281;cie"
  ]
  node [
    id 29
    label "wzbudzenie"
  ]
  node [
    id 30
    label "podniesienie"
  ]
  node [
    id 31
    label "zaaresztowanie"
  ]
  node [
    id 32
    label "rzucenie"
  ]
  node [
    id 33
    label "pochwytanie"
  ]
  node [
    id 34
    label "ochrona"
  ]
  node [
    id 35
    label "obserwacja"
  ]
  node [
    id 36
    label "kamera"
  ]
  node [
    id 37
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 38
    label "s&#322;o&#324;ce"
  ]
  node [
    id 39
    label "czynienie_si&#281;"
  ]
  node [
    id 40
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 41
    label "czas"
  ]
  node [
    id 42
    label "long_time"
  ]
  node [
    id 43
    label "przedpo&#322;udnie"
  ]
  node [
    id 44
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 45
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 46
    label "tydzie&#324;"
  ]
  node [
    id 47
    label "godzina"
  ]
  node [
    id 48
    label "t&#322;usty_czwartek"
  ]
  node [
    id 49
    label "wsta&#263;"
  ]
  node [
    id 50
    label "day"
  ]
  node [
    id 51
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 52
    label "przedwiecz&#243;r"
  ]
  node [
    id 53
    label "Sylwester"
  ]
  node [
    id 54
    label "po&#322;udnie"
  ]
  node [
    id 55
    label "wzej&#347;cie"
  ]
  node [
    id 56
    label "podwiecz&#243;r"
  ]
  node [
    id 57
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 58
    label "rano"
  ]
  node [
    id 59
    label "termin"
  ]
  node [
    id 60
    label "ranek"
  ]
  node [
    id 61
    label "doba"
  ]
  node [
    id 62
    label "wiecz&#243;r"
  ]
  node [
    id 63
    label "walentynki"
  ]
  node [
    id 64
    label "popo&#322;udnie"
  ]
  node [
    id 65
    label "noc"
  ]
  node [
    id 66
    label "wstanie"
  ]
  node [
    id 67
    label "cz&#322;owiek"
  ]
  node [
    id 68
    label "sprawiciel"
  ]
  node [
    id 69
    label "opowiada&#263;"
  ]
  node [
    id 70
    label "kancerowa&#263;"
  ]
  node [
    id 71
    label "kre&#347;li&#263;"
  ]
  node [
    id 72
    label "draw"
  ]
  node [
    id 73
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 74
    label "describe"
  ]
  node [
    id 75
    label "spis"
  ]
  node [
    id 76
    label "kompleks"
  ]
  node [
    id 77
    label "podstawa"
  ]
  node [
    id 78
    label "obja&#347;nienie"
  ]
  node [
    id 79
    label "narz&#281;dzie"
  ]
  node [
    id 80
    label "za&#322;&#261;cznik"
  ]
  node [
    id 81
    label "znak_muzyczny"
  ]
  node [
    id 82
    label "przycisk"
  ]
  node [
    id 83
    label "nakr&#281;ca&#263;"
  ]
  node [
    id 84
    label "spos&#243;b"
  ]
  node [
    id 85
    label "kliniec"
  ]
  node [
    id 86
    label "code"
  ]
  node [
    id 87
    label "szyfr"
  ]
  node [
    id 88
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 89
    label "instrument_strunowy"
  ]
  node [
    id 90
    label "z&#322;&#261;czenie"
  ]
  node [
    id 91
    label "zbi&#243;r"
  ]
  node [
    id 92
    label "szyk"
  ]
  node [
    id 93
    label "radical"
  ]
  node [
    id 94
    label "kod"
  ]
  node [
    id 95
    label "gwiazda"
  ]
  node [
    id 96
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 97
    label "supersamoch&#243;d"
  ]
  node [
    id 98
    label "Lamborghini"
  ]
  node [
    id 99
    label "samoch&#243;d"
  ]
  node [
    id 100
    label "minuta"
  ]
  node [
    id 101
    label "tercja"
  ]
  node [
    id 102
    label "milisekunda"
  ]
  node [
    id 103
    label "nanosekunda"
  ]
  node [
    id 104
    label "uk&#322;ad_SI"
  ]
  node [
    id 105
    label "mikrosekunda"
  ]
  node [
    id 106
    label "time"
  ]
  node [
    id 107
    label "jednostka_czasu"
  ]
  node [
    id 108
    label "jednostka"
  ]
  node [
    id 109
    label "umie&#347;ci&#263;"
  ]
  node [
    id 110
    label "zatrzyma&#263;"
  ]
  node [
    id 111
    label "park"
  ]
  node [
    id 112
    label "garderoba"
  ]
  node [
    id 113
    label "wiecha"
  ]
  node [
    id 114
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 115
    label "grupa"
  ]
  node [
    id 116
    label "budynek"
  ]
  node [
    id 117
    label "fratria"
  ]
  node [
    id 118
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 119
    label "poj&#281;cie"
  ]
  node [
    id 120
    label "rodzina"
  ]
  node [
    id 121
    label "substancja_mieszkaniowa"
  ]
  node [
    id 122
    label "instytucja"
  ]
  node [
    id 123
    label "dom_rodzinny"
  ]
  node [
    id 124
    label "stead"
  ]
  node [
    id 125
    label "siedziba"
  ]
  node [
    id 126
    label "Aventador"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 34
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 9
    target 12
  ]
  edge [
    source 9
    target 13
  ]
  edge [
    source 9
    target 14
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 100
  ]
  edge [
    source 11
    target 101
  ]
  edge [
    source 11
    target 102
  ]
  edge [
    source 11
    target 103
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 108
  ]
  edge [
    source 11
    target 126
  ]
  edge [
    source 11
    target 13
  ]
  edge [
    source 11
    target 14
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 109
  ]
  edge [
    source 12
    target 110
  ]
  edge [
    source 12
    target 111
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 126
  ]
  edge [
    source 14
    target 112
  ]
  edge [
    source 14
    target 113
  ]
  edge [
    source 14
    target 114
  ]
  edge [
    source 14
    target 115
  ]
  edge [
    source 14
    target 116
  ]
  edge [
    source 14
    target 117
  ]
  edge [
    source 14
    target 118
  ]
  edge [
    source 14
    target 119
  ]
  edge [
    source 14
    target 120
  ]
  edge [
    source 14
    target 121
  ]
  edge [
    source 14
    target 122
  ]
  edge [
    source 14
    target 123
  ]
  edge [
    source 14
    target 124
  ]
  edge [
    source 14
    target 125
  ]
  edge [
    source 14
    target 126
  ]
]
