graph [
  maxDegree 6
  minDegree 1
  meanDegree 4.6
  density 0.5111111111111111
  graphCliqueNumber 7
  node [
    id 0
    label "anonimowemirkowyznania"
    origin "text"
  ]
  node [
    id 1
    label "mireczka"
    origin "text"
  ]
  node [
    id 2
    label "pomozcie"
    origin "text"
  ]
  node [
    id 3
    label "P"
  ]
  node [
    id 4
    label "sekunda"
  ]
  node [
    id 5
    label "rad"
  ]
  node [
    id 6
    label "typ"
  ]
  node [
    id 7
    label "zw"
  ]
  node [
    id 8
    label "litr"
  ]
  node [
    id 9
    label "ko&#324;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 4
    target 8
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 6
    target 9
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 8
    target 9
  ]
]
