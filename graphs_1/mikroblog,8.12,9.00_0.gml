graph [
  maxDegree 39
  minDegree 1
  meanDegree 1.972972972972973
  density 0.02702702702702703
  graphCliqueNumber 2
  node [
    id 0
    label "oko"
    origin "text"
  ]
  node [
    id 1
    label "lets"
    origin "text"
  ]
  node [
    id 2
    label "czas"
    origin "text"
  ]
  node [
    id 3
    label "oddac"
    origin "text"
  ]
  node [
    id 4
    label "gryzo&#324;"
    origin "text"
  ]
  node [
    id 5
    label "rozdajo"
    origin "text"
  ]
  node [
    id 6
    label "wypowied&#378;"
  ]
  node [
    id 7
    label "siniec"
  ]
  node [
    id 8
    label "uwaga"
  ]
  node [
    id 9
    label "rzecz"
  ]
  node [
    id 10
    label "&#380;y&#322;a_&#347;rodkowa_siatk&#243;wki"
  ]
  node [
    id 11
    label "powieka"
  ]
  node [
    id 12
    label "oczy"
  ]
  node [
    id 13
    label "&#347;lepko"
  ]
  node [
    id 14
    label "ga&#322;ka_oczna"
  ]
  node [
    id 15
    label "ser_&#380;&#243;&#322;ty"
  ]
  node [
    id 16
    label "&#347;lepie"
  ]
  node [
    id 17
    label "twarz"
  ]
  node [
    id 18
    label "organ"
  ]
  node [
    id 19
    label "worek_spoj&#243;wkowy"
  ]
  node [
    id 20
    label "nerw_wzrokowy"
  ]
  node [
    id 21
    label "wzrok"
  ]
  node [
    id 22
    label "&#378;renica"
  ]
  node [
    id 23
    label "spoj&#243;wka"
  ]
  node [
    id 24
    label "narz&#261;d_&#322;zowy"
  ]
  node [
    id 25
    label "nab&#322;onek_barwnikowy"
  ]
  node [
    id 26
    label "kaprawie&#263;"
  ]
  node [
    id 27
    label "kaprawienie"
  ]
  node [
    id 28
    label "spojrzenie"
  ]
  node [
    id 29
    label "net"
  ]
  node [
    id 30
    label "r&#261;bek_rog&#243;wki"
  ]
  node [
    id 31
    label "coloboma"
  ]
  node [
    id 32
    label "ros&#243;&#322;"
  ]
  node [
    id 33
    label "czasokres"
  ]
  node [
    id 34
    label "trawienie"
  ]
  node [
    id 35
    label "kategoria_gramatyczna"
  ]
  node [
    id 36
    label "period"
  ]
  node [
    id 37
    label "odczyt"
  ]
  node [
    id 38
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 39
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 40
    label "chwila"
  ]
  node [
    id 41
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 42
    label "poprzedzenie"
  ]
  node [
    id 43
    label "koniugacja"
  ]
  node [
    id 44
    label "dzieje"
  ]
  node [
    id 45
    label "poprzedzi&#263;"
  ]
  node [
    id 46
    label "przep&#322;ywanie"
  ]
  node [
    id 47
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 48
    label "odwlekanie_si&#281;"
  ]
  node [
    id 49
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 50
    label "Zeitgeist"
  ]
  node [
    id 51
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 52
    label "okres_czasu"
  ]
  node [
    id 53
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 54
    label "pochodzi&#263;"
  ]
  node [
    id 55
    label "schy&#322;ek"
  ]
  node [
    id 56
    label "czwarty_wymiar"
  ]
  node [
    id 57
    label "chronometria"
  ]
  node [
    id 58
    label "poprzedzanie"
  ]
  node [
    id 59
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 60
    label "pogoda"
  ]
  node [
    id 61
    label "zegar"
  ]
  node [
    id 62
    label "trawi&#263;"
  ]
  node [
    id 63
    label "pochodzenie"
  ]
  node [
    id 64
    label "poprzedza&#263;"
  ]
  node [
    id 65
    label "time_period"
  ]
  node [
    id 66
    label "rachuba_czasu"
  ]
  node [
    id 67
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 68
    label "czasoprzestrze&#324;"
  ]
  node [
    id 69
    label "laba"
  ]
  node [
    id 70
    label "&#322;o&#380;yskowiec"
  ]
  node [
    id 71
    label "rodent"
  ]
  node [
    id 72
    label "ro&#347;lino&#380;erca"
  ]
  node [
    id 73
    label "gryzonie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
]
