graph [
  maxDegree 28
  minDegree 1
  meanDegree 1.8695652173913044
  density 0.04154589371980676
  graphCliqueNumber 2
  node [
    id 0
    label "czujedobrzeczlowiek"
    origin "text"
  ]
  node [
    id 1
    label "pies"
    origin "text"
  ]
  node [
    id 2
    label "zwierzaczek"
    origin "text"
  ]
  node [
    id 3
    label "tldr"
    origin "text"
  ]
  node [
    id 4
    label "wst&#281;p"
    origin "text"
  ]
  node [
    id 5
    label "cz&#322;owiek"
  ]
  node [
    id 6
    label "wy&#263;"
  ]
  node [
    id 7
    label "ssak_drapie&#380;ny"
  ]
  node [
    id 8
    label "spragniony"
  ]
  node [
    id 9
    label "rakarz"
  ]
  node [
    id 10
    label "psowate"
  ]
  node [
    id 11
    label "istota_&#380;ywa"
  ]
  node [
    id 12
    label "kabanos"
  ]
  node [
    id 13
    label "mi&#322;o&#347;nik"
  ]
  node [
    id 14
    label "&#322;ajdak"
  ]
  node [
    id 15
    label "czworon&#243;g"
  ]
  node [
    id 16
    label "policjant"
  ]
  node [
    id 17
    label "szczucie"
  ]
  node [
    id 18
    label "s&#322;u&#380;enie"
  ]
  node [
    id 19
    label "sobaka"
  ]
  node [
    id 20
    label "dogoterapia"
  ]
  node [
    id 21
    label "Cerber"
  ]
  node [
    id 22
    label "wyzwisko"
  ]
  node [
    id 23
    label "szczu&#263;"
  ]
  node [
    id 24
    label "wycie"
  ]
  node [
    id 25
    label "szczeka&#263;"
  ]
  node [
    id 26
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 27
    label "trufla"
  ]
  node [
    id 28
    label "samiec"
  ]
  node [
    id 29
    label "piese&#322;"
  ]
  node [
    id 30
    label "zawy&#263;"
  ]
  node [
    id 31
    label "doj&#347;cie"
  ]
  node [
    id 32
    label "zapowied&#378;"
  ]
  node [
    id 33
    label "evocation"
  ]
  node [
    id 34
    label "g&#322;oska"
  ]
  node [
    id 35
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 36
    label "wymowa"
  ]
  node [
    id 37
    label "pocz&#261;tek"
  ]
  node [
    id 38
    label "utw&#243;r"
  ]
  node [
    id 39
    label "podstawy"
  ]
  node [
    id 40
    label "tekst"
  ]
  node [
    id 41
    label "pan"
  ]
  node [
    id 42
    label "Janko"
  ]
  node [
    id 43
    label "Pan"
  ]
  node [
    id 44
    label "eee"
  ]
  node [
    id 45
    label "Fishery"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 44
    target 45
  ]
]
