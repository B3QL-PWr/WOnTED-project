graph [
  maxDegree 153
  minDegree 1
  meanDegree 2.045070422535211
  density 0.0057770350919073765
  graphCliqueNumber 3
  node [
    id 0
    label "peruwia&#324;ski"
    origin "text"
  ]
  node [
    id 1
    label "uczony"
    origin "text"
  ]
  node [
    id 2
    label "wykaza&#263;"
    origin "text"
  ]
  node [
    id 3
    label "oko&#322;o"
    origin "text"
  ]
  node [
    id 4
    label "rok"
    origin "text"
  ]
  node [
    id 5
    label "trz&#281;sienie"
    origin "text"
  ]
  node [
    id 6
    label "ziemia"
    origin "text"
  ]
  node [
    id 7
    label "sil"
    origin "text"
  ]
  node [
    id 8
    label "ma&#322;o"
    origin "text"
  ]
  node [
    id 9
    label "stopie&#324;"
    origin "text"
  ]
  node [
    id 10
    label "skala"
    origin "text"
  ]
  node [
    id 11
    label "richtera"
    origin "text"
  ]
  node [
    id 12
    label "doprowadzi&#263;"
    origin "text"
  ]
  node [
    id 13
    label "zniekszta&#322;cenie"
    origin "text"
  ]
  node [
    id 14
    label "konstrukcja"
    origin "text"
  ]
  node [
    id 15
    label "mach"
    origin "text"
  ]
  node [
    id 16
    label "picchu"
    origin "text"
  ]
  node [
    id 17
    label "zmotywowa&#263;"
    origin "text"
  ]
  node [
    id 18
    label "inka"
    origin "text"
  ]
  node [
    id 19
    label "wprowadzenie"
    origin "text"
  ]
  node [
    id 20
    label "zmiana"
    origin "text"
  ]
  node [
    id 21
    label "wznosi&#263;"
    origin "text"
  ]
  node [
    id 22
    label "budowla"
    origin "text"
  ]
  node [
    id 23
    label "odporny"
    origin "text"
  ]
  node [
    id 24
    label "wstrz&#261;s"
    origin "text"
  ]
  node [
    id 25
    label "po&#322;udniowoameryka&#324;ski"
  ]
  node [
    id 26
    label "po_peruwia&#324;sku"
  ]
  node [
    id 27
    label "inteligent"
  ]
  node [
    id 28
    label "cz&#322;owiek"
  ]
  node [
    id 29
    label "nauczny"
  ]
  node [
    id 30
    label "uczenie"
  ]
  node [
    id 31
    label "wykszta&#322;cony"
  ]
  node [
    id 32
    label "m&#261;dry"
  ]
  node [
    id 33
    label "Awerroes"
  ]
  node [
    id 34
    label "intelektualista"
  ]
  node [
    id 35
    label "wyrazi&#263;"
  ]
  node [
    id 36
    label "uzasadni&#263;"
  ]
  node [
    id 37
    label "testify"
  ]
  node [
    id 38
    label "stwierdzi&#263;"
  ]
  node [
    id 39
    label "realize"
  ]
  node [
    id 40
    label "stulecie"
  ]
  node [
    id 41
    label "kalendarz"
  ]
  node [
    id 42
    label "czas"
  ]
  node [
    id 43
    label "pora_roku"
  ]
  node [
    id 44
    label "cykl_astronomiczny"
  ]
  node [
    id 45
    label "p&#243;&#322;rocze"
  ]
  node [
    id 46
    label "grupa"
  ]
  node [
    id 47
    label "kwarta&#322;"
  ]
  node [
    id 48
    label "kurs"
  ]
  node [
    id 49
    label "jubileusz"
  ]
  node [
    id 50
    label "miesi&#261;c"
  ]
  node [
    id 51
    label "lata"
  ]
  node [
    id 52
    label "martwy_sezon"
  ]
  node [
    id 53
    label "ruszanie"
  ]
  node [
    id 54
    label "roztrzepywanie"
  ]
  node [
    id 55
    label "podporz&#261;dkowywanie"
  ]
  node [
    id 56
    label "jolt"
  ]
  node [
    id 57
    label "poruszanie"
  ]
  node [
    id 58
    label "agitation"
  ]
  node [
    id 59
    label "rz&#261;dzenie"
  ]
  node [
    id 60
    label "spin"
  ]
  node [
    id 61
    label "wytrz&#261;&#347;ni&#281;cie"
  ]
  node [
    id 62
    label "roztrz&#261;sanie"
  ]
  node [
    id 63
    label "roztrz&#261;&#347;ni&#281;cie"
  ]
  node [
    id 64
    label "wytrz&#261;sanie"
  ]
  node [
    id 65
    label "sk&#322;&#243;cenie"
  ]
  node [
    id 66
    label "Skandynawia"
  ]
  node [
    id 67
    label "Yorkshire"
  ]
  node [
    id 68
    label "Kaukaz"
  ]
  node [
    id 69
    label "Kaszmir"
  ]
  node [
    id 70
    label "Toskania"
  ]
  node [
    id 71
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 72
    label "&#321;emkowszczyzna"
  ]
  node [
    id 73
    label "obszar"
  ]
  node [
    id 74
    label "Amhara"
  ]
  node [
    id 75
    label "Lombardia"
  ]
  node [
    id 76
    label "Podbeskidzie"
  ]
  node [
    id 77
    label "Kalabria"
  ]
  node [
    id 78
    label "kort"
  ]
  node [
    id 79
    label "Tyrol"
  ]
  node [
    id 80
    label "Pamir"
  ]
  node [
    id 81
    label "Lubelszczyzna"
  ]
  node [
    id 82
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 83
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 84
    label "&#379;ywiecczyzna"
  ]
  node [
    id 85
    label "ryzosfera"
  ]
  node [
    id 86
    label "Europa_Wschodnia"
  ]
  node [
    id 87
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 88
    label "Zabajkale"
  ]
  node [
    id 89
    label "Kaszuby"
  ]
  node [
    id 90
    label "Bo&#347;nia"
  ]
  node [
    id 91
    label "Noworosja"
  ]
  node [
    id 92
    label "Ba&#322;kany"
  ]
  node [
    id 93
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 94
    label "Anglia"
  ]
  node [
    id 95
    label "Kielecczyzna"
  ]
  node [
    id 96
    label "Pomorze_Zachodnie"
  ]
  node [
    id 97
    label "Opolskie"
  ]
  node [
    id 98
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 99
    label "skorupa_ziemska"
  ]
  node [
    id 100
    label "Ko&#322;yma"
  ]
  node [
    id 101
    label "Oksytania"
  ]
  node [
    id 102
    label "Syjon"
  ]
  node [
    id 103
    label "posadzka"
  ]
  node [
    id 104
    label "pa&#324;stwo"
  ]
  node [
    id 105
    label "Kociewie"
  ]
  node [
    id 106
    label "Huculszczyzna"
  ]
  node [
    id 107
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 108
    label "budynek"
  ]
  node [
    id 109
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 110
    label "Bawaria"
  ]
  node [
    id 111
    label "pomieszczenie"
  ]
  node [
    id 112
    label "pr&#243;chnica"
  ]
  node [
    id 113
    label "glinowanie"
  ]
  node [
    id 114
    label "Maghreb"
  ]
  node [
    id 115
    label "Bory_Tucholskie"
  ]
  node [
    id 116
    label "Europa_Zachodnia"
  ]
  node [
    id 117
    label "Kerala"
  ]
  node [
    id 118
    label "Podhale"
  ]
  node [
    id 119
    label "Kabylia"
  ]
  node [
    id 120
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 121
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 122
    label "Ma&#322;opolska"
  ]
  node [
    id 123
    label "Polesie"
  ]
  node [
    id 124
    label "Liguria"
  ]
  node [
    id 125
    label "&#321;&#243;dzkie"
  ]
  node [
    id 126
    label "geosystem"
  ]
  node [
    id 127
    label "Palestyna"
  ]
  node [
    id 128
    label "Bojkowszczyzna"
  ]
  node [
    id 129
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 130
    label "Karaiby"
  ]
  node [
    id 131
    label "S&#261;decczyzna"
  ]
  node [
    id 132
    label "Sand&#380;ak"
  ]
  node [
    id 133
    label "Nadrenia"
  ]
  node [
    id 134
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 135
    label "Zakarpacie"
  ]
  node [
    id 136
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 137
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 138
    label "Zag&#243;rze"
  ]
  node [
    id 139
    label "Andaluzja"
  ]
  node [
    id 140
    label "Turkiestan"
  ]
  node [
    id 141
    label "Naddniestrze"
  ]
  node [
    id 142
    label "Hercegowina"
  ]
  node [
    id 143
    label "p&#322;aszczyzna"
  ]
  node [
    id 144
    label "Opolszczyzna"
  ]
  node [
    id 145
    label "jednostka_administracyjna"
  ]
  node [
    id 146
    label "Lotaryngia"
  ]
  node [
    id 147
    label "Afryka_Wschodnia"
  ]
  node [
    id 148
    label "Szlezwik"
  ]
  node [
    id 149
    label "powierzchnia"
  ]
  node [
    id 150
    label "glinowa&#263;"
  ]
  node [
    id 151
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 152
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 153
    label "podglebie"
  ]
  node [
    id 154
    label "Mazowsze"
  ]
  node [
    id 155
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 156
    label "teren"
  ]
  node [
    id 157
    label "Afryka_Zachodnia"
  ]
  node [
    id 158
    label "czynnik_produkcji"
  ]
  node [
    id 159
    label "Galicja"
  ]
  node [
    id 160
    label "Szkocja"
  ]
  node [
    id 161
    label "Walia"
  ]
  node [
    id 162
    label "Powi&#347;le"
  ]
  node [
    id 163
    label "penetrator"
  ]
  node [
    id 164
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 165
    label "kompleks_sorpcyjny"
  ]
  node [
    id 166
    label "Zamojszczyzna"
  ]
  node [
    id 167
    label "Kujawy"
  ]
  node [
    id 168
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 169
    label "Podlasie"
  ]
  node [
    id 170
    label "Laponia"
  ]
  node [
    id 171
    label "Umbria"
  ]
  node [
    id 172
    label "plantowa&#263;"
  ]
  node [
    id 173
    label "Mezoameryka"
  ]
  node [
    id 174
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 175
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 176
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 177
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 178
    label "Kurdystan"
  ]
  node [
    id 179
    label "Kampania"
  ]
  node [
    id 180
    label "Armagnac"
  ]
  node [
    id 181
    label "Polinezja"
  ]
  node [
    id 182
    label "Warmia"
  ]
  node [
    id 183
    label "Wielkopolska"
  ]
  node [
    id 184
    label "litosfera"
  ]
  node [
    id 185
    label "Bordeaux"
  ]
  node [
    id 186
    label "Lauda"
  ]
  node [
    id 187
    label "Mazury"
  ]
  node [
    id 188
    label "Podkarpacie"
  ]
  node [
    id 189
    label "Oceania"
  ]
  node [
    id 190
    label "Lasko"
  ]
  node [
    id 191
    label "Amazonia"
  ]
  node [
    id 192
    label "pojazd"
  ]
  node [
    id 193
    label "glej"
  ]
  node [
    id 194
    label "martwica"
  ]
  node [
    id 195
    label "zapadnia"
  ]
  node [
    id 196
    label "przestrze&#324;"
  ]
  node [
    id 197
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 198
    label "dotleni&#263;"
  ]
  node [
    id 199
    label "Kurpie"
  ]
  node [
    id 200
    label "Tonkin"
  ]
  node [
    id 201
    label "Azja_Wschodnia"
  ]
  node [
    id 202
    label "Mikronezja"
  ]
  node [
    id 203
    label "Ukraina_Zachodnia"
  ]
  node [
    id 204
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 205
    label "Turyngia"
  ]
  node [
    id 206
    label "Baszkiria"
  ]
  node [
    id 207
    label "Apulia"
  ]
  node [
    id 208
    label "miejsce"
  ]
  node [
    id 209
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 210
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 211
    label "Indochiny"
  ]
  node [
    id 212
    label "Biskupizna"
  ]
  node [
    id 213
    label "Lubuskie"
  ]
  node [
    id 214
    label "domain"
  ]
  node [
    id 215
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 216
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 217
    label "mo&#380;liwie"
  ]
  node [
    id 218
    label "nieznaczny"
  ]
  node [
    id 219
    label "kr&#243;tko"
  ]
  node [
    id 220
    label "nieistotnie"
  ]
  node [
    id 221
    label "nieliczny"
  ]
  node [
    id 222
    label "mikroskopijnie"
  ]
  node [
    id 223
    label "pomiernie"
  ]
  node [
    id 224
    label "ma&#322;y"
  ]
  node [
    id 225
    label "minuta"
  ]
  node [
    id 226
    label "forma"
  ]
  node [
    id 227
    label "znaczenie"
  ]
  node [
    id 228
    label "kategoria_gramatyczna"
  ]
  node [
    id 229
    label "przys&#322;&#243;wek"
  ]
  node [
    id 230
    label "szczebel"
  ]
  node [
    id 231
    label "element"
  ]
  node [
    id 232
    label "poziom"
  ]
  node [
    id 233
    label "degree"
  ]
  node [
    id 234
    label "podn&#243;&#380;ek"
  ]
  node [
    id 235
    label "rank"
  ]
  node [
    id 236
    label "przymiotnik"
  ]
  node [
    id 237
    label "podzia&#322;"
  ]
  node [
    id 238
    label "ocena"
  ]
  node [
    id 239
    label "kszta&#322;t"
  ]
  node [
    id 240
    label "wschodek"
  ]
  node [
    id 241
    label "schody"
  ]
  node [
    id 242
    label "gama"
  ]
  node [
    id 243
    label "podstopie&#324;"
  ]
  node [
    id 244
    label "d&#378;wi&#281;k"
  ]
  node [
    id 245
    label "wielko&#347;&#263;"
  ]
  node [
    id 246
    label "jednostka"
  ]
  node [
    id 247
    label "przedzia&#322;"
  ]
  node [
    id 248
    label "przymiar"
  ]
  node [
    id 249
    label "podzia&#322;ka"
  ]
  node [
    id 250
    label "proporcja"
  ]
  node [
    id 251
    label "tetrachord"
  ]
  node [
    id 252
    label "scale"
  ]
  node [
    id 253
    label "dominanta"
  ]
  node [
    id 254
    label "rejestr"
  ]
  node [
    id 255
    label "sfera"
  ]
  node [
    id 256
    label "struktura"
  ]
  node [
    id 257
    label "kreska"
  ]
  node [
    id 258
    label "zero"
  ]
  node [
    id 259
    label "interwa&#322;"
  ]
  node [
    id 260
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 261
    label "subdominanta"
  ]
  node [
    id 262
    label "dziedzina"
  ]
  node [
    id 263
    label "masztab"
  ]
  node [
    id 264
    label "part"
  ]
  node [
    id 265
    label "podzakres"
  ]
  node [
    id 266
    label "zbi&#243;r"
  ]
  node [
    id 267
    label "poprowadzi&#263;"
  ]
  node [
    id 268
    label "spowodowa&#263;"
  ]
  node [
    id 269
    label "pos&#322;a&#263;"
  ]
  node [
    id 270
    label "have_a_bun_in_the_oven"
  ]
  node [
    id 271
    label "wykona&#263;"
  ]
  node [
    id 272
    label "wzbudzi&#263;"
  ]
  node [
    id 273
    label "wprowadzi&#263;"
  ]
  node [
    id 274
    label "set"
  ]
  node [
    id 275
    label "take"
  ]
  node [
    id 276
    label "carry"
  ]
  node [
    id 277
    label "zmienienie"
  ]
  node [
    id 278
    label "potworniactwo"
  ]
  node [
    id 279
    label "diastrofizm"
  ]
  node [
    id 280
    label "contortion"
  ]
  node [
    id 281
    label "wytw&#243;r"
  ]
  node [
    id 282
    label "practice"
  ]
  node [
    id 283
    label "budowa"
  ]
  node [
    id 284
    label "rzecz"
  ]
  node [
    id 285
    label "wykre&#347;lanie"
  ]
  node [
    id 286
    label "element_konstrukcyjny"
  ]
  node [
    id 287
    label "jednostka_pr&#281;dko&#347;ci"
  ]
  node [
    id 288
    label "motivate"
  ]
  node [
    id 289
    label "zach&#281;ci&#263;"
  ]
  node [
    id 290
    label "kawa_bezkofeinowa"
  ]
  node [
    id 291
    label "rynek"
  ]
  node [
    id 292
    label "issue"
  ]
  node [
    id 293
    label "evocation"
  ]
  node [
    id 294
    label "wst&#281;p"
  ]
  node [
    id 295
    label "nuklearyzacja"
  ]
  node [
    id 296
    label "umo&#380;liwienie"
  ]
  node [
    id 297
    label "zacz&#281;cie"
  ]
  node [
    id 298
    label "wpisanie"
  ]
  node [
    id 299
    label "zapoznanie"
  ]
  node [
    id 300
    label "zrobienie"
  ]
  node [
    id 301
    label "czynno&#347;&#263;"
  ]
  node [
    id 302
    label "entrance"
  ]
  node [
    id 303
    label "wej&#347;cie"
  ]
  node [
    id 304
    label "podstawy"
  ]
  node [
    id 305
    label "spowodowanie"
  ]
  node [
    id 306
    label "zak&#322;&#243;cenie"
  ]
  node [
    id 307
    label "w&#322;&#261;czenie"
  ]
  node [
    id 308
    label "doprowadzenie"
  ]
  node [
    id 309
    label "przewietrzenie"
  ]
  node [
    id 310
    label "deduction"
  ]
  node [
    id 311
    label "umieszczenie"
  ]
  node [
    id 312
    label "anatomopatolog"
  ]
  node [
    id 313
    label "rewizja"
  ]
  node [
    id 314
    label "oznaka"
  ]
  node [
    id 315
    label "ferment"
  ]
  node [
    id 316
    label "komplet"
  ]
  node [
    id 317
    label "tura"
  ]
  node [
    id 318
    label "amendment"
  ]
  node [
    id 319
    label "zmianka"
  ]
  node [
    id 320
    label "odmienianie"
  ]
  node [
    id 321
    label "passage"
  ]
  node [
    id 322
    label "zjawisko"
  ]
  node [
    id 323
    label "change"
  ]
  node [
    id 324
    label "praca"
  ]
  node [
    id 325
    label "wytwarza&#263;"
  ]
  node [
    id 326
    label "raise"
  ]
  node [
    id 327
    label "podnosi&#263;"
  ]
  node [
    id 328
    label "kolumnada"
  ]
  node [
    id 329
    label "obudowywa&#263;"
  ]
  node [
    id 330
    label "zbudowa&#263;"
  ]
  node [
    id 331
    label "obudowanie"
  ]
  node [
    id 332
    label "obudowywanie"
  ]
  node [
    id 333
    label "Sukiennice"
  ]
  node [
    id 334
    label "odbudowywanie_si&#281;"
  ]
  node [
    id 335
    label "fundament"
  ]
  node [
    id 336
    label "stan_surowy"
  ]
  node [
    id 337
    label "obudowa&#263;"
  ]
  node [
    id 338
    label "postanie"
  ]
  node [
    id 339
    label "zbudowanie"
  ]
  node [
    id 340
    label "korpus"
  ]
  node [
    id 341
    label "odbudowywa&#263;_si&#281;"
  ]
  node [
    id 342
    label "uodpornienie_si&#281;"
  ]
  node [
    id 343
    label "silny"
  ]
  node [
    id 344
    label "uodpornienie"
  ]
  node [
    id 345
    label "uodpornianie"
  ]
  node [
    id 346
    label "hartowny"
  ]
  node [
    id 347
    label "uodparnianie"
  ]
  node [
    id 348
    label "uodparnianie_si&#281;"
  ]
  node [
    id 349
    label "mocny"
  ]
  node [
    id 350
    label "impact"
  ]
  node [
    id 351
    label "ruch"
  ]
  node [
    id 352
    label "szok"
  ]
  node [
    id 353
    label "zaburzenie"
  ]
  node [
    id 354
    label "Picchu"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 151
  ]
  edge [
    source 6
    target 152
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 217
  ]
  edge [
    source 8
    target 218
  ]
  edge [
    source 8
    target 219
  ]
  edge [
    source 8
    target 220
  ]
  edge [
    source 8
    target 221
  ]
  edge [
    source 8
    target 222
  ]
  edge [
    source 8
    target 223
  ]
  edge [
    source 8
    target 224
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 225
  ]
  edge [
    source 9
    target 226
  ]
  edge [
    source 9
    target 227
  ]
  edge [
    source 9
    target 228
  ]
  edge [
    source 9
    target 229
  ]
  edge [
    source 9
    target 230
  ]
  edge [
    source 9
    target 231
  ]
  edge [
    source 9
    target 232
  ]
  edge [
    source 9
    target 233
  ]
  edge [
    source 9
    target 234
  ]
  edge [
    source 9
    target 235
  ]
  edge [
    source 9
    target 236
  ]
  edge [
    source 9
    target 237
  ]
  edge [
    source 9
    target 238
  ]
  edge [
    source 9
    target 239
  ]
  edge [
    source 9
    target 240
  ]
  edge [
    source 9
    target 208
  ]
  edge [
    source 9
    target 241
  ]
  edge [
    source 9
    target 242
  ]
  edge [
    source 9
    target 243
  ]
  edge [
    source 9
    target 244
  ]
  edge [
    source 9
    target 245
  ]
  edge [
    source 9
    target 246
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 247
  ]
  edge [
    source 10
    target 248
  ]
  edge [
    source 10
    target 249
  ]
  edge [
    source 10
    target 250
  ]
  edge [
    source 10
    target 251
  ]
  edge [
    source 10
    target 252
  ]
  edge [
    source 10
    target 253
  ]
  edge [
    source 10
    target 254
  ]
  edge [
    source 10
    target 255
  ]
  edge [
    source 10
    target 256
  ]
  edge [
    source 10
    target 257
  ]
  edge [
    source 10
    target 258
  ]
  edge [
    source 10
    target 259
  ]
  edge [
    source 10
    target 260
  ]
  edge [
    source 10
    target 261
  ]
  edge [
    source 10
    target 262
  ]
  edge [
    source 10
    target 263
  ]
  edge [
    source 10
    target 264
  ]
  edge [
    source 10
    target 265
  ]
  edge [
    source 10
    target 266
  ]
  edge [
    source 10
    target 245
  ]
  edge [
    source 10
    target 246
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 267
  ]
  edge [
    source 12
    target 268
  ]
  edge [
    source 12
    target 269
  ]
  edge [
    source 12
    target 270
  ]
  edge [
    source 12
    target 271
  ]
  edge [
    source 12
    target 272
  ]
  edge [
    source 12
    target 273
  ]
  edge [
    source 12
    target 274
  ]
  edge [
    source 12
    target 275
  ]
  edge [
    source 12
    target 276
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 277
  ]
  edge [
    source 13
    target 278
  ]
  edge [
    source 13
    target 279
  ]
  edge [
    source 13
    target 280
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 281
  ]
  edge [
    source 14
    target 256
  ]
  edge [
    source 14
    target 282
  ]
  edge [
    source 14
    target 283
  ]
  edge [
    source 14
    target 284
  ]
  edge [
    source 14
    target 285
  ]
  edge [
    source 14
    target 286
  ]
  edge [
    source 14
    target 22
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 287
  ]
  edge [
    source 15
    target 354
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 288
  ]
  edge [
    source 17
    target 289
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 290
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 291
  ]
  edge [
    source 19
    target 292
  ]
  edge [
    source 19
    target 293
  ]
  edge [
    source 19
    target 294
  ]
  edge [
    source 19
    target 295
  ]
  edge [
    source 19
    target 296
  ]
  edge [
    source 19
    target 297
  ]
  edge [
    source 19
    target 298
  ]
  edge [
    source 19
    target 299
  ]
  edge [
    source 19
    target 300
  ]
  edge [
    source 19
    target 301
  ]
  edge [
    source 19
    target 302
  ]
  edge [
    source 19
    target 303
  ]
  edge [
    source 19
    target 304
  ]
  edge [
    source 19
    target 305
  ]
  edge [
    source 19
    target 306
  ]
  edge [
    source 19
    target 307
  ]
  edge [
    source 19
    target 308
  ]
  edge [
    source 19
    target 309
  ]
  edge [
    source 19
    target 310
  ]
  edge [
    source 19
    target 311
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 312
  ]
  edge [
    source 20
    target 313
  ]
  edge [
    source 20
    target 314
  ]
  edge [
    source 20
    target 42
  ]
  edge [
    source 20
    target 315
  ]
  edge [
    source 20
    target 316
  ]
  edge [
    source 20
    target 317
  ]
  edge [
    source 20
    target 318
  ]
  edge [
    source 20
    target 319
  ]
  edge [
    source 20
    target 320
  ]
  edge [
    source 20
    target 321
  ]
  edge [
    source 20
    target 322
  ]
  edge [
    source 20
    target 323
  ]
  edge [
    source 20
    target 324
  ]
  edge [
    source 20
    target 24
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 325
  ]
  edge [
    source 21
    target 326
  ]
  edge [
    source 21
    target 327
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 328
  ]
  edge [
    source 22
    target 329
  ]
  edge [
    source 22
    target 330
  ]
  edge [
    source 22
    target 331
  ]
  edge [
    source 22
    target 332
  ]
  edge [
    source 22
    target 333
  ]
  edge [
    source 22
    target 334
  ]
  edge [
    source 22
    target 335
  ]
  edge [
    source 22
    target 336
  ]
  edge [
    source 22
    target 284
  ]
  edge [
    source 22
    target 337
  ]
  edge [
    source 22
    target 338
  ]
  edge [
    source 22
    target 339
  ]
  edge [
    source 22
    target 340
  ]
  edge [
    source 22
    target 341
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 342
  ]
  edge [
    source 23
    target 343
  ]
  edge [
    source 23
    target 344
  ]
  edge [
    source 23
    target 345
  ]
  edge [
    source 23
    target 346
  ]
  edge [
    source 23
    target 347
  ]
  edge [
    source 23
    target 348
  ]
  edge [
    source 23
    target 349
  ]
  edge [
    source 24
    target 350
  ]
  edge [
    source 24
    target 314
  ]
  edge [
    source 24
    target 351
  ]
  edge [
    source 24
    target 352
  ]
  edge [
    source 24
    target 353
  ]
]
