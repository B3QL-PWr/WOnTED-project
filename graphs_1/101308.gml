graph [
  maxDegree 1
  minDegree 1
  meanDegree 1
  density 0.2
  graphCliqueNumber 2
  node [
    id 0
    label "vincent"
    origin "text"
  ]
  node [
    id 1
    label "og&#233;"
    origin "text"
  ]
  node [
    id 2
    label "Vincent"
  ]
  node [
    id 3
    label "Og&#233;"
  ]
  node [
    id 4
    label "Saint"
  ]
  node [
    id 5
    label "Domingue"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
]
