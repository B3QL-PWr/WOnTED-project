graph [
  maxDegree 23
  minDegree 1
  meanDegree 1.9487179487179487
  density 0.05128205128205128
  graphCliqueNumber 2
  node [
    id 0
    label "wiedzie&#263;"
    origin "text"
  ]
  node [
    id 1
    label "robi&#263;"
    origin "text"
  ]
  node [
    id 2
    label "ogl&#261;da&#263;"
    origin "text"
  ]
  node [
    id 3
    label "serial"
    origin "text"
  ]
  node [
    id 4
    label "ratunek"
    origin "text"
  ]
  node [
    id 5
    label "cognizance"
  ]
  node [
    id 6
    label "tentegowa&#263;"
  ]
  node [
    id 7
    label "urz&#261;dza&#263;"
  ]
  node [
    id 8
    label "give"
  ]
  node [
    id 9
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 10
    label "czyni&#263;"
  ]
  node [
    id 11
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 12
    label "post&#281;powa&#263;"
  ]
  node [
    id 13
    label "wydala&#263;"
  ]
  node [
    id 14
    label "oszukiwa&#263;"
  ]
  node [
    id 15
    label "organizowa&#263;"
  ]
  node [
    id 16
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 17
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 18
    label "work"
  ]
  node [
    id 19
    label "przerabia&#263;"
  ]
  node [
    id 20
    label "stylizowa&#263;"
  ]
  node [
    id 21
    label "falowa&#263;"
  ]
  node [
    id 22
    label "act"
  ]
  node [
    id 23
    label "peddle"
  ]
  node [
    id 24
    label "ukazywa&#263;"
  ]
  node [
    id 25
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 26
    label "praca"
  ]
  node [
    id 27
    label "notice"
  ]
  node [
    id 28
    label "zapoznawa&#263;_si&#281;"
  ]
  node [
    id 29
    label "styka&#263;_si&#281;"
  ]
  node [
    id 30
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 31
    label "seria"
  ]
  node [
    id 32
    label "Klan"
  ]
  node [
    id 33
    label "film"
  ]
  node [
    id 34
    label "Ranczo"
  ]
  node [
    id 35
    label "program_telewizyjny"
  ]
  node [
    id 36
    label "care"
  ]
  node [
    id 37
    label "pomoc"
  ]
  node [
    id 38
    label "wyratowanie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
]
