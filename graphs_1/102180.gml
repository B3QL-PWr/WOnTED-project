graph [
  maxDegree 72
  minDegree 1
  meanDegree 2.560224089635854
  density 0.0017941304061919092
  graphCliqueNumber 9
  node [
    id 0
    label "dawny"
    origin "text"
  ]
  node [
    id 1
    label "czas"
    origin "text"
  ]
  node [
    id 2
    label "cierpliwo&#347;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "statek"
    origin "text"
  ]
  node [
    id 4
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 5
    label "by&#263;"
    origin "text"
  ]
  node [
    id 6
    label "zawsze"
    origin "text"
  ]
  node [
    id 7
    label "siebie"
    origin "text"
  ]
  node [
    id 8
    label "kawa&#322;ek"
    origin "text"
  ]
  node [
    id 9
    label "chleb"
    origin "text"
  ]
  node [
    id 10
    label "zapewni&#263;"
    origin "text"
  ]
  node [
    id 11
    label "s&#322;u&#380;ba"
    origin "text"
  ]
  node [
    id 12
    label "pa&#324;skie"
    origin "text"
  ]
  node [
    id 13
    label "dw&#243;r"
    origin "text"
  ]
  node [
    id 14
    label "szlachcic"
    origin "text"
  ]
  node [
    id 15
    label "krzywda"
    origin "text"
  ]
  node [
    id 16
    label "robi&#263;"
    origin "text"
  ]
  node [
    id 17
    label "pan"
    origin "text"
  ]
  node [
    id 18
    label "ale"
    origin "text"
  ]
  node [
    id 19
    label "razem"
    origin "text"
  ]
  node [
    id 20
    label "opiekun"
    origin "text"
  ]
  node [
    id 21
    label "s&#322;uga"
    origin "text"
  ]
  node [
    id 22
    label "je&#380;eli"
    origin "text"
  ]
  node [
    id 23
    label "jak"
    origin "text"
  ]
  node [
    id 24
    label "ojciec"
    origin "text"
  ]
  node [
    id 25
    label "kara&#263;"
    origin "text"
  ]
  node [
    id 26
    label "kierowa&#263;"
    origin "text"
  ]
  node [
    id 27
    label "los"
    origin "text"
  ]
  node [
    id 28
    label "pami&#281;ta&#263;"
    origin "text"
  ]
  node [
    id 29
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 30
    label "dosta&#263;"
    origin "text"
  ]
  node [
    id 31
    label "si&#281;"
    origin "text"
  ]
  node [
    id 32
    label "trzeba"
    origin "text"
  ]
  node [
    id 33
    label "mie&#263;"
    origin "text"
  ]
  node [
    id 34
    label "zas&#322;ug"
    origin "text"
  ]
  node [
    id 35
    label "lub"
    origin "text"
  ]
  node [
    id 36
    label "krewna"
    origin "text"
  ]
  node [
    id 37
    label "albo"
    origin "text"
  ]
  node [
    id 38
    label "jaki"
    origin "text"
  ]
  node [
    id 39
    label "dobrodziej"
    origin "text"
  ]
  node [
    id 40
    label "chcie&#263;"
    origin "text"
  ]
  node [
    id 41
    label "wstawi&#263;"
    origin "text"
  ]
  node [
    id 42
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 43
    label "powiedzie&#263;"
    origin "text"
  ]
  node [
    id 44
    label "opatrzno&#347;&#263;"
    origin "text"
  ]
  node [
    id 45
    label "nic"
    origin "text"
  ]
  node [
    id 46
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 47
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 48
    label "odumrze&#263;"
    origin "text"
  ]
  node [
    id 49
    label "pi&#261;ta"
    origin "text"
  ]
  node [
    id 50
    label "rok"
    origin "text"
  ]
  node [
    id 51
    label "cho&#263;by"
    origin "text"
  ]
  node [
    id 52
    label "&#380;y&#322;a"
    origin "text"
  ]
  node [
    id 53
    label "pom&#243;c"
    origin "text"
  ]
  node [
    id 54
    label "za&#347;cianek"
    origin "text"
  ]
  node [
    id 55
    label "spokojny"
    origin "text"
  ]
  node [
    id 56
    label "rola"
    origin "text"
  ]
  node [
    id 57
    label "pilnowa&#263;"
    origin "text"
  ]
  node [
    id 58
    label "gdzie"
    origin "text"
  ]
  node [
    id 59
    label "prostaczek"
    origin "text"
  ]
  node [
    id 60
    label "docisn&#261;&#263;"
    origin "text"
  ]
  node [
    id 61
    label "moja"
    origin "text"
  ]
  node [
    id 62
    label "matka"
    origin "text"
  ]
  node [
    id 63
    label "potem"
    origin "text"
  ]
  node [
    id 64
    label "powt&#243;rnie"
    origin "text"
  ]
  node [
    id 65
    label "p&#243;j&#347;&#263;"
    origin "text"
  ]
  node [
    id 66
    label "m&#261;&#380;"
    origin "text"
  ]
  node [
    id 67
    label "ojczym"
    origin "text"
  ]
  node [
    id 68
    label "bardzo"
    origin "text"
  ]
  node [
    id 69
    label "rad"
    origin "text"
  ]
  node [
    id 70
    label "dom"
    origin "text"
  ]
  node [
    id 71
    label "trzyma&#263;"
    origin "text"
  ]
  node [
    id 72
    label "ubogi"
    origin "text"
  ]
  node [
    id 73
    label "w&#322;asne"
    origin "text"
  ]
  node [
    id 74
    label "doczeka&#263;"
    origin "text"
  ]
  node [
    id 75
    label "dziecko"
    origin "text"
  ]
  node [
    id 76
    label "b&#243;g"
    origin "text"
  ]
  node [
    id 77
    label "sierota"
    origin "text"
  ]
  node [
    id 78
    label "opuszcza&#263;"
    origin "text"
  ]
  node [
    id 79
    label "mia&#322;"
    origin "text"
  ]
  node [
    id 80
    label "wujaszek"
    origin "text"
  ]
  node [
    id 81
    label "wo&#378;ny"
    origin "text"
  ]
  node [
    id 82
    label "dworek"
    origin "text"
  ]
  node [
    id 83
    label "ogr&#243;d"
    origin "text"
  ]
  node [
    id 84
    label "nowogr&#243;dek"
    origin "text"
  ]
  node [
    id 85
    label "chocia&#380;"
    origin "text"
  ]
  node [
    id 86
    label "dwa"
    origin "text"
  ]
  node [
    id 87
    label "jednak&#380;e"
    origin "text"
  ]
  node [
    id 88
    label "zlitowa&#263;"
    origin "text"
  ]
  node [
    id 89
    label "nad"
    origin "text"
  ]
  node [
    id 90
    label "zabra&#263;"
    origin "text"
  ]
  node [
    id 91
    label "kiedy"
    origin "text"
  ]
  node [
    id 92
    label "si&#243;dmy"
    origin "text"
  ]
  node [
    id 93
    label "ko&#324;czy&#263;"
    origin "text"
  ]
  node [
    id 94
    label "wyj&#347;&#263;"
    origin "text"
  ]
  node [
    id 95
    label "jako&#347;"
    origin "text"
  ]
  node [
    id 96
    label "p&#243;&#322;cz&#322;owiek"
    origin "text"
  ]
  node [
    id 97
    label "cho&#263;"
    origin "text"
  ]
  node [
    id 98
    label "cz&#281;&#347;&#263;"
    origin "text"
  ]
  node [
    id 99
    label "przynajmniej"
    origin "text"
  ]
  node [
    id 100
    label "wywi&#261;za&#263;by&#263;"
    origin "text"
  ]
  node [
    id 101
    label "winny"
    origin "text"
  ]
  node [
    id 102
    label "niewielki"
    origin "text"
  ]
  node [
    id 103
    label "pomoc"
    origin "text"
  ]
  node [
    id 104
    label "pob&#322;ogos&#322;awi&#263;"
    origin "text"
  ]
  node [
    id 105
    label "&#380;aden"
    origin "text"
  ]
  node [
    id 106
    label "wnuk"
    origin "text"
  ]
  node [
    id 107
    label "nikt"
    origin "text"
  ]
  node [
    id 108
    label "nie"
    origin "text"
  ]
  node [
    id 109
    label "grzeczno&#347;ci"
    origin "text"
  ]
  node [
    id 110
    label "serce"
    origin "text"
  ]
  node [
    id 111
    label "potrzeba"
    origin "text"
  ]
  node [
    id 112
    label "ot&#243;&#380;"
    origin "text"
  ]
  node [
    id 113
    label "wo&#378;niowstwo"
    origin "text"
  ]
  node [
    id 114
    label "taki"
    origin "text"
  ]
  node [
    id 115
    label "dzienia"
    origin "text"
  ]
  node [
    id 116
    label "tynf"
    origin "text"
  ]
  node [
    id 117
    label "kapa&#263;"
    origin "text"
  ]
  node [
    id 118
    label "czerwony"
    origin "text"
  ]
  node [
    id 119
    label "z&#322;oty"
    origin "text"
  ]
  node [
    id 120
    label "pokaza&#263;"
    origin "text"
  ]
  node [
    id 121
    label "wujenka"
    origin "text"
  ]
  node [
    id 122
    label "by&#322;a"
    origin "text"
  ]
  node [
    id 123
    label "dobra"
    origin "text"
  ]
  node [
    id 124
    label "gospodyni"
    origin "text"
  ]
  node [
    id 125
    label "student"
    origin "text"
  ]
  node [
    id 126
    label "dyrektor"
    origin "text"
  ]
  node [
    id 127
    label "but"
    origin "text"
  ]
  node [
    id 128
    label "czy&#347;ci&#263;"
    origin "text"
  ]
  node [
    id 129
    label "ko&#347;ci&#243;&#322;"
    origin "text"
  ]
  node [
    id 130
    label "bractwo"
    origin "text"
  ]
  node [
    id 131
    label "&#347;piewa&#263;"
    origin "text"
  ]
  node [
    id 132
    label "ogroda"
    origin "text"
  ]
  node [
    id 133
    label "pieli&#263;"
    origin "text"
  ]
  node [
    id 134
    label "ma&#322;y"
    origin "text"
  ]
  node [
    id 135
    label "praca"
    origin "text"
  ]
  node [
    id 136
    label "wprawia&#263;by&#263;"
    origin "text"
  ]
  node [
    id 137
    label "pos&#322;u&#380;y&#263;"
    origin "text"
  ]
  node [
    id 138
    label "mieszka&#263;"
    origin "text"
  ]
  node [
    id 139
    label "sam"
    origin "text"
  ]
  node [
    id 140
    label "czwarta"
    origin "text"
  ]
  node [
    id 141
    label "klasa"
    origin "text"
  ]
  node [
    id 142
    label "wyuczy&#263;"
    origin "text"
  ]
  node [
    id 143
    label "czyta&#263;"
    origin "text"
  ]
  node [
    id 144
    label "pisa&#263;"
    origin "text"
  ]
  node [
    id 145
    label "tak"
    origin "text"
  ]
  node [
    id 146
    label "ksi&#261;dz"
    origin "text"
  ]
  node [
    id 147
    label "jezuita"
    origin "text"
  ]
  node [
    id 148
    label "wprost"
    origin "text"
  ]
  node [
    id 149
    label "przypu&#347;ci&#263;"
    origin "text"
  ]
  node [
    id 150
    label "infima"
    origin "text"
  ]
  node [
    id 151
    label "przechodzi&#263;"
    origin "text"
  ]
  node [
    id 152
    label "pro"
    origin "text"
  ]
  node [
    id 153
    label "forma"
    origin "text"
  ]
  node [
    id 154
    label "uczy&#263;by&#263;"
    origin "text"
  ]
  node [
    id 155
    label "najprz&#243;d"
    origin "text"
  ]
  node [
    id 156
    label "ksi&#261;&#380;ek"
    origin "text"
  ]
  node [
    id 157
    label "kupi&#263;"
    origin "text"
  ]
  node [
    id 158
    label "alwar"
    origin "text"
  ]
  node [
    id 159
    label "swobodnie"
    origin "text"
  ]
  node [
    id 160
    label "sw&#243;j"
    origin "text"
  ]
  node [
    id 161
    label "gorliwie"
    origin "text"
  ]
  node [
    id 162
    label "uczy&#263;"
    origin "text"
  ]
  node [
    id 163
    label "czym"
    origin "text"
  ]
  node [
    id 164
    label "pr&#243;&#380;nowa&#263;"
    origin "text"
  ]
  node [
    id 165
    label "p&#243;ki&#347;"
    origin "text"
  ]
  node [
    id 166
    label "dawa&#263;"
    origin "text"
  ]
  node [
    id 167
    label "barszcz"
    origin "text"
  ]
  node [
    id 168
    label "opo&#324;cza"
    origin "text"
  ]
  node [
    id 169
    label "wyrosn&#261;&#263;"
    origin "text"
  ]
  node [
    id 170
    label "czego"
    origin "text"
  ]
  node [
    id 171
    label "wypracowa&#263;"
    origin "text"
  ]
  node [
    id 172
    label "chyba"
    origin "text"
  ]
  node [
    id 173
    label "wy&#380;ebra&#263;"
    origin "text"
  ]
  node [
    id 174
    label "bo&#347;"
    origin "text"
  ]
  node [
    id 175
    label "go&#322;y"
    origin "text"
  ]
  node [
    id 176
    label "palec"
    origin "text"
  ]
  node [
    id 177
    label "boja"
    origin "text"
  ]
  node [
    id 178
    label "bywa&#263;"
    origin "text"
  ]
  node [
    id 179
    label "aby"
    origin "text"
  ]
  node [
    id 180
    label "dziad"
    origin "text"
  ]
  node [
    id 181
    label "ko&#347;cielna"
    origin "text"
  ]
  node [
    id 182
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 183
    label "pami&#281;&#263;"
    origin "text"
  ]
  node [
    id 184
    label "przed"
    origin "text"
  ]
  node [
    id 185
    label "wakacje"
    origin "text"
  ]
  node [
    id 186
    label "gramatyka"
    origin "text"
  ]
  node [
    id 187
    label "uzyska&#263;by&#263;"
    origin "text"
  ]
  node [
    id 188
    label "promocja"
    origin "text"
  ]
  node [
    id 189
    label "przesz&#322;y"
  ]
  node [
    id 190
    label "dawno"
  ]
  node [
    id 191
    label "dawniej"
  ]
  node [
    id 192
    label "kombatant"
  ]
  node [
    id 193
    label "stary"
  ]
  node [
    id 194
    label "odleg&#322;y"
  ]
  node [
    id 195
    label "anachroniczny"
  ]
  node [
    id 196
    label "przestarza&#322;y"
  ]
  node [
    id 197
    label "od_dawna"
  ]
  node [
    id 198
    label "poprzedni"
  ]
  node [
    id 199
    label "d&#322;ugoletni"
  ]
  node [
    id 200
    label "wcze&#347;niejszy"
  ]
  node [
    id 201
    label "niegdysiejszy"
  ]
  node [
    id 202
    label "czasokres"
  ]
  node [
    id 203
    label "trawienie"
  ]
  node [
    id 204
    label "kategoria_gramatyczna"
  ]
  node [
    id 205
    label "period"
  ]
  node [
    id 206
    label "odczyt"
  ]
  node [
    id 207
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 208
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 209
    label "chwila"
  ]
  node [
    id 210
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 211
    label "poprzedzenie"
  ]
  node [
    id 212
    label "koniugacja"
  ]
  node [
    id 213
    label "dzieje"
  ]
  node [
    id 214
    label "poprzedzi&#263;"
  ]
  node [
    id 215
    label "przep&#322;ywanie"
  ]
  node [
    id 216
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 217
    label "odwlekanie_si&#281;"
  ]
  node [
    id 218
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 219
    label "Zeitgeist"
  ]
  node [
    id 220
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 221
    label "okres_czasu"
  ]
  node [
    id 222
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 223
    label "pochodzi&#263;"
  ]
  node [
    id 224
    label "schy&#322;ek"
  ]
  node [
    id 225
    label "czwarty_wymiar"
  ]
  node [
    id 226
    label "chronometria"
  ]
  node [
    id 227
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 228
    label "poprzedzanie"
  ]
  node [
    id 229
    label "pogoda"
  ]
  node [
    id 230
    label "zegar"
  ]
  node [
    id 231
    label "pochodzenie"
  ]
  node [
    id 232
    label "poprzedza&#263;"
  ]
  node [
    id 233
    label "trawi&#263;"
  ]
  node [
    id 234
    label "time_period"
  ]
  node [
    id 235
    label "rachuba_czasu"
  ]
  node [
    id 236
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 237
    label "czasoprzestrze&#324;"
  ]
  node [
    id 238
    label "laba"
  ]
  node [
    id 239
    label "wytrwa&#322;o&#347;&#263;"
  ]
  node [
    id 240
    label "pacjencja"
  ]
  node [
    id 241
    label "patience"
  ]
  node [
    id 242
    label "korab"
  ]
  node [
    id 243
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 244
    label "zr&#281;bnica"
  ]
  node [
    id 245
    label "odkotwiczenie"
  ]
  node [
    id 246
    label "cumowanie"
  ]
  node [
    id 247
    label "zadokowanie"
  ]
  node [
    id 248
    label "bumsztak"
  ]
  node [
    id 249
    label "zacumowanie"
  ]
  node [
    id 250
    label "dobi&#263;"
  ]
  node [
    id 251
    label "odkotwiczanie"
  ]
  node [
    id 252
    label "kotwica"
  ]
  node [
    id 253
    label "zwodowa&#263;"
  ]
  node [
    id 254
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 255
    label "zakotwiczenie"
  ]
  node [
    id 256
    label "&#347;r&#243;dokr&#281;cie"
  ]
  node [
    id 257
    label "dzi&#243;b"
  ]
  node [
    id 258
    label "w&#281;z&#322;&#243;wka"
  ]
  node [
    id 259
    label "armada"
  ]
  node [
    id 260
    label "grobla"
  ]
  node [
    id 261
    label "kad&#322;ub"
  ]
  node [
    id 262
    label "dobijanie"
  ]
  node [
    id 263
    label "odkotwicza&#263;"
  ]
  node [
    id 264
    label "proporczyk"
  ]
  node [
    id 265
    label "luk"
  ]
  node [
    id 266
    label "odcumowanie"
  ]
  node [
    id 267
    label "kabina"
  ]
  node [
    id 268
    label "skrajnik"
  ]
  node [
    id 269
    label "kotwiczenie"
  ]
  node [
    id 270
    label "zwodowanie"
  ]
  node [
    id 271
    label "szkutnictwo"
  ]
  node [
    id 272
    label "pojazd"
  ]
  node [
    id 273
    label "wodowanie"
  ]
  node [
    id 274
    label "zacumowa&#263;"
  ]
  node [
    id 275
    label "sterownik_automatyczny"
  ]
  node [
    id 276
    label "p&#322;ywa&#263;"
  ]
  node [
    id 277
    label "zadokowa&#263;"
  ]
  node [
    id 278
    label "zakotwiczy&#263;"
  ]
  node [
    id 279
    label "sztormtrap"
  ]
  node [
    id 280
    label "pok&#322;ad"
  ]
  node [
    id 281
    label "kotwiczy&#263;"
  ]
  node [
    id 282
    label "&#380;yroskop"
  ]
  node [
    id 283
    label "odcumowa&#263;"
  ]
  node [
    id 284
    label "dobicie"
  ]
  node [
    id 285
    label "armator"
  ]
  node [
    id 286
    label "odbijacz"
  ]
  node [
    id 287
    label "dziennik_pok&#322;adowy"
  ]
  node [
    id 288
    label "reling"
  ]
  node [
    id 289
    label "flota"
  ]
  node [
    id 290
    label "kabestan"
  ]
  node [
    id 291
    label "nadbud&#243;wka"
  ]
  node [
    id 292
    label "dokowa&#263;"
  ]
  node [
    id 293
    label "cumowa&#263;"
  ]
  node [
    id 294
    label "odkotwiczy&#263;"
  ]
  node [
    id 295
    label "dobija&#263;"
  ]
  node [
    id 296
    label "odcumowywanie"
  ]
  node [
    id 297
    label "ster"
  ]
  node [
    id 298
    label "szkielet_jednostki_p&#322;ywaj&#261;cej"
  ]
  node [
    id 299
    label "odcumowywa&#263;"
  ]
  node [
    id 300
    label "&#347;wiat&#322;o_kotwiczne"
  ]
  node [
    id 301
    label "futr&#243;wka"
  ]
  node [
    id 302
    label "dokowanie"
  ]
  node [
    id 303
    label "trap"
  ]
  node [
    id 304
    label "zaw&#243;r_denny"
  ]
  node [
    id 305
    label "rostra"
  ]
  node [
    id 306
    label "free"
  ]
  node [
    id 307
    label "si&#281;ga&#263;"
  ]
  node [
    id 308
    label "trwa&#263;"
  ]
  node [
    id 309
    label "obecno&#347;&#263;"
  ]
  node [
    id 310
    label "stan"
  ]
  node [
    id 311
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 312
    label "stand"
  ]
  node [
    id 313
    label "mie&#263;_miejsce"
  ]
  node [
    id 314
    label "uczestniczy&#263;"
  ]
  node [
    id 315
    label "chodzi&#263;"
  ]
  node [
    id 316
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 317
    label "equal"
  ]
  node [
    id 318
    label "zaw&#380;dy"
  ]
  node [
    id 319
    label "ci&#261;gle"
  ]
  node [
    id 320
    label "na_zawsze"
  ]
  node [
    id 321
    label "cz&#281;sto"
  ]
  node [
    id 322
    label "podp&#322;ywa&#263;"
  ]
  node [
    id 323
    label "podp&#322;ywanie"
  ]
  node [
    id 324
    label "plot"
  ]
  node [
    id 325
    label "podp&#322;yn&#261;&#263;"
  ]
  node [
    id 326
    label "piece"
  ]
  node [
    id 327
    label "kawa&#322;"
  ]
  node [
    id 328
    label "utw&#243;r"
  ]
  node [
    id 329
    label "podp&#322;yni&#281;cie"
  ]
  node [
    id 330
    label "dar_bo&#380;y"
  ]
  node [
    id 331
    label "bochenek"
  ]
  node [
    id 332
    label "pieczywo"
  ]
  node [
    id 333
    label "wypiek"
  ]
  node [
    id 334
    label "konsubstancjacja"
  ]
  node [
    id 335
    label "utrzymanie"
  ]
  node [
    id 336
    label "translate"
  ]
  node [
    id 337
    label "give"
  ]
  node [
    id 338
    label "poinformowa&#263;"
  ]
  node [
    id 339
    label "spowodowa&#263;"
  ]
  node [
    id 340
    label "service"
  ]
  node [
    id 341
    label "ZOMO"
  ]
  node [
    id 342
    label "czworak"
  ]
  node [
    id 343
    label "zesp&#243;&#322;"
  ]
  node [
    id 344
    label "s&#322;u&#380;&#261;ca"
  ]
  node [
    id 345
    label "instytucja"
  ]
  node [
    id 346
    label "s&#322;u&#380;&#261;cy"
  ]
  node [
    id 347
    label "wys&#322;uga"
  ]
  node [
    id 348
    label "kamaryla"
  ]
  node [
    id 349
    label "pole"
  ]
  node [
    id 350
    label "domownicy"
  ]
  node [
    id 351
    label "urz&#281;dnik_nadworny"
  ]
  node [
    id 352
    label "przestrze&#324;"
  ]
  node [
    id 353
    label "court"
  ]
  node [
    id 354
    label "zap&#322;ocie"
  ]
  node [
    id 355
    label "&#347;wita"
  ]
  node [
    id 356
    label "ziemianin"
  ]
  node [
    id 357
    label "siedziba"
  ]
  node [
    id 358
    label "maj&#261;tek"
  ]
  node [
    id 359
    label "cz&#322;owiek"
  ]
  node [
    id 360
    label "przedstawiciel"
  ]
  node [
    id 361
    label "szlachciura"
  ]
  node [
    id 362
    label "szlachta"
  ]
  node [
    id 363
    label "notabl"
  ]
  node [
    id 364
    label "nieszcz&#281;&#347;cie"
  ]
  node [
    id 365
    label "bias"
  ]
  node [
    id 366
    label "obelga"
  ]
  node [
    id 367
    label "strata"
  ]
  node [
    id 368
    label "tentegowa&#263;"
  ]
  node [
    id 369
    label "urz&#261;dza&#263;"
  ]
  node [
    id 370
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 371
    label "czyni&#263;"
  ]
  node [
    id 372
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 373
    label "post&#281;powa&#263;"
  ]
  node [
    id 374
    label "wydala&#263;"
  ]
  node [
    id 375
    label "oszukiwa&#263;"
  ]
  node [
    id 376
    label "organizowa&#263;"
  ]
  node [
    id 377
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 378
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 379
    label "work"
  ]
  node [
    id 380
    label "przerabia&#263;"
  ]
  node [
    id 381
    label "stylizowa&#263;"
  ]
  node [
    id 382
    label "falowa&#263;"
  ]
  node [
    id 383
    label "act"
  ]
  node [
    id 384
    label "peddle"
  ]
  node [
    id 385
    label "ukazywa&#263;"
  ]
  node [
    id 386
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 387
    label "profesor"
  ]
  node [
    id 388
    label "kszta&#322;ciciel"
  ]
  node [
    id 389
    label "jegomo&#347;&#263;"
  ]
  node [
    id 390
    label "zwrot"
  ]
  node [
    id 391
    label "pracodawca"
  ]
  node [
    id 392
    label "rz&#261;dzenie"
  ]
  node [
    id 393
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 394
    label "ch&#322;opina"
  ]
  node [
    id 395
    label "bratek"
  ]
  node [
    id 396
    label "doros&#322;y"
  ]
  node [
    id 397
    label "preceptor"
  ]
  node [
    id 398
    label "Midas"
  ]
  node [
    id 399
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 400
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 401
    label "murza"
  ]
  node [
    id 402
    label "androlog"
  ]
  node [
    id 403
    label "pupil"
  ]
  node [
    id 404
    label "efendi"
  ]
  node [
    id 405
    label "nabab"
  ]
  node [
    id 406
    label "w&#322;odarz"
  ]
  node [
    id 407
    label "szkolnik"
  ]
  node [
    id 408
    label "pedagog"
  ]
  node [
    id 409
    label "popularyzator"
  ]
  node [
    id 410
    label "andropauza"
  ]
  node [
    id 411
    label "gra_w_karty"
  ]
  node [
    id 412
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 413
    label "Mieszko_I"
  ]
  node [
    id 414
    label "bogaty"
  ]
  node [
    id 415
    label "samiec"
  ]
  node [
    id 416
    label "przyw&#243;dca"
  ]
  node [
    id 417
    label "pa&#324;stwo"
  ]
  node [
    id 418
    label "belfer"
  ]
  node [
    id 419
    label "piwo"
  ]
  node [
    id 420
    label "&#322;&#261;cznie"
  ]
  node [
    id 421
    label "funkcjonariusz"
  ]
  node [
    id 422
    label "nadzorca"
  ]
  node [
    id 423
    label "minion"
  ]
  node [
    id 424
    label "parobek"
  ]
  node [
    id 425
    label "byd&#322;o"
  ]
  node [
    id 426
    label "zobo"
  ]
  node [
    id 427
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 428
    label "yakalo"
  ]
  node [
    id 429
    label "dzo"
  ]
  node [
    id 430
    label "pomys&#322;odawca"
  ]
  node [
    id 431
    label "tworzyciel"
  ]
  node [
    id 432
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 433
    label "papa"
  ]
  node [
    id 434
    label "&#347;w"
  ]
  node [
    id 435
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 436
    label "zakonnik"
  ]
  node [
    id 437
    label "kuwada"
  ]
  node [
    id 438
    label "przodek"
  ]
  node [
    id 439
    label "wykonawca"
  ]
  node [
    id 440
    label "rodzice"
  ]
  node [
    id 441
    label "rodzic"
  ]
  node [
    id 442
    label "discipline"
  ]
  node [
    id 443
    label "control"
  ]
  node [
    id 444
    label "ustawia&#263;"
  ]
  node [
    id 445
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 446
    label "motywowa&#263;"
  ]
  node [
    id 447
    label "order"
  ]
  node [
    id 448
    label "administrowa&#263;"
  ]
  node [
    id 449
    label "manipulate"
  ]
  node [
    id 450
    label "indicate"
  ]
  node [
    id 451
    label "przeznacza&#263;"
  ]
  node [
    id 452
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 453
    label "match"
  ]
  node [
    id 454
    label "sterowa&#263;"
  ]
  node [
    id 455
    label "wysy&#322;a&#263;"
  ]
  node [
    id 456
    label "zwierzchnik"
  ]
  node [
    id 457
    label "zarz&#261;dza&#263;"
  ]
  node [
    id 458
    label "si&#322;a"
  ]
  node [
    id 459
    label "przymus"
  ]
  node [
    id 460
    label "rzuci&#263;"
  ]
  node [
    id 461
    label "hazard"
  ]
  node [
    id 462
    label "destiny"
  ]
  node [
    id 463
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 464
    label "bilet"
  ]
  node [
    id 465
    label "przebieg_&#380;ycia"
  ]
  node [
    id 466
    label "&#380;ycie"
  ]
  node [
    id 467
    label "rzucenie"
  ]
  node [
    id 468
    label "zna&#263;"
  ]
  node [
    id 469
    label "troska&#263;_si&#281;"
  ]
  node [
    id 470
    label "zachowywa&#263;"
  ]
  node [
    id 471
    label "chowa&#263;"
  ]
  node [
    id 472
    label "think"
  ]
  node [
    id 473
    label "recall"
  ]
  node [
    id 474
    label "echo"
  ]
  node [
    id 475
    label "take_care"
  ]
  node [
    id 476
    label "jaki&#347;"
  ]
  node [
    id 477
    label "get"
  ]
  node [
    id 478
    label "zwiastun"
  ]
  node [
    id 479
    label "si&#281;gn&#261;&#263;"
  ]
  node [
    id 480
    label "develop"
  ]
  node [
    id 481
    label "catch"
  ]
  node [
    id 482
    label "uzyska&#263;"
  ]
  node [
    id 483
    label "wzi&#261;&#263;"
  ]
  node [
    id 484
    label "naby&#263;"
  ]
  node [
    id 485
    label "nabawienie_si&#281;"
  ]
  node [
    id 486
    label "obskoczy&#263;"
  ]
  node [
    id 487
    label "zapanowa&#263;"
  ]
  node [
    id 488
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 489
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 490
    label "zrobi&#263;"
  ]
  node [
    id 491
    label "nabawianie_si&#281;"
  ]
  node [
    id 492
    label "range"
  ]
  node [
    id 493
    label "schorzenie"
  ]
  node [
    id 494
    label "wystarczy&#263;"
  ]
  node [
    id 495
    label "wysta&#263;"
  ]
  node [
    id 496
    label "trza"
  ]
  node [
    id 497
    label "necessity"
  ]
  node [
    id 498
    label "czu&#263;"
  ]
  node [
    id 499
    label "need"
  ]
  node [
    id 500
    label "hide"
  ]
  node [
    id 501
    label "support"
  ]
  node [
    id 502
    label "kobieta"
  ]
  node [
    id 503
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 504
    label "krewni"
  ]
  node [
    id 505
    label "altruista"
  ]
  node [
    id 506
    label "desire"
  ]
  node [
    id 507
    label "kcie&#263;"
  ]
  node [
    id 508
    label "insert"
  ]
  node [
    id 509
    label "plant"
  ]
  node [
    id 510
    label "umie&#347;ci&#263;"
  ]
  node [
    id 511
    label "uprawi&#263;"
  ]
  node [
    id 512
    label "gotowy"
  ]
  node [
    id 513
    label "might"
  ]
  node [
    id 514
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 515
    label "express"
  ]
  node [
    id 516
    label "rzekn&#261;&#263;"
  ]
  node [
    id 517
    label "okre&#347;li&#263;"
  ]
  node [
    id 518
    label "wyrazi&#263;"
  ]
  node [
    id 519
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 520
    label "unwrap"
  ]
  node [
    id 521
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 522
    label "convey"
  ]
  node [
    id 523
    label "discover"
  ]
  node [
    id 524
    label "wydoby&#263;"
  ]
  node [
    id 525
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 526
    label "poda&#263;"
  ]
  node [
    id 527
    label "Posejdon"
  ]
  node [
    id 528
    label "niebiosa"
  ]
  node [
    id 529
    label "Dionizos"
  ]
  node [
    id 530
    label "Waruna"
  ]
  node [
    id 531
    label "ofiarowanie"
  ]
  node [
    id 532
    label "Ereb"
  ]
  node [
    id 533
    label "Janus"
  ]
  node [
    id 534
    label "Neptun"
  ]
  node [
    id 535
    label "Sylen"
  ]
  node [
    id 536
    label "Hesperos"
  ]
  node [
    id 537
    label "igrzyska_greckie"
  ]
  node [
    id 538
    label "s&#261;d_ostateczny"
  ]
  node [
    id 539
    label "politeizm"
  ]
  node [
    id 540
    label "Kupidyn"
  ]
  node [
    id 541
    label "istota_nadprzyrodzona"
  ]
  node [
    id 542
    label "Bachus"
  ]
  node [
    id 543
    label "osoba"
  ]
  node [
    id 544
    label "ofiarowa&#263;"
  ]
  node [
    id 545
    label "tr&#243;jca"
  ]
  node [
    id 546
    label "Boreasz"
  ]
  node [
    id 547
    label "miernota"
  ]
  node [
    id 548
    label "g&#243;wno"
  ]
  node [
    id 549
    label "love"
  ]
  node [
    id 550
    label "ilo&#347;&#263;"
  ]
  node [
    id 551
    label "brak"
  ]
  node [
    id 552
    label "ciura"
  ]
  node [
    id 553
    label "remark"
  ]
  node [
    id 554
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 555
    label "u&#380;ywa&#263;"
  ]
  node [
    id 556
    label "okre&#347;la&#263;"
  ]
  node [
    id 557
    label "j&#281;zyk"
  ]
  node [
    id 558
    label "say"
  ]
  node [
    id 559
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 560
    label "formu&#322;owa&#263;"
  ]
  node [
    id 561
    label "talk"
  ]
  node [
    id 562
    label "powiada&#263;"
  ]
  node [
    id 563
    label "informowa&#263;"
  ]
  node [
    id 564
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 565
    label "wydobywa&#263;"
  ]
  node [
    id 566
    label "chew_the_fat"
  ]
  node [
    id 567
    label "dysfonia"
  ]
  node [
    id 568
    label "umie&#263;"
  ]
  node [
    id 569
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 570
    label "tell"
  ]
  node [
    id 571
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 572
    label "wyra&#380;a&#263;"
  ]
  node [
    id 573
    label "gaworzy&#263;"
  ]
  node [
    id 574
    label "rozmawia&#263;"
  ]
  node [
    id 575
    label "dziama&#263;"
  ]
  node [
    id 576
    label "prawi&#263;"
  ]
  node [
    id 577
    label "czyj&#347;"
  ]
  node [
    id 578
    label "die"
  ]
  node [
    id 579
    label "zostawi&#263;"
  ]
  node [
    id 580
    label "godzina"
  ]
  node [
    id 581
    label "stulecie"
  ]
  node [
    id 582
    label "kalendarz"
  ]
  node [
    id 583
    label "pora_roku"
  ]
  node [
    id 584
    label "cykl_astronomiczny"
  ]
  node [
    id 585
    label "p&#243;&#322;rocze"
  ]
  node [
    id 586
    label "grupa"
  ]
  node [
    id 587
    label "kwarta&#322;"
  ]
  node [
    id 588
    label "kurs"
  ]
  node [
    id 589
    label "jubileusz"
  ]
  node [
    id 590
    label "miesi&#261;c"
  ]
  node [
    id 591
    label "lata"
  ]
  node [
    id 592
    label "martwy_sezon"
  ]
  node [
    id 593
    label "formacja_geologiczna"
  ]
  node [
    id 594
    label "vein"
  ]
  node [
    id 595
    label "chciwiec"
  ]
  node [
    id 596
    label "dost&#281;p_do&#380;ylny"
  ]
  node [
    id 597
    label "wymagaj&#261;cy"
  ]
  node [
    id 598
    label "lina"
  ]
  node [
    id 599
    label "materialista"
  ]
  node [
    id 600
    label "naczynie"
  ]
  node [
    id 601
    label "okrutnik"
  ]
  node [
    id 602
    label "sk&#261;piarz"
  ]
  node [
    id 603
    label "przew&#243;d"
  ]
  node [
    id 604
    label "sk&#261;py"
  ]
  node [
    id 605
    label "atleta"
  ]
  node [
    id 606
    label "help"
  ]
  node [
    id 607
    label "aid"
  ]
  node [
    id 608
    label "u&#322;atwi&#263;"
  ]
  node [
    id 609
    label "concur"
  ]
  node [
    id 610
    label "zaskutkowa&#263;"
  ]
  node [
    id 611
    label "ubocze"
  ]
  node [
    id 612
    label "wie&#347;"
  ]
  node [
    id 613
    label "nation"
  ]
  node [
    id 614
    label "uspokojenie"
  ]
  node [
    id 615
    label "uspokojenie_si&#281;"
  ]
  node [
    id 616
    label "niezak&#322;&#243;cony"
  ]
  node [
    id 617
    label "przyjemny"
  ]
  node [
    id 618
    label "wolny"
  ]
  node [
    id 619
    label "spokojnie"
  ]
  node [
    id 620
    label "cicho"
  ]
  node [
    id 621
    label "nietrudny"
  ]
  node [
    id 622
    label "bezproblemowy"
  ]
  node [
    id 623
    label "uspokajanie_si&#281;"
  ]
  node [
    id 624
    label "uspokajanie"
  ]
  node [
    id 625
    label "znaczenie"
  ]
  node [
    id 626
    label "ziemia"
  ]
  node [
    id 627
    label "sk&#322;ad"
  ]
  node [
    id 628
    label "zastosowanie"
  ]
  node [
    id 629
    label "zreinterpretowa&#263;"
  ]
  node [
    id 630
    label "zreinterpretowanie"
  ]
  node [
    id 631
    label "function"
  ]
  node [
    id 632
    label "zagranie"
  ]
  node [
    id 633
    label "p&#322;osa"
  ]
  node [
    id 634
    label "plik"
  ]
  node [
    id 635
    label "cel"
  ]
  node [
    id 636
    label "reinterpretowanie"
  ]
  node [
    id 637
    label "tekst"
  ]
  node [
    id 638
    label "wykonywa&#263;"
  ]
  node [
    id 639
    label "uprawienie"
  ]
  node [
    id 640
    label "gra&#263;"
  ]
  node [
    id 641
    label "radlina"
  ]
  node [
    id 642
    label "ustawi&#263;"
  ]
  node [
    id 643
    label "irygowa&#263;"
  ]
  node [
    id 644
    label "wrench"
  ]
  node [
    id 645
    label "irygowanie"
  ]
  node [
    id 646
    label "dialog"
  ]
  node [
    id 647
    label "zagon"
  ]
  node [
    id 648
    label "scenariusz"
  ]
  node [
    id 649
    label "zagra&#263;"
  ]
  node [
    id 650
    label "kszta&#322;t"
  ]
  node [
    id 651
    label "pierwszoplanowo&#347;&#263;"
  ]
  node [
    id 652
    label "ustawienie"
  ]
  node [
    id 653
    label "czyn"
  ]
  node [
    id 654
    label "gospodarstwo"
  ]
  node [
    id 655
    label "reinterpretowa&#263;"
  ]
  node [
    id 656
    label "granie"
  ]
  node [
    id 657
    label "wykonywanie"
  ]
  node [
    id 658
    label "kostium"
  ]
  node [
    id 659
    label "aktorstwo"
  ]
  node [
    id 660
    label "posta&#263;"
  ]
  node [
    id 661
    label "str&#243;&#380;y&#263;"
  ]
  node [
    id 662
    label "continue"
  ]
  node [
    id 663
    label "poczciwiec"
  ]
  node [
    id 664
    label "nacisn&#261;&#263;"
  ]
  node [
    id 665
    label "um&#281;czy&#263;"
  ]
  node [
    id 666
    label "Matka_Boska"
  ]
  node [
    id 667
    label "matka_zast&#281;pcza"
  ]
  node [
    id 668
    label "stara"
  ]
  node [
    id 669
    label "matczysko"
  ]
  node [
    id 670
    label "ro&#347;lina"
  ]
  node [
    id 671
    label "pi&#322;ka_palantowa"
  ]
  node [
    id 672
    label "gracz"
  ]
  node [
    id 673
    label "zawodnik"
  ]
  node [
    id 674
    label "macierz"
  ]
  node [
    id 675
    label "owad"
  ]
  node [
    id 676
    label "przyczyna"
  ]
  node [
    id 677
    label "macocha"
  ]
  node [
    id 678
    label "dwa_ognie"
  ]
  node [
    id 679
    label "staruszka"
  ]
  node [
    id 680
    label "kom&#243;rka_jajowa"
  ]
  node [
    id 681
    label "rozsadnik"
  ]
  node [
    id 682
    label "zakonnica"
  ]
  node [
    id 683
    label "obiekt"
  ]
  node [
    id 684
    label "samica"
  ]
  node [
    id 685
    label "przodkini"
  ]
  node [
    id 686
    label "znowu"
  ]
  node [
    id 687
    label "ponowny"
  ]
  node [
    id 688
    label "opu&#347;ci&#263;"
  ]
  node [
    id 689
    label "poruszy&#263;_si&#281;"
  ]
  node [
    id 690
    label "proceed"
  ]
  node [
    id 691
    label "powie&#347;&#263;_si&#281;"
  ]
  node [
    id 692
    label "popsu&#263;_si&#281;"
  ]
  node [
    id 693
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 694
    label "kopn&#261;&#263;_si&#281;"
  ]
  node [
    id 695
    label "zacz&#261;&#263;"
  ]
  node [
    id 696
    label "zmieni&#263;"
  ]
  node [
    id 697
    label "sail"
  ]
  node [
    id 698
    label "leave"
  ]
  node [
    id 699
    label "uda&#263;_si&#281;"
  ]
  node [
    id 700
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 701
    label "wyp&#322;yn&#261;&#263;"
  ]
  node [
    id 702
    label "sprzeda&#263;_si&#281;"
  ]
  node [
    id 703
    label "przyj&#261;&#263;"
  ]
  node [
    id 704
    label "potoczy&#263;_si&#281;"
  ]
  node [
    id 705
    label "become"
  ]
  node [
    id 706
    label "play_along"
  ]
  node [
    id 707
    label "travel"
  ]
  node [
    id 708
    label "pan_domu"
  ]
  node [
    id 709
    label "ch&#322;op"
  ]
  node [
    id 710
    label "ma&#322;&#380;onek"
  ]
  node [
    id 711
    label "&#347;lubny"
  ]
  node [
    id 712
    label "pan_i_w&#322;adca"
  ]
  node [
    id 713
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 714
    label "pan_m&#322;ody"
  ]
  node [
    id 715
    label "&#380;onaty"
  ]
  node [
    id 716
    label "w_chuj"
  ]
  node [
    id 717
    label "berylowiec"
  ]
  node [
    id 718
    label "u&#347;miechni&#281;ty"
  ]
  node [
    id 719
    label "pierwiastek_promieniotw&#243;rczy"
  ]
  node [
    id 720
    label "mikroradian"
  ]
  node [
    id 721
    label "zadowolenie_si&#281;"
  ]
  node [
    id 722
    label "usatysfakcjonowanie_si&#281;"
  ]
  node [
    id 723
    label "content"
  ]
  node [
    id 724
    label "jednostka_promieniowania"
  ]
  node [
    id 725
    label "miliradian"
  ]
  node [
    id 726
    label "jednostka"
  ]
  node [
    id 727
    label "garderoba"
  ]
  node [
    id 728
    label "wiecha"
  ]
  node [
    id 729
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 730
    label "budynek"
  ]
  node [
    id 731
    label "fratria"
  ]
  node [
    id 732
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 733
    label "poj&#281;cie"
  ]
  node [
    id 734
    label "rodzina"
  ]
  node [
    id 735
    label "substancja_mieszkaniowa"
  ]
  node [
    id 736
    label "dom_rodzinny"
  ]
  node [
    id 737
    label "stead"
  ]
  node [
    id 738
    label "zmusza&#263;"
  ]
  node [
    id 739
    label "uniemo&#380;liwia&#263;"
  ]
  node [
    id 740
    label "sympatyzowa&#263;"
  ]
  node [
    id 741
    label "pozostawa&#263;"
  ]
  node [
    id 742
    label "utrzymywa&#263;"
  ]
  node [
    id 743
    label "utrzymywa&#263;_si&#281;"
  ]
  node [
    id 744
    label "treat"
  ]
  node [
    id 745
    label "przetrzymywa&#263;"
  ]
  node [
    id 746
    label "sprawowa&#263;"
  ]
  node [
    id 747
    label "adhere"
  ]
  node [
    id 748
    label "dzier&#380;y&#263;"
  ]
  node [
    id 749
    label "podtrzymywa&#263;"
  ]
  node [
    id 750
    label "argue"
  ]
  node [
    id 751
    label "hodowa&#263;"
  ]
  node [
    id 752
    label "wychowywa&#263;"
  ]
  node [
    id 753
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 754
    label "ho&#322;ysz"
  ]
  node [
    id 755
    label "ubo&#380;enie"
  ]
  node [
    id 756
    label "zubo&#380;anie"
  ]
  node [
    id 757
    label "prosty"
  ]
  node [
    id 758
    label "biedota"
  ]
  node [
    id 759
    label "zubo&#380;enie"
  ]
  node [
    id 760
    label "biedny"
  ]
  node [
    id 761
    label "bankrutowanie"
  ]
  node [
    id 762
    label "proletariusz"
  ]
  node [
    id 763
    label "go&#322;odupiec"
  ]
  node [
    id 764
    label "biednie"
  ]
  node [
    id 765
    label "ubogo"
  ]
  node [
    id 766
    label "zbiednienie"
  ]
  node [
    id 767
    label "sytuowany"
  ]
  node [
    id 768
    label "raw_material"
  ]
  node [
    id 769
    label "poczeka&#263;"
  ]
  node [
    id 770
    label "pozosta&#263;"
  ]
  node [
    id 771
    label "draw"
  ]
  node [
    id 772
    label "potomstwo"
  ]
  node [
    id 773
    label "organizm"
  ]
  node [
    id 774
    label "sraluch"
  ]
  node [
    id 775
    label "utulanie"
  ]
  node [
    id 776
    label "pediatra"
  ]
  node [
    id 777
    label "dzieciarnia"
  ]
  node [
    id 778
    label "m&#322;odziak"
  ]
  node [
    id 779
    label "dzieciak"
  ]
  node [
    id 780
    label "utula&#263;"
  ]
  node [
    id 781
    label "potomek"
  ]
  node [
    id 782
    label "entliczek-pentliczek"
  ]
  node [
    id 783
    label "pedofil"
  ]
  node [
    id 784
    label "m&#322;odzik"
  ]
  node [
    id 785
    label "cz&#322;owieczek"
  ]
  node [
    id 786
    label "zwierz&#281;"
  ]
  node [
    id 787
    label "niepe&#322;noletni"
  ]
  node [
    id 788
    label "fledgling"
  ]
  node [
    id 789
    label "utuli&#263;"
  ]
  node [
    id 790
    label "utulenie"
  ]
  node [
    id 791
    label "ba&#322;wan"
  ]
  node [
    id 792
    label "uwielbienie"
  ]
  node [
    id 793
    label "idol"
  ]
  node [
    id 794
    label "ofiarowywanie"
  ]
  node [
    id 795
    label "gigant"
  ]
  node [
    id 796
    label "ofiarowywa&#263;"
  ]
  node [
    id 797
    label "istota_&#380;ywa"
  ]
  node [
    id 798
    label "osierocanie"
  ]
  node [
    id 799
    label "dupa_wo&#322;owa"
  ]
  node [
    id 800
    label "wiersz"
  ]
  node [
    id 801
    label "nieszcz&#281;&#347;nik"
  ]
  node [
    id 802
    label "osierocenie"
  ]
  node [
    id 803
    label "po&#347;miewisko"
  ]
  node [
    id 804
    label "omija&#263;"
  ]
  node [
    id 805
    label "obni&#380;a&#263;"
  ]
  node [
    id 806
    label "pozostawia&#263;"
  ]
  node [
    id 807
    label "potania&#263;"
  ]
  node [
    id 808
    label "przestawa&#263;"
  ]
  node [
    id 809
    label "abort"
  ]
  node [
    id 810
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 811
    label "traci&#263;"
  ]
  node [
    id 812
    label "forowa&#263;_si&#281;"
  ]
  node [
    id 813
    label "proszek"
  ]
  node [
    id 814
    label "urz&#281;dnik_s&#261;dowy"
  ]
  node [
    id 815
    label "pedel"
  ]
  node [
    id 816
    label "dozorca"
  ]
  node [
    id 817
    label "urz&#281;dnik_ziemski"
  ]
  node [
    id 818
    label "Soplicowo"
  ]
  node [
    id 819
    label "ermita&#380;"
  ]
  node [
    id 820
    label "grz&#261;dka"
  ]
  node [
    id 821
    label "trelia&#380;"
  ]
  node [
    id 822
    label "grota_ogrodowa"
  ]
  node [
    id 823
    label "podw&#243;rze"
  ]
  node [
    id 824
    label "inspekt"
  ]
  node [
    id 825
    label "klomb"
  ]
  node [
    id 826
    label "kulisa"
  ]
  node [
    id 827
    label "teren_zielony"
  ]
  node [
    id 828
    label "consume"
  ]
  node [
    id 829
    label "zaj&#261;&#263;"
  ]
  node [
    id 830
    label "przenie&#347;&#263;"
  ]
  node [
    id 831
    label "z&#322;apa&#263;"
  ]
  node [
    id 832
    label "przesun&#261;&#263;"
  ]
  node [
    id 833
    label "deprive"
  ]
  node [
    id 834
    label "poci&#261;gn&#261;&#263;"
  ]
  node [
    id 835
    label "abstract"
  ]
  node [
    id 836
    label "withdraw"
  ]
  node [
    id 837
    label "doprowadzi&#263;"
  ]
  node [
    id 838
    label "dzie&#324;"
  ]
  node [
    id 839
    label "zako&#324;cza&#263;"
  ]
  node [
    id 840
    label "satisfy"
  ]
  node [
    id 841
    label "close"
  ]
  node [
    id 842
    label "determine"
  ]
  node [
    id 843
    label "dokonywa&#263;_&#380;ywota"
  ]
  node [
    id 844
    label "stanowi&#263;"
  ]
  node [
    id 845
    label "ukaza&#263;_si&#281;_drukiem"
  ]
  node [
    id 846
    label "zej&#347;&#263;"
  ]
  node [
    id 847
    label "okaza&#263;_si&#281;"
  ]
  node [
    id 848
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 849
    label "sko&#324;czy&#263;"
  ]
  node [
    id 850
    label "ograniczenie"
  ]
  node [
    id 851
    label "ruszy&#263;"
  ]
  node [
    id 852
    label "wypa&#347;&#263;"
  ]
  node [
    id 853
    label "uko&#324;czy&#263;"
  ]
  node [
    id 854
    label "open"
  ]
  node [
    id 855
    label "moderate"
  ]
  node [
    id 856
    label "wydosta&#263;_si&#281;"
  ]
  node [
    id 857
    label "wywie&#347;&#263;_si&#281;"
  ]
  node [
    id 858
    label "mount"
  ]
  node [
    id 859
    label "drive"
  ]
  node [
    id 860
    label "zademonstrowa&#263;"
  ]
  node [
    id 861
    label "perform"
  ]
  node [
    id 862
    label "uwolni&#263;_si&#281;"
  ]
  node [
    id 863
    label "drop"
  ]
  node [
    id 864
    label "dziwnie"
  ]
  node [
    id 865
    label "w_miar&#281;"
  ]
  node [
    id 866
    label "przyzwoicie"
  ]
  node [
    id 867
    label "jako_taki"
  ]
  node [
    id 868
    label "nie&#378;le"
  ]
  node [
    id 869
    label "istota_fantastyczna"
  ]
  node [
    id 870
    label "whole"
  ]
  node [
    id 871
    label "Rzym_Zachodni"
  ]
  node [
    id 872
    label "element"
  ]
  node [
    id 873
    label "urz&#261;dzenie"
  ]
  node [
    id 874
    label "Rzym_Wschodni"
  ]
  node [
    id 875
    label "cierpki"
  ]
  node [
    id 876
    label "alkoholowy"
  ]
  node [
    id 877
    label "przewinienie"
  ]
  node [
    id 878
    label "sprawca"
  ]
  node [
    id 879
    label "odpowiadanie"
  ]
  node [
    id 880
    label "ma&#322;o"
  ]
  node [
    id 881
    label "nielicznie"
  ]
  node [
    id 882
    label "niewa&#380;ny"
  ]
  node [
    id 883
    label "zgodzi&#263;"
  ]
  node [
    id 884
    label "pomocnik"
  ]
  node [
    id 885
    label "doch&#243;d"
  ]
  node [
    id 886
    label "property"
  ]
  node [
    id 887
    label "przedmiot"
  ]
  node [
    id 888
    label "telefon_zaufania"
  ]
  node [
    id 889
    label "darowizna"
  ]
  node [
    id 890
    label "&#347;rodek"
  ]
  node [
    id 891
    label "liga"
  ]
  node [
    id 892
    label "bless"
  ]
  node [
    id 893
    label "laud"
  ]
  node [
    id 894
    label "os&#322;awi&#263;"
  ]
  node [
    id 895
    label "zaaprobowa&#263;"
  ]
  node [
    id 896
    label "pochwali&#263;"
  ]
  node [
    id 897
    label "nijaki"
  ]
  node [
    id 898
    label "wnucz&#281;"
  ]
  node [
    id 899
    label "sprzeciw"
  ]
  node [
    id 900
    label "komplement"
  ]
  node [
    id 901
    label "etykieta"
  ]
  node [
    id 902
    label "courage"
  ]
  node [
    id 903
    label "mi&#281;sie&#324;"
  ]
  node [
    id 904
    label "kompleks"
  ]
  node [
    id 905
    label "sfera_afektywna"
  ]
  node [
    id 906
    label "heart"
  ]
  node [
    id 907
    label "sumienie"
  ]
  node [
    id 908
    label "przedsionek"
  ]
  node [
    id 909
    label "entity"
  ]
  node [
    id 910
    label "nastawienie"
  ]
  node [
    id 911
    label "punkt"
  ]
  node [
    id 912
    label "kompleksja"
  ]
  node [
    id 913
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 914
    label "kier"
  ]
  node [
    id 915
    label "systol"
  ]
  node [
    id 916
    label "pikawa"
  ]
  node [
    id 917
    label "power"
  ]
  node [
    id 918
    label "zastawka"
  ]
  node [
    id 919
    label "psychika"
  ]
  node [
    id 920
    label "wola"
  ]
  node [
    id 921
    label "struna_&#347;ci&#281;gnista"
  ]
  node [
    id 922
    label "komora"
  ]
  node [
    id 923
    label "zapalno&#347;&#263;"
  ]
  node [
    id 924
    label "wsierdzie"
  ]
  node [
    id 925
    label "mi&#281;sie&#324;_sercowy"
  ]
  node [
    id 926
    label "podekscytowanie"
  ]
  node [
    id 927
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 928
    label "strunowiec"
  ]
  node [
    id 929
    label "favor"
  ]
  node [
    id 930
    label "dusza"
  ]
  node [
    id 931
    label "fizjonomia"
  ]
  node [
    id 932
    label "uk&#322;ad_przedsionkowo-komorowy"
  ]
  node [
    id 933
    label "przegroda_mi&#281;dzykomorowa"
  ]
  node [
    id 934
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 935
    label "charakter"
  ]
  node [
    id 936
    label "mikrokosmos"
  ]
  node [
    id 937
    label "dzwon"
  ]
  node [
    id 938
    label "koniuszek_serca"
  ]
  node [
    id 939
    label "pozytywno&#347;&#263;"
  ]
  node [
    id 940
    label "passion"
  ]
  node [
    id 941
    label "cecha"
  ]
  node [
    id 942
    label "pulsowa&#263;"
  ]
  node [
    id 943
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 944
    label "organ"
  ]
  node [
    id 945
    label "ego"
  ]
  node [
    id 946
    label "w&#281;ze&#322;_zatokowo-przedsionkowy"
  ]
  node [
    id 947
    label "kardiografia"
  ]
  node [
    id 948
    label "osobowo&#347;&#263;"
  ]
  node [
    id 949
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 950
    label "karta"
  ]
  node [
    id 951
    label "zatoka_wie&#324;cowa"
  ]
  node [
    id 952
    label "dobro&#263;"
  ]
  node [
    id 953
    label "podroby"
  ]
  node [
    id 954
    label "mi&#281;sie&#324;_brodawkowaty"
  ]
  node [
    id 955
    label "pulsowanie"
  ]
  node [
    id 956
    label "deformowa&#263;"
  ]
  node [
    id 957
    label "&#347;r&#243;dsierdzie"
  ]
  node [
    id 958
    label "seksualno&#347;&#263;"
  ]
  node [
    id 959
    label "deformowanie"
  ]
  node [
    id 960
    label "uk&#322;ad_krwiono&#347;ny"
  ]
  node [
    id 961
    label "elektrokardiografia"
  ]
  node [
    id 962
    label "wym&#243;g"
  ]
  node [
    id 963
    label "pragnienie"
  ]
  node [
    id 964
    label "sytuacja"
  ]
  node [
    id 965
    label "okre&#347;lony"
  ]
  node [
    id 966
    label "moneta"
  ]
  node [
    id 967
    label "potop_szwedzki"
  ]
  node [
    id 968
    label "wylewa&#263;"
  ]
  node [
    id 969
    label "run"
  ]
  node [
    id 970
    label "pieni&#261;dze"
  ]
  node [
    id 971
    label "sp&#322;ywa&#263;"
  ]
  node [
    id 972
    label "leak"
  ]
  node [
    id 973
    label "carry"
  ]
  node [
    id 974
    label "rezerwat"
  ]
  node [
    id 975
    label "Amerykanin"
  ]
  node [
    id 976
    label "zaczerwienienie"
  ]
  node [
    id 977
    label "Tito"
  ]
  node [
    id 978
    label "Gomu&#322;ka"
  ]
  node [
    id 979
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 980
    label "Gierek"
  ]
  node [
    id 981
    label "dojrza&#322;y"
  ]
  node [
    id 982
    label "czerwono"
  ]
  node [
    id 983
    label "rozpalanie_si&#281;"
  ]
  node [
    id 984
    label "kra&#347;ny"
  ]
  node [
    id 985
    label "czerwienienie"
  ]
  node [
    id 986
    label "rozpalenie_si&#281;"
  ]
  node [
    id 987
    label "lewactwo"
  ]
  node [
    id 988
    label "Bre&#380;niew"
  ]
  node [
    id 989
    label "ciep&#322;y"
  ]
  node [
    id 990
    label "Bierut"
  ]
  node [
    id 991
    label "Stalin"
  ]
  node [
    id 992
    label "czerwienienie_si&#281;"
  ]
  node [
    id 993
    label "Fidel_Castro"
  ]
  node [
    id 994
    label "reformator"
  ]
  node [
    id 995
    label "radyka&#322;"
  ]
  node [
    id 996
    label "demokrata"
  ]
  node [
    id 997
    label "dzia&#322;acz"
  ]
  node [
    id 998
    label "komunizowanie"
  ]
  node [
    id 999
    label "rozpalony"
  ]
  node [
    id 1000
    label "rumiany"
  ]
  node [
    id 1001
    label "lewicowiec"
  ]
  node [
    id 1002
    label "Polak"
  ]
  node [
    id 1003
    label "komuszek"
  ]
  node [
    id 1004
    label "sczerwienienie"
  ]
  node [
    id 1005
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 1006
    label "Chruszczow"
  ]
  node [
    id 1007
    label "Mao"
  ]
  node [
    id 1008
    label "lewicowy"
  ]
  node [
    id 1009
    label "zaczerwienienie_si&#281;"
  ]
  node [
    id 1010
    label "skomunizowanie"
  ]
  node [
    id 1011
    label "tubylec"
  ]
  node [
    id 1012
    label "szlachetny"
  ]
  node [
    id 1013
    label "metaliczny"
  ]
  node [
    id 1014
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 1015
    label "z&#322;oci&#347;cie"
  ]
  node [
    id 1016
    label "grosz"
  ]
  node [
    id 1017
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 1018
    label "utytu&#322;owany"
  ]
  node [
    id 1019
    label "poz&#322;ocenie"
  ]
  node [
    id 1020
    label "Polska"
  ]
  node [
    id 1021
    label "wspania&#322;y"
  ]
  node [
    id 1022
    label "doskona&#322;y"
  ]
  node [
    id 1023
    label "kochany"
  ]
  node [
    id 1024
    label "jednostka_monetarna"
  ]
  node [
    id 1025
    label "z&#322;ocenie"
  ]
  node [
    id 1026
    label "da&#263;_w_ko&#347;&#263;"
  ]
  node [
    id 1027
    label "przedstawi&#263;"
  ]
  node [
    id 1028
    label "testify"
  ]
  node [
    id 1029
    label "przeszkoli&#263;"
  ]
  node [
    id 1030
    label "udowodni&#263;"
  ]
  node [
    id 1031
    label "point"
  ]
  node [
    id 1032
    label "ciotka"
  ]
  node [
    id 1033
    label "partnerka"
  ]
  node [
    id 1034
    label "frymark"
  ]
  node [
    id 1035
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 1036
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 1037
    label "commodity"
  ]
  node [
    id 1038
    label "mienie"
  ]
  node [
    id 1039
    label "Wilko"
  ]
  node [
    id 1040
    label "centym"
  ]
  node [
    id 1041
    label "pomoc_domowa"
  ]
  node [
    id 1042
    label "tutor"
  ]
  node [
    id 1043
    label "akademik"
  ]
  node [
    id 1044
    label "immatrykulowanie"
  ]
  node [
    id 1045
    label "s&#322;uchacz"
  ]
  node [
    id 1046
    label "immatrykulowa&#263;"
  ]
  node [
    id 1047
    label "absolwent"
  ]
  node [
    id 1048
    label "indeks"
  ]
  node [
    id 1049
    label "dyro"
  ]
  node [
    id 1050
    label "dyrektoriat"
  ]
  node [
    id 1051
    label "dyrygent"
  ]
  node [
    id 1052
    label "Tadeusz_Rydzyk"
  ]
  node [
    id 1053
    label "zapi&#281;tek"
  ]
  node [
    id 1054
    label "zel&#243;wka"
  ]
  node [
    id 1055
    label "cholewka"
  ]
  node [
    id 1056
    label "wytw&#243;r"
  ]
  node [
    id 1057
    label "wzu&#263;"
  ]
  node [
    id 1058
    label "obcas"
  ]
  node [
    id 1059
    label "raki"
  ]
  node [
    id 1060
    label "sznurowad&#322;o"
  ]
  node [
    id 1061
    label "wzucie"
  ]
  node [
    id 1062
    label "napi&#281;tek"
  ]
  node [
    id 1063
    label "podeszwa"
  ]
  node [
    id 1064
    label "obuwie"
  ]
  node [
    id 1065
    label "wzuwanie"
  ]
  node [
    id 1066
    label "rozbijarka"
  ]
  node [
    id 1067
    label "cholewa"
  ]
  node [
    id 1068
    label "przyszwa"
  ]
  node [
    id 1069
    label "wyczyszcza&#263;"
  ]
  node [
    id 1070
    label "polish"
  ]
  node [
    id 1071
    label "authorize"
  ]
  node [
    id 1072
    label "zabiera&#263;"
  ]
  node [
    id 1073
    label "usuwa&#263;"
  ]
  node [
    id 1074
    label "uwalnia&#263;"
  ]
  node [
    id 1075
    label "purge"
  ]
  node [
    id 1076
    label "powodowa&#263;"
  ]
  node [
    id 1077
    label "rozwolnienie"
  ]
  node [
    id 1078
    label "oczyszcza&#263;"
  ]
  node [
    id 1079
    label "&#347;wi&#261;tynia"
  ]
  node [
    id 1080
    label "zakrystia"
  ]
  node [
    id 1081
    label "organizacja_religijna"
  ]
  node [
    id 1082
    label "nawa"
  ]
  node [
    id 1083
    label "nerwica_eklezjogenna"
  ]
  node [
    id 1084
    label "kropielnica"
  ]
  node [
    id 1085
    label "prezbiterium"
  ]
  node [
    id 1086
    label "wsp&#243;lnota"
  ]
  node [
    id 1087
    label "church"
  ]
  node [
    id 1088
    label "kruchta"
  ]
  node [
    id 1089
    label "Ska&#322;ka"
  ]
  node [
    id 1090
    label "kult"
  ]
  node [
    id 1091
    label "ub&#322;agalnia"
  ]
  node [
    id 1092
    label "zwi&#261;zek"
  ]
  node [
    id 1093
    label "towarzystwo"
  ]
  node [
    id 1094
    label "Bractwo_R&#243;&#380;a&#324;cowe"
  ]
  node [
    id 1095
    label "family"
  ]
  node [
    id 1096
    label "Chewra_Kadisza"
  ]
  node [
    id 1097
    label "wygadywa&#263;_si&#281;"
  ]
  node [
    id 1098
    label "chwali&#263;"
  ]
  node [
    id 1099
    label "spill_the_beans"
  ]
  node [
    id 1100
    label "chant"
  ]
  node [
    id 1101
    label "muzykowa&#263;"
  ]
  node [
    id 1102
    label "rozg&#322;asza&#263;"
  ]
  node [
    id 1103
    label "pia&#263;"
  ]
  node [
    id 1104
    label "os&#322;awia&#263;"
  ]
  node [
    id 1105
    label "weed"
  ]
  node [
    id 1106
    label "marny"
  ]
  node [
    id 1107
    label "nieznaczny"
  ]
  node [
    id 1108
    label "s&#322;aby"
  ]
  node [
    id 1109
    label "ch&#322;opiec"
  ]
  node [
    id 1110
    label "n&#281;dznie"
  ]
  node [
    id 1111
    label "przeci&#281;tny"
  ]
  node [
    id 1112
    label "nieliczny"
  ]
  node [
    id 1113
    label "wstydliwy"
  ]
  node [
    id 1114
    label "szybki"
  ]
  node [
    id 1115
    label "m&#322;ody"
  ]
  node [
    id 1116
    label "stosunek_pracy"
  ]
  node [
    id 1117
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 1118
    label "benedykty&#324;ski"
  ]
  node [
    id 1119
    label "pracowanie"
  ]
  node [
    id 1120
    label "zaw&#243;d"
  ]
  node [
    id 1121
    label "kierownictwo"
  ]
  node [
    id 1122
    label "zmiana"
  ]
  node [
    id 1123
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 1124
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 1125
    label "tynkarski"
  ]
  node [
    id 1126
    label "czynnik_produkcji"
  ]
  node [
    id 1127
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 1128
    label "zobowi&#261;zanie"
  ]
  node [
    id 1129
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 1130
    label "czynno&#347;&#263;"
  ]
  node [
    id 1131
    label "tyrka"
  ]
  node [
    id 1132
    label "pracowa&#263;"
  ]
  node [
    id 1133
    label "poda&#380;_pracy"
  ]
  node [
    id 1134
    label "miejsce"
  ]
  node [
    id 1135
    label "zak&#322;ad"
  ]
  node [
    id 1136
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 1137
    label "najem"
  ]
  node [
    id 1138
    label "przyda&#263;_si&#281;"
  ]
  node [
    id 1139
    label "zajmowa&#263;"
  ]
  node [
    id 1140
    label "sta&#263;"
  ]
  node [
    id 1141
    label "przebywa&#263;"
  ]
  node [
    id 1142
    label "room"
  ]
  node [
    id 1143
    label "panowa&#263;"
  ]
  node [
    id 1144
    label "fall"
  ]
  node [
    id 1145
    label "sklep"
  ]
  node [
    id 1146
    label "typ"
  ]
  node [
    id 1147
    label "warstwa"
  ]
  node [
    id 1148
    label "znak_jako&#347;ci"
  ]
  node [
    id 1149
    label "przepisa&#263;"
  ]
  node [
    id 1150
    label "arrangement"
  ]
  node [
    id 1151
    label "wagon"
  ]
  node [
    id 1152
    label "form"
  ]
  node [
    id 1153
    label "zaleta"
  ]
  node [
    id 1154
    label "poziom"
  ]
  node [
    id 1155
    label "dziennik_lekcyjny"
  ]
  node [
    id 1156
    label "&#347;rodowisko"
  ]
  node [
    id 1157
    label "atak"
  ]
  node [
    id 1158
    label "przepisanie"
  ]
  node [
    id 1159
    label "szko&#322;a"
  ]
  node [
    id 1160
    label "class"
  ]
  node [
    id 1161
    label "organizacja"
  ]
  node [
    id 1162
    label "obrona"
  ]
  node [
    id 1163
    label "type"
  ]
  node [
    id 1164
    label "&#322;awka"
  ]
  node [
    id 1165
    label "botanika"
  ]
  node [
    id 1166
    label "sala"
  ]
  node [
    id 1167
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 1168
    label "gromada"
  ]
  node [
    id 1169
    label "Ekwici"
  ]
  node [
    id 1170
    label "fakcja"
  ]
  node [
    id 1171
    label "tablica"
  ]
  node [
    id 1172
    label "programowanie_obiektowe"
  ]
  node [
    id 1173
    label "wykrzyknik"
  ]
  node [
    id 1174
    label "jednostka_systematyczna"
  ]
  node [
    id 1175
    label "mecz_mistrzowski"
  ]
  node [
    id 1176
    label "zbi&#243;r"
  ]
  node [
    id 1177
    label "jako&#347;&#263;"
  ]
  node [
    id 1178
    label "rezerwa"
  ]
  node [
    id 1179
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 1180
    label "nauczy&#263;"
  ]
  node [
    id 1181
    label "teach"
  ]
  node [
    id 1182
    label "przygotowa&#263;"
  ]
  node [
    id 1183
    label "dysleksja"
  ]
  node [
    id 1184
    label "wr&#243;&#380;y&#263;"
  ]
  node [
    id 1185
    label "przetwarza&#263;"
  ]
  node [
    id 1186
    label "read"
  ]
  node [
    id 1187
    label "poznawa&#263;"
  ]
  node [
    id 1188
    label "obserwowa&#263;"
  ]
  node [
    id 1189
    label "odczytywa&#263;"
  ]
  node [
    id 1190
    label "ozdabia&#263;"
  ]
  node [
    id 1191
    label "dysgrafia"
  ]
  node [
    id 1192
    label "prasa"
  ]
  node [
    id 1193
    label "spell"
  ]
  node [
    id 1194
    label "skryba"
  ]
  node [
    id 1195
    label "donosi&#263;"
  ]
  node [
    id 1196
    label "code"
  ]
  node [
    id 1197
    label "dysortografia"
  ]
  node [
    id 1198
    label "tworzy&#263;"
  ]
  node [
    id 1199
    label "styl"
  ]
  node [
    id 1200
    label "stawia&#263;"
  ]
  node [
    id 1201
    label "klecha"
  ]
  node [
    id 1202
    label "eklezjasta"
  ]
  node [
    id 1203
    label "rozgrzeszanie"
  ]
  node [
    id 1204
    label "duszpasterstwo"
  ]
  node [
    id 1205
    label "rozgrzesza&#263;"
  ]
  node [
    id 1206
    label "duchowny"
  ]
  node [
    id 1207
    label "ksi&#281;&#380;a"
  ]
  node [
    id 1208
    label "kap&#322;an"
  ]
  node [
    id 1209
    label "kol&#281;da"
  ]
  node [
    id 1210
    label "seminarzysta"
  ]
  node [
    id 1211
    label "pasterz"
  ]
  node [
    id 1212
    label "k&#322;amca"
  ]
  node [
    id 1213
    label "jezuici"
  ]
  node [
    id 1214
    label "Jakub_Wujek"
  ]
  node [
    id 1215
    label "fa&#322;szywy"
  ]
  node [
    id 1216
    label "otwarcie"
  ]
  node [
    id 1217
    label "prosto"
  ]
  node [
    id 1218
    label "naprzeciwko"
  ]
  node [
    id 1219
    label "admit"
  ]
  node [
    id 1220
    label "dopu&#347;ci&#263;"
  ]
  node [
    id 1221
    label "uzna&#263;"
  ]
  node [
    id 1222
    label "podlega&#263;"
  ]
  node [
    id 1223
    label "zmienia&#263;"
  ]
  node [
    id 1224
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 1225
    label "saturate"
  ]
  node [
    id 1226
    label "pass"
  ]
  node [
    id 1227
    label "doznawa&#263;"
  ]
  node [
    id 1228
    label "test"
  ]
  node [
    id 1229
    label "zalicza&#263;"
  ]
  node [
    id 1230
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 1231
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 1232
    label "conflict"
  ]
  node [
    id 1233
    label "nasyca&#263;_si&#281;"
  ]
  node [
    id 1234
    label "ko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 1235
    label "go"
  ]
  node [
    id 1236
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 1237
    label "przedostawa&#263;_si&#281;"
  ]
  node [
    id 1238
    label "zmienia&#263;_si&#281;"
  ]
  node [
    id 1239
    label "move"
  ]
  node [
    id 1240
    label "mija&#263;"
  ]
  node [
    id 1241
    label "zaczyna&#263;"
  ]
  node [
    id 1242
    label "i&#347;&#263;"
  ]
  node [
    id 1243
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 1244
    label "punkt_widzenia"
  ]
  node [
    id 1245
    label "do&#322;ek"
  ]
  node [
    id 1246
    label "formality"
  ]
  node [
    id 1247
    label "wz&#243;r"
  ]
  node [
    id 1248
    label "kantyzm"
  ]
  node [
    id 1249
    label "ornamentyka"
  ]
  node [
    id 1250
    label "odmiana"
  ]
  node [
    id 1251
    label "mode"
  ]
  node [
    id 1252
    label "style"
  ]
  node [
    id 1253
    label "formacja"
  ]
  node [
    id 1254
    label "maszyna_drukarska"
  ]
  node [
    id 1255
    label "poznanie"
  ]
  node [
    id 1256
    label "szablon"
  ]
  node [
    id 1257
    label "struktura"
  ]
  node [
    id 1258
    label "spirala"
  ]
  node [
    id 1259
    label "blaszka"
  ]
  node [
    id 1260
    label "linearno&#347;&#263;"
  ]
  node [
    id 1261
    label "temat"
  ]
  node [
    id 1262
    label "g&#322;owa"
  ]
  node [
    id 1263
    label "kielich"
  ]
  node [
    id 1264
    label "zdolno&#347;&#263;"
  ]
  node [
    id 1265
    label "pasmo"
  ]
  node [
    id 1266
    label "rdze&#324;"
  ]
  node [
    id 1267
    label "leksem"
  ]
  node [
    id 1268
    label "dyspozycja"
  ]
  node [
    id 1269
    label "wygl&#261;d"
  ]
  node [
    id 1270
    label "October"
  ]
  node [
    id 1271
    label "zawarto&#347;&#263;"
  ]
  node [
    id 1272
    label "creation"
  ]
  node [
    id 1273
    label "gwiazda"
  ]
  node [
    id 1274
    label "p&#281;tla"
  ]
  node [
    id 1275
    label "p&#322;at"
  ]
  node [
    id 1276
    label "arystotelizm"
  ]
  node [
    id 1277
    label "dzie&#322;o"
  ]
  node [
    id 1278
    label "wyra&#380;enie"
  ]
  node [
    id 1279
    label "miniatura"
  ]
  node [
    id 1280
    label "zwyczaj"
  ]
  node [
    id 1281
    label "morfem"
  ]
  node [
    id 1282
    label "najpierw"
  ]
  node [
    id 1283
    label "beget"
  ]
  node [
    id 1284
    label "pozyska&#263;"
  ]
  node [
    id 1285
    label "uwierzy&#263;"
  ]
  node [
    id 1286
    label "alwarowie"
  ]
  node [
    id 1287
    label "&#347;wi&#281;ty"
  ]
  node [
    id 1288
    label "poeta"
  ]
  node [
    id 1289
    label "lu&#378;no"
  ]
  node [
    id 1290
    label "naturalnie"
  ]
  node [
    id 1291
    label "swobodny"
  ]
  node [
    id 1292
    label "wolnie"
  ]
  node [
    id 1293
    label "dowolnie"
  ]
  node [
    id 1294
    label "bli&#378;ni"
  ]
  node [
    id 1295
    label "odpowiedni"
  ]
  node [
    id 1296
    label "swojak"
  ]
  node [
    id 1297
    label "samodzielny"
  ]
  node [
    id 1298
    label "gorliwy"
  ]
  node [
    id 1299
    label "intensywnie"
  ]
  node [
    id 1300
    label "ochoczo"
  ]
  node [
    id 1301
    label "zapoznawa&#263;"
  ]
  node [
    id 1302
    label "train"
  ]
  node [
    id 1303
    label "rozwija&#263;"
  ]
  node [
    id 1304
    label "szkoli&#263;"
  ]
  node [
    id 1305
    label "leni&#263;_si&#281;"
  ]
  node [
    id 1306
    label "render"
  ]
  node [
    id 1307
    label "hold"
  ]
  node [
    id 1308
    label "surrender"
  ]
  node [
    id 1309
    label "traktowa&#263;"
  ]
  node [
    id 1310
    label "dostarcza&#263;"
  ]
  node [
    id 1311
    label "tender"
  ]
  node [
    id 1312
    label "umieszcza&#263;"
  ]
  node [
    id 1313
    label "nalewa&#263;"
  ]
  node [
    id 1314
    label "p&#322;aci&#263;"
  ]
  node [
    id 1315
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 1316
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 1317
    label "powierza&#263;"
  ]
  node [
    id 1318
    label "hold_out"
  ]
  node [
    id 1319
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 1320
    label "wyrzeka&#263;_si&#281;"
  ]
  node [
    id 1321
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 1322
    label "t&#322;uc"
  ]
  node [
    id 1323
    label "wpiernicza&#263;"
  ]
  node [
    id 1324
    label "przekazywa&#263;"
  ]
  node [
    id 1325
    label "puszcza&#263;_si&#281;"
  ]
  node [
    id 1326
    label "zezwala&#263;"
  ]
  node [
    id 1327
    label "rap"
  ]
  node [
    id 1328
    label "obiecywa&#263;"
  ]
  node [
    id 1329
    label "&#322;adowa&#263;"
  ]
  node [
    id 1330
    label "odst&#281;powa&#263;"
  ]
  node [
    id 1331
    label "exsert"
  ]
  node [
    id 1332
    label "borsch"
  ]
  node [
    id 1333
    label "chwast"
  ]
  node [
    id 1334
    label "zupa"
  ]
  node [
    id 1335
    label "selerowate"
  ]
  node [
    id 1336
    label "przegubowiec"
  ]
  node [
    id 1337
    label "okrycie"
  ]
  node [
    id 1338
    label "os&#322;ona"
  ]
  node [
    id 1339
    label "przegub"
  ]
  node [
    id 1340
    label "element_konstrukcyjny"
  ]
  node [
    id 1341
    label "sta&#263;_si&#281;"
  ]
  node [
    id 1342
    label "turn"
  ]
  node [
    id 1343
    label "zjawi&#263;_si&#281;"
  ]
  node [
    id 1344
    label "urosn&#261;&#263;"
  ]
  node [
    id 1345
    label "wzrosn&#261;&#263;"
  ]
  node [
    id 1346
    label "sprout"
  ]
  node [
    id 1347
    label "podnie&#347;&#263;_si&#281;"
  ]
  node [
    id 1348
    label "dorobek"
  ]
  node [
    id 1349
    label "compose"
  ]
  node [
    id 1350
    label "go&#322;o"
  ]
  node [
    id 1351
    label "do_naga"
  ]
  node [
    id 1352
    label "wyczyszczenie_si&#281;"
  ]
  node [
    id 1353
    label "do_go&#322;a"
  ]
  node [
    id 1354
    label "wyczyszczanie_si&#281;"
  ]
  node [
    id 1355
    label "dzia&#322;anie"
  ]
  node [
    id 1356
    label "poduszka"
  ]
  node [
    id 1357
    label "koniuszek_palca"
  ]
  node [
    id 1358
    label "zap&#322;on"
  ]
  node [
    id 1359
    label "paznokie&#263;"
  ]
  node [
    id 1360
    label "polidaktylia"
  ]
  node [
    id 1361
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 1362
    label "knykie&#263;"
  ]
  node [
    id 1363
    label "pazur"
  ]
  node [
    id 1364
    label "d&#322;o&#324;"
  ]
  node [
    id 1365
    label "palpacja"
  ]
  node [
    id 1366
    label "element_anatomiczny"
  ]
  node [
    id 1367
    label "float"
  ]
  node [
    id 1368
    label "znak_nawigacyjny"
  ]
  node [
    id 1369
    label "p&#322;ywak"
  ]
  node [
    id 1370
    label "pokazywa&#263;_si&#281;"
  ]
  node [
    id 1371
    label "zdarza&#263;_si&#281;"
  ]
  node [
    id 1372
    label "&#380;egna&#263;"
  ]
  node [
    id 1373
    label "troch&#281;"
  ]
  node [
    id 1374
    label "&#322;&#243;dzki"
  ]
  node [
    id 1375
    label "dziadek"
  ]
  node [
    id 1376
    label "dziadyga"
  ]
  node [
    id 1377
    label "starszyzna"
  ]
  node [
    id 1378
    label "nie&#322;upka"
  ]
  node [
    id 1379
    label "dziadowina"
  ]
  node [
    id 1380
    label "kapu&#347;niak"
  ]
  node [
    id 1381
    label "starszy_cz&#322;owiek"
  ]
  node [
    id 1382
    label "rada_starc&#243;w"
  ]
  node [
    id 1383
    label "dziad_kalwaryjski"
  ]
  node [
    id 1384
    label "osta&#263;_si&#281;"
  ]
  node [
    id 1385
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 1386
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 1387
    label "change"
  ]
  node [
    id 1388
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 1389
    label "hipokamp"
  ]
  node [
    id 1390
    label "wymazanie"
  ]
  node [
    id 1391
    label "zachowa&#263;"
  ]
  node [
    id 1392
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 1393
    label "memory"
  ]
  node [
    id 1394
    label "pami&#281;tno&#347;&#263;"
  ]
  node [
    id 1395
    label "umys&#322;"
  ]
  node [
    id 1396
    label "sprz&#281;t_komputerowy"
  ]
  node [
    id 1397
    label "komputer"
  ]
  node [
    id 1398
    label "urlop"
  ]
  node [
    id 1399
    label "czas_wolny"
  ]
  node [
    id 1400
    label "rok_akademicki"
  ]
  node [
    id 1401
    label "rok_szkolny"
  ]
  node [
    id 1402
    label "j&#281;zykoznawstwo"
  ]
  node [
    id 1403
    label "fleksja"
  ]
  node [
    id 1404
    label "morfologia"
  ]
  node [
    id 1405
    label "sk&#322;adnia"
  ]
  node [
    id 1406
    label "nominacja"
  ]
  node [
    id 1407
    label "sprzeda&#380;"
  ]
  node [
    id 1408
    label "zamiana"
  ]
  node [
    id 1409
    label "graduacja"
  ]
  node [
    id 1410
    label "&#347;wiadectwo"
  ]
  node [
    id 1411
    label "gradation"
  ]
  node [
    id 1412
    label "brief"
  ]
  node [
    id 1413
    label "promotion"
  ]
  node [
    id 1414
    label "promowa&#263;"
  ]
  node [
    id 1415
    label "akcja"
  ]
  node [
    id 1416
    label "wypromowa&#263;"
  ]
  node [
    id 1417
    label "warcaby"
  ]
  node [
    id 1418
    label "popularyzacja"
  ]
  node [
    id 1419
    label "bran&#380;a"
  ]
  node [
    id 1420
    label "informacja"
  ]
  node [
    id 1421
    label "impreza"
  ]
  node [
    id 1422
    label "decyzja"
  ]
  node [
    id 1423
    label "okazja"
  ]
  node [
    id 1424
    label "commencement"
  ]
  node [
    id 1425
    label "udzieli&#263;"
  ]
  node [
    id 1426
    label "szachy"
  ]
  node [
    id 1427
    label "damka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 238
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 36
  ]
  edge [
    source 7
    target 37
  ]
  edge [
    source 7
    target 24
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 153
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 322
  ]
  edge [
    source 8
    target 323
  ]
  edge [
    source 8
    target 324
  ]
  edge [
    source 8
    target 325
  ]
  edge [
    source 8
    target 326
  ]
  edge [
    source 8
    target 327
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 328
  ]
  edge [
    source 8
    target 329
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 330
  ]
  edge [
    source 9
    target 331
  ]
  edge [
    source 9
    target 332
  ]
  edge [
    source 9
    target 333
  ]
  edge [
    source 9
    target 334
  ]
  edge [
    source 9
    target 335
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 336
  ]
  edge [
    source 10
    target 337
  ]
  edge [
    source 10
    target 338
  ]
  edge [
    source 10
    target 339
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 340
  ]
  edge [
    source 11
    target 341
  ]
  edge [
    source 11
    target 342
  ]
  edge [
    source 11
    target 343
  ]
  edge [
    source 11
    target 344
  ]
  edge [
    source 11
    target 345
  ]
  edge [
    source 11
    target 346
  ]
  edge [
    source 11
    target 135
  ]
  edge [
    source 11
    target 347
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 31
  ]
  edge [
    source 12
    target 24
  ]
  edge [
    source 12
    target 172
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 32
  ]
  edge [
    source 13
    target 348
  ]
  edge [
    source 13
    target 349
  ]
  edge [
    source 13
    target 350
  ]
  edge [
    source 13
    target 351
  ]
  edge [
    source 13
    target 352
  ]
  edge [
    source 13
    target 353
  ]
  edge [
    source 13
    target 354
  ]
  edge [
    source 13
    target 355
  ]
  edge [
    source 13
    target 356
  ]
  edge [
    source 13
    target 357
  ]
  edge [
    source 13
    target 358
  ]
  edge [
    source 13
    target 70
  ]
  edge [
    source 13
    target 82
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 53
  ]
  edge [
    source 14
    target 54
  ]
  edge [
    source 14
    target 359
  ]
  edge [
    source 14
    target 360
  ]
  edge [
    source 14
    target 361
  ]
  edge [
    source 14
    target 362
  ]
  edge [
    source 14
    target 363
  ]
  edge [
    source 14
    target 32
  ]
  edge [
    source 14
    target 36
  ]
  edge [
    source 14
    target 75
  ]
  edge [
    source 14
    target 98
  ]
  edge [
    source 14
    target 99
  ]
  edge [
    source 14
    target 155
  ]
  edge [
    source 14
    target 154
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 364
  ]
  edge [
    source 15
    target 365
  ]
  edge [
    source 15
    target 366
  ]
  edge [
    source 15
    target 367
  ]
  edge [
    source 15
    target 46
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 368
  ]
  edge [
    source 16
    target 369
  ]
  edge [
    source 16
    target 337
  ]
  edge [
    source 16
    target 370
  ]
  edge [
    source 16
    target 371
  ]
  edge [
    source 16
    target 372
  ]
  edge [
    source 16
    target 373
  ]
  edge [
    source 16
    target 374
  ]
  edge [
    source 16
    target 375
  ]
  edge [
    source 16
    target 376
  ]
  edge [
    source 16
    target 377
  ]
  edge [
    source 16
    target 378
  ]
  edge [
    source 16
    target 379
  ]
  edge [
    source 16
    target 380
  ]
  edge [
    source 16
    target 381
  ]
  edge [
    source 16
    target 382
  ]
  edge [
    source 16
    target 383
  ]
  edge [
    source 16
    target 384
  ]
  edge [
    source 16
    target 385
  ]
  edge [
    source 16
    target 386
  ]
  edge [
    source 16
    target 135
  ]
  edge [
    source 16
    target 25
  ]
  edge [
    source 16
    target 28
  ]
  edge [
    source 16
    target 57
  ]
  edge [
    source 16
    target 71
  ]
  edge [
    source 16
    target 93
  ]
  edge [
    source 16
    target 166
  ]
  edge [
    source 16
    target 24
  ]
  edge [
    source 16
    target 124
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 31
  ]
  edge [
    source 17
    target 61
  ]
  edge [
    source 17
    target 76
  ]
  edge [
    source 17
    target 103
  ]
  edge [
    source 17
    target 156
  ]
  edge [
    source 17
    target 126
  ]
  edge [
    source 17
    target 359
  ]
  edge [
    source 17
    target 387
  ]
  edge [
    source 17
    target 388
  ]
  edge [
    source 17
    target 389
  ]
  edge [
    source 17
    target 390
  ]
  edge [
    source 17
    target 391
  ]
  edge [
    source 17
    target 392
  ]
  edge [
    source 17
    target 66
  ]
  edge [
    source 17
    target 393
  ]
  edge [
    source 17
    target 394
  ]
  edge [
    source 17
    target 395
  ]
  edge [
    source 17
    target 20
  ]
  edge [
    source 17
    target 396
  ]
  edge [
    source 17
    target 397
  ]
  edge [
    source 17
    target 398
  ]
  edge [
    source 17
    target 399
  ]
  edge [
    source 17
    target 400
  ]
  edge [
    source 17
    target 401
  ]
  edge [
    source 17
    target 24
  ]
  edge [
    source 17
    target 402
  ]
  edge [
    source 17
    target 403
  ]
  edge [
    source 17
    target 404
  ]
  edge [
    source 17
    target 405
  ]
  edge [
    source 17
    target 406
  ]
  edge [
    source 17
    target 407
  ]
  edge [
    source 17
    target 408
  ]
  edge [
    source 17
    target 409
  ]
  edge [
    source 17
    target 410
  ]
  edge [
    source 17
    target 411
  ]
  edge [
    source 17
    target 412
  ]
  edge [
    source 17
    target 413
  ]
  edge [
    source 17
    target 414
  ]
  edge [
    source 17
    target 415
  ]
  edge [
    source 17
    target 416
  ]
  edge [
    source 17
    target 417
  ]
  edge [
    source 17
    target 418
  ]
  edge [
    source 17
    target 39
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 28
  ]
  edge [
    source 18
    target 29
  ]
  edge [
    source 18
    target 41
  ]
  edge [
    source 18
    target 42
  ]
  edge [
    source 18
    target 75
  ]
  edge [
    source 18
    target 168
  ]
  edge [
    source 18
    target 23
  ]
  edge [
    source 18
    target 419
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 420
  ]
  edge [
    source 19
    target 84
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 421
  ]
  edge [
    source 20
    target 359
  ]
  edge [
    source 20
    target 422
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 359
  ]
  edge [
    source 21
    target 423
  ]
  edge [
    source 21
    target 360
  ]
  edge [
    source 21
    target 424
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 25
  ]
  edge [
    source 22
    target 93
  ]
  edge [
    source 22
    target 113
  ]
  edge [
    source 22
    target 121
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 25
  ]
  edge [
    source 23
    target 115
  ]
  edge [
    source 23
    target 116
  ]
  edge [
    source 23
    target 169
  ]
  edge [
    source 23
    target 175
  ]
  edge [
    source 23
    target 176
  ]
  edge [
    source 23
    target 425
  ]
  edge [
    source 23
    target 426
  ]
  edge [
    source 23
    target 427
  ]
  edge [
    source 23
    target 428
  ]
  edge [
    source 23
    target 429
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 26
  ]
  edge [
    source 24
    target 34
  ]
  edge [
    source 24
    target 35
  ]
  edge [
    source 24
    target 47
  ]
  edge [
    source 24
    target 48
  ]
  edge [
    source 24
    target 101
  ]
  edge [
    source 24
    target 61
  ]
  edge [
    source 24
    target 430
  ]
  edge [
    source 24
    target 388
  ]
  edge [
    source 24
    target 431
  ]
  edge [
    source 24
    target 67
  ]
  edge [
    source 24
    target 432
  ]
  edge [
    source 24
    target 193
  ]
  edge [
    source 24
    target 415
  ]
  edge [
    source 24
    target 433
  ]
  edge [
    source 24
    target 434
  ]
  edge [
    source 24
    target 435
  ]
  edge [
    source 24
    target 436
  ]
  edge [
    source 24
    target 437
  ]
  edge [
    source 24
    target 438
  ]
  edge [
    source 24
    target 439
  ]
  edge [
    source 24
    target 440
  ]
  edge [
    source 24
    target 441
  ]
  edge [
    source 24
    target 49
  ]
  edge [
    source 24
    target 104
  ]
  edge [
    source 24
    target 153
  ]
  edge [
    source 24
    target 41
  ]
  edge [
    source 24
    target 64
  ]
  edge [
    source 24
    target 80
  ]
  edge [
    source 24
    target 126
  ]
  edge [
    source 24
    target 147
  ]
  edge [
    source 24
    target 172
  ]
  edge [
    source 24
    target 124
  ]
  edge [
    source 25
    target 442
  ]
  edge [
    source 25
    target 93
  ]
  edge [
    source 25
    target 113
  ]
  edge [
    source 25
    target 121
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 44
  ]
  edge [
    source 26
    target 45
  ]
  edge [
    source 26
    target 254
  ]
  edge [
    source 26
    target 443
  ]
  edge [
    source 26
    target 444
  ]
  edge [
    source 26
    target 445
  ]
  edge [
    source 26
    target 446
  ]
  edge [
    source 26
    target 447
  ]
  edge [
    source 26
    target 448
  ]
  edge [
    source 26
    target 449
  ]
  edge [
    source 26
    target 337
  ]
  edge [
    source 26
    target 450
  ]
  edge [
    source 26
    target 451
  ]
  edge [
    source 26
    target 452
  ]
  edge [
    source 26
    target 453
  ]
  edge [
    source 26
    target 454
  ]
  edge [
    source 26
    target 455
  ]
  edge [
    source 26
    target 456
  ]
  edge [
    source 26
    target 457
  ]
  edge [
    source 26
    target 60
  ]
  edge [
    source 26
    target 92
  ]
  edge [
    source 26
    target 110
  ]
  edge [
    source 26
    target 133
  ]
  edge [
    source 26
    target 138
  ]
  edge [
    source 26
    target 151
  ]
  edge [
    source 26
    target 80
  ]
  edge [
    source 26
    target 162
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 458
  ]
  edge [
    source 27
    target 459
  ]
  edge [
    source 27
    target 460
  ]
  edge [
    source 27
    target 461
  ]
  edge [
    source 27
    target 462
  ]
  edge [
    source 27
    target 463
  ]
  edge [
    source 27
    target 464
  ]
  edge [
    source 27
    target 465
  ]
  edge [
    source 27
    target 466
  ]
  edge [
    source 27
    target 467
  ]
  edge [
    source 28
    target 307
  ]
  edge [
    source 28
    target 468
  ]
  edge [
    source 28
    target 469
  ]
  edge [
    source 28
    target 470
  ]
  edge [
    source 28
    target 471
  ]
  edge [
    source 28
    target 472
  ]
  edge [
    source 28
    target 57
  ]
  edge [
    source 28
    target 473
  ]
  edge [
    source 28
    target 474
  ]
  edge [
    source 28
    target 475
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 476
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 477
  ]
  edge [
    source 30
    target 74
  ]
  edge [
    source 30
    target 478
  ]
  edge [
    source 30
    target 479
  ]
  edge [
    source 30
    target 480
  ]
  edge [
    source 30
    target 481
  ]
  edge [
    source 30
    target 482
  ]
  edge [
    source 30
    target 157
  ]
  edge [
    source 30
    target 483
  ]
  edge [
    source 30
    target 484
  ]
  edge [
    source 30
    target 485
  ]
  edge [
    source 30
    target 486
  ]
  edge [
    source 30
    target 487
  ]
  edge [
    source 30
    target 488
  ]
  edge [
    source 30
    target 489
  ]
  edge [
    source 30
    target 490
  ]
  edge [
    source 30
    target 491
  ]
  edge [
    source 30
    target 492
  ]
  edge [
    source 30
    target 493
  ]
  edge [
    source 30
    target 494
  ]
  edge [
    source 30
    target 495
  ]
  edge [
    source 31
    target 39
  ]
  edge [
    source 31
    target 40
  ]
  edge [
    source 31
    target 60
  ]
  edge [
    source 31
    target 74
  ]
  edge [
    source 31
    target 75
  ]
  edge [
    source 31
    target 88
  ]
  edge [
    source 31
    target 89
  ]
  edge [
    source 31
    target 50
  ]
  edge [
    source 31
    target 93
  ]
  edge [
    source 31
    target 100
  ]
  edge [
    source 31
    target 107
  ]
  edge [
    source 31
    target 108
  ]
  edge [
    source 31
    target 70
  ]
  edge [
    source 31
    target 120
  ]
  edge [
    source 31
    target 136
  ]
  edge [
    source 31
    target 63
  ]
  edge [
    source 31
    target 154
  ]
  edge [
    source 31
    target 155
  ]
  edge [
    source 31
    target 160
  ]
  edge [
    source 31
    target 145
  ]
  edge [
    source 31
    target 177
  ]
  edge [
    source 31
    target 162
  ]
  edge [
    source 32
    target 496
  ]
  edge [
    source 32
    target 497
  ]
  edge [
    source 32
    target 36
  ]
  edge [
    source 32
    target 75
  ]
  edge [
    source 32
    target 98
  ]
  edge [
    source 32
    target 99
  ]
  edge [
    source 32
    target 155
  ]
  edge [
    source 32
    target 154
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 108
  ]
  edge [
    source 33
    target 163
  ]
  edge [
    source 33
    target 311
  ]
  edge [
    source 33
    target 498
  ]
  edge [
    source 33
    target 499
  ]
  edge [
    source 33
    target 500
  ]
  edge [
    source 33
    target 501
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 36
    target 502
  ]
  edge [
    source 36
    target 503
  ]
  edge [
    source 36
    target 504
  ]
  edge [
    source 36
    target 75
  ]
  edge [
    source 36
    target 98
  ]
  edge [
    source 36
    target 99
  ]
  edge [
    source 36
    target 155
  ]
  edge [
    source 36
    target 154
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 39
    target 505
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 498
  ]
  edge [
    source 40
    target 506
  ]
  edge [
    source 40
    target 507
  ]
  edge [
    source 41
    target 508
  ]
  edge [
    source 41
    target 509
  ]
  edge [
    source 41
    target 510
  ]
  edge [
    source 41
    target 64
  ]
  edge [
    source 41
    target 80
  ]
  edge [
    source 41
    target 126
  ]
  edge [
    source 41
    target 147
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 511
  ]
  edge [
    source 42
    target 512
  ]
  edge [
    source 42
    target 513
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 514
  ]
  edge [
    source 43
    target 515
  ]
  edge [
    source 43
    target 516
  ]
  edge [
    source 43
    target 517
  ]
  edge [
    source 43
    target 518
  ]
  edge [
    source 43
    target 519
  ]
  edge [
    source 43
    target 520
  ]
  edge [
    source 43
    target 521
  ]
  edge [
    source 43
    target 522
  ]
  edge [
    source 43
    target 523
  ]
  edge [
    source 43
    target 524
  ]
  edge [
    source 43
    target 525
  ]
  edge [
    source 43
    target 526
  ]
  edge [
    source 44
    target 527
  ]
  edge [
    source 44
    target 528
  ]
  edge [
    source 44
    target 529
  ]
  edge [
    source 44
    target 530
  ]
  edge [
    source 44
    target 531
  ]
  edge [
    source 44
    target 532
  ]
  edge [
    source 44
    target 533
  ]
  edge [
    source 44
    target 534
  ]
  edge [
    source 44
    target 535
  ]
  edge [
    source 44
    target 536
  ]
  edge [
    source 44
    target 537
  ]
  edge [
    source 44
    target 538
  ]
  edge [
    source 44
    target 539
  ]
  edge [
    source 44
    target 540
  ]
  edge [
    source 44
    target 541
  ]
  edge [
    source 44
    target 542
  ]
  edge [
    source 44
    target 543
  ]
  edge [
    source 44
    target 544
  ]
  edge [
    source 44
    target 545
  ]
  edge [
    source 44
    target 546
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 547
  ]
  edge [
    source 45
    target 548
  ]
  edge [
    source 45
    target 549
  ]
  edge [
    source 45
    target 550
  ]
  edge [
    source 45
    target 551
  ]
  edge [
    source 45
    target 552
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 80
  ]
  edge [
    source 46
    target 162
  ]
  edge [
    source 46
    target 553
  ]
  edge [
    source 46
    target 554
  ]
  edge [
    source 46
    target 555
  ]
  edge [
    source 46
    target 556
  ]
  edge [
    source 46
    target 557
  ]
  edge [
    source 46
    target 558
  ]
  edge [
    source 46
    target 559
  ]
  edge [
    source 46
    target 560
  ]
  edge [
    source 46
    target 561
  ]
  edge [
    source 46
    target 562
  ]
  edge [
    source 46
    target 563
  ]
  edge [
    source 46
    target 564
  ]
  edge [
    source 46
    target 452
  ]
  edge [
    source 46
    target 565
  ]
  edge [
    source 46
    target 515
  ]
  edge [
    source 46
    target 566
  ]
  edge [
    source 46
    target 567
  ]
  edge [
    source 46
    target 568
  ]
  edge [
    source 46
    target 569
  ]
  edge [
    source 46
    target 570
  ]
  edge [
    source 46
    target 571
  ]
  edge [
    source 46
    target 572
  ]
  edge [
    source 46
    target 573
  ]
  edge [
    source 46
    target 574
  ]
  edge [
    source 46
    target 575
  ]
  edge [
    source 46
    target 576
  ]
  edge [
    source 47
    target 577
  ]
  edge [
    source 47
    target 66
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 578
  ]
  edge [
    source 48
    target 579
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 580
  ]
  edge [
    source 49
    target 101
  ]
  edge [
    source 49
    target 104
  ]
  edge [
    source 49
    target 153
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 92
  ]
  edge [
    source 50
    target 581
  ]
  edge [
    source 50
    target 582
  ]
  edge [
    source 50
    target 583
  ]
  edge [
    source 50
    target 584
  ]
  edge [
    source 50
    target 585
  ]
  edge [
    source 50
    target 586
  ]
  edge [
    source 50
    target 587
  ]
  edge [
    source 50
    target 588
  ]
  edge [
    source 50
    target 589
  ]
  edge [
    source 50
    target 590
  ]
  edge [
    source 50
    target 591
  ]
  edge [
    source 50
    target 592
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 80
  ]
  edge [
    source 52
    target 113
  ]
  edge [
    source 52
    target 359
  ]
  edge [
    source 52
    target 593
  ]
  edge [
    source 52
    target 594
  ]
  edge [
    source 52
    target 595
  ]
  edge [
    source 52
    target 596
  ]
  edge [
    source 52
    target 597
  ]
  edge [
    source 52
    target 598
  ]
  edge [
    source 52
    target 599
  ]
  edge [
    source 52
    target 600
  ]
  edge [
    source 52
    target 601
  ]
  edge [
    source 52
    target 602
  ]
  edge [
    source 52
    target 603
  ]
  edge [
    source 52
    target 604
  ]
  edge [
    source 52
    target 605
  ]
  edge [
    source 53
    target 606
  ]
  edge [
    source 53
    target 607
  ]
  edge [
    source 53
    target 608
  ]
  edge [
    source 53
    target 489
  ]
  edge [
    source 53
    target 609
  ]
  edge [
    source 53
    target 490
  ]
  edge [
    source 53
    target 610
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 90
  ]
  edge [
    source 54
    target 84
  ]
  edge [
    source 54
    target 611
  ]
  edge [
    source 54
    target 612
  ]
  edge [
    source 54
    target 613
  ]
  edge [
    source 54
    target 57
  ]
  edge [
    source 55
    target 614
  ]
  edge [
    source 55
    target 615
  ]
  edge [
    source 55
    target 616
  ]
  edge [
    source 55
    target 617
  ]
  edge [
    source 55
    target 618
  ]
  edge [
    source 55
    target 619
  ]
  edge [
    source 55
    target 620
  ]
  edge [
    source 55
    target 621
  ]
  edge [
    source 55
    target 622
  ]
  edge [
    source 55
    target 623
  ]
  edge [
    source 55
    target 624
  ]
  edge [
    source 55
    target 94
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 349
  ]
  edge [
    source 56
    target 625
  ]
  edge [
    source 56
    target 626
  ]
  edge [
    source 56
    target 627
  ]
  edge [
    source 56
    target 628
  ]
  edge [
    source 56
    target 629
  ]
  edge [
    source 56
    target 630
  ]
  edge [
    source 56
    target 631
  ]
  edge [
    source 56
    target 632
  ]
  edge [
    source 56
    target 633
  ]
  edge [
    source 56
    target 634
  ]
  edge [
    source 56
    target 635
  ]
  edge [
    source 56
    target 636
  ]
  edge [
    source 56
    target 637
  ]
  edge [
    source 56
    target 638
  ]
  edge [
    source 56
    target 511
  ]
  edge [
    source 56
    target 639
  ]
  edge [
    source 56
    target 640
  ]
  edge [
    source 56
    target 641
  ]
  edge [
    source 56
    target 642
  ]
  edge [
    source 56
    target 643
  ]
  edge [
    source 56
    target 644
  ]
  edge [
    source 56
    target 645
  ]
  edge [
    source 56
    target 646
  ]
  edge [
    source 56
    target 647
  ]
  edge [
    source 56
    target 648
  ]
  edge [
    source 56
    target 649
  ]
  edge [
    source 56
    target 650
  ]
  edge [
    source 56
    target 651
  ]
  edge [
    source 56
    target 652
  ]
  edge [
    source 56
    target 653
  ]
  edge [
    source 56
    target 654
  ]
  edge [
    source 56
    target 655
  ]
  edge [
    source 56
    target 656
  ]
  edge [
    source 56
    target 657
  ]
  edge [
    source 56
    target 658
  ]
  edge [
    source 56
    target 659
  ]
  edge [
    source 56
    target 660
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 470
  ]
  edge [
    source 57
    target 661
  ]
  edge [
    source 57
    target 662
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 170
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 663
  ]
  edge [
    source 60
    target 664
  ]
  edge [
    source 60
    target 665
  ]
  edge [
    source 60
    target 92
  ]
  edge [
    source 60
    target 110
  ]
  edge [
    source 60
    target 133
  ]
  edge [
    source 60
    target 138
  ]
  edge [
    source 60
    target 151
  ]
  edge [
    source 60
    target 80
  ]
  edge [
    source 60
    target 162
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 102
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 666
  ]
  edge [
    source 62
    target 667
  ]
  edge [
    source 62
    target 668
  ]
  edge [
    source 62
    target 441
  ]
  edge [
    source 62
    target 669
  ]
  edge [
    source 62
    target 670
  ]
  edge [
    source 62
    target 671
  ]
  edge [
    source 62
    target 672
  ]
  edge [
    source 62
    target 673
  ]
  edge [
    source 62
    target 674
  ]
  edge [
    source 62
    target 675
  ]
  edge [
    source 62
    target 676
  ]
  edge [
    source 62
    target 677
  ]
  edge [
    source 62
    target 678
  ]
  edge [
    source 62
    target 679
  ]
  edge [
    source 62
    target 680
  ]
  edge [
    source 62
    target 681
  ]
  edge [
    source 62
    target 682
  ]
  edge [
    source 62
    target 683
  ]
  edge [
    source 62
    target 684
  ]
  edge [
    source 62
    target 685
  ]
  edge [
    source 62
    target 440
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 93
  ]
  edge [
    source 63
    target 94
  ]
  edge [
    source 63
    target 137
  ]
  edge [
    source 63
    target 126
  ]
  edge [
    source 63
    target 121
  ]
  edge [
    source 63
    target 85
  ]
  edge [
    source 63
    target 156
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 686
  ]
  edge [
    source 64
    target 687
  ]
  edge [
    source 64
    target 80
  ]
  edge [
    source 64
    target 126
  ]
  edge [
    source 64
    target 147
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 688
  ]
  edge [
    source 65
    target 689
  ]
  edge [
    source 65
    target 690
  ]
  edge [
    source 65
    target 691
  ]
  edge [
    source 65
    target 692
  ]
  edge [
    source 65
    target 693
  ]
  edge [
    source 65
    target 694
  ]
  edge [
    source 65
    target 695
  ]
  edge [
    source 65
    target 696
  ]
  edge [
    source 65
    target 182
  ]
  edge [
    source 65
    target 697
  ]
  edge [
    source 65
    target 698
  ]
  edge [
    source 65
    target 699
  ]
  edge [
    source 65
    target 700
  ]
  edge [
    source 65
    target 489
  ]
  edge [
    source 65
    target 490
  ]
  edge [
    source 65
    target 701
  ]
  edge [
    source 65
    target 702
  ]
  edge [
    source 65
    target 703
  ]
  edge [
    source 65
    target 704
  ]
  edge [
    source 65
    target 705
  ]
  edge [
    source 65
    target 706
  ]
  edge [
    source 65
    target 707
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 66
    target 708
  ]
  edge [
    source 66
    target 359
  ]
  edge [
    source 66
    target 709
  ]
  edge [
    source 66
    target 710
  ]
  edge [
    source 66
    target 432
  ]
  edge [
    source 66
    target 193
  ]
  edge [
    source 66
    target 711
  ]
  edge [
    source 66
    target 712
  ]
  edge [
    source 66
    target 713
  ]
  edge [
    source 66
    target 714
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 67
    target 715
  ]
  edge [
    source 68
    target 716
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 69
    target 717
  ]
  edge [
    source 69
    target 718
  ]
  edge [
    source 69
    target 719
  ]
  edge [
    source 69
    target 720
  ]
  edge [
    source 69
    target 721
  ]
  edge [
    source 69
    target 722
  ]
  edge [
    source 69
    target 723
  ]
  edge [
    source 69
    target 724
  ]
  edge [
    source 69
    target 725
  ]
  edge [
    source 69
    target 726
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 119
  ]
  edge [
    source 70
    target 727
  ]
  edge [
    source 70
    target 728
  ]
  edge [
    source 70
    target 729
  ]
  edge [
    source 70
    target 586
  ]
  edge [
    source 70
    target 730
  ]
  edge [
    source 70
    target 731
  ]
  edge [
    source 70
    target 732
  ]
  edge [
    source 70
    target 733
  ]
  edge [
    source 70
    target 734
  ]
  edge [
    source 70
    target 735
  ]
  edge [
    source 70
    target 345
  ]
  edge [
    source 70
    target 736
  ]
  edge [
    source 70
    target 737
  ]
  edge [
    source 70
    target 357
  ]
  edge [
    source 70
    target 129
  ]
  edge [
    source 71
    target 125
  ]
  edge [
    source 71
    target 126
  ]
  edge [
    source 71
    target 662
  ]
  edge [
    source 71
    target 738
  ]
  edge [
    source 71
    target 739
  ]
  edge [
    source 71
    target 740
  ]
  edge [
    source 71
    target 741
  ]
  edge [
    source 71
    target 470
  ]
  edge [
    source 71
    target 742
  ]
  edge [
    source 71
    target 743
  ]
  edge [
    source 71
    target 744
  ]
  edge [
    source 71
    target 745
  ]
  edge [
    source 71
    target 746
  ]
  edge [
    source 71
    target 448
  ]
  edge [
    source 71
    target 747
  ]
  edge [
    source 71
    target 748
  ]
  edge [
    source 71
    target 749
  ]
  edge [
    source 71
    target 750
  ]
  edge [
    source 71
    target 751
  ]
  edge [
    source 71
    target 752
  ]
  edge [
    source 71
    target 753
  ]
  edge [
    source 71
    target 137
  ]
  edge [
    source 72
    target 73
  ]
  edge [
    source 72
    target 359
  ]
  edge [
    source 72
    target 754
  ]
  edge [
    source 72
    target 755
  ]
  edge [
    source 72
    target 756
  ]
  edge [
    source 72
    target 757
  ]
  edge [
    source 72
    target 758
  ]
  edge [
    source 72
    target 759
  ]
  edge [
    source 72
    target 760
  ]
  edge [
    source 72
    target 761
  ]
  edge [
    source 72
    target 762
  ]
  edge [
    source 72
    target 763
  ]
  edge [
    source 72
    target 764
  ]
  edge [
    source 72
    target 765
  ]
  edge [
    source 72
    target 766
  ]
  edge [
    source 72
    target 767
  ]
  edge [
    source 72
    target 768
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 74
    target 769
  ]
  edge [
    source 74
    target 770
  ]
  edge [
    source 74
    target 771
  ]
  edge [
    source 75
    target 86
  ]
  edge [
    source 75
    target 87
  ]
  edge [
    source 75
    target 96
  ]
  edge [
    source 75
    target 97
  ]
  edge [
    source 75
    target 359
  ]
  edge [
    source 75
    target 772
  ]
  edge [
    source 75
    target 773
  ]
  edge [
    source 75
    target 774
  ]
  edge [
    source 75
    target 775
  ]
  edge [
    source 75
    target 776
  ]
  edge [
    source 75
    target 777
  ]
  edge [
    source 75
    target 778
  ]
  edge [
    source 75
    target 779
  ]
  edge [
    source 75
    target 780
  ]
  edge [
    source 75
    target 781
  ]
  edge [
    source 75
    target 782
  ]
  edge [
    source 75
    target 783
  ]
  edge [
    source 75
    target 784
  ]
  edge [
    source 75
    target 785
  ]
  edge [
    source 75
    target 786
  ]
  edge [
    source 75
    target 787
  ]
  edge [
    source 75
    target 788
  ]
  edge [
    source 75
    target 789
  ]
  edge [
    source 75
    target 790
  ]
  edge [
    source 75
    target 77
  ]
  edge [
    source 75
    target 98
  ]
  edge [
    source 75
    target 99
  ]
  edge [
    source 75
    target 155
  ]
  edge [
    source 75
    target 154
  ]
  edge [
    source 76
    target 77
  ]
  edge [
    source 76
    target 104
  ]
  edge [
    source 76
    target 529
  ]
  edge [
    source 76
    target 534
  ]
  edge [
    source 76
    target 536
  ]
  edge [
    source 76
    target 791
  ]
  edge [
    source 76
    target 528
  ]
  edge [
    source 76
    target 532
  ]
  edge [
    source 76
    target 535
  ]
  edge [
    source 76
    target 792
  ]
  edge [
    source 76
    target 538
  ]
  edge [
    source 76
    target 793
  ]
  edge [
    source 76
    target 542
  ]
  edge [
    source 76
    target 544
  ]
  edge [
    source 76
    target 545
  ]
  edge [
    source 76
    target 530
  ]
  edge [
    source 76
    target 531
  ]
  edge [
    source 76
    target 537
  ]
  edge [
    source 76
    target 533
  ]
  edge [
    source 76
    target 540
  ]
  edge [
    source 76
    target 794
  ]
  edge [
    source 76
    target 543
  ]
  edge [
    source 76
    target 795
  ]
  edge [
    source 76
    target 546
  ]
  edge [
    source 76
    target 539
  ]
  edge [
    source 76
    target 541
  ]
  edge [
    source 76
    target 796
  ]
  edge [
    source 76
    target 527
  ]
  edge [
    source 77
    target 78
  ]
  edge [
    source 77
    target 359
  ]
  edge [
    source 77
    target 797
  ]
  edge [
    source 77
    target 798
  ]
  edge [
    source 77
    target 799
  ]
  edge [
    source 77
    target 800
  ]
  edge [
    source 77
    target 801
  ]
  edge [
    source 77
    target 802
  ]
  edge [
    source 77
    target 803
  ]
  edge [
    source 78
    target 79
  ]
  edge [
    source 78
    target 804
  ]
  edge [
    source 78
    target 805
  ]
  edge [
    source 78
    target 806
  ]
  edge [
    source 78
    target 807
  ]
  edge [
    source 78
    target 808
  ]
  edge [
    source 78
    target 809
  ]
  edge [
    source 78
    target 810
  ]
  edge [
    source 78
    target 337
  ]
  edge [
    source 78
    target 811
  ]
  edge [
    source 78
    target 812
  ]
  edge [
    source 79
    target 80
  ]
  edge [
    source 79
    target 81
  ]
  edge [
    source 79
    target 82
  ]
  edge [
    source 79
    target 85
  ]
  edge [
    source 79
    target 86
  ]
  edge [
    source 79
    target 813
  ]
  edge [
    source 80
    target 81
  ]
  edge [
    source 80
    target 106
  ]
  edge [
    source 80
    target 107
  ]
  edge [
    source 80
    target 112
  ]
  edge [
    source 80
    target 125
  ]
  edge [
    source 80
    target 138
  ]
  edge [
    source 80
    target 115
  ]
  edge [
    source 80
    target 80
  ]
  edge [
    source 80
    target 92
  ]
  edge [
    source 80
    target 110
  ]
  edge [
    source 80
    target 126
  ]
  edge [
    source 80
    target 147
  ]
  edge [
    source 80
    target 133
  ]
  edge [
    source 80
    target 151
  ]
  edge [
    source 80
    target 162
  ]
  edge [
    source 81
    target 814
  ]
  edge [
    source 81
    target 815
  ]
  edge [
    source 81
    target 816
  ]
  edge [
    source 81
    target 817
  ]
  edge [
    source 82
    target 83
  ]
  edge [
    source 82
    target 818
  ]
  edge [
    source 82
    target 168
  ]
  edge [
    source 83
    target 84
  ]
  edge [
    source 83
    target 626
  ]
  edge [
    source 83
    target 819
  ]
  edge [
    source 83
    target 820
  ]
  edge [
    source 83
    target 821
  ]
  edge [
    source 83
    target 822
  ]
  edge [
    source 83
    target 823
  ]
  edge [
    source 83
    target 824
  ]
  edge [
    source 83
    target 825
  ]
  edge [
    source 83
    target 826
  ]
  edge [
    source 83
    target 827
  ]
  edge [
    source 84
    target 85
  ]
  edge [
    source 84
    target 91
  ]
  edge [
    source 87
    target 88
  ]
  edge [
    source 89
    target 90
  ]
  edge [
    source 90
    target 828
  ]
  edge [
    source 90
    target 829
  ]
  edge [
    source 90
    target 483
  ]
  edge [
    source 90
    target 830
  ]
  edge [
    source 90
    target 339
  ]
  edge [
    source 90
    target 831
  ]
  edge [
    source 90
    target 832
  ]
  edge [
    source 90
    target 833
  ]
  edge [
    source 90
    target 699
  ]
  edge [
    source 90
    target 834
  ]
  edge [
    source 90
    target 835
  ]
  edge [
    source 90
    target 836
  ]
  edge [
    source 90
    target 837
  ]
  edge [
    source 91
    target 92
  ]
  edge [
    source 92
    target 838
  ]
  edge [
    source 92
    target 110
  ]
  edge [
    source 92
    target 133
  ]
  edge [
    source 92
    target 138
  ]
  edge [
    source 92
    target 151
  ]
  edge [
    source 92
    target 162
  ]
  edge [
    source 93
    target 839
  ]
  edge [
    source 93
    target 808
  ]
  edge [
    source 93
    target 840
  ]
  edge [
    source 93
    target 841
  ]
  edge [
    source 93
    target 842
  ]
  edge [
    source 93
    target 843
  ]
  edge [
    source 93
    target 844
  ]
  edge [
    source 93
    target 113
  ]
  edge [
    source 93
    target 121
  ]
  edge [
    source 94
    target 95
  ]
  edge [
    source 94
    target 477
  ]
  edge [
    source 94
    target 688
  ]
  edge [
    source 94
    target 845
  ]
  edge [
    source 94
    target 846
  ]
  edge [
    source 94
    target 847
  ]
  edge [
    source 94
    target 691
  ]
  edge [
    source 94
    target 848
  ]
  edge [
    source 94
    target 849
  ]
  edge [
    source 94
    target 850
  ]
  edge [
    source 94
    target 851
  ]
  edge [
    source 94
    target 852
  ]
  edge [
    source 94
    target 853
  ]
  edge [
    source 94
    target 854
  ]
  edge [
    source 94
    target 855
  ]
  edge [
    source 94
    target 482
  ]
  edge [
    source 94
    target 856
  ]
  edge [
    source 94
    target 857
  ]
  edge [
    source 94
    target 858
  ]
  edge [
    source 94
    target 698
  ]
  edge [
    source 94
    target 859
  ]
  edge [
    source 94
    target 649
  ]
  edge [
    source 94
    target 860
  ]
  edge [
    source 94
    target 494
  ]
  edge [
    source 94
    target 861
  ]
  edge [
    source 94
    target 862
  ]
  edge [
    source 94
    target 863
  ]
  edge [
    source 94
    target 98
  ]
  edge [
    source 95
    target 96
  ]
  edge [
    source 95
    target 864
  ]
  edge [
    source 95
    target 865
  ]
  edge [
    source 95
    target 866
  ]
  edge [
    source 95
    target 867
  ]
  edge [
    source 95
    target 868
  ]
  edge [
    source 95
    target 476
  ]
  edge [
    source 95
    target 141
  ]
  edge [
    source 96
    target 359
  ]
  edge [
    source 96
    target 869
  ]
  edge [
    source 97
    target 98
  ]
  edge [
    source 98
    target 99
  ]
  edge [
    source 98
    target 870
  ]
  edge [
    source 98
    target 871
  ]
  edge [
    source 98
    target 872
  ]
  edge [
    source 98
    target 550
  ]
  edge [
    source 98
    target 873
  ]
  edge [
    source 98
    target 874
  ]
  edge [
    source 98
    target 176
  ]
  edge [
    source 98
    target 155
  ]
  edge [
    source 99
    target 100
  ]
  edge [
    source 99
    target 155
  ]
  edge [
    source 101
    target 875
  ]
  edge [
    source 101
    target 876
  ]
  edge [
    source 101
    target 877
  ]
  edge [
    source 101
    target 878
  ]
  edge [
    source 101
    target 879
  ]
  edge [
    source 101
    target 104
  ]
  edge [
    source 101
    target 153
  ]
  edge [
    source 102
    target 103
  ]
  edge [
    source 102
    target 880
  ]
  edge [
    source 102
    target 881
  ]
  edge [
    source 102
    target 134
  ]
  edge [
    source 102
    target 882
  ]
  edge [
    source 102
    target 182
  ]
  edge [
    source 103
    target 883
  ]
  edge [
    source 103
    target 884
  ]
  edge [
    source 103
    target 885
  ]
  edge [
    source 103
    target 886
  ]
  edge [
    source 103
    target 887
  ]
  edge [
    source 103
    target 586
  ]
  edge [
    source 103
    target 888
  ]
  edge [
    source 103
    target 889
  ]
  edge [
    source 103
    target 890
  ]
  edge [
    source 103
    target 891
  ]
  edge [
    source 103
    target 141
  ]
  edge [
    source 103
    target 187
  ]
  edge [
    source 104
    target 105
  ]
  edge [
    source 104
    target 892
  ]
  edge [
    source 104
    target 893
  ]
  edge [
    source 104
    target 894
  ]
  edge [
    source 104
    target 895
  ]
  edge [
    source 104
    target 896
  ]
  edge [
    source 104
    target 490
  ]
  edge [
    source 104
    target 153
  ]
  edge [
    source 105
    target 106
  ]
  edge [
    source 105
    target 897
  ]
  edge [
    source 106
    target 898
  ]
  edge [
    source 106
    target 781
  ]
  edge [
    source 107
    target 547
  ]
  edge [
    source 107
    target 552
  ]
  edge [
    source 108
    target 109
  ]
  edge [
    source 108
    target 162
  ]
  edge [
    source 108
    target 899
  ]
  edge [
    source 109
    target 110
  ]
  edge [
    source 109
    target 900
  ]
  edge [
    source 109
    target 901
  ]
  edge [
    source 110
    target 111
  ]
  edge [
    source 110
    target 359
  ]
  edge [
    source 110
    target 902
  ]
  edge [
    source 110
    target 903
  ]
  edge [
    source 110
    target 904
  ]
  edge [
    source 110
    target 905
  ]
  edge [
    source 110
    target 906
  ]
  edge [
    source 110
    target 907
  ]
  edge [
    source 110
    target 908
  ]
  edge [
    source 110
    target 909
  ]
  edge [
    source 110
    target 910
  ]
  edge [
    source 110
    target 911
  ]
  edge [
    source 110
    target 912
  ]
  edge [
    source 110
    target 913
  ]
  edge [
    source 110
    target 914
  ]
  edge [
    source 110
    target 915
  ]
  edge [
    source 110
    target 916
  ]
  edge [
    source 110
    target 917
  ]
  edge [
    source 110
    target 918
  ]
  edge [
    source 110
    target 919
  ]
  edge [
    source 110
    target 920
  ]
  edge [
    source 110
    target 921
  ]
  edge [
    source 110
    target 922
  ]
  edge [
    source 110
    target 923
  ]
  edge [
    source 110
    target 924
  ]
  edge [
    source 110
    target 925
  ]
  edge [
    source 110
    target 926
  ]
  edge [
    source 110
    target 927
  ]
  edge [
    source 110
    target 928
  ]
  edge [
    source 110
    target 929
  ]
  edge [
    source 110
    target 930
  ]
  edge [
    source 110
    target 931
  ]
  edge [
    source 110
    target 932
  ]
  edge [
    source 110
    target 933
  ]
  edge [
    source 110
    target 934
  ]
  edge [
    source 110
    target 935
  ]
  edge [
    source 110
    target 936
  ]
  edge [
    source 110
    target 937
  ]
  edge [
    source 110
    target 938
  ]
  edge [
    source 110
    target 939
  ]
  edge [
    source 110
    target 940
  ]
  edge [
    source 110
    target 941
  ]
  edge [
    source 110
    target 942
  ]
  edge [
    source 110
    target 943
  ]
  edge [
    source 110
    target 944
  ]
  edge [
    source 110
    target 945
  ]
  edge [
    source 110
    target 946
  ]
  edge [
    source 110
    target 947
  ]
  edge [
    source 110
    target 948
  ]
  edge [
    source 110
    target 949
  ]
  edge [
    source 110
    target 650
  ]
  edge [
    source 110
    target 950
  ]
  edge [
    source 110
    target 951
  ]
  edge [
    source 110
    target 952
  ]
  edge [
    source 110
    target 953
  ]
  edge [
    source 110
    target 954
  ]
  edge [
    source 110
    target 955
  ]
  edge [
    source 110
    target 956
  ]
  edge [
    source 110
    target 957
  ]
  edge [
    source 110
    target 958
  ]
  edge [
    source 110
    target 959
  ]
  edge [
    source 110
    target 960
  ]
  edge [
    source 110
    target 961
  ]
  edge [
    source 110
    target 133
  ]
  edge [
    source 110
    target 138
  ]
  edge [
    source 110
    target 151
  ]
  edge [
    source 110
    target 162
  ]
  edge [
    source 111
    target 112
  ]
  edge [
    source 111
    target 499
  ]
  edge [
    source 111
    target 962
  ]
  edge [
    source 111
    target 497
  ]
  edge [
    source 111
    target 963
  ]
  edge [
    source 111
    target 964
  ]
  edge [
    source 111
    target 130
  ]
  edge [
    source 111
    target 146
  ]
  edge [
    source 113
    target 114
  ]
  edge [
    source 113
    target 121
  ]
  edge [
    source 114
    target 115
  ]
  edge [
    source 114
    target 965
  ]
  edge [
    source 114
    target 476
  ]
  edge [
    source 115
    target 161
  ]
  edge [
    source 115
    target 157
  ]
  edge [
    source 116
    target 117
  ]
  edge [
    source 116
    target 966
  ]
  edge [
    source 116
    target 967
  ]
  edge [
    source 117
    target 166
  ]
  edge [
    source 117
    target 968
  ]
  edge [
    source 117
    target 969
  ]
  edge [
    source 117
    target 970
  ]
  edge [
    source 117
    target 971
  ]
  edge [
    source 117
    target 972
  ]
  edge [
    source 117
    target 973
  ]
  edge [
    source 118
    target 119
  ]
  edge [
    source 118
    target 359
  ]
  edge [
    source 118
    target 974
  ]
  edge [
    source 118
    target 975
  ]
  edge [
    source 118
    target 976
  ]
  edge [
    source 118
    target 977
  ]
  edge [
    source 118
    target 978
  ]
  edge [
    source 118
    target 979
  ]
  edge [
    source 118
    target 980
  ]
  edge [
    source 118
    target 981
  ]
  edge [
    source 118
    target 982
  ]
  edge [
    source 118
    target 983
  ]
  edge [
    source 118
    target 984
  ]
  edge [
    source 118
    target 985
  ]
  edge [
    source 118
    target 986
  ]
  edge [
    source 118
    target 987
  ]
  edge [
    source 118
    target 988
  ]
  edge [
    source 118
    target 989
  ]
  edge [
    source 118
    target 990
  ]
  edge [
    source 118
    target 991
  ]
  edge [
    source 118
    target 992
  ]
  edge [
    source 118
    target 993
  ]
  edge [
    source 118
    target 994
  ]
  edge [
    source 118
    target 995
  ]
  edge [
    source 118
    target 996
  ]
  edge [
    source 118
    target 997
  ]
  edge [
    source 118
    target 998
  ]
  edge [
    source 118
    target 999
  ]
  edge [
    source 118
    target 1000
  ]
  edge [
    source 118
    target 1001
  ]
  edge [
    source 118
    target 1002
  ]
  edge [
    source 118
    target 1003
  ]
  edge [
    source 118
    target 1004
  ]
  edge [
    source 118
    target 1005
  ]
  edge [
    source 118
    target 1006
  ]
  edge [
    source 118
    target 1007
  ]
  edge [
    source 118
    target 1008
  ]
  edge [
    source 118
    target 1009
  ]
  edge [
    source 118
    target 1010
  ]
  edge [
    source 118
    target 1011
  ]
  edge [
    source 119
    target 1012
  ]
  edge [
    source 119
    target 1013
  ]
  edge [
    source 119
    target 1014
  ]
  edge [
    source 119
    target 1015
  ]
  edge [
    source 119
    target 1016
  ]
  edge [
    source 119
    target 1017
  ]
  edge [
    source 119
    target 1018
  ]
  edge [
    source 119
    target 1019
  ]
  edge [
    source 119
    target 1020
  ]
  edge [
    source 119
    target 979
  ]
  edge [
    source 119
    target 1021
  ]
  edge [
    source 119
    target 1022
  ]
  edge [
    source 119
    target 1023
  ]
  edge [
    source 119
    target 1024
  ]
  edge [
    source 119
    target 1025
  ]
  edge [
    source 119
    target 123
  ]
  edge [
    source 119
    target 128
  ]
  edge [
    source 120
    target 121
  ]
  edge [
    source 120
    target 339
  ]
  edge [
    source 120
    target 518
  ]
  edge [
    source 120
    target 1026
  ]
  edge [
    source 120
    target 1027
  ]
  edge [
    source 120
    target 1028
  ]
  edge [
    source 120
    target 450
  ]
  edge [
    source 120
    target 1029
  ]
  edge [
    source 120
    target 1030
  ]
  edge [
    source 120
    target 338
  ]
  edge [
    source 120
    target 526
  ]
  edge [
    source 120
    target 1031
  ]
  edge [
    source 120
    target 161
  ]
  edge [
    source 120
    target 167
  ]
  edge [
    source 121
    target 122
  ]
  edge [
    source 121
    target 157
  ]
  edge [
    source 121
    target 1032
  ]
  edge [
    source 122
    target 123
  ]
  edge [
    source 122
    target 1033
  ]
  edge [
    source 123
    target 124
  ]
  edge [
    source 123
    target 1034
  ]
  edge [
    source 123
    target 1035
  ]
  edge [
    source 123
    target 1036
  ]
  edge [
    source 123
    target 732
  ]
  edge [
    source 123
    target 1037
  ]
  edge [
    source 123
    target 1038
  ]
  edge [
    source 123
    target 1039
  ]
  edge [
    source 123
    target 1024
  ]
  edge [
    source 123
    target 1040
  ]
  edge [
    source 123
    target 128
  ]
  edge [
    source 124
    target 125
  ]
  edge [
    source 124
    target 502
  ]
  edge [
    source 124
    target 1041
  ]
  edge [
    source 125
    target 126
  ]
  edge [
    source 125
    target 1042
  ]
  edge [
    source 125
    target 1043
  ]
  edge [
    source 125
    target 1044
  ]
  edge [
    source 125
    target 1045
  ]
  edge [
    source 125
    target 1046
  ]
  edge [
    source 125
    target 1047
  ]
  edge [
    source 125
    target 1048
  ]
  edge [
    source 126
    target 127
  ]
  edge [
    source 126
    target 137
  ]
  edge [
    source 126
    target 1049
  ]
  edge [
    source 126
    target 1050
  ]
  edge [
    source 126
    target 1051
  ]
  edge [
    source 126
    target 1052
  ]
  edge [
    source 126
    target 400
  ]
  edge [
    source 126
    target 456
  ]
  edge [
    source 126
    target 147
  ]
  edge [
    source 127
    target 128
  ]
  edge [
    source 127
    target 1053
  ]
  edge [
    source 127
    target 1054
  ]
  edge [
    source 127
    target 1055
  ]
  edge [
    source 127
    target 1056
  ]
  edge [
    source 127
    target 1057
  ]
  edge [
    source 127
    target 1058
  ]
  edge [
    source 127
    target 557
  ]
  edge [
    source 127
    target 1059
  ]
  edge [
    source 127
    target 1060
  ]
  edge [
    source 127
    target 1061
  ]
  edge [
    source 127
    target 1062
  ]
  edge [
    source 127
    target 1063
  ]
  edge [
    source 127
    target 1064
  ]
  edge [
    source 127
    target 1065
  ]
  edge [
    source 127
    target 1066
  ]
  edge [
    source 127
    target 1067
  ]
  edge [
    source 127
    target 1068
  ]
  edge [
    source 128
    target 129
  ]
  edge [
    source 128
    target 1069
  ]
  edge [
    source 128
    target 1070
  ]
  edge [
    source 128
    target 1071
  ]
  edge [
    source 128
    target 1072
  ]
  edge [
    source 128
    target 1073
  ]
  edge [
    source 128
    target 1074
  ]
  edge [
    source 128
    target 1075
  ]
  edge [
    source 128
    target 1076
  ]
  edge [
    source 128
    target 1077
  ]
  edge [
    source 128
    target 1078
  ]
  edge [
    source 129
    target 130
  ]
  edge [
    source 129
    target 1079
  ]
  edge [
    source 129
    target 1080
  ]
  edge [
    source 129
    target 1081
  ]
  edge [
    source 129
    target 1082
  ]
  edge [
    source 129
    target 1083
  ]
  edge [
    source 129
    target 1084
  ]
  edge [
    source 129
    target 1085
  ]
  edge [
    source 129
    target 1086
  ]
  edge [
    source 129
    target 1087
  ]
  edge [
    source 129
    target 1088
  ]
  edge [
    source 129
    target 1089
  ]
  edge [
    source 129
    target 1090
  ]
  edge [
    source 129
    target 1091
  ]
  edge [
    source 130
    target 131
  ]
  edge [
    source 130
    target 1092
  ]
  edge [
    source 130
    target 1093
  ]
  edge [
    source 130
    target 1094
  ]
  edge [
    source 130
    target 1095
  ]
  edge [
    source 130
    target 1096
  ]
  edge [
    source 130
    target 146
  ]
  edge [
    source 131
    target 132
  ]
  edge [
    source 131
    target 515
  ]
  edge [
    source 131
    target 1097
  ]
  edge [
    source 131
    target 1098
  ]
  edge [
    source 131
    target 564
  ]
  edge [
    source 131
    target 1099
  ]
  edge [
    source 131
    target 565
  ]
  edge [
    source 131
    target 573
  ]
  edge [
    source 131
    target 1100
  ]
  edge [
    source 131
    target 1101
  ]
  edge [
    source 131
    target 1102
  ]
  edge [
    source 131
    target 1103
  ]
  edge [
    source 131
    target 1104
  ]
  edge [
    source 132
    target 133
  ]
  edge [
    source 133
    target 134
  ]
  edge [
    source 133
    target 1105
  ]
  edge [
    source 133
    target 1078
  ]
  edge [
    source 133
    target 138
  ]
  edge [
    source 133
    target 151
  ]
  edge [
    source 133
    target 162
  ]
  edge [
    source 134
    target 135
  ]
  edge [
    source 134
    target 165
  ]
  edge [
    source 134
    target 166
  ]
  edge [
    source 134
    target 1106
  ]
  edge [
    source 134
    target 1107
  ]
  edge [
    source 134
    target 1108
  ]
  edge [
    source 134
    target 1109
  ]
  edge [
    source 134
    target 880
  ]
  edge [
    source 134
    target 1110
  ]
  edge [
    source 134
    target 882
  ]
  edge [
    source 134
    target 1111
  ]
  edge [
    source 134
    target 1112
  ]
  edge [
    source 134
    target 1113
  ]
  edge [
    source 134
    target 1114
  ]
  edge [
    source 134
    target 1115
  ]
  edge [
    source 135
    target 136
  ]
  edge [
    source 135
    target 1116
  ]
  edge [
    source 135
    target 1117
  ]
  edge [
    source 135
    target 1118
  ]
  edge [
    source 135
    target 1119
  ]
  edge [
    source 135
    target 1120
  ]
  edge [
    source 135
    target 1121
  ]
  edge [
    source 135
    target 1122
  ]
  edge [
    source 135
    target 1123
  ]
  edge [
    source 135
    target 1056
  ]
  edge [
    source 135
    target 1124
  ]
  edge [
    source 135
    target 1125
  ]
  edge [
    source 135
    target 1126
  ]
  edge [
    source 135
    target 1127
  ]
  edge [
    source 135
    target 1128
  ]
  edge [
    source 135
    target 1129
  ]
  edge [
    source 135
    target 1130
  ]
  edge [
    source 135
    target 1131
  ]
  edge [
    source 135
    target 1132
  ]
  edge [
    source 135
    target 357
  ]
  edge [
    source 135
    target 1133
  ]
  edge [
    source 135
    target 1134
  ]
  edge [
    source 135
    target 1135
  ]
  edge [
    source 135
    target 1136
  ]
  edge [
    source 135
    target 1137
  ]
  edge [
    source 137
    target 1138
  ]
  edge [
    source 138
    target 1139
  ]
  edge [
    source 138
    target 1140
  ]
  edge [
    source 138
    target 1141
  ]
  edge [
    source 138
    target 1142
  ]
  edge [
    source 138
    target 1143
  ]
  edge [
    source 138
    target 1144
  ]
  edge [
    source 138
    target 151
  ]
  edge [
    source 138
    target 162
  ]
  edge [
    source 139
    target 140
  ]
  edge [
    source 139
    target 1145
  ]
  edge [
    source 139
    target 175
  ]
  edge [
    source 140
    target 141
  ]
  edge [
    source 140
    target 580
  ]
  edge [
    source 141
    target 142
  ]
  edge [
    source 141
    target 1146
  ]
  edge [
    source 141
    target 1147
  ]
  edge [
    source 141
    target 1148
  ]
  edge [
    source 141
    target 887
  ]
  edge [
    source 141
    target 1149
  ]
  edge [
    source 141
    target 586
  ]
  edge [
    source 141
    target 1150
  ]
  edge [
    source 141
    target 1151
  ]
  edge [
    source 141
    target 1152
  ]
  edge [
    source 141
    target 1153
  ]
  edge [
    source 141
    target 1154
  ]
  edge [
    source 141
    target 1155
  ]
  edge [
    source 141
    target 1156
  ]
  edge [
    source 141
    target 1157
  ]
  edge [
    source 141
    target 1158
  ]
  edge [
    source 141
    target 1159
  ]
  edge [
    source 141
    target 1160
  ]
  edge [
    source 141
    target 1161
  ]
  edge [
    source 141
    target 1162
  ]
  edge [
    source 141
    target 1163
  ]
  edge [
    source 141
    target 188
  ]
  edge [
    source 141
    target 1164
  ]
  edge [
    source 141
    target 588
  ]
  edge [
    source 141
    target 1165
  ]
  edge [
    source 141
    target 1166
  ]
  edge [
    source 141
    target 1167
  ]
  edge [
    source 141
    target 1168
  ]
  edge [
    source 141
    target 683
  ]
  edge [
    source 141
    target 1169
  ]
  edge [
    source 141
    target 1170
  ]
  edge [
    source 141
    target 1171
  ]
  edge [
    source 141
    target 1172
  ]
  edge [
    source 141
    target 1173
  ]
  edge [
    source 141
    target 1174
  ]
  edge [
    source 141
    target 1175
  ]
  edge [
    source 141
    target 1176
  ]
  edge [
    source 141
    target 1177
  ]
  edge [
    source 141
    target 1178
  ]
  edge [
    source 141
    target 1179
  ]
  edge [
    source 141
    target 150
  ]
  edge [
    source 142
    target 143
  ]
  edge [
    source 142
    target 1180
  ]
  edge [
    source 142
    target 1181
  ]
  edge [
    source 142
    target 1182
  ]
  edge [
    source 143
    target 144
  ]
  edge [
    source 143
    target 1183
  ]
  edge [
    source 143
    target 568
  ]
  edge [
    source 143
    target 1184
  ]
  edge [
    source 143
    target 1185
  ]
  edge [
    source 143
    target 1186
  ]
  edge [
    source 143
    target 1187
  ]
  edge [
    source 143
    target 1188
  ]
  edge [
    source 143
    target 1189
  ]
  edge [
    source 144
    target 145
  ]
  edge [
    source 144
    target 1190
  ]
  edge [
    source 144
    target 1191
  ]
  edge [
    source 144
    target 1192
  ]
  edge [
    source 144
    target 1193
  ]
  edge [
    source 144
    target 1194
  ]
  edge [
    source 144
    target 1195
  ]
  edge [
    source 144
    target 1196
  ]
  edge [
    source 144
    target 637
  ]
  edge [
    source 144
    target 1197
  ]
  edge [
    source 144
    target 1186
  ]
  edge [
    source 144
    target 1198
  ]
  edge [
    source 144
    target 560
  ]
  edge [
    source 144
    target 1199
  ]
  edge [
    source 144
    target 1200
  ]
  edge [
    source 145
    target 146
  ]
  edge [
    source 145
    target 176
  ]
  edge [
    source 145
    target 183
  ]
  edge [
    source 145
    target 184
  ]
  edge [
    source 146
    target 147
  ]
  edge [
    source 146
    target 1201
  ]
  edge [
    source 146
    target 1202
  ]
  edge [
    source 146
    target 1203
  ]
  edge [
    source 146
    target 1204
  ]
  edge [
    source 146
    target 1205
  ]
  edge [
    source 146
    target 1206
  ]
  edge [
    source 146
    target 1207
  ]
  edge [
    source 146
    target 1208
  ]
  edge [
    source 146
    target 1209
  ]
  edge [
    source 146
    target 1210
  ]
  edge [
    source 146
    target 1211
  ]
  edge [
    source 147
    target 148
  ]
  edge [
    source 147
    target 1212
  ]
  edge [
    source 147
    target 1213
  ]
  edge [
    source 147
    target 1214
  ]
  edge [
    source 147
    target 436
  ]
  edge [
    source 147
    target 1215
  ]
  edge [
    source 148
    target 149
  ]
  edge [
    source 148
    target 1216
  ]
  edge [
    source 148
    target 1217
  ]
  edge [
    source 148
    target 1218
  ]
  edge [
    source 149
    target 150
  ]
  edge [
    source 149
    target 1219
  ]
  edge [
    source 149
    target 1220
  ]
  edge [
    source 149
    target 1221
  ]
  edge [
    source 150
    target 151
  ]
  edge [
    source 151
    target 152
  ]
  edge [
    source 151
    target 1222
  ]
  edge [
    source 151
    target 1223
  ]
  edge [
    source 151
    target 690
  ]
  edge [
    source 151
    target 1224
  ]
  edge [
    source 151
    target 1225
  ]
  edge [
    source 151
    target 1226
  ]
  edge [
    source 151
    target 1227
  ]
  edge [
    source 151
    target 1228
  ]
  edge [
    source 151
    target 1229
  ]
  edge [
    source 151
    target 1230
  ]
  edge [
    source 151
    target 1231
  ]
  edge [
    source 151
    target 1232
  ]
  edge [
    source 151
    target 1233
  ]
  edge [
    source 151
    target 1234
  ]
  edge [
    source 151
    target 1235
  ]
  edge [
    source 151
    target 662
  ]
  edge [
    source 151
    target 1236
  ]
  edge [
    source 151
    target 313
  ]
  edge [
    source 151
    target 808
  ]
  edge [
    source 151
    target 380
  ]
  edge [
    source 151
    target 1237
  ]
  edge [
    source 151
    target 1238
  ]
  edge [
    source 151
    target 1239
  ]
  edge [
    source 151
    target 1141
  ]
  edge [
    source 151
    target 1240
  ]
  edge [
    source 151
    target 1241
  ]
  edge [
    source 151
    target 1242
  ]
  edge [
    source 151
    target 162
  ]
  edge [
    source 152
    target 153
  ]
  edge [
    source 153
    target 154
  ]
  edge [
    source 153
    target 1243
  ]
  edge [
    source 153
    target 1244
  ]
  edge [
    source 153
    target 1245
  ]
  edge [
    source 153
    target 1246
  ]
  edge [
    source 153
    target 1247
  ]
  edge [
    source 153
    target 1248
  ]
  edge [
    source 153
    target 753
  ]
  edge [
    source 153
    target 1249
  ]
  edge [
    source 153
    target 1250
  ]
  edge [
    source 153
    target 1251
  ]
  edge [
    source 153
    target 1252
  ]
  edge [
    source 153
    target 1253
  ]
  edge [
    source 153
    target 1254
  ]
  edge [
    source 153
    target 1255
  ]
  edge [
    source 153
    target 1256
  ]
  edge [
    source 153
    target 1257
  ]
  edge [
    source 153
    target 1258
  ]
  edge [
    source 153
    target 1259
  ]
  edge [
    source 153
    target 1260
  ]
  edge [
    source 153
    target 310
  ]
  edge [
    source 153
    target 1261
  ]
  edge [
    source 153
    target 941
  ]
  edge [
    source 153
    target 1262
  ]
  edge [
    source 153
    target 1263
  ]
  edge [
    source 153
    target 1264
  ]
  edge [
    source 153
    target 733
  ]
  edge [
    source 153
    target 650
  ]
  edge [
    source 153
    target 1265
  ]
  edge [
    source 153
    target 1266
  ]
  edge [
    source 153
    target 1267
  ]
  edge [
    source 153
    target 1268
  ]
  edge [
    source 153
    target 1269
  ]
  edge [
    source 153
    target 1270
  ]
  edge [
    source 153
    target 1271
  ]
  edge [
    source 153
    target 1272
  ]
  edge [
    source 153
    target 1273
  ]
  edge [
    source 153
    target 1274
  ]
  edge [
    source 153
    target 1275
  ]
  edge [
    source 153
    target 1276
  ]
  edge [
    source 153
    target 683
  ]
  edge [
    source 153
    target 1277
  ]
  edge [
    source 153
    target 600
  ]
  edge [
    source 153
    target 1278
  ]
  edge [
    source 153
    target 1174
  ]
  edge [
    source 153
    target 1279
  ]
  edge [
    source 153
    target 1280
  ]
  edge [
    source 153
    target 1281
  ]
  edge [
    source 153
    target 660
  ]
  edge [
    source 154
    target 159
  ]
  edge [
    source 154
    target 155
  ]
  edge [
    source 155
    target 156
  ]
  edge [
    source 155
    target 1282
  ]
  edge [
    source 157
    target 158
  ]
  edge [
    source 157
    target 477
  ]
  edge [
    source 157
    target 483
  ]
  edge [
    source 157
    target 481
  ]
  edge [
    source 157
    target 703
  ]
  edge [
    source 157
    target 1283
  ]
  edge [
    source 157
    target 1284
  ]
  edge [
    source 157
    target 642
  ]
  edge [
    source 157
    target 1221
  ]
  edge [
    source 157
    target 649
  ]
  edge [
    source 157
    target 1285
  ]
  edge [
    source 158
    target 159
  ]
  edge [
    source 158
    target 162
  ]
  edge [
    source 158
    target 183
  ]
  edge [
    source 158
    target 1286
  ]
  edge [
    source 158
    target 1287
  ]
  edge [
    source 158
    target 1288
  ]
  edge [
    source 159
    target 618
  ]
  edge [
    source 159
    target 1289
  ]
  edge [
    source 159
    target 1290
  ]
  edge [
    source 159
    target 1291
  ]
  edge [
    source 159
    target 1292
  ]
  edge [
    source 159
    target 1293
  ]
  edge [
    source 160
    target 161
  ]
  edge [
    source 160
    target 359
  ]
  edge [
    source 160
    target 1294
  ]
  edge [
    source 160
    target 1295
  ]
  edge [
    source 160
    target 1296
  ]
  edge [
    source 160
    target 1297
  ]
  edge [
    source 161
    target 1298
  ]
  edge [
    source 161
    target 1299
  ]
  edge [
    source 161
    target 1300
  ]
  edge [
    source 161
    target 167
  ]
  edge [
    source 162
    target 182
  ]
  edge [
    source 162
    target 1301
  ]
  edge [
    source 162
    target 1181
  ]
  edge [
    source 162
    target 1302
  ]
  edge [
    source 162
    target 1303
  ]
  edge [
    source 162
    target 452
  ]
  edge [
    source 162
    target 1132
  ]
  edge [
    source 162
    target 1304
  ]
  edge [
    source 163
    target 164
  ]
  edge [
    source 164
    target 165
  ]
  edge [
    source 164
    target 1305
  ]
  edge [
    source 166
    target 167
  ]
  edge [
    source 166
    target 1306
  ]
  edge [
    source 166
    target 1307
  ]
  edge [
    source 166
    target 1308
  ]
  edge [
    source 166
    target 1309
  ]
  edge [
    source 166
    target 1310
  ]
  edge [
    source 166
    target 1311
  ]
  edge [
    source 166
    target 1302
  ]
  edge [
    source 166
    target 337
  ]
  edge [
    source 166
    target 1312
  ]
  edge [
    source 166
    target 1313
  ]
  edge [
    source 166
    target 451
  ]
  edge [
    source 166
    target 1314
  ]
  edge [
    source 166
    target 1315
  ]
  edge [
    source 166
    target 1316
  ]
  edge [
    source 166
    target 1317
  ]
  edge [
    source 166
    target 1318
  ]
  edge [
    source 166
    target 1319
  ]
  edge [
    source 166
    target 1320
  ]
  edge [
    source 166
    target 313
  ]
  edge [
    source 166
    target 1321
  ]
  edge [
    source 166
    target 1322
  ]
  edge [
    source 166
    target 1323
  ]
  edge [
    source 166
    target 1324
  ]
  edge [
    source 166
    target 1325
  ]
  edge [
    source 166
    target 1326
  ]
  edge [
    source 166
    target 1327
  ]
  edge [
    source 166
    target 1328
  ]
  edge [
    source 166
    target 1329
  ]
  edge [
    source 166
    target 1330
  ]
  edge [
    source 166
    target 1331
  ]
  edge [
    source 167
    target 168
  ]
  edge [
    source 167
    target 1332
  ]
  edge [
    source 167
    target 1333
  ]
  edge [
    source 167
    target 1334
  ]
  edge [
    source 167
    target 1335
  ]
  edge [
    source 168
    target 1336
  ]
  edge [
    source 168
    target 1337
  ]
  edge [
    source 168
    target 1338
  ]
  edge [
    source 168
    target 1339
  ]
  edge [
    source 168
    target 1151
  ]
  edge [
    source 168
    target 1340
  ]
  edge [
    source 169
    target 170
  ]
  edge [
    source 169
    target 693
  ]
  edge [
    source 169
    target 1341
  ]
  edge [
    source 169
    target 1342
  ]
  edge [
    source 169
    target 1343
  ]
  edge [
    source 169
    target 1344
  ]
  edge [
    source 169
    target 489
  ]
  edge [
    source 169
    target 849
  ]
  edge [
    source 169
    target 1345
  ]
  edge [
    source 169
    target 1346
  ]
  edge [
    source 169
    target 1347
  ]
  edge [
    source 170
    target 171
  ]
  edge [
    source 171
    target 172
  ]
  edge [
    source 171
    target 1348
  ]
  edge [
    source 171
    target 1349
  ]
  edge [
    source 171
    target 848
  ]
  edge [
    source 172
    target 173
  ]
  edge [
    source 173
    target 174
  ]
  edge [
    source 174
    target 175
  ]
  edge [
    source 175
    target 1350
  ]
  edge [
    source 175
    target 1351
  ]
  edge [
    source 175
    target 1352
  ]
  edge [
    source 175
    target 1353
  ]
  edge [
    source 175
    target 760
  ]
  edge [
    source 175
    target 1354
  ]
  edge [
    source 176
    target 1355
  ]
  edge [
    source 176
    target 1356
  ]
  edge [
    source 176
    target 1357
  ]
  edge [
    source 176
    target 1358
  ]
  edge [
    source 176
    target 1359
  ]
  edge [
    source 176
    target 1360
  ]
  edge [
    source 176
    target 1361
  ]
  edge [
    source 176
    target 1362
  ]
  edge [
    source 176
    target 1363
  ]
  edge [
    source 176
    target 1364
  ]
  edge [
    source 176
    target 1365
  ]
  edge [
    source 176
    target 1366
  ]
  edge [
    source 177
    target 178
  ]
  edge [
    source 177
    target 1367
  ]
  edge [
    source 177
    target 1368
  ]
  edge [
    source 177
    target 1369
  ]
  edge [
    source 178
    target 179
  ]
  edge [
    source 178
    target 690
  ]
  edge [
    source 178
    target 1370
  ]
  edge [
    source 178
    target 1371
  ]
  edge [
    source 178
    target 770
  ]
  edge [
    source 178
    target 1372
  ]
  edge [
    source 179
    target 180
  ]
  edge [
    source 179
    target 1373
  ]
  edge [
    source 180
    target 181
  ]
  edge [
    source 180
    target 359
  ]
  edge [
    source 180
    target 1374
  ]
  edge [
    source 180
    target 797
  ]
  edge [
    source 180
    target 432
  ]
  edge [
    source 180
    target 1375
  ]
  edge [
    source 180
    target 1376
  ]
  edge [
    source 180
    target 1377
  ]
  edge [
    source 180
    target 1378
  ]
  edge [
    source 180
    target 1379
  ]
  edge [
    source 180
    target 1380
  ]
  edge [
    source 180
    target 1381
  ]
  edge [
    source 180
    target 760
  ]
  edge [
    source 180
    target 1382
  ]
  edge [
    source 180
    target 438
  ]
  edge [
    source 180
    target 1383
  ]
  edge [
    source 181
    target 182
  ]
  edge [
    source 182
    target 690
  ]
  edge [
    source 182
    target 481
  ]
  edge [
    source 182
    target 770
  ]
  edge [
    source 182
    target 1384
  ]
  edge [
    source 182
    target 1385
  ]
  edge [
    source 182
    target 1386
  ]
  edge [
    source 182
    target 489
  ]
  edge [
    source 182
    target 1387
  ]
  edge [
    source 182
    target 1388
  ]
  edge [
    source 183
    target 1389
  ]
  edge [
    source 183
    target 1390
  ]
  edge [
    source 183
    target 1056
  ]
  edge [
    source 183
    target 1391
  ]
  edge [
    source 183
    target 1392
  ]
  edge [
    source 183
    target 1393
  ]
  edge [
    source 183
    target 1394
  ]
  edge [
    source 183
    target 1395
  ]
  edge [
    source 183
    target 1396
  ]
  edge [
    source 183
    target 1397
  ]
  edge [
    source 184
    target 185
  ]
  edge [
    source 185
    target 186
  ]
  edge [
    source 185
    target 1398
  ]
  edge [
    source 185
    target 1399
  ]
  edge [
    source 185
    target 1400
  ]
  edge [
    source 185
    target 1401
  ]
  edge [
    source 186
    target 187
  ]
  edge [
    source 186
    target 204
  ]
  edge [
    source 186
    target 1402
  ]
  edge [
    source 186
    target 557
  ]
  edge [
    source 186
    target 1403
  ]
  edge [
    source 186
    target 1404
  ]
  edge [
    source 186
    target 1405
  ]
  edge [
    source 187
    target 188
  ]
  edge [
    source 188
    target 1406
  ]
  edge [
    source 188
    target 1407
  ]
  edge [
    source 188
    target 1408
  ]
  edge [
    source 188
    target 1409
  ]
  edge [
    source 188
    target 1410
  ]
  edge [
    source 188
    target 1411
  ]
  edge [
    source 188
    target 1412
  ]
  edge [
    source 188
    target 482
  ]
  edge [
    source 188
    target 1413
  ]
  edge [
    source 188
    target 1414
  ]
  edge [
    source 188
    target 1415
  ]
  edge [
    source 188
    target 1416
  ]
  edge [
    source 188
    target 1417
  ]
  edge [
    source 188
    target 1418
  ]
  edge [
    source 188
    target 1419
  ]
  edge [
    source 188
    target 1420
  ]
  edge [
    source 188
    target 1421
  ]
  edge [
    source 188
    target 1422
  ]
  edge [
    source 188
    target 1423
  ]
  edge [
    source 188
    target 1424
  ]
  edge [
    source 188
    target 1425
  ]
  edge [
    source 188
    target 1426
  ]
  edge [
    source 188
    target 1427
  ]
]
