graph [
  maxDegree 36
  minDegree 1
  meanDegree 1.971830985915493
  density 0.028169014084507043
  graphCliqueNumber 2
  node [
    id 0
    label "wezwa"
    origin "text"
  ]
  node [
    id 1
    label "pomoc"
    origin "text"
  ]
  node [
    id 2
    label "krzykn&#261;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "landau"
    origin "text"
  ]
  node [
    id 4
    label "razem"
    origin "text"
  ]
  node [
    id 5
    label "zszokowa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "spa&#347;lakiem"
    origin "text"
  ]
  node [
    id 7
    label "pop&#281;dzi&#263;"
    origin "text"
  ]
  node [
    id 8
    label "strona"
    origin "text"
  ]
  node [
    id 9
    label "rezydencja"
    origin "text"
  ]
  node [
    id 10
    label "zgodzi&#263;"
  ]
  node [
    id 11
    label "pomocnik"
  ]
  node [
    id 12
    label "doch&#243;d"
  ]
  node [
    id 13
    label "property"
  ]
  node [
    id 14
    label "przedmiot"
  ]
  node [
    id 15
    label "grupa"
  ]
  node [
    id 16
    label "telefon_zaufania"
  ]
  node [
    id 17
    label "darowizna"
  ]
  node [
    id 18
    label "&#347;rodek"
  ]
  node [
    id 19
    label "liga"
  ]
  node [
    id 20
    label "wydoby&#263;"
  ]
  node [
    id 21
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 22
    label "shout"
  ]
  node [
    id 23
    label "&#322;&#261;cznie"
  ]
  node [
    id 24
    label "oburzy&#263;"
  ]
  node [
    id 25
    label "przerazi&#263;"
  ]
  node [
    id 26
    label "zdziwi&#263;"
  ]
  node [
    id 27
    label "wzbudzi&#263;"
  ]
  node [
    id 28
    label "shock"
  ]
  node [
    id 29
    label "poruszy&#263;"
  ]
  node [
    id 30
    label "induce"
  ]
  node [
    id 31
    label "zmusi&#263;"
  ]
  node [
    id 32
    label "rush"
  ]
  node [
    id 33
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 34
    label "znagli&#263;"
  ]
  node [
    id 35
    label "skr&#281;canie"
  ]
  node [
    id 36
    label "voice"
  ]
  node [
    id 37
    label "forma"
  ]
  node [
    id 38
    label "internet"
  ]
  node [
    id 39
    label "skr&#281;ci&#263;"
  ]
  node [
    id 40
    label "kartka"
  ]
  node [
    id 41
    label "orientowa&#263;"
  ]
  node [
    id 42
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 43
    label "powierzchnia"
  ]
  node [
    id 44
    label "plik"
  ]
  node [
    id 45
    label "bok"
  ]
  node [
    id 46
    label "pagina"
  ]
  node [
    id 47
    label "orientowanie"
  ]
  node [
    id 48
    label "fragment"
  ]
  node [
    id 49
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 50
    label "s&#261;d"
  ]
  node [
    id 51
    label "skr&#281;ca&#263;"
  ]
  node [
    id 52
    label "g&#243;ra"
  ]
  node [
    id 53
    label "serwis_internetowy"
  ]
  node [
    id 54
    label "orientacja"
  ]
  node [
    id 55
    label "linia"
  ]
  node [
    id 56
    label "skr&#281;cenie"
  ]
  node [
    id 57
    label "layout"
  ]
  node [
    id 58
    label "zorientowa&#263;"
  ]
  node [
    id 59
    label "zorientowanie"
  ]
  node [
    id 60
    label "obiekt"
  ]
  node [
    id 61
    label "podmiot"
  ]
  node [
    id 62
    label "ty&#322;"
  ]
  node [
    id 63
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 64
    label "logowanie"
  ]
  node [
    id 65
    label "adres_internetowy"
  ]
  node [
    id 66
    label "uj&#281;cie"
  ]
  node [
    id 67
    label "prz&#243;d"
  ]
  node [
    id 68
    label "posta&#263;"
  ]
  node [
    id 69
    label "siedziba"
  ]
  node [
    id 70
    label "dom"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 30
  ]
  edge [
    source 7
    target 31
  ]
  edge [
    source 7
    target 32
  ]
  edge [
    source 7
    target 33
  ]
  edge [
    source 7
    target 34
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 35
  ]
  edge [
    source 8
    target 36
  ]
  edge [
    source 8
    target 37
  ]
  edge [
    source 8
    target 38
  ]
  edge [
    source 8
    target 39
  ]
  edge [
    source 8
    target 40
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 8
    target 46
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
]
