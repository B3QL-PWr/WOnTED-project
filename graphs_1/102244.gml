graph [
  maxDegree 28
  minDegree 1
  meanDegree 4.609375
  density 0.036294291338582675
  graphCliqueNumber 16
  node [
    id 0
    label "by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "niezgodny"
    origin "text"
  ]
  node [
    id 2
    label "art"
    origin "text"
  ]
  node [
    id 3
    label "usta"
    origin "text"
  ]
  node [
    id 4
    label "konstytucja"
    origin "text"
  ]
  node [
    id 5
    label "rzeczpospolita"
    origin "text"
  ]
  node [
    id 6
    label "polski"
    origin "text"
  ]
  node [
    id 7
    label "si&#281;ga&#263;"
  ]
  node [
    id 8
    label "trwa&#263;"
  ]
  node [
    id 9
    label "obecno&#347;&#263;"
  ]
  node [
    id 10
    label "stan"
  ]
  node [
    id 11
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 12
    label "stand"
  ]
  node [
    id 13
    label "mie&#263;_miejsce"
  ]
  node [
    id 14
    label "uczestniczy&#263;"
  ]
  node [
    id 15
    label "chodzi&#263;"
  ]
  node [
    id 16
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 17
    label "equal"
  ]
  node [
    id 18
    label "niezgodnie"
  ]
  node [
    id 19
    label "odmienny"
  ]
  node [
    id 20
    label "r&#243;&#380;ny"
  ]
  node [
    id 21
    label "niespokojny"
  ]
  node [
    id 22
    label "k&#322;&#243;tny"
  ]
  node [
    id 23
    label "napi&#281;ty"
  ]
  node [
    id 24
    label "warga_dolna"
  ]
  node [
    id 25
    label "ryjek"
  ]
  node [
    id 26
    label "zaci&#261;&#263;"
  ]
  node [
    id 27
    label "ssa&#263;"
  ]
  node [
    id 28
    label "twarz"
  ]
  node [
    id 29
    label "dzi&#243;b"
  ]
  node [
    id 30
    label "&#322;uk_Kupidyna"
  ]
  node [
    id 31
    label "ssanie"
  ]
  node [
    id 32
    label "zaci&#281;cie"
  ]
  node [
    id 33
    label "jadaczka"
  ]
  node [
    id 34
    label "zacinanie"
  ]
  node [
    id 35
    label "organ"
  ]
  node [
    id 36
    label "jama_ustna"
  ]
  node [
    id 37
    label "otw&#243;r_g&#281;bowy"
  ]
  node [
    id 38
    label "warga_g&#243;rna"
  ]
  node [
    id 39
    label "zacina&#263;"
  ]
  node [
    id 40
    label "dokument"
  ]
  node [
    id 41
    label "budowa"
  ]
  node [
    id 42
    label "akt"
  ]
  node [
    id 43
    label "cezar"
  ]
  node [
    id 44
    label "zbi&#243;r"
  ]
  node [
    id 45
    label "uchwa&#322;a"
  ]
  node [
    id 46
    label "struktura"
  ]
  node [
    id 47
    label "zwi&#261;zek_chemiczny"
  ]
  node [
    id 48
    label "Buriacja"
  ]
  node [
    id 49
    label "Abchazja"
  ]
  node [
    id 50
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 51
    label "Inguszetia"
  ]
  node [
    id 52
    label "Nachiczewan"
  ]
  node [
    id 53
    label "Karaka&#322;pacja"
  ]
  node [
    id 54
    label "Jakucja"
  ]
  node [
    id 55
    label "Singapur"
  ]
  node [
    id 56
    label "Karelia"
  ]
  node [
    id 57
    label "Komi"
  ]
  node [
    id 58
    label "Tatarstan"
  ]
  node [
    id 59
    label "Chakasja"
  ]
  node [
    id 60
    label "Dagestan"
  ]
  node [
    id 61
    label "Mordowia"
  ]
  node [
    id 62
    label "Ka&#322;mucja"
  ]
  node [
    id 63
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 64
    label "Republika_Cisalpi&#324;ska"
  ]
  node [
    id 65
    label "Baszkiria"
  ]
  node [
    id 66
    label "pa&#324;stwo_demokratyczne"
  ]
  node [
    id 67
    label "Mari_El"
  ]
  node [
    id 68
    label "Ad&#380;aria"
  ]
  node [
    id 69
    label "Czuwaszja"
  ]
  node [
    id 70
    label "Tuwa"
  ]
  node [
    id 71
    label "Czeczenia"
  ]
  node [
    id 72
    label "Udmurcja"
  ]
  node [
    id 73
    label "pa&#324;stwo"
  ]
  node [
    id 74
    label "lacki"
  ]
  node [
    id 75
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 76
    label "przedmiot"
  ]
  node [
    id 77
    label "sztajer"
  ]
  node [
    id 78
    label "drabant"
  ]
  node [
    id 79
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 80
    label "polak"
  ]
  node [
    id 81
    label "pierogi_ruskie"
  ]
  node [
    id 82
    label "krakowiak"
  ]
  node [
    id 83
    label "Polish"
  ]
  node [
    id 84
    label "j&#281;zyk"
  ]
  node [
    id 85
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 86
    label "oberek"
  ]
  node [
    id 87
    label "po_polsku"
  ]
  node [
    id 88
    label "mazur"
  ]
  node [
    id 89
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 90
    label "chodzony"
  ]
  node [
    id 91
    label "skoczny"
  ]
  node [
    id 92
    label "ryba_po_grecku"
  ]
  node [
    id 93
    label "goniony"
  ]
  node [
    id 94
    label "polsko"
  ]
  node [
    id 95
    label "kodeks"
  ]
  node [
    id 96
    label "cywilny"
  ]
  node [
    id 97
    label "ustawa"
  ]
  node [
    id 98
    label "zeszyt"
  ]
  node [
    id 99
    label "dzie&#324;"
  ]
  node [
    id 100
    label "28"
  ]
  node [
    id 101
    label "lipiec"
  ]
  node [
    id 102
    label "1990"
  ]
  node [
    id 103
    label "rok"
  ]
  node [
    id 104
    label "ojciec"
  ]
  node [
    id 105
    label "zmiana"
  ]
  node [
    id 106
    label "dziennik"
  ]
  node [
    id 107
    label "u"
  ]
  node [
    id 108
    label "ustawi&#263;"
  ]
  node [
    id 109
    label "rozporz&#261;dzi&#263;"
  ]
  node [
    id 110
    label "rada"
  ]
  node [
    id 111
    label "minister"
  ]
  node [
    id 112
    label "12"
  ]
  node [
    id 113
    label "grudzie&#324;"
  ]
  node [
    id 114
    label "wyspa"
  ]
  node [
    id 115
    label "sprawa"
  ]
  node [
    id 116
    label "warunki"
  ]
  node [
    id 117
    label "dziedziczy&#263;"
  ]
  node [
    id 118
    label "ustawowy"
  ]
  node [
    id 119
    label "gospodarstwo"
  ]
  node [
    id 120
    label "rolny"
  ]
  node [
    id 121
    label "konwencja"
  ]
  node [
    id 122
    label "ochrona"
  ]
  node [
    id 123
    label "prawy"
  ]
  node [
    id 124
    label "cz&#322;owiek"
  ]
  node [
    id 125
    label "i"
  ]
  node [
    id 126
    label "podstawowy"
  ]
  node [
    id 127
    label "wolno&#347;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 95
    target 96
  ]
  edge [
    source 95
    target 97
  ]
  edge [
    source 95
    target 98
  ]
  edge [
    source 95
    target 99
  ]
  edge [
    source 95
    target 100
  ]
  edge [
    source 95
    target 101
  ]
  edge [
    source 95
    target 102
  ]
  edge [
    source 95
    target 103
  ]
  edge [
    source 95
    target 104
  ]
  edge [
    source 95
    target 105
  ]
  edge [
    source 96
    target 97
  ]
  edge [
    source 96
    target 98
  ]
  edge [
    source 96
    target 99
  ]
  edge [
    source 96
    target 100
  ]
  edge [
    source 96
    target 101
  ]
  edge [
    source 96
    target 102
  ]
  edge [
    source 96
    target 103
  ]
  edge [
    source 96
    target 104
  ]
  edge [
    source 96
    target 105
  ]
  edge [
    source 97
    target 98
  ]
  edge [
    source 97
    target 99
  ]
  edge [
    source 97
    target 100
  ]
  edge [
    source 97
    target 101
  ]
  edge [
    source 97
    target 102
  ]
  edge [
    source 97
    target 103
  ]
  edge [
    source 97
    target 104
  ]
  edge [
    source 97
    target 105
  ]
  edge [
    source 97
    target 97
  ]
  edge [
    source 98
    target 99
  ]
  edge [
    source 98
    target 100
  ]
  edge [
    source 98
    target 101
  ]
  edge [
    source 98
    target 102
  ]
  edge [
    source 98
    target 103
  ]
  edge [
    source 98
    target 104
  ]
  edge [
    source 98
    target 105
  ]
  edge [
    source 98
    target 109
  ]
  edge [
    source 98
    target 110
  ]
  edge [
    source 98
    target 111
  ]
  edge [
    source 98
    target 112
  ]
  edge [
    source 98
    target 113
  ]
  edge [
    source 98
    target 114
  ]
  edge [
    source 98
    target 115
  ]
  edge [
    source 98
    target 116
  ]
  edge [
    source 98
    target 117
  ]
  edge [
    source 98
    target 118
  ]
  edge [
    source 98
    target 119
  ]
  edge [
    source 98
    target 120
  ]
  edge [
    source 99
    target 100
  ]
  edge [
    source 99
    target 101
  ]
  edge [
    source 99
    target 102
  ]
  edge [
    source 99
    target 103
  ]
  edge [
    source 99
    target 104
  ]
  edge [
    source 99
    target 105
  ]
  edge [
    source 99
    target 109
  ]
  edge [
    source 99
    target 110
  ]
  edge [
    source 99
    target 111
  ]
  edge [
    source 99
    target 112
  ]
  edge [
    source 99
    target 113
  ]
  edge [
    source 99
    target 114
  ]
  edge [
    source 99
    target 115
  ]
  edge [
    source 99
    target 116
  ]
  edge [
    source 99
    target 117
  ]
  edge [
    source 99
    target 118
  ]
  edge [
    source 99
    target 119
  ]
  edge [
    source 99
    target 120
  ]
  edge [
    source 100
    target 101
  ]
  edge [
    source 100
    target 102
  ]
  edge [
    source 100
    target 103
  ]
  edge [
    source 100
    target 104
  ]
  edge [
    source 100
    target 105
  ]
  edge [
    source 101
    target 102
  ]
  edge [
    source 101
    target 103
  ]
  edge [
    source 101
    target 104
  ]
  edge [
    source 101
    target 105
  ]
  edge [
    source 102
    target 103
  ]
  edge [
    source 102
    target 104
  ]
  edge [
    source 102
    target 105
  ]
  edge [
    source 102
    target 109
  ]
  edge [
    source 102
    target 110
  ]
  edge [
    source 102
    target 111
  ]
  edge [
    source 102
    target 112
  ]
  edge [
    source 102
    target 113
  ]
  edge [
    source 102
    target 114
  ]
  edge [
    source 102
    target 115
  ]
  edge [
    source 102
    target 116
  ]
  edge [
    source 102
    target 117
  ]
  edge [
    source 102
    target 118
  ]
  edge [
    source 102
    target 119
  ]
  edge [
    source 102
    target 120
  ]
  edge [
    source 103
    target 104
  ]
  edge [
    source 103
    target 105
  ]
  edge [
    source 103
    target 109
  ]
  edge [
    source 103
    target 110
  ]
  edge [
    source 103
    target 111
  ]
  edge [
    source 103
    target 112
  ]
  edge [
    source 103
    target 113
  ]
  edge [
    source 103
    target 114
  ]
  edge [
    source 103
    target 115
  ]
  edge [
    source 103
    target 116
  ]
  edge [
    source 103
    target 117
  ]
  edge [
    source 103
    target 118
  ]
  edge [
    source 103
    target 119
  ]
  edge [
    source 103
    target 120
  ]
  edge [
    source 104
    target 105
  ]
  edge [
    source 104
    target 121
  ]
  edge [
    source 104
    target 122
  ]
  edge [
    source 104
    target 123
  ]
  edge [
    source 104
    target 124
  ]
  edge [
    source 104
    target 125
  ]
  edge [
    source 104
    target 126
  ]
  edge [
    source 104
    target 127
  ]
  edge [
    source 106
    target 107
  ]
  edge [
    source 106
    target 108
  ]
  edge [
    source 109
    target 110
  ]
  edge [
    source 109
    target 111
  ]
  edge [
    source 109
    target 112
  ]
  edge [
    source 109
    target 113
  ]
  edge [
    source 109
    target 114
  ]
  edge [
    source 109
    target 115
  ]
  edge [
    source 109
    target 116
  ]
  edge [
    source 109
    target 117
  ]
  edge [
    source 109
    target 118
  ]
  edge [
    source 109
    target 119
  ]
  edge [
    source 109
    target 120
  ]
  edge [
    source 110
    target 111
  ]
  edge [
    source 110
    target 112
  ]
  edge [
    source 110
    target 113
  ]
  edge [
    source 110
    target 114
  ]
  edge [
    source 110
    target 115
  ]
  edge [
    source 110
    target 116
  ]
  edge [
    source 110
    target 117
  ]
  edge [
    source 110
    target 118
  ]
  edge [
    source 110
    target 119
  ]
  edge [
    source 110
    target 120
  ]
  edge [
    source 111
    target 112
  ]
  edge [
    source 111
    target 113
  ]
  edge [
    source 111
    target 114
  ]
  edge [
    source 111
    target 115
  ]
  edge [
    source 111
    target 116
  ]
  edge [
    source 111
    target 117
  ]
  edge [
    source 111
    target 118
  ]
  edge [
    source 111
    target 119
  ]
  edge [
    source 111
    target 120
  ]
  edge [
    source 112
    target 113
  ]
  edge [
    source 112
    target 114
  ]
  edge [
    source 112
    target 115
  ]
  edge [
    source 112
    target 116
  ]
  edge [
    source 112
    target 117
  ]
  edge [
    source 112
    target 118
  ]
  edge [
    source 112
    target 119
  ]
  edge [
    source 112
    target 120
  ]
  edge [
    source 113
    target 114
  ]
  edge [
    source 113
    target 115
  ]
  edge [
    source 113
    target 116
  ]
  edge [
    source 113
    target 117
  ]
  edge [
    source 113
    target 118
  ]
  edge [
    source 113
    target 119
  ]
  edge [
    source 113
    target 120
  ]
  edge [
    source 114
    target 115
  ]
  edge [
    source 114
    target 116
  ]
  edge [
    source 114
    target 117
  ]
  edge [
    source 114
    target 118
  ]
  edge [
    source 114
    target 119
  ]
  edge [
    source 114
    target 120
  ]
  edge [
    source 115
    target 116
  ]
  edge [
    source 115
    target 117
  ]
  edge [
    source 115
    target 118
  ]
  edge [
    source 115
    target 119
  ]
  edge [
    source 115
    target 120
  ]
  edge [
    source 116
    target 117
  ]
  edge [
    source 116
    target 118
  ]
  edge [
    source 116
    target 119
  ]
  edge [
    source 116
    target 120
  ]
  edge [
    source 117
    target 118
  ]
  edge [
    source 117
    target 119
  ]
  edge [
    source 117
    target 120
  ]
  edge [
    source 118
    target 119
  ]
  edge [
    source 118
    target 120
  ]
  edge [
    source 119
    target 120
  ]
  edge [
    source 121
    target 122
  ]
  edge [
    source 121
    target 123
  ]
  edge [
    source 121
    target 124
  ]
  edge [
    source 121
    target 125
  ]
  edge [
    source 121
    target 126
  ]
  edge [
    source 121
    target 127
  ]
  edge [
    source 122
    target 123
  ]
  edge [
    source 122
    target 124
  ]
  edge [
    source 122
    target 125
  ]
  edge [
    source 122
    target 126
  ]
  edge [
    source 122
    target 127
  ]
  edge [
    source 123
    target 124
  ]
  edge [
    source 123
    target 125
  ]
  edge [
    source 123
    target 126
  ]
  edge [
    source 123
    target 127
  ]
  edge [
    source 124
    target 125
  ]
  edge [
    source 124
    target 126
  ]
  edge [
    source 124
    target 127
  ]
  edge [
    source 125
    target 126
  ]
  edge [
    source 125
    target 127
  ]
  edge [
    source 126
    target 127
  ]
]
