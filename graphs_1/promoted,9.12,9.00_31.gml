graph [
  maxDegree 17
  minDegree 1
  meanDegree 1.9285714285714286
  density 0.07142857142857142
  graphCliqueNumber 2
  node [
    id 0
    label "kto&#347;"
    origin "text"
  ]
  node [
    id 1
    label "kamera"
    origin "text"
  ]
  node [
    id 2
    label "cie"
    origin "text"
  ]
  node [
    id 3
    label "nachodzi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "cz&#322;owiek"
  ]
  node [
    id 5
    label "znaczenie"
  ]
  node [
    id 6
    label "go&#347;&#263;"
  ]
  node [
    id 7
    label "osoba"
  ]
  node [
    id 8
    label "posta&#263;"
  ]
  node [
    id 9
    label "krosownica"
  ]
  node [
    id 10
    label "przyrz&#261;d"
  ]
  node [
    id 11
    label "wideotelefon"
  ]
  node [
    id 12
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 13
    label "ogarnia&#263;"
  ]
  node [
    id 14
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 15
    label "niepokoi&#263;"
  ]
  node [
    id 16
    label "attack"
  ]
  node [
    id 17
    label "nak&#322;ada&#263;_si&#281;"
  ]
  node [
    id 18
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 19
    label "dopada&#263;"
  ]
  node [
    id 20
    label "przychodzi&#263;"
  ]
  node [
    id 21
    label "naje&#380;d&#380;a&#263;"
  ]
  node [
    id 22
    label "pokrywa&#263;"
  ]
  node [
    id 23
    label "przys&#322;ania&#263;"
  ]
  node [
    id 24
    label "wype&#322;nia&#263;_si&#281;"
  ]
  node [
    id 25
    label "pokrywa&#263;_si&#281;"
  ]
  node [
    id 26
    label "wsp&#243;&#322;wyst&#281;powa&#263;"
  ]
  node [
    id 27
    label "foray"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
]
