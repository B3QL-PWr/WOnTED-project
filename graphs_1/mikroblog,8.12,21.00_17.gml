graph [
  maxDegree 42
  minDegree 1
  meanDegree 1.9854014598540146
  density 0.014598540145985401
  graphCliqueNumber 3
  node [
    id 0
    label "wita&#263;"
    origin "text"
  ]
  node [
    id 1
    label "wszyscy"
    origin "text"
  ]
  node [
    id 2
    label "ostatni"
    origin "text"
  ]
  node [
    id 3
    label "wpis"
    origin "text"
  ]
  node [
    id 4
    label "temat"
    origin "text"
  ]
  node [
    id 5
    label "stan"
    origin "text"
  ]
  node [
    id 6
    label "zdrowie"
    origin "text"
  ]
  node [
    id 7
    label "moje"
    origin "text"
  ]
  node [
    id 8
    label "syn"
    origin "text"
  ]
  node [
    id 9
    label "wojtek"
    origin "text"
  ]
  node [
    id 10
    label "min&#261;&#263;"
    origin "text"
  ]
  node [
    id 11
    label "ponad"
    origin "text"
  ]
  node [
    id 12
    label "miesi&#261;c"
    origin "text"
  ]
  node [
    id 13
    label "pozdrawia&#263;"
  ]
  node [
    id 14
    label "&#347;wi&#281;towa&#263;"
  ]
  node [
    id 15
    label "greet"
  ]
  node [
    id 16
    label "uzewn&#281;trznia&#263;_si&#281;"
  ]
  node [
    id 17
    label "welcome"
  ]
  node [
    id 18
    label "cz&#322;owiek"
  ]
  node [
    id 19
    label "kolejny"
  ]
  node [
    id 20
    label "istota_&#380;ywa"
  ]
  node [
    id 21
    label "najgorszy"
  ]
  node [
    id 22
    label "aktualny"
  ]
  node [
    id 23
    label "ostatnio"
  ]
  node [
    id 24
    label "niedawno"
  ]
  node [
    id 25
    label "wi&#261;&#380;&#261;cy"
  ]
  node [
    id 26
    label "sko&#324;czony"
  ]
  node [
    id 27
    label "poprzedni"
  ]
  node [
    id 28
    label "pozosta&#322;y"
  ]
  node [
    id 29
    label "w&#261;tpliwy"
  ]
  node [
    id 30
    label "czynno&#347;&#263;"
  ]
  node [
    id 31
    label "entrance"
  ]
  node [
    id 32
    label "inscription"
  ]
  node [
    id 33
    label "akt"
  ]
  node [
    id 34
    label "op&#322;ata"
  ]
  node [
    id 35
    label "tekst"
  ]
  node [
    id 36
    label "fraza"
  ]
  node [
    id 37
    label "forma"
  ]
  node [
    id 38
    label "melodia"
  ]
  node [
    id 39
    label "rzecz"
  ]
  node [
    id 40
    label "zbacza&#263;"
  ]
  node [
    id 41
    label "entity"
  ]
  node [
    id 42
    label "omawia&#263;"
  ]
  node [
    id 43
    label "topik"
  ]
  node [
    id 44
    label "wyraz_pochodny"
  ]
  node [
    id 45
    label "om&#243;wi&#263;"
  ]
  node [
    id 46
    label "omawianie"
  ]
  node [
    id 47
    label "w&#261;tek"
  ]
  node [
    id 48
    label "forum"
  ]
  node [
    id 49
    label "cecha"
  ]
  node [
    id 50
    label "zboczenie"
  ]
  node [
    id 51
    label "zbaczanie"
  ]
  node [
    id 52
    label "tre&#347;&#263;"
  ]
  node [
    id 53
    label "tematyka"
  ]
  node [
    id 54
    label "sprawa"
  ]
  node [
    id 55
    label "istota"
  ]
  node [
    id 56
    label "otoczka"
  ]
  node [
    id 57
    label "zboczy&#263;"
  ]
  node [
    id 58
    label "om&#243;wienie"
  ]
  node [
    id 59
    label "Arizona"
  ]
  node [
    id 60
    label "Georgia"
  ]
  node [
    id 61
    label "warstwa"
  ]
  node [
    id 62
    label "jednostka_administracyjna"
  ]
  node [
    id 63
    label "Hawaje"
  ]
  node [
    id 64
    label "Goa"
  ]
  node [
    id 65
    label "Floryda"
  ]
  node [
    id 66
    label "Oklahoma"
  ]
  node [
    id 67
    label "punkt"
  ]
  node [
    id 68
    label "Alaska"
  ]
  node [
    id 69
    label "wci&#281;cie"
  ]
  node [
    id 70
    label "Alabama"
  ]
  node [
    id 71
    label "Oregon"
  ]
  node [
    id 72
    label "poziom"
  ]
  node [
    id 73
    label "by&#263;"
  ]
  node [
    id 74
    label "Teksas"
  ]
  node [
    id 75
    label "Illinois"
  ]
  node [
    id 76
    label "Waszyngton"
  ]
  node [
    id 77
    label "Jukatan"
  ]
  node [
    id 78
    label "shape"
  ]
  node [
    id 79
    label "Nowy_Meksyk"
  ]
  node [
    id 80
    label "ilo&#347;&#263;"
  ]
  node [
    id 81
    label "state"
  ]
  node [
    id 82
    label "Nowy_York"
  ]
  node [
    id 83
    label "Arakan"
  ]
  node [
    id 84
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 85
    label "Kalifornia"
  ]
  node [
    id 86
    label "wektor"
  ]
  node [
    id 87
    label "Massachusetts"
  ]
  node [
    id 88
    label "miejsce"
  ]
  node [
    id 89
    label "Pensylwania"
  ]
  node [
    id 90
    label "Michigan"
  ]
  node [
    id 91
    label "Maryland"
  ]
  node [
    id 92
    label "Ohio"
  ]
  node [
    id 93
    label "Kansas"
  ]
  node [
    id 94
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 95
    label "Luizjana"
  ]
  node [
    id 96
    label "samopoczucie"
  ]
  node [
    id 97
    label "Wirginia"
  ]
  node [
    id 98
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 99
    label "os&#322;abia&#263;"
  ]
  node [
    id 100
    label "niszczy&#263;"
  ]
  node [
    id 101
    label "zniszczy&#263;"
  ]
  node [
    id 102
    label "firmness"
  ]
  node [
    id 103
    label "kondycja"
  ]
  node [
    id 104
    label "zniszczenie"
  ]
  node [
    id 105
    label "rozsypanie_si&#281;"
  ]
  node [
    id 106
    label "os&#322;abi&#263;"
  ]
  node [
    id 107
    label "zdarcie"
  ]
  node [
    id 108
    label "zdrowotno&#347;&#263;"
  ]
  node [
    id 109
    label "zedrze&#263;"
  ]
  node [
    id 110
    label "niszczenie"
  ]
  node [
    id 111
    label "os&#322;abienie"
  ]
  node [
    id 112
    label "soundness"
  ]
  node [
    id 113
    label "os&#322;abianie"
  ]
  node [
    id 114
    label "usynowienie"
  ]
  node [
    id 115
    label "usynawianie"
  ]
  node [
    id 116
    label "dziecko"
  ]
  node [
    id 117
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 118
    label "omin&#261;&#263;"
  ]
  node [
    id 119
    label "spowodowa&#263;"
  ]
  node [
    id 120
    label "run"
  ]
  node [
    id 121
    label "przesta&#263;"
  ]
  node [
    id 122
    label "przej&#347;&#263;"
  ]
  node [
    id 123
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 124
    label "die"
  ]
  node [
    id 125
    label "overwhelm"
  ]
  node [
    id 126
    label "czas"
  ]
  node [
    id 127
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 128
    label "rok"
  ]
  node [
    id 129
    label "miech"
  ]
  node [
    id 130
    label "kalendy"
  ]
  node [
    id 131
    label "tydzie&#324;"
  ]
  node [
    id 132
    label "intensywny"
  ]
  node [
    id 133
    label "terapia"
  ]
  node [
    id 134
    label "6"
  ]
  node [
    id 135
    label "brygada"
  ]
  node [
    id 136
    label "powietrznodesantowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 10
    target 121
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 10
    target 125
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 12
    target 131
  ]
  edge [
    source 132
    target 133
  ]
  edge [
    source 134
    target 135
  ]
  edge [
    source 134
    target 136
  ]
  edge [
    source 135
    target 136
  ]
]
