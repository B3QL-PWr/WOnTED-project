graph [
  maxDegree 3
  minDegree 1
  meanDegree 1.3076923076923077
  density 0.052307692307692305
  graphCliqueNumber 3
  node [
    id 0
    label "hristo"
    origin "text"
  ]
  node [
    id 1
    label "zlatanov"
    origin "text"
  ]
  node [
    id 2
    label "Hristo"
  ]
  node [
    id 3
    label "Zlatanov"
  ]
  node [
    id 4
    label "Copra"
  ]
  node [
    id 5
    label "Berni"
  ]
  node [
    id 6
    label "Piacenza"
  ]
  node [
    id 7
    label "seria"
  ]
  node [
    id 8
    label "albo"
  ]
  node [
    id 9
    label "Milan"
  ]
  node [
    id 10
    label "volley"
  ]
  node [
    id 11
    label "Edilcuoghi"
  ]
  node [
    id 12
    label "Ravenna"
  ]
  node [
    id 13
    label "Piaggio"
  ]
  node [
    id 14
    label "Roma"
  ]
  node [
    id 15
    label "Iveco"
  ]
  node [
    id 16
    label "Palermo"
  ]
  node [
    id 17
    label "Asystel"
  ]
  node [
    id 18
    label "Mediolan"
  ]
  node [
    id 19
    label "liga"
  ]
  node [
    id 20
    label "mistrz"
  ]
  node [
    id 21
    label "&#347;wiatowy"
  ]
  node [
    id 22
    label "puchar"
  ]
  node [
    id 23
    label "topi&#263;"
  ]
  node [
    id 24
    label "Teams"
  ]
  node [
    id 25
    label "CEV"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 21
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 24
  ]
  edge [
    source 22
    target 25
  ]
  edge [
    source 23
    target 24
  ]
]
