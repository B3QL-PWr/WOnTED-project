graph [
  maxDegree 35
  minDegree 1
  meanDegree 1.9666666666666666
  density 0.03333333333333333
  graphCliqueNumber 2
  node [
    id 0
    label "ameryka&#324;ski"
    origin "text"
  ]
  node [
    id 1
    label "sie&#263;"
    origin "text"
  ]
  node [
    id 2
    label "fast"
    origin "text"
  ]
  node [
    id 3
    label "food&#243;w"
    origin "text"
  ]
  node [
    id 4
    label "carl's"
    origin "text"
  ]
  node [
    id 5
    label "junior"
    origin "text"
  ]
  node [
    id 6
    label "nowoczesny"
  ]
  node [
    id 7
    label "p&#243;&#322;nocnoameryka&#324;ski"
  ]
  node [
    id 8
    label "boston"
  ]
  node [
    id 9
    label "po_ameryka&#324;sku"
  ]
  node [
    id 10
    label "cake-walk"
  ]
  node [
    id 11
    label "charakterystyczny"
  ]
  node [
    id 12
    label "placek_ameryka&#324;ski"
  ]
  node [
    id 13
    label "fajny"
  ]
  node [
    id 14
    label "j&#281;zyk_angielski"
  ]
  node [
    id 15
    label "Princeton"
  ]
  node [
    id 16
    label "pepperoni"
  ]
  node [
    id 17
    label "sa&#322;atka_Snickers"
  ]
  node [
    id 18
    label "zachodni"
  ]
  node [
    id 19
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 20
    label "anglosaski"
  ]
  node [
    id 21
    label "typowy"
  ]
  node [
    id 22
    label "hipertekst"
  ]
  node [
    id 23
    label "gauze"
  ]
  node [
    id 24
    label "nitka"
  ]
  node [
    id 25
    label "mesh"
  ]
  node [
    id 26
    label "e-hazard"
  ]
  node [
    id 27
    label "netbook"
  ]
  node [
    id 28
    label "cyberprzestrze&#324;"
  ]
  node [
    id 29
    label "biznes_elektroniczny"
  ]
  node [
    id 30
    label "snu&#263;"
  ]
  node [
    id 31
    label "organization"
  ]
  node [
    id 32
    label "zasadzka"
  ]
  node [
    id 33
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 34
    label "web"
  ]
  node [
    id 35
    label "provider"
  ]
  node [
    id 36
    label "struktura"
  ]
  node [
    id 37
    label "us&#322;uga_internetowa"
  ]
  node [
    id 38
    label "punkt_dost&#281;pu"
  ]
  node [
    id 39
    label "organizacja"
  ]
  node [
    id 40
    label "mem"
  ]
  node [
    id 41
    label "vane"
  ]
  node [
    id 42
    label "podcast"
  ]
  node [
    id 43
    label "grooming"
  ]
  node [
    id 44
    label "kszta&#322;t"
  ]
  node [
    id 45
    label "strona"
  ]
  node [
    id 46
    label "obiekt"
  ]
  node [
    id 47
    label "wysnu&#263;"
  ]
  node [
    id 48
    label "gra_sieciowa"
  ]
  node [
    id 49
    label "instalacja"
  ]
  node [
    id 50
    label "sie&#263;_komputerowa"
  ]
  node [
    id 51
    label "net"
  ]
  node [
    id 52
    label "plecionka"
  ]
  node [
    id 53
    label "media"
  ]
  node [
    id 54
    label "rozmieszczenie"
  ]
  node [
    id 55
    label "cz&#322;owiek"
  ]
  node [
    id 56
    label "potomek"
  ]
  node [
    id 57
    label "zawodnik"
  ]
  node [
    id 58
    label "m&#322;odzieniec"
  ]
  node [
    id 59
    label "Carls"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
]
