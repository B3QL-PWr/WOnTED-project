graph [
  maxDegree 40
  minDegree 1
  meanDegree 2.1056603773584905
  density 0.007975986277873071
  graphCliqueNumber 4
  node [
    id 0
    label "podstawa"
    origin "text"
  ]
  node [
    id 1
    label "art"
    origin "text"
  ]
  node [
    id 2
    label "usta"
    origin "text"
  ]
  node [
    id 3
    label "pkt"
    origin "text"
  ]
  node [
    id 4
    label "ustawa"
    origin "text"
  ]
  node [
    id 5
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 6
    label "sierpie&#324;"
    origin "text"
  ]
  node [
    id 7
    label "rocznik"
    origin "text"
  ]
  node [
    id 8
    label "finanse"
    origin "text"
  ]
  node [
    id 9
    label "publiczny"
    origin "text"
  ]
  node [
    id 10
    label "dziennik"
    origin "text"
  ]
  node [
    id 11
    label "poz"
    origin "text"
  ]
  node [
    id 12
    label "ustala&#263;"
    origin "text"
  ]
  node [
    id 13
    label "si&#281;"
    origin "text"
  ]
  node [
    id 14
    label "nast&#281;powa&#263;"
    origin "text"
  ]
  node [
    id 15
    label "instytucja"
    origin "text"
  ]
  node [
    id 16
    label "gospodarka"
    origin "text"
  ]
  node [
    id 17
    label "bud&#380;etowy"
    origin "text"
  ]
  node [
    id 18
    label "pod"
    origin "text"
  ]
  node [
    id 19
    label "nazwa"
    origin "text"
  ]
  node [
    id 20
    label "inwestycja"
    origin "text"
  ]
  node [
    id 21
    label "organizacja"
    origin "text"
  ]
  node [
    id 22
    label "traktat"
    origin "text"
  ]
  node [
    id 23
    label "p&#243;&#322;nocnoatlantyckiego"
    origin "text"
  ]
  node [
    id 24
    label "daleko"
    origin "text"
  ]
  node [
    id 25
    label "pos&#322;ugiwa&#263;"
    origin "text"
  ]
  node [
    id 26
    label "skr&#243;ci&#263;"
    origin "text"
  ]
  node [
    id 27
    label "podstawowy"
  ]
  node [
    id 28
    label "strategia"
  ]
  node [
    id 29
    label "pot&#281;ga"
  ]
  node [
    id 30
    label "zasadzenie"
  ]
  node [
    id 31
    label "za&#322;o&#380;enie"
  ]
  node [
    id 32
    label "&#347;ciana"
  ]
  node [
    id 33
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 34
    label "przedmiot"
  ]
  node [
    id 35
    label "documentation"
  ]
  node [
    id 36
    label "dzieci&#281;ctwo"
  ]
  node [
    id 37
    label "pomys&#322;"
  ]
  node [
    id 38
    label "bok"
  ]
  node [
    id 39
    label "d&#243;&#322;"
  ]
  node [
    id 40
    label "punkt_odniesienia"
  ]
  node [
    id 41
    label "column"
  ]
  node [
    id 42
    label "zasadzi&#263;"
  ]
  node [
    id 43
    label "background"
  ]
  node [
    id 44
    label "warga_dolna"
  ]
  node [
    id 45
    label "ssa&#263;"
  ]
  node [
    id 46
    label "zaci&#261;&#263;"
  ]
  node [
    id 47
    label "ryjek"
  ]
  node [
    id 48
    label "twarz"
  ]
  node [
    id 49
    label "dzi&#243;b"
  ]
  node [
    id 50
    label "&#322;uk_Kupidyna"
  ]
  node [
    id 51
    label "ssanie"
  ]
  node [
    id 52
    label "zaci&#281;cie"
  ]
  node [
    id 53
    label "jadaczka"
  ]
  node [
    id 54
    label "zacinanie"
  ]
  node [
    id 55
    label "organ"
  ]
  node [
    id 56
    label "jama_ustna"
  ]
  node [
    id 57
    label "otw&#243;r_g&#281;bowy"
  ]
  node [
    id 58
    label "warga_g&#243;rna"
  ]
  node [
    id 59
    label "zacina&#263;"
  ]
  node [
    id 60
    label "Karta_Nauczyciela"
  ]
  node [
    id 61
    label "marc&#243;wka"
  ]
  node [
    id 62
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 63
    label "akt"
  ]
  node [
    id 64
    label "przej&#347;&#263;"
  ]
  node [
    id 65
    label "charter"
  ]
  node [
    id 66
    label "przej&#347;cie"
  ]
  node [
    id 67
    label "s&#322;o&#324;ce"
  ]
  node [
    id 68
    label "czynienie_si&#281;"
  ]
  node [
    id 69
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 70
    label "czas"
  ]
  node [
    id 71
    label "long_time"
  ]
  node [
    id 72
    label "przedpo&#322;udnie"
  ]
  node [
    id 73
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 74
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 75
    label "tydzie&#324;"
  ]
  node [
    id 76
    label "godzina"
  ]
  node [
    id 77
    label "t&#322;usty_czwartek"
  ]
  node [
    id 78
    label "wsta&#263;"
  ]
  node [
    id 79
    label "day"
  ]
  node [
    id 80
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 81
    label "przedwiecz&#243;r"
  ]
  node [
    id 82
    label "Sylwester"
  ]
  node [
    id 83
    label "po&#322;udnie"
  ]
  node [
    id 84
    label "wzej&#347;cie"
  ]
  node [
    id 85
    label "podwiecz&#243;r"
  ]
  node [
    id 86
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 87
    label "rano"
  ]
  node [
    id 88
    label "termin"
  ]
  node [
    id 89
    label "ranek"
  ]
  node [
    id 90
    label "doba"
  ]
  node [
    id 91
    label "wiecz&#243;r"
  ]
  node [
    id 92
    label "walentynki"
  ]
  node [
    id 93
    label "popo&#322;udnie"
  ]
  node [
    id 94
    label "noc"
  ]
  node [
    id 95
    label "wstanie"
  ]
  node [
    id 96
    label "Wniebowzi&#281;cie_Naj&#347;wi&#281;tszej_Maryi_Panny"
  ]
  node [
    id 97
    label "miesi&#261;c"
  ]
  node [
    id 98
    label "Sierpie&#324;"
  ]
  node [
    id 99
    label "formacja"
  ]
  node [
    id 100
    label "kronika"
  ]
  node [
    id 101
    label "czasopismo"
  ]
  node [
    id 102
    label "yearbook"
  ]
  node [
    id 103
    label "uruchomienie"
  ]
  node [
    id 104
    label "nap&#322;yn&#261;&#263;"
  ]
  node [
    id 105
    label "nauka_ekonomiczna"
  ]
  node [
    id 106
    label "absolutorium"
  ]
  node [
    id 107
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 108
    label "podupada&#263;"
  ]
  node [
    id 109
    label "nap&#322;yni&#281;cie"
  ]
  node [
    id 110
    label "supernadz&#243;r"
  ]
  node [
    id 111
    label "nap&#322;ywanie"
  ]
  node [
    id 112
    label "podupadanie"
  ]
  node [
    id 113
    label "kwestor"
  ]
  node [
    id 114
    label "uruchamia&#263;"
  ]
  node [
    id 115
    label "mienie"
  ]
  node [
    id 116
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 117
    label "uruchamianie"
  ]
  node [
    id 118
    label "czynnik_produkcji"
  ]
  node [
    id 119
    label "jawny"
  ]
  node [
    id 120
    label "upublicznienie"
  ]
  node [
    id 121
    label "upublicznianie"
  ]
  node [
    id 122
    label "publicznie"
  ]
  node [
    id 123
    label "spis"
  ]
  node [
    id 124
    label "sheet"
  ]
  node [
    id 125
    label "gazeta"
  ]
  node [
    id 126
    label "diariusz"
  ]
  node [
    id 127
    label "pami&#281;tnik"
  ]
  node [
    id 128
    label "journal"
  ]
  node [
    id 129
    label "ksi&#281;ga"
  ]
  node [
    id 130
    label "program_informacyjny"
  ]
  node [
    id 131
    label "umacnia&#263;"
  ]
  node [
    id 132
    label "zmienia&#263;"
  ]
  node [
    id 133
    label "arrange"
  ]
  node [
    id 134
    label "robi&#263;"
  ]
  node [
    id 135
    label "unwrap"
  ]
  node [
    id 136
    label "decydowa&#263;"
  ]
  node [
    id 137
    label "peddle"
  ]
  node [
    id 138
    label "powodowa&#263;"
  ]
  node [
    id 139
    label "mie&#263;_miejsce"
  ]
  node [
    id 140
    label "chance"
  ]
  node [
    id 141
    label "zdarza&#263;_si&#281;"
  ]
  node [
    id 142
    label "alternate"
  ]
  node [
    id 143
    label "naciska&#263;"
  ]
  node [
    id 144
    label "atakowa&#263;"
  ]
  node [
    id 145
    label "afiliowa&#263;"
  ]
  node [
    id 146
    label "osoba_prawna"
  ]
  node [
    id 147
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 148
    label "urz&#261;d"
  ]
  node [
    id 149
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 150
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 151
    label "establishment"
  ]
  node [
    id 152
    label "standard"
  ]
  node [
    id 153
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 154
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 155
    label "zamykanie"
  ]
  node [
    id 156
    label "zamyka&#263;"
  ]
  node [
    id 157
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 158
    label "poj&#281;cie"
  ]
  node [
    id 159
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 160
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 161
    label "Fundusze_Unijne"
  ]
  node [
    id 162
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 163
    label "biuro"
  ]
  node [
    id 164
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 165
    label "rynek"
  ]
  node [
    id 166
    label "pole"
  ]
  node [
    id 167
    label "szkolnictwo"
  ]
  node [
    id 168
    label "przemys&#322;"
  ]
  node [
    id 169
    label "gospodarka_wodna"
  ]
  node [
    id 170
    label "fabryka"
  ]
  node [
    id 171
    label "rolnictwo"
  ]
  node [
    id 172
    label "gospodarka_le&#347;na"
  ]
  node [
    id 173
    label "gospodarowa&#263;"
  ]
  node [
    id 174
    label "sektor_prywatny"
  ]
  node [
    id 175
    label "obronno&#347;&#263;"
  ]
  node [
    id 176
    label "obora"
  ]
  node [
    id 177
    label "mieszkalnictwo"
  ]
  node [
    id 178
    label "sektor_publiczny"
  ]
  node [
    id 179
    label "czerwona_strefa"
  ]
  node [
    id 180
    label "struktura"
  ]
  node [
    id 181
    label "stodo&#322;a"
  ]
  node [
    id 182
    label "rybo&#322;&#243;wstwo"
  ]
  node [
    id 183
    label "produkowanie"
  ]
  node [
    id 184
    label "gospodarowanie"
  ]
  node [
    id 185
    label "agregat_ekonomiczny"
  ]
  node [
    id 186
    label "sch&#322;adza&#263;"
  ]
  node [
    id 187
    label "spichlerz"
  ]
  node [
    id 188
    label "inwentarz"
  ]
  node [
    id 189
    label "transport"
  ]
  node [
    id 190
    label "sch&#322;odzenie"
  ]
  node [
    id 191
    label "s&#322;u&#380;ba_zdrowia"
  ]
  node [
    id 192
    label "miejsce_pracy"
  ]
  node [
    id 193
    label "wytw&#243;rnia"
  ]
  node [
    id 194
    label "farmaceutyka"
  ]
  node [
    id 195
    label "bud&#380;et&#243;wka"
  ]
  node [
    id 196
    label "skarbowo&#347;&#263;"
  ]
  node [
    id 197
    label "administracja"
  ]
  node [
    id 198
    label "sch&#322;adzanie"
  ]
  node [
    id 199
    label "bankowo&#347;&#263;"
  ]
  node [
    id 200
    label "zasada"
  ]
  node [
    id 201
    label "sch&#322;odzi&#263;"
  ]
  node [
    id 202
    label "regulacja_cen"
  ]
  node [
    id 203
    label "etatowy"
  ]
  node [
    id 204
    label "bud&#380;etowo"
  ]
  node [
    id 205
    label "budgetary"
  ]
  node [
    id 206
    label "term"
  ]
  node [
    id 207
    label "wezwanie"
  ]
  node [
    id 208
    label "leksem"
  ]
  node [
    id 209
    label "patron"
  ]
  node [
    id 210
    label "inwestycje"
  ]
  node [
    id 211
    label "wydatki_maj&#261;tkowe"
  ]
  node [
    id 212
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 213
    label "wk&#322;ad"
  ]
  node [
    id 214
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 215
    label "kapita&#322;"
  ]
  node [
    id 216
    label "przedsi&#281;wzi&#281;cie"
  ]
  node [
    id 217
    label "inwestowanie"
  ]
  node [
    id 218
    label "bud&#380;et_domowy"
  ]
  node [
    id 219
    label "sentyment_inwestycyjny"
  ]
  node [
    id 220
    label "rezultat"
  ]
  node [
    id 221
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 222
    label "endecki"
  ]
  node [
    id 223
    label "komitet_koordynacyjny"
  ]
  node [
    id 224
    label "przybud&#243;wka"
  ]
  node [
    id 225
    label "ZOMO"
  ]
  node [
    id 226
    label "podmiot"
  ]
  node [
    id 227
    label "boj&#243;wka"
  ]
  node [
    id 228
    label "zesp&#243;&#322;"
  ]
  node [
    id 229
    label "organization"
  ]
  node [
    id 230
    label "TOPR"
  ]
  node [
    id 231
    label "jednostka_organizacyjna"
  ]
  node [
    id 232
    label "przedstawicielstwo"
  ]
  node [
    id 233
    label "Cepelia"
  ]
  node [
    id 234
    label "GOPR"
  ]
  node [
    id 235
    label "ZMP"
  ]
  node [
    id 236
    label "ZBoWiD"
  ]
  node [
    id 237
    label "od&#322;am"
  ]
  node [
    id 238
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 239
    label "centrala"
  ]
  node [
    id 240
    label "opracowanie"
  ]
  node [
    id 241
    label "traktat_wersalski"
  ]
  node [
    id 242
    label "ONZ"
  ]
  node [
    id 243
    label "zawrze&#263;"
  ]
  node [
    id 244
    label "treaty"
  ]
  node [
    id 245
    label "obja&#347;nienie"
  ]
  node [
    id 246
    label "umowa"
  ]
  node [
    id 247
    label "rozumowanie"
  ]
  node [
    id 248
    label "NATO"
  ]
  node [
    id 249
    label "zawarcie"
  ]
  node [
    id 250
    label "tekst"
  ]
  node [
    id 251
    label "cytat"
  ]
  node [
    id 252
    label "dawno"
  ]
  node [
    id 253
    label "nisko"
  ]
  node [
    id 254
    label "nieobecnie"
  ]
  node [
    id 255
    label "daleki"
  ]
  node [
    id 256
    label "het"
  ]
  node [
    id 257
    label "wysoko"
  ]
  node [
    id 258
    label "du&#380;o"
  ]
  node [
    id 259
    label "znacznie"
  ]
  node [
    id 260
    label "g&#322;&#281;boko"
  ]
  node [
    id 261
    label "use"
  ]
  node [
    id 262
    label "us&#322;ugiwa&#263;"
  ]
  node [
    id 263
    label "condense"
  ]
  node [
    id 264
    label "zmniejszy&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 11
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 119
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 10
    target 125
  ]
  edge [
    source 10
    target 126
  ]
  edge [
    source 10
    target 127
  ]
  edge [
    source 10
    target 128
  ]
  edge [
    source 10
    target 129
  ]
  edge [
    source 10
    target 130
  ]
  edge [
    source 11
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 131
  ]
  edge [
    source 12
    target 132
  ]
  edge [
    source 12
    target 133
  ]
  edge [
    source 12
    target 134
  ]
  edge [
    source 12
    target 135
  ]
  edge [
    source 12
    target 136
  ]
  edge [
    source 12
    target 137
  ]
  edge [
    source 12
    target 138
  ]
  edge [
    source 12
    target 19
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 13
    target 25
  ]
  edge [
    source 13
    target 19
  ]
  edge [
    source 14
    target 139
  ]
  edge [
    source 14
    target 140
  ]
  edge [
    source 14
    target 141
  ]
  edge [
    source 14
    target 142
  ]
  edge [
    source 14
    target 143
  ]
  edge [
    source 14
    target 144
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 145
  ]
  edge [
    source 15
    target 146
  ]
  edge [
    source 15
    target 147
  ]
  edge [
    source 15
    target 148
  ]
  edge [
    source 15
    target 149
  ]
  edge [
    source 15
    target 150
  ]
  edge [
    source 15
    target 151
  ]
  edge [
    source 15
    target 152
  ]
  edge [
    source 15
    target 21
  ]
  edge [
    source 15
    target 153
  ]
  edge [
    source 15
    target 154
  ]
  edge [
    source 15
    target 155
  ]
  edge [
    source 15
    target 156
  ]
  edge [
    source 15
    target 157
  ]
  edge [
    source 15
    target 158
  ]
  edge [
    source 15
    target 159
  ]
  edge [
    source 15
    target 160
  ]
  edge [
    source 15
    target 161
  ]
  edge [
    source 15
    target 162
  ]
  edge [
    source 15
    target 163
  ]
  edge [
    source 15
    target 164
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 165
  ]
  edge [
    source 16
    target 166
  ]
  edge [
    source 16
    target 167
  ]
  edge [
    source 16
    target 168
  ]
  edge [
    source 16
    target 169
  ]
  edge [
    source 16
    target 170
  ]
  edge [
    source 16
    target 171
  ]
  edge [
    source 16
    target 172
  ]
  edge [
    source 16
    target 173
  ]
  edge [
    source 16
    target 174
  ]
  edge [
    source 16
    target 175
  ]
  edge [
    source 16
    target 176
  ]
  edge [
    source 16
    target 177
  ]
  edge [
    source 16
    target 178
  ]
  edge [
    source 16
    target 179
  ]
  edge [
    source 16
    target 180
  ]
  edge [
    source 16
    target 181
  ]
  edge [
    source 16
    target 182
  ]
  edge [
    source 16
    target 183
  ]
  edge [
    source 16
    target 184
  ]
  edge [
    source 16
    target 185
  ]
  edge [
    source 16
    target 186
  ]
  edge [
    source 16
    target 187
  ]
  edge [
    source 16
    target 188
  ]
  edge [
    source 16
    target 189
  ]
  edge [
    source 16
    target 190
  ]
  edge [
    source 16
    target 191
  ]
  edge [
    source 16
    target 192
  ]
  edge [
    source 16
    target 193
  ]
  edge [
    source 16
    target 194
  ]
  edge [
    source 16
    target 195
  ]
  edge [
    source 16
    target 196
  ]
  edge [
    source 16
    target 197
  ]
  edge [
    source 16
    target 198
  ]
  edge [
    source 16
    target 199
  ]
  edge [
    source 16
    target 200
  ]
  edge [
    source 16
    target 201
  ]
  edge [
    source 16
    target 202
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 203
  ]
  edge [
    source 17
    target 204
  ]
  edge [
    source 17
    target 205
  ]
  edge [
    source 17
    target 20
  ]
  edge [
    source 17
    target 21
  ]
  edge [
    source 17
    target 22
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 26
  ]
  edge [
    source 19
    target 206
  ]
  edge [
    source 19
    target 207
  ]
  edge [
    source 19
    target 208
  ]
  edge [
    source 19
    target 209
  ]
  edge [
    source 19
    target 24
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 210
  ]
  edge [
    source 20
    target 211
  ]
  edge [
    source 20
    target 212
  ]
  edge [
    source 20
    target 213
  ]
  edge [
    source 20
    target 214
  ]
  edge [
    source 20
    target 215
  ]
  edge [
    source 20
    target 216
  ]
  edge [
    source 20
    target 217
  ]
  edge [
    source 20
    target 218
  ]
  edge [
    source 20
    target 219
  ]
  edge [
    source 20
    target 220
  ]
  edge [
    source 20
    target 22
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 221
  ]
  edge [
    source 21
    target 222
  ]
  edge [
    source 21
    target 223
  ]
  edge [
    source 21
    target 224
  ]
  edge [
    source 21
    target 225
  ]
  edge [
    source 21
    target 226
  ]
  edge [
    source 21
    target 227
  ]
  edge [
    source 21
    target 228
  ]
  edge [
    source 21
    target 229
  ]
  edge [
    source 21
    target 230
  ]
  edge [
    source 21
    target 231
  ]
  edge [
    source 21
    target 232
  ]
  edge [
    source 21
    target 233
  ]
  edge [
    source 21
    target 234
  ]
  edge [
    source 21
    target 235
  ]
  edge [
    source 21
    target 236
  ]
  edge [
    source 21
    target 180
  ]
  edge [
    source 21
    target 237
  ]
  edge [
    source 21
    target 238
  ]
  edge [
    source 21
    target 239
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 240
  ]
  edge [
    source 22
    target 241
  ]
  edge [
    source 22
    target 242
  ]
  edge [
    source 22
    target 243
  ]
  edge [
    source 22
    target 244
  ]
  edge [
    source 22
    target 245
  ]
  edge [
    source 22
    target 246
  ]
  edge [
    source 22
    target 247
  ]
  edge [
    source 22
    target 248
  ]
  edge [
    source 22
    target 249
  ]
  edge [
    source 22
    target 250
  ]
  edge [
    source 22
    target 251
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 252
  ]
  edge [
    source 24
    target 253
  ]
  edge [
    source 24
    target 254
  ]
  edge [
    source 24
    target 255
  ]
  edge [
    source 24
    target 256
  ]
  edge [
    source 24
    target 257
  ]
  edge [
    source 24
    target 258
  ]
  edge [
    source 24
    target 259
  ]
  edge [
    source 24
    target 260
  ]
  edge [
    source 25
    target 261
  ]
  edge [
    source 25
    target 262
  ]
  edge [
    source 26
    target 263
  ]
  edge [
    source 26
    target 264
  ]
]
