graph [
  maxDegree 8
  minDegree 1
  meanDegree 1.8571428571428572
  density 0.14285714285714285
  graphCliqueNumber 2
  node [
    id 0
    label "protest"
    origin "text"
  ]
  node [
    id 1
    label "&#380;&#243;&#322;ty"
    origin "text"
  ]
  node [
    id 2
    label "kamizelka"
    origin "text"
  ]
  node [
    id 3
    label "reakcja"
  ]
  node [
    id 4
    label "protestacja"
  ]
  node [
    id 5
    label "czerwona_kartka"
  ]
  node [
    id 6
    label "jasny"
  ]
  node [
    id 7
    label "typ_mongoloidalny"
  ]
  node [
    id 8
    label "kolorowy"
  ]
  node [
    id 9
    label "&#380;&#243;&#322;cenie"
  ]
  node [
    id 10
    label "ciep&#322;y"
  ]
  node [
    id 11
    label "&#380;&#243;&#322;to"
  ]
  node [
    id 12
    label "westa"
  ]
  node [
    id 13
    label "g&#243;ra"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
]
