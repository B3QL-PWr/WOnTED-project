graph [
  maxDegree 21
  minDegree 1
  meanDegree 1.96875
  density 0.03125
  graphCliqueNumber 2
  node [
    id 0
    label "&#347;wie&#380;y"
    origin "text"
  ]
  node [
    id 1
    label "informacja"
    origin "text"
  ]
  node [
    id 2
    label "julia"
    origin "text"
  ]
  node [
    id 3
    label "reda"
    origin "text"
  ]
  node [
    id 4
    label "przedostatni"
    origin "text"
  ]
  node [
    id 5
    label "etap"
    origin "text"
  ]
  node [
    id 6
    label "trialogu"
    origin "text"
  ]
  node [
    id 7
    label "sprawa"
    origin "text"
  ]
  node [
    id 8
    label "orze&#378;wiaj&#261;co"
  ]
  node [
    id 9
    label "jasny"
  ]
  node [
    id 10
    label "&#347;wie&#380;o"
  ]
  node [
    id 11
    label "orze&#378;wienie"
  ]
  node [
    id 12
    label "o&#380;ywczy"
  ]
  node [
    id 13
    label "soczysty"
  ]
  node [
    id 14
    label "inny"
  ]
  node [
    id 15
    label "nowotny"
  ]
  node [
    id 16
    label "przyjemny"
  ]
  node [
    id 17
    label "zdrowy"
  ]
  node [
    id 18
    label "&#380;ywy"
  ]
  node [
    id 19
    label "czysty"
  ]
  node [
    id 20
    label "orze&#378;wianie"
  ]
  node [
    id 21
    label "oryginalnie"
  ]
  node [
    id 22
    label "m&#322;ody"
  ]
  node [
    id 23
    label "energiczny"
  ]
  node [
    id 24
    label "nowy"
  ]
  node [
    id 25
    label "dobry"
  ]
  node [
    id 26
    label "rze&#347;ki"
  ]
  node [
    id 27
    label "surowy"
  ]
  node [
    id 28
    label "doj&#347;cie"
  ]
  node [
    id 29
    label "doj&#347;&#263;"
  ]
  node [
    id 30
    label "powzi&#261;&#263;"
  ]
  node [
    id 31
    label "wiedza"
  ]
  node [
    id 32
    label "sygna&#322;"
  ]
  node [
    id 33
    label "obiegni&#281;cie"
  ]
  node [
    id 34
    label "obieganie"
  ]
  node [
    id 35
    label "obiec"
  ]
  node [
    id 36
    label "dane"
  ]
  node [
    id 37
    label "obiega&#263;"
  ]
  node [
    id 38
    label "punkt"
  ]
  node [
    id 39
    label "publikacja"
  ]
  node [
    id 40
    label "powzi&#281;cie"
  ]
  node [
    id 41
    label "akwatorium"
  ]
  node [
    id 42
    label "morze"
  ]
  node [
    id 43
    label "falochron"
  ]
  node [
    id 44
    label "kolejny"
  ]
  node [
    id 45
    label "przedostatnio"
  ]
  node [
    id 46
    label "poprzedni"
  ]
  node [
    id 47
    label "pozosta&#322;y"
  ]
  node [
    id 48
    label "wcze&#347;niejszy"
  ]
  node [
    id 49
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 50
    label "miejsce"
  ]
  node [
    id 51
    label "odcinek"
  ]
  node [
    id 52
    label "czas"
  ]
  node [
    id 53
    label "temat"
  ]
  node [
    id 54
    label "kognicja"
  ]
  node [
    id 55
    label "idea"
  ]
  node [
    id 56
    label "szczeg&#243;&#322;"
  ]
  node [
    id 57
    label "rzecz"
  ]
  node [
    id 58
    label "wydarzenie"
  ]
  node [
    id 59
    label "przes&#322;anka"
  ]
  node [
    id 60
    label "rozprawa"
  ]
  node [
    id 61
    label "object"
  ]
  node [
    id 62
    label "proposition"
  ]
  node [
    id 63
    label "Julia"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
]
