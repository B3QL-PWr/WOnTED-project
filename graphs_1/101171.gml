graph [
  maxDegree 88
  minDegree 1
  meanDegree 2.0714285714285716
  density 0.01866151866151866
  graphCliqueNumber 6
  node [
    id 0
    label "osiedle"
    origin "text"
  ]
  node [
    id 1
    label "zielone"
    origin "text"
  ]
  node [
    id 2
    label "Powsin"
  ]
  node [
    id 3
    label "G&#243;rce"
  ]
  node [
    id 4
    label "Kar&#322;owice"
  ]
  node [
    id 5
    label "Rakowiec"
  ]
  node [
    id 6
    label "Dojlidy"
  ]
  node [
    id 7
    label "Horodyszcze"
  ]
  node [
    id 8
    label "Kujbyszewe"
  ]
  node [
    id 9
    label "Kabaty"
  ]
  node [
    id 10
    label "jednostka_osadnicza"
  ]
  node [
    id 11
    label "Ujazd&#243;w"
  ]
  node [
    id 12
    label "Kaw&#281;czyn"
  ]
  node [
    id 13
    label "Siersza"
  ]
  node [
    id 14
    label "Groch&#243;w"
  ]
  node [
    id 15
    label "Paw&#322;owice"
  ]
  node [
    id 16
    label "Bielice"
  ]
  node [
    id 17
    label "siedziba"
  ]
  node [
    id 18
    label "Tarchomin"
  ]
  node [
    id 19
    label "Br&#243;dno"
  ]
  node [
    id 20
    label "Jelcz"
  ]
  node [
    id 21
    label "Mariensztat"
  ]
  node [
    id 22
    label "Falenica"
  ]
  node [
    id 23
    label "Izborsk"
  ]
  node [
    id 24
    label "Wi&#347;niewo"
  ]
  node [
    id 25
    label "Marymont"
  ]
  node [
    id 26
    label "Solec"
  ]
  node [
    id 27
    label "Zakrz&#243;w"
  ]
  node [
    id 28
    label "Wi&#347;niowiec"
  ]
  node [
    id 29
    label "Natolin"
  ]
  node [
    id 30
    label "grupa"
  ]
  node [
    id 31
    label "Anin"
  ]
  node [
    id 32
    label "Grabiszyn"
  ]
  node [
    id 33
    label "Orunia"
  ]
  node [
    id 34
    label "Gronik"
  ]
  node [
    id 35
    label "Boryszew"
  ]
  node [
    id 36
    label "Bogucice"
  ]
  node [
    id 37
    label "&#379;era&#324;"
  ]
  node [
    id 38
    label "zesp&#243;&#322;"
  ]
  node [
    id 39
    label "Jasienica"
  ]
  node [
    id 40
    label "Laskowice_O&#322;awskie"
  ]
  node [
    id 41
    label "Salwator"
  ]
  node [
    id 42
    label "Zerze&#324;"
  ]
  node [
    id 43
    label "M&#322;ociny"
  ]
  node [
    id 44
    label "Branice"
  ]
  node [
    id 45
    label "Chojny"
  ]
  node [
    id 46
    label "Wad&#243;w"
  ]
  node [
    id 47
    label "jednostka_administracyjna"
  ]
  node [
    id 48
    label "Miedzeszyn"
  ]
  node [
    id 49
    label "Ok&#281;cie"
  ]
  node [
    id 50
    label "Lewin&#243;w"
  ]
  node [
    id 51
    label "Broch&#243;w"
  ]
  node [
    id 52
    label "Marysin"
  ]
  node [
    id 53
    label "Szack"
  ]
  node [
    id 54
    label "Wielopole"
  ]
  node [
    id 55
    label "Opor&#243;w"
  ]
  node [
    id 56
    label "Osobowice"
  ]
  node [
    id 57
    label "Lubiesz&#243;w"
  ]
  node [
    id 58
    label "&#379;erniki"
  ]
  node [
    id 59
    label "Powi&#347;le"
  ]
  node [
    id 60
    label "osadnictwo"
  ]
  node [
    id 61
    label "Wojn&#243;w"
  ]
  node [
    id 62
    label "Latycz&#243;w"
  ]
  node [
    id 63
    label "Kortowo"
  ]
  node [
    id 64
    label "Rej&#243;w"
  ]
  node [
    id 65
    label "Arsk"
  ]
  node [
    id 66
    label "&#321;agiewniki"
  ]
  node [
    id 67
    label "Azory"
  ]
  node [
    id 68
    label "Imielin"
  ]
  node [
    id 69
    label "Rataje"
  ]
  node [
    id 70
    label "Nadodrze"
  ]
  node [
    id 71
    label "Szczytniki"
  ]
  node [
    id 72
    label "Gr&#281;ba&#322;&#243;w"
  ]
  node [
    id 73
    label "dzielnica"
  ]
  node [
    id 74
    label "S&#281;polno"
  ]
  node [
    id 75
    label "G&#243;rczyn"
  ]
  node [
    id 76
    label "Szcz&#281;&#347;liwice"
  ]
  node [
    id 77
    label "Zalesie"
  ]
  node [
    id 78
    label "Ochock"
  ]
  node [
    id 79
    label "Gutkowo"
  ]
  node [
    id 80
    label "G&#322;uszyna"
  ]
  node [
    id 81
    label "Le&#347;nica"
  ]
  node [
    id 82
    label "Micha&#322;owo"
  ]
  node [
    id 83
    label "Jelonki"
  ]
  node [
    id 84
    label "Marysin_Wawerski"
  ]
  node [
    id 85
    label "Biskupin"
  ]
  node [
    id 86
    label "Goc&#322;aw"
  ]
  node [
    id 87
    label "Wawrzyszew"
  ]
  node [
    id 88
    label "warzywo"
  ]
  node [
    id 89
    label "sygnalizacja_&#347;wietlna"
  ]
  node [
    id 90
    label "jedzenie"
  ]
  node [
    id 91
    label "jarzynka"
  ]
  node [
    id 92
    label "zielona_fala"
  ]
  node [
    id 93
    label "zabawa"
  ]
  node [
    id 94
    label "towar"
  ]
  node [
    id 95
    label "zielony"
  ]
  node [
    id 96
    label "nowy"
  ]
  node [
    id 97
    label "huta"
  ]
  node [
    id 98
    label "by&#322;y"
  ]
  node [
    id 99
    label "2"
  ]
  node [
    id 100
    label "ulica"
  ]
  node [
    id 101
    label "Mo&#347;cicki"
  ]
  node [
    id 102
    label "&#379;eromski"
  ]
  node [
    id 103
    label "aleja"
  ]
  node [
    id 104
    label "r&#243;&#380;a"
  ]
  node [
    id 105
    label "wojciechowski"
  ]
  node [
    id 106
    label "plac"
  ]
  node [
    id 107
    label "centralny"
  ]
  node [
    id 108
    label "oddzia&#322;"
  ]
  node [
    id 109
    label "wyspa"
  ]
  node [
    id 110
    label "muzeum"
  ]
  node [
    id 111
    label "archeologiczny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 96
    target 97
  ]
  edge [
    source 96
    target 108
  ]
  edge [
    source 96
    target 109
  ]
  edge [
    source 96
    target 110
  ]
  edge [
    source 96
    target 111
  ]
  edge [
    source 97
    target 108
  ]
  edge [
    source 97
    target 109
  ]
  edge [
    source 97
    target 110
  ]
  edge [
    source 97
    target 111
  ]
  edge [
    source 98
    target 99
  ]
  edge [
    source 100
    target 101
  ]
  edge [
    source 100
    target 102
  ]
  edge [
    source 100
    target 105
  ]
  edge [
    source 103
    target 104
  ]
  edge [
    source 106
    target 107
  ]
  edge [
    source 108
    target 109
  ]
  edge [
    source 108
    target 110
  ]
  edge [
    source 108
    target 111
  ]
  edge [
    source 109
    target 110
  ]
  edge [
    source 109
    target 111
  ]
  edge [
    source 110
    target 111
  ]
]
