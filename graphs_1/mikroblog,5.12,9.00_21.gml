graph [
  maxDegree 51
  minDegree 1
  meanDegree 1.9615384615384615
  density 0.019044062733383122
  graphCliqueNumber 2
  node [
    id 0
    label "je&#380;eli"
    origin "text"
  ]
  node [
    id 1
    label "by&#263;by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "przekonany"
    origin "text"
  ]
  node [
    id 3
    label "podbiera&#263;"
    origin "text"
  ]
  node [
    id 4
    label "paliwo"
    origin "text"
  ]
  node [
    id 5
    label "firmowy"
    origin "text"
  ]
  node [
    id 6
    label "bus"
    origin "text"
  ]
  node [
    id 7
    label "by&#263;"
    origin "text"
  ]
  node [
    id 8
    label "szczyt"
    origin "text"
  ]
  node [
    id 9
    label "cebulactwa"
    origin "text"
  ]
  node [
    id 10
    label "zaskoczy&#263;"
    origin "text"
  ]
  node [
    id 11
    label "upewnienie_si&#281;"
  ]
  node [
    id 12
    label "ufanie"
  ]
  node [
    id 13
    label "wierzenie"
  ]
  node [
    id 14
    label "upewnianie_si&#281;"
  ]
  node [
    id 15
    label "kra&#347;&#263;"
  ]
  node [
    id 16
    label "pinch"
  ]
  node [
    id 17
    label "spalenie"
  ]
  node [
    id 18
    label "spali&#263;"
  ]
  node [
    id 19
    label "fuel"
  ]
  node [
    id 20
    label "tankowanie"
  ]
  node [
    id 21
    label "zgazowa&#263;"
  ]
  node [
    id 22
    label "pompa_wtryskowa"
  ]
  node [
    id 23
    label "tankowa&#263;"
  ]
  node [
    id 24
    label "antydetonator"
  ]
  node [
    id 25
    label "Orlen"
  ]
  node [
    id 26
    label "spalanie"
  ]
  node [
    id 27
    label "spala&#263;"
  ]
  node [
    id 28
    label "substancja"
  ]
  node [
    id 29
    label "markowy"
  ]
  node [
    id 30
    label "oryginalny"
  ]
  node [
    id 31
    label "firmowo"
  ]
  node [
    id 32
    label "autobus"
  ]
  node [
    id 33
    label "buspas"
  ]
  node [
    id 34
    label "samoch&#243;d"
  ]
  node [
    id 35
    label "znak_informacyjny"
  ]
  node [
    id 36
    label "si&#281;ga&#263;"
  ]
  node [
    id 37
    label "trwa&#263;"
  ]
  node [
    id 38
    label "obecno&#347;&#263;"
  ]
  node [
    id 39
    label "stan"
  ]
  node [
    id 40
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 41
    label "stand"
  ]
  node [
    id 42
    label "mie&#263;_miejsce"
  ]
  node [
    id 43
    label "uczestniczy&#263;"
  ]
  node [
    id 44
    label "chodzi&#263;"
  ]
  node [
    id 45
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 46
    label "equal"
  ]
  node [
    id 47
    label "Lubogoszcz"
  ]
  node [
    id 48
    label "koniec"
  ]
  node [
    id 49
    label "wierch"
  ]
  node [
    id 50
    label "czas"
  ]
  node [
    id 51
    label "&#321;omnica"
  ]
  node [
    id 52
    label "Wielki_Chocz"
  ]
  node [
    id 53
    label "Magura"
  ]
  node [
    id 54
    label "Turbacz"
  ]
  node [
    id 55
    label "Wo&#322;ek"
  ]
  node [
    id 56
    label "Walig&#243;ra"
  ]
  node [
    id 57
    label "Orlica"
  ]
  node [
    id 58
    label "korona"
  ]
  node [
    id 59
    label "bok"
  ]
  node [
    id 60
    label "Jaworzyna"
  ]
  node [
    id 61
    label "Groniczki"
  ]
  node [
    id 62
    label "Radunia"
  ]
  node [
    id 63
    label "Okr&#261;glica"
  ]
  node [
    id 64
    label "&#346;winica"
  ]
  node [
    id 65
    label "Beskid"
  ]
  node [
    id 66
    label "poziom"
  ]
  node [
    id 67
    label "wzmo&#380;enie"
  ]
  node [
    id 68
    label "Czupel"
  ]
  node [
    id 69
    label "fasada"
  ]
  node [
    id 70
    label "Rysianka"
  ]
  node [
    id 71
    label "g&#243;ra"
  ]
  node [
    id 72
    label "Jaworz"
  ]
  node [
    id 73
    label "Che&#322;miec"
  ]
  node [
    id 74
    label "Rudawiec"
  ]
  node [
    id 75
    label "zwie&#324;czenie"
  ]
  node [
    id 76
    label "Wielki_Bukowiec"
  ]
  node [
    id 77
    label "wzniesienie"
  ]
  node [
    id 78
    label "godzina_szczytu"
  ]
  node [
    id 79
    label "summit"
  ]
  node [
    id 80
    label "Wielka_Racza"
  ]
  node [
    id 81
    label "wierzcho&#322;"
  ]
  node [
    id 82
    label "&#346;nie&#380;nik"
  ]
  node [
    id 83
    label "&#347;ciana"
  ]
  node [
    id 84
    label "Barania_G&#243;ra"
  ]
  node [
    id 85
    label "Ja&#322;owiec"
  ]
  node [
    id 86
    label "Ko&#322;owr&#243;t"
  ]
  node [
    id 87
    label "Wielka_Sowa"
  ]
  node [
    id 88
    label "wierzcho&#322;ek"
  ]
  node [
    id 89
    label "Obidowa"
  ]
  node [
    id 90
    label "konferencja"
  ]
  node [
    id 91
    label "Cubryna"
  ]
  node [
    id 92
    label "Szrenica"
  ]
  node [
    id 93
    label "Czarna_G&#243;ra"
  ]
  node [
    id 94
    label "Mody&#324;"
  ]
  node [
    id 95
    label "&#346;l&#281;&#380;a"
  ]
  node [
    id 96
    label "zacz&#261;&#263;"
  ]
  node [
    id 97
    label "catch"
  ]
  node [
    id 98
    label "wpa&#347;&#263;"
  ]
  node [
    id 99
    label "zrozumie&#263;"
  ]
  node [
    id 100
    label "zdziwi&#263;"
  ]
  node [
    id 101
    label "XD"
  ]
  node [
    id 102
    label "pewnie"
  ]
  node [
    id 103
    label "dziwny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 36
  ]
  edge [
    source 7
    target 37
  ]
  edge [
    source 7
    target 38
  ]
  edge [
    source 7
    target 39
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 101
    target 102
  ]
  edge [
    source 101
    target 103
  ]
]
