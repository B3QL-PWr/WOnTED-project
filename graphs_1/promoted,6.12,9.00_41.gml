graph [
  maxDegree 27
  minDegree 1
  meanDegree 2
  density 0.02702702702702703
  graphCliqueNumber 2
  node [
    id 0
    label "tak"
    origin "text"
  ]
  node [
    id 1
    label "jakby"
    origin "text"
  ]
  node [
    id 2
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 3
    label "musie&#263;"
    origin "text"
  ]
  node [
    id 4
    label "kupi&#263;"
    origin "text"
  ]
  node [
    id 5
    label "schab"
    origin "text"
  ]
  node [
    id 6
    label "by&#263;by"
    origin "text"
  ]
  node [
    id 7
    label "wolno"
    origin "text"
  ]
  node [
    id 8
    label "je&#347;&#263;"
    origin "text"
  ]
  node [
    id 9
    label "dr&#243;b"
    origin "text"
  ]
  node [
    id 10
    label "asymilowa&#263;"
  ]
  node [
    id 11
    label "wapniak"
  ]
  node [
    id 12
    label "dwun&#243;g"
  ]
  node [
    id 13
    label "polifag"
  ]
  node [
    id 14
    label "wz&#243;r"
  ]
  node [
    id 15
    label "profanum"
  ]
  node [
    id 16
    label "hominid"
  ]
  node [
    id 17
    label "homo_sapiens"
  ]
  node [
    id 18
    label "nasada"
  ]
  node [
    id 19
    label "podw&#322;adny"
  ]
  node [
    id 20
    label "ludzko&#347;&#263;"
  ]
  node [
    id 21
    label "os&#322;abianie"
  ]
  node [
    id 22
    label "mikrokosmos"
  ]
  node [
    id 23
    label "portrecista"
  ]
  node [
    id 24
    label "duch"
  ]
  node [
    id 25
    label "g&#322;owa"
  ]
  node [
    id 26
    label "oddzia&#322;ywanie"
  ]
  node [
    id 27
    label "asymilowanie"
  ]
  node [
    id 28
    label "osoba"
  ]
  node [
    id 29
    label "os&#322;abia&#263;"
  ]
  node [
    id 30
    label "figura"
  ]
  node [
    id 31
    label "Adam"
  ]
  node [
    id 32
    label "senior"
  ]
  node [
    id 33
    label "antropochoria"
  ]
  node [
    id 34
    label "posta&#263;"
  ]
  node [
    id 35
    label "need"
  ]
  node [
    id 36
    label "pragn&#261;&#263;"
  ]
  node [
    id 37
    label "get"
  ]
  node [
    id 38
    label "wzi&#261;&#263;"
  ]
  node [
    id 39
    label "catch"
  ]
  node [
    id 40
    label "przyj&#261;&#263;"
  ]
  node [
    id 41
    label "beget"
  ]
  node [
    id 42
    label "pozyska&#263;"
  ]
  node [
    id 43
    label "ustawi&#263;"
  ]
  node [
    id 44
    label "uzna&#263;"
  ]
  node [
    id 45
    label "zagra&#263;"
  ]
  node [
    id 46
    label "uwierzy&#263;"
  ]
  node [
    id 47
    label "wieprzowina"
  ]
  node [
    id 48
    label "mi&#281;so"
  ]
  node [
    id 49
    label "tusza"
  ]
  node [
    id 50
    label "mi&#281;siwo"
  ]
  node [
    id 51
    label "wolny"
  ]
  node [
    id 52
    label "lu&#378;no"
  ]
  node [
    id 53
    label "wolniej"
  ]
  node [
    id 54
    label "thinly"
  ]
  node [
    id 55
    label "swobodny"
  ]
  node [
    id 56
    label "wolnie"
  ]
  node [
    id 57
    label "niespiesznie"
  ]
  node [
    id 58
    label "lu&#378;ny"
  ]
  node [
    id 59
    label "free"
  ]
  node [
    id 60
    label "write_out"
  ]
  node [
    id 61
    label "papusia&#263;"
  ]
  node [
    id 62
    label "wpieprza&#263;"
  ]
  node [
    id 63
    label "k&#261;sa&#263;"
  ]
  node [
    id 64
    label "spo&#380;ywa&#263;"
  ]
  node [
    id 65
    label "k&#322;u&#263;"
  ]
  node [
    id 66
    label "take"
  ]
  node [
    id 67
    label "&#380;re&#263;"
  ]
  node [
    id 68
    label "macanie"
  ]
  node [
    id 69
    label "bia&#322;e_mi&#281;so"
  ]
  node [
    id 70
    label "domestic_fowl"
  ]
  node [
    id 71
    label "skubarka"
  ]
  node [
    id 72
    label "maca&#263;"
  ]
  node [
    id 73
    label "tuszka"
  ]
  node [
    id 74
    label "ptactwo"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 48
  ]
  edge [
    source 9
    target 74
  ]
]
