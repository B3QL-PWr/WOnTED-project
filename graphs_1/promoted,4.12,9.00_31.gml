graph [
  maxDegree 7
  minDegree 1
  meanDegree 1.8888888888888888
  density 0.1111111111111111
  graphCliqueNumber 2
  node [
    id 0
    label "zn&#243;w"
    origin "text"
  ]
  node [
    id 1
    label "kto&#347;"
    origin "text"
  ]
  node [
    id 2
    label "obejrze&#263;"
    origin "text"
  ]
  node [
    id 3
    label "si&#281;"
    origin "text"
  ]
  node [
    id 4
    label "nale&#380;e&#263;"
    origin "text"
  ]
  node [
    id 5
    label "cz&#322;owiek"
  ]
  node [
    id 6
    label "znaczenie"
  ]
  node [
    id 7
    label "go&#347;&#263;"
  ]
  node [
    id 8
    label "osoba"
  ]
  node [
    id 9
    label "posta&#263;"
  ]
  node [
    id 10
    label "zapozna&#263;_si&#281;"
  ]
  node [
    id 11
    label "visualize"
  ]
  node [
    id 12
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 13
    label "trza"
  ]
  node [
    id 14
    label "uczestniczy&#263;"
  ]
  node [
    id 15
    label "zalicza&#263;_si&#281;"
  ]
  node [
    id 16
    label "para"
  ]
  node [
    id 17
    label "necessity"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
]
