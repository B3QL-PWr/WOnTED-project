graph [
  maxDegree 12
  minDegree 1
  meanDegree 2.230769230769231
  density 0.08923076923076922
  graphCliqueNumber 4
  node [
    id 0
    label "powiat"
    origin "text"
  ]
  node [
    id 1
    label "&#347;redzki"
    origin "text"
  ]
  node [
    id 2
    label "wojew&#243;dztwo"
    origin "text"
  ]
  node [
    id 3
    label "wielkopolski"
    origin "text"
  ]
  node [
    id 4
    label "gmina"
  ]
  node [
    id 5
    label "jednostka_administracyjna"
  ]
  node [
    id 6
    label "makroregion"
  ]
  node [
    id 7
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 8
    label "mikroregion"
  ]
  node [
    id 9
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 10
    label "pa&#324;stwo"
  ]
  node [
    id 11
    label "po_wielkopolsku"
  ]
  node [
    id 12
    label "knypek"
  ]
  node [
    id 13
    label "kwyrla"
  ]
  node [
    id 14
    label "bimba"
  ]
  node [
    id 15
    label "hy&#263;ka"
  ]
  node [
    id 16
    label "plyndz"
  ]
  node [
    id 17
    label "regionalny"
  ]
  node [
    id 18
    label "gzik"
  ]
  node [
    id 19
    label "polski"
  ]
  node [
    id 20
    label "myrdyrda"
  ]
  node [
    id 21
    label "&#347;roda"
  ]
  node [
    id 22
    label "nowy"
  ]
  node [
    id 23
    label "miasto"
  ]
  node [
    id 24
    label "nad"
  ]
  node [
    id 25
    label "warta"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 24
  ]
  edge [
    source 22
    target 25
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 25
  ]
  edge [
    source 24
    target 25
  ]
]
