graph [
  maxDegree 27
  minDegree 1
  meanDegree 1.9534883720930232
  density 0.046511627906976744
  graphCliqueNumber 2
  node [
    id 0
    label "zosta&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 2
    label "rok"
    origin "text"
  ]
  node [
    id 3
    label "wed&#322;ug"
    origin "text"
  ]
  node [
    id 4
    label "time"
    origin "text"
  ]
  node [
    id 5
    label "asymilowa&#263;"
  ]
  node [
    id 6
    label "wapniak"
  ]
  node [
    id 7
    label "dwun&#243;g"
  ]
  node [
    id 8
    label "polifag"
  ]
  node [
    id 9
    label "wz&#243;r"
  ]
  node [
    id 10
    label "profanum"
  ]
  node [
    id 11
    label "hominid"
  ]
  node [
    id 12
    label "homo_sapiens"
  ]
  node [
    id 13
    label "nasada"
  ]
  node [
    id 14
    label "podw&#322;adny"
  ]
  node [
    id 15
    label "ludzko&#347;&#263;"
  ]
  node [
    id 16
    label "os&#322;abianie"
  ]
  node [
    id 17
    label "mikrokosmos"
  ]
  node [
    id 18
    label "portrecista"
  ]
  node [
    id 19
    label "duch"
  ]
  node [
    id 20
    label "g&#322;owa"
  ]
  node [
    id 21
    label "oddzia&#322;ywanie"
  ]
  node [
    id 22
    label "asymilowanie"
  ]
  node [
    id 23
    label "osoba"
  ]
  node [
    id 24
    label "os&#322;abia&#263;"
  ]
  node [
    id 25
    label "figura"
  ]
  node [
    id 26
    label "Adam"
  ]
  node [
    id 27
    label "senior"
  ]
  node [
    id 28
    label "antropochoria"
  ]
  node [
    id 29
    label "posta&#263;"
  ]
  node [
    id 30
    label "stulecie"
  ]
  node [
    id 31
    label "kalendarz"
  ]
  node [
    id 32
    label "czas"
  ]
  node [
    id 33
    label "pora_roku"
  ]
  node [
    id 34
    label "cykl_astronomiczny"
  ]
  node [
    id 35
    label "p&#243;&#322;rocze"
  ]
  node [
    id 36
    label "grupa"
  ]
  node [
    id 37
    label "kwarta&#322;"
  ]
  node [
    id 38
    label "kurs"
  ]
  node [
    id 39
    label "jubileusz"
  ]
  node [
    id 40
    label "miesi&#261;c"
  ]
  node [
    id 41
    label "lata"
  ]
  node [
    id 42
    label "martwy_sezon"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 3
    target 4
  ]
]
