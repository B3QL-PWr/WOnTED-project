graph [
  maxDegree 29
  minDegree 1
  meanDegree 1.9767441860465116
  density 0.023255813953488372
  graphCliqueNumber 2
  node [
    id 0
    label "zdj&#281;cie"
    origin "text"
  ]
  node [
    id 1
    label "nisko"
    origin "text"
  ]
  node [
    id 2
    label "przedstawia&#263;"
    origin "text"
  ]
  node [
    id 3
    label "para"
    origin "text"
  ]
  node [
    id 4
    label "oszust"
    origin "text"
  ]
  node [
    id 5
    label "wyretuszowanie"
  ]
  node [
    id 6
    label "podlew"
  ]
  node [
    id 7
    label "po&#347;ci&#261;ganie"
  ]
  node [
    id 8
    label "cenzura"
  ]
  node [
    id 9
    label "legitymacja"
  ]
  node [
    id 10
    label "uniewa&#380;nienie"
  ]
  node [
    id 11
    label "abolicjonista"
  ]
  node [
    id 12
    label "withdrawal"
  ]
  node [
    id 13
    label "uwolnienie"
  ]
  node [
    id 14
    label "picture"
  ]
  node [
    id 15
    label "retuszowa&#263;"
  ]
  node [
    id 16
    label "fota"
  ]
  node [
    id 17
    label "obraz"
  ]
  node [
    id 18
    label "fototeka"
  ]
  node [
    id 19
    label "zrobienie"
  ]
  node [
    id 20
    label "retuszowanie"
  ]
  node [
    id 21
    label "monid&#322;o"
  ]
  node [
    id 22
    label "talbotypia"
  ]
  node [
    id 23
    label "relief"
  ]
  node [
    id 24
    label "wyretuszowa&#263;"
  ]
  node [
    id 25
    label "photograph"
  ]
  node [
    id 26
    label "zabronienie"
  ]
  node [
    id 27
    label "ziarno"
  ]
  node [
    id 28
    label "przepa&#322;"
  ]
  node [
    id 29
    label "fotogaleria"
  ]
  node [
    id 30
    label "cinch"
  ]
  node [
    id 31
    label "odsuni&#281;cie"
  ]
  node [
    id 32
    label "rozpakowanie"
  ]
  node [
    id 33
    label "blisko"
  ]
  node [
    id 34
    label "po&#347;lednio"
  ]
  node [
    id 35
    label "despicably"
  ]
  node [
    id 36
    label "ma&#322;o"
  ]
  node [
    id 37
    label "niski"
  ]
  node [
    id 38
    label "vilely"
  ]
  node [
    id 39
    label "uni&#380;enie"
  ]
  node [
    id 40
    label "wstydliwie"
  ]
  node [
    id 41
    label "pospolicie"
  ]
  node [
    id 42
    label "ma&#322;y"
  ]
  node [
    id 43
    label "przedstawienie"
  ]
  node [
    id 44
    label "pokazywa&#263;"
  ]
  node [
    id 45
    label "zapoznawa&#263;"
  ]
  node [
    id 46
    label "typify"
  ]
  node [
    id 47
    label "opisywa&#263;"
  ]
  node [
    id 48
    label "teatr"
  ]
  node [
    id 49
    label "podawa&#263;"
  ]
  node [
    id 50
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 51
    label "demonstrowa&#263;"
  ]
  node [
    id 52
    label "represent"
  ]
  node [
    id 53
    label "ukazywa&#263;"
  ]
  node [
    id 54
    label "attest"
  ]
  node [
    id 55
    label "exhibit"
  ]
  node [
    id 56
    label "stanowi&#263;"
  ]
  node [
    id 57
    label "zg&#322;asza&#263;"
  ]
  node [
    id 58
    label "display"
  ]
  node [
    id 59
    label "gaz_cieplarniany"
  ]
  node [
    id 60
    label "grupa"
  ]
  node [
    id 61
    label "smoke"
  ]
  node [
    id 62
    label "pair"
  ]
  node [
    id 63
    label "sztuka"
  ]
  node [
    id 64
    label "Albania"
  ]
  node [
    id 65
    label "dodatek"
  ]
  node [
    id 66
    label "odparowanie"
  ]
  node [
    id 67
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 68
    label "odparowywa&#263;"
  ]
  node [
    id 69
    label "nale&#380;e&#263;"
  ]
  node [
    id 70
    label "wyparowanie"
  ]
  node [
    id 71
    label "zesp&#243;&#322;"
  ]
  node [
    id 72
    label "parowanie"
  ]
  node [
    id 73
    label "damp"
  ]
  node [
    id 74
    label "odparowywanie"
  ]
  node [
    id 75
    label "poker"
  ]
  node [
    id 76
    label "moneta"
  ]
  node [
    id 77
    label "odparowa&#263;"
  ]
  node [
    id 78
    label "jednostka_monetarna"
  ]
  node [
    id 79
    label "uk&#322;ad"
  ]
  node [
    id 80
    label "gaz"
  ]
  node [
    id 81
    label "chodzi&#263;"
  ]
  node [
    id 82
    label "zbi&#243;r"
  ]
  node [
    id 83
    label "Imperium_Osma&#324;skie"
  ]
  node [
    id 84
    label "cz&#322;owiek"
  ]
  node [
    id 85
    label "istota_&#380;ywa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
]
