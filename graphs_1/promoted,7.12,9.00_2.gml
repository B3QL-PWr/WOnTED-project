graph [
  maxDegree 17
  minDegree 1
  meanDegree 2.3142857142857145
  density 0.03354037267080745
  graphCliqueNumber 6
  node [
    id 0
    label "czwartek"
    origin "text"
  ]
  node [
    id 1
    label "ruszy&#263;"
    origin "text"
  ]
  node [
    id 2
    label "pierwszy"
    origin "text"
  ]
  node [
    id 3
    label "lokalny"
    origin "text"
  ]
  node [
    id 4
    label "dddzia&#322;"
    origin "text"
  ]
  node [
    id 5
    label "biuro"
    origin "text"
  ]
  node [
    id 6
    label "rzecznik"
    origin "text"
  ]
  node [
    id 7
    label "ma&#322;y"
    origin "text"
  ]
  node [
    id 8
    label "&#347;rednia"
    origin "text"
  ]
  node [
    id 9
    label "przedsi&#281;biorca"
    origin "text"
  ]
  node [
    id 10
    label "krak"
    origin "text"
  ]
  node [
    id 11
    label "dzie&#324;_powszedni"
  ]
  node [
    id 12
    label "Bo&#380;e_Cia&#322;o"
  ]
  node [
    id 13
    label "Wielki_Czwartek"
  ]
  node [
    id 14
    label "tydzie&#324;"
  ]
  node [
    id 15
    label "motivate"
  ]
  node [
    id 16
    label "zabra&#263;"
  ]
  node [
    id 17
    label "wzbudzi&#263;"
  ]
  node [
    id 18
    label "zacz&#261;&#263;"
  ]
  node [
    id 19
    label "spowodowa&#263;"
  ]
  node [
    id 20
    label "allude"
  ]
  node [
    id 21
    label "stimulate"
  ]
  node [
    id 22
    label "cut"
  ]
  node [
    id 23
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 24
    label "zrobi&#263;"
  ]
  node [
    id 25
    label "go"
  ]
  node [
    id 26
    label "najwa&#380;niejszy"
  ]
  node [
    id 27
    label "pocz&#261;tkowy"
  ]
  node [
    id 28
    label "dobry"
  ]
  node [
    id 29
    label "ch&#281;tny"
  ]
  node [
    id 30
    label "dzie&#324;"
  ]
  node [
    id 31
    label "pr&#281;dki"
  ]
  node [
    id 32
    label "lokalnie"
  ]
  node [
    id 33
    label "s&#261;d"
  ]
  node [
    id 34
    label "biurko"
  ]
  node [
    id 35
    label "palestra"
  ]
  node [
    id 36
    label "pomieszczenie"
  ]
  node [
    id 37
    label "agency"
  ]
  node [
    id 38
    label "Biuro_Lustracyjne"
  ]
  node [
    id 39
    label "instytucja"
  ]
  node [
    id 40
    label "board"
  ]
  node [
    id 41
    label "dzia&#322;"
  ]
  node [
    id 42
    label "boks"
  ]
  node [
    id 43
    label "doradca"
  ]
  node [
    id 44
    label "przyjaciel"
  ]
  node [
    id 45
    label "przedstawiciel"
  ]
  node [
    id 46
    label "marny"
  ]
  node [
    id 47
    label "nieznaczny"
  ]
  node [
    id 48
    label "s&#322;aby"
  ]
  node [
    id 49
    label "ch&#322;opiec"
  ]
  node [
    id 50
    label "ma&#322;o"
  ]
  node [
    id 51
    label "n&#281;dznie"
  ]
  node [
    id 52
    label "niewa&#380;ny"
  ]
  node [
    id 53
    label "przeci&#281;tny"
  ]
  node [
    id 54
    label "nieliczny"
  ]
  node [
    id 55
    label "wstydliwy"
  ]
  node [
    id 56
    label "szybki"
  ]
  node [
    id 57
    label "m&#322;ody"
  ]
  node [
    id 58
    label "miara_tendencji_centralnej"
  ]
  node [
    id 59
    label "wydawca"
  ]
  node [
    id 60
    label "kapitalista"
  ]
  node [
    id 61
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 62
    label "osoba_fizyczna"
  ]
  node [
    id 63
    label "wsp&#243;lnik"
  ]
  node [
    id 64
    label "klasa_&#347;rednia"
  ]
  node [
    id 65
    label "i"
  ]
  node [
    id 66
    label "analiza"
  ]
  node [
    id 67
    label "wniosek"
  ]
  node [
    id 68
    label "wydzia&#322;"
  ]
  node [
    id 69
    label "wst&#281;pna"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 6
    target 9
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 66
    target 68
  ]
  edge [
    source 66
    target 69
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 67
    target 69
  ]
  edge [
    source 68
    target 69
  ]
]
