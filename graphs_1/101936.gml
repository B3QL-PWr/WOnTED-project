graph [
  maxDegree 41
  minDegree 1
  meanDegree 2.0617283950617282
  density 0.012805766428954835
  graphCliqueNumber 3
  node [
    id 0
    label "nak&#322;ada&#263;"
    origin "text"
  ]
  node [
    id 1
    label "licencjobiorca"
    origin "text"
  ]
  node [
    id 2
    label "obowi&#261;zek"
    origin "text"
  ]
  node [
    id 3
    label "wykorzystywa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "licencjonowa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "know"
    origin "text"
  ]
  node [
    id 6
    label "how"
    origin "text"
  ]
  node [
    id 7
    label "lub"
    origin "text"
  ]
  node [
    id 8
    label "prawy"
    origin "text"
  ]
  node [
    id 9
    label "w&#322;asno&#347;&#263;"
    origin "text"
  ]
  node [
    id 10
    label "przemys&#322;owy"
    origin "text"
  ]
  node [
    id 11
    label "obszar"
    origin "text"
  ]
  node [
    id 12
    label "pokrywa&#263;"
    origin "text"
  ]
  node [
    id 13
    label "si&#281;"
    origin "text"
  ]
  node [
    id 14
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 15
    label "prowadzi&#263;"
    origin "text"
  ]
  node [
    id 16
    label "dzia&#322;alno&#347;&#263;"
    origin "text"
  ]
  node [
    id 17
    label "inny"
    origin "text"
  ]
  node [
    id 18
    label "nosi&#263;"
  ]
  node [
    id 19
    label "introduce"
  ]
  node [
    id 20
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 21
    label "wk&#322;ada&#263;"
  ]
  node [
    id 22
    label "robi&#263;"
  ]
  node [
    id 23
    label "umieszcza&#263;"
  ]
  node [
    id 24
    label "obleka&#263;"
  ]
  node [
    id 25
    label "odziewa&#263;"
  ]
  node [
    id 26
    label "powodowa&#263;"
  ]
  node [
    id 27
    label "inflict"
  ]
  node [
    id 28
    label "ubiera&#263;"
  ]
  node [
    id 29
    label "wytw&#243;rca"
  ]
  node [
    id 30
    label "powinno&#347;&#263;"
  ]
  node [
    id 31
    label "zadanie"
  ]
  node [
    id 32
    label "wym&#243;g"
  ]
  node [
    id 33
    label "duty"
  ]
  node [
    id 34
    label "obarczy&#263;"
  ]
  node [
    id 35
    label "use"
  ]
  node [
    id 36
    label "krzywdzi&#263;"
  ]
  node [
    id 37
    label "wyr&#281;cza&#263;_si&#281;"
  ]
  node [
    id 38
    label "distribute"
  ]
  node [
    id 39
    label "wsp&#243;&#322;&#380;y&#263;"
  ]
  node [
    id 40
    label "give"
  ]
  node [
    id 41
    label "liga&#263;"
  ]
  node [
    id 42
    label "u&#380;ywa&#263;"
  ]
  node [
    id 43
    label "korzysta&#263;"
  ]
  node [
    id 44
    label "akceptowa&#263;"
  ]
  node [
    id 45
    label "udzieli&#263;"
  ]
  node [
    id 46
    label "udziela&#263;"
  ]
  node [
    id 47
    label "hodowla"
  ]
  node [
    id 48
    label "rasowy"
  ]
  node [
    id 49
    label "license"
  ]
  node [
    id 50
    label "licencja"
  ]
  node [
    id 51
    label "z_prawa"
  ]
  node [
    id 52
    label "cnotliwy"
  ]
  node [
    id 53
    label "moralny"
  ]
  node [
    id 54
    label "naturalny"
  ]
  node [
    id 55
    label "zgodnie_z_prawem"
  ]
  node [
    id 56
    label "legalny"
  ]
  node [
    id 57
    label "na_prawo"
  ]
  node [
    id 58
    label "s&#322;uszny"
  ]
  node [
    id 59
    label "w_prawo"
  ]
  node [
    id 60
    label "w_spos&#243;b_prawy"
  ]
  node [
    id 61
    label "prawicowy"
  ]
  node [
    id 62
    label "chwalebny"
  ]
  node [
    id 63
    label "zacny"
  ]
  node [
    id 64
    label "rodowo&#347;&#263;"
  ]
  node [
    id 65
    label "prawo_rzeczowe"
  ]
  node [
    id 66
    label "possession"
  ]
  node [
    id 67
    label "stan"
  ]
  node [
    id 68
    label "dobra"
  ]
  node [
    id 69
    label "charakterystyka"
  ]
  node [
    id 70
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 71
    label "patent"
  ]
  node [
    id 72
    label "mienie"
  ]
  node [
    id 73
    label "przej&#347;&#263;"
  ]
  node [
    id 74
    label "attribute"
  ]
  node [
    id 75
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 76
    label "przej&#347;cie"
  ]
  node [
    id 77
    label "przemys&#322;owo"
  ]
  node [
    id 78
    label "masowy"
  ]
  node [
    id 79
    label "pas_planetoid"
  ]
  node [
    id 80
    label "wsch&#243;d"
  ]
  node [
    id 81
    label "Neogea"
  ]
  node [
    id 82
    label "holarktyka"
  ]
  node [
    id 83
    label "Rakowice"
  ]
  node [
    id 84
    label "Kosowo"
  ]
  node [
    id 85
    label "Syberia_Wschodnia"
  ]
  node [
    id 86
    label "wymiar"
  ]
  node [
    id 87
    label "p&#243;&#322;noc"
  ]
  node [
    id 88
    label "akrecja"
  ]
  node [
    id 89
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 90
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 91
    label "terytorium"
  ]
  node [
    id 92
    label "antroposfera"
  ]
  node [
    id 93
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 94
    label "po&#322;udnie"
  ]
  node [
    id 95
    label "zach&#243;d"
  ]
  node [
    id 96
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 97
    label "Olszanica"
  ]
  node [
    id 98
    label "Syberia_Zachodnia"
  ]
  node [
    id 99
    label "przestrze&#324;"
  ]
  node [
    id 100
    label "Notogea"
  ]
  node [
    id 101
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 102
    label "Ruda_Pabianicka"
  ]
  node [
    id 103
    label "&#321;&#281;g"
  ]
  node [
    id 104
    label "Antarktyka"
  ]
  node [
    id 105
    label "Piotrowo"
  ]
  node [
    id 106
    label "Zab&#322;ocie"
  ]
  node [
    id 107
    label "zakres"
  ]
  node [
    id 108
    label "miejsce"
  ]
  node [
    id 109
    label "Pow&#261;zki"
  ]
  node [
    id 110
    label "Arktyka"
  ]
  node [
    id 111
    label "zbi&#243;r"
  ]
  node [
    id 112
    label "Ludwin&#243;w"
  ]
  node [
    id 113
    label "Zabu&#380;e"
  ]
  node [
    id 114
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 115
    label "Kresy_Zachodnie"
  ]
  node [
    id 116
    label "cover"
  ]
  node [
    id 117
    label "p&#322;aci&#263;"
  ]
  node [
    id 118
    label "zap&#322;adnia&#263;"
  ]
  node [
    id 119
    label "report"
  ]
  node [
    id 120
    label "defray"
  ]
  node [
    id 121
    label "smother"
  ]
  node [
    id 122
    label "przykrywa&#263;"
  ]
  node [
    id 123
    label "supernatural"
  ]
  node [
    id 124
    label "zaspokaja&#263;"
  ]
  node [
    id 125
    label "rozwija&#263;"
  ]
  node [
    id 126
    label "r&#243;wna&#263;"
  ]
  node [
    id 127
    label "maskowa&#263;"
  ]
  node [
    id 128
    label "control"
  ]
  node [
    id 129
    label "eksponowa&#263;"
  ]
  node [
    id 130
    label "kre&#347;li&#263;"
  ]
  node [
    id 131
    label "g&#243;rowa&#263;"
  ]
  node [
    id 132
    label "message"
  ]
  node [
    id 133
    label "partner"
  ]
  node [
    id 134
    label "string"
  ]
  node [
    id 135
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 136
    label "przesuwa&#263;"
  ]
  node [
    id 137
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 138
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 139
    label "kierowa&#263;"
  ]
  node [
    id 140
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 141
    label "manipulate"
  ]
  node [
    id 142
    label "&#380;y&#263;"
  ]
  node [
    id 143
    label "navigate"
  ]
  node [
    id 144
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 145
    label "ukierunkowywa&#263;"
  ]
  node [
    id 146
    label "linia_melodyczna"
  ]
  node [
    id 147
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 148
    label "prowadzenie"
  ]
  node [
    id 149
    label "tworzy&#263;"
  ]
  node [
    id 150
    label "przewy&#380;sza&#263;"
  ]
  node [
    id 151
    label "sterowa&#263;"
  ]
  node [
    id 152
    label "krzywa"
  ]
  node [
    id 153
    label "dzia&#322;anie"
  ]
  node [
    id 154
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 155
    label "absolutorium"
  ]
  node [
    id 156
    label "activity"
  ]
  node [
    id 157
    label "kolejny"
  ]
  node [
    id 158
    label "inaczej"
  ]
  node [
    id 159
    label "r&#243;&#380;ny"
  ]
  node [
    id 160
    label "inszy"
  ]
  node [
    id 161
    label "osobno"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 13
  ]
  edge [
    source 11
    target 14
  ]
  edge [
    source 11
    target 79
  ]
  edge [
    source 11
    target 80
  ]
  edge [
    source 11
    target 81
  ]
  edge [
    source 11
    target 82
  ]
  edge [
    source 11
    target 83
  ]
  edge [
    source 11
    target 84
  ]
  edge [
    source 11
    target 85
  ]
  edge [
    source 11
    target 86
  ]
  edge [
    source 11
    target 87
  ]
  edge [
    source 11
    target 88
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 11
    target 92
  ]
  edge [
    source 11
    target 93
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 11
    target 98
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 11
    target 100
  ]
  edge [
    source 11
    target 101
  ]
  edge [
    source 11
    target 102
  ]
  edge [
    source 11
    target 103
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 108
  ]
  edge [
    source 11
    target 109
  ]
  edge [
    source 11
    target 110
  ]
  edge [
    source 11
    target 111
  ]
  edge [
    source 11
    target 112
  ]
  edge [
    source 11
    target 113
  ]
  edge [
    source 11
    target 114
  ]
  edge [
    source 11
    target 115
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 116
  ]
  edge [
    source 12
    target 117
  ]
  edge [
    source 12
    target 118
  ]
  edge [
    source 12
    target 119
  ]
  edge [
    source 12
    target 120
  ]
  edge [
    source 12
    target 121
  ]
  edge [
    source 12
    target 122
  ]
  edge [
    source 12
    target 123
  ]
  edge [
    source 12
    target 23
  ]
  edge [
    source 12
    target 124
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 128
  ]
  edge [
    source 15
    target 129
  ]
  edge [
    source 15
    target 130
  ]
  edge [
    source 15
    target 131
  ]
  edge [
    source 15
    target 132
  ]
  edge [
    source 15
    target 133
  ]
  edge [
    source 15
    target 134
  ]
  edge [
    source 15
    target 135
  ]
  edge [
    source 15
    target 136
  ]
  edge [
    source 15
    target 137
  ]
  edge [
    source 15
    target 138
  ]
  edge [
    source 15
    target 26
  ]
  edge [
    source 15
    target 139
  ]
  edge [
    source 15
    target 140
  ]
  edge [
    source 15
    target 22
  ]
  edge [
    source 15
    target 141
  ]
  edge [
    source 15
    target 142
  ]
  edge [
    source 15
    target 143
  ]
  edge [
    source 15
    target 144
  ]
  edge [
    source 15
    target 145
  ]
  edge [
    source 15
    target 146
  ]
  edge [
    source 15
    target 147
  ]
  edge [
    source 15
    target 148
  ]
  edge [
    source 15
    target 149
  ]
  edge [
    source 15
    target 150
  ]
  edge [
    source 15
    target 151
  ]
  edge [
    source 15
    target 152
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 153
  ]
  edge [
    source 16
    target 154
  ]
  edge [
    source 16
    target 155
  ]
  edge [
    source 16
    target 156
  ]
  edge [
    source 17
    target 157
  ]
  edge [
    source 17
    target 158
  ]
  edge [
    source 17
    target 159
  ]
  edge [
    source 17
    target 160
  ]
  edge [
    source 17
    target 161
  ]
]
