graph [
  maxDegree 45
  minDegree 1
  meanDegree 2.125
  density 0.008333333333333333
  graphCliqueNumber 5
  node [
    id 0
    label "kilka"
    origin "text"
  ]
  node [
    id 1
    label "dni"
    origin "text"
  ]
  node [
    id 2
    label "temu"
    origin "text"
  ]
  node [
    id 3
    label "opisywa&#263;by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "r&#243;&#380;nica"
    origin "text"
  ]
  node [
    id 5
    label "wydajno&#347;&#263;"
    origin "text"
  ]
  node [
    id 6
    label "mi&#281;dzy"
    origin "text"
  ]
  node [
    id 7
    label "system"
    origin "text"
  ]
  node [
    id 8
    label "archlinux"
    origin "text"
  ]
  node [
    id 9
    label "przypadek"
    origin "text"
  ]
  node [
    id 10
    label "zastosowa&#263;"
    origin "text"
  ]
  node [
    id 11
    label "ten"
    origin "text"
  ]
  node [
    id 12
    label "sam"
    origin "text"
  ]
  node [
    id 13
    label "konfiguracja"
    origin "text"
  ]
  node [
    id 14
    label "sprz&#281;towy"
    origin "text"
  ]
  node [
    id 15
    label "pakiet"
    origin "text"
  ]
  node [
    id 16
    label "domy&#347;lnie"
    origin "text"
  ]
  node [
    id 17
    label "w&#322;&#261;czy&#263;"
    origin "text"
  ]
  node [
    id 18
    label "compiza"
    origin "text"
  ]
  node [
    id 19
    label "druga"
    origin "text"
  ]
  node [
    id 20
    label "czego"
    origin "text"
  ]
  node [
    id 21
    label "pierwszy"
    origin "text"
  ]
  node [
    id 22
    label "posiada&#263;"
    origin "text"
  ]
  node [
    id 23
    label "jeden"
    origin "text"
  ]
  node [
    id 24
    label "test"
    origin "text"
  ]
  node [
    id 25
    label "da&#263;"
    origin "text"
  ]
  node [
    id 26
    label "zdumiewaj&#261;cy"
    origin "text"
  ]
  node [
    id 27
    label "wynik"
    origin "text"
  ]
  node [
    id 28
    label "serwis"
    origin "text"
  ]
  node [
    id 29
    label "phoronix"
    origin "text"
  ]
  node [
    id 30
    label "com"
    origin "text"
  ]
  node [
    id 31
    label "postanowi&#263;"
    origin "text"
  ]
  node [
    id 32
    label "zrobi&#263;"
    origin "text"
  ]
  node [
    id 33
    label "kolejny"
    origin "text"
  ]
  node [
    id 34
    label "sprawdzi&#263;"
    origin "text"
  ]
  node [
    id 35
    label "dlaczego"
    origin "text"
  ]
  node [
    id 36
    label "tak"
    origin "text"
  ]
  node [
    id 37
    label "&#347;ledziowate"
  ]
  node [
    id 38
    label "ryba"
  ]
  node [
    id 39
    label "czas"
  ]
  node [
    id 40
    label "r&#243;&#380;nienie"
  ]
  node [
    id 41
    label "cecha"
  ]
  node [
    id 42
    label "kontrastowy"
  ]
  node [
    id 43
    label "discord"
  ]
  node [
    id 44
    label "skuteczno&#347;&#263;"
  ]
  node [
    id 45
    label "op&#322;acalno&#347;&#263;"
  ]
  node [
    id 46
    label "model"
  ]
  node [
    id 47
    label "sk&#322;ad"
  ]
  node [
    id 48
    label "zachowanie"
  ]
  node [
    id 49
    label "podstawa"
  ]
  node [
    id 50
    label "porz&#261;dek"
  ]
  node [
    id 51
    label "Android"
  ]
  node [
    id 52
    label "przyn&#281;ta"
  ]
  node [
    id 53
    label "jednostka_geologiczna"
  ]
  node [
    id 54
    label "metoda"
  ]
  node [
    id 55
    label "podsystem"
  ]
  node [
    id 56
    label "p&#322;&#243;d"
  ]
  node [
    id 57
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 58
    label "s&#261;d"
  ]
  node [
    id 59
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 60
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 61
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 62
    label "j&#261;dro"
  ]
  node [
    id 63
    label "eratem"
  ]
  node [
    id 64
    label "pulpit"
  ]
  node [
    id 65
    label "struktura"
  ]
  node [
    id 66
    label "spos&#243;b"
  ]
  node [
    id 67
    label "oddzia&#322;"
  ]
  node [
    id 68
    label "usenet"
  ]
  node [
    id 69
    label "o&#347;"
  ]
  node [
    id 70
    label "oprogramowanie"
  ]
  node [
    id 71
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 72
    label "poj&#281;cie"
  ]
  node [
    id 73
    label "w&#281;dkarstwo"
  ]
  node [
    id 74
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 75
    label "Leopard"
  ]
  node [
    id 76
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 77
    label "systemik"
  ]
  node [
    id 78
    label "rozprz&#261;c"
  ]
  node [
    id 79
    label "cybernetyk"
  ]
  node [
    id 80
    label "konstelacja"
  ]
  node [
    id 81
    label "doktryna"
  ]
  node [
    id 82
    label "net"
  ]
  node [
    id 83
    label "zbi&#243;r"
  ]
  node [
    id 84
    label "method"
  ]
  node [
    id 85
    label "systemat"
  ]
  node [
    id 86
    label "pacjent"
  ]
  node [
    id 87
    label "kategoria_gramatyczna"
  ]
  node [
    id 88
    label "schorzenie"
  ]
  node [
    id 89
    label "przeznaczenie"
  ]
  node [
    id 90
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 91
    label "wydarzenie"
  ]
  node [
    id 92
    label "happening"
  ]
  node [
    id 93
    label "przyk&#322;ad"
  ]
  node [
    id 94
    label "u&#380;y&#263;"
  ]
  node [
    id 95
    label "okre&#347;lony"
  ]
  node [
    id 96
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 97
    label "sklep"
  ]
  node [
    id 98
    label "uk&#322;ad"
  ]
  node [
    id 99
    label "po&#322;o&#380;enie"
  ]
  node [
    id 100
    label "cz&#261;steczka"
  ]
  node [
    id 101
    label "kompozycja"
  ]
  node [
    id 102
    label "relacja"
  ]
  node [
    id 103
    label "akcja"
  ]
  node [
    id 104
    label "ilo&#347;&#263;"
  ]
  node [
    id 105
    label "porcja"
  ]
  node [
    id 106
    label "package"
  ]
  node [
    id 107
    label "bundle"
  ]
  node [
    id 108
    label "jednostka_informacji"
  ]
  node [
    id 109
    label "znacz&#261;co"
  ]
  node [
    id 110
    label "zacz&#261;&#263;"
  ]
  node [
    id 111
    label "nastawi&#263;"
  ]
  node [
    id 112
    label "prosecute"
  ]
  node [
    id 113
    label "odkr&#281;ci&#263;_kurek"
  ]
  node [
    id 114
    label "impersonate"
  ]
  node [
    id 115
    label "umie&#347;ci&#263;"
  ]
  node [
    id 116
    label "obejrze&#263;"
  ]
  node [
    id 117
    label "draw"
  ]
  node [
    id 118
    label "incorporate"
  ]
  node [
    id 119
    label "uruchomi&#263;"
  ]
  node [
    id 120
    label "dokoptowa&#263;"
  ]
  node [
    id 121
    label "godzina"
  ]
  node [
    id 122
    label "najwa&#380;niejszy"
  ]
  node [
    id 123
    label "pocz&#261;tkowy"
  ]
  node [
    id 124
    label "dobry"
  ]
  node [
    id 125
    label "ch&#281;tny"
  ]
  node [
    id 126
    label "dzie&#324;"
  ]
  node [
    id 127
    label "pr&#281;dki"
  ]
  node [
    id 128
    label "wiedzie&#263;"
  ]
  node [
    id 129
    label "mie&#263;"
  ]
  node [
    id 130
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 131
    label "keep_open"
  ]
  node [
    id 132
    label "zawiera&#263;"
  ]
  node [
    id 133
    label "support"
  ]
  node [
    id 134
    label "zdolno&#347;&#263;"
  ]
  node [
    id 135
    label "kieliszek"
  ]
  node [
    id 136
    label "shot"
  ]
  node [
    id 137
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 138
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 139
    label "jaki&#347;"
  ]
  node [
    id 140
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 141
    label "jednolicie"
  ]
  node [
    id 142
    label "w&#243;dka"
  ]
  node [
    id 143
    label "ujednolicenie"
  ]
  node [
    id 144
    label "jednakowy"
  ]
  node [
    id 145
    label "do&#347;wiadczenie"
  ]
  node [
    id 146
    label "arkusz"
  ]
  node [
    id 147
    label "sprawdzian"
  ]
  node [
    id 148
    label "quiz"
  ]
  node [
    id 149
    label "przechodzi&#263;"
  ]
  node [
    id 150
    label "przechodzenie"
  ]
  node [
    id 151
    label "badanie"
  ]
  node [
    id 152
    label "narz&#281;dzie"
  ]
  node [
    id 153
    label "sytuacja"
  ]
  node [
    id 154
    label "dostarczy&#263;"
  ]
  node [
    id 155
    label "obieca&#263;"
  ]
  node [
    id 156
    label "pozwoli&#263;"
  ]
  node [
    id 157
    label "przeznaczy&#263;"
  ]
  node [
    id 158
    label "doda&#263;"
  ]
  node [
    id 159
    label "give"
  ]
  node [
    id 160
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 161
    label "wyrzec_si&#281;"
  ]
  node [
    id 162
    label "supply"
  ]
  node [
    id 163
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 164
    label "zada&#263;"
  ]
  node [
    id 165
    label "odst&#261;pi&#263;"
  ]
  node [
    id 166
    label "feed"
  ]
  node [
    id 167
    label "testify"
  ]
  node [
    id 168
    label "powierzy&#263;"
  ]
  node [
    id 169
    label "convey"
  ]
  node [
    id 170
    label "przekaza&#263;"
  ]
  node [
    id 171
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 172
    label "zap&#322;aci&#263;"
  ]
  node [
    id 173
    label "dress"
  ]
  node [
    id 174
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 175
    label "udost&#281;pni&#263;"
  ]
  node [
    id 176
    label "sztachn&#261;&#263;"
  ]
  node [
    id 177
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 178
    label "przywali&#263;"
  ]
  node [
    id 179
    label "rap"
  ]
  node [
    id 180
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 181
    label "picture"
  ]
  node [
    id 182
    label "zdumiewaj&#261;co"
  ]
  node [
    id 183
    label "niesamowity"
  ]
  node [
    id 184
    label "typ"
  ]
  node [
    id 185
    label "dzia&#322;anie"
  ]
  node [
    id 186
    label "przyczyna"
  ]
  node [
    id 187
    label "zaokr&#261;gli&#263;"
  ]
  node [
    id 188
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 189
    label "zaokr&#261;glenie"
  ]
  node [
    id 190
    label "event"
  ]
  node [
    id 191
    label "rezultat"
  ]
  node [
    id 192
    label "mecz"
  ]
  node [
    id 193
    label "service"
  ]
  node [
    id 194
    label "wytw&#243;r"
  ]
  node [
    id 195
    label "zak&#322;ad"
  ]
  node [
    id 196
    label "us&#322;uga"
  ]
  node [
    id 197
    label "uderzenie"
  ]
  node [
    id 198
    label "doniesienie"
  ]
  node [
    id 199
    label "zastawa"
  ]
  node [
    id 200
    label "YouTube"
  ]
  node [
    id 201
    label "punkt"
  ]
  node [
    id 202
    label "strona"
  ]
  node [
    id 203
    label "sta&#263;_si&#281;"
  ]
  node [
    id 204
    label "podj&#261;&#263;"
  ]
  node [
    id 205
    label "determine"
  ]
  node [
    id 206
    label "zorganizowa&#263;"
  ]
  node [
    id 207
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 208
    label "przerobi&#263;"
  ]
  node [
    id 209
    label "wystylizowa&#263;"
  ]
  node [
    id 210
    label "cause"
  ]
  node [
    id 211
    label "wydali&#263;"
  ]
  node [
    id 212
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 213
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 214
    label "post&#261;pi&#263;"
  ]
  node [
    id 215
    label "appoint"
  ]
  node [
    id 216
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 217
    label "nabra&#263;"
  ]
  node [
    id 218
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 219
    label "make"
  ]
  node [
    id 220
    label "inny"
  ]
  node [
    id 221
    label "nast&#281;pnie"
  ]
  node [
    id 222
    label "kt&#243;ry&#347;"
  ]
  node [
    id 223
    label "kolejno"
  ]
  node [
    id 224
    label "nastopny"
  ]
  node [
    id 225
    label "examine"
  ]
  node [
    id 226
    label "KDE"
  ]
  node [
    id 227
    label "3"
  ]
  node [
    id 228
    label "Mozilla"
  ]
  node [
    id 229
    label "Firefox"
  ]
  node [
    id 230
    label "Ben"
  ]
  node [
    id 231
    label "Zhao"
  ]
  node [
    id 232
    label "Ubuntu"
  ]
  node [
    id 233
    label "9"
  ]
  node [
    id 234
    label "10"
  ]
  node [
    id 235
    label "Karmic"
  ]
  node [
    id 236
    label "koala"
  ]
  node [
    id 237
    label "VLOS"
  ]
  node [
    id 238
    label "2"
  ]
  node [
    id 239
    label "0"
  ]
  node [
    id 240
    label "Matthew"
  ]
  node [
    id 241
    label "Dawkins"
  ]
  node [
    id 242
    label "unita"
  ]
  node [
    id 243
    label "Linux"
  ]
  node [
    id 244
    label "2010"
  ]
  node [
    id 245
    label "beta"
  ]
  node [
    id 246
    label "Mandriva"
  ]
  node [
    id 247
    label "Stefan"
  ]
  node [
    id 248
    label "Lippers"
  ]
  node [
    id 249
    label "Hollmann"
  ]
  node [
    id 250
    label "sidux"
  ]
  node [
    id 251
    label "2009"
  ]
  node [
    id 252
    label "04"
  ]
  node [
    id 253
    label "6"
  ]
  node [
    id 254
    label "32"
  ]
  node [
    id 255
    label "gruba"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 19
  ]
  edge [
    source 7
    target 20
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 38
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 26
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 11
    target 23
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 12
    target 15
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 98
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 13
    target 101
  ]
  edge [
    source 13
    target 102
  ]
  edge [
    source 13
    target 72
  ]
  edge [
    source 13
    target 65
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 101
  ]
  edge [
    source 15
    target 61
  ]
  edge [
    source 15
    target 103
  ]
  edge [
    source 15
    target 104
  ]
  edge [
    source 15
    target 105
  ]
  edge [
    source 15
    target 106
  ]
  edge [
    source 15
    target 107
  ]
  edge [
    source 15
    target 108
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 109
  ]
  edge [
    source 16
    target 19
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 110
  ]
  edge [
    source 17
    target 111
  ]
  edge [
    source 17
    target 112
  ]
  edge [
    source 17
    target 113
  ]
  edge [
    source 17
    target 114
  ]
  edge [
    source 17
    target 115
  ]
  edge [
    source 17
    target 116
  ]
  edge [
    source 17
    target 117
  ]
  edge [
    source 17
    target 118
  ]
  edge [
    source 17
    target 119
  ]
  edge [
    source 17
    target 120
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 121
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 122
  ]
  edge [
    source 21
    target 123
  ]
  edge [
    source 21
    target 124
  ]
  edge [
    source 21
    target 125
  ]
  edge [
    source 21
    target 126
  ]
  edge [
    source 21
    target 127
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 128
  ]
  edge [
    source 22
    target 129
  ]
  edge [
    source 22
    target 130
  ]
  edge [
    source 22
    target 131
  ]
  edge [
    source 22
    target 132
  ]
  edge [
    source 22
    target 133
  ]
  edge [
    source 22
    target 134
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 135
  ]
  edge [
    source 23
    target 136
  ]
  edge [
    source 23
    target 137
  ]
  edge [
    source 23
    target 138
  ]
  edge [
    source 23
    target 139
  ]
  edge [
    source 23
    target 140
  ]
  edge [
    source 23
    target 141
  ]
  edge [
    source 23
    target 142
  ]
  edge [
    source 23
    target 143
  ]
  edge [
    source 23
    target 144
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 33
  ]
  edge [
    source 24
    target 34
  ]
  edge [
    source 24
    target 145
  ]
  edge [
    source 24
    target 146
  ]
  edge [
    source 24
    target 147
  ]
  edge [
    source 24
    target 148
  ]
  edge [
    source 24
    target 149
  ]
  edge [
    source 24
    target 150
  ]
  edge [
    source 24
    target 151
  ]
  edge [
    source 24
    target 152
  ]
  edge [
    source 24
    target 153
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 154
  ]
  edge [
    source 25
    target 155
  ]
  edge [
    source 25
    target 156
  ]
  edge [
    source 25
    target 157
  ]
  edge [
    source 25
    target 158
  ]
  edge [
    source 25
    target 159
  ]
  edge [
    source 25
    target 160
  ]
  edge [
    source 25
    target 161
  ]
  edge [
    source 25
    target 162
  ]
  edge [
    source 25
    target 163
  ]
  edge [
    source 25
    target 164
  ]
  edge [
    source 25
    target 165
  ]
  edge [
    source 25
    target 166
  ]
  edge [
    source 25
    target 167
  ]
  edge [
    source 25
    target 168
  ]
  edge [
    source 25
    target 169
  ]
  edge [
    source 25
    target 170
  ]
  edge [
    source 25
    target 171
  ]
  edge [
    source 25
    target 172
  ]
  edge [
    source 25
    target 173
  ]
  edge [
    source 25
    target 174
  ]
  edge [
    source 25
    target 175
  ]
  edge [
    source 25
    target 176
  ]
  edge [
    source 25
    target 177
  ]
  edge [
    source 25
    target 32
  ]
  edge [
    source 25
    target 178
  ]
  edge [
    source 25
    target 179
  ]
  edge [
    source 25
    target 180
  ]
  edge [
    source 25
    target 181
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 182
  ]
  edge [
    source 26
    target 183
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 184
  ]
  edge [
    source 27
    target 185
  ]
  edge [
    source 27
    target 186
  ]
  edge [
    source 27
    target 187
  ]
  edge [
    source 27
    target 188
  ]
  edge [
    source 27
    target 189
  ]
  edge [
    source 27
    target 190
  ]
  edge [
    source 27
    target 191
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 192
  ]
  edge [
    source 28
    target 193
  ]
  edge [
    source 28
    target 194
  ]
  edge [
    source 28
    target 195
  ]
  edge [
    source 28
    target 196
  ]
  edge [
    source 28
    target 197
  ]
  edge [
    source 28
    target 198
  ]
  edge [
    source 28
    target 199
  ]
  edge [
    source 28
    target 200
  ]
  edge [
    source 28
    target 201
  ]
  edge [
    source 28
    target 105
  ]
  edge [
    source 28
    target 202
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 203
  ]
  edge [
    source 31
    target 204
  ]
  edge [
    source 31
    target 205
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 206
  ]
  edge [
    source 32
    target 207
  ]
  edge [
    source 32
    target 208
  ]
  edge [
    source 32
    target 209
  ]
  edge [
    source 32
    target 210
  ]
  edge [
    source 32
    target 211
  ]
  edge [
    source 32
    target 212
  ]
  edge [
    source 32
    target 213
  ]
  edge [
    source 32
    target 214
  ]
  edge [
    source 32
    target 215
  ]
  edge [
    source 32
    target 216
  ]
  edge [
    source 32
    target 217
  ]
  edge [
    source 32
    target 218
  ]
  edge [
    source 32
    target 219
  ]
  edge [
    source 32
    target 34
  ]
  edge [
    source 33
    target 220
  ]
  edge [
    source 33
    target 221
  ]
  edge [
    source 33
    target 222
  ]
  edge [
    source 33
    target 223
  ]
  edge [
    source 33
    target 224
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 225
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 226
    target 227
  ]
  edge [
    source 228
    target 229
  ]
  edge [
    source 230
    target 231
  ]
  edge [
    source 232
    target 233
  ]
  edge [
    source 232
    target 234
  ]
  edge [
    source 233
    target 234
  ]
  edge [
    source 235
    target 236
  ]
  edge [
    source 237
    target 238
  ]
  edge [
    source 237
    target 239
  ]
  edge [
    source 238
    target 239
  ]
  edge [
    source 238
    target 242
  ]
  edge [
    source 238
    target 243
  ]
  edge [
    source 238
    target 244
  ]
  edge [
    source 238
    target 245
  ]
  edge [
    source 238
    target 253
  ]
  edge [
    source 238
    target 254
  ]
  edge [
    source 238
    target 255
  ]
  edge [
    source 240
    target 241
  ]
  edge [
    source 242
    target 243
  ]
  edge [
    source 242
    target 244
  ]
  edge [
    source 242
    target 245
  ]
  edge [
    source 243
    target 244
  ]
  edge [
    source 243
    target 245
  ]
  edge [
    source 243
    target 246
  ]
  edge [
    source 244
    target 245
  ]
  edge [
    source 247
    target 248
  ]
  edge [
    source 247
    target 249
  ]
  edge [
    source 248
    target 249
  ]
  edge [
    source 250
    target 251
  ]
  edge [
    source 250
    target 252
  ]
  edge [
    source 251
    target 252
  ]
  edge [
    source 253
    target 254
  ]
]
