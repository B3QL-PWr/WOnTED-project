graph [
  maxDegree 36
  minDegree 1
  meanDegree 2.0451977401129944
  density 0.011620441705187468
  graphCliqueNumber 3
  node [
    id 0
    label "skoro"
    origin "text"
  ]
  node [
    id 1
    label "rozszyfrowa&#263;by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "exegi"
    origin "text"
  ]
  node [
    id 3
    label "monumentum"
    origin "text"
  ]
  node [
    id 4
    label "par"
    origin "text"
  ]
  node [
    id 5
    label "nona"
    origin "text"
  ]
  node [
    id 6
    label "omnis"
    origin "text"
  ]
  node [
    id 7
    label "moriar"
    origin "text"
  ]
  node [
    id 8
    label "jako"
    origin "text"
  ]
  node [
    id 9
    label "nie&#347;miertelno&#347;c"
    origin "text"
  ]
  node [
    id 10
    label "poezja"
    origin "text"
  ]
  node [
    id 11
    label "czy"
    origin "text"
  ]
  node [
    id 12
    label "si&#281;"
    origin "text"
  ]
  node [
    id 13
    label "zapisa&#263;"
    origin "text"
  ]
  node [
    id 14
    label "taki"
    origin "text"
  ]
  node [
    id 15
    label "raz"
    origin "text"
  ]
  node [
    id 16
    label "z&#322;o&#380;enie"
    origin "text"
  ]
  node [
    id 17
    label "rzuci&#263;"
    origin "text"
  ]
  node [
    id 18
    label "oko"
    origin "text"
  ]
  node [
    id 19
    label "opis"
    origin "text"
  ]
  node [
    id 20
    label "motyw"
    origin "text"
  ]
  node [
    id 21
    label "nie&#347;miertelno&#347;&#263;"
    origin "text"
  ]
  node [
    id 22
    label "by&#263;"
    origin "text"
  ]
  node [
    id 23
    label "jeden"
    origin "text"
  ]
  node [
    id 24
    label "strategia"
    origin "text"
  ]
  node [
    id 25
    label "Izba_Par&#243;w"
  ]
  node [
    id 26
    label "lord"
  ]
  node [
    id 27
    label "Izba_Lord&#243;w"
  ]
  node [
    id 28
    label "parlamentarzysta"
  ]
  node [
    id 29
    label "lennik"
  ]
  node [
    id 30
    label "godzina_kanoniczna"
  ]
  node [
    id 31
    label "interwa&#322;"
  ]
  node [
    id 32
    label "nastrojowo&#347;&#263;"
  ]
  node [
    id 33
    label "literacko&#347;&#263;"
  ]
  node [
    id 34
    label "literatura"
  ]
  node [
    id 35
    label "romanticism"
  ]
  node [
    id 36
    label "wierszoklectwo"
  ]
  node [
    id 37
    label "rewrite"
  ]
  node [
    id 38
    label "spowodowa&#263;"
  ]
  node [
    id 39
    label "napisa&#263;"
  ]
  node [
    id 40
    label "zaleci&#263;"
  ]
  node [
    id 41
    label "wype&#322;ni&#263;"
  ]
  node [
    id 42
    label "utrwali&#263;"
  ]
  node [
    id 43
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 44
    label "przekaza&#263;"
  ]
  node [
    id 45
    label "lekarstwo"
  ]
  node [
    id 46
    label "write"
  ]
  node [
    id 47
    label "substitute"
  ]
  node [
    id 48
    label "okre&#347;lony"
  ]
  node [
    id 49
    label "jaki&#347;"
  ]
  node [
    id 50
    label "chwila"
  ]
  node [
    id 51
    label "uderzenie"
  ]
  node [
    id 52
    label "cios"
  ]
  node [
    id 53
    label "time"
  ]
  node [
    id 54
    label "opracowanie"
  ]
  node [
    id 55
    label "zgi&#281;cie"
  ]
  node [
    id 56
    label "stage_set"
  ]
  node [
    id 57
    label "posk&#322;adanie"
  ]
  node [
    id 58
    label "zestawienie"
  ]
  node [
    id 59
    label "danie"
  ]
  node [
    id 60
    label "lodging"
  ]
  node [
    id 61
    label "zgromadzenie"
  ]
  node [
    id 62
    label "powiedzenie"
  ]
  node [
    id 63
    label "blend"
  ]
  node [
    id 64
    label "przekazanie"
  ]
  node [
    id 65
    label "derywat_z&#322;o&#380;ony"
  ]
  node [
    id 66
    label "fold"
  ]
  node [
    id 67
    label "pay"
  ]
  node [
    id 68
    label "leksem"
  ]
  node [
    id 69
    label "set"
  ]
  node [
    id 70
    label "przy&#322;o&#380;enie"
  ]
  node [
    id 71
    label "removal"
  ]
  node [
    id 72
    label "wyzwanie"
  ]
  node [
    id 73
    label "majdn&#261;&#263;"
  ]
  node [
    id 74
    label "opu&#347;ci&#263;"
  ]
  node [
    id 75
    label "cie&#324;"
  ]
  node [
    id 76
    label "konwulsja"
  ]
  node [
    id 77
    label "podejrzenie"
  ]
  node [
    id 78
    label "wywo&#322;a&#263;"
  ]
  node [
    id 79
    label "ruszy&#263;"
  ]
  node [
    id 80
    label "odej&#347;&#263;"
  ]
  node [
    id 81
    label "project"
  ]
  node [
    id 82
    label "da&#263;"
  ]
  node [
    id 83
    label "czar"
  ]
  node [
    id 84
    label "atak"
  ]
  node [
    id 85
    label "przewr&#243;ci&#263;"
  ]
  node [
    id 86
    label "zdecydowa&#263;"
  ]
  node [
    id 87
    label "rush"
  ]
  node [
    id 88
    label "bewilder"
  ]
  node [
    id 89
    label "zmieni&#263;_w&#322;a&#347;ciwo&#347;ci_fizyczne_lub_chemiczne_czego&#347;"
  ]
  node [
    id 90
    label "sygn&#261;&#263;"
  ]
  node [
    id 91
    label "zmieni&#263;"
  ]
  node [
    id 92
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 93
    label "poruszy&#263;"
  ]
  node [
    id 94
    label "&#347;wiat&#322;o"
  ]
  node [
    id 95
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 96
    label "most"
  ]
  node [
    id 97
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 98
    label "frame"
  ]
  node [
    id 99
    label "powiedzie&#263;"
  ]
  node [
    id 100
    label "przeznaczenie"
  ]
  node [
    id 101
    label "pierdoln&#261;&#263;"
  ]
  node [
    id 102
    label "peddle"
  ]
  node [
    id 103
    label "skonstruowa&#263;"
  ]
  node [
    id 104
    label "towar"
  ]
  node [
    id 105
    label "wypowied&#378;"
  ]
  node [
    id 106
    label "siniec"
  ]
  node [
    id 107
    label "uwaga"
  ]
  node [
    id 108
    label "rzecz"
  ]
  node [
    id 109
    label "&#380;y&#322;a_&#347;rodkowa_siatk&#243;wki"
  ]
  node [
    id 110
    label "powieka"
  ]
  node [
    id 111
    label "oczy"
  ]
  node [
    id 112
    label "&#347;lepko"
  ]
  node [
    id 113
    label "ga&#322;ka_oczna"
  ]
  node [
    id 114
    label "ser_&#380;&#243;&#322;ty"
  ]
  node [
    id 115
    label "&#347;lepie"
  ]
  node [
    id 116
    label "twarz"
  ]
  node [
    id 117
    label "organ"
  ]
  node [
    id 118
    label "worek_spoj&#243;wkowy"
  ]
  node [
    id 119
    label "nerw_wzrokowy"
  ]
  node [
    id 120
    label "wzrok"
  ]
  node [
    id 121
    label "&#378;renica"
  ]
  node [
    id 122
    label "spoj&#243;wka"
  ]
  node [
    id 123
    label "narz&#261;d_&#322;zowy"
  ]
  node [
    id 124
    label "nab&#322;onek_barwnikowy"
  ]
  node [
    id 125
    label "kaprawie&#263;"
  ]
  node [
    id 126
    label "kaprawienie"
  ]
  node [
    id 127
    label "spojrzenie"
  ]
  node [
    id 128
    label "net"
  ]
  node [
    id 129
    label "r&#261;bek_rog&#243;wki"
  ]
  node [
    id 130
    label "coloboma"
  ]
  node [
    id 131
    label "ros&#243;&#322;"
  ]
  node [
    id 132
    label "exposition"
  ]
  node [
    id 133
    label "czynno&#347;&#263;"
  ]
  node [
    id 134
    label "obja&#347;nienie"
  ]
  node [
    id 135
    label "fraza"
  ]
  node [
    id 136
    label "melodia"
  ]
  node [
    id 137
    label "temat"
  ]
  node [
    id 138
    label "przyczyna"
  ]
  node [
    id 139
    label "cecha"
  ]
  node [
    id 140
    label "ozdoba"
  ]
  node [
    id 141
    label "wydarzenie"
  ]
  node [
    id 142
    label "sytuacja"
  ]
  node [
    id 143
    label "si&#281;ga&#263;"
  ]
  node [
    id 144
    label "trwa&#263;"
  ]
  node [
    id 145
    label "obecno&#347;&#263;"
  ]
  node [
    id 146
    label "stan"
  ]
  node [
    id 147
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 148
    label "stand"
  ]
  node [
    id 149
    label "mie&#263;_miejsce"
  ]
  node [
    id 150
    label "uczestniczy&#263;"
  ]
  node [
    id 151
    label "chodzi&#263;"
  ]
  node [
    id 152
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 153
    label "equal"
  ]
  node [
    id 154
    label "kieliszek"
  ]
  node [
    id 155
    label "shot"
  ]
  node [
    id 156
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 157
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 158
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 159
    label "jednolicie"
  ]
  node [
    id 160
    label "w&#243;dka"
  ]
  node [
    id 161
    label "ten"
  ]
  node [
    id 162
    label "ujednolicenie"
  ]
  node [
    id 163
    label "jednakowy"
  ]
  node [
    id 164
    label "dokument"
  ]
  node [
    id 165
    label "wzorzec_projektowy"
  ]
  node [
    id 166
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 167
    label "gra"
  ]
  node [
    id 168
    label "pocz&#261;tki"
  ]
  node [
    id 169
    label "doktryna"
  ]
  node [
    id 170
    label "program"
  ]
  node [
    id 171
    label "plan"
  ]
  node [
    id 172
    label "metoda"
  ]
  node [
    id 173
    label "wojskowo&#347;&#263;"
  ]
  node [
    id 174
    label "operacja"
  ]
  node [
    id 175
    label "dziedzina"
  ]
  node [
    id 176
    label "wrinkle"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 15
  ]
  edge [
    source 8
    target 16
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 32
  ]
  edge [
    source 10
    target 33
  ]
  edge [
    source 10
    target 34
  ]
  edge [
    source 10
    target 35
  ]
  edge [
    source 10
    target 36
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 37
  ]
  edge [
    source 13
    target 38
  ]
  edge [
    source 13
    target 39
  ]
  edge [
    source 13
    target 40
  ]
  edge [
    source 13
    target 41
  ]
  edge [
    source 13
    target 42
  ]
  edge [
    source 13
    target 43
  ]
  edge [
    source 13
    target 44
  ]
  edge [
    source 13
    target 45
  ]
  edge [
    source 13
    target 46
  ]
  edge [
    source 13
    target 47
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 48
  ]
  edge [
    source 14
    target 49
  ]
  edge [
    source 15
    target 50
  ]
  edge [
    source 15
    target 51
  ]
  edge [
    source 15
    target 52
  ]
  edge [
    source 15
    target 53
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 54
  ]
  edge [
    source 16
    target 55
  ]
  edge [
    source 16
    target 56
  ]
  edge [
    source 16
    target 57
  ]
  edge [
    source 16
    target 58
  ]
  edge [
    source 16
    target 59
  ]
  edge [
    source 16
    target 60
  ]
  edge [
    source 16
    target 61
  ]
  edge [
    source 16
    target 62
  ]
  edge [
    source 16
    target 63
  ]
  edge [
    source 16
    target 64
  ]
  edge [
    source 16
    target 65
  ]
  edge [
    source 16
    target 66
  ]
  edge [
    source 16
    target 67
  ]
  edge [
    source 16
    target 68
  ]
  edge [
    source 16
    target 69
  ]
  edge [
    source 16
    target 70
  ]
  edge [
    source 16
    target 71
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 72
  ]
  edge [
    source 17
    target 73
  ]
  edge [
    source 17
    target 74
  ]
  edge [
    source 17
    target 75
  ]
  edge [
    source 17
    target 76
  ]
  edge [
    source 17
    target 77
  ]
  edge [
    source 17
    target 78
  ]
  edge [
    source 17
    target 79
  ]
  edge [
    source 17
    target 80
  ]
  edge [
    source 17
    target 81
  ]
  edge [
    source 17
    target 82
  ]
  edge [
    source 17
    target 83
  ]
  edge [
    source 17
    target 84
  ]
  edge [
    source 17
    target 85
  ]
  edge [
    source 17
    target 86
  ]
  edge [
    source 17
    target 87
  ]
  edge [
    source 17
    target 88
  ]
  edge [
    source 17
    target 89
  ]
  edge [
    source 17
    target 90
  ]
  edge [
    source 17
    target 91
  ]
  edge [
    source 17
    target 92
  ]
  edge [
    source 17
    target 93
  ]
  edge [
    source 17
    target 94
  ]
  edge [
    source 17
    target 95
  ]
  edge [
    source 17
    target 96
  ]
  edge [
    source 17
    target 97
  ]
  edge [
    source 17
    target 38
  ]
  edge [
    source 17
    target 98
  ]
  edge [
    source 17
    target 99
  ]
  edge [
    source 17
    target 100
  ]
  edge [
    source 17
    target 101
  ]
  edge [
    source 17
    target 102
  ]
  edge [
    source 17
    target 103
  ]
  edge [
    source 17
    target 104
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 105
  ]
  edge [
    source 18
    target 106
  ]
  edge [
    source 18
    target 107
  ]
  edge [
    source 18
    target 108
  ]
  edge [
    source 18
    target 109
  ]
  edge [
    source 18
    target 110
  ]
  edge [
    source 18
    target 111
  ]
  edge [
    source 18
    target 112
  ]
  edge [
    source 18
    target 113
  ]
  edge [
    source 18
    target 114
  ]
  edge [
    source 18
    target 115
  ]
  edge [
    source 18
    target 116
  ]
  edge [
    source 18
    target 117
  ]
  edge [
    source 18
    target 118
  ]
  edge [
    source 18
    target 119
  ]
  edge [
    source 18
    target 120
  ]
  edge [
    source 18
    target 121
  ]
  edge [
    source 18
    target 122
  ]
  edge [
    source 18
    target 123
  ]
  edge [
    source 18
    target 124
  ]
  edge [
    source 18
    target 125
  ]
  edge [
    source 18
    target 126
  ]
  edge [
    source 18
    target 127
  ]
  edge [
    source 18
    target 128
  ]
  edge [
    source 18
    target 129
  ]
  edge [
    source 18
    target 130
  ]
  edge [
    source 18
    target 131
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 132
  ]
  edge [
    source 19
    target 133
  ]
  edge [
    source 19
    target 105
  ]
  edge [
    source 19
    target 134
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 135
  ]
  edge [
    source 20
    target 136
  ]
  edge [
    source 20
    target 137
  ]
  edge [
    source 20
    target 138
  ]
  edge [
    source 20
    target 139
  ]
  edge [
    source 20
    target 140
  ]
  edge [
    source 20
    target 141
  ]
  edge [
    source 20
    target 142
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 139
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 143
  ]
  edge [
    source 22
    target 144
  ]
  edge [
    source 22
    target 145
  ]
  edge [
    source 22
    target 146
  ]
  edge [
    source 22
    target 147
  ]
  edge [
    source 22
    target 148
  ]
  edge [
    source 22
    target 149
  ]
  edge [
    source 22
    target 150
  ]
  edge [
    source 22
    target 151
  ]
  edge [
    source 22
    target 152
  ]
  edge [
    source 22
    target 153
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 154
  ]
  edge [
    source 23
    target 155
  ]
  edge [
    source 23
    target 156
  ]
  edge [
    source 23
    target 157
  ]
  edge [
    source 23
    target 49
  ]
  edge [
    source 23
    target 158
  ]
  edge [
    source 23
    target 159
  ]
  edge [
    source 23
    target 160
  ]
  edge [
    source 23
    target 161
  ]
  edge [
    source 23
    target 162
  ]
  edge [
    source 23
    target 163
  ]
  edge [
    source 24
    target 164
  ]
  edge [
    source 24
    target 165
  ]
  edge [
    source 24
    target 166
  ]
  edge [
    source 24
    target 167
  ]
  edge [
    source 24
    target 168
  ]
  edge [
    source 24
    target 169
  ]
  edge [
    source 24
    target 170
  ]
  edge [
    source 24
    target 171
  ]
  edge [
    source 24
    target 172
  ]
  edge [
    source 24
    target 173
  ]
  edge [
    source 24
    target 174
  ]
  edge [
    source 24
    target 175
  ]
  edge [
    source 24
    target 176
  ]
]
