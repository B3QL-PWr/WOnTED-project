graph [
  maxDegree 10
  minDegree 1
  meanDegree 1.8888888888888888
  density 0.1111111111111111
  graphCliqueNumber 2
  node [
    id 0
    label "zawsze"
    origin "text"
  ]
  node [
    id 1
    label "bawi&#263;"
    origin "text"
  ]
  node [
    id 2
    label "tak"
    origin "text"
  ]
  node [
    id 3
    label "samo"
    origin "text"
  ]
  node [
    id 4
    label "thebestofmirko"
    origin "text"
  ]
  node [
    id 5
    label "humorobrazkowy"
    origin "text"
  ]
  node [
    id 6
    label "zaw&#380;dy"
  ]
  node [
    id 7
    label "ci&#261;gle"
  ]
  node [
    id 8
    label "na_zawsze"
  ]
  node [
    id 9
    label "cz&#281;sto"
  ]
  node [
    id 10
    label "ubawia&#263;"
  ]
  node [
    id 11
    label "amuse"
  ]
  node [
    id 12
    label "zajmowa&#263;"
  ]
  node [
    id 13
    label "wzbudza&#263;"
  ]
  node [
    id 14
    label "przebywa&#263;"
  ]
  node [
    id 15
    label "sprawia&#263;"
  ]
  node [
    id 16
    label "zabawia&#263;"
  ]
  node [
    id 17
    label "play"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
]
