graph [
  maxDegree 45
  minDegree 1
  meanDegree 2.388392857142857
  density 0.00534316075423458
  graphCliqueNumber 7
  node [
    id 0
    label "koniec"
    origin "text"
  ]
  node [
    id 1
    label "chcie&#263;byby&#263;"
    origin "text"
  ]
  node [
    id 2
    label "troch&#281;"
    origin "text"
  ]
  node [
    id 3
    label "powiedzie&#263;"
    origin "text"
  ]
  node [
    id 4
    label "konsekwencja"
    origin "text"
  ]
  node [
    id 5
    label "p&#322;aci&#263;"
    origin "text"
  ]
  node [
    id 6
    label "tychy"
    origin "text"
  ]
  node [
    id 7
    label "sk&#322;adka"
    origin "text"
  ]
  node [
    id 8
    label "druga"
    origin "text"
  ]
  node [
    id 9
    label "strona"
    origin "text"
  ]
  node [
    id 10
    label "&#347;wiadczenie"
    origin "text"
  ]
  node [
    id 11
    label "emerytalno"
    origin "text"
  ]
  node [
    id 12
    label "my&#347;le&#263;"
    origin "text"
  ]
  node [
    id 13
    label "bardzo"
    origin "text"
  ]
  node [
    id 14
    label "rzadko"
    origin "text"
  ]
  node [
    id 15
    label "tym"
    origin "text"
  ]
  node [
    id 16
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 17
    label "rocznik"
    origin "text"
  ]
  node [
    id 18
    label "przeci&#281;tna"
    origin "text"
  ]
  node [
    id 19
    label "system"
    origin "text"
  ]
  node [
    id 20
    label "ubezpieczenie"
    origin "text"
  ]
  node [
    id 21
    label "spo&#322;eczny"
    origin "text"
  ]
  node [
    id 22
    label "wynosi&#263;"
    origin "text"
  ]
  node [
    id 23
    label "z&#322;oty"
    origin "text"
  ]
  node [
    id 24
    label "ale"
    origin "text"
  ]
  node [
    id 25
    label "przypadek"
    origin "text"
  ]
  node [
    id 26
    label "osoba"
    origin "text"
  ]
  node [
    id 27
    label "prowadz&#261;ca"
    origin "text"
  ]
  node [
    id 28
    label "dzia&#322;alno&#347;&#263;"
    origin "text"
  ]
  node [
    id 29
    label "gospodarczy"
    origin "text"
  ]
  node [
    id 30
    label "emerytura"
    origin "text"
  ]
  node [
    id 31
    label "renta"
    origin "text"
  ]
  node [
    id 32
    label "tytu&#322;"
    origin "text"
  ]
  node [
    id 33
    label "niezdolno&#347;&#263;"
    origin "text"
  ]
  node [
    id 34
    label "praca"
    origin "text"
  ]
  node [
    id 35
    label "og&#243;lnie"
    origin "text"
  ]
  node [
    id 36
    label "je&#380;eli"
    origin "text"
  ]
  node [
    id 37
    label "popatrze&#263;"
    origin "text"
  ]
  node [
    id 38
    label "jak"
    origin "text"
  ]
  node [
    id 39
    label "wygl&#261;da&#263;"
    origin "text"
  ]
  node [
    id 40
    label "niemal"
    origin "text"
  ]
  node [
    id 41
    label "pobiera&#263;"
    origin "text"
  ]
  node [
    id 42
    label "lub"
    origin "text"
  ]
  node [
    id 43
    label "wysoko&#347;&#263;"
    origin "text"
  ]
  node [
    id 44
    label "poni&#380;ej"
    origin "text"
  ]
  node [
    id 45
    label "te&#380;"
    origin "text"
  ]
  node [
    id 46
    label "musie&#263;"
    origin "text"
  ]
  node [
    id 47
    label "pami&#281;ta&#263;"
    origin "text"
  ]
  node [
    id 48
    label "pyta&#263;"
    origin "text"
  ]
  node [
    id 49
    label "dzi&#281;kowa&#263;"
    origin "text"
  ]
  node [
    id 50
    label "defenestracja"
  ]
  node [
    id 51
    label "szereg"
  ]
  node [
    id 52
    label "dzia&#322;anie"
  ]
  node [
    id 53
    label "miejsce"
  ]
  node [
    id 54
    label "ostatnie_podrygi"
  ]
  node [
    id 55
    label "kres"
  ]
  node [
    id 56
    label "agonia"
  ]
  node [
    id 57
    label "visitation"
  ]
  node [
    id 58
    label "szeol"
  ]
  node [
    id 59
    label "mogi&#322;a"
  ]
  node [
    id 60
    label "chwila"
  ]
  node [
    id 61
    label "wydarzenie"
  ]
  node [
    id 62
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 63
    label "pogrzebanie"
  ]
  node [
    id 64
    label "punkt"
  ]
  node [
    id 65
    label "&#380;a&#322;oba"
  ]
  node [
    id 66
    label "zabicie"
  ]
  node [
    id 67
    label "kres_&#380;ycia"
  ]
  node [
    id 68
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 69
    label "express"
  ]
  node [
    id 70
    label "rzekn&#261;&#263;"
  ]
  node [
    id 71
    label "okre&#347;li&#263;"
  ]
  node [
    id 72
    label "wyrazi&#263;"
  ]
  node [
    id 73
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 74
    label "unwrap"
  ]
  node [
    id 75
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 76
    label "convey"
  ]
  node [
    id 77
    label "discover"
  ]
  node [
    id 78
    label "wydoby&#263;"
  ]
  node [
    id 79
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 80
    label "poda&#263;"
  ]
  node [
    id 81
    label "zwi&#261;zek_przyczynowy"
  ]
  node [
    id 82
    label "skrupia&#263;_si&#281;"
  ]
  node [
    id 83
    label "skrupianie_si&#281;"
  ]
  node [
    id 84
    label "odczuwanie"
  ]
  node [
    id 85
    label "skrupienie_si&#281;"
  ]
  node [
    id 86
    label "skrupi&#263;_si&#281;"
  ]
  node [
    id 87
    label "odczucie"
  ]
  node [
    id 88
    label "odczuwa&#263;"
  ]
  node [
    id 89
    label "odczu&#263;"
  ]
  node [
    id 90
    label "event"
  ]
  node [
    id 91
    label "rezultat"
  ]
  node [
    id 92
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 93
    label "koszula_Dejaniry"
  ]
  node [
    id 94
    label "buli&#263;"
  ]
  node [
    id 95
    label "osi&#261;ga&#263;"
  ]
  node [
    id 96
    label "give"
  ]
  node [
    id 97
    label "wydawa&#263;"
  ]
  node [
    id 98
    label "pay"
  ]
  node [
    id 99
    label "arkusz"
  ]
  node [
    id 100
    label "z&#322;o&#380;y&#263;_si&#281;"
  ]
  node [
    id 101
    label "zbi&#243;rka"
  ]
  node [
    id 102
    label "&#243;semka"
  ]
  node [
    id 103
    label "sk&#322;ada&#263;_si&#281;"
  ]
  node [
    id 104
    label "czw&#243;rka"
  ]
  node [
    id 105
    label "nale&#380;no&#347;&#263;"
  ]
  node [
    id 106
    label "godzina"
  ]
  node [
    id 107
    label "skr&#281;canie"
  ]
  node [
    id 108
    label "voice"
  ]
  node [
    id 109
    label "forma"
  ]
  node [
    id 110
    label "internet"
  ]
  node [
    id 111
    label "skr&#281;ci&#263;"
  ]
  node [
    id 112
    label "kartka"
  ]
  node [
    id 113
    label "orientowa&#263;"
  ]
  node [
    id 114
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 115
    label "powierzchnia"
  ]
  node [
    id 116
    label "plik"
  ]
  node [
    id 117
    label "bok"
  ]
  node [
    id 118
    label "pagina"
  ]
  node [
    id 119
    label "orientowanie"
  ]
  node [
    id 120
    label "fragment"
  ]
  node [
    id 121
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 122
    label "s&#261;d"
  ]
  node [
    id 123
    label "skr&#281;ca&#263;"
  ]
  node [
    id 124
    label "g&#243;ra"
  ]
  node [
    id 125
    label "serwis_internetowy"
  ]
  node [
    id 126
    label "orientacja"
  ]
  node [
    id 127
    label "linia"
  ]
  node [
    id 128
    label "skr&#281;cenie"
  ]
  node [
    id 129
    label "layout"
  ]
  node [
    id 130
    label "zorientowa&#263;"
  ]
  node [
    id 131
    label "zorientowanie"
  ]
  node [
    id 132
    label "obiekt"
  ]
  node [
    id 133
    label "podmiot"
  ]
  node [
    id 134
    label "ty&#322;"
  ]
  node [
    id 135
    label "logowanie"
  ]
  node [
    id 136
    label "adres_internetowy"
  ]
  node [
    id 137
    label "uj&#281;cie"
  ]
  node [
    id 138
    label "prz&#243;d"
  ]
  node [
    id 139
    label "posta&#263;"
  ]
  node [
    id 140
    label "p&#322;acenie"
  ]
  node [
    id 141
    label "znaczenie"
  ]
  node [
    id 142
    label "sk&#322;adanie"
  ]
  node [
    id 143
    label "service"
  ]
  node [
    id 144
    label "czynienie_dobra"
  ]
  node [
    id 145
    label "informowanie"
  ]
  node [
    id 146
    label "command"
  ]
  node [
    id 147
    label "opowiadanie"
  ]
  node [
    id 148
    label "koszt_rodzajowy"
  ]
  node [
    id 149
    label "pracowanie"
  ]
  node [
    id 150
    label "przekonywanie"
  ]
  node [
    id 151
    label "wyraz"
  ]
  node [
    id 152
    label "us&#322;uga"
  ]
  node [
    id 153
    label "performance"
  ]
  node [
    id 154
    label "zobowi&#261;zanie"
  ]
  node [
    id 155
    label "take_care"
  ]
  node [
    id 156
    label "troska&#263;_si&#281;"
  ]
  node [
    id 157
    label "zamierza&#263;"
  ]
  node [
    id 158
    label "os&#261;dza&#263;"
  ]
  node [
    id 159
    label "robi&#263;"
  ]
  node [
    id 160
    label "argue"
  ]
  node [
    id 161
    label "rozpatrywa&#263;"
  ]
  node [
    id 162
    label "deliver"
  ]
  node [
    id 163
    label "w_chuj"
  ]
  node [
    id 164
    label "czasami"
  ]
  node [
    id 165
    label "rzadki"
  ]
  node [
    id 166
    label "niezwykle"
  ]
  node [
    id 167
    label "lu&#378;ny"
  ]
  node [
    id 168
    label "thinly"
  ]
  node [
    id 169
    label "remark"
  ]
  node [
    id 170
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 171
    label "u&#380;ywa&#263;"
  ]
  node [
    id 172
    label "okre&#347;la&#263;"
  ]
  node [
    id 173
    label "j&#281;zyk"
  ]
  node [
    id 174
    label "say"
  ]
  node [
    id 175
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 176
    label "formu&#322;owa&#263;"
  ]
  node [
    id 177
    label "talk"
  ]
  node [
    id 178
    label "powiada&#263;"
  ]
  node [
    id 179
    label "informowa&#263;"
  ]
  node [
    id 180
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 181
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 182
    label "wydobywa&#263;"
  ]
  node [
    id 183
    label "chew_the_fat"
  ]
  node [
    id 184
    label "dysfonia"
  ]
  node [
    id 185
    label "umie&#263;"
  ]
  node [
    id 186
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 187
    label "tell"
  ]
  node [
    id 188
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 189
    label "wyra&#380;a&#263;"
  ]
  node [
    id 190
    label "gaworzy&#263;"
  ]
  node [
    id 191
    label "rozmawia&#263;"
  ]
  node [
    id 192
    label "dziama&#263;"
  ]
  node [
    id 193
    label "prawi&#263;"
  ]
  node [
    id 194
    label "formacja"
  ]
  node [
    id 195
    label "kronika"
  ]
  node [
    id 196
    label "czasopismo"
  ]
  node [
    id 197
    label "yearbook"
  ]
  node [
    id 198
    label "warto&#347;&#263;"
  ]
  node [
    id 199
    label "model"
  ]
  node [
    id 200
    label "sk&#322;ad"
  ]
  node [
    id 201
    label "zachowanie"
  ]
  node [
    id 202
    label "podstawa"
  ]
  node [
    id 203
    label "porz&#261;dek"
  ]
  node [
    id 204
    label "Android"
  ]
  node [
    id 205
    label "przyn&#281;ta"
  ]
  node [
    id 206
    label "jednostka_geologiczna"
  ]
  node [
    id 207
    label "metoda"
  ]
  node [
    id 208
    label "podsystem"
  ]
  node [
    id 209
    label "p&#322;&#243;d"
  ]
  node [
    id 210
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 211
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 212
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 213
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 214
    label "j&#261;dro"
  ]
  node [
    id 215
    label "eratem"
  ]
  node [
    id 216
    label "ryba"
  ]
  node [
    id 217
    label "pulpit"
  ]
  node [
    id 218
    label "struktura"
  ]
  node [
    id 219
    label "spos&#243;b"
  ]
  node [
    id 220
    label "oddzia&#322;"
  ]
  node [
    id 221
    label "usenet"
  ]
  node [
    id 222
    label "o&#347;"
  ]
  node [
    id 223
    label "oprogramowanie"
  ]
  node [
    id 224
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 225
    label "poj&#281;cie"
  ]
  node [
    id 226
    label "w&#281;dkarstwo"
  ]
  node [
    id 227
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 228
    label "Leopard"
  ]
  node [
    id 229
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 230
    label "systemik"
  ]
  node [
    id 231
    label "rozprz&#261;c"
  ]
  node [
    id 232
    label "cybernetyk"
  ]
  node [
    id 233
    label "konstelacja"
  ]
  node [
    id 234
    label "doktryna"
  ]
  node [
    id 235
    label "net"
  ]
  node [
    id 236
    label "zbi&#243;r"
  ]
  node [
    id 237
    label "method"
  ]
  node [
    id 238
    label "systemat"
  ]
  node [
    id 239
    label "ochrona"
  ]
  node [
    id 240
    label "cover"
  ]
  node [
    id 241
    label "franszyza"
  ]
  node [
    id 242
    label "ryzyko_niewygas&#322;e"
  ]
  node [
    id 243
    label "suma_ubezpieczenia"
  ]
  node [
    id 244
    label "screen"
  ]
  node [
    id 245
    label "przyznanie"
  ]
  node [
    id 246
    label "umowa"
  ]
  node [
    id 247
    label "op&#322;ata"
  ]
  node [
    id 248
    label "uchronienie"
  ]
  node [
    id 249
    label "uprz&#261;&#380;_wspinaczkowa"
  ]
  node [
    id 250
    label "zapewnienie"
  ]
  node [
    id 251
    label "ubezpieczalnia"
  ]
  node [
    id 252
    label "bezpiecze&#324;stwo"
  ]
  node [
    id 253
    label "insurance"
  ]
  node [
    id 254
    label "niepubliczny"
  ]
  node [
    id 255
    label "spo&#322;ecznie"
  ]
  node [
    id 256
    label "publiczny"
  ]
  node [
    id 257
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 258
    label "odsuwa&#263;"
  ]
  node [
    id 259
    label "zanosi&#263;"
  ]
  node [
    id 260
    label "otrzymywa&#263;"
  ]
  node [
    id 261
    label "nasi&#261;ka&#263;"
  ]
  node [
    id 262
    label "rozpowszechnia&#263;"
  ]
  node [
    id 263
    label "ujawnia&#263;"
  ]
  node [
    id 264
    label "kra&#347;&#263;"
  ]
  node [
    id 265
    label "kwota"
  ]
  node [
    id 266
    label "liczy&#263;"
  ]
  node [
    id 267
    label "raise"
  ]
  node [
    id 268
    label "podnosi&#263;"
  ]
  node [
    id 269
    label "szlachetny"
  ]
  node [
    id 270
    label "metaliczny"
  ]
  node [
    id 271
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 272
    label "z&#322;ocenie"
  ]
  node [
    id 273
    label "grosz"
  ]
  node [
    id 274
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 275
    label "utytu&#322;owany"
  ]
  node [
    id 276
    label "poz&#322;ocenie"
  ]
  node [
    id 277
    label "Polska"
  ]
  node [
    id 278
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 279
    label "wspania&#322;y"
  ]
  node [
    id 280
    label "doskona&#322;y"
  ]
  node [
    id 281
    label "kochany"
  ]
  node [
    id 282
    label "jednostka_monetarna"
  ]
  node [
    id 283
    label "z&#322;oci&#347;cie"
  ]
  node [
    id 284
    label "piwo"
  ]
  node [
    id 285
    label "pacjent"
  ]
  node [
    id 286
    label "kategoria_gramatyczna"
  ]
  node [
    id 287
    label "schorzenie"
  ]
  node [
    id 288
    label "przeznaczenie"
  ]
  node [
    id 289
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 290
    label "happening"
  ]
  node [
    id 291
    label "przyk&#322;ad"
  ]
  node [
    id 292
    label "Zgredek"
  ]
  node [
    id 293
    label "Casanova"
  ]
  node [
    id 294
    label "Don_Juan"
  ]
  node [
    id 295
    label "Gargantua"
  ]
  node [
    id 296
    label "Faust"
  ]
  node [
    id 297
    label "profanum"
  ]
  node [
    id 298
    label "Chocho&#322;"
  ]
  node [
    id 299
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 300
    label "koniugacja"
  ]
  node [
    id 301
    label "Winnetou"
  ]
  node [
    id 302
    label "Dwukwiat"
  ]
  node [
    id 303
    label "homo_sapiens"
  ]
  node [
    id 304
    label "Edyp"
  ]
  node [
    id 305
    label "Herkules_Poirot"
  ]
  node [
    id 306
    label "ludzko&#347;&#263;"
  ]
  node [
    id 307
    label "mikrokosmos"
  ]
  node [
    id 308
    label "person"
  ]
  node [
    id 309
    label "Sherlock_Holmes"
  ]
  node [
    id 310
    label "portrecista"
  ]
  node [
    id 311
    label "Szwejk"
  ]
  node [
    id 312
    label "Hamlet"
  ]
  node [
    id 313
    label "duch"
  ]
  node [
    id 314
    label "g&#322;owa"
  ]
  node [
    id 315
    label "oddzia&#322;ywanie"
  ]
  node [
    id 316
    label "Quasimodo"
  ]
  node [
    id 317
    label "Dulcynea"
  ]
  node [
    id 318
    label "Don_Kiszot"
  ]
  node [
    id 319
    label "Wallenrod"
  ]
  node [
    id 320
    label "Plastu&#347;"
  ]
  node [
    id 321
    label "Harry_Potter"
  ]
  node [
    id 322
    label "figura"
  ]
  node [
    id 323
    label "parali&#380;owa&#263;"
  ]
  node [
    id 324
    label "istota"
  ]
  node [
    id 325
    label "Werter"
  ]
  node [
    id 326
    label "antropochoria"
  ]
  node [
    id 327
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 328
    label "absolutorium"
  ]
  node [
    id 329
    label "activity"
  ]
  node [
    id 330
    label "gospodarski"
  ]
  node [
    id 331
    label "czas"
  ]
  node [
    id 332
    label "ubezpieczenie_emerytalne"
  ]
  node [
    id 333
    label "&#347;wiadczenie_spo&#322;eczne"
  ]
  node [
    id 334
    label "egzystencja"
  ]
  node [
    id 335
    label "retirement"
  ]
  node [
    id 336
    label "economic_rent"
  ]
  node [
    id 337
    label "doch&#243;d"
  ]
  node [
    id 338
    label "podtytu&#322;"
  ]
  node [
    id 339
    label "debit"
  ]
  node [
    id 340
    label "szata_graficzna"
  ]
  node [
    id 341
    label "elevation"
  ]
  node [
    id 342
    label "wyda&#263;"
  ]
  node [
    id 343
    label "nadtytu&#322;"
  ]
  node [
    id 344
    label "tytulatura"
  ]
  node [
    id 345
    label "nazwa"
  ]
  node [
    id 346
    label "redaktor"
  ]
  node [
    id 347
    label "druk"
  ]
  node [
    id 348
    label "mianowaniec"
  ]
  node [
    id 349
    label "poster"
  ]
  node [
    id 350
    label "publikacja"
  ]
  node [
    id 351
    label "inability"
  ]
  node [
    id 352
    label "cecha"
  ]
  node [
    id 353
    label "stan"
  ]
  node [
    id 354
    label "brak"
  ]
  node [
    id 355
    label "stosunek_pracy"
  ]
  node [
    id 356
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 357
    label "benedykty&#324;ski"
  ]
  node [
    id 358
    label "zaw&#243;d"
  ]
  node [
    id 359
    label "kierownictwo"
  ]
  node [
    id 360
    label "zmiana"
  ]
  node [
    id 361
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 362
    label "wytw&#243;r"
  ]
  node [
    id 363
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 364
    label "tynkarski"
  ]
  node [
    id 365
    label "czynnik_produkcji"
  ]
  node [
    id 366
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 367
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 368
    label "czynno&#347;&#263;"
  ]
  node [
    id 369
    label "tyrka"
  ]
  node [
    id 370
    label "pracowa&#263;"
  ]
  node [
    id 371
    label "siedziba"
  ]
  node [
    id 372
    label "poda&#380;_pracy"
  ]
  node [
    id 373
    label "zak&#322;ad"
  ]
  node [
    id 374
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 375
    label "najem"
  ]
  node [
    id 376
    label "zbiorowo"
  ]
  node [
    id 377
    label "nadrz&#281;dnie"
  ]
  node [
    id 378
    label "generalny"
  ]
  node [
    id 379
    label "og&#243;lny"
  ]
  node [
    id 380
    label "posp&#243;lnie"
  ]
  node [
    id 381
    label "&#322;&#261;cznie"
  ]
  node [
    id 382
    label "visualize"
  ]
  node [
    id 383
    label "sta&#263;_si&#281;"
  ]
  node [
    id 384
    label "zrobi&#263;"
  ]
  node [
    id 385
    label "byd&#322;o"
  ]
  node [
    id 386
    label "zobo"
  ]
  node [
    id 387
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 388
    label "yakalo"
  ]
  node [
    id 389
    label "dzo"
  ]
  node [
    id 390
    label "by&#263;"
  ]
  node [
    id 391
    label "czeka&#263;"
  ]
  node [
    id 392
    label "lookout"
  ]
  node [
    id 393
    label "wyziera&#263;"
  ]
  node [
    id 394
    label "peep"
  ]
  node [
    id 395
    label "look"
  ]
  node [
    id 396
    label "patrze&#263;"
  ]
  node [
    id 397
    label "wycina&#263;"
  ]
  node [
    id 398
    label "open"
  ]
  node [
    id 399
    label "arise"
  ]
  node [
    id 400
    label "kopiowa&#263;"
  ]
  node [
    id 401
    label "wch&#322;ania&#263;"
  ]
  node [
    id 402
    label "bra&#263;"
  ]
  node [
    id 403
    label "pr&#243;bka"
  ]
  node [
    id 404
    label "tallness"
  ]
  node [
    id 405
    label "sum"
  ]
  node [
    id 406
    label "degree"
  ]
  node [
    id 407
    label "brzmienie"
  ]
  node [
    id 408
    label "altitude"
  ]
  node [
    id 409
    label "odcinek"
  ]
  node [
    id 410
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 411
    label "rozmiar"
  ]
  node [
    id 412
    label "cz&#281;stotliwo&#347;&#263;"
  ]
  node [
    id 413
    label "k&#261;t"
  ]
  node [
    id 414
    label "wielko&#347;&#263;"
  ]
  node [
    id 415
    label "nast&#281;pnie"
  ]
  node [
    id 416
    label "poni&#380;szy"
  ]
  node [
    id 417
    label "need"
  ]
  node [
    id 418
    label "pragn&#261;&#263;"
  ]
  node [
    id 419
    label "si&#281;ga&#263;"
  ]
  node [
    id 420
    label "zna&#263;"
  ]
  node [
    id 421
    label "zachowywa&#263;"
  ]
  node [
    id 422
    label "chowa&#263;"
  ]
  node [
    id 423
    label "think"
  ]
  node [
    id 424
    label "pilnowa&#263;"
  ]
  node [
    id 425
    label "recall"
  ]
  node [
    id 426
    label "echo"
  ]
  node [
    id 427
    label "inspect"
  ]
  node [
    id 428
    label "przes&#322;uchiwa&#263;"
  ]
  node [
    id 429
    label "ankieter"
  ]
  node [
    id 430
    label "sprawdza&#263;"
  ]
  node [
    id 431
    label "question"
  ]
  node [
    id 432
    label "odmawia&#263;"
  ]
  node [
    id 433
    label "wycofywa&#263;_si&#281;"
  ]
  node [
    id 434
    label "sk&#322;ada&#263;"
  ]
  node [
    id 435
    label "thank"
  ]
  node [
    id 436
    label "odwdzi&#281;cza&#263;_si&#281;"
  ]
  node [
    id 437
    label "etykieta"
  ]
  node [
    id 438
    label "Jaros&#322;awa"
  ]
  node [
    id 439
    label "Kalinowski"
  ]
  node [
    id 440
    label "Lidia"
  ]
  node [
    id 441
    label "Staro&#324;"
  ]
  node [
    id 442
    label "Agnieszka"
  ]
  node [
    id 443
    label "ch&#322;on&#261;&#263;"
  ]
  node [
    id 444
    label "Domi&#324;czak"
  ]
  node [
    id 445
    label "ministerstwo"
  ]
  node [
    id 446
    label "i"
  ]
  node [
    id 447
    label "polityka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 20
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 26
  ]
  edge [
    source 7
    target 34
  ]
  edge [
    source 7
    target 27
  ]
  edge [
    source 7
    target 28
  ]
  edge [
    source 7
    target 23
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 9
    target 116
  ]
  edge [
    source 9
    target 117
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 9
    target 119
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 129
  ]
  edge [
    source 9
    target 130
  ]
  edge [
    source 9
    target 131
  ]
  edge [
    source 9
    target 132
  ]
  edge [
    source 9
    target 133
  ]
  edge [
    source 9
    target 134
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 135
  ]
  edge [
    source 9
    target 136
  ]
  edge [
    source 9
    target 137
  ]
  edge [
    source 9
    target 138
  ]
  edge [
    source 9
    target 139
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 18
  ]
  edge [
    source 10
    target 19
  ]
  edge [
    source 10
    target 140
  ]
  edge [
    source 10
    target 141
  ]
  edge [
    source 10
    target 142
  ]
  edge [
    source 10
    target 143
  ]
  edge [
    source 10
    target 144
  ]
  edge [
    source 10
    target 145
  ]
  edge [
    source 10
    target 146
  ]
  edge [
    source 10
    target 147
  ]
  edge [
    source 10
    target 148
  ]
  edge [
    source 10
    target 149
  ]
  edge [
    source 10
    target 150
  ]
  edge [
    source 10
    target 151
  ]
  edge [
    source 10
    target 152
  ]
  edge [
    source 10
    target 153
  ]
  edge [
    source 10
    target 154
  ]
  edge [
    source 10
    target 20
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 23
  ]
  edge [
    source 12
    target 15
  ]
  edge [
    source 12
    target 155
  ]
  edge [
    source 12
    target 156
  ]
  edge [
    source 12
    target 157
  ]
  edge [
    source 12
    target 158
  ]
  edge [
    source 12
    target 159
  ]
  edge [
    source 12
    target 160
  ]
  edge [
    source 12
    target 161
  ]
  edge [
    source 12
    target 162
  ]
  edge [
    source 12
    target 20
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 49
  ]
  edge [
    source 13
    target 163
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 164
  ]
  edge [
    source 14
    target 165
  ]
  edge [
    source 14
    target 166
  ]
  edge [
    source 14
    target 167
  ]
  edge [
    source 14
    target 168
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 45
  ]
  edge [
    source 16
    target 169
  ]
  edge [
    source 16
    target 170
  ]
  edge [
    source 16
    target 171
  ]
  edge [
    source 16
    target 172
  ]
  edge [
    source 16
    target 173
  ]
  edge [
    source 16
    target 174
  ]
  edge [
    source 16
    target 175
  ]
  edge [
    source 16
    target 176
  ]
  edge [
    source 16
    target 177
  ]
  edge [
    source 16
    target 178
  ]
  edge [
    source 16
    target 179
  ]
  edge [
    source 16
    target 180
  ]
  edge [
    source 16
    target 181
  ]
  edge [
    source 16
    target 182
  ]
  edge [
    source 16
    target 69
  ]
  edge [
    source 16
    target 183
  ]
  edge [
    source 16
    target 184
  ]
  edge [
    source 16
    target 185
  ]
  edge [
    source 16
    target 186
  ]
  edge [
    source 16
    target 187
  ]
  edge [
    source 16
    target 188
  ]
  edge [
    source 16
    target 189
  ]
  edge [
    source 16
    target 190
  ]
  edge [
    source 16
    target 191
  ]
  edge [
    source 16
    target 192
  ]
  edge [
    source 16
    target 193
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 21
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 194
  ]
  edge [
    source 17
    target 195
  ]
  edge [
    source 17
    target 196
  ]
  edge [
    source 17
    target 197
  ]
  edge [
    source 17
    target 21
  ]
  edge [
    source 18
    target 23
  ]
  edge [
    source 18
    target 30
  ]
  edge [
    source 18
    target 198
  ]
  edge [
    source 18
    target 32
  ]
  edge [
    source 18
    target 36
  ]
  edge [
    source 18
    target 31
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 199
  ]
  edge [
    source 19
    target 200
  ]
  edge [
    source 19
    target 201
  ]
  edge [
    source 19
    target 202
  ]
  edge [
    source 19
    target 203
  ]
  edge [
    source 19
    target 204
  ]
  edge [
    source 19
    target 205
  ]
  edge [
    source 19
    target 206
  ]
  edge [
    source 19
    target 207
  ]
  edge [
    source 19
    target 208
  ]
  edge [
    source 19
    target 209
  ]
  edge [
    source 19
    target 210
  ]
  edge [
    source 19
    target 122
  ]
  edge [
    source 19
    target 211
  ]
  edge [
    source 19
    target 212
  ]
  edge [
    source 19
    target 213
  ]
  edge [
    source 19
    target 214
  ]
  edge [
    source 19
    target 215
  ]
  edge [
    source 19
    target 216
  ]
  edge [
    source 19
    target 217
  ]
  edge [
    source 19
    target 218
  ]
  edge [
    source 19
    target 219
  ]
  edge [
    source 19
    target 220
  ]
  edge [
    source 19
    target 221
  ]
  edge [
    source 19
    target 222
  ]
  edge [
    source 19
    target 223
  ]
  edge [
    source 19
    target 224
  ]
  edge [
    source 19
    target 225
  ]
  edge [
    source 19
    target 226
  ]
  edge [
    source 19
    target 227
  ]
  edge [
    source 19
    target 228
  ]
  edge [
    source 19
    target 229
  ]
  edge [
    source 19
    target 230
  ]
  edge [
    source 19
    target 231
  ]
  edge [
    source 19
    target 232
  ]
  edge [
    source 19
    target 233
  ]
  edge [
    source 19
    target 234
  ]
  edge [
    source 19
    target 235
  ]
  edge [
    source 19
    target 236
  ]
  edge [
    source 19
    target 237
  ]
  edge [
    source 19
    target 238
  ]
  edge [
    source 19
    target 25
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 239
  ]
  edge [
    source 20
    target 240
  ]
  edge [
    source 20
    target 241
  ]
  edge [
    source 20
    target 242
  ]
  edge [
    source 20
    target 243
  ]
  edge [
    source 20
    target 244
  ]
  edge [
    source 20
    target 245
  ]
  edge [
    source 20
    target 246
  ]
  edge [
    source 20
    target 247
  ]
  edge [
    source 20
    target 248
  ]
  edge [
    source 20
    target 249
  ]
  edge [
    source 20
    target 250
  ]
  edge [
    source 20
    target 251
  ]
  edge [
    source 20
    target 252
  ]
  edge [
    source 20
    target 253
  ]
  edge [
    source 20
    target 220
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 49
  ]
  edge [
    source 21
    target 254
  ]
  edge [
    source 21
    target 255
  ]
  edge [
    source 21
    target 256
  ]
  edge [
    source 21
    target 445
  ]
  edge [
    source 21
    target 34
  ]
  edge [
    source 21
    target 446
  ]
  edge [
    source 21
    target 447
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 257
  ]
  edge [
    source 22
    target 258
  ]
  edge [
    source 22
    target 259
  ]
  edge [
    source 22
    target 260
  ]
  edge [
    source 22
    target 261
  ]
  edge [
    source 22
    target 262
  ]
  edge [
    source 22
    target 263
  ]
  edge [
    source 22
    target 264
  ]
  edge [
    source 22
    target 265
  ]
  edge [
    source 22
    target 266
  ]
  edge [
    source 22
    target 267
  ]
  edge [
    source 22
    target 268
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 29
  ]
  edge [
    source 23
    target 30
  ]
  edge [
    source 23
    target 25
  ]
  edge [
    source 23
    target 34
  ]
  edge [
    source 23
    target 44
  ]
  edge [
    source 23
    target 269
  ]
  edge [
    source 23
    target 270
  ]
  edge [
    source 23
    target 271
  ]
  edge [
    source 23
    target 272
  ]
  edge [
    source 23
    target 273
  ]
  edge [
    source 23
    target 274
  ]
  edge [
    source 23
    target 275
  ]
  edge [
    source 23
    target 276
  ]
  edge [
    source 23
    target 277
  ]
  edge [
    source 23
    target 278
  ]
  edge [
    source 23
    target 279
  ]
  edge [
    source 23
    target 280
  ]
  edge [
    source 23
    target 281
  ]
  edge [
    source 23
    target 282
  ]
  edge [
    source 23
    target 283
  ]
  edge [
    source 23
    target 26
  ]
  edge [
    source 23
    target 31
  ]
  edge [
    source 23
    target 35
  ]
  edge [
    source 23
    target 27
  ]
  edge [
    source 23
    target 28
  ]
  edge [
    source 23
    target 46
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 284
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 285
  ]
  edge [
    source 25
    target 286
  ]
  edge [
    source 25
    target 287
  ]
  edge [
    source 25
    target 288
  ]
  edge [
    source 25
    target 289
  ]
  edge [
    source 25
    target 61
  ]
  edge [
    source 25
    target 290
  ]
  edge [
    source 25
    target 291
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 40
  ]
  edge [
    source 26
    target 292
  ]
  edge [
    source 26
    target 286
  ]
  edge [
    source 26
    target 293
  ]
  edge [
    source 26
    target 294
  ]
  edge [
    source 26
    target 295
  ]
  edge [
    source 26
    target 296
  ]
  edge [
    source 26
    target 297
  ]
  edge [
    source 26
    target 298
  ]
  edge [
    source 26
    target 299
  ]
  edge [
    source 26
    target 300
  ]
  edge [
    source 26
    target 301
  ]
  edge [
    source 26
    target 302
  ]
  edge [
    source 26
    target 303
  ]
  edge [
    source 26
    target 304
  ]
  edge [
    source 26
    target 305
  ]
  edge [
    source 26
    target 306
  ]
  edge [
    source 26
    target 307
  ]
  edge [
    source 26
    target 308
  ]
  edge [
    source 26
    target 309
  ]
  edge [
    source 26
    target 310
  ]
  edge [
    source 26
    target 311
  ]
  edge [
    source 26
    target 312
  ]
  edge [
    source 26
    target 313
  ]
  edge [
    source 26
    target 314
  ]
  edge [
    source 26
    target 315
  ]
  edge [
    source 26
    target 316
  ]
  edge [
    source 26
    target 317
  ]
  edge [
    source 26
    target 318
  ]
  edge [
    source 26
    target 319
  ]
  edge [
    source 26
    target 320
  ]
  edge [
    source 26
    target 321
  ]
  edge [
    source 26
    target 322
  ]
  edge [
    source 26
    target 323
  ]
  edge [
    source 26
    target 324
  ]
  edge [
    source 26
    target 325
  ]
  edge [
    source 26
    target 326
  ]
  edge [
    source 26
    target 139
  ]
  edge [
    source 26
    target 34
  ]
  edge [
    source 26
    target 28
  ]
  edge [
    source 26
    target 46
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 27
  ]
  edge [
    source 27
    target 34
  ]
  edge [
    source 27
    target 46
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 52
  ]
  edge [
    source 28
    target 327
  ]
  edge [
    source 28
    target 328
  ]
  edge [
    source 28
    target 329
  ]
  edge [
    source 28
    target 28
  ]
  edge [
    source 28
    target 34
  ]
  edge [
    source 28
    target 46
  ]
  edge [
    source 29
    target 31
  ]
  edge [
    source 29
    target 35
  ]
  edge [
    source 29
    target 41
  ]
  edge [
    source 29
    target 330
  ]
  edge [
    source 29
    target 29
  ]
  edge [
    source 30
    target 39
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 41
  ]
  edge [
    source 30
    target 42
  ]
  edge [
    source 30
    target 331
  ]
  edge [
    source 30
    target 332
  ]
  edge [
    source 30
    target 333
  ]
  edge [
    source 30
    target 334
  ]
  edge [
    source 30
    target 335
  ]
  edge [
    source 30
    target 33
  ]
  edge [
    source 30
    target 37
  ]
  edge [
    source 30
    target 43
  ]
  edge [
    source 30
    target 35
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 40
  ]
  edge [
    source 31
    target 42
  ]
  edge [
    source 31
    target 43
  ]
  edge [
    source 31
    target 336
  ]
  edge [
    source 31
    target 337
  ]
  edge [
    source 31
    target 333
  ]
  edge [
    source 31
    target 35
  ]
  edge [
    source 31
    target 36
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 338
  ]
  edge [
    source 32
    target 339
  ]
  edge [
    source 32
    target 340
  ]
  edge [
    source 32
    target 341
  ]
  edge [
    source 32
    target 342
  ]
  edge [
    source 32
    target 343
  ]
  edge [
    source 32
    target 344
  ]
  edge [
    source 32
    target 345
  ]
  edge [
    source 32
    target 346
  ]
  edge [
    source 32
    target 97
  ]
  edge [
    source 32
    target 347
  ]
  edge [
    source 32
    target 348
  ]
  edge [
    source 32
    target 349
  ]
  edge [
    source 32
    target 350
  ]
  edge [
    source 32
    target 36
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 351
  ]
  edge [
    source 33
    target 352
  ]
  edge [
    source 33
    target 353
  ]
  edge [
    source 33
    target 354
  ]
  edge [
    source 33
    target 37
  ]
  edge [
    source 33
    target 43
  ]
  edge [
    source 34
    target 355
  ]
  edge [
    source 34
    target 356
  ]
  edge [
    source 34
    target 357
  ]
  edge [
    source 34
    target 149
  ]
  edge [
    source 34
    target 358
  ]
  edge [
    source 34
    target 359
  ]
  edge [
    source 34
    target 360
  ]
  edge [
    source 34
    target 361
  ]
  edge [
    source 34
    target 362
  ]
  edge [
    source 34
    target 363
  ]
  edge [
    source 34
    target 364
  ]
  edge [
    source 34
    target 365
  ]
  edge [
    source 34
    target 366
  ]
  edge [
    source 34
    target 154
  ]
  edge [
    source 34
    target 367
  ]
  edge [
    source 34
    target 368
  ]
  edge [
    source 34
    target 369
  ]
  edge [
    source 34
    target 370
  ]
  edge [
    source 34
    target 371
  ]
  edge [
    source 34
    target 372
  ]
  edge [
    source 34
    target 53
  ]
  edge [
    source 34
    target 373
  ]
  edge [
    source 34
    target 374
  ]
  edge [
    source 34
    target 375
  ]
  edge [
    source 34
    target 46
  ]
  edge [
    source 34
    target 445
  ]
  edge [
    source 34
    target 446
  ]
  edge [
    source 34
    target 447
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 376
  ]
  edge [
    source 35
    target 377
  ]
  edge [
    source 35
    target 378
  ]
  edge [
    source 35
    target 379
  ]
  edge [
    source 35
    target 380
  ]
  edge [
    source 35
    target 381
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 382
  ]
  edge [
    source 37
    target 383
  ]
  edge [
    source 37
    target 384
  ]
  edge [
    source 37
    target 43
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 385
  ]
  edge [
    source 38
    target 386
  ]
  edge [
    source 38
    target 387
  ]
  edge [
    source 38
    target 388
  ]
  edge [
    source 38
    target 389
  ]
  edge [
    source 39
    target 390
  ]
  edge [
    source 39
    target 391
  ]
  edge [
    source 39
    target 392
  ]
  edge [
    source 39
    target 393
  ]
  edge [
    source 39
    target 394
  ]
  edge [
    source 39
    target 395
  ]
  edge [
    source 39
    target 396
  ]
  edge [
    source 41
    target 397
  ]
  edge [
    source 41
    target 398
  ]
  edge [
    source 41
    target 260
  ]
  edge [
    source 41
    target 399
  ]
  edge [
    source 41
    target 400
  ]
  edge [
    source 41
    target 267
  ]
  edge [
    source 41
    target 401
  ]
  edge [
    source 41
    target 402
  ]
  edge [
    source 41
    target 403
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 404
  ]
  edge [
    source 43
    target 405
  ]
  edge [
    source 43
    target 406
  ]
  edge [
    source 43
    target 407
  ]
  edge [
    source 43
    target 408
  ]
  edge [
    source 43
    target 409
  ]
  edge [
    source 43
    target 410
  ]
  edge [
    source 43
    target 411
  ]
  edge [
    source 43
    target 412
  ]
  edge [
    source 43
    target 413
  ]
  edge [
    source 43
    target 414
  ]
  edge [
    source 44
    target 415
  ]
  edge [
    source 44
    target 416
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 417
  ]
  edge [
    source 46
    target 418
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 419
  ]
  edge [
    source 47
    target 420
  ]
  edge [
    source 47
    target 156
  ]
  edge [
    source 47
    target 421
  ]
  edge [
    source 47
    target 422
  ]
  edge [
    source 47
    target 423
  ]
  edge [
    source 47
    target 424
  ]
  edge [
    source 47
    target 159
  ]
  edge [
    source 47
    target 425
  ]
  edge [
    source 47
    target 426
  ]
  edge [
    source 47
    target 155
  ]
  edge [
    source 48
    target 427
  ]
  edge [
    source 48
    target 428
  ]
  edge [
    source 48
    target 170
  ]
  edge [
    source 48
    target 429
  ]
  edge [
    source 48
    target 430
  ]
  edge [
    source 48
    target 431
  ]
  edge [
    source 49
    target 432
  ]
  edge [
    source 49
    target 433
  ]
  edge [
    source 49
    target 434
  ]
  edge [
    source 49
    target 435
  ]
  edge [
    source 49
    target 436
  ]
  edge [
    source 49
    target 189
  ]
  edge [
    source 49
    target 437
  ]
  edge [
    source 438
    target 439
  ]
  edge [
    source 440
    target 441
  ]
  edge [
    source 442
    target 443
  ]
  edge [
    source 442
    target 444
  ]
  edge [
    source 443
    target 444
  ]
  edge [
    source 445
    target 446
  ]
  edge [
    source 445
    target 447
  ]
  edge [
    source 446
    target 447
  ]
]
