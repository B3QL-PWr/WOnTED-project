graph [
  maxDegree 12
  minDegree 1
  meanDegree 2.0588235294117645
  density 0.062388591800356503
  graphCliqueNumber 2
  node [
    id 0
    label "nostalgia"
    origin "text"
  ]
  node [
    id 1
    label "dopa&#347;&#263;"
    origin "text"
  ]
  node [
    id 2
    label "sklep"
    origin "text"
  ]
  node [
    id 3
    label "musie&#263;by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "kupi&#263;"
    origin "text"
  ]
  node [
    id 5
    label "homesickness"
  ]
  node [
    id 6
    label "t&#281;sknota"
  ]
  node [
    id 7
    label "smutek"
  ]
  node [
    id 8
    label "wspominki"
  ]
  node [
    id 9
    label "chwyci&#263;"
  ]
  node [
    id 10
    label "rzuci&#263;_si&#281;"
  ]
  node [
    id 11
    label "dotrze&#263;"
  ]
  node [
    id 12
    label "dorwa&#263;"
  ]
  node [
    id 13
    label "zaatakowa&#263;"
  ]
  node [
    id 14
    label "dobra&#263;_si&#281;"
  ]
  node [
    id 15
    label "fall"
  ]
  node [
    id 16
    label "stoisko"
  ]
  node [
    id 17
    label "sk&#322;ad"
  ]
  node [
    id 18
    label "firma"
  ]
  node [
    id 19
    label "lokal_u&#380;ytkowy"
  ]
  node [
    id 20
    label "witryna"
  ]
  node [
    id 21
    label "obiekt_handlowy"
  ]
  node [
    id 22
    label "zaplecze"
  ]
  node [
    id 23
    label "p&#243;&#322;ka"
  ]
  node [
    id 24
    label "get"
  ]
  node [
    id 25
    label "wzi&#261;&#263;"
  ]
  node [
    id 26
    label "catch"
  ]
  node [
    id 27
    label "przyj&#261;&#263;"
  ]
  node [
    id 28
    label "beget"
  ]
  node [
    id 29
    label "pozyska&#263;"
  ]
  node [
    id 30
    label "ustawi&#263;"
  ]
  node [
    id 31
    label "uzna&#263;"
  ]
  node [
    id 32
    label "zagra&#263;"
  ]
  node [
    id 33
    label "uwierzy&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 0
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
]
