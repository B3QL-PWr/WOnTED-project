graph [
  maxDegree 13
  minDegree 1
  meanDegree 1.945945945945946
  density 0.05405405405405406
  graphCliqueNumber 2
  node [
    id 0
    label "rozpoczyna&#263;"
    origin "text"
  ]
  node [
    id 1
    label "nowy"
    origin "text"
  ]
  node [
    id 2
    label "cykl"
    origin "text"
  ]
  node [
    id 3
    label "tag"
    origin "text"
  ]
  node [
    id 4
    label "tinder"
    origin "text"
  ]
  node [
    id 5
    label "chadstories"
    origin "text"
  ]
  node [
    id 6
    label "start"
  ]
  node [
    id 7
    label "robi&#263;_pierwszy_krok"
  ]
  node [
    id 8
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 9
    label "begin"
  ]
  node [
    id 10
    label "sprawowa&#263;"
  ]
  node [
    id 11
    label "cz&#322;owiek"
  ]
  node [
    id 12
    label "nowotny"
  ]
  node [
    id 13
    label "drugi"
  ]
  node [
    id 14
    label "kolejny"
  ]
  node [
    id 15
    label "bie&#380;&#261;cy"
  ]
  node [
    id 16
    label "nowo"
  ]
  node [
    id 17
    label "narybek"
  ]
  node [
    id 18
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 19
    label "obcy"
  ]
  node [
    id 20
    label "sekwencja"
  ]
  node [
    id 21
    label "czas"
  ]
  node [
    id 22
    label "edycja"
  ]
  node [
    id 23
    label "przebieg"
  ]
  node [
    id 24
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 25
    label "okres"
  ]
  node [
    id 26
    label "cykl_p&#322;ciowy"
  ]
  node [
    id 27
    label "cycle"
  ]
  node [
    id 28
    label "owulacja"
  ]
  node [
    id 29
    label "miesi&#261;czka"
  ]
  node [
    id 30
    label "set"
  ]
  node [
    id 31
    label "identyfikator"
  ]
  node [
    id 32
    label "napis"
  ]
  node [
    id 33
    label "nerd"
  ]
  node [
    id 34
    label "komnatowy"
  ]
  node [
    id 35
    label "sport_elektroniczny"
  ]
  node [
    id 36
    label "znacznik"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 4
    target 5
  ]
]
