graph [
  maxDegree 42
  minDegree 1
  meanDegree 2.1260162601626016
  density 0.004329972016624444
  graphCliqueNumber 3
  node [
    id 0
    label "mama"
    origin "text"
  ]
  node [
    id 1
    label "przyjemno&#347;&#263;"
    origin "text"
  ]
  node [
    id 2
    label "prowadzi&#263;"
    origin "text"
  ]
  node [
    id 3
    label "tydzie&#324;"
    origin "text"
  ]
  node [
    id 4
    label "razem"
    origin "text"
  ]
  node [
    id 5
    label "albert"
    origin "text"
  ]
  node [
    id 6
    label "hup&#261;"
    origin "text"
  ]
  node [
    id 7
    label "isns"
    origin "text"
  ]
  node [
    id 8
    label "klub"
    origin "text"
  ]
  node [
    id 9
    label "dyskusyjny"
    origin "text"
  ]
  node [
    id 10
    label "technologia"
    origin "text"
  ]
  node [
    id 11
    label "spo&#322;ecze&#324;stwo"
    origin "text"
  ]
  node [
    id 12
    label "plac"
    origin "text"
  ]
  node [
    id 13
    label "bliski"
    origin "text"
  ]
  node [
    id 14
    label "poniedzia&#322;ek"
    origin "text"
  ]
  node [
    id 15
    label "zaprasza&#263;"
    origin "text"
  ]
  node [
    id 16
    label "rozmowa"
    origin "text"
  ]
  node [
    id 17
    label "wszech&#347;wiat"
    origin "text"
  ]
  node [
    id 18
    label "by&#263;"
    origin "text"
  ]
  node [
    id 19
    label "komputer"
    origin "text"
  ]
  node [
    id 20
    label "ksi&#261;dz"
    origin "text"
  ]
  node [
    id 21
    label "henryk"
    origin "text"
  ]
  node [
    id 22
    label "paprocka"
    origin "text"
  ]
  node [
    id 23
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 24
    label "koncepcja"
    origin "text"
  ]
  node [
    id 25
    label "niemiecki"
    origin "text"
  ]
  node [
    id 26
    label "pionier"
    origin "text"
  ]
  node [
    id 27
    label "cybernetyka"
    origin "text"
  ]
  node [
    id 28
    label "konrad"
    origin "text"
  ]
  node [
    id 29
    label "zuse"
    origin "text"
  ]
  node [
    id 30
    label "matematyka"
    origin "text"
  ]
  node [
    id 31
    label "informatyka"
    origin "text"
  ]
  node [
    id 32
    label "teologia"
    origin "text"
  ]
  node [
    id 33
    label "&#380;ycie"
    origin "text"
  ]
  node [
    id 34
    label "ca&#322;y"
    origin "text"
  ]
  node [
    id 35
    label "reszta"
    origin "text"
  ]
  node [
    id 36
    label "zaj&#281;cia"
    origin "text"
  ]
  node [
    id 37
    label "otwarty"
    origin "text"
  ]
  node [
    id 38
    label "dla"
    origin "text"
  ]
  node [
    id 39
    label "wszyscy"
    origin "text"
  ]
  node [
    id 40
    label "tak&#380;e"
    origin "text"
  ]
  node [
    id 41
    label "strona"
    origin "text"
  ]
  node [
    id 42
    label "internetowy"
    origin "text"
  ]
  node [
    id 43
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 44
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 45
    label "edytowa&#263;"
    origin "text"
  ]
  node [
    id 46
    label "ten"
    origin "text"
  ]
  node [
    id 47
    label "spos&#243;b"
    origin "text"
  ]
  node [
    id 48
    label "wsp&#243;lnie"
    origin "text"
  ]
  node [
    id 49
    label "podejmowa&#263;"
    origin "text"
  ]
  node [
    id 50
    label "decyzja"
    origin "text"
  ]
  node [
    id 51
    label "kolejny"
    origin "text"
  ]
  node [
    id 52
    label "temat"
    origin "text"
  ]
  node [
    id 53
    label "go&#347;cia"
    origin "text"
  ]
  node [
    id 54
    label "znany"
    origin "text"
  ]
  node [
    id 55
    label "matczysko"
  ]
  node [
    id 56
    label "macierz"
  ]
  node [
    id 57
    label "przodkini"
  ]
  node [
    id 58
    label "Matka_Boska"
  ]
  node [
    id 59
    label "macocha"
  ]
  node [
    id 60
    label "matka_zast&#281;pcza"
  ]
  node [
    id 61
    label "stara"
  ]
  node [
    id 62
    label "rodzice"
  ]
  node [
    id 63
    label "rodzic"
  ]
  node [
    id 64
    label "u&#380;ycie"
  ]
  node [
    id 65
    label "doznanie"
  ]
  node [
    id 66
    label "u&#380;y&#263;"
  ]
  node [
    id 67
    label "lubo&#347;&#263;"
  ]
  node [
    id 68
    label "bawienie"
  ]
  node [
    id 69
    label "dobrostan"
  ]
  node [
    id 70
    label "prze&#380;ycie"
  ]
  node [
    id 71
    label "u&#380;ywa&#263;"
  ]
  node [
    id 72
    label "mutant"
  ]
  node [
    id 73
    label "u&#380;ywanie"
  ]
  node [
    id 74
    label "control"
  ]
  node [
    id 75
    label "eksponowa&#263;"
  ]
  node [
    id 76
    label "kre&#347;li&#263;"
  ]
  node [
    id 77
    label "g&#243;rowa&#263;"
  ]
  node [
    id 78
    label "message"
  ]
  node [
    id 79
    label "partner"
  ]
  node [
    id 80
    label "string"
  ]
  node [
    id 81
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 82
    label "przesuwa&#263;"
  ]
  node [
    id 83
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 84
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 85
    label "powodowa&#263;"
  ]
  node [
    id 86
    label "kierowa&#263;"
  ]
  node [
    id 87
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 88
    label "robi&#263;"
  ]
  node [
    id 89
    label "manipulate"
  ]
  node [
    id 90
    label "&#380;y&#263;"
  ]
  node [
    id 91
    label "navigate"
  ]
  node [
    id 92
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 93
    label "ukierunkowywa&#263;"
  ]
  node [
    id 94
    label "linia_melodyczna"
  ]
  node [
    id 95
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 96
    label "prowadzenie"
  ]
  node [
    id 97
    label "tworzy&#263;"
  ]
  node [
    id 98
    label "przewy&#380;sza&#263;"
  ]
  node [
    id 99
    label "sterowa&#263;"
  ]
  node [
    id 100
    label "krzywa"
  ]
  node [
    id 101
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 102
    label "doba"
  ]
  node [
    id 103
    label "czas"
  ]
  node [
    id 104
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 105
    label "weekend"
  ]
  node [
    id 106
    label "miesi&#261;c"
  ]
  node [
    id 107
    label "&#322;&#261;cznie"
  ]
  node [
    id 108
    label "society"
  ]
  node [
    id 109
    label "jakobini"
  ]
  node [
    id 110
    label "klubista"
  ]
  node [
    id 111
    label "stowarzyszenie"
  ]
  node [
    id 112
    label "lokal"
  ]
  node [
    id 113
    label "od&#322;am"
  ]
  node [
    id 114
    label "siedziba"
  ]
  node [
    id 115
    label "bar"
  ]
  node [
    id 116
    label "kontrowersyjnie"
  ]
  node [
    id 117
    label "w&#261;tpliwy"
  ]
  node [
    id 118
    label "dyskusyjnie"
  ]
  node [
    id 119
    label "engineering"
  ]
  node [
    id 120
    label "technika"
  ]
  node [
    id 121
    label "mikrotechnologia"
  ]
  node [
    id 122
    label "technologia_nieorganiczna"
  ]
  node [
    id 123
    label "biotechnologia"
  ]
  node [
    id 124
    label "pole"
  ]
  node [
    id 125
    label "kastowo&#347;&#263;"
  ]
  node [
    id 126
    label "ludzie_pracy"
  ]
  node [
    id 127
    label "community"
  ]
  node [
    id 128
    label "status"
  ]
  node [
    id 129
    label "cywilizacja"
  ]
  node [
    id 130
    label "pozaklasowy"
  ]
  node [
    id 131
    label "aspo&#322;eczny"
  ]
  node [
    id 132
    label "uwarstwienie"
  ]
  node [
    id 133
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 134
    label "elita"
  ]
  node [
    id 135
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 136
    label "wspo&#322;ecze&#324;stwo"
  ]
  node [
    id 137
    label "klasa"
  ]
  node [
    id 138
    label "stoisko"
  ]
  node [
    id 139
    label "Majdan"
  ]
  node [
    id 140
    label "miejsce"
  ]
  node [
    id 141
    label "obszar"
  ]
  node [
    id 142
    label "kram"
  ]
  node [
    id 143
    label "pierzeja"
  ]
  node [
    id 144
    label "przestrze&#324;"
  ]
  node [
    id 145
    label "obiekt_handlowy"
  ]
  node [
    id 146
    label "targowica"
  ]
  node [
    id 147
    label "zgromadzenie"
  ]
  node [
    id 148
    label "miasto"
  ]
  node [
    id 149
    label "pole_bitwy"
  ]
  node [
    id 150
    label "&#321;ubianka"
  ]
  node [
    id 151
    label "area"
  ]
  node [
    id 152
    label "cz&#322;owiek"
  ]
  node [
    id 153
    label "blisko"
  ]
  node [
    id 154
    label "przesz&#322;y"
  ]
  node [
    id 155
    label "gotowy"
  ]
  node [
    id 156
    label "dok&#322;adny"
  ]
  node [
    id 157
    label "kr&#243;tki"
  ]
  node [
    id 158
    label "znajomy"
  ]
  node [
    id 159
    label "przysz&#322;y"
  ]
  node [
    id 160
    label "oddalony"
  ]
  node [
    id 161
    label "silny"
  ]
  node [
    id 162
    label "zbli&#380;enie"
  ]
  node [
    id 163
    label "zwi&#261;zany"
  ]
  node [
    id 164
    label "nieodleg&#322;y"
  ]
  node [
    id 165
    label "ma&#322;y"
  ]
  node [
    id 166
    label "dzie&#324;_powszedni"
  ]
  node [
    id 167
    label "Poniedzia&#322;ek_Wielkonocny"
  ]
  node [
    id 168
    label "invite"
  ]
  node [
    id 169
    label "ask"
  ]
  node [
    id 170
    label "oferowa&#263;"
  ]
  node [
    id 171
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 172
    label "discussion"
  ]
  node [
    id 173
    label "czynno&#347;&#263;"
  ]
  node [
    id 174
    label "odpowied&#378;"
  ]
  node [
    id 175
    label "rozhowor"
  ]
  node [
    id 176
    label "cisza"
  ]
  node [
    id 177
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 178
    label "makrokosmos"
  ]
  node [
    id 179
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 180
    label "czarna_dziura"
  ]
  node [
    id 181
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 182
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 183
    label "planeta"
  ]
  node [
    id 184
    label "ekosfera"
  ]
  node [
    id 185
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 186
    label "ciemna_materia"
  ]
  node [
    id 187
    label "mikrokosmos"
  ]
  node [
    id 188
    label "si&#281;ga&#263;"
  ]
  node [
    id 189
    label "trwa&#263;"
  ]
  node [
    id 190
    label "obecno&#347;&#263;"
  ]
  node [
    id 191
    label "stan"
  ]
  node [
    id 192
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 193
    label "stand"
  ]
  node [
    id 194
    label "mie&#263;_miejsce"
  ]
  node [
    id 195
    label "uczestniczy&#263;"
  ]
  node [
    id 196
    label "chodzi&#263;"
  ]
  node [
    id 197
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 198
    label "equal"
  ]
  node [
    id 199
    label "pami&#281;&#263;"
  ]
  node [
    id 200
    label "urz&#261;dzenie"
  ]
  node [
    id 201
    label "maszyna_Turinga"
  ]
  node [
    id 202
    label "emulacja"
  ]
  node [
    id 203
    label "botnet"
  ]
  node [
    id 204
    label "moc_obliczeniowa"
  ]
  node [
    id 205
    label "stacja_dysk&#243;w"
  ]
  node [
    id 206
    label "monitor"
  ]
  node [
    id 207
    label "instalowanie"
  ]
  node [
    id 208
    label "karta"
  ]
  node [
    id 209
    label "instalowa&#263;"
  ]
  node [
    id 210
    label "elektroniczna_maszyna_licz&#261;ca"
  ]
  node [
    id 211
    label "mysz"
  ]
  node [
    id 212
    label "pad"
  ]
  node [
    id 213
    label "zainstalowanie"
  ]
  node [
    id 214
    label "twardy_dysk"
  ]
  node [
    id 215
    label "radiator"
  ]
  node [
    id 216
    label "modem"
  ]
  node [
    id 217
    label "klawiatura"
  ]
  node [
    id 218
    label "zainstalowa&#263;"
  ]
  node [
    id 219
    label "p&#322;yta_g&#322;&#243;wna"
  ]
  node [
    id 220
    label "procesor"
  ]
  node [
    id 221
    label "klecha"
  ]
  node [
    id 222
    label "eklezjasta"
  ]
  node [
    id 223
    label "rozgrzeszanie"
  ]
  node [
    id 224
    label "duszpasterstwo"
  ]
  node [
    id 225
    label "rozgrzesza&#263;"
  ]
  node [
    id 226
    label "kap&#322;an"
  ]
  node [
    id 227
    label "ksi&#281;&#380;a"
  ]
  node [
    id 228
    label "duchowny"
  ]
  node [
    id 229
    label "kol&#281;da"
  ]
  node [
    id 230
    label "seminarzysta"
  ]
  node [
    id 231
    label "pasterz"
  ]
  node [
    id 232
    label "remark"
  ]
  node [
    id 233
    label "okre&#347;la&#263;"
  ]
  node [
    id 234
    label "j&#281;zyk"
  ]
  node [
    id 235
    label "say"
  ]
  node [
    id 236
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 237
    label "formu&#322;owa&#263;"
  ]
  node [
    id 238
    label "talk"
  ]
  node [
    id 239
    label "powiada&#263;"
  ]
  node [
    id 240
    label "informowa&#263;"
  ]
  node [
    id 241
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 242
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 243
    label "wydobywa&#263;"
  ]
  node [
    id 244
    label "express"
  ]
  node [
    id 245
    label "chew_the_fat"
  ]
  node [
    id 246
    label "dysfonia"
  ]
  node [
    id 247
    label "umie&#263;"
  ]
  node [
    id 248
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 249
    label "tell"
  ]
  node [
    id 250
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 251
    label "wyra&#380;a&#263;"
  ]
  node [
    id 252
    label "gaworzy&#263;"
  ]
  node [
    id 253
    label "rozmawia&#263;"
  ]
  node [
    id 254
    label "dziama&#263;"
  ]
  node [
    id 255
    label "prawi&#263;"
  ]
  node [
    id 256
    label "problem"
  ]
  node [
    id 257
    label "idea"
  ]
  node [
    id 258
    label "za&#322;o&#380;enie"
  ]
  node [
    id 259
    label "pomys&#322;"
  ]
  node [
    id 260
    label "poj&#281;cie"
  ]
  node [
    id 261
    label "uj&#281;cie"
  ]
  node [
    id 262
    label "zamys&#322;"
  ]
  node [
    id 263
    label "szwabski"
  ]
  node [
    id 264
    label "po_niemiecku"
  ]
  node [
    id 265
    label "niemiec"
  ]
  node [
    id 266
    label "cenar"
  ]
  node [
    id 267
    label "europejski"
  ]
  node [
    id 268
    label "German"
  ]
  node [
    id 269
    label "niemiecko"
  ]
  node [
    id 270
    label "j&#281;zyk_zachodniogerma&#324;ski"
  ]
  node [
    id 271
    label "zachodnioeuropejski"
  ]
  node [
    id 272
    label "strudel"
  ]
  node [
    id 273
    label "junkers"
  ]
  node [
    id 274
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 275
    label "prekursor"
  ]
  node [
    id 276
    label "&#380;o&#322;nierz"
  ]
  node [
    id 277
    label "g&#322;osiciel"
  ]
  node [
    id 278
    label "saper"
  ]
  node [
    id 279
    label "osadnik"
  ]
  node [
    id 280
    label "skaut"
  ]
  node [
    id 281
    label "system_autonomiczny"
  ]
  node [
    id 282
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 283
    label "biocybernetyka"
  ]
  node [
    id 284
    label "infimum"
  ]
  node [
    id 285
    label "matematyka_czysta"
  ]
  node [
    id 286
    label "przedmiot"
  ]
  node [
    id 287
    label "fizyka_matematyczna"
  ]
  node [
    id 288
    label "matma"
  ]
  node [
    id 289
    label "rachunki"
  ]
  node [
    id 290
    label "kryptologia"
  ]
  node [
    id 291
    label "supremum"
  ]
  node [
    id 292
    label "rachunek_operatorowy"
  ]
  node [
    id 293
    label "funkcja"
  ]
  node [
    id 294
    label "rzut"
  ]
  node [
    id 295
    label "forsing"
  ]
  node [
    id 296
    label "teoria_graf&#243;w"
  ]
  node [
    id 297
    label "logicyzm"
  ]
  node [
    id 298
    label "logika"
  ]
  node [
    id 299
    label "topologia_algebraiczna"
  ]
  node [
    id 300
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 301
    label "matematyka_stosowana"
  ]
  node [
    id 302
    label "kierunek"
  ]
  node [
    id 303
    label "modelowanie_matematyczne"
  ]
  node [
    id 304
    label "teoria_katastrof"
  ]
  node [
    id 305
    label "jednostka"
  ]
  node [
    id 306
    label "HP"
  ]
  node [
    id 307
    label "dost&#281;p"
  ]
  node [
    id 308
    label "sztuczna_inteligencja"
  ]
  node [
    id 309
    label "zamek"
  ]
  node [
    id 310
    label "baza_danych"
  ]
  node [
    id 311
    label "artefakt"
  ]
  node [
    id 312
    label "program"
  ]
  node [
    id 313
    label "przetwarzanie_j&#281;zyka_naturalnego"
  ]
  node [
    id 314
    label "przetwarzanie_informacji"
  ]
  node [
    id 315
    label "dziedzina_informatyki"
  ]
  node [
    id 316
    label "gramatyka_formalna"
  ]
  node [
    id 317
    label "infa"
  ]
  node [
    id 318
    label "patrologia"
  ]
  node [
    id 319
    label "dogmatyka"
  ]
  node [
    id 320
    label "teologia_pastoralna"
  ]
  node [
    id 321
    label "teologia_narracyjna"
  ]
  node [
    id 322
    label "teogonia"
  ]
  node [
    id 323
    label "katechetyka"
  ]
  node [
    id 324
    label "homiletyka"
  ]
  node [
    id 325
    label "antropologia_religijna"
  ]
  node [
    id 326
    label "religiologia"
  ]
  node [
    id 327
    label "liturgika"
  ]
  node [
    id 328
    label "nauka_humanistyczna"
  ]
  node [
    id 329
    label "misjologia"
  ]
  node [
    id 330
    label "eschatologia"
  ]
  node [
    id 331
    label "energy"
  ]
  node [
    id 332
    label "bycie"
  ]
  node [
    id 333
    label "zegar_biologiczny"
  ]
  node [
    id 334
    label "okres_noworodkowy"
  ]
  node [
    id 335
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 336
    label "entity"
  ]
  node [
    id 337
    label "prze&#380;ywanie"
  ]
  node [
    id 338
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 339
    label "wiek_matuzalemowy"
  ]
  node [
    id 340
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 341
    label "dzieci&#324;stwo"
  ]
  node [
    id 342
    label "power"
  ]
  node [
    id 343
    label "szwung"
  ]
  node [
    id 344
    label "menopauza"
  ]
  node [
    id 345
    label "umarcie"
  ]
  node [
    id 346
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 347
    label "life"
  ]
  node [
    id 348
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 349
    label "&#380;ywy"
  ]
  node [
    id 350
    label "rozw&#243;j"
  ]
  node [
    id 351
    label "po&#322;&#243;g"
  ]
  node [
    id 352
    label "byt"
  ]
  node [
    id 353
    label "przebywanie"
  ]
  node [
    id 354
    label "subsistence"
  ]
  node [
    id 355
    label "koleje_losu"
  ]
  node [
    id 356
    label "raj_utracony"
  ]
  node [
    id 357
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 358
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 359
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 360
    label "andropauza"
  ]
  node [
    id 361
    label "warunki"
  ]
  node [
    id 362
    label "do&#380;ywanie"
  ]
  node [
    id 363
    label "niemowl&#281;ctwo"
  ]
  node [
    id 364
    label "umieranie"
  ]
  node [
    id 365
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 366
    label "staro&#347;&#263;"
  ]
  node [
    id 367
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 368
    label "&#347;mier&#263;"
  ]
  node [
    id 369
    label "du&#380;y"
  ]
  node [
    id 370
    label "jedyny"
  ]
  node [
    id 371
    label "kompletny"
  ]
  node [
    id 372
    label "zdr&#243;w"
  ]
  node [
    id 373
    label "ca&#322;o"
  ]
  node [
    id 374
    label "pe&#322;ny"
  ]
  node [
    id 375
    label "calu&#347;ko"
  ]
  node [
    id 376
    label "podobny"
  ]
  node [
    id 377
    label "remainder"
  ]
  node [
    id 378
    label "wydanie"
  ]
  node [
    id 379
    label "wyda&#263;"
  ]
  node [
    id 380
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 381
    label "wydawa&#263;"
  ]
  node [
    id 382
    label "pozosta&#322;y"
  ]
  node [
    id 383
    label "kwota"
  ]
  node [
    id 384
    label "pensum"
  ]
  node [
    id 385
    label "enroll"
  ]
  node [
    id 386
    label "ewidentny"
  ]
  node [
    id 387
    label "bezpo&#347;redni"
  ]
  node [
    id 388
    label "otwarcie"
  ]
  node [
    id 389
    label "nieograniczony"
  ]
  node [
    id 390
    label "zdecydowany"
  ]
  node [
    id 391
    label "aktualny"
  ]
  node [
    id 392
    label "prostoduszny"
  ]
  node [
    id 393
    label "jawnie"
  ]
  node [
    id 394
    label "otworzysty"
  ]
  node [
    id 395
    label "dost&#281;pny"
  ]
  node [
    id 396
    label "publiczny"
  ]
  node [
    id 397
    label "aktywny"
  ]
  node [
    id 398
    label "kontaktowy"
  ]
  node [
    id 399
    label "skr&#281;canie"
  ]
  node [
    id 400
    label "voice"
  ]
  node [
    id 401
    label "forma"
  ]
  node [
    id 402
    label "internet"
  ]
  node [
    id 403
    label "skr&#281;ci&#263;"
  ]
  node [
    id 404
    label "kartka"
  ]
  node [
    id 405
    label "orientowa&#263;"
  ]
  node [
    id 406
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 407
    label "powierzchnia"
  ]
  node [
    id 408
    label "plik"
  ]
  node [
    id 409
    label "bok"
  ]
  node [
    id 410
    label "pagina"
  ]
  node [
    id 411
    label "orientowanie"
  ]
  node [
    id 412
    label "fragment"
  ]
  node [
    id 413
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 414
    label "s&#261;d"
  ]
  node [
    id 415
    label "skr&#281;ca&#263;"
  ]
  node [
    id 416
    label "g&#243;ra"
  ]
  node [
    id 417
    label "serwis_internetowy"
  ]
  node [
    id 418
    label "orientacja"
  ]
  node [
    id 419
    label "linia"
  ]
  node [
    id 420
    label "skr&#281;cenie"
  ]
  node [
    id 421
    label "layout"
  ]
  node [
    id 422
    label "zorientowa&#263;"
  ]
  node [
    id 423
    label "zorientowanie"
  ]
  node [
    id 424
    label "obiekt"
  ]
  node [
    id 425
    label "podmiot"
  ]
  node [
    id 426
    label "ty&#322;"
  ]
  node [
    id 427
    label "logowanie"
  ]
  node [
    id 428
    label "adres_internetowy"
  ]
  node [
    id 429
    label "prz&#243;d"
  ]
  node [
    id 430
    label "posta&#263;"
  ]
  node [
    id 431
    label "nowoczesny"
  ]
  node [
    id 432
    label "elektroniczny"
  ]
  node [
    id 433
    label "sieciowo"
  ]
  node [
    id 434
    label "netowy"
  ]
  node [
    id 435
    label "internetowo"
  ]
  node [
    id 436
    label "jaki&#347;"
  ]
  node [
    id 437
    label "dane"
  ]
  node [
    id 438
    label "edit"
  ]
  node [
    id 439
    label "modyfikowa&#263;"
  ]
  node [
    id 440
    label "okre&#347;lony"
  ]
  node [
    id 441
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 442
    label "model"
  ]
  node [
    id 443
    label "zbi&#243;r"
  ]
  node [
    id 444
    label "tryb"
  ]
  node [
    id 445
    label "narz&#281;dzie"
  ]
  node [
    id 446
    label "nature"
  ]
  node [
    id 447
    label "wsp&#243;lny"
  ]
  node [
    id 448
    label "sp&#243;lnie"
  ]
  node [
    id 449
    label "zmienia&#263;"
  ]
  node [
    id 450
    label "reagowa&#263;"
  ]
  node [
    id 451
    label "rise"
  ]
  node [
    id 452
    label "admit"
  ]
  node [
    id 453
    label "drive"
  ]
  node [
    id 454
    label "draw"
  ]
  node [
    id 455
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 456
    label "podnosi&#263;"
  ]
  node [
    id 457
    label "dokument"
  ]
  node [
    id 458
    label "resolution"
  ]
  node [
    id 459
    label "zdecydowanie"
  ]
  node [
    id 460
    label "wytw&#243;r"
  ]
  node [
    id 461
    label "podj&#281;cie_decyzji"
  ]
  node [
    id 462
    label "management"
  ]
  node [
    id 463
    label "inny"
  ]
  node [
    id 464
    label "nast&#281;pnie"
  ]
  node [
    id 465
    label "kt&#243;ry&#347;"
  ]
  node [
    id 466
    label "kolejno"
  ]
  node [
    id 467
    label "nastopny"
  ]
  node [
    id 468
    label "fraza"
  ]
  node [
    id 469
    label "melodia"
  ]
  node [
    id 470
    label "rzecz"
  ]
  node [
    id 471
    label "zbacza&#263;"
  ]
  node [
    id 472
    label "omawia&#263;"
  ]
  node [
    id 473
    label "topik"
  ]
  node [
    id 474
    label "wyraz_pochodny"
  ]
  node [
    id 475
    label "om&#243;wi&#263;"
  ]
  node [
    id 476
    label "omawianie"
  ]
  node [
    id 477
    label "w&#261;tek"
  ]
  node [
    id 478
    label "forum"
  ]
  node [
    id 479
    label "cecha"
  ]
  node [
    id 480
    label "zboczenie"
  ]
  node [
    id 481
    label "zbaczanie"
  ]
  node [
    id 482
    label "tre&#347;&#263;"
  ]
  node [
    id 483
    label "tematyka"
  ]
  node [
    id 484
    label "sprawa"
  ]
  node [
    id 485
    label "istota"
  ]
  node [
    id 486
    label "otoczka"
  ]
  node [
    id 487
    label "zboczy&#263;"
  ]
  node [
    id 488
    label "om&#243;wienie"
  ]
  node [
    id 489
    label "wielki"
  ]
  node [
    id 490
    label "rozpowszechnianie"
  ]
  node [
    id 491
    label "wyp&#322;yni&#281;cie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 116
  ]
  edge [
    source 9
    target 117
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 10
    target 121
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 10
    target 47
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 124
  ]
  edge [
    source 11
    target 125
  ]
  edge [
    source 11
    target 126
  ]
  edge [
    source 11
    target 127
  ]
  edge [
    source 11
    target 128
  ]
  edge [
    source 11
    target 129
  ]
  edge [
    source 11
    target 130
  ]
  edge [
    source 11
    target 131
  ]
  edge [
    source 11
    target 132
  ]
  edge [
    source 11
    target 133
  ]
  edge [
    source 11
    target 134
  ]
  edge [
    source 11
    target 135
  ]
  edge [
    source 11
    target 136
  ]
  edge [
    source 11
    target 137
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 138
  ]
  edge [
    source 12
    target 139
  ]
  edge [
    source 12
    target 140
  ]
  edge [
    source 12
    target 141
  ]
  edge [
    source 12
    target 142
  ]
  edge [
    source 12
    target 143
  ]
  edge [
    source 12
    target 144
  ]
  edge [
    source 12
    target 145
  ]
  edge [
    source 12
    target 146
  ]
  edge [
    source 12
    target 147
  ]
  edge [
    source 12
    target 148
  ]
  edge [
    source 12
    target 149
  ]
  edge [
    source 12
    target 150
  ]
  edge [
    source 12
    target 151
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 152
  ]
  edge [
    source 13
    target 153
  ]
  edge [
    source 13
    target 154
  ]
  edge [
    source 13
    target 155
  ]
  edge [
    source 13
    target 156
  ]
  edge [
    source 13
    target 157
  ]
  edge [
    source 13
    target 158
  ]
  edge [
    source 13
    target 159
  ]
  edge [
    source 13
    target 160
  ]
  edge [
    source 13
    target 161
  ]
  edge [
    source 13
    target 162
  ]
  edge [
    source 13
    target 163
  ]
  edge [
    source 13
    target 164
  ]
  edge [
    source 13
    target 165
  ]
  edge [
    source 13
    target 54
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 166
  ]
  edge [
    source 14
    target 167
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 168
  ]
  edge [
    source 15
    target 169
  ]
  edge [
    source 15
    target 170
  ]
  edge [
    source 15
    target 171
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 172
  ]
  edge [
    source 16
    target 173
  ]
  edge [
    source 16
    target 174
  ]
  edge [
    source 16
    target 175
  ]
  edge [
    source 16
    target 176
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 33
  ]
  edge [
    source 17
    target 34
  ]
  edge [
    source 17
    target 177
  ]
  edge [
    source 17
    target 178
  ]
  edge [
    source 17
    target 179
  ]
  edge [
    source 17
    target 180
  ]
  edge [
    source 17
    target 181
  ]
  edge [
    source 17
    target 144
  ]
  edge [
    source 17
    target 182
  ]
  edge [
    source 17
    target 183
  ]
  edge [
    source 17
    target 184
  ]
  edge [
    source 17
    target 185
  ]
  edge [
    source 17
    target 186
  ]
  edge [
    source 17
    target 187
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 22
  ]
  edge [
    source 18
    target 23
  ]
  edge [
    source 18
    target 36
  ]
  edge [
    source 18
    target 37
  ]
  edge [
    source 18
    target 53
  ]
  edge [
    source 18
    target 40
  ]
  edge [
    source 18
    target 188
  ]
  edge [
    source 18
    target 189
  ]
  edge [
    source 18
    target 190
  ]
  edge [
    source 18
    target 191
  ]
  edge [
    source 18
    target 192
  ]
  edge [
    source 18
    target 193
  ]
  edge [
    source 18
    target 194
  ]
  edge [
    source 18
    target 195
  ]
  edge [
    source 18
    target 196
  ]
  edge [
    source 18
    target 197
  ]
  edge [
    source 18
    target 198
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 199
  ]
  edge [
    source 19
    target 200
  ]
  edge [
    source 19
    target 201
  ]
  edge [
    source 19
    target 202
  ]
  edge [
    source 19
    target 203
  ]
  edge [
    source 19
    target 204
  ]
  edge [
    source 19
    target 205
  ]
  edge [
    source 19
    target 206
  ]
  edge [
    source 19
    target 207
  ]
  edge [
    source 19
    target 208
  ]
  edge [
    source 19
    target 209
  ]
  edge [
    source 19
    target 210
  ]
  edge [
    source 19
    target 211
  ]
  edge [
    source 19
    target 212
  ]
  edge [
    source 19
    target 213
  ]
  edge [
    source 19
    target 214
  ]
  edge [
    source 19
    target 215
  ]
  edge [
    source 19
    target 216
  ]
  edge [
    source 19
    target 217
  ]
  edge [
    source 19
    target 218
  ]
  edge [
    source 19
    target 219
  ]
  edge [
    source 19
    target 220
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 221
  ]
  edge [
    source 20
    target 222
  ]
  edge [
    source 20
    target 223
  ]
  edge [
    source 20
    target 224
  ]
  edge [
    source 20
    target 225
  ]
  edge [
    source 20
    target 226
  ]
  edge [
    source 20
    target 227
  ]
  edge [
    source 20
    target 228
  ]
  edge [
    source 20
    target 229
  ]
  edge [
    source 20
    target 230
  ]
  edge [
    source 20
    target 231
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 232
  ]
  edge [
    source 23
    target 171
  ]
  edge [
    source 23
    target 71
  ]
  edge [
    source 23
    target 233
  ]
  edge [
    source 23
    target 234
  ]
  edge [
    source 23
    target 235
  ]
  edge [
    source 23
    target 236
  ]
  edge [
    source 23
    target 237
  ]
  edge [
    source 23
    target 238
  ]
  edge [
    source 23
    target 239
  ]
  edge [
    source 23
    target 240
  ]
  edge [
    source 23
    target 241
  ]
  edge [
    source 23
    target 242
  ]
  edge [
    source 23
    target 243
  ]
  edge [
    source 23
    target 244
  ]
  edge [
    source 23
    target 245
  ]
  edge [
    source 23
    target 246
  ]
  edge [
    source 23
    target 247
  ]
  edge [
    source 23
    target 248
  ]
  edge [
    source 23
    target 249
  ]
  edge [
    source 23
    target 250
  ]
  edge [
    source 23
    target 251
  ]
  edge [
    source 23
    target 252
  ]
  edge [
    source 23
    target 253
  ]
  edge [
    source 23
    target 254
  ]
  edge [
    source 23
    target 255
  ]
  edge [
    source 23
    target 43
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 256
  ]
  edge [
    source 24
    target 257
  ]
  edge [
    source 24
    target 258
  ]
  edge [
    source 24
    target 259
  ]
  edge [
    source 24
    target 260
  ]
  edge [
    source 24
    target 261
  ]
  edge [
    source 24
    target 262
  ]
  edge [
    source 24
    target 30
  ]
  edge [
    source 24
    target 49
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 263
  ]
  edge [
    source 25
    target 264
  ]
  edge [
    source 25
    target 265
  ]
  edge [
    source 25
    target 266
  ]
  edge [
    source 25
    target 234
  ]
  edge [
    source 25
    target 267
  ]
  edge [
    source 25
    target 268
  ]
  edge [
    source 25
    target 269
  ]
  edge [
    source 25
    target 270
  ]
  edge [
    source 25
    target 271
  ]
  edge [
    source 25
    target 272
  ]
  edge [
    source 25
    target 273
  ]
  edge [
    source 25
    target 274
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 275
  ]
  edge [
    source 26
    target 276
  ]
  edge [
    source 26
    target 277
  ]
  edge [
    source 26
    target 278
  ]
  edge [
    source 26
    target 279
  ]
  edge [
    source 26
    target 280
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 281
  ]
  edge [
    source 27
    target 282
  ]
  edge [
    source 27
    target 283
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 284
  ]
  edge [
    source 30
    target 285
  ]
  edge [
    source 30
    target 286
  ]
  edge [
    source 30
    target 287
  ]
  edge [
    source 30
    target 288
  ]
  edge [
    source 30
    target 289
  ]
  edge [
    source 30
    target 290
  ]
  edge [
    source 30
    target 291
  ]
  edge [
    source 30
    target 282
  ]
  edge [
    source 30
    target 292
  ]
  edge [
    source 30
    target 293
  ]
  edge [
    source 30
    target 294
  ]
  edge [
    source 30
    target 295
  ]
  edge [
    source 30
    target 296
  ]
  edge [
    source 30
    target 297
  ]
  edge [
    source 30
    target 298
  ]
  edge [
    source 30
    target 299
  ]
  edge [
    source 30
    target 300
  ]
  edge [
    source 30
    target 301
  ]
  edge [
    source 30
    target 302
  ]
  edge [
    source 30
    target 303
  ]
  edge [
    source 30
    target 304
  ]
  edge [
    source 30
    target 305
  ]
  edge [
    source 30
    target 49
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 306
  ]
  edge [
    source 31
    target 307
  ]
  edge [
    source 31
    target 308
  ]
  edge [
    source 31
    target 290
  ]
  edge [
    source 31
    target 309
  ]
  edge [
    source 31
    target 310
  ]
  edge [
    source 31
    target 286
  ]
  edge [
    source 31
    target 302
  ]
  edge [
    source 31
    target 311
  ]
  edge [
    source 31
    target 312
  ]
  edge [
    source 31
    target 313
  ]
  edge [
    source 31
    target 282
  ]
  edge [
    source 31
    target 314
  ]
  edge [
    source 31
    target 315
  ]
  edge [
    source 31
    target 316
  ]
  edge [
    source 31
    target 317
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 318
  ]
  edge [
    source 32
    target 319
  ]
  edge [
    source 32
    target 320
  ]
  edge [
    source 32
    target 321
  ]
  edge [
    source 32
    target 322
  ]
  edge [
    source 32
    target 323
  ]
  edge [
    source 32
    target 324
  ]
  edge [
    source 32
    target 325
  ]
  edge [
    source 32
    target 326
  ]
  edge [
    source 32
    target 327
  ]
  edge [
    source 32
    target 328
  ]
  edge [
    source 32
    target 329
  ]
  edge [
    source 32
    target 330
  ]
  edge [
    source 33
    target 331
  ]
  edge [
    source 33
    target 103
  ]
  edge [
    source 33
    target 332
  ]
  edge [
    source 33
    target 333
  ]
  edge [
    source 33
    target 334
  ]
  edge [
    source 33
    target 335
  ]
  edge [
    source 33
    target 336
  ]
  edge [
    source 33
    target 337
  ]
  edge [
    source 33
    target 70
  ]
  edge [
    source 33
    target 338
  ]
  edge [
    source 33
    target 339
  ]
  edge [
    source 33
    target 340
  ]
  edge [
    source 33
    target 341
  ]
  edge [
    source 33
    target 342
  ]
  edge [
    source 33
    target 343
  ]
  edge [
    source 33
    target 344
  ]
  edge [
    source 33
    target 345
  ]
  edge [
    source 33
    target 346
  ]
  edge [
    source 33
    target 347
  ]
  edge [
    source 33
    target 348
  ]
  edge [
    source 33
    target 349
  ]
  edge [
    source 33
    target 350
  ]
  edge [
    source 33
    target 351
  ]
  edge [
    source 33
    target 352
  ]
  edge [
    source 33
    target 353
  ]
  edge [
    source 33
    target 354
  ]
  edge [
    source 33
    target 355
  ]
  edge [
    source 33
    target 356
  ]
  edge [
    source 33
    target 357
  ]
  edge [
    source 33
    target 358
  ]
  edge [
    source 33
    target 359
  ]
  edge [
    source 33
    target 360
  ]
  edge [
    source 33
    target 361
  ]
  edge [
    source 33
    target 362
  ]
  edge [
    source 33
    target 363
  ]
  edge [
    source 33
    target 364
  ]
  edge [
    source 33
    target 365
  ]
  edge [
    source 33
    target 366
  ]
  edge [
    source 33
    target 367
  ]
  edge [
    source 33
    target 368
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 369
  ]
  edge [
    source 34
    target 370
  ]
  edge [
    source 34
    target 371
  ]
  edge [
    source 34
    target 372
  ]
  edge [
    source 34
    target 349
  ]
  edge [
    source 34
    target 373
  ]
  edge [
    source 34
    target 374
  ]
  edge [
    source 34
    target 375
  ]
  edge [
    source 34
    target 376
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 377
  ]
  edge [
    source 35
    target 378
  ]
  edge [
    source 35
    target 379
  ]
  edge [
    source 35
    target 380
  ]
  edge [
    source 35
    target 381
  ]
  edge [
    source 35
    target 382
  ]
  edge [
    source 35
    target 383
  ]
  edge [
    source 36
    target 384
  ]
  edge [
    source 36
    target 385
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 386
  ]
  edge [
    source 37
    target 387
  ]
  edge [
    source 37
    target 388
  ]
  edge [
    source 37
    target 389
  ]
  edge [
    source 37
    target 390
  ]
  edge [
    source 37
    target 155
  ]
  edge [
    source 37
    target 391
  ]
  edge [
    source 37
    target 392
  ]
  edge [
    source 37
    target 393
  ]
  edge [
    source 37
    target 394
  ]
  edge [
    source 37
    target 395
  ]
  edge [
    source 37
    target 396
  ]
  edge [
    source 37
    target 397
  ]
  edge [
    source 37
    target 398
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 54
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 399
  ]
  edge [
    source 41
    target 400
  ]
  edge [
    source 41
    target 401
  ]
  edge [
    source 41
    target 402
  ]
  edge [
    source 41
    target 403
  ]
  edge [
    source 41
    target 404
  ]
  edge [
    source 41
    target 405
  ]
  edge [
    source 41
    target 406
  ]
  edge [
    source 41
    target 407
  ]
  edge [
    source 41
    target 408
  ]
  edge [
    source 41
    target 409
  ]
  edge [
    source 41
    target 410
  ]
  edge [
    source 41
    target 411
  ]
  edge [
    source 41
    target 412
  ]
  edge [
    source 41
    target 413
  ]
  edge [
    source 41
    target 414
  ]
  edge [
    source 41
    target 415
  ]
  edge [
    source 41
    target 416
  ]
  edge [
    source 41
    target 417
  ]
  edge [
    source 41
    target 418
  ]
  edge [
    source 41
    target 419
  ]
  edge [
    source 41
    target 420
  ]
  edge [
    source 41
    target 421
  ]
  edge [
    source 41
    target 422
  ]
  edge [
    source 41
    target 423
  ]
  edge [
    source 41
    target 424
  ]
  edge [
    source 41
    target 425
  ]
  edge [
    source 41
    target 426
  ]
  edge [
    source 41
    target 380
  ]
  edge [
    source 41
    target 427
  ]
  edge [
    source 41
    target 428
  ]
  edge [
    source 41
    target 261
  ]
  edge [
    source 41
    target 429
  ]
  edge [
    source 41
    target 430
  ]
  edge [
    source 42
    target 431
  ]
  edge [
    source 42
    target 432
  ]
  edge [
    source 42
    target 433
  ]
  edge [
    source 42
    target 434
  ]
  edge [
    source 42
    target 435
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 436
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 437
  ]
  edge [
    source 45
    target 438
  ]
  edge [
    source 45
    target 439
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 440
  ]
  edge [
    source 46
    target 441
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 442
  ]
  edge [
    source 47
    target 443
  ]
  edge [
    source 47
    target 444
  ]
  edge [
    source 47
    target 445
  ]
  edge [
    source 47
    target 446
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 447
  ]
  edge [
    source 48
    target 448
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 449
  ]
  edge [
    source 49
    target 450
  ]
  edge [
    source 49
    target 451
  ]
  edge [
    source 49
    target 452
  ]
  edge [
    source 49
    target 453
  ]
  edge [
    source 49
    target 88
  ]
  edge [
    source 49
    target 454
  ]
  edge [
    source 49
    target 455
  ]
  edge [
    source 49
    target 456
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 457
  ]
  edge [
    source 50
    target 458
  ]
  edge [
    source 50
    target 459
  ]
  edge [
    source 50
    target 460
  ]
  edge [
    source 50
    target 461
  ]
  edge [
    source 50
    target 462
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 463
  ]
  edge [
    source 51
    target 464
  ]
  edge [
    source 51
    target 465
  ]
  edge [
    source 51
    target 466
  ]
  edge [
    source 51
    target 467
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 468
  ]
  edge [
    source 52
    target 401
  ]
  edge [
    source 52
    target 469
  ]
  edge [
    source 52
    target 470
  ]
  edge [
    source 52
    target 471
  ]
  edge [
    source 52
    target 336
  ]
  edge [
    source 52
    target 472
  ]
  edge [
    source 52
    target 473
  ]
  edge [
    source 52
    target 474
  ]
  edge [
    source 52
    target 475
  ]
  edge [
    source 52
    target 476
  ]
  edge [
    source 52
    target 477
  ]
  edge [
    source 52
    target 478
  ]
  edge [
    source 52
    target 479
  ]
  edge [
    source 52
    target 480
  ]
  edge [
    source 52
    target 481
  ]
  edge [
    source 52
    target 482
  ]
  edge [
    source 52
    target 483
  ]
  edge [
    source 52
    target 484
  ]
  edge [
    source 52
    target 485
  ]
  edge [
    source 52
    target 486
  ]
  edge [
    source 52
    target 487
  ]
  edge [
    source 52
    target 488
  ]
  edge [
    source 53
    target 53
  ]
  edge [
    source 54
    target 489
  ]
  edge [
    source 54
    target 490
  ]
  edge [
    source 54
    target 491
  ]
]
