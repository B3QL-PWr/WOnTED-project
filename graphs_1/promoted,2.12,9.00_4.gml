graph [
  maxDegree 71
  minDegree 1
  meanDegree 1.984251968503937
  density 0.015748031496062992
  graphCliqueNumber 2
  node [
    id 0
    label "noriaki"
    origin "text"
  ]
  node [
    id 1
    label "kasai"
    origin "text"
  ]
  node [
    id 2
    label "zawody"
    origin "text"
  ]
  node [
    id 3
    label "puchar"
    origin "text"
  ]
  node [
    id 4
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 5
    label "startowa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "tak"
    origin "text"
  ]
  node [
    id 7
    label "d&#322;ugo"
    origin "text"
  ]
  node [
    id 8
    label "wydawa&#263;by"
    origin "text"
  ]
  node [
    id 9
    label "si&#281;"
    origin "text"
  ]
  node [
    id 10
    label "robi&#263;"
    origin "text"
  ]
  node [
    id 11
    label "zawsze"
    origin "text"
  ]
  node [
    id 12
    label "spadochroniarstwo"
  ]
  node [
    id 13
    label "champion"
  ]
  node [
    id 14
    label "tysi&#281;cznik"
  ]
  node [
    id 15
    label "walczenie"
  ]
  node [
    id 16
    label "kategoria_open"
  ]
  node [
    id 17
    label "walczy&#263;"
  ]
  node [
    id 18
    label "impreza"
  ]
  node [
    id 19
    label "rywalizacja"
  ]
  node [
    id 20
    label "contest"
  ]
  node [
    id 21
    label "zawarto&#347;&#263;"
  ]
  node [
    id 22
    label "zwyci&#281;stwo"
  ]
  node [
    id 23
    label "naczynie"
  ]
  node [
    id 24
    label "nagroda"
  ]
  node [
    id 25
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 26
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 27
    label "obszar"
  ]
  node [
    id 28
    label "obiekt_naturalny"
  ]
  node [
    id 29
    label "przedmiot"
  ]
  node [
    id 30
    label "biosfera"
  ]
  node [
    id 31
    label "grupa"
  ]
  node [
    id 32
    label "stw&#243;r"
  ]
  node [
    id 33
    label "Stary_&#346;wiat"
  ]
  node [
    id 34
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 35
    label "rzecz"
  ]
  node [
    id 36
    label "magnetosfera"
  ]
  node [
    id 37
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 38
    label "environment"
  ]
  node [
    id 39
    label "Nowy_&#346;wiat"
  ]
  node [
    id 40
    label "geosfera"
  ]
  node [
    id 41
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 42
    label "planeta"
  ]
  node [
    id 43
    label "przejmowa&#263;"
  ]
  node [
    id 44
    label "litosfera"
  ]
  node [
    id 45
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 46
    label "makrokosmos"
  ]
  node [
    id 47
    label "barysfera"
  ]
  node [
    id 48
    label "biota"
  ]
  node [
    id 49
    label "p&#243;&#322;noc"
  ]
  node [
    id 50
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 51
    label "fauna"
  ]
  node [
    id 52
    label "wszechstworzenie"
  ]
  node [
    id 53
    label "geotermia"
  ]
  node [
    id 54
    label "biegun"
  ]
  node [
    id 55
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 56
    label "ekosystem"
  ]
  node [
    id 57
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 58
    label "teren"
  ]
  node [
    id 59
    label "zjawisko"
  ]
  node [
    id 60
    label "p&#243;&#322;kula"
  ]
  node [
    id 61
    label "atmosfera"
  ]
  node [
    id 62
    label "mikrokosmos"
  ]
  node [
    id 63
    label "class"
  ]
  node [
    id 64
    label "po&#322;udnie"
  ]
  node [
    id 65
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 66
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 67
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 68
    label "przejmowanie"
  ]
  node [
    id 69
    label "przestrze&#324;"
  ]
  node [
    id 70
    label "asymilowanie_si&#281;"
  ]
  node [
    id 71
    label "przej&#261;&#263;"
  ]
  node [
    id 72
    label "ekosfera"
  ]
  node [
    id 73
    label "przyroda"
  ]
  node [
    id 74
    label "ciemna_materia"
  ]
  node [
    id 75
    label "geoida"
  ]
  node [
    id 76
    label "Wsch&#243;d"
  ]
  node [
    id 77
    label "populace"
  ]
  node [
    id 78
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 79
    label "huczek"
  ]
  node [
    id 80
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 81
    label "Ziemia"
  ]
  node [
    id 82
    label "universe"
  ]
  node [
    id 83
    label "ozonosfera"
  ]
  node [
    id 84
    label "rze&#378;ba"
  ]
  node [
    id 85
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 86
    label "zagranica"
  ]
  node [
    id 87
    label "hydrosfera"
  ]
  node [
    id 88
    label "woda"
  ]
  node [
    id 89
    label "kuchnia"
  ]
  node [
    id 90
    label "przej&#281;cie"
  ]
  node [
    id 91
    label "czarna_dziura"
  ]
  node [
    id 92
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 93
    label "morze"
  ]
  node [
    id 94
    label "katapultowa&#263;"
  ]
  node [
    id 95
    label "uczestniczy&#263;"
  ]
  node [
    id 96
    label "zaleca&#263;_si&#281;"
  ]
  node [
    id 97
    label "begin"
  ]
  node [
    id 98
    label "samolot"
  ]
  node [
    id 99
    label "zaczyna&#263;"
  ]
  node [
    id 100
    label "odchodzi&#263;"
  ]
  node [
    id 101
    label "d&#322;ugi"
  ]
  node [
    id 102
    label "tentegowa&#263;"
  ]
  node [
    id 103
    label "urz&#261;dza&#263;"
  ]
  node [
    id 104
    label "give"
  ]
  node [
    id 105
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 106
    label "czyni&#263;"
  ]
  node [
    id 107
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 108
    label "post&#281;powa&#263;"
  ]
  node [
    id 109
    label "wydala&#263;"
  ]
  node [
    id 110
    label "oszukiwa&#263;"
  ]
  node [
    id 111
    label "organizowa&#263;"
  ]
  node [
    id 112
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 113
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 114
    label "work"
  ]
  node [
    id 115
    label "przerabia&#263;"
  ]
  node [
    id 116
    label "stylizowa&#263;"
  ]
  node [
    id 117
    label "falowa&#263;"
  ]
  node [
    id 118
    label "act"
  ]
  node [
    id 119
    label "peddle"
  ]
  node [
    id 120
    label "ukazywa&#263;"
  ]
  node [
    id 121
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 122
    label "praca"
  ]
  node [
    id 123
    label "zaw&#380;dy"
  ]
  node [
    id 124
    label "ci&#261;gle"
  ]
  node [
    id 125
    label "na_zawsze"
  ]
  node [
    id 126
    label "cz&#281;sto"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 10
    target 105
  ]
  edge [
    source 10
    target 106
  ]
  edge [
    source 10
    target 107
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 10
    target 110
  ]
  edge [
    source 10
    target 111
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 10
    target 121
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 11
    target 123
  ]
  edge [
    source 11
    target 124
  ]
  edge [
    source 11
    target 125
  ]
  edge [
    source 11
    target 126
  ]
]
