graph [
  maxDegree 24
  minDegree 1
  meanDegree 1.92
  density 0.08
  graphCliqueNumber 2
  node [
    id 0
    label "tak"
    origin "text"
  ]
  node [
    id 1
    label "temat"
    origin "text"
  ]
  node [
    id 2
    label "fraza"
  ]
  node [
    id 3
    label "forma"
  ]
  node [
    id 4
    label "melodia"
  ]
  node [
    id 5
    label "rzecz"
  ]
  node [
    id 6
    label "zbacza&#263;"
  ]
  node [
    id 7
    label "entity"
  ]
  node [
    id 8
    label "omawia&#263;"
  ]
  node [
    id 9
    label "topik"
  ]
  node [
    id 10
    label "wyraz_pochodny"
  ]
  node [
    id 11
    label "om&#243;wi&#263;"
  ]
  node [
    id 12
    label "omawianie"
  ]
  node [
    id 13
    label "w&#261;tek"
  ]
  node [
    id 14
    label "forum"
  ]
  node [
    id 15
    label "cecha"
  ]
  node [
    id 16
    label "zboczenie"
  ]
  node [
    id 17
    label "zbaczanie"
  ]
  node [
    id 18
    label "tre&#347;&#263;"
  ]
  node [
    id 19
    label "tematyka"
  ]
  node [
    id 20
    label "sprawa"
  ]
  node [
    id 21
    label "istota"
  ]
  node [
    id 22
    label "otoczka"
  ]
  node [
    id 23
    label "zboczy&#263;"
  ]
  node [
    id 24
    label "om&#243;wienie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
]
