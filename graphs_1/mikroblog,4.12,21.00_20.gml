graph [
  maxDegree 36
  minDegree 1
  meanDegree 2.0428571428571427
  density 0.014696813977389518
  graphCliqueNumber 3
  node [
    id 0
    label "polecie&#263;"
    origin "text"
  ]
  node [
    id 1
    label "klasyk"
    origin "text"
  ]
  node [
    id 2
    label "dzisiaj"
    origin "text"
  ]
  node [
    id 3
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 4
    label "p&#243;j&#347;&#263;"
    origin "text"
  ]
  node [
    id 5
    label "tak"
    origin "text"
  ]
  node [
    id 6
    label "gdy"
    origin "text"
  ]
  node [
    id 7
    label "montowa&#263;"
    origin "text"
  ]
  node [
    id 8
    label "dwa"
    origin "text"
  ]
  node [
    id 9
    label "urz&#261;dzenie"
    origin "text"
  ]
  node [
    id 10
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 11
    label "otwiera&#263;"
    origin "text"
  ]
  node [
    id 12
    label "si&#281;"
    origin "text"
  ]
  node [
    id 13
    label "pod"
    origin "text"
  ]
  node [
    id 14
    label "wp&#322;yw"
    origin "text"
  ]
  node [
    id 15
    label "ruch"
    origin "text"
  ]
  node [
    id 16
    label "induce"
  ]
  node [
    id 17
    label "pobiec"
  ]
  node [
    id 18
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 19
    label "pogna&#263;"
  ]
  node [
    id 20
    label "decline"
  ]
  node [
    id 21
    label "spierdoli&#263;_si&#281;"
  ]
  node [
    id 22
    label "ruszy&#263;"
  ]
  node [
    id 23
    label "schrzani&#263;_si&#281;"
  ]
  node [
    id 24
    label "Mozart"
  ]
  node [
    id 25
    label "przedstawiciel"
  ]
  node [
    id 26
    label "Beethoven"
  ]
  node [
    id 27
    label "hit"
  ]
  node [
    id 28
    label "Haydn"
  ]
  node [
    id 29
    label "artysta"
  ]
  node [
    id 30
    label "filolog"
  ]
  node [
    id 31
    label "doba"
  ]
  node [
    id 32
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 33
    label "dzi&#347;"
  ]
  node [
    id 34
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 35
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 36
    label "opu&#347;ci&#263;"
  ]
  node [
    id 37
    label "poruszy&#263;_si&#281;"
  ]
  node [
    id 38
    label "proceed"
  ]
  node [
    id 39
    label "powie&#347;&#263;_si&#281;"
  ]
  node [
    id 40
    label "popsu&#263;_si&#281;"
  ]
  node [
    id 41
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 42
    label "kopn&#261;&#263;_si&#281;"
  ]
  node [
    id 43
    label "zacz&#261;&#263;"
  ]
  node [
    id 44
    label "zmieni&#263;"
  ]
  node [
    id 45
    label "zosta&#263;"
  ]
  node [
    id 46
    label "sail"
  ]
  node [
    id 47
    label "leave"
  ]
  node [
    id 48
    label "uda&#263;_si&#281;"
  ]
  node [
    id 49
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 50
    label "zrobi&#263;"
  ]
  node [
    id 51
    label "wyp&#322;yn&#261;&#263;"
  ]
  node [
    id 52
    label "sprzeda&#263;_si&#281;"
  ]
  node [
    id 53
    label "przyj&#261;&#263;"
  ]
  node [
    id 54
    label "potoczy&#263;_si&#281;"
  ]
  node [
    id 55
    label "become"
  ]
  node [
    id 56
    label "play_along"
  ]
  node [
    id 57
    label "travel"
  ]
  node [
    id 58
    label "sk&#322;ada&#263;"
  ]
  node [
    id 59
    label "konstruowa&#263;"
  ]
  node [
    id 60
    label "umieszcza&#263;"
  ]
  node [
    id 61
    label "tworzy&#263;"
  ]
  node [
    id 62
    label "raise"
  ]
  node [
    id 63
    label "supply"
  ]
  node [
    id 64
    label "organizowa&#263;"
  ]
  node [
    id 65
    label "scala&#263;"
  ]
  node [
    id 66
    label "przedmiot"
  ]
  node [
    id 67
    label "sprz&#281;t"
  ]
  node [
    id 68
    label "blokowanie"
  ]
  node [
    id 69
    label "zabezpieczenie"
  ]
  node [
    id 70
    label "kom&#243;rka"
  ]
  node [
    id 71
    label "narz&#281;dzie"
  ]
  node [
    id 72
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 73
    label "set"
  ]
  node [
    id 74
    label "komora"
  ]
  node [
    id 75
    label "j&#281;zyk"
  ]
  node [
    id 76
    label "aparatura"
  ]
  node [
    id 77
    label "zagospodarowanie"
  ]
  node [
    id 78
    label "przygotowanie"
  ]
  node [
    id 79
    label "zrobienie"
  ]
  node [
    id 80
    label "czynno&#347;&#263;"
  ]
  node [
    id 81
    label "impulsator"
  ]
  node [
    id 82
    label "system_energetyczny"
  ]
  node [
    id 83
    label "mechanizm"
  ]
  node [
    id 84
    label "wyrz&#261;dzenie"
  ]
  node [
    id 85
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 86
    label "furnishing"
  ]
  node [
    id 87
    label "ig&#322;a"
  ]
  node [
    id 88
    label "zablokowanie"
  ]
  node [
    id 89
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 90
    label "wirnik"
  ]
  node [
    id 91
    label "cia&#322;o"
  ]
  node [
    id 92
    label "begin"
  ]
  node [
    id 93
    label "train"
  ]
  node [
    id 94
    label "uruchamia&#263;"
  ]
  node [
    id 95
    label "rozk&#322;ada&#263;"
  ]
  node [
    id 96
    label "robi&#263;"
  ]
  node [
    id 97
    label "zaczyna&#263;"
  ]
  node [
    id 98
    label "unboxing"
  ]
  node [
    id 99
    label "przecina&#263;"
  ]
  node [
    id 100
    label "powodowa&#263;"
  ]
  node [
    id 101
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 102
    label "&#347;lad"
  ]
  node [
    id 103
    label "doch&#243;d_narodowy"
  ]
  node [
    id 104
    label "zjawisko"
  ]
  node [
    id 105
    label "rezultat"
  ]
  node [
    id 106
    label "kwota"
  ]
  node [
    id 107
    label "lobbysta"
  ]
  node [
    id 108
    label "manewr"
  ]
  node [
    id 109
    label "model"
  ]
  node [
    id 110
    label "movement"
  ]
  node [
    id 111
    label "apraksja"
  ]
  node [
    id 112
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 113
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 114
    label "poruszenie"
  ]
  node [
    id 115
    label "commercial_enterprise"
  ]
  node [
    id 116
    label "dyssypacja_energii"
  ]
  node [
    id 117
    label "zmiana"
  ]
  node [
    id 118
    label "utrzymanie"
  ]
  node [
    id 119
    label "utrzyma&#263;"
  ]
  node [
    id 120
    label "komunikacja"
  ]
  node [
    id 121
    label "tumult"
  ]
  node [
    id 122
    label "kr&#243;tki"
  ]
  node [
    id 123
    label "drift"
  ]
  node [
    id 124
    label "utrzymywa&#263;"
  ]
  node [
    id 125
    label "stopek"
  ]
  node [
    id 126
    label "kanciasty"
  ]
  node [
    id 127
    label "d&#322;ugi"
  ]
  node [
    id 128
    label "utrzymywanie"
  ]
  node [
    id 129
    label "myk"
  ]
  node [
    id 130
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 131
    label "wydarzenie"
  ]
  node [
    id 132
    label "taktyka"
  ]
  node [
    id 133
    label "move"
  ]
  node [
    id 134
    label "natural_process"
  ]
  node [
    id 135
    label "lokomocja"
  ]
  node [
    id 136
    label "mechanika"
  ]
  node [
    id 137
    label "proces"
  ]
  node [
    id 138
    label "strumie&#324;"
  ]
  node [
    id 139
    label "aktywno&#347;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 11
    target 92
  ]
  edge [
    source 11
    target 93
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 11
    target 98
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 11
    target 100
  ]
  edge [
    source 11
    target 101
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 102
  ]
  edge [
    source 14
    target 103
  ]
  edge [
    source 14
    target 104
  ]
  edge [
    source 14
    target 105
  ]
  edge [
    source 14
    target 106
  ]
  edge [
    source 14
    target 107
  ]
  edge [
    source 15
    target 108
  ]
  edge [
    source 15
    target 109
  ]
  edge [
    source 15
    target 110
  ]
  edge [
    source 15
    target 111
  ]
  edge [
    source 15
    target 112
  ]
  edge [
    source 15
    target 113
  ]
  edge [
    source 15
    target 114
  ]
  edge [
    source 15
    target 115
  ]
  edge [
    source 15
    target 116
  ]
  edge [
    source 15
    target 117
  ]
  edge [
    source 15
    target 118
  ]
  edge [
    source 15
    target 119
  ]
  edge [
    source 15
    target 120
  ]
  edge [
    source 15
    target 121
  ]
  edge [
    source 15
    target 122
  ]
  edge [
    source 15
    target 123
  ]
  edge [
    source 15
    target 124
  ]
  edge [
    source 15
    target 125
  ]
  edge [
    source 15
    target 126
  ]
  edge [
    source 15
    target 127
  ]
  edge [
    source 15
    target 104
  ]
  edge [
    source 15
    target 128
  ]
  edge [
    source 15
    target 80
  ]
  edge [
    source 15
    target 129
  ]
  edge [
    source 15
    target 130
  ]
  edge [
    source 15
    target 131
  ]
  edge [
    source 15
    target 132
  ]
  edge [
    source 15
    target 133
  ]
  edge [
    source 15
    target 134
  ]
  edge [
    source 15
    target 135
  ]
  edge [
    source 15
    target 136
  ]
  edge [
    source 15
    target 137
  ]
  edge [
    source 15
    target 138
  ]
  edge [
    source 15
    target 139
  ]
  edge [
    source 15
    target 57
  ]
]
