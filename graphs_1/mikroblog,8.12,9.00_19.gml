graph [
  maxDegree 3
  minDegree 1
  meanDegree 1.6
  density 0.4
  graphCliqueNumber 2
  node [
    id 0
    label "potem"
    origin "text"
  ]
  node [
    id 1
    label "rozsta&#263;"
    origin "text"
  ]
  node [
    id 2
    label "razem"
    origin "text"
  ]
  node [
    id 3
    label "p&#322;aka&#322;i"
    origin "text"
  ]
  node [
    id 4
    label "&#322;&#261;cznie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
]
