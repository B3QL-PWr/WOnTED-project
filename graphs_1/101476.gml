graph [
  maxDegree 5
  minDegree 1
  meanDegree 1.7142857142857142
  density 0.2857142857142857
  graphCliqueNumber 2
  node [
    id 0
    label "borek"
    origin "text"
  ]
  node [
    id 1
    label "powiat"
    origin "text"
  ]
  node [
    id 2
    label "podd&#281;bicki"
    origin "text"
  ]
  node [
    id 3
    label "zwi&#261;zek_nieorganiczny"
  ]
  node [
    id 4
    label "gmina"
  ]
  node [
    id 5
    label "jednostka_administracyjna"
  ]
  node [
    id 6
    label "wojew&#243;dztwo"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
]
