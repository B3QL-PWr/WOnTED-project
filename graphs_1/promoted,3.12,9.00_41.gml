graph [
  maxDegree 17
  minDegree 1
  meanDegree 1.9487179487179487
  density 0.05128205128205128
  graphCliqueNumber 2
  node [
    id 0
    label "r&#243;&#380;nica"
    origin "text"
  ]
  node [
    id 1
    label "mi&#281;dzy"
    origin "text"
  ]
  node [
    id 2
    label "podlewa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "choinka"
    origin "text"
  ]
  node [
    id 4
    label "wysuszy&#263;"
    origin "text"
  ]
  node [
    id 5
    label "podczas"
    origin "text"
  ]
  node [
    id 6
    label "po&#380;ar"
    origin "text"
  ]
  node [
    id 7
    label "wynik"
  ]
  node [
    id 8
    label "r&#243;&#380;nienie"
  ]
  node [
    id 9
    label "cecha"
  ]
  node [
    id 10
    label "kontrastowy"
  ]
  node [
    id 11
    label "discord"
  ]
  node [
    id 12
    label "nawil&#380;a&#263;"
  ]
  node [
    id 13
    label "wlewa&#263;"
  ]
  node [
    id 14
    label "chwoja"
  ]
  node [
    id 15
    label "przedmiot"
  ]
  node [
    id 16
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 17
    label "iglak"
  ]
  node [
    id 18
    label "fir"
  ]
  node [
    id 19
    label "drzewo"
  ]
  node [
    id 20
    label "przyczyni&#263;_si&#281;"
  ]
  node [
    id 21
    label "zmieni&#263;_w&#322;a&#347;ciwo&#347;ci_fizyczne_lub_chemiczne_czego&#347;"
  ]
  node [
    id 22
    label "spowodowa&#263;"
  ]
  node [
    id 23
    label "kryzys"
  ]
  node [
    id 24
    label "zap&#322;on"
  ]
  node [
    id 25
    label "miazmaty"
  ]
  node [
    id 26
    label "zalew"
  ]
  node [
    id 27
    label "podpalenie"
  ]
  node [
    id 28
    label "wojna"
  ]
  node [
    id 29
    label "p&#322;omie&#324;"
  ]
  node [
    id 30
    label "stra&#380;ak"
  ]
  node [
    id 31
    label "nawa&#322;no&#347;&#263;"
  ]
  node [
    id 32
    label "fire"
  ]
  node [
    id 33
    label "kl&#281;ska"
  ]
  node [
    id 34
    label "wydarzenie"
  ]
  node [
    id 35
    label "zapr&#243;szenie"
  ]
  node [
    id 36
    label "pogorzelec"
  ]
  node [
    id 37
    label "ogie&#324;"
  ]
  node [
    id 38
    label "przyp&#322;yw"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 23
  ]
  edge [
    source 6
    target 24
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
]
