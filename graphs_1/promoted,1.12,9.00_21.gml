graph [
  maxDegree 25
  minDegree 1
  meanDegree 1.9692307692307693
  density 0.03076923076923077
  graphCliqueNumber 2
  node [
    id 0
    label "media"
    origin "text"
  ]
  node [
    id 1
    label "spo&#322;eczno&#347;ciowy"
    origin "text"
  ]
  node [
    id 2
    label "zamieszcza&#263;"
    origin "text"
  ]
  node [
    id 3
    label "apel"
    origin "text"
  ]
  node [
    id 4
    label "policja"
    origin "text"
  ]
  node [
    id 5
    label "zaprzestanie"
    origin "text"
  ]
  node [
    id 6
    label "strzelanie"
    origin "text"
  ]
  node [
    id 7
    label "czarna"
    origin "text"
  ]
  node [
    id 8
    label "przekazior"
  ]
  node [
    id 9
    label "mass-media"
  ]
  node [
    id 10
    label "uzbrajanie"
  ]
  node [
    id 11
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 12
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 13
    label "medium"
  ]
  node [
    id 14
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 15
    label "medialny"
  ]
  node [
    id 16
    label "publiczny"
  ]
  node [
    id 17
    label "umieszcza&#263;"
  ]
  node [
    id 18
    label "publikowa&#263;"
  ]
  node [
    id 19
    label "uderzenie"
  ]
  node [
    id 20
    label "zdyscyplinowanie"
  ]
  node [
    id 21
    label "sygna&#322;"
  ]
  node [
    id 22
    label "bid"
  ]
  node [
    id 23
    label "recoil"
  ]
  node [
    id 24
    label "spotkanie"
  ]
  node [
    id 25
    label "zbi&#243;rka"
  ]
  node [
    id 26
    label "pro&#347;ba"
  ]
  node [
    id 27
    label "pies_my&#347;liwski"
  ]
  node [
    id 28
    label "znak"
  ]
  node [
    id 29
    label "szermierka"
  ]
  node [
    id 30
    label "komisariat"
  ]
  node [
    id 31
    label "psiarnia"
  ]
  node [
    id 32
    label "posterunek"
  ]
  node [
    id 33
    label "grupa"
  ]
  node [
    id 34
    label "organ"
  ]
  node [
    id 35
    label "s&#322;u&#380;ba"
  ]
  node [
    id 36
    label "oduczenie"
  ]
  node [
    id 37
    label "disavowal"
  ]
  node [
    id 38
    label "zako&#324;czenie"
  ]
  node [
    id 39
    label "odpalanie"
  ]
  node [
    id 40
    label "wystrzelanie"
  ]
  node [
    id 41
    label "trafianie"
  ]
  node [
    id 42
    label "ostrzeliwanie"
  ]
  node [
    id 43
    label "odstrzelenie"
  ]
  node [
    id 44
    label "plucie"
  ]
  node [
    id 45
    label "palenie"
  ]
  node [
    id 46
    label "kropni&#281;cie"
  ]
  node [
    id 47
    label "wstrzeliwanie_si&#281;"
  ]
  node [
    id 48
    label "uderzanie"
  ]
  node [
    id 49
    label "wylatywanie"
  ]
  node [
    id 50
    label "prze&#322;adowywanie"
  ]
  node [
    id 51
    label "zestrzelenie"
  ]
  node [
    id 52
    label "grzanie"
  ]
  node [
    id 53
    label "fire"
  ]
  node [
    id 54
    label "chybienie"
  ]
  node [
    id 55
    label "odstrzeliwanie"
  ]
  node [
    id 56
    label "przestrzeliwanie"
  ]
  node [
    id 57
    label "postrzelanie"
  ]
  node [
    id 58
    label "zestrzeliwanie"
  ]
  node [
    id 59
    label "walczenie"
  ]
  node [
    id 60
    label "ostrzelanie"
  ]
  node [
    id 61
    label "chybianie"
  ]
  node [
    id 62
    label "kawa"
  ]
  node [
    id 63
    label "czarny"
  ]
  node [
    id 64
    label "murzynek"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
]
