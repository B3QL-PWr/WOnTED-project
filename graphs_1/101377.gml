graph [
  maxDegree 6
  minDegree 1
  meanDegree 1.7777777777777777
  density 0.2222222222222222
  graphCliqueNumber 3
  node [
    id 0
    label "audi"
    origin "text"
  ]
  node [
    id 1
    label "samoch&#243;d_osobowy"
  ]
  node [
    id 2
    label "Audi"
  ]
  node [
    id 3
    label "samoch&#243;d"
  ]
  node [
    id 4
    label "A3"
  ]
  node [
    id 5
    label "volkswagen"
  ]
  node [
    id 6
    label "golf"
  ]
  node [
    id 7
    label "Cabrio"
  ]
  node [
    id 8
    label "A5"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 5
    target 6
  ]
]
