graph [
  maxDegree 31
  minDegree 1
  meanDegree 1.948051948051948
  density 0.02563226247436774
  graphCliqueNumber 2
  node [
    id 0
    label "&#380;y&#263;"
    origin "text"
  ]
  node [
    id 1
    label "poszukiwany"
    origin "text"
  ]
  node [
    id 2
    label "roksana"
    origin "text"
  ]
  node [
    id 3
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 4
    label "zagin&#261;&#263;"
    origin "text"
  ]
  node [
    id 5
    label "dwa"
    origin "text"
  ]
  node [
    id 6
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 7
    label "&#347;lub"
    origin "text"
  ]
  node [
    id 8
    label "ustali&#263;"
    origin "text"
  ]
  node [
    id 9
    label "wirtualny"
    origin "text"
  ]
  node [
    id 10
    label "polska"
    origin "text"
  ]
  node [
    id 11
    label "pause"
  ]
  node [
    id 12
    label "stay"
  ]
  node [
    id 13
    label "consist"
  ]
  node [
    id 14
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 15
    label "zachowywa&#263;_si&#281;"
  ]
  node [
    id 16
    label "istnie&#263;"
  ]
  node [
    id 17
    label "podejrzany"
  ]
  node [
    id 18
    label "cenny"
  ]
  node [
    id 19
    label "fail"
  ]
  node [
    id 20
    label "zgubi&#263;_si&#281;"
  ]
  node [
    id 21
    label "fly"
  ]
  node [
    id 22
    label "os&#322;abn&#261;&#263;"
  ]
  node [
    id 23
    label "znikn&#261;&#263;"
  ]
  node [
    id 24
    label "die"
  ]
  node [
    id 25
    label "s&#322;o&#324;ce"
  ]
  node [
    id 26
    label "czynienie_si&#281;"
  ]
  node [
    id 27
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 28
    label "czas"
  ]
  node [
    id 29
    label "long_time"
  ]
  node [
    id 30
    label "przedpo&#322;udnie"
  ]
  node [
    id 31
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 32
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 33
    label "tydzie&#324;"
  ]
  node [
    id 34
    label "godzina"
  ]
  node [
    id 35
    label "t&#322;usty_czwartek"
  ]
  node [
    id 36
    label "wsta&#263;"
  ]
  node [
    id 37
    label "day"
  ]
  node [
    id 38
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 39
    label "przedwiecz&#243;r"
  ]
  node [
    id 40
    label "Sylwester"
  ]
  node [
    id 41
    label "po&#322;udnie"
  ]
  node [
    id 42
    label "wzej&#347;cie"
  ]
  node [
    id 43
    label "podwiecz&#243;r"
  ]
  node [
    id 44
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 45
    label "rano"
  ]
  node [
    id 46
    label "termin"
  ]
  node [
    id 47
    label "ranek"
  ]
  node [
    id 48
    label "doba"
  ]
  node [
    id 49
    label "wiecz&#243;r"
  ]
  node [
    id 50
    label "walentynki"
  ]
  node [
    id 51
    label "popo&#322;udnie"
  ]
  node [
    id 52
    label "noc"
  ]
  node [
    id 53
    label "wstanie"
  ]
  node [
    id 54
    label "przysi&#281;ga"
  ]
  node [
    id 55
    label "wedding"
  ]
  node [
    id 56
    label "&#347;lubowanie"
  ]
  node [
    id 57
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 58
    label "chupa"
  ]
  node [
    id 59
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 60
    label "po&#347;lubiny"
  ]
  node [
    id 61
    label "zapowiedzi"
  ]
  node [
    id 62
    label "vow"
  ]
  node [
    id 63
    label "dru&#380;ba"
  ]
  node [
    id 64
    label "umocni&#263;"
  ]
  node [
    id 65
    label "bind"
  ]
  node [
    id 66
    label "zdecydowa&#263;"
  ]
  node [
    id 67
    label "spowodowa&#263;"
  ]
  node [
    id 68
    label "unwrap"
  ]
  node [
    id 69
    label "zrobi&#263;"
  ]
  node [
    id 70
    label "put"
  ]
  node [
    id 71
    label "mo&#380;liwy"
  ]
  node [
    id 72
    label "wirtualnie"
  ]
  node [
    id 73
    label "nieprawdziwy"
  ]
  node [
    id 74
    label "Roksana"
  ]
  node [
    id 75
    label "R"
  ]
  node [
    id 76
    label "Polska"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 74
    target 75
  ]
]
