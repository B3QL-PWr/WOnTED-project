graph [
  maxDegree 8
  minDegree 1
  meanDegree 1.8461538461538463
  density 0.15384615384615385
  graphCliqueNumber 2
  node [
    id 0
    label "polska"
    origin "text"
  ]
  node [
    id 1
    label "przybywa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "singel"
    origin "text"
  ]
  node [
    id 3
    label "zyskiwa&#263;"
  ]
  node [
    id 4
    label "get"
  ]
  node [
    id 5
    label "dociera&#263;"
  ]
  node [
    id 6
    label "nie&#380;onaty"
  ]
  node [
    id 7
    label "singiel"
  ]
  node [
    id 8
    label "p&#322;yta"
  ]
  node [
    id 9
    label "stan_wolny"
  ]
  node [
    id 10
    label "piosenka"
  ]
  node [
    id 11
    label "mecz"
  ]
  node [
    id 12
    label "karta"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
]
