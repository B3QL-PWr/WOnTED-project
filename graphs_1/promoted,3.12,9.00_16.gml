graph [
  maxDegree 20
  minDegree 1
  meanDegree 2
  density 0.03225806451612903
  graphCliqueNumber 3
  node [
    id 0
    label "lato"
    origin "text"
  ]
  node [
    id 1
    label "chiny"
    origin "text"
  ]
  node [
    id 2
    label "indie"
    origin "text"
  ]
  node [
    id 3
    label "wybudowa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "elektrownia"
    origin "text"
  ]
  node [
    id 5
    label "w&#281;glowy"
    origin "text"
  ]
  node [
    id 6
    label "&#322;&#261;czny"
    origin "text"
  ]
  node [
    id 7
    label "moc"
    origin "text"
  ]
  node [
    id 8
    label "gazeta_wyborcza"
    origin "text"
  ]
  node [
    id 9
    label "usa"
    origin "text"
  ]
  node [
    id 10
    label "ca&#322;y"
    origin "text"
  ]
  node [
    id 11
    label "w&#281;giel"
    origin "text"
  ]
  node [
    id 12
    label "pora_roku"
  ]
  node [
    id 13
    label "wytworzy&#263;"
  ]
  node [
    id 14
    label "establish"
  ]
  node [
    id 15
    label "budowla"
  ]
  node [
    id 16
    label "zak&#322;ad_komunalny"
  ]
  node [
    id 17
    label "w&#281;gielny"
  ]
  node [
    id 18
    label "w&#281;glisty"
  ]
  node [
    id 19
    label "&#322;&#261;cznie"
  ]
  node [
    id 20
    label "zbiorczy"
  ]
  node [
    id 21
    label "wa&#380;no&#347;&#263;"
  ]
  node [
    id 22
    label "nasycenie"
  ]
  node [
    id 23
    label "wuchta"
  ]
  node [
    id 24
    label "parametr"
  ]
  node [
    id 25
    label "immunity"
  ]
  node [
    id 26
    label "zdolno&#347;&#263;"
  ]
  node [
    id 27
    label "wytrzyma&#322;o&#347;&#263;"
  ]
  node [
    id 28
    label "mn&#243;stwo"
  ]
  node [
    id 29
    label "poj&#281;cie"
  ]
  node [
    id 30
    label "potencja"
  ]
  node [
    id 31
    label "izotonia"
  ]
  node [
    id 32
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 33
    label "nefelometria"
  ]
  node [
    id 34
    label "du&#380;y"
  ]
  node [
    id 35
    label "jedyny"
  ]
  node [
    id 36
    label "kompletny"
  ]
  node [
    id 37
    label "zdr&#243;w"
  ]
  node [
    id 38
    label "&#380;ywy"
  ]
  node [
    id 39
    label "ca&#322;o"
  ]
  node [
    id 40
    label "pe&#322;ny"
  ]
  node [
    id 41
    label "calu&#347;ko"
  ]
  node [
    id 42
    label "podobny"
  ]
  node [
    id 43
    label "surowiec_energetyczny"
  ]
  node [
    id 44
    label "w&#281;glowiec"
  ]
  node [
    id 45
    label "w&#281;glowodan"
  ]
  node [
    id 46
    label "kopalina_podstawowa"
  ]
  node [
    id 47
    label "coal"
  ]
  node [
    id 48
    label "przybory_do_pisania"
  ]
  node [
    id 49
    label "makroelement"
  ]
  node [
    id 50
    label "niemetal"
  ]
  node [
    id 51
    label "zsypnik"
  ]
  node [
    id 52
    label "w&#281;glarka"
  ]
  node [
    id 53
    label "zw&#281;glenie_si&#281;"
  ]
  node [
    id 54
    label "zw&#281;glanie_si&#281;"
  ]
  node [
    id 55
    label "coil"
  ]
  node [
    id 56
    label "bry&#322;a"
  ]
  node [
    id 57
    label "ska&#322;a"
  ]
  node [
    id 58
    label "carbon"
  ]
  node [
    id 59
    label "fulleren"
  ]
  node [
    id 60
    label "rysunek"
  ]
  node [
    id 61
    label "432"
  ]
  node [
    id 62
    label "GazetaWyborcza"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 19
  ]
  edge [
    source 6
    target 20
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 10
  ]
  edge [
    source 7
    target 11
  ]
  edge [
    source 7
    target 21
  ]
  edge [
    source 7
    target 22
  ]
  edge [
    source 7
    target 23
  ]
  edge [
    source 7
    target 24
  ]
  edge [
    source 7
    target 25
  ]
  edge [
    source 7
    target 26
  ]
  edge [
    source 7
    target 27
  ]
  edge [
    source 7
    target 28
  ]
  edge [
    source 7
    target 29
  ]
  edge [
    source 7
    target 30
  ]
  edge [
    source 7
    target 31
  ]
  edge [
    source 7
    target 32
  ]
  edge [
    source 7
    target 33
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 11
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 34
  ]
  edge [
    source 10
    target 35
  ]
  edge [
    source 10
    target 36
  ]
  edge [
    source 10
    target 37
  ]
  edge [
    source 10
    target 38
  ]
  edge [
    source 10
    target 39
  ]
  edge [
    source 10
    target 40
  ]
  edge [
    source 10
    target 41
  ]
  edge [
    source 10
    target 42
  ]
  edge [
    source 11
    target 43
  ]
  edge [
    source 11
    target 44
  ]
  edge [
    source 11
    target 45
  ]
  edge [
    source 11
    target 46
  ]
  edge [
    source 11
    target 47
  ]
  edge [
    source 11
    target 48
  ]
  edge [
    source 11
    target 49
  ]
  edge [
    source 11
    target 50
  ]
  edge [
    source 11
    target 51
  ]
  edge [
    source 11
    target 52
  ]
  edge [
    source 11
    target 53
  ]
  edge [
    source 11
    target 54
  ]
  edge [
    source 11
    target 55
  ]
  edge [
    source 11
    target 56
  ]
  edge [
    source 11
    target 57
  ]
  edge [
    source 11
    target 58
  ]
  edge [
    source 11
    target 59
  ]
  edge [
    source 11
    target 60
  ]
  edge [
    source 61
    target 62
  ]
]
