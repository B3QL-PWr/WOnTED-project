graph [
  maxDegree 3
  minDegree 1
  meanDegree 1.6
  density 0.4
  graphCliqueNumber 2
  node [
    id 0
    label "siemaneczko"
    origin "text"
  ]
  node [
    id 1
    label "mireczka"
    origin "text"
  ]
  node [
    id 2
    label "mirabelka"
    origin "text"
  ]
  node [
    id 3
    label "&#347;liwka"
  ]
  node [
    id 4
    label "&#347;liwa_domowa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
]
