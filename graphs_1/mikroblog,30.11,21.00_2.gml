graph [
  maxDegree 31
  minDegree 1
  meanDegree 2.2595419847328246
  density 0.005764137716155165
  graphCliqueNumber 3
  node [
    id 0
    label "dosta&#263;"
    origin "text"
  ]
  node [
    id 1
    label "podwy&#380;ka"
    origin "text"
  ]
  node [
    id 2
    label "umowa"
    origin "text"
  ]
  node [
    id 3
    label "nieokre&#347;lony"
    origin "text"
  ]
  node [
    id 4
    label "hehe"
    origin "text"
  ]
  node [
    id 5
    label "nie"
    origin "text"
  ]
  node [
    id 6
    label "brak"
    origin "text"
  ]
  node [
    id 7
    label "tym"
    origin "text"
  ]
  node [
    id 8
    label "m&#322;ody"
    origin "text"
  ]
  node [
    id 9
    label "ciesa&#263;"
    origin "text"
  ]
  node [
    id 10
    label "robota"
    origin "text"
  ]
  node [
    id 11
    label "mie&#263;'"
    origin "text"
  ]
  node [
    id 12
    label "tw&#243;j"
    origin "text"
  ]
  node [
    id 13
    label "pracodawca"
    origin "text"
  ]
  node [
    id 14
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 15
    label "zaczyna&#263;"
    origin "text"
  ]
  node [
    id 16
    label "poniedzia&#322;ek"
    origin "text"
  ]
  node [
    id 17
    label "delikatnie"
    origin "text"
  ]
  node [
    id 18
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 19
    label "podziela&#263;"
    origin "text"
  ]
  node [
    id 20
    label "poprzedni"
    origin "text"
  ]
  node [
    id 21
    label "ostatni"
    origin "text"
  ]
  node [
    id 22
    label "stara"
    origin "text"
  ]
  node [
    id 23
    label "robot"
    origin "text"
  ]
  node [
    id 24
    label "&#380;eby"
    origin "text"
  ]
  node [
    id 25
    label "dopi&#261;&#263;"
    origin "text"
  ]
  node [
    id 26
    label "formalno&#347;&#263;"
    origin "text"
  ]
  node [
    id 27
    label "po&#380;egna&#263;"
    origin "text"
  ]
  node [
    id 28
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 29
    label "prze&#322;o&#380;ony"
    origin "text"
  ]
  node [
    id 30
    label "oznajmia&#263;"
    origin "text"
  ]
  node [
    id 31
    label "nowa"
    origin "text"
  ]
  node [
    id 32
    label "praca"
    origin "text"
  ]
  node [
    id 33
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 34
    label "zadzwoni&#263;"
    origin "text"
  ]
  node [
    id 35
    label "przyj&#347;&#263;"
    origin "text"
  ]
  node [
    id 36
    label "dokument"
    origin "text"
  ]
  node [
    id 37
    label "pismo"
    origin "text"
  ]
  node [
    id 38
    label "jako"
    origin "text"
  ]
  node [
    id 39
    label "jedyny"
    origin "text"
  ]
  node [
    id 40
    label "temat"
    origin "text"
  ]
  node [
    id 41
    label "da&#263;"
    origin "text"
  ]
  node [
    id 42
    label "chujowy"
    origin "text"
  ]
  node [
    id 43
    label "bajeczny"
    origin "text"
  ]
  node [
    id 44
    label "lubi&#263;"
    origin "text"
  ]
  node [
    id 45
    label "wedlowski"
    origin "text"
  ]
  node [
    id 46
    label "jaka"
    origin "text"
  ]
  node [
    id 47
    label "przynie&#347;&#263;by&#263;"
    origin "text"
  ]
  node [
    id 48
    label "wyj&#347;&#263;"
    origin "text"
  ]
  node [
    id 49
    label "nic"
    origin "text"
  ]
  node [
    id 50
    label "get"
  ]
  node [
    id 51
    label "doczeka&#263;"
  ]
  node [
    id 52
    label "zwiastun"
  ]
  node [
    id 53
    label "si&#281;gn&#261;&#263;"
  ]
  node [
    id 54
    label "develop"
  ]
  node [
    id 55
    label "catch"
  ]
  node [
    id 56
    label "uzyska&#263;"
  ]
  node [
    id 57
    label "kupi&#263;"
  ]
  node [
    id 58
    label "wzi&#261;&#263;"
  ]
  node [
    id 59
    label "naby&#263;"
  ]
  node [
    id 60
    label "nabawienie_si&#281;"
  ]
  node [
    id 61
    label "obskoczy&#263;"
  ]
  node [
    id 62
    label "zapanowa&#263;"
  ]
  node [
    id 63
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 64
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 65
    label "zrobi&#263;"
  ]
  node [
    id 66
    label "nabawianie_si&#281;"
  ]
  node [
    id 67
    label "range"
  ]
  node [
    id 68
    label "schorzenie"
  ]
  node [
    id 69
    label "wystarczy&#263;"
  ]
  node [
    id 70
    label "wysta&#263;"
  ]
  node [
    id 71
    label "wzrost"
  ]
  node [
    id 72
    label "czyn"
  ]
  node [
    id 73
    label "contract"
  ]
  node [
    id 74
    label "gestia_transportowa"
  ]
  node [
    id 75
    label "zawrze&#263;"
  ]
  node [
    id 76
    label "klauzula"
  ]
  node [
    id 77
    label "porozumienie"
  ]
  node [
    id 78
    label "warunek"
  ]
  node [
    id 79
    label "zawarcie"
  ]
  node [
    id 80
    label "zamazywanie"
  ]
  node [
    id 81
    label "zamazanie"
  ]
  node [
    id 82
    label "niewyrazisto"
  ]
  node [
    id 83
    label "sprzeciw"
  ]
  node [
    id 84
    label "prywatywny"
  ]
  node [
    id 85
    label "defect"
  ]
  node [
    id 86
    label "odej&#347;cie"
  ]
  node [
    id 87
    label "gap"
  ]
  node [
    id 88
    label "kr&#243;tki"
  ]
  node [
    id 89
    label "wyr&#243;b"
  ]
  node [
    id 90
    label "nieistnienie"
  ]
  node [
    id 91
    label "wada"
  ]
  node [
    id 92
    label "odej&#347;&#263;"
  ]
  node [
    id 93
    label "odchodzenie"
  ]
  node [
    id 94
    label "odchodzi&#263;"
  ]
  node [
    id 95
    label "nie&#380;onaty"
  ]
  node [
    id 96
    label "wczesny"
  ]
  node [
    id 97
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 98
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 99
    label "charakterystyczny"
  ]
  node [
    id 100
    label "nowo&#380;eniec"
  ]
  node [
    id 101
    label "m&#261;&#380;"
  ]
  node [
    id 102
    label "m&#322;odo"
  ]
  node [
    id 103
    label "nowy"
  ]
  node [
    id 104
    label "stosunek_pracy"
  ]
  node [
    id 105
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 106
    label "benedykty&#324;ski"
  ]
  node [
    id 107
    label "pracowanie"
  ]
  node [
    id 108
    label "zaw&#243;d"
  ]
  node [
    id 109
    label "kierownictwo"
  ]
  node [
    id 110
    label "zmiana"
  ]
  node [
    id 111
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 112
    label "tynkarski"
  ]
  node [
    id 113
    label "czynnik_produkcji"
  ]
  node [
    id 114
    label "zobowi&#261;zanie"
  ]
  node [
    id 115
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 116
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 117
    label "czynno&#347;&#263;"
  ]
  node [
    id 118
    label "tyrka"
  ]
  node [
    id 119
    label "pracowa&#263;"
  ]
  node [
    id 120
    label "siedziba"
  ]
  node [
    id 121
    label "poda&#380;_pracy"
  ]
  node [
    id 122
    label "miejsce"
  ]
  node [
    id 123
    label "zak&#322;ad"
  ]
  node [
    id 124
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 125
    label "najem"
  ]
  node [
    id 126
    label "czyj&#347;"
  ]
  node [
    id 127
    label "p&#322;atnik"
  ]
  node [
    id 128
    label "zwierzchnik"
  ]
  node [
    id 129
    label "ponadzak&#322;adowy_uk&#322;ad_zbiorowy_pracy"
  ]
  node [
    id 130
    label "open"
  ]
  node [
    id 131
    label "odejmowa&#263;"
  ]
  node [
    id 132
    label "mie&#263;_miejsce"
  ]
  node [
    id 133
    label "robi&#263;_pierwszy_krok"
  ]
  node [
    id 134
    label "set_about"
  ]
  node [
    id 135
    label "begin"
  ]
  node [
    id 136
    label "post&#281;powa&#263;"
  ]
  node [
    id 137
    label "bankrupt"
  ]
  node [
    id 138
    label "dzie&#324;_powszedni"
  ]
  node [
    id 139
    label "Poniedzia&#322;ek_Wielkonocny"
  ]
  node [
    id 140
    label "tydzie&#324;"
  ]
  node [
    id 141
    label "&#322;agodny"
  ]
  node [
    id 142
    label "mi&#281;kko"
  ]
  node [
    id 143
    label "mi&#281;ciuchno"
  ]
  node [
    id 144
    label "przyjemnie"
  ]
  node [
    id 145
    label "subtelnie"
  ]
  node [
    id 146
    label "ostro&#380;nie"
  ]
  node [
    id 147
    label "&#322;agodnie"
  ]
  node [
    id 148
    label "grzecznie"
  ]
  node [
    id 149
    label "delikatny"
  ]
  node [
    id 150
    label "remark"
  ]
  node [
    id 151
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 152
    label "u&#380;ywa&#263;"
  ]
  node [
    id 153
    label "okre&#347;la&#263;"
  ]
  node [
    id 154
    label "j&#281;zyk"
  ]
  node [
    id 155
    label "say"
  ]
  node [
    id 156
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 157
    label "formu&#322;owa&#263;"
  ]
  node [
    id 158
    label "talk"
  ]
  node [
    id 159
    label "powiada&#263;"
  ]
  node [
    id 160
    label "informowa&#263;"
  ]
  node [
    id 161
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 162
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 163
    label "wydobywa&#263;"
  ]
  node [
    id 164
    label "express"
  ]
  node [
    id 165
    label "chew_the_fat"
  ]
  node [
    id 166
    label "dysfonia"
  ]
  node [
    id 167
    label "umie&#263;"
  ]
  node [
    id 168
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 169
    label "tell"
  ]
  node [
    id 170
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 171
    label "wyra&#380;a&#263;"
  ]
  node [
    id 172
    label "gaworzy&#263;"
  ]
  node [
    id 173
    label "rozmawia&#263;"
  ]
  node [
    id 174
    label "dziama&#263;"
  ]
  node [
    id 175
    label "prawi&#263;"
  ]
  node [
    id 176
    label "share"
  ]
  node [
    id 177
    label "authorize"
  ]
  node [
    id 178
    label "przyznawa&#263;"
  ]
  node [
    id 179
    label "wsp&#243;&#322;uczestniczy&#263;"
  ]
  node [
    id 180
    label "poprzednio"
  ]
  node [
    id 181
    label "przesz&#322;y"
  ]
  node [
    id 182
    label "wcze&#347;niejszy"
  ]
  node [
    id 183
    label "kolejny"
  ]
  node [
    id 184
    label "istota_&#380;ywa"
  ]
  node [
    id 185
    label "najgorszy"
  ]
  node [
    id 186
    label "aktualny"
  ]
  node [
    id 187
    label "ostatnio"
  ]
  node [
    id 188
    label "niedawno"
  ]
  node [
    id 189
    label "wi&#261;&#380;&#261;cy"
  ]
  node [
    id 190
    label "sko&#324;czony"
  ]
  node [
    id 191
    label "pozosta&#322;y"
  ]
  node [
    id 192
    label "w&#261;tpliwy"
  ]
  node [
    id 193
    label "matka"
  ]
  node [
    id 194
    label "kobieta"
  ]
  node [
    id 195
    label "partnerka"
  ]
  node [
    id 196
    label "&#380;ona"
  ]
  node [
    id 197
    label "starzy"
  ]
  node [
    id 198
    label "urz&#261;dzenie_kuchenne"
  ]
  node [
    id 199
    label "maszyna"
  ]
  node [
    id 200
    label "sprz&#281;t_AGD"
  ]
  node [
    id 201
    label "automat"
  ]
  node [
    id 202
    label "cope"
  ]
  node [
    id 203
    label "doj&#347;&#263;"
  ]
  node [
    id 204
    label "zapi&#261;&#263;"
  ]
  node [
    id 205
    label "sko&#324;czy&#263;"
  ]
  node [
    id 206
    label "rzecz"
  ]
  node [
    id 207
    label "interes"
  ]
  node [
    id 208
    label "pozdrowi&#263;"
  ]
  node [
    id 209
    label "rozsta&#263;_si&#281;"
  ]
  node [
    id 210
    label "asymilowa&#263;"
  ]
  node [
    id 211
    label "wapniak"
  ]
  node [
    id 212
    label "dwun&#243;g"
  ]
  node [
    id 213
    label "polifag"
  ]
  node [
    id 214
    label "wz&#243;r"
  ]
  node [
    id 215
    label "profanum"
  ]
  node [
    id 216
    label "hominid"
  ]
  node [
    id 217
    label "homo_sapiens"
  ]
  node [
    id 218
    label "nasada"
  ]
  node [
    id 219
    label "podw&#322;adny"
  ]
  node [
    id 220
    label "ludzko&#347;&#263;"
  ]
  node [
    id 221
    label "os&#322;abianie"
  ]
  node [
    id 222
    label "mikrokosmos"
  ]
  node [
    id 223
    label "portrecista"
  ]
  node [
    id 224
    label "duch"
  ]
  node [
    id 225
    label "g&#322;owa"
  ]
  node [
    id 226
    label "oddzia&#322;ywanie"
  ]
  node [
    id 227
    label "asymilowanie"
  ]
  node [
    id 228
    label "osoba"
  ]
  node [
    id 229
    label "os&#322;abia&#263;"
  ]
  node [
    id 230
    label "figura"
  ]
  node [
    id 231
    label "Adam"
  ]
  node [
    id 232
    label "senior"
  ]
  node [
    id 233
    label "antropochoria"
  ]
  node [
    id 234
    label "posta&#263;"
  ]
  node [
    id 235
    label "pryncypa&#322;"
  ]
  node [
    id 236
    label "kierowa&#263;"
  ]
  node [
    id 237
    label "inform"
  ]
  node [
    id 238
    label "gwiazda"
  ]
  node [
    id 239
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 240
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 241
    label "wytw&#243;r"
  ]
  node [
    id 242
    label "dzwonek"
  ]
  node [
    id 243
    label "call"
  ]
  node [
    id 244
    label "telefon"
  ]
  node [
    id 245
    label "zabrzmie&#263;"
  ]
  node [
    id 246
    label "zadrynda&#263;"
  ]
  node [
    id 247
    label "zabrz&#281;cze&#263;"
  ]
  node [
    id 248
    label "sound"
  ]
  node [
    id 249
    label "skontaktowa&#263;_si&#281;"
  ]
  node [
    id 250
    label "zabi&#263;"
  ]
  node [
    id 251
    label "nacisn&#261;&#263;"
  ]
  node [
    id 252
    label "jingle"
  ]
  node [
    id 253
    label "sta&#263;_si&#281;"
  ]
  node [
    id 254
    label "zaistnie&#263;"
  ]
  node [
    id 255
    label "czas"
  ]
  node [
    id 256
    label "become"
  ]
  node [
    id 257
    label "line_up"
  ]
  node [
    id 258
    label "przyby&#263;"
  ]
  node [
    id 259
    label "record"
  ]
  node [
    id 260
    label "&#347;wiadectwo"
  ]
  node [
    id 261
    label "zapis"
  ]
  node [
    id 262
    label "raport&#243;wka"
  ]
  node [
    id 263
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 264
    label "artyku&#322;"
  ]
  node [
    id 265
    label "plik"
  ]
  node [
    id 266
    label "writing"
  ]
  node [
    id 267
    label "utw&#243;r"
  ]
  node [
    id 268
    label "dokumentacja"
  ]
  node [
    id 269
    label "registratura"
  ]
  node [
    id 270
    label "parafa"
  ]
  node [
    id 271
    label "sygnatariusz"
  ]
  node [
    id 272
    label "fascyku&#322;"
  ]
  node [
    id 273
    label "paleograf"
  ]
  node [
    id 274
    label "list"
  ]
  node [
    id 275
    label "egzemplarz"
  ]
  node [
    id 276
    label "komunikacja"
  ]
  node [
    id 277
    label "psychotest"
  ]
  node [
    id 278
    label "ortografia"
  ]
  node [
    id 279
    label "handwriting"
  ]
  node [
    id 280
    label "grafia"
  ]
  node [
    id 281
    label "prasa"
  ]
  node [
    id 282
    label "adres"
  ]
  node [
    id 283
    label "script"
  ]
  node [
    id 284
    label "dzia&#322;"
  ]
  node [
    id 285
    label "paleografia"
  ]
  node [
    id 286
    label "Zwrotnica"
  ]
  node [
    id 287
    label "wk&#322;ad"
  ]
  node [
    id 288
    label "cecha"
  ]
  node [
    id 289
    label "przekaz"
  ]
  node [
    id 290
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 291
    label "interpunkcja"
  ]
  node [
    id 292
    label "communication"
  ]
  node [
    id 293
    label "ok&#322;adka"
  ]
  node [
    id 294
    label "dzie&#322;o"
  ]
  node [
    id 295
    label "czasopismo"
  ]
  node [
    id 296
    label "letter"
  ]
  node [
    id 297
    label "zajawka"
  ]
  node [
    id 298
    label "ukochany"
  ]
  node [
    id 299
    label "optymalnie"
  ]
  node [
    id 300
    label "najlepszy"
  ]
  node [
    id 301
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 302
    label "fraza"
  ]
  node [
    id 303
    label "forma"
  ]
  node [
    id 304
    label "melodia"
  ]
  node [
    id 305
    label "zbacza&#263;"
  ]
  node [
    id 306
    label "entity"
  ]
  node [
    id 307
    label "omawia&#263;"
  ]
  node [
    id 308
    label "topik"
  ]
  node [
    id 309
    label "wyraz_pochodny"
  ]
  node [
    id 310
    label "om&#243;wi&#263;"
  ]
  node [
    id 311
    label "omawianie"
  ]
  node [
    id 312
    label "w&#261;tek"
  ]
  node [
    id 313
    label "forum"
  ]
  node [
    id 314
    label "zboczenie"
  ]
  node [
    id 315
    label "zbaczanie"
  ]
  node [
    id 316
    label "tre&#347;&#263;"
  ]
  node [
    id 317
    label "tematyka"
  ]
  node [
    id 318
    label "sprawa"
  ]
  node [
    id 319
    label "istota"
  ]
  node [
    id 320
    label "otoczka"
  ]
  node [
    id 321
    label "zboczy&#263;"
  ]
  node [
    id 322
    label "om&#243;wienie"
  ]
  node [
    id 323
    label "dostarczy&#263;"
  ]
  node [
    id 324
    label "obieca&#263;"
  ]
  node [
    id 325
    label "pozwoli&#263;"
  ]
  node [
    id 326
    label "przeznaczy&#263;"
  ]
  node [
    id 327
    label "doda&#263;"
  ]
  node [
    id 328
    label "give"
  ]
  node [
    id 329
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 330
    label "wyrzec_si&#281;"
  ]
  node [
    id 331
    label "supply"
  ]
  node [
    id 332
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 333
    label "zada&#263;"
  ]
  node [
    id 334
    label "odst&#261;pi&#263;"
  ]
  node [
    id 335
    label "feed"
  ]
  node [
    id 336
    label "testify"
  ]
  node [
    id 337
    label "powierzy&#263;"
  ]
  node [
    id 338
    label "convey"
  ]
  node [
    id 339
    label "przekaza&#263;"
  ]
  node [
    id 340
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 341
    label "zap&#322;aci&#263;"
  ]
  node [
    id 342
    label "dress"
  ]
  node [
    id 343
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 344
    label "udost&#281;pni&#263;"
  ]
  node [
    id 345
    label "sztachn&#261;&#263;"
  ]
  node [
    id 346
    label "przywali&#263;"
  ]
  node [
    id 347
    label "rap"
  ]
  node [
    id 348
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 349
    label "picture"
  ]
  node [
    id 350
    label "wyzwisko"
  ]
  node [
    id 351
    label "z&#322;y"
  ]
  node [
    id 352
    label "do_dupy"
  ]
  node [
    id 353
    label "beznadziejny"
  ]
  node [
    id 354
    label "chujowo"
  ]
  node [
    id 355
    label "fantastyczny"
  ]
  node [
    id 356
    label "niesamowity"
  ]
  node [
    id 357
    label "ba&#347;niowo"
  ]
  node [
    id 358
    label "tajemniczy"
  ]
  node [
    id 359
    label "bajecznie"
  ]
  node [
    id 360
    label "Facebook"
  ]
  node [
    id 361
    label "czu&#263;"
  ]
  node [
    id 362
    label "chowa&#263;"
  ]
  node [
    id 363
    label "corroborate"
  ]
  node [
    id 364
    label "mie&#263;_do_siebie"
  ]
  node [
    id 365
    label "aprobowa&#263;"
  ]
  node [
    id 366
    label "love"
  ]
  node [
    id 367
    label "kaftan"
  ]
  node [
    id 368
    label "opu&#347;ci&#263;"
  ]
  node [
    id 369
    label "ukaza&#263;_si&#281;_drukiem"
  ]
  node [
    id 370
    label "zej&#347;&#263;"
  ]
  node [
    id 371
    label "okaza&#263;_si&#281;"
  ]
  node [
    id 372
    label "powie&#347;&#263;_si&#281;"
  ]
  node [
    id 373
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 374
    label "ograniczenie"
  ]
  node [
    id 375
    label "ruszy&#263;"
  ]
  node [
    id 376
    label "wypa&#347;&#263;"
  ]
  node [
    id 377
    label "uko&#324;czy&#263;"
  ]
  node [
    id 378
    label "moderate"
  ]
  node [
    id 379
    label "wydosta&#263;_si&#281;"
  ]
  node [
    id 380
    label "wywie&#347;&#263;_si&#281;"
  ]
  node [
    id 381
    label "mount"
  ]
  node [
    id 382
    label "leave"
  ]
  node [
    id 383
    label "drive"
  ]
  node [
    id 384
    label "zagra&#263;"
  ]
  node [
    id 385
    label "zademonstrowa&#263;"
  ]
  node [
    id 386
    label "perform"
  ]
  node [
    id 387
    label "uwolni&#263;_si&#281;"
  ]
  node [
    id 388
    label "drop"
  ]
  node [
    id 389
    label "miernota"
  ]
  node [
    id 390
    label "g&#243;wno"
  ]
  node [
    id 391
    label "ilo&#347;&#263;"
  ]
  node [
    id 392
    label "ciura"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 28
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 9
    target 26
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 10
    target 105
  ]
  edge [
    source 10
    target 106
  ]
  edge [
    source 10
    target 107
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 10
    target 110
  ]
  edge [
    source 10
    target 111
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 10
    target 121
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 10
    target 125
  ]
  edge [
    source 10
    target 32
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 26
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 127
  ]
  edge [
    source 13
    target 128
  ]
  edge [
    source 13
    target 129
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 43
  ]
  edge [
    source 14
    target 44
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 130
  ]
  edge [
    source 15
    target 131
  ]
  edge [
    source 15
    target 132
  ]
  edge [
    source 15
    target 133
  ]
  edge [
    source 15
    target 134
  ]
  edge [
    source 15
    target 135
  ]
  edge [
    source 15
    target 136
  ]
  edge [
    source 15
    target 137
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 138
  ]
  edge [
    source 16
    target 139
  ]
  edge [
    source 16
    target 140
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 141
  ]
  edge [
    source 17
    target 142
  ]
  edge [
    source 17
    target 143
  ]
  edge [
    source 17
    target 144
  ]
  edge [
    source 17
    target 145
  ]
  edge [
    source 17
    target 146
  ]
  edge [
    source 17
    target 147
  ]
  edge [
    source 17
    target 148
  ]
  edge [
    source 17
    target 149
  ]
  edge [
    source 18
    target 49
  ]
  edge [
    source 18
    target 150
  ]
  edge [
    source 18
    target 151
  ]
  edge [
    source 18
    target 152
  ]
  edge [
    source 18
    target 153
  ]
  edge [
    source 18
    target 154
  ]
  edge [
    source 18
    target 155
  ]
  edge [
    source 18
    target 156
  ]
  edge [
    source 18
    target 157
  ]
  edge [
    source 18
    target 158
  ]
  edge [
    source 18
    target 159
  ]
  edge [
    source 18
    target 160
  ]
  edge [
    source 18
    target 161
  ]
  edge [
    source 18
    target 162
  ]
  edge [
    source 18
    target 163
  ]
  edge [
    source 18
    target 164
  ]
  edge [
    source 18
    target 165
  ]
  edge [
    source 18
    target 166
  ]
  edge [
    source 18
    target 167
  ]
  edge [
    source 18
    target 168
  ]
  edge [
    source 18
    target 169
  ]
  edge [
    source 18
    target 170
  ]
  edge [
    source 18
    target 171
  ]
  edge [
    source 18
    target 172
  ]
  edge [
    source 18
    target 173
  ]
  edge [
    source 18
    target 174
  ]
  edge [
    source 18
    target 175
  ]
  edge [
    source 18
    target 44
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 176
  ]
  edge [
    source 19
    target 177
  ]
  edge [
    source 19
    target 178
  ]
  edge [
    source 19
    target 179
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 180
  ]
  edge [
    source 20
    target 181
  ]
  edge [
    source 20
    target 182
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 28
  ]
  edge [
    source 21
    target 183
  ]
  edge [
    source 21
    target 184
  ]
  edge [
    source 21
    target 185
  ]
  edge [
    source 21
    target 186
  ]
  edge [
    source 21
    target 187
  ]
  edge [
    source 21
    target 188
  ]
  edge [
    source 21
    target 189
  ]
  edge [
    source 21
    target 190
  ]
  edge [
    source 21
    target 191
  ]
  edge [
    source 21
    target 192
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 193
  ]
  edge [
    source 22
    target 194
  ]
  edge [
    source 22
    target 195
  ]
  edge [
    source 22
    target 196
  ]
  edge [
    source 22
    target 197
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 198
  ]
  edge [
    source 23
    target 199
  ]
  edge [
    source 23
    target 200
  ]
  edge [
    source 23
    target 201
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 202
  ]
  edge [
    source 25
    target 55
  ]
  edge [
    source 25
    target 203
  ]
  edge [
    source 25
    target 204
  ]
  edge [
    source 25
    target 205
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 206
  ]
  edge [
    source 26
    target 207
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 208
  ]
  edge [
    source 27
    target 209
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 210
  ]
  edge [
    source 28
    target 211
  ]
  edge [
    source 28
    target 212
  ]
  edge [
    source 28
    target 213
  ]
  edge [
    source 28
    target 214
  ]
  edge [
    source 28
    target 215
  ]
  edge [
    source 28
    target 216
  ]
  edge [
    source 28
    target 217
  ]
  edge [
    source 28
    target 218
  ]
  edge [
    source 28
    target 219
  ]
  edge [
    source 28
    target 220
  ]
  edge [
    source 28
    target 221
  ]
  edge [
    source 28
    target 222
  ]
  edge [
    source 28
    target 223
  ]
  edge [
    source 28
    target 224
  ]
  edge [
    source 28
    target 225
  ]
  edge [
    source 28
    target 226
  ]
  edge [
    source 28
    target 227
  ]
  edge [
    source 28
    target 228
  ]
  edge [
    source 28
    target 229
  ]
  edge [
    source 28
    target 230
  ]
  edge [
    source 28
    target 231
  ]
  edge [
    source 28
    target 232
  ]
  edge [
    source 28
    target 233
  ]
  edge [
    source 28
    target 234
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 109
  ]
  edge [
    source 29
    target 235
  ]
  edge [
    source 29
    target 236
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 237
  ]
  edge [
    source 30
    target 160
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 238
  ]
  edge [
    source 31
    target 239
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 104
  ]
  edge [
    source 32
    target 105
  ]
  edge [
    source 32
    target 106
  ]
  edge [
    source 32
    target 107
  ]
  edge [
    source 32
    target 108
  ]
  edge [
    source 32
    target 109
  ]
  edge [
    source 32
    target 110
  ]
  edge [
    source 32
    target 240
  ]
  edge [
    source 32
    target 241
  ]
  edge [
    source 32
    target 111
  ]
  edge [
    source 32
    target 112
  ]
  edge [
    source 32
    target 113
  ]
  edge [
    source 32
    target 115
  ]
  edge [
    source 32
    target 114
  ]
  edge [
    source 32
    target 116
  ]
  edge [
    source 32
    target 117
  ]
  edge [
    source 32
    target 118
  ]
  edge [
    source 32
    target 119
  ]
  edge [
    source 32
    target 120
  ]
  edge [
    source 32
    target 121
  ]
  edge [
    source 32
    target 122
  ]
  edge [
    source 32
    target 123
  ]
  edge [
    source 32
    target 124
  ]
  edge [
    source 32
    target 125
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 126
  ]
  edge [
    source 33
    target 101
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 242
  ]
  edge [
    source 34
    target 243
  ]
  edge [
    source 34
    target 244
  ]
  edge [
    source 34
    target 245
  ]
  edge [
    source 34
    target 246
  ]
  edge [
    source 34
    target 247
  ]
  edge [
    source 34
    target 248
  ]
  edge [
    source 34
    target 249
  ]
  edge [
    source 34
    target 250
  ]
  edge [
    source 34
    target 251
  ]
  edge [
    source 34
    target 252
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 253
  ]
  edge [
    source 35
    target 254
  ]
  edge [
    source 35
    target 255
  ]
  edge [
    source 35
    target 203
  ]
  edge [
    source 35
    target 256
  ]
  edge [
    source 35
    target 257
  ]
  edge [
    source 35
    target 258
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 259
  ]
  edge [
    source 36
    target 241
  ]
  edge [
    source 36
    target 260
  ]
  edge [
    source 36
    target 261
  ]
  edge [
    source 36
    target 262
  ]
  edge [
    source 36
    target 263
  ]
  edge [
    source 36
    target 264
  ]
  edge [
    source 36
    target 265
  ]
  edge [
    source 36
    target 266
  ]
  edge [
    source 36
    target 267
  ]
  edge [
    source 36
    target 268
  ]
  edge [
    source 36
    target 269
  ]
  edge [
    source 36
    target 270
  ]
  edge [
    source 36
    target 271
  ]
  edge [
    source 36
    target 272
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 273
  ]
  edge [
    source 37
    target 274
  ]
  edge [
    source 37
    target 275
  ]
  edge [
    source 37
    target 276
  ]
  edge [
    source 37
    target 277
  ]
  edge [
    source 37
    target 278
  ]
  edge [
    source 37
    target 279
  ]
  edge [
    source 37
    target 280
  ]
  edge [
    source 37
    target 281
  ]
  edge [
    source 37
    target 154
  ]
  edge [
    source 37
    target 282
  ]
  edge [
    source 37
    target 283
  ]
  edge [
    source 37
    target 284
  ]
  edge [
    source 37
    target 285
  ]
  edge [
    source 37
    target 286
  ]
  edge [
    source 37
    target 287
  ]
  edge [
    source 37
    target 288
  ]
  edge [
    source 37
    target 289
  ]
  edge [
    source 37
    target 290
  ]
  edge [
    source 37
    target 291
  ]
  edge [
    source 37
    target 292
  ]
  edge [
    source 37
    target 293
  ]
  edge [
    source 37
    target 294
  ]
  edge [
    source 37
    target 295
  ]
  edge [
    source 37
    target 296
  ]
  edge [
    source 37
    target 297
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 298
  ]
  edge [
    source 39
    target 299
  ]
  edge [
    source 39
    target 97
  ]
  edge [
    source 39
    target 300
  ]
  edge [
    source 39
    target 301
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 302
  ]
  edge [
    source 40
    target 303
  ]
  edge [
    source 40
    target 304
  ]
  edge [
    source 40
    target 206
  ]
  edge [
    source 40
    target 305
  ]
  edge [
    source 40
    target 306
  ]
  edge [
    source 40
    target 307
  ]
  edge [
    source 40
    target 308
  ]
  edge [
    source 40
    target 309
  ]
  edge [
    source 40
    target 310
  ]
  edge [
    source 40
    target 311
  ]
  edge [
    source 40
    target 312
  ]
  edge [
    source 40
    target 313
  ]
  edge [
    source 40
    target 288
  ]
  edge [
    source 40
    target 314
  ]
  edge [
    source 40
    target 315
  ]
  edge [
    source 40
    target 316
  ]
  edge [
    source 40
    target 317
  ]
  edge [
    source 40
    target 318
  ]
  edge [
    source 40
    target 319
  ]
  edge [
    source 40
    target 320
  ]
  edge [
    source 40
    target 321
  ]
  edge [
    source 40
    target 322
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 323
  ]
  edge [
    source 41
    target 324
  ]
  edge [
    source 41
    target 325
  ]
  edge [
    source 41
    target 326
  ]
  edge [
    source 41
    target 327
  ]
  edge [
    source 41
    target 328
  ]
  edge [
    source 41
    target 329
  ]
  edge [
    source 41
    target 330
  ]
  edge [
    source 41
    target 331
  ]
  edge [
    source 41
    target 332
  ]
  edge [
    source 41
    target 333
  ]
  edge [
    source 41
    target 334
  ]
  edge [
    source 41
    target 335
  ]
  edge [
    source 41
    target 336
  ]
  edge [
    source 41
    target 337
  ]
  edge [
    source 41
    target 338
  ]
  edge [
    source 41
    target 339
  ]
  edge [
    source 41
    target 340
  ]
  edge [
    source 41
    target 341
  ]
  edge [
    source 41
    target 342
  ]
  edge [
    source 41
    target 343
  ]
  edge [
    source 41
    target 344
  ]
  edge [
    source 41
    target 345
  ]
  edge [
    source 41
    target 64
  ]
  edge [
    source 41
    target 65
  ]
  edge [
    source 41
    target 346
  ]
  edge [
    source 41
    target 347
  ]
  edge [
    source 41
    target 348
  ]
  edge [
    source 41
    target 349
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 350
  ]
  edge [
    source 42
    target 351
  ]
  edge [
    source 42
    target 352
  ]
  edge [
    source 42
    target 353
  ]
  edge [
    source 42
    target 354
  ]
  edge [
    source 43
    target 355
  ]
  edge [
    source 43
    target 356
  ]
  edge [
    source 43
    target 357
  ]
  edge [
    source 43
    target 358
  ]
  edge [
    source 43
    target 359
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 360
  ]
  edge [
    source 44
    target 361
  ]
  edge [
    source 44
    target 362
  ]
  edge [
    source 44
    target 363
  ]
  edge [
    source 44
    target 364
  ]
  edge [
    source 44
    target 365
  ]
  edge [
    source 44
    target 366
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 367
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 50
  ]
  edge [
    source 48
    target 368
  ]
  edge [
    source 48
    target 369
  ]
  edge [
    source 48
    target 370
  ]
  edge [
    source 48
    target 371
  ]
  edge [
    source 48
    target 372
  ]
  edge [
    source 48
    target 373
  ]
  edge [
    source 48
    target 205
  ]
  edge [
    source 48
    target 374
  ]
  edge [
    source 48
    target 375
  ]
  edge [
    source 48
    target 376
  ]
  edge [
    source 48
    target 377
  ]
  edge [
    source 48
    target 130
  ]
  edge [
    source 48
    target 378
  ]
  edge [
    source 48
    target 56
  ]
  edge [
    source 48
    target 379
  ]
  edge [
    source 48
    target 380
  ]
  edge [
    source 48
    target 381
  ]
  edge [
    source 48
    target 382
  ]
  edge [
    source 48
    target 383
  ]
  edge [
    source 48
    target 384
  ]
  edge [
    source 48
    target 385
  ]
  edge [
    source 48
    target 69
  ]
  edge [
    source 48
    target 386
  ]
  edge [
    source 48
    target 387
  ]
  edge [
    source 48
    target 388
  ]
  edge [
    source 49
    target 389
  ]
  edge [
    source 49
    target 390
  ]
  edge [
    source 49
    target 366
  ]
  edge [
    source 49
    target 391
  ]
  edge [
    source 49
    target 392
  ]
]
