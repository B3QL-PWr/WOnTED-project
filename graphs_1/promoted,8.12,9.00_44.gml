graph [
  maxDegree 38
  minDegree 1
  meanDegree 2.0444444444444443
  density 0.022971285892634207
  graphCliqueNumber 3
  node [
    id 0
    label "starcie"
    origin "text"
  ]
  node [
    id 1
    label "gks"
    origin "text"
  ]
  node [
    id 2
    label "katowice"
    origin "text"
  ]
  node [
    id 3
    label "ruch"
    origin "text"
  ]
  node [
    id 4
    label "chorz&#243;w"
    origin "text"
  ]
  node [
    id 5
    label "obok"
    origin "text"
  ]
  node [
    id 6
    label "szpital"
    origin "text"
  ]
  node [
    id 7
    label "dzieci&#281;cy"
    origin "text"
  ]
  node [
    id 8
    label "ligocie"
    origin "text"
  ]
  node [
    id 9
    label "zranienie"
  ]
  node [
    id 10
    label "konfrontacyjny"
  ]
  node [
    id 11
    label "usuni&#281;cie"
  ]
  node [
    id 12
    label "military_action"
  ]
  node [
    id 13
    label "trafienie"
  ]
  node [
    id 14
    label "oczyszczenie"
  ]
  node [
    id 15
    label "sambo"
  ]
  node [
    id 16
    label "euroliga"
  ]
  node [
    id 17
    label "zaatakowanie"
  ]
  node [
    id 18
    label "interliga"
  ]
  node [
    id 19
    label "runda"
  ]
  node [
    id 20
    label "contest"
  ]
  node [
    id 21
    label "discord"
  ]
  node [
    id 22
    label "zagrywka"
  ]
  node [
    id 23
    label "obrona"
  ]
  node [
    id 24
    label "expunction"
  ]
  node [
    id 25
    label "rub"
  ]
  node [
    id 26
    label "wydarzenie"
  ]
  node [
    id 27
    label "rewan&#380;owy"
  ]
  node [
    id 28
    label "otarcie"
  ]
  node [
    id 29
    label "faza"
  ]
  node [
    id 30
    label "rozdrobnienie"
  ]
  node [
    id 31
    label "k&#322;&#243;tnia"
  ]
  node [
    id 32
    label "scramble"
  ]
  node [
    id 33
    label "manewr"
  ]
  node [
    id 34
    label "model"
  ]
  node [
    id 35
    label "movement"
  ]
  node [
    id 36
    label "apraksja"
  ]
  node [
    id 37
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 38
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 39
    label "poruszenie"
  ]
  node [
    id 40
    label "commercial_enterprise"
  ]
  node [
    id 41
    label "dyssypacja_energii"
  ]
  node [
    id 42
    label "zmiana"
  ]
  node [
    id 43
    label "utrzymanie"
  ]
  node [
    id 44
    label "utrzyma&#263;"
  ]
  node [
    id 45
    label "komunikacja"
  ]
  node [
    id 46
    label "tumult"
  ]
  node [
    id 47
    label "kr&#243;tki"
  ]
  node [
    id 48
    label "drift"
  ]
  node [
    id 49
    label "utrzymywa&#263;"
  ]
  node [
    id 50
    label "stopek"
  ]
  node [
    id 51
    label "kanciasty"
  ]
  node [
    id 52
    label "d&#322;ugi"
  ]
  node [
    id 53
    label "zjawisko"
  ]
  node [
    id 54
    label "utrzymywanie"
  ]
  node [
    id 55
    label "czynno&#347;&#263;"
  ]
  node [
    id 56
    label "myk"
  ]
  node [
    id 57
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 58
    label "taktyka"
  ]
  node [
    id 59
    label "move"
  ]
  node [
    id 60
    label "natural_process"
  ]
  node [
    id 61
    label "lokomocja"
  ]
  node [
    id 62
    label "mechanika"
  ]
  node [
    id 63
    label "proces"
  ]
  node [
    id 64
    label "strumie&#324;"
  ]
  node [
    id 65
    label "aktywno&#347;&#263;"
  ]
  node [
    id 66
    label "travel"
  ]
  node [
    id 67
    label "tu&#380;_tu&#380;"
  ]
  node [
    id 68
    label "bliski"
  ]
  node [
    id 69
    label "szpitalnictwo"
  ]
  node [
    id 70
    label "oddzia&#322;_septyczny"
  ]
  node [
    id 71
    label "klinicysta"
  ]
  node [
    id 72
    label "zabieg&#243;wka"
  ]
  node [
    id 73
    label "izba_chorych"
  ]
  node [
    id 74
    label "&#322;&#243;&#380;eczko_nadziei"
  ]
  node [
    id 75
    label "blok_operacyjny"
  ]
  node [
    id 76
    label "instytucja"
  ]
  node [
    id 77
    label "centrum_urazowe"
  ]
  node [
    id 78
    label "plac&#243;wka_s&#322;u&#380;by_zdrowia"
  ]
  node [
    id 79
    label "kostnica"
  ]
  node [
    id 80
    label "oddzia&#322;"
  ]
  node [
    id 81
    label "sala_chorych"
  ]
  node [
    id 82
    label "pocz&#261;tkowy"
  ]
  node [
    id 83
    label "dzieci&#281;co"
  ]
  node [
    id 84
    label "typowy"
  ]
  node [
    id 85
    label "dzieci&#324;ski"
  ]
  node [
    id 86
    label "GKS"
  ]
  node [
    id 87
    label "Katowice"
  ]
  node [
    id 88
    label "Chorz&#243;w"
  ]
  node [
    id 89
    label "kibol"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 86
    target 87
  ]
  edge [
    source 86
    target 89
  ]
  edge [
    source 87
    target 89
  ]
]
