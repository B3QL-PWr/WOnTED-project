graph [
  maxDegree 27
  minDegree 1
  meanDegree 1.8571428571428572
  density 0.04529616724738676
  graphCliqueNumber 2
  node [
    id 0
    label "ten"
    origin "text"
  ]
  node [
    id 1
    label "por"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "okre&#347;lony"
  ]
  node [
    id 5
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 6
    label "otw&#243;r"
  ]
  node [
    id 7
    label "w&#322;oszczyzna"
  ]
  node [
    id 8
    label "warzywo"
  ]
  node [
    id 9
    label "czosnek"
  ]
  node [
    id 10
    label "kapelusz"
  ]
  node [
    id 11
    label "uj&#347;cie"
  ]
  node [
    id 12
    label "remark"
  ]
  node [
    id 13
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 14
    label "u&#380;ywa&#263;"
  ]
  node [
    id 15
    label "okre&#347;la&#263;"
  ]
  node [
    id 16
    label "j&#281;zyk"
  ]
  node [
    id 17
    label "say"
  ]
  node [
    id 18
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 19
    label "formu&#322;owa&#263;"
  ]
  node [
    id 20
    label "talk"
  ]
  node [
    id 21
    label "powiada&#263;"
  ]
  node [
    id 22
    label "informowa&#263;"
  ]
  node [
    id 23
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 24
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 25
    label "wydobywa&#263;"
  ]
  node [
    id 26
    label "express"
  ]
  node [
    id 27
    label "chew_the_fat"
  ]
  node [
    id 28
    label "dysfonia"
  ]
  node [
    id 29
    label "umie&#263;"
  ]
  node [
    id 30
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 31
    label "tell"
  ]
  node [
    id 32
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 33
    label "wyra&#380;a&#263;"
  ]
  node [
    id 34
    label "gaworzy&#263;"
  ]
  node [
    id 35
    label "rozmawia&#263;"
  ]
  node [
    id 36
    label "dziama&#263;"
  ]
  node [
    id 37
    label "prawi&#263;"
  ]
  node [
    id 38
    label "Magda"
  ]
  node [
    id 39
    label "Gessler"
  ]
  node [
    id 40
    label "tw&#243;j"
  ]
  node [
    id 41
    label "imperium"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 40
    target 41
  ]
]
