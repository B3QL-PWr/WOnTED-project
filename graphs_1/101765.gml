graph [
  maxDegree 34
  minDegree 1
  meanDegree 2.0484848484848484
  density 0.012490761271249077
  graphCliqueNumber 3
  node [
    id 0
    label "brn&#261;&#263;"
    origin "text"
  ]
  node [
    id 1
    label "daleko"
    origin "text"
  ]
  node [
    id 2
    label "brodaty"
    origin "text"
  ]
  node [
    id 3
    label "metafora"
    origin "text"
  ]
  node [
    id 4
    label "droga"
    origin "text"
  ]
  node [
    id 5
    label "nowa"
    origin "text"
  ]
  node [
    id 6
    label "propozycja"
    origin "text"
  ]
  node [
    id 7
    label "uke"
    origin "text"
  ]
  node [
    id 8
    label "trudno"
    origin "text"
  ]
  node [
    id 9
    label "nawet"
    origin "text"
  ]
  node [
    id 10
    label "por&#243;wna&#263;"
    origin "text"
  ]
  node [
    id 11
    label "typowy"
    origin "text"
  ]
  node [
    id 12
    label "polska"
    origin "text"
  ]
  node [
    id 13
    label "krajowy"
    origin "text"
  ]
  node [
    id 14
    label "pe&#322;ny"
    origin "text"
  ]
  node [
    id 15
    label "koleina"
    origin "text"
  ]
  node [
    id 16
    label "&#378;le"
    origin "text"
  ]
  node [
    id 17
    label "wyprofilowa&#263;"
    origin "text"
  ]
  node [
    id 18
    label "zakr&#281;t"
    origin "text"
  ]
  node [
    id 19
    label "oznakowa&#263;"
    origin "text"
  ]
  node [
    id 20
    label "skrzy&#380;owanie"
    origin "text"
  ]
  node [
    id 21
    label "raczej"
    origin "text"
  ]
  node [
    id 22
    label "polny"
    origin "text"
  ]
  node [
    id 23
    label "niekt&#243;ry"
    origin "text"
  ]
  node [
    id 24
    label "odcinek"
    origin "text"
  ]
  node [
    id 25
    label "wyasfaltowa&#263;"
    origin "text"
  ]
  node [
    id 26
    label "ale"
    origin "text"
  ]
  node [
    id 27
    label "te&#380;"
    origin "text"
  ]
  node [
    id 28
    label "niechlujnie"
    origin "text"
  ]
  node [
    id 29
    label "pogr&#261;&#380;a&#263;"
  ]
  node [
    id 30
    label "tramp"
  ]
  node [
    id 31
    label "zapuszcza&#263;_si&#281;"
  ]
  node [
    id 32
    label "kopa&#263;_si&#281;"
  ]
  node [
    id 33
    label "i&#347;&#263;"
  ]
  node [
    id 34
    label "przedostawa&#263;_si&#281;"
  ]
  node [
    id 35
    label "dawno"
  ]
  node [
    id 36
    label "nisko"
  ]
  node [
    id 37
    label "nieobecnie"
  ]
  node [
    id 38
    label "daleki"
  ]
  node [
    id 39
    label "het"
  ]
  node [
    id 40
    label "wysoko"
  ]
  node [
    id 41
    label "du&#380;o"
  ]
  node [
    id 42
    label "znacznie"
  ]
  node [
    id 43
    label "g&#322;&#281;boko"
  ]
  node [
    id 44
    label "figura_stylistyczna"
  ]
  node [
    id 45
    label "sformu&#322;owanie"
  ]
  node [
    id 46
    label "metaforyka"
  ]
  node [
    id 47
    label "metaphor"
  ]
  node [
    id 48
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 49
    label "journey"
  ]
  node [
    id 50
    label "podbieg"
  ]
  node [
    id 51
    label "bezsilnikowy"
  ]
  node [
    id 52
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 53
    label "wylot"
  ]
  node [
    id 54
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 55
    label "drogowskaz"
  ]
  node [
    id 56
    label "nawierzchnia"
  ]
  node [
    id 57
    label "turystyka"
  ]
  node [
    id 58
    label "budowla"
  ]
  node [
    id 59
    label "spos&#243;b"
  ]
  node [
    id 60
    label "passage"
  ]
  node [
    id 61
    label "marszrutyzacja"
  ]
  node [
    id 62
    label "zbior&#243;wka"
  ]
  node [
    id 63
    label "rajza"
  ]
  node [
    id 64
    label "ekskursja"
  ]
  node [
    id 65
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 66
    label "ruch"
  ]
  node [
    id 67
    label "trasa"
  ]
  node [
    id 68
    label "wyb&#243;j"
  ]
  node [
    id 69
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 70
    label "ekwipunek"
  ]
  node [
    id 71
    label "korona_drogi"
  ]
  node [
    id 72
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 73
    label "pobocze"
  ]
  node [
    id 74
    label "gwiazda"
  ]
  node [
    id 75
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 76
    label "pomys&#322;"
  ]
  node [
    id 77
    label "proposal"
  ]
  node [
    id 78
    label "trudny"
  ]
  node [
    id 79
    label "hard"
  ]
  node [
    id 80
    label "zanalizowa&#263;"
  ]
  node [
    id 81
    label "compose"
  ]
  node [
    id 82
    label "typowo"
  ]
  node [
    id 83
    label "zwyczajny"
  ]
  node [
    id 84
    label "zwyk&#322;y"
  ]
  node [
    id 85
    label "cz&#281;sty"
  ]
  node [
    id 86
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 87
    label "rodzimy"
  ]
  node [
    id 88
    label "szcz&#281;&#347;liwy"
  ]
  node [
    id 89
    label "nieograniczony"
  ]
  node [
    id 90
    label "nape&#322;nienie_si&#281;"
  ]
  node [
    id 91
    label "kompletny"
  ]
  node [
    id 92
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 93
    label "r&#243;wny"
  ]
  node [
    id 94
    label "wype&#322;nianie_si&#281;"
  ]
  node [
    id 95
    label "bezwzgl&#281;dny"
  ]
  node [
    id 96
    label "zupe&#322;ny"
  ]
  node [
    id 97
    label "ca&#322;y"
  ]
  node [
    id 98
    label "satysfakcja"
  ]
  node [
    id 99
    label "wszechogarniaj&#261;cy"
  ]
  node [
    id 100
    label "pe&#322;no"
  ]
  node [
    id 101
    label "wype&#322;nienie"
  ]
  node [
    id 102
    label "otwarty"
  ]
  node [
    id 103
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 104
    label "niezgodnie"
  ]
  node [
    id 105
    label "niepomy&#347;lnie"
  ]
  node [
    id 106
    label "negatywnie"
  ]
  node [
    id 107
    label "piesko"
  ]
  node [
    id 108
    label "z&#322;y"
  ]
  node [
    id 109
    label "niew&#322;a&#347;ciwie"
  ]
  node [
    id 110
    label "gorzej"
  ]
  node [
    id 111
    label "niekorzystnie"
  ]
  node [
    id 112
    label "profile"
  ]
  node [
    id 113
    label "ukszta&#322;towa&#263;"
  ]
  node [
    id 114
    label "ozdobi&#263;"
  ]
  node [
    id 115
    label "nada&#263;"
  ]
  node [
    id 116
    label "miejsce"
  ]
  node [
    id 117
    label "ekscentryk"
  ]
  node [
    id 118
    label "podsterowno&#347;&#263;"
  ]
  node [
    id 119
    label "czas"
  ]
  node [
    id 120
    label "zapaleniec"
  ]
  node [
    id 121
    label "zwrot"
  ]
  node [
    id 122
    label "nadsterowno&#347;&#263;"
  ]
  node [
    id 123
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 124
    label "szalona_g&#322;owa"
  ]
  node [
    id 125
    label "serpentyna"
  ]
  node [
    id 126
    label "zajob"
  ]
  node [
    id 127
    label "oznaczy&#263;"
  ]
  node [
    id 128
    label "stamp"
  ]
  node [
    id 129
    label "uporz&#261;dkowanie"
  ]
  node [
    id 130
    label "intersection"
  ]
  node [
    id 131
    label "rzecz"
  ]
  node [
    id 132
    label "hybrydalno&#347;&#263;"
  ]
  node [
    id 133
    label "skrzy&#380;owanie_si&#281;"
  ]
  node [
    id 134
    label "&#347;wiat&#322;a"
  ]
  node [
    id 135
    label "przeci&#281;cie"
  ]
  node [
    id 136
    label "powi&#261;zanie"
  ]
  node [
    id 137
    label "rozmno&#380;enie"
  ]
  node [
    id 138
    label "dziki"
  ]
  node [
    id 139
    label "jaki&#347;"
  ]
  node [
    id 140
    label "pole"
  ]
  node [
    id 141
    label "part"
  ]
  node [
    id 142
    label "pokwitowanie"
  ]
  node [
    id 143
    label "kawa&#322;ek"
  ]
  node [
    id 144
    label "coupon"
  ]
  node [
    id 145
    label "teren"
  ]
  node [
    id 146
    label "line"
  ]
  node [
    id 147
    label "epizod"
  ]
  node [
    id 148
    label "moneta"
  ]
  node [
    id 149
    label "fragment"
  ]
  node [
    id 150
    label "wyla&#263;"
  ]
  node [
    id 151
    label "piwo"
  ]
  node [
    id 152
    label "byle_jak"
  ]
  node [
    id 153
    label "nieporz&#261;dnie"
  ]
  node [
    id 154
    label "niechlujny"
  ]
  node [
    id 155
    label "niedok&#322;adnie"
  ]
  node [
    id 156
    label "nasz"
  ]
  node [
    id 157
    label "klasy"
  ]
  node [
    id 158
    label "unia"
  ]
  node [
    id 159
    label "europejski"
  ]
  node [
    id 160
    label "EU"
  ]
  node [
    id 161
    label "15"
  ]
  node [
    id 162
    label "Broadband"
  ]
  node [
    id 163
    label "performance"
  ]
  node [
    id 164
    label "Index"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 82
  ]
  edge [
    source 11
    target 83
  ]
  edge [
    source 11
    target 84
  ]
  edge [
    source 11
    target 85
  ]
  edge [
    source 11
    target 86
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 87
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 88
  ]
  edge [
    source 14
    target 89
  ]
  edge [
    source 14
    target 90
  ]
  edge [
    source 14
    target 91
  ]
  edge [
    source 14
    target 92
  ]
  edge [
    source 14
    target 93
  ]
  edge [
    source 14
    target 94
  ]
  edge [
    source 14
    target 95
  ]
  edge [
    source 14
    target 96
  ]
  edge [
    source 14
    target 97
  ]
  edge [
    source 14
    target 98
  ]
  edge [
    source 14
    target 99
  ]
  edge [
    source 14
    target 100
  ]
  edge [
    source 14
    target 101
  ]
  edge [
    source 14
    target 102
  ]
  edge [
    source 14
    target 24
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 103
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 104
  ]
  edge [
    source 16
    target 105
  ]
  edge [
    source 16
    target 106
  ]
  edge [
    source 16
    target 107
  ]
  edge [
    source 16
    target 108
  ]
  edge [
    source 16
    target 109
  ]
  edge [
    source 16
    target 110
  ]
  edge [
    source 16
    target 111
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 112
  ]
  edge [
    source 17
    target 113
  ]
  edge [
    source 17
    target 114
  ]
  edge [
    source 17
    target 115
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 116
  ]
  edge [
    source 18
    target 117
  ]
  edge [
    source 18
    target 118
  ]
  edge [
    source 18
    target 119
  ]
  edge [
    source 18
    target 120
  ]
  edge [
    source 18
    target 121
  ]
  edge [
    source 18
    target 24
  ]
  edge [
    source 18
    target 122
  ]
  edge [
    source 18
    target 123
  ]
  edge [
    source 18
    target 124
  ]
  edge [
    source 18
    target 125
  ]
  edge [
    source 18
    target 67
  ]
  edge [
    source 18
    target 126
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 127
  ]
  edge [
    source 19
    target 128
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 129
  ]
  edge [
    source 20
    target 130
  ]
  edge [
    source 20
    target 131
  ]
  edge [
    source 20
    target 132
  ]
  edge [
    source 20
    target 133
  ]
  edge [
    source 20
    target 134
  ]
  edge [
    source 20
    target 135
  ]
  edge [
    source 20
    target 136
  ]
  edge [
    source 20
    target 137
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 138
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 139
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 140
  ]
  edge [
    source 24
    target 141
  ]
  edge [
    source 24
    target 142
  ]
  edge [
    source 24
    target 143
  ]
  edge [
    source 24
    target 144
  ]
  edge [
    source 24
    target 145
  ]
  edge [
    source 24
    target 146
  ]
  edge [
    source 24
    target 123
  ]
  edge [
    source 24
    target 147
  ]
  edge [
    source 24
    target 148
  ]
  edge [
    source 24
    target 149
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 150
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 151
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 28
    target 152
  ]
  edge [
    source 28
    target 153
  ]
  edge [
    source 28
    target 154
  ]
  edge [
    source 28
    target 155
  ]
  edge [
    source 156
    target 157
  ]
  edge [
    source 158
    target 159
  ]
  edge [
    source 160
    target 161
  ]
  edge [
    source 162
    target 163
  ]
  edge [
    source 162
    target 164
  ]
  edge [
    source 163
    target 164
  ]
]
