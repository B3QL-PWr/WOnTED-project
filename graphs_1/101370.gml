graph [
  maxDegree 6
  minDegree 1
  meanDegree 1.7142857142857142
  density 0.2857142857142857
  graphCliqueNumber 2
  node [
    id 0
    label "hrabstwo"
    origin "text"
  ]
  node [
    id 1
    label "mona"
    origin "text"
  ]
  node [
    id 2
    label "Yorkshire"
  ]
  node [
    id 3
    label "jednostka_administracyjna"
  ]
  node [
    id 4
    label "Norfolk"
  ]
  node [
    id 5
    label "Kornwalia"
  ]
  node [
    id 6
    label "maj&#261;tek"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
]
