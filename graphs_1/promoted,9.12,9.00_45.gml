graph [
  maxDegree 11
  minDegree 1
  meanDegree 2
  density 0.1
  graphCliqueNumber 2
  node [
    id 0
    label "gdy"
    origin "text"
  ]
  node [
    id 1
    label "recenzowa&#263;by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "dron"
    origin "text"
  ]
  node [
    id 3
    label "tello"
    origin "text"
  ]
  node [
    id 4
    label "szczeg&#243;lny"
    origin "text"
  ]
  node [
    id 5
    label "uwaga"
    origin "text"
  ]
  node [
    id 6
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 7
    label "zaprogramowa&#263;"
    origin "text"
  ]
  node [
    id 8
    label "samolot"
  ]
  node [
    id 9
    label "szczeg&#243;lnie"
  ]
  node [
    id 10
    label "wyj&#261;tkowy"
  ]
  node [
    id 11
    label "nagana"
  ]
  node [
    id 12
    label "wypowied&#378;"
  ]
  node [
    id 13
    label "stan"
  ]
  node [
    id 14
    label "dzienniczek"
  ]
  node [
    id 15
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 16
    label "wzgl&#261;d"
  ]
  node [
    id 17
    label "gossip"
  ]
  node [
    id 18
    label "upomnienie"
  ]
  node [
    id 19
    label "tekst"
  ]
  node [
    id 20
    label "free"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 4
    target 10
  ]
  edge [
    source 5
    target 11
  ]
  edge [
    source 5
    target 12
  ]
  edge [
    source 5
    target 13
  ]
  edge [
    source 5
    target 14
  ]
  edge [
    source 5
    target 15
  ]
  edge [
    source 5
    target 16
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 20
  ]
]
