graph [
  maxDegree 3
  minDegree 1
  meanDegree 2.25
  density 0.32142857142857145
  graphCliqueNumber 4
  node [
    id 0
    label "halka"
    origin "text"
  ]
  node [
    id 1
    label "miraski"
    origin "text"
  ]
  node [
    id 2
    label "sp&#243;d"
  ]
  node [
    id 3
    label "Miraski"
  ]
  node [
    id 4
    label "pozna&#263;"
  ]
  node [
    id 5
    label "CarCharger"
  ]
  node [
    id 6
    label "USB"
  ]
  node [
    id 7
    label "C"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 6
    target 7
  ]
]
