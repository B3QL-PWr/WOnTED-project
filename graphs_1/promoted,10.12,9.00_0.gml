graph [
  maxDegree 25
  minDegree 1
  meanDegree 2
  density 0.03636363636363636
  graphCliqueNumber 3
  node [
    id 0
    label "stroni&#263;"
    origin "text"
  ]
  node [
    id 1
    label "przy"
    origin "text"
  ]
  node [
    id 2
    label "manipulacja"
    origin "text"
  ]
  node [
    id 3
    label "temat"
    origin "text"
  ]
  node [
    id 4
    label "sam"
    origin "text"
  ]
  node [
    id 5
    label "artyku&#322;"
    origin "text"
  ]
  node [
    id 6
    label "komentarz"
    origin "text"
  ]
  node [
    id 7
    label "wykopki"
    origin "text"
  ]
  node [
    id 8
    label "z_daleka"
  ]
  node [
    id 9
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 10
    label "czynno&#347;&#263;"
  ]
  node [
    id 11
    label "podst&#281;p"
  ]
  node [
    id 12
    label "fraza"
  ]
  node [
    id 13
    label "forma"
  ]
  node [
    id 14
    label "melodia"
  ]
  node [
    id 15
    label "rzecz"
  ]
  node [
    id 16
    label "zbacza&#263;"
  ]
  node [
    id 17
    label "entity"
  ]
  node [
    id 18
    label "omawia&#263;"
  ]
  node [
    id 19
    label "topik"
  ]
  node [
    id 20
    label "wyraz_pochodny"
  ]
  node [
    id 21
    label "om&#243;wi&#263;"
  ]
  node [
    id 22
    label "omawianie"
  ]
  node [
    id 23
    label "w&#261;tek"
  ]
  node [
    id 24
    label "forum"
  ]
  node [
    id 25
    label "cecha"
  ]
  node [
    id 26
    label "zboczenie"
  ]
  node [
    id 27
    label "zbaczanie"
  ]
  node [
    id 28
    label "tre&#347;&#263;"
  ]
  node [
    id 29
    label "tematyka"
  ]
  node [
    id 30
    label "sprawa"
  ]
  node [
    id 31
    label "istota"
  ]
  node [
    id 32
    label "otoczka"
  ]
  node [
    id 33
    label "zboczy&#263;"
  ]
  node [
    id 34
    label "om&#243;wienie"
  ]
  node [
    id 35
    label "sklep"
  ]
  node [
    id 36
    label "dokument"
  ]
  node [
    id 37
    label "towar"
  ]
  node [
    id 38
    label "nag&#322;&#243;wek"
  ]
  node [
    id 39
    label "znak_j&#281;zykowy"
  ]
  node [
    id 40
    label "wyr&#243;b"
  ]
  node [
    id 41
    label "blok"
  ]
  node [
    id 42
    label "line"
  ]
  node [
    id 43
    label "paragraf"
  ]
  node [
    id 44
    label "rodzajnik"
  ]
  node [
    id 45
    label "prawda"
  ]
  node [
    id 46
    label "szkic"
  ]
  node [
    id 47
    label "tekst"
  ]
  node [
    id 48
    label "fragment"
  ]
  node [
    id 49
    label "comment"
  ]
  node [
    id 50
    label "ocena"
  ]
  node [
    id 51
    label "gossip"
  ]
  node [
    id 52
    label "interpretacja"
  ]
  node [
    id 53
    label "audycja"
  ]
  node [
    id 54
    label "zbi&#243;r"
  ]
  node [
    id 55
    label "sadzeniak"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
]
