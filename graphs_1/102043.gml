graph [
  maxDegree 39
  minDegree 1
  meanDegree 2.121037463976945
  density 0.006130166080858223
  graphCliqueNumber 3
  node [
    id 0
    label "je&#347;li"
    origin "text"
  ]
  node [
    id 1
    label "bzura"
    origin "text"
  ]
  node [
    id 2
    label "wzbi&#263;"
    origin "text"
  ]
  node [
    id 3
    label "si&#281;"
    origin "text"
  ]
  node [
    id 4
    label "wy&#380;yna"
    origin "text"
  ]
  node [
    id 5
    label "pokona&#263;"
    origin "text"
  ]
  node [
    id 6
    label "rz&#261;&#347;ni&#281;"
    origin "text"
  ]
  node [
    id 7
    label "zawodnik"
    origin "text"
  ]
  node [
    id 8
    label "dru&#380;yna"
    origin "text"
  ]
  node [
    id 9
    label "prezentowa&#263;"
    origin "text"
  ]
  node [
    id 10
    label "podobny"
    origin "text"
  ]
  node [
    id 11
    label "poziom"
    origin "text"
  ]
  node [
    id 12
    label "zwyci&#281;stwo"
    origin "text"
  ]
  node [
    id 13
    label "zdecydowa&#263;"
    origin "text"
  ]
  node [
    id 14
    label "niuans"
    origin "text"
  ]
  node [
    id 15
    label "wa&#380;ny"
    origin "text"
  ]
  node [
    id 16
    label "by&#263;"
    origin "text"
  ]
  node [
    id 17
    label "&#380;eby"
    origin "text"
  ]
  node [
    id 18
    label "wygra&#263;"
    origin "text"
  ]
  node [
    id 19
    label "chocia&#380;"
    origin "text"
  ]
  node [
    id 20
    label "jeden"
    origin "text"
  ]
  node [
    id 21
    label "mecz"
    origin "text"
  ]
  node [
    id 22
    label "wtedy"
    origin "text"
  ]
  node [
    id 23
    label "rywalizacja"
    origin "text"
  ]
  node [
    id 24
    label "przenie&#347;&#263;"
    origin "text"
  ]
  node [
    id 25
    label "ozorkowy"
    origin "text"
  ]
  node [
    id 26
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 27
    label "ciekawie"
    origin "text"
  ]
  node [
    id 28
    label "ostatni"
    origin "text"
  ]
  node [
    id 29
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 30
    label "przegrany"
    origin "text"
  ]
  node [
    id 31
    label "przez"
    origin "text"
  ]
  node [
    id 32
    label "g&#322;owa"
    origin "text"
  ]
  node [
    id 33
    label "zabrakn&#261;&#263;"
    origin "text"
  ]
  node [
    id 34
    label "odporno&#347;&#263;"
    origin "text"
  ]
  node [
    id 35
    label "psychiczny"
    origin "text"
  ]
  node [
    id 36
    label "motywacja"
    origin "text"
  ]
  node [
    id 37
    label "teraz"
    origin "text"
  ]
  node [
    id 38
    label "ozorkowianie"
    origin "text"
  ]
  node [
    id 39
    label "mogel"
    origin "text"
  ]
  node [
    id 40
    label "liczy&#263;"
    origin "text"
  ]
  node [
    id 41
    label "minimalny"
    origin "text"
  ]
  node [
    id 42
    label "wsparcie"
    origin "text"
  ]
  node [
    id 43
    label "kibic"
    origin "text"
  ]
  node [
    id 44
    label "lepsze"
    origin "text"
  ]
  node [
    id 45
    label "ni&#380;"
    origin "text"
  ]
  node [
    id 46
    label "ca&#322;kowity"
    origin "text"
  ]
  node [
    id 47
    label "brak"
    origin "text"
  ]
  node [
    id 48
    label "spowodowa&#263;"
  ]
  node [
    id 49
    label "wbi&#263;"
  ]
  node [
    id 50
    label "podnie&#347;&#263;"
  ]
  node [
    id 51
    label "pos&#322;a&#263;"
  ]
  node [
    id 52
    label "obszar"
  ]
  node [
    id 53
    label "l&#261;d"
  ]
  node [
    id 54
    label "Dekan"
  ]
  node [
    id 55
    label "Jura"
  ]
  node [
    id 56
    label "z&#322;oi&#263;"
  ]
  node [
    id 57
    label "zaatakowa&#263;"
  ]
  node [
    id 58
    label "zapobiec"
  ]
  node [
    id 59
    label "poradzi&#263;_sobie"
  ]
  node [
    id 60
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 61
    label "overwhelm"
  ]
  node [
    id 62
    label "facet"
  ]
  node [
    id 63
    label "czo&#322;&#243;wka"
  ]
  node [
    id 64
    label "lista_startowa"
  ]
  node [
    id 65
    label "uczestnik"
  ]
  node [
    id 66
    label "orygina&#322;"
  ]
  node [
    id 67
    label "sportowiec"
  ]
  node [
    id 68
    label "zi&#243;&#322;ko"
  ]
  node [
    id 69
    label "whole"
  ]
  node [
    id 70
    label "szczep"
  ]
  node [
    id 71
    label "dublet"
  ]
  node [
    id 72
    label "pluton"
  ]
  node [
    id 73
    label "formacja"
  ]
  node [
    id 74
    label "zesp&#243;&#322;"
  ]
  node [
    id 75
    label "force"
  ]
  node [
    id 76
    label "zast&#281;p"
  ]
  node [
    id 77
    label "pododdzia&#322;"
  ]
  node [
    id 78
    label "zapoznawa&#263;"
  ]
  node [
    id 79
    label "przedstawia&#263;"
  ]
  node [
    id 80
    label "present"
  ]
  node [
    id 81
    label "gra&#263;"
  ]
  node [
    id 82
    label "uprzedza&#263;"
  ]
  node [
    id 83
    label "represent"
  ]
  node [
    id 84
    label "program"
  ]
  node [
    id 85
    label "wyra&#380;a&#263;"
  ]
  node [
    id 86
    label "attest"
  ]
  node [
    id 87
    label "display"
  ]
  node [
    id 88
    label "podobnie"
  ]
  node [
    id 89
    label "upodabnianie_si&#281;"
  ]
  node [
    id 90
    label "zasymilowanie"
  ]
  node [
    id 91
    label "taki"
  ]
  node [
    id 92
    label "upodobnienie"
  ]
  node [
    id 93
    label "drugi"
  ]
  node [
    id 94
    label "charakterystyczny"
  ]
  node [
    id 95
    label "przypominanie"
  ]
  node [
    id 96
    label "asymilowanie"
  ]
  node [
    id 97
    label "upodobnienie_si&#281;"
  ]
  node [
    id 98
    label "wysoko&#347;&#263;"
  ]
  node [
    id 99
    label "faza"
  ]
  node [
    id 100
    label "szczebel"
  ]
  node [
    id 101
    label "po&#322;o&#380;enie"
  ]
  node [
    id 102
    label "kierunek"
  ]
  node [
    id 103
    label "wyk&#322;adnik"
  ]
  node [
    id 104
    label "budynek"
  ]
  node [
    id 105
    label "punkt_widzenia"
  ]
  node [
    id 106
    label "jako&#347;&#263;"
  ]
  node [
    id 107
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 108
    label "ranga"
  ]
  node [
    id 109
    label "p&#322;aszczyzna"
  ]
  node [
    id 110
    label "puchar"
  ]
  node [
    id 111
    label "beat"
  ]
  node [
    id 112
    label "sukces"
  ]
  node [
    id 113
    label "conquest"
  ]
  node [
    id 114
    label "poradzenie_sobie"
  ]
  node [
    id 115
    label "sta&#263;_si&#281;"
  ]
  node [
    id 116
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 117
    label "podj&#261;&#263;"
  ]
  node [
    id 118
    label "determine"
  ]
  node [
    id 119
    label "decide"
  ]
  node [
    id 120
    label "zrobi&#263;"
  ]
  node [
    id 121
    label "niuansowa&#263;_si&#281;"
  ]
  node [
    id 122
    label "niuansowa&#263;"
  ]
  node [
    id 123
    label "zniuansowa&#263;"
  ]
  node [
    id 124
    label "sk&#322;adnik"
  ]
  node [
    id 125
    label "zniuansowa&#263;_si&#281;"
  ]
  node [
    id 126
    label "silny"
  ]
  node [
    id 127
    label "wa&#380;nie"
  ]
  node [
    id 128
    label "eksponowany"
  ]
  node [
    id 129
    label "istotnie"
  ]
  node [
    id 130
    label "znaczny"
  ]
  node [
    id 131
    label "dobry"
  ]
  node [
    id 132
    label "wynios&#322;y"
  ]
  node [
    id 133
    label "dono&#347;ny"
  ]
  node [
    id 134
    label "si&#281;ga&#263;"
  ]
  node [
    id 135
    label "trwa&#263;"
  ]
  node [
    id 136
    label "obecno&#347;&#263;"
  ]
  node [
    id 137
    label "stan"
  ]
  node [
    id 138
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 139
    label "stand"
  ]
  node [
    id 140
    label "mie&#263;_miejsce"
  ]
  node [
    id 141
    label "uczestniczy&#263;"
  ]
  node [
    id 142
    label "chodzi&#263;"
  ]
  node [
    id 143
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 144
    label "equal"
  ]
  node [
    id 145
    label "score"
  ]
  node [
    id 146
    label "okaza&#263;_si&#281;"
  ]
  node [
    id 147
    label "zwojowa&#263;"
  ]
  node [
    id 148
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 149
    label "leave"
  ]
  node [
    id 150
    label "znie&#347;&#263;"
  ]
  node [
    id 151
    label "zagwarantowa&#263;"
  ]
  node [
    id 152
    label "instrument_muzyczny"
  ]
  node [
    id 153
    label "zagra&#263;"
  ]
  node [
    id 154
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 155
    label "net_income"
  ]
  node [
    id 156
    label "kieliszek"
  ]
  node [
    id 157
    label "shot"
  ]
  node [
    id 158
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 159
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 160
    label "jaki&#347;"
  ]
  node [
    id 161
    label "jednolicie"
  ]
  node [
    id 162
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 163
    label "w&#243;dka"
  ]
  node [
    id 164
    label "ten"
  ]
  node [
    id 165
    label "ujednolicenie"
  ]
  node [
    id 166
    label "jednakowy"
  ]
  node [
    id 167
    label "obrona"
  ]
  node [
    id 168
    label "gra"
  ]
  node [
    id 169
    label "dwumecz"
  ]
  node [
    id 170
    label "game"
  ]
  node [
    id 171
    label "serw"
  ]
  node [
    id 172
    label "kiedy&#347;"
  ]
  node [
    id 173
    label "contest"
  ]
  node [
    id 174
    label "wydarzenie"
  ]
  node [
    id 175
    label "dostosowa&#263;"
  ]
  node [
    id 176
    label "motivate"
  ]
  node [
    id 177
    label "strzeli&#263;"
  ]
  node [
    id 178
    label "shift"
  ]
  node [
    id 179
    label "deepen"
  ]
  node [
    id 180
    label "relocate"
  ]
  node [
    id 181
    label "skopiowa&#263;"
  ]
  node [
    id 182
    label "przelecie&#263;"
  ]
  node [
    id 183
    label "rozpowszechni&#263;"
  ]
  node [
    id 184
    label "transfer"
  ]
  node [
    id 185
    label "pocisk"
  ]
  node [
    id 186
    label "umie&#347;ci&#263;"
  ]
  node [
    id 187
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 188
    label "zmieni&#263;"
  ]
  node [
    id 189
    label "go"
  ]
  node [
    id 190
    label "mi&#281;sny"
  ]
  node [
    id 191
    label "dziwnie"
  ]
  node [
    id 192
    label "dobrze"
  ]
  node [
    id 193
    label "interesuj&#261;co"
  ]
  node [
    id 194
    label "swoi&#347;cie"
  ]
  node [
    id 195
    label "ciekawy"
  ]
  node [
    id 196
    label "cz&#322;owiek"
  ]
  node [
    id 197
    label "kolejny"
  ]
  node [
    id 198
    label "istota_&#380;ywa"
  ]
  node [
    id 199
    label "najgorszy"
  ]
  node [
    id 200
    label "aktualny"
  ]
  node [
    id 201
    label "ostatnio"
  ]
  node [
    id 202
    label "niedawno"
  ]
  node [
    id 203
    label "wi&#261;&#380;&#261;cy"
  ]
  node [
    id 204
    label "sko&#324;czony"
  ]
  node [
    id 205
    label "poprzedni"
  ]
  node [
    id 206
    label "pozosta&#322;y"
  ]
  node [
    id 207
    label "w&#261;tpliwy"
  ]
  node [
    id 208
    label "proceed"
  ]
  node [
    id 209
    label "catch"
  ]
  node [
    id 210
    label "pozosta&#263;"
  ]
  node [
    id 211
    label "osta&#263;_si&#281;"
  ]
  node [
    id 212
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 213
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 214
    label "change"
  ]
  node [
    id 215
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 216
    label "beznadziejny"
  ]
  node [
    id 217
    label "zrezygnowany"
  ]
  node [
    id 218
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 219
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 220
    label "ucho"
  ]
  node [
    id 221
    label "makrocefalia"
  ]
  node [
    id 222
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 223
    label "m&#243;zg"
  ]
  node [
    id 224
    label "kierownictwo"
  ]
  node [
    id 225
    label "czaszka"
  ]
  node [
    id 226
    label "dekiel"
  ]
  node [
    id 227
    label "umys&#322;"
  ]
  node [
    id 228
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 229
    label "&#347;ci&#281;cie"
  ]
  node [
    id 230
    label "sztuka"
  ]
  node [
    id 231
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 232
    label "g&#243;ra"
  ]
  node [
    id 233
    label "byd&#322;o"
  ]
  node [
    id 234
    label "alkohol"
  ]
  node [
    id 235
    label "wiedza"
  ]
  node [
    id 236
    label "ro&#347;lina"
  ]
  node [
    id 237
    label "&#347;ci&#281;gno"
  ]
  node [
    id 238
    label "&#380;ycie"
  ]
  node [
    id 239
    label "pryncypa&#322;"
  ]
  node [
    id 240
    label "fryzura"
  ]
  node [
    id 241
    label "noosfera"
  ]
  node [
    id 242
    label "kierowa&#263;"
  ]
  node [
    id 243
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 244
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 245
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 246
    label "cecha"
  ]
  node [
    id 247
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 248
    label "zdolno&#347;&#263;"
  ]
  node [
    id 249
    label "kszta&#322;t"
  ]
  node [
    id 250
    label "cz&#322;onek"
  ]
  node [
    id 251
    label "cia&#322;o"
  ]
  node [
    id 252
    label "obiekt"
  ]
  node [
    id 253
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 254
    label "lack"
  ]
  node [
    id 255
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 256
    label "wytrzyma&#322;o&#347;&#263;"
  ]
  node [
    id 257
    label "immunity"
  ]
  node [
    id 258
    label "zdrowie"
  ]
  node [
    id 259
    label "odciele&#347;nienie_si&#281;"
  ]
  node [
    id 260
    label "nerwowo_chory"
  ]
  node [
    id 261
    label "odciele&#347;nianie_si&#281;"
  ]
  node [
    id 262
    label "niematerialny"
  ]
  node [
    id 263
    label "niezr&#243;wnowa&#380;ony"
  ]
  node [
    id 264
    label "psychiatra"
  ]
  node [
    id 265
    label "nienormalny"
  ]
  node [
    id 266
    label "psychicznie"
  ]
  node [
    id 267
    label "justyfikacja"
  ]
  node [
    id 268
    label "wyraz_pochodny"
  ]
  node [
    id 269
    label "wyraz_podstawowy"
  ]
  node [
    id 270
    label "apologetyk"
  ]
  node [
    id 271
    label "informacja"
  ]
  node [
    id 272
    label "pobudka"
  ]
  node [
    id 273
    label "relacja"
  ]
  node [
    id 274
    label "chwila"
  ]
  node [
    id 275
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 276
    label "report"
  ]
  node [
    id 277
    label "dodawa&#263;"
  ]
  node [
    id 278
    label "wymienia&#263;"
  ]
  node [
    id 279
    label "okre&#347;la&#263;"
  ]
  node [
    id 280
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 281
    label "dyskalkulia"
  ]
  node [
    id 282
    label "wynagrodzenie"
  ]
  node [
    id 283
    label "admit"
  ]
  node [
    id 284
    label "osi&#261;ga&#263;"
  ]
  node [
    id 285
    label "wyznacza&#263;"
  ]
  node [
    id 286
    label "posiada&#263;"
  ]
  node [
    id 287
    label "mierzy&#263;"
  ]
  node [
    id 288
    label "odlicza&#263;"
  ]
  node [
    id 289
    label "bra&#263;"
  ]
  node [
    id 290
    label "wycenia&#263;"
  ]
  node [
    id 291
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 292
    label "rachowa&#263;"
  ]
  node [
    id 293
    label "tell"
  ]
  node [
    id 294
    label "uwzgl&#281;dnia&#263;"
  ]
  node [
    id 295
    label "policza&#263;"
  ]
  node [
    id 296
    label "count"
  ]
  node [
    id 297
    label "graniczny"
  ]
  node [
    id 298
    label "minimalnie"
  ]
  node [
    id 299
    label "minimalizowanie"
  ]
  node [
    id 300
    label "zminimalizowanie"
  ]
  node [
    id 301
    label "comfort"
  ]
  node [
    id 302
    label "u&#322;atwienie"
  ]
  node [
    id 303
    label "doch&#243;d"
  ]
  node [
    id 304
    label "oparcie"
  ]
  node [
    id 305
    label "telefon_zaufania"
  ]
  node [
    id 306
    label "dar"
  ]
  node [
    id 307
    label "zapomoga"
  ]
  node [
    id 308
    label "pocieszenie"
  ]
  node [
    id 309
    label "darowizna"
  ]
  node [
    id 310
    label "pomoc"
  ]
  node [
    id 311
    label "&#347;rodek"
  ]
  node [
    id 312
    label "wyci&#261;gni&#281;cie_pomocnej_d&#322;oni"
  ]
  node [
    id 313
    label "support"
  ]
  node [
    id 314
    label "fan"
  ]
  node [
    id 315
    label "widz"
  ]
  node [
    id 316
    label "&#380;yleta"
  ]
  node [
    id 317
    label "zach&#281;ta"
  ]
  node [
    id 318
    label "depression"
  ]
  node [
    id 319
    label "zjawisko"
  ]
  node [
    id 320
    label "nizina"
  ]
  node [
    id 321
    label "w_pizdu"
  ]
  node [
    id 322
    label "&#322;&#261;czny"
  ]
  node [
    id 323
    label "og&#243;lnie"
  ]
  node [
    id 324
    label "ca&#322;y"
  ]
  node [
    id 325
    label "pe&#322;ny"
  ]
  node [
    id 326
    label "zupe&#322;nie"
  ]
  node [
    id 327
    label "kompletnie"
  ]
  node [
    id 328
    label "prywatywny"
  ]
  node [
    id 329
    label "defect"
  ]
  node [
    id 330
    label "odej&#347;cie"
  ]
  node [
    id 331
    label "gap"
  ]
  node [
    id 332
    label "kr&#243;tki"
  ]
  node [
    id 333
    label "wyr&#243;b"
  ]
  node [
    id 334
    label "nieistnienie"
  ]
  node [
    id 335
    label "wada"
  ]
  node [
    id 336
    label "odej&#347;&#263;"
  ]
  node [
    id 337
    label "odchodzenie"
  ]
  node [
    id 338
    label "odchodzi&#263;"
  ]
  node [
    id 339
    label "Rafa&#322;"
  ]
  node [
    id 340
    label "Grabarczyk"
  ]
  node [
    id 341
    label "Damian"
  ]
  node [
    id 342
    label "Sobczak"
  ]
  node [
    id 343
    label "Adam"
  ]
  node [
    id 344
    label "Pietrzak"
  ]
  node [
    id 345
    label "Ikar"
  ]
  node [
    id 346
    label "Legnica"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 1
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 17
  ]
  edge [
    source 7
    target 31
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 19
  ]
  edge [
    source 8
    target 38
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 98
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 11
    target 100
  ]
  edge [
    source 11
    target 101
  ]
  edge [
    source 11
    target 102
  ]
  edge [
    source 11
    target 103
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 108
  ]
  edge [
    source 11
    target 109
  ]
  edge [
    source 11
    target 45
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 110
  ]
  edge [
    source 12
    target 111
  ]
  edge [
    source 12
    target 112
  ]
  edge [
    source 12
    target 113
  ]
  edge [
    source 12
    target 114
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 115
  ]
  edge [
    source 13
    target 116
  ]
  edge [
    source 13
    target 117
  ]
  edge [
    source 13
    target 118
  ]
  edge [
    source 13
    target 119
  ]
  edge [
    source 13
    target 120
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 121
  ]
  edge [
    source 14
    target 122
  ]
  edge [
    source 14
    target 123
  ]
  edge [
    source 14
    target 124
  ]
  edge [
    source 14
    target 125
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 126
  ]
  edge [
    source 15
    target 127
  ]
  edge [
    source 15
    target 128
  ]
  edge [
    source 15
    target 129
  ]
  edge [
    source 15
    target 130
  ]
  edge [
    source 15
    target 131
  ]
  edge [
    source 15
    target 132
  ]
  edge [
    source 15
    target 133
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 26
  ]
  edge [
    source 16
    target 27
  ]
  edge [
    source 16
    target 38
  ]
  edge [
    source 16
    target 39
  ]
  edge [
    source 16
    target 134
  ]
  edge [
    source 16
    target 135
  ]
  edge [
    source 16
    target 136
  ]
  edge [
    source 16
    target 137
  ]
  edge [
    source 16
    target 138
  ]
  edge [
    source 16
    target 139
  ]
  edge [
    source 16
    target 140
  ]
  edge [
    source 16
    target 141
  ]
  edge [
    source 16
    target 142
  ]
  edge [
    source 16
    target 143
  ]
  edge [
    source 16
    target 144
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 145
  ]
  edge [
    source 18
    target 146
  ]
  edge [
    source 18
    target 147
  ]
  edge [
    source 18
    target 148
  ]
  edge [
    source 18
    target 149
  ]
  edge [
    source 18
    target 150
  ]
  edge [
    source 18
    target 151
  ]
  edge [
    source 18
    target 152
  ]
  edge [
    source 18
    target 153
  ]
  edge [
    source 18
    target 154
  ]
  edge [
    source 18
    target 120
  ]
  edge [
    source 18
    target 155
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 38
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 156
  ]
  edge [
    source 20
    target 157
  ]
  edge [
    source 20
    target 158
  ]
  edge [
    source 20
    target 159
  ]
  edge [
    source 20
    target 160
  ]
  edge [
    source 20
    target 161
  ]
  edge [
    source 20
    target 162
  ]
  edge [
    source 20
    target 163
  ]
  edge [
    source 20
    target 164
  ]
  edge [
    source 20
    target 165
  ]
  edge [
    source 20
    target 166
  ]
  edge [
    source 20
    target 39
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 28
  ]
  edge [
    source 21
    target 29
  ]
  edge [
    source 21
    target 167
  ]
  edge [
    source 21
    target 168
  ]
  edge [
    source 21
    target 169
  ]
  edge [
    source 21
    target 170
  ]
  edge [
    source 21
    target 171
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 172
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 173
  ]
  edge [
    source 23
    target 174
  ]
  edge [
    source 24
    target 175
  ]
  edge [
    source 24
    target 176
  ]
  edge [
    source 24
    target 177
  ]
  edge [
    source 24
    target 178
  ]
  edge [
    source 24
    target 179
  ]
  edge [
    source 24
    target 180
  ]
  edge [
    source 24
    target 181
  ]
  edge [
    source 24
    target 182
  ]
  edge [
    source 24
    target 183
  ]
  edge [
    source 24
    target 184
  ]
  edge [
    source 24
    target 185
  ]
  edge [
    source 24
    target 186
  ]
  edge [
    source 24
    target 187
  ]
  edge [
    source 24
    target 188
  ]
  edge [
    source 24
    target 189
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 190
  ]
  edge [
    source 25
    target 34
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 191
  ]
  edge [
    source 27
    target 192
  ]
  edge [
    source 27
    target 193
  ]
  edge [
    source 27
    target 194
  ]
  edge [
    source 27
    target 195
  ]
  edge [
    source 27
    target 46
  ]
  edge [
    source 28
    target 196
  ]
  edge [
    source 28
    target 197
  ]
  edge [
    source 28
    target 198
  ]
  edge [
    source 28
    target 199
  ]
  edge [
    source 28
    target 200
  ]
  edge [
    source 28
    target 201
  ]
  edge [
    source 28
    target 202
  ]
  edge [
    source 28
    target 203
  ]
  edge [
    source 28
    target 204
  ]
  edge [
    source 28
    target 205
  ]
  edge [
    source 28
    target 206
  ]
  edge [
    source 28
    target 207
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 208
  ]
  edge [
    source 29
    target 209
  ]
  edge [
    source 29
    target 210
  ]
  edge [
    source 29
    target 211
  ]
  edge [
    source 29
    target 212
  ]
  edge [
    source 29
    target 213
  ]
  edge [
    source 29
    target 154
  ]
  edge [
    source 29
    target 214
  ]
  edge [
    source 29
    target 215
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 65
  ]
  edge [
    source 30
    target 216
  ]
  edge [
    source 30
    target 217
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 196
  ]
  edge [
    source 32
    target 218
  ]
  edge [
    source 32
    target 219
  ]
  edge [
    source 32
    target 220
  ]
  edge [
    source 32
    target 221
  ]
  edge [
    source 32
    target 222
  ]
  edge [
    source 32
    target 223
  ]
  edge [
    source 32
    target 224
  ]
  edge [
    source 32
    target 225
  ]
  edge [
    source 32
    target 226
  ]
  edge [
    source 32
    target 227
  ]
  edge [
    source 32
    target 228
  ]
  edge [
    source 32
    target 229
  ]
  edge [
    source 32
    target 230
  ]
  edge [
    source 32
    target 231
  ]
  edge [
    source 32
    target 232
  ]
  edge [
    source 32
    target 233
  ]
  edge [
    source 32
    target 234
  ]
  edge [
    source 32
    target 235
  ]
  edge [
    source 32
    target 236
  ]
  edge [
    source 32
    target 237
  ]
  edge [
    source 32
    target 238
  ]
  edge [
    source 32
    target 239
  ]
  edge [
    source 32
    target 240
  ]
  edge [
    source 32
    target 241
  ]
  edge [
    source 32
    target 242
  ]
  edge [
    source 32
    target 243
  ]
  edge [
    source 32
    target 244
  ]
  edge [
    source 32
    target 245
  ]
  edge [
    source 32
    target 246
  ]
  edge [
    source 32
    target 247
  ]
  edge [
    source 32
    target 248
  ]
  edge [
    source 32
    target 249
  ]
  edge [
    source 32
    target 250
  ]
  edge [
    source 32
    target 251
  ]
  edge [
    source 32
    target 252
  ]
  edge [
    source 32
    target 253
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 154
  ]
  edge [
    source 33
    target 254
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 255
  ]
  edge [
    source 34
    target 256
  ]
  edge [
    source 34
    target 257
  ]
  edge [
    source 34
    target 258
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 259
  ]
  edge [
    source 35
    target 260
  ]
  edge [
    source 35
    target 261
  ]
  edge [
    source 35
    target 262
  ]
  edge [
    source 35
    target 263
  ]
  edge [
    source 35
    target 264
  ]
  edge [
    source 35
    target 265
  ]
  edge [
    source 35
    target 266
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 267
  ]
  edge [
    source 36
    target 268
  ]
  edge [
    source 36
    target 269
  ]
  edge [
    source 36
    target 270
  ]
  edge [
    source 36
    target 271
  ]
  edge [
    source 36
    target 272
  ]
  edge [
    source 36
    target 273
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 274
  ]
  edge [
    source 37
    target 275
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 276
  ]
  edge [
    source 40
    target 277
  ]
  edge [
    source 40
    target 278
  ]
  edge [
    source 40
    target 279
  ]
  edge [
    source 40
    target 280
  ]
  edge [
    source 40
    target 281
  ]
  edge [
    source 40
    target 282
  ]
  edge [
    source 40
    target 283
  ]
  edge [
    source 40
    target 284
  ]
  edge [
    source 40
    target 285
  ]
  edge [
    source 40
    target 286
  ]
  edge [
    source 40
    target 287
  ]
  edge [
    source 40
    target 288
  ]
  edge [
    source 40
    target 289
  ]
  edge [
    source 40
    target 290
  ]
  edge [
    source 40
    target 291
  ]
  edge [
    source 40
    target 292
  ]
  edge [
    source 40
    target 293
  ]
  edge [
    source 40
    target 294
  ]
  edge [
    source 40
    target 295
  ]
  edge [
    source 40
    target 296
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 297
  ]
  edge [
    source 41
    target 298
  ]
  edge [
    source 41
    target 299
  ]
  edge [
    source 41
    target 300
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 301
  ]
  edge [
    source 42
    target 302
  ]
  edge [
    source 42
    target 303
  ]
  edge [
    source 42
    target 304
  ]
  edge [
    source 42
    target 305
  ]
  edge [
    source 42
    target 306
  ]
  edge [
    source 42
    target 307
  ]
  edge [
    source 42
    target 308
  ]
  edge [
    source 42
    target 309
  ]
  edge [
    source 42
    target 310
  ]
  edge [
    source 42
    target 311
  ]
  edge [
    source 42
    target 312
  ]
  edge [
    source 42
    target 313
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 314
  ]
  edge [
    source 43
    target 315
  ]
  edge [
    source 43
    target 316
  ]
  edge [
    source 43
    target 317
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 99
  ]
  edge [
    source 45
    target 318
  ]
  edge [
    source 45
    target 319
  ]
  edge [
    source 45
    target 320
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 321
  ]
  edge [
    source 46
    target 322
  ]
  edge [
    source 46
    target 323
  ]
  edge [
    source 46
    target 324
  ]
  edge [
    source 46
    target 325
  ]
  edge [
    source 46
    target 326
  ]
  edge [
    source 46
    target 327
  ]
  edge [
    source 47
    target 328
  ]
  edge [
    source 47
    target 329
  ]
  edge [
    source 47
    target 330
  ]
  edge [
    source 47
    target 331
  ]
  edge [
    source 47
    target 332
  ]
  edge [
    source 47
    target 333
  ]
  edge [
    source 47
    target 334
  ]
  edge [
    source 47
    target 335
  ]
  edge [
    source 47
    target 336
  ]
  edge [
    source 47
    target 337
  ]
  edge [
    source 47
    target 338
  ]
  edge [
    source 339
    target 340
  ]
  edge [
    source 341
    target 342
  ]
  edge [
    source 343
    target 344
  ]
  edge [
    source 345
    target 346
  ]
]
