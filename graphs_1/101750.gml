graph [
  maxDegree 49
  minDegree 1
  meanDegree 2.154241645244216
  density 0.005552169188773753
  graphCliqueNumber 3
  node [
    id 0
    label "trzeba"
    origin "text"
  ]
  node [
    id 1
    label "by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "tak"
    origin "text"
  ]
  node [
    id 3
    label "bill"
    origin "text"
  ]
  node [
    id 4
    label "dok&#322;ada&#263;"
    origin "text"
  ]
  node [
    id 5
    label "zemsta"
    origin "text"
  ]
  node [
    id 6
    label "nadej&#347;&#263;"
    origin "text"
  ]
  node [
    id 7
    label "szybko"
    origin "text"
  ]
  node [
    id 8
    label "cho&#263;"
    origin "text"
  ]
  node [
    id 9
    label "niespodziewanie"
    origin "text"
  ]
  node [
    id 10
    label "pierwsza"
    origin "text"
  ]
  node [
    id 11
    label "rocznica"
    origin "text"
  ]
  node [
    id 12
    label "wydanie"
    origin "text"
  ]
  node [
    id 13
    label "visty"
    origin "text"
  ]
  node [
    id 14
    label "r&#281;ka"
    origin "text"
  ]
  node [
    id 15
    label "konsument"
    origin "text"
  ]
  node [
    id 16
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 17
    label "nied&#322;ugo"
    origin "text"
  ]
  node [
    id 18
    label "rama"
    origin "text"
  ]
  node [
    id 19
    label "prezent"
    origin "text"
  ]
  node [
    id 20
    label "postanowi&#263;by&#263;"
    origin "text"
  ]
  node [
    id 21
    label "sprawi&#263;"
    origin "text"
  ]
  node [
    id 22
    label "siebie"
    origin "text"
  ]
  node [
    id 23
    label "egzemplarz"
    origin "text"
  ]
  node [
    id 24
    label "trzy"
    origin "text"
  ]
  node [
    id 25
    label "dni"
    origin "text"
  ]
  node [
    id 26
    label "mama"
    origin "text"
  ]
  node [
    id 27
    label "dzia&#322;a&#263;"
    origin "text"
  ]
  node [
    id 28
    label "system"
    origin "text"
  ]
  node [
    id 29
    label "wydawa&#263;by"
    origin "text"
  ]
  node [
    id 30
    label "si&#281;"
    origin "text"
  ]
  node [
    id 31
    label "instalacja"
    origin "text"
  ]
  node [
    id 32
    label "kilka"
    origin "text"
  ]
  node [
    id 33
    label "dystrybucja"
    origin "text"
  ]
  node [
    id 34
    label "linuksa"
    origin "text"
  ]
  node [
    id 35
    label "r&#243;&#380;ny"
    origin "text"
  ]
  node [
    id 36
    label "komputer"
    origin "text"
  ]
  node [
    id 37
    label "windows"
    origin "text"
  ]
  node [
    id 38
    label "nie"
    origin "text"
  ]
  node [
    id 39
    label "sprawia&#263;"
    origin "text"
  ]
  node [
    id 40
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 41
    label "problem"
    origin "text"
  ]
  node [
    id 42
    label "spodziewa&#263;by&#263;"
    origin "text"
  ]
  node [
    id 43
    label "vi&#347;cie"
    origin "text"
  ]
  node [
    id 44
    label "podoba&#263;"
    origin "text"
  ]
  node [
    id 45
    label "dysk"
    origin "text"
  ]
  node [
    id 46
    label "zara&#380;ony"
    origin "text"
  ]
  node [
    id 47
    label "linuksem"
    origin "text"
  ]
  node [
    id 48
    label "w&#322;asny"
    origin "text"
  ]
  node [
    id 49
    label "instalowa&#263;by&#263;"
    origin "text"
  ]
  node [
    id 50
    label "ciemno"
    origin "text"
  ]
  node [
    id 51
    label "program"
    origin "text"
  ]
  node [
    id 52
    label "sprawdzi&#263;"
    origin "text"
  ]
  node [
    id 53
    label "spe&#322;nia&#263;"
    origin "text"
  ]
  node [
    id 54
    label "wymagania"
    origin "text"
  ]
  node [
    id 55
    label "tylko"
    origin "text"
  ]
  node [
    id 56
    label "pod"
    origin "text"
  ]
  node [
    id 57
    label "trza"
  ]
  node [
    id 58
    label "necessity"
  ]
  node [
    id 59
    label "si&#281;ga&#263;"
  ]
  node [
    id 60
    label "trwa&#263;"
  ]
  node [
    id 61
    label "obecno&#347;&#263;"
  ]
  node [
    id 62
    label "stan"
  ]
  node [
    id 63
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 64
    label "stand"
  ]
  node [
    id 65
    label "mie&#263;_miejsce"
  ]
  node [
    id 66
    label "uczestniczy&#263;"
  ]
  node [
    id 67
    label "chodzi&#263;"
  ]
  node [
    id 68
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 69
    label "equal"
  ]
  node [
    id 70
    label "dodawa&#263;"
  ]
  node [
    id 71
    label "bind"
  ]
  node [
    id 72
    label "reakcja"
  ]
  node [
    id 73
    label "sta&#263;_si&#281;"
  ]
  node [
    id 74
    label "czas"
  ]
  node [
    id 75
    label "catch"
  ]
  node [
    id 76
    label "become"
  ]
  node [
    id 77
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 78
    label "line_up"
  ]
  node [
    id 79
    label "przyby&#263;"
  ]
  node [
    id 80
    label "quicker"
  ]
  node [
    id 81
    label "promptly"
  ]
  node [
    id 82
    label "bezpo&#347;rednio"
  ]
  node [
    id 83
    label "quickest"
  ]
  node [
    id 84
    label "sprawnie"
  ]
  node [
    id 85
    label "dynamicznie"
  ]
  node [
    id 86
    label "szybciej"
  ]
  node [
    id 87
    label "prosto"
  ]
  node [
    id 88
    label "szybciochem"
  ]
  node [
    id 89
    label "szybki"
  ]
  node [
    id 90
    label "nieoczekiwany"
  ]
  node [
    id 91
    label "zaskakuj&#261;co"
  ]
  node [
    id 92
    label "godzina"
  ]
  node [
    id 93
    label "termin"
  ]
  node [
    id 94
    label "obchody"
  ]
  node [
    id 95
    label "delivery"
  ]
  node [
    id 96
    label "podanie"
  ]
  node [
    id 97
    label "issue"
  ]
  node [
    id 98
    label "danie"
  ]
  node [
    id 99
    label "rendition"
  ]
  node [
    id 100
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 101
    label "impression"
  ]
  node [
    id 102
    label "odmiana"
  ]
  node [
    id 103
    label "zapach"
  ]
  node [
    id 104
    label "wytworzenie"
  ]
  node [
    id 105
    label "wprowadzenie"
  ]
  node [
    id 106
    label "zdarzenie_si&#281;"
  ]
  node [
    id 107
    label "zrobienie"
  ]
  node [
    id 108
    label "ujawnienie"
  ]
  node [
    id 109
    label "reszta"
  ]
  node [
    id 110
    label "ukazanie_si&#281;_drukiem"
  ]
  node [
    id 111
    label "zadenuncjowanie"
  ]
  node [
    id 112
    label "czasopismo"
  ]
  node [
    id 113
    label "urz&#261;dzenie"
  ]
  node [
    id 114
    label "d&#378;wi&#281;k"
  ]
  node [
    id 115
    label "publikacja"
  ]
  node [
    id 116
    label "krzy&#380;"
  ]
  node [
    id 117
    label "paw"
  ]
  node [
    id 118
    label "rami&#281;"
  ]
  node [
    id 119
    label "gestykulowanie"
  ]
  node [
    id 120
    label "pi&#322;ka_no&#380;na"
  ]
  node [
    id 121
    label "pracownik"
  ]
  node [
    id 122
    label "ko&#324;czyna_g&#243;rna"
  ]
  node [
    id 123
    label "bramkarz"
  ]
  node [
    id 124
    label "&#347;r&#243;dr&#281;cze"
  ]
  node [
    id 125
    label "handwriting"
  ]
  node [
    id 126
    label "hasta"
  ]
  node [
    id 127
    label "pi&#322;ka"
  ]
  node [
    id 128
    label "&#322;okie&#263;"
  ]
  node [
    id 129
    label "spos&#243;b"
  ]
  node [
    id 130
    label "zagrywka"
  ]
  node [
    id 131
    label "obietnica"
  ]
  node [
    id 132
    label "przedrami&#281;"
  ]
  node [
    id 133
    label "chwyta&#263;"
  ]
  node [
    id 134
    label "r&#261;czyna"
  ]
  node [
    id 135
    label "cecha"
  ]
  node [
    id 136
    label "wykroczenie"
  ]
  node [
    id 137
    label "kroki"
  ]
  node [
    id 138
    label "palec"
  ]
  node [
    id 139
    label "&#380;&#243;&#322;ta_kartka"
  ]
  node [
    id 140
    label "graba"
  ]
  node [
    id 141
    label "hand"
  ]
  node [
    id 142
    label "nadgarstek"
  ]
  node [
    id 143
    label "pomocnik"
  ]
  node [
    id 144
    label "k&#322;&#261;b"
  ]
  node [
    id 145
    label "hazena"
  ]
  node [
    id 146
    label "gestykulowa&#263;"
  ]
  node [
    id 147
    label "cmoknonsens"
  ]
  node [
    id 148
    label "d&#322;o&#324;"
  ]
  node [
    id 149
    label "chwytanie"
  ]
  node [
    id 150
    label "czerwona_kartka"
  ]
  node [
    id 151
    label "rynek"
  ]
  node [
    id 152
    label "odbiorca"
  ]
  node [
    id 153
    label "go&#347;&#263;"
  ]
  node [
    id 154
    label "zjadacz"
  ]
  node [
    id 155
    label "&#322;a&#324;cuch_pokarmowy"
  ]
  node [
    id 156
    label "restauracja"
  ]
  node [
    id 157
    label "heterotrof"
  ]
  node [
    id 158
    label "klient"
  ]
  node [
    id 159
    label "u&#380;ytkownik"
  ]
  node [
    id 160
    label "wpr&#281;dce"
  ]
  node [
    id 161
    label "blisko"
  ]
  node [
    id 162
    label "nied&#322;ugi"
  ]
  node [
    id 163
    label "kr&#243;tki"
  ]
  node [
    id 164
    label "zakres"
  ]
  node [
    id 165
    label "dodatek"
  ]
  node [
    id 166
    label "struktura"
  ]
  node [
    id 167
    label "stela&#380;"
  ]
  node [
    id 168
    label "za&#322;o&#380;enie"
  ]
  node [
    id 169
    label "human_body"
  ]
  node [
    id 170
    label "szablon"
  ]
  node [
    id 171
    label "oprawa"
  ]
  node [
    id 172
    label "paczka"
  ]
  node [
    id 173
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 174
    label "obramowanie"
  ]
  node [
    id 175
    label "pojazd"
  ]
  node [
    id 176
    label "postawa"
  ]
  node [
    id 177
    label "element_konstrukcyjny"
  ]
  node [
    id 178
    label "dar"
  ]
  node [
    id 179
    label "presentation"
  ]
  node [
    id 180
    label "wyrobi&#263;"
  ]
  node [
    id 181
    label "przygotowa&#263;"
  ]
  node [
    id 182
    label "wzi&#261;&#263;"
  ]
  node [
    id 183
    label "spowodowa&#263;"
  ]
  node [
    id 184
    label "frame"
  ]
  node [
    id 185
    label "przyswoi&#263;"
  ]
  node [
    id 186
    label "ewoluowanie"
  ]
  node [
    id 187
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 188
    label "wytw&#243;r"
  ]
  node [
    id 189
    label "obiekt"
  ]
  node [
    id 190
    label "przyswajanie"
  ]
  node [
    id 191
    label "wyewoluowanie"
  ]
  node [
    id 192
    label "ewoluowa&#263;"
  ]
  node [
    id 193
    label "nicpo&#324;"
  ]
  node [
    id 194
    label "przyswojenie"
  ]
  node [
    id 195
    label "wyewoluowa&#263;"
  ]
  node [
    id 196
    label "okaz"
  ]
  node [
    id 197
    label "part"
  ]
  node [
    id 198
    label "individual"
  ]
  node [
    id 199
    label "agent"
  ]
  node [
    id 200
    label "czynnik_biotyczny"
  ]
  node [
    id 201
    label "starzenie_si&#281;"
  ]
  node [
    id 202
    label "przyswaja&#263;"
  ]
  node [
    id 203
    label "sztuka"
  ]
  node [
    id 204
    label "matczysko"
  ]
  node [
    id 205
    label "macierz"
  ]
  node [
    id 206
    label "przodkini"
  ]
  node [
    id 207
    label "Matka_Boska"
  ]
  node [
    id 208
    label "macocha"
  ]
  node [
    id 209
    label "matka_zast&#281;pcza"
  ]
  node [
    id 210
    label "stara"
  ]
  node [
    id 211
    label "rodzice"
  ]
  node [
    id 212
    label "rodzic"
  ]
  node [
    id 213
    label "work"
  ]
  node [
    id 214
    label "reakcja_chemiczna"
  ]
  node [
    id 215
    label "function"
  ]
  node [
    id 216
    label "commit"
  ]
  node [
    id 217
    label "bangla&#263;"
  ]
  node [
    id 218
    label "robi&#263;"
  ]
  node [
    id 219
    label "determine"
  ]
  node [
    id 220
    label "tryb"
  ]
  node [
    id 221
    label "powodowa&#263;"
  ]
  node [
    id 222
    label "dziama&#263;"
  ]
  node [
    id 223
    label "istnie&#263;"
  ]
  node [
    id 224
    label "model"
  ]
  node [
    id 225
    label "sk&#322;ad"
  ]
  node [
    id 226
    label "zachowanie"
  ]
  node [
    id 227
    label "podstawa"
  ]
  node [
    id 228
    label "porz&#261;dek"
  ]
  node [
    id 229
    label "Android"
  ]
  node [
    id 230
    label "przyn&#281;ta"
  ]
  node [
    id 231
    label "jednostka_geologiczna"
  ]
  node [
    id 232
    label "metoda"
  ]
  node [
    id 233
    label "podsystem"
  ]
  node [
    id 234
    label "p&#322;&#243;d"
  ]
  node [
    id 235
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 236
    label "s&#261;d"
  ]
  node [
    id 237
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 238
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 239
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 240
    label "j&#261;dro"
  ]
  node [
    id 241
    label "eratem"
  ]
  node [
    id 242
    label "ryba"
  ]
  node [
    id 243
    label "pulpit"
  ]
  node [
    id 244
    label "oddzia&#322;"
  ]
  node [
    id 245
    label "usenet"
  ]
  node [
    id 246
    label "o&#347;"
  ]
  node [
    id 247
    label "oprogramowanie"
  ]
  node [
    id 248
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 249
    label "poj&#281;cie"
  ]
  node [
    id 250
    label "w&#281;dkarstwo"
  ]
  node [
    id 251
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 252
    label "Leopard"
  ]
  node [
    id 253
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 254
    label "systemik"
  ]
  node [
    id 255
    label "rozprz&#261;c"
  ]
  node [
    id 256
    label "cybernetyk"
  ]
  node [
    id 257
    label "konstelacja"
  ]
  node [
    id 258
    label "doktryna"
  ]
  node [
    id 259
    label "net"
  ]
  node [
    id 260
    label "zbi&#243;r"
  ]
  node [
    id 261
    label "method"
  ]
  node [
    id 262
    label "systemat"
  ]
  node [
    id 263
    label "czynno&#347;&#263;"
  ]
  node [
    id 264
    label "zmiana_po&#322;o&#380;enia"
  ]
  node [
    id 265
    label "uzbrajanie"
  ]
  node [
    id 266
    label "kompozycja"
  ]
  node [
    id 267
    label "proces"
  ]
  node [
    id 268
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 269
    label "&#347;ledziowate"
  ]
  node [
    id 270
    label "podzia&#322;"
  ]
  node [
    id 271
    label "r&#243;&#380;nie"
  ]
  node [
    id 272
    label "inny"
  ]
  node [
    id 273
    label "jaki&#347;"
  ]
  node [
    id 274
    label "pami&#281;&#263;"
  ]
  node [
    id 275
    label "maszyna_Turinga"
  ]
  node [
    id 276
    label "emulacja"
  ]
  node [
    id 277
    label "botnet"
  ]
  node [
    id 278
    label "moc_obliczeniowa"
  ]
  node [
    id 279
    label "stacja_dysk&#243;w"
  ]
  node [
    id 280
    label "monitor"
  ]
  node [
    id 281
    label "instalowanie"
  ]
  node [
    id 282
    label "karta"
  ]
  node [
    id 283
    label "instalowa&#263;"
  ]
  node [
    id 284
    label "elektroniczna_maszyna_licz&#261;ca"
  ]
  node [
    id 285
    label "mysz"
  ]
  node [
    id 286
    label "pad"
  ]
  node [
    id 287
    label "zainstalowanie"
  ]
  node [
    id 288
    label "twardy_dysk"
  ]
  node [
    id 289
    label "radiator"
  ]
  node [
    id 290
    label "modem"
  ]
  node [
    id 291
    label "klawiatura"
  ]
  node [
    id 292
    label "zainstalowa&#263;"
  ]
  node [
    id 293
    label "p&#322;yta_g&#322;&#243;wna"
  ]
  node [
    id 294
    label "procesor"
  ]
  node [
    id 295
    label "sprzeciw"
  ]
  node [
    id 296
    label "get"
  ]
  node [
    id 297
    label "act"
  ]
  node [
    id 298
    label "kupywa&#263;"
  ]
  node [
    id 299
    label "przygotowywa&#263;"
  ]
  node [
    id 300
    label "bra&#263;"
  ]
  node [
    id 301
    label "doros&#322;y"
  ]
  node [
    id 302
    label "wiele"
  ]
  node [
    id 303
    label "dorodny"
  ]
  node [
    id 304
    label "znaczny"
  ]
  node [
    id 305
    label "du&#380;o"
  ]
  node [
    id 306
    label "prawdziwy"
  ]
  node [
    id 307
    label "niema&#322;o"
  ]
  node [
    id 308
    label "wa&#380;ny"
  ]
  node [
    id 309
    label "rozwini&#281;ty"
  ]
  node [
    id 310
    label "trudno&#347;&#263;"
  ]
  node [
    id 311
    label "sprawa"
  ]
  node [
    id 312
    label "ambaras"
  ]
  node [
    id 313
    label "problemat"
  ]
  node [
    id 314
    label "pierepa&#322;ka"
  ]
  node [
    id 315
    label "obstruction"
  ]
  node [
    id 316
    label "problematyka"
  ]
  node [
    id 317
    label "jajko_Kolumba"
  ]
  node [
    id 318
    label "subiekcja"
  ]
  node [
    id 319
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 320
    label "pier&#347;cie&#324;_w&#322;&#243;knisty"
  ]
  node [
    id 321
    label "kr&#281;gos&#322;up"
  ]
  node [
    id 322
    label "p&#322;yta"
  ]
  node [
    id 323
    label "hard_disc"
  ]
  node [
    id 324
    label "j&#261;dro_mia&#380;d&#380;yste"
  ]
  node [
    id 325
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 326
    label "chrz&#261;stka"
  ]
  node [
    id 327
    label "ko&#322;o"
  ]
  node [
    id 328
    label "klaster_dyskowy"
  ]
  node [
    id 329
    label "chory"
  ]
  node [
    id 330
    label "swoisty"
  ]
  node [
    id 331
    label "czyj&#347;"
  ]
  node [
    id 332
    label "osobny"
  ]
  node [
    id 333
    label "zwi&#261;zany"
  ]
  node [
    id 334
    label "samodzielny"
  ]
  node [
    id 335
    label "ciemnota"
  ]
  node [
    id 336
    label "ciemniej"
  ]
  node [
    id 337
    label "noktowizja"
  ]
  node [
    id 338
    label "&#263;ma"
  ]
  node [
    id 339
    label "Ereb"
  ]
  node [
    id 340
    label "&#378;le"
  ]
  node [
    id 341
    label "pomrok"
  ]
  node [
    id 342
    label "ciemny"
  ]
  node [
    id 343
    label "sowie_oczy"
  ]
  node [
    id 344
    label "zjawisko"
  ]
  node [
    id 345
    label "cloud"
  ]
  node [
    id 346
    label "spis"
  ]
  node [
    id 347
    label "odinstalowanie"
  ]
  node [
    id 348
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 349
    label "emitowanie"
  ]
  node [
    id 350
    label "odinstalowywanie"
  ]
  node [
    id 351
    label "instrukcja"
  ]
  node [
    id 352
    label "punkt"
  ]
  node [
    id 353
    label "teleferie"
  ]
  node [
    id 354
    label "emitowa&#263;"
  ]
  node [
    id 355
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 356
    label "sekcja_krytyczna"
  ]
  node [
    id 357
    label "prezentowa&#263;"
  ]
  node [
    id 358
    label "blok"
  ]
  node [
    id 359
    label "podprogram"
  ]
  node [
    id 360
    label "dzia&#322;"
  ]
  node [
    id 361
    label "broszura"
  ]
  node [
    id 362
    label "deklaracja"
  ]
  node [
    id 363
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 364
    label "struktura_organizacyjna"
  ]
  node [
    id 365
    label "zaprezentowanie"
  ]
  node [
    id 366
    label "informatyka"
  ]
  node [
    id 367
    label "booklet"
  ]
  node [
    id 368
    label "menu"
  ]
  node [
    id 369
    label "furkacja"
  ]
  node [
    id 370
    label "odinstalowa&#263;"
  ]
  node [
    id 371
    label "okno"
  ]
  node [
    id 372
    label "pirat"
  ]
  node [
    id 373
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 374
    label "ogranicznik_referencyjny"
  ]
  node [
    id 375
    label "kana&#322;"
  ]
  node [
    id 376
    label "zaprezentowa&#263;"
  ]
  node [
    id 377
    label "interfejs"
  ]
  node [
    id 378
    label "odinstalowywa&#263;"
  ]
  node [
    id 379
    label "folder"
  ]
  node [
    id 380
    label "course_of_study"
  ]
  node [
    id 381
    label "ram&#243;wka"
  ]
  node [
    id 382
    label "prezentowanie"
  ]
  node [
    id 383
    label "oferta"
  ]
  node [
    id 384
    label "examine"
  ]
  node [
    id 385
    label "zrobi&#263;"
  ]
  node [
    id 386
    label "close"
  ]
  node [
    id 387
    label "perform"
  ]
  node [
    id 388
    label "urzeczywistnia&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 93
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 95
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 12
    target 23
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 12
    target 107
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 12
    target 109
  ]
  edge [
    source 12
    target 110
  ]
  edge [
    source 12
    target 111
  ]
  edge [
    source 12
    target 112
  ]
  edge [
    source 12
    target 113
  ]
  edge [
    source 12
    target 114
  ]
  edge [
    source 12
    target 115
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 54
  ]
  edge [
    source 13
    target 55
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 116
  ]
  edge [
    source 14
    target 117
  ]
  edge [
    source 14
    target 118
  ]
  edge [
    source 14
    target 119
  ]
  edge [
    source 14
    target 120
  ]
  edge [
    source 14
    target 121
  ]
  edge [
    source 14
    target 122
  ]
  edge [
    source 14
    target 123
  ]
  edge [
    source 14
    target 124
  ]
  edge [
    source 14
    target 125
  ]
  edge [
    source 14
    target 126
  ]
  edge [
    source 14
    target 127
  ]
  edge [
    source 14
    target 128
  ]
  edge [
    source 14
    target 129
  ]
  edge [
    source 14
    target 130
  ]
  edge [
    source 14
    target 131
  ]
  edge [
    source 14
    target 132
  ]
  edge [
    source 14
    target 133
  ]
  edge [
    source 14
    target 134
  ]
  edge [
    source 14
    target 135
  ]
  edge [
    source 14
    target 136
  ]
  edge [
    source 14
    target 137
  ]
  edge [
    source 14
    target 138
  ]
  edge [
    source 14
    target 139
  ]
  edge [
    source 14
    target 140
  ]
  edge [
    source 14
    target 141
  ]
  edge [
    source 14
    target 142
  ]
  edge [
    source 14
    target 143
  ]
  edge [
    source 14
    target 144
  ]
  edge [
    source 14
    target 145
  ]
  edge [
    source 14
    target 146
  ]
  edge [
    source 14
    target 147
  ]
  edge [
    source 14
    target 148
  ]
  edge [
    source 14
    target 149
  ]
  edge [
    source 14
    target 150
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 151
  ]
  edge [
    source 15
    target 152
  ]
  edge [
    source 15
    target 153
  ]
  edge [
    source 15
    target 154
  ]
  edge [
    source 15
    target 155
  ]
  edge [
    source 15
    target 156
  ]
  edge [
    source 15
    target 157
  ]
  edge [
    source 15
    target 158
  ]
  edge [
    source 15
    target 159
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 23
  ]
  edge [
    source 16
    target 24
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 160
  ]
  edge [
    source 17
    target 161
  ]
  edge [
    source 17
    target 162
  ]
  edge [
    source 17
    target 163
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 164
  ]
  edge [
    source 18
    target 165
  ]
  edge [
    source 18
    target 166
  ]
  edge [
    source 18
    target 167
  ]
  edge [
    source 18
    target 168
  ]
  edge [
    source 18
    target 169
  ]
  edge [
    source 18
    target 170
  ]
  edge [
    source 18
    target 171
  ]
  edge [
    source 18
    target 172
  ]
  edge [
    source 18
    target 173
  ]
  edge [
    source 18
    target 174
  ]
  edge [
    source 18
    target 175
  ]
  edge [
    source 18
    target 176
  ]
  edge [
    source 18
    target 177
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 178
  ]
  edge [
    source 19
    target 179
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 180
  ]
  edge [
    source 21
    target 181
  ]
  edge [
    source 21
    target 182
  ]
  edge [
    source 21
    target 75
  ]
  edge [
    source 21
    target 183
  ]
  edge [
    source 21
    target 184
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 185
  ]
  edge [
    source 23
    target 186
  ]
  edge [
    source 23
    target 187
  ]
  edge [
    source 23
    target 188
  ]
  edge [
    source 23
    target 189
  ]
  edge [
    source 23
    target 190
  ]
  edge [
    source 23
    target 72
  ]
  edge [
    source 23
    target 191
  ]
  edge [
    source 23
    target 192
  ]
  edge [
    source 23
    target 193
  ]
  edge [
    source 23
    target 194
  ]
  edge [
    source 23
    target 195
  ]
  edge [
    source 23
    target 196
  ]
  edge [
    source 23
    target 197
  ]
  edge [
    source 23
    target 198
  ]
  edge [
    source 23
    target 199
  ]
  edge [
    source 23
    target 200
  ]
  edge [
    source 23
    target 201
  ]
  edge [
    source 23
    target 202
  ]
  edge [
    source 23
    target 203
  ]
  edge [
    source 23
    target 35
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 74
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 204
  ]
  edge [
    source 26
    target 205
  ]
  edge [
    source 26
    target 206
  ]
  edge [
    source 26
    target 207
  ]
  edge [
    source 26
    target 208
  ]
  edge [
    source 26
    target 209
  ]
  edge [
    source 26
    target 210
  ]
  edge [
    source 26
    target 211
  ]
  edge [
    source 26
    target 212
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 65
  ]
  edge [
    source 27
    target 213
  ]
  edge [
    source 27
    target 214
  ]
  edge [
    source 27
    target 215
  ]
  edge [
    source 27
    target 216
  ]
  edge [
    source 27
    target 217
  ]
  edge [
    source 27
    target 218
  ]
  edge [
    source 27
    target 219
  ]
  edge [
    source 27
    target 220
  ]
  edge [
    source 27
    target 221
  ]
  edge [
    source 27
    target 222
  ]
  edge [
    source 27
    target 223
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 224
  ]
  edge [
    source 28
    target 225
  ]
  edge [
    source 28
    target 226
  ]
  edge [
    source 28
    target 227
  ]
  edge [
    source 28
    target 228
  ]
  edge [
    source 28
    target 229
  ]
  edge [
    source 28
    target 230
  ]
  edge [
    source 28
    target 231
  ]
  edge [
    source 28
    target 232
  ]
  edge [
    source 28
    target 233
  ]
  edge [
    source 28
    target 234
  ]
  edge [
    source 28
    target 235
  ]
  edge [
    source 28
    target 236
  ]
  edge [
    source 28
    target 237
  ]
  edge [
    source 28
    target 238
  ]
  edge [
    source 28
    target 239
  ]
  edge [
    source 28
    target 240
  ]
  edge [
    source 28
    target 241
  ]
  edge [
    source 28
    target 242
  ]
  edge [
    source 28
    target 243
  ]
  edge [
    source 28
    target 166
  ]
  edge [
    source 28
    target 129
  ]
  edge [
    source 28
    target 244
  ]
  edge [
    source 28
    target 245
  ]
  edge [
    source 28
    target 246
  ]
  edge [
    source 28
    target 247
  ]
  edge [
    source 28
    target 248
  ]
  edge [
    source 28
    target 249
  ]
  edge [
    source 28
    target 250
  ]
  edge [
    source 28
    target 251
  ]
  edge [
    source 28
    target 252
  ]
  edge [
    source 28
    target 253
  ]
  edge [
    source 28
    target 254
  ]
  edge [
    source 28
    target 255
  ]
  edge [
    source 28
    target 256
  ]
  edge [
    source 28
    target 257
  ]
  edge [
    source 28
    target 258
  ]
  edge [
    source 28
    target 259
  ]
  edge [
    source 28
    target 260
  ]
  edge [
    source 28
    target 261
  ]
  edge [
    source 28
    target 262
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 42
  ]
  edge [
    source 30
    target 38
  ]
  edge [
    source 30
    target 43
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 263
  ]
  edge [
    source 31
    target 264
  ]
  edge [
    source 31
    target 265
  ]
  edge [
    source 31
    target 266
  ]
  edge [
    source 31
    target 267
  ]
  edge [
    source 31
    target 268
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 269
  ]
  edge [
    source 32
    target 242
  ]
  edge [
    source 32
    target 54
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 270
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 271
  ]
  edge [
    source 35
    target 272
  ]
  edge [
    source 35
    target 273
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 274
  ]
  edge [
    source 36
    target 113
  ]
  edge [
    source 36
    target 275
  ]
  edge [
    source 36
    target 276
  ]
  edge [
    source 36
    target 277
  ]
  edge [
    source 36
    target 278
  ]
  edge [
    source 36
    target 279
  ]
  edge [
    source 36
    target 280
  ]
  edge [
    source 36
    target 281
  ]
  edge [
    source 36
    target 282
  ]
  edge [
    source 36
    target 283
  ]
  edge [
    source 36
    target 284
  ]
  edge [
    source 36
    target 285
  ]
  edge [
    source 36
    target 286
  ]
  edge [
    source 36
    target 287
  ]
  edge [
    source 36
    target 288
  ]
  edge [
    source 36
    target 289
  ]
  edge [
    source 36
    target 290
  ]
  edge [
    source 36
    target 291
  ]
  edge [
    source 36
    target 292
  ]
  edge [
    source 36
    target 293
  ]
  edge [
    source 36
    target 294
  ]
  edge [
    source 36
    target 45
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 56
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 295
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 296
  ]
  edge [
    source 39
    target 71
  ]
  edge [
    source 39
    target 297
  ]
  edge [
    source 39
    target 298
  ]
  edge [
    source 39
    target 299
  ]
  edge [
    source 39
    target 221
  ]
  edge [
    source 39
    target 300
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 301
  ]
  edge [
    source 40
    target 302
  ]
  edge [
    source 40
    target 303
  ]
  edge [
    source 40
    target 304
  ]
  edge [
    source 40
    target 305
  ]
  edge [
    source 40
    target 306
  ]
  edge [
    source 40
    target 307
  ]
  edge [
    source 40
    target 308
  ]
  edge [
    source 40
    target 309
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 310
  ]
  edge [
    source 41
    target 311
  ]
  edge [
    source 41
    target 312
  ]
  edge [
    source 41
    target 313
  ]
  edge [
    source 41
    target 314
  ]
  edge [
    source 41
    target 315
  ]
  edge [
    source 41
    target 316
  ]
  edge [
    source 41
    target 317
  ]
  edge [
    source 41
    target 318
  ]
  edge [
    source 41
    target 319
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 274
  ]
  edge [
    source 45
    target 320
  ]
  edge [
    source 45
    target 321
  ]
  edge [
    source 45
    target 322
  ]
  edge [
    source 45
    target 323
  ]
  edge [
    source 45
    target 324
  ]
  edge [
    source 45
    target 325
  ]
  edge [
    source 45
    target 326
  ]
  edge [
    source 45
    target 327
  ]
  edge [
    source 45
    target 328
  ]
  edge [
    source 46
    target 329
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 330
  ]
  edge [
    source 48
    target 331
  ]
  edge [
    source 48
    target 332
  ]
  edge [
    source 48
    target 333
  ]
  edge [
    source 48
    target 334
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 335
  ]
  edge [
    source 50
    target 336
  ]
  edge [
    source 50
    target 337
  ]
  edge [
    source 50
    target 338
  ]
  edge [
    source 50
    target 339
  ]
  edge [
    source 50
    target 340
  ]
  edge [
    source 50
    target 341
  ]
  edge [
    source 50
    target 342
  ]
  edge [
    source 50
    target 343
  ]
  edge [
    source 50
    target 344
  ]
  edge [
    source 50
    target 345
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 346
  ]
  edge [
    source 51
    target 347
  ]
  edge [
    source 51
    target 348
  ]
  edge [
    source 51
    target 168
  ]
  edge [
    source 51
    target 227
  ]
  edge [
    source 51
    target 349
  ]
  edge [
    source 51
    target 350
  ]
  edge [
    source 51
    target 351
  ]
  edge [
    source 51
    target 352
  ]
  edge [
    source 51
    target 353
  ]
  edge [
    source 51
    target 354
  ]
  edge [
    source 51
    target 188
  ]
  edge [
    source 51
    target 355
  ]
  edge [
    source 51
    target 356
  ]
  edge [
    source 51
    target 357
  ]
  edge [
    source 51
    target 358
  ]
  edge [
    source 51
    target 359
  ]
  edge [
    source 51
    target 220
  ]
  edge [
    source 51
    target 360
  ]
  edge [
    source 51
    target 361
  ]
  edge [
    source 51
    target 362
  ]
  edge [
    source 51
    target 363
  ]
  edge [
    source 51
    target 364
  ]
  edge [
    source 51
    target 365
  ]
  edge [
    source 51
    target 366
  ]
  edge [
    source 51
    target 367
  ]
  edge [
    source 51
    target 368
  ]
  edge [
    source 51
    target 247
  ]
  edge [
    source 51
    target 281
  ]
  edge [
    source 51
    target 369
  ]
  edge [
    source 51
    target 370
  ]
  edge [
    source 51
    target 283
  ]
  edge [
    source 51
    target 371
  ]
  edge [
    source 51
    target 372
  ]
  edge [
    source 51
    target 287
  ]
  edge [
    source 51
    target 373
  ]
  edge [
    source 51
    target 374
  ]
  edge [
    source 51
    target 292
  ]
  edge [
    source 51
    target 375
  ]
  edge [
    source 51
    target 376
  ]
  edge [
    source 51
    target 377
  ]
  edge [
    source 51
    target 378
  ]
  edge [
    source 51
    target 379
  ]
  edge [
    source 51
    target 380
  ]
  edge [
    source 51
    target 381
  ]
  edge [
    source 51
    target 382
  ]
  edge [
    source 51
    target 383
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 384
  ]
  edge [
    source 52
    target 385
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 218
  ]
  edge [
    source 53
    target 386
  ]
  edge [
    source 53
    target 387
  ]
  edge [
    source 53
    target 388
  ]
  edge [
    source 55
    target 56
  ]
]
